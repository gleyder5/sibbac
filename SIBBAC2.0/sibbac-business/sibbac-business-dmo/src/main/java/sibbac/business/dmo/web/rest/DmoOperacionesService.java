package sibbac.business.dmo.web.rest;

import java.text.ParseException;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.dmo.bo.DmoInformeMensualBo;
import sibbac.business.dmo.bo.DmoOperacionesBo;
import sibbac.business.dmo.web.dto.PeticionDTO;
import sibbac.business.dmo.web.util.Constantes;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.security.session.UserSession;

@RestController
@RequestMapping("/dmo")
public class DmoOperacionesService {

  @Autowired
  private DmoOperacionesBo dmoOperacionesBo;

  @Autowired
  private DmoInformeMensualBo dmoInformeMensualBo;

  @Autowired
  private HttpSession session;

  private static final Logger LOG = LoggerFactory.getLogger(DmoOperacionesService.class);

  @RequestMapping(value = "/datadmocollection", method = RequestMethod.POST)
  public CallableResultDTO executeAction(@RequestBody PeticionDTO request) throws SIBBACBusinessException {
    CallableResultDTO result = new CallableResultDTO();
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;
    try {
      result = dmoOperacionesBo.updateValues(request, auditUser);
    }
    catch (ParseException e1) {
      LOG.error(Constantes.ApplicationMessages.ERROR_DISABLING_DMO_DATA.value(), e1);
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_DISABLING_DMO_DATA.value(), e1);
    }
    try {
      result = dmoOperacionesBo.insertValues(request, auditUser);
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_INSERT_DMO.value(), e);
      result.addErrorMessage(Constantes.ApplicationMessages.ERROR_INSERT_DMO.value());
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_INSERT_DMO.value(), e);
    }
    return result;
  }

  @RequestMapping(value = "/dmoReport", method = RequestMethod.POST)
  public CallableResultDTO dmoReport(@RequestBody PeticionDTO request) throws SIBBACBusinessException {
    CallableResultDTO result = new CallableResultDTO();
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;

    try {
      result = dmoInformeMensualBo.sendMonthlyReport(request, auditUser);
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value(), e);
      result.addErrorMessage("Error en la insercción de datos DMO");
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value(), e);
    }
    return result;
  }
}
