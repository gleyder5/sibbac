package sibbac.business.dmo.web.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import sibbac.business.dmo.db.model.DmoOperacionesEntity;

public class DmoOperacionesDTO<T extends DmoOperacionesEntity> implements Serializable {

  /**
   * @author enieto
   *
   */
  
  private static final long serialVersionUID = -8493814934362766461L;
  private int idOperacion;
  private String idCliente;
  private String tipoOperacion;
  private Timestamp fechaOperacion;
  private BigDecimal importeEfectivo;
  private String moneda;
  private String cdcleare;
  private String paisTitular;
  private String provinciaTitular;
  private String municipioTitular;
  private String aliasCliente;
  private String cif;
  private String paisResidencia;
  private String domicilio;
  private String codigoAliasCliente;
  private int activo;
  private Timestamp fechaAlta;
  private Timestamp fechaMod;
  private Timestamp fechaBaja;
  private String usuarioAlta;
  private String usuarioMod;
  private String usuarioBaja;

  public DmoOperacionesDTO() {

  }

  public DmoOperacionesDTO(String id_cliente, String tipo_operacion, Timestamp fecha_operacion, BigDecimal importe_efectivo,
      String moneda, String cdcleare, String pais_titular, String provincia_titular, String municipio_titular,
      String alias_cliente, String cif, String pais_residencia, String domicilio, String codigo_alias_cliente,
      int activo, Timestamp fecha_alta, String usuario_alta) {
    this.idCliente = id_cliente;
    this.tipoOperacion = tipo_operacion;
    this.fechaOperacion = (fecha_operacion != null ? (Timestamp) fecha_operacion.clone() : null);
    this.importeEfectivo = importe_efectivo;
    this.moneda = moneda;
    this.cdcleare = cdcleare;
    this.paisTitular = pais_titular;
    this.provinciaTitular = provincia_titular;
    this.municipioTitular = municipio_titular;
    this.aliasCliente = alias_cliente;
    this.cif = cif;
    this.paisResidencia = pais_residencia;
    this.domicilio = domicilio;
    this.codigoAliasCliente = codigo_alias_cliente;
    this.activo = activo;
    this.fechaAlta = (fecha_alta != null ? (Timestamp) fecha_alta.clone() : null);
    this.usuarioAlta = usuario_alta;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  /**
   * @return the idOperacion
   */
  public int getIdOperacion() {
    return idOperacion;
  }

  /**
   * @param idOperacion the idOperacion to set
   */
  public void setIdOperacion(int idOperacion) {
    this.idOperacion = idOperacion;
  }

  /**
   * @return the idCliente
   */
  public String getIdCliente() {
    return idCliente;
  }

  /**
   * @param idCliente the idCliente to set
   */
  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  /**
   * @return the tipoOperacion
   */
  public String getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * @param tipoOperacion the tipoOperacion to set
   */
  public void setTipoOperacion(String tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * @return the importeEfectivo
   */
  public BigDecimal getImporteEfectivo() {
    return importeEfectivo;
  }

  /**
   * @param importeEfectivo the importeEfectivo to set
   */
  public void setImporteEfectivo(BigDecimal importeEfectivo) {
    this.importeEfectivo = importeEfectivo;
  }

  /**
   * @return the moneda
   */
  public String getMoneda() {
    return moneda;
  }

  /**
   * @param moneda the moneda to set
   */
  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }

  /**
   * @return the cdcleare
   */
  public String getCdcleare() {
    return cdcleare;
  }

  /**
   * @param cdcleare the cdcleare to set
   */
  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  /**
   * @return the paisTitular
   */
  public String getPaisTitular() {
    return paisTitular;
  }

  /**
   * @param paisTitular the paisTitular to set
   */
  public void setPaisTitular(String paisTitular) {
    this.paisTitular = paisTitular;
  }

  /**
   * @return the provinciaTitular
   */
  public String getProvinciaTitular() {
    return provinciaTitular;
  }

  /**
   * @param provinciaTitular the provinciaTitular to set
   */
  public void setProvinciaTitular(String provinciaTitular) {
    this.provinciaTitular = provinciaTitular;
  }

  /**
   * @return the municipioTitular
   */
  public String getMunicipioTitular() {
    return municipioTitular;
  }

  /**
   * @param municipioTitular the municipioTitular to set
   */
  public void setMunicipioTitular(String municipioTitular) {
    this.municipioTitular = municipioTitular;
  }

  /**
   * @return the aliasCliente
   */
  public String getAliasCliente() {
    return aliasCliente;
  }

  /**
   * @param aliasCliente the aliasCliente to set
   */
  public void setAliasCliente(String aliasCliente) {
    this.aliasCliente = aliasCliente;
  }

  /**
   * @return the cif
   */
  public String getCif() {
    return cif;
  }

  /**
   * @param cif the cif to set
   */
  public void setCif(String cif) {
    this.cif = cif;
  }

  /**
   * @return the paisResidencia
   */
  public String getPaisResidencia() {
    return paisResidencia;
  }

  /**
   * @param paisResidencia the paisResidencia to set
   */
  public void setPaisResidencia(String paisResidencia) {
    this.paisResidencia = paisResidencia;
  }

  /**
   * @return the domicilio
   */
  public String getDomicilio() {
    return domicilio;
  }

  /**
   * @param domicilio the domicilio to set
   */
  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }

  /**
   * @return the codigoAliasCliente
   */
  public String getCodigoAliasCliente() {
    return codigoAliasCliente;
  }

  /**
   * @param codigoAliasCliente the codigoAliasCliente to set
   */
  public void setCodigoAliasCliente(String codigoAliasCliente) {
    this.codigoAliasCliente = codigoAliasCliente;
  }

  /**
   * @return the activo
   */
  public int getActivo() {
    return activo;
  }

  /**
   * @param activo the activo to set
   */
  public void setActivo(int activo) {
    this.activo = activo;
  }

  /**
   * @return the usuarioAlta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * @param usuarioAlta the usuarioAlta to set
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * @return the usuarioMod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * @param usuarioMod the usuarioMod to set
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * @return the usuarioBaja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * @param usuarioBaja the usuarioBaja to set
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

  public Timestamp getFechaOperacion() {
    return fechaOperacion != null ? (Timestamp) fechaOperacion.clone() : null;
  }

  public void setFechaOperacion(Timestamp fecha_operacion) {
    this.fechaOperacion = (fechaOperacion != null ? (Timestamp) fechaOperacion.clone() : null);
  }

  public Timestamp getFechaAlta() {
    return fechaAlta != null ? (Timestamp) fechaAlta.clone() : null;
  }

  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (fechaAlta != null ? (Timestamp) fechaAlta.clone() : null);
  }

  public Timestamp getFechaMod() {
    return fechaMod != null ? (Timestamp) fechaMod.clone() : null;
  }

  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (fechaMod != null ? (Timestamp) fechaMod.clone() : null);
  }

  public Timestamp getFechaBaja() {
    return fechaBaja != null ? (Timestamp) fechaBaja.clone() : null;
  }

  public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaBaja = (fechaBaja != null ? (Timestamp) fechaBaja.clone() : null);
  }

}
