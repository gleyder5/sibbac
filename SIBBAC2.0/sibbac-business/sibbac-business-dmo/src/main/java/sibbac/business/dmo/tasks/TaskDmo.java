package sibbac.business.dmo.tasks;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.dmo.bo.DmoOperacionesBo;
import sibbac.business.dmo.db.dao.DmoOperacionesDAO;
import sibbac.business.dmo.web.util.Constantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
// 0 0 7 1 * ? Fire at 7:00am on the 1st day of every month
// 0 15 10 L * ? Fire at 10:15am on the last day of every month
@SIBBACJob(group = Task.GROUP_DMO.NAME, name = Task.GROUP_DMO.JOB_DATA_COLLECTION, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 7 1 * ?")
public class TaskDmo extends WrapperTaskConcurrencyPrevent {
  @Autowired
  private DmoOperacionesBo dmoOpBo;

  @Autowired
  private DmoOperacionesDAO dao;

  private String currentJob;

  @Override
  public void executeTask() throws Exception {
    currentJob = "[TaskDmoCollection :: ] ";
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_TASK_START.value() + currentJob);
    }
    Date referenceDate = new Date();
    Calendar c = Calendar.getInstance();
    c.setTime(referenceDate);
    Date dateFrom = null;
    Date dateTo = null;
    // Mes anterior
    c.add(Calendar.MONTH, -1);
    // Primer día del mes
    c.set(Calendar.DAY_OF_MONTH, 1);
    dateFrom = c.getTime();
    // Último día del mes
    c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
    dateTo = c.getTime();
    String auditUser = Constantes.Session.USER_CLIENTE.value();

    java.sql.Date sqldateFrom = new java.sql.Date(dateFrom.getTime());
    java.sql.Date sqldateTo = new java.sql.Date(dateTo.getTime());

    // Baja lógica
    dao.setDataToInactive(Constantes.Activo.INACTIVO.value(), new Timestamp(System.currentTimeMillis()),
        Constantes.Session.USER_CLIENTE.value(), sqldateFrom, sqldateTo);

    if (LOG.isDebugEnabled()) {
      LOG.debug(currentJob + Constantes.LoggingMessages.DEBUG_TASK_NATIONAL_DATA.value());
    }
    boolean existNationalData = dmoOpBo.getNationalData(sqldateFrom, sqldateTo, auditUser);
    if (LOG.isDebugEnabled()) {
      LOG.debug(currentJob + Constantes.LoggingMessages.DEBUG_TASK_INTERNATIONAL_DATA.value());
    }
    boolean existInternationalData = dmoOpBo.getInternationalData(sqldateFrom, sqldateTo, auditUser);

    if (existNationalData && existInternationalData) {
      LOG.info(currentJob + Constantes.ApplicationMessages.DMO_DATA_COLLECTION_OK.value());
    }
    else if (existNationalData && !existInternationalData) {
      LOG.info(currentJob + Constantes.ApplicationMessages.DMO_NATIONAL_DATA_COLLECTION_OK.value());
    }
    else if (!existNationalData && existInternationalData) {
      LOG.info(currentJob + Constantes.ApplicationMessages.DMO_INTERNATIONAL_DATA_COLLECTION_OK.value());
    }
    else {
      LOG.info(currentJob + Constantes.ApplicationMessages.DMO_DATA_COLLECTION_KO.value());
    }

  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_DMO;
  }
}
