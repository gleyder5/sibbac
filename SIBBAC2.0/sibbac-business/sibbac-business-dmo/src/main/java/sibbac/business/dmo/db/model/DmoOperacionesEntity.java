package sibbac.business.dmo.db.model;

/**
 * @author enieto
 *
 */
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.database.DBConstants;

@Table(name = DBConstants.PBCDMO.TMCT0_DMO_OPERACIONES)
@Entity
public class DmoOperacionesEntity implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1567174515769808844L;
  /**
   * 
   */

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_OPERACION", nullable = false)
  private int idOperacion;

  @Column(name = "ID_CLIENTE ", nullable = false, length = 20)
  private String idCliente;

  @Column(name = "TIPO_OPERACION ", nullable = false, length = 3)
  private String tipoOperacion;

  @Column(name = "FECHA_OPERACION ", nullable = false)
  private Timestamp fechaOperacion; // Timestamp

  @Column(name = "IMPORTE_EFECTIVO ", nullable = false)
  private BigDecimal importeEfectivo;

  @Column(name = "MONEDA ", nullable = false, length = 20)
  private String moneda;

  @Column(name = "CDCLEARE ", nullable = false, length = 20)
  private String cdCleare;

  @Column(name = "PAIS_TITULAR ", nullable = false, length = 100)
  private String paisTitular;

  @Column(name = "PROVINCIA_TITULAR ", nullable = false, length = 100)
  private String provinciaTitular;

  @Column(name = "MUNICIPIO_TITULAR ", nullable = false, length = 100)
  private String municipioTitular;

  @Column(name = "ALIAS_CLIENTE ", nullable = false, length = 50)
  private String aliasCliente;

  @Column(name = "CIF ", nullable = false, length = 20)
  private String cif;

  @Column(name = "PAIS_RESIDENCIA ", nullable = false, length = 100)
  private String paisResidencia;

  @Column(name = "DOMICILIO ", nullable = false, length = 200)
  private String domicilio;

  @Column(name = "CODIGO_ALIAS_CLIENTE ", nullable = false, length = 20)
  private String codigoAliasCliente;

  @Column(name = "ACTIVO ", nullable = false, length = 1)
  private int activo;

  @Column(name = "FECHA_ALTA ", nullable = false)
  private Timestamp fechaAlta; // Timestamp

  @Column(name = "FECHA_MOD ", nullable = true)
  private Timestamp fechaMod; // Timestamp

  @Column(name = "FECHA_BAJA ", nullable = true)
  private Timestamp fechaBaja; // Timestamp

  @Column(name = "USUARIO_ALTA ", nullable = false, length = 256)
  private String usuarioAlta;

  @Column(name = "USUARIO_MOD ", length = 256)
  private String usuarioMod;

  @Column(name = "USUARIO_BAJA ", length = 256)
  private String usuarioBaja;

  public DmoOperacionesEntity() {
  }

  /**
   * @return the idOperacion
   */
  public int getIdOperacion() {
    return idOperacion;
  }

  /**
   * @param idOperacion the idOperacion to set
   */
  public void setIdOperacion(int idOperacion) {
    this.idOperacion = idOperacion;
  }

  /**
   * @return the idCliente
   */
  public String getIdCliente() {
    return idCliente;
  }

  /**
   * @param idCliente the idCliente to set
   */
  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  /**
   * @return the tipoOperacion
   */
  public String getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * @param tipoOperacion the tipoOperacion to set
   */
  public void setTipoOperacion(String tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * @return the fechaOperacion
   */
  public Timestamp getFechaOperacion() {
    return fechaOperacion != null ? (Timestamp) fechaOperacion.clone() : null;
  }

  /**
   * @param fechaOperacion the fechaOperacion to set
   */
  public void setFechaOperacion(Timestamp fechaOperacion) {
    this.fechaOperacion = (Timestamp) fechaOperacion.clone();
  }

  /**
   * @return the importeEfectivo
   */
  public BigDecimal getImporteEfectivo() {
    return importeEfectivo;
  }

  /**
   * @param importeEfectivo the importeEfectivo to set
   */
  public void setImporteEfectivo(BigDecimal importeEfectivo) {
    this.importeEfectivo = importeEfectivo;
  }

  /**
   * @return the moneda
   */
  public String getMoneda() {
    return moneda;
  }

  /**
   * @param moneda the moneda to set
   */
  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }

  /**
   * @return the cdCleare
   */
  public String getCdCleare() {
    return cdCleare;
  }

  /**
   * @param cdCleare the cdCleare to set
   */
  public void setCdCleare(String cdCleare) {
    this.cdCleare = cdCleare;
  }

  /**
   * @return the paisTitular
   */
  public String getPaisTitular() {
    return paisTitular;
  }

  /**
   * @param paisTitular the paisTitular to set
   */
  public void setPaisTitular(String paisTitular) {
    this.paisTitular = paisTitular;
  }

  /**
   * @return the provinciaTitular
   */
  public String getProvinciaTitular() {
    return provinciaTitular;
  }

  /**
   * @param provinciaTitular the provinciaTitular to set
   */
  public void setProvinciaTitular(String provinciaTitular) {
    this.provinciaTitular = provinciaTitular;
  }

  /**
   * @return the municipioTitular
   */
  public String getMunicipioTitular() {
    return municipioTitular;
  }

  /**
   * @param municipioTitular the municipioTitular to set
   */
  public void setMunicipioTitular(String municipioTitular) {
    this.municipioTitular = municipioTitular;
  }

  /**
   * @return the aliasCliente
   */
  public String getAliasCliente() {
    return aliasCliente;
  }

  /**
   * @param aliasCliente the aliasCliente to set
   */
  public void setAliasCliente(String aliasCliente) {
    this.aliasCliente = aliasCliente;
  }

  /**
   * @return the cif
   */
  public String getCif() {
    return cif;
  }

  /**
   * @param cif the cif to set
   */
  public void setCif(String cif) {
    this.cif = cif;
  }

  /**
   * @return the paisResidencia
   */
  public String getPaisResidencia() {
    return paisResidencia;
  }

  /**
   * @param paisResidencia the paisResidencia to set
   */
  public void setPaisResidencia(String paisResidencia) {
    this.paisResidencia = paisResidencia;
  }

  /**
   * @return the domicilio
   */
  public String getDomicilio() {
    return domicilio;
  }

  /**
   * @param domicilio the domicilio to set
   */
  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }

  /**
   * @return the codigoAliasCliente
   */
  public String getCodigoAliasCliente() {
    return codigoAliasCliente;
  }

  /**
   * @param codigoAliasCliente the codigoAliasCliente to set
   */
  public void setCodigoAliasCliente(String codigoAliasCliente) {
    this.codigoAliasCliente = codigoAliasCliente;
  }

  /**
   * @return the activo
   */
  public int getActivo() {
    return activo;
  }

  /**
   * @param activo the activo to set
   */
  public void setActivo(int activo) {
    this.activo = activo;
  }

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Timestamp getFechaAlta() {
    return fechaAlta != null ? (Timestamp) fechaAlta.clone() : null;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (Timestamp) fechaAlta.clone();
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Timestamp getFechaMod() {
    return fechaMod != null ? (Timestamp) fechaMod.clone() : null;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (Timestamp) fechaMod.clone();
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Timestamp getFechaBaja() {
    return fechaBaja != null ? (Timestamp) fechaBaja.clone() : null;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaBaja = (Timestamp) fechaBaja.clone();
  }

  /**
   * @return the usuarioAlta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * @param usuarioAlta the usuarioAlta to set
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * @return the usuarioMod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * @param usuarioMod the usuarioMod to set
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * @return the usuarioBaja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * @param usuarioBaja the usuarioBaja to set
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

}
