package sibbac.business.dmo.bo;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.dmo.bo.common.DmoCommonBo;
import sibbac.business.dmo.db.dao.DmoOperacionesDAO;
import sibbac.business.dmo.db.model.DmoOperacionesEntity;
import sibbac.business.dmo.web.dto.DmoOperacionesDTO;
import sibbac.business.dmo.web.dto.PeticionDTO;
import sibbac.business.dmo.web.util.Constantes;
import sibbac.business.dmo.web.util.ConversionDtoToEntity;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class DmoOperacionesBo extends AbstractBo<DmoOperacionesEntity, Integer, DmoOperacionesDAO> {

  @Autowired
  private DmoOperacionesDAO dao;

  @Autowired
  private DmoCommonBo boCommon;

  @PersistenceContext
  private EntityManager em;

  private String currentProcess = "";

  /**
   * Realiza la baja lógica de los datos en estado activo de acuerdo con el
   * rango de fechas seleccionado.
   * @param request
   * @param auditUser
   * @return
   * @throws ParseException
   * @throws SIBBACBusinessException
   */
  public CallableResultDTO updateValues(PeticionDTO request, String auditUser)
      throws ParseException, SIBBACBusinessException {
    CallableResultDTO result = new CallableResultDTO();
    currentProcess = "Baja lógica";
    try {
      if (LOG.isDebugEnabled()) {
        LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_ENTER.value() + currentProcess);
      }
      Date dateFrom = boCommon.stringToDate((String) request.getParams().get("fechaDesde"));
      Date dateTo = boCommon.stringToDate((String) request.getParams().get("fechaHasta"));
      Timestamp dischargeDate = new Timestamp(System.currentTimeMillis());
      if (dateFrom != null) {
        if (dateTo != null) {
          dao.setDataToInactive(Constantes.Activo.INACTIVO.value(), dischargeDate,
              Constantes.Session.USER_CLIENTE.value(), dateFrom, dateTo);
          if (LOG.isDebugEnabled()) {
            LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_EXECUTION_OK.value() + currentProcess);
          }
        }
        else {
          result.addErrorMessage(Constantes.ApplicationMessages.ERROR_END_DATE.value());
          LOG.warn(Constantes.ApplicationMessages.ERROR_END_DATE.value());
        }
      }
      else {
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_START_DATE.value());
        LOG.warn(Constantes.ApplicationMessages.ERROR_START_DATE.value());
      }
    }
    catch (IllegalArgumentException e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_INPUT_DATA.toString(), e);
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_INPUT_DATA.toString(), e);
    }
    result.addMessage(Constantes.ApplicationMessages.UNSUBSCRIBE_LOGIC_PROCESS_OK.toString());
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.ApplicationMessages.UNSUBSCRIBE_LOGIC_PROCESS_OK.toString());
    }
    return result;
  }

  /**
   * Proceso encargado de genrear la insercción de los registros recopilados
   * para DMO
   * @param request
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  public CallableResultDTO insertValues(PeticionDTO request, String auditUser) throws SIBBACBusinessException {
    currentProcess = "Insercción datos DMO";
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_ENTER.value() + currentProcess);
    }
    CallableResultDTO result = new CallableResultDTO();
    List<Future<CallableResultDTO>> releasedProcessList = new ArrayList<Future<CallableResultDTO>>();
    Timestamp startDate = new Timestamp(System.currentTimeMillis());
    releaseProcess(request, auditUser, releasedProcessList);
    for (Future<CallableResultDTO> process : releasedProcessList) {
      try {
        result.append(process.get(60, TimeUnit.MINUTES));
        Timestamp endDate = new Timestamp(System.currentTimeMillis());
        long diffMinutes = boCommon.getExecutedMinutes(startDate, endDate);
        if (diffMinutes > 4) {
          boCommon.sendOverTimeMail(auditUser);
        }
      }
      catch (InterruptedException | ExecutionException e) {
        LOG.error(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value(), e);
        result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
      }
      catch (TimeoutException e) {
        LOG.info(Constantes.ApplicationMessages.ERROR_TIMEOUT.value(), e);
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_TIMEOUT.value());
      }
    }
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_EXECUTION_OK.value() + currentProcess);
    }
    return result;
  }

  /**
   * Proceso de activación del hilo de ejecución
   * @param request
   * @param auditUser
   * @param releasedProcessList
   * @throws SIBBACBusinessException
   */
  private void releaseProcess(PeticionDTO request, String auditUser,
      List<Future<CallableResultDTO>> releasedProcessList) throws SIBBACBusinessException {
    currentProcess = "Proceso de lanzamiento de hilos insercción de datos DMO";
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_ENTER.value() + currentProcess);
    }
    ExecutorService executor;
    Callable<CallableResultDTO> callableGenerate;
    Future<CallableResultDTO> futureProcess;
    executor = Executors.newSingleThreadExecutor();
    try {
      callableGenerate = setNewCallableInsertValues(request, auditUser);
      futureProcess = executor.submit(callableGenerate);
      releasedProcessList.add(futureProcess);
    }
    catch (Exception e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_THREAD_RELEASE.value(), executor, e);
      executor.shutdownNow();
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_THREAD_RELEASE.value(), e);
    }
    finally {
      if (!executor.isShutdown()) {
        if (LOG.isDebugEnabled()) {
          LOG.debug(Constantes.LoggingMessages.DEBUG_THREAD_SHUTDOWN.value(), executor);
        }
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        if (LOG.isDebugEnabled()) {
          LOG.debug(Constantes.LoggingMessages.DEBUG_THREAD_RUNING.value(), executor);
        }
        try {
          executor.awaitTermination(60, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.error(Constantes.ApplicationMessages.ERROR_THREAD_RELEASE.value(), e);
        }
      }
    }
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_EXECUTION_OK.value() + currentProcess);
    }
  }

  /**
   * Insercción de operaciones
   * @param request
   * @param auditUser
   * @return
   */
  private Callable<CallableResultDTO> setNewCallableInsertValues(final PeticionDTO request, final String auditUser) {
    Callable<CallableResultDTO> c = new Callable<CallableResultDTO>() {
      @Override
      public CallableResultDTO call() throws Exception {
        CallableResultDTO res = null;
        currentProcess = "Insercción operaciones DMO";
        if (LOG.isDebugEnabled()) {
          LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_ENTER.value() + currentProcess);
        }
        try {
          res = createDMOValues(request, auditUser);
          LOG.trace(Constantes.ApplicationMessages.EXECUTION_OK.value() + currentProcess);
        }
        catch (Exception e) {
          res = new CallableResultDTO();
          LOG.error(Constantes.ApplicationMessages.ERROR_LOADING_DMO_DATA.value(), e);
          res.addErrorMessage(Constantes.ApplicationMessages.ERROR_LOADING_DMO_DATA.value());
        }
        return res;
      }
    };
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_EXECUTION_OK.value() + currentProcess);
    }
    return c;
  }

  /**
   * De acuerdo con las fechas elegidas por pantalla inserta dependiendo de si
   * existen o no los datos de clientes y operaciones tanto para nacional como
   * para internacional
   * @param request
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  private CallableResultDTO createDMOValues(PeticionDTO request, String auditUser) throws SIBBACBusinessException {
    CallableResultDTO result = new CallableResultDTO();
    try {
      Date dateFrom = boCommon.stringToDate((String) request.getParams().get("fechaDesde"));
      Date dateTo = boCommon.stringToDate((String) request.getParams().get("fechaHasta"));
      boolean existNationalData = false;
      boolean existInterationalData = false;
      if (dateFrom != null) {
        if (dateTo != null) {
          existNationalData = getNationalData(dateFrom, dateTo, auditUser);
          existInterationalData = getInternationalData(dateFrom, dateTo, auditUser);
          if (existNationalData && existInterationalData) {
            LOG.info(Constantes.ApplicationMessages.DMO_DATA_COLLECTION_OK.value());
            result.addMessage(Constantes.ApplicationMessages.DMO_DATA_COLLECTION_OK.value());
          }
          else if (existNationalData && !existInterationalData) {
            LOG.info(Constantes.ApplicationMessages.DMO_NATIONAL_DATA_COLLECTION_OK.value());
            result.addMessage(Constantes.ApplicationMessages.DMO_NATIONAL_DATA_COLLECTION_OK.value());
          }
          else if (!existNationalData && existInterationalData) {
            LOG.info(Constantes.ApplicationMessages.DMO_INTERNATIONAL_DATA_COLLECTION_OK.value());
            result.addMessage(Constantes.ApplicationMessages.DMO_INTERNATIONAL_DATA_COLLECTION_OK.value());
          }
          else {
            LOG.info(Constantes.ApplicationMessages.DMO_DATA_COLLECTION_KO.value());
            result.addMessage(Constantes.ApplicationMessages.DMO_DATA_COLLECTION_KO.value());
          }
        }
        else {
          LOG.error(Constantes.ApplicationMessages.ERROR_END_DATE.value());
          result.addErrorMessage(Constantes.ApplicationMessages.ERROR_END_DATE.value());
        }
      }
      else {
        LOG.error(Constantes.ApplicationMessages.ERROR_START_DATE.value());
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_START_DATE.value());
      }
    }
    catch (ParseException e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_INPUT_DATA.value(), e);
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_INPUT_DATA.value(), e);
    }
    return result;
  }

  /**
   * Genera los datos de clientes y operaciones de carácter nacional (incluyendo
   * el job mensual)
   * @param dateFrom
   * @param dateTo
   * @return
   */
  public boolean getNationalData(Date dateFrom, Date dateTo, String auditUser) {
    boolean existNationalData = false;
    // Datos cliente
    List<Object[]> nationalClientList = new ArrayList<>();
    nationalClientList = dao.findNationalClientsData(Constantes.Estado.ASIGNADO.value(),
        Constantes.Indicador.INDICADOR_DMO.value(), dateFrom, dateTo, Constantes.Indicador.CDESTADOASIG65.value(),
        Constantes.Estado.CDCLSMDON.value());
    if (nationalClientList != null && !nationalClientList.isEmpty() && nationalClientList.size() != 0) {
      // Datos operaciones
      List<Object[]> operationClientList = new ArrayList<Object[]>();
      List<String> cdAliasFilterList = getAliasFilterList(nationalClientList);
      operationClientList = dao.findNationalOperationsData(Constantes.Tmct0cfg.APPLICATION.value(),
          Constantes.Tmct0cfg.PROCESS.value(), Constantes.Tmct0cfg.KEYNAME_NBMONEDA.value(),
          Constantes.Tmct0cfg.KEYNAME_CDSUCURS.value(), dateFrom, dateTo, Constantes.Indicador.CDESTADOASIG65.value(),
          Constantes.Estado.CDCLSMDON.value(), cdAliasFilterList);
      if (operationClientList != null && !operationClientList.isEmpty() && operationClientList.size() != 0) {
        // Definimos los registros e insertamos en la tabla de operaciones
        // asociada a DMO
        List<DmoOperacionesEntity> tmct0DmoOperacionesList = new ArrayList<>();
        for (Object[] resultClientList : nationalClientList) {
          for (Object[] resultOperationList : operationClientList) {
            if (resultClientList[0].toString().equalsIgnoreCase(resultOperationList[8].toString())) {
              tmct0DmoOperacionesList.add(setDmoOperacionesEntity(resultClientList, resultOperationList));
            }
          }
        }
        this.save(tmct0DmoOperacionesList);
        existNationalData = true;
      }
    }
    return existNationalData;
  }

  /**
   * Define una entidad para el tipo de operaciones asociada a DMO
   * @param resultClientList
   * @param resultOperationList
   * @return
   */
  @SuppressWarnings("unchecked")
  private DmoOperacionesEntity setDmoOperacionesEntity(Object[] resultClientList, Object[] resultOperationList) {
    @SuppressWarnings("rawtypes")
    DmoOperacionesDTO dmoDTO = setDmoOperacionesDTO(resultClientList, resultOperationList);
    DmoOperacionesEntity entity = new DmoOperacionesEntity();
    ConversionDtoToEntity.dtoToEntityDmoDataCollection(entity, dmoDTO);
    return entity;
  }

  /**
   * Creación de un data table object con los campos necesarios para las
   * operaciones asociadas a DMO
   * @param resultClientList
   * @param resultOperationList
   * @return
   */
  @SuppressWarnings("rawtypes")
  private DmoOperacionesDTO setDmoOperacionesDTO(Object[] resultClientList, Object[] resultOperationList) {
    // CDBROCLI
    String idCLiente = "";
    if (resultClientList[5] != null) {
      idCLiente = resultClientList[5].toString();
    }
    // TPOPERAC
    String tipoOperacion = "";
    if (resultOperationList[0] != null) {
      tipoOperacion = resultOperationList[0].toString();
    }
    // FHOPERAC
    Date fechaOperacionDate = null;
    if (resultOperationList[1] != null) {
      fechaOperacionDate = (Date) resultOperationList[1];
    }
    // IMOPERAC
    BigDecimal importeEfectivo = BigDecimal.ZERO;
    if (resultOperationList[2] != null) {
      importeEfectivo = (BigDecimal) resultOperationList[2];
    }
    // NBMONEDA
    String moneda = "";
    if (resultOperationList[3] != null) {
      moneda = resultOperationList[3].toString();
    }
    // CDCLEARE = CDSUCURS
    String cdcleare = "";
    if (resultOperationList[4] != null) {
      cdcleare = resultOperationList[4].toString();
    }
    // NBPAISDE
    String paisTitular = "";
    if (resultOperationList[5] != null) {
      paisTitular = resultOperationList[5].toString();
    }
    // NBINEPRO
    String provinciaTitular = null;
    if (resultOperationList[6] != null) {
      provinciaTitular = resultOperationList[6].toString();
    }
    // NBINEMUN
    String municipioTitular = null;
    if (resultOperationList[7] != null) {
      municipioTitular = resultOperationList[7].toString();
    }
    // NBDENSOC
    String aliasCliente = "";
    if (resultClientList[3] != null) {
      aliasCliente = resultClientList[3].toString();
    }
    // NUDNICIF
    String cif = "";
    if (resultClientList[1] != null) {
      cif = resultClientList[1].toString();
    }
    // NBPAISNA
    String paisResidencia = "";
    if (resultClientList[2] != null) {
      paisResidencia = resultClientList[2].toString();
    }
    // NBDOMICI
    String domicilio = "";
    if (resultClientList[4] != null) {
      domicilio = resultClientList[4].toString();
    }
    // CDALIASS
    String codigoAliasCliente = "";
    if (resultClientList[0] != null) {
      codigoAliasCliente = resultClientList[0].toString();
    }
    int activo = Constantes.Activo.ACTIVO.value();
    Timestamp fechaAlta = new Timestamp(System.currentTimeMillis());
    String usuarioAlta = Constantes.Session.USER_CLIENTE.value();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Timestamp fechaOperacion = new Timestamp(System.currentTimeMillis());
    try {
      if (fechaOperacion != null) {
        fechaOperacion = new Timestamp((dateFormat.parse(fechaOperacionDate.toString())).getTime());
      }
    }
    catch (ParseException e) {
      LOG.error(Constantes.ApplicationMessages.OBJECT_NULL.value());
    }
    DmoOperacionesDTO dmoDTO = new DmoOperacionesDTO(idCLiente, tipoOperacion, fechaOperacion, importeEfectivo, moneda,
        cdcleare, paisTitular, provinciaTitular, municipioTitular, aliasCliente, cif, paisResidencia, domicilio,
        codigoAliasCliente, activo, fechaAlta, usuarioAlta);
    return dmoDTO;
  }

  /**
   * Genera los datos de clientes y operaciones de carácter internacional
   * (incluyendo el job mensual)
   * @param dateFrom
   * @param dateTo
   * @param auditUser
   * @return
   */
  public boolean getInternationalData(Date dateFrom, Date dateTo, String auditUser) {
    boolean existInterationalData = false;
    // Datos cliente
    List<Object[]> internationalClientsList = new ArrayList<Object[]>();
    internationalClientsList = dao.findInternationalClientsData(Constantes.Indicador.INDICADOR_DMO.value(),
        Constantes.Estado.ASIGNADO.value(), dateFrom, dateTo, Constantes.Estado.CDCLSMDOE.value());
    if (internationalClientsList != null && !internationalClientsList.isEmpty()
        && internationalClientsList.size() != 0) {
      // Datos operaciones
      List<Object[]> operationsInternationalList = new ArrayList<Object[]>();
      List<String> cdAliasFilterlList = getAliasFilterList(internationalClientsList);
      operationsInternationalList = dao.findInternationalOperationsData(Constantes.Tmct0cfg.APPLICATION.value(),
          Constantes.Tmct0cfg.PROCESS.value(), Constantes.Tmct0cfg.KEYNAME_NBMONEDA.value(),
          Constantes.Tmct0cfg.KEYNAME_CDSUCURS.value(), dateFrom, dateTo, cdAliasFilterlList,
          Constantes.Estado.CDCLSMDOE.value());
      if (operationsInternationalList != null && !operationsInternationalList.isEmpty()
          && operationsInternationalList.size() != 0) {
        // // Definimos los registros e insertamos en la tabla de operaciones
        // asociada a DMO
        List<DmoOperacionesEntity> tmct0DmoOperacionesList = new ArrayList<DmoOperacionesEntity>();
        for (Object[] resultClientList : internationalClientsList) {
          for (Object[] resultOperationList : operationsInternationalList) {
            if (resultClientList[0].toString().equalsIgnoreCase(resultOperationList[8].toString())) {
              tmct0DmoOperacionesList.add(setDmoOperacionesEntity(resultClientList, resultOperationList));
            }
          }
        }
        this.save(tmct0DmoOperacionesList);
        existInterationalData = true;
      }
    }
    return existInterationalData;
  }

  /**
   * Genera el filtro de alias necesario para obtener las operaciones de
   * nacional
   * @param nationalClientList
   * @return
   */
  private List<String> getAliasFilterList(List<Object[]> nationalClientList) {
    List<String> cdAliasFilterList = new ArrayList<>();
    for (Object[] resultList : nationalClientList) {
      cdAliasFilterList.add(resultList[0].toString().trim());
    }
    return cdAliasFilterList;
  }

}
