package sibbac.business.dmo.bo.common;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.dmo.db.dao.DmoOperacionesDAO;
import sibbac.business.dmo.db.model.DmoOperacionesEntity;
import sibbac.business.dmo.web.util.Constantes;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;
import sibbac.webapp.security.database.model.Tmct0Users;

@Service
public class DmoCommonBo extends AbstractBo<DmoOperacionesEntity, Integer, DmoOperacionesDAO> {

  @Autowired
  private Tmct0UsersBo usersBo;

  @Autowired
  private SendMail sendMail;
  
  @PersistenceContext
  private EntityManager em;

  /**
   * Convierte una fecha pasada como String a tipo Date
   * @param fechaString
   * @return
   * @throws ParseException
   */
  public Date stringToDate(final String stringDate) throws ParseException {
    final DateFormat format = new SimpleDateFormat(Constantes.Fecha.FORMATO.value());
    Date dDate = null;
    if (StringUtils.isNotEmpty(stringDate)) {
      dDate = new Date(format.parse(stringDate).getTime());
    }
    return dDate;
  }

  /**
   * Obtiene los minutos en ejecución con el fin de comprobar si se superan los
   * 4 mins establecidos para dvolver el control al usuario
   * @param startDate
   * @param endDate
   * @return
   */
  public long getExecutedMinutes(Timestamp startDate, Timestamp endDate) {
    long milliseconds1 = startDate.getTime();
    long milliseconds2 = endDate.getTime();
    long diff = milliseconds2 - milliseconds1;
    long diffMinutes = diff / (60 * 1000);
    return diffMinutes;
  }

  /**
   * Envía un correo al usuario que ha inciado el proceso una vez ha finalizado
   * (Si el proceso ha superado los 4 mins inciales de ejecución)
   * @param auditUser
   */
  public void sendOverTimeMail(String auditUser) {
    String correousuario = null;
    Tmct0Users user = usersBo.getByUsername(auditUser);
    if (user != null && user.getEmail() != null && !user.getEmail().trim().isEmpty()) {
      correousuario = user.getEmail().trim();

      try {
        sendMail.sendMail(correousuario, "Recopilación de datos para DMO.",
            "La recopilación de datos para DMO ha finalizado correctamente.");
      }
      catch (IllegalArgumentException | MessagingException | IOException e) {
        LOG.error(Constantes.ApplicationMessages.ERROR_MAIL_OVERTIME.value(), e);
      }

    }
  }

}
