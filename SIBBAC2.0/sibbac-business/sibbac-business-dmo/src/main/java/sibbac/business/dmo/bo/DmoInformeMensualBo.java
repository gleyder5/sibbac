package sibbac.business.dmo.bo;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.dmo.bo.common.DmoCommonBo;
import sibbac.business.dmo.db.dao.DmoOperacionesDAO;
import sibbac.business.dmo.db.model.DmoOperacionesEntity;
import sibbac.business.dmo.web.dto.PeticionDTO;
import sibbac.business.dmo.web.util.Constantes;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;

@Service
public class DmoInformeMensualBo extends AbstractBo<DmoOperacionesEntity, Integer, DmoOperacionesDAO> {

  @Value("${generacioninformes.ruta.informes:/sbrmv/ficheros/informes/}")
  private String rutaInformes;

  @Value("${generacioninformes.url.descarga.informe:EXTRANJERO.csv}")
  private String urlInforme;

  @Value("${generacioninformes.informe.mensual.extranjero:http://wasphxpvlng01.nngg.corp:8080/sbrmv/ficheros/informes/}")
  private String monthlyExtranjeroFile;

  @Autowired
  private SendMail sendMail;

  @Autowired
  private DmoCommonBo boCommon;

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private DmoOperacionesDAO dao;

  private String currentProcess = "";

  /**
   * Envío del fichero csv por mail
   * @param request
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  public CallableResultDTO sendMonthlyReport(PeticionDTO request, String auditUser) throws SIBBACBusinessException {
    CallableResultDTO result = new CallableResultDTO();
    currentProcess = "Envío de informe extranjero.csv";
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_ENTER.value() + currentProcess);
    }
    List<Future<CallableResultDTO>> releasedProcessList = new ArrayList<Future<CallableResultDTO>>();
    Timestamp startDate = new Timestamp(System.currentTimeMillis());
    releaseSendExtranjeroProcess(request, releasedProcessList);
    for (Future<CallableResultDTO> process : releasedProcessList) {
      try {
        result.append(process.get(10, TimeUnit.MINUTES));
        Timestamp endDate = new Timestamp(System.currentTimeMillis());
        long diffMinutes = boCommon.getExecutedMinutes(startDate, endDate);
        if (diffMinutes > 4) {
          boCommon.sendOverTimeMail(auditUser);
        }
      }
      catch (InterruptedException | ExecutionException e) {
        LOG.error(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value(), e);
        result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
      }
      catch (TimeoutException e) {
        LOG.info(Constantes.ApplicationMessages.ERROR_TIMEOUT.value(), e);
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_TIMEOUT.value());
      }
    }
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_EXECUTION_OK.value() + currentProcess);
    }
    return result;
  }

  /**
   * Proceso que activa el hilo de ejecución
   * @param request
   * @param releasedProcessList
   * @throws SIBBACBusinessException
   */
  private void releaseSendExtranjeroProcess(PeticionDTO request, List<Future<CallableResultDTO>> releasedProcessList)
      throws SIBBACBusinessException {
    currentProcess = "Proceso de lanzamiento de hilos de para el envío del fichero de extranjero";
    ExecutorService executor;
    Callable<CallableResultDTO> callableGenerate;
    Future<CallableResultDTO> futureProcess;
    executor = Executors.newSingleThreadExecutor();
    try {
      callableGenerate = setNewCallableSendExtranjero(request);
      futureProcess = executor.submit(callableGenerate);
      releasedProcessList.add(futureProcess);
    }
    catch (Exception e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_THREAD_RELEASE.value(), executor, e);
      executor.shutdownNow();
    }
    finally {
      if (!executor.isShutdown()) {
        if (LOG.isDebugEnabled()) {
          LOG.debug(Constantes.LoggingMessages.DEBUG_THREAD_SHUTDOWN.value(), executor);
        }
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        if (LOG.isDebugEnabled()) {
          LOG.debug(Constantes.LoggingMessages.DEBUG_THREAD_RUNING.value(), executor);
        }
        try {
          executor.awaitTermination(10, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.error(Constantes.ApplicationMessages.ERROR_THREAD_RELEASE.value(), e);
        }
      }
    }
    if (LOG.isDebugEnabled()) {
      LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_EXECUTION_OK.value() + currentProcess);
    }
  }

  /**
   * Generación y envío de fichero de extranjero
   * @param request
   * @return
   */
  private Callable<CallableResultDTO> setNewCallableSendExtranjero(final PeticionDTO request) {
    Callable<CallableResultDTO> c = new Callable<CallableResultDTO>() {
      @Override
      public CallableResultDTO call() throws Exception {
        CallableResultDTO res = null;
        currentProcess = "Generación y envío de fichero de extranjero";
        if (LOG.isDebugEnabled()) {
          LOG.debug(Constantes.LoggingMessages.DEBUG_MESSAGE_ENTER.value() + currentProcess);
        }
        try {
          res = sendExtranjeroReport(request);
          LOG.trace(Constantes.ApplicationMessages.EXECUTION_OK.value() + currentProcess);
        }
        catch (Exception e) {
          res = new CallableResultDTO();
          LOG.error(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value(), e);
          res.addErrorMessage(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value());
          throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value());
        }
        return res;
      }
    };
    return c;
  }

  private CallableResultDTO sendExtranjeroReport(PeticionDTO solicitud) throws SIBBACBusinessException {
    CallableResultDTO result = new CallableResultDTO();
    try {
      Date dateFrom = boCommon.stringToDate((String) solicitud.getParams().get("fechaDesde"));
      Date dateTo = boCommon.stringToDate((String) solicitud.getParams().get("fechaHasta"));
      String accruedAmountString = dao.findAccruedAmount(Constantes.Tmct0cfg.APPLICATION.value(),
          Constantes.Tmct0cfg.PROCESS.value(), Constantes.Tmct0cfg.KEYNAME_ACCRUED_AMOUNT.value());
      int accruedAmount = Integer.parseInt(accruedAmountString);
      if (dateFrom != null) {
        if (dateTo != null) {
          if (accruedAmount >= 0) {
            List<Object[]> foreignDataList = new ArrayList<Object[]>();
            foreignDataList = dao.findDmoDataReport(Constantes.Activo.ACTIVO.value(), dateFrom, dateTo, accruedAmount);
            if (foreignDataList != null && !foreignDataList.isEmpty() && foreignDataList.size() != 0) {
              // Generación y envío de CSV de acuerdo con la parametrización de
              // BBDD
              csvFileSend(foreignDataList);
              result.addMessage(Constantes.ApplicationMessages.EXTRANJERO_REPORT_OK.value());
              LOG.info(Constantes.ApplicationMessages.EXTRANJERO_REPORT_OK.value());
            }
            else {
              result.addMessage(Constantes.ApplicationMessages.EXTRANJERO_REPORT_KO.value());
              LOG.info(Constantes.ApplicationMessages.EXTRANJERO_REPORT_KO.value());
            }
          }
          else {
            LOG.error(Constantes.ApplicationMessages.ERROR_ACCRUED_AMOUNT.value());
            result.addErrorMessage(Constantes.ApplicationMessages.ERROR_ACCRUED_AMOUNT.value());
          }
        }
        else {
          LOG.error(Constantes.ApplicationMessages.ERROR_END_DATE.value());
          result.addErrorMessage(Constantes.ApplicationMessages.ERROR_END_DATE.value());
        }
      }
      else {
        LOG.error(Constantes.ApplicationMessages.ERROR_START_DATE.value());
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_START_DATE.value());
      }
    }
    catch (ParseException e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_INPUT_DATA.value());
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_INPUT_DATA.value());
    }
    return result;
  }

  private void csvFileSend(List<Object[]> foreignDataList) throws SIBBACBusinessException {
    try {
      File directory = new File(rutaInformes);
      if (!directory.exists()) {
        directory.mkdirs();
        if (LOG.isDebugEnabled()) {
          LOG.trace(Constantes.ApplicationMessages.DIRECTORY_CREATION_OK.value());
        }
      }
      File file = new File(rutaInformes, FilenameUtils.getName(monthlyExtranjeroFile));
      Writer writer;
      try {
        writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        PrintWriter pw = new PrintWriter(writer);
        StringBuilder sb = new StringBuilder();
        // Generamos el csv
        // Cabeceras
        sb.append(getDmoMonthlyReportHeaders());
        sb.append('\n');
        // Cuerpo CSV
        sb.append(generateCsvBody(foreignDataList));
        pw.write(sb.toString());
        pw.close();
      }
      catch (UnsupportedEncodingException e) {
        LOG.error(Constantes.ApplicationMessages.ERROR_GENERATION_EXTRANJERO_FILE.value(), e);
      }
      catch (FileNotFoundException e) {
        LOG.error(Constantes.ApplicationMessages.ERROR_GENERATION_EXTRANJERO_FILE.value(), e);
      }
      // Enviamos el correo
      List<Object[]> partenonList = new ArrayList<Object[]>();
      partenonList = dao.findPartenonMailData(Constantes.Tmct0cfg.APPLICATION.value(),
          Constantes.Tmct0cfg.PROCESS.value(), Constantes.Tmct0cfg.KEYNAME_EMAIL_PARAMS.value());
      if (partenonList != null && !partenonList.isEmpty() && partenonList.size() != 0) {
        String keyname = "";
        String subject = "";
        String body = "";
        List<String> emailsTo = new ArrayList<>();
        // Adjuntos
        List<InputStream> listAttachmentAlias = new ArrayList<InputStream>();
        for (Object[] resultPartenonList : partenonList) {
          keyname = resultPartenonList[0].toString().toUpperCase().trim();
          if (keyname.indexOf("SUBJECT") != -1) {
            subject += resultPartenonList[1].toString();
          }
          if (keyname.indexOf("BODY") != -1) {
            body += resultPartenonList[1].toString();
          }
          if (keyname.indexOf("TO") != -1) {
            emailsTo.add(resultPartenonList[1].toString());
          }
        }
        // Añadimos (en caso de que no existan) los datos a completar
        List<String> emptyDmoDataList = checkDmoEmptyData(foreignDataList);
        if (emptyDmoDataList.size() > 0) {
          body = addDataMustBeFilled(emptyDmoDataList, body);
        }
        // Adjuntos
        InputStream inputStreamFileAlias;
        try {
          inputStreamFileAlias = new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
          listAttachmentAlias.add(inputStreamFileAlias);
          List<String> listaNameDest = new ArrayList<String>();
          listaNameDest.add(monthlyExtranjeroFile);
          try {
            sendMail.sendMail(emailsTo, subject, body, listAttachmentAlias, listaNameDest, null);
            file.delete();
          }
          catch (MessagingException e) {
            LOG.error(Constantes.ApplicationMessages.ERROR_MAIL_EXTRANJERO_FILE.value(), e);
          }
        }
        catch (IOException e) {
          LOG.error(Constantes.ApplicationMessages.ERROR_GENERATION_EXTRANJERO_FILE.value(), e);
        }
      }

    }
    catch (RuntimeException e) {
      LOG.error(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value(), e);
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.ERROR_SENDING_EXTRANJERO_REPORT.value(), e);
    }
  }

  /**
   * Forma las cabeceras del fichero extranjero.csv
   * @return
   */
  private String getDmoMonthlyReportHeaders() {
    return "TPOPERAC  ;FHOPERAC  ;IMOPERAC  ;NBMONEDA  ;CDSUCURS  ;NBPAISDE  ;NBINEPRO;  NBINEMUN;  NBDENSOC;  NUDNICIF;  NBPAISNA;  NBDOMICI;  CDALIASS;  CDBROCLI";
  }

  /**
   * Forma el cuerpo del fichero csv
   * @param foreignDataList
   * @return
   */
  private String generateCsvBody(List<Object[]> foreignDataList) {
    StringBuilder sb = new StringBuilder();
    String operationDateTimestamp = "";
    for (Object[] resultforeignDataList : foreignDataList) {
      // TIPO_OPERACION
      sb.append(String.valueOf(resultforeignDataList[1].toString()));
      sb.append(';');
      // FECHA_OPERACION
      if (!String.valueOf(resultforeignDataList[2].toString()).isEmpty()) {
        operationDateTimestamp = String.valueOf(resultforeignDataList[2].toString().substring(0, 10));
        String[] parts = operationDateTimestamp.split("-");
        String yearString = parts[0]; // year
        String monthString = parts[1]; // month
        String dayString = parts[2]; // day
        String operationDate = dayString + "/" + monthString + "/" + yearString;
        sb.append(operationDate);
        sb.append(';');
      }
      else {
        sb.append("");
        sb.append(';');
      }
      // IMPORTE_EFECTIVO
      sb.append(String.valueOf(resultforeignDataList[3].toString()));
      sb.append(';');
      // MONEDA
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBMONEDA_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[4].toString())));
      sb.append(';');
      // CDCLEARE
      sb.append(String.valueOf(resultforeignDataList[5].toString()));
      sb.append(';');
      // PAIS_TITULAR
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBPAISDE_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[6].toString())));
      sb.append(';');
      // PROVINCIA_TITULAR
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBINEPRO_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[7].toString())));
      sb.append(';');
      // MUNICIPIO_TITULAR
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBINEMUN_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[8].toString())));
      sb.append(';');
      // ALIAS_CLIENTE
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBDENSOC_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[9].toString())));
      sb.append(';');
      // CIF
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NUDNICIF_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[10].toString())));
      sb.append(';');
      // PAIS_RESIDENCIA
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBPAISNA_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[11].toString())));
      sb.append(';');
      // DOMICILIO
      sb.append(String.format(Constantes.FormatoRegistrosFicheroExtranjero.NBDOMICI_FORMAT_LENGTH.value(),
          String.valueOf(resultforeignDataList[12].toString())));
      sb.append(';');
      // CODIGO_ALIAS_CLIENTE
      sb.append(String.valueOf(resultforeignDataList[13].toString()));
      sb.append(';');
      // ID_CLIENTE
      sb.append(String.valueOf(resultforeignDataList[0].toString()));
      sb.append('\n');
    }
    return sb.toString();
  }

  /**
   * Informa de los registros cuyos clientes no tienen informados el paisTitular
   * o el domicilio en la tabla dmo operaciones
   * @param foreignDataList
   * @return
   */
  private List<String> checkDmoEmptyData(List<Object[]> foreignDataList) {
    List<String> resultList = new ArrayList<>();
    // NBPAISDE = paisTitular --> pos 6
    String paisTitular = "";
    // NBDOMICI = domicilio --> pos 12
    String domicilio = "";
    for (Object[] resultforeignDataList : foreignDataList) {
      paisTitular = String.valueOf(resultforeignDataList[6].toString());
      domicilio = String.valueOf(resultforeignDataList[12].toString());
      if (paisTitular.isEmpty() || domicilio.isEmpty()) {
        // ID_CLIENTE --> pos 0
        // IMPORTE --> pos 3
        resultList.add(String.valueOf(resultforeignDataList[0].toString()) + ","
            + String.valueOf(resultforeignDataList[3].toString()));
      }
    }
    return resultList;
  }

  private String addDataMustBeFilled(List<String> emptyDmoDataList, String body) {
    body += System.getProperty("line.separator") + System.getProperty("line.separator");
    body += Constantes.ApplicationMessages.USER_FILL_EMPTY_DATA_MESSAGE.value();
    body += System.getProperty("line.separator");
    if (emptyDmoDataList.size() > 0) {
      for (String resultemptyDmoDataList : emptyDmoDataList) {
        String[] parts = resultemptyDmoDataList.toString().split(",");
        String part1 = parts[0].trim(); // idCliente
        String part2 = parts[1].trim(); // importe
        body += "\t - Cliente: " + part1 + ", Importe Efectivo = " + part2 + System.getProperty("line.separator");
      }
    }
    return body;
  }

}
