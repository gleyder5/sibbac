package sibbac.business.dmo.db.dao;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dmo.db.model.DmoOperacionesEntity;

public interface DmoOperacionesDAO extends JpaRepository<DmoOperacionesEntity, Integer> {
  /**
   * @author enieto
   *
   */
  static final String NATIONAL_CLIENTS_QUERY = 
      " SELECT " + 
      "  B.CDALIAS CDALIASS, C.CIF NUDNICIF, P.NBPAIS NBPAISNA, A.DESCRALI AS NBDENSOC, " + 
      "  D.TPDOMICI || '/' || RTRIM(D.NBDOMICI) || ', ' || D.NUDOMICI AS NBDOMICI, " + 
      "  A.CDBROCLI AS CDBROCLI " + 
      " FROM " + 
      "  TMCT0BOK B " + 
      "  INNER JOIN TMCT0ORD O ON B.NUORDEN = O.NUORDEN " + 
      "  INNER JOIN TMCT0DESGLOSECAMARA DC ON B.NUORDEN = DC.NUORDEN AND B.NBOOKING = DC.NBOOKING " + 
      "  INNER JOIN TMCT0ALI A ON B.CDALIAS = A.CDALIASS AND A.FHFINAL >= TO_CHAR(CURRENT DATE,'YYYYMMDD') " + 
      "  INNER JOIN TMCT0CLI C ON A.CDBROCLI = C.CDBROCLI " + 
      "  LEFT JOIN TMCT0_ADDRESSES D ON C.IDCLIENT = D.IDCLIENT " + 
      "  INNER JOIN TMCT0PAISES P ON D.CDDEPAIS = P.CDHACIENDA AND P.CDESTADO = :cdEstado AND P.INDICADOR_DMO = :indicadorDmo " + 
      " WHERE " + 
      "  B.FETRADE >= :fechaDesde AND B.FETRADE <= :fechaHasta " + 
      "  AND B.FETRADE >= P.FECHA_INICIO_PAR_FISC " + 
      "  AND B.FETRADE <= P.FECHA_FIN_PAR_FISC AND B.CDESTADOASIG >= :cdEstadoAsig65 AND DC.CDESTADOASIG >= :cdEstadoAsig65 " + 
      "  AND O.CDCLSMDO = :cdclsmdoN " + 
      " GROUP " + 
      "  BY B.CDALIAS, C.CIF, A.DESCRALI, P.NBPAIS, A.CDBROCLI, D.TPDOMICI, D.NBDOMICI, D.NUDOMICI ";

  static final String NATIONAL_OPERATIONS_QUERY = "SELECT DECODE(B.CDTPOPER,'C',17,'V',18) TPOPERAC, "
      + "B.FETRADE FHOPERAC, ROUND(A.IMEFEAGR, 0) IMOPERAC, (SELECT KEYVALUE FROM TMCT0CFG "
      + "WHERE APPLICATION = :application AND PROCESS = :process AND KEYNAME = :nbmoneda) NBMONEDA, "
      + "(SELECT KEYVALUE FROM TMCT0CFG WHERE APPLICATION = :application AND PROCESS = :process AND KEYNAME = :cdsucurs) CDSUCURS, "
      + "M.NBPAISME NBPAISDE, '' NBINEPRO, '' NBINEMUN, B.CDALIAS CDALIASS FROM TMCT0BOK B "
      + "INNER JOIN TMCT0ORD O ON B.NUORDEN = O.NUORDEN "
      + "INNER JOIN TMCT0ALC A ON B.NUORDEN = A.NUORDEN AND B.NBOOKING = A.NBOOKING "
      + "LEFT JOIN TMCT0_MERCADO M ON O.CDMERCAD = M.CODIGO WHERE B.FETRADE BETWEEN :fechaDesde AND :fechaHasta "
      + "AND B.CDESTADOASIG >= :cdEstadoAsig65 AND A.CDESTADOASIG >= :cdEstadoAsig65 AND O.CDCLSMDO = :cdclsmdoN "
      + "AND B.CDALIAS IN (:cdAlias)";

  static final String INTERNATIONAL_CLIENTS_QUERY = 
      " SELECT " + 
      "  B.CDALIAS CDALIASS, C.CIF NUDNICIF, P.NBPAIS NBPAISNA, C.DESCLI AS NBDENSOC, " + 
      "  D.TPDOMICI  || '/'  || RTRIM(D.NBDOMICI) || ', '  || D.NUDOMICI AS NBDOMICI, " + 
      "  ALI.CDBROCLI AS CDBROCLI " + 
      " FROM " + 
      "  TMCT0BOK B " + 
      "  INNER JOIN TMCT0ORD O ON B.NUORDEN = O.NUORDEN " + 
      "  INNER JOIN TMCT0ALO A ON B.NUORDEN = A.NUORDEN AND B.NBOOKING = A.NBOOKING " + 
      "  INNER JOIN TMCT0ALI ALI ON B.CDALIAS = ALI.CDALIASS AND ALI.FHFINAL >= TO_CHAR(CURRENT DATE,'YYYYMMDD') " + 
      "  INNER JOIN TMCT0CLI C ON ALI.CDBROCLI = C.CDBROCLI " + 
      "  LEFT JOIN TMCT0_ADDRESSES D ON D.IDCLIENT = C.IDCLIENT " + 
      "  INNER JOIN TMCT0PAISES P ON D.CDDEPAIS = P.CDHACIENDA AND P.INDICADOR_DMO = :indicadorDmo AND P.CDESTADO = :cdEstado " + 
      " WHERE " + 
      "  B.FETRADE >= :fechaDesde AND B.FETRADE <= :fechaHasta " + 
      "  AND B.FETRADE >= P.FECHA_INICIO_PAR_FISC AND B.FETRADE <= P.FECHA_FIN_PAR_FISC " + 
      "  AND B.CDESTADO NOT IN ('002', '003') AND O.CDCLSMDO = :cdclsmdoE " + 
      " GROUP " + 
      "  BY B.CDALIAS, C.DESCLI, C.CIF, P.NBPAIS, ALI.CDBROCLI, D.TPDOMICI, D.NBDOMICI, D.NUDOMICI";

  static final String INTERNATIONAL_OPERATIONS_QUERY = "SELECT  DECODE(B.CDTPOPER,'C',17,'V',18) TPOPERAC, "
      + "B.FETRADE FHOPERAC, ROUND(A.IMBRMREU, 0) IMOPERAC, (SELECT KEYVALUE FROM TMCT0CFG "
      + "WHERE APPLICATION = :application AND PROCESS = :process AND KEYNAME = :nbmoneda) NBMONEDA, "
      + "(SELECT KEYVALUE FROM TMCT0CFG WHERE APPLICATION = :application AND PROCESS = :process AND KEYNAME = :cdsucurs) CDSUCURS, "
      + "M.NBPAISME NBPAISDE, '' NBINEPRO, '' NBINEMUN, B.CDALIAS CDALIASS FROM TMCT0BOK B "
      + "INNER JOIN TMCT0ORD O ON B.NUORDEN = O.NUORDEN "
      + "INNER JOIN TMCT0ALO A ON B.NUORDEN = A.NUORDEN AND B.NBOOKING = A.NBOOKING "
      + "LEFT JOIN TMCT0_MERCADO M ON O.CDMERCAD = M.CODIGO "
      + "WHERE B.FETRADE BETWEEN :fechaDesde AND :fechaHasta AND A.CDESTADO NOT IN ('002', '003') "
      + "AND B.CDALIAS IN (:cdAlias) AND O.CDCLSMDO = :cdclsmdoE";

  static final String SET_DATA_TO_INACTIVE_QUERY = "UPDATE TMCT0_DMO_OPERACIONES SET ACTIVO = :inactivo, fecha_baja = :fechaBaja, usuario_baja = :usuarioBaja "
      + "WHERE FECHA_OPERACION >= :fechaDesde AND FECHA_OPERACION <= :fechaHasta";

  static final String DMO_DATA_REPORT_QUERY = "SELECT D2.ID_CLIENTE, D2.TIPO_OPERACION, D2.FECHA_OPERACION, D2.IMPORTE_EFECTIVO, D2.MONEDA,"
      + " D2.CDCLEARE, RTRIM(D2.PAIS_TITULAR), RTRIM(D2.PROVINCIA_TITULAR), RTRIM(D2.MUNICIPIO_TITULAR), RTRIM(D2.ALIAS_CLIENTE)," 
      + " RTRIM(D2.CIF), RTRIM(D2.PAIS_RESIDENCIA), RTRIM(D2.DOMICILIO), RTRIM(D2.CODIGO_ALIAS_CLIENTE)"
      + " FROM ("
      + " SELECT D1.CODIGO_ALIAS_CLIENTE FROM  bsnbpsql.TMCT0_DMO_OPERACIONES D1" 
      + " WHERE D1.FECHA_OPERACION >= :fechaDesde AND D1.FECHA_OPERACION <= :fechaHasta AND D1.ACTIVO = :activo" 
      + " GROUP BY D1.CODIGO_ALIAS_CLIENTE"
      + " HAVING SUM(D1.IMPORTE_EFECTIVO) >= :importeAcumulado"
      + " ) AS CAC INNER JOIN  bsnbpsql.TMCT0_DMO_OPERACIONES D2 ON CAC.CODIGO_ALIAS_CLIENTE = D2.CODIGO_ALIAS_CLIENTE"
      + " WHERE FECHA_OPERACION >= :fechaDesde AND FECHA_OPERACION <= :fechaHasta AND ACTIVO = :activo"
      + " ORDER BY D2.codigo_alias_cliente, D2.fecha_operacion, D2.tipo_operacion";
  
  static final String DMO_MAIL_DATA_QUERY = "SELECT KEYNAME, KEYVALUE FROM TMCT0CFG WHERE APPLICATION = :application AND PROCESS = :process AND KEYNAME LIKE :emailParams";

  static final String DMO_ACCRUED_AMOUNT_QUERY = "SELECT KEYVALUE FROM TMCT0CFG WHERE APPLICATION = :application AND PROCESS = :process AND KEYNAME = :importeAcumulado";

  /**
   * Obtiene los registros asociados a los datos de Clientes de nacional
   * @param cdEstado
   * @param indicadorDmo
   * @param fechaDesde
   * @param fechaHasta
   * @param cdEstadoAsig65
   * @param cdclsmdoN
   * @return
   */
  @Query(value = NATIONAL_CLIENTS_QUERY, nativeQuery = true)
  List<Object[]> findNationalClientsData(@Param("cdEstado") String cdEstado, @Param("indicadorDmo") int indicadorDmo,
      @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta,
      @Param("cdEstadoAsig65") int cdEstadoAsig65, @Param("cdclsmdoN") String cdclsmdoN);

  /**
   * Obtiene los registros asociados a los datos de Operaciones de nacional
   * @param application
   * @param process
   * @param nbmoneda
   * @param cdsucurs
   * @param fechaDesde
   * @param fechaHasta
   * @param cdEstadoAsig65
   * @param cdclsmdoN
   * @param cdalias
   * @return
   */
  @Query(value = NATIONAL_OPERATIONS_QUERY, nativeQuery = true)
  List<Object[]> findNationalOperationsData(@Param("application") String application, @Param("process") String process,
      @Param("nbmoneda") String nbmoneda, @Param("cdsucurs") String cdsucurs, @Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta, @Param("cdEstadoAsig65") int cdEstadoAsig65,
      @Param("cdclsmdoN") String cdclsmdoN, @Param("cdAlias") List<String> cdAlias);

  /**
   * Obtiene los registros asociados a los datos de Clientes de internacional
   * @param indicadorDmo
   * @param cdEstado
   * @param fechaDesde
   * @param fechaHasta
   * @param cdclsmdoE
   * @return
   */
  @Query(value = INTERNATIONAL_CLIENTS_QUERY, nativeQuery = true)
  List<Object[]> findInternationalClientsData(@Param("indicadorDmo") int indicadorDmo,
      @Param("cdEstado") String cdEstado, @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta,
      @Param("cdclsmdoE") String cdclsmdoE);

  /**
   * Obtiene los registros asociados a los datos de Operaciones de internacional
   * @param application
   * @param process
   * @param nbmoneda
   * @param cdsucurs
   * @param fechaDesde
   * @param fechaHasta
   * @param cdalias
   * @param cdclsmdoE
   * @return
   */
  @Query(value = INTERNATIONAL_OPERATIONS_QUERY, nativeQuery = true)
  List<Object[]> findInternationalOperationsData(@Param("application") String application,
      @Param("process") String process, @Param("nbmoneda") String nbmoneda, @Param("cdsucurs") String cdsucurs,
      @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta,
      @Param("cdAlias") List<String> cdAlias, @Param("cdclsmdoE") String cdclsmdoE);

  /**
   * Realiza el proceso de baja lógica de los registros dmo activos
   * @param inactivo
   * @param fechaBaja
   * @param usuarioBaja
   * @param fechaDesde
   * @param fechaHasta
   */
  @Transactional
  @Modifying(clearAutomatically = true)
  @Query(value = SET_DATA_TO_INACTIVE_QUERY, nativeQuery = true)
  void setDataToInactive(@Param("inactivo") int inactivo, @Param("fechaBaja") Timestamp fechaBaja,
      @Param("usuarioBaja") String usuarioBaja, @Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta);

  /**
   * De acuerdo con el período de fechas correspondiente proporciona los
   * registros a insertar en el fichero de Extranjero.csv
   * @param activo
   * @param fechaDesde
   * @param fechaHasta
   * @param importeAcumulado
   * @return
   */
  @Query(value = DMO_DATA_REPORT_QUERY, nativeQuery = true)
  List<Object[]> findDmoDataReport(@Param("activo") int activo, @Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta, @Param("importeAcumulado") int importeAcumulado);

  /**
   * Recupera de base de datos los registros que se utilizarán para el envío del
   * correo
   * @param application
   * @param process
   * @param emailParams
   * @return
   */
  @Query(value = DMO_MAIL_DATA_QUERY, nativeQuery = true)
  List<Object[]> findPartenonMailData(@Param("application") String application, @Param("process") String process,
      @Param("emailParams") String emailParams);

  /**
   * Recupera de BBDD el valor parametrizado del importe acumulado
   * @param application
   * @param process
   * @param importeAcumulado
   * @return
   */
  @Query(value = DMO_ACCRUED_AMOUNT_QUERY, nativeQuery = true)
  String findAccruedAmount(@Param("application") String application, @Param("process") String process,
      @Param("importeAcumulado") String importeAcumulado);
}
