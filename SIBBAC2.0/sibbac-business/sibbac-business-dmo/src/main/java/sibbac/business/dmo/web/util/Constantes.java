package sibbac.business.dmo.web.util;

public interface Constantes<T> {

  public enum LoggingMessages implements Constantes<String> {

    /** */
    DEBUG_MESSAGE_ENTER("Entrada: Inicio del proceso: "),

    /** */
    DEBUG_MESSAGE_EXECUTION_OK("El proceso se ha ejecutado correctamente: "),

    /** */
    DEBUG_MESSAGE_EXIT("Salida del proceso: "),

    /** */
    DEBUG_THREAD_SHUTDOWN("Fin proceso (thread), se detiene el ejecutor. {}"),

    /** */
    DEBUG_THREAD_RUNING("Existen procesos en ejecución, los esperamos. {}"),

    /** */
    DEBUG_TASK_START("Inicio tarea programada: "),

    /** */
    DEBUG_TASK_NATIONAL_DATA("Recopilando datos de clientes y operaciones de carácter nacional"),

    /** */
    DEBUG_TASK_INTERNATIONAL_DATA("Recopilando datos de clientes y operaciones de carácter internacional"),

    /** */
    OBJECT_NULL("El objeto recibido viene nulo");

    private String value;

    private LoggingMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum ApplicationMessages implements Constantes<String> {

    /** Errores / alertas */
    UNEXPECTED_MESSAGE("Error inesperado consulte con administrador"),

    ERROR_START_DATE("Error, formato incorrecto en fecha de inicio"),

    ERROR_END_DATE("Error, formato incorrecto en fecha de fin"),

    ERROR_INPUT_DATA("Error, datos introducidos incorrectos"),

    ERROR_DATE("Error, la Fecha de Inicio no puede ser superior a la Fecha de Fin"),

    ERROR_TIMEOUT("El proceso ha fallado por timeout"),

    ERROR_THREAD_RELEASE("Error, ha ocurrido una excepcion lanzando los hilos de ejecución. {}"),

    ERROR_LOADING_DMO_DATA("Error al generar los datos asociados a DMO"),

    ERROR_MAIL_OVERTIME(
        "Error, en el envío de correo que indica la finalización del proceso que ha tardado más de 4 minutos"),

    ERROR_SENDING_EXTRANJERO_REPORT("Error en el proceso de generación y envío de correo del fichero Extranjero.csv"),

    EXTRANJERO_REPORT_KO("No existen datos para el período seleccionado"),

    ERROR_ACCRUED_AMOUNT("Importe acumulado incorrecto"),

    DMO_DATA_COLLECTION_KO("No existen operaciones para el período seleccionado"),

    ERROR_GENERATION_EXTRANJERO_FILE("Error, el fichero de Extranjero.csv no se ha creado ó llenado correctamente"),

    ERROR_MAIL_EXTRANJERO_FILE("Error, el correo con el Extranjero.csv no se ha enviado"),

    ERROR_DISABLING_DMO_DATA("Error en la desactivación de datos DMO"),

    ERROR_INSERT_DMO("Error en la insercción de datos DMO"),

    /** OK / INFO */

    UNSUBSCRIBE_LOGIC_PROCESS_OK("Proceso de baja lógica realizado correctamente"),

    DMO_DATA_COLLECTION_OK("Recopilación de operaciones DMO realizado correctamente"),

    DMO_NATIONAL_DATA_COLLECTION_OK(
        "Recopilación de operaciones DMO (nacional) realizado correctamente. No existen operaciones de carácter internacional"),

    DMO_INTERNATIONAL_DATA_COLLECTION_OK(
        "Recopilación de operaciones DMO (internacional) realizado correctamente. No existen operaciones de carácter nacional"),

    EXTRANJERO_REPORT_OK("La petición de generación y envio de informe mensual se ha realizado correctamente"),

    EXECUTION_OK("El proceso se ha ejecutado correctamente: "),

    DIRECTORY_CREATION_OK("Se ha creado la ruta de informes en el directorio ya que no existía"),

    USER_FILL_EMPTY_DATA_MESSAGE(
        "No se han informado datos relacionados con el país del titular o con el domicilio para: "),

    ADDED_DATA("Registro insertado correctamente"),

    OBJECT_NULL("El objeto recibido viene nulo");

    private String value;

    private ApplicationMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return value;
    }
  }

  public enum Activo implements Constantes<Integer> {

    /** */
    ACTIVO(1),

    /** */
    INACTIVO(0);

    private Integer value;

    private Activo(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return this.value;
    }
  }

  public enum Session implements Constantes<String> {

    /** */
    USER_SESSION("UserSession"),

    /** */
    USER_CLIENTE("CARGA_OPERACIONES_DMO");

    private String value;

    private Session(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }

  }

  public enum Estado implements Constantes<String> {

    ASIGNADO("A"),

    CDCLSMDON("N"),

    CDCLSMDOE("E");

    private String value;

    private Estado(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum Indicador implements Constantes<Integer> {
    INDICADOR_DMO(1),

    CDESTADOASIG65(65);

    private Integer value;

    private Indicador(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return this.value;
    }
  }

  public enum Tmct0cfg implements Constantes<String> {

    APPLICATION("SBRMV"),

    PROCESS("DMO"),

    KEYNAME_NBMONEDA("dmo.query.nbmoneda"),

    KEYNAME_CDSUCURS("dmo.query.cdsucurs"),

    KEYNAME_ACCRUED_AMOUNT("dmo.importe.acumulado"),

    KEYNAME_EMAIL_PARAMS("dmo.email%");

    private String value;

    private Tmct0cfg(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum Fecha implements Constantes<String> {

    FORMATO("dd/MM/yyyy");

    private String value;

    private Fecha(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum FormatoRegistrosFicheroExtranjero implements Constantes<String> {
    NBMONEDA_FORMAT_LENGTH("%-32s"),
    
    NBPAISDE_FORMAT_LENGTH("%-40s"),
    
    NBINEPRO_FORMAT_LENGTH("%-25s"),
    
    NBINEMUN_FORMAT_LENGTH("%-50s"),
    
    NBDENSOC_FORMAT_LENGTH("%-60s"),
    
    NUDNICIF_FORMAT_LENGTH("%-15s"),
    
    NBPAISNA_FORMAT_LENGTH("%-40s"),
    
    NBDOMICI_FORMAT_LENGTH("%-50s");

    private String value;

    private FormatoRegistrosFicheroExtranjero(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }

  }

  public T value();
}
