package sibbac.business.dmo.web.util;

import java.sql.Timestamp;

import sibbac.business.dmo.db.model.DmoOperacionesEntity;
import sibbac.business.dmo.web.dto.DmoOperacionesDTO;


public abstract class ConversionDtoToEntity {
  /**
   * @author enieto
   *
   */

  public static void dtoToEntityDmoDataCollection(DmoOperacionesEntity entity, DmoOperacionesDTO<DmoOperacionesEntity> dto) {

    if (dto.getIdCliente() != null) {
      entity.setIdCliente(dto.getIdCliente());
    }
    
    entity.setTipoOperacion(dto.getTipoOperacion());
    
    if (dto.getFechaOperacion() != null) {
      entity.setFechaOperacion(new Timestamp(dto.getFechaOperacion().getTime()));
    }
    
    entity.setImporteEfectivo(dto.getImporteEfectivo());
    entity.setMoneda(dto.getMoneda());
    entity.setCdCleare(dto.getCdcleare());
    entity.setPaisTitular(dto.getPaisTitular());
    entity.setProvinciaTitular(dto.getProvinciaTitular());
    entity.setMunicipioTitular(dto.getMunicipioTitular());
    entity.setAliasCliente(dto.getAliasCliente());
    entity.setCif(dto.getCif());
    entity.setPaisResidencia(dto.getPaisResidencia());
    entity.setDomicilio(dto.getDomicilio());
    entity.setCodigoAliasCliente(dto.getCodigoAliasCliente());
    entity.setActivo(dto.getActivo());
    
    if (dto.getFechaAlta() != null) {
      entity.setFechaAlta(new Timestamp(dto.getFechaAlta().getTime()));
    }

    if (dto.getFechaMod() != null) {
      entity.setFechaMod(new Timestamp(dto.getFechaMod().getTime()));
    }
    
    if (dto.getFechaBaja() != null) {
      entity.setFechaBaja(new Timestamp(dto.getFechaBaja().getTime()));
    }

    if (dto.getUsuarioAlta() != null) {
      entity.setUsuarioAlta(dto.getUsuarioAlta());
    }

    if (dto.getUsuarioMod() != null) {
      entity.setUsuarioMod(dto.getUsuarioMod());
    }

    if (dto.getUsuarioBaja() != null) {
      entity.setUsuarioBaja(dto.getUsuarioBaja());
    }
    
    
   
  }
}
