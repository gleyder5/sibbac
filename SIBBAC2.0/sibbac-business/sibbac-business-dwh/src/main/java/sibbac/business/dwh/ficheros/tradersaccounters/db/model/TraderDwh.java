package sibbac.business.dwh.ficheros.tradersaccounters.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class that represents a
 * {@link sibbac.business.wrappers.database.model.CodigoTptiprep }. Entity:
 * "CodigoTptiprep".
 * 
 * @version 1.0
 * @since 1.0
 */
//
@Table(name = "TMCT0_DWH_TRADER")
@Entity
public class TraderDwh implements java.io.Serializable {

  private static final long serialVersionUID = -840277152853228871L;

  @Id
  @Column(name = "ID", nullable = false)
  private BigInteger idTrader;

  @Column(name = "PRODUCTO", nullable = false, length = 4)
  private String producto;

  @Column(name = "CUENTA_TRADER", nullable = false)
  private Integer cuentaTrader;

  @Column(name = "USUARIO_TRADER", nullable = false, length = 10)
  private String usuarioTrader;

  @Column(name = "NOMBRE_CORTO", nullable = false, length = 5)
  private String nombreCorto;

  @Column(name = "ACTIVO", nullable = false)
  private Integer activo;

  @Column(name = "USUARIO_ALTA", nullable = false, length = 256)
  private String usuarioAlta;

  @Column(name = "USUARIO_MOD", nullable = true, length = 256)
  private String usuarioMod;

  @Column(name = "USUARIO_BAJA", nullable = true, length = 256)
  private String usuariobaja;

  @Column(name = "FECHA_ALTA", nullable = false)
  private Timestamp fechaAlta;

  @Column(name = "FECHA_BAJA", nullable = false)
  private Timestamp fechaBaja;

  @Column(name = "FECHA_MOD", nullable = true)
  private Timestamp fechaMod;

  /**
   * @return the idTrader
   */
  public BigInteger getIdTrader() {
    return idTrader;
  }

  /**
   * @param idTrader the idTrader to set
   */
  public void setIdTrader(BigInteger idTrader) {
    this.idTrader = idTrader;
  }

  /**
   * @return the producto
   */
  public String getProducto() {
    return producto;
  }

  /**
   * @param producto the producto to set
   */
  public void setProducto(String producto) {
    this.producto = producto;
  }

  /**
   * @return the cuentaTrader
   */
  public Integer getCuentaTrader() {
    return cuentaTrader;
  }

  /**
   * @param cuentaTrader the cuentaTrader to set
   */
  public void setCuentaTrader(Integer cuentaTrader) {
    this.cuentaTrader = cuentaTrader;
  }

  /**
   * @return the usuarioTrader
   */
  public String getUsuarioTrader() {
    return usuarioTrader;
  }

  /**
   * @param usuarioTrader the usuarioTrader to set
   */
  public void setUsuarioTrader(String usuarioTrader) {
    this.usuarioTrader = usuarioTrader;
  }

  /**
   * @return the nombreCorto
   */
  public String getNombreCorto() {
    return nombreCorto;
  }

  /**
   * @param nombreCorto the nombreCorto to set
   */
  public void setNombreCorto(String nombreCorto) {
    this.nombreCorto = nombreCorto;
  }

  /**
   * @return the activo
   */
  public Integer getActivo() {
    return activo;
  }

  /**
   * @param activo the activo to set
   */
  public void setActivo(Integer activo) {
    this.activo = activo;
  }

  /**
   * @return the usuariobaja
   */
  public String getUsuariobaja() {
    return usuariobaja;
  }

  /**
   * @param usuariobaja the usuariobaja to set
   */
  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }

  /**
   * @return the usuarioAlta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * @param usuarioAlta the usuarioAlta to set
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * @return the usuarioMod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * @param usuarioMod the usuarioMod to set
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * @return the fechaAlta
   */
  public Timestamp getFechaAlta() {
    return (Timestamp) fechaAlta.clone();
  }

  /**
   * @param fechaAlta the fechaAlta to set
   */
  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (Timestamp) fechaAlta.clone();
  }

  /**
   * @return the fechaMod
   */
  public Timestamp getFechaMod() {
    return (Timestamp) fechaMod.clone();
  }

  /**
   * @param fechaMod the fechaMod to set
   */
  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (Timestamp) fechaMod.clone();
  }

  /**
   * @return the fechaMod
   */
  public Timestamp getFechaBaja() {
    return (Timestamp) fechaMod.clone();
  }

  /**
   * @param fechaMod the fechaMod to set
   */
  public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaMod = (Timestamp) fechaBaja.clone();
  }

}