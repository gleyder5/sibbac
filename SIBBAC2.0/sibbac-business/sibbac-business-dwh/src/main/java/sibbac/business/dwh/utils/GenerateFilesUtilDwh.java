package sibbac.business.dwh.utils;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableTableEmisionLoaderBo;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableTableEmisorLoaderBo;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableTableMercadoLoaderBo;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableTableWarrantLoaderBo;
import sibbac.business.dwh.utils.DwhConstantes.TipoValoresProcess;
import sibbac.business.dwh.utils.DwhConstantes.TipoValoresValidation;

@Service
public class GenerateFilesUtilDwh {

  protected static final Logger LOG = LoggerFactory.getLogger(GenerateFilesUtilDwh.class);

  /**
   * Obtiene el BO para ser ejecutado en un hilo
   * @return
   */
  public static CallableProcessInterface getValoresProcess(ApplicationContext ctx,
      TipoValoresProcess tipoValoresProcess) {
    CallableProcessInterface callableBo = null;
    switch (tipoValoresProcess) {
      case VALORES_EMISION:
        callableBo = ctx.getBean(CallableTableEmisionLoaderBo.class);
        break;
      case VALORES_EMISOR:
        callableBo = ctx.getBean(CallableTableEmisorLoaderBo.class);
        break;
      case VALORES_MERCADO:
        callableBo = ctx.getBean(CallableTableMercadoLoaderBo.class);
        break;
      case VALORES_WARRANT:
        callableBo = ctx.getBean(CallableTableWarrantLoaderBo.class);
        break;
      default:
        break;
    }
    return callableBo;
  }

  /**
   * Obtiene el nombre del proceso que será ejecutado en el hilo
   * @param tipoValoresProcess
   * @return
   */
  public static String getValoresProcessName(TipoValoresProcess tipoValoresProcess) {
    String name = null;

    switch (tipoValoresProcess) {
      case VALORES_EMISION:
        name = "DHW_VALORES_EMISION";
        break;
      case VALORES_EMISOR:
        name = "DHW_VALORES_EMISOR";
        break;
      case VALORES_MERCADO:
        name = "DHW_VALORES_MERCADO";
        break;
      case VALORES_WARRANT:
        name = "DHW_VALORES_WARRANT";
        break;
      default:
        break;
    }
    return name;
  }

  public static String getValoresValidationProcessName(TipoValoresValidation tipoValoresValidation) {
    String name = null;

    switch (tipoValoresValidation) {
      case VALIDACION_ALTA:
        name = "VALIDACION_ALTA";
        break;
      case VALIDACION_BAJA:
        name = "VALIDACION_BAJA";
        break;
      case VALIDACION_MOD:
        name = "VALIDACION_MOD";
        break;
      default:
        break;
    }
    return name;
  }

  public static String formatNumber(Integer numOperations, int lenght) {
    return formatNumber(numOperations.toString(), lenght);
  }

  public static String formatNumber(String field, int lenght) {
    StringBuilder number = new StringBuilder();
    if (field.length() < lenght) {
      field = String.format("%s%s", getZeros(lenght - field.length()), field);
    }
    number.append(field);
    return number.toString();
  }

  public static String formatString(String numOperations, int lenght) {
    StringBuilder number = new StringBuilder();
    if (numOperations.length() < lenght) {
      numOperations = String.format("%s%s", numOperations, getBlanks(lenght - numOperations.length()));
    }
    number.append(numOperations);
    return number.toString();
  }

  public static String formatDate(String pattern, Date date) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String fecha = simpleDateFormat.format(date);

    return fecha;
  }

  public static String formatDecimal(int leftSide, int rightSide, BigDecimal importe) {
    importe = importe.setScale(rightSide, RoundingMode.CEILING);
    String saldo = importe.toPlainString();
    String[] partesImporte = saldo.split("\\.");
    String left = "";
    String right = "";
    if (partesImporte.length < 2) {
      left = partesImporte[0];
      right = "0";
    }
    else {
      left = partesImporte[0];
      right = partesImporte[1];
    }

    return String.format("%s.%s", formatNumber(left, leftSide), formatNumber(right, rightSide));
  }

  public static String formatDecimalWithSign(int leftSide, int rightSide, BigDecimal importe) {
    String sign = (importe.compareTo(BigDecimal.ZERO) >= 0) ? "+" : "-";

    return String.format("%s%s", sign, formatDecimal(leftSide, rightSide, importe));
  }

  public static String getBlanks(int blanksNumber) {
    StringBuilder blanks = new StringBuilder();
    for (int i = 0; i < blanksNumber; i++) {
      blanks.append(" ");
    }

    return blanks.toString();

  }

  public static String getZeros(int zerosNumber) {
    StringBuilder zeros = new StringBuilder();
    for (int i = 0; i < zerosNumber; i++) {
      zeros.append(0);
    }

    return zeros.toString();

  }

  public static Date dateFromString(String fecha) {

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    try {
      return dateFormat.parse(fecha);
    }
    catch (ParseException e) {
      LOG.info("Error al pasear la fecha de String a Date");
      return null;
    }

  }

  public static Date dateFromStringPattern(String fecha, String pattern) {

    DateFormat dateFormat = new SimpleDateFormat(pattern);

    try {
      return dateFormat.parse(fecha);
    }
    catch (ParseException e) {
      LOG.info("Error al pasear la fecha de String a Date");
      return null;
    }

  }

  /**
   * Crea la estructura de directorios necesaria para la generación del fichero
   * @param outPutDirFile
   */
  public static void createOutPutDir(String outPutDir) {
    final File outDir = new File(outPutDir);
    if (outDir.mkdirs()) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Se ha creado la estructura de directorios " + outPutDir);
      }
    }
    else {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Ya existe la estructura de directorios " + outPutDir);
      }
    }
  }

}