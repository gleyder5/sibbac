package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

@Repository
public interface TipoValoresDao extends JpaRepository</* FIXME entidad */MaestroValores, String> {

  /**
   * Query para DWHgrupoValoresDao, ORIGINAL
   */
  public static final String TIPO_VALORES_QUERY = /* FIXME Query */ "SELECT CDGRUVAL,NBGRUVAL,CDTIPREN,NBTIPREN "
      + "FROM TMCT0_VALORES_GRUPO " + "GROUP BY CDGRUVAL, NBGRUVAL, CDTIPREN, NBTIPREN";

  /**
   * Query para DWHtipoValoresDao con Paginación
   */
  public static final String TIPO_VALORES_PAGE_QUERY = TIPO_VALORES_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHtipoValoresDao
   */
  public static final String TIPO_VALORES_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + TIPO_VALORES_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHtipoValoresDao sin
   * paginación
   * @return
   */
  @Query(value = TIPO_VALORES_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListTipoValores(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHtipoValoresDao sin
   * paginación
   * @return
   */
  @Query(value = TIPO_VALORES_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListTipoValores();

}
