package sibbac.business.dwh.ficheros.commons.dto;

import sibbac.common.file.constants.FileResult;

public class ProcessFileResultDTO extends ProcessResultDTO {

  private static final long serialVersionUID = 1L;

  private String fileNameProcess;

  private FileResult fileResult;

  public String getFileNameProcess() {
    return fileNameProcess;
  }

  public void setFileNameProcess(String fileNameProcess) {
    this.fileNameProcess = fileNameProcess;
  }

  public FileResult getFileResult() {
    return fileResult;
  }

  public void setFileResult(FileResult fileResult) {
    this.fileResult = fileResult;
  }

}