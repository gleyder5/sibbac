package sibbac.business.dwh.ficheros.nacional.db.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0cfgId;

@Repository
public interface OperacionesAutexDao extends JpaRepository<Tmct0cfg, Tmct0cfgId> {

  public static final String AUTEX_QUERY = "SELECT  DCT.FEEJECUC,  DCT.CDISIN, SUM(DCT.IMEFECTIVO) "
      + "AS IMEFECTI, SUM(DCT.IMTITULOS) AS NUCANEJE, VAL.DESCRIPCION_CORTA AS CDAUTEXX FROM TMCT0DESGLOSECLITIT DCT "
      + "INNER JOIN TMCT0DESGLOSECAMARA DCM  ON DCT.IDDESGLOSECAMARA = DCM.IDDESGLOSECAMARA LEFT JOIN TMCT0_VALORES VAL "
      + "ON DCT.CDISIN = VAL.CODIGO_DE_VALOR WHERE DCM.FECONTRATACION >= (CAST(:fechaEjec AS DATE)) - 9 DAY AND DCM.CDESTADOASIG >= 65 "
      + "GROUP BY DCT.FEEJECUC, DCT.CDISIN, VAL.DESCRIPCION_CORTA ORDER BY DCT.FEEJECUC, DCT.CDISIN, VAL.DESCRIPCION_CORTA";

  public static final String AUTEX_PAGE_QUERY = AUTEX_QUERY + " LIMIT :offset, :pageSize";

  public static final String AUTEX_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + AUTEX_QUERY + ")";

  @Query(value = AUTEX_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListAutex(@Param("fechaEjec") Date fechaEjec, @Param("offset") Integer offset,
      @Param("pageSize") Integer pageSize);

  @Query(value = AUTEX_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListAutex(@Param("fechaEjec") Date fechaEjec);

}
