package sibbac.business.dwh.ficheros.clientes.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.clientes.db.model.Tmct0DwhClientes;

@Repository
public interface Tmct0DwhClientesDao extends JpaRepository<Tmct0DwhClientes, Integer> {

  /**
   * Query para obtener todos los clientes que NO estén de baja
   * @return List<Object[]>
   */
  @Query(value = "SELECT NUCLIENT, CDCLIENT, CDPRODUC FROM TMCT0_DWH_CLIENTES "
      + "WHERE SUBSTRING(NBDESCLI,1,6) <> '#BAJA#'", nativeQuery = true)
  public List<Object[]> findClientesActivos();

  /**
   * Query para comprobar si el cliente sigue activo
   * @param cdAlias
   * @return int
   */
  @Query(value = "SELECT cast(ALI.FHFINAL as varchar(10)) FROM TMCT0ALI ALI INNER JOIN TMCT0CLI CLI "
      + "ON ALI.CDBROCLI = CLI.CDBROCLI " + "WHERE NUCLIENT=:nuclient "
      + "AND (CDALIASS = :cdalias OR CDALIASS=LTRIM(:cdalias))", nativeQuery = true)
  public String checkClienteActivo(@Param("nuclient") Integer nuClient, @Param("cdalias") String cdAlias);

  /**
   * Query para comprobar si el cliente sigue activo
   * @param cdAlias
   * @return int
   */
  @Query(value = "SELECT COUNT(*) FROM TMCT0ALI ALI " + "INNER JOIN TMCT0CLI CLI ON ALI.CDBROCLI = CLI.CDBROCLI "
      + "WHERE CLI.FHFINAL >= TO_CHAR(CURRENT DATE,'YYYYMMDD') "
      + "AND ALI.FHFINAL >= TO_CHAR(CURRENT DATE,'YYYYMMDD') " + "AND NUCLIENT=:nuclient AND (CDALIASS = :cdalias "
      + "OR CDALIASS=LTRIM(:cdalias))", nativeQuery = true)
  public int checkClienteActivoCount(@Param("nuclient") Integer nuClient, @Param("cdalias") String cdAlias);

  /**
   * Query para actualizar un cliente y darlo de baja
   * @param cdClient
   */
  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_DWH_CLIENTES SET NBDESCLI = SUBSTR('#BAJA#' || NBDESCLI, 1, 40), "
      + "USUAUDIT= 'TaskGenDWHClientes', FHAUDIT = CURRENT DATE " + "WHERE CDCLIENT = :cdclient", nativeQuery = true)
  public void updateClienteBaja(@Param("cdclient") Integer cdClient);

  /**
   * Query para comprobar si un cliente que ha operado está en
   * TMCT0_DWH_CLIENTES
   * @param cdClient
   * @return List<Object[]>
   */
  @Query(value = "SELECT IDCLIENT, CDCLIENT, CDPRODUC, CDGRUPOA, NBGRUPOA FROM TMCT0_DWH_CLIENTES "
      + "WHERE CDCLIENT =:cdclient", nativeQuery = true)
  public List<Object[]> checkClienteDWH(@Param("cdclient") Integer cdClient);

  /**
   * Query para obtener todos los datos a mapear para un registro
   * TMCT0_DWH_CLIENTES
   * @param cdAlias
   * @param cdProduc
   * @return List<Object[]>
   */
  @Query(value = "SELECT  (CASE  WHEN ALI.FHFINAL >= to_char(current date,'yyyymmdd')  "
      + "THEN SUBSTR(ALI.DESCRALI, 1, 40) ELSE '#BAJA#'||SUBSTR(ALI.DESCRALI, 1, 34) END)"
      + "AS NBDESCLI, EST.NUCTAPRO, CLI.NUCLIENT, EST.CDPRODUC, EST.CDGRUPOO, EST.NBGRUPOO, EST.CDPROTRA, "
      + "EST.NUCTATRA, EST.CDPROSAL, EST.NUCTASAL, EST.CDPROACM, EST.NUCTAACM, EST.CDHPPAIS, "
      + "ESTT.STLEV0LM,  ONI.NBNIVEL0, ESTT.STLEV1LM,  ONI.NBNIVEL1, ESTT.STLEV2LM, ONI.NBNIVEL2, "
      + "ESTT.STLEV3LM,  ONI.NBNIVEL3, ESTT.STLEV4LM, ONI.NBNIVEL4, CLI.CIF FROM TMCT0ALI ALI  "
      + "INNER JOIN TMCT0CLI CLI  ON ALI.CDBROCLI = CLI.CDBROCLI INNER JOIN TCLI0EST EST ON ALI.CDALIASS = EST.NUCTAPRO "
      + "INNER JOIN TMCT0EST ESTT ON ALI.CDALIASS =ESTT.CDALIASS INNER JOIN TMCT0ONI ONI ON ESTT.STLEV0LM = ONI.nuorden0 "
      + "AND ESTT.STLEV1LM = ONI.NUORDEN1 AND ESTT.STLEV2LM = ONI.NUORDEN2 AND ESTT.STLEV3LM = ONI.NUORDEN3 "
      + "AND ESTT.STLEV4LM = ONI.NUORDEN4 WHERE (ALI.CDALIASS = :cdalias  OR ALI.CDALIASS=LTRIM(:cdalias)) "
      + "AND EST.CDPRODUC= :cdproduct", nativeQuery = true)
  public List<Object[]> dwhDataToMap(@Param("cdalias") String cdAlias, @Param("cdproduct") String cdProduc);

  /**
   * Query que calcula el valor de un nuevo cdGrupoA
   * @return Integer
   */
  @Query(value = "SELECT MAX(CDGRUPOA) + 1 FROM TMCT0_DWH_CLIENTES", nativeQuery = true)
  public Integer getCdGrupoA();

}
