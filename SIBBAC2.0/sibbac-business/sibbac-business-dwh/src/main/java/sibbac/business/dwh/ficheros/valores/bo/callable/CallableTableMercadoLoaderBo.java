package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresMercadoBo;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresMercado;
import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableTableMercadoLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private ValoresMercadoBo valoresMercadoBo;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    valoresMercadoBo.saveBulkValoresMercado(toValoresMercadoEntity());
    return true;
  }

  /**
   * Convierte una lista de String a una lista de Entidades
   * @param emisionStringList
   * @return
   */
  private List<ValoresMercado> toValoresMercadoEntity() {
    final List<ValoresMercado> entities = new ArrayList<>();

    for (PartenonFileLineDTO element : super.listData) {
      entities.add(ValoresMercado.createValoresMercado(element));
    }
    return entities;
  }
}
