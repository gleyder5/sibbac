package sibbac.business.dwh.ficheros.valores.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The Class TMCT0blanqueoControlAnalisisDTO.
 */
public class MaestroValoresDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5270093962321653900L;

  /** The idvalor. */
  private BigInteger idvalor;

  /** The cdcvalov. */
  private String cdcvalov;

  /** The cdbesnum. */
  private String cdbesnum;

  /** The fhiniciv. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhiniciv;

  /** The cdisinvv. */
  private String cdisinvv;

  /** The nbtitrev. */
  private String nbtitrev;

  /** The cdcvabev. */
  private String cdcvabev;

  /** The cdcvsibv. */
  private String cdcvsibv;

  /** The nunominv. */
  private BigDecimal nunominv;

  /** The cdcotmav. */
  private String cdcotmav;

  /** The cdcotbav. */
  private String cdcotbav;

  /** The cdcotbiv. */
  private String cdcotbiv;

  /** The cdcotvav. */
  private String cdcotvav;

  /** The cdcotvrv. */
  private String cdcotvrv;

  /** The cdcotexv. */
  private String cdcotexv;

  /** The cdmonedv. */
  private String cdmonedv;

  /** The cdsisliv. */
  private String cdsisliv;

  /** The cdnominv. */
  private String cdnominv;

  /** The cdgrsibv. */
  private String cdgrsibv;

  /** The cdvaibex. */
  private String cdvaibex;

  /** The cdinstru. */
  private String cdinstru;

  /** The cdcifsoc. */
  private String cdcifsoc;

  /** The cdpaisev. */
  private String cdpaisev;

  /** The cddivisv. */
  private String cddivisv;

  /** The cdpercuv. */
  private String cdpercuv;

  /** The nbtit 10 c. */
  private String nbtit10c;

  /** The cdorgano. */
  private String cdorgano;

  /** The cdsitval. */
  private String cdsitval;

  /** The idmercon. */
  private String idmercon;

  /** The tpcambio. */
  private String tpcambio;

  /** The nudelote. */
  private BigDecimal nudelote;

  /** The fhultmod. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhultmod;

  /** The fhaltav. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhaltav;

  /** The usualta. */
  private String usualta;

  /** The fhmodiv. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhmodiv;

  /** The usumodi. */
  private String usumodi;

  /** The fhbajacv. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhbajacv;

  /** The usubaja. */
  private String usubaja;

  /**
   * Gets the idvalor.
   *
   * @return the idvalor
   */
  public BigInteger getIdvalor() {
    return idvalor;
  }

  /**
   * Sets the idvalor.
   *
   * @param idvalor the new idvalor
   */
  public void setIdvalor(BigInteger idvalor) {
    this.idvalor = idvalor;
  }

  /**
   * Gets the cdcvalov.
   *
   * @return the cdcvalov
   */
  public String getCdcvalov() {
    return cdcvalov;
  }

  /**
   * Sets the cdcvalov.
   *
   * @param cdcvalov the new cdcvalov
   */
  public void setCdcvalov(String cdcvalov) {
    this.cdcvalov = cdcvalov;
  }

  /**
   * Gets the cdbesnum.
   *
   * @return the cdbesnum
   */
  public String getCdbesnum() {
    return cdbesnum;
  }

  /**
   * Sets the cdbesnum.
   *
   * @param cdbesnum the new cdbesnum
   */
  public void setCdbesnum(String cdbesnum) {
    this.cdbesnum = cdbesnum;
  }

  /**
   * Gets the fhiniciv.
   *
   * @return the fhiniciv
   */
  public Date getFhiniciv() {
    return fhiniciv;
  }

  /**
   * Sets the fhiniciv.
   *
   * @param fhiniciv the new fhiniciv
   */
  public void setFhiniciv(Date fhiniciv) {
    this.fhiniciv = fhiniciv;
  }

  /**
   * Gets the cdisinvv.
   *
   * @return the cdisinvv
   */
  public String getCdisinvv() {
    return cdisinvv;
  }

  /**
   * Sets the cdisinvv.
   *
   * @param cdisinvv the new cdisinvv
   */
  public void setCdisinvv(String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  /**
   * Gets the nbtitrev.
   *
   * @return the nbtitrev
   */
  public String getNbtitrev() {
    return nbtitrev;
  }

  /**
   * Sets the nbtitrev.
   *
   * @param nbtitrev the new nbtitrev
   */
  public void setNbtitrev(String nbtitrev) {
    this.nbtitrev = nbtitrev;
  }

  /**
   * Gets the cdcvabev.
   *
   * @return the cdcvabev
   */
  public String getCdcvabev() {
    return cdcvabev;
  }

  /**
   * Sets the cdcvabev.
   *
   * @param cdcvabev the new cdcvabev
   */
  public void setCdcvabev(String cdcvabev) {
    this.cdcvabev = cdcvabev;
  }

  /**
   * Gets the cdcvsibv.
   *
   * @return the cdcvsibv
   */
  public String getCdcvsibv() {
    return cdcvsibv;
  }

  /**
   * Sets the cdcvsibv.
   *
   * @param cdcvsibv the new cdcvsibv
   */
  public void setCdcvsibv(String cdcvsibv) {
    this.cdcvsibv = cdcvsibv;
  }

  /**
   * Gets the nunominv.
   *
   * @return the nunominv
   */
  public BigDecimal getNunominv() {
    return nunominv;
  }

  /**
   * Sets the nunominv.
   *
   * @param nunominv the new nunominv
   */
  public void setNunominv(BigDecimal nunominv) {
    this.nunominv = nunominv;
  }

  /**
   * Gets the cdcotmav.
   *
   * @return the cdcotmav
   */
  public String getCdcotmav() {
    return cdcotmav;
  }

  /**
   * Sets the cdcotmav.
   *
   * @param cdcotmav the new cdcotmav
   */
  public void setCdcotmav(String cdcotmav) {
    this.cdcotmav = cdcotmav;
  }

  /**
   * Gets the cdcotbav.
   *
   * @return the cdcotbav
   */
  public String getCdcotbav() {
    return cdcotbav;
  }

  /**
   * Sets the cdcotbav.
   *
   * @param cdcotbav the new cdcotbav
   */
  public void setCdcotbav(String cdcotbav) {
    this.cdcotbav = cdcotbav;
  }

  /**
   * Gets the cdcotbiv.
   *
   * @return the cdcotbiv
   */
  public String getCdcotbiv() {
    return cdcotbiv;
  }

  /**
   * Sets the cdcotbiv.
   *
   * @param cdcotbiv the new cdcotbiv
   */
  public void setCdcotbiv(String cdcotbiv) {
    this.cdcotbiv = cdcotbiv;
  }

  /**
   * Gets the cdcotvav.
   *
   * @return the cdcotvav
   */
  public String getCdcotvav() {
    return cdcotvav;
  }

  /**
   * Sets the cdcotvav.
   *
   * @param cdcotvav the new cdcotvav
   */
  public void setCdcotvav(String cdcotvav) {
    this.cdcotvav = cdcotvav;
  }

  /**
   * Gets the cdcotvrv.
   *
   * @return the cdcotvrv
   */
  public String getCdcotvrv() {
    return cdcotvrv;
  }

  /**
   * Sets the cdcotvrv.
   *
   * @param cdcotvrv the new cdcotvrv
   */
  public void setCdcotvrv(String cdcotvrv) {
    this.cdcotvrv = cdcotvrv;
  }

  /**
   * Gets the cdcotexv.
   *
   * @return the cdcotexv
   */
  public String getCdcotexv() {
    return cdcotexv;
  }

  /**
   * Sets the cdcotexv.
   *
   * @param cdcotexv the new cdcotexv
   */
  public void setCdcotexv(String cdcotexv) {
    this.cdcotexv = cdcotexv;
  }

  /**
   * Gets the cdmonedv.
   *
   * @return the cdmonedv
   */
  public String getCdmonedv() {
    return cdmonedv;
  }

  /**
   * Sets the cdmonedv.
   *
   * @param cdmonedv the new cdmonedv
   */
  public void setCdmonedv(String cdmonedv) {
    this.cdmonedv = cdmonedv;
  }

  /**
   * Gets the cdsisliv.
   *
   * @return the cdsisliv
   */
  public String getCdsisliv() {
    return cdsisliv;
  }

  /**
   * Sets the cdsisliv.
   *
   * @param cdsisliv the new cdsisliv
   */
  public void setCdsisliv(String cdsisliv) {
    this.cdsisliv = cdsisliv;
  }

  /**
   * Gets the cdnominv.
   *
   * @return the cdnominv
   */
  public String getCdnominv() {
    return cdnominv;
  }

  /**
   * Sets the cdnominv.
   *
   * @param cdnominv the new cdnominv
   */
  public void setCdnominv(String cdnominv) {
    this.cdnominv = cdnominv;
  }

  /**
   * Gets the cdgrsibv.
   *
   * @return the cdgrsibv
   */
  public String getCdgrsibv() {
    return cdgrsibv;
  }

  /**
   * Sets the cdgrsibv.
   *
   * @param cdgrsibv the new cdgrsibv
   */
  public void setCdgrsibv(String cdgrsibv) {
    this.cdgrsibv = cdgrsibv;
  }

  /**
   * Gets the cdvaibex.
   *
   * @return the cdvaibex
   */
  public String getCdvaibex() {
    return cdvaibex;
  }

  /**
   * Sets the cdvaibex.
   *
   * @param cdvaibex the new cdvaibex
   */
  public void setCdvaibex(String cdvaibex) {
    this.cdvaibex = cdvaibex;
  }

  /**
   * Gets the cdinstru.
   *
   * @return the cdinstru
   */
  public String getCdinstru() {
    return cdinstru;
  }

  /**
   * Sets the cdinstru.
   *
   * @param cdinstru the new cdinstru
   */
  public void setCdinstru(String cdinstru) {
    this.cdinstru = cdinstru;
  }

  /**
   * Gets the cdcifsoc.
   *
   * @return the cdcifsoc
   */
  public String getCdcifsoc() {
    return cdcifsoc;
  }

  /**
   * Sets the cdcifsoc.
   *
   * @param cdcifsoc the new cdcifsoc
   */
  public void setCdcifsoc(String cdcifsoc) {
    this.cdcifsoc = cdcifsoc;
  }

  /**
   * Gets the cdpaisev.
   *
   * @return the cdpaisev
   */
  public String getCdpaisev() {
    return cdpaisev;
  }

  /**
   * Sets the cdpaisev.
   *
   * @param cdpaisev the new cdpaisev
   */
  public void setCdpaisev(String cdpaisev) {
    this.cdpaisev = cdpaisev;
  }

  /**
   * Gets the cdpaisev.
   *
   * @return the cddivisv
   */
  public String getCddivisv() {
    return cddivisv;
  }

  /**
   * Sets the cddivisv.
   *
   * @param cddivisv the new cddivisv
   */
  public void setCddivisv(String cddivisv) {
    this.cddivisv = cddivisv;
  }

  /**
   * Gets the cdpercuv.
   *
   * @return the cdpercuv
   */
  public String getCdpercuv() {
    return cdpercuv;
  }

  /**
   * Sets the cdpercuv.
   *
   * @param cdpercuv the new cdpercuv
   */
  public void setCdpercuv(String cdpercuv) {
    this.cdpercuv = cdpercuv;
  }

  /**
   * Gets the nbtit 10 c.
   *
   * @return the nbtit 10 c
   */
  public String getNbtit10c() {
    return nbtit10c;
  }

  /**
   * Sets the nbtit 10 c.
   *
   * @param nbtit10c the new nbtit 10 c
   */
  public void setNbtit10c(String nbtit10c) {
    this.nbtit10c = nbtit10c;
  }

  /**
   * Gets the cdorgano.
   *
   * @return the cdorgano
   */
  public String getCdorgano() {
    return cdorgano;
  }

  /**
   * Sets the cdorgano.
   *
   * @param cdorgano the new cdorgano
   */
  public void setCdorgano(String cdorgano) {
    this.cdorgano = cdorgano;
  }

  /**
   * Gets the cdsitval.
   *
   * @return the cdsitval
   */
  public String getCdsitval() {
    return cdsitval;
  }

  /**
   * Sets the cdsitval.
   *
   * @param cdsitval the new cdsitval
   */
  public void setCdsitval(String cdsitval) {
    this.cdsitval = cdsitval;
  }

  /**
   * Gets the idmercon.
   *
   * @return the idmercon
   */
  public String getIdmercon() {
    return idmercon;
  }

  /**
   * Sets the idmercon.
   *
   * @param idmercon the new idmercon
   */
  public void setIdmercon(String idmercon) {
    this.idmercon = idmercon;
  }

  /**
   * Gets the tpcambio.
   *
   * @return the tpcambio
   */
  public String getTpcambio() {
    return tpcambio;
  }

  /**
   * Sets the tpcambio.
   *
   * @param tpcambio the new tpcambio
   */
  public void setTpcambio(String tpcambio) {
    this.tpcambio = tpcambio;
  }

  /**
   * Gets the nudelote.
   *
   * @return the nudelote
   */
  public BigDecimal getNudelote() {
    return nudelote;
  }

  /**
   * Sets the nudelote.
   *
   * @param nudelote the new nudelote
   */
  public void setNudelote(BigDecimal nudelote) {
    this.nudelote = nudelote;
  }

  /**
   * Gets the fhultmod.
   *
   * @return the fhultmod
   */
  public Date getFhultmod() {
    return fhultmod;
  }

  /**
   * Sets the fhultmod.
   *
   * @param fhultmod the new fhultmod
   */
  public void setFhultmod(Date fhultmod) {
    this.fhultmod = fhultmod;
  }

  /**
   * Gets the fhaltav.
   *
   * @return the fhaltav
   */
  public Date getFhaltav() {
    return fhaltav;
  }

  /**
   * Sets the fhaltav.
   *
   * @param fhaltav the new fhaltav
   */
  public void setFhaltav(Date fhaltav) {
    this.fhaltav = fhaltav;
  }

  /**
   * Gets the usualta.
   *
   * @return the usualta
   */
  public String getUsualta() {
    return usualta;
  }

  /**
   * Sets the usualta.
   *
   * @param usualta the new usualta
   */
  public void setUsualta(String usualta) {
    this.usualta = usualta;
  }

  /**
   * Gets the fhmodiv.
   *
   * @return the fhmodiv
   */
  public Date getFhmodiv() {
    return fhmodiv;
  }

  /**
   * Sets the fhmodiv.
   *
   * @param fhmodiv the new fhmodiv
   */
  public void setFhmodiv(Date fhmodiv) {
    this.fhmodiv = fhmodiv;
  }

  /**
   * Gets the usumodi.
   *
   * @return the usumodi
   */
  public String getUsumodi() {
    return usumodi;
  }

  /**
   * Sets the usumodi.
   *
   * @param usumodi the new usumodi
   */
  public void setUsumodi(String usumodi) {
    this.usumodi = usumodi;
  }

  /**
   * Gets the fhbajacv.
   *
   * @return the fhbajacv
   */
  public Date getFhbajacv() {
    return fhbajacv;
  }

  /**
   * Sets the fhbajacv.
   *
   * @param fhbajacv the new fhbajacv
   */
  public void setFhbajacv(Date fhbajacv) {
    this.fhbajacv = fhbajacv;
  }

  /**
   * Gets the usubaja.
   *
   * @return the usubaja
   */
  public String getUsubaja() {
    return usubaja;
  }

  /**
   * Sets the usubaja.
   *
   * @param usubaja the new usubaja
   */
  public void setUsubaja(String usubaja) {
    this.usubaja = usubaja;
  }

  /**
   * Gets the serialversionuid.
   *
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "MaestroValoresDTO [idvalor=" + this.idvalor + ", cdcvalov=" + this.cdcvalov + ", cdbesnum=" + this.cdbesnum
        + ", fhiniciv=" + this.fhiniciv + ", cdisinvv=" + this.cdisinvv + ", nbtitrev=" + this.nbtitrev + ", cdcvabev="
        + this.cdcvabev + ", cdcvsibv=" + this.cdcvsibv + ", nunominv=" + this.nunominv + ", cdcotmav=" + this.cdcotmav
        + ", cdcotbav=" + this.cdcotbav + ", cdcotbiv=" + this.cdcotbiv + ", cdcotvav=" + this.cdcotvav + ", cdcotvrv="
        + this.cdcotvrv + ", cdcotexv=" + this.cdcotexv + ", cdmonedv=" + this.cdmonedv + ", cdsisliv=" + this.cdsisliv
        + ", cdnominv=" + this.cdnominv + ", cdgrsibv=" + this.cdgrsibv + ", cdvaibex=" + this.cdvaibex + ", cdinstru="
        + this.cdinstru + ", cdcifsoc=" + this.cdcifsoc + ", cdpaisev=" + this.cdpaisev + ", cddivisv=" + this.cddivisv
        + ", cdpercuv=" + this.cdpercuv + ", nbtit10c=" + this.nbtit10c + ", cdorgano=" + this.cdorgano + ", cdsitval="
        + this.cdsitval + ", idmercon=" + this.idmercon + ", tpcambio=" + this.tpcambio + ", nudelote=" + this.nudelote
        + ", fhultmod=" + this.fhultmod + ", fhaltav=" + this.fhaltav + ", usualta=" + this.usualta + ", fhmodiv="
        + this.fhmodiv + ", usumodi=" + this.usumodi + ", fhbajacv=" + this.fhbajacv + ", usubaja=" + this.usubaja
        + "]";
  }
}
