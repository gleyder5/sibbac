package sibbac.business.dwh.web.dto;

public class LanzamientoManualDTO {

  private String fechaEjecucion;

  public LanzamientoManualDTO() {
    super();
  }

  public LanzamientoManualDTO(String fechaEjecucion) {
    super();
    this.fechaEjecucion = fechaEjecucion;
  }

  public String getFechaEjecucion() {
    return fechaEjecucion;
  }

  public void setFechaEjecucion(String fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

}
