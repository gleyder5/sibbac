package sibbac.business.dwh.ficheros.tradersaccounters.db.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Tmct0SalesTrader.
 */
@Entity
@Table(name = "TMCT0_SALES_TRADER")
public class Tmct0SalesTrader implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1367920644570427389L;

  /** The id cdempleado. */
  @Id
  @Column(name = "CDEMPLEADO", length = 10, nullable = false, unique = true)
  private String cdempleado;

  /** The nbnombre. */
  @Column(name = "NBNOMBRE", length = 30, nullable = false)
  private String nbnombre;

  /** The nbapellido 1. */
  @Column(name = "NBAPELLIDO1", length = 30, nullable = false)
  private String nbapellido1;

  /** The nbapellido 2. */
  @Column(name = "NBAPELLIDO2", length = 30, nullable = false)
  private String nbapellido2;

  /** The cdnacionalidad. */
  @Column(name = "CDNACIONALIDAD", length = 3, nullable = false)
  private String cdnacionalidad;

  /** The fechanacimiento. */
  @Column(name = "FECHA_NACIMIENTO", length = 10, nullable = false)
  private Date fechanacimiento;

  /** The tipoidmifid. */
  @Column(name = "TIPO_ID_MIFID", length = 1, nullable = false)
  private String tipoidmifid;

  /** The idmifid. */
  @Column(name = "ID_MIFID", length = 20, nullable = false)
  private String idmifid;

  /** The codigocorto. */
  @Column(name = "CODIGO_CORTO", length = 10, nullable = false)
  private Integer codigocorto;

  /** The cdtipousuario. */
  @Column(name = "CD_TIPO_USUARIO", length = 1, nullable = false)
  private String cdtipousuario;

  /** The fhinicio. */
  @Column(name = "FHINICIO", length = 10, nullable = false)
  private Date fhinicio;

  /** The fhfinal. */
  @Column(name = "FHFINAL", length = 10, nullable = false)
  private Date fhfinal;

  /** The fhaudit. */
  @Column(name = "FHAUDIT", length = 26, nullable = false)
  private Timestamp fhaudit;

  /** The cdusuaud. */
  @Column(name = "CDUSUAUD", length = 32, nullable = false)
  private String cdusuaud;

  /** The producto. */
  @Column(name = "PRODUCTO", length = 3, nullable = true)
  private String producto;

  /** The cuenta. */
  @Column(name = "CUENTA", length = 5, nullable = true)
  private Integer cuenta;

  /** The nbcorto. */
  @Column(name = "NBCORTO", length = 5, nullable = true)
  private String nbcorto;

  /** The indtrader. */
  @Column(name = "INDTRADER", length = 1, nullable = true)
  private Integer indtrader;

  /** The indsale. */
  @Column(name = "INDSALE", length = 1, nullable = true)
  private Integer indsale;

  /** The indtrad_sale. */
  @Column(name = "INDTRAD_SALE", length = 1, nullable = true)
  private Integer indtradsale;

  /** The indaccount. */
  @Column(name = "INDACCOUNT", length = 1, nullable = true)
  private Integer indaccount;

  /**
   * Gets the cdempleado.
   *
   * @return the cdempleado
   */
  public String getCdempleado() {
    return cdempleado;
  }

  /**
   * Sets the cdempleado.
   *
   * @param cdempleado the new cdempleado
   */
  public void setCdempleado(String cdempleado) {
    this.cdempleado = cdempleado;
  }

  /**
   * Gets the nbnombre.
   *
   * @return the nbnombre
   */
  public String getNbnombre() {
    return nbnombre;
  }

  /**
   * Sets the nbnombre.
   *
   * @param nbnombre the new nbnombre
   */
  public void setNbnombre(String nbnombre) {
    this.nbnombre = nbnombre;
  }

  /**
   * Gets the nbapellido 1.
   *
   * @return the nbapellido 1
   */
  public String getNbapellido1() {
    return nbapellido1;
  }

  /**
   * Sets the nbapellido 1.
   *
   * @param nbapellido1 the new nbapellido 1
   */
  public void setNbapellido1(String nbapellido1) {
    this.nbapellido1 = nbapellido1;
  }

  /**
   * Gets the nbapellido 2.
   *
   * @return the nbapellido 2
   */
  public String getNbapellido2() {
    return nbapellido2;
  }

  /**
   * Sets the nbapellido 2.
   *
   * @param nbapellido2 the new nbapellido 2
   */
  public void setNbapellido2(String nbapellido2) {
    this.nbapellido2 = nbapellido2;
  }

  /**
   * Gets the cdnacionalidad.
   *
   * @return the cdnacionalidad
   */
  public String getCdnacionalidad() {
    return cdnacionalidad;
  }

  /**
   * Sets the cdnacionalidad.
   *
   * @param cdnacionalidad the new cdnacionalidad
   */
  public void setCdnacionalidad(String cdnacionalidad) {
    this.cdnacionalidad = cdnacionalidad;
  }

  /**
   * Gets the fechanacimiento.
   *
   * @return the fechanacimiento
   */
  public Date getFechanacimiento() {
    return fechanacimiento;
  }

  /**
   * Sets the fechanacimiento.
   *
   * @param fechanacimiento the new fechanacimiento
   */
  public void setFechanacimiento(Date fechanacimiento) {
    this.fechanacimiento = fechanacimiento;
  }

  /**
   * Gets the tipoidmifid.
   *
   * @return the tipoidmifid
   */
  public String getTipoidmifid() {
    return tipoidmifid;
  }

  /**
   * Sets the tipoidmifid.
   *
   * @param tipoidmifid the new tipoidmifid
   */
  public void setTipoidmifid(String tipoidmifid) {
    this.tipoidmifid = tipoidmifid;
  }

  /**
   * Gets the idmifid.
   *
   * @return the idmifid
   */
  public String getIdmifid() {
    return idmifid;
  }

  /**
   * Sets the idmifid.
   *
   * @param idmifid the new idmifid
   */
  public void setIdmifid(String idmifid) {
    this.idmifid = idmifid;
  }

  /**
   * Gets the codigocorto.
   *
   * @return the codigocorto
   */
  public Integer getCodigocorto() {
    return codigocorto;
  }

  /**
   * Sets the codigocorto.
   *
   * @param codigocorto the new codigocorto
   */
  public void setCodigocorto(Integer codigocorto) {
    this.codigocorto = codigocorto;
  }

  /**
   * Gets the cdtipousuario.
   *
   * @return the cdtipousuario
   */
  public String getCdtipousuario() {
    return cdtipousuario;
  }

  /**
   * Sets the cdtipousuario.
   *
   * @param cdtipousuario the new cdtipousuario
   */
  public void setCdtipousuario(String cdtipousuario) {
    this.cdtipousuario = cdtipousuario;
  }

  /**
   * Gets the fhinicio.
   *
   * @return the fhinicio
   */
  public Date getFhinicio() {
    return fhinicio;
  }

  /**
   * Sets the fhinicio.
   *
   * @param fhinicio the new fhinicio
   */
  public void setFhinicio(Date fhinicio) {
    this.fhinicio = fhinicio;
  }

  /**
   * Gets the fhfinal.
   *
   * @return the fhfinal
   */
  public Date getFhfinal() {
    return fhfinal;
  }

  /**
   * Sets the fhfinal.
   *
   * @param fhfinal the new fhfinal
   */
  public void setFhfinal(Date fhfinal) {
    this.fhfinal = fhfinal;
  }

  /**
   * Gets the fhaudit.
   *
   * @return the fhaudit
   */
  public Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * Sets the fhaudit.
   *
   * @param fhaudit the new fhaudit
   */
  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * Gets the cdusuaud.
   *
   * @return the cdusuaud
   */
  public String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * Sets the cdusuaud.
   *
   * @param cdusuaud the new cdusuaud
   */
  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  /**
   * Gets the producto.
   *
   * @return the producto
   */
  public String getProducto() {
    return producto;
  }

  /**
   * Sets the producto.
   *
   * @param producto the new producto
   */
  public void setProducto(String producto) {
    this.producto = producto;
  }

  /**
   * Gets the cuenta.
   *
   * @return the cuenta
   */
  public Integer getCuenta() {
    return cuenta;
  }

  /**
   * Sets the cuenta.
   *
   * @param cuenta the new cuenta
   */
  public void setCuenta(Integer cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * Gets the nbcorto.
   *
   * @return the nbcorto
   */
  public String getNbcorto() {
    return nbcorto;
  }

  /**
   * Sets the nbcorto.
   *
   * @param nbcorto the new nbcorto
   */
  public void setNbcorto(String nbcorto) {
    this.nbcorto = nbcorto;
  }

  /**
   * Gets the indtrader.
   *
   * @return the indtrader
   */
  public Integer getIndtrader() {
    return indtrader;
  }

  /**
   * Sets the indtrader.
   *
   * @param indtrader the new indtrader
   */
  public void setIndtrader(Integer indtrader) {
    this.indtrader = indtrader;
  }

  /**
   * Gets the indsale.
   *
   * @return the indsale
   */
  public Integer getIndsale() {
    return indsale;
  }

  /**
   * Sets the indsale.
   *
   * @param indsale the new indsale
   */
  public void setIndsale(Integer indsale) {
    this.indsale = indsale;
  }

  /**
   * Gets the indtradsale.
   *
   * @return the indtradsale
   */
  public Integer getIndtradsale() {
    return indtradsale;
  }

  /**
   * Sets the indtradsale.
   *
   * @param indtradsale the new indtradsale
   */
  public void setIndtradsale(Integer indtradsale) {
    this.indtradsale = indtradsale;
  }

  /**
   * Gets the indaccount.
   *
   * @return the indaccount
   */
  public Integer getIndaccount() {
    return indaccount;
  }

  /**
   * Sets the indaccount.
   *
   * @param indaccount the new indaccount
   */
  public void setIndaccount(Integer indaccount) {
    this.indaccount = indaccount;
  }

  @Override
  public String toString() {
    return "Tmct0SalesTraderWithNewFields [cdempleado=" + cdempleado + ", nbnombre=" + nbnombre + ", nbapellido1="
        + nbapellido1 + ", nbapellido2=" + nbapellido2 + ", cdnacionalidad=" + cdnacionalidad + ", fechanacimiento="
        + fechanacimiento + ", tipoidmifid=" + tipoidmifid + ", idmifid=" + idmifid + ", codigocorto=" + codigocorto
        + ", cdtipousuario=" + cdtipousuario + ", fhinicio=" + fhinicio + ", fhfinal=" + fhfinal + ", fhaudit="
        + fhaudit + ", cdusuaud=" + cdusuaud + ", producto=" + producto + ", cuenta=" + cuenta + ", nbcorto=" + nbcorto
        + ", indtrader=" + indtrader + ", indsale=" + indsale + ", indtradsale=" + indtradsale + ", indaccount="
        + indaccount + "]";
  }

}
