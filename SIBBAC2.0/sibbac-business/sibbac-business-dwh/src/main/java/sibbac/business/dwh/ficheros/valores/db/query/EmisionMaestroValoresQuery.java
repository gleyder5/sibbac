package sibbac.business.dwh.ficheros.valores.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class MaestroValoresQuery.
 */
@Component("EmisionMaestroValoresQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EmisionMaestroValoresQuery extends AbstractDynamicPageQuery {

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT " + "  Q1.EXIST_MAESTRO_VALORES," + "  Q1.IS_WARRANT," + "  Q1.FEEMISIO,"
      + "  Q1.DSEMISIO," + "  Q1.IMNOMINA," + "  Q1.SITUVAL," + "  Q1.CDALFVAL," + "  Q1.FEESTADO," + "  Q1.CDCAMBIO,"
      + "  Q1.CDFORMAT," + "  Q1.CDSTLIQI," + "  Q1.CDEMISIO," + "  Q1.CDCVALOV," + "  Q1.CDBESNUM," + "  Q1.CDESTADO,"
      + "  Q1.CDCIFSOC," + "  Q1.CDPAISEV," + "  Q1.INDCOTIZ," + "  Q1.CDBOLSA," + "  Q1.CDMONEDA," + "  Q1.CDVALMDO,"
      + "  Q1.CDMERCAD," + "  Q1.CDSECTOR," + "  Q1.CDORGANT," + "  Q1.NULOTE," + "  Q1.NBTITREV," + "  Q1.NUNOMINV,"
      + "  Q1.CDSISLIV," + "  Q1.CDNOMINV," + "  Q1.CDISIN," + "  Q1.NBTIT10C," + "  Q1.CDORGANO";

  /** The Constant FROM. */
  private static final String FROM = " FROM ( SELECT"
      + "    (CASE WHEN (NVL(MAE.COUNTER, 0) > 0) THEN 1 ELSE 0 END) AS EXIST_MAESTRO_VALORES,"
      + "    (CASE WHEN (NVL(WARR.IS_WARRANT, 0) > 0) THEN 1 ELSE 0 END) AS IS_WARRANT,"
      + "    EMI.FEEMISIO, EMI.DSEMISIO, EMI.IMNOMINA," + "    EMI.SITUVAL, EMI.CDALFVAL, EMI.FEESTADO,"
      + "    EMI.CDCAMBIO, EMI.CDFORMAT, EMI.CDSTLIQI," + "    EMI.CDEMISIO, EMI.CDCVALOV, EMI.CDBESNUM,"
      + "    EMI.CDESTADO,"
      + "    EMI.CDISIN,"
      + "    EMS.CDNIF AS CDCIFSOC, (CASE WHEN (EMS.CDPAIS = '00' OR EMS.CDPAIS ='' OR EMS.CDPAIS IS NULL) THEN SUBSTR(EMI.CDISIN, 1, 2) ELSE EMS.CDPAIS END) AS CDPAISEV,"
      + "    VME.INDCOTIZ AS INDCOTIZ," + "    VME.CDBOLSA AS CDBOLSA," + "    MON.CDMONEDA AS CDMONEDA,"
      + "    VME.CDVALMDO AS CDVALMDO," + "    VME.CDMERCAD AS CDMERCAD,"
      + "    (CASE WHEN (VME.CDSECTOR = '46' OR VME.CDSECTOR = '48') THEN '89' ELSE VME.CDSECTOR END) AS CDSECTOR,"
      + "    VME.CDORGANT," + "    VME.NULOTE," + "    MAE.NBTITREV, MAE.NUNOMINV, MAE.CDSISLIV,"
      + "    MAE.CDNOMINV, MAE.NBTIT10C," + "    MAE.CDORGANO" + "  FROM"
      + "    (SELECT CDISIN, FEEMISIO, DSEMISIO, IMNOMINA, SITUVAL, CDALFVAL, FEESTADO, CDCAMBIO, CDFORMAT, CDSTLIQI, CDEMISIO, SUBSTR(CDEMISIO, 5,8) AS CDCVALOV, CONCAT('9999', SUBSTR(CDEMISIO, 1, 4)) AS CDBESNUM, CDESTADO FROM TMCT0_VALORES_EMISION) AS EMI"
      + "    LEFT JOIN TMCT0_VALORES_EMISOR EMS ON EMS.CDEMISIO = EMI.CDEMISIO"
      + "    LEFT JOIN TMCT0_VALORES_MERCADO VME ON VME.CDEMISIO = EMI.CDEMISIO"
      + "    LEFT JOIN TMCT0_MONEDA MON ON MON.NBCODIGO = VME.CDMONISO"
      + "    LEFT JOIN (SELECT COUNT(*) AS COUNTER, CDCVALOV, CDBESNUM, NBTITREV, NUNOMINV, CDSISLIV, CDNOMINV, CDISINVV, NBTIT10C, CDORGANO FROM TMCT0_MAESTRO_VALORES WHERE FHBAJACV > CURRENT_DATE GROUP BY CDCVALOV, CDBESNUM, NBTITREV, NUNOMINV, CDSISLIV, CDNOMINV, CDISINVV, NBTIT10C, CDORGANO) AS MAE ON MAE.CDCVALOV = EMI.CDCVALOV AND MAE.CDBESNUM = EMI.CDBESNUM"
      + "    LEFT JOIN (SELECT COUNT(CDEMISIO) AS IS_WARRANT, CDEMISIO FROM TMCT0_VALORES_WARRANT GROUP BY CDEMISIO) AS WARR ON WARR.CDEMISIO = EMI.CDEMISIO"
      + " ) AS Q1";

  /** The Constant WHERE. */
  private static final String WHERE = " ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("COUNTER", "COUNTER"),
      new DynamicColumn("CDCVALOV", "CDCVALOV"), new DynamicColumn("CDBESNUM", "CDBESNUM"),
      new DynamicColumn("CDESTADO", "CDESTADO"), new DynamicColumn("NBTITREV", "NBTITREV"),
      new DynamicColumn("NUNOMINV", "NUNOMINV"), new DynamicColumn("CDSISLIV", "CDSISLIV"),
      new DynamicColumn("CDNOMINV", "CDNOMINV"), new DynamicColumn("CDISINVV", "CDISINVV"),
      new DynamicColumn("NBTIT10C", "NBTIT10C"), new DynamicColumn("CDORGANO", "CDORGANO") };

  /**
   * Make filters.
   *
   * @return the list
   */
  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;
    res = new ArrayList<>();
    res.add(new StringDynamicFilter("COUNTER", "COUNTER"));
    res.add(new StringDynamicFilter("CDCVALOV", "CDCVALOV"));
    res.add(new StringDynamicFilter("CDBESNUM", "CDBESNUM"));
    res.add(new StringDynamicFilter("CDESTADO", "CDESTADO"));
    res.add(new StringDynamicFilter("NBTITREV", "NBTITREV"));
    res.add(new StringDynamicFilter("NUNOMINV", "NUNOMINV"));
    res.add(new StringDynamicFilter("CDSISLIV", "CDSISLIV"));
    res.add(new StringDynamicFilter("CDNOMINV", "CDNOMINV"));
    res.add(new StringDynamicFilter("CDISINVV", "CDISINVV"));
    res.add(new StringDynamicFilter("NBTIT10C", "NBTIT10C"));
    res.add(new StringDynamicFilter("CDORGANO", "CDORGANO"));
    return res;
  }

  /**
   * Instantiates a new maestro valores query.
   */
  public EmisionMaestroValoresQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  /**
   * Instantiates a new maestro valores query.
   *
   * @param columns the columns
   */
  public EmisionMaestroValoresQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  /**
   * Instantiates a new maestro valores query.
   *
   * @param filters the filters
   * @param columns the columns
   */
  public EmisionMaestroValoresQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}
