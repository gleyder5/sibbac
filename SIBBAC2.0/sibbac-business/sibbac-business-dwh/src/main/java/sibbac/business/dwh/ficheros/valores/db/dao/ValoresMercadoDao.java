package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.ValoresMercado;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ValoresMercadoDao extends JpaRepository<ValoresMercado, String> {

  @Query(value = "SELECT ME.CDMERCAD, ME.CDORGANT, ME.CDVALMDO, ME.CDSECTOR, ME.CDBOLSA, ME.NULOTE, ME.INDCOTIZ, MON.CDMONEDA "
      + "FROM TMCT0_VALORES_MERCADO ME INNER JOIN TMCT0_MONEDA MON "
      + "ON ME.CDMONISO = MON.NBCODIGO WHERE CDEMISIO=:cdemisio", nativeQuery = true)
  public List<Object[]> findMercado(@Param("cdemisio") String cdemisio);

}
