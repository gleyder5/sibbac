package sibbac.business.dwh.ficheros.valores.db.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.database.DBConstants.WRAPPERS;

/**
 * Class that represents a
 * {@link sibbac.business.wrappers.database.model.Monedas }. Entity: "Monedas".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table(name = WRAPPERS.MONEDA)
@Entity
public class Moneda implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1075968899412373267L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  /** The cdmoneda. */
  @Column(name = "CDMONEDA", nullable = false, length = 3)
  private String cdmoneda;
  
  /** The version. */
  @Column(name = "VERSION", nullable = false, length = 19)
  private BigInteger version;

  /** The nbdescripcion. */
  @Column(name = "NBDESCRIPCION", nullable = false, length = 50)
  private String nbdesc;

  /** The nbcodigo. */
  @Column(name = "NBCODIGO", nullable = false, length = 3)
  private String nbcodigo;

  /** The auditdate. */
  @Column(name = "AUDIT_DATE", nullable = false, length = 26)
  private Timestamp auditdate;

  /** The audituser. */
  @Column(name = "AUDIT_USER", nullable = false, length = 255)
  private String audituser;

  

  public BigInteger getId() {
    return id;
  }



  public void setId(BigInteger id) {
    this.id = id;
  }



  public String getCdmoneda() {
    return cdmoneda;
  }



  public void setCdmoneda(String cdmoneda) {
    this.cdmoneda = cdmoneda;
  }



  public BigInteger getVersion() {
    return version;
  }



  public void setVersion(BigInteger version) {
    this.version = version;
  }



  public String getNbdesc() {
    return nbdesc;
  }



  public void setNbdesc(String nbdesc) {
    this.nbdesc = nbdesc;
  }



  public String getNbcodigo() {
    return nbcodigo;
  }



  public void setNbcodigo(String nbcodigo) {
    this.nbcodigo = nbcodigo;
  }



  public Timestamp getAuditdate() {
    return auditdate != null ? (Timestamp) auditdate.clone() : null;
  }



  public void setAuditdate(Timestamp auditdate) {
    this.auditdate = (auditdate != null ? (Timestamp) auditdate.clone() : null);
  }



  public String getAudituser() {
    return audituser;
  }



  public void setAudituser(String audituser) {
    this.audituser = audituser;
  }



  /**
   * Gets the serialversionuid.
   *
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }
}
