package sibbac.business.dwh.ficheros.callable;

import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public abstract class AbstractCallableProcessBo implements CallableProcessInterface {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractCallableProcessBo.class);

  protected Future<Boolean> future;

  protected List<PartenonFileLineDTO> listData;

  private String name;

  @Override
  public Boolean call() throws Exception {
    Boolean result = false;
    long initTimeStamp = System.currentTimeMillis();

    result = executeProcess();

    long endTimeStamp = System.currentTimeMillis() - initTimeStamp;

    if (LOG.isDebugEnabled()) {
      if (listData != null) {
        LOG.debug(
            String.format("Insert de %d elementos para %s en %d ms", listData.size(), getProcessName(), endTimeStamp));
      }
      else {
        LOG.debug(String.format("Insert para %s en %d ms", getProcessName(), endTimeStamp));
      }

    }
    return result;
  }

  @Override
  public void setListData(List<PartenonFileLineDTO> listData) {
    this.listData = listData;
  }

  @Override
  public List<PartenonFileLineDTO> getListData() {
    return listData;
  }

  @Override
  public Future<Boolean> getFuture() {
    return this.future;
  }

  @Override
  public void setFuture(Future<Boolean> future) {
    this.future = future;
  }

  @Override
  public void setProcessName(String name) {
    this.name = name;
  }

  @Override
  public String getProcessName() {
    return name;
  }
}
