package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresEmisionBo;
import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableInsertaValoresSibbacLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private ValoresEmisionBo valoresEmisionBo;

  private List<MaestroValores> maestroValores;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    valoresEmisionBo.saveBulkMaestroValores(maestroValores);
    return true;
  }

  public List<MaestroValores> getMaestroValores() {
    return maestroValores;
  }

  public void setMaestroValores(List<MaestroValores> maestroValores) {
    this.maestroValores = maestroValores;
  }
}
