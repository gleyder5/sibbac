package sibbac.business.dwh.ficheros.nacional.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * DTO para trabajar con las reglas de Exoneracion
 * 
 */
public class OperacionNacionalDTO implements Serializable {

  private static int OPERACION_NUORDEN = 0;
  private static int OPERACION_NBOOKING = 1;
  private static int OPERACION_NUCNFCLT = 2;
  private static int OPERACION_NUCNFLIQ = 3;
  private static int OPERACION_CDCANAL = 4;
  private static int OPERACION_CDORIGEN = 5;
  private static int OPERACION_CDENTLIQ = 6;

  private static int OPERACION_NUTITLIQ = 7;
  private static int OPERACION_FEEJELIQ = 8;
  private static int OPERACION_CDALIAS = 9;
  private static int OPERACION_IMCOMBCO = 10;
  private static int OPERACION_IMCOMBRK = 11;
  private static int OPERACION_IMCOMDVO = 12;
  private static int OPERACION_IMCOMSIS = 13;
  private static int OPERACION_IMCOMSVB = 14;
  private static int OPERACION_CANONCONTRPAGOCLTE = 15;
  private static int OPERACION_CDISIN = 16;
  private static int OPERACION_CDTPOPER = 17;
  private static int OPERACION_STLEV4LM = 18;
  private static int OPERACION_IMBANSIS = 19;
  private static int OPERACION_CONT_REGISTRO = 20;

  /**
   * 
   */
  private static final long serialVersionUID = -6336295126742722529L;

  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private BigDecimal nucnfliq;
  private String cdCanal;
  private String cdOrigen;
  private String cdentliq;
  private BigDecimal nutitliq;
  private Date feejeliq;
  private String cdAlias;
  private BigDecimal imcombco;
  private BigDecimal imcombrk;
  private BigDecimal imcomdvo;
  private BigDecimal imcomsis;
  private BigDecimal imcomsvb;
  private Integer canoncontrpagoclte;
  private String cdIsin;
  private char cdtpoper;
  private String stlev4lm;
  private BigDecimal imbansis;
  private BigInteger contRegistro;

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public BigDecimal getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @param operac the nucnfliq to set
   */
  public void setNucnfliq(BigDecimal operac) {
    this.nucnfliq = operac;
  }

  /**
   * @return the cdCanal
   */
  public String getCdCanal() {
    return cdCanal;
  }

  /**
   * @param cdCanal the cdCanal to set
   */
  public void setCdCanal(String cdCanal) {
    this.cdCanal = cdCanal;
  }

  /**
   * @return the cdOrigen
   */
  public String getCdOrigen() {
    return cdOrigen;
  }

  /**
   * @param cdOrigen the cdOrigen to set
   */
  public void setCdOrigen(String cdOrigen) {
    this.cdOrigen = cdOrigen;
  }

  /**
   * @return the cdentliq
   */
  public String getCdentliq() {
    return cdentliq;
  }

  /**
   * @param cdentliq the cdentliq to set
   */
  public void setCdentliq(String cdentliq) {
    this.cdentliq = cdentliq;
  }

  /**
   * @return the nutitliq
   */
  public BigDecimal getNutitliq() {
    return nutitliq;
  }

  /**
   * @param nutitliq the nutitliq to set
   */
  public void setNutitliq(BigDecimal nutitliq) {
    this.nutitliq = nutitliq;
  }

  /**
   * @return the feejeliq
   */
  public Date getFeejeliq() {
    return (Date) feejeliq.clone();
  }

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = (Date) feejeliq.clone();
  }

  /**
   * @return the cdAlias
   */
  public String getCdAlias() {
    return cdAlias;
  }

  /**
   * @param cdAlias the cdAlias to set
   */
  public void setCdAlias(String cdAlias) {
    this.cdAlias = cdAlias;
  }

  /**
   * @return the imcombco
   */
  public BigDecimal getImcombco() {
    return imcombco;
  }

  /**
   * @param imcombco the imcombco to set
   */
  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  /**
   * @return the imcombrk
   */
  public BigDecimal getImcombrk() {
    return imcombrk;
  }

  /**
   * @param imcombrk the imcombrk to set
   */
  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  /**
   * @return the imcomdvo
   */
  public BigDecimal getImcomdvo() {
    return imcomdvo;
  }

  /**
   * @param imcomdvo the imcomdvo to set
   */
  public void setImcomdvo(BigDecimal imcomdvo) {
    this.imcomdvo = imcomdvo;
  }

  /**
   * @return the imcomsis
   */
  public BigDecimal getImcomsis() {
    return imcomsis;
  }

  /**
   * @param imcomsis the imcomsis to set
   */
  public void setImcomsis(BigDecimal imcomsis) {
    this.imcomsis = imcomsis;
  }

  /**
   * @return the imcomsvb
   */
  public BigDecimal getImcomsvb() {
    return imcomsvb;
  }

  /**
   * @param imcomsvb the imcomsvb to set
   */
  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  /**
   * @return the canoncontrpagoclte
   */
  public Integer getCanoncontrpagoclte() {
    return canoncontrpagoclte;
  }

  /**
   * @param canoncontrpagoclte the canoncontrpagoclte to set
   */
  public void setCanoncontrpagoclte(Integer canoncontrpagoclte) {
    this.canoncontrpagoclte = canoncontrpagoclte;
  }

  /**
   * @return the cdIsin
   */
  public String getCdIsin() {
    return cdIsin;
  }

  /**
   * @param cdIsin the cdIsin to set
   */
  public void setCdIsin(String cdIsin) {
    this.cdIsin = cdIsin;
  }

  /**
   * @return the cdtpoper
   */
  public char getCdtpoper() {
    return cdtpoper;
  }

  /**
   * @param cdtpoper the cdtpoper to set
   */
  public void setCdtpoper(char cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  /**
   * @return the stlev4lm
   */
  public String getStlev4lm() {
    return stlev4lm;
  }

  /**
   * @param stlev4lm the stlev4lm to set
   */
  public void setStlev4lm(String stlev4lm) {
    this.stlev4lm = stlev4lm;
  }

  /**
   * @return the imbamsis
   */
  public BigDecimal getImbansis() {
    return imbansis;
  }

  /**
   * @param imbamsis the imbamsis to set
   */
  public void setImbansis(BigDecimal imbansis) {
    this.imbansis = imbansis;
  }

  /**
   * @return the contRegistro
   */
  public BigInteger getContRegistro() {
    return contRegistro;
  }

  /**
   * @param operac the contRegistro to set
   */
  public void setContRegistro(BigInteger operac) {
    this.contRegistro = operac;
  }

  /**
   * @return
   */
  public static OperacionNacionalDTO mapObject(Object[] operac) {
    OperacionNacionalDTO operacion = new OperacionNacionalDTO();
    operacion.setNuorden((String) operac[OPERACION_NUORDEN]);
    operacion.setNbooking((String) operac[OPERACION_NBOOKING]);
    operacion.setNucnfclt((String) operac[OPERACION_NUCNFCLT]);
    operacion.setNucnfliq((BigDecimal) operac[OPERACION_NUCNFLIQ]);
    operacion.setCdCanal((String) operac[OPERACION_CDCANAL]);
    operacion.setCdOrigen((String) operac[OPERACION_CDORIGEN]);
    operacion.setCdentliq((String) operac[OPERACION_CDENTLIQ]);
    operacion.setNutitliq((BigDecimal) operac[OPERACION_NUTITLIQ]);
    operacion.setFeejeliq((Date) operac[OPERACION_FEEJELIQ]);
    operacion.setCdAlias((String) operac[OPERACION_CDALIAS]);
    operacion.setImcombco((BigDecimal) operac[OPERACION_IMCOMBCO]);
    operacion.setImcombrk((BigDecimal) operac[OPERACION_IMCOMBRK]);
    operacion.setImcomdvo((BigDecimal) operac[OPERACION_IMCOMDVO]);
    operacion.setImcomsis((BigDecimal) operac[OPERACION_IMCOMSIS]);
    operacion.setImcomsvb((BigDecimal) operac[OPERACION_IMCOMSVB]);
    operacion.setCanoncontrpagoclte((Integer) operac[OPERACION_CANONCONTRPAGOCLTE]);
    operacion.setCdIsin((String) operac[OPERACION_CDISIN]);
    operacion.setCdtpoper((char) operac[OPERACION_CDTPOPER]);
    operacion.setStlev4lm((String) operac[OPERACION_STLEV4LM]);
    operacion.setImbansis((BigDecimal) operac[OPERACION_IMBANSIS]);
    operacion.setContRegistro((BigInteger) operac[OPERACION_CONT_REGISTRO]);
    return operacion;
  }
}
