package sibbac.business.dwh.ficheros.valores.bo;

import java.util.List;

import sibbac.business.dwh.ficheros.valores.dto.AbstractDTO;
import sibbac.common.CallableResultDTO;

public interface BaseBo<T extends AbstractDTO> {

  public CallableResultDTO actionFindById(T solicitud);

  public CallableResultDTO actionSave(T solicitud);

  public CallableResultDTO actionUpdate(T solicitud);

  public CallableResultDTO actionDelete(List<String> solicitud);

}
