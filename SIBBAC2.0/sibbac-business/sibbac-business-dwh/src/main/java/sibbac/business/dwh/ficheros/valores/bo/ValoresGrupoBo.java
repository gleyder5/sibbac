package sibbac.business.dwh.ficheros.valores.bo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.ValoresGrupoDao;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresGrupo;
import sibbac.business.dwh.ficheros.valores.dto.ValoresGrupoDTO;
import sibbac.database.bo.AbstractBo;

/**
 * The Class ValoresGrupoBo.
 */
@Service
public class ValoresGrupoBo extends AbstractBo<ValoresGrupo, String, ValoresGrupoDao> {

  public List<ValoresGrupoDTO> getCdgrisivAndNbgrisibv() {
    List<Object[]> cdgrisivAndNbgrisibv = dao.getCdgrisivAndNbgrisibv();

    List<ValoresGrupoDTO> auxList = new ArrayList<>();
    Iterator<Object[]> it = cdgrisivAndNbgrisibv.iterator();

    while (it.hasNext()) {
      Object[] next = it.next();
      ValoresGrupoDTO aux = new ValoresGrupoDTO();
      aux.setCdSibe(next[0].toString());
      aux.setDescSibe(next[1].toString());
      auxList.add(aux);
    }

    return auxList;
  }

}
