package sibbac.business.dwh.ficheros.tradersaccounters.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.tradersaccounters.db.dao.FicheroTradersDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateFicheroTraders")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenerateFicheroTraders extends AbstractGenerateFile {

  private static final String FILE_TRADERS_LINE_SEPARATOR = "\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateFicheroTraders.class);

  /** Lógica a realizar sobre la BBDD. */
  @Autowired
  private FicheroTradersDao ficheroTradersDao;

  @Override
  public String getLineSeparator() {
    return FILE_TRADERS_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return ficheroTradersDao.findAllTraders(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return ficheroTradersDao.countAllTraders();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    final StringBuilder rowLine = new StringBuilder();

    final String msgProductoCuenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_8.value(),
        DwhConstantes.TRADERS_9);
    final String msgNombre = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_50.value(),
        DwhConstantes.TRADERS_DEFINIR);
    final String msgNombreCorto = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(),
        DwhConstantes.TRADERS_X);

    rowLine.append(msgProductoCuenta).append(msgNombre).append(msgNombreCorto);

    String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

    writeLine(outWriter, cadena);
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Se inicia la escritura en Trades file");
    }
    for (Object[] objects : fileData) {
      StringBuilder rowLine = new StringBuilder();
      for (int i = 0; i < objects.length; i++) {
        if (objects[i] == null) {
          objects[i] = "";
        }
      }

      String productoCuenta = ((String) objects[0]).trim();
      if (!objects[1].equals("")) {
        BigDecimal cuenta = (BigDecimal) objects[1];
        productoCuenta += String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
            cuenta.toBigInteger().intValue());
      }
      String nombreCorto = (String) objects[2];
      String nombre = (String) objects[3];

      String msgProductoCuenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_8.value(), productoCuenta);
      String msgNombre = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_50.value(), nombre);
      String msgNombreCorto = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(), nombreCorto);

      rowLine.append(msgProductoCuenta).append(msgNombre).append(msgNombreCorto);

      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);

    }
    if (LOG.isDebugEnabled()) {
      LOG.debug("Finaliza la escritura en Trades file");
    }
  }

}
