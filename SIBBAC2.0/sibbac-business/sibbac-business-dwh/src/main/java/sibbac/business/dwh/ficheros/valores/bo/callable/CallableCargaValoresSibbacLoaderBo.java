package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.db.dao.CargaValoresDao;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableCargaValoresSibbacLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private CargaValoresDao cargaValoresDao;

  private Integer idTipoProducto;

  private List<String> codigoValor;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    cargaValoresDao.updateValorCargaSibbacBulk(idTipoProducto, codigoValor);
    return true;
  }

  public Integer getIdTipoProducto() {
    return idTipoProducto;
  }

  public void setIdTipoProducto(Integer idTipoProducto) {
    this.idTipoProducto = idTipoProducto;
  }

  public List<String> getCodigoValor() {
    return codigoValor;
  }

  public void setCodigoValor(List<String> codigoValor) {
    this.codigoValor = codigoValor;
  }

}
