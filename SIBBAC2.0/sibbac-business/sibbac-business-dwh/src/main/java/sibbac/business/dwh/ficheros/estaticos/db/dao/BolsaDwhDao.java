package sibbac.business.dwh.ficheros.estaticos.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.dwh.ficheros.estaticos.db.model.BolsaDwh;

public interface BolsaDwhDao extends JpaRepository<BolsaDwh, String> {

  @Query(value = "SELECT ID_BOLSA, NOMBRE_BOLSA, CODIGO_PAIS FROM TMCT0_DWH_BOLSA WHERE FECHA_BAJA > CURRENT_DATE OR FECHA_BAJA IS NULL", nativeQuery = true)
  public List<Object[]> findActiveBolsas();

}