package sibbac.business.dwh.ficheros.nacional.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_DWH_EJECUCIONES")
public class Tmct0DwhEjecuciones implements java.io.Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1705795207942827578L;

  /** The id. */
  @Id
  @Column(name = "IDEJECU", length = 10, nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private BigInteger idEjecu;

  @Column(name = "CDCLIEST", length = 8, nullable = false)
  private Integer cdCliest;

  @Column(name = "CDISINVV", length = 12, nullable = false)
  private String cdIsinvv;

  @Column(name = "FHAUDIT", nullable = false)
  private Timestamp fhAudit;

  @Column(name = "FHEJECUC", nullable = false)
  private Date fhEjecu;

  @Column(name = "NUOPROUT", length = 15, nullable = false)
  private String nuOprout;

  @Column(name = "NUPAREJE", length = 4, nullable = false)
  private String nuPareje;

  @Column(name = "USUAUDIT", length = 20, nullable = false)
  private String usAudit;

  public BigInteger getIdEjecu() {
    return idEjecu;
  }

  public void setIdEjecu(BigInteger idEjecu) {
    this.idEjecu = idEjecu;
  }

  public Integer getCdCliest() {
    return cdCliest;
  }

  public void setCdCliest(Integer cdCliest) {
    this.cdCliest = cdCliest;
  }

  public String getCdIsinvv() {
    return cdIsinvv;
  }

  public void setCdIsinvv(String cdIsinvv) {
    this.cdIsinvv = cdIsinvv;
  }

  public Timestamp getFhAudit() {
    return fhAudit != null ? new Timestamp(fhAudit.getTime()) : null;
  }

  public void setFhAudit(Timestamp fhAudit) {
    this.fhAudit = fhAudit != null ? new Timestamp(fhAudit.getTime()) : null;
  }

  public Date getFhEjecu() {
    return fhEjecu != null ? new Date(fhEjecu.getTime()) : null;
  }

  public void setFhEjecu(Date fhEjecu) {
    this.fhEjecu = fhEjecu;
  }

  public String getNuOprout() {
    return nuOprout;
  }

  public void setNuOprout(String nuOprout) {
    this.nuOprout = nuOprout;
  }

  public String getNuPareje() {
    return nuPareje;
  }

  public void setNuPareje(String nuPareje) {
    this.nuPareje = nuPareje;
  }

  public String getUsAudit() {
    return usAudit;
  }

  public void setUsAudit(String usAudit) {
    this.usAudit = usAudit;
  }

  public Tmct0DwhEjecuciones() {
    super();
  }

}
