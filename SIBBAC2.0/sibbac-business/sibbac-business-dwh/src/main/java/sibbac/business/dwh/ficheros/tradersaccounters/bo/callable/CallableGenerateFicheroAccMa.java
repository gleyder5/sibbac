package sibbac.business.dwh.ficheros.tradersaccounters.bo.callable;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableProcessFileResult;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.tradersaccounters.bo.GenerateFicheroAccMa;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGenerateFicheroAccMa implements CallableProcessFileResult {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGenerateFicheroAccMa.class);

  @Autowired
  private GenerateFicheroAccMa generateFicheroAccMa;

  private Future<ProcessFileResultDTO> future;

  /** Metódo que se ejecutará en el hilo */
  @Override
  public ProcessFileResultDTO call() throws Exception {
    final ProcessFileResultDTO processFileResultDTO = new ProcessFileResultDTO();

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza el proceso en background de Account Management File");
    }

    generateFicheroAccMa.generateFile();

    processFileResultDTO.setFileNameProcess(generateFicheroAccMa.getFileName());
    processFileResultDTO.setFileResult(generateFicheroAccMa.getFileGenerationResult());

    return processFileResultDTO;
  }

  public GenerateFicheroAccMa getGenerateFicheroAccMa() {
    return generateFicheroAccMa;
  }

  public void setGenerateFicheroAccMa(GenerateFicheroAccMa generateFicheroAccMa) {
    this.generateFicheroAccMa = generateFicheroAccMa;
  }

  @Override
  public void setFuture(Future<ProcessFileResultDTO> future) {
    this.future = future;
  }

  @Override
  public Future<ProcessFileResultDTO> getFuture() {
    return future;
  }
}
