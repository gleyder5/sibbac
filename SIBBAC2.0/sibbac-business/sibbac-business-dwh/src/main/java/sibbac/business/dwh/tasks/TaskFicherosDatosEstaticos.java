package sibbac.business.dwh.tasks;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.estaticos.bo.FicherosDatosEstaticosBo;
import sibbac.business.dwh.tasks.commons.TaskProcessDWH;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.constants.FileResult;
import sibbac.common.utils.DateHelper;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_FICHERO_DATOS_ESTATICOS, interval = 1, intervalUnit = IntervalUnit.DAY, startTime = "04:00:00")
public class TaskFicherosDatosEstaticos extends WrapperTaskConcurrencyPrevent implements TaskProcessDWH {

  private static final Logger LOG = LoggerFactory.getLogger(TaskFicherosDatosEstaticos.class);

  @Autowired
  private FicherosDatosEstaticosBo ficheroBo;

  /**
   * Listado de resultados de la generacion de ficheros
   */
  private List<ProcessFileResultDTO> processFileResultsList;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    this.launchProcess();
  }

  @Override
  public void launchProcess() throws SIBBACBusinessException {
    processFileResultsList = new ArrayList<>();

    final long initStartTime = System.currentTimeMillis();

    processFileResultsList.addAll(launchDatosEstaticos());

    if (LOG.isDebugEnabled()) {
      LOG.debug(
          String.format("El proceso ha terminado, se ha ejecutado en %d", System.currentTimeMillis() - initStartTime));
    }
  }

  public List<ProcessFileResultDTO> launchDatosEstaticos() throws SIBBACBusinessException {
    final List<ProcessFileResultDTO> result = new ArrayList<>();
    Calendar cal = Calendar.getInstance();
    if (!(DateHelper.isFechaFinDeSemana(cal))) {

      FileResult fileResult = null;
      ProcessFileResultDTO processFileResultDTO = null;

      // Generacion de Fichero de Bolsas
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      fileResult = ficheroBo.generateFileBolsas();
      processFileResultDTO.setFileNameProcess(ficheroBo.getFileNameDwhBolsas());
      processFileResultDTO.setFileResult(fileResult);
      result.add(processFileResultDTO);

      // Generacion de Fichero de Brokers
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      fileResult = ficheroBo.generateFileBrokers();
      processFileResultDTO.setFileNameProcess(ficheroBo.getFileNameDwhBrokers());
      processFileResultDTO.setFileResult(fileResult);
      result.add(processFileResultDTO);

      // Generacion de Fichero de Entidades
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      fileResult = ficheroBo.generateFileEntidades();
      processFileResultDTO.setFileNameProcess(ficheroBo.getFileNameDwhEntidades());
      processFileResultDTO.setFileResult(fileResult);
      result.add(processFileResultDTO);

    }
    return result;
  }

  @Override
  public List<ProcessFileResultDTO> getProcessFileResultsList() {
    return processFileResultsList;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_FICHERO_DATOS_ESTATICOS;
  }
}
