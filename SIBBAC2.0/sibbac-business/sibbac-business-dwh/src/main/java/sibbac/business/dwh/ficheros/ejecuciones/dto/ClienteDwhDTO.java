package sibbac.business.dwh.ficheros.ejecuciones.dto;

import java.io.Serializable;

/**
 * DTO para trabajar con las reglas de Exoneracion
 * 
 */
public class ClienteDwhDTO implements Serializable {

  private static int CLIENTE_NBCLIENT = 0;
  private static int CLIENTE_NBCLIENT1 = 1;
  private static int CLIENTE_NBCLIENT2 = 2;

  /**
   * 
   */
  private static final long serialVersionUID = -6336295126742722529L;

  private String nbclient;
  private String nbclient1;
  private String nbclient2;

  /**
   * @return the nbclient
   */
  public String getNbclient() {
    return nbclient;
  }

  /**
   * @param nbclient the nbclient to set
   */
  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  /**
   * @return the nbclient1
   */
  public String getNbclient1() {
    return nbclient1;
  }

  /**
   * @param nbclient1 the nbclient1 to set
   */
  public void setNbclient1(String nbclient1) {
    this.nbclient1 = nbclient1;
  }

  /**
   * @return the nbclient2
   */
  public String getNbclient2() {
    return nbclient2;
  }

  /**
   * @param nbclient2 the nbclient2 to set
   */
  public void setNbclient2(String nbclient2) {
    this.nbclient2 = nbclient2;
  }

  /**
   * @param cli
   * @return
   */
  public static ClienteDwhDTO initialize() {
    ClienteDwhDTO cliente = new ClienteDwhDTO();
    cliente.setNbclient("");
    cliente.setNbclient1("");
    cliente.setNbclient2("");
    return cliente;
  }
  
  /**
   * @return
   */
  public static ClienteDwhDTO mapObject(Object[] cli) {
    ClienteDwhDTO cliente = new ClienteDwhDTO();
    cliente.setNbclient(String.valueOf(cli[CLIENTE_NBCLIENT]));
    cliente.setNbclient1(String.valueOf(cli[CLIENTE_NBCLIENT1]));
    cliente.setNbclient2(String.valueOf(cli[CLIENTE_NBCLIENT2]));
    return cliente;
  }
}
