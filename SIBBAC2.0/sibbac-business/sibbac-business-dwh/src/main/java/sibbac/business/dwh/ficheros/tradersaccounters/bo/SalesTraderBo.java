package sibbac.business.dwh.ficheros.tradersaccounters.bo;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.tradersaccounters.db.dao.Tmct0SalesTraderDao;
import sibbac.business.dwh.ficheros.tradersaccounters.db.model.Tmct0SalesTrader;
import sibbac.business.dwh.ficheros.valores.bo.BaseBo;
import sibbac.business.dwh.ficheros.valores.bo.PaisBo;
import sibbac.business.dwh.ficheros.valores.dto.Tmct0SalesTraderDTO;
import sibbac.business.dwh.utils.ErrorFinalHolderEnum;
import sibbac.business.dwh.utils.FinalHoldersValidateNIF;
import sibbac.business.wrappers.database.dao.CodigoErrorDao;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums.ApplicationMessages;
import sibbac.common.utils.SibbacEnums.LoggingMessages;
import sibbac.common.utils.SibbacEnums.TradersSalesMessages;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class SalesTraderBo.
 */
@Service("salesTraderBo")
public class SalesTraderBo extends AbstractBo<Tmct0SalesTrader, String, Tmct0SalesTraderDao>
    implements BaseBo<Tmct0SalesTraderDTO> {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(SalesTraderBo.class);

  private static final String DUPLICATED_ID = "Usuario ya registrado";

  /** The session. */
  @Autowired
  private HttpSession session;

  /** The pais bo. */
  @Autowired
  private PaisBo paisBo;

  /** The codigo error dao. */
  @Autowired
  private CodigoErrorDao codigoErrorDao;

  /**
   * Action save.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @Override
  public CallableResultDTO actionSave(Tmct0SalesTraderDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    final CallableResultDTO result = callableResultDTO;

    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Comienza el proceso 'actionSave'");
      }
      if (solicitud != null) {
        if (dao.findOne(solicitud.getCdempleado()) != null) {
          throw new SIBBACBusinessException(DUPLICATED_ID);
        }
        if (this.allExists(solicitud)) {

          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Se han validado todos los datos correctamente");
          }

          saveOperations(solicitud, result);
          if (result.getErrorMessage().isEmpty()) {
            saveIsOk(solicitud, result);
          }

        }
        else {
          LOGGER.debug("Se ha dado un error en la validacion de los datos obligatorios");
          throw new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value());
        }
      }
      else {
        LOGGER.debug(ApplicationMessages.UNEXPECTED_MESSAGE.value());
        throw new SIBBACBusinessException(ApplicationMessages.UNEXPECTED_MESSAGE.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error(LoggingMessages.BASIC_VALIDATION_ERROR.value(), e);
      result.addErrorMessage(e.getMessage());
    }
    catch (RuntimeException rex) {
      LOGGER.error(LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    catch (IllegalAccessException iaex) {
      LOGGER.error(LoggingMessages.ILLEGAL_ACCESS_EXCEPTION_MESSAGE.value(), iaex);
      result.addErrorMessage(ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finaliza el proceso 'actionSave'");
    }
    return result;
  }

  /**
   * Save is ok.
   *
   * @param solicitud the solicitud
   * @param result the result
   */
  private void saveIsOk(Tmct0SalesTraderDTO solicitud, final CallableResultDTO result) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Se mapea el objeto a guardar");
    }
    DozerBeanMapper mapper = new DozerBeanMapper();

    Tmct0SalesTrader mapEntity = mapper.map(solicitud, Tmct0SalesTrader.class);

    if (LOGGER.isDebugEnabled()) {
      String entityMsg = mapEntity.toString();
      LOGGER.debug(MessageFormat.format("Guardando [{0}] en BBDD", entityMsg));
    }
    dao.save(mapEntity);

    LOGGER.debug("Registro insertado correctamente");
    result.addMessage(ApplicationMessages.ADDED_DATA.value());
  }

  /**
   * Save operations.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void saveOperations(Tmct0SalesTraderDTO solicitud, final CallableResultDTO result)
      throws SIBBACBusinessException {
    List<Object> validCountry = paisBo.isValidCountry(solicitud.getCdnacionalidad());
    String country;
    if (validCountry.isEmpty()) {
      throw new SIBBACBusinessException(TradersSalesMessages.INVALID_COUNTRY.value());
    }
    else {
      country = (String) validCountry.get(0);
    }
    if (solicitud.getTipoidmifid().equals("N")) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Registro con Tipo ID MIFID igual a 'N'");
      }
      saveNType(solicitud, result, country);
    }
    else if (solicitud.getTipoidmifid().equals("C")) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Registro con Tipo ID MIFID igual a 'C'");
      }
      saveCType(solicitud, result, country);
    }
    else if (solicitud.getTipoidmifid().equals("P")) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Registro con Tipo ID MIFID igual a 'P'");
      }
      executeSaveAction(solicitud);
    }
    else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("No se ha seleccionado un tipo mifid correcto");
      }
      result.addErrorMessage("- No se ha seleccionado un tipo mifid correcto. ");
    }

  }

  /**
   * Save C type.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @param country the valid country
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void saveCType(Tmct0SalesTraderDTO solicitud, final CallableResultDTO result, String country)
      throws SIBBACBusinessException {
    Boolean validateConcat = this.validateConcat(solicitud, country);

    if (validateConcat) {
      executeSaveAction(solicitud);
    }
    else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(TradersSalesMessages.INVALID_IDMIFID.value());
      }
      result.addErrorMessage(TradersSalesMessages.INVALID_IDMIFID.value());
    }
  }

  /**
   * Save N type.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @param country the valid country
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void saveNType(Tmct0SalesTraderDTO solicitud, final CallableResultDTO result, String country)
      throws SIBBACBusinessException {
    List<String> errList = new ArrayList<>();
    if (country.length() > solicitud.getIdmifid().length()
        || !country.equals(solicitud.getIdmifid().substring(0, country.length()))) {
      throw new SIBBACBusinessException(TradersSalesMessages.INVALID_COUNTRY_MIFID.value());
    }
    if (country.equals("ES")) {
      validateNif(solicitud, errList);
      if (errList.isEmpty()) {
        executeSaveAction(solicitud);
      }
      else {
        List<CodigoErrorData> coList = codigoErrorDao.findByCodigoOrderByCodigoDataAsc(errList);
        for (CodigoErrorData error : coList) {
          result.addErrorMessage("- " + error.getDescripcion() + ". ");
        }
      }
    }
    else {
      executeSaveAction(solicitud);
    }
  }

  /**
   * Execute save action.
   *
   * @param solicitud the solicitud
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void executeSaveAction(Tmct0SalesTraderDTO solicitud) throws SIBBACBusinessException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Se procede a completar los campos restantes a guardar");
    }
    solicitud.setCuenta(dao.getCuenta());
    solicitud.setProducto("008");
    try {
      solicitud.setCodigocorto(Integer.parseInt(solicitud.getCdempleado().substring(1).trim()));
    }
    catch (NumberFormatException parse) {
      throw new SIBBACBusinessException(TradersSalesMessages.CDEMPLEADO_INCORRECT.value());
    }
    Object attribute = session.getAttribute(LoggingMessages.USER_SESSION.value());
    String name = ((UserSession) attribute).getName();
    String user = StringUtils.substring(name, 0, 10);
    solicitud.setCdusuaud(user);

    Calendar c = Calendar.getInstance();
    solicitud.setFhinicio(c.getTime());

    c.set(9999, 11, 31);

    solicitud.setFhfinal(c.getTime());

    long time = new Date().getTime();
    Timestamp fhaudit = new Timestamp(time);
    solicitud.setFhaudit(fhaudit);
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Se procede a completar los campos restantes a guardar");
    }

  }

  /**
   * Validate concat.
   *
   * @param solicitud the solicitud
   * @param country the country
   * @return true, if successful
   */
  private boolean validateConcat(Tmct0SalesTraderDTO solicitud, String country) {
    String idMifid = solicitud.getIdmifid();
    Date fechanacimiento = solicitud.getFechanacimiento();
    String nbnombre = String.format("%-5s", solicitud.getNbnombre()).replace(' ', '#').substring(0, 5);
    String nbapellido1 = String.format("%-5s", solicitud.getNbapellido1()).replace(' ', '#').substring(0, 5);

    Calendar cal = Calendar.getInstance();
    cal.setTime(fechanacimiento);

    // Validacion del pais
    Boolean validationCountry = idMifid.substring(0, 2).equals(country);
    // Validación del año
    Boolean validationYear = idMifid.substring(2, 6).equals(String.valueOf(cal.get(Calendar.YEAR)));
    // Validacion del mes
    Boolean validationMonth = idMifid.substring(6, 8).equals(String.format("%02d", cal.get(Calendar.MONTH) + 1));
    // Validacion del dia
    Boolean validationDay = idMifid.substring(8, 10).equals(String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)));
    // Validacion del nombre
    Boolean validationName = idMifid.substring(10, 15).equals(nbnombre);
    // Validación del apellido
    Boolean validationSubname = idMifid.substring(15, 20).equals(nbapellido1);

    return validationCountry && validationDay && validationMonth && validationName && validationSubname
        && validationYear;
  }

  /**
   * Validate nif.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void validateNif(Tmct0SalesTraderDTO solicitud, List<String> result) throws SIBBACBusinessException {
    // Validate nif
    if (solicitud.getIdmifid() == null || "".equals(solicitud.getIdmifid().trim())) {
      result.add(ErrorFinalHolderEnum.ERROR_05.getError());
    }
    else {
      FinalHoldersValidateNIF validatorNIF = new FinalHoldersValidateNIF();
      validatorNIF.setNudnicif(solicitud.getIdmifid().substring(2));
      validatorNIF.setResult(result);
      validatorNIF.validate();
    }
  }

  @Override
  public CallableResultDTO actionDelete(List<String> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Comienza el proceso 'actionDelete'");
      }

      for (String solicitud : solicitudList) {
        if (solicitud != null) {
          Tmct0SalesTrader entity = this.findById(solicitud);
          if (entity != null) {

            UserSession userSession = (UserSession) session.getAttribute(LoggingMessages.USER_SESSION.value());
            String userSessionSubstring = StringUtils.substring(userSession.getName(), 0, 10);
            entity.setCdusuaud(userSessionSubstring);

            long time = new Date().getTime();
            Timestamp fhaudit = new Timestamp(time);
            entity.setFhaudit(fhaudit);
            entity.setFhfinal(new Date());

            if (LOGGER.isDebugEnabled()) {
              String entityMsg = entity.toString();
              LOGGER.debug(MessageFormat.format("Eliminado [{0}] en BBDD", entityMsg));
            }
            dao.save(entity);

          }
          LOGGER.debug("Registro(s) borrado(s) correctamente");
          result.addMessage(ApplicationMessages.DELETED_DATA.value());
        }
      }
      if (solicitudList.isEmpty()) {
        LOGGER.error("Error inesperado, consulte con administrador",
            new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value()));
        throw new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error(LoggingMessages.MULTIPLE_ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(MessageFormat.format(ApplicationMessages.MULTIPLE_NOT_EXIST.value(), err.toString()));
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finaliza el proceso 'actionDelete'");
    }
    return result;
  }

  @Override
  public CallableResultDTO actionFindById(Tmct0SalesTraderDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO actionUpdate(Tmct0SalesTraderDTO solicitud) {
    CallableResultDTO result = new CallableResultDTO();
    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Comienza el proceso 'actionUpdate'");
      }
      if (solicitud != null) {
        Tmct0SalesTrader entitydb = dao.findOne(solicitud.getCdempleado());

        if (entitydb != null) {
          modifyOperations(solicitud, result, entitydb);
        }
        else {
          LOGGER.error("La primarykey del objeto mandado no existe",
              new SIBBACBusinessException(LoggingMessages.ID_OBJECT_NOT_FIND.value()));
          throw new SIBBACBusinessException(LoggingMessages.ID_OBJECT_NOT_FIND.value());
        }
      }
      if (solicitud == null) {
        LOGGER.error("Solicitud vacia", new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value()));
        throw new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value());
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Finaliza el proceso 'actionUpdate'");
      }
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error(LoggingMessages.BASIC_VALIDATION_ERROR.value(), e);
      result.addErrorMessage(e.getMessage());
    }
    catch (IllegalAccessException iaex) {
      LOGGER.error(LoggingMessages.ILLEGAL_ACCESS_EXCEPTION_MESSAGE.value(), iaex);
      result.addErrorMessage(ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  /**
   * Modify operations.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @param entitydb the entitydb
   * @throws SIBBACBusinessException the SIBBAC business exception
   * @throws IllegalAccessException the illegal access exception
   */
  private void modifyOperations(Tmct0SalesTraderDTO solicitud, CallableResultDTO result, Tmct0SalesTrader entitydb)
      throws SIBBACBusinessException, IllegalAccessException {
    List<Object> validCountry = paisBo.isValidCountry(solicitud.getCdnacionalidad());
    String country;
    if (validCountry.isEmpty()) {
      throw new SIBBACBusinessException(TradersSalesMessages.INVALID_COUNTRY.value());
    }
    else {
      country = (String) validCountry.get(0);
    }
    if (solicitud.getTipoidmifid().equals("N")) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Registro con Tipo ID MIFID igual a 'N'");
      }
      editNType(solicitud, result, entitydb, country);
    }
    else if (solicitud.getTipoidmifid().equals("C")) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Registro con Tipo ID MIFID igual a 'C'");
      }
      editCType(solicitud, result, entitydb, country);
    }
    else if (solicitud.getTipoidmifid().equals("P")) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Registro con Tipo ID MIFID igual a 'P'");
      }
      this.modifyProcess(solicitud, result, entitydb);
    }
    else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("No se ha seleccionado un tipo mifid correcto");
      }
      result.addErrorMessage("- No se ha seleccionado un tipo mifid correcto. ");
    }
  }

  /**
   * Edits the C type.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @param entitydb the entitydb
   * @param country the country
   * @throws IllegalAccessException the illegal access exception
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void editCType(Tmct0SalesTraderDTO solicitud, CallableResultDTO result, Tmct0SalesTrader entitydb,
      String country) throws IllegalAccessException, SIBBACBusinessException {
    Boolean validateConcat = this.validateConcat(solicitud, country);

    if (validateConcat) {
      this.modifyProcess(solicitud, result, entitydb);
    }
    else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(TradersSalesMessages.INVALID_IDMIFID.value());
      }
      result.addErrorMessage(TradersSalesMessages.INVALID_IDMIFID.value());
    }
  }

  /**
   * Edits the N type.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @param entitydb the entitydb
   * @param country the country
   * @throws SIBBACBusinessException the SIBBAC business exception
   * @throws IllegalAccessException the illegal access exception
   */
  private void editNType(Tmct0SalesTraderDTO solicitud, CallableResultDTO result, Tmct0SalesTrader entitydb,
      String country) throws SIBBACBusinessException, IllegalAccessException {
    List<String> errList = new ArrayList<>();
    if (country.equals(solicitud.getIdmifid().substring(0, country.length()))) {
      throw new SIBBACBusinessException(TradersSalesMessages.INVALID_COUNTRY_MIFID.value());
    }
    if (country.equals("ES")) {
      validateNif(solicitud, errList);
      if (errList.isEmpty()) {
        this.modifyProcess(solicitud, result, entitydb);
      }
      else {
        List<CodigoErrorData> coList = codigoErrorDao.findByCodigoOrderByCodigoDataAsc(errList);
        for (CodigoErrorData error : coList) {
          result.addErrorMessage("- " + error.getDescripcion() + ". ");
        }
      }
    }
    else {
      this.modifyProcess(solicitud, result, entitydb);
    }
  }

  /**
   * Modify process.
   *
   * @param solicitud the solicitud
   * @param result the result
   * @param entitydb the entitydb
   * @throws IllegalAccessException the illegal access exception
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  private void modifyProcess(Tmct0SalesTraderDTO solicitud, CallableResultDTO result, Tmct0SalesTrader entitydb)
      throws IllegalAccessException, SIBBACBusinessException {
    if (this.allExists(solicitud)) {

      DozerBeanMapper mapper = new DozerBeanMapper();

      Object attribute = session.getAttribute(LoggingMessages.USER_SESSION.value());
      String name = ((UserSession) attribute).getName();
      String user = StringUtils.substring(name, 0, 10);
      solicitud.setCdusuaud(user);

      long time = new Date().getTime();
      Timestamp fhaudit = new Timestamp(time);
      solicitud.setFhaudit(fhaudit);

      Tmct0SalesTrader maestroValoresEntity = mapper.map(solicitud, Tmct0SalesTrader.class);

      if (LOGGER.isDebugEnabled()) {
        String entityMsg = entitydb.toString();
        LOGGER.debug(MessageFormat.format("Modificando [{0}] en BBDD", entityMsg));
      }
      dao.save(maestroValoresEntity);

      LOGGER.debug("Registro modificado correctamente");
      result.addMessage(ApplicationMessages.MODIFIED_DATA.value());

    }
    else {
      LOGGER.debug("Se ha dado un error en la validacion de los datos obligatorios",
          new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value()));
      throw new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value());
    }
  }

  /**
   * All exists.
   *
   * @param solicitud the solicitud
   * @return the boolean
   * @throws IllegalAccessException the illegal access exception
   */
  private Boolean allExists(Tmct0SalesTraderDTO solicitud) throws IllegalAccessException {
    Boolean result = true;
    Field[] fields = Tmct0SalesTraderDTO.class.getDeclaredFields();
    String[] nullField = { "producto", "cuenta", "nbcorto", "indtrader", "indsale", "indtrad_sale", "indaccount",
        "codigocorto", "fhinicio", "fhfinal", "fhaudit", "cdusuaud" };
    List<String> asList = Arrays.asList(nullField);
    for (Field f : fields) {
      f.setAccessible(true);
      if (!asList.contains(f.getName()) && f.get(solicitud) == null) {
        result = false;
      }
    }
    return result;
  }
}
