package sibbac.business.dwh.ficheros.estaticos.db.model;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table( name = "TMCT0_DWH_BROKER")
@Entity
public class BrokerDwh implements java.io.Serializable {
  
  /**
   * 
   */
  private static final long serialVersionUID = 3954473618235757858L;

  @Id
  @Column( name = "ID_BROKER", nullable = false, length = 8)
  private String idBroker;
  
  @Column( name = "NOMBRE_BROKER", nullable = false, length = 50)
  private String nombreBroker;
  
  @Column( name = "TIPO_BROKER", nullable = false, length = 1)
  private String tipoBroker;
  
  @Column( name = "USUARIO_ALTA", nullable = false, length = 256 )
  private String usuarioAlta;
  
  @Column( name = "USUARIO_MOD", nullable = true, length = 256 )
  private String usuarioMod;
  
  @Column( name = "USUARIO_BAJA", nullable = true, length = 256 )
  private String usuariobaja;
  
  @Column( name = "FECHA_ALTA", nullable = false)
  private Timestamp fechaAlta;
  
  @Column( name = "FECHA_BAJA", nullable = false)
  private Timestamp fechaBaja;
  
  @Column( name = "FECHA_MOD", nullable = true)
  private Timestamp fechaMod;


  
  /**
   * @return the idBroker
   */
  public String getIdBroker() {
    return idBroker;
  }

  /**
   * @param idBroker the idBroker to set
   */
  public void setIdBroker(String idBroker) {
    this.idBroker = idBroker;
  }

  /**
   * @return the nombreBroker
   */
  public String getNombreBroker() {
    return nombreBroker;
  }

  /**
   * @param nombreBroker the nombreBroker to set
   */
  public void setNombreBroker(String nombreBroker) {
    this.nombreBroker = nombreBroker;
  }

  /**
   * @return the tipoBroker
   */
  public String getTipoBroker() {
    return tipoBroker;
  }

  /**
   * @param tipoBroker the tipoBroker to set
   */
  public void setTipoBroker(String tipoBroker) {
    this.tipoBroker = tipoBroker;
  }

  /**
   * @return the usuariobaja
   */
  public String getUsuariobaja() {
    return usuariobaja;
  }

  /**
   * @param usuariobaja the usuariobaja to set
   */
  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }

/**
 * @return the usuarioAlta
 */
public String getUsuarioAlta() {
	return usuarioAlta;
}

/**
 * @param usuarioAlta the usuarioAlta to set
 */
public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
}

/**
 * @return the usuarioMod
 */
public String getUsuarioMod() {
	return usuarioMod;
}

/**
 * @param usuarioMod the usuarioMod to set
 */
public void setUsuarioMod(String usuarioMod) {
	this.usuarioMod = usuarioMod;
}

/**
 * @return the fechaAlta
 */
public Timestamp getFechaAlta() {
	return (Timestamp) fechaAlta.clone();
}

/**
 * @param fechaAlta the fechaAlta to set
 */
public void setFechaAlta(Timestamp fechaAlta) {
	this.fechaAlta = (Timestamp) fechaAlta.clone();
}

/**
 * @return the fechaMod
 */
public Timestamp getFechaMod() {
	return (Timestamp) fechaMod.clone();
}

/**
 * @param fechaMod the fechaMod to set
 */
public void setFechaMod(Timestamp fechaBaja) {
	this.fechaMod = (Timestamp) fechaBaja.clone();
}

/**
 * @return the fechaMod
 */
public Timestamp getFechaBaja() {
  return (Timestamp) fechaMod.clone();
}

/**
 * @param fechaMod the fechaMod to set
 */
public void setFechaBaja(Timestamp fechaBaja) {
  this.fechaMod = (Timestamp) fechaBaja.clone();
}

  
}