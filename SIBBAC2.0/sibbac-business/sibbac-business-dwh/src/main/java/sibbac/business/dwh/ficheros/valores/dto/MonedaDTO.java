package sibbac.business.dwh.ficheros.valores.dto;

import java.sql.Timestamp;

/**
 * The Class TMCT0blanqueoControlAnalisisDTO.
 */
public class MonedaDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5270093962321653900L;

  /** The cdmoniso. */
  private String cdmoniso;

  /** The cdmoneda. */
  private String cdmoneda;

  /** The nbmoneda. */
  private String nbmoneda;

  /** The idpaieur. */
  private String idpaieur;

  /** The fhaudit. */
  private Timestamp fhaudit;

  /** The usuaudit. */
  private String usuaudit;

  /**
   * Gets the cdmoniso.
   *
   * @return the cdmoniso
   */
  public String getCdmoniso() {
    return cdmoniso;
  }

  /**
   * Sets the cdmoniso.
   *
   * @param cdmoniso the new cdmoniso
   */
  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  /**
   * Gets the cdmoneda.
   *
   * @return the cdmoneda
   */
  public String getCdmoneda() {
    return cdmoneda;
  }

  /**
   * Sets the cdmoneda.
   *
   * @param cdmoneda the new cdmoneda
   */
  public void setCdmoneda(String cdmoneda) {
    this.cdmoneda = cdmoneda;
  }

  /**
   * Gets the nbmoneda.
   *
   * @return the nbmoneda
   */
  public String getNbmoneda() {
    return nbmoneda;
  }

  /**
   * Sets the nbmoneda.
   *
   * @param nbmoneda the new nbmoneda
   */
  public void setNbmoneda(String nbmoneda) {
    this.nbmoneda = nbmoneda;
  }

  /**
   * Gets the idpaieur.
   *
   * @return the idpaieur
   */
  public String getIdpaieur() {
    return idpaieur;
  }

  /**
   * Sets the idpaieur.
   *
   * @param idpaieur the new idpaieur
   */
  public void setIdpaieur(String idpaieur) {
    this.idpaieur = idpaieur;
  }

  /**
   * Gets the fhaudit.
   *
   * @return the fhaudit
   */
  public Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * Sets the fhaudit.
   *
   * @param fhaudit the new fhaudit
   */
  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * Gets the usuaudit.
   *
   * @return the usuaudit
   */
  public String getUsuaudit() {
    return usuaudit;
  }

  /**
   * Sets the usuaudit.
   *
   * @param usuaudit the new usuaudit
   */
  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "MaestroValoresDTO [cdmoniso=" + this.cdmoniso + ", cdmoneda=" + this.cdmoneda + ", nbmoneda="
        + this.nbmoneda + ", idpaieur=" + this.idpaieur + ", fhaudit=" + this.fhaudit + ", usuaudit=" + this.usuaudit
        + "]";
  }
}
