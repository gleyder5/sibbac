package sibbac.business.dwh.ficheros.nacional.dto;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.OperacionesInternacionalDWHUtils;

public class MinoristaDTO {

  protected static final Logger LOG = LoggerFactory.getLogger(MinoristaDTO.class);

  /**
   * Order: 1 Desc: N�mero Operaci�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal NUOPROUT;

  /**
   * Order: 2 Desc: N�mero Operaci�n Tipo: NUMERIC Length: 11
   */
  private BigDecimal NUOPROU2;

  /**
   * Order: 3 Desc: N�mero Parcial Tipo: NUMERIC Length: 4
   */
  private BigDecimal NUPAREJE;

  /**
   * Order: 4 Desc: C�digo Cliente Tipo: NUMERIC Length: 8
   */
  private BigDecimal CDCLIENT;

  /**
   * Order: 5 Desc: C�digo Entidad Liquidadora Tipo: CHARACTER Length: 4
   */
  private String CDENTLIQ;

  /**
   * Order: 6 Desc: C�digo ISIN Tipo: CHARACTER Length: 12
   */
  private String CDISINVV;

  /**
   * Order: 7 Desc: C�digo Bolsas Tipo: CHARACTER Length: 3
   */
  private String CDBOLSAS;

  /**
   * Order: 8 Desc: Tipo Operaci�n Tipo: CHARACTER Length: 1
   */
  private String TPOPERAC;

  /**
   * Order: 9 Desc: Tipo Operaci�n Bolsa Tipo: CHARACTER Length: 2
   */
  private String TPEJERCI = "CV";

  /**
   * Order: 10 Desc: Tipo Ejecuci�n Tipo: CHARACTER Length: 1
   */
  private String TPEJECUC = "V";

  /**
   * Order: 11 Desc: Tipo Operaci�n Estado Tipo: CHARACTER Length: 1
   */
  private String TPOPESTD = "N";

  /**
   * Order: 12 Desc: Tipo Contrataci�n Tipo: CHARACTER Length: 1
   */
  private String TPCONTRA = "T";

  /**
   * Order: 13 Desc: C�digo Operador Mercado Tipo: CHARACTER Length: 8
   */
  private String CDOPEMER = "99999999";

  /**
   * Order: 14 Desc: Operacion de Routing Tipo: CHARACTER Length: 1
   */
  private String ISROUTIN;

  /**
   * Order: 15 Desc: Clase de mercado Tipo: CHARACTER Length: 1
   */
  private String CDCLSMER = "E";

  /**
   * Order: 16 Desc: Fecha Ejecuci�n Tipo: CHARACTER Length: 8
   */
  private String FHEJECUC;

  /**
   * Order: 17 Desc: Importe Efectivo Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMEFECTI;

  /**
   * Order: 18 Desc: T�tulos ejecutados Tipo: NUMERIC Length: 15
   */
  private BigDecimal NUCANEJE;

  /**
   * Order: 19 Desc: Importe Derechos Gesti�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMDERCOG;

  /**
   * Order: 20 Desc: Importe Derechos Liquidaci�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMDERLIQ;

  /**
   * Order: 21 Desc: Importe Comisi�n SVB Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMCOMSVB;

  /**
   * Order: 22 Desc: Importe Comisi�n Liquidadora Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMCOMLIQ;

  /**
   * Order: 23 Desc: Importe Comisi�n Broker Tipo: NUMERIC Length: 15
   */
  private String IMCOMBRK = "+000000000000000.00";

  /**
   * Order: 24 Desc: Importe Comisi�n UK2 Tipo: NUMERIC Length: 15
   */
  private String IMCOMUK2 = "+000000000000000.00";

  /**
   * Order: 25 Desc: Importe Comisi�n USA Tipo: NUMERIC Length: 15
   */
  private String IMCOMUSA = "+000000000000000.00";

  /**
   * Order: 26 Desc: Importe Comisi�n Devoluci�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMCOMDEV;

  /**
   * Order: 27 Desc: Periodo de Contrataci�n Tipo: NUMERIC Length: 4
   */
  private BigDecimal NUPERIOD;

  /**
   * Order: 28 Desc: Devoluci�n cliente D.A. Securities Tipo: CONSTANT
   */
  private String IMDBRKDA = "+000000000000000.00";

  /**
   * Order: 29 Desc: C�digo Cliente Tipo: NUMERIC Length: 8
   */
  private BigDecimal CDCLIEST;

  /**
   * Order: 30 Desc: Importe Bruto Derechos Gesti�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMBRUCOG;

  /**
   * Order: 31 Desc: Importe Bruto Derechos Liquidaci�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMBRULIQ;

  /**
   * Order: 32 Desc: Importe Comisi�n Banco Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMBRUBAN;

  /**
   * Order: 33 Desc: Comisi�n Banco SISA Tipo: CONSTANT Length: 15
   */
  private String IMBANSIS = "+000000000000000.00";

  /**
   * Order: 34 Desc: C�digo Broker Tipo: NUMERIC Length: 8
   */
  private BigDecimal CDBROKER = new BigDecimal(0);

  /**
   * Order: 35 Desc: Broker del grupo Tipo: CHARACTER Length: 1
   */
  private String TPBROKER = "2";

  /**
   * Order: 36 Desc: Importe Neto Cliente Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMNETCLT;

  /**
   * Order: 37 Desc: Importe Comisi�n Broker que contrata Tipo: NUMERIC Length:
   * 15
   */
  private BigDecimal IMBROCON;

  /**
   * Order: 38 Desc: Importe Comisi�n Sucursal Tipo: NUMERIC Length: 14
   */
  private BigDecimal IMCOMSUC;

  /**
   * Order: 39 Desc: Custodio Global Id Tipo: CHARACTER Length: 8
   */
  private String CUSGLOID = "        ";

  /**
   * Order: 40 Desc: Importe Comisi�n Custodio Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMCOMCUS;

  /**
   * Order: 41 Desc: 0 Tipo: CHARACTER Length: 6
   */
  private String CDUNOS = "010101";

  /**
   * Order: 42 Desc: Importe Comisi�n Grupo Tipo: NUMERIC Length: 14
   */
  private BigDecimal IMCOMGRU;

  /**
   * Order: 43 Desc: Valor espacios Tipo: CHARACTER Length: 6
   */
  private String CDESPACE = "      ";

  /**
   * Order: 44 Desc: 0 Tipo: NUMERIC Length: 14
   */
  private String IMREPGES = "+00000000000000.00";

  /**
   * Order: 45 Desc: 0 Tipo: NUMERIC Length: 14
   */
  private String IMREPREC = "+00000000000000.00";

  /**
   * Order: 46 Desc: 0 Tipo: NUMERIC Length: 14
   */
  private String IMREPCON = "+00000000000000.00";

  /**
   * Order: 47 Desc: 0 Tipo: CHARACTER Length: 8
   */
  private String CDVALOR = "00000049";

  /**
   * Order: 48 Desc: Código Origen Tipo: CHARACTER Length: 3
   */
  private String CDORIGEN;

  /**
   * Order: 49 Desc: Canal Tipo: CHARACTER Length: 3
   */
  private String CDCANALL;

  /**
   * Order: 50 Desc: 0 Tipo: NUMERIC Length: 15
   */
  private String IMNULO = "+000000000000000.00";

  /**
   * Order: 51 Desc: Fecha Creación Tipo: CHARACTER Length: 8
   */
  private String FHAUDIT;

  /**
   * Order: 52 Desc: Títulos ejecutados Tipo: NUMERIC Length: 15
   */
  private BigDecimal NUTITAGR;

  /**
   * Order: 53 Desc: 0 Tipo: CHARACTER Length: 2
   */
  private String CDTEXTO = "TE";

  /**
   * @param nuoprout el nUOPROUT a establecer @
   */
  public void setNUOPROUT(BigDecimal NUOPROUT) {
    if (NUOPROUT == null || NUOPROUT.toString().length() > 15) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.NUOPROUT = NUOPROUT;
  }

  /**
   * 
   * @param NUOPROU2 @
   */
  public void setNUOPROU2(BigDecimal NUOPROU2) {
    if (NUOPROU2 == null || NUOPROU2.toString().length() > 11) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.NUOPROU2 = NUOPROU2;
  }

  /**
   * @param nupareje el nUPAREJE a establecer @
   */
  public void setNUPAREJE(BigDecimal NUPAREJE) {
    if (NUPAREJE == null || NUPAREJE.toString().length() > 4) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.NUPAREJE = NUPAREJE;
  }

  /**
   * @param cdclient el cDCLIENT a establecer @
   */
  public void setCDCLIENT(BigDecimal CDCLIENT) {
    if (CDCLIENT == null || CDCLIENT.toString().length() > 8) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDCLIENT = CDCLIENT;
  }

  /**
   * @param cdentliq el cDENTLIQ a establecer @
   */
  public void setCDENTLIQ(String CDENTLIQ) {
    if (CDENTLIQ == null || CDENTLIQ.length() > 4) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDENTLIQ = CDENTLIQ;
  }

  /**
   * @param cdisinvv el cDISINVV a establecer @
   */
  public void setCDISINVV(String CDISINVV) {
    if (CDISINVV == null || CDISINVV.length() > 12) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDISINVV = CDISINVV;
  }

  /**
   * @param CDBOLSAS el CDBOLSAS a establecer @
   */
  public void setCDBOLSAS(String CDBOLSAS) {
    if (CDBOLSAS == null || CDBOLSAS.length() > 3) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDBOLSAS = CDBOLSAS;
  }

  /**
   * @param TPOPERAC el TPOPERAC a establecer @
   */
  public void setTPOPERAC(String TPOPERAC) {
    if (TPOPERAC == null || TPOPERAC.length() == 0) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    else if (TPOPERAC.length() > 1) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    else if (TPOPERAC.equals("C") || TPOPERAC.equals("V")) {
      this.TPOPERAC = TPOPERAC;
    }
  }

  /**
   * 
   * @param CDOPEMER @
   */
  public void setCDOPEMER(String CDOPEMER) {
    if (CDOPEMER == null || CDOPEMER.length() > 8) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDOPEMER = CDOPEMER;
  }

  /**
   * @param ISROUTIN el ISROUTIN a establecer
   */
  public void setISROUTIN(String ISROUTIN) {
    this.ISROUTIN = ISROUTIN;
  }

  /**
   * @param FHEJECUC el FHEJECUC a establecer @
   */
  public void setFHEJECUC(String FHEJECUC) {
    if (FHEJECUC == null || FHEJECUC.length() > 8) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.FHEJECUC = FHEJECUC;
  }

  /**
   * 
   * @param IMEFECTI el IMEFECTI a establece @
   */
  public void setIMEFECTI(BigDecimal IMEFECTI) {
    if (IMEFECTI == null || !OperacionesInternacionalDWHUtils.isValid(IMEFECTI, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMEFECTI = IMEFECTI;
  }

  /**
   * @param nucaneje el nUCANEJE a establecer @
   */
  public void setNUCANEJE(BigDecimal NUCANEJE) {
    if (NUCANEJE == null || !OperacionesInternacionalDWHUtils.isValid(NUCANEJE, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.NUCANEJE = NUCANEJE;
  }

  /**
   * @param imdercog el iMDERCOG a establecer @
   */
  public void setIMDERCOG(BigDecimal IMDERCOG) {
    // Cambio para amplicar campos
    if (IMDERCOG == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMDERCOG, 14, 2)) {
      // if (IMDERCOG == null || !Utils.isValid(IMDERCOG, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMDERCOG = IMDERCOG;
  }

  /**
   * @param imderliq el iMDERLIQ a establecer @
   */
  public void setIMDERLIQ(BigDecimal IMDERLIQ) {
    if (IMDERLIQ == null || !OperacionesInternacionalDWHUtils.isValid(IMDERLIQ, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMDERLIQ = IMDERLIQ;
  }

  /**
   * @param sicomsvb el sICOMSVB a establecer @
   */
  public void setIMCOMSVB(BigDecimal IMCOMSVB) {
    if (IMCOMSVB == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMSVB, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMCOMSVB = IMCOMSVB;
  }

  /**
   * @param sicomliq el sICOMLIQ a establecer @
   */
  public void setIMCOMLIQ(BigDecimal IMCOMLIQ) {
    if (IMCOMLIQ == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMLIQ, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMCOMLIQ = IMCOMLIQ;
  }

  /**
   * @param sicomdev el sICOMDEV a establecer @
   */
  public void setIMCOMDEV(BigDecimal IMCOMDEV) {
    if (IMCOMDEV == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMDEV, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMCOMDEV = IMCOMDEV;
  }

  /**
   * @param nuperiod el nUPERIOD a establecer @
   */
  public void setNUPERIOD(BigDecimal NUPERIOD) {
    if (NUPERIOD == null || !OperacionesInternacionalDWHUtils.isValid(NUPERIOD, 4, 0)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.NUPERIOD = NUPERIOD;
  }

  /**
   * @param cdcliest el cDCLIEST a establecer @
   */
  public void setCDCLIEST(BigDecimal CDCLIEST) {
    if (CDCLIEST == null || !OperacionesInternacionalDWHUtils.isValid(CDCLIEST, 8, 0)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDCLIEST = CDCLIEST;
  }

  /**
   * @param imbrucog el iMBRUCOG a establecer @
   */
  public void setIMBRUCOG(BigDecimal IMBRUCOG) {
    // Cambio para amplicar campos
    if (IMBRUCOG == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMBRUCOG, 14, 2)) {
      // if (IMBRUCOG == null || !Utils.isValid(IMBRUCOG, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMBRUCOG = IMBRUCOG;
  }

  /**
   * @param imbruliq el iMBRULIQ a establecer @
   */
  public void setIMBRULIQ(BigDecimal IMBRULIQ) {
    if (IMBRULIQ == null || !OperacionesInternacionalDWHUtils.isValid(IMBRULIQ, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMBRULIQ = IMBRULIQ;
  }

  /**
   * @param sibruban el sIBRUBAN a establecer @
   */
  public void setIMBRUBAN(BigDecimal IMBRUBAN) {
    if (IMBRUBAN == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMBRUBAN, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMBRUBAN = IMBRUBAN;
  }

  /**
   * @param cdbroker el cDBROKER a establecer @
   */
  public void setCDBROKER(BigDecimal CDBROKER) {
    if (CDBROKER == null || !OperacionesInternacionalDWHUtils.isValid(CDBROKER, 8, 0)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.CDBROKER = CDBROKER;
  }

  /**
   * @param sinetclt el sINETCLT a establecer @
   */
  public void setIMNETCLT(BigDecimal IMNETCLT) {
    if (IMNETCLT == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMNETCLT, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMNETCLT = IMNETCLT;
  }

  /**
   * @param sibrocon el sIBROCON a establecer @
   */
  public void setIMBROCON(BigDecimal IMBROCON) {
    if (IMBROCON == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMBROCON, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMBROCON = IMBROCON;
  }

  /**
   * @param sicomsuc el sICOMSUC a establecer @
   */
  public void setIMCOMSUC(BigDecimal IMCOMSUC) {
    if (IMCOMSUC == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMSUC, 14, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMCOMSUC = IMCOMSUC;
  }

  /**
   * @param sicomcus el sICOMCUS a establecer @
   */
  public void setIMCOMCUS(BigDecimal IMCOMCUS) {
    if (IMCOMCUS == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMCUS, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMCOMCUS = IMCOMCUS;
  }

  /**
   * @param imcomgru el iMCOMGRU a establecer @
   */
  public void setIMCOMGRU(BigDecimal IMCOMGRU) {
    if (IMCOMGRU == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMGRU, 14, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.IMCOMGRU = IMCOMGRU;
  }

  /**
   * @param cDORIGEN el cDORIGEN a establecer @
   */
  public void setCDORIGEN(String cDORIGEN) {
    this.CDORIGEN = cDORIGEN;
  }

  /**
   * @param cDCANALL el cDCANALL a establecer @
   */
  public void setCDCANALL(String cDCANALL) {
    this.CDCANALL = cDCANALL;
  }

  /**
   * @param iMNULO el iMNULO a establecer @
   */
  public void setIMNULO(String iMNULO) {
    this.IMNULO = iMNULO;
  }

  /**
   * @param fhaudit el fhaudit a establecer @
   */
  public void setFHAUDIT(String fhaudit) {
    if (fhaudit == null || fhaudit.length() > 8) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.FHAUDIT = fhaudit;
  }

  /**
   * @param nutitagr el nutitagr a establecer @
   */
  public void setNUTITAGR(BigDecimal NUTITAGR) {
    if (NUTITAGR == null || !OperacionesInternacionalDWHUtils.isValid(NUTITAGR, 15, 2)) {
      LOG.debug("Error en el tamaño de parámetro o nulo");
    }
    this.NUTITAGR = NUTITAGR;
  }

  /**
   * @param cdtexto el cdtexto a establecer @
   */
  public void setCDTEXTO(String cdtexto) {
    CDTEXTO = cdtexto;
  }

  /**
   * @return el nUOPROUT
   */
  public BigDecimal getNUOPROUT() {
    return NUOPROUT;
  }

  /**
   * @return el nUOPROU2
   */
  public BigDecimal getNUOPROU2() {
    return NUOPROU2;
  }

  /**
   * @return el nUPAREJE
   */
  public BigDecimal getNUPAREJE() {
    return NUPAREJE;
  }

  /**
   * @return el cDCLIENT
   */
  public BigDecimal getCDCLIENT() {
    return CDCLIENT;
  }

  /**
   * @return el cDENTLIQ
   */
  public String getCDENTLIQ() {
    return CDENTLIQ;
  }

  /**
   * @return el cDISINVV
   */
  public String getCDISINVV() {
    return CDISINVV;
  }

  /**
   * @return el cDBOLSAS
   */
  public String getCDBOLSAS() {
    return CDBOLSAS;
  }

  /**
   * @return el tPOPERAC
   */
  public String getTPOPERAC() {
    return TPOPERAC;
  }

  /**
   * @return el tPEJERCI
   */
  public String getTPEJERCI() {
    return TPEJERCI;
  }

  /**
   * @return el tPEJECUC
   */
  public String getTPEJECUC() {
    return TPEJECUC;
  }

  /**
   * @return el tPOPESTD
   */
  public String getTPOPESTD() {
    return TPOPESTD;
  }

  /**
   * @return el tPCONTRA
   */
  public String getTPCONTRA() {
    return TPCONTRA;
  }

  /**
   * @return el cDOPEMER
   */
  public String getCDOPEMER() {
    return CDOPEMER;
  }

  /**
   * @return el iSROUTIN
   */
  public String getISROUTIN() {
    return ISROUTIN;
  }

  /**
   * @return el cDCLSMER
   */
  public String getCDCLSMER() {
    return CDCLSMER;
  }

  /**
   * @return el fHEJECUC
   */
  public String getFHEJECUC() {
    return FHEJECUC;
  }

  /**
   * @return el iMEFECTI
   */
  public BigDecimal getIMEFECTI() {
    return IMEFECTI;
  }

  /**
   * @return el nUCANEJE
   */
  public BigDecimal getNUCANEJE() {
    return NUCANEJE;
  }

  /**
   * @return el iMDERCOG
   */
  public BigDecimal getIMDERCOG() {
    return IMDERCOG;
  }

  /**
   * @return el iMDERLIQ
   */
  public BigDecimal getIMDERLIQ() {
    return IMDERLIQ;
  }

  /**
   * @return el iMCOMSVB
   */
  public BigDecimal getIMCOMSVB() {
    return IMCOMSVB;
  }

  /**
   * @return el iMCOMLIQ
   */
  public BigDecimal getIMCOMLIQ() {
    return IMCOMLIQ;
  }

  /**
   * @return el iMCOMBRK
   */
  public String getIMCOMBRK() {
    return IMCOMBRK;
  }

  /**
   * @return el iMCOMUK2
   */
  public String getIMCOMUK2() {
    return IMCOMUK2;
  }

  /**
   * @return el iMCOMUSA
   */
  public String getIMCOMUSA() {
    return IMCOMUSA;
  }

  /**
   * @return el iMCOMDEV
   */
  public BigDecimal getIMCOMDEV() {
    return IMCOMDEV;
  }

  /**
   * @return el nUPERIOD
   */
  public BigDecimal getNUPERIOD() {
    return NUPERIOD;
  }

  /**
   * @return el iMDBRKDA
   */
  public String getIMDBRKDA() {
    return IMDBRKDA;
  }

  /**
   * @return el cDCLIEST
   */
  public BigDecimal getCDCLIEST() {
    return CDCLIEST;
  }

  /**
   * @return el iMBRUCOG
   */
  public BigDecimal getIMBRUCOG() {
    return IMBRUCOG;
  }

  /**
   * @return el iMBRULIQ
   */
  public BigDecimal getIMBRULIQ() {
    return IMBRULIQ;
  }

  /**
   * @return el iMBRUBAN
   */
  public BigDecimal getIMBRUBAN() {
    return IMBRUBAN;
  }

  /**
   * @return el iMBANSIS
   */
  public String getIMBANSIS() {
    return IMBANSIS;
  }

  /**
   * @return el cDBROKER
   */
  public BigDecimal getCDBROKER() {
    return CDBROKER;
  }

  /**
   * @return el tPBROKER
   */
  public String getTPBROKER() {
    return TPBROKER;
  }

  /**
   * @return el iMNETCLT
   */
  public BigDecimal getIMNETCLT() {
    return IMNETCLT;
  }

  /**
   * @return el iMBROCON
   */
  public BigDecimal getIMBROCON() {
    return IMBROCON;
  }

  /**
   * @return el iMCOMSUC
   */
  public BigDecimal getIMCOMSUC() {
    return IMCOMSUC;
  }

  /**
   * @return el cUSGLOID
   */
  public String getCUSGLOID() {
    return CUSGLOID;
  }

  /**
   * @return el iMCOMCUS
   */
  public BigDecimal getIMCOMCUS() {
    return IMCOMCUS;
  }

  /**
   * @return el cDUNOS
   */
  public String getCDUNOS() {
    return CDUNOS;
  }

  /**
   * @return el iMCOMGRU
   */
  public BigDecimal getIMCOMGRU() {
    return IMCOMGRU;
  }

  /**
   * @return el cDESPACE
   */
  public String getCDESPACE() {
    return CDESPACE;
  }

  /**
   * @return el iMREPGES
   */
  public String getIMREPGES() {
    return IMREPGES;
  }

  /**
   * @return el iMREPREC
   */
  public String getIMREPREC() {
    return IMREPREC;
  }

  /**
   * @return el iMREPCON
   */
  public String getIMREPCON() {
    return IMREPCON;
  }

  /**
   * @return el cDVALOR
   */
  public String getCDVALOR() {
    return CDVALOR;
  }

  /**
   * get NUOPROUT as String @
   */
  private String getNUOPROUTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUOPROUT(), "000000000000000");
  }

  /**
   * get NUOPROUT as String
   * @throws Exception
   */
  private String getNUOPROU2AsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUOPROU2(), "00000000000");
  }

  /**
   * get NUPAREJE as String
   * @throws Exception
   */
  private String getNUPAREJEAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUPAREJE(), "0000");
  }

  /**
   * get CDCLIENT as String
   * @throws Exception
   */
  private String getCDCLIENTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getCDCLIENT(), "00000000");
  }

  /**
   * get IMEFECTI as String
   * @throws Exception
   */
  private String getIMEFECTIAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMEFECTI(), "000000000000000.00");
  }

  /**
   * get NUCANEJE as String
   * @throws Exception
   */
  private String getNUCANEJEAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUCANEJE(), "000000000000000.00");
  }

  /**
   * get IMDERCOG as String
   * @throws Exception
   */
  private String getIMDERCOGAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMDERCOG(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get IMDERLIQ as String
   * @throws Exception
   */
  private String getIMDERLIQAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMDERLIQ(), "000000000000000.00");
  }

  /**
   * get SICOMSVB as String
   * @throws Exception
   */
  private String getIMCOMSVBAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMSVB(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get SICOMLIQ as String
   * @throws Exception
   */
  private String getIMCOMLIQAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMLIQ(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get SICOMDEV as String
   * @throws Exception
   */
  private String getIMCOMDEVAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMDEV(), "'+'000000000000000.00;'-'000000000000000.00");

  }

  /**
   * get NUPERIOD as String
   * @throws Exception
   */
  private String getNUPERIODAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUPERIOD(), "0000");
  }

  /**
   * get CDCLIEST as String
   * @throws Exception
   */
  private String getCDCLIESTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getCDCLIEST(), "00000000");
  }

  /**
   * get IMBRUCOG as String
   * @throws Exception
   */
  private String getIMBRUCOGAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMBRUCOG(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get IMBRULIQ as String
   * @throws Exception
   */
  private String getIMBRULIQAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMBRULIQ(), "000000000000000.00");
  }

  /**
   * get SIBRUBAN as String
   * @throws Exception
   */
  private String getIMBRUBANAsString() throws Exception {

    return OperacionesInternacionalDWHUtils.getAsString(getIMBRUBAN(), "'+'000000000000000.00;'-'000000000000000.00");

  }

  /**
   * get CDBROKER as String
   * @throws Exception
   */
  private String getCDBROKERAsString() throws Exception {

    return OperacionesInternacionalDWHUtils.getAsString(getCDBROKER(), "00000000");

  }

  /**
   * get SINETCLT as String
   * @throws Exception
   */
  private String getIMNETCLTAsString() throws Exception {

    return OperacionesInternacionalDWHUtils.getAsString(getIMNETCLT(), "'+'000000000000000.00;'-'000000000000000.00");

  }

  /**
   * get SIBROCON as String
   * @throws Exception
   */
  private String getIMBROCONAsString() throws Exception {

    return OperacionesInternacionalDWHUtils.getAsString(getIMBROCON(), "'+'000000000000000.00;'-'000000000000000.00");

  }

  /**
   * get SICOMSUC as String
   * @throws Exception
   */
  private String getIMCOMSUCAsString() throws Exception {

    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMSUC(), "'+'00000000000000.00;'-'00000000000000.00");

  }

  /**
   * get SICOMCUS as String
   * @throws Exception
   */
  private String getIMCOMCUSAsString() throws Exception {

    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMCUS(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get IMCOMGRU as String
   * @throws Exception
   */
  private String getIMCOMGRUAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMGRU(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get NUTITAGR as String
   * @throws Exception
   */
  private String getNUTITAGRAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUTITAGR(), "000000000000000.00");
  }

  /**
   * @return el CDORIGEN
   */
  public String getCDORIGEN() {
    return CDORIGEN;
  }

  /**
   * @return el CDCANALL
   */
  public String getCDCANALL() {
    return CDCANALL;
  }

  /**
   * @return el IMNULO
   */
  public String getIMNULO() {
    return IMNULO;
  }

  /**
   * @return el FHAUDIT
   */
  public String getFHAUDIT() {
    return FHAUDIT;
  }

  /**
   * @return el NUTITAGR
   */
  public BigDecimal getNUTITAGR() {
    return NUTITAGR;
  }

  /**
   * @return el CDTEXTO
   */
  public String getCDTEXTO() {
    return CDTEXTO;
  }

  public String getAsString(int fieldPos) throws Exception {
    String result = "";
    switch (fieldPos) {
      case DwhConstantes.MINORISTA_NUOPROUT:
        result = getNUOPROUTAsString();
        break;
      case DwhConstantes.MINORISTA_NUOPROU2:
        result = getNUOPROU2AsString();
        break;
      case DwhConstantes.MINORISTA_NUPAREJE:
        result = getNUPAREJEAsString();
        break;
      case DwhConstantes.MINORISTA_CDCLIENT:
        result = getCDCLIENTAsString();
        break;
      case DwhConstantes.MINORISTA_CDENTLIQ:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDENTLIQ(), 4);
        break;
      case DwhConstantes.MINORISTA_CDISINVV:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDISINVV(), 12);
        break;
      case DwhConstantes.MINORISTA_CDBOLSAS:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDBOLSAS(), 3);
        break;
      case DwhConstantes.MINORISTA_TPOPERAC:
        result = getTPOPERAC();
        break;
      case DwhConstantes.MINORISTA_TPEJERCI:
        result = getTPEJERCI();
        break;
      case DwhConstantes.MINORISTA_TPEJECUC:
        result = getTPEJECUC();
        break;
      case DwhConstantes.MINORISTA_TPOPESTD:
        result = getTPOPESTD();
        break;
      case DwhConstantes.MINORISTA_TPCONTRA:
        result = getTPCONTRA();
        break;
      case DwhConstantes.MINORISTA_CDOPEMER:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDOPEMER(), 8);
        break;
      case DwhConstantes.MINORISTA_ISROUTIN:
        result = getISROUTIN();
        break;
      case DwhConstantes.MINORISTA_CDCLSMER:
        result = getCDCLSMER();
        break;
      case DwhConstantes.MINORISTA_IMEFECTI:
        result = getIMEFECTIAsString();
        break;
      case DwhConstantes.MINORISTA_FHEJECUC:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getFHEJECUC(), 8);
        break;
      case DwhConstantes.MINORISTA_NUCANEJE:
        result = getNUCANEJEAsString();
        break;
      case DwhConstantes.MINORISTA_IMDERCOG:
        result = getIMDERCOGAsString();
        break;
      case DwhConstantes.MINORISTA_IMDERLIQ:
        result = getIMDERLIQAsString();
        break;
      case DwhConstantes.MINORISTA_IMCOMSVB:
        result = getIMCOMSVBAsString();
        break;
      case DwhConstantes.MINORISTA_IMCOMLIQ:
        result = getIMCOMLIQAsString();
        break;
      case DwhConstantes.MINORISTA_IMCOMBRK:
        result = getIMCOMBRK();
        break;
      case DwhConstantes.MINORISTA_IMCOMUK2:
        result = getIMCOMUK2();
        break;
      case DwhConstantes.MINORISTA_IMCOMUSA:
        result = getIMCOMUSA();
        break;
      case DwhConstantes.MINORISTA_IMCOMDEV:
        result = getIMCOMDEVAsString();
        break;
      case DwhConstantes.MINORISTA_NUPERIOD:
        result = getNUPERIODAsString();
        break;
      case DwhConstantes.MINORISTA_IMDBRKDA:
        result = getIMDBRKDA();
        break;
      case DwhConstantes.MINORISTA_CDCLIEST:
        result = getCDCLIESTAsString();
        break;
      case DwhConstantes.MINORISTA_IMBRUCOG:
        result = getIMBRUCOGAsString();
        break;
      case DwhConstantes.MINORISTA_IMBRULIQ:
        result = getIMBRULIQAsString();
        break;
      case DwhConstantes.MINORISTA_IMBRUBAN:
        result = getIMBRUBANAsString();
        break;
      case DwhConstantes.MINORISTA_IMBANSIS:
        result = getIMBANSIS();
        break;
      case DwhConstantes.MINORISTA_CDBROKER:
        result = getCDBROKERAsString();
        break;
      case DwhConstantes.MINORISTA_TPBROKER:
        result = getTPBROKER();
        break;
      case DwhConstantes.MINORISTA_IMNETCLT:
        result = getIMNETCLTAsString();
        break;
      case DwhConstantes.MINORISTA_IMBROCON:
        result = getIMBROCONAsString();
        break;
      case DwhConstantes.MINORISTA_IMCOMSUC:
        result = getIMCOMSUCAsString();
        break;
      case DwhConstantes.MINORISTA_CUSGLOID:
        result = getCUSGLOID();
        break;
      case DwhConstantes.MINORISTA_IMCOMCUS:
        result = getIMCOMCUSAsString();
        break;
      case DwhConstantes.MINORISTA_CDUNOS:
        result = getCDUNOS();
        break;
      case DwhConstantes.MINORISTA_IMCOMGRU:
        result = getIMCOMGRUAsString();
        break;
      case DwhConstantes.MINORISTA_CDESPACE:
        result = getCDESPACE();
        break;
      case DwhConstantes.MINORISTA_IMREPGES:
        result = getIMREPGES();
        break;
      case DwhConstantes.MINORISTA_IMREPREC:
        result = getIMREPREC();
        break;
      case DwhConstantes.MINORISTA_IMREPCON:
        result = getIMREPCON();
        break;
      case DwhConstantes.MINORISTA_CDVALOR:
        result = getCDVALOR();
        break;
      case DwhConstantes.MINORISTA_CDORIGEN:
        result = getCDORIGEN();
        break;
      case DwhConstantes.MINORISTA_CDCANALL:
        result = getCDCANALL();
        break;
      case DwhConstantes.MINORISTA_IMNULO:
        result = getIMNULO();
        break;
      case DwhConstantes.MINORISTA_FHAUDIT:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getFHAUDIT(), 8);
        break;
      case DwhConstantes.MINORISTA_NUTITAGR:
        result = getNUTITAGRAsString();
        break;
      case DwhConstantes.MINORISTA_CDTEXTO:
        result = getCDTEXTO();
        break;
    }
    return result;
  }

  /**
   * 
   */
  public String toString() {
    String result = "";
    for (int i = 1; i < 54; i++) {
      try {
        result += getAsString(i);
      }
      catch (Exception e) {
        LOG.debug("Error minoristas", e);
      }
    }
    return result;

  }

}
