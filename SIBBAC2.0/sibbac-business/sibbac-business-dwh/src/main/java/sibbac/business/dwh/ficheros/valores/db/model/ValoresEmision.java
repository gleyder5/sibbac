package sibbac.business.dwh.ficheros.valores.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Table(name = "TMCT0_VALORES_EMISION")
@Entity
public class ValoresEmision implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  @Column(name = "CDEMISIO", nullable = false, length = 12)
  private String cdemisio;

  @Column(name = "CDISIN", nullable = false, length = 12)
  private String cdisin;

  @Column(name = "CDALFVAL", nullable = false, length = 4)
  private String cdalfval;

  @Column(name = "DSEMISIO", nullable = false, length = 32)
  private String dsemisio;

  @Column(name = "CDACTIVO", nullable = false, length = 3)
  private String cdactivo;

  @Column(name = "CDCAMBIO", nullable = false, length = 1)
  private String cdcambio;

  @Column(name = "CDFORMAT", nullable = false, length = 2)
  private String cdformat;

  @Column(name = "FEEMISIO", nullable = false, length = 10)
  private String feemisio;

  @Column(name = "IMNOMINA", nullable = false, length = 17)
  private String imnomina;

  @Column(name = "CDMONNOM", nullable = false, length = 3)
  private String cdmonnom;

  @Column(name = "IMCAPITA", nullable = false, length = 17)
  private String imcapita;

  @Column(name = "CDCAPITA", nullable = false, length = 3)
  private String cdcapita;

  @Column(name = "CDSTLIQI", nullable = false, length = 1)
  private String cdstliqi;

  @Column(name = "CLASEVAL", nullable = false, length = 2)
  private String claseval;

  @Column(name = "CDNATEMI", nullable = false, length = 3)
  private String cdnatemi;

  @Column(name = "CDDGCI", nullable = false, length = 3)
  private String cddgci;

  @Column(name = "SITUVAL", nullable = false, length = 1)
  private String situval;

  @Column(name = "CDESTADO", nullable = false, length = 1)
  private String cdestado;

  @Column(name = "FEESTADO", nullable = false, length = 10)
  private String feestado;

  @Column(name = "ADUSUAR", nullable = false, length = 8)
  private String adusar;

  @Column(name = "FHAUDIT", nullable = false, length = 26)
  private Timestamp fhaudit;

  @Column(name = "USUAUDIT", nullable = false, length = 100)
  private String usuaudit;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getCdemisio() {
    return cdemisio;
  }

  public void setCdemisio(String cdemisio) {
    this.cdemisio = cdemisio;
  }

  public String getCdisin() {
    return cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public String getCdalfval() {
    return cdalfval;
  }

  public void setCdalfval(String cdalfval) {
    this.cdalfval = cdalfval;
  }

  public String getDsemisio() {
    return dsemisio;
  }

  public void setDsemisio(String dsemisio) {
    this.dsemisio = dsemisio;
  }

  public String getCdactivo() {
    return cdactivo;
  }

  public void setCdactivo(String cdactivo) {
    this.cdactivo = cdactivo;
  }

  public String getCdcambio() {
    return cdcambio;
  }

  public void setCdcambio(String cdcambio) {
    this.cdcambio = cdcambio;
  }

  public String getCdformat() {
    return cdformat;
  }

  public void setCdformat(String cdformat) {
    this.cdformat = cdformat;
  }

  public String getFeemisio() {
    return feemisio;
  }

  public void setFeemisio(String feemisio) {
    this.feemisio = feemisio;
  }

  public String getImnomina() {
    return imnomina;
  }

  public void setImnomina(String imnomina) {
    this.imnomina = imnomina;
  }

  public String getCdmonnom() {
    return cdmonnom;
  }

  public void setCdmonnom(String cdmonnom) {
    this.cdmonnom = cdmonnom;
  }

  public String getImcapita() {
    return imcapita;
  }

  public void setImcapita(String imcapita) {
    this.imcapita = imcapita;
  }

  public String getCdcapita() {
    return cdcapita;
  }

  public void setCdcapita(String cdcapita) {
    this.cdcapita = cdcapita;
  }

  public String getCdstliqi() {
    return cdstliqi;
  }

  public void setCdstliqi(String cdstliqi) {
    this.cdstliqi = cdstliqi;
  }

  public String getClaseval() {
    return claseval;
  }

  public void setClaseval(String claseval) {
    this.claseval = claseval;
  }

  public String getCdnatemi() {
    return cdnatemi;
  }

  public void setCdnatemi(String cdnatemi) {
    this.cdnatemi = cdnatemi;
  }

  public String getCddgci() {
    return cddgci;
  }

  public void setCddgci(String cddgci) {
    this.cddgci = cddgci;
  }

  public String getSituval() {
    return situval;
  }

  public void setSituval(String situval) {
    this.situval = situval;
  }

  public String getCdestado() {
    return cdestado;
  }

  public void setCdestado(String cdestado) {
    this.cdestado = cdestado;
  }

  public String getFeestado() {
    return feestado;
  }

  public void setFeestado(String feestado) {
    this.feestado = feestado;
  }

  public String getAdusar() {
    return adusar;
  }

  public void setAdusar(String adusar) {
    this.adusar = adusar;
  }

  public Timestamp getFhaudit() {
    return fhaudit != null ? (Timestamp) fhaudit.clone() : null;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = (fhaudit != null ? (Timestamp) fhaudit.clone() : null);
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }
  
  public static synchronized ValoresEmision createValoresEmision(PartenonFileLineDTO element){
    ValoresEmision vEmision = new ValoresEmision();
    
    String linea = element.getLine();
    
    vEmision.setId(element.getIdLine());

    String cdemisio = linea.substring(1, 13).trim();
    vEmision.setCdemisio(cdemisio);

    String cdisin = linea.substring(13, 25).trim();
    vEmision.setCdisin(cdisin);

    String cdalfval = linea.substring(25, 29).trim();
    vEmision.setCdalfval(cdalfval);

    String dsemisio = linea.substring(29, 61).trim();
    vEmision.setDsemisio(dsemisio);

    String cdactivo = linea.substring(61, 64).trim();
    vEmision.setCdactivo(cdactivo);

    String cdcambio = linea.substring(64, 65).trim();
    vEmision.setCdcambio(cdcambio);

    String cdformat = linea.substring(65, 67).trim();
    vEmision.setCdformat(cdformat);

    String feemisio = linea.substring(67, 77).trim();
    vEmision.setFeemisio(feemisio);

    String imnomina = linea.substring(77, 94).trim();
    vEmision.setImnomina(imnomina);

    String cdmonnom = linea.substring(94, 97).trim();
    vEmision.setCdmonnom(cdmonnom);

    String imcapita = linea.substring(97, 114).trim();
    vEmision.setImcapita(imcapita);

    String cdcapita = linea.substring(114, 117).trim();
    vEmision.setCdcapita(cdcapita);

    String cdstliqi = linea.substring(117, 118).trim();
    vEmision.setCdstliqi(cdstliqi);

    String claseval = linea.substring(118, 120).trim();
    vEmision.setClaseval(claseval);

    String cdnatemi = linea.substring(120, 123).trim();
    vEmision.setCdnatemi(cdnatemi);

    String cddgci = linea.substring(123, 126).trim();
    vEmision.setCddgci(cddgci);

    String situval = linea.substring(126, 127).trim();
    vEmision.setSituval(situval);

    String cdestado = linea.substring(127, 128).trim();
    vEmision.setCdestado(cdestado);

    String feestado = linea.substring(128, 138).trim();
    vEmision.setFeestado(feestado);

    String adusar = linea.substring(138).trim();
    vEmision.setAdusar(adusar);

    vEmision.setFhaudit(new Timestamp(new Date().getTime()));
    vEmision.setUsuaudit(" ");

    return vEmision;

  }

}
