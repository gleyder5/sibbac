package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

@Repository
public interface FicheroValorDao extends JpaRepository</* FIXME entidad */MaestroValores, String> {

  /**
   * Query para DWHficheroValoresDao, ORIGINAL
   */
  public static final String FICHERO_VALORES_QUERY = "SELECT CDISINVV, CDISINGR, NBTITGRU FROM TMCT0_DWH_VALORES_OPERADO";

  @Query(value = FICHERO_VALORES_QUERY, nativeQuery = true)
  public List<Object[]> getIsin();

  /**
   * Query para DWHficheroValoresDao, DATOS
   */
  public static final String DATOS_FICHERO_VALORES_QUERY = "SELECT " + "VALOR.CDISINVV, " + "VALOR.NBTITREV, "
      + "GRUPO.CDGRSIBV, " + "CASE WHEN GRUPO.NBGRSIBV IS NULL THEN 'NO EXISTE DESCRIPCION' "
      + "ELSE GRUPO.NBGRSIBV END AS NBGRSIBV, " + "CASE WHEN GRUPO.CDGRUVAL IS NULL THEN '95' "
      + "ELSE GRUPO.CDGRUVAL END AS CDAGRTIP, " + "CASE WHEN GRUPO.NBGRUVAL IS NULL THEN 'NO EXISTE DESCRIPCION' "
      + "ELSE GRUPO.NBGRUVAL END AS NBAGRTIP, " + "CASE WHEN GRUPO.CDTIPREN IS NULL THEN 'V' "
      + "ELSE GRUPO.CDTIPREN END AS CDTIPREN, " + "CASE WHEN GRUPO.NBTIPREN IS NULL THEN '' "
      + "ELSE GRUPO.NBTIPREN END AS NBTIPREN, " + "NVL(VALOR.CDVAIBEX,'') AS CDVAIBEX " + "FROM TMCT0_MAESTRO_VALORES VALOR "
      + "LEFT JOIN TMCT0_VALORES_GRUPO GRUPO " + "ON VALOR.CDGRSIBV = GRUPO.CDGRSIBV " + "WHERE CDISINVV = :cdisinvv "
      + "ORDER BY VALOR.FHBAJACV DESC, VALOR.FHINICIV DESC " + "FETCH FIRST 1 ROW ONLY";

  @Query(value = DATOS_FICHERO_VALORES_QUERY, nativeQuery = true)
  public List<Object[]> getDatosIsin(@Param("cdisinvv") String cdisinvv);

  /**
   * Query para DWHficheroValoresDao con Paginación
   */
  public static final String FICHERO_VALORES_PAGE_QUERY = FICHERO_VALORES_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHficheroValoresDao
   */
  public static final String FICHERO_VALORES_PAGE_QUERY_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + FICHERO_VALORES_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHficheroValoresDao sin
   * paginación
   * @return
   */
  @Query(value = FICHERO_VALORES_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListFicheroValor(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHagrupTipoValorDao sin
   * paginación
   * @return
   */
  @Query(value = FICHERO_VALORES_PAGE_QUERY_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListFicheroValor();

}
