package sibbac.business.dwh.ficheros.valores.db.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

@Repository
public interface MaestroValoresDao extends JpaRepository<MaestroValores, BigInteger> {

}