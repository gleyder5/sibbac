package sibbac.business.dwh.ficheros.valores.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Table(name = "TMCT0_VALORES_WARRANT")
@Entity
public class ValoresWarrant implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  @Column(name = "CDEMISIO", nullable = false, length = 12)
  private String cdemisio;

  @Column(name = "RESTO", nullable = false, length = 53)
  private String resto;

  @Column(name = "FHAUDIT", nullable = false, length = 26)
  private Timestamp fhaudit;

  @Column(name = "USUAUDIT", nullable = false, length = 100)
  private String usuaudit;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getCdemisio() {
    return cdemisio;
  }

  public void setCdemisio(String cdemisio) {
    this.cdemisio = cdemisio;
  }

  public String getResto() {
    return resto;
  }

  public void setResto(String resto) {
    this.resto = resto;
  }

  public Timestamp getFhaudit() {
    return fhaudit != null ? (Timestamp) fhaudit.clone() : null;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = (fhaudit != null ? (Timestamp) fhaudit.clone() : null);
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public static synchronized ValoresWarrant createValoresWarrant(PartenonFileLineDTO element) {
    ValoresWarrant vWarrant = new ValoresWarrant();

    String linea = element.getLine();

    vWarrant.setId(element.getIdLine());

    String cdemisio = linea.substring(1, 13).trim();
    vWarrant.setCdemisio(cdemisio);

    String resto = linea.substring(13).trim();
    vWarrant.setResto(resto);

    vWarrant.setFhaudit(new Timestamp(new Date().getTime()));
    vWarrant.setUsuaudit(" ");

    return vWarrant;

  }

}
