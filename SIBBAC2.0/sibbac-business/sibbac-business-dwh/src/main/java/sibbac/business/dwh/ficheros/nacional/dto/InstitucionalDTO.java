package sibbac.business.dwh.ficheros.nacional.dto;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.OperacionesInternacionalDWHUtils;

public class InstitucionalDTO {

  protected static final Logger LOG = LoggerFactory.getLogger(InstitucionalDTO.class);

  /**
   * Order: 1 Desc: N�mero Operaci�n Tipo: NUMERIC Length: 8
   */
  private BigDecimal NUOPROUT;

  /**
   * Order: 2 Desc: N�mero Parcial Tipo: NUMERIC Length: 4
   */
  private BigDecimal NUPAREJE;

  /**
   * Order: 3 Desc: C�digo Cliente Tipo: NUMERIC Length: 8
   */
  private BigDecimal CDCLIENT;

  /**
   * Order: 4 Desc: C�digo Entidad Liquidadora Tipo: CHARACTER Length: 4
   */
  private String CDENTLIQ;

  /**
   * Order: 5 Desc: C�digo ISIN Tipo: CHARACTER Length: 12
   */
  private String CDISINVV;

  /**
   * Order: 6 Desc: C�digo Bolsas Tipo: CHARACTER Length: 3
   */
  private String CDBOLSAS;

  /**
   * Order: 7 Desc: Tipo Operaci�n Tipo: CHARACTER Length: 1
   */
  private String TPOPERAC;

  /**
   * Order: 8 Desc: Tipo Operaci�n Bolsa Tipo: CHARACTER Length: 2
   */
  private String TPEJERCI = "CV";

  /**
   * Order: 9 Desc: Tipo Ejecuci�n Tipo: CHARACTER Length: 1
   */
  private String TPEJECUC = "V";

  /**
   * Order: 10 Desc: Tipo Operaci�n Estado Tipo: CHARACTER Length: 1
   */
  private String TPOPESTD = "N";

  /**
   * Order: 11 Desc: Tipo Contrataci�n Tipo: CHARACTER Length: 1
   */
  private String TPCONTRA = "T";

  /**
   * Order: 12 Desc: C�digo Operador Mercado Tipo: CHARACTER Length: 8
   */
  private String CDOPEMER = "99999999";

  /**
   * Order: 13 Desc: C�digo Derecho Tipo: CHARACTER Length: 1
   */
  private String CDDERECH;

  /**
   * Order: 14 Desc: C�digo Procedencia Tipo: CHARACTER Length: 1
   */
  private String CDPROCED = "D";

  /**
   * Order: 15 Desc: Fecha Ejecuci�n Tipo: CHARACTER Length: 10
   */
  private String FHEJECUC;

  /**
   * Order: 16 Desc: Importe Efectivo Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMEFECTI;

  /**
   * Order: 17 Desc: T�tulos ejecutados Tipo: NUMERIC Length: 15
   */
  private BigDecimal NUCANEJE;

  /**
   * Order: 18 Desc: Importe Derechos Gesti�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMDERCOG;

  /**
   * Order: 19 Desc: Importe Derechos Liquidaci�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMDERLIQ;

  /**
   * Order: 20 Desc: importe Comisi�n SVB Tipo: NUMERIC
   */
  private BigDecimal IMCOMSVB;

  /**
   * Order: 21 Desc: Comisi�n Liquidadora Tipo: NUMERIC
   */
  private BigDecimal IMCOMLIQ;

  /**
   * Order: 22 Desc: Comisi�n Broker Tipo: Numeric
   */
  private String IMCOMBRK = "+000000000000000.00";

  /**
   * Order: 23 Desc: Comisi�n UK2 Tipo: CONSTANT
   */
  private String IMCOMUK2 = "+000000000000000.00";

  /**
   * Order: 24 Desc: Comisi�n USA Tipo: CONSTANT
   */
  private String IMCOMUSA = "+000000000000000.00";

  /**
   * Order: 25 Desc: Importe Comisi�n Devoluci�n Tipo: NUMERIC Length: 1
   */
  private BigDecimal IMCOMDEV;

  /**
   * Order: 26 Desc: Periodo de Contrataci�n Tipo: NUMERIC Length: 8
   */
  private BigDecimal NUPERIOD;

  /**
   * Order: 27 Desc: Devoluci�n cliente D.A. Securities Tipo: NUMERIC
   */
  private String IMDBRKDA = "+000000000000000.00";

  /**
   * Order: 28 Desc: C�digo Cliente Tipo: NUMERIC Length: 8
   */
  private BigDecimal CDCLIEST;

  /**
   * Order: 29 Desc: Importe Bruto Derechos Gesti�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMBRUCOG;

  /**
   * Order: 30 Desc: Importe Bruto Derechos Liquidaci�n Tipo: NUMERIC Length: 15
   */
  private BigDecimal IMBRULIQ;

  /**
   * Order: 31 Desc: Importe Comisi�n Banco Tipo: NUMERIC
   */
  private BigDecimal IMBRUBAN;

  /**
   * Order: 32 Desc: importe Comisi�n Banco SISA Tipo: NUMERIC
   */
  private String IMBANSIS = "+000000000000000.00";

  /**
   * Order: 33 Desc: C�digo Broker Tipo: NUMERIC Length: 8
   */
  private BigDecimal CDBROKER;

  /**
   * Order: 34 Desc: Broker del grupo Tipo: CHARACTER Length: 1
   */
  private String CDGRUPBR = "G";

  /**
   * Order: 35 Desc: Importe Neto Cliente Tipo: CHARACTER Length: 1
   */
  private BigDecimal IMNETCLT;

  /**
   * Order: 36 Desc: importe Comisi�n Broker que contrata Tipo: CHARACTER
   * Length: 1
   */
  private BigDecimal IMBROCON;

  /**
   * Order: 37 Desc: importe Comisi�n Sucursal Tipo: CHARACTER Length: 1
   */
  private BigDecimal IMCOMSUC;

  /**
   * Order: 38 Desc: Custodio Global Tipo: CHARACTER Length: 8
   */
  private String CDCUSGLB = "        ";

  /**
   * Order: 39 Desc: importe Comisi�n Custodio Tipo: CHARACTER Length: 1
   */
  private BigDecimal IMCOMCUS;

  /**
   * Order: 40 Desc: Gestor del Cliente Tipo: CHARACTER Length: 3
   */
  private String CDGESTOR = "ES ";

  /**
   * Order: 41 Desc: 0 Tipo: CHARACTER Length: 3
   */
  private String CDUNIGEF = "ES ";

  /**
   * Order: 42 Desc: 0 Tipo: CHARACTER Length: 3
   */
  private String CDUNIGEB = "ES ";

  /**
   * Order: 43 Desc: Importe Comisi�n Grupo Tipo: NUMERIC
   */
  private BigDecimal IMCOMGRU;

  /**
   * Order: 44 Desc: Grupo de reparto Tipo: CHARACTER Length: 3
   */
  private String CDGRUREP = "   ";

  /**
   * Order: 45 Desc: Pa�s de contrataci�n Tipo: CHARACTER Length: 3
   */
  private String CDUNICON = "   ";

  /**
   * Order: 46 Desc: 0 Tipo: NUMERIC
   */
  private String IMREPGES = "+00000000000000.00";

  /**
   * Order: 47 Desc: 0 Tipo: NUMERIC
   */
  private String IMREPREC = "+00000000000000.00";

  /**
   * Order: 48 Desc: 0 Tipo: NUMERIC
   */
  private String IMREPCON = "+00000000000000.00";

  /**
   * @param nuoprout el nUOPROUT a establecer
   * @throws Exception
   */
  public void setNUOPROUT(BigDecimal NUOPROUT) throws Exception {
    if (NUOPROUT == null || NUOPROUT.toString().length() > 8) {
      throw new Exception();
    }
    this.NUOPROUT = NUOPROUT;
  }

  /**
   * @param nupareje el nUPAREJE a establecer
   * @throws Exception
   */
  public void setNUPAREJE(BigDecimal NUPAREJE) throws Exception {
    if (NUPAREJE == null || NUPAREJE.toString().length() > 4) {
      throw new Exception();
    }
    this.NUPAREJE = NUPAREJE;
  }

  /**
   * @param cdclient el cDCLIENT a establecer
   * @throws Exception
   */
  public void setCDCLIENT(BigDecimal CDCLIENT) throws Exception {
    if (CDCLIENT == null || CDCLIENT.toString().length() > 8) {
      throw new Exception();
    }
    this.CDCLIENT = CDCLIENT;
  }

  /**
   * @param cdentliq el cDENTLIQ a establecer
   * @throws Exception
   */
  public void setCDENTLIQ(String CDENTLIQ) throws Exception {
    if (CDENTLIQ == null || CDENTLIQ.length() > 4) {
      throw new Exception();
    }
    this.CDENTLIQ = CDENTLIQ;
  }

  /**
   * @param cdisinvv el cDISINVV a establecer
   * @throws Exception
   */
  public void setCDISINVV(String CDISINVV) throws Exception {
    if (CDISINVV == null || CDISINVV.length() > 12) {
      throw new Exception();
    }
    this.CDISINVV = CDISINVV;
  }

  /**
   * @param CDBOLSAS el CDBOLSAS a establecer
   * @throws Exception
   */
  public void setCDBOLSAS(String CDBOLSAS) throws Exception {
    if (CDBOLSAS == null || CDBOLSAS.length() > 3) {
      throw new Exception();
    }
    this.CDBOLSAS = CDBOLSAS;
  }

  /**
   * @param TPOPERAC el TPOPERAC a establecer
   * @throws Exception
   */
  public void setTPOPERAC(String TPOPERAC) throws Exception {
    if (TPOPERAC == null || TPOPERAC.length() > 1) {
      throw new Exception();
    }
    if (TPOPERAC.equals("C") || TPOPERAC.equals("V")) {
      this.TPOPERAC = TPOPERAC;
    }
    else {
      throw new Exception();
    }
  }

  /**
   * @param CDDERECH el CDDERECH a establecer
   * @throws Exception
   */
  public void setCDDERECH(String CDDERECH) throws Exception {
    if (CDDERECH == null || CDDERECH.length() > 1) {
      throw new Exception();
    }
    this.CDDERECH = CDDERECH;
  }

  /**
   * @param cdopemer el cdopemer a establecer
   * @throws Exception
   * @throws Exception
   */
  public void setCDOPEMER(String cdopemer) throws Exception {
    if (cdopemer == null || cdopemer.length() > 8) {
      throw new Exception();
    }
    this.CDOPEMER = cdopemer;
  }

  /**
   * @param FHEJECUC el FHEJECUC a establecer
   * @throws Exception
   */
  public void setFHEJECUC(String FHEJECUC) throws Exception {
    if (FHEJECUC == null || FHEJECUC.length() > 10) {
      throw new Exception();
    }
    this.FHEJECUC = FHEJECUC;
  }

  /**
   * 
   * @param IMEFECTI el IMEFECTI a establece
   * @throws Exception
   */
  public void setIMEFECTI(BigDecimal IMEFECTI) throws Exception {
    if (IMEFECTI == null || !OperacionesInternacionalDWHUtils.isValid(IMEFECTI, 15, 2)) {
      throw new Exception();
    }
    this.IMEFECTI = IMEFECTI;
  }

  /**
   * @param nucaneje el nUCANEJE a establecer
   * @throws Exception
   */
  public void setNUCANEJE(BigDecimal NUCANEJE) throws Exception {
    if (NUCANEJE == null || !OperacionesInternacionalDWHUtils.isValid(NUCANEJE, 15, 2)) {
      throw new Exception();
    }
    this.NUCANEJE = NUCANEJE;
  }

  /**
   * @param imdercog el iMDERCOG a establecer
   * @throws Exception
   */
  public void setIMDERCOG(BigDecimal IMDERCOG) throws Exception {
    if (IMDERCOG == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMDERCOG, 14, 2)) {
      // if (IMDERCOG == null || !Utils.isValid(IMDERCOG, 15, 2)) {
      throw new Exception();
    }
    this.IMDERCOG = IMDERCOG;
  }

  /**
   * @param imderliq el iMDERLIQ a establecer
   * @throws Exception
   */
  public void setIMDERLIQ(BigDecimal IMDERLIQ) throws Exception {
    if (IMDERLIQ == null || !OperacionesInternacionalDWHUtils.isValid(IMDERLIQ, 15, 2)) {
      throw new Exception();
    }
    this.IMDERLIQ = IMDERLIQ;
  }

  /**
   * @param sicomsvb el sICOMSVB a establecer
   * @throws Exception
   */
  public void setIMCOMSVB(BigDecimal IMCOMSVB) throws Exception {
    if (IMCOMSVB == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMSVB, 15, 2)) {
      throw new Exception();
    }
    this.IMCOMSVB = IMCOMSVB;
  }

  /**
   * @param sicomliq el sICOMLIQ a establecer
   * @throws Exception
   */
  public void setIMCOMLIQ(BigDecimal IMCOMLIQ) throws Exception {
    if (IMCOMLIQ == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMLIQ, 15, 2)) {
      throw new Exception();
    }
    this.IMCOMLIQ = IMCOMLIQ;
  }

  /**
   * @param sicomdev el sICOMDEV a establecer
   * @throws Exception
   */
  public void setIMCOMDEV(BigDecimal IMCOMDEV) throws Exception {
    if (IMCOMDEV == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMDEV, 15, 2)) {
      throw new Exception();
    }
    this.IMCOMDEV = IMCOMDEV;
  }

  /**
   * @param nuperiod el nUPERIOD a establecer
   * @throws Exception
   */
  public void setNUPERIOD(BigDecimal NUPERIOD) throws Exception {
    if (NUPERIOD == null || !OperacionesInternacionalDWHUtils.isValid(NUPERIOD, 8, 0)) {
      throw new Exception();
    }
    this.NUPERIOD = NUPERIOD;
  }

  /**
   * @param cdcliest el cDCLIEST a establecer
   * @throws Exception
   */
  public void setCDCLIEST(BigDecimal CDCLIEST) throws Exception {
    if (CDCLIEST == null || !OperacionesInternacionalDWHUtils.isValid(CDCLIEST, 8, 0)) {
      throw new Exception();
    }
    this.CDCLIEST = CDCLIEST;
  }

  /**
   * @param imbrucog el iMBRUCOG a establecer
   * @throws Exception
   */
  public void setIMBRUCOG(BigDecimal IMBRUCOG) throws Exception {
    if (IMBRUCOG == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMBRUCOG, 14, 2)) {
      // if (IMBRUCOG == null || !Utils.isValid(IMBRUCOG, 15, 2)) {
      throw new Exception();
    }
    this.IMBRUCOG = IMBRUCOG;
  }

  /**
   * @param imbruliq el iMBRULIQ a establecer
   * @throws Exception
   */
  public void setIMBRULIQ(BigDecimal IMBRULIQ) throws Exception {
    if (IMBRULIQ == null || !OperacionesInternacionalDWHUtils.isValid(IMBRULIQ, 15, 2)) {
      throw new Exception();
    }
    this.IMBRULIQ = IMBRULIQ;
  }

  /**
   * @param sibruban el sIBRUBAN a establecer
   * @throws Exception
   */
  public void setIMBRUBAN(BigDecimal IMBRUBAN) throws Exception {
    if (IMBRUBAN == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMBRUBAN, 15, 2)) {
      throw new Exception();
    }
    this.IMBRUBAN = IMBRUBAN;
  }

  /**
   * @param cdbroker el cDBROKER a establecer
   * @throws Exception
   */
  public void setCDBROKER(BigDecimal CDBROKER) throws Exception {
    if (CDBROKER == null || !OperacionesInternacionalDWHUtils.isValid(CDBROKER, 8, 0)) {
      throw new Exception();
    }
    this.CDBROKER = CDBROKER;
  }

  /**
   * @param sinetclt el sINETCLT a establecer
   * @throws Exception
   */
  public void setIMNETCLT(BigDecimal IMNETCLT) throws Exception {
    if (IMNETCLT == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMNETCLT, 14, 2)) {
      throw new Exception();
    }
    this.IMNETCLT = IMNETCLT;
  }

  /**
   * @param sibrocon el sIBROCON a establecer
   * @throws Exception
   */
  public void setIMBROCON(BigDecimal IMBROCON) throws Exception {
    if (IMBROCON == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMBROCON, 14, 2)) {
      throw new Exception();
    }
    this.IMBROCON = IMBROCON;
  }

  /**
   * @param sicomsuc el sICOMSUC a establecer
   * @throws Exception
   */
  public void setIMCOMSUC(BigDecimal IMCOMSUC) throws Exception {
    if (IMCOMSUC == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMSUC, 14, 2)) {
      throw new Exception();
    }
    this.IMCOMSUC = IMCOMSUC;
  }

  /**
   * @param sicomcus el sICOMCUS a establecer
   * @throws Exception
   */
  public void setIMCOMCUS(BigDecimal IMCOMCUS) throws Exception {
    if (IMCOMCUS == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMCUS, 14, 2)) {
      throw new Exception();
    }
    this.IMCOMCUS = IMCOMCUS;
  }

  /**
   * @param imcomgru el iMCOMGRU a establecer
   * @throws Exception
   */
  public void setIMCOMGRU(BigDecimal IMCOMGRU) throws Exception {
    if (IMCOMGRU == null || !OperacionesInternacionalDWHUtils.isValidSigned(IMCOMGRU, 14, 2)) {
      throw new Exception();
    }
    this.IMCOMGRU = IMCOMGRU;
  }

  /**
   * @return el nUOPROUT
   */
  public BigDecimal getNUOPROUT() {
    return NUOPROUT;
  }

  /**
   * @return el nUPAREJE
   */
  public BigDecimal getNUPAREJE() {
    return NUPAREJE;
  }

  /**
   * @return el cDCLIENT
   */
  public BigDecimal getCDCLIENT() {
    return CDCLIENT;
  }

  /**
   * @return el cDENTLIQ
   */
  public String getCDENTLIQ() {
    return CDENTLIQ;
  }

  /**
   * @return el cDISINVV
   */
  public String getCDISINVV() {
    return CDISINVV;
  }

  /**
   * @return el cDBOLSAS
   */
  public String getCDBOLSAS() {
    return CDBOLSAS;
  }

  /**
   * @return el tPOPERAC
   */
  public String getTPOPERAC() {
    return TPOPERAC;
  }

  /**
   * @return el tPEJERCI
   */
  public String getTPEJERCI() {
    return TPEJERCI;
  }

  /**
   * @return el tPEJECUC
   */
  public String getTPEJECUC() {
    return TPEJECUC;
  }

  /**
   * @return el tPOPESTD
   */
  public String getTPOPESTD() {
    return TPOPESTD;
  }

  /**
   * @return el tPCONTRA
   */
  public String getTPCONTRA() {
    return TPCONTRA;
  }

  /**
   * @return el cDOPEMER
   */
  public String getCDOPEMER() {
    return CDOPEMER;
  }

  /**
   * @return el cDDERECH
   */
  public String getCDDERECH() {
    return CDDERECH;
  }

  /**
   * @return el cDPROCED
   */
  public String getCDPROCED() {
    return CDPROCED;
  }

  /**
   * @return el iMEFECTI
   */
  public BigDecimal getIMEFECTI() {
    return IMEFECTI;
  }

  /**
   * @return el fHEJECUC
   */
  public String getFHEJECUC() {
    return FHEJECUC;
  }

  /**
   * @return el nUCANEJE
   */
  public BigDecimal getNUCANEJE() {
    return NUCANEJE;
  }

  /**
   * @return el iMDERCOG
   */
  public BigDecimal getIMDERCOG() {
    return IMDERCOG;
  }

  /**
   * @return el iMDERLIQ
   */
  public BigDecimal getIMDERLIQ() {
    return IMDERLIQ;
  }

  /**
   * @return el sICOMSVB
   */
  public BigDecimal getIMCOMSVB() {
    return IMCOMSVB;
  }

  /**
   * @return el sICOMLIQ
   */
  public BigDecimal getIMCOMLIQ() {
    return IMCOMLIQ;
  }

  /**
   * @return el sICOMBRK
   */
  public String getIMCOMBRK() {
    return IMCOMBRK;
  }

  /**
   * @return el sICOMUK2
   */
  public String getIMCOMUK2() {
    return IMCOMUK2;
  }

  /**
   * @return el sICOMUSA
   */
  public String getIMCOMUSA() {
    return IMCOMUSA;
  }

  /**
   * @return el sICOMDEV
   */
  public BigDecimal getIMCOMDEV() {
    return IMCOMDEV;
  }

  /**
   * @return el nUPERIOD
   */
  public BigDecimal getNUPERIOD() {
    return NUPERIOD;
  }

  /**
   * @return el sIDBRKDA
   */
  public String getIMDBRKDA() {
    return IMDBRKDA;
  }

  /**
   * @return el cDCLIEST
   */
  public BigDecimal getCDCLIEST() {
    return CDCLIEST;
  }

  /**
   * @return el iMBRUCOG
   */
  public BigDecimal getIMBRUCOG() {
    return IMBRUCOG;
  }

  /**
   * @return el iMBRULIQ
   */
  public BigDecimal getIMBRULIQ() {
    return IMBRULIQ;
  }

  /**
   * @return el sIBRUBAN
   */
  public BigDecimal getIMBRUBAN() {
    return IMBRUBAN;
  }

  /**
   * @return el sIBANSIS
   */
  public String getIMBANSIS() {
    return IMBANSIS;
  }

  /**
   * @return el cDBROKER
   */
  public BigDecimal getCDBROKER() {
    return CDBROKER;
  }

  /**
   * @return el cDGRUPBR
   */
  public String getCDGRUPBR() {
    return CDGRUPBR;
  }

  /**
   * @return el sINETCLT
   */
  public BigDecimal getIMNETCLT() {
    return IMNETCLT;
  }

  /**
   * @return el sIBROCON
   */
  public BigDecimal getIMBROCON() {
    return IMBROCON;
  }

  /**
   * @return el sICOMSUC
   */
  public BigDecimal getIMCOMSUC() {
    return IMCOMSUC;
  }

  /**
   * @return el cDCUSGLB
   */
  public String getCDCUSGLB() {
    return CDCUSGLB;
  }

  /**
   * @return el sICOMCUS
   */
  public BigDecimal getIMCOMCUS() {
    return IMCOMCUS;
  }

  /**
   * @return el cDGESTOR
   */
  public String getCDGESTOR() {
    return CDGESTOR;
  }

  /**
   * @return el cDUNIGEF
   */
  public String getCDUNIGEF() {
    return CDUNIGEF;
  }

  /**
   * @return el cDUNIGEB
   */
  public String getCDUNIGEB() {
    return CDUNIGEB;
  }

  /**
   * @return el iMCOMGRU
   */
  public BigDecimal getIMCOMGRU() {
    return IMCOMGRU;
  }

  /**
   * @return el cDGRUREP
   */
  public String getCDGRUREP() {
    return CDGRUREP;
  }

  /**
   * @return el cDUNICON
   */
  public String getCDUNICON() {
    return CDUNICON;
  }

  /**
   * @return el sIREPGES
   */
  public String getIMREPGES() {
    return IMREPGES;
  }

  /**
   * @return el sIREPREC
   */
  public String getIMREPREC() {
    return IMREPREC;
  }

  /**
   * @return el sIREPCON
   */
  public String getIMREPCON() {
    return IMREPCON;
  }

  /**
   * get NUOPROUT as String
   * @throws Exception
   */
  public String getNUOPROUTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUOPROUT(), "00000000");
  }

  /**
   * get NUPAREJE as String
   * @throws Exception
   */
  private String getNUPAREJEAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUPAREJE(), "0000");
  }

  /**
   * get CDCLIENT as String
   * @throws Exception
   */
  private String getCDCLIENTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getCDCLIENT(), "00000000");
  }

  /**
   * get IMEFECTI as String
   * @throws Exception
   */
  private String getIMEFECTIAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMEFECTI(), "000000000000000.00");
  }

  /**
   * get NUCANEJE as String
   * @throws Exception
   */
  private String getNUCANEJEAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUCANEJE(), "000000000000000.00");
  }

  /**
   * get IMDERCOG as String
   * @throws Exception
   */
  private String getIMDERCOGAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMDERCOG(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get IMDERLIQ as String
   * @throws Exception
   */
  private String getIMDERLIQAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMDERLIQ(), "000000000000000.00");
  }

  /**
   * get SICOMSVB as String
   * @throws Exception
   */
  private String getIMCOMSVBAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMSVB(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get SICOMLIQ as String
   * @throws Exception
   */
  private String getIMCOMLIQAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMLIQ(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get SICOMDEV as String
   * @throws Exception
   */
  private String getIMCOMDEVAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMDEV(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get NUPERIOD as String
   * @throws Exception
   */
  private String getNUPERIODAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getNUPERIOD(), "00000000");
  }

  /**
   * get CDCLIEST as String
   * @throws Exception
   */
  private String getCDCLIESTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getCDCLIEST(), "00000000");
  }

  /**
   * get IMBRUCOG as String
   * @throws Exception
   */
  private String getIMBRUCOGAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMBRUCOG(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get IMBRULIQ as String
   * @throws Exception
   */
  private String getIMBRULIQAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMBRULIQ(), "000000000000000.00");
  }

  /**
   * get SIBRUBAN as String
   * @throws Exception
   */
  private String getIMBRUBANAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMBRUBAN(), "'+'000000000000000.00;'-'000000000000000.00");
  }

  /**
   * get CDBROKER as String
   * @throws Exception
   */
  private String getCDBROKERAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getCDBROKER(), "00000000");
  }

  /**
   * get SINETCLT as String
   * @throws Exception
   */
  private String getIMNETCLTAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMNETCLT(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get SIBROCON as String
   * @throws Exception
   */
  private String getIMBROCONAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMBROCON(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get SICOMSUC as String
   * @throws Exception
   */
  private String getIMCOMSUCAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMSUC(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get SICOMCUS as String
   * @throws Exception
   */
  private String getIMCOMCUSAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMCUS(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  /**
   * get IMCOMGRU as String
   * @throws Exception
   */
  private String getIMCOMGRUAsString() throws Exception {
    return OperacionesInternacionalDWHUtils.getAsString(getIMCOMGRU(), "'+'00000000000000.00;'-'00000000000000.00");
  }

  public String getAsString(int fieldPos) throws Exception {
    String result = "";
    switch (fieldPos) {
      case DwhConstantes.INSTITUCIONAL_NUOPROUT:
        result = getNUOPROUTAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_NUPAREJE:
        result = getNUPAREJEAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_CDCLIENT:
        result = getCDCLIENTAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_CDENTLIQ:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDENTLIQ(), 4);
        break;
      case DwhConstantes.INSTITUCIONAL_CDISINVV:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDISINVV(), 12);
        break;
      case DwhConstantes.INSTITUCIONAL_CDBOLSAS:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getCDBOLSAS(), 3);
        break;
      case DwhConstantes.INSTITUCIONAL_TPOPERAC:
        result = getTPOPERAC();
        break;
      case DwhConstantes.INSTITUCIONAL_TPEJERCI:
        result = getTPEJERCI();
        break;
      case DwhConstantes.INSTITUCIONAL_TPEJECUC:
        result = getTPEJECUC();
        break;
      case DwhConstantes.INSTITUCIONAL_TPOPESTD:
        result = getTPOPESTD();
        break;
      case DwhConstantes.INSTITUCIONAL_TPCONTRA:
        result = getTPCONTRA();
        break;
      case DwhConstantes.INSTITUCIONAL_CDOPEMER:
        result = getCDOPEMER();
        break;
      case DwhConstantes.INSTITUCIONAL_CDDERECH:
        result = getCDDERECH();
        break;
      case DwhConstantes.INSTITUCIONAL_CDPROCED:
        result = getCDPROCED();
        break;
      case DwhConstantes.INSTITUCIONAL_FHEJECUC:
        result = OperacionesInternacionalDWHUtils.fillFieldString(getFHEJECUC(), 10);
        break;
      case DwhConstantes.INSTITUCIONAL_IMEFECTI:
        result = getIMEFECTIAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_NUCANEJE:
        result = getNUCANEJEAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMDERCOG:
        result = getIMDERCOGAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMDERLIQ:
        result = getIMDERLIQAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMSVB:
        result = getIMCOMSVBAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMLIQ:
        result = getIMCOMLIQAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMBRK:
        result = getIMCOMBRK();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMUK2:
        result = getIMCOMUK2();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMUSA:
        result = getIMCOMUSA();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMDEV:
        result = getIMCOMDEVAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_NUPERIOD:
        result = getNUPERIODAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMDBRKDA:
        result = getIMDBRKDA();
        break;
      case DwhConstantes.INSTITUCIONAL_CDCLIEST:
        result = getCDCLIESTAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMBRUCOG:
        result = getIMBRUCOGAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMBRULIQ:
        result = getIMBRULIQAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMBRUBAN:
        result = getIMBRUBANAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMBANSIS:
        result = getIMBANSIS();
        break;
      case DwhConstantes.INSTITUCIONAL_CDBROKER:
        result = getCDBROKERAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_CDGRUPBR:
        result = getCDGRUPBR();
        break;
      case DwhConstantes.INSTITUCIONAL_IMNETCLT:
        result = getIMNETCLTAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMBROCON:
        result = getIMBROCONAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMSUC:
        result = getIMCOMSUCAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_CDCUSGLB:
        result = getCDCUSGLB();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMCUS:
        result = getIMCOMCUSAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_CDGESTOR:
        result = getCDGESTOR();
        break;
      case DwhConstantes.INSTITUCIONAL_CDUNIGEF:
        result = getCDUNIGEF();
        break;
      case DwhConstantes.INSTITUCIONAL_CDUNIGEB:
        result = getCDUNIGEB();
        break;
      case DwhConstantes.INSTITUCIONAL_IMCOMGRU:
        result = getIMCOMGRUAsString();
        break;
      case DwhConstantes.INSTITUCIONAL_CDGRUREP:
        result = getCDGRUREP();
        break;
      case DwhConstantes.INSTITUCIONAL_CDUNICON:
        result = getCDUNICON();
        break;
      case DwhConstantes.INSTITUCIONAL_IMREPGES:
        result = getIMREPGES();
        break;
      case DwhConstantes.INSTITUCIONAL_IMREPREC:
        result = getIMREPREC();
        break;
      case DwhConstantes.INSTITUCIONAL_IMREPCON:
        result = getIMREPCON();
        break;

    }
    if (result == null)
      throw new Exception();
    return result;
  }

  /**
   * 
   */
  public String toString() {
    String result = "";
    for (int i = 1; i < 49; i++) {
      try {
        result += getAsString(i);
      }
      catch (Exception e) {
        LOG.debug("Error Institucionales", e);
      }
    }
    return result;

  }

}
