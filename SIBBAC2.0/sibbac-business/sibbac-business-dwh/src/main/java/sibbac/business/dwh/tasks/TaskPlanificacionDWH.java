package sibbac.business.dwh.tasks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.dwh.ficheros.callable.results.CallableListProcessFileResult;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.commons.bo.TestigoFileBo;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableGeneracionEjecucionesBo;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableGeneracionEstaticosBo;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableGeneracionTraAccBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_FICHERO_PLANIFICACION_DWH, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 22 ? * MON-FRI")
public class TaskPlanificacionDWH extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOGGER = LoggerFactory.getLogger(TaskPlanificacionDWH.class);

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${dwh.ficheros.nacional.batch.timeout.global: 90}")
  private int timeout;

  @Autowired
  private TestigoFileBo testigoFileBo;

  @Autowired
  private ApplicationContext ctx;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    List<ProcessFileResultDTO> listProcessResultDTO = null;

    try {
      final long initStartTime = System.currentTimeMillis();

      listProcessResultDTO = launchPlanificator();

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(String.format("El planificador ha terminado, ejecución en %d milisegundos",
            System.currentTimeMillis() - initStartTime));
      }
    }
    catch (InterruptedException | ExecutionException e) {
      LOGGER.error("Se ha producido un error durante la ejecucion de los hilos", e);
    }

    // Generamos el testigo que nos certifique si se han creado todos los
    // ficheros
    final File generatedFile = testigoFileBo.generateTestigoFile(listProcessResultDTO);
    if (generatedFile != null) {
      LOGGER.debug("Fichero testigo generado es '" + generatedFile.getAbsolutePath() + "'");
    } else {
      LOGGER.debug("Fichero testigo no ha sido generado");
    }
  }

  /**
   * Lanza el planificador
   * @throws SIBBACBusinessException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  private List<ProcessFileResultDTO> launchPlanificator()
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final List<CallableListProcessFileResult> processInterfaces = new ArrayList<>();

    // Lanzamos en paralelo las tres tareas
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    // 1. Obtenemos el bean para la tarea de Ejecuciones
    processInterfaces.add(ctx.getBean(CallableGeneracionEjecucionesBo.class));

    // 2. Obtenemos el bean para la tarea Traders y Accounters
    processInterfaces.add(ctx.getBean(CallableGeneracionTraAccBo.class));

    // 2. Obtenemos el bean para la tarea de Estáticos
    processInterfaces.add(ctx.getBean(CallableGeneracionEstaticosBo.class));

    return launchThreadBatch(processInterfaces);
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   * @throws ExecutionException
   */
  public List<ProcessFileResultDTO> launchThreadBatch(List<CallableListProcessFileResult> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final List<ProcessFileResultDTO> listProcessResultDTO = new ArrayList<>();

    final List<CallableListProcessFileResult> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(3, new NamedThreadFactory("batch_Planificador"));

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableListProcessFileResult callableBo : processInterfaces) {
      callableBo.setFuture(executor.submit(callableBo));
      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threads
    executor.shutdown();

    final boolean onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Si se ha superadFo el tiempo estipulado para la ejecucion de los
    // ficheros cancelamos los hilos
    if (onTime) {
      for (CallableListProcessFileResult processInterface : futureList) {
        listProcessResultDTO.addAll(processInterface.getFuture().get());
      }
    }
    else {
      for (CallableListProcessFileResult future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }

    return listProcessResultDTO;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_PLANIFICACION_DWH;
  }

}
