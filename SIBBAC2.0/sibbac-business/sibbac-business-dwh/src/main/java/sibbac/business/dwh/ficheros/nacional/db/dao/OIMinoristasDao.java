package sibbac.business.dwh.ficheros.nacional.db.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0brk;
import sibbac.business.fase0.database.model.Tmct0est;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctdId;

@Repository
public interface OIMinoristasDao extends JpaRepository<Tmct0ctd, Tmct0ctdId> {

  public static final String MINORISTAS_QUERY = "select CTD.CDALIAS, CTD.FETRADE, CTD.NUOPROUT, CTD.NUPAREJE, "
      + "CTD.CDCLEARE, CTD.CDISIN, CTD.NUCNFCLT, CTD.CDMERCAD, CTD.NUORDEN, CTD.CDBROKER, CTD.NBOOKING, CTD.CDTPOPER, "
      + "CTD.EUTOTBRU, CTD.NUTITAGR AS COL14, CTD.EUCOMDEV, CTD.EUTOTNET, CTD.IMFIBREU, CTD.FHAUDIT, CTD.IMSVBEU from TMCT0CTD CTD "
      + "INNER JOIN TMCT0CTA CTA ON CTD.NUCNFCLT=CTA.NUCNFCLT AND CTD.CDMERCAD=CTA.CDMERCAD AND CTD.NUORDEN=CTA.NUORDEN AND CTD.CDBROKER=CTA.CDBROKER AND CTD.NBOOKING=CTA.NBOOKING "
      + "INNER JOIN TMCT0CTG CTG ON CTA.NBOOKING=CTG.NBOOKING AND CTA.CDBROKER=CTG.CDBROKER AND CTA.NUORDEN=CTG.NUORDEN AND CTA.CDMERCAD=CTG.CDMERCAD "
      + "INNER JOIN TMCT0CTO CTO ON CTG.CDMERCAD=CTO.CDMERCAD AND CTG.NUORDEN=CTO.NUORDEN AND CTG.CDBROKER=CTO.CDBROKER "
      + "INNER JOIN TMCT0ORD ORD ON CTO.NUORDEN=ORD.NUORDEN INNER JOIN TMCT0STA STA ON CTD.CDESTADO=STA.CDESTADO AND CTD.CDTIPEST=STA.CDTIPEST "
      + "WHERE STA.CDESTADO >= :estado AND ORD.CDCLSMDO = 'E' AND CTD.FETRADE >= :tradeini AND CTD.FETRADE <= :tradefin "
      + "ORDER BY ORD.NUORDEN ASC, CTO.CDBROKER ASC, CTO.CDMERCAD ASC, CTG.NBOOKING ASC, CTA.NUCNFCLT ASC, CTD.NUALOSEC ASC";

  public static final String MINORISTAS_PAGE_QUERY = MINORISTAS_QUERY + " LIMIT :offset, :pageSize";

  public static final String MINORISTAS_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + MINORISTAS_QUERY + ")";

  @Query(value = MINORISTAS_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListMinoristas(@Param("estado") String estado, @Param("tradeini") Date tradeini,
      @Param("tradefin") Date tradefin, @Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

  @Query(value = MINORISTAS_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListMinoristas(@Param("estado") String estado, @Param("tradeini") Date tradeini,
      @Param("tradefin") Date tradefin);

  public static final String CDMERCAD_QUERY = "select CTO.CDMERCAD from TMCT0CTD CTD"
      + "INNER JOIN TMCT0CTA CTA ON CTD.NUCNFCLT=CTA.NUCNFCLT AND CTD.CDMERCAD=CTA.CDMERCAD AND CTD.NUORDEN=CTA.NUORDEN AND CTD.CDBROKER=CTA.CDBROKER AND CTD.NBOOKING=CTA.NBOOKING"
      + "INNER JOIN TMCT0CTG CTG ON CTA.NBOOKING=CTG.NBOOKING AND CTA.CDBROKER=CTG.CDBROKER AND CTA.NUORDEN=CTG.NUORDEN AND CTA.CDMERCAD=CTG.CDMERCAD"
      + "INNER JOIN TMCT0CTO CTO ON CTG.CDMERCAD=CTO.CDMERCAD AND CTG.NUORDEN=CTO.NUORDEN AND CTG.CDBROKER=CTO.CDBROKER"
      + "WHERE CTD.NUORDEN=:nuorden     AND CTD.CDBROKER=:cdbroker AND CTD.NBOOKING=:nbooking AND CTD.NUCNFCLT=:nucnfclt AND CTD.NUALOSEC=:nualosec ";

  @Query(value = CDMERCAD_QUERY, nativeQuery = true)
  public List<String> getCdMercad(@Param("nuorden") String nuorden, @Param("cdbroker") String cdbroker,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nualosec") Integer nualosec);

  public static final String ALICDBROCLI_QUERY = "select ALI.CDBROCLI from TMCT0ALI ALI WHERE ALI.CDALIASS=:cdaliass AND FHINICIO <=:fetrade AND FHFINAL >=:fetrade";

  @Query(value = ALICDBROCLI_QUERY, nativeQuery = true)
  public List<String> getAliCdBrokli(@Param("cdaliass") String cdaliass, @Param("fetrade") BigDecimal fetrade);

  public static final String CLITPCLIENN_QUERY = "select CLI.TPCLIENN FROM TMCT0CLI CLI WHERE CLI.CDBROCLI=:cdbrocli AND FHINICIO <=:fetrade AND FHFINAL >=:fetrade";

  @Query(value = CLITPCLIENN_QUERY, nativeQuery = true)
  public List<String> getCliTpClienn(@Param("cdbrocli") String cdbrocli, @Param("fetrade") BigDecimal fetrade);

  public static final String TMCT0EST_QUERY = "select EST FROM TMCT0EST EST WHERE EST.CDALIASS=:cdaliass AND EST.FHINICIO <=:fetrade AND EST.FHFINAL >=:fetrade";

  @Query(value = TMCT0EST_QUERY, nativeQuery = true)
  public Tmct0est getTmct0est(@Param("cdaliass") String cdaliass, @Param("fetrade") BigDecimal fetrade);

  public static final String TMCT0BRK_QUERY = "select * FROM TMCT0EST WHERE CDBROKER=:cdbroker";

  @Query(value = TMCT0BRK_QUERY, nativeQuery = true)
  public List<Tmct0brk> getTmct0brk(@Param("cdbroker") String cdbroker);

  public static final String CDBROKER_QUERY = "SELECT TRIM(CDIDENTI) FROM TMCT0BRK WHERE CDBROKER=:cdbroker";

  @Query(value = CDBROKER_QUERY, nativeQuery = true)
  public String getCdBroker(@Param("cdbroker") String cdbroker);

  public static final String CDBOLSAS_QUERY = "select inforas4 from tmct0xas where nbcamas4='CDWAREHO' and nbcamxml='CDMERCAD' and inforxml=:inforxmlBolsa";

  @Query(value = CDBOLSAS_QUERY, nativeQuery = true)
  public String getCdBolsas(@Param("inforxmlBolsa") String inforxmlBolsa);

}
