package sibbac.business.dwh.tasks;

import java.util.ArrayList;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.valores.bo.FicherosValoresBo;
import sibbac.business.dwh.ficheros.valores.bo.GenerateAgrupacionTipoValorFile;
import sibbac.business.dwh.ficheros.valores.bo.GenerateAutexFile;
import sibbac.business.dwh.ficheros.valores.bo.GenerateFicheroValoresFile;
import sibbac.business.dwh.ficheros.valores.bo.GenerateGrupoValoresFile;
import sibbac.business.dwh.ficheros.valores.bo.GenerateTipoValoresFile;
import sibbac.business.dwh.tasks.commons.TaskProcessDWH;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_ENVIO_FICHEROS_DWH_VALORES, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 23 ? * MON-FRI")

public class TaskGeneracionDWHValores extends WrapperTaskConcurrencyPrevent implements TaskProcessDWH {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOGGGER = LoggerFactory.getLogger(TaskGeneracionDWHValores.class);

  @Value("${dwh.ficheros.valores.db.default.page.size:200}")
  private int dbPageSize;

  /**
   * Nombre del fichero para la generacion del fichero de Grupo de Clientes
   * Estáticos
   */
  @Value("${dwh.ficheros.valores.generated_if_empty:true}")
  private Boolean generateIfEmpty;
  /**
   * Ruta donde se deposita el fichero
   */
  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String outPutDir;

  /**
   * Nombre del fichero generado por el proceso grupo de valores
   */
  @Value("${dwh.ficheros.valores.grupo_valores.file:fdwh0gvl.txt}")
  private String fileNameGrupoValores;

  /**
   * Nombre del fichero generado por el proceso tipo de valores
   */
  @Value("${dwh.ficheros.valores.tipo_valores.file:fdwh0atv.txt}")
  private String fileNameTipoValores;

  /**
   * Nombre del fichero generado por el proceso agrupación de valores
   */
  @Value("${dwh.ficheros.valores.agrupo_valores.file:fdwh0tvl.txt}")
  private String fileNameAgrupacionTipoValor;

  /**
   * Nombre del fichero generado por el proceso fichero de valores
   */
  @Value("${dwh.ficheros.valores.fichero_valores.file:fdwh0vlr.txt}")
  private String fileNameFicheroValores;

  /**
   * Nombre del fichero generado por el proceso Autex
   */
  @Value("${dwh.ficheros.valores.autex_valores.file:fdwh0atx.txt}")
  private String fileNameAutex;

  /**
   * Generador de Ficheros de Valores Grupo de Valores
   */
  @Autowired
  private GenerateGrupoValoresFile generateGrupoValoresFile;

  /**
   * Generador de Ficheros de Valores Grupo de Valores
   */
  @Autowired
  private GenerateTipoValoresFile generateTipoValoresFile;

  /**
   * Generador de Ficheros de Valores Grupo de Valores
   */
  @Autowired
  private GenerateAgrupacionTipoValorFile generateAgrupacionTipoValorFile;

  /**
   * Generador de Ficheros de Valores Grupo de Valores
   */
  @Autowired
  private GenerateFicheroValoresFile generateFicheroValoresFile;

  /**
   * Generador de Ficheros de AUTEX
   */
  @Autowired
  private GenerateAutexFile generateAutexFile;

  /**
   * Generador de Ficheros de Valores
   */
  @Autowired
  private FicherosValoresBo ficherosValoresBo;

  /**
   * Listado de resultados de la generacion de ficheros
   */
  private List<ProcessFileResultDTO> processFileResultsList;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    this.launchProcess();
  }

  @Override
  public void launchProcess() throws SIBBACBusinessException {
    processFileResultsList = new ArrayList<>();

    final long initStartTime = System.currentTimeMillis();

    processFileResultsList.addAll(launchValores());

    if (LOG.isDebugEnabled()) {
      LOG.debug(
          String.format("El proceso ha terminado, se ha ejecutado en %d", System.currentTimeMillis() - initStartTime));
    }
  }

  /**
   * Lanza el proceso de Valores
   * @throws SIBBACBusinessException
   */
  public List<ProcessFileResultDTO> launchValores() throws SIBBACBusinessException {
    final List<ProcessFileResultDTO> results = new ArrayList<>();
    try {      
      ProcessFileResultDTO processFileResultDTO = null;

      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.debug("Se inicia la tarea");
      }

      ficherosValoresBo.cargaValores();

      processFileResultDTO = new ProcessFileResultDTO();

      // Generamos el primer fichero 'Grupo de Valor'
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      generateGrupoValoresFile.setDbPageSize(dbPageSize);
      generateGrupoValoresFile.setGenerateIfEmpty(generateIfEmpty);
      generateGrupoValoresFile.generateFile(outPutDir, fileNameGrupoValores);

      processFileResultDTO.setFileNameProcess(generateGrupoValoresFile.getFileName());
      processFileResultDTO.setFileResult(generateGrupoValoresFile.getFileGenerationResult());
      results.add(processFileResultDTO);

      // Generamos el primer fichero 'Agrupación Tipo de Valor'
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateAgrupacionTipoValorFile.setDbPageSize(dbPageSize);
      generateAgrupacionTipoValorFile.setGenerateIfEmpty(generateIfEmpty);
      generateAgrupacionTipoValorFile.generateFile(outPutDir, fileNameAgrupacionTipoValor);

      processFileResultDTO.setFileNameProcess(generateAgrupacionTipoValorFile.getFileName());
      processFileResultDTO.setFileResult(generateAgrupacionTipoValorFile.getFileGenerationResult());
      results.add(processFileResultDTO);

      // Generamos el primer fichero 'Tipo de Valor'
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateTipoValoresFile.setDbPageSize(dbPageSize);
      generateTipoValoresFile.setGenerateIfEmpty(generateIfEmpty);
      generateTipoValoresFile.generateFile(outPutDir, fileNameTipoValores);

      processFileResultDTO.setFileNameProcess(generateTipoValoresFile.getFileName());
      processFileResultDTO.setFileResult(generateTipoValoresFile.getFileGenerationResult());
      results.add(processFileResultDTO);

      // Generamos el primer fichero 'Valor'
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateFicheroValoresFile.setDbPageSize(dbPageSize);
      generateFicheroValoresFile.setGenerateIfEmpty(generateIfEmpty);
      generateFicheroValoresFile.generateFile(outPutDir, fileNameFicheroValores);

      processFileResultDTO.setFileNameProcess(generateFicheroValoresFile.getFileName());
      processFileResultDTO.setFileResult(generateFicheroValoresFile.getFileGenerationResult());
      results.add(processFileResultDTO);

      // Generamos el primer fichero 'Autex'
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateAutexFile.setDbPageSize(dbPageSize);
      generateAutexFile.setGenerateIfEmpty(generateIfEmpty);
      generateAutexFile.generateFile(outPutDir, fileNameAutex);

      processFileResultDTO.setFileNameProcess(generateAutexFile.getFileName());
      processFileResultDTO.setFileResult(generateAutexFile.getFileGenerationResult());
      results.add(processFileResultDTO);

      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.info("Se termina la tarea");
      }
    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(e);
    }

    return results;
  }

  /**
   * Obtiene los resultados de la generación de los ficheros
   * @return
   */
  @Override
  public List<ProcessFileResultDTO> getProcessFileResultsList() {
    return processFileResultsList;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_FICHERO_VALORES;
  }
}
