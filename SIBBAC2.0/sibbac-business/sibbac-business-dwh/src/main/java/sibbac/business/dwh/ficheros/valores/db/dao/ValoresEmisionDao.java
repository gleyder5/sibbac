package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.valores.db.model.ValoresEmision;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ValoresEmisionDao extends JpaRepository<ValoresEmision, String> {

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  @Modifying
  @Query(value = "INSERT INTO TMCT0_VALORES_EMISION (CDEMISIO, CDISIN, CDALFVAL, DSEMISIO, CDACTIVO, CDCAMBIO, CDFORMAT, FEEMISIO, IMNOMINA, CDMONNOM, IMCAPITA, CDCAPITA, CDSTLIQI, CLASEVAL, CDNATEMI, CDDGCI, SITUVAL, CDESTADO, FEESTADO, ADUSUAR, FHAUDIT, USUAUDIT) "
      + "values (:entities);"
      , nativeQuery = true)
  public void saveAllBulk(@Param("entities") List<ValoresEmision> entities);

}
