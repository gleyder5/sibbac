package sibbac.business.dwh.ficheros.nacional.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.nacional.db.dao.FicheroNacionalDao;
import sibbac.business.dwh.ficheros.nacional.db.model.Tmct0DwhEjecuciones;
import sibbac.business.dwh.ficheros.nacional.dto.ClienteNacionalDTO;
import sibbac.business.dwh.ficheros.nacional.dto.DesgloseNacionalDTO;
import sibbac.business.dwh.ficheros.nacional.dto.InstitucionalDTO;
import sibbac.business.dwh.ficheros.nacional.dto.MinoristaDTO;
import sibbac.business.dwh.ficheros.nacional.dto.OperacionAutexDTO;
import sibbac.business.dwh.ficheros.nacional.dto.OperacionNacionalDTO;
import sibbac.business.dwh.unbundled.db.dao.UnbundledDao;
import sibbac.business.dwh.unbundled.db.model.Unbundled;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.common.SIBBACBusinessException;

@Component
public class FicheroNacionalBo {

  private static final Logger LOG = LoggerFactory.getLogger(FicheroNacionalBo.class);

  @Autowired
  FicheroNacionalDao ficheroNacionalDao;

  @Autowired
  private UnbundledDao unbundledDao;

  @Autowired
  GenerateOIMinoristasFile generateOIMinoristasFile;

  @Autowired
  GenerateOIInstitucionalesFile generateOIInstitucionalesFile;

  public Date fechaInicio;

  public Date fechaFin;

  public Date fechaEjecucion;

  public Date calculateDates(Date fechaEjec) throws SIBBACBusinessException {

    return fechaFin = getDateBySubstractDays(fechaEjec, 17);
  }

  /**
   * Método para obtener la fecha de Inicio
   * 
   * @param fecha
   * @return
   * @throws SIBBACBusinessException
   */
  public Date extraccionParametrizacion(Date fecha) {
    this.fechaEjecucion = fecha;

    String keyValue = ficheroNacionalDao.queryParametrizacionActual(fecha);

    if (keyValue != null) {
      fechaInicio = calculateNewDate(keyValue, fecha);
      return fechaInicio;

    }
    else {
      LOG.debug("Parámetros no dados de alta en CFG");
      return null;
    }
  }

  /**
   * Método que dependiendo de si es días o meses por su Keyvalue se resta a la
   * fecha actual para obtener la fecha de inicio
   * 
   * @param keyValue
   * @param fechaHasta
   * @return
   */
  public Date calculateNewDate(String keyValue, Date fechaHasta) {
    Date fechaRestada = null;

    String tipoUnidad = Character.toString(keyValue.charAt(0));
    Integer valorResta = Character.getNumericValue(keyValue.charAt(2));

    switch (tipoUnidad) {

      case "D":
        fechaRestada = getDateBySubstractDays(fechaHasta, valorResta);
        LOG.debug("Cálculo de nueva fecha restándole días");
        break;
      case "M":
        fechaRestada = getStartMonthBySubstractMonth(fechaHasta, valorResta);
        LOG.debug("Calcula de nueva fecha restándole meses");
        break;
      default:
        LOG.debug("Error, el valor de keyValue no es ni D ni M");
    }
    return fechaRestada;
  }

  /**
   * Devuelve la fecha que se pasa por parámetro menos los días a restar
   * @param fecha
   * @param dias
   * @return
   */
  public Date getDateBySubstractDays(Date fecha, int dias) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha);
    calendar.add(Calendar.DAY_OF_YEAR, -dias);

    return new Date(calendar.getTime().getTime());
  }

  /**
   * Devuelve la fecha que se pasa por parámetro menos los días a restar pero
   * sin contar los fines de semana, solo de lunes a viernes
   * @param fecha
   * @param dias
   * @return
   */
  public Date getDateBySubstractWorkingDays(Date fecha, int dias) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha);
    for (int i = 1; i < (dias + 1);) {
      calendar.add(Calendar.DAY_OF_YEAR, -1);
      if (!(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
          || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
        i = i + 1;
      }
    }
    return new Date(calendar.getTime().getTime());
  }

  /**
   * Devuelve la fecha que pasa por parámetro menos los meses a restar. Pone el
   * día como 1 de mes.
   * @param fecha
   * @param meses
   * @return
   */
  public Date getStartMonthBySubstractMonth(Date fecha, int meses) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha);
    calendar.add(Calendar.MONTH, -meses);
    calendar.set(Calendar.DAY_OF_MONTH, 1);

    return new Date(calendar.getTime().getTime());
  }

  /**
   * Método para obtener las operaciones nacional de la base de datos
   * @return List<OperacionNacionalDTO>
   */
  public List<OperacionNacionalDTO> getOperacionesNacional() {
    List<Object[]> operations = ficheroNacionalDao.findOperacionesNacional(fechaFin, fechaInicio);
    List<OperacionNacionalDTO> operaciones = new ArrayList<>();
    for (Object[] op : operations) {

      operaciones.add(OperacionNacionalDTO.mapObject(op));

      getDesgloseNacional(operaciones.get(0));
      getClienteNacional(operaciones.get(0));

    }
    return operaciones;
  }

  /**
   * Método para obtener los desgloses nacional desde la base de datos
   * @param operacion
   */
  public void getDesgloseNacional(OperacionNacionalDTO operacion) {
    DesgloseNacionalDTO desglose;
    List<Object[]> des = ficheroNacionalDao.findDesgloseNacional(operacion.getNuorden(), operacion.getNbooking(),
        operacion.getNucnfclt(), operacion.getNucnfliq());
    if (!des.isEmpty()) {
      desglose = DesgloseNacionalDTO.mapObject(des.get(0));
    }
    else {
      desglose = DesgloseNacionalDTO.initialize();
      LOG.info(String.format(
          "No se ha encontrado desglose para esta operacion: nuorden:%s, nbooking: %s,nucnfclt: %s, nucnfliq: %s",
          operacion.getNuorden(), operacion.getNbooking(), operacion.getNucnfclt(), operacion.getNucnfliq()));
    }
  }

  /**
   * Método para obtener los clientes nacional de la base de datos
   * @param operacion
   */
  public void getClienteNacional(OperacionNacionalDTO operacion) {
    ClienteNacionalDTO cliente;
    List<Object[]> cli = ficheroNacionalDao.findClienteNacional(operacion.getNuorden(), operacion.getNbooking(),
        operacion.getNucnfclt(), operacion.getNucnfliq());
    if (!cli.isEmpty()) {
      cliente = ClienteNacionalDTO.mapObject(cli.get(0));
    }
    else {
      cliente = ClienteNacionalDTO.initialize();
      LOG.info(String.format(
          "No se ha encontrado cliente para esta operacion: nuorden:%s, nbooking: %s,nucnfclt: %s, nucnfliq: %s",
          operacion.getNuorden(), operacion.getNbooking(), operacion.getNucnfclt(), operacion.getNucnfliq()));

    }
  }

  /**
   * Método para formatear la fecha
   * 
   * @param pattern
   * @param date
   * @return String
   */
  public String formatDate(String pattern, Date date) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String fecha = simpleDateFormat.format(date);

    return fecha;
  }

  /**
   * Genera una cabecera estática de 500 posiciones con formato:
   * ---------1---------2---------3---------4---------5---------6....
   * @return String
   */
  public String headerFechaFile() {

    String headerBuilder = null;
    final StringBuilder rowLine = new StringBuilder();
    for (int i = 1; i < 51; i++) {
      headerBuilder = String.format("% 10d", i).replace(" ", "-");
      rowLine.append(headerBuilder);
    }

    return String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
  }

  private static final String NACIONAL_LINE_SEPARATOR = "\r\n";

  private final static String MAP_NUOPROUT_CDALIAS = "23573";
  private final static String MAP_NUOPROUT_NUCNFCLT_PREFIX = "PARTENON%";
  private final static String MAP_NUOPROUT_CONCAT_PREFIX = "10000";
  private final static String MAP_NUOPROUT_CONCAT_SUFIX = "0001";

  private final static String MAP_CUENTA_PREFIX = "008";

  private final static String MAP_BOLSA_M = "9838";
  private final static String MAP_BOLSA_V = "9370";
  private final static String MAP_BOLSA_B = "9472";
  private final static String MAP_BOLSA_L = "9577";
  private final static String MAP_BOLSA_ME = "CHIX";
  private final static String MAP_BOLSA_BE = "BATE";
  private final static String MAP_BOLSA_TE = "TRQX";

  private final static String MAP_TPMERCAD = "C";
  private final static String MAP_TPCONTRA = "T";
  private final static String MAP_CDOPEMER = "RDMA    ";
  private final static String MAP_CDPROCED = "D";
  private final static String MAP_NUEJECUC = "0000000000000010000";
  private final static String MAP_TPCARGAS = "C";

  private final static String MAP_CUENTA_CW = "014";
  private final static String MAP_CUENTA_CV_EO = "008";
  private final static String MAP_CUENTA_DEFAULT = "012";

  private final static String MAP_ORIGEN_FI = "FIX";
  private final static String MAP_ORIGEN_OTHER = "ROUTING";
  private final static String MAP_ORIGEN_DEFAULT = "F  ";
  private final static String MAP_ORIGEN_RI = "4716";
  private final static String MAP_ORIGEN_RK = "22672";
  private final static String MAP_ORIGEN_RB = "22645";

  private final static String MAP_UNBUNDLED_NUMPAREJE = "1";
  private final static String MAP_UNBUNDLED_DEFAULT_NUMBER = "0";
  private final static String MAP_UNBUNDLED_DEFAULT_STRING = " ";
  private final static String MAP_UNBUNDLED_CDENTLIQ = "38";
  private final static String MAP_UNBUNDLED_CDISINVV = "ES0UNBLUNDED";
  private final static String MAP_UNBUNDLED_BOLSA = "M";
  private final static String MAP_UNBUNDLED_TPOPERAC = "C";
  private final static String MAP_UNBUNDLED_TPMERCAD = "C";
  private final static String MAP_UNBUNDLED_TEJERCI = "CV";
  private final static String MAP_UNBUNDLED_TPEJECUC = "V";
  private final static String MAP_UNBUNDLED_TPOPESTD = "N";
  private final static String MAP_UNBUNDLED_TPCONTRA = "T";
  private final static String MAP_UNBUNDLED_CDDERECH = "S";
  private final static String MAP_UNBUNDLED_CDPROCED = "D";
  private final static String MAP_UNBUNDLED_NUEJECUC = "0000000000000010000";
  private final static String MAP_UNBUNDLED_SUBMER_ID = "ZZ";
  private final static String MAP_UNBUNDLED_CDORIGEN = "ZZZ";

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void writeOperacionesLines(List<OperacionNacionalDTO> operaciones, List<Tmct0DwhEjecuciones> ejecuciones,
      BufferedWriter writer) throws SIBBACBusinessException, IOException {
    List<Tmct0DwhEjecuciones> ejecucionesTemp = new ArrayList<Tmct0DwhEjecuciones>();
    for (OperacionNacionalDTO operac : operaciones) {
      DesgloseNacionalDTO desglose;
      Integer contNbooking = 0;
      List<Object[]> des = unbundledDao.findDesgloseNacional(operac.getNuorden(), operac.getNbooking(),
          operac.getNucnfclt(), operac.getNucnfliq());

      if (!des.isEmpty()) {
        for (Object[] desTemp : des) {
          desglose = DesgloseNacionalDTO.mapObject(desTemp);
          contNbooking = contNbooking + 1;
          String line = getLineExecution(operac, desglose, contNbooking);
          writer.append(
              (String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line)) + NACIONAL_LINE_SEPARATOR);
        }
      }
      else {
        desglose = DesgloseNacionalDTO.initialize();
        LOG.info(String.format(
            "No se ha encontrado desglose para esta operacion: nuorden:%s, nbooking: %s,nucnfclt: %s, nucnfliq: %s",
            operac.getNuorden(), operac.getNbooking(), operac.getNucnfclt(), operac.getNucnfliq()));
        contNbooking = 1;
        String line = getLineExecution(operac, desglose, contNbooking);
        writer.append(
            (String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line)) + NACIONAL_LINE_SEPARATOR);
      }

      // Campos para añadira nueva Tmct0DwhEjecuciones
      String nuOproutNuPareje = getNuoproutAndNupareje(operac.getCdAlias(), operac.getNucnfclt(), operac.getNucnfclt(),
          operac.getNbooking(), operac.getContRegistro());
      String nuOprout = nuOproutNuPareje.substring(0, 15);
      String nuPareje = (MAP_NUOPROUT_CONCAT_SUFIX);
      Integer cdCliest = Integer.valueOf(getCuentaLocal(operac.getCdAlias()));
      ejecucionesTemp.add(insertEjecuciones(nuOprout, nuPareje, cdCliest, operac.getCdIsin(), operac.getFeejeliq()));

    }
    ejecuciones.addAll(ejecucionesTemp);
  }

  public String getLineExecution(OperacionNacionalDTO operacion, DesgloseNacionalDTO desglose, Integer contNbooking)
      throws SIBBACBusinessException {
    ClienteNacionalDTO cliente;
    List<Object[]> cli = unbundledDao.findClienteNacional(operacion.getNuorden(), operacion.getNbooking(),
        operacion.getNucnfclt(), operacion.getNucnfliq());
    if (!cli.isEmpty()) {
      cliente = ClienteNacionalDTO.mapObject(cli.get(0));
    }
    else {
      cliente = ClienteNacionalDTO.initialize();
      LOG.info(String.format(
          "No se ha encontrado cliente para esta operacion: nuorden:%s, nbooking: %s,nucnfclt: %s, nucnfliq: %s",
          operacion.getNuorden(), operacion.getNbooking(), operacion.getNucnfclt(), operacion.getNucnfliq()));

    }
    return mapLineEjecucion(operacion, cliente, desglose, contNbooking);
  }

  private String mapLineEjecucion(OperacionNacionalDTO operacion, ClienteNacionalDTO cliente,
      DesgloseNacionalDTO desglose, Integer contNbooking) throws SIBBACBusinessException {

    try {
      StringBuilder line = new StringBuilder();
      line.append(getNuoproutAndNupareje(operacion.getCdAlias(), operacion.getNucnfclt(), desglose.getNucnfclt(),
          operacion.getNbooking(), operacion.getContRegistro()));

      line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_4.value(), contNbooking));

      line.append(getCuentaLocal(operacion.getCdAlias())).append("");

      line.append(
          String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_4.value(), Integer.valueOf(operacion.getCdentliq())));
      line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_12.value(), operacion.getCdIsin()));
      line.append(getBolsaId(desglose.getCdmiembromkt(), desglose.getCdPlataformaNegociacion()));
      line.append(operacion.getCdtpoper());
      line.append(MAP_TPMERCAD);
      line.append((desglose.getTpopebol() == "") ? "  " : desglose.getTpopebol());
      line.append(("CW".equals(desglose.getCdsegmento()) ? "F" : "V"));
      line.append(("CV".equals(desglose.getTpopebol()) || "EO".equals(desglose.getTpopebol())) ? "N" : "E");
      line.append(MAP_TPCONTRA);
      line.append(MAP_CDOPEMER);
      line.append((operacion.getCanoncontrpagoclte() == 1) ? "S" : "N");
      line.append(MAP_CDPROCED);
      line.append(formatDate("dd/MM/yyyy", operacion.getFeejeliq()));
      line.append(String.format("%s.%s", GenerateFilesUtilDwh.getZeros(13), GenerateFilesUtilDwh.getZeros(4)));
      line.append(GenerateFilesUtilDwh.formatDecimal(8, 4, desglose.getImprecio()));
      line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, desglose.getTotalImefectivo()));
      line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, desglose.getTotalImtitulos()));
      line.append(GenerateFilesUtilDwh.formatDecimalWithSign(14, 2, desglose.getTotalImcanoncontreu()));
      line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, desglose.getTotalImcanoncompeu()));
      line.append(getImcomField(operacion.getImcomsvb(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
      line.append(getImcomField(operacion.getImcombco(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
      line.append(getImcomField(operacion.getImcomdvo(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
      line.append(getImdevbrk(operacion.getStlev4lm(), operacion.getImcomdvo(), desglose.getTotalImtitulos(),
          operacion.getNutitliq()));
      line.append(getImcomField(operacion.getImcomsis(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
      line.append(getImcomField(operacion.getImcombrk(), desglose.getTotalImtitulos(), operacion.getNutitliq()));

      StringBuilder nbClient = new StringBuilder();
      nbClient.append(cliente.getNbclient().trim()).append("").append(cliente.getNbclient1().trim()).append("")
          .append(cliente.getNbclient2().trim());

      line.append(
          (String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_50.value(), nbClient.toString())).substring(0, 50));
      line.append(MAP_NUEJECUC);
      line.append(formatDate("yyyy", operacion.getFeejeliq()));
      line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
      line.append(MAP_TPCARGAS);
      // line.append(getCuentaID(desglose.getCdsegmento()));
      line.append(formatCuentaID(desglose.getCdsegmento(), desglose.getTpopebol(), operacion.getCdAlias()));
      line.append(GenerateFilesUtilDwh.formatDecimalWithSign(14, 2,
          (operacion.getCanoncontrpagoclte() == 0) ? desglose.getTotalImcanoncontreu() : BigDecimal.ZERO));
      line.append(GenerateFilesUtilDwh.formatDecimal(15, 2,
          (operacion.getCanoncontrpagoclte() == 0) ? desglose.getTotalImcanoncompeu() : BigDecimal.ZERO));
      line.append(getImcomField(operacion.getImcombco(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
      line.append(getImcomField(operacion.getImbansis(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
      line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_3.value(), operacion.getCdCanal()));
      line.append((desglose.getCdsegmento() == "") ? "  " : desglose.getCdsegmento().substring(0, 2));
      line.append(getOrigen(operacion.getCdOrigen(), operacion.getCdAlias()));
      line.append(GenerateFilesUtilDwh.formatDecimalWithSign(9, 2, BigDecimal.ZERO));

      return line.toString();

    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e + " Error al mapear registro a escribir en el fichero ");
    }
  }

  /**
   * Método para generar cada Tmct0DwhEjecuciones que será guardado en la BD
   * posteriormente
   * @param nuOprout
   * @param nuPareje
   * @param cdCliest
   * @param cdIsinvv
   * @param fhEjecu
   * @return Tmct0DwhEjecuciones
   */
  public Tmct0DwhEjecuciones insertEjecuciones(String nuOprout, String nuPareje, Integer cdCliest, String cdIsinvv,
      Date fhEjecu) {

    Tmct0DwhEjecuciones tmct0DwhEjecuciones = new Tmct0DwhEjecuciones();

    tmct0DwhEjecuciones.setNuOprout(nuOprout);
    tmct0DwhEjecuciones.setNuPareje(nuPareje);
    tmct0DwhEjecuciones.setCdCliest(cdCliest);
    tmct0DwhEjecuciones.setCdIsinvv(cdIsinvv);
    tmct0DwhEjecuciones.setFhEjecu(fhEjecu);
    tmct0DwhEjecuciones.setUsAudit("TaskDWHEjecuciones");
    tmct0DwhEjecuciones.setFhAudit(new Timestamp(new Date().getTime()));

    return tmct0DwhEjecuciones;
  }

  private String getNuoproutAndNupareje(String cdAlias, String nucnfclt, String nucnfcltDesglose, String nbooking,
      BigInteger contRegistro) {
    StringBuilder field = new StringBuilder();
    // if (MAP_NUOPROUT_CDALIAS.equals(cdAlias) ||
    // nucnfclt.startsWith(MAP_NUOPROUT_NUCNFCLT_PREFIX)) {
    // field.append(MAP_NUOPROUT_CONCAT_PREFIX);
    // field.append(nucnfcltDesglose.substring(9, 19));
    // field.append(MAP_NUOPROUT_CONCAT_SUFIX);
    // }
    // else {
    // field.append(nbooking.substring(1, 12));
    // field.append("0000");
    // field.append(String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_4.value(),
    // contRegistro.intValue()));
    // // TODO: Contador de registro por NBOOKING
    // }
    field.append(nbooking.substring(0, 11));
    field.append("0000");
    return field.toString();
  }

  private String getCuentaLocal(String cdAlias) {
    StringBuilder field = new StringBuilder();
    cdAlias = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), Integer.parseInt(cdAlias.trim()));
    field.append(MAP_CUENTA_PREFIX).append(cdAlias);

    return field.toString();
  }

  private String getBolsaId(String cdmiembromkt, String cdPlataformaNegociacion) {
    switch (cdmiembromkt) {
      case MAP_BOLSA_M:
        return "M  ";
      case MAP_BOLSA_V:
        return "V  ";
      case MAP_BOLSA_B:
        return "B  ";
      case MAP_BOLSA_L:
        return "I  ";
      default:
        return getBolsaIdbycdPlat(cdPlataformaNegociacion);

    }
  }

  private String getBolsaIdbycdPlat(String cdPlataformaNegociacion) {
    switch (cdPlataformaNegociacion) {
      case MAP_BOLSA_ME:
        return "ME ";
      case MAP_BOLSA_BE:
        return "BE ";
      case MAP_BOLSA_TE:
        return "TE ";
      default:
        return "   ";
    }
  }

  private String getImcomField(BigDecimal imcom, BigDecimal titulos, BigDecimal nutit) {
    String result = "";

    if (imcom.doubleValue() < 0) {
      result = "-";
    }
    else {
      result = "+";
    }

    BigDecimal imcomResult = imcom.multiply(titulos.divide(nutit, 2, RoundingMode.HALF_UP)).abs();
    return result + GenerateFilesUtilDwh.formatDecimal(15, 2, imcomResult);
  }

  public List<OperacionNacionalDTO> getOperacionesDwh(Date fechaInicio, Date fechaFin) {
    List<Object[]> operations = ficheroNacionalDao.findOperacionesNacional(fechaInicio, fechaFin);
    List<OperacionNacionalDTO> operaciones = new ArrayList<>();
    for (Object[] op : operations) {

      operaciones.add(OperacionNacionalDTO.mapObject(op));
    }

    return operaciones;
  }

  private String getImdevbrk(String stlev4lm, BigDecimal imcomdvo, BigDecimal totalImtitulos, BigDecimal nutitliq) {

    switch (stlev4lm) {
      case "50":
      case "455":
      case "855":
      case "1110":
      case "1570":
        return getImcomField(imcomdvo, totalImtitulos, nutitliq);
      default:
        return GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO);

    }
  }

  private Object getOrigen(String cdOrigen, String cdAlias) {
    switch (cdOrigen) {
      case MAP_ORIGEN_FI:
        return "FI ";
      case MAP_ORIGEN_OTHER:
        return getOrigenbycdAlias(cdAlias);
      default:
        return MAP_ORIGEN_DEFAULT;

    }
  }

  private String getOrigenbycdAlias(String cdAlias) {
    switch (cdAlias) {
      case MAP_ORIGEN_RK:
        return "RK ";
      case MAP_ORIGEN_RB:
        return "RB ";
      case MAP_ORIGEN_RI:
      default:
        return "RI ";
    }
  }

  private String getCuentaID(String cdsegmento, String tpopebol) {
    String cuentaId = MAP_CUENTA_DEFAULT;
    if (cdsegmento.equals("CW")) {
      cuentaId = MAP_CUENTA_CW;
    }
    else if (tpopebol.equals("CV") || tpopebol.equals("EO")) {
      cuentaId = MAP_CUENTA_CV_EO;
    }
    return cuentaId;
  }

  private String formatCuentaID(String cdsegmento, String tpopebol, String cdAlias) {

    String cdAliasCuentaID = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
        Integer.parseInt(cdAlias.trim()));
    String cuentaId = getCuentaID(cdsegmento.trim(), tpopebol.trim());
    StringBuilder line = new StringBuilder();
    line.append(cuentaId).append(cdAliasCuentaID);
    return line.toString();

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void writeUnbundledLine(List<Unbundled> operacionesUnbundled, List<Tmct0DwhEjecuciones> ejecuciones,
      BufferedWriter writer) throws IOException {
    List<Tmct0DwhEjecuciones> ejecucionesTemp = new ArrayList<Tmct0DwhEjecuciones>();
    for (Unbundled operac : operacionesUnbundled) {
      String line = mapLineExecutionUnbundled(operac);
      // Campos para añadira nueva Tmct0DwhEjecuciones
      String numOper = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_15.value(), operac.getNumOper().trim())
          .replace(' ', '0');
      ejecucionesTemp.add(insertEjecuciones(numOper, MAP_NUOPROUT_CONCAT_SUFIX, Integer.valueOf(operac.getCliente()),
          MAP_UNBUNDLED_CDISINVV, operac.getFechaPago()));
      writer.append(
          (String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line)) + NACIONAL_LINE_SEPARATOR);
    }
    ejecuciones.addAll(ejecucionesTemp);
  }

  private String mapLineExecutionUnbundled(Unbundled operacion) {

    StringBuilder line = new StringBuilder();

    line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_15.value(), operacion.getNumOper().trim())
        .replace(' ', '0'));
    line.append(GenerateFilesUtilDwh.formatNumber(MAP_UNBUNDLED_NUMPAREJE, 4));
    line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(),
        Integer.valueOf(operacion.getCliente().trim())));
    line.append(GenerateFilesUtilDwh.formatNumber(MAP_UNBUNDLED_CDENTLIQ, 4));
    line.append(MAP_UNBUNDLED_CDISINVV);
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_BOLSA, 3));
    line.append(MAP_UNBUNDLED_TPOPERAC);
    line.append(MAP_UNBUNDLED_TPMERCAD);
    line.append(MAP_UNBUNDLED_TEJERCI);
    line.append(MAP_UNBUNDLED_TPEJECUC);
    line.append(MAP_UNBUNDLED_TPOPESTD);
    line.append(MAP_UNBUNDLED_TPCONTRA);
    line.append("        ");
    line.append(MAP_UNBUNDLED_CDDERECH);
    line.append(MAP_UNBUNDLED_CDPROCED);
    line.append(GenerateFilesUtilDwh.formatDate("dd/MM/yyyy", operacion.getFechaPago()));
    line.append(String.format("%s.%s", GenerateFilesUtilDwh.getZeros(13), GenerateFilesUtilDwh.getZeros(4)));
    line.append(GenerateFilesUtilDwh.formatDecimal(8, 4, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(14, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, operacion.getComision()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_50.value(), ""));
    line.append(MAP_UNBUNDLED_NUEJECUC);
    line.append(GenerateFilesUtilDwh.formatDate("yyyy", operacion.getFechaPago()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_DEFAULT_STRING, 1));
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_DEFAULT_STRING, 8));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(14, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_DEFAULT_STRING, 3));
    line.append(MAP_UNBUNDLED_SUBMER_ID);
    line.append(MAP_UNBUNDLED_CDORIGEN);
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(9, 2, BigDecimal.ZERO));

    return line.toString();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void writeAutexLines(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    OperacionAutexDTO operacionAutexDTO;

    for (Object[] object : fileData) {

      operacionAutexDTO = OperacionAutexDTO.mapObject(object);

      StringBuilder line = new StringBuilder();
      line.append(formatDate("dd/MM/yyyy", operacionAutexDTO.getFeEjecuc()));
      line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_12.value(), operacionAutexDTO.getCdIsin()));
      line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, operacionAutexDTO.getImEfecti()));

      line.append(String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_15.value(),
          (operacionAutexDTO.getNucanEje()).intValue()));
      line.append(
          String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_10.value(), operacionAutexDTO.getCdAutexx()));

      outWriter.write(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line.toString())
          + NACIONAL_LINE_SEPARATOR);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void writeMinoristaLines(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] object : fileData) {

      Tmct0ctd ctd = Tmct0ctd.mapFromObject(object);
      MinoristaDTO dataMin = new MinoristaDTO();

      String line;
      line = generateOIMinoristasFile.loadMinorista(dataMin, ctd);
      if (line != null) {
        outWriter.write(
            String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line) + NACIONAL_LINE_SEPARATOR);
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void writeInstitucionalesLines(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] object : fileData) {

      Tmct0ctd ctd = Tmct0ctd.mapFromObject(object);
      InstitucionalDTO dataIns = new InstitucionalDTO();

      String line = generateOIInstitucionalesFile.loadInstitucional(dataIns, ctd);
      if (line != null) {
        outWriter.write(
            String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line) + NACIONAL_LINE_SEPARATOR);
      }
    }
  }
}
