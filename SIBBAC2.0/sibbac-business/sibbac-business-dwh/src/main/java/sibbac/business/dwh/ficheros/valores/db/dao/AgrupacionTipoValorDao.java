package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

@Repository
public interface AgrupacionTipoValorDao
    extends JpaRepository</* FIXME entidad */MaestroValores, String> {

  /**
   * Query para DWHgrupoValoresDao, ORIGINAL
   */
  public static final String AGRUP_TIPO_VALOR_QUERY = /* FIXME Query */ "SELECT CDGRSIBV, NBGRSIBV, CDGRUVAL, NBGRUVAL, CDTIPREN, NBTIPREN "
      + "FROM TMCT0_VALORES_GRUPO";

  /**
   * Query para DWHagrupTipoValorDao con Paginación
   */
  public static final String AGRUP_TIPO_VALOR_PAGE_QUERY = AGRUP_TIPO_VALOR_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHagrupTipoValorDao
   */
  public static final String AGRUP_TIPO_VALOR_PAGE_QUERY_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + AGRUP_TIPO_VALOR_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHagrupTipoValorDao sin
   * paginación
   * @return
   */
  @Query(value = AGRUP_TIPO_VALOR_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListAgrupTipoValor(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHagrupTipoValorDao sin
   * paginación
   * @return
   */
  @Query(value = AGRUP_TIPO_VALOR_PAGE_QUERY_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListAgrupTipoValor();

}
