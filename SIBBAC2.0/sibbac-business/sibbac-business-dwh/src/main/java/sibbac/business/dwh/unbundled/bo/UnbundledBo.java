package sibbac.business.dwh.unbundled.bo;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.unbundled.db.dao.UnbundledDao;
import sibbac.business.dwh.unbundled.db.model.Unbundled;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.business.unbundled.dto.AliasClienteDwhDTO;
import sibbac.business.unbundled.dto.PeticionUnbundledDTO;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

@Service
public class UnbundledBo extends AbstractBo<Unbundled, String, UnbundledDao> {

  @Autowired
  private HttpSession session;

  private static String MENSAJE_ERROR_PARSEO_FECHAS = "Error al formatear la fecha ";
  private static String MENSAJE_ERROR_JSON = "Error al formatear la respuesta ";

  private static String MENSAJE_UNBUNDLED_CREADO = "Registro creado correctamente";
  private static String MENSAJE_UNBUNDLED_MODIFICADO = "Registro modificado correctamente";

  private final DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");

  public Unbundled findById(final String numOperacion) {
    return this.dao.findOne(numOperacion);
  }

  @Override
  public Unbundled save(Unbundled unbundled) {
    this.dao.save(unbundled);
    return unbundled;
  }

  /**
   * @param fechaString
   * @return
   * @throws ParseException
   */
  private Date convertirStringADate(final String fechaString) throws ParseException {
    Date fecha = null;
    if (StringUtils.isNotEmpty(fechaString)) {
      fecha = new Date(dF.parse(fechaString).getTime());
    }
    return fecha;
  }

  /**
   * @param solicitud
   * @return
   * @throws SIBBACBusinessException
   */
  public CallableResultDTO createUnbundled(PeticionUnbundledDTO solicitud) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();

    String usuarioAuditoria = StringUtils.substring(((UserSession) session.getAttribute("UserSession")).getName(), 0,
        10);

    String cliente = (String) solicitud.getParams().get("cliente");
    BigDecimal importe = BigDecimal.ZERO;

    if (solicitud.getParams().get("importe") instanceof Double) {
      double imp = (double) solicitud.getParams().get("importe");
      importe = BigDecimal.valueOf(imp);
    }
    else {
      double imp = new Integer((int) solicitud.getParams().get("importe")).doubleValue();
      importe = BigDecimal.valueOf(imp);
    }

    Date fechaPago;

    try {
      fechaPago = convertirStringADate((String) solicitud.getParams().get("fechaPago"));
    }
    catch (ParseException e) {
      LOG.error(MENSAJE_ERROR_PARSEO_FECHAS, e);
      throw new SIBBACBusinessException(MENSAJE_ERROR_PARSEO_FECHAS);
    }

    Unbundled nuevaOperacion = new Unbundled();

    nuevaOperacion.setNumOper(getNewId(fechaPago));
    nuevaOperacion.setCliente("008" + cliente);
    nuevaOperacion.setComision(importe);
    nuevaOperacion.setFechaPago(fechaPago);
    nuevaOperacion.setFechaAlta(new Timestamp(System.currentTimeMillis()));
    nuevaOperacion.setUsuarioAlta(usuarioAuditoria);

    save(nuevaOperacion);
    res.addMessage(MENSAJE_UNBUNDLED_CREADO);
    return res;
  }

  /**
   * @param solicitud
   * @return
   * @throws SIBBACBusinessException
   */
  public CallableResultDTO modUnbundled(PeticionUnbundledDTO solicitud) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();

    String usuarioAuditoria = StringUtils.substring(((UserSession) session.getAttribute("UserSession")).getName(), 0,
        10);

    String numOperacion = (String) solicitud.getParams().get("id");
    BigDecimal importe = BigDecimal.ZERO;
    if (solicitud.getParams().get("importe") instanceof Double) {
      double imp = (double) solicitud.getParams().get("importe");
      importe = BigDecimal.valueOf(imp);
    }
    else {
      double imp = new Integer((int) solicitud.getParams().get("importe")).doubleValue();
      importe = BigDecimal.valueOf(imp);
    }

    Date fechaPago;

    try {
      fechaPago = convertirStringADate((String) solicitud.getParams().get("fechaPago"));
    }
    catch (ParseException e) {
      LOG.error(MENSAJE_ERROR_PARSEO_FECHAS, e);
      throw new SIBBACBusinessException(MENSAJE_ERROR_PARSEO_FECHAS);
    }

    Unbundled nuevaOperacion = findById(numOperacion);

    nuevaOperacion.setComision(importe);
    nuevaOperacion.setFechaPago(fechaPago);
    nuevaOperacion.setFechaMod(new Timestamp(System.currentTimeMillis()));
    nuevaOperacion.setUsuarioMod(usuarioAuditoria);

    save(nuevaOperacion);
    res.addMessage(MENSAJE_UNBUNDLED_MODIFICADO);
    return res;
  }

  public JSONArray getClientesUnbundled(String term) throws SIBBACBusinessException {
    JSONArray result = new JSONArray();
    term = String.format("%s%s%s", "%", term, "%");
    for (Object[] cliente :  dao.findPredictCli(term)) {
      JSONObject clienteJson = new JSONObject();
      try {
        String cdAlias = (String) cliente[0];
        cdAlias = cdAlias.trim();
        String descAlias = (String) cliente[1];
        descAlias = descAlias.trim();
        clienteJson.put("label", cdAlias + "-" + descAlias);
        clienteJson.put("value", cdAlias);
      } catch(JSONException e) {
        LOG.error(MENSAJE_ERROR_JSON, e);
        throw new SIBBACBusinessException(MENSAJE_ERROR_JSON);
      }
      result.put(clienteJson);
    }
    return result;
  }
  
  public JSONArray getFilterClientesUnbundled(String term) throws SIBBACBusinessException {
    JSONArray result = new JSONArray();
    term = String.format("%s%s%s", "%", term, "%");
    for (Object[] cliente :  dao.findFilterPredictCli(term)) {
      JSONObject clienteJson = new JSONObject();
      try {
        clienteJson.put("cdaliass", cliente[0]);
        clienteJson.put("descrali", cliente[1]);
        clienteJson.put("cdbrocli", cliente[2]);
      } catch(JSONException e) {
        LOG.error(MENSAJE_ERROR_JSON, e);
        throw new SIBBACBusinessException(MENSAJE_ERROR_JSON);
      }
      result.put(clienteJson);
    }
    return result;
  }
  
  private String getNewId(Date fechaPago) {

    int numOperations = dao.getNumberOperationsByDate(fechaPago);
    StringBuilder id = new StringBuilder();
    String numOp = GenerateFilesUtilDwh.formatNumber(numOperations, 4);
    id.append(GenerateFilesUtilDwh.formatDate("yyyyMMdd", fechaPago)).append(numOp);

    return id.toString();
  }
  
  
  public List<AliasClienteDwhDTO> getAllFilterCliente() throws SIBBACBusinessException {
    List<Object[]> alias = dao.findAllFilterPredictCli();
    List<AliasClienteDwhDTO> result = new ArrayList<>();
    
    for (Object[] cliente :  alias) {
      result.add(AliasClienteDwhDTO.mapObject(cliente));
    }
    return result;
  }
  
  
}