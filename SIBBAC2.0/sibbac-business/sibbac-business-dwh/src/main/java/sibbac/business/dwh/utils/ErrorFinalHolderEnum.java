package sibbac.business.dwh.utils;

public enum ErrorFinalHolderEnum {
	
	ERROR_01( "ERR001" ),
	ERROR_02( "ERR002" ),
	ERROR_03( "ERR003" ),
	ERROR_04( "ERR004" ),
	ERROR_05( "ERR005" ),
	ERROR_06( "ERR006" ),
	ERROR_07( "ERR007" ),
	ERROR_08( "ERR008" ),
	ERROR_09( "ERR009" ),
	ERROR_10( "ERR010" ),
	ERROR_11( "ERR011" ),
	ERROR_12( "ERR012" ),
	ERROR_13( "ERR013" ),
	ERROR_14( "ERR014" ),
	ERROR_15( "ERR015" ),
	ERROR_16( "ERR016" ),
	ERROR_17( "ERR017" ),
	ERROR_18( "ERR018" ),
	ERROR_19( "ERR019" ),
	ERROR_20( "ERR020" ),
	ERROR_21( "ERR021" ),
	ERROR_22( "ERR022" ),
	ERROR_23( "ERR023" ),
	ERROR_24( "ERR024" ),
	ERROR_25( "ERR025" ),
	ERROR_26( "ERR026" ),
	ERROR_27( "ERR027" ),
	ERROR_28( "ERR028" ),
	ERROR_29( "ERR029" ),
	ERROR_30( "ERR030" ),
	ERROR_31( "ERR031" ),
	ERROR_32( "ERR032" ),
	ERROR_33( "ERR033" ),
	ERROR_34( "ERR034" ),
	ERROR_35( "ERR035" ),
	ERROR_36( "ERR036" ),
	ERROR_37( "ERR037" ),
	ERROR_38( "ERR038" ),
	ERROR_39( "ERR039" ),
	ERROR_40( "ERR040" ),
	ERROR_41( "ERR041" ),
	ERROR_42( "ERR042" ),
	ERROR_43( "ERR043" ),
	ERROR_44( "ERR044" ),
	ERROR_45( "ERR045" ),
	ERROR_46( "ERR046" ),
	ERROR_47( "ERR047" ),
	ERROR_48( "ERR048" ),
	ERROR_51( "ERR051" ),
	ERROR_60( "ERR060" ),
	ERROR_61( "ERR061" ),
	ERROR_62( "ERR062" ),
	ERROR_63( "ERR063" ),
	ERROR_64( "ERR064" ),
	ERROR_65( "ERR065" ),
	ERROR_66( "ERR066" ),
	ERROR_67( "ERR067" ),
	ERROR_68( "ERR068" ),
	ERROR_69( "ERR069" ),
	ERROR_70( "ERR070" ),
	ERROR_71( "ERR071" ),
	ERROR_72( "ERR072" ),
	ERROR_73( "ERR073" ),
	ERROR_74( "ERR074" ),
	ERROR_80( "ERR080" ),
	ERROR_81( "ERR081" ),
	ERROR_82( "ERR082" ),
	ERROR_83( "ERR083" ),
	ERROR_84( "ERR084" ),
	ERROR_85( "ERR085" ),
	ERROR_98( "ERR098" ),
	ERROR_99( "ERR099" ),
	ERROR_100( "ERR100" ),
	ERROR_101( "ERR101" ),
	ERROR_102( "ERR102" ),
	ERROR_103( "ERR103" ),
	@Deprecated ERROR_104( "ERR104" ),
	ERROR_105( "ERR105" ),
	ERROR_106( "ERR106" ), // Tipo ID MiFID para persona jurídica tiene que ser LEI
	ERROR_107( "ERR107" ), // Tipo ID MiFID para persona física tiene que ser: Identif.Nacional, Pasaporte o CONCAT
	ERROR_108( "ERR108" ), // Fecha de Nacimiento obligatoria para persona física
	ERROR_109( "ERR109" ), // Tipo de identificación no válida para el país de nacionalidad
	ERROR_110( "ERR110" ), // El código LEI tiene que ser una cadena de 20 posiciones alfanuméricas
	ERROR_111( "ERR111" ), // Las dos primeras posiciones de la identificación MiFID tienen que ser el código ISO alfabético del país de nacionalidad
	ERROR_112( "ERR112" ), // La identificación MiFID no se corresponde con el documento introducido
	ERROR_113( "ERR113" ), // Numero cliente santander obligatorio
	ERROR_114( "ERR114" ), // Numero cliente santander no debe estar informado
	ERROR_115( "ERR115" ), // ID Mifid de tipo CONCAT es erróneo
	
	
	SIN_ERROR( "" );


	private String	error;
	
	public static ErrorFinalHolderEnum getEnum(String str){
        try {
            for (ErrorFinalHolderEnum b : ErrorFinalHolderEnum.values()) {
                if (str.equals(b.getError())) {
                    return b;
                }
            }
        } catch (Exception ex) {
            
            return SIN_ERROR;
        }
        return null;
    }


	private ErrorFinalHolderEnum( String error ) {
		this.error = error;
	}

	/**
	 * Gets the command.
	 *
	 * @return the command
	 */
	public String getError() {
		return error;
	}

}
