package sibbac.business.dwh.ficheros.tradersaccounters.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.tradersaccounters.db.model.Tmct0SalesTrader;

/**
 * The Interface Tmct0SalesTraderDao.
 */
@Repository
public interface Tmct0SalesTraderDao extends JpaRepository<Tmct0SalesTrader, String> {

  /**
   * Query para setear en el campo Cuenta
   * @return Integer
   */
  @Query(value = "SELECT VARCHAR_FORMAT(TMCT0_SALES_TRADER_SEQ.nextval, '00000') FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  public Integer getCuenta();

}
