package sibbac.business.dwh.ficheros.estaticos.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.dwh.ficheros.estaticos.db.model.BolsaDwh;

public interface EntidadesDwhDao extends JpaRepository<BolsaDwh, String> {

  @Query(value = "SELECT ID_ENTIDAD, NOMBRE_ENTIDAD FROM TMCT0_DWH_ENTIDAD_LIQUIDADORA WHERE FECHA_BAJA > CURRENT_DATE OR FECHA_BAJA IS NULL", nativeQuery = true)
  public List<Object[]> findActiveEntidades();

}