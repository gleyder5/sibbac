package sibbac.business.dwh.ficheros.tradersaccounters.bo.callable;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableProcessFileResult;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.tradersaccounters.bo.GenerateFicheroSales;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGenerateFicheroSales implements CallableProcessFileResult {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGenerateFicheroSales.class);

  @Autowired
  private GenerateFicheroSales generateFicheroSales;

  private Future<ProcessFileResultDTO> future;

  /** Metódo que se ejecutará en el hilo */
  @Override
  public ProcessFileResultDTO call() throws Exception {
    final ProcessFileResultDTO processFileResultDTO = new ProcessFileResultDTO();

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza el proceso en background de Sales File");
    }

    generateFicheroSales.generateFile();

    processFileResultDTO.setFileNameProcess(generateFicheroSales.getFileName());
    processFileResultDTO.setFileResult(generateFicheroSales.getFileGenerationResult());

    return processFileResultDTO;
  }

  public GenerateFicheroSales getGenerateFicheroSales() {
    return generateFicheroSales;
  }

  public void setGenerateFicheroSales(GenerateFicheroSales generateFicheroSales) {
    this.generateFicheroSales = generateFicheroSales;
  }

  @Override
  public void setFuture(Future<ProcessFileResultDTO> future) {
    this.future = future;
  }

  @Override
  public Future<ProcessFileResultDTO> getFuture() {
    return future;
  }
}
