package sibbac.business.dwh.ficheros.clientes.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.clientes.db.dao.DWHClientesNifDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("clientesNifFileGenerator")
public class GenerateClientesNifFile extends AbstractGenerateFile {

  private static final String CLIENTES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateClientesNifFile.class);

  private static int CLIENTES_CDCLIENT = 0;
  private static int CLIENTES_NUCLIENT = 1;
  private static int CLIENTES_NUDNICIF = 2;

  @Autowired
  private DWHClientesNifDao dwhClientesNifDao;

  @Override
  public String getLineSeparator() {
    return CLIENTES_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return dwhClientesNifDao.findAllListClientesNif(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return dwhClientesNifDao.countAllListClientesNif();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {

      Integer cdcli = (Integer) (objects[CLIENTES_CDCLIENT]);
      Integer nucliente = (Integer) (objects[CLIENTES_NUCLIENT]);
      String nudniciff = (String) objects[CLIENTES_NUDNICIF];

      String cdcliente = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdcli);
      String nuclient = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_6.value(), nucliente);
      String nudnicif = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_6.value(), nudniciff);

      final StringBuilder rowLine = new StringBuilder();

      rowLine.append(cdcliente).append(nuclient).append(nudnicif);

      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

      writeLine(outWriter, cadena);
    }
  }

}
