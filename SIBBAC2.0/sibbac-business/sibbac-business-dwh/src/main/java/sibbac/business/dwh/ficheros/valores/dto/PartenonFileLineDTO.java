package sibbac.business.dwh.ficheros.valores.dto;

import java.math.BigInteger;

public class PartenonFileLineDTO {

  private BigInteger idLine;

  private String line;

  public PartenonFileLineDTO() {
    super();
  }

  public PartenonFileLineDTO(BigInteger idLine, String line) {
    super();
    this.idLine = idLine;
    this.line = line;
  }

  public BigInteger getIdLine() {
    return idLine;
  }

  public void setIdLine(BigInteger idLine) {
    this.idLine = idLine;
  }

  public String getLine() {
    return line;
  }

  public void setLine(String line) {
    this.line = line;
  }

}
