package sibbac.business.dwh.ficheros.valores.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.AutexDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateAutexFile")
public class GenerateAutexFile extends AbstractGenerateFile {

  private static final String VALORES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateAutexFile.class);

  /**
   * DAO de Acceso a Base de Datos 
   */
  @Autowired
  private AutexDao autexDao;

  @Override
  public String getLineSeparator() {
    return VALORES_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return autexDao.findAllListAutex(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return autexDao.countAllListAutex();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      if(!objects[0].equals("null") && !objects[1].equals("null"))
      {
        String cdautexx = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_10.value(), (String) objects[0]);
        String nbtitrev = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), (String) objects[1]);

        final StringBuilder rowLine = new StringBuilder();

        rowLine.append(cdautexx).append(nbtitrev);

        String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

        writeLine(outWriter, cadena);
      }
    }

  }

}
