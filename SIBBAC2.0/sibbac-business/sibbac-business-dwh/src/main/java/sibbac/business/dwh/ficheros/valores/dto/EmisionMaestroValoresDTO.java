package sibbac.business.dwh.ficheros.valores.dto;

import java.math.BigDecimal;

public class EmisionMaestroValoresDTO {

  // ~~~~~~~~~~ Counter: verifica cuantos registros existen MAESTRO_VALORES
  private Integer existmaestrovalores;
  private Integer iswarrant;

  // ~~~~~~~~~~ Campos de EMSION
  private String feemisio;
  private String dsemisio;
  private String imnomina;
  private String situval;
  private String cdalfval;
  private String feestado;
  private String cdcambio;
  private String cdformat;
  private String cdstliqi;
  private String cdemisio;
  private String cdcvalov;
  private String cdbesnum;
  private String cdestado;
  private String cdisin;
  // ~~~~~~~~~~ Campos de EMISOR
  private String cdcifsoc;
  private String cdpaisev;
  // ~~~~~~~~~~ Campos de MERCADO
  private String indcotiz;
  private String cdbolsa;
  private String cdmoneda;
  private String cdvalmdo;
  private String cdmercad;
  private String cdsector;
  private String cdorgant;
  private String nulote;

  // ~~~~~~~~~~ Campos de MAESTRO_VALORES
  private String nbtitrev;
  private BigDecimal nunominv;
  private String cdsisliv;
  private String cdnominv;

  private String nbtit10c;
  private String cdorgano;

  public Integer getExistmaestrovalores() {
    return existmaestrovalores;
  }

  public void setExistmaestrovalores(Integer existmaestrovalores) {
    this.existmaestrovalores = existmaestrovalores;
  }

  public Integer getIswarrant() {
    return iswarrant;
  }

  public void setIswarrant(Integer iswarrant) {
    this.iswarrant = iswarrant;
  }

  public String getCdemisio() {
    return cdemisio;
  }

  public void setCdemisio(String cdemisio) {
    this.cdemisio = cdemisio;
  }

  public String getCdcvalov() {
    return cdcvalov;
  }

  public void setCdcvalov(String cdcvalov) {
    this.cdcvalov = cdcvalov;
  }

  public String getCdbesnum() {
    return cdbesnum;
  }

  public void setCdbesnum(String cdbesnum) {
    this.cdbesnum = cdbesnum;
  }

  public String getCdestado() {
    return cdestado;
  }

  public void setCdestado(String cdestado) {
    this.cdestado = cdestado;
  }

  public String getNbtitrev() {
    return nbtitrev;
  }

  public void setNbtitrev(String nbtitrev) {
    this.nbtitrev = nbtitrev;
  }

  public BigDecimal getNunominv() {
    return nunominv;
  }

  public void setNunominv(BigDecimal nunominv) {
    this.nunominv = nunominv;
  }

  public String getCdsisliv() {
    return cdsisliv;
  }

  public void setCdsisliv(String cdsisliv) {
    this.cdsisliv = cdsisliv;
  }

  public String getCdnominv() {
    return cdnominv;
  }

  public void setCdnominv(String cdnominv) {
    this.cdnominv = cdnominv;
  }

  public String getCdisin() {
    return cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public String getNbtit10c() {
    return nbtit10c;
  }

  public void setNbtit10c(String nbtit10c) {
    this.nbtit10c = nbtit10c;
  }

  public String getCdorgano() {
    return cdorgano;
  }

  public void setCdorgano(String cdorgano) {
    this.cdorgano = cdorgano;
  }

  public String getFeemisio() {
    return feemisio;
  }

  public void setFeemisio(String feemisio) {
    this.feemisio = feemisio;
  }

  public String getDsemisio() {
    return dsemisio;
  }

  public void setDsemisio(String dsemisio) {
    this.dsemisio = dsemisio;
  }

  public String getImnomina() {
    return imnomina;
  }

  public void setImnomina(String imnomina) {
    this.imnomina = imnomina;
  }

  public String getSituval() {
    return situval;
  }

  public void setSituval(String situval) {
    this.situval = situval;
  }

  public String getCdalfval() {
    return cdalfval;
  }

  public void setCdalfval(String cdalfval) {
    this.cdalfval = cdalfval;
  }

  public String getFeestado() {
    return feestado;
  }

  public void setFeestado(String feestado) {
    this.feestado = feestado;
  }

  public String getCdcambio() {
    return cdcambio;
  }

  public void setCdcambio(String cdcambio) {
    this.cdcambio = cdcambio;
  }

  public String getCdformat() {
    return cdformat;
  }

  public void setCdformat(String cdformat) {
    this.cdformat = cdformat;
  }

  public String getCdstliqi() {
    return cdstliqi;
  }

  public void setCdstliqi(String cdstliqi) {
    this.cdstliqi = cdstliqi;
  }

  public String getCdcifsoc() {
    return cdcifsoc;
  }

  public void setCdcifsoc(String cdcifsoc) {
    this.cdcifsoc = cdcifsoc;
  }

  public String getCdpaisev() {
    return cdpaisev;
  }

  public void setCdpaisev(String cdpaisev) {
    this.cdpaisev = cdpaisev;
  }

  public String getIndcotiz() {
    return indcotiz;
  }

  public void setIndcotiz(String indcotiz) {
    this.indcotiz = indcotiz;
  }

  public String getCdbolsa() {
    return cdbolsa;
  }

  public void setCdbolsa(String cdbolsa) {
    this.cdbolsa = cdbolsa;
  }

  public String getCdmoneda() {
    return cdmoneda;
  }

  public void setCdmoneda(String cdmoneda) {
    this.cdmoneda = cdmoneda;
  }

  public String getCdvalmdo() {
    return cdvalmdo;
  }

  public void setCdvalmdo(String cdvalmdo) {
    this.cdvalmdo = cdvalmdo;
  }

  public String getCdmercad() {
    return cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public String getCdsector() {
    return cdsector;
  }

  public void setCdsector(String cdsector) {
    this.cdsector = cdsector;
  }

  public String getCdorgant() {
    return cdorgant;
  }

  public void setCdorgant(String cdorgant) {
    this.cdorgant = cdorgant;
  }

  public String getNulote() {
    return nulote;
  }

  public void setNulote(String nulote) {
    this.nulote = nulote;
  }

}
