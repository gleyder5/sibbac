package sibbac.business.dwh.ficheros.tradersaccounters.bo.callable;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableProcessFileResult;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.tradersaccounters.bo.GenerateFicheroTraders;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGenerateFicheroTraders implements CallableProcessFileResult {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGenerateFicheroTraders.class);

  @Autowired
  private GenerateFicheroTraders generateFicheroTraders;

  private Future<ProcessFileResultDTO> future;

  /** Metódo que se ejecutará en el hilo */
  @Override
  public ProcessFileResultDTO call() throws Exception {
    final ProcessFileResultDTO processFileResultDTO = new ProcessFileResultDTO();

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza el proceso en background de Trades File");
    }

    generateFicheroTraders.generateFile();

    processFileResultDTO.setFileNameProcess(generateFicheroTraders.getFileName());
    processFileResultDTO.setFileResult(generateFicheroTraders.getFileGenerationResult());

    return processFileResultDTO;
  }

  public GenerateFicheroTraders getGenerateFicheroTraders() {
    return generateFicheroTraders;
  }

  public void setGenerateFicheroTraders(GenerateFicheroTraders generateFicheroTraders) {
    this.generateFicheroTraders = generateFicheroTraders;
  }

  @Override
  public void setFuture(Future<ProcessFileResultDTO> future) {
    this.future = future;
  }

  @Override
  public Future<ProcessFileResultDTO> getFuture() {
    return future;
  }
}
