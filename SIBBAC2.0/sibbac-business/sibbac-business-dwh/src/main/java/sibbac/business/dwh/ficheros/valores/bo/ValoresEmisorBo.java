package sibbac.business.dwh.ficheros.valores.bo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.valores.db.dao.ValoresEmisorDao;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresEmisor;
import sibbac.database.bo.AbstractBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValoresEmisorBo extends AbstractBo<ValoresEmisor, String, ValoresEmisorDao> {

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  private int commitInterval = 100;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void saveAll(List<ValoresEmisor> entities) {
    dao.save(entities);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkValoresEmisor(List<ValoresEmisor> entities) {
    Integer resultCounter = 0;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (ValoresEmisor emisorEntity : entities) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          entityTransaction.commit();
          entityTransaction.begin();
          entityManager.clear();
        }

        entityManager.persist(emisorEntity);
        resultCounter++;
      }

      entityTransaction.commit();
    }
    catch (RuntimeException e) {
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw e;
    }
    finally {
      entityManager.close();
    }

    return resultCounter;
  }

  public List<Object[]> findEmisor(String cdemisio) {
    return dao.findEmisor(cdemisio);
  }
}
