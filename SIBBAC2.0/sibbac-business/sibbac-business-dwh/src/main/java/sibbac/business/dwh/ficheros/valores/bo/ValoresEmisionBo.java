package sibbac.business.dwh.ficheros.valores.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.valores.db.dao.CargaValoresDao;
import sibbac.business.dwh.ficheros.valores.db.dao.ValoresEmisionDao;
import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresEmision;
import sibbac.business.dwh.ficheros.valores.dto.EmisionMaestroValoresDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValoresEmisionBo extends AbstractBo<ValoresEmision, String, ValoresEmisionDao> {

  private Map<String, String[]> cotValuesMap;

  private Map<String, String> situvalMap;

  private Map<String, String> cdcambioMap;

  private Map<String, String> idMerconMap;

  private Map<String, String> cdnominvMap;

  private Map<String, String> cdpercuvMap;

  private Map<String, String> cdorgantMap;

  /** Número de Hilos de ejecucion */
  @Value("${fileName.dwh.partenon.threads:5}")
  private int threads;

  @Autowired
  private CargaValoresDao cargaValoresDao;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  private int commitInterval = 100;

  static final String cdCvsibv = "99999999";

  final List<String> bajaIsinListValidAlta = new ArrayList<>();

  final List<MaestroValores> listSaveAllEntitiesValidAlta = new ArrayList<>();

  final List<String> cdemisioList58Mod = new ArrayList<>();

  final List<String> cdemisioList14Mod = new ArrayList<>();

  final List<String> cdisinListMod = new ArrayList<>();
  final List<String> cdemisioListMod = new ArrayList<>();
  final List<Date> feemisioListMod = new ArrayList<>();

  final List<MaestroValores> listSaveAllEntitiesValidMod = new ArrayList<>();

  final List<String> bajaIsinListValidMod = new ArrayList<>();

  /**
   * Inicializa los mapas para acceder rápidamente a la info
   */
  public void initialiceMaps() {
    initialiceCotValuesMap();
    initialiceSituvalMap();
    initialiceCdCambiolMap();
    initialiceCdNominMap();
    initialiceCdpercuvMap();
    initialiceIdMerconMap();
    initialiceCdorgantMap();

  }

  /**
   * Destruye los mapas para liberar memoria
   */
  public void destroyMaps() {
    this.cotValuesMap = null;
    this.idMerconMap = null;
  }

  /**
   * Inicializa la Cache para COT_VALUES
   */
  private void initialiceCotValuesMap() {
    this.cotValuesMap = new HashMap<>();
    this.cotValuesMap.put("001", new String[] { "S", "N", "N", "N" });
    this.cotValuesMap.put("002", new String[] { "N", "S", "N", "N" });
    this.cotValuesMap.put("003", new String[] { "N", "N", "S", "N" });
    this.cotValuesMap.put("004", new String[] { "N", "N", "N", "S" });
    this.cotValuesMap.put("00", new String[] { "S", "S", "S", "S" });
    this.cotValuesMap.put("LIB", new String[] { "S", "S", "S", "S" });
    this.cotValuesMap.put("DEFAULT", new String[] { "N", "N", "N", "N" });
  }

  /**
   * Inicializa el mapa de SITUVAL
   */
  private void initialiceSituvalMap() {
    this.situvalMap = new HashMap<String, String>();
    this.situvalMap.put("1", "A");
    this.situvalMap.put("2", "R");
    this.situvalMap.put("3", "");
    this.situvalMap.put("4", "E");
    this.situvalMap.put("5", "N");
    this.situvalMap.put("6", "P");

  }

  /**
   * Inicializa el mapa de CDCAMBIO
   */
  private void initialiceCdCambiolMap() {
    this.cdcambioMap = new HashMap<String, String>();
    this.cdcambioMap.put("1", "T");
    this.cdcambioMap.put("2", "P");
    this.cdcambioMap.put("3", "P");
    this.cdcambioMap.put("4", "P");
    this.cdcambioMap.put("5", "P");
    this.cdcambioMap.put("6", "P");
    this.cdcambioMap.put("E", "P");
    this.cdcambioMap.put("C", "P");
  }

  /**
   * Inicializa la Cache para IDMERCON
   */
  private void initialiceIdMerconMap() {
    this.idMerconMap = new HashMap<>();
    this.idMerconMap.put("4", "00");
    this.idMerconMap.put("2", "01");
    this.idMerconMap.put("5", "02");
    this.idMerconMap.put("3", "06");
    this.idMerconMap.put("9", "09");
    this.idMerconMap.put("NUDELOTE", "03");
    this.idMerconMap.put("WARRANT", "04");
    this.idMerconMap.put("002", "01");
    this.idMerconMap.put("005", "02");
  }

  /**
   * Inicializa la Cache para CDNOMINV
   */
  private void initialiceCdNominMap() {
    this.cdnominvMap = new HashMap<String, String>();
    this.cdnominvMap.put("01", "P");
    this.cdnominvMap.put("03", "P");
    this.cdnominvMap.put("04", "N");
    this.cdnominvMap.put("05", "P");
    this.cdnominvMap.put("06", "P");
    this.cdnominvMap.put("07", "P");
    this.cdnominvMap.put("08", "P");
    this.cdnominvMap.put("10", "P");
    this.cdnominvMap.put("11", "P");
    this.cdnominvMap.put("12", "P");
    this.cdnominvMap.put("13", "P");
    this.cdnominvMap.put("14", "P");
    this.cdnominvMap.put("20", "P");
    this.cdnominvMap.put("21", "P");
    this.cdnominvMap.put("22", "P");
    this.cdnominvMap.put("23", "P");
  }

  /**
   * Inicializa la Cache para CDPERCUV
   */
  private void initialiceCdpercuvMap() {
    this.cdpercuvMap = new HashMap<String, String>();
    this.cdpercuvMap.put("0", "P");
    this.cdpercuvMap.put("1", "D");
    this.cdpercuvMap.put("3", "B");
    this.cdpercuvMap.put("4", "C");
  }

  /**
   * Inicializa la Cache para CDORGANT
   */
  private void initialiceCdorgantMap() {
    this.cdorgantMap = new HashMap<>();
    this.cdorgantMap.put("002", "3504");
    this.cdorgantMap.put("003", "3502");
    this.cdorgantMap.put("004", "3503");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void saveAll(List<ValoresEmision> entities) {
    dao.save(entities);
  }

  /**
   * Guarda una lista en ValoresEmision
   * @param entities
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkValoresEmision(List<ValoresEmision> entities) {
    Integer resultCounter = 0;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (ValoresEmision emisionEntity : entities) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          entityTransaction.commit();
          entityTransaction.begin();
          entityManager.clear();
        }

        entityManager.persist(emisionEntity);
        resultCounter++;
      }

      entityTransaction.commit();
    }
    catch (RuntimeException e) {
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw e;
    }
    finally {
      entityManager.close();
    }
    return resultCounter;
  }

  /**
   * Guarda una lista de MaestroValores en Base de Datos
   * @param entities
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkMaestroValores(List<MaestroValores> entities) {
    Integer resultCounter = 0;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (MaestroValores mercadoEntity : entities) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          entityTransaction.commit();
          entityTransaction.begin();
          entityManager.clear();
        }

        entityManager.persist(mercadoEntity);
        resultCounter++;
      }

      entityTransaction.commit();
    }
    catch (RuntimeException e) {
      LOG.error("Error en la carga de Maestro Valores desde Partenon: ", e);
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw e;
    }
    finally {
      entityManager.close();
    }

    return resultCounter;
  }

  /**
   * Funcion alta ver DDS
   * @param listAlta
   * @throws SIBBACBusinessException
   */
  public void funcionAlta(List<EmisionMaestroValoresDTO> listAlta) throws SIBBACBusinessException {

    for (EmisionMaestroValoresDTO emisionMaestro : listAlta) {
      // Llamar para toda la Lista
      MaestroValores maestroV = rellenarSalida(emisionMaestro);

      maestroV.setFhaltav(new Date());
      maestroV.setUsualta("TaskCargaValores-Partenon");

      // Dar de baja a toda la lista
      bajaIsinListValidAlta.add(maestroV.getCdisinvv());
      // Insertar en maestro de valores
      listSaveAllEntitiesValidAlta.add(maestroV);
    }

    cargaValoresDao.funcionBajaIsinsAnteriores(bajaIsinListValidAlta);
    saveBulkMaestroValores(listSaveAllEntitiesValidAlta);

  }

  /**
   * Funcion Mod ver DDS
   * @param listModificacion
   * @throws SIBBACBusinessException
   */
  public void funcionMod(List<EmisionMaestroValoresDTO> listModificacion) throws SIBBACBusinessException {

    for (EmisionMaestroValoresDTO emisionMaestro : listModificacion) {

      try {
        // Llamar <Rellenar Salida>
        MaestroValores maestroV = rellenarSalida(emisionMaestro);

        // Validar si es una nueva emisión del valor.
        // Comparar campos con los campos devueltos en la query de validación
        // NBTITREV, NUNOMINV, CDSISLIV, CDNOMINV, CDISINVV, NBTIT10C, CDORGANO
        List<Object[]> valid = cargaValoresDao.validacionValores(emisionMaestro.getCdemisio());

        String cdemisio58 = emisionMaestro.getCdemisio().substring(4, 12);
        String cdemisio14 = "9999" + emisionMaestro.getCdemisio().substring(0, 4);

        String cdisin = emisionMaestro.getCdisin();

        Date feemisio = FormatDataUtils.convertStringToDate(emisionMaestro.getFeemisio(),
            FormatDataUtils.DATE_FORMAT_1);

        if (!valid.isEmpty()) {

          for (Object[] objects : valid) {

            String nbtitrev = (String) (objects[0]);
            BigDecimal nunominv = (BigDecimal) (objects[1]);
            String cdsisliv = (String) (objects[2]);
            String cdnominv = (String) (objects[3]);
            String cdisinvv = (String) (objects[4]);
            String nbtit10c = (String) (objects[5]);
            String cdorgano = (String) (objects[6]);

            // Si valores son iguales Modificar
            if (nbtitrev.equals(emisionMaestro.getNbtitrev()) && nunominv == emisionMaestro.getNunominv()
                && cdsisliv.equals(emisionMaestro.getCdsisliv()) && cdnominv.equals(emisionMaestro.getCdnominv())
                && cdisinvv.equals(emisionMaestro.getCdisin()) && nbtit10c.equals(emisionMaestro.getNbtit10c())
                && cdorgano.equals(emisionMaestro.getCdorgano())) {

              cdemisioList58Mod.add(cdemisio58);
              cdemisioList14Mod.add(cdemisio14);
            }
            else {
              // Acumulamos datos para baja de emisión anterior
              cdisinListMod.add(cdisin);
              cdemisioListMod.add(cdemisio14);
              feemisioListMod.add(feemisio);
              // Acumulamos datos Insertar registro en TMCT0_MAESTRO_VALORES
              listSaveAllEntitiesValidMod.add(maestroV);
            }
          }
        }
      }
      catch (Exception e) {
        LOG.error("Error al ejecurtar funcion Modificar", e);
        throw new SIBBACBusinessException("Error al ejecurtar funcion Modificar", e);
      }
      // Actualizar registro de TMCT0_MAESTRO_VALORES
      if (!cdemisioList58Mod.isEmpty() && !cdemisioList14Mod.isEmpty()) {
        cargaValoresDao.funcionMod(cdemisioList58Mod, cdemisioList14Mod);
      }
      // Baja de emisión anterior
      if (!cdisinListMod.isEmpty() && !cdemisioListMod.isEmpty() && !feemisioListMod.isEmpty()) {
        cargaValoresDao.funcionBajaEmisionAnterior(cdisinListMod, cdemisioListMod, feemisioListMod);
      }
      // Insertar registro en TMCT0_MAESTRO_VALORES
      if (!listSaveAllEntitiesValidMod.isEmpty()) {
        saveBulkMaestroValores(listSaveAllEntitiesValidMod);
      }
    }
  }

  /**
   * Función Baja ver DDS
   * @param listBaja
   * @throws SIBBACBusinessException
   */
  public void funcionBaja(List<EmisionMaestroValoresDTO> listBaja) throws SIBBACBusinessException {
    final List<String> bajaIsinAnteriorList = new ArrayList<>();

    final List<String> bajaIsinList58 = new ArrayList<>();
    final List<String> bajaIsinList14 = new ArrayList<>();

    for (EmisionMaestroValoresDTO valoresEmision : listBaja) {

      bajaIsinAnteriorList.add(valoresEmision.getCdisin());

      String cdemisio = valoresEmision.getCdemisio();

      bajaIsinList58.add(cdemisio.substring(4, 12));
      bajaIsinList14.add("9999" + cdemisio.substring(0, 4));
    }

    cargaValoresDao.funcionBajaIsin(bajaIsinList58, bajaIsinList14);
    cargaValoresDao.funcionBajaIsinsAnteriores(bajaIsinAnteriorList);
  }

  /**
   * Rellenar Salida MaestroValores DDS
   * @param mValor
   * @return
   */
  public MaestroValores rellenarSalida(EmisionMaestroValoresDTO emisionMaestroValoresDTO) {

    MaestroValores maestroValores = new MaestroValores();

    // Datos obligatorios
    maestroValores.setCdvaibex("N");
    maestroValores.setFhbajacv(FormatDataUtils.convertStringToDate("9999-12-31", FormatDataUtils.DATE_FORMAT_1));
    maestroValores.setCdbesnum(emisionMaestroValoresDTO.getCdbesnum());
    maestroValores.setCdcvalov(emisionMaestroValoresDTO.getCdcvalov());
    maestroValores.setFhiniciv(
        FormatDataUtils.convertStringToDate(emisionMaestroValoresDTO.getFeemisio(), FormatDataUtils.DATE_FORMAT_1));
    maestroValores.setNbtitrev(emisionMaestroValoresDTO.getDsemisio());
    maestroValores.setNunominv(new BigDecimal(emisionMaestroValoresDTO.getImnomina()));
    maestroValores.setCdisinvv(emisionMaestroValoresDTO.getCdisin());
    maestroValores.setCdcvabev(emisionMaestroValoresDTO.getCdisin().substring(3, 11));
    maestroValores.setCdsitval(getCdsitval(emisionMaestroValoresDTO.getSituval()));
    maestroValores.setNbtit10c(emisionMaestroValoresDTO.getCdalfval());
    maestroValores.setCdcvsibv(cdCvsibv);
    maestroValores.setFhultmod(
        FormatDataUtils.convertStringToDate(emisionMaestroValoresDTO.getFeestado(), FormatDataUtils.DATE_FORMAT_1));
    maestroValores.setTpcambio(getTpcambio(emisionMaestroValoresDTO.getCdcambio(), emisionMaestroValoresDTO));
    maestroValores.setCdnominv(getCdnominv(emisionMaestroValoresDTO.getCdformat(), emisionMaestroValoresDTO));
    maestroValores.setCdsisliv(getCdsisliv(emisionMaestroValoresDTO.getCdstliqi(), emisionMaestroValoresDTO));
    maestroValores.setCdpercuv(getCdpercuv(emisionMaestroValoresDTO.getCdisin()));
    maestroValores.setCdcifsoc(emisionMaestroValoresDTO.getCdcifsoc());
    maestroValores.setCdpaisev(emisionMaestroValoresDTO.getCdpaisev());
    final String[] cdCotArray = getCdCot(emisionMaestroValoresDTO.getIndcotiz(), emisionMaestroValoresDTO.getCdbolsa());
    maestroValores.setCdcotmav(cdCotArray[0]);
    maestroValores.setCdcotbav(cdCotArray[1]);
    maestroValores.setCdcotbiv(cdCotArray[2]);
    maestroValores.setCdcotvav(cdCotArray[3]);

    maestroValores
        .setCdmonedv(emisionMaestroValoresDTO.getCdmoneda() == null ? "" : emisionMaestroValoresDTO.getCdmoneda());

    maestroValores.setNudelote(new BigDecimal(emisionMaestroValoresDTO.getNulote()));
    maestroValores.setIdmercon(getIdMerCon(emisionMaestroValoresDTO.getCdvalmdo().substring(0, 1),
        Integer.valueOf(emisionMaestroValoresDTO.getNulote()), emisionMaestroValoresDTO.getIswarrant(),
        emisionMaestroValoresDTO.getCdmercad()));
    maestroValores.setCdgrsibv(getCdgrsibv(emisionMaestroValoresDTO.getCdsector()));
    maestroValores
        .setCdorgano(getCdorgano(emisionMaestroValoresDTO.getCdorgant(), emisionMaestroValoresDTO.getCdbolsa()));

    return maestroValores;
  }

  /**
   * Obtiene el mapeo de getCot 
   * @param indcotiz
   * @param cdbolsa
   * @return
   */
  private String[] getCdCot(String indcotiz, String cdbolsa) {
    if (indcotiz.equals("S")) {
      if (this.cotValuesMap.containsKey(cdbolsa)) {
        return this.cotValuesMap.get(cdbolsa);
      }
      else {
        final String cdbolsasub = cdbolsa.substring(0, 2);
        if (this.cotValuesMap.containsKey(cdbolsasub)) {
          return this.cotValuesMap.get(cdbolsasub);
        }
        else {
          return this.cotValuesMap.get("DEFAULT");
        }
      }
    }
    else {
      return this.cotValuesMap.get("DEFAULT");
    }
  }

  /**
   * Obtiene el mapeo de IdMerCon
   * @param indcotiz
   * @param cdbolsa
   * @return
   */
  private String getIdMerCon(String cdvalmdo, Integer nudelote, Integer warrant, String cdmercad) {
    if (this.idMerconMap.containsKey(cdvalmdo)) {
      return this.idMerconMap.get(cdvalmdo);
    }
    else {
      if (nudelote > 1) {
        return this.idMerconMap.get("NUDELOTE");
      }
      else {
        if (warrant == 0) {
          return this.idMerconMap.get("WARRANT");
        }
        else {
          return this.idMerconMap.get(cdmercad);
        }
      }
    }
  }

  /**
   * Obtiene el mapeo de Sitval
   * @param situval
   * @return
   */
  public String getCdsitval(String situval) {
    if (this.situvalMap.containsKey(situval)) {
      return this.situvalMap.get(situval);
    }
    else {
      return "";
    }
  }

  /**
   * Obtiene el mapeo de Tpcambio
   * @param cdCambio
   * @param emisionMaestroValoresDTO
   * @return
   */
  public String getTpcambio(String cdCambio, EmisionMaestroValoresDTO emisionMaestroValoresDTO) {
    if (this.cdcambioMap.containsKey(cdCambio)) {
      return this.cdcambioMap.get(cdCambio);
    }
    else {
      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("Valor de tipo de cambio incorrecto, %s | %s | %s",
            emisionMaestroValoresDTO.getCdemisio(), emisionMaestroValoresDTO.getCdisin(), "T"));
      }
      return "T";
    }
  }

  /**
   * Obtiene el mapeo de CdNominv
   * @param cdFormat
   * @param emisionMaestroValoresDTO
   * @return
   */
  public String getCdnominv(String cdFormat, EmisionMaestroValoresDTO emisionMaestroValoresDTO) {
    if (this.cdnominvMap.containsKey(cdFormat)) {
      return this.cdnominvMap.get(cdFormat);
    }
    else {
      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("Valor de codigo nominativo incorrecto, %s | %s | %s",
            emisionMaestroValoresDTO.getCdemisio(), emisionMaestroValoresDTO.getCdisin(), "N"));
      }
      return "N";
    }
  }

  /**
   * Obtiene el mapeo de Cdsisliv
   * @param cdstliqi
   * @param emisionMaestroValoresDTO
   * @return
   */
  public String getCdsisliv(String cdstliqi, EmisionMaestroValoresDTO emisionMaestroValoresDTO) {
    if (cdstliqi.equals("0")) {
      return "T";
    }
    else if (cdstliqi.equals("1") || cdstliqi.equals("4")) {
      return "R";
    }
    else {
      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("Valor de sistema de liquidacion incorrecto, %s | %s | %s",
            emisionMaestroValoresDTO.getCdemisio(), emisionMaestroValoresDTO.getCdisin(), "T"));
      }
      return "T";
    }
  }

  /**
   * Obtiene el mapeo de CdPercuv
   * @param cdIsin
   * @return
   */
  public String getCdpercuv(String cdIsin) {
    String cdPercuv = "";
    if (cdIsin.substring(0, 2).equals("ES") && this.cdpercuvMap.containsKey(cdIsin.substring(4, 5))) {
      cdPercuv = this.cdpercuvMap.get(cdIsin.substring(3, 4));
    }
    return cdPercuv;
  }

  /**
   * Obtiene el mapeo de cdgrsibv
   * @param cdSector
   * @return
   */
  public String getCdgrsibv(String cdSector) {
    if (cdSector.equals("46") || cdSector.equals("48")) {
      return "89";
    }
    else {
      return cdSector;
    }
  }

  /** 
   * Obtiene el mapeo de CdOrgano
   * @param cdOrgant
   * @param cdBolsa
   * @return
   */
  public String getCdorgano(String cdOrgant, String cdBolsa) {
    String cdOrgano = cdOrgant;
    if (cdOrgant.substring(0, 3).equals("350") && this.cdorgantMap.containsKey(cdBolsa)) {
      cdOrgano = this.cdorgantMap.get(cdBolsa);
    }
    return cdOrgano;
  }

}
