package sibbac.business.dwh.ficheros.tradersaccounters.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class MantenimientoTraSaleAccQuery.
 */
@Component("MantenimientoTraSaleAccQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MantenimientoTraSaleAccQuery extends AbstractDynamicPageQuery {

  /** The Constant CDEMPLEADO. */
  private static final String CDEMPLEADO = "CDEMPLEADO";

  /** The Constant CUENTA. */
  private static final String CUENTA = "CUENTA";

  /** The Constant FHINICIO. */
  private static final String FHINICIO = "FHINICIO";

  /** The Constant FHFINAL. */
  private static final String FHFINAL = "FHFINAL";

  /** The Constant PRODUCTO. */
  private static final String PRODUCTO = "PRODUCTO";

  /** The Constant NBNOMBRE. */
  private static final String NBNOMBRE = "NBNOMBRE";

  /** The Constant NBAPELLIDO1. */
  private static final String NBAPELLIDO1 = "NBAPELLIDO1";

  /** The Constant NBAPELLIDO2. */
  private static final String NBAPELLIDO2 = "NBAPELLIDO2";

  /** The Constant NBCORTO. */
  private static final String NBCORTO = "NBCORTO";

  /** The Constant TIPO_ID_MIFID. */
  private static final String TIPO_ID_MIFID = "TIPOIDMIFID";

  /** The Constant ID_MIFID. */
  private static final String ID_MIFID = "IDMIFID";

  /** The Constant CDNACIONALIDAD. */
  private static final String CDNACIONALIDAD = "CDNACIONALIDAD";

  /** The Constant FECHA_NACIMIENTO. */
  private static final String FECHA_NACIMIENTO = "FECHANACIMIENTO";

  /** The Constant CODIGO_CORTO. */
  private static final String CODIGO_CORTO = "CODIGOCORTO";

  /** The Constant CD_TIPO_USUARIO. */
  private static final String CD_TIPO_USUARIO = "CDTIPOUSUARIO";

  /** The Constant INDTRADER. */
  private static final String INDTRADER = "INDTRADER";

  /** The Constant INDSALE. */
  private static final String INDSALE = "INDSALE";

  /** The Constant INDTRAD_SALE. */
  private static final String INDTRAD_SALE = "INDTRADSALE";

  /** The Constant INDACCOUNT. */
  private static final String INDACCOUNT = "INDACCOUNT";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT * ";

  /** The Constant FROM. */
  private static final String FROM = " FROM TMCT0_SALES_TRADER ST ";

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE FHFINAL = DATE('31/12/9999') ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("Usuario", CDEMPLEADO),
      new DynamicColumn("Cuenta", CUENTA + PRODUCTO), new DynamicColumn("Nombre", NBNOMBRE),
      new DynamicColumn("Primer Apellido", NBAPELLIDO1), new DynamicColumn("Segundo Apellido", NBAPELLIDO2),
      new DynamicColumn("Nombre Corto", NBCORTO), new DynamicColumn("Tipo ID MIFID", TIPO_ID_MIFID),
      new DynamicColumn("ID MIFID", ID_MIFID), new DynamicColumn("Pais de nacionalidad", CDNACIONALIDAD),
      new DynamicColumn("Fecha de Nacimiento", FECHA_NACIMIENTO, DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Código Corto", CODIGO_CORTO), new DynamicColumn("Tipo de usuario", CD_TIPO_USUARIO),
      new DynamicColumn("Figura Trader", INDTRADER), new DynamicColumn("Figura Sale", INDSALE),
      new DynamicColumn("Figura Trader/Sale", INDTRAD_SALE), new DynamicColumn("Figura Account", INDACCOUNT),
      new DynamicColumn("Fecha Inicio", FHINICIO, DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Fecha Fin", FHFINAL, DynamicColumn.ColumnType.DATE) };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter(CDEMPLEADO, "Código de Usuario"));
    res.add(new StringDynamicComplexFilter(CUENTA, "Código de cuenta"));
    res.add(new DateDynamicFilter(FHINICIO, "Fecha inicio"));
    res.add(new StringDynamicFilter("TIPOFIGURA", "Tipo Figura"));
    res.add(new StringDynamicFilter(INDTRADER, ""));
    res.add(new StringDynamicFilter(INDSALE, ""));
    res.add(new StringDynamicFilter("INDTRAD_SALE", ""));
    res.add(new StringDynamicFilter(INDACCOUNT, ""));
    return res;
  }

  public MantenimientoTraSaleAccQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public MantenimientoTraSaleAccQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public MantenimientoTraSaleAccQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }

  /**
   * Implementa el filtro complejo
   * @author jgarvila
   *
   */
  public static class StringDynamicComplexFilter extends StringDynamicFilter {

    private static final long serialVersionUID = 8651860688765840632L;

    public StringDynamicComplexFilter(String field, String messageKey) {
      super(field, messageKey);
    }

    @Override
    public String getFilterSqlFormat() throws SIBBACBusinessException {
      final StringBuilder valuesStr = new StringBuilder();

      for (int i = 0; i < this.values.size(); i++) {
        final String value = this.values.get(i);
        valuesStr.append(String.format("'%s'", value));
        if (i < this.values.size() - 1) {
          valuesStr.append(", ");
        }
      }

      return String.format("CONCAT(CUENTA, PRODUCTO) IN (%s)", valuesStr.toString());
    }
  }
}
