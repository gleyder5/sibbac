package sibbac.business.dwh.ficheros.valores.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.TipoValoresDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateTipoValoresFile")
public class GenerateTipoValoresFile extends AbstractGenerateFile {

  private static final String VALORES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateTipoValoresFile.class);
  
  /**
   * DAO de Acceso a Base de Datos 
   */
  @Autowired
  private TipoValoresDao tipoValoresDao;

  @Override
  public String getLineSeparator() {
    return VALORES_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return tipoValoresDao.findAllListTipoValores(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return tipoValoresDao.countAllListTipoValores();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    String cdG = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "95");
    String nbG = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), "NO EXISTE DESCRIPCION");
    String codR = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(), "V");
    String nbR = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), "VALOR DE RENTA VARIABLE");
    String linea1 = cdG + nbG + codR + nbR;

    String cadenaPrev = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), linea1);
    writeLine(outWriter, cadenaPrev);
    for (Object[] objects : fileData) {
      if(!String.valueOf(objects[0]).equals("null") && !String.valueOf(objects[1]).equals("null") && !String.valueOf(objects[2]).equals("null") && !String.valueOf(objects[3]).equals("null"))
      {
        String codTipoValor = String.format(DwhConstantes.PaddingFormats.PADDIND_NUM0_2.value(),
            Integer.parseInt((String) objects[0]));
        String nomTipoValor = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), (String) objects[1]);
        String codTipoRenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(), (String) objects[2]);
        String descTipoRenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), (String) objects[3]);

        final StringBuilder rowLine = new StringBuilder();

        rowLine.append(codTipoValor).append(nomTipoValor).append(codTipoRenta).append(descTipoRenta);

        String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

        writeLine(outWriter, cadena);
      }
    }
  }

}
