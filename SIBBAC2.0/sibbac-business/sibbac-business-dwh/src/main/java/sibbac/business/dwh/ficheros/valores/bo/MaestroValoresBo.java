package sibbac.business.dwh.ficheros.valores.bo;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.MaestroValoresDao;
import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;
import sibbac.business.dwh.ficheros.valores.dto.MaestroValoresDTO;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums.ApplicationMessages;
import sibbac.common.utils.SibbacEnums.LoggingMessages;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class TMCT0BlanqueoControlDocumentacionBo.
 */
@Service("maestroValoresBo")
public class MaestroValoresBo extends AbstractBo<MaestroValores, BigInteger, MaestroValoresDao>
    implements BaseBo<MaestroValoresDTO> {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MaestroValoresBo.class);

  /** The session. */
  @Autowired
  private HttpSession session;

  /**
   * 
   */
  @Override
  public CallableResultDTO actionSave(MaestroValoresDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    final CallableResultDTO result = callableResultDTO;

    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Comienza el proceso 'actionSave'");
      }
      
      Date fechaBajacv = removeTime(solicitud.getFhbajacv());
      Date fechaMax = removeTime(this.fechaBajaMax());
      
      if (fechaMax.before(fechaBajacv)) {
        LOGGER.error("Fecha baja no permitida, no puede ser mayor que el 31/12/9999");
        throw new SIBBACBusinessException("Fecha baja no permitida, no puede ser mayor que el 31/12/9999");
      }
      if (solicitud != null) {
        if (solicitud.getFhiniciv() != null && !solicitud.getCdisinvv().isEmpty() && !solicitud.getNbtitrev().isEmpty()
            && !solicitud.getCdcvabev().isEmpty() && !solicitud.getCdmonedv().isEmpty()) {

          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Se han validado todos los datos correctamente");
          }
          Object attribute = session.getAttribute(LoggingMessages.USER_SESSION.value());
          String name = ((UserSession) attribute).getName();
          String user = StringUtils.substring(name, 0, 10);
          solicitud.setUsualta(user);

          solicitud.setFhaltav(new Date());

          DozerBeanMapper mapper = new DozerBeanMapper();

          MaestroValores mapEntity = mapper.map(solicitud, MaestroValores.class);
          
          // Por defecto este valor irá a N
          if (StringUtils.isEmpty(mapEntity.getCdvaibex())) {
            mapEntity.setCdvaibex("N");
          }
         
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Guardando [" + mapEntity.toString() + "] en BBDD");
          }
          dao.save(mapEntity);

          LOGGER.debug("Registro insertado correctamente");
          result.addMessage(ApplicationMessages.ADDED_DATA.value());

        }
        else {
          LOGGER.debug(ApplicationMessages.INVALID_VALIDATION.value(), new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value()));
          throw new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value());
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error(LoggingMessages.BASIC_VALIDATION_ERROR.value(), e);
      result.addErrorMessage(e.getMessage());
    }
    catch (RuntimeException rex) {
      LOGGER.error(LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    catch (ParseException e) {
      LOGGER.error(LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), e);
      result.addErrorMessage(ApplicationMessages.PARSE_FAILED.value());
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finaliza el proceso 'actionSave'");
    }
    return result;
  }

  /**
   * 
   */
  @Override
  public CallableResultDTO actionDelete(List<String> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Comienza el proceso 'actionDelete'");
      }
      for (String solicitud : solicitudList) {
        if (solicitud != null) {
          MaestroValores entity = this.findById(new BigInteger(solicitud));
          if (entity != null) {

            entity.setUsubaja(StringUtils.substring(
                ((UserSession) session.getAttribute(LoggingMessages.USER_SESSION.value())).getName(), 0, 10));
            entity.setFhbajacv(new Date());
            entity.setFhultmod(new Date());

            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Eliminado [" + entity.toString() + "] en BBDD");
            }
            dao.save(entity);

          }
          LOGGER.debug("Registro(s) borrado(s) correctamente");
          result.addMessage(ApplicationMessages.DELETED_DATA.value());
        }
      }
      if (solicitudList.isEmpty()) {
        LOGGER.error(ApplicationMessages.EMPTY_CALL.value(),
            new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value()));
        throw new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error(LoggingMessages.MULTIPLE_ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(MessageFormat.format(ApplicationMessages.MULTIPLE_NOT_EXIST.value(), err.toString()));
    }
    catch (RuntimeException rex) {
      LOGGER.error(LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finaliza el proceso 'actionDelete'");
    }
    return result;
  }

  @Override
  public CallableResultDTO actionFindById(MaestroValoresDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO actionUpdate(MaestroValoresDTO solicitud) {
    CallableResultDTO result = new CallableResultDTO();
    try {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Comienza el proceso 'actionUpdate'");
      }
      if (solicitud != null) {
        MaestroValores entitydb = dao.findOne(solicitud.getIdvalor());
        
        Date fechaBajacv = removeTime(solicitud.getFhbajacv());
        Date fechaMax = removeTime(this.fechaBajaMax());
        
        if (fechaMax.before(fechaBajacv)) {
          LOGGER.error("Fecha baja no permitida, no puede ser mayor que el 31/12/9999");
          throw new SIBBACBusinessException("Fecha baja no permitida, no puede ser mayor que el 31/12/9999");
        }
        if (entitydb != null) {
          if (solicitud.getIdvalor() != null && solicitud.getFhiniciv() != null && !solicitud.getCdisinvv().isEmpty()
              && !solicitud.getNbtitrev().isEmpty() && !solicitud.getCdcvabev().isEmpty()
              && !solicitud.getCdmonedv().isEmpty()) {

            DozerBeanMapper mapper = new DozerBeanMapper();

            solicitud.setUsumodi(StringUtils.substring(
                ((UserSession) session.getAttribute(LoggingMessages.USER_SESSION.value())).getName(), 0, 10));
            solicitud.setFhmodiv(new Date());
            solicitud.setFhultmod(new Date());

            MaestroValores maestroValoresEntity = mapper.map(solicitud, MaestroValores.class);
            
            if (LOGGER.isDebugEnabled()) {
              LOGGER.debug("Modificando [" + entitydb.toString() + "] en BBDD");
            }
            dao.save(maestroValoresEntity);

            LOGGER.debug("Registro modificado correctamente");
            result.addMessage(ApplicationMessages.MODIFIED_DATA.value());

          }
          else {
            LOGGER.debug(ApplicationMessages.INVALID_VALIDATION.value(), new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value()));
            throw new SIBBACBusinessException(ApplicationMessages.INVALID_VALIDATION.value());
          }
        }
        else {
          LOGGER.error("La primarykey del objeto mandado no existe",
              new SIBBACBusinessException(LoggingMessages.ID_OBJECT_NOT_FIND.value()));
          throw new SIBBACBusinessException(LoggingMessages.ID_OBJECT_NOT_FIND.value());
        }
      }
      if (solicitud == null) {
        LOGGER.error(ApplicationMessages.EMPTY_CALL.value(),
            new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value()));
        throw new SIBBACBusinessException(ApplicationMessages.EMPTY_CALL.value());
      }
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Finaliza el proceso 'actionUpdate'");
      }
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error(LoggingMessages.BASIC_VALIDATION_ERROR.value(), e);
      result.addErrorMessage(e.getMessage());
    }
    catch (RuntimeException rex) {
      LOGGER.error(LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    catch (ParseException e) {
      LOGGER.error(LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), e);
      result.addErrorMessage(ApplicationMessages.PARSE_FAILED.value());
    }
    return result;
  }
  
  public static Date removeTime(Date date) {    
    Calendar cal = Calendar.getInstance();  
    cal.setTime(date);  
    cal.set(Calendar.HOUR_OF_DAY, 0);  
    cal.set(Calendar.MINUTE, 0);  
    cal.set(Calendar.SECOND, 0);  
    cal.set(Calendar.MILLISECOND, 0);  
    return cal.getTime(); 
  }

  private Date fechaBajaMax() throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    return simpleDateFormat.parse("9999-12-31");
  }
}
