package sibbac.business.dwh.ficheros.clientes.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.clientes.db.model.Tmct0DwhClientes;

@Repository
public interface DWHClientesEstaticosDao extends JpaRepository<Tmct0DwhClientes, String> {

  /**
   * Query para DWHclientesEstaticosDao, ORIGINAL
   */
  public static final String CLIENTES_ESTATICOS_QUERY = "SELECT CDCLIENT,NBDESCLI,NUCLIENT,CDPRODUC,"
      + "CDGRUPOO,NBGRUPOO,CDCLITRA,CDCLISAL,CDCLIACM,CDHPPAIS,NUNIVEL0,NBNIVEL0,NUNIVEL1,NBNIVEL1,NUNIVEL2,NBNIVEL2,NUNIVEL3,NBNIVEL3,"
      + "NUNIVEL4,NBNIVEL4,CDGRUPOA,NBGRUPOA FROM TMCT0_DWH_CLIENTES";

  /**
   * Query para DWHclientesEstaticosDao con Paginación
   */
  public static final String CLIENTES_ESTATICOS_PAGE_QUERY = CLIENTES_ESTATICOS_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHclientesEstaticosDao
   */
  public static final String CLIENTES_ESTATICOS_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + CLIENTES_ESTATICOS_QUERY
      + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHclientesEstaticosDao
   * sin paginación
   * @return
   */
  @Query(value = CLIENTES_ESTATICOS_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListClientesEstaticos(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHclientesEstaticosDao
   * sin paginación
   * @return
   */
  @Query(value = CLIENTES_ESTATICOS_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListClientesEstaticos();

}
