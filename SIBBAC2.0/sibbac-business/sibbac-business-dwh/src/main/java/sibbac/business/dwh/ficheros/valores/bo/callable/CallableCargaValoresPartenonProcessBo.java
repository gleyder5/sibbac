package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.valores.dto.EmisionMaestroValoresDTO;
import sibbac.business.dwh.utils.DwhConstantes.TipoValoresValidation;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.bo.QueryBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableCargaValoresPartenonProcessBo extends AbstractCallableProcessBo {

  private static final Logger LOG = LoggerFactory.getLogger(CallableCargaValoresPartenonProcessBo.class);

  private static final String QUERY_NAME = "EmisionMaestroValoresQuery";

  /** Tamaño de la pagina */
  @Value("${fileName.dwh.partenon.read.batch:200}")
  private int pageSize;

  /** Número de Hilos de ejecucion */
  @Value("${fileName.dwh.partenon.threads:5}")
  private int threads;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${sibbac.batch.timeout: 90}")
  private int timeout;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private QueryBo queryBo;

  private List<EmisionMaestroValoresDTO> valoresEmisionBajaList;

  private List<EmisionMaestroValoresDTO> valoresEmisionAltaList;

  private List<EmisionMaestroValoresDTO> valoresEmisionModList;

  @Override
  public Boolean executeProcess() {
    Boolean result = false;

    if (LOG.isInfoEnabled()) {
      LOG.info("Se inicia el proceso de validacion");
    }

    try {
      // Ejecutamos el proceso de carga
      executeValoresProcess();
    }
    catch (SIBBACBusinessException e) {
      LOG.error("Se ha producido un error: ", e);
      result = false;
    }
    if (LOG.isInfoEnabled()) {
      LOG.info("Se termina el proceso de validacion");
    }
    return result;
  }

  /**
   * Procesa las listas y las setea en sus respectivos BO para poder ejecutarlos
   * con posterioridad
   * @param valoresEmisorList
   */
  public void executeValoresProcess() throws SIBBACBusinessException {
    try {
      long initStartTime = System.currentTimeMillis();

      final AbstractDynamicPageQuery adpq = ctx.getBean(QUERY_NAME, AbstractDynamicPageQuery.class);

      final List<EmisionMaestroValoresDTO> resultList = queryBo.executeQueryNativeMapped(adpq,
          EmisionMaestroValoresDTO.class);

      if (LOG.isDebugEnabled()) {
        LOG.debug(String.format("La consulta '%s' ha tardado %d milisegundos en recuperar %d elementos", QUERY_NAME,
            (System.currentTimeMillis() - initStartTime), resultList.size()));
      }

      // Obtiene las listas para ser tratadas de forma independiente
      splitValidationList(resultList);

      // Lanzamos el proceso de Validacion
      launchValidationProcess();
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error ejecutando executeValoresProcess", e);
    }
  }

  /**
   * Procesa la lista resultante y la
   */
  private void splitValidationList(List<EmisionMaestroValoresDTO> resultList) {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Iniciamos el proceso de repartición de datos");
    }

    final List<EmisionMaestroValoresDTO> valoresEmisionAltaExitsList = new ArrayList<>();
    valoresEmisionBajaList = new ArrayList<>();
    valoresEmisionAltaList = new ArrayList<>();
    valoresEmisionModList = new ArrayList<>();

    long initStartTime = System.currentTimeMillis();

    String cdEstado = null;
    Integer counter = 0;

    for (EmisionMaestroValoresDTO emisionMaestroValoresDTO : resultList) {
      cdEstado = emisionMaestroValoresDTO.getCdestado();
      counter = emisionMaestroValoresDTO.getExistmaestrovalores();

      // Ver algoritmo del DDS pag 19
      if (cdEstado.equals("A")) {
        if (counter > 0) {
          // Si devuelve registros, escribimos en el log
          if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Emisión ya dada de alta -> %s|%s|%s", emisionMaestroValoresDTO.getCdemisio(),
                emisionMaestroValoresDTO.getCdisin(), emisionMaestroValoresDTO.getCdestado()));
          }
          valoresEmisionAltaExitsList.add(emisionMaestroValoresDTO);
        }
        else {
          // sino funcionAlta (lo metemos en una lista para su procesado por
          // lotes)
          valoresEmisionAltaList.add(emisionMaestroValoresDTO);
        }
      }
      else if (cdEstado.equals("M")) {
        if (counter > 0) {
          // Si devuelve registros, funcionAlta (lo metemos en una lista para su
          // procesado por lotes)
          valoresEmisionModList.add(emisionMaestroValoresDTO);
        }
        else {
          // sino funcionAlta (lo metemos en una lista para su procesado por
          // lotes)
          valoresEmisionAltaList.add(emisionMaestroValoresDTO);
        }
      }
      else if (cdEstado.equals("B")) {
        valoresEmisionBajaList.add(emisionMaestroValoresDTO);
      }
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("La repartición de '%d' ha tardado %d milisegundos", resultList.size(),
          (System.currentTimeMillis() - initStartTime)));
      LOG.debug(String.format("Valores a ya dados de alta %d elementos", valoresEmisionAltaExitsList.size()));
      LOG.debug(String.format("Valores a funcionAlta %d elementos", valoresEmisionAltaList.size()));
      LOG.debug(String.format("Valores a funcionMod %d elementos", valoresEmisionModList.size()));
      LOG.debug(String.format("Valores a funcionBaja %d elementos", valoresEmisionBajaList.size()));
    }
  }

  /**
   * Lanza el proceso de validacion en Hilos
   */
  private void launchValidationProcess() throws SIBBACBusinessException {
    long startTimeProcessFile = System.currentTimeMillis();

    final List<CallableTableConcurrentProcessLoaderBo> futureList = new ArrayList<>();

    boolean onTime = true;

    final ExecutorService executor = Executors.newFixedThreadPool(3, new NamedThreadFactory("validationEmision"));

    final CallableCargaValoresPartenonValidationBo altaValidation = ctx.getBean(CallableCargaValoresPartenonValidationBo.class);
    altaValidation.setValidationList(valoresEmisionAltaList);
    altaValidation.setTipoValoresValidation(TipoValoresValidation.VALIDACION_ALTA);
    altaValidation.setFuture(executor.submit(altaValidation));

    final CallableCargaValoresPartenonValidationBo bajaValidation = ctx.getBean(CallableCargaValoresPartenonValidationBo.class);
    bajaValidation.setValidationList(null);
    bajaValidation.setTipoValoresValidation(TipoValoresValidation.VALIDACION_BAJA);
    bajaValidation.setFuture(executor.submit(bajaValidation));

    final CallableCargaValoresPartenonValidationBo modValidation = ctx.getBean(CallableCargaValoresPartenonValidationBo.class);
    modValidation.setValidationList(null);
    modValidation.setTipoValoresValidation(TipoValoresValidation.VALIDACION_MOD);
    modValidation.setFuture(executor.submit(modValidation));

    executor.shutdown();

    try {
      onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

      if (!onTime) { // *
        for (CallableTableConcurrentProcessLoaderBo future : futureList) {
          if (!future.getFuture().isDone()) {
            future.getFuture().cancel(true);
            throw new SIBBACBusinessException("Error, se ha agotado el tiempo de ejecucion para la carga concurrente");
          }
        }
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error, se ha agotado el tiempo de ejecucion para la carga concurrente", e);
    }

    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("El proceso de validacion ha terminado en %d milisegundos",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }
  }

}
