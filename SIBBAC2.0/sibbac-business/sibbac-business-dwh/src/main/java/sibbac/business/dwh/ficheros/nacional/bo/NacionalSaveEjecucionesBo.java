package sibbac.business.dwh.ficheros.nacional.bo;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.nacional.db.dao.Tmct0DwhEjecucionesDao;
import sibbac.business.dwh.ficheros.nacional.db.model.Tmct0DwhEjecuciones;
import sibbac.database.bo.AbstractBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NacionalSaveEjecucionesBo extends AbstractBo<Tmct0DwhEjecuciones, BigInteger, Tmct0DwhEjecucionesDao> {

  @Autowired
  Tmct0DwhEjecucionesDao tmct0DwhEjecucionesDao;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  /**
   * Método para guardar en la BD cada una de las Tmct0DwhEjecuciones que se han
   * ido generando en el proceso del fichero
   * @param ejecuciones
   */
  public void saveEjecuciones(List<Tmct0DwhEjecuciones> ejecuciones) {

    if (!ejecuciones.isEmpty() && ejecuciones != null) {
      saveBulkEjecuciones(ejecuciones);
    }
    else {
      LOG.debug("No hay ejecuciones para guardar en Tmct0DwhEjecuciones. Se borra la tabla");
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkEjecuciones(List<Tmct0DwhEjecuciones> ejecuciones) {
    Integer resultCounter = 0;
    int commitInterval = 100;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (Tmct0DwhEjecuciones tmct0DwhEjecuciones : ejecuciones) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          entityTransaction.commit();
          entityTransaction.begin();
          entityManager.clear();
        }

        entityManager.persist(tmct0DwhEjecuciones);
        resultCounter++;
      }

      entityTransaction.commit();
    }
    catch (RuntimeException e) {
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw e;
    }
    finally {
      entityManager.close();
    }

    return resultCounter;
  }

}
