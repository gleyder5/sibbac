package sibbac.business.dwh.ficheros.valores.bo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.MonedaDao;
import sibbac.business.dwh.ficheros.valores.db.model.Moneda;
import sibbac.business.dwh.ficheros.valores.dto.MonedaDTO;
import sibbac.database.bo.AbstractBo;

/**
 * The Class MonedaBo.
 */
@Service
public class MonedaBo extends AbstractBo<Moneda, String, MonedaDao> {

  /**
   * Gets the cdmoneda and nbdescripcion.
   *
   * @return the cdmoneda and nbdescripcion
   */
  public List<MonedaDTO> getCdmonedaAndNbdescripcion() {
    final List<Object[]> cdmonedaAndNbdescripcion = dao.getCdmonedaAndNbdescripcion();
    final List<MonedaDTO> auxList = new ArrayList<>();
    
    MonedaDTO monedaDTO = null;
    for (Object[] objects : cdmonedaAndNbdescripcion) {    
      if (objects[0] != null && objects[1] != null) {
        monedaDTO = new MonedaDTO();
        monedaDTO.setCdmoneda(objects[0].toString());
        monedaDTO.setNbmoneda(objects[1].toString());
        auxList.add(monedaDTO); 
      }
    }
    return auxList;
  }
}
