package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresEmisionBo;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresEmision;
import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableTableEmisionLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private ValoresEmisionBo valoresEmisionBo;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    valoresEmisionBo.saveBulkValoresEmision(toValoresEmisionEntity());
    return true;
  }

  /**
   * Convierte una lista de String a una lista de Entidades
   * @param emisionStringList
   * @return
   */
  private List<ValoresEmision> toValoresEmisionEntity() {
    final List<ValoresEmision> entities = new ArrayList<>();

    for (PartenonFileLineDTO element : listData) {
      entities.add(ValoresEmision.createValoresEmision(element));
    }
    return entities;
  }
}
