package sibbac.business.dwh.ficheros.nacional.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO para trabajar con las reglas de Exoneracion
 * 
 */
public class DesgloseNacionalDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3496535749119892709L;

  private static int DESGLOSE_NUORDEN = 0;
  private static int DESGLOSE_NBOOKING = 1;
  private static int DESGLOSE_NUCNFCLT = 2;
  private static int DESGLOSE_NUCNFLIQ = 3;
  private static int DESGLOSE_CDMIEMBROMKT = 4;
  private static int DESGLOSE_CDPLATAFORMA_NEGOCIACION = 5;
  private static int DESGLOSE_TPOPEBOL = 6;
  private static int DESGLOSE_CDSEGMENTO = 7;
  private static int DESGLOSE_IMPRECIO = 8;
  private static int DESGLOSE_TOTAL_IMTITULOS = 9;
  private static int DESGLOSE_TOTAL_IMCANONCONTREU = 10;
  private static int DESGLOSE_TOTAL_IMCANONCOMPEU = 11;
  private static int DESGLOSE_TOTAL_IMEFECTIVO = 12;

  /**
   * 
   */

  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private BigDecimal nucnfliq;
  private String cdmiembromkt;
  private String cdPlataformaNegociacion;
  private String tpopebol;
  private String cdsegmento;
  private BigDecimal imprecio;
  private BigDecimal totalImtitulos;
  private BigDecimal totalImcanoncontreu;
  private BigDecimal totalImcanoncompeu;
  private BigDecimal totalImefectivo;

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public BigDecimal getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(BigDecimal nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @return the cdmiembromkt
   */
  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  /**
   * @param cdmiembromkt the cdmiembromkt to set
   */
  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  /**
   * @return the cdPlataformaNegociacion
   */
  public String getCdPlataformaNegociacion() {
    return cdPlataformaNegociacion;
  }

  /**
   * @param cdPlataformaNegociacion the cdPlataformaNegociacion to set
   */
  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  /**
   * @return the tpopebol
   */
  public String getTpopebol() {
    return tpopebol;
  }

  /**
   * @param tpopebol the tpopebol to set
   */
  public void setTpopebol(String tpopebol) {
    this.tpopebol = tpopebol;
  }

  /**
   * @return the cdsegmento
   */
  public String getCdsegmento() {
    return cdsegmento;
  }

  /**
   * @param cdsegmento the cdsegmento to set
   */
  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  /**
   * @return the imprecio
   */
  public BigDecimal getImprecio() {
    return imprecio;
  }

  /**
   * @param imprecio the imprecio to set
   */
  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  /**
   * @return the totalImtitulos
   */
  public BigDecimal getTotalImtitulos() {
    return totalImtitulos;
  }

  /**
   * @param totalImtitulos the totalImtitulos to set
   */
  public void setTotalImtitulos(BigDecimal totalImtitulos) {
    this.totalImtitulos = totalImtitulos;
  }

  /**
   * @return the totalImcanoncontreu
   */
  public BigDecimal getTotalImcanoncontreu() {
    return totalImcanoncontreu;
  }

  /**
   * @param totalImcanoncontreu the totalImcanoncontreu to set
   */
  public void setTotalImcanoncontreu(BigDecimal totalImcanoncontreu) {
    this.totalImcanoncontreu = totalImcanoncontreu;
  }

  /**
   * @return the totalImcanoncompeu
   */
  public BigDecimal getTotalImcanoncompeu() {
    return totalImcanoncompeu;
  }

  /**
   * @param totalImcanoncompeu the totalImcanoncompeu to set
   */
  public void setTotalImcanoncompeu(BigDecimal totalImcanoncompeu) {
    this.totalImcanoncompeu = totalImcanoncompeu;
  }

  /**
   * @return the totalImefectivo
   */
  public BigDecimal getTotalImefectivo() {
    return totalImefectivo;
  }

  /**
   * @param totalImefectivo the totalImefectivo to set
   */
  public void setTotalImefectivo(BigDecimal totalImefectivo) {
    this.totalImefectivo = totalImefectivo;
  }

  public static DesgloseNacionalDTO initialize() {
    DesgloseNacionalDTO desglose = new DesgloseNacionalDTO();
    desglose.setNuorden("");
    desglose.setNbooking("");
    desglose.setNucnfclt("");
    desglose.setNucnfliq(BigDecimal.ZERO);
    desglose.setCdmiembromkt("");
    desglose.setCdPlataformaNegociacion("");
    desglose.setTpopebol("");
    desglose.setCdsegmento("");
    desglose.setImprecio(BigDecimal.ZERO);
    desglose.setTotalImtitulos(BigDecimal.ZERO);
    desglose.setTotalImcanoncontreu(BigDecimal.ZERO);
    desglose.setTotalImcanoncompeu(BigDecimal.ZERO);
    desglose.setTotalImefectivo(BigDecimal.ZERO);

    return desglose;
  }

  /**
   * @param desg
   * @return
   */
  public static DesgloseNacionalDTO mapObject(Object[] desg) {
    DesgloseNacionalDTO desglose = new DesgloseNacionalDTO();
    desglose.setNuorden((String) desg[DESGLOSE_NUORDEN]);
    desglose.setNbooking((String) desg[DESGLOSE_NBOOKING]);
    desglose.setNucnfclt((String) desg[DESGLOSE_NUCNFCLT]);
    desglose.setNucnfliq((BigDecimal) desg[DESGLOSE_NUCNFLIQ]);
    desglose.setCdmiembromkt((String) desg[DESGLOSE_CDMIEMBROMKT]);
    desglose.setCdPlataformaNegociacion((String) desg[DESGLOSE_CDPLATAFORMA_NEGOCIACION]);
    desglose.setTpopebol((String) desg[DESGLOSE_TPOPEBOL]);
    desglose.setCdsegmento((String) desg[DESGLOSE_CDSEGMENTO]);
    desglose.setImprecio((BigDecimal) desg[DESGLOSE_IMPRECIO]);
    desglose.setTotalImtitulos((BigDecimal) desg[DESGLOSE_TOTAL_IMTITULOS]);
    desglose.setTotalImcanoncontreu((BigDecimal) desg[DESGLOSE_TOTAL_IMCANONCONTREU]);
    desglose.setTotalImcanoncompeu((BigDecimal) desg[DESGLOSE_TOTAL_IMCANONCOMPEU]);
    desglose.setTotalImefectivo((BigDecimal) desg[DESGLOSE_TOTAL_IMEFECTIVO]);

    return desglose;
  }
}
