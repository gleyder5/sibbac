package sibbac.business.dwh.ficheros.valores.db.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "TMCT0_VALORES_GRUPO")
@Entity
public class ValoresGrupo implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1014715701004521139L;

  @Id
  @Column(name = "CDGRSIBV", nullable = false, length = 2)
  private String cdSibe;

  @Column(name = "NBGRSIBV", nullable = false, length = 31)
  private String descSibe;

  @Column(name = "CDGRUVAL", nullable = false, length = 2)
  private String cdgruval;

  @Column(name = "NBGRUVAL", nullable = false, length = 31)
  private String nbgruval;

  @Column(name = "CDTIPREN", nullable = false, length = 1)
  private String cdtipren;

  @Column(name = "NBTIPREN", nullable = false, length = 31)
  private String nbtipren;

  @Column(name = "FHAUDIT", nullable = false, length = 26)
  private Timestamp fhaudit;

  @Column(name = "USUAUDIT", nullable = false, length = 100)
  private String usuaudit;

  public String getCdSibe() {
    return cdSibe;
  }

  public void setCdSibe(String cdSibe) {
    this.cdSibe = cdSibe;
  }

  public String getDescSibe() {
    return descSibe;
  }

  public void setDescSibe(String descSibe) {
    this.descSibe = descSibe;
  }

  public String getCdgruval() {
    return cdgruval;
  }

  public void setCdgruval(String cdgruval) {
    this.cdgruval = cdgruval;
  }

  public String getNbgruval() {
    return nbgruval;
  }

  public void setNbgruval(String nbgruval) {
    this.nbgruval = nbgruval;
  }

  public String getCdtipren() {
    return cdtipren;
  }

  public void setCdtipren(String cdtipren) {
    this.cdtipren = cdtipren;
  }

  public String getNbtipren() {
    return nbtipren;
  }

  public void setNbtipren(String nbtipren) {
    this.nbtipren = nbtipren;
  }

  public Timestamp getFhaudit() {
    return fhaudit != null ? (Timestamp) fhaudit.clone() : null;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = (fhaudit != null ? (Timestamp) fhaudit.clone() : null);
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }
}
