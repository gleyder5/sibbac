package sibbac.business.dwh.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.dwh.ficheros.callable.results.CallableProcessFileResult;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.nacional.bo.GenerateFechaFile;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableGeneracionFicherosEjecucion;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableGeneracionFicherosEjecucion.TipoFicheroEjecuciones;
import sibbac.business.dwh.tasks.commons.TaskProcessDWH;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_FICHERO_NACIONAL, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 25 ? * MON-FRI")
public class TaskGeneracionDWHEjecuciones extends WrapperTaskConcurrencyPrevent implements TaskProcessDWH {

  private static final Logger LOGGER = LoggerFactory.getLogger(TaskGeneracionDWHEjecuciones.class);

  @Autowired
  private ApplicationContext ctx;

  /**
   * Tamaño de la paginacion
   */
  @Value("${dwh.ficheros.nacional.db.default.page.size:500}")
  private int dbPageSize;

  /**
   * Directorio de escritura de DWH
   */
  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String outPutDir;

  /**
   * Nombre del fichero para la generacion del fichero de Grupo de Clientes
   * Estáticos
   */
  @Value("${dwh.ficheros.nacional.generated_if_empty:true}")
  private Boolean generateIfEmpty;

  /**
   * Timeout del proceso de ejecuciones
   */
  @Value("${dwh.ficheros.nacional.batch.timeout.global: 90}")
  private int timeout;

  @Autowired
  private GenerateFechaFile generateFechaFile;

  private Date fechaEjecucion = null;

  /**
   * Listado de resultados de la generacion de ficheros
   */
  private List<ProcessFileResultDTO> processFileResultsList;

  /**
   * Ruta donde se deposita el fichero
   */
  @Override
  public void executeTask() throws SIBBACBusinessException {
    this.launchProcess();
  }

  /**
   * Lanza todos los ficheros de ejecución nacional
   */
  @Override
  public void launchProcess() throws SIBBACBusinessException {
    processFileResultsList = new ArrayList<>();

    final long initStartTime = System.currentTimeMillis();

    try {
      processFileResultsList.addAll(launchGeneracionEjecuciones());
    }
    catch (InterruptedException | ExecutionException exception) {
      throw new SIBBACBusinessException(exception);
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug(
          String.format("El proceso ha terminado, se ha ejecutado en %d", System.currentTimeMillis() - initStartTime));
    }
  }

  /**
   * Lanza todos los ficheros de ejecución nacional
   */
  public List<ProcessFileResultDTO> launchGeneracionEjecuciones()
      throws InterruptedException, SIBBACBusinessException, ExecutionException {
    final List<ProcessFileResultDTO> results = new ArrayList<>();

    if (fechaEjecucion == null) {
      this.fechaEjecucion = new Date(System.currentTimeMillis());
    }

    results.add(generateFechaFile.generateFileFicheroFecha(fechaEjecucion, "fecha"));

    results.add(generateFechaFile.generateFileFicheroFecha(fechaEjecucion, "fechaDesde"));

    final long initStartTime = System.currentTimeMillis();

    results.addAll(launchAutexNacionalMinoristaInstitucional());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(
          String.format("El proceso ha terminado, se ha ejecutado en %d", System.currentTimeMillis() - initStartTime));
    }

    fechaEjecucion = null;

    return results;
  }

  /**
   * Lanza el planificador
   * @throws SIBBACBusinessException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  private List<ProcessFileResultDTO> launchAutexNacionalMinoristaInstitucional()
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final List<CallableProcessFileResult> processInterfaces = new ArrayList<>();

    // Generamos el fichero nacional
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGeneracionFicherosEjecucion generacionFicheroNacional = ctx
        .getBean(CallableGeneracionFicherosEjecucion.class);

    generacionFicheroNacional.setTipoFichero(TipoFicheroEjecuciones.NACIONAL);
    generacionFicheroNacional.getGenerateEjecucionesNacionalFile().setFechaEjecucionNacional(fechaEjecucion);

    processInterfaces.add(generacionFicheroNacional);

    // Generamos el fichero autex, seteando su paginación y fecha.
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGeneracionFicherosEjecucion generacionFicheroAutex = ctx
        .getBean(CallableGeneracionFicherosEjecucion.class);

    generacionFicheroAutex.setTipoFichero(TipoFicheroEjecuciones.AUTEX);
    generacionFicheroAutex.getGenerateOperacionesAutexFile().configureName();
    generacionFicheroAutex.getGenerateOperacionesAutexFile().setOutPutDir(outPutDir);
    generacionFicheroAutex.getGenerateOperacionesAutexFile().setDbPageSize(dbPageSize);
    generacionFicheroAutex.getGenerateOperacionesAutexFile().setGenerateIfEmpty(generateIfEmpty);
    generacionFicheroAutex.getGenerateOperacionesAutexFile().setFechaEjecucionAutex(fechaEjecucion);

    processInterfaces.add(generacionFicheroAutex);

    // Generamos el fichero de Minoristas
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGeneracionFicherosEjecucion generacionFicheroMinoristas = ctx
        .getBean(CallableGeneracionFicherosEjecucion.class);

    generacionFicheroMinoristas.setTipoFichero(TipoFicheroEjecuciones.MINORISTA);
    generacionFicheroMinoristas.getGenerateOIMinoristasFile().configureName();
    generacionFicheroMinoristas.getGenerateOIMinoristasFile().setOutPutDir(outPutDir);
    generacionFicheroMinoristas.getGenerateOIMinoristasFile().setDbPageSize(dbPageSize);
    generacionFicheroMinoristas.getGenerateOIMinoristasFile().setGenerateIfEmpty(generateIfEmpty);
    generacionFicheroMinoristas.getGenerateOIMinoristasFile()
        .setMinoristasParams(DwhConstantes.FICHEROS_INTERNACIONAL_ESTADO, fechaEjecucion);

    processInterfaces.add(generacionFicheroMinoristas);

    // Generamos el fichero de Institucionales
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGeneracionFicherosEjecucion generacionFicheroInstitucionales = ctx
        .getBean(CallableGeneracionFicherosEjecucion.class);

    generacionFicheroInstitucionales.setTipoFichero(TipoFicheroEjecuciones.INSTITUCIONAL);
    generacionFicheroInstitucionales.getGenerateOIInstitucionalesFile().configureName();
    generacionFicheroInstitucionales.getGenerateOIInstitucionalesFile().setOutPutDir(outPutDir);
    generacionFicheroInstitucionales.getGenerateOIInstitucionalesFile().setDbPageSize(dbPageSize);
    generacionFicheroInstitucionales.getGenerateOIInstitucionalesFile().setGenerateIfEmpty(generateIfEmpty);
    generacionFicheroInstitucionales.getGenerateOIInstitucionalesFile()
        .setInstitucionalParams(DwhConstantes.FICHEROS_INTERNACIONAL_ESTADO, fechaEjecucion);

    processInterfaces.add(generacionFicheroInstitucionales);

    return launchThreadBatch(processInterfaces);
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   * @throws ExecutionException
   */
  private List<ProcessFileResultDTO> launchThreadBatch(List<CallableProcessFileResult> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final List<ProcessFileResultDTO> listProcessResultDTO = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(4, new NamedThreadFactory("batch_Ejecuciones"));

    final List<CallableProcessFileResult> futureList = new ArrayList<>();

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessFileResult callableBo : processInterfaces) {
      callableBo.setFuture(executor.submit(callableBo));
      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    final boolean onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Si se ha superado el tiempo estipulado para la ejecucion de los
    // ficheros cancelamos los hilos
    if (onTime) {
      for (CallableProcessFileResult processInterface : futureList) {
        listProcessResultDTO.add(processInterface.getFuture().get());
      }
    }
    else {
      for (CallableProcessFileResult future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }

    return listProcessResultDTO;
  }

  public void setFechaEjecucion(Date fechaEjecucion) {
    if (fechaEjecucion != null) {
      this.fechaEjecucion = fechaEjecucion;
    }
    else {
      this.fechaEjecucion = new Date(System.currentTimeMillis());
    }
  }

  @Override
  public List<ProcessFileResultDTO> getProcessFileResultsList() {
    return processFileResultsList;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_FICHERO_NACIONAL;
  }

}
