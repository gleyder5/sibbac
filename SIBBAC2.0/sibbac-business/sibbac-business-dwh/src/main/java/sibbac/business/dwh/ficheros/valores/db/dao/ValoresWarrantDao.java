package sibbac.business.dwh.ficheros.valores.db.dao;

import java.math.BigInteger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.ValoresWarrant;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ValoresWarrantDao extends JpaRepository<ValoresWarrant, BigInteger> {

  @Query(value = "SELECT COUNT(*) FROM TMCT0_VALORES_WARRANT " + "WHERE CDEMISIO = :cdemisio", nativeQuery = true)
  public Integer countWarrant(@Param("cdemisio") String cdemisio);
}
