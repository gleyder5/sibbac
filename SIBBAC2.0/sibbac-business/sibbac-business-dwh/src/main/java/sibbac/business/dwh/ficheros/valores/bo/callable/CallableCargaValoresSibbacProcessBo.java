package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableCargaValoresSibbacProcessBo extends AbstractCallableProcessBo {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractCallableProcessBo.class);

  /** Tamaño de la pagina */
  @Value("${dwh.carga.sibbac.read.batch:1000}")
  private int pageSize;

  /** Número de Hilos de ejecucion */
  @Value("${dwh.pool.carga.sibbac.update.pool.number:5}")
  private int threads;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${sibbac.batch.timeout: 90}")
  private int timeout;

  @Autowired
  private ApplicationContext ctx;

  private Integer idTipoProducto;

  private List<String> codigoValor;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    Boolean result = false;

    try {
      executeProcessCargaValoresSibbac();
    }
    catch (SIBBACBusinessException e) {
      LOG.error("Se ha producido un error: ", e);
      result = false;
    }

    return result;
  }

  /**
   * 
   * @throws SIBBACBusinessException
   */
  public void executeProcessCargaValoresSibbac() throws SIBBACBusinessException {
    try {
      final List<CallableCargaValoresSibbacLoaderBo> processInterfaces = new ArrayList<>();

      final Integer numPaginas = codigoValor.size() / pageSize;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * pageSize;
        Integer toIndex = ((pageNum + 1) * pageSize);

        if (toIndex > codigoValor.size() || fromIndex > codigoValor.size()) {
          toIndex = codigoValor.size();
        }

        final List<String> subList = codigoValor.subList(fromIndex, toIndex);

        // Cargamos el BO que contiene la lógica de validacion.
        final CallableCargaValoresSibbacLoaderBo callableBo = ctx.getBean(CallableCargaValoresSibbacLoaderBo.class);
        callableBo.setIdTipoProducto(idTipoProducto);
        callableBo.setCodigoValor(subList);

        processInterfaces.add(callableBo);
      }

      // Lanzamos los hilos
      launchProcess(processInterfaces);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para CargaValoresSibbac", e);
    }
  }

  /**
   * 
   */
  public void launchProcess(List<CallableCargaValoresSibbacLoaderBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {
    try {
      final Integer numPaginas = processInterfaces.size() / threads;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threads;
        Integer toIndex = ((pageNum + 1) * threads);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableCargaValoresSibbacLoaderBo> subList = processInterfaces.subList(fromIndex, toIndex);

        launchThreadBatch(subList);
      }
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para CargaValoresSibbac", e);

    }
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatch(List<CallableCargaValoresSibbacLoaderBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threads,
        new NamedThreadFactory("batchCargaValoresSibbac"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  public Integer getIdTipoProducto() {
    return idTipoProducto;
  }

  public void setIdTipoProducto(Integer idTipoProducto) {
    this.idTipoProducto = idTipoProducto;
  }

  public List<String> getCodigoValor() {
    return codigoValor;
  }

  public void setCodigoValor(List<String> codigoValor) {
    this.codigoValor = codigoValor;
  }

}
