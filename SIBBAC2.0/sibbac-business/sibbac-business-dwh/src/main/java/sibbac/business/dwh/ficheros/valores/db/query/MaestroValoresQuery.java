package sibbac.business.dwh.ficheros.valores.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class MaestroValoresQuery.
 */
@Component("MaestroValoresQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MaestroValoresQuery extends AbstractDynamicPageQuery {

  /** The Constant CDISINVV. */
  private static final String CODIGOISIN = "CDISINVV";

  /** The Constant NBTITREV. */
  private static final String DESCRIPCION = "NBTITREV";

  /** The Constant FHINICIV. */
  private static final String FECHAEMISION = "FHINICIV";

  /** The Constant FHBAJACV. */
  private static final String FECHABAJA = "FHBAJACV";

  /** The Constant CDCVABEV. */
  private static final String CODBANCOESP = "CDCVABEV";

  /** The Constant CDGRSIBV. */
  private static final String GRUPSIBE = "CDGRSIBV";

  /** The Constant NBTIT10C. */
  private static final String CODSIBE = "NBTIT10C";

  /** The Constant CDINSTRU. */
  private static final String TIPOSINSTRU = "CDINSTRU";

  /** The Constant CDCOTMAV. */
  private static final String COTIZMAD = "CDCOTMAV";

  /** The Constant CDCOTBAV. */
  private static final String COTIZBAR = "CDCOTBAV";

  /** The Constant CDCOTBIV. */
  private static final String COTIZBIL = "CDCOTBIV";

  /** The Constant CDCOTVAV. */
  private static final String COTIZVAL = "CDCOTVAV";

  /** The Constant CDCOTVRV. */
  private static final String COTIZOTRA = "CDCOTVRV";

  /** The Constant CDCOTEXV. */
  private static final String COTIZEXT = "CDCOTEXV";

  /** The Constant CDCIFSOC. */
  private static final String CIFEMISOR = "CDCIFSOC";

  /** The Constant CDNOMINV. */
  private static final String NOMINAL = "NUNOMINV";

  /** The Constant CDMONEDV. */
  private static final String ISOMONEDA = "CDMONEDV";

  /** The Constant CDPAISEV. */
  private static final String PAISEMISOR = "CDPAISEV";

  /** The Constant CDDIVISV. */
  private static final String DIVISA = "CDDIVISV";

  /** The Constant NUDELOTE. */
  private static final String LOTELATIVEX = "NUDELOTE";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT " + "CDISINVV," + "NBTITREV," + "FHINICIV," + "FHBAJACV," + "CDCVABEV,"
      + "CDGRSIBV," + "NBTIT10C," + "CDINSTRU," + "CDCOTMAV," + "CDCOTBAV," + "CDCOTBIV," + "CDCOTVAV," + "CDCOTVRV,"
      + "CDCOTEXV," + "CDCIFSOC," + "NUNOMINV," + "CDMONEDV," + "CDPAISEV," + "CDDIVISV," + "NUDELOTE ";

  /** The Constant FROM. */
  private static final String FROM = " FROM TMCT0_MAESTRO_VALORES ";

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE FHBAJACV = DATE('31/12/9999')";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("Código ISIN", CODIGOISIN),
      new DynamicColumn("Descripción", DESCRIPCION),
      new DynamicColumn("Fecha emisión", FECHAEMISION, DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Fecha baja", FECHABAJA, DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Código banco España", CODBANCOESP), new DynamicColumn("Grupo SIBE", GRUPSIBE),
      new DynamicColumn("Código SIBE", CODSIBE), new DynamicColumn("Tipo Instrumento", TIPOSINSTRU),
      new DynamicColumn("Cotiza Madrid", COTIZMAD), new DynamicColumn("Cotiza Barcelona", COTIZBAR),
      new DynamicColumn("Cotiza Bilbao", COTIZBIL), new DynamicColumn("Cotiza Valencia", COTIZVAL),
      new DynamicColumn("Cotiza Otras", COTIZOTRA), new DynamicColumn("Cotiza Extrangero", COTIZEXT),
      new DynamicColumn("CIF Emisor", CIFEMISOR), new DynamicColumn("Nominal", NOMINAL),
      new DynamicColumn("ISO Moneda", ISOMONEDA), new DynamicColumn("Pais Emisor", PAISEMISOR),
      new DynamicColumn("Divisa", DIVISA), new DynamicColumn("Lote LATIVEX", LOTELATIVEX) };

  /**
   * Make filters.
   *
   * @return the list
   */
  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter(CODIGOISIN, "Código de ISIN"));
    res.add(new StringDynamicFilter(DESCRIPCION, "Nombre de ISIN"));
    res.add(new DateDynamicFilter(FECHAEMISION, "Fecha emisión"));
    return res;
  }

  /**
   * Instantiates a new maestro valores query.
   */
  public MaestroValoresQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  /**
   * Instantiates a new maestro valores query.
   *
   * @param columns the columns
   */
  public MaestroValoresQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  /**
   * Instantiates a new maestro valores query.
   *
   * @param filters the filters
   * @param columns the columns
   */
  public MaestroValoresQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}
