package sibbac.business.dwh.ficheros.valores.bo;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableCargaValoresSibbacProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableInsertaValoresSibbacProcessBo;
import sibbac.business.dwh.ficheros.valores.db.dao.CargaValoresDao;
import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.database.bo.AbstractBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CargaValoresBo extends AbstractBo<MaestroValores, BigInteger, CargaValoresDao> {

  private static final Logger LOG = LoggerFactory.getLogger(CargaValoresBo.class);

  public static final String CARGA_VALORES_SIBBAC_INSERTS = "INSERTS";

  public static final String CARGA_VALORES_SIBBAC_UPDATES = "UPDATES";

  /** Tamaño de la página para. */
  @Value("${fichero.valores.db.page.size:20}")
  private int dbPageSize;

  /** Ruta donde se deposita el fichero. */
  @Value("${fileName.dwh.temp.dir:/sbrmv/ficheros/temp}")
  private String outPutDir;

  /** Nombre del fichero generado por el proceso. */
  @Value("${fileName.dwh.temp:VALORES_ERROR.txt}")
  private String fileNameValoresError;

  /** Número de Hilos de ejecucion */
  @Value("${fileName.dwh.partenon.threads:5}")
  private int threads;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${sibbac.batch.timeout: 90}")
  private int timeout;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private PartenonBo partenonBo;

  @Autowired
  private CargaValoresDao cargaValoresDao;

  @Autowired
  private GenerateValoresFile generateValoresFile;

  private List<String> rowList;

  private Future<String> future;

  private Map<String, String> cdpercuvMap;

  /**
   * Carga las tablas intermedias
   * @throws SIBBACBusinessException
   */
  public void cargaValoresTablasIntermedias() throws SIBBACBusinessException {
    partenonBo.loadTemporaryTables();
  }

  /**
   * Carga los valores desde partenon
   * @throws SIBBACBusinessException
   */
  public void cargaValoresPartenon() throws SIBBACBusinessException {
    partenonBo.cargaValoresPartenon();
  }

  /**
   * Carga los valores desde partenon
   * @throws SIBBACBusinessException
   */
  public void muevePartenonFile() throws SIBBACBusinessException {
    partenonBo.movePartenonFile();
  }

  /**
   * Carga los valores desde SIBBAC
   * @throws SIBBACBusinessException
   */
  public void cargaValoresSibbac() throws SIBBACBusinessException {
    // 1. Leemos el fichero de Partenon
    if (LOG.isInfoEnabled()) {
      LOG.info("Iniciamos la validacion/carga de MAESTRO_VALORES desde SIBBAC");
    }

    long startTimeProcess = System.currentTimeMillis();
    long startTimeProcessFile = startTimeProcess;

    // 1. Separamos los valores a insertar de los de actualizar
    if (LOG.isDebugEnabled()) {
      LOG.debug("Iniciamos la Separacion de valores");
    }

    startTimeProcessFile = System.currentTimeMillis();

    final Map<String, List<Object[]>> mapsList = divideValoresAltaActualizacion();

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("Finalizada la Separacion de valores en %d ms",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }

    // 2. inicializamos los mapas para el mapeo
    initialiceMaps();

    // 3. obtenemos aquellos valores nuevos a insertar y los guardamos
    if (LOG.isDebugEnabled()) {
      LOG.debug("Iniciamos la inserción de Maestro Valores desde SIBBAC");
    }

    startTimeProcessFile = System.currentTimeMillis();

    insertarMaestroValoresFromValores(mapsList.get(CargaValoresBo.CARGA_VALORES_SIBBAC_INSERTS));

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("Finalizada la inserción de Maestro Valores desde SIBBAC en %d ms",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }

    // 4. Obtenemos los que ya existían en la tabla y debemos actualizar
    if (LOG.isDebugEnabled()) {
      LOG.debug("Iniciamos la actualizacion de Maestro Valores desde SIBBAC");
    }

    startTimeProcessFile = System.currentTimeMillis();

    actualizarMaestroValoresFromValores(mapsList.get(CargaValoresBo.CARGA_VALORES_SIBBAC_UPDATES));

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("Finalizada la actualizacion de Maestro Valores desde SIBBAC en %d ms",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }

    // liberamos la memoria de la cache
    destroyMaps();

    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("Finalizada la validacion/carga de MAESTRO_VALORES desde SIBBAC, %d ms",
          (System.currentTimeMillis() - startTimeProcess)));
    }
  }

  /**
   * Inicializa la Cache para CDPERCUV
   */
  private void initialiceCdpercuvMap() {
    this.cdpercuvMap = new HashMap<String, String>();
    this.cdpercuvMap.put("0", "P");
    this.cdpercuvMap.put("1", "D");
    this.cdpercuvMap.put("3", "B");
    this.cdpercuvMap.put("4", "C");
  }

  /**
   * Inicializa los mapas para acceder rápidamente a la info
   */
  public void initialiceMaps() {
    initialiceCdpercuvMap();
  }

  /**
   * Destruye los mapas para liberar memoria
   */
  public void destroyMaps() {
    this.cdpercuvMap = null;
  }

  // Constantes usadas
  static final String NBTIT10C = "          ";
  static final String CDCVSIBV = "99999999";
  static final String USUALTA = "TaskCargaValores-SIBBAC";

  // limpiar tablas
  public void cleanTables() throws SIBBACBusinessException {
    try {
      cargaValoresDao.cleanEmisor();
      LOG.info("Se ha borrado el contenido de la tablas DWH_VALORES_EMISOR");

      cargaValoresDao.cleanMercado();
      LOG.info("Se ha borrado el contenido de la tablas DWH_VALORES_MERCADO");

      cargaValoresDao.cleanWarrant();
      LOG.info("Se ha borrado el contenido de la tablas DWH_VALORES_WARRANT");

      cargaValoresDao.cleanEmision();
      LOG.info("Se ha borrado el contenido de la tablas DWH_VALORES_EMISION");
    }
    catch (Exception e) {
      LOG.error("Error al limpiar tablas", e);
      throw new SIBBACBusinessException("Error al limpiar tablas", e);
    }
  }

  public List<String> getValidationValoresList() {
    List<Object[]> listIsin = cargaValoresDao.getYesterdayIsin();
    List<String> aux = new ArrayList<>();

    for (int i = 0; i < listIsin.size(); i++) {
      String isin = String.valueOf(listIsin.get(i));
      if (cargaValoresDao.getMasterIsin(isin) == 0) {
        aux.add(isin);
      }
    }
    return aux;
  }

  public File validacionValores() throws SIBBACBusinessException {
    File res = null;

    final List<String> aux = getValidationValoresList();

    if (!aux.isEmpty()) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      String format = sdf.format(new Date()) + "_";
      String fileName = format + fileNameValoresError;
      try {
        generateValoresFile.loadValidationValores(aux);
        generateValoresFile.setDbPageSize(dbPageSize);
        generateValoresFile.generateFile(outPutDir, fileName);
        res = new File(outPutDir + "/" + fileName);
      }
      catch (SIBBACBusinessException e) {
        LOG.error(e.getMessage(), e);
        throw new SIBBACBusinessException(
            MessageFormat.format(ConstantesError.ERROR_GENERACION_FICHERO.getValue(), fileName));
      }
    }
    return res;
  }

  public List<String> getRowList() {
    return rowList;
  }

  public void setRowList(List<String> rowList) {
    this.rowList = rowList;
  }

  /**
   * Separamos los valores para el alta y la modificación
   */
  private Map<String, List<Object[]>> divideValoresAltaActualizacion() {
    final Map<String, List<Object[]>> valoresProcess = new HashMap<>();

    final List<Object[]> listInsertAllValoresProcess = new ArrayList<>();
    final List<Object[]> listUpdateAllValoresProcess = new ArrayList<>();

    long startTimeProcessFile;

    startTimeProcessFile = System.currentTimeMillis();

    if (LOG.isDebugEnabled()) {
      LOG.debug("Iniciamos la obtención de valores desde TMCT0_VALORES");
    }

    final List<Object[]> valores = cargaValoresDao.getValoresByInsertOrUpdate();

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("Obtenidos %d elementos de TMCT0_VALORES en %d ms", valores.size(),
          (System.currentTimeMillis() - startTimeProcessFile)));
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug("Iniciamos la separación de valores para el Alta y Modificacion");
    }

    startTimeProcessFile = System.currentTimeMillis();

    Integer esModificacion;
    String activo;

    for (Object[] valor : valores) {
      esModificacion = (Integer) valor[5];
      activo = (String) valor[3];

      // Si modificacion es 1 actualizamos si no insertamos
      if (esModificacion == 1) {
        if (activo.equals("S")) {
          listUpdateAllValoresProcess.add(valor);
        }
        else {
          if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("EL registro %s esta inactivo en TMCT0_VALORES, no se hace nada con el",
                (String) valor[0]));
          }
        }
      }
      else {
        listInsertAllValoresProcess.add(valor);
      }
    }

    valoresProcess.put(CARGA_VALORES_SIBBAC_INSERTS, listInsertAllValoresProcess);

    valoresProcess.put(CARGA_VALORES_SIBBAC_UPDATES, listUpdateAllValoresProcess);

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("Separación de valores finalizada en %d ms, Alta %d elementos, Modificación %d elementos",
          (System.currentTimeMillis() - startTimeProcessFile), listInsertAllValoresProcess.size(),
          listUpdateAllValoresProcess.size()));
    }

    return valoresProcess;
  }

  /**
   * Insertamos en MaestroValores
   * @throws SIBBACBusinessException
   */
  private void insertarMaestroValoresFromValores(List<Object[]> listSaveAllValoresProcess)
      throws SIBBACBusinessException {

    final List<MaestroValores> listSaveAllValores = toMaestroValoresList(listSaveAllValoresProcess);

    // Si la lista no esta vacía lanzamos los hilos
    if (!listSaveAllValores.isEmpty()) {
      final List<CallableInsertaValoresSibbacProcessBo> processInterfaces = new ArrayList<>();

      // Lanzar los hilos
      final CallableInsertaValoresSibbacProcessBo callableBo = ctx.getBean(CallableInsertaValoresSibbacProcessBo.class);

      callableBo.setMaestroValores(listSaveAllValores);
      processInterfaces.add(callableBo);

      // Lanzanmos los hilos por tandas
      try {
        launchProcessInsertaValores(processInterfaces);
      }
      catch (InterruptedException e) {
        throw new SIBBACBusinessException(
            "Se ha producido un error durante la ejecución de hilos para la carga de registros", e);
      }
    }
  }

  /**
   * Lanzamos los procesos para la Inserción en Maestro Valores
   * @param processInterfaces
   * @throws InterruptedException
   * @throws SIBBACBusinessException
   */
  private void launchProcessInsertaValores(List<CallableInsertaValoresSibbacProcessBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {
    try {
      final Integer numPaginas = processInterfaces.size() / threads;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threads;
        Integer toIndex = ((pageNum + 1) * threads);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableInsertaValoresSibbacProcessBo> subList = processInterfaces.subList(fromIndex, toIndex);

        launchThreadBatchInsertaValores(subList);
      }
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para la inserción de registros", e);

    }
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatchInsertaValores(List<CallableInsertaValoresSibbacProcessBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threads,
        new NamedThreadFactory("batchValoresSibbac"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableInsertaValoresSibbacProcessBo callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  /**
   * Actualiza los valores en maestro valores
   */
  private void actualizarMaestroValoresFromValores(List<Object[]> listUpdateAllValoresProcess)
      throws SIBBACBusinessException {
    final Map<Integer, List<String>> tipoProductCodigoValores = new HashMap<>();

    for (Object[] valor : listUpdateAllValoresProcess) {
      final Integer idTipoProducto = (Integer) valor[4];
      final String codigoValor = (String) valor[0];

      if (!tipoProductCodigoValores.containsKey(idTipoProducto)) {
        tipoProductCodigoValores.put(idTipoProducto, new ArrayList<String>());
      }
      tipoProductCodigoValores.get(idTipoProducto).add(codigoValor);
    }

    final List<CallableCargaValoresSibbacProcessBo> processInterfaces = new ArrayList<>();

    final Set<Entry<Integer, List<String>>> entrySet = tipoProductCodigoValores.entrySet();
    for (Entry<Integer, List<String>> entry : entrySet) {
      final Integer idTipoProducto = entry.getKey();
      final List<String> codigoValor = entry.getValue();

      // Lanzar los hilos
      final CallableCargaValoresSibbacProcessBo callableBo = ctx.getBean(CallableCargaValoresSibbacProcessBo.class);

      callableBo.setCodigoValor(codigoValor);
      callableBo.setIdTipoProducto(idTipoProducto);

      processInterfaces.add(callableBo);
    }

    // Lanzanmos los hilos por tandas
    try {
      launchProcess(processInterfaces);
    }
    catch (InterruptedException | SIBBACBusinessException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para la carga de registros", e);
    }
  }

  /**
   * 
   */
  public void launchProcess(List<CallableCargaValoresSibbacProcessBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {
    try {
      final Integer numPaginas = processInterfaces.size() / threads;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threads;
        Integer toIndex = ((pageNum + 1) * threads);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableCargaValoresSibbacProcessBo> subList = processInterfaces.subList(fromIndex, toIndex);

        launchThreadBatch(subList);
      }
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para la carga de registros", e);

    }
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatch(List<CallableCargaValoresSibbacProcessBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threads,
        new NamedThreadFactory("batchValoresSibbac"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableCargaValoresSibbacProcessBo callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  private MaestroValores rellenaMaestroSibbac(String codigoValor, String divisa, String descripcion,
      Integer idTipoProducto) {

    MaestroValores entity = new MaestroValores();
    // CDCLAVAL a cero por constructor
    entity.setFhbajacv(FormatDataUtils.convertStringToDate("9999-12-31", FormatDataUtils.DATE_FORMAT_1));
    entity.setFhiniciv(new Date());
    entity.setNbtitrev(descripcion);
    entity.setNunominv(new BigDecimal(0));
    entity.setCdisinvv(codigoValor);
    entity.setCdcvabev(codigoValor.substring(3, 11));
    entity.setCdmonedv(divisa);
    entity.setCdpercuv(getCdpercuv(codigoValor));
    entity.setNbtit10c(NBTIT10C);
    entity.setCdcvsibv(CDCVSIBV);
    entity.setCdpaisev(codigoValor.substring(0, 2));
    entity.setFhaltav(new Date());
    entity.setUsualta(USUALTA);
    entity.setCdinstru(String.valueOf(idTipoProducto));
    entity.setCdvaibex("N");

    return entity;

  }

  public String getCdpercuv(String codigoValor) {
    String cdPercuv = "";
    if (codigoValor.substring(0, 2).equals("ES") && this.cdpercuvMap.containsKey(codigoValor.substring(4, 5))) {
      cdPercuv = this.cdpercuvMap.get(codigoValor.substring(3, 4));
    }
    return cdPercuv;
  }

  /**
   * Transforma la lista de Objectos a una lista de maestro valores
   * @param listSaveAllValoresProcess
   * @return
   */
  private List<MaestroValores> toMaestroValoresList(List<Object[]> listSaveAllValoresProcess) {
    final List<MaestroValores> listSaveAllValores = new ArrayList<>();

    // Formamos la lista de Entidades
    for (Object[] valor : listSaveAllValoresProcess) {
      String codigoValor = (String) valor[0];
      String divisa = (String) valor[1];
      String descripcion = (String) valor[2];
      Integer idTipoProducto = (Integer) valor[4];

      listSaveAllValores.add(rellenaMaestroSibbac(codigoValor, divisa, descripcion, idTipoProducto));
    }

    return listSaveAllValores;
  }

  public Future<String> getFuture() {
    return future;
  }

  public void setFuture(Future<String> future) {
    this.future = future;
  }
}
