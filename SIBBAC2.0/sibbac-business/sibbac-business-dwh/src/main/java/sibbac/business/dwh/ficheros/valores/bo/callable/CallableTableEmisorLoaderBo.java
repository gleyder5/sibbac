package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresEmisorBo;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresEmisor;
import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableTableEmisorLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private ValoresEmisorBo valoresEmisorBo;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    valoresEmisorBo.saveBulkValoresEmisor(toValoresEmisorEntity());
    return true;
  }

  /**
   * Convierte una lista de String a una lista de Entidades
   * @param emisionStringList
   * @return
   */
  private List<ValoresEmisor> toValoresEmisorEntity() {
    final List<ValoresEmisor> entities = new ArrayList<>();

    for (PartenonFileLineDTO element : listData) {
      entities.add(ValoresEmisor.createValoresEmisor(element));
    }
    return entities;
  }
}
