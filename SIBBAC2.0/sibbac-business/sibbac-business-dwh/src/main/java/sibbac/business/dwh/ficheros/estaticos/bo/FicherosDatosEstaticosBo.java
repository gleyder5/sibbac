package sibbac.business.dwh.ficheros.estaticos.bo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.estaticos.db.dao.BolsaDwhDao;
import sibbac.business.dwh.ficheros.estaticos.db.dao.BrokerDwhDao;
import sibbac.business.dwh.ficheros.estaticos.db.dao.EntidadesDwhDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.constants.FileResult;

@Component
public class FicherosDatosEstaticosBo {

  protected static final Logger LOG = LoggerFactory.getLogger(FicherosDatosEstaticosBo.class);

  private final static Charset ENCODING = StandardCharsets.UTF_8;

  private final static int BROKER_ID = 0;
  private final static int BROKER_NOMBRE = 1;
  private final static int BROKER_TIPO = 2;

  private final static int BOLSA_ID = 0;
  private final static int BOLSA_NOMBRE = 1;
  private final static int BOLSA_CODIGO_PAIS = 2;

  private final static int ENTIDAD_ID = 0;
  private final static int ENTIDAD_NOMBRE = 1;

  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String filePath;

  @Value("${dwh.ficheros.estaticos.brokers.file:fdwh0brk.txt}")
  private String fileNameDwhBrokers;

  @Value("${dwh.ficheros.estaticos.bolsas.file:fdwh0bls.txt}")
  private String fileNameDwhBolsas;

  @Value("${dwh.ficheros.estaticos.entidades.file:fdwh0eli.txt}")
  private String fileNameDwhEntidades;

  @Value("${dwh.ficheros.estaticos.generated_if_empty:true}")
  private Boolean generateIfEmpty;

  @Autowired
  private BrokerDwhDao brokersDao;

  @Autowired
  private BolsaDwhDao bolsasDao;

  @Autowired
  private EntidadesDwhDao entidadesDao;

  /**
   * Genera el fichero de Brokers
   * @throws SIBBACBusinessException
   */
  public FileResult generateFileBrokers() throws SIBBACBusinessException {
    FileResult fileResult = null;

    LOG.info("[GenerarFicherosDatosEstaticos :: executeTask] inicio generación fichero de brokers");

    GenerateFilesUtilDwh.createOutPutDir(filePath);

    final StringBuilder fileName = new StringBuilder();
    fileName.append(filePath).append(File.separator).append(fileNameDwhBrokers);
    Path path = Paths.get(FilenameUtils.getFullPath(fileName.toString()), FilenameUtils.getName(fileName.toString()));

    List<Object[]> brokers = brokersDao.findActiveBrokers();
    if (!brokers.isEmpty()) {
      try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
        for (Object[] broker : brokers) {
          String line = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), getLineBroker(broker));
          writer.append(line);
          writer.newLine();
        }

        fileResult = FileResult.GENETARATED_OK;
      }
      catch (IOException | SIBBACBusinessException e) {
        fileResult = FileResult.NOT_GENERATED;
        LOG.error("Error al generar el fichero de Brokers de DWH", e.getMessage(), e);
        throw new SIBBACBusinessException("Error al generar el fichero de Brokers de DWH", e);
      }
    }
    else {
      fileResult = FileResult.GENETARATED_EMPTY;
      LOG.info("No se han encontrado brokers activos");
    }
    LOG.info("[GenerarFicherosDatosEstaticos :: executeTask] fin generación fichero de brokers");

    return fileResult;
  }

  /**
   * Genera del fichero de Bolsas
   * @throws SIBBACBusinessException
   */
  public FileResult generateFileBolsas() throws SIBBACBusinessException {
    FileResult fileResult = null;

    LOG.info("[GenerarFicherosDatosEstaticos :: executeTask] inicio generación fichero de bolsas");

    GenerateFilesUtilDwh.createOutPutDir(filePath);

    final StringBuilder fileName = new StringBuilder();
    fileName.append(filePath).append(File.separator).append(fileNameDwhBolsas);
    Path path = Paths.get(FilenameUtils.getFullPath(fileName.toString()), FilenameUtils.getName(fileName.toString()));

    List<Object[]> bolsas = bolsasDao.findActiveBolsas();
    if (!bolsas.isEmpty()) {
      try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
        for (Object[] bolsa : bolsas) {
          String line = getLineBolsa(bolsa);
          writer.append(line);
          writer.newLine();
        }
        fileResult = FileResult.GENETARATED_OK;
      }
      catch (IOException | SIBBACBusinessException e) {
        fileResult = FileResult.NOT_GENERATED;
        LOG.error("Error al generar el fichero de Bolsas de DWH", e.getMessage(), e);
        throw new SIBBACBusinessException("Error al generar el fichero de Bolsas de DWH", e);
      }
    }
    else {
      fileResult = FileResult.GENETARATED_EMPTY;
      LOG.info("No se han encontrado bolsas activas");
    }
    LOG.info("[GenerarFicherosDatosEstaticos :: executeTask] fin generación fichero de bolsas");

    return fileResult;
  }

  /**
   * Genera el fichero de Entidades
   * @throws SIBBACBusinessException
   */
  public FileResult generateFileEntidades() throws SIBBACBusinessException {
    FileResult fileResult = null;

    LOG.info("[GenerarFicherosDatosEstaticos :: executeTask] inicio generación fichero de entidades");

    GenerateFilesUtilDwh.createOutPutDir(filePath);

    final StringBuilder fileName = new StringBuilder();
    fileName.append(filePath).append(File.separator).append(fileNameDwhEntidades);
    Path path = Paths.get(FilenameUtils.getFullPath(fileName.toString()), FilenameUtils.getName(fileName.toString()));

    List<Object[]> entidades = entidadesDao.findActiveEntidades();
    if (!entidades.isEmpty()) {
      try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
        for (Object[] entidad : entidades) {
          String line = getLineEntidad(entidad);
          writer.append(line);
          writer.newLine();
        }
        fileResult = FileResult.GENETARATED_OK;
      }
      catch (IOException | SIBBACBusinessException e) {
        fileResult = FileResult.NOT_GENERATED;
        LOG.error("Error al generar el fichero de Entidades liquidadoras de DWH", e.getMessage(), e);
        throw new SIBBACBusinessException("Error al generar el fichero de Entidades liquidadoras de DWH", e);
      }
    }
    else {
      fileResult = FileResult.GENETARATED_EMPTY;
      LOG.error("No se han encontrado entidades liquidadoras activas");
    }
    LOG.info("[GenerarFicherosDatosEstaticos :: executeTask] fin generación fichero de entidades");

    return fileResult;
  }

  private String getLineBroker(Object[] broker) throws SIBBACBusinessException {
    StringBuilder lineBroker = new StringBuilder();
    try {

      String nameBroker = (String) broker[BROKER_NOMBRE];
      nameBroker = nameBroker.replace("\n", "");
      lineBroker.append(GenerateFilesUtilDwh.formatString((String) broker[BROKER_ID], 8));
      lineBroker.append(GenerateFilesUtilDwh.formatString(nameBroker.trim(), 50));
      lineBroker.append((String) broker[BROKER_TIPO]);

    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error al formatear broker", e);
    }

    return lineBroker.toString();
  }

  private String getLineBolsa(Object[] bolsa) throws SIBBACBusinessException {
    StringBuilder lineBolsa = new StringBuilder();
    String cadena = "";
    try {

      String nameBolsa = (String) bolsa[BOLSA_NOMBRE];
      nameBolsa = nameBolsa.replace("\n", "");
      lineBolsa.append(GenerateFilesUtilDwh.formatString((String) bolsa[BOLSA_ID], 3));
      lineBolsa.append(GenerateFilesUtilDwh.formatString(nameBolsa.trim(), 16));
      lineBolsa.append(GenerateFilesUtilDwh.formatString((String) bolsa[BOLSA_CODIGO_PAIS], 3));

      cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), lineBolsa);

    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error al formatear bolsa", e);
    }

    return cadena.toString();
  }

  private String getLineEntidad(Object[] entidad) throws SIBBACBusinessException {
    StringBuilder lineEntidad = new StringBuilder();
    String cadena = "";
    try {

      String nameEntidad = (String) entidad[ENTIDAD_NOMBRE];
      nameEntidad = nameEntidad.replace("\n", "");
      lineEntidad.append(GenerateFilesUtilDwh.formatString((String) entidad[ENTIDAD_ID], 4));
      lineEntidad.append(GenerateFilesUtilDwh.formatString(nameEntidad.trim(), 40));

      cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), lineEntidad);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error al formatear bolsa", e);
    }
    return cadena.toString();
  }

  public String getFileNameDwhBrokers() {
    return fileNameDwhBrokers;
  }

  public String getFileNameDwhBolsas() {
    return fileNameDwhBolsas;
  }

  public String getFileNameDwhEntidades() {
    return fileNameDwhEntidades;
  }

}
