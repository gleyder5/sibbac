package sibbac.business.dwh.ficheros.valores.db.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "TMCT0_DWH_VALORES_OPERADO")
@Entity
public class ValoresOperados implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /** The id. */
  @Id
  @Column(name = "CDISINVV", length = 12, nullable = false)
  private String cdisinvv;

  @Column(name = "FHINICIV", length = 10)
  private Date fhiniciv;

  @Column(name = "FHBAJACV", length = 10)
  private Date fhbajacv;

  @Column(name = "NBTITREV", length = 31)
  private String nbtitrev;

  @Column(name = "CDCIFSOC", length = 9)
  private String cdcifsoc;

  @Column(name = "CDISINGR", length = 12)
  private String cdisingr;

  @Column(name = "NBTITGRU", length = 31)
  private String nbtitgru;

  @Column(name = "FHAUDIT", length = 26, nullable = false)
  private Timestamp fhaudit;

  @Column(name = "USUAUDIT", length = 100, nullable = false)
  private String usuaudit;

  public String getCdisinvv() {
    return cdisinvv;
  }

  public void setCdisinvv(String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  public Date getFhiniciv() {
    return fhiniciv != null ? (Date) fhiniciv.clone() : null;
  }

  public void setFhiniciv(Date fhiniciv) {
    this.fhiniciv = (fhiniciv != null ? (Date) fhiniciv.clone() : null);
  }

  public Date getFhbajacv() {
    return fhbajacv != null ? (Date) fhbajacv.clone() : null;
  }

  public void setFhbajacv(Date fhbajacv) {
    this.fhbajacv = (fhbajacv != null ? (Date) fhbajacv.clone() : null);
  }

  public String getNbtitrev() {
    return nbtitrev;
  }

  public void setNbtitrev(String nbtitrev) {
    this.nbtitrev = nbtitrev;
  }

  public String getCdcifsoc() {
    return cdcifsoc;
  }

  public void setCdcifsoc(String cdcifsoc) {
    this.cdcifsoc = cdcifsoc;
  }

  public String getCdisingr() {
    return cdisingr;
  }

  public void setCdisingr(String cdisingr) {
    this.cdisingr = cdisingr;
  }

  public String getNbtitgru() {
    return nbtitgru;
  }

  public void setNbtitgru(String nbtitgru) {
    this.nbtitgru = nbtitgru;
  }

  public Timestamp getFhaudit() {
    return fhaudit != null ? (Timestamp) fhaudit.clone() : null;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = (fhaudit != null ? (Timestamp) fhaudit.clone() : null);
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public String toString() {
    return "[ " + this.getCdisinvv() + ", " + this.getFhiniciv() + ", " + this.getFhbajacv() + ", " + this.getNbtitrev()
        + ", " + this.getCdcifsoc() + ", " + this.getCdisingr() + ", " + this.getNbtitgru() + ", " + this.getFhaudit()
        + ", " + this.getUsuaudit() + " ]";
  }

}
