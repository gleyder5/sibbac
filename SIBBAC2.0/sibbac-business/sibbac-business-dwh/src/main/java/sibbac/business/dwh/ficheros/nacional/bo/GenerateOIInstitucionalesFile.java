package sibbac.business.dwh.ficheros.nacional.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableNacionalInstitucionalesLineThreadsBo;
import sibbac.business.dwh.ficheros.nacional.db.dao.OIInstitucionalesDao;
import sibbac.business.dwh.ficheros.nacional.dto.InstitucionalDTO;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.business.dwh.utils.OperacionesInternacionalDWHUtils;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0estBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0est;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.AbstractGenerateFile;
import sibbac.common.utils.SibbacEnums;

@Component
public class GenerateOIInstitucionalesFile extends AbstractGenerateFile {

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  // @Value("${sibbac.batch.timeout: 60}")
  private int timeout = 60;

  @Autowired
  private ApplicationContext ctx;

  private static final Logger LOG = LoggerFactory.getLogger(GenerateOIInstitucionalesFile.class);

  private static final String INSTITUCIONALES_LINE_SEPARATOR = "\r\n";

  private static final String INSTITUCIONALES_FILE_HEADER = "";

  private static final String INSTITUCIONALES_FILE_FOOTER = "";

  public String estado = DwhConstantes.FICHEROS_INTERNACIONAL_ESTADO;

  public Date tradeini;

  public Date tradefin;

  /**
   * Nombre del fichero de Institucionales
   */
  @Value("${dwh.ficheros.nacional.institucionales.file:fdwe0e97p.txt}")
  private String fileNameInstitucionales;

  @Autowired
  FicheroNacionalBo ficheroNacionalBo;

  @Autowired
  OIInstitucionalesDao oIInstitucionalesDao;

  @Autowired
  Tmct0brkBo tmct0brkBo;

  @Autowired
  Tmct0aliBo tmct0aliBo;

  @Autowired
  Tmct0cliBo tmct0cliBo;

  @Autowired
  Tmct0aloBo tmct0aloBo;

  @Autowired
  Tmct0estBo tmct0estBo;

  @Autowired
  Tmct0xasBo tmct0xasBo;

  private Tmct0cta tmct0cta = new Tmct0cta();
  private Tmct0ctg tmct0ctg = new Tmct0ctg();
  private Tmct0cto tmct0cto = new Tmct0cto();
  private Tmct0ord tmct0ord = new Tmct0ord();

  /**
   * Obtiene el separador de linea
   */
  @Override
  public String getLineSeparator() {
    return INSTITUCIONALES_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {

    return oIInstitucionalesDao.findAllListInstitucionales(estado, tradeini, tradefin, pageable.getOffset(),
        pageable.getPageSize());
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    return oIInstitucionalesDao.countAllListInstitucionales(estado, tradeini, tradefin);

  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {

    if (LOG.isDebugEnabled()) {
      LOG.debug("Fichero sin cabecera");
    }
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {

    if (LOG.isDebugEnabled()) {
      LOG.debug("Fichero sin pié de página");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    try {
      launchInstitucionalesWriteLine(fileData, outWriter);
    }
    catch (InterruptedException e) {
    }
  }

  public String loadInstitucional(InstitucionalDTO dataIns, Tmct0ctd ctd) {

    try {
      String alias = "";
      if (ctd.getCdalias().indexOf('|') == -1) {
        alias = ctd.getCdalias();
      }
      else {
        alias = ctd.getCdalias().substring(ctd.getCdalias().indexOf('|') + 1);
      }
      if (getTpClienn(ctd.getCdalias(), new Integer(GenerateFilesUtilDwh.formatDate("yyyyMMdd", ctd.getFetrade())))
          .equals(DwhConstantes.INSTITUCIONAL_TIPO)) {

        /* RELLENAMOS LOS CAMPOS */
        // 1 NUOPROUT
        dataIns.setNUOPROUT(
            BigDecimal.valueOf(ctd.getNuoprout()).divide(new BigDecimal("10")).setScale(0, BigDecimal.ROUND_FLOOR));
        // 2 NUPAREJE
        String nuoprout = OperacionesInternacionalDWHUtils.formatNum(BigDecimal.valueOf(ctd.getNuoprout()), 9, 0);
        String nupareje = OperacionesInternacionalDWHUtils.formatNum(BigDecimal.valueOf(ctd.getNupareje()), 4, 0);
        dataIns.setNUPAREJE(new BigDecimal(nuoprout.substring(8) + nupareje.substring(1)));
        // CDCLIENT
        String clientAlias = "";

        Tmct0est tmct0est = getTmct0est(ctd.getCdalias(),
            new BigDecimal(GenerateFilesUtilDwh.formatDate("yyyyMMdd", ctd.getFetrade())));

        clientAlias = alias.trim();
        // 3 CDCLIENT
        dataIns.setCDCLIENT(new BigDecimal(clientAlias));
        // 4 CDENTLIQ
        dataIns.setCDENTLIQ(ctd.getCdcleare().substring(0, 4));
        // 5 CDISINVV
        dataIns.setCDISINVV(ctd.getCdisin());

        initEntities(ctd);

        // 6 CDBOLSAS
        String inforxml = "";
        if (ctd.getId().getCdmercad().trim() != null) {
          inforxml = ctd.getId().getCdmercad().trim();
        }

        String cdbolsas = oIInstitucionalesDao.getCdBolsas(inforxml);
        cdbolsas = cdbolsas.trim();
        if (cdbolsas == null) {
          cdbolsas = "   ";
        }
        /*
         * if (tmct0cto != null) { inforxml =
         * tmct0cto.getId().getCdmercad().trim(); }
         * 
         * String cdbolsas = getInforas4(DwhConstantes.NBCAMXML, inforxml,
         * DwhConstantes.NBCAMAS4);
         * dataIns.setCDBOLSAS(OperacionesInternacionalDWHUtils.padRight(
         * cdbolsas, 3)); if (dataIns.getCDBOLSAS().trim().equals("")) { //
         * dataMin.setCDBOLSAS(ctd.getFk_tmct0cta_1().getFk_tmct0ctg_1().
         * getFk_tmct0cto_1().getCdmercad().trim());
         * dataIns.setCDBOLSAS(inforxml); }
         */
        dataIns.setCDBOLSAS(cdbolsas);

        // 7 TPOPERAC
        dataIns.setTPOPERAC(ctd.getCdtpoper().toString().trim());
        // 8 TPEJERCI - Por defecto 'CV' en el constructor.
        // 9 TPEJECUC - Por defecto 'V' en el constructor.
        // 10 TPOPESTD - Por defecto 'N' en el constructor.
        // 11 TPCONTRA - Por defecto 'T' en el constructor.
        // 12 CDOPEMER

        dataIns.setCDOPEMER("");

        if (tmct0est != null) {
          String trader = tmct0est.getCdsaleid().trim();
          dataIns.setCDOPEMER(getInforas4(DwhConstantes.NBCAMTRADERXML, trader, DwhConstantes.NBCAMAS4));
        }
        if (dataIns.getCDOPEMER().trim().equals("")) {
          dataIns.setCDOPEMER("99999999");
        }
        // 13 CDDERECH
        dataIns.setCDDERECH("N");
        // 14 CDPROCED - Por defecto 'D' en el constructor.
        // 15 FHEJECUC
        dataIns.setFHEJECUC(OperacionesInternacionalDWHUtils.fillDate(ctd.getFetrade(), "dd/MM/yyyy"));
        // 16 IMEFECTI
        dataIns.setIMEFECTI(ctd.getEutotbru().setScale(2, BigDecimal.ROUND_FLOOR));
        // 17 NUCANEJE
        dataIns.setNUCANEJE(ctd.getNutitagr().setScale(2, BigDecimal.ROUND_FLOOR));
        // 18 IMDERCOG
        dataIns.setIMDERCOG(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 19 IMDERLIQ
        dataIns.setIMDERLIQ(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // *20 IMCOMSVB
        // FIXME arreglar diferencias
        final BigDecimal imcomsvb = getDiferencias(ctd);

        dataIns.setIMCOMSVB(imcomsvb.setScale(2, BigDecimal.ROUND_FLOOR));
        // 21 IMCOMLIQ
        dataIns.setIMCOMLIQ(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 22 IMCOMBRK - Por defecto '+000000000000000.00' en el
        // constructor.
        // 23 IMCOMUK2 - Por defecto '+000000000000000.00' en el
        // constructor.
        // 24 IMCOMUSA - Por defecto '+000000000000000.00' en el
        // constructor.
        // 25 IMCOMDEV
        dataIns.setIMCOMDEV(ctd.getEucomdev().setScale(2, BigDecimal.ROUND_FLOOR));
        // 26 NUPERIOD
        Date aux = ctd.getFetrade();
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
        String yearString = simpleDateformat.format(aux);
        dataIns.setNUPERIOD(new BigDecimal(yearString).setScale(0, BigDecimal.ROUND_FLOOR));
        // 27 IMDBRKDA - Por defecto '+000000000000000.00' en el constructor.
        // 28 CDCLIEST
        dataIns.setCDCLIEST(new BigDecimal(clientAlias));
        // 29 IMBRUCOG
        dataIns.setIMBRUCOG(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 30 IMBRULIQ
        dataIns.setIMBRULIQ(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 31 IMBRUBAN
        dataIns.setIMBRUBAN(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 32 IMBANSIS - Por defecto '+000000000000000.00' en el
        // constructor.
        // 33 CDBROKER
        final String cdBroker = ctd.getId().getCdbroker();
        if (cdBroker != null) {
          final String broker = oIInstitucionalesDao.getCdBroker(cdBroker.trim());
          dataIns.setCDBROKER(new BigDecimal(broker));
        }
        else {
          dataIns.setCDBROKER(new BigDecimal("00000000"));
        }

        // 34 CDGRUPBR - Por defecto 'G' en el constructor.
        // 35 IMNETCLT
        dataIns.setIMNETCLT(ctd.getEutotnet().setScale(2, BigDecimal.ROUND_FLOOR));
        // 36 IMBROCON
        dataIns.setIMBROCON(ctd.getImfibreu().setScale(2, BigDecimal.ROUND_FLOOR));
        // 37 IMCOMSUC
        dataIns.setIMCOMSUC(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 38 CDCUSGLB - Por defecto ' ' en el constructor.
        // 39 IMCOMCUS
        dataIns.setIMCOMCUS(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 40 CDGESTOR - Por defecto 'ES' en el constructor.
        // 41 CDUNIGEF - Por defecto 'ES' en el constructor.
        // 42 CDUNIGEB - Por defecto 'ES' en el constructor.
        // 43 IMCOMGRU
        dataIns.setIMCOMGRU(imcomsvb.subtract(ctd.getEucomdev()).setScale(2, BigDecimal.ROUND_FLOOR));
        // 44CDGRUREP - Por defecto ' ' en el constructor.
        // 45 CDUNICON - Por defecto ' ' en el constructor.
        // 46 IMREPGES - Por defecto '+00000000000000.00' en el
        // constructor.
        // 47 IMREPREC - Por defecto '+00000000000000.00' en el
        // constructor.
        // 48 IMREPCON - Por defecto '+00000000000000.00' en el
        // constructor.

        return dataIns.toString();
      }
      else {
        LOG.debug("El cliente no es de tipo Institucional");
        return null;
      }

    }
    catch (Exception e) {
      LOG.debug("No se pudo mapear InstitucionalDTO {} Tmct0ctd {}", dataIns, ctd.getId().getNuorden(),
          ctd.getId().getCdbroker(), ctd.getId().getCdmercad(), ctd.getId().getNbooking(), ctd.getId().getNucnfclt(),
          ctd.getId().getNualosec());
      return null;
    }
  }

  public void setInstitucionalParams(String estado, Date fechaEjecucion) throws SIBBACBusinessException {
    this.estado = estado;
    this.tradeini = ficheroNacionalBo.extraccionParametrizacion(fechaEjecucion);
    this.tradefin = fechaEjecucion;
  }

  public void initEntities(Tmct0ctd ctd) {
    tmct0cta = ctd.getTmct0cta();
    if (tmct0cta != null) {
      tmct0ctg = tmct0cta.getTmct0ctg();
    }
    if (tmct0ctg != null) {
      tmct0cto = tmct0ctg.getTmct0cto();
    }
    if (tmct0cto != null) {
      tmct0ord = tmct0cto.getTmct0ord();
    }
  }

  public String getTpClienn(String cdaliass, Integer fecha) {

    String tpclienn = "";
    Tmct0cli tmct0cli = null;

    String cdalias = OperacionesInternacionalDWHUtils.getCdalias(cdaliass);

    Tmct0ali tmct0ali = tmct0aliBo.findByCdaliassFechaActual(cdalias, fecha);

    if (tmct0ali != null) {
      tmct0cli = tmct0cliBo.findByCdbrocliAndFecha(tmct0ali.getCdbrocli(), fecha);
    }
    if (tmct0cli != null) {
      tpclienn = tmct0cli.getTpclienn().toString();
    }
    return tpclienn;
  }

  public BigDecimal getDiferencias(Tmct0ctd ctd) {
    BigDecimal imsvb = (new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));

    if (ctd.getImsvbeu() != null) {
      imsvb = ctd.getImsvbeu();
    }

    if (tmct0ord != null) {
      final String cdclsmdo = tmct0ord.getCdclsmdo().toString();
      final Tmct0cta tmct0cta = ctd.getTmct0cta();
      if (tmct0cta != null) {
        final Tmct0ctg tmct0ctg = tmct0cta.getTmct0ctg();

        if (cdclsmdo != null && "E".equals(cdclsmdo) && tmct0ctg != null) {
          final String cdmercad = tmct0ctg.getTmct0cto().getId().getCdmercad();
          final String cdalias = OperacionesInternacionalDWHUtils.getCdalias(ctd.getCdalias());
          final Tmct0alo tmct0alo = tmct0aloBo.getAlo(tmct0ctg.getTmct0cto().getTmct0ord().getNuorden(),
              tmct0ctg.getId().getNbooking(), ctd.getTmct0cta().getNucnfclt());

          if (cdmercad != null && tmct0alo != null && cdalias != null) {
            final Properties warehouseProperties = getCfgForProcess(
                SibbacEnums.Tmct0cfgConfig.DATAWAREHOUSE_0.getProcess());
            final String nationalTaxesString = warehouseProperties
                .getProperty(SibbacEnums.Tmct0cfgConfig.DATAWAREHOUSE_NATIONAL_TAXES.getKeyname());
            final String exceptionNationalTaxesString = warehouseProperties
                .getProperty(SibbacEnums.Tmct0cfgConfig.EXCEPTION_DATAWAREHOUSE_NATIONAL_TAXES.getKeyname());

            if (nationalTaxesString != null && exceptionNationalTaxesString != null) {
              final String[] nationalTaxes = nationalTaxesString.split(",");

              if (nationalTaxes != null && nationalTaxes.length > 0) {
                for (String nationalTax : nationalTaxes) {
                  if (nationalTax != null && nationalTax.equals(cdmercad.trim())
                      && !exceptionNationalTaxesString.contains(cdalias)) {

                    final BigDecimal imgatdvsAlo = tmct0alo.getImgatdvs();
                    final BigDecimal imgatdvsCtd = (imgatdvsAlo.multiply(ctd.getNutitagr()))
                        .divide(tmct0alo.getNutitcli());

                    imsvb = imsvb.subtract(imgatdvsCtd);
                  }
                }
              }
              else {
                LOG.debug("El campo nationalTaxes es nulo");
              }
            }
            else {
              LOG.debug("El campo nationalTaxesString es nulo");
            }
          }
          else {
            LOG.debug("El campo cdmercad o tmct0alo o cdalias es nulo");
          }
        }
        else {
          LOG.debug("El campo cdclsmdo es nulo");
        }
      }
      else {
        LOG.debug("El campo tmct0cta es nulo", ctd.getId().getNuorden(), ctd.getId().getCdbroker(),
            ctd.getId().getCdmercad(), ctd.getId().getNbooking(), ctd.getId().getNucnfclt(), ctd.getId().getNualosec());
      }
    }
    else {
      LOG.debug("El campo tmct0ord es nulo");
    }

    return imsvb;
  }

  /**
   * Obtiene un objeto Properties con la configuraciÃ³n relacionada con el
   * proceso pasado como parÃ¡metro
   * 
   * @param process
   * @param hpm Gestor de persistencia
   * @return un objeto Properties con la configuraciÃ³n relacionada con el
   * proceso pasado como parÃ¡metro
   * @throws Exception
   */
  public static Properties getCfgForProcess(String process) {
    Properties result = new Properties();
    try {

      List<Tmct0cfg> list = (List<Tmct0cfg>) getCfgMultiple(process, "");

      if (list.size() > 0) {
        for (Tmct0cfg item : list) {
          result.put(item.getId().getKeyname().trim(), item.getKeyvalue().trim());
        }
      }
    }
    catch (Exception e) {
      // Poner el mensaje de log
    }
    finally {
    }
    return result;
  }

  private static Map<String, Tmct0cfg> cfg = new HashMap<String, Tmct0cfg>();

  public static List<Tmct0cfg> getCfgMultiple(String process, String keyname) {
    List<Tmct0cfg> list = new ArrayList<Tmct0cfg>();
    String key = getKey(process, keyname, "");
    for (String keyCfg : cfg.keySet()) {
      if (keyCfg.startsWith(key)) {
        list.add(cfg.get(keyCfg));
      }
    }
    Collections.sort(list, new Comparator<Tmct0cfg>() {

      @Override
      public int compare(Tmct0cfg object1, Tmct0cfg object2) {
        if (object1.getId().getProcess().compareTo(object2.getId().getProcess()) != 0) {
          return object1.getId().getProcess().compareTo(object2.getId().getProcess());
        }
        else if (object1.getId().getKeyname().compareTo(object2.getId().getKeyname()) != 0) {
          return object1.getId().getKeyname().compareTo(object2.getId().getKeyname());
        }
        else {
          return object1.getKeyvalue().compareTo(object2.getKeyvalue());
        }
      }

    });
    return list;
  }

  private static String getKey(String key1, String key2, String key3) {
    return key1.trim() + "" + key2.trim() + "" + key3.trim();
  }

  public String getCdalias(String cdalias) {
    cdalias = cdalias.trim();
    String[] alias = cdalias.split("\\|");
    if (alias.length == 2)
      cdalias = alias[1].trim();
    return cdalias;
  }

  public Tmct0est getTmct0est(String cdaliass, BigDecimal fecha) {
    Tmct0est tmct0est = null;
    String cdalias = getCdalias(cdaliass);
    tmct0est = tmct0estBo.findByCdaliassFechaActual(cdalias, fecha);
    return tmct0est;
  }

  public String getInforas4(String nbcamxml, String inforxml, String nbcamas4) {
    Tmct0xas destinoCfg = tmct0xasBo.findByNameAndInforAndNbcamas(nbcamxml, inforxml, nbcamas4);
    String result = "";
    if (destinoCfg != null && (destinoCfg.getInforas4() != null)) {
      result = destinoCfg.getInforas4().trim();
    }
    return result;
  }

  ////////////////////////////////////////////////

  private void launchInstitucionalesWriteLine(List<Object[]> institucionalesList, BufferedWriter writer)
      throws InterruptedException {
    final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

    final CallableNacionalInstitucionalesLineThreadsBo writeInstitucionalesLine = ctx
        .getBean(CallableNacionalInstitucionalesLineThreadsBo.class);

    writeInstitucionalesLine.setWriter(writer);
    writeInstitucionalesLine.setInstitucionalesList(institucionalesList);

    processInterfaces.add(writeInstitucionalesLine);

    try {
      launchThreadBatchInstitucionales(processInterfaces);
    }
    catch (SIBBACBusinessException e) {
    }

  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatchInstitucionales(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final String currentProcessName = "InstitucionalesWriter";

    final ExecutorService executor = Executors.newFixedThreadPool(1,
        new NamedThreadFactory("batch_" + currentProcessName));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      callableBo.setProcessName(currentProcessName);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  public String getFileNameInstitucionales() {
    return fileNameInstitucionales;
  }

  public void setFileNameInstitucionales(String fileNameInstitucionales) {
    this.fileNameInstitucionales = fileNameInstitucionales;
  }

  public void configureName() {
    super.setFileName(this.fileNameInstitucionales);
  }

}
