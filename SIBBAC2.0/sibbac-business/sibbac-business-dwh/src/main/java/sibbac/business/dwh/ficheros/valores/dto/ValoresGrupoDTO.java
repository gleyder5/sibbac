package sibbac.business.dwh.ficheros.valores.dto;

import java.sql.Timestamp;

/**
 * The Class TMCT0blanqueoControlAnalisisDTO.
 */
public class ValoresGrupoDTO extends AbstractDTO {

  /**
   * 
   */
  private static final long serialVersionUID = -5270093962321653900L;

  private String cdSibe;

  private String descSibe;

  private String cdgruval;

  private String nbgruval;

  private String cdtipren;

  private String nbtipren;

  private Timestamp fhaudit;

  private String usuaudit;

  public String getCdSibe() {
    return cdSibe;
  }

  public void setCdSibe(String cdSibe) {
    this.cdSibe = cdSibe;
  }

  public String getDescSibe() {
    return descSibe;
  }

  public void setDescSibe(String descSibe) {
    this.descSibe = descSibe;
  }

  public String getCdgruval() {
    return cdgruval;
  }

  public void setCdgruval(String cdgruval) {
    this.cdgruval = cdgruval;
  }

  public String getNbgruval() {
    return nbgruval;
  }

  public void setNbgruval(String nbgruval) {
    this.nbgruval = nbgruval;
  }

  public String getCdtipren() {
    return cdtipren;
  }

  public void setCdtipren(String cdtipren) {
    this.cdtipren = cdtipren;
  }

  public String getNbtipren() {
    return nbtipren;
  }

  public void setNbtipren(String nbtipren) {
    this.nbtipren = nbtipren;
  }

  public Timestamp getFhaudit() {
    return fhaudit;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "ValoresGrupoDTO [cdSibe=" + this.cdSibe + ", descSibe=" + this.descSibe + ", cdgruval=" + this.cdgruval
        + ", nbgruval=" + this.nbgruval + ", cdtipren=" + this.cdtipren + ", nbtipren=" + this.nbtipren + ", fhaudit="
        + this.fhaudit + ", usuaudit=" + this.usuaudit + "]";
  }
}
