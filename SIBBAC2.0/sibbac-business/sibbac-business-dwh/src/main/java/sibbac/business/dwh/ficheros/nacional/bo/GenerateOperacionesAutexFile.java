package sibbac.business.dwh.ficheros.nacional.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableNacionalAutexLineThreadsBo;
import sibbac.business.dwh.ficheros.nacional.db.dao.OperacionesAutexDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.AbstractGenerateFile;

@Component
public class GenerateOperacionesAutexFile extends AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateOperacionesAutexFile.class);

  private static final String AUTEX_LINE_SEPARATOR = "\r\n";

  /**
   * Nombre del fichero generado por el proceso
   */
  @Value("${dwh.ficheros.nacional.autex.file:fdwh0ejt.txt}")
  private String fileName;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${dwh.ficheros.nacional.batch.timeout.global: 60}")
  private int timeout = 60;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private OperacionesAutexDao operacionesAutexDao;

  public Date fechaEjecucion;

  /**
   * Obtiene el separador de linea
   */
  @Override
  public String getLineSeparator() {
    return AUTEX_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {

    return operacionesAutexDao.findAllListAutex(fechaEjecucion, pageable.getOffset(), pageable.getPageSize());
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    return operacionesAutexDao.countAllListAutex(fechaEjecucion);
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero no tiene cabecera");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    try {
      launchAutexWriteLine(fileData, outWriter);
    }
    catch (InterruptedException e) {
    }

  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {

    if (LOG.isDebugEnabled()) {
      LOG.debug("Fichero sin pié de página");
    }
  }

  /**
   * Método para setear la fecha deseada para la generación de Op.Autex
   * @param fecha
   */
  public void setFechaEjecucionAutex(Date fecha) {
    this.fechaEjecucion = fecha;
  }

  ////////////////////////////////////////////////

  private void launchAutexWriteLine(List<Object[]> autexList, BufferedWriter writer) throws InterruptedException {
    final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

    final CallableNacionalAutexLineThreadsBo writeAutexLine = ctx.getBean(CallableNacionalAutexLineThreadsBo.class);

    writeAutexLine.setWriter(writer);
    writeAutexLine.setAutexList(autexList);

    processInterfaces.add(writeAutexLine);

    try {
      launchThreadBatchAutex(processInterfaces);
    }
    catch (SIBBACBusinessException e) {
    }

  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatchAutex(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final String currentProcessName = "AutexdWriter";

    final ExecutorService executor = Executors.newFixedThreadPool(1,
        new NamedThreadFactory("batch_" + currentProcessName));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      callableBo.setProcessName(currentProcessName);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  public void configureName() {
    super.setFileName(this.fileName);
  }
}
