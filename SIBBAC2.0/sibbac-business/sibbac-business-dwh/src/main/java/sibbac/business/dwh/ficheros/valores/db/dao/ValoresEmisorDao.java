package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.ValoresEmisor;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface ValoresEmisorDao extends JpaRepository<ValoresEmisor, String> {

  @Query(value = "SELECT CDPAIS, CDFISCAL, CDNIF, DSEMISOR "
      + "FROM TMCT0_VALORES_EMISOR WHERE CDEMISIO=:cdemisio", nativeQuery = true)
  public List<Object[]> findEmisor(@Param("cdemisio") String cdemisio);

}
