package sibbac.business.dwh.ficheros.callable.thread;

import java.util.concurrent.ThreadFactory;

/**
 * Factoria de Hilos
 * @author ipalacio
 *
 */
public class NamedThreadFactory implements ThreadFactory {

  private int count = 0;

  private String name;

  public NamedThreadFactory(String name) {
    this.name = name;
  }

  @Override
  public Thread newThread(Runnable r) {
    final Thread thread = new Thread(r);
    thread.setName(String.format("SIBBAC_%s-%02d", name, count++));
    return thread;
  }
}