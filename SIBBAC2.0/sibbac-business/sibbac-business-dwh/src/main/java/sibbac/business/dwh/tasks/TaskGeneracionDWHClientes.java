package sibbac.business.dwh.tasks;

import java.util.ArrayList;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.dwh.ficheros.clientes.bo.FicherosClientesBo;
import sibbac.business.dwh.ficheros.clientes.bo.GenerateClientesEstaticosFile;
import sibbac.business.dwh.ficheros.clientes.bo.GenerateClientesNifFile;
import sibbac.business.dwh.ficheros.clientes.bo.GenerateEstadisticasClientesNiveles;
import sibbac.business.dwh.ficheros.clientes.bo.GenerateGrupoClientesEstaticosFile;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.tasks.commons.TaskProcessDWH;
import sibbac.business.dwh.utils.DwhConstantes.NivelesFicheroClientes;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_ENVIO_FICHEROS_DWH_CLIENTES, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 23 ? * MON-FRI")
public class TaskGeneracionDWHClientes extends WrapperTaskConcurrencyPrevent implements TaskProcessDWH {

  private static final Logger LOG = LoggerFactory.getLogger(TaskGeneracionDWHClientes.class);

  @Value("${dwh.ficheros.clientes.db.default.page.size:200}")
  private int dbPageSize;

  /**
   * Nombre del fichero para la generacion del fichero de Grupo de Clientes
   * Estáticos
   */
  @Value("${dwh.ficheros.clientes.generated_if_empty:true}")
  private Boolean generateIfEmpty;

  /**
   * Ruta donde se deposita el fichero
   */
  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String outPutDir;

  /**
   * Nombre del fichero generado por el proceso
   */
  @Value("${dwh.ficheros.clientes.estaticos.file:clidwh.txt}")
  private String fileNameCliEstaticos;

  /**
   * Nombre del fichero para la generacion del ficheros de clientes y sus nif
   */
  @Value("${dwh.ficheros.clientes.cif.file:clientescif.txt}")
  private String fileNameCliNif;

  /**
   * Nombre del fichero para la generacion del fichero de Grupo de Clientes
   * Estáticos
   */
  @Value("${dwh.ficheros.clientes.estaticos_grupos.file:grcldwh.txt}")
  private String fileNameGruposCli;

  /**
   * Validador BD
   */
  @Autowired
  private FicherosClientesBo ficherosClientes;

  /**
   * Generador de Ficheros de Clientes DWH
   */
  @Autowired
  private GenerateClientesEstaticosFile generateClientesEstaticosFile;

  /**
   * Generador del fichero de clientes NIF
   */
  @Autowired
  private GenerateClientesNifFile generateClientesNifFile;

  /**
   * Generador del fichero de clientes estáticos
   */
  @Autowired
  private GenerateGrupoClientesEstaticosFile generateGrupoClientesEstaticosFile;

  /**
   * Generador del fichero de clientes estáticos por niveles
   */
  @Autowired
  private GenerateEstadisticasClientesNiveles generateEstadisticasClientesNiveles;

  /**
   * Listado de resultados de la generacion de ficheros
   */
  private List<ProcessFileResultDTO> processFileResultsList;

  /*
   * Ejecuta la tarea Clientes
   */
  @Override
  public void executeTask() throws SIBBACBusinessException {
    this.launchProcess();
  }

  @Override
  public void launchProcess() throws SIBBACBusinessException {
    processFileResultsList = new ArrayList<>();

    final long initStartTime = System.currentTimeMillis();

    processFileResultsList.addAll(launchClientes());

    if (LOG.isDebugEnabled()) {
      LOG.debug(
          String.format("El proceso ha terminado, se ha ejecutado en %d", System.currentTimeMillis() - initStartTime));
    }
  }

  /**
   * Lanza el proceso de Valores
   * @throws SIBBACBusinessException
   */
  public List<ProcessFileResultDTO> launchClientes() throws SIBBACBusinessException {
    final List<ProcessFileResultDTO> result = new ArrayList<>();
    try {
      ProcessFileResultDTO processFileResultDTO = null;

      if (LOG.isDebugEnabled()) {
        LOG.debug("Se inicia la tarea");
      }

      // validaciones previas bd
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ficherosClientes.clientesAlta();
      ficherosClientes.clientesOperado();

      processFileResultDTO = new ProcessFileResultDTO();

      // Generacion de Fichero de Clientes Estáticos
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      generateClientesEstaticosFile.setDbPageSize(dbPageSize);
      generateClientesEstaticosFile.setGenerateIfEmpty(generateIfEmpty);
      generateClientesEstaticosFile.generateFile(outPutDir, fileNameCliEstaticos);

      processFileResultDTO.setFileNameProcess(generateClientesEstaticosFile.getFileName());
      processFileResultDTO.setFileResult(generateClientesEstaticosFile.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Generacion de Fichero de Clientes NIF
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateClientesNifFile.setDbPageSize(dbPageSize);
      generateClientesNifFile.setGenerateIfEmpty(generateIfEmpty);
      generateClientesNifFile.generateFile(outPutDir, fileNameCliNif);

      processFileResultDTO.setFileNameProcess(generateClientesNifFile.getFileName());
      processFileResultDTO.setFileResult(generateClientesNifFile.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Generacion de Fichero de Grupos Clientes
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateGrupoClientesEstaticosFile.setDbPageSize(dbPageSize);
      generateGrupoClientesEstaticosFile.setGenerateIfEmpty(generateIfEmpty);
      generateGrupoClientesEstaticosFile.generateFile(outPutDir, fileNameGruposCli);

      processFileResultDTO.setFileNameProcess(generateGrupoClientesEstaticosFile.getFileName());
      processFileResultDTO.setFileResult(generateGrupoClientesEstaticosFile.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Gerenación de los niveles
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateEstadisticasClientesNiveles.setDbPageSize(dbPageSize);
      generateEstadisticasClientesNiveles.setGenerateIfEmpty(generateIfEmpty);
      generateEstadisticasClientesNiveles.setOutPutDir(outPutDir);

      // Gerenación de los nivel 0
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateEstadisticasClientesNiveles.generateFileClientePorNivel(NivelesFicheroClientes.NIVEL0);

      processFileResultDTO.setFileNameProcess(generateEstadisticasClientesNiveles.getFileName());
      processFileResultDTO.setFileResult(generateEstadisticasClientesNiveles.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Gerenación de los nivel 1
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateEstadisticasClientesNiveles.generateFileClientePorNivel(NivelesFicheroClientes.NIVEL1);

      processFileResultDTO.setFileNameProcess(generateEstadisticasClientesNiveles.getFileName());
      processFileResultDTO.setFileResult(generateEstadisticasClientesNiveles.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Gerenación de los nivel 2
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateEstadisticasClientesNiveles.generateFileClientePorNivel(NivelesFicheroClientes.NIVEL2);

      processFileResultDTO.setFileNameProcess(generateEstadisticasClientesNiveles.getFileName());
      processFileResultDTO.setFileResult(generateEstadisticasClientesNiveles.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Gerenación de los nivel 3
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateEstadisticasClientesNiveles.generateFileClientePorNivel(NivelesFicheroClientes.NIVEL3);

      processFileResultDTO.setFileNameProcess(generateEstadisticasClientesNiveles.getFileName());
      processFileResultDTO.setFileResult(generateEstadisticasClientesNiveles.getFileGenerationResult());
      result.add(processFileResultDTO);

      // Gerenación de los nivel 4
      // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      processFileResultDTO = new ProcessFileResultDTO();
      generateEstadisticasClientesNiveles.generateFileClientePorNivel(NivelesFicheroClientes.NIVEL4);

      processFileResultDTO.setFileNameProcess(generateEstadisticasClientesNiveles.getFileName());
      processFileResultDTO.setFileResult(generateEstadisticasClientesNiveles.getFileGenerationResult());
      result.add(processFileResultDTO);

      if (LOG.isDebugEnabled()) {
        LOG.info("Se termina la tarea");
      }
    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(e);
    }

    return result;
  }

  /**
   * Obtiene los resultados de la generación de los ficheros
   * @return
   */
  @Override
  public List<ProcessFileResultDTO> getProcessFileResultsList() {
    return processFileResultsList;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_FICHERO_CLIENTES;
  }
}
