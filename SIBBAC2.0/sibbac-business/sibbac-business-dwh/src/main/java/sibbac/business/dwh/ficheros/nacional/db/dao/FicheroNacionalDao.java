package sibbac.business.dwh.ficheros.nacional.db.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.unbundled.db.model.Unbundled;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0cfgId;

@Repository
public interface FicheroNacionalDao extends JpaRepository<Tmct0cfg, Tmct0cfgId> {

  public static final String QUERY_PARAMETRIZACION = "SELECT KEYVALUE FROM TMCT0CFG WHERE APPLICATION = 'SIBBAC' "
      + "AND PROCESS='DWH_SIBBAC' AND KEYNAME = 'DWH_' || (SELECT DAYOFWEEK_ISO ((CAST(:fechaEjecucion AS DATE))) FROM SYSIBM.SYSDUMMY1) ";

  /**
   * Query de extracción para la parametrización
   * @return
   */
  @Query(value = QUERY_PARAMETRIZACION, nativeQuery = true)
  public String queryParametrizacionActual(@Param("fechaEjecucion") Date fechaEjecucion);

  @Query(value = "SELECT ORD.NUORDEN, ALC.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ, left(trim(coalesce(ord.cdcanal,''))||'000',3), ORD.CDORIGEN,"
      + " ALC.CDENTLIQ,  ALC.NUTITLIQ, ALC.FEEJELIQ, trim(BOK.CDALIAS), ALC.IMCOMBCO, ALC.IMCOMBRK, "
      + " ALC.IMCOMDVO, ALC.IMCOMSIS, ALC.IMCOMSVB, BOK.CANONCONTRPAGOCLTE,  BOK.CDISIN, "
      + " BOK.CDTPOPER, EST.STLEV4LM, ALC.IMBANSIS, (row_number() over(PARTITION BY ALC.NBOOKING order by ALC.NUCNFLIQ)) "
      + " FROM TMCT0ORD ORD" + " INNER JOIN TMCT0BOK BOK ON " + "  BOK.FETRADE >= :fechaInicio "
      + "  AND BOK.FETRADE <= :fechaFin" + "  AND ORD.NUORDEN = BOK.NUORDEN" + "  AND BOK.CDALIAS NOT IN ('99999')"
      + "  AND BOK.CDESTADOASIG >= 65" + " INNER JOIN TMCT0ALC ALC ON" + "  BOK.NUORDEN = ALC.NUORDEN"
      + "  AND BOK.NBOOKING = ALC.NBOOKING" + "  AND ALC.CDESTADOASIG >= 65" + "  AND ALC.NUTITLIQ>0"
      + " INNER JOIN TMCT0EST EST ON" + "  EST.CDALIASS = BOK.CDALIAS"
      + "  AND EST.FHFINAL >= TO_CHAR(CURRENT DATE, 'YYYYMMDD')"
      + " ORDER BY ORD.NUORDEN, BOK.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ", nativeQuery = true)
  public List<Object[]> findOperacionesNacional(@Param("fechaInicio") Date fechaInicio,
      @Param("fechaFin") Date fechaFin);

  @Query(value = "SELECT " + "    ALC.NUORDEN, " + "    ALC.NBOOKING, " + "    ALC.NUCNFCLT, " + "    ALC.NUCNFLIQ, "
      + "    DCT.CDMIEMBROMKT, " + "    DCT.CD_PLATAFORMA_NEGOCIACION, " + "    DCT.TPOPEBOL, " + "    DCT.CDSEGMENTO, "
      + "    DCT.IMPRECIO,  " + "    SUM( COALESCE( DCT.IMTITULOS, 0 )) AS TOTAL_IMTITULOS, "
      + "    SUM( COALESCE( DCT.IMCANONCONTREU, 0 )) AS TOTAL_IMCANONCONTREU, "
      + "    SUM( COALESCE( DCT.IMCANONCOMPEU, 0 )) AS TOTAL_IMCANONCOMPEU, "
      + "    SUM( COALESCE( DCT.IMEFECTIVO, 0 )) AS TOTAL_IMEFECTIVO " + "FROM " + "    TMCT0BOK BOK "
      + "INNER JOIN TMCT0ALC ALC ON " + "    BOK.NUORDEN = ALC.NUORDEN " + "    AND BOK.NBOOKING = ALC.NBOOKING "
      + "    AND ALC.CDESTADOASIG >= 65 " + "    AND BOK.CDESTADOASIG >= 65 " + "INNER JOIN TMCT0DESGLOSECAMARA DCM ON "
      + "    ALC.NUORDEN = DCM.NUORDEN " + "    AND ALC.NBOOKING = DCM.NBOOKING "
      + "    AND ALC.NUCNFCLT = DCM.NUCNFCLT " + "    AND ALC.NUCNFLIQ = DCM.NUCNFLIQ "
      + "    AND DCM.CDESTADOASIG >= 65 " + "INNER JOIN TMCT0DESGLOSECLITIT DCT ON "
      + "    DCM.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA " + "WHERE " + "    ALC.NUORDEN = :nuorden "
      + "    AND ALC.NBOOKING = :nbooking " + "    AND ALC.NUCNFCLT = :nuvnfclt " + "    AND ALC.NUCNFLIQ = :nucnfliq "
      + "GROUP BY " + "    ALC.NUORDEN, " + "    ALC.NBOOKING, " + "    ALC.NUCNFCLT, " + "    ALC.NUCNFLIQ, "
      + "    DCT.CDMIEMBROMKT, " + "    DCT.CD_PLATAFORMA_NEGOCIACION, " + "    DCT.TPOPEBOL, " + "    DCT.CDSEGMENTO, "
      + "    DCT.IMPRECIO", nativeQuery = true)
  public List<Object[]> findDesgloseNacional(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nuvnfclt") String nuvnfclt, @Param("nucnfliq") BigDecimal bigDecimal);

  @Query(value = "SELECT NBCLIENT, NBCLIENT1, NBCLIENT2 " + " FROM TMCT0AFI" + " WHERE CDINACTI = 'A'"
      + " AND NUORDEN = :nuorden" + " AND NBOOKING = :nbooking" + " AND NUCNFCLT = :nuvnfclt"
      + " AND NUCNFLIQ = :nucnfliq", nativeQuery = true)
  public List<Object[]> findClienteNacional(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nuvnfclt") String nuvnfclt, @Param("nucnfliq") BigDecimal bigDecimal);

  @Query("SELECT unb FROM Unbundled unb WHERE fechaPago > :fechaDesde AND fechaPago < :fechaHasta")
  public List<Unbundled> findbyDate(@Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);
}
