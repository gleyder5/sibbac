package sibbac.business.dwh.ficheros.nacional.db.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctdId;

@Repository
public interface OIInstitucionalesDao extends JpaRepository<Tmct0ctd, Tmct0ctdId> {

  public static final String INSTITUCIONALES_QUERY = "select CTD.CDALIAS, CTD.FETRADE, CTD.NUOPROUT, CTD.NUPAREJE, "
      + "CTD.CDCLEARE, CTD.CDISIN, CTD.NUCNFCLT, CTD.CDMERCAD, CTD.NUORDEN, CTD.CDBROKER, CTD.NBOOKING, CTD.CDTPOPER, "
      + "CTD.EUTOTBRU, CTD.NUTITAGR AS COL14, CTD.EUCOMDEV, CTD.EUTOTNET, CTD.IMFIBREU, CTD.FHAUDIT, CTD.IMSVBEU from TMCT0CTD CTD "
      + "INNER JOIN TMCT0CTA CTA ON CTD.NUCNFCLT=CTA.NUCNFCLT AND CTD.CDMERCAD=CTA.CDMERCAD AND CTD.NUORDEN=CTA.NUORDEN AND CTD.CDBROKER=CTA.CDBROKER AND CTD.NBOOKING=CTA.NBOOKING "
      + "INNER JOIN TMCT0CTG CTG ON CTA.NBOOKING=CTG.NBOOKING AND CTA.CDBROKER=CTG.CDBROKER AND CTA.NUORDEN=CTG.NUORDEN AND CTA.CDMERCAD=CTG.CDMERCAD "
      + "INNER JOIN TMCT0CTO CTO ON CTG.CDMERCAD=CTO.CDMERCAD AND CTG.NUORDEN=CTO.NUORDEN AND CTG.CDBROKER=CTO.CDBROKER "
      + "INNER JOIN TMCT0ORD ORD ON CTO.NUORDEN=ORD.NUORDEN INNER JOIN TMCT0STA STA ON CTD.CDESTADO=STA.CDESTADO AND CTD.CDTIPEST=STA.CDTIPEST "
      + "WHERE STA.CDESTADO >= :estado AND ORD.CDCLSMDO = 'E' AND CTD.FETRADE >= :tradeini AND CTD.FETRADE <= :tradefin "
      + "ORDER BY ORD.NUORDEN ASC, CTO.CDBROKER ASC, CTO.CDMERCAD ASC, CTG.NBOOKING ASC, CTA.NUCNFCLT ASC, CTD.NUALOSEC ASC";

  public static final String INSTITUCIONALES_PAGE_QUERY = INSTITUCIONALES_QUERY + " LIMIT :offset, :pageSize";

  public static final String INSTITUCIONALES_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + INSTITUCIONALES_QUERY + ")";

  @Query(value = INSTITUCIONALES_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListInstitucionales(@Param("estado") String estado, @Param("tradeini") Date tradeini,
      @Param("tradefin") Date tradefin, @Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

  @Query(value = INSTITUCIONALES_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListInstitucionales(@Param("estado") String estado, @Param("tradeini") Date tradeini,
      @Param("tradefin") Date tradefin);

  public static final String CDBROKER_QUERY = "SELECT TRIM(CDIDENTI) FROM TMCT0BRK WHERE CDBROKER=:cdbroker";

  @Query(value = CDBROKER_QUERY, nativeQuery = true)
  public String getCdBroker(@Param("cdbroker") String cdbroker);

  public static final String CDBOLSAS_QUERY = "select inforas4 from tmct0xas where nbcamas4='CDWAREHO' and nbcamxml='CDMERCAD' and inforxml=:inforxmlBolsa";

  @Query(value = CDBOLSAS_QUERY, nativeQuery = true)
  public String getCdBolsas(@Param("inforxmlBolsa") String inforxmlBolsa);
}
