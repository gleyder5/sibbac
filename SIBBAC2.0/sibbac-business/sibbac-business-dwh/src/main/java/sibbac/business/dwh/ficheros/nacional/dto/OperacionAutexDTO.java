package sibbac.business.dwh.ficheros.nacional.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO para trabajar con las reglas de Exoneracion
 * 
 */
public class OperacionAutexDTO implements Serializable {

  private static int AUTEX_FEEJCUC = 0;
  private static int AUTEX_CDISIN = 1;
  private static int AUTEX_IMEFECI = 2;
  private static int AUTEX_NUCANEJE = 3;
  private static int AUTEX_CDAUTEXX = 4;

  /**
   * 
   */
  private static final long serialVersionUID = -6336295126742722529L;

  private Date feEjecuc;
  private String cdIsin;
  private BigDecimal imEfecti;
  private BigDecimal nucanEje;
  private String cdAutexx;

  public Date getFeEjecuc() {
    return feEjecuc;
  }

  public void setFeEjecuc(Date feEjecuc) {
    this.feEjecuc = feEjecuc;
  }

  public String getCdIsin() {
    return cdIsin;
  }

  public void setCdIsin(String cdIsin) {
    this.cdIsin = cdIsin;
  }

  public BigDecimal getImEfecti() {
    return imEfecti;
  }

  public void setImEfecti(BigDecimal imEfecti) {
    this.imEfecti = imEfecti;
  }

  public BigDecimal getNucanEje() {
    return nucanEje;
  }

  public void setNucanEje(BigDecimal nucanEje) {
    this.nucanEje = nucanEje;
  }

  public String getCdAutexx() {
    return cdAutexx;
  }

  public void setCdAutexx(String cdAutexx) {
    this.cdAutexx = cdAutexx;
  }

  /**
   * @return
   */
  public static OperacionAutexDTO mapObject(Object[] cli) {
    OperacionAutexDTO autex = new OperacionAutexDTO();
    autex.setFeEjecuc((Date) (cli[AUTEX_FEEJCUC]));
    autex.setCdIsin((String) (cli[AUTEX_CDISIN]));
    autex.setImEfecti((BigDecimal) (cli[AUTEX_IMEFECI]));
    autex.setNucanEje((BigDecimal) (cli[AUTEX_NUCANEJE]));
    autex.setCdAutexx((String) (cli[AUTEX_CDAUTEXX]));

    return autex;
  }
}
