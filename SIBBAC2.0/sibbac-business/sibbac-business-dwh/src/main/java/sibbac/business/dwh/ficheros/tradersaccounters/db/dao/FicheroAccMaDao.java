package sibbac.business.dwh.ficheros.tradersaccounters.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.estaticos.db.model.BolsaDwh;

@Repository
public interface FicheroAccMaDao extends JpaRepository<BolsaDwh, String> {

  /**
   * Obtiene los datos para la generación del fichero FicheroAccMaDao sin
   * paginación
   * @return
   */
  @Query(value = "SELECT PRODUCTO, CUENTA, NBCORTO, TRIM(NBNOMBRE) || ' ' || TRIM(NBAPELLIDO1) || ' ' || TRIM(NBAPELLIDO2) AS NOMBRE "
      + "FROM TMCT0_SALES_TRADER WHERE INDACCOUNT= 1 LIMIT ?1, ?2", nativeQuery = true)
  public List<Object[]> findAllAccMa(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero FicheroAccMaDao sin
   * paginación
   * @return
   */
  @Query(value = "SELECT COUNT(*) FROM (SELECT PRODUCTO, CUENTA, NBCORTO, TRIM(NBNOMBRE) || ' ' || TRIM(NBAPELLIDO1) || ' ' || TRIM(NBAPELLIDO2) AS NOMBRE "
      + "FROM TMCT0_SALES_TRADER WHERE INDACCOUNT= 1)", nativeQuery = true)
  public Integer countAllAccMa();
}
