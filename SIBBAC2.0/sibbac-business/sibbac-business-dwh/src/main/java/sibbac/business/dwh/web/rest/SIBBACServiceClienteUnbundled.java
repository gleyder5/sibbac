package sibbac.business.dwh.web.rest;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.dwh.unbundled.bo.UnbundledBo;
import sibbac.business.unbundled.dto.AliasClienteDwhDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;;


@SIBBACService
public class SIBBACServiceClienteUnbundled implements SIBBACServiceBean {
  private static final Logger LOG = LoggerFactory.getLogger( SIBBACServiceClienteUnbundled.class );

  // COMMANDS
  private static final String CMD_GET_ALIAS = "getAlias";
  // RESULTS
  private static final String RESULT_ALIAS  = "result_alias";
  

  @Autowired
  private UnbundledBo unbundledBo;

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    final String command = webRequest.getAction();
    LOG.debug("Entra con command: " + command);
    WebResponse response = new WebResponse();
    Map<String, Object> result = new HashMap< String, Object >();
    List<AliasClienteDwhDTO> dataList;
    switch (command) {
      case CMD_GET_ALIAS:
        dataList = getAlias();
        result.put(RESULT_ALIAS, dataList);
        response.setResultados(result);
        break;
      default:
        break;
    }
    return response;
  }

  private List<AliasClienteDwhDTO> getAlias() throws SIBBACBusinessException {
    List<AliasClienteDwhDTO> dataList = unbundledBo.getAllFilterCliente();
    
    return dataList;
  }


  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new LinkedList<String>();
    commands.add( CMD_GET_ALIAS );
    return commands;
  }

  @Override
  public List<String> getFields() {
    // TODO Auto-generated method stub
    return null;
  }

}
