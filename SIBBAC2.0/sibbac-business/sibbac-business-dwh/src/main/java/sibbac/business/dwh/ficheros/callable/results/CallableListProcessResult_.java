package sibbac.business.dwh.ficheros.callable.results;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import sibbac.business.dwh.ficheros.commons.dto.ProcessResultDTO;

public interface CallableListProcessResult_ extends Callable<List<ProcessResultDTO>> {

  public void setFuture(Future<List<ProcessResultDTO>> future);

  public Future<List<ProcessResultDTO>> getFuture();

}