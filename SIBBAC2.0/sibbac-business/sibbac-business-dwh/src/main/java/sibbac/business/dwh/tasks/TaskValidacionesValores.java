package sibbac.business.dwh.tasks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.dwh.ficheros.valores.bo.CargaValoresBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_VALIDACION_VALORES, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 1 ? * MON-FRI *")
public class TaskValidacionesValores extends WrapperTaskConcurrencyPrevent {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOGGGER = LoggerFactory.getLogger(TaskValidacionesValores.class);

  @Autowired
  private CargaValoresBo cargaValoresBo;

  @Override
  public void executeTask() throws Exception {
    try {
      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.debug("Se inica la tarea TaskValidacionesValores");
      }

      final File validacionValoresFile = cargaValoresBo.validacionValores();
      if (validacionValoresFile != null) {
        TIPO tipoApunte = determinarTipoApunte();
       
        if (tipoApunte != null) {
          if (LOGGGER.isDebugEnabled()) {
            LOGGGER.debug("Se ha generado el fichero de error correctamente");
          }
          final Tmct0men tmct0men = menBo.findById("582");
         
          final List<File> files = new ArrayList<>();
          
          files.add(validacionValoresFile);
          
          if (LOGGGER.isDebugEnabled()) {
            LOGGGER.debug("Enviando correo de error");
          }
          this.sendMailWithExceptionMessage(tmct0men,
              new SIBBACBusinessException("Fallo en la operativa diaria de SIBBAC, Fantan ISIN por dar de alta"),
              files);
        }
      }

      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.info("Se termina la tarea TaskValidacionesValores");
      }

    }
    catch (Exception e) {
      LOG.error("Error al ejecutar TaskValidacionesValores", e);
      throw new SIBBACBusinessException("Error al ejecutar TaskValidacionesValores", e);
    }

  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_VALIDACIONES_VALORES;
  }

}
