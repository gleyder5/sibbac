package sibbac.business.dwh.ficheros.callable.results;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;

public interface CallableProcessFileResult extends Callable<ProcessFileResultDTO> {

  public void setFuture(Future<ProcessFileResultDTO> future);

  public Future<ProcessFileResultDTO> getFuture();

}