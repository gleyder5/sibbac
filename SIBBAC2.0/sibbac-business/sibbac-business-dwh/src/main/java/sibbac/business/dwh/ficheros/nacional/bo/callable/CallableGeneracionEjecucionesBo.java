package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableListProcessFileResult;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.tasks.TaskGeneracionDWHEjecuciones;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGeneracionEjecucionesBo implements CallableListProcessFileResult {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGeneracionEjecucionesBo.class);

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${dwh.ficheros.valores.batch.timeout.global: 60}")
  private int timeout;

  @Autowired
  private TaskGeneracionDWHEjecuciones taskGeneracionDWHEjecuciones;

  @Autowired
  private ApplicationContext ctx;

  private Future<List<ProcessFileResultDTO>> future;

  /**
   * Ejecuta el proceso
   * @throws ExecutionException
   * @throws SIBBACBusinessException
   * @throws InterruptedException
   */
  @Override
  public List<ProcessFileResultDTO> call() throws InterruptedException, SIBBACBusinessException, ExecutionException {
    final List<ProcessFileResultDTO> result = new ArrayList<>();

    result.addAll(taskGeneracionDWHEjecuciones.launchGeneracionEjecuciones());

    try {
      result.addAll(launchClienteValores());
    }
    catch (InterruptedException | ExecutionException e) {
      LOG.error("Se ha producido un error en la ejecución del hilo de Generación de Ejecuciones", e);
    }

    return result;
  }

  /**
   * Lanza el planificador
   * @throws SIBBACBusinessException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  private List<ProcessFileResultDTO> launchClienteValores() throws InterruptedException, ExecutionException {
    final List<CallableListProcessFileResult> processInterfaces = new ArrayList<>();

    processInterfaces.add(ctx.getBean(CallableGeneracionClientesBo.class));
    processInterfaces.add(ctx.getBean(CallableGeneracionValoresBo.class));

    return launchThreadBatch(processInterfaces);
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   * @throws ExecutionException
   */
  public List<ProcessFileResultDTO> launchThreadBatch(List<CallableListProcessFileResult> processInterfaces)
      throws InterruptedException, ExecutionException {
    final List<ProcessFileResultDTO> result = new ArrayList<>();

    final List<CallableListProcessFileResult> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(2, new NamedThreadFactory("clienteValores_Nacional"));

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableListProcessFileResult callableBo : processInterfaces) {
      callableBo.setFuture(executor.submit(callableBo));
      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threads
    executor.shutdown();

    // Return true si executor ha terminado
    final boolean onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (onTime) { // *
      for (CallableListProcessFileResult future : futureList) {
        result.addAll(future.getFuture().get());
      }
    }
    return result;
  }

  @Override
  public void setFuture(Future<List<ProcessFileResultDTO>> future) {
    this.future = future;
  }

  @Override
  public Future<List<ProcessFileResultDTO>> getFuture() {
    return future;
  }
}
