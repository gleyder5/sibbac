package sibbac.business.dwh.web.rest;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.dwh.ficheros.nacional.bo.LanzamientoManualProcess;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.business.dwh.web.dto.LanzamientoManualDTO;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.aop.logging.SIBBACLogging;
import sibbac.webapp.security.session.UserSession;

@RestController
@RequestMapping("dwh/LanzamientoManual")
public class SIBBACServiceLanzamientoManual {
  /** The Constant LOG. */
  protected static final String DEFAULT_CHARSET = CharEncoding.UTF_8;

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceLanzamientoManual.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES
  public static final String RESULT_LISTA = "lista";

  protected static final String DEFAULT_APPLICATION_TYPE = MediaType.APPLICATION_JSON + ";charset=" + DEFAULT_CHARSET;

  @Autowired
  private LanzamientoManualProcess lanzamientoManualProcess;

  @Autowired
  private HttpSession session;

  @Autowired
  protected Tmct0menBo menBo;

  @SIBBACLogging
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody LanzamientoManualDTO solicitud) {
    CallableResultDTO result = new CallableResultDTO();

    final boolean isRunningIBackGround = lanzamientoManualProcess.isRunningInBackGround();

    if (isRunningIBackGround) {
      result.addMessage("La tarea esta en ejecucion en segundo plano");
    }
    else {
      final String userSession = StringUtils.substring(((UserSession) session.getAttribute("UserSession")).getName(), 0,
          10);

      try {
        final Date fechaEjecucion = GenerateFilesUtilDwh.dateFromString(solicitud.getFechaEjecucion());
        lanzamientoManualProcess.setFechaEjecucion(fechaEjecucion);
        lanzamientoManualProcess.setUserSession(userSession);
        result = lanzamientoManualProcess.launchGenerateAndExportProcess();

      }
      catch (SIBBACBusinessException e) {
        LOG.error(e.getMessage(), e);
        result.addErrorMessage(e.getMessage());
        return result;
      }
    }

    return result;

  }
}
