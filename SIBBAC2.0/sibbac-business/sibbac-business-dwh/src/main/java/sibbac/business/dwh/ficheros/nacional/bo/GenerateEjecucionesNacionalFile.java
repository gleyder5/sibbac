package sibbac.business.dwh.ficheros.nacional.bo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableNacionalOperacionesLineThreadsBo;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableNacionalSaveEjecucionesThreadsBo;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableNacionalUnbundledLineThreadsBo;
import sibbac.business.dwh.ficheros.nacional.db.dao.FicheroNacionalDao;
import sibbac.business.dwh.ficheros.nacional.db.dao.Tmct0DwhEjecucionesDao;
import sibbac.business.dwh.ficheros.nacional.db.model.Tmct0DwhEjecuciones;
import sibbac.business.dwh.ficheros.nacional.dto.OperacionNacionalDTO;
import sibbac.business.dwh.unbundled.db.model.Unbundled;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.constants.FileResult;
import sibbac.database.bo.AbstractBo;

@Component
public class GenerateEjecucionesNacionalFile
    extends AbstractBo<Tmct0DwhEjecuciones, BigInteger, Tmct0DwhEjecucionesDao> {

  @Autowired
  Tmct0DwhEjecucionesDao tmct0DwhEjecucionesDao;

  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String filePath;

  @Value("${dwh.ficheros.ejecuciones.ejecuciones.file:fdwh0e97.txt}")
  private String fileName;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  // @Value("${sibbac.batch.timeout: 60}")
  private int timeout = 60;

  private final static Charset ENCODING = StandardCharsets.UTF_8;

  protected static final Logger LOG = LoggerFactory.getLogger(GenerateEjecucionesNacionalFile.class);

  @Autowired
  private FicheroNacionalBo ficheroNacionalBo;

  @Autowired
  private FicheroNacionalDao ficheroNacionalDao;

  @Autowired
  private ApplicationContext ctx;

  /**
   * Parámetro para activar generacion de fichero sin datos
   */
  private Boolean generateIfEmpty;

  /**
   * Resultado de la generación del fichero
   */
  private FileResult fileResult;

  private Date fechaEjecucion;

  /**
   * Método para generar fichero fdwh0e97.txt con fechas de Inicio y Fin
   * @param fechaEjecucion
   * @throws SIBBACBusinessException
   */
  public void generateFileEjecuciones() throws SIBBACBusinessException {
    GenerateFilesUtilDwh.createOutPutDir(filePath);

    Date fechaInicio = ficheroNacionalBo.extraccionParametrizacion(fechaEjecucion);

    LOG.info("Inicio tarea fichero Ejecuciones Nacional");
    generateFileEjecuciones(fechaInicio, fechaEjecucion);
    LOG.info("Fin tarea fichero Ejecuciones Nacional");

  }

  private void generateFileEjecuciones(Date fechaInicio, Date fechaFin) throws SIBBACBusinessException {

    StringBuilder fullFileNamePath = new StringBuilder();
    fullFileNamePath.append(filePath).append(File.separator).append(fileName);
    Path path = Paths.get(FilenameUtils.getFullPath(fullFileNamePath.toString()),
        FilenameUtils.getName(fullFileNamePath.toString()));

    List<OperacionNacionalDTO> operaciones = ficheroNacionalBo.getOperacionesDwh(fechaInicio, fechaFin);
    List<Unbundled> operacionesUnbundled = ficheroNacionalDao.findbyDate(fechaInicio, fechaFin);
    List<Tmct0DwhEjecuciones> ejecuciones = new ArrayList<>();

    BufferedWriter writer = null;
    try {
      writer = Files.newBufferedWriter(path, ENCODING);
      if (!operaciones.isEmpty() || !operacionesUnbundled.isEmpty()) {

        try {
          launchOperacionesWriteLine(operaciones, ejecuciones, writer);
          launchUnbundledWriteLine(operacionesUnbundled, ejecuciones, writer);
          fileResult = FileResult.GENETARATED_OK;

        }
        catch (InterruptedException e) {
        }
      }
      else {
        fileResult = FileResult.GENETARATED_EMPTY;
        LOG.info("No se han encontrado operaciones ni nacionales ni unbundled");
      }

      try {
        tmct0DwhEjecucionesDao.cleanEjecuciones();
        launchSaveEjecucionesProcess(ejecuciones);
      }
      catch (InterruptedException e) {
      }

    }
    catch (IOException | SIBBACBusinessException e) {
      fileResult = FileResult.GENETARATED_KO;
      LOG.error("Error al generar el fichero de Ejecuciones de nacional", e.getMessage(), e);
      throw new SIBBACBusinessException("Error al generar el fichero de Ejecuciones de nacional", e);
    }
    finally {
      if (writer != null) {
        try {
          writer.close();
        }
        catch (IOException e) {
          LOG.error("No se puede cerrar el buffer de escritura", e);
        }
      }
    }
  }

  /**
   * Método para generar cada Tmct0DwhEjecuciones que será guardado en la BD
   * posteriormente
   * @param nuOprout
   * @param nuPareje
   * @param cdCliest
   * @param cdIsinvv
   * @param fhEjecu
   * @return Tmct0DwhEjecuciones
   */
  public Tmct0DwhEjecuciones insertEjecuciones(String nuOprout, String nuPareje, Integer cdCliest, String cdIsinvv,
      Date fhEjecu) {

    Tmct0DwhEjecuciones tmct0DwhEjecuciones = new Tmct0DwhEjecuciones();

    tmct0DwhEjecuciones.setNuOprout(nuOprout);
    tmct0DwhEjecuciones.setNuPareje(nuPareje);
    tmct0DwhEjecuciones.setCdCliest(cdCliest);
    tmct0DwhEjecuciones.setCdIsinvv(cdIsinvv);
    tmct0DwhEjecuciones.setFhEjecu(fhEjecu);
    tmct0DwhEjecuciones.setUsAudit("TaskDWHEjecuciones");
    tmct0DwhEjecuciones.setFhAudit(new Timestamp(new Date().getTime()));

    return tmct0DwhEjecuciones;
  }

  ////////////////////////////////////////////////

  private void launchUnbundledWriteLine(List<Unbundled> unbundledList, List<Tmct0DwhEjecuciones> ejecuciones,
      BufferedWriter writer) throws InterruptedException, SIBBACBusinessException {
    final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

    final CallableNacionalUnbundledLineThreadsBo writeUnbundledLine = ctx
        .getBean(CallableNacionalUnbundledLineThreadsBo.class);

    writeUnbundledLine.setWriter(writer);
    writeUnbundledLine.setEjecucionesList(ejecuciones);
    writeUnbundledLine.setUnbundledList(unbundledList);

    processInterfaces.add(writeUnbundledLine);

    launchThreadBatchUnbundled(processInterfaces);

  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatchUnbundled(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final String currentProcessName = "UnbundledWriter";

    final ExecutorService executor = Executors.newFixedThreadPool(1,
        new NamedThreadFactory("batch_" + currentProcessName));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      callableBo.setProcessName(currentProcessName);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  ////////////////////////////////////////////////

  private void launchOperacionesWriteLine(List<OperacionNacionalDTO> operacionesList,
      List<Tmct0DwhEjecuciones> ejecuciones, BufferedWriter writer)
      throws InterruptedException, SIBBACBusinessException {
    final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

    final CallableNacionalOperacionesLineThreadsBo writeOperacionesLine = ctx
        .getBean(CallableNacionalOperacionesLineThreadsBo.class);

    writeOperacionesLine.setWriter(writer);
    writeOperacionesLine.setEjecucionesList(ejecuciones);
    writeOperacionesLine.setOperacionesList(operacionesList);

    processInterfaces.add(writeOperacionesLine);

    launchThreadBatchOperaciones(processInterfaces);

  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatchOperaciones(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final String currentProcessName = "OperacionesWriter";

    final ExecutorService executor = Executors.newFixedThreadPool(1,
        new NamedThreadFactory("batch_" + currentProcessName));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      callableBo.setProcessName(currentProcessName);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  ////////////////////////////////////////////////

  private void launchSaveEjecucionesProcess(List<Tmct0DwhEjecuciones> ejecucionesList)
      throws InterruptedException, SIBBACBusinessException {
    final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

    final CallableNacionalSaveEjecucionesThreadsBo saveEjecuciones = ctx
        .getBean(CallableNacionalSaveEjecucionesThreadsBo.class);

    saveEjecuciones.setEjecucionesList(ejecucionesList);
    processInterfaces.add(saveEjecuciones);

    launchThreadBatch(processInterfaces);

  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatch(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(1, new NamedThreadFactory("batch_SaveEjecuciones"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   * Método para setear la fecha deseada para la generación de Op.Autex
   * @param fecha
   */
  public void setFechaEjecucionNacional(Date fecha) {
    this.fechaEjecucion = fecha;
  }

  public FileResult getFileGenerationResult() {
    return fileResult;
  }

  public Boolean getGenerateIfEmpty() {
    return generateIfEmpty;
  }

  public void setGenerateIfEmpty(Boolean generateIfEmpty) {
    this.generateIfEmpty = generateIfEmpty;
  }
}
