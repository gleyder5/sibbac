package sibbac.business.dwh.ficheros.valores.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.FicheroValorDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateFicheroValoresFile")
public class GenerateFicheroValoresFile extends AbstractGenerateFile {

  private static final String VALORES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateFicheroValoresFile.class);

  /**
   * DAO de Acceso a Base de Datos 
   */
  @Autowired
  private FicheroValorDao ficheroValorDao;

  @Override
  public String getLineSeparator() {
    return VALORES_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return ficheroValorDao.findAllListFicheroValor(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return ficheroValorDao.countAllListFicheroValor();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera de pagina, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    for (Object[] objects : fileData) {
      if(!String.valueOf(objects[0]).equals("null") && !String.valueOf(objects[1]).equals("null") && !String.valueOf(objects[2]).equals("null")){
        String cdisinvv = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_12.value(),
            String.valueOf((String) objects[0]));
        String cdisingr = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_12.value(),
            String.valueOf((String) objects[1]));
        String nbtitgru = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(),
            String.valueOf((String) objects[2]));

        List<Object[]> datos = ficheroValorDao.getDatosIsin(String.valueOf((String) objects[0]));

        for (Object[] objectss : datos) {
          if(!String.valueOf(objectss[0]).equals("null") && !String.valueOf(objectss[1]).equals("null") && !String.valueOf(objectss[2]).equals("null") && !String.valueOf(objectss[3]).equals("null") && !String.valueOf(objectss[4]).equals("null") && !String.valueOf(objectss[5]).equals("null") && !String.valueOf(objectss[6]).equals("null") && !String.valueOf(objectss[7]).equals("null") && !String.valueOf(objectss[8]).equals("null"))
          {
            String nbtitrev = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(),
                String.valueOf((String) objectss[1]));
            String cdgrsibv = String.format(DwhConstantes.PaddingFormats.PADDIND_NUM0_2.value(),
                Integer.parseInt(String.valueOf((String) objectss[2])));
            String nbgrupov = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(),
                String.valueOf((String) objectss[3]));
            String cdgruval = String.format(DwhConstantes.PaddingFormats.PADDIND_NUM0_2.value(),
                Integer.parseInt(String.valueOf((String) objectss[4])));
            String nbgruval = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(),
                String.valueOf((String) objectss[5]));
            String cdtipren = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(),
                String.valueOf((String) objectss[6]));
            String nbtipren = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(),
                String.valueOf((String) objectss[7]));
            String cdvaibex = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(),
                String.valueOf((String) objectss[8]));

            final StringBuilder rowLine = new StringBuilder();

            rowLine.append(cdisinvv).append(nbtitrev).append(cdisingr).append(nbtitgru).append(cdgrsibv).append(nbgrupov)
                .append(cdgruval).append(nbgruval).append(cdtipren).append(nbtipren).append(cdvaibex);

            String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

            writeLine(outWriter, cadena);
          }
        }
      }
    }
  }

}
