package sibbac.business.dwh.ficheros.clientes.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cli;

@Repository
public interface DWHEstadisticasClientesNivelDao extends JpaRepository<Tmct0cli, String> {

  /**
   * Query para DWHestadisticasClientes0Dao, ORIGINAL
   */
  public static final String ESTADISTICAS_CLIENTES_0_QUERY = "SELECT NUNIVEL0, NBNIVEL0 FROM TMCT0ONI GROUP BY NUNIVEL0, NBNIVEL0 ";

  /**
   * Query para DWHestadisticasClientes0Dao con Paginación
   */
  public static final String ESTADISTICAS_CLIENTES_0_PAGE_QUERY = ESTADISTICAS_CLIENTES_0_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHestadisticasClientes0Dao
   */
  public static final String ESTADISTICAS_CLIENTES_0_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + ESTADISTICAS_CLIENTES_0_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes0Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_0_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListEstadisticasClientes0(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes0Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_0_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListEstadisticasClientes0();

  /**
   * Query para DWHestadisticasClientes1Dao, ORIGINAL
   */
  public static final String ESTADISTICAS_CLIENTES_1_QUERY = "SELECT NUNIVEL0, NBNIVEL0, NUNIVEL1,  NBNIVEL1 FROM TMCT0ONI  GROUP BY NUNIVEL0, NBNIVEL0, NUNIVEL1, NBNIVEL1";

  /**
   * Query para DWHestadisticasClientes1Dao con Paginación
   */
  public static final String ESTADISTICAS_CLIENTES_1_PAGE_QUERY = ESTADISTICAS_CLIENTES_1_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHestadisticasClientes1Dao
   */
  public static final String ESTADISTICAS_CLIENTES_1_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + ESTADISTICAS_CLIENTES_1_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes1Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_1_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListEstadisticasClientes1(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes1Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_1_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListEstadisticasClientes1();

  /**
   * Query para DWHestadisticasClientes2Dao, ORIGINAL
   */
  public static final String ESTADISTICAS_CLIENTES_2_QUERY = "SELECT NUNIVEL0, NBNIVEL0, NUNIVEL1,  NBNIVEL1, NUNIVEL2,  NBNIVEL2 "
      + "FROM TMCT0ONI  GROUP BY NUNIVEL0, NBNIVEL0, NUNIVEL1, NBNIVEL1, NUNIVEL2, NBNIVEL2 ";

  /**
   * Query para DWHestadisticasClientes2Dao con Paginación
   */
  public static final String ESTADISTICAS_CLIENTES_2_PAGE_QUERY = ESTADISTICAS_CLIENTES_2_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHestadisticasClientes2Dao
   */
  public static final String ESTADISTICAS_CLIENTES_2_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + ESTADISTICAS_CLIENTES_2_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes2Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_2_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListEstadisticasClientes2(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes2Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_2_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListEstadisticasClientes2();

  /**
   * Query para DWHestadisticasClientes3Dao, ORIGINAL
   */
  public static final String ESTADISTICAS_CLIENTES_3_QUERY = "SELECT NUNIVEL0, NBNIVEL0, NUNIVEL1,  NBNIVEL1, NUNIVEL2,  NBNIVEL2, NUNIVEL3,  NBNIVEL3 "
      + "FROM TMCT0ONI  GROUP BY NUNIVEL0, NBNIVEL0, NUNIVEL1, NBNIVEL1, NUNIVEL2, NBNIVEL2, NUNIVEL3, NBNIVEL3 ";

  /**
   * Query para DWHestadisticasClientes3Dao con Paginación
   */
  public static final String ESTADISTICAS_CLIENTES_3_PAGE_QUERY = ESTADISTICAS_CLIENTES_3_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHestadisticasClientes3Dao
   */
  public static final String ESTADISTICAS_CLIENTES_3_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + ESTADISTICAS_CLIENTES_3_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes3Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_3_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListEstadisticasClientes3(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes3Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_3_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListEstadisticasClientes3();

  /**
   * Query para DWHestadisticasClientes4Dao, ORIGINAL
   */
  public static final String ESTADISTICAS_CLIENTES_4_QUERY = "SELECT NUNIVEL0, NBNIVEL0, NUNIVEL1,  NBNIVEL1, NUNIVEL2,  NBNIVEL2, NUNIVEL3,  NBNIVEL3, NUNIVEL4,  NBNIVEL4 "
      + "FROM TMCT0ONI  GROUP BY NUNIVEL0, NBNIVEL0, NUNIVEL1, NBNIVEL1, NUNIVEL2, NBNIVEL2, NUNIVEL3, NBNIVEL3, NUNIVEL4, NBNIVEL4 ";

  /**
   * Query para DWHestadisticasClientes4Dao con Paginación
   */
  public static final String ESTADISTICAS_CLIENTES_4_PAGE_QUERY = ESTADISTICAS_CLIENTES_4_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHestadisticasClientes4Dao
   */
  public static final String ESTADISTICAS_CLIENTES_4_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + ESTADISTICAS_CLIENTES_4_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes4Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_4_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListEstadisticasClientes4(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero
   * DWHestadisticasClientes4Dao sin paginación
   * @return
   */
  @Query(value = ESTADISTICAS_CLIENTES_4_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListEstadisticasClientes4();

}
