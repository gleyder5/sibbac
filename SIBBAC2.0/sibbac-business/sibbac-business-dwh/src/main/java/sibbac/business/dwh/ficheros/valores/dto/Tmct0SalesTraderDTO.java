package sibbac.business.dwh.ficheros.valores.dto;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The Class Tmct0SalesTraderDTO.
 */
public class Tmct0SalesTraderDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5270093962321653900L;

  /** The cdempleado. */
  private String cdempleado;

  /** The nbnombre. */
  private String nbnombre;

  /** The nbapellido 1. */
  private String nbapellido1;

  /** The nbapellido 2. */
  private String nbapellido2;

  /** The cdnacionalidad. */
  private String cdnacionalidad;

  /** The fechanacimiento. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fechanacimiento;

  /** The tipoidmifid. */
  private String tipoidmifid;

  /** The idmifid. */
  private String idmifid;

  /** The codigocorto. */
  private Integer codigocorto;

  /** The cdtipousuario. */
  private String cdtipousuario;

  /** The fhinicio. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhinicio;

  /** The fhfinal. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Date fhfinal;

  /** The fhaudit. */
  @JsonFormat(pattern = "dd/MM/yyyy")
  private Timestamp fhaudit;
  
  /** The cdusuaud. */
  private String cdusuaud;

  /** The producto. */
  private String producto;

  /** The cuenta. */
  private Integer cuenta;

  /** The nbcorto. */
  private String nbcorto;

  /** The indtrader. */
  private Integer indtrader;

  /** The indsale. */
  private Integer indsale;

  /** The indtradsale. */
  private Integer indtradsale;

  /** The indaccount. */
  private Integer indaccount;

  /**
   * Gets the serialversionuid.
   *
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }
  
  /**
   * Gets the cdempleado.
   *
   * @return the cdempleado
   */
  public String getCdempleado() {
    return cdempleado;
  }

  /**
   * Sets the cdempleado.
   *
   * @param cdempleado the new cdempleado
   */
  public void setCdempleado(String cdempleado) {
    this.cdempleado = cdempleado;
  }

  /**
   * Gets the nbnombre.
   *
   * @return the nbnombre
   */
  public String getNbnombre() {
    return nbnombre;
  }

  /**
   * Sets the nbnombre.
   *
   * @param nbnombre the new nbnombre
   */
  public void setNbnombre(String nbnombre) {
    this.nbnombre = nbnombre;
  }

  /**
   * Gets the nbapellido 1.
   *
   * @return the nbapellido 1
   */
  public String getNbapellido1() {
    return nbapellido1;
  }

  /**
   * Sets the nbapellido 1.
   *
   * @param nbapellido1 the new nbapellido 1
   */
  public void setNbapellido1(String nbapellido1) {
    this.nbapellido1 = nbapellido1;
  }

  /**
   * Gets the nbapellido 2.
   *
   * @return the nbapellido 2
   */
  public String getNbapellido2() {
    return nbapellido2;
  }

  /**
   * Sets the nbapellido 2.
   *
   * @param nbapellido2 the new nbapellido 2
   */
  public void setNbapellido2(String nbapellido2) {
    this.nbapellido2 = nbapellido2;
  }

  /**
   * Gets the cdnacionalidad.
   *
   * @return the cdnacionalidad
   */
  public String getCdnacionalidad() {
    return cdnacionalidad;
  }

  /**
   * Sets the cdnacionalidad.
   *
   * @param cdnacionalidad the new cdnacionalidad
   */
  public void setCdnacionalidad(String cdnacionalidad) {
    this.cdnacionalidad = cdnacionalidad;
  }

  /**
   * Gets the fechanacimiento.
   *
   * @return the fechanacimiento
   */
  public Date getFechanacimiento() {
    return fechanacimiento;
  }

  /**
   * Sets the fechanacimiento.
   *
   * @param fechanacimiento the new fechanacimiento
   */
  public void setFechanacimiento(Date fechanacimiento) {
    this.fechanacimiento = fechanacimiento;
  }

  /**
   * Gets the tipoidmifid.
   *
   * @return the tipoidmifid
   */
  public String getTipoidmifid() {
    return tipoidmifid;
  }

  /**
   * Sets the tipoidmifid.
   *
   * @param tipoidmifid the new tipoidmifid
   */
  public void setTipoidmifid(String tipoidmifid) {
    this.tipoidmifid = tipoidmifid;
  }

  /**
   * Gets the idmifid.
   *
   * @return the idmifid
   */
  public String getIdmifid() {
    return idmifid;
  }

  /**
   * Sets the idmifid.
   *
   * @param idmifid the new idmifid
   */
  public void setIdmifid(String idmifid) {
    this.idmifid = idmifid;
  }
  
  /**
   * Gets the codigocorto.
   *
   * @return the codigocorto
   */
  public Integer getCodigocorto() {
    return codigocorto;
  }
  
  /**
   * Sets the codigocorto.
   *
   * @param codigocorto the new codigocorto
   */
  public void setCodigocorto(Integer codigocorto) {
    this.codigocorto = codigocorto;
  }

  /**
   * Gets the cdtipousuario.
   *
   * @return the cdtipousuario
   */
  public String getCdtipousuario() {
    return cdtipousuario;
  }

  /**
   * Sets the cdtipousuario.
   *
   * @param cdtipousuario the new cdtipousuario
   */
  public void setCdtipousuario(String cdtipousuario) {
    this.cdtipousuario = cdtipousuario;
  }

  /**
   * Gets the fhinicio.
   *
   * @return the fhinicio
   */
  public Date getFhinicio() {
    return fhinicio;
  }

  /**
   * Sets the fhinicio.
   *
   * @param fhinicio the new fhinicio
   */
  public void setFhinicio(Date fhinicio) {
    this.fhinicio = fhinicio;
  }

  /**
   * Gets the fhfinal.
   *
   * @return the fhfinal
   */
  public Date getFhfinal() {
    return fhfinal;
  }

  /**
   * Sets the fhfinal.
   *
   * @param fhfinal the new fhfinal
   */
  public void setFhfinal(Date fhfinal) {
    this.fhfinal = fhfinal;
  }

  /**
   * Gets the fhaudit.
   *
   * @return the fhaudit
   */
  public Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * Sets the fhaudit.
   *
   * @param fhaudit the new fhaudit
   */
  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * Gets the cdusuaud.
   *
   * @return the cdusuaud
   */
  public String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * Sets the cdusuaud.
   *
   * @param cdusuaud the new cdusuaud
   */
  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }
  
  /**
   * Gets the producto.
   *
   * @return the producto
   */
  public String getProducto() {
    return producto;
  }

  /**
   * Sets the producto.
   *
   * @param producto the new producto
   */
  public void setProducto(String producto) {
    this.producto = producto;
  }

  /**
   * Gets the cuenta.
   *
   * @return the cuenta
   */
  public Integer getCuenta() {
    return cuenta;
  }

  /**
   * Sets the cuenta.
   *
   * @param cuenta the new cuenta
   */
  public void setCuenta(Integer cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * Gets the nbcorto.
   *
   * @return the nbcorto
   */
  public String getNbcorto() {
    return nbcorto;
  }

  /**
   * Sets the nbcorto.
   *
   * @param nbcorto the new nbcorto
   */
  public void setNbcorto(String nbcorto) {
    this.nbcorto = nbcorto;
  }

  /**
   * Gets the indtrader.
   *
   * @return the indtrader
   */
  public Integer getIndtrader() {
    return indtrader;
  }

  /**
   * Sets the indtrader.
   *
   * @param indtrader the new indtrader
   */
  public void setIndtrader(Integer indtrader) {
    this.indtrader = indtrader;
  }

  /**
   * Gets the indsale.
   *
   * @return the indsale
   */
  public Integer getIndsale() {
    return indsale;
  }

  /**
   * Sets the indsale.
   *
   * @param indsale the new indsale
   */
  public void setIndsale(Integer indsale) {
    this.indsale = indsale;
  }

  /**
   * Gets the indtradsale.
   *
   * @return the indtradsale
   */
  public Integer getIndtradsale() {
    return indtradsale;
  }

  /**
   * Sets the indtradsale.
   *
   * @param indtradsale the new indtradsale
   */
  public void setIndtradsale(Integer indtradsale) {
    this.indtradsale = indtradsale;
  }

  /**
   * Gets the indaccount.
   *
   * @return the indaccount
   */
  public Integer getIndaccount() {
    return indaccount;
  }

  /**
   * Sets the indaccount.
   *
   * @param indaccount the new indaccount
   */
  public void setIndaccount(Integer indaccount) {
    this.indaccount = indaccount;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "Tmct0SalesTraderDTO [cdempleado=" + this.cdempleado + ", nbnombre=" + this.nbnombre + ", nbapellido1=" + this.nbapellido1
        + ", nbapellido2=" + this.nbapellido2 + ", cdnacionalidad=" + this.cdnacionalidad + ", fechanacimiento=" + this.fechanacimiento + ", tipoidmifid="
        + this.tipoidmifid + ", idmifid=" + this.idmifid + ", codigocorto=" + this.codigocorto + ", cdtipousuario=" + this.cdtipousuario
        + ", fhinicio=" + this.fhinicio + ", fhfinal=" + this.fhfinal + ", fhaudit=" + this.fhaudit + ", cdusuaud=" + this.cdusuaud + ", producto=" 
        + this.producto + ", cuenta=" + this.cuenta + ", indtrader=" + this.indtrader + ", indsale=" + this.indsale + ", indtradsale=" + this.indtradsale 
        + ", indaccount=" + this.indaccount + "]";
  }
}
