package sibbac.business.dwh.web.rest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.FilterInfo;
import sibbac.database.bo.QueryBo;

public abstract class BaseService {

  protected static final String DEFAULT_CHARSET = CharEncoding.UTF_8;

  protected static final String DEFAULT_APPLICATION_TYPE = "application/json;charset=" + DEFAULT_CHARSET;

  @Autowired
  protected ApplicationContext ctx;

  @Autowired
  protected QueryBo queryBo;

  protected QueryInfo[] queryInfo;

  /**
   * Constructor por parámetros
   * @param descripcion
   * @param accion
   * @param nombreClase
   */
  protected BaseService(String descripcion, String accion, String nombreClase) {
    this.queryInfo = new QueryInfo[] { new QueryInfo(descripcion, accion, nombreClase) };
  }

  /**
   * Contructor con varios QueryInfo
   * @param queryInfo
   */
  protected BaseService(QueryInfo... queryInfo) {
    this.queryInfo = queryInfo;
  }

  /**
   * 
   * @param servletResponse
   * @throws IOException
   */
  @RequestMapping(produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public void getQueries(HttpServletResponse servletResponse) throws IOException {
    servletResponse.setCharacterEncoding(DEFAULT_CHARSET);
    servletResponse.setContentType(DEFAULT_APPLICATION_TYPE);
    try (JsonGenerator writer = new JsonFactory().createGenerator(servletResponse.getWriter())) {
      writer.writeStartArray();

      for (QueryInfo item : queryInfo) {
        writeQuery(writer, item.getNombreClase(), item.getDescripcion(), item.getAccion());
      }

      writer.writeEndArray();
    }
  }

  /**
   * 
   * @param name
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/{name}/filters", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<FilterInfo> getFilters(@PathVariable("name") String name) throws SIBBACBusinessException {
    final AbstractDynamicPageQuery query;

    query = getDynamicQuery(name);
    return query.getFiltersInfo();
  }

  /**
   * 
   * @param name
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/{name}/columns", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<DynamicColumn> getColumns(@PathVariable("name") String name) throws SIBBACBusinessException {
    try {
      final AbstractDynamicPageQuery query = getDynamicQuery(name);
      return query.getColumns();
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error obteniendo columnas", e);
    }
  }

  /**
   * 
   * @param name
   * @param request
   * @param servletResponse
   * @throws Exception
   */
  @RequestMapping(value = "/{name}", method = RequestMethod.GET)
  public void executeQuery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws SIBBACBusinessException {
    try {
      final AbstractDynamicPageQuery adpq;
      final StreamResult streamResult;

      adpq = getFilledDynamicQuery(name, request.getParameterMap());
      adpq.setIsoFormat(true);
      streamResult = new HttpStreamResultHelper(servletResponse);
      queryBo.executeQuery(adpq, FormatStyle.JSON_OBJECTS, streamResult);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error ejecutando Query", e);
    }
  }

  /**
   * 
   * @param writer
   * @param className
   * @param desc
   * @param accion
   * @throws IOException
   */
  private void writeQuery(JsonGenerator writer, String className, String desc, String accion) throws IOException {
    writer.writeStartObject();
    writer.writeFieldName("name");
    writer.writeString(className);
    writer.writeFieldName("desc");
    writer.writeString(desc);
    writer.writeFieldName("accion");
    writer.writeString(accion);
    writer.writeEndObject();
  }

  /**
   * 
   * @param name
   * @return
   * @throws Exception
   */
  private AbstractDynamicPageQuery getDynamicQuery(String name) throws SIBBACBusinessException {
    return ctx.getBean(name, AbstractDynamicPageQuery.class);
  }

  /**
   * 
   * @param name
   * @param params
   * @return
   * @throws Exception
   */
  private AbstractDynamicPageQuery getFilledDynamicQuery(String name, Map<String, String[]> params)
      throws SIBBACBusinessException {
    try {

      final AbstractDynamicPageQuery adpq;

      adpq = getDynamicQuery(name);
      adpq.fillDynamicQuery(params);

      return adpq;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error obteniendo Query dinámicamente rellenada", e);
    }
  }

  /**
   * Define las quieries que se van a ejecutar
   * @author ipalacio
   *
   */
  protected static class QueryInfo {

    protected String descripcion;

    protected String accion;

    protected String nombreClase;

    public QueryInfo(String descripcion, String accion, String nombreClase) {
      super();
      this.descripcion = descripcion;
      this.accion = accion;
      this.nombreClase = nombreClase;
    }

    public String getDescripcion() {
      return descripcion;
    }

    public void setDescripcion(String descripcion) {
      this.descripcion = descripcion;
    }

    public String getAccion() {
      return accion;
    }

    public void setAccion(String accion) {
      this.accion = accion;
    }

    public String getNombreClase() {
      return nombreClase;
    }

    public void setNombreClase(String nombreClase) {
      this.nombreClase = nombreClase;
    }
  }

}
