package sibbac.business.dwh.tasks.commons;

import java.util.List;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.common.SIBBACBusinessException;

public interface TaskProcessDWH {

  List<ProcessFileResultDTO> getProcessFileResultsList();

  void launchProcess() throws SIBBACBusinessException;

}
