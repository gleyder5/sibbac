package sibbac.business.dwh.ficheros.valores.bo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.PaisDao;
import sibbac.business.dwh.ficheros.valores.db.model.Pais;
import sibbac.business.dwh.ficheros.valores.db.model.PaisPK;
import sibbac.business.dwh.ficheros.valores.dto.PaisDTO;
import sibbac.database.bo.AbstractBo;

/**
 * The Class PaisBo.
 */
@Service
public class PaisBo extends AbstractBo<Pais, PaisPK, PaisDao> {

  /**
   * Gets the cdisonum and nbpais.
   *
   * @return the cdisonum and nbpais
   */
  public List<PaisDTO> getCdisonumAndNbpais() {
    List<Object[]> cdisonumAndNbpais = dao.getCdisonumAndNbpaisa();

    List<PaisDTO> auxList = new ArrayList<>();
    Iterator<Object[]> it = cdisonumAndNbpais.iterator();

    while (it.hasNext()) {
      Object[] next = it.next();
      PaisDTO aux = new PaisDTO();
      aux.setCdisonum(next[0].toString());
      aux.setNbpais(next[1].toString());
      auxList.add(aux);
    }

    return auxList;
  }

  /**
   * Validate paises.
   *
   * @return the list
   */
  public List<String> validatePaises() {
    List<Object> countryList = dao.validatePaises();

    List<String> auxList = new ArrayList<>();
    Iterator<Object> it = countryList.iterator();

    while (it.hasNext()) {
      auxList.add((String) it.next());
    }

    return auxList;
  }

  /**
   * Checks if is valid country.
   *
   * @param country the country
   * @return the object
   */
  public List<Object> isValidCountry(String country) {
    List<Object> countryList = dao.isValidCountry(country);

    return countryList;
  }
}
