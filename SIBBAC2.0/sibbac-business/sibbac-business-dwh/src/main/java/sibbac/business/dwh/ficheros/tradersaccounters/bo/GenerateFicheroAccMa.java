package sibbac.business.dwh.ficheros.tradersaccounters.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.commons.dto.ProcessResultDTO;
import sibbac.business.dwh.ficheros.tradersaccounters.db.dao.FicheroAccMaDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.web.util.Constantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateFicheroAccMa")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenerateFicheroAccMa extends AbstractGenerateFile {

  private static final String FILE_ACC_MA_LINE_SEPARATOR = "\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateFicheroAccMa.class);

  /** Lógica a realizar sobre la BBDD. */
  @Autowired
  private FicheroAccMaDao ficheroAccMaDao;

  /** Metódo que se ejecutará en el hilo */
  public ProcessResultDTO generateAccountManagementFile() throws Exception {
    final ProcessResultDTO processResultDto = new ProcessResultDTO();

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza el proceso de Generate Account Management File");
    }

    if (getFileCountData() > 0) {
      generateFile();
      processResultDto.setResult(true);
      processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_ACCOUNT_OK.value());
      if (LOG.isDebugEnabled()) {
        LOG.debug(Constantes.ApplicationMessages.GENERACION_ACCOUNT_OK.value());
      }
    }
    else {
      processResultDto.setResult(false);
      processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_ACCOUNT_ERROR.value()
          + Constantes.ApplicationMessages.GENERACION_ERROR_PART2.value());
      if (LOG.isDebugEnabled()) {
        LOG.debug(Constantes.ApplicationMessages.GENERACION_ACCOUNT_ERROR.value()
            + Constantes.ApplicationMessages.GENERACION_ERROR_PART2.value());
      }
    }

    return processResultDto;
  }

  @Override
  public String getLineSeparator() {
    return FILE_ACC_MA_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return ficheroAccMaDao.findAllAccMa(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return ficheroAccMaDao.countAllAccMa();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    final StringBuilder rowLine = new StringBuilder();

    final String msgProductoCuenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_8.value(),
        DwhConstantes.TRADERS_9);
    final String msgNombre = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_50.value(),
        DwhConstantes.TRADERS_DEFINIR);
    final String msgNombreCorto = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(),
        DwhConstantes.TRADERS_X);

    rowLine.append(msgProductoCuenta).append(msgNombre).append(msgNombreCorto);

    String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

    writeLine(outWriter, cadena);
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Se inicia la escritura en Account Management file");
    }
    for (Object[] objects : fileData) {
      StringBuilder rowLine = new StringBuilder();
      for (int i = 0; i < objects.length; i++) {
        if (objects[i] == null) {
          objects[i] = "";
        }
      }

      String productoCuenta = ((String) objects[0]).trim();
      if (!objects[1].equals("")) {
        BigDecimal cuenta = (BigDecimal) objects[1];
        productoCuenta += String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
            cuenta.toBigInteger().intValue());
      }
      String nombreCorto = (String) objects[2];
      String nombre = (String) objects[3];

      String msgProductoCuenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_8.value(), productoCuenta);
      String msgNombre = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_50.value(), nombre);
      String msgNombreCorto = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(), nombreCorto);

      rowLine.append(msgProductoCuenta).append(msgNombre).append(msgNombreCorto);

      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);
    }
    if (LOG.isDebugEnabled()) {
      LOG.debug("Finaliza la escritura en Account Management file");
    }
  }

}
