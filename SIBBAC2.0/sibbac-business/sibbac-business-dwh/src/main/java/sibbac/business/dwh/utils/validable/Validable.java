package sibbac.business.dwh.utils.validable;

import sibbac.common.SIBBACBusinessException;

/**
 * Design pattern command interface
 * @author NEORIS
 *
 */
public interface Validable {

	/**
	 * Validate command executor
	 * @throws SIBBACBusinessException 
	 */
	public void validate() throws SIBBACBusinessException;
}
