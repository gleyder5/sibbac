  package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.io.BufferedWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.nacional.bo.FicheroNacionalBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableNacionalInstitucionalesLineBo extends AbstractCallableProcessBo {

  @Autowired
  private FicheroNacionalBo ficheroNacionalBo;

  BufferedWriter writer = null;

  private List<Object[]> institucionalesList;

  private static final Logger LOG = LoggerFactory.getLogger(CallableNacionalInstitucionalesLineBo.class);

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {

    try {
      long initTimeStampAlta = System.currentTimeMillis();

      ficheroNacionalBo.writeInstitucionalesLines(writer, institucionalesList);

      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("TERMINADO escribir fichero institucionales en %d milisegundos",
            (System.currentTimeMillis() - initTimeStampAlta)));
      }
    }
    catch (Exception e) {
    }
    return true;
  }

  public BufferedWriter getWriter() {
    return writer;
  }

  public void setWriter(BufferedWriter writer) {
    this.writer = writer;
  }

  public List<Object[]> getInstitucionalesList() {
    return institucionalesList;
  }

  public void setInstitucionalesList(List<Object[]> institucionalesList) {
    this.institucionalesList = institucionalesList;
  }

}
