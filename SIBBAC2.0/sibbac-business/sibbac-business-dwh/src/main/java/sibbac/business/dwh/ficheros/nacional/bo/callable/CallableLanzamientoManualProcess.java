package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableListProcessFileResult;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.commons.bo.TestigoFileBo;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.nacional.bo.LanzamientoManualProcess;
import sibbac.business.dwh.tasks.TaskGeneracionDWHEjecuciones;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;

@Component
public class CallableLanzamientoManualProcess implements CallableListProcessFileResult {

  protected static final Logger LOG = LoggerFactory.getLogger(CallableLanzamientoManualProcess.class);

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${dwh.ficheros.nacional.batch.timeout.global: 90}")
  private int timeout;

  @Autowired
  private SendMail sendMail;

  @Autowired
  private TaskGeneracionDWHEjecuciones taskGeneracionDWHEjecuciones;

  @Autowired
  private TestigoFileBo testigoFileBo;

  private Date fechaEjecucion;

  private String userSession;

  private Boolean sendMailOnFinish = false;

  private Future<List<ProcessFileResultDTO>> future;

  private LanzamientoManualProcess parent;

  private boolean isRunning;

  @Autowired
  Tmct0UsersBo tmct0UsersBo;

  Boolean res = true;

  @Autowired
  private ApplicationContext ctx;

  @Override
  public List<ProcessFileResultDTO> call() throws Exception {
    List<ProcessFileResultDTO> processResultDto = null;

    sendMailOnFinish = false;

    long initTime = System.currentTimeMillis();

    if (LOG.isDebugEnabled()) {
      LOG.info("Se inicia la tarea de Generación de Ejecuciones Nacional");
    }

    // 1. Creamos el mensaje - Iniciamos los procesos - Este proceso es un Hilo
    processResultDto = launchEjecucionesProcess();

    // Generamos el testigo que nos certifique si se han creado todos los
    // ficheros
    final File generatedFile = testigoFileBo.generateTestigoFile(processResultDto);
    if (generatedFile != null) {
      LOG.debug("Fichero testigo generado es '" + generatedFile.getAbsolutePath() + "'");
    }
    else {
      LOG.debug("Fichero testigo no ha sido generado");
    }

    if (parent.getIsWaitting()) {
      LOG.debug("El proceso padre está en espera, notificamos su wakeup");
      parent.wakeUpProcess();
    }

    if (sendMailOnFinish) {
      LOG.debug("Iniciamos el envio del mail al usuario que lo peticiono");
      sendFinishMail(processResultDto);
    }

    if (LOG.isDebugEnabled()) {
      LOG.info(String.format("Se finaliza la tarea de Ejecucion Nacional, Tiempo de ejecucion %d milisegundos",
          System.currentTimeMillis() - initTime));
    }

    return processResultDto;
  }

  public List<ProcessFileResultDTO> launchEjecucionesProcess()
      throws InterruptedException, SIBBACBusinessException, ExecutionException {
    isRunning = true;

    final List<ProcessFileResultDTO> listProcessResultDTO = new ArrayList<>();

    taskGeneracionDWHEjecuciones.setFechaEjecucion(fechaEjecucion);

    listProcessResultDTO.addAll(taskGeneracionDWHEjecuciones.launchGeneracionEjecuciones());

    listProcessResultDTO.addAll(launchClienteValores());

    isRunning = false;

    return listProcessResultDTO;
  }

  private List<ProcessFileResultDTO> launchClienteValores()
      throws InterruptedException, SIBBACBusinessException, ExecutionException {
    final List<ProcessFileResultDTO> result = new ArrayList<>();

    final List<CallableListProcessFileResult> futureList = new ArrayList<>();

    try {
      final CallableGeneracionClientesBo callableGeneracionClientesBo = ctx.getBean(CallableGeneracionClientesBo.class);
      final CallableGeneracionValoresBo callableGeneracionValoresBo = ctx.getBean(CallableGeneracionValoresBo.class);

      final ExecutorService executor = Executors.newFixedThreadPool(2,
          new NamedThreadFactory("clienteValores_Lanzamiento Manual"));

      callableGeneracionClientesBo.setFuture(executor.submit(callableGeneracionClientesBo));
      callableGeneracionValoresBo.setFuture(executor.submit(callableGeneracionValoresBo));

      futureList.add(callableGeneracionClientesBo);
      futureList.add(callableGeneracionValoresBo);

      // Hacemos que se ejecuten siempre en paralelo n threadas

      executor.shutdown();

      // Return true si executor ha terminado
      final boolean onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

      // Si se ha superado el tiempo estipulado para la ejecucion de los
      // ficheros cancelamos los hilos
      if (onTime) {
        for (CallableListProcessFileResult processInterface : futureList) {
          result.addAll(processInterface.getFuture().get());
        }
      }
      else {
        // Si se ha superado el tiempo permitido Cancelamos los procesos que
        // están en ejecución
        for (CallableListProcessFileResult processInterface : futureList) {
          if (!processInterface.getFuture().isDone()) {
            processInterface.getFuture().cancel(true);
          }
        }
      }
    }
    catch (InterruptedException | ExecutionException exception) {
      LOG.error("[sendBatch] Se ha interrumpido el hilo en espera con mensaje {}", exception.getMessage());
    }

    return result;

  }

  /**
   * Enviamos el mail
   * @param processResultDto
   */
  private void sendFinishMail(List<ProcessFileResultDTO> listProcessResultDto) {
    final String email = tmct0UsersBo.getByUsername(userSession).getEmail();
    final List<String> emails = new ArrayList<String>();

    emails.add(email);
    StringBuilder body = new StringBuilder(
        FormatDataUtils.toUTF8("La tarea de lanzamiento manual ha terminado correctamente."));

    body.append("Gracias.");

    try {
      sendMail.setUtf8(true);
      sendMail.sendMailHtml(emails, FormatDataUtils.toUTF8("Lanzamiento Manual"), body.toString(), null, null, null);
    }
    catch (IllegalArgumentException | MessagingException | IOException e) {
      LOG.error("Se ha producido un error en el envío del mail", e);
    }

    sendMailOnFinish = false;
  }

  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  public String getUserSession() {
    return userSession;
  }

  public void setUserSession(String userSession) {
    this.userSession = userSession;
  }

  public Boolean getSendMailOnFinish() {
    return sendMailOnFinish;
  }

  public void setSendMailOnFinish(Boolean sendMailOnFinish) {
    this.sendMailOnFinish = sendMailOnFinish;
  }

  public Future<List<ProcessFileResultDTO>> getFuture() {
    return future;
  }

  public void setFuture(Future<List<ProcessFileResultDTO>> future) {
    this.future = future;
  }

  public LanzamientoManualProcess getParent() {
    return parent;
  }

  public void setParent(LanzamientoManualProcess parent) {
    this.parent = parent;
  }

  public boolean isRunning() {
    return isRunning;
  }

  public void setRunning(boolean isRunning) {
    this.isRunning = isRunning;
  }

}
