package sibbac.business.dwh.ficheros.clientes.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.clientes.db.model.Tmct0DwhClientes;

@Repository
public interface DWHGrupoClientesEstaticosDao extends JpaRepository<Tmct0DwhClientes, String> {

  /**
   * Query para DWHgrupoClientesEstaticosDao, ORIGINAL
   */
  public static final String GRUPO_CLIENTES_ESTATICOS_QUERY = "SELECT CDGRUPOO, NBGRUPOO, NUNIVEL0, NBNIVEL0,NUNIVEL1, "
      + "NBNIVEL1, NUNIVEL2, NBNIVEL2,NUNIVEL3,NBNIVEL3, NUNIVEL4, NBNIVEL4 "
      + "FROM TMCT0_DWH_CLIENTES GROUP BY CDGRUPOO, NBGRUPOO, NUNIVEL0, NBNIVEL0,NUNIVEL1,NBNIVEL1, "
      + "NUNIVEL2, NBNIVEL2,NUNIVEL3, NBNIVEL3, NUNIVEL4,NBNIVEL4";

  /**
   * Query para DWHgrupoClientesEstaticosDao con Paginación
   */
  public static final String GRUPO_CLIENTES_ESTATICOS_PAGE_QUERY = GRUPO_CLIENTES_ESTATICOS_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHgrupoClientesEstaticosDao
   */
  public static final String GRUPO_CLIENTES_ESTATICOS_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + GRUPO_CLIENTES_ESTATICOS_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero
   * DWHgrupoClientesEstaticosDao sin paginación
   * @return
   */
  @Query(value = GRUPO_CLIENTES_ESTATICOS_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListGrupoClientesEstaticos(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero
   * DWHgrupoClientesEstaticosDao sin paginación
   * @return
   */
  @Query(value = GRUPO_CLIENTES_ESTATICOS_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListGrupoClientesEstaticos();
}
