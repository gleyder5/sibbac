package sibbac.business.dwh.ficheros.ejecuciones.bo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.ejecuciones.dto.ClienteDwhDTO;
import sibbac.business.dwh.ficheros.ejecuciones.dto.DesgloseDwhDTO;
import sibbac.business.dwh.ficheros.ejecuciones.dto.OperacionDwhDTO;
import sibbac.business.dwh.unbundled.db.dao.UnbundledDao;
import sibbac.business.dwh.unbundled.db.model.Unbundled;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.constants.FileResult;

@Component
public class FicheroEjecucionesBo {

  @Autowired
  private UnbundledDao unbundledDao;

  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String filePath;

  @Value("${dwh.ficheros.ejecuciones.ejecuciones.file:fdwh0e97.txt}")
  private String fileNameEjecuciones;

  @Value("${dwh.ficheros.ejecuciones.generated_if_empty:true}")
  private Boolean generateIfEmpty;

  private static final Charset ENCODING = StandardCharsets.UTF_8;
  private static final String MAP_NUOPROUT_CDALIAS = "23573";
  private static final String MAP_NUOPROUT_NUCNFCLT_PREFIX = "PARTENON%";
  private static final String MAP_NUOPROUT_CONCAT_PREFIX = "10000";
  private static final String MAP_NUOPROUT_CONCAT_SUFIX = "0001";

  private static final String MAP_CUENTA_PREFIX = "008";

  private static final String MAP_BOLSA_M = "9838";
  private static final String MAP_BOLSA_V = "9370";
  private static final String MAP_BOLSA_B = "9472";
  private static final String MAP_BOLSA_L = "9577";
  private static final String MAP_BOLSA_ME = "CHIX";
  private static final String MAP_BOLSA_BE = "BATE";
  private static final String MAP_BOLSA_TE = "TQX";

  private static final String MAP_TPMERCAD = "C";
  private static final String MAP_TPCONTRA = "T";
  private static final String MAP_CDOPEMER = "RDMA     ";
  private static final String MAP_CDPROCED = "D";
  private static final String MAP_NUEJECUC = "0000000000000010000";
  private static final String MAP_TPCARGAS = "C";

  private static final String MAP_CUENTA_CW = "014";
  private static final String MAP_CUENTA_CV_EO = "008";
  private static final String MAP_CUENTA_DEFAULT = "012";

  private static final String MAP_ORIGEN_FI = "FIX";
  private static final String MAP_ORIGEN_OTHER = "ROUTING";
  private static final String MAP_ORIGEN_DEFAULT = "F  ";
  private static final String MAP_ORIGEN_RI = "4716";
  private static final String MAP_ORIGEN_RK = "22672";
  private static final String MAP_ORIGEN_RB = "22645";

  private static final String MAP_UNBUNDLED_NUMPAREJE = "1";
  private static final String MAP_UNBUNDLED_DEFAULT_NUMBER = "0";
  private static final String MAP_UNBUNDLED_DEFAULT_STRING = " ";
  private static final String MAP_UNBUNDLED_CDENTLIQ = "38";
  private static final String MAP_UNBUNDLED_CDISINVV = "ES0UNBLUNDED";
  private static final String MAP_UNBUNDLED_BOLSA = "M";
  private static final String MAP_UNBUNDLED_TPOPERAC = "C";
  private static final String MAP_UNBUNDLED_TPMERCAD = "C";
  private static final String MAP_UNBUNDLED_TEJERCI = "CV";
  private static final String MAP_UNBUNDLED_TPEJECUC = "V";
  private static final String MAP_UNBUNDLED_TPOPESTD = "N";
  private static final String MAP_UNBUNDLED_TPCONTRA = "T";
  private static final String MAP_UNBUNDLED_CDDERECH = "S";
  private static final String MAP_UNBUNDLED_CDPROCED = "D";
  private static final String MAP_UNBUNDLED_NUEJECUC = "0000000000000010000";
  private static final String MAP_UNBUNDLED_SUBMER_ID = "ZZ";
  private static final String MAP_UNBUNDLED_CDORIGEN = "ZZZ";

  protected static final Logger LOG = LoggerFactory.getLogger(FicheroEjecucionesBo.class);

  public FileResult generateFileEjecucionesbyType(String type) throws SIBBACBusinessException {
    FileResult fileResult = null;

    final Date fechaHasta = new Date(System.currentTimeMillis());
    GenerateFilesUtilDwh.createOutPutDir(filePath);

    switch (type) {
      case "SEMANA":
        LOG.info("[GenerarFicherosTraderAccounts :: executeTask] inicio generacion fichero ejecuciones semanas");
        fileResult = generateFileEjecuciones(getDateBySubstractDays(fechaHasta, 7), fechaHasta);
        LOG.info("[GenerarFicherosTraderAccounts :: executeTask] fin generacion fichero ejecuciones semanas");
        break;
      case "MESES":
        LOG.info("[GenerarFicherosTraderAccounts :: executeTask] inicio generacion fichero ejecuciones meses");
        fileResult = generateFileEjecuciones(getStartMonthBySubstractMonth(fechaHasta, 3), fechaHasta);
        LOG.info("[GenerarFicherosTraderAccounts :: executeTask] fin generacion fichero ejecuciones meses");
        break;
      default:
        throw new SIBBACBusinessException("El tipo de ejecucion especificado no esta contemplado");
    }

    return fileResult;
  }

  private FileResult generateFileEjecuciones(Date fechaDesde, Date fechaHasta) throws SIBBACBusinessException {
    FileResult fileResult = null;

    final StringBuilder fileName = new StringBuilder();
    fileName.append(filePath).append(File.separator).append(fileNameEjecuciones);
    Path path = Paths.get(FilenameUtils.getFullPath(fileName.toString()), FilenameUtils.getName(fileName.toString()));

    final List<OperacionDwhDTO> operaciones = getOperacionesDwh(fechaDesde, fechaHasta);
    final List<Unbundled> operacionesUnbundled = unbundledDao.findbyDate(fechaDesde, fechaHasta);

    if (!operaciones.isEmpty() || !operacionesUnbundled.isEmpty()) {
      try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
        for (OperacionDwhDTO operac : operaciones) {
          String line = getLineExecution(operac);
          writer.append(line);
          writer.newLine();
        }
        for (Unbundled operac : operacionesUnbundled) {
          String line = mapLineExecutionUnbundled(operac);
          writer.append(line);
          writer.newLine();
        }

        fileResult = FileResult.GENETARATED_OK;
      }
      catch (IOException | SIBBACBusinessException e) {
        fileResult = FileResult.NOT_GENERATED;
        LOG.error("Error al generar el fichero de Ejecuciones de nacional", e.getMessage(), e);
        throw new SIBBACBusinessException("Error al generar el fichero de Ejecuciones de nacional", e);
      }
    }
    else {
      fileResult = FileResult.GENETARATED_EMPTY;
      LOG.info("No se han encontrado operaciones ni FoperacionesUnbundled activos");
    }

    return fileResult;
  }

  private String getLineExecution(OperacionDwhDTO operacion) throws SIBBACBusinessException {
    ClienteDwhDTO cliente;
    DesgloseDwhDTO desglose;
    List<Object[]> cli = unbundledDao.findClienteNacional(operacion.getNuorden(), operacion.getNbooking(),
        operacion.getNucnfclt(), operacion.getNucnfliq());
    if (!cli.isEmpty()) {
      cliente = ClienteDwhDTO.mapObject(cli.get(0));
    }
    else {
      cliente = ClienteDwhDTO.initialize();
      LOG.info(String.format(
          "No se ha encontrado cliente para esta operacion: nuorden:%s, nbooking: %s,nucnfclt: %s, nucnfliq: %s",
          operacion.getNuorden(), operacion.getNbooking(), operacion.getNucnfclt(), operacion.getNucnfliq()));

    }
    List<Object[]> des = unbundledDao.findDesgloseNacional(operacion.getNuorden(), operacion.getNbooking(),
        operacion.getNucnfclt(), operacion.getNucnfliq());
    if (!des.isEmpty()) {
      desglose = DesgloseDwhDTO.mapObject(des.get(0));
    }
    else {
      desglose = DesgloseDwhDTO.mapObject(des.get(0));
      LOG.info(String.format(
          "No se ha encontrado desglose para esta operacion: nuorden:%s, nbooking: %s,nucnfclt: %s, nucnfliq: %s",
          operacion.getNuorden(), operacion.getNbooking(), operacion.getNucnfclt(), operacion.getNucnfliq()));
    }

    String lineResult = mapLineEjecucion(operacion, cliente, desglose);

    return lineResult;
  }

  private String mapLineEjecucion(OperacionDwhDTO operacion, ClienteDwhDTO cliente, DesgloseDwhDTO desglose) {
    StringBuilder line = new StringBuilder();
    line.append(getNuoproutAndNupareje(operacion.getCdAlias(), operacion.getNucnfclt(), desglose.getNucnfclt(),
        operacion.getNbooking(), operacion.getContRegistro()));
    line.append(getCuentaLocal(operacion.getCdAlias())).append("");
    line.append(operacion.getCdentliq());
    line.append(operacion.getCdIsin());
    line.append(getBolsaId(desglose.getCdmiembromkt(), desglose.getCdPlataformaNegociacion()));
    line.append(operacion.getCdtpoper());
    line.append(MAP_TPMERCAD);
    line.append(desglose.getTpopebol());
    line.append(("CW".equals(desglose.getCdsegmento()) ? "F" : "V"));
    line.append(("CV".equals(desglose.getTpopebol()) || "EO".equals(desglose.getTpopebol())) ? "N" : "E");
    line.append(MAP_TPCONTRA);
    line.append(MAP_CDOPEMER);
    line.append((operacion.getCanoncontrpagoclte() == 1) ? "S" : "N");
    line.append(MAP_CDPROCED);
    line.append(GenerateFilesUtilDwh.formatDate("dd/MM/yyyy", operacion.getFeejeliq()));
    line.append(String.format("%s.%s", GenerateFilesUtilDwh.getZeros(13), GenerateFilesUtilDwh.getZeros(4)));
    line.append(GenerateFilesUtilDwh.formatDecimal(8, 4, desglose.getImprecio()));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, desglose.getTotalImefectivo()));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, desglose.getTotalImtitulos()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(14, 2, desglose.getTotalImcanoncontreu()));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, desglose.getTotalImcanoncompeu()));
    line.append(getImcomField(operacion.getImcomsvb(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(getImcomField(operacion.getImcombco(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(getImcomField(operacion.getImcomdvo(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(getImdevbrk(operacion.getStlev4lm(), operacion.getImcomdvo(), desglose.getTotalImtitulos(),
        operacion.getNutitliq()));
    line.append(getImcomField(operacion.getImcomsis(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(getImcomField(operacion.getImcombrk(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(GenerateFilesUtilDwh.formatString(
        String.format("%s%s%s", cliente.getNbclient(), cliente.getNbclient1(), cliente.getNbclient2()), 50));
    line.append(MAP_NUEJECUC);
    line.append(GenerateFilesUtilDwh.formatDate("yyyy", operacion.getFeejeliq()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(MAP_TPCARGAS);
    line.append(getCuentaID(desglose.getCdsegmento()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2,
        (operacion.getCanoncontrpagoclte() == 0) ? desglose.getTotalImcanoncontreu() : BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2,
        (operacion.getCanoncontrpagoclte() == 0) ? desglose.getTotalImcanoncompeu() : BigDecimal.ZERO));
    line.append(getImcomField(operacion.getImcombco(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(getImcomField(operacion.getImbansis(), desglose.getTotalImtitulos(), operacion.getNutitliq()));
    line.append(operacion.getCdCanal());
    line.append(desglose.getCdsegmento());
    line.append(getOrigen(operacion.getCdOrigen(), operacion.getCdAlias()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(9, 2, BigDecimal.ZERO));

    return line.toString();
  }

  private String mapLineExecutionUnbundled(Unbundled operacion) {

    StringBuilder line = new StringBuilder();

    line.append(operacion.getNumOper());
    line.append(GenerateFilesUtilDwh.formatNumber(MAP_UNBUNDLED_NUMPAREJE, 4));
    line.append(GenerateFilesUtilDwh.formatNumber(MAP_UNBUNDLED_DEFAULT_NUMBER, 8));
    line.append(GenerateFilesUtilDwh.formatNumber(MAP_UNBUNDLED_CDENTLIQ, 4));
    line.append(MAP_UNBUNDLED_CDISINVV);
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_BOLSA, 3));
    line.append(MAP_UNBUNDLED_TPOPERAC);
    line.append(MAP_UNBUNDLED_TPMERCAD);
    line.append(MAP_UNBUNDLED_TEJERCI);
    line.append(MAP_UNBUNDLED_TPEJECUC);
    line.append(MAP_UNBUNDLED_TPOPESTD);
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_TPCONTRA, 9));
    line.append(MAP_UNBUNDLED_CDDERECH);
    line.append(MAP_UNBUNDLED_CDPROCED);
    line.append(GenerateFilesUtilDwh.formatDate("dd/MM/yyyy", operacion.getFechaPago()));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(10, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(14, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, operacion.getComision()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatString(operacion.getCliente(), 50));
    line.append(MAP_UNBUNDLED_NUEJECUC);
    line.append(GenerateFilesUtilDwh.formatDate("yyyy", operacion.getFechaPago()));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_DEFAULT_STRING, 1));
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_DEFAULT_STRING, 5));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimal(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO));
    line.append(GenerateFilesUtilDwh.formatString(MAP_UNBUNDLED_DEFAULT_STRING, 3));
    line.append(MAP_UNBUNDLED_SUBMER_ID);
    line.append(MAP_UNBUNDLED_CDORIGEN);
    line.append(GenerateFilesUtilDwh.formatDecimalWithSign(9, 2, BigDecimal.ZERO));

    return line.toString();
  }

  private String getNuoproutAndNupareje(String cdAlias, String nucnfclt, String nucnfcltDesglose, String nbooking,
      BigDecimal contRegistro) {
    StringBuilder field = new StringBuilder();
    if (MAP_NUOPROUT_CDALIAS.equals(cdAlias) || nucnfclt.startsWith(MAP_NUOPROUT_NUCNFCLT_PREFIX)) {
      field.append(MAP_NUOPROUT_CONCAT_PREFIX);
      field.append(nucnfcltDesglose.substring(9, 19));
      field.append(MAP_NUOPROUT_CONCAT_SUFIX);
    }
    else {
      field.append("0000");
      field.append(nbooking.substring(1, 12));
      String contRegistroCad = String.valueOf(contRegistro);
      field.append(GenerateFilesUtilDwh.formatString(contRegistroCad, 4));
      // TODO: Contador de registro por NBOOKING
    }
    return field.toString();
  }

  private String getCuentaLocal(String cdAlias) {
    StringBuilder field = new StringBuilder();
    field.append(MAP_CUENTA_PREFIX).append(GenerateFilesUtilDwh.formatNumber(cdAlias, 1));
    return field.toString();
  }

  private String getBolsaId(String cdmiembromkt, String cdPlataformaNegociacion) {
    switch (cdmiembromkt) {
      case MAP_BOLSA_M:
        return "M  ";
      case MAP_BOLSA_V:
        return "V  ";
      case MAP_BOLSA_B:
        return "B  ";
      case MAP_BOLSA_L:
        return "L  ";
      default:
        return getBolsaIdbycdPlat(cdPlataformaNegociacion);

    }
  }

  private String getBolsaIdbycdPlat(String cdPlataformaNegociacion) {
    switch (cdPlataformaNegociacion) {
      case MAP_BOLSA_ME:
        return "ME ";
      case MAP_BOLSA_BE:
        return "BE ";
      case MAP_BOLSA_TE:
        return "TE ";
      default:
        return "   ";
    }
  }

  private String getImcomField(BigDecimal imcom, BigDecimal titulos, BigDecimal nutit) {
    BigDecimal result = imcom.multiply(titulos.add(nutit)).abs();

    return GenerateFilesUtilDwh.formatDecimal(15, 2, result);
  }

  private List<OperacionDwhDTO> getOperacionesDwh(Date fechaDesde, Date fechaHasta) {
    List<Object[]> operations = unbundledDao.findOperacionesNacional(fechaHasta, fechaDesde);
    List<OperacionDwhDTO> operaciones = new ArrayList<>();
    for (Object[] op : operations) {

      operaciones.add(OperacionDwhDTO.mapObject(op));
    }

    return operaciones;
  }

  private String getImdevbrk(String stlev4lm, BigDecimal imcomdvo, BigDecimal totalImtitulos, BigDecimal nutitliq) {

    switch (stlev4lm) {
      case "50":
      case "455":
      case "855":
      case "1110":
      case "1570":
        return getImcomField(imcomdvo, totalImtitulos, nutitliq);
      default:
        return GenerateFilesUtilDwh.formatDecimalWithSign(15, 2, BigDecimal.ZERO);

    }
  }

  private Object getOrigen(String cdOrigen, String cdAlias) {
    switch (cdOrigen) {
      case MAP_ORIGEN_FI:
        return "FI ";
      case MAP_ORIGEN_OTHER:
        return getOrigenbycdAlias(cdAlias);
      default:
        return MAP_ORIGEN_DEFAULT;

    }
  }

  private String getOrigenbycdAlias(String cdAlias) {
    switch (cdAlias) {
      case MAP_ORIGEN_RK:
        return "RK ";
      case MAP_ORIGEN_RB:
        return "RB ";
      case MAP_ORIGEN_RI:
      default:
        return "RI ";
    }
  }

  private Object getCuentaID(String cdsegmento) {
    switch (cdsegmento) {
      case "CW":
        return MAP_CUENTA_CW;
      case "CV":
      case "EO":
        return MAP_CUENTA_CV_EO;
      default:
        return MAP_CUENTA_DEFAULT;

    }
  }

  private Date getDateBySubstractDays(Date fecha, int dias) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha);
    calendar.add(Calendar.DAY_OF_YEAR, -dias);

    return new Date(calendar.getTime().getTime());
  }

  private Date getStartMonthBySubstractMonth(Date fecha, int meses) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha);
    calendar.add(Calendar.MONTH, -meses);
    calendar.set(Calendar.DAY_OF_MONTH, 1);

    return new Date(calendar.getTime().getTime());
  }

  public String getFileName() {
    return fileNameEjecuciones;
  }
}
