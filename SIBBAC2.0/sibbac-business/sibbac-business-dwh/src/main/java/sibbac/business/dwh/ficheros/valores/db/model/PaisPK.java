package sibbac.business.dwh.ficheros.valores.db.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The Class PaisPK.
 */
@Embeddable
public class PaisPK implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2240166070992229937L;

  /** The cdisoalf 2. */
  @Column(name = "CDISOALF2", nullable = false, length = 2)
  private String cdisoalf2;

  /** The cdisoalf 3. */
  @Column(name = "CDISOALF3", nullable = false, length = 3)
  private String cdisoalf3;

  /** The cdisonum. */
  @Column(name = "CDISONUM", nullable = false, length = 6)
  private String cdisonum;

  /**
   * Gets the cdisoalf 2.
   *
   * @return the cdisoalf 2
   */
  public String getCdisoalf2() {
    return cdisoalf2;
  }

  /**
   * Sets the cdisoalf 2.
   *
   * @param cdisoalf2 the new cdisoalf 2
   */
  public void setCdisoalf2(String cdisoalf2) {
    this.cdisoalf2 = cdisoalf2;
  }

  /**
   * Gets the cdisoalf 3.
   *
   * @return the cdisoalf 3
   */
  public String getCdisoalf3() {
    return cdisoalf3;
  }

  /**
   * Sets the cdisoalf 3.
   *
   * @param cdisoalf3 the new cdisoalf 3
   */
  public void setCdisoalf3(String cdisoalf3) {
    this.cdisoalf3 = cdisoalf3;
  }

  /**
   * Gets the cdisonum.
   *
   * @return the cdisonum
   */
  public String getCdisonum() {
    return cdisonum;
  }

  /**
   * Sets the cdisonum.
   *
   * @param cdisonum the new cdisonum
   */
  public void setCdisonum(String cdisonum) {
    this.cdisonum = cdisonum;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PaisPK [cdisoalf2=" + cdisoalf2 + ", cdisoalf3=" + cdisoalf3 + ", cdisonum=" + cdisonum + "]";
  }
}
