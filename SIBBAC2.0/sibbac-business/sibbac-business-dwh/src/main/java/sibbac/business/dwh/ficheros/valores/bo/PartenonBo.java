package sibbac.business.dwh.ficheros.valores.bo;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableTableConcurrentProcessLoaderBo;
import sibbac.business.dwh.ficheros.valores.bo.callable.CallableCargaValoresPartenonProcessBo;
import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;
import sibbac.business.dwh.utils.DwhConstantes.TipoValoresProcess;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PartenonBo {

  private static final Logger LOG = LoggerFactory.getLogger(PartenonBo.class);

  /** Ruta donde se deposita el fichero. */
  @Value("${dwh.ficheros.valores.partenon.in.dir:/sbrmv/ficheros/partenon/in/valores/}")
  private String partenonDir;

  /** Ruta donde se deposita el fichero. */
  @Value("${dwh.ficheros.valores.partenon.in.file:FVLR0ISB}")
  private String partenonFile;

  /** Ruta donde se deposita el fichero. */
  @Value("${dwh.ficheros.valores.partenon.out.dir:/sbrmv/ficheros/partenon/in/valores/tratados/}")
  private String partenonTratadosDir;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${sibbac.batch.timeout: 90}")
  private int timeout;

  private List<PartenonFileLineDTO> valoresEmisorList;

  private List<PartenonFileLineDTO> valoresMercadoList;

  private List<PartenonFileLineDTO> valoresWarrantList;

  private List<PartenonFileLineDTO> valoresEmisionList;

  @Autowired
  private ApplicationContext ctx;

  /**
   * Procesa el Fichero de partenon
   * @throws SIBBACBusinessException
   */
  public void loadTemporaryTables() throws SIBBACBusinessException {
    final File file;
    long startTimeProcessFile;

    // 1. Leemos el fichero de Partenon
    if (LOG.isInfoEnabled()) {
      LOG.info("Iniciamos la lectura del fichero de Partenon");
    }

    startTimeProcessFile = System.currentTimeMillis();

    file = new File(partenonDir + partenonFile);

    readPartenonFile(file);

    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("Finalizada la lectura del fichero de Partenon, %d ms",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }

    // 2. Lanzamos el proceso de Carga de las tablas Emision, Emisor, Warrant y
    // Mercado
    if (LOG.isInfoEnabled()) {
      LOG.info("Iniciamos la carga de las tablas Temporales");
    }

    startTimeProcessFile = System.currentTimeMillis();

    loadTemporaryTablesFromPartenon();

    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("Terminamos la carga de las tablas Temporales, %d ms",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }
  }

  /**
   * Cargamos los Valores en Maestro Valores desde Partenon ()
   */
  public void cargaValoresPartenon() throws SIBBACBusinessException {
    if (LOG.isInfoEnabled()) {
      LOG.info("Iniciamos la validacion/carga de MAESTRO_VALORES desde Partenon");
    }

    long startTimeProcessFile = System.currentTimeMillis();

    final List<CallableTableConcurrentProcessLoaderBo> futureList = new ArrayList<>();
    final ExecutorService executor = Executors.newFixedThreadPool(1, new NamedThreadFactory("validator"));

    boolean onTime = true;

    // Lanzamos el proceso de validacion
    final CallableCargaValoresPartenonProcessBo callableValoreProcessValidator = ctx
        .getBean(CallableCargaValoresPartenonProcessBo.class);

    callableValoreProcessValidator.setFuture(executor.submit(callableValoreProcessValidator));

    executor.shutdown();

    try {
      onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

      if (!onTime) { // *
        for (CallableTableConcurrentProcessLoaderBo future : futureList) {
          if (!future.getFuture().isDone()) {
            future.getFuture().cancel(true);
            throw new SIBBACBusinessException("Error, se ha agotado el tiempo de ejecucion para la validacion");
          }
        }
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error durante la carga", e);
    }

    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("Terminamos la validacion/carga de MAESTRO_VALORES desde Partenon, %d ms",
          (System.currentTimeMillis() - startTimeProcessFile)));
    }
  }

  /**
   * Mueve el fichero de partenon a tratados
   */
  public void movePartenonFile() {
    if (LOG.isInfoEnabled()) {
      LOG.info("Se mueve el fichero de partenon a tratados");
    }

    final File file = new File(partenonDir + partenonFile);
    
    final String partenonHistFile = FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.DATE_FORMAT) + "_"
        + partenonFile;
    final File destinationPartenonFile = new File(partenonTratadosDir + partenonHistFile);

    final File destinationFolder = new File(partenonTratadosDir);

    if (!destinationFolder.exists()) {
      destinationFolder.mkdirs();
      if (LOG.isDebugEnabled()) {
        LOG.debug("Se ha creado el directorio '" + partenonTratadosDir + "'");
      }
    }

    try {
      FileUtils.moveFile(file, destinationPartenonFile);
      if (LOG.isDebugEnabled()) {
        LOG.debug("Se ha movido el fichero  '" + partenonFile + "' a '" + partenonTratadosDir + "', con el nombre '"
            + partenonHistFile + "'");
      }
    }
    catch (IOException exception) {
      LOG.error("No se ha movido el fichero  '" + partenonFile + "' a '" + partenonTratadosDir + "'", exception);
    }
  }
  
  /**
   * Lanza el proceso de Carga
   * @param valoresEmisorList
   * @param valoresMercadoList
   * @param valoresWarrantList
   * @param valoresEmisionList
   * @throws SIBBACBusinessException
   */
  private void loadTemporaryTablesFromPartenon() throws SIBBACBusinessException {
    final List<CallableTableConcurrentProcessLoaderBo> futureList = new ArrayList<>();
    final ExecutorService executor = Executors.newFixedThreadPool(4, new NamedThreadFactory("loader"));

    boolean onTime = true;

    // Lanzamos el proceso de Carga de Valores de Emision
    final CallableTableConcurrentProcessLoaderBo callableValoresEmision = ctx.getBean(CallableTableConcurrentProcessLoaderBo.class);
    callableValoresEmision.setTipoValoresProcess(TipoValoresProcess.VALORES_EMISION);
    callableValoresEmision.setListData(valoresEmisionList);
    callableValoresEmision.setFuture(executor.submit(callableValoresEmision));

    // Lanzamos el proceso de Carga de Valores Emisor
    final CallableTableConcurrentProcessLoaderBo callableValoresEmisor = ctx.getBean(CallableTableConcurrentProcessLoaderBo.class);
    callableValoresEmisor.setTipoValoresProcess(TipoValoresProcess.VALORES_EMISOR);
    callableValoresEmisor.setListData(valoresEmisorList);
    callableValoresEmisor.setFuture(executor.submit(callableValoresEmisor));

    // Lanzamos el proceso de Carga de Valores Mercado
    final CallableTableConcurrentProcessLoaderBo callableValoresMercado = ctx.getBean(CallableTableConcurrentProcessLoaderBo.class);
    callableValoresMercado.setTipoValoresProcess(TipoValoresProcess.VALORES_MERCADO);
    callableValoresMercado.setListData(valoresMercadoList);
    callableValoresMercado.setFuture(executor.submit(callableValoresMercado));

    // Lanzamos el proceso de Carga de Warrant
    final CallableTableConcurrentProcessLoaderBo callableValoresWarrant = ctx.getBean(CallableTableConcurrentProcessLoaderBo.class);
    callableValoresWarrant.setTipoValoresProcess(TipoValoresProcess.VALORES_WARRANT);
    callableValoresWarrant.setListData(valoresWarrantList);
    callableValoresWarrant.setFuture(executor.submit(callableValoresWarrant));

    executor.shutdown();

    try {
      onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

      if (!onTime) { // *
        for (CallableTableConcurrentProcessLoaderBo future : futureList) {
          if (!future.getFuture().isDone()) {
            future.getFuture().cancel(true);
            throw new SIBBACBusinessException("Error, se ha agotado el tiempo de ejecucion para la carga concurrente");
          }
        }
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error durante la carga", e);
    }
  }

  /**
   * Lee el fichero de Partenon y lo divide en listas
   * @return
   * @throws SIBBACBusinessException
   */
  private void readPartenonFile(File file) throws SIBBACBusinessException {
    BigInteger emisorCounter = BigInteger.ZERO;
    BigInteger emisionCounter = BigInteger.ZERO;
    BigInteger mercadoCounter = BigInteger.ZERO;
    BigInteger warrantCounter = BigInteger.ZERO;

    valoresEmisorList = new ArrayList<>();
    valoresMercadoList = new ArrayList<>();
    valoresWarrantList = new ArrayList<>();
    valoresEmisionList = new ArrayList<>();

    FileInputStream fstream = null;
    DataInputStream in = null;
    BufferedReader br = null;

    try {
      fstream = new FileInputStream(file);
      in = new DataInputStream(fstream);
      br = new BufferedReader(new InputStreamReader(in, "ISO-8859-1"));

      String fileLine = null;
      while ((fileLine = br.readLine()) != null) {

        final String type = fileLine.substring(0, 1);

        if (type.equals("E")) {
          valoresEmisorList.add(new PartenonFileLineDTO(emisorCounter, fileLine));
          emisorCounter = emisorCounter.add(BigInteger.ONE);
        }
        else if (type.equals("M")) {
          valoresMercadoList.add(new PartenonFileLineDTO(mercadoCounter, fileLine));
          mercadoCounter = mercadoCounter.add(BigInteger.ONE);
        }
        else if (type.equals("W")) {
          valoresWarrantList.add(new PartenonFileLineDTO(warrantCounter, fileLine));
          warrantCounter = warrantCounter.add(BigInteger.ONE);
        }
        else if (type.equals("1")) {
          valoresEmisionList.add(new PartenonFileLineDTO(emisionCounter, fileLine));
          emisionCounter = emisionCounter.add(BigInteger.ONE);
        }
      }
    }
    catch (IOException exception) {
      throw new SIBBACBusinessException("El fichero '" + file.getAbsolutePath() + "' no ha sido encontrado", exception);
    }
    finally {
      if (br != null) {
        try {
          br.close();
        }
        catch (IOException e) {
          LOG.error("No se pudo cerrar el BufferedReader", e);
        }
      }

      if (in != null) {
        try {
          in.close();
        }
        catch (IOException e) {
          LOG.error("No se pudo cerrar el DataInputStream", e);
        }
      }

      if (fstream != null) {
        try {
          fstream.close();
        }
        catch (IOException e) {
          LOG.error("No se pudo cerrar el FileInputStream", e);
        }
      }
    }
  }
}
