package sibbac.business.dwh.ficheros.clientes.db.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_DWH_CLIENTES")
public class Tmct0DwhClientes implements java.io.Serializable {

  private static final long serialVersionUID = 1367920644570427389L;

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDCLIENT", length = 10, nullable = false, unique = true)
  private Integer idClient;

  @Column(name = "CDCLIENT", length = 8, nullable = false)
  private Integer cdClient;

  @Column(name = "NBDESCLI", length = 40, nullable = false)
  private String nbDesCli;

  @Column(name = "NUCLIENT", length = 6, nullable = false)
  private Integer nuClient;

  @Column(name = "CDPRODUC", length = 3, nullable = false)
  private String cdProduct;

  @Column(name = "CDGRUPOO", length = 8, nullable = false)
  private Integer cdGrupo;

  @Column(name = "NBGRUPOO", length = 40, nullable = false)
  private String nbGrupo;

  @Column(name = "CDCLITRA", length = 8, nullable = false)
  private Integer cdLitra;

  @Column(name = "CDCLISAL", length = 8, nullable = false)
  private Integer cdLisal;

  @Column(name = "CDCLIACM", length = 8, nullable = false)
  private Integer cdcLiacm;

  @Column(name = "CDHPPAIS", length = 3, nullable = false)
  private String cdhpPais;

  @Column(name = "NUNIVEL0", length = 5, nullable = false)
  private Integer nuNivel0;

  @Column(name = "NBNIVEL0", length = 25, nullable = false)
  private String nbNivel0;

  @Column(name = "NUNIVEL1", length = 5, nullable = false)
  private Integer nuNivel1;

  @Column(name = "NBNIVEL1", length = 25, nullable = false)
  private String nbNivel1;

  @Column(name = "NUNIVEL2", length = 5, nullable = false)
  private Integer nuNivel2;

  @Column(name = "NBNIVEL2", length = 25, nullable = false)
  private String nbNivel2;

  @Column(name = "NUNIVEL3", length = 5, nullable = false)
  private Integer nuNivel3;

  @Column(name = "NBNIVEL3", length = 25, nullable = false)
  private String nbNivel3;

  @Column(name = "NUNIVEL4", length = 5, nullable = false)
  private Integer nuNivel4;

  @Column(name = "NBNIVEL4", length = 25, nullable = false)
  private String nbNivel4;

  @Column(name = "CDGRUPOA", length = 8, nullable = false)
  private Integer cdGrupoA;

  @Column(name = "NBGRUPOA", length = 40, nullable = false)
  private String nbGrupoA;

  @Column(name = "NUDNICIF", length = 15, nullable = false)
  private String nudNiCif;

  @Column(name = "USUAUDIT", length = 20, nullable = false)
  private String usAudit;

  @Column(name = "FHAUDIT", nullable = false)
  private Timestamp fhAudit;

  public Tmct0DwhClientes() {
    super();
  }

  public Tmct0DwhClientes(Integer idClient, Integer cdClient, String nbDesCli, Integer nuClient, String cdProduct,
      Integer cdGrupo, String nbGrupo, Integer cdLitra, Integer cdLisal, Integer cdcLiacm, String cdhpPais,
      Integer nuNivel0, String nbNivel0, Integer nuNivel1, String nbNivel1, Integer nuNivel2, String nbNivel2,
      Integer nuNivel3, String nbNivel3, Integer nuNivel4, String nbNivel4, Integer cdGrupoA, String nbGrupoA,
      String nudNiCif, String usAudit, Timestamp fhAudit) {
    super();
    this.idClient = idClient;
    this.cdClient = cdClient;
    this.nbDesCli = nbDesCli;
    this.nuClient = nuClient;
    this.cdProduct = cdProduct;
    this.cdGrupo = cdGrupo;
    this.nbGrupo = nbGrupo;
    this.cdLitra = cdLitra;
    this.cdLisal = cdLisal;
    this.cdcLiacm = cdcLiacm;
    this.cdhpPais = cdhpPais;
    this.nuNivel0 = nuNivel0;
    this.nbNivel0 = nbNivel0;
    this.nuNivel1 = nuNivel1;
    this.nbNivel1 = nbNivel1;
    this.nuNivel2 = nuNivel2;
    this.nbNivel2 = nbNivel2;
    this.nuNivel3 = nuNivel3;
    this.nbNivel3 = nbNivel3;
    this.nuNivel4 = nuNivel4;
    this.nbNivel4 = nbNivel4;
    this.cdGrupoA = cdGrupoA;
    this.nbGrupoA = nbGrupoA;
    this.nudNiCif = nudNiCif;
    this.usAudit = usAudit;
    this.fhAudit = fhAudit != null ? new Timestamp(fhAudit.getTime()) : null;
  }

  public Integer getIdClient() {
    return idClient;
  }

  public void setIdClient(Integer idClient) {
    this.idClient = idClient;
  }

  public Integer getCdClient() {
    return cdClient;
  }

  public void setCdClient(Integer cdClient) {
    this.cdClient = cdClient;
  }

  public String getNbDesCli() {
    return nbDesCli;
  }

  public void setNbDesCli(String nbDesCli) {
    this.nbDesCli = nbDesCli;
  }

  public Integer getNuClient() {
    return nuClient;
  }

  public void setNuClient(Integer nuClient) {
    this.nuClient = nuClient;
  }

  public String getCdProduct() {
    return cdProduct;
  }

  public void setCdProduct(String cdProduct) {
    this.cdProduct = cdProduct;
  }

  public Integer getCdGrupo() {
    return cdGrupo;
  }

  public void setCdGrupo(Integer cdGrupo) {
    this.cdGrupo = cdGrupo;
  }

  public String getNbGrupo() {
    return nbGrupo;
  }

  public void setNbGrupo(String nbGrupo) {
    this.nbGrupo = nbGrupo;
  }

  public Integer getCdLitra() {
    return cdLitra;
  }

  public void setCdLitra(Integer cdLitra) {
    this.cdLitra = cdLitra;
  }

  public Integer getCdLisal() {
    return cdLisal;
  }

  public void setCdLisal(Integer cdLisal) {
    this.cdLisal = cdLisal;
  }

  public Integer getCdcLiacm() {
    return cdcLiacm;
  }

  public void setCdcLiacm(Integer cdcLiacm) {
    this.cdcLiacm = cdcLiacm;
  }

  public String getCdhpPais() {
    return cdhpPais;
  }

  public void setCdhpPais(String cdhpPais) {
    this.cdhpPais = cdhpPais;
  }

  public Integer getNuNivel0() {
    return nuNivel0;
  }

  public void setNuNivel0(Integer nuNivel0) {
    this.nuNivel0 = nuNivel0;
  }

  public String getNbNivel0() {
    return nbNivel0;
  }

  public void setNbNivel0(String nbNivel0) {
    this.nbNivel0 = nbNivel0;
  }

  public Integer getNuNivel1() {
    return nuNivel1;
  }

  public void setNuNivel1(Integer nuNivel1) {
    this.nuNivel1 = nuNivel1;
  }

  public String getNbNivel1() {
    return nbNivel1;
  }

  public void setNbNivel1(String nbNivel1) {
    this.nbNivel1 = nbNivel1;
  }

  public Integer getNuNivel2() {
    return nuNivel2;
  }

  public void setNuNivel2(Integer nuNivel2) {
    this.nuNivel2 = nuNivel2;
  }

  public String getNbNivel2() {
    return nbNivel2;
  }

  public void setNbNivel2(String nbNivel2) {
    this.nbNivel2 = nbNivel2;
  }

  public Integer getNuNivel3() {
    return nuNivel3;
  }

  public void setNuNivel3(Integer nuNivel3) {
    this.nuNivel3 = nuNivel3;
  }

  public String getNbNivel3() {
    return nbNivel3;
  }

  public void setNbNivel3(String nbNivel3) {
    this.nbNivel3 = nbNivel3;
  }

  public Integer getNuNivel4() {
    return nuNivel4;
  }

  public void setNuNivel4(Integer nuNivel4) {
    this.nuNivel4 = nuNivel4;
  }

  public String getNbNivel4() {
    return nbNivel4;
  }

  public void setNbNivel4(String nbNivel4) {
    this.nbNivel4 = nbNivel4;
  }

  public Integer getCdGrupoA() {
    return cdGrupoA;
  }

  public void setCdGrupoA(Integer cdGrupoA) {
    this.cdGrupoA = cdGrupoA;
  }

  public String getNbGrupoA() {
    return nbGrupoA;
  }

  public void setNbGrupoA(String nbGrupoA) {
    this.nbGrupoA = nbGrupoA;
  }

  public String getNudNiCif() {
    return nudNiCif;
  }

  public void setNudNiCif(String nudNiCif) {
    this.nudNiCif = nudNiCif;
  }

  public String getUsAudit() {
    return usAudit;
  }

  public void setUsAudit(String usAudit) {
    this.usAudit = usAudit;
  }

  public Timestamp getFhAudit() {
    return (Timestamp) fhAudit.clone();
  }

  public void setFhAudit(Timestamp fhAudit) {
    this.fhAudit = (Timestamp) fhAudit.clone();
  }
}
