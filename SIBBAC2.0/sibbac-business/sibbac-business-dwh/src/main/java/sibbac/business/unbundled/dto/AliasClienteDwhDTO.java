package sibbac.business.unbundled.dto;

public class AliasClienteDwhDTO {

  private String alias;
  private String descripcion;
  private String  cdBroCli;
  


  public AliasClienteDwhDTO(){
    
  }

  public AliasClienteDwhDTO( String alias, String descripcion ) {
    this.alias = ( alias != null && !"".equals( alias ) ) ? alias.trim() : "";
    this.descripcion = ( descripcion != null && !"".equals( descripcion ) ) ? descripcion.trim() : "";
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  
  /**
   * @return the cdBroCli
   */
  public String getCdBroCli() {
    return cdBroCli;
  }

  /**
   * @param cdBroCli the cdBroCli to set
   */
  public void setCdBroCli(String cdBroCli) {
    this.cdBroCli = cdBroCli;
  }

  public static AliasClienteDwhDTO mapObject(Object[] valuesFromQuery){
    
    AliasClienteDwhDTO alias = new AliasClienteDwhDTO();
    
    String codAlias = (String) valuesFromQuery[0];
    String descAlias = (String) valuesFromQuery[1];
    String cdBroCli = (String) valuesFromQuery[2];
    
    alias.setAlias(codAlias.trim());
    alias.setDescripcion(descAlias.trim());
    alias.setCdBroCli(cdBroCli.trim());
    
    return alias;
  }
  

}
