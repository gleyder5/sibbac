package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.io.BufferedWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.nacional.bo.FicheroNacionalBo;
import sibbac.business.dwh.ficheros.nacional.db.model.Tmct0DwhEjecuciones;
import sibbac.business.dwh.unbundled.db.model.Unbundled;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableNacionalUnbundledLineBo extends AbstractCallableProcessBo {

  @Autowired
  private FicheroNacionalBo ficheroNacionalBo;

  BufferedWriter writer = null;

  private List<Unbundled> unbundledList;

  private List<Tmct0DwhEjecuciones> ejecuciones;

  private static final Logger LOG = LoggerFactory.getLogger(CallableNacionalUnbundledLineBo.class);

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {

    try {
      long initTimeStampAlta = System.currentTimeMillis();

      ficheroNacionalBo.writeUnbundledLine(unbundledList, ejecuciones, writer);

      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("TERMINADO save de ejecuciones en %d milisegundos",
            (System.currentTimeMillis() - initTimeStampAlta)));
      }
    }
    catch (Exception e) {
    }
    return true;
  }

  public List<Tmct0DwhEjecuciones> getEjecucionesList() {
    return ejecuciones;
  }

  public void setEjecucionesList(List<Tmct0DwhEjecuciones> ejecucionesList) {
    this.ejecuciones = ejecucionesList;
  }

  public BufferedWriter getWriter() {
    return writer;
  }

  public void setWriter(BufferedWriter writer) {
    this.writer = writer;
  }

  public List<Unbundled> getOperacionesList() {
    return unbundledList;
  }

  public void setUnbundledList(List<Unbundled> operacionesList) {
    this.unbundledList = operacionesList;
  }
}
