package sibbac.business.dwh.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.dwh.ficheros.valores.bo.BaseBo;
import sibbac.business.dwh.ficheros.valores.bo.GruposValoresBo;
import sibbac.business.dwh.ficheros.valores.bo.MonedaBo;
import sibbac.business.dwh.ficheros.valores.bo.PaisBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresGrupoBo;
import sibbac.business.dwh.ficheros.valores.db.query.MaestroValoresQuery;
import sibbac.business.dwh.ficheros.valores.dto.GruposValoresDTO;
import sibbac.business.dwh.ficheros.valores.dto.MaestroValoresDTO;
import sibbac.business.dwh.ficheros.valores.dto.MonedaDTO;
import sibbac.business.dwh.ficheros.valores.dto.PaisDTO;
import sibbac.business.dwh.ficheros.valores.dto.ValoresGrupoDTO;
import sibbac.common.CallableResultDTO;

/**
 * The Class SIBBACServiceParaisosFiscales.
 */
@RestController
@RequestMapping("/datosMaestros/valores")
public class SIBBACServiceMaestroValores extends BaseService {

  /** The maestro valores bo. */
  @Autowired
  private BaseBo<MaestroValoresDTO> maestroValoresBo;

  /** The valores grupo bo. */
  @Autowired
  private ValoresGrupoBo valoresGrupoBo;

  /** The grupo valores bo. */
  @Autowired
  private GruposValoresBo gruposValoresBo;

  /** The moneda bo. */
  @Autowired
  private MonedaBo monedaBo;

  /** The pais bo. */
  @Autowired
  private PaisBo paisBo;

  /**
   * Instantiates a new clientes PBC service.
   */
  public SIBBACServiceMaestroValores() {
    super("Configuración de Maestros Valores", "maestrosValores", MaestroValoresQuery.class.getSimpleName());
  }

  /**
   * Execute action post.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody MaestroValoresDTO solicitud) {
    return maestroValoresBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody MaestroValoresDTO solicitud) {
    return maestroValoresBo.actionUpdate(solicitud);
  }

  /**
   * Execute action delete List.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/delete", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionDelete(@RequestBody List<String> solicitud) {
    return maestroValoresBo.actionDelete(solicitud);
  }

  /**
   * Execute action get values CDGRSIBV, NBGRSIBV from TMCT0_VALORES_GRUPO
   * table.
   *
   * @return the callable result DTO
   */
  @RequestMapping(value = "/getValoresGrupo", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<ValoresGrupoDTO> executeGetValoresGrupo() {
    return valoresGrupoBo.getCdgrisivAndNbgrisibv();
  }

  /**
   * Execute action get values ID, NB_DESCRIPCION from TMCT0_GRUPOS_VALORES
   * table.
   *
   * @return the callable result DTO
   */
  @RequestMapping(value = "/getGruposValores", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<GruposValoresDTO> executeGetGruposValores() {
    return gruposValoresBo.getIdAndNbdescipcion();
  }

  /**
   * Execute action get values CDMONEDA, NBDESCRIPCION from TMCT0_MONEDA table.
   *
   * @return the callable result DTO
   */
  @RequestMapping(value = "/getMonedas", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<MonedaDTO> executeGetMonedas() {
    return monedaBo.getCdmonedaAndNbdescripcion();
  }

  /**
   * Execute action get values CDISONUM, NBPAIS from TMCT0PAISES table.
   *
   * @return the callable result DTO
   */
  @RequestMapping(value = "/getPaises", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<PaisDTO> executeGetPaises() {
    return paisBo.getCdisonumAndNbpais();
  }
}
