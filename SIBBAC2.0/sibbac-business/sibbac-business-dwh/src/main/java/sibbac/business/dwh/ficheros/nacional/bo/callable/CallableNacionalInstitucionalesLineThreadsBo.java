package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableNacionalInstitucionalesLineThreadsBo extends AbstractCallableProcessBo {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractCallableProcessBo.class);

  /** Tamaño de la pagina */
  @Value("${fileName.dwh.nacional.autex.batch:100}")
  private int pageSize;

  /** Número de Hilos de ejecucion */
  @Value("${fileName.dwh.partenon.threads:5}")
  private int threads;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${sibbac.batch.timeout: 60}")
  private int timeout;

  @Autowired
  private ApplicationContext ctx;

  private List<Object[]> institucionalesList;

  BufferedWriter writer = null;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    Boolean result = false;
    try {
      executeWriteInstitucionalesLineProcess();
    }
    catch (SIBBACBusinessException e) {
      LOG.error("Se ha producido un error: ", e);
      result = false;
    }

    return result;
  }

  /**
   * Procesa las listas y las setea en sus respectivos BO para poder ejecutarlos
   * con posterioridad
   * @param valoresEmisorList
   */
  public void executeWriteInstitucionalesLineProcess() throws SIBBACBusinessException {
    try {
      final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

      final Integer numPaginas = institucionalesList.size() / pageSize;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * pageSize;
        Integer toIndex = ((pageNum + 1) * pageSize);

        if (toIndex > institucionalesList.size() || fromIndex > institucionalesList.size()) {
          toIndex = institucionalesList.size();
        }

        final List<Object[]> subList = institucionalesList.subList(fromIndex, toIndex);

        final CallableNacionalInstitucionalesLineBo callableBo = ctx
            .getBean(CallableNacionalInstitucionalesLineBo.class);

        callableBo.setWriter(writer);
        callableBo.setInstitucionalesList(subList);

        processInterfaces.add(callableBo);
      }

      // Lanzamos los hilos
      launchProcess(processInterfaces);
    }
    catch (BeansException e) {
      throw new SIBBACBusinessException("", e);
    }
    catch (InterruptedException e) {
      throw new SIBBACBusinessException("", e);
    }
  }

  /**
   * 
   */
  public void launchProcess(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {
    try {
      final Integer numPaginas = processInterfaces.size() / threads;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threads;
        Integer toIndex = ((pageNum + 1) * threads);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableProcessInterface> subList = processInterfaces.subList(fromIndex, toIndex);

        launchThreadBatch(subList);
      }
    }
    catch (BeansException e) {
      throw new SIBBACBusinessException("", e);

    }
    catch (InterruptedException e) {
      throw new SIBBACBusinessException("", e);
    }
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatch(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threads,
        new NamedThreadFactory("batchSaveEjecuciones"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  public List<Object[]> getInstitucionalesList() {
    return institucionalesList;
  }

  public void setInstitucionalesList(List<Object[]> institucionalesList) {
    this.institucionalesList = institucionalesList;
  }

  public BufferedWriter getWriter() {
    return writer;
  }

  public void setWriter(BufferedWriter writer) {
    this.writer = writer;
  }

}
