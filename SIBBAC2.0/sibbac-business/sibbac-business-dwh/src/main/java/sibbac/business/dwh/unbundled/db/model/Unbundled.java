package sibbac.business.dwh.unbundled.db.model;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.CodigoTptiprep }. Entity:
 * "CodigoTptiprep".
 * 
 * @version 1.0
 * @since 1.0
 */
//
@Table( name = "TMCT0_DWH_UNBUNDLED")
@Entity
public class Unbundled implements java.io.Serializable {
  /**
   * 
   */
  private static final long serialVersionUID  = -5668031503577663012L;
  /**
   * 
   */
  
  @Id
  @Column( name = "NUM_OPERACION", nullable = false, length = 12)
  private String numOper;
  
  @Column( name = "CLIENTE", nullable = false, length = 20)
  private String cliente;
  
  @Column( name = "COMISION_PAGADA", nullable = false)
  private BigDecimal comision;
  
  @Column( name = "FECHA_PAGO", nullable = false )
  private Date fechaPago;
  
  @Column( name = "OBSERVACION", nullable = true, length = 256 )
  private String observacion;
  
  @Column( name = "USUARIO_ALTA", nullable = false, length = 256 )
  private String usuarioAlta;
  
  @Column( name = "USUARIO_MOD", nullable = true, length = 256 )
  private String usuarioMod;
  
  @Column( name = "FECHA_ALTA", nullable = false)
  private Timestamp fechaAlta;
  
  @Column( name = "FECHA_MOD", nullable = true)
  private Timestamp fechaMod;

/**
 * @return the numOper
 */
public String getNumOper() {
	return numOper;
}

/**
 * @param numOper the numOper to set
 */
public void setNumOper(String numOper) {
	this.numOper = numOper;
}

/**
 * @return the cliente
 */
public String getCliente() {
	return cliente;
}

/**
 * @param cliente the cliente to set
 */
public void setCliente(String cliente) {
	this.cliente = cliente;
}

/**
 * @return the comision
 */
public BigDecimal getComision() {
	return comision;
}

/**
 * @param comision the comision to set
 */
public void setComision(BigDecimal comision) {
	this.comision = comision;
}

/**
 * @return the fechaPago
 */
public Date getFechaPago() {
	return (Date) fechaPago.clone();
}

/**
 * @param fechaPago the fechaPago to set
 */
public void setFechaPago(Date fechaPago) {
	this.fechaPago = (Date) fechaPago.clone();
}

/**
 * @return the origen
 */
public String getOrigen() {
	return observacion;
}

/**
 * @param origen the origen to set
 */
public void setOrigen(String origen) {
	this.observacion = origen;
}

/**
 * @return the usuarioAlta
 */
public String getUsuarioAlta() {
	return usuarioAlta;
}

/**
 * @param usuarioAlta the usuarioAlta to set
 */
public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
}

/**
 * @return the usuarioMod
 */
public String getUsuarioMod() {
	return usuarioMod;
}

/**
 * @param usuarioMod the usuarioMod to set
 */
public void setUsuarioMod(String usuarioMod) {
	this.usuarioMod = usuarioMod;
}

/**
 * @return the fechaAlta
 */
public Timestamp getFechaAlta() {
	return (Timestamp) fechaAlta.clone();
}

/**
 * @param fechaAlta the fechaAlta to set
 */
public void setFechaAlta(Timestamp fechaAlta) {
	this.fechaAlta = (Timestamp) fechaAlta.clone();
}

/**
 * @return the fechaMod
 */
public Timestamp getFechaMod() {
	return (Timestamp) fechaMod.clone();
}

/**
 * @param fechaMod the fechaMod to set
 */
public void setFechaMod(Timestamp fechaMod) {
	this.fechaMod = (Timestamp) fechaMod.clone();
}

  
}