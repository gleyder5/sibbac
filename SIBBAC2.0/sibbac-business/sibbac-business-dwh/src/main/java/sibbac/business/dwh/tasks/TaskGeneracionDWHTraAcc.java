package sibbac.business.dwh.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.dwh.ficheros.callable.results.CallableProcessFileResult;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.tradersaccounters.bo.callable.CallableGenerateFicheroAccMa;
import sibbac.business.dwh.ficheros.tradersaccounters.bo.callable.CallableGenerateFicheroSales;
import sibbac.business.dwh.ficheros.tradersaccounters.bo.callable.CallableGenerateFicheroTraders;
import sibbac.business.dwh.tasks.commons.TaskProcessDWH;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_ENVIO_FICHEROS_DWH_TRACACC, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 23 ? * MON-FRI")
public class TaskGeneracionDWHTraAcc extends WrapperTaskConcurrencyPrevent implements TaskProcessDWH {

  private static final Logger LOG = LoggerFactory.getLogger(TaskGeneracionDWHTraAcc.class);

  /**
   * Ruta donde se deposita el fichero
   */
  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String outPutDir;

  @Value("${fichero.clientes.db.page.size:20}")
  private int dbPageSize;

  /**
   * Nombre del fichero para la generacion del fichero de Grupo de Clientes
   * Estáticos
   */
  @Value("${dwh.ficheros.traders_accounters.generated_if_empty:true}")
  private Boolean generateIfEmpty;

  /**
   * Nombre del fichero de Traders
   */
  @Value("${dwh.ficheros.traders_accounters.traders.file:fdwh0trae.txt}")
  private String fileNameTraders;

  /**
   * Nombre del fichero de Sales
   */
  @Value("${dwh.ficheros.traders_accounters.sales.file:fdwh0sale.txt}")
  private String fileNameSales;

  /**
   * Nombre del fichero de Account Manager
   */
  @Value("${dwh.ficheros.traders_accounters.account.file:fdwh0acme.txt}")
  private String fileNameAccountManager;

  @Value("${dwh.ficheros.traders_accounters.batch.timeout.global: 90}")
  private int timeout;

  @Autowired
  private ApplicationContext ctx;

  /**
   * Listado de resultados de la generacion de ficheros
   */
  private List<ProcessFileResultDTO> processFileResultsList;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    this.launchProcess();
  }

  @Override
  public void launchProcess() throws SIBBACBusinessException {
    processFileResultsList = new ArrayList<>();

    final long initStartTime = System.currentTimeMillis();

    try {
      processFileResultsList.addAll(launchTraderAccounters());
    }
    catch (InterruptedException | ExecutionException exception) {
      throw new SIBBACBusinessException(exception);
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug(
          String.format("El proceso ha terminado, se ha ejecutado en %d", System.currentTimeMillis() - initStartTime));
    }
  }

  /**
   * Lanza el proceso de de Traders y Accounters
   * @throws SIBBACBusinessException
   */
  public List<ProcessFileResultDTO> launchTraderAccounters()
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final List<CallableProcessFileResult> processInterfaces = new ArrayList<>();

    // Generamos el fichero autex, seteando su paginación y fecha.
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGenerateFicheroTraders generateFicheroTraders = ctx.getBean(CallableGenerateFicheroTraders.class);
    generateFicheroTraders.getGenerateFicheroTraders().setOutPutDir(outPutDir);
    generateFicheroTraders.getGenerateFicheroTraders().setDbPageSize(dbPageSize);
    generateFicheroTraders.getGenerateFicheroTraders().setGenerateIfEmpty(generateIfEmpty);
    generateFicheroTraders.getGenerateFicheroTraders().setFileName(fileNameTraders);

    processInterfaces.add(generateFicheroTraders);

    // Generamos el fichero autex, seteando su paginación y fecha.
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGenerateFicheroSales generateFicheroSales = ctx.getBean(CallableGenerateFicheroSales.class);
    generateFicheroSales.getGenerateFicheroSales().setOutPutDir(outPutDir);
    generateFicheroSales.getGenerateFicheroSales().setDbPageSize(dbPageSize);
    generateFicheroSales.getGenerateFicheroSales().setGenerateIfEmpty(generateIfEmpty);
    generateFicheroSales.getGenerateFicheroSales().setFileName(fileNameSales);

    processInterfaces.add(generateFicheroSales);

    // Generamos el fichero autex, seteando su paginación y fecha.
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    final CallableGenerateFicheroAccMa generateFicheroAccMa = ctx.getBean(CallableGenerateFicheroAccMa.class);
    generateFicheroAccMa.getGenerateFicheroAccMa().setOutPutDir(outPutDir);
    generateFicheroAccMa.getGenerateFicheroAccMa().setDbPageSize(dbPageSize);
    generateFicheroAccMa.getGenerateFicheroAccMa().setGenerateIfEmpty(generateIfEmpty);
    generateFicheroAccMa.getGenerateFicheroAccMa().setFileName(fileNameAccountManager);

    processInterfaces.add(generateFicheroAccMa);

    return launchThreadBatch(processInterfaces);
  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   * @throws ExecutionException
   */
  private List<ProcessFileResultDTO> launchThreadBatch(List<CallableProcessFileResult> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final List<ProcessFileResultDTO> listProcessResultDTO = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(4, new NamedThreadFactory("batch_TradersAccounters"));

    final List<CallableProcessFileResult> futureList = new ArrayList<>();

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessFileResult callableBo : processInterfaces) {
      callableBo.setFuture(executor.submit(callableBo));
      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    final boolean onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Si se ha superado el tiempo estipulado para la ejecucion de los
    // ficheros cancelamos los hilos
    if (onTime) {
      for (CallableProcessFileResult processInterface : futureList) {
        listProcessResultDTO.add(processInterface.getFuture().get());
      }
    }
    else {
      for (CallableProcessFileResult future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }

    return listProcessResultDTO;
  }

  /**
   * Obtiene los resultados de la generación de los ficheros
   * @return
   */
  @Override
  public List<ProcessFileResultDTO> getProcessFileResultsList() {
    return processFileResultsList;
  }

  /**
   * Determinamos el tipo de tarea que vamos a ejecutar (Trade Sale Account).
   *
   * @return the tipo
   */
  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_FICHERO_TRADERS;
  }
}
