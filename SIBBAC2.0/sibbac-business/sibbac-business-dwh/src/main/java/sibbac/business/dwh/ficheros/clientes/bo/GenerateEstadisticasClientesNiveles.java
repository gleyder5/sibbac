package sibbac.business.dwh.ficheros.clientes.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.clientes.db.dao.DWHEstadisticasClientesNivelDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.DwhConstantes.NivelesFicheroClientes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateEstadisticasClientes0File")
public class GenerateEstadisticasClientesNiveles extends AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateEstadisticasClientesNiveles.class);

  private static final String ESTADISTICAS_CLIENTES_0_LINE_SEPARATOR = "\r\n";

  @Value("${dwh.ficheros.clientes.estadisticas_n0.file:fdwh0en0e.txt}")
  private String fileNameNivel0;

  @Value("${dwh.ficheros.clientes.estadisticas_n1.file:fdwh0en1e.txt}")
  private String fileNameNivel1;

  @Value("${dwh.ficheros.clientes.estadisticas_n2.file:fdwh0en2e.txt}")
  private String fileNameNivel2;

  @Value("${dwh.ficheros.clientes.estadisticas_n3.file:fdwh0en3e.txt}")
  private String fileNameNivel3;

  @Value("${dwh.ficheros.clientes.estadisticas_n4.file:fdwh0en4e.txt}")
  private String fileNameNivel4;

  private NivelesFicheroClientes nivel;

  private static int CLIENTES_NUNIVEL0 = 0;
  private static int CLIENTES_NBNIVEL0 = 1;
  private static int CLIENTES_NUNIVEL1 = 2;
  private static int CLIENTES_NBNIVEL1 = 3;
  private static int CLIENTES_NUNIVEL2 = 4;
  private static int CLIENTES_NBNIVEL2 = 5;
  private static int CLIENTES_NUNIVEL3 = 6;
  private static int CLIENTES_NBNIVEL3 = 7;
  private static int CLIENTES_NUNIVEL4 = 8;
  private static int CLIENTES_NBNIVEL4 = 9;
  /**
   * DAO de Acceso a Base de Datos
   */
  @Autowired
  private DWHEstadisticasClientesNivelDao estadisticasClientesNivelesDao;

  /**
   * Obtiene el separador de linea
   */
  @Override
  public String getLineSeparator() {
    return ESTADISTICAS_CLIENTES_0_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    List<Object[]> result = null;

    switch (nivel) {
      case NIVEL0:
        result = estadisticasClientesNivelesDao.findAllListEstadisticasClientes0(pageable.getOffset(),
            pageable.getPageSize());
        break;
      case NIVEL1:
        result = estadisticasClientesNivelesDao.findAllListEstadisticasClientes1(pageable.getOffset(),
            pageable.getPageSize());
        break;
      case NIVEL2:
        result = estadisticasClientesNivelesDao.findAllListEstadisticasClientes2(pageable.getOffset(),
            pageable.getPageSize());
        break;
      case NIVEL3:
        result = estadisticasClientesNivelesDao.findAllListEstadisticasClientes3(pageable.getOffset(),
            pageable.getPageSize());
        break;
      case NIVEL4:
        result = estadisticasClientesNivelesDao.findAllListEstadisticasClientes4(pageable.getOffset(),
            pageable.getPageSize());
        break;
      default:
        result = new ArrayList<>();
        break;
    }

    return result;
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    Integer countData;

    switch (nivel) {
      case NIVEL0:
        countData = estadisticasClientesNivelesDao.countAllListEstadisticasClientes0();
        break;
      case NIVEL1:
        countData = estadisticasClientesNivelesDao.countAllListEstadisticasClientes1();
        break;
      case NIVEL2:
        countData = estadisticasClientesNivelesDao.countAllListEstadisticasClientes2();
        break;
      case NIVEL3:
        countData = estadisticasClientesNivelesDao.countAllListEstadisticasClientes3();
        break;
      case NIVEL4:
        countData = estadisticasClientesNivelesDao.countAllListEstadisticasClientes4();
        break;
      default:
        countData = 0;
        break;
    }

    return countData;
  }

  /**
   * 
   */
  public void generateFileClientePorNivel(NivelesFicheroClientes nivel) throws SIBBACBusinessException {
    this.nivel = nivel;

    switch (nivel) {
      case NIVEL0:
        setFileName(fileNameNivel0);
        break;
      case NIVEL1:
        setFileName(fileNameNivel1);
        break;
      case NIVEL2:
        setFileName(fileNameNivel2);
        break;
      case NIVEL3:
        setFileName(fileNameNivel3);
        break;
      case NIVEL4:
        setFileName(fileNameNivel4);
        break;
      default:
        generateFile(getOutPutDir(), fileNameNivel0);
        break;
    }
    generateFile();
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    switch (nivel) {
      case NIVEL0:
        processRegistrer0(outWriter, fileData);
        break;
      case NIVEL1:
        processRegistrer1(outWriter, fileData);
        break;
      case NIVEL2:
        processRegistrer2(outWriter, fileData);
        break;
      case NIVEL3:
        processRegistrer3(outWriter, fileData);
        break;
      case NIVEL4:
        processRegistrer4(outWriter, fileData);
        break;
      default:
        processRegistrer0(outWriter, fileData);
        break;
    }
  }

  public void processRegistrer0(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL0])).intValue());
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL0]);

      final StringBuilder rowLine = new StringBuilder();
      rowLine.append(nunivel0).append(nbnivel0);
      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);
    }
  }

  public void processRegistrer1(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL0])).intValue());
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL0]);
      String nunivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL1])).intValue());
      String nbnivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL1]);

      final StringBuilder rowLine = new StringBuilder();
      rowLine.append(nunivel0).append(nbnivel0).append(nunivel1).append(nbnivel1);
      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);
    }
  }

  public void processRegistrer2(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL0])).intValue());
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL0]);
      String nunivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL1])).intValue());
      String nbnivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL1]);
      String nunivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL2])).intValue());
      String nbnivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(),
          (String) objects[CLIENTES_NBNIVEL2]);

      final StringBuilder rowLine = new StringBuilder();
      rowLine.append(nunivel0).append(nbnivel0).append(nunivel1).append(nbnivel1).append(nunivel2).append(nbnivel2);
      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);
    }
  }

  public void processRegistrer3(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL0])).intValue());
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL0]);
      String nunivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL1])).intValue());
      String nbnivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL1]);
      String nunivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL2])).intValue());
      String nbnivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(),
          (String) objects[CLIENTES_NBNIVEL2]);
      String nunivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL3])).intValue());
      String nbnivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL3]);

      final StringBuilder rowLine = new StringBuilder();
      rowLine.append(nunivel0).append(nbnivel0).append(nunivel1).append(nbnivel1).append(nunivel2).append(nbnivel2)
          .append(nunivel3).append(nbnivel3);
      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);
    }
  }

  public void processRegistrer4(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL0])).intValue());
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL0]);
      String nunivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL1])).intValue());
      String nbnivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL1]);
      String nunivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL2])).intValue());
      String nbnivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_5.value(),
          (String) objects[CLIENTES_NBNIVEL2]);
      String nunivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL3])).intValue());
      String nbnivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL3]);
      String nunivel4 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(),
          ((BigDecimal) (objects[CLIENTES_NUNIVEL4])).intValue());
      String nbnivel4 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(),
          (String) objects[CLIENTES_NBNIVEL4]);

      final StringBuilder rowLine = new StringBuilder();
      rowLine.append(nunivel0).append(nbnivel0).append(nunivel1).append(nbnivel1).append(nunivel2).append(nbnivel2)
          .append(nunivel3).append(nbnivel3).append(nunivel4).append(nbnivel4);
      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);
      writeLine(outWriter, cadena);
    }
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }
}
