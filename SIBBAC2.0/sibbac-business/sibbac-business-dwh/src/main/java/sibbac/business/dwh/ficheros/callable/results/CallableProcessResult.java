package sibbac.business.dwh.ficheros.callable.results;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import sibbac.business.dwh.ficheros.commons.dto.ProcessResultDTO;

public interface CallableProcessResult extends Callable<ProcessResultDTO> {

  public void setFuture(Future<ProcessResultDTO> future);

  public Future<ProcessResultDTO> getFuture();

}