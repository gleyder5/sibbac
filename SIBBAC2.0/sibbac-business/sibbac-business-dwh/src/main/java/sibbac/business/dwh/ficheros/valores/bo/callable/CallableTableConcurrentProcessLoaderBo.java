package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;
import sibbac.business.dwh.utils.DwhConstantes.TipoValoresProcess;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableTableConcurrentProcessLoaderBo extends AbstractCallableProcessBo {

  private static final Logger LOG = LoggerFactory.getLogger(CallableTableConcurrentProcessLoaderBo.class);

  private TipoValoresProcess tipoValoresProcess;

  /** Tamaño de la pagina */
  @Value("${fileName.dwh.partenon.read.batch:200}")
  private int pageSize;

  /** Número de Hilos de ejecucion */
  @Value("${fileName.dwh.partenon.threads:5}")
  private int threads;

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  @Value("${sibbac.batch.timeout: 90}")
  private int timeout;

  @Autowired
  private ApplicationContext ctx;

  private String currentTable;

  @Override
  public Boolean executeProcess() {
    Boolean result = false;

    currentTable = GenerateFilesUtilDwh.getValoresProcessName(tipoValoresProcess);

    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("Se inicia el proceso de carga para %s, se procesarán %d", currentTable,
          this.getListData().size()));
    }

    try {
      // Ejecutamos el proceso de carga de las tablas temporales
      
      long startTimeProcessFile = System.currentTimeMillis();
      
      executeValoresProcess();
    
      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("Se insertaton %d elementos en %s en %d ms", 
            this.getListData().size(), 
            currentTable,
            (System.currentTimeMillis() - startTimeProcessFile)));
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error("Se ha producido un error: ", e);
      result = false;
    }
   
    if (LOG.isInfoEnabled()) {
      LOG.info(String.format("Se termina el proceso de carga para %s", currentTable));
    }
    return result;
  }

  /**
   * Procesa las listas y las setea en sus respectivos BO para poder ejecutarlos
   * con posterioridad
   * @param valoresEmisorList
   */
  public void executeValoresProcess() throws SIBBACBusinessException {
    try {
      final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

      final Integer numPaginas = this.getListData().size() / pageSize;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * pageSize;
        Integer toIndex = ((pageNum + 1) * pageSize);

        if (toIndex > this.getListData().size() || fromIndex > this.getListData().size()) {
          toIndex = this.getListData().size();
        }

        final List<PartenonFileLineDTO> subList = this.getListData().subList(fromIndex, toIndex);

        // Obtenemos el tipo de proceso de carga de la funcion de utilidad
        final CallableProcessInterface callableBo = GenerateFilesUtilDwh.getValoresProcess(ctx, tipoValoresProcess);

        callableBo.setListData(subList);

        processInterfaces.add(callableBo);
      }

      // Lanzamos los hilos
      launchProcess(processInterfaces);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para la carga de registros", e);
    }
  }

  /**
   * Divide la lista principal a carga en paginas y las setea en n callables
   * para que sean procesados de forma paralela
   */
  public void launchProcess(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {
    try {
      final Integer numPaginas = processInterfaces.size() / threads;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threads;
        Integer toIndex = ((pageNum + 1) * threads);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        // Obtenemos una lista de callables que serán procesados por lotes
        final List<CallableProcessInterface> subList = processInterfaces.subList(fromIndex, toIndex);

        // Mandamos a ejecutar todos los callables de la lista anterior
        launchThreadBatch(subList);
      }
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(
          "Se ha producido un error durante la ejecución de hilos para la carga de registros", e);
    }
  }

  /**
   * Lanza todos los callables que se encuentran dentro de la lista
   * @param processInterfaces
   */
  public void launchThreadBatch(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threads,
        new NamedThreadFactory("batchInsert_" + currentTable));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {
      // Hacemos que se ejecuten siempre en paralelo n threadas
      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      callableBo.setProcessName(currentTable);

      futureList.add(callableBo);
    }

    // Evitamos que se añadan nuevos hilos al pool
    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  /**
   * Setea el tipo de proceso que será ejecutado
   * @param tipoValoresProcess
   */
  public void setTipoValoresProcess(TipoValoresProcess tipoValoresProcess) {
    this.tipoValoresProcess = tipoValoresProcess;
  }
}
