package sibbac.business.dwh.ficheros.valores.bo;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.valores.db.dao.ValoresOperadosDao;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresOperados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Component
public class FicherosValoresBo extends AbstractBo<ValoresOperados, String, ValoresOperadosDao> {

  private static final Logger LOG = LoggerFactory.getLogger(FicherosValoresBo.class);

  @Autowired
  private ValoresOperadosDao valoresOperadosDao;

  /**
   * Carga inicial de valores operados recogemos el campo cdisinvv de la tabla
   * TMCT0_DWH_EJECUCIONES y controlamos si existe o no en la tabla
   * TMCT0_DWH_VALORES_OPERADO si no existe lo insertamos y posteriormente
   * actuaizamos
   */
  public void cargaValores() throws SIBBACBusinessException {
    List<Object[]> valOperados = valoresOperadosDao.findValoresOperados();

    for (int i = 0; i < valOperados.size(); i++) {
      String cdisinvv = String.valueOf(valOperados.get(i));

      Integer countVal = valoresOperadosDao.countValoresOperados(cdisinvv);

      if (countVal == 0) {
        insertVal(cdisinvv);
        preUpdateVal(cdisinvv);
      }
    }
  }

  /**
   * Insertamos en la tabla TMCT0_DWH_VALORES_OPERADO
   * @param cdisinvv
   * @throws SIBBACBusinessException
   */
  public void insertVal(String cdisinvv) throws SIBBACBusinessException {
    try {
      ValoresOperados valor = new ValoresOperados();
      valor.setCdisinvv(cdisinvv);
      valor.setFhaudit(new Timestamp(System.currentTimeMillis()));
      valor.setUsuaudit("TaskGeneracionDWHValores-CargaInicial");
      valoresOperadosDao.save(valor);
    }
    catch (Exception e) {
      LOG.error("Error al insertar Valores Operados", e);
      throw new SIBBACBusinessException("Error al insertar Valores Operados", e);
    }
  }

  /**
   * Una vez insertado el registro en la tabla TMCT0_DWH_VALORES_OPERADO
   * Revisamos si existe en la tabla TMCT0_MAESTRO_VALORES recogemos los campos
   * fhiniciv, fhbajacv, nbtitrev, cdcifsoc
   * @param cdisinvv
   * @throws SIBBACBusinessException
   */
  public void preUpdateVal(String cdisinvv) throws SIBBACBusinessException {
    try {

      List<Object[]> getVolOpeUpdate = valoresOperadosDao.findValoresOperadosUpd(cdisinvv);

      for (Object[] objects : getVolOpeUpdate) {  
        final Date fhiniciv = (Date)objects[0];
        final Date fhbajacv = (Date)objects[1];
        final String nbtitrev = (String)objects[2];
        final String cdcifsoc = (String)objects[3];

        updateVal(fhiniciv, fhbajacv, nbtitrev, cdcifsoc, cdisinvv);
      }
    }
    catch (Exception e) {
      LOG.error("Error al obtener datos Valores Operados", e);
      throw new SIBBACBusinessException("Error al obtener datos Valores Operados", e);
    }
  }

  /**
   * Actualizamos el registro en la tabla TMCT0_DWH_VALORES_OPERADO
   * @param fhiniciv
   * @param fhbajacv
   * @param nbtitrev
   * @param cdcifsoc
   * @param cdisinvv
   * @throws SIBBACBusinessException
   */
  public void updateVal(Date fhiniciv, Date fhbajacv, String nbtitrev, String cdcifsoc, String cdisinvv)
      throws SIBBACBusinessException {
    try {
      valoresOperadosDao.updateValoresOperados(fhiniciv, fhbajacv, nbtitrev, cdcifsoc, cdisinvv);
      getCdcifsoc();
    }
    catch (Exception e) {
      LOG.error("Error al actualizar Valores Operados", e);
      throw new SIBBACBusinessException("Error al actualizar Valores Operados", e);
    }

  }

  /**
   * Recogemos el campo cdcifsoc de la tabla TMCT0_DWH_VALORES_OPERADO para
   * posteriormente recoger los campos cdisinvv y nbtitrev y volver a actualizar
   * el registro
   * @throws SIBBACBusinessException
   */
  public void getCdcifsoc() throws SIBBACBusinessException {
    try {
      List<Object[]> getCdcifsoc = valoresOperadosDao.findCdcifsoc();

      for (int i = 0; i < getCdcifsoc.size(); i++) {
        String cdcifsoc = String.valueOf(getCdcifsoc.get(i));
        List<Object[]> getDatosCdcifsoc = valoresOperadosDao.findDatosCdcifsoc(cdcifsoc);
        for (Object[] objects : getDatosCdcifsoc) {
          String cdisinvv = String.valueOf(objects[0]);
          String nbtitrev = String.valueOf(objects[1]);
          valoresOperadosDao.updateValoresOperados2(cdisinvv, nbtitrev, cdcifsoc);
        }
      }
    }
    catch (Exception e) {
      LOG.error("Error al actualizar Valores Operados", e);
      throw new SIBBACBusinessException("Error al actualizar Valores Operados", e);
    }
  }
}
