package sibbac.business.dwh.ficheros.valores.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.AbstractGenerateFile;

@Service("valoresFileGenerator")
public class GenerateValoresFile extends AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateValoresFile.class);

  private static final String CUCO_LINE_SEPARATOR = "\r\n";

  private List<Object[]> validationValoresList;

  @Override
  public String getLineSeparator() {
    return CUCO_LINE_SEPARATOR;
  }

  public void loadValidationValores(List<String> validationStringList) {
    if (validationStringList != null && !validationStringList.isEmpty()) {
      validationValoresList = new ArrayList<>();
      for (String string : validationStringList) {
        validationValoresList.add(new Object[] { string });
      }
    }
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return getPageList(validationValoresList, pageable);
  }

  /**
   * Obtiene una pagina de la lista de entrada
   * @param listData
   * @param pageable
   * @return
   * @throws SIBBACBusinessException
   */
  private List<Object[]> getPageList(List<Object[]> listData, Pageable pageable) {
    int pageSize = pageable.getPageSize();
   
    int pageNum = pageable.getPageNumber();

    final Integer fromIndex = pageNum * pageSize;
    Integer toIndex = ((pageNum + 1) * pageSize);

    if (toIndex > listData.size() && fromIndex > listData.size()) {
      return null;
    }
    else if (toIndex > listData.size()) {
      return listData.subList(fromIndex, listData.size());
    }
    else {
      return listData.subList(fromIndex, toIndex);
    }
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {

    if (validationValoresList != null && !validationValoresList.isEmpty()) {
      return validationValoresList.size();
    }
    else {
      return 0;
    }
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    StringBuilder line = new StringBuilder();
    for (int i = 0; i < fileData.size(); i++) {
      if (String.valueOf(fileData.get(i)) != null) {
        Object[] obj = fileData.get(i);
        String registro = String.valueOf(obj[0]);
        line.setLength(0);
        line.append(registro);
        String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), line);
        writeLine(outWriter, cadena);
      }
      else {
        throw new IOException("Se ha encontado un objeto a nulo");
      }
    }
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }
}
