package sibbac.business.dwh.tasks;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.dwh.ficheros.valores.bo.CargaValoresBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DWH.NAME, name = Task.GROUP_DWH.JOB_CARGA_VALORES, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 1 ? * MON-FRI *")
public class TaskCargaValores extends WrapperTaskConcurrencyPrevent {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOG = LoggerFactory.getLogger(TaskCargaValores.class);

  @Autowired
  private CargaValoresBo cargaValoresBo;

  @Override
  public void executeTask() throws Exception {
    try {
      if (LOG.isInfoEnabled()) {
        LOG.debug("Se inicia la tarea de Carga de Valores");
      }

      /** Limpiamos las tablas intermedias */
      cargaValoresBo.cleanTables();

      /** Iniciamos el proceso de Carga de las tablas intermedias */
      cargaValoresBo.cargaValoresTablasIntermedias();

      /** Iniciamos el proceso de Carga desde Partenon */
      cargaValoresBo.cargaValoresPartenon();

      /** Iniciamos la carga MaestroValores desde SIBBAC */
      cargaValoresBo.cargaValoresSibbac();

      /** Movemos el fichero a tratados */
      cargaValoresBo.muevePartenonFile();
    }
    catch (Exception e) {
      LOG.error("Error al ejecutar TaskCargaValores", e);
      throw new SIBBACBusinessException("Error al ejecutar Carga de Valores", e);
    }
    finally {
      if (LOG.isInfoEnabled()) {
        LOG.info("Se terminala tarea de Carga de Valores");
      }
    }
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_CARGA_VALORES;
  }
}
