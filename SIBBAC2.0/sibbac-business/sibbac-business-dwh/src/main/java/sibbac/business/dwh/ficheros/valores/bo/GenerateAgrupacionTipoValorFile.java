package sibbac.business.dwh.ficheros.valores.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.AgrupacionTipoValorDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("generateAgrupacionTipoValorFile")
public class GenerateAgrupacionTipoValorFile extends AbstractGenerateFile {

  private static final String VALORES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateAgrupacionTipoValorFile.class);

  /**
   * DAO de Acceso a Base de Datos 
   */
  @Autowired
  private AgrupacionTipoValorDao agrupacionTipoValorDao;

  @Override
  public String getLineSeparator() {
    return VALORES_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return agrupacionTipoValorDao.findAllListAgrupTipoValor(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return agrupacionTipoValorDao.countAllListAgrupTipoValor();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    String codG = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "00");
    String nbG = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), "NO EXISTE DESCRIPCION");
    String cdG2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "95");
    String nbG2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), "NO EXISTE DESCRIPCION");
    String codR = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(), "V");
    String nbR = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "VALOR DE RENTA VARIABLE");
    String linea1 = codG + nbG + cdG2 + nbG2 + codR + nbR;

    String cadenaPrev = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), linea1);
    writeLine(outWriter, cadenaPrev);

    String codG3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "70");
    String nbG3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), "NO EXISTE DESCRIPCION");
    String cdG4 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "95");
    String nbG4 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), "NO EXISTE DESCRIPCION");
    String codR3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(), "V");
    String nbR3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_2.value(), "VALOR DE RENTA VARIABLE");
    String linea2 = codG3 + nbG3 + cdG4 + nbG4 + codR3 + nbR3;

    String cadenaPrev2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), linea2);
    writeLine(outWriter, cadenaPrev2);

    for (Object[] objects : fileData) {
      if(!objects[0].equals("null") && !objects[1].equals("null") && !objects[2].equals("null") && !objects[3].equals("null") && !objects[4].equals("null") && !objects[5].equals("null"))
      {
        String codGrupoValor = String.format(DwhConstantes.PaddingFormats.PADDIND_NUM0_2.value(),
            Integer.parseInt((String) objects[0]));
        String nomGrupoValor = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), objects[1]);
        String codTipoValor = String.format(DwhConstantes.PaddingFormats.PADDIND_NUM0_2.value(),
            Integer.parseInt((String) objects[2]));
        String nomTipoValor = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), objects[3]);
        String codTipoRenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_1.value(), objects[4]);
        String descTipoRenta = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_31.value(), objects[5]);

        final StringBuilder rowLine = new StringBuilder();

        rowLine.append(codGrupoValor).append(nomGrupoValor).append(codTipoValor).append(nomTipoValor).append(codTipoRenta)
            .append(descTipoRenta);

        String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

        writeLine(outWriter, cadena);
      }
    }
  }

}
