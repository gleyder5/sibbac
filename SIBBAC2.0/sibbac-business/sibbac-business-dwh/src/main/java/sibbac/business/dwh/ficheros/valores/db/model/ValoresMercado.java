package sibbac.business.dwh.ficheros.valores.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Table(name = "TMCT0_VALORES_MERCADO")
@Entity
public class ValoresMercado implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  @Column(name = "CDEMISIO", nullable = false, length = 12)
  private String cdemisio;

  @Column(name = "CDMERCAD", nullable = false, length = 3)
  private String cdmercad;

  @Column(name = "CDMEREXT", nullable = false, length = 4)
  private String cdmerext;

  @Column(name = "CDMONISO", nullable = false, length = 3)
  private String cdmoniso;

  @Column(name = "CDORGANT", nullable = false, length = 4)
  private String cdorgant;

  @Column(name = "CDREUTER", nullable = false, length = 12)
  private String cdreuter;

  @Column(name = "CDVALMDO", nullable = false, length = 3)
  private String cdvalmdo;

  @Column(name = "CDSECTOR", nullable = false, length = 2)
  private String cdsector;

  @Column(name = "FEINIVIG", nullable = false, length = 10)
  private String feinivig;

  @Column(name = "FEFINVIG", nullable = false, length = 10)
  private String fefinvig;

  @Column(name = "CDBOLSA", nullable = false, length = 3)
  private String cdbolsa;

  @Column(name = "NULOTE", nullable = false, length = 5)
  private String nulote;

  @Column(name = "INDCOTIZ", nullable = false, length = 1)
  private String indcotiz;

  @Column(name = "FHAUDIT", nullable = false, length = 26)
  private Timestamp fhaudit;

  @Column(name = "USUAUDIT", nullable = false, length = 100)
  private String usuaudit;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getCdemisio() {
    return cdemisio;
  }

  public void setCdemisio(String cdemisio) {
    this.cdemisio = cdemisio;
  }

  public String getCdmercad() {
    return cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public String getCdmerext() {
    return cdmerext;
  }

  public void setCdmerext(String cdmerext) {
    this.cdmerext = cdmerext;
  }

  public String getCdmoniso() {
    return cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public String getCdorgant() {
    return cdorgant;
  }

  public void setCdorgant(String cdorgant) {
    this.cdorgant = cdorgant;
  }

  public String getCdreuter() {
    return cdreuter;
  }

  public void setCdreuter(String cdreuter) {
    this.cdreuter = cdreuter;
  }

  public String getCdvalmdo() {
    return cdvalmdo;
  }

  public void setCdvalmdo(String cdvalmdo) {
    this.cdvalmdo = cdvalmdo;
  }

  public String getCdsector() {
    return cdsector;
  }

  public void setCdsector(String cdsector) {
    this.cdsector = cdsector;
  }

  public String getFeinivig() {
    return feinivig;
  }

  public void setFeinivig(String feinivig) {
    this.feinivig = feinivig;
  }

  public String getFefinvig() {
    return fefinvig;
  }

  public void setFefinvig(String fefinvig) {
    this.fefinvig = fefinvig;
  }

  public String getCdbolsa() {
    return cdbolsa;
  }

  public void setCdbolsa(String cdbolsa) {
    this.cdbolsa = cdbolsa;
  }

  public String getNulote() {
    return nulote;
  }

  public void setNulote(String nulote) {
    this.nulote = nulote;
  }

  public String getIndcotiz() {
    return indcotiz;
  }

  public void setIndcotiz(String indcotiz) {
    this.indcotiz = indcotiz;
  }

  public Timestamp getFhaudit() {
    return fhaudit != null ? (Timestamp) fhaudit.clone() : null;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = (fhaudit != null ? (Timestamp) fhaudit.clone() : null);
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public static synchronized ValoresMercado createValoresMercado(PartenonFileLineDTO element) {
    ValoresMercado vMercado = new ValoresMercado();

    String linea = element.getLine();

    vMercado.setId(element.getIdLine());

    String cdemisio = linea.substring(1, 13).trim();
    vMercado.setCdemisio(cdemisio);

    String cdmercad = linea.substring(13, 16).trim();
    vMercado.setCdmercad(cdmercad);

    String cdmerext = linea.substring(16, 20).trim();
    vMercado.setCdmerext(cdmerext);

    String cdmoniso = linea.substring(20, 23).trim();
    vMercado.setCdmoniso(cdmoniso);

    String cdorgant = linea.substring(23, 27).trim();
    vMercado.setCdorgant(cdorgant);

    String cdreuter = linea.substring(27, 39).trim();
    vMercado.setCdreuter(cdreuter);

    String cdvalmdo = linea.substring(39, 42).trim();
    vMercado.setCdvalmdo(cdvalmdo);

    String cdsector = linea.substring(42, 44).trim();
    vMercado.setCdsector(cdsector);

    String feinivig = linea.substring(44, 54).trim();
    vMercado.setFeinivig(feinivig);

    String fefinvig = linea.substring(54, 64).trim();
    vMercado.setFefinvig(fefinvig);

    String cdbolsa = linea.substring(64, 67).trim();
    vMercado.setCdbolsa(cdbolsa);

    String nulote = linea.substring(67, 72).trim();
    vMercado.setNulote(nulote);

    String indcotiz = linea.substring(72).trim();
    vMercado.setIndcotiz(indcotiz);

    vMercado.setFhaudit(new Timestamp(new Date().getTime()));
    vMercado.setUsuaudit(" ");

    return vMercado;

  }

}
