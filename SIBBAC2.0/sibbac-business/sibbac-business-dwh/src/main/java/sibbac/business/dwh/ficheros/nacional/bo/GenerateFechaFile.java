package sibbac.business.dwh.ficheros.nacional.bo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.aop.logging.SIBBACLogging;
import sibbac.common.file.constants.FileResult;

/**
 * @author jgarvila
 *
 */
@Component
public class GenerateFechaFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateFechaFile.class);

  private static final String LINE_SEPARATOR = "\r\n";

  /**
   * Directorio de escritura de DWH
   */
  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String outPutDir;

  /**
   * Nombre del fichero de Fecha
   */
  @Value("${dwh.ficheros.nacional.fecha.file:fdwh0fei.txt}")
  private String fileNameFecha;

  /**
   * Nombre fichero de Fecha Desde
   */
  @Value("${dwh.ficheros.nacional.fecha_desde.file:fdwh0fex.txt}")
  private String fileNameFechaDesde;

  /**
   * Fichero Nacional
   */
  @Autowired
  private FicheroNacionalBo ficheroNacionalBo;

  /**
   * Método para generar fichero de Fecha
   * 
   * @param fechaEjecucion
   * @param tipoFichero
   * @throws SIBBACBusinessException
   */
  @SIBBACLogging
  public ProcessFileResultDTO generateFileFicheroFecha(Date fechaEjecucion, String tipoFichero) {
    final ProcessFileResultDTO processFileResultDTO = new ProcessFileResultDTO();

    // Creamos los directorios si no existen
    createOutPutDir();

    final String fileName = getFileNameFromTipoFichero(tipoFichero);

    // Seteamos el nombre del fichero en el resultado
    processFileResultDTO.setFileNameProcess(fileName);

    final Date fechaInicio = ficheroNacionalBo.extraccionParametrizacion(fechaEjecucion);

    if (fechaInicio != null) {
      BufferedWriter outWriter = null;
      FileWriter fileWriter = null;
      processFileResultDTO.setFileResult(FileResult.GENETARATED_EMPTY);
      try {
        fileWriter = new FileWriter(getFullFileAccesPath(fileName));
        outWriter = new BufferedWriter(fileWriter);

        if (tipoFichero.equals("fecha")) {
          final String cadenaFechaInicio = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(),
              ficheroNacionalBo.formatDate("yyyyMMdd", fechaInicio));
          outWriter.write(cadenaFechaInicio);
          outWriter.write(LINE_SEPARATOR);

          // Fichero generado correctamente
          processFileResultDTO.setFileResult(FileResult.GENETARATED_OK);
        }
        else if (tipoFichero.equals("fechaDesde")) {
          final StringBuilder fechaDesdeHasta = new StringBuilder();
          Date desde = ficheroNacionalBo.getDateBySubstractWorkingDays(fechaEjecucion, 9);
          fechaDesdeHasta.append(ficheroNacionalBo.formatDate("dd/MM/yyyy", desde))
              .append(ficheroNacionalBo.formatDate("dd/MM/yyyy", fechaEjecucion));
          String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), fechaDesdeHasta);
          outWriter.write(cadena);

          // Fichero generado correctamente
          processFileResultDTO.setFileResult(FileResult.GENETARATED_OK);
        }
        else {
          LOG.debug("Error, el fichero no es ni fichero fecha ni fichero desde ");
        }
      }
      catch (IOException e) {
        processFileResultDTO.setFileResult(FileResult.GENETARATED_KO);
        LOG.error("Error al generar el fichero de Ejecuciones de nacional", e.getMessage(), e);
      }
      finally {
        if (outWriter != null) {
          try {
            outWriter.close();
          }
          catch (IOException e) {
            LOG.error("No se puede cerrar el buffer de escritura", e);
          }
        }

        if (fileWriter != null) {
          try {
            fileWriter.close();
          }
          catch (IOException e) {
            LOG.error("No se puede cerrar el writer de escritura", e);
          }
        }
      }
    }
    else {
      processFileResultDTO.setFileResult(FileResult.GENETARATED_KO);
      LOG.info("No se han recuperado valores para para generar el fichero por lo que no se ha generado");
    }

    return processFileResultDTO;
  }

  /**
   * Obtiene el nombre del fichero
   * @param tipoFichero
   * @return
   */
  private String getFileNameFromTipoFichero(String tipoFichero) {
    if (tipoFichero.equals("fecha")) {
      return this.fileNameFecha;
    }
    else if (tipoFichero.equals("fechaDesde")) {
      return this.fileNameFechaDesde;
    }
    else {
      LOG.error("Error, el fichero no es ni fichero fecha ni fichero desde ");
      return null;
    }
  }

  /**
   * 
   * @param tipoFichero
   * @return String
   */
  private String getFullFileAccesPath(String fileName) {
    if (outPutDir.endsWith(File.separator)) {
      return outPutDir + fileName;
    }
    else {
      return outPutDir + File.separator + fileName;
    }
  }

  /**
   * Creación del directorio del fichero
   */
  private void createOutPutDir() {
    final File outDir = new File(this.outPutDir);
    if (outDir.mkdirs()) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Se ha creado la estructura de directorios " + this.outPutDir);
      }
    }
    else {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Ya existe la estructura de directorios " + this.outPutDir);
      }
    }
  }
}
