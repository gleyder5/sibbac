package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresEmisionBo;
import sibbac.business.dwh.ficheros.valores.dto.EmisionMaestroValoresDTO;
import sibbac.business.dwh.utils.DwhConstantes.TipoValoresValidation;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableCargaValoresPartenonLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private ValoresEmisionBo valoresEmisionBo;

  private List<EmisionMaestroValoresDTO> validationList;

  private TipoValoresValidation tipoValoresValidation;

  private static final Logger LOG = LoggerFactory.getLogger(CallableTableConcurrentProcessLoaderBo.class);

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    try {
      long initTimeStampAlta = System.currentTimeMillis();
      String funcion = null;

      valoresEmisionBo.initialiceMaps();

      switch (tipoValoresValidation) {
        case VALIDACION_ALTA:
          valoresEmisionBo.funcionAlta(validationList);
          funcion = "ALTA";
          break;
        case VALIDACION_MOD:
          valoresEmisionBo.funcionMod(validationList);
          funcion = "MODIFICACION";
          break;
        case VALIDACION_BAJA:
          valoresEmisionBo.funcionBaja(validationList);
          funcion = "BAJA";
          break;
        default:
          break;
      }

      valoresEmisionBo.destroyMaps();

      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("TERMINADA validacion de %s en %d milisegundos", funcion,
            (System.currentTimeMillis() - initTimeStampAlta)));
      }
    }
    catch (SIBBACBusinessException exception) {
      LOG.error("Error en el proceso de carga en MAESTRO_VALORES", exception);
    }
    return true;
  }

  public List<EmisionMaestroValoresDTO> getValidationList() {
    return validationList;
  }

  public void setValidationList(List<EmisionMaestroValoresDTO> validationList) {
    this.validationList = validationList;
  }

  public TipoValoresValidation getTipoValoresValidation() {
    return tipoValoresValidation;
  }

  public void setTipoValoresValidation(TipoValoresValidation tipoValoresValidation) {
    this.tipoValoresValidation = tipoValoresValidation;
  }
}
