package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.Pais;
import sibbac.business.dwh.ficheros.valores.db.model.PaisPK;

/**
 * The Interface PaisDao.
 */
@Repository
public interface PaisDao extends JpaRepository<Pais, PaisPK> {

  /**
   * Gets the cdisonum and nbpaisa.
   *
   * @return the cdisonum and nbpaisa
   */
  @Query(value = "SELECT CDISONUM, NBPAIS FROM TMCT0PAISES WHERE CDESTADO='A' AND CDISONUM<>'' ORDER BY NBPAIS ASC", nativeQuery = true)
  public List<Object[]> getCdisonumAndNbpaisa();

  /**
   * Validate paises.
   *
   * @return the list
   */
  @Query(value = "SELECT CDISONUM FROM TMCT0PAISES WHERE CDESTADO='A' ORDER BY NBPAIS ASC", nativeQuery = true)
  public List<Object> validatePaises();

  /**
   * Checks if is valid country.
   *
   * @param country the country
   * @return the list
   */
  @Query(value = "SELECT CDISOALF2 FROM TMCT0PAISES WHERE CDESTADO='A' AND CDISONUM = ?1", nativeQuery = true)
  public List<Object> isValidCountry(String country);

}
