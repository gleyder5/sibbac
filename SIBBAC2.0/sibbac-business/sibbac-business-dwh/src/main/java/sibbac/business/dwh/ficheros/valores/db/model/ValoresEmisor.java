package sibbac.business.dwh.ficheros.valores.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Table(name = "TMCT0_VALORES_EMISOR")
@Entity
public class ValoresEmisor implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  @Column(name = "CDEMISIO", nullable = false, length = 12)
  private String cdemisio;

  @Column(name = "CDPAIS", nullable = false, length = 2)
  private String cdpais;

  @Column(name = "CDFISCAL", nullable = false, length = 1)
  private String cdfiscal;

  @Column(name = "CDNIF", nullable = false, length = 9)
  private String cdnif;

  @Column(name = "DSEMISOR", nullable = false, length = 32)
  private String cdemisor;

  @Column(name = "FHAUDIT", nullable = false, length = 26)
  private Timestamp fhaudit;

  @Column(name = "USUAUDIT", nullable = false, length = 100)
  private String usuaudit;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getCdemisio() {
    return cdemisio;
  }

  public void setCdemisio(String cdemisio) {
    this.cdemisio = cdemisio;
  }

  public String getCdpais() {
    return cdpais;
  }

  public void setCdpais(String cdpais) {
    this.cdpais = cdpais;
  }

  public String getCdfiscal() {
    return cdfiscal;
  }

  public void setCdfiscal(String cdfiscal) {
    this.cdfiscal = cdfiscal;
  }

  public String getCdnif() {
    return cdnif;
  }

  public void setCdnif(String cdnif) {
    this.cdnif = cdnif;
  }

  public String getCdemisor() {
    return cdemisor;
  }

  public void setCdemisor(String cdemisor) {
    this.cdemisor = cdemisor;
  }

  public Timestamp getFhaudit() {
    return fhaudit != null ? (Timestamp) fhaudit.clone() : null;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = (fhaudit != null ? (Timestamp) fhaudit.clone() : null);
  }

  public String getUsuaudit() {
    return usuaudit;
  }

  public void setUsuaudit(String usuaudit) {
    this.usuaudit = usuaudit;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }
  
  /**
   * Obtiene un ValorEmisor a partir de una linea
   * @param linea
   * @return
   */
  public static synchronized ValoresEmisor createValoresEmisor(PartenonFileLineDTO element) {
    ValoresEmisor vEmisor = new ValoresEmisor();

    
    String linea = element.getLine();
    
    vEmisor.setId(element.getIdLine());
    
    String cdemisio = linea.substring(1, 13).trim();
    vEmisor.setCdemisio(cdemisio);

    String cdpais = linea.substring(13, 15).trim();
    vEmisor.setCdpais(cdpais);

    String cdfiscal = linea.substring(15, 16).trim();
    vEmisor.setCdfiscal(cdfiscal);

    String cdnif = linea.substring(16, 25).trim();
    vEmisor.setCdnif(cdnif);

    String cdemisor = linea.substring(25).trim();
    vEmisor.setCdemisor(cdemisor);

    vEmisor.setFhaudit(new Timestamp(new Date().getTime()));
    vEmisor.setUsuaudit(" ");

    return vEmisor;

  }


}
