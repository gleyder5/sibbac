package sibbac.business.dwh.ficheros.valores.db.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.dwh.ficheros.valores.db.model.GruposValores;

public interface GruposValoresDao extends JpaRepository<GruposValores, BigInteger> {
  
  public static final String GRUPO_VALORES_QUERY = "SELECT CDISINGR, NBTITGRU FROM TMCT0_DWH_VALORES_OPERADO GROUP BY CDISINGR, NBTITGRU";

  public static final String GRUPO_VALORES_PAGE_QUERY = GRUPO_VALORES_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHgrupoValoresDao con Paginación
   */
  @Query(value = GRUPO_VALORES_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListGrupoValores(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHgrupoValoresDao sin
   * paginación
   * @return
   */
  @Query(value = "SELECT COUNT(*) FROM (" + GRUPO_VALORES_QUERY + ")", nativeQuery = true)
  public Integer countAllListGrupoValores();

  /**
   * Query para recoger los valores ID, NB_DESCRIPCION de la tabla TMCT0_GRUPOS_VALORES
   */
  @Query(value = "SELECT ID, NB_DESCRIPCION FROM TMCT0_GRUPOS_VALORES ORDER BY ID ASC", nativeQuery = true)
  public List<Object[]> getIdAndNbdescipcion();

}
