package sibbac.business.dwh.ficheros.valores.bo.callable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.valores.bo.ValoresWarrantBo;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresWarrant;
import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableTableWarrantLoaderBo extends AbstractCallableProcessBo {

  @Autowired
  private ValoresWarrantBo valoresWarrantBo;

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {
    valoresWarrantBo.saveBulkValoresWarrant(toValoresWarrantEntity());
    return true;
  }

  /**
   * Convierte una lista de String a una lista de Entidades
   * @param emisionStringList
   * @return
   */
  private List<ValoresWarrant> toValoresWarrantEntity() {
    final List<ValoresWarrant> entities = new ArrayList<>();

    for (PartenonFileLineDTO element : listData) {
      entities.add(ValoresWarrant.createValoresWarrant(element));
    }
    return entities;
  }
}
