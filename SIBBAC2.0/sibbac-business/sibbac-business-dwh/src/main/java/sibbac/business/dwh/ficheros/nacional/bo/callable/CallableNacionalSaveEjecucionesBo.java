package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.AbstractCallableProcessBo;
import sibbac.business.dwh.ficheros.nacional.bo.NacionalSaveEjecucionesBo;
import sibbac.business.dwh.ficheros.nacional.db.model.Tmct0DwhEjecuciones;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

public class CallableNacionalSaveEjecucionesBo extends AbstractCallableProcessBo {

  @Autowired
  private NacionalSaveEjecucionesBo nacionalSaveEjecucionesBo;

  private List<Tmct0DwhEjecuciones> ejecucionesList;

  private static final Logger LOG = LoggerFactory.getLogger(CallableNacionalSaveEjecucionesBo.class);

  /**
   * Ejecuta el proceso
   */
  @Override
  public Boolean executeProcess() {

    try {
      long initTimeStampAlta = System.currentTimeMillis();

      nacionalSaveEjecucionesBo.saveEjecuciones(ejecucionesList);

      if (LOG.isInfoEnabled()) {
        LOG.info(String.format("TERMINADO save de ejecuciones en %d milisegundos",
            (System.currentTimeMillis() - initTimeStampAlta)));
      }
    }
    catch (Exception e) {
    }
    return true;
  }

  public List<Tmct0DwhEjecuciones> getEjecucionesList() {
    return ejecucionesList;
  }

  public void setEjecucionesList(List<Tmct0DwhEjecuciones> ejecucionesList) {
    this.ejecucionesList = ejecucionesList;
  }
}
