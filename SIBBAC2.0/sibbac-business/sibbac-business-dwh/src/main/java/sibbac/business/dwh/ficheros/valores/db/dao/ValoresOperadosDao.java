package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.valores.db.model.ValoresOperados;

@Repository
public interface ValoresOperadosDao extends JpaRepository<ValoresOperados, String> {

  /**
   * Query para obtener todos los valores operados
   * @return List<Object[]>
   */
  @Query(value = "SELECT CDISINVV FROM TMCT0_DWH_EJECUCIONES GROUP BY CDISINVV", nativeQuery = true)
  public List<Object[]> findValoresOperados();

  /**
   * Query para contar los valores operados en tabla tmct0_dwh_valores_operados
   * @return List<Object[]>
   */
  @Query(value = "SELECT COUNT(*) FROM TMCT0_DWH_VALORES_OPERADO WHERE CDISINVV = :cdisinvv", nativeQuery = true)
  public Integer countValoresOperados(@Param("cdisinvv") String cdisinvv);

  /**
   * Query para obtener los valores operados en tabla tmct0_dwh_valores_operados
   * @return List<Object[]>
   */
  @Query(value = "SELECT FHINICIV, FHBAJACV, NBTITREV, CDCIFSOC FROM TMCT0_MAESTRO_VALORES WHERE CDISINVV = :cdisinvv ORDER BY FHBAJACV DESC, FHINICIV DESC FETCH FIRST ROW ONLY", nativeQuery = true)
  public List<Object[]> findValoresOperadosUpd(@Param("cdisinvv") String cdisinvv);

  /**
   * Query para actualizar los valores operados en tabla
   * tmct0_dwh_valores_operados
   * @return List<Object[]>
   */
  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_DWH_VALORES_OPERADO SET FHINICIV = :fhiniciv, "
      + "FHBAJACV = :fhbajacv, NBTITREV = :nbtitrev, CDCIFSOC = :cdcifsoc, "
      + "FHAUDIT = CURRENT_DATE, USUAUDIT = 'TaskGeneracionDWHValores-ActIsin' "
      + "WHERE CDISINVV = :cdisinvv", nativeQuery = true)
  public void updateValoresOperados(@Param("fhiniciv") Date fhiniciv, @Param("fhbajacv") Date fhbajacv,
      @Param("nbtitrev") String nbtitrev, @Param("cdcifsoc") String cdcifsoc, @Param("cdisinvv") String cdisinvv);

  /**
   * Query para obtener los cdcifsoc de la tabla tmct0_dwh_valores_operados
   * @return List<Object[]>
   */
  @Query(value = "SELECT CDCIFSOC FROM TMCT0_DWH_VALORES_OPERADO GROUP BY CDCIFSOC", nativeQuery = true)
  public List<Object[]> findCdcifsoc();

  /**
   * Query para obtener datos por cdcifsoc de la tabla
   * tmct0_dwh_valores_operados
   * @return List<Object[]>
   */
  @Query(value = "SELECT CDISINVV, NBTITREV FROM TMCT0_DWH_VALORES_OPERADO WHERE CDCIFSOC = :cdcifsoc "
      + "ORDER BY CDCIFSOC, FHBAJACV DESC, FHINICIV DESC, NBTITREV FETCH FIRST ROW ONLY", nativeQuery = true)
  public List<Object[]> findDatosCdcifsoc(@Param("cdcifsoc") String cdcifsoc);

  /**
   * Query para actualizar datos por cdcifsoc de la tabla
   * tmct0_dwh_valores_operados
   * @return List<Object[]>
   */
  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_DWH_VALORES_OPERADO SET CDISINGR = :cdisinvv,"
      + " NBTITGRU = :nbtitrev, FHAUDIT = CURRENT_DATE, USUAUDIT = 'TaskGeneracionDWHValores-ActGrup'"
      + " WHERE CDCIFSOC = :cdcifsoc", nativeQuery = true)
  public void updateValoresOperados2(@Param("cdisinvv") String cdisinvv, @Param("nbtitrev") String nbtitrev,
      @Param("cdcifsoc") String cdcifsoc);

}
