package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.dwh.ficheros.valores.db.model.ValoresGrupo;

public interface ValoresGrupoDao extends JpaRepository<ValoresGrupo, String> {

  /**
   * Query para recoger los valores CDGRSIBV, NBGRSIBV de la tabla
   * TMCT0_VALORES_GRUPO
   */
  @Query(value = "SELECT CDGRSIBV, NBGRSIBV FROM TMCT0_VALORES_GRUPO ORDER BY CAST(CDGRSIBV as INTEGER)", nativeQuery = true)
  public List<Object[]> getCdgrisivAndNbgrisibv();

}
