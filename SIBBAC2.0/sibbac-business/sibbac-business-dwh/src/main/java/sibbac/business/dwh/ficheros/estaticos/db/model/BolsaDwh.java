package sibbac.business.dwh.ficheros.estaticos.db.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class that represents a
 * {@link sibbac.business.wrappers.database.model.CodigoTptiprep }. Entity:
 * "CodigoTptiprep".
 * 
 * @version 1.0
 * @since 1.0
 */
//
@Table(name = "TMCT0_DWH_BOLSA")
@Entity
public class BolsaDwh implements java.io.Serializable {

  private static final long serialVersionUID = -840277152853228871L;

  @Id
  @Column(name = "ID_BOLSA", nullable = false, length = 3)
  private String idBolsa;

  @Column(name = "NOMBRE_BOLSA", nullable = false, length = 16)
  private String nombreBolsa;

  @Column(name = "CODIGO_PAIS", nullable = false, length = 3)
  private String codigoPais;

  @Column(name = "USUARIO_ALTA", nullable = false, length = 256)
  private String usuarioAlta;

  @Column(name = "USUARIO_MOD", nullable = true, length = 256)
  private String usuarioMod;

  @Column(name = "USUARIO_BAJA", nullable = true, length = 256)
  private String usuariobaja;

  @Column(name = "FECHA_ALTA", nullable = false)
  private Timestamp fechaAlta;

  @Column(name = "FECHA_BAJA", nullable = false)
  private Timestamp fechaBaja;

  @Column(name = "FECHA_MOD", nullable = true)
  private Timestamp fechaMod;

  /**
   * @return the idBolsa
   */
  public String getIdBolsa() {
    return idBolsa;
  }

  /**
   * @param idBolsa the idBolsa to set
   */
  public void setIdBolsa(String idBolsa) {
    this.idBolsa = idBolsa;
  }

  /**
   * @return the nombreBolsa
   */
  public String getNombreBolsa() {
    return nombreBolsa;
  }

  /**
   * @param nombreBolsa the nombreBolsa to set
   */
  public void setNombreBolsa(String nombreBolsa) {
    this.nombreBolsa = nombreBolsa;
  }

  /**
   * @return the codigoPais
   */
  public String getCodigoPais() {
    return codigoPais;
  }

  /**
   * @param codigoPais the codigoPais to set
   */
  public void setCodigoPais(String codigoPais) {
    this.codigoPais = codigoPais;
  }

  /**
   * @return the usuariobaja
   */
  public String getUsuariobaja() {
    return usuariobaja;
  }

  /**
   * @param usuariobaja the usuariobaja to set
   */
  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }

  /**
   * @return the usuarioAlta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * @param usuarioAlta the usuarioAlta to set
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * @return the usuarioMod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * @param usuarioMod the usuarioMod to set
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * @return the fechaAlta
   */
  public Timestamp getFechaAlta() {
    return (Timestamp) fechaAlta.clone();
  }

  /**
   * @param fechaAlta the fechaAlta to set
   */
  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (Timestamp) fechaAlta.clone();
  }

  /**
   * @return the fechaMod
   */
  public Timestamp getFechaMod() {
    return (Timestamp) fechaMod.clone();
  }

  /**
   * @param fechaMod the fechaMod to set
   */
  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (Timestamp) fechaMod.clone();
  }

  /**
   * @return the fechaMod
   */
  public Timestamp getFechaBaja() {
    return (Timestamp) fechaMod.clone();
  }

  /**
   * @param fechaMod the fechaMod to set
   */
  public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaMod = (Timestamp) fechaBaja.clone();
  }

}