package sibbac.business.dwh.ficheros.clientes.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.clientes.db.dao.DWHGrupoClientesEstaticosDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("grupoClientesFileGenerator")
public class GenerateGrupoClientesEstaticosFile extends AbstractGenerateFile {

  private static final String CLIENTES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateGrupoClientesEstaticosFile.class);

  private static int CLIENTES_CDGRUPOA = 0;
  private static int CLIENTES_NBGRUPOA = 1;
  private static int CLIENTES_NUNIVEL0 = 2;
  private static int CLIENTES_NBNIVEL0 = 3;
  private static int CLIENTES_NUNIVEL1 = 4;
  private static int CLIENTES_NBNIVEL1 = 5;
  private static int CLIENTES_NUNIVEL2 = 6;
  private static int CLIENTES_NBNIVEL2 = 7;
  private static int CLIENTES_NUNIVEL3 = 8;
  private static int CLIENTES_NBNIVEL3 = 9;
  private static int CLIENTES_NUNIVEL4 = 10;
  private static int CLIENTES_NBNIVEL4 = 11;

  @Override
  public String getLineSeparator() {
    return CLIENTES_LINE_SEPARATOR;
  }

  @Autowired
  private DWHGrupoClientesEstaticosDao dwhGrupoClientesEstaticosDao;

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return dwhGrupoClientesEstaticosDao.findAllListGrupoClientesEstaticos(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return dwhGrupoClientesEstaticosDao.countAllListGrupoClientesEstaticos();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {

      Integer cdgrupoo = (Integer) (objects[CLIENTES_CDGRUPOA]);
      String nbgrupoo = (String) objects[CLIENTES_NBGRUPOA];
      Integer nunivel00 = (Integer) (objects[CLIENTES_NUNIVEL0]);
      String nbnivel00 = (String) objects[CLIENTES_NBNIVEL0];
      Integer nunivel11 = (Integer) (objects[CLIENTES_NUNIVEL1]);
      String nbnivel11 = (String) objects[CLIENTES_NBNIVEL1];
      Integer nunivel22 = (Integer) (objects[CLIENTES_NUNIVEL2]);
      String nbnivel22 = (String) objects[CLIENTES_NBNIVEL2];
      Integer nunivel33 = (Integer) (objects[CLIENTES_NUNIVEL3]);
      String nbnivel33 = (String) objects[CLIENTES_NBNIVEL3];
      Integer nunivel44 = (Integer) (objects[CLIENTES_NUNIVEL4]);
      String nbnivel44 = (String) objects[CLIENTES_NBNIVEL4];

      String cdgrupo = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdgrupoo);
      String nbgrupo = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_40.value(), nbgrupoo);
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel00);
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel00);
      String nunivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel11);
      String nbnivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel11);
      String nunivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel22);
      String nbnivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel22);
      String nunivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel33);
      String nbnivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel33);
      String nunivel4 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel44);
      String nbnivel4 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel44);

      final StringBuilder rowLine = new StringBuilder();

      rowLine.append(cdgrupo).append(nbgrupo).append(nunivel0).append(nbnivel0).append(nunivel1).append(nbnivel1)
          .append(nunivel2).append(nbnivel2).append(nunivel3).append(nbnivel3).append(nunivel4).append(nbnivel4);

      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

      writeLine(outWriter, cadena);
    }
  }

}
