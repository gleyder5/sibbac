package sibbac.business.dwh.ficheros.valores.dto;

import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * The Class GruposValoresDTO.
 */
public class GruposValoresDTO extends AbstractDTO{

  /**
   * 
   */
  private static final long serialVersionUID = -5270093962321653900L;

  private BigInteger id;

  private String nbdescripcion;

  private String cdcodigo;

  private Timestamp auditdate;

  private String audituser;
  
  private BigInteger version;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getNbDescripcion() {
    return nbdescripcion;
  }

  public void setNbDescripcion(String nbDescripcion) {
    this.nbdescripcion = nbDescripcion;
  }

  public String getCdCodigo() {
    return cdcodigo;
  }

  public void setCdCodigo(String cdcodigo) {
    this.cdcodigo= cdcodigo;
  }

  public Timestamp getAuditdate() {
    return auditdate != null ? (Timestamp) auditdate.clone() : null;
  }

  public void setFhaudit(Timestamp auditdate) {
    this.auditdate = (auditdate != null ? (Timestamp) auditdate.clone() : null);
  }

  public String getAuditUser() {
    return audituser;
  }

  public void setAuditUser(String audituser) {
    this.audituser= audituser;
  }
  
  public BigInteger getVersion() {
    return version;
  }
  
  public void setVersion(BigInteger version) {
    this.version = version;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "GrupoValoresDTO [id=" + this.id + ", nbdescripcion=" + this.nbdescripcion
        + ", cdcodigo=" + this.cdcodigo + ", auditdate=" + this.auditdate+ ", audituser="
        + this.audituser+ ", version=" + this.version + "]";
  }
}
