package sibbac.business.dwh.utils;

public interface DwhConstantes<T> {

  public static final String TRADERS_9 = "99999999";
  public static final String TRADERS_DEFINIR = "SIN DEFINIR";
  public static final String TRADERS_X = "XXX";
  public static final String FICHEROS_INTERNACIONAL_ESTADO = "300";

  /**
   * Niveles para el fichero de Clientes
   * @author ipalacio
   *
   */
  public enum NivelesFicheroClientes implements DwhConstantes<Integer> {

    NIVEL0(0),

    NIVEL1(1),

    NIVEL2(2),

    NIVEL3(3),

    NIVEL4(4);

    private Integer value;

    private NivelesFicheroClientes(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return value;
    }
  }

  /**
   * Formateo de columnas
   * @author ipalacio
   *
   */
  public enum PaddingFormats implements DwhConstantes<String> {

    PADDIND_NUM0_2("%02d"),

    PADDIND_ZERO_3("%03d"),

    PADDIND_ZERO_4("%04d"),

    PADDIND_ZERO_5("%05d"),

    PADDIND_ZERO_6("%06d"),

    PADDIND_ZERO_8("%08d"),

    PADDIND_SPACE_1("%1$-1s"),

    PADDIND_SPACE_2("%1$-2s"),

    PADDIND_SPACE_3("%1$-3s"),

    PADDIND_SPACE_5("%1$-5s"),

    PADDIND_SPACE_6("%1$-6s"),

    PADDIND_SPACE_8("%1$-8s"),

    PADDIND_SPACE_10("%1$-10s"),

    PADDIND_SPACE_12("%1$-12s"),

    PADDIND_SPACE_15("%1$-15s"),

    PADDIND_ZERO_15("%015d"),

    PADDIND_SPACE_25("%1$-25s"),

    PADDIND_SPACE_31("%1$-31s"),

    PADDIND_SPACE_40("%1$-40s"),

    PADDIND_SPACE_50("%1$-50s"),

    PADDIND_SPACE_500("%1$-500s");

    private String value;

    private PaddingFormats(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return value;
    }
  }

  /**
   * 
   * @return
   */
  public enum TipoValoresProcess implements DwhConstantes<Integer> {

    VALORES_EMISOR(0),

    VALORES_EMISION(1),

    VALORES_WARRANT(2),

    VALORES_MERCADO(3);

    private Integer value;

    private TipoValoresProcess(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return value;
    }
  }

  /**
   * 
   * @return
   */
  public enum TipoValoresValidation implements DwhConstantes<Integer> {

    VALIDACION_ALTA(0),

    VALIDACION_BAJA(1),

    VALIDACION_MOD(2);

    private Integer value;

    private TipoValoresValidation(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return value;
    }
  }

  // Constantes para generacion de ficheros de Operaciones Internacionales

  /** Field order for NUOPROUT */
  public static final int MINORISTA_NUOPROUT = 1;

  /** Field order for NUOPROU2 */
  public static final int MINORISTA_NUOPROU2 = 2;

  /** Field order for NUPAREJE */
  public static final int MINORISTA_NUPAREJE = 3;

  /** Field order for CDCLIENT */
  public static final int MINORISTA_CDCLIENT = 4;

  /** Field order for CDENTLIQ */
  public static final int MINORISTA_CDENTLIQ = 5;

  /** Field order for CDISINVV */
  public static final int MINORISTA_CDISINVV = 6;

  /** Field order for CDBOLSAS */
  public static final int MINORISTA_CDBOLSAS = 7;

  /** Field order for TPOPERAC */
  public static final int MINORISTA_TPOPERAC = 8;

  /** Field order for TPEJERCI */
  public static final int MINORISTA_TPEJERCI = 9;

  /** Field order for TPEJECUC */
  public static final int MINORISTA_TPEJECUC = 10;

  /** Field order for TPOPESTD */
  public static final int MINORISTA_TPOPESTD = 11;

  /** Field order for TPCONTRA */
  public static final int MINORISTA_TPCONTRA = 12;

  /** Field order for CDOPEMER */
  public static final int MINORISTA_CDOPEMER = 13;

  /** Field order for CDDERECH */
  public static final int MINORISTA_ISROUTIN = 14;

  /** Field order for CDPROCED */
  public static final int MINORISTA_CDCLSMER = 15;

  /** Field order for FHEJECUC */
  public static final int MINORISTA_FHEJECUC = 16;

  /** Field order for IMEFECTI */
  public static final int MINORISTA_IMEFECTI = 17;

  /** Field order for NUCANEJE */
  public static final int MINORISTA_NUCANEJE = 18;

  /** Field order for IMDERCOG */
  public static final int MINORISTA_IMDERCOG = 19;

  /** Field order for IMDERLIQ */
  public static final int MINORISTA_IMDERLIQ = 20;

  /** Field order for IMCOMSVB */
  public static final int MINORISTA_IMCOMSVB = 21;

  /** Field order for IMCOMLIQ */
  public static final int MINORISTA_IMCOMLIQ = 22;

  /** Field order for IMCOMBRK */
  public static final int MINORISTA_IMCOMBRK = 23;

  /** Field order for IMCOMUK2 */
  public static final int MINORISTA_IMCOMUK2 = 24;

  /** Field order for IMCOMUSA */
  public static final int MINORISTA_IMCOMUSA = 25;

  /** Field order for IMCOMDEV */
  public static final int MINORISTA_IMCOMDEV = 26;

  /** Field order for NUPERIOD */
  public static final int MINORISTA_NUPERIOD = 27;

  /** Field order for IMDBRKDA */
  public static final int MINORISTA_IMDBRKDA = 28;

  /** Field order for CDCLIEST */
  public static final int MINORISTA_CDCLIEST = 29;

  /** Field order for IMBRUCOG */
  public static final int MINORISTA_IMBRUCOG = 30;

  /** Field order for IMBRULIQ */
  public static final int MINORISTA_IMBRULIQ = 31;

  /** Field order for IMBRUBAN */
  public static final int MINORISTA_IMBRUBAN = 32;

  /** Field order for IMBANSIS */
  public static final int MINORISTA_IMBANSIS = 33;

  /** Field order for CDBROKER */
  public static final int MINORISTA_CDBROKER = 34;

  /** Field order for CDGRUPBR */
  public static final int MINORISTA_TPBROKER = 35;

  /** Field order for IMNETCLT */
  public static final int MINORISTA_IMNETCLT = 36;

  /** Field order for IMBROCON */
  public static final int MINORISTA_IMBROCON = 37;

  /** Field order for IMCOMSUC */
  public static final int MINORISTA_IMCOMSUC = 38;

  /** Field order for CDCUSGLB */
  public static final int MINORISTA_CUSGLOID = 39;

  /** Field order for IMCOMCUS */
  public static final int MINORISTA_IMCOMCUS = 40;

  /** Field order for CDGESTOR */
  public static final int MINORISTA_CDUNOS = 41;

  /** Field order for IMCOMGRU */
  public static final int MINORISTA_IMCOMGRU = 42;

  /** Field order for CDGRUREP */
  public static final int MINORISTA_CDESPACE = 43;

  /** Field order for IMREPGES */
  public static final int MINORISTA_IMREPGES = 44;

  /** Field order for IMREPREC */
  public static final int MINORISTA_IMREPREC = 45;

  /** Field order for IMREPCON */
  public static final int MINORISTA_IMREPCON = 46;

  /** Field order for CDUNIGEF */
  public static final int MINORISTA_CDVALOR = 47;

  /** Field order for CDUNIGEB */
  public static final int MINORISTA_CDORIGEN = 48;

  /** Field order for IMCOMGRU */
  public static final int MINORISTA_CDCANALL = 49;

  /** Field order for CDGRUREP */
  public static final int MINORISTA_IMNULO = 50;

  /** Field order for IMREPGES */
  public static final int MINORISTA_FHAUDIT = 51;

  /** Field order for IMREPREC */
  public static final int MINORISTA_NUTITAGR = 52;

  /** Field order for IMREPCON */
  public static final int MINORISTA_CDTEXTO = 53;

  /** Field order for NUOPROUT */
  public static final int INSTITUCIONAL_NUOPROUT = 1;

  /** Field order for NUPAREJE */
  public static final int INSTITUCIONAL_NUPAREJE = 2;

  /** Field order for CDCLIENT */
  public static final int INSTITUCIONAL_CDCLIENT = 3;

  /** Field order for CDENTLIQ */
  public static final int INSTITUCIONAL_CDENTLIQ = 4;

  /** Field order for CDISINVV */
  public static final int INSTITUCIONAL_CDISINVV = 5;

  /** Field order for CDBOLSAS */
  public static final int INSTITUCIONAL_CDBOLSAS = 6;

  /** Field order for TPOPERAC */
  public static final int INSTITUCIONAL_TPOPERAC = 7;

  /** Field order for TPEJERCI */
  public static final int INSTITUCIONAL_TPEJERCI = 8;

  /** Field order for TPEJECUC */
  public static final int INSTITUCIONAL_TPEJECUC = 9;

  /** Field order for TPOPESTD */
  public static final int INSTITUCIONAL_TPOPESTD = 10;

  /** Field order for TPCONTRA */
  public static final int INSTITUCIONAL_TPCONTRA = 11;

  /** Field order for CDOPEMER */
  public static final int INSTITUCIONAL_CDOPEMER = 12;

  /** Field order for CDDERECH */
  public static final int INSTITUCIONAL_CDDERECH = 13;

  /** Field order for CDPROCED */
  public static final int INSTITUCIONAL_CDPROCED = 14;

  /** Field order for FHEJECUC */
  public static final int INSTITUCIONAL_FHEJECUC = 15;

  /** Field order for IMEFECTI */
  public static final int INSTITUCIONAL_IMEFECTI = 16;

  /** Field order for NUCANEJE */
  public static final int INSTITUCIONAL_NUCANEJE = 17;

  /** Field order for IMDERCOG */
  public static final int INSTITUCIONAL_IMDERCOG = 18;

  /** Field order for IMDERLIQ */
  public static final int INSTITUCIONAL_IMDERLIQ = 19;

  /** Field order for IMCOMSVB */
  public static final int INSTITUCIONAL_IMCOMSVB = 20;

  /** Field order for IMCOMLIQ */
  public static final int INSTITUCIONAL_IMCOMLIQ = 21;

  /** Field order for IMCOMBRK */
  public static final int INSTITUCIONAL_IMCOMBRK = 22;

  /** Field order for IMCOMUK2 */
  public static final int INSTITUCIONAL_IMCOMUK2 = 23;

  /** Field order for IMCOMUSA */
  public static final int INSTITUCIONAL_IMCOMUSA = 24;

  /** Field order for IMCOMDEV */
  public static final int INSTITUCIONAL_IMCOMDEV = 25;

  /** Field order for NUPERIOD */
  public static final int INSTITUCIONAL_NUPERIOD = 26;

  /** Field order for IMDBRKDA */
  public static final int INSTITUCIONAL_IMDBRKDA = 27;

  /** Field order for CDCLIEST */
  public static final int INSTITUCIONAL_CDCLIEST = 28;

  /** Field order for IMBRUCOG */
  public static final int INSTITUCIONAL_IMBRUCOG = 29;

  /** Field order for IMBRULIQ */
  public static final int INSTITUCIONAL_IMBRULIQ = 30;

  /** Field order for IMBRUBAN */
  public static final int INSTITUCIONAL_IMBRUBAN = 31;

  /** Field order for IMBANSIS */
  public static final int INSTITUCIONAL_IMBANSIS = 32;

  /** Field order for CDBROKER */
  public static final int INSTITUCIONAL_CDBROKER = 33;

  /** Field order for CDGRUPBR */
  public static final int INSTITUCIONAL_CDGRUPBR = 34;

  /** Field order for IMNETCLT */
  public static final int INSTITUCIONAL_IMNETCLT = 35;

  /** Field order for IMBROCON */
  public static final int INSTITUCIONAL_IMBROCON = 36;

  /** Field order for IMCOMSUC */
  public static final int INSTITUCIONAL_IMCOMSUC = 37;

  /** Field order for CDCUSGLB */
  public static final int INSTITUCIONAL_CDCUSGLB = 38;

  /** Field order for IMCOMCUS */
  public static final int INSTITUCIONAL_IMCOMCUS = 39;

  /** Field order for CDGESTOR */
  public static final int INSTITUCIONAL_CDGESTOR = 40;

  /** Field order for CDUNIGEF */
  public static final int INSTITUCIONAL_CDUNIGEF = 41;

  /** Field order for CDUNIGEB */
  public static final int INSTITUCIONAL_CDUNIGEB = 42;

  /** Field order for IMCOMGRU */
  public static final int INSTITUCIONAL_IMCOMGRU = 43;

  /** Field order for CDGRUREP */
  public static final int INSTITUCIONAL_CDGRUREP = 44;

  /** Field order for CDUNICON */
  public static final int INSTITUCIONAL_CDUNICON = 45;

  /** Field order for IMREPGES */
  public static final int INSTITUCIONAL_IMREPGES = 46;

  /** Field order for IMREPREC */
  public static final int INSTITUCIONAL_IMREPREC = 47;

  /** Field order for IMREPCON */
  public static final int INSTITUCIONAL_IMREPCON = 48;

  /** Tipo Institucional */
  public static final String INSTITUCIONAL_TIPO = "I";

  /** Tipo Minorista */
  public static final String MINORISTA_TIPO = "M";

  /** Tipo Institucional */
  public static final String INSTITUCIONAL_FILE = "fdwe0e97p";

  /** Tipo Minorista */
  public static final String MINORISTA_FILE = "fdwe0exmp";

  /** Tipo Institucional */
  public static final String INSTITUCIONAL_TXT = "INSTITUCIONAL";

  /** Tipo Minorista */
  public static final String MINORISTA_TXT = "MINORISTA";

  /** Clave Mercado XAS */
  public static final String NBCAMXML = "CDMERCAD";

  /** Clave Mercado-Datawarehouse XAS */
  public static final String NBCAMAS4 = "CDWAREHO";

  /** Clave Trader XAS */
  public static final String NBCAMTRADERXML = "CDTRADER";

  public T value();

}
