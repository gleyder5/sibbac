package sibbac.business.dwh.ficheros.valores.db.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "TMCT0_GRUPOS_VALORES")
@Entity
public class GruposValores implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1014715701004521139L;

  @Id
  @Column(name = "ID", nullable = false, length = 10)
  private BigInteger id;

  @Column(name = "NB_DESCRIPCION", nullable = false, length = 255)
  private String nbdescripcion;

  @Column(name = "CD_CODIGO", nullable = false, length = 4)
  private String cdcodigo;

  @Column(name = "AUDIT_DATE", nullable = false, length = 26)
  private Timestamp auditdate;

  @Column(name = "AUDIT_USER", nullable = false, length = 255)
  private String audituser;
  
  @Column(name = "VERSION", nullable = false, length = 19)
  private BigInteger version;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getNbDescripcion() {
    return nbdescripcion;
  }

  public void setNbDescripcion(String nbDescripcion) {
    this.nbdescripcion = nbDescripcion;
  }

  public String getCdCodigo() {
    return cdcodigo;
  }

  public void setCdCodigo(String cdcodigo) {
    this.cdcodigo= cdcodigo;
  }

  public Timestamp getAuditdate() {
    return auditdate != null ? (Timestamp) auditdate.clone() : null;
  }

  public void setFhaudit(Timestamp auditdate) {
    this.auditdate = (auditdate != null ? (Timestamp) auditdate.clone() : null);
  }

  public String getAuditUser() {
    return audituser;
  }

  public void setAuditUser(String audituser) {
    this.audituser= audituser;
  }
  
  public BigInteger getVersion() {
    return version;
  }
  
  public void setVersion(BigInteger version) {
    this.version = version;
  }
}
