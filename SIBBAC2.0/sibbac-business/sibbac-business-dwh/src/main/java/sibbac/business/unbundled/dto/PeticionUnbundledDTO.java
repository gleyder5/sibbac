package sibbac.business.unbundled.dto;
import java.util.List;
import java.util.Map;
public class PeticionUnbundledDTO {
  
  private Map<String, Object> params;
  
  private List<Map<String, Object>> items;
  public Map<String, Object> getParams() {
    return params;
  }
  public List<Map<String, Object>> getItems() {
    return items;
  }
  public void setParams(Map<String, Object> params) {
    this.params = params;
  }
  public void setItems(List<Map<String, Object>> items) {
    this.items = items;
  }
}