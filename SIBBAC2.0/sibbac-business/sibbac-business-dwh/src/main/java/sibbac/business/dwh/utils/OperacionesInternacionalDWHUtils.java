package sibbac.business.dwh.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0estBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;

public class OperacionesInternacionalDWHUtils {

  @Autowired
  static Tmct0aloBo tmct0aloBo;

  @Autowired
  static Tmct0estBo tmct0estBo;

  @Autowired
  static Tmct0xasBo tmct0xasBo;

  @Autowired
  static Tmct0aliBo tmct0aliBo;

  @Autowired
  static Tmct0cliBo tmct0cliBo;

  protected static String CDCLENTE_ROUTING = "ROUTING_BCH";
  protected static String CDORIGEN_ROUTING = "ROUTING";

  /**
   * 
   * @param inputString
   * @return
   */
  public static boolean isInteger(String inputString) {
    boolean result = true;
    if (inputString != null) {
      try {
        final Integer temp = NumberUtils.createInteger(inputString);
        if (temp == null) {
          result = false;
        }
      }
      catch (NumberFormatException ex) {
        result = false;
      }
    }
    else {
      result = false;
    }
    return result;
  }

  /**
   * 
   * @param inputString
   * @return
   */
  public static boolean isLong(String inputString) {
    boolean result = true;
    if (inputString != null) {
      try {
        final Long temp = NumberUtils.createLong(inputString);
        if (temp == null) {
          result = false;
        }
      }
      catch (NumberFormatException ex) {
        result = false;
      }
    }
    else {
      result = false;
    }
    return result;
  }

  /**
   * 
   * @param inputString
   * @return
   */
  public static boolean fillField(String inputString) {
    boolean result = true;
    if (inputString != null) {
      try {
        Long.parseLong(inputString);
      }
      catch (NumberFormatException ex) {
        result = false;
      }
    }
    else {
      result = false;
    }
    return result;
  }

  /**
   * 
   * @param inputString
   * @param length
   * @return
   */
  public static String fillFieldLong(String inputString, int length) {
    String result = inputString;
    return result;
  }

  /**
   * 
   * @param inputString
   * @param i
   * @return
   */
  public static String fillFieldString(String inputString, int i) {
    String result = inputString;
    while (result.length() < i) {
      result += " ";
    }
    return result;
  }

  public static BigDecimal fillFieldBigDecimal(BigDecimal nuoprout, int i) {
    return null;
  }

  /**
   * 
   * @param toCheck
   * @param parteEnteraSize
   * @param parteDecimalSize
   * @return
   */
  public static boolean isValid(BigDecimal toCheck, int parteEnteraSize, int parteDecimalSize) {
    boolean result = true;
    if (toCheck.signum() != -1) {
      String[] resultFromSplit = toCheck.toString().split("\\.");
      String parteEntera = resultFromSplit[0];
      if (parteEntera.length() <= parteEnteraSize) {
        if (resultFromSplit.length == 2) {
          String parteDecimal = resultFromSplit[1];
          if (parteDecimal.length() > parteDecimalSize) {
            result = false;
          }
        }
      }
      else {
        result = false;
      }
    }
    else {
      return false;
    }
    return result;

  }

  /**
   * 
   * @param toCheck
   * @param parteEnteraSize
   * @param parteDecimalSize
   * @return
   */
  public static boolean isValidSigned(BigDecimal toCheck, int parteEnteraSize, int parteDecimalSize) {
    boolean result = true;

    String[] resultFromSplit = toCheck.toString().split("\\.");
    String parteEntera = resultFromSplit[0];
    /* Eliminamos el signo */
    if (parteEntera.startsWith("-")) {
      parteEntera = parteEntera.substring(1);
    }
    if (parteEntera.length() <= parteEnteraSize) {
      if (resultFromSplit.length == 2) {
        String parteDecimal = resultFromSplit[1];
        if (parteDecimal.length() > parteDecimalSize) {
          result = false;
        }
      }
    }
    else {
      result = false;
    }
    return result;
  }

  public static String getAsString(BigDecimal input, String pattern) throws Exception {
    String result;
    if (input != null) {

      NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
      DecimalFormat df = (DecimalFormat) nf;
      df.applyPattern(pattern);
      result = df.format(input);
    }
    else {
      throw new Exception();
    }
    return result;
  }

  public static String padRight(String s, int n) {
    return String.format("%1$-" + n + "s", s);
  }

  /**
   * Comprobar Longitud del dato y rellenar a la misma. Para tipos Date Si nulos
   * rellenar a blancos.
   * 
   * @param valor Fecha a comprobar
   * @param format Formato de la fecha para la salida, un fomato valido de
   * SimpleDateFormat
   * @return La fecha fomateada
   * @throws ConversionException
   */
  public static String fillDate(Date valor, String format) {
    if (valor == null) {
      // throw new ConversionException(Messages
      // .getString("GeneralValidation.fecha.error.obligatoria"));
      char[] fecha = new char[format.length()];
      Arrays.fill(fecha, ' ');
      return new String(fecha);
    }
    String valorFmt = new SimpleDateFormat(format).format(valor);
    return valorFmt;
  }

  /**
   * Una operacion es de Routing si: el contenido del campo cdclente es igual a
   * "ROUTING_BCH" o si el contenido del campo cdclente es distinto de
   * "ROUTING_BCH" y cdorigen es igual a ROUTING
   * 
   * @return boolean
   */
  public static boolean isROUTING(String cdclente, String cdorigen) {

    if (CDCLENTE_ROUTING.equalsIgnoreCase(cdclente.trim())) {
      return true;
    }
    else {
      if (CDORIGEN_ROUTING.equalsIgnoreCase(cdorigen.trim())) {
        return true;
      }
    }
    return false;
  }

  /**
   * Formatear Numerico a tamaï¿½o dado con Signo +/-
   * 
   * @param valor Numero a formatear
   * @param Longitud Longitud Total del numero incluidos los decimales
   * @param decimales Numero de Decimales
   * @param signo si true se le aï¿½ade el signo en cabecera (positivo/negativo)
   * con lo se aï¿½ade una posicion a la longitud total si false no se pone
   * signo y se pasa valor a abs()
   * @return El numero formateado
   * 
   */
  public static String formatNum(BigDecimal valor, int longitud, int decimales, boolean signo) {
    if (valor == null) {
      valor = new BigDecimal(0);
    }
    if (!signo) {
      if (valor.compareTo(new BigDecimal(0)) == -1) {
        valor = valor.abs();
      }
    }
    // Enteros es la longitud total, asï¿½ que hay que quitarle los
    // decimales
    longitud = longitud - decimales;
    String valorFmt = "";
    // Pattern
    String pattern = "";
    char[] ptnEnteros = new char[longitud];
    Arrays.fill(ptnEnteros, '0');
    pattern = new String(ptnEnteros);
    if (decimales != 0) {
      char[] ptnDecimales = new char[decimales];
      Arrays.fill(ptnDecimales, '0');
      pattern = pattern + "." + new String(ptnDecimales);
    }
    // Formatear
    DecimalFormat df = new DecimalFormat(pattern);
    valorFmt = df.format(valor);
    valorFmt = removeChars(valorFmt, df.getDecimalFormatSymbols().getDecimalSeparator());
    // AÃ±adir signo positivo si procede
    if (signo) {
      if (valor.signum() >= 0) {
        valorFmt = "+" + valorFmt;
      }

      /*
       * else { valorFmt = "-" + valorFmt; }
       */
    }
    return valorFmt;
  }

  /**
   * Formatear Numerico a tamaï¿½o dado Mostrando Signo sï¿½lo si es negativo,
   * la longitud total es la especificada (el signo se incluye a la izquierda).
   * 
   * @param valor Numero a formatear
   * @param Longitud Longitud Total del numero incluidos los decimales
   * @param decimales Numero de Decimales
   * 
   * @return El numero formateado
   * 
   */
  public static String formatNum(BigDecimal valor, int longitud, int decimales) {
    if (valor == null) {
      valor = new BigDecimal(0);
    }
    // Enteros es la longitud total, asï¿½ que hay que quitarle los
    // decimales
    longitud = longitud - decimales;
    // Si negativo quitar otra posicion para meter signo negativo
    if (valor.compareTo(new BigDecimal(0)) == -1) {
      longitud = longitud - 1;
    }
    String valorFmt = "";
    // Pattern
    String pattern = "";
    char[] ptnEnteros = new char[longitud];
    Arrays.fill(ptnEnteros, '0');
    pattern = new String(ptnEnteros);
    if (decimales != 0) {
      char[] ptnDecimales = new char[decimales];
      Arrays.fill(ptnDecimales, '0');
      pattern = pattern + "." + new String(ptnDecimales);
    }
    // Formatear
    DecimalFormat df = new DecimalFormat(pattern);
    valorFmt = df.format(valor);
    valorFmt = removeChars(valorFmt, df.getDecimalFormatSymbols().getDecimalSeparator());
    return valorFmt;
  }

  /**
   * Quitar el sÃ­mbolo de decimal al string y juntarlo en uno solo
   * 
   * @param src String a formatear
   * @param chr Caracter a quitar
   * @return El String formateado
   * 
   */
  protected static String removeChars(String src, char chr) {
    if (src.indexOf((int) chr) < 0) {
      return src;
    }
    char[] dst = new char[src.length() - 1];
    char[] src2 = src.toCharArray();
    int i = 0;
    for (char c : src2) {
      if (c == chr) {
        continue;
      }
      dst[i] = c;
      i++;
    }
    // String s2=src.substring(0,src.indexOf((int)
    // chr))+src.substring(src.indexOf((int) chr)+1);
    return new String(dst);
  }

  public static String getCdalias(String cdalias) {
    cdalias = cdalias.trim();
    String[] alias = cdalias.split("\\|");
    if (alias.length == 2)
      cdalias = alias[1].trim();
    return cdalias;
  }
}
