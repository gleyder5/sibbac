package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

public interface AutexDao extends JpaRepository</* FIXME entidad */MaestroValores, String> {

  /**
   * Query para DWHAutexDao, ORIGINAL
   */
  public static final String AUTEX_QUERY = "SELECT CDAUTEXX,NBTITREV FROM TMCT0_DWH_AUTEX";

  /**
   * Query para DWHficheroValoresDao con Paginación
   */
  public static final String AUTEX_PAGE_QUERY = AUTEX_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHficheroValoresDao
   */
  public static final String AUTEX_PAGE_QUERY_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + AUTEX_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHficheroValoresDao sin
   * paginación
   * @return
   */
  @Query(value = AUTEX_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListAutex(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHagrupTipoValorDao sin
   * paginación
   * @return
   */
  @Query(value = AUTEX_PAGE_QUERY_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListAutex();

}
