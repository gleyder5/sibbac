package sibbac.business.dwh.ficheros.estaticos.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.dwh.ficheros.estaticos.db.model.BrokerDwh;

public interface BrokerDwhDao extends JpaRepository<BrokerDwh, String> {

  @Query(value = "SELECT ID_BROKER, NOMBRE_BROKER, TIPO_BROKER FROM TMCT0_DWH_BROKER WHERE FECHA_BAJA > CURRENT_DATE OR FECHA_BAJA IS NULL", nativeQuery = true)
  public List<Object[]> findActiveBrokers();

}