package sibbac.business.dwh.ficheros.nacional.bo;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableLanzamientoManualProcess;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Component
public class LanzamientoManualProcess {

  protected static final Logger LOG = LoggerFactory.getLogger(LanzamientoManualProcess.class);

  @Autowired
  private CallableLanzamientoManualProcess callableLanzamientoManualProcess;

  @Value("${dwh.ficheros.nacional.gui.timeout.millis:2}")
  private int timeout;

  /** The fecha ejecución. */
  private Date fechaEjecucion;

  private String userSession;

  private final Object lock = new Object();

  private Boolean isWaitting;

  public CallableResultDTO launchGenerateAndExportProcess() throws SIBBACBusinessException {
    final CallableResultDTO result = new CallableResultDTO();

    isWaitting = false;

    // Seteamos los parámetros de entrada al proceso
    callableLanzamientoManualProcess.setFechaEjecucion(fechaEjecucion);
    callableLanzamientoManualProcess.setUserSession(userSession);
    callableLanzamientoManualProcess.setParent(this);

    try {
      final ExecutorService executor = Executors.newFixedThreadPool(1);

      callableLanzamientoManualProcess.setFuture(executor.submit(callableLanzamientoManualProcess));

      executor.shutdown();

      waitProcess(executor);

      Thread.sleep(1000); // Esperamos un segundo a que el executor acabe

      // Pasado ese tiempo comprobamos si el proceso General ha terminado.
      if (executor.isTerminated()) {
        LOG.debug(
            "El proceso de generación las tareas DWH Ejecuciones ha terminado enviamos los resultados al usuario en espera");
        // Unificar resultados
        // final List<ProcessFileResultDTO> processResultDTOs =
        // callableLanzamientoManualProcess.getFuture().get();
        // for (ProcessFileResultDTO element : processResultDTOs) {
        // final String filename = element.getFileNameProcess();
        // final String statusFile = element.getFileResult().toString();
        // final String message = "";
        // result.getMessage()
        // .add(String.format("Fichero:\t%s Estado:\t%s, Mensaje:\t%s",
        // filename, statusFile, message));
        // result.getMessage().add("La tarea de lanzamiento manual ha terminado
        // correctamente.");
        // }
        result.getMessage().add("La tarea de lanzamiento manual ha terminado correctamente.");
      }
      else {
        LOG.debug("El proceso de generación las tareas DWH Ejecuciones seguirá en ejecución");
        // Exportamos los fichero mediante correo al usuario
        callableLanzamientoManualProcess.setSendMailOnFinish(true);
        result.getMessage().add(
            "El proceso de lanzamiento manual se ejecutará en segundo plano, se le enviará un correo cuando termine la generación");
      }
    }
    catch (/* ExecutionException | */ InterruptedException exception) {
      result.getErrorMessage().add("Se ha producido un error inesperado cuando se intentaba iniciar el proceso.");
    }

    return result;
  }

  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  public String getUserSession() {
    return userSession;
  }

  public void setUserSession(String userSession) {
    this.userSession = userSession;
  }

  public Boolean getIsWaitting() {
    return isWaitting;
  }

  public void setIsWaitting(Boolean isWaitting) {
    this.isWaitting = isWaitting;
  }

  /**
   * Wait del proceso, evitanto el spurious
   */
  public void waitProcess(ExecutorService executor) {
    if (!executor.isTerminated()) {
      long timeoutmillis = (long) timeout * 60L * 1000L;

      synchronized (lock) {
        try {
          isWaitting = true;

          LOG.debug(String.format("Hacemos un wait de %d milisegundos", timeoutmillis));

          // Algoritmo para evitar un spurious wakeup
          long initTimeOut = System.currentTimeMillis();
          while (isWaitting) {
            // Evitamos un spurious wakeup
            lock.wait(timeoutmillis);
            // Si nos llega un spurious wakeup debemos seguir esperando pero
            // como
            // ya ha esperado
            // debemos seguir esperando el tiempo restante hasta completar el
            // tiempo definido en
            // timeout
            long restTimeOut = System.currentTimeMillis() - initTimeOut;
            if (isWaitting && restTimeOut < timeoutmillis) {
              timeoutmillis = timeoutmillis - restTimeOut;
              if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Hemos recibido un Spurious Wakeup al proceso hemos de seguir esperando %d",
                    timeoutmillis));
              }
            }
            else if ((System.currentTimeMillis() - initTimeOut) >= timeoutmillis) {
              if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Se ha agotado el tiempo de espera de %d", timeoutmillis));
              }
              isWaitting = false;
            }
            else {
              if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("El proceso ha terminado antes que se agotaase el tiempo de espera de %d",
                    timeoutmillis));
              }
              isWaitting = false;
            }
          }
        }
        catch (InterruptedException e) {
          LOG.error("Error el hilo no puede esperar", e);
        }
      }
    }
  }

  /**
   * Wakeup del proces
   */
  public void wakeUpProcess() {
    synchronized (lock) {
      isWaitting = false;
      lock.notifyAll();
      LOG.debug("Se ha notificado el wakeup al proceso");
    }
  }

  public boolean isRunningInBackGround() {
    return callableLanzamientoManualProcess.isRunning();
  }
}
