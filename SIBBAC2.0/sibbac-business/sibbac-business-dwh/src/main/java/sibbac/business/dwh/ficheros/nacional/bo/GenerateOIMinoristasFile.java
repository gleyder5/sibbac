package sibbac.business.dwh.ficheros.nacional.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.CallableProcessInterface;
import sibbac.business.dwh.ficheros.callable.thread.NamedThreadFactory;
import sibbac.business.dwh.ficheros.nacional.bo.callable.CallableNacionalMinoristasLineThreadsBo;
import sibbac.business.dwh.ficheros.nacional.db.dao.OIMinoristasDao;
import sibbac.business.dwh.ficheros.nacional.dto.MinoristaDTO;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.business.dwh.utils.OperacionesInternacionalDWHUtils;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0estBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0est;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.AbstractGenerateFile;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums;;

@Component
public class GenerateOIMinoristasFile extends AbstractGenerateFile {

  /** Minutos de espera para que el ejecutor realice toda la tarea. */
  // @Value("${sibbac.batch.timeout: 60}")
  private int timeout = 60;

  @Autowired
  private ApplicationContext ctx;

  private static final Logger LOG = LoggerFactory.getLogger(GenerateOIMinoristasFile.class);

  private static final String MINORISTAS_LINE_SEPARATOR = "\r\n";

  private static final String MINORISTAS_FILE_HEADER = "";

  private static final String MINORISTAS_FILE_FOOTER = "";

  public String estado = DwhConstantes.FICHEROS_INTERNACIONAL_ESTADO;

  public Date tradeini;

  public Date tradefin;

  /**
   * Nombre del fichero de Minoristas
   */
  @Value("${dwh.ficheros.nacional.minoristas.file:fdwe0exmp.txt}")
  private String fileNameMinoristas;

  @Autowired
  FicheroNacionalBo ficheroNacionalBo;

  @Autowired
  OIMinoristasDao oIMinoristasDao;

  @Autowired
  Tmct0aloBo tmct0aloBo;

  @Autowired
  Tmct0estBo tmct0estBo;

  @Autowired
  Tmct0xasBo tmct0xasBo;

  @Autowired
  Tmct0aliBo tmct0aliBo;

  @Autowired
  Tmct0cliBo tmct0cliBo;

  @Autowired
  Tmct0brkBo tmct0brkBo;

  private Tmct0cta tmct0cta = new Tmct0cta();
  private Tmct0ctg tmct0ctg = new Tmct0ctg();
  private Tmct0cto tmct0cto = new Tmct0cto();
  private Tmct0ord tmct0ord = new Tmct0ord();

  /**
   * Obtiene el separador de linea
   */
  @Override
  public String getLineSeparator() {
    return MINORISTAS_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {

    return oIMinoristasDao.findAllListMinoristas(estado, tradeini, tradefin, pageable.getOffset(),
        pageable.getPageSize());
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    return oIMinoristasDao.countAllListMinoristas(estado, tradeini, tradefin);
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {

    if (LOG.isDebugEnabled()) {
      LOG.debug("Fichero sin cabecera");
    }
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {

    if (LOG.isDebugEnabled()) {
      LOG.debug("Fichero sin pié de página");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    try {
      launchMinoristasWriteLine(fileData, outWriter);
    }
    catch (InterruptedException e) {
    }

  }

  public String loadMinorista(MinoristaDTO dataMin, Tmct0ctd ctd) {

    try {
      String alias = "";
      if (ctd.getCdalias().indexOf('|') == -1) {
        alias = ctd.getCdalias();
      }
      else {
        alias = ctd.getCdalias().substring(ctd.getCdalias().indexOf('|') + 1);
      }

      if (getTpClienn(ctd.getCdalias(), new Integer(GenerateFilesUtilDwh.formatDate("yyyyMMdd", ctd.getFetrade())))
          .equals(DwhConstantes.MINORISTA_TIPO)) {

        // 1 NOUPROU1
        dataMin.setNUOPROUT(BigDecimal.valueOf(ctd.getNuoprout()));
        // 2 NUOPROU2
        dataMin.setNUOPROU2(BigDecimal.valueOf(ctd.getNuoprout()));
        // 3 NUPAREJE
        dataMin.setNUPAREJE(BigDecimal.valueOf(ctd.getNupareje()));
        // CDCLIENT
        String clientAlias = "";

        Tmct0est tmct0est = getTmct0est(ctd.getCdalias(),
            new BigDecimal(GenerateFilesUtilDwh.formatDate("yyyyMMdd", ctd.getFetrade())));

        clientAlias = alias.trim();
        // 4 CDCLIENT
        dataMin.setCDCLIENT(new BigDecimal(clientAlias));
        // 5 CDENTLIQ
        dataMin.setCDENTLIQ(ctd.getCdcleare().substring(0, 4));
        // 6 CDISINVV
        dataMin.setCDISINVV(ctd.getCdisin());
        // *7 CDBOLSAS

        initEntities(ctd);

        String inforxml = "";
        if (ctd.getId().getCdmercad().trim() != null) {
          inforxml = ctd.getId().getCdmercad().trim();
        }

        String cdbolsas = oIMinoristasDao.getCdBolsas(inforxml);
        cdbolsas = cdbolsas.trim();
        if (cdbolsas == null) {
          cdbolsas = "   ";
        }

        dataMin.setCDBOLSAS(cdbolsas);

        // 8 TPOPERAC
        dataMin.setTPOPERAC(ctd.getCdtpoper().toString().trim());
        // 9 TPEJERCI - Por defecto 'CV' en el constructor.
        // 10 TPEJECUC - Por defecto 'V' en el constructor.
        // 11 TPOPESTD - Por defecto 'N' en el constructor.
        // 12 TPCONTRA - Por defecto 'T' en el constructor.

        dataMin.setCDOPEMER("");
        // *13 CDOPEMER
        String cdopemer = "";
        if (tmct0est != null) {
          String trader = tmct0est.getCdsalelm().trim();
          cdopemer = getInforas4(DwhConstantes.NBCAMTRADERXML, trader, DwhConstantes.NBCAMAS4).trim();
        }
        if (cdopemer.trim().equals("")) {
          dataMin.setCDOPEMER("99999999");
        }
        else {
          dataMin.setCDOPEMER(
              String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), Integer.valueOf(cdopemer.trim())));
        }
        // *14 ISROUTIN
        if (tmct0ord != null) {
          if (OperacionesInternacionalDWHUtils.isROUTING(tmct0ord.getCdclente().trim(),
              tmct0ord.getCdorigen().trim())) {
            dataMin.setISROUTIN("S");
          }
          else {
            dataMin.setISROUTIN("N");
          }
        }
        // 15 CDCLSMER - Por defecto 'E' en el constructor.
        // *16 FHEJECUC
        dataMin.setFHEJECUC(OperacionesInternacionalDWHUtils.fillDate(ctd.getFetrade(), "yyyyMMdd"));
        // 17 IMEFECTI
        dataMin.setIMEFECTI(ctd.getEutotbru().setScale(2, BigDecimal.ROUND_FLOOR));
        // 18 NUCANEJE
        dataMin.setNUCANEJE(ctd.getNutitagr().setScale(2, BigDecimal.ROUND_FLOOR));
        // 19 IMDERCOG
        dataMin.setIMDERCOG(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 20 IMDERLIQ
        dataMin.setIMDERLIQ(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // *21 IMCOMSVB

        final BigDecimal imcomsvb = getDiferencias(ctd);

        dataMin.setIMCOMSVB(imcomsvb.setScale(2, BigDecimal.ROUND_FLOOR));
        // 22 IMCOMLIQ
        dataMin.setIMCOMLIQ(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 23 IMCOMBRK - Por defecto '+000000000000000.00' en el
        // constructor.
        // 24 IMCOMUK2 - Por defecto '+000000000000000.00' en el
        // constructor.
        // 25 IMCOMUSA - Por defecto '+000000000000000.00' en el
        // constructor.
        // 26 IMCOMDEV
        dataMin.setIMCOMDEV(ctd.getEucomdev().setScale(2, BigDecimal.ROUND_FLOOR));
        // 27 NUPERIOD
        Date aux = ctd.getFetrade();

        String yearString = FormatDataUtils.convertDateToString(aux, "yyyy");

        dataMin.setNUPERIOD(new BigDecimal(yearString).setScale(0, BigDecimal.ROUND_FLOOR));
        // 28 IMDBRKDA - Por defecto '+000000000000000.00' en el
        // constructor.
        // 29 CDCLIEST
        dataMin.setCDCLIEST(new BigDecimal(clientAlias));
        // 30 IMBRUCOG
        dataMin.setIMBRUCOG(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 31 IMBRULIQ
        dataMin.setIMBRULIQ(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 32 IMBRUBAN
        dataMin.setIMBRUBAN(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 33 IMBANSIS - Por defecto '+000000000000000.00' en el
        // constructor.

        // *34 CDBROKER
        // Tmct0brk tmct0brk = JdoGetObject
        // .getTmct0brk(ctd.getFk_tmct0cta_1().getFk_tmct0ctg_1().getFk_tmct0cto_1().getCdbroker(),
        // hpm);
        // tmct0brk = hpm.getPersistenceManager().detachCopy(tmct0brk);

        final String cdBroker = ctd.getId().getCdbroker();
        if (cdBroker != null) {
          final String broker = oIMinoristasDao.getCdBroker(cdBroker.trim());
          dataMin.setCDBROKER(new BigDecimal(broker));
        }
        else {
          dataMin.setCDBROKER(new BigDecimal("00000000"));
        }

        // 35 TPBROKER - Por defecto '2' en el constructor.
        // 36 IMNETCLT
        dataMin.setIMNETCLT(ctd.getEutotnet().setScale(2, BigDecimal.ROUND_FLOOR));
        // 37 IMBROCON
        dataMin.setIMBROCON(ctd.getImfibreu().setScale(2, BigDecimal.ROUND_FLOOR));
        // 38 IMCOMSUC
        dataMin.setIMCOMSUC(ctd.getImfibreu().setScale(2, BigDecimal.ROUND_FLOOR));
        // 39 CUSGLOID - Por defecto ' ' en el constructor.
        // 40 IMCOMCUS
        dataMin.setIMCOMCUS(new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));
        // 41 CDUNOS - Por defecto '010101' en el constructor.
        // 42 IMCOMGRU
        dataMin.setIMCOMGRU(imcomsvb.subtract(ctd.getEucomdev()).setScale(2, BigDecimal.ROUND_FLOOR));
        // 43 CDESPACE - Por defecto ' ' en el constructor.
        // 44 IMREPGES - Por defecto '+00000000000000.00' en el
        // constructor.
        // 45 IMREPREC - Por defecto '+00000000000000.00' en el
        // constructor.
        // 46 IMREPCON - Por defecto '+00000000000000.00' en el
        // constructor.
        // 47 CDVALOR - Por defecto '00000049' en el constructor.
        // *48 CDORIGEN
        // String origen =
        // ctd.getFk_tmct0cta_1().getFk_tmct0ctg_1().getFk_tmct0cto_1().getFk_tmct0ord_2().getCdorigen().trim();
        String origen = "   ";
        dataMin.setCDORIGEN(origen);
        if (tmct0ord != null) {
          if (tmct0ord.getCdorigen() != null) {
            origen = tmct0ord.getCdorigen().trim();
          }
        }
        if (origen.equals("ROUTING")) {
          dataMin.setCDORIGEN("RI ");
        }
        else if (origen.equals("FRONT")) {
          dataMin.setCDORIGEN("F  ");
        }
        else if (origen.equals("FIX")) {
          dataMin.setCDORIGEN("FIX");
        }
        // *49 CDCANALL
        if (tmct0ord != null) {
          dataMin
              .setCDCANALL(String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_3.value(), tmct0ord.getCdcanal()));
        }
        else {
          dataMin.setCDCANALL("   ");
        }
        // 50 IMNULO - Por defecto '+000000000000000.00' en el constructor.
        // 51 FHAUDIT
        dataMin.setFHAUDIT(OperacionesInternacionalDWHUtils.fillDate(ctd.getFhaudit(), "yyyyMMdd"));
        // 52 NUTITAGR
        dataMin.setNUTITAGR(ctd.getNutitagr().setScale(2, BigDecimal.ROUND_FLOOR));
        // 53 CDTEXTO - Por defecto 'TE' en el constructor.
        return dataMin.toString();

      }
      else {
        LOG.debug("El cliente no es de tipo Minorista");
        return null;
      }
    }
    catch (Exception e) {
      LOG.debug("No se pudo mapear MinoristaDTO {} Tmct0ctd {}", dataMin, ctd.getId().getNuorden(),
          ctd.getId().getCdbroker(), ctd.getId().getCdmercad(), ctd.getId().getNbooking(), ctd.getId().getNucnfclt(),
          ctd.getId().getNualosec());
      return null;
    }
  }

  public void setMinoristasParams(String estado, Date fechaEjecucion) throws SIBBACBusinessException {

    this.estado = estado;
    this.tradeini = ficheroNacionalBo.extraccionParametrizacion(fechaEjecucion);
    this.tradefin = fechaEjecucion;
  }

  public void initEntities(Tmct0ctd ctd) {
    tmct0cta = ctd.getTmct0cta();
    if (tmct0cta != null) {
      tmct0ctg = tmct0cta.getTmct0ctg();
    }
    if (tmct0ctg != null) {
      tmct0cto = tmct0ctg.getTmct0cto();
    }
    if (tmct0cto != null) {
      tmct0ord = tmct0cto.getTmct0ord();
    }
  }

  public String getTpClienn(String cdaliass, Integer fecha) {

    String tpclienn = "";
    Tmct0cli tmct0cli = null;

    String cdalias = OperacionesInternacionalDWHUtils.getCdalias(cdaliass);

    Tmct0ali tmct0ali = tmct0aliBo.findByCdaliassFechaActual(cdalias, fecha);

    if (tmct0ali != null) {
      tmct0cli = tmct0cliBo.findByCdbrocliAndFecha(tmct0ali.getCdbrocli(), fecha);
    }
    if (tmct0cli != null) {
      tpclienn = tmct0cli.getTpclienn().toString();
    }
    return tpclienn;
  }

  /**
   * 
   * @param ctd
   * @return
   */
  public BigDecimal getDiferencias(Tmct0ctd ctd) {
    BigDecimal imsvb = (new BigDecimal("0").setScale(2, BigDecimal.ROUND_FLOOR));

    if (ctd.getImsvbeu() != null) {
      imsvb = ctd.getImsvbeu();
    }

    if (tmct0ord != null) {
      final String cdclsmdo = tmct0ord.getCdclsmdo().toString();
      final Tmct0cta tmct0cta = ctd.getTmct0cta();
      if (tmct0cta != null) {
        final Tmct0ctg tmct0ctg = tmct0cta.getTmct0ctg();

        if (cdclsmdo != null && "E".equals(cdclsmdo) && tmct0ctg != null) {
          final String cdmercad = tmct0ctg.getTmct0cto().getId().getCdmercad();
          final String cdalias = OperacionesInternacionalDWHUtils.getCdalias(ctd.getCdalias());
          final Tmct0alo tmct0alo = tmct0aloBo.getAlo(tmct0ctg.getTmct0cto().getTmct0ord().getNuorden(),
              tmct0ctg.getId().getNbooking(), ctd.getTmct0cta().getNucnfclt());

          if (cdmercad != null && tmct0alo != null && cdalias != null) {
            final Properties warehouseProperties = getCfgForProcess(
                SibbacEnums.Tmct0cfgConfig.DATAWAREHOUSE_0.getProcess());
            final String nationalTaxesString = warehouseProperties
                .getProperty(SibbacEnums.Tmct0cfgConfig.DATAWAREHOUSE_NATIONAL_TAXES.getKeyname());
            final String exceptionNationalTaxesString = warehouseProperties
                .getProperty(SibbacEnums.Tmct0cfgConfig.EXCEPTION_DATAWAREHOUSE_NATIONAL_TAXES.getKeyname());

            if (nationalTaxesString != null && exceptionNationalTaxesString != null) {
              final String[] nationalTaxes = nationalTaxesString.split(",");

              if (nationalTaxes != null && nationalTaxes.length > 0) {
                for (String nationalTax : nationalTaxes) {
                  if (nationalTax != null && nationalTax.equals(cdmercad.trim())
                      && !exceptionNationalTaxesString.contains(cdalias)) {

                    final BigDecimal imgatdvsAlo = tmct0alo.getImgatdvs();
                    final BigDecimal imgatdvsCtd = (imgatdvsAlo.multiply(ctd.getNutitagr()))
                        .divide(tmct0alo.getNutitcli());

                    imsvb = imsvb.subtract(imgatdvsCtd);
                  }
                }
              }
              else {
                LOG.debug("El campo nationalTaxes es nulo");
              }
            }
            else {
              LOG.debug("El campo nationalTaxesString es nulo");
            }
          }
          else {
            LOG.debug("El campo cdmercad o tmct0alo o cdalias es nulo");
          }
        }
        else {
          LOG.debug("El campo cdclsmdo es nulo");
        }
      }
      else {
        LOG.debug("El campo tmct0cta es nulo", ctd.getId().getNuorden(), ctd.getId().getCdbroker(),
            ctd.getId().getCdmercad(), ctd.getId().getNbooking(), ctd.getId().getNucnfclt(), ctd.getId().getNualosec());
      }
    }
    else {
      LOG.debug("El campo tmct0ord es nulo");
    }

    return imsvb;
  }

  /**
   * Obtiene un objeto Properties con la configuraciÃ³n relacionada con el
   * proceso pasado como parÃ¡metro
   * 
   * @param process
   * @param hpm Gestor de persistencia
   * @return un objeto Properties con la configuraciÃ³n relacionada con el
   * proceso pasado como parÃ¡metro
   * @throws Exception
   */
  public static Properties getCfgForProcess(String process) {
    Properties result = new Properties();
    try {

      List<Tmct0cfg> list = (List<Tmct0cfg>) getCfgMultiple(process, "");

      if (list.size() > 0) {
        for (Tmct0cfg item : list) {
          result.put(item.getId().getKeyname().trim(), item.getKeyvalue().trim());
        }
      }
    }
    catch (Exception e) {
      // Poner mensaje de log
    }
    finally {
    }
    return result;
  }

  private static Map<String, Tmct0cfg> cfg = new HashMap<String, Tmct0cfg>();

  public static List<Tmct0cfg> getCfgMultiple(String process, String keyname) {
    List<Tmct0cfg> list = new ArrayList<Tmct0cfg>();
    String key = getKey(process, keyname, "");
    for (String keyCfg : cfg.keySet()) {
      if (keyCfg.startsWith(key)) {
        list.add(cfg.get(keyCfg));
      }
    }
    Collections.sort(list, new Comparator<Tmct0cfg>() {

      @Override
      public int compare(Tmct0cfg object1, Tmct0cfg object2) {
        if (object1.getId().getProcess().compareTo(object2.getId().getProcess()) != 0) {
          return object1.getId().getProcess().compareTo(object2.getId().getProcess());
        }
        else if (object1.getId().getKeyname().compareTo(object2.getId().getKeyname()) != 0) {
          return object1.getId().getKeyname().compareTo(object2.getId().getKeyname());
        }
        else {
          return object1.getKeyvalue().compareTo(object2.getKeyvalue());
        }
      }

    });
    return list;
  }

  private static String getKey(String key1, String key2, String key3) {
    return key1.trim() + "" + key2.trim() + "" + key3.trim();
  }

  public String getCdalias(String cdalias) {
    cdalias = cdalias.trim();
    String[] alias = cdalias.split("\\|");
    if (alias.length == 2)
      cdalias = alias[1].trim();
    return cdalias;
  }

  public Tmct0est getTmct0est(String cdaliass, BigDecimal fecha) {
    Tmct0est tmct0est = null;
    String cdalias = getCdalias(cdaliass);
    tmct0est = tmct0estBo.findByCdaliassFechaActual(cdalias, fecha);
    return tmct0est;
  }

  public String getInforas4(String nbcamxml, String inforxml, String nbcamas4) {
    Tmct0xas destinoCfg = tmct0xasBo.findByNameAndInforAndNbcamas(nbcamxml, inforxml, nbcamas4);
    String result = "";
    if (destinoCfg != null && (destinoCfg.getInforas4() != null)) {
      result = destinoCfg.getInforas4().trim();
    }
    return result;
  }

  ////////////////////////////////////////////////

  private void launchMinoristasWriteLine(List<Object[]> minoristasList, BufferedWriter writer)
      throws InterruptedException {
    final List<CallableProcessInterface> processInterfaces = new ArrayList<>();

    final CallableNacionalMinoristasLineThreadsBo writeMinoristasLine = ctx
        .getBean(CallableNacionalMinoristasLineThreadsBo.class);

    writeMinoristasLine.setWriter(writer);
    writeMinoristasLine.setMinoristasList(minoristasList);

    processInterfaces.add(writeMinoristasLine);

    try {
      launchThreadBatchMinoristas(processInterfaces);
    }
    catch (SIBBACBusinessException e) {
    }

  }

  /**
   * Lanza los hilos
   * @param processInterfaces
   */
  public void launchThreadBatchMinoristas(List<CallableProcessInterface> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final List<CallableProcessInterface> futureList = new ArrayList<>();

    final String currentProcessName = "MinoristasWriter";

    final ExecutorService executor = Executors.newFixedThreadPool(1,
        new NamedThreadFactory("batch_" + currentProcessName));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableProcessInterface callableBo : processInterfaces) {

      final Future<Boolean> submited = executor.submit(callableBo);

      callableBo.setFuture(submited);

      callableBo.setProcessName(currentProcessName);

      futureList.add(callableBo);
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (!onTime) { // *
      for (CallableProcessInterface future : futureList) {
        if (!future.getFuture().isDone()) {
          future.getFuture().cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
  }

  public String getFileNameMinoristas() {
    return fileNameMinoristas;
  }

  public void setFileNameMinoristas(String fileNameMinoristas) {
    this.fileNameMinoristas = fileNameMinoristas;
  }

  public void configureName() {
    super.setFileName(this.fileNameMinoristas);
  }

}
