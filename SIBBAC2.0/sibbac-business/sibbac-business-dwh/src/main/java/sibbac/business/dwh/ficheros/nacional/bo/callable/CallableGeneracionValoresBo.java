package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableListProcessFileResult;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.tasks.TaskGeneracionDWHValores;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGeneracionValoresBo implements CallableListProcessFileResult {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGeneracionValoresBo.class);

  @Autowired
  private TaskGeneracionDWHValores taskGeneracionDWHValores;

  private Future<List<ProcessFileResultDTO>> future;

  @Override
  public List<ProcessFileResultDTO> call() throws Exception {
    List<ProcessFileResultDTO> result = null;

    try {
      taskGeneracionDWHValores.executeTask();
      result = taskGeneracionDWHValores.getProcessFileResultsList();
    }
    catch (SIBBACBusinessException e) {
      LOG.error("Error durante la ejecución del proceso de generación del fichero de clientes", e);
    }

    return result;
  }

  @Override
  public Future<List<ProcessFileResultDTO>> getFuture() {
    return future;
  }

  @Override
  public void setFuture(Future<List<ProcessFileResultDTO>> future) {
    this.future = future;
  }

}
