package sibbac.business.dwh.ficheros.valores.db.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.valores.db.model.MaestroValores;

@Repository
public interface CargaValoresDao extends JpaRepository<MaestroValores, BigInteger> {

  /**
   * Query para limpiar tabla TMCT0_VALORES_MERCADO
   */
  @Transactional
  @Modifying
  @Query(value = "DELETE FROM TMCT0_VALORES_MERCADO", nativeQuery = true)
  public void cleanMercado();

  /**
   * Query para limpiar tabla TMCT0_VALORES_WARRANT
   */
  @Transactional
  @Modifying
  @Query(value = "DELETE FROM TMCT0_VALORES_WARRANT", nativeQuery = true)
  public void cleanWarrant();

  /**
   * Query para limpiar tabla TMCT0_VALORES_EMISOR
   */
  @Transactional
  @Modifying
  @Query(value = "DELETE FROM TMCT0_VALORES_EMISOR", nativeQuery = true)
  public void cleanEmisor();

  /**
   * Query para limpiar tabla TMCT0_VALORES_EMISION
   */
  @Transactional
  @Modifying
  @Query(value = "DELETE FROM TMCT0_VALORES_EMISION", nativeQuery = true)
  public void cleanEmision();

  public static final String FICHERO_VALORES_QUERY = "SELECT DISTINCT(O.CDISIN) FROM TMCT0ORD O WHERE O.FECONTRA = (CURRENT DATE - 1)";

  /**
   * Query para obtener todos los ISIN con fecha contrato igual al dia anterior
   */
  @Query(value = FICHERO_VALORES_QUERY, nativeQuery = true)
  public List<Object[]> getYesterdayIsin();

  /**
   * Query para DWHficheroValoresDao con Paginación
   */
  public static final String FICHERO_VALORES_PAGE_QUERY = FICHERO_VALORES_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHficheroValoresDao
   */
  public static final String FICHERO_VALORES_PAGE_QUERY_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM ("
      + FICHERO_VALORES_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHficheroValoresDao sin
   * paginación
   * @return
   */
  @Query(value = FICHERO_VALORES_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListFicheroValor(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHagrupTipoValorDao sin
   * paginación
   * @return
   */
  @Query(value = FICHERO_VALORES_PAGE_QUERY_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListFicheroValor();

  /**
   * Query para obtener todos los ISIN de maestros
   */
  @Query(value = "SELECT COUNT(*) FROM TMCT0_MAESTRO_VALORES V WHERE V.CDISINVV = :isin AND V.FHBAJACV > CURRENT DATE", nativeQuery = true)
  public Integer getMasterIsin(@Param("isin") String isin);

  /**
   * Query para validar
   * @return List<Object[]>
   */
  @Query(value = "SELECT NBTITREV, NUNOMINV, CDSISLIV, CDNOMINV, CDISINVV, NBTIT10C, CDORGANO "
      + "FROM TMCT0_MAESTRO_VALORES " + "WHERE CDCVALOV = SUBSTR(:cdemisio, 5, 8) "
      + "AND CDBESNUM = '9999'||SUBSTR(:cdemisio, 1, 4) " + "AND FHBAJACV = CURRENT_DATE - 1 DAY", nativeQuery = true)
  public List<Object[]> validacionValores(@Param("cdemisio") String cdemisio);

  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_MAESTRO_VALORES SET FHBAJACV = CURRENT_DATE - 1 DAY, "
      + "USUBAJA = 'TaskCargaValores-Partenon' WHERE CDISINVV IN :cdisin "
      + "AND CDBESNUM <>'00000000' AND SUBSTR(CDBESNUM, 1, 4) <>'9999' "
      + "AND FHBAJACV >= CURRENT_DATE", nativeQuery = true)
  public void funcionBajaIsinsAnteriores(@Param("cdisin") List<String> cdisin);

  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_MAESTRO_VALORES SET FHBAJACV = CURRENT_DATE, "
      + "CDSITVAL = 'C', USUBAJA = 'TaskCargaValores-Partenon' WHERE CDCVALOV IN :cdemisio58 "
      + "AND CDBESNUM IN :cdemisio14 AND FHBAJACV >= CURRENT_DATE", nativeQuery = true)
  public void funcionBajaIsin(@Param("cdemisio58") List<String> cdemisio58,
      @Param("cdemisio14") List<String> cdemisio14);

  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_MAESTRO_VALORES SET USUMODI = 'TaskCargaValores-Partenon', "
      + "FHMODIV = CURRENT_DATE WHERE CDCVALOV IN :cdemisio58 "
      + "AND CDBESNUM IN :cdemisio14 AND FHBAJACV > CURRENT_DATE - 1", nativeQuery = true)
  public void funcionMod(@Param("cdemisio58") List<String> cdemisio58, @Param("cdemisio14") List<String> cdemisio14);

  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_MAESTRO_VALORES SET "
      + "FHBAJACV = :feemisio, USUMODI = 'TaskCargaValores-Partenon' WHERE CDISINVV = :cdisinvv "
      + "AND CDBESNUM = '9999'|| SUBSTR(:cdemisio, 1, 4) AND FHBAJACV > CURRENT_DATE - 1", nativeQuery = true)
  public void funcionBajaEmisionAnterior(@Param("cdisinvv") List<String> cdisinvv,
      @Param("cdemisio") List<String> cdemisio, @Param("feemisio") List<Date> feemisio);

  @Query(value = 
      " SELECT "
      + "  VAL.CODIGO_DE_VALOR,  VAL.CDMONEDA,   VAL.DESCRIPCION,  VAL.ACTIVO, VAL.ID_TIPO_PRODUCTO, CASE WHEN COUNT(MVAL.CDISINVV) > 0 THEN 1 ELSE 0 END AS MODIFICAR " 
      + " FROM "
      + "  (SELECT DISTINCT VAL.CODIGO_DE_VALOR, VAL.DESCRIPCION, VAL.ACTIVO, VAL.ID_TIPO_PRODUCTO, MON.CDMONEDA  FROM TMCT0_VALORES VAL  INNER JOIN TMCT0_MONEDA MON ON MON.NBCODIGO = VAL.DIVISA) VAL " 
      + "  LEFT JOIN (SELECT CDISINVV FROM TMCT0_MAESTRO_VALORES WHERE FHBAJACV >= CURRENT DATE) MVAL ON VAL.CODIGO_DE_VALOR = MVAL.CDISINVV "
      + " GROUP BY "
      + " VAL.CODIGO_DE_VALOR, VAL.DESCRIPCION, VAL.ACTIVO, VAL.CDMONEDA, VAL.ID_TIPO_PRODUCTO", nativeQuery = true)
  public List<Object[]> getValoresByInsertOrUpdate();

  @Transactional
  @Modifying
  @Query(value = "UPDATE TMCT0_MAESTRO_VALORES SET CDINSTRU = :idTipoProducto, USUMODI='CargaValores-SIBBAC', "
      + "FHMODIV= CURRENT_DATE WHERE CDISINVV IN :codigoValor", nativeQuery = true)
  public void updateValorCargaSibbacBulk(@Param("idTipoProducto") Integer idTipoProducto,
      @Param("codigoValor") List<String> codigoValor);
  
}
