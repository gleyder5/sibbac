package sibbac.business.dwh.ficheros.valores.db.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class that represents a
 * {@link sibbac.business.dwh.database.model.MaestroValores }. Entity:
 * "MaestroValores".
 * 
 * @version 1.0
 * @since 1.0
 */
@Table(name = "TMCT0_MAESTRO_VALORES")
@Entity
public class MaestroValores implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -840277152853228871L;

  /** The idvalor. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDVALOR", nullable = false, length = 10)
  private BigInteger idvalor;

  /** The cdcvalov. */
  @Column(name = "CDCVALOV", nullable = true, length = 8)
  private String cdcvalov;

  /** The cdbesnum. */
  @Column(name = "CDBESNUM", nullable = true, length = 8)
  private String cdbesnum;

  /** The fhiniciv. */
  @Column(name = "FHINICIV", nullable = false)
  private Date fhiniciv;

  /** The cdisinvv. */
  @Column(name = "CDISINVV", nullable = false, length = 12)
  private String cdisinvv;

  /** The nbtitrev. */
  @Column(name = "NBTITREV", nullable = false, length = 31)
  private String nbtitrev;

  /** The cdvabev. */
  @Column(name = "CDCVABEV", nullable = false, length = 8)
  private String cdcvabev;

  /** The cdcvsibv. */
  @Column(name = "CDCVSIBV", nullable = true, length = 8)
  private String cdcvsibv;

  /** The nunominv. */
  @Column(name = "NUNOMINV", nullable = true, precision = 15, scale = 1)
  private BigDecimal nunominv;

  /** The cdcotmav. */
  @Column(name = "CDCOTMAV", nullable = true, length = 1)
  private String cdcotmav;

  /** The cdcotbav. */
  @Column(name = "CDCOTBAV", nullable = true, length = 1)
  private String cdcotbav;

  /** The cdcotbiv. */
  @Column(name = "CDCOTBIV", nullable = true, length = 1)
  private String cdcotbiv;

  /** The cdcotvav. */
  @Column(name = "CDCOTVAV", nullable = true, length = 1)
  private String cdcotvav;

  /** The cdmonedv. */
  @Column(name = "CDMONEDV", nullable = false, length = 3)
  private String cdmonedv;

  /** The cdsisliv. */
  @Column(name = "CDSISLIV", nullable = true, length = 1)
  private String cdsisliv;

  /** The cdnominv. */
  @Column(name = "CDNOMINV", nullable = true, length = 1)
  private String cdnominv;

  /** The cdgrsibv. */
  @Column(name = "CDGRSIBV", nullable = true, length = 2)
  private String cdgrsibv;

  /** The cdvaibex. */
  @Column(name = "CDVAIBEX", nullable = true, length = 1)
  private String cdvaibex;

  /** The cdinstru. */
  @Column(name = "CDINSTRU", nullable = true, length = 2)
  private String cdinstru;

  /** The cdcifsoc. */
  @Column(name = "CDCIFSOC", nullable = true, length = 9)
  private String cdcifsoc;

  /** The cdpaisev. */
  @Column(name = "CDPAISEV", nullable = true, length = 3)
  private String cdpaisev;

  /** The cdpercuv. */
  @Column(name = "CDPERCUV", nullable = true, length = 1)
  private String cdpercuv;

  /** The nbtit 10 c. */
  @Column(name = "NBTIT10C", nullable = true, length = 10)
  private String nbtit10c;

  /** The cdorgano. */
  @Column(name = "CDORGANO", nullable = true, length = 4)
  private String cdorgano;

  /** The cdsitval. */
  @Column(name = "CDSITVAL", nullable = true, length = 1)
  private String cdsitval;

  /** The idmercon. */
  @Column(name = "IDMERCON", nullable = true, length = 2)
  private String idmercon;

  /** The tpcambio. */
  @Column(name = "TPCAMBIO", nullable = true, length = 1)
  private String tpcambio;

  /** The nudelote. */
  @Column(name = "NUDELOTE", nullable = true, precision = 7, scale = 0)
  private BigDecimal nudelote;

  /** The fhultmod. */
  @Column(name = "FHULTMOD", nullable = true)
  private Date fhultmod;

  /** The fhaltav. */
  @Column(name = "FHALTAV", nullable = false)
  private Date fhaltav;

  /** The usualta. */
  @Column(name = "USUALTA", nullable = false, length = 100)
  private String usualta;

  /** The fhmodiv. */
  @Column(name = "FHMODIV", nullable = true)
  private Date fhmodiv;

  /** The usumodi. */
  @Column(name = "USUMODI", nullable = true, length = 100)
  private String usumodi;

  /** The fhbajacv. */
  @Column(name = "FHBAJACV", nullable = false)
  private Date fhbajacv;

  /** The usubaja. */
  @Column(name = "USUBAJA", nullable = true, length = 100)
  private String usubaja;

  /** The serie. */
  @Column(name = "NUSERIEV", nullable = true, columnDefinition = "VARCHAR(2) DEFAULT '  '")
  private String serie;

  /** The pasa A. */
  @Column(name = "CDCVNEWV", nullable = true, columnDefinition = "VARCHAR(8) DEFAULT '00000000'")
  private String pasaA;

  /** The junta sindica. */
  @Column(name = "CDCVAJSV", nullable = true, columnDefinition = "VARCHAR(8) DEFAULT '00000000'")
  private String juntaSindica;

  /** The cotiza santander. */
  @Column(name = "CDCOTSAV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String cotizaSantander;

  /** The cdcotvrv. */
  @Column(name = "CDCOTVRV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String cdcotvrv;

  /** The cdcotexv. */
  @Column(name = "CDCOTEXV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String cdcotexv;

  public String getCdcotvrv() {
    return cdcotvrv;
  }

  public void setCdcotvrv(String cdcotvrv) {
    this.cdcotvrv = cdcotvrv;
  }

  public String getCdcotexv() {
    return cdcotexv;
  }

  public void setCdcotexv(String cdcotexv) {
    this.cdcotexv = cdcotexv;
  }

  public String getCddivisv() {
    return cddivisv;
  }

  public void setCddivisv(String cddivisv) {
    this.cddivisv = cddivisv;
  }

  /** The cotiza excepciones. */
  @Column(name = "CDCOTESV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String cotizaExcepciones;

  /** The cotiza merc unico. */
  @Column(name = "CDCOTUNV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String cotizaMercUnico;

  /** The banco extrangero. */
  @Column(name = "CDBOEXTV", nullable = true, columnDefinition = "VARCHAR(4) DEFAULT '    '")
  private String bancoExtrangero;

  /** The calificado. */
  @Column(name = "CDCALIFV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String calificado;

  /** The codigo dimision. */
  @Column(name = "CDCOMISV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String codigoDimision;

  /** The grupo comision. */
  @Column(name = "CDGRCOMV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String grupoComision;

  /** The titulos pts cupones. */
  @Column(name = "CDPESETV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT '0'")
  private String titulosPtsCupones;

  /** The polizas. */
  @Column(name = "CDPOLIZV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String polizas;

  /** The incorpor. */
  @Column(name = "CDINCORV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String incorpor;

  /** The clase. */
  @Column(name = "CDFICHEV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String clase;

  /** The fecha ult amortiz. */
  @Column(name = "FHULAMOV", nullable = true, length = 6, columnDefinition = "VARCHAR(6) DEFAULT '000000'")
  private String fechaUltAmortiz;

  /** The fecha prim amortiz. */
  @Column(name = "FHPRAMOV", nullable = true, length = 6, columnDefinition = "VARCHAR(6) DEFAULT '000000'")
  private String fechaPrimAmortiz;

  /** The fecha reembolso. */
  @Column(name = "FHREEBAV", nullable = true, columnDefinition = "VARCHAR(4) DEFAULT '0000'")
  private String fechaReembolso;

  /** The cod periodo amort. */
  @Column(name = "CDPERAMV", nullable = true, columnDefinition = "VARCHAR(2) DEFAULT '  '")
  private String codPeriodoAmort;

  /** The cod factur amort. */
  @Column(name = "CDFACAMV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String codFacturAmort;

  /** The gastos cobro. */
  @Column(name = "CDGCOAMV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String gastosCobro;

  /** The convertibles. */
  @Column(name = "CDCONVEV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String convertibles;

  /** The fecha opc amort A. */
  @Column(name = "FHAMORAV", nullable = true, length = 6, columnDefinition = "VARCHAR(6) DEFAULT '000000'")
  private String fechaOpcAmortA;

  /** The fecha opc amort B. */
  @Column(name = "FHAMORBV", nullable = true, length = 6, columnDefinition = "VARCHAR(6) DEFAULT '000000'")
  private String fechaOpcAmortB;

  /** The fecha opc amort C. */
  @Column(name = "FHAMORCV", nullable = true, length = 6, columnDefinition = "VARCHAR(6) DEFAULT '000000'")
  private String fechaOpcAmortC;

  /** The banco amortiz. */
  @Column(name = "NBBANQCV", nullable = true, columnDefinition = "VARCHAR(20) DEFAULT '                    '")
  private String bancoAmortiz;

  /** The naturaleza emisor. */
  @Column(name = "CDNAEMIV", nullable = true, length = 3, columnDefinition = "VARCHAR(3) DEFAULT '   '")
  private String naturalezaEmisor;

  /** The clase valor. */
  @Column(name = "CDCLASEV", nullable = true, columnDefinition = "VARCHAR(2) DEFAULT '  '")
  private String claseValor;

  /** The divisa. */
  @Column(name = "CDDIVISV", nullable = true, columnDefinition = "VARCHAR(3) DEFAULT '000'")
  private String cddivisv;

  /** The porc imp deveng. */
  @Column(name = "PCIMDEVV", nullable = true, columnDefinition = "DECIMAL(5,2) DEFAULT '000,00'")
  private BigDecimal porcImpDeveng;

  /** The porc imp socied. */
  @Column(name = "PCIMSOCV", nullable = true, columnDefinition = "DECIMAL(5,2) DEFAULT '000,00'")
  private BigDecimal porcImpSocied;

  /** The importe bruto. */
  @Column(name = "CTIMBRUV", nullable = true, columnDefinition = "DECIMAL(11,3) DEFAULT '00000000,000'")
  private BigDecimal importeBruto;

  /** The importe neto. */
  @Column(name = "CTIMNETV", nullable = true, columnDefinition = "DECIMAL(11,3) DEFAULT '00000000,000'")
  private BigDecimal importeNeto;

  /** The factura cupones. */
  @Column(name = "CDFACCUV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String facturaCupones;

  /** The rel pro cupones. */
  @Column(name = "CDPROCUV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String relProCupones;

  /** The gastos cobro cupon. */
  @Column(name = "CDGCOCUV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String gastosCobroCupon;

  /** The vencimiento cupones. */
  @Column(name = "CDVTOCUV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT ' '")
  private String vencimientoCupones;

  /** The fecha vto base. */
  @Column(name = "FHVTOBAV", nullable = true, columnDefinition = "VARCHAR(4) DEFAULT '0000'")
  private String fechaVtoBase;

  /** The n ultimo cupon. */
  @Column(name = "NUULTCUV", nullable = true, columnDefinition = "VARCHAR(4) DEFAULT '0000'")
  private String nUltimoCupon;

  /** The observ cupon. */
  @Column(name = "TXCUPONV", nullable = true, columnDefinition = "VARCHAR(20) DEFAULT '                    '")
  private String observCupon;

  /** The observ amortiz. */
  @Column(name = "TXAMORTV", nullable = true, columnDefinition = "VARCHAR(20) DEFAULT '                    '")
  private String observAmortiz;

  /** The nuagealc. */
  @Column(name = "NUAGEALC", nullable = true, columnDefinition = "DECIMAL(5,0) DEFAULT '00000'")
  private BigDecimal nuagealc;

  /** The nuageapc. */
  @Column(name = "NUAGEAPC", nullable = true, columnDefinition = "DECIMAL(5,0) DEFAULT '00000'")
  private BigDecimal nuageapc;

  /** The cod continuo barcelona. */
  @Column(name = "CDCOCBAV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String codContinuoBarcelona;

  /** The cod continuo bilbao. */
  @Column(name = "CDCOCBIV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String codContinuoBilbao;

  /** The cod continuo valencia. */
  @Column(name = "CDCOCVAV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String codContinuoValencia;

  /** The cod continuo madrid. */
  @Column(name = "CDCOCMAV", nullable = true, columnDefinition = "VARCHAR(1) DEFAULT 'N'")
  private String codContinuoMadrid;

  /** The cdactivi. */
  @Column(name = "CDACTIVI", nullable = true, columnDefinition = "VARCHAR(3) DEFAULT '000'")
  private String cdactivi;

  /** The cdclaval. */
  @Column(name = "CDCLAVAL", nullable = true, columnDefinition = "VARCHAR(2) DEFAULT '00'")
  private String cdclaval;
  
  public MaestroValores()
  {
    super();
    this.serie = "  ";
    this.pasaA = "00000000";
    this.juntaSindica = "00000000";
    this.cotizaSantander = "N";
    this.cdcotvrv = "N";
    this.cdcotexv = "N";
    this.cotizaExcepciones = "N";
    this.cotizaMercUnico = "N";
    this.bancoExtrangero = "    ";
    this.calificado = "N";
    this.codigoDimision = " ";
    this.grupoComision = " ";
    this.titulosPtsCupones = "0";
    this.polizas = " ";
    this.incorpor = "N";
    this.clase = " ";
    this.fechaUltAmortiz = "000000";
    this.fechaPrimAmortiz = "000000";
    this.fechaReembolso = "0000";
    this.codPeriodoAmort = "  ";
    this.codFacturAmort = "N";
    this.gastosCobro = "N";
    this.convertibles = "N";
    this.fechaOpcAmortA = "000000";
    this.fechaOpcAmortB = "000000";
    this.fechaOpcAmortC = "000000";
    this.bancoAmortiz = "                    ";
    this.naturalezaEmisor = "   ";
    this.claseValor = "  ";
    this.cddivisv = "000";
    this.porcImpDeveng = BigDecimal.valueOf(000,00);
    this.porcImpSocied = BigDecimal.valueOf(000,00);
    this.importeBruto = BigDecimal.valueOf(00000000,000);
    this.importeNeto = BigDecimal.valueOf(00000000,000);
    this.facturaCupones = " ";
    this.relProCupones = " ";
    this.gastosCobroCupon = " ";
    this.vencimientoCupones = " ";
    this.fechaVtoBase = "0000";
    this.nUltimoCupon = "0000";
    this.observCupon = "                    ";
    this.observAmortiz = "                    ";
    this.nuagealc = new BigDecimal(00000);
    this.nuageapc = new BigDecimal(00000);
    this.codContinuoBarcelona = "N";
    this.codContinuoBilbao = "N";
    this.codContinuoValencia = "N";
    this.codContinuoMadrid = "N";
    this.cdactivi = "000";
    this.cdclaval = "00";
  }



  /**
   * Gets the idvalor.
   *
   * @return the idvalor
   */
  public BigInteger getIdvalor() {
    return idvalor;
  }

  /**
   * Sets the idvalor.
   *
   * @param idvalor the new idvalor
   */
  public void setIdvalor(BigInteger idvalor) {
    this.idvalor = idvalor;
  }

  /**
   * Gets the cdcvalov.
   *
   * @return the cdcvalov
   */
  public String getCdcvalov() {
    return cdcvalov;
  }

  /**
   * Sets the cdcvalov.
   *
   * @param cdcvalov the new cdcvalov
   */
  public void setCdcvalov(String cdcvalov) {
    this.cdcvalov = cdcvalov;
  }

  /**
   * Gets the cdbesnum.
   *
   * @return the cdbesnum
   */
  public String getCdbesnum() {
    return cdbesnum;
  }

  /**
   * Sets the cdbesnum.
   *
   * @param cdbesnum the new cdbesnum
   */
  public void setCdbesnum(String cdbesnum) {
    this.cdbesnum = cdbesnum;
  }

  /**
   * Gets the fhiniciv.
   *
   * @return the fhiniciv
   */
  public Date getFhiniciv() {
    return fhiniciv;
  }

  /**
   * Sets the fhiniciv.
   *
   * @param fhiniciv the new fhiniciv
   */
  public void setFhiniciv(Date fhiniciv) {
    this.fhiniciv = fhiniciv;
  }

  /**
   * Gets the cdisinvv.
   *
   * @return the cdisinvv
   */
  public String getCdisinvv() {
    return cdisinvv;
  }

  /**
   * Sets the cdisinvv.
   *
   * @param cdisinvv the new cdisinvv
   */
  public void setCdisinvv(String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  /**
   * Gets the nbtitrev.
   *
   * @return the nbtitrev
   */
  public String getNbtitrev() {
    return nbtitrev;
  }

  /**
   * Sets the nbtitrev.
   *
   * @param nbtitrev the new nbtitrev
   */
  public void setNbtitrev(String nbtitrev) {
    this.nbtitrev = nbtitrev;
  }

  /**
   * Gets the cdvabev.
   *
   * @return the cdvabev
   */
  public String getCdcvabev() {
    return cdcvabev;
  }

  /**
   * Sets the cdcvabev.
   *
   * @param cdcvabev the new cdcvabev
   */
  public void setCdcvabev(String cdcvabev) {
    this.cdcvabev = cdcvabev;
  }

  /**
   * Gets the cdcvsibv.
   *
   * @return the cdcvsibv
   */
  public String getCdcvsibv() {
    return cdcvsibv;
  }

  /**
   * Sets the cdcvsibv.
   *
   * @param cdcvsibv the new cdcvsibv
   */
  public void setCdcvsibv(String cdcvsibv) {
    this.cdcvsibv = cdcvsibv;
  }

  /**
   * Gets the nunominv.
   *
   * @return the nunominv
   */
  public BigDecimal getNunominv() {
    return nunominv;
  }

  /**
   * Sets the nunominv.
   *
   * @param nunominv the new nunominv
   */
  public void setNunominv(BigDecimal nunominv) {
    this.nunominv = nunominv;
  }

  /**
   * Gets the cdcotmav.
   *
   * @return the cdcotmav
   */
  public String getCdcotmav() {
    return cdcotmav;
  }

  /**
   * Sets the cdcotmav.
   *
   * @param cdcotmav the new cdcotmav
   */
  public void setCdcotmav(String cdcotmav) {
    this.cdcotmav = cdcotmav;
  }

  /**
   * Gets the cdcotbav.
   *
   * @return the cdcotbav
   */
  public String getCdcotbav() {
    return cdcotbav;
  }

  /**
   * Sets the cdcotbav.
   *
   * @param cdcotbav the new cdcotbav
   */
  public void setCdcotbav(String cdcotbav) {
    this.cdcotbav = cdcotbav;
  }

  /**
   * Gets the cdcotbiv.
   *
   * @return the cdcotbiv
   */
  public String getCdcotbiv() {
    return cdcotbiv;
  }

  /**
   * Sets the cdcotbiv.
   *
   * @param cdcotbiv the new cdcotbiv
   */
  public void setCdcotbiv(String cdcotbiv) {
    this.cdcotbiv = cdcotbiv;
  }

  /**
   * Gets the cdcotvav.
   *
   * @return the cdcotvav
   */
  public String getCdcotvav() {
    return cdcotvav;
  }

  /**
   * Sets the cdcotvav.
   *
   * @param cdcotvav the new cdcotvav
   */
  public void setCdcotvav(String cdcotvav) {
    this.cdcotvav = cdcotvav;
  }

  /**
   * Gets the cdmonedv.
   *
   * @return the cdmonedv
   */
  public String getCdmonedv() {
    return cdmonedv;
  }

  /**
   * Sets the cdmonedv.
   *
   * @param cdmonedv the new cdmonedv
   */
  public void setCdmonedv(String cdmonedv) {
    this.cdmonedv = cdmonedv;
  }

  /**
   * Gets the cdsisliv.
   *
   * @return the cdsisliv
   */
  public String getCdsisliv() {
    return cdsisliv;
  }

  /**
   * Sets the cdsisliv.
   *
   * @param cdsisliv the new cdsisliv
   */
  public void setCdsisliv(String cdsisliv) {
    this.cdsisliv = cdsisliv;
  }

  /**
   * Gets the cdnominv.
   *
   * @return the cdnominv
   */
  public String getCdnominv() {
    return cdnominv;
  }

  /**
   * Sets the cdnominv.
   *
   * @param cdnominv the new cdnominv
   */
  public void setCdnominv(String cdnominv) {
    this.cdnominv = cdnominv;
  }

  /**
   * Gets the cdgrsibv.
   *
   * @return the cdgrsibv
   */
  public String getCdgrsibv() {
    return cdgrsibv;
  }

  /**
   * Sets the cdgrsibv.
   *
   * @param cdgrsibv the new cdgrsibv
   */
  public void setCdgrsibv(String cdgrsibv) {
    this.cdgrsibv = cdgrsibv;
  }

  /**
   * Gets the cdvaibex.
   *
   * @return the cdvaibex
   */
  public String getCdvaibex() {
    return cdvaibex;
  }

  /**
   * Sets the cdvaibex.
   *
   * @param cdvaibex the new cdvaibex
   */
  public void setCdvaibex(String cdvaibex) {
    this.cdvaibex = cdvaibex;
  }

  /**
   * Gets the cdinstru.
   *
   * @return the cdinstru
   */
  public String getCdinstru() {
    return cdinstru;
  }

  /**
   * Sets the cdinstru.
   *
   * @param cdinstru the new cdinstru
   */
  public void setCdinstru(String cdinstru) {
    this.cdinstru = cdinstru;
  }

  /**
   * Gets the cdcifsoc.
   *
   * @return the cdcifsoc
   */
  public String getCdcifsoc() {
    return cdcifsoc;
  }

  /**
   * Sets the cdcifsoc.
   *
   * @param cdcifsoc the new cdcifsoc
   */
  public void setCdcifsoc(String cdcifsoc) {
    this.cdcifsoc = cdcifsoc;
  }

  /**
   * Gets the cdpaisev.
   *
   * @return the cdpaisev
   */
  public String getCdpaisev() {
    return cdpaisev;
  }

  /**
   * Sets the cdpaisev.
   *
   * @param cdpaisev the new cdpaisev
   */
  public void setCdpaisev(String cdpaisev) {
    this.cdpaisev = cdpaisev;
  }

  /**
   * Gets the cdpercuv.
   *
   * @return the cdpercuv
   */
  public String getCdpercuv() {
    return cdpercuv;
  }

  /**
   * Sets the cdpercuv.
   *
   * @param cdpercuv the new cdpercuv
   */
  public void setCdpercuv(String cdpercuv) {
    this.cdpercuv = cdpercuv;
  }

  /**
   * Gets the nbtit 10 c.
   *
   * @return the nbtit 10 c
   */
  public String getNbtit10c() {
    return nbtit10c;
  }

  /**
   * Sets the nbtit 10 c.
   *
   * @param nbtit10c the new nbtit 10 c
   */
  public void setNbtit10c(String nbtit10c) {
    this.nbtit10c = nbtit10c;
  }

  /**
   * Gets the cdorgano.
   *
   * @return the cdorgano
   */
  public String getCdorgano() {
    return cdorgano;
  }

  /**
   * Sets the cdorgano.
   *
   * @param cdorgano the new cdorgano
   */
  public void setCdorgano(String cdorgano) {
    this.cdorgano = cdorgano;
  }

  /**
   * Gets the cdsitval.
   *
   * @return the cdsitval
   */
  public String getCdsitval() {
    return cdsitval;
  }

  /**
   * Sets the cdsitval.
   *
   * @param cdsitval the new cdsitval
   */
  public void setCdsitval(String cdsitval) {
    this.cdsitval = cdsitval;
  }

  /**
   * Gets the idmercon.
   *
   * @return the idmercon
   */
  public String getIdmercon() {
    return idmercon;
  }

  /**
   * Sets the idmercon.
   *
   * @param idmercon the new idmercon
   */
  public void setIdmercon(String idmercon) {
    this.idmercon = idmercon;
  }

  /**
   * Gets the tpcambio.
   *
   * @return the tpcambio
   */
  public String getTpcambio() {
    return tpcambio;
  }

  /**
   * Sets the tpcambio.
   *
   * @param tpcambio the new tpcambio
   */
  public void setTpcambio(String tpcambio) {
    this.tpcambio = tpcambio;
  }

  /**
   * Gets the nudelote.
   *
   * @return the nudelote
   */
  public BigDecimal getNudelote() {
    return nudelote;
  }

  /**
   * Sets the nudelote.
   *
   * @param nudelote the new nudelote
   */
  public void setNudelote(BigDecimal nudelote) {
    this.nudelote = nudelote;
  }

  /**
   * Gets the fhultmod.
   *
   * @return the fhultmod
   */
  public Date getFhultmod() {
    return fhultmod;
  }

  /**
   * Sets the fhultmod.
   *
   * @param fhultmod the new fhultmod
   */
  public void setFhultmod(Date fhultmod) {
    this.fhultmod = fhultmod;
  }

  /**
   * Gets the fhaltav.
   *
   * @return the fhaltav
   */
  public Date getFhaltav() {
    return fhaltav;
  }

  /**
   * Sets the fhaltav.
   *
   * @param fhaltav the new fhaltav
   */
  public void setFhaltav(Date fhaltav) {
    this.fhaltav = fhaltav;
  }

  /**
   * Gets the usualta.
   *
   * @return the usualta
   */
  public String getUsualta() {
    return usualta;
  }

  /**
   * Sets the usualta.
   *
   * @param usualta the new usualta
   */
  public void setUsualta(String usualta) {
    this.usualta = usualta;
  }

  /**
   * Gets the fhmodiv.
   *
   * @return the fhmodiv
   */
  public Date getFhmodiv() {
    return fhmodiv;
  }

  /**
   * Sets the fhmodiv.
   *
   * @param fhmodiv the new fhmodiv
   */
  public void setFhmodiv(Date fhmodiv) {
    this.fhmodiv = fhmodiv;
  }

  /**
   * Gets the usumodi.
   *
   * @return the usumodi
   */
  public String getUsumodi() {
    return usumodi;
  }

  /**
   * Sets the usumodi.
   *
   * @param usumodi the new usumodi
   */
  public void setUsumodi(String usumodi) {
    this.usumodi = usumodi;
  }

  /**
   * Gets the fhbajacv.
   *
   * @return the fhbajacv
   */
  public Date getFhbajacv() {
    return fhbajacv;
  }

  /**
   * Sets the fhbajacv.
   *
   * @param fhbajacv the new fhbajacv
   */
  public void setFhbajacv(Date fhbajacv) {
    this.fhbajacv = fhbajacv;
  }

  /**
   * Gets the usubaja.
   *
   * @return the usubaja
   */
  public String getUsubaja() {
    return usubaja;
  }

  /**
   * Sets the usubaja.
   *
   * @param usubaja the new usubaja
   */
  public void setUsubaja(String usubaja) {
    this.usubaja = usubaja;
  }

  /**
   * Gets the serialversionuid.
   *
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }
}
