package sibbac.business.dwh.ficheros.valores.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.valores.db.model.Moneda;

@Repository
public interface MonedaDao extends JpaRepository<Moneda, String> {

  @Query(value = "SELECT CDMONEDA, NBDESCRIPCION FROM TMCT0_MONEDA ORDER BY CDMONEDA ASC", nativeQuery = true)
  public List<Object[]> getCdmonedaAndNbdescripcion();

}
