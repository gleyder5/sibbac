package sibbac.business.dwh.ficheros.clientes.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.clientes.db.dao.DWHClientesEstaticosDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.common.file.AbstractGenerateFile;

@Service("dwhClientesFileGenerator")
public class GenerateClientesEstaticosFile extends AbstractGenerateFile {

  private static final String CLIENTES_LINE_SEPARATOR = "\r\n";

  private static final Logger LOG = LoggerFactory.getLogger(GenerateClientesEstaticosFile.class);

  private static int CLIENTES_CDCLIENT = 0;
  private static int CLIENTES_NBDESCLI = 1;
  private static int CLIENTES_NUCLIENT = 2;
  private static int CLIENTES_CDPRODUC = 3;
  private static int CLIENTES_CDGRUPOO = 4;
  private static int CLIENTES_NBGRUPOO = 5;
  private static int CLIENTES_CDLITRA = 6;
  private static int CLIENTES_CDLISAL = 7;
  private static int CLIENTES_CDCLIACM = 8;
  private static int CLIENTES_CDHPPAIS = 9;
  private static int CLIENTES_NUNIVEL0 = 10;
  private static int CLIENTES_NBNIVEL0 = 11;
  private static int CLIENTES_NUNIVEL1 = 12;
  private static int CLIENTES_NBNIVEL1 = 13;
  private static int CLIENTES_NUNIVEL2 = 14;
  private static int CLIENTES_NBNIVEL2 = 15;
  private static int CLIENTES_NUNIVEL3 = 16;
  private static int CLIENTES_NBNIVEL3 = 17;
  private static int CLIENTES_NUNIVEL4 = 18;
  private static int CLIENTES_NBNIVEL4 = 19;
  private static int CLIENTES_CDGRUPOA = 20;
  private static int CLIENTES_NBGRUPOA = 21;
  /**
   * DAO de Acceso a Base de Datos
   */
  @Autowired
  private DWHClientesEstaticosDao dwhClientesEstaticosDao;

  @Override
  public String getLineSeparator() {
    return CLIENTES_LINE_SEPARATOR;
  }

  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    return dwhClientesEstaticosDao.findAllListClientesEstaticos(pageable.getOffset(), pageable.getPageSize());
  }

  @Override
  public Integer getFileCountData() {
    return dwhClientesEstaticosDao.countAllListClientesEstaticos();
  }

  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {

      Integer cdcli = (Integer) (objects[CLIENTES_CDCLIENT]);
      String nbdesclien = (String) objects[CLIENTES_NBDESCLI];
      Integer nucliente = (Integer) (objects[CLIENTES_NUCLIENT]);
      String cdproducto = (String) objects[CLIENTES_CDPRODUC];
      Integer cdgrupo = (Integer) (objects[CLIENTES_CDGRUPOO]);
      String nbgrupo = (String) objects[CLIENTES_NBGRUPOO];
      Integer cdclitraa = (Integer) (objects[CLIENTES_CDLITRA]);
      Integer cdclisall = (Integer) (objects[CLIENTES_CDLISAL]);
      Integer cdcliacmm = (Integer) (objects[CLIENTES_CDCLIACM]);
      String cdhppaiss = String.valueOf(objects[CLIENTES_CDHPPAIS]);
      Integer nunivel00 = (Integer) (objects[CLIENTES_NUNIVEL0]);
      String nbnivel00 = (String) objects[CLIENTES_NBNIVEL0];
      Integer nunivel11 = (Integer) (objects[CLIENTES_NUNIVEL1]);
      String nbnivel11 = (String) objects[CLIENTES_NBNIVEL1];
      Integer nunivel22 = (Integer) (objects[CLIENTES_NUNIVEL2]);
      String nbnivel22 = (String) objects[CLIENTES_NBNIVEL2];
      Integer nunivel33 = (Integer) (objects[CLIENTES_NUNIVEL3]);
      String nbnivel33 = (String) objects[CLIENTES_NBNIVEL3];
      Integer nunivel44 = (Integer) (objects[CLIENTES_NUNIVEL4]);
      String nbnivel44 = (String) objects[CLIENTES_NBNIVEL4];
      Integer cdgrupoaa = (Integer) (objects[CLIENTES_CDGRUPOA]);
      String nbgrupoaa = (String) objects[CLIENTES_NBGRUPOA];

      String cdcliente = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdcli);
      String nbdescli = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_40.value(), nbdesclien);
      String nuclient = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_6.value(), nucliente);
      String cdproduct = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_3.value(), cdproducto);
      String cdgrupoo = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdgrupo);
      String nbgrupoo = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_40.value(), nbgrupo);
      String cdclitra = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdclitraa);
      String cdclisal = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdclisall);
      String cdcliacm = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdcliacmm);
      String cdhppais = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_3.value(), cdhppaiss);
      String nunivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel00);
      String nbnivel0 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel00);
      String nunivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel11);
      String nbnivel1 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel11);
      String nunivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel22);
      String nbnivel2 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel22);
      String nunivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel33);
      String nbnivel3 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel33);
      String nunivel4 = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_5.value(), nunivel44);
      String nbnivel4 = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_25.value(), nbnivel44);
      String cdgrupoa = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_8.value(), cdgrupoaa);
      String nbgrupoa = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_40.value(), nbgrupoaa);

      final StringBuilder rowLine = new StringBuilder();

      rowLine.append(cdcliente).append(nbdescli).append(nuclient).append(cdproduct).append(cdgrupoo).append(nbgrupoo)
          .append(cdclitra).append(cdclisal).append(cdcliacm).append(cdhppais).append(nunivel0).append(nbnivel0)
          .append(nunivel1).append(nbnivel1).append(nunivel2).append(nbnivel2).append(nunivel3).append(nbnivel3)
          .append(nunivel4).append(nbnivel4).append(cdgrupoa).append(nbgrupoa);

      String cadena = String.format(DwhConstantes.PaddingFormats.PADDIND_SPACE_500.value(), rowLine);

      writeLine(outWriter, cadena);
    }
  }

}
