package sibbac.business.dwh.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.dwh.ficheros.tradersaccounters.db.query.MantenimientoTraSaleAccQuery;
import sibbac.business.dwh.ficheros.valores.bo.BaseBo;
import sibbac.business.dwh.ficheros.valores.bo.PaisBo;
import sibbac.business.dwh.ficheros.valores.dto.PaisDTO;
import sibbac.business.dwh.ficheros.valores.dto.Tmct0SalesTraderDTO;
import sibbac.common.CallableResultDTO;

/**
 * The Class SIBBACServiceParaisosFiscales.
 */
@RestController
@RequestMapping("/tradSaleAcc/mantenimiento")
public class SIBBACServiceMantenimientoTradSaleAcc extends BaseService {

  @Autowired
  private BaseBo<Tmct0SalesTraderDTO> salesTraderBo;

  /** The pais bo. */
  @Autowired
  private PaisBo paisBo;

  /**
   * Instantiates a new clientes PBC service.
   */
  public SIBBACServiceMantenimientoTradSaleAcc() {
    super("Mantenimiento Trader/Sale y Account Manager", "maintenanceTraSaleAcc",
        MantenimientoTraSaleAccQuery.class.getSimpleName());
  }

  /**
   * Execute action post.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody Tmct0SalesTraderDTO solicitud) {
    return salesTraderBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody Tmct0SalesTraderDTO solicitud) {
    return salesTraderBo.actionUpdate(solicitud);
  }

  /**
   * Execute action delete List.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/delete", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionDelete(@RequestBody List<String> solicitud) {
    return salesTraderBo.actionDelete(solicitud);
  }

  /**
   * Execute action get values CDISONUM, NBPAIS from TMCT0PAISES table.
   *
   * @return the callable result DTO
   */
  @RequestMapping(value = "/getPaises", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<PaisDTO> executeGetPaises() {
    return paisBo.getCdisonumAndNbpais();
  }

  /**
   * Execute action get true if country exists.
   *
   * @return Boolean
   */
  @RequestMapping(value = "/isValidCountry", method = RequestMethod.POST)
  public List<Object> executeIsValidCountry(@RequestBody String country) {
    return paisBo.isValidCountry(country);
  }
}
