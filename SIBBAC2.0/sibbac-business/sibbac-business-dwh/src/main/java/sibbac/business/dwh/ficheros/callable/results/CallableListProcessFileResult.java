package sibbac.business.dwh.ficheros.callable.results;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;

public interface CallableListProcessFileResult extends Callable<List<ProcessFileResultDTO>> {

  public void setFuture(Future<List<ProcessFileResultDTO>> future);

  public Future<List<ProcessFileResultDTO>> getFuture();

}