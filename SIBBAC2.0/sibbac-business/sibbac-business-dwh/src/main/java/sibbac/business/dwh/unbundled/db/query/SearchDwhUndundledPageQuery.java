package sibbac.business.dwh.unbundled.db.query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.DynamicColumn.ColumnType;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

@Component("SearchDwhUndundledPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SearchDwhUndundledPageQuery extends AbstractDynamicPageQuery {
  private static final String CLIENTE = "CLIENTE";
  private static final String FECHA_PAGO = "FECHA_PAGO";
  
  private static final String SELECT = "SELECT "
      + "NUM_OPERACION, CLIENTE, COMISION_PAGADA, FECHA_PAGO";
  private static final String FROM = "FROM TMCT0_DWH_UNBUNDLED";
  private static final String WHERE = "";
  private static final String GROUP_BY = "";
  
  
  private static final DynamicColumn[] COLUMNS = { 
	  new DynamicColumn("Fecha Pago", "FECHA_PAGO", ColumnType.DATE),
      new DynamicColumn("Cliente", "CLIENTE", ColumnType.STRING),
      new DynamicColumn("Comision Pagada", "COMISION_PAGADA")};
  
  
  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;
    res = new ArrayList<>();
    res.add(new StringDynamicFilter(CLIENTE, "Cliente"));
    DateDynamicFilter fechaPago = new DateDynamicFilter(FECHA_PAGO, "Fecha de saldo");
    res.add(fechaPago);
    return res;
  }
  public SearchDwhUndundledPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }
  @Override
  public String getSelect() {
    return SELECT;
  }
  @Override
  public String getFrom() {
    return FROM;
  }
  @Override
  public String getWhere() {
    return WHERE;
  }
  @Override
  public String getGroup() {
    return GROUP_BY;
  }
  @Override
  public String getHaving() {
    return "";
  }
  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }
  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }
  @Override
  public String getPostWhereConditions() {
    // TODO Auto-generated method stub
    return "";
  }
  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    // TODO Auto-generated method stub
    
  }
}