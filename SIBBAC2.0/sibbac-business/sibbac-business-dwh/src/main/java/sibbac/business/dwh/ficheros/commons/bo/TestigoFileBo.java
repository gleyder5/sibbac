package sibbac.business.dwh.ficheros.commons.bo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.common.SIBBACBusinessException;

@Component
public class TestigoFileBo {

  private static final Logger LOG = LoggerFactory.getLogger(TestigoFileBo.class);

  private static final String LINE_SEPARATOR = "\r\n";

  @Value("${file.dwh.path:/sbrmv/ficheros/dwh/out}")
  private String filePath;

  @Value("${dwh.ficheros.nacional.testigo.file:testigo.txt}")
  private String fileNameTestigo;

  /**
   * Genera el fichero de testigo con los resultados de la generacion de los
   * ficheros de los procesos
   * @param listProcessFileResultDTO
   * @throws SIBBACBusinessException
   */
  public File generateTestigoFile(List<ProcessFileResultDTO> listProcessFileResultDTO) {
    File testigoFile = null;
    GenerateFilesUtilDwh.createOutPutDir(filePath);

    BufferedWriter outWriter = null;
    FileWriter fileWriter = null;

    try {
      testigoFile = new File(getFullFileAccesPath());
      fileWriter = new FileWriter(testigoFile);
      outWriter = new BufferedWriter(fileWriter);

      for (ProcessFileResultDTO processFileResultDTO : listProcessFileResultDTO) {
        final String filename = processFileResultDTO.getFileNameProcess();
        final String statusFile = processFileResultDTO.getFileResult().toString();
        final String message = "";

        outWriter.write(String.format("Fichero:\t%s Estado:\t%s, Mensaje:\t%s", filename, statusFile, message));
        outWriter.write(LINE_SEPARATOR);
      }
    }
    catch (IOException e) {
      testigoFile = null;
    }
    finally {
      if (outWriter != null) {
        try {
          outWriter.close();
        }
        catch (IOException e) {
          LOG.error("No se puede cerrar el buffer de escritura", e);
        }
      }

      if (fileWriter != null) {
        try {
          fileWriter.close();
        }
        catch (IOException e) {
          LOG.error("No se puede cerrar el writer de escritura", e);
        }
      }
    }
    return testigoFile;
  }

  private String getFullFileAccesPath() {
    if (filePath.endsWith(File.separator)) {
      return filePath + this.fileNameTestigo;
    }
    else {
      return filePath + File.separator + this.fileNameTestigo;
    }
  }

}
