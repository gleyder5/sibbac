package sibbac.business.dwh.ficheros.nacional.db.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.nacional.db.model.Tmct0DwhEjecuciones;

public interface Tmct0DwhEjecucionesDao extends JpaRepository<Tmct0DwhEjecuciones, BigInteger> {
  /**
   * Query para obtener todos los clientes que han operado
   * @return List<Object[]>
   */

  @Query(value = "SELECT DISTINCT(CDCLIEST) FROM TMCT0_DWH_EJECUCIONES", nativeQuery = true)
  public List<BigDecimal> findAllClientesOperado();

  @Transactional
  @Modifying
  @Query(value = "TRUNCATE TABLE TMCT0_DWH_EJECUCIONES IMMEDIATE", nativeQuery = true)
  public void cleanEjecuciones();
}
