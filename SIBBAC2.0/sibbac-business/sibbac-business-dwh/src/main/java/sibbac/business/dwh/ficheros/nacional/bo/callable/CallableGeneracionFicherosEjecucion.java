package sibbac.business.dwh.ficheros.nacional.bo.callable;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.callable.results.CallableProcessFileResult;
import sibbac.business.dwh.ficheros.commons.dto.ProcessFileResultDTO;
import sibbac.business.dwh.ficheros.nacional.bo.GenerateEjecucionesNacionalFile;
import sibbac.business.dwh.ficheros.nacional.bo.GenerateOIInstitucionalesFile;
import sibbac.business.dwh.ficheros.nacional.bo.GenerateOIMinoristasFile;
import sibbac.business.dwh.ficheros.nacional.bo.GenerateOperacionesAutexFile;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGeneracionFicherosEjecucion implements CallableProcessFileResult {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGeneracionFicherosEjecucion.class);

  public enum TipoFicheroEjecuciones {
    NACIONAL, AUTEX, MINORISTA, INSTITUCIONAL;
  }

  private Future<ProcessFileResultDTO> future;

  private TipoFicheroEjecuciones tipoFichero;

  @Autowired
  private GenerateOperacionesAutexFile generateOperacionesAutexFile;

  @Autowired
  private GenerateEjecucionesNacionalFile generateEjecucionesNacionalFile;

  @Autowired
  private GenerateOIMinoristasFile generateOIMinoristasFile;

  @Autowired
  private GenerateOIInstitucionalesFile generateOIInstitucionalesFile;

  /**
   * Ejecuta el proceso
   */
  @Override
  public ProcessFileResultDTO call() throws Exception {
    final ProcessFileResultDTO processFileResultDTO = new ProcessFileResultDTO();

    try {
      switch (tipoFichero) {
        case NACIONAL:
          generateEjecucionesNacionalFile.generateFileEjecuciones();
          processFileResultDTO.setFileNameProcess(generateEjecucionesNacionalFile.getFileName());
          processFileResultDTO.setFileResult(generateEjecucionesNacionalFile.getFileGenerationResult());
          break;
        case AUTEX:
          generateOperacionesAutexFile.generateFile();
          processFileResultDTO.setFileNameProcess(generateOperacionesAutexFile.getFileName());
          processFileResultDTO.setFileResult(generateOperacionesAutexFile.getFileGenerationResult());
          break;
        case MINORISTA:
          generateOIMinoristasFile.generateFile();
          processFileResultDTO.setFileNameProcess(generateOIMinoristasFile.getFileName());
          processFileResultDTO.setFileResult(generateOIMinoristasFile.getFileGenerationResult());
          break;
        case INSTITUCIONAL:
          generateOIInstitucionalesFile.generateFile();
          processFileResultDTO.setFileNameProcess(generateOIInstitucionalesFile.getFileName());
          processFileResultDTO.setFileResult(generateOIInstitucionalesFile.getFileGenerationResult());
          break;
      }

    }
    catch (SIBBACBusinessException e) {
      LOG.error("Error durante la ejecución del proceso de generación del fichero nacional "
          + processFileResultDTO.getFileNameProcess(), e);
    }

    return processFileResultDTO;
  }

  public GenerateOperacionesAutexFile getGenerateOperacionesAutexFile() {
    return generateOperacionesAutexFile;
  }

  public void setGenerateOperacionesAutexFile(GenerateOperacionesAutexFile generateOperacionesAutexFile) {
    this.generateOperacionesAutexFile = generateOperacionesAutexFile;
  }

  public TipoFicheroEjecuciones getTipoFichero() {
    return tipoFichero;
  }

  public void setTipoFichero(TipoFicheroEjecuciones tipoFichero) {
    this.tipoFichero = tipoFichero;
  }

  public GenerateEjecucionesNacionalFile getGenerateEjecucionesNacionalFile() {
    return generateEjecucionesNacionalFile;
  }

  public void setGenerateEjecucionesNacionalFile(GenerateEjecucionesNacionalFile generateEjecucionesNacionalFile) {
    this.generateEjecucionesNacionalFile = generateEjecucionesNacionalFile;
  }

  public GenerateOIMinoristasFile getGenerateOIMinoristasFile() {
    return generateOIMinoristasFile;
  }

  public void setGenerateOIMinoristasFile(GenerateOIMinoristasFile generateOIMinoristasFile) {
    this.generateOIMinoristasFile = generateOIMinoristasFile;
  }

  public GenerateOIInstitucionalesFile getGenerateOIInstitucionalesFile() {
    return generateOIInstitucionalesFile;
  }

  public void setGenerateOIInstitucionalesFile(GenerateOIInstitucionalesFile generateOIInstitucionalesFile) {
    this.generateOIInstitucionalesFile = generateOIInstitucionalesFile;
  }

  @Override
  public Future<ProcessFileResultDTO> getFuture() {
    return future;
  }

  @Override
  public void setFuture(Future<ProcessFileResultDTO> future) {
    this.future = future;
  }
}
