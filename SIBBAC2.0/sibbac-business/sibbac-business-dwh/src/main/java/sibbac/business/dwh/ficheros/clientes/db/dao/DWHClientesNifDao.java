package sibbac.business.dwh.ficheros.clientes.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.dwh.ficheros.clientes.db.model.Tmct0DwhClientes;

@Repository
public interface DWHClientesNifDao extends JpaRepository<Tmct0DwhClientes, String> {

  /**
   * Query para DWHclientesNifDao, ORIGINAL
   */
  public static final String CLIENTES_NIF_QUERY = "SELECT CDCLIENT,NUCLIENT,NUDNICIF FROM TMCT0_DWH_CLIENTES WHERE "
      + "NUDNICIF IS NOT NULL GROUP BY CDCLIENT, NUCLIENT, NUDNICIF";

  /**
   * Query para DWHclientesNifDao con Paginación
   */
  public static final String CLIENTES_NIF_PAGE_QUERY = CLIENTES_NIF_QUERY + " LIMIT ?1, ?2";

  /**
   * Query para DWHclientesNifDao
   */
  public static final String CLIENTES_NIF_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + CLIENTES_NIF_QUERY + ")";

  /**
   * Obtiene los datos para la generación del fichero DWHclientesNifDao sin
   * paginación
   * @return
   */
  @Query(value = CLIENTES_NIF_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListClientesNif(Integer offset, Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero DWHclientesNifDao sin
   * paginación
   * @return
   */
  @Query(value = CLIENTES_NIF_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListClientesNif();
}
