package sibbac.business.dwh.ficheros.callable;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import sibbac.business.dwh.ficheros.valores.dto.PartenonFileLineDTO;

public interface CallableProcessInterface extends Callable<Boolean> {

  public void setListData(List<PartenonFileLineDTO> listData);

  public List<PartenonFileLineDTO> getListData();

  public Future<Boolean> getFuture();

  public void setFuture(Future<Boolean> future);
  
  public Boolean executeProcess();
  
  public void setProcessName(String name);
  
  public String getProcessName();

}
