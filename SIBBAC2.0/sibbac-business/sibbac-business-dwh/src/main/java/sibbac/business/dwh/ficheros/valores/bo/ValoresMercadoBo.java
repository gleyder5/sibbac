package sibbac.business.dwh.ficheros.valores.bo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.dwh.ficheros.valores.db.dao.ValoresMercadoDao;
import sibbac.business.dwh.ficheros.valores.db.model.ValoresMercado;
import sibbac.database.bo.AbstractBo;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ValoresMercadoBo extends AbstractBo<ValoresMercado, String, ValoresMercadoDao> {

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  private int commitInterval = 100;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void saveAll(List<ValoresMercado> entities) {
    dao.save(entities);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkValoresMercado(List<ValoresMercado> entities) {
    Integer resultCounter = 0;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (ValoresMercado mercadoEntity : entities) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          entityTransaction.commit();
          entityTransaction.begin();
          entityManager.clear();
        }

        entityManager.persist(mercadoEntity);
        resultCounter++;
      }

      entityTransaction.commit();
    }
    catch (RuntimeException e) {
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw e;
    }
    finally {
      entityManager.close();
    }

    return resultCounter;
  }

  public List<Object[]> findMercado(String cdemisio) {
    return dao.findMercado(cdemisio);
  }

}
