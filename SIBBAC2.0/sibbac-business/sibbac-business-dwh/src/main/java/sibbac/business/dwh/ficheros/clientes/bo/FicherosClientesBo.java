package sibbac.business.dwh.ficheros.clientes.bo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sibbac.business.dwh.ficheros.clientes.db.dao.Tmct0DwhClientesDao;
import sibbac.business.dwh.ficheros.clientes.db.model.Tmct0DwhClientes;
import sibbac.business.dwh.ficheros.nacional.db.dao.Tmct0DwhEjecucionesDao;
import sibbac.business.dwh.utils.DwhConstantes;
import sibbac.business.dwh.utils.GenerateFilesUtilDwh;
import sibbac.database.bo.AbstractBo;

@Component
public class FicherosClientesBo extends AbstractBo<Tmct0DwhClientes, Integer, Tmct0DwhClientesDao> {

  private static final Logger LOG = LoggerFactory.getLogger(FicherosClientesBo.class);

  @Autowired
  private Tmct0DwhClientesDao tmct0DwhClientesDao;

  @Autowired
  private Tmct0DwhEjecucionesDao tmct0DwhEjecucionesDao;

  /**
   * Método para validar clientes activos y actualizar las bajas
   */
  public void clientesAlta() {

    // obtenemos todos los clientes de TMCT0_DWH_CLIENTES que NO están a baja
    List<Object[]> clientes = tmct0DwhClientesDao.findClientesActivos();
    Integer cdClient = 0;
    Integer nuClient = 0;
    String cdAlias = "";
    String cdProduc = "";

    if (clientes != null) {
      // por cada cliente obtenido anteriormente comprobamos que está activo si
      // checkClienteActivo >0
      for (Object[] obj : clientes) {
        // obtenemos el nuClient cogiendo el primer campo de la consulta
        // findClientesActivos
        nuClient = (Integer) obj[0];
        // obtenemos el cdClient cogiendo el segundo campo de la consulta
        // findClientesActivos
        cdClient = (Integer) obj[1];
        // obtenemos el cdProduc cogiendo el tercer campo de la consulta
        // findClientesActivos
        cdProduc = (String) obj[2];
        // calculamos el alias con el cdClient
        cdAlias = calculateAlias(cdClient, cdProduc);
        String checkActivoString = tmct0DwhClientesDao.checkClienteActivo(nuClient, cdAlias);
        if (checkActivoString != null) {
          Date checkActivo = GenerateFilesUtilDwh.dateFromStringPattern(checkActivoString, "yyyyMMdd");
          if (checkActivo == null || checkActivo.after(new Date())) {
            // el cliente está de alta, no hacemos nada
            if (LOG.isDebugEnabled()) {
              LOG.debug("El cliente " + cdClient + " está activo");
            }
          }
          else {
            // el cliente está inactivo, se pone a baja con updateClienteBaja
            if (LOG.isDebugEnabled()) {
              LOG.debug("El cliente " + cdClient + " está inactivo");
            }
            tmct0DwhClientesDao.updateClienteBaja(cdClient);
          }
        }
        else {
          if (LOG.isDebugEnabled()) {
            LOG.debug("Registro no encontrado en la consulta");
          }
        }
      }
    }
    else {
      LOG.debug("No hay clientes de alta");
    }
  }

  /**
   * Método para validar los clientes que han operado
   */
  public void clientesOperado() {
    // obtenemos una lista con todos los clientes que han operado
    // de la tabla TMCT0_DWH_EJECUCIONES
    List<BigDecimal> clientesOperado = tmct0DwhEjecucionesDao.findAllClientesOperado();
    Integer cdClient = 0;
    String cdAlias = "";
    String cdProduc = "";
    Integer cdGrupoA = 0;

    if (clientesOperado != null) {
      // por cada cliente obtenido validamos si está en TMCT0_DWH_CLIENTES
      for (BigDecimal obj : clientesOperado) {
        // obtenemos el campo CDCLIENT del primer campo de la query anterior
        cdClient = obj.intValue();
        // obtenemos el cliente que ha operado en(si existe)
        List<Object[]> cliente = tmct0DwhClientesDao.checkClienteDWH(cdClient);
        // si el cliente ha operado y no existe en TMCT0_DWH_CLIENTES
        if (cliente.isEmpty()) {
          // insertamos un nuevo cliente
          try {
            // calculamos su nuevo grupo con la consulta
            cdGrupoA = tmct0DwhClientesDao.getCdGrupoA();
            if (cdGrupoA == null) {
              cdGrupoA = 1;
            }
            cdProduc = calculateCdProducNewClient(cdClient);
            cdAlias = calculateAlias(cdClient, cdProduc);
            List<Object[]> cli = tmct0DwhClientesDao.dwhDataToMap(cdAlias, cdProduc);
            if (!cli.isEmpty() && cdProduc.matches("008|012|014")) {
              insertDwhCliente(cli.get(0), cdGrupoA, cdClient);
            }
            else {
              LOG.error(
                  "La Query para recuperarar los datos a mapear para la tabla TMCT0_DWH_CLIENTES no ha devuelto valores o el CDPRODUC no es 008 o 012 o 014 con CDALIAS="
                      + cdAlias + " y CDPRODUC=" + cdProduc);
            }
          }
          catch (Exception e) {
            LOG.error(
                "Error al insertar nuevo registro en Tmct0DwhClientes debido a que uno de sus campos recuperados de la BD está nulo");
          }
        }
        // si ya existe se actualizará el registro en la tabla
        // TMCT0_DWH_CLIENTES
        else {
          try {
            Integer idCliente = (Integer) (cliente.get(0)[0]);
            cdProduc = (String) (cliente.get(0)[2]);
            cdGrupoA = (Integer) (cliente.get(0)[3]);
            String nbGrupoA = (String) (cliente.get(0)[4]);
            cdAlias = calculateAlias(cdClient, cdProduc);
            List<Object[]> cli = tmct0DwhClientesDao.dwhDataToMap(cdAlias, cdProduc);
            if (!cli.isEmpty()) {
              updateDwhCliente(cli.get(0), cdClient, idCliente, cdGrupoA, nbGrupoA);
            }
            else {
              LOG.error(
                  "La Query para recuperarar los datos a mapear para la tabla TMCT0_DWH_CLIENTES no ha devuelto valores con CDALIAS="
                      + cdAlias + " y CDPRODUC=" + cdProduc);
            }
          }
          catch (Exception e) {
            LOG.error(
                "Error al insertar nuevo registro en Tmct0DwhClientes debido a que uno de sus campos recuperados de la BD está nulo");
          }
        }
      }
    }
    else {
      LOG.debug("No hay ningún cliente que haya operado");
    }
  }

  /**
   * Método para calcular el cdAlias a partir del cdProduc y cdClient
   * @param cdClient cdProduct
   * @return String
   */
  public String calculateAlias(Integer cdClient, String cdProduc) {
    String alias = String.valueOf(cdClient);
    String strPattern = "^0+";
    cdProduc = cdProduc.replaceAll(strPattern, "");

    if (alias.startsWith(cdProduc)) {
      alias = alias.replaceFirst(cdProduc, "");
    }

    alias = String.valueOf(Integer.parseInt(alias));
    return alias;
  }

  public String calculateCdProducNewClient(Integer cdClient) {

    String cdClientS = String.valueOf(cdClient);
    String cdProduc = "";
    if (cdClientS.startsWith("8")) {
      cdProduc = "8";
    }
    else if (cdClientS.startsWith("12")) {
      cdProduc = "12";
    }
    else if (cdClientS.startsWith("14")) {
      cdProduc = "14";
    }
    cdProduc = String.format(DwhConstantes.PaddingFormats.PADDIND_ZERO_3.value(), Integer.parseInt(cdProduc));

    return cdProduc;
  }

  private static int CLIENTES_NBDESCLI = 0;
  private static int CLIENTES_CDALIAS = 1;
  private static int CLIENTES_NUCLIENT = 2;
  private static int CLIENTES_CDPRODUC = 3;
  private static int CLIENTES_CDGRUPOO = 4;
  private static int CLIENTES_NBGRUPOO = 5;
  private static int CLIENTES_CDLITRA = 6;
  private static int CLIENTES_NUCTATRA = 7;
  private static int CLIENTES_CDLISAL = 8;
  private static int CLIENTES_NUCTASAL = 9;
  private static int CLIENTES_CDCLIACM = 10;
  private static int CLIENTES_NUCTAACM = 11;
  private static int CLIENTES_CDHPPAIS = 12;
  private static int CLIENTES_NUNIVEL0 = 13;
  private static int CLIENTES_NBNIVEL0 = 14;
  private static int CLIENTES_NUNIVEL1 = 15;
  private static int CLIENTES_NBNIVEL1 = 16;
  private static int CLIENTES_NUNIVEL2 = 17;
  private static int CLIENTES_NBNIVEL2 = 18;
  private static int CLIENTES_NUNIVEL3 = 19;
  private static int CLIENTES_NBNIVEL3 = 20;
  private static int CLIENTES_NUNIVEL4 = 21;
  private static int CLIENTES_NBNIVEL4 = 22;
  private static int CLIENTES_NUDNICIF = 23;

  /**
   * Método para insertar un nuevo cliente
   * @param cli
   * @param cdGrupoA
   */
  public void insertDwhCliente(Object[] cli, Integer cdGrupoA, Integer cdClient) {
    Tmct0DwhClientes cliente = new Tmct0DwhClientes();
    mapObject(cli, cliente, cdClient);
    cliente.setCdGrupoA(cdGrupoA);
    cliente.setNbGrupoA(String.valueOf(cli[CLIENTES_NBDESCLI]));
    tmct0DwhClientesDao.save(cliente);

  }

  /**
   * Método para actualizar un cliente
   * @param cli
   */
  public void updateDwhCliente(Object[] cli, Integer cdClient, Integer idClient, Integer cdGrupoA, String nbGrupoA) {
    Tmct0DwhClientes cliente = new Tmct0DwhClientes();
    mapObject(cli, cliente, cdClient);
    cliente.setIdClient(idClient);
    cliente.setCdGrupoA(cdGrupoA);
    cliente.setNbGrupoA(nbGrupoA);
    tmct0DwhClientesDao.save(cliente);
  }

  /**
   * Método para mapeo de registros de la entidad Tmct0DwhClientes
   * @param cli
   * @return Tmct0DwhClientes
   */
  public static void mapObject(Object[] cli, Tmct0DwhClientes cliente, Integer cdClient) {

    cliente.setCdClient(cdClient);
    cliente.setNbDesCli(String.valueOf(cli[CLIENTES_NBDESCLI]));
    cliente.setNuClient(((BigDecimal) (cli[CLIENTES_NUCLIENT])).intValue());
    cliente.setCdProduct(String.valueOf(cli[CLIENTES_CDPRODUC]));
    if ((String.valueOf(cli[CLIENTES_CDGRUPOO])).trim().isEmpty()) {
      // cliente.setCdGrupo(Integer.parseInt(String.valueOf(cli[CLIENTES_CDPRODUC]).trim()));
      cliente.setCdGrupo(cdClient);
      cliente.setNbGrupo(String.valueOf(cli[CLIENTES_NBDESCLI]));
    }
    else {
      cliente.setCdGrupo(Integer.parseInt(String.valueOf(cli[CLIENTES_CDGRUPOO]).trim()));
      cliente.setNbGrupo(String.valueOf(cli[CLIENTES_NBGRUPOO]));
    }

    if ((String.valueOf(cli[CLIENTES_CDLITRA])).trim().isEmpty()) {
      cliente.setCdLitra(Integer.valueOf("000" + String.valueOf(cli[CLIENTES_NUCTATRA]).trim()));
    }
    else {
      cliente.setCdLitra(
          Integer.valueOf(String.valueOf(cli[CLIENTES_CDLITRA]) + String.valueOf(cli[CLIENTES_NUCTATRA]).trim()));
    }

    if ((String.valueOf(cli[CLIENTES_CDLISAL])).trim().isEmpty()) {
      cliente.setCdLisal(Integer.valueOf("000" + String.valueOf(cli[CLIENTES_NUCTASAL]).trim()));
    }
    else {
      cliente.setCdLisal(
          Integer.valueOf(String.valueOf(cli[CLIENTES_CDLISAL]) + String.valueOf(cli[CLIENTES_NUCTASAL]).trim()));
    }

    if ((String.valueOf(cli[CLIENTES_CDCLIACM])).trim().isEmpty()) {
      cliente.setCdcLiacm(Integer.valueOf("000" + String.valueOf(cli[CLIENTES_NUCTAACM]).trim()));
    }
    else {
      cliente.setCdcLiacm(
          Integer.valueOf(String.valueOf(cli[CLIENTES_CDCLIACM]) + String.valueOf(cli[CLIENTES_NUCTAACM]).trim()));
    }

    if ((String.valueOf(cli[CLIENTES_CDHPPAIS])).trim().isEmpty()) {
      cliente.setCdhpPais(String.valueOf(000));
    }
    else {
      cliente.setCdhpPais(String.valueOf(cli[CLIENTES_CDHPPAIS]));
    }
    cliente.setNuNivel0(Integer.valueOf(String.valueOf(cli[CLIENTES_NUNIVEL0]).trim()));
    cliente.setNbNivel0(String.valueOf(cli[CLIENTES_NBNIVEL0]));
    cliente.setNuNivel1(Integer.valueOf(String.valueOf(cli[CLIENTES_NUNIVEL1]).trim()));
    cliente.setNbNivel1(String.valueOf(cli[CLIENTES_NBNIVEL1]));
    cliente.setNuNivel2(Integer.valueOf(String.valueOf(cli[CLIENTES_NUNIVEL2]).trim()));
    cliente.setNbNivel2(String.valueOf(cli[CLIENTES_NBNIVEL2]));
    cliente.setNuNivel3(Integer.valueOf(String.valueOf(cli[CLIENTES_NUNIVEL3]).trim()));
    cliente.setNbNivel3(String.valueOf(cli[CLIENTES_NBNIVEL3]));
    cliente.setNuNivel4(Integer.valueOf(String.valueOf(cli[CLIENTES_NUNIVEL4]).trim()));
    cliente.setNbNivel4(String.valueOf(cli[CLIENTES_NBNIVEL4]));
    cliente.setNudNiCif(String.valueOf(cli[CLIENTES_NUDNICIF]));
    cliente.setFhAudit(new Timestamp(new Date().getTime()));
    cliente.setUsAudit("TaskGenDWGClientes");
  }

}
