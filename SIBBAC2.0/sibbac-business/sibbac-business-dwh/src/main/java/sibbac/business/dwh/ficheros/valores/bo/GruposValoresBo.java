package sibbac.business.dwh.ficheros.valores.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.dwh.ficheros.valores.db.dao.GruposValoresDao;
import sibbac.business.dwh.ficheros.valores.db.model.GruposValores;
import sibbac.business.dwh.ficheros.valores.dto.GruposValoresDTO;
import sibbac.database.bo.AbstractBo;

/**
 * The Class GruposValoresBo.
 */
@Service
public class GruposValoresBo extends AbstractBo<GruposValores, BigInteger, GruposValoresDao> {

  public List<GruposValoresDTO> getIdAndNbdescipcion() {
    List<Object[]> idAndnbdescipcion = dao.getIdAndNbdescipcion();

    List<GruposValoresDTO> auxList = new ArrayList<>();
    Iterator<Object[]> it = idAndnbdescipcion.iterator();

    while (it.hasNext()) {
      Object[] next = it.next();
      GruposValoresDTO aux = new GruposValoresDTO();
      aux.setId(new BigInteger(next[0].toString()));
      aux.setNbDescripcion(next[1].toString());
      auxList.add(aux);
    }

    return auxList;
  }

}
