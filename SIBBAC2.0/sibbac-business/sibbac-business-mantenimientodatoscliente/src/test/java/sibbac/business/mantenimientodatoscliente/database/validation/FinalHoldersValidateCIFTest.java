package sibbac.business.mantenimientodatoscliente.database.validation;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.Before;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;


public class FinalHoldersValidateCIFTest {
    
    private FinalHoldersValidateCIF validator;
    
    private List<String> resultado;
    
    @Before
    public void before() {
        resultado = new ArrayList<>();
        validator = new FinalHoldersValidateCIF();
        validator.setResult(resultado);
    }

    @Test
    public void testCalculaDigitoControlWikiEjemplo() {
        int verificador;
        
        validator.setNudnicif("A58818501");
        verificador = validator.calculaDigitoControl();
        assertEquals(1, verificador);
        assertTrue(resultado.isEmpty());
    }
    
    @Test
    public void testValidateWikiEjemplo() {
        validator.setNudnicif("A58818501");
        validator.validate();
        assertTrue(resultado.isEmpty());
    }
    
    @Test
    public void testCalculaDigitoControlNumericoCorrecto() {
        int verificador;
        
        validator.setNudnicif("B41647280");
        verificador = validator.calculaDigitoControl();
        assertEquals(0, verificador);
        assertTrue(resultado.isEmpty());
    }
    
    @Test
    public void testValidateVerificadorNumericoCorrecto() {
        validator.setNudnicif("B41647280");
        validator.validate();
        assertTrue(resultado.isEmpty());
    }
    
    
    @Test
    public void testValidateVerificadorNumericoIncorrecto() {
        validator.setNudnicif("B41647285");
        validator.validate();
        assertFalse(resultado.isEmpty());
        assertTrue(resultado.contains(ErrorFinalHolderEnum.ERROR_37.getError()));
    }
    
    @Test
    public void testValidateNoNumericos() {
        validator.setNudnicif("Ñ41647285");
        validator.validate();
        assertFalse(resultado.isEmpty());
        assertTrue(resultado.contains(ErrorFinalHolderEnum.ERROR_47.getError()));
    }

    @Test
    public void testValidateVerificadorLiteralCorrecto() {
        validator.setNudnicif("R0100016E");
        validator.validate();
        assertTrue(resultado.isEmpty());
    }
    
    @Test
    public void testValidateVerificadorLiteralIncorrecto() {
        validator.setNudnicif("R0100016J");
        validator.validate();
        assertFalse(resultado.isEmpty());
        assertTrue(resultado.contains(ErrorFinalHolderEnum.ERROR_37.getError()));
    }
    
    @Test
    public void testValidateVerificadorLiteralIncorrectoNumerico() {
        validator.setNudnicif("R01000164");
        validator.validate();
        assertFalse(resultado.isEmpty());
        assertTrue(resultado.contains(ErrorFinalHolderEnum.ERROR_37.getError()));
    }
}
