package sibbac.business.mantenimientodatoscliente.database.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CliMifid;

@Repository
public class Tmct0cliMifidDaoImp {
	@PersistenceContext
	private EntityManager em;

	public List<Tmct0CliMifid> findTmct0CliMifidFiltro(Long idCliente, String categoria, String testConveniencia,
			String testIdoneidad, Date fechaFinVigConv, Date fechaFinVigIdon, String categoriaNot, String testConvenienciaNot,
			String testIdoneidadNot) {

		Query query;
		List<Tmct0CliMifid> result;

		StringBuffer qlString = new StringBuffer("SELECT ");
		qlString.append(" mifid ");
		qlString.append(" FROM Tmct0CliMifid mifid ");
		qlString.append(" WHERE 1=1 ");
		if (idCliente != null) {
			qlString.append("AND mifid.cliente.idClient = :idCliente ");
		}
		if (categoria != null && !categoria.isEmpty()) {
			qlString.append(" AND mifid.category = :categoria ");
		}
		if (categoriaNot != null && !categoriaNot.isEmpty()) {
			qlString.append(" AND mifid.category <> :categoriaNot ");
		}
		if (testConveniencia != null && !testConveniencia.isEmpty()) {
			qlString.append(" AND mifid.tConven = :testConveniencia ");
		}
		if (testConvenienciaNot != null && !testConvenienciaNot.isEmpty()) {
			qlString.append(" AND mifid.tConven <> :testConvenienciaNot ");
		}

		if (testIdoneidad != null && !testIdoneidad.isEmpty()) {
			qlString.append(" AND mifid.tIdoneid = :testIdoneidad ");
		}
		if (testIdoneidadNot != null && !testIdoneidadNot.isEmpty()) {
			qlString.append(" AND mifid.tIdoneid <> :testIdoneidadNot ");
		}

		if (fechaFinVigConv != null) {
			qlString.append(" AND mifid.fhConv = :fechaFinVigConv ");
		}

		if (fechaFinVigIdon != null) {
			qlString.append(" AND mifid.fhIdon = :fechaFinVigIdon ");
		}

		try {
			query = em.createQuery(qlString.toString());
			if (idCliente != null) {
				query.setParameter("idCliente", idCliente);
			}

			if (categoria != null && !categoria.isEmpty()) {
				query.setParameter("categoria", categoria);
			}
			if (testConveniencia != null && !testConveniencia.isEmpty()) {
				query.setParameter("testConveniencia", testConveniencia);
			}
			if (testIdoneidad != null && !testIdoneidad.isEmpty()) {
				query.setParameter("testIdoneidad", testIdoneidad);
			}
			if (categoriaNot != null && !categoriaNot.isEmpty()) {
				query.setParameter("categoriaNot", categoriaNot);
			}
			if (testConvenienciaNot != null && !testConvenienciaNot.isEmpty()) {
				query.setParameter("testConvenienciaNot", testConvenienciaNot);
			}
			if (testIdoneidadNot != null && !testIdoneidadNot.isEmpty()) {
				query.setParameter("testIdoneidadNot", testIdoneidadNot);
			}

			if (fechaFinVigConv != null) {
				query.setParameter("fechaFinVigConv", fechaFinVigConv);
			}

			if (fechaFinVigIdon != null) {
				query.setParameter("fechaFinVigIdon", fechaFinVigIdon);
			}

			result = query.getResultList();
		} catch (PersistenceException ex) {
			result = null;
			// LOG.error( prefix + "ERROR(): {}", ex.getClass().getName(),
			// ex.getMessage() );

		}
		return result;

	}

}
