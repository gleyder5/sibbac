package sibbac.business.mantenimientodatoscliente.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CliMifid;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cliId;


@Repository
public interface Tmct0cliMifidDao extends JpaRepository< Tmct0CliMifid, Long > {

	@Query( "SELECT cli FROM Tmct0cli cli WHERE cdbrocli =:cdbrocli and fhfinal = :fhfinal" )
	public Tmct0cli findByCdbrocli( @Param( "cdbrocli" ) String cdbrocli, @Param( "fhfinal" ) int fhfinal );

}
