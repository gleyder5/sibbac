package sibbac.business.mantenimientodatoscliente.database.validation;

/**
 * Ayudar para validar string
 * @author miriam.gomez
 *
 */
public class ValidateString {
  

  private static final String AUD="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\'()*+,-.:;<=>/?[]\\^_`ÌÎÍ{}|¡ºª¿ÑÁÀÂÄÇÉËÈÍÌÎÏÓÖÒÔÚÜÛÙ£ ";
	
	public static boolean validateABC(String str){
		
		if (str == null || "".equals(str)) return false;		
				
		if ((str).matches("([A-Z]|\\s)+")){			
			return false;
		} else {
			return true;
		}	
	}
	
	public static boolean validateAlphanumericWithoutSpaces(String str){
		
		if (str == null || "".equals(str)) return false;		
		
		if ((str).matches("([A-Z]|[0-9])+")){			
			return false;
		} else {
			return true;
		}	
	}
	
	public static boolean validateAlphanumericWithSpaces(String str){
		
		if (str == null || "".equals(str)) return false;		
		
		if ((str).matches("([A-Z]|[0-9]|\\s)+")){			
			return false;
		} else {
			return true;
		}	
	}
	
public static boolean validateAUDWithSpaces(String str){
    
    if (str == null || "".equals(str)) return false;    
    
    for(int i=0; i<str.length(); i++){
      if (AUD.indexOf(str.charAt(i),0)==-1){
         return true;
      }
   }
    return false;
  }
	




}
