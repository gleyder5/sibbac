package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.database.bsnvasql.model.Tval0ps2;


/*
 * Validaciones sobre nacionalidad
 */
public class FinalHoldersValidateNationalityCode implements Validable {

	
	// NationalityCode value
	private String nationalitycode;
	public void setNationalityCode(String nationalitycode) {
		this.nationalitycode = nationalitycode;
	}
	
	// Errors accumulator
	private List<String> result;
	public void setResult(List<String> result) {
		this.result = result;
	}
			
			
	
	@Override
	public void validate() {

		
		
		
	}
	
	public void validateNationalityCode(Tval0ps2 tval0ps2) {

		char caracteres;
		boolean nationality=true;
		
		if (nationalitycode.length() != 3) {
			result.add(ErrorFinalHolderEnum.ERROR_24.getError());
		} else {
			for(int i=0; i<3;i++){
				caracteres = nationalitycode.charAt(i);
				if(Character.isLetter(caracteres)){
					nationality=false;
				}
			}
			
			if(nationality){
				//Compruebo el valor del codigo de nacionalidad
				if (tval0ps2 == null) {
					result.add(ErrorFinalHolderEnum.ERROR_25.getError());
				}
			}else{
				result.add(ErrorFinalHolderEnum.ERROR_24.getError());
			}
		}
		
		
		
	}

}
