package sibbac.business.mantenimientodatoscliente.tasks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0MifidTestDao;
import sibbac.business.mantenimientodatoscliente.database.model.FechaMifidTest;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class CargaMifidMultithread {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CargaMifidMultithread.class);

  private final static Integer AWAIT_TERMINATION_MINUTES = 120;
  private final static Integer AWAIT_TERMINATION_MINUTES_FILE = 120;
  private final static Integer MAX_THREAD_POOL_SIZE_FILE = 5;
  private final static Integer MAX_THREAD_POOL_SIZE = 10;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0MifidTestDao tmct0MifidTestDao;

  /**
   * 
   * @param codigo C0011 o C0013
   * @param persona F o J
   * @throws SIBBACBusinessException
   */
  @Transactional
  public Integer actualizarFechasConveniencia(String codigo, String persona) throws SIBBACBusinessException {

    LOG.debug("Inicio [actualizarFechasConveniencia para tipo persona " + persona + " ]");
    final List<FechaMifidTest> fechas;
    final String message;
    Integer actualizados = 0;

    try {

      fechas = tmct0MifidTestDao.ultimasFechasTestNew(codigo);

      if (fechas != null && !fechas.isEmpty()) {
        LOG.debug("[tmct0MifidTestDao.ultimasFechasTest para codigo: " + codigo + " devuelve " + fechas.size()
            + "resultados]");
        Iterator<FechaMifidTest> fechasTestIterator = fechas.iterator();

        CargaMifidConvenienciaCallable cargaMifidConvenienciaCallable;
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        tfb.setNameFormat("Thread-CargaMifidConvenienciaMulti-%d");

        ExecutorService executor = Executors.newFixedThreadPool(MAX_THREAD_POOL_SIZE, tfb.build());
        Future<Integer> future;
        List<Future<Integer>> listFutures = new ArrayList<Future<Integer>>();
        Map<Future<Integer>, CargaMifidConvenienciaCallable> mapCallables = new HashMap<Future<Integer>, CargaMifidConvenienciaCallable>();

        try {

          for (int executorsNumber = 0; executorsNumber < MAX_THREAD_POOL_SIZE; executorsNumber++) {
            cargaMifidConvenienciaCallable = ctx.getBean(CargaMifidConvenienciaCallable.class, fechasTestIterator,
                persona);
            future = executor.submit(cargaMifidConvenienciaCallable);
            listFutures.add(future);
            mapCallables.put(future, cargaMifidConvenienciaCallable);
          }

          executor.shutdown();
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();
        }
        finally {

          try {
            LOG.debug("[actualizarFechasConveniencia] - Esperamos " + AWAIT_TERMINATION_MINUTES + " minutos a que acaben los hilos");
            executor.awaitTermination(AWAIT_TERMINATION_MINUTES, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }

          for (Future<Integer> future2 : listFutures) {
            cargaMifidConvenienciaCallable = mapCallables.get(future2);

            if (cargaMifidConvenienciaCallable != null) {
              LOG.trace("[actualizarFechasConveniencia] revisando resultado hilo : {}", cargaMifidConvenienciaCallable);
            }
            try {
              actualizados += future2.get(AWAIT_TERMINATION_MINUTES, TimeUnit.MINUTES);
            }
            catch (InterruptedException | ExecutionException e) {
              LOG.warn("[actualizarFechasConveniencia] incidencia con  {} {}", cargaMifidConvenienciaCallable,
                  e.getMessage(), e);
            }
            catch (TimeoutException e) {
              if (!future2.isDone() && future2.cancel(true)) {
                LOG.warn("[actualizarFechasConveniencia] cancelando el future: {} : {}", future2,
                    cargaMifidConvenienciaCallable);
              }
              else {
                LOG.warn("[actualizarFechasConveniencia] incidencia con  {} {}", cargaMifidConvenienciaCallable,
                    e.getMessage(), e);
              }
            }
            catch (Exception e) {
              LOG.warn("[actualizarFechasConveniencia] incidencia con  {} {}", cargaMifidConvenienciaCallable,
                  e.getMessage(), e);
            }
          }

          if (!executor.isTerminated()) {
            executor.shutdownNow();
            try {
              executor.awaitTermination(AWAIT_TERMINATION_MINUTES, TimeUnit.MINUTES);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
            }
          }
          mapCallables.clear();
          listFutures.clear();

          LOG.debug("Se han actualizado " + actualizados);
        }
      }
    }

    catch (IllegalArgumentException e1) {
      LOG.debug(e1.getMessage(), e1);
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    catch (Exception rex) {
      message = String.format("Error inesperado al intentar actualizar fecha conveniencia con codigo %s", codigo);
      LOG.error("[actualizarFechasConveniencia] {}", message, rex);
      throw new SIBBACBusinessException(message, rex);
    }

    return actualizados;

  }

  /**
   * 
   * @param codigo C0012 o C0014
   * @param persona F o J
   * @throws SIBBACBusinessException
   */
  @Transactional
  public Integer actualizarFechasIdoneidad(String codigo, String persona) throws SIBBACBusinessException {

    LOG.debug("Inicio [actualizarFechasIdoneidad para tipo persona " + persona + " ]");
    final List<FechaMifidTest> fechas;
    final String message;
    Integer actualizados = 0;

    try {

      fechas = tmct0MifidTestDao.ultimasFechasTestNew(codigo);

      if (fechas != null && !fechas.isEmpty()) {
        LOG.debug("[tmct0MifidTestDao.ultimasFechasTest para codigo: " + codigo + " devuelve " + fechas.size()
            + "resultados]");
        Iterator<FechaMifidTest> fechasTestIterator = fechas.iterator();

        CargaMifidConvenienciaCallable cargaMifidConvenienciaCallable;
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        tfb.setNameFormat("Thread-CargaMifidConvenienciaMulti-%d");

        ExecutorService executor = Executors.newFixedThreadPool(MAX_THREAD_POOL_SIZE, tfb.build());
        Future<Integer> future;
        List<Future<Integer>> listFutures = new ArrayList<Future<Integer>>();
        Map<Future<Integer>, CargaMifidConvenienciaCallable> mapCallables = new HashMap<Future<Integer>, CargaMifidConvenienciaCallable>();

        try {

          for (int executorsNumber = 0; executorsNumber < MAX_THREAD_POOL_SIZE; executorsNumber++) {
            cargaMifidConvenienciaCallable = ctx.getBean(CargaMifidConvenienciaCallable.class, fechasTestIterator,
                persona);
            future = executor.submit(cargaMifidConvenienciaCallable);
            listFutures.add(future);
            mapCallables.put(future, cargaMifidConvenienciaCallable);
          }

          executor.shutdown();
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();
        }
        finally {

          try {
            LOG.debug("[actualizarFechasIdoneidad] - Esperamos " + AWAIT_TERMINATION_MINUTES + " minutos a que acaben los hilos");
            executor.awaitTermination(AWAIT_TERMINATION_MINUTES, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }

          for (Future<Integer> future2 : listFutures) {
            cargaMifidConvenienciaCallable = mapCallables.get(future2);

            if (cargaMifidConvenienciaCallable != null) {
              LOG.trace("[actualizarFechasConveniencia] revisando resultado hilo : {}", cargaMifidConvenienciaCallable);
            }
            try {
              actualizados += future2.get(AWAIT_TERMINATION_MINUTES, TimeUnit.MINUTES);
            }
            catch (InterruptedException | ExecutionException e) {
              LOG.warn("[actualizarFechasConveniencia] incidencia con  {} {}", cargaMifidConvenienciaCallable,
                  e.getMessage(), e);
            }
            catch (TimeoutException e) {
              if (!future2.isDone() && future2.cancel(true)) {
                LOG.warn("[actualizarFechasConveniencia] cancelando el future: {} : {}", future2,
                    cargaMifidConvenienciaCallable);
              }
              else {
                LOG.warn("[actualizarFechasConveniencia] incidencia con  {} {}", cargaMifidConvenienciaCallable,
                    e.getMessage(), e);
              }
            }
            catch (Exception e) {
              LOG.warn("[actualizarFechasConveniencia] incidencia con  {} {}", cargaMifidConvenienciaCallable,
                  e.getMessage(), e);
            }
          }

          if (!executor.isTerminated()) {
            executor.shutdownNow();
            try {
              executor.awaitTermination(AWAIT_TERMINATION_MINUTES, TimeUnit.MINUTES);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
            }
          }
          mapCallables.clear();
          listFutures.clear();

          LOG.debug("Se han actualizado " + actualizados);
        }
      }
    }

    catch (IllegalArgumentException e1) {
      LOG.debug(e1.getMessage(), e1);
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    catch (Exception rex) {
      message = String.format("Error inesperado al intentar actualizar fecha conveniencia con codigo %s", codigo);
      LOG.error("[actualizarFechasConveniencia] {}", message, rex);
      throw new SIBBACBusinessException(message, rex);
    }

    return actualizados;

  }

  @Transactional
  public int cargaFicheroMultithread(File file) {
    Integer actualizados = 0;
    try (InputStream is = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(is, StandardCharsets.ISO_8859_1);) {
      LineIterator it = new LineIterator(isr);

      LOG.debug("Inicio [cargaFicheroMultithread " + file.getName() + "]");

      try {

        CargaFileMifidTestCallable cargaFileMifidCallable;
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        tfb.setNameFormat("Thread-CargaFileMifidTestCallable-%d");

        ExecutorService executor = Executors.newFixedThreadPool(MAX_THREAD_POOL_SIZE_FILE, tfb.build());

        Future<Integer> future;
        List<Future<Integer>> listFutures = new ArrayList<Future<Integer>>();
        Map<Future<Integer>, CargaFileMifidTestCallable> mapCallables = new HashMap<Future<Integer>, CargaFileMifidTestCallable>();

        try {

          for (int executorsNumber = 0; executorsNumber < MAX_THREAD_POOL_SIZE_FILE; executorsNumber++) {
            cargaFileMifidCallable = ctx.getBean(CargaFileMifidTestCallable.class, it);
            future = executor.submit(cargaFileMifidCallable);
            listFutures.add(future);
            mapCallables.put(future, cargaFileMifidCallable);
          }

          executor.shutdown();
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();
        }
        finally {

          try {
            LOG.debug("[cargaFicheroMultithread] - Esperamos " + AWAIT_TERMINATION_MINUTES + " minutos a que acaben los hilos");
            executor.awaitTermination(AWAIT_TERMINATION_MINUTES_FILE, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }

          for (Future<Integer> future2 : listFutures) {
            cargaFileMifidCallable = mapCallables.get(future2);

            if (cargaFileMifidCallable != null) {
              LOG.trace("[cargaFicheroMultithread] revisando resultado hilo : {}", cargaFileMifidCallable);
            }
            try {
              actualizados += future2.get(AWAIT_TERMINATION_MINUTES_FILE, TimeUnit.MINUTES);
            }
            catch (InterruptedException | ExecutionException e) {
              LOG.warn("[cargaFicheroMultithread] incidencia con  {} {}", cargaFileMifidCallable, e.getMessage(), e);
            }
            catch (TimeoutException e) {
              if (!future2.isDone() && future2.cancel(true)) {
                LOG.warn("[cargaFicheroMultithread] cancelando el future: {} : {}", future2, cargaFileMifidCallable);
              }
              else {
                LOG.warn("[cargaFicheroMultithread] incidencia con  {} {}", cargaFileMifidCallable, e.getMessage(), e);
              }
            }
            catch (Exception e) {
              LOG.warn("[cargaFicheroMultithread] incidencia con  {} {}", cargaFileMifidCallable, e.getMessage(), e);
            }
          }

          if (!executor.isTerminated()) {
            executor.shutdownNow();
            try {
              executor.awaitTermination(AWAIT_TERMINATION_MINUTES_FILE, TimeUnit.MINUTES);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
            }
          }
          mapCallables.clear();
          listFutures.clear();

          LOG.debug("Se han cargado en TMCT0_MIFIDTEST " + actualizados + " registros ");
        }

      }
      finally {
        LineIterator.closeQuietly(it);
      }
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }

    LOG.debug("finish!");
    return actualizados;

  }

  public void borrarTestsMifid() {
    try {
      tmct0MifidTestDao.deleteAll();
    }
    catch (Exception ex) {
      LOG.debug("Error while trying to delete all records from TMCT0_MIFIDTEST", ex);
    }
  }
}
