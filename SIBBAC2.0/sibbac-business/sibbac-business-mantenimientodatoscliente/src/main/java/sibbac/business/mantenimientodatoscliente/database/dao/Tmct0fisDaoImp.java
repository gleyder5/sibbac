package sibbac.business.mantenimientodatoscliente.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.mantenimientodatoscliente.database.common.Utilidades;
import sibbac.business.mantenimientodatoscliente.database.dto.FinalHolderDTO;

/*
 * the class Tmct0fisDaoImp
 */
@Repository
public class Tmct0fisDaoImp {

  /** The Constant LOG. */ 
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0fisDaoImp.class);

  @PersistenceContext
  private EntityManager em;

  /**
   * Devuelve el listado de titulares acorde a los filtros introducidos por el usuario
   * 
   * @param sTipoPersona
   * @param tipoTitularVector
   * @param tipoDocumentoVector
   * @param sDocumento
   * @param sNombre
   * @param sPrimerApellido
   * @param sSegundoApellido
   * @param sTipoNacionalidad
   * @param nacionalidadVector
   * @param sTipoDireccion
   * @param sDireccion
   * @param sCiudad
   * @param sProvincia
   * @param sCodigoPostal
   * @param paisResidenciaVector
   * @return
   */
  @SuppressWarnings("unchecked")
  public List<FinalHolderDTO> findFinalHoldersFiltro(String sTipoPersona,
                                                     List<String> tipoTitularVector,
                                                     List<String> tipoDocumentoVector,
                                                     String sDocumento,
                                                     String sNombre,
                                                     String sPrimerApellido,
                                                     String sSegundoApellido,
                                                     String sTipoNacionalidad,
                                                     List<String> nacionalidadVector,
                                                     String sTipoDireccion,
                                                     String sDireccion,
                                                     String sCiudad,
                                                     String sProvincia,
                                                     String sCodigoPostal,
                                                     List<String> paisResidenciaVector) {

    List<FinalHolderDTO> listaFinalHolders = new ArrayList<FinalHolderDTO>();
    Map<String, Object> parameters = new HashMap<String, Object>();

    try {

      LOG.debug("findFinalHoldersFiltro - Entro en el metodo");

      String sFechaActual = Utilidades.getCadenaFechaActual("YYYYMMdd");

      // Se forma la consulta.
      final StringBuilder select = new StringBuilder(
                                                     "select  fh.cddclave, fh.tpsocied,  fh.tptiprep, fh.tpidenti, fh.cdholder, fh.nbclient, fh.nbclien1, fh.nbclien2, fh.tpnactit, fh.cdnactit,  pn.NBPAIS as nacionalidad,  ");
      select.append(" fh.TPDOMICI, fh.NBDOMICI, fh.nudomici, fh.NBCIUDAD, fh.NBPROVIN, fh.CDPOSTAL, fh.CDDEPAIS, pr.NBPAIS as paisResidencia, fh.fecha_nacimiento, fh.TIPO_ID_MIFID, fh.ID_MIFID, fh.IND_CLIENTE_SAN, fh.NUM_PERSONA_SAN ");
      final StringBuilder from = new StringBuilder(" from tmct0fis fh ,    tmct0paises pn,     tmct0paises pr  ");
      final StringBuilder where = new StringBuilder(" WHERE fh.FHFINAL >= ").append(sFechaActual)
                                                                            .append(" and pn.CDISONUM = fh.CDNACTIT    AND pn.CDESTADO = 'A'    AND pr.CDHACIENDA = fh.CDDEPAIS     AND pr.CDESTADO = 'A' ");

      // Filtro por Tipo de persona
      if (sTipoPersona != null && !sTipoPersona.equals("")) {
        LOG.debug("Parametro recibido: Tipo Persona -  " + sTipoPersona);
        if (sTipoPersona.startsWith("#")) {
          where.append(" AND fh.tpsocied != :tipopersona ");
          sTipoPersona = sTipoPersona.replaceFirst("#", "");
        } else {
          where.append(" AND fh.tpsocied = :tipopersona ");
        }
        parameters.put("tipopersona", sTipoPersona);

      }

      // Filtro por los valores de tipos titulares, incluidos o no incluidos.
      if (tipoTitularVector != null && tipoTitularVector.size() > 0) {
        LOG.debug("Parametro recibido: Tipos titulares -  " + tipoTitularVector);
        String primero = tipoTitularVector.get(0);
        if (primero.indexOf("#") != -1) {
          tipoTitularVector.set(0, primero.replace("#", ""));
          where.append(" and fh.tptiprep not in (:tipotitularVector) ");
        } else
          where.append(" and fh.tptiprep in (:tipotitularVector) ");

        parameters.put("tipotitularVector", tipoTitularVector);
      }

      // Filtro por los valores de tipos titulares, incluidos o no incluidos.
      if (tipoDocumentoVector != null && tipoDocumentoVector.size() > 0) {
        LOG.debug("Parametro recibido: Tipos documentos -  " + tipoDocumentoVector);
        String primero = tipoDocumentoVector.get(0);
        if (primero.indexOf("#") != -1) {
          tipoDocumentoVector.set(0, primero.replace("#", ""));
          where.append(" and fh.tpidenti not in (:tipodocumentoVector) ");
        } else
          where.append(" and fh.tpidenti in (:tipodocumentoVector) ");

        parameters.put("tipodocumentoVector", tipoDocumentoVector);
      }

      // Filtro por Valor documento igual o distinto
      if (sDocumento != null && !sDocumento.equals("")) {
        LOG.debug("Parametro recibido: Documento -  " + sDocumento);
        if (sDocumento.startsWith("#")) {
          where.append(" AND fh.cdholder != :documento ");
          sDocumento = sDocumento.replaceFirst("#", "");
        } else {
          where.append(" AND fh.cdholder = :documento ");
        }
        parameters.put("documento", sDocumento);
      }

      // Filtro por Valor nombre igual o distinto
      if (sNombre != null && !sNombre.equals("")) {
        LOG.debug("Parametro recibido: Nombre -  " + sNombre);
        if (sNombre.startsWith("#")) {
          where.append(" AND fh.nbclient != :nombre ");
          sNombre = sNombre.replaceFirst("#", "");
        } else {
          where.append(" AND fh.nbclient = :nombre ");
        }
        parameters.put("nombre", sNombre);
      }

      // Filtro por Valor Primer Apellido igual o distinto
      if (sPrimerApellido != null && !sPrimerApellido.equals("")) {
        LOG.debug("Parametro recibido: Primer Apellido -  " + sPrimerApellido);
        if (sPrimerApellido.startsWith("#")) {
          where.append(" AND fh.nbclien1 != :primerapellido ");
          sPrimerApellido = sPrimerApellido.replaceFirst("#", "");
        } else {
          where.append(" AND fh.nbclien1 = :primerapellido ");
        }
        parameters.put("primerapellido", sPrimerApellido);
      }

      // Filtro por Valor Segundo Apellido igual o distinto
      if (!sSegundoApellido.equals("") && sSegundoApellido != null) {
        LOG.debug("Parametro recibido: Segundo Apellido -  " + sSegundoApellido);
        if (sSegundoApellido.startsWith("#")) {
          where.append(" AND fh.nbclien2 != :segundoapellido ");
          sSegundoApellido = sSegundoApellido.replaceFirst("#", "");
        } else {
          where.append(" AND fh.nbclien2 = :segundoapellido ");
        }
        parameters.put("segundoapellido", sSegundoApellido);
      }

      // Filtro por Tipo de nacionalidad
      if (sTipoNacionalidad != null && !sTipoNacionalidad.equals("")) {
        LOG.debug("Parametro recibido: Tipo Nacionalidad -  " + sTipoNacionalidad);
        if (sTipoNacionalidad.startsWith("#")) {
          where.append(" AND fh.tpnactit != :tiponacionalidad ");
          sTipoNacionalidad = sTipoNacionalidad.replaceFirst("#", "");
        } else {
          where.append(" AND fh.tpnactit = :tiponacionalidad ");
        }
        parameters.put("tiponacionalidad", sTipoNacionalidad);
      }

      // Filtro por los valores de nacionalidad, incluidos o no incluidos.
      if (nacionalidadVector != null && nacionalidadVector.size() > 0) {
        LOG.debug("Parametro recibido: Codigos Nacionalidades -  " + nacionalidadVector);
        String primero = nacionalidadVector.get(0);
        if (primero.indexOf("#") != -1) {
          nacionalidadVector.set(0, primero.replace("#", ""));
          where.append(" and fh.cdnactit not in (:nacionalidadVector) ");
        } else
          where.append(" and fh.cdnactit in (:nacionalidadVector) ");

        parameters.put("nacionalidadVector", nacionalidadVector);
      }

      // Filtro por campos de DIRECCION
      // Tipo direccion
      if (sTipoDireccion != null && !sTipoDireccion.equals("")) {
        LOG.debug("Parametro recibido: Tipo Direccion -  " + sTipoDireccion);
        if (sTipoDireccion.startsWith("#")) {
          where.append(" AND fh.tpdomici != :tipodireccion ");
          sTipoDireccion = sTipoDireccion.replaceFirst("#", "");
        } else {
          where.append(" AND fh.tpdomici = :tipodireccion");
        }
        parameters.put("tipodireccion", sTipoDireccion);
      }

      // Direccion
      if (sDireccion != null && !sDireccion.equals("")) {
        LOG.debug("Parametro recibido: Direccion -  " + sDireccion);
        if (sDireccion.startsWith("#")) {
          where.append(" AND fh.nbdomici != :direccion ");
          sDireccion = sDireccion.replaceFirst("#", "");
        } else {
          where.append(" AND fh.nbdomici = :direccion");
        }
        parameters.put("direccion", sDireccion);
      }

      // CIUDAD
      if (sCiudad != null && !sCiudad.equals("")) {
        LOG.debug("Parametro recibido: Ciudad -  " + sCiudad);
        if (sCiudad.startsWith("#")) {
          where.append(" AND fh.NBCIUDAD != :ciudad ");
          sCiudad = sCiudad.replaceFirst("#", "");
        } else {
          where.append(" AND fh.NBCIUDAD = :ciudad");
        }
        parameters.put("ciudad", sCiudad);
      }

      // Provincia
      if (sProvincia != null && !sProvincia.equals("")) {
        LOG.debug("Parametro recibido: Provincia -  " + sProvincia);
        if (sProvincia.startsWith("#")) {
          where.append(" AND fh.NBPROVIN != :provincia ");
          sProvincia = sProvincia.replaceFirst("#", "");
        } else {
          where.append(" AND fh.NBPROVIN = :provincia");
        }
        parameters.put("provincia", sProvincia);
      }

      // Codigo postal
      if (sCodigoPostal != null && !sCodigoPostal.equals("")) {
        LOG.debug("Parametro recibido: Codigo Postal -  " + sCodigoPostal);
        if (sCodigoPostal.startsWith("#")) {
          where.append(" AND fh.CDPOSTAL != :codigopostal ");
          sCodigoPostal = sCodigoPostal.replaceFirst("#", "");
        } else {
          where.append(" AND fh.CDPOSTAL = :codigopostal");
        }
        parameters.put("codigopostal", sCodigoPostal);
      }

      // Filtro por los valores de pais de residencia, incluidos o no incluidos.
      if (paisResidenciaVector != null && paisResidenciaVector.size() > 0) {
        LOG.debug("Parametro recibido: Codigos Paises Residencia -  " + paisResidenciaVector);
        String primero = paisResidenciaVector.get(0);
        if (primero.indexOf("#") != -1) {
          paisResidenciaVector.set(0, primero.replace("#", ""));
          where.append(" and fh.cddepais not in (:paisResidenciaVector) ");
        } else
          where.append(" and fh.cddepais in (:paisResidenciaVector) ");

        parameters.put("paisResidenciaVector", paisResidenciaVector);
      }

      // where.append("order by cdalias,nbooking");
      String sQuery = select.append(from.append(where)).toString();
      Query query = null;

      LOG.debug("Creamos y ejecutamos la query: " + sQuery);

      query = em.createNativeQuery(sQuery);

      for (Entry<String, Object> entry : parameters.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }

      FinalHolderDTO fisDto = null;
      List<Object> resultList = null;

      resultList = query.getResultList();

      LOG.debug("Query ejecutada, obtenidos " + resultList.size() + " registros ");

      if (CollectionUtils.isNotEmpty(resultList)) {
        for (Object obj : resultList) {

          fisDto = FinalHolderDTO.convertObjectToClienteDTO((Object[]) obj);
          listaFinalHolders.add(fisDto);
        }
      }

    } catch (Exception e) {
      LOG.error("findFinalHoldersFiltroError no controlado en el metodo ", e);
      throw e;
    }

    LOG.debug("findFinalHoldersFiltro - Salgo del metodo - Todo ok");
    return listaFinalHolders;

  }

}
