package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;

/*
 * validaciones sobre el tipo de persona, titular, nacionalidad
 */
public class FinalHoldersValidateTypeSociety implements Validable {

  public static String FISICA = FinalHolderValidator.FISICA;
  public static String NACIONAL = FinalHolderValidator.NACIONAL;
  public static String EXTRANJERO = FinalHolderValidator.EXTRANJERO;
  public static String JURIDICA = FinalHolderValidator.JURIDICA;

  public static String NIF = FinalHolderValidator.NIF;
  public static String NIE = FinalHolderValidator.NIE;
  public static String CIF = FinalHolderValidator.CIF;
  public static String BIC = FinalHolderValidator.BIC;
  public static String OTROS = FinalHolderValidator.OTROS;

  public static String ESPCODE = FinalHolderValidator.ESPCODE;

  public static String REPRESENTANTE = FinalHolderValidator.REPRESENTANTE;

  // Type Identity value
  private String tpidenti;

  public void setTpIdenti(String tpidenti) {
    this.tpidenti = tpidenti;
  }

  // Type Society value
  private String tpsocied;

  public void setTpSociedad(String tpsocied) {
    this.tpsocied = tpsocied;
  }

  // Type Nationality value
  private String tpnactit;

  public void setTypeNationality(String tpnactit) {
    this.tpnactit = tpnactit;
  }

  // CIF value
  private String nudnicif;

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  // Errors accumulator
  private List<String> result;

  public void setResult(List<String> result) {
    this.result = result;
  }

  @Override
  public void validate() {

    // Si la sociedad es Fisica debe ser NIF, NIE, Otros.
    if (tpsocied.equals(FISICA)) {
      if (!tpidenti.equals(NIE) && !tpidenti.equals(OTROS) && !tpidenti.equals(NIF)) {
        // Type Society error: Must be NIF, NIE or Others
        result.add(ErrorFinalHolderEnum.ERROR_62.getError());
      }

    }

    // Si la sociedad es Juridica debe ser CIF, BIC, Otros.
    if (tpsocied.equals(JURIDICA)) {
      if (!tpidenti.equals(CIF) && !tpidenti.equals(BIC) && !tpidenti.equals(OTROS)) {
        // Type Society error: Must be CIF, BIC or Others
        result.add(ErrorFinalHolderEnum.ERROR_63.getError());
      }

    }

    // Si la sociedad es Fisica y Type Nationality es Nacional campo CIF/NIF No
    // puede ser Tipo M
    if (tpsocied.equals(FISICA) && tpnactit.equals(NACIONAL)) {
      if (nudnicif != null && !"".equals(nudnicif)) {
        if (nudnicif.startsWith("M")) {
          // Type Society error: The CIF/NIF can not start with the letter M
          result.add(ErrorFinalHolderEnum.ERROR_65.getError());
        }
      }
    }

    // Si la sociedad es Fisica y Type Nationality es Extranjero campo CIF/NIF
    // solo puede ser Tipo M
    if (tpsocied.equals(FISICA) && tpnactit.equals(EXTRANJERO)) {
      if (nudnicif != null && !"".equals(nudnicif)) {
        if (!nudnicif.substring(0, 1).equals("M") && NIF.equals(tpidenti)) {
          // Type Society error: The CIF/NIF can only start with the letter M
          result.add(ErrorFinalHolderEnum.ERROR_67.getError());
        }
      }
    }

    // Si la sociedad es Juridica y Tipo de Nacionalidad es Nacional campo
    // CIF/NIF no puede tener N o W
    if (tpsocied.equals(JURIDICA) && tpnactit.equals(NACIONAL)) {
      if (nudnicif != null && !"".equals(nudnicif)) {
        if (nudnicif.startsWith("N") || nudnicif.startsWith("W")) {
          // Type Society error: The CIF/NIF can not start with the letter N or W
          result.add(ErrorFinalHolderEnum.ERROR_66.getError());
        }
      }
    }

    // Si la sociedad es Juridica y Tipo de Nacionalidad es Extranjero campo
    // CIF/NIF solo puede tener N o W
    if (tpsocied.equals(JURIDICA) && tpnactit.equals(EXTRANJERO)) {
      if (nudnicif != null && !"".equals(nudnicif)) {
        if (!nudnicif.startsWith("N") && !nudnicif.startsWith("W") && CIF.equals(tpidenti)) {
          // Type Society error: The CIF/NIF can only start with the letter N or W
          result.add(ErrorFinalHolderEnum.ERROR_68.getError());
        }
      }
    }

  }

}
