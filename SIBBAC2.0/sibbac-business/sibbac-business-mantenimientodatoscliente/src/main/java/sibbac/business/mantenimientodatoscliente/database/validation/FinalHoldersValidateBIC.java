package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0swi;

/*
 * Validaciones sobre el tipo de documento BIC
 */
public class FinalHoldersValidateBIC implements Validable {

	// BIC value
		private String cdddebic;
		public void setBic(String Cdddebic) {
			this.cdddebic = Cdddebic;
		}
		
		// Errors accumulator
		private List<String> result;
		public void setResult(List<String> result) {
			this.result = result;
		}
		
		
	
	@Override
	public void validate() {
		
		
		
	}
	
	public void validateBIC(Tmct0paises pais, Tmct0swi tmct0swi) {
		
		// Validate length field
		if (this.cdddebic.trim().length() != 11) {
			// BIC format error: a BIC must have 11 position
			this.result.add(ErrorFinalHolderEnum.ERROR_42.getError());
			
		} else {
			
			if (ValidateString.validateAlphanumericWithoutSpaces(this.cdddebic)) {
				// BIC field error: only caracters from [A-Z] are admited (without spaces)
				//TODO: Miriam no se si es este error o no...
				result.add(ErrorFinalHolderEnum.ERROR_47.getError());
				result.add(ErrorFinalHolderEnum.ERROR_38.getError());
			} else {
				
				Boolean controlBic = false;
				char letra;
				//Compruebo que los primeros 6 caracteres sean letras *con un for
				for(int i=0; i<6;i++){
					letra = this.cdddebic.charAt(i);
					if(Character.isLetter(letra)){
						controlBic=true;
					}else
					{
						controlBic=false;
						break;
					}
				}
				
				//Dependiendo del valor de ControlBic agrego el tipo de ERROR
				if(!controlBic){
					// BIC format error: The first 6 characters must be letters
					this.result.add(ErrorFinalHolderEnum.ERROR_39.getError());
				}
				
				
				
				if(controlBic){
					//Busco las posiciones del codigo del pais que correspondan con la codificacion ISO 3166
					if (pais == null) {
						// 'Bic Code (ISO)' positions 5-6 not match any country code (ISO 3166-1)
						result.add(ErrorFinalHolderEnum.ERROR_40.getError());
					}
				}
				
				if ( tmct0swi == null) {					
					// BIC inexistente en la lista de c�digos BIC de SWIFT
					result.add(ErrorFinalHolderEnum.ERROR_51.getError());
				} 
				
			}
						
		}
		
	}

}
