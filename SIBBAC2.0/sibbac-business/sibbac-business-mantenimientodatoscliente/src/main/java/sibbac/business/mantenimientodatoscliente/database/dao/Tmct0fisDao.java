package sibbac.business.mantenimientodatoscliente.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fisId;

@Repository
public interface Tmct0fisDao extends JpaRepository<Tmct0fis, Tmct0fisId> {

  @Query("SELECT o FROM Tmct0fis o WHERE o.id.cddclave =:cddclave AND o.id.numsec=:numsec ")
  public Tmct0fis getFisById(@Param("cddclave") String cddclave, @Param("numsec") BigDecimal numsec);

  public List<Tmct0fis> findBynbciudad(String nCiudad);

  @Query("SELECT DISTINCT nbciudad from Tmct0fis")
  public List<String> findCiudades();

  public Tmct0fis findBycdholder(String cdholder);

  @Query("SELECT DISTINCT nbclient from Tmct0fis")
  public List<String> findNombres();

  @Query("SELECT DISTINCT nbclien1 from Tmct0fis")
  public List<String> findApellido1();

  @Query("SELECT DISTINCT nbclien2 from Tmct0fis")
  public List<String> findApellido2();

  @Query("SELECT DISTINCT cdholder from Tmct0fis")
  public List<String> findCdholder();

  @Query("SELECT DISTINCT nbclient || ' ' || nbclien1 || ' ' || nbclien2 from Tmct0fis")
  public List<String> getIdTitleList();

  @Query(value = "select * from tmct0fis where cddclave = ?1", nativeQuery = true)
  public Tmct0fis traerDatosPersona(String cddclave);

} // Tmct0fisDao
