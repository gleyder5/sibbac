package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import sibbac.business.mantenimientodatoscliente.database.common.ConcatUtils;
import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.business.mantenimientodatoscliente.database.dto.ConcatDTO;
import sibbac.business.mantenimientodatoscliente.database.dto.FinalHolderDTO;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0provincias;
import sibbac.business.wrappers.database.model.Tmct0swi;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TipoIdMifid;
import sibbac.common.utils.SibbacEnums.TipoCodigoIdentificacion;
import sibbac.common.utils.SibbacEnums.TipoIdentificacion;
import sibbac.common.utils.SibbacEnums.TipoNacionalidad;

/**
 * Clase para validar los registros de final holder
 * 
 * @author miriam.gomez
 *
 */
@Component
public class FinalHolderValidator {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static String FISICA = "F";
  public static String NACIONAL = "N";
  public static String EXTRANJERO = "E";
  public static String JURIDICA = "J";

  public static String NIF = "N";
  public static String NIE = "E";
  public static String CIF = "C";
  public static String BIC = "B";
  public static String OTROS = "O";

  public static String ESPCODE = "011";
  public static String TEST_ESPCODE = "724";

  public static String REPRESENTANTE = "R";

  private static final Logger LOG = LoggerFactory.getLogger(FinalHolderValidator.class);

  @Autowired
  private ConcatUtils concatUtils;

  /** The paises bo. */
  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  /**
   * Validaciones sobre controles adicionales e imprescindibles
   * 
   * @param finalHolderDTO
   * @param errores
   * @return
   * @throws SIBBACBusinessException
   */
  private List<String> validateFinalHolderSocietyType(FinalHolderDTO finalHolderDTO, Tmct0paises pais,
      Tmct0paises paisBIC, Map<String, Boolean> errores) throws SIBBACBusinessException {

    List<String> result = new ArrayList<String>();

    try {

      // Validaciones sobre el campo Type Society
      FinalHoldersValidateTypeSociety validatorTypeSociety = new FinalHoldersValidateTypeSociety();
      validatorTypeSociety.setTpIdenti(finalHolderDTO.getTipoDocumento());
      validatorTypeSociety.setTpSociedad(finalHolderDTO.getTipoPersona());
      validatorTypeSociety.setTypeNationality(finalHolderDTO.getTipoNacionalidad());
      validatorTypeSociety.setNudnicif(finalHolderDTO.getDocumento());
      validatorTypeSociety.setResult(result);
      validatorTypeSociety.validate();

      // Validaciones sobre el codigo de pais

      FinalHoldersValidateCountryCode validatorCountryCode = new FinalHoldersValidateCountryCode();
      validatorCountryCode.setCountryCode(finalHolderDTO.getCodPaisResidencia());
      validatorCountryCode.setResult(result);
      validatorCountryCode.validateCountryCode(pais, paisBIC);

      if (!result.isEmpty()) {
        errores.put("erroresData", Boolean.TRUE);
      }

    }
    catch (Exception e) {
      LOG.error(e.getLocalizedMessage());
      throw new SIBBACBusinessException("Error en la validación de los datos");
    }
    return result;
  }

  /**
   * Validaciones sobre el pais de residencia
   * 
   * @param finalHolderDTO
   * @param errores
   * @return
   * @throws SIBBACBusinessException
   */
  private List<String> validateFinalHolderCross(FinalHolderDTO finalHolderDTO, Map<String, Boolean> errores)
      throws SIBBACBusinessException {

    List<String> result = new ArrayList<String>();

    try {

      if (ESPCODE.equals(finalHolderDTO.getCodPaisResidencia())) {
        if (!(NIF.equals(finalHolderDTO.getTipoDocumento()) || CIF.equals(finalHolderDTO.getTipoDocumento()) || NIE
            .equals(finalHolderDTO.getTipoDocumento()))) {
          // Whether 'Country code' field is ES_ESPAÑA, only 'Type identity' NIF
          // or CIF are allowed
          result.add(ErrorFinalHolderEnum.ERROR_64.getError());
        }
      }
      else {
        if (!(NIE.equals(finalHolderDTO.getTipoDocumento()) || BIC.equals(finalHolderDTO.getTipoDocumento()) || OTROS
            .equals(finalHolderDTO.getTipoDocumento()))) {

          if (!"-".equals(finalHolderDTO.getCodPaisResidencia())) {
            if (finalHolderDTO.getDocumento() != null && !"".equals(finalHolderDTO.getDocumento())) {
              if (!finalHolderDTO.getDocumento().startsWith("N") && !finalHolderDTO.getDocumento().startsWith("W")) {
                if (JURIDICA.equals(finalHolderDTO.getTipoPersona())) {
                  result.add(ErrorFinalHolderEnum.ERROR_68.getError());
                }
              }
            }
          }
          else {
            result.add(ErrorFinalHolderEnum.ERROR_26.getError());
          }
        }
      }

      if (!result.isEmpty()) {
        errores.put("erroresData", Boolean.TRUE);
      }

    }
    catch (Exception e) {
      LOG.error(e.getLocalizedMessage());
      throw new SIBBACBusinessException("Error en la validación de los datos");
    }
    return result;
  }

  /**
   * validaciones sobre la dirección
   * 
   * @param finalHolderDTO
   * @param pais
   * @param tval0pro
   * @param errores
   * @return
   * @throws SIBBACBusinessException
   */
  private List<String> validateFinalHolderAddress(FinalHolderDTO finalHolderDTO, Tmct0paises pais,
      Tmct0provincias tval0pro, Map<String, Boolean> errores) throws SIBBACBusinessException {

    List<String> result = new ArrayList<String>();

    try {

      FinalHoldersValidateAddress validatorAddress = new FinalHoldersValidateAddress(finalHolderDTO.getTipoDireccion(),
          finalHolderDTO.getDomicilio(), finalHolderDTO.getCiudad(), finalHolderDTO.getCodPaisResidencia(),
          finalHolderDTO.getCodigoPostal(), finalHolderDTO.getProvincia(), finalHolderDTO.getNumDomicilio());
      validatorAddress.setResult(result);
      validatorAddress.validate(pais, tval0pro);

      if (!result.isEmpty()) {
        errores.put("erroresDireccion", Boolean.TRUE);
      }

    }
    catch (Exception e) {
      LOG.error(e.getLocalizedMessage());
      throw new SIBBACBusinessException("Error en la validación de los datos");
    }
    return result;
  }

  /**
   * validaciones sobre un titular
   * 
   * @param finalHolderDTO
   * @param paisNacionalidad
   * @param paisBIC
   * @param tmct0swi
   * @param titularDocumento
   * @param titularBIC
   * @param errores
   * @param isModificar
   * @return
   * @throws SIBBACBusinessException
   */
  private List<String> validateFinalHolderData(FinalHolderDTO finalHolderDTO, Tmct0paises paisNacionalidad,
      Tmct0paises paisBIC, Tmct0swi tmct0swi, Tmct0fis titularDocumento, Tmct0fis titularBIC,
      Map<String, Boolean> errores, boolean isModificar) throws SIBBACBusinessException {

    List<String> result = new ArrayList<String>();

    try {

      /*
       * Validaciones sobre los campos nombre y apellidos
       */

      boolean name = false;
      boolean first = false;
      boolean second = false;

      if (finalHolderDTO.getNombre() != null && !"".equals(finalHolderDTO.getNombre())) {

        name = true;

        if (ValidateString.validateAUDWithSpaces(finalHolderDTO.getNombre().trim())) {
          if (FISICA.equals(finalHolderDTO.getTipoPersona())) {
            // Name field error: only characters from [A-Z] are admitted
            result.add(ErrorFinalHolderEnum.ERROR_80.getError());
          }
          else if (JURIDICA.equals(finalHolderDTO.getTipoPersona())) {
            result.add(ErrorFinalHolderEnum.ERROR_83.getError());
          }
        }
      }

      if (finalHolderDTO.getApellido1() != null && !"".equals(finalHolderDTO.getApellido1())) {

        first = true;

        if (ValidateString.validateAUDWithSpaces(finalHolderDTO.getApellido1().trim())) {
          if (FISICA.equals(finalHolderDTO.getTipoPersona())) {
            // Name field error: only characters from [A-Z] are admitted
            result.add(ErrorFinalHolderEnum.ERROR_81.getError());
          }
          else if (JURIDICA.equals(finalHolderDTO.getTipoPersona())) {
            result.add(ErrorFinalHolderEnum.ERROR_83.getError());
          }
        }
      }

      if (finalHolderDTO.getApellido2() != null && !"".equals(finalHolderDTO.getApellido2())) {

        second = true;

        if (ValidateString.validateAUDWithSpaces(finalHolderDTO.getApellido2().trim())) {

          if (FISICA.equals(finalHolderDTO.getTipoPersona())) {
            // Second Name field error: only characters from [A-Z] are admitted
            result.add(ErrorFinalHolderEnum.ERROR_82.getError());
          }
        }
      }

      if (FISICA.equals(finalHolderDTO.getTipoPersona()) && NACIONAL.equals(finalHolderDTO.getTipoNacionalidad())) {
        if (!(name && first && second)) {
          // Fields 'Name', 'First name' and 'Second name' are required for a
          // national physic person
          result.add(ErrorFinalHolderEnum.ERROR_01.getError());
        }
      }

      if (FISICA.equals(finalHolderDTO.getTipoPersona()) && EXTRANJERO.equals(finalHolderDTO.getTipoNacionalidad())) {
        if (!(name && first)) {
          // Fields 'Name' and 'First name' are required
          result.add(ErrorFinalHolderEnum.ERROR_02.getError());
        }
      }

      if (JURIDICA.equals(finalHolderDTO.getTipoPersona()) && EXTRANJERO.equals(finalHolderDTO.getTipoNacionalidad())) {

        if (name || second) {
          // Fields 'Name' and 'Second name' must not be filled for a legal
          // person
          result.add(ErrorFinalHolderEnum.ERROR_04.getError());
        }
      }

      if (JURIDICA.equals(finalHolderDTO.getTipoPersona())) {
        if (name || second || !first) {
          // Fields 'Name' and 'Second name' must not be filled for a legal
          // person
          result.add(ErrorFinalHolderEnum.ERROR_03.getError());
        }
      }

      /*
       * Required fields
       */
      if (finalHolderDTO.getTipoDocumento() == null || "".equals(finalHolderDTO.getTipoDocumento().trim())) {
        // Type Identity field is required
        result.add(ErrorFinalHolderEnum.ERROR_06.getError());
      }

      // Society type validations
      if (finalHolderDTO.getTipoPersona() == null || "".equals(finalHolderDTO.getTipoPersona().trim())) {
        // Type Society field is required
        result.add(ErrorFinalHolderEnum.ERROR_07.getError());
      }
      else {
        if (FISICA.equals(finalHolderDTO.getTipoPersona())) {
          if (!NIF.equals(finalHolderDTO.getTipoDocumento()) && !NIE.equals(finalHolderDTO.getTipoDocumento())
              && !OTROS.equals(finalHolderDTO.getTipoDocumento())) {
            result.add(ErrorFinalHolderEnum.ERROR_62.getError());
          }
        }
        else if (JURIDICA.equals(finalHolderDTO.getTipoPersona())) {
          if (!CIF.equals(finalHolderDTO.getTipoDocumento()) && !BIC.equals(finalHolderDTO.getTipoDocumento())
              && !OTROS.equals(finalHolderDTO.getTipoDocumento())) {
            result.add(ErrorFinalHolderEnum.ERROR_63.getError());
          }
        }
      }

      if (finalHolderDTO.getCodNacionalidad() == null || "".equals(finalHolderDTO.getCodNacionalidad().trim())
          || "-".equals(finalHolderDTO.getCodNacionalidad().trim())) {
        // Nationality code field is required
        result.add(ErrorFinalHolderEnum.ERROR_09.getError());
      }
      else {
        if (finalHolderDTO.getCodNacionalidad().length() == 3) {
          if (TEST_ESPCODE.equals(finalHolderDTO.getCodNacionalidad())
              && !NACIONAL.equals(finalHolderDTO.getTipoNacionalidad())) {
            // If 'Nationality code' is ES-ESPAÑA then 'Type nationality' must
            // be 'NATIONAL'
            result.add(ErrorFinalHolderEnum.ERROR_60.getError());
          }
          if (!TEST_ESPCODE.equals(finalHolderDTO.getCodNacionalidad())
              && NACIONAL.equals(finalHolderDTO.getTipoNacionalidad())) {
            result.add(ErrorFinalHolderEnum.ERROR_61.getError());
          }
        }
        else {
          // Nationality code field must have 3 positions
          result.add(ErrorFinalHolderEnum.ERROR_24.getError());
        }
      }

      if (finalHolderDTO.getTipoNacionalidad() == null || "".equals(finalHolderDTO.getTipoNacionalidad().trim())) {
        // 'Type nationality' field (nacional/extranjero) is required
        result.add(ErrorFinalHolderEnum.ERROR_08.getError());
      }

      if (finalHolderDTO.getTipoTitular() == null || "".equals(finalHolderDTO.getTipoTitular().trim())) {
        // 'Holder type' field is required
        result.add(ErrorFinalHolderEnum.ERROR_10.getError());
      }
      else {
        if (REPRESENTANTE.equals(finalHolderDTO.getTipoTitular()) && BIC.equals(finalHolderDTO.getTipoDocumento())) {
          // A Representative cannot have as 'Type Identity' a BIC
          result.add(ErrorFinalHolderEnum.ERROR_69.getError());
        }
      }

      // NIF, CIF, NIE cannot be filled at the same time that BIC
      // Comento este error porque no va ser posible que se genere
      /*
       * if ((finalHolderDTO.getDocumento() != null && !
       * "".equals(finalHolderDTO.getDocumento().trim()) &&
       * finalHolderDTO.getDocumento().length() > 0) &&
       * (finalHolderDTO.getDocumento() != null && !
       * "".equals(finalHolderDTO.getDocumento().trim()) &&
       * finalHolderDTO.getDocumento().length() > 0)) { //
       * "DNI/CIF field and BIC field cannot be filled simultaneusly"
       * result.add(ErrorFinalHolderEnum.ERROR_44.getError()); }
       */

      /*
       * Supported values
       */
      if (!TipoCodigoIdentificacion.CIF.getValue().contains(finalHolderDTO.getTipoDocumento())
          && !TipoCodigoIdentificacion.NIE.getValue().contains(finalHolderDTO.getTipoDocumento())
          && !TipoCodigoIdentificacion.NIF.getValue().contains(finalHolderDTO.getTipoDocumento())
          && !TipoCodigoIdentificacion.BIC.getValue().contains(finalHolderDTO.getTipoDocumento())
          && !TipoCodigoIdentificacion.OTROS.getValue().contains(finalHolderDTO.getTipoDocumento())) {
        // 'Type Identity' field error: Values must be one of: N (NIF), C (CIF),
        // E (NIE), B (BIC), O (Otro)
        result.add(ErrorFinalHolderEnum.ERROR_20.getError());
      }

      if (!TipoIdentificacion.FISICA.getValue().contains(finalHolderDTO.getTipoPersona())
          && !TipoIdentificacion.JURIDICA.getValue().contains(finalHolderDTO.getTipoPersona())) {
        // 'Type Society' field error: Values must be one of: F (Física), J
        // (Jurídica)
        result.add(ErrorFinalHolderEnum.ERROR_21.getError());
      }

      if (!TipoNacionalidad.NACIONAL.getValue().contains(finalHolderDTO.getTipoNacionalidad())
          && !TipoNacionalidad.EXTRANJERO.getValue().contains(finalHolderDTO.getTipoNacionalidad())) {
        // 'Type Nationality' field error: Values must be: N (Nacional), E
        // (Extranjero)
        result.add(ErrorFinalHolderEnum.ERROR_22.getError());
      }

      if (!"T".equals(finalHolderDTO.getTipoTitular()) && !"P".equals(finalHolderDTO.getTipoTitular())
          && !"U".equals(finalHolderDTO.getTipoTitular()) && !"N".equals(finalHolderDTO.getTipoTitular())
          && !"R".equals(finalHolderDTO.getTipoTitular()) && !"M".equals(finalHolderDTO.getTipoTitular())) {
        // 'Holder Type' field error: Values must be one of: T (Titular), P
        // (Propietario),
        // U (Usufructuario), M (Menor), R (Representante)
        result.add(ErrorFinalHolderEnum.ERROR_23.getError());
      }

      /*
       * Validaciones sobre NIF
       */
      if (NIF.equals(finalHolderDTO.getTipoDocumento())) {

        if (finalHolderDTO.getDocumento() == null || "".equals(finalHolderDTO.getDocumento().trim())) {
          result.add(ErrorFinalHolderEnum.ERROR_05.getError());
        }
        else {
          FinalHoldersValidateNIF validatorNIF = new FinalHoldersValidateNIF();
          validatorNIF.setNudnicif(finalHolderDTO.getDocumento());
          validatorNIF.setResult(result);
          validatorNIF.validate();
        }

      }

      /*
       * Validaciones sobre NIE
       */
      if (NIE.equals(finalHolderDTO.getTipoDocumento())) {

        if (finalHolderDTO.getDocumento() == null || "".equals(finalHolderDTO.getDocumento().trim())) {
          result.add(ErrorFinalHolderEnum.ERROR_05.getError());
        }
        else {
          FinalHoldersValidateNIE validatorNIE = new FinalHoldersValidateNIE();
          validatorNIE.setNudnicif(finalHolderDTO.getDocumento());
          validatorNIE.setResult(result);
          validatorNIE.validate();
        }

      }

      // Validaciones sobre CIF
      if (CIF.equals(finalHolderDTO.getTipoDocumento())) {

        if (finalHolderDTO.getDocumento() == null || "".equals(finalHolderDTO.getDocumento().trim())) {
          result.add(ErrorFinalHolderEnum.ERROR_05.getError());
        }
        else {
          FinalHoldersValidateCIF validatorCIF = new FinalHoldersValidateCIF();
          validatorCIF.setNudnicif(finalHolderDTO.getDocumento());
          validatorCIF.setResult(result);
          validatorCIF.validate();
        }

      }

      // Validaciones sobre BIC
      if (BIC.equals(finalHolderDTO.getTipoDocumento())) {

        if (finalHolderDTO.getDocumento() == null || "".equals(finalHolderDTO.getDocumento().trim())) {
          result.add(ErrorFinalHolderEnum.ERROR_05.getError());
        }
        else {
          FinalHoldersValidateBIC validatorBIC = new FinalHoldersValidateBIC();
          validatorBIC.setBic(finalHolderDTO.getDocumento());
          validatorBIC.setResult(result);
          validatorBIC.validateBIC(paisBIC, tmct0swi);
        }

      }

      // Validaciones sobre OTROS
      if (OTROS.equals(finalHolderDTO.getTipoDocumento())) {

        if (finalHolderDTO.getDocumento() == null || "".equals(finalHolderDTO.getDocumento().trim())) {
          result.add(ErrorFinalHolderEnum.ERROR_05.getError());
        }
        else {
          FinalHoldersValidateOTROS validatorOTROS = new FinalHoldersValidateOTROS();
          validatorOTROS.setOtros(finalHolderDTO.getDocumento());
          validatorOTROS.setResult(result);
          validatorOTROS.validate();
        }
      }

      Tmct0fis titular = null;
      if (!BIC.equals(finalHolderDTO.getTipoDocumento())) {
        if (finalHolderDTO.getDocumento() != null && !"".equals(finalHolderDTO.getDocumento())) {
          titular = titularDocumento;
        }
      }
      else {
        if (finalHolderDTO.getDocumento() != null && !"".equals(finalHolderDTO.getDocumento())) {
          titular = titularBIC;
        }
      }

      if (null != titular) {
        if (OTROS.equals(finalHolderDTO.getTipoDocumento())) {
          if (isModificar) {
            if ((!titular.getId().getCddclave().trim().equals(finalHolderDTO.getCodigo().trim()))
                && (titular.getCdnactit().equals(finalHolderDTO.getCodNacionalidad()))) {
              result.add(ErrorFinalHolderEnum.ERROR_48.getError());
            }
          }
          else {
            if (titular.getCdnactit().equals(finalHolderDTO.getCodNacionalidad())) {
              result.add(ErrorFinalHolderEnum.ERROR_48.getError());
            }
          }
        }
        else {
          // Document number already exist as a Holder
          if (isModificar) {
            if (!titular.getId().getCddclave().trim().equals(finalHolderDTO.getCodigo().trim())) {
              result.add(ErrorFinalHolderEnum.ERROR_98.getError());
            }
          }
          else {
            result.add(ErrorFinalHolderEnum.ERROR_98.getError());
          }
        }
      }
      else {
        if (isModificar) {
          result.add("ERROR_100");
        }
      }

      // Si la sociedad es Fisica debe ser NIF, NIE, Otros.
      if (finalHolderDTO.getTipoPersona().equals(TipoIdentificacion.FISICA.getValue())) {

        if (finalHolderDTO.getTipoIdMifid() != TipoIdMifid.IDENTIFICACION_NACIONAL
            && finalHolderDTO.getTipoIdMifid() != TipoIdMifid.PASAPORTE
            && finalHolderDTO.getTipoIdMifid() != TipoIdMifid.CONCAT) {
          result.add(ErrorFinalHolderEnum.ERROR_107.getError());
        }

        if (finalHolderDTO.getFechaNacimiento() == null || finalHolderDTO.getFechaNacimiento().trim().isEmpty()) {
          result.add(ErrorFinalHolderEnum.ERROR_108.getError());
        }
        String[] valoresArr = paisNacionalidad.getCdTipoIdMifid().split(",");
        List<String> valoresList = Arrays.asList(valoresArr);
        if (!valoresList.contains(finalHolderDTO.getTipoIdMifid() + "")) {
          result.add(ErrorFinalHolderEnum.ERROR_109.getError());
        }

        if (finalHolderDTO.getTipoNacionalidad().equals(TipoNacionalidad.NACIONAL)) {
          if (finalHolderDTO.getIdMifid() == null || finalHolderDTO.getIdMifid().trim().length() < 3
              || !finalHolderDTO.getIdMifid().substring(3).startsWith(finalHolderDTO.getDocumento().trim())) {
            result.add(ErrorFinalHolderEnum.ERROR_112.getError());
          }
        }
      }

      // Si la sociedad es Juridica debe ser CIF, BIC, Otros.
      if (finalHolderDTO.getTipoPersona().equals(TipoIdentificacion.JURIDICA.getValue())) {

        if (finalHolderDTO.getTipoIdMifid() != TipoIdMifid.LEI) {
          result.add(ErrorFinalHolderEnum.ERROR_106.getError());
        }
      }

      if (finalHolderDTO.getTipoIdMifid() == TipoIdMifid.LEI) {
        if (finalHolderDTO.getIdMifid() == null || finalHolderDTO.getIdMifid().trim().length() < 20
            || !finalHolderDTO.getIdMifid().matches("^[a-zA-Z0-9_]*$")) {
          result.add(ErrorFinalHolderEnum.ERROR_110.getError());
        }

      }
      else if (finalHolderDTO.getTipoIdMifid() == TipoIdMifid.CONCAT
          || finalHolderDTO.getTipoIdMifid() == TipoIdMifid.PASAPORTE
          || finalHolderDTO.getTipoIdMifid() == TipoIdMifid.IDENTIFICACION_NACIONAL) {
        if (!finalHolderDTO.getIdMifid().startsWith(paisNacionalidad.getId().getCdisoalf2())) {
          result.add(ErrorFinalHolderEnum.ERROR_111.getError());
        }
      }

      if (finalHolderDTO.getTipoIdMifid() == TipoIdMifid.CONCAT) {
        if (!isValidConcatIdMifid(finalHolderDTO)) {
          result.add(ErrorFinalHolderEnum.ERROR_115.getError());
        }
      }

      if (finalHolderDTO.getIndClienteSan() == 'S'
          && (finalHolderDTO.getNumPersonaSan() == null || finalHolderDTO.getNumPersonaSan().trim().isEmpty())) {
        result.add(ErrorFinalHolderEnum.ERROR_113.getError());

      }
      else if (finalHolderDTO.getIndClienteSan() == 'N'
          && (finalHolderDTO.getNumPersonaSan() != null && !finalHolderDTO.getNumPersonaSan().trim().isEmpty())) {
        result.add(ErrorFinalHolderEnum.ERROR_114.getError());
      }

      if (!result.isEmpty()) {
        errores.put("erroresData", Boolean.TRUE);
      }

    }
    catch (Exception e) {
      LOG.error(e.getLocalizedMessage());
      throw new SIBBACBusinessException("Error en la validación de los datos");
    }
    return result;

  }

  /**
   * Method that validates wheter a concat type id mifid is correct or not
   * @param finalHolderDTO
   * @return
   */
  private boolean isValidConcatIdMifid(FinalHolderDTO finalHolderDTO) {
    ConcatDTO concat = concatUtils.obtainConcat(finalHolderDTO);
    return finalHolderDTO.getIdMifid().equals(concat.getsConcat());
  }

  /**
   * Validaciones sobre un titular
   * 
   * @param finalHolderDTO
   * @param pais
   * @param paisBIC
   * @param tmct0swi
   * @param titularDocumento
   * @param titularBIC
   * @param tval0pro
   * @param errores
   * @param isModificar
   * @return
   * @throws SIBBACBusinessException
   */
  public List<String> validateFormat(FinalHolderDTO finalHolderDTO, Tmct0paises pais, Tmct0paises paisBIC,
      Tmct0swi tmct0swi, Tmct0fis titularDocumento, Tmct0fis titularBIC, Tmct0provincias tval0pro,
      Map<String, Boolean> errores, boolean isModificar) throws SIBBACBusinessException {
    List<String> result = new ArrayList<String>();
    try {

      result.addAll(validateFinalHolderData(finalHolderDTO, pais, paisBIC, tmct0swi, titularDocumento, titularBIC,
          errores, isModificar));
      result.addAll(validateFinalHolderAddress(finalHolderDTO, pais, tval0pro, errores));

      result.addAll(validateFinalHolderSocietyType(finalHolderDTO, pais, paisBIC, errores));

      result.addAll(validateFinalHolderCross(finalHolderDTO, errores));

    }
    catch (Exception e) {
      LOG.error(e.getLocalizedMessage());
      throw new SIBBACBusinessException("Error en la validación de los datos");
    }
    return result;

  }
}
