package sibbac.business.mantenimientodatoscliente.database.bo;

import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0MifidCategoriaDao;
import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0MifidCuestionarioDao;
import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0MifidTestDao;
import sibbac.business.mantenimientodatoscliente.database.model.FechaMifidTest;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidCategoria;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidCategoriaId;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidCuestionario;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidCuestionarioId;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidTest;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidTestId;
import sibbac.business.wrappers.database.dao.Tmct0CliMifidDao;
import sibbac.business.wrappers.database.dao.Tmct0cliDao;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;
import sibbac.common.SIBBACBusinessException;

@Service
public class Tmct0MifidBo {

  private static final Logger LOG = getLogger(Tmct0MifidBo.class);

  private static final String DATE_FORMAT_CATEGORIA = "yyyy.MM.dd";
  private static final String DATE_FORMAT_TEST_CUESTIONARIO = "dd.MM.yyyy";
  private static final String TIME_FORMAT = "HH.mm.ss";

  @Autowired
  private Tmct0MifidTestDao tmct0MifidTestDao;

  @Autowired
  private Tmct0MifidCategoriaDao tmct0MifidCategoriaDao;

  @Autowired
  private Tmct0MifidCuestionarioDao tmct0MifidCuestionarioDao;

  @Autowired
  private Tmct0cliDao cliDao;

  @Autowired
  private Tmct0CliMifidDao cliMifidDao;

  @Transactional(propagation = REQUIRES_NEW)
  public void tratadoMIFIDCuestionario(List<String> lines) throws SIBBACBusinessException {
    final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_TEST_CUESTIONARIO);
    int linea = 0;

    LOG.trace("[tratadoMIFIDCuestionario] Inicio");
    try {
      for (String line : lines) {
        Tmct0MifidCuestionario tmct0MifidCuestionario = new Tmct0MifidCuestionario();
        Tmct0MifidCuestionarioId tmct0MifidCuestionarioId = new Tmct0MifidCuestionarioId();
        tmct0MifidCuestionarioId.setIdempr(line.substring(0, 4));
        tmct0MifidCuestionarioId.setIdcuest(line.substring(4, 9));
        tmct0MifidCuestionarioId.setCodseg(line.substring(9, 11));
        tmct0MifidCuestionario.setId(tmct0MifidCuestionarioId);
        tmct0MifidCuestionario.setTipcuest(line.substring(21, 26));
        tmct0MifidCuestionario.setSubtipco(line.substring(26, 28));
        tmct0MifidCuestionario.setIdprod(line.substring(28, 31));
        tmct0MifidCuestionario.setIdsubtip(line.substring(31, 34));
        tmct0MifidCuestionario.setTipopers(line.substring(34, 35));
        tmct0MifidCuestionario.setTipservi(line.substring(35, 37));
        tmct0MifidCuestionario.setConsulta(line.substring(47, 48));
        tmct0MifidCuestionario.setUsualt(line.substring(48, 56));
        tmct0MifidCuestionario.setUsumodi(line.substring(66, 74));
        Date dateFecini = formatter.parse(line.substring(11, 21));
        Date dateFecfin = formatter.parse(line.substring(37, 47));
        Date dateFecmodi = formatter.parse(line.substring(56, 66));
        tmct0MifidCuestionario.setFecini(dateFecini);
        tmct0MifidCuestionario.setFecfin(dateFecfin);
        tmct0MifidCuestionario.setFecmodi(dateFecmodi);
        tmct0MifidCuestionarioDao.save(tmct0MifidCuestionario);
        linea++;
      }
      LOG.trace("[tratadoMIFIDCuestionario] Fin");
    }
    catch (ParseException e) {
      throw new SIBBACBusinessException(format(
          "Error al parsear fecha leyendo el fichero MIFID Cuestionario en la linea %d", linea), e);
    }
    catch (RuntimeException ex) {
      throw new SIBBACBusinessException(format(
          "Error no contralado leyendo el fichero MIDIF Cuestionario en la linea %d", linea), ex);
    }
  }

  @Transactional(propagation = REQUIRES_NEW)
  public void tratadoMIFIDTest(List<String> lines) throws SIBBACBusinessException {
    final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_TEST_CUESTIONARIO);
    final SimpleDateFormat formatterHour = new SimpleDateFormat(TIME_FORMAT);
    BigDecimal porcentaje;
    int linea = 0;

    LOG.trace("[tratadoMIFIDTest] Inicio");
    try {
      for (String line : lines) {
        Tmct0MifidTest tmct0MifidTest = new Tmct0MifidTest();
        Tmct0MifidTestId tmct0MifidTestId = new Tmct0MifidTestId();
        tmct0MifidTestId.setIdempr(line.substring(0, 4));
        tmct0MifidTestId.setTipopers(line.substring(4, 5));
        tmct0MifidTestId.setCodpers(Long.valueOf(line.substring(5, 14)));
        tmct0MifidTestId.setIdtest(Long.valueOf(line.substring(14, 19)));
        Date dateFecfin = formatter.parse(line.substring(68, 78));
        tmct0MifidTestId.setFecfin(dateFecfin);
        tmct0MifidTest.setId(tmct0MifidTestId);
        tmct0MifidTest.setEstcuest(line.substring(37, 39));
        tmct0MifidTest.setIdcuest(line.substring(39, 44));
        tmct0MifidTest.setTipoper2(line.substring(44, 45));
        tmct0MifidTest.setCodpers2(Long.valueOf(line.substring(45, 54)));
        tmct0MifidTest.setPuntuac(Long.valueOf(line.substring(54, 57)));
        Double prueba = Double.parseDouble(line.substring(57, 62));
        porcentaje = new BigDecimal(prueba / 100);
        tmct0MifidTest.setPorcenta(porcentaje);
        tmct0MifidTest.setCdperfil(line.substring(62, 64));
        tmct0MifidTest.setCdcentro(line.substring(64, 68));
        tmct0MifidTest.setUsualta(line.substring(88, 96));
        tmct0MifidTest.setUsumodi(line.substring(106, 114));
        Date dateFecini = formatter.parse(line.substring(19, 29));
        Date dateFecmodi = formatter.parse(line.substring(96, 106));
        Date dateFecalta = formatter.parse(line.substring(78, 88));
        Date dateHoraactu = formatterHour.parse(line.substring(29, 37));
        tmct0MifidTest.setFecini(dateFecini);
        tmct0MifidTest.setFecmodi(dateFecmodi);
        tmct0MifidTest.setFecalta(dateFecalta);
        tmct0MifidTest.setHoraactu(dateHoraactu);
        tmct0MifidTestDao.save(tmct0MifidTest);
        linea++;
      }
      LOG.trace("[tratadoMIFIDTest] Fin");
    }
    catch (ParseException e) {
      throw new SIBBACBusinessException(format("Error al parsear fecha leyendo el fichero MIFID Test en línea %d",
          linea), e);
    }
    catch (RuntimeException e) {
      throw new SIBBACBusinessException(format("Error no contralado leyendo el fichero MIFID Test en línea %d", linea),
          e);
    }
  }

  @Transactional(propagation = REQUIRES_NEW)
  public void tratadoMIFIDCategoria(List<String> lines) throws SIBBACBusinessException {
    Date dateFecini;
    Date dateFecfin;
    Date dateFecrevis;
    Date dateFecaltsi;
    Date dateFeultact;
    Date dateTimultac;
    final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_CATEGORIA);
    final SimpleDateFormat formatterHour = new SimpleDateFormat(TIME_FORMAT);
    int linea = 0;

    LOG.trace("[tratadoMIFIDCategoria] Inicio");
    try {
      for (String line : lines) {
        Tmct0MifidCategoria tmct0MifidCategoria = new Tmct0MifidCategoria();
        Tmct0MifidCategoriaId tmct0MifidCategoriaId = new Tmct0MifidCategoriaId();
        tmct0MifidCategoriaId.setIdempr(line.substring(0, 4));
        tmct0MifidCategoriaId.setTipopers(line.substring(4, 5));
        tmct0MifidCategoriaId.setCodpers(Long.valueOf(line.substring(5, 14)));
        dateFecini = formatter.parse(line.substring(14, 24));
        dateFecfin = formatter.parse(line.substring(24, 34));

        tmct0MifidCategoria.setId(tmct0MifidCategoriaId);
        tmct0MifidCategoria.setCdsegmen(line.substring(34, 36));
        tmct0MifidCategoria.setCodseg(line.substring(36, 38));
        tmct0MifidCategoria.setCritsegm(line.substring(38, 41));
        tmct0MifidCategoria.setEstadcg(line.substring(41, 43));
        tmct0MifidCategoria.setNrodomic(Long.valueOf(line.substring(43, 52)));
        tmct0MifidCategoria.setIddispo(line.substring(52, 54));
        tmct0MifidCategoria.setJnrosec(Long.valueOf(line.substring(54, 58)));
        tmct0MifidCategoria.setComents(line.substring(58, 258));
        tmct0MifidCategoria.setUsuualtac(line.substring(278, 286));
        tmct0MifidCategoria.setEmultact(line.substring(286, 290));
        tmct0MifidCategoria.setCeultact(line.substring(290, 294));

        dateFecrevis = formatter.parse(line.substring(258, 268));
        dateFecaltsi = formatter.parse(line.substring(268, 278));
        dateFeultact = formatter.parse(line.substring(294, 304));
        dateTimultac = formatterHour.parse(line.substring(304, 312));
        tmct0MifidCategoria.setFecini(dateFecini);
        tmct0MifidCategoria.setFecfin(dateFecfin);
        tmct0MifidCategoria.setFecrevis(dateFecrevis);
        tmct0MifidCategoria.setFecaltsi(dateFecaltsi);
        tmct0MifidCategoria.setFeultact(dateFeultact);
        tmct0MifidCategoria.setTimultac(dateTimultac);
        tmct0MifidCategoriaDao.save(tmct0MifidCategoria);
        linea++;
      }
      LOG.trace("[tratadoMIFIDCategoria] Fin");
    }
    catch (ParseException e) {
      throw new SIBBACBusinessException(format(
          "Error al parsear fecha leyendo el fichero MIFID Categorías en la línea %d", linea), e);
    }
    catch (RuntimeException e) {
      throw new SIBBACBusinessException(format(
          "Error no controlado leyendo el fichero MIFID Categorías en la línea-%s", linea), e);
    }
  }

  /**
   * 
   * @param codigo C0012 o C0014
   * @param persona F o J
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = REQUIRED)
  public int actualizaCaducidadesIdoneidad(String codigo, String persona) throws SIBBACBusinessException {

    final List<FechaMifidTest> fechas;
    final String message;

    Date fechaActual;
    Tmct0CliMifid cliMifid;
    List<BigInteger> cdcliIdsWithFechaIdoneidad;
    Long idClientL;
    int i = 0;

    LOG.debug("Inicio [actualizaCaducidadesIdoneidad para tipo persona " + persona + " ]");

    try {
      fechas = tmct0MifidTestDao.ultimasFechasTestNew(codigo);

      if (fechas != null && !fechas.isEmpty()) {
        LOG.debug("[tmct0MifidTestDao.ultimasFechasTest para codigo: " + codigo + " devuelve " + fechas.size()
            + "resultados]");
      }

      for (FechaMifidTest fechaTest : fechas) {

        fechaActual = fechaTest.getFechaActual();

        cdcliIdsWithFechaIdoneidad = cliDao.findCliActualizaFechaIdoneidad(fechaActual, fechaTest.getCDBDP(), persona);

        for (BigInteger idCliente : cdcliIdsWithFechaIdoneidad) {
          idClientL = new Long(idCliente.toString());
          cliMifid = cliMifidDao.findByIdCliente(idClientL);
          if (cliMifid == null) {
            cliMifid = new Tmct0CliMifid();
            cliMifid.setCliente(cliDao.findOne(idClientL));
            cliMifid.setCategory("");
            cliMifid.settConven("");
            cliMifid.settIdoneid("");
          }
          cliMifid.setFhIdon(fechaActual);
          cliMifidDao.save(cliMifid);
          i++;
        }
      }
    }
    catch (Exception rex) {
      message = String.format("Error inesperado al intentar actualizar caducidades con codigo %s", codigo);
      LOG.error("[actaulizaCaducidades] {}", message, rex);
      throw new SIBBACBusinessException(message, rex);
    }
    LOG.debug("FIN [actualizaCaducidadesIdoneidad para tipo persona " + persona + " ] " + i + " registros actualizados");
    return i;
  }

  /**
   * 
   * @param codigo C0011 o C0013
   * @param persona F o J
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = REQUIRED)
  public int actualizaCaducidadesConveniencia(String codigo, String persona) throws SIBBACBusinessException {
    final List<FechaMifidTest> fechas;
    final String message;
    Date fechaActual;
    Tmct0CliMifid cliMifid;
    List<BigInteger> cdcliIdsWithFechaConveniencia;
    Long idClientL;
    int i = 0;

    LOG.debug("Inicio [actualizaCaducidadesConveniencia para tipo persona " + persona + " ]");

    try {
      fechas = tmct0MifidTestDao.ultimasFechasTestNew(codigo);

      if (fechas != null && !fechas.isEmpty()) {
        LOG.debug("[tmct0MifidTestDao.ultimasFechasTest para codigo: " + codigo + " devuelve " + fechas.size()
            + "resultados]");
      }

      for (FechaMifidTest fechaTest : fechas) {

        fechaActual = fechaTest.getFechaActual();

        cdcliIdsWithFechaConveniencia = cliDao.findCliActualizaFechaConveniencia(fechaActual, fechaTest.getCDBDP(),
            persona);
        for (BigInteger idCliente : cdcliIdsWithFechaConveniencia) {
          idClientL = new Long(idCliente.toString());
          cliMifid = cliMifidDao.findByIdCliente(idClientL);
          if (cliMifid == null) {
            cliMifid = new Tmct0CliMifid();
            cliMifid.setCliente(cliDao.findOne(idClientL));
            cliMifid.setCategory("");
            cliMifid.settConven("");
            cliMifid.settIdoneid("");
          }
          cliMifid.setFhConv(fechaActual);
          cliMifidDao.save(cliMifid);
          i++;
        }

      }
    }
    catch (RuntimeException rex) {
      message = String.format("Error inesperado al intentar actualizar caducidades con codigo %s", codigo);
      LOG.error("[actaulizaCaducidades] {}", message, rex);
      throw new SIBBACBusinessException(message, rex);
    }
    LOG.debug("FIN [actualizaCaducidadesConveniencia para tipo persona " + persona + " ] " + i
        + " registros actualizados");
    return i;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public Integer saveMifidTestLineTransactional(String line) throws SIBBACBusinessException {
    final SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_TEST_CUESTIONARIO);
    final SimpleDateFormat formatterHour = new SimpleDateFormat(TIME_FORMAT);
    BigDecimal porcentaje;
    int lineNumber = 0;
    Tmct0MifidTest tmct0MifidTest = new Tmct0MifidTest();
    Tmct0MifidTestId tmct0MifidTestId = new Tmct0MifidTestId();

    if (line != null) {
      try {

        Long codPers = Long.valueOf(line.substring(5, 14));
        Long idTest = Long.valueOf(line.substring(14, 19));
        String idEmpr = line.substring(0, 4);
        String tipoPers = line.substring(4, 5);
        Date dateFecfin = formatter.parse(line.substring(68, 78));

        // comprobamos si existe en la tabla de TMCT0CLI. Si no existe, no lo guardamos.
        if ((cliDao.findByCdbdp(String.format("%09d", codPers)) > 0)
            && !(String.format("%09d", codPers).equals("000000000"))) {

          if (codPers != null && idEmpr != null && tipoPers != null && idTest != null && dateFecfin != null) {

            tmct0MifidTestId.setCodpers(codPers);
            tmct0MifidTestId.setIdempr(idEmpr);
            tmct0MifidTestId.setTipopers(tipoPers);
            tmct0MifidTestId.setIdtest(idTest);
            tmct0MifidTestId.setFecfin(dateFecfin);
            tmct0MifidTest.setId(tmct0MifidTestId);

            tmct0MifidTest.setEstcuest(line.substring(37, 39));
            tmct0MifidTest.setIdcuest(line.substring(39, 44));
            tmct0MifidTest.setTipoper2(line.substring(44, 45));
            tmct0MifidTest.setCodpers2(Long.valueOf(line.substring(45, 54)));
            tmct0MifidTest.setPuntuac(Long.valueOf(line.substring(54, 57)));
            Double prueba = Double.parseDouble(line.substring(57, 62));
            porcentaje = new BigDecimal(prueba / 100);
            tmct0MifidTest.setPorcenta(porcentaje);
            tmct0MifidTest.setCdperfil(line.substring(62, 64));
            tmct0MifidTest.setCdcentro(line.substring(64, 68));
            tmct0MifidTest.setUsualta(line.substring(88, 96));
            tmct0MifidTest.setUsumodi(line.substring(106, 114));
            Date dateFecini = formatter.parse(line.substring(19, 29));
            Date dateFecmodi = formatter.parse(line.substring(96, 106));
            Date dateFecalta = formatter.parse(line.substring(78, 88));
            Date dateHoraactu = formatterHour.parse(line.substring(29, 37));
            tmct0MifidTest.setFecini(dateFecini);
            tmct0MifidTest.setFecmodi(dateFecmodi);
            tmct0MifidTest.setFecalta(dateFecalta);
            tmct0MifidTest.setHoraactu(dateHoraactu);
            if (tmct0MifidTestId.getCDBDP() != null && tmct0MifidTestId.getIdempr() != null
                && tmct0MifidTestId.getTipopers() != null && tmct0MifidTestId.getCodpers() != null
                && tmct0MifidTestId.getIdtest() != null) {
              tmct0MifidTestDao.save(tmct0MifidTest);
            }
            lineNumber++;
          }
        }
      }
      catch (ParseException e) {
        LOG.debug("ParseException con linea: " + line);
        throw new SIBBACBusinessException(format(
            "Error al parsear fecha leyendo el fichero MIFID Test con objeto %s y linea %s", tmct0MifidTest, line), e);
      }
      catch (RuntimeException e) {
        LOG.debug("RuntimeException con: " + tmct0MifidTest, e);
        throw new SIBBACBusinessException(format(
            "Error no contralado leyendo el fichero MIFID Test con objeto %s y linea %s", tmct0MifidTest, line), e);
      }
      catch (Exception e) {
        LOG.debug("[[[[[[[ Exception general con " + tmct0MifidTest + " y linea " + line, e);
      }
    }

    return lineNumber;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public Integer updateCaducidadesIdoneidadTransactional(FechaMifidTest fechaMifidTest, String persona)
      throws SIBBACBusinessException {
    Integer actualizados = 0;
    LOG.trace("[updateCaducidadesIdoneidadTransactional] con " + fechaMifidTest.getFechaActual() + " y codpersona "
        + fechaMifidTest.getCDBDP());

    List<BigInteger> cdcliIdsWithFechaIdoneidad;
    cdcliIdsWithFechaIdoneidad = cliDao.findCliActualizaFechaIdoneidad(fechaMifidTest.getFechaActual(),
        fechaMifidTest.getCDBDP(), persona);

    for (BigInteger idCliente : cdcliIdsWithFechaIdoneidad) {
      Long idClientL = new Long(idCliente.toString());
      Tmct0CliMifid cliMifid = cliMifidDao.findByIdCliente(idClientL);
      if (cliMifid == null) {
        cliMifid = new Tmct0CliMifid();
        cliMifid.setCliente(cliDao.findOne(idClientL));
        cliMifid.setCategory("");
        cliMifid.settConven("");
        cliMifid.settIdoneid("");
        cliMifid.setFhIdon(fechaMifidTest.getFechaActual());
        cliMifidDao.save(cliMifid);
        actualizados++;
      }
      // si la fecha no es null pero la que tiene no es más actual que la que viene del fichero, actualizamos
      else if (cliMifid.getFhIdon() != null && cliMifid.getFhIdon().before(fechaMifidTest.getFechaActual())) {
        cliMifid.setFhIdon(fechaMifidTest.getFechaActual());
        cliMifidDao.save(cliMifid);
        actualizados++;
      }
      // si la fecha es null, actualizamos
      else if (cliMifid.getFhIdon() == null) {
        cliMifid.setFhIdon(fechaMifidTest.getFechaActual());
        cliMifidDao.save(cliMifid);
        actualizados++;
      }
    }

    LOG.trace("[updateCaducidadesIdoneidadTransactional] fin {}");
    return actualizados;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public Integer updateCaducidadesConvenienciaTransactional(FechaMifidTest fechaMifidTest, String persona)
      throws SIBBACBusinessException {
    Integer actualizados = 0;
    LOG.trace("[updateCaducidadesConvenienciaTransactional] con " + fechaMifidTest.getFechaActual() + " y codpersona "
        + fechaMifidTest.getCDBDP());
    List<BigInteger> cdcliIdsWithFechaConveniencia;
    cdcliIdsWithFechaConveniencia = cliDao.findCliActualizaFechaConveniencia(fechaMifidTest.getFechaActual(),
        fechaMifidTest.getCDBDP(), persona);

    boolean guardar = false;

    for (BigInteger idCliente : cdcliIdsWithFechaConveniencia) {
      Long idClientL = new Long(idCliente.toString());
      Tmct0CliMifid cliMifid = cliMifidDao.findByIdCliente(idClientL);
      if (cliMifid == null) {
        cliMifid = new Tmct0CliMifid();
        cliMifid.setCliente(cliDao.findOne(idClientL));
        cliMifid.setCategory("");
        cliMifid.settConven("");
        cliMifid.settIdoneid("");
        guardar = true;
      }
      // si la fecha no es null pero la que tiene no es más actual que la que viene del fichero, actualizamos
      else if (cliMifid.getFhConv() != null && cliMifid.getFhConv().before(fechaMifidTest.getFechaActual())) {
        guardar = true;
      }
      // si la fecha es null, actualizamos
      else if (cliMifid.getFhConv() == null) {
        guardar = true;
      }

      if (guardar) {
        cliMifid.setFhConv(fechaMifidTest.getFechaActual());
        cliMifidDao.save(cliMifid);
        actualizados++;
        guardar = false;
      }
    }

    LOG.trace("[updateCaducidadesConvenienciaTransactional] fin {}");
    return actualizados;
  }

}
