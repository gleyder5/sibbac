package sibbac.business.mantenimientodatoscliente.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.mantenimientodatoscliente.database.model.FechaMifidTest;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidTest;

@Repository
public interface Tmct0MifidTestDao extends JpaRepository<Tmct0MifidTest, String> {

  void deleteAll();

  @Query(value = "SELECT new "
      + "sibbac.business.mantenimientodatoscliente.database.model.FechaMifidTest(t.id.codpers, MAX(t.id.fecfin))"
      + " FROM Tmct0MifidTest t WHERE t.idcuest=:codigo GROUP BY t.id.codpers")
  List<FechaMifidTest> ultimasFechasTestNew(@Param("codigo") String codigo);
   
  @Query( "SELECT count(c) FROM Tmct0MifidTest c WHERE c.id.idempr = :idempr "
      + "AND c.id.tipopers = :tipoPers AND c.id.codpers = :codPers "
      + "AND c.id.idtest = :idTest")
  public Integer existsByTmct0MifidTestId(@Param( "idempr" ) String idempr, 
      @Param( "tipoPers" ) String tipoPers, @Param( "codPers" ) Long codPers,  @Param( "idTest" ) Long idTest);
  
  @Query( "SELECT c FROM Tmct0MifidTest c WHERE c.id.idempr = :idempr "
      + "AND c.id.tipopers = :tipoPers AND c.id.codpers = :codPers "
      + "AND c.id.idtest = :idTest")
  public Tmct0MifidTest findByTmct0MifidTestId(@Param( "idempr" ) String idempr, 
      @Param( "tipoPers" ) String tipoPers, @Param( "codPers" ) Long codPers,  @Param( "idTest" ) Long idTest);  
}