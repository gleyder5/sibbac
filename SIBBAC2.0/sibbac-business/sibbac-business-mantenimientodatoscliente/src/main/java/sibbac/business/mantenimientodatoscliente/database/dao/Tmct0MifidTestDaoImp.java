package sibbac.business.mantenimientodatoscliente.database.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidTest;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;

@Repository
public class Tmct0MifidTestDaoImp {
	@PersistenceContext
	private EntityManager em;
	
	private static final String INSERT_CDESTADOCONT = 
	    "INSERT INTO tmct0_alc_estadocont (estadocont, nbooking, nuorden, nucnfliq, nucnfclt, tipoestado)"
      + " VALUES (:estadocont, :nbooking, :nuorden, :nucnfliq, :nucnfclt, :tipoestado)";	

//insert into BSNBPSQL.TMCT0_MIFIDTEST (CDCENTRO, CDPERFIL, CODPERS2, ESTCUEST, FECALTA, FECFIN, FECINI, FECMODI, HORAACTU, IDCUEST, PORCENTA, PUNTUAC, TIPOPER2, USUALTA, USUMODI, CODPERS, IDEMPR, IDTEST, TIPOPERS) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not perform addBatch


 
	
  public int insertNativo(Tmct0MifidTest tmct0MifidTest ) {
    Query query = em.createNativeQuery(INSERT_CDESTADOCONT);
    putParameters(query, tmct0MifidTest);
    return query.executeUpdate();
  } 
  
  private void putParameters(Query query, Tmct0MifidTest t) {
    query.setParameter("CDCENTRO", t.getCdcentro());

    
    query.setParameter("IDEMPR", t.getCdcentro());
    query.setParameter("IDTEST", t.getCdcentro());
    query.setParameter("TIPOPERS", t.getCdcentro());
    
    query.setParameter("PORCENTA", t.getCdcentro());
    query.setParameter("PUNTUAC", t.getCdcentro());
    query.setParameter("TIPOPER2", t.getCdcentro());
    query.setParameter("USUALTA", t.getCdcentro());
    query.setParameter("USUMODI", t.getCdcentro());
    query.setParameter("CODPERS", t.getCdcentro());
    
    query.setParameter("CDPERFIL", t.getCdcentro());
    query.setParameter("CODPERS2", t.getCdcentro());
    query.setParameter("ESTCUEST", t.getCdcentro());
    query.setParameter("FECALTA", t.getCdcentro());
    query.setParameter("FECFIN", t.getCdcentro());
    query.setParameter("FECINI", t.getCdcentro());
    query.setParameter("FECMODI", t.getCdcentro());
    query.setParameter("HORAACTU", t.getCdcentro());
    query.setParameter("IDCUEST", t.getCdcentro());
    
//    query.setParameter("nbooking", nbooking);
//    query.setParameter("nuorden", nuorden);
//    query.setParameter("nucnfliq", nucnfliq);
//    query.setParameter("nucnfclt", nucnfclt);
//    query.setParameter("tipoestado", tipoestado);
  } // putParameters

}
