package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.common.SIBBACBusinessException;

/*
 * Validaciones sobre el tipo de documento NIF
 */
public class FinalHoldersValidateNIF implements  Validable{
	
	private static final Logger	LOG				= LoggerFactory.getLogger( FinalHoldersValidateNIF.class );

	// NIF value
	private String nudnicif;
	
	public void setNudnicif(String nudnicif) {
		this.nudnicif = nudnicif;
	}
	
	// Errors accumulator
	private List<String> result;
	public void setResult(List<String> result) {
		this.result = result;
	}
	
	/**
	 * Validate command executor 4 NIF validations
	 */
	@Override
	public void validate() throws SIBBACBusinessException{
		
		try{
		
		if ( ! this.nudnicif.toUpperCase().startsWith("K") && ! this.nudnicif.toUpperCase().startsWith("L") && ! this.nudnicif.toUpperCase().startsWith("M") && ! Character.isDigit(this.nudnicif.charAt(0))){
			// NIF format error: First position must be one of K, L, M or a digit between 0-9
			this.result.add(ErrorFinalHolderEnum.ERROR_30.getError());
			this.result.add(ErrorFinalHolderEnum.ERROR_31.getError());
		} else {
			
			if (ValidateString.validateAlphanumericWithoutSpaces(nudnicif)) {
				// NIF field error: only caracters from [0-9] and [A-Z] are admited
				//TODO: Miriam no se si es este error o no...
				result.add(ErrorFinalHolderEnum.ERROR_47.getError());
			} else {
				// Validate length field
				if (this.nudnicif.length() != 9) {
					// NIF format error: a NIF must have 9 positions
					this.result.add(ErrorFinalHolderEnum.ERROR_31.getError());
					this.result.add(ErrorFinalHolderEnum.ERROR_41.getError());
				} else {
					if(ValidateString.validateABC(nudnicif.substring(nudnicif.length()-1))){
						this.result.add(ErrorFinalHolderEnum.ERROR_31.getError());
						
					}else{
						// Validate control digit
						String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
						String nif = "";
						
						/////////////////
						// Si no es numerico descartar el primer caracter
						char primera_letra = this.nudnicif.charAt(0);
						int modulus=0;
						if(!Character.isDigit(primera_letra) ){
							nif= this.nudnicif.substring(1,8);
							modulus  = Integer.parseInt(nif) % 23;
						}else
						{
							nif= this.nudnicif.substring(0,8);
							modulus  = Integer.parseInt(nif) % 23;
						}
							
						//////////////////////
						
						String legalDigitControl = letras.substring(modulus, modulus+1);			
						String digitControl = this.nudnicif.substring(8);
						
						if ( ! digitControl.toUpperCase().equals(legalDigitControl)) {
							// NIF format error: Control digit not match
							this.result.add(ErrorFinalHolderEnum.ERROR_34.getError());
						}
					}
				}
			}
			
		}
		}catch(Exception e){
			LOG.error(e.getLocalizedMessage());
			throw new SIBBACBusinessException("Error en la validación de los datos");
		}
		
	}

}
