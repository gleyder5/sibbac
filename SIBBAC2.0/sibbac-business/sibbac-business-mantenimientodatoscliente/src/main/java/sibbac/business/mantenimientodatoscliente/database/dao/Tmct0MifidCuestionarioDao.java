package sibbac.business.mantenimientodatoscliente.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidCuestionario;

@Repository
public interface Tmct0MifidCuestionarioDao extends JpaRepository <Tmct0MifidCuestionario, String>{

}
