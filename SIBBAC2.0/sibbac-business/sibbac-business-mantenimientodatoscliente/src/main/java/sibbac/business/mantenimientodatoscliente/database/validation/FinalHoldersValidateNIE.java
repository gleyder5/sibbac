package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;

/*
 * Validaciones sobre el tipo de documento NIE
 */
public class FinalHoldersValidateNIE implements Validable {

	// NIE value
	private String nudnicif;
	public void setNudnicif(String nudnicif) {
		this.nudnicif = nudnicif;
	}
	
	// Errors accumulator
	private List<String> result;
	public void setResult(List<String> result) {
		this.result = result;
	}
	
	/**
	 * Validate command executor 4 NIE validations
	 */
	@Override
	public void validate() {
		
		// Validate first position
		if ( ! this.nudnicif.toUpperCase().startsWith("X") && ! this.nudnicif.toUpperCase().startsWith("Y") && ! this.nudnicif.toUpperCase().startsWith("Z")){
			//	NIE format error: First position must be one of X, Y or Z
			this.result.add(ErrorFinalHolderEnum.ERROR_32.getError());
			this.result.add(ErrorFinalHolderEnum.ERROR_33.getError());
		} else {
			
			if (ValidateString.validateAlphanumericWithoutSpaces(this.nudnicif)) {
				// NIE field error: only caracters from [0-9] and [A-Z] are admited
				//TODO: Miriam no se si es este error o no...
				result.add(ErrorFinalHolderEnum.ERROR_47.getError());
			} else {
				
				// Validate length field
				if (this.nudnicif.length() != 9) {
					// NIE format error: a NIE must have 9 positions
					this.result.add(ErrorFinalHolderEnum.ERROR_33.getError());
					this.result.add(ErrorFinalHolderEnum.ERROR_41.getError());
				} else {
					if(ValidateString.validateABC(nudnicif.substring(nudnicif.length()-1))){
						this.result.add(ErrorFinalHolderEnum.ERROR_33.getError());
						
					}else{
						// Validate control digit
						String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
						String nie = "";
						
						switch (this.nudnicif.toUpperCase().charAt(0)){
							case 'X': 
									nie = "0".concat(this.nudnicif.substring(1, 8));
									break;
							case 'Y': 
									nie = "1".concat(this.nudnicif.substring(1, 8));
									break;
							case 'Z': 
									nie = "2".concat(this.nudnicif.substring(1, 8));
									break;
						}
						
						int modulus = Integer.parseInt(nie) % 23;
						
						
						String legalDigitControl = letras.substring(modulus, modulus+1);			
						String digitControl = this.nudnicif.substring(8);
						
						if ( ! digitControl.toUpperCase().equals(legalDigitControl)) {
							// NIE format error: Control digit not match
							this.result.add(ErrorFinalHolderEnum.ERROR_34.getError());
						}
					}
				}
				
			}
			
		}
	}
	
	
	

}
