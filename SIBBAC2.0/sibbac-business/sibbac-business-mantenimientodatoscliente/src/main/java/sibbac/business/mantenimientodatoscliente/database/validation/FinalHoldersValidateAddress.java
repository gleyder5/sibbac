package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0provincias;

/*
 * Validaciones sobre la direccion
 */
public class FinalHoldersValidateAddress implements Validable {

  private String tpdomici;
  private String nbdomici;
  private String nbciudad;
  private String cddepais;
  private String cdpostal;
  private String nbprovin;
  private String nudomici;

  private List<String> result;

  public void setResult(List<String> result) {
    this.result = result;
  }

  public FinalHoldersValidateAddress(String tpdomici, String nbdomici, String nbciudad, String cddepais,
      String cdpostal, String nbprovin, String nudomici) {

    this.tpdomici = tpdomici;
    this.nbdomici = nbdomici;
    this.nbciudad = nbciudad;
    this.cddepais = cddepais;
    this.cdpostal = cdpostal;
    this.nbprovin = nbprovin;
    this.nudomici = nudomici;

  }

  @Override
  public void validate() {

  }

  public void validate(Tmct0paises pais, Tmct0provincias tval0pro) {

    // type address validations
    if ("".equals(this.tpdomici.trim())) {
      // 'Address type' field has to be filled
      this.result.add(ErrorFinalHolderEnum.ERROR_11.getError());
    }

    // value address validations
    if ("".equals(this.nbdomici.trim())) {
      // 'Address' field is required
      this.result.add(ErrorFinalHolderEnum.ERROR_12.getError());
    }
    else {
      if (this.nbdomici.trim().length() < 3) {
        // 'Address' field length has to be at less 3 characters
        this.result.add(ErrorFinalHolderEnum.ERROR_28.getError());
      }
    }

    // number address validations
    if ("".equals(this.nudomici.trim())) {
      // 'Number' field is required
      this.result.add(ErrorFinalHolderEnum.ERROR_13.getError());
    }

    // name city validations
    if ("".equals(this.nbciudad.trim())) {
      // 'City' field is required
      this.result.add(ErrorFinalHolderEnum.ERROR_14.getError());
    }

    // Country code validations
    if ("".equals(this.cddepais.trim()) || "-".equals(this.cddepais.trim())) {
      // 'Country code' field is required
      this.result.add(ErrorFinalHolderEnum.ERROR_17.getError());
    }
    else {
      if (this.cddepais.length() != 3) {
        result.add(ErrorFinalHolderEnum.ERROR_26.getError());
      }
      else {

        if (pais == null) {
          // Country code field error, code not found in ISO 3166-1
          result.add(ErrorFinalHolderEnum.ERROR_25.getError());
        }
      }
    }

    // Postal code validations
    if (this.cdpostal == null || "".equals(this.cdpostal.trim())) {
      // 'Postal code' field is required
      if (FinalHolderValidator.ESPCODE.equals(this.cddepais)) {
        this.result.add(ErrorFinalHolderEnum.ERROR_16.getError());
      }
    }
    else {
      if (this.cdpostal.length() != 5) {
        // 'Postal code' field must have 5 digits
        this.result.add(ErrorFinalHolderEnum.ERROR_29.getError());
      }
      else {

        boolean continuar = true;
        for (int i = 0; i < this.cdpostal.length(); i++) {
          if (!Character.isDigit(this.cdpostal.charAt(i))) {
            this.result.add(ErrorFinalHolderEnum.ERROR_29.getError());
            continuar = false;
            break;
          }
        }

        if (continuar) {

          if (this.cddepais.length() == 3) {
            if (FinalHolderValidator.ESPCODE.equals(this.cddepais)) {

              int first2 = Integer.parseInt(this.cdpostal.substring(0, 2));
              if (first2 < 1 || first2 > 52) {
                // 'Postal code' field must be beetwen '01' and '52' for Spain
                // postal codes
                this.result.add(ErrorFinalHolderEnum.ERROR_72.getError());
              }
              int last3 = Integer.parseInt(this.cdpostal.substring(2, 5));
              if (last3 == 0) {
                // El código postal no puede terminar en 000 para España
                this.result.add(ErrorFinalHolderEnum.ERROR_74.getError());
              }

            }
            else {

              int first2 = Integer.parseInt(cdpostal.substring(0, 2));
              if (first2 != 99) {
                // 'Postal code' field has to begin with 99
                this.result.add(ErrorFinalHolderEnum.ERROR_70.getError());
              }
              else {
                if (!this.cdpostal.substring(2).equals(cddepais.trim())) {
                  this.result.add(ErrorFinalHolderEnum.ERROR_71.getError());
                }
              }

            }
          }
        }
      }
    }

    // Name provincia validations
    if ("".equals(this.nbprovin.trim())) {
      // 'Country code' field is required
      this.result.add(ErrorFinalHolderEnum.ERROR_15.getError());
    }
    else if (FinalHolderValidator.ESPCODE.equals(this.cddepais)) {

      if (tval0pro == null || !this.cdpostal.substring(0, 2).equals(tval0pro.getCdcodigo().trim())) {
        this.result.add(ErrorFinalHolderEnum.ERROR_72.getError());
      }
      else {
        if (!tval0pro.getNbprovin().trim().equals(this.nbprovin.toUpperCase())) {
          // The name of the province of the registered holder must match the
          // name of the province in the first two positions of the postal code
          this.result.add(ErrorFinalHolderEnum.ERROR_73.getError());
        }
      }
    }
  }

}
