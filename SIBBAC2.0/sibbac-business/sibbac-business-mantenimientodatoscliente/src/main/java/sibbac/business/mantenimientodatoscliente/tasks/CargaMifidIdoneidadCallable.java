package sibbac.business.mantenimientodatoscliente.tasks;

import java.util.Iterator;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0MifidBo;
import sibbac.business.mantenimientodatoscliente.database.model.FechaMifidTest;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class CargaMifidIdoneidadCallable implements Callable<Integer> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CargaMifidIdoneidadCallable.class);

  private Iterator<FechaMifidTest> fechaMifidTestIterator;
  private String persona;
  private Integer count = 0;

  @Autowired
  private Tmct0MifidBo tmct0MifidBo;

  public CargaMifidIdoneidadCallable(Iterator<FechaMifidTest> fechaMifidTestI, String persona) {
    super();
    this.fechaMifidTestIterator = fechaMifidTestI;
    this.persona = persona;
  }

  @Override
  public Integer call() throws SIBBACBusinessException {
    Exception ex = null;
    FechaMifidTest fechaMifidTest;
    while (fechaMifidTestIterator.hasNext()) {
      fechaMifidTest = fechaMifidTestIterator.next();
      try {
        count += tmct0MifidBo.updateCaducidadesIdoneidadTransactional(fechaMifidTest, persona);
      }
      catch (Exception e) {
        LOG.debug("Incidencia procesando fecha: {} persona: {} - {}", fechaMifidTest, persona, e);
        ex = e;
      }
    }
    if (ex != null) {
      throw new SIBBACBusinessException(ex);
    }
    return count;
  }

  @Override
  public String toString() {
    return String.format("Nº de registros procesados por el hilo: %s", this.count);
  }
}
