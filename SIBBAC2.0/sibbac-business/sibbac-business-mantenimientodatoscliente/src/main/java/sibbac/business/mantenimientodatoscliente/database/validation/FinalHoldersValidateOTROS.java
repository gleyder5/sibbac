package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;

/*
 * Validaciones sobre el tipo de documento OTROS
 */
public class FinalHoldersValidateOTROS implements Validable{

	// Others value
	private String otros;
	public void setOtros(String otros) {
		this.otros = otros;
	}
	
	// Errors accumulator
	private List<String> result;
	public void setResult(List<String> result) {
		this.result = result;
	}
	
	@Override
	public void validate() {
		
//		if(otros.startsWith("COD")){
//			// 'OTROS' field cannot begin with 'COD'
//			this.result.add(ErrorFinalHolderEnum.ERROR_45.getError());
//		} else 
		if (otros.trim().length()>40) {		
			// Others length should not be greater than 40
			this.result.add(ErrorFinalHolderEnum.ERROR_43.getError());
		}			

	}

}
