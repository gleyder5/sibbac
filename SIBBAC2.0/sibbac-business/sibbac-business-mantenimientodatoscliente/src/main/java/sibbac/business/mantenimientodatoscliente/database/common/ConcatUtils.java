package sibbac.business.mantenimientodatoscliente.database.common;

import java.lang.Character.UnicodeBlock;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sibbac.business.mantenimientodatoscliente.database.dto.ConcatDTO;
import sibbac.business.mantenimientodatoscliente.database.dto.FinalHolderDTO;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.common.utils.FormatDataUtils;

@Component
public class ConcatUtils {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(ConcatUtils.class);

  /** Caracteres especiales */
  public static final String CARACTERES_ESPECIALES = "º,ª";

  /** Titulos a evaular en el nombre del titular */
  public static final String TITULOS = "AATY,COACH,DAME,DR,FR,GOV,HONORABLE,MADAM(E),MAID,MASTER,MISS,MONSIEUR,MR,MRS,MS,MX,OFC,PH.D,PRES,PROF,REV,SIR";

  /** Prefijos a evaluar en los apellidos del titular */
  private static final String[] PREFIJOS = { "AM", "AUF", "AUF DEM", "AUS DER", "D", "DA", "DE", "DE L’", "DEL",
      "DE LA", "DE LE", "DI", "DO", "DOS", "DU", "IM", "LA", "LE", "MAC", "MC", "MHAC", "MHÍC", "MHIC GIOLLA", "MIC",
      "NI", "NÍ", "NÍC", "O", "Ó", "UA", "UI", "UÍ", "VAN", "VAN DE", "VAN DEN", "VAN DER", "VOM", "VON", "VON DEM",
      "VON DEN", "VON DER" };

  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_A = "Ææ";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_D = "ð";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_O = "Øø";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_S = "ß";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_T = "Þþ";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN_EXTENDED_D = "đĐ";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN_EXTENDED_I = "ı";

  /** Caracteres especiales para la transliteración */
  public static final String LATIN_EXTENDED_L = "Łł";

  /** The paises bo. */
  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  public ConcatUtils() {
  }

  public ConcatDTO obtainConcat(FinalHolderDTO finalHolderDTO) {

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    String dateInString = finalHolderDTO.getFechaNacimiento();
    Date date = null;
    try {
      date = formatter.parse(dateInString);
    }
    catch (ParseException e) {
      LOG.warn(e.getMessage(), e);
    }

    return obtainConcat(finalHolderDTO.getNombre(), finalHolderDTO.getApellido1(), finalHolderDTO.getApellido2(), date,
        finalHolderDTO.getCodNacionalidad());
  }

  public ConcatDTO obtainConcat(String nombreTitular, String apellido1, String apellido2, Date fechaNac,
      String codNacionalidad) {

    ConcatDTO resultado = new ConcatDTO();

    String caracteresEspeciales = checkSpecialChars(nombreTitular, apellido1, apellido2);

    if (caracteresEspeciales.isEmpty()) {

      // pasar a minusculas el nombre y los apellidos porque la letra ß en
      // mayusculas es SS.
      nombreTitular = nombreTitular.toLowerCase();
      apellido1 = apellido1.toLowerCase();
      apellido2 = apellido2.toLowerCase();

      // Se eliminan los titulos del nombre
      nombreTitular = eliminarTitulos(nombreTitular);

      // Se eliminan los prefijos de los apellidos
      apellido1 = deletePrefixes(apellido1);
      apellido2 = deletePrefixes(apellido2);

      // Transliteracion de apostrofes, acentos, guiones, espacios y similares
      nombreTitular = translitarTexto(nombreTitular);
      apellido1 = translitarTexto(apellido1);
      apellido2 = translitarTexto(apellido2);
      resultado.setsConcat(concatEverything(nombreTitular, apellido1, apellido2, fechaNac, codNacionalidad, resultado));

    }
    else {
      resultado.setsError(0);
      resultado.setsConcat(caracteresEspeciales);
    }

    return resultado;
  }

  private static String checkSpecialChars(String nombreTitular, String apellido1, String apellido2) {

    StringTokenizer stCaracteresEspeciales = new StringTokenizer(CARACTERES_ESPECIALES, ",");
    String resultado = "";
    String errorNombre = "";
    String errorPrimerApellido = "";
    String errorSegundoApellido = "";
    String sValor = "";
    while (stCaracteresEspeciales.hasMoreTokens()) {
      sValor = stCaracteresEspeciales.nextToken();
      if (nombreTitular.contains(sValor)) {
        if (errorNombre.isEmpty()) {
          errorNombre = "El nombre del titular contiene: " + sValor;
        }
        else {
          errorNombre += ", " + sValor;
        }
      }

      if (apellido1.contains(sValor)) {
        if (errorPrimerApellido.isEmpty()) {
          errorPrimerApellido = " El primer apellido contiene: " + sValor;
        }
        else {
          errorPrimerApellido += ", " + sValor;
        }
      }

      if (apellido2.contains(sValor)) {
        if (errorSegundoApellido.isEmpty()) {
          errorSegundoApellido = " El primer apellido contiene: " + sValor;
        }
        else {
          errorSegundoApellido += ", " + sValor;
        }
      }
    }

    resultado = errorNombre.concat(errorPrimerApellido).concat(errorSegundoApellido);

    return resultado;
  }

  private static String eliminarTitulos(String nombreTitular) {

    String[] nombreSeparado = nombreTitular.split("\\s+");
    StringBuilder nombreFormateado = new StringBuilder();
    boolean found = false;

    // Si el split es mayor que 1, es porque tiene un titulo o es nombre
    // compuesto.
    if (nombreSeparado.length > 1) {
      // Recorro el split del nombre del titular y compruebo si alguno de ellos
      // esta en el string de titulos. sí lo esta, lo elimino.
      for (int x = 0; x < nombreSeparado.length; x++) {
        found = TITULOS.contains(nombreSeparado[x].toUpperCase());

        if (found) {
          nombreSeparado[x] = "";
        }
      }

      for (int y = 0; y < nombreSeparado.length; y++) {
        if (!nombreSeparado[y].isEmpty() && !(nombreFormateado.length() > 0)) {
          return nombreFormateado.append(nombreSeparado[y]).toString();
        }
      }
    }
    else {
      nombreFormateado.append(nombreTitular).toString();
    }

    return nombreFormateado.toString();
  }

  // Cambios función eliminarPrefijos
  private static String deletePrefixes(String apellido) {

    String[] nombreSeparado = apellido.split("\\s+");
    boolean found = false;

    // escapar y agregar limites de palabra completa - case-insensitive
    Pattern regex;
    Matcher match;
    int cantidadPalabrasPrefijo = 0;
    String[] cantidadPalabrasPrefijoLista;

    if (nombreSeparado.length > 0) {

      for (int x = 0; x < PREFIJOS.length; x++) {
        regex = Pattern.compile(".*\\b" + Pattern.quote(PREFIJOS[x]) + "\\b.*", Pattern.CASE_INSENSITIVE);
        match = regex.matcher(apellido);

        // cuanto cuantas palabras contiene el prefijo del apellido, para
        // comparar sí,
        // la cantidad de palabras del prefijo del apellido son igual que el de
        // la lista.
        // ej. van der rohe -> coincide con "van" y esto no me vale, tiene que
        // ser la
        // palabra completa "van der" por eso comparo longitudes. Y por eso hago
        // la
        // comprobación dentro del match.find porque si entra es porque la
        // encontró.
        cantidadPalabrasPrefijo = nombreSeparado.length - 1;

        if (match.find()) {
          cantidadPalabrasPrefijoLista = PREFIJOS[x].split("\\s+");
          if (cantidadPalabrasPrefijo == cantidadPalabrasPrefijoLista.length) {
            found = true;
          }
        }
      }

      if (found) {
        return nombreSeparado[nombreSeparado.length - 1];
      }
    }
    return apellido;

  }

  private static String translitarTexto(String cadena) {

    Character.UnicodeBlock block;

    char[] chars = cadena.toCharArray();
    String cadenaFormateada = "";
    String textoTransliterado = "";

    for (int i = 0; i < chars.length; i++) {
      cadenaFormateada = new String(new char[] { chars[i] });
      block = UnicodeBlock.of(chars[i]);
      cadenaFormateada = checkUnicode(block, cadenaFormateada);
      textoTransliterado += Normalizer.normalize(cadenaFormateada, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    return textoTransliterado.replaceAll("[^a-zA-Z]", "");
  }

  private static String checkUnicode(UnicodeBlock block, String cadenaFormateada) {

    switch (block.toString()) {
      case "LATIN_1_SUPPLEMENT":

        if (LATIN1_SUPLEMENT_A.contains(cadenaFormateada)) {
          cadenaFormateada = "A";
        }
        else if (LATIN1_SUPLEMENT_D.contains(cadenaFormateada)) {
          cadenaFormateada = "D";
        }
        else if (LATIN1_SUPLEMENT_O.contains(cadenaFormateada)) {
          cadenaFormateada = "O";
        }
        else if (LATIN1_SUPLEMENT_S.contains(cadenaFormateada)) {
          cadenaFormateada = "S";
        }
        else if (LATIN1_SUPLEMENT_T.contains(cadenaFormateada)) {
          cadenaFormateada = "T";
        }

        break;

      case "LATIN_EXTENDED_A":

        if (LATIN_EXTENDED_D.contains(cadenaFormateada)) {
          cadenaFormateada = "A";
        }
        else if (LATIN_EXTENDED_I.contains(cadenaFormateada)) {
          cadenaFormateada = "I";
        }
        else if (LATIN_EXTENDED_L.contains(cadenaFormateada)) {
          cadenaFormateada = "L";
        }
        break;
    }

    return cadenaFormateada;
  }

  private String concatEverything(String nombreTitular, String apellido1, String apellido2, Date fechaNac,
      String codNacionalidad, ConcatDTO resultado) {

    String concat = "";
    concat = findCountryByISO(resultado, codNacionalidad);
    if (!concat.isEmpty() || resultado.getsError() > 0) {
      concat += composeDate(fechaNac);

      concat += composeName(nombreTitular);

      concat += composeSurname(apellido1);

      resultado.setsError(1);
      // concat += componerApellido(apellido2); se debe hacer solo el del
      // primer apellido o los 2?
    }
    else {
      return resultado.getsConcat();
    }

    return concat.toUpperCase();
  }

  private static String composeName(String nombreTitular) {

    String[] nombreCompuesto = nombreTitular.split("\\s+");

    if (nombreCompuesto.length > 1) {
      nombreTitular = nombreCompuesto[nombreCompuesto.length - 1];
    }
    else if (nombreTitular.length() < 5) {
      switch (nombreTitular.length()) {
        case 3:
          nombreTitular += "##";
          break;
        case 4:
          nombreTitular += "#";
          break;
      }
    }
    else if (nombreTitular.length() > 5) {
      nombreTitular = nombreTitular.substring(0, 5);
    }

    return nombreTitular;
  }

  private static String composeSurname(String apellido) {

    if (apellido.length() < 5) {
      switch (apellido.length()) {
        case 3:
          apellido += "##";
          break;
        case 4:
          apellido += "#";
          break;
      }
    }
    else if (apellido.length() > 5) {
      apellido = apellido.substring(0, 5);
    }
    return apellido;
  }

  private static String composeDate(Date fechaNac) {

    if (fechaNac != null) {
      String fechaNacFormateada = "";
      fechaNacFormateada = FormatDataUtils.convertDateToString(fechaNac, FormatDataUtils.DATE_FORMAT);

      return fechaNacFormateada;
    }
    else {
      return "";
    }
  }

  private String findCountryByISO(ConcatDTO resultado, String codNacionalidad) {

    Tmct0paises pais = tmct0paisesDao.findByisoNumActivo(String.valueOf(codNacionalidad));
    String codIsoPais = "";
    // Sí el país no existe informamos del error
    if (pais == null) {
      resultado.setsError(0);
      resultado.setsConcat("El código del país no es correcto o está dado de baja.");
    }
    else {
      if (!pais.getId().getCdisoalf2().matches("[a-zA-Z]+")) {
        resultado.setsError(0);
        resultado.setsConcat("El código ISO Alfanúmerico no es correcto.");
      }
      else {
        codIsoPais = pais.getId().getCdisoalf2();
        return codIsoPais;
      }
    }

    return codIsoPais;
  }
}
