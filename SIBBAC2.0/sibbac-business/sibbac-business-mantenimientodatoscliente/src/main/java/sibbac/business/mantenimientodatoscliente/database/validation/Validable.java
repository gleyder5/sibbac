package sibbac.business.mantenimientodatoscliente.database.validation;

import sibbac.common.SIBBACBusinessException;

/**
 * Design pattern command interface
 * @author NEORIS
 *
 */
public interface Validable {

	/**
	 * Validate command executor
	 * @throws SIBBACBusinessException 
	 */
	public void validate() throws SIBBACBusinessException;
}
