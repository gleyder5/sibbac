package sibbac.business.mantenimientodatoscliente.tasks;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0MifidBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.business.wrappers.util.FicherosUtils;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_MANTENIMIENTO.NAME, name = Task.GROUP_MANTENIMIENTO.JOB_CARGA_MIFID_MULTI, interval = 1, intervalUnit = IntervalUnit.DAY)
@DisallowConcurrentExecution
public class TaskCargaMIFIDImproved extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOG = LoggerFactory.getLogger(TaskCargaMIFIDImproved.class);

  @Autowired
  private CargaMifidMultithread cargaMifidMultithread;

  @Autowired
  private Tmct0MifidBo tmct0MifidBo;

  @Autowired
  private Tmct0menBo tmct0menBo;

  @Value("${partenon.mantenimiento.idoneidad.fisica}")
  private String cuestionarioIdoneidadFisica;

  @Value("${partenon.mantenimiento.conveniencia.fisica}")
  private String cuestionarioConvenienciaFisica;

  @Value("${partenon.mantenimiento.idoneidad.juridica}")
  private String cuestionarioIdoneidadJuridica;

  @Value("${partenon.mantenimiento.conveniencia.juridica}")
  private String cuestionarioConvenienciaJuridica;

  @Value("${ruta_servidor.paternon.in.mifid}")
  private String path;

  private Integer lineasLeidasMIFIDTest = 0;

  @Override
  public void executeTask() throws Exception {
    final File dir = new File(path + File.separator);
    cargaFicheroMifid(dir);

    if (lineasLeidasMIFIDTest > 0) {
      actualizaCaducidades();
      borrarTestsMifid();
    }

  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_APUNTE.TASK_MANTENIMIENTO_MIFID;
  }

  private void cargaFicheroMifid(File dir) {
    final List<File> toTratados;
    final File[] foundFiles;
    List<String> lines;
    String fileName = null;

    try {
      foundFiles = dir.listFiles(new MifidFilter());
      LOG.debug("[{}::{}] Encontrados {} que empiecen con 'MIFID'", Task.GROUP_MANTENIMIENTO.NAME,
          Task.GROUP_MANTENIMIENTO.JOB_CARGA_MIFID, foundFiles.length);
      if (foundFiles.length > 0) {
        toTratados = new ArrayList<File>();
        for (File file : foundFiles) {
          fileName = file.getName();
          if (fileName.startsWith("MIFIDCUESTIONARI")) {
            lines = FileUtils.readLines(file, StandardCharsets.ISO_8859_1);
            tmct0MifidBo.tratadoMIFIDCuestionario(lines);
            toTratados.add(file);
          }
          else if (fileName.startsWith("MIFIDTEST")) {
            lineasLeidasMIFIDTest = cargaMifidMultithread.cargaFicheroMultithread(file);
            toTratados.add(file);
          }
          else if (fileName.startsWith("MIFIDCATEGORIA")) {
            lines = FileUtils.readLines(file, StandardCharsets.ISO_8859_1);
            tmct0MifidBo.tratadoMIFIDCategoria(lines);
            toTratados.add(file);
          }
        }
        FicherosUtils.filesToTratados(toTratados);
      }
    }
    catch (IOException e) {
      LOG.error("[cargaFicheroMifid] No se ha podido abrir el fichero: {} {}", dir, fileName, e);
    }
    catch (SIBBACBusinessException sbe) {
      LOG.error("[cargaFicheroMifid] Error en carga de fichero {} {}", dir, fileName, sbe);
    }
    catch (RuntimeException rex) {
      LOG.error("[cargaFicheroMifid] Error no controlado leyendo el fichero {} {}", dir, fileName, rex);
    }
  }

  private void actualizaCaducidades() throws SIBBACBusinessException {
    int c = 0;

    LOG.debug("[actualizaCaducidades] Inicio");

    c += cargaMifidMultithread.actualizarFechasConveniencia(cuestionarioConvenienciaFisica, "F");
    c += cargaMifidMultithread.actualizarFechasConveniencia(cuestionarioConvenienciaJuridica, "J");
    c += cargaMifidMultithread.actualizarFechasIdoneidad(cuestionarioIdoneidadFisica, "F");
    c += cargaMifidMultithread.actualizarFechasIdoneidad(cuestionarioIdoneidadJuridica, "J");

    LOG.debug("[actualizaCaducidades] Fin con {} actualizaciones", c);
  }

  private void borrarTestsMifid() {
    LOG.debug("[borrarTestsMifid] Entramos en borrarTestsMifid");
    cargaMifidMultithread.borrarTestsMifid();
    LOG.debug("[borrarTestsMifid] Salimos de borrarTestsMifid");
  }

  private class MifidFilter implements FilenameFilter {

    public boolean accept(File dir, String name) {
      return name.startsWith("MIFID");
    }
  }
}
