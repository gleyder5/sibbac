package sibbac.business.mantenimientodatoscliente.rest;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import sibbac.business.fase0.database.bo.Tval0vpbBo;
import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0CliMifidBo;
import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.business.mantenimientodatoscliente.database.validation.ClienteValidator;
import sibbac.business.mantenimientodatoscliente.database.validation.FinalHoldersValidateAddress;
import sibbac.business.pbcdmo.bo.BlanqueoClientesBo;
import sibbac.business.pbcdmo.db.dao.BlanqueoClientesDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoObservacionDAO;
import sibbac.business.pbcdmo.db.model.BlanqueoClientesEntity;
import sibbac.business.pbcdmo.web.dto.BlanqueoClientesDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.bo.ContactoBo;
import sibbac.business.wrappers.database.bo.Tmct0ClientBo;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;
import sibbac.business.wrappers.database.dao.CodigoErrorDao;
import sibbac.business.wrappers.database.dao.ContactoDao;
import sibbac.business.wrappers.database.dao.Tmct0SelectsDao;
import sibbac.business.wrappers.database.dao.Tmct0cliDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0cliHistoricalDao;
import sibbac.business.wrappers.database.dao.Tmct0cliHistoricalDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.dao.Tmct0provinciasDao;
import sibbac.business.wrappers.database.dao.Tmct0swiDao;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.dto.ClienteDTO;
import sibbac.business.wrappers.database.dto.assembler.Tmct0ClientAssembler;
import sibbac.business.wrappers.database.dto.assembler.Tmct0ContactoAssembler;
import sibbac.business.wrappers.database.dto.assembler.Tmct0cliHistoricalAssembler;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Tmct0Address;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;
import sibbac.business.wrappers.database.model.Tmct0Selects;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cliHistorical;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0provincias;
import sibbac.business.wrappers.database.model.Tmct0swi;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bsnvasql.model.Tval0vpb;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceGestionClientes implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGestionClientes.class);

  private static DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  private Tmct0SelectsDao tmct0SelectsDao;

  @Autowired
  private Tval0vpbBo tpdomiciBo;

  @Autowired
  Tmct0cliBo clienteBo;

  @Autowired
  ContactoBo contactoBo;

  @Autowired
  Tmct0CliMifidBo mifidBo;

  @Autowired
  Tmct0ClientBo tmct0ClientBo;

  @Autowired
  Tmct0ClientAssembler tmct0ClientAssembler;

  @Autowired
  Tmct0ContactoAssembler tmct0ContactoAssembler;

  @Autowired
  Tmct0cliHistoricalAssembler tmct0cliHistoricalAssembler;

  @Autowired
  Tmct0cliDaoImp tmct0cliDaoImp;

  @Autowired
  ContactoDao contactoDao;

  @Autowired
  private CodigoErrorDao codigoErrorDao;

  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  @Autowired
  private Tmct0provinciasDao tmct0provinciasDao;

  @Autowired
  private Tmct0swiDao tmct0swiDao;

  @Autowired
  private Tmct0cliHistoricalDao tmct0cliHistoricalDao;

  @Autowired
  private Tmct0cliHistoricalDaoImp tmct0cliHistoricalDaoImp;

  @Autowired
  private BlanqueoClientesDAO blanqueoClientesDAO;

  @Autowired
  private BlanqueoObservacionDAO blanqueoObservacionDAO;

  @Autowired
  private Tmct0aliDao alidao;

  @Autowired
  private BlanqueoClientesBo cliBo;

  /** Recupera los profiles para cargar los datos del filtro */
  private static final String BUSQUEDA_CLIENTES = "BusquedaClientes";

  /** Recupera la lista de usuarios */
  private static final String BUSQUEDA_CONTACTOS = "BusquedaContactos";

  /** Recupera la lista de acciones */
  private static final String BUSQUEDA_MIFIDS = "BusquedaMifids";

  /** Crea cliente */
  private static final String CREAR_CLIENTE = "CrearCliente";

  /** Recupera lista de clientes. */
  private static final String CARGAR_CLIENTES = "CargarClientes";

  /** Modifica el cliente */
  private static final String MODIFICAR_CLIENTE = "ModificarCliente";

  /** Modifica el contacto */
  private static final String MODIFICAR_CONTACTO = "ModificarContacto";

  /** Eliminar cliente */
  private static final String ELIMINAR_CLIENTE = "EliminarCliente";

  /** Eliminar contacto */
  private static final String ELIMINAR_CONTACTO = "EliminarContacto";

  /** Recuperar cliente */
  private static final String RECUPERAR_CLIENTE = "RecuperarCliente";

  /** Ver historico cliente */
  private static final String VER_HISTORICO_CLIENTE = "VerHistoricoCliente";

  /** Exportar clientes Excel */
  private static final String EXPORTAR_CLIENTES_XLS = "ExportaClientesXLS";

  // CLIENTES
  private static final String FILTER_ID_CLIENTE = "idCliente";
  private static final String FILTER_TIPO_DOCUMENTO = "tipoDocumento";
  private static final String FILTER_NUMERO_DOCUMENTO = "numDocumento";
  private static final String FILTER_TIPO_RESIDENCIA = "tipoResidencia";
  private static final String FILTER_CODIGO_PERSONA_BDP = "codPersonaBDP";
  private static final String FILTER_NUMERO_FACTURA = "numeroFactura";
  private static final String RESULT_TIPO_CLIENTE = "tipoCliente";
  private static final String RESULT_NATURALEZA_CLIENTE = "naturalezaCliente";
  private static final String RESULT_ESTADO_CLIENTE = "estadoCliente";
  private static final String RESULT_FECHA_ALTA = "fechaAlta";
  private static final String RESULT_FECHA_BAJA = "fechaBaja";
  private static final String RESULT_CODIGO_KGR = "codigoKGR";
  private static final String RESULT_TIPO_NACIONALIDAD = "tipoNacionalidad";
  private static final String RESULT_CODIGO_PAIS = "codigoPais";
  private static final String RESULT_CATEGORIZACION = "categorizacion";
  private static final String RESULT_CONTROL_PBC = "controlPBC";
  private static final String RESULT_RIESGO_PBC = "riesgoPBC";
  private static final String RESULT_FH_REV_RIESGO_PBC = "fhRevRiesgoPbc";
  private static final String RESULT_MOTIVO_PBC = "motivoPBC";

  // CONTACTOS
  private static final String FILTER_NOMBRE = "nombre";
  private static final String FILTER_POSICION = "posicion";
  private static final String FILTER_TELEFONO = "telefono";
  private static final String FILTER_EMAIL = "email";
  private static final String FILTER_FAX = "fax";
  private static final String FILTER_DIRECCION = "direccion";

  // MIFIDS
  private static final String FILTER_CATEGORIA = "categoria";
  private static final String FILTER_TEST_CONVENIENCIA = "testConveniencia";
  private static final String FILTER_TEST_IDONEIDAD = "testIdoneidad";
  private static final String FILTER_FECHA_FIN_VIG_CONVENIEN = "fechaFinVigConveniencia";
  private static final String FILTER_FECHA_FIN_VIG_IDONEIDAD = "fechaFinVigIdoneidad";

  private static Integer HEADERS_SIZE = 42;

  private static final String NOT = "Not";

  private Map<String, Object> catalogos = new HashMap<String, Object>();

  /**
   * The Enum Campos plantilla .
   */
  private enum FieldsPlantilla {
    PLANTILLA("plantilla");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsPlantilla(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * sibbac.webapp.business.SIBBACServiceGestionClientes#process(sibbac.webapp
   * .beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACServiceGestionClientes the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {

    final WebResponse webResponse = new WebResponse();

    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    String command = webRequest.getAction();
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    Map<String, String> paramsFilters = webRequest.getFilters();

    try {
      LOG.debug("Entro al servicio GESTION DE CLIENTES");

      switch (command) {
        case BUSQUEDA_CLIENTES:
          result.setResultados(buscarClientes(webRequest, webResponse));
          break;
        case BUSQUEDA_CONTACTOS:
          result.setResultados(buscarContactos(webRequest, webResponse));
          break;
        case BUSQUEDA_MIFIDS:
          result.setResultados(buscarMifids(webRequest, webResponse));
          break;
        // case RECUPERAR_PAGINAS_ACCIONES:
        // result.setResultados(cargarPaginasAcciones());
        // break;
        //
        // case RECUPERAR_PERFILES_FILTRO:
        // result.setResultados(getPerfilesFiltro(paramsObjects));
        // break;
        case CREAR_CLIENTE:
          result.setResultados(crearCliente(paramsObjects));
          break;
        case CARGAR_CLIENTES:
          result.setResultados(this.cargarClientes(paramsFilters));
          break;
        case MODIFICAR_CLIENTE:
          result.setResultados(modificarCliente(paramsObjects));
          break;
        case MODIFICAR_CONTACTO:
          result.setResultados(modificarContacto(paramsObjects));
          break;
        case ELIMINAR_CLIENTE:
          result.setResultados(eliminarClientes(paramsObjects));
          break;
        case ELIMINAR_CONTACTO:
          result.setResultados(eliminarContacto(paramsObjects));
          break;
        case RECUPERAR_CLIENTE:
          result.setResultados(recuperarCliente(paramsObjects));
          break;
        case VER_HISTORICO_CLIENTE:
          result.setResultados(this.verHistoricoCliente(paramsObjects));
          break;
        case EXPORTAR_CLIENTES_XLS:
          result.setResultados(exportXLSClientes(webRequest, webResponse));
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    }
    catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      LOG.error("Error al ejecutar comando", e);
      result.setResultados(resultados);
      result.setError(e.getMessage());
    }
    catch (Exception e) {
      resultados.put("status", "KO");
      LOG.error("Error al ejecutar comando", e);
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceGestionClientes");

    return result;
  }

  /**
   * Obtencion de los cleintes actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> buscarClientes(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException {

    LOG.trace("Inicio la busqueda de Clientes");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      final Map<String, String> filtros = webRequest.getFilters();

      final Long idCliente = getIdCliente(filtros.get(FILTER_ID_CLIENTE));
      final String tipoDocumento = filtros.get(FILTER_TIPO_DOCUMENTO);
      final String numDocumento = filtros.get(FILTER_NUMERO_DOCUMENTO);
      final String tipoResidencia = filtros.get(FILTER_TIPO_RESIDENCIA);
      final String codPersonaBDP = filtros.get(FILTER_CODIGO_PERSONA_BDP);
      final String numeroFactura = filtros.get(FILTER_NUMERO_FACTURA);
      final String tipoCliente = filtros.get(RESULT_TIPO_CLIENTE);
      final String naturalezaCliente = filtros.get(RESULT_NATURALEZA_CLIENTE);
      final String estadoCliente = filtros.get(RESULT_ESTADO_CLIENTE);
      final Date fechaAlta = convertirStringADate(filtros.get(RESULT_FECHA_ALTA));
      final Date fechaBaja = convertirStringADate(filtros.get(RESULT_FECHA_BAJA));
      final String codigoKGR = filtros.get(RESULT_CODIGO_KGR);
      final String tipoNacionalidad = filtros.get(RESULT_TIPO_NACIONALIDAD);
      final String codigoPais = filtros.get(RESULT_CODIGO_PAIS);
      final String categorizacion = filtros.get(RESULT_CATEGORIZACION);
      final String controlPBC = filtros.get(RESULT_CONTROL_PBC);
      final String motivoPBC = filtros.get(RESULT_MOTIVO_PBC);
      final String riesgoPBC = filtros.get(RESULT_RIESGO_PBC);
      final Date fhRevRiesgoPbc = convertirStringADate(filtros.get(RESULT_FH_REV_RIESGO_PBC));

      final Long idClienteNot = getIdCliente(filtros.get(FILTER_ID_CLIENTE + NOT));
      final String tipoDocumentoNot = filtros.get(FILTER_TIPO_DOCUMENTO + NOT);
      final String numDocumentoNot = filtros.get(FILTER_NUMERO_DOCUMENTO + NOT);
      final String tipoResidenciaNot = filtros.get(FILTER_TIPO_RESIDENCIA + NOT);
      final String codPersonaBDPNot = filtros.get(FILTER_CODIGO_PERSONA_BDP + NOT);
      final String numeroFacturaNot = filtros.get(FILTER_NUMERO_FACTURA + NOT);
      final String tipoClienteNot = filtros.get(RESULT_TIPO_CLIENTE + NOT);
      final String naturalezaClienteNot = filtros.get(RESULT_NATURALEZA_CLIENTE + NOT);
      final String estadoClienteNot = filtros.get(RESULT_ESTADO_CLIENTE + NOT);
      final String codigoKGRNot = filtros.get(RESULT_CODIGO_KGR + NOT);
      final String tipoNacionalidadNot = filtros.get(RESULT_TIPO_NACIONALIDAD + NOT);
      final String codigoPaisNot = filtros.get(RESULT_CODIGO_PAIS + NOT);
      final String categorizacionNot = filtros.get(RESULT_CATEGORIZACION + NOT);
      final String controlPBCNot = filtros.get(RESULT_CONTROL_PBC + NOT);
      final String motivoPBCNot = filtros.get(RESULT_MOTIVO_PBC + NOT);
      final String riesgoPBCNot = filtros.get(RESULT_RIESGO_PBC + NOT);

      List<ClienteDTO> clientes = clienteBo.findClientesFiltro(idCliente, tipoDocumento, numDocumento, tipoResidencia,
          codPersonaBDP, numeroFactura, tipoCliente, naturalezaCliente, estadoCliente, fechaAlta, fechaBaja, codigoKGR,
          tipoNacionalidad, codigoPais, categorizacion, controlPBC, motivoPBC, riesgoPBC, fhRevRiesgoPbc, idClienteNot,
          tipoDocumentoNot, numDocumentoNot, tipoResidenciaNot, codPersonaBDPNot, numeroFacturaNot, tipoClienteNot,
          naturalezaClienteNot, estadoClienteNot, codigoKGRNot, tipoNacionalidadNot, codigoPaisNot, categorizacionNot,
          controlPBCNot, motivoPBCNot, riesgoPBCNot);

      mSalida.put("listaClientes", clientes);

    }
    catch (Exception e) {
      LOG.error("Error metodo init - buscarClientes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de buscarClientes", e);
    }

    LOG.trace("Fin recuperación datos para buscarClientes");

    return mSalida;
  }

  /**
   * Obtencion de los cleintes actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> buscarMifids(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException {

    LOG.trace("Inicio la busqueda de Mifids");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      final Map<String, String> filtros = webRequest.getFilters();
      // MFG 28/11/2016 como se ha quitado el cliente obligatorio, si que puede
      // venir el filtro nulo y tiene que buscar
      // todo.
      // if (MapUtils.isEmpty(filtros)) {
      // throw new SIBBACBusinessException("No se ha especificado el filtro [" +
      // "Contactos" + "]");
      // }

      final Long idCliente = getIdCliente(filtros.get(FILTER_ID_CLIENTE));
      final String categoria = filtros.get(FILTER_CATEGORIA);
      final String testConv = filtros.get(FILTER_TEST_CONVENIENCIA);
      final String testIdon = filtros.get(FILTER_TEST_IDONEIDAD);
      final String fechaVigConv = filtros.get(FILTER_FECHA_FIN_VIG_CONVENIEN);
      final String fechaVigIdon = filtros.get(FILTER_FECHA_FIN_VIG_IDONEIDAD);

      final String categoriaNot = filtros.get(FILTER_CATEGORIA + NOT);
      final String testConvNot = filtros.get(FILTER_TEST_CONVENIENCIA + NOT);
      final String testIdonNot = filtros.get(FILTER_TEST_IDONEIDAD + NOT);

      Date fechaFinVigConv = null;

      if (fechaVigConv != null && fechaVigConv != "") {
        try {
          fechaFinVigConv = dF.parse(fechaVigConv);
        }
        catch (ParseException e) {
          LOG.error("No valida la fecha", e);
        }

      }

      Date fechaFinVigIdon = null;

      if (fechaVigIdon != null && fechaVigIdon != "") {
        try {
          fechaFinVigIdon = dF.parse(fechaVigIdon);
        }
        catch (ParseException e) {
          LOG.error("No valida la fecha", e);
        }

      }

      List<Tmct0CliMifid> tmct0Profiles = mifidBo.findTmct0CliMifidFiltro(idCliente, categoria, testConv, testIdon,
          fechaFinVigConv, fechaFinVigIdon, categoriaNot, testConvNot, testIdonNot);

      mSalida.put("listaClientes", tmct0Profiles);

    }
    catch (Exception e) {
      LOG.error("Error metodo init - buscarClientes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de buscarClientes");
    }

    LOG.trace("Fin recuperación datos para buscarClientes");

    return mSalida;
  }

  /**
   * Obtencion de los cleintes actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> buscarContactos(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException {

    LOG.trace("Inicio la busqueda de Contactos");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      final Map<String, String> filtros = webRequest.getFilters();

      // MFG 28/11/2016 como se ha quitado el cliente obligatorio, si que puede
      // venir el filtro nulo y tiene que buscar
      // todo.
      // if (MapUtils.isEmpty(filtros)) {
      // throw new SIBBACBusinessException("No se ha especificado el filtro [" +
      // "Contactos" + "]");
      // }

      final Long idCliente = getIdCliente(filtros.get(FILTER_ID_CLIENTE));
      final String nombre = filtros.get(FILTER_NOMBRE);
      final String posicion = filtros.get(FILTER_POSICION);
      final String telefono = filtros.get(FILTER_TELEFONO);
      final String email = filtros.get(FILTER_EMAIL);
      final String fax = filtros.get(FILTER_FAX);
      final String direccion = filtros.get(FILTER_DIRECCION);

      final String nombreNot = filtros.get(FILTER_NOMBRE + NOT);
      final String posicionNot = filtros.get(FILTER_POSICION + NOT);
      final String telefonoNot = filtros.get(FILTER_TELEFONO + NOT);
      final String emailNot = filtros.get(FILTER_EMAIL + NOT);
      final String faxNot = filtros.get(FILTER_FAX + NOT);
      final String direccionNot = filtros.get(FILTER_DIRECCION + NOT);

      Long idAddress = null;
      Long idAddressNot = null;
      if (direccion != null && !direccion.isEmpty()) {
        idAddress = Long.valueOf(direccion);
      }
      if (direccionNot != null && !direccionNot.isEmpty()) {
        idAddress = Long.valueOf(direccionNot);
      }

      List<Contacto> tmct0Profiles = contactoBo.findContactosFiltro(idCliente, nombre, posicion, telefono, email, fax,
          idAddress, nombreNot, posicionNot, telefonoNot, emailNot, faxNot, idAddressNot);

      mSalida.put("listaClientes", tmct0Profiles);

    }
    catch (Exception e) {
      LOG.error("Error metodo init - buscarClientes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de buscarClientes");
    }

    LOG.trace("Fin recuperación datos para buscarClientes");

    return mSalida;
  }

  /**
   * @param filtros
   * @return
   */
  private Date convertirStringADate(final String fechaString) {
    Date fecha = null;
    if (StringUtils.isNotEmpty(fechaString)) {
      fecha = FormatDataUtils.convertStringToDate(fechaString, "dd/MM/yyyy");
    }
    return fecha;
  }

  /**
   * @param idAliasStr
   * @return
   * @throws SIBBACBusinessException
   */
  private Long getIdCliente(final String idClienteStr) throws SIBBACBusinessException {
    Long idAlias = null;
    try {
      if (StringUtils.isNotBlank(idClienteStr)) {
        idAlias = NumberUtils.createLong(idClienteStr);
      }
    }
    catch (NumberFormatException e) {
      throw new SIBBACBusinessException("Error leyendo el valor de \"idClienteStr\": [" + idClienteStr + "] ");
    }
    return idAlias;
  }

  // /**
  // * Obtencion de los profiles actuales
  // *
  // * @return
  // * @throws SIBBACBusinessException
  // */
  // private Map<String, Object> cargarPerfiles() throws
  // SIBBACBusinessException {
  //
  // LOG.trace("Iniciando datos para la lista cargarPerfiles");
  //
  // Map<String, Object> mSalida = new HashMap<String, Object>();
  //
  // try {
  //
  // List<SelectDTO> listaPerfiles = new ArrayList<SelectDTO>();
  // SelectDTO select = null;
  //
  // List<Tmct0Profiles> tmct0Profiles = tmct0ProfilesDao.findAll();
  // for (Tmct0Profiles tmct0Profile : tmct0Profiles) {
  // select = new SelectDTO(String.valueOf(tmct0Profile.getIdProfile()),
  // (tmct0Profile.getName()) + " "
  // + tmct0Profile.getDescription());
  // listaPerfiles.add(select);
  // }
  // mSalida.put("listaPerfiles", listaPerfiles);
  //
  // } catch (Exception e) {
  // LOG.error("Error metodo init - cargarPerfiles " + e.getMessage(), e);
  // throw new SIBBACBusinessException("Error en la carga de datos iniciales
  // de las opciones de busqueda");
  // }
  //
  // LOG.trace("Fin recuperación datos para la lista cargarPerfiles");
  //
  // return mSalida;
  // }

  /**
   * Obtencion de los clientes actuales
   * 
   * @param paramsFilters paramsFilters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarClientes(Map<String, String> paramsFilters) throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarClientes");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaClientes = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Object[]> tmct0Clients = null;

      if (paramsFilters != null && paramsFilters.get("descripCliente") != null) {
        tmct0Clients = this.tmct0cliDaoImp.findClientsByDescription(paramsFilters.get("descripCliente").toUpperCase());
      }
      else {
        tmct0Clients = this.tmct0cliDaoImp.findAllProyection();
      }

      if (CollectionUtils.isNotEmpty(tmct0Clients)) {
        for (Object[] tmct0Client : tmct0Clients) {
          select = new SelectDTO(String.valueOf(tmct0Client[0]), ((String) tmct0Client[1]).toUpperCase());
          listaClientes.add(select);
        }
      }
      mSalida.put("listaClientes", listaClientes);

    }
    catch (Exception e) {
      LOG.error("Error metodo init - cargarClientes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.trace("Fin recuperación datos para la lista cargarClientes");

    return mSalida;
  }

  // /**
  // * Obtencion de la lista de acciones actuales
  // *
  // * @return
  // * @throws SIBBACBusinessException
  // */
  // private Map<String, Object> cargarAcciones() throws
  // SIBBACBusinessException {
  //
  // LOG.trace("Iniciando datos para la lista cargarAcciones");
  //
  // Map<String, Object> mSalida = new HashMap<String, Object>();
  //
  // try {
  //
  // List<SelectDTO> listaAcciones = new ArrayList<SelectDTO>();
  // SelectDTO select = null;
  //
  // List<Tmct0Actions> listaAccionesActuales =
  // tmct0ActionDao.findActionsOrderByOrderAction();
  // for (Tmct0Actions tmct0Action : listaAccionesActuales) {
  // select = new SelectDTO(String.valueOf(tmct0Action.getIdAction()),
  // tmct0Action.getName(),
  // tmct0Action.getDescription());
  // listaAcciones.add(select);
  // }
  // mSalida.put("listaAcciones", listaAcciones);
  //
  // } catch (Exception e) {
  // LOG.error("Error metodo init - cargarUsuarios " + e.getMessage(), e);
  // throw new SIBBACBusinessException("Error en la carga de datos iniciales -
  // cargarAcciones");
  // }
  //
  // LOG.trace("Fin recuperación datos para la lista cargarAcciones");
  //
  // return mSalida;
  // }

  // /**
  // * Obtencion de la lista de paginas con las acciones que se pueden hacer
  // en cada una de ellas - Necesaria para poder
  // * asignar a un rol nuevo las acciones sobre las paginas.
  // *
  // * @return
  // * @throws SIBBACBusinessException
  // */
  // private Map<String, Object> cargarPaginasAcciones() throws
  // SIBBACBusinessException {
  //
  // LOG.trace("Iniciando datos para la lista cargarPaginasAcciones");
  //
  // Map<String, Object> mSalida = new HashMap<String, Object>();
  //
  // try {
  //
  // List<Tmct0PagesProfileDTO> listaPaginasAcciones = new
  // ArrayList<Tmct0PagesProfileDTO>();
  //
  // List<Tmct0Pages> listaPaginas = tmct0PagesDao.getPagesWithUrl();
  //
  // for (Tmct0Pages tmct0Pages : listaPaginas) {
  //
  // Tmct0PagesProfileDTO paginaProfileDTO = new Tmct0PagesProfileDTO();
  // List<Tmct0ActionPageProfileDTO> listaActionPageProfileDTO = new
  // ArrayList<Tmct0ActionPageProfileDTO>();
  //
  // paginaProfileDTO.setIdPage(tmct0Pages.getIdPage());
  // // paginaProfileDTO.setIdProfile(datosProfile.getIdProfile());
  // paginaProfileDTO.setName(tmct0Pages.getName());
  // paginaProfileDTO.setDescription(tmct0Pages.getDescription());
  //
  // // Se recuperan todas las acciones y si la pagina la contiene o no, si el
  // idactionpage es 0 no la contiene
  // List<Object[]> listaAccionesPagina =
  // tmct0ActionPageDao.getActionsByPage(tmct0Pages.getIdPage());
  //
  // for (Object[] tmct0ActionPage : listaAccionesPagina) {
  // Tmct0ActionPageProfileDTO actionPageProfileDTO = new
  // Tmct0ActionPageProfileDTO();
  //
  // BigInteger idActionPage = (BigInteger) tmct0ActionPage[0];
  // BigInteger idAction = (BigInteger) tmct0ActionPage[1];
  //
  // actionPageProfileDTO.setIdAction(idAction.longValue());
  // actionPageProfileDTO.setIdActionPage(idActionPage.longValue());
  // actionPageProfileDTO.setbAplicaProfile(false); // Esta estructura es
  // generia para que el usuario pueda empezar
  // // asignar los permisos.
  //
  // listaActionPageProfileDTO.add(actionPageProfileDTO);
  //
  // }
  //
  // paginaProfileDTO.setListaAccionesPaginaProfile(listaActionPageProfileDTO);
  //
  // listaPaginasAcciones.add(paginaProfileDTO);
  //
  // }
  // mSalida.put("listaPaginasAcciones", listaPaginasAcciones);
  //
  // } catch (Exception e) {
  // LOG.error("Error metodo init - cargarPaginasAcciones " + e.getMessage(),
  // e);
  // throw new SIBBACBusinessException("Error en la carga de datos iniciales -
  // cargarPaginasAcciones");
  // }
  //
  // LOG.trace("Fin recuperación datos para la lista cargarPaginasAcciones");
  //
  // return mSalida;
  // }

  // /**
  // * Obtencion de todas las perfiles actuales que cumplen el filtro para la
  // carga del grid inicial
  // *
  // * @param filters
  // * @return
  // * @throws SIBBACBusinessException
  // */
  // private Map<String, Object> getPerfilesFiltro(Map<String, Object>
  // paramsObjects) throws SIBBACBusinessException {
  //
  // LOG.debug("Entra en getPerfilesFiltro ");
  //
  // Map<String, Object> result = new HashMap<String, Object>();
  // List<Object> listaPerfilessFiltroDto = new ArrayList<Object>();
  //
  // try {
  //
  // // Se recuperan los perfiles que cumplen con los criterios establecidos
  //
  // Tmct0PerfilFilterDTO filters =
  // tmct0PerfilAssembler.getTmct0PerfilFilterDTO(paramsObjects);
  //
  // List<Tmct0Profiles> listaPerfilesFiltro =
  // tmct0PerfilesDaoImp.findPerfilesFiltro(filters);
  //
  // // Se transforman a lista de DTO
  // for (Tmct0Profiles tmct0Perfil : listaPerfilesFiltro) {
  // try {
  // Tmct0ProfilesDTO miDTO =
  // tmct0PerfilAssembler.getTmct0ProfileDTO(tmct0Perfil);
  // listaPerfilessFiltroDto.add(miDTO);
  // } catch (Exception e) {
  // LOG.error("Error metodo getPaginasFiltro - miAssembler.getTmct0InformeDto
  // " + e.getMessage(), e);
  // // TODO: handle exception
  // }
  // }
  //
  // } catch (Exception e) {
  // LOG.error("Error metodo getPaginasFiltro - " + e.getMessage(), e);
  // throw new SIBBACBusinessException("Error en la peticion de datos con las
  // opciones de busqueda seleccionadas");
  // }
  //
  // result.put("listaPerfilesFiltroDto", listaPerfilessFiltroDto);
  //
  // LOG.debug("Fin getPerfilesFiltro");
  //
  // return result;
  //
  // }
  //
  /**
   * Crear el cliente con los datos recibidos como parametro
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> crearCliente(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio crearCliente");
    Map<String, Object> resultado = new HashMap<String, Object>();
    List<String> errores = new ArrayList<String>();

    try {
      // Se convierte el objeto en entidad
      Tmct0cli tmct0Client = tmct0ClientAssembler.getTmct0ClientEntity(paramsObject);
      errores = validarCliente(tmct0Client, new HashMap<String, Boolean>(), false);
      checkOperativa(paramsObject, errores);
      checkKGR(paramsObject, errores);
      if (errores.isEmpty()) {
        // Si Operativa Recurrente = NO, daremos de alta el cliente en la tabla
        // clientes PBC
        if (!tmct0Client.getCdkgr().equalsIgnoreCase("9RED")) {
          saveClientPBC(tmct0Client);
        }
        // Se guarda la información en la tabla TMCT0CLI
        tmct0ClientBo.saveClient(tmct0Client, null);
        resultado.put("errores", null);
        LOG.debug("Cliente creado correctamente");
      }
      else {
        resultado.put("errores", errores);
      }

    }
    catch (Exception e) {
      errores.add("Error al intentar crear el cliente");
      resultado.put("errores", errores);
      LOG.error("Error metodo init - crearCliente " + e.getMessage(), e);
    }
    LOG.debug("Fin crearCliente");
    return resultado;
  }

  private void saveClientPBC(Tmct0cli cliente) {
    if (null != cliente) {
      BlanqueoClientesDTO solicitud = new BlanqueoClientesDTO();
      solicitud.setId(cliente.getCdbrocli());

      // Si la categorizacion es igual a UNBUNDLED, es un cliente research.
      if (cliente.getCdcat().equalsIgnoreCase("Unbundled")) {
        solicitud.setIdControlEfectivo(1);
      }
      else {
        solicitud.setIdControlEfectivo(2);
      }
      solicitud.setIdControlDocumentacion("ND");
      cliBo.actionSave(solicitud);
    }
  }

  /**
   * Elimina los clientes seleccionados
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> eliminarClientes(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio eliminarClientes");
    List<Object> listClientesBorrar = (List<Object>) paramsObject.get("listaBorrar");
    List<String> solicitudList = new ArrayList<>();
    for (Object idClienteBorrarObject : listClientesBorrar) {
      Tmct0cli cliente = clienteBo.findById(new Long((Integer) idClienteBorrarObject));
      Date fhfinal = new Date();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      String strfhfinal = sdf.format(fhfinal);
      cliente.setFhfinal(Integer.valueOf(strfhfinal));
      cliente.setCdestado('B');
      try {
        tmct0ClientBo.saveDeleteClient(cliente);
        solicitudList.add(cliente.getCdbrocli());
      }
      catch (Exception e) {
        LOG.error("Error al borrar el cliente " + idClienteBorrarObject);
        LOG.error(e.getMessage());
        throw new SIBBACBusinessException("No se ha podido borrar el cliente seleccionado - Consulte con informatica ");
      }
    }
    eliminarEnTablaPBC(solicitudList);
    LOG.info("Cliente/s eliminado correctamente");
    return paramsObject;

  }

  private void eliminarEnTablaPBC(List<String> solicitudList) throws SIBBACBusinessException {
    try {
      if (!solicitudList.isEmpty()) {
        cliBo.actionDelete(solicitudList);
      }
    }
    catch (Exception e) {
      LOG.error("Error al borrar los clientes de la tabla PBC");
      LOG.error(e.getMessage());
      throw new SIBBACBusinessException("No se ha podido borrar el/los cliente/s seleccionado/s");
    }
  }

  /**
   * Elimina los clientes seleccionados
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> eliminarContacto(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio eliminarContacto");
    List<Object> listClientesBorrar = (List<Object>) paramsObject.get("listaBorrar");
    String userName = (String) paramsObject.get("userName");
    for (Object idClienteBorrarObject : listClientesBorrar) {
      Long id = new Long((Integer) idClienteBorrarObject);
      Contacto contacto = contactoDao.findById(id);
      if (contacto != null) {

        contacto.setActivo('N');
        contactoBo.save(contacto);
        Tmct0cliHistorical historico = new Tmct0cliHistorical();
        historico.setFhmod(new Date());
        historico.setIdClient(contacto.getIdCliente());
        historico.setUsumod(userName);
        historico.setData("Activo");
        historico.setTpdata("Contacto");
        historico.setValuenew("N");
        historico.setValueprev("S");
        tmct0cliHistoricalDao.save(historico);

      }
      else {
        throw new SIBBACBusinessException(
            "No se ha podido borrar el contacto seleccionado - Consulte con informatica ");
      }
    }
    LOG.info("Contacto eliminado correctamente");
    return paramsObject;

  }

  /**
   * Recupera el cliente
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> recuperarCliente(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio recuperarCliente");

    Tmct0cli cliente = clienteBo.findById(new Long((Integer) paramsObject.get("id")));
    Map<String, Object> mSalida = new HashMap<String, Object>();
    mSalida.put("cliente", cliente);
    return mSalida;

  }

  /**
   * Modificación del cliente pasado como parametro
   *
   * @return
   * @throws SIBBACBusinessException
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> modificarCliente(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio modificarCliente");
    Map<String, Object> resultado = new HashMap<String, Object>();
    List<String> errores = new ArrayList<String>();

    try {

      Map<String, Object> datosCliente = (Map<String, Object>) paramsObject.get("datosCliente");
      Long lidClient = Long.valueOf(datosCliente.get("idClient").toString());
      Tmct0cli clienteOriginal = this.clienteBo.findById(lidClient);

      Tmct0cli clienteParaFechaRev = new Tmct0cli();
      // Se obtiene lista de historico de modificaciones del cliente.
      List<Tmct0cliHistorical> historicosCliente = this.tmct0cliHistoricalAssembler
          .getCambiosHistoricoCliente(clienteOriginal, paramsObject, clienteParaFechaRev);

      Tmct0cli clienteModificado = tmct0ClientAssembler.getTmct0ClientEntity(paramsObject);

      // Si la ha modificado CONTROL PBC o MOTIVO PBC, se habrá cambiado la
      // fecha de revision prevista, por lo que tenemos que asignarle este
      // cambio al cliente modificado
      if (null != clienteParaFechaRev.getFhRevPrev()) {
        clienteModificado.setFhRevPrev(clienteParaFechaRev.getFhRevPrev());
      }

      // Si ha habido cambios sobre CONTROL PBC, actualizaremos el cliente
      // correspondiente en la tabla blanqueo clientes
      if (!historicosCliente.isEmpty()) {
        for (int i = 0; i < historicosCliente.size(); i++) {
          if (historicosCliente.get(i).getData().equalsIgnoreCase("Código KGR")) {
            comprobarCodigoKGR(clienteModificado, historicosCliente.get(i));
          }
          if (historicosCliente.get(i).getData().equalsIgnoreCase("Control PBC")) {
            actualizarClientePBC(clienteModificado, historicosCliente.get(i));
          }
        }
      }

      if (CollectionUtils.isNotEmpty(clienteModificado.getMifid())) {
        for (Tmct0CliMifid mifid : clienteModificado.getMifid()) {
          mifid.setCliente(clienteModificado);
        }
      }

      if (clienteModificado.getAddress() != null) {
        clienteModificado.getAddress().setCliente(clienteModificado);
      }

      if (CollectionUtils.isNotEmpty(clienteModificado.getContactos())) {
        for (Contacto contacto : clienteModificado.getContactos()) {
          contacto.setCliente(clienteModificado);
        }
      }

      errores = validarCliente(clienteModificado, new HashMap<String, Boolean>(), false);

      if (errores.isEmpty()) {
        // Se guarda la información
        tmct0ClientBo.saveClient(clienteModificado, historicosCliente);
        resultado.put("errores", null);
        LOG.debug("Cliente creado correctamente");
      }
      else {
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        resultado.put("errores", errores);
      }

    }
    catch (Exception e) {
      errores.add("Error al intentar modificar el cliente");
      resultado.put("errores", errores);
      LOG.error("Error metodo init - modificarCliente " + e.getMessage(), e);
    }
    LOG.debug("Fin modificarCliente");
    return resultado;

  }

  private void comprobarCodigoKGR(Tmct0cli clienteModificado, Tmct0cliHistorical tmct0cliHistorical) {

    LOG.info("Comprobamos codigo KGR");
    if (null != clienteModificado) {
      BlanqueoClientesEntity entity = cliBo.findById(clienteModificado.getCdbrocli());
      BlanqueoClientesDTO cliente = new BlanqueoClientesDTO();
      ObservacionClienteDTO solicitud = new ObservacionClienteDTO();

      // KGR = 9RED a KGR <> 9RED, damos de alta en cliente PBC
      if (tmct0cliHistorical.getValueprev().equalsIgnoreCase("9RED")
          && !tmct0cliHistorical.getValuenew().equalsIgnoreCase("9RED")) {
        // Ya existe
        if (null != entity) {
          LOG.info(
              "Aunque se ha cambiado el codigo KGR por otro distinto a 9RED, el cliente ya existia en la tabla BlanqueoClientes.");
          // ¿Esta dado de baja logica?
          if (null != entity.getFechaBaja()) {
            // Si tiene la baja logica, lo damos de alta y actualizamos
            cliente.setFechaBaja(null);
          }
          /************************************************************************/
          /** VALORES A DEFINIR */
          cliente.setId(entity.getId());
          cliente.setIdControlEfectivo(entity.getIdControlEfectivo().getId());
          cliente.setIdControlDocumentacion(entity.getIdControlDocumentacion().getId());

          solicitud.setIdObservacion(null);
          solicitud.setCliente(cliente);
          solicitud.getCliente().setActivo(1);
          /************************************************************************/
          cliBo.update(solicitud);
        }
        // No existe
        else {
          LOG.info("Damos de alta el cliente en BlanqueoCliente ya que su codigo KGR ya no es 9RED");
          /************************************************************************/
          /** VALORES A DEFINIR */
          cliente.setId(clienteModificado.getCdbrocli());
          cliente.setIdControlEfectivo(2);
          cliente.setIdControlDocumentacion("ND");

          solicitud.setIdObservacion(null);
          solicitud.setCliente(cliente);
          /************************************************************************/
          cliBo.actionSave(cliente);
        }
      }
      // KGR <> 9RED a KGR =9RED, damos de baja de cliente PBC
      else if (!tmct0cliHistorical.getValueprev().equalsIgnoreCase("9RED")
          && tmct0cliHistorical.getValuenew().equalsIgnoreCase("9RED")) {
        LOG.info("Comprobamos si existe en Cliente PBC para hacerle baja logica.");
        if (null != entity) {
          // Ya Existe
          LOG.info("Existe en BlanqueoCliente, le hacemos baja logica.");
          List<String> solicitudList = new ArrayList<>();
          solicitudList.add(entity.getId());
          cliBo.actionDelete(solicitudList);
        }
        else {
          // No existe
          LOG.info(
              "Aunque su codigo era distinto de 9RED, no estaba creada en Blanqueo Cliente por lo que no se hace baja logica.");
        }
      }
    }
    LOG.info("FIN Comprobación codigo KGR");
  }

  private void actualizarClientePBC(Tmct0cli clienteModificado, Tmct0cliHistorical tmct0cliHistorical) {
    if (null != clienteModificado) {
      BlanqueoClientesDTO cliente = new BlanqueoClientesDTO();

      if (tmct0cliHistorical.getValuenew().equalsIgnoreCase("N")) {
        clienteModificado.setMotiPrev(null);
        clienteModificado.setRiesgoPbc(null);
      }
      cliente.setId(clienteModificado.getCdbrocli());
      cliente.setIdControlEfectivo(2);
      cliente.setIdControlDocumentacion("ND");

      ObservacionClienteDTO solicitud = new ObservacionClienteDTO();
      solicitud.setIdObservacion(null);
      solicitud.setCliente(cliente);

      // Comprobamos si existe o no el cliente para saber si tenemos que
      // guardarlo o actualizarlo
      BlanqueoClientesEntity entity = cliBo.findById(solicitud.getCliente().getId());
      // if (null == entity) {
      // cliBo.actionSave(cliente);
      // }
      // else {
      if (null != entity) {
        solicitud.getCliente().setActivo(entity.getActivo());
        cliBo.update(solicitud);
      }
    }
  }

  /**
   * pasa las validaciones a un titular insertado o modificado
   * 
   * @param dto
   * @param listErroresExcel
   * @param errores
   * @param isModificar
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private List<String> validarCliente(Tmct0cli cliente, Map<String, Boolean> errores, boolean isModificar)
      throws SIBBACBusinessException, ParseException {

    List<String> listErrores = new ArrayList<String>();

    Tmct0paises paisBIC = null;
    Tmct0swi swift = null;
    Tmct0paises pais = null;

    if (cliente.getCdNacTit() != null && !cliente.getCdNacTit().isEmpty()) {
      pais = tmct0paisesDao.findByCdHaciendaActivo(cliente.getCdNacTit());
      if (cliente.getCif() != null && !cliente.getCif().isEmpty() && 'B' == cliente.getTpidenti()) {
        paisBIC = tmct0paisesDao.findBycdisoalf2(cliente.getCif().trim().substring(4, 6));
        swift = tmct0swiDao.findBySwift(cliente.getCif().replaceAll(" ", ""));
      }
    }

    Tmct0provincias provincia = null;

    if (cliente.getAddress().getCdPostal() != null && !cliente.getAddress().getCdPostal().isEmpty()) {
      provincia = tmct0provinciasDao.findBycdcodigo(cliente.getAddress().getCdPostal().substring(0, 2));
    }

    listErrores = ClienteValidator.validateFormat(cliente, pais, paisBIC, swift, provincia, errores, isModificar);

    if (clienteBo.isClienteExistente(cliente)) {
      listErrores.add(ErrorFinalHolderEnum.ERROR_98.getError());
    }

    if (!listErrores.isEmpty()) {
      List<CodigoErrorData> coList = codigoErrorDao.findByCodigoOrderByCodigoDataAsc(listErrores);
      listErrores = new ArrayList<String>();
      for (CodigoErrorData error : coList) {
        listErrores.add(error.getCodigo() + "-" + error.getDescripcion() + ". ");
      }
    }

    return listErrores;

  }

  /**
   * pasa las validaciones a un titular insertado o modificado
   * 
   * @param dto
   * @param listErroresExcel
   * @param errores
   * @param isModificar
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private List<String> validarContacto(Contacto contacto, Map<String, Boolean> errores, boolean isModificar)
      throws SIBBACBusinessException, ParseException {

    List<String> listErrores = new ArrayList<String>();
    Tmct0paises pais = null;

    Tmct0Address address = new Tmct0Address();
    BeanUtils.copyProperties(contacto.getAddress(), address);

    pais = tmct0paisesDao.findByCdHaciendaActivo(address.getCdDePais());

    Tmct0provincias provincia = null;
    if (address.getCdPostal() != null && !address.getCdPostal().isEmpty()) {
      provincia = tmct0provinciasDao.findBycdcodigo(address.getCdPostal().substring(0, 2));
    }

    FinalHoldersValidateAddress validatorAddress = new FinalHoldersValidateAddress(address.getTpDomici(),
        address.getNbDomici(), address.getNbCiudad(), address.getCdDePais(), address.getCdPostal(),
        address.getNbProvin(), address.getNuDomici());
    validatorAddress.setResult(listErrores);
    validatorAddress.validate(pais, provincia);

    if (!listErrores.isEmpty()) {
      List<CodigoErrorData> coList = codigoErrorDao.findByCodigoOrderByCodigoDataAsc(listErrores);
      listErrores = new ArrayList<String>();
      for (CodigoErrorData error : coList) {
        listErrores.add(error.getCodigo() + "-" + error.getDescripcion() + ". ");
      }
    }

    return listErrores;

  }

  @SuppressWarnings("unchecked")
  private Map<String, Object> exportXLSClientes(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException, FileNotFoundException {

    Map<String, Object> resultados = new HashMap<String, Object>();
    List<ClienteDTO> dtos = new ArrayList<ClienteDTO>();
    List<Tmct0cli> tmct0Clientes = new ArrayList<Tmct0cli>();

    Map<String, String> filtros = webRequest.getFilters();

    if (filtros != null) {
      dtos = (List<ClienteDTO>) buscarClientes(webRequest, webResponse).get("listaClientes");
      if (dtos != null && !dtos.isEmpty()) {
        for (ClienteDTO dto : dtos) {
          tmct0Clientes.add(clienteBo.findById(dto.getId()));
        }
      }
    }
    else {
      tmct0Clientes = clienteBo.findAll();
    }

    if (!tmct0Clientes.isEmpty()) {

      initCatalogos();
      ByteArrayOutputStream bos = new ByteArrayOutputStream();

      XSSFWorkbook wb = generarExcelClientes(tmct0Clientes);

      try {
        wb.write(bos);
      }
      catch (IOException e) {
        LOG.error(e.getMessage());
        LOG.error(e.toString());
      }

      resultados.put("ExcelClientes", bos.toByteArray());
    }

    return resultados;
  }

  /**
   * Generar informe dinamico en excel de los filtros pasados (los filtros
   * contienen la plantilla del informe)
   * 
   * @param paramsObjects: Objeto de parametros de tipo
   * getTmct0GeneracionInformeFilterDto generarExcelSimple: Especificación de si
   * se debe generar el excel simple con la información
   * @return
   * @throws SIBBACBusinessException
   */
  private XSSFWorkbook generarExcelClientes(List<Tmct0cli> tmct0Clientes) throws SIBBACBusinessException {

    XSSFWorkbook workbook = new XSSFWorkbook();
    // Estilo de la cabecera
    XSSFCellStyle xssfCellStyleCabecera = workbook.createCellStyle();

    xssfCellStyleCabecera.setFillForegroundColor(new XSSFColor(Color.RED));
    xssfCellStyleCabecera.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

    // Crear la fuente de la cabecera
    XSSFFont xssfFont = workbook.createFont();
    xssfFont.setFontHeightInPoints((short) 8);
    xssfFont.setFontName("ARIAL");
    xssfFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
    xssfFont.setColor(new XSSFColor(Color.WHITE));
    xssfCellStyleCabecera.setFont(xssfFont);

    xssfCellStyleCabecera.setBorderBottom(XSSFCellStyle.BORDER_THIN);
    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    xssfCellStyleCabecera.setBorderRight(XSSFCellStyle.BORDER_THIN);
    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());

    xssfCellStyleCabecera.setAlignment(XSSFCellStyle.ALIGN_CENTER);
    xssfCellStyleCabecera.setVerticalAlignment(XSSFCellStyle.ALIGN_FILL);

    // Hoja con el listado de errores
    XSSFSheet registros = workbook.createSheet("CLIENTES");
    int rowMin = 0;

    // Cabecera
    XSSFRow row = registros.createRow(rowMin++);
    row.setHeight((short) 600);
    XSSFCell cell = null;

    String[] header = getHeaders();
    int indexHeader = 0;

    for (String title : header) {
      cell = row.createCell(indexHeader++);
      cell.setCellValue(title);
      cell.setCellStyle(xssfCellStyleCabecera);
    }

    for (int i = 0; i < tmct0Clientes.size(); i++) {

      int indexContent = 0;
      Tmct0cli tmct0cli = tmct0Clientes.get(i);
      row = registros.createRow(rowMin++);

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getIdCliente());

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getCdbrocli());

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getDescli());

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("tiposDeDocumento", tmct0cli.getTpidenti().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getCif());

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("tiposDeResidencia", tmct0cli.getTpreside().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getCdbdp());

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("tiposDeCliente", tmct0cli.getTpclienn().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("naturalezaDeCliente", tmct0cli.getTpfisjur().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("estadosDeCliente", tmct0cli.getCdestado().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getFhinicio());

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getCdkgr());

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("tiposDeNacionalidad", tmct0cli.getTpNacTit().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("nacionalidades", tmct0cli.getCdNacTit()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("categorizacionesCliente", tmct0cli.getCdcat()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("siNo", tmct0cli.getCddeprev().toString()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("motivosPBC", tmct0cli.getMotiPrev()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getValorDescriptivo("riesgosPBC", tmct0cli.getRiesgoPbc()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(getFormattedDate(tmct0cli.getFhRevRiesgoPbc()));

      cell = row.createCell(indexContent++);
      cell.setCellValue(tmct0cli.getDireccionPrincipal());

      List<Tmct0CliMifid> listMifid = tmct0cli.getMifid();
      for (int x = 0; x < listMifid.size(); x++) {

        Tmct0CliMifid mifidCli = listMifid.get(x);

        cell = row.createCell(indexContent++);
        cell.setCellValue(getValorDescriptivo("categorias", mifidCli.getCategory()));

        cell = row.createCell(indexContent++);
        cell.setCellValue(getValorDescriptivo("testDeConveniencia", mifidCli.gettConven()));

        cell = row.createCell(indexContent++);
        cell.setCellValue(getValorDescriptivo("testDeIdoneidad", mifidCli.gettIdoneid()));

        cell = row.createCell(indexContent++);
        cell.setCellValue(getFormattedDate(mifidCli.getFhConv()));

        cell = row.createCell(indexContent++);
        cell.setCellValue(getFormattedDate(mifidCli.getFhIdon()));
      }

      List<Contacto> listContactos = tmct0cli.getContactos();

      for (int y = 0; y < listContactos.size(); y++) {

        int contactIndex = 0;

        Contacto contacto = listContactos.get(y);

        if (y > 0) {
          row = registros.createRow(rowMin++);
        }

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getNombre());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getApellido1() + " " + contacto.getApellido2());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getDescripcion());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getTelefonos());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getCdEtrali());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getMovil());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getFax());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getEmail());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getComentarios());

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getPosicionCargo());

        cell = row.createCell(indexContent + contactIndex++);
        String activo = contacto.getActivo().toString();
        if (activo == "S") {
          activo = "SI";
        }
        else {
          activo = "NO";
        }
        cell.setCellValue(activo);

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getGestor() != null ? contacto.getGestor() : true);

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(contacto.getDireccionPrincipal());

        BlanqueoClientesEntity blanqueoCliente = blanqueoClientesDAO.findOne(tmct0cli.getCdbrocli());

        // Añadimos los nuevos datos
        String idAnalisis = "";
        String idControlEfectivo = "";
        String idObservacion = "";
        String descripcion = "";

        if (blanqueoCliente != null) {
          idAnalisis = blanqueoClientesDAO.findIdAnalisisCliente(blanqueoCliente.getId());
          idControlEfectivo = blanqueoClientesDAO.findIdControlEfectivo(blanqueoCliente.getId());
          idObservacion = blanqueoClientesDAO.findIdObservacion(blanqueoCliente.getId());
          descripcion = blanqueoObservacionDAO.findDesObservacion(idObservacion);
        }

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(idAnalisis);

        cell = row.createCell(indexContent + contactIndex++);
        cell.setCellValue(idControlEfectivo);

        cell = row.createCell(indexContent + contactIndex++);
        if (idObservacion != null && descripcion != null) {
          cell.setCellValue(idObservacion + '-' + descripcion);
        }

        Date fechaRevPrev = blanqueoClientesDAO.findFechaRevPrev(tmct0cli.getIdClient());
        cell = row.createCell(indexContent + contactIndex++);
        if (fechaRevPrev != null) {
          DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
          Date fecha = fechaRevPrev;
          String reportDate = df.format(fecha);
          cell.setCellValue(reportDate);
        }
      }
    }

    return workbook;
  }

  /**
   * method that returns the headers of the excel file
   * @return
   */
  private String[] getHeaders() {
    int index = 0;
    String[] header = new String[HEADERS_SIZE];

    header[index++] = "ID";
    header[index++] = "CLIENTE";
    header[index++] = "DESCRIPCIÓN";
    header[index++] = "TIPO DOCUMENTO";
    header[index++] = "NUM. DOCUMENTO";
    header[index++] = "TIPO RESIDENCIA";
    header[index++] = "CODIGO DE PERSONA BDP";
    header[index++] = "TIPO CLIENTE";
    header[index++] = "NATURALEZA CLIENTE";
    header[index++] = "ESTADO CLIENTE";
    header[index++] = "FECHA DE ALTA";
    header[index++] = "CÓDIGO KGR";
    header[index++] = "TIPO DE NACIONALIDAD";
    header[index++] = "CODIGO DE PAIS NACIONALIDAD";
    header[index++] = "CATEGORIZACIÓN";
    header[index++] = "EXCEPTUADO CONTROL PBC";
    header[index++] = "MOTIVO PBC";
    header[index++] = "RIESGO PBC";
    header[index++] = "PROX. REVISIÓN RIESGO PBC";
    header[index++] = "DIRECCIÓN PRINCIPAL";
    header[index++] = "CATEGORIZACION MIFID";
    header[index++] = "TEST CONVENIENCIA MIFID";
    header[index++] = "TEST IDONEIDAD MIFID";
    header[index++] = "FECHA FIN CONVENIENCIA";
    header[index++] = "FECHA FIN IDONEIDAD";
    header[index++] = "NOMBRE CONTACTO";
    header[index++] = "APELLIDOS";
    header[index++] = "DESCRIPCIÓN";
    header[index++] = "TELÉFONOS";
    header[index++] = "CÓDIGO ETRALI";
    header[index++] = "MOVIL";
    header[index++] = "FAX";
    header[index++] = "EMAIL";
    header[index++] = "COMENTARIOS";
    header[index++] = "CARGO";
    header[index++] = "ACTIVO";
    header[index++] = "GESTOR";
    header[index++] = "DIRECCION";
    // Añadimos los nuevos datos
    header[index++] = "CODIGO CONTROL ANALISIS";
    header[index++] = "CODIGO DE CCE";
    header[index++] = "ULTIMA OBSERVACION";
    header[index++] = "FECHA DE REVISIÓN PREVENCIÓN";

    return header;
  }

  /**
   * Modificación del contacto pasado como parametro
   *
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> modificarContacto(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio modificarContacto");
    Map<String, Object> resultado = new HashMap<String, Object>();
    List<String> errores = new ArrayList<String>();

    try {

      Contacto contacto = tmct0ContactoAssembler.getTmct0ContactoEntity(paramsObject);
      errores = validarContacto(contacto, new HashMap<String, Boolean>(), false);

      if (errores.isEmpty()) {
        // Se guarda la información
        contactoDao.save(contacto);
        resultado.put("errores", null);
        LOG.debug("Contacto modificado correctamente");
      }
      else {
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        resultado.put("errores", errores);
      }

    }
    catch (Exception e) {
      errores.add("Error al intentar modificar el cliente");
      resultado.put("errores", errores);
      LOG.error("Error metodo init - modificarCliente " + e.getMessage(), e);
    }
    LOG.debug("Fin modificarCliente");
    return resultado;

  }

  /**
   * Obtencion del historico de un cliente seleccionado.
   * 
   * @param paramsFilters paramsFilters
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> verHistoricoCliente(Map<String, Object> paramsObject)
      throws SIBBACBusinessException, ParseException {

    LOG.trace("Iniciando datos para la lista verHistoricoCliente");
    Map<String, Object> mSalida = new HashMap<String, Object>();

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date fechaDesde = null;
    if (paramsObject.get("fechaDesde") != null) {
      fechaDesde = formatter.parse(String.valueOf(paramsObject.get("fechaDesde")));
    }

    try {
      List<Tmct0cliHistorical> listadoHistoricoCliente = this.tmct0cliHistoricalDaoImp.findHistoricoClientesByFiltro(
          new Long((Integer) paramsObject.get("id")), fechaDesde, (String) paramsObject.get("tipoDato"));
      mSalida.put("listadoHistoricoCliente", listadoHistoricoCliente);

    }
    catch (Exception e) {
      LOG.error("Error metodo init - verHistoricoCliente " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales del historico del cliente");
    }

    LOG.trace("Fin recuperación datos para la lista verHistoricoCliente");

    return mSalida;
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    FieldsPlantilla[] fields = FieldsPlantilla.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();

    return result;
  }

  @SuppressWarnings("unchecked")
  private String getValorDescriptivo(String catalogo, String valor) {

    if (catalogos.get(catalogo) != null) {
      List<SelectDTO> valores = (List<SelectDTO>) catalogos.get(catalogo);
      for (SelectDTO dto : valores) {
        if (dto.getKey().equals(valor)) {
          return dto.getValue();
        }
      }
    }
    return valor;

  }

  private void initCatalogos() throws SIBBACBusinessException {

    catalogos.put("tiposDeDocumento", this.getTiposDeDocumento());
    catalogos.put("tiposDeResidencia", this.getTiposDeResidencia());
    catalogos.put("tiposDeCliente", this.getTiposDeCliente());
    catalogos.put("naturalezaDeCliente", this.getNaturalezaDeCliente());
    catalogos.put("estadosDeCliente", this.getEstadosDeCliente());
    catalogos.put("tiposDeNacionalidad", this.getTiposDeNacionalidad());
    catalogos.put("nacionalidades", this.getNacionalidades());
    catalogos.put("nacionalidadesCodigoPostal", this.getNacionalidadesCodPostal());
    catalogos.put("categorizacionesCliente", this.getCategorizacionesCliente());
    catalogos.put("siNo", this.getSiNo());
    catalogos.put("motivosPBC", this.getMotivosPBC());
    catalogos.put("direcciones", this.getDirecciones());
    catalogos.put("testDeConveniencia", this.getTestDeConveniencia());
    catalogos.put("testDeIdoneidad", this.getTestDeIdoneidad());
    catalogos.put("categorias", this.getCategorias());
    catalogos.put("ambitosCorporativos", this.getAmbitosCorporativos());
    catalogos.put("categoriasFiscal", this.getCategoriasFiscal());
    catalogos.put("provincias", this.getProvincias());
    catalogos.put("tiposDomicilio", this.getTiposDomicilio());

  }

  private List<SelectDTO> getTiposDeDocumento() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("tiposDeDocumento");
  }

  private List<SelectDTO> getTiposDeResidencia() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("tiposDeResidencia");
  }

  private List<SelectDTO> getTiposDeCliente() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("tiposDeCliente");
  }

  private List<SelectDTO> getNaturalezaDeCliente() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("naturalezaDeCliente");
  }

  private List<SelectDTO> getEstadosDeCliente() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("estadosDeCliente");
  }

  private List<SelectDTO> getTiposDeNacionalidad() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("tiposDeNacionalidad");
  }

  private List<SelectDTO> getNacionalidades() throws SIBBACBusinessException {
    List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
    for (Tmct0paises pais : tmct0paisesDao.findAllActivos()) {
      catalogo.add(new SelectDTO(String.valueOf(pais.getCdhacienda()).trim(), pais.getNbpais()));
    }
    return catalogo;
  }

  private List<SelectDTO> getNacionalidadesCodPostal() throws SIBBACBusinessException {
    List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
    for (Tmct0paises pais : tmct0paisesDao.findAllActivos()) {
      catalogo.add(new SelectDTO(pais.getNbpais(), pais.getCdhacienda()));
    }
    return catalogo;
  }

  private List<SelectDTO> getProvincias() throws SIBBACBusinessException {
    List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
    for (Tmct0provincias p : tmct0provinciasDao.findAllActivas()) {
      catalogo.add(new SelectDTO(p.getCdcodigo(), p.getNbprovin()));
    }
    return catalogo;
  }

  private List<SelectDTO> getTiposDomicilio() throws SIBBACBusinessException {
    List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
    for (Tval0vpb tipoDomicilio : tpdomiciBo.findAll()) {
      catalogo.add(new SelectDTO(tipoDomicilio.getCdviapub(),
          "(" + tipoDomicilio.getCdviapub() + ") " + tipoDomicilio.getNbviapub()));
    }
    return catalogo;
  }

  private List<SelectDTO> getCategorizacionesCliente() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("categorizacionesCliente");
  }

  private List<SelectDTO> getSiNo() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("siNo");
  }

  private List<SelectDTO> getMotivosPBC() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("motivosPBC");
  }

  private List<SelectDTO> getDirecciones() throws SIBBACBusinessException {
    List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
    return catalogo;
  }

  private List<SelectDTO> getTestDeConveniencia() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("testDeConveniencia");
  }

  private List<SelectDTO> getTestDeIdoneidad() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("testDeIdoneidad");
  }

  private List<SelectDTO> getCategorias() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("categorias");
  }

  private List<SelectDTO> getAmbitosCorporativos() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("ambitosCorporativos");
  }

  private List<SelectDTO> getCategoriasFiscal() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("categoriasFiscal");
  }

  /**
   * Devuelve DTO con clave-valor de select populado.
   *
   * @param catalogo catalogo
   * @return List<SelectDTO> List<SelectDTO>
   */
  private List<SelectDTO> getListSelectDTOByCatalogo(String catalogo) {
    List<SelectDTO> catalogoList = new ArrayList<SelectDTO>();
    List<Tmct0Selects> selects = this.tmct0SelectsDao.findByCatalogo(catalogo);
    if (CollectionUtils.isNotEmpty(selects)) {
      for (Tmct0Selects sel : selects) {
        catalogoList.add(new SelectDTO(String.valueOf(sel.getClave()).trim(), String.valueOf(sel.getValor()).trim()));
      }
    }
    return catalogoList;
  }

  private String getFormattedDate(Date date) {
    if (date != null) {
      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
      return formatter.format(date);
    }
    else {
      return "";
    }
  }

  @SuppressWarnings("unchecked")
  private void checkOperativa(Map<String, Object> paramsObject, List<String> errores) throws SIBBACBusinessException {

    LOG.debug("Inicio Map<String, Object> paramsObject de checkOperativa");
    try {
      Map<String, Object> datosCliente = (Map<String, Object>) paramsObject.get("datosCliente");
      String tipoCliente = datosCliente.get("tpclienn").toString();
      String operativa = datosCliente.get("cddeprev").toString();

      // Si se cumple la condicion de que es institucional NO Bolsa Plus,
      // debemos controlar que el valor del campo "Operativa Recurrente" sea NO.
      if (tipoCliente.equalsIgnoreCase("I") && !operativa.equalsIgnoreCase("N")) {
        errores.add("Un nuevo cliente institucional debe marcarse como Operativa NO Recurrente.");
      }
    }
    catch (Exception e) {
      errores.add("Error al intentar comprobar operativa");
      LOG.error("Error metodo init - checkOperativa " + e.getMessage(), e);
    }
    LOG.debug("Fin checkOperativa");
  }

  @SuppressWarnings("unchecked")
  private void checkKGR(Map<String, Object> paramsObject, List<String> errores) {
    LOG.debug("Inicio Map<String, Object> paramsObject de checkKGR");
    try {
      Map<String, Object> datosCliente = (Map<String, Object>) paramsObject.get("datosCliente");
      String tipoCliente = datosCliente.get("tpclienn").toString();
      String cdKgr = datosCliente.get("cdkgr").toString();
      String operativa = datosCliente.get("cddeprev").toString();

      if (cdKgr.equalsIgnoreCase("9RED")) {
        if (tipoCliente.equalsIgnoreCase("I")) {
          errores.add("El codigo KGR de un cliente institucional no puede ser igual a 9RED.");
        }
        if (operativa.equalsIgnoreCase("N")) {
          errores.add("Un nuevo cliente bolsa plus debe marcarse como Operativa Recurrente.");
        }
      }
    }
    catch (Exception e) {
      errores.add("Error al intentar comprobar kgr");
      LOG.error("Error metodo init - checkOperativa " + e.getMessage(), e);
    }
    LOG.debug("Fin checkKGR");
  }
}