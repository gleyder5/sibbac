package sibbac.business.mantenimientodatoscliente.rest;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tval0vpbBo;
import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0fisBo;
import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0fisDao;
import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0fisDaoImp;
import sibbac.business.mantenimientodatoscliente.database.dto.FinalHolderDTO;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.mantenimientodatoscliente.database.validation.FinalHolderValidator;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.dao.CodigoErrorDao;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.dao.Tmct0provinciasDao;
import sibbac.business.wrappers.database.dao.Tmct0swiDao;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0provincias;
import sibbac.business.wrappers.database.model.Tmct0swi;
import sibbac.business.wrappers.database.model.Tmct0tfi;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bsnvasql.model.Tval0vpb;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * The Class SIBBACServiceTmct0Fis.
 */
@SIBBACService
final class SIBBACServiceTmct0Fis implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceTmct0Fis.class);
  /**
   * Mapa que cargara todos los valores iniciales y estaticos de las listas del
   * filtro.
   */
  private static final Map<String, Object> mStatico = new HashMap<String, Object>();

  /** The Tmct0fis bo. */
  @Autowired
  private Tmct0fisBo tmct0fisBo;

  /** The paises bo. */
  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  /** The provincias bo. */
  @Autowired
  private Tmct0provinciasDao tmct0provinciasDao;

  /** The Tmct0fis dao. */
  @Autowired
  private Tmct0fisDaoImp tmct0fisDaoImp;

  /** The Tmct0fis dao. */
  @Autowired
  private Tmct0fisDao tmct0fisDao;

  /** The swift dao. */
  @Autowired
  private Tmct0swiDao tmct0swiDao;

  /** The codigo error dao. */
  @Autowired
  private CodigoErrorDao codigoErrorDao;

  /** The Tmct0tfi bo. */
  @Autowired
  private Tmct0tfiBo tmct0tfiBo;

  @Autowired
  private Tval0vpbBo tpdomiciBo;
   
  @Autowired
  private FinalHolderValidator finalHolderValidator;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  private static final String FILTER_FILE = "file";
  private static final String FILTER_FILE_NAME = "filename";
  private static final String NOMBRE_SERVICIO = "SIBBACServiceTmct0Fis";
  
  private static final int TIPO_ACCION = 0;
  private static final int TIPO_PERSONA = 1;
  private static final int TIPO_DOCUMENTO = 2;
  private static final int DOCUMENTO = 3;
  private static final int TIPO_NACIONALIDAD = 4;
  private static final int CODIGO_ISO_NACIONALIDAD = 5;
  private static final int NOMBRE = 6;
  private static final int APELLIDO_1 = 7;
  private static final int APELLIDO_2 = 8;
  private static final int TIPO_DIR = 9;
  private static final int DOMICILIO = 10;
  private static final int NUM_DOMICILIO = 11;
  private static final int CIUDAD = 12;
  private static final int PROVINCIA = 13;
  private static final int COD_POSTAL = 14;
  private static final int COD_PAIS_RESIDENCIA = 15;
  private static final int TIPO_TITULAR = 16;
  private static final int FECHA_NACIMIENTO = 17;
  private static final int TIPO_MIFID = 18;
  private static final int ID_MIFID = 19;
  private static final int IND_CLIENTE_SAN = 20;
  private static final int NUM_PERSONA_SAN = 21;
  
  private static final String ALTA = "A";
  private static final String BAJA = "B";
  private static final String MODIFICACION = "M";
  
  private static final String ERRORES_DATA = "erroresData";
  private static final String ERRORES_DIRECCION = "erroresDireccion";
  private static final String ERROR_100 = "ERROR_100";
  
  private static final Integer COLUMN_WITH_SMALL = 5000;
  private static final Integer COLUMN_WITH_LARGE = 40000;
  private static final String DATE_FORMAT = "dd/MM/yyyy";
  
  static {
    Map<String, String> mTipoPersona = new LinkedHashMap<String, String>();
    mTipoPersona.put("", "Todos");
    mTipoPersona.put("F", "FÍSICA");
    mTipoPersona.put("J", "JURÍDICA");
    mStatico.put("tipoPersona", mTipoPersona);

    Map<String, String> mTipoTitular = new LinkedHashMap<String, String>();
    mTipoTitular.put("T", "TITULAR");
    mTipoTitular.put("R", "REPRESENTANTE");
    mTipoTitular.put("U", "USUFRUCTURARIO");
    mTipoTitular.put("N", "NUDOPROPIETARIO");
    mStatico.put("tipoTitular", mTipoTitular);

    Map<String, String> mTipoDocumento = new LinkedHashMap<String, String>();
    mTipoDocumento.put("N", "NIF");
    mTipoDocumento.put("C", "CIF");
    mTipoDocumento.put("E", "NIE");
    mTipoDocumento.put("B", "BIC");
    mTipoDocumento.put("O", "OTROS");
    mStatico.put("tipoDocumento", mTipoDocumento);

    Map<String, String> mTipoNacionalidad = new LinkedHashMap<String, String>();
    mTipoNacionalidad.put("", "Todos");
    mTipoNacionalidad.put("N", "NACIONAL");
    mTipoNacionalidad.put("E", "EXTRANJERO");
    mStatico.put("tipoNacionalidad", mTipoNacionalidad);
    
    Map<String, String> mTipoIdMifid = new LinkedHashMap<String, String>();
    mTipoIdMifid.put("L", "LEI");
    mTipoIdMifid.put("I", "IDENTIF");
    mTipoIdMifid.put("P", "PASAPORTE");
    mTipoIdMifid.put("C", "CONCAT");
    mStatico.put("tipoIdMifid", mTipoIdMifid);
    
    Map<String, String> mIndClienteSan = new LinkedHashMap<String, String>();
    mIndClienteSan.put("S", "SI");
    mIndClienteSan.put("N", "NO");
    mStatico.put("indClienteSan", mIndClienteSan);

  }

  /**
   * The Enum ComandosTitulares.
   */
  private enum ComandosTitulares {

    /** Inicializa los datos del filtro de pantalla */
    INICIALIZA_PANTALLA("init"),

    /** Obtiene los distintos nombres existentes en base de datos */
    CARGA_NOMBRES("CargaNombres"),

    /** Obtiene los distintos apellidos existentes en base de datos */
    CARGA_APELLIDOS("CargaApellidos"),

    /** Obtiene los distintos documentos existentes en base de datos */
    CARGA_DOCUMENTOS("CargaDocumentos"),

    /** Consulta de los Finalholders que cumplen el filtro. */
    CONSULTA_TITULARES("ConsultaTitularesFiltro"),

    /** Crear un nuevo titular con los datos Recibidos */
    VALIDA_TITULAR("ValidaTitular"),

    /** Crear un nuevo titular con los datos Recibidos */
    CREAR_TITULAR("CrearTitular"),

    /** Crear un nuevo titular con los datos Recibidos */
    MODIFICAR_TITULAR("ModificarTitular"),

    /** Elimina los Finalholders checkeados */
    ELIMINAR_TITULARES("EliminarTitulares"),

    /** Elimina los Finalholders checkeados */
    COMPROBAR_ELIMINAR_TITULAR("ComprobarEliminarTitular"),

    /** Importacion de Final Holders Masivo */
    IMPORTAR_EXCEL_CARGA_MASIVA("ImportarExcelCargaMasiva"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos titulares.
     *
     * @param command the command
     */
    private ComandosTitulares(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * The Enum FieldsTmct0fis.
   */
  private enum FieldsTmct0fis {
    CLAVE("clave"), NUMSEC("numsec"), ID("id"), DESCRIPCION("descripcion"), CODIGO("codigo"), TIPOPERSONA("tipopersona"), TIPOTITULAR(
        "tipotitular"), TIPODOCUMENTO("tipodocumento"), DOCUMENTO("documento"), NOMBRE("nombre"), PRIMERAPELLIDO(
        "primerapellido"), SEGUNDOAPELLIDO("segundoapellido"), TIPONACIONALIDAD("tiponacionalidad"), CODNACIONALIDAD(
        "codnacionalidad"), NACIONALIDAD("nacionalidad"), TIPODIRECCION("tipodireccion"), DIRECCION("direccion"), DOMICILIO(
        "domicilio"), NUMDOMICILIO("numdomicilio"), CIUDAD("ciudad"), PROVINCIA("provincia"), CODPOSTAL("codpostal"), CODPAISRESIDENCIA(
        "codpaisresidencia"), FECHANACIMIENTO("fechanacimiento"), PAISRESIDENCIA("paisresidencia"), TIPO_ID_MIFID(
        "tipoIdMifid"), ID_MIFID("idMifid"), IND_CLIENTE_SAN("indClienteSan"), NUM_PERSONA_SAN("numPersonaSan");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsTmct0fis(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.
   * WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, String> filters = webRequest.getFilters();
    List<Map<String, String>> parametros = webRequest.getParams();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosTitulares command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceTmct0Fis");

      switch (command) {
        case INICIALIZA_PANTALLA:
          result.setResultados(init());
          break;
        case CARGA_NOMBRES:
          result.setResultados(cargaNombres());
          break;
        case CARGA_APELLIDOS:
          result.setResultados(cargaApellidos());
          break;
        case CARGA_DOCUMENTOS:
          result.setResultados(cargaDocumentos());
          break;
        case CONSULTA_TITULARES:
          result.setResultados(getFinalHoldersFiltro(filters));
          break;
        case VALIDA_TITULAR:
          result.setResultados(validaTitular(filters));
          break;

        case CREAR_TITULAR:
          result.setResultados(crearTitular(filters));
          break;

        case MODIFICAR_TITULAR:
          result.setResultados(modificarTitular(filters));
          break;

        case ELIMINAR_TITULARES:
          result.setResultados(eliminarTitulares(parametros));
          break;

        case COMPROBAR_ELIMINAR_TITULAR:
          result.setResultados(comprobarEliminarTitulares(parametros));
          break;
        case IMPORTAR_EXCEL_CARGA_MASIVA:
          List<FinalHolderDTO> finalHoldersList = importarExcelCargaMasiva(webRequest, result);
          result.setResultados(exportXLSErroresCargaMasiva(finalHoldersList));
          break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    }
    catch (IOException e) {
      LOG.error("[process] No se encuentra fichero de importación de carga masiva", e);
      throw new SIBBACBusinessException("No se encuentra fichero de importación de carga masiva final holders", e);
    }
    catch (RuntimeException e) {
      LOG.error("[process] Error inesperado en servicio Tmct0Fis", e);
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }
    LOG.debug("Salgo del servicio SIBBACServiceTmct0Fis");
    return result;
  }

  /**
   * Carga inicial
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> init() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para el filtro de Final Holder");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    Map<String, Object> map = new HashMap<String, Object>();

    try {

      map.putAll(mStatico);

      // Obtenemos la lista de paises Activos.
      List<Tmct0paises> mNacionalidades = new ArrayList<Tmct0paises>();

      for (Tmct0paises pais : tmct0paisesDao.findAllActivos()) {
        mNacionalidades.add(pais);
      }
      map.put("nacionalidades", mNacionalidades);

      // Obtenemos la lista de provincias Activas.
      List<Tmct0provincias> mProvincias = new ArrayList<Tmct0provincias>();

      for (Tmct0provincias provincia : tmct0provinciasDao.findAllActivas()) {
        mProvincias.add(provincia);
      }
      map.put("provincias", mProvincias);

      // Obtenemos la lista de tipos de domicilio

      Map<String, String> mTipoDomicilio = new LinkedHashMap<String, String>();
      for (Tval0vpb tipoDomicilio : tpdomiciBo.findAll()) {
        mTipoDomicilio.put(tipoDomicilio.getCdviapub(),
            "(" + tipoDomicilio.getCdviapub() + ") " + tipoDomicilio.getNbviapub());
      }
      map.put("tipoDomicilio", mTipoDomicilio);

      mSalida.put("datosFiltro", map);

    }
    catch (Exception e) {
      LOG.error("Error metodo init - Final holders " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion datos filtro de Final Holder");

    return mSalida;
  }

  /**
   * Obtiene un lista con los distintos nombres existentes en la tabla en campo
   * nbclient
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargaNombres() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la carga del autocompletable de nombres de Final Holder");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    Map<String, Object> map = new HashMap<String, Object>();

    try {

      // obtenemos los nombres existentes
      List<String> mNombre = new ArrayList<String>();

      for (String nombre : tmct0fisDao.findNombres()) {
        if (nombre != null && !nombre.trim().isEmpty()) {
          mNombre.add(nombre);
        }
      }
      map.put("nombres", mNombre);

      mSalida.put("datosFiltro", map);

    }
    catch (Exception e) {
      LOG.error("Error metodo cargaNombres - Final holders " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en  cargaNombress iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion cargaNombres datos filtro de Final Holder");

    return mSalida;
  }

  /**
   * Obtiene un lista con los distintos documentos existentes en la tabla en
   * campo chholder
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargaDocumentos() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la carga del autocompletable de documentos de Final Holder");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    Map<String, Object> map = new HashMap<String, Object>();

    try {

      // obtenemos los documentos existentes
      List<String> mDocumento = new ArrayList<String>();

      for (String documento : tmct0fisDao.findCdholder()) {
        if (documento != null && !documento.trim().isEmpty()) {
          mDocumento.add(documento);
        }
      }
      map.put("documentos", mDocumento);

      mSalida.put("datosFiltro", map);

    }
    catch (Exception e) {
      LOG.error("Error metodo cargaDocumentos - Final holders " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en  cargaDocumentos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion cargaDocumentos datos filtro de Final Holder");

    return mSalida;
  }

  /**
   * Obtiene un lista con los distintos Apellidos existentes en la tabla en
   * campo nbclient1 y nbclient2
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargaApellidos() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la carga del autocompletable de apellidos de Final Holder");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    Map<String, Object> map = new HashMap<String, Object>();

    try {

      // obtenemos los apellidos1 existentes
      List<String> mApellido = new ArrayList<String>();

      for (String ape1 : tmct0fisDao.findApellido1()) {
        if (ape1 != null && !ape1.trim().isEmpty()) {
          mApellido.add(ape1);
        }
      }
      map.put("apellidos1", mApellido);

      // obtenemos los apellidos2 existentes
      List<String> mApellido2 = new ArrayList<String>();

      for (String ape2 : tmct0fisDao.findApellido2()) {
        if (ape2 != null && !ape2.trim().isEmpty()) {
          mApellido2.add(ape2);
        }
      }
      map.put("apellidos2", mApellido2);

      mSalida.put("datosFiltro", map);

    }
    catch (Exception e) {
      LOG.error("Error metodo cargaApellidos - Final holders " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en  cargaApellidos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion cargaNombres datos filtro de Final Holder");

    return mSalida;
  }

  /**
   * Busqueda de titulares bajo los filtros definidos por el usuario
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getFinalHoldersFiltro(Map<String, String> filters) throws SIBBACBusinessException {

    Map<String, Object> result = new HashMap<String, Object>();

    LOG.debug("Entra en  getFinalHoldersFiltro ");

    try {

      LOG.debug("Obtencion parametros insertados por el usuario.");

      // Recuperación de los filtros establecidos
      final String tipoPersona = filters.get(FieldsTmct0fis.TIPOPERSONA.getField());
      List<String> tipoTitularVector = new ArrayList<String>();
      try {
        if (StringUtils.isNotEmpty(filters.get(FieldsTmct0fis.TIPOTITULAR.getField()))) {

          String[] parts = filters.get(FieldsTmct0fis.TIPOTITULAR.getField()).split(";");
          for (int i = 0; i < parts.length; i++) {
            tipoTitularVector.add(parts[i]);
          }
        }
      }
      catch (Exception e1) {
        LOG.error("Error al leer el filtro tipo titular ", e1.getLocalizedMessage());
      }
      List<String> tipoDocumentoVector = new ArrayList<String>();
      try {
        if (StringUtils.isNotEmpty(filters.get(FieldsTmct0fis.TIPODOCUMENTO.getField()))) {

          String[] parts = filters.get(FieldsTmct0fis.TIPODOCUMENTO.getField()).split(";");
          for (int i = 0; i < parts.length; i++) {
            tipoDocumentoVector.add(parts[i]);
          }
        }
      }
      catch (Exception e1) {
        LOG.error("Error al leer el filtro tipo documento ", e1.getLocalizedMessage());
      }
      final String documento = filters.get(FieldsTmct0fis.DOCUMENTO.getField());
      final String nombre = filters.get(FieldsTmct0fis.NOMBRE.getField());
      final String primerApellido = filters.get(FieldsTmct0fis.PRIMERAPELLIDO.getField());
      final String segundoApellido = filters.get(FieldsTmct0fis.SEGUNDOAPELLIDO.getField());
      final String tipoNacionalidad = filters.get(FieldsTmct0fis.TIPONACIONALIDAD.getField());
      List<String> nacionalidadVector = new ArrayList<String>();
      try {
        if (StringUtils.isNotEmpty(filters.get(FieldsTmct0fis.NACIONALIDAD.getField()))) {

          String[] parts = filters.get(FieldsTmct0fis.NACIONALIDAD.getField()).split(";");
          for (int i = 0; i < parts.length; i++) {
            nacionalidadVector.add(parts[i]);
          }
        }
      }
      catch (Exception e1) {
        LOG.error("Error al leer el filtro nacionalidad ", e1.getLocalizedMessage());
      }

      final String tipodireccion = filters.get(FieldsTmct0fis.TIPODIRECCION.getField());
      final String direccion = filters.get(FieldsTmct0fis.DIRECCION.getField());
      final String ciudad = filters.get(FieldsTmct0fis.CIUDAD.getField());
      final String provincia = filters.get(FieldsTmct0fis.PROVINCIA.getField());
      final String codigopostal = filters.get(FieldsTmct0fis.CODPOSTAL.getField());

      List<String> paisResidenciaVector = new ArrayList<String>();
      try {
        if (StringUtils.isNotEmpty(filters.get(FieldsTmct0fis.PAISRESIDENCIA.getField()))) {

          String[] parts = filters.get(FieldsTmct0fis.PAISRESIDENCIA.getField()).split(";");
          for (int i = 0; i < parts.length; i++) {
            paisResidenciaVector.add(parts[i]);
          }
        }
      }
      catch (Exception e1) {
        LOG.error("Error al leer el filtro pais residencia ", e1.getLocalizedMessage());
      }

      LOG.debug("Peticion de datos tmct0fisDaoImp.findFinalHoldersFiltro con los siguientes parametros: ");

      LOG.debug(" tipoPersona: " + tipoPersona + " tipos Titulares " + tipoTitularVector + " tipos Documento "
          + tipoDocumentoVector + " documento " + documento + " nombre " + nombre + " primerApellido " + primerApellido
          + " segundoApellido " + segundoApellido + " tipoNacionalidad " + tipoNacionalidad + " nacionalidades "
          + nacionalidadVector + " tipodireccion " + tipodireccion + " direccion " + direccion + " ciudad " + ciudad
          + " provincia " + provincia + " codigo postal " + codigopostal + " paises Residencia " + paisResidenciaVector);

      List<FinalHolderDTO> lista_tmct0fis = tmct0fisDaoImp.findFinalHoldersFiltro(tipoPersona, tipoTitularVector,
          tipoDocumentoVector, documento, nombre, primerApellido, segundoApellido, tipoNacionalidad,
          nacionalidadVector, tipodireccion, direccion, ciudad, provincia, codigopostal, paisResidenciaVector);

      LOG.debug("Fin Peticion de datos tmct0fisDaoImp.findFinalHoldersFiltro:  Registros recuperados: "
          + lista_tmct0fis.size());

      result.put(RESULT_LISTA, lista_tmct0fis);

    }
    catch (Exception e) {
      LOG.error("Error metodo getListaTmct0fis - Final holders " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
    }

    LOG.debug("Fin getFinalHoldersFiltro");

    return result;

  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    FieldsTmct0fis[] fields = FieldsTmct0fis.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosTitulares getCommand(String command) {
    ComandosTitulares[] commands = ComandosTitulares.values();
    ComandosTitulares result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosTitulares.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  /**
   * pasa las validaciones a un titular insertado o modificado
   * 
   * @param dto
   * @param listErroresExcel
   * @param errores
   * @param isModificar
   * @return
   * @throws SIBBACBusinessException
   */
  private List<String> validateFinalHolder(FinalHolderDTO dto, List<String> listErroresExcel,
      Map<String, Boolean> errores, boolean isModificar) throws SIBBACBusinessException {

    Tmct0fis finalholderBBDD = null;
    if (dto.getDocumento() != null && !dto.getDocumento().isEmpty() && dto.getDocumento().length() < 40) {
      finalholderBBDD = tmct0fisDao.findBycdholder(dto.getDocumento());
    }
    Tmct0paises paisBIC = null;
    Tmct0swi swift = null;
    Tmct0paises pais = null;

    if (dto.getCodNacionalidad() != null && !dto.getCodNacionalidad().isEmpty()) {
      pais = tmct0paisesDao.findByisoNumActivo(dto.getCodNacionalidad());
      if (dto.getDocumento() != null && !dto.getDocumento().isEmpty() && dto.getTipoDocumento().equals(BAJA)) {
        paisBIC = tmct0paisesDao.findBycdisoalf2(dto.getDocumento().trim().substring(4, 6));
        swift = tmct0swiDao.findBySwift(dto.getDocumento().replaceAll(" ", ""));
      }
    }

    Tmct0provincias provincia = null;

    if (dto.getCodigoPostal() != null && !dto.getCodigoPostal().isEmpty()) {
      provincia = tmct0provinciasDao.findBycdcodigo(dto.getCodigoPostal().substring(0, 2));
    }

    List<String> listErrores = finalHolderValidator.validateFormat(dto, pais, paisBIC, swift, finalholderBBDD,
        finalholderBBDD, provincia, errores, isModificar);

    listErroresExcel.addAll(listErrores);

    if (!listErrores.isEmpty()) {
      List<CodigoErrorData> coList = codigoErrorDao.findByCodigoOrderByCodigoDataAsc(listErrores);
      listErrores = new ArrayList<String>();
      for (CodigoErrorData error : coList) {
        listErrores.add(error.getCodigo() + "-" + error.getDescripcion());
      }
      
      dto.setErrorsList(coList);
    }

    return listErrores;

  }

  /**
   * Valida los datos del titular recibido y devuelve la lista de errores.
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> validaTitular(Map<String, String> filters) throws SIBBACBusinessException {

    Map<String, Object> resultados = new HashMap<>();

    // Se forma el dto con los datos del formulario
    FinalHolderDTO nuevoFinalHolder = cargaFinalHolderDTO(filters);

    Map<String, Boolean> errores = new HashMap<>();
    errores.put("erroresData", Boolean.FALSE);
    errores.put("erroresDireccion", Boolean.FALSE);

    List<String> listadoErrores = validateFinalHolder(nuevoFinalHolder, new ArrayList<String>(), errores, false);

    resultados.put("listErrores", listadoErrores);
    return resultados;

  }

  /**
   * Valida los datos recibidos y si esta todo ok le inserta, sino devuelve la
   * lista de errores
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> crearTitular(Map<String, String> filters) throws SIBBACBusinessException {
    Map<String, Object> resultados = new HashMap<String, Object>();

    // Se forma el dto con los datos del formulario
    FinalHolderDTO nuevoFinalHolder = cargaFinalHolderDTO(filters);

    Map<String, Boolean> errores = new HashMap<String, Boolean>();
    errores.put("erroresData", Boolean.FALSE);
    errores.put("erroresDireccion", Boolean.FALSE);

    List<String> listadoErrores = validateFinalHolder(nuevoFinalHolder, new ArrayList<String>(), errores, false);

    // Se carga la lista de los errores.
    resultados.put("listErrores", listadoErrores);

    // Se comprueba si la información contiene errores.
    if (listadoErrores.size() == 0) {
      // Se realiza el insert si no hay errores
      tmct0fisBo.insertFinalHolder(nuevoFinalHolder);
    }

    return resultados;

  }

  /**
   * Valida los datos recibidos y si esta todo ok le modifica, sino devuelve la
   * lista de errores
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> modificarTitular(Map<String, String> filters) throws SIBBACBusinessException {
    Map<String, Object> resultados = new HashMap<String, Object>();

    // Se forma el dto con los datos del formulario
    FinalHolderDTO miFinalHolder = cargaFinalHolderDTO(filters);

    Map<String, Boolean> errores = new HashMap<String, Boolean>();
    errores.put("erroresData", Boolean.FALSE);
    errores.put("erroresDireccion", Boolean.FALSE);

    List<String> listadoErrores = validateFinalHolder(miFinalHolder, new ArrayList<String>(), errores, true);

    // Se carga la lista de los errores.
    resultados.put("listErrores", listadoErrores);

    // Se comprueba si la información contiene errores.
    if (listadoErrores.size() == 0) { // Se realiza la modificaciion si no hay
                                      // errores
      tmct0fisBo.updateFinalHolder(miFinalHolder);
    }

    return resultados;

  }

  /**
   * Genera un dto a partir de los datos de pantalla
   * 
   * @param filters
   * @return
   */
  private FinalHolderDTO cargaFinalHolderDTO(Map<String, String> filters) {

    FinalHolderDTO fhFinalHolder = new FinalHolderDTO();

    // MFG 13/04/2016 Incidencia 6 - Se quita el codigo de toda la parte front y
    // se carga con el valor del campo
    // documento.
    fhFinalHolder.setCodigo(filters.get(FieldsTmct0fis.DOCUMENTO.getField()));
    fhFinalHolder.setTipoPersona(filters.get(FieldsTmct0fis.TIPOPERSONA.getField()));
    fhFinalHolder.setTipoTitular(filters.get(FieldsTmct0fis.TIPOTITULAR.getField()));
    fhFinalHolder.setDocumento(filters.get(FieldsTmct0fis.DOCUMENTO.getField()));
    fhFinalHolder.setTipoDocumento(filters.get(FieldsTmct0fis.TIPODOCUMENTO.getField()));
    fhFinalHolder.setNombre(filters.get(FieldsTmct0fis.NOMBRE.getField()));
    fhFinalHolder.setApellido1(filters.get(FieldsTmct0fis.PRIMERAPELLIDO.getField()));
    fhFinalHolder.setApellido2(filters.get(FieldsTmct0fis.SEGUNDOAPELLIDO.getField()));
    fhFinalHolder.setFechaNacimiento(filters.get(FieldsTmct0fis.FECHANACIMIENTO.getField()));
    fhFinalHolder.setTipoNacionalidad(filters.get(FieldsTmct0fis.TIPONACIONALIDAD.getField()));
    fhFinalHolder.setCodNacionalidad(filters.get(FieldsTmct0fis.CODNACIONALIDAD.getField()));
    fhFinalHolder.setTipoDireccion(filters.get(FieldsTmct0fis.TIPODIRECCION.getField()));
    fhFinalHolder.setDomicilio(filters.get(FieldsTmct0fis.DOMICILIO.getField()));
    fhFinalHolder.setNumDomicilio(filters.get(FieldsTmct0fis.NUMDOMICILIO.getField()));
    fhFinalHolder.setCiudad(filters.get(FieldsTmct0fis.CIUDAD.getField()));
    fhFinalHolder.setProvincia(filters.get(FieldsTmct0fis.PROVINCIA.getField()));
    fhFinalHolder.setCodigoPostal(filters.get(FieldsTmct0fis.CODPOSTAL.getField()));
    fhFinalHolder.setCodPaisResidencia(filters.get(FieldsTmct0fis.CODPAISRESIDENCIA.getField()));

    // CAMPOS MIFID II
    if (filters.get(FieldsTmct0fis.TIPO_ID_MIFID.getField()) != null && !filters.get(FieldsTmct0fis.TIPO_ID_MIFID.getField()).isEmpty()) {
      fhFinalHolder.setTipoIdMifid(filters.get(FieldsTmct0fis.TIPO_ID_MIFID.getField()).charAt(0));
    }
    fhFinalHolder.setIdMifid(filters.get(FieldsTmct0fis.ID_MIFID.getField()));
    if (filters.get(FieldsTmct0fis.IND_CLIENTE_SAN.getField()) != null && !filters.get(FieldsTmct0fis.IND_CLIENTE_SAN.getField()).isEmpty()) {
      fhFinalHolder.setIndClienteSan(filters.get(FieldsTmct0fis.IND_CLIENTE_SAN.getField()).charAt(0));
    }
    fhFinalHolder.setNumPersonaSan(filters.get(FieldsTmct0fis.NUM_PERSONA_SAN.getField()));

    return fhFinalHolder;
  }

  /**
   * 
   * elimina uno o varios titulares
   * 
   * @param parametros
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> eliminarTitulares(List<Map<String, String>> parametros) throws SIBBACBusinessException {
    Map<String, Object> resultados = new HashMap<>();

    Map<String, String> registro;
    String codFinalHolder;
    Integer nCont = 0;

    Iterator<Map<String, String>> it = parametros.iterator();
    while (it.hasNext()) {
      registro = it.next();
      codFinalHolder = registro.get("codFinalHolder");
      tmct0fisBo.deleteFinalHolder(codFinalHolder);
      tmct0tfiBo.delete(codFinalHolder);
      nCont++;
    }
    if (nCont > 1) {
      resultados.put(ComandosTitulares.ELIMINAR_TITULARES.getCommand(), "Se han eliminado " + nCont.toString()
          + " final holders correctamente");
    }
    else {
      resultados
          .put(ComandosTitulares.ELIMINAR_TITULARES.getCommand(), "Se ha eliminado el final holder correctamente");
    }

    return resultados;
  }

  /**
   * Comprueba si un titular tiene asociado instrucciones de liquidacion
   * 
   * @param parametros
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> comprobarEliminarTitulares(List<Map<String, String>> parametros)
      throws SIBBACBusinessException {
    Map<String, Object> resultados = new HashMap<>();
    Map<String, String> registro;
    String codFinalHolder;

    Iterator<Map<String, String>> it = parametros.iterator();
    while (it.hasNext()) {
      registro = it.next();
      codFinalHolder = registro.get("codFinalHolder");
      List<Tmct0tfi> list = tmct0tfiBo.getByCddclaveAndFhfinal(codFinalHolder);
      if (list != null && !list.isEmpty()) {
        resultados.put(ComandosTitulares.COMPROBAR_ELIMINAR_TITULAR.getCommand(),
            "Este titular está asociado a instrucciones de liquidación, ¿desea dar de baja el titular?");
      }
      else {
        resultados.put(ComandosTitulares.COMPROBAR_ELIMINAR_TITULAR.getCommand(), "OK");
      }

    }

    return resultados;
  }

  /**
   * exporta fichero con los errores de la carga masiva
   * 
   * @param finalHolderErroresList
   * @return
   * @throws SIBBACBusinessException
   * @throws FileNotFoundException
   */
  private Map<String, Object> exportXLSErroresCargaMasiva(List<FinalHolderDTO> finalHolderErroresList)
      throws SIBBACBusinessException, FileNotFoundException {

    Map<String, Object> resultados = new HashMap<String, Object>();

    if (!finalHolderErroresList.isEmpty()) {

      ByteArrayOutputStream bos = new ByteArrayOutputStream();

      XSSFWorkbook wb = generarExcelErrores(finalHolderErroresList);

      try {
        wb.write(bos);
      }
      catch (IOException e) {
        LOG.error("Error al escribir fichero", e);
      }

      resultados.put("fileErrorsCargaMasiva", bos.toByteArray());
    }
    else {
      resultados.put("fileErrors", "OK");
    }

    return resultados;
  }

  /**
   * Genera Excel de errores de carga masiva
   * 
   * @param errorRecords
   * @return
   */
  private XSSFWorkbook generarExcelErrores(List<FinalHolderDTO> errorRecords) {

    XSSFWorkbook workbook = new XSSFWorkbook();
    CreationHelper factory = workbook.getCreationHelper();
    // Estilo de la cabecera
    XSSFCellStyle xssfCellStyleHeader = workbook.createCellStyle();

    xssfCellStyleHeader.setFillForegroundColor(new XSSFColor(Color.RED));
    xssfCellStyleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

    // Crear la fuente de la cabecera
    XSSFFont xssfFont = workbook.createFont();
    xssfFont.setFontHeightInPoints((short) 8);
    xssfFont.setFontName("ARIAL");
    xssfFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
    xssfFont.setColor(new XSSFColor(Color.WHITE));
    xssfCellStyleHeader.setFont(xssfFont);

    xssfCellStyleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
    xssfCellStyleHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    xssfCellStyleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);
    xssfCellStyleHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());

    xssfCellStyleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
    xssfCellStyleHeader.setVerticalAlignment(XSSFCellStyle.ALIGN_FILL);

    // Hoja con el listado de errores
    XSSFSheet recordsSheet = workbook.createSheet("REGISTROS CON ERROR");
    int rowMin = 0;

    // Cabecera
    XSSFRow row = recordsSheet.createRow(rowMin++);
    row.setHeight((short) 600);
    XSSFCell cell = null;

    int cellnum = 0;
    //0- TIPO DE ACCIÓN
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO OPERACIÓN");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "A-Alta\n B-Baja\n M-Modificacion");
    //1- TIPO DE PERSONA
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO PERSONA");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "Tipo persona:\nF-Fisica\nJ-Juridica");
    //2- TIPO DE DOCUMENTO
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO DOCUMENTO");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "Tipo Documento\nB-bic\nC-cif\nN-nif\nO-otros");
    //3- DOCUMENTO
    cell = row.createCell(cellnum++);
    cell.setCellValue("DOCUMENTO");
    cell.setCellStyle(xssfCellStyleHeader);
    //4- COD NACIONALIDAD
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO NACIONALIDAD");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "Tipo nacionalidad\nN-Nacional\nE-Extranjero");
    //5- NACIONALIDAD
    cell = row.createCell(cellnum++);
    cell.setCellValue("ISO NACIONALIDAD");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "Codigo Iso Nacionalidad");
    //6- NOMBRE
    cell = row.createCell(cellnum++);
    cell.setCellValue("NOMBRE");
    cell.setCellStyle(xssfCellStyleHeader);
    //7- APELLIDO1
    cell = row.createCell(cellnum++);
    cell.setCellValue("APELLIDO1/RAZÓN SOCIAL");
    cell.setCellStyle(xssfCellStyleHeader);
    //8- APELLIDO2
    cell = row.createCell(cellnum++);
    cell.setCellValue("APELLIDO2");
    cell.setCellStyle(xssfCellStyleHeader);
    //9-TIPO DOMICILIO
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO DIRECCIÓN");
    cell.setCellStyle(xssfCellStyleHeader);
    //10- DOMICILIO
    cell = row.createCell(cellnum++);
    cell.setCellValue("DIRECCIÓN");
    cell.setCellStyle(xssfCellStyleHeader);
    //11- NUM DOMICILIO
    cell = row.createCell(cellnum++);
    cell.setCellValue("NÚMERO");
    cell.setCellStyle(xssfCellStyleHeader);
    //12- CIUDAD
    cell = row.createCell(cellnum++);
    cell.setCellValue("CIUDAD");
    cell.setCellStyle(xssfCellStyleHeader);
    //13- PROVINCIA
    cell = row.createCell(cellnum++);
    cell.setCellValue("PROVINCIA");
    cell.setCellStyle(xssfCellStyleHeader);
    //14- COD POSTAL
    cell = row.createCell(cellnum++);
    cell.setCellValue("COD. POSTAL");
    cell.setCellStyle(xssfCellStyleHeader);
    //15- PAIS RESIDENCIA
    cell = row.createCell(cellnum++);
    cell.setCellValue("ISO RESIDENCIA");
    cell.setCellStyle(xssfCellStyleHeader);
    //16- TIPO TITULAR
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO TITULAR");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "Tipo Titular:\nT-Titular\nR-Representante\nU-Usufructuario\nN-Nudopropietario");
    //17- FECHA NACIMIENTO
    cell = row.createCell(cellnum++);
    cell.setCellValue("FECHA NACIMIENTO");
    cell.setCellStyle(xssfCellStyleHeader);   
    createComment(factory, recordsSheet, cell, row, "Formato: dd/mm/aaaa");
    //18- TIPO ID MIFID
    cell = row.createCell(cellnum++);
    cell.setCellValue("TIPO ID MIFID");
    cell.setCellStyle(xssfCellStyleHeader);   
    createComment(factory, recordsSheet, cell, row, "Tipo Id Mifid\nL-Lei\nI-Identif\nP-Pasaporte\nC-Concat");
    //19- ID MIFID
    cell = row.createCell(cellnum++);
    cell.setCellValue("ID MIFID");
    cell.setCellStyle(xssfCellStyleHeader);    
    //20- IND CLI SAN
    cell = row.createCell(cellnum++);
    cell.setCellValue("IND CLI SANTANDER");
    cell.setCellStyle(xssfCellStyleHeader);
    createComment(factory, recordsSheet, cell, row, "Indicador cliente Santander\nS-Si\nN-No");
    //21- NUM PERSONA SANTANDER
    cell = row.createCell(cellnum++);
    cell.setCellValue("NUM PERSONA SANTANDER");
    cell.setCellStyle(xssfCellStyleHeader);    

    int numColumn = cellnum;

    for (int i = 0; i < errorRecords.size(); i++) {

      FinalHolderDTO finalHolderDTO = errorRecords.get(i);
      row = recordsSheet.createRow(rowMin++);
      
      cellnum = 0;
      //0- TIPO DE ACCIÓN
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoAccion());
      //1- TIPO DE PERSONA
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoPersona());
      //2- TIPO DE DOCUMENTO
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoDocumento());
      //3- DOCUMENTO
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getDocumento());
      //4- COD NACIONALIDAD
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoNacionalidad());
      //5- NACIONALIDAD
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getCodNacionalidad());
      //6- NOMBRE
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getNombre());
      //7- APELLIDO1
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getApellido1());
      //8- APELLIDO2
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getApellido2());
      //9-TIPO DOMICILIO
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoDireccion());
      //10- DOMICILIO
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getDomicilio());
      //11- NUM DOMICILIO
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getNumDomicilio());
      //12- CIUDAD
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getCiudad());
      //13- PROVINCIA
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getProvincia());
      //14- COD POSTAL
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getCodigoPostal());
      //15- PAIS RESIDENCIA
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getCodPaisResidencia());
      //16- TIPO TITULAR
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoTitular());
      //17- FECHA NACIMIENTO
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getFechaNacimiento());
      //18- TIPO ID MIFID
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getTipoIdMifid() + "");
      //19- ID MIFID
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getIdMifid());
      //20- IND CLI SAN
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getIndClienteSan() + "");
      //21- NUM PERSONA SANTANDER
      cell = row.createCell(cellnum++);
      cell.setCellValue(finalHolderDTO.getNumPersonaSan());      
    }

    String lastCellReference = cell.getReference();
    recordsSheet.setAutoFilter(CellRangeAddress.valueOf("A1:" + lastCellReference));

    for (int i = 0; i < numColumn; i++) {
      recordsSheet.autoSizeColumn(i);
    }
  
    // Hoja con el catalogo de errores    
    XSSFSheet sheetErrorsFound = workbook.createSheet("ERRORES ENCONTRADOS");
    rowMin = 0;
    // Cabecera
    row = sheetErrorsFound.createRow(rowMin++);
    cell = null;

    cellnum = 0;
    cell = row.createCell(cellnum++);
    cell.setCellValue("Fila con errores");
    cell.setCellStyle(xssfCellStyleHeader);
    cell = row.createCell(cellnum++);
    
    cell.setCellValue("Cod. Error");
    cell.setCellStyle(xssfCellStyleHeader);
    cell = row.createCell(cellnum);
    
    cell.setCellValue("Control Realizado");
    cell.setCellStyle(xssfCellStyleHeader);
    
    int indice = 1;
    for(FinalHolderDTO finalHolderDTO : errorRecords){
      if(finalHolderDTO.getErrorsList()!=null && !finalHolderDTO.getErrorsList().isEmpty()){
        for (CodigoErrorData errorData : finalHolderDTO.getErrorsList()) {
          cellnum = 0;
          row = sheetErrorsFound.createRow(rowMin++);
          
          cell = row.createCell(cellnum++);       
          cell.setCellValue("Fila "+indice);          
          
          cell = row.createCell(cellnum++);       
          String code = errorData.getCodigo().trim();
          cell.setCellValue(code.substring(code.length() - 2));
                   
          cell = row.createCell(cellnum);
          cell.setCellValue(errorData.getDescripcion().trim());
        }
      }
      indice++;
    }

    sheetErrorsFound.setColumnWidth(0, COLUMN_WITH_SMALL);
    sheetErrorsFound.setColumnWidth(1, COLUMN_WITH_SMALL);
    sheetErrorsFound.setColumnWidth(2, COLUMN_WITH_LARGE);    

    // Hoja con el catalogo de errores
    List<CodigoErrorData> listaErrores = codigoErrorDao.findAllByGrupoOrderByCodigoDataAsc("Final Holders");
    XSSFSheet catalogoErrores = workbook.createSheet("CATALOGO ERRORES");
    rowMin = 0;
    // Cabecera
    row = catalogoErrores.createRow(rowMin++);
    cell = null;

    cellnum = 0;
    cell = row.createCell(cellnum);
    cell.setCellValue("Cod. Error");
    cell.setCellStyle(xssfCellStyleHeader);
    cellnum++;// 1
    cell = row.createCell(cellnum);
    cell.setCellValue("Control Realizado");
    cell.setCellStyle(xssfCellStyleHeader);

    for (CodigoErrorData errorData : listaErrores) {
      cellnum = 0;
      
      row = catalogoErrores.createRow(rowMin++);
      cell = row.createCell(cellnum++);
      
      String codigo = errorData.getCodigo().trim();
      cell.setCellValue(codigo.substring(codigo.length() - 2));
      
      cell = row.createCell(cellnum);
      cell.setCellValue(errorData.getDescripcion().trim());
    }

    catalogoErrores.setColumnWidth(0, COLUMN_WITH_SMALL);
    catalogoErrores.setColumnWidth(1, COLUMN_WITH_LARGE);

    return workbook;
  }
  
  /**
   * Method that adds a comment to a given cell 
   * @param factory
   * @param sheet
   * @param cell
   * @param row
   */
  private void createComment(CreationHelper factory, XSSFSheet sheet, XSSFCell cell, XSSFRow row, String commentText) {
    // When the comment box is visible, have it show in a 1x3 space
    ClientAnchor anchor = factory.createClientAnchor();
    anchor.setCol1(cell.getColumnIndex());
    anchor.setCol2(cell.getColumnIndex() + 1);
    anchor.setRow1(row.getRowNum());
    anchor.setRow2(row.getRowNum() + 3);

    Drawing drawing = sheet.createDrawingPatriarch();
    // Create the comment and set the text+author
    Comment comment = drawing.createCellComment(anchor);
    RichTextString str = factory.createRichTextString(commentText);
    comment.setString(str);
    comment.setAuthor("Produbán CORP");

    // Assign the comment to the cell
    cell.setCellComment(comment);

  }

  /**
   * Valida los datos recibidos por el excel y si esta todo ok le inserta, sino
   * devuelve false
   * 
   * @param dto
   * @return
   * @throws SIBBACBusinessException
   */
  private boolean crearTitular(FinalHolderDTO dto) throws SIBBACBusinessException {

    boolean creado = false;

    Map<String, Boolean> errores = new HashMap<String, Boolean>();
    errores.put(ERRORES_DATA, Boolean.FALSE);
    errores.put(ERRORES_DIRECCION, Boolean.FALSE);
    List<String> listadoErrores = new ArrayList<String>();

    validateFinalHolder(dto, listadoErrores, errores, false);

    if (!errores.get(ERRORES_DATA) && !errores.get(ERRORES_DIRECCION)) {
      tmct0fisBo.insertFinalHolder(dto);
      creado = true;
    }
    else {
      if (errores.get(ERRORES_DIRECCION) && !errores.get(ERRORES_DATA)) {
        tmct0fisBo.insertFinalHolder(dto);
      }
      String erroresString = "";
      for (String error : listadoErrores) {
        String codigo = error.trim();
        erroresString += codigo.substring(codigo.length() - 2) + ";";
      }
      dto.setErrores(erroresString);
    }
    return creado;
  }

  /**
   * Valida los datos recibidos por el excel y si esta todo ok le modifica, sino
   * devuelve false
   * 
   * @param dto
   * @return
   * @throws SIBBACBusinessException
   */
  private boolean modificarTitular(FinalHolderDTO dto) throws SIBBACBusinessException {

    boolean modificado = false;

    Map<String, Boolean> errores = new HashMap<String, Boolean>();
    errores.put(ERRORES_DATA, Boolean.FALSE);
    errores.put(ERRORES_DIRECCION, Boolean.FALSE);
    List<String> listadoErrores = new ArrayList<String>();

    validateFinalHolder(dto, listadoErrores, errores, true);

    if (!errores.get(ERRORES_DATA) && !errores.get(ERRORES_DIRECCION)) {

      tmct0fisBo.updateFinalHolder(dto);
      modificado = true;

    }
    else {

      if (errores.get(ERRORES_DIRECCION) && !errores.get(ERRORES_DATA)) {

        tmct0fisBo.updateFinalHolder(dto);
      }

      String erroresString = "";
      for (String error : listadoErrores) {
        String codigo = error.trim();
        if (!codigo.equals(ERROR_100)) {
          erroresString += codigo.substring(codigo.length() - 2) + ";";
        }
        else {
          erroresString += "El numero de documento no existe";
        }
      }

      dto.setErrores(erroresString);
    }

    return modificado;

  }

  /**
   * Valida los datos recibidos por el excel y si esta todo ok le elimina, sino
   * devuelve false
   * 
   * @param dto
   * @return
   * @throws SIBBACBusinessException
   */
  private boolean eliminarTitular(FinalHolderDTO dto) {

    boolean eliminado = false;

    String codFinalHolder = dto.getCodigo();
    if (codFinalHolder != null && !"".equals(codFinalHolder) && codFinalHolder.length() < 40) {
      try {

        tmct0fisBo.deleteFinalHolder(codFinalHolder);
        eliminado = true;
      }
      catch (SIBBACBusinessException e) {
        dto.setErrores("El numero de documento no existe");
      }
    }

    return eliminado;
  }

  /**
   * Dependiendo de la accion enviada en el excel, realiza la accion
   * correspondiente
   * 
   * @param dto
   * @return
   * @throws SIBBACBusinessException
   */
  private boolean realizarTipoAccion(FinalHolderDTO dto) throws SIBBACBusinessException {
    boolean resp = false;

    switch (dto.getTipoAccion()) {
      case ALTA:
        resp = crearTitular(dto);
        break;
      case MODIFICACION:
        resp = modificarTitular(dto);
        break;
      case BAJA:
        resp = eliminarTitular(dto);
        break;
      default:
        break;
    }

    return resp;

  }

  private Workbook openWorkbook(String excelFileName, InputStream inputStream) throws SIBBACBusinessException,
      IOException {
    if ("xlsx".equalsIgnoreCase(getFileExtension(excelFileName)))
      return new XSSFWorkbook(inputStream);
    if ("xls".equalsIgnoreCase(getFileExtension(excelFileName))) {
      POIFSFileSystem fs = new POIFSFileSystem(inputStream);
      return new HSSFWorkbook(fs);
    }
    throw new SIBBACBusinessException("El fichero no corresponde con el formato adecuado para la importacion");
  }

  /**
   * importa el fichero seleccionado por el usuario para cargas los titulares
   * 
   * @param webRequest
   * @param webResponse
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private List<FinalHolderDTO> importarExcelCargaMasiva(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException, IOException {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: importarExcelCargaMasiva] Init");
    String cellValue;
    List<FinalHolderDTO> listFinalHolder = new ArrayList<FinalHolderDTO>();
    Map<String, String> filters = webRequest.getFilters();
    final String excelFile = processString(filters.get(FILTER_FILE));
    final String excelFileName = processString(filters.get(FILTER_FILE_NAME));
    byte[] data = Base64.decodeBase64(excelFile);

    try (InputStream inputStream = new ByteArrayInputStream(data);
        Workbook workbook = openWorkbook(excelFileName, inputStream)) {
      Sheet sheet = workbook.getSheetAt(0);
      FinalHolderDTO dto;
      Iterator<Row> rows = sheet.rowIterator();
      Row rowHead = rows.next();

      while (rows.hasNext()) {
        Row row = rows.next();

        dto = new FinalHolderDTO();

        Iterator<Cell> celdas = rowHead.cellIterator();
        while (celdas.hasNext()) {
          int columnIndex = celdas.next().getColumnIndex();
          Cell celda = row.getCell(columnIndex);
          switch (columnIndex) {
            case TIPO_ACCION:
              dto.setTipoAccion(getCellValue(celda));
              break;

            case TIPO_PERSONA:
              dto.setTipoPersona(getCellValue(celda));
              break;

            case TIPO_DOCUMENTO:
              dto.setTipoDocumento(getCellValue(celda));
              break;

            case DOCUMENTO:
              if ("COD38000001646".equals(getCellValue(celda))) {
                dto.setDocumento(getCellValue(celda));
              }
              dto.setCodigo(getCellValue(celda));
              dto.setDocumento(getCellValue(celda));
              break;

            case TIPO_NACIONALIDAD:
              dto.setTipoNacionalidad(getCellValue(celda));
              break;

            case CODIGO_ISO_NACIONALIDAD:
              dto.setCodNacionalidad(getCellValue(celda));
              break;

            case NOMBRE:
              dto.setNombre(getCellValue(celda));
              break;

            case APELLIDO_1:
              dto.setApellido1(getCellValue(celda));
              break;

            case APELLIDO_2:
              dto.setApellido2(getCellValue(celda));
              break;

            case TIPO_DIR:
              dto.setTipoDireccion(getCellValue(celda));
              break;

            case DOMICILIO:
              dto.setDomicilio(getCellValue(celda));
              break;

            case NUM_DOMICILIO:
              dto.setNumDomicilio(getCellValue(celda));
              break;

            case CIUDAD:
              dto.setCiudad(getCellValue(celda));
              break;

            case PROVINCIA:
              dto.setProvincia(getCellValue(celda));
              break;

            case COD_POSTAL:
              dto.setCodigoPostal(getCellValue(celda));
              break;

            case COD_PAIS_RESIDENCIA:
              dto.setCodPaisResidencia(getCellValue(celda));
              break;

            case TIPO_TITULAR:
              dto.setTipoTitular(getCellValue(celda));
              break;

            case FECHA_NACIMIENTO:
              dto.setFechaNacimiento(getCellValue(celda));
              break;

            case TIPO_MIFID:
              cellValue = getCellValue(celda);
              if (cellValue != null && !cellValue.isEmpty()) {
                dto.setTipoIdMifid(cellValue.charAt(0));
              }
              break;

            case ID_MIFID:
              dto.setIdMifid(getCellValue(celda));
              break;

            case IND_CLIENTE_SAN:
              cellValue = getCellValue(celda);
              if (cellValue != null && !cellValue.isEmpty()) {
                dto.setIndClienteSan(cellValue.charAt(0));
              }
              break;
    
            case NUM_PERSONA_SAN:
              dto.setNumPersonaSan(getCellValue(celda));
              break;
        
            default:
              break;
          }
        }

        if (!realizarTipoAccion(dto)) {
          listFinalHolder.add(dto);
        }
      }

    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: importarExcelCargaMasiva] Final");

    return listFinalHolder;

  }

  /**
   * Recupera la extension de un fichero
   * 
   * @param fileName
   * @return
   */
  private String getFileExtension(String fileName) {

    int mid = fileName.lastIndexOf(".");
    return fileName.substring(mid + 1, fileName.length());
  }

  /**
   * recupera el valor de una celda de excel
   * 
   * @param cell
   * @return
   */
  private String getCellValue(Cell cell) {

    String result = "";

    if (cell != null) {
      switch (cell.getCellType()) {
        case Cell.CELL_TYPE_STRING:
          result = cell.getStringCellValue();
          break;
        case Cell.CELL_TYPE_NUMERIC:
          if (DateUtil.isCellDateFormatted(cell)) {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            result = sdf.format(cell.getDateCellValue());
          }
          else {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            result = cell.getStringCellValue();
          }
          break;
        case Cell.CELL_TYPE_BOOLEAN:
          result = String.valueOf(cell.getBooleanCellValue());
          break;
          
        default:
          break;
      }
    }
    return result.trim();
  }

  /**
   * transforma un string en null
   * 
   * @param filterString
   * @return
   */
  private String processString(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    return processedString;
  }

}
