package sibbac.business.mantenimientodatoscliente.tasks;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.quartz.DateBuilder.IntervalUnit;
import org.apache.commons.io.FileUtils;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0MifidBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.util.FicherosUtils;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

//@SIBBACJob(group = Task.GROUP_MANTENIMIENTO.NAME, name = Task.GROUP_MANTENIMIENTO.JOB_CARGA_MIFID, interval = 1, intervalUnit = IntervalUnit.DAY)
//@DisallowConcurrentExecution
//Ponemos entre comentarios para evitar que salga el job en la pantalla de gestión de procesos.
//eliminar este proceso a partir de la fecha 21/10/2018
public class TaskCargaMIFID extends SIBBACTask {

  private static final Logger LOG = LoggerFactory.getLogger(TaskCargaMIFID.class);

  @Autowired
  private Tmct0MifidBo tmct0MifidBo;

  @Autowired
  private Tmct0menBo tmct0menBo;

  @Value("${partenon.mantenimiento.idoneidad.fisica}")
  private String cuestionarioIdoneidadFisica;

  @Value("${partenon.mantenimiento.conveniencia.fisica}")
  private String cuestionarioConvenienciaFisica;

  @Value("${partenon.mantenimiento.idoneidad.juridica}")
  private String cuestionarioIdoneidadJuridica;

  @Value("${partenon.mantenimiento.conveniencia.juridica}")
  private String cuestionarioConvenienciaJuridica;

  @Value("${ruta_servidor.paternon.in.mifid}")
  private String path;

  @Override
  public void execute() {
    final File dir;
    Tmct0men tmct0men = null;

    LOG.debug("[execute] Iniciando...");
    TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
    TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
    TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
    try {
      LOG.debug("[execute] ==> Detectando si se está ejecutando la tarea ....");
      tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_MANTENIMIENTO_MIFID, estadoIni, estadoExe);

      try {
        dir = new File(path + File.separator);
        LOG.debug("[{}::{}] Init, directorio {}", Task.GROUP_MANTENIMIENTO.NAME,
            Task.GROUP_MANTENIMIENTO.JOB_CARGA_MIFID, dir.getPath());
        cargaFicheroMifid(dir);
        actualizaCaducidades();
        tmct0menBo.putEstadoMEN(tmct0men, estadoIni);
        LOG.info("[execute] execute Se acabó la tarea y se dejó en estado disponible");
      }
      catch (RuntimeException ex) {
        tmct0menBo.putEstadoMEN(tmct0men, estadoError);
        LOG.error("[execute] Error ejecutando la tarea carga MIFID. {}", ex.getMessage(), ex);
      }
    }
    catch (SIBBACBusinessException | RuntimeException e) {
      LOG.error("[execute] Error en el control de ejecución de tarea de carga MIFID", e);
    }
    LOG.debug("[{}::{}] Fin", Task.GROUP_MANTENIMIENTO.NAME, Task.GROUP_MANTENIMIENTO.JOB_CARGA_MIFID);
  }

  private void cargaFicheroMifid(File dir) {
    final List<File> toTratados;
    final File[] foundFiles;
    List<String> lines;
    String fileName = null;

    try {
      foundFiles = dir.listFiles(new MifidFilter());
      LOG.debug("[{}::{}] Encontrados {} que empiecen con 'MIFID'", Task.GROUP_MANTENIMIENTO.NAME,
          Task.GROUP_MANTENIMIENTO.JOB_CARGA_MIFID, foundFiles.length);
      if (foundFiles.length > 0) {
        toTratados = new ArrayList<File>();
        for (File file : foundFiles) {
          fileName = file.getName();
          if (fileName.startsWith("MIFIDCUESTIONARI")) {
            lines = FileUtils.readLines(file, StandardCharsets.ISO_8859_1);
            tmct0MifidBo.tratadoMIFIDCuestionario(lines);
            toTratados.add(file);
          }
          else if (fileName.startsWith("MIFIDTEST")) {
            lines = FileUtils.readLines(file, StandardCharsets.ISO_8859_1);
            tmct0MifidBo.tratadoMIFIDTest(lines);
            toTratados.add(file);
          }
          else if (fileName.startsWith("MIFIDCATEGORIA")) {
            lines = FileUtils.readLines(file, StandardCharsets.ISO_8859_1);
            tmct0MifidBo.tratadoMIFIDCategoria(lines);
            toTratados.add(file);
          }
        }
        FicherosUtils.filesToTratados(toTratados);
      }
    }
    catch (IOException e) {
      LOG.error("[cargaFicheroMifid] No se ha podido abrir el fichero: {} {}", dir, fileName, e);
    }
    catch (SIBBACBusinessException sbe) {
      LOG.error("[cargaFicheroMifid] Error en carga de fichero {} {}", dir, fileName, sbe);
    }
    catch (RuntimeException rex) {
      LOG.error("[cargaFicheroMifid] Error no controlado leyendo el fichero {} {}", dir, fileName, rex);
    }
  }

  private void actualizaCaducidades() throws SIBBACBusinessException {
    int c = 0;

    LOG.debug("[actualizaCaducidades] Inicio");
    c += tmct0MifidBo.actualizaCaducidadesIdoneidad(cuestionarioIdoneidadFisica, "F");
    c += tmct0MifidBo.actualizaCaducidadesIdoneidad(cuestionarioIdoneidadJuridica, "J");
    c += tmct0MifidBo.actualizaCaducidadesConveniencia(cuestionarioConvenienciaFisica, "F");
    c += tmct0MifidBo.actualizaCaducidadesConveniencia(cuestionarioConvenienciaJuridica, "J");
    LOG.debug("[actualizaCaducidades] Fin con {} actualizaciones", c);
  }

  private class MifidFilter implements FilenameFilter {

    public boolean accept(File dir, String name) {
      return name.startsWith("MIFID");
    }

  }

}
