package sibbac.business.mantenimientodatoscliente.rest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.slf4j.LoggerFactory.getLogger;

import com.fasterxml.jackson.databind.ObjectMapper;

import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0fisBo;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fisId;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.filter.FinalHoldersQuery;
import sibbac.business.wrappers.rest.filter.FinalHoldersFilterImpl;
import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.StreamResult;
import sibbac.database.bo.QueryBo;

@RestController
@RequestMapping("/final-holders")
public class SIBBACServiceImportaTitularesDesdeFH {
  
  private static final Logger LOG = getLogger(SIBBACServiceImportaTitularesDesdeFH.class);
  
  @Autowired
  private QueryBo queryBo;
  
  @Autowired
  private Tmct0fisBo fisBo;
  
  @Autowired
  private Tmct0afiBo afiBo;

  @RequestMapping(method = GET)
  public void list(HttpServletRequest req, HttpServletResponse res) throws IOException {
    final FinalHoldersFilterImpl filter;
    final FinalHoldersQuery query;
    final StreamResult streamResult;
    
    try {
      filter = new FinalHoldersFilterImpl(req.getParameterMap());
      streamResult = new HttpStreamResultHelper(res);
      if(!filter.isThereName()) {
        sendError(res, "El nombre es obligatorio");
        return;
      }
      query = new FinalHoldersQuery(filter);
      queryBo.executeQuery(query, FormatStyle.JSON_OBJECTS, streamResult);
    }
    catch(RuntimeException rex) {
      LOG.error("[list] Error no controlado al listar final holders", rex);
      sendError(res, "Error inesperado, vuelva a intentar");
    }
  }
  
  @RequestMapping(value = "/importacion/agrega", method = POST)
  public void agregaTitularesImportados(@RequestBody ImportaFinalHolders importaFinalHolders, 
      HttpServletResponse resp) throws IOException {
    final Map<String, Object> res;

    try {
      for(Tmct0fisId id : importaFinalHolders.getFinalHolders()) {
        Tmct0fis fis = fisBo.findById(id);
        if(fis != null) {
          importaFinalHolders.addFinalHolder(fis);
        }
      }
      res = afiBo.createTitulares(importaFinalHolders);
      sendResultadoImportacion(resp, res);
    }
    catch(RuntimeException rex) {
      LOG.error("[agregaTitularesImportados] Error no controlado en importación", rex);
      sendError(resp, "Error no esperado, vuelva intentar");
    }
  }
  
  @RequestMapping(value = "/importacion/sustituye", method = POST)
  public void sustituyeTitularesImportados(@RequestBody ImportaFinalHolders importaFinalHolders,
      HttpServletResponse resp) throws IOException {
    final Map<String, Object> res;
    
    try {
      for(Tmct0fisId id : importaFinalHolders.getFinalHolders()) {
        Tmct0fis fis = fisBo.findById(id);
        if(fis != null) {
          importaFinalHolders.addFinalHolder(fis);
          break;
        }
      }
      res = afiBo.updateTitularesList(importaFinalHolders);
      sendResultadoImportacion(resp, res);
    }
    catch(RuntimeException rex) {
      LOG.error("[agregaTitularesImportados] Error no controlado en importación", rex);
      sendError(resp, "Error no esperado, vuelva intentar");
    }
  }
  
  private void sendError(HttpServletResponse res, String message, Object... params) throws IOException {
    final String finalMessage;
    
    finalMessage = String.format(message, params);
    res.setStatus(500);
    res.setContentType("text/plain");
    res.setContentLength(finalMessage.length());
    res.getWriter().println(message);
  }
  
  private void sendResultadoImportacion(HttpServletResponse resp, Map<String, Object> res) throws IOException {
    final ObjectMapper mapper;
    
    mapper = new ObjectMapper();
    resp.setContentType("application/json;UTF-8");
    try(PrintWriter writer = resp.getWriter()) {
      mapper.writeValue(writer,  res);
    }
  }
  
}
