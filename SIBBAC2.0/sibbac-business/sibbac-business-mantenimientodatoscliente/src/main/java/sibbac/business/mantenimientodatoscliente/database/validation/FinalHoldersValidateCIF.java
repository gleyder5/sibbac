package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;

/*
 * Validaciones sobre el tipo de documento CIF
 */
public class FinalHoldersValidateCIF implements Validable {
    
    private static final String PROV_NO_RESIDENTE = "00";
    
    private static final Character[] ACCEPTED_INITIAL_LETTERS = { 
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'U', 'V', 'N', 'W', 'P', 'Q', 'S', 'R' };
    
    private static final List<Character> CIF_STARTS_WITH = Arrays.asList(ACCEPTED_INITIAL_LETTERS);

    private final EnumSet<ErrorFinalHolderEnum> errors;
    
	/** CIF value */
	private String nudnicif;
	
    /** Errors accumulator */
    private List<String> result;

    public FinalHoldersValidateCIF() {
        errors = EnumSet.noneOf(ErrorFinalHolderEnum.class);
    }

    public void setNudnicif(String nudnicif) {
		this.nudnicif = nudnicif;
	}
	
	public void setResult(List<String> result) {
		this.result = result;
	}
	
    /**
     * Validate command executor for CIF validations
     */
	@Override
	public void validate() {
	    final char primeraLetra, letraFinal;
	    final String provincia;
	    final int enteroFinal;
	    
		// Validate length field
		if (nudnicif.length() != 9) {
			// "CIF format error: a CIF must have 9 positions"
			errors.add(ErrorFinalHolderEnum.ERROR_41);
		} 
		else {
			if (ValidateString.validateAlphanumericWithoutSpaces(nudnicif)) {
				// CIF field error: only caracters from [0-9] and [A-Z] are admited
			    errors.add(ErrorFinalHolderEnum.ERROR_47);
			} else {
				primeraLetra = nudnicif.charAt(0);
				letraFinal = nudnicif.charAt(nudnicif.length() - 1);
				if(CIF_STARTS_WITH.contains(primeraLetra)){
					switch(primeraLetra) {
					    case 'P': case 'Q': case 'S': case 'W':
					        if(!Character.isLetter(letraFinal)){
	                            // CIF format error: The last character must be a Letter.
	                            errors.add(ErrorFinalHolderEnum.ERROR_37);
	                        }
					        break;
					    case 'A': case 'B': case 'E': case 'H':
    						if(!Character.isDigit(letraFinal)){
    							// CIF format error: The last character must be a number
    							errors.add(ErrorFinalHolderEnum.ERROR_37);
    						}
					}
					
				}
				else {
					// A CIF must begin with one of this letters: A, B, C, D, E, F, G, H, J, U, V, N, W, P, Q, S o R
				    errors.add(ErrorFinalHolderEnum.ERROR_35);
				    errors.add(ErrorFinalHolderEnum.ERROR_36);
				}
                provincia = nudnicif.substring(1, 3);
                if(PROV_NO_RESIDENTE.equals(provincia) &&
                        !Character.isLetter(letraFinal)) {
                    errors.add(ErrorFinalHolderEnum.ERROR_37);
                }
                switch(letraFinal) {
                    case 'J': enteroFinal = 0; break;
                    case 'A': enteroFinal = 1; break;
                    case 'B': enteroFinal = 2; break;
                    case 'C': enteroFinal = 3; break;
                    case 'D': enteroFinal  = 4; break; 
                    case 'E': enteroFinal  = 5; break; 
                    case 'F': enteroFinal  = 6; break;
                    case 'G': enteroFinal  = 7; break;
                    case 'H': enteroFinal  = 8; break;
                    case 'I': enteroFinal  = 9; break;
                    default:
                        if(Character.isDigit(letraFinal))
                            enteroFinal = Character.digit(letraFinal, 10);
                        else
                            enteroFinal = -1;
                }
                if(enteroFinal != calculaDigitoControl())
                    errors.add(ErrorFinalHolderEnum.ERROR_37);
			}
			
		}
        for(ErrorFinalHolderEnum e : errors)
            result.add(e.getError());
	}
	
	/**
	 * Calculo del digito de control como entero
	 * @return el digito de control.
	 */
	int calculaDigitoControl() {
	    final char[] digitosCentrales;
	    char[] digitosDeImpar;
	    int sumaPares = 0, sumaImpares = 0, dobleImpar, valor, digito;
	    
	    digitosCentrales = nudnicif.substring(1,8).toCharArray();
	    for(int i = 0; i < 7; i++) {
	        valor = Character.digit(digitosCentrales[i], 10);
	        if(i % 2 == 0) {
	            dobleImpar = valor * 2;
	            digitosDeImpar = String.valueOf(dobleImpar).toCharArray();
	            valor = 0;
	            for(int j = 0; j < digitosDeImpar.length; j++)
	                valor += Character.digit(digitosDeImpar[j], 10);
	            sumaImpares += valor;
	        }
	        else
	            sumaPares += valor;
	    }
	    digito = (sumaPares + sumaImpares) % 10;
	    return digito > 0 ? 10 - digito : 0;
	}
}
