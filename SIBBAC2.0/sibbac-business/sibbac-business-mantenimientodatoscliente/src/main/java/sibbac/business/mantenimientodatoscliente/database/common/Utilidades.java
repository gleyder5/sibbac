package sibbac.business.mantenimientodatoscliente.database.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;



public class Utilidades {
	
	 public static final String  getCadenaFechaActual(String formato) {

		 Calendar cal = Calendar.getInstance();
		 SimpleDateFormat sdf = new SimpleDateFormat(formato);
		 return sdf.format(cal.getTime());
		 
	 }
	
}
