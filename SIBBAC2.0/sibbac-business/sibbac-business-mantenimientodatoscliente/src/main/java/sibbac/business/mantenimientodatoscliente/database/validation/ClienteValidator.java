package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0provincias;
import sibbac.business.wrappers.database.model.Tmct0swi;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TipoIdMifid;
import sibbac.common.utils.SibbacEnums.TipoCodigoIdentificacion;

/**
 * Clase para validar los registros de final holder
 * 
 * @author miriam.gomez
 *
 */
public class ClienteValidator {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	public static Character FISICA = 'F';
	public static Character NACIONAL = 'N';
	public static Character EXTRANJERO = 'E';
	public static Character JURIDICA = 'J';

	public static Character NIF = 'N';
	public static Character NIE = 'E';
	public static Character CIF = 'C';
	public static Character BIC = 'B';
	public static Character OTROS = 'O';

	public static String ESPCODE = "011";
	public static String TEST_ESPCODE = "011";

	public static String REPRESENTANTE = "R";

	private static final Logger LOG = LoggerFactory.getLogger(ClienteValidator.class);

	/**
	 * Validaciones sobre controles adicionales e imprescindibles
	 * 
	 * @param cliente
	 * @param errores
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private static List<String> validateClienteTypeSociety(Tmct0cli cliente, Tmct0paises pais, Tmct0paises paisBIC,
			Map<String, Boolean> errores) throws SIBBACBusinessException {

		List<String> result = new ArrayList<String>();

		try {

			// Validaciones sobre el campo Type Society
			FinalHoldersValidateTypeSociety validatorTypeSociety = new FinalHoldersValidateTypeSociety();
			validatorTypeSociety.setTpIdenti(cliente.getTpidenti().toString());
			validatorTypeSociety.setTpSociedad(cliente.getTpfisjur().toString());
			validatorTypeSociety.setTypeNationality(cliente.getTpNacTit().toString());
			validatorTypeSociety.setNudnicif(cliente.getCif());
			validatorTypeSociety.setResult(result);
			validatorTypeSociety.validate();

			// Validaciones sobre el codigo de pais

			FinalHoldersValidateCountryCode validatorCountryCode = new FinalHoldersValidateCountryCode();
			validatorCountryCode.setCountryCode(cliente.getAddress().getCdDePais());
			validatorCountryCode.setResult(result);
			validatorCountryCode.validateCountryCode(pais, paisBIC);

			if (!result.isEmpty()) {
				errores.put("erroresData", Boolean.TRUE);
			}

		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage());
			throw new SIBBACBusinessException("Error en la validación de los datos");
		}
		return result;
	}

	/**
	 * Validaciones sobre el pais de residencia
	 * 
	 * @param cliente
	 * @param errores
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private static List<String> validateClienteCross(Tmct0cli cliente, Map<String, Boolean> errores)
			throws SIBBACBusinessException {

		List<String> result = new ArrayList<String>();

		try {

			if (ESPCODE.equals(cliente.getCdNacTit())) { // nacional
				
				if (!(NIF.equals(cliente.getTpidenti()) || CIF.equals(cliente.getTpidenti()))) {
					// Whether 'Country code' field is ES_ESPAÑA, only 'Type
					// identity' NIF or CIF are allowed
					result.add(ErrorFinalHolderEnum.ERROR_64.getError());
				}
			} else { // extranjero

				if (ESPCODE.equals(cliente.getAddress().getCdDePais())) {

					if (!"R".equals(cliente.getTpreside())) {
						result.add(ErrorFinalHolderEnum.ERROR_105.getError());
					}
					
					if (OTROS.equals(cliente.getTpidenti())) {
						// Si el país de residencia es España no puede tener
						// tipo de identificador O de Otros
						result.add(ErrorFinalHolderEnum.ERROR_101.getError());
					}

					if (FISICA.equals(cliente.getTpfisjur())) {

						// En el caso de persona física extranjera residente en
						// España, tiene que tener una E de NIE o una N de NIF
						// siempre que el NIF sea de tipo M, es decir comience
						// por esa letra. NIF de tipo M es para extranjeros que
						// no dispongan de NIE, transitoria o definitivamente.
						
						if (!(NIE.equals(cliente.getTpidenti()) || NIF.equals(cliente.getTpidenti()))) {
							result.add(ErrorFinalHolderEnum.ERROR_102.getError());
						} else if (NIF.equals(cliente.getTpidenti())) {
							if (!cliente.getCif().startsWith("M")) {
								result.add(ErrorFinalHolderEnum.ERROR_67.getError());
							}
						}
						
					}
					if (JURIDICA.equals(cliente.getTpfisjur())) {

						// En el caso de persona jurídica extranjera residente
						// en España, tiene que tener una C de CIF siempre que
						// el CIF sea de tipo N o W, es decir comience por esa
						// letra. CIF de tipo N es para entidades extranjeras,
						// mientras que de tipo W es para establecimientos
						// permanentes de entidades no residentes.
						
						if (!(CIF.equals(cliente.getTpidenti()))) {
							result.add(ErrorFinalHolderEnum.ERROR_103.getError());
						} else if (cliente.getCif() != null && !"".equals(cliente.getCif())) {
							if (!cliente.getCif().startsWith("N") && !cliente.getCif().startsWith("W")) {
								result.add(ErrorFinalHolderEnum.ERROR_68.getError());
							}
						}
						
					}

				} else {

					if (!"NR".equals(cliente.getTpreside())) {
						result.add(ErrorFinalHolderEnum.ERROR_105.getError());
					}
					
				}

			}

			if (!result.isEmpty()) {
				errores.put("erroresData", Boolean.TRUE);
			}

		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage());
			throw new SIBBACBusinessException("Error en la validación de los datos");
		}
		return result;
	}

	/**
	 * validaciones osbre la direccion
	 * 
	 * @param cliente
	 * @param pais
	 * @param tval0pro
	 * @param errores
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private static List<String> validateClienteAddress(Tmct0cli cliente, Tmct0paises pais, Tmct0provincias tval0pro,
			Map<String, Boolean> errores) throws SIBBACBusinessException {

		List<String> result = new ArrayList<String>();

		try {

			FinalHoldersValidateAddress validatorAddress = new FinalHoldersValidateAddress(
					cliente.getAddress().getTpDomici(), cliente.getAddress().getNbDomici(),
					cliente.getAddress().getNbCiudad(), cliente.getAddress().getCdDePais(),
					cliente.getAddress().getCdPostal(), cliente.getAddress().getNbProvin(),
					cliente.getAddress().getNuDomici());
			validatorAddress.setResult(result);
			validatorAddress.validate(pais, tval0pro);

			if (!result.isEmpty()) {
				errores.put("erroresDireccion", Boolean.TRUE);
			}

		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage());
			throw new SIBBACBusinessException("Error en la validación de los datos");
		}
		return result;
	}

	/**
	 * validaciones sobre un titular
	 * 
	 * @param cliente
	 * @param pais
	 * @param paisBIC
	 * @param tmct0swi
	 * @param titularDocumento
	 * @param titularBIC
	 * @param errores
	 * @param isModificar
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private static List<String> validateData(Tmct0cli cliente, Tmct0paises pais, Tmct0paises paisBIC, Tmct0swi tmct0swi,
			Map<String, Boolean> errores, boolean isModificar) throws SIBBACBusinessException {

		List<String> result = new ArrayList<String>();

		try {

			/*
			 * Validaciones sobre los campos nombre y apellidos
			 */

			if (cliente.getCdbrocli() != null && !"".equals(cliente.getCdbrocli())) {

				if (ValidateString.validateAUDWithSpaces(cliente.getCdbrocli().trim())) {
					if (FISICA.equals(cliente.getTpfisjur())) {
						// Name field error: only caracters from [A-Z] are
						// admited
						result.add(ErrorFinalHolderEnum.ERROR_80.getError());
					} else if (JURIDICA.equals(cliente.getTpfisjur())) {
						result.add(ErrorFinalHolderEnum.ERROR_83.getError());
					}
				}

			}

			if (cliente.getDescli() != null && !"".equals(cliente.getDescli())) {

				if (ValidateString.validateAUDWithSpaces(cliente.getDescli().trim())) {
					if (FISICA.equals(cliente.getTpfisjur())) {
						// Name field error: only caracters from [A-Z] are
						// admited
						result.add(ErrorFinalHolderEnum.ERROR_80.getError());
					} else if (JURIDICA.equals(cliente.getTpfisjur())) {
						result.add(ErrorFinalHolderEnum.ERROR_83.getError());
					}
				}

			}

			/*
			 * Required fields
			 */
			if (cliente.getTpidenti() == null || "".equals(cliente.getTpidenti())) {
				// Type Identity field is required
				result.add(ErrorFinalHolderEnum.ERROR_06.getError());
			}

			// Validaciones sobre type society
			if (cliente.getTpfisjur() == null || "".equals(cliente.getTpfisjur())) {
				// Type Society field is required
				result.add(ErrorFinalHolderEnum.ERROR_07.getError());
			} else {
				if (FISICA.equals(cliente.getTpfisjur())) {
					if (!NIF.equals(cliente.getTpidenti()) && !NIE.equals(cliente.getTpidenti())
							&& !OTROS.equals(cliente.getTpidenti())) {
						result.add(ErrorFinalHolderEnum.ERROR_62.getError());
					}
				} else if (JURIDICA.equals(cliente.getTpfisjur())) {
					if (!CIF.equals(cliente.getTpidenti()) && !BIC.equals(cliente.getTpidenti())
							&& !OTROS.equals(cliente.getTpidenti())) {
						result.add(ErrorFinalHolderEnum.ERROR_63.getError());
					}
				}
			}

			if (cliente.getCdNacTit() == null || "".equals(cliente.getCdNacTit().trim())
					|| "-".equals(cliente.getCdNacTit().trim())) {
				// Nationality code field is required
				result.add(ErrorFinalHolderEnum.ERROR_09.getError());
			} else {
				if (cliente.getCdNacTit().length() == 3) {
					if (TEST_ESPCODE.equals(cliente.getCdNacTit()) && !NACIONAL.equals(cliente.getTpNacTit())) {
						// If 'Nationality code' is ES-ESPAÑA then 'Type
						// nationality' must be 'NATIONAL'
						result.add(ErrorFinalHolderEnum.ERROR_60.getError());
					}
					if (!TEST_ESPCODE.equals(cliente.getCdNacTit()) && NACIONAL.equals(cliente.getTpNacTit())) {
						result.add(ErrorFinalHolderEnum.ERROR_61.getError());
					}
				} else {
					// Nationality code field must have 3 positions
					result.add(ErrorFinalHolderEnum.ERROR_24.getError());
				}
			}

			if (cliente.getCdNacTit() == null || "".equals(cliente.getCdNacTit().trim())) {
				// 'Type nationality' field (nacional/extranjero) is required
				result.add(ErrorFinalHolderEnum.ERROR_08.getError());
			}

			// NIF, CIF, NIE cannot be filled at the same time that BIC
			// Comento este error porque no va ser posible que se genere
			/*
			 * if ((cliente.getCif() != null && !
			 * "".equals(cliente.getCif().trim()) && cliente.getCif().length() >
			 * 0) && (cliente.getCif() != null && !
			 * "".equals(cliente.getCif().trim()) && cliente.getCif().length() >
			 * 0)) { //
			 * "DNI/CIF field and BIC field cannot be filled simultaneusly"
			 * result.add(ErrorFinalHolderEnum.ERROR_44.getError()); }
			 */

			/*
			 * Supported values
			 */
			if (!TipoCodigoIdentificacion.CIF.getValue().contains(cliente.getTpidenti().toString())
					&& !TipoCodigoIdentificacion.NIE.getValue().contains(cliente.getTpidenti().toString())
					&& !TipoCodigoIdentificacion.NIF.getValue().contains(cliente.getTpidenti().toString())
					&& !TipoCodigoIdentificacion.BIC.getValue().contains(cliente.getTpidenti().toString())
					&& !TipoCodigoIdentificacion.OTROS.getValue().contains(cliente.getTpidenti().toString())) {
				// 'Type Identity' field error: Values must be one of: N (NIF),
				// C (CIF), E (NIE), B (BIC), O (Otro)
				result.add(ErrorFinalHolderEnum.ERROR_20.getError());
			}

			if (TipoCodigoIdentificacion.CIF.getValue().contains(cliente.getTpidenti().toString())
					|| TipoCodigoIdentificacion.NIE.getValue().contains(cliente.getTpidenti().toString())
					|| TipoCodigoIdentificacion.NIF.getValue().contains(cliente.getTpidenti().toString())) {
				if (!"R".equals(cliente.getTpreside())) {
					result.add(ErrorFinalHolderEnum.ERROR_100.getError());
				}
			} else {
				if (!"NR".equals(cliente.getTpreside())) {
					result.add(ErrorFinalHolderEnum.ERROR_100.getError());
				}
			}

			/*
			 * Validaciones sobre NIF
			 */
			if (NIF.equals(cliente.getTpidenti())) {

				if (cliente.getCif() == null || "".equals(cliente.getCif().trim())) {
					result.add(ErrorFinalHolderEnum.ERROR_05.getError());
				} else {
					FinalHoldersValidateNIF validatorNIF = new FinalHoldersValidateNIF();
					validatorNIF.setNudnicif(cliente.getCif());
					validatorNIF.setResult(result);
					validatorNIF.validate();
				}

			}

			/*
			 * Validaciones sobre NIE
			 */
			if (NIE.equals(cliente.getTpidenti())) {

				if (cliente.getCif() == null || "".equals(cliente.getCif().trim())) {
					result.add(ErrorFinalHolderEnum.ERROR_05.getError());
				} else {
					FinalHoldersValidateNIE validatorNIE = new FinalHoldersValidateNIE();
					validatorNIE.setNudnicif(cliente.getCif());
					validatorNIE.setResult(result);
					validatorNIE.validate();
				}

			}

			// Validaciones sobre CIF
			if (CIF.equals(cliente.getTpidenti())) {

				if (cliente.getCif() == null || "".equals(cliente.getCif().trim())) {
					result.add(ErrorFinalHolderEnum.ERROR_05.getError());
				} else {
					FinalHoldersValidateCIF validatorCIF = new FinalHoldersValidateCIF();
					validatorCIF.setNudnicif(cliente.getCif());
					validatorCIF.setResult(result);
					validatorCIF.validate();
				}

			}

			// Validaciones sobre BIC
			if (BIC.equals(cliente.getTpidenti())) {

				if (cliente.getCif() == null || "".equals(cliente.getCif().trim())) {
					result.add(ErrorFinalHolderEnum.ERROR_05.getError());
				} else {
					FinalHoldersValidateBIC validatorBIC = new FinalHoldersValidateBIC();
					validatorBIC.setBic(cliente.getCif());
					validatorBIC.setResult(result);
					validatorBIC.validateBIC(paisBIC, tmct0swi);
				}

			}

			// Validaciones sobre OTROS
			if (OTROS.equals(cliente.getTpidenti())) {

				if (cliente.getCif() == null || "".equals(cliente.getCif().trim())) {
					result.add(ErrorFinalHolderEnum.ERROR_05.getError());
				} else {
					FinalHoldersValidateOTROS validatorOTROS = new FinalHoldersValidateOTROS();
					validatorOTROS.setOtros(cliente.getCif());
					validatorOTROS.setResult(result);
					validatorOTROS.validate();
				}
			}

			if (!result.isEmpty()) {
				errores.put("erroresData", Boolean.TRUE);
			}

		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage());
			throw new SIBBACBusinessException("Error en la validación de los datos");
		}
		return result;

	}

	/**
	 * Validaciones sobre un titular
	 * 
	 * @param cliente
	 * @param pais
	 * @param paisBIC
	 * @param tmct0swi
	 * @param titularDocumento
	 * @param titularBIC
	 * @param tval0pro
	 * @param errores
	 * @param isModificar
	 * @return
	 * @throws SIBBACBusinessException
	 */
	public static List<String> validateFormat(Tmct0cli cliente, Tmct0paises pais, Tmct0paises paisBIC,
			Tmct0swi tmct0swi, Tmct0provincias tval0pro, Map<String, Boolean> errores, boolean isModificar)
			throws SIBBACBusinessException {
		List<String> result = new ArrayList<String>();
		try {

			result.addAll(validateData(cliente, pais, paisBIC, tmct0swi, errores, isModificar));
			result.addAll(validateClienteAddress(cliente, pais, tval0pro, errores));
			result.addAll(validateClienteTypeSociety(cliente, pais, paisBIC, errores));
			result.addAll(validateClienteCross(cliente, errores));

		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage());
			throw new SIBBACBusinessException("Error en la validación de los datos");
		}
		return result;

	}

}
