package sibbac.business.mantenimientodatoscliente.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fisId;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0afiId;
import sibbac.business.wrappers.rest.FuenteTitulares;

public class ImportaFinalHolders implements FuenteTitulares, Serializable {

  /**
   * Serial number 
   */
  private static final long serialVersionUID = 8588658729549718358L;
  
  private final List<TitularesDTO> titularesDTO;
  
  private final List<PartenonRecordBean> titulares;

  private List<Tmct0fisId> finalHolders;
  
  private List<Tmct0afiId> listAfiIds;

  private List<Long> listAfiErrorIds;
  
  public ImportaFinalHolders() {
    titularesDTO = new ArrayList<>();
    titulares = new ArrayList<>();
  }
  
  public List<Tmct0fisId> getFinalHolders() {
    return finalHolders;
  }
  
  public void setFinalHolders(List<Tmct0fisId> finalHolders) {
    this.finalHolders = finalHolders;
  }

  @Override
  public List<Tmct0afiId> getListAfiIds() {
    return listAfiIds;
  }

  public void setListAfiIds(List<Tmct0afiId> listAfiIds) {
    this.listAfiIds = listAfiIds;
  }

  @Override
  public List<Long> getListAfiErrorIds() {
    return listAfiErrorIds;
  }

  public void setListAfiErrorIds(List<Long> listAfiErrorIds) {
    this.listAfiErrorIds = listAfiErrorIds;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((finalHolders == null) ? 0 : finalHolders.hashCode());
    result = prime * result + ((listAfiErrorIds == null) ? 0 : listAfiErrorIds.hashCode());
    result = prime * result + ((listAfiIds == null) ? 0 : listAfiIds.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ImportaFinalHolders other = (ImportaFinalHolders) obj;
    if (finalHolders == null) {
      if (other.finalHolders != null)
        return false;
    } else if (!finalHolders.equals(other.finalHolders))
      return false;
    if (listAfiErrorIds == null) {
      if (other.listAfiErrorIds != null)
        return false;
    } else if (!listAfiErrorIds.equals(other.listAfiErrorIds))
      return false;
    if (listAfiIds == null) {
      if (other.listAfiIds != null)
        return false;
    } else if (!listAfiIds.equals(other.listAfiIds))
      return false;
    return true;
  }
  
  public String toString() {
    return String.format("ImportaFinalHolders, finalHolders: %d, afis: %d, afiErrors: %d", finalHolders.size(),
        listAfiIds.size(), listAfiErrorIds.size());
  }

  @Override
  public TitularesDTO getTitular() {
    if(titularesDTO.isEmpty()) {
      return null;
    }
    return titularesDTO.get(0);
  }

  @Override
  public List<PartenonRecordBean> listTitulares() {
    return titulares;
  }

  @Override
  public List<TitularesDTO> listTitularesDto() {
    return titularesDTO;
  }
  
  void addFinalHolder(Tmct0fis finalHolder) {
    addPartenon(finalHolder);
    addDto(finalHolder);
  }
  
  private void addPartenon(Tmct0fis finalHolder) {
    final PartenonRecordBean bean;
    
    bean = new PartenonRecordBean();
    bean.setNbclient(finalHolder.getNbclient());
    bean.setNbclient1(finalHolder.getNbclien1());
    bean.setNbclient2(finalHolder.getNbclien2());
    bean.setNifbic(finalHolder.getNudnicif());
    bean.setIndNac(firstChar(finalHolder.getTpnactit()));
    bean.setNacionalidad(finalHolder.getCdnactit());
    bean.setTipoDocumento(firstChar(finalHolder.getTpidenti()));
    bean.setPaisResidencia(finalHolder.getCddepais());
    bean.setProvincia(finalHolder.getNbprovin());
    bean.setPoblacion(finalHolder.getNbciudad());
    bean.setDistrito(finalHolder.getCdpostal());
    bean.setTiptitu('T');
    bean.setTipoPersona(firstChar(finalHolder.getTpsocied()));
    bean.setParticip(new BigDecimal(100));
    // Domicilio
    bean.setTpdomici(finalHolder.getTpdomici());
    bean.setNbdomici(finalHolder.getNbdomici());
    bean.setNudomici(finalHolder.getNudomici());
    titulares.add(bean);
  }
  
  private void addDto(Tmct0fis finalHolder) {
    final TitularesDTO dto;
    
    dto = new TitularesDTO();
    dto.setNbclient(finalHolder.getNbclient());
    dto.setNbclient1(finalHolder.getNbclien1());
    dto.setNbclient2(finalHolder.getNbclien2());
    dto.setNudnicif(finalHolder.getNudnicif());
    dto.setTipnactit(firstChar(finalHolder.getTpnactit()));
    dto.setNacionalidad(finalHolder.getCdnactit());
    dto.setTipdoc(firstChar(finalHolder.getTpidenti()));
    dto.setPaisres(finalHolder.getCddepais());
    dto.setProvincia(finalHolder.getNbprovin());
    dto.setPoblacion(finalHolder.getNbciudad());
    dto.setCodpostal(finalHolder.getCdpostal());
    dto.setTptiprep(firstChar(finalHolder.getTptiprep()));
    dto.setCcv(null);
    dto.setTipopers(firstChar(finalHolder.getTpsocied()));
    dto.setParticip(new BigDecimal(100));
    dto.setTpdomici(finalHolder.getTpdomici());
    dto.setNbdomici(finalHolder.getNbdomici());
    dto.setNudomici(finalHolder.getNudomici());
    dto.setRefCliente(null);
    
    titularesDTO.add(dto);
  }
  
  private Character firstChar(String string) {
    if(string == null || string.isEmpty()) {
      return null;
    }
    return string.charAt(0);
  }
  
}
