package sibbac.business.mantenimientodatoscliente.database.bo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0cliMifidDao;
import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0cliMifidDaoImp;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;
import sibbac.database.bo.AbstractBo;

@Service
@Transactional
public class Tmct0CliMifidBo extends AbstractBo<Tmct0CliMifid, Long, Tmct0cliMifidDao> {

	@Autowired
	private Tmct0cliMifidDaoImp daoImp;

	public List<Tmct0CliMifid> findTmct0CliMifidFiltro(Long idCliente, String categoria, String testConveniencia,
			String testIdoneidad, Date fechaFinVigConv, Date fechaFinVigIdon, String categoriaNot, String testConvenienciaNot, String testIdoneidadNot) {

		return daoImp.findTmct0CliMifidFiltro(idCliente, categoria, testConveniencia, testIdoneidad, fechaFinVigConv,
				fechaFinVigIdon, categoriaNot, testConvenienciaNot, testIdoneidadNot);

	}

}
