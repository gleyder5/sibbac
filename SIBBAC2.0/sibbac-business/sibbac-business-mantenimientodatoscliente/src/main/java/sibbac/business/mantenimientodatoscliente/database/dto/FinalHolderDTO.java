package sibbac.business.mantenimientodatoscliente.database.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.data.CodigoErrorData;

/**
 * DTO para trabajar con final holders
 * 
 * @author miriam.gomez
 *
 */
public class FinalHolderDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6336295126742766529L;

  private String errores;
  private String tipoAccion;
  private String codigo;
  private String tipoPersona;
  private String tipoTitular;
  private String tipoDocumento;
  private String documento;
  private String nombre;
  private String apellido1;
  private String apellido2;
  private String tipoNacionalidad;
  private String nacionalidad;
  private String codNacionalidad;
  private String tipoDireccion;
  private String domicilio;
  private String numDomicilio;
  private String ciudad;
  private String provincia;
  private String codigoPostal;
  private String paisResidencia;
  private String codPaisResidencia;
  private String fechaNacimiento;
  private char tipoIdMifid = ' ';
  private String idMifid = "";
  private char indClienteSan = 'N';
  private String numPersonaSan = "";
  private List<CodigoErrorData> errorsList = new ArrayList<CodigoErrorData>();

  public FinalHolderDTO() {

  }

  public List<CodigoErrorData> getErrorsList() {
    return errorsList;
  }

  public void setErrorsList(List<CodigoErrorData> errorsList) {
    this.errorsList = errorsList;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getTipoPersona() {
    return tipoPersona;
  }

  public void setTipoPersona(String tipoPersona) {
    this.tipoPersona = tipoPersona;
  }

  public String getTipoTitular() {
    return tipoTitular;
  }

  public void setTipoTitular(String tipoTitular) {
    this.tipoTitular = tipoTitular;
  }

  public String getTipoDocumento() {
    return tipoDocumento;
  }

  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  public String getDocumento() {
    return documento;
  }

  public void setDocumento(String documento) {
    this.documento = documento;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido1() {
    return apellido1;
  }

  public void setApellido1(String apellido1) {
    this.apellido1 = apellido1;
  }

  public String getApellido2() {
    return apellido2;
  }

  public void setApellido2(String apellido2) {
    this.apellido2 = apellido2;
  }

  public String getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(String fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  public String getTipoNacionalidad() {
    return tipoNacionalidad;
  }

  public void setTipoNacionalidad(String tipoNacionalidad) {
    this.tipoNacionalidad = tipoNacionalidad;
  }

  public String getNacionalidad() {
    return nacionalidad;
  }

  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  public String getCodNacionalidad() {
    return codNacionalidad;
  }

  public void setCodNacionalidad(String codNacionalidad) {
    this.codNacionalidad = codNacionalidad;
  }

  public String getTipoDireccion() {
    return tipoDireccion;
  }

  public void setTipoDireccion(String tipoDireccion) {
    this.tipoDireccion = tipoDireccion;
  }

  public String getDomicilio() {
    return domicilio;
  }

  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }

  public String getNumDomicilio() {
    return numDomicilio;
  }

  public void setNumDomicilio(String numDomicilio) {
    this.numDomicilio = numDomicilio;
  }

  public String getCiudad() {
    return ciudad;
  }

  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  public String getProvincia() {
    return provincia;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public String getCodigoPostal() {
    return codigoPostal;
  }

  public void setCodigoPostal(String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  public String getPaisResidencia() {
    return paisResidencia;
  }

  public void setPaisResidencia(String paisResidencia) {
    this.paisResidencia = paisResidencia;
  }

  public String getCodPaisResidencia() {
    return codPaisResidencia;
  }

  public void setCodPaisResidencia(String codPaisResidencia) {
    this.codPaisResidencia = codPaisResidencia;
  }

  public String getErrores() {
    return errores;
  }

  public void setErrores(String errores) {
    this.errores = errores;
  }

  public String getTipoAccion() {
    return tipoAccion;
  }

  public void setTipoAccion(String tipoAccion) {
    this.tipoAccion = tipoAccion;
  }

  public char getTipoIdMifid() {
    return tipoIdMifid;
  }

  public String getIdMifid() {
    return idMifid;
  }

  public char getIndClienteSan() {
    return indClienteSan;
  }

  public String getNumPersonaSan() {
    return numPersonaSan;
  }

  public void setTipoIdMifid(char tipoIdMifid) {
    this.tipoIdMifid = tipoIdMifid;
  }

  public void setIdMifid(String idMifid) {
    this.idMifid = idMifid;
  }

  public void setIndClienteSan(char indClienteSan) {
    this.indClienteSan = indClienteSan;
  }

  public void setNumPersonaSan(String numPersonaSan) {
    this.numPersonaSan = numPersonaSan;
  }

  public static FinalHolderDTO convertObjectToClienteDTO(Object[] valuesFromQuery) {

    FinalHolderDTO dto = new FinalHolderDTO();
    dto.setCodigo((valuesFromQuery[0] != null) ? (String) valuesFromQuery[0].toString().trim() : "");
    dto.setTipoPersona((valuesFromQuery[1] != null) ? (String) valuesFromQuery[1].toString().trim() : "");
    dto.setTipoTitular((valuesFromQuery[2] != null) ? (String) valuesFromQuery[2].toString().trim() : "");
    dto.setTipoDocumento((valuesFromQuery[3] != null) ? (String) valuesFromQuery[3].toString().trim() : "");
    dto.setDocumento((valuesFromQuery[4] != null) ? (String) valuesFromQuery[4].toString().trim() : "");
    dto.setNombre((valuesFromQuery[5] != null) ? (String) valuesFromQuery[5].toString().trim() : "");
    dto.setApellido1((valuesFromQuery[6] != null) ? (String) valuesFromQuery[6].toString().trim() : "");
    dto.setApellido2((valuesFromQuery[7] != null) ? (String) valuesFromQuery[7].toString().trim() : "");
    dto.setTipoNacionalidad((valuesFromQuery[8] != null) ? (String) valuesFromQuery[8].toString().trim() : "");
    dto.setCodNacionalidad((valuesFromQuery[9] != null) ? (String) valuesFromQuery[9].toString().trim() : "");
    dto.setNacionalidad((valuesFromQuery[10] != null) ? (String) valuesFromQuery[10].toString().trim() : "");
    dto.setTipoDireccion((valuesFromQuery[11] != null) ? (String) valuesFromQuery[11].toString().trim() : "");
    dto.setDomicilio((valuesFromQuery[12] != null) ? (String) valuesFromQuery[12].toString().trim() : "");
    dto.setNumDomicilio((valuesFromQuery[13] != null) ? (String) valuesFromQuery[13].toString().trim() : "");
    dto.setCiudad((valuesFromQuery[14] != null) ? (String) valuesFromQuery[14].toString().trim() : "");
    dto.setProvincia((valuesFromQuery[15] != null) ? (String) valuesFromQuery[15].toString().trim() : "");
    dto.setCodigoPostal((valuesFromQuery[16] != null) ? (String) valuesFromQuery[16].toString().trim() : "");
    dto.setCodPaisResidencia((valuesFromQuery[17] != null) ? (String) valuesFromQuery[17].toString().trim() : "");
    dto.setPaisResidencia((valuesFromQuery[18] != null) ? (String) valuesFromQuery[18].toString().trim() : "");

    Date miFechaNacimiento = null;
    String sFecha = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    if (valuesFromQuery[19] != null) {
      miFechaNacimiento = (Date) valuesFromQuery[19];
      sFecha = sdf.format(miFechaNacimiento);

    }
    dto.setFechaNacimiento(sFecha);

    // CAMPOS MIFID II
    dto.setTipoIdMifid((valuesFromQuery[20] != null) ? (Character) valuesFromQuery[20] : ' ');
    dto.setIdMifid((valuesFromQuery[21] != null) ? (String) valuesFromQuery[21].toString().trim() : "");
    dto.setIndClienteSan((valuesFromQuery[22] != null) ? (Character) valuesFromQuery[22] : ' ');
    dto.setNumPersonaSan((valuesFromQuery[23] != null) ? (String) valuesFromQuery[23].toString().trim() : "");

    return dto;
  }

  @Override
  public String toString() {
    return "FinalHolderDTO [errores=" + errores + ", tipoAccion=" + tipoAccion + ", codigo=" + codigo
        + ", tipoPersona=" + tipoPersona + ", tipoTitular=" + tipoTitular + ", tipoDocumento=" + tipoDocumento
        + ", documento=" + documento + ", nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2
        + ", fechaNacimiento=" + fechaNacimiento + ", tipoNacionalidad=" + tipoNacionalidad + ", nacionalidad="
        + nacionalidad + ", codNacionalidad=" + codNacionalidad + ", tipoDireccion=" + tipoDireccion + ", domicilio="
        + domicilio + ", numDomicilio=" + numDomicilio + ", ciudad=" + ciudad + ", provincia=" + provincia
        + ", codigoPostal=" + codigoPostal + ", paisResidencia=" + paisResidencia + ", codPaisResidencia="
        + codPaisResidencia + ", tipoIdMifid=" + tipoIdMifid + ", idMifid=" + idMifid + ", indClienteSan="
        + indClienteSan + ", numPersonaSan=" + numPersonaSan + "]";
  }

}
