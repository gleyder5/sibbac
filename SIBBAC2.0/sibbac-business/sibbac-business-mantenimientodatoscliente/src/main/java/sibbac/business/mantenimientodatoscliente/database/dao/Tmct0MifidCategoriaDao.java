package sibbac.business.mantenimientodatoscliente.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.mantenimientodatoscliente.database.model.Tmct0MifidCategoria;

@Repository
public interface Tmct0MifidCategoriaDao extends JpaRepository <Tmct0MifidCategoria, String>{

}
