package sibbac.business.mantenimientodatoscliente.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.Tmct0swiBo;
import sibbac.business.wrappers.database.model.Tmct0swi;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * The Class SIBBACServiceTmct0swi - Servicio para la importacion del fichero SWIFT
 */
@SIBBACService
public class SIBBACServiceTmct0swi implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceTmct0swi.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES 
  /** Constantes del proceso */
  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  private static final String NOMBRE_SERVICIO = "SIBBACServiceTmct0swi";

  private static final String FILTER_FILE = "file";

  private static final String FILTER_FILENAME = "filename";

  /** The swift bo. */
  @Autowired
  private Tmct0swiBo tmct0swiBo;

  private enum ComandosFicheroSwi {
    /** Inicializa los datos del filtro de pantalla */
    INICIALIZA_PANTALLA("init"),

    /**Importa fichero swift. */
    IMPORTAR_FICHERO("importarFichero"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    private ComandosFicheroSwi(String command) {
      this.command = command;
    }

    public String getCommand() {
      return command;
    }
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosFicheroSwi getCommand(String command) {
    ComandosFicheroSwi[] commands = ComandosFicheroSwi.values();
    ComandosFicheroSwi result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosFicheroSwi.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
   
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosFicheroSwi command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceTmct0Fis");

      switch (command) {
        case IMPORTAR_FICHERO:
          this.importarFichero(webRequest, result);
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceTmct0Fis");
    return result;
  }

  /**
   * metodo que importa el fichero de swift
   * 
   * @param webRequest
   * @param webResponse
   * @throws SIBBACBusinessException
   */
  @SuppressWarnings("deprecation")
  //@Transactional(propagation = Propagation.REQUIRES_NEW)
  private void importarFichero(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {

    LOG.debug("[" + NOMBRE_SERVICIO + " :: importarFichero] Inicio");
    Map<String, String> filters = webRequest.getFilters();
    Map<String, Object> resultados = new HashMap<String, Object>();
    final String fichero = processString(filters.get(FILTER_FILE));
    final String ficheroName = processString(filters.get(FILTER_FILENAME));

    LOG.debug("[" + NOMBRE_SERVICIO + " :: Procesamos el fichero]  " + ficheroName);

    StringTokenizer st = new StringTokenizer(fichero, "\n");
    ArrayList<Tmct0swi> datosSwiOk = new ArrayList<Tmct0swi>();
    boolean bErrores = false;
    StringBuffer sbLineasError = new StringBuffer();
    int numLinea = 1;
    int numErrores = 0;
    String sLinea = null;
    String sCodigo = null;
    String sEntidad = null;
    String sSucursal = null;

    while (st.hasMoreTokens()) {
      sLinea = st.nextToken();
      
      if (sLinea != null && sLinea.length() > 17) {  // tiene que tener por lo menos los caracteres de la clave primaria.

        sCodigo = sLinea.substring(3, 14);

        // El codigo no puede contener ni espacios en blanco y las posiciones 5 y 6 no pueden tener numeros.
        if ((sCodigo.indexOf(" ") > 0) || Character.isDigit(sCodigo.charAt(4)) || Character.isDigit(sCodigo.charAt(5))) {
          LOG.error("[" + NOMBRE_SERVICIO + " :: Error detectado en la linea] Numero de linea " + numLinea
                    + " Contiene datos erroneos en BIC: o espacios en blanco o valor invalido del pais ");
          bErrores = true;
          if (numErrores == 0) {
            sbLineasError.append(numLinea);
          } else {
            sbLineasError.append(" - ").append(numLinea);
          }
          numErrores++;
        }

        if  (sLinea.length() >= 119){ // Tiene caracteres de la descripción de la entidad
          sEntidad = sLinea.substring(14, 119).trim();
          // Comprobamos si tambien hay caracteres para la sucursal
          if  (sLinea.length() >= 178) {
            sSucursal = sLinea.substring(119, 178).trim();          
          }else{
            sSucursal = sLinea.substring(119, sLinea.length()).trim();
          }
          
        }else{
          sEntidad = sLinea.substring(14, sLinea.length()).trim();
        }

        StringBuffer sbDescripcion = new StringBuffer();
        sbDescripcion.append(sEntidad).append(" ").append(sSucursal);

        if (!bErrores) {
          Tmct0swi swiNuevo = new Tmct0swi(sCodigo, sbDescripcion.toString());
          datosSwiOk.add(swiNuevo);
        }

      } else {
        LOG.error("[" + NOMBRE_SERVICIO + " :: Error detectado en la linea] Numero de linea " + numLinea + " Es vacia o no tiene la longitud permitida");
        bErrores = true;
        if (numErrores == 0) {
          sbLineasError.append(numLinea);
        } else {
          sbLineasError.append(" - ").append(numLinea);
        }
        numErrores++;

      }

      numLinea++;

    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: Fichero PROCESADO]  ");
    
    // Si no se han producido errores, se pasa al bo las lista de objetos para que borre los anteriores y cargue los
    // nuevos.
    if (!bErrores) {

      // Se llama al bo para que borre los datos actuales y cargue los nuevos.
      LOG.debug("[" + NOMBRE_SERVICIO
                + " :: Fichero con los datos Ok]  Se procede al borrado y carga de los nuevos datos ");
      tmct0swiBo.recargaDatosTabla(datosSwiOk);
      LOG.debug("[" + NOMBRE_SERVICIO + " :: Recarda de datos Ok ]");
      resultados.put("status", "OK");
      webResponse.setResultados(resultados);

    } else {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: El fichero contiene ERRORES - NO SE PROCESA ]");
      resultados.put("status", "KO");
      webResponse.setResultados(resultados);

      if (numErrores > 1) {
        webResponse.setError("Las lineas "
                             + sbLineasError.append(" contienen errores. Revise los datos del fichero.").toString());
      } else {
        webResponse.setError("La linea: "
                             + sbLineasError.append(" contiene errores. Revise los datos del fichero.").toString());
      }

    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: Procesadas  ] " + numLinea + " Lineas");
    LOG.debug("[" + NOMBRE_SERVICIO + " :: importarFichero] fin");
  }

  /**
   * @param filtros
   * @return
   */
  private String processString(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    return processedString;
  }

  @Override
  public List<String> getAvailableCommands() {

    return null;
  }

  @Override
  public List<String> getFields() {

    return null;
  }

}
