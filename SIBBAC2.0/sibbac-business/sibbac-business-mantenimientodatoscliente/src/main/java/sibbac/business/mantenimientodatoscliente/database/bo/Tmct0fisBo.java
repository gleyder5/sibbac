package sibbac.business.mantenimientodatoscliente.database.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.RollbackException;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0fisDao;
import sibbac.business.mantenimientodatoscliente.database.dto.FinalHolderDTO;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fisId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

@Service
public class Tmct0fisBo extends AbstractBo<Tmct0fis, Tmct0fisId, Tmct0fisDao> {

  private final String fechaFin = "99991231";

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0fisBo.class);

  @Autowired
  private HttpSession session;

  public Tmct0fis getFisById(String codclave, BigDecimal numsec) {
    return dao.getFisById(codclave, numsec);
  }

  public List<Tmct0fis> getFisBynbciudad(String sCiudad) {
    return dao.findBynbciudad(sCiudad);
  }

  @Transactional
  public Tmct0fis deleteFinalHolder(String codigo) throws SIBBACBusinessException {
    Tmct0fis finalHolder = getFisById(codigo, BigDecimal.ONE);
    if (finalHolder == null) {
      throw new SIBBACBusinessException("No existe el final Holder buscado");
    }
    else {
      finalHolder
          .setFhfinal(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date())));
      this.save(finalHolder);
    }

    return finalHolder;
  }

  @Transactional(readOnly = false)
  public Tmct0fis insertFinalHolder(FinalHolderDTO dto) throws SIBBACBusinessException {
    Tmct0fis finalHolder = null;
    try {
      finalHolder = getFisById(dto.getCodigo(), BigDecimal.ONE);
      if (finalHolder != null) {
        throw new SIBBACBusinessException("Existe un Final Holder con el código introducido");
      }
      else {

        String sUsuario = (((UserSession) session.getAttribute("UserSession")).getName().length() >= 10) ? ((UserSession) session
            .getAttribute("UserSession")).getName().substring(0, 10) : ((UserSession) session
            .getAttribute("UserSession")).getName();

        Tmct0fisId id = new Tmct0fisId(dto.getCodigo(), BigDecimal.ONE);
        finalHolder = new Tmct0fis();
        finalHolder.setId(id);
        finalHolder.setCdholder(dto.getDocumento());
        if (dto.getTipoDocumento().equals("B") || dto.getTipoDocumento().equals("O")) {
          int valor = (dto.getDocumento().length() >= 11) ? 11 : dto.getDocumento().length();
          finalHolder.setCdddebic(dto.getDocumento().substring(0, valor));
          finalHolder.setNudnicif("");
        }
        else {
          int valor = (dto.getDocumento().length() >= 10) ? 10 : dto.getDocumento().length();
          finalHolder.setNudnicif(dto.getDocumento().substring(0, valor));
          finalHolder.setCdddebic("");
        }
        String tipoDireccion = dto.getTipoDireccion() != null && dto.getTipoDireccion().length() > 2 ? dto
            .getTipoDireccion().substring(0, 2) : dto.getTipoDireccion();
        finalHolder.setTpdomici(tipoDireccion);
        finalHolder.setNbdomici(dto.getDomicilio());
        finalHolder.setNudomici(dto.getNumDomicilio());
        finalHolder.setNbciudad(dto.getCiudad());
        finalHolder.setNbprovin(dto.getProvincia());
        finalHolder.setCdpostal(dto.getCodigoPostal());
        finalHolder.setCddepais(dto.getCodPaisResidencia());
        finalHolder.setNbclient(dto.getNombre());
        finalHolder.setNbclien1(dto.getApellido1());
        finalHolder.setNbclien2(dto.getApellido2());
        finalHolder.setTptiprep(dto.getTipoTitular());
        finalHolder.setCdnactit(dto.getCodNacionalidad());
        finalHolder.setFhmodifi(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
            .convertDateToString(new Date())));
        finalHolder.setHrmodifi(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
            .convertTimeToString(new Date())));
        finalHolder.setFechaNacimiento(FormatDataUtils.convertStringToDate(dto.getFechaNacimiento(), "dd/MM/yyyy"));
        finalHolder.setCdusuari(sUsuario);
        int valor = (dto.getDocumento().length() >= 25) ? 25 : dto.getDocumento().length();
        finalHolder.setCdordtit(dto.getDocumento().substring(0, valor));
        valor = (dto.getDocumento().length() >= 16) ? 16 : dto.getDocumento().length();
        finalHolder.setRftitaud(dto.getDocumento().substring(0, valor));
        finalHolder.setTpidenti(dto.getTipoDocumento());
        finalHolder.setTpsocied(dto.getTipoPersona());
        finalHolder.setTpnactit(dto.getTipoNacionalidad());
        finalHolder.setCdusuaud(sUsuario);
        finalHolder.setFhaudit(new Date());
        finalHolder.setFhinicio(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
            .convertDateToString(new Date())));
        finalHolder.setFhfinal(FormatDataUtils.convertStringToBigDecimal(fechaFin));
        finalHolder.setCategory("");
        finalHolder.setCdrefban("");
        finalHolder.setTipoIdMifid(dto.getTipoIdMifid());
        finalHolder.setIdMifid(dto.getIdMifid());
        finalHolder.setIndClienteSan(dto.getIndClienteSan());
        finalHolder.setNumPersonaSan(dto.getNumPersonaSan());
        LOG.info("LOG: Before saving finalHolder: " + finalHolder);
        this.save(finalHolder);
      }
    }
    catch (RollbackException ex) {
      LOG.error(ex.getLocalizedMessage());
    }

    return finalHolder;
  }

  @Transactional
  public Tmct0fis updateFinalHolder(FinalHolderDTO dto) throws SIBBACBusinessException {
    Tmct0fis finalHolder = getFisById(dto.getCodigo(), BigDecimal.ONE);
    if (finalHolder == null) {
      throw new SIBBACBusinessException("No existe el final Holder buscado");
    }
    else {

      String sUsuario = (((UserSession) session.getAttribute("UserSession")).getName().length() >= 10) ? ((UserSession) session
          .getAttribute("UserSession")).getName().substring(0, 10)
          : ((UserSession) session.getAttribute("UserSession")).getName();

      finalHolder.setCdholder(dto.getDocumento());
      if (dto.getTipoDocumento().equals("B") || dto.getTipoDocumento().equals("O")) {
        int valor = (dto.getDocumento().length() >= 11) ? 11 : dto.getDocumento().length();
        finalHolder.setCdddebic(dto.getDocumento().substring(0, valor));
        finalHolder.setNudnicif("");
      }
      else {
        int valor = (dto.getDocumento().length() >= 10) ? 10 : dto.getDocumento().length();
        finalHolder.setNudnicif(dto.getDocumento().substring(0, valor));
        finalHolder.setCdddebic("");
      }
      String tipoDireccion = dto.getTipoDireccion() != null && dto.getTipoDireccion().length() > 2 ? dto
          .getTipoDireccion().substring(0, 2) : dto.getTipoDireccion();
      finalHolder.setTpdomici(tipoDireccion);
      finalHolder.setNbdomici(dto.getDomicilio());
      finalHolder.setNudomici(dto.getNumDomicilio());
      finalHolder.setNbciudad(dto.getCiudad());
      finalHolder.setNbprovin(dto.getProvincia());
      finalHolder.setCdpostal(dto.getCodigoPostal());
      finalHolder.setCddepais(dto.getCodPaisResidencia());
      finalHolder.setNbclient(dto.getNombre());
      finalHolder.setNbclien1(dto.getApellido1());
      finalHolder.setNbclien2(dto.getApellido2());
      finalHolder.setTptiprep(dto.getTipoTitular());
      finalHolder.setCdnactit(dto.getCodNacionalidad());
      finalHolder
          .setFhmodifi(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date())));
      finalHolder
          .setHrmodifi(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertTimeToString(new Date())));
      finalHolder.setFechaNacimiento(FormatDataUtils.convertStringToDate(dto.getFechaNacimiento(), "dd/MM/yyyy"));
      finalHolder.setCdusuari(sUsuario);
      int valor = (dto.getDocumento().length() >= 25) ? 25 : dto.getDocumento().length();
      finalHolder.setCdordtit(dto.getDocumento().substring(0, valor));
      valor = (dto.getDocumento().length() >= 16) ? 16 : dto.getDocumento().length();
      finalHolder.setRftitaud(dto.getDocumento().substring(0, valor));
      finalHolder.setTpidenti(dto.getTipoDocumento());
      finalHolder.setTpsocied(dto.getTipoPersona());
      finalHolder.setTpnactit(dto.getTipoNacionalidad());
      finalHolder.setCdusuaud("");
      finalHolder.setFhaudit(new Date());
      finalHolder.setCategory("");
      finalHolder.setCdrefban("");
      finalHolder.setCdusuaud(sUsuario);
      
      finalHolder.setTipoIdMifid(dto.getTipoIdMifid());
      finalHolder.setIdMifid(dto.getIdMifid());
      finalHolder.setIndClienteSan(dto.getIndClienteSan());
      finalHolder.setNumPersonaSan(dto.getNumPersonaSan());
      this.save(finalHolder);
    }

    return finalHolder;
  }

} // Tmct0fisBo

