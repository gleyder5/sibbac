package sibbac.business.mantenimientodatoscliente.database.dto;

public class ConcatDTO {
  // Indica si hay o no error
  private int sError;
  // Si no hay error, valor del concat
  // Si hay error, descripción del error.
  private String sConcat;

  public ConcatDTO() {

  }

  public ConcatDTO(int sError, String sConcat) {
    this.sError = sError;
    this.sConcat = sConcat;
  }

  /**
   * @return the sError
   */
  public int getsError() {
    return sError;
  }

  /**
   * @param sError the sError to set
   */
  public void setsError(int sError) {
    this.sError = sError;
  }

  /**
   * @return the sConcat
   */
  public String getsConcat() {
    return sConcat;
  }

  /**
   * @param sConcat the sConcat to set
   */
  public void setsConcat(String sConcat) {
    this.sConcat = sConcat;
  }

}
