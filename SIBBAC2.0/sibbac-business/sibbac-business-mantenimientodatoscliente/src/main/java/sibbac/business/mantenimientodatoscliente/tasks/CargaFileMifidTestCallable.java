package sibbac.business.mantenimientodatoscliente.tasks;

import java.util.concurrent.Callable;

import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import sibbac.business.mantenimientodatoscliente.database.bo.Tmct0MifidBo;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class CargaFileMifidTestCallable implements Callable<Integer> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CargaFileMifidTestCallable.class);

  private LineIterator lineIterator;

  @Autowired
  private Tmct0MifidBo tmct0MifidBo;

  public CargaFileMifidTestCallable(LineIterator lineIterator) {
    super();
    this.lineIterator = lineIterator;
  }

  @Override
  public Integer call() throws SIBBACBusinessException {
    Exception ex = null;
    int count = 0;
    String line;

    LOG.debug("Entramos en el call de CargaFileMifidTestCallable... ");

    while ((line = this.getNextFromLineIterator(lineIterator)) != null) {
      try {
        count += tmct0MifidBo.saveMifidTestLineTransactional(line);
      }
      catch (DataIntegrityViolationException dataEx) {
        LOG.warn("Se ha detectado una línea duplicada en el fichero. " + line, dataEx.getMessage());
      }
      catch (ObjectOptimisticLockingFailureException opEx) {
        LOG.debug("#### interbloqueo!! linea: " + line);
      }
      catch (Exception e) {
        LOG.debug("Incidencia procesando linea {} ", line, e);
        ex = e;
      }
    }
    if (ex != null) {
      throw new SIBBACBusinessException(ex);
    }
    LOG.debug("Entramos en el call de CargaFileMifidTestCallable... Lineas procesadas (no lineas cargadas): " + count);
    return count;
  }

  /*
   * Controlamos que la ejecución concurrente de los hilos accedan al LineIterator de manera sincronizada
   */
  private String getNextFromLineIterator(LineIterator li) {
    String line = null;
    synchronized (li) {
      if (li.hasNext()) {
        line = li.next();
      }
    }
    return line;
  }
}