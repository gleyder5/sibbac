package sibbac.business.mantenimientodatoscliente.database.validation;

import java.util.List;

import sibbac.business.mantenimientodatoscliente.database.common.ErrorFinalHolderEnum;
import sibbac.business.wrappers.database.model.Tmct0paises;


/*
 * Validaciones sobre el pais de residencia
 */
public class FinalHoldersValidateCountryCode implements Validable {

	// CountryCode value
	
		private String countrycode;
		public void setCountryCode(String countrycode) {
			this.countrycode = countrycode;
		}
				
	// Errors accumulator
		private List<String> result;
		public void setResult(List<String> result) {
			this.result = result;
		}
				

	
	@Override
	public void validate() {
		
	
		
	}
	
	public void validateCountryCode( Tmct0paises pais,Tmct0paises paisBIC) {

		int contador_letras=0;
		
		
		if (countrycode.length() != 3) {
			result.add(ErrorFinalHolderEnum.ERROR_26.getError());
		} else {
			for(int i=0; i<3;i++){
				if(Character.isLetter(countrycode.charAt(i))){
					contador_letras+=1;
				}
			}
			
			if(contador_letras==0){
				//Compruebo el valor del codigo de nacionalidad
				
				if (pais == null && paisBIC == null) {
					result.add(ErrorFinalHolderEnum.ERROR_84.getError());
				}
			}else{
				result.add(ErrorFinalHolderEnum.ERROR_26.getError());
			}
		}
		
		
	}

}
