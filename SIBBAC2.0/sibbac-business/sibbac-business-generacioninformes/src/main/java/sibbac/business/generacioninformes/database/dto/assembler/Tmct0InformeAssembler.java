package sibbac.business.generacioninformes.database.dto.assembler;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosFiltrosDao;
import sibbac.business.generacioninformes.database.dto.ComponenteDirectivaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeFilterDTO;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosFiltros;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosId;
import sibbac.business.generacioninformes.database.model.Tmct0InformesTipos;
import sibbac.business.generacioninformes.helper.GeneracionInformesHelper;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;


/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_INFORMES
 * @author Neoris
 *
 */
@Service
public class Tmct0InformeAssembler {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformeAssembler.class);
  
  final SimpleDateFormat formatDate = new SimpleDateFormat(FormatDataUtils.DATE_FORMAT_1);
  
  @Autowired
  private Tmct0InformesDao     tmct0InformesDao;  

  @Autowired
  private Tmct0cfgDao tmct0cfgDao;
  
  @Autowired
  private Tmct0InformesDatosFiltrosDao tmct0InformesDatosFiltrosDao;
  
  @Autowired
  private Tmct0InformesFiltrosAssembler tmct0InformesFiltrosAssembler;
  
  public Tmct0InformeAssembler() {
    super();
  }

  /**
   * Realiza la conversion entre la entidad y el dto
   * @param datosInforme -> Entidad que se quiere trasformar en DTO
   * @return  DTO con los datos de la entidad tratados
   */
  public Tmct0InformeDTO getTmct0InformeDto(Tmct0Informes datosInforme) {
    
    Tmct0InformeDTO miDTO = new Tmct0InformeDTO();
    
    List<SelectDTO> listaCamposDetalle  = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposAgrupacion = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposOrdenacion = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposCabecera = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposSeparacion = new ArrayList<SelectDTO>();
    List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
    List<SelectDTO> listaIsin = new ArrayList<SelectDTO>();
    
    // Valores directos
    miDTO.setIdInforme(datosInforme.getId());
    miDTO.setNbdescription(datosInforme.getNbdescription());
    miDTO.setNbidioma(datosInforme.getNbidioma());
    miDTO.setTotalizar(datosInforme.getTotalizar());
    miDTO.setNbtxtlibre(datosInforme.getNbtxtlibre());

    // Formamos los campos detalle y las listas de campos detalle, ordenacion, agrupacion, cabecera y separacion
    
    StringBuffer sCamposDetalle = new StringBuffer();
    int i = 0;
    SelectDTO datoInforme;
            
    for (Tmct0InformesDatos campoInforme : datosInforme.getTmct0InformesDatosList()) {
      
      switch (campoInforme.getId().getIdTipo().getId()) {
        case 0:  // Campo detalle
          if (i > 0){
            sCamposDetalle.append(",").append(campoInforme.getId().getIdCampo().getNbpantalla());
          }else{
            sCamposDetalle.append(campoInforme.getId().getIdCampo().getNbpantalla());
          }
          i++;          
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla(), String.valueOf(campoInforme.getId().getIdCampo().getTotalizar()));
          listaCamposDetalle.add(datoInforme);
          break;
        case 1:  // Campo ordenacion
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla());
          listaCamposOrdenacion.add(datoInforme);
          break;
        case 2:  // Campo agrupacion  
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla(),String.valueOf(campoInforme.getId().getIdCampo().getTotalizar()));
          listaCamposAgrupacion.add(datoInforme);
          break;
        case 3:  // Campo separacion  
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla());
          listaCamposSeparacion.add(datoInforme);
          break;
        case 4:  // Campo cabecera  
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla());
          listaCamposCabecera.add(datoInforme);
          break;
        default:
          break;
      }
    }
    
    // Asignamos la cadena de campos detalle
    if (sCamposDetalle != null) {
      miDTO.setCamposDetalle(sCamposDetalle.toString());
    }
    
    // Consultamos los filtros adicionales
    List<Tmct0InformesDatosFiltros> filtrosAdicionales = tmct0InformesDatosFiltrosDao.findByIdInforme(datosInforme.getId());
    if (null != filtrosAdicionales && !filtrosAdicionales.isEmpty()) {
    	miDTO.setFiltrosAdicionales("Si");
    } else {
    	miDTO.setFiltrosAdicionales("No");
    }
    
    
    // Asignamos las listas.
    miDTO.setListaCamposDetalle(listaCamposDetalle);
    miDTO.setListaCamposOrdenacion(listaCamposOrdenacion);
    miDTO.setListaCamposAgrupacion(listaCamposAgrupacion);
    miDTO.setListaCamposSeparacion(listaCamposSeparacion);
    miDTO.setListaCamposCabecera(listaCamposCabecera);
    miDTO.setListaAlias(listaAlias);
    miDTO.setListaCdisin(listaIsin);
    
    return miDTO;
    
  }
  
  /**
   * Realiza la conversion entre el DTO y la entidad
   * @param datosInformeDTO -> Dto que se quiere trasformar en Entidad
   * @return  Entidad con los datos del DTO tratados
   */
  public Tmct0Informes  getTmct0InformeEntidad ( Tmct0InformeDTO datosInformeDTO) {
    
    Tmct0Informes miEntidad = new Tmct0Informes();

    List<Tmct0InformesDatos> tmct0InformesDatosList = new ArrayList<Tmct0InformesDatos>();
    

    if (datosInformeDTO.getIdInforme()!= 0){
       miEntidad = tmct0InformesDao.findOne(datosInformeDTO.getIdInforme());
    }

    // Valores directos
    miEntidad.setNbdescription(datosInformeDTO.getNbdescription());
    miEntidad.setNbidioma(datosInformeDTO.getNbidioma());
    miEntidad.setTotalizar(datosInformeDTO.getTotalizar());
    miEntidad.setNbtxtlibre(datosInformeDTO.getNbtxtlibre());
    
    Tmct0InformesDatos campoInformeEnt;
    Tmct0InformesDatosId datosInformePK;
    Tmct0InformesCampos tmct0InformeCampos ;
    Tmct0InformesTipos  tmct0InformesTipos ;
    
     
   
    // Se recorren cada una de las listas y se forman los campos que componen el listado.
    
    // Campos detalle TIPO 0
    int iPosicion = 1;   
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposDetalle()) {
      
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(0);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion); 
      tmct0InformesDatosList.add(campoInformeEnt);      
      iPosicion++;
    }
    
    // Campos Ordenacion TIPO 1
    iPosicion = 1;
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposOrdenacion()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(1);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      
      
      
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Agrupacion TIPO 2
    iPosicion = 1;
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposAgrupacion()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(2);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Separacion TIPO 3
    iPosicion = 1;
    
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposSeparacion()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(3);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Cabecera TIPO 4
    iPosicion = 1;
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposCabecera()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(4);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Asigno la lista de campos a la entidad
    miEntidad.setTmct0InformesDatosList(tmct0InformesDatosList);
    
    return miEntidad;
    
  }
  

  /**
   * Realiza la conversion entre los valores recogidos de la pantalla y el DTO
   * @param paramsObjects ->  Valores que recibe el service de la pantalla y que se quieren transformar en DTO.
   * @return  Entidad con los datos del DTO tratados
   */
  public Tmct0InformeDTO getTmct0InformeDto(Map<String, Object> paramsObjects){
    
    
      Tmct0InformeDTO miDTO = new Tmct0InformeDTO();
      
      StringBuffer sCamposDetalle = new StringBuffer();
      int i = 0;

      
      //List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
       
      List<SelectDTO> listaCamposDetalle  = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposAgrupacion = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposOrdenacion = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposCabecera = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposSeparacion = new ArrayList<SelectDTO>();
      List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCdisin = new ArrayList<SelectDTO>();
    

      if (paramsObjects.get("idInforme").toString() != ""){
        miDTO.setIdInforme(Integer.parseInt(paramsObjects.get("idInforme").toString()));  
      }else{
        // Se hace esto porque al guardar la identidad si el id es nulo casca y para que genere uno nuevo tiene que tener el valor 0
        miDTO.setIdInforme(0);
      }
      
      miDTO.setNbdescription(String.valueOf(paramsObjects.get("nbdescription")));
      

      miDTO.setNbidioma(String.valueOf(paramsObjects.get("nbidioma")));
      
      miDTO.setTotalizar(String.valueOf(paramsObjects.get("totalizar")));
     
      if (paramsObjects.get("nbtxtlibre") != null){
        miDTO.setNbtxtlibre(String.valueOf(paramsObjects.get("nbtxtlibre")));  
      }
      
      
      int iAux = 0;
      StringBuffer sbAlias = new StringBuffer();
      SelectDTO selectDTO;
      List<Map<String,String>> aliasSeleccionados = (List<Map<String,String>>) paramsObjects.get("listaAlias");
      for(Map<String,String> alias : aliasSeleccionados){
        selectDTO = new SelectDTO(String.valueOf(alias.get("key")),  String.valueOf(alias.get("value")));
        listaAlias.add(selectDTO);
        if (iAux == 0){
          sbAlias.append("'").append(String.valueOf(alias.get("key")).trim()).append("'");  
        }else{
          sbAlias.append(",").append("'").append(String.valueOf(alias.get("key")).trim()).append("'");
        }
         
        iAux++;
      }
      
      
      miDTO.setNbalias(sbAlias.toString());
      
      
      // Generación del isin.
      iAux = 0;
      StringBuffer sbIsin = new StringBuffer();
      List<Map<String,String>> isinSeleccionados = (List<Map<String,String>>) paramsObjects.get("listaCdisin");
      for(Map<String,String> isin : isinSeleccionados){
        selectDTO = new SelectDTO(String.valueOf(isin.get("key")),  String.valueOf(isin.get("value")));
        listaCdisin.add(selectDTO);
        if (iAux == 0){
          sbIsin.append("'").append(String.valueOf(isin.get("key")).trim()).append("'");  
        }else{
          sbIsin.append(",").append("'").append(String.valueOf(isin.get("key")).trim()).append("'");
        }
         
        iAux++;
      }
      
      
      miDTO.setCdisin(sbIsin.toString());
      
      
      
      List<Map<String,String>> camposDetalle = (List<Map<String,String>>) paramsObjects.get("listaCamposDetalle");
      for(Map<String,String> cDetalle : camposDetalle){
        selectDTO = new SelectDTO(String.valueOf(cDetalle.get("key")), String.valueOf(cDetalle.get("value")));
        listaCamposDetalle.add(selectDTO);
        
        if (i > 0){
          sCamposDetalle.append(",").append(String.valueOf(cDetalle.get("value")));
        }else{
          sCamposDetalle.append(String.valueOf(cDetalle.get("value")));
        }
        i++; 
        
      }
      
      
      miDTO.setCamposDetalle(sCamposDetalle.toString());
      
      List<Map<String,String>> camposAgrupacion = (List<Map<String,String>>) paramsObjects.get("listaCamposAgrupacion");
      for(Map<String,String> cAgrupacion : camposAgrupacion){
        selectDTO = new SelectDTO(String.valueOf(cAgrupacion.get("key")), String.valueOf(cAgrupacion.get("value")));
        listaCamposAgrupacion.add(selectDTO);
      }
      
      List<Map<String,String>> camposOrdenacion = (List<Map<String,String>>) paramsObjects.get("listaCamposOrdenacion");
      for(Map<String,String> cOrdenacion: camposOrdenacion){
        selectDTO = new SelectDTO(String.valueOf(cOrdenacion.get("key")), String.valueOf(cOrdenacion.get("value")));
        listaCamposOrdenacion.add(selectDTO);
      }
      
      List<Map<String,String>> camposCabecera = (List<Map<String,String>>) paramsObjects.get("listaCamposCabecera");
      for(Map<String,String> cCabecera : camposCabecera){
        selectDTO = new SelectDTO(String.valueOf(cCabecera.get("key")), String.valueOf(cCabecera.get("value")));
        listaCamposCabecera.add(selectDTO);
      }
      
      List<Map<String,String>> camposSeparacion = (List<Map<String,String>>) paramsObjects.get("listaCamposSeparacion");
      for(Map<String,String> cSeparacion : camposSeparacion){
        selectDTO = new SelectDTO(String.valueOf(cSeparacion.get("key")), String.valueOf(cSeparacion.get("value")));
        listaCamposSeparacion.add(selectDTO);
      }
      
      miDTO.setListaCamposDetalle(listaCamposDetalle);
      miDTO.setListaCamposOrdenacion(listaCamposOrdenacion);
      miDTO.setListaCamposAgrupacion(listaCamposAgrupacion);
      miDTO.setListaCamposSeparacion(listaCamposSeparacion);
      miDTO.setListaCamposCabecera(listaCamposCabecera);
      miDTO.setListaAlias(listaAlias);
      miDTO.setListaCdisin(listaCdisin);
      
      return miDTO;
    
  }
  
  public Tmct0InformeFilterDTO getTmct0InformeFilterDto(Map<String, Object> paramsObjects) throws SIBBACBusinessException{
	  
      Tmct0InformeFilterDTO miFilterDTO = new Tmct0InformeFilterDTO();

	  /** Inicio - Componente filtros dinamicos. */
	  miFilterDTO.setListComponenteDirectivaDTO(
			  this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOFromLHM(
					  (List<List<LinkedHashMap>>) paramsObjects.get("listaFiltrosDinamicos")));
	  /** Fin - Componente filtros dinamicos. */
    
      miFilterDTO.setPlantilla(String.valueOf(paramsObjects.get("plantilla")));
      miFilterDTO.setNotPlantilla(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notPlantilla"))));
      miFilterDTO.setClase(String.valueOf(paramsObjects.get("clase")));
      miFilterDTO.setNotClase(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notClase"))));
      miFilterDTO.setIdioma(String.valueOf(paramsObjects.get("idioma")));
      miFilterDTO.setNotIdioma(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notIdioma"))));
      miFilterDTO.setMercado(String.valueOf(paramsObjects.get("mercado")));
      miFilterDTO.setNotMercado(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notMercado"))));
      miFilterDTO.setTotalizar(String.valueOf(paramsObjects.get("totalizar")));
      miFilterDTO.setNotTotalizar(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notTotalizar"))));
      miFilterDTO.setAlias(String.valueOf(paramsObjects.get("alias")));
      miFilterDTO.setNotAlias(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notAlias"))));
      miFilterDTO.setIsin(String.valueOf(paramsObjects.get("isin")));
      miFilterDTO.setNotIsin(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notIsin"))));
      
      SelectDTO selectDTO; 
      List<SelectDTO> listaCamposDetalle  = new ArrayList<SelectDTO>();
      List<Map<String,String>> camposDetalle = (List<Map<String,String>>) paramsObjects.get("camposDetalle");
      for(Map<String,String> cDetalle : camposDetalle){
        selectDTO = new SelectDTO(String.valueOf(cDetalle.get("key")), String.valueOf(cDetalle.get("value")));
        listaCamposDetalle.add(selectDTO);
      }
      
      miFilterDTO.setCamposDetalle(listaCamposDetalle);
      miFilterDTO.setNotCamposDetalle(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notCamposDetalle"))));
      
      return miFilterDTO;
    
  }

  public Tmct0GeneracionInformeFilterDTO getTmct0GeneracionInformeFilterDto(
	Map<String, Object> paramsObjects) throws ParseException, SIBBACBusinessException {

    LOG.debug("Inicio de la transformación Tmct0GeneracionInformeFilterDTO");
    
    Tmct0GeneracionInformeFilterDTO miFilterDTO = new Tmct0GeneracionInformeFilterDTO();
    
    /** Inicio - Componente filtros dinamicos. */
    miFilterDTO.setListComponenteDirectivaDTO(
    	this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOFromLHM(
    		(List<List<LinkedHashMap>>) paramsObjects.get("listaFiltrosDinamicos")));
    /** Fin - Componente filtros dinamicos. */
    
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");    
    if (paramsObjects.get("plantilla") != ""){
      miFilterDTO.setIdInforme(Integer.parseInt(String.valueOf(paramsObjects.get("plantilla"))));
    }
    
    if (paramsObjects.get("fecContratDesd") != "") {
      miFilterDTO.setFechaContratacionDesde(formatter.parse(String.valueOf(paramsObjects.get("fecContratDesd"))));      
    }
    
    if (paramsObjects.get("fecContratHast") != "") {
      miFilterDTO.setFechaContratacionHasta(formatter.parse(String.valueOf(paramsObjects.get("fecContratHast"))));
    }
    
    if (paramsObjects.get("fecLiquiDesd") != "") {
      miFilterDTO.setFechaLiquidacionDesde(formatter.parse(String.valueOf(paramsObjects.get("fecLiquiDesd"))));
    }
    
    if (paramsObjects.get("fecLiquiHast") != "") {
      miFilterDTO.setFechaLiquidacionHasta(formatter.parse(String.valueOf(paramsObjects.get("fecLiquiHast"))));
    }
    
    //se tratan los alias que vienen en una lista
    StringBuffer sbAlias = new StringBuffer();
   
    if (paramsObjects.get("listaAlias") != null) {
      if (paramsObjects.get("listaAlias") !=  "") {
        int iAux = 0;
        SelectDTO selectDTO;
        List<Map<String,String>> aliasSeleccionados = (List<Map<String,String>>) paramsObjects.get("listaAlias");
        for(Map<String,String> alias : aliasSeleccionados){
          selectDTO = new SelectDTO(String.valueOf(alias.get("key")),  String.valueOf(alias.get("value")));
          if (iAux == 0){
            sbAlias.append("'").append(String.valueOf(alias.get("key")).trim()).append("'");  
          }else{
            sbAlias.append(",").append("'").append(String.valueOf(alias.get("key")).trim()).append("'");
          }
           
          iAux++;
        }
        miFilterDTO.setAlias(sbAlias);
      }
    }
    
    
    //se tratan los alias que vienen en una lista
    StringBuffer sbIsin = new StringBuffer();
    if (paramsObjects.get("listaCdisin") != null) {
      if (paramsObjects.get("listaCdisin") !=  "") {
        int iAux = 0;
        SelectDTO selectDTO;
        List<Map<String,String>> isinSeleccionados = (List<Map<String,String>>) paramsObjects.get("listaCdisin");
        for(Map<String,String> isin : isinSeleccionados){
          selectDTO = new SelectDTO(String.valueOf(isin.get("key")),  String.valueOf(isin.get("value")));
          if (iAux == 0){
            sbIsin.append("'").append(String.valueOf(isin.get("key")).trim()).append("'");  
          }else{
            sbIsin.append(",").append("'").append(String.valueOf(isin.get("key")).trim()).append("'");
          }
           
          iAux++;
        }
        miFilterDTO.setIsin(sbIsin);  
      }
    }
    
    if (paramsObjects.get("especiales") != "") {
      miFilterDTO.setOpcionesEspeciales(String.valueOf(paramsObjects.get("especiales")));
    }
    
    LOG.debug("Fin de la transformación Tmct0GeneracionInformeFilterDTO");
    return miFilterDTO;
  }
  
  /**
   * 	Devuelve una fecha corroborada como dia habil en forma de Sring.
   * 	@param fecha
   * 	@param operador: si es: 
   * 	(+) -> incrementa hasta encontrar siguiente dia habil 
   * 	(-) -> Decrementa hasta encontrar siguiente dia habil
   * 	@throws SIBBACBusinessException 
   * 	@throws ParseException 
   */
  public String getFechaHabil(
  	  String fecha, String operador) throws SIBBACBusinessException, ParseException {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date fechaDate = sdf.parse(fecha);
    Calendar c = Calendar.getInstance();
    c.setTime(fechaDate);
    while (!this.isDiaHabil(fechaDate)) {
  	  if (operador.equals("+")) {
  		  c.add(Calendar.DATE, 1);
  		  fechaDate = c.getTime();
  	  } else if (operador.equals("-")) {
  		  c.add(Calendar.DATE, (-1) * 1);
  		  fechaDate = c.getTime();
  	  } else if (operador.equals("=")) {
  		  fechaDate = c.getTime();
  		  break;
  	  }
    }
    return sdf.format(fechaDate);
  }
  
  /**
   *	Devuelve true si se devuelve un dia habil (ni feriado ni festivo). 
   * 	@return boolean
   * 	@throws SIBBACBusinessException
   */
  private boolean isDiaHabil(Date fechaEjecucion) throws SIBBACBusinessException {
	  return !Utilidad.isWeekEnd(fechaEjecucion) 
			  && CollectionUtils.isEmpty(this.tmct0cfgDao.findByProcessKeyNameLikeAndKeyValue(
					  "CONFIG", "%holydays.anual.days%", formatDate.format(fechaEjecucion)));
  }
  
  /**
   *	Complementa la query del componente generico.
   *	@param listComponenteDirectivaDTO
   *	@return complQuery
   *	@throws SIBBACBusinessException 
   */
  public String getQueryFromComponenteGenerico(
		  List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO, String query) throws SIBBACBusinessException {
	  StringBuffer complQuery = new StringBuffer();
	  if (CollectionUtils.isNotEmpty(listComponenteDirectivaDTO)) {
		  for (List<ComponenteDirectivaDTO> compDirDTOList : listComponenteDirectivaDTO) {
			  for (ComponenteDirectivaDTO compDirDTO : compDirDTOList) {
				  if (StringUtils.isNotBlank(compDirDTO.getTabla()) 
						  && query.toUpperCase().contains(compDirDTO.getTabla().toUpperCase())) {
	
					  // Para tipo AUTORRELLENABLE se compara con el label de sentido.
					  StringBuffer sbComplemento = new StringBuffer();
					  if (compDirDTO.isTipoAutorrellenable() && compDirDTO.getLblBtnSentidoFiltro().equals("<>")) {
						  sbComplemento.append(" NOT ");
					  }
					  
					  // Caso en que se pasa un valor si o si que se va a usar posteriormente.
					  if (StringUtils.isNotBlank(compDirDTO.getValorCampo()) 
							  || CollectionUtils.isNotEmpty(compDirDTO.getListaSeleccSelectDTO())) {
						  complQuery.append(" AND " + sbComplemento.toString() + compDirDTO.getTabla() + "." + compDirDTO.getCampo());
					  }
					  
					  // Tipos que usan valorCampo
					  if (StringUtils.isNotBlank(compDirDTO.getValorCampo())) {
						  if (compDirDTO.isTipoTexto() || compDirDTO.isTipoAutorrellenable() || compDirDTO.isTipoCombo()) {
							  // Para tipos TEXTO, COMBO y AUTORRELLENABLE solo se compara con un valor.
							  complQuery.append(" = '" + compDirDTO.getValorCampo() + "'");
						  } else if (compDirDTO.isTipoFecha()) {
							  try {
								  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
								  SimpleDateFormat sdfDB = new SimpleDateFormat("yyyy-MM-dd");
								  // Para tipos FECHA se utiliza BETWEEN en subtipo DESDE-HASTA
								  if (compDirDTO.isSubtipoDesdeHasta()) {
									  complQuery.append(" >= TO_DATE('" + sdfDB.format(sdf.parse(compDirDTO.getValorCampo())) + "', 'YYYY-MM-DD')"
											  + " AND " + compDirDTO.getTabla() + "." + compDirDTO.getCampo() + " <= TO_DATE('" + sdfDB.format(sdf.parse(compDirDTO.getValorCampoHasta())) + "', 'YYYY-MM-DD')");
								  } else {
									  complQuery.append(" = TO_DATE('" + sdfDB.format(sdf.parse(compDirDTO.getValorCampo())) + "', 'YYYY-MM-DD')");
								  }
							  } catch (ParseException e) {
								  LOG.info("Error al parsear fecha: " + e);
							  }
						  }
					  }
					  
					  // Tipos que usan listaSeleccSelectDTO
					  if (CollectionUtils.isNotEmpty(compDirDTO.getListaSeleccSelectDTO())) {
						  if (compDirDTO.isTipoLista()) {
							  // Para tipos LISTA se usa operador IN ya que pueden venir varios valores
							  complQuery.append(" IN('" + GeneracionInformesHelper.getDatosListaFormateados(compDirDTO.getListaSeleccSelectDTO()) + "')");						  
						  }
					  }
				  }
			  }
		  }
	  }
	  return complQuery.toString();
  }
  
}