package sibbac.business.generacioninformes.tasks;

import sibbac.common.SIBBACBusinessException;

@SuppressWarnings("serial")
public class BatchException extends SIBBACBusinessException {

   public BatchException(String message) {
     super(message);
   }
   
   public BatchException(String message, Exception cause) {
     super(message, cause);
   }
   
   
}
