package sibbac.business.generacioninformes.database.dto;

/**
 * DTO para cargar las listas en la vista con angular
 * 
 * @author mario.flores
 *
 */
public class InformeDTO {

  /**
   * key - Clave del elemento
   */
  private String key;
  /**
   * Value - Valor del elemento
   */
  private String value;

  /**
   * posicion - posicion
   */
  private Integer posicion;
  /**
   * fecha base - fecha base
   */
  private String fechaBase;
  
  /**
   * clase - clase
   */
  private String clase;

  public InformeDTO(String key, String value, Integer posicion, String fechaBase, String clase) {
    this.key = key;
    this.value = value;
    this.posicion = posicion;
    this.fechaBase=fechaBase;
    this.clase=clase;
  }

public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

/**
 * @return the posicion
 */
public Integer getPosicion() {
	return posicion;
}

/**
 * @param posicion the posicion to set
 */
public void setPosicion(Integer posicion) {
	this.posicion = posicion;
}

/**
 * @return the fechaBase
 */
public String getFechaBase() {
	return fechaBase;
}

/**
 * @param fechaBase the fechaBase to set
 */
public void setFechaBase(String fechaBase) {
	this.fechaBase = fechaBase;
}

/**
 * @return the clase
 */
public String getClase() {
	return clase;
}

/**
 * @param clase the clase to set
 */
public void setClase(String clase) {
	this.clase = clase;
}

}
