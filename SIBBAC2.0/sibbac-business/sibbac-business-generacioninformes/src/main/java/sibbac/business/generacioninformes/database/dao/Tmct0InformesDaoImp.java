package sibbac.business.generacioninformes.database.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.dto.Tmct0InformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.wrappers.common.SelectDTO;

/*
 * the class Tmct0InformesDaoImp para la consulta de los informes con el filtro
 */
@Repository
public class Tmct0InformesDaoImp {

  @Autowired
  Tmct0InformeAssembler tmct0InformeAssembler;

  @Autowired
  Tmct0InformesDao tmct0InformesDao;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesDaoImp.class);

  @PersistenceContext
  private EntityManager em;

  public List<Tmct0Informes> findPlantillasInformesFiltro(Tmct0InformeFilterDTO datosFiltro) {

    List<Tmct0Informes> listaPlantillasInforme = new ArrayList<Tmct0Informes>();

    Map<String, Object> parameters = new HashMap<String, Object>();
    LOG.debug("findPlantillasInformesFiltro - Entro en el metodo");

    LOG.debug("Filtro Recibido: ");

    // Se forma la consulta con los id informes que cumplen todos los requisitos

    final StringBuilder select = new StringBuilder(" SELECT inf ");
    final StringBuilder from = new StringBuilder(" FROM Tmct0Informes inf ");
    final StringBuilder where = new StringBuilder(" WHERE 1=1 AND inf.idPadre IS NULL AND inf.estado = 'A'  ");
    final StringBuilder order = new StringBuilder(" ORDER BY inf.nbdescription  ");

    // Filtro por la plantilla elegida
    if (datosFiltro.getPlantilla() != null && !datosFiltro.getPlantilla().equals("")) {

      if (datosFiltro.isNotPlantilla()) {
        LOG.debug("Filtro Recibido - Plantilla distinto: " + datosFiltro.getPlantilla());
        where.append(" AND inf.id != :plantilla");
      } else {
        LOG.debug("Filtro Recibido - Plantilla igual: " + datosFiltro.getPlantilla());
        where.append(" AND inf.id = :plantilla");
      }
      parameters.put("plantilla", Integer.parseInt(datosFiltro.getPlantilla()));

    }

    // Filtro por la clase elegida
    if (datosFiltro.getClase() != null && !datosFiltro.getClase().equals("")) {

      if (datosFiltro.isNotClase()) {
        LOG.debug("Filtro Recibido - Clase distinto: " + datosFiltro.getClase());
        where.append(" AND inf.tmct0InformesClases.id != :clase");
      } else {
        LOG.debug("Filtro Recibido - Clase igual: " + datosFiltro.getPlantilla());
        where.append(" AND inf.tmct0InformesClases.id = :clase");
      }
      parameters.put("clase", Integer.valueOf(datosFiltro.getClase()));

    }

    // Filtro por el idioma
    if (datosFiltro.getIdioma() != null && !datosFiltro.getIdioma().equals("")) {

      if (datosFiltro.isNotIdioma()) {
        LOG.debug("Filtro Recibido - Idioma distinto: " + datosFiltro.getIdioma());
        where.append(" AND inf.nbidioma != :idioma");
      } else {
        LOG.debug("Filtro Recibido - Idioma igual: " + datosFiltro.getIdioma());
        where.append(" AND inf.nbidioma = :idioma");
      }
      parameters.put("idioma", datosFiltro.getIdioma());

    }

    // Filtro por el mercado
    if (datosFiltro.getMercado() != null && !datosFiltro.getMercado().equals("")) {

      if (datosFiltro.isNotMercado()) {
        LOG.debug("Filtro Recibido - Mercado distinto: " + datosFiltro.getMercado());
        where.append(" AND inf.mercado != :mercado");
      } else {
        LOG.debug("Filtro Recibido - Mercado igual: " + datosFiltro.getMercado());
        where.append(" AND inf.mercado = :mercado");
      }
      parameters.put("mercado", datosFiltro.getMercado());

    }

    // Filtro por campos detalle
    if (datosFiltro.getCamposDetalle().size() > 0) {

      StringBuffer sIdentificadoresCampos = new StringBuffer("");
      int inumCamposAux = 0;

      for (SelectDTO campoDTO : datosFiltro.getCamposDetalle()) {
        if (inumCamposAux == 0) {
          sIdentificadoresCampos.append(campoDTO.getKey());
        } else {
          sIdentificadoresCampos.append(",").append(campoDTO.getKey());
        }
        inumCamposAux++;
      }

      if (datosFiltro.isNotCamposDetalle()) {
    	  // Cogemos los informes que tengan algun campo excluidos los que nos pasan
    	  where.append(" AND inf.id NOT IN ( SELECT DISTINCT(ic.tmct0InformesDatosId.informe.id)  FROM Tmct0InformesDatos ic WHERE ");
    	  where.append(" ic.tmct0InformesDatosId.tipoCampo.idTipo = 0 AND ic.tmct0InformesDatosId.campoInforme.id IN (");
    	  where.append(sIdentificadoresCampos.toString());
    	  where.append(") GROUP BY ic.tmct0InformesDatosId.informe.id HAVING COUNT(*) >= :numCampos) ");
    	  parameters.put("numCampos", Long.valueOf((datosFiltro.getCamposDetalle().size())));
      } else {
        // Cogemos los informes que tengan como minimo los que nos pasan
//        where.append(" AND inf.id IN ( SELECT DISTINCT(ic.tmct0InformesDatosId.informe.id)  FROM Tmct0InformesDatos ic WHERE  ic.tmct0InformesDatosId.tipoCampo.idTipo = 0  AND ic.tmct0InformesDatosId.campoInforme.id  IN (  ")
//             .append(sIdentificadoresCampos.toString())
//             .append(") AND GROUP BY ic.tmct0InformesDatosId.informe.id  HAVING COUNT(1) >= :numCampos) ");
//        parameters.put("numCampos", datosFiltro.getCamposDetalle().size());
    	  where.append(" AND inf.id IN ( SELECT DISTINCT(ic.tmct0InformesDatosId.informe.id)  FROM Tmct0InformesDatos ic WHERE ");
    	  where.append(" ic.tmct0InformesDatosId.tipoCampo.idTipo = 0 AND ic.tmct0InformesDatosId.campoInforme.id IN (");
    	  where.append(sIdentificadoresCampos.toString());
    	  where.append(") GROUP BY ic.tmct0InformesDatosId.informe.id HAVING COUNT(*) >= :numCampos) ");
    	  parameters.put("numCampos", Long.valueOf((datosFiltro.getCamposDetalle().size())));
      }

    }

    // Filtro por totalizar
    if (datosFiltro.getTotalizar() != null && !datosFiltro.getTotalizar().equals("")) {

      if (datosFiltro.isNotTotalizar()) {
        LOG.debug("Filtro Recibido - Totalizar distinto: " + datosFiltro.getTotalizar());
        where.append(" AND inf.totalizar != :totalizar");
      } else {
        LOG.debug("Filtro Recibido - Totalizar igual: " + datosFiltro.getTotalizar());
        where.append(" AND inf.totalizar = :totalizar");
      }
      parameters.put("totalizar", datosFiltro.getTotalizar());

    }

    // Filtro por Alias
    if (datosFiltro.getAlias() != null && !datosFiltro.getAlias().equals("")) {
      if (datosFiltro.isNotAlias()) {
        LOG.debug("Filtro Recibido - Alias distinto: " + datosFiltro.getTotalizar());
        where.append(" AND  inf.nbalias NOT LIKE :nbalias ");
      } else {
        LOG.debug("Filtro Recibido - Alias igual: " + datosFiltro.getTotalizar());
        where.append(" AND  inf.nbalias LIKE :nbalias   ");
      }

      parameters.put("nbalias",  "%'" + datosFiltro.getAlias() + "'%");

    }

    // Filtro por Isin
    if (datosFiltro.getIsin() != null && !datosFiltro.getIsin().equals("")) {

      if (datosFiltro.isNotIsin()) {
        LOG.debug("Filtro Recibido - Isin distinto: " + datosFiltro.getIsin());
        where.append(" AND inf.cdisin NOT LIKE :cdisin ");
      } else {
        LOG.debug("Filtro Recibido - Isin igual: " + datosFiltro.getIsin());
        where.append(" AND inf.cdisin LIKE :cdisin ");
      }
      parameters.put("cdisin",  "%'" + datosFiltro.getIsin()+ "'%");

    }

    String sQuery = select.append(from.append(where.append(order))).toString();
    Query query = null;

    LOG.debug("Creamos y ejecutamos la query: " + sQuery);

    query = em.createQuery(sQuery);

    for (Entry<String, Object> entry : parameters.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }

    // Se forman todas las entidades imforme con los datos recuperados.
    List<Tmct0Informes> listaObjetosResultado = query.getResultList();

    LOG.debug("Query ejecutada, obtenidos " + listaObjetosResultado.size() + " registros ");

    LOG.debug("findPlantillasInformesFiltro - Se sale del metodo");
    
    return listaObjetosResultado;
  }

}
