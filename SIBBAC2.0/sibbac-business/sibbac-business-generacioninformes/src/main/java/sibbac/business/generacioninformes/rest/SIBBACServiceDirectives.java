package sibbac.business.generacioninformes.rest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.generacioninformes.database.dao.Tmct0InformesClasesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosFiltrosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesFiltrosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesFiltrosDaoImp;
import sibbac.business.generacioninformes.database.dto.ComponenteDirectivaDTO;
import sibbac.business.generacioninformes.database.dto.ValoresPrecargadosFiltroDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformesFiltrosAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosFiltros;
import sibbac.business.generacioninformes.rest.SIBBACServicePlantillasInformesNEW.EnuMercado;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 *	Corresponde a un servicio usado para la creacion de directivas. 
 */
@SIBBACService
public class SIBBACServiceDirectives implements SIBBACServiceBean {

	  /** The Constant LOG. */
	  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceDirectives.class);

	  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	  @Autowired
	  private Tmct0InformesDao tmct0InformesDao;
	  
	  @Autowired
	  private Tmct0InformesFiltrosDao tmct0InformesFiltrosDao;
	  
	  @Autowired
	  private Tmct0InformesFiltrosAssembler tmct0InformesFiltrosAssembler;
	  
	  @Autowired
	  private Tmct0InformesDatosFiltrosDao tmct0InformesDatosFiltrosDao;

	  @Autowired
	  private Tmct0InformesClasesDao tmct0InformesClasesDao;

	  /**
	   * The Enum para los tipos de campos del informe
	   */
	  public enum EnuTipoCampo {

	    DETALLE(0),
	    ORDENACION(1),
	    AGRUPACION(2),
	    SEPARADOR(3);

	    private final int value;

	    EnuTipoCampo(int value) {
	      this.value = value;
	    }

	    public int getValue() {
	      return value;
	    }

	  }

	  /**
	   * The Enum ComandosPlantillas.
	   */
	  private enum ComandosPlantillas {

	    /** Inicializa los datos del filtro de pantalla */
		DEVOLVER_ELEMENTOS("DevolverElementos"),
		DEVOLVER_ELEMENTOS_BY_PARAMS("DevolverElementosByParams"),
	    DEVOLVER_IDS_ELEMENTOS("DevolverIdsElementos"),
	    DEVOLVER_VALOR_PRECARGADO("DevolverValorPrecargado"),
	    SIN_COMANDO("");

	    /** The command. */
	    private final String command;

	    /**
	     * Instantiates a new comandos Plantillas.
	     *
	     * @param command the command
	     */
	    private ComandosPlantillas(String command) {
	      this.command = command;
	    }

	    /**
	     * Gets the command.
	     *
	     * @return the command
	     */
	    private String getCommand() {
	      return command;
	    }

	  }

	  /**
	   * The Enum Campos plantilla .
	   */
	  private enum FieldsPlantilla {
	    PLANTILLA("plantilla");

	    /** The field. */
	    private String field;

	    /**
	     * Instantiates a new fields.
	     *
	     * @param field the field
	     */
	    private FieldsPlantilla(String field) {
	      this.field = field;
	    }

	    /**
	     * Gets the field.
	     *
	     * @return the field
	     */
	    private String getField() {
	      return field;
	    }
	  }

	  /**
	   * Process.
	   *
	   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
	   * @param webRequest the web request
	   * @return the web response
	   * @throws SIBBACBusinessException the SIBBAC business exception
	   */
	  @Override
	  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
	    Map<String, Object> paramsObjects = webRequest.getParamsObject();
	    WebResponse result = new WebResponse();
	    Map<String, Object> resultados = new HashMap<String, Object>();
	    ComandosPlantillas command = getCommand(webRequest.getAction());

	    try {
	      LOG.debug("Entro al servicio SIBBACServiceGenerarInformesNEW");

	      switch (command) {
	        case DEVOLVER_ELEMENTOS:
	          result.setResultados(this.getElementos());
	          break;
	        case DEVOLVER_ELEMENTOS_BY_PARAMS:
		          result.setResultados(this.getElementosByByParams(paramsObjects));
		          break;
	        case DEVOLVER_IDS_ELEMENTOS:
	          result.setResultados(this.getIdsElementos(paramsObjects));
	          break;
	        case DEVOLVER_VALOR_PRECARGADO:
		      result.setResultados(this.getValorPrecargado(paramsObjects));
		      break;
	        default:
	          result.setError("An action must be set. Valid actions are: " + command);
	      } // switch
	    } catch (SIBBACBusinessException e) {
	      resultados.put("status", "KO");
	      result.setResultados(resultados);
	      result.setError(e.getMessage());
	    } catch (Exception e) {
	      resultados.put("status", "KO");
	      result.setResultados(resultados);
	      result.setError(MESSAGE_ERROR);
	    }

	    LOG.debug("Salgo del servicio SIBBACServiceGenerarInformesNEW");

	    return result;
	  }

	  /**
	   * Gets the fields.
	   *
	   * @return the fields
	   */
	  @Override
	  public List<String> getFields() {
	    List<String> result = new ArrayList<>();
	    FieldsPlantilla[] fields = FieldsPlantilla.values();
	    for (int i = 0; i < fields.length; i++) {
	      result.add(fields[i].getField());
	    }
	    return result;
	  }

	  @Override
	  public List<String> getAvailableCommands() {
	    return Collections.emptyList();
	  }

	  /**
	   * Devuelve elementos al consultar a tabla de la DB.
	   * 
	   * @return Map<String, Object>
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> getElementos() throws SIBBACBusinessException {
		  LOG.debug("Iniciando datos - getElementos");
		  Map<String, Object> map = new HashMap<String, Object>();
		  try {
			  List<ComponenteDirectivaDTO> datosFiltroInforme = 
					  this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTO();
			  map.put("datosFiltroInforme", datosFiltroInforme);
		  } catch (Exception e) {
			  LOG.error("Error metodo init - Obtener id de elemento en directiva " + e.getMessage(), e);
			  throw new SIBBACBusinessException(
					  "Error al obtener id de elemento en directiva - getElementos");
		  }
		  LOG.debug("Finalizando datos - getElementos");
		  return map;
	  }
	  
	  /**
	   * Devuelve elementos al consultar a tabla de la DB en base a parametros como id de informe.
	   * 
	   * @param paramsObjects
	   * @return Map<String, Object>
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> getElementosByByParams(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		  LOG.debug("Iniciando datos - getElementosByByParams");
		  Map<String, Object> map = new HashMap<String, Object>();
		  Integer idInforme = (Integer) paramsObjects.get("idInforme");
		  String clase = "";
		  String mercado = "";
		  if(null!=paramsObjects.get("clase") && StringUtils.isNotBlank(String.valueOf((Map<String, String>) paramsObjects.get("clase")))){
			  clase = String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("clase"));
		  }
		  if(null!=paramsObjects.get("mercado") && StringUtils.isNotBlank(String.valueOf((Map<String, String>) paramsObjects.get("mercado")))){
			  mercado = String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key"));
		  } else {
			  mercado = tmct0InformesDao.findMercadoInformeById(idInforme);
		  }
		  try {
			  List<List<ComponenteDirectivaDTO>> datosFiltroInformeByIdInforme = new ArrayList<List<ComponenteDirectivaDTO>>();
			  if (idInforme != null && !idInforme.equals(0)) {
				  datosFiltroInformeByIdInforme = this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOByIdInforme(idInforme,mercado);				  
			  }
			  if (clase != null && !clase.equals("")) {
				  List<List<ComponenteDirectivaDTO>> list = this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOByClase(clase,mercado);
				  if (null != list) {
					  datosFiltroInformeByIdInforme.addAll(list);
				  }
			  }
			  map.put("datosFiltroInformeByIdInforme", datosFiltroInformeByIdInforme);
		  } catch (Exception e) {
			  LOG.error("Error metodo init - Obtener id de elemento en directiva " + e.getMessage(), e);
			  throw new SIBBACBusinessException(
					  "Error al obtener id de elemento en directiva - getElementosByByParams");
		  }
		  LOG.debug("Finalizando datos - getElementosByByParams");
		  return map;
	  }
	  
	  /**
	   * Devuelve ids de elementos al consultar a tabla de la DB.
	   * @param paramsObjects
	   * @return Map<String, Object>
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> getIdsElementos(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		  LOG.debug("Iniciando datos - getIdsElementos");
		  Map<String, Object> map = new HashMap<String, Object>();
		  Integer idInforme = (Integer) paramsObjects.get("idInforme");
		  try {
			  List<BigInteger> idsElementos = new ArrayList<BigInteger>();
			  if (idInforme != null) {
				  List<Object> idsObject = this.tmct0InformesFiltrosDao.getDatosInformeByIdInforme(idInforme);
				  if (CollectionUtils.isNotEmpty(idsObject)) {
					  for (Object obj : idsObject) {
						  idsElementos.add((BigInteger) obj);
					  }
				  }
				  map.put("idsElementos", idsElementos);
			  }
		  } catch (Exception e) {
			  LOG.error("Error metodo init - Obtener ids elementos en directiva " + e.getMessage(), e);
			  throw new SIBBACBusinessException(
					  "Error al obtener ids elementos en directiva - getIdsElementos");
		  }
		  LOG.debug("Finalizando datos - getIdsElementos");
		  return map;
	  }
	  
	  /**
	   * Devuelve el valor precargado en el registro de la tabla de informes datos filtro.
	   * @param paramsObjects
	   * @return Map<String, Object>
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> getValorPrecargado(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		  LOG.debug("Iniciando datos - getValorPrecargado");
		  Map<String, Object> map = new HashMap<String, Object>();
		  try {
			  List<Integer> idsElementos = new ArrayList<Integer>();
			  List<Tmct0InformesDatosFiltros> tmct0InformesDatosFiltrosList = null;
			  String idInforme = (String) paramsObjects.get("idInforme");
			  List<Integer> idInformeFiltroList = (ArrayList<Integer>) paramsObjects.get("idInformeFiltro");
			  List<ValoresPrecargadosFiltroDTO> tmct0InformesDatosFiltrosListDTO = new ArrayList<ValoresPrecargadosFiltroDTO>();
			  if (idInforme != null) {
				  List<Object> idsObject = this.tmct0InformesFiltrosDao.getDatosInformeByIdInforme(Integer.valueOf(idInforme));
				  if (CollectionUtils.isNotEmpty(idsObject)) {
					  for (Object obj : idsObject) {
						  BigInteger valor = (BigInteger) obj; 
						  idsElementos.add(valor.intValue());
					  }
				  }
				  if (CollectionUtils.isNotEmpty(idsElementos)) {
					  tmct0InformesDatosFiltrosList = this.tmct0InformesDatosFiltrosDao.findByIdInformeAndIdInformeFiltroList(
							  new Integer(idInforme), idsElementos);
					  if (CollectionUtils.isNotEmpty(tmct0InformesDatosFiltrosList)) {
						  for (Tmct0InformesDatosFiltros vpfDTOElem : tmct0InformesDatosFiltrosList) {
							  ValoresPrecargadosFiltroDTO vpfDTO = new ValoresPrecargadosFiltroDTO(
									  vpfDTOElem.getId(), 
									  vpfDTOElem.getTmct0InformesFiltros() != null ? vpfDTOElem.getTmct0InformesFiltros().getId() : null, 
									  vpfDTOElem.getValorFiltro());					  
							  tmct0InformesDatosFiltrosListDTO.add(vpfDTO);
						  }
					  }
				  }
			  }
			  map.put("idsElementos", idsElementos);
			  map.put("valorPrecargado", tmct0InformesDatosFiltrosListDTO);
		  } catch (Exception e) {
			  LOG.error("Error metodo init - Obtener valor precargado " + e.getMessage(), e);
			  throw new SIBBACBusinessException(
					  "Error al obtener valor precargado en directiva - getValorPrecargado");
		  }
		  LOG.debug("Finalizando datos - getValorPrecargado");
		  return map;
	  }

	  /**
	   * Gets the command.
	   *
	   * @param command the command
	   * @return the command
	   */
	  private ComandosPlantillas getCommand(String command) {
	    ComandosPlantillas[] commands = ComandosPlantillas.values();
	    ComandosPlantillas result = null;
	    for (int i = 0; i < commands.length; i++) {
	      if (commands[i].getCommand().equalsIgnoreCase(command)) {
	        result = commands[i];
	      }
	    }
	    if (result == null) {
	      result = ComandosPlantillas.SIN_COMANDO;
	    }
	    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
	    return result;
	  }

}
