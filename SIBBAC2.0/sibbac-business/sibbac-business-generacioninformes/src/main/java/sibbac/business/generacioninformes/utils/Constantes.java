package sibbac.business.generacioninformes.utils;

/**
 * The Class Constantes para generacion informes.
 */
public class Constantes {

	/** Valores campo TOTALIZAR para tabla de informes campos. */
	public static final char VALOR_TOTALIZAR_COUNT = 'C';
	public static final char VALOR_TOTALIZAR_MAX = 'X';
	public static final char VALOR_TOTALIZAR_MIN = 'M';
	public static final char VALOR_TOTALIZAR_NO = 'N';
	public static final char VALOR_TOTALIZAR_SUM = 'S';
	public static final char VALOR_TOTALIZAR_COUNT_DISTINCT = 'D';
	public static final char VALOR_TOTALIZAR_SUM_DISTINCT = 'T';
	
	/** Expresion alias. */
	public static final String EXPR_ALIAS = "ALIAS";
	
	/** Literal tomado para el envio de contactos por alias. */
	public static final String LITERAL_CDALIAS = "%CDALIAS%";
	
	/** Constantes para reemplazo de las plantillas de mail **/
	public static final String PLANT_MAIL_CLAVE_FECHA = "%FECHA%";
	public static final String PLANT_MAIL_CLAVE_INFORME = "%INFORME%";
}