package sibbac.business.generacioninformes.helper;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.generacioninformes.database.dto.ComponenteDirectivaDTO;
import sibbac.business.generacioninformes.database.dto.Tmc0InformeCeldaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTONEW;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.generacioninformes.utils.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;

/**
 * Contiene metodos de utilidad para la funcionalidad de Generacion de Informes.
 */
public class GeneracionInformesHelper {
	
	protected static final Logger LOG = LoggerFactory.getLogger(GeneracionInformesHelper.class);
	
	private static final int TAMANIO_FUENTE_DEFECTO = 8;
	private static final String FUENTE_DEFECTO = "Calibri";
	private static final String FORMATO_MONTOS = "#,##0.00";
	private static final String FORMATO_FECHA_ES = "dd/MM/yyyy";
	private static final String FORMATO_HORA_ES = "HH:mm:ss";
	
	/**
	 *	Devuelve los datos de la lista formateados.
	 *	@param listaSeleccionados
	 *	@return String 
	 *	@throws SIBBACBusinessException
	 */
	public static String getDatosListaFormateados(List<SelectDTO> listaSeleccionados) throws SIBBACBusinessException {
		StringBuffer sb = new StringBuffer();
		for (Object selDTO : listaSeleccionados) {
			LinkedHashMap map = (LinkedHashMap) selDTO;
			String key = map.get("key").toString().trim();
			if (key.contains("||")) {
				String[] keyArray = key.split("\\|\\|",-1);
				sb.append("'" + keyArray[0] + "',");
			} else {
				sb.append("'" + map.get("key").toString().trim() + "',");
			}
		}
		if (sb.toString().contains(",")) {
			return sb.toString().substring(0, sb.toString().length() - 1);		  
		} else {
			return sb.toString();
		}
	}
	
	/**
	 *	Devuelve los datos de la lista formateados para clausulas like.
	 *	@param listaSeleccionados
	 *	@return String 
	 *	@throws SIBBACBusinessException
	 */
	public static String getDatosListaFormateadosLike(List<SelectDTO> listaSeleccionados, String campo) throws SIBBACBusinessException {
		StringBuffer sb = new StringBuffer();

		for (Object selDTO : listaSeleccionados) {
			LinkedHashMap map = (LinkedHashMap) selDTO;
			String key = map.get("key").toString().trim();
			if (key.contains("||")) {
				String[] keyArray = key.split("\\|\\|",-1);
				if (StringUtils.isNotBlank(sb.toString())) {
					sb.append(campo + " LIKE '%" + keyArray[0] + "%' OR ");
				} else {
					sb.append(" OR " + campo + " LIKE '%" + keyArray[0] + "%' OR ");
				}
			} else {
				sb.append(campo + " LIKE '%"  + map.get("key").toString().trim() + "%' OR ");
			}
		}
		if (sb.toString().contains(" OR ")) {
			return sb.toString().substring(0, sb.toString().length() - 4);		  
		} else {
			return sb.toString();
		}
	}
	
	/**
	 *	Devuelve los datos de la lista formateados para clausulas like.
	 *	@param listaSeleccionados
	 *	@return String 
	 *	@throws SIBBACBusinessException
	 */
	public static String getDatosListaFormateadosNotLike(List<SelectDTO> listaSeleccionados, String campo) throws SIBBACBusinessException {
		StringBuffer sb = new StringBuffer();

		for (Object selDTO : listaSeleccionados) {
			LinkedHashMap map = (LinkedHashMap) selDTO;
			String key = map.get("key").toString().trim();
			if (key.contains("||")) {
				String[] keyArray = key.split("\\|\\|",-1);
				if (StringUtils.isNotBlank(sb.toString())) {
					sb.append(campo + " NOT LIKE '%" + keyArray[0] + "%' AND ");
				} else {
					sb.append(" AND " + campo + " NOT LIKE '%" + keyArray[0] + "%' AND ");
				}
			} else {
				sb.append(campo + " NOT LIKE '%"  + map.get("key").toString().trim() + "%' AND ");
			}
		}
		if (sb.toString().contains(" AND ")) {
			return sb.toString().substring(0, sb.toString().length() - 5);		  
		} else {
			return sb.toString();
		}
	}
	
	/** 
	 * 	Crea el estilo por defecto de la celda, se asigna fuente y tamaño.
	 * 	@param workbook
	 * 	@return XSSFCellStyle
	 */
	public static void createWorkbookStyles(Workbook workbook, XSSFCellStyle style, Font font) {
		// Se pide un tamaño de 8 puntos por defecto para todos 
		// los datos
		font.setFontHeightInPoints((short) TAMANIO_FUENTE_DEFECTO); 
		font.setFontName(FUENTE_DEFECTO);
		style.setFont(font);
	}
	
	/**
	 * Se añaden los totales.
	 * @param filters dto informe isTerceros
	 * @return int
	 * @throws SIBBACBusinessException
	 */
	public static int createTotals(Workbook workbook, Sheet sheet,
			List<String> columnsNames, HashMap<String, Double> totales,
			CellStyle style, Integer rowNum) {

		rowNum++;
		Row row = sheet.createRow(rowNum);
		DataFormat format = workbook.createDataFormat();
		if (totales != null) {
			int cellNum = 0;
			// se recorren las columnas para ver en qué posición se debe pintar
			// el total
			for (String colName : columnsNames) {
				Double total = totales.get(colName);
				if (total != null) {
					row.createCell(cellNum).setCellValue(total);
					style = workbook.createCellStyle();
					Font font = workbook.createFont();
					font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
					style.setFont(font);
					style.setDataFormat(format.getFormat(FORMATO_MONTOS));
					row.getCell(cellNum).setCellStyle(style);
				}
				cellNum++;
			}
		}
		return rowNum;

	}
	
	/**
	 *	Creacion de encabezados de las columnas.
	 *	@param workbook workbook
	 *	@param sheet sheet
	 *	@param columnsNames columnsNames
	 *	@param rowNum rowNum
	 *	@return int int 
	 */
	public static int createColumnsHeaders(
			Workbook workbook, Sheet sheet, List<String> columnsNames, Integer rowNum) {

		int cellNum = 0;
		Row headerRow = sheet.createRow(rowNum);
		for (String colName : columnsNames) {
			Cell headerCol = headerRow.createCell(cellNum);
			XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
			XSSFFont font = style.getFont(); // Se utiliza la fuente definida por defecto
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			style.setFont(font);
			headerCol.setCellStyle(style);
			headerCol.setCellValue(colName);
			cellNum++;
		}
		return rowNum;
	}
	
	/**
	 *	Se fija un valor para cada campo del Excel.
	 *	@param cell cell
	 *	@param value value
	 *	@param formato formato
	 *	@param workbook workbook 
	 */
	public static void setCellValue(Cell cell, Object value, String formato, Workbook workbook, 
									XSSFCellStyle style, List<StyleBean> listStyle, Font font, SimpleDateFormat formatter, 
									SimpleDateFormat formatterHora, DataFormat format, int colNum,
									XSSFCellStyle styleDate, XSSFCellStyle styleHora) {

		// MFG 12/09/2016 Se aplica el estilo por defecto - peticion de cambio a fuente calibri tamaño 8 .
		
		if (value instanceof String) {
			// si era un numero y se ha transformado
			if (formato.length() > 0) {
				for(StyleBean sb :listStyle){
					if(sb.getColNum() == colNum){
						sb.getStyle().setDataFormat(format.getFormat(formato));
						cell.setCellStyle(sb.getStyle());
					}
				}
			
				cell.setCellValue(StringHelper.stringToDouble(String.valueOf(value)));
			} else {
				try{
					cell.setCellStyle(styleHora);
					cell.setCellValue(formatterHora.parse((String)value));
				}catch(Exception e){
					try{
						cell.setCellStyle(styleDate);
						cell.setCellValue(formatter.parse((String)value));
					}catch(Exception e1){
						cell.setCellStyle(style);
						cell.setCellValue((String) value);
					}
				}
			}
		} else if (value instanceof BigDecimal) {
			
			if (formato.length() > 0) {
				for(StyleBean sb :listStyle){
					if(sb.getColNum() == colNum){
						sb.getStyle().setDataFormat(format.getFormat(formato));
						cell.setCellStyle(sb.getStyle());
					}
				}
			
				cell.setCellValue(StringHelper.stringToDouble(String.valueOf(value)));
			} else {
				style.setDataFormat(format.getFormat(FORMATO_MONTOS));
				cell.setCellStyle(style);
			}
			
			cell.setCellValue(new Double(String.valueOf(value)));
		} else if (value instanceof Double) {
			
			if (formato.length() > 0) {
				for(StyleBean sb :listStyle){
					if(sb.getColNum() == colNum){
						sb.getStyle().setDataFormat(format.getFormat(formato));
						cell.setCellStyle(sb.getStyle());
					}
				}
			
				cell.setCellValue(StringHelper.stringToDouble(String.valueOf(value)));
			} else {
				style.setDataFormat(format.getFormat(FORMATO_MONTOS));
				cell.setCellStyle(style);
			}
			
			cell.setCellValue((Double) value);
		} else if (value != null) {
			try{
				
				cell.setCellStyle(styleHora);
				cell.setCellValue(formatterHora.parse((String)value));
			}catch(Exception e){
				try{
					cell.setCellStyle(styleDate);
					cell.setCellValue(formatter.parse((String)value));
				}catch(Exception e1){
					cell.setCellStyle(style);
					cell.setCellValue(value.toString());
				}
			}
		}
		
	}
	
	/**
	 *	Creacion de resultados.
	 *	@param workbook workbook
	 *	@param sheet sheet
	 *	@param results results
	 *	@param style style
	 *	@param rowNum rowNum
	 *	@return int int  
	 */
	public static int createResults(Workbook workbook,
			Sheet sheet,
			List<List<Tmc0InformeCeldaDTO>> results,
			XSSFCellStyle style, Font font,
			Integer rowNum) {
		SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_ES);
		SimpleDateFormat formatterHora = new SimpleDateFormat(FORMATO_HORA_ES);
		DataFormat format = workbook.createDataFormat();
		if (results != null && !results.isEmpty()) {
			Row row = null;
			Cell cell = null;
			Object value = null;
			String formato = "";
//			String formatoAnterior = ""; 
			
			List<StyleBean> listStyles = new ArrayList<StyleBean>();
			
			XSSFCellStyle styleDate = (XSSFCellStyle) workbook.createCellStyle();
			Font fontDate = workbook.createFont();
			CreationHelper createHelper = workbook.getCreationHelper();
			styleDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
			GeneracionInformesHelper.createWorkbookStyles(workbook, styleDate, fontDate);
			
			XSSFCellStyle styleHora = (XSSFCellStyle) workbook.createCellStyle();
			Font fontHora = workbook.createFont();
			CreationHelper createHelperHora = workbook.getCreationHelper();
			styleHora.setDataFormat(createHelperHora.createDataFormat().getFormat("hh:mm:ss"));
			GeneracionInformesHelper.createWorkbookStyles(workbook, styleHora, fontHora);
			
			int firstRowNum = rowNum + 1;
			for (List<Tmc0InformeCeldaDTO> result : results) {
				int cellNum = 0;
				rowNum++;
				LOG.debug("Crea la fila " + rowNum);
				row = sheet.createRow(rowNum);
				
				for (Tmc0InformeCeldaDTO column : result) {
					
					cell = row.createCell(cellNum);
					value = column.getContenido();
					formato = column.getFormato();
					
					if(rowNum == firstRowNum){
						if (formato.length() > 0) {
							StyleBean styleBean = new StyleBean();
							XSSFCellStyle style1 = (XSSFCellStyle) workbook.createCellStyle();
							Font font1 = workbook.createFont();
							GeneracionInformesHelper.createWorkbookStyles(workbook, style1, font1);
							styleBean.setStyle(style1);
							styleBean.setColNum(cellNum);
							listStyles.add(styleBean);
						}
					}
					
					GeneracionInformesHelper.setCellValue(cell, value, formato, workbook, style, 
														  listStyles, font, formatter, formatterHora, 
														  format, cellNum, styleDate, styleHora);
					cellNum++;
				}
			}
		} else {
			rowNum++;
		}
		return rowNum;
	}
	
	/*** Inicio - Totalizacion por Java. **/
	/**
	 *	Totaliza objetos de la DB manualmente segun el tipo de operacion
	 *	del campo TOTALIZAR de la tabla de informes campos. 
	 *	@param auxTotales
	 *	@param iColumnasTotalizan
	 *	@param tmct0InformeFila
	 *	@param tResultados
	 * 	@throws SIBBACBusinessException 
	 */
	public static void totalizarObjsDBManualClaveNueva(
			List<Tmc0InformeCeldaDTO> auxTotales, 
			List<Integer> iColumnasTotalizan,
			List<Tmc0InformeCeldaDTO> tmct0InformeFila,
			TreeMap<String, List<Tmc0InformeCeldaDTO>> tResultados,
			String sClave) throws SIBBACBusinessException {

		String valorColumna = null;
		// En este punto se iteran c/u de las celdas campos de detalle
		for (int i = 0; i < iColumnasTotalizan.size(); i++) {
			boolean totalizarMinMax = false;
			int iColumna = iColumnasTotalizan.get(i);
			if ((tmct0InformeFila.get(iColumna).isTotalizarCount())
					|| (tmct0InformeFila.get(iColumna).isTotalizarMax())
					|| (tmct0InformeFila.get(iColumna).isTotalizarSum())
					|| (tmct0InformeFila.get(iColumna).isTotalizarMin())
					|| (tmct0InformeFila.get(iColumna).isTotalizarCountDistinct())
					|| (tmct0InformeFila.get(iColumna).isTotalizarSumDistinct())) {
				valorColumna = StringHelper.isNotBlankAndStringNotNull(
						tmct0InformeFila.get(iColumna).getValor()) ? tmct0InformeFila.get(iColumna).getValor() : "";
			}
			switch (tmct0InformeFila.get(iColumna).getTotalizar()) {
			case Constantes.VALOR_TOTALIZAR_SUM:
				// Suma
				double valorSclaveSum = StringHelper.stringToDouble(String.valueOf(auxTotales.get(iColumna).getValor()))
						+ StringHelper.stringToDouble(String.valueOf(tmct0InformeFila.get(iColumna).getValor()));
				auxTotales.get(iColumna).setContenido(String.valueOf(valorSclaveSum));
				tResultados.put(sClave, auxTotales);
				break;
			case Constantes.VALOR_TOTALIZAR_COUNT:
				// Conteo
				if (StringHelper.isNotBlankAndStringNotNull(valorColumna)) {
					String valorActualSclave = (String) auxTotales.get(iColumna).getValor();
					auxTotales.get(iColumna).setContenido(Integer.valueOf(valorActualSclave) + 1);
				} else {
					auxTotales.get(iColumna).setContenido(String.valueOf(0));
				}
				tResultados.put(sClave, auxTotales);
				break;
			case Constantes.VALOR_TOTALIZAR_MAX:
				// Maximo
				String valorActualSclaveTotMax = (String) auxTotales.get(iColumna).getValor();
				if (StringHelper.isNumerico(valorColumna) 
						&& StringHelper.isNumerico(valorActualSclaveTotMax) 
						&& (Double.parseDouble(valorColumna) > Double.parseDouble(valorActualSclaveTotMax))) {
					// Se evalua como numerico
					totalizarMinMax = true;
				} else {
					// Se evalua como String
					// Si el acumulador esta en NULL tambien se totaliza
					if (valorColumna.compareTo(valorActualSclaveTotMax) > 0 
							|| valorActualSclaveTotMax == null || valorActualSclaveTotMax.equals("null")) {
						totalizarMinMax = true;
					}
				}
				break;
			case Constantes.VALOR_TOTALIZAR_MIN:
				// Minimo
				String valorActualSclaveTotMin = (String) auxTotales.get(iColumna).getValor();
				if (StringHelper.isNumerico(valorColumna) 
						&& StringHelper.isNumerico(valorActualSclaveTotMin) 
						&& (Double.parseDouble(valorColumna) < Double.parseDouble(valorActualSclaveTotMin))) {
					// Se evalua como numerico
					totalizarMinMax = true;
				} else {
					// Se evalua como String
					// Si el acumulador esta en NULL tambien se totaliza
					if (valorColumna.compareTo(valorActualSclaveTotMin) < 0 
							|| valorActualSclaveTotMin == null || valorActualSclaveTotMin.equals("null")) {
						totalizarMinMax = true;
					}
				}
				break;
			case Constantes.VALOR_TOTALIZAR_COUNT_DISTINCT:
				// TODO - Implementar logica
				// Para una clave unica siempre es 1 o 0 y al final se deberia aplicar el conteo entre claves distintas
				if (StringHelper.isNotBlankAndStringNotNull(valorColumna)) {
					if (auxTotales.get(iColumna).getContenido() instanceof String) {
						auxTotales.get(iColumna).getContenidoString().add(valorColumna);
					}
				}
				tResultados.put(sClave, auxTotales);
				break;
			case Constantes.VALOR_TOTALIZAR_SUM_DISTINCT:
				if (StringHelper.isNotBlankAndStringNotNull(valorColumna)) {
					if (auxTotales.get(iColumna).getContenido() instanceof BigDecimal) {
						auxTotales.get(iColumna).getContenidoBigDecimal().add(NumberUtils.createBigDecimal(valorColumna));
					} else if (auxTotales.get(iColumna).getContenido() instanceof Integer) {
						auxTotales.get(iColumna).getContenidoInteger().add(Integer.valueOf(valorColumna));
					}
				}
				tResultados.put(sClave, auxTotales);
				break;
			default:
				break;
			}

			// Se asigna contenido para la totalizacion para los casos MIN-MAX en caso necesario.
			if (tmct0InformeFila.get(iColumna).isTotalizarMax()
					|| tmct0InformeFila.get(iColumna).isTotalizarMin()) {
				// Totalizacion para campos MIN o MAX
				if (totalizarMinMax) {
					auxTotales.get(iColumna).setContenido(String.valueOf(valorColumna));
				}				  
				// Solo se almacena el resultado en la ultima fila
				tResultados.put(sClave, auxTotales);
			}
		}
	}
	/*** Fin - Totalizacion por Java. **/
	
	/**
	   * Gets las fechas de ejecucion del informe. .
	   *
	   * @param dFechaInicio - Fecha de inicio
	   * @param dFechaFin - Fecha de Fin
	   * @return Lista con todos los dias que hay entre las fechas dadas
	   * @throws Exception
	   */
	  public static List<Date> getFechasEjecucion(Date dFechaInicio, Date dFechaFin) throws Exception {
	    LOG.debug("Entra en getFechasEjecucion");

	    List<Date> lResultado = new ArrayList<Date>();
	    Date diaSiguiente;
	    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

	    if (formatoFecha.parse(formatoFecha.format(dFechaFin)).equals(formatoFecha.parse("31/12/9999"))) {
	    	dFechaFin = null;
	    }
	    
	    try {
	      // Tratamiento de las fechas de inicio y final.
	      // Se tiene que incluir la fecha de inicio que no puede ser nula.
	      lResultado.add(dFechaInicio);

	      // Si la fecha final es nula, se inicializa con la fecha actual sin horas minutos y segundos
	      if (dFechaFin == null) {

	        dFechaFin = formatoFecha.parse(formatoFecha.format(new Date()));
	      }

	      diaSiguiente = dFechaInicio;

	      Calendar calendar = Calendar.getInstance();
	      while (diaSiguiente.before(dFechaFin)) {
	        calendar.setTime(diaSiguiente);
	        calendar.add(Calendar.DAY_OF_YEAR, 1);
	        diaSiguiente = calendar.getTime();
	        lResultado.add(diaSiguiente);
	      }

	    } catch (Exception e) {
	      LOG.error("getFechasEjecucion - Se produjo un error al obtener las fechas de ejecucion " + e.getMessage());
	      throw e;
	    }
	    LOG.debug("Fin en getFechasEjecucion");
	    return lResultado;

	  }
	  
	  /**
	   * 	Devuelve indice en caso de que el componente generico exista 
	   * 	y tenga un campo de tipo FECHA CON subtipo DESDE-HASTA no vacío.
	   * 	@param listComponenteDirectivaDTO listComponenteDirectivaDTO
	   * 	@param informe informe
	   * 	@return indice elemento (-1 si no se encuentra)
	   */
	  public static int getIndexComponenteGenericoCampoFECHARango(List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO, Tmct0Informes informe, int posicionMercado) throws SIBBACBusinessException {
		  int index = -1;
		  if (CollectionUtils.isNotEmpty(listComponenteDirectivaDTO)) {
			  for (List<ComponenteDirectivaDTO> compDirDTOList : listComponenteDirectivaDTO) {
				  for (int x = 0; x < compDirDTOList.size(); x++) {
					  if (compDirDTOList.get(x).isTipoFecha() && compDirDTOList.get(x).isSubtipoDesdeHasta() && (StringUtils.isNotBlank(compDirDTOList.get(x).getValorCampo()) || StringUtils.isNotBlank(compDirDTOList.get(x).getValorNumeroFechaDesde()))) {
						  if (informe.getTmct0InformesClases() != null && StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase())) {
							  // Si la fecha base de la clase no esta vacia se recorre cada una de ellas,
							  // si su alias.campo == FECHA_BASE de la clase se retorna ese indice.
							  String fechaBaseClase = StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase()) ? informe.getTmct0InformesClases().getFechaBase().trim() : "";
							  String fechaBaseCampo = "";
							  if (compDirDTOList.get(x).getCampo().contains("/")) {
								  fechaBaseCampo = (compDirDTOList.get(x).getCampo().split("/",-1))[posicionMercado];
							  } else {
								  fechaBaseCampo = compDirDTOList.get(x).getCampo();
							  }
							  if (fechaBaseClase.equals(fechaBaseCampo)) {
								  return x;								
							  }
						  } else {
							  // Si no hay coincidencia con fecha base se toma la 1er fecha que encuentre 
							  if (index == -1) {
								  index = x;								
							  }
						  }
					  }
				  }
			  }
		  }
		  return index;
	  }
	  
	  /**
	   * 	Devuelve indice en caso de que el componente generico exista 
	   * 	y tenga un campo de tipo FECHA SIN subtipo DESDE-HASTA no vacío.
	   * 	@param listComponenteDirectivaDTO listComponenteDirectivaDTO
	   * 	@param informe informe
	   * 	@return indice elemento (-1 si no se encuentra)
	   * 	@throws SIBBACBusinessException 
	   */
	  public static int getIndexComponenteGenericoCampoFECHASinRango(List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO, Tmct0Informes informe, int posicionMercado) throws SIBBACBusinessException {
		  int index = -1;
		  if (CollectionUtils.isNotEmpty(listComponenteDirectivaDTO)) {
			  for (List<ComponenteDirectivaDTO> compDirDTOList : listComponenteDirectivaDTO) {
				  for (int x = 0; x < compDirDTOList.size(); x++) {
					  if (compDirDTOList.get(x).isTipoFecha() && !compDirDTOList.get(x).isSubtipoDesdeHasta() && (StringUtils.isNotBlank(compDirDTOList.get(x).getValorCampo()) || StringUtils.isNotBlank(compDirDTOList.get(x).getValorNumeroFechaDesde()))) {
						  // Si la fecha base de la clase no esta vacia se recorre cada una de ellas,
						  // si su alias.campo == FECHA_BASE de la clase se retorna ese indice.
						  if (informe.getTmct0InformesClases() != null && StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase())) {
							  // Si la fecha base de la clase no esta vacia se recorre cada una de ellas,
							  // si su alias.campo == FECHA_BASE de la clase se retorna ese indice.
							  String fechaBaseClase = StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase()) ? informe.getTmct0InformesClases().getFechaBase().trim() : "";
							  String fechaBaseCampo = "";
							  if (compDirDTOList.get(x).getCampo().contains("/")) {
								  fechaBaseCampo = (compDirDTOList.get(x).getCampo().split("/",-1))[posicionMercado];
							  } else {
								  fechaBaseCampo = compDirDTOList.get(x).getCampo();
							  }
							  if (fechaBaseClase.equals(fechaBaseCampo)) {
								  return x;								
							  }
						  } else {
							  // Si no hay coincidencia con fecha base se toma la 1er fecha que encuentre 
							  if (index == -1) {
								  index = x;								
							  }
						  }
					  }					  
				  }
			  }
		  }
		  return index;
	  }
	  
	  /**
	   *	Devuelve un listado de tipo String donde cada elemento es 
	   *	una fila con delimitador de elementos del archivo .TXT a generar	
	   * 	@param contenidoLista: contenido del fichero
	   * 	@param tmct0InformesPlanificacion: datos necesarios
	   *	@return List<String>: contenido del fichero formateado para el .TXT
	   * 	@throws SIBBACBusinessException 
	   */
	  public static synchronized List<String> getContenidoTXTForBatch(
			  List<List<Tmc0InformeCeldaDTO>> contenidoLista, Tmct0InformesPlanificacion tmct0InformesPlanificacion) throws SIBBACBusinessException {
		  List<String> contenidoFichero = new ArrayList<String>();
		  StringBuffer sbCabecera = new StringBuffer();
		  StringBuffer[] sbContenido = new StringBuffer[contenidoLista.size()];
		  DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		  boolean separadorMiles = true;
		  boolean separadorDecimales = true;
		  //Comprobamos si el separador de miles es vacio
		  if (tmct0InformesPlanificacion.getSeparadorMiles() == ' ') {
			  separadorMiles = false;
		  } else {
			  symbols.setGroupingSeparator(tmct0InformesPlanificacion.getSeparadorMiles());
		  }
		  //Comprobamos si el separador de decimales es vacio
		  if (tmct0InformesPlanificacion.getSeparadorDecimales() == ' ') {
			  separadorDecimales = false;
		  } else {
			  symbols.setDecimalSeparator(tmct0InformesPlanificacion.getSeparadorDecimales());
		  }
		  
		  DecimalFormat formatter = new DecimalFormat("###,###.######", symbols);
		  String separador = null;
		  if (!tmct0InformesPlanificacion.getSeparadorCampos().equals("")) {
			  if (tmct0InformesPlanificacion.getSeparadorCampos().toUpperCase().contains("CHR(")) {
				  int i = Integer.valueOf(tmct0InformesPlanificacion.getSeparadorCampos().toUpperCase().replace("CHR(", "").replace(")", ""));
				  separador = Character.toString ((char) i);
			  } else {
				  separador = tmct0InformesPlanificacion.getSeparadorCampos();
			  }
		  } else {
			  // El separador se pone por defecto a "|"
			  separador = "";			  
		  }
		  
		  if (CollectionUtils.isNotEmpty(contenidoLista)) {
			  int conteoFila = 0;
			  for (List<Tmc0InformeCeldaDTO> contenidoFila : contenidoLista) {
				  for (int i=0;i<contenidoFila.size();i++) {
					  if (conteoFila == 0 && !tmct0InformesPlanificacion.getTipoDestino().equals("RUTA SIN CABECERA")) {
						  sbCabecera.append(contenidoFila.get(i).getColumna()).append(separador);
					  }
					  if (sbContenido[conteoFila] == null) {
						  sbContenido[conteoFila] = new StringBuffer();
					  }
					  if (i==(contenidoFila.size()-1)) {
						  sbContenido[conteoFila].append(getContenidoAsString(contenidoFila.get(i),symbols,formatter, separadorMiles, separadorDecimales));
					  } else {
						  sbContenido[conteoFila].append(getContenidoAsString(contenidoFila.get(i),symbols,formatter, separadorMiles, separadorDecimales)).append(separador);
					  }
				  }
				  sbContenido[conteoFila].append(" \n");
				  conteoFila = conteoFila + 1;
			  }
		  }
		  contenidoFichero.add(sbCabecera.append(" \n").toString());
		  for (StringBuffer sb : sbContenido) {
			  contenidoFichero.add(sb.toString());
		  }
		  return contenidoFichero;
	  }
	  
	  /**
	   *	Ajusta valores de agrupacion de clave nueva.
	   *	@param tmct0InformeFila tmct0InformeFila
	   *	@param iColumnasTotalizan iColumnasTotalizan
	   *	@throws SIBBACBusinessException SIBBACBusinessException 
	   */
	  public static void ajustarValoresAgrupacionClaveNueva(
			  List<Tmc0InformeCeldaDTO> tmct0InformeFila, 
			  List<Integer> iColumnasTotalizan) throws SIBBACBusinessException {
		  
		  // Aqui para el caso de claves nuevas, si hay un conteo este debe inicializar
		  if (CollectionUtils.isNotEmpty(tmct0InformeFila)) {
			  // Se itera cada uno de los campos de detalle
			  for (Tmc0InformeCeldaDTO infCeldDTO : tmct0InformeFila) {
				  // Ajuste para conteo
				  if (infCeldDTO.isTotalizarCount()) {
					  if (StringHelper.isNotBlankAndStringNotNull(infCeldDTO.getValor())) {
						  // Campo no esta vacio se cuenta
						  infCeldDTO.setContenido(1);	
					  } else {
						  // Campo vacio no se cuenta
						  infCeldDTO.setContenido(0);
					  }
				  }
				  // Se carga en la lista de conteo el valor con la clave nueva.
				  if (infCeldDTO.isTotalizarCountDistinct()) {
					  if (StringHelper.isNotBlankAndStringNotNull(infCeldDTO.getValor())) {
							if (infCeldDTO.getContenido() instanceof String) {
								infCeldDTO.getContenidoString().add(infCeldDTO.getValor());
							}
						}
				  }
				  // Se carga en la lista de conteo el valor con la clave nueva.
				  if (infCeldDTO.isTotalizarSumDistinct()) {
					  if (StringHelper.isNotBlankAndStringNotNull(infCeldDTO.getValor())) {
						  if (infCeldDTO.getContenido() instanceof BigDecimal) {
							  infCeldDTO.getContenidoBigDecimal().add(
									  NumberUtils.createBigDecimal(infCeldDTO.getValor()));
						  } else if (infCeldDTO.getContenido() instanceof Integer) {
							  infCeldDTO.getContenidoInteger().add(Integer.valueOf(infCeldDTO.getValor()));
						  }
					  } 
				  }
			  }
		  }
	  }
	  
	  /**
	   *	Obtiene contenido del tipo que sea como un String.
	   *	@param valor
	   *	@return valor como String
	   *	@throws SIBBACBusinessException 
	   */
	  public static String getContenidoAsString(Tmc0InformeCeldaDTO celda,DecimalFormatSymbols symbols, DecimalFormat formatter, boolean separadorMiles, boolean separadorDecimales) throws SIBBACBusinessException {
		 try {
		  if (celda.getContenido() != null) {
			  if (celda.getTipoCampo().equals("TEXTO")) {
				  try {
					  return (String) celda.getContenido();
				  } catch (Exception e) {
					  return ""+celda.getContenido();
				  }
			  } else if (celda.getTipoCampo().equals("DECIMAL")) {
				  celda.setContenido(((String) celda.getContenido()).replace(",", "."));
				  if (!separadorDecimales && celda.getContenido().toString().contains(".")) {
					  return formatter.format((BigDecimal) celda.getContenido()).replace(".", "");
				  }
				  if (celda.getContenido().toString().contains(".")) {
					  return formatter.format(new BigDecimal(celda.getContenido().toString()));
				  }
			  } else if (celda.getTipoCampo().equals("NUMERO")) {
				  return formatter.format((Integer) celda.getContenido());
			  } else if (celda.getTipoCampo().equals("FECHA")){
				  if (celda.getContenido() instanceof java.sql.Timestamp) {
					  return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format((java.sql.Timestamp) celda.getContenido());
				  } else if (celda.getContenido() instanceof java.util.Date) {
					  return new SimpleDateFormat("yyyy-MM-dd").format((java.util.Date) celda.getContenido());
				  }
			  } else {
				  return ""+celda.getContenido();
			  }
		  }
		 } catch (Exception e) {
			 LOG.error("El tipo del campo introducido en la tabla TMCT0_INFORMES_CAMPOS es incorrecto -" + celda.getColumna() + " - " + celda.getTipoCampo());
			 if (celda.getContenido() instanceof String) {
				 return (String) celda.getContenido();
			 } else if (celda.getContenido() instanceof java.sql.Timestamp) {
				 return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format((java.sql.Timestamp) celda.getContenido());
			 } else if (celda.getContenido() instanceof java.util.Date) {
				 return new SimpleDateFormat("yyyy-MM-dd").format((java.util.Date) celda.getContenido());
			 }	else if (celda.getContenido() instanceof BigDecimal) {
				 if (!separadorDecimales) {
					 return formatter.format(((BigDecimal) celda.getContenido()).doubleValue()).replace(".", "");
				 }
				 return formatter.format(((BigDecimal) celda.getContenido()).doubleValue());
			 } else if (celda.getContenido() instanceof Integer) {
				 return formatter.format((Integer) celda.getContenido());
			 }	else if (celda.getContenido() instanceof Double) {
				 if (!separadorDecimales) {
					 return formatter.format((Double) celda.getContenido()).replace(".", "");
				 }
				 return formatter.format((Double) celda.getContenido());
			 }	else if (celda.getContenido() instanceof Float) {
				 if (!separadorDecimales) {
					 return formatter.format((Float) celda.getContenido()).replace(".", "");
				 }
				 return formatter.format((Float) celda.getContenido());
			 }
		 }
		  return null;
	  }
	  
	  /**
	   * 	Chequeo de si se aplica funcionalidad:
	   * 	Envio de informes a todos los contactos de los alias autorizados para recibir informes
	   * 	@param tmct0InformesPlanificacion tmct0InformesPlanificacion
	   * 	@param plantilla plantilla
	   * 	@throws SIBBACBusinessException SIBBACBusinessException
	   */
	  public static synchronized boolean isEnvioListaContactos(
			  Tmct0InformesPlanificacion tmct0InformesPlanificacion, Tmct0InformeDTONEW plantilla) 
					  throws SIBBACBusinessException {
		  // Cuando se vaya a crear una PLANIFICACION se comprobarán las siguientes condiciones:
		  //	Si el campo DESTINO está vacio.
		  //	Si el INFORME seleccionado tiene CAMPOS DE SEPARACION.
		  //	Si el PRIMERO de los CAMPOS DE SEPARACION es un ALIAS.
		  //	Solo se aplica a TIPO DESTINO E-mail
		  if (tmct0InformesPlanificacion != null && tmct0InformesPlanificacion.isTipoDestinoEMAIL() 
				  && StringUtils.isBlank(tmct0InformesPlanificacion.getDestino())
				  && CollectionUtils.isNotEmpty(plantilla.getListaCamposSeparacion())) {
			  String descripcion = plantilla.getListaCamposSeparacion().get(0).getValue() != null ? 
					  plantilla.getListaCamposSeparacion().get(0).getValue() : ""; 
			  return descripcion.toUpperCase().contains(Constantes.EXPR_ALIAS);		  
		  }
		  return false;
	  }

	  /**
	   *	Se devuelve contenido de mail formateado para la funcionalidad
	   *	de envios de mails a contactos de alias.
	   *	@param contenido a formatear
	   *	@param informe del que se necesitan datos
	   *	@return contenido formateado String 
	   *	@throws SIBBACBusinessException
	   */
	  public static synchronized String getPlantillasMailFormateadas(String contenido, Tmct0Informes informe) throws SIBBACBusinessException {
		  if (StringUtils.isNotBlank(contenido) && informe != null) {
			  return contenido
					  .replace(Constantes.PLANT_MAIL_CLAVE_FECHA, DateHelper.convertDateToString(new Date(), DateHelper.FORMAT_DD_MM_YYYY_HH_MM_SS))
					  .replace(Constantes.PLANT_MAIL_CLAVE_INFORME, informe.getNbdescription());
		  }
		  return contenido;
	  }
	  
	  /**
	   *	Se agrupan los resultados del DTO para COUNT DISTINCT y SUM DISTINCT.
	   *	@param dto dto
	   *	@return Map<String, List<Tmc0InformeCeldaDTO>> Map<String, List<Tmc0InformeCeldaDTO>>
	   *	@throws SIBBACBusinessException SIBBACBusinessException	 
	   */
	  public static Map<String, List<Tmc0InformeCeldaDTO>> getResultadosAgrupadosDTO(
			  Tmct0InformeGeneradoDTO dto) throws SIBBACBusinessException {
		  Map<String, List<Tmc0InformeCeldaDTO>> mapaClaves = new HashMap<String, List<Tmc0InformeCeldaDTO>>();
		  if (dto != null && CollectionUtils.isNotEmpty(dto.getResultados())) {
			  for (List<Tmc0InformeCeldaDTO> listDTO : dto.getResultados()) {
				  StringBuffer sbHash = new StringBuffer();
//				  String valorActualSumDistinct = "";
//				  boolean isContieneTotalizadoresNoSUM = isContieneTotalizadoresNoSUM(listDTO);

				  for (Tmc0InformeCeldaDTO celdaDTO : listDTO) {
					  switch (celdaDTO.getTotalizar()) {
					  case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_COUNT_DISTINCT:
						  if (CollectionUtils.isNotEmpty(celdaDTO.getContenidoString())) {
							  HashSet<String> countDistinct = new HashSet<String>();
							  for (String string : celdaDTO.getContenidoString()) {
								  countDistinct.add(string);
							  }
							  celdaDTO.setContenido(countDistinct.size());							  
						  } 
						  break;
					  case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM_DISTINCT:
						  if (CollectionUtils.isNotEmpty(celdaDTO.getContenidoInteger())) {
//							  celdaDTO.setContenido(Long.valueOf(celdaDTO.getContenidoInteger().stream().distinct().count()).intValue());
							  HashSet<Integer> sumDistinctInteger = new HashSet<Integer>();
							  Integer distinctSumInteger = 0;
							  for (Integer integer : celdaDTO.getContenidoInteger()) {
								  sumDistinctInteger.add(integer);
							  }
							  for (Integer integer : sumDistinctInteger ) {
								  distinctSumInteger = distinctSumInteger + integer;
								}
							  celdaDTO.setContenido(distinctSumInteger);
						  } else if (CollectionUtils.isNotEmpty(celdaDTO.getContenidoBigDecimal())) {
							  HashSet<BigDecimal> sumDistinctDecimal = new HashSet<BigDecimal>();
							  BigDecimal distinctSumDecimales = BigDecimal.ZERO;
							  for (BigDecimal bigDecimal : celdaDTO.getContenidoBigDecimal()) {
								  sumDistinctDecimal.add(bigDecimal);
							  }
							  for (BigDecimal bigDecimal : sumDistinctDecimal ) {
								  distinctSumDecimales = distinctSumDecimales.add(bigDecimal);
								}
							  celdaDTO.setContenido(distinctSumDecimales);
						  }
						  break;
					  }
					  sbHash.append(celdaDTO.getValor());
				  }
				  mapaClaves.put(sbHash.toString(), listDTO);
			  }
		  }
		  return mapaClaves;
	  }
	  
	  /**
	   *	Devuelve true si contiene alguna columna no sujeta a totalizadores
	   *	SUM y DISTINCT SUM. Evitaria que no se carguen claves vacias cuando hay solo totalizadores SUM.
	   *	@param listDTO
	   *	@return boolean
	   *	@throws SIBBACBusinessException 
	   */
	  public static boolean isContieneTotalizadoresNoSUM(
			  List<Tmc0InformeCeldaDTO> listDTO) throws SIBBACBusinessException {
		  if (CollectionUtils.isNotEmpty(listDTO)) {
			  for (Tmc0InformeCeldaDTO celdaDTO : listDTO) {
				  if (celdaDTO.isContieneTotalizadoresNoSUM()) {
					  return true;
				  }
			  }
		  }
		  return false;
	  }
	  
	  /**
	   *	Devuelve true si contiene alguna columna con totalizadores COUNT DISTINCT, SUM y SUM DISTINCT. 
	   *	@param listDTO
	   *	@return boolean
	   *	@throws SIBBACBusinessException 
	   */
	  public static boolean isContieneTotalizadoresSUMorDSUMorDCOUNT(
			  List<List<Tmc0InformeCeldaDTO>> resultados) throws SIBBACBusinessException {
		  if (CollectionUtils.isNotEmpty(resultados)) {
			  for (List<Tmc0InformeCeldaDTO> listDTO : resultados) {
				  if (CollectionUtils.isNotEmpty(listDTO)) {
					  for (Tmc0InformeCeldaDTO celdaDTO : listDTO) {
						  if (celdaDTO.isTotalizarCountDistinct() || celdaDTO.isTotalizarSum() || celdaDTO.isTotalizarSumDistinct()) {
							  return true;
						  }
					  }
				  }				  
			  }
		  }
		  return false;
	  }
	  
	  /**
	   *	Se realiza el formato a la precision especificada de los resultados agrupados.
	   *	@param dto dto
	   *	@throws SIBBACBusinessException SIBBACBusinessException
	   */
	  public static void formatearCamposResultadosAgrupacion(
			  List<List<Tmc0InformeCeldaDTO>> dto) throws SIBBACBusinessException {
		  String contenidoFinal = null;
		  if (dto != null && CollectionUtils.isNotEmpty(dto)) {
			  for (List<Tmc0InformeCeldaDTO> listInformeCeldaDTO : dto) {
				  if (CollectionUtils.isNotEmpty(listInformeCeldaDTO)) {
					  for (Tmc0InformeCeldaDTO informeCeldaDTO : listInformeCeldaDTO) {
						  // si tiene decimales se convierte a string con los decimales indicados para que se vea bien en pantalla
						  if (informeCeldaDTO.getPrecisionCampo() != null && informeCeldaDTO.getPrecisionCampo().toString().trim().length() != 0 
								  && Character.isDigit(informeCeldaDTO.getPrecisionCampo())) {
							  try {
								  // Si el contenido es null no se modifica.	
								  if (StringUtils.isNotBlank((String) informeCeldaDTO.getContenido())) {
									  // para los contenidos de tipo number que nos lleguen como string
									  contenidoFinal = (String) String.format(Locale.GERMANY, "%." + informeCeldaDTO.getPrecisionCampo().toString() + "f", informeCeldaDTO.getContenido());
									  informeCeldaDTO.setContenido(contenidoFinal != null ? contenidoFinal : contenidoFinal);
								  }
							  } catch (Exception e) {
								  // para los contenidos de tipo number que nos lleguen como number
								  Double contDouble = new Double(String.valueOf(informeCeldaDTO.getContenido()));
								  contenidoFinal = (String) String.format(Locale.GERMANY, "%." + informeCeldaDTO.getPrecisionCampo().toString() + "f", contDouble);
								  informeCeldaDTO.setContenido(contenidoFinal != null ? contenidoFinal : contenidoFinal);					        	  
							  }							  
						  }
					  }
				  }
			  }
		  }
	  }
}
