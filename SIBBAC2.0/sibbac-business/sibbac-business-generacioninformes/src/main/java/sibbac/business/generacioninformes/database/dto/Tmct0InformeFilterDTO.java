package sibbac.business.generacioninformes.database.dto;

import java.util.ArrayList;
import java.util.List;

import sibbac.business.wrappers.common.SelectDTO;

public class Tmct0InformeFilterDTO {

    private String plantilla;
    private boolean notPlantilla;
    private String clase;
    private boolean notClase;
    private String idioma;
    private boolean notIdioma;
    private String mercado;
    private boolean notMercado;
    private List<SelectDTO> camposDetalle;
    private boolean notCamposDetalle;
    private String totalizar;
    private boolean notTotalizar;
    private String alias;
    private boolean notAlias;
    private String isin;
    private boolean notIsin;
    private List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO = new ArrayList<List<ComponenteDirectivaDTO>>();
    
	public String getPlantilla() {
		return plantilla;
	}
	public void setPlantilla(String plantilla) {
		this.plantilla = plantilla;
	}
	public boolean isNotPlantilla() {
		return notPlantilla;
	}
	public void setNotPlantilla(boolean notPlantilla) {
		this.notPlantilla = notPlantilla;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public boolean isNotIdioma() {
		return notIdioma;
	}
	public void setNotIdioma(boolean notIdioma) {
		this.notIdioma = notIdioma;
	}
	public List<SelectDTO> getCamposDetalle() {
		return camposDetalle;
	}
	public void setCamposDetalle(List<SelectDTO> camposDetalle) {
		this.camposDetalle = camposDetalle;
	}
	public boolean isNotCamposDetalle() {
		return notCamposDetalle;
	}
	public void setNotCamposDetalle(boolean notCamposDetalle) {
		this.notCamposDetalle = notCamposDetalle;
	}
	public String getTotalizar() {
		return totalizar;
	}
	public void setTotalizar(String totalizar) {
		this.totalizar = totalizar;
	}
	public boolean isNotTotalizar() {
		return notTotalizar;
	}
	public void setNotTotalizar(boolean notTotalizar) {
		this.notTotalizar = notTotalizar;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public boolean isNotAlias() {
		return notAlias;
	}
	public void setNotAlias(boolean notAlias) {
		this.notAlias = notAlias;
	}
	public String getIsin() {
		return isin;
	}
	public void setIsin(String isin) {
		this.isin = isin;
	}
	public boolean isNotIsin() {
		return notIsin;
	}
	public void setNotIsin(boolean notIsin) {
		this.notIsin = notIsin;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public boolean isNotClase() {
		return notClase;
	}
	public void setNotClase(boolean notClase) {
		this.notClase = notClase;
	}
	public String getMercado() {
		return mercado;
	}
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	public boolean isNotMercado() {
		return notMercado;
	}
	public void setNotMercado(boolean notMercado) {
		this.notMercado = notMercado;
	}
	public List<List<ComponenteDirectivaDTO>> getListComponenteDirectivaDTO() {
		return this.listComponenteDirectivaDTO;
	}
	public void setListComponenteDirectivaDTO(
			List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO) {
		this.listComponenteDirectivaDTO = listComponenteDirectivaDTO;
	}
}
