package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosFiltros;

@Repository
public interface Tmct0InformesDao  extends JpaRepository<Tmct0Informes,Integer> {

  public List<Tmct0Informes> findBynbdescription(String nbdescription);
  
  @Query( "SELECT m FROM Tmct0Informes m WHERE m.estado = 'A' AND m.nbdescription = :nbdescription" )
  public List<Tmct0Informes> findBynbdescriptionAndEstado(@Param("nbdescription") String nbdescription);
  
  @Query( "SELECT m FROM Tmct0Informes m WHERE m.idPadre IS NULL AND m.estado = 'A' order by m.nbdescription asc" )
  public List<Tmct0Informes> findInformesOrderByNbdescription();
  
  @Query(value="SELECT IDF.IDINFORME, INF.NOMBRE, INF.TIPO, INF.SUBTIPO, INF.CDTABLA, INF.CAMPO, INF.VALOR, INF.QUERY, CAST (IDF.VALOR_FILTRO AS VARCHAR(30000)) FROM BSNBPSQL.TMCT0_INFORMES_DATOS_FILTROS IDF JOIN BSNBPSQL.TMCT0_INFORMES_FILTROS INF ON INF.ID=IDF.IDFILTRO WHERE IDF.IDINFORME = :idInforme",nativeQuery=true)
  public List<Object[]> findGenericElementsDataByInformeId(@Param("idInforme") Integer idInforme);

  /**
   *	Devuelve valores si hay campos TIPO FECHA para ese informe que tengan valor precargado.
   *	@param idInforme idInforme
   *	@return List<Tmct0InformesDatosFiltros> List<Tmct0InformesDatosFiltros>  
   */
  @Query( "SELECT idf FROM Tmct0InformesDatosFiltros idf, Tmct0InformesFiltros ifi, Tmct0InformesClases icl WHERE idf.tmct0InformesFiltros.id = ifi.id AND icl.clase = ifi.clase AND idf.tmct0Informes.id = :idInforme AND (ifi.tipo = 'FECHA' AND  idf.valorFiltro <> '' AND idf.valorFiltro IS NOT NULL))" )
  public List<Tmct0InformesDatosFiltros> findTipoFechaConValorFiltroByInformeId(@Param("idInforme") Integer idInforme);
  
  /**
   *	Devuelve valores si hay campos TIPO distinto de FECHA para ese informe que tengan valor precargado.
   *	@param idInforme idInforme
   *	@return List<Tmct0InformesDatosFiltros> List<Tmct0InformesDatosFiltros>  
   */
  @Query( "SELECT inf FROM Tmct0Informes inf, Tmct0InformesClases icl, Tmct0InformesFiltros ifi WHERE inf.tmct0InformesClases.id = icl.id AND inf.tmct0InformesClases.id = inf.tmct0InformesClases.id AND inf.id = :idInforme AND ifi.tipo <> 'FECHA'" )
  public List<Tmct0InformesDatosFiltros> findTipoDistintoFechaConValorFiltroByInformeId(@Param("idInforme") Integer idInforme);
  
  /**
   *	Devuelve valores si hay campos TIPO FECHA para ese informe.
   *	@param idInforme idInforme
   *	@return List<Tmct0InformesDatosFiltros> List<Tmct0InformesDatosFiltros>  
   */
  @Query( "SELECT idf FROM Tmct0InformesDatosFiltros idf, Tmct0InformesFiltros ifi, Tmct0InformesClases icl WHERE idf.tmct0InformesFiltros.id = ifi.id AND icl.clase = ifi.clase AND idf.tmct0Informes.id = :idInforme AND ifi.tipo = 'FECHA')" )
  public List<Tmct0InformesDatosFiltros> findFiltrosTipoFechaByInformeId(@Param("idInforme") Integer idInforme);
  
  @Query( "SELECT m.id FROM Tmct0Informes m WHERE m.idPadre = :idPadre")
  public List<Integer> getIdsInformesHijos(@Param("idPadre")Integer idInforme);
  
  @Query( "SELECT inf FROM Tmct0Informes inf WHERE inf.id = :idInforme or inf.idPadre = :idInforme")
  public List<Tmct0Informes> findByIdInformeHijosyPadres(@Param("idInforme") Integer idInforme);
  
  @Query( "SELECT inf.mercado FROM Tmct0Informes inf WHERE inf.id = :idInforme")
  public String findMercadoInformeById(@Param("idInforme") Integer idInforme);
  
  @Modifying @Query(value="DELETE FROM TMCT0_INFORMES WHERE IDINFORME IN (:id)",nativeQuery=true)
  public void deleteVariosInformes(@Param("id") List<Integer> id);
  
  @Query( "SELECT m FROM Tmct0Informes m WHERE m.idPadre IS NULL AND m.tmct0InformesClases.id = :idClase AND m.estado = 'A' order by m.nbdescription asc" )
  public List<Tmct0Informes> findInformesByClaseOrderByNbdescription(@Param("idClase") Integer idClase);
  
  @Query( "SELECT m FROM Tmct0Informes m WHERE m.idPadre IS NULL AND m.tmct0InformesClases.id = :idClase AND m.mercado = :mercado AND m.estado = 'A' order by m.nbdescription asc" )
  public List<Tmct0Informes> findInformesByClaseMercadoOrderByNbdescription(@Param("idClase") Integer idClase,@Param("mercado") String mercado);
  
}