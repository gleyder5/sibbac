package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad para la gestion de TMCT0_INFORMES_DATOS_FILTROS.
 */
@Entity
@Table( name = "TMCT0_INFORMES_DATOS_FILTROS" )
public class Tmct0InformesDatosFiltros implements java.io.Serializable {

  
	/**
	 * 
	 */
	private static final long serialVersionUID = 7944112358199114525L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	/**
	 * identificador de la tabla TMCT0_INFORMES
	 */
	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name = "IDINFORME")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0Informes tmct0Informes;
	
	/**
	 * identificador de la tabla TMCT0_INFORMES_FILTRO
	 */
	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name = "IDFILTRO")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0InformesFiltros tmct0InformesFiltros;
	
	@Column(name = "VALOR_FILTRO", length = 30000, nullable = true)
	private String valorFiltro;
	
	@Column(name = "TIPO_BUSQUEDA", length = 2, nullable = true)
	private String tipoBusqueda;

	/**
	 *	constructor no-arg. 
	 */
	public Tmct0InformesDatosFiltros() {}

	/**
	 *	constructor arg.
	 *	@param id
	 *	@param tmct0Informes 
	 *	@param tmct0InformesFiltros 
	 *	@param valorFiltro 
	 */
	public Tmct0InformesDatosFiltros(Integer id, Tmct0Informes tmct0Informes,
			Tmct0InformesFiltros tmct0InformesFiltros, String valorFiltro, String tipoBusqueda) {
		super();
		this.id = id;
		this.tmct0Informes = tmct0Informes;
		this.tmct0InformesFiltros = tmct0InformesFiltros;
		this.valorFiltro = valorFiltro;
		this.tipoBusqueda=tipoBusqueda;
	}

	/**
	 *	@return id 
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 *	@param id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *	@return tmct0Informes 
	 */
	public Tmct0Informes getTmct0Informes() {
		return this.tmct0Informes;
	}

	/**
	 *	@param tmct0Informes 
	 */
	public void setTmct0Informes(Tmct0Informes tmct0Informes) {
		this.tmct0Informes = tmct0Informes;
	}

	/**
	 *	@return tmct0InformesFiltros 
	 */
	public Tmct0InformesFiltros getTmct0InformesFiltros() {
		return this.tmct0InformesFiltros;
	}

	/**
	 *	@param tmct0InformesFiltros 
	 */
	public void setTmct0InformesFiltros(Tmct0InformesFiltros tmct0InformesFiltros) {
		this.tmct0InformesFiltros = tmct0InformesFiltros;
	}

	/**
	 *	@return valorFiltro 
	 */
	public String getValorFiltro() {
		return this.valorFiltro;
	}

	/**
	 *	@param valorFiltro 
	 */
	public void setValorFiltro(String valorFiltro) {
		this.valorFiltro = valorFiltro;
	}
	
	/**
	 * @return the tipoBusqueda
	 */
	public String getTipoBusqueda() {
		return tipoBusqueda;
	}

	/**
	 * @param tipoBusqueda the tipoBusqueda to set
	 */
	public void setTipoBusqueda(String tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}
}
