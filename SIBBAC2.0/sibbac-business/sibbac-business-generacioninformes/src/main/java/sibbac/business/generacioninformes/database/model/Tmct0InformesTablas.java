package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;

@Entity
@Proxy(lazy = false)
@Table(name = "TMCT0_INFORMES_TABLAS")
public class Tmct0InformesTablas {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1589483921783978327L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	/**
	 * identificador de la tabla TMCT0_INFORMES_CLASES
	 */
	@ManyToOne()
	@JoinColumn(name = "IDINFORME")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0Informes tmct0Informes;

	@Column(name = "TABLA", length = 50, nullable = false)
	private String tabla;
	
	
	public Tmct0InformesTablas(){
		
	}

	public Tmct0InformesTablas(Integer id, Tmct0Informes tmct0Informes, String tabla) {
		super();
		this.id = id;
		this.tmct0Informes = tmct0Informes;
		this.tabla = tabla;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tmct0Informes getTmct0Informes() {
		return tmct0Informes;
	}

	public void setTmct0Informes(Tmct0Informes tmct0Informes) {
		this.tmct0Informes = tmct0Informes;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

}
