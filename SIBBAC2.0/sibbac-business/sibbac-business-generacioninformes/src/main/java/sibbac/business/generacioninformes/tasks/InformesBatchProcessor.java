package sibbac.business.generacioninformes.tasks;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssemblerNEW;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.OrigenInformeExcelEnum;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.helper.GenericLoggers;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class InformesBatchProcessor implements Callable<Void> {

  private static final Logger LOG = LoggerFactory.getLogger(InformesBatchProcessor.class);
  
  private static final String CHAR_SET = "UTF-8";

  private DataSource ds;

  private SendMailExecutorInformes sendMailExecutor;

  private Tmct0InformesPlanificacion tmct0InformesPlanificacion;

  private File workdirectory;
  
  private Future<Void> future;

  @Autowired
  private Tmct0InformesBoNEW tmct0InformesBoNEW;
  
  @Autowired
  private Tmct0InformesDao tmct0InformesDao;
  
  @Autowired
  Tmct0InformeAssemblerNEW tmct0InformeAssemblerNEW;

  @Value("${batch.compress : true}")
  private boolean compress;

  File origen;

  File destino;
  
  
  @Autowired
  public void setDs(DataSource ds) {
    this.ds = ds;
  }
  
  @Autowired
  public void setSendMailExecutor(SendMailExecutorInformes sendMailExecutor) {
    this.sendMailExecutor = sendMailExecutor;
  }
  
  public void setWorkdirectory(File workdirectory) {
    this.workdirectory = workdirectory;
  }

  public void setFuture(Future<Void> future) {
    this.future = future;
  }
  
  public Future<Void> getFuture() {
    return future;
  }

  public Tmct0InformesPlanificacion getTmct0InformesPlanificacion() {
	  return this.tmct0InformesPlanificacion;
  }

  public void setTmct0InformesPlanificacion(
		  Tmct0InformesPlanificacion tmct0InformesPlanificacion) {
	  this.tmct0InformesPlanificacion = tmct0InformesPlanificacion;
  }

  @Override
  public Void call() throws Exception {
	  LOG.debug("Id de planificación a ejecutarse en la task: " + this.getTmct0InformesPlanificacion().getId());
	  LOG.debug("Id del informe a ejecutarse en la task: " + this.getTmct0InformesPlanificacion().getTmct0Informes().getId());
	  LOG.debug("[call] Inicio...");

	  try {
		  // CASO 2: Si hay presentes filtros de fecha sin fechas -> No se ejecuta la generacion de informe.
		  if (this.getTmct0InformesPlanificacion() != null 
				  && this.tmct0InformesBoNEW.isGeneraInformeForTask(tmct0InformesPlanificacion)) {
	
			  // Si el valor del TIPO_DESTINO de la planificación es RUTA se genera un informe Excel
			  Map<String, Object> paramsObjects = new HashMap<String, Object>();
	
			  paramsObjects.put("plantilla", this.getTmct0InformesPlanificacion().getTmct0Informes().getId());
	
			  // Valores por defecto para la task.
			  paramsObjects.put("especiales", "EXCLROUTING");
			  paramsObjects.put("userName", "PLANIFICACION");
			  
			  List<Object[]> listaElementosGenericos = this.tmct0InformesDao.findGenericElementsDataByInformeId(
					  this.getTmct0InformesPlanificacion().getTmct0Informes().getId());
	
			  // Se cargan los valores necesarios del componente generico 
			  paramsObjects.put("listaFiltrosDinamicos", 
					  tmct0InformeAssemblerNEW.getListMapByInformeIdForTask(listaElementosGenericos));
			  
			  this.tmct0InformesBoNEW.getInformeExcel(
					  paramsObjects, false, true, this.getTmct0InformesPlanificacion(), OrigenInformeExcelEnum.TASK_BATCH.getOrigen(), null);
			  
		  } else {
			  // Si no hay filtros dinamicos de tipo FECHA con valor precargado en la tabla de informes datos filtro
			  // la tarea no se ejecuta
			  GenericLoggers.logError(
					  InformesBatchProcessor.class, Constantes.GENERACION_INFORMES, "call", 
					  "No se ha podido generar el informe ya que no hay filtros dinámicos tipo fecha con datos disponibles"
					  + " para informe con id = " 
					  + (this.getTmct0InformesPlanificacion() != null ? 
							(this.getTmct0InformesPlanificacion().getTmct0Informes() != null ? this.getTmct0InformesPlanificacion().getTmct0Informes().getId() : null ) 
							: null ));
		  }
	  } catch (Exception e) {
		  GenericLoggers.logError(
				  InformesBatchProcessor.class, Constantes.GENERACION_INFORMES, "call", 
				  "Error al correr la Task Batch para generar informe.");
		  throw new Exception();
	  }
	  LOG.debug("[call] Fin...");
      return null;
  }

  @Value("${batch.workdirectory ?: /sbrmv/ficheros/batch}")
  void setWorkDirectory(String workdirectory) {
    this.workdirectory = new File(workdirectory);
  }

}
