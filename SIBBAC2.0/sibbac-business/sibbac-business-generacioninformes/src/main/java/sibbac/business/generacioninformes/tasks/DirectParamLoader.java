package sibbac.business.generacioninformes.tasks;

import static java.text.MessageFormat.format;

import java.util.List;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import sibbac.business.batch.common.BatchException;
//import sibbac.business.batch.database.model.Param;

class DirectParamLoader {

  private static final Logger LOG = LoggerFactory.getLogger(DirectParamLoader.class);

  private final StatementExecutor executor;

  DirectParamLoader(StatementExecutor executor) {
    this.executor = executor;
  }
/*
 	// TODO - Determinar si se necesita una tabla para la ejecución de registros en TMCT0_BATCH_QUERY_PARAM
  void executeQuery(PreparedStatement pstmt, List<Param> parameters) throws BatchException {
    String message;
    
    LOG.debug("[executeQuery] Inicio...");
    for (Param param : parameters) {
      try {
        if (param.getValue() == null) {
          param.fillNullParameter(pstmt);
        }
        else {
          param.fillParameter(pstmt, param.getValue());
        }
      }
      catch (SQLException ex) {
        message = format("Al llenar el parametro {0} con el valor {1}", param.getId().toString(),
            param.getValue());
        LOG.error("[executeQuery] {}", message);
        throw new BatchException(message, ex);
      }
    }
    executor.executeStatement(pstmt);
    LOG.debug("[executeQuery] Fin");
  }
*/
}
