/**
 * 
 */
package sibbac.business.generacioninformes.database.dto;

/**
 * @author lucio.vilar
 *
 */
public class Tmct0PlanificacionInformeDTO {

	private Integer idPlanificacion;
	private String informe;
	private String idInforme;
	private String periodo;
	private String idPeriodo;
	private String nombreFichero;
	private String tipoDestino;
	private String destino;
	private String formato;
	private String separadorDecimal;
	private String separadorMiles;
	private String separadorCampos;
	
	public Tmct0PlanificacionInformeDTO() {}
	
	public Tmct0PlanificacionInformeDTO(Integer id, String unInforme, String idInf, String unPeriodo, String idPer, String unNombreFichero, String unTipoDest, String unDestino, String unFormato, String unSeparadorDec, String unSeparadorMil, String unSeparadorCampo) {
		this.idPlanificacion = id;
		this.informe = unInforme;
		this.idInforme = idInf;
		this.periodo = unPeriodo;
		this.idPeriodo = idPer;
		this.nombreFichero = unNombreFichero;
		this.tipoDestino = unTipoDest;
		this.destino = unDestino;
		this.formato = unFormato;
		this.separadorDecimal = unSeparadorDec;
		this.separadorMiles = unSeparadorMil;
		this.separadorCampos = unSeparadorCampo;
	}

	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public String getTipoDestino() {
		return tipoDestino;
	}

	public void setTipoDestino(String tipoDestino) {
		this.tipoDestino = tipoDestino;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getSeparadorDecimal() {
		return separadorDecimal;
	}

	public void setSeparadorDecimal(String separadorDecimal) {
		this.separadorDecimal = separadorDecimal;
	}

	public String getSeparadorMiles() {
		return separadorMiles;
	}

	public void setSeparadorMiles(String separadorMiles) {
		this.separadorMiles = separadorMiles;
	}

	public String getSeparadorCampos() {
		return separadorCampos;
	}

	public void setSeparadorCampos(String separadorCampos) {
		this.separadorCampos = separadorCampos;
	}

	public Integer getIdPlanificacion() {
		return idPlanificacion;
	}

	public void setIdPlanificacion(Integer idPlanificacion) {
		this.idPlanificacion = idPlanificacion;
	}

	public String getIdInforme() {
		return idInforme;
	}

	public void setIdInforme(String idInforme) {
		this.idInforme = idInforme;
	}

	public String getIdPeriodo() {
		return idPeriodo;
	}

	public void setIdPeriodo(String idPeriodo) {
		this.idPeriodo = idPeriodo;
	}

	
}
