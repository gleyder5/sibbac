/**
 * 
 */
package sibbac.business.generacioninformes.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesClasesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesPlanificacionDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesPlanificacionDaoImp;
import sibbac.business.generacioninformes.database.dto.ClaseInformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0PlanificacionInformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0PlanificacionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssemblerNEW;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0PlanificacionInformeAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesClases;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.OrigenInformeExcelEnum;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.PlanifInformeTipoDestinoEnum;
import sibbac.business.generacioninformes.utils.Constantes;
import sibbac.business.periodicidades.database.dao.ReglaPeriodicidadDao;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.dto.InfoMailContactosDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * @author lucio.vilar
 *
 */
@SIBBACService
public class SIBBACServicePlanificacionInformes implements SIBBACServiceBean {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServicePlanificacionInformes.class);
	
	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";
	
	/** Clave Sentidos de la tabla TMCT0_SIBBAC_CONTANTES. */
	private static final String CLAVE_TIPO_DESTINO = "comboTipoDestino";
	
	/** Clave Sentidos de la tabla TMCT0_SIBBAC_CONTANTES. */
	private static final String CLAVE_FORMATO = "comboFormato";
	
	@Autowired
	private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;
	
	@Autowired
	private Tmct0InformesDao tmct0InformesDao;
	
	@Autowired
	private ReglaPeriodicidadDao reglaPeriodicidadDao;
	
	@Autowired
	private Tmct0InformesPlanificacionDao tmct0InformesPlanificacionDao;
	
	@Autowired
	private Tmct0InformesPlanificacionDaoImp tmct0InformesPlanificacionDaoImp;

	@Autowired
	private Tmct0PlanificacionInformeAssembler tmct0PlanificacionInformeAssembler;
	
	@Autowired
	private Tmct0InformesDatosDao tmct0InformesDatosDao;
	
	@Autowired
	private Tmct0InformesBoNEW tmct0InformesBoNEW;

	@Autowired
	Tmct0InformeAssemblerNEW tmct0InformeAssemblerNEW;
	
	@Autowired
	Tmct0InformesClasesDao tmct0InformesClasesDao;

	@Value("${generacioninformes.mensaje.validacion.noBodyNoSubject}")
	private String msgNoBodyNoSubject;
	
	@Value("${generacioninformes.mensaje.validacion.noBody}")
	private String msgNoBody;
	
	@Value("${generacioninformes.mensaje.validacion.noSubject}")
	private String msgNoSubject;
	
	@Value("${generacioninformes.batch.mail.subject.spa}")
	private String msgMailSubject;
	
	@Value("${generacioninformes.batch.mail.body.spa}")
	private String msgMailBody;
	
	private enum ComandosPlanifInformes {
	    /** Inicializa los datos del filtro de pantalla */
	    CARGA_SELECTS_ESTATICOS("CargaSelectsEstaticos"),
	    /** Carga combo informe */
	    CARGA_COMBO_INFORME("CargaComboInforme"),
	    /** Carga combo informe */
	    CARGA_COMBO_PERIODO("CargaComboPeriodo"),
	    /** Carga autorellenable fichero */
	    CARGA_AUTORELLENABLE_FICHERO("CargaAutorellenableFichero"),
	    /** Carga autorellenable fichero */
	    CARGA_AUTORELLENABLE_DESTINO("CargaAutorellenableDestino"),
	    /** Busca las planificaciones de informes */
	    BUSCAR_PLANIFICACION_INFORMES("CargaPlanificacionInformes"),
	    /** Guarda una planificacion nueva */
	    GUARDAR_PLANIFICACION_INFORMES("GuardarPlanificacionInformes"),
	    /** Elimina una planificacion de informe */
	    ELIMINAR_PLANIFICACION_INFORMES("EliminarPlanificacionInformes"),
	    /** Obtiene el detalle de una planificación de informe */
	    DETALLE_PLANIFICACION_INFORMES("DetallePlanificacionInforme"),
	    /** Valida una planificación de informe */
	    VALIDAR_PLANIFICACION_INFORMES("ValidarPlanificacionInforme"),
	    CARGA_CLASES("CargaClases"),
	    CARGA_PLANTILLAS_FILTRO("CargarPlantillasFiltro"),
	    /** Sin comando*/
		SIN_COMANDO("");
		
	    /** The command. */
	    private String command;

	    private ComandosPlanifInformes(String comando) {
	    	this.command = comando;
	    }
	    
	    /**
	     * Gets the command.
	     *
	     * @return the command
	     */
	    public String getCommand() {
	    	return command;
	    }
	}

	/* (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
//	    Map<String, String> filters = webRequest.getFilters();
//	    List<Map<String, String>> parametros = webRequest.getParams();
	    Map<String, Object> paramsObjects = webRequest.getParamsObject();
	    WebResponse result = new WebResponse();
	    Map<String, Object> resultados = new HashMap<String, Object>();
	    Map<String, Object> resultadosError = new HashMap<String, Object>();
	    ComandosPlanifInformes comando = getCommand(webRequest.getAction());
	    
	    try {
	        LOG.debug("Entro al servicio SIBBACServicePlantillasInformesNEW");

	        switch (comando) {
	          case CARGA_SELECTS_ESTATICOS:
	            result.setResultados(cargarSelectsEstaticos());
	            break;
	          case CARGA_COMBO_INFORME:
		            result.setResultados(cargarComboInforme());
		            break;
	          case CARGA_COMBO_PERIODO:
		            result.setResultados(cargarComboPeriodo());
		            break;
	          case CARGA_AUTORELLENABLE_FICHERO:
		            result.setResultados(cargarComboFichero());
		            break;
	          case CARGA_AUTORELLENABLE_DESTINO:
		            result.setResultados(cargarComboDestino());
		            break;
	          case BUSCAR_PLANIFICACION_INFORMES:
	        	  result.setResultados(buscarPlanificacionesInformes(paramsObjects));
	        	  break;
	          case VALIDAR_PLANIFICACION_INFORMES:
	        	  /** TODO - HACER EL WIRING CON EL FRONT. **/
	        	  resultadosError = this.validarPlanificacionInforme(paramsObjects);
		        	if (resultadosError != null && !resultadosError.isEmpty()) {
		        		result.setResultadosError(resultadosError);
		        	}
		        	break;
	          case GUARDAR_PLANIFICACION_INFORMES:
	        	  result.setResultados(guardarPlanificacionInformes(paramsObjects));
	        	  break;
	          case ELIMINAR_PLANIFICACION_INFORMES:
	        	  result.setResultados(eliminarPlanificacionInformes(paramsObjects));
	        	  break;
	          case DETALLE_PLANIFICACION_INFORMES:
	        	  result.setResultados(detallePlanificacionInforme(paramsObjects));
	        	  break;
		        case CARGA_CLASES:
		            result.setResultados(cargarClases());
		            break;
		        case CARGA_PLANTILLAS_FILTRO:
		            result.setResultados(cargarPlantillasMercadoFiltro(paramsObjects));
		            break;
	          default:
	              result.setError("An action must be set. Valid actions are: " + comando);
	        }
	    	
		} catch (SIBBACBusinessException e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

	    LOG.debug("Salgo del servicio SIBBACServicePlanificacionInformes");
	    return result;
	}
	
	/**
	 * Valida si debe generarse o no una planificacion con campo destino vacio.
	 * @param paramsObjects paramsObjects
	 * @return Map<String, Object> Map<String, Object>
	 * @throws Exception 
	 */
	private Map<String, Object> validarPlanificacionInforme(Map<String, Object> paramsObjects) throws Exception {
		LOG.trace("Inicio Valida Envio Mails Contacto");
		Map<String, Object> result = new HashMap<String, Object>();
		// Lo primero para esta validacion es si el campo de destino esta vacio y es de tipo mail 
		Tmct0InformesPlanificacion tmct0InformesPlanificacion = tmct0PlanificacionInformeAssembler.getTmct0InformesPlanificacion(paramsObjects);

		// La validacion solo se lleva a cabo si el campo de tipo destino es EMAIL 
		// y el campo destino esta en blanco
		if (StringUtils.isNotBlank(tmct0InformesPlanificacion.getTipoDestino()) 
				&& tmct0InformesPlanificacion.isTipoDestinoEMAIL() 
				&& StringUtils.isBlank(tmct0InformesPlanificacion.getDestino()) 
				&& tmct0InformesPlanificacion.getTmct0Informes() != null) {

			/************************* VALIDACION RESTRICTIVA LITERAL ALIAS *************************/
			// Se busca campo de separacion en posicion 1 con nombre del literal 'CDALIAS' para ese informe.
			Tmct0InformesDatos tmct0InformesDatos = this.tmct0InformesDatosDao.findByTipoCampoAndNbCampoAndNuposiciAndInformeId(
					GeneracionInformesEnum.TipoCampoInformeEnum.SEPARACION.getValue(), 
					Constantes.LITERAL_CDALIAS, 1, tmct0InformesPlanificacion.getTmct0Informes().getId());

			// Si no se encuentra lo anterior se debe emitir mensaje de confirmacion.
			if (tmct0InformesDatos == null) {
				StringBuffer sbMsgValidPlanif = new StringBuffer();
				sbMsgValidPlanif.append("Para generar una planificación de informe sin informar el campo destino "
						+ "es obligatorio que el informe contenga campos de separación y que el primero de ellos sea un alias");
				result.put("msgValidPlanif", sbMsgValidPlanif.toString());
				return result;
			}

			/************************* VALIDACION NO-RESTRICTIVA BODY Y/O SUBJECT *************************/
			/*** TODO - HAY QUE CORRER LA QUERY PARA VERIFICAR VALIDACION ***/
			// Si el valor del TIPO_DESTINO de la planificación es RUTA se genera un informe Excel
			Map<String, Object> paramsObjectsPlanif = new HashMap<String, Object>();

			paramsObjectsPlanif.put("plantilla", tmct0InformesPlanificacion.getTmct0Informes().getId());

			// Valores por defecto para la task.
			paramsObjectsPlanif.put("especiales", "EXCLROUTING");
			paramsObjectsPlanif.put("userName", "SIBBAC");

			List<Object[]> listaElementosGenericos = this.tmct0InformesDao.findGenericElementsDataByInformeId(
					tmct0InformesPlanificacion.getTmct0Informes().getId());

			// Se cargan los valores necesarios del componente generico 
			paramsObjectsPlanif.put("listaFiltrosDinamicos", 
					tmct0InformeAssemblerNEW.getListMapByInformeIdForTask(listaElementosGenericos));

			Map<String, Object> mapa = this.tmct0InformesBoNEW.getInformeExcel(
					paramsObjectsPlanif, true, true, tmct0InformesPlanificacion, OrigenInformeExcelEnum.VALIDACION.getOrigen(), null);
			List<InfoMailContactosDTO> listInfoMailContactosDTO = 
					this.tmct0InformesBoNEW.getInfoMailsContacto(tmct0InformesPlanificacion, (Tmct0InformeGeneradoDTO) mapa.get("informe"));
			String msgConfirmPlanif = this.getTextoValidacionBodySubjectMailContacto(listInfoMailContactosDTO);
			if (CollectionUtils.isEmpty(listInfoMailContactosDTO)) {
				result.put("msgConfirmPlanif", msgNoBodyNoSubject + " <br/><br/> " + msgMailSubject + " <br/> " + msgMailBody);
			} else if (StringUtils.isNotBlank(msgConfirmPlanif)) {
				result.put("msgConfirmPlanif", this.getTextoValidacionBodySubjectMailContacto(listInfoMailContactosDTO));
				return result;
			}
		}
		LOG.trace("Fin Valida Envio Mails Contacto");
		return result;
	}
	
	/**
	 *	Si no existen datos de subject o body en la lista de contactos 
	 *	se devuelve el mensaje correspondiente para informarle al usuario por pantalla.
	 *	@param listInfoMailContactosDTO listInfoMailContactosDTO
	 *	@throws SIBBACBusinessException SIBBACBusinessException
	 *	@return String String 
	 */
	private String getTextoValidacionBodySubjectMailContacto(List<InfoMailContactosDTO> listInfoMailContactosDTO) throws SIBBACBusinessException {
		// No existe SUBJECT parametrizado en BBDD para el cliente seleccionado, se pondrá por defecto el siguiente: (mostrar el texto de SUBJECT del .properties)
		// No existe BODY parametrizado en BBDD para el cliente seleccionado, se pondrá por defecto el siguiente: (mostrar el texto de SUBJECT del .properties)
		StringBuffer sbMsgConfirmPlanif = new StringBuffer(); 
		if (CollectionUtils.isNotEmpty(listInfoMailContactosDTO)) {
			for (InfoMailContactosDTO infMailContactoDTO : listInfoMailContactosDTO) {
				if (StringUtils.isNotBlank(infMailContactoDTO.getInformesSubjectAndBody())) {
					String[] arrBodySubject = infMailContactoDTO.getInformesSubjectAndBody().split("\\|\\|");
					if (arrBodySubject.length > 0) {
						if (StringUtils.isBlank(arrBodySubject[0])) {
							// Subject el el elemento 0
							sbMsgConfirmPlanif.append(msgNoSubject + " " + msgMailSubject);
						} else if (StringUtils.isBlank(arrBodySubject[1])) {
							// Body el el elemento 1
							sbMsgConfirmPlanif.append(msgNoBody + " " + msgMailBody);
						}
					}
				} else {
					sbMsgConfirmPlanif.append(msgNoBodyNoSubject + " <br/><br/> " + msgMailSubject + " <br/> " + msgMailBody);
				}
			}
		}
		return sbMsgConfirmPlanif.toString();
	}
	
	@Transactional
	private Map<String, Object> eliminarPlanificacionInformes(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			if (null != paramsObjects && !paramsObjects.get("listaPlanificacionesBorrar").equals("")) {
				@SuppressWarnings("unchecked")
				List<Integer> idsEliminar = (List<Integer>) paramsObjects.get("listaPlanificacionesBorrar");
				
				if (null != idsEliminar) {
					for (Integer id : idsEliminar) {
						tmct0InformesBoNEW.eliminarPlanificacionesInforme(id);
					}
				}
			}
			result.put("status", "OK");
		} catch (Exception e) {
			result.put("status", "KO");
		}
		
		return result;
	}

	@Transactional
	private Map<String, Object> guardarPlanificacionInformes(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			Tmct0InformesPlanificacion entity = tmct0PlanificacionInformeAssembler.getTmct0InformesPlanificacion(paramsObjects);
			tmct0InformesPlanificacionDao.save(entity);
			result.put("status", "OK");
		} catch (Exception e) {
			result.put("status", "KO");
		}
		
		return result;
	}

	private Map<String, Object> buscarPlanificacionesInformes(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		LOG.trace("Iniciando busqueda de planificaciones de informes");
		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {
			
			Tmct0PlanificacionInformeFilterDTO filter = this.tmct0PlanificacionInformeAssembler.getTmct0PlanificacionInformeFilterDTO(paramsObjects);
			
			List<Tmct0InformesPlanificacion> listEntidad = this.tmct0InformesPlanificacionDaoImp.buscarInformesPlanificacionByFilter(filter);
			List<Tmct0PlanificacionInformeDTO> listDto =  new ArrayList<Tmct0PlanificacionInformeDTO>();
			
			if (null != listEntidad && !listEntidad.isEmpty()) {
				for (Tmct0InformesPlanificacion tmct0InformesPlanificacion : listEntidad) {
					listDto.add(this.tmct0PlanificacionInformeAssembler.getTmct0PlanificacionInformeDTO(tmct0InformesPlanificacion));
				}
			}

			mSalida.put("listInformesPlanificacion", listDto);
			mSalida.put("status", "OK");
		} catch (Exception e) {
			LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la busqueda de planificaciones de informes");
		}
		LOG.debug("Fin busqueda de planificaciones de informes");
		
		return mSalida;
	}

	private Map<String, Object> cargarComboDestino() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para el combo fichero del filtro");
		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {
			
			List<SelectDTO> listaDestino = new ArrayList<SelectDTO>();
			List<String> listDestinos = tmct0InformesPlanificacionDao.buscarDestinosPlanificacionInformes();

			if (null != listDestinos) {
				// Se transforman a lista de DTO
				for (String destinos : listDestinos) {
					SelectDTO destino = new SelectDTO(destinos, destinos);
					listaDestino.add(destino);
				}
			}
			
			mSalida.put("listDestino", listaDestino);
			
		} catch (Exception e) {
			LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}
		LOG.debug("Fin inicializacion datos filtro de fichero");
		
		return mSalida;
	}

	private Map<String, Object> cargarComboFichero() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para el combo fichero del filtro");
		Map<String, Object> mSalida = new HashMap<String, Object>();
		
		try {
			List<SelectDTO> listaFicheros = new ArrayList<SelectDTO>();
			List<String> listNombreFicheros = tmct0InformesPlanificacionDao.buscarNombreFicherosPlanificacionInformes();

			if (null != listNombreFicheros) {
				// Se transforman a lista de DTO
				for (String nombreFicheros : listNombreFicheros) {
					SelectDTO informe = new SelectDTO(nombreFicheros, nombreFicheros);
					listaFicheros.add(informe);
				}
			}
			
			mSalida.put("listFicheros", listaFicheros);
			
		} catch (Exception e) {
			LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}
		LOG.debug("Fin inicializacion datos filtro de fichero");

		return mSalida;
	}

	private Map<String, Object> cargarComboPeriodo() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para el combo periodo del filtro");
		Map<String, Object> mSalida = new HashMap<String, Object>();
		
		try {
			
			List<SelectDTO> listaPeriodos = new ArrayList<SelectDTO>();
			
			List<ReglaPeriodicidad> listPeriodicidad = reglaPeriodicidadDao.findAll();

			// Se transforman a lista de DTO
			for (ReglaPeriodicidad periodo : listPeriodicidad) {
				SelectDTO periodoDTO = new SelectDTO(periodo.getId().toString(), periodo.getIdReglaUsuario());
				listaPeriodos.add(periodoDTO);
			}
			
			mSalida.put("listPeriodos", listaPeriodos);
			
		} catch (Exception e) {
			LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}
		LOG.debug("Fin inicializacion datos filtro de periodo");

		return mSalida;
	}

	/**
	 * Carga inicial de los datos estaticos de la pantalla
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Map<String, Object> cargarSelectsEstaticos() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para combos estaticos del filtro");
		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {
			List<SelectDTO> listaTipoDestino = new ArrayList<SelectDTO>();
			List<SelectDTO> listaFormatos = new ArrayList<SelectDTO>();
			String[] lisTipoDestinoSplit = null;
			String[] lisFormatoSplit = null;

			// Realizamos la llamada a la consulta de conceptos
			List<Tmct0SibbacConstantes> lisTipoDestino = tmct0SibbacConstantesDao.findByClave(CLAVE_TIPO_DESTINO);

			if (null!=lisTipoDestino && lisTipoDestino.size()>0) {
				lisTipoDestinoSplit = StringUtils.splitPreserveAllTokens(lisTipoDestino.get(0).getValor(), ",");
			}

			// Se transforman a lista de DTO
			for (String sentido : lisTipoDestinoSplit) {

				SelectDTO tipoDestinoDTO = new SelectDTO(sentido, sentido);
				listaTipoDestino.add(tipoDestinoDTO);
			}

			mSalida.put("listaTipoDestino", listaTipoDestino);

			// Realizamos la llamada a la consulta de conceptos
			List<Tmct0SibbacConstantes> lisFormato = tmct0SibbacConstantesDao.findByClave(CLAVE_FORMATO);

			if (null!=lisFormato && lisFormato.size()>0) {
				lisFormatoSplit = StringUtils.splitPreserveAllTokens(lisFormato.get(0).getValor(), ",");
			}

			// Se transforman a lista de DTO
			for (String formato : lisFormatoSplit) {
				SelectDTO formatoDTO = new SelectDTO(formato, formato);
				listaFormatos.add(formatoDTO);
			}

			mSalida.put("listaFormatos", listaFormatos);

		} catch (Exception e) {
			LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}
		LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

		return mSalida;
	}
	
	private Map<String, Object> detallePlanificacionInforme(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		LOG.trace("Iniciando obtención detalle planificación informe");
		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {
			
			Tmct0InformesPlanificacion planificacionInforme = this.tmct0InformesPlanificacionDao.findOne((Integer)paramsObjects.get("idPlanificacion"));
			Tmct0PlanificacionInformeDTO dto =  new Tmct0PlanificacionInformeDTO();
			
			if (null != planificacionInforme) {
				dto = this.tmct0PlanificacionInformeAssembler.getTmct0PlanificacionInformeDTO(planificacionInforme);
			}

			mSalida.put("detallePlanificacionInforme", dto);
			mSalida.put("status", "OK");
		} catch (Exception e) {
			LOG.error("Error metodo detallePlanificacionInforme " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la busqueda de planificaciones de informes");
		}
		LOG.debug("Fin obtención detalle planificación informe");
		
		return mSalida;
	}
	
	private Map<String, Object> cargarComboInforme() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para combo informe del filtro");
		Map<String, Object> mSalida = new HashMap<String, Object>();
		List<SelectDTO> listaFormatos = new ArrayList<SelectDTO>();
		
		List<Tmct0Informes> listInformes = tmct0InformesDao.findInformesOrderByNbdescription();

		// Se transforman a lista de DTO
		for (Tmct0Informes tmct0Informes : listInformes) {
			SelectDTO formatoDTO = new SelectDTO(tmct0Informes.getId().toString(), tmct0Informes.getNbdescription());
			listaFormatos.add(formatoDTO);
		}
		
		mSalida.put("listInformes", listaFormatos);
		
		LOG.debug("Fin inicializacion datos combo informe del filtro");

		return mSalida;
	}
	
	
	/* (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List<String> getAvailableCommands() {
		List<String> result = new ArrayList<String>();
		return result;
	}

	/* (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List<String> getFields() {
		List<String> result = new ArrayList<String>();
		return result;
	}

	/**
	 * Gets the command.
	 *
	 * @param command
	 *            the command
	 * @return the command
	 */
	private ComandosPlanifInformes getCommand(String command) {
		ComandosPlanifInformes[] commands = ComandosPlanifInformes.values();
		ComandosPlanifInformes result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandosPlanifInformes.SIN_COMANDO;
		}
		LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
		return result;
	}

	private Map<String, Object> cargarClases() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para combo clase del filtro");

		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {

			List<ClaseInformeDTO> listaClases = new ArrayList<ClaseInformeDTO>();
			ClaseInformeDTO claseInformeDTO = null;
			List<Tmct0InformesClases> clases = tmct0InformesClasesDao.getAllInformesClases();
			
			if (null != clases) {
				for (Tmct0InformesClases tmct0InformesClases : clases) {
					claseInformeDTO = new ClaseInformeDTO(tmct0InformesClases.getId().toString(), tmct0InformesClases.getNombre(), tmct0InformesClases.getMercado(), tmct0InformesClases.getClase());
					listaClases.add(claseInformeDTO);
				}
			}
			
			mSalida.put("listClases", listaClases);
		} catch (Exception e) {
			LOG.error("Error metodo init - Clases Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}

		LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

		return mSalida;
	}
	
	  /**
	   * Carga las plantillas a raiz de un idClase
	   * 
	   * @return
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> cargarPlantillasMercadoFiltro(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

	    LOG.trace("Iniciando datos para el filtro de Plantillas Informes - cargarPlantillasMercadoFiltro ");

	    Map<String, Object> map = new HashMap<String, Object>();

	    try {
	    	Integer idClase = Integer.parseInt(((Map<String, String>) paramsObjects.get("clase")).get("id"));
	    	String clase = String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("clase"));
	    	String mercadoFiltro = "";
	    	if (null!=paramsObjects.get("mercado")) {
	    		mercadoFiltro = String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("value"));
	    	}
	    	SelectDTO selectPlantillas = null;
	    	SelectDTO selectMercados = null;

	    	//Obtenemos las plantillas asociadas a la clase seleccionada
	    	List<SelectDTO> listaPlantillasFiltro = new ArrayList<SelectDTO>();
	    	List<Tmct0Informes> listaPlantillasInformesFiltro = new ArrayList<Tmct0Informes>();
	    	if ("".equals(mercadoFiltro)) {
	    		listaPlantillasInformesFiltro = tmct0InformesDao.findInformesByClaseOrderByNbdescription(idClase);
	    	} else {
	    		listaPlantillasInformesFiltro = tmct0InformesDao.findInformesByClaseMercadoOrderByNbdescription(idClase,mercadoFiltro);
	    	}

	    	for (Tmct0Informes tmct0Informes : listaPlantillasInformesFiltro) {
	    		selectPlantillas = new SelectDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription());
	    		listaPlantillasFiltro.add(selectPlantillas);
	    	}

	    	map.put("listaPlantillasFiltro", listaPlantillasFiltro);
	    	
	    	if ("".equals(mercadoFiltro)) {
	    		//Obtenemos los mercados asociados a la clase seleccionada
	    		List<SelectDTO> listaMercados = new ArrayList<SelectDTO>();
	    		List<String> listaMercadosFiltro = tmct0InformesClasesDao.findMercadoByClase(clase);

	    		for (String mercado : listaMercadosFiltro) {
	    			selectMercados = new SelectDTO(mercado, mercado);
	    			listaMercados.add(selectMercados);
	    		}
	    		//Añadimos a la última posición el mercado TODOS
	    		selectMercados = new SelectDTO("TODOS", "TODOS");
	    		listaMercados.add(selectMercados);

	    		map.put("listaMercados", listaMercados);
	    	}

	    } catch (Exception e) {
	      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
	      throw new SIBBACBusinessException(
	                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarPlantillasFiltro");
	    }

	    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarPlantillasMercadoFiltro");

	    return map;
	  }
}
