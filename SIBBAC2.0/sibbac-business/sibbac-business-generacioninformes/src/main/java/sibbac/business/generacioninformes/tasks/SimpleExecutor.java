package sibbac.business.generacioninformes.tasks;

import java.sql.PreparedStatement;
import java.sql.SQLException;

class SimpleExecutor extends StatementExecutor {
  
  @Override
  public void executeStatement(PreparedStatement stmt) throws BatchException {
    try {
      addAffectedRows(stmt.executeUpdate());
    }
    catch(SQLException ex) {
      throw new BatchException("Al hacer una execución de actualizacion", ex);
    }
  }

}
