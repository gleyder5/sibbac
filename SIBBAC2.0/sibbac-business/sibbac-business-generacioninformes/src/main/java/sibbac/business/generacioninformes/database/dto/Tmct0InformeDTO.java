package sibbac.business.generacioninformes.database.dto;

import java.util.List;

import sibbac.business.wrappers.common.SelectDTO;


/**
 * DTO para la gestion de TMCT0_INFORMES
 * @author Neoris
 *
 */
public class Tmct0InformeDTO {
  
  /** idInforme - Identificador del informe  */ 
  private Integer idInforme;
  /** nbdescription - Nombre descriptivo del informe  */
  private String       nbdescription;
  /** nbidioma - Idioma del informe  */
  private String       nbidioma;
  /** totalizar - Indica si el informe tiene totales  */
  private String   totalizar;
  /** nbalias - Alias de este informe */
  private String       nbalias;
  /** cdisin - Isin del informe  */
  private String  cdisin;
  /** camposDetalle - Nombres separados por comas de todos los campos detalle que forman el informe  */
  private String camposDetalle;
  
  /** nbtxtlibre - Texto libre que ha insertado el usuario  */
  private String nbtxtlibre;
  
  private String filtrosAdicionales;
  
  
  /** listaCamposDetalle - Lista de campos que forman el detalle  */
  List<SelectDTO> listaCamposDetalle;
  /** listaCamposAgrupacion - Lista de campos que forman la agrupacion de la consulta  */
  List<SelectDTO> listaCamposAgrupacion;
  /** listaCamposOrdenacion - Lista de campos por los que se ordena la consulta  */
  List<SelectDTO> listaCamposOrdenacion;
  /** listaCamposCabecera - Lista de campos que forman la cabecera del listado Excel */
  List<SelectDTO> listaCamposCabecera;
  /** listaCamposSeparacion - Lista de campos por los que se separa el listado Excel en varias hojas.  */
  List<SelectDTO> listaCamposSeparacion;
  
  /** listaAlias - Lista de alias seleccionados en esta plantilla.  */
  List<SelectDTO> listaAlias;
  
  /** listaAlias - Lista de isin seleccionados en esta plantilla.  */
  List<SelectDTO> listaCdisin;

  
  public List<SelectDTO> getListaCdisin() {
    return listaCdisin;
  }

  public void setListaCdisin(List<SelectDTO> listaCdisin) {
    this.listaCdisin = listaCdisin;
  }

  public List<SelectDTO> getListaAlias() {
    return listaAlias;
  }

  public void setListaAlias(List<SelectDTO> listaAlias) {
    this.listaAlias = listaAlias;
  }

  private boolean selected;
  
  public Tmct0InformeDTO () {
    
  }
  
  public Tmct0InformeDTO(Integer idInforme,
                         String nbdescription,
                         String nbidioma,
                         String totalizar,
                         String nbalias,
                         String cdisin,
                         String camposDetalle,
                         String nbtxtlibre,
                         String filtrosAdicionales,
                         List<SelectDTO> listaCamposDetalle,
                         List<SelectDTO> listaCamposAgrupacion,
                         List<SelectDTO> listaCamposOrdenacion,
                         List<SelectDTO> listaCamposCabecera,
                         List<SelectDTO> listaCamposSeparacion,
                         boolean selected) {
    this.idInforme = idInforme;
    this.nbdescription = nbdescription;
    this.nbidioma = nbidioma;
    this.totalizar = totalizar;
    this.nbalias = nbalias;
    this.cdisin = cdisin;
    this.camposDetalle = camposDetalle;
    this.nbtxtlibre = nbtxtlibre;
    this.filtrosAdicionales = filtrosAdicionales;
    this.listaCamposDetalle = listaCamposDetalle;
    this.listaCamposAgrupacion = listaCamposAgrupacion;
    this.listaCamposOrdenacion = listaCamposOrdenacion;
    this.listaCamposCabecera = listaCamposCabecera;
    this.listaCamposSeparacion = listaCamposSeparacion;
    this.selected = selected;
  }
  
  
  public Integer getIdInforme() {
    return idInforme;
  }
  public void setIdInforme(Integer idInforme) {
    this.idInforme = idInforme;
  }
  public String getNbdescription() {
    return nbdescription;
  }
  public void setNbdescription(String nbdescription) {
    this.nbdescription = nbdescription;
  }
  public String getNbidioma() {
    return nbidioma;
  }
  public void setNbidioma(String nbidioma) {
    this.nbidioma = nbidioma;
  }
  public String getTotalizar() {
    return totalizar;
  }
  public void setTotalizar(String totalizar) {
    this.totalizar = totalizar;
  }
  public String getNbalias() {
    return nbalias;
  }
  public void setNbalias(String nbalias) {
    this.nbalias = nbalias;
  }
  public String getCdisin() {
    return cdisin;
  }
  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }
  public String getCamposDetalle() {
    return camposDetalle;
  }
  
  public String getNbtxtlibre() {
    return nbtxtlibre;
  }

  public void setNbtxtlibre(String nbtxtlibre) {
    this.nbtxtlibre = nbtxtlibre;
  }

  public void setCamposDetalle(String camposDetalle) {
    this.camposDetalle = camposDetalle;
  }
  public List<SelectDTO> getListaCamposDetalle() {
    return listaCamposDetalle;
  }
  public void setListaCamposDetalle(List<SelectDTO> listaCamposDetalle) {
    this.listaCamposDetalle = listaCamposDetalle;
  }
  public List<SelectDTO> getListaCamposAgrupacion() {
    return listaCamposAgrupacion;
  }
  public void setListaCamposAgrupacion(List<SelectDTO> listaCamposAgrupacion) {
    this.listaCamposAgrupacion = listaCamposAgrupacion;
  }
  public List<SelectDTO> getListaCamposOrdenacion() {
    return listaCamposOrdenacion;
  }
  public void setListaCamposOrdenacion(List<SelectDTO> listaCamposOrdenacion) {
    this.listaCamposOrdenacion = listaCamposOrdenacion;
  }
  public List<SelectDTO> getListaCamposCabecera() {
    return listaCamposCabecera;
  }
  public void setListaCamposCabecera(List<SelectDTO> listaCamposCabecera) {
    this.listaCamposCabecera = listaCamposCabecera;
  }
  public List<SelectDTO> getListaCamposSeparacion() {
    return listaCamposSeparacion;
  }
  public void setListaCamposSeparacion(List<SelectDTO> listaCamposSeparacion) {
    this.listaCamposSeparacion = listaCamposSeparacion;
  }

  public boolean isSelected() {
	return selected;
  }

  public void setSelected(boolean selected) {
	this.selected = selected;
  }

	public String getFiltrosAdicionales() {
		return filtrosAdicionales;
	}
	
	public void setFiltrosAdicionales(String filtrosAdicionales) {
		this.filtrosAdicionales = filtrosAdicionales;
	}
	 
  
    
  
}
