package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosId;

@Repository
public interface Tmct0InformesDatosDao  extends JpaRepository< Tmct0InformesDatos,  Tmct0InformesDatosId > {

  @Query("SELECT o FROM Tmct0InformesDatos o WHERE o.tmct0InformesDatosId.informe =:idInforme  ")
  public List <Tmct0InformesDatos> getDatosInformeByIdInforme(@Param("idInforme") Tmct0Informes idInforme);
  
  @Query("SELECT o FROM Tmct0InformesDatos o WHERE o.tmct0InformesDatosId.informe.id=:id AND o.tmct0InformesDatosId.tipoCampo.idTipo=0 ORDER BY o.nuposici ASC")
  public List <Tmct0InformesDatos> getDatosInformeCamposDetalle(@Param("id") Integer id);
  
  
  @Modifying @Query(value="DELETE FROM TMCT0_INFORMES_DATOS WHERE IDINFORME=:id",nativeQuery=true)
  public void deleteInformeDatos(@Param("id") int id);
  
  @Modifying @Query(value="DELETE FROM TMCT0_INFORMES_DATOS WHERE IDINFORME IN (:id)",nativeQuery=true)
  public void deleteVariosInformeDatos(@Param("id") List<Integer> id);
  
  @Query(value="SELECT ID1.NUPOSICI FROM TMCT0_INFORMES_DATOS ID1 WHERE ID1.IDCAMPO IN (SELECT ID2.IDCAMPO FROM TMCT0_INFORMES_DATOS ID2 WHERE ID2.IDTIPO = 5 AND ID2.IDINFORME = :id) AND ID1.IDTIPO = 0 AND ID1.IDINFORME = :id",nativeQuery=true)
  public List <Integer> getPosicionAgrupacionExcel(@Param("id") int id);
  
  
  /**
   *	Permite hallar la entidad de informes datos por
   *	@param idTipo idTipo
   *	@param nbcampo nbcampo
   *	@param nuposici nuposici
   *	@return Tmct0InformesDatos Tmct0InformesDatos 
   */
  @Query(value="SELECT infdxs FROM Tmct0InformesDatos infdxs WHERE infdxs.tmct0InformesDatosId.tipoCampo.idTipo = :idTipo "
  		+ "AND infdxs.tmct0InformesDatosId.campoInforme.nbcampo LIKE :nbcampo AND infdxs.nuposici = :nuposici AND infdxs.tmct0InformesDatosId.informe.id = :idInforme")
  public Tmct0InformesDatos findByTipoCampoAndNbCampoAndNuposiciAndInformeId(
		  @Param("idTipo") Integer idTipo, @Param("nbcampo") String nbcampo, @Param("nuposici") Integer nuposici, @Param("idInforme") Integer idInforme);
  
}


