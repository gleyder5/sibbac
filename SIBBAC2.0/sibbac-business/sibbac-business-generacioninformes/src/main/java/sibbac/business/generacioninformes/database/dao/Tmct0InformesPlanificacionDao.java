/**
 * 
 */
package sibbac.business.generacioninformes.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.common.SIBBACBusinessException;

/**
 * @author lucio.vilar
 *
 */
@Repository
public interface Tmct0InformesPlanificacionDao extends JpaRepository<Tmct0InformesPlanificacion, Integer> {
	
	@Query( "SELECT m FROM Tmct0InformesPlanificacion m WHERE m.estado = :estado" )
	public List<Tmct0InformesPlanificacion> findPlanificacionInformesByEstado(@Param("estado") char estado) throws SIBBACBusinessException;
	
	@Query( "SELECT m FROM Tmct0InformesPlanificacion m WHERE m.tmct0Informes.id = :idInforme" )
	public List<Tmct0InformesPlanificacion> findPlanificacionInformesByInforme(@Param("idInforme") Integer idInforme) throws SIBBACBusinessException;
	
	@Query( "SELECT DISTINCT m.nombreFichero FROM Tmct0InformesPlanificacion m where m.estado ='A'" )
	public List<String> buscarNombreFicherosPlanificacionInformes() throws SIBBACBusinessException;
	
	@Query( "SELECT DISTINCT m.destino FROM Tmct0InformesPlanificacion m where m.estado ='A'" )
	public List<String> buscarDestinosPlanificacionInformes() throws SIBBACBusinessException;
	
	@Transactional @Modifying @Query(value="UPDATE BSNBPSQL.TMCT0_INFORMES_PLANIFICACION SET FECHA_EJECUCION = :fechaEjecucion WHERE IDINFORME = :idInforme",nativeQuery=true)
	public void updateFechaEjecucionCurrentDate(@Param("idInforme") Integer idInforme, @Param("fechaEjecucion") Date fechaEjecucion);
	
}
