package sibbac.business.generacioninformes.database.bo;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.RollbackException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesClasesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosFiltrosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesFiltrosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesRelacionesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesTablasDao;
import sibbac.business.generacioninformes.database.dto.ComponenteDirectivaDTO;
import sibbac.business.generacioninformes.database.dto.SelectDetalleDTO;
import sibbac.business.generacioninformes.database.dto.Tmc0InformeCeldaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeColumnaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTONEW;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssemblerNEW;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesClases;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosFiltros;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosId;
import sibbac.business.generacioninformes.database.model.Tmct0InformesFiltros;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.generacioninformes.database.model.Tmct0InformesRelaciones;
import sibbac.business.generacioninformes.database.model.Tmct0InformesTablas;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.OrigenInformeExcelEnum;
import sibbac.business.generacioninformes.helper.GeneracionInformesHelper;
import sibbac.business.generacioninformes.rest.SIBBACServiceGenerarInformesNEW;
import sibbac.business.generacioninformes.rest.SIBBACServicePlantillasInformesNEW;
import sibbac.business.generacioninformes.rest.SIBBACServicePlantillasInformesNEW.EnuMercado;
import sibbac.business.generacioninformes.tasks.InformesBatchProcessor;
import sibbac.business.generacioninformes.threads.ProcesaInformeDiaRunnableNEW;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.TextosIdiomasBo;
import sibbac.business.wrappers.database.dao.ContactoDao;
import sibbac.business.wrappers.database.dao.IdiomaDao;
import sibbac.business.wrappers.database.dao.TextoDao;
import sibbac.business.wrappers.database.dao.TextosIdiomasDao;
import sibbac.business.wrappers.database.dao.Tmct0GeneracionInformeDAOImpl;
import sibbac.business.wrappers.database.dto.InfoMailContactosDTO;
import sibbac.business.wrappers.database.dto.assembler.Tmct0ContactoAssembler;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.business.wrappers.database.model.TextosIdiomas;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;

import com.google.code.externalsorting.ExternalSort;

@Service
public class Tmct0InformesBoNEW extends AbstractBo<Tmct0Informes, Integer, Tmct0InformesDao> {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesBoNEW.class);
  private static final String FORMATO_FECHA_ES = "dd/MM/yyyy";

  /**
   * Valor recuperado del propertis con el numero de hilos que se ejecutaran a
   * la vez
   */
  @Value("${generacioninformes.numThreadsPool}")
  private Integer numThreadsPool;

  /**
   * Lista que almacena los hilos.
   */
  final List<ProcesaInformeDiaRunnableNEW> misTthreads = new ArrayList<ProcesaInformeDiaRunnableNEW>();

  @Value("${mail.source.folder}")
  private String TEMPLATES_FOLDER;

  @Value("${generacioninformes.alias.routing}")
  private String ROUTING;

  @Value("${generacioninformes.alias.barrido}")
  private String BARRIDO;

  @Value("${generacioninformes.limite.registros.excel:50000}")
  // Se pone un valor por defecto.
  private int limiteRegistrosExcel;

  @Value("${generacioninformes.limite.excel.consulta:2000}")
  // Se pone un valor por defecto
  private int limiteRegistrosExcelConsulta;

  @Value("${generacioninformes.batch.mail.subject.spa}")
  private String toMailSubjectBatchEsp;

  @Value("${generacioninformes.batch.mail.body.spa}")
  private String toMailBodyEsp;

  @Value("${generacioninformes.batch.mail.subject.eng}")
  private String toMailSubjectBatchEng;

  @Value("${generacioninformes.batch.mail.body.eng}")
  private String toMailBodyEng;

  @Value("${generacioninformes.timeout}")
  private String timeout;

  @Value("${generacioninformes.ruta.informes}")
  private String rutaInformes;

  @Value("${generacioninformes.url.descarga.informe}")
  private String urlInforme;

  @Value("${generacioninformes.limite.registros.csv}")
  private int limiteExcelCsv;

  @Value("${generacioninformes.clases.routing}")
  private String CLASESROUTING;

  @PersistenceContext
  private EntityManager em;

  @Autowired
  Tmct0InformesDao tmct0InformesDao;

  @Autowired
  Tmct0InformesCamposDao tmct0InformesCamposDao;

  @Autowired
  Tmct0InformesRelacionesDao tmct0InformesRelacionesDao;

  @Autowired
  Tmct0InformeAssemblerNEW tmct0InformeAssembler;

  @Autowired
  Tmct0GeneracionInformeDAOImpl generacionInformeDao;

  @Autowired
  Tmct0InformesDatosDao tmct0InformesDatosDao;

  @Autowired
  Tmct0InformesClasesDao tmct0InformesClasesDao;

  @Autowired
  private Tmct0InformesFiltrosDao tmct0InformesFiltrosDao;

  @Autowired
  private Tmct0InformesTablasDao tmct0InformesTablasDao;

  @Autowired
  private Tmct0InformesDatosFiltrosDao tmct0InformesDatosFiltrosDao;

  @Autowired
  private Tmct0InformeAssemblerNEW tmct0InformeAssemblerNEW;

  @Autowired
  ContactoDao contactoDao;

  @Autowired
  private SendMail sendMail;

  @Autowired
  Tmct0UsersBo tmct0UsersBo;

  @Autowired
  private Tmct0ContactoAssembler tmct0ContactoAssembler;

  @Autowired
  private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;

  @Autowired
  private TextoDao textoDao;

  @Autowired
  private TextosIdiomasDao textosIdiomasDao;

  @Autowired
  private IdiomaDao idiomaDao;

  @Autowired
  TextosIdiomasBo textosIdiomasBo;

  @Autowired
  ApplicationContext ctx;

  private SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");

  /**
   * 
   * 
   * Graba los datos de la plantilla (Nueva o Existente) a partir de los datos
   * del dto recibido. El metodo obtiene los campos query.
   * 
   * @param dto - con los datos de la plantilla a crear o modificar
   * @param idClaseInformePadre idClaseInformePadre
   * @param esHijo esHijo
   * @param altaModif altaModif
   * @throws SIBBACBusinessException
   */
  @Transactional
  public Integer grabarPlantillaInforme(Tmct0InformeDTONEW dto, Integer idClaseInformePadre, boolean esHijo,
      String altaModif) throws SIBBACBusinessException {

    Tmct0Informes miInforme = new Tmct0Informes();

    try {
      // COMPROBACIONES INICIALES
      // MFG 16/06/2016 Se ha incluido un nuevo campo de texto libre para añadir
      // condiciones al where. Se realizan las
      // comprobaciones previas:
      if (dto.getNbtxtlibre() != null && dto.getNbtxtlibre() != "") {
        String sCadena = dto.getNbtxtlibre().trim().toUpperCase();
        if ((sCadena.indexOf("AND") != 0) || (sCadena.length() <= 3)) {
          throw new SIBBACBusinessException(
              "El texto libre tiene que comenzar con AND y contener un criterio de filtrado");
        }
      }

      // Comprobacion de error en la formación de los campos de agrupación
      // Si el informe tiene que agrupar se tiene que controlar: -> todos los
      // campos detalle que No totalizan tienen que
      // estar incluida en ella sino la consulta devolveria un error.

      if (dto.getListaCamposAgrupacion().size() > 0) {
        for (SelectDetalleDTO campoDetalleAux : dto.getListaCamposDetalle()) {
          Tmct0InformesCampos datosCampoDetalle = tmct0InformesCamposDao.findOne(Integer.parseInt(campoDetalleAux
              .getKey()));
          // tienen que estar incluidos los que tengan el campos totalizar = N
          // ya que los otros los convierto en un SUM
          if ((datosCampoDetalle.getTotalizar() == 'N')
              && (!existeCampoDetalleEnLista(dto.getListaCamposAgrupacion(), campoDetalleAux))) {
            throw new SIBBACBusinessException(
                "Hay campos seleccionados para un informe agrupado que no se pueden sumarizar, por favor, inclúyalos como criterio de agrupación o elimínelos del informe");
          }
        }
      }

      String sEspaniol = SIBBACServicePlantillasInformesNEW.EnuIdioma.ESPAÑOL.name();

      StringBuffer sQuery1 = new StringBuffer("");
      StringBuffer sQuery2 = new StringBuffer("");

      StringBuffer sCampos = new StringBuffer(" SELECT ");
      StringBuffer sFrom = new StringBuffer(" FROM ");
      StringBuffer sInnerJoin = new StringBuffer(" ");

      StringBuffer sOrderby = new StringBuffer("");

      int iNumCampos = 0;

      List<String> lTablas = new ArrayList<String>();

      // ubico la tabla origen
      Tmct0InformesClases clase = tmct0InformesClasesDao.findOne(dto.getIdClase());
      if (null != clase) {
        Tmct0InformesRelaciones relacion = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(clase.getTablaBase(),
            clase.getClase(), clase.getMercado());
        if (null != relacion) {
          sFrom.append(relacion.getNbtabla1()).append(" ").append(clase.getTablaBase());
          lTablas.add(clase.getTablaBase());
        }
      }

      // Obtenemos el listado de mercados asociados a la clase
      List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(clase.getClase());

      // Recorremos la lista de mercados de la clase para saber que posicion
      // ocupa el especifico de esta clase
      int posicionMercado = 0;
      for (int i = 0; i < mercadosClase.size(); i++) {
        if (mercadosClase.get(i).equals(clase.getMercado())) {
          posicionMercado = i;
          break;
        }
      }

      for (SelectDetalleDTO tmct0InformeDatoDTO : dto.getListaCamposDetalle()) {
        Tmct0InformesCampos campoDetalle = tmct0InformesCamposDao
            .findOne(Integer.parseInt(tmct0InformeDatoDTO.getKey()));

        String sTablaOrigen = "";
        String sTablaDestinoRelacion = "";
        Tmct0InformesRelaciones relacionCampo = new Tmct0InformesRelaciones();
        boolean campoDetalleExiste = true;
        String cdTablaOriginalCampoDetalle = "";
        // Si el mercado del campoDetalle es vacio significa que su cdTabla
        // contendra ///
        if (campoDetalle.getMercado().equals("")) {
          String[] listaCdtabla = campoDetalle.getCdTabla().split("/", -1);
          String cdtabla = listaCdtabla[posicionMercado];
          cdTablaOriginalCampoDetalle = cdtabla;
          // Si el cdtabla no es vacio continuamos
          if (!cdtabla.equals("")) {
            relacionCampo = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(cdtabla, clase.getClase(),
                clase.getMercado());
            if (cdtabla.equals(clase.getTablaBase())) {
              sTablaOrigen = cdtabla;
            }
            else if (null != relacionCampo) {
              sTablaOrigen = relacionCampo.getCdtabla1();
              sTablaDestinoRelacion = relacionCampo.getCdtabla2();
            }
            else {
              campoDetalleExiste = false;
            }

          }
          else {
            campoDetalleExiste = false;
          }
        }
        else {
          relacionCampo = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(campoDetalle.getCdTabla(),
              clase.getClase(), clase.getMercado());
          if (campoDetalle.getCdTabla().equals(clase.getTablaBase())) {
            sTablaOrigen = campoDetalle.getCdTabla();
          }
          else if (null != relacionCampo) {
            sTablaOrigen = relacionCampo.getCdtabla1();
            sTablaDestinoRelacion = relacionCampo.getCdtabla2();
          }
          else {
            campoDetalleExiste = false;
          }
          cdTablaOriginalCampoDetalle = campoDetalle.getCdTabla();
        }

        if (campoDetalle.getClase().equals(clase.getClase()) && campoDetalleExiste
            && (campoDetalle.getMercado().equals(clase.getMercado()) || campoDetalle.getMercado().equals(""))) {

          StringBuffer sRelacion = new StringBuffer(" ");

          if (!lTablas.contains(sTablaOrigen)) {

            // Si el campo esta en una de las tablas que todavia no esta
            // incluida,
            // la incluimos al from y la relación en el where

            sRelacion.insert(0, " " + relacionCampo.getNbrelacion()).append(" ");
            lTablas.add(sTablaOrigen);

            // Siempre que se añade una tabla en la consulta, hay que asegurar
            // que se tienen todas las realaciones para acceder a la información
            // del campo.
            // Se comprueba que la tabla destino de la relación de la nueva
            // tabla ya esta incluida sino se siguen
            // incluyendo las tablas y las relaciones hasta que tengamos una que
            // si esta incluida.
            boolean bExiste = false;

            do {

              if (!lTablas.contains(sTablaDestinoRelacion)) {
                // Coger la relacion de la tabla destino
                Tmct0InformesRelaciones relacionTablaDestino = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(
                    sTablaDestinoRelacion, clase.getClase(), clase.getMercado());

                if (relacionTablaDestino == null) {
                  LOG.error("No se han podido determinar todas las relaciones de la consulta - No se han encontrado la relacion para la tabla  "
                      + sTablaDestinoRelacion);

                  throw new SIBBACBusinessException(
                      "Error generico formando los datos del informe - Consulte con los responsables de informatica");
                }

                sTablaOrigen = relacionTablaDestino.getCdtabla1();
                sTablaDestinoRelacion = relacionTablaDestino.getCdtabla2();
                sRelacion.insert(0, " " + relacionTablaDestino.getNbrelacion());
                lTablas.add(sTablaOrigen);

              }
              else {
                bExiste = true;
              }

            }
            while (!bExiste);

            // Se concatena las relaciones a la variable general
            sInnerJoin.append(sRelacion);

          }
        }

        if (campoDetalleExiste
            && (lTablas.contains(cdTablaOriginalCampoDetalle) || campoDetalle.getMercado().equals(""))) {
          // Se forma el campo el cual depende si el informe es en español o en
          // ingles.
          StringBuffer sCampo = new StringBuffer("");

          // Si el mercado es "" significa que los campos de conversion pueden
          // ser multiples
          String conversion = "";
          String conversionIngles = "";

          if (campoDetalle.getNbconversion().contains(" / ")) {
            String[] listaConversion = campoDetalle.getNbconversion().split(" / ", -1);
            String[] listaConversionIngles = campoDetalle.getNbconvingles().split(" / ", -1);
            conversion = listaConversion[posicionMercado];
            conversionIngles = listaConversionIngles[posicionMercado];
            ;
          }
          else {
            conversion = campoDetalle.getNbconversion();
            conversionIngles = campoDetalle.getNbconvingles();
          }

          if (dto.getNbidioma().equals(sEspaniol)) {
            if (null != tmct0InformeDatoDTO.getTraduccion() && !"".equals(tmct0InformeDatoDTO.getTraduccion().trim())) {
              sCampo.append(tmct0InformeDatoDTO.getTraduccion()).append(" AS ").append(campoDetalle.getNbcolumna());
            }
            else {
              sCampo.append(conversion).append(" AS ").append(campoDetalle.getNbcolumna());
            }
          }
          else {
            if (null != tmct0InformeDatoDTO.getTraduccion() && !"".equals(tmct0InformeDatoDTO.getTraduccion().trim())) {
              sCampo.append(tmct0InformeDatoDTO.getTraduccion()).append(" AS ").append(campoDetalle.getNbcolingles());
            }
            else {
              sCampo.append(conversionIngles).append(" AS ").append(campoDetalle.getNbcolingles());
            }
          }

          if (iNumCampos == 0) {
            sCampos.append(sCampo.toString());
          }
          else {
            sCampos.append(" , ").append(sCampo.toString());
          }

        }
        else {
          if (iNumCampos == 0) {
            sCampos.append("NULL").append(" AS ").append(campoDetalle.getNbcolumna());
            ;
          }
          else {
            sCampos.append(" , ").append("NULL").append(" AS ").append(campoDetalle.getNbcolumna());
            ;
          }
        }
        iNumCampos++;
      }

      sQuery1 = sQuery1.append(sCampos.toString()).append(sFrom.toString()).append(sInnerJoin);

      // Formamos los campos de ordenacion
      iNumCampos = 0;

      for (SelectDTO tmct0InformeDatoDTO : dto.getListaCamposOrdenacion()) {
        // Se forma el campo el cual depende si el informe es en español o en
        // ingles.
        StringBuffer sCampo = new StringBuffer("");

        Tmct0InformesCampos campoOrdenacion = tmct0InformesCamposDao.findOne(Integer.parseInt(tmct0InformeDatoDTO
            .getKey()));
        if (dto.getNbidioma().equals(sEspaniol)) {
          sCampo.append(campoOrdenacion.getNbcolumna());
        }
        else {
          sCampo.append(campoOrdenacion.getNbcolingles());
        }

        if (iNumCampos == 0) {
          sOrderby.append(" ORDER BY ").append(sCampo.toString());
        }
        else {
          sOrderby.append(" , ").append(sCampo.toString());
        }

        iNumCampos++;
      }

      miInforme = tmct0InformeAssembler.getTmct0InformeEntidad(dto);

      miInforme.setNbquery1(sQuery1.toString());

      miInforme.setNbquery2(sQuery2.toString());

      List<Tmct0InformesTablas> listaTablasInformes = new ArrayList<Tmct0InformesTablas>();

      if (null != miInforme.getId()) {
        for (Tmct0InformesTablas tmct0InformesTabla : miInforme.getTmct0InformesTablas()) {
          tmct0InformesTablasDao.delete(tmct0InformesTabla.getId());
        }
      }

      for (String tabla : lTablas) {
        Tmct0InformesTablas tmct0InformesTablas = new Tmct0InformesTablas();
        tmct0InformesTablas.setTmct0Informes(miInforme);
        tmct0InformesTablas.setTabla(tabla);
        listaTablasInformes.add(tmct0InformesTablas);
      }

      miInforme.setTmct0InformesTablas(listaTablasInformes);

      // Guardamos la plantilla
      tmct0InformesDao.save(miInforme);

      /** Inicio - Componente filtros dinamicos. */
      if (!esHijo) {
        this.saveInfoFiltrosDinamicos(miInforme, dto);
      }
      /** Fin - Componente filtros dinamicos. */

      LOG.debug("Plantilla grabada correctamente");

      /** Inicio - Generacion de registros de TEXTO para obtener datos mail. */
      // TODO - Verificar funcionamiento - Ver como funciona la clonacion.
      if (miInforme != null) {
        switch (altaModif) {
          case Constantes.ALTA:
            List<Texto> textos = this.textoDao.save(this.getPopulatedListForCreateTextoByInforme(miInforme));
            this.textoDao.flush();
            // En el alta se generan los registros de TextosIdiomas necesarios.
            if (CollectionUtils.isNotEmpty(textos)) {
              this.textosIdiomasDao.save(this.getListaObjetosIdioma(textos));
            }
            break;
          case Constantes.MODIFICACION:
            // Modificacion de subject.
            this.textoDao.updateDescripByTipo(Constantes.PREFIX_DESCRIPCIONES + miInforme.getNbdescription()
                + Constantes.SUFFIX_DESCRIPCIONES_SUBJECT, Constantes.PREFIX_TIPO + miInforme.getId()
                + Constantes.SUFFIX_TIPO_SUBJECT);
            // Modificacion de body.
            this.textoDao.updateDescripByTipo(Constantes.PREFIX_DESCRIPCIONES + miInforme.getNbdescription()
                + Constantes.SUFFIX_DESCRIPCIONES_BODY, Constantes.PREFIX_TIPO + miInforme.getId()
                + Constantes.SUFFIX_TIPO_BODY);
            break;
          default:
            break;
        }
        ;
      }
      /** Fin - Generacion de registros de TEXTO para obtener datos mail. */

    }
    catch (RollbackException ex) {
      LOG.error(ex.getLocalizedMessage(), ex);
      return 0;
    }
    return miInforme.getId();
  }

  /**
   * Aqui se obtiene la lista de objetos idioma relacionadas a objetos Texto.
   * @param textos
   * @return List<TextosIdiomas>
   * @throws SIBBACBusinessException
   */
  @Transactional
  private List<TextosIdiomas> getListaObjetosIdioma(List<Texto> textos) throws SIBBACBusinessException {
    List<TextosIdiomas> listTextosIdiomas = new ArrayList<TextosIdiomas>();

    if (textos.size() > 0) {
      /****************************** SUBJECT ESPAÑOL *********************************/
      TextosIdiomas txtIdioSubjectEsp = getTextosIdiomasInicial();
      txtIdioSubjectEsp.setTexto(textos.get(0));
      txtIdioSubjectEsp.setIdioma(this.idiomaDao.findByDescripcion(SIBBACServicePlantillasInformesNEW.EnuIdioma.ESPAÑOL
          .name()));
      // Idiomas Parametricamente: id = 2 -> Espaniol, id = 3 -> Ingles
      txtIdioSubjectEsp.setDescripcion(toMailSubjectBatchEsp);
      listTextosIdiomas.add(txtIdioSubjectEsp);

      /****************************** SUBJECT INGLES *********************************/
      TextosIdiomas txtIdioSubjectEng = getTextosIdiomasInicial();
      txtIdioSubjectEng.setTexto(textos.get(0));
      txtIdioSubjectEng.setIdioma(this.idiomaDao.findByDescripcion(SIBBACServicePlantillasInformesNEW.EnuIdioma.INGLES
          .name()));
      // Idiomas Parametricamente: id = 2 -> Espaniol, id = 3 -> Ingles
      txtIdioSubjectEng.setDescripcion(toMailSubjectBatchEng);
      listTextosIdiomas.add(txtIdioSubjectEng);
    }

    if (textos.size() > 1) {
      /****************************** BODY ESPAÑOL *********************************/
      TextosIdiomas txtIdioBodyEsp = getTextosIdiomasInicial();
      txtIdioBodyEsp.setTexto(textos.get(1));
      txtIdioBodyEsp.setIdioma(this.idiomaDao.findByDescripcion(SIBBACServicePlantillasInformesNEW.EnuIdioma.ESPAÑOL
          .name()));
      // Idiomas Parametricamente: id = 2 -> Espaniol, id = 3 -> Ingles
      txtIdioBodyEsp.setDescripcion(toMailBodyEsp);
      listTextosIdiomas.add(txtIdioBodyEsp);

      /****************************** BODY INGLES *********************************/
      TextosIdiomas txtIdioBodyEng = getTextosIdiomasInicial();
      txtIdioBodyEng.setTexto(textos.get(1));
      txtIdioBodyEng.setIdioma(this.idiomaDao.findByDescripcion(SIBBACServicePlantillasInformesNEW.EnuIdioma.INGLES
          .name()));
      // Idiomas Parametricamente: id = 2 -> Espaniol, id = 3 -> Ingles
      txtIdioBodyEng.setDescripcion(toMailBodyEng);
      listTextosIdiomas.add(txtIdioBodyEng);
    }
    return listTextosIdiomas;
  }

  /**
   * Se obtiene precargado un objeto TextoIdiomas inicial.
   * @param txt
   * @return txtIdio
   */
  private TextosIdiomas getTextosIdiomasInicial() {
    TextosIdiomas txtIdio = new TextosIdiomas();
    txtIdio.setAuditDate(new Date());
    txtIdio.setAuditUser(Constantes.USUARIO_SIBBAC);
    txtIdio.setVersion(Constantes.VERSION);
    return txtIdio;
  }

  /**
   * Generacion de registros de Texto populado con id de informe - ALTA.
   * @param miInforme miInforme
   * @return List<Texto> List<Texto>
   * @throws SIBBACBusinessException SIBBACBusinessException
   */
  private List<Texto> getPopulatedListForCreateTextoByInforme(Tmct0Informes miInforme) throws SIBBACBusinessException {

    // TODO - Verificar funcionamiento

    List<Texto> listaTexto = new ArrayList<Texto>();
    // IX de Subject de mail
    Texto textoSubject = new Texto();
    textoSubject.setAuditUser(Constantes.USUARIO_SIBBAC);
    textoSubject.setAuditDate(new Date());
    textoSubject.setVersion(Constantes.VERSION);
    textoSubject.setTipo(Constantes.PREFIX_TIPO + miInforme.getId() + Constantes.SUFFIX_TIPO_SUBJECT);
    textoSubject.setDescripcion(Constantes.PREFIX_DESCRIPCIONES + miInforme.getNbdescription()
        + Constantes.SUFFIX_DESCRIPCIONES_SUBJECT);
    listaTexto.add(textoSubject);

    // IX de body de mail
    Texto textoBody = new Texto();
    textoBody.setAuditUser(Constantes.USUARIO_SIBBAC);
    textoBody.setAuditDate(new Date());
    textoBody.setVersion(Constantes.VERSION);
    textoBody.setTipo(Constantes.PREFIX_TIPO + miInforme.getId() + Constantes.SUFFIX_TIPO_BODY);
    textoBody.setDescripcion(Constantes.PREFIX_DESCRIPCIONES + miInforme.getNbdescription()
        + Constantes.SUFFIX_DESCRIPCIONES_BODY);
    listaTexto.add(textoBody);
    return listaTexto;
  }

  private boolean existeCampoDetalleEnLista(List<SelectDTO> listaCamposAgrupacion, SelectDetalleDTO campoDetalleAux) {
    boolean result = false;

    if (null != listaCamposAgrupacion) {
      for (SelectDTO selectDTO : listaCamposAgrupacion) {
        if (selectDTO.getKey().equalsIgnoreCase(campoDetalleAux.getKey())) {
          result = true;
          break;
        }
      }
    }

    return result;
  }

  /** Inicio - Componente filtros dinamicos. */
  /**
   * Se persisten datos de los filtros dinamicos generados.
   * @param miInforme: Informes
   * @param dto: DTO de informes
   * @throws SIBBACBusinessException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  private void saveInfoFiltrosDinamicos(Tmct0Informes miInforme, Tmct0InformeDTONEW dto) throws SIBBACBusinessException {
    List<SelectDTO> selectDTOs = new ArrayList<SelectDTO>();
    String valoresConcat = "";
    if (CollectionUtils.isNotEmpty(dto.getListComponenteDirectivaDTO())) {
      List<Tmct0InformesDatosFiltros> listTmct0InformesDatosFiltros = new ArrayList<Tmct0InformesDatosFiltros>();
      for (List<ComponenteDirectivaDTO> compDirDTOList : dto.getListComponenteDirectivaDTO()) {
        for (ComponenteDirectivaDTO compDirDTO : compDirDTOList) {
          boolean isNoVacioValorCampo = (compDirDTO.getValorCampo() != null && !"".equals(compDirDTO.getValorCampo())
              || (compDirDTO.getValorNumeroFechaDesde() != null && !"".equals(compDirDTO.getValorNumeroFechaDesde())) || (compDirDTO
              .getValorNumeroFechaHasta() != null && !"".equals(compDirDTO.getValorNumeroFechaHasta())))
              && ("NUMERO".equals(compDirDTO.getTipo()) || "FECHA".equals(compDirDTO.getTipo()) || "COMBO"
                  .equals(compDirDTO.getTipo()));
          boolean isNoVacioListSeleccSelect = !compDirDTO.getListaSeleccSelectDTO().isEmpty()
              && ("AUTORRELLENABLE".equals(compDirDTO.getTipo()) || "LISTA".equals(compDirDTO.getTipo()) || "TEXTO"
                  .equals(compDirDTO.getTipo()));
          if (isNoVacioValorCampo || isNoVacioListSeleccSelect) {
            Tmct0InformesFiltros tmct0InformesFiltros = this.tmct0InformesFiltrosDao.findOne(compDirDTO.getId());
            if (tmct0InformesFiltros != null) {
              Tmct0InformesDatosFiltros tmct0InformesDatosFiltros = new Tmct0InformesDatosFiltros();
              tmct0InformesDatosFiltros.setTmct0Informes(miInforme);
              tmct0InformesDatosFiltros.setTmct0InformesFiltros(tmct0InformesFiltros);

              switch (compDirDTO.getTipo()) {
                case "FECHA":
                  informarFecha(compDirDTO, tmct0InformesDatosFiltros);
                  break;
                case "NUMERO":
                  if (compDirDTO.getSubTipo().equals("DESDE-HASTA")) {
                    valoresConcat = compDirDTO.getValorCampo() + " , " + compDirDTO.getValorCampoHasta();
                    tmct0InformesDatosFiltros.setValorFiltro(valoresConcat);
                  }
                  else {
                    tmct0InformesDatosFiltros.setValorFiltro(compDirDTO.getValorCampo());
                  }
                  break;
                case "LISTA":
                  selectDTOs = compDirDTO.getListaSeleccSelectDTO();
                  valoresConcat = concatenarValores(selectDTOs);

                  tmct0InformesDatosFiltros.setValorFiltro(valoresConcat);

                  break;
                case "AUTORRELLENABLE":
                  selectDTOs = compDirDTO.getListaSeleccSelectDTO();
                  valoresConcat = concatenarValores(selectDTOs);

                  tmct0InformesDatosFiltros.setValorFiltro(valoresConcat);

                  break;
                case "COMBO":
                  tmct0InformesDatosFiltros.setValorFiltro(compDirDTO.getValorCampo());
                  break;
                case "TEXTO":
                  selectDTOs = compDirDTO.getListaSeleccSelectDTO();
                  valoresConcat = concatenarValores(selectDTOs);

                  tmct0InformesDatosFiltros.setValorFiltro(valoresConcat);
                  tmct0InformesDatosFiltros.setTipoBusqueda(compDirDTO.getLblTipoBusqueda());
                  break;

                default:
                  break;
              }

              listTmct0InformesDatosFiltros.add(tmct0InformesDatosFiltros);
            }
          }
        }
      }
      if (CollectionUtils.isNotEmpty(listTmct0InformesDatosFiltros)) {
        this.tmct0InformesDatosFiltrosDao.save(listTmct0InformesDatosFiltros);
      }
    }
  }

  /**
   * Asiganamos el valor de la fecha comprobando sí, es informada a traves del
   * datapicker o el campo númerico.
   * 
   * */
  private void informarFecha(ComponenteDirectivaDTO compDirDTO, Tmct0InformesDatosFiltros tmct0InformesDatosFiltros) {

    String valoresConcat = "";
    if (compDirDTO.getSubTipo().equals("DESDE-HASTA")) {
      // comprobamos si la fecha introducida viene dada en numero (hoy + x)
      if (StringUtils.isNotBlank(compDirDTO.getValorNumeroFechaDesde())) {
        valoresConcat = (StringUtils.isNotBlank(compDirDTO.getValorNumeroFechaDesde()) ? (compDirDTO
            .getLblBtnSentidoFiltro() + compDirDTO.getValorNumeroFechaDesde()) : "")
            + " , "
            + (StringUtils.isNotBlank(compDirDTO.getValorNumeroFechaHasta()) ? (compDirDTO
                .getLblBtnSentidoFiltroHasta() + compDirDTO.getValorNumeroFechaHasta()) : "");
        tmct0InformesDatosFiltros.setValorFiltro(valoresConcat);
      }
      else {
        valoresConcat = compDirDTO.getValorCampo() + " , " + compDirDTO.getValorCampoHasta();
        tmct0InformesDatosFiltros.setValorFiltro(valoresConcat);
      }
    }
    else {
      if (null != compDirDTO.getValorCampo() && StringUtils.isNotBlank(compDirDTO.getValorCampo())) {
        tmct0InformesDatosFiltros.setValorFiltro(compDirDTO.getValorCampo());
      }
      else if (null != compDirDTO.getValorNumeroFechaDesde()
          && StringUtils.isNotBlank(compDirDTO.getValorNumeroFechaDesde())) {
        tmct0InformesDatosFiltros.setValorFiltro(compDirDTO.getValorNumeroFechaDesde());
      }
    }
  }

  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  private String concatenarValores(List<SelectDTO> selectDTOs) {
    String result = "";
    try {
      if (null != selectDTOs) {
        for (Object selectDTO : selectDTOs) {
          if (!result.equals("")) {
            result = result + " , ";
          }
          LinkedHashMap<String, String> mapa = (LinkedHashMap<String, String>) selectDTO;
          result = result + mapa.get("key").trim() + "||" + mapa.get("value").trim();
        }
      }
    }
    catch (Exception e) {
      LOG.error("Ocurrio un error en la concatenacion de valores de select", e);
    }
    return result;
  }

  /** Fin - Componente filtros dinamicos. */
  @Transactional
  public Tmct0Informes getInforme(Integer id, boolean esPlanificacion) {
    Tmct0Informes informess;
    informess = tmct0InformesDao.getOne(id);
    if (esPlanificacion) {
      informess.postLoad();
    }
    return informess;
  }

  private Boolean isConSeparacion(Tmct0Informes informe) {
    LOG.debug("ENTRA EN isConSeparacion");
    return !getColumnasSeparacion(informe).isEmpty();
  }

  private String formarQueryConUnion(Tmct0GeneracionInformeFilterDTO filterDto, Tmct0Informes informe,
      String queryUnion, boolean isLastQuery) throws SIBBACBusinessException {

    String query = "";
    String queryExc = "";

    // Obtenemos el listado de mercados asociados a la clase
    List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(informe.getTmct0InformesClases()
        .getClase());
    // Recorremos la lista de mercados de la clase para saber que posicion ocupa
    // el especifico de esta clase
    int posicionMercado = 0;
    for (int i = 0; i < mercadosClase.size(); i++) {
      if (mercadosClase.get(i).equals(informe.getTmct0InformesClases().getMercado())) {
        posicionMercado = i;
        break;
      }
    }

    StringBuffer sInnerJoin = new StringBuffer(" ");
    List<String> lTablas = tmct0InformesTablasDao.getTablasByIdInforme(informe.getId());

    String sTablaOrigen = "";
    String sTablaDestinoRelacion = "";
    Tmct0InformesRelaciones relacionCampo = new Tmct0InformesRelaciones();

    if (null != filterDto.getListComponenteDirectivaDTO() && filterDto.getListComponenteDirectivaDTO().size() > 0) {
      for (ComponenteDirectivaDTO componenteDinamico : filterDto.getListComponenteDirectivaDTO().get(0)) {
        if ((!componenteDinamico.getTipo().equals("FECHA")
            && (StringUtils.isNotBlank(componenteDinamico.getValorCampo()) || CollectionUtils
                .isNotEmpty(componenteDinamico.getListaSeleccSelectDTO())) || (componenteDinamico.getTipo().equals(
            "FECHA") && (StringUtils.isNotBlank(componenteDinamico.getValorNumeroFechaDesde()) || StringUtils
            .isNotBlank(componenteDinamico.getValorCampo()))))) {
          StringBuffer sRelacion = new StringBuffer(" ");
          String[] listaCdtabla = componenteDinamico.getTabla().split("/", -1);

          if (listaCdtabla.length > 1) {
            String cdtabla = listaCdtabla[posicionMercado];
            relacionCampo = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(cdtabla, informe
                .getTmct0InformesClases().getClase(), informe.getTmct0InformesClases().getMercado());
            if (cdtabla.equals(informe.getTmct0InformesClases().getTablaBase())) {
              sTablaOrigen = cdtabla;
            }
            else if (null != relacionCampo) {
              sTablaOrigen = relacionCampo.getCdtabla1();
              sTablaDestinoRelacion = relacionCampo.getCdtabla1();
            }
          }
          else {
            relacionCampo = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(componenteDinamico.getTabla(),
                informe.getTmct0InformesClases().getClase(), informe.getTmct0InformesClases().getMercado());
            if (componenteDinamico.getTabla().equals(informe.getTmct0InformesClases().getTablaBase())) {
              sTablaOrigen = componenteDinamico.getTabla();
            }
            else if (null != relacionCampo) {
              sTablaOrigen = relacionCampo.getCdtabla1();
              sTablaDestinoRelacion = relacionCampo.getCdtabla1();
            }
          }

          if (!lTablas.contains(sTablaOrigen)) {

            // Si el campo esta en una de las tablas que todavia no esta
            // incluida, la incluimos al from y la relación en
            // el where

            sRelacion.insert(0, " " + relacionCampo.getNbrelacion()).append(" ");
            lTablas.add(sTablaOrigen);

            // Siempre que se añade una tabla en la consulta, hay que asegurar
            // que se tienen todas las realaciones para
            // acceder a la información del campo.
            // Se comprueba que la tabla destino de la relación de la nueva
            // tabla ya esta incluida sino se siguen
            // incluyendo las tablas y las relaciones hasta que tengamos una que
            // si esta incluida.
            boolean bExiste = false;

            do {

              if (!lTablas.contains(sTablaDestinoRelacion)) {
                // Coger la relacion de la tabla destino
                Tmct0InformesRelaciones relacionTablaDestino = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(
                    sTablaDestinoRelacion, informe.getTmct0InformesClases().getClase(), informe
                        .getTmct0InformesClases().getMercado());

                if (relacionTablaDestino == null) {
                  LOG.error("No se han podido determinar todas las relaciones de la consulta - No se han encontrado la relacion para la tabla  "
                      + sTablaDestinoRelacion);

                  throw new SIBBACBusinessException(
                      "Error generico formando los datos del informe - Consulte con los responsables de informatica");
                }

                sTablaOrigen = relacionTablaDestino.getCdtabla1();
                sTablaDestinoRelacion = relacionTablaDestino.getCdtabla2();
                sRelacion.insert(0, " " + relacionTablaDestino.getNbrelacion());
                lTablas.add(sTablaOrigen);

              }
              else {
                bExiste = true;
              }

            }
            while (!bExiste);

            // Se concatena las relaciones a la variable general
            sInnerJoin.append(sRelacion);
          }
        }
      }
    }

    String opcion = filterDto.getOpcionesEspeciales();
    if (opcion != null) {
      if (opcion.equals("EXCLROUTING")) {
        StringBuffer sRelacion = new StringBuffer(" ");
        String sTablaDestinoRelacionExcRout = "BOK";
        // hasta que tengamos una que si esta incluida.
        boolean bExiste = false;

        do {

          if (!lTablas.contains(sTablaDestinoRelacionExcRout)) {
            // Coger la relacion de la tabla destino
            Tmct0InformesRelaciones relacionTablaDestino = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(
                sTablaDestinoRelacionExcRout, informe.getTmct0InformesClases().getClase(), informe
                    .getTmct0InformesClases().getMercado());

            if (relacionTablaDestino == null) {
              LOG.error("No se han podido determinar todas las relaciones de la consulta - No se han encontrado la relacion para la tabla  "
                  + sTablaDestinoRelacionExcRout);

              throw new SIBBACBusinessException(
                  "Error generico formando los datos del informe - Consulte con los responsables de informatica");
            }

            sTablaOrigen = relacionTablaDestino.getCdtabla1();
            sTablaDestinoRelacionExcRout = relacionTablaDestino.getCdtabla2();
            sRelacion.insert(0, " " + relacionTablaDestino.getNbrelacion());
            lTablas.add(sTablaOrigen);

          }
          else {
            bExiste = true;
          }

        }
        while (!bExiste);

        // Se concatena las relaciones a la variable general
        sInnerJoin.append(sRelacion);
        queryExc = " AND BOK.CDALIAS NOT IN (" + ROUTING + ") ";

      }
      else if (opcion.equals("EXCLBARRID")) {
        StringBuffer sRelacion = new StringBuffer(" ");
        String sTablaDestinoRelacionExcBarr = "BOK";
        // hasta que tengamos una que si esta incluida.
        boolean bExiste = false;

        do {

          if (!lTablas.contains(sTablaDestinoRelacionExcBarr)) {
            // Coger la relacion de la tabla destino
            Tmct0InformesRelaciones relacionTablaDestino = tmct0InformesRelacionesDao.findByCdTabla1ClaseMercado(
                sTablaDestinoRelacionExcBarr, informe.getTmct0InformesClases().getClase(), informe
                    .getTmct0InformesClases().getMercado());

            if (relacionTablaDestino == null) {
              LOG.error("No se han podido determinar todas las relaciones de la consulta - No se han encontrado la relacion para la tabla  "
                  + sTablaDestinoRelacionExcBarr);

              throw new SIBBACBusinessException(
                  "Error generico formando los datos del informe - Consulte con los responsables de informatica");
            }

            sTablaOrigen = relacionTablaDestino.getCdtabla1();
            sTablaDestinoRelacionExcBarr = relacionTablaDestino.getCdtabla2();
            sRelacion.insert(0, " " + relacionTablaDestino.getNbrelacion());
            lTablas.add(sTablaOrigen);

          }
          else {
            bExiste = true;
          }

        }
        while (!bExiste);

        // Se concatena las relaciones a la variable general
        sInnerJoin.append(sRelacion);
        queryExc = " AND BOK.CDALIAS NOT IN (" + BARRIDO + ") ";
      }
    }

    if (informe.getNbquery1() != null) {
      query = informe.getNbquery1() + " " + sInnerJoin + " WHERE 1=1 ";
    }

    if (informe.getNbtxtlibre() != null) {
      query += informe.getNbtxtlibre();
    }

    /** Inicio - Componente filtros dinamicos. */
    try {
      query += this.tmct0InformeAssembler.getQueryFromComponenteGenerico(filterDto.getListComponenteDirectivaDTO(),
          query, filterDto.getFechaEjecucion(), posicionMercado, informe.getTmct0InformesClases().getFechaBase());
    }
    catch (ParseException e) {
      LOG.info("Error al parsear fecha: {}", e.getMessage(), e);
    }
    /** Fin - Componente filtros dinamicos. */

    query += queryExc;

    if (informe.getNbquery2() != null && isLastQuery) {
      query += informe.getNbquery2();
    }
    if (queryUnion.isEmpty()) {
      queryUnion += query;
    }
    else {
      queryUnion += "\nUNION ALL\n" + query;
    }
    return queryUnion;
  }

  public Tmct0InformeGeneradoDTO queryInforme(Tmct0GeneracionInformeFilterDTO filterDto, Tmct0Informes informe)
      throws SIBBACBusinessException {
    LOG.debug("Se va a construir la query para ejecutar el informe ");

    SimpleDateFormat sdfDB = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat inSDF = new SimpleDateFormat("dd/MM/yyyy");

    Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();

    dto.setColumnas(getColumnasInformeDto(informe));

    // Comprobamos sí el informe tiene hijos
    List<Tmct0Informes> listInformes = new ArrayList<Tmct0Informes>();
    listInformes = tmct0InformesDao.findByIdInformeHijosyPadres(informe.getId());

    if (listInformes.size() > 1) {
      String queryUnion = "";
      int conteo = 0;
      for (Tmct0Informes informexx : listInformes) {
        conteo = conteo + 1;
        queryUnion = formarQueryConUnion(filterDto, informexx, queryUnion, (conteo == listInformes.size()));

      }
      if (StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase())
          && informe.getTmct0InformesClases().getQueryPorDia().equals("SI")) {
        try {
          queryUnion = queryUnion.replace("\"FECHA_DESDE\"",
              "TO_DATE('" + sdfDB.format(inSDF.parse(filterDto.getFechaDesdeBase())) + "', 'YYYY-MM-DD')");
          queryUnion = queryUnion.replace("\"FECHA_HASTA\"",
              "TO_DATE('" + sdfDB.format(inSDF.parse(filterDto.getFechaHastaBase())) + "', 'YYYY-MM-DD')");
        }
        catch (ParseException e) {
          LOG.error(
              "Error al transformar el texto adicional FECHA_DESDE o FECHA_HASTA. FECHA_DESDE = "
                  + filterDto.getFechaDesdeBase() + " FECHA_HASTA = " + filterDto.getFechaHastaBase(), e);
        }
      }
      dto.setQuery(queryUnion);

    }
    else {
      if (null != informe) {
        String query = "";
        query = formarQueryConUnion(filterDto, informe, "", true);
        if (StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase())
            && informe.getTmct0InformesClases().getQueryPorDia().equals("SI")) {
          try {
            query = query.replace("\"FECHA_DESDE\"",
                "TO_DATE('" + sdfDB.format(inSDF.parse(filterDto.getFechaDesdeBase())) + "', 'YYYY-MM-DD')");
            query = query.replace("\"FECHA_HASTA\"",
                "TO_DATE('" + sdfDB.format(inSDF.parse(filterDto.getFechaHastaBase())) + "', 'YYYY-MM-DD')");
          }
          catch (ParseException e) {
            LOG.error(
                "Error al transformar el texto adicional FECHA_DESDE o FECHA_HASTA. FECHA_DESDE = "
                    + filterDto.getFechaDesdeBase() + " FECHA_HASTA = " + filterDto.getFechaHastaBase(), e);
          }
        }
        dto.setQuery(query);
      }
    }

    LOG.debug("Query generada " + dto.getQuery());

    return dto;
  }

  @Transactional
  public void eliminarPlanificacionesInforme(Integer id) {
    Tmct0InformesPlanificacion planificacion = em.find(Tmct0InformesPlanificacion.class, id);
    if (planificacion != null) {
      planificacion.setEstado(Constantes.PLANTILLA_BAJA);
      em.persist(planificacion);
    }
  }

  public int ejecutaInforme(Tmct0GeneracionInformeFilterDTO filterDto, Tmct0Informes informe,
      Tmct0InformeDTONEW plantilla) throws SIBBACBusinessException {

    LOG.debug("Entra en ejecutaInforme ");
    // se inicializan los dto

    Tmct0InformeGeneradoDTO dto = queryInforme(filterDto, informe);
    List<Integer> iColumnasOrdencacion = getIndexOrdenacion(plantilla);

    int numRows = generacionInformeDao.ejecutaQueryToFile(dto.getQuery(), iColumnasOrdencacion, rutaInformes,
        informe.getNbdescription(), informe.getGenInicio(), filterDto.getFechaEjecucion());

    LOG.debug("Sale de ejecutaInforme ");
    return numRows;

  }

  private List<Integer> getIndexOrdenacion(Tmct0InformeDTONEW plantilla) {

    List<Integer> iColumnasOrdencacion = new ArrayList<>();

    for (int k = 0; k < plantilla.getListaCamposAgrupacion().size(); k++) {
      // Se busca la posicion dentro de la lista de campos detalle.
      for (int j = 0; j < plantilla.getListaCamposDetalle().size(); j++) {
        int iClaveCampoOrdenacion = Integer.parseInt(plantilla.getListaCamposAgrupacion().get(k).getKey());
        if (Integer.parseInt(plantilla.getListaCamposDetalle().get(j).getKey()) == iClaveCampoOrdenacion) {
          iColumnasOrdencacion.add(j);
        }
      }
    }

    if (iColumnasOrdencacion.size() != plantilla.getListaCamposDetalle().size()) {
      for (int j = 0; j < plantilla.getListaCamposDetalle().size(); j++) {
        if (!iColumnasOrdencacion.contains((Integer) j)) {
          iColumnasOrdencacion.add(j);
        }
      }
    }

    return iColumnasOrdencacion;
  }

  private Tmc0InformeCeldaDTO getCeldaInformeDTO(Tmct0InformeGeneradoDTO dto, int i, Object columna) {
    String cabeceraCampo = dto.getColumnas().get(i).getCabecera();
    Character precisionCampo = dto.getColumnas().get(i).getPrecision();
    Character totalizar = dto.getColumnas().get(i).getTotalizar();
    String tipoCampo = dto.getColumnas().get(i).getTipoCampo();

    Object contenidoFinal = null;
    StringBuilder formato = new StringBuilder();

    if (precisionCampo.toString().trim().length() != 0 && Character.isDigit(precisionCampo)) {
      contenidoFinal = columna;
      // se evalua que el numero de decimales sea mayor de 0
      // El formato con el que se pintan los datos es "#,##0.00"
      if (Integer.valueOf(precisionCampo.toString()) > 0) {
        formato = new StringBuilder("#,##0.");
        for (int j = 0; j < Integer.valueOf(precisionCampo.toString()); j++) {
          formato.append("0");
        }
      }
      else {
        formato = new StringBuilder("#,##0");
      }
    }
    else {
      contenidoFinal = columna;
    }

    return (new Tmc0InformeCeldaDTO(cabeceraCampo, null != contenidoFinal ? contenidoFinal : "", formato.toString(),
        totalizar, precisionCampo, tipoCampo));
  }

  private void addTotalizar(Tmct0InformeGeneradoDTO dto, int i, Object columna) {
    try {
      String key = dto.getColumnas().get(i).getCabecera();
      Double total = 0.0;
      if (null != dto.getTotales().get(key)) {
        total = dto.getTotales().get(key);
      }
      if (total != null && NumberUtils.isNumber(String.valueOf(columna))) {
        total = total + new Double(String.valueOf(columna));
        dto.getTotales().put(key, total);
      }
    }
    catch (Exception ex) {
      LOG.debug("Exception @ addTotalizar:",ex);
    }
  }

  private List<Tmct0InformeColumnaDTO> getColumnasInformeDto(Tmct0Informes informe) {
    LOG.debug("Se van a establecer las columnas-detalle del informe ");

    List<Tmct0InformeColumnaDTO> listaColumns = new ArrayList<Tmct0InformeColumnaDTO>();

    String sIngles = SIBBACServicePlantillasInformesNEW.EnuIdioma.INGLES.name();
    boolean informeIngles = informe.getNbidioma().equals(sIngles);

    List<Tmct0InformesDatos> datos = informe.getTmct0InformesDatosList();
    if (datos != null && !datos.isEmpty()) {
      // se ordenan las columnas
      Collections.sort(datos, new Comparator<Tmct0InformesDatos>() {
        public int compare(Tmct0InformesDatos p1, Tmct0InformesDatos p2) {
          return new Integer(p1.getNuposici()).compareTo(new Integer(p2.getNuposici()));
        }
      });

      for (Tmct0InformesDatos dato : datos) {
        Tmct0InformesDatosId informeDato = dato.getId();
        // Si el tipo de campo es 0 es el detalle
        if (informeDato.getIdTipo().getId().equals(SIBBACServiceGenerarInformesNEW.EnuTipoCampo.DETALLE.getValue())) {
          Tmct0InformesCampos column = informeDato.getIdCampo();
          Tmct0InformeColumnaDTO miColumn = new Tmct0InformeColumnaDTO();
          // se establece el texto de la cabecera
          if (informeIngles) {
            if (StringUtils.isNotBlank(dato.getNombre())) {
              miColumn.setCabecera(dato.getNombre());
            }
            else {
              miColumn.setCabecera(column.getNbcolingles());
            }
          }
          else {
            if (StringUtils.isNotBlank(dato.getNombre())) {
              miColumn.setCabecera(dato.getNombre());
            }
            else {
              miColumn.setCabecera(column.getNbcolumna());
            }
          }

          // se establece la precision
          if (column.getPrecision() != null) {
            miColumn.setPrecision(column.getPrecision());
          }

          // se establece si es un campo que totaliza
          if (column.getTotalizar() == 'N') {
            miColumn.setTotal(false);
          }
          else {
            miColumn.setTotal(true);
          }

          // Se obtiene info de la totalizacion
          miColumn.setTotalizar(column.getTotalizar());

          // Se obtiene el tipo del campo
          miColumn.setTipoCampo(column.getTipoCampo());

          listaColumns.add(miColumn);
        }
      }
    }

    LOG.debug("Se han establecido las columnas-detalle del informe ");
    return listaColumns;
  }

  public List<String> getColumnasSeparacion(Tmct0Informes informe) {
    LOG.debug("Entra en getColumnasSeparacion ");
    List<String> columnas = new ArrayList<String>();
    List<Tmct0InformesDatos> datos = informe.getTmct0InformesDatosList();
    if (datos != null && !datos.isEmpty()) {

      Collections.sort(datos, new Comparator<Tmct0InformesDatos>() {
        public int compare(Tmct0InformesDatos p1, Tmct0InformesDatos p2) {
          return new Integer(p1.getNuposici()).compareTo(new Integer(p2.getNuposici()));
        }
      });

      for (Tmct0InformesDatos dato : datos) {
        Tmct0InformesDatosId informeDato = dato.getId();
        if (informeDato.getIdTipo().getId().equals(SIBBACServiceGenerarInformesNEW.EnuTipoCampo.SEPARADOR.getValue())) {
          Tmct0InformesCampos elCampo = informeDato.getIdCampo();
          String sIngles = SIBBACServicePlantillasInformesNEW.EnuIdioma.INGLES.name();
          if (informe.getNbidioma().equals(sIngles)) {
            columnas.add(elCampo.getNbcolingles());
          }
          else {
            columnas.add(elCampo.getNbcolumna());
          }
        }
      }
    }

    LOG.debug("fin getColumnasSeparacion ");

    return columnas;

  }

  @Transactional
  /**
   * Recorre todas las fechas y crea un nuevo hilo de ejecucion del informe por cada fecha.
   * 
   * @param lFechasEjecucion - Lista de las fechas
   * @param filters -- Filtros
   * @param lDatosPorDias -- Lista resultado con los datos de cada dia.
   */
  public Integer ejecutaInformePorDias(List<Date> lFechasEjecucion, Tmct0GeneracionInformeFilterDTO filters,
      Tmct0Informes informe, Tmct0InformeDTONEW plantilla) throws SIBBACBusinessException {

    LOG.debug("Entra en ejecutaInformePorDias");
    Integer numRows = 0;

    // Inicializaciones
    // Se van a crear un pool de creas indicado en la variable. Son el numero de
    // hilos que se ejecutan a la vez.
    // Se realiza esto porque al ser el mismo objeto unos hilos estaban cogiendo
    // los filtros de otros
    ExecutorService executor = Executors.newFixedThreadPool(numThreadsPool);

    List<Future<Integer>> futures = new ArrayList<>();

    for (int i = 0; i < lFechasEjecucion.size(); i++) {
      // Se crea un nuevo filtro para cada ejecucion aunque solo va a cambiar la
      // fecha de contratacion
      // pero es necesario crer un objeto nuevo para que cada hilo coja su
      // filtro
      Tmct0GeneracionInformeFilterDTO filters2 = new Tmct0GeneracionInformeFilterDTO();
      // Se asignan los valores que no varian
      filters2.setIdInforme(filters.getIdInforme());
      filters2.setOpcionesEspeciales(filters.getOpcionesEspeciales());
      filters2.setListComponenteDirectivaDTO(filters.getListComponenteDirectivaDTO());
      filters2.setFechaEjecucion(lFechasEjecucion.get(i));
      filters2.setNameFile(filters.getNameFile());
      filters2.setFechaDesdeBase(filters.getFechaDesdeBase());
      filters2.setFechaHastaBase(filters.getFechaHastaBase());

      ProcesaInformeDiaRunnableNEW runnable = new ProcesaInformeDiaRunnableNEW(filters2, this, informe, i, plantilla);
      futures.add(executor.submit(runnable));

    }

    try {
      executor.shutdown();
      executor.awaitTermination(12, TimeUnit.HOURS);
    }
    catch (InterruptedException e) {
      LOG.error(
          "[Tmct0InformesBo :: ejecutaInformePorDias] -  Se ha interrumpido algún thread en el control de cuantos quedan vivos ",
          e);
      return null;
    }

    for (Future<Integer> future : futures) {
      if (future.isDone()) {
        try {
          numRows = numRows + future.get();
        }
        catch (InterruptedException e) {
          SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
          for (int i = 0; i < lFechasEjecucion.size(); i++) {
            try {
              File file = new File(rutaInformes + "temp/" + informe.getNbdescription()
                  + informe.getGenInicio().getTime() + sdf.format(lFechasEjecucion.get(i)) + ".csv");
              file.delete();
            }
            catch (Exception e1) {

            }

          }
          LOG.error("[Tmct0InformesBo :: ejecutaInformePorDias] - Se inturrumpio la ejecucion del hilo ", e);
        }
        catch (ExecutionException e) {
          SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
          for (int i = 0; i < lFechasEjecucion.size(); i++) {
            try {
              File file = new File(rutaInformes + "temp/" + informe.getNbdescription()
                  + informe.getGenInicio().getTime() + sdf.format(lFechasEjecucion.get(i)) + ".csv");
              file.delete();
            }
            catch (Exception e1) {

            }

          }
          throw new SIBBACBusinessException(
              "[Tmct0InformesBo :: ejecutaInformePorDias] - Error en la ejecucion del hilo ", e.getCause());
        }
      }
    }

    LOG.debug("Fin de ejecutaInformePorDias, numero de datos generados " + numRows);

    return numRows;

  }

  /**
   * Actualizacion de campos en la entidad de informe una vez lanzado dicho
   * informe al inicio.
   * @param codigoUsuario
   * @param tmct0Informes
   * @throws SIBBACBusinessException
   */
  @Transactional
  public void saveDatosAdicionalesGenerarInformeInicio(String codigoUsuario, Tmct0Informes tmct0Informes)
      throws SIBBACBusinessException {

    // GEN_USU: codigo de usuario que está lanzando el informe,
    // si el campo tenía un valor previo, se sustituye.
    tmct0Informes.setGenUsu(codigoUsuario);

    // GEN_INICIO: Con el timestamp actual, si el campo tenía un valor previo,
    // se sustituye.
    tmct0Informes.setGenInicio(new Date());

    // GEN_FIN: Se carga a null cuando empieza a generarse el informe.
    tmct0Informes.setGenFin(null);

    this.save(tmct0Informes);
  }

  /**
   * Actualizacion de campos en la entidad de informe una vez lanzado dicho
   * informe al finalizar.
   * @param codigoUsuario
   * @param tmct0Informes
   * @throws SIBBACBusinessException
   */
  @Transactional
  private void saveDatosAdicionalesGenerarInformeFin(String codigoUsuario, Tmct0Informes tmct0Informes)
      throws SIBBACBusinessException {

    // GEN_FIN: Con null al iniciar la generación del informe.
    // Finalizado el informe se sustituye el null por el timestamp actual.
    tmct0Informes.setGenFin(new Date());

    this.save(tmct0Informes);
  }

  /**
   * Generar informe dinamico en excel de los filtros pasados (los filtros
   * contienen la plantilla del informe)
   * 
   * @param paramsObjects: Objeto de parametros de tipo
   * getTmct0GeneracionInformeFilterDto generarExcelSimple: Especificación de si
   * se debe generar el excel simple con la información
   * @param getTerceros es cuenta terceros
   * @param getSimple es simple
   * @param Tmct0InformesPlanificacion: util para procesos batch
   * @param origen origen
   * @return Mapa para generación en Excel
   * @throws Exception POR FAVOR NO QUITAR LA PALABRA CLAVE SYNCHRONIZED YA QUE
   * SI SE LO HACE AFECTA EL MULTI-THREADING
   */

  public Map<String, Object> getInformeExcel(Map<String, Object> paramsObjects, boolean getTerceros, boolean getSimple,
      Tmct0InformesPlanificacion tmct0InformesPlanificacion, String origen, Tmct0Informes informe) throws Exception {

    LOG.debug("Entra en exportaInformeExcel");

    Date dateInicioEjecucion = new Date();

    Map<String, Object> result = new HashMap<>();
    Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();
    TreeMap<String, List<Tmc0InformeCeldaDTO>> tResultados = new TreeMap<>();
    List<Integer> iColumnasTotalizan = new ArrayList<>();
    List<Integer> iColumnasOrdencacion = new ArrayList<>();
    List<Tmct0InformeColumnaDTO> columnasInforme = new ArrayList<>();
    List<List<Tmc0InformeCeldaDTO>> lDatosFinales = new ArrayList<>();
    // Se necesita nombre de usuario para datos de generacion.
    String codigoUsuario = (String) paramsObjects.get("userName");
    LOG.debug("Codigo de usuario para informes: " + codigoUsuario);
    int contador = 0;
    List<Tmc0InformeCeldaDTO> tmct0InformeFilaAux = new ArrayList<Tmc0InformeCeldaDTO>();
    List<List<Tmc0InformeCeldaDTO>> lAgrupacionCamposExcel = new ArrayList<>();
    List<Tmc0InformeCeldaDTO> listaAgrupacionCamposExcel = new ArrayList<Tmc0InformeCeldaDTO>();
    boolean claveVacia = false;
    Tmct0GeneracionInformeFilterDTO filters = new Tmct0GeneracionInformeFilterDTO();

    try {
      // Se recupera el informe solicitado en los parametros. No se inicializa
      // el objeto ensamblador por estar en
      // autowired.
      filters = tmct0InformeAssemblerNEW.getTmct0GeneracionInformeFilterDto(paramsObjects);

      if (null == informe) {
        Tmct0InformesBoNEW s = ctx.getBean(Tmct0InformesBoNEW.class);
        informe = s.getInforme(filters.getIdInforme(), true);
        String[] clasesRouting = CLASESROUTING.split(",", -1);
        for (int i = 0; i < clasesRouting.length; i++) {
          if (informe.getTmct0InformesClases().getClase().equals(clasesRouting[i])) {
            filters.setOpcionesEspeciales("EXCLROUTING");
            break;
          }
          else {
            filters.setOpcionesEspeciales(null);
          }
        }
      }

      filters.setNameFile(codigoUsuario + "_" + informe.getNbdescription() + "_" + df.format(new Date()));

      // Se obtiene tambien la plantilla.
      Tmct0InformeDTONEW plantilla = tmct0InformeAssemblerNEW.getTmct0InformeDto(informe);

      List<Integer> listaPosicionesAgrupacionExcel = tmct0InformesDatosDao.getPosicionAgrupacionExcel(plantilla
          .getIdInforme());

      // se escribe la traza del informe ejecutado en el log de la aplicación
      this.logInforme(filters, informe.getNbdescription());

      // Obtenemos el listado de mercados asociados a la clase
      List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(informe.getTmct0InformesClases()
          .getClase());

      // Recorremos la lista de mercados de la clase para saber que posicion
      // ocupa el especifico de esta clase
      int posicionMercado = 0;
      for (int i = 0; i < mercadosClase.size(); i++) {
        if (mercadosClase.get(i).equals(informe.getTmct0InformesClases().getMercado())) {
          posicionMercado = i;
          break;
        }
      }

      // Se ejecuta la query de informe en base a cierta logica.
      Integer numRows = this.getResultadosQueryInforme(filters, informe, posicionMercado, plantilla);

      agrupaFicherosInforme(filters.getFechasEjecucion(), informe, filters);

      long diff = new Date().getTime() - dateInicioEjecucion.getTime();
      long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

      if ((numRows > limiteExcelCsv || minutes >= Long.valueOf(timeout)) && null == tmct0InformesPlanificacion) {

        generaInformePorEmail(informe, filters, plantilla, columnasInforme, codigoUsuario,
            listaPosicionesAgrupacionExcel);
        result.clear();
        result.put("timeout", "El tamaño o el tiempo de generación del informe es excesivo. Se enviará por email.");
        return result;

      }
      else {
        DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // Se procesan todos las listas y se unen en un map con clave:
        // -- Si el informe no agrupa, la clave esta formada por todos los
        // campos.
        // -- Si el informe agrupa, la clave esta formada por los campos que
        // agrupan y no totalizan
        dto.setColumnas(getColumnasInformeDto(informe));
        // Obtenemos las columnas que totalizan
        LOG.debug("Se obtienen las columnas que totalizan");
        columnasInforme = dto.getColumnas();

        Tmc0InformeCeldaDTO cell;
        List<Tmc0InformeCeldaDTO> fila;
        String[] linea;
        String lineaf;
        List<List<Tmc0InformeCeldaDTO>> listaFilas = new ArrayList<List<Tmc0InformeCeldaDTO>>();
        try (FileReader fr = new FileReader(new File(rutaInformes + filters.getNameFile() + "1.csv"));
            BufferedReader lnr = new BufferedReader(fr);) {

          iColumnasOrdencacion = getIndexOrdenacion(plantilla);

          while ((lineaf = lnr.readLine()) != null) {
            fila = new ArrayList<Tmc0InformeCeldaDTO>();
            linea = StringUtils.splitPreserveAllTokens(lineaf, ";");
            for (int j = 0; j < linea.length; j++) {
              try {
                cell = getCeldaInformeDTO(dto, j, new BigInteger(linea[iColumnasOrdencacion.indexOf(j)]));
                if (cell.getTotalizar() != 'N') {
                  addTotalizar(dto, j, new BigInteger(linea[iColumnasOrdencacion.indexOf(j)]));
                }
              }
              catch (Exception e) {
                try {
                  cell = getCeldaInformeDTO(dto, j, new BigDecimal(linea[iColumnasOrdencacion.indexOf(j)]));
                  if (cell.getTotalizar() != 'N') {
                    addTotalizar(dto, j, new BigDecimal(linea[iColumnasOrdencacion.indexOf(j)]));
                  }
                  LOG.debug("Exception e @ getInformeExcel: ",e);
                }
                catch (Exception e1) {
                  try {
                    cell = getCeldaInformeDTO(dto, j,
                        formatter.format((Date) parser.parse(linea[iColumnasOrdencacion.indexOf(j)])));
                    if (cell.getTotalizar() != 'N') {
                      addTotalizar(dto, j,
                          formatter.format((Date) parser.parse(linea[iColumnasOrdencacion.indexOf(j)])));
                    }
                  }
                  catch (Exception e2) {
                    cell = getCeldaInformeDTO(dto, j, linea[iColumnasOrdencacion.indexOf(j)].replaceAll("string", ""));
                    if (cell.getTotalizar() != 'N') {
                      addTotalizar(dto, j, linea[iColumnasOrdencacion.indexOf(j)].replaceAll("string", ""));
                    }
                    LOG.debug("Exception e2 @ getInformeExcel: ",e2);
                  }
                }
              }
              fila.add(cell);
            }
            listaFilas.add(fila);
          }

        }// autoclose buffered reader
        for (int i = 0; i < columnasInforme.size(); i++) {
          if (columnasInforme.get(i).isTotal()) {
            iColumnasTotalizan.add(Integer.valueOf(i));
          }
        }

        if (columnasInforme.size() == iColumnasTotalizan.size()) {
          claveVacia = true;
        }

        iColumnasOrdencacion.clear();
        // Se obtienen las columnas de ordenacion
        LOG.debug("Se obtienen las columnas de ordenacion");
        for (int i = 0; i < plantilla.getListaCamposOrdenacion().size(); i++) {
          int iClaveCampoOrdenacion = Integer.parseInt(plantilla.getListaCamposOrdenacion().get(i).getKey());
          // Se busca la posicion dentro de la lista de campos detalle.
          for (int j = 0; j < plantilla.getListaCamposDetalle().size(); j++) {
            if (Integer.parseInt(plantilla.getListaCamposDetalle().get(j).getKey()) == iClaveCampoOrdenacion) {
              iColumnasOrdencacion.add(j);
            }
          }
        }

        LOG.debug("Se unen todos los resultados ordenados y agrupados");
        List<Tmc0InformeCeldaDTO> auxTotales; // Variable auxiliar para ir
                                              // sumando las registros
                                              // duplicados

        int iNumeroFila = 0;
        for (List<Tmc0InformeCeldaDTO> tmct0InformeFila : listaFilas) {

          iNumeroFila++;

          // Se forma la clave. esta va a estar formada por los campos de
          // ordenacion mas los campos de ag
          final String sClave;

          final StringBuffer sCamposDetalle = new StringBuffer();

          // Para cuando el informe no esta agrupado y tengo que insertar todos
          // los registros,
          // Le uno tambien el valor de una variable auxiliar iNumeroFila porque
          // si se da el caso que todos los valores
          // son iguales a los de otra fila, la clave que generaria seria la
          // misma.
          if (plantilla.getListaCamposAgrupacion().isEmpty()) {
            sCamposDetalle.append(iNumeroFila).append(sCamposDetalle);
          }

          final StringBuffer sCamposOrdenacion = new StringBuffer();
          String sFormato;

          // Formo la clave de ordenacion
          for (int j = 0; j < iColumnasOrdencacion.size(); j++) {

            sFormato = tmct0InformeFila.get(iColumnasOrdencacion.get(j)).getFormato();

            if (sFormato != null && !sFormato.equals("")) { // Es un campo
                                                            // numerico se le
                                                            // ponen ceros por
                                                            // la derecha y
                                                            // por la izquiera
                                                            // para que ordene
                                                            // bien

              String sValor = tmct0InformeFila.get(iColumnasOrdencacion.get(j)).getValor();

              if (StringHelper.isNumerico(sValor)) {
                if (sValor.equals("0E-8")) {
                  sValor = "0.0";
                }
                // Se busca el separador de decimales
                StringTokenizer st = new StringTokenizer(sValor, ".");

                String sDecimal = String.format("%1$015d", Integer.parseInt(st.nextToken()));
                String sFraccion = String
                    .format("%1$015d", Integer.parseInt(st.hasMoreTokens() ? st.nextToken() : "0"));
                sCamposOrdenacion.append(sDecimal).append(",").append(sFraccion);
              }

            }
            else {
              sCamposOrdenacion.append(tmct0InformeFila.get(iColumnasOrdencacion.get(j)).getValor());
            }

          }

          for (int i = 0; i < tmct0InformeFila.size(); i++) {
            // Si el informe esta agrupado solo se cogen como clave los que no
            // totalizan.
            if ((plantilla.getListaCamposAgrupacion().isEmpty()) || (!iColumnasTotalizan.contains(i))) {
              sCamposDetalle.append(tmct0InformeFila.get(i).getValor());
            }
          }

          if (claveVacia) {
            sClave = "";
          }
          else {
            sClave = sCamposOrdenacion.toString().concat(sCamposDetalle.toString());
          }

          if (!tResultados.containsKey(sClave)) {
            // Clave nueva - solo debe actualizarse
            /*** Inicio - Totalizacion por Java. **/
            // Aqui para el caso de claves nuevas, si hay un conteo este debe
            // inicializar
            GeneracionInformesHelper.ajustarValoresAgrupacionClaveNueva(tmct0InformeFila, iColumnasTotalizan);
            /*** Fin - Totalizacion por Java. **/

            tResultados.put(sClave, tmct0InformeFila);

          }
          else {
            /*** Inicio - Totalizacion por Java. **/
            // Elemento ya existente en el mapa, no se inserta uno nuevo, se
            // suman los campos que totalizan
            auxTotales = tResultados.get(sClave);

            // Se totalizan los objetos dependiendo del campo TOTALIZAR de cada
            // uno
            // de los campos de detalle.
            GeneracionInformesHelper.totalizarObjsDBManualClaveNueva(auxTotales, iColumnasTotalizan, tmct0InformeFila,
                tResultados, sClave);
            /*** Fin - Totalizacion por Java. **/
          }
        }

        // Ordenacion.
        LOG.debug("Se convierte el listado al formato que necesita la pantalla ");
        // Se recorre el map y se forma una lista única de datos finales que se
        // envia a pantalla y se procesa en el excel
        Iterator<Map.Entry<String, List<Tmc0InformeCeldaDTO>>> it = tResultados.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, List<Tmc0InformeCeldaDTO>> me = it.next();
          lDatosFinales.add((List<Tmc0InformeCeldaDTO>) me.getValue());
        }

        LOG.debug("Fin formacion datos de cosulta se asigna al dto final ");
        dto.setResultados(lDatosFinales);
        // La siguiente es una segunda agrupacion que solo se aplica si hay
        // algunos de los siguientes totalizadores:
        // SUM / SUM DISTINCT / COUNT DISTINCT
        if (GeneracionInformesHelper.isContieneTotalizadoresSUMorDSUMorDCOUNT(dto.getResultados())) {
          Map<String, List<Tmc0InformeCeldaDTO>> mapa = GeneracionInformesHelper.getResultadosAgrupadosDTO(dto);
          List<List<Tmc0InformeCeldaDTO>> listListTmc0InformeCeldaDTO = new ArrayList<List<Tmc0InformeCeldaDTO>>();
          for (Map.Entry<String, List<Tmc0InformeCeldaDTO>> entry : mapa.entrySet()) {
            listListTmc0InformeCeldaDTO.add(entry.getValue());
          }
          dto.setResultados(listListTmc0InformeCeldaDTO);
        }

        if (null != listaPosicionesAgrupacionExcel && listaPosicionesAgrupacionExcel.size() > 0) {
          for (int contadorAux = 0; contadorAux < lDatosFinales.size(); contadorAux++) {

            /*** Inicio - Agrupación campos excel. **/
            boolean mismosCamposAgrupacionExcel = false;
            if (contador == 0) {
              tmct0InformeFilaAux = new ArrayList<Tmc0InformeCeldaDTO>();
              for (int i = 0; i < lDatosFinales.get(contadorAux).size(); i++) {
                Tmc0InformeCeldaDTO tmc0InformeCeldaDTO = new Tmc0InformeCeldaDTO(lDatosFinales.get(contadorAux).get(i)
                    .getColumna(), lDatosFinales.get(contadorAux).get(i).getContenido(), lDatosFinales.get(contadorAux)
                    .get(i).getFormato(), lDatosFinales.get(contadorAux).get(i).getTotalizar());
                tmct0InformeFilaAux.add(tmc0InformeCeldaDTO);
              }
            }
            else if (contador > 0) {
              for (int i = 0; i < listaPosicionesAgrupacionExcel.size(); i++) {
                if (tmct0InformeFilaAux.get((listaPosicionesAgrupacionExcel.get(i)) - 1).getValor()
                    .equals(lDatosFinales.get(contadorAux).get((listaPosicionesAgrupacionExcel.get(i)) - 1).getValor())) {
                  mismosCamposAgrupacionExcel = true;
                }
                else {
                  mismosCamposAgrupacionExcel = false;
                  break;
                }
              }

              tmct0InformeFilaAux = new ArrayList<Tmc0InformeCeldaDTO>();
              listaAgrupacionCamposExcel = new ArrayList<Tmc0InformeCeldaDTO>();
              for (int i = 0; i < lDatosFinales.get(contadorAux).size(); i++) {
                Tmc0InformeCeldaDTO tmc0InformeCeldaDTO = new Tmc0InformeCeldaDTO(lDatosFinales.get(contadorAux).get(i)
                    .getColumna(), lDatosFinales.get(contadorAux).get(i).getContenido(), lDatosFinales.get(contadorAux)
                    .get(i).getFormato(), lDatosFinales.get(contadorAux).get(i).getTotalizar());
                tmct0InformeFilaAux.add(tmc0InformeCeldaDTO);
                Tmc0InformeCeldaDTO tmc0InformeCeldaDTO2 = new Tmc0InformeCeldaDTO(lDatosFinales.get(contadorAux)
                    .get(i).getColumna(), lDatosFinales.get(contadorAux).get(i).getContenido(), lDatosFinales
                    .get(contadorAux).get(i).getFormato(), lDatosFinales.get(contadorAux).get(i).getTotalizar());
                listaAgrupacionCamposExcel.add(tmc0InformeCeldaDTO2);
              }

              if (mismosCamposAgrupacionExcel) {
                for (int i = 0; i < listaPosicionesAgrupacionExcel.size(); i++) {
                  listaAgrupacionCamposExcel.get((listaPosicionesAgrupacionExcel.get(i)) - 1).setContenido("");
                  listaAgrupacionCamposExcel.get((listaPosicionesAgrupacionExcel.get(i)) - 1).setFormato("");
                }
              }
            }
            contador++;

            /*** Fin - Agrupación campos excel. **/
            if (contador == 1) {
              lAgrupacionCamposExcel.add(tmct0InformeFilaAux);
            }
            else {
              lAgrupacionCamposExcel.add(listaAgrupacionCamposExcel);
            }

          }
          // Se realizan el formateo de los campos resultado de la agrupacion.
          GeneracionInformesHelper.formatearCamposResultadosAgrupacion(lAgrupacionCamposExcel);
          if (informe.getRellenoCampos().equals('S')) {
            rellenarConCaracteres(informe, lAgrupacionCamposExcel);
          }
          dto.setResultados(lAgrupacionCamposExcel);
        }
        else {
          // Se realizan el formateo de los campos resultado de la agrupacion.
          GeneracionInformesHelper.formatearCamposResultadosAgrupacion(lDatosFinales);
          if (informe.getRellenoCampos().equals('S')) {
            rellenarConCaracteres(informe, lDatosFinales);
          }
          dto.setResultados(lDatosFinales);
        }

        result.put("informe", dto);

        // Para las validaciones de alias-contacto se retorna solo la query.
        if (origen != null && origen.equals(OrigenInformeExcelEnum.VALIDACION.getOrigen())) {
          return result;
        }

        // MFG 02/12/2014 Control generacion de la excel:
        // Si el numero de registros es mayor al limite maximo de registros en
        // un excel permitido establecido en propertis
        // no se genera ningun.
        // Si se quieren crear los dos excel (Porque esta consultando y se
        // quieren llevar ya a pantalla), solo se le
        // permite si el numero de registros es superior al limite establecido

        boolean bGenerarFichero = true; // Se inicializa para que genere la
                                        // excel o TXT.

        if ((getSimple && getTerceros && lDatosFinales.size() > limiteRegistrosExcelConsulta)
            || (lDatosFinales.size() > limiteRegistrosExcel)) {
          bGenerarFichero = false;
          // Se informa que no se genera el informe - util para avisar en caso
          // de task batch.
          GenericLoggers.logError(
              InformesBatchProcessor.class,
              Constantes.GENERACION_INFORMES,
              "getInformeExcel",
              "No se ha generado el fichero correspondiente ya que la consulta ha excedido "
                  + "el límite de registros permitidos para el informe con id = "
                  + (informe.getId() != null ? String.valueOf(informe.getId()) : ""));
        }

        /*****************************************************************************************/
        /** TASK BATCH. - Logica para informar sobre contactos de alias. */
        boolean isEnvioListaContactos = false;
        List<InfoMailContactosDTO> listInfoMailContactosDTO = null;
        if (origen != null && origen.equals(OrigenInformeExcelEnum.TASK_BATCH.getOrigen())) {
          // Funcionalidad: Envio de informes a todos los contactos de los alias
          // autorizados para recibir informes
          if (GeneracionInformesHelper.isEnvioListaContactos(tmct0InformesPlanificacion, plantilla)) {
            listInfoMailContactosDTO = this.getInfoMailsContacto(tmct0InformesPlanificacion, dto);
            isEnvioListaContactos = true;
          }
        }

        /*****************************************************************************************/

        if (tmct0InformesPlanificacion == null
            || (tmct0InformesPlanificacion != null && tmct0InformesPlanificacion.isFormatoExcel())) {
          // Se crea el informe Excel si no hay planificacion o si la
          // planificacion tiene formato Excel
          result = this.crearInformeExcel(informe, getTerceros, getSimple, result, filters, lDatosFinales,
              bGenerarFichero, dto, isEnvioListaContactos, listInfoMailContactosDTO, codigoUsuario,
              dateInicioEjecucion, tmct0InformesPlanificacion);
        }
        else {
          // Se crea el informe Excel si no hay planificacion
          // o si la planificacion tiene formato Excel
          result = this.crearInformeTexto(result, lDatosFinales, bGenerarFichero, dto, tmct0InformesPlanificacion);
        }

        /*****************************************************************************************/
        /**
         * TASK BATCH. - Solo se ejecuta si se le pasa alguna planificacion al
         * metodo y si se genera fichero de informe.
         */
        if (origen != null && origen.equals(OrigenInformeExcelEnum.TASK_BATCH.getOrigen()) && bGenerarFichero) {
          this.procesarFicherosTaskBatch(tmct0InformesPlanificacion, result, plantilla, dto, isEnvioListaContactos,
              listInfoMailContactosDTO);
        }
        /*****************************************************************************************/
      }
    }
    catch (Exception e) {
      // Se controla la excepción
      LOG.error("Se produjo un error en la generación del fichero Excel " + e.getMessage(), e);
      dto.getErrores().add("Se produjo un error en la generación del fichero Excel");
      result.put("informe", dto);
      throw e;
    }
    finally {
      // Actualiza campos de generacion de informe hay resultados o no.
      File fileConsulta = new File(rutaInformes + filters.getNameFile() + "1.csv");
      fileConsulta.delete();
      if (informe != null) {
        this.saveDatosAdicionalesGenerarInformeFin(codigoUsuario, informe);
      }
    }

    return result;
  }

  private void agrupaFicherosInforme(List<Date> lFechasEjecucion, Tmct0Informes informe,
      Tmct0GeneracionInformeFilterDTO filters) throws IOException {

    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

    try (FileWriter fw = new FileWriter(rutaInformes + filters.getNameFile() + "1.csv", true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw);) {

      for (int i = 0; i < lFechasEjecucion.size(); i++) {
        try {
          File file = new File(rutaInformes + "temp/" + informe.getNbdescription() + informe.getGenInicio().getTime()
              + sdf.format(lFechasEjecucion.get(i)) + ".csv");
          try (FileReader fr1 = new FileReader(file); BufferedReader lnr1 = new BufferedReader(fr1);) {

            String linea;

            while ((linea = lnr1.readLine()) != null) {
              out.print(linea + "\n");
            }
          }
        }
        catch (Exception e) {

        }
      }

      for (int i = 0; i < lFechasEjecucion.size(); i++) {
        try {
          File file = new File(rutaInformes + "temp/" + informe.getNbdescription() + informe.getGenInicio().getTime()
              + sdf.format(lFechasEjecucion.get(i)) + ".csv");
          file.delete();
        }
        catch (Exception e) {

        }
      }

      // Si no es un informe por fechas
      if (null != lFechasEjecucion && lFechasEjecucion.size() == 0) {
        try {
          File file = new File(rutaInformes + "temp/" + informe.getNbdescription() + informe.getGenInicio().getTime()
              + sdf.format(new Date()) + ".csv");
          try (FileReader fr1 = new FileReader(file); BufferedReader lnr1 = new BufferedReader(fr1);) {

            String linea;
            while ((linea = lnr1.readLine()) != null) {
              out.print(linea + "\n");
            }
            file.delete();
          }
        }
        catch (Exception e) {

        }
      }
    }// autoclose writers
  }

  /**
   * 
   * @param informe
   * @param filters
   * @param plantilla
   * @param columnasInforme
   * @param codigoUsuario
   * @param listaPosicionesAgrupacionExcel
   * @throws SIBBACBusinessException
   * @throws IOException
   */
  private void generaInformePorEmail(Tmct0Informes informe, Tmct0GeneracionInformeFilterDTO filters,
      Tmct0InformeDTONEW plantilla, List<Tmct0InformeColumnaDTO> columnasInforme, String codigoUsuario,
      List<Integer> listaPosicionesAgrupacionExcel) throws SIBBACBusinessException, IOException {

    Comparator<String> comparator = new Comparator<String>() {

      String columnaA = "";
      String columnaB = "";

      @Override
      public int compare(String s1, String s2) {

        StringTokenizer stA = new StringTokenizer(s1, ";");
        StringTokenizer stB = new StringTokenizer(s2, ";");

        while (stA.hasMoreTokens() && stB.hasMoreTokens()) {
          columnaA = stA.nextToken();
          columnaB = stB.nextToken();
          try {

            if (new BigInteger(columnaA).compareTo(new BigInteger(columnaB)) != 0) {
              return new BigInteger(columnaA).compareTo(new BigInteger(columnaB));
            }

          }
          catch (Exception e) {
            try {

              if (new BigDecimal(columnaA).compareTo(new BigDecimal(columnaB)) != 0) {
                return new BigDecimal(columnaA).compareTo(new BigDecimal(columnaB));
              }
            }
            catch (Exception e1) {
              if (columnaA.compareTo(columnaB) != 0) {
                return columnaA.compareTo(columnaB);
              }
            }
          }

        }

        return s1.compareTo(s2);
      }
    };

    File sortTemp = new File(rutaInformes + "sortTemp/");
    if (!sortTemp.exists()) {
      sortTemp.mkdir();
    }

    ExternalSort.mergeSortedFiles(
        ExternalSort.sortInBatch(new File(rutaInformes + filters.getNameFile() + "1.csv"), comparator, 5,
            Charset.defaultCharset(), sortTemp, false), new File(rutaInformes + filters.getNameFile() + "2.csv"));

    columnasInforme = getColumnasInformeDto(informe);

    try {

      List<Integer> indexOrdenacion = getIndexOrdenacion(plantilla);
      Map<Integer, Object> mapTotales = new HashMap<>();
      String terminacionCsv = ".csv";

      if (null != listaPosicionesAgrupacionExcel && listaPosicionesAgrupacionExcel.size() > 0) {
        terminacionCsv = "4.csv";
      }

      if (!plantilla.getListaCamposOrdenacion().isEmpty()) {

        try (FileReader fr = new FileReader(new File(rutaInformes + filters.getNameFile() + "2.csv"));
            BufferedReader lnr = new BufferedReader(fr);

            FileWriter fw = new FileWriter(rutaInformes + filters.getNameFile() + "3.csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);) {
          String lineaf;
          String lineafAnterior = "";
          String linea[];
          String[] lineaAnterior = new String[0];

          BigInteger countLineas = BigInteger.ZERO;
          while ((lineaf = lnr.readLine()) != null) {
            linea = StringUtils.splitPreserveAllTokens(lineaf, ";");
            if (countLineas.compareTo(BigInteger.ZERO) == 0) {
              out.println(lineaf);

            }
            else {
              lineaAnterior = StringUtils.splitPreserveAllTokens(lineafAnterior, ";");
              for (int i = 0; i < linea.length; i++) {
                if (!linea[i].equals(lineaAnterior[i])
                    && columnasInforme.get(indexOrdenacion.get(i)).getTotalizar() == 'N') {
                  out.println(lineaf);
                  break;
                }
              }
            }
            lineafAnterior = lineaf;
            countLineas = countLineas.add(BigInteger.ONE);
          }
        }

        try (FileReader fr1 = new FileReader(new File(rutaInformes + filters.getNameFile() + "2.csv"));
            BufferedReader lnr1 = new BufferedReader(fr1);

            FileReader fr2 = new FileReader(new File(rutaInformes + filters.getNameFile() + "3.csv"));
            BufferedReader lnr2 = new BufferedReader(fr2);

            FileWriter fw = new FileWriter(rutaInformes + filters.getNameFile() + terminacionCsv, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);) {
          for (int j = 0; j < columnasInforme.size(); j++) {
            if (j == (columnasInforme.size() - 1)) {
              out.print(columnasInforme.get(j).getCabecera());
            }
            else {
              out.print(columnasInforme.get(j).getCabecera() + ";");
            }
          }
          out.print("\n");

          String linea1[];
          String linea2[];

          String lineaf2;
          String lineaf1 = lnr1.readLine();
          Map<String, Integer> countDistinct;
          Map<String, Integer> sumDistinct;
          BigInteger count;
          StringBuilder formato = new StringBuilder();

          while ((lineaf2 = lnr2.readLine()) != null) {

            linea2 = StringUtils.splitPreserveAllTokens(lineaf2, ";");
            count = BigInteger.ZERO;
            countDistinct = new HashMap<>();
            sumDistinct = new HashMap<>();
            boolean iguales = true;
            boolean primera = true;

            while (lineaf1 != null && iguales) {
              linea1 = StringUtils.splitPreserveAllTokens(lineaf1, ";");
              for (int i = 0; i < linea1.length; i++) {
                if (!linea1[i].equals(linea2[i]) && columnasInforme.get(indexOrdenacion.get(i)).getTotalizar() == 'N') {
                  iguales = false;
                  break;
                }
              }
              if (iguales) {
                for (int i = 0; i < linea1.length; i++) {
                  Tmct0InformeColumnaDTO columna = columnasInforme.get(indexOrdenacion.get(i));
                  switch (columna.getTotalizar()) {
                    case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_MIN:
                      try {
                        if (new BigInteger(linea2[i]).compareTo(new BigInteger(linea1[i])) == 1) {
                          linea2[i] = linea1[i];
                        }
                      }
                      catch (Exception e) {
                        try {
                          if (new BigDecimal(linea2[i]).compareTo(new BigDecimal(linea1[i])) == 1) {
                            linea2[i] = linea1[i];
                          }
                        }
                        catch (Exception e1) {
                          if (linea2[i].compareTo(linea1[i]) == 1) {
                            linea2[i] = linea1[i];
                          }
                        }
                      }
                      break;
                    case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_MAX:
                      try {
                        if (new BigInteger(linea2[i]).compareTo(new BigInteger(linea1[i])) != 1) {
                          linea2[i] = linea1[i];
                        }
                      }
                      catch (Exception e) {
                        try {
                          if (new BigDecimal(linea2[i]).compareTo(new BigDecimal(linea1[i])) != 1) {
                            linea2[i] = linea1[i];
                          }
                        }
                        catch (Exception e1) {
                          if (linea2[i].compareTo(linea1[i]) != 1) {
                            linea2[i] = linea1[i];
                          }
                        }
                      }
                      break;
                    case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM:
                      if (!primera) {
                        try {
                          linea2[i] = String.valueOf((new BigInteger(linea1[i]).add(new BigInteger(linea2[i])))
                              .longValue());
                        }
                        catch (Exception e) {
                          try {
                            linea2[i] = String.valueOf((new BigDecimal(linea1[i]).add(new BigDecimal(linea2[i])))
                                .doubleValue());
                          }
                          catch (Exception e1) {
                          }
                        }
                      }
                      break;
                    case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_COUNT:
                      if (linea2[i].compareTo(linea1[i]) != 1) {
                        count = count.add(BigInteger.ONE);
                      }
                      break;
                    case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_COUNT_DISTINCT:
                      if (!countDistinct.isEmpty()) {
                        if (!countDistinct.containsKey(linea1[i])) {
                          countDistinct.put(linea1[i], i);
                        }
                      }
                      else {
                        countDistinct.put(linea1[i], i);
                      }
                      break;
                    case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM_DISTINCT:
                      if (!sumDistinct.isEmpty()) {
                        if (!sumDistinct.containsKey(linea1[i])) {
                          sumDistinct.put(linea1[i], i);
                        }
                      }
                      else {
                        sumDistinct.put(linea1[i], i);
                      }
                      break;

                  }
                }
                lineaf1 = lnr1.readLine();
                primera = false;
              }
            }

            for (int j = 0; j < linea2.length; j++) {
              // Pintar la linea en el fichero con los valores correspondientes
              switch (columnasInforme.get(j).getTotalizar()) {
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_NO:
                  if (j == (linea2.length - 1)) {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  \n");
                  }
                  else {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  ;");
                  }
                  break;
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_MIN:
                  if (j == (linea2.length - 1)) {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  \n");
                  }
                  else {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  ;");
                  }
                  if (columnasInforme.get(j).getTotalizar() != 'N') {
                    if (!mapTotales.containsKey(j)) {
                      try {
                        mapTotales.put(j, new BigInteger(linea2[indexOrdenacion.indexOf(j)]));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j, new BigDecimal(linea2[indexOrdenacion.indexOf(j)]));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                    else {
                      try {
                        mapTotales.put(j,
                            ((BigInteger) mapTotales.get(j)).add(new BigInteger(linea2[indexOrdenacion.indexOf(j)])));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j,
                              ((BigDecimal) mapTotales.get(j)).add(new BigDecimal(linea2[indexOrdenacion.indexOf(j)])));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                  }
                  break;
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_MAX:
                  if (j == (linea2.length - 1)) {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  \n");
                  }
                  else {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  ;");
                  }
                  if (columnasInforme.get(j).getTotalizar() != 'N') {
                    if (!mapTotales.containsKey(j)) {
                      try {
                        mapTotales.put(j, new BigInteger(linea2[indexOrdenacion.indexOf(j)]));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j, new BigDecimal(linea2[indexOrdenacion.indexOf(j)]));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                    else {
                      try {
                        mapTotales.put(j,
                            ((BigInteger) mapTotales.get(j)).add(new BigInteger(linea2[indexOrdenacion.indexOf(j)])));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j,
                              ((BigDecimal) mapTotales.get(j)).add(new BigDecimal(linea2[indexOrdenacion.indexOf(j)])));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                  }
                  break;
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM:
                  if (j == (linea2.length - 1)) {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  \n");
                  }
                  else {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  ;");
                  }
                  if (columnasInforme.get(j).getTotalizar() != 'N') {
                    if (!mapTotales.containsKey(j)) {
                      try {
                        mapTotales.put(j, new BigInteger(linea2[indexOrdenacion.indexOf(j)]));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j, new BigDecimal(linea2[indexOrdenacion.indexOf(j)]));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                    else {
                      try {
                        mapTotales.put(j,
                            ((BigInteger) mapTotales.get(j)).add(new BigInteger(linea2[indexOrdenacion.indexOf(j)])));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j,
                              ((BigDecimal) mapTotales.get(j)).add(new BigDecimal(linea2[indexOrdenacion.indexOf(j)])));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                  }
                  break;
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_COUNT:
                  if (j == (linea2.length - 1)) {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  \n");
                  }
                  else {
                    out.print(getFormatoNumero(linea2[indexOrdenacion.indexOf(j)], columnasInforme.get(j)
                        .getPrecision(), formato)
                        + "  ;");
                  }
                  if (columnasInforme.get(j).getTotalizar() != 'N') {
                    if (!mapTotales.containsKey(j)) {
                      try {
                        mapTotales.put(j, new BigInteger(linea2[indexOrdenacion.indexOf(j)]));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j, new BigDecimal(linea2[indexOrdenacion.indexOf(j)]));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                    else {
                      try {
                        mapTotales.put(j,
                            ((BigInteger) mapTotales.get(j)).add(new BigInteger(linea2[indexOrdenacion.indexOf(j)])));
                      }
                      catch (Exception e) {
                        try {
                          mapTotales.put(j,
                              ((BigDecimal) mapTotales.get(j)).add(new BigDecimal(linea2[indexOrdenacion.indexOf(j)])));
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                  }
                  break;
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_COUNT_DISTINCT:
                  BigInteger cuentaDist = BigInteger.ZERO;
                  for (Map.Entry<String, Integer> entry : countDistinct.entrySet()) {
                    if (entry.getValue() == indexOrdenacion.indexOf(j)) {
                      cuentaDist = cuentaDist.add(BigInteger.ONE);
                    }
                  }
                  if (j == (linea2.length - 1)) {
                    out.print(String.valueOf(cuentaDist.longValue()) + "  \n");
                  }
                  else {
                    out.print(String.valueOf(cuentaDist.longValue()) + "  ;");
                  }
                  if (columnasInforme.get(j).getTotalizar() != 'N') {
                    if (!mapTotales.containsKey(j)) {
                      mapTotales.put(j, cuentaDist);
                    }
                    else {
                      mapTotales.put(j, ((BigInteger) mapTotales.get(j)).add(cuentaDist));
                    }
                  }
                  break;
                case sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM_DISTINCT:
                  BigInteger cuentaDistI = BigInteger.ZERO;
                  BigDecimal cuentaDistD = BigDecimal.ZERO;
                  for (Map.Entry<String, Integer> entry : sumDistinct.entrySet()) {
                    if (entry.getValue() == indexOrdenacion.indexOf(j)) {
                      try {
                        cuentaDistI = BigInteger.valueOf(Long.valueOf(entry.getKey())).add(cuentaDistI);
                      }
                      catch (Exception e) {
                        try {
                          cuentaDistD = BigDecimal.valueOf(Double.valueOf(entry.getKey())).add(cuentaDistD);
                        }
                        catch (Exception e1) {
                        }
                      }
                    }
                  }
                  if (j == (linea2.length - 1)) {
                    if (cuentaDistI.compareTo(BigInteger.ZERO) == 0) {
                      out.print(getFormatoNumero(String.valueOf(cuentaDistD.doubleValue()), columnasInforme.get(j)
                          .getPrecision(), formato)
                          + "  \n");
                    }
                    else {
                      out.print(getFormatoNumero(String.valueOf(cuentaDistI.longValue()), columnasInforme.get(j)
                          .getPrecision(), formato)
                          + "  \n");
                    }
                  }
                  else {
                    if (cuentaDistI.compareTo(BigInteger.ZERO) == 0) {
                      out.print(getFormatoNumero(String.valueOf(cuentaDistD.doubleValue()), columnasInforme.get(j)
                          .getPrecision(), formato)
                          + "  ;");
                    }
                    else {
                      out.print(getFormatoNumero(String.valueOf(cuentaDistI.longValue()), columnasInforme.get(j)
                          .getPrecision(), formato)
                          + "  ;");
                    }

                  }
                  if (columnasInforme.get(j).getTotalizar() != 'N') {
                    if (!mapTotales.containsKey(j)) {
                      if (cuentaDistI.compareTo(BigInteger.ZERO) == 0) {
                        mapTotales.put(j, cuentaDistD);
                      }
                      else {
                        mapTotales.put(j, cuentaDistI);
                      }
                    }
                    else {
                      if (cuentaDistI.compareTo(BigInteger.ZERO) == 0) {
                        mapTotales.put(j, ((BigDecimal) mapTotales.get(j)).add(cuentaDistD));
                      }
                      else {
                        mapTotales.put(j, ((BigInteger) mapTotales.get(j)).add(cuentaDistI));
                      }

                    }
                  }
                  break;

              }

            }
          }

          if (!informe.getTotalizar().equals("N")) {
            if (!mapTotales.isEmpty()) {
              for (int j = 0; j < columnasInforme.size(); j++) {
                if (mapTotales.containsKey(j)) {
                  try {
                    out.print(getFormatoNumero(String.valueOf(((BigInteger) mapTotales.get(j)).longValue()),
                        columnasInforme.get(j).getPrecision(), formato) + "  ;");
                  }
                  catch (Exception e) {
                    out.print(getFormatoNumero(String.valueOf(((BigDecimal) mapTotales.get(j)).doubleValue()),
                        columnasInforme.get(j).getPrecision(), formato) + "  ;");
                  }
                }
                else {
                  out.print(" ;");
                }
              }
              out.print(" \n");
            }
          }

        }

        if (null != listaPosicionesAgrupacionExcel && listaPosicionesAgrupacionExcel.size() > 0) {
          try (FileReader fr = new FileReader(new File(rutaInformes + filters.getNameFile() + terminacionCsv));
              BufferedReader lnr = new BufferedReader(fr);

              FileWriter fwAgrupExcel = new FileWriter(rutaInformes + filters.getNameFile() + ".csv", true);
              BufferedWriter bwAgrupExcel = new BufferedWriter(fwAgrupExcel);
              PrintWriter outAgrupExcel = new PrintWriter(bwAgrupExcel);) {
            String lineaf;
            String lineafAnterior = "";
            String linea[];
            String[] lineaAnterior = new String[0];

            BigInteger countLineas = BigInteger.ZERO;
            while ((lineaf = lnr.readLine()) != null) {
              boolean mismosCamposAgrupacionExcel = false;
              linea = StringUtils.splitPreserveAllTokens(lineaf, ";");
              if (countLineas.compareTo(BigInteger.ZERO) == 0) {
                outAgrupExcel.println(lineaf);

              }
              else {
                lineaAnterior = StringUtils.splitPreserveAllTokens(lineafAnterior, ";");
                for (int i = 0; i < listaPosicionesAgrupacionExcel.size(); i++) {
                  if (linea[listaPosicionesAgrupacionExcel.get(i) - 1]
                      .equals(lineaAnterior[listaPosicionesAgrupacionExcel.get(i) - 1])) {
                    mismosCamposAgrupacionExcel = true;
                  }
                  else {
                    mismosCamposAgrupacionExcel = false;
                    break;
                  }
                }
                if (mismosCamposAgrupacionExcel) {
                  for (int i = 0; i < listaPosicionesAgrupacionExcel.size(); i++) {
                    linea[listaPosicionesAgrupacionExcel.get(i) - 1] = "";
                  }
                  for (int i = 0; i < linea.length; i++) {
                    if (linea.length == (i + 1)) {
                      outAgrupExcel.print(linea[i]);
                    }
                    else {
                      outAgrupExcel.print(linea[i] + ";");
                    }
                  }
                  outAgrupExcel.print("\n");
                }
                else {
                  outAgrupExcel.println(lineaf);
                }
              }
              lineafAnterior = lineaf;
              countLineas = countLineas.add(BigInteger.ONE);
            }
          }
        }
      }
      else {

        try (FileReader fr1 = new FileReader(new File(rutaInformes + filters.getNameFile() + "2.csv"));
            BufferedReader lnr1 = new BufferedReader(fr1);

            FileWriter fw = new FileWriter(rutaInformes + filters.getNameFile() + terminacionCsv, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);) {

          String linea1[];
          String lineaf1 = lnr1.readLine();
          StringBuilder formato = new StringBuilder();
          while ((lineaf1 = lnr1.readLine()) != null) {
            linea1 = StringUtils.splitPreserveAllTokens(lineaf1, ";");
            LOG.debug("Memoria disponible en la JVM Informe sin agrupar: " + Runtime.getRuntime().freeMemory());
            for (int i = 0; i < linea1.length; i++) {

              if (i == (linea1.length - 1)) {
                out.print(getFormatoNumero(linea1[indexOrdenacion.indexOf(i)],
                    columnasInforme.get(indexOrdenacion.get(i)).getPrecision(), formato)
                    + "  \n");
              }
              else {
                out.print(getFormatoNumero(linea1[indexOrdenacion.indexOf(i)],
                    columnasInforme.get(indexOrdenacion.get(i)).getPrecision(), formato)
                    + "  ;");
              }

              if (columnasInforme.get(indexOrdenacion.get(i)).getTotalizar() != 'N') {
                if (!mapTotales.containsKey(i)) {
                  try {
                    mapTotales.put(i, new BigInteger(linea1[indexOrdenacion.indexOf(i)]));
                  }
                  catch (Exception e) {
                    try {
                      mapTotales.put(i, new BigDecimal(linea1[indexOrdenacion.indexOf(i)]));
                    }
                    catch (Exception e1) {
                      mapTotales.put(i, BigInteger.ONE);
                    }
                  }
                }
                else {
                  try {
                    mapTotales.put(i,
                        ((BigInteger) mapTotales.get(i)).add(new BigInteger(linea1[indexOrdenacion.indexOf(i)])));
                  }
                  catch (Exception e) {
                    try {
                      mapTotales.put(i,
                          ((BigDecimal) mapTotales.get(i)).add(new BigDecimal(linea1[indexOrdenacion.indexOf(i)])));
                    }
                    catch (Exception e1) {
                      mapTotales.put(i, ((BigInteger) mapTotales.get(i)).add(BigInteger.ONE));
                    }
                  }
                }
              }
            }
          }
          if (!informe.getTotalizar().equals("N")) {
            if (!mapTotales.isEmpty()) {
              for (int j = 0; j < columnasInforme.size(); j++) {
                if (mapTotales.containsKey(j)) {
                  out.print(mapTotales.get(j) + "  ;");
                }
                else {
                  out.print("  ;");
                }
              }
              out.print(" \n");
            }
          }

        }
        // Si hay agrupación por excel
        if (null != listaPosicionesAgrupacionExcel && listaPosicionesAgrupacionExcel.size() > 0) {
          try (FileReader fr = new FileReader(new File(rutaInformes + filters.getNameFile() + terminacionCsv));
              BufferedReader lnr = new BufferedReader(fr);

              FileWriter fwAgrupExcel = new FileWriter(rutaInformes + filters.getNameFile() + ".csv", true);
              BufferedWriter bwAgrupExcel = new BufferedWriter(fwAgrupExcel);
              PrintWriter outAgrupExcel = new PrintWriter(bwAgrupExcel);) {
            String lineaf;
            String lineafAnterior = "";
            String linea[];
            String[] lineaAnterior = new String[0];

            BigInteger countLineas = BigInteger.ZERO;
            while ((lineaf = lnr.readLine()) != null) {
              boolean mismosCamposAgrupacionExcel = false;
              linea = StringUtils.splitPreserveAllTokens(lineaf, ";");
              if (countLineas.compareTo(BigInteger.ZERO) == 0) {
                outAgrupExcel.println(lineaf);

              }
              else {
                lineaAnterior = StringUtils.splitPreserveAllTokens(lineafAnterior, ";");
                for (int i = 0; i < listaPosicionesAgrupacionExcel.size(); i++) {
                  if (linea[listaPosicionesAgrupacionExcel.get(i) - 1]
                      .equals(lineaAnterior[listaPosicionesAgrupacionExcel.get(i) - 1])) {
                    mismosCamposAgrupacionExcel = true;
                  }
                  else {
                    mismosCamposAgrupacionExcel = false;
                    break;
                  }
                }
                if (mismosCamposAgrupacionExcel) {
                  for (int i = 0; i < listaPosicionesAgrupacionExcel.size(); i++) {
                    linea[listaPosicionesAgrupacionExcel.get(i) - 1] = "";
                  }
                  for (int i = 0; i < linea.length; i++) {
                    if (linea.length == (i + 1)) {
                      outAgrupExcel.print(linea[i]);
                    }
                    else {
                      outAgrupExcel.print(linea[i] + ";");
                    }
                  }
                  outAgrupExcel.print("\n");
                }
                else {
                  outAgrupExcel.println(lineaf);
                }
              }
              lineafAnterior = lineaf;
              countLineas = countLineas.add(BigInteger.ONE);
            }
          }
        }
      }

      File file2 = new File(rutaInformes + filters.getNameFile() + "2.csv");
      file2.delete();

      File file3 = new File(rutaInformes + filters.getNameFile() + "3.csv");
      file3.delete();

      if (null != listaPosicionesAgrupacionExcel && listaPosicionesAgrupacionExcel.size() > 0) {
        File file4 = new File(rutaInformes + filters.getNameFile() + "4.csv");
        file4.delete();
      }

      String email = tmct0UsersBo.getByUsername(codigoUsuario).getEmail();
      List<String> emails = new ArrayList<String>();
      List<String> nombreFicheros = new ArrayList<String>();
      nombreFicheros.add("reporte.zip");
      emails.add(email);
      String body = "El informe " + informe.getNbdescription()
          + " se genero correctamente. Si desea visualizarlo pinche <a href=\'" + urlInforme + filters.getNameFile()
          + ".csv\'>aqui</a>";

      sendMail.sendMailHtml(emails, "Informe " + informe.getNbdescription(), body, null, null, null);

    }
    catch (Exception e) {
      LOG.error("Excepcion leyendo fichero " + filters.getNameFile() + "1.csv" + ": " + e.getMessage(), e);
    }

  }

  private String getFormatoNumero(String valor, Character precisionCampo, StringBuilder formato) {

    if (precisionCampo.toString().trim().length() != 0 && Character.isDigit(precisionCampo)) {
      if (null != valor && !"".equals(valor)) {
        if (Integer.valueOf(precisionCampo.toString()) > 0) {
          formato = new StringBuilder("#,##0.");
          for (int j = 0; j < Integer.valueOf(precisionCampo.toString()); j++) {
            formato.append("0");
          }
        }
        else {
          formato = new StringBuilder("#,##0");
        }
        valor = new DecimalFormat(formato.toString()).format(new BigDecimal(valor).doubleValue()).replaceAll(",", "#");
        valor = valor.replaceAll("\\.", ",");
        valor = valor.replaceAll("#", "\\.");
      }
      return valor;
    }

    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    DateFormat parserTimestamp = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
    DateFormat formatterTimestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    try {
      valor = formatterTimestamp.format((Timestamp) parserTimestamp.parse(valor));
    }
    catch (Exception e) {
      try {
        valor = formatter.format((Date) parser.parse(valor));
      }
      catch (Exception e1) {
        try {
          new BigInteger(valor).longValue();
          valor = valor.replaceAll(",", "#");
          valor = valor.replaceAll("\\.", ",");
          valor = valor.replaceAll("#", "\\.");
        }
        catch (Exception e2) {
          if (valor.indexOf("string") > -1) {
            valor = valor.replaceAll("string", "");
          }
        }
      }
    }

    return valor;
  }

  /**
   * Devuelve resultados generados por la query.
   * @param filters filters
   * @return List<Tmct0InformeGeneradoDTO>
   * @throws Exception
   * @throws ParseException
   */
  private Integer getResultadosQueryInforme(Tmct0GeneracionInformeFilterDTO filters, Tmct0Informes informe,
      int posicionMercado, Tmct0InformeDTONEW plantilla) throws ParseException, Exception {

    Integer numRows = 0;
    List<Date> lFechasEjecucion = new ArrayList<>();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    if (StringUtils.isNotBlank(informe.getTmct0InformesClases().getFechaBase())
        && informe.getTmct0InformesClases().getQueryPorDia().equals("SI")) {
      // Se buscan indices de componentes de fecha.
      int indexFechaRango = GeneracionInformesHelper.getIndexComponenteGenericoCampoFECHARango(
          filters.getListComponenteDirectivaDTO(), informe, posicionMercado);
      int indexFechaSinRango = GeneracionInformesHelper.getIndexComponenteGenericoCampoFECHASinRango(
          filters.getListComponenteDirectivaDTO(), informe, posicionMercado);

      if (indexFechaRango != -1) {
        // En caso de que el componente generico tenga un tipo FECHA CON RANGO
        // Si tiene rango DESDE-HASTA solo toma el primero.
        ComponenteDirectivaDTO compDirDTO = null;
        for (List<ComponenteDirectivaDTO> compDirDTOElemList : filters.getListComponenteDirectivaDTO()) {
          compDirDTO = compDirDTOElemList.get(indexFechaRango);
          break;
        }

        // Lo ejecuto por dias.
        // Se obtienen las fechas de ejecucion
        // CASO 3 - si hay fecha desde-hasta con rango de fechas se ejecuta ese
        // rango
        // CASO 4 - si hay fecha desde y no hasta se ejecuta desde esa fecha
        // hasta la actual
        lFechasEjecucion = GeneracionInformesHelper.getFechasEjecucion(StringUtils.isNotBlank(compDirDTO
            .getValorCampo()) ? sdf.parse(compDirDTO.getValorCampo()) : null, StringUtils.isNotBlank(compDirDTO
            .getValorCampoHasta()) ? sdf.parse(compDirDTO.getValorCampoHasta()) : new Date());
        filters.setFechaContratacionDesde(lFechasEjecucion.get(0));
        filters.setFechaContratacionHasta(lFechasEjecucion.get(lFechasEjecucion.size() - 1));
        filters.setFechaDesdeBase(compDirDTO.getValorCampo());
        filters.setFechaHastaBase(compDirDTO.getValorCampoHasta());
        // Se llama al metodo del bo que ejecuta el informe tantas veces como
        // dias se hayan obtenido
        // Para cada dia se crea un hilo de ejecucion
        if (null == filters.getFechaHastaBase() || "".equals(filters.getFechaHastaBase())) {
          filters.setFechaHastaBase("31/12/9999");
        }
        numRows = this.ejecutaInformePorDias(lFechasEjecucion, filters, informe, plantilla);
      }
      else if (indexFechaSinRango != -1) {
        // CASO 5: Si hay solo una fecha se ejecuta para ese dia
        // Si tiene rango FECHA solo toma el primero.
        // En caso de que el componente generico tenga un tipo FECHA SIN RANGO
        ComponenteDirectivaDTO compDirDTO = null;
        for (List<ComponenteDirectivaDTO> compDirDTOElemList : filters.getListComponenteDirectivaDTO()) {
          compDirDTO = compDirDTOElemList.get(indexFechaSinRango);
          break;
        }

        // Lo ejecuto por dias.
        // Se obtienen las fechas de ejecucion
        lFechasEjecucion = GeneracionInformesHelper.getFechasEjecucion(
            StringUtils.isNotBlank(compDirDTO.getValorCampo()) ? sdf.parse(compDirDTO.getValorCampo()) : null,
            StringUtils.isNotBlank(compDirDTO.getValorCampo()) ? sdf.parse(compDirDTO.getValorCampo()) : null);
        filters.setFechaContratacionDesde(lFechasEjecucion.get(0));
        filters.setFechaContratacionHasta(lFechasEjecucion.get(lFechasEjecucion.size() - 1));
        filters.setFechaDesdeBase(compDirDTO.getValorCampo());
        filters.setFechaHastaBase(compDirDTO.getValorCampoHasta());
        // Se llama al metodo del bo que ejecuta el informe tantas veces como
        // dias se hayan obtenido
        // Para cada dia se crea un hilo de ejecucion
        if (null == filters.getFechaHastaBase() || "".equals(filters.getFechaHastaBase())) {
          filters.setFechaHastaBase("31/12/9999");
        }
        numRows = this.ejecutaInformePorDias(lFechasEjecucion, filters, informe, plantilla);
      }
      else {
        // CASO 1 - No hay filtros de fecha se ejecuta en un hilo
        // Lo ejecuto total
        numRows = this.ejecutaInforme(filters, informe, plantilla);
      }
    }
    else {
      // CASO 1 - No hay filtros de fecha se ejecuta en un hilo
      // Lo ejecuto total
      numRows = this.ejecutaInforme(filters, informe, plantilla);
    }

    filters.setFechasEjecucion(lFechasEjecucion);

    return numRows;

  }

  /**
   * Procesamiento de ficheros para Task Batch.
   * @param tmct0InformesPlanificacion
   * @param result
   * @param plantilla
   * @param Tmct0InformeGeneradoDTO
   * @param isEnvioListaContactos
   * @param listInfoMailContactosDTO
   * @throws SIBBACBusinessException
   * @throws IOException
   * @throws MessagingException
   * @throws IllegalArgumentException
   */
  private synchronized void procesarFicherosTaskBatch(Tmct0InformesPlanificacion tmct0InformesPlanificacion,
      Map<String, Object> result, Tmct0InformeDTONEW plantilla, Tmct0InformeGeneradoDTO dto,
      boolean isEnvioListaContactos, List<InfoMailContactosDTO> listInfoMailContactosDTO)
      throws SIBBACBusinessException, IOException, IllegalArgumentException, MessagingException {
    if (tmct0InformesPlanificacion != null && tmct0InformesPlanificacion.getTipoDestino() != null) {

      if (tmct0InformesPlanificacion.isTipoDestinoRUTA()) {
        /* TIPO DESTINO RUTA */
        String folderOut = StringUtils.isNotBlank(tmct0InformesPlanificacion.getDestino()) ? tmct0InformesPlanificacion
            .getDestino() : "";
        StringBuffer sbTituloFichero = new StringBuffer();
        // Aqui se genera el fichero en una ruta fisica.
        SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyyhhmmss");
        String fechaFormatted = formatDate.format(new Date());
        sbTituloFichero.append(fechaFormatted);
        if (result.get("file") != null) {
          FileUtils.writeByteArrayToFile(new File(folderOut + tmct0InformesPlanificacion.getNombreFichero()),
              (byte[]) result.get("file"));
        }
        if (result.get("fileSimpleXLS") != null) {
          FileUtils.writeByteArrayToFile(new File(folderOut + tmct0InformesPlanificacion.getNombreFichero()),
              (byte[]) result.get("fileSimpleXLS"));
        }
      }
      else if (tmct0InformesPlanificacion.isTipoDestinoEMAIL()) {
        /* TIPO DESTINO EMAIL */
        List<InputStream> listAttachment = new ArrayList<InputStream>();
        if (result.get("file") != null) {
          InputStream inputStreamFile = new ByteArrayInputStream((byte[]) result.get("file"));
          listAttachment.add(inputStreamFile);
        }
        if (result.get("fileSimpleXLS") != null) {
          InputStream inputStreamFile = new ByteArrayInputStream((byte[]) result.get("fileSimpleXLS"));
          listAttachment.add(inputStreamFile);
        }
        List<String> toMail = new ArrayList<String>();
        if (isEnvioListaContactos && CollectionUtils.isNotEmpty(listInfoMailContactosDTO)) {
          // Envio de informes a todos los contactos de los alias autorizados
          // para recibir informes
          for (InfoMailContactosDTO infDTO : listInfoMailContactosDTO) {
            // TODO - Corroborar funcionamiento.
            List<InputStream> listAttachmentAlias = new ArrayList<InputStream>();
            if (result.get("file_" + infDTO.getIdAlias()) != null) {
              InputStream inputStreamFileAlias = new ByteArrayInputStream((byte[]) result.get("file_"
                  + infDTO.getIdAlias()));
              listAttachmentAlias.add(inputStreamFileAlias);
            }
            String[] subjBodyArr = infDTO.getInformesSubjectAndBody().split("\\|\\|");
            toMail = new ArrayList<String>();
            toMail.add(infDTO.getEmailContacto());
            if (subjBodyArr != null && subjBodyArr.length > 0) {
              // TODO - Corroborar funcionamiento
              List<String> listaNameDest = new ArrayList<String>();
              listaNameDest.add(tmct0InformesPlanificacion.getNombreFichero() + ".XLSX");
              try {
                this.sendMail.sendMail(
                    toMail,
                    GeneracionInformesHelper.getPlantillasMailFormateadas(subjBodyArr[0],
                        tmct0InformesPlanificacion.getTmct0Informes()),
                    GeneracionInformesHelper.getPlantillasMailFormateadas(subjBodyArr[1],
                        tmct0InformesPlanificacion.getTmct0Informes()), listAttachmentAlias, listaNameDest, null);
              }
              catch (Exception e) {
                // En caso de error en envio de mail no se corta ejecucion.
                LOG.error("Se ha producido un error al realizar el envío de email - " + e.getMessage());
                if (e.getMessage().contains("size exceeds")) {
                  try {
                    this.sendMail.sendMail(toMail, toMailSubjectBatchEsp,
                        "Se ha producido un error al realizar el envío de email - " + e.getMessage(), null, null, null);
                  }
                  catch (Exception e1) {
                    LOG.error("Se ha producido un error al realizar el envío de email - " + e1.getMessage());
                  }
                }
              }
            }
          }
        }
        else {
          if (StringUtils.isNotBlank(tmct0InformesPlanificacion.getDestino())) {
            List<String> listaNameDest = new ArrayList<String>();
            String[] toMailArray = tmct0InformesPlanificacion.getDestino().split(";", -1);
            for (int i = 0; i < toMailArray.length; i++) {
              toMail.add(toMailArray[i]);
            }
            if (tmct0InformesPlanificacion.getFormato().equals("TEXTO PLANO")) {
              listaNameDest.add(tmct0InformesPlanificacion.getNombreFichero());
            }
            else {
              listaNameDest.add(tmct0InformesPlanificacion.getNombreFichero() + ".XLSX");
            }
            try {
              this.sendMail.sendMail(toMail, toMailSubjectBatchEsp, toMailBodyEsp, listAttachment, listaNameDest, null);
            }
            catch (Exception e) {
              // En caso de error en envio de mail no se corta ejecucion.
              LOG.error("Se ha producido un error al realizar el envío de email - " + e.getMessage());
              if (e.getMessage().contains("size exceeds")) {
                try {
                  this.sendMail.sendMail(toMail, toMailSubjectBatchEsp,
                      "Se ha producido un error al realizar el envío de email - " + e.getMessage(), null, null, null);
                }
                catch (Exception e1) {
                  LOG.error("Se ha producido un error al realizar el envío de email - " + e1.getMessage());
                }
              }
            }
          }
          else {
            GenericLoggers
                .logError(
                    InformesBatchProcessor.class,
                    Constantes.GENERACION_INFORMES,
                    "procesarFicherosTaskBatch",
                    "No se ha podido enviar el mail ya que no hay dirección de e-mail en la planificación "
                        + " para informe con id = "
                        + (tmct0InformesPlanificacion != null ? (tmct0InformesPlanificacion.getTmct0Informes() != null ? tmct0InformesPlanificacion
                            .getTmct0Informes().getId() : null)
                            : null));
          }
        }
      }
    }
  }

  /**
   * Se obtiene info de los mails de contacto.
   * @param tmct0InformesPlanificacion tmct0InformesPlanificacion
   * @param Tmct0InformeGeneradoDTO dto
   * @throws SIBBACBusinessException SIBBACBusinessException
   * @return List<InfoMailContactosDTO> List<InfoMailContactosDTO>
   */
  public List<InfoMailContactosDTO> getInfoMailsContacto(Tmct0InformesPlanificacion tmct0InformesPlanificacion,
      Tmct0InformeGeneradoDTO dto) throws SIBBACBusinessException {

    List<InfoMailContactosDTO> listInfoMailContactosDTO = new ArrayList<InfoMailContactosDTO>();
    if (dto != null && CollectionUtils.isNotEmpty(dto.getResultados())) {
      Tmct0SibbacConstantes ctteBatchInformes = this.tmct0SibbacConstantesDao.findByClaveAndValor(
          Constantes.CLAVE_JOB_GENERAR_INFORMES, Constantes.VALOR_JOB_GENERAR_INFORMES);
      for (List<Tmc0InformeCeldaDTO> listInfCeldaDTO : dto.getResultados()) {
        if (StringUtils.isNotBlank((String) listInfCeldaDTO.get(0).getContenido())
            && StringUtils.isNumeric((String) listInfCeldaDTO.get(0).getContenido())) {
          List<Object[]> datosMailContacto = this.contactoDao.findDatosMailContactoByAlias((String) listInfCeldaDTO
              .get(0).getContenido(), Constantes.PREFIX_TIPO + tmct0InformesPlanificacion.getTmct0Informes().getId()
              + Constantes.SUFFIX_TIPO_SUBJECT, Constantes.PREFIX_TIPO
              + tmct0InformesPlanificacion.getTmct0Informes().getId() + Constantes.SUFFIX_TIPO_BODY, ctteBatchInformes
              .getValor());
          if (CollectionUtils.isNotEmpty(datosMailContacto)) {
            listInfoMailContactosDTO.addAll(this.tmct0ContactoAssembler.getListaInfoMailContactos(datosMailContacto));
          }
        }
      }
    }
    return listInfoMailContactosDTO;
  }

  /**
   * Logger utilizado para la generacion de informes.
   * @param filters
   * @param informe
   */
  private void logInforme(Tmct0GeneracionInformeFilterDTO filters, String informe) {

    String mensajeLog = "Lanzado informe " + informe + " (" + filters.getIdInforme() + ") ";

    if (filters.getOpcionesEspeciales() != null) {
      if (filters.getOpcionesEspeciales().equals("ALL")) {
        mensajeLog += ", Opciones especiales: Incluir todos";
      }
      else if (filters.getOpcionesEspeciales().equals("EXCLROUTING")) {
        mensajeLog += ", Opciones especiales: Excluir routing (ALIAS NOT IN " + ROUTING + ")";
      }
      else if (filters.getOpcionesEspeciales().equals("EXCLBARRID")) {
        mensajeLog += ", Opciones especiales: Excluir barridos de derechos (ALIAS NOT IN " + BARRIDO + ")";
      }
    }
    LOG.info(mensajeLog);
  }

  /**
   * Creacion de informe del Excel.
   * @param informe informe
   * @param getTerceros getTerceros
   * @param getSimple getSimple
   * @param result result
   * @param filters filters
   * @param lDatosFinales lDatosFinales
   * @param bGenerarExcel bGenerarExcel
   * @param Tmct0InformeGeneradoDTO dto
   * @param isEnvioListaContactos
   * @param listInfoMailContactosDTO
   * @throws SIBBACBusinessException SIBBACBusinessException
   * @throws IOException
   * @throws InvalidFormatException
   * @throws MessagingException
   * @throws IllegalArgumentException
   */
  private Map<String, Object> crearInformeExcel(Tmct0Informes informe, boolean getTerceros, boolean getSimple,
      Map<String, Object> result, Tmct0GeneracionInformeFilterDTO filters,
      List<List<Tmc0InformeCeldaDTO>> lDatosFinales, boolean bGenerarExcel, Tmct0InformeGeneradoDTO dto,
      boolean isEnvioListaContactos, List<InfoMailContactosDTO> listInfoMailContactosDTO, String codigoUsuario,
      Date dateInicioEjecucion, Tmct0InformesPlanificacion tmct0InformesPlanificacion) throws SIBBACBusinessException,
      IOException, InvalidFormatException, IllegalArgumentException, MessagingException {

    Workbook workbookSimple = new SXSSFWorkbook(1000);
    Workbook workbookAlias = new SXSSFWorkbook(1000);

    // TODO - Corroborar funcionamiento
    if (bGenerarExcel && isEnvioListaContactos && CollectionUtils.isNotEmpty(listInfoMailContactosDTO)) {

      // Funcionalidad de envio de mails de contacto por alias
      LOG.debug("Envio por alias de contactos: Se va a crear una excel con: " + lDatosFinales.size() + " Filas");

      boolean reportConSeparacion = this.isConSeparacion(informe);
      // Solo se genera informe para reportes con campos de separacion
      if (reportConSeparacion) {
        LOG.debug("Es con separacion");
        ByteArrayOutputStream bos;
        for (InfoMailContactosDTO infMailCntDTO : listInfoMailContactosDTO) {

          if (getSimple) {
            LOG.debug("LLAMA A crearInformePorHojas SIMPLE - ALIAS");
            bos = new ByteArrayOutputStream();
            crearInformePorHojas(filters, dto, informe, false, infMailCntDTO.getIdAlias(), workbookAlias,
                tmct0InformesPlanificacion);
            workbookAlias.write(bos);
            LOG.debug("FIN crearInformePorHojas SIMPLE - ALIAS");
          }

          LOG.debug("FIN CREACION INFORME ALIAS");
          if (workbookAlias != null) {
            LOG.debug("CREACION  ByteArrayOutputStream del workbookAlias");
            bos = new ByteArrayOutputStream();
            try {
              workbookAlias.write(bos);

            }
            catch (IOException e) {
              LOG.error("Error ", e.getClass().getName(), e.getMessage());
            }

            // El report de terceros se genera siempre y se guarda en el objeto
            // file
            result.put("file_" + infMailCntDTO.getIdAlias(), bos.toByteArray());
          }
          workbookAlias.close();
        }
      }

    }
    else if (bGenerarExcel && dto.getResultados() != null && !dto.getResultados().isEmpty()) {

      LOG.debug("Se va a crear una excel con: " + lDatosFinales.size() + " Filas");
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      boolean reportConSeparacion = this.isConSeparacion(informe);

      if (reportConSeparacion) {
        LOG.debug("Es con separacion");

        if (getSimple) {
          LOG.debug("LLAMA A crearInformePorHojas SIMPLE");
          crearInformePorHojas(filters, dto, informe, false, null, workbookSimple, null);
          workbookSimple.write(bos);
          result.put("fileSimpleXLS", bos.toByteArray());
          LOG.debug("FIN crearInformePorHojas SIMPLE");
        }

        if (getTerceros) {
          LOG.debug("Genera la excel de TERCEROS a partir de la excel normal");
          try {

            if (getSimple) {
              InputStream miInput = new ByteArrayInputStream(bos.toByteArray());
              workbookSimple = WorkbookFactory.create(miInput);
            }
            else {
              crearInformePorHojas(filters, dto, informe, false, null, workbookSimple, null);
              workbookSimple.write(bos);
            }

            // Se inserta la cabecera.

            int nHojas = workbookSimple.getNumberOfSheets();

            InputStream miInput = new ByteArrayInputStream(bos.toByteArray());
            workbookSimple = WorkbookFactory.create(miInput);
            miInput.close();

            for (int i = 0; i < nHojas; i++) {
              int iNumFilas = workbookSimple.getSheetAt(i).getLastRowNum();
              workbookSimple.getSheetAt(i).shiftRows(0, iNumFilas, 9);
              createHeaderTerceros(workbookSimple, workbookSimple.getSheetAt(i), informe, filters, 0);
            }
            bos = new ByteArrayOutputStream();
            workbookSimple.write(bos);
            result.put("file", bos.toByteArray());
          }
          catch (Exception e) {
            LOG.error("Se produjo un error en la generación del fichero Excel " + e.getMessage());
          }
        }
        bos.close();
        workbookSimple.close();

      }
      else {

        LOG.debug("Es SIN separacion");
        if (getSimple) {
          LOG.debug("LLAMA A crearInformeSolaHoja SIMPLE");
          crearInformeSolaHoja(filters, dto, informe, false, workbookSimple);
          workbookSimple.write(bos);
          result.put("fileSimpleXLS", bos.toByteArray());
          LOG.debug("FIN crearInformeSolaHoja SIMPLE");
        }

        if (getTerceros) {

          LOG.debug("Genera la excel de TERCEROS a partir de la excel normal");
          try {

            if (getSimple) {
              InputStream miInput = new ByteArrayInputStream(bos.toByteArray());
              workbookSimple = WorkbookFactory.create(miInput);

              int iNumFilas = workbookSimple.getSheetAt(0).getLastRowNum();
              workbookSimple.getSheetAt(0).shiftRows(0, iNumFilas, 9);

              createHeaderTerceros(workbookSimple, workbookSimple.getSheetAt(0), informe, filters, 0);

            }
            else {
              crearInformeSolaHoja(filters, dto, informe, true, workbookSimple);
            }

            bos = new ByteArrayOutputStream();
            workbookSimple.write(bos);
            result.put("file", bos.toByteArray());
          }
          catch (Exception e) {
            LOG.error("Se produjo un error en la generación del fichero Excel " + e.getMessage());
          }
          LOG.debug("FIN crearInformeSolaHoja TERCEROS");
        }

        bos.close();
        workbookSimple.close();

      }
    }

    return result;
  }

  /**
   * Crea el reporte en excel en varias hojas.
   * @param filters
   * @param dto
   * @param informe
   * @param isTerceros
   * @param idAlias
   * @return XSSFWorkbook
   */
  private void crearInformePorHojas(Tmct0GeneracionInformeFilterDTO filters, Tmct0InformeGeneradoDTO dto,
      Tmct0Informes informe, Boolean isTerceros, String idAlias, Workbook workbook,
      Tmct0InformesPlanificacion tmct0InformesPlanificacion) {

    TreeMap<String, List<List<Tmc0InformeCeldaDTO>>> resultados = new TreeMap<String, List<List<Tmc0InformeCeldaDTO>>>();

    XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
    Font font = workbook.createFont();
    GeneracionInformesHelper.createWorkbookStyles(workbook, style, font);

    List<String> colsSeparacion = this.getColumnasSeparacion(informe);

    // TODO - Corroborar funcionamiento
    if (StringUtils.isNotBlank(idAlias)) {
      // Inicio - Genero las combinaciones de páginas según los separadores -
      // hay filtrado por id de alias
      if (dto.getResultados() != null && !dto.getResultados().isEmpty()) {
        for (List<Tmc0InformeCeldaDTO> result : dto.getResultados()) {
          String key = "";
          for (Tmc0InformeCeldaDTO column : result) {
            if (colsSeparacion.contains(column.getColumna()) && idAlias.trim().equals(column.getValor())) {
              if (key.isEmpty()) {
                key += column.getContenido();
              }
              else {
                key += " / " + column.getContenido();
              }
            }
          }
          if (resultados.get(key) == null && StringUtils.isNotBlank(key)) {
            resultados.put(key, new ArrayList<List<Tmc0InformeCeldaDTO>>());
            resultados.get(key).add(result);
          }
        }
      }
      // Fin - Genero las combinaciones de páginas según los separadores - hay
      // filtrado por id de alias
    }
    else {
      // Genero las combinaciones de páginas según los separadores
      if (dto.getResultados() != null && !dto.getResultados().isEmpty()) {
        for (List<Tmc0InformeCeldaDTO> result : dto.getResultados()) {
          String key = "";
          for (Tmc0InformeCeldaDTO column : result) {
            if (colsSeparacion.contains(column.getColumna())) {
              if (key.isEmpty()) {
                key += column.getContenido();
              }
              else {
                key += " / " + column.getContenido();
              }
            }
          }
          if (resultados.get(key) == null) {
            resultados.put(key, new ArrayList<List<Tmc0InformeCeldaDTO>>());
          }
          resultados.get(key).add(result);
        }
      }
    }

    int rowNum = 0;
    for (String page : resultados.keySet()) {

      rowNum = 0;
      String namePage = page.replace(" / ", "_").trim().replace(":", "").trim();

      // Se controla que el nombre de la página tenga una longitud entre 1 y 31
      if (namePage.length() < 1) {
        namePage = "<<NULL>>";
      }
      else if (namePage.length() > 31) {
        namePage = namePage.substring(0, 28) + "...";
      }

      // En caso de que ya exista la hoja Excel no debe crearse
      // sino que se debe recobrar.
      Sheet sheet = null;
      if (workbook.getSheet(namePage) == null) {
        sheet = workbook.createSheet(namePage);
      }
      else {
        sheet = workbook.getSheet(namePage);
      }

      if (isTerceros) {
        rowNum = this.createHeaderTerceros(workbook, sheet, informe, filters, rowNum);
      }

      CellStyle style2 = workbook.createCellStyle();
      font = workbook.createFont();
      font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
      style2.setFont(font);
      sheet.createRow(rowNum).createCell(0).setCellValue(page);
      sheet.getRow(rowNum).getCell(0).setCellStyle(style2);

      rowNum++;
      if (null == tmct0InformesPlanificacion
          || (null != tmct0InformesPlanificacion && !tmct0InformesPlanificacion.getTipoDestino().equals(
              "EMAIL SIN CABECERA"))) {
        rowNum = GeneracionInformesHelper.createColumnsHeaders(workbook, sheet, dto.getCabeceras(), rowNum);
      }

      rowNum = GeneracionInformesHelper.createResults(workbook, sheet, resultados.get(page), style, font, rowNum);

      HashMap<String, Double> totales = new HashMap<String, Double>();
      for (List<Tmc0InformeCeldaDTO> resultado : resultados.get(page)) {
        int i = 0;
        for (Tmc0InformeCeldaDTO columna : resultado) {
          if (dto.getColumnas().get(i).getTotalizar() != 'N') {

            String key = dto.getColumnas().get(i).getCabecera();
            Double total = 0.0;
            if (null != totales.get(key)) {
              total = totales.get(key);
            }
            String valorSinComaPunto = (String.valueOf(columna.getContenido()).replace(".", "").trim().replace(",", "")
                .trim());
            if (total != null && NumberUtils.isNumber(valorSinComaPunto)) {
              total = total
                  + new Double(String.valueOf(columna.getContenido()).replace(".", "").trim().replace(",", ".").trim());
              totales.put(key, total);
            }
          }
          i++;
        }
      }

      if (informe.getTotalizar().equals("S")) {
        GeneracionInformesHelper.createTotals(workbook, sheet, dto.getCabeceras(), totales, style, rowNum + 1);
      }
    }
  }

  /**
   * Se crea la cabecera para el reporte de terceros.
   * @param workbook
   * @param sheet
   * @param informe
   * @param filters
   * @param style
   * @param rowNum
   * @return int
   * @throws SIBBACBusinessException
   */
  public int createHeaderTerceros(Workbook workbook, Sheet sheet, Tmct0Informes informe,
      Tmct0GeneracionInformeFilterDTO filters, Integer rowNum) {

    try {

      FileInputStream stream = new FileInputStream(TEMPLATES_FOLDER + "/LogotipoGlobalBanking.png");
      CreationHelper helper = workbook.getCreationHelper();
      Drawing drawing = sheet.createDrawingPatriarch();
      ClientAnchor anchor = helper.createClientAnchor();
      anchor.setAnchorType(ClientAnchor.MOVE_AND_RESIZE);
      int pictureIndex;
      if (workbook instanceof XSSFWorkbook) {
        pictureIndex = ((XSSFWorkbook) workbook).addPicture(stream, Workbook.PICTURE_TYPE_PNG);
      }
      else {
        InputStream inputStream = new FileInputStream(TEMPLATES_FOLDER + "/LogotipoGlobalBanking.png");
        byte[] bytes = IOUtils.toByteArray(inputStream);
        pictureIndex = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
      }

      anchor.setCol1(0);
      anchor.setRow1(0);

      Picture pict = drawing.createPicture(anchor, pictureIndex);
      pict.resize(1.5, 3.0);

    }
    catch (FileNotFoundException e1) {
      LOG.error("No se pudo recuperar la imagen de cabecera");
    }
    catch (IOException e) {
      LOG.error("No se pudo recuperar la imagen de cabecera");
    }
    catch (Exception e) {
      LOG.error("No se pudo recuperar la imagen de cabecera");
    }
    finally {

      SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_ES);
      // Internacionalizar textos
      String cell1 = "De:";
      String cell2 = "Informe:";
      String cell3 = "Fecha:";
      if (informe.getNbidioma().equals("INGLES")) {
        cell1 = "From:";
        cell2 = "Report:";
        cell3 = "Date:";
      }

      rowNum = rowNum + 5;
      Row row = sheet.createRow(rowNum);
      row.createCell(0).setCellValue(cell1);
      row.createCell(1).setCellValue("BANCO SANTANDER S.A.");
      rowNum++;

      row = sheet.createRow(rowNum);
      row.createCell(0).setCellValue(cell2);
      row.createCell(1).setCellValue(informe.getNbdescription());
      CellStyle style2 = workbook.createCellStyle();
      Font font = workbook.createFont();
      font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
      style2.setFont(font);
      row.getCell(1).setCellStyle(style2);

      rowNum++;

      row = sheet.createRow(rowNum);

      if (null != filters.getFechaContratacionDesde()) {
        row.createCell(0).setCellValue(cell3);

        String texto = "Desde " + formatter.format(filters.getFechaContratacionDesde());
        if (filters.getFechaContratacionHasta() != null) {
          texto += " hasta " + formatter.format(filters.getFechaContratacionHasta());
        }
        row.createCell(1).setCellValue(texto);
      }
      // Se inserta una línea vacía
      rowNum++;
      rowNum++;
    }

    return rowNum;

  }

  /**
   * Crea el reporte en excel en una única hoja
   * 
   * @param filters dto informe isTerceros
   * @return
   * @throws SIBBACBusinessException
   */
  private void crearInformeSolaHoja(Tmct0GeneracionInformeFilterDTO filters, Tmct0InformeGeneradoDTO dto,
      Tmct0Informes informe, Boolean isTerceros, Workbook workbook) {

    LOG.debug("INICIO crearInformeSolaHoja");
    Integer resultsStartRowNum = 0;
    Integer rowNum = 0;

    try {

      XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
      Font font = workbook.createFont();
      GeneracionInformesHelper.createWorkbookStyles(workbook, style, font);

      Sheet sheet = workbook.createSheet("Informe");

      if (isTerceros) {
        LOG.debug("LLAMA createHeaderTerceros DENTRO TERCEROS");
        rowNum = this.createHeaderTerceros(workbook, workbook.getSheetAt(0), informe, filters, rowNum);
        LOG.debug("FIN createHeaderTerceros DENTRO TERCEROS");
      }

      LOG.debug("LLAMA createHeaderTerceros");
      rowNum = GeneracionInformesHelper.createColumnsHeaders(workbook, sheet, dto.getCabeceras(), rowNum);
      LOG.debug("FIN createHeaderTerceros");
      resultsStartRowNum = rowNum;

      LOG.debug("LLAMA createResults");
      rowNum = GeneracionInformesHelper.createResults(workbook, sheet, dto.getResultados(), style, font, rowNum);
      LOG.debug("FIN createResults");

      if (informe.getTotalizar().equals("S")) {
        LOG.debug("LLAMA createTotals");
        rowNum = GeneracionInformesHelper.createTotals(workbook, sheet, dto.getCabeceras(), dto.getTotales(), style,
            rowNum + 1);
        LOG.debug("FIN createTotals");
      }

      LOG.debug("Autosize columans final");

      Row row = sheet.getRow(resultsStartRowNum + 1);
      for (int colNum = 0; colNum < row.getLastCellNum(); colNum++)
        try {
          sheet.autoSizeColumn(colNum);
        }
        catch (Exception e) {
          LOG.debug("No se pudo ajustar una columna");
        }

    }
    catch (Exception e) {
      LOG.debug("Excepción " + e.getMessage());
    }

    LOG.debug("FIN crearInformeSolaHoja");
  }

  /**
   * Creacion de informe del Excel.
   *
   * @param result result
   * @param lDatosFinales lDatosFinales
   * @param bGenerarFichero bGenerarFichero
   * @param Tmct0InformeGeneradoDTO dto
   * @param Tmct0InformesPlanificacion tmct0InformesPlanificacion
   * @throws SIBBACBusinessException SIBBACBusinessException
   * @throws IOException
   */
  private synchronized Map<String, Object> crearInformeTexto(Map<String, Object> result,
      List<List<Tmc0InformeCeldaDTO>> lDatosFinales, boolean bGenerarFichero, Tmct0InformeGeneradoDTO dto,
      Tmct0InformesPlanificacion tmct0InformesPlanificacion) throws SIBBACBusinessException, IOException {

    if (bGenerarFichero && CollectionUtils.isNotEmpty(dto.getResultados())) {
      LOG.debug("Se va a crear un fichero de texto con: " + lDatosFinales.size() + " Filas");
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      List<String> contenidoFichero = GeneracionInformesHelper.getContenidoTXTForBatch(dto.getResultados(),
          tmct0InformesPlanificacion);
      DataOutputStream out = new DataOutputStream(bos);
      for (String fila : contenidoFichero) {
        out.writeBytes(fila);
      }
      result.put("file", bos.toByteArray());
    }
    return result;
  }

  /**
   * Devuelve true si se genera el informe para la task.
   * @param tmct0InformesPlanificacion tmct0InformesPlanificacion
   * @return boolean boolean
   * 
   */
  public boolean isGeneraInformeForTask(Tmct0InformesPlanificacion tmct0InformesPlanificacion) {
    boolean isGeneraInformeForTask = false;
    // Caso en que valida que haya filtros tipo FECHA con valor precargado
    if (CollectionUtils.isNotEmpty(this.tmct0InformesDao
        .findTipoFechaConValorFiltroByInformeId(tmct0InformesPlanificacion.getTmct0Informes().getId()))) {
      return true;
    }

    // Si hay filtros y ninguno es de tipo FECHA se corre la query plana
    if (CollectionUtils.isNotEmpty(this.tmct0InformesDao
        .findTipoDistintoFechaConValorFiltroByInformeId(tmct0InformesPlanificacion.getTmct0Informes().getId()))
        && CollectionUtils.isEmpty(this.tmct0InformesDao.findFiltrosTipoFechaByInformeId(tmct0InformesPlanificacion
            .getTmct0Informes().getId()))) {
      return true;
    }
    return isGeneraInformeForTask;
  }

  public Map<String, Object> modificarPlantilla(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    Map<String, Object> map = new HashMap<String, Object>();

    String mercado = String.valueOf(((Map<String, String>) paramsObject.get("mercado")).get("key"));
    Integer idClase = Integer.parseInt(((Map<String, String>) paramsObject.get("clase")).get("id"));
    String clase = String.valueOf(((Map<String, String>) paramsObject.get("clase")).get("clase"));
    Tmct0InformeDTONEW miDTO = tmct0InformeAssembler.getTmct0InformeDto(paramsObject);

    // Comprobacion que el identificador de informe sea valido
    if (miDTO.getIdInforme() == 0) {
      LOG.error("No se ha podido modificar el informe  ya que el id es cero, no se han recibido bien los datos -  ");
      throw new SIBBACBusinessException(
          "No se puede modificar el informe ya que el id es cero, no se han recibido bien los datos ");
    }

    // Comprobacion que no exista ya un informe con ese nombre que no sea el que
    // se esta intentando modificar
    List<Tmct0Informes> listInformes = tmct0InformesDao.findBynbdescriptionAndEstado(miDTO.getNbdescription());
    if (listInformes.size() > 0) {
      for (Tmct0Informes informe : listInformes) {
        // Si el idInforme no es el mismo que el que estamos intentando
        // modificar salta excepción
        if (informe.getId().compareTo(miDTO.getIdInforme()) != 0) {
          LOG.error("Se esta intentanto modificar una plantilla con el mismo nombre que una existente -  "
              + miDTO.getNbdescription());
          throw new SIBBACBusinessException("Ya existe una plantilla con el nombre " + miDTO.getNbdescription());
        }
      }

    }

    if (miDTO.getNbdescription().contains("/")) {
      LOG.error("Se esta intentanto crear una plantilla cuyo nombre contiene el caracter / -  "
          + miDTO.getNbdescription());
      throw new SIBBACBusinessException("El nombre de la plantilla no puede contener el caracter /");
    }

    // Se borran los campos actuales del informe para formar el informe con los
    // nuevos campos seleccionados
    tmct0InformesDatosFiltrosDao.deleteInformeDatosFiltros(miDTO.getIdInforme());

    List<Integer> idsHijos = tmct0InformesDao.getIdsInformesHijos(miDTO.getIdInforme());

    if (null != mercado && !mercado.equalsIgnoreCase(EnuMercado.TODOS.getKey())) {
      // Si el mercado es distinto de TODOS se borran todos los registros de la
      // tabla TMCT0_INFORMES_DATOS que tengan el ID igual al IDINFORME del
      // padre y los hijos y los hijos de la tabla TMCT0_INFORMES
      List<Integer> idsAborrar = new ArrayList<Integer>();
      idsAborrar.add(miDTO.getIdInforme());
      if (null != idsHijos && idsHijos.size() > 0) {
        for (int i = 0; i < idsHijos.size(); i++) {
          idsAborrar.add(idsHijos.get(i));
        }
      }
      tmct0InformesDatosDao.deleteVariosInformeDatos(idsAborrar);

      if (null != idsHijos && idsHijos.size() > 0) {
        tmct0InformesTablasDao.deleteVariosInformeTablas(idsHijos);
        tmct0InformesDao.deleteVariosInformes(idsHijos);
      }

    }
    else if (null != mercado && mercado.equalsIgnoreCase(EnuMercado.TODOS.getKey())) {
      // Si el mercado es TODOS se borran todos los registros de la tabla
      // TMCT0_INFORMES_DATOS que tengan el ID igual al IDINFORME del padre y
      // los hijos
      List<Integer> idsAborrar = new ArrayList<Integer>();
      idsAborrar.add(miDTO.getIdInforme());
      if (null != idsHijos && idsHijos.size() > 0) {
        for (int i = 0; i < idsHijos.size(); i++) {
          idsAborrar.add(idsHijos.get(i));
        }
      }
      tmct0InformesDatosDao.deleteVariosInformeDatos(idsAborrar);

      if (null != idsHijos && idsHijos.size() > 0) {
        tmct0InformesTablasDao.deleteVariosInformeTablas(idsHijos);
        tmct0InformesDao.deleteVariosInformes(idsHijos);
      }
    }
    // se graba la plantilla con las nuevas consultas
    grabarPlantillaInforme(miDTO, idClase, false, Constantes.MODIFICACION);

    boolean recuperarTodosIdsClase = false;
    List<Integer> idsClase = new ArrayList<Integer>();
    Tmct0InformeDTONEW dto = tmct0InformeAssembler.getTmct0InformeDto(paramsObject);

    if (null != mercado && mercado.equalsIgnoreCase(EnuMercado.TODOS.getKey())) {
      List<Integer> listaIdsCamposDetalle = new ArrayList<Integer>();
      for (int i = 0; i < miDTO.getListaCamposDetalle().size(); i++) {
        listaIdsCamposDetalle.add(Integer.parseInt(miDTO.getListaCamposDetalle().get(i).getKey()));
      }
      List<String> listaMercadosClase = tmct0InformesCamposDao.findMercadosDistintosPorClase(clase,
          listaIdsCamposDetalle);
      for (int i = 0; i < listaMercadosClase.size(); i++) {
        if (listaMercadosClase.get(i).equals("")) {
          recuperarTodosIdsClase = true;
          break;
        }
      }

      if (recuperarTodosIdsClase) {
        idsClase = tmct0InformesClasesDao.getClasesConTodosMercadosInforme(idClase);
      }
      else {
        idsClase = tmct0InformesClasesDao.getClasesConTodosMercadosInforme(idClase, listaMercadosClase);
      }

      Integer idInforme = miDTO.getIdInforme();

      if (null != idsClase) {
        Tmct0InformeDTONEW informeHijo = null;
        for (Integer id : idsClase) {
          if (!id.equals(idClase)) {
            informeHijo = dto;
            informeHijo.setIdInforme(null);
            informeHijo.setIdClase(id);
            String nombre = informeHijo.getNbdescription() + " - " + idInforme;
            informeHijo.setNbdescription(nombre);
            informeHijo.setIdInformePadre(idInforme);
            informeHijo.setListComponenteDirectivaDTO(null);

            grabarPlantillaInforme(informeHijo, idClase, true, Constantes.MODIFICACION);
          }
        }
      }

    }

    map.put("idInforme", miDTO.getIdInforme());

    return map;
  }

  private void rellenarConCaracteres(Tmct0Informes informe, List<List<Tmc0InformeCeldaDTO>> resultados) {

    List<Tmct0InformesDatos> informesDatos = tmct0InformesDatosDao.getDatosInformeCamposDetalle(informe.getId());

    try {
      for (int i = 0; i < resultados.size(); i++) {
        List<Tmc0InformeCeldaDTO> resultado = resultados.get(i);
        for (int j = 0; j < resultado.size(); j++) {
          if (null == informesDatos.get(j).getRelleno()) {
            informesDatos.get(j).setRelleno(Character.MIN_VALUE);
          }
          if (informesDatos.get(j).getId().getIdCampo().getTipoCampo().equals("TEXTO")) {
            if (informesDatos.get(j).getRellenoTipo().equals("DERECHA")) {
              if (resultado.get(j).getContenido() instanceof BigDecimal) {
                resultado.get(j).setContenido(
                    StringHelper.rightPad(String.valueOf(((BigDecimal) resultado.get(j).getContenido()).intValue()),
                        informesDatos.get(j).getLongitudAlfanumerico(), informesDatos.get(j).getRelleno()));
                resultado.get(j).setFormato("");
              }
              else {
                if (informesDatos.get(j).getLongitudAlfanumerico() < String.valueOf(resultado.get(j).getContenido())
                    .length()) {
                  resultado.get(j).setContenido(
                      StringHelper.rightPad(
                          String.valueOf(resultado.get(j).getContenido()).substring(0,
                              informesDatos.get(j).getLongitudAlfanumerico()), informesDatos.get(j)
                              .getLongitudAlfanumerico(), informesDatos.get(j).getRelleno()));
                  resultado.get(j).setFormato("");
                }
                else {
                  resultado.get(j).setContenido(
                      StringHelper.rightPad(String.valueOf(resultado.get(j).getContenido()), informesDatos.get(j)
                          .getLongitudAlfanumerico(), informesDatos.get(j).getRelleno()));
                  resultado.get(j).setFormato("");
                }
              }
            }
            else if (informesDatos.get(j).getRellenoTipo().equals("IZQUIERDA")) {
              if (resultado.get(j).getContenido() instanceof BigDecimal) {
                resultado.get(j).setContenido(
                    StringHelper.leftPad(String.valueOf(((BigDecimal) resultado.get(j).getContenido()).intValue()),
                        informesDatos.get(j).getLongitudAlfanumerico(), informesDatos.get(j).getRelleno()));
                resultado.get(j).setFormato("");
              }
              else {
                if (informesDatos.get(j).getLongitudAlfanumerico() < String.valueOf(resultado.get(j).getContenido())
                    .length()) {
                  resultado.get(j).setContenido(
                      StringHelper.leftPad(
                          String.valueOf(resultado.get(j).getContenido()).substring(0,
                              informesDatos.get(j).getLongitudAlfanumerico()), informesDatos.get(j)
                              .getLongitudAlfanumerico(), informesDatos.get(j).getRelleno()));
                  resultado.get(j).setFormato("");
                }
                else {
                  resultado.get(j).setContenido(
                      StringHelper.leftPad(String.valueOf(resultado.get(j).getContenido()), informesDatos.get(j)
                          .getLongitudAlfanumerico(), informesDatos.get(j).getRelleno()));
                  resultado.get(j).setFormato("");
                }
              }
            }
          }
          else if (informesDatos.get(j).getId().getIdCampo().getTipoCampo().equals("NUMERO")) {
            if (resultado.get(j).getContenido() instanceof BigDecimal) {
              resultado.get(j).setContenido(
                  StringHelper.leftPad(String.valueOf(((BigDecimal) resultado.get(j).getContenido()).intValue()),
                      informesDatos.get(j).getLongitudEntero(), informesDatos.get(j).getRelleno()));
              resultado.get(j).setFormato("");
            }
            else {
              resultado.get(j).setContenido(
                  StringHelper.leftPad(String.valueOf(resultado.get(j).getContenido()), informesDatos.get(j)
                      .getLongitudEntero(), informesDatos.get(j).getRelleno()));
              resultado.get(j).setFormato("");
            }
          }
          else if (informesDatos.get(j).getId().getIdCampo().getTipoCampo().equals("DECIMAL")) {
            String[] enteroDecimal = null;
            if (resultado.get(j).getContenido().toString().contains(",")) {
              enteroDecimal = resultado.get(j).getContenido().toString().split(",");
            }
            else if (resultado.get(j).getContenido().toString().contains(".")) {
              enteroDecimal = resultado.get(j).getContenido().toString().split("\\.");
            }
            else {
              enteroDecimal = new String[1];
              enteroDecimal[0] = String.valueOf(resultado.get(j).getContenido());
            }

            if (null != enteroDecimal) {
              if (enteroDecimal.length > 1) {
                if (informesDatos.get(j).getLongitudDecimal() < enteroDecimal[1].length()) {
                  resultado.get(j).setContenido(resultado.get(j).getContenido().toString().replace(",", "."));
                  resultado.get(j).setContenido(
                      (new BigDecimal(resultado.get(j).getContenido().toString())).setScale(informesDatos.get(j)
                          .getLongitudDecimal(), BigDecimal.ROUND_HALF_UP));
                  enteroDecimal = resultado.get(j).getContenido().toString().split("\\.");
                  enteroDecimal[0] = StringHelper.leftPad(String.valueOf(enteroDecimal[0]), informesDatos.get(j)
                      .getLongitudEntero(), informesDatos.get(j).getRelleno());
                  resultado.get(j).setContenido(enteroDecimal[0] + "." + enteroDecimal[1]);
                  resultado.get(j).setFormato("");
                }
                else {
                  enteroDecimal[0] = StringHelper.leftPad(String.valueOf(enteroDecimal[0]), informesDatos.get(j)
                      .getLongitudEntero(), informesDatos.get(j).getRelleno());
                  enteroDecimal[1] = StringHelper.rightPad(String.valueOf(enteroDecimal[1]), informesDatos.get(j)
                      .getLongitudDecimal(), informesDatos.get(j).getRelleno());
                  resultado.get(j).setContenido(enteroDecimal[0] + "." + enteroDecimal[1]);
                  resultado.get(j).setFormato("");
                }
              }
              else {
                resultado.get(j).setContenido(enteroDecimal[0]);
                resultado.get(j).setFormato("");
              }

            }
          }

        }
      }
    }
    catch (Exception e) {
      LOG.error("El relleno de campos ha fallado");
    }
  }

  @Transactional
  public void eliminarInformes(Integer id) {
    Tmct0Informes informe = em.find(Tmct0Informes.class, id);
    if (informe != null) {
      informe.setEstado(Constantes.PLANTILLA_BAJA);
      em.persist(informe);
    }

  }

  private void enviarWorkbookZip(String usuario, String nombreInforme, ByteArrayOutputStream bos) {

    try {
      byte data[] = new byte[2048];
      ByteArrayOutputStream bose = new ByteArrayOutputStream();
      ZipOutputStream zos = new ZipOutputStream(bose);
      BufferedInputStream entryStream = new BufferedInputStream(new ByteArrayInputStream((byte[]) bos.toByteArray()),
          2048);
      ZipEntry entry = new ZipEntry("reporte.xlsx");
      zos.putNextEntry(entry);
      int count;
      while ((count = entryStream.read(data, 0, 2048)) != -1) {
        zos.write(data, 0, count);
      }

      List<InputStream> listAttachmentZip = new ArrayList<InputStream>();
      listAttachmentZip.add(new ByteArrayInputStream(bose.toByteArray()));

      entryStream.close();
      zos.closeEntry();
      zos.close();
      bose.close();

      String email = tmct0UsersBo.getByUsername(usuario).getEmail();
      List<String> emails = new ArrayList<String>();
      List<String> nombreFicheros = new ArrayList<String>();
      nombreFicheros.add("reporte.zip");
      emails.add(email);
      String body = "Se adjunta el informe " + nombreInforme + " generado en formato .zip";

      sendMail.sendMail(emails, "subject mail", body, listAttachmentZip, nombreFicheros, null);

    }
    catch (IllegalArgumentException | IOException | MessagingException e) {
      LOG.error("El envio del email con el informe adjunto ha fallado");
    }
    catch (Exception e) {
      LOG.error("El envio del email con el informe adjunto ha fallado");
    }

  }

  public boolean diferenciaEjecucionMenorMinuto(Date fechaEjecución) {
    // Si la fecha de ejecución es nula o vacía se lanza la planificación
    if (null == fechaEjecución || "".equals(fechaEjecución)) {
      return true;
    }
    long diferenciaMilis = new Date().getTime() - fechaEjecución.getTime();
    long diferenciaMinutos = diferenciaMilis / (60 * 1000) % 60;

    if (diferenciaMinutos < 1) {
      return false;
    }
    return true;
  }
}
