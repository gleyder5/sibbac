package sibbac.business.generacioninformes.tasks;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.SQLException;
import static sibbac.common.utils.FormatDataUtils.convertDateTimeToString;
import static sibbac.common.utils.FormatDataUtils.convertDateToString;
import static sibbac.common.utils.FormatDataUtils.convertTimeToString;

abstract class StatementExecutor {
  
  private static final Logger LOG = LoggerFactory.getLogger(StatementExecutor.class);
  
  private int executions;
  
  private int affectedRows;
  
  private int returnedRows;
  
  abstract void executeStatement(PreparedStatement stmt) throws BatchException;
  
  final String format(ResultSet rs, int pos, int type) throws SQLException {
    final Date date;

    switch(type) {
      case Types.DATE:
        date = rs.getDate(pos);
        if(rs.wasNull()) return null;
        return convertDateToString(date);
      case Types.TIME:
        date = rs.getDate(pos);
        if(rs.wasNull()) return null;
        return convertTimeToString(date);
      case Types.TIMESTAMP:
        date = rs.getDate(pos);
        if(rs.wasNull()) return null;
        return convertDateTimeToString(date);
      default:
        return rs.getString(pos);
    }
    
  }
  
  final void addAffectedRows(int rows) {
    executions++;
    affectedRows += rows;
  }
  
  final void addReturnedRows(int rows) {
    executions++;
    returnedRows += rows;
  }
  
  final void logExecutions(String source) {
    LOG.debug("[{}] Ejecuciones: {}, registros afectados: {}, registros consultados: {}", source, 
        executions, affectedRows, returnedRows); 
  }
  
  final void resetCounts() {
    executions = returnedRows = affectedRows = 0;
  }

}
