package sibbac.business.generacioninformes.database.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_INFORMES_CLASES.
 */
@Entity
@Table( name = "TMCT0_INFORMES_CLASES" )
public class Tmct0InformesClases implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6607984909846136714L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	@Column(name = "CLASE", length = 50, nullable = false)
	private String clase;
	
	@Column(name = "NOMBRE", length = 50, nullable = false)
	private String nombre;
	
	@Column(name = "MERCADO", length = 50, nullable = true)
	private String mercado;
	
	@Column(name = "TOTALIZAR", length = 2, nullable = false)
	private String totalizar;

	@Column(name = "QUERY_POR_DIA", length = 2, nullable = false)
	private String queryPorDia;
	
	@Column(name = "TABLA_BASE", length = 50, nullable = false)
	private String tablaBase;

	@Column(name = "FECHA_BASE", length = 50, nullable = true)
	private String fechaBase;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0InformesClases() {}

	/**
	 *	Constructor args.
	 *	@param id
	 *	@param clase
	 *	@param nombre
	 *	@param mercado
	 *	@param totalizar
	 *	@param queryPorDia
	 *	@param tablaBase
	 *  @param fechaBase
	 */
	public Tmct0InformesClases(Integer id, String clase, String nombre,
			String mercado, String totalizar, String queryPorDia,
			String tablaBase, String fechaBase) {
		super();
		this.id = id;
		this.clase = clase;
		this.nombre = nombre;
		this.mercado = mercado;
		this.totalizar = totalizar;
		this.queryPorDia = queryPorDia;
		this.tablaBase = tablaBase;
		this.fechaBase = fechaBase;
	}

	/**
	 *	@return id 
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 *	@param id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *	@return clase 
	 */
	public String getClase() {
		return this.clase;
	}

	/**
	 *	@param clase 
	 */
	public void setClase(String clase) {
		this.clase = clase;
	}

	/**
	 *	@return nombre 
	 */
	public String getNombre() {
		return this.nombre;
	}

	/**
	 *	@param nombre 
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 *	@return mercado 
	 */
	public String getMercado() {
		return this.mercado;
	}

	/**
	 *	@param mercado 
	 */
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}

	/**
	 *	@return totalizar 
	 */
	public String getTotalizar() {
		return this.totalizar;
	}

	/**
	 *	@param totalizar 
	 */
	public void setTotalizar(String totalizar) {
		this.totalizar = totalizar;
	}

	/**
	 *	@return queryPorDia 
	 */
	public String getQueryPorDia() {
		return this.queryPorDia;
	}

	/**
	 *	@param queryPorDia 
	 */
	public void setQueryPorDia(String queryPorDia) {
		this.queryPorDia = queryPorDia;
	}

	/**
	 *	@return tablaBase 
	 */
	public String getTablaBase() {
		return this.tablaBase;
	}

	/**
	 *	@param tablaBase 
	 */
	public void setTablaBase(String tablaBase) {
		this.tablaBase = tablaBase;
	}

	/**
	 *	@return fechaBase 
	 */
	public String getFechaBase() {
		return this.fechaBase;
	}

	/**
	 *	@param fechaBase 
	 */
	public void setFechaBase(String fechaBase) {
		this.fechaBase = fechaBase;
	}

}
