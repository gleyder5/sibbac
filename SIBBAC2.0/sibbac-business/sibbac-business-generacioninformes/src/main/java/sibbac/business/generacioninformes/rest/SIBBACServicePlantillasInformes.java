package sibbac.business.generacioninformes.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.generacioninformes.database.bo.Tmct0InformesBo;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDaoImp;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosDao;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dto.ValoresDTO;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServicePlantillasInformes implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServicePlantillasInformes.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  private static final String NOMBRE_SERVICIO = "SIBBACServicePlantillasInformes";

  @Autowired
  Tmct0InformesCamposDao tmct0InformesCamposDao;

  @Autowired
  Tmct0InformesDao tmct0InformesDao;

  @Autowired
  Tmct0InformesBo tmct0InformesBo;

  @Autowired
  private Tmct0aliDao aliDao;

  @Autowired
  private Tmct0ValoresBo valoresBo; // Para obtener los isin

  @Autowired
  Tmct0InformeAssembler tmct0InformeAssembler;

  @Autowired
  Tmct0InformesDaoImp tmct0InformesDaoImp;

  @Autowired
  Tmct0InformesDatosDao tmct0InformesDatosDao;

  /**
   * The Enum para la lista totalizar
   */
  public enum EnuTotalizar {

    SI("S"),
    NO("N");

    private String key;

    EnuTotalizar(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }

  /**
   * The Enum para la lista totalizar
   */
  public enum EnuIdioma {

    ESPAÑOL("ESPAÑOL"),
    INGLES("INGLES");

    private String key;

    EnuIdioma(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }

  /**
   * The Enum ComandosPlantillas.
   */
  private enum ComandosPlantillas {

    /** Inicializa los datos del filtro de pantalla */
    CARGA_SELECTS_ESTATICOS("CargaSelectsEstaticos"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_CAMPOS_DETALLES("CargaCamposDetalle"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_ALIAS("CargaAlias"),

    CARGA_ISIN("CargaIsin"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_PLANTILLAS("CargaPlantillas"),

    /** Inicializa el grid con todas las plantillas actuales */
    CARGA_PLANTILLAS_INFORMES("CargaPlantillasInformes"),

    /** Consulta de las plantillas que cumplen el filtro. */
    CONSULTA_PLANTILLAS("ConsultaPlantillas"),

    /** Crear un plantilla con los datos Recibidos */
    CREAR_PLANTILLA("CrearPlantilla"),

    /** Modifica los datos de una plantilla con los datos Recibidos */
    MODIFICAR_PLANTILLA("ModificarPlantilla"),

    /** Elimina las plantillas checkeados */
    ELIMINAR_PLANTILLAS("EliminarPlantillas"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandosPlantillas(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * The Enum Campos plantilla .
   */
  private enum FieldsPlantilla {
    PLANTILLA("plantilla"),
    IDIOMA("idioma"),
    TOTALIZAR("totalizar"),
    CAMPOSDETALLE("camposdetalle"),
    ALIAS("alias"),
    ISIN("isin");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsPlantilla(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, String> filters = webRequest.getFilters();
    List<Map<String, String>> parametros = webRequest.getParams();
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosPlantillas command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServicePlantillasInformes");

      switch (command) {
        case CARGA_SELECTS_ESTATICOS:
          result.setResultados(cargarSelectsEstaticos());
          break;
        case CARGA_CAMPOS_DETALLES:
          result.setResultados(cargarCamposDetalle());
          break;
        case CARGA_ALIAS:
          result.setResultados(cargarAlias());
          break;
        case CARGA_ISIN:
          result.setResultados(cargarIsin());
          break;
        // Carga la lista de las plantillas del filtro
        case CARGA_PLANTILLAS:
          result.setResultados(cargarPlantillas());
          break;
        case CREAR_PLANTILLA:
          result.setResultados(crearPlantilla(paramsObjects));
          break;
        case MODIFICAR_PLANTILLA:
          result.setResultados(modificarPlantilla(paramsObjects));
          break;
        case CARGA_PLANTILLAS_INFORMES:
          result.setResultados(getPlantillasInformes(paramsObjects));
          break;
        case ELIMINAR_PLANTILLAS:
          result.setResultados(eliminarPlantillas(paramsObjects));
          break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServicePlantillasInformes");
    return result;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarSelectsEstaticos() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para combos estaticos del filtro");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaTotalizar = new ArrayList<SelectDTO>();
      List<SelectDTO> listaIdiomas = new ArrayList<SelectDTO>();

      SelectDTO select;

      // Formamos los valores de totalizar
      for (EnuTotalizar totalizar : EnuTotalizar.values()) {
        select = new SelectDTO(totalizar.getKey(), totalizar.name());
        listaTotalizar.add(select);
      }

      mSalida.put("listaTotalizar", listaTotalizar);

      // Formamos los idiomas
      for (EnuIdioma idio : EnuIdioma.values()) {
        select = new SelectDTO(idio.getKey(), idio.name());
        listaIdiomas.add(select);
      }

      mSalida.put("listaIdiomas", listaIdiomas);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

    return mSalida;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarCamposDetalle() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos cargar Campos Detalle  ");

    Map<String, Object> map = new HashMap<String, Object>();

    try {
      List<SelectDTO> listaCampos = new ArrayList<SelectDTO>();

      SelectDTO select;

      List<Tmct0InformesCampos> listInformesCampos = tmct0InformesCamposDao.findAllByOrderByNbpantallaAsc();
      for (Tmct0InformesCampos tmct0InformesCampos : listInformesCampos) {
        select = new SelectDTO(tmct0InformesCampos.getId().toString(), tmct0InformesCampos.getNbpantalla(),
                               String.valueOf(tmct0InformesCampos.getTotalizar()));
        listaCampos.add(select);
      }

      map.put("listInformesCampos", listaCampos);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion datos cargar Campos Detalle");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarAlias() throws SIBBACBusinessException {

    LOG.debug("Iniciando datos para el filtro de Plantillas Informes - cargarAlias");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Calendar cal = Calendar.getInstance();
      Integer fechaActivo = Integer.parseInt(sdf.format(cal.getTime()));
      List<Tmct0aliDTO> listAlias = aliDao.findAllAli(fechaActivo);

      for (Tmct0aliDTO tmct0aliDTO : listAlias) {
        select = new SelectDTO(tmct0aliDTO.getAlias(), tmct0aliDTO.getAlias() + " -  " + tmct0aliDTO.getDescripcion());
        listaAlias.add(select);
      }
      map.put("listaAlias", listaAlias);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException(
                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarAlias");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarAlias");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarIsin() throws SIBBACBusinessException {

    LOG.debug("Iniciando datos para el filtro de Plantillas Informes - cargarIsin");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaIsin = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      List<Tmct0Valores> listIsin = valoresBo.findAll();
      List<ValoresDTO> listIsinDTO = valoresBo.entitiesListToAnDTOList(listIsin);

      for (ValoresDTO valoresDTO : listIsinDTO) {
        select = new SelectDTO(valoresDTO.getCodigo(), valoresDTO.getCodigo() + " -  " + valoresDTO.getDescripcion());
        listaIsin.add(select);
      }

      map.put("listaIsin", listaIsin);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda - cargarIsin");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarIsin");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPlantillas() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para el filtro de Plantillas Informes - cargarPlantillas ");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaPlantillas = new ArrayList<SelectDTO>();

      List<Tmct0Informes> listaPlantillasInformes = tmct0InformesDao.findInformesOrderByNbdescription();

      SelectDTO select = null;

      for (Tmct0Informes tmct0Informes : listaPlantillasInformes) {
        // select = new SelectDTO(tmct0Informes.getId().toString(), tmct0Informes.getNbdescription());
        select = new SelectDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription());
        listaPlantillas.add(select);
      }

      map.put("listaPlantillas", listaPlantillas);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException(
                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarPlantillas");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarPlantillas");

    return map;
  }

  /**
   * Crea una nueva plantilla con los datos recibidos.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> crearPlantilla(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Inicio crearPlantilla");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      // Tmct0InformeAssembler miAssembler = new Tmct0InformeAssembler();
      Tmct0InformeDTO miDTO = tmct0InformeAssembler.getTmct0InformeDto(paramsObjects);

      // Comprobacion que no exista ya un informe con ese nombre

      List<Tmct0Informes> listInformes = tmct0InformesDao.findBynbdescription(miDTO.getNbdescription());

      if (listInformes.size() > 0) {
        LOG.error("Se esta intentanto crear una plantilla con el mismo nombre -  " + miDTO.getNbdescription());
        throw new SIBBACBusinessException("Ya existe una plantilla con el nombre " + miDTO.getNbdescription());
      }

      Integer idInforme = tmct0InformesBo.grabarPlantillaInforme(miDTO);
      map.put("idInforme", idInforme);

    } catch (SIBBACBusinessException e) {
      throw e;
    }

    LOG.info("Plantilla creada correctamente");

    LOG.debug("Fin crearPlantilla");

    return map;
  }

  /**
   * Modifica la plantilla con los nuevos datos recibidos.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> modificarPlantilla(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio modificarPlantilla");

    Map<String, Object> map = new HashMap<String, Object>();

    Tmct0InformeDTO miDTO = tmct0InformeAssembler.getTmct0InformeDto(paramsObject);

    // Comprobacion que el identificador de informe sea valido
    if (miDTO.getIdInforme() == 0) {
      LOG.error("No se ha podido modificar el informe  ya que el id es cero, no se han recibido bien los datos -  ");
      throw new SIBBACBusinessException(
                                        "No se puede modificar el informe ya que el id es cero, no se han recibido bien los datos ");
    }

    // Comprobacion que no exista ya un informe con ese nombre que no sea el que se esta intentando modificar
    List<Tmct0Informes> listInformes = tmct0InformesDao.findBynbdescription(miDTO.getNbdescription());
    if (listInformes.size() > 0) {
      for (Tmct0Informes informe : listInformes) {
        // Si el idInforme no es el mismo que el que estamos intentando modificar salta excepción
        if (informe.getId().compareTo(miDTO.getIdInforme()) != 0) {
          LOG.error("Se esta intentanto modificar una plantilla con el mismo nombre que una existente -  "
                    + miDTO.getNbdescription());
          throw new SIBBACBusinessException("Ya existe una plantilla con el nombre " + miDTO.getNbdescription());
        }
      }

    }

    // Se borran los campos actuales del informe para formar el informe con los nuevos campos selecciionados
    tmct0InformesDatosDao.deleteInformeDatos(miDTO.getIdInforme());

    // se graba la plantilla con las nuevas consultas
    tmct0InformesBo.grabarPlantillaInforme(miDTO);

    LOG.info("Plantilla modificada correctamente");

    LOG.debug("Fin modificarPlantilla");

    return map;
  }

  /**
   * Elimina las plantillas seleccionadas.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> eliminarPlantillas(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio eliminarPlantillas");

    Map<String, Object> map = new HashMap<String, Object>();

    List<Integer> ListInformesBorrar = (List<Integer>) paramsObject.get("listaPlantillasBorrar");

    for (Integer idInforme : ListInformesBorrar) {
      LOG.info("Borramos el informe con id " + idInforme.toString());
      try {
        tmct0InformesDao.delete(idInforme);
      } catch (Exception e) {
        LOG.error("Error al borrar el informe " + idInforme);
        LOG.error(e.getMessage());
        throw new SIBBACBusinessException("No se ha podido borrar el informe seleccionado - Consulte con informatica ");
      }
    }

    LOG.info("Plantillas eliminadas correctamente");

    LOG.debug("Fin eliminarPlantillas");

    return map;
  }

  /**
   * Obtencion de todas las plantillas actuales para la carga del grid inicial
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getPlantillasInformes(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Entra en  getPlantillasInformesFiltro ");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaPlantillasInformes = new ArrayList<Object>();
    List<Object> listaPlantillasInformesFiltroDto = new ArrayList<Object>();

    try {

      // Se recuperan todos las plantillas
      Tmct0InformeFilterDTO filters = tmct0InformeAssembler.getTmct0InformeFilterDto(paramsObjects);

      // Se recuperan los informes que cumplen con los criterios estableceidos
      List<Tmct0Informes> listaPlantillasInformesFiltro = tmct0InformesDaoImp.findPlantillasInformesFiltro(filters);

      // Se transforman a lista de DTO
      for (Tmct0Informes tmct0Informes : listaPlantillasInformesFiltro) {
        try {
          Tmct0InformeDTO miDTO = tmct0InformeAssembler.getTmct0InformeDto(tmct0Informes);
          listaPlantillasInformesFiltroDto.add(miDTO);
        } catch (Exception e) {
          LOG.error("Error metodo getPlantillasInformes -  miAssembler.getTmct0InformeDto " + e.getMessage(), e);
          // TODO: handle exception
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo getPlantillasInformes -  " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
    }

    result.put("listaPlantillasInformes", listaPlantillasInformesFiltroDto);

    LOG.debug("Fin getFinalHoldersFiltro");

    return result;

  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    FieldsPlantilla[] fields = FieldsPlantilla.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosPlantillas getCommand(String command) {
    ComandosPlantillas[] commands = ComandosPlantillas.values();
    ComandosPlantillas result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosPlantillas.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

}
