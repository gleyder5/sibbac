package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;


@Repository
public interface Tmct0InformesCamposDao extends JpaRepository< Tmct0InformesCampos, Integer > {


  public List<Tmct0InformesCampos> findAllByOrderByNbpantallaAsc();
  
  @Query("SELECT m FROM Tmct0InformesCampos m WHERE m.clase = :clase ORDER BY m.nbpantalla ASC")
  public List<Tmct0InformesCampos> findByClase(@Param("clase") String clase);
  
  @Query("SELECT m FROM Tmct0InformesCampos m WHERE (m.clase = :clase AND m.mercado = :mercado) OR (m.clase = :clase AND m.mercado='') ORDER BY m.nbpantalla ASC")
  public List<Tmct0InformesCampos> findByClaseAndMercado(@Param("clase") String clase, @Param("mercado") String mercado);
  
  @Query("SELECT DISTINCT m.mercado FROM Tmct0InformesCampos m WHERE m.clase = :clase AND m.id IN (:listaIdsCamposDetalle)")
  public List<String> findMercadosDistintosPorClase(@Param("clase") String clase, @Param("listaIdsCamposDetalle") List<Integer> listaIdsCamposDetalle);
  
}
