package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Entidad para la gestion de TMCT0_INFORMES_RELACIONES
 * @author Neoris
 *
 */
@Entity
@Table( name = "TMCT0_INFORMES_RELACIONES" )
public class Tmct0InformesRelaciones {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	@Column( name = "CDTABLA1", unique=true, length = 5, nullable = false )
	private String  cdtabla1;  

	@Column( name = "CDTABLA2", length = 5, nullable = false )
	private String  cdtabla2;  

	@Column( name = "CLASE", length = 50, nullable = false)
	private String  clase;

	@Column( name = "MERCADO", length = 50, nullable = false)
	private String  mercado;
	
	@Column( name = "NBTABLA1", length = 100, nullable = false )
	private String  nbtabla1;  

	@Column( name = "NBTABLA2", length = 100, nullable = false )
	private String  nbtabla2;  

	@Column( name = "NBRELACION", length = 250, nullable = false )
	private String  nbrelacion;

	public String getNbtabla1() {
		return nbtabla1;
	}

	public void setNbtabla1(String nbtabla1) {
		this.nbtabla1 = nbtabla1;
	}

	public String getCdtabla1() {
		return cdtabla1;
	}

	public void setCdtabla1(String cdtabla1) {
		this.cdtabla1 = cdtabla1;
	}

	public String getNbtabla2() {
		return nbtabla2;
	}

	public void setNbtabla2(String nbtabla2) {
		this.nbtabla2 = nbtabla2;
	}

	public String getCdtabla2() {
		return cdtabla2;
	}

	public void setCdtabla2(String cdtabla2) {
		this.cdtabla2 = cdtabla2;
	}

	public String getNbrelacion() {
		return nbrelacion;
	}

	public void setNbrelacion(String nbrelacion) {
		this.nbrelacion = nbrelacion;
	}

	  /**
	 * @return the clase
	 */
	public String getClase() {
		return clase;
	}

	/**
	 * @param clase the clase to set
	 */
	public void setClase(String clase) {
		this.clase = clase;
	}

	/**
	 * @return the mercado
	 */
	public String getMercado() {
		return mercado;
	}

	/**
	 * @param mercado the mercado to set
	 */
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
}
