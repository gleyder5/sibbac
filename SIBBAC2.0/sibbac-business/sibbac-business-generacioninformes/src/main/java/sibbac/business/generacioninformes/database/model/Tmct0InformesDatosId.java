package sibbac.business.generacioninformes.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class Tmct0InformesDatosId implements Serializable {

  
  
  private static final long serialVersionUID = 1L;
  
  @ManyToOne
  @JoinColumn( name = "IDINFORME", referencedColumnName = "IDINFORME", nullable=false )
  private Tmct0Informes informe; 

  @ManyToOne
  @JoinColumn( name = "IDTIPO", referencedColumnName = "IDTIPO", nullable=false )
  private Tmct0InformesTipos tipoCampo;
  
  @ManyToOne
  @JoinColumn( name = "IDCAMPO", referencedColumnName = "IDCAMPO", nullable=false )
  private Tmct0InformesCampos campoInforme;
  
  
  public Tmct0InformesDatosId() {
  }

  public Tmct0InformesDatosId(Tmct0Informes idInforme, Tmct0InformesCampos idCampo, Tmct0InformesTipos idTipo ) {
    this.campoInforme = idCampo;
    this.tipoCampo = idTipo;
    this.informe = idInforme;
  }
  
  public Tmct0InformesCampos getIdCampo() {
    return campoInforme;
  }


  public void setIdCampo(Tmct0InformesCampos idCampo) {
    this.campoInforme = idCampo;
  }


  public Tmct0InformesTipos getIdTipo() {
    return tipoCampo;
  }


  public void setIdTipo(Tmct0InformesTipos idTipo) {
    this.tipoCampo = idTipo;
  }


  public Tmct0Informes getIdInforme() {
    return informe;
  }


  public void setIdInforme(Tmct0Informes idInforme) {
    this.informe = idInforme;
  }



  
    
  

  
}
