package sibbac.business.generacioninformes.database.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * DTO para la gestion de TMCT0_INFORMES
 * 
 * @author Neoris
 *
 */
public class Tmct0InformeGeneradoDTO {
  private List<Tmct0InformeColumnaDTO> columnas;
  private List<List<Tmc0InformeCeldaDTO>> resultados;
  private HashMap<String, Double> totales;
  private List<String> errores;
  private String query;
  private boolean freeMemory = true;

  public Tmct0InformeGeneradoDTO() {
    super();
    this.resultados = new ArrayList<List<Tmc0InformeCeldaDTO>>();
    this.totales = new HashMap<String, Double>();
    this.errores = new ArrayList<String>();
  }

  public Tmct0InformeGeneradoDTO(List<String> cabeceras,
                                 List<String> keys,
                                 List<List<Tmc0InformeCeldaDTO>> resultados,
                                 HashMap<String, Double> totales) {
    super();
    this.resultados = resultados;
    this.totales = totales;
  }

  public HashMap<String, Double> getTotales() {
    return totales;
  }

  public void setTotales(HashMap<String, Double> totales) {
    this.totales = totales;
  }

  public List<List<Tmc0InformeCeldaDTO>> getResultados() {
    return resultados;
  }

  public void setResultados(List<List<Tmc0InformeCeldaDTO>> resultados) {
    this.resultados = resultados;
  }

  public List<String> getCabeceras() {
    List<String> cabeceras = new ArrayList<String>();
    Iterator<Tmct0InformeColumnaDTO> itr = columnas.iterator();
    while (itr.hasNext()) {
      Tmct0InformeColumnaDTO informColDto = itr.next();
      cabeceras.add(informColDto.getCabecera());
    }
    return cabeceras;
  }

  public List<String> getErrores() {
    return errores;
  }

  public void setErrores(List<String> errores) {
    this.errores = errores;
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public List<Tmct0InformeColumnaDTO> getColumnas() {
    return columnas;
  }

  public void setColumnas(List<Tmct0InformeColumnaDTO> columnas) {
    this.columnas = columnas;
  }

  public boolean isFreeMemory() {
	return freeMemory;
  }

  public void setFreeMemory(boolean freeMemory) {
	this.freeMemory = freeMemory;
  }
}
