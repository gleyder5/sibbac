/**
 * 
 */
package sibbac.business.generacioninformes.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.PlanifInformeFormatoEnum;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.PlanifInformeTipoDestinoEnum;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;

/**
 * @author lucio.vilar
 *
 */
@Entity
@Table( name = "TMCT0_INFORMES_PLANIFICACION" )
public class Tmct0InformesPlanificacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5181679250119721094L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	/**
	 * identificador de la tabla TMCT0_INFORMES
	 */
	@ManyToOne()
	@JoinColumn(name = "IDINFORME")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0Informes tmct0Informes;
	
	@Column(name = "NOMBRE_FICHERO", length = 100, nullable = false)
	private String nombreFichero;
	
	@ManyToOne()
	@JoinColumn(name = "ID_PERIODICIDAD")
	@LazyCollection(LazyCollectionOption.FALSE)
	private ReglaPeriodicidad periodicidad;
	
	@Column(name = "TIPO_DESTINO", length = 50, nullable = false)
	private String tipoDestino;

	@Column(name = "DESTINO", length = 200, nullable = false)
	private String destino;

	@Column(name = "FORMATO", length = 20, nullable = false)
	private String formato;
	
	@Column(name = "SEPARADOR_DECIMAL", length = 1, nullable = true)
	private char separadorDecimales;

	@Column(name = "SEPARADOR_MILES", length = 1, nullable = true)
	private char separadorMiles;
	
	@Column(name = "SEPARADOR_CAMPOS", length = 20, nullable = true)
	private String separadorCampos;

	@Column(name = "ESTADO", length = 1, nullable = true)
	private char estado;
	
	@Column(name = "FECHA_EJECUCION", length = 10, nullable = true)
	private Date fechaEjecucion;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0InformesPlanificacion(){}
	
	public Tmct0InformesPlanificacion(Integer anId, Tmct0Informes informe, String nomFichero, ReglaPeriodicidad periodicidad, String unTipoDestino,
			String unDestino, String unFormato, char unSeparadorDecimales, char unSeparadorMiles, String unSeparadorCampos, char unEstado) {
		this.id = anId;
		this.tmct0Informes = informe;
		this.nombreFichero = nomFichero;
		this.periodicidad = periodicidad;
		this.tipoDestino = unTipoDestino;
		this.destino = unDestino;
		this.formato = unFormato;
		this.separadorDecimales = unSeparadorDecimales;
		this.separadorMiles = unSeparadorMiles;
		this.separadorCampos = unSeparadorCampos;
		this.estado = unEstado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tmct0Informes getTmct0Informes() {
		return tmct0Informes;
	}

	public void setTmct0Informes(Tmct0Informes tmct0Informes) {
		this.tmct0Informes = tmct0Informes;
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public String getTipoDestino() {
		return tipoDestino;
	}

	public void setTipoDestino(String tipoDestino) {
		this.tipoDestino = tipoDestino;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public char getSeparadorDecimales() {
		return separadorDecimales;
	}

	public void setSeparadorDecimales(char separadorDecimales) {
		this.separadorDecimales = separadorDecimales;
	}

	public char getSeparadorMiles() {
		return separadorMiles;
	}

	public void setSeparadorMiles(char separadorMiles) {
		this.separadorMiles = separadorMiles;
	}

	public String getSeparadorCampos() {
		return separadorCampos;
	}

	public void setSeparadorCampos(String separadorCampos) {
		this.separadorCampos = separadorCampos;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ReglaPeriodicidad getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(ReglaPeriodicidad periodicidad) {
		this.periodicidad = periodicidad;
	}
	
	/**
	 *	Devuelve true si el tipo de destino de la planificacion es EMAIL.
	 *	@return boolean 
	 */
	public boolean isTipoDestinoEMAIL() {
		return this.tipoDestino != null 
			&& this.tipoDestino.equals(PlanifInformeTipoDestinoEnum.EMAIL.getTipoDestino());
	}

	/**
	 *	Devuelve true si el tipo de destino de la planificacion es RUTA.
	 *	@return boolean 
	 */
	public boolean isTipoDestinoRUTA() {
		return this.tipoDestino != null 
			&& this.tipoDestino.equals(PlanifInformeTipoDestinoEnum.RUTA.getTipoDestino());
	}
	
	/**
	 *	Devuelve true si el formato de la planificacion es EXCEL.
	 *	@return boolean 
	 */
	public boolean isFormatoExcel() {
		return this.formato != null
			&& this.formato.equals(PlanifInformeFormatoEnum.EXCEL.getFormato());
	}
	
	/**
	 *	Devuelve true si el formato de la planificacion es TEXTO PLANO.
	 *	@return boolean 
	 */
	public boolean isFormatoTexto() {
		return this.formato != null
			&& this.formato.equals(PlanifInformeFormatoEnum.TEXTO.getFormato());
	}
	
	/**
	 * @return the fechaEjecucion
	 */
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	/**
	 * @param fechaEjecucion the fechaEjecucion to set
	 */
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
}