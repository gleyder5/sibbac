package sibbac.business.generacioninformes.database.dto;

/**
 * DTO para cargar las listas en la vista con angular
 * 
 * @author mario.flores
 *
 */
public class ClaseInformeDTO {

  /**
   * key - Clave del elemento
   */
  private String id;
  /**
   * Value - Valor del elemento
   */
  private String nombre;

  /**
   * description - Descripcion adicional
   */
  private String mercado;
  
  /**
   * clase - clase informe
   */
  private String clase;

  public ClaseInformeDTO(String id, String nombre, String mercado, String clase) {
	this.id = id;
	this.nombre = nombre;
	this.mercado = mercado;
	this.clase = clase;
  }

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getMercado() {
		return mercado;
	}
	
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	
	public String getClase() {
		return clase;
	}
	
	public void setClase(String clase) {
		this.clase = clase;
	}
  
}
