package sibbac.business.generacioninformes.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.generacioninformes.database.bo.Tmct0InformesBo;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;

public class ProcesaInformeDiaRunnable implements Runnable {

  /** Datos con los que se tiene que generar el informe */
  private Tmct0GeneracionInformeFilterDTO filters = null;

  /** Bo con los metodos que realizan las operaciones */
  private Tmct0InformesBo miTmct0InformesBo = null;

  private Tmct0Informes informe = null;

  /** Identificador de hilo que se esta creando. */
  private int iNumJob = 0;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaInformeDiaRunnable.class);
  private static final String NOMBRE_SERVICIO = "ProcesaInformeDiaRunnable";
  private Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();

  public ProcesaInformeDiaRunnable(Tmct0GeneracionInformeFilterDTO filters,
                                   Tmct0InformesBo miTmct0InformesBo,
                                   Tmct0Informes informe,
                                   int iNumJob) {
    this.filters = filters;
    this.miTmct0InformesBo = miTmct0InformesBo;
    this.informe = informe;
    this.iNumJob = iNumJob;
  }

  @Override
  public void run() {
    try {
      // Metodo que ejecuta un informe

      LOG.debug("[ProcesaInformeDiaRunnable :: Inicio ejecutaInforme] Para el dia - "
                + filters.getFechaContratacion().toString());

      dto = miTmct0InformesBo.ejecutaInforme(filters, informe);

      LOG.debug("[ProcesaInformeDiaRunnable :: FIN ejecutaInforme] Para el dia - "
                + filters.getFechaContratacion().toString());

      miTmct0InformesBo.annadirResultados(dto);

      // Metodo que elimina el hilo de la lista de hilos
      miTmct0InformesBo.shutdownRunnable(this, iNumJob, false);

    } catch (Exception e) {
      LOG.error("[" + NOMBRE_SERVICIO + " :: Error en la ejcucion del hilo ]  " + iNumJob);
      LOG.error("[" + NOMBRE_SERVICIO + " :: Exception: ]  " + e.getMessage(), e);
      // Se marca en el proceso principal que se ha producido un error para notificar del mismo al usuario
      miTmct0InformesBo.shutdownRunnable(this, iNumJob, true);

    } finally {
      miTmct0InformesBo.shutdownRunnable(this, iNumJob, false);
    }

  }

}
