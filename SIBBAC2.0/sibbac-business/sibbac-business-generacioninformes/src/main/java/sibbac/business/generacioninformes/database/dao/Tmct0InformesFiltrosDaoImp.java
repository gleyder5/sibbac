package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.common.SIBBACBusinessException;

/**
 * Implementacion del DAO para entidad Tmct0InformesFiltros.
 */
@Repository
public class Tmct0InformesFiltrosDaoImp {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesFiltrosDaoImp.class);

	@PersistenceContext
	private EntityManager em;

	/**
	 *	Encuentra registros por query nativa.
	 *	@return List<Object[]> List<Object[]>
	 *	@throws SIBBACBusinessException SIBBACBusinessException 
	 */
	@SuppressWarnings("unchecked")
	public List<Object[][]> findByNativeQuery(String consulta) throws SIBBACBusinessException {
		return ((List<Object[][]>) em.createNativeQuery(consulta).getResultList());
	}

}
