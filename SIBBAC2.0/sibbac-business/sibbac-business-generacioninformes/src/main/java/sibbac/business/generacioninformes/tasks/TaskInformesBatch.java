package sibbac.business.generacioninformes.tasks;

import static java.text.MessageFormat.format;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesPlanificacionDao;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_GENERAR_INFORMES.NAME, name = Task.GROUP_GENERAR_INFORMES.JOB_BATCH_GENERARA_INFORMES, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 * * ? * *")
public class TaskInformesBatch extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOG = LoggerFactory.getLogger(TaskInformesBatch.class);

  @Autowired
  private GestorPeriodicidades gestorPeriodicidades;

  @Autowired
  Tmct0InformesPlanificacionDao tmct0InformesPlanificacionDao;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private SendMail sendMail;

  @Value("${sibbac.batch.threads: 5}")
  private int threads;

  @Value("${sibbac.batch.path:/sbrmv/ficheros/batch/}")
  private String workPath;

  @Autowired
  private Tmct0InformesBoNEW tmct0InformesBoNEW;

  /**
   * Minutos de espera para que el ejecutor realice toda la tarea.
   */
  @Value("${sibbac.batch.timeout: 60}")
  private int timeout;

  @Override
  public void executeTask() throws Exception {
    final ExecutorService executor;
    final List<Tmct0InformesPlanificacion> listInformesPlanif;
    List<InformesBatchProcessor> futureList;
    final Date dateTime;
    final File workdirectory;
    Future<Void> submited;
    InformesBatchProcessor informesBatchProcessor;
    boolean onTime, allRight;
    int cancelled = 0;

    workdirectory = new File(workPath);
    dateTime = new Date();
    futureList = new ArrayList<>();
    onTime = true;
    try {
      executor = Executors.newFixedThreadPool(threads, new NamedThreadFactory());
      listInformesPlanif = this.tmct0InformesPlanificacionDao.findPlanificacionInformesByEstado('A');

      if (CollectionUtils.isNotEmpty(listInformesPlanif)) {
        for (Tmct0InformesPlanificacion infPlanif : listInformesPlanif) {
          if (gestorPeriodicidades.isReglaActiva(infPlanif.getPeriodicidad(), dateTime)) {
            if (tmct0InformesBoNEW.diferenciaEjecucionMenorMinuto(infPlanif.getFechaEjecucion())) {
              infPlanif.setFechaEjecucion(new Date());
              tmct0InformesPlanificacionDao.save(infPlanif);
              // tmct0InformesPlanificacionDao.updateFechaEjecucionCurrentDate(infPlanif.getId(),
              // new Date());
              informesBatchProcessor = ctx.getBean(InformesBatchProcessor.class);
              informesBatchProcessor.setTmct0InformesPlanificacion(infPlanif);
              informesBatchProcessor.setWorkdirectory(workdirectory);
              submited = executor.submit(informesBatchProcessor);
              informesBatchProcessor.setFuture(submited);
              futureList.add(informesBatchProcessor);
            }
          }
        }
        executor.shutdown();
        onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);
        if (!onTime) {
          for (InformesBatchProcessor future : futureList) {
            if (!future.getFuture().isDone()) {
              future.getFuture().cancel(true);
              cancelled++;
            }
          }
          LOG.error("[sendBatch] Ha finalizado el tiempo de espera de {} minutos, se solicita cancelar {} tareas",
              timeout, cancelled);
        }
      }
    }
    catch (InterruptedException iex) {
      LOG.error("[sendBatch] Se ha interrumpido el hilo en espera con mensaje {}", iex.getMessage());
    }
    catch (RuntimeException rex) {
      LOG.error("[sendBatch] Error no esperado al lanzar o ejecutar tareas batch", rex);
    }
    catch (SIBBACBusinessException e) {
      LOG.error("[sendBatch] Error al traer informes de planificación con mensaje {}", e.getMessage());
    }
    allRight = true;
    for (InformesBatchProcessor future : futureList) {
      allRight &= checkFuture(future);
    }
    if (!(onTime && allRight)) {
      LOG.error("La tarea batch ha finalizado fuera de tiempo o con batch con errores");
      throw new Exception();
    }
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_GENERAR_INFORMES_BATCH;
  }

  boolean checkFuture(InformesBatchProcessor future) {
    try {
      future.getFuture().get();
      return true;
    }
    catch (ExecutionException eex) {
      notifyBatchFailure(future.getTmct0InformesPlanificacion().getTmct0Informes().getNbdescription(),
          Arrays.asList(future.getTmct0InformesPlanificacion().getDestino().split(";")), eex);
    }
    catch (InterruptedException e) {
      LOG.warn("[checkFuture] Se ha cancelado un hilo del batch {}", future.getTmct0InformesPlanificacion(), e);
    }
    catch (RuntimeException ex) {
      LOG.warn("[checkFuture] Error inesperado en la evaluacion del resultado del batch {}",
          future.getTmct0InformesPlanificacion(), ex);
    }
    return false;
  }

  private void notifyBatchFailure(String code, List<String> receptors, ExecutionException eex) {
    final Throwable cause;
    final BatchException bex;

    cause = eex.getCause();
    if (cause != null) {
      if (cause instanceof BatchException) {
        bex = BatchException.class.cast(cause);
        LOG.error("[notifyBatchFailure] Batch {} finalizado con error", code, cause);
        if (receptors.isEmpty()) {
          LOG.warn(
              "[notifyBatchFailure] Batch {} no contiene destinatarios, no se notifica error por correo electronico",
              code);
          return;
        }
        try {
          try (StringWriter errors = new StringWriter(); PrintWriter pw = new PrintWriter(errors)) {
            bex.printStackTrace(pw);
            sendMail.sendMail(
                receptors,
                format("Error en ejecucion batch {0}", code),
                format("El batch con codigo {0} ha finalizado con error\nMensaje:{1}\nTraza de error:\n{2}", code,
                    bex.getMessage(), errors.toString()));
          }

          LOG.debug("[notifyBatchFailure] Se ha enviado un email notificando de un error en el batch {} "
              + "a los siguientes destinatarios: {}", code, receptors);
        }
        catch (Exception ex) {
          LOG.error("[notifyBatchFailure] Error al intentar enviar un email notificando un error en el batch {}", code,
              ex);
        }
        return;
      }
      LOG.error("[notifyBatchFailure] Hilo de ejecución finalizado con error no controlado para batch {}", code, cause);
      return;
    }
    LOG.error("[notifyBatchFailure] Hilo de ejecucion finalizado con causa nula para batch", code, eex);
  }

  private class NamedThreadFactory implements ThreadFactory {

    private int count = 0;

    @Override
    public Thread newThread(Runnable r) {
      final Thread thread;

      thread = new Thread(r);
      thread.setName(String.format("SIBBAC_Batch-%d", count++));
      return thread;
    }

  }

}
