/**
 * 
 */
package sibbac.business.generacioninformes.database.dto.assembler;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dto.Tmct0PlanificacionInformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0PlanificacionInformeFilterDTO;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.periodicidades.database.dao.ReglaPeriodicidadDao;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;

/**
 * @author lucio.vilar
 *
 */
@Service
public class Tmct0PlanificacionInformeAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0PlanificacionInformeAssembler.class);
	
	@Autowired
	private Tmct0InformesDao tmct0InformesDao;
	
	@Autowired
	private ReglaPeriodicidadDao reglaPeriodicidadDao;
	
	public Tmct0PlanificacionInformeDTO getTmct0PlanificacionInformeDTO(Tmct0InformesPlanificacion entidad) {
		LOG.debug("INICIO - getTmct0PlanificacionInformeDTO");
		Tmct0PlanificacionInformeDTO dto = null;
		
		if (null != entidad) {
			String unInforme = "";
			String idInforme = null;
			if (null != entidad.getTmct0Informes()) {
				unInforme = entidad.getTmct0Informes().getNbdescription();
				idInforme = entidad.getTmct0Informes().getId().toString();
			}
			String unPeriodo = "";
			String idPeriodo = null;
			if (null != entidad.getPeriodicidad()) {
				unPeriodo = entidad.getPeriodicidad().getIdReglaUsuario();
				idPeriodo = entidad.getPeriodicidad().getId().toString();
			}
			String unNombreFichero = entidad.getNombreFichero();
			String unTipoDest = entidad.getTipoDestino();
			String unDestino = entidad.getDestino();
			String unFormato = entidad.getFormato();
			String unSeparadorDec = String.valueOf(entidad.getSeparadorDecimales());
			String unSeparadorMil = String.valueOf(entidad.getSeparadorMiles());
			String unSeparadorCampo = String.valueOf(entidad.getSeparadorCampos());
			Integer id = entidad.getId();
			dto = new Tmct0PlanificacionInformeDTO(id , unInforme, idInforme, unPeriodo, idPeriodo , unNombreFichero , unTipoDest , unDestino , unFormato , unSeparadorDec , unSeparadorMil, unSeparadorCampo);
		}
		LOG.debug("FIN - getTmct0PlanificacionInformeDTO");
		return dto;
	}
	
	@SuppressWarnings("unchecked")
	public Tmct0PlanificacionInformeFilterDTO getTmct0PlanificacionInformeFilterDTO(Map<String, Object> paramsObjects) {
		Tmct0PlanificacionInformeFilterDTO filter = null;
		
		if (null != paramsObjects) {
			String unInforme = null;
			if (null != paramsObjects.get("informe") && !paramsObjects.get("informe").equals("")) {
				unInforme = ((Map<String, String>) paramsObjects.get("informe")).get("key");
			}
			String unPeriodo = null;
			if (null != paramsObjects.get("periodo") && !paramsObjects.get("periodo").equals("")) {
				unPeriodo = ((Map<String, String>) paramsObjects.get("periodo")).get("key");
			}
			String unNombreFichero = null;
			if (null != paramsObjects.get("fichero") && !paramsObjects.get("fichero").equals("")) {
				unNombreFichero = (String) paramsObjects.get("fichero");
			}
			String unTipoDest = null;
			if (null != paramsObjects.get("tipoDestino") && !paramsObjects.get("tipoDestino").equals("")) {
				unTipoDest = ((Map<String, String>)paramsObjects.get("tipoDestino")).get("value");
			}
			String unDestino = null;
			if (null != paramsObjects.get("destino") && !paramsObjects.get("destino").equals("")) {
				unDestino = (String) paramsObjects.get("destino");
			}
			String unFormato = null;
			if (null != paramsObjects.get("formato") && !paramsObjects.get("formato").equals("")) {
				unFormato = ((Map<String, String>) paramsObjects.get("formato")).get("value");
			}
			filter = new Tmct0PlanificacionInformeFilterDTO(unInforme, unPeriodo, unNombreFichero, unTipoDest, unDestino, unFormato);
		}
		
		return filter;
	}

	@SuppressWarnings("unchecked")
	public Tmct0InformesPlanificacion getTmct0InformesPlanificacion(Map<String, Object> paramsObjects) {
		Tmct0InformesPlanificacion entity = null;
		
		if (null != paramsObjects) {
			Integer id = null;
			if (null != paramsObjects.get("idPlanificacion") && !paramsObjects.get("idPlanificacion").equals("")) {
				id = (Integer) paramsObjects.get("idPlanificacion");
			}
			Tmct0Informes unInforme = null;
			if (null != paramsObjects.get("informe") && !paramsObjects.get("informe").equals("")) {
				unInforme = tmct0InformesDao.findOne(Integer.valueOf((String) paramsObjects.get("informe")));
			}
			ReglaPeriodicidad unPeriodo = null;
			if (null != paramsObjects.get("periodo") && !paramsObjects.get("periodo").equals("")) {
				unPeriodo = reglaPeriodicidadDao.findById(Long.valueOf((String) paramsObjects.get("periodo")));
			}
			String unNombreFichero = null;
			if (null != paramsObjects.get("fichero") && !paramsObjects.get("fichero").equals("")) {
				unNombreFichero = (String) paramsObjects.get("fichero");
			}
			String unTipoDest = null;
			if (null != paramsObjects.get("tipoDestino") && !paramsObjects.get("tipoDestino").equals("")) {
				unTipoDest = (String) paramsObjects.get("tipoDestino");
			}
			// El destino se inicializa con comillas ya que puede darse el caso de destino vacio.
			String unDestino = "";
			if (null != paramsObjects.get("destino") && !paramsObjects.get("destino").equals("")) {
				unDestino = (String) paramsObjects.get("destino");
			}
			String unFormato = null;
			if (null != paramsObjects.get("formato") && !paramsObjects.get("formato").equals("")) {
				unFormato = (String) paramsObjects.get("formato");
			}
			char unSeparadorDecimal = ' ';
			if (null != paramsObjects.get("sepDecimal") && !paramsObjects.get("sepDecimal").equals("")) {
				unSeparadorDecimal = ((String)paramsObjects.get("sepDecimal")).charAt(0);
			}
			char unSeparadorMiles = ' ';
			if (null != paramsObjects.get("sepMiles") && !paramsObjects.get("sepMiles").equals("")) {
				unSeparadorMiles = ((String)paramsObjects.get("sepMiles")).charAt(0);
			}
			String unSeparadorCampos = "";
			if (null != paramsObjects.get("sepCampos") && !paramsObjects.get("sepCampos").equals("")) {
				unSeparadorCampos = (String)paramsObjects.get("sepCampos");
			}
			
			entity = new Tmct0InformesPlanificacion(id , unInforme, unNombreFichero, unPeriodo, unTipoDest, unDestino, unFormato, unSeparadorDecimal, unSeparadorMiles, unSeparadorCampos, 'A');
		}
		
		return entity;
	}
	
}
