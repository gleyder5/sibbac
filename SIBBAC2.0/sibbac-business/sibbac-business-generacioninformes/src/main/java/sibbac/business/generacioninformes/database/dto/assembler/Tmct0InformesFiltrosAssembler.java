package sibbac.business.generacioninformes.database.dto.assembler;


import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.annotations.common.util.StringHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.generacioninformes.database.dao.Tmct0InformesFiltrosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesFiltrosDaoImp;
import sibbac.business.generacioninformes.database.dto.ComponenteDirectivaDTO;
import sibbac.business.generacioninformes.database.model.Tmct0InformesFiltros;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;


/**
 * Assembler para la entidad Tmct0InformesFiltros.
 */
@Service
public class Tmct0InformesFiltrosAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesFiltrosAssembler.class);

	@Autowired
	private Tmct0InformesFiltrosDao tmct0InformesFiltrosDao;
	
	@Autowired
	private Tmct0InformesFiltrosDaoImp tmct0InformesFiltrosDaoImp;
	
	@Autowired
	private Tmct0InformeAssemblerNEW tmct0InformeAssemblerNEW;
	
	public Tmct0InformesFiltrosAssembler() {
		super();
	}

	/** Inicio - Componente filtros dinamicos. */
	/**
	 * 	Permite obtener un lista del DTO de Componentes Directiva de la entidad.
	 * 	@return List<ComponenteDirectivaDTO> List<ComponenteDirectivaDTO>
	 * 	@throws SIBBACBusinessException SIBBACBusinessException 	
	 */
	public List<ComponenteDirectivaDTO> getListComponenteDirectivaDTO() throws SIBBACBusinessException {
		List<Tmct0InformesFiltros> listTmct0InformesFiltros = this.tmct0InformesFiltrosDao.findAll();
		List<ComponenteDirectivaDTO> datosFiltroInforme = new ArrayList<ComponenteDirectivaDTO>();
		if (CollectionUtils.isNotEmpty(listTmct0InformesFiltros)) {
			for (Tmct0InformesFiltros tmct0InformesFiltro : listTmct0InformesFiltros) {
				datosFiltroInforme.add(getComponenteDirectivaDTOFromEntity(tmct0InformesFiltro));					  
			}
		}
		return datosFiltroInforme;
	}
	/** Fin - Componente filtros dinamicos. */
	
	/** Inicio - Componente filtros dinamicos. */
	/**
	 * 	Permite obtener un lista del DTO de Componentes Directiva de la entidad por id de informe.
	 * 	@param idInforme
	 * 	@return List<ComponenteDirectivaDTO> List<ComponenteDirectivaDTO>
	 * 	@throws SIBBACBusinessException SIBBACBusinessException 	
	 */
	public List<List<ComponenteDirectivaDTO>> getListComponenteDirectivaDTOByIdInforme(Integer idInforme, String mercado) throws SIBBACBusinessException {
		
		List<List<ComponenteDirectivaDTO>> datosFiltroInformeLista = new ArrayList<List<ComponenteDirectivaDTO>>();
		List<Object[]> listTmct0InformesFiltrosDxs = new ArrayList<Object[]>();
		if ("TODOS".equals(mercado)) {
			listTmct0InformesFiltrosDxs = this.tmct0InformesFiltrosDao.findInformesFiltrosAndInformesDatosFiltrosByInformeId(idInforme);
		} else {
			listTmct0InformesFiltrosDxs = this.tmct0InformesFiltrosDao.findInformesFiltrosAndInformesDatosFiltrosByInformeIdAndMercado(idInforme, mercado);
		}
		
		if (CollectionUtils.isNotEmpty(listTmct0InformesFiltrosDxs)) {
			List<ComponenteDirectivaDTO> datosFiltroInforme = new ArrayList<ComponenteDirectivaDTO>();
			List<ComponenteDirectivaDTO> datosFiltroInformeBis = new ArrayList<ComponenteDirectivaDTO>();
			
			for (int i=0;i<listTmct0InformesFiltrosDxs.size();i++) {
				ComponenteDirectivaDTO componenteDirectivaDTO = getComponenteDirectivaDTOFromEntityObject(listTmct0InformesFiltrosDxs.get(i));
				if(componenteDirectivaDTO.getTipo().equals("AUTORRELLENABLE") || componenteDirectivaDTO.getTipo().equals("LISTA") || componenteDirectivaDTO.getTipo().equals("TEXTO") ||componenteDirectivaDTO.getSubTipo().equals("DESDE-HASTA")){
					datosFiltroInforme.add(componenteDirectivaDTO);
				}else{
					datosFiltroInformeBis.add(componenteDirectivaDTO);
				}
			}
			
			datosFiltroInforme.addAll(datosFiltroInformeBis);
			
			for (int i=0;i<datosFiltroInforme.size();i++) {
				datosFiltroInformeBis = new ArrayList<ComponenteDirectivaDTO>();
				for (int j=0;j<4;j++){
					if((i+j)<datosFiltroInforme.size()){
						datosFiltroInformeBis.add(datosFiltroInforme.get(i+j));
					}
				}
				datosFiltroInformeLista.add(datosFiltroInformeBis);
				i = i+3;
			}
		}
		return datosFiltroInformeLista;
	}
	
	/**
	 * 	Permite obtener un lista del DTO de Componentes Directiva de la entidad por id de clase.
	 * 	@param idInforme
	 * 	@return List<ComponenteDirectivaDTO> List<ComponenteDirectivaDTO>
	 * 	@throws SIBBACBusinessException SIBBACBusinessException 	
	 */
	public List<List<ComponenteDirectivaDTO>> getListComponenteDirectivaDTOByClase(String clase, String mercado) throws SIBBACBusinessException {
		
		
		List<List<ComponenteDirectivaDTO>> datosFiltroInformeLista = new ArrayList<List<ComponenteDirectivaDTO>>();
		List<Object[]> listTmct0InformesFiltrosDxs = new ArrayList<Object[]>();
		if ("TODOS".equals(mercado)) {
			listTmct0InformesFiltrosDxs = this.tmct0InformesFiltrosDao.findInformesFiltrosAndInformesDatosFiltrosByClase(clase);
		} else {
			listTmct0InformesFiltrosDxs = this.tmct0InformesFiltrosDao.findInformesFiltrosAndInformesDatosFiltrosByClaseAndMercado(clase, mercado);
		}
		
		if (CollectionUtils.isNotEmpty(listTmct0InformesFiltrosDxs)) {
			List<ComponenteDirectivaDTO> datosFiltroInforme = new ArrayList<ComponenteDirectivaDTO>();
			List<ComponenteDirectivaDTO> datosFiltroInformeBis = new ArrayList<ComponenteDirectivaDTO>();
			
			for (int i=0;i<listTmct0InformesFiltrosDxs.size();i++) {
				ComponenteDirectivaDTO componenteDirectivaDTO = getComponenteDirectivaDTOFromEntityObject(listTmct0InformesFiltrosDxs.get(i));
				if(componenteDirectivaDTO.getTipo().equals("AUTORRELLENABLE") || 
						componenteDirectivaDTO.getTipo().equals("LISTA") || 
						componenteDirectivaDTO.getSubTipo().equals("DESDE-HASTA")){
					datosFiltroInforme.add(componenteDirectivaDTO);
				}else{
					datosFiltroInformeBis.add(componenteDirectivaDTO);
				}
			}
			
			datosFiltroInforme.addAll(datosFiltroInformeBis);
			
			for (int i=0;i<datosFiltroInforme.size();i++) {
				datosFiltroInformeBis = new ArrayList<ComponenteDirectivaDTO>();
				for (int j=0;j<4;j++){
					if((i+j)<datosFiltroInforme.size()){
						datosFiltroInformeBis.add(datosFiltroInforme.get(i+j));
					}
				}
				datosFiltroInformeLista.add(datosFiltroInformeBis);
				i = i+3;
			}
		}
		return datosFiltroInformeLista;
	}
	
	/** Fin - Componente filtros dinamicos. */
	
	/** Inicio - Componente filtros dinamicos. */
	/**
	 * 	Permite obtener DTO de Componentes Directiva de la entidad.
	 * 	@param tmct0InformesFiltro tmct0InformesFiltro
	 * 	@return ComponenteDirectivaDTO ComponenteDirectivaDTO
	 * 	@throws SIBBACBusinessException SIBBACBusinessException
	 */
	private ComponenteDirectivaDTO getComponenteDirectivaDTOFromEntityObject(
			Object[] tmct0InformesFiltro) throws SIBBACBusinessException {
		String valorDesde = "";
		String valorHasta = "";
		String lblBtnSentidoFiltro = "";
		String lblBtnSentidoFiltroHasta = "";
		try {
			if ("FECHA".equals((String) tmct0InformesFiltro[2])) {
				if ("FECHA".equals((String) tmct0InformesFiltro[2])
						&& "DESDE-HASTA".equals((String) tmct0InformesFiltro[3])) {
					String fechas = (String) tmct0InformesFiltro[6];
					if (null != fechas && fechas.contains(" , ")) {
						String arrayFechas[] = fechas.split(" , ");
						for(int i=0;i<arrayFechas.length;i++){
							if(i==0){
								valorDesde = arrayFechas[0];
								lblBtnSentidoFiltro = valorDesde.substring(0, 1);
							}
							if(i==1){
								valorHasta = arrayFechas[1];
								lblBtnSentidoFiltroHasta = valorHasta.substring(0, 1);
							}
						}
					}
				} else {
					valorDesde = (String) tmct0InformesFiltro[6];
				}
			}

			if ("NUMERO".equals((String) tmct0InformesFiltro[2])) {
				if ("NUMERO".equals((String) tmct0InformesFiltro[2])
						&& "DESDE-HASTA".equals((String) tmct0InformesFiltro[3])) {
					String numeros = (String) tmct0InformesFiltro[6];
					if (null != numeros && numeros.contains(" , ")) {
						String arrayNumeros[] = numeros.split(" , ");
						for(int i=0;i<arrayNumeros.length;i++){
							if(i==0){
								valorDesde = arrayNumeros[0];
							}
							if(i==1){
								valorHasta = arrayNumeros[1];
							}
						}
					}
				} else {
					valorDesde = (String) tmct0InformesFiltro[6];
				}
			}

			List<SelectDTO> listaSelecSelect = new ArrayList<SelectDTO>();
			if ("LISTA".equals((String) tmct0InformesFiltro[2])	|| "AUTORRELLENABLE".equals((String) tmct0InformesFiltro[2]) || "TEXTO".equals((String) tmct0InformesFiltro[2])) {
				String valores = (String) tmct0InformesFiltro[6];
				if (null != valores) {
					String arrayValores[] = valores.split(" , ");
					if (null != arrayValores) {
						for (String campo : arrayValores) {
							if (StringUtils.isNotBlank(campo)) {
								SelectDTO select = new SelectDTO("","");
								if (campo.contains("||")){
									String arrayKeyValue[] = campo.split("\\|\\|",-1);
									select = new SelectDTO(arrayKeyValue[0], arrayKeyValue[1]);
								} else {
									select = new SelectDTO(campo, campo);
								}
								listaSelecSelect.add(select);							
							}
						}
					}
				}
			}

			if ("COMBO".equals((String) tmct0InformesFiltro[2])) {
				valorDesde = (String) tmct0InformesFiltro[6];
			}

			String tipoBusqueda = "";
			if (tmct0InformesFiltro.length>9) {
				tipoBusqueda = (String) tmct0InformesFiltro[9];
			}
			return new ComponenteDirectivaDTO(
					((BigInteger) tmct0InformesFiltro[0]).intValue(),
					(String) tmct0InformesFiltro[1],
					(String) tmct0InformesFiltro[2], 
					(String) tmct0InformesFiltro[3],
					(String) tmct0InformesFiltro[4],
					(String) tmct0InformesFiltro[5],
					this.getValoresFromEntity((String) tmct0InformesFiltro[7], (String) tmct0InformesFiltro[8]),
					valorDesde,
					valorHasta,
					false,
					false,
					false,
					false,
					//"+",
					lblBtnSentidoFiltro,
					//"+",
					lblBtnSentidoFiltroHasta,
					listaSelecSelect,
					"IGUAL",
					tipoBusqueda);	
		} catch (Exception e) {
			LOG.error("Error al pasar a DTO los filtros dinámicos " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error al pasar a DTO los filtros dinámicos - getComponenteDirectivaDTOFromEntityObject");
		}
	}
	/** Fin - Componente filtros dinamicos. */
	
	/** Inicio - Componente filtros dinamicos. */
	/**
	 * 	Permite obtener DTO de Componentes Directiva de la entidad.
	 * 	@param tmct0InformesFiltro tmct0InformesFiltro
	 * 	@return ComponenteDirectivaDTO ComponenteDirectivaDTO
	 * 	@throws SIBBACBusinessException SIBBACBusinessException
	 */
	private ComponenteDirectivaDTO getComponenteDirectivaDTOFromEntity(
		Tmct0InformesFiltros tmct0InformesFiltro) throws SIBBACBusinessException {
		return new ComponenteDirectivaDTO(
				tmct0InformesFiltro.getId(),
				tmct0InformesFiltro.getNombre(),
				tmct0InformesFiltro.getTipo(), 
				tmct0InformesFiltro.getSubTipo(),
				tmct0InformesFiltro.getTabla(),
				tmct0InformesFiltro.getCampo(),
				this.getValoresFromEntity(tmct0InformesFiltro.getValor(), tmct0InformesFiltro.getQuery()),
				null,
				null,
				false,
				false,
				false,
				false,
				"+",
				"+",
				new ArrayList<SelectDTO>(),
				"IGUAL",
				"I");					  
	}
	/** Fin - Componente filtros dinamicos. */
	
	/** Inicio - Componente filtros dinamicos. */
	/**
	 * 	Esto devuelve una lista de entidades para rellenar una lista o autocompletable.
	 * 	@param valor valor
	 * 	@param query query
	 * 	@return List<SelectDTO> List<SelectDTO>
	 * 	@throws SIBBACBusinessException SIBBACBusinessException  
	 */
	private List<SelectDTO> getValoresFromEntity(
		String valor, String query) throws SIBBACBusinessException {
		GenericLoggers.logDebug(Tmct0InformesFiltrosAssembler.class, "PLANTILLAS INFORME", "getValoresFromEntity", "INICIO");
		List<SelectDTO> listSelectDTO = new ArrayList<SelectDTO>();
		// Flag para que procese por un solo flujo a la vez.
		boolean exitoso = false;
		if (StringHelper.isNotEmpty(valor)) {
			// VALOR - Cuando el campo VALOR esté relleno, es la lista de valores separados por “,” 
			// que se debe usar para rellenar el campo autorrellenable o la lista/combo
			String[] arrayValor = valor.split(",");
			if (arrayValor != null && arrayValor.length > 0) {
				for (String val : arrayValor) {
					listSelectDTO.add(new SelectDTO(val, val));
				}
				exitoso = true;
			}
		} else if (!exitoso && StringHelper.isNotEmpty(query)) {
			try {
				// QUERY - Cuando el campo QUERY esté relleno, es la query que se debe usar 
				// para rellenar el campo autorrellenable o la lista/combo
				List<Object[][]> listaObjects = this.tmct0InformesFiltrosDaoImp.findByNativeQuery(query);

				String valueKey = "";
				String value = "";
				if (CollectionUtils.isNotEmpty(listaObjects)) {
					for (Object[] obj : listaObjects) {
						if(null != obj[0]) {
							valueKey =  obj[0].toString();
						}
						if(null != obj[1]) {
							value =  obj[1].toString();
						}
						if (StringUtils.isNotBlank(valueKey) || StringUtils.isNotBlank(value)) {
							listSelectDTO.add(new SelectDTO(valueKey, value));
						}
					}
				}
				exitoso = true;
			} catch (Exception e) {
				GenericLoggers.logError(
						Tmct0InformesFiltrosAssembler.class, "PLANTILLAS INFORME", "getValoresFromEntity", 
						"Error al obtener intentar obtener datos por campo QUERY en tabla Informes Filtros");
			}
		} 
		GenericLoggers.logDebug(Tmct0InformesFiltrosAssembler.class, "PLANTILLAS INFORME", "getValoresFromEntity", "FIN");
		return listSelectDTO;
	}
	/** Fin - Componente filtros dinamicos. */
	
	/** Inicio - Componente filtros dinamicos. */
	/**
	 *	Devuelve una lista de componentes del filtro dinamico en base a una lista de LinkedHashMap.
	 *	@param linkHMComponenteDirectiva linkHMComponenteDirectiva
	 *	@return listaComponenteDirectivaDTO listaComponenteDirectivaDTO 
	 *	@throws SIBBACBusinessException SIBBACBusinessException  
	 */
	public List<List<ComponenteDirectivaDTO>> getListComponenteDirectivaDTOFromLHM(
		List<List<LinkedHashMap>> linkHMComponenteDirectiva) throws SIBBACBusinessException {
		List<ComponenteDirectivaDTO> listaComponenteDirectivaDTO = new ArrayList<ComponenteDirectivaDTO>();
		List<List<ComponenteDirectivaDTO>> listaListaComponenteDirectivaDTO = new ArrayList<List<ComponenteDirectivaDTO>>();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		if (CollectionUtils.isNotEmpty(linkHMComponenteDirectiva)) {
			try{
			for(List<LinkedHashMap> linkHMComponenteDirectivaList : linkHMComponenteDirectiva){
				for(LinkedHashMap<String, String> compDir : linkHMComponenteDirectivaList){
		    		ComponenteDirectivaDTO compDirDTO = new ComponenteDirectivaDTO();
			    	for (Map.Entry entry : compDir.entrySet()) {
			    		switch ((String) entry.getKey()) {
			    		case "id":
			    			if (entry.getValue() instanceof BigInteger) {
			    				compDirDTO.setId(((BigInteger) entry.getValue()).intValue());
			    			} else {
			    			compDirDTO.setId((Integer) entry.getValue());
			    			}
			    			break;
			    		case "nombre":
			    			compDirDTO.setNombre((String) entry.getValue());
			    			break;
			    		case "tipo":
			    			compDirDTO.setTipo((String) entry.getValue());
			    			break;
			    		case "subTipo":
			    			compDirDTO.setSubTipo((String) entry.getValue());
			    			break;
			    		case "tabla":
			    			compDirDTO.setTabla((String) entry.getValue());
			    			break;
			    		case "campo":
			    			compDirDTO.setCampo((String) entry.getValue());
			    			break;
			    		case "listaSelectDTO":
			    			compDirDTO.setListaSelectDTO((List<SelectDTO>) entry.getValue());
			    			break;
			    		case "valorCampo":
			    			if(entry.getValue() instanceof String){
			    				compDirDTO.setValorCampo((String) entry.getValue());
			    			}else{
			    				if(null != entry.getValue() && !"".equals(entry.getValue())){
			    					compDirDTO.setValorCampo(String.valueOf(entry.getValue()));
			    				}else{
			    					compDirDTO.setValorCampo("");
			    				}
			    			}
			    			break;
			    		case "valorCampoHasta":
			    			if(entry.getValue() instanceof String){
			    				compDirDTO.setValorCampoHasta((String) entry.getValue());
			    			}else{
			    				if(null != entry.getValue() && !"".equals(entry.getValue())){
			    					compDirDTO.setValorCampoHasta(String.valueOf(entry.getValue()));
			    				}else{
			    					compDirDTO.setValorCampoHasta("");
			    				}
			    			}
			    			break;
			    		case "valor":
			    			compDirDTO.setValor((String) entry.getValue());
			    			break;
			    		case "query":
			    			compDirDTO.setQuery((String) entry.getValue());
			    			break;
			    		case "lblBtnSentidoFiltro":
			    			compDirDTO.setLblBtnSentidoFiltro((String) entry.getValue());
			    			break;
			    		case "lblBtnSentidoFiltroHasta":
			    			compDirDTO.setLblBtnSentidoFiltroHasta((String) entry.getValue());
			    			break;
			    		case "listaSeleccSelectDTO":
			    			compDirDTO.setListaSeleccSelectDTO((List<SelectDTO>) entry.getValue());
			    			break;
			    		case "valorNumeroFechaDesde":
			    			compDirDTO.setValorNumeroFechaDesde((String) entry.getValue());
			    			break;
			    		case "valorNumeroFechaHasta":
			    			compDirDTO.setValorNumeroFechaHasta((String) entry.getValue());
			    			break;
			    		case "lblTipoBusqueda":
			    			compDirDTO.setLblTipoBusqueda((String) entry.getValue());
			    		default:
			    			break;
			    		}
			    	}
				
		    	
			    	// Se hace una correccion sobre los campos de fecha 
			    	if (compDirDTO.isTipoFecha() && StringUtils.isNotBlank(compDirDTO.getValorCampo())) {
			    		if (compDirDTO.getValorCampo().contains("+")) {
			    			Date dayAfter = DateUtils.addDays(new Date(), Integer.parseInt((compDirDTO.getValorCampo().replace("+", "")))); 
			    			compDirDTO.setValorCampo(tmct0InformeAssemblerNEW.getFechaHabil(df.format(dayAfter), "+"));

			    		} else if (compDirDTO.getValorCampo().contains("-")) {
			    			c.setTime(dt); 
			    			c.add(Calendar.DAY_OF_MONTH, Integer.parseInt(compDirDTO.getValorCampo()));
			    			dt = c.getTime();
			    			compDirDTO.setValorCampo(tmct0InformeAssemblerNEW.getFechaHabil(df.format(dt), "-"));
			    		}
			    		if (null!=compDirDTO.getLblBtnSentidoFiltro() && !"".equals(compDirDTO.getLblBtnSentidoFiltro())) {
			    			compDirDTO.setValorCampo(tmct0InformeAssemblerNEW.getFechaHabil(compDirDTO.getValorCampo(), compDirDTO.getLblBtnSentidoFiltro()));
			    		} else {
		    				compDirDTO.setValorCampo(compDirDTO.getValorCampo());
		    			}

			    		if (compDirDTO.isSubtipoDesdeHasta() && StringUtils.isNotBlank(compDirDTO.getValorCampoHasta())) {
			    			if (compDirDTO.getValorCampoHasta().contains("+")) {
			    				Date dayAfter = DateUtils.addDays(new Date(), Integer.parseInt((compDirDTO.getValorCampoHasta().replace("+", ""))));
			    				compDirDTO.setValorCampoHasta(tmct0InformeAssemblerNEW.getFechaHabil(df.format(dayAfter), "+"));
			    			} else if (compDirDTO.getValorCampoHasta().contains("-")) {
			    				c.setTime(dt); 
			    				c.add(Calendar.DAY_OF_MONTH, Integer.parseInt(compDirDTO.getValorCampoHasta()));
			    				dt = c.getTime();
			    				compDirDTO.setValorCampoHasta(tmct0InformeAssemblerNEW.getFechaHabil(df.format(dt), "-"));
			    			}
			    			if (null!=compDirDTO.getLblBtnSentidoFiltroHasta() && !"".equals(compDirDTO.getLblBtnSentidoFiltroHasta())) {
			    				compDirDTO.setValorCampoHasta(tmct0InformeAssemblerNEW.getFechaHabil(compDirDTO.getValorCampoHasta(), compDirDTO.getLblBtnSentidoFiltroHasta()));
			    			} else {
			    				compDirDTO.setValorCampoHasta(compDirDTO.getValorCampoHasta());
			    			}
			    		}
			    	}
			    	listaComponenteDirectivaDTO.add(compDirDTO);
				}
			}
			listaListaComponenteDirectivaDTO.add(listaComponenteDirectivaDTO);
			
			}catch(Exception e){
				LOG.error("Error al crear DTO de componente directiva:   " + e.getMessage(), e);
			}
		}  
		
		
		return listaListaComponenteDirectivaDTO;
	}
	/** Fin - Componente filtros dinamicos. */
}