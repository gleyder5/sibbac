/**
 * 
 */
package sibbac.business.generacioninformes.database.dto;

/**
 * @author lucio.vilar
 *
 */
public class Tmct0PlanificacionInformeFilterDTO {

	private String informe;
	private String periodo;
	private String nombreFichero;
	private String tipoDestino;
	private String destino;
	private String formato;
	
	public Tmct0PlanificacionInformeFilterDTO() {}
	
	public Tmct0PlanificacionInformeFilterDTO(String unInforme, String unPeriodo, String unNombreFichero, String unTipoDest, String unDestino, String unFormato) {
		this.informe = unInforme;
		this.periodo = unPeriodo;
		this.nombreFichero = unNombreFichero;
		this.tipoDestino = unTipoDest;
		this.destino = unDestino;
		this.formato = unFormato;
	}

	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public String getTipoDestino() {
		return tipoDestino;
	}

	public void setTipoDestino(String tipoDestino) {
		this.tipoDestino = tipoDestino;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}
	
}
