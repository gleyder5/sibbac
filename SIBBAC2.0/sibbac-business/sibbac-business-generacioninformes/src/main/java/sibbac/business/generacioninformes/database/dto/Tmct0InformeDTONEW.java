package sibbac.business.generacioninformes.database.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.common.SelectDTO;


/**
 * DTO para la gestion de TMCT0_INFORMES
 * @author Neoris
 *
 */
public class Tmct0InformeDTONEW {
  
  /** idInforme - Identificador del informe  */ 
  private Integer idInforme;
  /** idClase - Identificador de la clase */
  private Integer idClase;
  /** mercado del informe */
  private String mercado;
  /** nbdescription - Nombre descriptivo del informe  */
  private String       nbdescription;
  /** nbidioma - Idioma del informe  */
  private String       nbidioma;
  /** totalizar - Indica si el informe tiene totales  */
  private String   totalizar;
  /** nbalias - Alias de este informe */
  private String       nbalias;
  /** cdisin - Isin del informe  */
  private String  cdisin;
  /** camposDetalle - Nombres separados por comas de todos los campos detalle que forman el informe  */
  private String camposDetalle;
  
  /** nbtxtlibre - Texto libre que ha insertado el usuario  */
  private String nbtxtlibre;
  
  private String filtrosAdicionales;
  
  /** rellenoCampos - Relleno de campos que ha insertado el usuario  */
  private Character rellenoCampos;
  
  
  /** listaCamposDetalle - Lista de campos que forman el detalle  */
  List<SelectDetalleDTO> listaCamposDetalle;
  /** listaCamposAgrupacion - Lista de campos que forman la agrupacion de la consulta  */
  List<SelectDTO> listaCamposAgrupacion;
  /** listaCamposOrdenacion - Lista de campos por los que se ordena la consulta  */
  List<SelectDTO> listaCamposOrdenacion;
  /** listaCamposCabecera - Lista de campos que forman la cabecera del listado Excel */
  List<SelectDTO> listaCamposCabecera;
  /** listaCamposSeparacion - Lista de campos por los que se separa el listado Excel en varias hojas.  */
  List<SelectDTO> listaCamposSeparacion;
  /** listaCamposAgrupacionExcel -  */
  List<SelectDTO> listaCamposAgrupacionExcel;

/** listaAlias - Lista de alias seleccionados en esta plantilla.  */
  List<SelectDTO> listaAlias;
  
  /** listaAlias - Lista de isin seleccionados en esta plantilla.  */
  List<SelectDTO> listaCdisin;
  
  private List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO = new ArrayList<List<ComponenteDirectivaDTO>>();

  private boolean selected;
  
  private Integer idInformePadre;
  
  private boolean esPlanificada;
  
  private ClaseInformeDTO clase;
  
  private String planificada;
  
  private String nbClase;
  
  /**
   * posicion - posicion
   */
  private Integer posicion;
  /**
   * description - fecha base
   */
  private String fechaBase;
  /**
   * description - modUsu
   */
  private String modUsu;
  /**
   * description - modFechaHora
   */
  private Date modFechaHora;

public Tmct0InformeDTONEW () {
    
  }
  
  public Tmct0InformeDTONEW(Integer idInforme,
		  					Integer idClase,
                         String nbdescription,
                         String nbidioma,
                         String totalizar,
                         String nbalias,
                         String cdisin,
                         String camposDetalle,
                         String nbtxtlibre,
                         String filtrosAdicionales,
                         String planificada,
                         Character rellenoCampos,
                         List<SelectDetalleDTO> listaCamposDetalle,
                         List<SelectDTO> listaCamposAgrupacion,
                         List<SelectDTO> listaCamposOrdenacion,
                         List<SelectDTO> listaCamposCabecera,
                         List<SelectDTO> listaCamposSeparacion,
                         List<SelectDTO> listaCamposAgrupacionExcel,
                         boolean selected,
                         Integer posicion,
                         String fechaBase,
                         String nbClase,
                         String modUsu,
                         Date modFechaHora) {
    this.idInforme = idInforme;
    this.idClase = idClase;
    this.nbdescription = nbdescription;
    this.nbidioma = nbidioma;
    this.totalizar = totalizar;
    this.nbalias = nbalias;
    this.cdisin = cdisin;
    this.camposDetalle = camposDetalle;
    this.nbtxtlibre = nbtxtlibre;
    this.filtrosAdicionales = filtrosAdicionales;
    this.setPlanificada(planificada);
    this.rellenoCampos=rellenoCampos;
    this.listaCamposDetalle = listaCamposDetalle;
    this.listaCamposAgrupacion = listaCamposAgrupacion;
    this.listaCamposOrdenacion = listaCamposOrdenacion;
    this.listaCamposCabecera = listaCamposCabecera;
    this.listaCamposSeparacion = listaCamposSeparacion;
    this.listaCamposAgrupacionExcel=listaCamposAgrupacionExcel;
    this.selected = selected;
    this.setPosicion(posicion);
    this.setFechaBase(fechaBase);
    this.setNbClase(nbClase);
    this.modUsu=modUsu;
    this.modFechaHora=modFechaHora;
  }
  
  /**
 * @return the modUsu
 */
public String getModUsu() {
	return modUsu;
}

/**
 * @param modUsu the modUsu to set
 */
public void setModUsu(String modUsu) {
	this.modUsu = modUsu;
}

/**
 * @return the modFechaHora
 */
public Date getModFechaHora() {
	return modFechaHora;
}

/**
 * @param modFechaHora the modFechaHora to set
 */
public void setModFechaHora(Date modFechaHora) {
	this.modFechaHora = modFechaHora;
}

public List<SelectDTO> getListaCdisin() {
	  return listaCdisin;
  }

  public void setListaCdisin(List<SelectDTO> listaCdisin) {
	  this.listaCdisin = listaCdisin;
  }

  public List<SelectDTO> getListaAlias() {
	  return listaAlias;
  }

  public void setListaAlias(List<SelectDTO> listaAlias) {
	  this.listaAlias = listaAlias;
  }

  /**
   * @return the rellenoCampos
   */
  public Character getRellenoCampos() {
	  return rellenoCampos;
  }

  /**
   * @param rellenoCampos the rellenoCampos to set
   */
  public void setRellenoCampos(Character rellenoCampos) {
	  this.rellenoCampos = rellenoCampos;
  }

public Integer getIdInforme() {
    return idInforme;
  }
  public void setIdInforme(Integer idInforme) {
    this.idInforme = idInforme;
  }
  public String getNbdescription() {
    return nbdescription;
  }
  public void setNbdescription(String nbdescription) {
    this.nbdescription = nbdescription;
  }
  public String getNbidioma() {
    return nbidioma;
  }
  public void setNbidioma(String nbidioma) {
    this.nbidioma = nbidioma;
  }
  public String getTotalizar() {
    return totalizar;
  }
  public void setTotalizar(String totalizar) {
    this.totalizar = totalizar;
  }
  public String getNbalias() {
    return nbalias;
  }
  public void setNbalias(String nbalias) {
    this.nbalias = nbalias;
  }
  public String getCdisin() {
    return cdisin;
  }
  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }
  public String getCamposDetalle() {
    return camposDetalle;
  }
  
  public String getNbtxtlibre() {
    return nbtxtlibre;
  }

  public void setNbtxtlibre(String nbtxtlibre) {
    this.nbtxtlibre = nbtxtlibre;
  }

  public void setCamposDetalle(String camposDetalle) {
    this.camposDetalle = camposDetalle;
  }
  public List<SelectDetalleDTO> getListaCamposDetalle() {
    return listaCamposDetalle;
  }
  public void setListaCamposDetalle(List<SelectDetalleDTO> listaCamposDetalle) {
    this.listaCamposDetalle = listaCamposDetalle;
  }
  public List<SelectDTO> getListaCamposAgrupacion() {
    return listaCamposAgrupacion;
  }
  public void setListaCamposAgrupacion(List<SelectDTO> listaCamposAgrupacion) {
    this.listaCamposAgrupacion = listaCamposAgrupacion;
  }
  public List<SelectDTO> getListaCamposOrdenacion() {
    return listaCamposOrdenacion;
  }
  public void setListaCamposOrdenacion(List<SelectDTO> listaCamposOrdenacion) {
    this.listaCamposOrdenacion = listaCamposOrdenacion;
  }
  public List<SelectDTO> getListaCamposCabecera() {
    return listaCamposCabecera;
  }
  public void setListaCamposCabecera(List<SelectDTO> listaCamposCabecera) {
    this.listaCamposCabecera = listaCamposCabecera;
  }
  public List<SelectDTO> getListaCamposSeparacion() {
    return listaCamposSeparacion;
  }
  public void setListaCamposSeparacion(List<SelectDTO> listaCamposSeparacion) {
    this.listaCamposSeparacion = listaCamposSeparacion;
  }

  public boolean isSelected() {
	return selected;
  }

  public void setSelected(boolean selected) {
	this.selected = selected;
  }

	public String getFiltrosAdicionales() {
		return filtrosAdicionales;
	}
	
	public void setFiltrosAdicionales(String filtrosAdicionales) {
		this.filtrosAdicionales = filtrosAdicionales;
	}

	public Integer getIdClase() {
		return idClase;
	}

	public void setIdClase(Integer idClase) {
		this.idClase = idClase;
	}

	public List<List<ComponenteDirectivaDTO>> getListComponenteDirectivaDTO() {
		return this.listComponenteDirectivaDTO;
	}

	public void setListComponenteDirectivaDTO(
			List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO) {
		this.listComponenteDirectivaDTO = listComponenteDirectivaDTO;
	}

	public Integer getIdInformePadre() {
		return idInformePadre;
	}

	public void setIdInformePadre(Integer idInformePadre) {
		this.idInformePadre = idInformePadre;
	}
  
	  /**
	 * @return the listaCamposAgrupacionExcel
	 */
	public List<SelectDTO> getListaCamposAgrupacionExcel() {
		return listaCamposAgrupacionExcel;
	}

	/**
	 * @param listaCamposAgrupacionExcel the listaCamposAgrupacionExcel to set
	 */
	public void setListaCamposAgrupacionExcel(
			List<SelectDTO> listaCamposAgrupacionExcel) {
		this.listaCamposAgrupacionExcel = listaCamposAgrupacionExcel;
	}

	public String getMercado() {
		return mercado;
	}

	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	
	  /**
	 * @return the esPlanificada
	 */
	public boolean isEsPlanificada() {
		return esPlanificada;
	}

	/**
	 * @param esPlanificada the esPlanificada to set
	 */
	public void setEsPlanificada(boolean esPlanificada) {
		this.esPlanificada = esPlanificada;
	}
	
	/**
	 * @return the clase
	 */
	public ClaseInformeDTO getClase() {
		return clase;
	}

	/**
	 * @param clase the clase to set
	 */
	public void setClase(ClaseInformeDTO clase) {
		this.clase = clase;
	}

	/**
	 * @return the posicion
	 */
	public Integer getPosicion() {
		return posicion;
	}

	/**
	 * @param posicion the posicion to set
	 */
	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	/**
	 * @return the fechaBase
	 */
	public String getFechaBase() {
		return fechaBase;
	}

	/**
	 * @param fechaBase the fechaBase to set
	 */
	public void setFechaBase(String fechaBase) {
		this.fechaBase = fechaBase;
	}

	/**
	 * @return the planificada
	 */
	public String getPlanificada() {
		return planificada;
	}

	/**
	 * @param planificada the planificada to set
	 */
	public void setPlanificada(String planificada) {
		this.planificada = planificada;
	}

	/**
	 * @return the nbClase
	 */
	public String getNbClase() {
		return nbClase;
	}

	/**
	 * @param nbClase the nbClase to set
	 */
	public void setNbClase(String nbClase) {
		this.nbClase = nbClase;
	}
}
