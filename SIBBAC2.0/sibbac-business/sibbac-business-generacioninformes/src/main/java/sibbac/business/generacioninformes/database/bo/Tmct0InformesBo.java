package sibbac.business.generacioninformes.database.bo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.RollbackException;

import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesRelacionesDao;
import sibbac.business.generacioninformes.database.dto.Tmc0InformeCeldaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeColumnaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosId;
import sibbac.business.generacioninformes.database.model.Tmct0InformesRelaciones;
import sibbac.business.generacioninformes.rest.SIBBACServiceGenerarInformes;
import sibbac.business.generacioninformes.rest.SIBBACServicePlantillasInformes;
import sibbac.business.generacioninformes.threads.ProcesaInformeDiaRunnable;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.dao.Tmct0GeneracionInformeDAOImpl;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0InformesBo extends AbstractBo<Tmct0Informes, Integer, Tmct0InformesDao> {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesBo.class);

  private static final String TABLA_ALIAS = "BOK";
  private static final String TXT_ALIAS_ERROR = "La plantilla seleccionada no contiene información de ALIAS por lo que no puede aplicar un filtro sobre dicho campo";
  private static final String TXT_GEN_ERROR = "La generación del informe ha devuelto un error, por favor contacte con Soporte IT.";
  private static final String NOMBRE_BO = "Tmct0InformesBo";
  private static final String FORMATO_FECHA_ES = "dd/MM/yyyy";

  
  /** Valor recuperado del propertis con el numero de hilos que se ejecutaran a la vez */
  @Value("${generacioninformes.numThreadsPool}")
  private Integer numThreadsPool;
  
  @Value("${generacioninformes.limite.registros.excel:50000}")
  // Se pone un valor por defecto.
  private int limiteRegistrosExcel;

  /**
   * Ejecutor de los hilos.
   */
  private ExecutorService executor;

  /**
   * Lista que almacena los hilos.
   */
  final List<ProcesaInformeDiaRunnable> misTthreads = new ArrayList<ProcesaInformeDiaRunnable>();

  /**
   * Lista con los datos resultados de la ejecucion de cada hilo.
   */
  List<Tmct0InformeGeneradoDTO> lDatosPorDias;

  /**
   * Variable que controla si el proceso ha tenido o no errores.
   */
  private int iNumErrores;

  @Value("${generacioninformes.alias.routing}")
  private String ROUTING;

  @Value("${generacioninformes.alias.barrido}")
  private String BARRIDO;

  @Value("${mail.source.folder}")
  private String TEMPLATES_FOLDER;
  
  @Value("${generacioninformes.limite.excel.consulta:2000}")
  // Se pone un valor por defecto
  private int limiteRegistrosExcelConsulta;
  
  @PersistenceContext
  private EntityManager em;

  @Autowired
  Tmct0InformesDao tmct0InformesDao;

  @Autowired
  Tmct0InformesCamposDao tmct0InformesCamposDao;

  @Autowired
  Tmct0InformesRelacionesDao tmct0InformesRelacionesDao;

  @Autowired
  Tmct0InformeAssembler tmct0InformeAssembler;

  @Autowired
  Tmct0GeneracionInformeDAOImpl generacionInformeDao;

  @Autowired
  Tmct0InformesDatosDao tmct0InformesDatosDao;
  
  /**
   * 
   * 
   * Graba los datos de la plantilla (Nueva o Existente) a partir de los datos del dto recibido. El metodo obtiene los
   * campos query.
   * 
   * 
   * @param dto - con los datos de la plantilla a crear o modificar
   * @throws SIBBACBusinessException
   */
  @Transactional
  public Integer grabarPlantillaInforme(Tmct0InformeDTO dto) throws SIBBACBusinessException {

    Tmct0Informes miInforme = new Tmct0Informes();

    try {

      // COMPROBACIONES INICIALES
      // MFG 16/06/2016 Se ha incluido un nuevo campo de texto libre para añadir condiciones al where. Se realizan las
      // comprobaciones previas:
      if (dto.getNbtxtlibre() != null && dto.getNbtxtlibre() != "") {
        String sCadena = dto.getNbtxtlibre().trim().toUpperCase();
        if ((sCadena.indexOf("AND") != 0) || (sCadena.length() <= 3)) {
          throw new SIBBACBusinessException(
                                            "El texto libre tiene que comenzar con AND y contener un criterio de filtrado");
        }
      }

      // Comprobacion de error en la formación de los campos de agrupación
      // Si el informe tiene que agrupar se tiene que controlar: -> todos los campos detalle que No totalizan tienen que
      // estar incluida en ella sino la consulta devolveria un error.

      boolean bAgrupa = false; // Indica si el informe agrupa o no
      if (dto.getListaCamposAgrupacion().size() > 0) {
        // if (dto.getListaCamposAgrupacion().size() > 0 || bCamposTotalizar){
        bAgrupa = true;

        for (SelectDTO campoDetalleAux : dto.getListaCamposDetalle()) {
          Tmct0InformesCampos datosCampoDetalle = tmct0InformesCamposDao.findOne(Integer.parseInt(campoDetalleAux.getKey()));
          // tienen que estar incluidos los que tengan el campos totalizar = N ya que los otros los convierto en un SUM
          if ((datosCampoDetalle.getTotalizar() == 'N') && (!dto.getListaCamposAgrupacion().contains(campoDetalleAux))) {
            throw new SIBBACBusinessException(
                                              "Hay campos seleccionados para un informe agrupado que no se pueden sumarizar, por favor, inclúyalos como criterio de agrupación o elimínelos del informe");
          }

        }
      }

      String sEspaniol = SIBBACServicePlantillasInformes.EnuIdioma.ESPAÑOL.name();

      StringBuffer sQuery1 = new StringBuffer("");
      StringBuffer sQuery2 = new StringBuffer("");

      StringBuffer sCampos = new StringBuffer(" SELECT ");
      StringBuffer sFrom = new StringBuffer(" FROM BSNBPSQL.TMCT0EJE EJE ");
      StringBuffer sInnerJoin = new StringBuffer(" ");
      StringBuffer sWhere = new StringBuffer(" WHERE 1=1 ");

      StringBuffer sOrderby = new StringBuffer("");
      StringBuffer sGroupby = new StringBuffer("");

      int iNumCampos = 0;

      List lTablas = new ArrayList<String>();
      lTablas.add("EJE");

      for (SelectDTO tmct0InformeDatoDTO : dto.getListaCamposDetalle()) {
        Tmct0InformesCampos campoDetalle = tmct0InformesCamposDao.findOne(Integer.parseInt(tmct0InformeDatoDTO.getKey()));
        // Se forma el campo el cual depende si el informe es en español o en ingles.
        StringBuffer sCampo = new StringBuffer("");
        StringBuffer sRelacion = new StringBuffer(" ");
        boolean bSumarizar = false; // Indica si le tengo que incluir el sum

        // CONTROL PARA VER SI A UN CAMPO LE TENGO QUE INCLUIR LA FUNCION DE AGRUPACION SUM A LOS CAMPOS QUE TOTALIZAN.
        // Solo se SUMAN si el informe agrupa y no estan incluidos dentro del group by
        if (campoDetalle.getTotalizar() == 'S' && bAgrupa
            && !dto.getListaCamposAgrupacion().contains(tmct0InformeDatoDTO)) {
          bSumarizar = true;
        }

        if (!bSumarizar) {
          if (dto.getNbidioma().equals(sEspaniol)) {
            sCampo.append(campoDetalle.getNbconversion()).append(" AS ").append(campoDetalle.getNbcolumna());
          } else {
            sCampo.append(campoDetalle.getNbconvingles()).append(" AS ").append(campoDetalle.getNbcolingles());
          }

        } else {
          if (dto.getNbidioma().equals(sEspaniol)) {
            sCampo.append(" SUM(").append(campoDetalle.getNbconversion()).append(") AS ")
                  .append(campoDetalle.getNbcolumna());

          } else {
            sCampo.append(" SUM(").append(campoDetalle.getNbconvingles()).append(") AS ")
                  .append(campoDetalle.getNbcolingles());

          }
        }

//        Tmct0InformesRelaciones relacionCampo = campoDetalle.getTmct0InformesRelaciones();
        Tmct0InformesRelaciones relacionCampo = new Tmct0InformesRelaciones();
        String sTablaOrigen = relacionCampo.getCdtabla1();
        String sTablaDestinoRelacion = relacionCampo.getCdtabla2();
        LOG.debug("Informacion campo : " + sCampo + " de la tabla " + sTablaOrigen);

        if (iNumCampos == 0) {
          sCampos.append(sCampo.toString());
        } else {
          sCampos.append(" , ").append(sCampo.toString());
        }

        iNumCampos++;

        if (!lTablas.contains(sTablaOrigen)) {

          // Si el campo esta en una de las tablas que todavia no esta incluida, la incluimos al from y la relación en
          // el where

          // sFrom.append(",").append(relacionCampo.getNbtabla1()).append(" ").append(sTablaOrigen);
          sRelacion.insert(0, " " + relacionCampo.getNbrelacion()).append(" ");
          // sWhere.append(" AND ").append(relacionCampo.getNbrelacion());
          lTablas.add(sTablaOrigen);

          // Siempre que se añade una tabla en la consulta, hay que asegurar que se tienen todas las realaciones para
          // acceder a la información del campo.
          // Se comprueba que la tabla destino de la relación de la nueva tabla ya esta incluida sino se siguen
          // incluyendo las tablas y las relaciones

          // hasta que tengamos una que si esta incluida.
          boolean bExiste = false;

          do {

            if (!lTablas.contains(sTablaDestinoRelacion)) {
              // Coger la relacion de la tabla destino
              Tmct0InformesRelaciones relacionTablaDestino = tmct0InformesRelacionesDao.getOne(sTablaDestinoRelacion);

              if (relacionTablaDestino == null) {
                LOG.error("No se han podido determinar todas las relaciones de la consulta - No se han encontrado la relacion para la tabla  "
                          + sTablaDestinoRelacion);

                throw new SIBBACBusinessException(
                                                  "Error generico formando los datos del informe - Consulte con los responsables de informatica");
              }

              sTablaOrigen = relacionTablaDestino.getCdtabla1();
              sTablaDestinoRelacion = relacionTablaDestino.getCdtabla2();
              // sFrom.append(",").append(relacionTablaDestino.getNbtabla1()).append(" ").append(sTablaOrigen);
              sRelacion.insert(0, " " + relacionTablaDestino.getNbrelacion());
              // sWhere.append(" AND ").append(relacionTablaDestino.getNbrelacion());
              lTablas.add(sTablaOrigen);

            } else {
              bExiste = true;
            }

          } while (!bExiste);

          // Se concatena las relaciones a la variable general
          sInnerJoin.append(sRelacion);

        }

      }

      // Comprobacion si ha insertado un ISIN
      if (!dto.getCdisin().equals("")) {
        sWhere.append(" AND EJE.CDISIN IN (").append(dto.getCdisin()).append(") ");
      }

      // Comprobacion si ha algun alias

      if (!dto.getNbalias().equals("")) {
        sWhere.append(" AND BOK.CDALIAS IN (").append(dto.getNbalias()).append(") ");
      }

      sQuery1 = sQuery1.append(sCampos.toString()).append(sFrom.toString()).append(sInnerJoin)
                       .append(sWhere.toString());

      // Formamos los campos de ordenacion
      iNumCampos = 0;

      for (SelectDTO tmct0InformeDatoDTO : dto.getListaCamposOrdenacion()) {
        // Se forma el campo el cual depende si el informe es en español o en ingles.
        StringBuffer sCampo = new StringBuffer("");

        Tmct0InformesCampos campoOrdenacion = tmct0InformesCamposDao.findOne(Integer.parseInt(tmct0InformeDatoDTO.getKey()));
        if (dto.getNbidioma().equals(sEspaniol)) {
          sCampo.append(campoOrdenacion.getNbcolumna());
        } else {
          sCampo.append(campoOrdenacion.getNbcolingles());
        }

        if (iNumCampos == 0) {
          sOrderby.append(" ORDER BY ").append(sCampo.toString());
        } else {
          sOrderby.append(" , ").append(sCampo.toString());
        }

        iNumCampos++;
      }

      // Se forman los campos de agrupacion
      iNumCampos = 0;
      for (SelectDTO tmct0InformeDatoDTO : dto.getListaCamposAgrupacion()) {
        // Se forma el campo el cual depende si el informe es en español o en ingles.
        StringBuffer sCampo = new StringBuffer("");

        Tmct0InformesCampos campoAgrupacion = tmct0InformesCamposDao.findOne(Integer.parseInt(tmct0InformeDatoDTO.getKey()));
        if (dto.getNbidioma().equals(sEspaniol)) {
          sCampo.append(campoAgrupacion.getNbconversion());
        } else {
          sCampo.append(campoAgrupacion.getNbconvingles());
        }

        if (iNumCampos == 0) {
          sGroupby.append(" GROUP BY ").append(sCampo.toString());
        } else {
          sGroupby.append(" , ").append(sCampo.toString());
        }

        iNumCampos++;
      }

      if (!sGroupby.equals("")) {
        sQuery2.append(sGroupby.toString());
      }

      if (!sOrderby.equals("")) {
        sQuery2.append(sOrderby.toString());
      }

      miInforme = tmct0InformeAssembler.getTmct0InformeEntidad(dto);

      miInforme.setNbquery1(sQuery1.toString());

      miInforme.setNbquery2(sQuery2.toString());

      // Guardamos la plantilla
      tmct0InformesDao.save(miInforme);

      LOG.debug("Plantilla grabada correctamente");

    } catch (RollbackException ex) {
      LOG.error(ex.getLocalizedMessage());
      return 0;
    }

    return miInforme.getId();

  }

  public Tmct0Informes getInforme(Integer id) {
    return tmct0InformesDao.getOne(id);
  }

  public Boolean isConSeparacion(Tmct0Informes informe) {
    LOG.debug("ENTRA EN isConSeparacion");
    return !getColumnasSeparacion(informe).isEmpty();
  }

  // Se controla la excepción de que el usuario quiera filtrar por una alias cuando la plantilla no contiene ningun
  // campo de esa tabla
  private boolean validarAlias(Tmct0Informes informe) {
    boolean result = false;
    String tbAlias = TABLA_ALIAS;
    if (informe != null) {
      if (!informe.getNbquery1().contains(tbAlias)) {
        result = true;

      }
    }
    return result;
  }

  public Tmct0InformeGeneradoDTO queryInforme(Tmct0GeneracionInformeFilterDTO filterDto, Tmct0Informes informe) throws SIBBACBusinessException {
    LOG.debug("Se va a construir la query para ejecutar el informe ");

    Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();
    // Tmct0Informes informe = new Tmct0Informes();
    // informe = tmct0InformesDao.getOne(filterDto.getIdInforme());

    dto.setColumnas(getColumnasInformeDto(informe));

    String queryFiltros = new String();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    Boolean errorTbAlias = false;

    if (filterDto.getAlias() != null) {
      if (!filterDto.getAlias().toString().equals("")) {
        queryFiltros = " AND BOK.CDALIAS IN (" + filterDto.getAlias().toString() + ") ";
        errorTbAlias = validarAlias(informe);
        if (errorTbAlias) {
          dto.getErrores().add(TXT_ALIAS_ERROR);
        }
      }
    }

    // Se analizan las opciones especiales
    if (!errorTbAlias) {
      String opcion = filterDto.getOpcionesEspeciales();
      if (opcion != null) {
        if (opcion.equals("EXCLROUTING")) {
          errorTbAlias = validarAlias(informe);
          if (errorTbAlias) {
            dto.getErrores().add(TXT_ALIAS_ERROR);
          } else {
            queryFiltros += " AND BOK.CDALIAS NOT IN (" + ROUTING + ") ";
          }
        } else if (opcion.equals("EXCLBARRID")) {
          errorTbAlias = validarAlias(informe);
          if (errorTbAlias) {
            dto.getErrores().add(TXT_ALIAS_ERROR);
          } else {
            queryFiltros += " AND BOK.CDALIAS NOT IN (" + BARRIDO + ") ";
          }
        }
      }
    }

    if (!errorTbAlias) {
      if (filterDto.getIsin() != null) {
        if (!filterDto.getIsin().toString().equals("")) {
          queryFiltros += " and EJE.CDISIN IN (" + filterDto.getIsin() + ")";
        }
      }

      // MFG 16/11/2016 Si el campo fecha contratacion tiene un valor la query tiene que ser para ese dia.
      if (filterDto.getFechaContratacion() != null) {
        queryFiltros += " and EJE.FEEJECUC = TO_DATE('" + formatter.format(filterDto.getFechaContratacion())
                        + "','YYYY-MM-DD')";
      } else {
        if (filterDto.getFechaContratacionDesde() != null) {
          queryFiltros += " and EJE.FEEJECUC >= TO_DATE('" + formatter.format(filterDto.getFechaContratacionDesde())
                          + "','YYYY-MM-DD')";
        }
        if (filterDto.getFechaContratacionHasta() != null) {
          queryFiltros += " and EJE.FEEJECUC <= TO_DATE('" + formatter.format(filterDto.getFechaContratacionHasta())
                          + "','YYYY-MM-DD')";
        }
      }

      if (filterDto.getFechaLiquidacionDesde() != null) {
        queryFiltros += " and EJE.FEVALOR >= TO_DATE('" + formatter.format(filterDto.getFechaLiquidacionDesde())
                        + "','YYYY-MM-DD')";
      }
      if (filterDto.getFechaLiquidacionHasta() != null) {
        queryFiltros += " and EJE.FEVALOR <= TO_DATE('" + formatter.format(filterDto.getFechaLiquidacionHasta())
                        + "','YYYY-MM-DD')";
      }

      if (informe != null) {
        String query = "";
        if (informe.getNbquery1() != null) {
          query = informe.getNbquery1();
        }
        if (informe.getNbtxtlibre() != null) {
          query += informe.getNbtxtlibre();
        }
        if (queryFiltros != null) {
          query += queryFiltros;
        }
        
        /** Inicio - Componente filtros dinamicos.	*/
        query += this.tmct0InformeAssembler.getQueryFromComponenteGenerico(
        		filterDto.getListComponenteDirectivaDTO(), query);
        /** Fin - Componente filtros dinamicos.	*/
        
        if (informe.getNbquery2() != null) {
          query += informe.getNbquery2();
        }

        dto.setQuery(query);
      }
    }

    LOG.debug("Query generada " + dto.getQuery());

    return dto;
  }

  public Tmct0InformeGeneradoDTO ejecutaInforme(Tmct0GeneracionInformeFilterDTO filterDto, Tmct0Informes informe) throws SIBBACBusinessException {

    LOG.debug("Entra en ejecutaInforme ");

    // se inicializan los dto
    Tmct0InformeGeneradoDTO dto = queryInforme(filterDto, informe);
    List<List<Tmc0InformeCeldaDTO>> listaListas = new ArrayList<List<Tmc0InformeCeldaDTO>>();
    try {

      List<Object[]> resultados = generacionInformeDao.ejecutaQuery(dto.getQuery());
      LOG.debug("Nº de resultados de la query " + resultados.size());
      if (resultados != null && !resultados.isEmpty()) {
        // se controla el caso de que el informe tenga una única columna, sino da error de casteo
        if (dto.getColumnas().size() == 1) {
          for (Object columna : resultados) {
            List<Tmc0InformeCeldaDTO> fila = new ArrayList<Tmc0InformeCeldaDTO>();
            fila.add(getCeldaInformeDTO(dto, 0, columna));
            addTotalizar(dto, 0, columna);
            listaListas.add(fila);
          }
        } else {
          for (Object[] resultado : resultados) {
            int i = 0;
            List<Tmc0InformeCeldaDTO> fila = new ArrayList<Tmc0InformeCeldaDTO>();
            for (Object columna : resultado) {
              fila.add(getCeldaInformeDTO(dto, i, columna));
              addTotalizar(dto, i, columna);
              i++;
            }
            listaListas.add(fila);
          }
        }
        dto.setResultados(listaListas);
      }
    } catch (RollbackException ex) {
      LOG.error("Error en RollbackException " + ex.getMessage());
      dto.getErrores().add("TXT_GEN_ERROR" + ex.getMessage());

    } catch (SIBBACBusinessException ex) {
      // Se ha producido un error en la ejecución de la query la tratamos
      LOG.error("Error en SIBBACBusinessException " + ex.getMessage());
      dto.getErrores().add(TXT_GEN_ERROR + ex.getMessage());

    } catch (Exception e) {
      LOG.error(e.getLocalizedMessage());
      dto.getErrores().add(TXT_GEN_ERROR);
    }

    LOG.debug("Sale de ejecutaInforme ");
    return dto;

  }

  private Tmc0InformeCeldaDTO getCeldaInformeDTO(Tmct0InformeGeneradoDTO dto, int i, Object columna) {
    String cabeceraCampo = dto.getColumnas().get(i).getCabecera();
    Character precisionCampo = dto.getColumnas().get(i).getPrecision();
    Character totalizar = dto.getColumnas().get(i).getTotalizar();

    Object contenidoFinal;
    StringBuilder formato = new StringBuilder();

    if (precisionCampo.toString().trim().length() != 0 && Character.isDigit(precisionCampo)) {
      try {
        // si tiene decimales se convierte a string con los decimales indicados para que se vea bien en pantalla
        try {
          if (columna == null) {
              // Se contempla el caso donde el valor puede venir null
        	  contenidoFinal = "";
          }	else {
              // para los contenidos de tipo number que nos lleguen como string
              contenidoFinal = (String) String.format(Locale.GERMANY, "%." + precisionCampo.toString() + "f", columna);        	  
          }
        } catch (Exception e) {
          // para los contenidos de tipo number que nos lleguen como number
          Double contDouble = new Double(String.valueOf(columna));
          contenidoFinal = (String) String.format(Locale.GERMANY, "%." + precisionCampo.toString() + "f", contDouble);
        }
        // se evalua que el numero de decimales sea mayor de 0
        // El formato con el que se pintan los datos es "#,##0.00"
        if (Integer.valueOf(precisionCampo.toString()) > 0) {
          formato = new StringBuilder("#,##0.");
          for (int j = 0; j < Integer.valueOf(precisionCampo.toString()); j++) {
            formato.append("0");
          }
        } else {
          formato = new StringBuilder("#,##0");
        }

      } catch (Exception e) {
        contenidoFinal = columna;
      }
    } else {
      contenidoFinal = columna;
    }

    return (new Tmc0InformeCeldaDTO(cabeceraCampo, contenidoFinal, formato.toString(), totalizar));
  }

  private void addTotalizar(Tmct0InformeGeneradoDTO dto, int i, Object columna) {
    String key = dto.getColumnas().get(i).getCabecera();
    Double total = dto.getTotales().get(key);
    if (total != null) {
      total = total + new Double(String.valueOf(columna));
      dto.getTotales().put(key, total);
    }
  }

  private HashMap<String, Double> getTotalesPorColumna(Tmct0Informes informe) {

    LOG.debug("Se va a establecer las columnas a totalizar ");
    HashMap<String, Double> totales = new HashMap<String, Double>();
    for (String columna : getColumnasTotalizar(informe)) {
      totales.put(columna, new Double(0));
    }

    LOG.debug("Fin de la creación de las columnas a totalizar ");
    return totales;
  }

  private List<Tmct0InformeColumnaDTO> getColumnasInformeDto(Tmct0Informes informe) {
    LOG.debug("Se van a establecer las columnas-detalle del informe ");

    List<Tmct0InformeColumnaDTO> listaColumns = new ArrayList<Tmct0InformeColumnaDTO>();

    String sIngles = SIBBACServicePlantillasInformes.EnuIdioma.INGLES.name();
    boolean informeIngles = informe.getNbidioma().equals(sIngles);

    List<Tmct0InformesDatos> datos = informe.getTmct0InformesDatosList();
    if (datos != null && !datos.isEmpty()) {
      // se ordenan las columnas
      Collections.sort(datos, new Comparator<Tmct0InformesDatos>() {
        public int compare(Tmct0InformesDatos p1, Tmct0InformesDatos p2) {
          return new Integer(p1.getNuposici()).compareTo(new Integer(p2.getNuposici()));
        }
      });

      for (Tmct0InformesDatos dato : datos) {
        Tmct0InformesDatosId informeDato = dato.getId();
        // Si el tipo de campo es 0 es el detalle
        if (informeDato.getIdTipo().getId().equals(SIBBACServiceGenerarInformes.EnuTipoCampo.DETALLE.getValue())) {
          Tmct0InformesCampos column = informeDato.getIdCampo();
          Tmct0InformeColumnaDTO miColumn = new Tmct0InformeColumnaDTO();
          // se establece el texto de la cabecera
          if (informeIngles) {
            miColumn.setCabecera(column.getNbcolingles());
          } else {
            miColumn.setCabecera(column.getNbcolumna());
          }

          // se establece la precision
          if (column.getPrecision() != null) {
            miColumn.setPrecision(column.getPrecision());
          }

          // se establece si es un campo que totaliza
          if (column.getTotalizar() == 'N') {
            miColumn.setTotal(false);
          } else {
            miColumn.setTotal(true);
          }
          
          // Se obtiene info de la totalizacion
          miColumn.setTotalizar(column.getTotalizar());
          
          listaColumns.add(miColumn);
        }
      }
    }

    LOG.debug("Se han establecido las columnas-detalle del informe ");
    return listaColumns;
  }

  public List<String> getColumnasSeparacion(Tmct0Informes informe) {
    LOG.debug("Entra en getColumnasSeparacion ");
    List<String> columnas = new ArrayList<String>();
    List<Tmct0InformesDatos> datos = informe.getTmct0InformesDatosList();
    if (datos != null && !datos.isEmpty()) {

      Collections.sort(datos, new Comparator<Tmct0InformesDatos>() {
        public int compare(Tmct0InformesDatos p1, Tmct0InformesDatos p2) {
          return new Integer(p1.getNuposici()).compareTo(new Integer(p2.getNuposici()));
        }
      });

      for (Tmct0InformesDatos dato : datos) {
        Tmct0InformesDatosId informeDato = dato.getId();
        if (informeDato.getIdTipo().getId().equals(SIBBACServiceGenerarInformes.EnuTipoCampo.SEPARADOR.getValue())) {
          Tmct0InformesCampos elCampo = informeDato.getIdCampo();
          String sIngles = SIBBACServicePlantillasInformes.EnuIdioma.INGLES.name();
          if (informe.getNbidioma().equals(sIngles)) {
            columnas.add(elCampo.getNbcolingles());
          } else {
            columnas.add(elCampo.getNbcolumna());
          }
        }
      }
    }

    LOG.debug("fin getColumnasSeparacion ");

    return columnas;

  }

  public List<String> getColumnasTotalizar(Tmct0Informes informe) {

    List<String> columnas = new ArrayList<String>();
    // Si el informe esta marcado para totalizar se calculan las columnas que apliquen
    if (informe.getTotalizar().equals("S")) {
      List<Tmct0InformesDatos> datos = informe.getTmct0InformesDatosList();
      if (datos != null && !datos.isEmpty()) {

        Collections.sort(datos, new Comparator<Tmct0InformesDatos>() {
          public int compare(Tmct0InformesDatos p1, Tmct0InformesDatos p2) {
            return new Integer(p1.getNuposici()).compareTo(new Integer(p2.getNuposici()));
          }
        });

        for (Tmct0InformesDatos dato : datos) {
          Tmct0InformesDatosId informeDato = dato.getId();
          if (informeDato.getIdTipo().getId().equals(SIBBACServiceGenerarInformes.EnuTipoCampo.DETALLE.getValue())) {
            Tmct0InformesCampos elCampo = informeDato.getIdCampo();
            // Si el campo esta marcado para totalizar
            if (elCampo.getTotalizar() == 'S') {
              String sIngles = SIBBACServicePlantillasInformes.EnuIdioma.INGLES.name();
              if (informe.getNbidioma().equals(sIngles)) {
                columnas.add(elCampo.getNbcolingles());
              } else {
                columnas.add(elCampo.getNbcolumna());
              }
            }
          }
        }
      }

    }
    return columnas;

  }

  @Transactional
  /**
   * Recorre todas las fechas y crea un nuevo hilo de ejecucion del informe por cada fecha.
   * 
   * @param lFechasEjecucion - Lista de las fechas
   * @param filters -- Filtros
   * @param lDatosPorDias -- Lista resultado con los datos de cada dia.
   */
  public List<Tmct0InformeGeneradoDTO> ejecutaInformePorDias(List<Date> lFechasEjecucion,
                                                             Tmct0GeneracionInformeFilterDTO filters,
                                                             Tmct0Informes informe) throws SIBBACBusinessException {

    LOG.debug("Entra en ejecutaInformePorDias");

    // Inicializaciones
    // Se van a crear un pool de creas indicado en la variable. Son el numero de hilos que se ejecutan a la vez.
    // Se realiza esto porque al ser el mismo objeto unos hilos estaban cogiendo los filtros de otros
    executor = Executors.newFixedThreadPool(numThreadsPool);

    lDatosPorDias = new ArrayList<Tmct0InformeGeneradoDTO>();

    for (int i = 0; i < lFechasEjecucion.size(); i++) {
      // Se crea un nuevo filtro para cada ejecucion aunque solo va a cambiar la fecha de contratacion
      // pero es necesario crer un objeto nuevo para que cada hilo coja su filtro
      Tmct0GeneracionInformeFilterDTO filters2 = new Tmct0GeneracionInformeFilterDTO();
      // Se asignan los valores que no varian
      filters2.setAlias(filters.getAlias());
      filters2.setFechaContratacion(filters.getFechaContratacion());
      filters2.setFechaContratacionDesde(filters.getFechaContratacionDesde());
      filters2.setFechaContratacionHasta(filters.getFechaContratacionHasta());
      filters2.setFechaLiquidacionDesde(filters.getFechaLiquidacionDesde());
      filters2.setFechaLiquidacionHasta(filters.getFechaLiquidacionHasta());
      filters2.setIdInforme(filters.getIdInforme());
      filters2.setIsin(filters.getIsin());
      filters2.setOpcionesEspeciales(filters.getOpcionesEspeciales());

      // Se asigna la fecha de contratacion con la que se tiene que ejecutar.
      filters2.setFechaContratacion(lFechasEjecucion.get(i));
      filters2.setListComponenteDirectivaDTO(filters.getListComponenteDirectivaDTO());
      ejecutarHilo(filters2, informe, i);
    }

    LOG.debug("[Tmct0InformesBo :: ejecutaInformePorDias] Numero de threads creados - " + misTthreads.size());

    while (misTthreads.size() > 0) {
      LOG.info("******{}::{} procesando hilos, quedan " + misTthreads.size());
      try {
        Thread.sleep(1000); // Espero 1 segundo
      } catch (InterruptedException e) {
        LOG.error("[Tmct0InformesBo :: ejecutaInformePorDias] -  Se ha interrumpido algún thread en el control de cuantos quedan vivos ");
        LOG.error(e.getMessage(), e);
        shutdownAllRunnable();

      }
    }

    LOG.debug("Fin de ejecutaInformePorDias, datos gerados " + lDatosPorDias.size());

    return lDatosPorDias;

  }

  /**
   * Crea un nuevo hilo y lo incorpora al pool
   * 
   * @param listaProcesar - Lista con los elementos que tiene que procesar el hilo
   * @param iNumHilo -- Numero identificativo del hilo
   */
  private void ejecutarHilo(Tmct0GeneracionInformeFilterDTO filters, Tmct0Informes informe, int iNumHilo) {
    ProcesaInformeDiaRunnable runnable = new ProcesaInformeDiaRunnable(filters, this, informe, iNumHilo);
    executor.execute(runnable);
    misTthreads.add(runnable);
  }

  /**
   * Añade el dto resultado de ejecutar un informe a la lista global
   * 
   * @param dto -- Dto resultado de la ejecucion del informe.
   */

  public void annadirResultados(Tmct0InformeGeneradoDTO dto) {
    lDatosPorDias.add(dto);
  }

  /**
   * Elimina el job recibido y si ha habido errores, marca al proceso principal la situacion que el hilo tenia errores
   * 
   * @param current - Job que se quiere eliminar
   * @param iNumJob -
   * @param tieneErrores
   */
  public void shutdownRunnable(ProcesaInformeDiaRunnable current, int iNumJob, boolean tieneErrores) {
    LOG.debug("[" + NOMBRE_BO + " ::SE ELIMINA EL JOB " + iNumJob);

    if (tieneErrores) {
      LOG.debug("[" + NOMBRE_BO + " ::EL JOB TENIA ERRORES: " + iNumJob);
      iNumErrores++;
    }

    misTthreads.remove(current);

  }

  /**
   * Elimina los jobs pendientes del pool.
   */
  public void shutdownAllRunnable() {
    LOG.error("[" + NOMBRE_BO + " ::ERROR NO CONTROLADO- SE BORRAR TODOS LOS JOBS ");
    for (ProcesaInformeDiaRunnable miHilo : misTthreads) {
      misTthreads.remove(miHilo);
    }
  }
  
  /**
   * 	Actualizacion de campos en la entidad de informe una vez lanzado dicho informe al inicio.
   * 	@param codigoUsuario
   * 	@param tmct0Informes
   * 	@throws SIBBACBusinessException
   */
  @Transactional
  public void saveDatosAdicionalesGenerarInformeInicio(
		  String codigoUsuario, Tmct0Informes tmct0Informes) throws SIBBACBusinessException {
	  
	  // GEN_USU: codigo de usuario que está lanzando el informe, 
	  // si el campo tenía un valor previo, se sustituye.
	  tmct0Informes.setGenUsu(codigoUsuario);
	  
	  // GEN_INICIO: Con el timestamp actual, si el campo tenía un valor previo, se sustituye.
	  tmct0Informes.setGenInicio(new Date());
	  
	  // GEN_FIN: Con null al inicial la generación del informe.
	  tmct0Informes.setGenFin(null);
	  
	  this.save(tmct0Informes);
  }
  
  /**
   * 	Actualizacion de campos en la entidad de informe una vez lanzado dicho informe al finalizar.
   * 	@param codigoUsuario
   * 	@param tmct0Informes
   * 	@throws SIBBACBusinessException
   */
  @Transactional
  public void saveDatosAdicionalesGenerarInformeFin(
		  String codigoUsuario, Tmct0Informes tmct0Informes) throws SIBBACBusinessException {
	  
	  // GEN_FIN: Con null al iniciar la generación del informe. 
	  // Finalizado el informe se sustituye el null por el timestamp actual.
	  tmct0Informes.setGenFin(new Date());
	  
	  this.save(tmct0Informes);
  }
  
  /**
   * 	Se crea la cabecera para el reporte de terceros.
   * 	@param workbook 
   * 	@param sheet 
   * 	@param informe
   * 	@param filters
   * 	@param style
   * 	@param rowNum
   * 	@return int
   * 	@throws SIBBACBusinessException
   */
  public int createHeaderTerceros(XSSFWorkbook workbook,
                                   XSSFSheet sheet,
                                   Tmct0Informes informe,
                                   Tmct0GeneracionInformeFilterDTO filters,
                                   XSSFCellStyle style,
                                   Integer rowNum) {

	  try {

		  final FileInputStream stream = new FileInputStream(TEMPLATES_FOLDER + "/LogotipoGlobalBanking.png");
		  final CreationHelper helper = workbook.getCreationHelper();
		  final Drawing drawing = sheet.createDrawingPatriarch();
		  final ClientAnchor anchor = helper.createClientAnchor();
		  anchor.setAnchorType(ClientAnchor.MOVE_AND_RESIZE);
		  final int pictureIndex = workbook.addPicture(stream, Workbook.PICTURE_TYPE_PNG);
		  anchor.setCol1(0);
		  anchor.setRow1(0);
		  final Picture pict = drawing.createPicture(anchor, pictureIndex);
		  pict.resize(1.5, 3);

	  } catch (FileNotFoundException e1) {
		  LOG.error("No se pudo recuperar la imagen de cabecera");
	  } catch (IOException e) {
		  LOG.error("No se pudo recuperar la imagen de cabecera");
	  } catch (Exception e) {
		  LOG.error("No se pudo recuperar la imagen de cabecera");
	  } finally {

		  SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_ES);
		  // Internacionalizar textos
		  String cell1 = "De:";
		  String cell2 = "Informe:";
		  String cell3 = "Fecha:";
		  if (informe.getNbidioma().equals("INGLES")) {
			  cell1 = "From:";
			  cell2 = "Report:";
			  cell3 = "Date:";
		  }

		  rowNum = rowNum + 5;
		  XSSFRow row = sheet.createRow(rowNum);
		  row.createCell(0).setCellValue(cell1);
		  row.createCell(1).setCellValue("BANCO SANTANDER S.A.");
		  rowNum++;

		  row = sheet.createRow(rowNum);
		  row.createCell(0).setCellValue(cell2);
		  row.createCell(1).setCellValue(informe.getNbdescription());
		  XSSFCellStyle style2 = workbook.createCellStyle();
		  XSSFFont font = workbook.createFont();
		  font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		  style2.setFont(font);
		  row.getCell(1).setCellStyle(style2);

		  rowNum++;

		  row = sheet.createRow(rowNum);
		  row.createCell(0).setCellValue(cell3);

		  String texto = "Desde " + formatter.format(filters.getFechaContratacionDesde());
		  if (filters.getFechaContratacionHasta() != null) {
			  texto += " hasta " + formatter.format(filters.getFechaContratacionHasta());
		  }
		  row.createCell(1).setCellValue(texto);

		  // Se inserta una línea vacía
		  rowNum++;
		  rowNum++;
	  }

	  return rowNum;

  }
  
	/**
	 *	Logger utilizado para la generacion de informes.
	 *	@param filters
	 *	@param informe
	 */
  public void logInforme(Tmct0GeneracionInformeFilterDTO filters, String informe) {
	  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	  String mensajeLog = "Lanzado informe " + informe + " (" + filters.getIdInforme() + ") ";

	  if (filters.getFechaContratacionDesde() != null) {
		  mensajeLog += "con FH.CONTRATACION DESDE: " + formatter.format(filters.getFechaContratacionDesde());
		  if (filters.getFechaContratacionHasta() != null) {
			  mensajeLog += " HASTA: " + formatter.format(filters.getFechaContratacionHasta());
		  }
		  if (filters.getFechaLiquidacionDesde() != null) {
			  mensajeLog += ", FH.LIQUIDACION DESDE: " + formatter.format(filters.getFechaLiquidacionDesde());
			  if (filters.getFechaLiquidacionHasta() != null) {
				  mensajeLog += " HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
			  }
		  } else {
			  if (filters.getFechaLiquidacionHasta() != null) {
				  mensajeLog += ", FH.LIQUIDACION HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
			  }
		  }
	  } else {
		  if (filters.getFechaContratacionHasta() != null) {
			  mensajeLog += "con FH.CONTRATACION HASTA: " + formatter.format(filters.getFechaContratacionHasta());
			  if (filters.getFechaLiquidacionDesde() != null) {
				  mensajeLog += ", FH.LIQUIDACION DESDE: " + formatter.format(filters.getFechaLiquidacionDesde());
				  if (filters.getFechaLiquidacionHasta() != null) {
					  mensajeLog += " HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
				  }
			  } else {
				  if (filters.getFechaLiquidacionHasta() != null) {
					  mensajeLog += ", FH.LIQUIDACION HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
				  }
			  }
		  } else {
			  if (filters.getFechaLiquidacionDesde() != null) {
				  mensajeLog += "con FH.LIQUIDACION DESDE: " + formatter.format(filters.getFechaLiquidacionDesde());
				  if (filters.getFechaLiquidacionHasta() != null) {
					  mensajeLog += " HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
				  }
			  } else {
				  if (filters.getFechaLiquidacionHasta() != null) {
					  mensajeLog += "con FH.LIQUIDACION HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
				  }
			  }
		  }
	  }
	  if (filters.getAlias() != null) {
		  if (!filters.getAlias().toString().equals("")) {
			  mensajeLog += ", ALIAS: " + filters.getAlias().toString().replace("'", "");
		  }
	  }
	  if (filters.getIsin() != null) {
		  if (!filters.getIsin().toString().equals("")) {
			  mensajeLog += ", ISIN: " + filters.getIsin().toString().replace("'", "");
		  }
	  }

	  if (filters.getOpcionesEspeciales() != null) {
		  if (filters.getOpcionesEspeciales().equals("ALL")) {
			  mensajeLog += ", Opciones especiales: Incluir todos";
		  } else if (filters.getOpcionesEspeciales().equals("EXCLROUTING")) {
			  mensajeLog += ", Opciones especiales: Excluir routing (ALIAS NOT IN " + ROUTING + ")";
		  } else if (filters.getOpcionesEspeciales().equals("EXCLBARRID")) {
			  mensajeLog += ", Opciones especiales: Excluir barridos de derechos (ALIAS NOT IN " + BARRIDO + ")";
		  }
	  }
	  // TODO: activar cuando este realizada la gestión de usuarios
	  // + " por el usuario: uuuuuuuuu";
	  LOG.info(mensajeLog);
  }
}