package sibbac.business.generacioninformes.enums;

/**
 * Clase con las enumeraciones para generacion de informes.
 */
public class GeneracionInformesEnum {
	
	/** Tipos de componente para la tabla de informes filtro. */
	public static enum TipoCompInfFiltroEnum {

		FECHA ("FECHA"),
		AUTORRELLENABLE ("AUTORRELLENABLE"),
		COMBO ("COMBO"),
		LISTA ("LISTA"),
		NUMERO ("NUMERO"),
		TEXTO ("TEXTO");
		
		/** Tipo de componente. */
		private String tipoComponente;

		/**
		 * @param tipoComponente
		 */
		private TipoCompInfFiltroEnum(String tipoComponente) {
			this.tipoComponente = tipoComponente;
		}

		/**
		 * @return tipoComponente
		 */
		public String getTipoComponente() {
			return this.tipoComponente;
		}
	}
	
	/** SubTipos de componente para la tabla de informes filtro. */
	public static enum SubtipoCompInfFiltroEnum {

		DESDE_HASTA ("DESDE-HASTA");
		
		/** Subtipo de componente. */
		private String subtipoComponente;

		/**
		 * @param subtipoComponente
		 */
		private SubtipoCompInfFiltroEnum(String subtipoComponente) {
			this.subtipoComponente = subtipoComponente;
		}

		/**
		 * @return subtipoComponente
		 */
		public String getSubtipoComponente() {
			return this.subtipoComponente;
		}
	}
	
	/** Planificación de informes: Campo Tipo Destino. */
	public static enum PlanifInformeTipoDestinoEnum {

		RUTA ("RUTA"),
		EMAIL ("EMAIL");
		
		/** Tipo Destino. */
		private String tipoDestino;

		/**
		 * @param tipoDestino
		 */
		private PlanifInformeTipoDestinoEnum(String tipoDestino) {
			this.tipoDestino = tipoDestino;
		}

		/**
		 * @return tipoDestino
		 */
		public String getTipoDestino() {
			return this.tipoDestino;
		}
	}
	
	/** Planificación de informes: Campo Formato. */
	public static enum PlanifInformeFormatoEnum {

		EXCEL ("EXCEL"),
		TEXTO ("TEXTO PLANO");
		
		/** Formato. */
		private String formato;

		/**
		 * @param formato
		 */
		private PlanifInformeFormatoEnum(String formato) {
			this.formato = formato;
		}

		/**
		 * @return formato
		 */
		public String getFormato() {
			return this.formato;
		}
	}
	
	/**
	 * Enum para los tipos de campos del informe
	 */
	public enum TipoCampoInformeEnum {
		DETALLE(0),
		ORDENACION(1),
		AGRUPACION(2),
		SEPARACION(3),
		CABECERA(4),
		AGRUPACION_EXCEL(5);

		private final int value;

		/**
		 *	Constructor-arg. 
		 */
		TipoCampoInformeEnum(int value) {
			this.value = value;
		}

		/**
		 * @return value
		 */
		public int getValue() {
			return this.value;
		}
	}
	
	/** Origen para invocacion metodo generar informe Excel. */
	public static enum OrigenInformeExcelEnum {

		TASK_BATCH ("TASK_BATCH"),
		PANTALLA ("PANTALLA"),
		VALIDACION ("VALIDACION");
		
		/** origen. */
		private String origen;

		/**
		 * @param origen
		 */
		private OrigenInformeExcelEnum(String origen) {
			this.origen = origen;
		}

		/**
		 * @return origen
		 */
		public String getOrigen() {
			return this.origen;
		}
	}
	
	/** Agrupamiento Java de query generacion Informes. */
	public static enum AgrupamientoTotalizarEnum {

		COUNT ('C', "COUNT"),
		MAX ('X', "MAX"),
		MIN ('M', "MIN"),
		SIN_TOTALIZAR ('N', "SIN TOTALIZAR"),
		SUM ('S', "SUM"),
		COUNT_DISTINCT ('D', "COUNT DISTINCT"),
		SUM_DISTINCT ('T', "SUM DISTINCT");
		
		/** clave. */
		private Character clave;
		/** descrip. */
		private String descrip;

		/**
		 *	@param clave clave
		 *	@param descrip descrip 
		 */
		private AgrupamientoTotalizarEnum(Character clave, String descrip) {
			this.clave = clave;
			this.descrip = descrip;
		}

		/**
		 *	@return clave 
		 */
		public Character getClave() {
			return this.clave;
		}

		/**
		 *	@return descrip 
		 */
		public String getDescrip() {
			return this.descrip;
		}
		
		public static String getTextoByClaveAndCampo(Character clave, String campo) {
			StringBuffer sb = new StringBuffer();
			sb.append("TOTALIZACIÓN QUERY INFORME POR JAVA: Columna: " + campo + ", cláusula: " );
			if (clave != null) {
				if (clave.equals(AgrupamientoTotalizarEnum.COUNT.getClave())) {
					sb.append(AgrupamientoTotalizarEnum.COUNT.getDescrip());
				} else if (clave.equals(AgrupamientoTotalizarEnum.MAX.getClave())) {
					sb.append(AgrupamientoTotalizarEnum.MAX.getDescrip());				
				} else if (clave.equals(AgrupamientoTotalizarEnum.MIN.getClave())) {
					sb.append(AgrupamientoTotalizarEnum.MIN.getDescrip());
				} else if (clave.equals(AgrupamientoTotalizarEnum.SUM.getClave())) {
					sb.append(AgrupamientoTotalizarEnum.SUM.getDescrip());
				} else if (clave.equals(AgrupamientoTotalizarEnum.SIN_TOTALIZAR.getClave())) {
					sb.append(AgrupamientoTotalizarEnum.SIN_TOTALIZAR.getDescrip());
				} 
			}
			return sb.toString();
		}
		
	}
	
}