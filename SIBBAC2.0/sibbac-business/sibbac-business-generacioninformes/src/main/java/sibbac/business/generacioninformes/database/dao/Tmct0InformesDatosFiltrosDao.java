package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosFiltros;

/**
 * Data access object de informes datos filtro.
 */
@Repository
public interface Tmct0InformesDatosFiltrosDao extends JpaRepository<Tmct0InformesDatosFiltros, Integer> {

	@Query("SELECT infDxFilt FROM Tmct0InformesDatosFiltros infDxFilt "
			+ "WHERE infDxFilt.tmct0Informes.id = :idInforme AND infDxFilt.tmct0InformesFiltros.id = :idInformeFiltro")
	public Tmct0InformesDatosFiltros findByIdInformeAndIdInformeFiltro(
			@Param("idInforme") Integer idInforme, @Param("idInformeFiltro") Integer idInformeFiltro);
	
	@Query("SELECT infDxFilt FROM Tmct0InformesDatosFiltros infDxFilt "
			+ "WHERE infDxFilt.tmct0Informes.id = :idInforme AND infDxFilt.tmct0InformesFiltros.id IN (:idInformeFiltroList)")
	public List<Tmct0InformesDatosFiltros> findByIdInformeAndIdInformeFiltroList(
			@Param("idInforme") Integer idInforme, @Param("idInformeFiltroList") List<Integer> idInformeFiltroList);
	
	@Query("SELECT infDxFilt FROM Tmct0InformesDatosFiltros infDxFilt WHERE infDxFilt.tmct0Informes.id = :idInforme")
	public List<Tmct0InformesDatosFiltros> findByIdInforme(@Param("idInforme") Integer idInforme);
	
	  @Modifying @Query(value="DELETE FROM TMCT0_INFORMES_DATOS_FILTROS WHERE IDINFORME=:id",nativeQuery=true)
	  public void deleteInformeDatosFiltros(@Param("id") int id);

}