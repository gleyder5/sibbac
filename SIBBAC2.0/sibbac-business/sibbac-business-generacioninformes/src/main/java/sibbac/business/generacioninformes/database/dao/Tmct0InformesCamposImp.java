package sibbac.business.generacioninformes.database.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


@Repository
public class Tmct0InformesCamposImp {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesCamposImp.class);

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public Object[] consultarTipoCampo(String tabla, String columna) {

		LOG.info("INICIO - DAOIMPL - consultarTipoCampo");

		Map<String, Object> parameters = new HashMap<String, Object>();
		Object[] objTipoCampo = new Object[1];

		try {
			StringBuilder consulta = new StringBuilder(
					"select TYPENAME, LENGTH, SCALE "
					+ "from syscat.columns "
					+ "where tabname = :tabla "
					+ "AND COLNAME = :columna "
					+ "AND TABSCHEMA = 'BSNBPSQL'");

			parameters.put("tabla", tabla);
			parameters.put("columna", columna);

			Query query = null;
			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			query = em.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			List<Object> resultList = null;

			resultList = query.getResultList();

			if (resultList.size() > 0) {
				objTipoCampo = (Object[]) resultList.get(0);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarTipoCampo -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarTipoCampo");

		return objTipoCampo;
	}
}
