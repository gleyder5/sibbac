package sibbac.business.generacioninformes.database.dto;

/**
 * 	DTO para cargar los campos del filtro predeterminados por la tabla de informes datos filtros 
 * 	del componente generico campo dinamico que vienen del back.
 */
public class ValoresPrecargadosFiltroDTO {

	private Integer id;
	private Integer idFiltro;
	private String valorFiltro;
	
	/**
	 *	Constructor no-arg. 
	 */
	public ValoresPrecargadosFiltroDTO() {
		super();
	}

	/**
	 *	Constructor args. 
	 */
	public ValoresPrecargadosFiltroDTO(Integer id, Integer idFiltro,
			String valorFiltro) {
		super();
		this.id = id;
		this.idFiltro = idFiltro;
		this.valorFiltro = valorFiltro;
	}

	/**
	 *	@return id 
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 *	@param id 
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *	@return idFiltro 
	 */
	public Integer getIdFiltro() {
		return this.idFiltro;
	}

	/**
	 *	@param idFiltro 
	 */
	public void setIdFiltro(Integer idFiltro) {
		this.idFiltro = idFiltro;
	}

	/**
	 *	@return valorFiltro 
	 */
	public String getValorFiltro() {
		return this.valorFiltro;
	}

	/**
	 *	@param valorFiltro 
	 */
	public void setValorFiltro(String valorFiltro) {
		this.valorFiltro = valorFiltro;
	}

}
