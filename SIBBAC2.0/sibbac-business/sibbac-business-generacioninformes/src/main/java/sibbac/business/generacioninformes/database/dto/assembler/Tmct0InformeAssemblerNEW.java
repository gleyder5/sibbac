package sibbac.business.generacioninformes.database.dto.assembler;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesClasesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosFiltrosDao;
import sibbac.business.generacioninformes.database.dto.ClaseInformeDTO;
import sibbac.business.generacioninformes.database.dto.ComponenteDirectivaDTO;
import sibbac.business.generacioninformes.database.dto.SelectDetalleDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTONEW;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeFilterDTO;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesClases;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosFiltros;
import sibbac.business.generacioninformes.database.model.Tmct0InformesDatosId;
import sibbac.business.generacioninformes.database.model.Tmct0InformesFiltros;
import sibbac.business.generacioninformes.database.model.Tmct0InformesTipos;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.AgrupamientoTotalizarEnum;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.SubtipoCompInfFiltroEnum;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.TipoCompInfFiltroEnum;
import sibbac.business.generacioninformes.helper.GeneracionInformesHelper;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.model.Tmct0Users;


/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_INFORMES
 * @author Neoris
 *
 */
@Service
public class Tmct0InformeAssemblerNEW {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformeAssemblerNEW.class);
  
  final SimpleDateFormat formatDate = new SimpleDateFormat(FormatDataUtils.DATE_FORMAT_1);
  
  @Autowired
  private Tmct0InformesDao tmct0InformesDao;  
  
  @Autowired
  private Tmct0InformesDatosFiltrosDao tmct0InformesDatosFiltrosDao;
  
  @Autowired
  private Tmct0InformesFiltrosAssembler tmct0InformesFiltrosAssembler;
  
  @Autowired
  private Tmct0InformesClasesDao tmct0InformesClasesDao;
  
  @Autowired
  private Tmct0UsersDao tmct0UsersDao;
  
  @Autowired
  private Tmct0cfgDao tmct0cfgDao;
  
  public Tmct0InformeAssemblerNEW() {
    super();
  }

  /**
   * Realiza la conversion entre la entidad y el dto
   * @param datosInforme -> Entidad que se quiere trasformar en DTO
   * @return  DTO con los datos de la entidad tratados
   * @throws SIBBACBusinessException 
   */
  public Tmct0InformeDTONEW getTmct0InformeDto(Tmct0Informes datosInforme) throws SIBBACBusinessException {
    
    Tmct0InformeDTONEW miDTO = new Tmct0InformeDTONEW();
    
    List<SelectDetalleDTO> listaCamposDetalle  = new ArrayList<SelectDetalleDTO>();
    List<SelectDTO> listaCamposAgrupacion = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposOrdenacion = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposCabecera = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposSeparacion = new ArrayList<SelectDTO>();
    List<SelectDTO> listaCamposAgrupacionExcel = new ArrayList<SelectDTO>();
    
    // Valores directos
    miDTO.setIdInforme(datosInforme.getId());
    miDTO.setNbdescription(datosInforme.getNbdescription());
    miDTO.setNbidioma(datosInforme.getNbidioma());
    miDTO.setTotalizar(datosInforme.getTotalizar());
    miDTO.setNbtxtlibre(datosInforme.getNbtxtlibre());
    miDTO.setRellenoCampos(datosInforme.getRellenoCampos());
    miDTO.setMercado(datosInforme.getMercado());
    boolean esPlanificada = false;
    
//	miDTO.setListComponenteDirectivaDTO(cargarListaComponentesDirectivaDto(datosInforme));
    
    if (null != datosInforme.getTmct0InformesClases()) {
    	miDTO.setIdClase(datosInforme.getTmct0InformesClases().getId());
    	miDTO.setClase(new ClaseInformeDTO(datosInforme.getTmct0InformesClases().getId().toString(), datosInforme.getTmct0InformesClases().getNombre(), datosInforme.getTmct0InformesClases().getMercado(), datosInforme.getTmct0InformesClases().getClase()));
    }
    
    // Formamos los campos detalle y las listas de campos detalle, ordenacion, agrupacion, cabecera y separacion
    
    StringBuffer sCamposDetalle = new StringBuffer();
    int i = 0;
    SelectDTO datoInforme;
    SelectDetalleDTO detalle;
            
    for (Tmct0InformesDatos campoInforme : datosInforme.getTmct0InformesDatosList()) {
      
      switch (campoInforme.getId().getIdTipo().getId()) {
        case 0:  // Campo detalle
          if (i > 0){
            sCamposDetalle.append(",").append(campoInforme.getId().getIdCampo().getNbpantalla());
          }else{
            sCamposDetalle.append(campoInforme.getId().getIdCampo().getNbpantalla());
          }
          i++;          
          String nombreCampo="";
          if (null!=campoInforme.getNombre() && !campoInforme.getNombre().equals("")) {
        	  nombreCampo=campoInforme.getNombre();
          } else {
        	  nombreCampo=campoInforme.getId().getIdCampo().getNbpantalla();
          }

    	  String nombreOriginal = campoInforme.getId().getIdCampo().getNbpantalla();
    	  String tipoCampo = campoInforme.getId().getIdCampo().getTipoCampo();
          detalle = new SelectDetalleDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),
        		  						nombreCampo, 
        		  						campoInforme.getNombre()!=null?campoInforme.getNombre():"",
        		  						campoInforme.getTraduccion()!=null?campoInforme.getTraduccion():"",
        		  						campoInforme.getRellenoTipo()!=null?campoInforme.getRellenoTipo():"",
        		  						campoInforme.getRelleno()!=null?campoInforme.getRelleno().toString():"",
        		  						campoInforme.getLongitudEntero(),
        		  						campoInforme.getLongitudDecimal(),
        		  						campoInforme.getLongitudAlfanumerico(),
        		  						tipoCampo,
        		  						nombreOriginal,
        		  						String.valueOf(campoInforme.getTotalizar()));
          listaCamposDetalle.add(detalle);
          break;
        case 1:  // Campo ordenacion
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla());
          listaCamposOrdenacion.add(datoInforme);
          break;
        case 2:  // Campo agrupacion
        	// Se pasa el log correspondiente para verificar la agrupacion.
        	if (campoInforme != null && campoInforme.getId() != null) {
        		LOG.debug(AgrupamientoTotalizarEnum.getTextoByClaveAndCampo(
        				campoInforme.getId().getIdCampo().getTotalizar(), campoInforme.getId().getIdCampo().getNbcolumna()));        		
        	}
        	datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla(),String.valueOf(campoInforme.getId().getIdCampo().getTotalizar()));
        	listaCamposAgrupacion.add(datoInforme);
        	break;
        case 3:  // Campo separacion  
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla());
          listaCamposSeparacion.add(datoInforme);
          break;
        case 4:  // Campo cabecera  
          datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla());
          listaCamposCabecera.add(datoInforme);
          break;
        case 5:  // Campo Agrupacion Excel  
            datoInforme = new SelectDTO(String.valueOf(campoInforme.getId().getIdCampo().getId()),campoInforme.getId().getIdCampo().getNbpantalla(),campoInforme.getId().getIdCampo().getNbcolumna());
            listaCamposAgrupacionExcel.add(datoInforme);
            break;
        default:
          break;
      }
    }
    
    // Asignamos la cadena de campos detalle
    if (sCamposDetalle != null) {
      miDTO.setCamposDetalle(sCamposDetalle.toString());
    }
    
    // Consultamos los filtros adicionales
    List<Tmct0InformesDatosFiltros> filtrosAdicionales = tmct0InformesDatosFiltrosDao.findByIdInforme(datosInforme.getId());
    if (null != filtrosAdicionales && !filtrosAdicionales.isEmpty()) {
    	miDTO.setFiltrosAdicionales("Si");
    } else {
    	miDTO.setFiltrosAdicionales("No");
    }

    // Cargamos la lista de filtros dinamicos
    List<List<ComponenteDirectivaDTO>> datosFiltroInformeByIdInforme = new ArrayList<List<ComponenteDirectivaDTO>>();
	Integer idInforme = datosInforme.getId();
	datosFiltroInformeByIdInforme = this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOByIdInforme(idInforme, miDTO.getMercado());	
	
    
    // Asignamos las listas.
    miDTO.setListaCamposDetalle(listaCamposDetalle);
    miDTO.setListaCamposOrdenacion(listaCamposOrdenacion);
    miDTO.setListaCamposAgrupacion(listaCamposAgrupacion);
    miDTO.setListaCamposSeparacion(listaCamposSeparacion);
    miDTO.setListaCamposCabecera(listaCamposCabecera);
    miDTO.setListaCamposAgrupacionExcel(listaCamposAgrupacionExcel);
    miDTO.setListComponenteDirectivaDTO(datosFiltroInformeByIdInforme);
    
    // Comprobamos si para el informe seleccionado hay planificaciones y si alguna de ellas está activa, marcamos el flag de comprobación de fechas
    if (null!=datosInforme.getTmct0InformesPlanificacion() && datosInforme.getTmct0InformesPlanificacion().size()>0) {
    	for (int j=0;j<datosInforme.getTmct0InformesPlanificacion().size();j++) {
    		if (datosInforme.getTmct0InformesPlanificacion().get(j).getEstado() == 'A') {
    			esPlanificada = true;
    			break;
    		}
    	}
    }
    miDTO.setEsPlanificada(esPlanificada);
    
    return miDTO;
    
  }
  
  private List<List<ComponenteDirectivaDTO>> cargarListaComponentesDirectivaDto(Tmct0Informes datosInforme) {
	    
		List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO = new ArrayList<List<ComponenteDirectivaDTO>>();

		if (null != datosInforme && null != datosInforme.getTmct0InformesDatosFiltros()) {
			List<ComponenteDirectivaDTO> compDirDTOList = new ArrayList<ComponenteDirectivaDTO>();
			for (Tmct0InformesDatosFiltros tmct0InformesDatosFiltros : datosInforme.getTmct0InformesDatosFiltros()) {
				Tmct0InformesFiltros tmct0InformesFiltros = tmct0InformesDatosFiltros.getTmct0InformesFiltros();
				ComponenteDirectivaDTO compDirDTO = new ComponenteDirectivaDTO();
				compDirDTO.setId(tmct0InformesFiltros.getId());
				compDirDTO.setTipo(tmct0InformesFiltros.getTipo());
				compDirDTO.setSubTipo(tmct0InformesFiltros.getSubTipo());
				compDirDTO.setCampo(tmct0InformesFiltros.getCampo());
				compDirDTO.setNombre(tmct0InformesFiltros.getNombre());
				compDirDTO.setQuery(tmct0InformesFiltros.getQuery());
				compDirDTO.setTabla(tmct0InformesFiltros.getTabla());
//				compDirDTO.setListaSelectDTO(tmct0InformesFiltros);
				

				switch (tmct0InformesFiltros.getTipo()) {
				case "FECHA":
					if (tmct0InformesFiltros.getSubTipo().equals("DESDE-HASTA")) {
						String fechas = tmct0InformesDatosFiltros.getValorFiltro();
						if (null != fechas && fechas.contains(",")) {
							String[] arrayFechas = fechas.split(",");
							compDirDTO.setValorCampo(arrayFechas[0]);
							compDirDTO.setValorCampoHasta(arrayFechas[1]);
						}
					} else {
						compDirDTO.setValorCampo(tmct0InformesDatosFiltros.getValorFiltro());
					}
					break;
				case "NUMERO":
					if (tmct0InformesFiltros.getSubTipo().equals("DESDE-HASTA")) {
						String numeros = tmct0InformesDatosFiltros.getValorFiltro();
						if (null != numeros && numeros.contains(",")) {
							String[] arrayNumeros = numeros.split(",");
							compDirDTO.setValorCampo(arrayNumeros[0]);
							compDirDTO.setValorCampoHasta(arrayNumeros[1]);
						}
					} else {
						compDirDTO.setValorCampo(tmct0InformesDatosFiltros.getValorFiltro());
					}
					break;
				case "LISTA":
					String valoresConcat = tmct0InformesDatosFiltros.getValorFiltro();
					if (null != valoresConcat && valoresConcat.contains(",")) {
						List<SelectDTO> selectDTOs = new ArrayList<SelectDTO>();
						SelectDTO selectDTO = null;
						String[] valores = valoresConcat.split(",");
						if (null != valores) {
							for (String valor : valores) {
								selectDTO = new SelectDTO(valor, valor, valor);
								selectDTOs.add(selectDTO);
							}
						}
						compDirDTO.setListaSeleccSelectDTO(selectDTOs);
					}
					break;
				case "AUTORRELLENABLE":
					String valoresConcatAuto = tmct0InformesDatosFiltros.getValorFiltro();
					if (null != valoresConcatAuto && valoresConcatAuto.contains(",")) {
						List<SelectDTO> selectDTOs = new ArrayList<SelectDTO>();
						SelectDTO selectDTO = null;
						String[] valores = valoresConcatAuto.split(",");
						if (null != valores) {
							for (String valor : valores) {
								selectDTO = new SelectDTO(valor, valor, valor);
								selectDTOs.add(selectDTO);
							}
						}
						compDirDTO.setListaSeleccSelectDTO(selectDTOs);
					}

					break;
				case "COMBO":
					compDirDTO.setValorCampo(tmct0InformesDatosFiltros.getValorFiltro());
					break;
				case "TEXTO":
					compDirDTO.setValorCampo(tmct0InformesDatosFiltros.getValorFiltro());
					break;
				default:
					break;
				}
				
				compDirDTOList.add(compDirDTO);
			}
			List<ComponenteDirectivaDTO> datosFiltroInforme = new ArrayList<ComponenteDirectivaDTO>();
			List<ComponenteDirectivaDTO> datosFiltroInformeBis = new ArrayList<ComponenteDirectivaDTO>();

			for (ComponenteDirectivaDTO componenteDirectivaDTO : compDirDTOList) {
				if(componenteDirectivaDTO.getTipo().equals("AUTORRELLENABLE") || componenteDirectivaDTO.getTipo().equals("LISTA") || componenteDirectivaDTO.getSubTipo().equals("DESDE-HASTA")){
					datosFiltroInforme.add(componenteDirectivaDTO);
				}else{
					datosFiltroInformeBis.add(componenteDirectivaDTO);
				}
			}
			datosFiltroInforme.addAll(datosFiltroInformeBis);
			
			for (int i=0;i<datosFiltroInforme.size();i++) {
				datosFiltroInformeBis = new ArrayList<ComponenteDirectivaDTO>();
				for (int j=0;j<4;j++){
					if((i+j)<datosFiltroInforme.size()){
						datosFiltroInformeBis.add(datosFiltroInforme.get(i+j));
					}
				}
				listComponenteDirectivaDTO.add(datosFiltroInformeBis);
				i = i+3;
			}
			
//			listComponenteDirectivaDTO.add(compDirDTOList);
		}
    
	return listComponenteDirectivaDTO;
}

/**
   * Realiza la conversion entre la entidad y el dto
   * @param datosInforme -> Entidad que se quiere trasformar en DTO
   * @return  DTO con los datos de la entidad tratados
 * @throws SIBBACBusinessException 
   */
  public Tmct0InformeDTONEW getTmct0InformeGridDto(Tmct0Informes datosInforme) throws SIBBACBusinessException {
    
    Tmct0InformeDTONEW miDTO = new Tmct0InformeDTONEW();
    
    // Valores directos
    miDTO.setIdInforme(datosInforme.getId());
    miDTO.setNbdescription(datosInforme.getNbdescription());
    miDTO.setNbidioma(datosInforme.getNbidioma());
    miDTO.setNbClase(datosInforme.getTmct0InformesClases().getClase());
    miDTO.setMercado(datosInforme.getMercado());
    miDTO.setModFechaHora(new Date(datosInforme.getModFechaHora().getTime()));
    
	Tmct0Users tmct0Users = tmct0UsersDao.getByUsername(datosInforme.getModUsu());
	if (null != tmct0Users) {
		 miDTO.setModUsu(datosInforme.getModUsu() + " - " + tmct0Users.getName() + " " + tmct0Users.getLastname());
	} else {
		 miDTO.setModUsu("");
	}
    
    // Formamos los campos detalle y las listas de campos detalle, ordenacion, agrupacion, cabecera y separacion
    
    StringBuffer sCamposDetalle = new StringBuffer();
    int i = 0;
            
    for (Tmct0InformesDatos campoInforme : datosInforme.getTmct0InformesDatosList()) {
      if(campoInforme.getId().getIdTipo().getId() == 0) {
          if (i > 0){
            sCamposDetalle.append(",").append(campoInforme.getId().getIdCampo().getNbpantalla());
          }else{
            sCamposDetalle.append(campoInforme.getId().getIdCampo().getNbpantalla());
          }
          i++;          
      }
    }
    
    // Asignamos la cadena de campos detalle
    if (sCamposDetalle != null) {
      miDTO.setCamposDetalle(sCamposDetalle.toString());
    }
    
    // Consultamos los filtros adicionales
    List<Tmct0InformesDatosFiltros> filtrosAdicionales = tmct0InformesDatosFiltrosDao.findByIdInforme(datosInforme.getId());
    if (null != filtrosAdicionales && !filtrosAdicionales.isEmpty()) {
    	miDTO.setFiltrosAdicionales("Si");
    } else {
    	miDTO.setFiltrosAdicionales("No");
    }
    
    //Consultamos si está planificada
    miDTO.setPlanificada("No");
    if (null!=datosInforme.getTmct0InformesPlanificacion() && datosInforme.getTmct0InformesPlanificacion().size()>0) {
    	for (int j=0;j<datosInforme.getTmct0InformesPlanificacion().size();j++) {
    		if (datosInforme.getTmct0InformesPlanificacion().get(j).getEstado() == 'A') {
    			miDTO.setPlanificada("Si");
    			break;
    		}
    	}
    }

    return miDTO;
    
  }
  
  /**
   * Realiza la conversion entre el DTO y la entidad
   * @param datosInformeDTO -> Dto que se quiere trasformar en Entidad
   * @return  Entidad con los datos del DTO tratados
   */
  public Tmct0Informes  getTmct0InformeEntidad ( Tmct0InformeDTONEW datosInformeDTO) {
    
    Tmct0Informes miEntidad = new Tmct0Informes();

    List<Tmct0InformesDatos> tmct0InformesDatosList = new ArrayList<Tmct0InformesDatos>();
    

    if (null != datosInformeDTO.getIdInforme() && datosInformeDTO.getIdInforme()!= 0){
       miEntidad = tmct0InformesDao.findOne(datosInformeDTO.getIdInforme());
    }

    // Valores directos
    miEntidad.setNbdescription(datosInformeDTO.getNbdescription());
    miEntidad.setNbidioma(datosInformeDTO.getNbidioma());
    miEntidad.setTotalizar(datosInformeDTO.getTotalizar());
    miEntidad.setNbtxtlibre(datosInformeDTO.getNbtxtlibre());
    miEntidad.setRellenoCampos(datosInformeDTO.getRellenoCampos());
    miEntidad.setIdPadre(datosInformeDTO.getIdInformePadre());
    miEntidad.setMercado(datosInformeDTO.getMercado());
    miEntidad.setModFechaHora(datosInformeDTO.getModFechaHora());
    miEntidad.setModUsu(datosInformeDTO.getModUsu());
    miEntidad.setGenInicio(datosInformeDTO.getModFechaHora());
    miEntidad.setGenFin(datosInformeDTO.getModFechaHora());
    Tmct0InformesDatos campoInformeEnt;
    Tmct0InformesDatosId datosInformePK;
    Tmct0InformesCampos tmct0InformeCampos ;
    Tmct0InformesTipos  tmct0InformesTipos ;
    Tmct0InformesClases tmct0InformesClases;

    tmct0InformesClases = tmct0InformesClasesDao.findOne(datosInformeDTO.getIdClase());
    
    miEntidad.setTmct0InformesClases(tmct0InformesClases);
    miEntidad.setEstado(Constantes.PLANTILLA_ALTA);     
   
    // Se recorren cada una de las listas y se forman los campos que componen el listado.
    
    // Campos detalle TIPO 0
    int iPosicion = 1;   
    for (SelectDetalleDTO campoInforme : datosInformeDTO.getListaCamposDetalle()) {
      
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(0);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      //campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion,campoInforme.getRenombre(),campoInforme.getTipoRelleno(),campoInforme.getCaracterRelleno().charAt(0),campoInforme.getLongitudEntero(),campoInforme.getLongitudDecimal(),campoInforme.getLongitudAlfanumerico(),campoInforme.getTraduccion());
      if (null != campoInforme) {
    	  campoInformeEnt.setRellenoTipo(campoInforme.getTipoRelleno());
    	  if (null != campoInforme.getCaracterRelleno() && campoInforme.getCaracterRelleno().length() == 1) {
    		  campoInformeEnt.setRelleno(campoInforme.getCaracterRelleno().charAt(0));
    	  } else {
    		  campoInformeEnt.setRelleno(null);
    	  }
    	  campoInformeEnt.setLongitudEntero(campoInforme.getLongitudEntero());
    	  campoInformeEnt.setLongitudDecimal(campoInforme.getLongitudDecimal());
    	  campoInformeEnt.setLongitudAlfanumerico(campoInforme.getLongitudAlfanumerico());
    	  campoInformeEnt.setTraduccion(campoInforme.getTraduccion());
    	  campoInformeEnt.setTotalizar(campoInforme.getDescription().charAt(0));
      }
      if (null != campoInforme.getRenombre()) {
    	  campoInformeEnt.setNombre(campoInforme.getRenombre());
      }
      tmct0InformesDatosList.add(campoInformeEnt);      
      iPosicion++;
    }
    
    // Campos Ordenacion TIPO 1
    iPosicion = 1;
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposOrdenacion()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(1);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      
      
      
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Agrupacion TIPO 2
    iPosicion = 1;
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposAgrupacion()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(2);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Separacion TIPO 3
    iPosicion = 1;
    
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposSeparacion()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(3);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Cabecera TIPO 4
    iPosicion = 1;
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposCabecera()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(4);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Campos Agrupacion Excel TIPO 5
    iPosicion = 1;
    
    for (SelectDTO campoInforme : datosInformeDTO.getListaCamposAgrupacionExcel()) {
      tmct0InformeCampos = new Tmct0InformesCampos();
      tmct0InformeCampos.setId(Integer.parseInt(campoInforme.getKey()));
      tmct0InformesTipos = new Tmct0InformesTipos();
      tmct0InformesTipos.setId(5);
      datosInformePK = new Tmct0InformesDatosId(miEntidad,tmct0InformeCampos,tmct0InformesTipos);
      campoInformeEnt = new Tmct0InformesDatos (datosInformePK,iPosicion);
      tmct0InformesDatosList.add(campoInformeEnt);
      iPosicion++;
    }
    
    // Asigno la lista de campos a la entidad
    miEntidad.setTmct0InformesDatosList(tmct0InformesDatosList);
    
    return miEntidad;
    
  }
  

  /**
   * Realiza la conversion entre los valores recogidos de la pantalla y el DTO
   * @param paramsObjects ->  Valores que recibe el service de la pantalla y que se quieren transformar en DTO.
   * @return  Entidad con los datos del DTO tratados
   * @throws SIBBACBusinessException 
   */
  @SuppressWarnings("unchecked")
public Tmct0InformeDTONEW getTmct0InformeDto(Map<String, Object> paramsObjects) throws SIBBACBusinessException{
    
    
      Tmct0InformeDTONEW miDTO = new Tmct0InformeDTONEW();
      
      StringBuffer sCamposDetalle = new StringBuffer();
      int i = 0;
       
      List<SelectDetalleDTO> listaCamposDetalle  = new ArrayList<SelectDetalleDTO>();
      List<SelectDTO> listaCamposAgrupacion = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposOrdenacion = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposCabecera = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposSeparacion = new ArrayList<SelectDTO>();
      List<SelectDTO> listaCamposAgrupacionExcel = new ArrayList<SelectDTO>();
    

      if (paramsObjects.get("idInforme").toString() != ""){
        miDTO.setIdInforme(Integer.parseInt(paramsObjects.get("idInforme").toString()));  
      }else{
        // Se hace esto porque al guardar la identidad si el id es nulo casca y para que genere uno nuevo tiene que tener el valor 0
        miDTO.setIdInforme(0);
      }
      
        miDTO.setIdClase(Integer.parseInt(((Map<String, String>) paramsObjects.get("clase")).get("id"))); 
        
      if(null != String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key")) && String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key")) != "" ) {
    	  miDTO.setMercado(String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key")));
      }
      
      miDTO.setNbdescription(String.valueOf(paramsObjects.get("nbdescription")));
      
      miDTO.setRellenoCampos(String.valueOf(((Map<String, String>) paramsObjects.get("relleno")).get("key")).charAt(0));
      
      miDTO.setNbidioma(String.valueOf(((Map<String, String>) paramsObjects.get("nbidioma")).get("key")));
      
      miDTO.setTotalizar(String.valueOf(((Map<String, String>) paramsObjects.get("totalizar")).get("key")));
     
      if (paramsObjects.get("nbtxtlibre") != null){
        miDTO.setNbtxtlibre(String.valueOf(paramsObjects.get("nbtxtlibre")));  
      }
      
      miDTO.setModUsu((String) paramsObjects.get("auditUser"));
      
      miDTO.setModFechaHora(new Timestamp(new Date().getTime()));
      
      SelectDTO selectDTO;

      List<Map<String, Object>> camposDetalle = (List<Map<String, Object>>) paramsObjects.get("listaCamposDetalle");
      for(Map<String, Object> cDetalle : camposDetalle){
    	  String tipoRelleno = "";
    	  if (cDetalle.get("tipoRelleno") instanceof String) {
    		  tipoRelleno = (String)cDetalle.get("tipoRelleno");
    	  } else {
    		  tipoRelleno = ((Map<String, String>) cDetalle.get("tipoRelleno")).get("value");
    	  }
    	  SelectDetalleDTO selectDetalleDTO = new SelectDetalleDTO(String.valueOf(cDetalle.get("key")), String.valueOf(cDetalle.get("value")), 
    			  String.valueOf(cDetalle.get("renombre")), String.valueOf(cDetalle.get("traduccion")),
    			  tipoRelleno, String.valueOf(cDetalle.get("caracterRelleno")), 
    			  Integer.parseInt(String.valueOf(cDetalle.get("longitudEntero"))), Integer.parseInt(String.valueOf(cDetalle.get("longitudDecimal"))),
    			  Integer.parseInt(String.valueOf(cDetalle.get("longitudAlfanumerico"))), String.valueOf(cDetalle.get("tipoCampo")),
    			  String.valueOf(cDetalle.get("nombreOriginal")),
    			  String.valueOf(cDetalle.get("description")));
        listaCamposDetalle.add(selectDetalleDTO);
        
        if (i > 0){
          sCamposDetalle.append(",").append(String.valueOf(cDetalle.get("value")));
        }else{
          sCamposDetalle.append(String.valueOf(cDetalle.get("value")));
        }
        i++; 
        
      }
      
      
      miDTO.setCamposDetalle(sCamposDetalle.toString());
      
      List<Map<String,String>> camposAgrupacion = (List<Map<String,String>>) paramsObjects.get("listaCamposAgrupacion");
      for(Map<String,String> cAgrupacion : camposAgrupacion){
        selectDTO = new SelectDTO(String.valueOf(cAgrupacion.get("key")), String.valueOf(cAgrupacion.get("value")));
        listaCamposAgrupacion.add(selectDTO);
      }
      
      List<Map<String,String>> camposOrdenacion = (List<Map<String,String>>) paramsObjects.get("listaCamposOrdenacion");
      for(Map<String,String> cOrdenacion: camposOrdenacion){
        selectDTO = new SelectDTO(String.valueOf(cOrdenacion.get("key")), String.valueOf(cOrdenacion.get("value")));
        listaCamposOrdenacion.add(selectDTO);
      }
      
      List<Map<String,String>> camposCabecera = (List<Map<String,String>>) paramsObjects.get("listaCamposCabecera");
      for(Map<String,String> cCabecera : camposCabecera){
        selectDTO = new SelectDTO(String.valueOf(cCabecera.get("key")), String.valueOf(cCabecera.get("value")));
        listaCamposCabecera.add(selectDTO);
      }
      
      List<Map<String,String>> camposSeparacion = (List<Map<String,String>>) paramsObjects.get("listaCamposSeparacion");
      for(Map<String,String> cSeparacion : camposSeparacion){
        selectDTO = new SelectDTO(String.valueOf(cSeparacion.get("key")), String.valueOf(cSeparacion.get("value")));
        listaCamposSeparacion.add(selectDTO);
      }
      
      List<Map<String,String>> camposAgrupacionExcel = (List<Map<String,String>>) paramsObjects.get("listaCamposAgrupacionExcel");
      for(Map<String,String> cAgrupacionExcel : camposAgrupacionExcel){
        selectDTO = new SelectDTO(String.valueOf(cAgrupacionExcel.get("key")), String.valueOf(cAgrupacionExcel.get("value")));
        listaCamposAgrupacionExcel.add(selectDTO);
      }
      
      miDTO.setListaCamposDetalle(listaCamposDetalle);
      miDTO.setListaCamposOrdenacion(listaCamposOrdenacion);
      miDTO.setListaCamposAgrupacion(listaCamposAgrupacion);
      miDTO.setListaCamposSeparacion(listaCamposSeparacion);
      miDTO.setListaCamposCabecera(listaCamposCabecera);
      miDTO.setListaCamposAgrupacionExcel(listaCamposAgrupacionExcel);
      
      /** Inicio - Componente filtros dinamicos. */
      // Datos del componente generico filtros dinamicos.
      miDTO.setListComponenteDirectivaDTO(
      	this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOFromLHM((List<List<LinkedHashMap>>) paramsObjects.get("listaFiltrosDinamicos")));
      /** Fin - Componente filtros dinamicos. */
      return miDTO;
    
  }
  
  public Tmct0InformeFilterDTO getTmct0InformeFilterDto(Map<String, Object> paramsObjects){
    
    
      Tmct0InformeFilterDTO miFilterDTO = new Tmct0InformeFilterDTO();
      
      if (null!=String.valueOf(paramsObjects.get("plantilla")) && !String.valueOf(paramsObjects.get("plantilla")).equals("")) {
    	  miFilterDTO.setPlantilla(String.valueOf(((Map<String, String>) paramsObjects.get("plantilla")).get("key")));
      } else {
    	  miFilterDTO.setPlantilla(String.valueOf(paramsObjects.get("plantilla")));
      }
 
      miFilterDTO.setNotPlantilla(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notPlantilla"))));
      if (null!=String.valueOf(paramsObjects.get("clase")) && !String.valueOf(paramsObjects.get("clase")).equals("")) {
    	  miFilterDTO.setClase(String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("id")));
      }
      miFilterDTO.setNotClase(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notClase"))));
      if (null!=String.valueOf(paramsObjects.get("idioma")) && !String.valueOf(paramsObjects.get("idioma")).equals("")) {
    	  miFilterDTO.setIdioma(String.valueOf(((Map<String, String>) paramsObjects.get("idioma")).get("key")));
      }
      miFilterDTO.setNotIdioma(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notIdioma"))));
      if (null!=String.valueOf(paramsObjects.get("mercado")) && !String.valueOf(paramsObjects.get("mercado")).equals("")) {
    	  miFilterDTO.setMercado(String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key")));
      }
      miFilterDTO.setNotMercado(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notMercado"))));
      
      SelectDTO selectDTO; 
      List<SelectDTO> listaCamposDetalle  = new ArrayList<SelectDTO>();
      List<Map<String,String>> camposDetalle = (List<Map<String,String>>) paramsObjects.get("camposDetalle");
      for(Map<String,String> cDetalle : camposDetalle){
        selectDTO = new SelectDTO(String.valueOf(cDetalle.get("key")), String.valueOf(cDetalle.get("value")));
        listaCamposDetalle.add(selectDTO);
      }
      
      miFilterDTO.setCamposDetalle(listaCamposDetalle);
      miFilterDTO.setNotCamposDetalle(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notCamposDetalle"))));
      
      return miFilterDTO;
    
  }

  public Tmct0GeneracionInformeFilterDTO getTmct0GeneracionInformeFilterDto(
	Map<String, Object> paramsObjects) throws ParseException, SIBBACBusinessException {

    LOG.debug("Inicio de la transformación Tmct0GeneracionInformeFilterDTO");
    
    Tmct0GeneracionInformeFilterDTO miFilterDTO = new Tmct0GeneracionInformeFilterDTO();
    
    // Datos del componente generico filtros dinamicos.
    miFilterDTO.setListComponenteDirectivaDTO(
    	this.tmct0InformesFiltrosAssembler.getListComponenteDirectivaDTOFromLHM(
    		(List<List<LinkedHashMap>>) paramsObjects.get("listaFiltrosDinamicos")));
    
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");    
    if (paramsObjects.get("plantilla") != ""){
      miFilterDTO.setIdInforme(Integer.parseInt(String.valueOf(paramsObjects.get("plantilla"))));
    }
   
    if (paramsObjects.get("especiales") != "") {
      miFilterDTO.setOpcionesEspeciales(String.valueOf(paramsObjects.get("especiales")));
    }
    
    LOG.debug("Fin de la transformación Tmct0GeneracionInformeFilterDTO");
    return miFilterDTO;
  }
  
  /**
   *	Complementa la query del componente generico.
   *	@param listComponenteDirectivaDTO
   *	@return complQuery
   *	@throws SIBBACBusinessException 
 * @throws ParseException 
   */
  public String getQueryFromComponenteGenerico(
		  List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO, String query, Date fechaEjecucion, int posicionMercado, String fechaBase) throws SIBBACBusinessException, ParseException {
	  StringBuffer complQuery = new StringBuffer();
	  SimpleDateFormat sdfDB = new SimpleDateFormat("yyyy-MM-dd");
	  SimpleDateFormat inSDF = new SimpleDateFormat("dd/MM/yyyy");
	  if (CollectionUtils.isNotEmpty(listComponenteDirectivaDTO)) {
		  for (List<ComponenteDirectivaDTO> compDirDTOList : listComponenteDirectivaDTO) {
			  for (ComponenteDirectivaDTO compDirDTO : compDirDTOList) {
		        	String[] listaCdtabla = compDirDTO.getTabla().split("/",-1);
		        	String[] listaCampos = compDirDTO.getCampo().split("/",-1);
		        	String cdtabla = "";
		        	String campo = "";
		        	if (listaCdtabla.length>1) {
		        		cdtabla = listaCdtabla[posicionMercado];
		        	} else {
		        		cdtabla = listaCdtabla[0];
		        	}
		        	
		        	if (listaCampos.length>1) {
		        		campo = listaCampos[posicionMercado];
		        	} else {
		        		campo = listaCampos[0];
		        	}
				  if (StringUtils.isNotBlank(cdtabla) && query.toUpperCase().contains(cdtabla.toUpperCase())) {
	
					  // Para tipo AUTORRELLENABLE se compara con el label de sentido.
					  StringBuffer sbComplemento = new StringBuffer();
					  if (compDirDTO.isTipoAutorrellenable() && compDirDTO.getLblBtnSentidoFiltro().equals("<>")) {
						  sbComplemento.append(" NOT ");
					  }
					  
					  // Caso en que se pasa un valor si o si que se va a usar posteriormente.
					  if (StringUtils.isNotBlank(compDirDTO.getValorCampo()) || CollectionUtils.isNotEmpty(compDirDTO.getListaSeleccSelectDTO())) {
						  if (compDirDTO.isTipoFecha()) {
							  if (compDirDTO.isSubtipoDesdeHasta() && !fechaBase.equals(campo)) {
								  if (StringUtils.isNotBlank(compDirDTO.getValorCampo())) {
									  if (!compDirDTO.getValorCampo().equals("01/01/1900")) {
										  complQuery.append(" AND DATE(" + sbComplemento.toString() + campo+")");
										  complQuery.append(" >= TO_DATE('" + sdfDB.format(inSDF.parse(compDirDTO.getValorCampo())) + "', 'YYYY-MM-DD')");
									  }
									  if (!compDirDTO.getValorCampoHasta().equals("31/12/9999")) {
										  complQuery.append(" AND DATE(" + sbComplemento.toString() + campo+")");
										  if (StringUtils.isNotBlank(compDirDTO.getValorCampoHasta())) {
											  complQuery.append(" <= TO_DATE('" + sdfDB.format(inSDF.parse((compDirDTO.getValorCampoHasta()))) + "', 'YYYY-MM-DD')");
										  } else{
											  complQuery.append(" <= TO_DATE('" + sdfDB.format(new Date()) + "', 'YYYY-MM-DD')");
										  }
									  }
								  } else if (StringUtils.isNotBlank(compDirDTO.getValorCampoHasta())) {
									  if (!compDirDTO.getValorCampoHasta().equals("31/12/9999")) {
										  complQuery.append(" AND DATE(" + sbComplemento.toString() + campo+")");
										  if (StringUtils.isNotBlank(compDirDTO.getValorCampoHasta())) {
											  complQuery.append(" <= TO_DATE('" + sdfDB.format(inSDF.parse((compDirDTO.getValorCampoHasta()))) + "', 'YYYY-MM-DD')");
										  } else {
											  complQuery.append(" <= TO_DATE('" + sdfDB.format(new Date()) + "', 'YYYY-MM-DD')");
										  }
									  }
								  }
								  
							  } else if (fechaBase.equals(campo) || !compDirDTO.isSubtipoDesdeHasta()) {
								  complQuery.append(" AND DATE(" + sbComplemento.toString() /*+ cdtabla + "."*/ + campo+")");
							  }
						  } else if (!compDirDTO.isTipoTexto() && !compDirDTO.isTipoLista()) {
							  complQuery.append(" AND " + sbComplemento.toString() /*+ cdtabla + "."*/ + campo);
						  }
						  
					  }
					  
					  // Tipos que usan valorCampo
					  if (StringUtils.isNotBlank(compDirDTO.getValorCampo())) {
						  if (compDirDTO.isTipoCombo()) {
							  // Para tipos COMBO solo se compara con un valor.
							  complQuery.append(" = '" + compDirDTO.getValorCampo() + "'");
						  } else if (compDirDTO.isTipoFecha()) {
							  if ((fechaBase.equals(campo))){
								  try {
									  complQuery.append(" = TO_DATE('" + sdfDB.format(fechaEjecucion) + "', 'YYYY-MM-DD')");

								  } catch (Exception e) {
									  LOG.info("Error al parsear fecha: " + e);
								  }
							  } else if(!compDirDTO.isSubtipoDesdeHasta()){
								  complQuery.append(" = TO_DATE('" + sdfDB.format(inSDF.parse((compDirDTO.getValorCampo()))) + "', 'YYYY-MM-DD')");
							  }
						  }
						  
						  if(compDirDTO.isTipoNumero()) {
							  if(compDirDTO.isSubtipoDesdeHasta()){
								  if (StringUtils.isNotBlank(compDirDTO.getValorCampo()) && StringUtils.isNotBlank(compDirDTO.getValorCampoHasta())) {
									  complQuery.append(" >= " + compDirDTO.getValorCampo()
											  + " AND " /*+ cdtabla + "."*/ + campo 
											  + " <= " + compDirDTO.getValorCampoHasta());
								  }else if (StringUtils.isNotBlank(compDirDTO.getValorCampo())) {
									  complQuery.append(" >= " + compDirDTO.getValorCampo());
								  } else if (StringUtils.isNotBlank(compDirDTO.getValorCampoHasta())) {
									  complQuery.append(" <= " + compDirDTO.getValorCampoHasta());
								  }
							  }else{
								  complQuery.append(" = " + compDirDTO.getValorCampo());
							  }
						  }
					  }
					  
					  // Tipos que usan listaSeleccSelectDTO
					  if (CollectionUtils.isNotEmpty(compDirDTO.getListaSeleccSelectDTO())) {
						  if (compDirDTO.isTipoAutorrellenable()) {
							  // Para tipos LISTA se usa operador IN ya que pueden venir varios valores
							  //original complQuery.append(" IN('" + GeneracionInformesHelper.getDatosListaFormateados(compDirDTO.getListaSeleccSelectDTO()) + "')");
							  complQuery.append(" IN(" + GeneracionInformesHelper.getDatosListaFormateados(compDirDTO.getListaSeleccSelectDTO()) + ")");
						  } else if (compDirDTO.isTipoTexto() || compDirDTO.isTipoLista()) {
							  if (compDirDTO.getLblTipoBusqueda().equals("I")) {
								  complQuery.append(" AND "+campo+" IN(" + GeneracionInformesHelper.getDatosListaFormateados(compDirDTO.getListaSeleccSelectDTO()) + ")");
							  } else if (compDirDTO.getLblTipoBusqueda().equals("D")) {
								  complQuery.append(" AND "+campo+" NOT IN(" + GeneracionInformesHelper.getDatosListaFormateados(compDirDTO.getListaSeleccSelectDTO()) + ")");
							  } else if (compDirDTO.getLblTipoBusqueda().equals("C")) {
								  complQuery.append(" AND (" + GeneracionInformesHelper.getDatosListaFormateadosLike(compDirDTO.getListaSeleccSelectDTO(),campo) + ")");
							  } else if (compDirDTO.getLblTipoBusqueda().equals("NC")) {
								  complQuery.append(" AND (" + GeneracionInformesHelper.getDatosListaFormateadosNotLike(compDirDTO.getListaSeleccSelectDTO(),campo) + ")");
							  }
						  }
					  }
				  }
			  }
		  }
	  }
	  return complQuery.toString();
  }
  
  /**
   * 	Se populan datos preseteados de los filtros para utilizarse en la Task.
   * 	@param listaElementosGenericos
   * 	@return List<LinkedHashMap>
   * 	@throws SIBBACBusinessException	
   */
  public ArrayList<ArrayList<LinkedHashMap<String, Object>>> getListMapByInformeIdForTask(List<Object[]> listaElementosGenericos) throws SIBBACBusinessException {
	  ArrayList<ArrayList<LinkedHashMap<String, Object>>> listaListaCompDirLinkedHM = new ArrayList<ArrayList<LinkedHashMap<String, Object>>>();
	  ArrayList<LinkedHashMap<String, Object>> listaCompDirLinkedHM = new ArrayList<LinkedHashMap<String, Object>>();
	  
	  if (CollectionUtils.isNotEmpty(listaElementosGenericos)) {
		  for (Object[] elemGenerico : listaElementosGenericos) {
			  LinkedHashMap<String, Object> elemGenHM = new LinkedHashMap<String, Object>(); 
			  // ID de Informe
			  elemGenHM.put("id", elemGenerico[0]);
			  // Nombre del filtro
			  elemGenHM.put("nombre", elemGenerico[1]);
			  // Tipo de filtro
			  elemGenHM.put("tipo", elemGenerico[2]);
			  // SubTipo de filtro
			  elemGenHM.put("subTipo", elemGenerico[3]);
			  // Tabla
			  elemGenHM.put("tabla", elemGenerico[4]);
			  // Campo
			  elemGenHM.put("campo", elemGenerico[5]);
			  // Valor
			  elemGenHM.put("valor", elemGenerico[6]);
			  // Query
			  elemGenHM.put("query", elemGenerico[7]);
			  if (elemGenHM.get("tipo").equals(TipoCompInfFiltroEnum.TEXTO.getTipoComponente()) || elemGenHM.get("tipo").equals(TipoCompInfFiltroEnum.AUTORRELLENABLE.getTipoComponente()) || elemGenHM.get("tipo").equals(TipoCompInfFiltroEnum.LISTA.getTipoComponente())) {
				  // listaSeleccSelectDTO
				  List<LinkedHashMap> listaMap = new ArrayList<LinkedHashMap>();
				  if ((elemGenerico[8]).toString().contains(",")) {
					  String[] arrayLista = (elemGenerico[8]).toString().split(",",-1);
					  for (String cadena : arrayLista) {
						  LinkedHashMap lhm = new LinkedHashMap();
						  lhm.put("key", cadena);
						  lhm.put("value", cadena);
						  lhm.put("description", "");
						  listaMap.add(lhm);
					  }
				  } else {
					  LinkedHashMap lhm = new LinkedHashMap();
					  lhm.put("key", elemGenerico[8].toString());
					  lhm.put("value", elemGenerico[8].toString());
					  lhm.put("description", "");
					  listaMap.add(lhm);
				  }
				  elemGenHM.put("listaSeleccSelectDTO", listaMap); 
			  } else {
				  // valorCampo
				  elemGenHM.put("valorCampo", elemGenerico[8]);
			  }
			  
			  // Caso fecha Desde-hasta delimitado por comas
			  if ((elemGenHM.get("tipo").equals(TipoCompInfFiltroEnum.FECHA.getTipoComponente()) || elemGenHM.get("tipo").equals(TipoCompInfFiltroEnum.NUMERO.getTipoComponente())) 
					  && elemGenHM.get("subTipo").equals(SubtipoCompInfFiltroEnum.DESDE_HASTA.getSubtipoComponente())) {
				  if (elemGenHM.get("valorCampo") != null && ((String) elemGenHM.get("valorCampo")).contains(",")) {
					  String[] arrayValorCampo = ((String) elemGenHM.get("valorCampo")).split(",",-1);
					  elemGenHM.put("valorCampo", arrayValorCampo[0] != null ? ((String) arrayValorCampo[0]).trim(): null);
					  elemGenHM.put("valorCampoHasta", arrayValorCampo[1] != null ? ((String) arrayValorCampo[1]).trim(): null);
				  }
			  }
			  
			  listaCompDirLinkedHM.add(elemGenHM);
		  }
		  listaListaCompDirLinkedHM.add(listaCompDirLinkedHM);
	  }
	  return listaListaCompDirLinkedHM;
  }
  
  /**
   * 	Devuelve una fecha corroborada como dia habil en forma de Sring.
   * 	@param fecha
   * 	@param operador: si es: 
   * 	(+) -> incrementa hasta encontrar siguiente dia habil 
   * 	(-) -> Decrementa hasta encontrar siguiente dia habil
   * 	@throws SIBBACBusinessException 
   * 	@throws ParseException 
   */
  public String getFechaHabil(
  	  String fecha, String operador) throws SIBBACBusinessException, ParseException {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date fechaDate = sdf.parse(fecha);
    Calendar c = Calendar.getInstance();
    c.setTime(fechaDate);
    while (!this.isDiaHabil(fechaDate)) {
  	  if (operador.equals("+")) {
  		  c.add(Calendar.DATE, 1);
  		  fechaDate = c.getTime();
  	  } else if (operador.equals("-")) {
  		  c.add(Calendar.DATE, (-1) * 1);
  		  fechaDate = c.getTime();
  	  } else if (operador.equals("=")) {
  		  fechaDate = c.getTime();
  		  break;
  	  }
    }
    return sdf.format(fechaDate);
  }
  
  /**
   *	Devuelve true si se devuelve un dia habil (ni feriado ni festivo). 
   * 	@return boolean
   * 	@throws SIBBACBusinessException
   */
  private boolean isDiaHabil(Date fechaEjecucion) throws SIBBACBusinessException {
	  return !Utilidad.isWeekEnd(fechaEjecucion) 
			  && CollectionUtils.isEmpty(this.tmct0cfgDao.findByProcessKeyNameLikeAndKeyValue(
					  "CONFIG", "%holydays.anual.days%", formatDate.format(fechaEjecucion)));
  }
}
