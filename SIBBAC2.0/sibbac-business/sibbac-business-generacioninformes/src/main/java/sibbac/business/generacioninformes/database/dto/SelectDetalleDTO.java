package sibbac.business.generacioninformes.database.dto;

/**
 * DTO para cargar las listas en la vista con angular
 * 
 * @author mario.flores
 *
 */
public class SelectDetalleDTO {

  /**
   * key - Clave del elemento
   */
  private String key;
  /**
   * Value - Valor del elemento
   */
  private String value;

  /**
   * description - Descripcion adicional
   */
  private String description;
  
  /**
   * traduccion - usado para la edicion del campo
   */
  private String traduccion;
  
  /**
   * renombre - usado para la edicion del campo
   */
  private String renombre;
  
  /**
   * tipoRelleno - usado para la edicion del campo
   */
  private String tipoRelleno;
  
  /**
   * caracterRelleno - usado para la edicion del campo
   */
  private String caracterRelleno;
  
  /**
   * longitudEntero - usado para la edicion del campo
   */
  private Integer longitudEntero;
  
  /**
   * longitudDecimal - usado para la edicion del campo
   */
  private Integer longitudDecimal;
  
  /**
   * longitudAlfanumerico - usado para la edicion del campo
   */
  private Integer longitudAlfanumerico;
  
  /**
   * tipoCampo - usado para la edicion del campo
   */
  private String tipoCampo;
  
  /**
   * nombreOriginal - usado para la edicion del campo
   */
  private String nombreOriginal;

public SelectDetalleDTO(String key, String value) {

    this.key = key;
    this.value = value;
  }

  public SelectDetalleDTO(String key, String value, String description) {
    this.key = key;
    this.value = value;
    this.description = description;
  }

  public SelectDetalleDTO(Long id, String descripcion) {
	this.key = String.valueOf(id);
	this.value = descripcion;
	this.description = descripcion;
  }

  public SelectDetalleDTO(String key, String value, String renombre, String traduccion, String tipoRelleno, String caracterRelleno, Integer longitudEntero, Integer longitudDecimal, Integer longitudAlfanumerico, String tipoCampo, String nombreOriginal, String descripcion) {
	this.key = key;
	this.value = value;
	this.renombre = renombre;
	this.traduccion = traduccion;
	this.tipoRelleno=tipoRelleno;
	this.caracterRelleno=caracterRelleno;
	this.longitudEntero=longitudEntero;
	this.longitudDecimal=longitudDecimal;
	this.longitudAlfanumerico=longitudAlfanumerico;
	this.setTipoCampo(tipoCampo);
	this.nombreOriginal=nombreOriginal;
	this.description = descripcion;
  }

public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((key == null) ? 0 : key.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SelectDetalleDTO other = (SelectDetalleDTO) obj;
    if (description == null) {
      if (other.description != null)
        return false;
    } else if (!description.equals(other.description))
      return false;
    if (key == null) {
      if (other.key != null)
        return false;
    } else if (!key.equals(other.key))
      return false;
    if (value == null) {
      if (other.value != null)
        return false;
    } else if (!value.equals(other.value))
      return false;
    return true;
  }

public String getTraduccion() {
	return traduccion;
}

public void setTraduccion(String traduccion) {
	this.traduccion = traduccion;
}

public String getRenombre() {
	return renombre;
}

public void setRenombre(String renombre) {
	this.renombre = renombre;
}

/**
* @return the tipoRelleno
*/
public String getTipoRelleno() {
	return tipoRelleno;
}

/**
* @param tipoRelleno the tipoRelleno to set
*/
public void setTipoRelleno(String tipoRelleno) {
	this.tipoRelleno = tipoRelleno;
}

/**
* @return the caracterRelleno
*/
public String getCaracterRelleno() {
	return caracterRelleno;
}

/**
* @param caracterRelleno the caracterRelleno to set
*/
public void setCaracterRelleno(String caracterRelleno) {
	this.caracterRelleno = caracterRelleno;
}

/**
* @return the longitudEntero
*/
public Integer getLongitudEntero() {
	return longitudEntero;
}

/**
* @param longitudEntero the longitudEntero to set
*/
public void setLongitudEntero(Integer longitudEntero) {
	this.longitudEntero = longitudEntero;
}

/**
* @return the longitudDecimal
*/
public Integer getLongitudDecimal() {
	return longitudDecimal;
}

/**
* @param longitudDecimal the longitudDecimal to set
*/
public void setLongitudDecimal(Integer longitudDecimal) {
	this.longitudDecimal = longitudDecimal;
}

/**
* @return the longitudAlfanumerico
*/
public Integer getLongitudAlfanumerico() {
	return longitudAlfanumerico;
}

/**
* @param longitudAlfanumerico the longitudAlfanumerico to set
*/
public void setLongitudAlfanumerico(Integer longitudAlfanumerico) {
	this.longitudAlfanumerico = longitudAlfanumerico;
}

/**
* @return the nombreOriginal
*/
public String getNombreOriginal() {
	return nombreOriginal;
}

/**
* @param nombreOriginal the nombreOriginal to set
*/
public void setNombreOriginal(String nombreOriginal) {
	this.nombreOriginal = nombreOriginal;
}

/**
 * @return the tipoCampo
 */
public String getTipoCampo() {
	return tipoCampo;
}

/**
 * @param tipoCampo the tipoCampo to set
 */
public void setTipoCampo(String tipoCampo) {
	this.tipoCampo = tipoCampo;
}
}
