package sibbac.business.generacioninformes.rest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.generacioninformes.database.bo.Tmct0InformesBo;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dto.Tmc0InformeCeldaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeColumnaDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dao.Tmct0GeneraInformeDaoImpl;
import sibbac.business.wrappers.database.dto.ValoresDTO;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceGenerarInformes implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGenerarInformes.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  private static final String FORMATO_FECHA_ES = "dd/MM/yyyy";

  private static final String FORMATO_HORA_ES = "HH:mm:ss";

  private static final String FORMATO_MONTOS = "#,##0.00";

  private static final int TAMANIO_FUENTE_DEFECTO = 8;

  private static final String FUENTE_DEFECTO = "Calibri";

  @Value("${generacioninformes.alias.routing}")
  private String ROUTING;

  @Value("${generacioninformes.limite.registros.excel:50000}")
  // Se pone un valor por defecto.
  private int limiteRegistrosExcel;

  @Value("${generacioninformes.limite.excel.consulta:2000}")
  // Se pone un valor por defecto
  private int limiteRegistrosExcelConsulta;

  @Value("${generacioninformes.alias.barrido}")
  private String BARRIDO;

  @Value("${mail.source.folder}")
  private String TEMPLATES_FOLDER;

  @Autowired
  Tmct0InformesCamposDao tmct0InformesCamposDao;

  @Autowired
  Tmct0InformesDao tmct0InformesDao;

  @Autowired
  private Tmct0aliDao aliDao; // Para obtener los alias

  @Autowired
  private Tmct0ValoresBo valoresBo; // Para obtener los isin

  @Autowired
  Tmct0InformesBo tmct0InformesBo;

  @Autowired
  Tmct0GeneraInformeDaoImpl tmct0GeneraInformeDaoImpl;

  @Autowired
  Tmct0InformeAssembler tmct0InformeAssembler;

  /**
   * The Enum para los tipos de campos del informe
   */
  public enum EnuTipoCampo {

    DETALLE(0),
    ORDENACION(1),
    AGRUPACION(2),
    SEPARADOR(3);

    private final int value;

    EnuTipoCampo(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

  }

  /**
   * The Enum ComandosPlantillas.
   */
  private enum ComandosPlantillas {

    CARGA_INFORME_XLS("ExportaInformeXLS"),
    CARGA_INFORME_TERCEROS_XLS("ExportaInformeTercerosXLS"),
    CARGA_GENERACION_INFORMES("CargaGeneracionInformes"),
    /** Inicializa los datos del filtro de pantalla */
    CARGA_ALIAS("CargaAlias"),
    CARGA_ISIN("CargaIsin"),
    CARGA_PLANTILLAS("CargaPlantillas"),
    OBTENER_QUERY_INFORME("ObtenerQueryInforme"),
    SIN_COMANDO("");

    /** The command. */
    private final String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandosPlantillas(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    private String getCommand() {
      return command;
    }

  }

  /**
   * The Enum Campos plantilla .
   */
  private enum FieldsPlantilla {
    PLANTILLA("plantilla");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsPlantilla(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    private String getField() {
      return field;
    }
  }

  /**
   * Process.
   *
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosPlantillas command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceGenerarInformes");

      switch (command) {
        case CARGA_GENERACION_INFORMES:
          result.setResultados(getInformeGenerado(paramsObjects));
          break;
        case CARGA_ALIAS:
          result.setResultados(cargarAlias());
          break;
        case CARGA_ISIN:
          result.setResultados(cargarIsin());
          break;
        case CARGA_PLANTILLAS:
          result.setResultados(cargarPlantillas());
          break;
        case CARGA_INFORME_XLS:
          result.setResultados(getInformeExcel(paramsObjects, false, true));
          break;
        case CARGA_INFORME_TERCEROS_XLS:
          result.setResultados(getInformeExcel(paramsObjects, true, false));
          break;
        case OBTENER_QUERY_INFORME:
          result.setResultados(getQueryInforme(paramsObjects));
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceGenerarInformes");

    return result;
  }

  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<>();
    FieldsPlantilla[] fields = FieldsPlantilla.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    return Collections.emptyList();
  }

  /**
   * Obtencion de las plantillas actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPlantillas() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarPlantillas");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      // Inicializo los limites de registros en el tratamiendo de las excel.
      mSalida.put("limite_total", limiteRegistrosExcel);

      List<SelectDTO> listaInformes = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Tmct0Informes> listaPlantillasInformes = tmct0InformesDao.findInformesOrderByNbdescription();
      for (Tmct0Informes tmct0Informes : listaPlantillasInformes) {
        select = new SelectDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription());
        listaInformes.add(select);
      }
      mSalida.put("listaInformes", listaInformes);

    } catch (Exception e) {
      LOG.error("Error metodo init - Generador Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.trace("Fin cargar lista cargarPlantillas");

    return mSalida;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarAlias() throws SIBBACBusinessException {

    LOG.debug("Iniciando datos para el filtro de Generacion Informes - cargarAlias");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Calendar cal = Calendar.getInstance();
      Integer fechaActivo = Integer.parseInt(sdf.format(cal.getTime()));
      List<Tmct0aliDTO> listAlias = aliDao.findAllAli(fechaActivo);

      for (Tmct0aliDTO tmct0aliDTO : listAlias) {
        select = new SelectDTO(tmct0aliDTO.getAlias(), tmct0aliDTO.getAlias() + " -  " + tmct0aliDTO.getDescripcion());
        listaAlias.add(select);
      }
      map.put("listaAlias", listaAlias);

    } catch (Exception e) {
      LOG.error("Error metodo init - Generacion Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException(
                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarAlias");
    }

    LOG.debug("Fin inicializacion datos filtro de Generacion Informes - cargarAlias");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarIsin() throws SIBBACBusinessException {

    LOG.debug("Iniciando datos para el filtro de Generacion Informes - cargarIsin");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaIsin = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      List<Tmct0Valores> listIsin = valoresBo.findAll();
      List<ValoresDTO> listIsinDTO = valoresBo.entitiesListToAnDTOList(listIsin);

      for (ValoresDTO valoresDTO : listIsinDTO) {
        select = new SelectDTO(valoresDTO.getCodigo(), valoresDTO.getCodigo() + " -  " + valoresDTO.getDescripcion());
        listaIsin.add(select);
      }

      map.put("listaIsin", listaIsin);

    } catch (Exception e) {
      LOG.error("Error metodo init - Generacion Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda - cargarIsin");
    }

    LOG.debug("Fin inicializacion datos filtro de Generacion Informes - cargarIsin");

    return map;
  }

  /**
   * Generar query del informe dinamico de los filtros pasados (los filtros contienen la plantilla del informe)
   * 
   * @param paramsObjects
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getQueryInforme(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Entra en getQueryInforme");

    Map<String, Object> result = new HashMap<String, Object>();
    Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();

    try {
      // Se recupera el informe solicitado en los parametros.
      Tmct0GeneracionInformeFilterDTO filters = tmct0InformeAssembler.getTmct0GeneracionInformeFilterDto(paramsObjects);
      LOG.debug("filters.getIdInforme() " + filters.getIdInforme());

      Tmct0Informes informe = tmct0InformesBo.getInforme(filters.getIdInforme());

      dto = tmct0InformesBo.queryInforme(filters, informe);
      result.put("informe", dto);

    } catch (Exception e) {
      LOG.error("Error metodo exportaInformeExcel -  " + e.getMessage(), e.getMessage());
      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
    }

    LOG.debug("Sale de getQueryInforme");

    return result;

  }

  /**
   * Generar informe dinamico de los filtros pasados (los filtros contienen la plantilla del informe)
   * 
   * @param paramsObjects
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getInformeGenerado(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
    LOG.debug("Entra en getInformeGenerado");
    return getInformeExcel(paramsObjects, true, true);
  }

  /**
   * Generar informe dinamico en excel de los filtros pasados (los filtros contienen la plantilla del informe)
   * 
   * @param paramsObjects: Objeto de parametros de tipo getTmct0GeneracionInformeFilterDto generarExcelSimple:
   *          Especificación de si se debe generar el excel simple con la información
   * @param getTerceros es cuenta terceros
   * @param getSimple es simple
   * @return Mapa para generación en Excel
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getInformeExcel(Map<String, Object> paramsObjects, boolean getTerceros, boolean getSimple) throws SIBBACBusinessException {

    LOG.debug("Entra en exportaInformeExcel");

    Map<String, Object> result = new HashMap<>();
    Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();
    XSSFWorkbook workbook = null;
    XSSFWorkbook workbookSimple = null;
    TreeMap<String, List<Tmc0InformeCeldaDTO>> tResultados = new TreeMap<>();
    List<Integer> iColumnasTotalizan = new ArrayList<>();
    List<Integer> iColumnasOrdencacion = new ArrayList<>();
    List<Tmct0InformeColumnaDTO> columnasInforme = new ArrayList<>();
    List<Tmct0InformeGeneradoDTO> lDatosPorDias;
    List<List<Tmc0InformeCeldaDTO>> lDatosFinales = new ArrayList<>();
    List<Date> lFechasEjecucion = new ArrayList<>();

    try {

      // Se recupera el informe solicitado en los parametros. No se inicializa el objeto ensamblador por estar en
      // autowired.
      Tmct0GeneracionInformeFilterDTO filters = tmct0InformeAssembler.getTmct0GeneracionInformeFilterDto(paramsObjects);
      Tmct0Informes informe = tmct0InformesBo.getInforme(filters.getIdInforme());

      // Se obtiene tambien la plantilla.

      Tmct0InformeDTO plantilla = tmct0InformeAssembler.getTmct0InformeDto(informe);

      // se escribe la traza del informe ejecutado en el log de la aplicación
      logInforme(filters, informe.getNbdescription());

      // Se inicializa el dto con toda la información del informe a ejecutar.
      dto = tmct0InformesBo.queryInforme(filters, informe);

      // Lo ejecuto total
      // dto = tmct0InformesBo.ejecutaInforme(filters, informe);

      // Lo ejecuto por dias.
      // Se obtienen las fechas de ejecucion
      lFechasEjecucion = getFechasEjecucion(filters.getFechaContratacionDesde(), filters.getFechaContratacionHasta());

      // Se llama al metodo del bo que ejecuta el informe tantas veces como dias se hayan obtenido
      // Para cada dia se crea un hilo de ejecucion
      lDatosPorDias = tmct0InformesBo.ejecutaInformePorDias(lFechasEjecucion, filters, informe);

      // Se procesan todos las listas y se unen en un map con clave:
      // -- Si el informe no agrupa, la clave esta formada por todos los campos.
      // -- Si el informe agrupa, la clave esta formada por los campos que agrupan y no totalizan

      // Obtenemos las columnas que totalizan
      LOG.debug("Se obtienen las columnas que totalizan");
      columnasInforme = dto.getColumnas();

      for (int i = 0; i < columnasInforme.size(); i++) {

        if (columnasInforme.get(i).isTotal()) {
          iColumnasTotalizan.add(Integer.valueOf(i));
        }
      }

      // Se obtienen las columnas de ordenacion
      LOG.debug("Se obtienen las columnas de ordenacion");
      for (int i = 0; i < plantilla.getListaCamposOrdenacion().size(); i++) {
        int iClaveCampoOrdenacion = Integer.parseInt(plantilla.getListaCamposOrdenacion().get(i).getKey());
        // Se busca la posicion dentro de la lista de campos detalle.
        for (int j = 0; j < plantilla.getListaCamposDetalle().size(); j++) {
          if (Integer.parseInt(plantilla.getListaCamposDetalle().get(j).getKey()) == iClaveCampoOrdenacion) {
            iColumnasOrdencacion.add(j);
          }
        }

      }

      LOG.debug("Se unen todos los resultados ordenados y agrupados");
      List<Tmc0InformeCeldaDTO> auxSumaTotales; // Variable auxiliar para ir sumando las registros duplicados

      int iNumeroFila = 0;

      for (Tmct0InformeGeneradoDTO datosUnDia : lDatosPorDias) {

        for (List<Tmc0InformeCeldaDTO> tmct0InformeFila : datosUnDia.getResultados()) {

          iNumeroFila++;

          // Se forma la clave. esta va a estar formada por los campos de ordenacion mas los campos de ag
          final String sClave;

          final StringBuffer sCamposDetalle = new StringBuffer();

          // Para cuando el informe no esta agrupado y tengo que insertar todos los registros,
          // Le uno tambien el valor de una variable auxiliar iNumeroFila porque si se da el caso que todos los valores
          // son iguales a los de otra fila, la clave que generaria seria la misma.
          if (plantilla.getListaCamposAgrupacion().isEmpty()) {
            sCamposDetalle.append(iNumeroFila).append(sCamposDetalle);
          }

          final StringBuffer sCamposOrdenacion = new StringBuffer();
          String sFormato;

          // Formo la clave de ordenacion
          for (int j = 0; j < iColumnasOrdencacion.size(); j++) {

            sFormato = tmct0InformeFila.get(iColumnasOrdencacion.get(j)).getFormato();

            if (sFormato != null && !sFormato.equals("")) { // Es un campo numerico se le ponen ceros por la derecha y
                                                            // por la izquiera para que
              // ordene bien

              String sValor = tmct0InformeFila.get(iColumnasOrdencacion.get(j)).getValor();

              // Se busca el separador de decimales
              StringTokenizer st = new StringTokenizer(sValor, ",");

              String sDecimal = String.format("%1$015d", Integer.parseInt(st.nextToken()));
              String sFraccion = String.format("%1$015d", Integer.parseInt(st.nextToken()));

              sCamposOrdenacion.append(sDecimal).append(",").append(sFraccion);

            } else {
              sCamposOrdenacion.append(tmct0InformeFila.get(iColumnasOrdencacion.get(j)).getValor());
            }

          }

          for (int i = 0; i < tmct0InformeFila.size(); i++) {

            // Si el informe no esta agrupado, la clave la forman todos los campos,

            // Si el informe esta agrupado solo se cogen como clave los que no totalizan.
            if ((plantilla.getListaCamposAgrupacion().isEmpty()) || (!iColumnasTotalizan.contains(i))) {
              sCamposDetalle.append(tmct0InformeFila.get(i).getValor());
            }
          }

          sClave = sCamposOrdenacion.toString().concat(sCamposDetalle.toString());

          if (!tResultados.containsKey(sClave)) {
            tResultados.put(sClave, tmct0InformeFila);
          } else {
            LOG.debug("Elemento ya existente sumo sus totales ");
            // Elemento ya existente en el mapa, no se inserta uno nuevo, se suman los campos que totalizan
            auxSumaTotales = tResultados.get(sClave);
            double dValor = 0.0;
            for (int i = 0; i < iColumnasTotalizan.size(); i++) {
              int iColumna = iColumnasTotalizan.get(i);
              dValor = this.stringToDouble(String.valueOf(auxSumaTotales.get(iColumna).getValor()))
                       + this.stringToDouble(String.valueOf(tmct0InformeFila.get(iColumna).getValor()));

              auxSumaTotales.get(iColumna).setContenido(String.valueOf(dValor));
              tResultados.put(sClave, auxSumaTotales);
            }
          }
        }
      }

      // Ordenacion.

      LOG.debug("Se convierte el listado al formato que necesita la pantalla ");
      // Se recorre el map y se forma una lista única de datos finales que se envia a pantalla y se procesa en el excel
      Iterator<Map.Entry<String, List<Tmc0InformeCeldaDTO>>> it = tResultados.entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry<String, List<Tmc0InformeCeldaDTO>> me = it.next();
        lDatosFinales.add((List<Tmc0InformeCeldaDTO>) me.getValue());
      }

      LOG.debug("Fin formacion datos de cosulta se asigna al dto final ");
      dto.setResultados(lDatosFinales);
      result.put("informe", dto);
      // result.put("informe", lDatosFinales);

      // MFG 02/12/2014 Control generacion de la excel:
      // Si el numero de registros es mayor al limite maximo de registros en un excel permitido establecido en propertis
      // no se genera ningun.
      // Si se quieren crear los dos excel (Porque esta consultando y se quieren llevar ya a pantalla), solo se le
      // permite si el numero de registros es superior al limite establecido

      boolean bGenerarExcel = true; // Se inicializa para que genere la excel.

      if ((getSimple && getTerceros && lDatosFinales.size() > limiteRegistrosExcelConsulta)
          || (lDatosFinales.size() > limiteRegistrosExcel)) {
        bGenerarExcel = false;
      }

      if (bGenerarExcel && dto.getResultados() != null && !dto.getResultados().isEmpty()) {

        LOG.debug("Se va a crear una excel con: " + lDatosFinales.size() + " Filas");

        boolean reportConSeparacion = tmct0InformesBo.isConSeparacion(informe);
        if (reportConSeparacion) {
          LOG.debug("Es con separacion");

          if (getSimple) {
            LOG.debug("LLAMA A crearInformePorHojas SIMPLE");
            workbookSimple = crearInformePorHojas(filters, dto, informe, false);
            LOG.debug("FIN crearInformePorHojas SIMPLE");
          }

          if (getTerceros) {
            if (workbookSimple != null) { // Se coge el informe generado en el paso anterior y se le añade la cabecera.
              LOG.debug("Genera la excel de TERCEROS a partir de la excel normal");
              try {

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                workbookSimple.write(bos);
                InputStream miInput = new ByteArrayInputStream(bos.toByteArray());
                workbook = new XSSFWorkbook(miInput);

                // Se inserta la cabecera.
                XSSFCellStyle style = createWorkbookStyles(workbook);

                int nHojas = workbook.getNumberOfSheets();

                for (int i = 0; i < nHojas; i++) {
                  // Se desplaza 9 filas para insertar la cabecera
                  int iNumFilas = workbook.getSheetAt(i).getLastRowNum();
                  workbook.getSheetAt(i).shiftRows(0, iNumFilas, 9);
                  // Se inserta la cabecera.
                  createHeaderTerceros(workbook, workbook.getSheetAt(i), informe, filters, style, 0);
                }

              } catch (Exception e) {
                LOG.error("Se produjo un error en la generación del fichero Excel " + e.getMessage());
              }
            } else {
              LOG.debug("Genera la excel de TERCEROS NUEVA");
              workbook = crearInformePorHojas(filters, dto, informe, true);
            }
          }

        } else {
          LOG.debug("Es SIN separacion");
          if (getSimple) {
            LOG.debug("LLAMA A crearInformeSolaHoja SIMPLE");
            workbookSimple = crearInformeSolaHoja(filters, dto, informe, false);
            LOG.debug("FIN crearInformeSolaHoja SIMPLE");
          }

          if (getTerceros) {

            if (workbookSimple != null) { // Se coge el informe generado en el paso anterior y se le añade la cabecera.
              LOG.debug("Genera la excel de TERCEROS a partir de la excel normal");
              try {

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                workbookSimple.write(bos);
                InputStream miInput = new ByteArrayInputStream(bos.toByteArray());
                workbook = new XSSFWorkbook(miInput);

                // Se desplaza 9 filas para insertar la cabecera.
                int iNumFilas = workbook.getSheetAt(0).getLastRowNum();
                workbook.getSheetAt(0).shiftRows(0, iNumFilas, 9);

                // Se inserta la cabecera.
                XSSFCellStyle style = createWorkbookStyles(workbook);

                createHeaderTerceros(workbook, workbook.getSheetAt(0), informe, filters, style, 0);
              } catch (Exception e) {
                LOG.error("Se produjo un error en la generación del fichero Excel " + e.getMessage());
              }
            } else {
              LOG.debug("Genera la excel de TERCEROS NUEVA");
              workbook = crearInformeSolaHoja(filters, dto, informe, true);
            }
            LOG.debug("FIN crearInformeSolaHoja TERCEROS");
          }

        }

        LOG.debug("FIN CREACION INFORME");
        if (workbook != null) {
          LOG.debug("CREACION  ByteArrayOutputStream del workbook");
          ByteArrayOutputStream bos = new ByteArrayOutputStream();
          try {
            workbook.write(bos);
          } catch (IOException e) {
        	  LOG.error("Error ", e.getClass().getName(), e.getMessage());
          }

          // El report de terceros se genera siempre y se guarda en el objeto file
          result.put("file", bos.toByteArray());

          workbook.close();
        }

        if (workbookSimple != null) {
          LOG.debug("CREACION  ByteArrayOutputStream del workbookSimple");
          ByteArrayOutputStream bos = new ByteArrayOutputStream();
          try {
            workbookSimple.write(bos);
          } catch (IOException e) {
        	  LOG.error("Error ", e.getClass().getName(), e.getMessage());
          }

          // Objeto para enviar el excel sin cabecera
          result.put("fileSimpleXLS", bos.toByteArray());

          workbookSimple.close();
        }

        LOG.debug("FIN CREACION EXCEL ANTES EXCEPCIONES");

      }

    } catch (Exception e) {
    	// Se controla la excepción
    	LOG.error("Se produjo un error en la generación del fichero Excel " + e.getMessage());
    	dto.getErrores().add("Se produjo un error en la generación del fichero Excel");
    	result.put("informe", dto);
    }

    return result;
  }

  /**
   * Crea el reporte en excel en una única hoja
   * 
   * @param filters dto informe isTerceros
   * @return
   * @throws SIBBACBusinessException
   */
  private XSSFWorkbook crearInformeSolaHoja(Tmct0GeneracionInformeFilterDTO filters,
                                            Tmct0InformeGeneradoDTO dto,
                                            Tmct0Informes informe,
                                            Boolean isTerceros) {

    LOG.debug("INICIO crearInformeSolaHoja");
    Integer resultsStartRowNum = 0;
    Integer rowNum = 0;
    XSSFWorkbook workbook = new XSSFWorkbook();

    try {

      XSSFCellStyle style = createWorkbookStyles(workbook);

      XSSFSheet sheet = workbook.createSheet("Informe");

      if (isTerceros) {
        LOG.debug("LLAMA createHeaderTerceros DENTRO TERCEROS");
        rowNum = createHeaderTerceros(workbook, sheet, informe, filters, style, rowNum);
        LOG.debug("FIN createHeaderTerceros DENTRO TERCEROS");
      }

      LOG.debug("LLAMA createHeaderTerceros");
      rowNum = this.createColumnsHeaders(workbook, sheet, dto.getCabeceras(), rowNum);
      LOG.debug("FIN createHeaderTerceros");
      resultsStartRowNum = rowNum;

      LOG.debug("LLAMA createResults");
      rowNum = createResults(workbook, sheet, dto.getResultados(), style, rowNum);
      LOG.debug("FIN createResults");

      LOG.debug("LLAMA createTotals");
      rowNum = createTotals(workbook, sheet, dto.getCabeceras(), dto.getTotales(), style, rowNum);
      LOG.debug("FIN createTotals");

      LOG.debug("Autosize columans final");

      XSSFRow row = sheet.getRow(resultsStartRowNum + 1);
      for (int colNum = 0; colNum < row.getLastCellNum(); colNum++)
        try {
          sheet.autoSizeColumn(colNum);
        } catch (Exception e) {
          LOG.debug("No se pudo ajustar una columna");
        }

    } catch (Exception e) {
      LOG.debug("Excepción " + e.getMessage());
    }

    LOG.debug("FIN crearInformeSolaHoja");

    return workbook;

  }

  /**
   * Crea el reporte en excel en varias hojas
   * 
   * @param filters dto informe isTerceros
   * @return
   * @throws SIBBACBusinessException
   */
  private XSSFWorkbook crearInformePorHojas(Tmct0GeneracionInformeFilterDTO filters,
                                            Tmct0InformeGeneradoDTO dto,
                                            Tmct0Informes informe,
                                            Boolean isTerceros) {

    XSSFWorkbook workbook = new XSSFWorkbook();
    TreeMap<String, List<List<Tmc0InformeCeldaDTO>>> resultados = new TreeMap<String, List<List<Tmc0InformeCeldaDTO>>>();

    XSSFCellStyle style = createWorkbookStyles(workbook);

    List<String> colsSeparacion = tmct0InformesBo.getColumnasSeparacion(informe);

    // Genero las combinaciones de páginas según los separadores
    if (dto.getResultados() != null && !dto.getResultados().isEmpty()) {
      for (List<Tmc0InformeCeldaDTO> result : dto.getResultados()) {
        String key = "";
        for (Tmc0InformeCeldaDTO column : result) {
          if (colsSeparacion.contains(column.getColumna())) {
            if (key.isEmpty()) {
              key += column.getContenido();
            } else {
              key += " / " + column.getContenido();
            }
          }
        }
        if (resultados.get(key) == null) {
          resultados.put(key, new ArrayList<List<Tmc0InformeCeldaDTO>>());
        }
        resultados.get(key).add(result);
      }
    }

    int rowNum = 0;
    int resultsStartRowNum = 0;

    for (String page : resultados.keySet()) {

      rowNum = 0;

      String namePage = page.replace(" / ", "_").trim();

      // Se controla que el nombre de la página tenga una longitud entre 1 y 31
      if (namePage.length() < 1) {
        namePage = "<<NULL>>";
      } else if (namePage.length() > 31) {
        namePage = namePage.substring(0, 28) + "...";
      }

      XSSFSheet sheet = workbook.createSheet(namePage);

      List<String> colsTotalizar = tmct0InformesBo.getColumnasTotalizar(informe);

      // Totales por página
      HashMap<String, Double> totales = new HashMap<String, Double>();
      for (String columna : colsTotalizar) {
        totales.put(columna, new Double(0));
      }

      if (isTerceros) {
        rowNum = createHeaderTerceros(workbook, sheet, informe, filters, style, rowNum);
        resultsStartRowNum = rowNum;
      }

      XSSFCellStyle style2 = workbook.createCellStyle();
      XSSFFont font = workbook.createFont();
      font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
      style2.setFont(font);
      sheet.createRow(rowNum).createCell(0).setCellValue(page);
      sheet.getRow(rowNum).getCell(0).setCellStyle(style2);

      rowNum++;
      rowNum = this.createColumnsHeaders(workbook, sheet, dto.getCabeceras(), rowNum);

      for (List<Tmc0InformeCeldaDTO> result : resultados.get(page)) {

        int cellNum = 0;
        rowNum++;
        XSSFRow row = sheet.createRow(rowNum);

        for (Tmc0InformeCeldaDTO column : result) {

          XSSFCell cell = row.createCell(cellNum);

          Object value = column.getContenido();
          String tipo = column.getFormato();
          this.setCellValue(cell, value, tipo, workbook);

          Double total = totales.get(column.getColumna());
          if (total != null) {
            try {
              // Para parsear los decimales con ','
              total = total + this.stringToDouble(String.valueOf(column.getValor()));
            } catch (Exception e) {
              total = total + new Double(String.valueOf(column.getValor()));
            }

            totales.put(column.getColumna(), total);
          }

          cellNum++;

        }

      }

      this.createTotals(workbook, sheet, dto.getCabeceras(), totales, style, rowNum + 1);

    }

    for (XSSFSheet theSheet : workbook) {
      XSSFRow row = theSheet.getRow(resultsStartRowNum + 1);
      for (int colNum = 0; colNum < row.getLastCellNum(); colNum++)
        try {
          theSheet.autoSizeColumn(colNum);
        } catch (Exception e) {
          LOG.debug("No se pudo ajustar una columna");
        }

    }

    return workbook;
  }

  /**
   * Se añaden los totales
   * 
   * @param filters dto informe isTerceros
   * @return
   * @throws SIBBACBusinessException
   */
  private int createTotals(XSSFWorkbook workbook,
                           XSSFSheet sheet,
                           List<String> columnsNames,
                           HashMap<String, Double> totales,
                           XSSFCellStyle style,
                           Integer rowNum) {

    rowNum++;
    XSSFRow row = sheet.createRow(rowNum);
    DataFormat format = workbook.createDataFormat();
    if (totales != null) {
      int cellNum = 0;

      // se recorren las columnas para ver en qué posición se debe pintar el total
      for (String colName : columnsNames) {
        Double total = totales.get(colName);
        if (total != null) {
          row.createCell(cellNum).setCellValue(total);
          style = workbook.createCellStyle();
          XSSFFont font = workbook.createFont();
          font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
          style.setFont(font);
          style.setDataFormat(format.getFormat(FORMATO_MONTOS));
          row.getCell(cellNum).setCellStyle(style);
        }
        cellNum++;
      }

    }
    return rowNum;

  }

  /**
   * Se crea la cabecera para el reporte de terceros
   * 
   * @param filters dto informe isTerceros
   * @return
   * @throws SIBBACBusinessException
   */
  private int createHeaderTerceros(XSSFWorkbook workbook,
                                   XSSFSheet sheet,
                                   Tmct0Informes informe,
                                   Tmct0GeneracionInformeFilterDTO filters,
                                   XSSFCellStyle style,
                                   Integer rowNum) {

    try {

      final FileInputStream stream = new FileInputStream(TEMPLATES_FOLDER + "/LogotipoGlobalBanking.png");
      final CreationHelper helper = workbook.getCreationHelper();
      final Drawing drawing = sheet.createDrawingPatriarch();
      final ClientAnchor anchor = helper.createClientAnchor();
      anchor.setAnchorType(ClientAnchor.MOVE_AND_RESIZE);
      final int pictureIndex = workbook.addPicture(stream, Workbook.PICTURE_TYPE_PNG);
      anchor.setCol1(0);
      anchor.setRow1(0);
      final Picture pict = drawing.createPicture(anchor, pictureIndex);
      pict.resize(1.5, 3);

    } catch (FileNotFoundException e1) {
      LOG.error("No se pudo recuperar la imagen de cabecera");
    } catch (IOException e) {
      LOG.error("No se pudo recuperar la imagen de cabecera");
    } catch (Exception e) {
      LOG.error("No se pudo recuperar la imagen de cabecera");
    } finally {

      SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_ES);
      // Internacionalizar textos
      String cell1 = "De:";
      String cell2 = "Informe:";
      String cell3 = "Fecha:";
      if (informe.getNbidioma().equals("INGLES")) {
        cell1 = "From:";
        cell2 = "Report:";
        cell3 = "Date:";
      }

      rowNum = rowNum + 5;
      XSSFRow row = sheet.createRow(rowNum);
      row.createCell(0).setCellValue(cell1);
      row.createCell(1).setCellValue("BANCO SANTANDER S.A.");
      rowNum++;

      row = sheet.createRow(rowNum);
      row.createCell(0).setCellValue(cell2);
      row.createCell(1).setCellValue(informe.getNbdescription());
      XSSFCellStyle style2 = workbook.createCellStyle();
      XSSFFont font = workbook.createFont();
      font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
      style2.setFont(font);
      row.getCell(1).setCellStyle(style2);

      rowNum++;

      row = sheet.createRow(rowNum);
      row.createCell(0).setCellValue(cell3);

      String texto = "Desde " + formatter.format(filters.getFechaContratacionDesde());
      if (filters.getFechaContratacionHasta() != null) {
        texto += " hasta " + formatter.format(filters.getFechaContratacionHasta());
      }
      row.createCell(1).setCellValue(texto);

      // Se inserta una línea vacía
      rowNum++;
      rowNum++;
    }

    return rowNum;

  }

  private int createResults(XSSFWorkbook workbook,
                            XSSFSheet sheet,
                            List<List<Tmc0InformeCeldaDTO>> results,
                            XSSFCellStyle style,
                            Integer rowNum) {

    if (results != null && !results.isEmpty()) {

      for (List<Tmc0InformeCeldaDTO> result : results) {

        int cellNum = 0;
        rowNum++;
        LOG.trace("Crea la fila " + rowNum);
        XSSFRow row = sheet.createRow(rowNum);

        for (Tmc0InformeCeldaDTO column : result) {

          XSSFCell cell = row.createCell(cellNum);

          Object value = column.getContenido();
          String formato = column.getFormato();
          this.setCellValue(cell, value, formato, workbook);
          cellNum++;
        }
      }
    } else {
      rowNum++;
    }

    return rowNum;

  }

  private int createColumnsHeaders(XSSFWorkbook workbook, XSSFSheet sheet, List<String> columnsNames, Integer rowNum) {

    int cellNum = 0;
    XSSFRow headerRow = sheet.createRow(rowNum);
    for (String colName : columnsNames) {
      XSSFCell headerCol = headerRow.createCell(cellNum);
      // XSSFCellStyle style = workbook.createCellStyle();
      XSSFCellStyle style = this.createWorkbookStyles(workbook);
      XSSFFont font = style.getFont(); // Se utiliza la fuente definida por defecto
      font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
      style.setFont(font);
      headerCol.setCellStyle(style);
      headerCol.setCellValue(colName);
      cellNum++;
    }
    return rowNum;

  }

  // Crea el estilo por defecto de la celda, se asigna fuente y tamaño
  private XSSFCellStyle createWorkbookStyles(XSSFWorkbook workbook) {

    XSSFCellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setFontHeightInPoints((short) TAMANIO_FUENTE_DEFECTO); // Se pide un tamaño de 8 puntos por defecto para todos
                                                                // los datos

    font.setFontName(FUENTE_DEFECTO);
    style.setFont(font);
    return style;

  }

  private void setCellValue(XSSFCell cell, Object value, String formato, XSSFWorkbook workbook) {

    SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_ES);
    SimpleDateFormat formatterHora = new SimpleDateFormat(FORMATO_HORA_ES);
    DataFormat format = workbook.createDataFormat();

    // XSSFCellStyle style = workbook.createCellStyle();
    // MFG 12/09/2016 Se aplica el estilo por defecto - peticion de cambio a fuente calibri tamaño 8 .
    XSSFCellStyle style = this.createWorkbookStyles(workbook);
    cell.setCellStyle(style);
    if (value instanceof Time) {
      Date theDate = (Date) value;
      cell.setCellValue(formatterHora.format(theDate));
    } else if (value instanceof Date) {
      Date theDate = (Date) value;
      cell.setCellValue(formatter.format(theDate));
    } else if (value instanceof Boolean) {
      cell.setCellValue((Boolean) value);
    } else if (value instanceof String) {
      // si era un numero y se ha transformado
      if (formato.length() > 0) {

        style.setDataFormat(format.getFormat(formato));
        cell.setCellStyle(style);
        cell.setCellValue(this.stringToDouble(String.valueOf(value)));
      } else {
        cell.setCellValue((String) value);
      }
    } else if (value instanceof BigDecimal) {
      // XSSFCellStyle style = workbook.createCellStyle();
      style.setDataFormat(format.getFormat(FORMATO_MONTOS));
      cell.setCellStyle(style);
      cell.setCellValue(new Double(String.valueOf(value)));
    } else if (value instanceof Double) {
      // XSSFCellStyle style = workbook.createCellStyle();
      style.setDataFormat(format.getFormat(FORMATO_MONTOS));
      cell.setCellStyle(style);
      cell.setCellValue((Double) value);
    } else if (value != null) {
      cell.setCellValue(value.toString());
    }

  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosPlantillas getCommand(String command) {
    ComandosPlantillas[] commands = ComandosPlantillas.values();
    ComandosPlantillas result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosPlantillas.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  private double stringToDouble(String cantidad) {
    double resultado = 0.0;
    String[] arrcantidad = cantidad.split("\\,");
    if (arrcantidad.length > 0) {
      String cantidadEntera = arrcantidad[0];
      String cantidadDecimal = "0";
      // si tiene parte decimal
      if (arrcantidad.length > 1) {
        cantidadDecimal = arrcantidad[1];
      }
      if (cantidadEntera != null) {
        if (!cantidadEntera.equals("")) {
          resultado = Double.parseDouble(cantidadEntera);
        }
      }
      if (cantidadDecimal != null) {
        if (cantidadDecimal.equals("")) {
          cantidadDecimal = "0";
        }
        int contDecimales_ot = cantidadDecimal.length();
        resultado += Double.parseDouble(cantidadDecimal) / Math.pow(10, contDecimales_ot);
      }
    }
    return resultado;
  }

  private void logInforme(Tmct0GeneracionInformeFilterDTO filters, String informe) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    String mensajeLog = "Lanzado informe " + informe + " (" + filters.getIdInforme() + ") ";

    if (filters.getFechaContratacionDesde() != null) {
      mensajeLog += "con FH.CONTRATACION DESDE: " + formatter.format(filters.getFechaContratacionDesde());
      if (filters.getFechaContratacionHasta() != null) {
        mensajeLog += " HASTA: " + formatter.format(filters.getFechaContratacionHasta());
      }
      if (filters.getFechaLiquidacionDesde() != null) {
        mensajeLog += ", FH.LIQUIDACION DESDE: " + formatter.format(filters.getFechaLiquidacionDesde());
        if (filters.getFechaLiquidacionHasta() != null) {
          mensajeLog += " HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
        }
      } else {
        if (filters.getFechaLiquidacionHasta() != null) {
          mensajeLog += ", FH.LIQUIDACION HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
        }
      }
    } else {
      if (filters.getFechaContratacionHasta() != null) {
        mensajeLog += "con FH.CONTRATACION HASTA: " + formatter.format(filters.getFechaContratacionHasta());
        if (filters.getFechaLiquidacionDesde() != null) {
          mensajeLog += ", FH.LIQUIDACION DESDE: " + formatter.format(filters.getFechaLiquidacionDesde());
          if (filters.getFechaLiquidacionHasta() != null) {
            mensajeLog += " HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
          }
        } else {
          if (filters.getFechaLiquidacionHasta() != null) {
            mensajeLog += ", FH.LIQUIDACION HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
          }
        }
      } else {
        if (filters.getFechaLiquidacionDesde() != null) {
          mensajeLog += "con FH.LIQUIDACION DESDE: " + formatter.format(filters.getFechaLiquidacionDesde());
          if (filters.getFechaLiquidacionHasta() != null) {
            mensajeLog += " HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
          }
        } else {
          if (filters.getFechaLiquidacionHasta() != null) {
            mensajeLog += "con FH.LIQUIDACION HASTA: " + formatter.format(filters.getFechaLiquidacionHasta());
          }
        }
      }
    }
    if (filters.getAlias() != null) {
      if (!filters.getAlias().toString().equals("")) {
        mensajeLog += ", ALIAS: " + filters.getAlias().toString().replace("'", "");
      }
    }
    if (filters.getIsin() != null) {
      if (!filters.getIsin().toString().equals("")) {
        mensajeLog += ", ISIN: " + filters.getIsin().toString().replace("'", "");
      }
    }

    if (filters.getOpcionesEspeciales() != null) {
      if (filters.getOpcionesEspeciales().equals("ALL")) {
        mensajeLog += ", Opciones especiales: Incluir todos";
      } else if (filters.getOpcionesEspeciales().equals("EXCLROUTING")) {
        mensajeLog += ", Opciones especiales: Excluir routing (ALIAS NOT IN " + ROUTING + ")";
      } else if (filters.getOpcionesEspeciales().equals("EXCLBARRID")) {
        mensajeLog += ", Opciones especiales: Excluir barridos de derechos (ALIAS NOT IN " + BARRIDO + ")";
      }
    }
    // TODO: activar cuando este realizada la gestión de usuarios
    // + " por el usuario: uuuuuuuuu";
    LOG.info(mensajeLog);
  }

  /**
   * Gets las fechas de ejecucion del informe. .
   *
   * @param dFechaInicio - Fecha de inicio
   * @param dFechaFin - Fecha de Fin
   * @return Lista con todos los dias que hay entre las fechas dadas
   * @throws Exception
   */
  private List<Date> getFechasEjecucion(Date dFechaInicio, Date dFechaFin) throws Exception {
    LOG.debug("Entra en getFechasEjecucion");

    List<Date> lResultado = new ArrayList<Date>();

    Date diaSiguiente;
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    try {
      // Tratamiento de las fechas de inicio y final.
      // Se tiene que incluir la fecha de inicio que no puede ser nula.
      lResultado.add(dFechaInicio);

      // Si la fecha final es nula, se inicializa con la fecha actual sin horas minutos y segundos
      if (dFechaFin == null) {

        dFechaFin = formatoFecha.parse(formatoFecha.format(new Date()));
      }

      diaSiguiente = dFechaInicio;

      Calendar calendar = Calendar.getInstance();
      while (diaSiguiente.before(dFechaFin)) {
        calendar.setTime(diaSiguiente);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        diaSiguiente = calendar.getTime();
        lResultado.add(diaSiguiente);
      }

    } catch (Exception e) {
      LOG.error("getFechasEjecucion - Se produjo un error al obtener las fechas de ejecucion " + e.getMessage());
      throw e;
    }

    LOG.debug("Fin en getFechasEjecucion");

    return lResultado;

  }

}
