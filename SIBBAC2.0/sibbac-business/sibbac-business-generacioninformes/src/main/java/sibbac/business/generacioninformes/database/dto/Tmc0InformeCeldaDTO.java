package sibbac.business.generacioninformes.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import sibbac.business.generacioninformes.utils.Constantes;

/**
 * DTO para cargar resultados de celdas
 * 
 * @author neoris
 *
 */
public class Tmc0InformeCeldaDTO implements Serializable{

  /** Nombre de columna */
  private String columna;

  /** Valor del elemento */
  private Object contenido;

  /** Formato de la celda si es especial */
  private String formato;
  
  /** Totalizar **/
  private char totalizar;
  
  /** Precision campo */
  private Character precisionCampo;
  
  /** Tipo del campo */
  private String tipoCampo;
  
  private List<Integer> contenidoInteger = new ArrayList<Integer>();
  private List<String> contenidoString = new ArrayList<String>();
  private List<BigDecimal> contenidoBigDecimal = new ArrayList<BigDecimal>();
  
  public Tmc0InformeCeldaDTO (){
	  
  }
  
  public Tmc0InformeCeldaDTO(String columna, Object contenido, String tipo, char totalizar) {
	  this.columna = columna;
	  this.contenido = contenido;
	  this.formato = tipo;
	  this.totalizar = totalizar;
  }
  
  public Tmc0InformeCeldaDTO(String columna, Object contenido, String tipo, char totalizar, Character precisionCampo, String tipoCampo) {
    this.columna = columna;
    this.contenido = contenido;
    this.formato = tipo;
    this.totalizar = totalizar;
    this.precisionCampo = precisionCampo;
    this.tipoCampo=tipoCampo;
  }

  /**
 * @return the tipoCampo
 */
public String getTipoCampo() {
	return tipoCampo;
}

/**
 * @param tipoCampo the tipoCampo to set
 */
public void setTipoCampo(String tipoCampo) {
	this.tipoCampo = tipoCampo;
}

public String getValor() {
    return String.valueOf(contenido);
  }

  public String getColumna() {
    return columna;
  }

  public void setColumna(String columna) {
    this.columna = columna;
  }

  public Object getContenido() {
    return contenido;
  }

  public void setContenido(Object contenido) {
    this.contenido = contenido;
  }

  public String getFormato() {
    return formato;
  }

  public void setFormato(String formato) {
    this.formato = formato;
  }

  public char getTotalizar() {
	return totalizar;
  }

  public void setTotalizar(char totalizar) {
	this.totalizar = totalizar;
  }

  public Character getPrecisionCampo() {
	  return this.precisionCampo;
  }

  public void setPrecisionCampo(Character precisionCampo) {
	  this.precisionCampo = precisionCampo;
  }

  public List<Integer> getContenidoInteger() {
	  return contenidoInteger;
  }

  public void setContenidoInteger(List<Integer> contenidoInteger) {
	  this.contenidoInteger = contenidoInteger;
  }

  public List<String> getContenidoString() {
	  return this.contenidoString;
  }

  public void setContenidoString(List<String> contenidoString) {
	  this.contenidoString = contenidoString;
  }

  public List<BigDecimal> getContenidoBigDecimal() {
	  return this.contenidoBigDecimal;
  }

  public void setContenidoBigDecimal(List<BigDecimal> contenidoBigDecimal) {
	  this.contenidoBigDecimal = contenidoBigDecimal;
  }

  /**
   * Devuelve true si el campo TOTALIZAR es tipo COUNT.
   * @return boolean
   */
  public boolean isTotalizarCount() {
	  return this.totalizar == Constantes.VALOR_TOTALIZAR_COUNT;
  }

  /**
   * Devuelve true si el campo TOTALIZAR es tipo SUM.
   * @return boolean
   */
  public boolean isTotalizarSum() {
	  return this.totalizar == Constantes.VALOR_TOTALIZAR_SUM;
  }

  /**
   * Devuelve true si el campo TOTALIZAR es tipo MAX.
   * @return boolean
   */
  public boolean isTotalizarMax() {
	  return this.totalizar == Constantes.VALOR_TOTALIZAR_MAX;
  }

  /**
   * Devuelve true si el campo TOTALIZAR es tipo MIN.
   * @return boolean
   */
  public boolean isTotalizarMin() {
	  return this.totalizar == Constantes.VALOR_TOTALIZAR_MIN;
  }

  /**
   * Devuelve true si el campo TOTALIZAR es tipo COUNT DISTINCT.
   * @return boolean
   */
  public boolean isTotalizarCountDistinct() {
	  return this.totalizar == Constantes.VALOR_TOTALIZAR_COUNT_DISTINCT;
  }

  /**
   * Devuelve true si el campo TOTALIZAR es tipo SUM DISTINCT.
   * @return boolean
   */
  public boolean isTotalizarSumDistinct() {
	  return this.totalizar == Constantes.VALOR_TOTALIZAR_SUM_DISTINCT;
  }

  /**
   *	Devuelve true si el totalizador no contiene clausulas 
   *	SUM y SUM DISTINCT.
   *	@return boolean 
   */
  public boolean isContieneTotalizadoresNoSUM() {
	  return sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM != this.getTotalizar()
			  && sibbac.business.generacioninformes.utils.Constantes.VALOR_TOTALIZAR_SUM_DISTINCT != this.getTotalizar();
  }
}
