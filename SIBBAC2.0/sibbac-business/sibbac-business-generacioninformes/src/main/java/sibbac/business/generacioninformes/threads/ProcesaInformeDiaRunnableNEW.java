package sibbac.business.generacioninformes.threads;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTONEW;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;

public class ProcesaInformeDiaRunnableNEW implements Callable<Integer> {

  /** Datos con los que se tiene que generar el informe */
  private final Tmct0GeneracionInformeFilterDTO filters;

  /** Bo con los metodos que realizan las operaciones */
  private final Tmct0InformesBoNEW miTmct0InformesBo;

  private final Tmct0Informes informe;

  /** Identificador de hilo que se esta creando. */
  private final int iNumJob;
  
  private final Tmct0InformeDTONEW plantilla;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaInformeDiaRunnableNEW.class);
  

  public ProcesaInformeDiaRunnableNEW(Tmct0GeneracionInformeFilterDTO filters,
                                   Tmct0InformesBoNEW miTmct0InformesBo,
                                   Tmct0Informes informe,
                                   int iNumJob,
                                   Tmct0InformeDTONEW plantilla) {
    this.filters = filters;
    this.miTmct0InformesBo = miTmct0InformesBo;
    this.informe = informe;
    this.iNumJob = iNumJob;
    this.plantilla = plantilla;
  }

  @Override
  public Integer call() throws Exception{
	  
	  LOG.debug("{}", iNumJob);
	  
      // Metodo que ejecuta un informe
      
      return miTmct0InformesBo.ejecutaInforme(filters, informe, plantilla);

  }

}
