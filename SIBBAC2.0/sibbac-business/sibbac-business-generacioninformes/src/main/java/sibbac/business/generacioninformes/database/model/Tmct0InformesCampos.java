package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_INFORMES_CAMPOS
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_INFORMES_CAMPOS")
public class Tmct0InformesCampos {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDCAMPO", length = 4, nullable = false)
  private Integer id;

  @Column(name = "NBCAMPO", length = 100, nullable = false)
  private String nbcampo;
  
  @Column(name = "NBPANTALLA", length = 100, nullable = false)
  private String nbpantalla;
  
  @Column(name = "NBCOLUMNA", length = 100, nullable = false)
  private String nbcolumna;
  
  @Column(name = "NBCOLINGLES", length = 100, nullable = false)
  private String nbcolingles;
  
  @Column(name = "NBCONVERSION", length = 2000, nullable = false)
  private String nbconversion;
  
  @Column(name = "NBCONVINGLES", length = 2000, nullable = false)
  private String nbconvingles;
  
  @Column(name = "TOTALIZAR", length = 1, nullable = false)
  private char totalizar;

//  @ManyToOne(fetch = FetchType.LAZY)
//  @JoinColumn(name = "CDTABLA", referencedColumnName = "CDTABLA1")
//  private Tmct0InformesRelaciones tmct0InformesRelaciones;
  
  @Column(name = "CDTABLA", length = 1000, nullable = false)
  private String cdTabla;

  @Column(name = "IMP_PRECISION", length = 1, nullable = true)
  private Character precision;
  
  @Column(name = "CLASE", length = 50, nullable = false)
  private String clase;
	
  @Column(name = "MERCADO", length = 50, nullable = false)
  private String mercado;

  @Column(name = "TIPO_CAMPO", length = 50, nullable = true)
  private String tipoCampo;
  
  
  public Tmct0InformesCampos() {
  }


  public Tmct0InformesCampos(Integer id, String nbcampo, String nbpantalla, String nbcolumna, String nbcolingles,
		String nbconversion, String nbconvingles, char totalizar, String cdTabla,Tmct0InformesRelaciones tmct0InformesRelaciones,
		Character precision, String clase, String mercado, String tipoCampo) {
	super();
	this.id = id;
	this.nbcampo = nbcampo;
	this.nbpantalla = nbpantalla;
	this.nbcolumna = nbcolumna;
	this.nbcolingles = nbcolingles;
	this.nbconversion = nbconversion;
	this.nbconvingles = nbconvingles;
	this.totalizar = totalizar;
//	this.tmct0InformesRelaciones = tmct0InformesRelaciones;
	this.cdTabla=cdTabla;
	this.precision = precision;
	this.clase = clase;
	this.mercado = mercado;
	this.tipoCampo = tipoCampo;
  }


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /*
   * public String getNbtabla() { return nbtabla; } public void setNbtabla(String nbtabla) { this.nbtabla = nbtabla; }
   */
  public String getNbcampo() {
    return nbcampo;
  }

  public void setNbcampo(String nbcampo) {
    this.nbcampo = nbcampo;
  }

  public String getNbpantalla() {
    return nbpantalla;
  }

  public void setNbpantalla(String nbpantalla) {
    this.nbpantalla = nbpantalla;
  }

  public String getNbcolumna() {
    return nbcolumna;
  }

  public void setNbcolumna(String nbcolumna) {
    this.nbcolumna = nbcolumna;
  }

  public String getNbcolingles() {
    return nbcolingles;
  }

  public void setNbcolingles(String nbcolingles) {
    this.nbcolingles = nbcolingles;
  }

  public String getNbconversion() {
    return nbconversion;
  }

  public void setNbconversion(String nbconversion) {
    this.nbconversion = nbconversion;
  }

  public String getNbconvingles() {
    return nbconvingles;
  }

  public void setNbconvingles(String nbconvingles) {
    this.nbconvingles = nbconvingles;
  }

  public char getTotalizar() {
    return totalizar;
  }

  public void setTotalizar(char totalizar) {
    this.totalizar = totalizar;
  }

//  public Tmct0InformesRelaciones getTmct0InformesRelaciones() {
//    return tmct0InformesRelaciones;
//  }
//
//  public void setTmct0InformesRelaciones(Tmct0InformesRelaciones tmct0InformesRelaciones) {
//    this.tmct0InformesRelaciones = tmct0InformesRelaciones;
//  }

  public Character getPrecision() {
	  return precision;
  }

  /**
 * @return the cdTabla
 */
public String getCdTabla() {
	return cdTabla;
}


/**
 * @param cdTabla the cdTabla to set
 */
public void setCdTabla(String cdTabla) {
	this.cdTabla = cdTabla;
}


public void setPrecision(Character precision) {
	  this.precision = precision;
  }

  /**
   *	@return tipoCampo  
   */
  public String getTipoCampo() {
	  return this.tipoCampo;
  }

  /**
   *	@param tipoCampo  
   */
  public void setTipoCampo(String tipoCampo) {
	  this.tipoCampo = tipoCampo;
  }

  /**
   *	@return mercado  
   */
  public String getMercado() {
	  return this.mercado;
  }

  /**
   *	@param mercado  
   */
  public void setMercado(String mercado) {
	  this.mercado = mercado;
  }

  public String getClase() {
	  return clase;
  }

  public void setClase(String clase) {
	  this.clase = clase;
  }
  
  

}
