package sibbac.business.generacioninformes.helper;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;

public class StyleBean {
	
	private int colNum;
	private XSSFCellStyle style;
	
	
	public int getColNum() {
		return colNum;
	}
	public void setColNum(int colNum) {
		this.colNum = colNum;
	}
	public XSSFCellStyle getStyle() {
		return style;
	}
	public void setStyle(XSSFCellStyle style) {
		this.style = style;
	}
	
	

}
