package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_INFORMES_TIPOS
 * @author Neoris
 *
 */
@Entity
@Table( name = "TMCT0_INFORMES_TIPOS" )
public class Tmct0InformesTipos {

  /** Campos de la entidad */
  private static final long serialVersionUID  = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column( name = "IDTIPO", length = 4, nullable = false )
  private Integer idTipo;
  @Column( name = "NBTIPO", length = 20, nullable = false )
  private String       nbtipo;
 
  
  public Tmct0InformesTipos() {
    
  }
    
  public Tmct0InformesTipos(Integer id, String nbtipo) {
    this.idTipo = id;
    this.nbtipo = nbtipo;
  }


  public Integer getId() {
    return idTipo;
  }


  public void setId(Integer id) {
    this.idTipo = id;
  }


  public String getNbtipo() {
    return nbtipo;
  }


  public void setNbtipo(String nbtipo) {
    this.nbtipo = nbtipo;
  }

  
  
  
}
