/**
 * 
 */
package sibbac.business.generacioninformes.database.dto;

/**
 * @author lucio.vilar
 *
 */
public class FiltroDinamicoDTO {
	
	/**
	 * The Enum para la lista de tipos
	 */
	public enum EnuTipo {

		FECHA("FECHA"), AUTORELLENABLE("AUTORELLENBLE"), LISTA("LISTA");

		private String key;

		EnuTipo(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

	}

	/**
	 * The Enum para la lista de subtipos
	 */
	public enum EnuSubtipo {

		DESDE_HASTA("DESDE_HASTA");

		private String key;

		EnuSubtipo(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

	}
	
	private String tipo;
	
	private String subtipo;
	
	private String tabla;
	
	private String campo;
	
	private String valor;
	
	private String valorHasta;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSubtipo() {
		return subtipo;
	}

	public void setSubtipo(String subtipo) {
		this.subtipo = subtipo;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValorHasta() {
		return valorHasta;
	}

	public void setValorHasta(String valorHasta) {
		this.valorHasta = valorHasta;
	}

}
