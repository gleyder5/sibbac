/**
 * 
 */
package sibbac.business.generacioninformes.database.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.dto.Tmct0PlanificacionInformeFilterDTO;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.common.SIBBACBusinessException;

/**
 * @author lucio.vilar
 *
 */
@Repository
public class Tmct0InformesPlanificacionDaoImp {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0InformesPlanificacionDaoImp.class);

	@PersistenceContext
	private EntityManager em;	  

	@Autowired
	private Tmct0InformesPlanificacionDao tmct0InformesPlanificacionDao;
	  
	public List<Tmct0InformesPlanificacion> buscarInformesPlanificacionByFilter(
			Tmct0PlanificacionInformeFilterDTO filter)
			throws SIBBACBusinessException {
		List<Tmct0InformesPlanificacion> result = null;
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
		    // Se forma la consulta con los id informes que cumplen todos los requisitos
		    final StringBuilder select = new StringBuilder(" SELECT inf.ID ");
		    final StringBuilder from = new StringBuilder(" FROM TMCT0_INFORMES_PLANIFICACION inf ");
		    final StringBuilder where = new StringBuilder(" WHERE inf.ESTADO='A' ");

			if (null != filter && filter.getInforme() != null && !filter.getInforme().equals("")) {
				where.append(" AND inf.IDINFORME = :plantilla");
				parameters.put("plantilla", filter.getInforme());
			}
			if (null != filter && filter.getPeriodo() != null && !filter.getPeriodo().equals("")) {
				where.append(" AND inf.ID_PERIODICIDAD = :periodo");
				parameters.put("periodo", filter.getPeriodo());
			}
			if (null != filter && filter.getNombreFichero() != null && !filter.getNombreFichero().equals("")) {
				where.append(" AND inf.NOMBRE_FICHERO = :nombreFich");
				parameters.put("nombreFich", filter.getNombreFichero());
			}
			if (null != filter && filter.getTipoDestino() != null && !filter.getTipoDestino().equals("")) {
				where.append(" AND inf.TIPO_DESTINO = :tipoDest");
				parameters.put("tipoDest", filter.getTipoDestino());
			}
			if (null != filter && filter.getDestino() != null && !filter.getDestino().equals("")) {
				where.append(" AND inf.DESTINO = :dest");
				parameters.put("dest", filter.getDestino());
			}
			if (null != filter && filter.getFormato() != null && !filter.getFormato().equals("")) {
				where.append(" AND inf.FORMATO = :formato");
				parameters.put("formato", filter.getFormato());
			}

			String sQuery = select.append(from.append(where)).toString();
		    Query query = null;

		    LOG.debug("Creamos y ejecutamos la query: " + sQuery);

		    query = em.createNativeQuery(sQuery);

		    for (Entry<String, Object> entry : parameters.entrySet()) {
		      query.setParameter(entry.getKey(), entry.getValue());
		    }

		    // Se forman todas las entidades informe con los datos recuperados.
		    List<BigInteger> listaObjetosResultado = query.getResultList();

		    if (CollectionUtils.isNotEmpty(listaObjetosResultado)) {
		    	result = new ArrayList<Tmct0InformesPlanificacion>();
		    	for (BigInteger id : listaObjetosResultado) {
					if (id.intValue() > 0) {
						Tmct0InformesPlanificacion informe = tmct0InformesPlanificacionDao.findOne(id.intValue());
						result.add(informe);
					}
				}
		    	LOG.debug("Query ejecutada, obtenidos " + result.size() + " registros ");
		    }

		} catch (Exception e) {
			LOG.error("Error en buscarInformesPlanificacionByFilter - " + e.getCause());
			throw new SIBBACBusinessException(e);
		}

		LOG.debug("buscarInformesPlanificacionByFilter - Se sale del metodo");

		return result;
	}

}
