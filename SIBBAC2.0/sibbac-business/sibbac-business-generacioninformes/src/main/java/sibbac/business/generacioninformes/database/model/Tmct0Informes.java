package sibbac.business.generacioninformes.database.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PostLoad;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;

/**
 * Entidad para la gestion de TMCT0_INFORMES
 * 
 * @author Neoris
 *
 */
@Entity
@Proxy(lazy = false)
@Table(name = "TMCT0_INFORMES")
public class Tmct0Informes implements java.io.Serializable {

	  /** Campos de la entidad */
	  private static final long serialVersionUID = 1748837168974655383L;
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  @Column(name = "IDINFORME", length = 8, nullable = false)
	  private Integer id;
	  
	  @Column(name = "NBDESCRIPCION", length = 100, nullable = false)
	  private String nbdescription;
	  
	  @Column(name = "NBIDIOMA", length = 100, nullable = false)
	  private String nbidioma;
	  
	  @Column(name = "TOTALIZAR", length = 1, nullable = false)
	  private String totalizar;
	
	  @Column(name = "NBQUERY1", nullable = false)
	  private String nbquery1;
	
	  @Column(name = "NBQUERY2", nullable = false)
	  private String nbquery2;
	
	  @Column(name = "NBTXTLIBRE", nullable = true)
	  private String nbtxtlibre;
	
	  /**
	   * identificador del de la tabla TMCT0_INFORMES_CLASES
	   */
	  @ManyToOne()
	  @JoinColumn(name = "IDCLASE")
	  @LazyCollection(LazyCollectionOption.FALSE)
	  private Tmct0InformesClases tmct0InformesClases;
	  
	  @Column(name = "MOD_USU", length = 100, nullable = true)
	  private String modUsu;
	  
	  @Column(name = "MOD_FECHAHORA", length = 12, nullable = true)
	  private Date modFechaHora;
	  
	  @Column(name = "GEN_USU", length = 100, nullable = true)
	  private String genUsu;
	  
	  @Column(name = "GEN_INICIO", length = 12, nullable = true)
	  private Date genInicio;
	  
	  @Column(name = "GEN_FIN", length = 12, nullable = true)
	  private Date genFin;
	  
	  @Column(name = "MERCADO", length = 50, nullable = true)
	  private String mercado;
	  
	  @Column(name = "ESTADO", length = 1, nullable = true)
	  private Character estado;
	  
	  @Column(name = "RELLENO_CAMPOS", length = 1, nullable = true)
	  private Character rellenoCampos;
	
	  @OneToMany(targetEntity = Tmct0InformesDatos.class,
	             mappedBy = "tmct0InformesDatosId.informe",
	             cascade = CascadeType.ALL)
	  @OrderBy("tmct0InformesDatosId.tipoCampo.idTipo,nuposici")
	  private List<Tmct0InformesDatos> tmct0InformesDatosList;
	
	  @OneToMany(mappedBy = "tmct0Informes",cascade = CascadeType.ALL)  
	  private List<Tmct0InformesDatosFiltros> tmct0InformesDatosFiltros;
	  
	  @OneToMany(mappedBy = "tmct0Informes",cascade = CascadeType.ALL)  
	  private List<Tmct0InformesTablas> tmct0InformesTablas;
	
	  @Column(name = "IDPADRE", length = 8, nullable = true)
	  private Integer idPadre;
	  
	  @OneToMany()
	  @JoinColumn(name = "IDINFORME")
	  @LazyCollection(LazyCollectionOption.FALSE)
	  private List<Tmct0InformesPlanificacion> Tmct0InformesPlanificacion;
	  
	  public Tmct0Informes() {
	  }
	
	  public void postLoad () {
		  tmct0InformesDatosList.size();
		  tmct0InformesDatosFiltros.size();
		  tmct0InformesTablas.size();
	  }
	  
	  public Tmct0Informes(Integer id,
	                       String nbdescription,
	                       String nbidioma,
	                       String totalizar,
	                       String nbquery1,
	                       String nbquery2,
	                       String nbtxtlibre,
	                       List<Tmct0InformesDatos> tmct0InformesDatosList) {
	    this.id = id;
	    this.nbdescription = nbdescription;
	    this.nbidioma = nbidioma;
	    this.totalizar = totalizar;
	    this.nbquery1 = nbquery1;
	    this.nbquery2 = nbquery2;
	    this.nbtxtlibre = nbtxtlibre;
	    this.tmct0InformesDatosList = tmct0InformesDatosList;
	  }

	  public Integer getId() {
	    return id;
	  }
	
	  public void setId(Integer id) {
	    this.id = id;
	  }
	
	  public List<Tmct0InformesDatos> getTmct0InformesDatosList() {
	    return tmct0InformesDatosList;
	  }
	
	  public void setTmct0InformesDatosList(List<Tmct0InformesDatos> tmct0InformesDatosList) {
	
	    this.tmct0InformesDatosList = tmct0InformesDatosList;
	  }
	
	  public String getNbdescription() {
	    return nbdescription;
	  }
	
	  public void setNbdescription(String nbdescription) {
	    this.nbdescription = nbdescription;
	  }
	
	  public String getNbidioma() {
	    return nbidioma;
	  }
	
	  public void setNbidioma(String nbidioma) {
	    this.nbidioma = nbidioma;
	  }
	
	  public String getTotalizar() {
	    return totalizar;
	  }
	
	  public void setTotalizar(String totalizar) {
	    this.totalizar = totalizar;
	  }
	
	  public String getNbquery1() {
	    return nbquery1;
	  }
	
	  public void setNbquery1(String nbquery1) {
	    this.nbquery1 = nbquery1;
	  }
	
	  public String getNbquery2() {
	    return nbquery2;
	  }
	
	  public void setNbquery2(String nbquery2) {
	    this.nbquery2 = nbquery2;
	  }
	
	  public String getNbtxtlibre() {
	    return nbtxtlibre;
	  }
	
	  public void setNbtxtlibre(String nbtxtlibre) {
	    this.nbtxtlibre = nbtxtlibre;
	  }
	
	  /**
	   *	@return tmct0InformesClases
	   */
	  public Tmct0InformesClases getTmct0InformesClases() {
		  return this.tmct0InformesClases;
	  }
	
	  /**
	   *	@param tmct0InformesClases
	   */
	  public void setTmct0InformesClases(Tmct0InformesClases tmct0InformesClases) {
		  this.tmct0InformesClases = tmct0InformesClases;
	  }
	
	  /**
	   *	@return modUsu
	   */
	  public String getModUsu() {
		  return this.modUsu;
	  }
	
	  /**
	   *	@param modUsu
	   */
	  public void setModUsu(String modUsu) {
		  this.modUsu = modUsu;
	  }
	
	  /**
	   *	@return modFechaHora
	   */
	  public Date getModFechaHora() {
		  return this.modFechaHora;
	  }
	
	  /**
	   *	@param modFechaHora
	   */
	  public void setModFechaHora(Date modFechaHora) {
		  this.modFechaHora = modFechaHora;
	  }
	
	  /**
	   *	@return genUsu
	   */
	  public String getGenUsu() {
		  return this.genUsu;
	  }
	
	  /**
	   *	@param genUsu
	   */
	  public void setGenUsu(String genUsu) {
		  this.genUsu = genUsu;
	  }
	
	  /**
	   *	@return genInicio
	   */
	  public Date getGenInicio() {
		  return this.genInicio;
	  }
	
	  /**
	   *	@param genInicio
	   */
	  public void setGenInicio(Date genInicio) {
		  this.genInicio = genInicio;
	  }
	
	  /**
	   *	@return genFin
	   */
	  public Date getGenFin() {
		  return this.genFin;
	  }
	
	  /**
	   *	@param genFin
	   */
	  public void setGenFin(Date genFin) {
		  this.genFin = genFin;
	  }
	
	  /**
	   *	@return mercado
	   */
	  public String getMercado() {
		  return this.mercado;
	  }
	
	  /**
	   *	@param mercado
	   */
	  public void setMercado(String mercado) {
		  this.mercado = mercado;
	  }
	
	  /**
	   *	@return estado
	   */
	  public Character getEstado() {
		  return this.estado;
	  }
	
	  /**
	   *	@param estado
	   */
	  public void setEstado(Character estado) {
		  this.estado = estado;
	  }
	  

	public List<Tmct0InformesTablas> getTmct0InformesTablas() {
		return tmct0InformesTablas;
	}

	public void setTmct0InformesTablas(List<Tmct0InformesTablas> tmct0InformesTablas) {
		this.tmct0InformesTablas = tmct0InformesTablas;
	}

	public List<Tmct0InformesDatosFiltros> getTmct0InformesDatosFiltros() {
		return tmct0InformesDatosFiltros;
	}
	
	public void setTmct0InformesDatosFiltros(
			List<Tmct0InformesDatosFiltros> tmct0InformesDatosFiltros) {
		this.tmct0InformesDatosFiltros = tmct0InformesDatosFiltros;
	}
	
	
	/**
	* @return the rellenoCampos
	*/
	public Character getRellenoCampos() {
		return rellenoCampos;
	}
	
	/**
	* @param rellenoCampos the rellenoCampos to set
	*/
	public void setRellenoCampos(Character rellenoCampos) {
		this.rellenoCampos = rellenoCampos;
	}
	
	public Integer getIdPadre() {
		return idPadre;
	}
	
	public void setIdPadre(Integer idPadre) {
		this.idPadre = idPadre;
	}
	
	/**
	* @return the tmct0InformesPlanificacion
	*/
	public List<Tmct0InformesPlanificacion> getTmct0InformesPlanificacion() {
		return Tmct0InformesPlanificacion;
	}
	
	/**
	* @param tmct0InformesPlanificacion the tmct0InformesPlanificacion to set
	*/
	public void setTmct0InformesPlanificacion(
			List<Tmct0InformesPlanificacion> tmct0InformesPlanificacion) {
		Tmct0InformesPlanificacion = tmct0InformesPlanificacion;
	}

}
