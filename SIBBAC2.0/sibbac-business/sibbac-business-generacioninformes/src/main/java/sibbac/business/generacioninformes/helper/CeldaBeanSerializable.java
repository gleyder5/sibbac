package sibbac.business.generacioninformes.helper;

import java.io.Serializable;

public class CeldaBeanSerializable implements Serializable{
	
	  private static final long serialVersionUID = 1L;

	/** Nombre de columna */
	  private String columna;

	  /** Valor del elemento */
	  private String contenido;

	  /** Formato de la celda si es especial */
	  private String formato;
	  
	  /** Totalizar **/
	  private String totalizar;
	  
	  /** Precision campo */
	  private String precisionCampo;

	public String getColumna() {
		return columna;
	}

	public void setColumna(String columna) {
		this.columna = columna;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getTotalizar() {
		return totalizar;
	}

	public void setTotalizar(String totalizar) {
		this.totalizar = totalizar;
	}

	public String getPrecisionCampo() {
		return precisionCampo;
	}

	public void setPrecisionCampo(String precisionCampo) {
		this.precisionCampo = precisionCampo;
	}

	@Override
	public String toString() {
		return columna + "|" + contenido + "|" + formato
				+ "|" + totalizar + "|" + precisionCampo + "\n";
	}
	  
}
