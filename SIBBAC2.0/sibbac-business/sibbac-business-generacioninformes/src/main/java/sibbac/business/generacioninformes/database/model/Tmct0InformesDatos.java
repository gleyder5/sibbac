package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_INFORMES_DATOS
 * @author Neoris
 */
@Entity
@Table( name = "TMCT0_INFORMES_DATOS" )
public class Tmct0InformesDatos {

  
  @EmbeddedId
  private Tmct0InformesDatosId tmct0InformesDatosId;
  
  @Column( name = "NUPOSICI", length = 4, nullable = false )
  private Integer nuposici;
  
  @Column( name = "NOMBRE", length = 50, nullable = true )
  private String  nombre;
  
  @Column( name = "RELLENO_TIPO", length = 50, nullable = true )
  private String  rellenoTipo;
  
  @Column( name = "RELLENO", length = 1, nullable = true )
  private Character relleno;
  
  @Column( name = "LONGITUD_ENTERO", length = 4, nullable = true )
  private Integer longitudEntero;  
  
  @Column( name = "LONGITUD_DECIMAL", length = 4, nullable = true )
  private Integer longitudDecimal;
  
  @Column( name = "LONGITUD_ALFANUMERICO", length = 4, nullable = true )
  private Integer  longitudAlfanumerico;
  
  @Column( name = "TRADUCCION", length = 200, nullable = true )
  private String  traduccion;
  
  @Column(name = "TOTALIZAR", length = 1, nullable = false)
  private char totalizar;

public Tmct0InformesDatos() {}
  
  public Tmct0InformesDatos(Tmct0InformesDatosId id ,Integer nuposici) {
    if (this.tmct0InformesDatosId == null){
      this.tmct0InformesDatosId = new Tmct0InformesDatosId();
    }
    this.tmct0InformesDatosId = id;
    this.nuposici = nuposici;
  }
  
  public Tmct0InformesDatos(Tmct0InformesDatosId id ,Integer nuposici, String nombre, String rellenoTipo, Character relleno, Integer longitudEntero, Integer longitudDecimal, Integer longitudAlfanumerico, String traduccion, char totalizar) {
	    if (this.tmct0InformesDatosId == null){
	      this.tmct0InformesDatosId = new Tmct0InformesDatosId();
	    }
	    this.tmct0InformesDatosId = id;
	    this.nuposici = nuposici;
	    this.nombre=nombre;
	    this.rellenoTipo=rellenoTipo;
	    this.relleno=relleno;
	    this.longitudEntero=longitudEntero;
	    this.longitudDecimal=longitudDecimal;
	    this.longitudAlfanumerico=longitudAlfanumerico;
	    this.traduccion=traduccion;
	    this.totalizar=totalizar;
	  }

  public Integer getNuposici() {
    return nuposici;
  }

  public void setNuposici(Integer nuposici) {
    this.nuposici = nuposici;
  }

  public Tmct0InformesDatosId getId() {
    return tmct0InformesDatosId;
  }

  public void setId(Tmct0InformesDatosId id) {
    this.tmct0InformesDatosId = id;
  }


  /**
   *	@return nombre 
   */
  public String getNombre() {
	  return this.nombre;
  }

  /**
   *	@param nombre 
   */
  public void setNombre(String nombre) {
	  this.nombre = nombre;
  }

  /**
   *	@return rellenoTipo 
   */
  public String getRellenoTipo() {
	  return this.rellenoTipo;
  }

  /**
   *	@param rellenoTipo 
   */
  public void setRellenoTipo(String rellenoTipo) {
	  this.rellenoTipo = rellenoTipo;
  }

  /**
   *	@return relleno 
   */
  public Character getRelleno() {
	  return this.relleno;
  }

  /**
   *	@param relleno 
   */
  public void setRelleno(Character relleno) {
	  this.relleno = relleno;
  }

  /**
   *	@return longitudEntero 
   */
  public Integer getLongitudEntero() {
	  return this.longitudEntero;
  }

  /**
   *	@param longitudEntero 
   */
  public void setLongitudEntero(Integer longitudEntero) {
	  this.longitudEntero = longitudEntero;
  }

  /**
   *	@return longitudDecimal 
   */
  public Integer getLongitudDecimal() {
	  return this.longitudDecimal;
  }

  /**
   *	@param longitudDecimal 
   */
  public void setLongitudDecimal(Integer longitudDecimal) {
	  this.longitudDecimal = longitudDecimal;
  }

  /**
   *	@return LongitudAlfanumerico 
   */
  public Integer getLongitudAlfanumerico() {
	  return this.longitudAlfanumerico;
  }

  /**
   *	@param LongitudAlfanumerico 
   */
  public void setLongitudAlfanumerico(Integer longitudAlfanumerico) {
	  this.longitudAlfanumerico = longitudAlfanumerico;
  }

  /**
   *	@return traduccion 
   */
  public String getTraduccion() {
	  return this.traduccion;
  }

  /**
   *	@param traduccion 
   */
  public void setTraduccion(String traduccion) {
	  this.traduccion = traduccion;
  }
  
  /**
   * @return the totalizar
   */
  public char getTotalizar() {
	  return totalizar;
  }

  /**
   * @param totalizar the totalizar to set
   */
  public void setTotalizar(char totalizar) {
	  this.totalizar = totalizar;
  }

}