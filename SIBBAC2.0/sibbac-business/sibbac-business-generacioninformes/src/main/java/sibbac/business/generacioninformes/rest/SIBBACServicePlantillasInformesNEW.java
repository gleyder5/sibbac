package sibbac.business.generacioninformes.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposImp;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesClasesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDaoImp;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDatosFiltrosDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesPlanificacionDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesRelacionesDao;
import sibbac.business.generacioninformes.database.dto.ClaseInformeDTO;
import sibbac.business.generacioninformes.database.dto.InformeDTO;
import sibbac.business.generacioninformes.database.dto.SelectDetalleDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeDTONEW;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssemblerNEW;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesCampos;
import sibbac.business.generacioninformes.database.model.Tmct0InformesClases;
import sibbac.business.generacioninformes.database.model.Tmct0InformesPlanificacion;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dto.ValoresDTO;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServicePlantillasInformesNEW implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServicePlantillasInformesNEW.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0InformesCamposDao tmct0InformesCamposDao;

  @Autowired
  Tmct0InformesDao tmct0InformesDao;

  @Autowired
  Tmct0InformesBoNEW tmct0InformesBoNEW;

  @Autowired
  private Tmct0aliDao aliDao;

  @Autowired
  private Tmct0ValoresBo valoresBo; // Para obtener los isin

  @Autowired
  Tmct0InformeAssemblerNEW tmct0InformeAssembler;

  @Autowired
  Tmct0InformesDaoImp tmct0InformesDaoImp;

  @Autowired
  Tmct0InformesDatosDao tmct0InformesDatosDao;
  
  @Autowired
  Tmct0InformesDatosFiltrosDao tmct0InformesDatosFiltrosDao;
  
  @Autowired
  Tmct0InformesClasesDao tmct0InformesClasesDao;
  
  @Autowired
  Tmct0InformesRelacionesDao tmct0InformesRelacionesDao;
  
  @Autowired
  Tmct0InformesCamposImp tmct0InformesCamposImp;
  
  @Autowired
  Tmct0InformesPlanificacionDao tmct0InformesPlanificacionDao;

  /**
   * The Enum para la lista totalizar
   */
  public enum EnuTotalizar {

    SI("S"),
    NO("N");

    private String key;

    EnuTotalizar(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }

  /**
   * The Enum para la lista totalizar
   */
  public enum EnuRellenoCampos {

    SI("S"),
    NO("N");

    private String key;

    EnuRellenoCampos(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }

  /**
   * The Enum para la lista totalizar
   */
  public enum EnuIdioma {

    ESPAÑOL("ESPAÑOL"),
    INGLES("INGLES");

    private String key;

    EnuIdioma(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }

  /**
   * The Enum para la lista Mercados
   */
  public enum EnuMercado {

	TODOS("TODOS"),
    NACIONAL("NACIONAL"),
    INTERNACIONAL("INTERNACIONAL");

    private String key;

    EnuMercado(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }

  /**
   * The Enum para los tipos de relleno
   */
  public enum EnuTipoRelleno {

	VACIO("Vacío"),
    IZQUIERDA("Izquierda"),
    DERECHA("Derecha");

    private String key;

    EnuTipoRelleno(String key) {
      this.key = key;
    }

    public String getKey() {
      return key;
    }

  }
  
  /**
   * The Enum ComandosPlantillas.
   */
  private enum ComandosPlantillas {

    /** Inicializa los datos del filtro de pantalla */
    CARGA_SELECTS_ESTATICOS("CargaSelectsEstaticos"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_CAMPOS_DETALLES("CargaCamposDetalle"),
    
    /** Inicializa los datos del filtro de pantalla */
    CARGA_CAMPOS_DETALLES_FILTRO("CargaCamposDetalleFiltro"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_ALIAS("CargaAlias"),

    CARGA_ISIN("CargaIsin"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_CLASES("CargaClases"),
    
    /** Inicializa los datos del filtro de pantalla */
    CARGA_MERCADOS("CargaMercados"),

    /** Inicializa los datos del filtro de pantalla */
    CARGA_PLANTILLAS("CargaPlantillas"),
    
    /** Inicializa formulario modificar plantilla informe */
    CARGA_PLANTILLA_INFORMES("CargaPlantillaInforme"),

    /** Inicializa el grid con todas las plantillas actuales */
    CARGA_PLANTILLAS_INFORMES("CargaPlantillasInformes"),

    /** Crear un plantilla con los datos Recibidos */
    CREAR_PLANTILLA("CrearPlantilla"),

    /** Modifica los datos de una plantilla con los datos Recibidos */
    MODIFICAR_PLANTILLA("ModificarPlantilla"),

    /** Elimina las plantillas checkeados */
    ELIMINAR_PLANTILLAS("EliminarPlantillas"),
    
    /** Carga las plantillas a raiz de una clase */
    CARGA_PLANTILLAS_FILTRO("CargarPlantillasFiltro"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandosPlantillas(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * The Enum Campos plantilla .
   */
  private enum FieldsPlantilla {
    PLANTILLA("plantilla"),
    IDIOMA("idioma"),
    TOTALIZAR("totalizar"),
    CAMPOSDETALLE("camposdetalle"),
    ALIAS("alias"),
    ISIN("isin");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsPlantilla(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {

    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosPlantillas command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServicePlantillasInformesNEW");

      switch (command) {
        case CARGA_SELECTS_ESTATICOS:
          result.setResultados(cargarSelectsEstaticos());
          break;
        case CARGA_CAMPOS_DETALLES:
          result.setResultados(cargarCamposDetalle(paramsObjects));
          break;
        case CARGA_CAMPOS_DETALLES_FILTRO:
            result.setResultados(cargarCamposDetalleFiltro(paramsObjects));
            break;
        case CARGA_ALIAS:
          result.setResultados(cargarAlias());
          break;
        case CARGA_ISIN:
          result.setResultados(cargarIsin());
          break;
        case CARGA_CLASES:
          result.setResultados(cargarClases());
          break;
        case CARGA_MERCADOS:
        	result.setResultados(cargarMercados());
        	break;
        // Carga la lista de las plantillas del filtro
        case CARGA_PLANTILLAS:
          result.setResultados(cargarPlantillas());
          break;
        case CREAR_PLANTILLA:
          result.setResultados(crearPlantilla(paramsObjects));
          break;
        case MODIFICAR_PLANTILLA:
          result.setResultados(modificarPlantilla(paramsObjects));
          break;
        case CARGA_PLANTILLA_INFORMES:
            result.setResultados(getPlantillaInforme(paramsObjects));
            break;
        case CARGA_PLANTILLAS_INFORMES:
          result.setResultados(getPlantillasInformes(paramsObjects));
          break;
        case ELIMINAR_PLANTILLAS:
          result.setResultados(eliminarPlantillas(paramsObjects));
          break;
        case CARGA_PLANTILLAS_FILTRO:
            result.setResultados(cargarPlantillasMercadoFiltro(paramsObjects));
            break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServicePlantillasInformesNEW");
    return result;
  }

	private Map<String, Object> cargarMercados() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para combo mercados del filtro");

		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {

			List<SelectDTO> listaMercados = new ArrayList<SelectDTO>();
			SelectDTO selectDTO = null;
			List<String> clases = tmct0InformesClasesDao.getMercadosInformesClases();
			selectDTO = new SelectDTO("TODOS", "TODOS");
			listaMercados.add(selectDTO);
			if (null != clases) {
				for (String mercados : clases) {
					selectDTO = new SelectDTO(mercados, mercados);
					listaMercados.add(selectDTO);
				}
			}
			
			mSalida.put("listMercadosInforme", listaMercados);
		} catch (Exception e) {
			LOG.error("Error metodo init - Mercado Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}

		LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

		return mSalida;
	}

	private Map<String, Object> cargarClases() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para combo clase del filtro");

		Map<String, Object> mSalida = new HashMap<String, Object>();

		try {

			List<ClaseInformeDTO> listaClases = new ArrayList<ClaseInformeDTO>();
			ClaseInformeDTO claseInformeDTO = null;
			List<Tmct0InformesClases> clases = tmct0InformesClasesDao.getAllInformesClases();
			
			if (null != clases) {
				for (Tmct0InformesClases tmct0InformesClases : clases) {
					claseInformeDTO = new ClaseInformeDTO(tmct0InformesClases.getId().toString(), tmct0InformesClases.getNombre(), tmct0InformesClases.getMercado(), tmct0InformesClases.getClase());
					listaClases.add(claseInformeDTO);
				}
			}
			
			mSalida.put("listClases", listaClases);
		} catch (Exception e) {
			LOG.error("Error metodo init - Clases Informes " + e.getMessage(), e);
			throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
		}

		LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

		return mSalida;
	}

/**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarSelectsEstaticos() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para combos estaticos del filtro");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaIdiomas = new ArrayList<SelectDTO>();
      List<SelectDTO> listaMercados = new ArrayList<SelectDTO>();
      List<SelectDTO> listaRellenoCampos = new ArrayList<SelectDTO>();
      List<SelectDTO> listaTotalizar = new ArrayList<SelectDTO>();
      List<SelectDTO> listaTipoRelleno = new ArrayList<SelectDTO>();

      SelectDTO select;

      // Formamos los idiomas
      for (EnuIdioma idio : EnuIdioma.values()) {
        select = new SelectDTO(idio.getKey(), idio.name());
        listaIdiomas.add(select);
      }

      mSalida.put("listaIdiomas", listaIdiomas);

      // Formamos los mercados
      for (EnuMercado merc : EnuMercado.values()) {
        select = new SelectDTO(merc.getKey(), merc.name());
        listaMercados.add(select);
      }

      mSalida.put("listaMercados", listaMercados);

      // Formamos los relleno campos
      for (EnuRellenoCampos relleno : EnuRellenoCampos.values()) {
        select = new SelectDTO(relleno.getKey(), relleno.name());
        listaRellenoCampos.add(select);
      }

      mSalida.put("listaRelleno", listaRellenoCampos);

      // Formamos los totalizar
      for (EnuTotalizar totalizar : EnuTotalizar.values()) {
        select = new SelectDTO(totalizar.getKey(), totalizar.name());
        listaTotalizar.add(select);
      }
      
      mSalida.put("listaTotalizar", listaTotalizar);
      
      // Formamos los tipos de relleno
      for (EnuTipoRelleno tipoRelleno : EnuTipoRelleno.values()) {
        select = new SelectDTO(tipoRelleno.getKey(), tipoRelleno.name());
        listaTipoRelleno.add(select);
      }
      
      mSalida.put("listaTipoRelleno", listaTipoRelleno);
      
    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

    return mSalida;
  }
  
  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarCamposDetalleFiltro(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.trace("Iniciando datos cargar Campos Detalle  ");

    Map<String, Object> map = new HashMap<String, Object>();

    try {
      List<SelectDetalleDTO> listaCampos = new ArrayList<SelectDetalleDTO>();
      
      SelectDetalleDTO select;

      String clase = String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("clase"));
      String mercado = String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key"));
      
      List<Tmct0InformesCampos> listInformesCampos = new ArrayList<Tmct0InformesCampos>();
      
      if ("".equals(mercado) || mercado.equalsIgnoreCase(EnuMercado.TODOS.getKey())) {
    	  listInformesCampos = tmct0InformesCamposDao.findByClase(clase);
      } else {
    	  listInformesCampos = tmct0InformesCamposDao.findByClaseAndMercado(clase, mercado);
      }


      for (Tmct0InformesCampos tmct0InformesCampos : listInformesCampos) {
    	  
    	  String nombreOriginal = tmct0InformesCampos.getNbpantalla();
    	  String tipoCampo = tmct0InformesCampos.getTipoCampo();
    	  select = new SelectDetalleDTO(tmct0InformesCampos.getId().toString(), tmct0InformesCampos.getNbpantalla(),"","","","",0,0,0,tipoCampo,nombreOriginal,String.valueOf(tmct0InformesCampos.getTotalizar()));
    	  listaCampos.add(select);
      }

      map.put("listInformesCampos", listaCampos);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion datos cargar Campos Detalle");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarCamposDetalle(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.trace("Iniciando datos cargar Campos Detalle  ");

    Map<String, Object> map = new HashMap<String, Object>();

    try {
      List<SelectDetalleDTO> listaCampos = new ArrayList<SelectDetalleDTO>();
      
      SelectDetalleDTO select;
      String clase = "";
      String mercado = "";

      if (paramsObjects.get("clase") instanceof String) {
    	  clase = String.valueOf(paramsObjects.get("clase"));
      } else {
    	  clase = String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("clase"));  
      }
      
      if (paramsObjects.get("mercado") instanceof String) {
    	  mercado = String.valueOf(paramsObjects.get("mercado"));
      } else {
    	  mercado = String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key"));
      }
      
      
      List<Tmct0InformesCampos> listInformesCampos = new ArrayList<Tmct0InformesCampos>();
      
      if (!"".equals(mercado) && !mercado.equalsIgnoreCase(EnuMercado.TODOS.getKey())) {
    	  listInformesCampos = tmct0InformesCamposDao.findByClaseAndMercado(clase, mercado);
      } else {
    	  listInformesCampos = tmct0InformesCamposDao.findByClase(clase);
      }


      for (Tmct0InformesCampos tmct0InformesCampos : listInformesCampos) {
    	  
    	  String nombreOriginal = tmct0InformesCampos.getNbpantalla();
    	  String tipoCampo = tmct0InformesCampos.getTipoCampo();
    	  select = new SelectDetalleDTO(tmct0InformesCampos.getId().toString(), tmct0InformesCampos.getNbpantalla(),"","","","",0,0,0,tipoCampo,nombreOriginal,String.valueOf(tmct0InformesCampos.getTotalizar()));
    	  listaCampos.add(select);
      }

      map.put("listInformesCampos", listaCampos);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.debug("Fin inicializacion datos cargar Campos Detalle");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarAlias() throws SIBBACBusinessException {

    LOG.debug("Iniciando datos para el filtro de Plantillas Informes - cargarAlias");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Calendar cal = Calendar.getInstance();
      Integer fechaActivo = Integer.parseInt(sdf.format(cal.getTime()));
      List<Tmct0aliDTO> listAlias = aliDao.findAllAli(fechaActivo);

      for (Tmct0aliDTO tmct0aliDTO : listAlias) {
        select = new SelectDTO(tmct0aliDTO.getAlias(), tmct0aliDTO.getAlias() + " -  " + tmct0aliDTO.getDescripcion());
        listaAlias.add(select);
      }
      map.put("listaAlias", listaAlias);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException(
                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarAlias");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarAlias");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarIsin() throws SIBBACBusinessException {

    LOG.debug("Iniciando datos para el filtro de Plantillas Informes - cargarIsin");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaIsin = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      List<Tmct0Valores> listIsin = valoresBo.findAll();
      List<ValoresDTO> listIsinDTO = valoresBo.entitiesListToAnDTOList(listIsin);

      for (ValoresDTO valoresDTO : listIsinDTO) {
        select = new SelectDTO(valoresDTO.getCodigo(), valoresDTO.getCodigo() + " -  " + valoresDTO.getDescripcion());
        listaIsin.add(select);
      }

      map.put("listaIsin", listaIsin);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda - cargarIsin");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarIsin");

    return map;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPlantillas() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para el filtro de Plantillas Informes - cargarPlantillas ");

    Map<String, Object> map = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaPlantillas = new ArrayList<SelectDTO>();

      List<Tmct0Informes> listaPlantillasInformes = tmct0InformesDao.findInformesOrderByNbdescription();

      SelectDTO select = null;

      for (Tmct0Informes tmct0Informes : listaPlantillasInformes) {
        // select = new SelectDTO(tmct0Informes.getId().toString(), tmct0Informes.getNbdescription());
        select = new SelectDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription());
        listaPlantillas.add(select);
      }

      map.put("listaPlantillas", listaPlantillas);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException(
                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarPlantillas");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarPlantillas");

    return map;
  }

  /**
   * Crea una nueva plantilla con los datos recibidos.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> crearPlantilla(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Inicio crearPlantilla");

    Map<String, Object> map = new HashMap<String, Object>();

    try {
    	
      String mercado = String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("key"));
      Integer idClase = Integer.parseInt(((Map<String, String>) paramsObjects.get("clase")).get("id"));
      String clase = String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("clase"));

      Tmct0InformeDTONEW miDTO = tmct0InformeAssembler.getTmct0InformeDto(paramsObjects);
      miDTO.setIdInforme(0);

      List<Tmct0Informes> listInformes = tmct0InformesDao.findBynbdescriptionAndEstado(miDTO.getNbdescription());

      if (listInformes.size() > 0) {
        LOG.error("Se esta intentanto crear una plantilla con el mismo nombre -  " + miDTO.getNbdescription());
        throw new SIBBACBusinessException("Ya existe una plantilla con el nombre " + miDTO.getNbdescription());
      }
      
      if (miDTO.getNbdescription().contains("/")) {
    	  LOG.error("Se esta intentanto crear una plantilla cuyo nombre contiene el caracter / -  " + miDTO.getNbdescription());
          throw new SIBBACBusinessException("El nombre de la plantilla no puede contener el caracter /");
      }

      Integer idInforme = tmct0InformesBoNEW.grabarPlantillaInforme(miDTO, idClase, false, Constantes.ALTA);
      List<Integer> idsClase = new ArrayList<Integer>();
      boolean recuperarTodosIdsClase = false;
      
      if (null != mercado && mercado.equalsIgnoreCase(EnuMercado.TODOS.getKey())) {
    	  List<Integer> listaIdsCamposDetalle = new ArrayList<Integer>();
    	  for (int i=0;i<miDTO.getListaCamposDetalle().size();i++) {
    		  listaIdsCamposDetalle.add(Integer.parseInt(miDTO.getListaCamposDetalle().get(i).getKey()));
    	  }
    	  List<String> listaMercadosClase = tmct0InformesCamposDao.findMercadosDistintosPorClase(clase,listaIdsCamposDetalle);
    	  for (int i=0;i<listaMercadosClase.size();i++) {
    		  if (listaMercadosClase.get(i).equals("")) {
    			  recuperarTodosIdsClase = true;
    			  break;
    		  }
    	  }

    	  if (recuperarTodosIdsClase) {
    		  idsClase = tmct0InformesClasesDao.getClasesConTodosMercadosInforme(idClase);
    	  } else {
    		  idsClase = tmct0InformesClasesDao.getClasesConTodosMercadosInforme(idClase,listaMercadosClase);
    	  }


    	  if (null != idsClase) {
    		  Tmct0InformeDTONEW informeHijo = null;
    		  for (Integer id : idsClase) {
    			  if (!id.equals(idClase)) {
    				  informeHijo = miDTO;
    				  informeHijo.setIdClase(id);
    				  String nombre = informeHijo.getNbdescription() + " - " + idInforme;
    				  informeHijo.setNbdescription(nombre);
    				  informeHijo.setIdInformePadre(idInforme);
    				  tmct0InformesBoNEW.grabarPlantillaInforme(informeHijo, idClase , true, Constantes.ALTA);
    			  }
    		  }
    	  }

      }
      
      map.put("idInforme", idInforme);

    } catch (SIBBACBusinessException e) {
      throw e;
    }

    LOG.info("Plantilla creada correctamente");

    LOG.debug("Fin crearPlantilla");

    return map;
  }

  /**
   * Modifica la plantilla con los nuevos datos recibidos.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> modificarPlantilla(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio modificarPlantilla");

    Map<String, Object> map = new HashMap<String, Object>();
    
    map = tmct0InformesBoNEW.modificarPlantilla(paramsObject);

    LOG.info("Plantilla modificada correctamente");

    LOG.debug("Fin modificarPlantilla");

    return map;
  }

  /**
   * Elimina las plantillas seleccionadas.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> eliminarPlantillas(Map<String, Object> paramsObject) throws SIBBACBusinessException {
		LOG.debug("Inicio eliminarPlantillas");
		Map<String, Object> map = new HashMap<String, Object>();

		List<Integer> ListInformesBorrar = (List<Integer>) paramsObject.get("listaPlantillasBorrar");
		Boolean eliminarPlanificaciones = (Boolean) paramsObject.get("eliminarPlanificacion");
		List<Tmct0InformesPlanificacion> planificaciones = new ArrayList<Tmct0InformesPlanificacion>();
		String plantillasConPlanificacion = "";
		for (Integer idInforme : ListInformesBorrar) {
			try {
				List<Tmct0InformesPlanificacion> list = tmct0InformesPlanificacionDao.findPlanificacionInformesByInforme(idInforme);
				if(null != list && !list.isEmpty()) {
					planificaciones.addAll(list);
				}

			} catch (Exception e) {
				LOG.error("Error al borrar el informe " + idInforme);
				LOG.error(e.getMessage());
				throw new SIBBACBusinessException(
						"No se ha podido borrar el informe seleccionado - Consulte con informatica ");
			}
		}

		if (planificaciones.isEmpty() || eliminarPlanificaciones) {
			if (null != planificaciones) {
				for (Tmct0InformesPlanificacion tmct0InformesPlanificacion : planificaciones) {
					//tmct0InformesPlanificacionDao.delete(tmct0InformesPlanificacion);
					tmct0InformesBoNEW.eliminarPlanificacionesInforme(tmct0InformesPlanificacion.getId());
				}
			}
			for (Integer idInforme : ListInformesBorrar) {
				try {
					LOG.info("Borramos el informe con id " + idInforme.toString());
					List<Tmct0Informes> listaInformesPadreHijos = tmct0InformesDao.findByIdInformeHijosyPadres(idInforme);
					for(Tmct0Informes informe : listaInformesPadreHijos){
						//tmct0InformesDao.delete(informe.getId());
						tmct0InformesBoNEW.eliminarInformes(informe.getId());
					}
					
				} catch (Exception e) {
					LOG.error("Error al borrar el informe " + idInforme);
					LOG.error(e.getMessage());
					throw new SIBBACBusinessException(
							"No se ha podido borrar el informe seleccionado - Consulte con informatica ");
				}
			}
		} else {
			for (Tmct0InformesPlanificacion planific : planificaciones) {
				if(!plantillasConPlanificacion.equals("")) {
					plantillasConPlanificacion += ", ";
				}
				plantillasConPlanificacion +=  planific.getTmct0Informes().getNbdescription();
			}
			if (plantillasConPlanificacion.equals("")) {
				plantillasConPlanificacion = null;
			}
			map.put("tmct0Informes", plantillasConPlanificacion);
		}

		LOG.info("Plantillas eliminadas correctamente");

		LOG.debug("Fin eliminarPlantillas");

		return map;
  }

  /**
   * Obtencion de todas las plantillas actuales para la carga del grid inicial
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getPlantillaInforme(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Entra en  getPlantillasInformesFiltro ");

    Map<String, Object> result = new HashMap<String, Object>();
    
    Tmct0InformeDTONEW miDTO = null;

    try {

    	Tmct0Informes tmct0Informes = tmct0InformesDao.findOne(Integer.parseInt(paramsObjects.get("idInforme").toString()));

        miDTO = tmct0InformeAssembler.getTmct0InformeDto(tmct0Informes);
        
    	//Recuperamos la posicion que ocupa para esa clase
  	  //Obtenemos el listado de mercados asociados a la clase
  	  List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(tmct0Informes.getTmct0InformesClases().getClase());
  	  //Recorremos la lista de mercados de la clase para saber que posicion ocupa el especifico de esta clase
  	  int posicionMercado = 0;
  	  for (int i=0;i<mercadosClase.size();i++) {
  		  if (mercadosClase.get(i).equals(tmct0Informes.getTmct0InformesClases().getMercado())) {
  			  posicionMercado = i;
  			  break;
  		  }
  	  }
  	miDTO.setPosicion(posicionMercado);
  	miDTO.setFechaBase(tmct0Informes.getTmct0InformesClases().getFechaBase());

    } catch (Exception e) {
      LOG.error("Error metodo getPlantillasInformes -  " + e.getMessage(), e); 
      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
    }

    result.put("plantillaInforme", miDTO);

    LOG.debug("Fin getFinalHoldersFiltro");

    return result;

  }
  
  /**
   * Obtencion de todas las plantillas actuales para la carga del grid inicial
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getPlantillasInformes(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Entra en  getPlantillasInformesFiltro ");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaPlantillasInformesFiltroDto = new ArrayList<Object>();

    try {

      // Se recuperan todos las plantillas
      Tmct0InformeFilterDTO filters = tmct0InformeAssembler.getTmct0InformeFilterDto(paramsObjects);

      // Se recuperan los informes que cumplen con los criterios estableceidos
      List<Tmct0Informes> listaPlantillasInformesFiltro = tmct0InformesDaoImp.findPlantillasInformesFiltro(filters);
    	
      // Se transforman a lista de DTO
      for (Tmct0Informes tmct0Informes : listaPlantillasInformesFiltro) {
        try {
          Tmct0InformeDTONEW miDTO = tmct0InformeAssembler.getTmct0InformeGridDto(tmct0Informes);
          listaPlantillasInformesFiltroDto.add(miDTO);
        } catch (Exception e) {
          LOG.error("Error metodo getPlantillasInformes -  miAssembler.getTmct0InformeDto " + e.getMessage(), e);
          // TODO: handle exception
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo getPlantillasInformes -  " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
    }

    result.put("listaPlantillasInformes", listaPlantillasInformesFiltroDto);

    LOG.debug("Fin getFinalHoldersFiltro");

    return result;

  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    FieldsPlantilla[] fields = FieldsPlantilla.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosPlantillas getCommand(String command) {
    ComandosPlantillas[] commands = ComandosPlantillas.values();
    ComandosPlantillas result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosPlantillas.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }
  
  /**
   * Carga las plantillas a raiz de un idClase
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPlantillasMercadoFiltro(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para el filtro de Plantillas Informes - cargarPlantillasMercadoFiltro ");

    Map<String, Object> map = new HashMap<String, Object>();

    try {
    	Integer idClase = Integer.parseInt(paramsObjects.get("id").toString());
    	String clase = paramsObjects.get("clase").toString();
    	InformeDTO selectPlantillas = null;
    	SelectDTO selectMercados = null;

    	//Obtenemos las plantillas asociadas a la clase seleccionada
    	List<InformeDTO> listaPlantillasFiltro = new ArrayList<InformeDTO>();
    	List<Tmct0Informes> listaPlantillasInformesFiltro = tmct0InformesDao.findInformesByClaseOrderByNbdescription(idClase);


    	for (Tmct0Informes tmct0Informes : listaPlantillasInformesFiltro) {
	    	//Recuperamos la posicion que ocupa para esa clase
	    	  //Obtenemos el listado de mercados asociados a la clase
	    	  List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(tmct0Informes.getTmct0InformesClases().getClase());
	    	  //Recorremos la lista de mercados de la clase para saber que posicion ocupa el especifico de esta clase
	    	  int posicionMercado = 0;
	    	  for (int i=0;i<mercadosClase.size();i++) {
	    		  if (mercadosClase.get(i).equals(tmct0Informes.getTmct0InformesClases().getMercado())) {
	    			  posicionMercado = i;
	    			  break;
	    		  }
	    	  }
	    	selectPlantillas = new InformeDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription(),posicionMercado,tmct0Informes.getTmct0InformesClases().getFechaBase(),tmct0Informes.getTmct0InformesClases().getClase());
	    	listaPlantillasFiltro.add(selectPlantillas);
    	}

    	map.put("listaPlantillasFiltro", listaPlantillasFiltro);
    	
    	//Obtenemos los mercados asociados a la clase seleccionada
    	List<SelectDTO> listaMercados = new ArrayList<SelectDTO>();
    	List<String> listaMercadosFiltro = tmct0InformesClasesDao.findMercadoByClase(clase);
    	
        for (String mercado : listaMercadosFiltro) {
        	selectMercados = new SelectDTO(mercado, mercado);
            listaMercados.add(selectMercados);
          }
        //Añadimos a la última posición el mercado TODOS
        selectMercados = new SelectDTO("TODOS", "TODOS");
        listaMercados.add(selectMercados);
        
        map.put("listaMercados", listaMercados);

    } catch (Exception e) {
      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
      throw new SIBBACBusinessException(
                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarPlantillasFiltro");
    }

    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarPlantillasMercadoFiltro");

    return map;
  }

}
