package sibbac.business.generacioninformes.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesCamposDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesClasesDao;
import sibbac.business.generacioninformes.database.dao.Tmct0InformesDao;
import sibbac.business.generacioninformes.database.dto.ClaseInformeDTO;
import sibbac.business.generacioninformes.database.dto.InformeDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0GeneracionInformeFilterDTO;
import sibbac.business.generacioninformes.database.dto.Tmct0InformeGeneradoDTO;
import sibbac.business.generacioninformes.database.dto.assembler.Tmct0InformeAssembler;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.database.model.Tmct0InformesClases;
import sibbac.business.generacioninformes.threads.ProcesarExcelnformes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dao.Tmct0GeneraInformeDaoImpl;
import sibbac.business.wrappers.database.dto.ValoresDTO;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 *	Corresponde a la implementacion de la V2 de la funcionalidad de generacion de informes. 
 */
@SIBBACService
public class SIBBACServiceGenerarInformesNEW implements SIBBACServiceBean {

	  /** The Constant LOG. */
	  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGenerarInformesNEW.class);

	  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	  public static final String RESULT_LISTA = "lista";

	  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	  @Value("${generacioninformes.clases.routing}")
	  private String CLASESROUTING;

	  @Value("${generacioninformes.limite.registros.excel:50000}")
	  private int limiteRegistrosExcel;
	  
	  @Value("${generacioninformes.limite.registros.pantalla}")
	  private int limiteRegistrosPantalla;
	  
	  @Value("${generacioninformes.limite.dias}")
	  private int limiteDias;
	  
	  @Value("${generacioninformes.timeout}")
	  private String timeout;
	  

	  @Value("${generacioninformes.alias.barrido}")
	  private String BARRIDO;

	  @Autowired
	  Tmct0InformesCamposDao tmct0InformesCamposDao;

	  @Autowired
	  Tmct0InformesDao tmct0InformesDao;

	  @Autowired
	  private Tmct0aliDao aliDao; // Para obtener los alias

	  @Autowired
	  private Tmct0ValoresBo valoresBo; // Para obtener los isin

	  @Autowired
	  Tmct0InformesBoNEW tmct0InformesBoNEW;

	  @Autowired
	  Tmct0GeneraInformeDaoImpl tmct0GeneraInformeDaoImpl;

	  @Autowired
	  Tmct0InformeAssembler tmct0InformeAssembler;
	  
	  @Autowired
	  Tmct0InformesClasesDao tmct0InformesClasesDao;

	  /**
	   * The Enum para los tipos de campos del informe
	   */
	  public enum EnuTipoCampo {

	    DETALLE(0),
	    ORDENACION(1),
	    AGRUPACION(2),
	    SEPARADOR(3);

	    private final int value;

	    EnuTipoCampo(int value) {
	      this.value = value;
	    }

	    public int getValue() {
	      return value;
	    }

	  }

	  /**
	   * The Enum ComandosPlantillas.
	   */
	  private enum ComandosPlantillas {

	    CARGA_INFORME_XLS("ExportaInformeXLS"),
	    CARGA_INFORME_TERCEROS_XLS("ExportaInformeTercerosXLS"),
	    CARGA_GENERACION_INFORMES("CargaGeneracionInformes"),
	    /** Inicializa los datos del filtro de pantalla */
	    CARGA_ALIAS("CargaAlias"),
	    CARGA_ISIN("CargaIsin"),
	    CARGA_PLANTILLAS("CargaPlantillas"),
	    OBTENER_QUERY_INFORME("ObtenerQueryInforme"),
	    VALIDAR_GENERACION_EXCEL("ValidarGeneracionExcel"),
	    CARGA_CLASES("CargaClases"),
	    CARGA_PLANTILLAS_FILTRO("CargarPlantillasFiltro"),
	    SIN_COMANDO("");

	    /** The command. */
	    private final String command;

	    /**
	     * Instantiates a new comandos Plantillas.
	     *
	     * @param command the command
	     */
	    private ComandosPlantillas(String command) {
	      this.command = command;
	    }

	    /**
	     * Gets the command.
	     *
	     * @return the command
	     */
	    private String getCommand() {
	      return command;
	    }

	  }

	  /**
	   * The Enum Campos plantilla .
	   */
	  private enum FieldsPlantilla {
	    PLANTILLA("plantilla");

	    /** The field. */
	    private String field;

	    /**
	     * Instantiates a new fields.
	     *
	     * @param field the field
	     */
	    private FieldsPlantilla(String field) {
	      this.field = field;
	    }

	    /**
	     * Gets the field.
	     *
	     * @return the field
	     */
	    private String getField() {
	      return field;
	    }
	  }

	  /**
	   * Process.
	   *
	   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
	   * @param webRequest the web request
	   * @return the web response
	   * @throws SIBBACBusinessException the SIBBAC business exception
	   */
	  @Override
	  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
	    Map<String, Object> paramsObjects = webRequest.getParamsObject();
	    WebResponse result = new WebResponse();
	    Map<String, Object> resultados = new HashMap<String, Object>();
	    Map<String, Object> resultadosError = new HashMap<String, Object>();
	    ComandosPlantillas command = getCommand(webRequest.getAction());

	    try {
	      LOG.debug("Entro al servicio SIBBACServiceGenerarInformesNEW");

	      switch (command) {
	        case CARGA_GENERACION_INFORMES:
	          result.setResultados(getInformeGenerado(paramsObjects));
	          break;
	        case CARGA_ALIAS:
	          result.setResultados(cargarAlias());
	          break;
	        case CARGA_ISIN:
	          result.setResultados(cargarIsin());
	          break;
	        case CARGA_PLANTILLAS:
	          result.setResultados(cargarPlantillas());
	          break;
	        case CARGA_INFORME_XLS:
		          result.setResultados(getInformeGeneradoExcel(paramsObjects));
	          break;
	        case CARGA_INFORME_TERCEROS_XLS:
		          result.setResultados(getInformeGeneradoExcelTerceros(paramsObjects));
	          break;
	        case OBTENER_QUERY_INFORME:
	          result.setResultados(getQueryInforme(paramsObjects));
	          break;
	        case VALIDAR_GENERACION_EXCEL:
	        	resultadosError = this.validarGeneracionExcel(paramsObjects);
	        	if (resultadosError != null && !resultadosError.isEmpty()) {
	        		result.setResultadosError(resultadosError);
	        	}
	        	break;
	        case CARGA_CLASES:
	            result.setResultados(cargarClases());
	            break;
	        case CARGA_PLANTILLAS_FILTRO:
	            result.setResultados(cargarPlantillasMercadoFiltro(paramsObjects));
	            break;
	        default:
	          result.setError("An action must be set. Valid actions are: " + command);
	      } // switch
	    } catch (SIBBACBusinessException e) {
	      resultados.put("status", "KO");
	      result.setResultados(resultados);
	      result.setError(e.getMessage());
	    } catch (Exception e) {
	      resultados.put("status", "KO");
	      result.setResultados(resultados);
	      result.setError(MESSAGE_ERROR);
	    }

	    LOG.debug("Salgo del servicio SIBBACServiceGenerarInformesNEW");

	    return result;
	  }

	  /**
	   * Gets the fields.
	   *
	   * @return the fields
	   */
	  @Override
	  public List<String> getFields() {
	    List<String> result = new ArrayList<>();
	    FieldsPlantilla[] fields = FieldsPlantilla.values();
	    for (int i = 0; i < fields.length; i++) {
	      result.add(fields[i].getField());
	    }
	    return result;
	  }

	  @Override
	  public List<String> getAvailableCommands() {
	    return Collections.emptyList();
	  }

	  /**
	   * Obtencion de las plantillas actuales
	   * 
	   * @return
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> cargarPlantillas() throws SIBBACBusinessException {

	    LOG.trace("Iniciando datos para la lista cargarPlantillas");

	    Map<String, Object> mSalida = new HashMap<String, Object>();

	    try {

	      // Inicializo los limites de registros en el tratamiendo de las excel.
	      mSalida.put("limite_total", limiteRegistrosExcel);

	      // Inicializo los limites de registros a mostrar en pantalla.
	      mSalida.put("limite_total_pantalla", limiteRegistrosPantalla);
	      
	      // Inicializo los limites de dias a generar informe
	      mSalida.put("limite_dias", limiteDias);
	      
	      List<InformeDTO> listaInformes = new ArrayList<InformeDTO>();
	      InformeDTO select = null;

	      List<Tmct0Informes> listaPlantillasInformes = tmct0InformesDao.findInformesOrderByNbdescription();
	      for (Tmct0Informes tmct0Informes : listaPlantillasInformes) {
	    	//Recuperamos la posicion que ocupa para esa clase
	    	  //Obtenemos el listado de mercados asociados a la clase
	    	  List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(tmct0Informes.getTmct0InformesClases().getClase());
	    	  //Recorremos la lista de mercados de la clase para saber que posicion ocupa el especifico de esta clase
	    	  int posicionMercado = 0;
	    	  for (int i=0;i<mercadosClase.size();i++) {
	    		  if (mercadosClase.get(i).equals(tmct0Informes.getTmct0InformesClases().getMercado())) {
	    			  posicionMercado = i;
	    			  break;
	    		  }
	    	  }
	        select = new InformeDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription(),posicionMercado,tmct0Informes.getTmct0InformesClases().getFechaBase(),tmct0Informes.getTmct0InformesClases().getClase());
	        listaInformes.add(select);
	      }
	      mSalida.put("listaInformes", listaInformes);
	      
	      String[] clasesRouting = CLASESROUTING.split(",",-1);
	      mSalida.put("clasesRouting", clasesRouting);

	    } catch (Exception e) {
	      LOG.error("Error metodo init - Generador Plantillas Informes " + e.getMessage(), e);
	      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
	    }

	    LOG.trace("Fin cargar lista cargarPlantillas");

	    return mSalida;
	  }

	  /**
	   * Carga inicial de los datos estaticos de la pantalla
	   * 
	   * @return
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> cargarAlias() throws SIBBACBusinessException {

	    LOG.debug("Iniciando datos para el filtro de Generacion Informes - cargarAlias");

	    Map<String, Object> map = new HashMap<String, Object>();

	    try {

	      List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
	      SelectDTO select = null;
	      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	      Calendar cal = Calendar.getInstance();
	      Integer fechaActivo = Integer.parseInt(sdf.format(cal.getTime()));
	      List<Tmct0aliDTO> listAlias = aliDao.findAllAli(fechaActivo);

	      for (Tmct0aliDTO tmct0aliDTO : listAlias) {
	        select = new SelectDTO(tmct0aliDTO.getAlias(), tmct0aliDTO.getAlias() + " -  " + tmct0aliDTO.getDescripcion());
	        listaAlias.add(select);
	      }
	      map.put("listaAlias", listaAlias);

	    } catch (Exception e) {
	      LOG.error("Error metodo init - Generacion Informes " + e.getMessage(), e);
	      throw new SIBBACBusinessException(
	                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarAlias");
	    }

	    LOG.debug("Fin inicializacion datos filtro de Generacion Informes - cargarAlias");

	    return map;
	  }

	  /**
	   * Carga inicial de los datos estaticos de la pantalla
	   * 
	   * @return
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> cargarIsin() throws SIBBACBusinessException {

	    LOG.debug("Iniciando datos para el filtro de Generacion Informes - cargarIsin");

	    Map<String, Object> map = new HashMap<String, Object>();

	    try {

	      List<SelectDTO> listaIsin = new ArrayList<SelectDTO>();
	      SelectDTO select = null;
	      List<Tmct0Valores> listIsin = valoresBo.findAll();
	      List<ValoresDTO> listIsinDTO = valoresBo.entitiesListToAnDTOList(listIsin);

	      for (ValoresDTO valoresDTO : listIsinDTO) {
	        select = new SelectDTO(valoresDTO.getCodigo(), valoresDTO.getCodigo() + " -  " + valoresDTO.getDescripcion());
	        listaIsin.add(select);
	      }

	      map.put("listaIsin", listaIsin);

	    } catch (Exception e) {
	      LOG.error("Error metodo init - Generacion Informes " + e.getMessage(), e);
	      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda - cargarIsin");
	    }

	    LOG.debug("Fin inicializacion datos filtro de Generacion Informes - cargarIsin");

	    return map;
	  }

	  /**
	   * Generar query del informe dinamico de los filtros pasados (los filtros contienen la plantilla del informe)
	   * 
	   * @param paramsObjects
	   * @return
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> getQueryInforme(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

	    LOG.debug("Entra en getQueryInforme");

	    Map<String, Object> result = new HashMap<String, Object>();
	    Tmct0InformeGeneradoDTO dto = new Tmct0InformeGeneradoDTO();

	    try {
	      // Se recupera el informe solicitado en los parametros.
	      Tmct0GeneracionInformeFilterDTO filters = tmct0InformeAssembler.getTmct0GeneracionInformeFilterDto(paramsObjects);
	      LOG.debug("filters.getIdInforme() " + filters.getIdInforme());

	      Tmct0Informes informe = tmct0InformesBoNEW.getInforme(filters.getIdInforme(),false);

	      dto = tmct0InformesBoNEW.queryInforme(filters, informe);
	      result.put("informe", dto);

	    } catch (Exception e) {
	      LOG.error("Error metodo exportaInformeExcel -  " + e.getMessage(), e.getMessage());
	      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
	    }

	    LOG.debug("Sale de getQueryInforme");

	    return result;

	  }

	  /**
	   * Generar informe dinamico de los filtros pasados (los filtros contienen la plantilla del informe)
	   * 
	   * @param paramsObjects
	   * @return
	   * @throws Exception 
	   */
	  private Map<String, Object> getInformeGenerado(Map<String, Object> paramsObjects) throws Exception {
	    LOG.debug("Entra en getInformeGenerado");
	    
	    Tmct0Informes informe = null;
	    
	    if (paramsObjects.get("plantilla") != ""){
	    	informe = this.tmct0InformesBoNEW.getInforme(Integer.parseInt(String.valueOf(paramsObjects.get("plantilla"))),false);
	      }
	    
	    String codigoUsuario = (String) paramsObjects.get("userName");

	    // Actualiza campos de generacion de informe al inicio
	    this.tmct0InformesBoNEW.saveDatosAdicionalesGenerarInformeInicio(codigoUsuario, informe);
	
	    Map<String, Object> result = new HashMap<String, Object>();
	    
	    Callable<Map<String, Object>> call = new ProcesarExcelnformes(tmct0InformesBoNEW, paramsObjects, informe, "informe");
	    ExecutorService executor = Executors.newFixedThreadPool(1);
	    Future<Map<String, Object>> future = executor.submit(call);
	    executor.shutdown();
	    
	    try{
	    	result = future.get(Long.valueOf(timeout), TimeUnit.MINUTES);
	    } catch (InterruptedException e) {
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    } catch (ExecutionException e) {
	    	result.put("timeout", "Se produjo un error en el lanzamiento del informe. Contacte con soporte IT.");
	    }catch(TimeoutException e){
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }catch(Exception e){
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }
	    
	    return result; 
	  }
	  
	  /**
	   * Generar informe dinamico de los filtros pasados (los filtros contienen la plantilla del informe)
	   * 
	   * @param paramsObjects
	   * @return
	   * @throws Exception 
	   */
	  private Map<String, Object> getInformeGeneradoExcel(Map<String, Object> paramsObjects) throws Exception {
	    LOG.debug("Entra en getInformeGenerado Excel");
	    
	    Tmct0Informes informe = null;
	    
	    if (paramsObjects.get("plantilla") != ""){
	    	informe = this.tmct0InformesBoNEW.getInforme(Integer.parseInt(String.valueOf(paramsObjects.get("plantilla"))),false);
	      }
	    
	    String codigoUsuario = (String) paramsObjects.get("userName");

	    // Actualiza campos de generacion de informe al inicio
	    this.tmct0InformesBoNEW.saveDatosAdicionalesGenerarInformeInicio(codigoUsuario, informe);
	    
	    Map<String, Object> result = new HashMap<String, Object>();
	    
	    Callable<Map<String, Object>> call = new ProcesarExcelnformes(tmct0InformesBoNEW, paramsObjects, informe, "excel");
	    ExecutorService executor = Executors.newFixedThreadPool(1);
	    Future<Map<String, Object>> future = executor.submit(call);
	    executor.shutdown();
	    
	    try{
	    	result = future.get(Long.valueOf(timeout), TimeUnit.MINUTES);
	    } catch (InterruptedException e) {
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    } catch (ExecutionException e) {
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }catch(TimeoutException e){
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }catch(Exception e){
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }
	    
	    return result; 
	  }
	  
	  /**
	   * Generar informe dinamico de los filtros pasados (los filtros contienen la plantilla del informe)
	   * 
	   * @param paramsObjects
	   * @return
	   * @throws Exception 
	   */
	  private Map<String, Object> getInformeGeneradoExcelTerceros(Map<String, Object> paramsObjects) throws Exception {
	    LOG.debug("Entra en getInformeGenerado Excel Terceros");
	    
	
	    Tmct0Informes informe = null;
	    
	    if (paramsObjects.get("plantilla") != ""){
	    	informe = this.tmct0InformesBoNEW.getInforme(Integer.parseInt(String.valueOf(paramsObjects.get("plantilla"))),false);
	      }
	    
	    String codigoUsuario = (String) paramsObjects.get("userName");

	    // Actualiza campos de generacion de informe al inicio
	    this.tmct0InformesBoNEW.saveDatosAdicionalesGenerarInformeInicio(codigoUsuario, informe);
	    
	    Map<String, Object> result = new HashMap<String, Object>();
	    
	    Callable<Map<String, Object>> call = new ProcesarExcelnformes(tmct0InformesBoNEW, paramsObjects, informe, "terceros");
	    ExecutorService executor = Executors.newFixedThreadPool(1);
	    Future<Map<String, Object>> future = executor.submit(call);
	    executor.shutdown();
	    
	    try{
	    	result = future.get(Long.valueOf(timeout), TimeUnit.MINUTES);
	    } catch (InterruptedException e) {
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    } catch (ExecutionException e) {
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }catch(TimeoutException e){
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }catch(Exception e){
	    	result.put("timeout", "El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
	    }
	    
	    return result; 
	  }
	  

	  /**
	   * Gets the command.
	   *
	   * @param command the command
	   * @return the command
	   */
	  private ComandosPlantillas getCommand(String command) {
	    ComandosPlantillas[] commands = ComandosPlantillas.values();
	    ComandosPlantillas result = null;
	    for (int i = 0; i < commands.length; i++) {
	      if (commands[i].getCommand().equalsIgnoreCase(command)) {
	        result = commands[i];
	      }
	    }
	    if (result == null) {
	      result = ComandosPlantillas.SIN_COMANDO;
	    }
	    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
	    return result;
	  }
	  
	  /**
	   * Valida si debe generarse o no un Excel.
	   * @param paramsObjects
	   * @return Map<String, Object>
	   * @throws SIBBACBusinessException
	   */
	  private Map<String, Object> validarGeneracionExcel(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
		  LOG.trace("Inicio Valida Generacion Excel");
		  Map<String, Object> result = new HashMap<String, Object>();
		  
		  Tmct0Informes informe = null;
		  if (paramsObjects.get("plantilla") != "") {
			  Integer idInforme = Integer.parseInt(String.valueOf(paramsObjects.get("plantilla")));
			  if (idInforme != null) {
				  informe = tmct0InformesBoNEW.getInforme(idInforme,false);
				  if (informe != null && informe.getGenFin() == null) {
					  // Cuando un usuario lance un informe, se debe verificar en la tabla TMCT0_INFORMES que el campo GEN_FIN 
					  // es distinto de nulo, si es nulo, se debe dar un warning al usuario indicando
					  StringBuffer sbMsgConfirmGeneraExcel = new StringBuffer();
					  sbMsgConfirmGeneraExcel.append(
						"El informe " + informe.getNbdescription() + " ha sido lanzado por el usuario "
						+ informe.getGenUsu() + " en la fecha: " + DateHelper.convertDateToString(informe.getGenInicio(), "dd/MM/yyyy HH:mm:ss") 
						+ " y todavía no ha finalizado. ¿Desea ejecutar el mismo informe de nuevo sin esperar a la finalización del anterior?");
					  result.put("msgConfirmGeneraExcel", sbMsgConfirmGeneraExcel.toString());				  
				  }
			  }
		  }
		  LOG.trace("Fin Valida Generacion Excel");
		  return result;
	  }
	  
		private Map<String, Object> cargarClases() throws SIBBACBusinessException {
			LOG.trace("Iniciando datos para combo clase del filtro");

			Map<String, Object> mSalida = new HashMap<String, Object>();

			try {

				List<ClaseInformeDTO> listaClases = new ArrayList<ClaseInformeDTO>();
				ClaseInformeDTO claseInformeDTO = null;
				List<Tmct0InformesClases> clases = tmct0InformesClasesDao.getAllInformesClases();
				
				if (null != clases) {
					for (Tmct0InformesClases tmct0InformesClases : clases) {
						claseInformeDTO = new ClaseInformeDTO(tmct0InformesClases.getId().toString(), tmct0InformesClases.getNombre(), tmct0InformesClases.getMercado(), tmct0InformesClases.getClase());
						listaClases.add(claseInformeDTO);
					}
				}
				
				mSalida.put("listClases", listaClases);
			} catch (Exception e) {
				LOG.error("Error metodo init - Clases Informes " + e.getMessage(), e);
				throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
			}

			LOG.debug("Fin inicializacion datos filtro de Plantillas Informes");

			return mSalida;
		}
		
		  /**
		   * Carga las plantillas a raiz de un idClase
		   * 
		   * @return
		   * @throws SIBBACBusinessException
		   */
		  private Map<String, Object> cargarPlantillasMercadoFiltro(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

		    LOG.trace("Iniciando datos para el filtro de Plantillas Informes - cargarPlantillasMercadoFiltro ");

		    Map<String, Object> map = new HashMap<String, Object>();

		    try {
		    	Integer idClase = Integer.parseInt(((Map<String, String>) paramsObjects.get("clase")).get("id"));
		    	String clase = String.valueOf(((Map<String, String>) paramsObjects.get("clase")).get("clase"));
		    	String mercadoFiltro = "";
		    	if (null!=paramsObjects.get("mercado")) {
		    		mercadoFiltro = String.valueOf(((Map<String, String>) paramsObjects.get("mercado")).get("value"));
		    	}
		    	InformeDTO selectPlantillas = null;
		    	SelectDTO selectMercados = null;

		    	//Obtenemos las plantillas asociadas a la clase seleccionada
		    	List<InformeDTO> listaPlantillasFiltro = new ArrayList<InformeDTO>();
		    	List<Tmct0Informes> listaPlantillasInformesFiltro = new ArrayList<Tmct0Informes>();
		    	if ("".equals(mercadoFiltro)) {
		    		listaPlantillasInformesFiltro = tmct0InformesDao.findInformesByClaseOrderByNbdescription(idClase);
		    	} else {
		    		listaPlantillasInformesFiltro = tmct0InformesDao.findInformesByClaseMercadoOrderByNbdescription(idClase,mercadoFiltro);
		    	}

		    	for (Tmct0Informes tmct0Informes : listaPlantillasInformesFiltro) {
			    	//Recuperamos la posicion que ocupa para esa clase
			    	  //Obtenemos el listado de mercados asociados a la clase
			    	  List<String> mercadosClase = tmct0InformesClasesDao.getMercadosInformesClases(tmct0Informes.getTmct0InformesClases().getClase());
			    	  //Recorremos la lista de mercados de la clase para saber que posicion ocupa el especifico de esta clase
			    	  int posicionMercado = 0;
			    	  for (int i=0;i<mercadosClase.size();i++) {
			    		  if (mercadosClase.get(i).equals(tmct0Informes.getTmct0InformesClases().getMercado())) {
			    			  posicionMercado = i;
			    			  break;
			    		  }
			    	  }
			    	selectPlantillas = new InformeDTO(String.valueOf(tmct0Informes.getId()), tmct0Informes.getNbdescription(),posicionMercado,tmct0Informes.getTmct0InformesClases().getFechaBase(),tmct0Informes.getTmct0InformesClases().getClase());
			    	listaPlantillasFiltro.add(selectPlantillas);
		    	}

		    	map.put("listaPlantillasFiltro", listaPlantillasFiltro);
		    	
		    	if ("".equals(mercadoFiltro)) {
		    		//Obtenemos los mercados asociados a la clase seleccionada
		    		List<SelectDTO> listaMercados = new ArrayList<SelectDTO>();
		    		List<String> listaMercadosFiltro = tmct0InformesClasesDao.findMercadoByClase(clase);

		    		for (String mercado : listaMercadosFiltro) {
		    			selectMercados = new SelectDTO(mercado, mercado);
		    			listaMercados.add(selectMercados);
		    		}
		    		//Añadimos a la última posición el mercado TODOS
		    		selectMercados = new SelectDTO("TODOS", "TODOS");
		    		listaMercados.add(selectMercados);

		    		map.put("listaMercados", listaMercados);
		    	}

		    } catch (Exception e) {
		      LOG.error("Error metodo init - Plantillas Informes " + e.getMessage(), e);
		      throw new SIBBACBusinessException(
		                                        "Error en la carga de datos iniciales de las opciones de busqueda - cargarPlantillasFiltro");
		    }

		    LOG.debug("Fin inicializacion datos filtro de Plantillas Informes - cargarPlantillasMercadoFiltro");

		    return map;
		  }
}