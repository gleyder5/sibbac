package sibbac.business.generacioninformes.database.dto;

public class Tmct0InformeColumnaDTO {

  /** Nombre de columna: NBDESCRIPTION */
  private String cabecera;

  /** Precision del elemento, en caso de tratarse de valores con decimales **/
  private char precision;

  /** Campo que totaliza **/
  private boolean isTotal;
  
  /** Camoi de totalizacion **/
  private char totalizar;
  
  /** Tipo del campo */
  private String tipoCampo;

  /**
 * @return the tipoCampo
 */
public String getTipoCampo() {
	return tipoCampo;
}

/**
 * @param tipoCampo the tipoCampo to set
 */
public void setTipoCampo(String tipoCampo) {
	this.tipoCampo = tipoCampo;
}

public String getCabecera() {
    return cabecera;
  }

  public void setCabecera(String cabecera) {
    this.cabecera = cabecera;
  }

  public char getPrecision() {
    return precision;
  }

  public void setPrecision(char c) {
    this.precision = c;
  }

  public boolean isTotal() {
    return isTotal;
  }

  public void setTotal(boolean isTotal) {
    this.isTotal = isTotal;
  }

  public char getTotalizar() {
	return this.totalizar;
  }
	
  public void setTotalizar(char totalizar) {
	this.totalizar = totalizar;
  }

}
