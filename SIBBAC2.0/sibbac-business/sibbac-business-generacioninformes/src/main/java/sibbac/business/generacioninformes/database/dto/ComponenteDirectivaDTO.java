package sibbac.business.generacioninformes.database.dto;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import sibbac.business.generacioninformes.enums.GeneracionInformesEnum;
import sibbac.business.wrappers.common.SelectDTO;

/**
 * 	DTO para cargar los campos del filtro de informes para el 
 * 	componente generico campo dinamico.
 */
public class ComponenteDirectivaDTO {

	/**
	 *	ID 
	 */
	private Integer id;
	
	/**
	 * NOMBRE 
	 */
	private String nombre;
	
	/**
	 * TIPO
	 */
	private String tipo;

	/**
	 * SUBTIPO
	 */
	private String subTipo;
	
	/**
	 * mostrar fecha desde
	 */
	private boolean mostrarFechaDesde = false;

	/**
	 * TABLA
	 */
	private String tabla;

	/**
	 * CAMPO 
	 */
	private String campo;
	
	/**
	 * 	listaSelectDTO
	 */
	private List<SelectDTO> listaSelectDTO;

	/**
	 * 	valorCampo
	 */
	private String valorCampo;
	
	/**
	 * 	valorCampoHasta (se usa para el rango de fecha hasta)
	 */
	private String valorCampoHasta;
	
	/**
	 * 	valor
	 */
	private String valor;
	
	/**
	 * 	valor numero días desde
	 */
	private String valorNumeroFechaDesde;
	
	/**
	 * 	valor numero días hasta
	 */
	private String valorNumeroFechaHasta;
	
	/**
	 * 	query
	 */
	private String query;
	
	/**
	 * 	isInhabilitar
	 */
	private boolean isInhabilitar;
	
	/**
	 * 	isInhabilitaHastar
	 */ 
	private boolean isInhabilitarHasta;

	/**
	 * 	isInhabilitarNumerico
	 */
	private boolean isInhabilitarNumerico;
	
	/**
	 * 	isInhabilitar
	 */
	private boolean isInhabilitarNumericoHasta;
	
	/**
	 *	label boton igual distinto 
	 */
	private String lblBtnSentidoFiltro = "vacio";
	
	/**
	 *	label boton igual distinto hasta
	 */
	private String lblBtnSentidoFiltroHasta = "vacio";
	
	/**
	 *	Texto auxiliar usado para el componente. 
	 */
	private String textoAuxiliar = "IGUAL";
	
	/**
	 * 	listaSelectDTO
	 */
	private List<SelectDTO> listaSeleccSelectDTO;
	
	/**
	 *	label boton tipo busqueda
	 */
	private String lblTipoBusqueda = "vacio";
	
	/**
	 * Constructor no-arg 
	 */
	public ComponenteDirectivaDTO() {}

	/**
	 * Constructor arg 
	 */
	public ComponenteDirectivaDTO(Integer id, String nombre, String tipo, String subTipo,
			String tabla, String campo, List<SelectDTO> listaSelectDTO, String valorCampo, String valorCampoHasta, 
			boolean isInhabilitar, boolean isInhabilitarHasta, boolean isInhabilitarNumerico, boolean isInhabilitarNumericoHasta, 
			String lblBtnSentidoFiltro, String lblBtnSentidoFiltroHasta, List<SelectDTO> listaSeleccSelectDTO, String textoAuxiliar, String lblTipoBusqueda) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.subTipo = subTipo;
		this.tabla = tabla;
		this.campo = campo;
		this.listaSelectDTO = listaSelectDTO;
		
		if(null != valorCampo && valorCampo.matches("[0-9+--]+")){
			this.valorNumeroFechaDesde = valorCampo.substring(1, valorCampo.length());
			if ("NUMERO".equals(tipo)) {
				this.valorCampo = valorCampo;
			}
		}else {
			this.valorCampo = valorCampo;
		}
		
		if(null != valorCampoHasta && valorCampoHasta.matches("[0-9+-]+")){
			this.valorNumeroFechaHasta = valorCampoHasta.substring(1, valorCampoHasta.length());
			if ("NUMERO".equals(tipo) && "DESDE-HASTA".equals(subTipo)) {
				this.valorCampoHasta = valorCampoHasta;
			}
		}else {
			this.valorCampoHasta = valorCampoHasta;
		}
		
		this.isInhabilitar = isInhabilitar;
		this.isInhabilitarHasta = isInhabilitarHasta;
		this.isInhabilitarNumerico = isInhabilitarNumerico;
		this.isInhabilitarNumericoHasta = isInhabilitarNumericoHasta;
		if("".equals(lblBtnSentidoFiltro) || "vacio".equals(lblBtnSentidoFiltro)) {
			this.lblBtnSentidoFiltro = "+";
		}else {
			this.lblBtnSentidoFiltro = lblBtnSentidoFiltro;
		}
		
		if("".equals(lblBtnSentidoFiltroHasta) || "vacio".equals(lblBtnSentidoFiltroHasta)) {
			this.lblBtnSentidoFiltroHasta = "+";
		}else {
			this.lblBtnSentidoFiltroHasta = lblBtnSentidoFiltroHasta;
		}
		
		this.listaSeleccSelectDTO = listaSeleccSelectDTO;
		this.textoAuxiliar = textoAuxiliar;
		
		if("".equals(lblTipoBusqueda) || "vacio".equals(lblTipoBusqueda) || null == lblTipoBusqueda) {
			this.lblTipoBusqueda = "I";
		}else {
			this.lblTipoBusqueda = lblTipoBusqueda;
		}
	}
	
	/**
	 * @return the lblTipoBusqueda
	 */
	public String getLblTipoBusqueda() {
		return lblTipoBusqueda;
	}

	/**
	 * @param lblTipoBusqueda the lblTipoBusqueda to set
	 */
	public void setLblTipoBusqueda(String lblTipoBusqueda) {
		this.lblTipoBusqueda = lblTipoBusqueda;
	}

	/**
	 * 	@return id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 	@param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * 	@return nombre
	 */
	public String getNombre() {
		return this.nombre;
	}
	
	/**
	 * 	@param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 	@return tipo
	 */
	public String getTipo() {
		return this.tipo;
	}

	/**
	 * 	@param tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * 	@return subTipo
	 */
	public String getSubTipo() {
		return this.subTipo;
	}

	/**
	 * 	@param subTipo
	 */
	public void setSubTipo(String subTipo) {
		this.subTipo = subTipo;
	}
	
	/**
	 * 	@return tabla
	 */
	public String getTabla() {
		return this.tabla;
	}

	/**
	 * 	@param tabla
	 */
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	/**
	 * 	@return campo
	 */
	public String getCampo() {
		return this.campo;
	}

	/**
	 * 	@param campo
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}

	/**
	 * 	@return listaSelectDTO
	 */
	public List<SelectDTO> getListaSelectDTO() {
		return this.listaSelectDTO;
	}

	/**
	 * 	@param listaSelectDTO
	 */
	public void setListaSelectDTO(List<SelectDTO> listaSelectDTO) {
		this.listaSelectDTO = listaSelectDTO;
	}
	
	/**
	 * 	@return valorCampo
	 */
	public String getValorCampo() {
		return this.valorCampo;
	}

	/**
	 * 	@param valorCampo
	 */
	public void setValorCampo(String valorCampo) {
		this.valorCampo = valorCampo;
	}
	
	/**
	 * 	@return valorCampoHasta
	 */
	public String getValorCampoHasta() {
		return this.valorCampoHasta;
	}

	/**
	 * 	@param valorCampoHasta
	 */
	public void setValorCampoHasta(String valorCampoHasta) {
		this.valorCampoHasta = valorCampoHasta;
	}

	/**
	 * 	@return valor
	 */
	public String getValor() {
		return this.valor;
	}

	/**
	 * 	@param valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * 	@return query
	 */
	public String getQuery() {
		return this.query;
	}

	/**
	 * 	@param query
	 */
	public void setQuery(String query) {
		this.query = query;
	}
	
	/**
	 * 	@return isInhabilitar
	 */
	public boolean isInhabilitar() {
		return this.isInhabilitar;
	}

	/**
	 * 	@param isInhabilitar
	 */
	public void setInhabilitar(boolean isInhabilitar) {
		this.isInhabilitar = isInhabilitar;
	}
	
	/**
	 * 	@return isInhabilitarHasta
	 */
	public boolean isInhabilitarHasta() {
		return this.isInhabilitarHasta;
	}

	/**
	 * 	@param isInhabilitarHasta
	 */
	public void setInhabilitarHasta(boolean isInhabilitarHasta) {
		this.isInhabilitarHasta = isInhabilitarHasta;
	}
	
	/**
	 * 	@return isInhabilitarNumerico
	 */
	public boolean isInhabilitarNumerico() {
		return this.isInhabilitarNumerico;
	}

	/**
	 * 	@param isInhabilitarNumerico
	 */
	public void setInhabilitarNumerico(boolean isInhabilitarNumerico) {
		this.isInhabilitarNumerico = isInhabilitarNumerico;
	}
	
	/**
	 * 	@return isInhabilitarNumericoHasta
	 */
	public boolean isInhabilitarNumericoHasta() {
		return this.isInhabilitarNumericoHasta;
	}

	/**
	 * 	@param isInhabilitarNumericoHasta
	 */
	public void setInhabilitarNumericoHasta(boolean isInhabilitarNumericoHasta) {
		this.isInhabilitarNumericoHasta = isInhabilitarNumericoHasta;
	}
	
	/**
	 * 	@return lblBtnSentidoFiltro
	 */
	public String getLblBtnSentidoFiltro() {
		return this.lblBtnSentidoFiltro;
	}

	/**
	 * 	@param lblBtnSentidoFiltro
	 */
	public void setLblBtnSentidoFiltro(String lblBtnSentidoFiltro) {
		this.lblBtnSentidoFiltro = lblBtnSentidoFiltro;
	}
	
	/**
	 * 	@return lblBtnSentidoFiltroHasta
	 */
	public String getLblBtnSentidoFiltroHasta() {
		return this.lblBtnSentidoFiltroHasta;
	}

	/**
	 * 	@param lblBtnSentidoFiltroHasta
	 */
	public void setLblBtnSentidoFiltroHasta(String lblBtnSentidoFiltroHasta) {
		this.lblBtnSentidoFiltroHasta = lblBtnSentidoFiltroHasta;
	}

	/**
	 * 	@return listaSeleccSelectDTO
	 */
	public List<SelectDTO> getListaSeleccSelectDTO() {
		return this.listaSeleccSelectDTO;
	}

	/**
	 * 	@param listaSeleccSelectDTO
	 */
	public void setListaSeleccSelectDTO(List<SelectDTO> listaSeleccSelectDTO) {
		this.listaSeleccSelectDTO = listaSeleccSelectDTO;
	}
	
	/**
	 * 	@return textoAuxiliar
	 */
	public String getTextoAuxiliar() {
		return this.textoAuxiliar;
	}

	/**
	 * 	@param textoAuxiliar
	 */
	public void setTextoAuxiliar(String textoAuxiliar) {
		this.textoAuxiliar = textoAuxiliar;
	}

	/**
	 *	Concatena id y tipo.
	 *	@return id+tipo 
	 */
	public String getIdentificador() {
		return this.tipo + this.id; 
	}
	
	/**
	 *	Devuelve true si el tipo es TEXTO.
	 *	@return boolean
	 */	
	public boolean isTipoTexto() {
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.TipoCompInfFiltroEnum.TEXTO.getTipoComponente()); 
	}
	
	/**
	 *	Devuelve true si el tipo es AUTORRELLENABLE.
	 *	@return boolean
	 */	
	public boolean isTipoAutorrellenable() { 
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.TipoCompInfFiltroEnum.AUTORRELLENABLE.getTipoComponente()); 
	}
	
	/**
	 *	Devuelve true si el tipo es AUTORRELLENABLE.
	 *	@return boolean
	 */	
	public boolean isTipoNumero() {
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.TipoCompInfFiltroEnum.NUMERO.getTipoComponente()); 
	}
	
	/**
	 *	Devuelve true si el tipo es COMBO.
	 *	@return boolean
	 */	
	public boolean isTipoCombo() {
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.TipoCompInfFiltroEnum.COMBO.getTipoComponente()); 
	}
	
	/**
	 *	Devuelve true si el tipo es FECHA.
	 *	@return boolean
	 */	
	public boolean isTipoFecha() {
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.TipoCompInfFiltroEnum.FECHA.getTipoComponente()); 
	}
	
	/**
	 *	Devuelve true si el tipo es LISTA.
	 *	@return boolean
	 */	
	public boolean isTipoLista() {
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.TipoCompInfFiltroEnum.LISTA.getTipoComponente()); 
	}
	
	/**
	 *	Devuelve true si el subtipo es DESDE-HASTA.
	 *	@return boolean
	 */	
	public boolean isSubtipoDesdeHasta() {
		return StringUtils.isNotBlank(this.getTipo()) 
				&& this.getSubTipo().trim().equalsIgnoreCase(
						GeneracionInformesEnum.SubtipoCompInfFiltroEnum.DESDE_HASTA.getSubtipoComponente()); 
	}

	public boolean isMostrarFechaDesde() {
		return mostrarFechaDesde;
	}

	public void setMostrarFechaDesde(boolean mostrarFechaDesde) {
		this.mostrarFechaDesde = mostrarFechaDesde;
	}

	public String getValorNumeroFechaDesde() {
		return valorNumeroFechaDesde;
	}

	public void setValorNumeroFechaDesde(String valorNumeroFechaDesde) {
		this.valorNumeroFechaDesde = valorNumeroFechaDesde;
	}

	public String getValorNumeroFechaHasta() {
		return valorNumeroFechaHasta;
	}

	public void setValorNumeroFechaHasta(String valorNumeroFechaHasta) {
		this.valorNumeroFechaHasta = valorNumeroFechaHasta;
	}
	
	

}
