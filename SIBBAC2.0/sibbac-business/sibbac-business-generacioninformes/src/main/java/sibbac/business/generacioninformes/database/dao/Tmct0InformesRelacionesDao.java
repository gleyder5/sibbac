package sibbac.business.generacioninformes.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0InformesRelaciones;

@Repository
public interface Tmct0InformesRelacionesDao  extends JpaRepository <Tmct0InformesRelaciones, String>{

	@Query( "SELECT m FROM Tmct0InformesRelaciones m WHERE m.cdtabla1 = :alias" )
	Tmct0InformesRelaciones findByAlias(@Param("alias") String alias);
	
	@Query( "SELECT ir.cdtabla2 FROM Tmct0InformesRelaciones ir WHERE ir.cdtabla1 = :cdTabla1" )
	String findByCdtabla1(@Param("cdTabla1") String cdTabla1);
	
	@Query("SELECT ir FROM Tmct0InformesRelaciones ir WHERE ir.cdtabla1 = :cdtabla1 AND ir.clase = :clase AND ir.mercado = :mercado")
	Tmct0InformesRelaciones findByCdTabla1ClaseMercado(@Param("cdtabla1") String cdTabla1,@Param("clase") String clase,@Param("mercado") String mercado);
}


