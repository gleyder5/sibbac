package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0InformesTablas;

@Repository
public interface Tmct0InformesTablasDao  extends JpaRepository<Tmct0InformesTablas, Integer> {

	@Query (" SELECT IT.tabla FROM Tmct0InformesTablas IT WHERE IT.tmct0Informes.id = :id")
	public List<String> getTablasByIdInforme(@Param("id") int id);
	
	@Modifying @Query(value="DELETE FROM TMCT0_INFORMES_TABLAS WHERE IDINFORME IN (:id)",nativeQuery=true)
	  public void deleteVariosInformeTablas(@Param("id") List<Integer> id);
}
