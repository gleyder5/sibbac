/**
 * 
 */
package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0InformesClases;

/**
 * @author lucio.vilar
 *
 */
@Repository
public interface Tmct0InformesClasesDao extends JpaRepository<Tmct0InformesClases, Integer> {

	@Query( "SELECT m FROM Tmct0InformesClases m ORDER BY m.nombre ASC" )
	public List<Tmct0InformesClases> getAllInformesClases();

	@Query( "SELECT DISTINCT (m.mercado) FROM Tmct0InformesClases m ORDER BY m.mercado ASC" )
	public List<String> getMercadosInformesClases();

	@Query( "SELECT m.clase FROM Tmct0InformesClases m WHERE m.clase = :clase")
	public List<String> getClasesConTodosMercados(@Param("clase")String clase);
	
	@Query( "SELECT m.id FROM Tmct0InformesClases m WHERE m.clase =  (SELECT m2.clase FROM Tmct0InformesClases m2 WHERE m2.id = :idClase) AND m.mercado IN (:listaMercados)")
	public List<Integer> getClasesConTodosMercadosInforme(@Param("idClase")Integer idClase, @Param("listaMercados")List<String> listaMercados);
	
	@Query( "SELECT m.id FROM Tmct0InformesClases m WHERE m.clase =  (SELECT m2.clase FROM Tmct0InformesClases m2 WHERE m2.id = :idClase)")
	public List<Integer> getClasesConTodosMercadosInforme(@Param("idClase")Integer idClase);
	
	@Query( "SELECT m.id FROM Tmct0InformesClases m WHERE m.clase = (SELECT m2.clase FROM Tmct0InformesClases m2 WHERE m2.id = :idClase) AND m.mercado = :mercado")
	public List<Integer> getClasesConMercadoSelecc(@Param("idClase")Integer idClase, @Param("mercado")String mercado);
	
	@Query( "SELECT m.mercado FROM Tmct0InformesClases m WHERE m.clase = :clase ORDER BY m.id ASC" )
	public List<String> getMercadosInformesClases(@Param("clase")String clase);
	
	@Query( "SELECT DISTINCT m.mercado FROM Tmct0InformesClases m WHERE m.clase = :clase AND m.mercado <> ''  ORDER BY m.mercado ASC" )
	public List<String> findMercadoByClase(@Param("clase")String clase);
}
