package sibbac.business.generacioninformes.threads;

import java.util.Map;
import java.util.concurrent.Callable;

import sibbac.business.generacioninformes.database.bo.Tmct0InformesBoNEW;
import sibbac.business.generacioninformes.database.model.Tmct0Informes;
import sibbac.business.generacioninformes.enums.GeneracionInformesEnum.OrigenInformeExcelEnum;

public class ProcesarExcelnformes implements Callable<Map<String, Object>> {
	
	private final Tmct0InformesBoNEW tmct0InformesBoNEW;
	private final Map<String, Object> paramsObjects;
	private final Tmct0Informes informe;
	private final String tipo;
	
	public ProcesarExcelnformes(Tmct0InformesBoNEW tmct0InformesBoNEW, Map<String, Object> paramsObjects, Tmct0Informes informe, String tipo){
		this.tmct0InformesBoNEW = tmct0InformesBoNEW;
		this.paramsObjects = paramsObjects;
		this.informe = informe;
		this.tipo = tipo;
	}
	
	@Override
	public Map<String, Object> call() throws Exception {
		
		if("informe".equals(this.tipo)){
			return tmct0InformesBoNEW.getInformeExcel(paramsObjects, false, true, null, OrigenInformeExcelEnum.PANTALLA.getOrigen(), informe);
		}else{
			if("excel".equals(this.tipo)){
				return tmct0InformesBoNEW.getInformeExcel(paramsObjects, false, true, null, OrigenInformeExcelEnum.PANTALLA.getOrigen(), informe);
			}else{
				return tmct0InformesBoNEW.getInformeExcel(paramsObjects, true, false, null, OrigenInformeExcelEnum.PANTALLA.getOrigen(), informe);
			}
		}
	}

}
