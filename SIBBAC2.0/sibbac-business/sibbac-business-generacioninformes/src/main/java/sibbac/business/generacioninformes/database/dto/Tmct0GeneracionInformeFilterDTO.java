package sibbac.business.generacioninformes.database.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Tmct0GeneracionInformeFilterDTO {

  private Integer idInforme;
  private Date fechaContratacionDesde;
  private Date fechaContratacionHasta;
  private Date fechaLiquidacionDesde;
  private Date fechaLiquidacionHasta;
  private StringBuffer alias;
  private StringBuffer isin;
  private String opcionesEspeciales;
  // MFG 16/11/2016 Las consultas se tienen que ejecutar dia por dia, se añade un nuevo campo con el dia de ejecución.
  private Date fechaContratacion;
  private List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO = new ArrayList<List<ComponenteDirectivaDTO>>();
  private Date fechaEjecucion;
  private String nameFile;
  private List<Date> fechasEjecucion;
  private String fechaDesdeBase;
  private String fechaHastaBase;

public Date getFechaContratacionDesde() {
    return fechaContratacionDesde;
  }

  public void setFechaContratacionDesde(Date fechaContratacionDesde) {
    this.fechaContratacionDesde = fechaContratacionDesde;
  }

  public Date getFechaContratacionHasta() {
    return fechaContratacionHasta;
  }

  public void setFechaContratacionHasta(Date fechaContratacionHasta) {
    this.fechaContratacionHasta = fechaContratacionHasta;
  }

  public Date getFechaLiquidacionDesde() {
    return fechaLiquidacionDesde;
  }

  public void setFechaLiquidacionDesde(Date fechaLiquidacionDesde) {
    this.fechaLiquidacionDesde = fechaLiquidacionDesde;
  }

  public Date getFechaLiquidacionHasta() {
    return fechaLiquidacionHasta;
  }

  public void setFechaLiquidacionHasta(Date fechaLiquidacionHasta) {
    this.fechaLiquidacionHasta = fechaLiquidacionHasta;
  }

  public StringBuffer getAlias() {
    return alias;
  }

  public void setAlias(StringBuffer alias) {
    this.alias = alias;
  }

  public StringBuffer getIsin() {
    return isin;
  }

  public void setIsin(StringBuffer isin) {
    this.isin = isin;
  }

  public Integer getIdInforme() {
    return idInforme;
  }

  public void setIdInforme(Integer idInforme) {
    this.idInforme = idInforme;
  }

  public String getOpcionesEspeciales() {
    return opcionesEspeciales;
  }

  public void setOpcionesEspeciales(String opcionesEspeciales) {
    this.opcionesEspeciales = opcionesEspeciales;
  }

  public Date getFechaContratacion() {
    return fechaContratacion;
  }

  public void setFechaContratacion(Date fechaContratacion) {
    this.fechaContratacion = fechaContratacion;
  }

  public List<List<ComponenteDirectivaDTO>> getListComponenteDirectivaDTO() {
	  return this.listComponenteDirectivaDTO;
  }

  public void setListComponenteDirectivaDTO(
		  List<List<ComponenteDirectivaDTO>> listComponenteDirectivaDTO) {
	  this.listComponenteDirectivaDTO = listComponenteDirectivaDTO;
  }
  
  public String getNameFile() {
	return nameFile;
 }

	 public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	 }

	/**
	 * @return the fechaEjecucion
	 */
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	
	/**
	 * @param fechaEjecucion the fechaEjecucion to set
	 */
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public List<Date> getFechasEjecucion() {
		return fechasEjecucion;
	}

	public void setFechasEjecucion(List<Date> fechasEjecucion) {
		this.fechasEjecucion = fechasEjecucion;
	}

	/**
	 * @return the fechaDesdeBase
	 */
	public String getFechaDesdeBase() {
		return fechaDesdeBase;
	}

	/**
	 * @param fechaDesdeBase the fechaDesdeBase to set
	 */
	public void setFechaDesdeBase(String fechaDesdeBase) {
		this.fechaDesdeBase = fechaDesdeBase;
	}

	/**
	 * @return the fechaHastaBase
	 */
	public String getFechaHastaBase() {
		return fechaHastaBase;
	}

	/**
	 * @param fechaHastaBase the fechaHastaBase to set
	 */
	public void setFechaHastaBase(String fechaHastaBase) {
		this.fechaHastaBase = fechaHastaBase;
	}
}
