package sibbac.business.generacioninformes.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.generacioninformes.database.model.Tmct0InformesFiltros;

@Repository
public interface Tmct0InformesFiltrosDao  extends JpaRepository<Tmct0InformesFiltros, Integer> {
	
	@Query(value = "SELECT FILT.ID FROM BSNBPSQL.TMCT0_INFORMES_DATOS_FILTROS IDF "
			+ "INNER JOIN BSNBPSQL.TMCT0_INFORMES_FILTROS FILT ON IDF.IDFILTRO = FILT.ID "
			+ "WHERE IDF.IDINFORME = :idInforme ORDER BY FILT.ID ASC", nativeQuery = true)
	public List <Object> getDatosInformeByIdInforme(@Param("idInforme") Integer idInforme);
	
	@Query("SELECT infDxFilt.tmct0InformesFiltros FROM Tmct0InformesDatosFiltros infDxFilt WHERE infDxFilt.tmct0Informes.id = :idInforme")
	public List<Tmct0InformesFiltros> findByIdInforme(@Param("idInforme") Integer idInforme);

//	@Query(value = "SELECT DISTINCT FILT.ID, FILT.NOMBRE, FILT.TIPO, FILT.SUBTIPO, FILT.TABLA, FILT.CAMPO, TIDF.VALOR_FILTRO, FILT.VALOR, FILT.QUERY FROM BSNBPSQL.TMCT0_INFORMES_FILTROS FILT JOIN BSNBPSQL.TMCT0_INFORMES INF ON FILT.IDCLASE = INF.IDCLASE LEFT JOIN BSNBPSQL.TMCT0_INFORMES_DATOS_FILTROS TIDF ON TIDF.IDINFORME = INF.IDINFORME AND FILT.ID = TIDF.IDFILTRO WHERE INF.IDINFORME = :idInf OR INF.IDPADRE= :idInf", nativeQuery = true)
	@Query(value = "SELECT * FROM (SELECT DISTINCT INFILT.ID, INFILT.NOMBRE, INFILT.TIPO, INFILT.SUBTIPO, INFILT.CDTABLA, INFILT.CAMPO, CAST (INFDATFIL.VALOR_FILTRO AS VARCHAR(30000)), INFILT.VALOR, INFILT.QUERY, INFDATFIL.TIPO_BUSQUEDA FROM BSNBPSQL.TMCT0_INFORMES_FILTROS INFILT FULL OUTER JOIN BSNBPSQL.TMCT0_INFORMES_DATOS_FILTROS INFDATFIL ON INFDATFIL.IDINFORME IN  (SELECT INF.IDINFORME FROM BSNBPSQL.TMCT0_INFORMES INF WHERE INF.IDINFORME = :idInf OR INF.IDPADRE = :idInf) AND INFDATFIL.IDFILTRO=INFILT.ID WHERE INFILT.CLASE = (SELECT CLASE FROM TMCT0_INFORMES_CLASES IC JOIN TMCT0_INFORMES I ON I.IDCLASE=IC.ID WHERE I.IDINFORME = :idInf OR I.IDPADRE = :idInf GROUP BY CLASE))TABLA  WHERE TABLA.ID IS NOT NULL", nativeQuery = true) 
	public List<Object[]> findInformesFiltrosAndInformesDatosFiltrosByInformeId(@Param("idInf") Integer idInf);
	
	@Query(value = "SELECT * FROM (SELECT DISTINCT INFILT.ID, INFILT.NOMBRE, INFILT.TIPO, INFILT.SUBTIPO, INFILT.CDTABLA, INFILT.CAMPO, CAST (INFDATFIL.VALOR_FILTRO AS VARCHAR(30000)), INFILT.VALOR, INFILT.QUERY, INFDATFIL.TIPO_BUSQUEDA, INFILT.MERCADO FROM BSNBPSQL.TMCT0_INFORMES_FILTROS INFILT FULL OUTER JOIN BSNBPSQL.TMCT0_INFORMES_DATOS_FILTROS INFDATFIL ON INFDATFIL.IDINFORME IN (SELECT INF.IDINFORME FROM BSNBPSQL.TMCT0_INFORMES INF WHERE INF.IDINFORME = :idInf OR INF.IDPADRE = :idInf) AND INFDATFIL.IDFILTRO=INFILT.ID WHERE INFILT.CLASE = (SELECT CLASE FROM TMCT0_INFORMES_CLASES IC JOIN TMCT0_INFORMES I ON I.IDCLASE=IC.ID WHERE I.IDINFORME=:idInf OR I.IDPADRE=:idInf GROUP BY CLASE) AND (INFILT.MERCADO=:mercado OR INFILT.MERCADO=''))TABLA WHERE TABLA.ID IS NOT NULL", nativeQuery = true) 
	public List<Object[]> findInformesFiltrosAndInformesDatosFiltrosByInformeIdAndMercado(@Param("idInf") Integer idInf, @Param("mercado") String mercado);

	@Query(value = "SELECT DISTINCT FILT.ID, FILT.NOMBRE, FILT.TIPO, FILT.SUBTIPO, FILT.CDTABLA, FILT.CAMPO, NULL, FILT.VALOR, FILT.QUERY FROM BSNBPSQL.TMCT0_INFORMES_FILTROS FILT WHERE FILT.CLASE = :clase", nativeQuery = true)
	public List<Object[]> findInformesFiltrosAndInformesDatosFiltrosByClase(@Param("clase") String clase);
	
	@Query(value = "SELECT DISTINCT FILT.ID, FILT.NOMBRE, FILT.TIPO, FILT.SUBTIPO, FILT.CDTABLA, FILT.CAMPO, NULL, FILT.VALOR, FILT.QUERY FROM BSNBPSQL.TMCT0_INFORMES_FILTROS FILT WHERE FILT.CLASE = :clase AND (FILT.MERCADO = :mercado OR FILT.MERCADO = '')" , nativeQuery = true)
	public List<Object[]> findInformesFiltrosAndInformesDatosFiltrosByClaseAndMercado(@Param("clase") String clase, @Param("mercado") String mercado);
	
	@Query(value = "SELECT filt.CDTABLA FROM BSNBPSQL.TMCT0_INFORMES_FILTROS filt WHERE filt.CLASE = :clase ", nativeQuery = true)
	public List<String> findInformesFiltrosByClase(@Param("clase") String clase);
	
	@Query(value = "SELECT DISTINCT FIL.CDTABLA FROM TMCT0_INFORMES_FILTROS FIL WHERE FIL.ID IN (SELECT DAT.IDFILTRO FROM TMCT0_INFORMES_DATOS_FILTROS DAT WHERE DAT.IDINFORME = :idInforme)", nativeQuery = true)
	public List<String> findInformesFiltrosByIdInforme(@Param("idInforme") Integer idInforme);
	
	
	
}


