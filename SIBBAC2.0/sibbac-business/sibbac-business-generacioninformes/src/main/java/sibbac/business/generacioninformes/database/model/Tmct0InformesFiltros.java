package sibbac.business.generacioninformes.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entidad para la gestion de TMCT0_INFORMES_FILTROS.
 */
@Entity
@Table( name = "TMCT0_INFORMES_FILTROS" )
public class Tmct0InformesFiltros implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1589483921783978327L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	@Column(name = "NOMBRE", length = 50, nullable = false)
	private String nombre;
	
	@Column(name = "CLASE", length = 50, nullable = false)
	private String clase;
	
	@Column(name = "MERCADO", length = 50, nullable = false)
	private String mercado;

	@Column(name = "TIPO", length = 50, nullable = false)
	private String tipo;

	@Column(name = "SUBTIPO", length = 50, nullable = true)
	private String subTipo;

	@Column(name = "CDTABLA", length = 1000, nullable = false)
	private String tabla;

	@Column(name = "QUERY", length = 50, nullable = true)
	private String query;

	@Column(name = "CAMPO", length = 50, nullable = false)
	private String campo;

	@Column(name = "VALOR", length = 50, nullable = true)
	private String valor;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0InformesFiltros() {
	}

	/**
	 *	constructor arg.
	 *	@param id
	 *	@param tmct0InformesClases
	 *	@param nombre
	 *	@param tipo
	 *	@param subTipo
	 *	@param tabla
	 *	@param query
	 *	@param campo
	 *	@param valor
	 */
	public Tmct0InformesFiltros(Integer id, String nombre,
			String tipo, String subTipo, String tabla, String query,
			String campo, String valor, String clase, String mercado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.subTipo = subTipo;
		this.tabla = tabla;
		this.query = query;
		this.campo = campo;
		this.valor = valor;
		this.clase = clase;
		this.mercado = mercado;
	}

	/**
	 *	@return id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 *	@param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *	@return nombre
	 */
	public String getNombre() {
		return this.nombre;
	}

	/**
	 *	@param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 *	@return tipo
	 */
	public String getTipo() {
		return this.tipo;
	}

	/**
	 *	@param tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 *	@return subTipo
	 */
	public String getSubTipo() {
		return this.subTipo;
	}

	/**
	 *	@param subTipo
	 */
	public void setSubTipo(String subTipo) {
		this.subTipo = subTipo;
	}

	/**
	 *	@return tabla
	 */
	public String getTabla() {
		return this.tabla;
	}

	/**
	 *	@param tabla
	 */
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	/**
	 *	@return query
	 */
	public String getQuery() {
		return this.query;
	}

	/**
	 *	@param query
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 *	@return campo
	 */
	public String getCampo() {
		return this.campo;
	}

	/**
	 *	@param campo
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}

	/**
	 *	@return valor
	 */
	public String getValor() {
		return this.valor;
	}

	/**
	 *	@param valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getMercado() {
		return mercado;
	}

	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	
	
	
}
