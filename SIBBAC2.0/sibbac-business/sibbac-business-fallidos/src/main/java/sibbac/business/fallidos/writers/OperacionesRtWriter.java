package sibbac.business.fallidos.writers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.bo.ConstanteH47Bo;
import sibbac.business.fallidos.database.bo.OperacionesTrBo;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.wrappers.common.fileWriter.SibbacFileWriter;
import sibbac.common.SIBBACBusinessException;

/**
 * Escribe los ficheros de Operaciones, Clientes y Titulares MiFIDII.
 * 
 */

@Component
public class OperacionesRtWriter extends SibbacFileWriter<OperacionesRtRecordBean> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(OperacionesRtWriter.class);

  /** Delimitador de líneas. */
  private DelimitedLineAggregator<OperacionesRtRecordBean> delimitedLineAggregator;

  /** Mapeador de campos del tipo de registros a escribir en el fichero. */
  private BeanWrapperFieldExtractor<OperacionesRtRecordBean> fieldExtractorOperacionesRt;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
  /** The paises bo. */

  @Autowired
  private OperacionesTrBo operacionesTrBo;

  @Autowired
  private ConstanteH47Bo constanteH47Bo;

  @Autowired
  Tmct0CnmvLogDao tmct0CnmvLogDao;

  /** Carpeta donde almacenar el fichero generado de operaciones temporal. */
  @Value("${nombre_operaciones_tmp.tr.sending}")
  private String sFileNameTmp;

  /** Carpeta donde almacenar el fichero generado de operaciones definitivo. */
  @Value("${nombre_operaciones.tr.sending}")
  private String sFinalFileName;

  /** Nombre del fichero temporal. */
  private File tempFileOperaciones;

  /** Nombre del fichero definitivo */
  private File file;

  /** Writer para escribir en el fichero. */
  private FlatFileItemWriter<OperacionesRtRecordBean> writer;

  /** Lista con la informacion a escribir. */
  private List<OperacionesRtRecordBean> listaOperaciones;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES
  public OperacionesRtWriter() {
  }

  /**
   * @param listaOperaciones the listaOperaciones to set
   */
  public void setListaOperaciones(List<OperacionesRtRecordBean> listaOperaciones) {
    this.listaOperaciones = listaOperaciones;
  }

  @Autowired
  public OperacionesRtWriter(@Value("${nombre_operaciones_tmp.tr.sending}") String fileOperacionesName) {
    super(fileOperacionesName);
    initalizeFieldExtractorOperacionesRt();
    initializeDelimitedLineAggregator();

  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLMENTADOS
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { SIBBACBusinessException.class, Exception.class })
  @Override
  public boolean writeIntoFile() throws SIBBACBusinessException {

    Boolean bResultado = Boolean.TRUE;

    if (listaOperaciones != null && !listaOperaciones.isEmpty()) {
      try {
        writer = this.getNewWriter();
        writer.write(listaOperaciones);
        writer.close();

      }
      catch (IOException e) {
        bResultado = Boolean.FALSE;
        LOG.error("[writeIntoFile] excepción no controlada. " + e.getMessage());
        writer.close();
      }
      catch (Exception e) {
        bResultado = Boolean.FALSE;
        LOG.error("[writeIntoFile] excepción no controlada. " + e.getMessage());
        writer.close();
      }
    }
    else {
      bResultado = Boolean.FALSE;
    }
    LOG.debug("[writeIntoFile] Fin ...");

    return bResultado;
  }

  public FlatFileItemWriter<OperacionesRtRecordBean> getNewWriter() throws SIBBACBusinessException {
    return this.getNewWriter(new ExecutionContext());

  }

  public FlatFileItemWriter<OperacionesRtRecordBean> getNewWriter(ExecutionContext ctx) throws SIBBACBusinessException {

    try {
      tempFileOperaciones = new File(sFileNameTmp);
      tempFileOperaciones.getParentFile().mkdirs();
      tempFileOperaciones.createNewFile();
      return this.getNewWriter(ctx, tempFileOperaciones);
    }
    catch (IOException e) {
      throw new SIBBACBusinessException("No se ha podido crear el writer para las operaciones: " + e.getMessage(), e);

    }
    catch (Exception e) {
      throw new SIBBACBusinessException("No se ha podido crear el writer para las operaciones: " + e.getMessage(), e);
    }

  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void closeWriter(FlatFileItemWriter<OperacionesRtRecordBean> writer, ExecutionContext ctx) {
    if (writer != null) {
      if (ctx != null) {
        writer.update(ctx);
      }
      writer.close();
    }
  }

  /**
   * Inicializa el delimitador de líneas.
   */
  private void initializeDelimitedLineAggregator() {
    LOG.debug("[initializeDelimitedLineAggregator] Inicio ...");
    delimitedLineAggregator = new DelimitedLineAggregator<OperacionesRtRecordBean>();
    delimitedLineAggregator.setDelimiter("|");
    delimitedLineAggregator.setFieldExtractor(fieldExtractorOperacionesRt);
    LOG.debug("[initializeDelimitedLineAggregator] Fin ...");
  }

  private void initalizeFieldExtractorOperacionesRt() {
    LOG.debug("[initalizeFieldExtractorOperacionesRt] Inicio ...");
    fieldExtractorOperacionesRt = new BeanWrapperFieldExtractor<OperacionesRtRecordBean>();
    fieldExtractorOperacionesRt.setNames(new String[] { "campo1", "campo2", "campo3", "campo4", "campo5", "campo6",
        "campo7", "campo8", "campo9", "campo10", "campo11", "campo12", "campo13", "campo14", "campo15", "campo16",
        "campo17", "campo18", "campo19", "campo20", "campo21", "campo22", "campo23", "campo24", "campo25", "campo26",
        "campo27", "campo28", "campo29", "campo30", "campo31", "campo32", "campo33", "campo34", "campo35", "campo36",
        "campo37", "campo38", "campo39", "campo40", "campo41", "campo42", "campo43", "campo44", "campo45", "campo46",
        "campo47", "campo48", "campo49", "campo50", "campo51", "campo52", "campo53", "campo54", "campo55", "campo56",
        "campo57", "campo58", "campo59", "campo60" });
    LOG.debug("[initalizeFieldExtractorOperacionesRt] Fin ...");
  }

  /**
   * Inicializa el writer para escribir en el fichero.
   * 
   * @param file Fichero en el que escribir.
   */
  private void initalizeWriter(File file) {
    LOG.debug("[initalizeWriter] Inicio ...");
    writer = this.getNewWriter(file);
    LOG.debug("[initalizeWriter] Fin ...");
  }

  /**
   * Inicializa el writer para escribir en el fichero.
   * 
   * @param file Fichero en el que escribir.
   */
  private FlatFileItemWriter<OperacionesRtRecordBean> getNewWriter(File file) {
    return this.getNewWriter(new ExecutionContext(), file);
  }

  /**
   * Inicializa el writer para escribir en el fichero.
   * 
   * @param file Fichero en el que escribir.
   */
  private FlatFileItemWriter<OperacionesRtRecordBean> getNewWriter(ExecutionContext ctx, File file) {
    LOG.debug("[getNewWriter] Inicio ...");
    FlatFileItemWriter<OperacionesRtRecordBean> writer = new FlatFileItemWriter<OperacionesRtRecordBean>();
    writer.setEncoding(StandardCharsets.ISO_8859_1.name());
    writer.setResource(new FileSystemResource(file));
    writer.setLineAggregator(delimitedLineAggregator);
    writer.setTransactional(false);
    writer.setForceSync(true);
    writer.setShouldDeleteIfExists(true);
    writer.setShouldDeleteIfEmpty(true);

    writer.open(ctx);
    LOG.debug("[getNewWriter] Fin ...");
    return writer;
  }

  public void renameTempFile() throws SIBBACBusinessException {

    try {
      String sFinalFileNameAux = sFinalFileName;
      LOG.debug("[renameTempFile] Inicio ...");
      // variable temporal para pruebas
      // String entorno = "c:";

      if (tempFileOperaciones.exists()) {
        sFinalFileNameAux = operacionesTrBo.componerNombreFichero(sFinalFileNameAux);
        // file = new File(entorno.concat(sFinalFileNameAux));
        file = new File(sFinalFileNameAux);
        tempFileOperaciones.renameTo(file);
      }

      LOG.debug("[renameTempFile] Fin ...");

    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }

  }

  public String deleteTempFile() {

    String resultado = null;

    try {
      if (tempFileOperaciones.exists()) {
        tempFileOperaciones.delete();
      }
    }
    catch (Exception e) {
      LOG.warn("[deleteTempFile] excepción no controlada. " + e.getMessage());
      resultado = e.getMessage();
    }

    return resultado;
  }
}
