package sibbac.business.fallidos.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.Fallido;

@Repository
public interface FallidoDao extends JpaRepository<Fallido, Long> {

  @Query("SELECT f FROM Fallido f WHERE f.cdIsin=:isin AND f.feopeliq=:feopeliq AND f.cdCuentaCompensacion=:cdCC AND f.tpoper='V'")
  public List<Fallido> findByFeopeliqIsinAndCC(@Param("feopeliq") Date feopeliq,
                                               @Param("isin") String isin,
                                               @Param("cdCC") String cdCC);

  @Query("SELECT f FROM Fallido f WHERE f.iLiqEcc=:iLiqEcc AND f.liquidado=false AND f.tpoper='V' ORDER BY f.saldoNeto DESC, f.cdAlias ASC")
  public List<Fallido> findByILiqEcc(@Param("iLiqEcc") String iLiqEcc);

} // FallidoDao
