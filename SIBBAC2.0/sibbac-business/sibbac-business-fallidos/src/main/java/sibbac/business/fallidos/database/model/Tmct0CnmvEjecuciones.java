package sibbac.business.fallidos.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonFormat;

import sibbac.business.fase0.database.model.Tmct0estado;

/**
 * Entidad para la gestion de TMCT0_CNMV_EJECUCIONES
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CNMV_EJECUCIONES")
public class Tmct0CnmvEjecuciones implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7760152997717200082L;

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0CnmvEjecuciones.class);

  private static final MathContext EFECTIVO_CONTEXT = new MathContext(2, RoundingMode.HALF_EVEN);

  /** Campos de la entidad */

  /**
   * Identificador del registro en la tabla
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", length = 8, nullable = false)
  private BigInteger id;

  /**
   * Fecha en la que actual de la creación o última modificación
   */
  @Column(name = "AUDIT_DATE", length = 10, nullable = false)
  private Date auditDate;

  /**
   * identificador del usuario que creó o modificó por última vez la factura
   */
  @Column(name = "AUDIT_USER", length = 20, nullable = false)
  private String auditUser;

  /**
   * identificador del usuario que creó o modificó por última vez la factura
   */
  @Column(name = "IDEJECUCION", length = 17, nullable = false)
  private String idEjecucion;

  /**
   * Fecha de ejecucion
   */

  @Temporal(TemporalType.DATE)
  @Column(name = "FEJECUCION", length = 10, nullable = false)
  private Date fEjecucion;

  /**
   * Hora de ejecucion
   */

  @JsonFormat(pattern = "HH:mm:ss")
  @Temporal(TemporalType.TIME)
  @Column(name = "HEJECUCION", length = 10, nullable = false)
  private Date hEjecucion;

  /**
   * Fecha valor
   */

  @Temporal(TemporalType.DATE)
  @Column(name = "FVALOR", length = 10, nullable = false)
  private Date fValor;

  /**
   * B Compra / S Venta
   */

  @Column(name = "SENTIDO", length = 1, nullable = false)
  private Character sentido;

  /**
   * A Ajena / P Propia
   */

  @Column(name = "CAPACIDADNEGOCIACION", length = 1)
  private Character capacidadNegociacion;

  /**
   * Código ISIN del instrumento
   */

  @Column(name = "IDINSTRUMENTO", length = 12, nullable = false)
  private String idInstrumento;

  /**
   * Títulos negociados
   */

  @Column(name = "TITULOS_EJECUCION", precision = 18, scale = 6, nullable = false)
  private BigDecimal titulosEjecucion;

  /**
   * Suma de títulos de los desgloses creados
   */
  @Column(name = "TITULOS_DESGLOSES", precision = 18, scale = 6, nullable = false)
  private BigDecimal titulosDesgloses;

  /**
   * Precio negociado
   */
  @Column(name = "PRECIO", precision = 18, scale = 6, nullable = false)
  private BigDecimal precio;

  /**
   * Efectivo de la operación
   */
  @Column(name = "EFECTIVO", precision = 18, scale = 6, nullable = false)
  private BigDecimal efectivo;

  /**
   * Divisa de la negociación
   */

  @Column(name = "DIVISA", length = 3)
  private String divisa;

  /**
   * Código MIC del mercado
   */

  @Column(name = "SISTEMA_NEGOCIACION", length = 4, nullable = false)
  private String sistemaNegociacion;

  /**
   * Valor fijo ‘M’
   */

  @Column(name = "IND_SISTEMA_NEGOCIACION", length = 1, nullable = false)
  private Character indSistemaNegociacion;

  /**
   * Referencia de la ejecución en la S.V.
   */

  @Column(name = "REFOPERACION", length = 52)
  private String refOperacion;

  @Column(name = "INDICADOR_TRANSMISION", length = 1)
  private Character indicadorTransmision;

  @Column(name = "ID_EJECUTOR", length = 50)
  private String idEjecutor;

  @Column(name = "CD_PAIS_SUCURSAL_EJEC", length = 2)
  private String cdPaisSucursalEjec;

  @Column(name = "INDICADOR_EXENCION", length = 24)
  private String indicadorExencion;

  @Column(name = "INDICADOR_OTC", length = 24)
  private String indicadorOtc;

  /**
   * A Alta / B Baja
   */

  @Column(name = "ESTADO_OPERACION", length = 1, nullable = false)
  private Character estadoOperacion;

  /**
   * Situación del envío del registro
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOENVIO")
  public Tmct0estado cdEstadoEnvio;

  /**
   * Fecha de envío del alta de la ejecución a CNMV
   */

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FECHA_ENVIO_ALTA")
  private Date fEnvioAlta;

  /**
   * Fecha de envío de la baja de la ejecución a CNMV
   */

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FECHA_ENVIO_BAJA")
  private Date fEnvioBaja;

  /**
   * Código de error recibido para el envío realizado
   */

  @Column(name = "CDERRORCNMV", length = 3)
  private String cdErrorCNMV;

  @Column(name = "ID_CAMARA", length = 8)
  private BigInteger idCamara;

  @Column(name = "REPORTABLE", length = 3)
  private String reportable;

  @Column(name = "ID_ENT_CONTRAPARTIDA", length = 20)
  private String idEntContrapartida;

  @Column(name = "TIPO_ID_ENT_CONTRAPARTIDA", length = 3)
  private String tipoIdEntContrapartida;

  @Column(name = "TIPO_CANTIDAD", length = 4)
  private String tipoCantidad;

  @Column(name = "TIPO_PRECIO", length = 4)
  private String tipoPrecio;

  @Column(name = "TIPO_ID_EJECUTOR", length = 3)
  private String tipoIdEjecutor;

  /**
   * fecha/hora ejecucion con milisegundos
   * */
  @Column(name = "FECHA_HORA_EJEC", length = 27)
  private String fechaHoraEjec;

  /**
   * @return the tipoIdEntContrapartida
   */
  public String getTipoIdEntContrapartida() {
    return tipoIdEntContrapartida;
  }

  /**
   * @param tipoIdEntContrapartida the tipoIdEntContrapartida to set
   */
  public void setTipoIdEntContrapartida(String tipoIdEntContrapartida) {
    this.tipoIdEntContrapartida = tipoIdEntContrapartida;
  }

  /**
   * @return the tipoCantidad
   */
  public String getTipoCantidad() {
    return tipoCantidad;
  }

  /**
   * @param tipoCantidad the tipoCantidad to set
   */
  public void setTipoCantidad(String tipoCantidad) {
    this.tipoCantidad = tipoCantidad;
  }

  /**
   * @return the tipoPrecio
   */
  public String getTipoPrecio() {
    return tipoPrecio;
  }

  /**
   * @param tipoPrecio the tipoPrecio to set
   */
  public void setTipoPrecio(String tipoPrecio) {
    this.tipoPrecio = tipoPrecio;
  }

  /**
   * @return the tipoIdEjecutor
   */
  public String getTipoIdEjecutor() {
    return tipoIdEjecutor;
  }

  /**
   * @param tipoIdEjecutor the tipoIdEjecutor to set
   */
  public void setTipoIdEjecutor(String tipoIdEjecutor) {
    this.tipoIdEjecutor = tipoIdEjecutor;
  }

  /**
   * @param idEntContrapartida the idEntContrapartida to set
   */
  public void setIdEntContrapartida(String idEntContrapartida) {
    this.idEntContrapartida = idEntContrapartida;
  }

  public static char corrigeSentido(Character sentidoOperacion) {
    if (sentidoOperacion != null) {
      switch (sentidoOperacion.charValue()) {
        case 'C':
          return 'B';
        case 'V':
          return 'S';
        case 'B':
        case 'S':
          return sentidoOperacion.charValue();
        default:
          return '*';
      }
    }
    return '*';
  }

  /**
   * Constructor
   */
  public Tmct0CnmvEjecuciones() {
    super();
  }

  /**
   * Constructor con parametros
   */

  public Tmct0CnmvEjecuciones(BigInteger id, Date auditDate, String auditUser, String idEjecucion, Date fEjecucion,
      Date hEjecucion, Date fValor, Character sentido, Character capacidadNegociacion, String idInstrumento,
      BigDecimal titulosEjecucion, BigDecimal titulosDesgloses, BigDecimal precio, BigDecimal efectivo, String divisa,
      String sistemaNegociacion, Character indSistemaNegociacion, String refOperacion, Character indicadorTransmision,
      String idEjecutor, String cdPaisSucursalEjec, String indicadorExencion, String indicadorOtc,
      Character estadoOperacion, Tmct0estado cdEstadoEnvio, Date fEnvioAlta, Date fEnvioBaja, String cdErrorCNMV,
      BigInteger idCamara, String reportable, String idEntContrapartida, String tipoIdEntContrapartida,
      String tipoCantidad, String tipoPrecio, String tipoIdEjecutor) {
    super();
    this.id = id;
    this.auditDate = auditDate;
    this.auditUser = auditUser;
    this.idEjecucion = idEjecucion;
    this.fEjecucion = fEjecucion;
    this.hEjecucion = hEjecucion;
    this.fValor = fValor;
    this.sentido = sentido;
    this.capacidadNegociacion = capacidadNegociacion;
    this.idInstrumento = idInstrumento;
    this.titulosEjecucion = titulosEjecucion;
    this.titulosDesgloses = titulosDesgloses;
    this.precio = precio;
    this.efectivo = efectivo;
    this.divisa = divisa;
    this.sistemaNegociacion = sistemaNegociacion;
    this.indSistemaNegociacion = indSistemaNegociacion;
    this.refOperacion = refOperacion;
    this.indicadorTransmision = indicadorTransmision;
    this.idEjecutor = idEjecutor;
    this.cdPaisSucursalEjec = cdPaisSucursalEjec;
    this.indicadorExencion = indicadorExencion;
    this.indicadorOtc = indicadorOtc;
    this.estadoOperacion = estadoOperacion;
    this.cdEstadoEnvio = cdEstadoEnvio;
    this.fEnvioAlta = fEnvioAlta;
    this.fEnvioBaja = fEnvioBaja;
    this.cdErrorCNMV = cdErrorCNMV;
    this.idCamara = idCamara;
    this.reportable = reportable;
    this.idEntContrapartida = idEntContrapartida;
    this.tipoIdEntContrapartida = tipoIdEntContrapartida;
    this.tipoCantidad = tipoCantidad;
    this.tipoPrecio = tipoPrecio;
    this.tipoIdEjecutor = tipoIdEjecutor;
  }

  /**
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * @return the auditDate
   */
  public Date getAuditDate() {
    return auditDate;
  }

  /**
   * @param auditDate the auditDate to set
   */
  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  /**
   * @return the auditUser
   */
  public String getAuditUser() {
    return auditUser;
  }

  /**
   * @param auditUser the auditUser to set
   */
  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * @return the idEjecucion
   */
  public String getIdEjecucion() {
    return idEjecucion;
  }

  /**
   * @param idEjecucion the idEjecucion to set
   */
  public void setIdEjecucion(String idEjecucion) {
    this.idEjecucion = idEjecucion;
  }

  /**
   * @return the fEjecucion
   */
  public Date getfEjecucion() {
    return fEjecucion;
  }

  /**
   * @param fEjecucion the fEjecucion to set
   */
  public void setfEjecucion(Date fEjecucion) {
    this.fEjecucion = fEjecucion;
  }

  /**
   * @return the hEjecucion
   */
  public Date gethEjecucion() {
    return hEjecucion;
  }

  /**
   * @param hEjecucion the hEjecucion to set
   */
  public void sethEjecucion(Date hEjecucion) {
    this.hEjecucion = hEjecucion;
  }

  /**
   * @return the fValor
   */
  public Date getfValor() {
    return fValor;
  }

  /**
   * @param fValor the fValor to set
   */
  public void setfValor(Date fValor) {
    this.fValor = fValor;
  }

  /**
   * @return the sentido
   */
  public Character getSentido() {
    return sentido;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(Character sentido, Boolean bCambio) {
    if (bCambio)
      this.sentido = corrigeSentido(sentido);
    else
      this.sentido = sentido;
  }

  /**
   * @return the capacidadNegociacion
   */
  public Character getCapacidadNegociacion() {
    return capacidadNegociacion;
  }

  /**
   * @param capacidadNegociacion the capacidadNegociacion to set
   */
  public void setCapacidadNegociacion(Character capacidadNegociacion) {
    this.capacidadNegociacion = capacidadNegociacion;
  }

  /**
   * @return the idInstrumento
   */
  public String getIdInstrumento() {
    return idInstrumento;
  }

  /**
   * @param idInstrumento the idInstrumento to set
   */
  public void setIdInstrumento(String idInstrumento) {
    this.idInstrumento = idInstrumento;
  }

  /**
   * @return the titulosEjecucion
   */
  public BigDecimal getTitulosEjecucion() {
    return titulosEjecucion;
  }

  /**
   * @param titulosEjecucion the titulosEjecucion to set
   */
  public void setTitulosEjecucion(BigDecimal titulosEjecucion) {
    this.titulosEjecucion = titulosEjecucion;
  }

  /**
   * @return the titulosDesgloses
   */
  public BigDecimal getTitulosDesgloses() {
    return titulosDesgloses;
  }

  /**
   * @param titulosDesgloses the titulosDesgloses to set
   */
  public void setTitulosDesgloses(BigDecimal titulosDesgloses) {
    this.titulosDesgloses = titulosDesgloses;
  }

  /**
   * @return the precio
   */
  public BigDecimal getPrecio() {
    return precio;
  }

  /**
   * @param precio the precio to set
   */
  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  /**
   * @return the efectivo
   */
  public BigDecimal getEfectivo() {
    return efectivo;
  }

  /**
   * @param efectivo the efectivo to set
   */
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  /**
   * @return the divisa
   */
  public String getDivisa() {
    return divisa;
  }

  /**
   * @param divisa the divisa to set
   */
  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  /**
   * @return the sistemaNegociacion
   */
  public String getSistemaNegociacion() {
    return sistemaNegociacion;
  }

  /**
   * @param sistemaNegociacion the sistemaNegociacion to set
   */
  public void setSistemaNegociacion(String sistemaNegociacion) {
    this.sistemaNegociacion = sistemaNegociacion;
  }

  /**
   * @return the indSistemaNegociacion
   */
  public Character getIndSistemaNegociacion() {
    return indSistemaNegociacion;
  }

  /**
   * @param indSistemaNegociacion the indSistemaNegociacion to set
   */
  public void setIndSistemaNegociacion(Character indSistemaNegociacion) {
    this.indSistemaNegociacion = indSistemaNegociacion;
  }

  /**
   * @return the refOperacion
   */
  public String getRefOperacion() {
    return refOperacion;
  }

  /**
   * @param refOperacion the refOperacion to set
   */
  public void setRefOperacion(String refOperacion) {
    this.refOperacion = refOperacion;
  }

  /**
   * @return the indicadorTransmision
   */
  public Character getIndicadorTransmision() {
    return indicadorTransmision;
  }

  /**
   * @param indicadorTransmision the indicadorTransmision to set
   */
  public void setIndicadorTransmision(Character indicadorTransmision) {
    this.indicadorTransmision = indicadorTransmision;
  }

  /**
   * @return the idEjecutor
   */
  public String getIdEjecutor() {
    return idEjecutor;
  }

  /**
   * @param idEjecutor the idEjecutor to set
   */
  public void setIdEjecutor(String idEjecutor) {
    this.idEjecutor = idEjecutor;
  }

  /**
   * @return the cdPaisSucursalEjec
   */
  public String getCdPaisSucursalEjec() {
    return cdPaisSucursalEjec;
  }

  /**
   * @param cdPaisSucursalEjec the cdPaisSucursalEjec to set
   */
  public void setCdPaisSucursalEjec(String cdPaisSucursalEjec) {
    this.cdPaisSucursalEjec = cdPaisSucursalEjec;
  }

  /**
   * @return the indicadorExencion
   */
  public String getIndicadorExencion() {
    return indicadorExencion;
  }

  /**
   * @param indicadorExencion the indicadorExencion to set
   */
  public void setIndicadorExencion(String indicadorExencion) {
    this.indicadorExencion = indicadorExencion;
  }

  /**
   * @return the indicadorOtc
   */
  public String getIndicadorOtc() {
    return indicadorOtc;
  }

  /**
   * @param indicadorOtc the indicadorOtc to set
   */
  public void setIndicadorOtc(String indicadorOtc) {
    this.indicadorOtc = indicadorOtc;
  }

  /**
   * @return the estadoOperacion
   */
  public Character getEstadoOperacion() {
    return estadoOperacion;
  }

  /**
   * @param estadoOperacion the estadoOperacion to set
   */
  public void setEstadoOperacion(Character estadoOperacion) {
    this.estadoOperacion = estadoOperacion;
  }

  /**
   * @return the cdEstadoEnvio
   */
  public Tmct0estado getCdEstadoEnvio() {
    return cdEstadoEnvio;
  }

  /**
   * @param cdEstadoEnvio the cdEstadoEnvio to set
   */
  public void setCdEstadoEnvio(Tmct0estado cdEstadoEnvio) {
    this.cdEstadoEnvio = cdEstadoEnvio;
  }

  /**
   * @return the fEnvioAlta
   */
  public Date getfEnvioAlta() {
    return fEnvioAlta;
  }

  /**
   * @param fEnvioAlta the fEnvioAlta to set
   */
  public void setfEnvioAlta(Date fEnvioAlta) {
    this.fEnvioAlta = fEnvioAlta;
  }

  /**
   * @return the fEnvioBaja
   */
  public Date getfEnvioBaja() {
    return fEnvioBaja;
  }

  /**
   * @param fEnvioBaja the fEnvioBaja to set
   */
  public void setfEnvioBaja(Date fEnvioBaja) {
    this.fEnvioBaja = fEnvioBaja;
  }

  /**
   * @return the cdErrorCNMV
   */
  public String getCdErrorCNMV() {
    return cdErrorCNMV;
  }

  /**
   * @param cdErrorCNMV the cdErrorCNMV to set
   */
  public void setCdErrorCNMV(String cdErrorCNMV) {
    this.cdErrorCNMV = cdErrorCNMV;
  }

  /**
   * @return the idCamara
   */
  public BigInteger getIdCamara() {
    return idCamara;
  }

  /**
   * @param idCamara the idCamara to set
   */
  public void setIdCamara(BigInteger idCamara) {
    this.idCamara = idCamara;
  }

  /**
   * @return the reportable
   */
  public String getReportable() {
    return reportable;
  }

  /**
   * @param reportable the reportable to set
   */
  public void setReportable(String reportable) {
    this.reportable = reportable;
  }

  /**
   * @return the idEntContrapartida
   */
  public String getIdEntContrapartida() {
    return idEntContrapartida;
  }

  /**
   * @param idEntContrapartida the idEntContrapartida to set
   */
  public void setIdEntContrapartidaa(String idEntContrapartida) {
    this.idEntContrapartida = idEntContrapartida;
  }

  public String getFechaHoraEjec() {
    return fechaHoraEjec;
  }

  public void setFechaHoraEjec(String fechaHoraEjec) {
    this.fechaHoraEjec = fechaHoraEjec;
  }

  @Transient
  public void calculaEfectivo() {
    if (precio != null && titulosEjecucion != null) {
      efectivo = precio.multiply(titulosEjecucion, EFECTIVO_CONTEXT);
    }
    else {
      LOG.warn(
          "[calculaEfectivo] Se ha solicitado calcular el efectivo para la ejecucion [{}] pero sin cumplirse las condiciones:"
              + " precio [{}], titulosEjecucion [{}]", idEjecucion, precio, titulosEjecucion);
    }
  }

}
