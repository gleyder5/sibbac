package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.fallidos.database.bo.TipoMovimientosFallidosBo;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link sibbac.business.fallidos.database.model.MovimientoFallido}.
 * 
 * @version 1.0
 * @author XI316153
 */
@Table(name = DBConstants.FALLIDOS.FALLIDOS_MOVIMIENTOS)
@Entity
public class MovimientoFallido extends ATable<MovimientoFallido> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -3151027891777620491L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Código del tipo de movimiento fallido. */
  @Column(name = "CDTIPO", nullable = false, length = 3)
  private String cdtipo;

  /** Descripcion del tipo de movimiento fallido. */
  @Column(name = "DESCRIPCION", nullable = false, length = 40)
  private String descripcion;

  /** Numero de títulos afectados. */
  @Column(name = "TITULOS_AFECTADOS", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosAfectados;

  /** importe afectado. */
  @Column(name = "SALDO_AFECTADO", nullable = false, precision = 15, scale = 2)
  private BigDecimal saldoAfectado;

  /** Código del holder. */
  @Column(name = "HOLDER", nullable = true, length = 20)
  private String holder;

  /** Código del alias. */
  @Column(name = "ALIAS_CLIENTE", nullable = false, length = 20)
  private String aliasCliente;

  /** Código de la subcuenta. */
  @Column(name = "SUBCUENTA", nullable = true, length = 20)
  private String subcuenta;

  /** Descripción de alias o de la subcuenta. */
  @Column(name = "DESCRALI", nullable = false, length = 130)
  private String descrali;

  /** Codigo del valor. Isin. */
  @Column(name = "ISIN", nullable = false, length = 12)
  private String isin;

  /** Nombre del valor. */
  @Column(name = "NBVALOR", nullable = true, length = 40)
  private String nbvalor;

  /** Camara donde se ha realizado la operación. */
  @Column(name = "CAMARA", nullable = false, length = 40)
  private String camara;

  /** Segmento. */
  @Column(name = "SEGMENTO", nullable = true, length = 40)
  private String segmento;

  /** Sentido de la opearción. */
  @Column(name = "SENTIDO", nullable = false, length = 1)
  private Character sentido;

  /** numero de la operación. */
  @Column(name = "NUMERO_OPERACION_FALLIDA", nullable = false, length = 16)
  private String numero_operacion_fallida;

  /** numero de la opearcion previa. */
  @Column(name = "NUMERO_OPERACION_FALLIDA_PREVIA", nullable = true, length = 16)
  private String numero_operacion_fallida_previa;

  /** referencia evento previa. */
  @Column(name = "REF_EVENTO_CORPORATIVO", nullable = true, length = 35)
  private String refEventoCorporativo;

  /** referencia evento previa. */
  @Column(name = "TRX_TM", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date trxTm;

  /** Fecha de liquidación. */
  @Column(name = "STTLDT", nullable = true)
  @Temporal(TemporalType.DATE)
  private Date sttlDt;

  /** Fecha de liquidación. */
  @Column(name = "TRADEDT", nullable = true)
  @Temporal(TemporalType.DATE)
  private Date tradeDt;

  /** Indica si los movimientos han sido contabilizados. */
  @Column(name = "CONTABILIZADO", nullable = false)
  private Boolean contabilizado;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public MovimientoFallido() {
    this.contabilizado = Boolean.FALSE;
  } // MovimientoFallido

  /**
   * Constructor II. Crea una movimiento fallido a partir de los datos especificados.
   * <p>
   * El constructor ya se encarga de insertar la descripción y el signo a los campos de títulos y efectivo en función
   * del tipo de operación especificada.
   * 
   * @param cdtipo Código de la operación.
   * @param titulosAfectados Número de títulos afectados.
   * @param saldoAfectado Importe afectado.
   * @param holder Identificador del titular de la operación.
   * @param nbholder Nombre del titular de la operación.
   * @param alias Código del alias de la operación.
   * @param subcuenta Código de la subcuenta.
   * @param descrali Descripción del alias o la subcuenta si esta está especificada.
   * @param isin Código del valor.
   * @param nbvalor Nombre del valor.
   * @param camara Camara donde se ha realizado la operación.
   * @param segmento Segmento.
   * @param sentido Sentido de la operación.
   */
  public MovimientoFallido(String cdtipo,
                           BigDecimal titulosAfectados,
                           BigDecimal saldoAfectado,
                           String holder,
                           String alias,
                           String subcuenta,
                           String descrali,
                           String isin,
                           String nbvalor,
                           String camara,
                           String segmento,
                           char sentido,
                           String numero_operacion_fallida,
                           Date trxTm,
                           Date sttlDt,
                           Date tradeDt,
                           TipoMovimientosFallidosBo tmct0tipomovimientofallidoBo) {
    this();

    this.cdtipo = cdtipo;

    this.descripcion = tmct0tipomovimientofallidoBo.getDescripcion(cdtipo);

    if (!tmct0tipomovimientofallidoBo.getModoAfectacion(cdtipo, 0).equals("N")) {
      this.titulosAfectados = new BigDecimal(tmct0tipomovimientofallidoBo.getModoAfectacion(cdtipo, 0)
                                             + titulosAfectados);
    } else {
      this.titulosAfectados = BigDecimal.ZERO;
    } // else

    if (!tmct0tipomovimientofallidoBo.getModoAfectacion(cdtipo, 1).equals("N")) {
      this.saldoAfectado = new BigDecimal(tmct0tipomovimientofallidoBo.getModoAfectacion(cdtipo, 1) + saldoAfectado);
    } else {
      this.saldoAfectado = BigDecimal.ZERO;
    } // else

    this.holder = holder;
    this.aliasCliente = alias;
    this.subcuenta = subcuenta;
    this.descrali = descrali;
    this.isin = isin;
    this.nbvalor = nbvalor;
    this.camara = camara;
    this.segmento = segmento;
    this.sentido = sentido;

    this.trxTm = trxTm;
    this.sttlDt = sttlDt;
    this.tradeDt = tradeDt;

    this.numero_operacion_fallida = numero_operacion_fallida;
  } // MovimientoFallido

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "MovimientoFallido [cdtipo=" + cdtipo + ", descripcion=" + descripcion + ", titulosAfectados="
           + titulosAfectados + ", saldoAfectado=" + saldoAfectado + ", holder=" + holder + ", aliasCliente="
           + aliasCliente + ", subcuenta=" + subcuenta + ", descrali=" + descrali + ", isin=" + isin + ", nbvalor="
           + nbvalor + ", camara=" + camara + ", segmento=" + segmento + ", sentido=" + sentido
           + ", numero_operacion_fallida=" + numero_operacion_fallida + ", numero_operacion_fallida_previa="
           + numero_operacion_fallida_previa + ", refEventoCorporativo=" + refEventoCorporativo + ", trxTm=" + trxTm
           + ", sttlDt=" + sttlDt + ", tradeDt=" + tradeDt + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the cdtipo
   */
  public String getCdtipo() {
    return cdtipo;
  }

  /**
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * @return the titulosAfectados
   */
  public BigDecimal getTitulosAfectados() {
    return titulosAfectados;
  }

  /**
   * @return the saldoAfectado
   */
  public BigDecimal getSaldoAfectado() {
    return saldoAfectado;
  }

  /**
   * @return the holder
   */
  public String getHolder() {
    return holder;
  }

  /**
   * @return the aliasCliente
   */
  public String getAliasCliente() {
    return aliasCliente;
  }

  /**
   * @return the subcuenta
   */
  public String getSubcuenta() {
    return subcuenta;
  }

  /**
   * @return the descrali
   */
  public String getDescrali() {
    return descrali;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the nbvalor
   */
  public String getNbvalor() {
    return nbvalor;
  }

  /**
   * @return the camara
   */
  public String getCamara() {
    return camara;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @return the sentido
   */
  public char getSentido() {
    return sentido;
  }

  /**
   * @return the numero_operacion_fallida
   */
  public String getNumero_operacion_fallida() {
    return numero_operacion_fallida;
  }

  /**
   * @return the numero_operacion_fallida_previa
   */
  public String getNumero_operacion_fallida_previa() {
    return numero_operacion_fallida_previa;
  }

  /**
   * @return the refEventoCorporativo
   */
  public String getRefEventoCorporativo() {
    return refEventoCorporativo;
  }

  /**
   * @return the trxTm
   */
  public Date getTrxTm() {
    return trxTm;
  }

  /**
   * @return the sttlDt
   */
  public Date getSttlDt() {
    return sttlDt;
  }

  /**
   * @return the tradeDt
   */
  public Date getTradeDt() {
    return tradeDt;
  }

  /**
   * @return the contabilizado
   */
  public Boolean getContabilizado() {
    return contabilizado;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param cdtipo the cdtipo to set
   */
  public void setCdtipo(String cdtipo) {
    this.cdtipo = cdtipo;
  }

  /**
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * @param titulosAfectados the titulosAfectados to set
   */
  public void setTitulosAfectados(BigDecimal titulosAfectados) {
    this.titulosAfectados = titulosAfectados;
  }

  /**
   * @param saldoAfectado the saldoAfectado to set
   */
  public void setSaldoAfectado(BigDecimal saldoAfectado) {
    this.saldoAfectado = saldoAfectado;
  }

  /**
   * @param holder the holder to set
   */
  public void setHolder(String holder) {
    this.holder = holder;
  }

  /**
   * @param aliasCliente the aliasCliente to set
   */
  public void setAliasCliente(String aliasCliente) {
    this.aliasCliente = aliasCliente;
  }

  /**
   * @param subcuenta the subcuenta to set
   */
  public void setSubcuenta(String subcuenta) {
    this.subcuenta = subcuenta;
  }

  /**
   * @param descrali the descrali to set
   */
  public void setDescrali(String descrali) {
    this.descrali = descrali;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param nbvalor the nbvalor to set
   */
  public void setNbvalor(String nbvalor) {
    this.nbvalor = nbvalor;
  }

  /**
   * @param camara the camara to set
   */
  public void setCamara(String camara) {
    this.camara = camara;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(char sentido) {
    this.sentido = sentido;
  }

  /**
   * @param numero_operacion_fallida the numero_operacion_fallida to set
   */
  public void setNumero_operacion_fallida(String numero_operacion_fallida) {
    this.numero_operacion_fallida = numero_operacion_fallida;
  }

  /**
   * @param numero_operacion_fallida_previa the numero_operacion_fallida_previa to set
   */
  public void setNumero_operacion_fallida_previa(String numero_operacion_fallida_previa) {
    this.numero_operacion_fallida_previa = numero_operacion_fallida_previa;
  }

  /**
   * @param refEventoCorporativo the refEventoCorporativo to set
   */
  public void setRefEventoCorporativo(String refEventoCorporativo) {
    this.refEventoCorporativo = refEventoCorporativo;
  }

  /**
   * @param trxTm the trxTm to set
   */
  public void setTrxTm(Date trxTm) {
    this.trxTm = trxTm;
  }

  /**
   * @param sttlDt the sttlDt to set
   */
  public void setSttlDt(Date sttlDt) {
    this.sttlDt = sttlDt;
  }

  /**
   * @param tradeDt the tradeDt to set
   */
  public void setTradeDt(Date tradeDt) {
    this.tradeDt = tradeDt;
  }

  /**
   * @param contabilizado the contabilizado to set
   */
  public void setContabilizado(Boolean contabilizado) {
    this.contabilizado = contabilizado;
  }

} // MovimientoFallido
