package sibbac.business.fallidos.dto;

import java.math.BigInteger;

import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.common.utils.DateHelper;

public class ResultadoEjecucionDTO {

  private String fEjecucion;
  private String hEjecucion;
  private String usuario;
  private String proceso;
  private BigInteger numAltas;
  private BigInteger numBajas;
  private Character bIncidencia;
  private String comentario;
  
  
  
  
  public ResultadoEjecucionDTO() {

  }


  public ResultadoEjecucionDTO(String fEjecucion,
                               String hEjecucion,
                               String usuario,
                               String proceso,
                               BigInteger numAltas,
                               BigInteger numBajas,
                               Character bIncidencia,
                               String comentario) {
    this.fEjecucion = fEjecucion;
    this.hEjecucion = hEjecucion;
    this.usuario = usuario;
    this.proceso = proceso;
    this.numAltas = numAltas;
    this.numBajas = numBajas;
    this.bIncidencia = bIncidencia;
    this.comentario = comentario;
  }
  

  public ResultadoEjecucionDTO(Tmct0CnmvLog logEntity){
    
    this.fEjecucion = DateHelper.convertDateToString(logEntity.getAuditDate(),"dd-MM-yyyy");
    this.hEjecucion = DateHelper.convertDateToString(logEntity.getAuditDate(),"HH:mm:ss");
    this.usuario = logEntity.getAuditUser();
    this.proceso = logEntity.getProceso();
    this.numAltas = logEntity.getContadorAltas();
    this.numBajas = logEntity.getContadorBajas();
    this.bIncidencia = logEntity.getIncidencia();
    this.comentario = logEntity.getComentario();
    
  }
  
  /**
   * @return the fEjecucion
   */
  public String getfEjecucion() {
    return fEjecucion;
  }
  /**
   * @param fEjecucion the fEjecucion to set
   */
  public void setfEjecucion(String fEjecucion) {
    this.fEjecucion = fEjecucion;
  }
  /**
   * @return the hEjecucion
   */
  public String gethEjecucion() {
    return hEjecucion;
  }
  /**
   * @param hEjecucion the hEjecucion to set
   */
  public void sethEjecucion(String hEjecucion) {
    this.hEjecucion = hEjecucion;
  }
  /**
   * @return the usuario
   */
  public String getUsuario() {
    return usuario;
  }
  /**
   * @param usuario the usuario to set
   */
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
  /**
   * @return the proceso
   */
  public String getProceso() {
    return proceso;
  }
  /**
   * @param proceso the proceso to set
   */
  public void setProceso(String proceso) {
    this.proceso = proceso;
  }
  /**
   * @return the numAltas
   */
  public BigInteger getNumAltas() {
    return numAltas;
  }
  /**
   * @param numAltas the numAltas to set
   */
  public void setNumAltas(BigInteger numAltas) {
    this.numAltas = numAltas;
  }
  /**
   * @return the numBajas
   */
  public BigInteger getNumBajas() {
    return numBajas;
  }
  /**
   * @param numBajas the numBajas to set
   */
  public void setNumBajas(BigInteger numBajas) {
    this.numBajas = numBajas;
  }
  /**
   * @return the bIncidencia
   */
  public Character getbIncidencia() {
    return bIncidencia;
  }
  /**
   * @param bIncidencia the bIncidencia to set
   */
  public void setbIncidencia(Character bIncidencia) {
    this.bIncidencia = bIncidencia;
  }
  /**
   * @return the comentario
   */
  public String getComentario() {
    return comentario;
  }
  /**
   * @param comentario the comentario to set
   */
  public void setComentario(String comentario) {
    this.comentario = comentario;
  }
  
  
  
  
}
