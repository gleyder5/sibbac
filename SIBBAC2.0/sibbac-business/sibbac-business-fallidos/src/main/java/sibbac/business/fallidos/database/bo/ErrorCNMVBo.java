package sibbac.business.fallidos.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.ErrorCNMVDao;
import sibbac.business.fallidos.database.model.ErrorCNMV;
import sibbac.database.bo.AbstractBo;


@Service
public class ErrorCNMVBo extends AbstractBo< ErrorCNMV, Long, ErrorCNMVDao > {

	public static final String	ERROR_EN_OTRA_OPERACION				= "999";
	public static final String	ERROR_PAREJA_NO_ENVIADA				= "998";

	public ErrorCNMV findFirstByCodigo(String codigo){
		return dao.findFirstByCodigo(codigo);
	}

}
