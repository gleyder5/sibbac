package sibbac.business.fallidos.utils;

public class ValidatorFieldTitulares {

	public static String validarTipoRegistro(String tipoRegistro) {
		
		String tipoRegistroValidado = "";
		
		if(tipoRegistro != null && tipoRegistro.length() > 2) {
			tipoRegistroValidado = tipoRegistro.substring(0, 2);
		}else {
			return tipoRegistro;
		}
		
		return tipoRegistroValidado;
	}
	public static String validarRefOperacion(String refOperacion) {
		
		String refOperacionValidado = "";
		
		if(refOperacion != null && refOperacion.length() > 52) {
			refOperacionValidado = refOperacion.substring(0, 52);
		}else {
			return refOperacion;
		}
		
		return refOperacionValidado;
	}
	
	public static String validarIdentificacion(String idEntContrapartida) {
		
		String idEntCompradorValidado = "";
		
		if(idEntContrapartida != null) {
			idEntContrapartida = idEntContrapartida.trim();
		}
			
		if(idEntContrapartida != null && idEntContrapartida.length() > 35) {
			idEntCompradorValidado = idEntContrapartida.substring(0, 35);
		}else {
			return idEntContrapartida;
		}
			
		return idEntCompradorValidado;
	}
	
	public static String validarTipoIdentificacion(String idEntContrapartida) {
		
		String tipoIdComprador = "";
		
		if(idEntContrapartida != null && idEntContrapartida.length() > 35) {
			tipoIdComprador = idEntContrapartida.substring(0, 3);
		}else {
			return idEntContrapartida;
		}
		
		return tipoIdComprador;
	}
	
	/**
	 * Metodo que valida la longitud del valor pasado por parámetro y lo corta si es mayor a la longitud determinada.
	 * 	
	 * @param tipoIdResponsable
	 * @return tipoIdResponsableValidado
	 */
	public static String validarTipoIdentificacionResponsable(String tipoIdResponsable) {
		
		String tipoIdResponsableValidado = "";
		
		if(tipoIdResponsable != null && tipoIdResponsable.length() > 3) {
			tipoIdResponsableValidado = tipoIdResponsable.substring(0, 3);
		}else {
			return tipoIdResponsableValidado;
		}
		
		return tipoIdResponsableValidado;
	}
	
	public static String validarPaisSucursal(String paisSucursual) {
		
		String paisSucursualValidado = "";
		
		if(paisSucursual != null && paisSucursual.length() > 2) {
			paisSucursualValidado = paisSucursual.substring(0, 2);
		}else {
			return paisSucursual;
		}
		
		return paisSucursualValidado;
	}
	
	public static String validarTipoPersona(String tipoPersona) {
		
		String tipoPesonaValidado = "";
		
		if(tipoPersona != null && tipoPersona.length() > 1) {
			tipoPesonaValidado = tipoPersona.substring(0, 1);
		}else {
			return tipoPersona;
		}
		
		return tipoPesonaValidado;
	}
	
	public static String validarNombreApellido(String nombre) {
		
		String nombreCompradorValidado = "";
		
		if(nombre != null) {
			nombre = nombre.trim();
		}
		
		if(nombre != null && nombre.length() > 140) {
			nombreCompradorValidado = nombre.substring(0, 140);
		}else {
			return nombre;
		}
		
		return nombreCompradorValidado;
	}
	
	public static String validarFecNac(String fecNac) {
		
		String fechaNacValidada = "";
		
		if(fecNac != null && fecNac.length() > 10) {
			fechaNacValidada = fecNac.substring(0, 10);
		}else {
			return fecNac;
		}
		
		return fechaNacValidada;
	}
	
	public static String validarIdRespInversion(String IdRespInversion) {
		
		String idRespInversionValidado = "";
		
		if(IdRespInversion != null) {
			IdRespInversion = IdRespInversion.trim();
		}
		
		if(IdRespInversion != null && IdRespInversion.length() > 34) {
			idRespInversionValidado = IdRespInversion.substring(0, 34);
		}else {
			return IdRespInversion;
		}
		
		return idRespInversionValidado;
	}
	
	public static String componerApellidos(String apellido1, String apellido2) {
		
		String apellido = "";
		if(apellido1 == null) {
			apellido1 = Constantes.BLANK;
		}else {
			apellido = apellido1;
		}
		
		if(apellido2 == null) {
			apellido2 = Constantes.BLANK;
		}else {
      apellido = apellido.concat(",").concat(apellido2).trim();
		}
		
		if(apellido.isEmpty()) {
			return apellido1.concat(apellido2).trim();
		}else {
			return apellido.trim();
		}
		
	}
}
