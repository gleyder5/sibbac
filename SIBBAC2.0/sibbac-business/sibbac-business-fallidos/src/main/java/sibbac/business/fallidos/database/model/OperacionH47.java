package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTimeComparator;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link sibbac.database.model.OperacionH47 }. Entity: "OperacionH47".
 * 
 * @version 1.0
 * @since 1.0
 */
@Table(name = DBConstants.FALLIDOS.H47_OPERACION)
@Entity
public class OperacionH47 extends ATable<OperacionH47> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 5514383769621490882L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Temporal(TemporalType.DATE)
  @Column(name = "FHNEGOCIACION")
  private Date fhNegociacion;

  @Temporal(TemporalType.TIME)
  @Column(name = "HNEGOCIACION")
  private Date hNegociacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FHVALOR")
  private Date fhValor;

  @Column(name = "PRECIOUNITARIO", precision = 17, scale = 5)
  private BigDecimal precioUnitario;

  @Column(name = "CANTIDADTOTAL", precision = 17, scale = 5)
  private BigDecimal cantidadTotal;

  @Column(name = "EFECTIVOTOTAL", precision = 18, scale = 5)
  private BigDecimal efectivoTotal;

  @Column(name = "IDTRANSACCION", length = 17)
  private String identificadorTransaccion;

  @Column(name = "SENTIDO", length = 1)
  private Character sentido;

  @Column(name = "CAPACIDADNEGOCIACION", length = 1)
  private Character capacidadNegociacion;

  @Column(name = "IDINSTRUMENTO", length = 60)
  private String idInstrumento;

  @Column(name = "TITULOS", precision = 16, scale = 5)
  private BigDecimal titulos;

  @Column(name = "TITULARPRINCIPAL", length = 150)
  private String titularPrincipal;

  @Column(name = "NBOOKING", nullable = false, length = 20)
  private String nbooking;

  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  private String nucnfclt;

  @Column(name = "NUCNFLIQ", nullable = false, precision = 4)
  private Short nucnfliq;

  @Column(name = "ESTOPERACION")
  private Character estOperacion;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ESTADOENVIO")
  public Tmct0estado estadoEnvio;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "operacionH47")
  private List<TitularesH47> titularesH47;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDERRORCNMV")
  private ErrorCNMV error;
  

  @Column(name = "IDEJECUCION")
  private String idEjecucion;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * 
   */
  public OperacionH47() {
    super();
  } // OperacionH47

  public String getHoraNegociacion() {

      SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
      return sdf.format(hNegociacion);
      
  }
  
  public String getIdEjecucion() {
	return idEjecucion;
}

public void setIdEjecucion(String idEjecucion) {
	this.idEjecucion = idEjecucion;
}

/**
   * 
   * @param operacion
   */
  public OperacionH47(OperacionH47 operacion) {
    this.estOperacion = operacion.getEstOperacion();
    this.sentido = operacion.getSentido();
    // this.fhLiquidacion = operacion.getFhLiquidacion();
    // this.fhEjecucion = operacion.getFhEjecucion();
    this.idInstrumento = operacion.getIdInstrumento();
    this.titulos = operacion.getTitulos();
    this.titularPrincipal = operacion.getTitularPrincipal();
    this.nbooking = operacion.getNbooking();
    this.nucnfclt = operacion.getNucnfclt();
    this.nucnfliq = operacion.getNucnfliq();
    this.identificadorTransaccion = operacion.getIdentificadorTransaccion();
    this.capacidadNegociacion = operacion.getCapacidadNegociacion();
    this.fhNegociacion = operacion.getFhNegociacion();
    this.hNegociacion = operacion.gethNegociacion();
    this.fhValor = operacion.getFhValor();
    this.precioUnitario = operacion.getPrecioUnitario();
    this.cantidadTotal = operacion.getCantidadTotal();
    this.efectivoTotal = operacion.getEfectivoTotal();
  }

  /**
   * 
   * @param fhLiquidacion
   * @param fhEjecucion
   * @param fhNegociacion
   * @param hNegociacion
   * @param fhValor
   * @param precioUnitario
   * @param cantidadTotal
   * @param efectivoTotal
   * @param identificadorTransaccion
   * @param sentido
   * @param capacidadNegociacion
   * @param idInstrumento
   * @param titulos
   * @param titularPrincipal
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   * @param estOperacion
   * @param estadoEnvio
   * @param titularesH47
   * @param error
   */
  public OperacionH47(Date fhLiquidacion,
                      Date fhEjecucion,
                      Date fhNegociacion,
                      Date hNegociacion,
                      Date fhValor,
                      BigDecimal precioUnitario,
                      BigDecimal cantidadTotal,
                      BigDecimal efectivoTotal,
                      String identificadorTransaccion,
                      Character sentido,
                      Character capacidadNegociacion,
                      String idInstrumento,
                      BigDecimal titulos,
                      String titularPrincipal,
                      String nbooking,
                      String nucnfclt,
                      Short nucnfliq,
                      Character estOperacion,
                      Tmct0estado estadoEnvio,
                      List<TitularesH47> titularesH47,
                      ErrorCNMV error) {
    super();
    // this.fhLiquidacion = fhLiquidacion;
    // this.fhEjecucion = fhEjecucion;
    this.fhNegociacion = fhNegociacion;
    this.hNegociacion = hNegociacion;
    this.fhValor = fhValor;
    this.precioUnitario = precioUnitario;
    this.cantidadTotal = cantidadTotal;
    this.efectivoTotal = efectivoTotal;
    this.identificadorTransaccion = identificadorTransaccion;
    this.sentido = sentido;
    this.capacidadNegociacion = capacidadNegociacion;
    this.idInstrumento = idInstrumento;
    this.titulos = titulos;
    this.titularPrincipal = titularPrincipal;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.estOperacion = estOperacion;
    this.estadoEnvio = estadoEnvio;
    this.titularesH47 = titularesH47;
    this.error = error;
  }

  /**
   * Creacion de operacion para los que vienen con ALC
   * 
   * @param alc
   * @param eje
   * @param nudnicifBanco
   */
  public OperacionH47(Tmct0alc alc,
                      String nudnicifBanco,
                      List<Tmct0desgloseclitit> listaClitits,
                      Date horaNegociacionMaxima) {
    BigDecimal precio = BigDecimal.ZERO;
    BigDecimal titulos = BigDecimal.ZERO;
    BigDecimal efectivo = BigDecimal.ZERO;

    for (Tmct0desgloseclitit clitit : listaClitits) {
      if (clitit.getImtitulos() != null && clitit.getImprecio() != null) {
        efectivo = efectivo.add(clitit.getImtitulos().multiply(clitit.getImprecio()));
      }
      if (clitit.getImtitulos() != null) {
        titulos = titulos.add(clitit.getImtitulos());
      }
    }

    if (titulos != null && titulos.compareTo(BigDecimal.ZERO) != 0) {
      precio = efectivo.divide(titulos, 5, RoundingMode.HALF_UP);
    }
    this.efectivoTotal = efectivo.setScale(2, BigDecimal.ROUND_HALF_UP);
    this.precioUnitario = precio;
    this.cantidadTotal = titulos;
    this.titulos = titulos;
    this.estOperacion = 'A';
    if (new Character('C').equals(alc.getTmct0alo().getCdtpoper())) {
      this.sentido = 'B';
    } else {
      this.sentido = 'S';
    }
    // this.fhLiquidacion = alc.getFeopeliq();
    // this.fhEjecucion = alc.getFeejeliq();
    this.idInstrumento = alc.getTmct0alo().getCdisin();
    this.titularPrincipal = alc.getCdniftit();
    this.nbooking = alc.getId().getNbooking();
    this.nucnfclt = alc.getId().getNucnfclt();
    this.nucnfliq = alc.getId().getNucnfliq();
    if (nudnicifBanco.equals(alc.getCdordtit().trim())) {
      this.capacidadNegociacion = 'P';
    } else {
      this.capacidadNegociacion = 'A';
    }

    guardarHoras(horaNegociacionMaxima);

    this.identificadorTransaccion = alc.getTmct0alo().getCdmoniso();

  }

  /**
   * Creacion de operacion para los que vienen con movimiento ecc
   * 
   * @param movimientosOperacionNueva
   * @param nudnicifBanco
   */
  public OperacionH47(Tmct0alc alc,
                      List<Tmct0movimientooperacionnuevaecc> movimientosOperacionNueva,
                      String nudnicifBanco,
                      Date horaNegociacionMaxima) {
    BigDecimal precio = BigDecimal.ZERO;
    BigDecimal titulos = BigDecimal.ZERO;
    BigDecimal efectivo = BigDecimal.ZERO;

    for (Tmct0movimientooperacionnuevaecc operacionNuevaEcc : movimientosOperacionNueva) {
      if (operacionNuevaEcc.getImtitulos() != null && operacionNuevaEcc.getImprecio() != null) {
        efectivo = efectivo.add(operacionNuevaEcc.getImtitulos().multiply(operacionNuevaEcc.getImprecio()));
      }
      if (operacionNuevaEcc.getImtitulos() != null) {
        titulos = titulos.add(operacionNuevaEcc.getImtitulos());
      }
    }

    if (titulos != null && titulos.compareTo(BigDecimal.ZERO) != 0) {
      precio = efectivo.divide(titulos, 5, RoundingMode.HALF_UP);
    }
    this.efectivoTotal = efectivo.setScale(2, BigDecimal.ROUND_HALF_UP);
    this.precioUnitario = precio;
    this.cantidadTotal = titulos;
    this.titulos = titulos;
    this.estOperacion = 'A';
    if (new Character('C').equals(alc.getTmct0alo().getCdtpoper())) {
      this.sentido = 'B';
    } else {
      this.sentido = 'S';
    }
    // this.fhLiquidacion = alc.getFeopeliq();
    // this.fhEjecucion = alc.getFeejeliq();
    this.idInstrumento = alc.getTmct0alo().getCdisin();
    this.titulos = alc.getNutitfallidos();
    this.titularPrincipal = alc.getCdniftit();
    this.nbooking = alc.getId().getNbooking();
    this.nucnfclt = alc.getId().getNucnfclt();
    this.nucnfliq = alc.getId().getNucnfliq();
    if (nudnicifBanco.equals(alc.getCdordtit().trim())) {
      this.capacidadNegociacion = 'P';
    } else {
      this.capacidadNegociacion = 'A';
    }
    guardarHoras(horaNegociacionMaxima);
    this.identificadorTransaccion = alc.getTmct0alo().getCdmoniso();
  }
  
  /**
   * Creacion de operacion para los que vienen con ALC
   * 
   * @param alc
   * @param eje
   * @param nudnicifBanco
   */
  public OperacionH47(Tmct0alc alc,
                      String nudnicifBanco,
                      BigDecimal titulos,
                      BigDecimal efectivo,
                      Date horaNegociacionMaxima) {
    BigDecimal precio = BigDecimal.ZERO;
    
    if (efectivo != null && titulos != null && titulos.signum() > 0) {
      precio = efectivo.divide(titulos, 5, RoundingMode.HALF_UP);
    }
    this.titulos = titulos;
    this.cantidadTotal = titulos;
    this.efectivoTotal = efectivo.setScale(2, BigDecimal.ROUND_HALF_UP);
    this.precioUnitario = precio;

    this.estOperacion = 'A';
    if (new Character('C').equals(alc.getTmct0alo().getCdtpoper())) {
      this.sentido = 'B';
    } else {
      this.sentido = 'S';
    }
    this.idInstrumento = alc.getTmct0alo().getCdisin();
    this.titularPrincipal = alc.getCdniftit();
    this.nbooking = alc.getId().getNbooking();
    this.nucnfclt = alc.getId().getNucnfclt();
    this.nucnfliq = alc.getId().getNucnfliq();
    if (nudnicifBanco.equals(alc.getCdordtit().trim())) {
      this.capacidadNegociacion = 'P';
    } else {
      this.capacidadNegociacion = 'A';
    }
//    guardarHoras(horaNegociacionMaxima);
    this.hNegociacion = horaNegociacionMaxima;
    this.identificadorTransaccion = alc.getTmct0alo().getCdmoniso();
  } // OperacionH47

  private void guardarHoras(Date horaNegociacionMaxima) {
    Date date = new Date();
    DateTimeComparator comparator = DateTimeComparator.getTimeOnlyInstance();

    this.fhNegociacion = date;
    if (comparator.compare(horaNegociacionMaxima, date) < 0) {
      this.hNegociacion = horaNegociacionMaxima;
    } else {
      this.hNegociacion = date;
    }
    this.fhValor = date;
  }

  public Date getFhNegociacion() {
    return fhNegociacion;
  }

  public void setFhNegociacion(Date fhNegociacion) {
    this.fhNegociacion = fhNegociacion;
  }

  public Date gethNegociacion() {
    return hNegociacion;
  }

  public void sethNegociacion(Date hNegociacion) {
    this.hNegociacion = hNegociacion;
  }

  public Date getFhValor() {
    return fhValor;
  }

  public void setFhValor(Date fhValor) {
    this.fhValor = fhValor;
  }

  public BigDecimal getPrecioUnitario() {
    return precioUnitario;
  }

  public void setPrecioUnitario(BigDecimal precioUnitario) {
    this.precioUnitario = precioUnitario;
  }

  public BigDecimal getCantidadTotal() {
    return cantidadTotal;
  }

  public void setCantidadTotal(BigDecimal cantidadTotal) {
    this.cantidadTotal = cantidadTotal;
  }

  public BigDecimal getEfectivoTotal() {
    return efectivoTotal;
  }

  public void setEfectivoTotal(BigDecimal efectivoTotal) {
    this.efectivoTotal = efectivoTotal;
  }

  public String getIdentificadorTransaccion() {
    return identificadorTransaccion;
  }

  public void setIdentificadorTransaccion(String identificadorTransaccion) {
    this.identificadorTransaccion = identificadorTransaccion;
  }

  // public Date getFhLiquidacion() {
  // return fhLiquidacion;
  // }
  //
  // public void setFhLiquidacion( Date fhLiquidacion ) {
  // this.fhLiquidacion = fhLiquidacion;
  // }
  //
  // public Date getFhEjecucion() {
  // return fhEjecucion;
  // }
  //
  // public void setFhEjecucion( Date fhEjecucion ) {
  // this.fhEjecucion = fhEjecucion;
  // }

  public Character getSentido() {
    return sentido;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public Character getCapacidadNegociacion() {
    return capacidadNegociacion;
  }

  public void setCapacidadNegociacion(Character capacidadNegociacion) {
    this.capacidadNegociacion = capacidadNegociacion;
  }

  public String getIdInstrumento() {
    return idInstrumento;
  }

  public void setIdInstrumento(String idInstrumento) {
    this.idInstrumento = idInstrumento;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public String getTitularPrincipal() {
    return titularPrincipal;
  }

  public void setTitularPrincipal(String titularPrincipal) {
    this.titularPrincipal = titularPrincipal;
  }

  public Character getEstOperacion() {
    return estOperacion;
  }

  public void setEstOperacion(Character estOperacion) {
    this.estOperacion = estOperacion;
  }

  public Tmct0estado getEstadoEnvio() {
    return estadoEnvio;
  }

  public void setEstadoEnvio(Tmct0estado estadoEnvio) {
    this.estadoEnvio = estadoEnvio;
  }

  public String getNbooking() {
    return nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public Short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public List<TitularesH47> getTitularesH47() {
    return titularesH47;
  }

  public void setTitularesH47(List<TitularesH47> titularesH47) {
    this.titularesH47 = titularesH47;
  }

  public ErrorCNMV getError() {
    return error;
  }

  public void setError(ErrorCNMV error) {
    this.error = error;
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer("H47_OPERACION");
    // sb.append( " [ISIN==" + this.isin + "]" );
    return sb.toString();
  }

}
