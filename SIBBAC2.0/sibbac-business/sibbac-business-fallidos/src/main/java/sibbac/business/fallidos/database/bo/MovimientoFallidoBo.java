package sibbac.business.fallidos.database.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.MovimientoFallidoDao;
import sibbac.business.fallidos.database.model.Fallido;
import sibbac.business.fallidos.database.model.MovimientoFallido;
import sibbac.business.fallidos.dto.MovimientoFallidoDTO;
import sibbac.business.fallidos.utils.MovimientoFallidoHelper;
import sibbac.database.bo.AbstractBo;

@Service
public class MovimientoFallidoBo extends AbstractBo<MovimientoFallido, Long, MovimientoFallidoDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(MovimientoFallidoBo.class);

  /** Tipo de afectación en títulos. */
  private static final Integer TIPO_AFECTACION_TITULOS = 0;

  /** Tipo de afectación en efectivo. */
  private static final Integer TIPO_AFECTACION_EFECTIVO = 1;

  /** Tipo de afectación en títulos. */
  private static final String RESTA = "-";

  /** Tipo de afectación en efectivo. */
  private static final String SUMA = "+";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>TMCT0_TIPO_MOVIMIENTOS_FALLIDOS</code>. */
  @Autowired
  private TipoMovimientosFallidosBo tipoMovimientoFallidoBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS

  /**
   * 
   * @param filtros
   * @return
   */
  public Map<String, MovimientoFallidoDTO> searchMovimientos(Map<String, String> filtros) {
    Map<String, MovimientoFallidoDTO> result = new HashMap<String, MovimientoFallidoDTO>();
    Iterator<MovimientoFallido> itMovimientos = findAll().iterator();
    MovimientoFallido movFallido = null;
    while (itMovimientos.hasNext()) {
      movFallido = itMovimientos.next();
      MovimientoFallidoDTO movFallidoDTO = MovimientoFallidoHelper.movimientoFallidoToDTO(movFallido);
      result.put(movFallido.getId().toString(), movFallidoDTO);
    } // while
    return result;
  } // searchMovimientos

  /**
   * Crea y persiste un movimiento fallido a partir de los datos especificados.
   * 
   * @param cdtipo Código del tipo de movimiento.
   * @param titulosAfectados Número de títulos afectados.
   * @param saldoAfectado Saldo afectado.
   * @param holder Código del holder.
   * @param nbholder Nombre del holder.
   * @param alias Código de alias.
   * @param subcuenta Código de subcuenta.
   * @param descrali Descripción del alias/subcuenta.
   * @param isin Código del valor.
   * @param nbvalor Nombre del valor.
   * @param camara Camara.
   * @param segmento Segmento.
   * @param sentido Sentido de la operación.
   * @param numero_operacion_fallida Número de instrucción de liquidación ECC.
   * @param trxTm Timestamp de transmisión del fichero.
   * @param sttlDt Fecha de liquidación.
   * @param tradeDt Fecha de contratación.
   * @return <code>sibbac.business.fallidos.database.model.MovimientoFallido - </code>Movimiento fallido.
   */
  public MovimientoFallido createAndSaveMovimientoFallido(String cdTipoMovimiento,
                                                          Fallido f,
                                                          BigDecimal titulosAfectados,
                                                          BigDecimal saldoAfectado,
                                                          Date trxTm,
                                                          Date sttlDt) {
    LOG.debug("[createAndSaveMovimientoFallido] Inicio ...");
    LOG.debug("[createAndSaveMovimientoFallido] titulosAfectados: " + titulosAfectados);
    LOG.debug("[createAndSaveMovimientoFallido] saldoAfectado: " + saldoAfectado);
    MovimientoFallido mf = new MovimientoFallido();

    mf.setCdtipo(cdTipoMovimiento);
    mf.setDescripcion(tipoMovimientoFallidoBo.getDescripcion(cdTipoMovimiento));

    if (tipoMovimientoFallidoBo.getModoAfectacion(cdTipoMovimiento, 0).compareTo("N") != 0) {
      // mf.setTitulosAfectados(new BigDecimal(tipoMovimientoFallidoBo.getModoAfectacion(cdTipoMovimiento, TIPO_AFECTACION_TITULOS) +
      // titulosAfectados));
      mf.setTitulosAfectados(tipoMovimientoFallidoBo.getModoAfectacion(cdTipoMovimiento, TIPO_AFECTACION_TITULOS).compareTo(RESTA) == 0 ? titulosAfectados.negate()
                                                                                                                                    : titulosAfectados);
    } else {
      mf.setTitulosAfectados(BigDecimal.ZERO);
    } // else

    if (tipoMovimientoFallidoBo.getModoAfectacion(cdTipoMovimiento, 1).compareTo("N") != 0) {
      // mf.setSaldoAfectado(new BigDecimal(tipoMovimientoFallidoBo.getModoAfectacion(cdTipoMovimiento, TIPO_AFECTACION_EFECTIVO) + saldoAfectado));
      mf.setSaldoAfectado(tipoMovimientoFallidoBo.getModoAfectacion(cdTipoMovimiento, TIPO_AFECTACION_EFECTIVO).compareTo(RESTA) == 0 ? saldoAfectado.negate()
                                                                                                                                     : saldoAfectado);
    } else {
      mf.setSaldoAfectado(BigDecimal.ZERO);
    } // else

    mf.setHolder("");
    mf.setAliasCliente(f.getCdAlias());
    mf.setSubcuenta("");
    mf.setDescrali(f.getDsAlias());
    mf.setIsin(f.getCdIsin());
    mf.setNbvalor(f.getDsIsin());
    mf.setCamara(f.getCamara());
    mf.setSegmento(f.getSegmento());
    mf.setSentido(f.getTpoper() == 'C' ? '1' : '2');
    mf.setTrxTm(trxTm);
    mf.setSttlDt(sttlDt);

    // mf.setTradeDt(f.getFeejeliq());
    mf.setTradeDt(sttlDt);

    mf.setNumero_operacion_fallida(f.getiLiqEcc());

    dao.save(mf);
    LOG.debug("[createAndSaveMovimientoFallido] Fin ...");

    return mf;
  } // createAndSaveMovimientoFallido

} // MovimientoFallidoBo
