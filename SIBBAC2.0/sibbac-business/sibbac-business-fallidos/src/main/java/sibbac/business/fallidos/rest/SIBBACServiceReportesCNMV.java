package sibbac.business.fallidos.rest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fallidos.database.bo.ConstanteH47Bo;
import sibbac.business.fallidos.database.bo.OperacionH47Bo;
import sibbac.business.fallidos.database.bo.OperacionesH47MtfBo;
import sibbac.business.fallidos.database.bo.OperacionesTrBo;
import sibbac.business.fallidos.database.bo.TitularesH47Bo;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fallidos.database.model.TitularesH47;
import sibbac.business.fallidos.dto.OperacionesH47DTO;
import sibbac.business.fallidos.dto.ResultadoEjecucionDTO;
import sibbac.business.fallidos.dto.TitularesH47DTO;
import sibbac.business.fallidos.writers.OperacionesRtWriter;
import sibbac.business.fallidos.writers.TitularesRtWriter;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0estadoDTO;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.common.utils.DateHelper;
import sibbac.database.DBConstants;
import sibbac.pti.common.SIBBACPTICommons;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * @author XI316153
 */
@SIBBACService
public class SIBBACServiceReportesCNMV implements SIBBACServiceBean {

  @Autowired
  OperacionH47Bo operacionBo;
  @Autowired
  TitularesH47Bo titularesBo;
  @Autowired
  Tmct0afiBo afiBo;
  @Autowired
  Tmct0alcBo alcBo;
  @Autowired
  Tmct0estadoBo estadosBo;
  @Autowired
  ConstanteH47Bo constantesBo;
  @Autowired
  Tmct0cfgBo cfgBo;
  @Autowired
  Tmct0movimientooperacionnuevaeccBo movOperacionNuevaEccBo;
  @Autowired
  Tmct0movimientoeccBo movEccBo;
  @Autowired
  OperacionesH47MtfBo generaMtfBo;
  
  @Autowired
  Tmct0CnmvLogDao tmct0CnmvLogDao; 
  
  @Autowired
  private OperacionesRtWriter writer;
  
  @Autowired
  private TitularesRtWriter writerTitulares;
  
  @Autowired
  private OperacionesTrBo operacionesTrBo;

  // CFF 11/12/2015

  @Autowired
  Tmct0aliDao aliDao;
  
  
  @Value("${mtf.automatic.creation}")
  private String crearMTF;
  
  @Value("${failed.automatic.creation}")
  private String crearFallidos;
  
  @Value("${tr.automatic.creation}")
  private String crearTr;

  /** Referencia para la inserción de logs. */
  protected static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceReportesCNMV.class);

  private static final String NOMBRE_SERVICIO = "SIBBACServiceReportesCNMV";

  private static final String CMD_LIST_OPERACIONES_H47 = "getOperacionesH47";
  private static final String CMD_LIST_TITULARES_H47 = "getTitularesH47";
  private static final String CMD_LIST_TITULAR_BY_ID = "getTitularById";
  private static final String CMD_LIST_ALCS_FROM_BOOKING = "getAlcsFromNbooking";
  private static final String CMD_CONVERT_ALCS = "convertAlcsToH47";
  private static final String CMD_UPDATE_OPERACIONES_H47 = "updateOperacionesH47";
  private static final String CMD_UPDATE_TITULARES_H47 = "updateTitularesH47";
  private static final String CMD_LIST_ESTADOS_ENVIO = "getEstadosEnvio";
  private static final String CMD_UPDATE_ESTADOS_MASIVA = "updateEstadosMasiva";
  private static final String CMD_GET_HORA_MAXIMA = "getHoraMaxima";
  private static final String CMD_GET_HORA_NUM_TITULOS = "getNumTitulosActuales";

  // Ejecuciones automaticas
  private static final String CMD_GET_PARAM_EJECUCION = "getParametrosEjecucion";
  
  private static final String CMD_RESULTADOS_EJECUCIONES = "getResultadosEjecuciones";
  
  
  
  private static final String PARAM_ESTADO_OPERACION = "estoper";
  private static final String PARAM_ESTADO_ENVIO = "estenv";
  private static final String PARAM_FLIQUIDACION_DE = "fliquidacionDe";
  private static final String PARAM_FLIQUIDACION_A = "fliquidacionA";
  private static final String PARAM_FLIQUIDACION = "fliquidacion";
  private static final String PARAM_FEJECUCION_DE = "fejecucionDe";
  private static final String PARAM_FEJECUCION_A = "fejecucionA";
  private static final String PARAM_FEJECUCION = "fejecucion";
  private static final String PARAM_FENEGOCIACION = "fenegociacion";
  private static final String PARAM_FEVALOR = "fevalor";
  private static final String PARAM_HNEGOCIACION = "hnegociacion";
  private static final String PARAM_ID_TRANSACCION = "idTransaccion";
  private static final String PARAM_ISIN = "isin";
  private static final String PARAM_SENTIDO = "sentido";
  private static final String PARAM_TITULAR_PRINCIPAL = "titularPrincipal";
  private static final String PARAM_TITULOS = "titulos";
  private static final String PARAM_NBOOKING = "nbooking";
  private static final String PARAM_NUCNFCLT = "nucnfclt";
  private static final String PARAM_NUCNFLIQ = "nucnfliq";
  private static final String PARAM_ID_OPERACIONH47 = "idOperacionH47";
  private static final String PARAM_ID_TITULARH47 = "idTitularH47";
  private static final String PARAM_CAPACIDAD_NEGOCIACION = "capacidadNegociacion";
  private static final String PARAM_CANTIDAD_TOTAL = "cantidadTotal";
  private static final String PARAM_EFECTIVO_TOTAL = "efectivoTotal";
  private static final String PARAM_PRECIO_UNITARIO = "precioUnitario";
  private static final String PARAM_APELLIDO1 = "apellido1";
  private static final String PARAM_APELLIDO2 = "apellido2";
  private static final String PARAM_NOMBRE = "nombre";
  private static final String PARAM_REP_APELLIDO1 = "repApellido1";
  private static final String PARAM_REP_APELLIDO2 = "repApellido2";
  private static final String PARAM_REP_NOMBRE = "repNombre";
  private static final String PARAM_REP_IDENTIFICACION = "repIdentificacion";
  private static final String PARAM_REP_TPIDENTI = "repTpidenti";
  private static final String PARAM_TPIDENTI = "tpidenti";
  private static final String PARAM_TPSOCIED = "tpsocied";
  private static final String PARAM_IDENTIFICACION = "identificacion";
  private static final String PARAM_ID = "id";
  private static final String PARAM_HORA_MAXIMA = "horaMaxima";

  private static final String PARAM_CANTIDAD_VALORES = "cantidadValores";

  /** Contenido de la respuesta para el comando CMD_LIST. */
  private static final String RESULT_LIST_OPERACIONES_H47 = "listadoOperacionesH47";
  private static final String RESULT_LIST_ESTADOS = "listadoEstados";
  private static final String RESULT_CONVERT_ALCS = "operacionesCreadasDesdeAlc";
  private static final String RESULT_LIST_TITULARES_H47 = "listadoTitularesH47";
  private static final String RESULT_LIST_ALCS = "listadoAlcsByBooking";
  private static final String RESULT_UPDATE_OPERACIONH47 = "resultadoUpdateOperacionH47";

  private static final String ERROR_OPERACION_NO_ENCONTRADA = "No existe una operación con ese id: ";

  // Datos Tmct0cfg
  private static final String TMCT0CFG_APPLICATION = "SBRMV";
  private static final String TMCT0CFG_PROCESS = "IF_RULES";
  private static final String TMCT0CFG_KEYNAME = "sibbac.rules.IF";

  // Datos H47 Constantes
  private static final String H47_CONSTANTES_HORA = "HoraEjecucion";
  private static final String H47_CONSTANTES_NEGOCIACION = "HoraNegociacion";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public SIBBACServiceReportesCNMV() {
  } // SIBBACServiceReportesCNMV

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  @Override
  public WebResponse process(WebRequest webRequest) {
    LOG.debug("[SIBBACServiceReportesCNMV :: process] Processing a web request ...");

    final String command = webRequest.getAction();
    LOG.debug("[SIBBACServiceReportesCNMV :: process] Command: [{}]", command);

    final WebResponse webResponse = new WebResponse();
    switch (command) {
      case CMD_LIST_TITULARES_H47:
        this.getTitularesH47(webRequest, webResponse);
        break;
      case CMD_LIST_TITULAR_BY_ID:
        this.getTitularById(webRequest, webResponse);
        break;
      case CMD_LIST_OPERACIONES_H47:
        this.getOperacionesH47(webRequest, webResponse);
        break;
      case CMD_LIST_ALCS_FROM_BOOKING:
        if (getAlcsFromBookingCheckFilters(webRequest, webResponse)) {
          this.getAlcsFromBooking(webRequest, webResponse);
        }
        break;
      case CMD_CONVERT_ALCS:
        this.convertALCsToH47(webRequest, webResponse);
        break;
      case CMD_LIST_ESTADOS_ENVIO:
        this.getEstadosEnvio(webRequest, webResponse);
        break;
      case CMD_UPDATE_OPERACIONES_H47:
        if (updateOperacionH47CheckFilters(webRequest, webResponse)) {
          this.updateOperacionH47(webRequest, webResponse);
        }
        break;
      case CMD_UPDATE_TITULARES_H47:
        if (updateTitularH47CheckFilters(webRequest, webResponse)) {
          this.updateTitularH47(webRequest, webResponse);
        }
        break;
      case CMD_UPDATE_ESTADOS_MASIVA:
        this.updateEstadosMasiva(webRequest, webResponse);
        break;
      case CMD_GET_HORA_MAXIMA:
        this.getHoraMaxima(webResponse);
        break;
      case CMD_GET_HORA_NUM_TITULOS:
        this.getNumTitulosActuales(webRequest, webResponse);
        break;
      case CMD_GET_PARAM_EJECUCION:
          this.getParametrosEjecucion(webResponse);
          break;
      case CMD_RESULTADOS_EJECUCIONES:
          this.getResultadosEjecuciones(webRequest,webResponse);
          break;
      default:
        break;
    } // switch

    return webResponse;
  } // process

  private void getHoraMaxima(WebResponse webResponse) {
    final String NOMBRE_METODO = "getHoraMaxima";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> resultado = new HashMap<String, String>();

    String horaMaxima = null;
    try {
      horaMaxima = constantesBo.findByCampo(H47_CONSTANTES_HORA).getValor();
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo horaMaxima de "
                + DBConstants.FALLIDOS.H47_CONSTANTES + ":" + e.getMessage());
    }
    resultado.put(PARAM_HORA_MAXIMA, horaMaxima);
    webResponse.setResultados(resultado);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  private void getNumTitulosActuales(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "getNumTitulosActuales";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> resultado = new HashMap<String, String>();

    List<Map<String, String>> params = webRequest.getParams();
    Map<String, String> paramElement = params.get(0);

    final String nbooking = paramElement.get(PARAM_NBOOKING);
    final String nucnfclt = paramElement.get(PARAM_NUCNFCLT);
    final Short nucnfliq = Short.valueOf(paramElement.get(PARAM_NUCNFLIQ));

    Character capacidadNegociadora = 'A';
    Character estadoOperacion = 'A';

    BigInteger numTitulos = BigInteger.ZERO;

    Long id = null;
    if (StringUtils.isNumeric(paramElement.get(PARAM_ID))) {
      id = Long.valueOf(paramElement.get(PARAM_ID));
    } // if

    if (id != null) {

      OperacionH47 operacion = null;
      try {
        operacion = operacionBo.findById(id);
        capacidadNegociadora = operacion.getCapacidadNegociacion();
        estadoOperacion = operacion.getEstOperacion();

      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo operacion con id " + id + ": "
                  + e.getMessage());
      }

      // Se obtiene el numero de titulos
      numTitulos = operacionBo.findNumTitulosDesgloseMovimiento(nbooking, nucnfliq, nucnfclt, capacidadNegociadora,
                                                                estadoOperacion, id);

      // Se obtienen los titulos del desglose cuando se esta modificando una operación, es un valor que no se tiene.
      Tmct0alc miAlc = alcBo.findByNbookingAndNucnfliqAndNucnfclt(nbooking, nucnfliq, nucnfclt);
      BigDecimal titulosAlc = BigDecimal.ZERO;
      List<Tmct0movimientoecc> movimientosEcc = alcBo.getMovimientosEcc(miAlc);

      List<String> listaCdCodigosValidos = new ArrayList<String>();

      Tmct0cfg cfg = cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION, TMCT0CFG_PROCESS,
                                                                TMCT0CFG_KEYNAME);
      listaCdCodigosValidos = Arrays.asList(cfg.getKeyvalue().split(","));

      if (movimientosEcc.size() > 0) {
        LOG.debug("[getNumTitulosActuales] Obtenidos '" + movimientosEcc.size()
                  + "' movimientosEcc para el desglose del nbooking: " + miAlc.getNbooking() + "; nucnfclt: "
                  + miAlc.getNucnfclt() + "; nucnfliq:" + miAlc.getNucnfliq());

        for (Tmct0movimientoecc movimiento : movimientosEcc) {
          LOG.debug("[getNumTitulosActuales] Procesando movimientoecc: " + movimiento.getIdmovimiento());

          if (StringUtils.isNumeric(movimiento.getCdestadomov().trim())
              && Integer.parseInt(movimiento.getCdestadomov().trim()) == 9
              && listaCdCodigosValidos.contains(movimiento.getCuentaCompensacionDestino())) {
            LOG.debug("[getNumTitulosActuales] El movimientoecc cumple las condiciones para ser procesado");

            titulosAlc = titulosAlc.add(movimiento.getImtitulos());

            resultado.put("NumTitululosDesglose", String.valueOf(titulosAlc.intValue()));

          } else {
            LOG.debug("[getNumTitulosActuales] El movimientoecc NO cumple las condiciones para ser procesado");
          } // else
        } // for (Tmct0movimientoecc movimiento : movimientosEcc)
      }
    } else {
      numTitulos = operacionBo.findNumTitulosDesgloseActuales(nbooking, nucnfliq, nucnfclt, capacidadNegociadora,
                                                              estadoOperacion);
    }

    if (numTitulos == null) {
      numTitulos = BigInteger.ZERO;
    }

    resultado.put("NumTitululosActuales", numTitulos.toString());
    webResponse.setResultados(resultado);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  private void updateEstadosMasiva(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "updateEstadosMasiva";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> filtros = webRequest.getFilters();
    final Map<String, String> resultadoOperaciones = new HashMap<String, String>();
    String errorMsg = new String();
    // Integer idEstado = Integer.parseInt( filtros.get(
    // PARAM_ESTADO_OPERACION ) );
    Integer idEstado = Integer.parseInt(filtros.get(PARAM_ESTADO_ENVIO));
    Tmct0estado estado = estadosBo.findById(idEstado);
    int operacionesActualizadas = 0;
    List<Map<String, String>> params = webRequest.getParams();
    for (Map<String, String> paramElement : params) {
      try {
        final String id = paramElement.get(PARAM_ID);
        OperacionH47 operacion = operacionBo.findById(Long.parseLong(id));
        operacion.setEstadoEnvio(estado);
        operacionBo.save(operacion);
        operacionesActualizadas++;
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING actualizando operacion id: "
                  + paramElement.get(PARAM_ID) + " al estado: " + idEstado + ": " + e.getMessage());
        errorMsg = errorMsg + "WARNING actualizando operacion id: " + paramElement.get(PARAM_ID) + " al estado: "
                   + idEstado;
        webResponse.setError(errorMsg);
      }
    }

    String resultado = "Actualizados los estados de " + operacionesActualizadas + " operaciones.";
    resultadoOperaciones.put(RESULT_UPDATE_OPERACIONH47, resultado);
    webResponse.setResultados(resultadoOperaciones);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  private void getTitularById(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "getTitularById";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> filtros = webRequest.getFilters();
    final Map<String, List<TitularesH47DTO>> resultadoTitulares = new HashMap<String, List<TitularesH47DTO>>();
    List<TitularesH47DTO> listaTitulares = new ArrayList<TitularesH47DTO>();

    final Long idH47 = Long.parseLong(filtros.get(PARAM_ID_TITULARH47));
    TitularesH47 operacion = null;
    try {
      operacion = titularesBo.findById(idH47);
      TitularesH47DTO titularDTO = new TitularesH47DTO(operacion);
      listaTitulares.add(titularDTO);
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo titular por id " + idH47 + ": "
                + e.getMessage());
    }
    resultadoTitulares.put(RESULT_LIST_TITULARES_H47, listaTitulares);

    webResponse.setResultados(resultadoTitulares);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  private boolean getAlcsFromBookingCheckFilters(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "getAlcsFromNbooking";
    String errorMsg = new String();
    boolean filtersRight = true;
    Map<String, Integer> sizes = getSizeEntityMethods(new Tmct0alcId());
    Map<String, String> filtros = webRequest.getFilters();
    if (sizes.get("getNbooking") != null) {
      if (StringUtils.isEmpty(filtros.get(PARAM_NBOOKING))) {
        errorMsg = errorMsg + PARAM_NBOOKING + " debe estar definido ";
        filtersRight = false;
      } else if (filtros.get(PARAM_NBOOKING).length() > sizes.get("getNbooking")) {
        errorMsg = errorMsg + PARAM_NBOOKING + " debe tener tamaño máximo " + sizes.get("getNbooking") + ". ";
        filtersRight = false;
      }
    } else {
      LOG.debug("["
                + NOMBRE_SERVICIO
                + " :: "
                + NOMBRE_METODO
                + "] No se pueden chequear los errores porque no se encuentra la información en las anotaciones. Quizá se cambiaron de sitio o nombre. ");
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] ErrMsg: " + errorMsg);
    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
    return filtersRight;

  }

  private void updateTitularH47(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "updateTitularH47";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> resultadoModificacion = new HashMap<String, String>();
    String resultado = "";
    final Map<String, String> filtros = webRequest.getFilters();
    final Long idTitularH47 = Long.parseLong(filtros.get(PARAM_ID_TITULARH47));
    TitularesH47 titular = null;
    try {
      titular = titularesBo.findById(idTitularH47);
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo titular por id" + idTitularH47
                + ": " + e.getMessage());
    }
    if (titular != null) {
      OperacionH47 operacion = titular.getOperacionH47();
      if (operacion.getEstadoEnvio() == null
          || (!EstadosEnumerados.H47.ENVIADA.getId().equals(operacion.getEstadoEnvio().getIdestado())
              && !EstadosEnumerados.H47.ACEPTADA_BME.getId().equals(operacion.getEstadoEnvio().getIdestado()) && !operacion.getEstOperacion()
                                                                                                                           .equals('B'))) {
        modifyTitularFromFilters(filtros, titular);
        try {
          titularesBo.save(titular);
          resultado = "Titular modificado con éxito";
        } catch (Exception e) {
          resultado = "WARNING guardando el titular modificado";
          LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING modificando el titular con id"
                    + idTitularH47 + ": " + e.getMessage());
        }
      } else {
        resultado = "No se puede modificar un titular si el estado de su operación es ENVIADA, ACEPTADA o Baja";
      }
    } else {
      resultado = "No existe titular para ese ID";
    }
    resultadoModificacion.put(RESULT_UPDATE_OPERACIONH47, resultado);
    webResponse.setResultados(resultadoModificacion);
  }

  private Map<String, Integer> getSizeTitularesFields() {
    TitularesH47 titular = new TitularesH47();
    Field[] fields = titular.getClass().getDeclaredFields();
    Map<String, Integer> sizes = new HashMap<String, Integer>();
    for (int i = 0; i < fields.length; i++) {
      Column column = fields[i].getAnnotation(Column.class);
      if (column != null && column.name() != null) {
        sizes.put(fields[i].getName(), new Integer(column.length()));
      }
    }
    return sizes;
  }

  private Map<String, Integer> getSizeOperacionFields() {
    OperacionH47 titular = new OperacionH47();
    Field[] fields = titular.getClass().getDeclaredFields();
    Map<String, Integer> sizes = new HashMap<String, Integer>();
    for (int i = 0; i < fields.length; i++) {
      Column column = fields[i].getAnnotation(Column.class);
      if (column != null && column.name() != null) {
        sizes.put(fields[i].getName(), new Integer(column.length()));
      }
    }
    return sizes;
  }

  private Map<String, Integer> getSizeEntityMethods(Object entity) {
    Method[] method = entity.getClass().getDeclaredMethods();
    Map<String, Integer> sizes = new HashMap<String, Integer>();
    for (int i = 0; i < method.length; i++) {
      Column column = method[i].getAnnotation(Column.class);
      if (column != null && column.name() != null) {
        sizes.put(method[i].getName(), new Integer(column.length()));
      }
    }
    return sizes;
  }

  private boolean updateOperacionH47CheckFilters(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "updateOperacionH47CheckFilters";
    String errorMsg = new String();
    boolean filtersRight = true;
    Map<String, Integer> sizes = getSizeOperacionFields();
    Map<String, String> filtros = webRequest.getFilters();

    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_CAPACIDAD_NEGOCIACION
    // ) ) ) {
    // operacion.setCapacidadNegociacion( filtros.get(
    // PARAM_CAPACIDAD_NEGOCIACION ).charAt( 0 ) );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_ESTADO_ENVIO ) ) &&
    // StringUtils.isNumeric( filtros.get( PARAM_ESTADO_ENVIO ) ) )
    // {
    // operacion.setEstadoEnvio( estadosBo.findById( Integer.parseInt(
    // filtros.get( PARAM_ESTADO_ENVIO ) ) ) );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_ESTADO_OPERACION ) )
    // ) {
    // operacion.setEstOperacion( filtros.get( PARAM_ESTADO_OPERACION
    // ).charAt( 0 ) );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_FEJECUCION ) ) ) {
    // operacion.setFhEjecucion( obtenerFecha( PARAM_FEJECUCION, filtros )
    // );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_FLIQUIDACION ) ) ) {
    // operacion.setFhLiquidacion( obtenerFecha( PARAM_FLIQUIDACION, filtros
    // ) );
    // }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ISIN))
        && filtros.get(PARAM_ISIN).length() > sizes.get("idInstrumento")) {
      errorMsg = errorMsg + PARAM_ISIN + " debe tener tamaño máximo " + sizes.get("idInstrumento") + ". ";
      filtersRight = false;
    }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_SENTIDO ) ) ) {
    // operacion.setSentido( filtros.get( PARAM_SENTIDO ).charAt( 0 ) );
    // }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_TITULAR_PRINCIPAL))
        && filtros.get(PARAM_TITULAR_PRINCIPAL).length() > sizes.get(PARAM_TITULAR_PRINCIPAL)) {
      errorMsg = errorMsg + PARAM_TITULAR_PRINCIPAL + " debe tener tamaño máximo " + sizes.get(PARAM_TITULAR_PRINCIPAL)
                 + ". ";
      filtersRight = false;
    }
    BigDecimal aux;
    if (StringUtils.isNotEmpty(filtros.get(PARAM_TITULOS))) {
      try {
        aux = new BigDecimal(filtros.get(PARAM_TITULOS));
      } catch (Exception e) {
        filtersRight = false;
        errorMsg = errorMsg + PARAM_TITULOS + " debe tener formato de número. ";
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_TITULOS
                  + " a número: " + e.getMessage());
      }
    }

    if (StringUtils.isNotEmpty(filtros.get(PARAM_CANTIDAD_TOTAL))) {
      try {
        aux = new BigDecimal(filtros.get(PARAM_CANTIDAD_TOTAL));
      } catch (Exception e) {
        filtersRight = false;
        errorMsg = errorMsg + PARAM_CANTIDAD_TOTAL + " debe tener formato de número. ";
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_CANTIDAD_TOTAL
                  + " a número: " + e.getMessage());
      }
    }

    if (StringUtils.isNotEmpty(filtros.get(PARAM_EFECTIVO_TOTAL))) {
      try {
        aux = new BigDecimal(filtros.get(PARAM_EFECTIVO_TOTAL));
      } catch (Exception e) {
        filtersRight = false;
        errorMsg = errorMsg + PARAM_EFECTIVO_TOTAL + " debe tener formato de número. ";
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_EFECTIVO_TOTAL
                  + " a número: " + e.getMessage());
      }
    }

    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_FENEGOCIACION ) ) ) {
    // operacion.setFhNegociacion( obtenerFecha( PARAM_FENEGOCIACION,
    // filtros ) );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_FEVALOR ) ) ) {
    // operacion.setFhValor( obtenerFecha( PARAM_FEVALOR, filtros ) );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_HNEGOCIACION ) ) ) {
    // operacion.sethNegociacion( obtenerHora( PARAM_HNEGOCIACION, filtros )
    // );
    // }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ID_TRANSACCION))
        && filtros.get(PARAM_ID_TRANSACCION).length() > sizes.get("identificadorTransaccion")) {
      errorMsg = errorMsg + PARAM_ID_TRANSACCION + " debe tener tamaño máximo " + sizes.get("identificadorTransaccion")
                 + ". ";
      filtersRight = false;
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_PRECIO_UNITARIO))) {
      try {
        aux = new BigDecimal(filtros.get(PARAM_PRECIO_UNITARIO));
      } catch (Exception e) {
        filtersRight = false;
        errorMsg = errorMsg + PARAM_PRECIO_UNITARIO + " debe tener formato de número. ";
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_PRECIO_UNITARIO
                  + " a número: " + e.getMessage());
      }
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING: " + errorMsg);
    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
    return filtersRight;
  }

  private boolean updateTitularH47CheckFilters(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "updateTitularH47CheckFilters";
    String errorMsg = new String();
    boolean filtersRight = true;
    Map<String, Integer> sizes = getSizeTitularesFields();
    Map<String, String> filtros = webRequest.getFilters();
    BigDecimal aux;
    if (StringUtils.isNotEmpty(filtros.get(PARAM_CANTIDAD_VALORES))) {
      try {
        aux = new BigDecimal(filtros.get(PARAM_CANTIDAD_VALORES));
      } catch (Exception e) {
        filtersRight = false;
        errorMsg = errorMsg + PARAM_CANTIDAD_VALORES + " debe tener formato de número. ";
      }
    }

    if (sizes.get(PARAM_APELLIDO1) != null && sizes.get(PARAM_APELLIDO2) != null && sizes.get(PARAM_NOMBRE) != null
        && sizes.get(PARAM_IDENTIFICACION) != null && sizes.get(PARAM_REP_APELLIDO1) != null
        && sizes.get(PARAM_REP_APELLIDO2) != null && sizes.get(PARAM_REP_NOMBRE) != null
        && sizes.get(PARAM_REP_IDENTIFICACION) != null) {

      if (StringUtils.isNotEmpty(filtros.get(PARAM_APELLIDO1))
          && filtros.get(PARAM_APELLIDO1).length() > sizes.get(PARAM_APELLIDO1)) {
        errorMsg = errorMsg + PARAM_APELLIDO1 + " debe tener tamaño máximo " + sizes.get(PARAM_APELLIDO1) + ". ";
        filtersRight = false;
      }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_APELLIDO2))
          && filtros.get(PARAM_APELLIDO2).length() > sizes.get(PARAM_APELLIDO2)) {
        errorMsg = errorMsg + PARAM_APELLIDO2 + " debe tener tamaño máximo " + sizes.get(PARAM_APELLIDO2) + ". ";
        filtersRight = false;
      }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_NOMBRE))
          && filtros.get(PARAM_NOMBRE).length() > sizes.get(PARAM_NOMBRE)) {
        errorMsg = errorMsg + PARAM_NOMBRE + " debe tener tamaño máximo " + sizes.get(PARAM_NOMBRE) + ". ";
        filtersRight = false;
      }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_IDENTIFICACION))
          && filtros.get(PARAM_IDENTIFICACION).length() > sizes.get(PARAM_IDENTIFICACION)) {
        errorMsg = errorMsg + PARAM_IDENTIFICACION + " debe tener tamaño máximo " + sizes.get(PARAM_IDENTIFICACION)
                   + ". ";
        filtersRight = false;
      }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_APELLIDO1))
          && filtros.get(PARAM_REP_APELLIDO1).length() > sizes.get(PARAM_REP_APELLIDO1)) {
        errorMsg = errorMsg + PARAM_REP_APELLIDO1 + " debe tener tamaño máximo " + sizes.get(PARAM_REP_APELLIDO1)
                   + ". ";
        filtersRight = false;
      }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_APELLIDO2))
          && filtros.get(PARAM_REP_APELLIDO2).length() > sizes.get(PARAM_REP_APELLIDO2)) {
        errorMsg = errorMsg + PARAM_REP_APELLIDO2 + " debe tener tamaño máximo " + sizes.get(PARAM_REP_APELLIDO2)
                   + ". ";
        filtersRight = false;
      }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_NOMBRE))
          && filtros.get(PARAM_REP_NOMBRE).length() > sizes.get(PARAM_REP_NOMBRE)) {
        errorMsg = errorMsg + PARAM_REP_NOMBRE + " debe tener tamaño máximo " + sizes.get(PARAM_REP_NOMBRE) + ". ";
        filtersRight = false;
      }
      // if ( StringUtils.isNotEmpty( filtros.get( PARAM_REP_TPIDENTI ) )
      // ) {
      // titular.setRepTpidenti( filtros.get( PARAM_REP_TPIDENTI ).charAt(
      // 0 ) );
      // }
      // if ( StringUtils.isNotEmpty( filtros.get( PARAM_TPIDENTI ) ) ) {
      // titular.setTpidenti( filtros.get( PARAM_TPIDENTI ).charAt( 0 ) );
      // }
      // if ( StringUtils.isNotEmpty( filtros.get( PARAM_TPSOCIED ) ) ) {
      // titular.setTpsocied( filtros.get( PARAM_TPSOCIED ).charAt( 0 ) );
      // }
      if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_IDENTIFICACION))
          && filtros.get(PARAM_REP_IDENTIFICACION).length() > sizes.get(PARAM_REP_IDENTIFICACION)) {
        errorMsg = errorMsg + PARAM_REP_IDENTIFICACION + " debe tener tamaño máximo "
                   + sizes.get(PARAM_REP_IDENTIFICACION) + ". ";
        filtersRight = false;
      }
      if (filtersRight == false) {
        webResponse.setError(errorMsg);
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING: " + errorMsg);
      }
    } else {
      LOG.debug("["
                + NOMBRE_SERVICIO
                + " :: "
                + NOMBRE_METODO
                + "] No se pueden chequear los errores porque no se encuentra la información en las anotaciones. Quizá se cambiaron de sitio o nombre. ");
    }
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
    return filtersRight;
  }

  private void modifyTitularFromFilters(Map<String, String> filtros, TitularesH47 titular) {
    final String NOMBRE_METODO = "modifyTitularFromFilters";
    if (StringUtils.isNotEmpty(filtros.get(PARAM_CANTIDAD_VALORES))) {
      try {
        BigDecimal cantidadValores = new BigDecimal(filtros.get(PARAM_CANTIDAD_VALORES));
        titular.setCantidadValores(cantidadValores);
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_CANTIDAD_VALORES
                  + " a número: " + e.getMessage());
      }
    }

    if (StringUtils.isNotEmpty(filtros.get(PARAM_APELLIDO1))) {
      titular.setApellido1(filtros.get(PARAM_APELLIDO1));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_APELLIDO2))) {
      titular.setApellido2(filtros.get(PARAM_APELLIDO2));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_NOMBRE))) {
      titular.setNombre(filtros.get(PARAM_NOMBRE));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_IDENTIFICACION))) {
      titular.setIdentificacion(filtros.get(PARAM_IDENTIFICACION));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_APELLIDO1))) {
      titular.setRepApellido1(filtros.get(PARAM_REP_APELLIDO1));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_APELLIDO2))) {
      titular.setRepApellido2(filtros.get(PARAM_REP_APELLIDO2));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_NOMBRE))) {
      titular.setRepNombre(filtros.get(PARAM_REP_NOMBRE));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_TPIDENTI))) {
      titular.setRepTpidenti(filtros.get(PARAM_REP_TPIDENTI).charAt(0));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_TPIDENTI))) {
      titular.setTpidenti(filtros.get(PARAM_TPIDENTI).charAt(0));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_TPSOCIED))) {
      titular.setTpsocied(filtros.get(PARAM_TPSOCIED).charAt(0));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_REP_IDENTIFICACION))) {
      titular.setRepIdentificacion(filtros.get(PARAM_REP_IDENTIFICACION));
    }
  }

  private void updateOperacionH47(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "updateOperacionH47";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> resultadoModificacion = new HashMap<String, String>();
    String resultado = "";
    final Map<String, String> filtros = webRequest.getFilters();
    final Long idH47 = Long.parseLong(filtros.get(PARAM_ID_OPERACIONH47));
    OperacionH47 operacion = null;
    try {
      operacion = operacionBo.findById(idH47);
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo operacion con id " + idH47
                + ": " + e.getMessage());
    }
    if (operacion == null) {
      webResponse.setError(ERROR_OPERACION_NO_ENCONTRADA + idH47);
    } else {
      if (operacion.getEstadoEnvio() == null
          || (!EstadosEnumerados.H47.ENVIADA.getId().equals(operacion.getEstadoEnvio().getIdestado())
              && !EstadosEnumerados.H47.ACEPTADA_CNMV.getId().equals(operacion.getEstadoEnvio().getIdestado())
              && !EstadosEnumerados.H47.ACEPTADA_BME.getId().equals(operacion.getEstadoEnvio().getIdestado()) && !operacion.getEstOperacion()
                                                                                                                           .equals('B'))) {
        modifyOperacionFromFilters(filtros, operacion);
        try {
          operacionBo.save(operacion);
          resultado = "Operación modificada con éxito";
        } catch (Exception e) {
          webResponse.setError("WARNING guardando operacion: " + idH47 + " " + e.getMessage());
        }
      } else if (EstadosEnumerados.H47.ACEPTADA_CNMV.getId().equals(operacion.getEstadoEnvio().getIdestado())
                 || EstadosEnumerados.H47.ACEPTADA_BME.getId().equals(operacion.getEstadoEnvio().getIdestado())) {
        modifyOperacionAceptadaFromFilters(filtros, operacion);
        try {
          operacionBo.save(operacion);
          resultado = "Operación modificada con éxito";
        } catch (Exception e) {
          webResponse.setError("WARNING guardando operacion: " + idH47 + " " + e.getMessage());
        }
      } else {
        resultado = "No se puede modificar una operacion si su estado es ENVIADA o Baja";
      }
    }
    resultadoModificacion.put(RESULT_UPDATE_OPERACIONH47, resultado);
    webResponse.setResultados(resultadoModificacion);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  private void modifyOperacionAceptadaFromFilters(Map<String, String> filtros, OperacionH47 operacion) {
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ESTADO_ENVIO))
        && StringUtils.isNumeric(filtros.get(PARAM_ESTADO_ENVIO))) {
      operacion.setEstadoEnvio(estadosBo.findById(Integer.parseInt(filtros.get(PARAM_ESTADO_ENVIO))));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ESTADO_OPERACION))) {
      operacion.setEstOperacion(filtros.get(PARAM_ESTADO_OPERACION).charAt(0));
    }
  }

  private void modifyOperacionFromFilters(Map<String, String> filtros, OperacionH47 operacion) {
    final String NOMBRE_METODO = "modifyOperacionFromFilters";

    if (StringUtils.isNotEmpty(filtros.get(PARAM_CAPACIDAD_NEGOCIACION))) {
      operacion.setCapacidadNegociacion(filtros.get(PARAM_CAPACIDAD_NEGOCIACION).charAt(0));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ESTADO_ENVIO))
        && StringUtils.isNumeric(filtros.get(PARAM_ESTADO_ENVIO))) {
      operacion.setEstadoEnvio(estadosBo.findById(Integer.parseInt(filtros.get(PARAM_ESTADO_ENVIO))));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ESTADO_OPERACION))) {
      operacion.setEstOperacion(filtros.get(PARAM_ESTADO_OPERACION).charAt(0));
    }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_FEJECUCION ) ) ) {
    // operacion.setFhEjecucion( obtenerFecha( PARAM_FEJECUCION, filtros )
    // );
    // }
    // if ( StringUtils.isNotEmpty( filtros.get( PARAM_FLIQUIDACION ) ) ) {
    // operacion.setFhLiquidacion( obtenerFecha( PARAM_FLIQUIDACION, filtros
    // ) );
    // }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ISIN))) {
      operacion.setIdInstrumento(filtros.get(PARAM_ISIN));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_SENTIDO))) {
      operacion.setSentido(filtros.get(PARAM_SENTIDO).charAt(0));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_TITULAR_PRINCIPAL))) {
      operacion.setTitularPrincipal(filtros.get(PARAM_TITULAR_PRINCIPAL));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_TITULOS))) {
      try {
        BigDecimal titulos = new BigDecimal(filtros.get(PARAM_TITULOS));
        operacion.setTitulos(titulos);
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo títulos a número: "
                  + e.getMessage());
      }
    }

    if (StringUtils.isNotEmpty(filtros.get(PARAM_CANTIDAD_TOTAL))) {
      try {
        BigDecimal cantidadTotal = new BigDecimal(filtros.get(PARAM_CANTIDAD_TOTAL));
        operacion.setCantidadTotal(cantidadTotal);
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_CANTIDAD_TOTAL
                  + " a número: " + e.getMessage());
      }
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_EFECTIVO_TOTAL))) {
      try {
        BigDecimal efectivoTotal = new BigDecimal(filtros.get(PARAM_EFECTIVO_TOTAL));
        operacion.setEfectivoTotal(efectivoTotal);
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_EFECTIVO_TOTAL
                  + " a número: " + e.getMessage());
      }
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_FENEGOCIACION))) {
      operacion.setFhNegociacion(obtenerFecha(PARAM_FENEGOCIACION, filtros));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_FEVALOR))) {
      operacion.setFhValor(obtenerFecha(PARAM_FEVALOR, filtros));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_HNEGOCIACION))) {
      operacion.sethNegociacion(obtenerHora(PARAM_HNEGOCIACION, filtros));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ID_TRANSACCION))) {
      operacion.setIdentificadorTransaccion(filtros.get(PARAM_ID_TRANSACCION));
    }
    if (StringUtils.isNotEmpty(filtros.get(PARAM_PRECIO_UNITARIO))) {
      try {
        BigDecimal precioUnitario = new BigDecimal(filtros.get(PARAM_PRECIO_UNITARIO));
        operacion.setPrecioUnitario(precioUnitario);
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo " + PARAM_PRECIO_UNITARIO
                  + " a número: " + e.getMessage());
      }
    }

  }

  private void getTitularesH47(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "getTitularesH47";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, String> filtros = webRequest.getFilters();
    final Map<String, List<TitularesH47DTO>> resultadoTitulares = new HashMap<String, List<TitularesH47DTO>>();
    List<TitularesH47DTO> listaTitulares = new ArrayList<TitularesH47DTO>();

    final Long idH47 = Long.parseLong(filtros.get(PARAM_ID_OPERACIONH47));
    OperacionH47 operacion = null;
    try {
      operacion = operacionBo.findById(idH47);
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo operación por id " + idH47
                + ": " + e.getMessage());
    }
    if (operacion != null && operacion.getTitularesH47() != null) {
      for (TitularesH47 titular : operacion.getTitularesH47()) {
        listaTitulares.add(new TitularesH47DTO(titular));
      }
    } else {
      webResponse.setError(ERROR_OPERACION_NO_ENCONTRADA + idH47);
    }
    resultadoTitulares.put(RESULT_LIST_TITULARES_H47, listaTitulares);

    webResponse.setResultados(resultadoTitulares);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  private void getEstadosEnvio(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "getEstadosEnvio";
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
    final Map<String, List<Tmct0estadoDTO>> resultadoDetalle = new HashMap<String, List<Tmct0estadoDTO>>();
    List<Tmct0estadoDTO> listaEstados = new ArrayList<Tmct0estadoDTO>();
    try {
      listaEstados = estadosBo.findByIdtipoestadoOrderByNombreAsc(12);
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING consultando Tmct0estados: "
                + e.getMessage());
    }
    resultadoDetalle.put(RESULT_LIST_ESTADOS, listaEstados);
    webResponse.setResultados(resultadoDetalle);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
  }

  /**
   * 
   * @param webRequest
   * @param webResponse
   */
  private void convertALCsToH47(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[convertALCsToH47] Inicio ...");
    String errorMsg = new String();

    final Map<String, List<OperacionH47>> resultadoOperaciones = new HashMap<String, List<OperacionH47>>();

    List<String> listaCdCodigosValidos = new ArrayList<String>();

    Tmct0cfg cfg = cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION, TMCT0CFG_PROCESS, TMCT0CFG_KEYNAME);
    listaCdCodigosValidos = Arrays.asList(cfg.getKeyvalue().split(","));

    List<Map<String, String>> params = webRequest.getParams();
    String nudnicifBanco = obtenerNudnicifBanco();
    String nombreBanco = obtenerNombreBanco();
    Date horaNegociacionMaxima = obtenerHoraNegociacionMaxima();

    Tmct0alc tmct0alc;
    BigDecimal titulosAlc = BigDecimal.ZERO;
    BigDecimal efectivoAlc = BigDecimal.ZERO;
    List<OperacionH47> lasOperacionesCreadas = new ArrayList<OperacionH47>();

    for (Map<String, String> paramElement : params) {
      List<Tmct0alc> alcsList = new ArrayList<Tmct0alc>();

      final String nbooking = paramElement.get(PARAM_NBOOKING);
      final String nucnfclt = paramElement.get(PARAM_NUCNFCLT);
      final Short nucnfliq = Short.valueOf(paramElement.get(PARAM_NUCNFLIQ));
      final Boolean isCreation = Boolean.valueOf(paramElement.get("isCreation"));

      Long id = null;
      if (StringUtils.isNumeric(paramElement.get(PARAM_ID))) {
        id = Long.valueOf(paramElement.get(PARAM_ID));
      } // if

      if (id != null) {
      } else {
        // Asociado a alc
        // Porque devolver una lista si sólo puede haber un alc??
        alcsList = alcBo.findAlcsByIdViaSpecifications(null, nbooking, nucnfliq, nucnfclt);

        tmct0alc = alcsList.get(0);
        titulosAlc = BigDecimal.ZERO;
        efectivoAlc = BigDecimal.ZERO;

        if (afiBo.findActivosByAlcData(tmct0alc.getNuorden(), tmct0alc.getNbooking(), tmct0alc.getNucnfclt(),
                                       tmct0alc.getNucnfliq()).size() > 0) {

          List<Tmct0movimientoecc> movimientosEcc = alcBo.getMovimientosEcc(tmct0alc);
          LOG.debug("[convertALCsToH47] Obtenidos '" + movimientosEcc.size()
                    + "' movimientosEcc para el desglose del nbooking: " + tmct0alc.getNbooking() + "; nucnfclt: "
                    + tmct0alc.getNucnfclt() + "; nucnfliq:" + tmct0alc.getNucnfliq());

          for (Tmct0movimientoecc movimiento : movimientosEcc) {
            LOG.debug("[convertALCsToH47] Procesando movimientoecc: " + movimiento.getIdmovimiento());

            if (StringUtils.isNumeric(movimiento.getCdestadomov().trim())
                && Integer.parseInt(movimiento.getCdestadomov().trim()) == 9
                && listaCdCodigosValidos.contains(movimiento.getCuentaCompensacionDestino())) {
              LOG.debug("[convertALCsToH47] El movimientoecc cumple las condiciones para ser procesado");

              titulosAlc = titulosAlc.add(movimiento.getImtitulos());
              efectivoAlc = efectivoAlc.add(movimiento.getImefectivo());
            } else {
              LOG.debug("[convertALCsToH47] El movimientoecc NO cumple las condiciones para ser procesado");
            } // else
          } // for (Tmct0movimientoecc movimiento : movimientosEcc)

          OperacionH47 operacion = new OperacionH47(tmct0alc, nudnicifBanco, titulosAlc, efectivoAlc,
                                                    horaNegociacionMaxima);
          OperacionH47 operacionContraria = operacionBo.createOperacionContraria(operacion, nudnicifBanco);

          operacion.setFhNegociacion(new Date());
          operacion.setFhValor(new Date());
          operacionContraria.setFhNegociacion(new Date());
          operacionContraria.setFhValor(new Date());

          if (isCreation) {
            modifyOperacionFromFilters(paramElement, operacion);
            modifyOperacionFromFilters(paramElement, operacionContraria);
            operacion.setCapacidadNegociacion('A');
            operacionContraria.setCapacidadNegociacion('P');
            operacionContraria.setTitularPrincipal(nudnicifBanco);
          }

          lasOperacionesCreadas.add(operacion);
          lasOperacionesCreadas.add(operacionContraria);

          if (isCreation) {

            try {
              operacionBo.save(operacion);
              operacionBo.save(operacionContraria);
              SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
              String idEjecucion = sdf.format(operacion.getFhNegociacion()) + operacion.getId();
              operacion.setIdEjecucion(idEjecucion);
              operacionContraria.setIdEjecucion(idEjecucion);
              operacionBo.save(operacion);
              operacionBo.save(operacionContraria);
            } catch (Exception e) {
              LOG.debug("[convertALCsToH47] WARNING guardando operacion: " + e.getMessage());
            } // catch

            // Una vez guardada la operacion, guardamos los titulares desde sus afis
            List<Tmct0afi> listaAfis = afiBo.findActivosByAlcData(alcsList.get(0).getNuorden(), alcsList.get(0)
                                                                                                        .getNbooking(),
                                                                  alcsList.get(0).getNucnfclt(), alcsList.get(0)
                                                                                                         .getNucnfliq());
            Tmct0afi afiRepresentante = findRepresentante(listaAfis);
            for (Tmct0afi afi : listaAfis) {
              TitularesH47 titular = new TitularesH47(afi, afiRepresentante);
              titular.setOperacionH47(operacion);
              titularesBo.save(titular);
            }

            try {
              // Se asocia el titular a la operacion contraria
              TitularesH47 titularContrario = titularesBo.createTitularBanco(nudnicifBanco, nombreBanco);
              titularContrario.setOperacionH47(operacionContraria);
              titularesBo.save(titularContrario);
            } catch (Exception e) {
              LOG.debug("[convertALCsToH47] WARNING guardando titular: " + e.getMessage());
            }

          }

        } else {
          LOG.debug("[convertALCsToH47] La orden [booking: " + tmct0alc.getNbooking() + "; nucnfclt: "
                    + tmct0alc.getNucnfclt() + "; nucnfliq:" + tmct0alc.getNucnfliq() + "] no tiene titulares activos");
          errorMsg += "La orden [booking: " + tmct0alc.getNbooking() + "; nucnfclt: " + tmct0alc.getNucnfclt()
                      + "; nucnfliq:" + tmct0alc.getNucnfliq() + "] no tiene titulares activos";
        } // else
      } // else
    } // for (Map<String, String> paramElement : params)

    webResponse.setError(errorMsg);
    resultadoOperaciones.put(RESULT_CONVERT_ALCS, lasOperacionesCreadas);
    webResponse.setResultados(resultadoOperaciones);

    LOG.debug("convertALCsToH47] Fin ...");
  } // convertALCsToH47

  private Date obtenerHoraNegociacionMaxima() {
    final String NOMBRE_METODO = "obtenerHoraNegociacionMaxima";
    String horaMaxima = null;
    Date horaConvertida = new Date();
    try {
      horaMaxima = constantesBo.findByCampo(H47_CONSTANTES_NEGOCIACION).getValor();
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo horaMaxima de "
                + DBConstants.FALLIDOS.H47_CONSTANTES + ":" + e.getMessage());
    }
    try {
      horaConvertida = SIBBACPTICommons.convertStringToDate(horaMaxima, "HHmmss");
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING Convirtiendo horaMaxima de "
                + DBConstants.FALLIDOS.H47_CONSTANTES + ":" + e.getMessage());
    }
    return horaConvertida;
  }

  private String obtenerNombreBanco() {
    final String NOMBRE_METODO = "obtenerNombreBanco";
    String nombreBanco = "";
    try {
      nombreBanco = cfgBo.findByAplicationAndProcessAndKeyname("SBRMV", "SVB", "svb.name").getKeyvalue();
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo nudncifBanco de Tmct0cfg: "
                + e.getMessage());
    }
    return nombreBanco;
  }

  private String obtenerNudnicifBanco() {
    final String NOMBRE_METODO = "obtenerNudnicifBanco";
    String nudnicifBanco = "";
    try {
      nudnicifBanco = cfgBo.findByAplicationAndProcessAndKeyname("SBRMV", "SVB", "svb.cif").getKeyvalue();
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo nudncifBanco de Tmct0cfg: "
                + e.getMessage());
    }
    return nudnicifBanco;
  }

  private Tmct0afi findRepresentante(List<Tmct0afi> titulares) {
    for (Tmct0afi titular : titulares) {
      if ('R' == titular.getTptiprep()) {
        return titular;
      }
    }
    return null;
  }

  /**
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getAlcsFromBooking(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[getAlcsFromBooking] Inicio ...");

    final Map<String, String> filtros = webRequest.getFilters();
    final String nbooking = filtros.get(PARAM_NBOOKING);

    final Map<String, List<OperacionesH47DTO>> resultados = new HashMap<String, List<OperacionesH47DTO>>();

    List<Tmct0alc> listaAlcs = new ArrayList<Tmct0alc>();
    List<String> listaCdCodigosValidos = new ArrayList<String>();

    Tmct0cfg cfg = cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION, TMCT0CFG_PROCESS, TMCT0CFG_KEYNAME);
    listaCdCodigosValidos = Arrays.asList(cfg.getKeyvalue().split(","));

    String nudnicifBanco = cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION, "SVB", "svb.cif")
                                .getKeyvalue();

    try {
      listaAlcs = alcBo.findByNbookingAndCuentaCompensacionDestino(nbooking, listaCdCodigosValidos);
      LOG.debug("[getAlcsFromBooking] Obtenidos '" + listaAlcs.size() + "' desgloses para el nbooking: " + nbooking);
    } catch (Exception e) {
      LOG.debug("[getAlcsFromBooking] Incidencia obteniendo desgloses para el nbooking: '" + nbooking + "': "
                + e.getMessage());
    } // catch

    BigDecimal titulosAlc = BigDecimal.ZERO;
    BigDecimal efectivoAlc = BigDecimal.ZERO;

    String estado = null;
    String alias;
    Tmct0alo tmct0alo;

    String estadoMovimientosH47 = "Finalizado";

    try {
      if (listaAlcs.size() > 0) {
        List<OperacionesH47DTO> listaOperacionesAlc = new ArrayList<OperacionesH47DTO>();

        for (Tmct0alc alc : listaAlcs) {
          tmct0alo = alc.getTmct0alo();
          alias = tmct0alo.getTmct0bok().getCdalias();
          titulosAlc = BigDecimal.ZERO;
          efectivoAlc = BigDecimal.ZERO;
          estado = estadosBo.findById(alc.getCdestadoasig()).getNombre();

          List<Tmct0afi> titulares = afiBo.findActivosByAlcData(alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(),
                                                                alc.getNucnfliq());
          if (titulares.size() > 0 && isNotFromTheBank(titulares, nudnicifBanco)) {
            LOG.debug("[getAlcsFromBooking] Obtenidos '" + titulares.size()
                      + "' titulares asociados al desglose del booking: " + alc.getNbooking() + "; nucnfclt: "
                      + alc.getNucnfclt() + "; nucnfliq:" + alc.getNucnfliq());

            Tmct0afi titularPrincipal = titulares.get(0);
            for (Tmct0afi titular : titulares) {
              if ('T' == titular.getTptiprep()) {
                titularPrincipal = titular;
                break;
              } // if
            } // for
            LOG.debug("[getAlcsFromBooking] Su titular principal tiene nudnicif: " + titularPrincipal.getNudnicif()
                      + " y bic: " + titularPrincipal.getCdddebic());

            List<Tmct0movimientoecc> movimientosEcc = alcBo.getMovimientosEcc(alc);
            if (movimientosEcc.size() > 0) {
              LOG.debug("[getAlcsFromBooking] Obtenidos '" + movimientosEcc.size()
                        + "' movimientosEcc para el desglose del nbooking: " + alc.getNbooking() + "; nucnfclt: "
                        + alc.getNucnfclt() + "; nucnfliq:" + alc.getNucnfliq());

              for (Tmct0movimientoecc movimiento : movimientosEcc) {
                LOG.debug("[getAlcsFromBooking] Procesando movimientoecc: " + movimiento.getIdmovimiento());

                if (StringUtils.isNumeric(movimiento.getCdestadomov().trim())
                    && Integer.parseInt(movimiento.getCdestadomov().trim()) == 9
                    && listaCdCodigosValidos.contains(movimiento.getCuentaCompensacionDestino())) {
                  LOG.debug("[getAlcsFromBooking] El movimientoecc cumple las condiciones para ser procesado");

                  titulosAlc = titulosAlc.add(movimiento.getImtitulos());
                  efectivoAlc = efectivoAlc.add(movimiento.getImefectivo());
                } else {
                  LOG.debug("[getAlcsFromBooking] El movimientoecc NO cumple las condiciones para ser procesado");
                } // else
              } // for (Tmct0movimientoecc movimiento : movimientosEcc)

              OperacionesH47DTO operacionMovimiento = new OperacionesH47DTO(alc, tmct0alo, titularPrincipal,
                                                                            titulosAlc, efectivoAlc,
                                                                            estadoMovimientosH47, alias, estado);
              LOG.debug("[getAlcsFromBooking] Operacion H47 generada correctamente");

              listaOperacionesAlc.add(operacionMovimiento);
              LOG.debug("[getAlcsFromBooking] Operacion H47 incluida en la lista");
            } else {
              LOG.debug("[getAlcsFromBooking] No se han encontrado movimientosEcc para el desglose del nbooking: "
                        + alc.getNbooking() + "; nucnfclt: " + alc.getNucnfclt() + "; nucnfliq:" + alc.getNucnfliq());
            } // else
          } else {
            LOG.debug("[getAlcsFromBooking] No se han encontrado titulares asociados al desglose del booking: "
                      + alc.getNbooking() + "; nucnfclt: " + alc.getNucnfclt() + "; nucnfliq:" + alc.getNucnfliq());
          } // else
        } // for (Tmct0alc alc : listaAlcs)

        if (listaOperacionesAlc.size() > 0) {
          resultados.put(RESULT_LIST_ALCS, listaOperacionesAlc);
          webResponse.setResultados(resultados);
        } else {
          webResponse.setError("No se han encontrado operaciones para el booking solicitado. Compruebe si sus desgloses tienen titulares correctos y movimientos con cuenta de compensacion "
                               + listaCdCodigosValidos.toString() + " y estado finalizado");
        } // else
      } else {
        webResponse.setError("No se han encontrado desgloses en las cuentas de intermediario financiero para el booking solicitado");
      } // else
    } catch (Exception e) {
      LOG.debug("[getAlcsFromBooking] Incidencia durante el proceso", e);
      webResponse.setError("Ha ocurrido un error durante el proceso: " + e.getMessage());
    } // catch

    LOG.debug("[getAlcsFromBooking] Fin ...");
  } // getAlcsFromBooking

  private boolean isNotFromTheBank(List<Tmct0afi> titulares, String nudnicifBanco) {
    boolean notFromTheBank = true;
    for (Tmct0afi afi : titulares) {
      if (nudnicifBanco.equals(afi.getNudnicif().trim())) {
        notFromTheBank = false;
        break;
      }
    }
    return notFromTheBank;
  }

  private void getOperacionesH47(WebRequest webRequest, WebResponse webResponse) {
    final String NOMBRE_METODO = "getOperacionesH47";
    final Map<String, List<OperacionesH47DTO>> resultado = new HashMap<String, List<OperacionesH47DTO>>();
    Map<String, String> filtros = new HashMap<String, String>();
    if (webRequest.getFilters() != null) {
      filtros = webRequest.getFilters();
    }
    String isin = null;
    if (StringUtils.isNotEmpty(StringUtils.trimToEmpty(filtros.get(PARAM_ISIN)))) {
      isin = filtros.get(PARAM_ISIN);
    }
    Date fliquidacionDe = obtenerFecha(PARAM_FLIQUIDACION_DE, filtros);
    Date fliquidacionA = obtenerFecha(PARAM_FLIQUIDACION_A, filtros);
    Date fejecucionDe = obtenerFecha(PARAM_FEJECUCION_DE, filtros);
    Date fejecucionA = obtenerFecha(PARAM_FEJECUCION_A, filtros);
    Character estadoOperacion = null;
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ESTADO_OPERACION))) {
      estadoOperacion = filtros.get(PARAM_ESTADO_OPERACION).charAt(0);
    }
    // Integer estadoEnvio = null;
    String estadoEnvio = "";
    if (StringUtils.isNotEmpty(filtros.get(PARAM_ESTADO_ENVIO))) { // )
      // && StringUtils.isNumeric(filtros.get(PARAM_ESTADO_ENVIO))) {
      // estadoEnvio = Integer.valueOf(filtros.get(PARAM_ESTADO_ENVIO));
      estadoEnvio = filtros.get(PARAM_ESTADO_ENVIO);
    }

    List<OperacionesH47DTO> listaOperaciones = new ArrayList<OperacionesH47DTO>();
    List<OperacionH47> listaOperacionesEntidades = new ArrayList<OperacionH47>();
    try {
      listaOperacionesEntidades = operacionBo.findViaSpecifications(isin, fliquidacionDe, fliquidacionA, fejecucionDe,
                                                                    fejecucionA, estadoOperacion, estadoEnvio);
    } catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING obteniendo lista operaciones h47: "
                + e.getMessage());
    }
    for (OperacionH47 operacionH47 : listaOperacionesEntidades) {
      try {
        listaOperaciones.add(new OperacionesH47DTO(operacionH47));
      } catch (Exception e) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] WARNING convirtiendo a DTO operacion con id: "
                  + operacionH47.getId());
      }
    }
    resultado.put(RESULT_LIST_OPERACIONES_H47, listaOperaciones);

    webResponse.setResultados(resultado);
  }
  
  private void getParametrosEjecucion(WebResponse webResponse) {
	    final String NOMBRE_METODO = "getParametrosEjecucion";
	    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
	    final Map<String, String> resultado = new HashMap<String, String>();
	    resultado.put("crearMTF", crearMTF );
	    resultado.put("crearFallidos", crearFallidos );
	    resultado.put("crearOperacionesTR", crearTr );
	    
	    webResponse.setResultados(resultado);
	    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");
	  }  

  private void getResultadosEjecuciones(WebRequest webRequest, WebResponse webResponse) {
	    final String NOMBRE_METODO = "getResultadosEjecuciones";
	    final Map<String,  List<ResultadoEjecucionDTO>> resultado = new HashMap<String,  List<ResultadoEjecucionDTO>>();
	    List<ResultadoEjecucionDTO> listaResultadosDto = null;
	    
	    Map<String, String> filtros = new HashMap<String, String>();
	    if (webRequest.getFilters() != null) {
	      filtros = webRequest.getFilters();
	    }
	    
	    String sFechaDesde = filtros.get("fejecucionDesde");
	    String sFechaHasta = filtros.get("fejecucionHasta");
	    
	    Date dFechaDesde = DateHelper.convertStringToDate(sFechaDesde, "dd/MM/yyyy");
	    
	    Date dFechaHasta = new Date();
	    
	    if (sFechaHasta != "" ){
	      dFechaHasta =  DateHelper.convertStringToDate(sFechaHasta, "dd/MM/yyyy");
	      dFechaHasta = DateHelper.getEndOfDay(dFechaHasta);
	    }
	    
	    try {
		    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Init");
		    listaResultadosDto = tmct0CnmvLogDao.findByFechasEjecucion(dFechaDesde, dFechaHasta);
		    resultado.put("listaResultados", listaResultadosDto);
		    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] Fin");			
		} catch (Exception e) {
			 LOG.error("[" + NOMBRE_SERVICIO + " :: " + NOMBRE_METODO + "] getResultadosEjecuciones: "
		                + e.getMessage());
		}

    webResponse.setResultados(resultado);  
	    
	    
}    
  

  
  
    
  
  public Date obtenerFecha(String param, final Map<String, String> filtros) {
    Date returnedDate = null;
    final String strFcontratacionDe = StringUtils.trimToEmpty(filtros.get(param));
    if (StringUtils.isNotEmpty(strFcontratacionDe)) {
      returnedDate = SIBBACPTICommons.convertStringToDate(filtros.get(param), "yyyyMMdd");
    }
    return returnedDate;
  }

  public Date obtenerHora(String param, final Map<String, String> filtros) {
    Date returnedDate = null;
    final String strFcontratacionDe = StringUtils.trimToEmpty(filtros.get(param));
    if (StringUtils.isNotEmpty(strFcontratacionDe)) {
      returnedDate = SIBBACPTICommons.convertStringToDate(filtros.get(param), "HHmmss");
    }
    return returnedDate;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_LIST_OPERACIONES_H47);
    commands.add(CMD_LIST_TITULARES_H47);
    commands.add(CMD_LIST_ALCS_FROM_BOOKING);
    commands.add(CMD_CONVERT_ALCS);
    commands.add(CMD_UPDATE_OPERACIONES_H47);
    commands.add(CMD_UPDATE_TITULARES_H47);
    commands.add(CMD_LIST_ESTADOS_ENVIO);

    return commands;
  } // getAvailableCommands

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  @Override
  public List<String> getFields() {
    return null;
  } // getFields
  
} // SIBBACServiceReportesCNMV
