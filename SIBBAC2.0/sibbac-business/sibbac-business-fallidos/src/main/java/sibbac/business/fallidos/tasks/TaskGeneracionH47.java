package sibbac.business.fallidos.tasks;

import java.util.concurrent.TimeUnit;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.writers.H47Writer;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Contiene la lógica de inicialización de la tarea de generación del fichero H47..
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author XI316153
 */
@SIBBACJob(group = Task.GROUP_FALLIDOS.NAME,
           name = Task.GROUP_FALLIDOS.JOB_H47,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "12:00:00")
public class TaskGeneracionH47 extends FallidosTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskGeneracionH47.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto que escribe el fichero. */
  @Autowired
  private H47Writer writer;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.business.fallidos.tasks.FallidosTaskConcurrencyPrevent#executeTask()
   */
  @Override
  public void executeTask() throws Exception {
    final Long startTime = System.currentTimeMillis();
    LOG.debug("[executeTask] Iniciando la tarea de generacion del fichero H47 ...");

    try {
      if (writer.writeIntoFile()) {
        writer.renameTempFile();
      }
    } catch (Exception e) {
      LOG.warn("[executeTask] Incidencia generando el fichero H47 ... " + e.getMessage(), e);
      throw new Exception(e.getMessage(), e);
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[executeTask] Finalizada la tarea de generacion del fichero H47 ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally
  } // executeTask

  /*
   * @see sibbac.business.fallidos.tasks.FallidosTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.FALLIDO_GENERACIONH47;
  } // determinarTipoApunte

} // TaskGeneracionH47
