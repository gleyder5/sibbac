package sibbac.business.fallidos.utils;


public enum SentidoEcc {

	COMPRA( "1", "E", "C" ), VENTA( "2", "S", "V" );

	private String	sentidoEcc;
	private String	sentidoMovimiento;
	private String	sentidoSibbac;

	private SentidoEcc( String sentidoEcc, String sentidoMovimiento, String sentidoSibbac ) {
		this.sentidoMovimiento = sentidoMovimiento;
		this.sentidoSibbac = sentidoSibbac;
		this.sentidoEcc = sentidoEcc;
	}

	public String getSentidoEcc() {
		return sentidoEcc;
	}

	public String getSentidoMovimiento() {
		return sentidoMovimiento;
	}

	public String getSentidoSibbac() {
		return sentidoSibbac;
	}

	public void setSentidoEcc( String sentidoEcc ) {
		this.sentidoEcc = sentidoEcc;
	}

	public void setSentidoMovimiento( String sentidoMovimiento ) {
		this.sentidoMovimiento = sentidoMovimiento;
	}

	public void setSentidoSibbac( String sentidoSibbac ) {
		this.sentidoSibbac = sentidoSibbac;
	}
}
