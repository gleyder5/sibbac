package sibbac.business.fallidos.database.bo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.Tmct0CnmvDesglosesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.dto.DesgloseMtfDto;
import sibbac.business.fallidos.dto.OperacionTrDTO;
import sibbac.business.fallidos.threads.ProcesaOperacionesTrDiaRunnable;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * Clase de negocios para generar operaciones de RT para MIFID II
 */
@Service
public class OperacionesTrBo {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(OperacionesTrBo.class);

  private Date ahora;
  private String fechaFormateada;
  private String horaFormateada;

  /** Log de la ejecucion */
  public Tmct0CnmvLog tmct0CnmvLog;

  /**
   * @return the tmct0CnmvLog
   */
  public Tmct0CnmvLog getTmct0CnmvLog() {
    return tmct0CnmvLog;
  }

  /**
   * @param tmct0CnmvLog the tmct0CnmvLog to set
   */
  public void setTmct0CnmvLog(Tmct0CnmvLog tmct0CnmvLog) {
    this.tmct0CnmvLog = tmct0CnmvLog;
  }

  /** Descripcion de todos los errores */
  private StringBuilder sbComentario = new StringBuilder();

  @Value("${valid.ccp.mtf.creation}")
  private String sCamarasCompensacion;

  @Value("${identif_sv_mtf_creation}")
  private String sIdentifSv;

  @Value("${cdestadoenvio.tr.creation}")
  private int iCdEstadoEnvio;

  // Para TR
  @Value("${days.tr.creation}")
  private int iDiasEjecucionTR;

  @Value("${first.day.tr.creation:}")
  private String primerDiaCreacionTr;

  /** Carpeta donde almacenar el fichero generado de operaciones temporal. */
  @Value("${nombre_operaciones_tmp.tr.sending}")
  private String sOperacionesFileNameTmp;

  /** Carpeta donde almacenar el fichero generado de operaciones temporal. */
  @Value("${nombre_titulares_tmp.tr.sending}")
  private String sTitularesFileNameTmp;

  @Autowired
  Tmct0CnmvDesglosesDao tmct0CnmvDesglosesDao;

  @Autowired
  Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;

  @Autowired
  Tmct0estadoDao tmct0estadoDao;

  @Autowired
  ProcesaOperacionTrBo procesaOperacionH47MtfBo;

  @Autowired
  Tmct0CnmvLogDao tmct0CnmvLogDao;

  @Autowired
  WriterOperacionesTrBo writerOperacionesTrBo;

  /**
   * Genera las operaciones RT de forma automatica y por dias mediante hilos de
   * ejecucion Metodo al que se llamara desde la pantalla y desde la task * para
   * las RT
   *
   * @throws Exception
   */
  public void generaOperacionesRT(boolean isScriptDividend) throws Exception {

    try {

      // Inicializaciones
      SimpleDateFormat formatoFecha2 = new SimpleDateFormat("yyyy-MM-dd");
      tmct0CnmvLog = new Tmct0CnmvLog();
      tmct0CnmvLog.setAuditUser("SISTEMA");
      tmct0CnmvLog.setAuditDate(new Date());
      tmct0CnmvLog.setProceso(Constantes.PROCESO_RT);
      tmct0CnmvLog.setContadorAltas(BigInteger.ZERO);
      tmct0CnmvLog.setContadorBajas(BigInteger.ZERO);
      tmct0CnmvLog.setIncidencia('N');

      // Se obtienen los dias de ejecución:
      // Se obtienen a partir del parametro days.mtf.creation como la resta del
      // dia actual menos el numero de dias del parametro.
      Date dFechaPrimeraEjecucion = null;
      if (StringUtils.isNotBlank(primerDiaCreacionTr)) {
        dFechaPrimeraEjecucion = formatoFecha2.parse(primerDiaCreacionTr);
      }
      Date dFechaFin = new Date();
      Date dFechaInicio = DateHelper.restaDias(dFechaFin, iDiasEjecucionTR);
      if (dFechaPrimeraEjecucion != null) {
        BigDecimal finicioBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
            .convertDateToString(dFechaInicio));
        BigDecimal fPrimeraEjecucionBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
            .convertDateToString(dFechaPrimeraEjecucion));

        if (fPrimeraEjecucionBd.compareTo(finicioBd) > 0) {
          dFechaInicio = dFechaPrimeraEjecucion;
        }
      }

      List<Date> lDiasEjecucion = DateHelper.getListaFechas(dFechaInicio, dFechaFin);

      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat("Thread-GeneracionTr-%d");
      ExecutorService executor = Executors.newFixedThreadPool(Math.min(lDiasEjecucion.size(), 15), tfb.build());
      // Se ejecuta un hilo con las operaciones de cada dia.
      for (int i = 0; i < lDiasEjecucion.size(); i++) {
        ProcesaOperacionesTrDiaRunnable runnable = new ProcesaOperacionesTrDiaRunnable(this, lDiasEjecucion.get(i),
            isScriptDividend);
        executor.execute(runnable);
      }

      // Se eliminan los hilos.
      try {
        executor.shutdown();
        executor.awaitTermination((long) 24, TimeUnit.HOURS);
      }
      catch (InterruptedException e) {
        LOG.error("generaOperacionesRT - Error de la ejecucion de los hilos  " + e.getMessage());
        sbComentario.append("  Error en la ejecucion de los hilos. Proceso Abortado - Contacte con IT : Error:  ")
            .append(e.getMessage());
      }

      // Se escriben los ficheros con las operaciones pendientes
      Boolean bEscribir = writerOperacionesTrBo.escribirFicheroOperacionesTrMultiThread(isScriptDividend);
      // Boolean bEscribir =
      // writerOperacionesTrBo.escribirFicheroOperacionesTr();

      if (!bEscribir) {
        sbComentario.append(" - No se han generado los ficheros ");
      }

      // Cuando se finaliza la ejecucion de todos los hilos se registra en el
      // log la ejecucion
      if (sbComentario.length() > 0) {
        tmct0CnmvLog.setIncidencia('S');
        if (sbComentario.length() > 1000) {
          tmct0CnmvLog.setComentario(sbComentario.toString().substring(0, 999));
        }
        else {
          tmct0CnmvLog.setComentario(sbComentario.toString());
        }

      }
      else {
        tmct0CnmvLog.setIncidencia('N');
      }
      this.tmct0CnmvLogDao.save(tmct0CnmvLog);

    }
    catch (Exception e) {
      LOG.error("generaOperacionesRT - Se produjo un error  " + e.getMessage());
      sbComentario.append("  Error en la ejecucion generaOperacionesRT:  Proceso Abortado - Contacte con IT : Error:")
          .append(e.getMessage());
      tmct0CnmvLog.setIncidencia('S');
      tmct0CnmvLog.setComentario(sbComentario.toString().substring(0, 1000));
      this.tmct0CnmvLogDao.save(tmct0CnmvLog);
      throw e;
    }
  }

  /**
   * Genera las operaciones RT del dia recibido por parametro
   *
   *
   * @throws Exception
   */
  public void generaOperacionesDiaRT(Date fechaEjecucion, boolean isScriptDividend) {
    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
    String linea;
    OperacionTrDTO operacionRtDTO;

    try {
      LOG.debug("Inicio generaOperacionesDiaRT para el dia " + formatoFecha.format(fechaEjecucion).toString());

      // Se obtiene el estado PTE de enviar
      // Tmct0estado estadoPte =
      // tmct0estadoDao.findOne(EstadosEnumerados.H47.PTE_ENVIAR.getId());

      // Se obtiene la lista de desgloses a procesar para ese dia

      Path tempFile = this.recuperarInformacionTratarTr(fechaEjecucion, isScriptDividend);
      if (tempFile != null && Files.exists(tempFile) && Files.size(tempFile) > 0) {
        ObjectMapper jsonMapper = new ObjectMapper();
        try (InputStream is = Files.newInputStream(tempFile, StandardOpenOption.READ);
            GZIPInputStream gis = new GZIPInputStream(is);
            BufferedReader br = new BufferedReader(new InputStreamReader(gis, StandardCharsets.ISO_8859_1));) {
          while ((linea = br.readLine()) != null) {
            operacionRtDTO = jsonMapper.readValue(linea, OperacionTrDTO.class);
            try {

              procesaOperacionH47MtfBo.trataOperacionRtDTO(operacionRtDTO);
            }
            catch (SIBBACBusinessException sb) {
              String sError = " - ERROR: al procesar los datos del desglose: " + " Nuorden - "
                  + operacionRtDTO.getNuorden().trim() + " - Nbooking - " + operacionRtDTO.getNbooking().trim()
                  + " - Nucnfclt - " + operacionRtDTO.getNucnfclt().trim() + " - Nucnfliq -  "
                  + operacionRtDTO.getNucnfliq() + " - Iddesglosecamara  " + operacionRtDTO.getIddesglosecamara()
                  + " - Ejecucion  " + operacionRtDTO.getNurefexemkt().trim() + " " + sb.getMessage();
              LOG.error(sError);
              sbComentario.append(sError);
            }

            catch (Exception e) {
              String sError = " - ERROR: Excepcion no controlada al procesar los datos del desglose: " + " Nuorden - "
                  + operacionRtDTO.getNuorden() + " - Nbooking - " + operacionRtDTO.getNbooking() + " - Nucnfclt - "
                  + operacionRtDTO.getNucnfclt() + " - Nucnfliq -  " + operacionRtDTO.getNucnfliq()
                  + " - Iddesglosecamara  " + operacionRtDTO.getIddesglosecamara() + " - Ejecucion  "
                  + operacionRtDTO.getNurefexemkt() + " " + e.getMessage();
              LOG.error(sError);
              sbComentario.append(sError);

            }
          }
        }
        catch (Exception e) {
          String sError = " Error  generaOperacionesDiaRT para el dia "
              + formatoFecha.format(fechaEjecucion).toString()
              + " Proceso Abortado para los datos de este día: - Error  " + e.getMessage();
          LOG.error(sError);
          sbComentario.append(sError);
        }
        finally {
          Files.deleteIfExists(tempFile);
        }
      }

      LOG.debug("Fin generaOperacionesDiaRT para el dia " + formatoFecha.format(fechaEjecucion).toString());
    }
    catch (Exception e) {
      String sError = " Error  generaOperacionesDiaRT para el dia " + formatoFecha.format(fechaEjecucion).toString()
          + " Proceso Abortado para los datos de este día: - Error  " + e.getMessage();
      LOG.error(sError);
      sbComentario.append(sError);
    }
  }

  private Path recuperarInformacionTratarTr(Date fechaEjecucion, boolean isScriptDividend) throws IOException {
    int countRegs;
    String line;
    boolean conDatos = false;
    Pageable page = new PageRequest(0, 10000);
    ObjectMapper jsonMapper = new ObjectMapper();

    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyyMMdd");

    char isscrdiv = isScriptDividend ? 'S' : 'N';
    // Se obtiene la lista de desgloses a procesar para ese dia
    List<OperacionTrDTO> lDesgloses2;
    Path tempFile = Files.createTempFile(
        String.format("TrInsertTablasQueryDayJson_%s_", formatoFecha.format(fechaEjecucion)), ".txt.gz");
    try (OutputStream os = Files.newOutputStream(tempFile, StandardOpenOption.WRITE);
        OutputStream gos = new GZIPOutputStream(os);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(gos, StandardCharsets.ISO_8859_1))) {
      do {
        lDesgloses2 = tmct0CnmvDesglosesDao.findDesglosesProcesoAutomaticoRtDtos(fechaEjecucion, isscrdiv, page);
        countRegs = lDesgloses2.size();
        for (OperacionTrDTO reg : lDesgloses2) {
          line = jsonMapper.writeValueAsString(reg);
          bw.write(line);
          bw.newLine();
          conDatos = true;
        }
        page = page.next();
        lDesgloses2.clear();
      }
      while (countRegs == page.getPageSize());
    }
    if (!conDatos) {
      Files.deleteIfExists(tempFile);
      return null;
    }
    else {
      return tempFile;
    }
  }

  /**
   * Quita de la lista de desgloses los titulares que no se van a enviar por
   * superar el numero maximo
   * 
   * @param List<DesgloseMtfDto> lista de los desgloses que supera el maximo
   * @param iNumTitulares Numero de titulares que se pueden enviar
   * @return List<DesgloseMtfDto> lista de los desgloses reducida al numero de
   * desgloses que se pueden enviar
   */
  public List<DesgloseMtfDto> obtenerTitularesEnvio(List<DesgloseMtfDto> listaDesgloses, int iNumTitulares) {

    List<DesgloseMtfDto> listaDesglosesReducida = new ArrayList<DesgloseMtfDto>();

    // Se crea una nueva lista con todos los desglose pero sin titulares
    for (DesgloseMtfDto desgloseMtfDto : listaDesgloses) {
      DesgloseMtfDto desgloseMtfDtoNuevo = new DesgloseMtfDto();
      desgloseMtfDtoNuevo.setbTitulosPropios(desgloseMtfDto.getbTitulosPropios());
      desgloseMtfDtoNuevo.setDesglose(desgloseMtfDto.getDesglose());
      desgloseMtfDtoNuevo.setListaRepresentantes(desgloseMtfDto.getListaRepresentantes());
      desgloseMtfDtoNuevo.setListaTitulares(new ArrayList<Tmct0CnmvTitulares>());
      listaDesglosesReducida.add(desgloseMtfDtoNuevo);
    }

    // Se recorren los titulares de los desgloses antiguos y solo se insertan
    // los primeros de cada desglose hasta completar el numero máximo
    int iContador = 0;

    int iNumTitularesActuales = 0;
    while (iNumTitularesActuales < iNumTitulares) {
      int iDesglose = 0;
      for (DesgloseMtfDto desgloseMtfDto : listaDesgloses) {
        if (desgloseMtfDto.getListaTitulares().size() > iContador && iNumTitularesActuales < iNumTitulares) {
          listaDesglosesReducida.get(iDesglose).getListaTitulares()
              .add(desgloseMtfDto.getListaTitulares().get(iContador));
          iNumTitularesActuales++;
        }
        iDesglose++;

      }
      iContador++;
    }

    return listaDesglosesReducida;

  }

  /**
   * 
   * @return Estado Envio según parametro cdestadoenvio.tr.creation del fichero
   * de configuración.
   * */
  public int obtenerEstadoEnvio() {

    return iCdEstadoEnvio;
  }

  public String componerNombreFichero(String sFichero) {

    // poner File.separator en el lastIndexOf, de momento dejo '\'
    String sNombreSeparado = sFichero.substring(sFichero.lastIndexOf("/") + 1);
    String[] sNombreSinExtension = sNombreSeparado.split("\\.");
    String nombreFicheroAcual = sNombreSinExtension[0];
    File titularesFile = new File(sTitularesFileNameTmp);
    String nombreFinalAux = "";

    if (ahora == null) {
      ahora = new Date();
      fechaFormateada = FormatDataUtils.convertDateToString(ahora, FormatDataUtils.DATE_FORMAT);
      horaFormateada = FormatDataUtils.convertDateToString(ahora, FormatDataUtils.TIME_FORMAT);
    }
    else {

      if (titularesFile.exists()) {
        nombreFinalAux = nombreFicheroAcual.concat("_").concat(fechaFormateada).concat("_").concat(horaFormateada)
            .concat(Constantes.EXTENSION_FICHEROS_MIFIDII);
        nombreFinalAux = sFichero.replace(sNombreSeparado, nombreFinalAux);
        ahora = null;
        fechaFormateada = "";
        horaFormateada = "";
      }
    }

    if (!nombreFinalAux.equals("")) {
      return nombreFinalAux;
    }

    String nombreFinal = nombreFicheroAcual.concat("_").concat(fechaFormateada).concat("_").concat(horaFormateada)
        .concat(Constantes.EXTENSION_FICHEROS_MIFIDII);

    nombreFinal = sFichero.replace(sNombreSeparado, nombreFinal);

    return nombreFinal;
  }

  /**
   * Metodo que después de haber generado los ficheros de MiFIDII, actualiza los
   * estados de las ejecuciones tratadas al estado "ENVIADA BME"
   * 
   * @param listaOperaciones
   * 
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void actualizarEstadoEnvios(List<Tmct0CnmvEjecuciones> listOperacionesTr,
      List<Tmct0CnmvDesgloses> listDesglosesTr) {

    try {

      // De momento estoy reutilizando el estado del H47 para buscar el estado
      // "ENVIADA BME".
      Tmct0estado estadoEnviada = tmct0estadoDao.findOne(EstadosEnumerados.H47.ENVIADA.getId());

      // Se actualizan las ejecuciones escritas en los ficheros.
      for (Tmct0CnmvEjecuciones ejecucion : listOperacionesTr) {

        // if(listDesglosesDto.size() == 1) {
        switch (ejecucion.getEstadoOperacion()) {
          case 'A':
            if (ejecucion.getfEnvioAlta() == null) {
              ejecucion.setfEnvioAlta(new Date());
            }

            break;
          case 'B':
            if (ejecucion.getfEnvioBaja() == null) {
              ejecucion.setfEnvioBaja(new Date());
            }

          default:
            break;
        }
        ejecucion.setCdEstadoEnvio(estadoEnviada);

        // }
      }

      // Se actualizan los desgloses escritos en los ficheros.
      for (Tmct0CnmvDesgloses desglose : listDesglosesTr) {

        // if(listDesglosesDto.size() == 1) {
        switch (desglose.getEstadoDesglose()) {
          case 'A':
            desglose.setfEnvioAlta(new Date());
            break;
          case 'B':
            desglose.setfEnvioBaja(new Date());
          default:
            break;
        }

        desglose.setCdEstadoEnvio(estadoEnviada.getIdestado());

        // }
      }
      tmct0CnmvEjecucionesDao.save(listOperacionesTr);
      tmct0CnmvDesglosesDao.save(listDesglosesTr);

    }
    catch (Exception e) {
      LOG.error(" actualizaEjecuciones  - Error: Excepcion no controlada  " + e.getMessage());
    }

  }

  public void guardarLog(Tmct0CnmvLog logProceso, StringBuilder sbcomentario) {

    try {

      // Actualizamos la fecha de grabación del registro
      logProceso.setAuditDate(new Date());
      // Cuando se finaliza la ejecucion se genera el log
      if (sbcomentario.length() > 0) {
        logProceso.setIncidencia('S');
        if (sbcomentario.length() > 1000) {
          logProceso.setComentario(sbcomentario.toString().substring(0, 999));
        }
        else {
          logProceso.setComentario(sbcomentario.toString());
        }
      }
      else {
        logProceso.setIncidencia('N');
      }

      tmct0CnmvLogDao.save(logProceso);
    }
    catch (Exception e) {
      LOG.error(" actualizaEjecuciones  - Error: Excepcion no controlada  " + e.getMessage());
    }

  }

}
