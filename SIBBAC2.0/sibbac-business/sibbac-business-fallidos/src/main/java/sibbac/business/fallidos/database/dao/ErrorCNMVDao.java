package sibbac.business.fallidos.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.ErrorCNMV;


@Repository
public interface ErrorCNMVDao extends JpaRepository< ErrorCNMV, Long > {

	ErrorCNMV findFirstByCodigo( String codigo );

}
