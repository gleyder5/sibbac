package sibbac.business.fallidos.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.dto.IsinDTO;
import sibbac.business.wrappers.database.dao.Tmct0ValoresDao;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * @author XI316153
 */
@SIBBACService
public class SIBBACServiceIsin implements SIBBACServiceBean {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Referencia para la inserción de logs. */
	protected static final Logger	LOG			= LoggerFactory.getLogger( SIBBACServiceIsin.class );

	/** Comando para el listado de los Isin. */
	private static final String		CMD_LIST	= "listIsin";

	/** Contenido de la respuesta para el comando CMD_LIST. */
	private static final String		RESULT_LIST	= "listadoIsin";

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	@Autowired
	private Tmct0ValoresDao			tmct0ValoresDao;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public SIBBACServiceIsin() {
	} // SIBBACServiceIsin

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.debug( "[SIBBACServiceIsin :: process] Processing a web request ..." );

		final String command = webRequest.getAction();
		LOG.debug( "[SIBBACServiceIsin :: process] Command: [{}]", command );

		final WebResponse webResponse = new WebResponse();

		List< Tmct0Valores > listaValores = null;
		List< IsinDTO > listaIsin = null;

		switch ( command ) {
			case CMD_LIST:
				listaValores = tmct0ValoresDao.findAll();

				final Map< String, List< IsinDTO >> valores = new HashMap< String, List< IsinDTO >>();
				listaIsin = new ArrayList< IsinDTO >();

				for ( Tmct0Valores valor : listaValores ) {
					listaIsin.add( new IsinDTO( valor ) );
				} // for

				valores.put( RESULT_LIST, listaIsin );

				webResponse.setResultados( valores );

				break;
			default:
				webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
		} // switch
		return webResponse;
	} // process

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LIST );
		return commands;
	} // getAvailableCommands

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	} // getFields

} // SIBBACServiceIsin
