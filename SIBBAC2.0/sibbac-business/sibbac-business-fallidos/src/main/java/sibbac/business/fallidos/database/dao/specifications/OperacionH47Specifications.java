package sibbac.business.fallidos.database.dao.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fase0.database.model.Tmct0estado;


public class OperacionH47Specifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( OperacionH47Specifications.class );

	public static Specification< OperacionH47 > isin( final String isin ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate isinPredicate = null;
				LOG.trace( "[OperacionH47Specifications::isin] [isin=={}]", isin );
				if ( isin != null ) {
				    if(isin.indexOf("#") != -1 ){
						isinPredicate = builder.notEqual( root.< String > get( "idInstrumento" ), isin.replace("#","") );
				    }else
				    	isinPredicate = builder.equal( root.< String > get( "idInstrumento" ), isin );
				}
				return isinPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > estadoOperacion( final Character estadoOperacion ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate estadoOperacionPredicate = null;
				LOG.trace( "[OperacionH47Specifications::estadoOperacion] [estadoOperacion=={}]", estadoOperacion );
				if ( estadoOperacion != null ) {
					estadoOperacionPredicate = builder.equal( root.< Character > get( "estOperacion" ), estadoOperacion );
				}
				return estadoOperacionPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > estadoEnvio( final String estadoEnvioS ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate estadoEnvioPredicate = null;
				LOG.trace( "[OperacionH47Specifications::estadoEnvio] [estadoEnvio=={}]", estadoEnvioS );
				Integer estadoEnvio = null;
				if ( estadoEnvioS != null && !estadoEnvioS.equals("") ) {
					if(estadoEnvioS.indexOf("#") != -1){
					    estadoEnvio = Integer.valueOf(estadoEnvioS.replace("#", ""));
						estadoEnvioPredicate = builder.notEqual( root.< Tmct0estado > get( "estadoEnvio" ).<Integer>get("idestado"), estadoEnvio );
					}else{	
					    estadoEnvio = Integer.valueOf(estadoEnvioS);
						estadoEnvioPredicate = builder.equal( root.< Tmct0estado > get( "estadoEnvio" ).<Integer>get("idestado"), estadoEnvio );
					}
				}
				return estadoEnvioPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > fhLiquidacionDe( final Date fecha ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacionH47Specifications::fhLiquidacionDesde] [fhValor>={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fhValor" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > fhLiquidacionA( final Date fecha ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacionH47Specifications::fhLiquidacionDesde] [fhValor<={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fhValor" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > fhEjecucionDe( final Date fecha ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacionH47Specifications::fhEjecucionDesde] [fhNegociacion>={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fhNegociacion" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > fhEjecucionA( final Date fecha ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacionH47Specifications::fhEjecucionDesde] [fhNegociacion<={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fhNegociacion" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< OperacionH47 > listadoFriltrado( final String isin, final Date fhLiquidacionDe, final Date fhLiquidacionA,
			final Date fhEjecucionDe, final Date fhEjecucionA, final Character estadoOperacion, final String estadoEnvio ) {
		return new Specification< OperacionH47 >() {

			public Predicate toPredicate( Root< OperacionH47 > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< OperacionH47 > specs = null;

				if ( isin != null ) {
					if ( specs == null ) {
						specs = Specifications.where( isin( isin ) );
					}
				}

				if ( estadoOperacion != null ) {
					if ( specs == null ) {
						specs = Specifications.where( estadoOperacion( estadoOperacion ) );
					} else {
						specs = specs.and( estadoOperacion( estadoOperacion ) );
					}

				}

				if ( estadoEnvio != null ) {
					if ( specs == null ) {
						specs = Specifications.where( estadoEnvio( estadoEnvio ) );
					} else {
						specs = specs.and( estadoEnvio( estadoEnvio ) );
					}

				}
				
				if ( fhLiquidacionDe != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fhLiquidacionDe( fhLiquidacionDe ) );
					} else {
						specs = specs.and( fhLiquidacionDe( fhLiquidacionDe ) );
					}

				}

				if ( fhLiquidacionA != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fhLiquidacionA( fhLiquidacionA ) );
					} else {
						specs = specs.and( fhLiquidacionA( fhLiquidacionA ) );
					}

				}

				if ( fhEjecucionDe != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fhEjecucionDe( fhEjecucionDe ) );
					} else {
						specs = specs.and( fhEjecucionDe( fhEjecucionDe ) );
					}

				}

				if ( fhEjecucionA != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fhEjecucionA( fhEjecucionA ) );
					} else {
						specs = specs.and( fhEjecucionA( fhEjecucionA ) );
					}

				}

				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}
}
