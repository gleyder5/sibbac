package sibbac.business.fallidos.dto;


import java.io.Serializable;

import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;


/**
 * @version 1.0
 * @author XI316153
 */
public class CamaraDTO implements Serializable {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Identificador para la serialización de objetos. */
	private static final long	serialVersionUID	= -6932036951899548748L;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	/** Identificador. */
	private Long				id;

	/** Código. */
	private String				codigo;

	/** Descripción. */
	private String				descripcion;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public CamaraDTO() {
	} // CamaraDTO

	/**
	 * Constructor II. Crea un objeto a partir de los datos del objeto <code>Tmct0CamaraCompensacion</code> recibido.
	 */
	public CamaraDTO( Tmct0CamaraCompensacion camara ) {
		this.setId( camara.getIdCamaraCompensacion() );
		this.setCodigo( camara.getCdCodigo() );
		this.setDescripcion( camara.getNbDescripcion() );
	} // CamaraDTO

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CamaraDTO [id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion + "]";
	} // toString

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

	/**
	 * @param id the id to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

} // CamaraDTO
