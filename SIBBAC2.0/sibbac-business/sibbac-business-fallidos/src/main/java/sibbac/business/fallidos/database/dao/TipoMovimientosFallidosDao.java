package sibbac.business.fallidos.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.TipoMovimientosFallidos;


@Repository
public interface TipoMovimientosFallidosDao extends JpaRepository< TipoMovimientosFallidos, Long > {

} // TipoMovimientosFallidosDao
