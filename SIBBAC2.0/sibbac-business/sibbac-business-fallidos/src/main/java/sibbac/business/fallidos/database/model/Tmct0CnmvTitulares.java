package sibbac.business.fallidos.database.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_CNMV_TITULARES
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CNMV_TITULARES")
public class Tmct0CnmvTitulares {

	private static final Logger LOG = LoggerFactory
			.getLogger(Tmct0CnmvEjecuciones.class);

	/** Campos de la entidad */

	/**
	 * Identificador del registro en la tabla
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;

	/**
	 * Fecha en la que actual de la creación o última modificación
	 */
	@Column(name = "AUDIT_DATE", length = 10, nullable = false)
	private Date auditDate;

	/**
	 * identificador del usuario que creó o modificó por última vez la factura
	 */
	@Column(name = "AUDIT_USER", length = 20, nullable = false)
	private String auditUser;

	/**
	 * Datos de la ejecucion a la que pertenece:
	 */
	@OneToOne
	@JoinColumn(name = "ID_REG_DESGLOSE" , referencedColumnName = "ID", nullable=false)
	private Tmct0CnmvDesgloses desglose;

	/**
	 * Tipo de identificación del titular
	 */
	@Column(name = "TPIDENTI", length = 3, nullable = false)
	private String tpidenti;

	/**
	 * Código de Identificación del titular
	 */
	@Column(name = "IDENTIFICACION", length = 35, nullable = false)
	private String identificacion;

	/**
	 * Nombre / Razón Social del titular
	 */
	@Column(name = "NOMBRE", length = 140, nullable = false)
	private String nombre;

	/**
	 * Apellido 1 del titular
	 */
	@Column(name = "APELLIDO1", length = 70)
	private String apellido1;

	/**
	 * Apellido 2 del titular
	 */
	@Column(name = "APELLIDO2", length = 70)
	private String apellido2;

	/**
	 * Fecha de nacimiento del titular
	 */

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_NACIMIENTO")
	private Date fNacimiento;

	/**
	 * Tipo de persona: F (Física) / J (Jurídica)
	 */
	@Column(name = "TIPO_PERSONA", length = 1, nullable = false)
	private Character tipoPersona;

	/**
	 * Tipo de titular:  T,N.U,R
	 */
	@Column(name = "TIPO_TITULAR", length = 1, nullable = false)
	private Character tipoTiular;
	
	/**
	 * Código del país de la sucursal del cliente
	 */
	@Column(name = "CD_PAIS_SUC_CLIENTE", length = 2)
	private Character cdPaisSucCliente;

	/**
	 * Tipo de identificación del responsable de la decisión de inversión
	 */
	@Column(name = "TPIDENTI_RESPONSABLE", length = 3)
	private String tpidentiResp;

	/**
	 * Código de Identificación del responsable de la decisión de inversión
	 */
	@Column(name = "IDENTIFICACION_RESP", length = 35)
	private String identificacionResp;

	/**
	 * Nombre / Razón Social del responsable de la decisión de inversión
	 */
	@Column(name = "NOMBRE_RESPONSABLE", length = 140)
	private String nombreResp;

	/**
	 * Apellido 1 del responsable de la decisión de inversión
	 */
	@Column(name = "APELLIDO1_RESPONSABLE", length = 70)
	private String apellido1Resp;

	/**
	 * Apellido 2 del responsable de la decisión de inversión
	 */
	@Column(name = "APELLIDO2_RESPONSABLE", length = 70)
	private String apellido2Resp;

	/**
	 * Fecha de nacimiento del responsable de la decisión de inversión
	 */

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_NACIMIENTO_RESP")
	private Date fNacimientoResp;

	/**
	 * A Alta / B Baja
	 */

	@Column(name = "ESTADO_TITULAR", length = 1, nullable = false)
	private Character estadoTitular;	
	
	
	
	/**
   * cdnactit
   */
  @Column(name = "CDNACTIT", length = 3)
  private Character cdnactit;
	
	/**
	 * Constructor
	 */
	public Tmct0CnmvTitulares() {
		super();
	}

	/**
	 * Constructor con parametros
	 */

	public Tmct0CnmvTitulares(BigInteger id, Date auditDate, String auditUser,
			Tmct0CnmvDesgloses desglose, String tpidenti,
			String identificacion, String nombre, String apellido1,
			String apellido2, Date fNacimiento, Character tipoPersona,
			Character tipoTitular,
			Character cdPaisSucCliente, String tpidentiResp,
			String identificacionResp, String nombreResp, String apellido1Resp,
			String apellido2Resp, Date fNacimientoResp,
			Character estadoTitular, Character cdnactit) {
		this.id = id;
		this.auditDate = auditDate;
		this.auditUser = auditUser;
		this.desglose = desglose;
		this.tpidenti = tpidenti;
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.fNacimiento = fNacimiento;
		this.tipoPersona = tipoPersona;
		this.tipoTiular = tipoTitular;
		this.cdPaisSucCliente = cdPaisSucCliente;
		this.tpidentiResp = tpidentiResp;
		this.identificacionResp = identificacionResp;
		this.nombreResp = nombreResp;
		this.apellido1Resp = apellido1Resp;
		this.apellido2Resp = apellido2Resp;
		this.fNacimientoResp = fNacimientoResp;
		this.estadoTitular=  estadoTitular;
		this.cdnactit=  cdnactit;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate
	 *            the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser
	 *            the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * @return the desglose
	 */
	public Tmct0CnmvDesgloses getDesglose() {
		return desglose;
	}

	/**
	 * @param desglose
	 *            the desglose to set
	 */
	public void setDesglose(Tmct0CnmvDesgloses desglose) {
		this.desglose = desglose;
	}

	/**
	 * @return the tpidenti
	 */
	public String getTpidenti() {
		return tpidenti;
	}

	/**
	 * @param tpidenti
	 *            the tpidenti to set
	 */
	public void setTpidenti(String tpidenti) {
		this.tpidenti = tpidenti;
	}

	/**
	 * @return the identificacion
	 */
	public String getIdentificacion() {
		return identificacion;
	}

	/**
	 * @param identificacion
	 *            the identificacion to set
	 */
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellido1
	 */
	public String getApellido1() {
		return apellido1;
	}

	/**
	 * @param apellido1
	 *            the apellido1 to set
	 */
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	/**
	 * @return the apellido2
	 */
	public String getApellido2() {
		return apellido2;
	}

	/**
	 * @param apellido2
	 *            the apellido2 to set
	 */
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	/**
	 * @return the fNacimiento
	 */
	public Date getfNacimiento() {
		return fNacimiento;
	}

	/**
	 * @param fNacimiento
	 *            the fNacimiento to set
	 */
	public void setfNacimiento(Date fNacimiento) {
		this.fNacimiento = fNacimiento;
	}

	/**
	 * @return the tipoPersona
	 */
	public Character getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(Character tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * @return the tipoTiular
	 */
	public Character getTipoTiular() {
		return tipoTiular;
	}

	/**
	 * @param tipoTiular the tipoTiular to set
	 */
	public void setTipoTiular(Character tipoTiular) {
		this.tipoTiular = tipoTiular;
	}

	/**
	 * @return the cdPaisSucCliente
	 */
	public Character getCdPaisSucCliente() {
		return cdPaisSucCliente;
	}

	/**
	 * @param cdPaisSucCliente
	 *            the cdPaisSucCliente to set
	 */
	public void setCdPaisSucCliente(Character cdPaisSucCliente) {
		this.cdPaisSucCliente = cdPaisSucCliente;
	}

	/**
	 * @return the tpidentiResp
	 */
	public String getTpidentiResp() {
		return tpidentiResp;
	}

	/**
	 * @param tpidentiResp
	 *            the tpidentiResp to set
	 */
	public void setTpidentiResp(String tpidentiResp) {
		this.tpidentiResp = tpidentiResp;
	}

	/**
	 * @return the identificacionResp
	 */
	public String getIdentificacionResp() {
		return identificacionResp;
	}

	/**
	 * @param identificacionResp
	 *            the identificacionResp to set
	 */
	public void setIdentificacionResp(String identificacionResp) {
		this.identificacionResp = identificacionResp;
	}

	/**
	 * @return the nombreResp
	 */
	public String getNombreResp() {
		return nombreResp;
	}

	/**
	 * @param nombreResp
	 *            the nombreResp to set
	 */
	public void setNombreResp(String nombreResp) {
		this.nombreResp = nombreResp;
	}

	/**
	 * @return the apellido1Resp
	 */
	public String getApellido1Resp() {
		return apellido1Resp;
	}

	/**
	 * @param apellido1Resp
	 *            the apellido1Resp to set
	 */
	public void setApellido1Resp(String apellido1Resp) {
		this.apellido1Resp = apellido1Resp;
	}

	/**
	 * @return the apellido2Resp
	 */
	public String getApellido2Resp() {
		return apellido2Resp;
	}

	/**
	 * @param apellido2Resp
	 *            the apellido2Resp to set
	 */
	public void setApellido2Resp(String apellido2Resp) {
		this.apellido2Resp = apellido2Resp;
	}

	/**
	 * @return the fNacimientoResp
	 */
	public Date getfNacimientoResp() {
		return fNacimientoResp;
	}

	/**
	 * @param fNacimientoResp
	 *            the fNacimientoResp to set
	 */
	public void setfNacimientoResp(Date fNacimientoResp) {
		this.fNacimientoResp = fNacimientoResp;
	}

	/**
	 * @return the estadoTitular
	 */
	public Character getEstadoTitular() {
		return estadoTitular;
	}

	/**
	 * @param estadoTitular the estadoTitular to set
	 */
	public void setEstadoTitular(Character estadoTitular) {
		this.estadoTitular = estadoTitular;
	}

  /**
   * @return the cdnactit
   */
  public Character getCdnactit() {
    return cdnactit;
  }

  /**
   * @param cdnactit the cdnactit to set
   */
  public void setCdnactit(Character cdnactit) {
    this.cdnactit = cdnactit;
  }



	
}
