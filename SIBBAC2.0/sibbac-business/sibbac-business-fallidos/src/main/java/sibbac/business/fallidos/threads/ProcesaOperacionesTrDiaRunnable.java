package sibbac.business.fallidos.threads;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.database.bo.OperacionesTrBo;

/**
 * Representa un hilo que procesa las Operaciones RT de un dia en concreto
 * 
 * @author Neoris
 *
 */
public class ProcesaOperacionesTrDiaRunnable implements Runnable {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaOperacionesTrDiaRunnable.class);

  // Bo con la logica de las operaciones
  private OperacionesTrBo generarAutoRTBo = null;

  private Date fechaEjecucion = null;

  private boolean isScriptDividend;

  // Constructor
  public ProcesaOperacionesTrDiaRunnable(OperacionesTrBo generarAutoRTBo, Date fechaEjecucion, boolean isSd) {
    this.generarAutoRTBo = generarAutoRTBo;
    this.fechaEjecucion = fechaEjecucion;
    this.isScriptDividend = isSd;
  }

  @Override
  public void run() {
    generarAutoRTBo.generaOperacionesDiaRT(fechaEjecucion, isScriptDividend);
  }

}
