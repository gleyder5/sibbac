package sibbac.business.fallidos.dto;

import java.util.List;

import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;

public class DesgloseMtfDto {

 
  private Tmct0CnmvDesgloses desglose;
   
  private List<Tmct0CnmvTitulares> listaTitulares;
  
  private List<Tmct0CnmvTitulares> listaRepresentantes;
  
  private boolean bTitulosPropios = false;

  
  public DesgloseMtfDto(){
    
  };
  
  

  public DesgloseMtfDto(Tmct0CnmvDesgloses desglose,
                        List<Tmct0CnmvTitulares> listaTitulares,
                        List<Tmct0CnmvTitulares> listaRepresentantes,
                        boolean bTitulosPropios) {
    super();
    this.desglose = desglose;
    this.listaTitulares = listaTitulares;
    this.listaRepresentantes = listaRepresentantes;
    this.bTitulosPropios = bTitulosPropios;
  }


  /**
   * @return the desglose
   */
  public Tmct0CnmvDesgloses getDesglose() {
    return desglose;
  }

  /**
   * @param desglose the desglose to set
   */
  public void setDesglose(Tmct0CnmvDesgloses desglose) {
    this.desglose = desglose;
  }

  /**
   * @return the listaTitulares
   */
  public List<Tmct0CnmvTitulares> getListaTitulares() {
    return listaTitulares;
  }

  /**
   * @param listaTitulares the listaTitulares to set
   */
  public void setListaTitulares(List<Tmct0CnmvTitulares> listaTitulares) {
    this.listaTitulares = listaTitulares;
  }

  /**
   * @return the listaRepresentantes
   */
  public List<Tmct0CnmvTitulares> getListaRepresentantes() {
    return listaRepresentantes;
  }

  /**
   * @param listaRepresentantes the listaRepresentantes to set
   */
  public void setListaRepresentantes(List<Tmct0CnmvTitulares> listaRepresentantes) {
    this.listaRepresentantes = listaRepresentantes;
  }

  /**
   * @return the bTitulosPropios
   */
  public boolean getbTitulosPropios() {
    return bTitulosPropios;
  }

  /**
   * @param bTitulosPropios the bTitulosPropios to set
   */
  public void setbTitulosPropios(boolean bTitulosPropios) {
    this.bTitulosPropios = bTitulosPropios;
  }
  
  
  
} 
