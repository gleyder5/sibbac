package sibbac.business.fallidos.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.TitularesH47;


@Repository
public interface TitularesH47Dao extends JpaRepository< TitularesH47, Long > {

}
