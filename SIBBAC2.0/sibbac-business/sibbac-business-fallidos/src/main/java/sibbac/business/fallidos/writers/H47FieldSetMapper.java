package sibbac.business.fallidos.writers;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.validation.BindException;


public class H47FieldSetMapper implements FieldSetMapper< H47RecordBean > {

	/** Referencia para la inserción de logs. */
	private static final Logger LOG = LoggerFactory.getLogger(H47FieldSetMapper.class);

	public final static String	delimiter	= ";";

	private DefaultLineMapper<H47TitularRecordBean> titularesLineMapper;
	private FixedLengthTokenizer titularesLineTokenizer;
	
	public enum H47Fields {
		ICCSERVI( "iccservi", 6 ),
		ICCERRME( "iccerrme", 3 ),
		ICCFORMA( "iccforma", 3 ),
		ICCENTSV( "iccentsv", 4 ),
		ICFCREAC( "icfcreac", 8 ),
		ICHCREAC( "ichcreac", 6 ),
		ICDCORRE( "icdcorre", 60 ),
		ICUBICEN( "icubicen", 1 ),
		ICCBICEN( "iccbicen", 40 ),
		ICFNEGOC( "icfnegoc", 8 ),
		ICHNEGOC( "ichnegoc", 6 ),
		ICUHORA( "icuhora", 3 ),
		ICFVALOR( "icfvalor", 8 ),
		ICUTIOPE( "icutiope", 1 ),
		ICUPROTE( "icuprote", 1 ),
		ICCVALOR( "iccvalor", 60 ),
		ICUINSTRU( "icuinstru", 4 ),
		ICUIDENTI( "icuidenti", 60 ),
		ICUSUBYA( "icusubya", 4 ),
		ICUTIPOI( "icutipoi", 3 ),
		ICFVENCI( "icfvenci", 8 ),
		ICUDERIV( "icuderiv", 1 ),
		ICUPUCAL( "icupucal", 1 ),
		ICIPRCIJ( "iciprcij", 17 ),
		ICQPRCIJ( "icqprcij", 17 ),
		ICIPRECI( "icipreci", 17 ),
		ICUPRECI( "icupreci", 1 ),
		ICCDIVIS( "iccdivis", 3 ),
		ICVCONTR( "icvcontr", 17 ),
		ICUCONTR( "icucontr", 1 ),
		ICIEFECT( "iciefect", 15 ),
		ICCBICCO( "iccbicco", 13 ),
		ICUBICCO( "icubicco", 1 ),
		ICDSISTE( "icdsiste", 11 ),
		ICUSISTE( "icusiste", 1 ),
		ICNREFME( "icnrefme", 17 ),
		ICUACTU( "icuactu", 1 ),
		ICNERG01( "icnreg01", 5 ),
		TITULARES("titulares", 10000);

		private final String	text;
		private final int		length;

		/**
		 * @param text
		 */
		private H47Fields( final String text, int length ) {
			this.text = text;
			this.length = length;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return text;
		}

		public static String[] getFieldNames() {
			H47Fields[] fields = H47Fields.values();
			final String[] namesArray = new String[ fields.length ];
			for ( int i = 0; i < fields.length; i++ ) {
				namesArray[ i ] = fields[ i ].text;
			}
			return namesArray;
		}

		public static int[] getH47Lengths() {
			H47Fields[] fields = H47Fields.values();
			final int[] lengthsArray = new int[ fields.length ];
			for ( int i = 0; i < fields.length; i++ ) {
				lengthsArray[ i ] = fields[ i ].length;
			}
			return lengthsArray;
		}
	}
	
	public H47FieldSetMapper(){
		titularesLineMapper = new DefaultLineMapper< H47TitularRecordBean>();
		titularesLineTokenizer = new FixedLengthTokenizer();
		titularesLineTokenizer.setNames( H47TitularFieldSetMapper.H47TitularFields.getFieldNames() );
		titularesLineTokenizer.setColumns( this.convertLengthsIntoRanges( H47TitularFieldSetMapper.H47TitularFields.getH47TitularLengths()) );
		titularesLineTokenizer.setStrict( false );
		( ( DefaultLineMapper< H47TitularRecordBean > ) titularesLineMapper ).setLineTokenizer( titularesLineTokenizer );
		( ( DefaultLineMapper< H47TitularRecordBean > ) titularesLineMapper ).setFieldSetMapper( new H47TitularFieldSetMapper() );
	}

	@Override
	public H47RecordBean mapFieldSet( FieldSet fieldSet ) throws BindException {
		H47RecordBean h47bean = new H47RecordBean();
		h47bean.setIccerrme( fieldSet.readString( H47Fields.ICCERRME.text ) );
		h47bean.setIcfnegoc( fieldSet.readString( H47Fields.ICFNEGOC.text ) );
		h47bean.setIcutiope( fieldSet.readString( H47Fields.ICUTIOPE.text ) );
		h47bean.setIcnrefme( fieldSet.readString( H47Fields.ICNREFME.text ) );
		return h47bean;
	}

//	@Override
	/*
	 * Este se usaria si se desea recuperar toda la información del registro. Parece que al final no hará falta
	 */
	public H47RecordBean mapFieldSetOld( FieldSet fieldSet ) throws BindException {
		H47RecordBean h47bean = new H47RecordBean();
		h47bean.setIccservi( fieldSet.readString( H47Fields.ICCSERVI.text ) );
		h47bean.setIccerrme( fieldSet.readString( H47Fields.ICCERRME.text ) );
		h47bean.setIccforma( fieldSet.readString( H47Fields.ICCFORMA.text ) );
		h47bean.setIccentsv( fieldSet.readString( H47Fields.ICCENTSV.text ) );
		h47bean.setIcfcreac( fieldSet.readString( H47Fields.ICFCREAC.text ) );
		h47bean.setIchcreac( fieldSet.readString( H47Fields.ICHCREAC.text ) );
		h47bean.setIcdcorre( fieldSet.readString( H47Fields.ICDCORRE.text ) );
		h47bean.setIcubicen( fieldSet.readString( H47Fields.ICUBICEN.text ) );
		h47bean.setIccbicen( fieldSet.readString( H47Fields.ICCBICEN.text ) );
		h47bean.setIcfnegoc( fieldSet.readString( H47Fields.ICFNEGOC.text ) );
		h47bean.setIcuhora( fieldSet.readString( H47Fields.ICUHORA.text ) );
		h47bean.setIcfvalor( fieldSet.readString( H47Fields.ICFVALOR.text ) );
		h47bean.setIcutiope( fieldSet.readString( H47Fields.ICUTIOPE.text ) );
		h47bean.setIcuprote( fieldSet.readString( H47Fields.ICUPROTE.text ) );
		h47bean.setIccvalor( fieldSet.readString( H47Fields.ICCVALOR.text ) );
		h47bean.setIcuinstru( fieldSet.readString( H47Fields.ICUINSTRU.text ) );
		h47bean.setIcuidenti( fieldSet.readString( H47Fields.ICUIDENTI.text ) );
		h47bean.setIcusubya( fieldSet.readString( H47Fields.ICUSUBYA.text ) );
		h47bean.setIcutipoi( fieldSet.readString( H47Fields.ICUTIPOI.text ) );
		h47bean.setIcfvenci( fieldSet.readString( H47Fields.ICFVENCI.text ) );
		h47bean.setIcuderiv( fieldSet.readString( H47Fields.ICUDERIV.text ) );
		h47bean.setIcupucal( fieldSet.readString( H47Fields.ICUPUCAL.text ) );

		h47bean.setIciprcij( fieldSet.readString( H47Fields.ICIPRCIJ.text ) );
		h47bean.setIcqprcij( fieldSet.readString( H47Fields.ICQPRCIJ.text ) );
		h47bean.setIcipreci( fieldSet.readString( H47Fields.ICIPRECI.text ) );

		h47bean.setIcupreci( fieldSet.readString( H47Fields.ICUPRECI.text ) );
		h47bean.setIccdivis( fieldSet.readString( H47Fields.ICCDIVIS.text ) );

		h47bean.setIcvcontr( fieldSet.readString( H47Fields.ICVCONTR.text ) );

		h47bean.setIcucontr( fieldSet.readString( H47Fields.ICUCONTR.text ) );

		h47bean.setIciefect( fieldSet.readString( H47Fields.ICIEFECT.text ) );

		h47bean.setIccbicco( fieldSet.readString( H47Fields.ICCBICCO.text ) );
		h47bean.setIcubicco( fieldSet.readString( H47Fields.ICUBICCO.text ) );
		h47bean.setIcdsiste( fieldSet.readString( H47Fields.ICDSISTE.text ) );
		h47bean.setIcusiste( fieldSet.readString( H47Fields.ICUSISTE.text ) );
		h47bean.setIcnrefme( fieldSet.readString( H47Fields.ICNREFME.text ) );
		h47bean.setIcuactu( fieldSet.readString( H47Fields.ICUACTU.text ) );
//		Integer nTitulares = fieldSet.readInt( H47Fields.ICNERG01.text );
		String textoTitulares = fieldSet.readString( H47Fields.TITULARES.text );

		Integer lineNumber = 0;
		int longitudTitular = H47TitularFieldSetMapper.H47TitularFields.getH47TitularTotalLength();

		List<String> infoTitulares = Arrays.asList(textoTitulares.split("(?<=\\G.{"+longitudTitular+"})"));

		for(String textoTitular:infoTitulares){
			if ( StringUtils.isNotEmpty( StringUtils.trimToEmpty( textoTitular ) ) ) {
				H47TitularRecordBean beanTitular = null;
				try {
					beanTitular = ( H47TitularRecordBean ) titularesLineMapper.mapLine( textoTitular, lineNumber );
				} catch ( Exception e ) {
					LOG.error("Error: ", e.getClass().getName(), e.getMessage());
				}
				h47bean.getListaTitulares().add( beanTitular );
			}
			lineNumber++;
		}
		return h47bean;
	}
	
	
	protected Range[] convertLengthsIntoRanges( final int[] fieldLenghts ) {
		Range[] ranges = new Range[ fieldLenghts.length ];
		int distAux = 1;
		for ( int i = 0; i < fieldLenghts.length; i++ ) {
			if ( fieldLenghts[ i ] > 0 ) {
				Range auxRange = new Range( distAux, distAux + fieldLenghts[ i ] - 1 );
				distAux += fieldLenghts[ i ];
				ranges[ i ] = auxRange;
			}
		}
		return ranges;
	}

}
