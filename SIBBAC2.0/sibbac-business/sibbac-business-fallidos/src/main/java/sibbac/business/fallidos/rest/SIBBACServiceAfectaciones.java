package sibbac.business.fallidos.rest;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.dto.AfectacionDTO;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
//import sibbac.business.fallidos.database.bo.AfectacionBo;


/**
 * Servicio con las operaciones disponibles sobre la tabla <code>Afectaciones</code>.
 * 
 * @author XI316153
 */
@SIBBACService
public class SIBBACServiceAfectaciones implements SIBBACServiceBean {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Referencia para la inserción de logs. */
	protected static final Logger		LOG									= LoggerFactory.getLogger( SIBBACServiceAfectaciones.class );

	/** Comando para el listado de las afectaciones. */
	private static final String			CMD_LIST_AFECTACIONES				= "listAfectaciones";

	/** Comando para persistir las afectaciones. */
	private static final String			CMD_SAVE_AFECTACIONES				= "saveAfectaciones";

	/** Parámetro necesario para las operaciones de listado y guardado de afectaciones. */
	private static final String			PARAM_OPERACION_FALLIDA_ID			= "operacionFallidaId";

	/** Indica si el registro es editable. */
	private static final String			PARAM_AFECTACION_EDITABLE			= "editable";

	/** Identificador de la orden a la que pertence el DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_NUORDEN			= "nuorden";

	/** Identificador del booking al que pertenece el DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_NBOOKING			= "nbooking";

	/** Identificador del ALO al que pertenece el DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_NUCNFCLT			= "nucnfclt";

	/** Identificador del ALC al que pertenece el DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_NUCNFLIQ			= "nucnfliq";

	/** Identificador del DESGLOSECAMARA al que pertenece el DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_IDDESGLOSECAMARA	= "iddesglosecamara";

	/** Identificador del DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_NUDESGLOSE			= "nudesglose";

	/** Número de títulos del DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_IMTITULOS			= "imtitulos";

	/** Precio por título del DESGLOSECLITIT. */
	private static final String			PARAM_AFECTACION_IMPRECIO			= "imprecio";

	/** Alias al que pertenece la ejecución. */
	private static final String			PARAM_AFECTACION_CDALIAS			= "cdalias";

	/** Fecha de realización de la ejecución. */
	private static final String			PARAM_AFECTACION_FEEJECUC			= "feejecuc";

	/** Hora de realización de la ejecución. */
	private static final String			PARAM_AFECTACION_HOEJECUC			= "hoejecuc";

	/** Número de títulos afectados. */
	private static final String			PARAM_AFECTACION_NUTITAFECTADOS		= "nuTitAfectados";

	/** Saldo Neto. */
	private static final String			PARAM_AFECTACION_SALDONETO			= "saldoNeto";

	/** Contenido de la respuesta para el comando CMD_LIST. */
	private static final String			RESULT_LIST_AFECTACIONES			= "listadoAfectaciones";

	private static final String			RESULTADO							= "resultado";
	private static final String			RESULTADO_SUCCESS					= "SUCCESS";
	private static final String			RESULTADO_FAIL						= "FAIL";
	/** Nombre de la clave que se envia en el mapa de resultados con el mensaje del error producido. */
	private static final String			ERROR_MESSAGE						= "error_message";

	/** Formateardor de números. */
	private static final NumberFormat	nF									= new DecimalFormat( "#0.00", new DecimalFormatSymbols(
																					new Locale( "es", "ES" ) ) );

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

//	/** Business object para la tabla <code>TMCT0_AFECTACIONES_FALLIDOS</code>. */
//	@Autowired
//	private AfectacionBo				afectacionesBo;

//	/** Objeto para el acceso a la tabla <code>TMCT0_OPERACION_FALLIDO</code>. */
//	@Autowired
//	private OperacionFallidoDao			operacionFallidoDao;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public SIBBACServiceAfectaciones() {
	} // SIBBACServiceAfectaciones

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process( WebRequest webRequest ) {
		final String command = webRequest.getAction();
		final WebResponse webResponse = new WebResponse();

		final Map< String, String > filtros = webRequest.getFilters();
		final List< Map< String, String >> params = webRequest.getParams();

		List< AfectacionDTO > listaValores = null;
		try {
			switch ( command ) {
				case CMD_LIST_AFECTACIONES:
					if ( MapUtils.isNotEmpty( filtros ) ) {
						LOG.trace( "[SIBBACServiceAfectaciones :: process] filtros.get(PARAM_OPERACION_FALLIDA_ID): {}",
								filtros.get( PARAM_OPERACION_FALLIDA_ID ) );
						LOG.trace(
								"[SIBBACServiceAfectaciones :: process] NumberUtils.createLong(filtros.get(PARAM_OPERACION_FALLIDA_ID)): {}",
								NumberUtils.createLong( filtros.get( PARAM_OPERACION_FALLIDA_ID ) ) );
//						listaValores = afectacionesBo.obtenerAfectaciones( operacionFallidoDao.getOne( NumberUtils.createLong( filtros
//								.get( PARAM_OPERACION_FALLIDA_ID ) ) ) );

						final Map< String, List< AfectacionDTO >> valores = new HashMap< String, List< AfectacionDTO >>();
						valores.put( RESULT_LIST_AFECTACIONES, listaValores );

						webResponse.setResultados( valores );
					} else {
						webResponse.setError( command + " needs the OperacionFallido filter." );
					} // else
					break;
				case CMD_SAVE_AFECTACIONES:
					for ( final Map< String, String > param : params ) {
						try {
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_EDITABLE: "
									+ param.get( PARAM_AFECTACION_EDITABLE ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_NUORDEN: "
									+ param.get( PARAM_AFECTACION_NUORDEN ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_NBOOKING: "
									+ param.get( PARAM_AFECTACION_NBOOKING ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_NUCNFCLT: "
									+ param.get( PARAM_AFECTACION_NUCNFCLT ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_NUCNFLIQ: "
									+ NumberUtils.createBigDecimal( param.get( PARAM_AFECTACION_NUCNFLIQ ) ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_IDDESGLOSECAMARA: "
									+ NumberUtils.createLong( param.get( PARAM_AFECTACION_IDDESGLOSECAMARA ) ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_NUDESGLOSE: "
									+ NumberUtils.createLong( param.get( PARAM_AFECTACION_NUDESGLOSE ) ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_IMTITULOS: "
									+ NumberUtils.createBigDecimal( param.get( PARAM_AFECTACION_IMTITULOS ) ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_IMPRECIO: "
									+ NumberUtils.createBigDecimal( nF.parse( param.get( PARAM_AFECTACION_IMPRECIO ) ).toString() ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_CDALIAS: "
									+ param.get( PARAM_AFECTACION_CDALIAS ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_NUTITAFECTADOS: "
									+ NumberUtils.createBigDecimal( param.get( PARAM_AFECTACION_NUTITAFECTADOS ) ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_SALDONETO: "
									+ NumberUtils.createBigDecimal( nF.parse( param.get( PARAM_AFECTACION_SALDONETO ) ).toString() ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_FEEJECUC: "
									+ new java.sql.Date( FormatDataUtils.convertStringToDate( param.get( PARAM_AFECTACION_FEEJECUC ),
											"dd/MM/yyyy" ).getTime() ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_HOEJECUC: "
									+ param.get( PARAM_AFECTACION_HOEJECUC ) );
							LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]: PARAM_AFECTACION_HOEJECUC Time: "
									+ FormatDataUtils.convertStringToTime( param.get( PARAM_AFECTACION_HOEJECUC ), "hh:mm:ss" ) );
						} catch ( Exception e ) {
							LOG.error( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES] :: ERROR "
									+ e.getLocalizedMessage() );
						} // catch
					} // for

					LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]" );
					if ( MapUtils.isNotEmpty( filtros ) && ( CollectionUtils.isNotEmpty( params ) ) ) {
						LOG.trace( "[SIBBACServiceAfectaciones :: process] filtros.get(PARAM_OPERACION_FALLIDA_ID): {}",
								filtros.get( PARAM_OPERACION_FALLIDA_ID ) );
						LOG.trace(
								"[SIBBACServiceAfectaciones :: process] NumberUtils.createLong(filtros.get(PARAM_OPERACION_FALLIDA_ID)): {}",
								NumberUtils.createLong( filtros.get( PARAM_OPERACION_FALLIDA_ID ) ) );

						LOG.trace( "[SIBBACServiceAfectaciones :: process :: CMD_SAVE_AFECTACIONES]" );

//						afectacionesBo.salvarAltaAfectaciones(
//								operacionFallidoDao.getOne( NumberUtils.createLong( filtros.get( PARAM_OPERACION_FALLIDA_ID ) ) ),
//								parseaAfectacionesParam( params ) );

						final Map< String, String > resultados = new HashMap< String, String >();
						resultados.put( RESULTADO, RESULTADO_SUCCESS );
						webResponse.setResultados( resultados );
					} else {
						webResponse.setError( command + " needs the OperacionFallido filter and afectacionesDTO param." );
					} // else
					break;
				default:
					webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
			} // switch
		} catch ( Exception e ) {
			LOG.error( "[SIBBACServiceAfectaciones :: process] Error: " + e );
			LOG.error( "[SIBBACServiceAfectaciones :: process] Error Message: " + e.getMessage() );

			Map< String, Object > resultado = new HashMap< String, Object >();

			resultado.put( RESULTADO, RESULTADO_FAIL );
			resultado.put( ERROR_MESSAGE, e.getMessage() );

			webResponse.setResultados( resultado );
		} // catch

		return webResponse;
	} // process

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LIST_AFECTACIONES );
		commands.add( CMD_SAVE_AFECTACIONES );
		return commands;
	} // getAvailableCommands

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	} // getFields

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

	/**
	 * @param webRequest
	 * @param webResponse
	 * @throws ParseException
	 */
	private List< AfectacionDTO > parseaAfectacionesParam( List< Map< String, String >> params ) throws ParseException {
		List< AfectacionDTO > afectacionesDTO = new ArrayList< AfectacionDTO >();
		AfectacionDTO afectacionDTO = null;

		for ( final Map< String, String > param : params ) {
			afectacionDTO = new AfectacionDTO( param.get( PARAM_AFECTACION_EDITABLE ).compareTo( "true" ) == 0 ? Boolean.TRUE
					: Boolean.FALSE, param.get( PARAM_AFECTACION_NUORDEN ), param.get( PARAM_AFECTACION_NBOOKING ),
					param.get( PARAM_AFECTACION_NUCNFCLT ), NumberUtils.createBigDecimal( param.get( PARAM_AFECTACION_NUCNFLIQ ) ),
					NumberUtils.createLong( param.get( PARAM_AFECTACION_IDDESGLOSECAMARA ) ), NumberUtils.createLong( param
							.get( PARAM_AFECTACION_NUDESGLOSE ) ), NumberUtils.createBigDecimal( param.get( PARAM_AFECTACION_IMTITULOS ) ),
					NumberUtils.createBigDecimal( nF.parse( param.get( PARAM_AFECTACION_IMPRECIO ) ).toString() ),
					param.get( PARAM_AFECTACION_CDALIAS ), new java.sql.Date( FormatDataUtils.convertStringToDate(
							param.get( PARAM_AFECTACION_FEEJECUC ), "dd/MM/yyyy" ).getTime() ), FormatDataUtils.convertStringToTime(
							param.get( PARAM_AFECTACION_HOEJECUC ), "hh:mm:ss" ), NumberUtils.createBigDecimal( param
							.get( PARAM_AFECTACION_NUTITAFECTADOS ) ), NumberUtils.createBigDecimal( nF.parse(
							param.get( PARAM_AFECTACION_SALDONETO ) ).toString() ) );

			afectacionesDTO.add( afectacionDTO );
		} // for

		return afectacionesDTO;
	} // parseaAfectacionesParam

} // SIBBACServiceAfectaciones
