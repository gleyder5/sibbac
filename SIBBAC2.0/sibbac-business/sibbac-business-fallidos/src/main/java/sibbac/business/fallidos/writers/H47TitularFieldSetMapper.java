package sibbac.business.fallidos.writers;


import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;


public class H47TitularFieldSetMapper implements FieldSetMapper< H47TitularRecordBean > {

	public final static String	delimiter	= ";";

	public enum H47TitularFields {
		ICUTIREG( "icutireg", 2 ),
		ICUCLIENT( "icuclient", 1 ),
		ICCLIENT( "icclient", 1 ),
		ICDCLIEN( "icdclien", 40 ),
		ICDNOMBR( "icdnombr", 60 ),
		ICDAPEL1( "icdapel1", 40 ),
		ICDAPEL2( "icdapel2", 40 ),
		ICUTIPOR( "icutipor", 1 ),
		ICDIDENT( "icdident", 40 ),
		ICDNOMRE( "icdnomre", 60 ),
		ICDAP1RE( "icdap1re", 40 ),
		ICDAP2RE( "icdap2re", 40 ),
		ICVCANTI( "icvcanti", 17 );

		private final String	text;
		private final int		length;

		/**
		 * @param text
		 */
		private H47TitularFields( final String text, int length ) {
			this.text = text;
			this.length = length;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return text;
		}

		public static String[] getFieldNames() {
			H47TitularFields[] fields = H47TitularFields.values();
			final String[] namesArray = new String[ fields.length ];
			for ( int i = 0; i < fields.length; i++ ) {
				namesArray[ i ] = fields[ i ].text;
			}
			return namesArray;
		}

		public static int[] getH47TitularLengths() {
			H47TitularFields[] fields = H47TitularFields.values();
			final int[] lengthsArray = new int[ fields.length ];
			for ( int i = 0; i < fields.length; i++ ) {
				lengthsArray[ i ] = fields[ i ].length;
			}
			return lengthsArray;
		}
		

		public static int getH47TitularTotalLength() {
			H47TitularFields[] fields = H47TitularFields.values();
			int totalLength = 0;
			for ( int i = 0; i < fields.length; i++ ) {
				totalLength+= fields[ i ].length;
			}
			return totalLength;
		}
	}

	@Override
	public H47TitularRecordBean mapFieldSet( FieldSet fieldSet ) throws BindException {
		H47TitularRecordBean h47TitularBean = new H47TitularRecordBean();

		h47TitularBean.setIcuclient( fieldSet.readString( H47TitularFields.ICUCLIENT.text ) );
		h47TitularBean.setIcclient( fieldSet.readString( H47TitularFields.ICCLIENT.text )  );
		h47TitularBean.setIcdclient( fieldSet.readString( H47TitularFields.ICDCLIEN.text ) );
		h47TitularBean.setIcdnombr( fieldSet.readString( H47TitularFields.ICDNOMBR.text )   );
		h47TitularBean.setIcdapel1( fieldSet.readString( H47TitularFields.ICDAPEL1.text )  );
		h47TitularBean.setIcdapel2( fieldSet.readString( H47TitularFields.ICDAPEL2.text )  );
	
		h47TitularBean.setIcutipor( fieldSet.readString( H47TitularFields.ICUTIPOR.text )  );
		h47TitularBean.setIcdident(  fieldSet.readString( H47TitularFields.ICDIDENT.text ) );
		h47TitularBean.setIcdnombre( fieldSet.readString( H47TitularFields.ICDNOMRE.text )  );
		h47TitularBean.setIcdap1re( fieldSet.readString( H47TitularFields.ICDAP1RE.text )  );
		h47TitularBean.setIcdap2re( fieldSet.readString( H47TitularFields.ICDAP2RE.text )  );

		h47TitularBean.setIcvcanti( fieldSet.readString( H47TitularFields.ICVCANTI.text )  );

		return h47TitularBean;
	}

}
