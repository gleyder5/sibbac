package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Clase que representa la tabla TMCT0_INSTRUCCION_LIQUIDACION_FALLIDA de la base de datos.
 */
@Entity
@Table(name = DBConstants.FALLIDOS.FALLIDOS_IL)
public class InstruccionLiquidacionFallida extends ATable<InstruccionLiquidacionFallida> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -5633465129287229969L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** dentificador de registro de la ECC. */
  @Column(name = "NUMERO_OPERACION", nullable = false, length = 16)
  private String numeroOperacionEcc;

  /** Tipo de Operación de la ECC. */
  @Column(name = "TIPO_OPERACION_ECC", nullable = false)
  private String tipoOperacionEcc;

  /** Volumen de la instrucción. */
  @Column(name = "VOLUMEN_OPERACION", nullable = false, precision = 18, scale = 6)
  private BigDecimal volumenInstruccion;

  /** Volumen vivo de la instrucción. */
  @Column(name = "VOLUMEN_VIVO_OPERACION", nullable = false, precision = 18, scale = 6)
  private BigDecimal volumenVivoInstruccion;

  /** Precio de la instrucción. */
  @Column(name = "PRECIO_OPERACION", nullable = false, precision = 13, scale = 6)
  private BigDecimal precioInstruccion;

  /** Código de divisa. Expresada según estándar ISO 4217. */
  @Column(name = "DIVISA", nullable = false, length = 3)
  private String divisa;

  /** Fecha teórica de liquidación. */
  @Column(name = "FECHA_LIQUIDACION", nullable = false)
  @Temporal(TemporalType.DATE)
  private Date fechaLiquidacion;

  /** Importe efectivo de la instrucción. */
  @Column(name = "IMPORTE_EFECTIVO_INSTRUCCION", nullable = false, precision = 15, scale = 2)
  private BigDecimal importeEfectivoInstruccion;

  /** Tipo de mensaje. */
  @Column(name = "TIPO_MENSAJE", nullable = false)
  private String tipoMensaje;

  /** Identificador de la entidad que envía el mensaje. */
  @Column(name = "ID_ENTIDAD_MENSAJE", nullable = false, length = 4)
  private String idEntidadMensaje;

  /** Código de segmento de la ECC. */
  @Column(name = "SEGMENTO", nullable = false, length = 2)
  private String segmento;

  /** Código ISIN. */
  @Column(name = "ISIN", nullable = false, length = 12)
  private String isin;

  /** Importe efectivo pendiente de liquidar total o parcialmente. */
  @Column(name = "IMPORTE_EFECTIVO_VIVO_OPERACION", nullable = false, precision = 15, scale = 2)
  private BigDecimal importeEfectivoVivoOperacion;

  /** Signo de la posición de valores. */
  @Column(name = "SENTIDO", nullable = false)
  private String sentido;

  /** Indicador de posición. */
  @Column(name = "INDICADOR_POSICION", nullable = false)
  private String indicadorPosicion;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * Constructor I. Constructor por defecto.
   */
  public InstruccionLiquidacionFallida() {
  } // InstruccionLiquidacionFallida

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "InstruccionLiquidacionFallida [numeroOperacionEcc=" + numeroOperacionEcc + ", tipoOperacionEcc="
           + tipoOperacionEcc + ", volumenInstruccion=" + volumenInstruccion + ", volumenVivoInstruccion="
           + volumenVivoInstruccion + ", precioInstruccion=" + precioInstruccion + ", divisa=" + divisa
           + ", fechaLiquidacion=" + fechaLiquidacion + ", importeEfectivoInstruccion=" + importeEfectivoInstruccion
           + ", tipoMensaje=" + tipoMensaje + ", idEntidadMensaje=" + idEntidadMensaje + ", segmento=" + segmento
           + ", isin=" + isin + ", importeEfectivoVivoOperacion=" + importeEfectivoVivoOperacion + ", sentido="
           + sentido + ", indicadorPosicion=" + indicadorPosicion + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the numeroOperacionEcc
   */
  public String getNumeroOperacionEcc() {
    return numeroOperacionEcc;
  }

  /**
   * @return the tipoOperacionEcc
   */
  public String getTipoOperacionEcc() {
    return tipoOperacionEcc;
  }

  /**
   * @return the volumenInstruccion
   */
  public BigDecimal getVolumenInstruccion() {
    return volumenInstruccion;
  }

  /**
   * @return the volumenVivoInstruccion
   */
  public BigDecimal getVolumenVivoInstruccion() {
    return volumenVivoInstruccion;
  }

  /**
   * @return the precioInstruccion
   */
  public BigDecimal getPrecioInstruccion() {
    return precioInstruccion;
  }

  /**
   * @return the divisa
   */
  public String getDivisa() {
    return divisa;
  }

  /**
   * @return the fechaLiquidacion
   */
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  /**
   * @return the importeEfectivoInstruccion
   */
  public BigDecimal getImporteEfectivoInstruccion() {
    return importeEfectivoInstruccion;
  }

  /**
   * @return the tipoMensaje
   */
  public String getTipoMensaje() {
    return tipoMensaje;
  }

  /**
   * @return the idEntidadMensaje
   */
  public String getIdEntidadMensaje() {
    return idEntidadMensaje;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the importeEfectivoVivoOperacion
   */
  public BigDecimal getImporteEfectivoVivoOperacion() {
    return importeEfectivoVivoOperacion;
  }

  /**
   * @return the sentido
   */
  public String getSentido() {
    return sentido;
  }

  /**
   * @return the indicadorPosicion
   */
  public String getIndicadorPosicion() {
    return indicadorPosicion;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param numeroOperacionEcc the numeroOperacionEcc to set
   */
  public void setNumeroOperacionEcc(String numeroOperacionEcc) {
    this.numeroOperacionEcc = numeroOperacionEcc;
  }

  /**
   * @param tipoOperacionEcc the tipoOperacionEcc to set
   */
  public void setTipoOperacionEcc(String tipoOperacionEcc) {
    this.tipoOperacionEcc = tipoOperacionEcc;
  }

  /**
   * @param volumenInstruccion the volumenInstruccion to set
   */
  public void setVolumenInstruccion(BigDecimal volumenInstruccion) {
    this.volumenInstruccion = volumenInstruccion;
  }

  /**
   * @param volumenVivoInstruccion the volumenVivoInstruccion to set
   */
  public void setVolumenVivoInstruccion(BigDecimal volumenVivoInstruccion) {
    this.volumenVivoInstruccion = volumenVivoInstruccion;
  }

  /**
   * @param precioInstruccion the precioInstruccion to set
   */
  public void setPrecioInstruccion(BigDecimal precioInstruccion) {
    this.precioInstruccion = precioInstruccion;
  }

  /**
   * @param divisa the divisa to set
   */
  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  /**
   * @param fechaLiquidacion the fechaLiquidacion to set
   */
  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  /**
   * @param importeEfectivoInstruccion the importeEfectivoInstruccion to set
   */
  public void setImporteEfectivoInstruccion(BigDecimal importeEfectivoInstruccion) {
    this.importeEfectivoInstruccion = importeEfectivoInstruccion;
  }

  /**
   * @param tipoMensaje the tipoMensaje to set
   */
  public void setTipoMensaje(String tipoMensaje) {
    this.tipoMensaje = tipoMensaje;
  }

  /**
   * @param idEntidadMensaje the idEntidadMensaje to set
   */
  public void setIdEntidadMensaje(String idEntidadMensaje) {
    this.idEntidadMensaje = idEntidadMensaje;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param importeEfectivoVivoOperacion the importeEfectivoVivoOperacion to set
   */
  public void setImporteEfectivoVivoOperacion(BigDecimal importeEfectivoVivoOperacion) {
    this.importeEfectivoVivoOperacion = importeEfectivoVivoOperacion;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(String sentido) {
    this.sentido = sentido;
  }

  /**
   * @param indicadorPosicion the indicadorPosicion to set
   */
  public void setIndicadorPosicion(String indicadorPosicion) {
    this.indicadorPosicion = indicadorPosicion;
  }

} // InstruccionLiquidacionFallida
