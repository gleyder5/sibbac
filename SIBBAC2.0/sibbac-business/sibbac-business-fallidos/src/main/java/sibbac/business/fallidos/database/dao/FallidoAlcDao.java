package sibbac.business.fallidos.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.FallidoAlc;

@Repository
public interface FallidoAlcDao extends JpaRepository<FallidoAlc, Long> {

} // FallidoAlcDao
