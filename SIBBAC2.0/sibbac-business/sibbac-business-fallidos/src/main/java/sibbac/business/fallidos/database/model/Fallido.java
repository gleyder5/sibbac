package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

@Table(name = DBConstants.FALLIDOS.FALLIDOS)
@Entity
public class Fallido extends ATable<Fallido> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 5848263577507002128L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Tipo de operación. */
  @Column(name = "TPOPER", nullable = false, length = 1)
  private Character tpoper;

  /** Número de instrucción de liquidación de la ECC. */
  @Column(name = "ILIQECC", nullable = false, length = 20)
  private String iLiqEcc;

  /** Número de instrucción de liquidación de la IBERCLEAR. */
  @Column(name = "ILIQDCV", nullable = false, length = 20)
  private String iLiqDcv;

  /** Fecha de ejecución de la operación. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC", nullable = false)
  private Date feejeliq;

  /** Fecha de liquidación. */
  @Temporal(TemporalType.DATE)
  @Column(name = "FEOPELIQ", nullable = false)
  private Date feopeliq;

  /** Código del alias (cuenta). */
  @Column(name = "CDALIAS", nullable = false, length = 20)
  private String cdAlias;

  /** Descripcion del alias. */
  @Column(name = "DSALIAS", nullable = true, length = 130)
  private String dsAlias;

  /** Código del valor. */
  @Column(name = "CDISIN", nullable = false, length = 12)
  private String cdIsin;

  /** Descripcion del valor. */
  @Column(name = "DSISIN", nullable = true, length = 40)
  private String dsIsin;

  /** Código de la cuenta de compensación. */
  @Column(name = "CDCUENTACOMP", nullable = false, length = 3)
  private String cdCuentaCompensacion;

  /** Código de la cuenta de liquidación. */
  @Column(name = "CDCUENTALIQ", nullable = false, length = 3)
  private String cdCuentaLiquidacion;

  /** Cámara. */
  @Column(name = "CAMARA", nullable = false, length = 4)
  private String camara;

  /** Segmento. */
  @Column(name = "SEGMENTO", nullable = false, length = 2)
  private String segmento;

  /** Títulos fallidos. */
  @Column(name = "TITULOS", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulos;

  /** Efectivo fallido. */
  @Column(name = "EFECTIVO", nullable = false, precision = 15, scale = 6)
  private BigDecimal efectivo;

  /** Saldo neto del cliente para el isin, fecha de liquidación y cuenta de compensación. */
  @Column(name = "SALDO_NETO", nullable = false, precision = 18, scale = 6)
  private BigDecimal saldoNeto;

  /** Títulos del fallidos que han sido liquidados. */
  @Column(name = "TITULOS_LIQUIDADOS", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosLiquidados;

  /** Títulos de un PUI que se le asignarton en origen. */
  @Column(name = "TITULOS_PUI", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosPui;

  /** Títulos asignados de un PUI que no han sido liquidados. */
  @Column(name = "TITULOS_PUI_ACTIVOS", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosPuiActivos;

  /** Títulos recomprados. */
  @Column(name = "TITULOS_RECOMPRADOS", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosRecomprados;

  /** Títulos recomprados en efectivo. */
  @Column(name = "TITULOS_RECOMPRADOS_EFECTIVO", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosRecompradosEfectivo;

  /** Títulos asignados de un PUI que no han sido liquidados. */
  @Column(name = "COLATERAL", nullable = false, precision = 15, scale = 6)
  private BigDecimal colateral;

  /** Indica si la operación fallida está liquidada. */
  @Column(name = "LIQUIDADO", nullable = false)
  private Boolean liquidado;

  /** Indica si los movimientos de la operación fallida han sido contabilizados. */
  @Column(name = "CONTABILIZADO", nullable = false)
  private Boolean contabilizado;

  // /** Lista de afectaciones. */
  // @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
  // @JoinTable(name = DBConstants.FALLIDOS.FALLIDO_AFECTACION, joinColumns = { @JoinColumn(name = "ID_FALLIDO",
  // referencedColumnName = "ID",
  // nullable = true,
  // updatable = false)
  // }, inverseJoinColumns = { @JoinColumn(name = "ID_AFECTACION",
  // referencedColumnName = "ID",
  // nullable = true,
  // updatable = false)
  // })
  // private Set<Afectacion> afectaciones;

  /** Alcs del fallido */
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "fallido_id")
  private Set<FallidoAlc> alcs;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /** Constructor I. Inicializa los atributos de la clase. */
  public Fallido() {
    iLiqEcc = "";
    iLiqDcv = "";
    titulos = BigDecimal.ZERO;
    efectivo = BigDecimal.ZERO;
    saldoNeto = BigDecimal.ZERO;
    titulosLiquidados = BigDecimal.ZERO;
    titulosPui = BigDecimal.ZERO;
    titulosPuiActivos = BigDecimal.ZERO;
    titulosRecomprados = BigDecimal.ZERO;
    titulosRecompradosEfectivo = BigDecimal.ZERO;
    colateral = BigDecimal.ZERO;
    camara = "BME"; // Se debería cargar del desglose cámara
    segmento = "EQ"; // Se debería cargar del desglose cámara
    contabilizado = Boolean.FALSE;
    liquidado = Boolean.FALSE;
    // afectaciones = new HashSet<Afectacion>(0);
  } // Fallido

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Fallido [tpoper=" + tpoper + ", iLiqEcc=" + iLiqEcc + ", iLiqDcv=" + iLiqDcv + ", feejeliq=" + feejeliq
           + ", feopeliq=" + feopeliq + ", cdAlias=" + cdAlias + ", dsAlias=" + dsAlias + ", cdIsin=" + cdIsin
           + ", dsIsin=" + dsIsin + ", cdCuentaCompensacion=" + cdCuentaCompensacion + ", cdCuentaLiquidacion="
           + cdCuentaLiquidacion + ", camara=" + camara + ", segmento=" + segmento + ", titulos=" + titulos
           + ", efectivo=" + efectivo + ", saldoNeto=" + saldoNeto + ", titulosLiquidados=" + titulosLiquidados
           + ", titulosPui=" + titulosPui + ", titulosPuiActivos=" + titulosPuiActivos + ", titulosRecomprados="
           + titulosRecomprados + ", titulosRecompradosEfectivo=" + titulosRecompradosEfectivo + ", colateral="
           + colateral + ", liquidado=" + liquidado + ", contabilizado=" + contabilizado + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the tpoper
   */
  public Character getTpoper() {
    return tpoper;
  }

  /**
   * @return the iLiqEcc
   */
  public String getiLiqEcc() {
    return iLiqEcc;
  }

  /**
   * @return the iLiqDcv
   */
  public String getiLiqDcv() {
    return iLiqDcv;
  }

  /**
   * @return the feejeliq
   */
  public Date getFeejeliq() {
    return feejeliq;
  }

  /**
   * @return the feopeliq
   */
  public Date getFeopeliq() {
    return feopeliq;
  }

  /**
   * @return the cdAlias
   */
  public String getCdAlias() {
    return cdAlias;
  }

  /**
   * @return the dsAlias
   */
  public String getDsAlias() {
    return dsAlias;
  }

  /**
   * @return the cdIsin
   */
  public String getCdIsin() {
    return cdIsin;
  }

  /**
   * @return the dsIsin
   */
  public String getDsIsin() {
    return dsIsin;
  }

  /**
   * @return the cdCuentaCompensacion
   */
  public String getCdCuentaCompensacion() {
    return cdCuentaCompensacion;
  }

  /**
   * @return the cdCuentaLiquidacion
   */
  public String getCdCuentaLiquidacion() {
    return cdCuentaLiquidacion;
  }

  /**
   * @return the camara
   */
  public String getCamara() {
    return camara;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @return the titulos
   */
  public BigDecimal getTitulos() {
    return titulos;
  }

  /**
   * @return the efectivo
   */
  public BigDecimal getEfectivo() {
    return efectivo;
  }

  /**
   * @return the saldoNeto
   */
  public BigDecimal getSaldoNeto() {
    return saldoNeto;
  }

  /**
   * @return the titulosLiquidados
   */
  public BigDecimal getTitulosLiquidados() {
    return titulosLiquidados;
  }

  /**
   * @return the titulosPui
   */
  public BigDecimal getTitulosPui() {
    return titulosPui;
  }

  /**
   * @return the titulosPuiActivos
   */
  public BigDecimal getTitulosPuiActivos() {
    return titulosPuiActivos;
  }

  /**
   * @return the titulosRecomprados
   */
  public BigDecimal getTitulosRecomprados() {
    return titulosRecomprados;
  }

  /**
   * @return the titulosRecompradosEfectivo
   */
  public BigDecimal getTitulosRecompradosEfectivo() {
    return titulosRecompradosEfectivo;
  }

  /**
   * @return the colateral
   */
  public BigDecimal getColateral() {
    return colateral;
  }

  /**
   * @return the liquidado
   */
  public Boolean getLiquidado() {
    return liquidado;
  }

  /**
   * @return the contabilizado
   */
  public Boolean getContabilizado() {
    return contabilizado;
  }

  // /**
  // * @return the afectaciones
  // */
  // public Set<Afectacion> getAfectaciones() {
  // return afectaciones;
  // }

  /**
   * @return the alcs
   */
  public Set<FallidoAlc> getAlcs() {
    return alcs;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param tpoper the tpoper to set
   */
  public void setTpoper(Character tpoper) {
    this.tpoper = tpoper;
  }

  /**
   * @param iLiqEcc the iLiqEcc to set
   */
  public void setiLiqEcc(String iLiqEcc) {
    this.iLiqEcc = iLiqEcc;
  }

  /**
   * @param iLiqDcv the iLiqDcv to set
   */
  public void setiLiqDcv(String iLiqDcv) {
    this.iLiqDcv = iLiqDcv;
  }

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  /**
   * @param feopeliq the feopeliq to set
   */
  public void setFeopeliq(Date feopeliq) {
    this.feopeliq = feopeliq;
  }

  /**
   * @param cdAlias the cdAlias to set
   */
  public void setCdAlias(String cdAlias) {
    this.cdAlias = cdAlias;
  }

  /**
   * @param dsAlias the dsAlias to set
   */
  public void setDsAlias(String dsAlias) {
    this.dsAlias = dsAlias;
  }

  /**
   * @param cdIsin the cdIsin to set
   */
  public void setCdIsin(String cdIsin) {
    this.cdIsin = cdIsin;
  }

  /**
   * @param dsIsin the dsIsin to set
   */
  public void setDsIsin(String dsIsin) {
    this.dsIsin = dsIsin;
  }

  /**
   * @param cdCuentaCompensacion the cdCuentaCompensacion to set
   */
  public void setCdCuentaCompensacion(String cdCuentaCompensacion) {
    this.cdCuentaCompensacion = cdCuentaCompensacion;
  }

  /**
   * @param cdCuentaLiquidacion the cdCuentaLiquidacion to set
   */
  public void setCdCuentaLiquidacion(String cdCuentaLiquidacion) {
    this.cdCuentaLiquidacion = cdCuentaLiquidacion;
  }

  /**
   * @param camara the camara to set
   */
  public void setCamara(String camara) {
    this.camara = camara;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @param titulos the titulos to set
   */
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  /**
   * @param efectivon the efectivo to set
   */
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  /**
   * @param saldoNeto the saldoNeto to set
   */
  public void setSaldoNeto(BigDecimal saldoNeto) {
    this.saldoNeto = saldoNeto;
  }

  /**
   * @param titulosLiquidados the titulosLiquidados to set
   */
  public void setTitulosLiquidados(BigDecimal titulosLiquidados) {
    this.titulosLiquidados = titulosLiquidados;
  }

  /**
   * @param titulosPui the titulosPui to set
   */
  public void setTitulosPui(BigDecimal titulosPui) {
    this.titulosPui = titulosPui;
  }

  /**
   * @param titulosPuiActivos the titulosPuiActivos to set
   */
  public void setTitulosPuiActivos(BigDecimal titulosPuiActivos) {
    this.titulosPuiActivos = titulosPuiActivos;
  }

  /**
   * @param titulosRecomprados the titulosRecomprados to set
   */
  public void setTitulosRecomprados(BigDecimal titulosRecomprados) {
    this.titulosRecomprados = titulosRecomprados;
  }

  /**
   * @param titulosRecompradosEfectivo the titulosRecompradosEfectivo to set
   */
  public void setTitulosRecompradosEfectivo(BigDecimal titulosRecompradosEfectivo) {
    this.titulosRecompradosEfectivo = titulosRecompradosEfectivo;
  }

  /**
   * @param colateral the colateral to set
   */
  public void setColateral(BigDecimal colateral) {
    this.colateral = colateral;
  }

  /**
   * @param liquidado the liquidado to set
   */
  public void setLiquidado(Boolean liquidado) {
    this.liquidado = liquidado;
  }

  /**
   * @param contabilizado the contabilizado to set
   */
  public void setContabilizado(Boolean contabilizado) {
    this.contabilizado = contabilizado;
  }

  // /**
  // * @param afectaciones the afectaciones to set
  // */
  // public void setAfectaciones(Set<Afectacion> afectaciones) {
  // this.afectaciones = afectaciones;
  // }

  /**
   * @param alcs the alcs to set
   */
  public void setAlcs(Set<FallidoAlc> alcs) {
    this.alcs = alcs;
  }

} // Fallido
