package sibbac.business.fallidos.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.FallidoAlcDao;
import sibbac.business.fallidos.database.dao.FallidoDao;
import sibbac.business.fallidos.database.model.Fallido;
import sibbac.business.fallidos.database.model.FallidoAlc;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.dao.S3LiquidacionDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dto.Alc2FailDto;
import sibbac.business.wrappers.database.dto.Alc2FailWithAliasKey;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

/**
 * @author XI316153
 */
@Service
public class FallidoBo extends AbstractBo<Fallido, Long, FallidoDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FallidoBo.class);

  /** Estado de liquidación que indica que está liquidado. */
  private static final String S3LIQ = "LIQ";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0alc</code>. */
  @Autowired
  private Tmct0alcDao tmct0alcDao;

  /** Business object para la tabla <code>TMCT0_S3LIQUIDACION</code>. */
  @Autowired
  private S3LiquidacionDao s3LiquidacionDao;

  /** Business object para la tabla <code>TMCT0_FALLIDOS_ALC</code>. */
  @Autowired
  private FallidoAlcDao fallidoAlcDao;
  
  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Crea las operaciones fallidas para las operaciones de venta y compra pendientes de liquidar, para los ALCs cuya
   * fecha de liquidación sea la especificada y estén sin liquidar.
   * 
   * @param fechaLiquidacion
   * @param isNetting
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {SIBBACBusinessException.class, Exception.class})
  public void createFallidos(Date fechaLiquidacion, Boolean isNetting) throws SIBBACBusinessException {
    LOG.debug("[createFallidos] Inicio ...");
    LOG.debug("[createFallidos] Fecha: " + fechaLiquidacion);
    LOG.debug("[createFallidos] isNetting: " + isNetting);

    // new Date(115, 06, 01)
    List<Object[]> opFallidasList = tmct0alcDao.findAllAlcByEnviadasSinLiquidar(ASIGNACIONES.PDTE_LIQUIDAR.getId(),
                                                                                fechaLiquidacion,
                                                                                CLEARING.PDTE_FICHERO.getId(),
                                                                                CLEARING.LIQUIDADA.getId(),
                                                                                CLEARING.LIQUIDADA_PARCIAL.getId(),
                                                                                isNetting ? 'S' : 'N');
    List<Alc2FailDto> alc2FailDTOList = new ArrayList<Alc2FailDto>(opFallidasList.size());
    for (Object[] fallido : opFallidasList) {
      alc2FailDTOList.add(new Alc2FailDto(fallido));
    } // for

    // Se agrupan los alcs resultantes por fecha de liquidación, isin, cuenta de compensación y alias
    Map<Alc2FailWithAliasKey, List<Alc2FailDto>> fallidosPorTipoMap = new HashMap<Alc2FailWithAliasKey, List<Alc2FailDto>>();
    Alc2FailWithAliasKey alc2FailWithAliasKey = null;
    List<Alc2FailDto> opFallidas = null;
    for (Alc2FailDto alc2FailDto : alc2FailDTOList) {
      alc2FailWithAliasKey = new Alc2FailWithAliasKey(alc2FailDto);

      if ((opFallidas = fallidosPorTipoMap.get(alc2FailWithAliasKey)) != null) {
        opFallidas.add(alc2FailDto);
      } else {
        opFallidas = new ArrayList<Alc2FailDto>();
        opFallidas.add(alc2FailDto);
        fallidosPorTipoMap.put(alc2FailWithAliasKey, opFallidas);
      } // else
    } // for (Alc2FailDto alc2FailDTO :alc2FailDTOList)

    Fallido fallidoVenta;
    Fallido fallidoCompra;
    List<Fallido> fallidosList = new ArrayList<Fallido>();
    List<FallidoAlc> fallidosAlcList = new ArrayList<FallidoAlc>();

    BigDecimal saldoNeto;

    for (Map.Entry<Alc2FailWithAliasKey, List<Alc2FailDto>> entry : fallidosPorTipoMap.entrySet()) {
      LOG.debug("[createFallidos] Procesando entrada: " + entry.getKey().toString());

      fallidoCompra = null;
      fallidoVenta = null;
      saldoNeto = BigDecimal.ZERO;

      for (Alc2FailDto alc2FailDto : entry.getValue()) {
        LOG.debug("[createFallidos] Procesando alc: " + alc2FailDto.toString());

        if (alc2FailDto.getTipoOperacion().compareTo('C') == 0) {
          if (fallidoCompra == null) {
            fallidoCompra = new Fallido();
          }

          // Si los títulos son negativos es porque los datos son del sumatorio de liquidaciones parciales del alc,
          // por lo que no hay que tenerlo en cuenta para el cálculo del saldo neto
          if (alc2FailDto.getTitulos().signum() > 0) {
            saldoNeto = saldoNeto.subtract(alc2FailDto.getNeto());
          } // if

          generateFallido(alc2FailDto, fallidoCompra, fallidosAlcList);
        } else if (alc2FailDto.getTipoOperacion().compareTo('V') == 0) {
          if (fallidoVenta == null) {
            fallidoVenta = new Fallido();
          }

          // Si los títulos son negativos es porque los datos son del sumatorio de liquidaciones parciales del alc,
          // por lo que no hay que tenerlo en cuenta para el cálculo del saldo neto
          if (alc2FailDto.getTitulos().signum() > 0) {
            saldoNeto = saldoNeto.add(alc2FailDto.getNeto());
          } // if

          generateFallido(alc2FailDto, fallidoVenta, fallidosAlcList);
        } // else if (alc2FailDto.getTipoOperacion().compareTo('V') == 0)
      } // for (Alc2FailDto alc2FailDto : entry.getValue())

      if (fallidoCompra != null) {
        fallidoCompra.setSaldoNeto(saldoNeto);
        fallidosList.add(fallidoCompra);
        LOG.debug("[createFallidos] Creada compra afectada: " + fallidoCompra.toString());
      } // if

      if (fallidoVenta != null) {
        fallidoVenta.setSaldoNeto(saldoNeto);
        fallidosList.add(fallidoVenta);
        LOG.debug("[createFallidos] Creada venta fallida: " + fallidoVenta.toString());
      } // if
    } // for (Map.Entry<Alc2FailWithAliasKey, List<Alc2FailDto>> entry : fallidosPorTipoMap.entrySet())

    dao.save(fallidosList);
    fallidoAlcDao.save(fallidosAlcList);

    LOG.debug("[createFallidos] Fin ...");
  } // createFallidos

  /**
   * 
   * @param alc2FailDto
   * @param fallido
   * @param fallidosAlcList
   */
  private void generateFallido(Alc2FailDto alc2FailDto, Fallido fallido, List<FallidoAlc> fallidosAlcList)
      throws SIBBACBusinessException {
    LOG.debug("[generateFallido] Inicio ...");
    Integer estadoEntrecDesglosecamara;

    Boolean desgloseLiq = Boolean.FALSE;
    Boolean desgloseNoLiq = Boolean.FALSE;
    String referenciaS3;

    Object[][] sumLiqParciales = null;
    BigDecimal sumTitulosLiqParciales;
    BigDecimal sumEfectivoLiqParcial;

    FallidoAlc alcFallido;
    Boolean existsFallidoAlc = Boolean.FALSE;

    if (fallido.getCdAlias() == null) {
      fallido.setTpoper(alc2FailDto.getTipoOperacion());
      fallido.setFeejeliq(alc2FailDto.getFeejeliq());
      fallido.setFeopeliq(alc2FailDto.getFeopeliq());
      fallido.setCdAlias(alc2FailDto.getCdAlias());
      fallido.setDsAlias(alc2FailDto.getDsAlias());
      fallido.setCdIsin(alc2FailDto.getCdIsin());
      fallido.setDsIsin(alc2FailDto.getDsIsin());
      fallido.setCdCuentaCompensacion(alc2FailDto.getCdCuentaCompensacion());
      fallido.setCdCuentaLiquidacion(alc2FailDto.getCdCuentaLiquidacion());
    } // if (fallido.getCdAlias() == null)

    fallido.setTitulos(fallido.getTitulos().add(alc2FailDto.getTitulos()));
    fallido.setEfectivo(fallido.getEfectivo().add(alc2FailDto.getEfectivo()));

    // Si la referenciaS3 es nula es que el alc sin liquidar es de una cuenta propia.
    // Hay que buscar todos sus desgloses cámara, sumar los titulos liquidados, restarselos a los títulos del alc y
    // referenciar esas patas mercado con el fallido
    if (alc2FailDto.getReferenciaS3() == null) {
      Tmct0alc tmct0alc = tmct0alcDao.findByNuordenAndNbookingAndNucnfcltAndNucnfliq(alc2FailDto.getNuorden(),
                                                                                     alc2FailDto.getNbooking(),
                                                                                     alc2FailDto.getNucnfclt(),
                                                                                     alc2FailDto.getNucnfliq());
      for (Tmct0desglosecamara tmct0desglosecamara : tmct0alc.getTmct0desglosecamaras()) {
        estadoEntrecDesglosecamara = tmct0desglosecamara.getTmct0estadoByCdestadoentrec().getIdestado();

        if (estadoEntrecDesglosecamara > CLEARING.ENVIADO_FICHERO.getId()) {
          desgloseLiq = Boolean.FALSE;
          desgloseNoLiq = Boolean.FALSE;
          referenciaS3 = alcOrdenesBo.referenciaS3porDesglose(tmct0desglosecamara.getIddesglosecamara());
          sumTitulosLiqParciales = BigDecimal.ZERO;
          sumEfectivoLiqParcial = BigDecimal.ZERO;

          if (estadoEntrecDesglosecamara >= CLEARING.LIQUIDADA.getId()) {
            desgloseLiq = Boolean.TRUE;
          } else if (estadoEntrecDesglosecamara == CLEARING.LIQUIDADA_PARCIAL.getId()) {
            desgloseLiq = Boolean.TRUE;
            desgloseNoLiq = Boolean.TRUE;
          } else if (estadoEntrecDesglosecamara < CLEARING.LIQUIDADA.getId()) {
            desgloseNoLiq = Boolean.TRUE;
          } // else

          if (desgloseLiq) {
            sumLiqParciales = s3LiquidacionDao.findByIdFicheroAndCdestadoS3(referenciaS3, S3LIQ);

            sumTitulosLiqParciales = new BigDecimal(sumLiqParciales[0][0].toString());
            sumEfectivoLiqParcial = new BigDecimal(sumLiqParciales[0][1].toString());

            fallido.setTitulos(fallido.getTitulos().subtract(sumTitulosLiqParciales));
            fallido.setEfectivo(fallido.getEfectivo().subtract(sumEfectivoLiqParcial));
          } // if

          if (desgloseNoLiq) {
            alcFallido = new FallidoAlc();
            alcFallido.setTmct0alcId(new Tmct0alcId(alc2FailDto.getNbooking(), alc2FailDto.getNuorden(),
                                                    alc2FailDto.getNucnfliq(), alc2FailDto.getNucnfclt()));
            alcFallido.setReferenciaS3(referenciaS3);
            alcFallido.setTitulosFallidos(tmct0desglosecamara.getNutitulos().subtract(sumTitulosLiqParciales));
            alcFallido.setFallido_id(fallido);

            fallidosAlcList.add(alcFallido);
          } // if (desgloseNoLiq)
        } // if (tmct0estadoDesglosecamara > CLEARING.ENVIADO_FICHERO.getId())
      } // for (Tmct0desglosecamara tmct0desglosecamara : tmct0desglosecamaraList)
    } else {
      for (FallidoAlc alcFallidoTmp : fallidosAlcList) {
        if (alcFallidoTmp.getReferenciaS3().compareTo(alc2FailDto.getReferenciaS3()) == 0) {
          alcFallidoTmp.setTitulosFallidos(alcFallidoTmp.getTitulosFallidos().add(alc2FailDto.getTitulos()));
          existsFallidoAlc = Boolean.TRUE;
          break;
        } // if
      } // for (FallidoAlc alcFallidoTmp : alcsFallidosList)

      if (!existsFallidoAlc) {
        alcFallido = new FallidoAlc();
        alcFallido.setTmct0alcId(new Tmct0alcId(alc2FailDto.getNbooking(), alc2FailDto.getNuorden(),
                                                alc2FailDto.getNucnfliq(), alc2FailDto.getNucnfclt()));
        alcFallido.setReferenciaS3(alc2FailDto.getReferenciaS3());
        alcFallido.setTitulosFallidos(alc2FailDto.getTitulos());
        alcFallido.setFallido_id(fallido);
        fallidosAlcList.add(alcFallido);
      } // if (!existsFallidoAlc)
    } // else

    LOG.debug("[generateFallido] Fin ...");
  } // generateFallido

} // FallidoBo
