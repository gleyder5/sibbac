package sibbac.business.fallidos.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.ConstanteH47;


@Repository
public interface ConstanteH47Dao extends JpaRepository< ConstanteH47, Long > {

	ConstanteH47 findByCampo( String campo );

}
