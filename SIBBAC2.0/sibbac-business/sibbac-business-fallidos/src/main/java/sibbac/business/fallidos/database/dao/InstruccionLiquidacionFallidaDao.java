package sibbac.business.fallidos.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.InstruccionLiquidacionFallida;

@Repository
public interface InstruccionLiquidacionFallidaDao extends JpaRepository<InstruccionLiquidacionFallida, Long> {

  /**
   * Busca una instrucción de liquidación cuyo número de operación sea el especificado.
   * 
   * @param il Número de operación a buscar.
   * @return <code>sibbac.business.fallidos.database.model.InstruccionLiquidacionFallida - </code>Instrucción de
   *         liquidación fallida.
   * @author XI316153
   */
  @Query("SELECT ilFallida FROM InstruccionLiquidacionFallida ilFallida WHERE ilFallida.numeroOperacionEcc = :il")
  public InstruccionLiquidacionFallida findByNumeroOperacionEcc(@Param("il") String il);

} // InstruccionLiquidacionFallidaDao
