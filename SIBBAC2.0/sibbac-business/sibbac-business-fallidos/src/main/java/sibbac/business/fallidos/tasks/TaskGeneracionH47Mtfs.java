package sibbac.business.fallidos.tasks;

import java.math.BigInteger;
import java.util.Date;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fallidos.database.bo.OperacionesH47MtfBo;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FALLIDOS.NAME,
name = Task.GROUP_FALLIDOS.JOB_MTF,
interval = 1,
intervalUnit = IntervalUnit.DAY,
startTime = "07:00:00")
public class TaskGeneracionH47Mtfs  extends SIBBACTask{

  @Autowired
  OperacionesH47MtfBo generaMtfBo;
  
  @Autowired
  Tmct0menBo tmct0menBo;
  
  @Autowired
  Tmct0CnmvLogDao tmct0CnmvLogDao;
  
  @Value("${mtf.automatic.creation}")
  private String crearMTF;
  
  
  @Override
  public void execute() {
    
    LOG.debug("[" + new Date() + "][" + Task.GROUP_FALLIDOS.NAME + " : " + ":"
        + Task.GROUP_FALLIDOS.JOB_MTF + "] Init");

    Tmct0men tmct0men = null;
    TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
    TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
    TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
    
    try {
      // Solo se ejecuta cuando esta habilitada
      if (crearMTF.equals("S")) {
        tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.FALLIDO_GENERACIONH47_MTF, estadoIni, estadoExe);
        
        generaMtfBo.generaOperacionesMTFs();
        
        tmct0menBo.putEstadoMEN(tmct0men, estadoIni);       
      }else{
        LOG.debug("[" + new Date() + "][" + Task.GROUP_FALLIDOS.NAME + " : " + ":"
            + Task.GROUP_FALLIDOS.JOB_MTF + "] NO HABILITADA SU EJECUCION");
        
        // Creamos un registro en el log.
        Tmct0CnmvLog logProceso = new Tmct0CnmvLog();
        logProceso.setAuditUser("SISTEMA");
        logProceso.setAuditDate(new Date());
        logProceso.setProceso(Constantes.PROCESO_MTF);
        logProceso.setContadorAltas(BigInteger.ZERO);
        logProceso.setContadorBajas(BigInteger.ZERO);
        logProceso.setIncidencia('S');
        logProceso.setComentario("El parametro del sistema indica la NO ejecución del proceso.  Proceso Abortado - Contacte con IT ");
        
        
        this.tmct0CnmvLogDao.save(logProceso);        
        
      }


    }catch (SIBBACBusinessException e) {
      LOG.warn("[TaskActualizacionDesglose :: execute] Error intentando bloquear el semaforo ... " + e.getMessage());
      tmct0menBo.putEstadoMEN(tmct0men, estadoError);
    
    }catch (Exception e) {
      LOG.error("[" + Task.GROUP_FALLIDOS.NAME + " : " + ":"
          + Task.GROUP_FALLIDOS.JOB_MTF +  "] Error al generar las operaciones: " + e.getMessage());
      tmct0menBo.putEstadoMEN(tmct0men, estadoError);
    }

    LOG.debug("[" + new Date() + "][" + Task.GROUP_FALLIDOS.NAME + " : " + ":"
        + Task.GROUP_FALLIDOS.JOB_MTF + "] Fin");

  }
}
