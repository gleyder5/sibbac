package sibbac.business.fallidos.database.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.MovimientoFallido;

@Repository
public interface MovimientoFallidoDao extends JpaRepository<MovimientoFallido, Long> {

  /**
   * 
   * @param numero_operacion_fallida
   * @param cdtipo_alta
   * @param cdtipo_baja
   * @param alias
   * @param subcuenta
   * @param isin
   * @return
   */
  @Query("SELECT mf FROM MovimientoFallido mf WHERE mf.numero_operacion_fallida=:numero_operacion_fallida AND mf.cdtipo=:cdtipo_baja AND mf.aliasCliente=:alias AND mf.subcuenta=:subcuenta AND mf.isin=:isin AND EXISTS ( SELECT mf2 FROM MovimientoFallido mf2 WHERE mf2.numero_operacion_fallida=:numero_operacion_fallida AND mf2.cdtipo=:cdtipo_alta AND mf2.aliasCliente=:alias AND mf2.subcuenta=:subcuenta AND mf2.isin=:isin)")
  public List<MovimientoFallido> findBajasParciales(@Param("numero_operacion_fallida") String numero_operacion_fallida,
                                                    @Param("cdtipo_alta") String cdtipo_alta,
                                                    @Param("cdtipo_baja") String cdtipo_baja,
                                                    @Param("alias") String alias,
                                                    @Param("subcuenta") String subcuenta,
                                                    @Param("isin") String isin);

  /**
   * @param numero_operacion_fallida
   * @param cdtipo_alta
   * @param cdtipo_baja
   * @param alias
   * @param subcuenta
   * @param isin
   * @return
   * @author XI316153
   */
  @Query("SELECT mf FROM MovimientoFallido mf WHERE mf.numero_operacion_fallida=:numero_operacion_fallida AND mf.cdtipo=:cdtipo_alta AND mf.aliasCliente=:alias AND mf.subcuenta=:subcuenta AND mf.isin=:isin AND NOT EXISTS ( SELECT mf2 FROM MovimientoFallido mf2 WHERE mf2.numero_operacion_fallida=:numero_operacion_fallida AND mf2.cdtipo=:cdtipo_baja AND mf2.aliasCliente=:alias AND mf2.subcuenta=:subcuenta AND mf2.isin=:isin)")
  public MovimientoFallido findAltasSinBajas(@Param("numero_operacion_fallida") String numero_operacion_fallida,
                                             @Param("cdtipo_alta") String cdtipo_alta,
                                             @Param("cdtipo_baja") String cdtipo_baja,
                                             @Param("alias") String alias,
                                             @Param("subcuenta") String subcuenta,
                                             @Param("isin") String isin);

  /**
   * @param tipo
   * @param numeroOperacionFallida
   * @return
   * @author XI316153
   */
  @Query("SELECT mf FROM MovimientoFallido mf WHERE mf.cdtipo IN (:tipo) AND mf.numero_operacion_fallida = :numeroOperacionFallida")
  public List<MovimientoFallido> findByTipoMovimientoAndNumeroOperacionfallida(@Param("tipo") Collection<String> tipo,
                                                                               @Param("numeroOperacionFallida") String numeroOperacionFallida);

  /**
   * Obtiene los movimientos fallidos no contabilizados para los filtros especificados.
   * 
   * @param aliasCliente Identificador del alias.
   * @param isin Instrumento.
   * @param camara Camara.
   * @param numero_operacion_fallida Número de instrucción de liquidación.
   * @param tipo Lista de tipos de movimientos.
   * @return <code>java.util.List<MovimientoFallido> - </code>Lista de movimientos fallidos encontradas.
   * @author XI316153
   */
  @Query("SELECT mf FROM MovimientoFallido mf WHERE contabilizado=false AND cdtipo IN (:tipo) AND aliasCliente=:aliasCliente AND isin=:isin AND camara=:camara AND numero_operacion_fallida=:numero_operacion_fallida ORDER BY id ASC")
  public List<MovimientoFallido> findNoContabilizadosByFields(@Param("aliasCliente") String aliasCliente,
                                                              @Param("isin") String isin,
                                                              @Param("camara") String camara,
                                                              @Param("numero_operacion_fallida") String numero_operacion_fallida,
                                                              @Param("tipo") Collection<String> tipo);

  /**
   * Obtiene los movimientos fallidos no contabilizados para los filtros especificados.
   * 
   * @param aliasCliente Identificador del alias.
   * @param subcuenta Identificador de la subcuenta
   * @param isin Instrumento.
   * @param camara Camara.
   * @param numero_operacion_fallida Número de instrucción de liquidación.
   * @param tipo Lista de tipos de movimientos.
   * @return <code>java.util.List<MovimientoFallido> - </code>Lista de movimientos fallidos encontradas.
   * @author XI316153
   */
  @Query("SELECT mf FROM MovimientoFallido mf WHERE contabilizado=false AND cdtipo IN (:tipo) AND aliasCliente=:aliasCliente AND subcuenta=:subcuenta AND isin=:isin AND camara=:camara AND numero_operacion_fallida=:numero_operacion_fallida ORDER BY id ASC")
  public List<MovimientoFallido> findNoContabilizadosByFields(@Param("aliasCliente") String aliasCliente,
                                                              @Param("subcuenta") String subcuenta,
                                                              @Param("isin") String isin,
                                                              @Param("camara") String camara,
                                                              @Param("numero_operacion_fallida") String numero_operacion_fallida,
                                                              @Param("tipo") Collection<String> tipo);

  @Query("SELECT mf FROM MovimientoFallido mf"
         + " WHERE mf.contabilizado = :contabilizado AND mf.cdtipo IN (:tipo) ORDER BY mf.numero_operacion_fallida, mf.id")
  public List<MovimientoFallido> findByTipoMovimientoAndContabilizado(@Param("contabilizado") Boolean contabilizado,
                                                                      @Param("tipo") Collection<String> tipo);

  @Query("SELECT mf FROM MovimientoFallido mf"
         + " WHERE mf.contabilizado = :contabilizado AND mf.numero_operacion_fallida = :numero_operacion_fallida AND mf.cdtipo IN (:tipo) ORDER BY mf.id")
  public List<MovimientoFallido> findByTipoMovimientoContabilizadoAndIliqecc(@Param("contabilizado") Boolean contabilizado,
                                                                             @Param("numero_operacion_fallida") String numero_operacion_fallida,
                                                                             @Param("tipo") Collection<String> tipo);

} // MovimientoFallidoDao
