package sibbac.business.fallidos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.utils.FormatDataUtils;

/**
 * @version 1.0 Represanta una operacion MTF para enviar a la CNMV
 * @author Neoris
 */
public class OperacionMtfDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES ********************

  private static final long serialVersionUID = 1L;

  protected static final Logger LOG = LoggerFactory.getLogger(OperacionMtfDTO.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS ********************

  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private Short nucnfliq;
  private Date feejecuc;
  private Date hoejecuc;
  private Date feliquidacion;
  private Integer cdestadoasig;
  private Character sentido;
  private String cd_plataforma_negociacion;
  private String nurefexemkt;
  private String isin;
  private BigDecimal imtitulos;
  private BigDecimal imprecio;
  private BigDecimal imefectivo;
  private String cddivisa;
  private Long iddesglosecamara;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constructores ********************
  public OperacionMtfDTO(String nuorden, String nbooking, String nucnfclt, Short nucnfliq, Date feejecuc,
      Date hoejecuc, Date feliquidacion, Integer cdestadoasig, Character sentido, String cd_plataforma_negociacion,
      String nurefexemkt, String isin, BigDecimal imtitulos, BigDecimal imprecio, BigDecimal imefectivo,
      String cddivisa, Long iddesglosecamara) {
    super();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.feliquidacion = feliquidacion;
    this.cdestadoasig = cdestadoasig;
    this.sentido = sentido;
    this.cd_plataforma_negociacion = cd_plataforma_negociacion;
    this.nurefexemkt = nurefexemkt;
    this.isin = isin;
    this.imtitulos = imtitulos;
    this.imprecio = imprecio;
    this.imefectivo = imefectivo;
    this.cddivisa = cddivisa;
    this.iddesglosecamara = iddesglosecamara;
  }

  public OperacionMtfDTO(Object[] operacionMTFobj) {
    if (operacionMTFobj != null && operacionMTFobj.length == 15) {
      if (operacionMTFobj[0] != null)
        this.nuorden = String.valueOf(operacionMTFobj[0]).trim();
      if (operacionMTFobj[1] != null)
        this.nbooking = String.valueOf(operacionMTFobj[1]).trim();
      if (operacionMTFobj[2] != null)
        this.nucnfclt = String.valueOf(operacionMTFobj[2]).trim();
      if (operacionMTFobj[3] != null)
        this.nucnfliq = Short.valueOf(operacionMTFobj[3].toString());
      if (operacionMTFobj[4] != null)
        this.feejecuc = FormatDataUtils.convertStringToDate(operacionMTFobj[4].toString(),
            FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
      if (operacionMTFobj[5] != null)
        this.feliquidacion = FormatDataUtils.convertStringToDate(operacionMTFobj[5].toString(),
            FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
      if (operacionMTFobj[6] != null)
        this.cdestadoasig = Integer.valueOf(operacionMTFobj[6].toString());
      if (operacionMTFobj[7] != null)
        this.sentido = String.valueOf(operacionMTFobj[7]).charAt(0);
      if (operacionMTFobj[8] != null)
        this.cd_plataforma_negociacion = String.valueOf(operacionMTFobj[8]).trim();
      if (operacionMTFobj[9] != null)
        this.nurefexemkt = String.valueOf(operacionMTFobj[9]).trim();
      if (operacionMTFobj[10] != null)
        this.isin = String.valueOf(operacionMTFobj[10]).trim();
      if (operacionMTFobj[11] != null)
        this.imtitulos = new BigDecimal(operacionMTFobj[11].toString());
      if (operacionMTFobj[12] != null)
        this.imprecio = new BigDecimal(operacionMTFobj[12].toString());
      if (operacionMTFobj[13] != null)
        this.imefectivo = new BigDecimal(operacionMTFobj[13].toString());
      if (operacionMTFobj[14] != null)
        this.cddivisa = String.valueOf(operacionMTFobj[14]).trim();
      if (operacionMTFobj[15] != null)
        this.iddesglosecamara = new Long(operacionMTFobj[15].toString());
    }
  }

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @return the feejecuc
   */
  public Date getFeejecuc() {
    return feejecuc;
  }

  /**
   * @param feejecuc the feejecuc to set
   */
  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  /**
   * @return the hoejecuc
   */
  public Date getHoejecuc() {
    return hoejecuc;
  }

  /**
   * @param hoejecuc the hoejecuc to set
   */
  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  /**
   * @return the feliquidacion
   */
  public Date getFeliquidacion() {
    return feliquidacion;
  }

  /**
   * @param feliquidacion the feliquidacion to set
   */
  public void setFeliquidacion(Date feliquidacion) {
    this.feliquidacion = feliquidacion;
  }

  /**
   * @return the cdestadoasig
   */
  public Integer getCdestadoasig() {
    return cdestadoasig;
  }

  /**
   * @param cdestadoasig the cdestadoasig to set
   */
  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  /**
   * @return the sentido
   */
  public Character getSentido() {
    return sentido;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  /**
   * @return the cd_plataforma_negociacion
   */
  public String getCd_plataforma_negociacion() {
    return cd_plataforma_negociacion;
  }

  /**
   * @param cd_plataforma_negociacion the cd_plataforma_negociacion to set
   */
  public void setCd_plataforma_negociacion(String cd_plataforma_negociacion) {
    this.cd_plataforma_negociacion = cd_plataforma_negociacion;
  }

  /**
   * @return the nurefexemkt
   */
  public String getNurefexemkt() {
    return nurefexemkt;
  }

  /**
   * @param nurefexemkt the nurefexemkt to set
   */
  public void setNurefexemkt(String nurefexemkt) {
    this.nurefexemkt = nurefexemkt;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @return the imtitulos
   */
  public BigDecimal getImtitulos() {
    return imtitulos;
  }

  /**
   * @param imtitulos the imtitulos to set
   */
  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  /**
   * @return the imprecio
   */
  public BigDecimal getImprecio() {
    return imprecio;
  }

  /**
   * @param imprecio the imprecio to set
   */
  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  /**
   * @return the imefectivo
   */
  public BigDecimal getImefectivo() {
    return imefectivo;
  }

  /**
   * @param imefectivo the imefectivo to set
   */
  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  /**
   * @return the cddivisa
   */
  public String getCddivisa() {
    return cddivisa;
  }

  /**
   * @param cddivisa the cddivisa to set
   */
  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  /**
   * @return the iddesglosecamara
   */
  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  /**
   * @param iddesglosecamara the iddesglosecamara to set
   */
  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

}
