package sibbac.business.fallidos.utils;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R00;
import sibbac.pti.messages.an.R06;
import sibbac.pti.messages.an.R07;

public class FallidoHelper {

  private static final String CAMARA = "BME";
  public static final Logger LOG = LoggerFactory.getLogger(FallidoHelper.class);

  public static boolean isAnFallido(AN an) {

    boolean isFallido = false;

    List<PTIMessageRecord> anR06s = an.getAll(PTIMessageRecordType.R06);
    List<PTIMessageRecord> anR07s = an.getAll(PTIMessageRecordType.R07);

    if (anR06s != null && !anR06s.isEmpty()) {
      R06 r06 = (R06) anR06s.get(0);

      if (r06.getTitulosFallidosEnOP() != null && r06.getTitulosFallidosEnOP().compareTo(BigDecimal.ZERO) > 0) {
        LOG.debug("R06 fallidos: " + r06.getTitulosFallidosEnOP());
        isFallido = true;

      }
    }

    if (anR07s != null && !anR07s.isEmpty()) {

      for (PTIMessageRecord anR07 : anR07s) {
        R07 r07 = (R07) anR07;

        if (r07.getTitulosFallidosOPenIL() != null && r07.getTitulosFallidosOPenIL().compareTo(BigDecimal.ZERO) > 0) {
          LOG.debug("R07 fallidos: " + r07.getTitulosFallidosOPenIL());
          isFallido = true;

        }
      }
    }
    return isFallido;
  }

  // public static List< OperacionFallido > anToOperacionFallido( AN an ) {
  // List< PTIMessageRecord > anR00s = an.getAll( PTIMessageRecordType.R00 );
  // List< PTIMessageRecord > anR01s = an.getAll( PTIMessageRecordType.R01 );
  // List< PTIMessageRecord > anR02s = an.getAll( PTIMessageRecordType.R02 );
  // List< PTIMessageRecord > anR06s = an.getAll( PTIMessageRecordType.R06 );
  // List< PTIMessageRecord > anR07s = an.getAll( PTIMessageRecordType.R07 );
  //
  // OperacionFallido operacionFallido = null;
  // List< OperacionFallido > result = new ArrayList<>();
  //
  // R00 r00 = ( R00 ) anR00s.get( 0 );
  // R01 r01 = ( R01 ) anR01s.get( 0 );
  // R02 r02 = ( R02 ) anR02s.get( 0 );
  // R06 r06 = ( R06 ) anR06s.get( 0 );
  //
  // for ( PTIMessageRecord anR07 : anR07s ) {
  // R07 r07 = ( R07 ) anR07;
  // // campos r07
  // operacionFallido = new OperacionFallido();
  // operacionFallido.setTitulosOperacionIL( r07.getTitulosOPenIL() );
  // operacionFallido.setTitulosFallidosOperacionIL( r07.getTitulosFallidosOPenIL() );
  // operacionFallido.setNumeroOperacionDcv( r07.getNumeroOperacionDCV() );
  // operacionFallido.setNumeroOperacionIlEcc( r07.getNumeroOperacionILdeLaECC() );
  // operacionFallido.setEfectivoOperacionIL( r07.getEfectivoOPenIL() );
  // operacionFallido.setEstadoIL( r07.getEstadoIL() );
  //
  // // campos r06
  // operacionFallido.setTitulosFallidosOperacion( r06.getTitulosFallidosEnOP() );
  // operacionFallido.setSituacionOperacion( r06.getSituacionOperacion() );
  //
  // // campos r01
  // operacionFallido.setNumeroOperacion( r01.getNumeroOperacion() );
  // operacionFallido.setNumeroOperacionInicial( r01.getNumeroOperacionInicial() );
  // operacionFallido.setNumeroOperacionPrevia( r01.getNumeroOperacionPrevia() );
  // operacionFallido.setSentido( r01.getSentido() );
  // operacionFallido.setPrecio( r01.getPrecio() );
  // operacionFallido.setFeejecuc( r01.getFechaContratacion() );
  // operacionFallido.setSttlDt( r01.getFechaLiquidacionTeorica() );
  //
  // // campos r00
  // operacionFallido.setIsin( r00.getCodigoDeValor() );
  // operacionFallido.setCuentaLiquidacion( r00.getCuentaLiquidacion() );
  //
  // // campos r02
  // if ( r02 != null ) {
  // operacionFallido.setNumeroEjecucionMercado( r02.getNumeroDeEjecucionDeMercado().toPlainString() );
  // operacionFallido.setNumeroOrdenMercado( r02.getNumeroDeLaOrdenDeMercado().toPlainString() );
  // operacionFallido.setSegmento( r02.getSegmento() );
  // }
  //
  // operacionFallido.setCamara( CAMARA );
  // operacionFallido.setLiquidado( false );
  //
  // result.add( operacionFallido );
  //
  // }
  //
  // return result;
  // }

  public static String getCodigoCuentaCompensacion(AN anFallido) {
    List<PTIMessageRecord> anR00s = anFallido.getAll(PTIMessageRecordType.R00);
    R00 r00 = (R00) anR00s.get(0);
    String result = r00.getCuentaDeCompensacion();
    return result;
  }

  public static boolean isValidAn(AN an) throws PTIMessageException {
    List<PTIMessageRecord> anR00s = an.getAll(PTIMessageRecordType.R00);
    List<PTIMessageRecord> anR01s = an.getAll(PTIMessageRecordType.R01);
    List<PTIMessageRecord> anR06s = an.getAll(PTIMessageRecordType.R06);
    // List< PTIMessageRecord > anR02s = an.getAll( PTIMessageRecordType.R02 );
    // List< PTIMessageRecord > anR05s = an.getAll( PTIMessageRecordType.R05 );

    boolean result = false;

    if (anR00s != null && !anR00s.isEmpty() && anR00s.size() == 1 && anR01s != null && !anR01s.isEmpty()
        && anR01s.size() == 1 && anR06s != null && !anR06s.isEmpty() && anR06s.size() == 1) {

      result = true;
    }

    // }else if ( anR02s != null && !anR02s.isEmpty() && anR02s.size() == 1
    // && anR05s != null && !anR05s.isEmpty() && anR05s.size() == 1 ){
    //
    // result = false;
    // }else{
    // LOG.error("Mensaje AN PTI incorrecto: {} ", an.toPTIFormat());
    // throw new PTIMessageException("Numero de registros de AN incorrecto");
    // }

    return result;
  }
}
