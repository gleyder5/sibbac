package sibbac.business.fallidos.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.dto.SegmentoDTO;
import sibbac.business.wrappers.database.dao.Tmct0SegmentosCamaraDao;
import sibbac.business.wrappers.database.model.Tmct0SegmentosCamara;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * @author XI316153
 */
@SIBBACService
public class SIBBACServiceSegmento implements SIBBACServiceBean {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Referencia para la inserción de logs. */
	protected static final Logger	LOG								= LoggerFactory.getLogger( SIBBACServiceSegmento.class );

	/** Comando para el listado de los segmentos por camara. */
	private static final String		CMD_LIST_SEGMENTO_POR_CAMARA	= "listSegmentoPorCamara";

	/** Filtro necesario para el listado de Segmentos. */
	private static final String		PARAM_ID_CAMARA					= "camaraId";

	/** Contenido de la respuesta para el comando CMD_LIST. */
	private static final String		RESULT_LIST_SEGMENTO_POR_CAMARA	= "listadoSegmentoPorCamara";

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	@Autowired
	private Tmct0SegmentosCamaraDao	tmct0SegmentosCamaraDao;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public SIBBACServiceSegmento() {
	} // SIBBACServiceSegmento

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.debug( "[SIBBACServiceSegmento :: process] Processing a web request ..." );

		final String command = webRequest.getAction();
		LOG.debug( "[SIBBACServiceSegmento :: process] Command: [{}]", command );

		final WebResponse webResponse = new WebResponse();

		final Map< String, String > filtros = webRequest.getFilters();

		List< Tmct0SegmentosCamara > listaValores = null;
		List< SegmentoDTO > listaSegmento = null;

		switch ( command ) {
			case CMD_LIST_SEGMENTO_POR_CAMARA:
				if ( MapUtils.isNotEmpty( filtros ) ) {
					listaValores = tmct0SegmentosCamaraDao.findByCamara( NumberUtils.createLong( filtros.get( PARAM_ID_CAMARA ) ) );

					final Map< String, List< SegmentoDTO >> valores = new HashMap< String, List< SegmentoDTO >>();
					listaSegmento = new ArrayList< SegmentoDTO >();

					for ( Tmct0SegmentosCamara valor : listaValores ) {
						listaSegmento.add( new SegmentoDTO( valor ) );
					} // for

					valores.put( RESULT_LIST_SEGMENTO_POR_CAMARA, listaSegmento );

					webResponse.setResultados( valores );
				} else {
					webResponse.setError( command + " needs the camera filter." );
				} // else
				break;
			default:
				webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
				break;
		} // switch

		return webResponse;
	} // process

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LIST_SEGMENTO_POR_CAMARA );
		return commands;
	} // getAvailableCommands

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	} // getFields

} // SIBBACServiceSegmento
