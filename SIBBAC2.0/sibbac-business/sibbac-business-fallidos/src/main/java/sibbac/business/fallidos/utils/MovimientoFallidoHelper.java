package sibbac.business.fallidos.utils;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import sibbac.business.fallidos.database.bo.TipoMovimientosFallidosBo;
import sibbac.business.fallidos.database.model.MovimientoFallido;
import sibbac.business.fallidos.dto.MovimientoFallidoDTO;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;


public class MovimientoFallidoHelper {

	public static MovimientoFallidoDTO movimientoFallidoToDTO( MovimientoFallido movimientoFallido ) {
		MovimientoFallidoDTO movFallidoDTO = new MovimientoFallidoDTO();
		movFallidoDTO.setId( movimientoFallido.getId() );
		movFallidoDTO.setAlias( movimientoFallido.getAliasCliente() );
		movFallidoDTO.setCamara( movimientoFallido.getCamara() );
		movFallidoDTO.setDescripcion( movimientoFallido.getDescripcion() );
		movFallidoDTO.setDescripcionAlias( movimientoFallido.getDescrali() );
		movFallidoDTO.setDescripcionIsin( movimientoFallido.getNbvalor() );
		movFallidoDTO.setHolder( movimientoFallido.getHolder() );
		movFallidoDTO.setIsin( movimientoFallido.getIsin() );
		movFallidoDTO.setSaldoAfectado( movimientoFallido.getSaldoAfectado() );
		movFallidoDTO.setSegmento( movimientoFallido.getSegmento() );
		movFallidoDTO.setSentido( movimientoFallido.getSentido() );
		movFallidoDTO.setSubcuenta( movimientoFallido.getSubcuenta() );
		// FIXME cambiar cuando se meta la propiedad en la entity
		movFallidoDTO.setManual( false );
		return movFallidoDTO;
	}

	public static Tmct0MovimientoCuentaAlias movimientoFallidoToMovimientoCuentaAlias( MovimientoFallido movimientoFallido, String cdCuentaLiquidacion ) {

		Character sentido = null;
		Tmct0MovimientoCuentaAlias movimientoCuentaAlias = new Tmct0MovimientoCuentaAlias();

		movimientoCuentaAlias.setCdaliass( movimientoFallido.getAliasCliente() );
		movimientoCuentaAlias.setCdoperacion( movimientoFallido.getCdtipo() );
		movimientoCuentaAlias.setImefectivo( movimientoFallido.getSaldoAfectado() );
		movimientoCuentaAlias.setIsin( movimientoFallido.getIsin() );
		movimientoCuentaAlias.setMercado( movimientoFallido.getCamara() );
		movimientoCuentaAlias.setSettlementdate( movimientoFallido.getSttlDt() );

		if ( movimientoFallido.getTitulosAfectados() != null && movimientoFallido.getTitulosAfectados().compareTo( BigDecimal.ZERO ) > 0 ) {
			// FIXME constantes
			sentido = 'E';
		} else if ( movimientoFallido.getTitulosAfectados() != null
				&& movimientoFallido.getTitulosAfectados().compareTo( BigDecimal.ZERO ) < 0 ) {
			sentido = 'S';
		} else {
			if ( movimientoFallido.getSentido() == '1' ) {
				sentido = 'E';
			} else {
				sentido = 'S';
			}
		}

		if ( movimientoFallido.getSaldoAfectado() != null && movimientoFallido.getSaldoAfectado().compareTo( BigDecimal.ZERO ) > 0 ) {
			// FIXME constantes
			sentido = 'E';
		} else if ( movimientoFallido.getSaldoAfectado() != null && movimientoFallido.getSaldoAfectado().compareTo( BigDecimal.ZERO ) < 0 ) {
			sentido = 'S';
		} else {
			if ( movimientoFallido.getSentido() == '1' ) {
				sentido = 'E';
			} else {
				sentido = 'S';
			}
		}

		movimientoCuentaAlias.setSignoanotacion( sentido );
		movimientoCuentaAlias.setTitulos( movimientoFallido.getTitulosAfectados() );
		movimientoCuentaAlias.setTradedate( movimientoFallido.getTradeDt() );
		movimientoCuentaAlias.setRefmovimiento( movimientoFallido.getNumero_operacion_fallida() );
		
		movimientoCuentaAlias.setCdcodigocuentaliq(cdCuentaLiquidacion);

		return movimientoCuentaAlias;
	}

//	public static List< Tmct0MovimientoCuentaAlias > listaMovimientoFallidoToListaMovimientoCuentaAlias(
//			List< MovimientoFallido > movimientosFallidos ) {
//		Tmct0MovimientoCuentaAlias movimientoCuentaAlias = null;
//		List< Tmct0MovimientoCuentaAlias > result = new ArrayList<>();
//		if ( movimientosFallidos != null && !movimientosFallidos.isEmpty() ) {
//			for ( MovimientoFallido movimientoFallido : movimientosFallidos ) {
//				movimientoCuentaAlias = movimientoFallidoToMovimientoCuentaAlias( movimientoFallido );
//				result.add( movimientoCuentaAlias );
//			}
//		}
//
//		return result;
//	}

	public static MovimientoFallido createMovimientoFallido( String cdtipo, BigDecimal titulosAfectados, BigDecimal saldoAfectado,
			String holder, String nbholder, String alias, String subcuenta, String descrali, String isin, String nbvalor, String camara,
			String segmento, char sentido, String numero_operacion_fallida, Timestamp trxTm, Date sttlDt, Date tradeDt,
			TipoMovimientosFallidosBo tmct0tipomovimientofallidoBo ) {
		MovimientoFallido result = new MovimientoFallido( cdtipo, titulosAfectados, saldoAfectado, holder, /* nbholder, */alias,
				subcuenta, descrali, isin, nbvalor, camara, segmento, sentido, numero_operacion_fallida, trxTm, sttlDt, tradeDt,
				tmct0tipomovimientofallidoBo );
		return result;
	}

	// public static MovimientoFallido afectacionToMovimientoFallido(Afectacion afectacion){
	// MovimientoFallido movFallido = new MovimientoFallido();
	// movFallido.setAliasCliente(afectacion.getCdalias());
	// movFallido.setCamara(afectacion.get);
	// movFallido.setCdtipo(cdtipo);
	// movFallido.setHolder(holder);
	// movFallido.setIsin(isin);
	// movFallido.setDescrali(afectacion.get);
	// return movFallido;
	// }
}
