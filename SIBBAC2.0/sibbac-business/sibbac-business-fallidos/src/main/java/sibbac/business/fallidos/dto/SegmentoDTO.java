package sibbac.business.fallidos.dto;


import java.io.Serializable;

import sibbac.business.wrappers.database.model.Tmct0SegmentosCamara;


/**
 * @version 1.0
 * @author XI316153
 */
public class SegmentoDTO implements Serializable {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Identificador para la serialización de objetos. */
	private static final long	serialVersionUID	= -507365729046681997L;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	/** Identificador. */
	private Integer				id;

	/** Código. */
	private String				codigo;

	/** Descripción. */
	private String				descripcion;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public SegmentoDTO() {
	} // SegmentoDTO

	/**
	 * Constructor II. Crea un objeto a partir de los datos del objeto <code>Tmct0CamaraCompensacion</code> recibido.
	 */
	public SegmentoDTO( Tmct0SegmentosCamara segmento ) {
		this.setId( segmento.getIdSegmentoCamara() );
		this.setCodigo( segmento.getCdCodigo() );
		this.setDescripcion( segmento.getNbDescripcion() );
	} // SegmentoDTO

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IsinDTO [id=" + id + ", codigo=" + codigo + ", descripcion=" + descripcion + "]";
	} // toString

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

	/**
	 * @param id the id to set
	 */
	public void setId( Integer id ) {
		this.id = id;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

} // SegmentoDTO
