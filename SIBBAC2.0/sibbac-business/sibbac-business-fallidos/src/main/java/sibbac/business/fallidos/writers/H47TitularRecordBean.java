package sibbac.business.fallidos.writers;


import java.math.BigDecimal;

import sibbac.business.fallidos.database.model.TitularesH47;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.RecordBean;


public class H47TitularRecordBean extends RecordBean {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8898326482883307076L;
	/**
	 * 
	 */
	private String				icutireg;
	private String				icuclient;
	private String				icclient;
	private String				icdclient;
	private String				icdnombr;
	private String				icdapel1;
	private String				icdapel2;
	private String				icutipor;
	private String				icdident;
	private String				icdnombre;
	private String				icdap1re;
	private String				icdap2re;
	private String				icvcanti;

	public H47TitularRecordBean() {
		super();
	}

	public H47TitularRecordBean( int posicion, TitularesH47 titular ) {
		try {
			this.icutireg = StringHelper.formatBigDecimal( new BigDecimal( String.valueOf( posicion ) ), 2, 0 );
			this.icuclient = titular.getTpsocied().toString();
			this.icclient = titular.getTpidenti().toString();
			this.icdclient = StringHelper.padStringWithSpacesRight( titular.getIdentificacion(), 40 );
			this.icdnombr = StringHelper.padStringWithSpacesRight( titular.getNombre(), 60 );
			this.icdapel1 = StringHelper.padStringWithSpacesRight( titular.getApellido1(), 40 );
			this.icdapel2 = StringHelper.padStringWithSpacesRight( titular.getApellido2(), 40 );
			if ( titular.getRepTpidenti() != null ) {
				this.icutipor = titular.getRepTpidenti().toString();
			} else {
				this.icutipor = StringHelper.returnNWhiteSpaces( 1 );
			}
			if ( titular.getRepIdentificacion() != null ) {
				this.icdident = StringHelper.padStringWithSpacesRight( titular.getRepIdentificacion(), 40 );
			} else {
				this.icdident = StringHelper.returnNWhiteSpaces( 40 );
			}
			if ( titular.getRepNombre() != null ) {
				this.icdnombre = StringHelper.padStringWithSpacesRight( titular.getRepNombre(), 60 );
			} else {
				this.icdnombre = StringHelper.returnNWhiteSpaces( 60 );
			}
			if ( titular.getRepApellido1() != null ) {
				this.icdap1re = StringHelper.padStringWithSpacesRight( titular.getRepApellido1(), 40 );
			} else {
				this.icdap1re = StringHelper.returnNWhiteSpaces( 40 );
			}
			if ( titular.getRepApellido2() != null ) {
				this.icdap2re = StringHelper.padStringWithSpacesRight( titular.getRepApellido2(), 40 );
			} else {
				this.icdap2re = StringHelper.returnNWhiteSpaces( 40 );
			}
			this.icvcanti = StringHelper.formatBigDecimal( BigDecimal.ZERO, 12, 5 );
		} catch ( Exception e ) {
			// FIXME tipo excepcion
		}
	}

	public H47TitularRecordBean( int posicion, Tmct0CnmvTitulares titular,   Tmct0CnmvTitulares representante) throws Exception {
    try {
      this.icutireg = StringHelper.formatBigDecimal( new BigDecimal( String.valueOf( posicion ) ), 2, 0 );
      this.icuclient = titular.getTipoPersona().toString();
      this.icclient = titular.getTpidenti().toString();
      this.icdclient = StringHelper.padStringWithSpacesRight( titular.getIdentificacion(), 40 );
      this.icdnombr = StringHelper.padStringWithSpacesRight( titular.getNombre(), 60 );
      // Hay campos que pueden ser nuloas
      if (titular.getApellido1() != null){
        this.icdapel1 = StringHelper.padStringWithSpacesRight( titular.getApellido1(), 40 );  
      }else{
        this.icdapel1 = StringHelper.returnNWhiteSpaces( 40 );
      }
      
      if (titular.getApellido2() != null){
        this.icdapel2 = StringHelper.padStringWithSpacesRight( titular.getApellido2(), 40 );  
      }else{
        this.icdapel2 = StringHelper.returnNWhiteSpaces( 40 );
      }

      if ( representante != null ) {
        this.icutipor = representante.getTpidenti().toString();
        this.icdident = StringHelper.padStringWithSpacesRight( representante.getIdentificacion(), 40 );
        this.icdnombre = StringHelper.padStringWithSpacesRight( representante.getNombre(), 60 );
        
        if (representante.getApellido1() != null){
          this.icdap1re = StringHelper.padStringWithSpacesRight( representante.getApellido1(), 40 );  
        }else{
          this.icdap1re = StringHelper.returnNWhiteSpaces( 40 );
        }
        if (representante.getApellido2() != null){
          this.icdap2re = StringHelper.padStringWithSpacesRight( representante.getApellido2(), 40 );  
        }else{
          this.icdap2re = StringHelper.returnNWhiteSpaces( 40 );
        }
  
      } else {
        this.icutipor = StringHelper.returnNWhiteSpaces( 1 );
        this.icdident = StringHelper.returnNWhiteSpaces( 40 );
        this.icdnombre = StringHelper.returnNWhiteSpaces( 60 );
        this.icdap1re = StringHelper.returnNWhiteSpaces( 40 );
        this.icdap2re = StringHelper.returnNWhiteSpaces( 40 );        
      }

      // Inicialmente se rellena siempre con 0 pero si es el ultimo se tiene que actualizar.
      this.icvcanti = StringHelper.formatBigDecimal( BigDecimal.ZERO, 12, 5 );
    } catch ( Exception e ) {
       throw e;
    }
  }
	
	public String getIcutireg() {
		return icutireg;
	}

	public void setIcutireg( String icutireg ) {
		this.icutireg = icutireg;
	}

	public String getIcuclient() {
		return icuclient;
	}

	public void setIcuclient( String icuclient ) {
		this.icuclient = icuclient;
	}

	public String getIcclient() {
		return icclient;
	}

	public void setIcclient( String icclient ) {
		this.icclient = icclient;
	}

	public String getIcdclient() {
		return icdclient;
	}

	public void setIcdclient( String icdclient ) {
		this.icdclient = icdclient;
	}

	public String getIcdnombr() {
		return icdnombr;
	}

	public void setIcdnombr( String icdnombr ) {
		this.icdnombr = icdnombr;
	}

	public String getIcdapel1() {
		return icdapel1;
	}

	public void setIcdapel1( String icdapel1 ) {
		this.icdapel1 = icdapel1;
	}

	public String getIcdapel2() {
		return icdapel2;
	}

	public void setIcdapel2( String icdapel2 ) {
		this.icdapel2 = icdapel2;
	}

	public String getIcutipor() {
		return icutipor;
	}

	public void setIcutipor( String icutipor ) {
		this.icutipor = icutipor;
	}

	public String getIcdident() {
		return icdident;
	}

	public void setIcdident( String icdident ) {
		this.icdident = icdident;
	}

	public String getIcdnombre() {
		return icdnombre;
	}

	public void setIcdnombre( String icdnombre ) {
		this.icdnombre = icdnombre;
	}

	public String getIcdap1re() {
		return icdap1re;
	}

	public void setIcdap1re( String icdap1re ) {
		this.icdap1re = icdap1re;
	}

	public String getIcdap2re() {
		return icdap2re;
	}

	public void setIcdap2re( String icdap2re ) {
		this.icdap2re = icdap2re;
	}

	public String getIcvcanti() {
		return icvcanti;
	}

	public void setIcvcanti( String icvcanti ) {
		this.icvcanti = icvcanti;
	}

	@Override
	public String toString() {
		return icutireg + icuclient + icclient + icdclient + icdnombr + icdapel1 + icdapel2 + icutipor + icdident + icdnombre + icdap1re
				+ icdap2re + icvcanti;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
