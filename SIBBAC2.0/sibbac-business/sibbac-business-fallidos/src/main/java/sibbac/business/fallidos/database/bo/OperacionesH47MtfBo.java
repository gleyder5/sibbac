package sibbac.business.fallidos.database.bo;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.Tmct0CnmvDesglosesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.dto.DesgloseMtfDto;
import sibbac.business.fallidos.dto.OperacionMtfDTO;
import sibbac.business.fallidos.threads.ProcesaOperacionesMTFDiaRunnable;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.common.utils.DateHelper;


/**
 * Clase de negocios para generar operaciones de MTF’s de forma automatica..
 */
@Service
public class OperacionesH47MtfBo {

  
	  /** The Constant LOG. */
	  private static final Logger LOG = LoggerFactory.getLogger(OperacionesH47MtfBo.class);
	
	  /** Log de la ejecucion */
	  public Tmct0CnmvLog tmct0CnmvLog;  
	  
	   /**
     * @return the tmct0CnmvLog
     */
    public Tmct0CnmvLog getTmct0CnmvLog() {
      return tmct0CnmvLog;
    }


    /**
     * @param tmct0CnmvLog the tmct0CnmvLog to set
     */
    public void setTmct0CnmvLog(Tmct0CnmvLog tmct0CnmvLog) {
      this.tmct0CnmvLog = tmct0CnmvLog;
    }



    /** Descripcion de todos los errores */
	  private StringBuilder sbComentario = new StringBuilder();
	  	  
	  
	  @Value("${days.mtf.creation}")
	  private int iDiasEjecucion;

	  
	  @Value("${valid.ccp.mtf.creation}")
	  private String sCamarasCompensacion;
	  
	  
	  @Value("${excludedmarket.mtf.sending}")
    private String sCamarasExcluidasEnvio;


   @Value("${identif_sv_mtf_creation}")
    private String sIdentifSv;

	  
	  @Autowired
	  Tmct0CnmvDesglosesDao tmct0CnmvDesglosesDao;

	  
	   @Autowired
	   Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;
	  
	  @Autowired
	  Tmct0estadoDao tmct0estadoDao;

	  @Autowired
	  ProcesaOperacionH47MtfBo  procesaOperacionH47MtfBo;
	  
	  @Autowired
	  Tmct0CnmvLogDao tmct0CnmvLogDao;
	  
	  
	/**
	 * Genera las operaciones MTFs de forma automatica y por dias mediante hilos
	 * de ejecucion Metodo al que se llamara desde la pantalla y desde la task	 * para las MTFs
	 *
	 * @throws Exception
	 */
	public void generaOperacionesMTFs() throws Exception {

		try {

			// Inicializaciones
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			tmct0CnmvLog = new Tmct0CnmvLog();
			tmct0CnmvLog.setAuditUser("SISTEMA");
			tmct0CnmvLog.setAuditDate(new Date());
			tmct0CnmvLog.setProceso(Constantes.PROCESO_MTF);
			tmct0CnmvLog.setContadorAltas(BigInteger.ZERO);
			tmct0CnmvLog.setContadorBajas(BigInteger.ZERO);
			tmct0CnmvLog.setIncidencia('N');

			 
			// Se obtienen los dias de ejecución:
			// Se obtienen a partir del parametro days.mtf.creation como la resta del dia actual menos el numero de dias del parametro.
			Date dFechaFin = formatoFecha.parse(formatoFecha.format(new Date()));
			Date dFechaInicio = DateHelper.restaDias(dFechaFin, iDiasEjecucion)		; 
			List<Date> lDiasEjecucion = DateHelper.getListaFechas(dFechaInicio, dFechaFin);
			
			
			//TODO quitar solo para prueba de dia con datos.
	   // lDiasEjecucion.add(formatoFecha.parse("20/01/2017"));
			
			ExecutorService executor = Executors.newFixedThreadPool(lDiasEjecucion.size());
			// Se ejecuta un hilo con las operaciones de cada dia.
			 for (int i = 0; i < lDiasEjecucion.size(); i++) {
				 ProcesaOperacionesMTFDiaRunnable   runnable = new ProcesaOperacionesMTFDiaRunnable (this, lDiasEjecucion.get(i));
				 executor.execute(runnable);
			 }
			 
			 // Se eliminan los hilos.
	      try {
	          executor.shutdown();
	          executor.awaitTermination(2, TimeUnit.HOURS);
	        } catch (InterruptedException e) {
	        	  LOG.error("generaOperacionesMTFs - Error de la ejecucion de los hilos  " + e.getMessage());
	            sbComentario.append("  Error en la ejecucion de los hilos. Proceso Abortado - Contacte con IT : Error:  ").append(e.getMessage());
	        }
			 
       // Cuando se finaliza la ejecucion de todos los hilos se registra en el log la ejecucion
	      if (sbComentario.length() > 0){
	        tmct0CnmvLog.setIncidencia('S');
	        if (sbComentario.length() > 1000){
	           tmct0CnmvLog.setComentario(sbComentario.toString().substring(0,999));	          
	        }else{
	          tmct0CnmvLog.setComentario(sbComentario.toString());  
	        }
 
	      }else{
	        tmct0CnmvLog.setIncidencia('N');
	      }
        this.tmct0CnmvLogDao.save(tmct0CnmvLog);
	      
	      
		} catch (Exception e) {
		      LOG.error("generaOperacionesMTFs - Se produjo un error  " + e.getMessage());
		      sbComentario.append("  Error en la ejecucion generaOperacionesMTFs:  Proceso Abortado - Contacte con IT : Error:").append(e.getMessage());
		      tmct0CnmvLog.setIncidencia('S');
		      tmct0CnmvLog.setComentario(sbComentario.toString().substring(0,1000));
		      this.tmct0CnmvLogDao.save(tmct0CnmvLog);
		      throw e;
		}
	}

	  
		/**
		 * Genera las operaciones MTFs del dia recibido por parametro
		 *
		 *
		 * @throws Exception
		 */
		public void generaOperacionesDiaMTFs( Date fechaEjecucion)  {
		    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
		    try {
				LOG.debug("Inicio generaOperacionesDiaMTFs para el dia " + formatoFecha.format(fechaEjecucion).toString() );
		    	// Se obtienen las camaras para las que son validas los movimientos. 
		    	// Se trata de un valor definido en el properties con los codigos separados por comas
		    	List<String> lCamaras = new ArrayList<String>();
		    	StringTokenizer st = new StringTokenizer(sCamarasCompensacion,",");
		    	while (st.hasMoreElements()) {
		    		lCamaras.add((String) st.nextElement());
				}
		    	
		    	// Se obtiene el estado PTE de enviar
		    	Tmct0estado  estadoPte   = tmct0estadoDao.findOne(EstadosEnumerados.H47.PTE_ENVIAR.getId());
				
			
				// Se obtiene la lista de desgloses a procesar para ese dia
				List<OperacionMtfDTO> lDesgloses2 = tmct0CnmvDesglosesDao.findDesglosesProcesoAutomaticoMTFsDtos(fechaEjecucion, lCamaras);
				
				
				//Se procesa cada uno de los desgloses.
				for (OperacionMtfDTO operacionMtfDTO : lDesgloses2) {
  				try{
  				  procesaOperacionH47MtfBo.trataOperacionMtfDTO(operacionMtfDTO);
  		    } catch (Exception e) {
  		      String sError = " - ERROR: Excepcion no controlada al procesar los datos del desglose: "
                + " Nuorden - " + operacionMtfDTO.getNuorden()  + " - Nbooking - "    + operacionMtfDTO.getNbooking()   + " - Nucnfclt - "  + operacionMtfDTO.getNucnfclt()
                + " - Nucnfliq -  "     + operacionMtfDTO.getNucnfliq() + " - Iddesglosecamara  " + operacionMtfDTO.getIddesglosecamara() + " - Ejecucion  " + operacionMtfDTO.getNurefexemkt()
                + " " + e.getMessage(); 
  		      LOG.error(sError); 
  		      sbComentario.append(sError);
  		      
  		    }	
				}
				LOG.debug("Fin generaOperacionesDiaMTFs para el dia " + formatoFecha.format(fechaEjecucion).toString() );				
			} catch (Exception e) {
			  String sError = " Error  generaOperacionesDiaMTFs para el dia " + formatoFecha.format(fechaEjecucion).toString() + " Proceso Abortado para los datos de este día: - Error  " + e.getMessage();
			    LOG.error(sError);
			    sbComentario.append(sError);
			}
		}
	
    /**
     * Actualiza las operaciones que tienen el mismo valor en titulos ejecucion y desglose y su estado es menor al pendiente de enviar
     * 
     *
     * 
     */
	  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class })
		public void actualizaEjecucionesPendientes (){
      try {
        // Se obtiene el estado PTE de enviar
        Tmct0estado  estadoPte   = tmct0estadoDao.findOne(EstadosEnumerados.H47.PTE_ENVIAR.getId());     
   

        // Se obtienen las camaras para las que se excluye el envio
        // Se trata de un valor definido en el properties con los codigos separados por comas

        
        // Optenemos los pendientes de envio no actulizados.
        List <Tmct0CnmvEjecuciones> listEjecucionesActualizar = this.tmct0CnmvEjecucionesDao.findPendientesActualizar(EstadosEnumerados.H47.PTE_ENVIAR.getId()  )  ;        
        for (Tmct0CnmvEjecuciones tmct0CnmvEjecuciones : listEjecucionesActualizar) {
          tmct0CnmvEjecuciones.setCdEstadoEnvio(estadoPte);
          tmct0CnmvEjecucionesDao.save(tmct0CnmvEjecuciones);
       }        
      } catch (Exception e) {
        
        LOG.error(" actualizaEjecucionesPendientes  - Error: Excepcion no controlada  " + e.getMessage()); 

      }		  
		}

	  
	  
    public  List<String> obtenerCamarasExcluidas(){
    
      List<String> lCamaras = new ArrayList<String>();
      try {

        StringTokenizer st = new StringTokenizer(sCamarasExcluidasEnvio,",");
        while (st.hasMoreElements()) {
          lCamaras.add((String) st.nextElement());
        }
      } catch (Exception e) {
        LOG.error(" obtenerCamarasExcluidas al procesar el valor "  + sCamarasExcluidasEnvio + " - Error: Excepcion no controlada  " + e.getMessage()); 
      }

      return lCamaras;
    }

    /**
     * Descompone una ejecucion en la lista de desgloses con la información de sus titulares y representates asi como la pertenencia de los titulos
     * 
     * @param Tmct0CnmvEjecuciones Entidad de la ejecucion que se quiere descomponer
     * @return List<DesgloseMtfDto> lista de los desgloses de la ejecucioon con los titulares descompuestos.
     */
    public List<DesgloseMtfDto> getDesglosesEjecucion (Tmct0CnmvEjecuciones entEjecucion){
      
      List<DesgloseMtfDto> listaDesglosesDto = new ArrayList<DesgloseMtfDto>();
      
      List<Tmct0CnmvDesgloses> listDesglosesEntidad = this.tmct0CnmvDesglosesDao.findActivosByEjecucion(entEjecucion.getId());
      
      for (Tmct0CnmvDesgloses tmct0CnmvDesgloses : listDesglosesEntidad) {
        
        DesgloseMtfDto desgloseDto = new DesgloseMtfDto ();
        desgloseDto.setDesglose(tmct0CnmvDesgloses);
        List<Tmct0CnmvTitulares> listaTitulares = new ArrayList<Tmct0CnmvTitulares>();
        List<Tmct0CnmvTitulares> listaRepresentantes = new ArrayList<Tmct0CnmvTitulares>();
        Boolean bPropios = false;
        
        HashSet<String> hsIncluidos = new HashSet<String>();  // Estructura para no incluir repetidos
        
        for (Tmct0CnmvTitulares tmct0CnmvTitulares : tmct0CnmvDesgloses.getTmct0CnmvTitularesList()) {
          
          // Solo se tratan los activos.
          if (tmct0CnmvTitulares.getEstadoTitular() == 'A'){
            
            if (sIdentifSv.equals(tmct0CnmvTitulares.getIdentificacion())){
              bPropios = true;
            }
            
            if (tmct0CnmvTitulares.getTipoTiular() == 'R'){
              listaRepresentantes.add(tmct0CnmvTitulares);
            }else{
              // No se permiten repetidos
              if (!hsIncluidos.contains(tmct0CnmvTitulares.getIdentificacion())){
                listaTitulares.add(tmct0CnmvTitulares);
                hsIncluidos.add(tmct0CnmvTitulares.getIdentificacion());
              }
            }
          }
        }
        
        desgloseDto.setbTitulosPropios(bPropios);
        desgloseDto.setListaTitulares(listaTitulares);
        desgloseDto.setListaRepresentantes(listaRepresentantes);
        
        listaDesglosesDto.add(desgloseDto);
        
      }
      
      return listaDesglosesDto;
    }
    


    /**
     * Quita de la lista de desgloses los titulares que no se van a enviar por superar el numero maximo
     * 
     * @param List<DesgloseMtfDto> lista de los desgloses que supera el maximo
     * @param iNumTitulares Numero de titulares que se pueden enviar
     * @return List<DesgloseMtfDto> lista de los desgloses reducida al numero de desgloses que se pueden enviar
     */
    public List<DesgloseMtfDto> obtenerTitularesEnvio (List <DesgloseMtfDto> listaDesgloses, int iNumTitulares ){
      
      List<DesgloseMtfDto> listaDesglosesReducida = new ArrayList<DesgloseMtfDto>();
      

      // Se crea una nueva lista con todos los desglose pero sin titulares
      for (DesgloseMtfDto desgloseMtfDto : listaDesgloses) {
        DesgloseMtfDto desgloseMtfDtoNuevo = new DesgloseMtfDto();
        desgloseMtfDtoNuevo.setbTitulosPropios( desgloseMtfDto.getbTitulosPropios());
        desgloseMtfDtoNuevo.setDesglose(desgloseMtfDto.getDesglose());
        desgloseMtfDtoNuevo.setListaRepresentantes(desgloseMtfDto.getListaRepresentantes());
        desgloseMtfDtoNuevo.setListaTitulares(new ArrayList<Tmct0CnmvTitulares>());
        listaDesglosesReducida.add(desgloseMtfDtoNuevo);
      }
      
      
      // Se recorren los titulares de los desgloses antiguos y solo se insertan los primeros de cada desglose hasta completar el numero máximo
      int iContador = 0;

      int iNumTitularesActuales = 0;
      while  (iNumTitularesActuales < iNumTitulares) {
        int iDesglose = 0;        
        for (DesgloseMtfDto desgloseMtfDto : listaDesgloses) {
          if (desgloseMtfDto.getListaTitulares().size() > iContador && iNumTitularesActuales < iNumTitulares ){
            listaDesglosesReducida.get(iDesglose).getListaTitulares().add(desgloseMtfDto.getListaTitulares().get(iContador));
            iNumTitularesActuales++;
          }
          iDesglose++;
          
        }
        iContador++;
      }
      
      
      return listaDesglosesReducida;
      
      
    }

       

}
