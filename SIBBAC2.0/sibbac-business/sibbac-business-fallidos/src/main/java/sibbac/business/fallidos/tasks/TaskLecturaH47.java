package sibbac.business.fallidos.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import sibbac.business.fallidos.readers.H47FileReader;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FALLIDOS.NAME,
           name = Task.GROUP_FALLIDOS.JOB_LECTURA_H47,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "18:00:00")
public class TaskLecturaH47 extends FallidosTaskConcurrencyPrevent {

  @Autowired
  private H47FileReader reader;

  @Override
  public void executeTask() {
    try {
      reader.readFromFile(true, "UTF-8");
    } catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
    }
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.FALLIDO_LECTURAH47;
  }

}
