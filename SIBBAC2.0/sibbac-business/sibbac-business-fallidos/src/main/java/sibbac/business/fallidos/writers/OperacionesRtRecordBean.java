package sibbac.business.fallidos.writers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fallidos.utils.ValidatorFieldOperacionesTr;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.common.utils.FormatDataUtils;


public class OperacionesRtRecordBean extends RecordBean {

   
	/** Identificador para la serialización de objetos. */
	private static final long serialVersionUID = -4350988045738880922L;
             
	/** Referencia para la inserción de logs. */
	private static final Logger LOG = LoggerFactory.getLogger(OperacionesRtRecordBean.class);

	/** Número máximo de titulares que se pueden enviar en una línea. */
	public static final Integer MAX_HOLDERS = 19;
	  
	//Atributos
	
	/** tipoRegistro. */
	String campo1;  
	/** refOperacion. */
	String campo2;
	/** estado. */
	String campo3;
  /** idEjecucion. */	
	String campo4;
  /** idEntEjecutora. */
	String campo5;
	/** entidadMiFID. */
	String campo6; 
	/** sentido. */
	String campo7; 
	/** indicadorTransmision. */
	String campo8;  
	/** transmisoraCliente. */
	String campo9;
	/** fechaHoraNegociacion. */
	String campo10;
	/** capacidadNegociacion. */
	String campo11;
	/** titulosDesgloses. */
	String campo12;
	/** divisa. */
	String campo13;
	/** aumentoDisminucion. */
	String campo14;
	/** precio. */
	String campo15;
	/** divisaPrecio. */
	String campo16;
	/** efectivo. */
	String campo17;
	/** centroNegociacion. */
	String campo18;
	/** cdPaisSucursalEjec. */
	String campo19;
	/** Campo Vacio. */
	String campo20;
  /** Campo Vacio. */
  String campo21;
  /** Campo Vacio. */
  String campo22;
  /** Campo Vacio. */
  String campo23;
  /** Campo Vacio. */
  String campo24;
  /** Campo Vacio. */
  String campo25;
  /** Campo Vacio. */
  String campo26;
  /** Campo Vacio. */
  String campo27;
  /** Campo Vacio. */
  String campo28;
  /** Campo Vacio. */
  String campo29;
  /** Campo Vacio. */
  String campo30;
  /** Campo Vacio. */
  String campo31;
  /** Campo Vacio. */
  String campo32;
  /** Campo Vacio. */
  String campo33;
  /** Campo Vacio. */
  String campo34;
  /** Campo Vacio. */
  String campo35;
  /** Campo Vacio. */
  String campo36;
  /** Campo Vacio. */
  String campo37;
  /** Campo Vacio. */
  String campo38;
	/** idDecisorInversion. */
	String campo39;
	/** cdPaisDecisor. */
	String campo40;
	/** idEjecutor. */
	String campo41;
	/** paisSucursalEjec. */
	String campo42;
	/** indicadorExencion. */
	String campo43;
	/** indicadorVentaCorto. */
	String campo44;
	/** indicadorOtc. */
	String campo45;
	/** indicadorDerivadosMateriaPrima. */
	String campo46;
	/** indicadorOperacionFinanciacion. */
	String campo47;
	/** reportable. */
	String campo48;
	/** motivoNoReportable. */
	String campo49;
	/** refCruceTitulares. */
	String campo50;
	/** tipoIdTransmisora. */
	String campo51;
	/** tipoIdTransmisoraCliente. */
	String campo52;
	/** tipoCantidad. */
	String campo53;
	/** indNoPrecio. */
	String campo54;
	/** tipoPrecio. */
	String campo55;
	/** tipoIdDecisorInversion. */
	String campo56;
	/** tipoIdEjecutor. */
	String campo57;
	/** auxCdTransmisora. */
	String campo58;
	/** tipoAuxCdTransmisora. */
	String campo59;
  /** origenCruce. */
  String campo60;	
	
	
//	List<OperacionesRtRecordBean> listaTitulares = new ListaTitulares<OperacionesRtRecordBean>();
	


  //Constructores
	public OperacionesRtRecordBean(){
		super();		
	}

	public OperacionesRtRecordBean(Tmct0CnmvDesgloses entDesglose) {
		super();
		try {
			
		  // Tipo Registro
			this.campo1 = ValidatorFieldOperacionesTr.validarTipoRegistro(Constantes.TIPO_REGISTRO);
			// Referencia de operacion de la ejecucion
			if ( entDesglose.getRefOperacion() != null){
			  this.campo2 = ValidatorFieldOperacionesTr.validarRefOperacion(entDesglose.getRefOperacion().trim()) ;  
			}
			
			// Estado
			if(entDesglose.getEstadoDesglose().equals(Constantes.ALTA)) {
				this.campo3 = ValidatorFieldOperacionesTr.validarEstado(Constantes.ALTA.toString());
			}else if (entDesglose.getEstadoDesglose().equals(Constantes.BAJA)) {
				this.campo3 = ValidatorFieldOperacionesTr.validarEstado(Constantes.BAJA.toString());
			}
			// Id ejecucion
			if (entDesglose.getEjecucion().getIdEjecucion() != null){
			  this.campo4 =    ValidatorFieldOperacionesTr.validarIdEjecucion(entDesglose.getEjecucion().getIdEjecucion().trim());  
			}
			
			// Campos 5 y 6 se asignan con los parametros que no se han podido injectar al bean
			// this.campo5 = ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora);
			// this.campo6 = ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid);

			// Sentido
			if(entDesglose.getEjecucion().getSentido() != null) {
				this.campo7 = ValidatorFieldOperacionesTr.validarSentidoOperacion(entDesglose.getEjecucion().getSentido().toString().trim());
			}
			
			// Indicador de transmision
			if(entDesglose.getEjecucion().getIndicadorTransmision() != null){
				this.campo8 = ValidatorFieldOperacionesTr.validarIndicadorTransmision(entDesglose.getEjecucion().getIndicadorTransmision().toString().trim());
			}
			
			//Identificacion de la entidad transmisora
			if (entDesglose.getIdTransmisora() != null){
			  this.campo9 = ValidatorFieldOperacionesTr.validarIdEntTransmisora(entDesglose.getIdTransmisora().trim());  
			}
			
			//Fecha y hora de negociacion.   
			this.campo10 = ValidatorFieldOperacionesTr.validarFechaHoraNegociacion(FormatDataUtils.comprobarFechaVeranoInvierno(entDesglose.getEjecucion().getfEjecucion(), entDesglose.getEjecucion().gethEjecucion()));
			// Capacidad de negociacion
			if (entDesglose.getCapacidadNegociacion() != null){
			  this.campo11 = ValidatorFieldOperacionesTr.validarIndCapacidad(entDesglose.getCapacidadNegociacion().trim());  
			}
			
			// Titulos ejecutados.
			if(entDesglose.getTitulosDesgloses() != null) {
				if(entDesglose.getTitulosDesgloses().signum() == -1) {
					this.campo12 = "- ".concat(ValidatorFieldOperacionesTr.validarPrecio(entDesglose.getTitulosDesgloses().toPlainString()));
				}else {
					this.campo12 = ValidatorFieldOperacionesTr.validarPrecio(entDesglose.getTitulosDesgloses().toPlainString());
				}
			}

			//Divisa DDR 3.3 se cambia a vacio
			//this.campo13 = ValidatorFieldOperacionesTr.validarDivisa(entDesglose.getEjecucion().getDivisa());
			
			//Aumento  sin informar
			//this.campo14 = Constantes.BLANK;
			
			//Precio
			if(entDesglose.getEjecucion().getPrecio() != null) {
				if(entDesglose.getEjecucion().getPrecio().signum() == -1) {
					this.campo15 = "- ".concat(ValidatorFieldOperacionesTr.validarPrecio(entDesglose.getEjecucion().getPrecio().toPlainString()));
				}else {
					this.campo15 = ValidatorFieldOperacionesTr.validarPrecio(entDesglose.getEjecucion().getPrecio().toPlainString());
				}
			}
			
			//Divisa
			if (entDesglose.getEjecucion().getDivisa() != null) {
			  this.campo16 = ValidatorFieldOperacionesTr.validarDivisa(entDesglose.getEjecucion().getDivisa().trim());  
			}
			

			// Efectivo
			if(entDesglose.getEfectivoDesglose() != null) {
				if(entDesglose.getEfectivoDesglose().signum() == -1) {
					this.campo17 = "- ".concat(ValidatorFieldOperacionesTr.validarPrecio(entDesglose.getEfectivoDesglose().toPlainString())); 
				}else {
					this.campo17 = ValidatorFieldOperacionesTr.validarPrecio(entDesglose.getEfectivoDesglose().toPlainString());
				}
			}
			
			//Centro de negociacion
			if (entDesglose.getEjecucion().getSistemaNegociacion() != null){
			  this.campo18 = ValidatorFieldOperacionesTr.validarCentroNegociacion(entDesglose.getEjecucion().getSistemaNegociacion().trim());  
			}
			
     
			// Codigo pais de la sucursal
			if (entDesglose.getEjecucion().getCdPaisSucursalEjec() != null)  {
	      this.campo19 = ValidatorFieldOperacionesTr.validarCdPais(entDesglose.getEjecucion().getCdPaisSucursalEjec().trim());			  
			}
			
			// Campos 28 -38 Vacios
			// DDR 3.3 El campo 23 tiene el valor de IDINSTRUMENTO
      if (entDesglose.getEjecucion().getIdInstrumento() != null)  {
        this.campo23 = entDesglose.getEjecucion().getIdInstrumento().trim();        
      }
			
			//Identificador decisor
      if (entDesglose.getIdDecisorInversion() != null){
        this.campo39 = ValidatorFieldOperacionesTr.validarIdDecisorInversion(entDesglose.getIdDecisorInversion().trim());  
      }
			
			// pais del decisor
      if (entDesglose.getCdPaisDecisor() != null) {
        this.campo40 = ValidatorFieldOperacionesTr.validarCdPais(entDesglose.getCdPaisDecisor().trim());  
      }
			
			//id ejecutor
      if ( entDesglose.getEjecucion().getIdEjecutor() != null) {
        this.campo41 = ValidatorFieldOperacionesTr.validarIdEjecutor(entDesglose.getEjecucion().getIdEjecutor().trim());  
      }			
			
			// pais sucursal responsable ejecucion
      if ( entDesglose.getEjecucion().getCdPaisSucursalEjec() != null) {
        this.campo42 = ValidatorFieldOperacionesTr.validarCdPais(entDesglose.getEjecucion().getCdPaisSucursalEjec().trim()); //campo 42  
      }			
      
      // Indicador de exencion
      if (entDesglose.getEjecucion().getIndicadorExencion() != null) {
        this.campo43 = ValidatorFieldOperacionesTr.validarIndicadorExcencion(entDesglose.getEjecucion().getIndicadorExencion().trim());  
      }      
			
			// Indicador venta corto
      if ( entDesglose.getIndicadorVentaCorto() != null) {
        this.campo44 = ValidatorFieldOperacionesTr.validarIndicadorVentaCorto(entDesglose.getIndicadorVentaCorto().trim());  
      }			
			
			// Indicador postnegociacion OTC
      if ( entDesglose.getEjecucion().getIndicadorOtc() != null) {
        this.campo45 = ValidatorFieldOperacionesTr.validarIndicadorPostNegociacion(entDesglose.getEjecucion().getIndicadorOtc().trim());        
      }			

			// Indicador derivados materia prima
			this.campo46 = Constantes.INDICADOR_DERIVADOS_MATERIA_PRIMA;
			// Indicador operacion financiacion
			this.campo47 = Constantes.INDICADOR_OPERACION_FINANCIACION;
			//Reportable
      if (entDesglose.getReportable() != null) {
        this.campo48 = ValidatorFieldOperacionesTr.validarReportable(entDesglose.getReportable().trim());  
      }			
			
			// Motivo no reportable
      if ( entDesglose.getMotivoNoReportable() != null) {
        this.campo49 = ValidatorFieldOperacionesTr.validarMotivoNoReportable(entDesglose.getMotivoNoReportable().trim());        
      }

			//Referencia de cruce de titulares
      if ( entDesglose.getRefCruceTitulares() != null) {
        this.campo50 = ValidatorFieldOperacionesTr.validarRefCruceTitulares(entDesglose.getRefCruceTitulares().trim());        
      }			

			// Tipo de identificacion de la entidad,  valor de la properties que no se ha podido injectar
		//	this.campo51 = ValidatorFieldOperacionesTr.validarTipoIdEntTransmisora(paramTipoIdEntTransmisora);
			// Tipo de identificacion de la entidad transmisora
      if ( entDesglose.getTipoIdTransmisora() != null) {
        this.campo52 = ValidatorFieldOperacionesTr.validarTipoIdEntTransmisoraCliente(entDesglose.getTipoIdTransmisora().trim());        
      }			

			// Tipo de cantidad
      if (entDesglose.getEjecucion().getTipoCantidad() != null) {
        this.campo53 = ValidatorFieldOperacionesTr.validarTipoCantidad(entDesglose.getEjecucion().getTipoCantidad().trim());        
      }			

			
			// Indicado de no precio - Sin informar
			//this.campo54 = Constantes.BLANK; //se informa vacio
			
			// Tipo de precio
      if ( entDesglose.getEjecucion().getTipoPrecio() != null) {
        this.campo55 = ValidatorFieldOperacionesTr.validarTipoPrecio(entDesglose.getEjecucion().getTipoPrecio().trim());  
      }			
			
			//  Tipo de identificacion del decisor de la inversion
      if (entDesglose.getTipoIdDecisorInversion() != null) {
        this.campo56 = ValidatorFieldOperacionesTr.validarTipoIdDecisor(entDesglose.getTipoIdDecisorInversion().trim());  
      }			
			
			// Tipo de identificacion del ejecutor
      if ( entDesglose.getEjecucion().getTipoIdEjecutor() != null) {
        this.campo57 = ValidatorFieldOperacionesTr.validarTipoIdEjecutor(entDesglose.getEjecucion().getTipoIdEjecutor().trim());  
      }			
			
			// Campo auxiliar para la identificacion de la entidad transmisor
      if (entDesglose.getAuxIdTransmisora() != null) {
        this.campo58 = ValidatorFieldOperacionesTr.validarAuxCdTransmisora(entDesglose.getAuxIdTransmisora().trim());        
      }			

			// Campo auxiliar para el tipo de identificacion de la entidad transmisor
      if (entDesglose.getTipoAuxIdTransmisora() != null) {
        this.campo59 = ValidatorFieldOperacionesTr.validarTipoAuxCdTransmisora(entDesglose.getTipoAuxIdTransmisora().trim());        
      }			

			// Campo origen Cruce
      if (entDesglose.getOrigenCruce() != null) {
        this.campo60 = entDesglose.getOrigenCruce().trim();        
      }			

			
		}catch (Exception e) {
			LOG.error("[writeIntoFile] Error: ", e);
		}
		
	}

  
	

	// Contructor del registro extra de la operacion cuando tiene varios desgloses.
	// Se forma a partir de la ejecución y con los datos de uno de sus desgloses.
  public OperacionesRtRecordBean(Tmct0CnmvEjecuciones entEjecucion, Tmct0CnmvDesgloses entDesglose) {
    super();
    try {
      
      // Tipo Registro
      this.campo1 = ValidatorFieldOperacionesTr.validarTipoRegistro(Constantes.TIPO_REGISTRO);
      // Referencia de operacion de la ejecucion
      if ( entEjecucion.getRefOperacion() != null){
        this.campo2 = ValidatorFieldOperacionesTr.validarRefOperacion(entEjecucion.getRefOperacion().trim()) ;  
      }
      
      // Estado
      if(entEjecucion.getEstadoOperacion().equals(Constantes.ALTA)) {
        this.campo3 = ValidatorFieldOperacionesTr.validarEstado(Constantes.ALTA.toString());
      }else if (entEjecucion.getEstadoOperacion().equals(Constantes.BAJA)) {
        this.campo3 = ValidatorFieldOperacionesTr.validarEstado(Constantes.BAJA.toString());
      }
      // Id ejecucion
      if (entEjecucion.getIdEjecucion() != null){
        this.campo4 =    ValidatorFieldOperacionesTr.validarIdEjecucion(entEjecucion.getIdEjecucion().trim());  
      }
      
      // Campos 5 y 6 se asignan con los parametros que no se han podido injectar al bean
      // this.campo5 = ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora);
      // this.campo6 = ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid);

      // Sentido
      if(entEjecucion.getSentido() != null) {
        this.campo7 = ValidatorFieldOperacionesTr.validarSentidoOperacion(entEjecucion.getSentido().toString().trim());
      }
      
      // Indicador de transmision
      if(entEjecucion.getIndicadorTransmision() != null){
        this.campo8 = ValidatorFieldOperacionesTr.validarIndicadorTransmision(entEjecucion.getIndicadorTransmision().toString().trim());
      }
      
      //Identificacion de la entidad transmisora - En este registro va sin informar
      //this.campo9 = ValidatorFieldOperacionesTr.validarIdEntTransmisora(entDesglose.getIdTransmisora());

      
      //Fecha y hora de negociacion.   
     this.campo10 = ValidatorFieldOperacionesTr.validarFechaHoraNegociacion(FormatDataUtils.comprobarFechaVeranoInvierno(entEjecucion.getfEjecucion(), entEjecucion.gethEjecucion()));
     // Capacidad de negociacion
     this.campo11 = ValidatorFieldOperacionesTr.validarIndCapacidad(entDesglose.getCapacidadNegociacion());
      
     // Titulos ejecutados.
     if(entEjecucion.getTitulosEjecucion() != null) {
       if(entEjecucion.getTitulosEjecucion().signum() == -1) {
         this.campo12 = "- ".concat(ValidatorFieldOperacionesTr.validarPrecio(entEjecucion.getTitulosEjecucion().toPlainString()));
       }else {
         this.campo12 = ValidatorFieldOperacionesTr.validarPrecio(entEjecucion.getTitulosEjecucion().toPlainString());
       }
     }

      //Divisa DDR 3.3 se cambia a vacio
      //this.campo13 = ValidatorFieldOperacionesTr.validarDivisa(entDesglose.getEjecucion().getDivisa());
      
      //Aumento  sin informar
      //this.campo14 = Constantes.BLANK;
      
     //Precio
     if(entEjecucion.getPrecio() != null) {
       if(entEjecucion.getPrecio().signum() == -1) {
         this.campo15 = "- ".concat(ValidatorFieldOperacionesTr.validarPrecio(entEjecucion.getPrecio().toPlainString()));
       }else {
         this.campo15 = ValidatorFieldOperacionesTr.validarPrecio(entEjecucion.getPrecio().toPlainString());
       }
     }
      
      //Divisa
      if (entEjecucion.getDivisa() != null) {
        this.campo16 = ValidatorFieldOperacionesTr.validarDivisa(entEjecucion.getDivisa().trim());  
      }
      

      // Efectivo
      if(entEjecucion.getEfectivo() != null) {
        if(entEjecucion.getEfectivo().signum() == -1) {
          this.campo17 = "- ".concat(ValidatorFieldOperacionesTr.validarPrecio(entEjecucion.getEfectivo().toPlainString())); 
        }else {
          this.campo17 = ValidatorFieldOperacionesTr.validarPrecio(entEjecucion.getEfectivo().toPlainString());
        }
      }
      
      //Centro de negociacion
      if (entEjecucion.getSistemaNegociacion() != null){
        this.campo18 = ValidatorFieldOperacionesTr.validarCentroNegociacion(entEjecucion.getSistemaNegociacion().trim());  
      }
      
     
      // Codigo pais de la sucursal
      if (entEjecucion.getCdPaisSucursalEjec() != null)  {
        this.campo19 = ValidatorFieldOperacionesTr.validarCdPais(entEjecucion.getCdPaisSucursalEjec().trim());        
      }
      
      // Campos 28 -38 Vacios
      // DDR 3.3 El campo 23 tiene el valor de IDINSTRUMENTO
      if (entEjecucion.getIdInstrumento() != null)  {
        this.campo23 = entEjecucion.getIdInstrumento().trim();        
      }

      //Identificador decisor  -- En este registro va sin informar
      //this.campo39 = ValidatorFieldOperacionesTr.validarIdDecisorInversion(entDesglose.getIdDecisorInversion());
      // pais del decisor -- En este registro va sin informar
      //this.campo40 = ValidatorFieldOperacionesTr.validarCdPais(entDesglose.getCdPaisDecisor());
      //id ejecutor
      if ( entEjecucion.getIdEjecutor() != null) {
        this.campo41 = ValidatorFieldOperacionesTr.validarIdEjecutor(entEjecucion.getIdEjecutor().trim());  
      }     
      
      // pais sucursal responsable ejecucion
      if ( entEjecucion.getCdPaisSucursalEjec() != null) {
        this.campo42 = ValidatorFieldOperacionesTr.validarCdPais(entEjecucion.getCdPaisSucursalEjec().trim()); //campo 42  
      }     
      
      // Indicador de exencion
      if (entEjecucion.getIndicadorExencion() != null) {
        this.campo43 = ValidatorFieldOperacionesTr.validarIndicadorExcencion(entEjecucion.getIndicadorExencion().trim());  
      }      

      // Indicador venta corto - -- En este registro va sin informar
      //this.campo44 = ValidatorFieldOperacionesTr.validarIndicadorVentaCorto(entDesglose.getIndicadorVentaCorto());
      // Indicador postnegociacion OTC
      if (entEjecucion.getIndicadorOtc() != null) {
        this.campo45 = ValidatorFieldOperacionesTr.validarIndicadorPostNegociacion(entEjecucion.getIndicadorOtc().trim());        
      }     

      // Indicador derivados materia prima
      this.campo46 = Constantes.INDICADOR_DERIVADOS_MATERIA_PRIMA;
      // Indicador operacion financiacion
      this.campo47 = Constantes.INDICADOR_OPERACION_FINANCIACION;

      //Reportable
      if (entDesglose.getReportable() != null) {
        this.campo48 = ValidatorFieldOperacionesTr.validarReportable(entDesglose.getReportable().trim());  
      }     
      
      // Motivo no reportable
      if ( entDesglose.getMotivoNoReportable() != null) {
        this.campo49 = ValidatorFieldOperacionesTr.validarMotivoNoReportable(entDesglose.getMotivoNoReportable().trim());        
      }

      //Referencia de cruce de titulares  -- En este registro va sin informar
     // this.campo50 = ValidatorFieldOperacionesTr.validarRefCruceTitulares(entDesglose.getRefCruceTitulares());
   
      // Tipo de identificacion de la entidad,  valor de la properties que no se ha podido injectar
    //  this.campo51 = ValidatorFieldOperacionesTr.validarTipoIdEntTransmisora(paramTipoIdEntTransmisora);

      // Tipo de identificacion de la entidad transmisora  -- En este registro va sin informar
      //this.campo52 = ValidatorFieldOperacionesTr.validarTipoIdEntTransmisoraCliente(entDesglose.getTipoIdTransmisora());
      // Tipo de cantidad
      if (entEjecucion.getTipoCantidad() != null) {
        this.campo53 = ValidatorFieldOperacionesTr.validarTipoCantidad(entEjecucion.getTipoCantidad().trim());        
      }     

      
      // Indicado de no precio - Sin informar
      //this.campo54 = Constantes.BLANK; //se informa vacio
      
      // Tipo de precio
      if ( entEjecucion.getTipoPrecio() != null) {
        this.campo55 = ValidatorFieldOperacionesTr.validarTipoPrecio(entEjecucion.getTipoPrecio().trim());  
      }  
      //  Tipo de identificacion del decisor de la inversion   -- En este registro va sin informar
      //this.campo56 = ValidatorFieldOperacionesTr.validarTipoIdDecisor(entDesglose.getTipoIdDecisorInversion());
      // Tipo de identificacion del ejecutor
      if ( entEjecucion.getTipoIdEjecutor() != null) {
        this.campo57 = ValidatorFieldOperacionesTr.validarTipoIdEjecutor(entEjecucion.getTipoIdEjecutor().trim());  
      }     
      // Campo auxiliar para la identificacion de la entidad transmisor  -- En este registro va sin informar
      //this.campo58 = ValidatorFieldOperacionesTr.validarAuxCdTransmisora(entDesglose.getAuxIdTransmisora());
      // Campo auxiliar para el tipo de identificacion de la entidad transmisor  -- En este registro va sin informar
      //this.campo59 = ValidatorFieldOperacionesTr.validarTipoAuxCdTransmisora(entDesglose.getTipoAuxIdTransmisora());
      // Campo origen Cruce
      if (entDesglose.getOrigenCruce() != null) {
        this.campo60 = entDesglose.getOrigenCruce().trim();        
      }     
      
    }catch (Exception e) {
      LOG.error("[writeIntoFile] Error: ", e);
    }
    
  }


	
	/**
   * @return the campo1
   */
  public String getCampo1() {
    return campo1;
  }

  /**
   * @param campo1 the campo1 to set
   */
  public void setCampo1(String campo1) {
    this.campo1 = campo1;
  }

  /**
   * @return the campo2
   */
  public String getCampo2() {
    return campo2;
  }

  /**
   * @param campo2 the campo2 to set
   */
  public void setCampo2(String campo2) {
    this.campo2 = campo2;
  }

  /**
   * @return the campo3
   */
  public String getCampo3() {
    return campo3;
  }

  /**
   * @param campo3 the campo3 to set
   */
  public void setCampo3(String campo3) {
    this.campo3 = campo3;
  }

  /**
   * @return the campo4
   */
  public String getCampo4() {
    return campo4;
  }

  /**
   * @param campo4 the campo4 to set
   */
  public void setCampo4(String campo4) {
    this.campo4 = campo4;
  }

  /**
   * @return the campo5
   */
  public String getCampo5() {
    return campo5;
  }

  /**
   * @param campo5 the campo5 to set
   */
  public void setCampo5(String campo5) {
    this.campo5 = campo5;
  }

  /**
   * @return the campo6
   */
  public String getCampo6() {
    return campo6;
  }

  /**
   * @param campo6 the campo6 to set
   */
  public void setCampo6(String campo6) {
    this.campo6 = campo6;
  }

  /**
   * @return the campo7
   */
  public String getCampo7() {
    return campo7;
  }

  /**
   * @param campo7 the campo7 to set
   */
  public void setCampo7(String campo7) {
    this.campo7 = campo7;
  }

  /**
   * @return the campo8
   */
  public String getCampo8() {
    return campo8;
  }

  /**
   * @param campo8 the campo8 to set
   */
  public void setCampo8(String campo8) {
    this.campo8 = campo8;
  }

  /**
   * @return the campo9
   */
  public String getCampo9() {
    return campo9;
  }

  /**
   * @param campo9 the campo9 to set
   */
  public void setCampo9(String campo9) {
    this.campo9 = campo9;
  }

  /**
   * @return the campo10
   */
  public String getCampo10() {
    return campo10;
  }

  /**
   * @param campo10 the campo10 to set
   */
  public void setCampo10(String campo10) {
    this.campo10 = campo10;
  }

  /**
   * @return the campo11
   */
  public String getCampo11() {
    return campo11;
  }

  /**
   * @param campo11 the campo11 to set
   */
  public void setCampo11(String campo11) {
    this.campo11 = campo11;
  }

  /**
   * @return the campo12
   */
  public String getCampo12() {
    return campo12;
  }

  /**
   * @param campo12 the campo12 to set
   */
  public void setCampo12(String campo12) {
    this.campo12 = campo12;
  }

  /**
   * @return the campo13
   */
  public String getCampo13() {
    return campo13;
  }

  /**
   * @param campo13 the campo13 to set
   */
  public void setCampo13(String campo13) {
    this.campo13 = campo13;
  }

  /**
   * @return the campo14
   */
  public String getCampo14() {
    return campo14;
  }

  /**
   * @param campo14 the campo14 to set
   */
  public void setCampo14(String campo14) {
    this.campo14 = campo14;
  }

  /**
   * @return the campo15
   */
  public String getCampo15() {
    return campo15;
  }

  /**
   * @param campo15 the campo15 to set
   */
  public void setCampo15(String campo15) {
    this.campo15 = campo15;
  }

  /**
   * @return the campo16
   */
  public String getCampo16() {
    return campo16;
  }

  /**
   * @param campo16 the campo16 to set
   */
  public void setCampo16(String campo16) {
    this.campo16 = campo16;
  }

  /**
   * @return the campo17
   */
  public String getCampo17() {
    return campo17;
  }

  /**
   * @param campo17 the campo17 to set
   */
  public void setCampo17(String campo17) {
    this.campo17 = campo17;
  }

  /**
   * @return the campo18
   */
  public String getCampo18() {
    return campo18;
  }

  /**
   * @param campo18 the campo18 to set
   */
  public void setCampo18(String campo18) {
    this.campo18 = campo18;
  }

  /**
   * @return the campo19
   */
  public String getCampo19() {
    return campo19;
  }

  /**
   * @param campo19 the campo19 to set
   */
  public void setCampo19(String campo19) {
    this.campo19 = campo19;
  }

  /**
   * @return the campo20
   */
  public String getCampo20() {
    return campo20;
  }

  /**
   * @param campo20 the campo20 to set
   */
  public void setCampo20(String campo20) {
    this.campo20 = campo20;
  }

  /**
   * @return the campo21
   */
  public String getCampo21() {
    return campo21;
  }

  /**
   * @param campo21 the campo21 to set
   */
  public void setCampo21(String campo21) {
    this.campo21 = campo21;
  }

  /**
   * @return the campo22
   */
  public String getCampo22() {
    return campo22;
  }

  /**
   * @param campo22 the campo22 to set
   */
  public void setCampo22(String campo22) {
    this.campo22 = campo22;
  }

  /**
   * @return the campo23
   */
  public String getCampo23() {
    return campo23;
  }

  /**
   * @param campo23 the campo23 to set
   */
  public void setCampo23(String campo23) {
    this.campo23 = campo23;
  }

  /**
   * @return the campo24
   */
  public String getCampo24() {
    return campo24;
  }

  /**
   * @param campo24 the campo24 to set
   */
  public void setCampo24(String campo24) {
    this.campo24 = campo24;
  }

  /**
   * @return the campo25
   */
  public String getCampo25() {
    return campo25;
  }

  /**
   * @param campo25 the campo25 to set
   */
  public void setCampo25(String campo25) {
    this.campo25 = campo25;
  }

  /**
   * @return the campo26
   */
  public String getCampo26() {
    return campo26;
  }

  /**
   * @param campo26 the campo26 to set
   */
  public void setCampo26(String campo26) {
    this.campo26 = campo26;
  }

  /**
   * @return the campo27
   */
  public String getCampo27() {
    return campo27;
  }

  /**
   * @param campo27 the campo27 to set
   */
  public void setCampo27(String campo27) {
    this.campo27 = campo27;
  }

  /**
   * @return the campo28
   */
  public String getCampo28() {
    return campo28;
  }

  /**
   * @param campo28 the campo28 to set
   */
  public void setCampo28(String campo28) {
    this.campo28 = campo28;
  }

  /**
   * @return the campo29
   */
  public String getCampo29() {
    return campo29;
  }

  /**
   * @param campo29 the campo29 to set
   */
  public void setCampo29(String campo29) {
    this.campo29 = campo29;
  }

  /**
   * @return the campo30
   */
  public String getCampo30() {
    return campo30;
  }

  /**
   * @param campo30 the campo30 to set
   */
  public void setCampo30(String campo30) {
    this.campo30 = campo30;
  }

  /**
   * @return the campo31
   */
  public String getCampo31() {
    return campo31;
  }

  /**
   * @param campo31 the campo31 to set
   */
  public void setCampo31(String campo31) {
    this.campo31 = campo31;
  }

  /**
   * @return the campo32
   */
  public String getCampo32() {
    return campo32;
  }

  /**
   * @param campo32 the campo32 to set
   */
  public void setCampo32(String campo32) {
    this.campo32 = campo32;
  }

  /**
   * @return the campo33
   */
  public String getCampo33() {
    return campo33;
  }

  /**
   * @param campo33 the campo33 to set
   */
  public void setCampo33(String campo33) {
    this.campo33 = campo33;
  }

  /**
   * @return the campo34
   */
  public String getCampo34() {
    return campo34;
  }

  /**
   * @param campo34 the campo34 to set
   */
  public void setCampo34(String campo34) {
    this.campo34 = campo34;
  }

  /**
   * @return the campo35
   */
  public String getCampo35() {
    return campo35;
  }

  /**
   * @param campo35 the campo35 to set
   */
  public void setCampo35(String campo35) {
    this.campo35 = campo35;
  }

  /**
   * @return the campo36
   */
  public String getCampo36() {
    return campo36;
  }

  /**
   * @param campo36 the campo36 to set
   */
  public void setCampo36(String campo36) {
    this.campo36 = campo36;
  }

  /**
   * @return the campo37
   */
  public String getCampo37() {
    return campo37;
  }

  /**
   * @param campo37 the campo37 to set
   */
  public void setCampo37(String campo37) {
    this.campo37 = campo37;
  }

  /**
   * @return the campo38
   */
  public String getCampo38() {
    return campo38;
  }

  /**
   * @param campo38 the campo38 to set
   */
  public void setCampo38(String campo38) {
    this.campo38 = campo38;
  }

  /**
   * @return the campo39
   */
  public String getCampo39() {
    return campo39;
  }

  /**
   * @param campo39 the campo39 to set
   */
  public void setCampo39(String campo39) {
    this.campo39 = campo39;
  }

  /**
   * @return the campo40
   */
  public String getCampo40() {
    return campo40;
  }

  /**
   * @param campo40 the campo40 to set
   */
  public void setCampo40(String campo40) {
    this.campo40 = campo40;
  }

  /**
   * @return the campo41
   */
  public String getCampo41() {
    return campo41;
  }

  /**
   * @param campo41 the campo41 to set
   */
  public void setCampo41(String campo41) {
    this.campo41 = campo41;
  }

  /**
   * @return the campo42
   */
  public String getCampo42() {
    return campo42;
  }

  /**
   * @param campo42 the campo42 to set
   */
  public void setCampo42(String campo42) {
    this.campo42 = campo42;
  }

  /**
   * @return the campo43
   */
  public String getCampo43() {
    return campo43;
  }

  /**
   * @param campo43 the campo43 to set
   */
  public void setCampo43(String campo43) {
    this.campo43 = campo43;
  }

  /**
   * @return the campo44
   */
  public String getCampo44() {
    return campo44;
  }

  /**
   * @param campo44 the campo44 to set
   */
  public void setCampo44(String campo44) {
    this.campo44 = campo44;
  }

  /**
   * @return the campo45
   */
  public String getCampo45() {
    return campo45;
  }

  /**
   * @param campo45 the campo45 to set
   */
  public void setCampo45(String campo45) {
    this.campo45 = campo45;
  }

  /**
   * @return the campo46
   */
  public String getCampo46() {
    return campo46;
  }

  /**
   * @param campo46 the campo46 to set
   */
  public void setCampo46(String campo46) {
    this.campo46 = campo46;
  }

  /**
   * @return the campo47
   */
  public String getCampo47() {
    return campo47;
  }

  /**
   * @param campo47 the campo47 to set
   */
  public void setCampo47(String campo47) {
    this.campo47 = campo47;
  }

  /**
   * @return the campo48
   */
  public String getCampo48() {
    return campo48;
  }

  /**
   * @param campo48 the campo48 to set
   */
  public void setCampo48(String campo48) {
    this.campo48 = campo48;
  }

  /**
   * @return the campo49
   */
  public String getCampo49() {
    return campo49;
  }

  /**
   * @param campo49 the campo49 to set
   */
  public void setCampo49(String campo49) {
    this.campo49 = campo49;
  }

  /**
   * @return the campo50
   */
  public String getCampo50() {
    return campo50;
  }

  /**
   * @param campo50 the campo50 to set
   */
  public void setCampo50(String campo50) {
    this.campo50 = campo50;
  }

  /**
   * @return the campo51
   */
  public String getCampo51() {
    return campo51;
  }

  /**
   * @param campo51 the campo51 to set
   */
  public void setCampo51(String campo51) {
    this.campo51 = campo51;
  }

  /**
   * @return the campo52
   */
  public String getCampo52() {
    return campo52;
  }

  /**
   * @param campo52 the campo52 to set
   */
  public void setCampo52(String campo52) {
    this.campo52 = campo52;
  }

  /**
   * @return the campo53
   */
  public String getCampo53() {
    return campo53;
  }

  /**
   * @param campo53 the campo53 to set
   */
  public void setCampo53(String campo53) {
    this.campo53 = campo53;
  }

  /**
   * @return the campo54
   */
  public String getCampo54() {
    return campo54;
  }

  /**
   * @param campo54 the campo54 to set
   */
  public void setCampo54(String campo54) {
    this.campo54 = campo54;
  }

  /**
   * @return the campo55
   */
  public String getCampo55() {
    return campo55;
  }

  /**
   * @param campo55 the campo55 to set
   */
  public void setCampo55(String campo55) {
    this.campo55 = campo55;
  }

  /**
   * @return the campo56
   */
  public String getCampo56() {
    return campo56;
  }

  /**
   * @param campo56 the campo56 to set
   */
  public void setCampo56(String campo56) {
    this.campo56 = campo56;
  }

  /**
   * @return the campo57
   */
  public String getCampo57() {
    return campo57;
  }

  /**
   * @param campo57 the campo57 to set
   */
  public void setCampo57(String campo57) {
    this.campo57 = campo57;
  }

  /**
   * @return the campo58
   */
  public String getCampo58() {
    return campo58;
  }

  /**
   * @param campo58 the campo58 to set
   */
  public void setCampo58(String campo58) {
    this.campo58 = campo58;
  }

  /**
   * @return the campo59
   */
  public String getCampo59() {
    return campo59;
  }

  /**
   * @param campo59 the campo59 to set
   */
  public void setCampo59(String campo59) {
    this.campo59 = campo59;
  }
	

  /**
   * @return the campo60
   */
  public String getCampo60() {
    return campo60;
  }

  /**
   * @param campo60 the campo60 to set
   */
  public void setCampo60(String campo60) {
    this.campo60 = campo60;
  }
	
}
