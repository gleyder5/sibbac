package sibbac.business.fallidos.dto;


import java.io.Serializable;
import java.math.BigDecimal;


public class MovimientoFallidoDTO implements Serializable {

	private static final long	serialVersionUID	= 2119849766772903085L;

	private Long				id;
	private String				tipo;
	private String				descripcion;
	private BigDecimal			titulosAfectados;
	private BigDecimal			saldoAfectado;
	private String				holder;
	private String				nbholder;
	private String				alias;
	private String				descripcionAlias;
	private String				subcuenta;
	private String				isin;
	private String				descripcionIsin;
	private String				camara;
	private String				segmento;
	private char				sentido;
	private boolean				manual;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo( String tipo ) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public BigDecimal getTitulosAfectados() {
		return titulosAfectados;
	}

	public void setTitulosAfectados( BigDecimal titulosAfectados ) {
		this.titulosAfectados = titulosAfectados;
	}

	public BigDecimal getSaldoAfectado() {
		return saldoAfectado;
	}

	public void setSaldoAfectado( BigDecimal saldoAfectado ) {
		this.saldoAfectado = saldoAfectado;
	}

	public String getHolder() {
		return holder;
	}

	public void setHolder( String holder ) {
		this.holder = holder;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getDescripcionAlias() {
		return descripcionAlias;
	}

	public void setDescripcionAlias( String descripcionAlias ) {
		this.descripcionAlias = descripcionAlias;
	}

	public String getSubcuenta() {
		return subcuenta;
	}

	public void setSubcuenta( String subcuenta ) {
		this.subcuenta = subcuenta;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescripcionIsin() {
		return descripcionIsin;
	}

	public void setDescripcionIsin( String descripcionIsin ) {
		this.descripcionIsin = descripcionIsin;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento( String segmento ) {
		this.segmento = segmento;
	}

	public char getSentido() {
		return sentido;
	}

	public void setSentido( char sentido ) {
		this.sentido = sentido;
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual( boolean manual ) {
		this.manual = manual;
	}

	/**
	 * @return the nbholder
	 */
	public String getNbholder() {
		return nbholder;
	}

	/**
	 * @param nbholder the nbholder to set
	 */
	public void setNbholder( String nbholder ) {
		this.nbholder = nbholder;
	}
}
