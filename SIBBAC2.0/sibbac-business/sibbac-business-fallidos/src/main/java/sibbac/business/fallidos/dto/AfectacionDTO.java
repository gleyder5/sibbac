package sibbac.business.fallidos.dto;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;


/**
 * @version 1.0
 * @author XI316153
 */
public class AfectacionDTO implements Serializable {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Identificador para la serialización de objetos. */
	private static final long	serialVersionUID	= 3367013009006468823L;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	/** Indica si el registro es editable. */
	private Boolean				editable;

	/** Identificador de la orden a la que pertence el DESGLOSECLITIT. */
	private String				nuorden;

	/** Identificador del booking al que pertenece el DESGLOSECLITIT. */
	private String				nbooking;

	/** Identificador del ALO al que pertenece el DESGLOSECLITIT. */
	private String				nucnfclt;

	/** Identificador del ALC al que pertenece el DESGLOSECLITIT. */
	private BigDecimal			nucnfliq;

	/** Identificador del DESGLOSECAMARA al que pertenece el DESGLOSECLITIT. */
	private Long				iddesglosecamara;

	/** Identificador del DESGLOSECLITIT. */
	private Long				nudesglose;

	/** Número de títulos del DESGLOSECLITIT. */
	private BigDecimal			imtitulos;

	/** Precio por título del DESGLOSECLITIT. */
	private BigDecimal			imprecio;

	/** Alias al que pertenece la ejecución. */
	private String				cdalias;

	/** Fecha de realización de la ejecución. */
	private Date				feejecuc;

	/** Hora de realización de la ejecución. */
	private Time				hoejecuc;

	/** Número de títulos afectados. */
	private BigDecimal			nuTitAfectados;

	/** Saldo neto del cliente en la fecha de la operacion. */
	private BigDecimal			saldoNeto;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public AfectacionDTO() {
	} // AfectacionDTO

	// /**
	// * Constructor II. Genera un objeto AfectacionDTO a partir de la Afectacion especificada.
	// *
	// * @param afectacion
	// * @param editable
	// */
	// public AfectacionDTO(Afectacion afectacion, boolean editable) {
	// this.editable = editable;
	// this.nuorden = afectacion.getNuorden();
	// this.nbooking = afectacion.getNbooking();
	// this.nucnfclt = afectacion.getNucnfclt();
	// this.nucnfliq = afectacion.getNucnfliq();
	// this.iddesglosecamara = afectacion.getIddesglosecamara();
	// this.nudesglose = afectacion.getNudesglose();
	// this.imtitulos = afectacion.getImtitulos();
	// this.imprecio = afectacion.getImprecio();
	// this.cdalias = afectacion.getCdalias();
	// this.feejecuc = afectacion.getFeejecuc();
	// this.hoejecuc = afectacion.getHoejecuc();
	// this.nuTitAfectados = afectacion.getNuTitAfectados();
	// } // AfectacionDTO
	//
	// /**
	// * Constructor III. Crea una afectación a partir de los datos contenidos en el array especificado.
	// *
	// * @param afectacion Datos de la afectación.
	// */
	// public AfectacionDTO(Object[] afectacion, boolean editable) {
	// this.editable = editable;
	//
	// this.cdalias = afectacion[0].toString().trim();
	//
	// this.nuorden = afectacion[1].toString();
	// this.nbooking = afectacion[2].toString();
	// this.nucnfclt = afectacion[3].toString();
	// this.nucnfliq = new BigDecimal(afectacion[4].toString());
	//
	// this.iddesglosecamara = Long.valueOf(afectacion[5].toString());
	//
	// this.nudesglose = Long.valueOf(afectacion[6].toString());
	//
	// this.imtitulos = new BigDecimal(afectacion[7].toString()).subtract(new BigDecimal(afectacion[9].toString()));
	// this.imprecio = new BigDecimal(afectacion[8].toString());
	//
	// this.feejecuc = Date.valueOf(afectacion[10].toString());
	// this.hoejecuc = Time.valueOf(afectacion[11].toString());
	//
	// this.nuTitAfectados = BigDecimal.ZERO;
	// } // Afectacion
	//
	/**
	 * Constructor IV. Crea una afectacionDTO a partir de los datos especificados.
	 * 
	 * @param editable
	 * @param nuorden
	 * @param nbooking
	 * @param nucnfclt
	 * @param nucnfliq
	 * @param iddesglosecamara
	 * @param nudesglose
	 * @param imtitulos
	 * @param imprecio
	 * @param cdalias
	 * @param feejecuc
	 * @param hoejecuc
	 * @param nuTitAfectados
	 */
	public AfectacionDTO( Boolean editable, String nuorden, String nbooking, String nucnfclt, BigDecimal nucnfliq, Long iddesglosecamara,
			Long nudesglose, BigDecimal imtitulos, BigDecimal imprecio, String cdalias, Date feejecuc, Time hoejecuc,
			BigDecimal nuTitAfectados, BigDecimal saldoNeto ) {
		this.editable = editable;
		this.nuorden = nuorden;
		this.nbooking = nbooking;
		this.nucnfclt = nucnfclt;
		this.nucnfliq = nucnfliq;
		this.iddesglosecamara = iddesglosecamara;
		this.nudesglose = nudesglose;
		this.imtitulos = imtitulos;
		this.imprecio = imprecio;
		this.cdalias = cdalias;
		this.feejecuc = feejecuc;
		this.hoejecuc = hoejecuc;
		this.nuTitAfectados = nuTitAfectados;
		this.saldoNeto = saldoNeto;
	} // AfectacionDTO

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AfectacionDTO [editable=" + editable + ", nuorden=" + nuorden + ", nbooking=" + nbooking + ", nucnfclt=" + nucnfclt
				+ ", nucnfliq=" + nucnfliq + ", iddesglosecamara=" + iddesglosecamara + ", nudesglose=" + nudesglose + ", imtitulos="
				+ imtitulos + ", imprecio=" + imprecio + ", cdalias=" + cdalias + ", feejecuc=" + feejecuc + ", hoejecuc=" + hoejecuc
				+ ", nuTitAfectados=" + nuTitAfectados + ", saldoNeto=" + saldoNeto + "]";
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

	/**
	 * @return the editable
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * @return the nuTitAfectados
	 */
	public BigDecimal getNuTitAfectados() {
		return nuTitAfectados;
	}

	/**
	 * @return the cdalias
	 */
	public String getCdalias() {
		return cdalias;
	}

	/**
	 * @return the nuorden
	 */
	public String getNuorden() {
		return nuorden;
	}

	/**
	 * @return the nbooking
	 */
	public String getNbooking() {
		return nbooking;
	}

	/**
	 * @return the nucnfclt
	 */
	public String getNucnfclt() {
		return nucnfclt;
	}

	/**
	 * @return the nucnfliq
	 */
	public BigDecimal getNucnfliq() {
		return nucnfliq;
	}

	/**
	 * @return the iddesglosecamara
	 */
	public Long getIddesglosecamara() {
		return iddesglosecamara;
	}

	/**
	 * @return the nudesglose
	 */
	public Long getNudesglose() {
		return nudesglose;
	}

	/**
	 * @return the imtitulos
	 */
	public BigDecimal getImtitulos() {
		return imtitulos;
	}

	/**
	 * @return the imprecio
	 */
	public BigDecimal getImprecio() {
		return imprecio;
	}

	/**
	 * @return the feejecuc
	 */
	public Date getFeejecuc() {
		return feejecuc;
	}

	/**
	 * @return the hoejecuc
	 */
	public Time getHoejecuc() {
		return hoejecuc;
	}

	/**
	 * @return the saldoNeto
	 */
	public BigDecimal getSaldoNeto() {
		return saldoNeto;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

	/**
	 * @param editable the editable to set
	 */
	public void setEditable( Boolean editable ) {
		this.editable = editable;
	}

	/**
	 * @param nuTitAfectados the nuTitAfectados to set
	 */
	public void setNuTitAfectados( BigDecimal nuTitAfectados ) {
		this.nuTitAfectados = nuTitAfectados;
	}

	/**
	 * @param cdalias the cdalias to set
	 */
	public void setCdalias( String cdalias ) {
		this.cdalias = cdalias;
	}

	/**
	 * @param nuorden the nuorden to set
	 */
	public void setNuorden( String nuorden ) {
		this.nuorden = nuorden;
	}

	/**
	 * @param nbooking the nbooking to set
	 */
	public void setNbooking( String nbooking ) {
		this.nbooking = nbooking;
	}

	/**
	 * @param nucnfclt the nucnfclt to set
	 */
	public void setNucnfclt( String nucnfclt ) {
		this.nucnfclt = nucnfclt;
	}

	/**
	 * @param nucnfliq the nucnfliq to set
	 */
	public void setNucnfliq( BigDecimal nucnfliq ) {
		this.nucnfliq = nucnfliq;
	}

	/**
	 * @param iddesglosecamara the iddesglosecamara to set
	 */
	public void setIddesglosecamara( Long iddesglosecamara ) {
		this.iddesglosecamara = iddesglosecamara;
	}

	/**
	 * @param nudesglose the nudesglose to set
	 */
	public void setNudesglose( Long nudesglose ) {
		this.nudesglose = nudesglose;
	}

	/**
	 * @param imtitulos the imtitulos to set
	 */
	public void setImtitulos( BigDecimal imtitulos ) {
		this.imtitulos = imtitulos;
	}

	/**
	 * @param imprecio the imprecio to set
	 */
	public void setImprecio( BigDecimal imprecio ) {
		this.imprecio = imprecio;
	}

	/**
	 * @param feejecuc the feejecuc to set
	 */
	public void setFeejecuc( Date feejecuc ) {
		this.feejecuc = feejecuc;
	}

	/**
	 * @param hoejecuc the hoejecuc to set
	 */
	public void setHoejecuc( Time hoejecuc ) {
		this.hoejecuc = hoejecuc;
	}

	/**
	 * @param saldoNeto the saldoNeto to set
	 */
	public void setSaldoNeto( BigDecimal saldoNeto ) {
		this.saldoNeto = saldoNeto;
	}

} // AfectacionDTO
