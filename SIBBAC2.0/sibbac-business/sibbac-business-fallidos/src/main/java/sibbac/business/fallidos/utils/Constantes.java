package sibbac.business.fallidos.utils;

/**
 * Contiene las constantes utilizadas.
 * 
 * @author XI316153
 */
public class Constantes {

  // COMANDOS PARA LOS SERVICIOS DE FALLIDOS Y AJUSTES

  /** Comando para los servicios de fallidos de listado de información. */
  public static final String CMD_LIST = "list";

  /** Comando para los servicios de fallidos de alta de información. */
  public static final String CMD_ADD = "add";

  /** Comando para los servicios de fallidos de actualización de información. */
  public static final String CMD_UPDATE = "update";

  /** Comando para los servicios de fallidos de borrado de información. */
  public static final String CMD_DELETE = "delete";

  /** Comando para los servicios de fallidos de inicialización de campos de los formularios. */
  public static final String CMD_INIT = "init";

  /** Comando para los servicios de fallidos de filtrado de información. */
  public static final String CMD_FIND = "filtra";

  // CODIGOS DE OPERACIÓN ECC PARA FALLIDOS Y AJUSTES

  /** Código de operación ECC que indica que la operación es de generación de una instrucción de liquidación. */
  public static final String FIXML_GENERACION_IL = "3";

  /** Código de operación ECC que indica que la operación es de creación de la 'initial leg' de un PUI. */
  public static final String FIXML_PRESTAMO_IDA = "4";

  /** Código de operación ECC que indica que la operación es de creación de la 'return leg' de un PUI. */
  public static final String FIXML_PRESTAMO_VUELTA = "5";

  /** Código de operación ECC que indica que la operación es de creación de una recompra de títulos. */
  public static final String FIXML_RECOMPRA = "6";

  /** Código de operación ECC que indica que la operación es de creación de una recompra en efectivo. */
  public static final String FIXML_LIQUIDACION_EFECTIVO = "7";

  /** Código de operación ECC que indica que la operación es de actualización del colateral de un PUI. */
  public static final String FIXML_ACTUALIZACION_COLATERAL_PRESTAMO = "8";

  /** Código de operación ECC que indica que la operación es de ajuste por PUI. */
  public static final String FIXML_AJUSTE_PRESTAMO = "9";

  /** Código de operación ECC que indica que la operación es de liquidación de una instrucción de liquidación. */
  public static final String FIXML_LIQUIDACION_IL = "B";

  /** Código de operación ECC que indica que la operación es de ajuste eventos financieros. */
  public static final String FIXML_AJUSTE_POR_EVENTOS = "C";

  // CÓDIGOS DE LOS TIPOS DE MOVIMIENTOS

  /** Nombre del fichero FIXML que contiene las instrucciones pendientes de liquidar. */
  public static final String CPENDINGINST_FILENAME = "CPENDINGINST.C0";

  /** Nombre del fichero FIXML que contiene las instrucciones de generación de los PUI, initial leg y return leg. */
  public static final String CSECLENDING_FILENAME = "CSECLENDING.C0";

  /** Nombre del fichero FIXML que contiene las actualizaciones del colateral. */
  public static final String CSECLENDVALUE_FILENAME = "CSECLENDVALUE.C0";

  /** Nombre del fichero FIXML que contiene las remuneraciones diarias de los PUI. */
  public static final String CSECLENDREMUN_FILENAME = "CSECLENDREMUN.C0";

  /** Nombre del fichero FIXML que contiene las penalizaciones de los PUI. */
  public static final String CFAILPENALTIES_FILENAME = "CFAILPENALTIES.C0";

  /** Nombre del fichero FIXML que contiene las cancelación de los PUI. */
  public static final String CSECLENDCANC_FILENAME = "CSECLENDCANC.C0";

  /** Nombre del fichero FIXML que contiene las recompras de títulos. */
  public static final String CFAILBUYIN_FILENAME = "CFAILBUYIN.C0";

  /** Nombre del fichero FIXML que contiene las liquidaciones en efectivo. */
  public static final String CFAILCASHSETTL_FILENAME = "CFAILCASHSETTL.C0";

  /** Extensión de los ficheros. */
  public static final String FIXML_EXT = ".XML";

  /** Posicion que ocupa el tipo de operación dentro de un número de instrucción de liquidación de la ECC. */
  public static final Integer TRDID_EXCHTRDTYP_POS = 13;

  /** Fecha que indica que el proceso de busqueda de fallidos es para el día de la ejecución. */
  public static final String NO_MANUAL_DATE = "9999-12-31";
  
  /** Nombre del PROCESO DE FALLIDOS*/
  public static final String PROCESO_FALLIDOS = "CREACION OPERACIONES FALLIDOS";
  
  /** Nombre del PROCESO DE MTF*/
  public static final String PROCESO_MTF = "CREACION OPERACIONES MTF";
  
  /** Nombre del PROCESO DE RT*/
  public static final String PROCESO_RT = "CREACION OPERACIONES RT";
  

  /** Nombre del PROCESO Generar fichero*/
  public static final String PROCESO_GENERAR_FICHERO = "GENERACION DE FICHERO H47";
  
  public static final String PROCESO_GENERAR_FICHERO_RT = "GENERACION DE FICHERO RT";
  
  
  // FUNCION CONCAT
  
  /** Caracteres especiales */
  public static final String CARACTERES_ESPECIALES = "º,ª";
  
  /** Titulos a evaular en el nombre del titular */
  public static final String TITULOS = "AATY,COACH,DAME,DR,FR,GOV,HONORABLE,MADAM(E),MAID,MASTER,MISS,MONSIEUR,MR,MRS,MS,MX,OFC,PH.D,PRES,PROF,REV,SIR";
  
  /** Prefijos a evaluar en los apellidos del titular */
  @SuppressWarnings("unused")
  private static final String[] PREFIJOS = {"AM", "AUF", "AUF DEM", "AUS DER", "D", "DA", "DE", "DE L’", "DEL", "DE LA", "DE LE", "DI", "DO", "DOS", "DU", "IM", "LA", "LE", "MAC", "MC", "MHAC", "MHÍC", "MHIC GIOLLA", "MIC", "NI", "NÍ", "NÍC", "O", "Ó", "UA", "UI", "UÍ", "VAN", "VAN DE", "VAN DEN", "VAN DER", "VOM", "VON", "VON DEM", "VON DEN", "VON DER"};
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_A = "Ææ";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_D = "ð";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_O = "Øø";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_S = "ß";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN1_SUPLEMENT_T = "Þþ";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN_EXTENDED_D = "đĐ";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN_EXTENDED_I = "ı";
  
  /** Caracteres especiales para la transliteración */
  public static final String LATIN_EXTENDED_L = "Łł";
  
 // Operaciones TR MiFIDII 
  
  /** Valor fijo Tipo de registro para las operaciones TR */
  public static final String TIPO_REGISTRO= "OP";
  
  /** Indicador de derivados materia prima para las operaciones TR */
  public static final String INDICADOR_DERIVADOS_MATERIA_PRIMA = "F";
  
  /** Indicador operacion financiacion para las operaciones TR*/
  public static final String INDICADOR_OPERACION_FINANCIACION = "F";  
  
  /** Indicador si una operación es una ALTA */
  public static final Character ALTA = 'A';
  
  /** Indicador si una operación es una BAJA*/
  public static final Character BAJA = 'B';
  
  /** Valor vacio */
  public static final String BLANK = "";
  
  // Titulares TR MiFIDII
  
  /** Valor fijo tipo de registro para los titulares TR */
  public static final String TIPO_REGISTRO_TITULARES = "TI";
    
  // Extensión ficheros MiFIDII
  public static final String EXTENSION_FICHEROS_MIFIDII = ".txt";

  
} // Constantes
