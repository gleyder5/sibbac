package sibbac.business.fallidos.rest;


// The annotation
// Utils
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// Logging
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.database.bo.MovimientoFallidoBo;
import sibbac.webapp.annotations.SIBBACService;
// The incoming data
import sibbac.webapp.beans.WebRequest;
// The outgoing response
import sibbac.webapp.beans.WebResponse;
// The abstract class for the business service.
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * Perform business operations related with: "Fallido".<br/>
 * This class, implementing {@link SIBBACServiceBean}, is responsible of parsing incoming {@link WebRequest}'s and process them
 * returning a {@link WebResponse} to be sent back to the client.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@SIBBACService
public class SIBBACServiceFallidos implements SIBBACServiceBean {

	// ------------------------------------------ Bean static properties

	private static final Logger	LOG				= LoggerFactory.getLogger( SIBBACServiceFallidos.class );

	// Commands
	public static final String	KEY_COMMAND		= "command";
	public static final String	CMD_LISTING		= "LISTING";
	public static final String	CMD_DETAIL		= "DETAIL";

	// Fields
	public static final String	FIELD_JOB_NAME	= "name";
	public static final String	FIELD_INFO		= "info";
	public static final String	FIELD_ERROR		= "error";
	public static final String	FIELD_STATUS	= "status";

	// ------------------------------------------------- Bean properties

	public String				module			= "SIBBACServiceFallidos";

	// ----------------------------------------------- Bean Constructors
	@Autowired
	private MovimientoFallidoBo	movimientoFallidoBo;

	/**
	 * The default constructor.
	 */
	public SIBBACServiceFallidos() {
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LISTING );
		commands.add( CMD_DETAIL );
		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		List< String > fields = new ArrayList< String >();
		fields.add( FIELD_JOB_NAME );
		fields.add( FIELD_STATUS );
		return fields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#process(WebRequest)
	 */
	@Override
	public WebResponse process( final WebRequest webRequest ) {
		String prefix = "[" + this.module + "::process] ";
		LOG.debug( prefix + "Processing a web request..." );

		// Prepare the response
		WebResponse webResponse = new WebResponse();

		// Retrieve the requested action.
		String action = webRequest.getAction();
		LOG.debug( prefix + "+ Action: [{}]", action );

		// Retrieve more requested data.
		Map< String, String > filtros = webRequest.getFilters();

		// Business tasks...
		switch ( webRequest.getAction() ) {
			case CMD_LISTING:
				// TODO hay que hacer que sea paginado y admita filtros de busqueda
				// FIXME solo para que se pueda empezar la parte web
				webResponse.setResultados( movimientoFallidoBo.searchMovimientos( filtros ) );
				LOG.trace( " Llamada el WebResponse con la lista de cobros " );

				break;

			default:

				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;
		}
		// And quit.
		return webResponse;
	}

}
