package sibbac.business.fallidos.writers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fallidos.database.model.TitularesH47;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.dto.DesgloseMtfDto;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.common.utils.FormatDataUtils;

/**
 * Especifica los atributos de un registro H47.
 * 
 * @author XI316153
 * @see RecordBean
 */
public class H47RecordBean extends RecordBean {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -3247135247243793655L;

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(H47RecordBean.class);

  /** Número máximo de titulares que se pueden enviar en una línea. */
  public static final Integer MAX_HOLDERS = 19;
  

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  String iccservi;
  String iccerrme;
  String iccforma;
  String iccentsv;
  String icfcreac;
  String ichcreac;
  String icdcorre;
  String icubicen;
  String iccbicen;
  String icfnegoc;
  String ichnegoc;
  String icuhora;
  String icfvalor;
  String icutiope;
  String icuprote;
  String iccvalor;
  String icuinstru;
  String icuidenti;
  String icusubya;
  String icutipoi;
  String icfvenci;
  String icuderiv;
  String icupucal;
  String iciprcij;
  String icqprcij;
  String icipreci;
  String icupreci;
  String iccdivis;
  String icvcontr;
  String icucontr;
  String iciefect;
  String iccbicco;
  String icubicco;
  String icdsiste;
  String icusiste;
  String icnrefme;
  String icuactu;
  String icnreg01;
  List<H47TitularRecordBean> listaTitulares = new ListaTitulares<H47TitularRecordBean>();
  String filler;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * Constructor I. Llama al constructor de la clase padre.
   */
  public H47RecordBean() {
    super();
  } // H47RecordBean

  /**
   * Constructor II. Llama al constructor de la clase padre.
   * 
   * @param operacion
   * @param mapaConstantes
   */
  public H47RecordBean(OperacionH47 operacion, Map<String, String> mapaConstantes) {
    super();
    try {
      this.iccservi = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCSERVI"), 6);
      this.iccerrme = StringHelper.returnNWhiteSpaces(3);
      this.iccforma = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCFORMA"), 3);
      this.iccentsv = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCENTSV"), 4);
      this.icfcreac = mapaConstantes.get("ICFCREAC");
      this.ichcreac = mapaConstantes.get("ICHCREAC");
      this.icdcorre = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICDCORRE"), 60);
      this.icubicen = mapaConstantes.get("ICUBICEN");
      this.iccbicen = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCBICEN"), 40);

      // this.icfnegoc = SIBBACPTICommons.convertDateToString(operacion.getFhNegociacion(), formatoFecha);
      this.icfnegoc = FormatDataUtils.convertDateToString(operacion.getFhNegociacion(), FormatDataUtils.DATE_FORMAT);

      if (operacion.gethNegociacion() != null) {
        // this.ichnegoc = SIBBACPTICommons.convertDateToString(operacion.gethNegociacion(), formatoHora);
        this.ichnegoc = FormatDataUtils.convertDateToString(operacion.gethNegociacion(), FormatDataUtils.TIME_FORMAT);
      } else {
        // this.ichnegoc = "000000";
        this.ichnegoc = StringHelper.returnNCeros(6);
      } // else

      if (TimeZone.getDefault().inDaylightTime(new Date())) {
        this.icuhora = mapaConstantes.get("ICUHORAVERANO");
      } else {
        this.icuhora = mapaConstantes.get("ICUHORAINVIERNO");
      } // else

      // this.icfvalor = SIBBACPTICommons.convertDateToString(operacion.getFhValor(), formatoFecha);
      this.icfvalor = FormatDataUtils.convertDateToString(operacion.getFhValor(), FormatDataUtils.DATE_FORMAT);

      this.icutiope = operacion.getSentido().toString();
      this.icuprote = operacion.getCapacidadNegociacion().toString();
      this.iccvalor = StringHelper.padStringWithSpacesRight(operacion.getIdInstrumento(), 60);
      this.icuinstru = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICUINSTRU"), 4);
      this.icuidenti = StringHelper.returnNWhiteSpaces(60);
      this.icusubya = StringHelper.returnNWhiteSpaces(4);
      this.icutipoi = StringHelper.returnNWhiteSpaces(3);
      this.icfvenci = StringHelper.returnNWhiteSpaces(8);
      this.icuderiv = StringHelper.returnNWhiteSpaces(1);
      this.icupucal = StringHelper.returnNWhiteSpaces(1);
      this.iciprcij = StringHelper.returnNCeros(17);
      this.icqprcij = StringHelper.returnNCeros(17);
      this.icipreci = StringHelper.formatBigDecimal(operacion.getPrecioUnitario(), 12, 5);
      this.icupreci = mapaConstantes.get("ICUPRECI");
      this.iccdivis = StringHelper.padStringWithSpacesRight(operacion.getIdentificadorTransaccion(), 3);
      this.icvcontr = StringHelper.formatBigDecimal(operacion.getCantidadTotal(), 12, 5);
      this.icucontr = mapaConstantes.get("ICUCONTR");
      this.iciefect = StringHelper.formatBigDecimal(operacion.getEfectivoTotal(), 13, 2);
      this.iccbicco = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCBICCO"), 13);
      this.icubicco = mapaConstantes.get("ICUBICCO");
      this.icdsiste = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICDSISTE"), 11);
      this.icusiste = mapaConstantes.get("ICUSISTE");

      // this.icnrefme =
      // StringHelper.padStringWithSpacesRight(SIBBACPTICommons.convertDateToString(operacion.getFhNegociacion(),
      // formatoFecha)
      // .concat(operacion.getId().toString()), 17);
      this.icnrefme = operacion.getIdEjecucion();

      if ('B' == (operacion.getEstOperacion())) {
        this.icuactu = "S";
      } else {
        this.icuactu = StringHelper.returnNWhiteSpaces(1);
      } // else

      this.icnreg01 = StringHelper.formatBigDecimal(new BigDecimal(String.valueOf(operacion.getTitularesH47().size())),
                                                    5, 0);

      int contadorTitulares = 1;
      H47TitularRecordBean titularBean = null;

      for (TitularesH47 titular : operacion.getTitularesH47()) {
        titularBean = new H47TitularRecordBean(contadorTitulares, titular);
        listaTitulares.add(titularBean);

        contadorTitulares++;

        if (contadorTitulares > MAX_HOLDERS) {
          break;
        } // if
      } // for (TitularesH47 titular : operacion.getTitularesH47())

      if (titularBean != null) {
        titularBean.setIcvcanti(StringHelper.formatBigDecimal(operacion.getCantidadTotal(), 12, 5));
      }

      // La linea tiene que medir 10004
      // int lineaTotal = 10004;
      // int sizeCuerpo = 436;
      // int sizeTitular = 382;
      // int fillerSize = LINE_LENGTH - BODY_LENGTH - HOLDER_LENGTH * nTitulares;
      // this.filler = StringHelper.padStringWithSpacesRight("", fillerSize);
      // this.filler = StringHelper.returnNBarraCeros(LINE_LENGTH - BODY_LENGTH - HOLDER_LENGTH * nTitulares);
      // this.filler = "";
    } catch (Exception e) {
      LOG.error("[writeIntoFile] Error: ", e);
    } // catch
  } // H47RecordBean


  /**
   * Constructor III. Llama al constructor de la clase padre.
   * 
   * @param Tmct0CnmvEjecuciones registro de la ejecucion
   * @param mapaConstantes
   * @param listDesglosesDto Lista de desgloses y titulares que tiene la ejecucion
   */
  public H47RecordBean(Tmct0CnmvEjecuciones operacionMtf, Map<String, String> mapaConstantes, List<DesgloseMtfDto> listDesglosesDto) throws Exception {
    super();
    try {
      this.iccservi = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCSERVI"), 6);
      this.iccerrme = StringHelper.returnNWhiteSpaces(3);
      this.iccforma = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCFORMA"), 3);
      this.iccentsv = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCENTSV"), 4);
      this.icfcreac = mapaConstantes.get("ICFCREAC");
      this.ichcreac = mapaConstantes.get("ICHCREAC");
      this.icdcorre = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICDCORRE"), 60);
      this.icubicen = mapaConstantes.get("ICUBICEN");
      this.iccbicen = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICCBICEN"), 40);

      this.icfnegoc = FormatDataUtils.convertDateToString(operacionMtf.getfEjecucion(), FormatDataUtils.DATE_FORMAT);

      if (operacionMtf.gethEjecucion() != null) {
        // this.ichnegoc = SIBBACPTICommons.convertDateToString(operacion.gethNegociacion(), formatoHora);
        this.ichnegoc = FormatDataUtils.convertDateToString(operacionMtf.gethEjecucion(), FormatDataUtils.TIME_FORMAT);
      } else {
        // this.ichnegoc = "000000";
        this.ichnegoc = StringHelper.returnNCeros(6);
      } // else

      if (TimeZone.getDefault().inDaylightTime(new Date())) {
        this.icuhora = mapaConstantes.get("ICUHORAVERANO");
      } else {
        this.icuhora = mapaConstantes.get("ICUHORAINVIERNO");
      } // else

      // this.icfvalor = SIBBACPTICommons.convertDateToString(operacion.getFhValor(), formatoFecha);
      this.icfvalor = FormatDataUtils.convertDateToString(operacionMtf.getfValor(), FormatDataUtils.DATE_FORMAT);

      this.icutiope = operacionMtf.getSentido().toString();
      this.icuprote = operacionMtf.getCapacidadNegociacion().toString();
      this.iccvalor = StringHelper.padStringWithSpacesRight(operacionMtf.getIdInstrumento(), 60);
      this.icuinstru = StringHelper.padStringWithSpacesRight(mapaConstantes.get("ICUINSTRU"), 4);
      this.icuidenti = StringHelper.returnNWhiteSpaces(60);
      this.icusubya = StringHelper.returnNWhiteSpaces(4);
      this.icutipoi = StringHelper.returnNWhiteSpaces(3);
      this.icfvenci = StringHelper.returnNWhiteSpaces(8);
      this.icuderiv = StringHelper.returnNWhiteSpaces(1);
      this.icupucal = StringHelper.returnNWhiteSpaces(1);
      this.iciprcij = StringHelper.returnNCeros(17);
      this.icqprcij = StringHelper.returnNCeros(17);
      this.icipreci = StringHelper.formatBigDecimal(operacionMtf.getPrecio(), 12, 5);
      this.icupreci = mapaConstantes.get("ICUPRECI");
      this.iccdivis = StringHelper.padStringWithSpacesRight(operacionMtf.getDivisa(), 3);
      this.icvcontr = StringHelper.formatBigDecimal(operacionMtf.getTitulosEjecucion(), 12, 5);
      this.icucontr = mapaConstantes.get("ICUCONTR");
      this.iciefect = StringHelper.formatBigDecimal(operacionMtf.getEfectivo(), 13, 2);
      this.iccbicco = StringHelper.padStringWithSpacesRight(operacionMtf.getSistemaNegociacion(), 13);
      this.icubicco = operacionMtf.getIndSistemaNegociacion().toString();
      this.icdsiste = StringHelper.padStringWithSpacesRight(operacionMtf.getSistemaNegociacion(), 11);
      this.icusiste = operacionMtf.getIndSistemaNegociacion().toString();

      // this.icnrefme =
      // StringHelper.padStringWithSpacesRight(SIBBACPTICommons.convertDateToString(operacion.getFhNegociacion(),
      // formatoFecha)
      // .concat(operacion.getId().toString()), 17);
      this.icnrefme = StringHelper.padStringWithSpacesRight(operacionMtf.getIdEjecucion(), 17);

      if ('B' == (operacionMtf.getEstadoOperacion())) {
        this.icuactu = "S";
      } else {
        this.icuactu = StringHelper.returnNWhiteSpaces(1);
      } 
      
      // Numero de titulares
      int contadorTitulares = 0;
      
      for (DesgloseMtfDto desgloseMtfDto : listDesglosesDto) {
        contadorTitulares = contadorTitulares + desgloseMtfDto.getListaTitulares().size();
      }

      // Campo 38 numero de titulares que se envian.
      this.icnreg01 = StringHelper.formatBigDecimal(new BigDecimal(String.valueOf(contadorTitulares)),5, 0);
      
      // Se escriben los titulares  Campos 39-51 repetido por cada titular y su representante si lo tiene.
      int iContadorTitulares = 0;       // Titulares tratados para escribir en el campo 38 
      int iContadorTitularesDesglose = 0;  // Titulares que se lleva de cada desglose para saber cual es el último
      for (DesgloseMtfDto desgloseMtfDto : listDesglosesDto) {
        iContadorTitularesDesglose = 0;
        List<Tmct0CnmvTitulares> listTitularesDesglose = desgloseMtfDto.getListaTitulares();
        Tmct0CnmvTitulares representanteTitular = null; 
        if  (!desgloseMtfDto.getListaRepresentantes().isEmpty()){
          representanteTitular = desgloseMtfDto.getListaRepresentantes().get(0);  
        }

        for (Tmct0CnmvTitulares tmct0CnmvTitulares : listTitularesDesglose) {
          iContadorTitulares++;
          iContadorTitularesDesglose++;
          H47TitularRecordBean titularBean = null;
          
          // Si el titular es igual al representante se tiene que dejar en blanco los datos del representante.
          if (representanteTitular != null && tmct0CnmvTitulares.getIdentificacion().equals(representanteTitular.getIdentificacion())){
            titularBean = new H47TitularRecordBean(iContadorTitulares,tmct0CnmvTitulares,representanteTitular);
          }else{
            titularBean = new H47TitularRecordBean(iContadorTitulares,tmct0CnmvTitulares,null);
          }
         
          // Si es el ultimo se tiene que insertar los titulos del desglose 
          if (iContadorTitularesDesglose == listTitularesDesglose.size()){
            titularBean.setIcvcanti(StringHelper.formatBigDecimal(desgloseMtfDto.getDesglose().getTitulosDesgloses(), 12, 5));
          }
          
          listaTitulares.add(titularBean);
        }
      }
    } catch (Exception e) {
      LOG.error("[writeIntoFile] Error en la escritura", e.getMessage());
      throw e;
    } // catch
  } // H47RecordBean


  
  /**
   * 
   * @param iccservi
   * @param iccerrme
   * @param iccforma
   * @param iccentsv
   * @param icfcreac
   * @param ichcreac
   * @param icdcorre
   * @param icubicen
   * @param iccbicen
   * @param icfnegoc
   * @param ichnegoc
   * @param icuhora
   * @param icfvalor
   * @param icutiope
   * @param icuprote
   * @param iccvalor
   * @param icuinstru
   * @param icuidenti
   * @param icusubya
   * @param icutipoi
   * @param icfvenci
   * @param icuderiv
   * @param icupucal
   * @param iciprcij
   * @param icqprcij
   * @param icipreci
   * @param icupreci
   * @param iccdivis
   * @param icvcontr
   * @param icucontr
   * @param iciefect
   * @param iccbicco
   * @param icubicco
   * @param icdsiste
   * @param icusiste
   * @param icnrefme
   * @param icuactu
   * @param icnreg01
   */
  public H47RecordBean(String iccservi,
                       String iccerrme,
                       String iccforma,
                       String iccentsv,
                       String icfcreac,
                       String ichcreac,
                       String icdcorre,
                       String icubicen,
                       String iccbicen,
                       String icfnegoc,
                       String ichnegoc,
                       String icuhora,
                       String icfvalor,
                       String icutiope,
                       String icuprote,
                       String iccvalor,
                       String icuinstru,
                       String icuidenti,
                       String icusubya,
                       String icutipoi,
                       String icfvenci,
                       String icuderiv,
                       String icupucal,
                       String iciprcij,
                       String icqprcij,
                       String icipreci,
                       String icupreci,
                       String iccdivis,
                       String icvcontr,
                       String icucontr,
                       String iciefect,
                       String iccbicco,
                       String icubicco,
                       String icdsiste,
                       String icusiste,
                       String icnrefme,
                       String icuactu,
                       String icnreg01) {
    super();
    this.iccservi = iccservi;
    this.iccerrme = iccerrme;
    this.iccforma = iccforma;
    this.iccentsv = iccentsv;
    this.icfcreac = icfcreac;
    this.ichcreac = ichcreac;
    this.icdcorre = icdcorre;
    this.icubicen = icubicen;
    this.iccbicen = iccbicen;
    this.icfnegoc = icfnegoc;
    this.ichnegoc = ichnegoc;
    this.icuhora = icuhora;
    this.icfvalor = icfvalor;
    this.icutiope = icutiope;
    this.icuprote = icuprote;
    this.iccvalor = iccvalor;
    this.icuinstru = icuinstru;
    this.icuidenti = icuidenti;
    this.icusubya = icusubya;
    this.icutipoi = icutipoi;
    this.icfvenci = icfvenci;
    this.icuderiv = icuderiv;
    this.icupucal = icupucal;
    this.iciprcij = iciprcij;
    this.icqprcij = icqprcij;
    this.icipreci = icipreci;
    this.icupreci = icupreci;
    this.iccdivis = iccdivis;
    this.icvcontr = icvcontr;
    this.icucontr = icucontr;
    this.iciefect = iciefect;
    this.iccbicco = iccbicco;
    this.icubicco = icubicco;
    this.icdsiste = icdsiste;
    this.icusiste = icusiste;
    this.icnrefme = icnrefme;
    this.icuactu = icuactu;
    this.icnreg01 = icnreg01;
  } // H47RecordBean

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the iccservi
   */
  public String getIccservi() {
    return iccservi;
  }

  /**
   * @return the iccerrme
   */
  public String getIccerrme() {
    return iccerrme;
  }

  /**
   * @return the iccforma
   */
  public String getIccforma() {
    return iccforma;
  }

  /**
   * @return the iccentsv
   */
  public String getIccentsv() {
    return iccentsv;
  }

  /**
   * @return the icfcreac
   */
  public String getIcfcreac() {
    return icfcreac;
  }

  /**
   * @return the ichcreac
   */
  public String getIchcreac() {
    return ichcreac;
  }

  /**
   * @return the icdcorre
   */
  public String getIcdcorre() {
    return icdcorre;
  }

  /**
   * @return the icubicen
   */
  public String getIcubicen() {
    return icubicen;
  }

  /**
   * @return the iccbicen
   */
  public String getIccbicen() {
    return iccbicen;
  }

  /**
   * @return the icfnegoc
   */
  public String getIcfnegoc() {
    return icfnegoc;
  }

  /**
   * @return the ichnegoc
   */
  public String getIchnegoc() {
    return ichnegoc;
  }

  /**
   * @return the icuhora
   */
  public String getIcuhora() {
    return icuhora;
  }

  /**
   * @return the icfvalor
   */
  public String getIcfvalor() {
    return icfvalor;
  }

  /**
   * @return the icutiope
   */
  public String getIcutiope() {
    return icutiope;
  }

  /**
   * @return the icuprote
   */
  public String getIcuprote() {
    return icuprote;
  }

  /**
   * @return the iccvalor
   */
  public String getIccvalor() {
    return iccvalor;
  }

  /**
   * @return the icuinstru
   */
  public String getIcuinstru() {
    return icuinstru;
  }

  /**
   * @return the icuidenti
   */
  public String getIcuidenti() {
    return icuidenti;
  }

  /**
   * @return the icusubya
   */
  public String getIcusubya() {
    return icusubya;
  }

  /**
   * @return the icutipoi
   */
  public String getIcutipoi() {
    return icutipoi;
  }

  /**
   * @return the icfvenci
   */
  public String getIcfvenci() {
    return icfvenci;
  }

  /**
   * @return the icuderiv
   */
  public String getIcuderiv() {
    return icuderiv;
  }

  /**
   * @return the icupucal
   */
  public String getIcupucal() {
    return icupucal;
  }

  /**
   * @return the iciprcij
   */
  public String getIciprcij() {
    return iciprcij;
  }

  /**
   * @return the icqprcij
   */
  public String getIcqprcij() {
    return icqprcij;
  }

  /**
   * @return the icipreci
   */
  public String getIcipreci() {
    return icipreci;
  }

  /**
   * @return the icupreci
   */
  public String getIcupreci() {
    return icupreci;
  }

  /**
   * @return the iccdivis
   */
  public String getIccdivis() {
    return iccdivis;
  }

  /**
   * @return the icvcontr
   */
  public String getIcvcontr() {
    return icvcontr;
  }

  /**
   * @return the icucontr
   */
  public String getIcucontr() {
    return icucontr;
  }

  /**
   * @return the iciefect
   */
  public String getIciefect() {
    return iciefect;
  }

  /**
   * @return the iccbicco
   */
  public String getIccbicco() {
    return iccbicco;
  }

  /**
   * @return the icubicco
   */
  public String getIcubicco() {
    return icubicco;
  }

  /**
   * @return the icdsiste
   */
  public String getIcdsiste() {
    return icdsiste;
  }

  /**
   * @return the icusiste
   */
  public String getIcusiste() {
    return icusiste;
  }

  /**
   * @return the icnrefme
   */
  public String getIcnrefme() {
    return icnrefme;
  }

  /**
   * @return the icuactu
   */
  public String getIcuactu() {
    return icuactu;
  }

  /**
   * @return the icnreg01
   */
  public String getIcnreg01() {
    return icnreg01;
  }

  /**
   * @return the listaTitulares
   */
  public List<H47TitularRecordBean> getListaTitulares() {
    return listaTitulares;
  }

  /**
   * @return the filler
   */
  public String getFiller() {
    return filler;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param iccservi the iccservi to set
   */
  public void setIccservi(String iccservi) {
    this.iccservi = iccservi;
  }

  /**
   * @param iccerrme the iccerrme to set
   */
  public void setIccerrme(String iccerrme) {
    this.iccerrme = iccerrme;
  }

  /**
   * @param iccforma the iccforma to set
   */
  public void setIccforma(String iccforma) {
    this.iccforma = iccforma;
  }

  /**
   * @param iccentsv the iccentsv to set
   */
  public void setIccentsv(String iccentsv) {
    this.iccentsv = iccentsv;
  }

  /**
   * @param icfcreac the icfcreac to set
   */
  public void setIcfcreac(String icfcreac) {
    this.icfcreac = icfcreac;
  }

  /**
   * @param ichcreac the ichcreac to set
   */
  public void setIchcreac(String ichcreac) {
    this.ichcreac = ichcreac;
  }

  /**
   * @param icdcorre the icdcorre to set
   */
  public void setIcdcorre(String icdcorre) {
    this.icdcorre = icdcorre;
  }

  /**
   * @param icubicen the icubicen to set
   */
  public void setIcubicen(String icubicen) {
    this.icubicen = icubicen;
  }

  /**
   * @param iccbicen the iccbicen to set
   */
  public void setIccbicen(String iccbicen) {
    this.iccbicen = iccbicen;
  }

  /**
   * @param icfnegoc the icfnegoc to set
   */
  public void setIcfnegoc(String icfnegoc) {
    this.icfnegoc = icfnegoc;
  }

  /**
   * @param ichnegoc the ichnegoc to set
   */
  public void setIchnegoc(String ichnegoc) {
    this.ichnegoc = ichnegoc;
  }

  /**
   * @param icuhora the icuhora to set
   */
  public void setIcuhora(String icuhora) {
    this.icuhora = icuhora;
  }

  /**
   * @param icfvalor the icfvalor to set
   */
  public void setIcfvalor(String icfvalor) {
    this.icfvalor = icfvalor;
  }

  /**
   * @param icutiope the icutiope to set
   */
  public void setIcutiope(String icutiope) {
    this.icutiope = icutiope;
  }

  /**
   * @param icuprote the icuprote to set
   */
  public void setIcuprote(String icuprote) {
    this.icuprote = icuprote;
  }

  /**
   * @param iccvalor the iccvalor to set
   */
  public void setIccvalor(String iccvalor) {
    this.iccvalor = iccvalor;
  }

  /**
   * @param icuinstru the icuinstru to set
   */
  public void setIcuinstru(String icuinstru) {
    this.icuinstru = icuinstru;
  }

  /**
   * @param icuidenti the icuidenti to set
   */
  public void setIcuidenti(String icuidenti) {
    this.icuidenti = icuidenti;
  }

  /**
   * @param icusubya the icusubya to set
   */
  public void setIcusubya(String icusubya) {
    this.icusubya = icusubya;
  }

  /**
   * @param icutipoi the icutipoi to set
   */
  public void setIcutipoi(String icutipoi) {
    this.icutipoi = icutipoi;
  }

  /**
   * @param icfvenci the icfvenci to set
   */
  public void setIcfvenci(String icfvenci) {
    this.icfvenci = icfvenci;
  }

  /**
   * @param icuderiv the icuderiv to set
   */
  public void setIcuderiv(String icuderiv) {
    this.icuderiv = icuderiv;
  }

  /**
   * @param icupucal the icupucal to set
   */
  public void setIcupucal(String icupucal) {
    this.icupucal = icupucal;
  }

  /**
   * @param iciprcij the iciprcij to set
   */
  public void setIciprcij(String iciprcij) {
    this.iciprcij = iciprcij;
  }

  /**
   * @param icqprcij the icqprcij to set
   */
  public void setIcqprcij(String icqprcij) {
    this.icqprcij = icqprcij;
  }

  /**
   * @param icipreci the icipreci to set
   */
  public void setIcipreci(String icipreci) {
    this.icipreci = icipreci;
  }

  /**
   * @param icupreci the icupreci to set
   */
  public void setIcupreci(String icupreci) {
    this.icupreci = icupreci;
  }

  /**
   * @param iccdivis the iccdivis to set
   */
  public void setIccdivis(String iccdivis) {
    this.iccdivis = iccdivis;
  }

  /**
   * @param icvcontr the icvcontr to set
   */
  public void setIcvcontr(String icvcontr) {
    this.icvcontr = icvcontr;
  }

  /**
   * @param icucontr the icucontr to set
   */
  public void setIcucontr(String icucontr) {
    this.icucontr = icucontr;
  }

  /**
   * @param iciefect the iciefect to set
   */
  public void setIciefect(String iciefect) {
    this.iciefect = iciefect;
  }

  /**
   * @param iccbicco the iccbicco to set
   */
  public void setIccbicco(String iccbicco) {
    this.iccbicco = iccbicco;
  }

  /**
   * @param icubicco the icubicco to set
   */
  public void setIcubicco(String icubicco) {
    this.icubicco = icubicco;
  }

  /**
   * @param icdsiste the icdsiste to set
   */
  public void setIcdsiste(String icdsiste) {
    this.icdsiste = icdsiste;
  }

  /**
   * @param icusiste the icusiste to set
   */
  public void setIcusiste(String icusiste) {
    this.icusiste = icusiste;
  }

  /**
   * @param icnrefme the icnrefme to set
   */
  public void setIcnrefme(String icnrefme) {
    this.icnrefme = icnrefme;
  }

  /**
   * @param icuactu the icuactu to set
   */
  public void setIcuactu(String icuactu) {
    this.icuactu = icuactu;
  }

  /**
   * @param icnreg01 the icnreg01 to set
   */
  public void setIcnreg01(String icnreg01) {
    this.icnreg01 = icnreg01;
  }

  /**
   * @param listaTitulares the listaTitulares to set
   */
  public void setListaTitulares(List<H47TitularRecordBean> listaTitulares) {
    this.listaTitulares = listaTitulares;
  }

  /**
   * @param filler the filler to set
   */
  public void setFiller(String filler) {
    this.filler = filler;
  }

} // H47RecordBean
