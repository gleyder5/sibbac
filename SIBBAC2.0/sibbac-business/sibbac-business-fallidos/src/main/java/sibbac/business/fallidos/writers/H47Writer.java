package sibbac.business.fallidos.writers;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.bo.ConstanteH47Bo;
import sibbac.business.fallidos.database.bo.ErrorCNMVBo;
import sibbac.business.fallidos.database.bo.OperacionH47Bo;
import sibbac.business.fallidos.database.bo.OperacionesH47MtfBo;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.fallidos.database.model.ConstanteH47;
import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.dto.DesgloseMtfDto;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.common.fileWriter.SibbacFileWriter;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

/**
 * Escribe el fichero H47.
 * 
 * @author XI316153
 * @see SibbacFileWriter
 * @see H47RecordBean
 */
@Component
public class H47Writer extends SibbacFileWriter<H47RecordBean> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(H47Writer.class);

  /** Nombre del fichero temporal que se genera. */
  private static final String H47_TEMPFIlENAME = "H47TempFile.tmp";

  /** Nombre del fichero final que se genera. */
  private static final String H47_FINALFIlENAME = "H47";

  /** Extensión de los ficheros a generar. */
  private static final String H47_FIlENAME_EXTENSION = ".DAT";
  
  

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private OperacionH47Bo operacionH47Bo;

  @Autowired
  private ConstanteH47Bo constanteH47Bo;

  @Autowired
  private Tmct0estadoBo tmct0estadoBo;

  @Autowired
  private ErrorCNMVBo errorCNMVBo;
  
  @Autowired
  private   OperacionesH47MtfBo operacionesH47MtfBo;
  
  
  @Autowired
  private   Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;
  
  
  @Autowired
  Tmct0CnmvLogDao tmct0CnmvLogDao;

  /** Carpeta donde almacenar el fichero generado. */
  @Value("${sibbac.wrappers.h47OutFolderTratados}")
  private String tratadosFolderName;

  /** Writer para escribir en el fichero. */
  private FlatFileItemWriter<H47RecordBean> writer;

  /** Delimitador de líneas. */
  private DelimitedLineAggregator<H47RecordBean> delimitedLineAggregator;

  /** Mapeador de campos del tipo de registros a escribir en el fichero. */
  private BeanWrapperFieldExtractor<H47RecordBean> fieldExtractorH47;

  /** Nombre del fichero temporal. */
  private File tempFile;

  /** Fecha de ejecución. El fichero final ira con el timestamp de este objeto. */
  private Date ahora;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Inicializa los atributos de la clase.
   * 
   * @param folderName Carpeta donde almacenar el fichero generado.
   */
  @Autowired
  public H47Writer(@Value("${sibbac.wrappers.h47OutFolder}") String folderName) {
    super(folderName);

    initalizeFieldExtractorH47();
    initializeDelimitedLineAggregator();
  } // H47Writer

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLMENTADOS

  /*
   * @see sibbac.business.wrappers.common.fileWriter.SibbacFileWriter#writeIntoFile()
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { SIBBACBusinessException.class, Exception.class
  })
  public boolean writeIntoFile() throws SIBBACBusinessException {
    LOG.debug("[writeIntoFile] Inicio ...");
    
    Boolean bResultado = Boolean.TRUE;
    
    // Creamos un registro en el log.
    Tmct0CnmvLog logProceso = new Tmct0CnmvLog();
    logProceso.setAuditUser("SISTEMA");
    logProceso.setAuditDate(new Date());
    logProceso.setProceso(Constantes.PROCESO_GENERAR_FICHERO);
    logProceso.setContadorAltas(BigInteger.ZERO);
    logProceso.setContadorBajas(BigInteger.ZERO);
    logProceso.setIncidencia('N');

    StringBuilder sbComentario = new StringBuilder();
    
    // Operaciones h47
    List<OperacionH47> operacionesList;
    try {
      operacionesList = operacionH47Bo.findAllForFile();
    } catch (Exception e) {
      throw new SIBBACBusinessException("Incidencia consultando operaciones H47 a enviar", e);
    } // catch

    
    // MFG 06/06/2017  Se incluye el tratamiento de las nuevas operaciones h47 de las MTFs
    // Primero se actualizan las pendientes de enviar que tienen ya todos los titulos.
    this.operacionesH47MtfBo.actualizaEjecucionesPendientes();
    
    
    // Se obtiene la lista de  OPERACIONES MTFs pendientes de enviar que no estan en las camaras excluidas
    List<Tmct0CnmvEjecuciones> listOperacionesMtf;
    listOperacionesMtf = this.tmct0CnmvEjecucionesDao.findPendientesEnviar(EstadosEnumerados.H47.PTE_ENVIAR.getId(), this.operacionesH47MtfBo.obtenerCamarasExcluidas());
    
    
    // Si alguna de las dos listas no es vacia comienzo el tratamiento que crea el fichero
    if ((operacionesList != null && !operacionesList.isEmpty())  || (listOperacionesMtf != null && !listOperacionesMtf.isEmpty())) {
      LOG.debug("[writeIntoFile] Se han encontrado '{}' operaciones a enviar ...", operacionesList.size());
      try {
        tempFile = new File(folderName + File.separator + H47_TEMPFIlENAME);
        tempFile.getParentFile().mkdirs();
        tempFile.createNewFile();
      } catch (Exception e) {
        sbComentario.append("Incidencia creando fichero temporal H47 Proceso Abortado - Contacte con IT : Error: ").append(tempFile).append(" .... ").append(e.getMessage());
        throw new SIBBACBusinessException("Incidencia creando fichero temporal H47 '" + tempFile + "'", e);
      } // catch

      try {
        ahora = new Date();

        Map<String, String> mapaConstantes = new HashMap<String, String>();
        for (ConstanteH47 constante : constanteH47Bo.findAll()) {
          mapaConstantes.put(constante.getCampo(), constante.getValor());
        } // for

        // Se incluyen la fecha y la hora en el mapa de constantes
        mapaConstantes.put("ICFCREAC", FormatDataUtils.convertDateToString(ahora, FormatDataUtils.DATE_FORMAT));
        mapaConstantes.put("ICHCREAC", FormatDataUtils.convertDateToString(ahora, FormatDataUtils.TIME_FORMAT));

        List<H47RecordBean> listaRecordBeans = new ArrayList<H47RecordBean>();
        List<OperacionH47> listaOperacionesEnviadas = new ArrayList<OperacionH47>();
        List<Tmct0CnmvEjecuciones> listaOperacionesMtfsEnviada = new ArrayList<Tmct0CnmvEjecuciones>();

        H47RecordBean h47Bean;
        for (OperacionH47 operacion : operacionesList) {
          h47Bean = new H47RecordBean(operacion, mapaConstantes);
          listaRecordBeans.add(h47Bean);
          listaOperacionesEnviadas.add(operacion);
        } // for

        
        // MFG 06/06/2017  Se recorre la lista de las operaciones MTFs y se incluyen los bean y los objetos para actualizar.
         for (Tmct0CnmvEjecuciones tmct0CnmvEjecuciones : listOperacionesMtf) {
           try {
             
             List<DesgloseMtfDto> listDesglosesDto = null;
             
             listDesglosesDto = this.operacionesH47MtfBo.getDesglosesEjecucion(tmct0CnmvEjecuciones);

             // Una vez que se tienen todos los desgloses y sabemos si son propios o ajenos se obtiene el valor de la capacidad de negociacion
             BigDecimal iNumPropios = new BigDecimal("0");
             BigDecimal iNumAjenos = new BigDecimal("0");
             
             for (DesgloseMtfDto desgloseMtfDto : listDesglosesDto) {
              if (desgloseMtfDto.getbTitulosPropios()){
                iNumPropios = iNumPropios.add(desgloseMtfDto.getDesglose().getTitulosDesgloses());
              }else{
                iNumAjenos = iNumAjenos.add(desgloseMtfDto.getDesglose().getTitulosDesgloses());
              }
            }
             
            if  (iNumPropios.compareTo(iNumAjenos) >= 0){
              tmct0CnmvEjecuciones.setCapacidadNegociacion('P');
            }else{
              tmct0CnmvEjecuciones.setCapacidadNegociacion('A');
            }
             
             // Al fichero no se pueden enviar mas del numero de titulares indicado sin incluir los repetidos
             int contadorTitulares = 0;
             
             for (DesgloseMtfDto desgloseMtfDto : listDesglosesDto) {
               contadorTitulares = contadorTitulares + desgloseMtfDto.getListaTitulares().size();
             }

             // Si el numero es mayor, se quitan titulares segun las especificaciones dadas.  
             if (contadorTitulares > H47RecordBean.MAX_HOLDERS){
               // De todos los titulares nos tenemos que quedar con los que se pueden enviar, los primeros de cada descuadre hasta complentar el máximo
               listDesglosesDto = this.operacionesH47MtfBo.obtenerTitularesEnvio(listDesglosesDto, H47RecordBean.MAX_HOLDERS);
             }


             
             
             h47Bean = new H47RecordBean(tmct0CnmvEjecuciones, mapaConstantes,listDesglosesDto);
             listaRecordBeans.add(h47Bean);
             listaOperacionesMtfsEnviada.add(tmct0CnmvEjecuciones);
          } catch (Exception e) {
              // No se añade el registro porque no se ha creado bien el bean.
            sbComentario.append(" Error No se ha podido preparar y escribir en el fichero la información para la ejecucion con id: ").append(tmct0CnmvEjecuciones.getId()).append( " Excepcion " ).append(e.getMessage()) ;
          }
           
        }
        
        
        if (listaRecordBeans.size() > 0) {
          initalizeWriter(tempFile);
          writer.write(listaRecordBeans);
          writer.close();

          LOG.debug("[writeIntoFile] Actualizando estado de las operaciones h47 ...");
          Tmct0estado estadoEnviadaH47 = tmct0estadoBo.findById(EstadosEnumerados.H47.ENVIADA.getId());
          
          for (OperacionH47 operacion : listaOperacionesEnviadas) {
            operacion.setEstadoEnvio(estadoEnviadaH47);
            operacionH47Bo.save(operacion);
          } // for

          LOG.debug("[writeIntoFile] Actualizando estado de las operaciones h47 MTFs ...");
          
          for (Tmct0CnmvEjecuciones tmct0CnmvEjecuciones : listaOperacionesMtfsEnviada) {
            tmct0CnmvEjecuciones.setCdEstadoEnvio(estadoEnviadaH47);
            
            // Se actualiza la capacidad de negociacion
            
            if (tmct0CnmvEjecuciones.getEstadoOperacion() == 'A'){
              tmct0CnmvEjecuciones.setfEnvioAlta(new Date());
              logProceso.setContadorAltas(logProceso.getContadorAltas().add(BigInteger.ONE));
            }
            

            if (tmct0CnmvEjecuciones.getEstadoOperacion() == 'B'){
              tmct0CnmvEjecuciones.setfEnvioBaja(new Date());
              logProceso.setContadorBajas(logProceso.getContadorBajas().add(BigInteger.ONE));
            }
            
            
            
            this.tmct0CnmvEjecucionesDao.save(tmct0CnmvEjecuciones);
          } 

        } else {
          LOG.warn("[writeIntoFile] No se han generado operaciones para enviar ...");
          sbComentario.append("No se han encontrado operaciones a enviar ");
          tempFile.delete();
        } // else
      } catch (Exception e) {
        try {
          writer.close();
        } catch (Exception e1) {
          // Se ignora
        } // catch

        tempFile.delete();
        LOG.debug("[writeIntoFile] Incidencia escribiendo fichero temporal H47 '" + tempFile + "' ... " + e.getMessage(), e);
        sbComentario.append("[writeIntoFile] Incidencia escribiendo fichero temporal H47 '").append(tempFile).append(" ... ").append(e.getMessage());
        throw new SIBBACBusinessException("Incidencia escribiendo fichero temporal H47 '" + tempFile + "'", e);
      } // catch
    } else {
      LOG.debug("[writeIntoFile] No se han encontrado operaciones a enviar ... Fin ...");
      sbComentario.append("No se han encontrado operaciones a enviar ");
      bResultado = Boolean.FALSE;

    } // else

    
    // Cuando se finaliza la ejecucion se genera el log
    // Cuando se finaliza la ejecucion de todos los hilos se registra en el log la ejecucion
    if (sbComentario.length() > 0){
      logProceso.setIncidencia('S');
      if (sbComentario.length() > 1000){
        logProceso.setComentario(sbComentario.toString().substring(0,999));            
      }else{
        logProceso.setComentario(sbComentario.toString());  
      }

    }else{
      logProceso.setIncidencia('N');
    }
    this.tmct0CnmvLogDao.save(logProceso);
    
   
    LOG.debug("[writeIntoFile] Fin ...");
    return bResultado;
  } // writeIntoFile

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Renombra el fichero temporal especificado a su nombre y ruta finales.
   * 
   * @param tempFile Fichero a renombrar.
   * @param ahora Momento de generación del fichero.
   * @throws SIBBACBusinessException Si ocurre algún error al renombrar el fichero.
   */
  // public void renameTempFile(File tempFile, Date ahora) throws SIBBACBusinessException {
  public void renameTempFile() throws SIBBACBusinessException {
    LOG.debug("[renameTempFile] Inicio ...");

    File finalFile = new File(folderName + File.separator + H47_FINALFIlENAME
                              + FormatDataUtils.convertDateToString(ahora, FormatDataUtils.TIMESTAMP_FORMAT) + H47_FIlENAME_EXTENSION);
    if (tempFile.exists()) {
      if (tempFile.renameTo(finalFile)) {
        try {
          File tratadosFinalFile = new File(tratadosFolderName + File.separator + finalFile.getName());
          tratadosFinalFile.getParentFile().mkdirs();
          FileUtils.copyFile(finalFile, tratadosFinalFile);
        } catch (IOException e) {
          throw new SIBBACBusinessException("Incidencia copiando fichero a la carpeta tratados ...", e);
        } // catch
      } else {
        throw new SIBBACBusinessException("Incidencia renombrando fichero temporal ...");
      } // else
    } else {
      LOG.warn("[renameTempFile] El fichero temporal H47 '" + tempFile + "' no existe");
    } // else
    LOG.debug("[renameTempFile] Fin ...");
  } // renameTempFile

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Inicializa el array de campos del fichero.
   */
  private void initalizeFieldExtractorH47() {
    LOG.debug("[initalizeFieldExtractorH47] Inicio ...");
    fieldExtractorH47 = new BeanWrapperFieldExtractor<H47RecordBean>();
    // fieldExtractorH47.setNames(new String[] { "iccservi", "iccerrme", "iccforma", "iccentsv", "icfcreac", "ichcreac",
    // "icdcorre", "icubicen", "iccbicen", "icfnegoc", "ichnegoc", "icuhora", "icfvalor", "icutiope", "icuprote",
    // "iccvalor", "icuinstru", "icuidenti", "icusubya", "icutipoi", "icfvenci", "icuderiv", "icupucal", "iciprcij",
    // "icqprcij", "icipreci", "icupreci", "iccdivis", "icvcontr", "icucontr", "iciefect", "iccbicco", "icubicco",
    // "icdsiste", "icusiste", "icnrefme", "icuactu", "icnreg01", "listaTitulares", "filler"
    // });
    fieldExtractorH47.setNames(new String[] { "iccservi", "iccerrme", "iccforma", "iccentsv", "icfcreac", "ichcreac", "icdcorre", "icubicen", "iccbicen", "icfnegoc", "ichnegoc", "icuhora", "icfvalor", "icutiope", "icuprote", "iccvalor", "icuinstru", "icuidenti", "icusubya", "icutipoi", "icfvenci", "icuderiv", "icupucal", "iciprcij", "icqprcij", "icipreci", "icupreci", "iccdivis", "icvcontr", "icucontr", "iciefect", "iccbicco", "icubicco", "icdsiste", "icusiste", "icnrefme", "icuactu", "icnreg01", "listaTitulares"
    });
    LOG.debug("[initalizeFieldExtractorH47] Fin ...");
  } // initalizeFieldExtractorH47

  /**
   * Inicializa el delimitador de líneas.
   */
  private void initializeDelimitedLineAggregator() {
    LOG.debug("[initializeDelimitedLineAggregator] Inicio ...");
    delimitedLineAggregator = new DelimitedLineAggregator<H47RecordBean>();
    delimitedLineAggregator.setDelimiter("");
    delimitedLineAggregator.setFieldExtractor(fieldExtractorH47);
    LOG.debug("[initializeDelimitedLineAggregator] Fin ...");
  } // initializeDelimitedLineAggregator

  /**
   * Inicializa el writer para escribir en el fichero.
   * 
   * @param file Fichero en el que escribir.
   */
  private void initalizeWriter(File file) {
    LOG.debug("[initalizeWriter] Inicio ...");
    
    writer = new FlatFileItemWriter<H47RecordBean>();
    writer.setEncoding(StandardCharsets.ISO_8859_1.name());
    writer.setResource(new FileSystemResource(file));
    writer.open(new ExecutionContext());
    writer.setLineAggregator(delimitedLineAggregator);
    LOG.debug("[initalizeWriter] Fin ...");
  } // initalizeWriter

} // H47Writer
