package sibbac.business.fallidos.tasks;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.pti.messages.an.AN;

/**
 * Task para procesar los TI's de Intermediarios Financieros.
 * <p/>
 * 
 * Es llamada desde {@link PTIFilesJob}.<br/>
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public class PTIAnFallidoJob extends FallidosTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(PTIAnFallidoJob.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  // /** Business object para la tabla <code>TMCT0_OPERACION_FALLIDO</code>. */
  // @Autowired
  // private OperacionFallidoBo operacionFallidoBo;

  // /** Business object para la tabla <code>TMCT0_AN_FALLIDO</code>. */
  // @Autowired
  // private AnFallidoBo anFallidoBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public PTIAnFallidoJob() {
    LOG.debug("[PTIAnFallidoJob<Construct>] Construct time: " + new Date());
  } // PTIAnFallidoJob

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /**
   * Lógica principal del proceso.
   */
  @Override
  public void executeTask() {
    LOG.debug("[PTIAnFallidoJob<execute>] Start time: " + new Date());

    @SuppressWarnings("unchecked")
    List<AN> anFallidos = (List<AN>) jobDataMap.get("listAnFallidos");
    if (anFallidos != null && !anFallidos.isEmpty()) {
      LOG.info("[PTIAnFallidoJob<execute>] Total AN fallidos a procesar: {}", anFallidos.size());
      // operacionFallidoBo.createOperacionFallidoFromAns( anFallidos );
    } else {
      LOG.warn("[PTIAnFallidoJob<execute>] lista de fallidos vacia");
    }
    // anFallidoBo.createAnFallidoFromAns(anFallidos);
    LOG.debug("[PTIAnFallidoJob<execute>] End time: " + new Date());
  } // execute

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return null;
  }

} // PTIAnFallidoJob
