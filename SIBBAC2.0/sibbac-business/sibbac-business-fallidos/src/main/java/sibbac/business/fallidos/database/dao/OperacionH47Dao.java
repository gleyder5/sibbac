package sibbac.business.fallidos.database.dao;

import java.math.BigInteger;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.OperacionH47;

/**
 * Data access object para la clase OperacionH47.
 * 
 * @author XI316153
 * @see JpaRepository
 * @see OperacionH47
 * @see JpaSpecificationExecutor
 */
@Repository
public interface OperacionH47Dao extends JpaRepository<OperacionH47, Long>, JpaSpecificationExecutor<OperacionH47> {

  /**
   * 
   * @param identificadorTransaccion
   * @param fhNegociacion
   * @param sentido
   * @return
   */
  OperacionH47 findFirstByIdentificadorTransaccionAndFhNegociacionAndSentido(String identificadorTransaccion,
                                                                             Date fhNegociacion,
                                                                             Character sentido);

  /**
   * 
   * @param id
   * @param fhNegociacion
   * @return
   */
  OperacionH47 findByIdAndFhNegociacion(Long id, Date fhNegociacion);

  @Query("SELECT  SUM(A.titulos) FROM OperacionH47 A WHERE A.nbooking = :nbooking and A.nucnfliq = :nucnfliq and A.nucnfclt = :nucnfclt AND A.capacidadNegociacion = :capacidadNegociadora and  A.estOperacion = :estadoOperacion ")
  public BigInteger findNumTitulosDesgloseActuales(@Param("nbooking") String nbooking,
                                                   @Param("nucnfliq") Short nucnfliq,
                                                   @Param("nucnfclt") String nucnfclt,
                                                   @Param("capacidadNegociadora") Character capacidadNegociadora,
                                                   @Param("estadoOperacion") Character estadoOperacion);

  @Query("SELECT  SUM(A.titulos) FROM OperacionH47 A WHERE A.id != :idMovimiento and A.nbooking = :nbooking and A.nucnfliq = :nucnfliq and A.nucnfclt = :nucnfclt AND A.capacidadNegociacion = :capacidadNegociadora and  A.estOperacion = :estadoOperacion ")
  public BigInteger findNumTitulosDesgloseMovimiento(@Param("nbooking") String nbooking,
                                                     @Param("nucnfliq") Short nucnfliq,
                                                     @Param("nucnfclt") String nucnfclt,
                                                     @Param("capacidadNegociadora") Character capacidadNegociadora,
                                                     @Param("estadoOperacion") Character estadoOperacion,
                                                     @Param("idMovimiento") Long idMovimiento);

} // OperacionH47Dao

