package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link sibbac.business.fallidos.database.model.FixmlFallido}. Entity: "fixml_fallidos".
 */
@Entity
@Table(name = DBConstants.FALLIDOS.FALLIDOS_FIXML)
public class FixmlFallido extends ATable<FixmlFallido> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 8922463762647365437L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Numero Operacion. TrdId1. */
  @Column(name = "NUMERO_OPERACION", nullable = true, length = 16)
  private String numeroOperacion;

  /** Volumen de la operacion. */
  @Column(name = "VOLUMEN_OPERACION", nullable = true, precision = 18, scale = 6)
  private BigDecimal volumenOperacion;

  /** Identificador instruccion origen. OrigTrdId. */
  @Column(name = "INSTRUCCION_ORIGEN", nullable = true, length = 16)
  private String instruccionOrigen;

  /** Numero Operacion inicial. TrdId2. */
  @Column(name = "NUMERO_OPERACION_INICIAL", nullable = true, length = 16)
  private String numeroOperacionInicial;

  /** Referencia común a las dos patas del prestamo. */
  @Column(name = "REFERENCIA_PRESTAMO", nullable = true, length = 16)
  private String referenciaPrestamo;

  /** Volumen vivo de la operación. */
  @Column(name = "VOLUMEN_VIVO_OPERACION", nullable = true, precision = 18, scale = 6)
  private BigDecimal volumenVivoOperacion;

  /** Precio de la operación. */
  @Column(name = "PRECIO_OPERACION", nullable = true, precision = 13, scale = 6)
  private BigDecimal precioOperacion;

  /** Divisa. */
  @Column(name = "DIVISA", nullable = true, length = 3)
  private String divisa;

  /** BizDt. */
  @Column(name = "FECHA_LIQUIDACION", nullable = true)
  @Temporal(TemporalType.DATE)
  private Date fechaLiquidacion;

  /** Importe efectivo Instrucción. */
  @Column(name = "IMPORTE_EFECTIVO_INSTRUCCION", nullable = true, precision = 15, scale = 2)
  private BigDecimal importeEfectivoInstruccion;

  /** Tipo de operación de la ECC. */
  @Column(name = "TIPO_OPERACION_ECC", nullable = true)
  private String tipoOperacionEcc;

  /** Código segmento de la ECC. */
  @Column(name = "SEGMENTO", nullable = true, length = 2)
  private String segmento;

  /** Código ISIN. */
  @Column(name = "ISIN", nullable = true, length = 12)
  private String isin;

  /** Importe efectivo vivo de la operación. */
  @Column(name = "IMPORTE_EFECTIVO_VIVO_OPERACION", nullable = true, precision = 15, scale = 2)
  private BigDecimal importeEfectivoVivoOperacion;

  /** Signo de la posicion de valores. */
  @Column(name = "SENTIDO", nullable = true)
  private String sentido;

  /** Indicador de posicion. */
  @Column(name = "INDICADOR_POSICION", nullable = true)
  private String indicadorPosicion;

  /** Porcentaje aplicable a la penalización. */
  @Column(name = "PORCENTAJE_PENALIZACION", nullable = true)
  private Float porcentajePenalizacion;

  /** RSN. */
  @Column(name = "RSN", nullable = true)
  private String rsn;

  /** Referencia al evento corporativo. */
  @Column(name = "REF_EVENTO_CORPORATIVO", nullable = true, length = 35)
  private String refEventoCorporativo;

  /** Fecha y hora, en formato UTC, en que se realizó la transacción. */
  @Column(name = "TRX_TM", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date trxTm;

  /** Precio de revaluación del colateral de un préstamo. */
  @Column(name = "CLT_TRD_PX", nullable = true, precision = 13, scale = 6)
  private BigDecimal clrTrdPx;

  /** Fecha de liquidación. */
  @Column(name = "STTLDT", nullable = true)
  @Temporal(TemporalType.DATE)
  private Date sttlDt;

  /** Hora de generación del fichero. */
  @Column(name = "SNT", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Date snt;

  /** Fichero. */
  @Column(name = "FILE_TYPE", nullable = true, length = 20)
  private String fileType;

  /** si ha sido procesado */
  @Column(name = "PROCESADO", nullable = false)
  private Boolean procesado;

  /** Fichero. */
  @Column(name = "ILIQDCV", nullable = true, length = 16)
  private String iliqdcv;

  /** Fichero. */
  @Column(name = "ILQS3", nullable = true, length = 16)
  private String ilqs3;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public FixmlFallido() {
    procesado = Boolean.FALSE;
  } // FixmlFallido

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "FixmlFallido [numeroOperacion=" + numeroOperacion + ", volumenOperacion=" + volumenOperacion + ", instruccionOrigen=" + instruccionOrigen
           + ", numeroOperacionInicial=" + numeroOperacionInicial + ", referenciaPrestamo=" + referenciaPrestamo + ", volumenVivoOperacion="
           + volumenVivoOperacion + ", precioOperacion=" + precioOperacion + ", divisa=" + divisa + ", BizDt=" + fechaLiquidacion
           + ", importeEfectivoInstruccion=" + importeEfectivoInstruccion + ", tipoOperacionEcc=" + tipoOperacionEcc + ", segmento=" + segmento
           + ", isin=" + isin + ", importeEfectivoVivoOperacion=" + importeEfectivoVivoOperacion + ", sentido=" + sentido + ", indicadorPosicion="
           + indicadorPosicion + ", porcentajePenalizacion=" + porcentajePenalizacion + ", rsn=" + rsn + ", refEventoCorporativo="
           + refEventoCorporativo + ", trxTm=" + trxTm + ", clrTrdPx=" + clrTrdPx + ", sttlDt=" + sttlDt + ", snt=" + snt + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the numeroOperacion
   */
  public String getNumeroOperacion() {
    return numeroOperacion;
  }

  /**
   * @return the volumenOperacion
   */
  public BigDecimal getVolumenOperacion() {
    return volumenOperacion;
  }

  /**
   * @return the instruccionOrigen
   */
  public String getInstruccionOrigen() {
    return instruccionOrigen;
  }

  /**
   * @return the numeroOperacionInicial
   */
  public String getNumeroOperacionInicial() {
    return numeroOperacionInicial;
  }

  /**
   * @return the referenciaPrestamo
   */
  public String getReferenciaPrestamo() {
    return referenciaPrestamo;
  }

  /**
   * @return the volumenVivoOperacion
   */
  public BigDecimal getVolumenVivoOperacion() {
    return volumenVivoOperacion;
  }

  /**
   * @return the precioOperacion
   */
  public BigDecimal getPrecioOperacion() {
    return precioOperacion;
  }

  /**
   * @return the divisa
   */
  public String getDivisa() {
    return divisa;
  }

  /**
   * @return the fechaLiquidacion
   */
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  /**
   * @return the importeEfectivoInstruccion
   */
  public BigDecimal getImporteEfectivoInstruccion() {
    return importeEfectivoInstruccion;
  }

  /**
   * @return the tipoOperacionEcc
   */
  public String getTipoOperacionEcc() {
    return tipoOperacionEcc;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the importeEfectivoVivoOperacion
   */
  public BigDecimal getImporteEfectivoVivoOperacion() {
    return importeEfectivoVivoOperacion;
  }

  /**
   * @return the sentido
   */
  public String getSentido() {
    return sentido;
  }

  /**
   * @return the indicadorPosicion
   */
  public String getIndicadorPosicion() {
    return indicadorPosicion;
  }

  /**
   * @return the porcentajePenalizacion
   */
  public float getPorcentajePenalizacion() {
    return porcentajePenalizacion;
  }

  /**
   * @return the rsn
   */
  public String getRsn() {
    return rsn;
  }

  /**
   * @return the refEventoCorporativo
   */
  public String getRefEventoCorporativo() {
    return refEventoCorporativo;
  }

  /**
   * @return the trxTm
   */
  public Date getTrxTm() {
    return trxTm;
  }

  /**
   * @return the clrTrdPx
   */
  public BigDecimal getClrTrdPx() {
    return clrTrdPx;
  }

  /**
   * @return the sttlDt
   */
  public Date getSttlDt() {
    return sttlDt;
  }

  /**
   * @return the snt
   */
  public Date getSnt() {
    return snt;
  }

  /**
   * @return the fileType
   */
  public String getFileType() {
    return fileType;
  }

  /**
   * 
   * @return
   */
  public Boolean isProcesado() {
    return procesado;
  }

  /**
   * @return the iliqdcv
   */
  public String getIliqdcv() {
    return iliqdcv;
  }

  /**
   * @return the procesado
   */
  public Boolean getProcesado() {
    return procesado;
  }

  /**
   * @return the ilqs3
   */
  public String getIlqs3() {
    return ilqs3;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param numeroOperacion the numeroOperacion to set
   */
  public void setNumeroOperacion(String numeroOperacion) {
    this.numeroOperacion = numeroOperacion;
  }

  /**
   * @param volumenOperacion the volumenOperacion to set
   */
  public void setVolumenOperacion(BigDecimal volumenOperacion) {
    this.volumenOperacion = volumenOperacion;
  }

  /**
   * @param instruccionOrigen the instruccionOrigen to set
   */
  public void setInstruccionOrigen(String instruccionOrigen) {
    this.instruccionOrigen = instruccionOrigen;
  }

  /**
   * @param numeroOperacionInicial the numeroOperacionInicial to set
   */
  public void setNumeroOperacionInicial(String numeroOperacionInicial) {
    this.numeroOperacionInicial = numeroOperacionInicial;
  }

  /**
   * @param referenciaPrestamo the referenciaPrestamo to set
   */
  public void setReferenciaPrestamo(String referenciaPrestamo) {
    this.referenciaPrestamo = referenciaPrestamo;
  }

  /**
   * @param volumenVivoOperacion the volumenVivoOperacion to set
   */
  public void setVolumenVivoOperacion(BigDecimal volumenVivoOperacion) {
    this.volumenVivoOperacion = volumenVivoOperacion;
  }

  /**
   * @param precioOperacion the precioOperacion to set
   */
  public void setPrecioOperacion(BigDecimal precioOperacion) {
    this.precioOperacion = precioOperacion;
  }

  /**
   * @param divisa the divisa to set
   */
  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  /**
   * @param fechaLiquidacion the fechaLiquidacion to set
   */
  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  /**
   * @param importeEfectivoInstruccion the importeEfectivoInstruccion to set
   */
  public void setImporteEfectivoInstruccion(BigDecimal importeEfectivoInstruccion) {
    this.importeEfectivoInstruccion = importeEfectivoInstruccion;
  }

  /**
   * @param tipoOperacionEcc the tipoOperacionEcc to set
   */
  public void setTipoOperacionEcc(String tipoOperacionEcc) {
    this.tipoOperacionEcc = tipoOperacionEcc;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param importeEfectivoVivoOperacion the importeEfectivoVivoOperacion to set
   */
  public void setImporteEfectivoVivoOperacion(BigDecimal importeEfectivoVivoOperacion) {
    this.importeEfectivoVivoOperacion = importeEfectivoVivoOperacion;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(String sentido) {
    this.sentido = sentido;
  }

  /**
   * @param indicadorPosicion the indicadorPosicion to set
   */
  public void setIndicadorPosicion(String indicadorPosicion) {
    this.indicadorPosicion = indicadorPosicion;
  }

  /**
   * @param porcentajePenalizacion the porcentajePenalizacion to set
   */
  public void setPorcentajePenalizacion(Float porcentajePenalizacion) {
    this.porcentajePenalizacion = porcentajePenalizacion;
  }

  /**
   * @param rsn the rsn to set
   */
  public void setRsn(String rsn) {
    this.rsn = rsn;
  }

  /**
   * @param refEventoCorporativo the refEventoCorporativo to set
   */
  public void setRefEventoCorporativo(String refEventoCorporativo) {
    this.refEventoCorporativo = refEventoCorporativo;
  }

  /**
   * @param trxTm the trxTm to set
   */
  public void setTrxTm(Date trxTm) {
    this.trxTm = trxTm;
  }

  /**
   * @param clrTrdPx the clrTrdPx to set
   */
  public void setClrTrdPx(BigDecimal clrTrdPx) {
    this.clrTrdPx = clrTrdPx;
  }

  /**
   * @param sttlDt the sttlDt to set
   */
  public void setSttlDt(Date sttlDt) {
    this.sttlDt = sttlDt;
  }

  /**
   * @param snt the snt to set
   */
  public void setSnt(Date snt) {
    this.snt = snt;
  }

  /**
   * @param fileType the fileType to set
   */
  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  /**
   * @param procesado the procesado to set
   */
  public void setProcesado(Boolean procesado) {
    this.procesado = procesado;
  }

  /**
   * @param iliqdcv the iliqdcv to set
   */
  public void setIliqdcv(String iliqdcv) {
    this.iliqdcv = iliqdcv;
  }

  /**
   * @param ilqs3 the iliqs3 to set
   */
  public void setIlqs3(String ilqs3) {
    this.ilqs3 = ilqs3;
  }

} // FixmlFallido
