package sibbac.business.fallidos.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.ConstanteH47Dao;
import sibbac.business.fallidos.database.model.ConstanteH47;
import sibbac.database.bo.AbstractBo;


@Service
public class ConstanteH47Bo extends AbstractBo< ConstanteH47, Long, ConstanteH47Dao > {

	
	public ConstanteH47 findByCampo(String campo){
		return this.dao.findByCampo(campo);
	}
}
