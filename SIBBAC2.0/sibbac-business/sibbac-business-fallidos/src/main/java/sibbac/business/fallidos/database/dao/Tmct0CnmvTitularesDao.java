package sibbac.business.fallidos.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;

@Repository
public interface Tmct0CnmvTitularesDao extends JpaRepository<Tmct0CnmvTitulares, BigInteger> {

	@Query("SELECT tit from Tmct0CnmvTitulares tit WHERE tit.desglose.id = :idDesglose")
    public List<Tmct0CnmvTitulares> findByDesglose(@Param("idDesglose") BigInteger idDesglose );

}
