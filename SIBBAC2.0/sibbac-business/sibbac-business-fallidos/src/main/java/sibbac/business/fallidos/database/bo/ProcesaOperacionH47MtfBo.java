package sibbac.business.fallidos.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.Tmct0CnmvDesglosesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvTitularesDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.dto.OperacionMtfDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0desgloseCamaraDao;
import sibbac.business.wrappers.database.dao.Tmct0ejeDao;
import sibbac.business.wrappers.database.model.Tmct0afi;

@Service
public class ProcesaOperacionH47MtfBo {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaOperacionH47MtfBo.class);
  

  @Value("${cdestadoasig.mtf.creation}")
  private Integer iCdEstadoAsignacion;    

  
  @Value("${cdestadoenvio.mtf.creation}")
  private Integer iCdEstadoEnvio;   
  
  
  @Autowired
  Tmct0CnmvDesglosesDao tmct0CnmvDesglosesDao;
    
  @Autowired
  Tmct0estadoDao tmct0estadoDao;
  
  @Autowired
  Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;
  
  @Autowired
  Tmct0CnmvTitularesDao  tmct0CnmvTitularesDao; 
  
  @Autowired
  Tmct0desgloseCamaraDao tmct0desgloseCamaraDao;
  
  @Autowired
  Tmct0afiDao  tmct0afiDao;

  @Autowired
  Tmct0ejeDao  tmct0ejeDao;
  
  @Autowired
  OperacionesH47MtfBo operacionesH47MtfBo;

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void trataOperacionMtfDTO(OperacionMtfDTO operacionMtfDTO) {
    char sentidoCorregido;
    
      // Se obtiene el estado PTE de enviar
      Tmct0estado estadoPte    = tmct0estadoDao.findOne(EstadosEnumerados.H47.PTE_ENVIAR.getId());

      // A partir del DTO se obtiene la ejecucion y desglose si ya existieran.
      // Se busca la ejecución con estado = 'A' y los siguientes valores
      sentidoCorregido = Tmct0CnmvEjecuciones.corrigeSentido(operacionMtfDTO.getSentido());
      Tmct0CnmvEjecuciones entEjecucion = tmct0CnmvEjecucionesDao.findByIdEjecucion(operacionMtfDTO.getNurefexemkt(),
                                                                                    operacionMtfDTO.getFeejecuc(),
                                                                                    operacionMtfDTO.getCd_plataforma_negociacion(),
                                                                                    sentidoCorregido);
      
      

      // Si existe ejecución se busca el desglose
      BigInteger idDesglose = null;
      if (entEjecucion != null) {
        idDesglose = tmct0CnmvDesglosesDao.findIdByDatosAlc(operacionMtfDTO.getNuorden(),
                                                            operacionMtfDTO.getNbooking(),
                                                            operacionMtfDTO.getNucnfclt(),
                                                            operacionMtfDTO.getNucnfliq(),
                                                            operacionMtfDTO.getIddesglosecamara(), entEjecucion.getId());
      }

      if (operacionMtfDTO.getCdestadoasig() >= iCdEstadoAsignacion) { // Se procesa el alta
        // ******************* ALTA DE UN DESGLOSE *********************

        Boolean bAltaEjecucion = false; // Para ver si la ejecucion es nueva o ya existe.
        
        // 1- Comprobamos si el desglose existe.
        if (idDesglose == null) {
          // Desglose NO existe, se procede al alta del desglose, junto con su ejecución si no existiera ya y sus
          // titulares.
          if (entEjecucion == null) {
            entEjecucion = creaTmct0CnmvEjecucionesWithDto(operacionMtfDTO);
            bAltaEjecucion = true;
            
          }
          
          Tmct0CnmvDesgloses entDesglose = creaTmct0CnmvDesglosesWithDto(operacionMtfDTO);
          entDesglose.setEjecucion(entEjecucion);

          // Se dan de alta los titulares activos.
          List<Tmct0afi> listTitularesAfi = tmct0afiDao.findActivosByAlcData(operacionMtfDTO.getNuorden(),
                                                                             operacionMtfDTO.getNbooking(),
                                                                             operacionMtfDTO.getNucnfclt(),
                                                                             operacionMtfDTO.getNucnfliq());
          for (Tmct0afi tmct0afi : listTitularesAfi) {
            Tmct0CnmvTitulares entTitularNuevo = creaCNMVTitular(tmct0afi, entDesglose);
            entDesglose.getTmct0CnmvTitularesList().add(entTitularNuevo);
          }

          // Se actualizan los titulos.
          entEjecucion.setTitulosDesgloses(entEjecucion.getTitulosDesgloses().add(entDesglose.getTitulosDesgloses()));

          // Si el numero de titulos de la ejecucción y de sus desgloses es igual se pone a pendiente de enviar
          int diferencia = entEjecucion.getTitulosEjecucion().compareTo(entEjecucion.getTitulosDesgloses());
          if (diferencia == 0) {
            entEjecucion.setCdEstadoEnvio(estadoPte);
          }
          
          // Guardado final de la ejecucion.
          tmct0CnmvEjecucionesDao.save(entEjecucion);
          
          
          // Se guardan datos del desglose
          tmct0CnmvDesglosesDao.save(entDesglose);
          
          if (bAltaEjecucion) {  // Se incrementa el contador de altas
            this.operacionesH47MtfBo.getTmct0CnmvLog().setContadorAltas(this.operacionesH47MtfBo.getTmct0CnmvLog().getContadorAltas().add(BigInteger.ONE));            
          }



        } else {
          // Desglose ya existe, solo comprobamos cambio de titularidad
          Tmct0CnmvDesgloses entDesglose = tmct0CnmvDesglosesDao.findOne(idDesglose);

          // Se obtienen los titulares del desglose actual
          List<String> listIdentificaciones = new ArrayList<String>();
          List<Tmct0CnmvTitulares> listTitularesMTF = entDesglose.getTmct0CnmvTitularesList();
          // List<Tmct0CnmvTitulares> listTitularesMTF = tmct0CnmvTitularesDao.findByDesglose(idDesglose);
          for (Tmct0CnmvTitulares tmct0CnmvTitulares : listTitularesMTF) {
            if (tmct0CnmvTitulares.getEstadoTitular() == 'A') { // solo los que estan en estado A
              listIdentificaciones.add(tmct0CnmvTitulares.getIdentificacion().trim());
            }

          }

          // Se obtinen los titulares del desglose resgistrado
          List<String> listCDHolder = new ArrayList<String>();
          List<Tmct0afi> listTitularesSibbac = tmct0afiDao.findActivosByAlcData(operacionMtfDTO.getNuorden(),
                                                                                operacionMtfDTO.getNbooking(),
                                                                                operacionMtfDTO.getNucnfclt(),
                                                                                operacionMtfDTO.getNucnfliq());

          for (Tmct0afi tmct0afi2 : listTitularesSibbac) {
            listCDHolder.add(tmct0afi2.getCdholder().trim());
          }

          // Se comprueba si se ha cambiado algun titular.
          Collections.sort(listIdentificaciones);
          Collections.sort(listCDHolder);

          boolean bIguales = listIdentificaciones.equals(listCDHolder);

          if (!bIguales) {
            // Tratamiento del desglose y su ejecucion. Depende de si la ejecucion ha sido o no enviada.

            if (entEjecucion.getCdEstadoEnvio() == null
                || entEjecucion.getCdEstadoEnvio().getIdestado() < iCdEstadoEnvio) {
              // DESGLOSE NO ENVIADO
              // Se marcan como estado Enviado = 'X' los titulares que ya no estan y se crean los nuevos.
              for (Tmct0CnmvTitulares tmct0CnmvTitular : listTitularesMTF) {
                if (!listCDHolder.contains(tmct0CnmvTitular.getIdentificacion().trim())) {
                  // Se actualiza el estado
                  tmct0CnmvTitular.setEstadoTitular('X');
                }
              }
              // Se crean los nuevos
              for (Tmct0afi tmct0afi : listTitularesSibbac) {
                if (!listIdentificaciones.contains(tmct0afi.getCdholder().trim())) {
                  // Se crea nuevo
                  Tmct0CnmvTitulares entTitularNuevo = creaCNMVTitular(tmct0afi, entDesglose);
                  entDesglose.getTmct0CnmvTitularesList().add(entTitularNuevo);
                }
              }

              tmct0CnmvDesglosesDao.save(entDesglose);

            } else {
              // DESGLOSE ENVIADO
              // Se da de baja la ejecucion y se pone pendiente de enviar la baja
              entEjecucion.setEstadoOperacion('B');
              entEjecucion.setCdEstadoEnvio(estadoPte);
              tmct0CnmvEjecucionesDao.save(entEjecucion);

              this.operacionesH47MtfBo.getTmct0CnmvLog().setContadorBajas(this.operacionesH47MtfBo.getTmct0CnmvLog().getContadorBajas().add(BigInteger.ONE));            

              // Creamos el alta de la nueva
              Tmct0CnmvEjecuciones entEjecucionClon = creaCnmvEjecucionClon(entEjecucion);
              entEjecucionClon.setEstadoOperacion('A');
              entEjecucionClon.setCdEstadoEnvio(null);
              entEjecucionClon.setCapacidadNegociacion(null);
              entEjecucionClon.setfEnvioAlta(null);
              entEjecucionClon.setfEnvioBaja(null);
              tmct0CnmvEjecucionesDao.save(entEjecucionClon);
              
              // Se incrementa el contador ya que se ha creado el clon nuevo como un alta.
              this.operacionesH47MtfBo.getTmct0CnmvLog().setContadorAltas(this.operacionesH47MtfBo.getTmct0CnmvLog().getContadorAltas().add(BigInteger.ONE));
              
              List<Tmct0CnmvDesgloses> listDesglosesEjecucion = tmct0CnmvDesglosesDao.findByEjecucion(entEjecucion.getId());

              for (Tmct0CnmvDesgloses tmct0CnmvDesgloses : listDesglosesEjecucion) {
                Tmct0CnmvDesgloses desgloseNuevo = creaCnmvDesgloseClon(tmct0CnmvDesgloses);
                desgloseNuevo.setEjecucion(entEjecucionClon);
                // Si es el desglose que tiene el cambio de titularidad, se le crean los titulares nuevos
                if (tmct0CnmvDesgloses.getId().equals(entDesglose.getId())) {
                  List<Tmct0afi> listTitularesNuevo = tmct0afiDao.findActivosByAlcData(operacionMtfDTO.getNuorden(),
                                                                                       operacionMtfDTO.getNbooking(),
                                                                                       operacionMtfDTO.getNucnfclt(),
                                                                                       operacionMtfDTO.getNucnfliq());

                  for (Tmct0afi tmct0afi : listTitularesNuevo) {
                    Tmct0CnmvTitulares entTitularNuevo = creaCNMVTitular(tmct0afi, desgloseNuevo);
                    desgloseNuevo.getTmct0CnmvTitularesList().add(entTitularNuevo);
                  }

                } else { // Se le crean los mismos pero asociados al desglose nuevo
                  List<Tmct0CnmvTitulares> listTitularesAClonar = tmct0CnmvDesgloses.getTmct0CnmvTitularesList();
                  for (Tmct0CnmvTitulares tmct0CnmvTitular : listTitularesAClonar) {
                    Tmct0CnmvTitulares entTitularNuevo = creaCnmvTitularClon(tmct0CnmvTitular);
                    entTitularNuevo.setDesglose(desgloseNuevo);
                    desgloseNuevo.getTmct0CnmvTitularesList().add(entTitularNuevo);
                  }
                }
                // Se crea un desglose nuevo con los datos nuevos asociada a la ejecucion nueva
                tmct0CnmvDesglosesDao.save(desgloseNuevo);
              }
            }
          }
        }

      } else {// Se procesa una baja
        // ******************* BAJA DE UN DESGLOSE *********************
        // Se verifica si existe el desglose
        if (idDesglose == null) {
          // Error se quiere procesar la baja de un desglose que no se ha encontrado
          LOG.warn(" - Error al procesar la baja de un desglose - No existe el desglose en TMCT0_CNMV_DESGLOSES con los siguientes datos: " + " Nuorden - "
                    + operacionMtfDTO.getNuorden()  + " - Nbooking - "    + operacionMtfDTO.getNbooking()   + " - Nucnfclt - "  + operacionMtfDTO.getNucnfclt()
                    + " - Nucnfliq -  "     + operacionMtfDTO.getNucnfliq() + " - Iddesglosecamara  " + operacionMtfDTO.getIddesglosecamara() + " - Ejecucion  " + operacionMtfDTO.getNurefexemkt());
        }
        if (entEjecucion == null) {
          // Error -> me viene una baja de un desglose que no he tratado previamente ya que no tengo su ejecucion.
          LOG.warn(" - Error al procesar la baja de un desglose - - No existe la ejecucion asociada al desglose: "
              + " Nuorden - " + operacionMtfDTO.getNuorden()  + " - Nbooking - "    + operacionMtfDTO.getNbooking()   + " - Nucnfclt - "  + operacionMtfDTO.getNucnfclt()
              + " - Nucnfliq -  "     + operacionMtfDTO.getNucnfliq() + " - Iddesglosecamara  " + operacionMtfDTO.getIddesglosecamara() + " - Ejecucion  " + operacionMtfDTO.getNurefexemkt());        }

        // Se verifica si existe la ejecución
        if (entEjecucion != null && idDesglose != null) {

          // Se obtiene el desglose.
          Tmct0CnmvDesgloses entDesglose = tmct0CnmvDesglosesDao.findOne(idDesglose);

          // Su tratamiento depende si la ejececucion ha sido no enviada.
          if (entEjecucion.getCdEstadoEnvio() == null || entEjecucion.getCdEstadoEnvio().getIdestado() < iCdEstadoEnvio) {
            // EJECUCION NO ENVIADA
            // Se da de baja el desglose y todos sus titulares pasando su estado al valor X
            entDesglose.setEstadoDesglose('X');

            for (Tmct0CnmvTitulares tmct0CnmvTitulares : entDesglose.getTmct0CnmvTitularesList()) {
              tmct0CnmvTitulares.setEstadoTitular('X');
            }

            // Se restan los titulos de ejecucion
            entEjecucion.setTitulosDesgloses(entEjecucion.getTitulosDesgloses()
                                                         .subtract(entDesglose.getTitulosDesgloses()));
            
            if (entEjecucion.getTitulosDesgloses().compareTo(BigDecimal.ZERO) == 0) {
              entEjecucion.setEstadoOperacion('X');
            }
            
            // se graban los cambios
            tmct0CnmvEjecucionesDao.save(entEjecucion);
            tmct0CnmvDesglosesDao.save(entDesglose);

          } else {
            // DESGLOSE ENVIADO
            // Se da de baja el desglose.
            entDesglose.setEstadoDesglose('B');

            // Se restan los titulos de ejecucion
            entEjecucion.setTitulosDesgloses(entEjecucion.getTitulosDesgloses()
                                                         .subtract(entDesglose.getTitulosDesgloses()));
            entEjecucion.setEstadoOperacion('B');
            entEjecucion.setCdEstadoEnvio(estadoPte);
            tmct0CnmvEjecucionesDao.save(entEjecucion);
            tmct0CnmvDesglosesDao.save(entDesglose);

            this.operacionesH47MtfBo.getTmct0CnmvLog().setContadorBajas(this.operacionesH47MtfBo.getTmct0CnmvLog().getContadorBajas().add(BigInteger.ONE));     

            // Si la ejecucion tiene mas desgloses de alta, hay que clonar la ejecucion.
            int iContador = 0;

            List<Tmct0CnmvDesgloses> listDesglosesEjecucion = tmct0CnmvDesglosesDao.findByEjecucion(entEjecucion.getId());
            for (Tmct0CnmvDesgloses tmct0CnmvDesgloses2 : listDesglosesEjecucion) {
              // si el estado es activo y no es el desglose que hemos dado de baja
              if ((tmct0CnmvDesgloses2.getEstadoDesglose() == 'A')
                  && (!tmct0CnmvDesgloses2.getId().equals(entDesglose.getId()))) {
                iContador++;
              }

            }
            // Creamos el alta de la nueva
            if (iContador > 0) {
              Tmct0CnmvEjecuciones entEjecucionClon = creaCnmvEjecucionClon(entEjecucion);
              entEjecucionClon.setEstadoOperacion('A');
              entEjecucionClon.setCdEstadoEnvio(null);
              entEjecucionClon.setCapacidadNegociacion(null);
              entEjecucionClon.setfEnvioAlta(null);
              entEjecucionClon.setfEnvioBaja(null);
              tmct0CnmvEjecucionesDao.save(entEjecucionClon);

              // Se incrementa el contador de altas
              this.operacionesH47MtfBo.getTmct0CnmvLog().setContadorAltas(this.operacionesH47MtfBo.getTmct0CnmvLog().getContadorAltas().add(BigInteger.ONE));     

              List<Tmct0CnmvDesgloses> listDesglosesEjecucionBaja = tmct0CnmvDesglosesDao.findByEjecucion(entEjecucion.getId());

              for (Tmct0CnmvDesgloses tmct0CnmvDesgloses : listDesglosesEjecucion) {

                if (!tmct0CnmvDesgloses.getId().equals(entDesglose.getId())
                    && tmct0CnmvDesgloses.getEstadoDesglose() == 'A') {
                  // Se crea un desglose nuevo asociado a la ejecucion nueva con los mismos titulares que tenia.
                  Tmct0CnmvDesgloses desgloseNuevo = creaCnmvDesgloseClon(tmct0CnmvDesgloses);
                  desgloseNuevo.setEjecucion(entEjecucionClon);
                  List<Tmct0CnmvTitulares> listTitularesAClonar = tmct0CnmvDesgloses.getTmct0CnmvTitularesList();
                  for (Tmct0CnmvTitulares tmct0CnmvTitular : listTitularesAClonar) {
                    Tmct0CnmvTitulares entTitularNuevo = creaCnmvTitularClon(tmct0CnmvTitular);
                    entTitularNuevo.setDesglose(desgloseNuevo);
                    desgloseNuevo.getTmct0CnmvTitularesList().add(entTitularNuevo);
                  }

                  // Se crea un desglose nuevo con los datos nuevos asociada a la ejecucion nueva
                  tmct0CnmvDesglosesDao.save(desgloseNuevo);

                }
              }
            }
          }
        }
     }
  }
    
  
  private Tmct0CnmvEjecuciones creaCnmvEjecucionClon (Tmct0CnmvEjecuciones entEjecucionOld){
    
    Tmct0CnmvEjecuciones entEjecucionNueva = new Tmct0CnmvEjecuciones();

    entEjecucionNueva.setAuditDate(new Date());
    entEjecucionNueva.setAuditUser(entEjecucionOld.getAuditUser());
    entEjecucionNueva.setIdEjecucion(entEjecucionOld.getIdEjecucion());
    entEjecucionNueva.setfEjecucion(entEjecucionOld.getfEjecucion());
    entEjecucionNueva.sethEjecucion(entEjecucionOld.gethEjecucion()); 
    entEjecucionNueva.setfValor(entEjecucionOld.getfValor());
    entEjecucionNueva.setSentido(entEjecucionOld.getSentido(),true);
    entEjecucionNueva.setCapacidadNegociacion(entEjecucionOld.getCapacidadNegociacion()); // En la creación no se puede determinar
    entEjecucionNueva.setIdInstrumento(entEjecucionOld.getIdInstrumento());
    entEjecucionNueva.setTitulosEjecucion(entEjecucionOld.getTitulosEjecucion());  
    entEjecucionNueva.setTitulosDesgloses(entEjecucionOld.getTitulosDesgloses());
    entEjecucionNueva.setPrecio(entEjecucionOld.getPrecio());
    entEjecucionNueva.calculaEfectivo();
    entEjecucionNueva.setDivisa(entEjecucionOld.getDivisa());
    entEjecucionNueva.setSistemaNegociacion(entEjecucionOld.getSistemaNegociacion());
    entEjecucionNueva.setIndSistemaNegociacion(entEjecucionOld.getIndSistemaNegociacion());
    entEjecucionNueva.setEstadoOperacion(entEjecucionOld.getEstadoOperacion());
    entEjecucionNueva.setCdEstadoEnvio(entEjecucionOld.getCdEstadoEnvio());
    return entEjecucionNueva;
    
  }

  private Tmct0CnmvEjecuciones creaTmct0CnmvEjecucionesWithDto( OperacionMtfDTO operacionMtfDTO){
    Tmct0CnmvEjecuciones entEjecucionNueva = new Tmct0CnmvEjecuciones();
    entEjecucionNueva.setAuditDate( new Date());
    entEjecucionNueva.setAuditUser("process");
    entEjecucionNueva.setIdEjecucion(operacionMtfDTO.getNurefexemkt());
    entEjecucionNueva.setfEjecucion(operacionMtfDTO.getFeejecuc());
    entEjecucionNueva.sethEjecucion(operacionMtfDTO.getHoejecuc()); 
    entEjecucionNueva.setfValor(operacionMtfDTO.getFeliquidacion());
    entEjecucionNueva.setSentido(operacionMtfDTO.getSentido(),true);
    entEjecucionNueva.setCapacidadNegociacion(null); // En la creación no se puede determinar
    entEjecucionNueva.setIdInstrumento(operacionMtfDTO.getIsin());
    entEjecucionNueva.setTitulosEjecucion(tmct0ejeDao.getTitulosEjecucion(operacionMtfDTO.getNuorden(), operacionMtfDTO.getNbooking(), operacionMtfDTO.getNurefexemkt()));  
    entEjecucionNueva.setTitulosDesgloses(new BigDecimal(0));
    entEjecucionNueva.setPrecio(operacionMtfDTO.getImprecio());
    entEjecucionNueva.calculaEfectivo();
    entEjecucionNueva.setDivisa(operacionMtfDTO.getCddivisa());
    entEjecucionNueva.setSistemaNegociacion(operacionMtfDTO.getCd_plataforma_negociacion());
    entEjecucionNueva.setIndSistemaNegociacion('M');
    entEjecucionNueva.setEstadoOperacion('A');
    entEjecucionNueva.setCdEstadoEnvio(null);
    return entEjecucionNueva;
  }
  
  private Tmct0CnmvDesgloses creaCnmvDesgloseClon (Tmct0CnmvDesgloses entDesgloseOld){
    
    Tmct0CnmvDesgloses entDesgloseNueva = new Tmct0CnmvDesgloses(); 
    entDesgloseNueva.setAuditDate(new Date());
    entDesgloseNueva.setAuditUser(entDesgloseOld.getAuditUser());
    entDesgloseNueva.setEjecucion(null);
    entDesgloseNueva.setNuorden(entDesgloseOld.getNuorden());
    entDesgloseNueva.setNbooking(entDesgloseOld.getNbooking());
    entDesgloseNueva.setNucnfclt(entDesgloseOld.getNucnfclt());
    entDesgloseNueva.setNucnfliq(entDesgloseOld.getNucnfliq());
    entDesgloseNueva.setIdDesgloseCamara(entDesgloseOld.getIdDesgloseCamara());
    entDesgloseNueva.setTitulosDesgloses(entDesgloseOld.getTitulosDesgloses());
    entDesgloseNueva.setEstadoDesglose(entDesgloseOld.getEstadoDesglose());
    entDesgloseNueva.setTmct0CnmvTitularesList(new ArrayList<Tmct0CnmvTitulares>()); 
    return  entDesgloseNueva;
  } 
  
  
  private Tmct0CnmvDesgloses creaTmct0CnmvDesglosesWithDto( OperacionMtfDTO operacionMtfDTO){
    Tmct0CnmvDesgloses entDesgloseNueva = new Tmct0CnmvDesgloses(); 
    entDesgloseNueva.setAuditDate( new Date());
    entDesgloseNueva.setAuditUser("process");
    entDesgloseNueva.setNuorden(operacionMtfDTO.getNuorden());
    entDesgloseNueva.setNbooking(operacionMtfDTO.getNbooking());
    entDesgloseNueva.setNucnfclt(operacionMtfDTO.getNucnfclt());
    entDesgloseNueva.setNucnfliq(operacionMtfDTO.getNucnfliq());
    entDesgloseNueva.setIdDesgloseCamara(operacionMtfDTO.getIddesglosecamara());
    entDesgloseNueva.setTitulosDesgloses(operacionMtfDTO.getImtitulos());
    entDesgloseNueva.setEstadoDesglose('A');
    entDesgloseNueva.setTmct0CnmvTitularesList(new ArrayList<Tmct0CnmvTitulares>());
    return  entDesgloseNueva; 
  }
  
  private Tmct0CnmvTitulares creaCnmvTitularClon (Tmct0CnmvTitulares entTitularOld){
    
    Tmct0CnmvTitulares entTitularNuevo = new Tmct0CnmvTitulares();
    entTitularNuevo.setAuditDate(new Date());
    entTitularNuevo.setAuditUser(entTitularOld.getAuditUser());
    entTitularNuevo.setDesglose(null);
    entTitularNuevo.setTpidenti(entTitularOld.getTpidenti());
    entTitularNuevo.setIdentificacion(entTitularOld.getIdentificacion());
    entTitularNuevo.setNombre(entTitularOld.getNombre());
    entTitularNuevo.setApellido1(entTitularOld.getApellido1());
    entTitularNuevo.setApellido2(entTitularOld.getApellido2());
    entTitularNuevo.setTipoPersona(entTitularOld.getTipoPersona());
    entTitularNuevo.setTipoTiular(entTitularOld.getTipoTiular());
    entTitularNuevo.setEstadoTitular(entTitularOld.getEstadoTitular());
    return entTitularNuevo;
  }
  
  private Tmct0CnmvTitulares creaCNMVTitular (Tmct0afi tmct0afi, Tmct0CnmvDesgloses entDesglose ){
    Tmct0CnmvTitulares entTitular = new Tmct0CnmvTitulares();
    entTitular.setAuditDate(new Date());
    entTitular.setAuditUser("process");
    entTitular.setDesglose(entDesglose);
    entTitular.setTpidenti(tmct0afi.getTpidenti().toString());
    entTitular.setIdentificacion(tmct0afi.getCdholder());
    // Nombre y aplledos dependen del tipo de sociedad Fisica o
    // Juridica.
    if (tmct0afi.getTpsocied() == 'F') {
      entTitular.setNombre(tmct0afi.getNbclient());
      entTitular.setApellido1(tmct0afi.getNbclient1());
      entTitular.setApellido2(tmct0afi.getNbclient2());
    } else {
      entTitular.setNombre(tmct0afi.getNbclient1());
    }

    entTitular.setTipoPersona(tmct0afi.getTpsocied());
    entTitular.setTipoTiular(tmct0afi.getTptiprep());
    entTitular.setEstadoTitular('A');

    return entTitular;
  }
 

}

