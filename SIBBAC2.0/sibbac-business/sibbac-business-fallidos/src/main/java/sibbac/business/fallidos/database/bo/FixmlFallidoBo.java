package sibbac.business.fallidos.database.bo;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.FallidoDao;
import sibbac.business.fallidos.database.dao.FixmlFallidoDao;
import sibbac.business.fallidos.database.model.Fallido;
import sibbac.business.fallidos.database.model.FallidoAlc;
import sibbac.business.fallidos.database.model.FixmlFallido;
import sibbac.business.fallidos.database.model.MovimientoFallido;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fallidos.utils.MovimientoFallidoHelper;
import sibbac.business.fixml.database.dao.FixmlFileDao;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.TextosIdiomasBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums.MovimientosFallidos;
import sibbac.database.bo.AbstractBo;
import sibbac.fixml.classes.AbstractMessageT;
import sibbac.fixml.classes.PartiesBlockT;
import sibbac.fixml.classes.PositionAmountDataBlockT;
import sibbac.fixml.classes.StipulationsBlockT;
import sibbac.fixml.classes.TradeCaptureReportMessageT;
import sibbac.fixml.classes.TrdCapRptSideGrpBlockT;
import sibbac.tasks.database.JobPK;

@Service
public class FixmlFallidoBo extends AbstractBo<FixmlFallido, Long, FixmlFallidoDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FixmlFallidoBo.class);

  /** Clave de los textos de los asuntos de los email de fallidos. */
  private static final String KEY_TEXTO_ASUNTO_FALLIDOS = "c";

  /** Clave de los textos de los cuerpos de los email de fallidos. */
  private static final String KEY_TEXTO_CUERPO_FALLIDOS = "d";
  
  private static final String TRM_ID_DEFAULT = "%-3s-%-12s";
  
  private static final BigInteger R38 = BigInteger.valueOf(38l);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0_FALLIDO</code>. */
  @Autowired
  private FallidoDao fallidoDao;

  /** Objeto para acceder a la tabla tmct0movimientofallido. */
  @Autowired
  private MovimientoFallidoBo movimientofallidoBo;

  @Autowired
  private Tmct0MovimientoCuentaAliasBo movAliasBo;

  /** Objeto para enviar correos electrónicos a clientes. */
  @Autowired
  private SendMail mailer;

  /** Business object para la tabla <code>TMCT0_ALIAS</code>. */
  @Autowired
  private AliasBo aliasBo;

  /** Business object para la tabla <code>TMCT0_TEXTO_IDIOMA</code>. */
  @Autowired
  private TextosIdiomasBo textoIdiomaBo;

  /** Business object para la tabla <code>TMCT0_ETIQUETAS</code>. */
  @Autowired
  private EtiquetasDao etiquetasDao;

  @Autowired
  private GestorServicios gestorServicios;

  /** Objeto para el acceso a la tabla <code>TMCT0_FIXMLFILES</code>. */
  @Autowired
  private FixmlFileDao fixmlFileDao;

  @Autowired
  private SendMail sendMail;

  @Value("${aviso.asignacion.pui}")
  private String emailsAsignacionPui;

  /** Asunto del mail enviado a S3 con las referencias S3 a las que se le ha asignado un PUI. */
  @Value("${aviso.asignacion.pui.subject}")
  private String subject;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Procesa las entradas del los ficheros <code>CSECLENDING</code>, <code>CSECLENDVALUE</code>,
   * <code>CSECLENDREMUN</code>, <code>CFAILPENALTIES</code>, <code>CSECLENDCANC</code>, <code>CFAILBUYIN</code>,
   * <code>CFAILCASHSETTL</code> que contienen los movimientos producidos por operaciones fallidas.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param lm Lista de JAXB del fichero XML a tratar.
   * @param zip Fichero ZIP que contiene el XML a tratar.
   * @param tipoFichero Tipo de fichero XML a tratar.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void procesarFIXMLFallidos(JobPK jobPk,
                                    List<JAXBElement<? extends AbstractMessageT>> lm,
                                    String zip,
                                    String tipoFichero) {
    LOG.debug("[procesarFIXMLFallidos] Inicio ...");
    LOG.debug("[procesarFIXMLFallidos] Tipo de fichero: " + tipoFichero);

    TradeCaptureReportMessageT am = null;

    // InstruccionLiquidacionFallida instruccionLiquidacionFallida = null;
    FixmlFallido fixmlFallido = null;

    List<FixmlFallido> fixmls = null;
    String instruccionOrigen = null;

    for (JAXBElement<? extends AbstractMessageT> b : lm) {
      am = (TradeCaptureReportMessageT) b.getValue();

      if (tipoFichero.compareTo(Constantes.CFAILBUYIN_FILENAME) == 0) {
        // 1.- En los fichero CFAILBUYIN de las ILIQECC que reciben PUI vienen con dos bloques de información, uno de
        // ExchTrdTyp="5" (return leg del PUI) que no procesamos y otro ExchTrdTyp="6" que si procesamos y que traen
        // como TrdID el 6 y OrigTrdID el 5.
        // 2.- En los fichero CFAILBUYIN de las ILIQECC que no reciben PUI vienen con dos bloques de información, uno de
        // ExchTrdTyp="3" (ILIQECC fallida) que no procesamos y otro ExchTrdTyp="6" que si procesamos y que traen
        // como TrdID el 6 y OrigTrdID el 3.
        if (am.getExchTrdTyp().compareTo(Constantes.FIXML_GENERACION_IL) == 0) {
          continue;
        } else if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
          continue;
        } else if (am.getExchTrdTyp().compareTo(Constantes.FIXML_RECOMPRA) == 0) {
          if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_GENERACION_IL.charAt(0)) {
            instruccionOrigen = am.getOrigTrdID();
          } else if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0)) {
            fixmls = dao.findByNumeroOperacion(am.getOrigTrdID());
            if (fixmls != null && !fixmls.isEmpty()) {
              instruccionOrigen = fixmls.get(0).getInstruccionOrigen();
            }
          }
        }
      } else if (tipoFichero.compareTo(Constantes.CFAILCASHSETTL_FILENAME) == 0) {
        // 1.- En los fichero CFAILCASHSETTL de las ILIQECC que reciben PUI vienen con dos bloques de información, uno
        // de ExchTrdTyp="5" (return leg del PUI) que no procesamos y otro ExchTrdTyp="7" que si procesamos y que traen
        // como TrdID el 7 y OrigTrdID el 5.
        // 2.- En los fichero CFAILCASHSETTL de las ILIQECC que no reciben PUI vienen con dos bloques de información,
        // uno de ExchTrdTyp="3" (ILIQECC fallida) que no procesamos y otro ExchTrdTyp="7" que si procesamos y que traen
        // como TrdID el 7 y OrigTrdID el 3.
        if (am.getExchTrdTyp().compareTo(Constantes.FIXML_GENERACION_IL) == 0) {
          continue;
        } else if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
          continue;
        } else if (am.getExchTrdTyp().compareTo(Constantes.FIXML_LIQUIDACION_EFECTIVO) == 0) {
          if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_GENERACION_IL.charAt(0)) {
            instruccionOrigen = am.getOrigTrdID();
          } else if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0)) {
            fixmls = dao.findByNumeroOperacion(am.getOrigTrdID());
            if (fixmls != null && !fixmls.isEmpty()) {
              instruccionOrigen = fixmls.get(0).getInstruccionOrigen();
            }
          }
        }
      } else if (tipoFichero.compareTo(Constantes.CFAILPENALTIES_FILENAME) == 0) {
        // En los ficheros CFAILPENALTIES si el campo TrdID viene con el 3 (instrucción fallida original) en el
        // campo OrigTrdID vendrá el mismo 3 y traeran las penalizaciones diarias por fallidos (Rsn=2000), si el campo
        // TrdID viene con el 5 (return leg del PUI) en el campo OrigTrdID vendrá el 4 (referencia entre patas del PUI)
        // y traeran comisión por constitución de PUI (Rsn=2002) y costes administrativos por recompras (Rsn=2001).
      	if(am.getTrdID() == null) {
      		am.setTrdID(String.format(TRM_ID_DEFAULT, buscaR38(am.getRptSide()), am.getInstrmt() == null ? "" : am.getInstrmt().getID()));
      	}
      	if (am.getTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_GENERACION_IL.charAt(0)) {
          instruccionOrigen = am.getTrdID();
        } else {
          fixmls = dao.findByNumeroOperacionAndReferenciaPrestamo(am.getTrdID(), am.getOrigTrdID());
          if (fixmls != null && !fixmls.isEmpty()) {
            instruccionOrigen = fixmls.get(0).getInstruccionOrigen();
          }
        }
      } else if (tipoFichero.compareTo(Constantes.CSECLENDVALUE_FILENAME) == 0) {
        // Con la documentación de BME no queda claro si se van a recibir más tipos de operaciones en el campo
        // OrigTrdID, así que para curarnos en salud ...
        if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0)) {
          fixmls = dao.findByNumeroOperacion(am.getOrigTrdID());
          if (fixmls != null && !fixmls.isEmpty()) {
            instruccionOrigen = fixmls.get(0).getInstruccionOrigen();
          }
        } else {
          continue;
        }
      } else if (tipoFichero.compareTo(Constantes.CSECLENDING_FILENAME) == 0) {
        if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_IDA) == 0
            || am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
          instruccionOrigen = am.getOrigTrdID();
        } else {
          continue;
        }
      } else if (tipoFichero.compareTo(Constantes.CSECLENDREMUN_FILENAME) == 0) {
        if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
          instruccionOrigen = am.getOrigTrdID();
        } else {
          continue;
        }
      } else if (tipoFichero.compareTo(Constantes.CSECLENDCANC_FILENAME) == 0) {
        if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
          instruccionOrigen = am.getOrigTrdID();
        } else {
          continue;
        }
      } // else
      // ya no es busca la instruccion fallida antes de procesar los xml's
      // LOG.debug("[procesarFIXMLFallidos] Buscando ILIQECC fallida: " + instruccionOrigen);
      // instruccionLiquidacionFallida = instruccionLiquidacionFallidaDao.findByNumeroOperacionEcc(instruccionOrigen);
      // if (instruccionLiquidacionFallida == null) {
      // LOG.warn("[procesarFIXMLFallidos] No se ha encontrado ILIQECC fallida. Se ignora ...");
      // } else {
      // LOG.debug("[procesarFIXMLFallidos] Encontrado ILIQECC fallida ... Buscando fixml ...");
      if (tipoFichero.compareTo(Constantes.CSECLENDING_FILENAME) == 0) {
        // fixmlFallido = dao.findByNumeroOperacionAndFileType(am.getTrdID(), tipoFichero);
        fixmlFallido = dao.findByNumeroOperacionAndVolumenVivoOperacionAndFileType(am.getTrdID(), am.getLeavesQty(),
                                                                                   tipoFichero);
      } else if (tipoFichero.compareTo(Constantes.CFAILPENALTIES_FILENAME) == 0) {
        fixmlFallido = null; //La referencia no es identificador único y este control no es válido ya.
        /*dao.findByNumeroOperacionAndSntAndFileTypeAndRsn(am.getTrdID(), am.getHdr().getSnt()
              .toGregorianCalendar().getTime(), tipoFichero, am.getAmt().get(0).getRsn());*/
      } else if (tipoFichero.compareTo(Constantes.CSECLENDCANC_FILENAME) == 0) {
        if (am.getSetSesID().value().compareTo("EOD") == 0) {
          fixmlFallido = dao.findByNumeroOperacionAndSntAndFileType(am.getTrdID(), am.getHdr().getSnt()
                                                                                     .toGregorianCalendar().getTime(),
                                                                    tipoFichero);
        } else {
          LOG.info("[procesarFIXMLFallidos] FIXML CSECLENDCANC.C0 de intradia. No se procesa ...");
          continue;
        } // else
      } else {
        fixmlFallido = dao.findByNumeroOperacionAndSntAndFileType(am.getTrdID(), am.getHdr().getSnt()
                                                                                   .toGregorianCalendar().getTime(),
                                                                  tipoFichero);
      } // else

      if (fixmlFallido == null) {
        LOG.debug("[procesarFIXMLFallidos] Fixml no encontrado ... Insertando ...");
        fixmlFallido = new FixmlFallido();
        fixmlFallido.setFileType(tipoFichero);

        // RAIZ
        fixmlFallido.setNumeroOperacion(am.getTrdID()); // Identificador de registro de la ECC
        fixmlFallido.setInstruccionOrigen(am.getOrigTrdID()); // Identificador de la IL fallida
        fixmlFallido.setReferenciaPrestamo(am.getLinkID()); // Referencia común a las dos patas del préstamo
        fixmlFallido.setTipoOperacionEcc(am.getExchTrdTyp()); // Tipo de operación de la ECC
        fixmlFallido.setPrecioOperacion(am.getLastPx()); // Precio medio de la instrucción que liquida
        fixmlFallido.setVolumenOperacion(am.getLastQty()); // Volumen del préstamo
        fixmlFallido.setVolumenVivoOperacion(am.getLeavesQty()); // Volumen vivo del préstamo
        fixmlFallido.setDivisa(am.getCcy()); // Código de divisa

        fixmlFallido.setFechaLiquidacion(am.getBizDt().toGregorianCalendar().getTime());

        // En los ficheros CFAILPENALTIES los atributos SettlDt y TrxTm no existen
        if (tipoFichero.compareTo(Constantes.CFAILPENALTIES_FILENAME) != 0) {
          fixmlFallido.setSttlDt(am.getSettlDt().toGregorianCalendar().getTime());
          // Ahora en los ficheros CSECLENDVALUE hay bloques de información que no traen esta etiqueta pero otros si
          try {
            fixmlFallido.setTrxTm(am.getTxnTm().toGregorianCalendar().getTime());
          } catch (NullPointerException npe) {
            // Se ignora
          } // catch
        } // if

        fixmlFallido.setImporteEfectivoInstruccion(am.getGrossTrdAmt()); // Importe del colateral inicial
        fixmlFallido.setClrTrdPx(am.getClrTrdPx()); // Precio de revaluación del colateral del Préstamo

        // HDR
        fixmlFallido.setSegmento(am.getHdr().getSSub()); // Segmento de la ECC
        // Timestamp de generación del fichero
        fixmlFallido.setSnt(am.getHdr().getSnt().toGregorianCalendar().getTime());

        // INSTRMNT
        fixmlFallido.setIsin(am.getInstrmt().getID()); // ISIN

        // AMT
        // En los ficheros CSECLENDCANC, CFAILBUYIN y CFAILCASHSETTL la etiqueta AMT no existe
        if (tipoFichero.compareTo(Constantes.CSECLENDCANC_FILENAME) != 0
            && tipoFichero.compareTo(Constantes.CFAILBUYIN_FILENAME) != 0
            && tipoFichero.compareTo(Constantes.CFAILCASHSETTL_FILENAME) != 0) {
          fixmlFallido.setImporteEfectivoVivoOperacion(am.getAmt().get(0).getAmt());
          fixmlFallido.setRsn(am.getAmt().get(0).getRsn());
        } // if

        // RPTSIDE
        for (TrdCapRptSideGrpBlockT rptSide : am.getRptSide()) {
          LOG.debug("[procesarFIXMLFallidos] TrdCapRptSideGrpBlockT rptSide ...");

          fixmlFallido.setSentido(rptSide.getSide()); // Sentido

          for (StipulationsBlockT stip : rptSide.getStip()) {
            LOG.debug("[procesarFIXMLFallidos] StipulationsBlockT stip ...");
            if (stip.getTyp().value().compareTo("ILIQDCV") == 0) {
              LOG.debug("[procesarFIXMLFallidos] ILIQDCV: " + stip.getVal());
              fixmlFallido.setIliqdcv(stip.getVal()); // ILIQDCV
            } // else if
          } // for

          // En el fichero CFAILPENALTIES no existe el campo PosEfct
          if (tipoFichero.compareTo(Constantes.CFAILPENALTIES_FILENAME) != 0) {
            fixmlFallido.setIndicadorPosicion(rptSide.getPosEfct().value()); // Tipo de movimiento
          } // if
        } // for

        switch (tipoFichero) {
          case Constantes.CSECLENDING_FILENAME:
            if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_IDA) == 0
                && am.getLeavesQty() != BigDecimal.ZERO) {
              // Sólo generan movimientos las initial leg de los PUI para no duplicar con las return leg y solo
              // aquellas que indiquen que quedan titulos por liquidar
              procesarCSECLENDING(jobPk, am);
            }
            break;
          case Constantes.CSECLENDCANC_FILENAME:
            procesarCSECLENDCANC(jobPk, am);
            break;
          case Constantes.CSECLENDVALUE_FILENAME:
            procesarCSECLENDVALUE(jobPk, am);
            break;
          case Constantes.CSECLENDREMUN_FILENAME:
            // if (am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
            procesarCSECLENDREMUN(jobPk, am);
            // }
            break;
          case Constantes.CFAILBUYIN_FILENAME:
            procesarCFAILBUYIN(jobPk, am, instruccionOrigen);
            break;
          case Constantes.CFAILCASHSETTL_FILENAME:
            procesarCFAILCASHSETTL(jobPk, am, instruccionOrigen);
            break;
          case Constantes.CFAILPENALTIES_FILENAME:
            // if (am.getExchTrdTyp().compareTo(Constantes.FIXML_GENERACION_IL) == 0
            // || am.getExchTrdTyp().compareTo(Constantes.FIXML_PRESTAMO_VUELTA) == 0) {
            procesarCFAILPENALTIES(jobPk, am, instruccionOrigen);
            // }
            break;
          default:
            LOG.warn("[procesarFIXMLFallidos] El tipo de fichero '" + tipoFichero + "' no es de fallidos");
        } // switch

        dao.save(fixmlFallido);
      } else {
        LOG.debug("[procesarFIXMLFallidos] FIXML ya procesado ...");
      } // else
      // } // else
    } // for (JAXBElement<? extends AbstractMessageT> b : lm)

    fixmlFileDao.updateProcesadoByZipAndTipo(zip, tipoFichero);
    LOG.debug("[procesarFIXMLFallidos] Fin ...");
  } // procesarFIXMLFallidos

  private String buscaR38(List<TrdCapRptSideGrpBlockT> rptSide) {
    TrdCapRptSideGrpBlockT block;
    int size;
    
    if(rptSide != null) {
      size = rptSide.size();
      for(int i = 0; i < size; i++) {
        block = rptSide.get(i);
        if(block.getPty() != null) {
          for(PartiesBlockT parties : block.getPty()) {
            if(R38.equals(parties.getR())) {
              return parties.getID();
            }
          }
        }
      }
    }
    return "";
  }

  /**
   * Crea un objeto <code>Tmct0MovimientoCuentaAlias</code> a partir del <code>MovimientoFallido</code> y lo persiste en
   * base de datos.
   * 
   * @param movimientoFallido Movimiento fallido.
   * @param cdCuentaLiquidacion Cuenta de liquidación.
   */
  public void saveMovimientoAlias(MovimientoFallido movimientoFallido, String cdCuentaLiquidacion) {
    Tmct0MovimientoCuentaAlias movAlias = MovimientoFallidoHelper.movimientoFallidoToMovimientoCuentaAlias(movimientoFallido,
                                                                                                           cdCuentaLiquidacion);
    movAliasBo.save(movAlias);
  } // saveMovimientoAlias

  /**
   * Envía un correo electrónico a las direcciones dadas de alta para el cliente que ha recibido el movimiento
   * especificado.
   * 
   * @param jobPk Para determinar los contactos.
   * @param movimientoFallido Movimiento producido.
   */
  public void enviarCorreo(final JobPK jobPk, final MovimientoFallido movimientoFallido) {
    List<Contacto> contactos = null;
    // Recuperamos la informacion del alias
    String cdAlias = movimientoFallido.getAliasCliente();
    Alias alias = aliasBo.findByCdaliass(cdAlias);
    if (alias != null) {
      // Montamos el mapping con los datos que se van a sustituir en el cuerpo del Mail
      LOG.trace("[enviarCorreo] Preparando el mapping...");
      Map<String, String> mapping = this.createMapping(alias, movimientoFallido);

      // Recuperamos el Subject y el body
      String asunto = textoIdiomaBo.recuperarTextoIdioma(KEY_TEXTO_ASUNTO_FALLIDOS, alias.getIdioma());
      asunto = reemplazarKeys(asunto, mapping);
      String body = textoIdiomaBo.recuperarTextoIdioma(KEY_TEXTO_CUERPO_FALLIDOS, alias.getIdioma());
      body = reemplazarKeys(body, mapping);
      LOG.trace("[enviarCorreo] Subject: [{}]", asunto);
      LOG.trace("[enviarCorreo] Body...: [{}]", body);

      // Se recuperan los contactos del alias
      contactos = gestorServicios.getContactos(alias, jobPk);

      if (contactos == null || contactos.isEmpty()) {
        LOG.warn("[enviarCorreo] WARN: No se puede enviar el correo a este alias [{}] porque carece de contactos!",
                 alias.getId());
      } else {
        List<String> destinatarios = new ArrayList<String>();

        // Procesamos los destinatarios (contactos)
        String[] mail = null;
        for (Contacto destinatario : contactos) {
          mail = destinatario.getEmail().split(";");
          for (int i = 0; i < mail.length; i++) {
            destinatarios.add(mail[i].trim());
          }
        }

        try {
          mailer.sendMail(destinatarios, asunto, body);
          LOG.debug("[enviarCorreo] Correo enviado.");
        } catch (MailSendException | IllegalArgumentException | MessagingException | IOException e) {
          LOG.warn("[enviarCorreo] WARN({}): Incidencia enviando el correo de ajustes: [{}]", e.getClass().getName(),
                   e.getMessage());
        }
      }
    } else {
      LOG.warn("[enviarCorreo] WARN: No se envia email. No se ha encontrado Alias {} ", cdAlias);
    }
  } // enviarCorreo

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Procesa las entradas del los ficheros <code>CSECLENDING</code> que contienen las generaciones de PUI.
   * <p>
   * El PUI puede ser parcial.
   * <p>
   * ¿Se pueden recibir varios PUI sobre un fallido?.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CPENDINGINST.C0.XML.
   */
  private void procesarCSECLENDING(JobPK jobPk, TradeCaptureReportMessageT am) {
    LOG.debug("[procesarCSECLENDING] Inicio ...");

    List<Fallido> fallidosList = fallidoDao.findByILiqEcc(am.getOrigTrdID());
    LOG.debug("[procesarCSECLENDING] Numero de operaciones fallidas: " + fallidosList.size());

    MovimientoFallido mf = null;

    BigDecimal volumenProporcional = BigDecimal.ZERO;
    // BigDecimal sumaVolumenesProporcionales = BigDecimal.ZERO;
    BigDecimal colateralProporcional = BigDecimal.ZERO;
    BigDecimal sumaColateralProporcional = BigDecimal.ZERO;

    BigDecimal volumenPorAsignar = am.getLeavesQty();
    BigDecimal volumenAsignado = BigDecimal.ZERO;

    // Mail S3
    Map<Fallido, BigDecimal> asignacionesPui = new HashMap<Fallido, BigDecimal>();
    StringBuffer body;
    String ccPui = null;

    Integer contador = 1;

    for (Fallido fallido : fallidosList) {
      LOG.debug("[procesarCSECLENDING] Procesando fallido: " + fallido.toString());
      LOG.debug("[procesarCSECLENDING] Titulos PUI por asignar: " + volumenPorAsignar);

      if (volumenPorAsignar.signum() > 0) {
        // Si quedan títulos del PUI por asignar
        if (fallido.getTitulos().subtract(fallido.getTitulosPui()).signum() > 0) {
          // Si no se le asignó ya un PUI recibido anteriormente que completó el fallido
          // ¿Se pueden recibir varios PUI sobre un fallido?, si no es así las comprobaciones sobre el campo TitulosPui
          // sobran, hasta saberlo me curo en salud ...
          if (volumenPorAsignar.compareTo(fallido.getTitulos().subtract(fallido.getTitulosPui())) >= 0) {
            volumenAsignado = fallido.getTitulos().subtract(fallido.getTitulosPui());
            volumenPorAsignar = volumenPorAsignar.subtract(volumenAsignado);
            LOG.debug("[procesarCSECLENDING] El PUI completa totalmente los fallidos del alias: '{}'", volumenAsignado);
          } else {
            volumenAsignado = volumenPorAsignar;
            volumenPorAsignar = BigDecimal.ZERO;
            LOG.debug("[procesarCSECLENDING] El PUI no completa totalmente los fallidos del alias: '{}'",
                      volumenAsignado);
          } // else
        } else {
          LOG.debug("[procesarCSECLENDING] El fallido ya tiene todos los titulos asignados a un PUI ...");
          continue;
        } // else
      } else {
        LOG.debug("[procesarCSECLENDING] No hay mas titulos del PUI por asignar ...");
        continue;
      } // else

      if (contador != fallidosList.size()) {
        volumenProporcional = volumenAsignado.divide(am.getLeavesQty(), 6, RoundingMode.HALF_UP);

        // El campo 'GrossTrdAmt' trae el importe inicial del colateral del prestamo
        colateralProporcional = am.getGrossTrdAmt().multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
        sumaColateralProporcional = sumaColateralProporcional.add(colateralProporcional);
      } else {
        colateralProporcional = am.getGrossTrdAmt().subtract(sumaColateralProporcional);
      } // else
      LOG.debug("[procesarCSECLENDING] Colateral proporcional: " + colateralProporcional);

      // Se da de alta el PUI
      mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.APU.getTipo(), fallido,
                                                              volumenAsignado, BigDecimal.ZERO,
                                                              am.getTxnTm().toGregorianCalendar().getTime(),
                                                              am.getBizDt().toGregorianCalendar().getTime());
      saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
      enviarCorreo(jobPk, mf);

      // Se da de alta el colateral
      mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.ACO.getTipo(), fallido,
                                                              BigDecimal.ZERO, colateralProporcional,
                                                              am.getTxnTm().toGregorianCalendar().getTime(),
                                                              am.getBizDt().toGregorianCalendar().getTime());
      saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
      enviarCorreo(jobPk, mf);

      // Se almacenan los titulos asignados del PUI
      // Si no se puede recibir más de un PUI no hace falta sumarizar los campos, hasta saberlo me curo en salud ...
      // fallido.setTitulosPui(volumenAsignado);
      // fallido.setTitulosPuiActivos(volumenAsignado);
      // fallido.setColateral(am.getGrossTrdAmt().multiply(volumenProporcional));
      fallido.setTitulosPui(fallido.getTitulosPui().add(volumenAsignado));
      fallido.setTitulosPuiActivos(fallido.getTitulosPuiActivos().add(volumenAsignado));
      // fallido.setColateral(fallido.getColateral().add(am.getGrossTrdAmt().multiply(volumenProporcional)));
      fallido.setColateral(fallido.getColateral().add(colateralProporcional));
      fallidoDao.save(fallido);

      // Para el mail con las patas a las que se le ha asignado el PUI
      ccPui = fallido.getCdCuentaCompensacion();
      asignacionesPui.put(fallido, volumenAsignado);

      contador++;
    } // for (OperacionFallido of : ofList)

    try {
      if (asignacionesPui.size() > 0) {
        LOG.debug("[procesarCSECLENDING] Preparando email de asignacion de PUI ...");
        body = new StringBuffer();
        body.append("Recibido PUI de '" + am.getLeavesQty() + "' titulos para el ISIN " + am.getInstrmt().getID()
                    + " en la cuenta '" + ccPui + "' concedido en fecha '"
                    + FormatDataUtils.convertDateToStringSeparator(am.getBizDt().toGregorianCalendar().getTime()) + "'");
        body.append(System.getProperty("line.separator"));
        body.append(System.getProperty("line.separator"));
        body.append("Operaciones afectadas:");
        body.append(System.getProperty("line.separator"));
        body.append(System.getProperty("line.separator"));

        BigDecimal titulosAsignados;
        ArrayList<FallidoAlc> fallidosAlcList = new ArrayList<FallidoAlc>();

        for (Map.Entry<Fallido, BigDecimal> entry : asignacionesPui.entrySet()) {
          titulosAsignados = entry.getValue();

          fallidosAlcList.clear();
          fallidosAlcList.addAll(entry.getKey().getAlcs());
          Collections.sort(fallidosAlcList, Collections.reverseOrder(new FallidoAlcTitulosFallidosComparator()));

          for (FallidoAlc fallidoAlc : fallidosAlcList) {
            if (titulosAsignados.compareTo(fallidoAlc.getTitulosFallidos()) > 0) {
              body.append("  - " + fallidoAlc.getReferenciaS3() + " por "
                          + fallidoAlc.getTitulosFallidos().setScale(0, BigDecimal.ROUND_DOWN) + " titulos");
              body.append(System.getProperty("line.separator"));
              titulosAsignados = titulosAsignados.subtract(fallidoAlc.getTitulosFallidos());
            } else {
              body.append("  - " + fallidoAlc.getReferenciaS3() + " por "
                          + titulosAsignados.setScale(0, BigDecimal.ROUND_DOWN) + " titulos");
              body.append(System.getProperty("line.separator"));
              // titulosAsignados = BigDecimal.ZERO;
              break;
            } // else
          } // for (FallidoAlc fallidoAlc : fallidosAlcList)
        } // for (Map.Entry<Fallido, BigDecimal> entry : asignacionesPui.entrySet())

        sendMail.sendMail(Arrays.asList(emailsAsignacionPui.split(";")), subject, body.toString());
      } // if (registrosNovalidos.size() > 0)
    } catch (Exception e) {
      LOG.warn("[procesarCSECLENDING] Incidencia al enviar email ... " + e.getMessage());
    } // catch

    LOG.debug("[procesarCSECLENDING] Fin ...");
  } // procesarCSECLENDING

  /**
   * Procesa las entradas del los ficheros <code>CSECLENDCANC</code> que contienen las cacelaciones de PUI.
   * <p>
   * La cancelación del PUI puede ser parcial.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CSECLENDCANC.C0.XML.
   */
  private void procesarCSECLENDCANC(JobPK jobPk, TradeCaptureReportMessageT am) {
    LOG.debug("[procesarCSECLENDCANC] Inicio ...");

    List<Fallido> fallidosList = fallidoDao.findByILiqEcc(am.getOrigTrdID());
    LOG.debug("[procesarCSECLENDCANC] Numero de operaciones fallidas: " + fallidosList.size());

    MovimientoFallido mf = null;

    BigDecimal volumenPorCancelar = am.getLastQty();
    BigDecimal volumenCancelado = BigDecimal.ZERO;

    Integer contador = 1;

    for (Fallido fallido : fallidosList) {
      LOG.debug("[procesarCSECLENDCANC] Procesando fallido: " + fallido.toString());
      LOG.debug("[procesarCSECLENDCANC] Titulos PUI por cancelar: " + volumenPorCancelar);

      if (volumenPorCancelar.signum() > 0) {
        // Si quedan títulos del PUI por asignar
        if (fallido.getTitulosPuiActivos().signum() > 0) {
          // Si quedan tituloes del PUI concedido por cancelar
          if (volumenPorCancelar.compareTo(fallido.getTitulosPuiActivos()) >= 0) {
            volumenCancelado = fallido.getTitulosPuiActivos();
            volumenPorCancelar = volumenPorCancelar.subtract(volumenCancelado);
            LOG.debug("[procesarCSECLENDCANC] La baja del PUI completa totalmente los PUI activos del alias: '{}'",
                      volumenCancelado);
          } else {
            volumenCancelado = volumenPorCancelar;
            volumenPorCancelar = BigDecimal.ZERO;
            LOG.debug("[procesarCSECLENDCANC] La baja del PUI no completa totalmente los PUI activos del alias: '{}'",
                      volumenCancelado);
          }
        } else {
          LOG.debug("[procesarCSECLENDCANC] El fallido no tiene titulos asignados del PUI ...");
          continue;
        } // else
      } else {
        LOG.debug("[procesarCSECLENDCANC] No hay mas titulos de la baja del PUI por asignar ...");
        continue;
      } // else

      // Se da de baja el PUI
      mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.BPU.getTipo(), fallido,
                                                              volumenCancelado, BigDecimal.ZERO,
                                                              am.getTxnTm().toGregorianCalendar().getTime(),
                                                              am.getBizDt().toGregorianCalendar().getTime());
      saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
      enviarCorreo(jobPk, mf);

      // Se almacenan los titulos cancelados del PUI
      fallido.setTitulosPuiActivos(fallido.getTitulosPuiActivos().subtract(volumenCancelado));
      fallido.setTitulosLiquidados(fallido.getTitulosLiquidados() != null ? fallido.getTitulosLiquidados()
                                                                                   .add(volumenCancelado)
                                                                         : volumenCancelado);

      // Si la cancelación es total se da de baja el colateral, si no, se espera al fichero CSECLENDREMUN que nos
      // actualiza el dato
      // En la documentación de BME dice que el campo grossTrdAmt trae el colateral actualizado, pero en el único
      // fichero que he visto de cancelación de PUI el colateral era el mismo
      if (fallido.getTitulosPuiActivos().signum() == 0) {
        mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.BCO.getTipo(), fallido,
                                                                BigDecimal.ZERO, fallido.getColateral(),
                                                                am.getTxnTm().toGregorianCalendar().getTime(),
                                                                am.getBizDt().toGregorianCalendar().getTime());
        saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
        enviarCorreo(jobPk, mf);

        fallido.setColateral(BigDecimal.ZERO);
      } // if

      fallidoDao.save(fallido);

      contador++;
    } // for (OperacionFallido of : ofList)

    LOG.debug("[procesarCSECLENDCANC] Fin ...");
  } // procesarCSECLENDCANC

  /**
   * Procesa las entradas del los ficheros <code>CSECLENDVALUE</code> que contienen las actualizaciones del colateral de
   * los PUI.
   * <p>
   * Estos fichero ya no se usan, las actualizaciones del colateral de procesan de los ficheros
   * <code>CSECLENDREMUN</code>.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CSECLENDVALUE.C0.XML.
   */
  private void procesarCSECLENDVALUE(JobPK jobPk, TradeCaptureReportMessageT am) {
    LOG.debug("[procesarCSECLENDVALUE] Inicio ...");
    LOG.debug("[procesarCSECLENDVALUE] Este tipo de ficheros no genera ningun movimiento ...");
    LOG.debug("[procesarCSECLENDVALUE] Fin ...");
  } // procesarCSECLENDVALUE

  /**
   * Procesa las entradas del los ficheros <code>CSECLENDREMUN</code> que contienen las actualizaciones del colateral,
   * los intereses que genera el colateral y los intereses que genera el PUI.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CSECLENDREMUN.C0.XML.
   */
  private void procesarCSECLENDREMUN(JobPK jobPk, TradeCaptureReportMessageT am) {
    LOG.debug("[procesarCSECLENDREMUN] Inicio ...");

    List<Fallido> fallidosList = fallidoDao.findByILiqEcc(am.getOrigTrdID());
    LOG.debug("[procesarCSECLENDREMUN] Numero de operaciones fallidas: " + fallidosList.size());

    MovimientoFallido mf = null;

    BigDecimal volumenProporcional = BigDecimal.ZERO;
    // BigDecimal sumaVolumenesProporcionales = BigDecimal.ZERO;

    Boolean hayColateral = Boolean.FALSE;
    BigDecimal colateral = BigDecimal.ZERO;
    BigDecimal colateralProporcional = BigDecimal.ZERO;
    BigDecimal sumaColateralProporcional = BigDecimal.ZERO;

    Boolean hayInp = Boolean.FALSE;
    BigDecimal inp = BigDecimal.ZERO;
    BigDecimal inpProporcional = BigDecimal.ZERO;
    BigDecimal sumaInpProporcional = BigDecimal.ZERO;

    Boolean hayInc = Boolean.FALSE;
    BigDecimal inc = BigDecimal.ZERO;
    BigDecimal incProporcional = BigDecimal.ZERO;
    BigDecimal sumaIncProporcional = BigDecimal.ZERO;

    for (PositionAmountDataBlockT pos : am.getAmt()) {
      LOG.debug("[procesarCSECLENDREMUN] Rsn: " + pos.getRsn());
      if (pos.getRsn().compareTo(String.valueOf(1000)) == 0) {
        // Rsn = 1000 -> Amt = Colateral actualizado
        colateral = pos.getAmt();
        hayColateral = Boolean.TRUE;
        LOG.debug("[procesarCSECLENDREMUN] colateral: " + colateral);
      } else if (pos.getRsn().compareTo(String.valueOf(1100)) == 0) {
        // Rsn = 1100 -> Amt = Remuneración Préstamo Valores
        // Precio que el prestatario paga al prestamista por disponer de los títulos
        // inp = pos.getAmt().abs();
        inp = pos.getAmt();
        hayInp = Boolean.TRUE;
        LOG.debug("[procesarCSECLENDREMUN] inp: " + inp);
      } else if (pos.getRsn().compareTo(String.valueOf(1101)) == 0) {
        // Rsn = 1101 -> Amt = Pago disponibilidad colateral
        // Intereses que genera el colateral
        // inc = pos.getAmt().abs();
        inc = pos.getAmt();
        hayInc = Boolean.TRUE;
        LOG.debug("[procesarCSECLENDREMUN] inc: " + inc);
      } // else
    } // for (PositionAmountDataBlockT pos : am.getAmt())

    Integer contador = 1;

    for (Fallido fallido : fallidosList) {
      LOG.debug("[procesarCSECLENDREMUN] Procesando fallido: " + fallido.toString());

      if (fallido.getTitulosPuiActivos().signum() > 0) {
        LOG.debug("[procesarCSECLENDREMUN] El fallido tiene '{}' titulos asignados del PUI",
                  fallido.getTitulosPuiActivos());

        if (contador != fallidosList.size()) {
          volumenProporcional = fallido.getTitulosPuiActivos().divide(am.getLeavesQty(), 6, RoundingMode.HALF_UP);
          // sumaVolumenesProporcionales = sumaVolumenesProporcionales.add(volumenProporcional);

          if (hayColateral) {
            colateralProporcional = colateral.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
            sumaColateralProporcional = sumaColateralProporcional.add(colateralProporcional);
          } // if (hayColateral)

          if (hayInp) {
            inpProporcional = inp.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
            sumaInpProporcional = sumaInpProporcional.add(inpProporcional);
          } // if (hayInp)

          if (hayInc) {
            incProporcional = inc.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
            sumaIncProporcional = sumaIncProporcional.add(incProporcional);
          } // if (hayInc)
        } else {
          // volumenProporcional = BigDecimal.ONE.subtract(sumaVolumenesProporcionales);

          colateralProporcional = colateral.subtract(sumaColateralProporcional);
          inpProporcional = inp.subtract(sumaInpProporcional);
          incProporcional = inc.subtract(sumaIncProporcional);
        } // else
        LOG.debug("[procesarCSECLENDREMUN] Colateral proporcional: " + colateralProporcional);
        LOG.debug("[procesarCSECLENDREMUN] Inp proporcional: " + inpProporcional);
        LOG.debug("[procesarCSECLENDREMUN] Inc proporcional: " + incProporcional);

        if (hayColateral) {
          // Se generar un movimiento BCO para el ACO existente si ha cambiado el colateral
          if (fallido.getColateral().compareTo(colateralProporcional) != 0) {
            // Si el colateral ha cambiado se da de baja el antiguo
            mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.BCO.getTipo(), fallido,
                                                                    BigDecimal.ZERO, fallido.getColateral(),
                                                                    am.getTxnTm().toGregorianCalendar().getTime(),
                                                                    am.getBizDt().toGregorianCalendar().getTime());
            saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
            enviarCorreo(jobPk, mf);

            // Si el nuevo colateral es distinto de cero se da de alta un nuevo colateral proporcinal
            if (colateralProporcional.signum() > 0) {
              mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.ACO.getTipo(), fallido,
                                                                      BigDecimal.ZERO, colateralProporcional,
                                                                      am.getTxnTm().toGregorianCalendar().getTime(),
                                                                      am.getBizDt().toGregorianCalendar().getTime());
              saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
              enviarCorreo(jobPk, mf);

              fallido.setColateral(colateralProporcional);
            } else {
              fallido.setColateral(BigDecimal.ZERO);
            } // else

            fallidoDao.save(fallido);
          } // if colateral ha cambiado
        } // if (hayColateral)

        if (hayInp) {
          // Generar un movimiento INP
          mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.INP.getTipo(), fallido,
                                                                  BigDecimal.ZERO, inpProporcional,
                                                                  am.getTxnTm().toGregorianCalendar().getTime(),
                                                                  am.getBizDt().toGregorianCalendar().getTime());
          saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
          enviarCorreo(jobPk, mf);
        } // if (hayInp)

        if (hayInc) {
          // Generar un movimiento INC
          mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.INC.getTipo(), fallido,
                                                                  BigDecimal.ZERO, incProporcional,
                                                                  am.getTxnTm().toGregorianCalendar().getTime(),
                                                                  am.getBizDt().toGregorianCalendar().getTime());
          saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
          enviarCorreo(jobPk, mf);
        } // else if
      } // if (fallido.getTitulosPuiActivos().signum() > 0)

      contador++;
    } // for (Fallido fallido : fallidosList)

    LOG.debug("[procesarCSECLENDREMUN] Fin ...");
  } // procesarCSECLENDREMUN

  /**
   * Procesa las entradas del los ficheros <code>CFAILBUYIN</code> que contienen la recompras en títulos.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CFAILBUYIN.C0.XML.
   * @param instruccionOrigen Número de instrucción de liquidación fallida de la ECC.
   */
  private void procesarCFAILBUYIN(JobPK jobPk, TradeCaptureReportMessageT am, String instruccionOrigen) {
    LOG.debug("[procesarCFAILBUYIN] Inicio ...");

    List<Fallido> fallidosList = fallidoDao.findByILiqEcc(instruccionOrigen);
    LOG.debug("[procesarCFAILBUYIN] Numero de operaciones fallidas: " + fallidosList.size());

    MovimientoFallido mf = null;

    BigDecimal grossTrdAmt = am.getGrossTrdAmt(); // Coste de la recompra
    BigDecimal grossTrdAmtProporcional = BigDecimal.ZERO;
    BigDecimal sumaGrossTrdAmtProporcional = BigDecimal.ZERO;
    BigDecimal grossTrdAmtAsignado = BigDecimal.ZERO;

    BigDecimal stipAmtm = BigDecimal.ZERO; // Colateral devuelto por la ECC
    BigDecimal stipAmtmProporcional = BigDecimal.ZERO;
    BigDecimal sumaStipAmtmProporcional = BigDecimal.ZERO;

    BigDecimal stipQtym = BigDecimal.ZERO; // Titulos recomprados
    BigDecimal volumenPorAsignar = BigDecimal.ZERO;
    BigDecimal volumenAsignado = BigDecimal.ZERO;

    BigDecimal volumenProporcional = BigDecimal.ZERO;
    // BigDecimal sumaVolumenesProporcionales = BigDecimal.ZERO;

    // Boolean isRecompraTotal = Boolean.FALSE;

    for (TrdCapRptSideGrpBlockT rptSide : am.getRptSide()) {
      for (StipulationsBlockT stip : rptSide.getStip()) {
        if (stip.getTyp().value().compareTo("QTYM") == 0) {
          LOG.debug("[procesarCFAILBUYIN] QTYM: " + stip.getVal());
          stipQtym = new BigDecimal(stip.getVal());
        } else if (stip.getTyp().value().compareTo("AMTM") == 0) {
          LOG.debug("[procesarCFAILBUYIN] AMTM: " + stip.getVal());
          stipAmtm = new BigDecimal(stip.getVal());
        } // else if
      } // for
    } // for
    volumenPorAsignar = stipQtym;

    Integer contador = 1;

    // A cada posible operación de la IL fallida le corresponden parte de los títulos de la recompra
    // Pueden ser recompras parciales
    for (Fallido fallido : fallidosList) {
      LOG.debug("[procesarCFAILBUYIN] Procesando fallido: " + fallido.toString());
      LOG.debug("[procesarCFAILBUYIN] Titulos recomprados por asignar: " + volumenPorAsignar);

      if (volumenPorAsignar.signum() > 0) {
        // Si quedan títulos de la recompra por asignar
        if (volumenPorAsignar.compareTo(fallido.getTitulos().subtract(fallido.getTitulosLiquidados())) >= 0) {
          // Si con los titulos de la recompra que queden por asignar dan para cubrir el fallido totalmente
          // No se pueden producir varias recompras en titulos, lo que no se recompre en titulos se recomprará en
          // efectivo
          // Se han podido liquidar parte de los titulos fallidos si se le concedió un PUI y devolvió parte de esto
          // títulos
          volumenAsignado = fallido.getTitulos().subtract(fallido.getTitulosLiquidados());
          volumenPorAsignar = volumenPorAsignar.subtract(volumenAsignado);
          // isRecompraTotal = Boolean.TRUE;
          LOG.debug("[procesarCFAILBUYIN] La recompra completa totalmente los fallidos del alias '{}'", volumenAsignado);
        } else {
          volumenAsignado = volumenPorAsignar;
          volumenPorAsignar = BigDecimal.ZERO;
          // isRecompraTotal = Boolean.FALSE;
          LOG.debug("[procesarCFAILBUYIN] La recompra no completa totalmente los fallidos del alias '{}'",
                    volumenAsignado);
        } // else
      } else {
        LOG.debug("[procesarCFAILBUYIN] No hay mas titulos de la recompra por asignar ...");
        continue;
      } // else

      if (contador != fallidosList.size()) {
        volumenProporcional = volumenAsignado.divide(stipQtym, 6, RoundingMode.HALF_UP);

        grossTrdAmtProporcional = grossTrdAmt.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
        sumaGrossTrdAmtProporcional = sumaGrossTrdAmtProporcional.add(grossTrdAmt);

        if (volumenPorAsignar.signum() == 0) {
          stipAmtmProporcional = stipAmtm.subtract(grossTrdAmtAsignado);
        } else {
          stipAmtmProporcional = stipAmtm.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
          sumaStipAmtmProporcional = sumaStipAmtmProporcional.add(stipAmtmProporcional);
        } // else
      } else {
        grossTrdAmtProporcional = grossTrdAmt.subtract(sumaGrossTrdAmtProporcional);

        // Cuando grossTrdAmtAsignado sea cero siempre va a ser cero sumaStipAmtmProporcional
        // if (grossTrdAmtAsignado.signum() == 0) {
        // stipAmtmProporcional = stipAmtm.subtract(sumaStipAmtmProporcional);
        // } else {
        stipAmtmProporcional = stipAmtm.subtract(grossTrdAmtAsignado);
        // } // else
      } // else
      LOG.debug("[procesarCFAILBUYIN] Efectivo recompra proporcional: " + grossTrdAmtProporcional);
      LOG.debug("[procesarCFAILBUYIN] Colateral devuelto proporcional: " + stipAmtmProporcional);

      // Se da de alta la recompra
      mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.RPA.getTipo(), fallido,
                                                              volumenAsignado, grossTrdAmtProporcional,
                                                              am.getTxnTm().toGregorianCalendar().getTime(),
                                                              am.getBizDt().toGregorianCalendar().getTime());
      saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
      enviarCorreo(jobPk, mf);

      // Se actualizan los datos del fallido
      fallido.setTitulosLiquidados(fallido.getTitulosLiquidados().add(volumenAsignado));
      fallido.setTitulosRecomprados(fallido.getTitulosRecomprados().add(volumenAsignado));

      if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0)) {
        // Se da de baja el PUI
        mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.BPU.getTipo(), fallido,
                                                                volumenAsignado, BigDecimal.ZERO,
                                                                am.getTxnTm().toGregorianCalendar().getTime(),
                                                                am.getBizDt().toGregorianCalendar().getTime());
        saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
        enviarCorreo(jobPk, mf);

        // Se actualizan los datos del fallido
        fallido.setTitulosPuiActivos(fallido.getTitulosPuiActivos().subtract(volumenAsignado));

        // Se da de baja el Colateral
        mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.BCO.getTipo(), fallido,
                                                                BigDecimal.ZERO, fallido.getColateral(),
                                                                am.getTxnTm().toGregorianCalendar().getTime(),
                                                                am.getBizDt().toGregorianCalendar().getTime());
        saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
        enviarCorreo(jobPk, mf);

        if (fallido.getTitulosPuiActivos().signum() > 0) {
          mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.ACO.getTipo(),
                                                                  fallido,
                                                                  BigDecimal.ZERO,
                                                                  fallido.getColateral().subtract(stipAmtmProporcional),
                                                                  am.getTxnTm().toGregorianCalendar().getTime(),
                                                                  am.getBizDt().toGregorianCalendar().getTime());
          saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
          enviarCorreo(jobPk, mf);

          fallido.setColateral(fallido.getColateral().subtract(stipAmtmProporcional));
        } else {
          grossTrdAmtAsignado = grossTrdAmtAsignado.add(fallido.getColateral());
          fallido.setColateral(BigDecimal.ZERO);
        } // else
      } // if (instruccionOrigen.charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0))

      fallidoDao.save(fallido);

      contador++;
    } // for (OperacionFallido of : ofList)

    LOG.debug("[procesarCFAILBUYIN] Fin ...");
  } // procesarCFAILBUYIN

  /**
   * Procesa las entradas del los ficheros <code>CFAILCASHSETTL</code> que contienen las liquidaciones en efectivo.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CFAILCASHSETTL.C0.XML.
   * @param instruccionOrigen Número de instrucción de liquidación fallida de la ECC.
   */
  private void procesarCFAILCASHSETTL(JobPK jobPk, TradeCaptureReportMessageT am, String instruccionOrigen) {
    LOG.debug("[procesarCFAILCASHSETTL] Inicio ...");

    List<Fallido> fallidosList = fallidoDao.findByILiqEcc(instruccionOrigen);
    LOG.debug("[procesarCFAILCASHSETTL] Numero de operaciones fallidas: " + fallidosList.size());

    MovimientoFallido mf = null;

    BigDecimal grossTrdAmt = am.getGrossTrdAmt(); // Coste de la recompra
    BigDecimal grossTrdAmtProporcional = BigDecimal.ZERO;
    BigDecimal sumaGrossTrdAmtProporcional = BigDecimal.ZERO;
    BigDecimal grossTrdAmtAsignado = BigDecimal.ZERO;

    BigDecimal stipAmtm = BigDecimal.ZERO; // Colateral devuelto por la ECC
    BigDecimal stipAmtmProporcional = BigDecimal.ZERO;
    BigDecimal sumaStipAmtmProporcional = BigDecimal.ZERO;

    BigDecimal stipQtym = BigDecimal.ZERO; // Titulos recomprados
    BigDecimal volumenPorAsignar = BigDecimal.ZERO;
    BigDecimal volumenAsignado = BigDecimal.ZERO;

    BigDecimal volumenProporcional = BigDecimal.ZERO;
    // BigDecimal sumaVolumenesProporcionales = BigDecimal.ZERO;

    for (TrdCapRptSideGrpBlockT rptSide : am.getRptSide()) {
      for (StipulationsBlockT stip : rptSide.getStip()) {
        if (stip.getTyp().value().compareTo("QTYM") == 0) {
          LOG.debug("[procesarCFAILCASHSETTL] QTYM: " + stip.getVal());
          stipQtym = new BigDecimal(stip.getVal());
        } else if (stip.getTyp().value().compareTo("AMTM") == 0) {
          LOG.debug("[procesarCFAILCASHSETTL] AMTM: " + stip.getVal());
          stipAmtm = new BigDecimal(stip.getVal());
        } // else if
      } // for
    } // for
    volumenPorAsignar = stipQtym;

    Integer contador = 1;

    // A cada posible operación de la IL fallida le corresponden parte de los títulos de la recompra
    for (Fallido fallido : fallidosList) {
      LOG.debug("[procesarCFAILCASHSETTL] Procesando fallido: " + fallido.toString());
      LOG.debug("[procesarCFAILCASHSETTL] Titulos recomprados por asignar: " + volumenPorAsignar);

      if (volumenPorAsignar.signum() > 0) {
        // Si quedan títulos de la recompra por asignar
        if (volumenPorAsignar.compareTo(fallido.getTitulos().subtract(fallido.getTitulosLiquidados())) >= 0) {
          // Si con los titulos de la recompra que queden por asignar dan para cubrir el fallido totalmente
          // Se han podido liquidar parte de los titulos fallidos si se le concedió un PUI y devolvió parte de estos
          // títulos
          volumenAsignado = fallido.getTitulos().subtract(fallido.getTitulosLiquidados());
          volumenPorAsignar = volumenPorAsignar.subtract(volumenAsignado);
          LOG.debug("[procesarCFAILCASHSETTL] La recompra completa totalmente los fallidos del alias '{}'",
                    volumenAsignado);
        } else {
          volumenAsignado = volumenPorAsignar;
          volumenPorAsignar = BigDecimal.ZERO;
          LOG.debug("[procesarCFAILCASHSETTL] La recompra no completa totalmente los fallidos del alias '{}'",
                    volumenAsignado);
        } // else
      } else {
        LOG.debug("[procesarCFAILCASHSETTL] No hay mas titulos de la recompra por asignar ...");
        continue;
      } // else

      if (contador != fallidosList.size()) {
        volumenProporcional = volumenAsignado.divide(stipQtym, 6, RoundingMode.HALF_UP);

        grossTrdAmtProporcional = grossTrdAmt.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
        sumaGrossTrdAmtProporcional = sumaGrossTrdAmtProporcional.add(grossTrdAmt);

        // Realmente todas las comprobaciones por problemas de reparto con los decimales en la recompra en efectivo no
        // tienen sentido, dado que todo
        // lo que no se haya resuelto por cualquiera de los mecanismos anteriores se recompra en efectivo y todo debería
        // quedar resuelto y a cero.
        // Pero para comprobar que no se comenten errores en los calculos nos sirve.
        // stipAmtmProporcional = stipAmtm.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
        // sumaStipAmtmProporcional = sumaStipAmtmProporcional.add(stipAmtmProporcional);
        if (volumenPorAsignar.signum() == 0) {
          stipAmtmProporcional = stipAmtm.subtract(grossTrdAmtAsignado);
        } else {
          stipAmtmProporcional = stipAmtm.multiply(volumenProporcional).setScale(2, RoundingMode.HALF_UP);
          sumaStipAmtmProporcional = sumaStipAmtmProporcional.add(stipAmtmProporcional);
        } // else
      } else {
        grossTrdAmtProporcional = grossTrdAmt.subtract(sumaGrossTrdAmtProporcional);

        // Cuando grossTrdAmtAsignado sea cero siempre va a ser cero sumaStipAmtmProporcional
        // if (grossTrdAmtAsignado.signum() == 0) {
        // stipAmtmProporcional = stipAmtm.subtract(sumaStipAmtmProporcional);
        // } else {
        stipAmtmProporcional = stipAmtm.subtract(grossTrdAmtAsignado);
        // } // else
      } // else
      LOG.debug("[procesarCFAILCASHSETTL] Efectivo recompra proporcional: " + grossTrdAmtProporcional);
      LOG.debug("[procesarCFAILCASHSETTL] Colateral devuelto proporcional: " + stipAmtmProporcional);

      // if (contador != fallidosList.size()) {
      // volumenProporcional = volumenAsignado.divide(stipQtym, 6, RoundingMode.HALF_UP);
      // sumaVolumenesProporcionales = sumaVolumenesProporcionales.add(volumenProporcional);
      // } else {
      // volumenProporcional = BigDecimal.ONE.subtract(sumaVolumenesProporcionales);
      // } // else
      // LOG.debug("[procesarCFAILCASHSETTL] Volumen proporcional: " + volumenProporcional);

      // Se da de alta la recompra
      mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.RPE.getTipo(), fallido,
                                                              volumenAsignado, grossTrdAmtProporcional,
                                                              am.getTxnTm().toGregorianCalendar().getTime(),
                                                              am.getBizDt().toGregorianCalendar().getTime());
      saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
      enviarCorreo(jobPk, mf);

      // Se actualizan los datos del fallido
      fallido.setTitulosLiquidados(fallido.getTitulosLiquidados().add(volumenAsignado));
      fallido.setTitulosRecompradosEfectivo(fallido.getTitulosRecompradosEfectivo().add(volumenAsignado));

      if (am.getOrigTrdID().charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0)) {
        // Se cancela el PUI
        mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.CPU.getTipo(), fallido,
                                                                volumenAsignado, BigDecimal.ZERO,
                                                                am.getTxnTm().toGregorianCalendar().getTime(),
                                                                am.getBizDt().toGregorianCalendar().getTime());
        saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
        enviarCorreo(jobPk, mf);

        // Se actualizan los datos del fallido
        fallido.setTitulosPuiActivos(fallido.getTitulosPuiActivos().subtract(volumenAsignado));

        // Se da de baja el Colateral
        mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.BCO.getTipo(), fallido,
                                                                BigDecimal.ZERO, fallido.getColateral(),
                                                                am.getTxnTm().toGregorianCalendar().getTime(),
                                                                am.getBizDt().toGregorianCalendar().getTime());
        saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
        enviarCorreo(jobPk, mf);

        // Se actualizan los datos del fallido
        // TODO Si se hubiera cometido algún error con los cálculos se vería reflejado al establecer el valor con la
        // operación en vez de con el valor que se tiene. Sólo es por ver si hay errores o si la camara no nos devuelve
        // todo el colatgeral.
        // Debe dar cero.
        // fallido.setColateral(fallido.getColateral().subtract(stipAmtm.multiply(volumenProporcional)));
        grossTrdAmtAsignado = grossTrdAmtAsignado.add(fallido.getColateral());
        fallido.setColateral(fallido.getColateral().subtract(stipAmtmProporcional));
      } // if (instruccionOrigen.charAt(Constantes.TRDID_EXCHTRDTYP_POS) == Constantes.FIXML_PRESTAMO_VUELTA.charAt(0))

      fallidoDao.save(fallido);

      contador++;
    } // for (OperacionFallido of : ofList)

    LOG.debug("[procesarCFAILCASHSETTL] Fin ...");
  } // procesarCFAILCASHSETTL

  /**
   * Procesa las entradas del los ficheros <code>CFAILPENALTIES</code> que contienen las penalizaciones diarias por
   * fallido y prestamos.<br>
   * RSN = 2000 = Penalización diaria por fallido.<br>
   * RSN = 2001 = Coste administrativo recompra o liquidación en efectivo.<br>
   * RSN = 2002 = Comisión por constitución del préstamo.
   * 
   * @param jobPk Para la deteccion de contactos a la hora de enviar correos.
   * @param am Bloque <code>TrdCaptRpt</code> parseado de un fichero CFAILPENALTIES.C0.XML.
   * @param instruccionOrigen Número de instrucción de liquidación fallida de la ECC.
   */
  private void procesarCFAILPENALTIES(JobPK jobPk, TradeCaptureReportMessageT am, String instruccionOrigen) {
    LOG.debug("[procesarCFAILPENALTIES] Inicio ...");

    List<Fallido> fallidosList = fallidoDao.findByILiqEcc(instruccionOrigen);
    LOG.debug("[procesarCFAILPENALTIES] Numero de operaciones fallidas: " + fallidosList.size());

    MovimientoFallido mf = null;

    BigDecimal pdf = BigDecimal.ZERO;
    Boolean hayPdf = Boolean.FALSE;
    BigDecimal volumenProporcionalPdf = BigDecimal.ZERO;
    BigDecimal pdfProporcional = BigDecimal.ZERO;
    BigDecimal sumaPdfProporcional = BigDecimal.ZERO;

    BigDecimal car = BigDecimal.ZERO;
    Boolean hayCar = Boolean.FALSE;
    BigDecimal volumenProporcionalCar = BigDecimal.ZERO;
    BigDecimal carProporcional = BigDecimal.ZERO;
    BigDecimal sumaCarProporcional = BigDecimal.ZERO;

    BigDecimal ccp = BigDecimal.ZERO;
    Boolean hayCcp = Boolean.FALSE;
    BigDecimal volumenProporcionalCcp = BigDecimal.ZERO;
    BigDecimal ccpProporcional = BigDecimal.ZERO;
    BigDecimal sumaCcpProporcional = BigDecimal.ZERO;

    // Se calculan los titulos fallidos de la instrucción de liquidación de la ECC
    BigDecimal titulosFallidosILECC = BigDecimal.ZERO;
    for (Fallido fallido : fallidosList) {
      titulosFallidosILECC = titulosFallidosILECC.add(fallido.getTitulos().subtract(fallido.getTitulosLiquidados()));
    } // for

    for (PositionAmountDataBlockT pos : am.getAmt()) {
      LOG.debug("[procesarCFAILPENALTIES] Rsn: " + pos.getRsn());
      if (pos.getRsn().compareTo(String.valueOf(2000)) == 0) {
        // Rsn = 2000 -> Amt = Penalización diaria por fallido
        pdf = pos.getAmt();
        hayPdf = Boolean.TRUE;
        LOG.debug("[procesarCFAILPENALTIES] pdf: " + pdf);
      } else if (pos.getRsn().compareTo(String.valueOf(2001)) == 0) {
        // Rsn = 2001 -> Amt = Coste administrativo recompra o liquidación en efectivo
        car = pos.getAmt();
        hayCar = Boolean.TRUE;
        LOG.debug("[procesarCFAILPENALTIES] car: " + car);
      } else if (pos.getRsn().compareTo(String.valueOf(2002)) == 0) {
        // Rsn = 2002 -> Amt = Comisión por constitución del préstamo
        // ccp = pos.getAmt().abs();
        ccp = pos.getAmt();
        hayCcp = Boolean.TRUE;
        LOG.debug("[procesarCFAILPENALTIES] ccp: " + ccp);
      } // else
    } // for (PositionAmountDataBlockT pos : am.getAmt())

    Integer contador = 1;

    for (Fallido fallido : fallidosList) {
      LOG.debug("[procesarCFAILPENALTIES] Procesando fallido: " + fallido.toString());
      LOG.debug("[procesarCFAILPENALTIES] am.getLastQty(): " + am.getLastQty());

      if (hayPdf) {
        if (fallido.getTitulos().subtract(fallido.getTitulosLiquidados()).signum() > 0) {
          // Si tiene titulos fallidos, si ya liquido todos no se asigna coste por fallidos
          if (contador != fallidosList.size()) {
            volumenProporcionalPdf = fallido.getTitulos().subtract(fallido.getTitulosLiquidados())
                                            .divide(titulosFallidosILECC, 6, RoundingMode.HALF_UP);

            pdfProporcional = pdf.multiply(volumenProporcionalPdf).setScale(2, RoundingMode.HALF_UP);
            sumaPdfProporcional = sumaPdfProporcional.add(pdfProporcional);
          } else {
            pdfProporcional = pdf.subtract(sumaPdfProporcional);
          } // else
          LOG.debug("[procesarCFAILPENALTIES] Pdf proporcional: " + pdfProporcional);

          mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.PDF.getTipo(), fallido,
                                                                  BigDecimal.ZERO, pdfProporcional,
                                                                  am.getHdr().getSnt().toGregorianCalendar().getTime(),
                                                                  am.getBizDt().toGregorianCalendar().getTime());
          saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
          enviarCorreo(jobPk, mf);
        } // if (fallido.getTitulos().subtract(fallido.getTitulosLiquidados()).signum() > 0)
      } // if (hayPdf)

      if (hayCar) {
        if (fallido.getTitulosRecomprados().add(fallido.getTitulosRecompradosEfectivo()).signum() > 0) {
          // Si se tuvieron que recomprar titulos para el fallido, si resolvió antes no se le asigna coste por recompra
          if (contador != fallidosList.size()) {
            volumenProporcionalCar = fallido.getTitulosRecomprados().add(fallido.getTitulosRecompradosEfectivo())
                                            .divide(am.getLastQty(), 6, RoundingMode.HALF_UP);

            carProporcional = car.multiply(volumenProporcionalCar).setScale(2, RoundingMode.HALF_UP);
            sumaCarProporcional = sumaCarProporcional.add(carProporcional);
          } else {
            carProporcional = car.subtract(sumaCarProporcional);
          } // else
          LOG.debug("[procesarCFAILPENALTIES] Car proporcional: " + carProporcional);

          mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.CAR.getTipo(), fallido,
                                                                  BigDecimal.ZERO, carProporcional,
                                                                  am.getHdr().getSnt().toGregorianCalendar().getTime(),
                                                                  am.getBizDt().toGregorianCalendar().getTime());
          saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
          enviarCorreo(jobPk, mf);
        } // fallido.getTitulosRecomprados().add(fallido.getTitulosRecompradosEfectivo()).signum() > 0) {
      } // hayCar

      if (hayCcp) {
        if (fallido.getTitulosPui().signum() > 0) {
          // Si le han tocado titulos del PUI le corresponde parte del coste, sino no
          if (contador != fallidosList.size()) {
            volumenProporcionalCcp = fallido.getTitulosPui().divide(am.getLastQty(), 6, RoundingMode.HALF_UP);

            ccpProporcional = ccp.multiply(volumenProporcionalCcp).setScale(2, RoundingMode.HALF_UP);
            sumaCcpProporcional = sumaCcpProporcional.add(ccpProporcional);
          } else {
            ccpProporcional = ccp.subtract(sumaCcpProporcional);
          } // else
          LOG.debug("[procesarCFAILPENALTIES] Ccp proporcional: " + ccpProporcional);

          mf = movimientofallidoBo.createAndSaveMovimientoFallido(MovimientosFallidos.CCP.getTipo(), fallido,
                                                                  BigDecimal.ZERO, ccpProporcional,
                                                                  am.getHdr().getSnt().toGregorianCalendar().getTime(),
                                                                  am.getBizDt().toGregorianCalendar().getTime());
          saveMovimientoAlias(mf, fallido.getCdCuentaLiquidacion());
          enviarCorreo(jobPk, mf);
        } // if (fallido.getTitulosPui().signum() > 0)
      } // hayCcp

      contador++;
    } // for (OperacionFallidoDTO of : ofList)

    LOG.debug("[procesarCFAILPENALTIES] Fin ...");
  } // procesarCFAILPENALTIES

  /**
   * Busca las etiquetas de los mails de fallidos y genera un mapa relacionandolos con los valores que las sustituiran.
   * 
   * @param alias Alias al que se enviará el email para obtener las etiquetas en su idioma.
   * @param movimientoFallido Movimiento del que obtener los valores de las etiquetas.
   * @return <code>Map<String, String> - </code>Mapa con la relación etiqueta valor.
   */
  private Map<String, String> createMapping(Alias alias, MovimientoFallido movimientoFallido) {
    BigDecimal titulos = null;
    BigDecimal efectivo = null;

    // Recuperamos datos basicos.
    titulos = movimientoFallido.getTitulosAfectados();
    efectivo = movimientoFallido.getSaldoAfectado();
    LOG.debug("[createMapping] Envio de correo de ajustes al alias: [{} ({})]...", alias.getCdaliass(),
              alias.getCdbrocli());
    LOG.trace("[createMapping] [titulos=={}]...", titulos);
    LOG.trace("[createMapping] [efectivo=={}]...", efectivo);

    if (titulos == null) {
      titulos = new BigDecimal(0);
    }

    if (efectivo == null) {
      efectivo = new BigDecimal(0);
    }

    Map<String, String> mapping = new HashMap<String, String>();
    List<Etiquetas> etiquetas = (List<Etiquetas>) etiquetasDao.findAllByTabla("Fallidos");
    String key = null;
    String campo = null;
    String value = null;

    for (Etiquetas etiqueta : etiquetas) {
      campo = etiqueta.getCampo();
      key = etiqueta.getEtiqueta();
      switch (campo) {
        case "titulos":
          value = titulos.toString();
          break;
        case "saldo":
          value = efectivo.toString();
          break;
        default:
          value = "";
      } // switch
      mapping.put(key, value);
    } // for (Etiquetas etiqueta : etiquetas)

    return mapping;
  } // createMapping

  /**
   * Reemplza las etiquetas del mensaje por su contenido.
   * 
   * @param texto Texto a modificar.
   * @param mapping Mapa de etiquetas con su contenido.
   * @return <code>java.lang.String - </code>Texto modificado.
   */
  private String reemplazarKeys(String texto, Map<String, String> mapping) {
    for (String key : mapping.keySet()) {
      texto = texto.replace(key, mapping.get(key));
    } // for
    return texto;
  } // reemplazarKeys

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INNER CLASSES

  /**
   * Comparador para ordenar los objetos <code>FallidosAlc</code> por el número de títulos fallidos descendentemente.
   * 
   * @author XI316153
   */
  private class FallidoAlcTitulosFallidosComparator implements Comparator<FallidoAlc> {
    /*
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(FallidoAlc fallidoAlc1, FallidoAlc fallidoAlc2) {
      return fallidoAlc1.getTitulosFallidos().compareTo(fallidoAlc2.getTitulosFallidos());
    } // compare
  } // FallidoAlcTitulosFallidosComparator

} // FixmlFallidoBo
