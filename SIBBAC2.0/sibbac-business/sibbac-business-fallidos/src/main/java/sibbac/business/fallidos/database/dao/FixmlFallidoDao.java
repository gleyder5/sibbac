package sibbac.business.fallidos.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.FixmlFallido;

@Repository
public interface FixmlFallidoDao extends JpaRepository<FixmlFallido, Long> {

  /**
   * Recupera todos los registros para el número de operación especificado.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @return <code>List<FixmlFallido> - </code>Lista de registros encontrados.
   */
  public List<FixmlFallido> findByNumeroOperacion(String numeroOperacion);

  /**
   * Recupera todos los registros para el número de operación y enlace entre patas de prestamo especificados.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @param referenciaPrestamo Enlace entre patas de prestamo por el que filtrar.
   * @return <code>List<FixmlFallido> - </code>Lista de registros encontrados.
   */
  public List<FixmlFallido> findByNumeroOperacionAndReferenciaPrestamo(String numeroOperacion, String referenciaPrestamo);

  /**
   * Recupera el registro para los datos especificados.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @param volumenVivoOperacion Volumen vivo de la operación.
   * @param fileType Tipo de fichero que contiene los datos.
   * @return <code>FixmlFallido - </code>Registro.
   */
  public FixmlFallido findByNumeroOperacionAndVolumenVivoOperacionAndFileType(String numeroOperacion, BigDecimal volumenVivoOperacion, String fileType);

  /**
   * Recupera el registro para los datos especificados.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @param snt Timestamp de envío del fichero.
   * @param fileType Tipo de fichero que contiene los datos.
   * @param rsn Rsn del registro.
   * @return <code>FixmlFallido - </code>Registro.
   */
  public FixmlFallido findByNumeroOperacionAndSntAndFileTypeAndRsn(String numeroOperacion, Date snt, String fileType, String rsn);

  /**
   * Recupera el registro para los datos especificados.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @param snt Timestamp de envío del fichero.
   * @param fileType Tipo de fichero que contiene los datos.
   * @return <code>FixmlFallido - </code>Registro.
   */
  public FixmlFallido findByNumeroOperacionAndSntAndFileType(String numeroOperacion, Date snt, String fileType);

  // /**
  // * Recupera todos los registros para el número de operación y el tipo de fichero especificados ordenados por fecha
  // de
  // * envío del fiichero descendentemente.
  // *
  // * @param numeroOperacion Número de instrucción por el que filtrar.
  // * @return <code>List<FixmlFallido> - </code>Lista de registros encontrados.
  // */
  // @Query("SELECT f FROM FixmlFallido f WHERE f.numeroOperacion=:numeroOperacion AND f.instruccionOrigen=:origTrdId AND f.fileType=:fileType ORDER BY f.snt DESC")
  // public List<FixmlFallido> findPuiLeavesQty(String numeroOperacion, String fileType);

  // TODO

  /**
   * Recupera el registro para los datos especificados.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @param fileType Tipo de fichero.
   * @return <code>FixmlFallido - </code>Registro.
   */
  public FixmlFallido findByNumeroOperacionAndFileType(String numeroOperacion, String fileType);

  /**
   * Recupera todos los registros para el número de operación y enlace entre patas de prestamo especificados.
   * 
   * @param numeroOperacion Número de instrucción por el que filtrar.
   * @param referenciaPrestamo Enlace entre patas de prestamo por el que filtrar.
   * @return <code>List<FixmlFallido> - </code>Lista de registros encontrados.
   */
  @Query("SELECT MAX(f.volumenVivoOperacion) FROM FixmlFallido f WHERE f.numeroOperacion=:trdId AND f.instruccionOrigen=:origTrdId AND f.referenciaPrestamo=:linkId")
  public String findLeavesQtyByMaxSntAndTrdIdAndOrigTrdIdAndLinkId(String trdId, String origTrdId, String linkId);

  /**
   * 
   * @param numeroOperacion
   * @return
   */
  // public FixmlFallido findByNumeroOperacion(String numeroOperacion);

  /**
   * 
   * @param numeroOperacionInical
   * @return
   */
  public FixmlFallido findByNumeroOperacionInicial(String numeroOperacionInical);

  /**
   * 
   * @param numeroOperacionInical
   * @param importeEfectivoInstruccion
   * @return
   */
  @Deprecated
  public List<FixmlFallido> findByInstruccionOrigenAndImporteEfectivoInstruccion(String instruccionOrigen, BigDecimal importeEfectivoInstruccion);

  /**
   * 
   * @param numeroOperacionInical
   * @param sentido
   * @param tipoOperacionEcc
   * @return
   */
  @Deprecated
  public List<FixmlFallido> findByNumeroOperacionInicialAndTipoOperacionEccAndIndicadorPosicion(String numeroOperacionInical,
                                                                                                String tipoOperacionEcc,
                                                                                                String indicadorPosicion);

  /**
   * 
   * @param numeroOperacion
   * @param tipoOperacionEcc
   * @param indicadorPosicion
   * @return
   */
  public List<FixmlFallido> findByNumeroOperacionAndTipoOperacionEccAndIndicadorPosicion(String numeroOperacion,
                                                                                         String tipoOperacionEcc,
                                                                                         String indicadorPosicion);

  /**
   * 
   * @param numeroOperacionInical
   * @param tipoOperacionEcc
   * @param indicadorPosicion
   * @return
   */
  public int countFixmlFallidosByInstruccionOrigenAndTipoOperacionEccAndIndicadorPosicion(String instruccionOrigen,
                                                                                          String tipoOperacionEcc,
                                                                                          String indicadorPosicion);

  /**
   * Si existe una ocurrencia
   * 
   * @param numeroOperacion
   * @return
   */
  public int countByNumeroOperacion(String numeroOperacion);

  /**
   * Para ver si existe un ajuste de titulos previo
   * 
   * @param instruccionOrigen
   * @param refEventoCorporativo
   * @param indicadorPosicion
   * @param volumeOperacion
   * @return
   */
  public int countByNumeroOperacionNotAndInstruccionOrigenAndRefEventoCorporativoAndIndicadorPosicionAndVolumenOperacionIsGreaterThan(String numeroOperacion,
                                                                                                                                      String instruccionOrigen,
                                                                                                                                      String refEventoCorporativo,
                                                                                                                                      String indicadorPosicion,
                                                                                                                                      BigDecimal volumeOperacion);

  /**
   * Recupera todos los ajustes no procesados
   * 
   * @param tipos
   * @return
   */
  @Query("SELECT f FROM FixmlFallido f WHERE f.fileType = :tipo AND f.procesado = false ORDER BY f.fileType DESC, f.snt ASC")
  public List<FixmlFallido> findByTipoFicheroAndNotProcesadoOrderByTipoFicheroDescSntAsc(@Param("tipo") String tipo);

  @Query("SELECT DISTINCT ff.instruccionOrigen FROM FixmlFallido ff WHERE ff.iliqdcv = :iliqdcv")
  public String findByIliqdcv(@Param("iliqdcv") String iliqdcv);

  @Query("SELECT ff FROM FixmlFallido ff"
         + " WHERE ff.importeEfectivoInstruccion = :importeEfectivoInstruccion AND ff.importeEfectivoVivoOperacion = :importeEfectivoVivoOperacion"
         + "       AND ff.isin = :isin AND ff.volumenOperacion = :volumenOperacion AND ff.volumenVivoOperacion = :volumenVivoOperacion"
         + "       AND ff.fileType = :fileType AND ff.fechaLiquidacion = :fechaLiquidacion AND ff.sentido = '1' ")
  public FixmlFallido findFixmlForS3Colateral(@Param("importeEfectivoInstruccion") BigDecimal importeEfectivoInstruccion,
                                              @Param("importeEfectivoVivoOperacion") BigDecimal importeEfectivoVivoOperacion,
                                              @Param("isin") String isin,
                                              @Param("volumenOperacion") BigDecimal volumenOperacion,
                                              @Param("volumenVivoOperacion") BigDecimal volumenVivoOperacion,
                                              @Param("fileType") String fileType,
                                              @Param("fechaLiquidacion") Date fechaLiquidacion);

  @Query("SELECT ff FROM FixmlFallido ff"
         + " WHERE ff.importeEfectivoInstruccion = :importeEfectivoInstruccion AND ff.isin = :isin AND ff.fileType = :fileType AND ff.fechaLiquidacion = :fechaLiquidacion AND ff.sentido = '1' ")
  public FixmlFallido findFixmlForS3AjusteColateral(@Param("importeEfectivoInstruccion") BigDecimal importeEfectivoInstruccion,
                                                    @Param("isin") String isin,
                                                    @Param("fileType") String fileType,
                                                    @Param("fechaLiquidacion") Date fechaLiquidacion);

  @Query("SELECT ff FROM FixmlFallido ff WHERE ff.ilqs3 = :ilqs3")
  public FixmlFallido findFixmlForMn43(@Param("ilqs3") String ilqs3);

} // FixmlFallidoDao

