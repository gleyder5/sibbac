package sibbac.business.fallidos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonFormat;

import sibbac.common.utils.FormatDataUtils;

/**
 * @version 1.0 Represanta una operacion RT para enviar a la CNMV
 * @author Neoris
 */
public class OperacionTrDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES ********************

  private static final long serialVersionUID = 1L;

  protected static final Logger LOG = LoggerFactory.getLogger(OperacionTrDTO.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS ********************

  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private Short nucnfliq;
  private Date feejecuc;
  @JsonFormat(pattern = "HH:mm:ss")
  private Date hoejecuc;
  private Date fechaHoraEjec;
  private Date feliquidacion;
  private Integer cdestadoasig;
  private Character sentido;
  private String cd_plataforma_negociacion;
  private String nurefexemkt;
  private String isin;
  private BigDecimal imtitulos;
  private BigDecimal imprecio;
  private BigDecimal imefectivo;
  private String cddivisa;
  private Long iddesglosecamara;
  private String estadoCNMV;
  private String codigoLei;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constructores ********************

  public OperacionTrDTO() {

  }

  public OperacionTrDTO(String nuorden, String nbooking, String nucnfclt, Short nucnfliq, Date feejecuc, Date hoejecuc,
      Date feliquidacion, Integer cdestadoasig, Character sentido, String cd_plataforma_negociacion,
      String nurefexemkt, String isin, BigDecimal imtitulos, BigDecimal imprecio, BigDecimal imefectivo,
      String cddivisa, Long iddesglosecamara, String estadoCNMV, Date fechaHoraEjec, String codigoLei) {
    super();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.feliquidacion = feliquidacion;
    this.cdestadoasig = cdestadoasig;
    this.sentido = sentido;
    this.cd_plataforma_negociacion = cd_plataforma_negociacion;
    this.nurefexemkt = nurefexemkt;
    this.isin = isin;
    this.imtitulos = imtitulos;
    this.imprecio = imprecio;
    this.imefectivo = imefectivo;
    this.cddivisa = cddivisa;
    this.iddesglosecamara = iddesglosecamara;
    this.estadoCNMV = estadoCNMV;
    this.fechaHoraEjec = fechaHoraEjec;
    this.codigoLei = codigoLei;
  }

  public OperacionTrDTO(Object[] operacionRTobj) {
    this.nuorden = String.valueOf(operacionRTobj[0]).trim();
    this.nbooking = String.valueOf(operacionRTobj[1]).trim();
    this.nucnfclt = String.valueOf(operacionRTobj[2]).trim();
    this.nucnfliq = Short.valueOf(operacionRTobj[3].toString());
    this.feejecuc = FormatDataUtils.convertStringToDate(operacionRTobj[4].toString(),
        FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    this.feliquidacion = FormatDataUtils.convertStringToDate(operacionRTobj[5].toString(),
        FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    this.cdestadoasig = Integer.valueOf(operacionRTobj[6].toString());
    this.sentido = String.valueOf(operacionRTobj[7]).charAt(0);
    this.cd_plataforma_negociacion = String.valueOf(operacionRTobj[8]).trim();
    this.nurefexemkt = String.valueOf(operacionRTobj[9]).trim();
    this.isin = String.valueOf(operacionRTobj[10]).trim();
    this.imtitulos = new BigDecimal(operacionRTobj[11].toString());
    this.imprecio = new BigDecimal(operacionRTobj[12].toString());
    this.imefectivo = new BigDecimal(operacionRTobj[13].toString());
    this.cddivisa = String.valueOf(operacionRTobj[14]).trim();
    ;
    this.iddesglosecamara = new Long(operacionRTobj[15].toString());
    ;

  }

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @return the feejecuc
   */
  public Date getFeejecuc() {
    return feejecuc;
  }

  /**
   * @param feejecuc the feejecuc to set
   */
  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  /**
   * @return the hoejecuc
   */
  public Date getHoejecuc() {
    return hoejecuc;
  }

  /**
   * @param hoejecuc the hoejecuc to set
   */
  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  /**
   * @return the feliquidacion
   */
  public Date getFeliquidacion() {
    return feliquidacion;
  }

  /**
   * @param feliquidacion the feliquidacion to set
   */
  public void setFeliquidacion(Date feliquidacion) {
    this.feliquidacion = feliquidacion;
  }

  /**
   * @return the cdestadoasig
   */
  public Integer getCdestadoasig() {
    return cdestadoasig;
  }

  /**
   * @param cdestadoasig the cdestadoasig to set
   */
  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  /**
   * @return the sentido
   */
  public Character getSentido() {
    return sentido;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  /**
   * @return the cd_plataforma_negociacion
   */
  public String getCd_plataforma_negociacion() {
    return cd_plataforma_negociacion;
  }

  /**
   * @param cd_plataforma_negociacion the cd_plataforma_negociacion to set
   */
  public void setCd_plataforma_negociacion(String cd_plataforma_negociacion) {
    this.cd_plataforma_negociacion = cd_plataforma_negociacion;
  }

  /**
   * @return the nurefexemkt
   */
  public String getNurefexemkt() {
    return nurefexemkt;
  }

  /**
   * @param nurefexemkt the nurefexemkt to set
   */
  public void setNurefexemkt(String nurefexemkt) {
    this.nurefexemkt = nurefexemkt;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @return the imtitulos
   */
  public BigDecimal getImtitulos() {
    return imtitulos;
  }

  /**
   * @param imtitulos the imtitulos to set
   */
  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  /**
   * @return the imprecio
   */
  public BigDecimal getImprecio() {
    return imprecio;
  }

  /**
   * @param imprecio the imprecio to set
   */
  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  /**
   * @return the imefectivo
   */
  public BigDecimal getImefectivo() {
    return imefectivo;
  }

  /**
   * @param imefectivo the imefectivo to set
   */
  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  /**
   * @return the cddivisa
   */
  public String getCddivisa() {
    return cddivisa;
  }

  /**
   * @param cddivisa the cddivisa to set
   */
  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  /**
   * @return the iddesglosecamara
   */
  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  /**
   * @param iddesglosecamara the iddesglosecamara to set
   */
  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  /**
   * @return the estadoCNMV
   */
  public String getEstadoCNMV() {
    return estadoCNMV;
  }

  /**
   * @param estadoCNMV the estadoCNMV to set
   */
  public void setEstadoCNMV(String estadoCNMV) {
    this.estadoCNMV = estadoCNMV;
  }

  public Date getFechaHoraEjec() {
    return fechaHoraEjec;
  }

  public void setFechaHoraEjec(Date fechaHoraEjec) {
    this.fechaHoraEjec = fechaHoraEjec;
  }

  public String getCodigoLei() {
    return codigoLei;
  }

  public void setCodigoLei(String codigoLei) {
    this.codigoLei = codigoLei;
  }

}
