package sibbac.business.fallidos.database.bo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.Tmct0CnmvDesglosesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvTitularesDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fallidos.utils.ValidatorFieldOperacionesTr;
import sibbac.business.fallidos.writers.OperacionesRtRecordBean;
import sibbac.business.fallidos.writers.OperacionesRtWriter;
import sibbac.business.fallidos.writers.TitularesRtWriter;
import sibbac.business.fallidos.writers.TitularesTrRecordBean;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class WriterOperacionesTrBo {

  @Autowired
  private OperacionesRtWriter writerOperacionesRt;

  @Autowired
  private TitularesRtWriter writerTitulares;

  @Autowired
  private OperacionesTrBo operacionesTrBo;

  @Autowired
  private Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;

  @Autowired
  private Tmct0CnmvDesglosesDao tmct0CnmvDesglosesDao;

  @Autowired
  private Tmct0CnmvTitularesDao tmct0CnmvTitularesDao;

  @Autowired
  private Tmct0CnmvLogDao tmct0CnmvLogDao;

  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  @Autowired
  private ApplicationContext ctx;

  @Value("${id_sibsa.tr.default}")
  private String paramIdEntEjecutora;

  @Value("${entidad_mifid.tr.default}")
  private String paramEntidadMiFid;

  @Value("${tipo_id_sibsa.tr.default}")
  private String paramTipoIdEntTransmisora;

  @Value("${cuenta_agregada.tr.default}")
  private String paramCuentaAgregada;

  @Value("${tipo_cuenta_agregada.tr.default}")
  private String paramTipoCuentaAgregada;

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(OperacionesRtWriter.class);

  public boolean escribirFicheroOperacionesTr() throws SIBBACBusinessException {

    Boolean resultado = Boolean.FALSE;
    Boolean resultadoOperaciones = Boolean.FALSE;
    Boolean resultadoTitulares = Boolean.FALSE;
    StringBuilder sbComentario = new StringBuilder();
    int contador = 1;

    // Se procesan todas las ejecuciones y se obtienen los datos de las
    // operaciones y titulares a escribir en los ficheros.

    // Creamos un registro en el log.
    Tmct0CnmvLog logProceso = new Tmct0CnmvLog();
    logProceso.setAuditUser("SISTEMA");
    logProceso.setAuditDate(new Date());
    logProceso.setProceso(Constantes.PROCESO_GENERAR_FICHERO_RT);
    logProceso.setContadorAltas(BigInteger.ZERO);
    logProceso.setContadorBajas(BigInteger.ZERO);
    logProceso.setIncidencia('N');

    // Recorremos todas las ejecuciones pendientes y a partir de ellas formamos
    // dos listas una con las operaciones y otra con los titulares
    List<Tmct0CnmvEjecuciones> listEjecucionesTr = new ArrayList<Tmct0CnmvEjecuciones>(); // Ejecuciones
                                                                                          // pendientes
                                                                                          // que
                                                                                          // se
                                                                                          // han
                                                                                          // tratado
    List<OperacionesRtRecordBean> listaOperaciones = new ArrayList<OperacionesRtRecordBean>();
    List<TitularesTrRecordBean> listaTitularesOperacionesTr = new ArrayList<TitularesTrRecordBean>();
    List<Tmct0CnmvDesgloses> listDesglosesTr = new ArrayList<Tmct0CnmvDesgloses>(); // Desgloses
                                                                                    // pendientes
                                                                                    // que
                                                                                    // se
                                                                                    // han
                                                                                    // tratado

    OperacionesRtRecordBean operacionTrBean;
    TitularesTrRecordBean titularTrRecordBean;

    Pageable page = new PageRequest(0, 100000);

    Tmct0estado estadoPte = tmct0estadoDao.findOne(EstadosEnumerados.H47.PTE_ENVIAR.getId());

    do {
      // Se cogen las ejecuciones pendientes de enviar que tienen los mismos
      // titulos en la ejecucion que en sus desgloses.
      listEjecucionesTr = tmct0CnmvEjecucionesDao.findOperacionesTr(estadoPte.getIdestado(), page);
      for (Tmct0CnmvEjecuciones ejecucion : listEjecucionesTr) {

        List<Tmct0CnmvDesgloses> listDesglosesEjecucion = null;

        // Se recogen los desglose que se tienen que enviar que son aquellos que
        // tienen un estado desglose distinto a X y estado envio pendiente
        listDesglosesEjecucion = tmct0CnmvDesglosesDao.findPendientesEnvioByEjecucion(ejecucion.getId(),
            estadoPte.getIdestado(), new PageRequest(0, Integer.MAX_VALUE));

        // Ejecucion con un único desglose
        if (listDesglosesEjecucion.size() == 1) {
          contador = 1;
          Tmct0CnmvDesgloses entDesglose = listDesglosesEjecucion.get(0);
          operacionTrBean = new OperacionesRtRecordBean(entDesglose);
          operacionTrBean.setCampo5(ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora));
          operacionTrBean.setCampo6(ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid));
          operacionTrBean
              .setCampo51(ValidatorFieldOperacionesTr.validarTipoIdEntTransmisora(paramTipoIdEntTransmisora));
          listaOperaciones.add(operacionTrBean);
          listDesglosesTr.add(entDesglose);

          // Si es un Alta generamos registros el fichero con los titulares.
          if (entDesglose.getEstadoDesglose().equals('A')) {
            // Se crea el primer registro. Titular extra
            titularTrRecordBean = new TitularesTrRecordBean(listDesglosesEjecucion.get(0)); // Contrapartida
            listaTitularesOperacionesTr.add(titularTrRecordBean);
            contador++;
            // Se recorren el resto de titulares y se escriben

            List<Tmct0CnmvTitulares> listaTitulares = tmct0CnmvTitularesDao.findByDesglose(listDesglosesEjecucion
                .get(0).getId());

            for (Tmct0CnmvTitulares tmct0CnmvTitular : listaTitulares) {
              titularTrRecordBean = new TitularesTrRecordBean(listDesglosesEjecucion.get(0), tmct0CnmvTitular, contador);
              listaTitularesOperacionesTr.add(titularTrRecordBean);
              contador++;
            }
          }

        }

        // Ejecucion con varios desgloses.
        if (listDesglosesEjecucion.size() > 1) {

          // Datos del registro extra de la operacion, a partir de la ejecución
          // y de uno de sus desgloses.

          // Modificacion DDR 3.2 Solo se genera si ESTADO_OPERACION de
          // bsnbpsql.tmct0_cnmv_ejecuciones es “A” y FECHA_ENVIO_ALTA de
          // bsnbpsql.tmct0_cnmv_ejecuciones no está informado (es null),
          // o si ESTADO_OPERACION de bsnbpsql.tmct0_cnmv_ejecuciones es “B” y
          // FECHA_ENVIO_BAJA de bsnbpsql.tmct0_cnmv_ejecuciones no está
          // informado (es null):
          if ((ejecucion.getEstadoOperacion() == 'A' && ejecucion.getfEnvioAlta() == null)
              || (ejecucion.getEstadoOperacion() == 'B' && ejecucion.getfEnvioBaja() == null)) {
            // Datos de la operacion
            operacionTrBean = new OperacionesRtRecordBean(ejecucion, listDesglosesEjecucion.get(0));
            operacionTrBean.setCampo5(ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora));
            operacionTrBean.setCampo6(ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid));
            operacionTrBean.setCampo51(ValidatorFieldOperacionesTr
                .validarTipoIdEntTransmisora(paramTipoIdEntTransmisora));
            listaOperaciones.add(operacionTrBean);

          }

          // Datos titulares extra de la operacion.
          // Modificacion DDR 3.2 Solo se genera si ESTADO_OPERACION de
          // bsnbpsql.tmct0_cnmv_ejecuciones es “A” y FECHA_ENVIO_ALTA de
          // bsnbpsql.tmct0_cnmv_ejecuciones no está informado (es null),
          if (ejecucion.getEstadoOperacion() == 'A' && ejecucion.getfEnvioAlta() == null) {
            // Datos del Primer titular extra - Vendedor
            titularTrRecordBean = new TitularesTrRecordBean(ejecucion); // Contrapartida
            listaTitularesOperacionesTr.add(titularTrRecordBean);
            // Datos del Segundo titular extra - Comprador
            titularTrRecordBean = new TitularesTrRecordBean(ejecucion, paramCuentaAgregada, paramTipoCuentaAgregada); // Contrapartida
            listaTitularesOperacionesTr.add(titularTrRecordBean);

          }

          // Se recorren los desgloses y se añaden los datos de la operacion y
          // sus titulares.
          for (Tmct0CnmvDesgloses entDesglose : listDesglosesEjecucion) {
            contador = 1;

            // Datos de la operacion
            operacionTrBean = new OperacionesRtRecordBean(entDesglose);
            operacionTrBean.setCampo5(ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora));
            operacionTrBean.setCampo6(ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid));
            operacionTrBean.setCampo51(ValidatorFieldOperacionesTr
                .validarTipoIdEntTransmisora(paramTipoIdEntTransmisora));
            listaOperaciones.add(operacionTrBean);
            listDesglosesTr.add(entDesglose);

            // Datos de los titulares , Solo se escriben si la operacion es un
            // alta.
            // Si es un Alta generamos registros el fichero con los titulares.
            if (entDesglose.getEstadoDesglose().equals('A')) {
              // Se crea el primer registro. Titular extra del desglose actual
              titularTrRecordBean = new TitularesTrRecordBean(entDesglose); // Contrapartida
              listaTitularesOperacionesTr.add(titularTrRecordBean);
              contador++;
              // Se recorren el resto de titulares y se escriben
              List<Tmct0CnmvTitulares> listaTitulares = tmct0CnmvTitularesDao.findByDesglose(entDesglose.getId());
              for (Tmct0CnmvTitulares tmct0CnmvTitular : listaTitulares) {
                titularTrRecordBean = new TitularesTrRecordBean(entDesglose, tmct0CnmvTitular, contador);
                listaTitularesOperacionesTr.add(titularTrRecordBean);
                contador++;
              }
            }
          }
        }
      }

      // Despues de recorrer las ejecuciones y preparar los datos de las
      // operaciones y los titulares, se escriben en los ficheros.
      if (listaOperaciones != null && !listaOperaciones.isEmpty()) {

        writerOperacionesRt.setListaOperaciones(listaOperaciones);
        resultadoOperaciones = writerOperacionesRt.writeIntoFile();

        // Si las operaciones tienen algun titular, se escriben.
        if (listaTitularesOperacionesTr != null && !listaTitularesOperacionesTr.isEmpty()) {
          writerTitulares.setListTitularesTr(listaTitularesOperacionesTr);
          resultadoTitulares = writerTitulares.writeIntoFile();
        }
      }
      else {
        LOG.warn("[writeIntoFile] No se han generado operaciones RT para enviar ...");
        sbComentario.append(" No se han encontrado operaciones RT a enviar");
      }

      // Si se ha escrito operaciones
      if (resultadoOperaciones) {
        try {
          writerOperacionesRt.renameTempFile();
          // Si se ha escrito titualres
          if (resultadoTitulares) {
            writerTitulares.renameTempFile();
          }

          // Una vez escrito bien los ficheros sin ningun problema, se
          // actualizan los contadores y las fechas de envio.
          for (OperacionesRtRecordBean operacionTrBeanEnviada : listaOperaciones) {
            // En el campo 3 se incluye el estado de la operacion, lo usuamos
            // para incrementar el contador de altas o bajas.
            if (operacionTrBeanEnviada.getCampo3().equals("A")) {
              logProceso.setContadorAltas(logProceso.getContadorAltas().add(BigInteger.ONE));
            }

            if (operacionTrBeanEnviada.getCampo3().equals("B")) {
              logProceso.setContadorBajas(logProceso.getContadorBajas().add(BigInteger.ONE));
            }
          }

          operacionesTrBo.actualizarEstadoEnvios(listEjecucionesTr, listDesglosesTr);
          resultado = Boolean.TRUE;
        }
        catch (Exception e) {
          LOG.error("[escribirFicheroOperacionesTr] excepción no controlada. " + e.getMessage());
        }
      }

      // Guardamos el resultado del proceso.
      operacionesTrBo.guardarLog(logProceso, sbComentario);
    }
    while (page.getPageSize() == listEjecucionesTr.size());
    return resultado;
  }

  @Transactional
  public boolean escribirFicheroOperacionesTrMultiThread(boolean isSd) throws SIBBACBusinessException {
    int tamThreadPool = 15;
    Boolean resultado = Boolean.FALSE;

    WriteOperacionesTrCallable callable;

    StringBuilder sbComentario = new StringBuilder();
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();

    // Se procesan todas las ejecuciones y se obtienen los datos de las
    // operaciones y titulares a escribir en los ficheros.

    // Creamos un registro en el log.
    Tmct0CnmvLog logProceso = new Tmct0CnmvLog();
    logProceso.setAuditUser("SISTEMA");
    logProceso.setAuditDate(new Date());
    logProceso.setProceso(Constantes.PROCESO_GENERAR_FICHERO_RT);
    logProceso.setContadorAltas(BigInteger.ZERO);
    logProceso.setContadorBajas(BigInteger.ZERO);
    logProceso.setIncidencia('N');
    Path ficheroQuery;
    try {
      ficheroQuery = this.recuperarDatos();
    }
    catch (IOException e) {
      throw new SIBBACBusinessException("No se ha podido recuperar los datos para enviar el fichero TR CNMV", e);
    }

    if (ficheroQuery != null && Files.exists(ficheroQuery)) {

      ExecutionContext executionContext = new ExecutionContext();

      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat("Thread-GeneracionFicheroTrCnmv-%d");

      ExecutorService executor = Executors.newFixedThreadPool(tamThreadPool, tfb.build());

      FlatFileItemWriter<OperacionesRtRecordBean> opWriter = writerOperacionesRt.getNewWriter(executionContext);

      FlatFileItemWriter<TitularesTrRecordBean> titWriter = writerTitulares.getNewWriter(executionContext);
      try (InputStream is = Files.newInputStream(ficheroQuery, StandardOpenOption.READ);
          GZIPInputStream gis = new GZIPInputStream(is);
          BufferedReader br = new BufferedReader(new InputStreamReader(gis, StandardCharsets.ISO_8859_1));) {
        for (int i = 0; i < tamThreadPool; i++) {
          callable = ctx.getBean(WriteOperacionesTrCallable.class, br, isSd ? 1 : 100, opWriter, titWriter, logProceso);
          listFutures.add(executor.submit(callable));
        }
        executor.shutdown();

        try {
          executor.awaitTermination((long) 12, TimeUnit.HOURS);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage());
          LOG.trace(e.getMessage(), e);
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException("No se ha podido leer el fichero de datos para TR CNMV.", e);
      }
      finally {
        try {
          Files.deleteIfExists(ficheroQuery);
        }
        catch (IOException e) {
          LOG.warn("No se ha podido borrar el fichero. {}", e.getMessage());
          LOG.trace(e.getMessage(), e);
        }
      }

      for (Future<Void> future : listFutures) {
        try {
          future.get((long) 2, TimeUnit.MINUTES);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
          LOG.warn(e.getMessage());
          LOG.trace(e.getMessage(), e);
          sbComentario.append(e.getMessage()).append(" - ");
        }
      }

      writerOperacionesRt.closeWriter(opWriter, executionContext);
      writerTitulares.closeWriter(titWriter, executionContext);

      try {
        Thread.sleep(10000);
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
        sbComentario.append(e.getMessage()).append(" - ");
      }

      writerOperacionesRt.renameTempFile();
      writerTitulares.renameTempFile();
      operacionesTrBo.guardarLog(logProceso, sbComentario);
      resultado = true;
    }
    return resultado;
  }

  private Path recuperarDatos() throws IOException {
    int count;
    boolean datos = false;
    List<BigInteger> listEjecucionesTr;
    Pageable page = new PageRequest(0, 10000);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    Path res = Files.createTempFile(String.format("QueryGeneracionFicheroTrCnmv_%s_", sdf.format(new Date())),
        ".txt.gz");
    try (OutputStream os = Files.newOutputStream(res, StandardOpenOption.WRITE);
        GZIPOutputStream gos = new GZIPOutputStream(os);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(gos, StandardCharsets.ISO_8859_1));) {

      do {
        listEjecucionesTr = tmct0CnmvEjecucionesDao.findAllIdsOperacionesTr(EstadosEnumerados.H47.PTE_ENVIAR.getId(),
            page);
        count = listEjecucionesTr.size();

        for (BigInteger reg : listEjecucionesTr) {
          bw.write(reg.toString());
          bw.newLine();
          datos = true;
        }

        page = page.next();
      }
      while (count == page.getPageSize());

    }
    if (datos) {
      return res;
    }
    else {
      Files.deleteIfExists(res);
      return null;
    }
  }
}
