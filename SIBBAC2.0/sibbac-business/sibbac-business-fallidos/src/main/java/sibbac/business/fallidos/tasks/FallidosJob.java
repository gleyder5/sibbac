package sibbac.business.fallidos.tasks;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.quartz.DateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fallidos.database.bo.FallidoBo;
import sibbac.business.fallidos.database.bo.FixmlFallidoBo;
import sibbac.business.fallidos.database.bo.InstruccionLiquidacionFallidaBo;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fixml.database.dao.FixmlFileDao;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums;
import sibbac.fixml.classes.AbstractMessageT;
import sibbac.fixml.classes.BatchT;
import sibbac.fixml.classes.FIXML;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Contiene la lógica de inicialización de la tarea de control de operaciones fallidas.
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author XI316153
 */
@SIBBACJob(name = Task.GROUP_FALLIDOS.JOB_FALLIDOS,
           group = Task.GROUP_FALLIDOS.NAME,
           interval = 10,
           delay = 1,
           intervalUnit = DateBuilder.IntervalUnit.MINUTE)
public class FallidosJob extends FallidosTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FallidosJob.class);

  /** Formato del campo <code>cdmensa</code> de la tabla <code>tmct0men</code>. */
  private static final NumberFormat CDMENSA_FORMAT = new DecimalFormat("000");

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0cfg</code>. */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo = null;

  /** Business object para la tabla <code>Tmct0_fallidos_fallidos</code>. */
  @Autowired
  private FallidoBo fallidoBo;

  /** Objeto para el acceso a la tabla <code>TMCT0_FIXMLFILES</code>. */
  @Autowired
  private FixmlFileDao fixmlFileDao;

  /** Business object para la tabla <code>TMCT0_INSTRUCCION_LIQUIDACION_FALLIDA</code>. */
  @Autowired
  private InstruccionLiquidacionFallidaBo instruccionLiquidacionFallidaBo;

  /** Business object para la tabla <code>TMCT0_FIXML_FALLIDOS</code>. */
  @Autowired
  private FixmlFallidoBo fixmlFallidoBo;

  /** Campo <code>application</code> de la tabla <code>tmct0cfg</code>. */
  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  /** Colección de ficheros de fallidos que se deben procesar. */
  private List<String> tipoFicherosFallidos = new ArrayList<String>();

  /** Fecha para la que buscar los fallidos. */
  private Date searchDate;

  /** Fecha que indica que la búsqueda de fallidos es para el día de la ejecución. */
  private Date noManualDate;

  /** Calendar del día de 'hoy' sin hora. */
  private Calendar hoySinHora;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void executeTask() throws SIBBACBusinessException {
    final long startTime = System.currentTimeMillis();
    LOG.debug("[executeTask] Iniciando la tarea de control de operaciones fallidas ...");

    try {
      if (canExecute()) {
        loadConfiguration();
        fallidoBo.createFallidos(searchDate, Boolean.FALSE);

        if (!searchDate.equals(hoySinHora.getTime())) {
          // El parámetro de la tmct0cfg está relleno con una fecha a procesar. Se marcan como no procesados los fixml
          // que ya lo han sido desde la fecha configurada hasta 'ayer'.
          fixmlFileDao.updateProcesadoByFechaRecepcionAndTipo(searchDate, /* hoySinHora.getTime(), */
                                                              tipoFicherosFallidos);

          // Se actualiza el valor del flag a 9999-12-31
          tmct0cfgBo.getDao().updateKeyvalue(sibbac20ApplicationName, SibbacEnums.Tmct0cfgConfig.SIBBAC20_FALLIDOS_MANUAL_DATE.getProcess(),
                                             SibbacEnums.Tmct0cfgConfig.SIBBAC20_FALLIDOS_MANUAL_DATE.getKeyname(),
                                             FormatDataUtils.convertDateToString(noManualDate, FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR));

          readFIXML(fixmlFileDao.findByTipoAndNotProcesadoAndFeprocesoOrderByZipAsc(searchDate, hoySinHora.getTime(), tipoFicherosFallidos));
        } else {
          readFIXML(fixmlFileDao.findByTipoAndNotProcesadoAndFeprocesoOrderByZipAsc(searchDate, tipoFicherosFallidos));
        } // else

        LOG.debug("[executeTask] Tarea ejecutada correctamente");
      } else {
        LOG.debug("[executeTask] El limite horario no permite ejecutar la tarea");
      }
    } 
    catch (SIBBACBusinessException ex) {
      LOG.error("[executeTask] Error en la ejecucion de fallidos: {}", ex.getMessage(), ex);
      throw ex;
    }
    catch (RuntimeException ex) {
      LOG.error("[executeTask] Error inesperado en la ejecucion de fallidos {}", ex.getMessage(), ex);
      throw ex;
    } 
    finally {
      final long endTime = System.currentTimeMillis(), dif = endTime - startTime;
      LOG.info("[executeTask] Finalizada la tarea de control de operaciones fallidas ... [{}]",
               String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(dif),
                               TimeUnit.MILLISECONDS.toSeconds(dif)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(dif))));
    } // finally
  } // executeTask

  /*
   * @see sibbac.business.fallidos.tasks.FallidosTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.FALLIDO_FALLIDOS;
  } // determinarTipoApunte

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /**
   * Procesa los ficheros FIXML de fallidos recibidos de BME que no esté procesados.
   */
  private void readFIXML(List<String> fixmlFileRegs) {
    LOG.debug("[readFIXML] Inicio ...");
    LOG.debug("[readFIXML] Ficheros ZIP encontrados: " + fixmlFileRegs.size());

    if (fixmlFileRegs != null && !fixmlFileRegs.isEmpty()) {

      ZipFile zipFile = null;
      Enumeration<? extends ZipEntry> entries = null;
      ZipEntry entry = null;

      List<JAXBElement<? extends AbstractMessageT>> lm = null;
      Unmarshaller jaxbUnmarshaller = null;
      FIXML fixed = null;
      List<BatchT> lb = null;
      BatchT batch = null;
      try {
        JAXBContext jaxbContext = JAXBContext.newInstance(FIXML.class);
        jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      } catch (JAXBException jaxbe) {
        LOG.warn("[readFIXML] Incidencia cargando contexto JAXB");
        LOG.warn("[readFIXML] Incidencia: " + jaxbe.getLocalizedMessage());
        LOG.debug("[readFIXML] Pila de la incidencia: ", jaxbe);
      } // catch

      for (String zip : fixmlFileRegs) {
        try {
          LOG.debug("[readFIXML] Procesando ZIP: " + zip.toString());
          zipFile = new ZipFile(zip);

          // Se necesitan procesar los ficheros según el orden en el que están en la lista de tipos de ficheros a
          // procesar
          for (String tipoFichero : tipoFicherosFallidos) {
            LOG.debug("[readFIXML] Buscando ficheros del tipo: " + tipoFichero);

            // zipFile = new ZipFile(zip);
            entries = zipFile.entries();

            while (entries.hasMoreElements()) {
              entry = entries.nextElement();
              LOG.debug("[readFIXML] Procesando ZipEntry: " + entry.getName());

              if (tipoFichero.compareTo(entry.getName().substring(0, entry.getName().lastIndexOf("."))) == 0) {
                try {
                  LOG.debug("[readFIXML] Cargando fixml ...");
                  fixed = (FIXML) jaxbUnmarshaller.unmarshal(zipFile.getInputStream(entry));
                  LOG.debug("[readFIXML] Fixml cargado con exito ...");
                } catch (JAXBException jaxbe) {
                  LOG.warn("[readFIXML] Incidencia haciendo 'unmarshal' del fichero: " + zip + "/" + entry.getName());
                  LOG.warn("[readFIXML] Incidencia: " + jaxbe.getLocalizedMessage());
                  LOG.debug("[readFIXML] Pila de incidencia: ", jaxbe);
                } catch (Exception e) {
                  LOG.warn("[readFIXML] Incidencia generica 'unmarshal' del fichero: " + zip + "/" + entry.getName());
                  LOG.warn("[readFIXML] Incidencia: " + e.getLocalizedMessage());
                  LOG.debug("[readFIXML] Pila de incidencia: ", e);
                }

                try {
                  lb = fixed.getBatch();
                  batch = lb.get(0);
                  lm = batch.getMessage();

                  LOG.debug("[readFIXML] Procesando fixml ...");
                  if (tipoFichero.compareTo(tipoFicherosFallidos.get(0)) == 0) {
                    instruccionLiquidacionFallidaBo.procesarCPENDINGINST(lm, zip, tipoFichero);
                  } else {
                    fixmlFallidoBo.procesarFIXMLFallidos(this.jobPk, lm, zip, tipoFichero);
                  } // else

                  LOG.debug("[readFIXML] Fixml procesado con exito ...");
                } catch (Exception ex) {
                  LOG.warn("[readFIXML] Incidencia procesando Fixml ... " + ex.getMessage());
                  LOG.debug("[readFIXML] Pila de la incidencia", ex);
                  LOG.info("[readFIXML] Se continua con el siguiente fichero ...");
                } // catch

                break; // Sólo va a haber un fichero como máximo de cada tipo por ZIP
              } // if
            } // while
          } // for (String tipoFichero : tipoFicherosFallidos)

        } catch (IOException ioe) {
          LOG.warn("[readFIXML] Incidencia procesando ZIP: " + zip);
          LOG.warn("[readFIXML] Incidencia: " + ioe.getLocalizedMessage());
          LOG.debug("[readFIXML] Pila de incidencia: ", ioe);
        } // catch
      } // for (FixmlFile fixmlFileReg : fixmlFileRegs)
    } // f (fixmlFileRegs != null && !fixmlFileRegs.isEmpty())

    LOG.debug("[readFIXML] Fin ...");
  } // readFIXML

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Comprueba si la tarea se puede ejecutar.<br>
   * La tarea se puede ejecutar el campo <code>femensa</code> es el día de ejecución y el campo <code>hrmensa</code> es una hora posterior a las
   * <code>15:30:00</code>.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si la tarea se puede ejecutar; <code>false</code> en caso contrario.
   */
  private boolean canExecute() {
    LOG.debug("[canExecute] Inicio ...");

    hoySinHora = Calendar.getInstance();
    hoySinHora.set(Calendar.HOUR_OF_DAY, 0);
    hoySinHora.set(Calendar.MINUTE, 0);
    hoySinHora.set(Calendar.SECOND, 0);
    hoySinHora.set(Calendar.MILLISECOND, 0);
    Tmct0cfg tmct0cfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                        SibbacEnums.Tmct0cfgConfig.SIBBAC20_FALLIDOS_MANUAL_DATE.getProcess(),
                                                                        SibbacEnums.Tmct0cfgConfig.SIBBAC20_FALLIDOS_MANUAL_DATE.getKeyname());
    if (tmct0cfg == null) {
      searchDate = FormatDataUtils.convertStringToDate(Constantes.NO_MANUAL_DATE, FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    } else {
      searchDate = FormatDataUtils.convertStringToDate(tmct0cfg.getKeyvalue().trim(), FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    } // else
    LOG.debug("[canExecute] searchDate: {}", searchDate);

    noManualDate = FormatDataUtils.convertStringToDate(Constantes.NO_MANUAL_DATE, FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);

    if (searchDate.equals(noManualDate)) {
      Tmct0men tmct0men = tmct0menBo.findByCdmensa(CDMENSA_FORMAT.format(TIPO_TAREA.TASK_CLEARING_RESPUESTA_FICHERO_MEGARA.getTipo()));

      Calendar calFemensaRespuestaMegara = Calendar.getInstance();
      calFemensaRespuestaMegara.setTime(tmct0men.getFemensa());
      LOG.debug("[canExecute] Task Respuesta Megara: Femensa [{}], Hrmensa [{}], Nuenvio [{}], Momento actual [{}]",
          tmct0men.getFemensa(), tmct0men.getHrmensa(), tmct0men.getNuenvio(), hoySinHora.getTime());
      if (calFemensaRespuestaMegara.getTime().compareTo(hoySinHora.getTime()) == 0 && tmct0men.getNuenvio() > 2) {
        LOG.debug("[canExecute] La tarea puede ejecutarse ...");
        searchDate = hoySinHora.getTime();
      } else {
        LOG.debug("[canExecute] La tarea no puede ejecutarse ...");
        return false;
      } // else
    }
    return true;
  } // canExecute

  /**
   * Carga los valores de configuración del proceso.
   */
  private void loadConfiguration() {
    LOG.debug("[loadConfiguration] Inicio ...");
    tipoFicherosFallidos.add(Constantes.CPENDINGINST_FILENAME); // Instrucciones pendientes de liquidar - 3
    tipoFicherosFallidos.add(Constantes.CSECLENDING_FILENAME); // Generación de los PUI - 4 y 5
    tipoFicherosFallidos.add(Constantes.CSECLENDCANC_FILENAME); // Cancelación de los PUI
    tipoFicherosFallidos.add(Constantes.CSECLENDVALUE_FILENAME); // Actualizaciones del colateral - 5
    tipoFicherosFallidos.add(Constantes.CSECLENDREMUN_FILENAME); // Remuneraciones/penalizaciones diarias de los PUI - 5
    tipoFicherosFallidos.add(Constantes.CFAILBUYIN_FILENAME); // Recompras de títulos
    tipoFicherosFallidos.add(Constantes.CFAILCASHSETTL_FILENAME); // Liquidaciones en efectivo
    tipoFicherosFallidos.add(Constantes.CFAILPENALTIES_FILENAME); // Penalizaciones de los PUI
    LOG.debug("[loadConfiguration] Fin ...");
  } // loadConfiguration

} // FallidosJob
