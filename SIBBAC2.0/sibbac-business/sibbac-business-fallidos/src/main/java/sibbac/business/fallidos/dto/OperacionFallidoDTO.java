package sibbac.business.fallidos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OperacionFallidoDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -5651084561231989974L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Identificador del registro. */
  private Long id;

  /** Numero operacion. */
  private String numeroOperacion;

  /** Numero operacion previa. */
  private String numeroOperacionPrevia;

  /** Numero operacion inicial. */
  private String numeroOperacionInicial;

  /** Títulos fallidos op. En IL. */
  private BigDecimal titulosFallidosOperacionIL;

  /** Títulos op. En IL. */
  private BigDecimal titulosOperacionIL;

  /** Títulos fallidos op. */
  private BigDecimal titulosFallidosOperacion;

  /** Efectivo op. En IL. */
  private BigDecimal efectivoOperacionIL;

  /** Número op. de IL en la ECC. */
  private String numeroOperacionIlEcc;

  /** Número op. DCV. */
  private String numeroOperacionDcv;

  /** Estado IL. */
  private String estadoIL;

  /** Referencia del evento corporativo. */
  private String refEventoCorporativo;

  /** Situación operación. */
  private String situacionOperacion;

  /** ISIN. */
  private String isin;

  /** Precio. */
  private BigDecimal precio;

  /** Sentido. */
  private Character sentido;

  /** Numero de la orden de mercado. */
  private String numeroOrdenMercado;

  /** Numero de la ejecucion de mercado. */
  private String numeroEjecucionMercado;

  /** Holder. */
  private String holder;

  /** Segmento. */
  private String segmento;

  /** Camara. */
  private String camara;

  /** Descripcion del alias. */
  private String descripcionAlias;

  /** Descripcion del ISIN. */
  private String descripcionIsin;

  /** Alias (cuenta). */
  private String alias;

  /** Subcuenta. */
  private String subCuenta;

  /** Fecha de ejecución de la operación. */
  private Date feejecuc;

  /** Fecha de liquidación. */
  private Date sttlDt;

  private boolean liquidado;

  private String descripcionHolder;

  private String cuentaLiquidacion;

  /** Importe efectivo fallido. */
  private BigDecimal importeEfectivoFallido;

  /** Corretaje. */
  private BigDecimal corretaje;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public OperacionFallidoDTO() {
  } // OperacionFallidoDTO

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "OperacionFallidoDTO [id=" + id + ", numeroOperacion=" + numeroOperacion + ", numeroOperacionPrevia=" + numeroOperacionPrevia
        + ", numeroOperacionInicial=" + numeroOperacionInicial + ", titulosFallidosOperacionIL=" + titulosFallidosOperacionIL
        + ", titulosOperacionIL=" + titulosOperacionIL + ", titulosFallidosOperacion=" + titulosFallidosOperacion
        + ", efectivoOperacionIL=" + efectivoOperacionIL + ", numeroOperacionIlEcc=" + numeroOperacionIlEcc + ", numeroOperacionDcv="
        + numeroOperacionDcv + ", estadoIL=" + estadoIL + ", refEventoCorporativo=" + refEventoCorporativo + ", situacionOperacion="
        + situacionOperacion + ", isin=" + isin + ", precio=" + precio + ", sentido=" + sentido + ", numeroOrdenMercado="
        + numeroOrdenMercado + ", numeroEjecucionMercado=" + numeroEjecucionMercado + ", holder=" + holder + ", segmento=" + segmento
        + ", camara=" + camara + ", descripcionAlias=" + descripcionAlias + ", descripcionIsin=" + descripcionIsin + ", alias=" + alias
        + ", subCuenta=" + subCuenta + ", feejecuc=" + feejecuc + ", sttlDt=" + sttlDt + ", liquidado=" + liquidado
        + ", descripcionHolder=" + descripcionHolder + ", cuentaLiquidacion=" + cuentaLiquidacion + ", importeEfectivoFallido="
        + importeEfectivoFallido + ", corretaje=" + corretaje + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the numeroOperacion
   */
  public String getNumeroOperacion() {
    return numeroOperacion;
  }

  /**
   * @return the numeroOperacionPrevia
   */
  public String getNumeroOperacionPrevia() {
    return numeroOperacionPrevia;
  }

  /**
   * @return the numeroOperacionInicial
   */
  public String getNumeroOperacionInicial() {
    return numeroOperacionInicial;
  }

  /**
   * @return the titulosFallidosOperacionIL
   */
  public BigDecimal getTitulosFallidosOperacionIL() {
    return titulosFallidosOperacionIL;
  }

  /**
   * @return the titulosOperacionIL
   */
  public BigDecimal getTitulosOperacionIL() {
    return titulosOperacionIL;
  }

  /**
   * @return the titulosFallidosOperacion
   */
  public BigDecimal getTitulosFallidosOperacion() {
    return titulosFallidosOperacion;
  }

  /**
   * @return the efectivoOperacionIL
   */
  public BigDecimal getEfectivoOperacionIL() {
    return efectivoOperacionIL;
  }

  /**
   * @return the numeroOperacionIlEcc
   */
  public String getNumeroOperacionIlEcc() {
    return numeroOperacionIlEcc;
  }

  /**
   * @return the numeroOperacionDcv
   */
  public String getNumeroOperacionDcv() {
    return numeroOperacionDcv;
  }

  /**
   * @return the estadoIL
   */
  public String getEstadoIL() {
    return estadoIL;
  }

  /**
   * @return the refEventoCorporativo
   */
  public String getRefEventoCorporativo() {
    return refEventoCorporativo;
  }

  /**
   * @return the situacionOperacion
   */
  public String getSituacionOperacion() {
    return situacionOperacion;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the precio
   */
  public BigDecimal getPrecio() {
    return precio;
  }

  /**
   * @return the sentido
   */
  public Character getSentido() {
    return sentido;
  }

  /**
   * @return the numeroOrdenMercado
   */
  public String getNumeroOrdenMercado() {
    return numeroOrdenMercado;
  }

  /**
   * @return the numeroEjecucionMercado
   */
  public String getNumeroEjecucionMercado() {
    return numeroEjecucionMercado;
  }

  /**
   * @return the holder
   */
  public String getHolder() {
    return holder;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @return the camara
   */
  public String getCamara() {
    return camara;
  }

  /**
   * @return the descripcionAlias
   */
  public String getDescripcionAlias() {
    return descripcionAlias;
  }

  /**
   * @return the descripcionIsin
   */
  public String getDescripcionIsin() {
    return descripcionIsin;
  }

  /**
   * @return the alias
   */
  public String getAlias() {
    return alias;
  }

  /**
   * @return the subCuenta
   */
  public String getSubCuenta() {
    return subCuenta;
  }

  /**
   * @return the feejecuc
   */
  public Date getFeejecuc() {
    return feejecuc;
  }

  /**
   * @return the sttlDt
   */
  public Date getSttlDt() {
    return sttlDt;
  }

  /**
   * @return the cuentaLiquidacion
   */
  public String getCuentaLiquidacion() {
    return cuentaLiquidacion;
  }

  /**
   * @return the importeEfectivoFallido
   */
  public BigDecimal getImporteEfectivoFallido() {
    return importeEfectivoFallido;
  }

  /**
   * @return the corretaje
   */
  public BigDecimal getCorretaje() {
    return corretaje;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @param numeroOperacion the numeroOperacion to set
   */
  public void setNumeroOperacion(String numeroOperacion) {
    this.numeroOperacion = numeroOperacion;
  }

  /**
   * @param numeroOperacionPrevia the numeroOperacionPrevia to set
   */
  public void setNumeroOperacionPrevia(String numeroOperacionPrevia) {
    this.numeroOperacionPrevia = numeroOperacionPrevia;
  }

  /**
   * @param numeroOperacionInicial the numeroOperacionInicial to set
   */
  public void setNumeroOperacionInicial(String numeroOperacionInicial) {
    this.numeroOperacionInicial = numeroOperacionInicial;
  }

  /**
   * @param titulosFallidosOperacionIL the titulosFallidosOperacionIL to set
   */
  public void setTitulosFallidosOperacionIL(BigDecimal titulosFallidosOperacionIL) {
    this.titulosFallidosOperacionIL = titulosFallidosOperacionIL;
  }

  /**
   * @param titulosOperacionIL the titulosOperacionIL to set
   */
  public void setTitulosOperacionIL(BigDecimal titulosOperacionIL) {
    this.titulosOperacionIL = titulosOperacionIL;
  }

  /**
   * @param titulosFallidosOperacion the titulosFallidosOperacion to set
   */
  public void setTitulosFallidosOperacion(BigDecimal titulosFallidosOperacion) {
    this.titulosFallidosOperacion = titulosFallidosOperacion;
  }

  /**
   * @param efectivoOperacionIL the efectivoOperacionIL to set
   */
  public void setEfectivoOperacionIL(BigDecimal efectivoOperacionIL) {
    this.efectivoOperacionIL = efectivoOperacionIL;
  }

  /**
   * @param numeroOperacionIlEcc the numeroOperacionIlEcc to set
   */
  public void setNumeroOperacionIlEcc(String numeroOperacionIlEcc) {
    this.numeroOperacionIlEcc = numeroOperacionIlEcc;
  }

  /**
   * @param numeroOperacionDcv the numeroOperacionDcv to set
   */
  public void setNumeroOperacionDcv(String numeroOperacionDcv) {
    this.numeroOperacionDcv = numeroOperacionDcv;
  }

  /**
   * @param estadoIL the estadoIL to set
   */
  public void setEstadoIL(String estadoIL) {
    this.estadoIL = estadoIL;
  }

  /**
   * @param refEventoCorporativo the refEventoCorporativo to set
   */
  public void setRefEventoCorporativo(String refEventoCorporativo) {
    this.refEventoCorporativo = refEventoCorporativo;
  }

  /**
   * @param situacionOperacion the situacionOperacion to set
   */
  public void setSituacionOperacion(String situacionOperacion) {
    this.situacionOperacion = situacionOperacion;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param precio the precio to set
   */
  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  /**
   * @param numeroOrdenMercado the numeroOrdenMercado to set
   */
  public void setNumeroOrdenMercado(String numeroOrdenMercado) {
    this.numeroOrdenMercado = numeroOrdenMercado;
  }

  /**
   * @param numeroEjecucionMercado the numeroEjecucionMercado to set
   */
  public void setNumeroEjecucionMercado(String numeroEjecucionMercado) {
    this.numeroEjecucionMercado = numeroEjecucionMercado;
  }

  /**
   * @param holder the holder to set
   */
  public void setHolder(String holder) {
    this.holder = holder;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @param camara the camara to set
   */
  public void setCamara(String camara) {
    this.camara = camara;
  }

  /**
   * @param descripcionAlias the descripcionAlias to set
   */
  public void setDescripcionAlias(String descripcionAlias) {
    this.descripcionAlias = descripcionAlias;
  }

  /**
   * @param descripcionIsin the descripcionIsin to set
   */
  public void setDescripcionIsin(String descripcionIsin) {
    this.descripcionIsin = descripcionIsin;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * @param subCuenta the subCuenta to set
   */
  public void setSubCuenta(String subCuenta) {
    this.subCuenta = subCuenta;
  }

  /**
   * @param feejecuc the feejecuc to set
   */
  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  /**
   * @param sttlDt the sttlDt to set
   */
  public void setSttlDt(Date sttlDt) {
    this.sttlDt = sttlDt;
  }

  /**
   * @return the liquidado
   */
  public boolean isLiquidado() {
    return liquidado;
  }

  /**
   * @param liquidado the liquidado to set
   */
  public void setLiquidado(boolean liquidado) {
    this.liquidado = liquidado;
  }

  public String getDescripcionHolder() {
    return descripcionHolder;
  }

  public void setDescripcionHolder(String descripcionHolder) {
    this.descripcionHolder = descripcionHolder;
  }

  /**
   * @param cuentaLiquidacion the cuentaLiquidacion to set
   */
  public void setCuentaLiquidacion(String cuentaLiquidacion) {
    this.cuentaLiquidacion = cuentaLiquidacion;
  }

  /**
   * @param importeEfectivoFallido the importeEfectivoFallido to set
   */
  public void setImporteEfectivoFallido(BigDecimal importeEfectivoFallido) {
    this.importeEfectivoFallido = importeEfectivoFallido;
  }

  /**
   * @param corretaje the corretaje to set
   */
  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }

} // OperacionFallidoDTO
