package sibbac.business.fallidos.rest;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.dto.OperacionFallidoDTO;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * Servicio con las operaciones disponibles sobre la tabla de operaciones fallidas.
 * 
 * @author XI316153
 */
@SIBBACService
public class SIBBACServiceOperacionFallida implements SIBBACServiceBean {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  protected static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceOperacionFallida.class);

  /** Comando para el listado de los segmentos por camara. */
  private static final String CMD_LIST_OPERACIONES_FALLIDAS = "listOperacionesFallidas";

  /** Comando para el listado de los segmentos por camara. */
  private static final String CMD_CREATE_OPERACION_FALLIDA = "createOperacionFallida";

  /** Nombre del parámetro de respuesta de la operación 'listOperacionesFallidas'. */
  private static final String RESULT_LIST_OPERACION_FALLIDA = "listadoOperacionFallida";

  /** Nombre del parámetro de respuesta de las operaciones. */
  private static final String RESULTADO = "resultado";

  /** Parámetro de respuesta que indica que una operación se ha realizado correctamente. */
  private static final String RESULTADO_SUCCESS = "SUCCESS";

  /** Parámetro de respuesta que indica que una operación se ha realizado correctamente. */
  private static final String RESULTADO_FAIL = "FAIL";

  /** Nombre de la clave que se envia en el mapa de resultados con el mensaje del error producido. */
  private static final String ERROR_MESSAGE = "error_message";

  private static final String PARAM_OPERACION_FALLIDA_NUMEROOPERACIONILECC = "NumeroOperacionIlEcc";

  private static final String PARAM_OPERACION_FALLIDA_ALIAS = "Alias";
  private static final String PARAM_OPERACION_FALLIDA_SUBCUENTA = "SubCuenta";
  private static final String PARAM_OPERACION_FALLIDA_DESCRIPCIONALIAS = "DescripcionAlias";
  private static final String PARAM_OPERACION_FALLIDA_ISIN = "Isin";
  private static final String PARAM_OPERACION_FALLIDA_DESCRIPCIONISIN = "DescripcionIsin";

  private static final String PARAM_OPERACION_FALLIDA_TITULOSFALLIDOSOPERACIONIL = "TitulosFallidosOperacionIL";
  private static final String PARAM_OPERACION_FALLIDA_TITULOSOPERACIONIL = "TitulosOperacionIL";
  private static final String PARAM_OPERACION_FALLIDA_TITULOSFALLIDOSOPERACION = "TitulosFallidosOperacion";

  private static final String PARAM_OPERACION_FALLIDA_FEEJECUC = "Feejecuc";
  private static final String PARAM_OPERACION_FALLIDA_STTLDT = "Sttldt";

  private static final String PARAM_OPERACION_FALLIDA_CAMARA = "Camara";
  private static final String PARAM_OPERACION_FALLIDA_CUENTA_LIQUIDACION = "cuentaLiquidacion";

  private static final String PARAM_OPERACION_FALLIDA_IMPORTEEFECTIVOFALLIDO = "importeEfectivoFallido";
  private static final String PARAM_OPERACION_FALLIDA_CORRETAJE = "corretaje";

  // private static final String PARAM_OPERACION_FALLIDA_ESTADOIL = "EstadoIL";
  // private static final String PARAM_OPERACION_FALLIDA_SITUACIONOPERACION = "SituacionOperacion";
  // private static final String PARAM_OPERACION_FALLIDA_SENTIDO = "Sentido";

  //

  private static final String PARAM_OPERACION_FALLIDA_NUMEROOPERACION = "NumeroOperacion";
  private static final String PARAM_OPERACION_FALLIDA_NUMEROOPERACIONPREVIA = "NumeroOperacionPrevia";
  private static final String PARAM_OPERACION_FALLIDA_NUMEROOPERACIONINICIAL = "NumeroOperacionInicial";
  private static final String PARAM_OPERACION_FALLIDA_EFECTIVOOPERACIONIL = "EfectivoOperacionIL";
  private static final String PARAM_OPERACION_FALLIDA_NUMEROOPERACIONDCV = "NumeroOperacionDcv";
  private static final String PARAM_OPERACION_FALLIDA_REFEVENTOCORPORATIVO = "RefEventoCorporativo";
  private static final String PARAM_OPERACION_FALLIDA_PRECIO = "Precio";
  private static final String PARAM_OPERACION_FALLIDA_NUMEROORDENMERCADO = "NumeroOrdenMercado";
  private static final String PARAM_OPERACION_FALLIDA_NUMEROEJECUCIONMERCADO = "NumeroEjecuciomMercado";
  private static final String PARAM_OPERACION_FALLIDA_HOLDER = "Holder";
  private static final String PARAM_OPERACION_FALLIDA_SEGMENTO = "Segmento";

  /** Formateardor de números. */
  private static final NumberFormat nF = new DecimalFormat("#0.00", new DecimalFormatSymbols(new Locale("es", "ES")));

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

//  /** Objeto para el acceso a la tabla <code>TMCT0_OPERACION_FALLIDO</code>. */
//  @Autowired
//  private OperacionFallidoDao operacionFallidoDao;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public SIBBACServiceOperacionFallida() {
  } // SIBBACServiceOperacionFallida

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
   */
  @Override
  public WebResponse process(WebRequest webRequest) {
    LOG.debug("[SIBBACServiceOperacionFallida :: process] Processing a web request ...");

    final String command = webRequest.getAction();
    LOG.debug("[SIBBACServiceOperacionFallida :: process] Command: [{}]", command);

    final WebResponse webResponse = new WebResponse();

    final List<Map<String, String>> params = webRequest.getParams();

    try {
      switch (command) {
        case CMD_LIST_OPERACIONES_FALLIDAS:
          List<OperacionFallidoDTO> oflDTO = new ArrayList<OperacionFallidoDTO>();

//          List<OperacionFallido> listaOperacionFallido = null;
//          listaOperacionFallido = operacionFallidoDao.findByLiquidado(false);
//
//          for (OperacionFallido of : listaOperacionFallido) {
//            oflDTO.add(OperacionFallidoHelper.OperacionFallidoToOperacionFallidoDTO(of));
//          } // for

          final Map<String, List<OperacionFallidoDTO>> valoresOperacionFallido = new HashMap<String, List<OperacionFallidoDTO>>();
          valoresOperacionFallido.put(RESULT_LIST_OPERACION_FALLIDA, oflDTO);

          webResponse.setResultados(valoresOperacionFallido);
          break;
        case CMD_CREATE_OPERACION_FALLIDA:
          webResponse.setError(command + ". Operación no permitida");
          // if (CollectionUtils.isNotEmpty(params)) {
          // operacionFallidoDao.save(parseaOperacionFallidoParam(params.get(0)));
          //
          // final Map<String, String> resultados = new HashMap<String, String>();
          // resultados.put(RESULTADO, RESULTADO_SUCCESS);
          // webResponse.setResultados(resultados);
          // } else {
          // webResponse.setError(command + " needs the OperacionFallido filter and OperacionFallidoDTO param.");
          // } // else
          break;
        default:
          webResponse.setError(command + " is not a valid action. Valid actions are: " + this.getAvailableCommands());
      } // switch
    } catch (Exception e) {
      LOG.debug("[SIBBACServiceOperacionFallida :: process] Error: " + e);
      LOG.debug("[SIBBACServiceOperacionFallida :: process] Error Message: " + e.getMessage());

      Map<String, Object> resultado = new HashMap<String, Object>();

      resultado.put(RESULTADO, RESULTADO_FAIL);
      resultado.put(ERROR_MESSAGE, e.getMessage());

      webResponse.setResultados(resultado);
    } // catch

    return webResponse;
  } // process

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_LIST_OPERACIONES_FALLIDAS);
    commands.add(CMD_CREATE_OPERACION_FALLIDA);
    return commands;
  } // getAvailableCommands

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  @Override
  public List<String> getFields() {
    return null;
  } // getFields

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

//  /**
//   * Parsea los parámetros recibidos para la operación de alta de operación fallida.
//   * 
//   * @param param Parámetros.
//   * @return Operación fallida a crear.
//   * @throws ParseException Si ocurre algún error al procesar los parámetros recibidos.
//   */
//  private OperacionFallido parseaOperacionFallidoParam(Map<String, String> param) throws ParseException {
//    LOG.debug("[SIBBACServiceOperacionFallida :: parseaOperacionFallidoParam] Iniciando parseo ...");
//
//    OperacionFallido of = new OperacionFallido();
//
//    of.setNumeroOperacionIlEcc(param.get(PARAM_OPERACION_FALLIDA_NUMEROOPERACIONILECC));
//    of.setAliasCliente(param.get(PARAM_OPERACION_FALLIDA_ALIAS));
//    of.setSubCuenta(param.get(PARAM_OPERACION_FALLIDA_SUBCUENTA) != null ? param.get(PARAM_OPERACION_FALLIDA_SUBCUENTA)
//                                                                        : "");
//    of.setDescripcionAlias(param.get(PARAM_OPERACION_FALLIDA_DESCRIPCIONALIAS));
//    of.setIsin(param.get(PARAM_OPERACION_FALLIDA_ISIN));
//    of.setDescripcionIsin(param.get(PARAM_OPERACION_FALLIDA_DESCRIPCIONISIN));
//    of.setTitulosFallidosOperacion(NumberUtils.createBigDecimal(param.get(PARAM_OPERACION_FALLIDA_TITULOSFALLIDOSOPERACION)));
//    of.setTitulosFallidosOperacionIL(NumberUtils.createBigDecimal(param.get(PARAM_OPERACION_FALLIDA_TITULOSFALLIDOSOPERACIONIL)));
//    of.setTitulosOperacionIL(NumberUtils.createBigDecimal(param.get(PARAM_OPERACION_FALLIDA_TITULOSOPERACIONIL)));
//    of.setFeejecuc(FormatDataUtils.convertStringToDate(param.get(PARAM_OPERACION_FALLIDA_FEEJECUC), "yyyyMMdd"));
//    of.setSttlDt(FormatDataUtils.convertStringToDate(param.get(PARAM_OPERACION_FALLIDA_STTLDT), "yyyyMMdd"));
//    of.setCamara(param.get(PARAM_OPERACION_FALLIDA_CAMARA));
//    of.setCuentaLiquidacion(param.get(PARAM_OPERACION_FALLIDA_CUENTA_LIQUIDACION));
//    of.setImporteEfectivoFallido(NumberUtils.createBigDecimal(nF.parse(param.get(PARAM_OPERACION_FALLIDA_IMPORTEEFECTIVOFALLIDO))
//                                                                .toString()));
//    of.setCorretaje(NumberUtils.createBigDecimal(nF.parse(param.get(PARAM_OPERACION_FALLIDA_CORRETAJE)).toString()));
//
//    // of.setEstadoIL(param.get(PARAM_OPERACION_FALLIDA_ESTADOIL));
//    of.setEstadoIL("F");
//
//    // of.setSituacionOperacion(param.get(PARAM_OPERACION_FALLIDA_SITUACIONOPERACION));
//    of.setSituacionOperacion("F");
//
//    // of.setSentido(param.get(PARAM_OPERACION_FALLIDA_SENTIDO));
//    of.setSentido("1"); // Una operación fallida siempre es una venta
//
//    of.setLiquidado(false);
//    of.setContabilizado(false);
//
//    //
//
//    of.setNumeroOperacion(param.get(PARAM_OPERACION_FALLIDA_NUMEROOPERACION));
//    of.setNumeroOperacionPrevia(param.get(PARAM_OPERACION_FALLIDA_NUMEROOPERACIONPREVIA));
//    of.setNumeroOperacionInicial(param.get(PARAM_OPERACION_FALLIDA_NUMEROOPERACIONINICIAL));
//    of.setEfectivoOperacionIL(NumberUtils.createBigDecimal(param.get(PARAM_OPERACION_FALLIDA_EFECTIVOOPERACIONIL)));
//    of.setNumeroOperacionDcv(param.get(PARAM_OPERACION_FALLIDA_NUMEROOPERACIONDCV));
//    of.setRefEventoCorporativo(param.get(PARAM_OPERACION_FALLIDA_REFEVENTOCORPORATIVO));
//    of.setPrecio(NumberUtils.createBigDecimal(param.get(PARAM_OPERACION_FALLIDA_PRECIO)));
//    of.setNumeroOrdenMercado(param.get(PARAM_OPERACION_FALLIDA_NUMEROORDENMERCADO));
//    of.setNumeroEjecucionMercado(param.get(PARAM_OPERACION_FALLIDA_NUMEROEJECUCIONMERCADO));
//    of.setHolder(param.get(PARAM_OPERACION_FALLIDA_HOLDER));
//    of.setSegmento(param.get(PARAM_OPERACION_FALLIDA_SEGMENTO));
//
//    LOG.debug("[SIBBACServiceOperacionFallida :: parseaOperacionFallidoParam] Parseo finalizado ...");
//    LOG.debug("[SIBBACServiceOperacionFallida :: parseaOperacionFallidoParam] Operacion fallida: " + of.toString());
//
//    return of;
//  } // parseaOperacionFallidoParamam

} // SIBBACServiceOperacionFallida
