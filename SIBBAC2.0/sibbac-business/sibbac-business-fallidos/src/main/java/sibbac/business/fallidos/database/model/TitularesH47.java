package sibbac.business.fallidos.database.model;


import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import sibbac.business.fallidos.writers.H47TitularRecordBean;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.RecordBean.TipoDocumento;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.database.model.Fallido }.
 * Entity: "Fallido".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.FALLIDOS.H47_TITULARES )
@Entity
@Component
public class TitularesH47 extends ATable< TitularesH47 > implements Serializable {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
	@Column( name = "TPSOCIED", length = 1 )
	private Character		tpsocied;
	@Column( name = "TPIDENTI", length = 1 )
	private Character		tpidenti;
	@Column( name = "IDENTIFICACION", length = 40 )
	private String			identificacion;
	@Column( name = "NOMBRE", length = 60 )
	private String			nombre;
	@Column( name = "APELLIDO1", length = 40 )
	private String			apellido1;
	@Column( name = "APELLIDO2", length = 40 )
	private String			apellido2;
	@Column( name = "REP_TPIDENTI", length = 1 )
	private Character		repTpidenti;
	@Column( name = "REP_IDENTIFICACION", length = 40 )
	private String			repIdentificacion;
	@Column( name = "REP_NOMBRE", length = 60 )
	private String			repNombre;
	@Column( name = "REP_APELLIDO1", length = 40 )
	private String			repApellido1;
	@Column( name = "REP_APELLIDO2", length = 40 )
	private String			repApellido2;
	@Column( name = "CANTIDADVALORES", precision = 17, scale = 5 )
	private BigDecimal		cantidadValores;
	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "IDOPERACION", referencedColumnName = "ID", nullable = false )
	private OperacionH47	operacionH47;

	public TitularesH47() {
		super();
	}

	public TitularesH47( Character tpsocied, Character tpidenti, String identificacion, String nombre, String apellido1, String apellido2,
			Character repTpsocied, Character repTpidenti, String repIdentificacion, String repNombre, String repApellido1,
			String repApellido2, BigDecimal cantidadValores, OperacionH47 operacionH47 ) {
		super();
		this.tpsocied = tpsocied;
		this.tpidenti = tpidenti;
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.repTpidenti = repTpidenti;
		this.repIdentificacion = repIdentificacion;
		this.repNombre = repNombre;
		this.repApellido1 = repApellido1;
		this.repApellido2 = repApellido2;
		this.cantidadValores = cantidadValores;
		this.operacionH47 = operacionH47;
	}

	public TitularesH47( Tmct0afi afi, Tmct0afi afiRepresentante ) {
		super();
		this.tpsocied = afi.getTpsocied();
		this.tpidenti = afi.getTpidenti();
		if (TipoDocumento.BIC.getTipoDocumento().equals(afi.getTpidenti())) {
			this.identificacion = afi.getCdholder();
		} else {
			this.identificacion = afi.getCdholder();
		}
		if ( new Character( 'F' ).equals( afi.getTpsocied() ) ) {
			this.nombre = afi.getNbclient();
			this.apellido1 = afi.getNbclient1();
			this.apellido2 = afi.getNbclient2();
		} else {
			this.nombre = afi.getNbclient1();
			if(StringUtils.isNotEmpty( afi.getNbclient2() )){
				this.nombre = this.nombre + afi.getNbclient2();
				this.apellido1 = "";
				this.apellido2 = "";
			}
		}
		if ( afiRepresentante != null ) {
			if (TipoDocumento.BIC.getTipoDocumento().equals(afi.getTpidenti())) {
				this.repIdentificacion = afiRepresentante.getCdholder();
			} else {
				this.repIdentificacion = afiRepresentante.getCdholder();
			}
			if ( new Character( 'F' ).equals( afiRepresentante.getTpsocied() ) ) {
				this.repNombre = afiRepresentante.getNbclient();
				this.repApellido1 = afiRepresentante.getNbclient1();
				this.repApellido2 = afiRepresentante.getNbclient2();
			} else {
				this.repNombre = afiRepresentante.getNbclient1();
				if(StringUtils.isNotEmpty( afi.getNbclient2() )){
					this.repNombre = this.repNombre + afi.getNbclient2();
					this.repApellido1 = "";
					this.repApellido2 = "";
				}
			}
			this.repTpidenti = afiRepresentante.getTpidenti();
		}
		this.cantidadValores = null;
	}

	public TitularesH47( H47TitularRecordBean bean ) {
		super();
		this.tpsocied = bean.getIcuclient().charAt( 0 );
		this.tpidenti = bean.getIcclient().charAt( 0 );
		this.identificacion = bean.getIcdclient();
		this.nombre = bean.getIcdnombr();
		this.apellido1 = bean.getIcdapel1();
		this.apellido2 = bean.getIcdapel2();
		this.repTpidenti = bean.getIcutipor().charAt( 0 );
		this.repIdentificacion = bean.getIcdident();
		this.repNombre = bean.getIcdnombre();
		this.repApellido1 = bean.getIcdap1re();
		this.repApellido2 = bean.getIcdap2re();
		this.cantidadValores = StringHelper.convertNum( bean.getIcvcanti(), 17, 5 );
	}

	public Character getTpsocied() {
		return tpsocied;
	}

	public void setTpsocied( Character tpsocied ) {
		this.tpsocied = tpsocied;
	}

	public Character getTpidenti() {
		return tpidenti;
	}

	public void setTpidenti( Character tpidenti ) {
		this.tpidenti = tpidenti;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion( String identificacion ) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1( String apellido1 ) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2( String apellido2 ) {
		this.apellido2 = apellido2;
	}

	public Character getRepTpidenti() {
		return repTpidenti;
	}

	public void setRepTpidenti( Character repTpidenti ) {
		this.repTpidenti = repTpidenti;
	}

	public String getRepIdentificacion() {
		return repIdentificacion;
	}

	public void setRepIdentificacion( String repIdentificacion ) {
		this.repIdentificacion = repIdentificacion;
	}

	public String getRepNombre() {
		return repNombre;
	}

	public void setRepNombre( String repNombre ) {
		this.repNombre = repNombre;
	}

	public String getRepApellido1() {
		return repApellido1;
	}

	public void setRepApellido1( String repApellido1 ) {
		this.repApellido1 = repApellido1;
	}

	public String getRepApellido2() {
		return repApellido2;
	}

	public void setRepApellido2( String repApellido2 ) {
		this.repApellido2 = repApellido2;
	}

	public BigDecimal getCantidadValores() {
		return cantidadValores;
	}

	public void setCantidadValores( BigDecimal cantidadValores ) {
		this.cantidadValores = cantidadValores;
	}

	public OperacionH47 getOperacionH47() {
		return operacionH47;
	}

	public void setOperacionH47( OperacionH47 operacionH47 ) {
		this.operacionH47 = operacionH47;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer( "H47_TITULARES" );
		// sb.append( " [ISIN==" + this.isin + "]" );
		return sb.toString();
	}

}
