package sibbac.business.fallidos.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.Tmct0CnmvDesglosesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvTitularesDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.dto.OperacionTrDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0desgloseCamaraDao;
import sibbac.business.wrappers.database.dao.Tmct0ejeDao;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.common.SIBBACBusinessException;

@Service
public class ProcesaOperacionTrBo {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaOperacionTrBo.class);

  @Value("${cdestadoasig.tr.creation}")
  private Integer iCdEstadoAsignacion;

  @Value("${cdestadoenvio.tr.creation}")
  private Integer iCdEstadoEnvio;

  @Autowired
  Tmct0CnmvDesglosesDao tmct0CnmvDesglosesDao;

  @Autowired
  Tmct0desgloseCamaraDao tmct0desglosecamaraDao;

  @Autowired
  Tmct0estadoDao tmct0estadoDao;

  @Autowired
  Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;

  @Autowired
  Tmct0CnmvTitularesDao tmct0CnmvTitularesDao;

  @Autowired
  Tmct0desgloseCamaraDao tmct0desgloseCamaraDao;

  @Autowired
  Tmct0afiDao tmct0afiDao;

  @Autowired
  Tmct0ejeDao tmct0ejeDao;

  @Autowired
  OperacionesTrBo operacionesRtBo;

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void trataOperacionRtDTO(OperacionTrDTO operacionRtDTO) throws SIBBACBusinessException {
    // char sentidoCorregido;

    // Se obtiene el estado PTE de enviar
    Tmct0estado estadoPte = tmct0estadoDao.findOne(EstadosEnumerados.H47.PTE_ENVIAR.getId());

    // A partir del DTO se obtiene la ejecucion y desglose si ya existieran.
    // Se busca la ejecución con estado = 'A' y los siguientes valores
    Tmct0CnmvEjecuciones entEjecucion = tmct0CnmvEjecucionesDao.findByIdEjecucion(operacionRtDTO.getNurefexemkt(),
        operacionRtDTO.getFeejecuc(), operacionRtDTO.getCd_plataforma_negociacion(), operacionRtDTO.getSentido());

    // Si existe ejecución se busca el desglose tambien tiene que estar en
    // estado A
    BigInteger idDesglose = null;
    if (entEjecucion != null) {
      idDesglose = tmct0CnmvDesglosesDao.findIdByDatosAlc(operacionRtDTO.getNuorden(), operacionRtDTO.getNbooking(),
          operacionRtDTO.getNucnfclt(), operacionRtDTO.getNucnfliq(), operacionRtDTO.getIddesglosecamara(),
          entEjecucion.getId());
    }

    if (operacionRtDTO.getCdestadoasig() >= iCdEstadoAsignacion) { // Se procesa
                                                                   // el alta
      // ******************* ALTA DE UN DESGLOSE *********************

      Boolean bAltaEjecucion = false; // Para ver si la ejecucion es nueva o ya
                                      // existe.

      // 1- Comprobamos si el desglose existe.
      if (idDesglose == null) {
        // Desglose NO existe, se procede al alta del desglose, junto con su
        // ejecución si no existiera ya y sus
        // titulares.
        if (entEjecucion == null) {
          entEjecucion = creaTmct0CnmvEjecucionesWithDto(operacionRtDTO);
          bAltaEjecucion = true;

        }

        Tmct0CnmvDesgloses entDesglose = creaTmct0CnmvDesglosesWithDto(operacionRtDTO);
        entDesglose.setEjecucion(entEjecucion);
        // Se actualiza la referencia de operacion
        entDesglose.setRefOperacion(entDesglose.getNucnfclt().trim() + entDesglose.getNucnfliq()
            + entEjecucion.getIdEjecucion());

        // Se dan de alta los titulares activos.
        // MFG 02/10/2017 Cambio con respecto a la version anterior, ahora los
        // titulares no solo se obtienen de la AFI.
        // Se crea un metodo nuevo que crea los titulares y los devuelve en una
        // lista.
        List<Tmct0CnmvTitulares> listaTitulares = obtenerTitularesDesglose(operacionRtDTO.getNuorden(),
            operacionRtDTO.getNbooking(), operacionRtDTO.getNucnfclt(), operacionRtDTO.getNucnfliq(), entDesglose,
            "AFI");
        // Solo se dan de alta el desglose si tiene algun titular.
        if (listaTitulares.size() > 0) {

          entDesglose.setTmct0CnmvTitularesList(listaTitulares);

          // Se actualizan los titulos.
          entEjecucion.setTitulosDesgloses(entEjecucion.getTitulosDesgloses().add(entDesglose.getTitulosDesgloses()));

          // Si el numero de titulos de la ejecucción y de sus desgloses es
          // igual se pone a pendiente de enviar
          // MFG 09/10/2017 Se crean todas las ejecuciones como pendientes, ya
          // no se crean con estado nulo y solo se ponen a pendiente cuando el
          // numero de titulos de los desgloses
          // sea igual al de la ejecucion.
          entEjecucion.setCdEstadoEnvio(estadoPte);

          // Se escribe un warning si los titulos de los desgloses son mayores a
          // los de la ejecucion
          int diferencia = entEjecucion.getTitulosEjecucion().compareTo(entEjecucion.getTitulosDesgloses());
          if (diferencia < 0) {
            LOG.warn(" - Numero de titulos desgloses superior al de la ejecución al procesar el alta de un desglose  con los siguientes datos: "
                + " Nuorden - "
                + operacionRtDTO.getNuorden()
                + " - Nbooking - "
                + operacionRtDTO.getNbooking()
                + " - Nucnfclt - "
                + operacionRtDTO.getNucnfclt()
                + " - Nucnfliq -  "
                + operacionRtDTO.getNucnfliq()
                + " - Iddesglosecamara  "
                + operacionRtDTO.getIddesglosecamara()
                + " - Ejecucion  "
                + operacionRtDTO.getNurefexemkt());
          }
          // Guardado final de la ejecucion como PTE de enviar
          tmct0CnmvEjecucionesDao.save(entEjecucion);

          // Se guardan datos del desglose
          entDesglose.setCdEstadoEnvio(estadoPte.getIdestado());
          tmct0CnmvDesglosesDao.save(entDesglose);

          // Se actualiza el desgloseCamara Asociado como tratado.
          ActualizaDesgloseAProcesado(entDesglose);

          if (bAltaEjecucion) { // Se incrementa el contador de altas
            this.operacionesRtBo.getTmct0CnmvLog().setContadorAltas(
                this.operacionesRtBo.getTmct0CnmvLog().getContadorAltas().add(BigInteger.ONE));
          }

        }
        else { // El desglose no tiene titulares, se crea un error.
          throw new SIBBACBusinessException(" - El desglose no tiene titulares ");
        }

      }
      else {

        // Desglose ya existe
        Tmct0CnmvDesgloses entDesglose = tmct0CnmvDesglosesDao.findOne(idDesglose);

        // comprobamos cambio de titularidad
        if (operacionRtDTO.getEstadoCNMV() != null && operacionRtDTO.getEstadoCNMV().trim().equals("2")) {

          // Se obtienen los titulares del desglose actual
          List<String> listIdentificacionesOld = new ArrayList<String>();
          List<Tmct0CnmvTitulares> listTitularesMTF = entDesglose.getTmct0CnmvTitularesList();
          for (Tmct0CnmvTitulares tmct0CnmvTitulares : listTitularesMTF) {
            if (tmct0CnmvTitulares.getEstadoTitular() == 'A') { // solo los que
                                                                // estan en
                                                                // estado A
              listIdentificacionesOld.add(tmct0CnmvTitulares.getIdentificacion().trim());
            }

          }

          // Se obtinen los titulares del desglose resgistrado
          List<String> listIdentificadoresNew = new ArrayList<String>();

          // MFG 02/10/2017 Cambio con respecto a la version anterior, ahora los
          // titulares no solo se obtienen de la AFI.
          // Se crea un metodo nuevo que crea los titulares y los devuelve en
          // una lista.
          List<Tmct0CnmvTitulares> listaTitulares = obtenerTitularesDesglose(operacionRtDTO.getNuorden(),
              operacionRtDTO.getNbooking(), operacionRtDTO.getNucnfclt(), operacionRtDTO.getNucnfliq(), entDesglose,
              "AFI");

          // Solo se modifican los titulares el desglose tiene algun titular.
          if (listaTitulares.size() > 0) {

            for (Tmct0CnmvTitulares tmct0Titular : listaTitulares) {
              listIdentificadoresNew.add(tmct0Titular.getIdentificacion().trim());
            }

            // MFG 09/10/2016 La comprobacion se mira si el desglose ha sido o
            // no enviado
            if (entDesglose.getCdEstadoEnvio() == null || entDesglose.getCdEstadoEnvio() < iCdEstadoEnvio) {
              // DESGLOSE NO ENVIADO
              // Se marcan como estado Enviado = 'X' los titulares que ya no
              // estan y se crean los nuevos.
              for (Tmct0CnmvTitulares tmct0CnmvTitular : listTitularesMTF) {
                if (!listIdentificadoresNew.contains(tmct0CnmvTitular.getIdentificacion().trim())) {
                  // Se actualiza el estado
                  tmct0CnmvTitular.setEstadoTitular('X');
                }
              }
              // Se crean los nuevos
              for (Tmct0CnmvTitulares tmct0Titular : listaTitulares) {
                if (!listIdentificacionesOld.contains(tmct0Titular.getIdentificacion().trim())) {
                  entDesglose.getTmct0CnmvTitularesList().add(tmct0Titular);
                }
              }

              tmct0CnmvDesglosesDao.save(entDesglose);

            }
            else {
              // DESGLOSE ENVIADO
              // MFG 10/10/2017 Cambio grande, ya no se crean clones de
              // ejecucion desgloses y titulares, ahora todos los desgloses son
              // altas y bajas de la misma ejecución.

              // Se pone la ejecución a pendiente de enviar
              entEjecucion.setCdEstadoEnvio(estadoPte);

              // Se de de baja y se pone pendiente de enviar el desglose actual
              // con los titulares antiguos
              entDesglose.setEstadoDesglose('B');
              entDesglose.setCdEstadoEnvio(estadoPte.getIdestado());

              // Se incrementa el contador de bajas
              this.operacionesRtBo.getTmct0CnmvLog().setContadorBajas(
                  this.operacionesRtBo.getTmct0CnmvLog().getContadorBajas().add(BigInteger.ONE));

              // Se crea un desglose nuevo

              Tmct0CnmvDesgloses desgloseNuevo = creaCnmvDesgloseClon(entDesglose);
              desgloseNuevo.setEstadoDesglose('A');
              desgloseNuevo.setCdEstadoEnvio(estadoPte.getIdestado());

              // se asignan los nuevos titulares
              // Se cambia el desglose al que estan asociados
              for (Tmct0CnmvTitulares tmct0CnmvTitulares : listaTitulares) {
                tmct0CnmvTitulares.setDesglose(desgloseNuevo);
              }
              desgloseNuevo.setTmct0CnmvTitularesList(listaTitulares);

              // Se asigna la ejecucion
              desgloseNuevo.setEjecucion(entEjecucion);

              // Se incrementa el contador de altas.
              this.operacionesRtBo.getTmct0CnmvLog().setContadorAltas(
                  this.operacionesRtBo.getTmct0CnmvLog().getContadorAltas().add(BigInteger.ONE));

              // Se guarda la ejecucion como pendiente y los dos desgloses el de
              // baja y el nuevo de alta
              tmct0CnmvEjecucionesDao.save(entEjecucion);

              tmct0CnmvDesglosesDao.save(entDesglose);

              tmct0CnmvDesglosesDao.save(desgloseNuevo);

            }

          }
          else { // SE ha marcado un cambio de titulares al desblose y ahora no
                 // tiene ningun tulares

            throw new SIBBACBusinessException(
                " - El desglose tiene marcado un cambio de titulares y ahora no tiene ningún titular ");
          }
        }

        // Se actualiza el desgloseCamara Asociado como tratado.
        ActualizaDesgloseAProcesado(entDesglose);

      }

    }
    else {// Se procesa una baja
      // ******************* BAJA DE UN DESGLOSE *********************

      BigInteger idDesgloseBajaPte = null;

      if (entEjecucion != null) {
        idDesgloseBajaPte = tmct0CnmvDesglosesDao.findIdByDatosAlcDeBajaPte(operacionRtDTO.getNuorden(),
            operacionRtDTO.getNbooking(), operacionRtDTO.getNucnfclt(), operacionRtDTO.getNucnfliq(),
            operacionRtDTO.getIddesglosecamara(), entEjecucion.getId(), estadoPte.getIdestado());
      }

      // Si existe la ejecución y el desglose y este no esta de baja y Pte de
      // enviar.
      if ((entEjecucion != null) && (idDesglose != null) && (idDesgloseBajaPte == null)) {

        // Se obtiene el desglose.
        Tmct0CnmvDesgloses entDesglose = tmct0CnmvDesglosesDao.findOne(idDesglose);

        // Su tratamiento depende si el desglose ha sido o no enviado.
        if (entDesglose.getCdEstadoEnvio() == null || entDesglose.getCdEstadoEnvio() < iCdEstadoEnvio) {
          // DESGLOSE NO ENVIADO
          // Se da de baja el desglose y todos sus titulares pasando su estado
          // al valor X
          entDesglose.setEstadoDesglose('X');

          for (Tmct0CnmvTitulares tmct0CnmvTitulares : entDesglose.getTmct0CnmvTitularesList()) {
            tmct0CnmvTitulares.setEstadoTitular('X');
          }

          // Se restan los titulos de ejecucion
          entEjecucion.setTitulosDesgloses(entEjecucion.getTitulosDesgloses().subtract(
              entDesglose.getTitulosDesgloses()));

          if (entEjecucion.getTitulosDesgloses().compareTo(BigDecimal.ZERO) == 0) {
            entEjecucion.setEstadoOperacion('X');
          }

          // se graban los cambios
          tmct0CnmvEjecucionesDao.save(entEjecucion);
          tmct0CnmvDesglosesDao.save(entDesglose);

          // Se actualiza el desgloseCamara Asociado como tratado.
          ActualizaDesgloseAProcesado(entDesglose);

        }
        else {
          // DESGLOSE ENVIADO
          // Se da de baja el desglose Y se pone a pte de enviar.
          entDesglose.setEstadoDesglose('B');
          entDesglose.setCdEstadoEnvio(estadoPte.getIdestado());

          // Se restan los titulos de ejecucion y se pone la ejecución Pte de
          // enviar.
          entEjecucion.setTitulosDesgloses(entEjecucion.getTitulosDesgloses().subtract(
              entDesglose.getTitulosDesgloses()));
          entEjecucion.setCdEstadoEnvio(estadoPte);
          tmct0CnmvEjecucionesDao.save(entEjecucion);
          tmct0CnmvDesglosesDao.save(entDesglose);

          // Se actualiza el desgloseCamara Asociado como tratado.
          ActualizaDesgloseAProcesado(entDesglose);

          this.operacionesRtBo.getTmct0CnmvLog().setContadorBajas(
              this.operacionesRtBo.getTmct0CnmvLog().getContadorBajas().add(BigInteger.ONE));

        }
      }
    }
  }

  private Tmct0CnmvEjecuciones creaCnmvEjecucionClon(Tmct0CnmvEjecuciones entEjecucionOld) {

    Tmct0CnmvEjecuciones entEjecucionNueva = new Tmct0CnmvEjecuciones();

    entEjecucionNueva.setAuditDate(new Date());
    entEjecucionNueva.setAuditUser(entEjecucionOld.getAuditUser());
    entEjecucionNueva.setIdEjecucion(entEjecucionOld.getIdEjecucion());
    entEjecucionNueva.setfEjecucion(entEjecucionOld.getfEjecucion());
    entEjecucionNueva.sethEjecucion(entEjecucionOld.gethEjecucion());
    entEjecucionNueva.setfValor(entEjecucionOld.getfValor());
    entEjecucionNueva.setSentido(entEjecucionOld.getSentido(), false);
    entEjecucionNueva.setCapacidadNegociacion(entEjecucionOld.getCapacidadNegociacion()); // En
                                                                                          // la
                                                                                          // creación
                                                                                          // no
                                                                                          // se
                                                                                          // puede
                                                                                          // determinar
    entEjecucionNueva.setIdInstrumento(entEjecucionOld.getIdInstrumento());
    entEjecucionNueva.setTitulosEjecucion(entEjecucionOld.getTitulosEjecucion());
    entEjecucionNueva.setTitulosDesgloses(entEjecucionOld.getTitulosDesgloses());
    entEjecucionNueva.setPrecio(entEjecucionOld.getPrecio());
    entEjecucionNueva.calculaEfectivo();
    entEjecucionNueva.setDivisa(entEjecucionOld.getDivisa());
    entEjecucionNueva.setSistemaNegociacion(entEjecucionOld.getSistemaNegociacion());
    entEjecucionNueva.setIndSistemaNegociacion(entEjecucionOld.getIndSistemaNegociacion());
    entEjecucionNueva.setEstadoOperacion(entEjecucionOld.getEstadoOperacion());
    entEjecucionNueva.setCdEstadoEnvio(entEjecucionOld.getCdEstadoEnvio());
    return entEjecucionNueva;

  }

  private Tmct0CnmvEjecuciones creaTmct0CnmvEjecucionesWithDto(OperacionTrDTO operacionRtDTO) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    Tmct0CnmvEjecuciones entEjecucionNueva = new Tmct0CnmvEjecuciones();
    entEjecucionNueva.setAuditDate(new Date());
    entEjecucionNueva.setAuditUser("process");
    entEjecucionNueva.setIdEjecucion(operacionRtDTO.getNurefexemkt());
    entEjecucionNueva.setfEjecucion(operacionRtDTO.getFeejecuc());
    entEjecucionNueva.sethEjecucion(operacionRtDTO.getHoejecuc());
    entEjecucionNueva.setfValor(operacionRtDTO.getFeliquidacion());
    entEjecucionNueva.setSentido(operacionRtDTO.getSentido(), false);
    entEjecucionNueva.setCapacidadNegociacion(null); // En la creación no se
                                                     // puede determinar
    entEjecucionNueva.setIdInstrumento(operacionRtDTO.getIsin());
    entEjecucionNueva.setTitulosEjecucion(tmct0ejeDao.getTitulosEjecucion(operacionRtDTO.getNuorden(),
        operacionRtDTO.getNbooking(), operacionRtDTO.getNurefexemkt()));
    entEjecucionNueva.setTitulosDesgloses(new BigDecimal(0));
    entEjecucionNueva.setPrecio(operacionRtDTO.getImprecio());
    entEjecucionNueva.calculaEfectivo();
    entEjecucionNueva.setDivisa(operacionRtDTO.getCddivisa());
    entEjecucionNueva.setSistemaNegociacion(operacionRtDTO.getCd_plataforma_negociacion());
    entEjecucionNueva.setIndSistemaNegociacion('M');
    entEjecucionNueva.setEstadoOperacion('A');
    entEjecucionNueva.setCdEstadoEnvio(null);
    entEjecucionNueva.setRefOperacion("PTE-Definir"); // Pendiente de definir,
                                                      // tiene que tener un
                                                      // valor

    // TODO campos que faltan por definir.
    entEjecucionNueva.setIdCamara(new BigInteger("0"));
    entEjecucionNueva.setReportable("");
    if (operacionRtDTO.getCodigoLei() == null) {
      entEjecucionNueva.setIdEntContrapartida("");
    }
    else {
      entEjecucionNueva.setIdEntContrapartida(operacionRtDTO.getCodigoLei());
    }
    entEjecucionNueva.setTipoIdEntContrapartida("");
    entEjecucionNueva.setTipoCantidad("");
    entEjecucionNueva.setTipoPrecio("");
    entEjecucionNueva.setTipoIdEjecutor("");
    if (operacionRtDTO.getFechaHoraEjec() != null) {
      entEjecucionNueva.setFechaHoraEjec(sdf.format(operacionRtDTO.getFechaHoraEjec()));
    }

    return entEjecucionNueva;
  }

  private Tmct0CnmvDesgloses creaCnmvDesgloseClon(Tmct0CnmvDesgloses entDesgloseOld) {

    Tmct0CnmvDesgloses entDesgloseNueva = new Tmct0CnmvDesgloses();
    entDesgloseNueva.setAuditDate(new Date());
    entDesgloseNueva.setAuditUser(entDesgloseOld.getAuditUser());
    entDesgloseNueva.setEjecucion(null);
    entDesgloseNueva.setNuorden(entDesgloseOld.getNuorden());
    entDesgloseNueva.setNbooking(entDesgloseOld.getNbooking());
    entDesgloseNueva.setNucnfclt(entDesgloseOld.getNucnfclt());
    entDesgloseNueva.setNucnfliq(entDesgloseOld.getNucnfliq());
    entDesgloseNueva.setIdDesgloseCamara(entDesgloseOld.getIdDesgloseCamara());
    entDesgloseNueva.setTitulosDesgloses(entDesgloseOld.getTitulosDesgloses());
    entDesgloseNueva.setEstadoDesglose(entDesgloseOld.getEstadoDesglose());
    entDesgloseNueva.setRefOperacion(entDesgloseOld.getRefOperacion());
    entDesgloseNueva.setCdPaisDecisor(entDesgloseOld.getCdPaisDecisor());
    entDesgloseNueva.setIndicadorVentaCorto(entDesgloseOld.getIndicadorVentaCorto());
    entDesgloseNueva.setRefCruceTitulares(entDesgloseOld.getRefCruceTitulares());
    entDesgloseNueva.setTipoIdTransmisora(entDesgloseOld.getTipoIdTransmisora());
    entDesgloseNueva.setIdTransmisora(entDesgloseOld.getIdTransmisora());
    entDesgloseNueva.setTipoAuxIdTransmisora(entDesgloseOld.getTipoAuxIdTransmisora());
    entDesgloseNueva.setAuxIdTransmisora(entDesgloseOld.getAuxIdTransmisora());
    entDesgloseNueva.setCapacidadNegociacion(entDesgloseOld.getCapacidadNegociacion());
    entDesgloseNueva.setEfectivoDesglose(entDesgloseOld.getEfectivoDesglose());
    entDesgloseNueva.setTipoIdDecisorInversion(entDesgloseOld.getTipoIdDecisorInversion());
    entDesgloseNueva.setIdDecisorInversion(entDesgloseOld.getIdDecisorInversion());
    entDesgloseNueva.setReportable(entDesgloseOld.getReportable());
    entDesgloseNueva.setMotivoNoReportable(entDesgloseOld.getMotivoNoReportable());
    return entDesgloseNueva;
  }

  private Tmct0CnmvDesgloses creaTmct0CnmvDesglosesWithDto(OperacionTrDTO operacionRtDTO) {
    Tmct0CnmvDesgloses entDesgloseNueva = new Tmct0CnmvDesgloses();
    entDesgloseNueva.setAuditDate(new Date());
    entDesgloseNueva.setAuditUser("process");
    entDesgloseNueva.setNuorden(operacionRtDTO.getNuorden());
    entDesgloseNueva.setNbooking(operacionRtDTO.getNbooking());
    entDesgloseNueva.setNucnfclt(operacionRtDTO.getNucnfclt());
    entDesgloseNueva.setNucnfliq(operacionRtDTO.getNucnfliq());
    entDesgloseNueva.setIdDesgloseCamara(operacionRtDTO.getIddesglosecamara());
    entDesgloseNueva.setTitulosDesgloses(operacionRtDTO.getImtitulos());
    entDesgloseNueva.setEstadoDesglose('A');
    entDesgloseNueva.setTmct0CnmvTitularesList(new ArrayList<Tmct0CnmvTitulares>());

    // TODO campos que faltan por definir.
    entDesgloseNueva.setIndicadorVentaCorto("");
    entDesgloseNueva.setRefCruceTitulares("");
    entDesgloseNueva.setReportable("");
    entDesgloseNueva.setMotivoNoReportable("");
    entDesgloseNueva.setTipoIdTransmisora("");
    entDesgloseNueva.setIdTransmisora("");
    entDesgloseNueva.setTipoAuxIdTransmisora("");
    entDesgloseNueva.setAuxIdTransmisora("");
    entDesgloseNueva.setCapacidadNegociacion("");
    entDesgloseNueva.setEfectivoDesglose(new BigDecimal(0));
    entDesgloseNueva.setTipoIdDecisorInversion("");
    entDesgloseNueva.setIdDecisorInversion("");
    entDesgloseNueva.setCdPaisDecisor("");
    entDesgloseNueva.setCdEstadoEnvio(0);
    return entDesgloseNueva;
  }

  private Tmct0CnmvTitulares creaCnmvTitularClon(Tmct0CnmvTitulares entTitularOld) {

    Tmct0CnmvTitulares entTitularNuevo = new Tmct0CnmvTitulares();
    entTitularNuevo.setAuditDate(new Date());
    entTitularNuevo.setAuditUser(entTitularOld.getAuditUser());
    entTitularNuevo.setDesglose(null);
    entTitularNuevo.setTpidenti(entTitularOld.getTpidenti());
    entTitularNuevo.setIdentificacion(entTitularOld.getIdentificacion());
    entTitularNuevo.setNombre(entTitularOld.getNombre());
    entTitularNuevo.setApellido1(entTitularOld.getApellido1());
    entTitularNuevo.setApellido2(entTitularOld.getApellido2());
    entTitularNuevo.setTipoPersona(entTitularOld.getTipoPersona());
    entTitularNuevo.setTipoTiular(entTitularOld.getTipoTiular());
    entTitularNuevo.setEstadoTitular(entTitularOld.getEstadoTitular());
    return entTitularNuevo;
  }

  private Tmct0CnmvTitulares creaCNMVTitular(Tmct0afi tmct0afi, Tmct0CnmvDesgloses entDesglose) {
    Tmct0CnmvTitulares entTitular = new Tmct0CnmvTitulares();
    entTitular.setAuditDate(new Date());
    entTitular.setAuditUser("process");
    entTitular.setDesglose(entDesglose);
    entTitular.setTpidenti(tmct0afi.getTpidenti().toString());
    entTitular.setIdentificacion(tmct0afi.getCdholder());
    // Nombre y aplledos dependen del tipo de sociedad Fisica o
    // Juridica.
    if (tmct0afi.getTpsocied() == 'F') {
      entTitular.setNombre(tmct0afi.getNbclient());
      entTitular.setApellido1(tmct0afi.getNbclient1());
      entTitular.setApellido2(tmct0afi.getNbclient2());
      entTitular.setfNacimiento(tmct0afi.getFechaNacimiento());
    }
    else {
      entTitular.setNombre(tmct0afi.getNbclient1());
    }

    entTitular.setTipoPersona(tmct0afi.getTpsocied());
    entTitular.setTipoTiular(tmct0afi.getTptiprep());
    entTitular.setEstadoTitular('A');

    return entTitular;
  }

  private List<Tmct0CnmvTitulares> obtenerTitularesDesglose(String nuorden, String nbooking, String nucnfclt,
      Short nucnfliq, Tmct0CnmvDesgloses entDesglose, String sTipo) {

    List<Tmct0CnmvTitulares> listaTitulares = new ArrayList<Tmct0CnmvTitulares>();

    if (sTipo.equals("AFI")) {

      List<Character> lTipoTitularesNoPermitidos = new ArrayList<Character>();

      lTipoTitularesNoPermitidos.add('R');
      lTipoTitularesNoPermitidos.add('U');

      List<Tmct0afi> listTitularesAfi = tmct0afiDao.findActivosTmct0afiExcludedTipos(nuorden, nbooking, nucnfclt,
          nucnfliq, lTipoTitularesNoPermitidos);
      for (Tmct0afi tmct0afi : listTitularesAfi) {
        Tmct0CnmvTitulares entTitularNuevo = creaCNMVTitular(tmct0afi, entDesglose);
        listaTitulares.add(entTitularNuevo);
      }
    }
    return listaTitulares;
  }

  private void ActualizaDesgloseAProcesado(Tmct0CnmvDesgloses entDesglose) {

    // Se actualiza su desglose camara asociado como enviado con el valor 9
    Tmct0desglosecamara miDesgloseCamara = tmct0desglosecamaraDao.findOne(entDesglose.getIdDesgloseCamara());
    miDesgloseCamara.setEstadoCNMV("9");
    tmct0desglosecamaraDao.save(miDesgloseCamara);

  }
}
