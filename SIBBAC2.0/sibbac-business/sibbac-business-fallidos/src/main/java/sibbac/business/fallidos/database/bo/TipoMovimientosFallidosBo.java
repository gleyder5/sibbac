package sibbac.business.fallidos.database.bo;


import java.util.HashMap;

import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.TipoMovimientosFallidosDao;
import sibbac.business.fallidos.database.model.TipoMovimientosFallidos;
import sibbac.database.bo.AbstractBo;


@Service
public class TipoMovimientosFallidosBo extends AbstractBo< TipoMovimientosFallidos, Long, TipoMovimientosFallidosDao > {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	/** Mapa de tipos de movimientos posibles en fallidos, relacionados con el modo en que afectan en títulos y saldo. */
	private static HashMap< String, String >	tipos_movimientos_fallidos			= null;

	/** Mapa de tipos de movimientos posibles en fallidos, relacionados con el modo en que afectan en títulos y saldo. */
	private static HashMap< String, String >	descripcion_movimientos_fallidos	= null;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

	/**
	 * Devuelve el modo de afectación a aplicar a una operación del tipo de movimiento y el tipo de afectación
	 * especificados.
	 * 
	 * @param tipoMovimiento Tipo de movimiento.
	 * @param tipoAfectacion Tipo de afectación. <code>0</code> si el tipo de afectación es en títulos; <code>1</code> si
	 *            el tipo de afectación es en saldo.
	 * @return <code>java.lang.String - </code>Modo en que se debe afectar o <code>null</code> en caso de no existir el
	 *         tipo de movimiento o el tipo de afectación especificados.
	 */
	public String getModoAfectacion( String tipoMovimiento, int tipoAfectacion ) {

		if ( tipos_movimientos_fallidos == null || tipos_movimientos_fallidos.isEmpty() ) {
			tipos_movimientos_fallidos = new HashMap< String, String >();
			descripcion_movimientos_fallidos = new HashMap< String, String >();

			for ( TipoMovimientosFallidos tipo_movimiento : dao.findAll() ) {
				tipos_movimientos_fallidos.put( tipo_movimiento.getCodigo(),
						tipo_movimiento.getAfectacionTitulos() + "#" + tipo_movimiento.getAfectacionSaldo() );
				descripcion_movimientos_fallidos.put( tipo_movimiento.getCodigo(), tipo_movimiento.getDescripcion() );
			} // for
		} // if

		return tipos_movimientos_fallidos.get( tipoMovimiento ) != null ? tipos_movimientos_fallidos.get( tipoMovimiento ).split( "#" )[ tipoAfectacion ]
				: null;
	} // getModoAfectacion

	/**
	 * Devuelve la descripción del tipo de movimiento especificado.
	 * 
	 * @param tipoMovimiento Tipo de movimiento.
	 * @return <code>java.lang.String - </code>Descripción.
	 */
	public String getDescripcion( String tipoMovimiento ) {

		if ( tipos_movimientos_fallidos == null || tipos_movimientos_fallidos.isEmpty() ) {
			tipos_movimientos_fallidos = new HashMap< String, String >();
			descripcion_movimientos_fallidos = new HashMap< String, String >();

			for ( TipoMovimientosFallidos tipo_movimiento : dao.findAll() ) {
				tipos_movimientos_fallidos.put( tipo_movimiento.getCodigo(),
						tipo_movimiento.getAfectacionTitulos() + "#" + tipo_movimiento.getAfectacionSaldo() );
				descripcion_movimientos_fallidos.put( tipo_movimiento.getCodigo(), tipo_movimiento.getDescripcion() );
			} // for
		} // if

		return descripcion_movimientos_fallidos.get( tipoMovimiento );
	} // getDescripcion

} // TipoMovimientosFallidosBo
