package sibbac.business.fallidos.utils;

public enum TipoMovimientoConciliacionEnum {
	
	BAJA_PRESTAMO_CONVERSION("BPC"), 
	ALTA_PRESTAMO_CONVERSION("ACP"), 
	AJUSTE_DISTRIBUCION_EFECTIVO("ADE"), 
	AJUSTE_DISTRIBUCION_VALORES("ADV"), 
	ENTREGA_DISTRIBUCION_EFECTIVO("EDE"), 
	ENTREGA_DISTRIBUCION_VALORES("EDV"),
	FRACCCIONES_DISTRIBUCION_VALORES("FDV"),
	AJUSTE_FRACCIONES_DISTRIBUCION_VALORES("AFV"),
	AJUSTE_DISTRIBUCION_PRESTAMO("ADP"),
	ALTA_PRESTAMO_AJUSTE("APJ"),
	DEVOLUCION_PRESTAMO_AJUSTE("DPA"),
	CANCELACION_PRESTAMO_EFECTIVO("CPU"),
	RECOMPRA_EFECTIVO("RPE");
	
	private String tipoOperacion;
	
	TipoMovimientoConciliacionEnum(String tipoOperacion){
		this.tipoOperacion = tipoOperacion;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
}
