package sibbac.business.fallidos.database.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import sibbac.business.fallidos.database.dao.Tmct0CnmvDesglosesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvTitularesDao;
import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.utils.ValidatorFieldOperacionesTr;
import sibbac.business.fallidos.writers.OperacionesRtRecordBean;
import sibbac.business.fallidos.writers.TitularesTrRecordBean;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class WriteOperacionesTrCallableImpl implements WriteOperacionesTrCallable {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(WriteOperacionesTrCallableImpl.class);

  @Value("${id_sibsa.tr.default}")
  private String paramIdEntEjecutora;

  @Value("${entidad_mifid.tr.default}")
  private String paramEntidadMiFid;

  @Value("${tipo_id_sibsa.tr.default}")
  private String paramTipoIdEntTransmisora;

  @Value("${cuenta_agregada.tr.default}")
  private String paramCuentaAgregada;

  @Value("${tipo_cuenta_agregada.tr.default}")
  private String paramTipoCuentaAgregada;

  @Autowired
  private Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;

  @Autowired
  private Tmct0CnmvDesglosesDao tmct0CnmvDesglosesDao;

  @Autowired
  private Tmct0CnmvTitularesDao tmct0CnmvTitularesDao;

  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  @Autowired
  private EntityManagerFactory emf;

  private int transactionSize = 1;
  private BufferedReader origenDatos;

  private Tmct0CnmvLog logProceso;

  private FlatFileItemWriter<TitularesTrRecordBean> fileTitulares;
  private FlatFileItemWriter<OperacionesRtRecordBean> fileOperaciones;

  public WriteOperacionesTrCallableImpl(BufferedReader br, int transactionSize,
      FlatFileItemWriter<OperacionesRtRecordBean> fileOperaciones,
      FlatFileItemWriter<TitularesTrRecordBean> fileTitulares, Tmct0CnmvLog logProceso) {
    this.fileOperaciones = fileOperaciones;
    this.fileTitulares = fileTitulares;
    this.origenDatos = br;
    this.transactionSize = transactionSize;
    this.logProceso = logProceso;
  }

  @Override
  public Void call() throws SIBBACBusinessException {

    List<String> lineas;
    int posicionUltimaEjecucionProcesada;

    ExecutionContext ctx = new ExecutionContext();
    EntityManager em = emf.createEntityManager();

    EntityTransaction tx = em.getTransaction();
    List<OperacionesRtRecordBean> listaOperaciones;
    List<TitularesTrRecordBean> listaTitularesOperacionesTr;
    List<Tmct0CnmvEjecuciones> listEjecucionesTr;
    List<Tmct0CnmvDesgloses> listDesglosesTr;
    List<BigInteger> listEjecucionesFromOrigenDatos;

    listaOperaciones = new ArrayList<OperacionesRtRecordBean>();
    listaTitularesOperacionesTr = new ArrayList<TitularesTrRecordBean>();

    listEjecucionesTr = new ArrayList<Tmct0CnmvEjecuciones>();
    listDesglosesTr = new ArrayList<Tmct0CnmvDesgloses>();
    try {
      do {

        lineas = this.readLines();
        if (!lineas.isEmpty()) {
          posicionUltimaEjecucionProcesada = 0;

          listEjecucionesFromOrigenDatos = this.convertToEjecuciones(lineas);
          try {

            while (posicionUltimaEjecucionProcesada < listEjecucionesFromOrigenDatos.size()) {
              posicionUltimaEjecucionProcesada += this.generarRegistrosOperacionAndTitular(
                  listEjecucionesFromOrigenDatos.subList(posicionUltimaEjecucionProcesada,
                      listEjecucionesFromOrigenDatos.size()), listaOperaciones, listaTitularesOperacionesTr,
                  listEjecucionesTr, listDesglosesTr, posicionUltimaEjecucionProcesada);
              tx.begin();
              this.actualizarEstadoEnvios(listEjecucionesTr, listDesglosesTr);

              tmct0CnmvEjecucionesDao.flush();

              this.addContadores(listaOperaciones, logProceso);

              synchronized (fileOperaciones) {
                fileOperaciones.update(ctx);
                fileTitulares.update(ctx);
                try {
                  fileOperaciones.write(listaOperaciones);
                  fileTitulares.write(listaTitularesOperacionesTr);
                }
                catch (Exception e) {
                  throw new SIBBACBusinessException("No se ha podido escribir los datos en los ficheros destino.", e);
                }
              }
              tx.commit();
              listaOperaciones.clear();
              listaTitularesOperacionesTr.clear();
              listEjecucionesTr.clear();
              listDesglosesTr.clear();
            }
          }
          catch (SIBBACBusinessException e) {
            throw e;
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
          finally {
            listaOperaciones.clear();
            listaTitularesOperacionesTr.clear();
            listEjecucionesTr.clear();
            listDesglosesTr.clear();
            listEjecucionesFromOrigenDatos.clear();
            if (tx.isActive()) {
              tx.rollback();
            }
          }
        }
      }
      while (!lineas.isEmpty());
    }
    catch (SIBBACBusinessException e) {
      throw e;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {
      em.close();
    }
    return null;
  }

  private int generarRegistrosOperacionAndTitular(List<BigInteger> listIds,
      List<OperacionesRtRecordBean> listaOperaciones, List<TitularesTrRecordBean> listaTitularesOperacionesTr,
      List<Tmct0CnmvEjecuciones> listEjecucionesTr, List<Tmct0CnmvDesgloses> listDesglosesTr, int posicionInicio) {
    int posicionUltimaEjecucion = 0;
    LOG.debug("[generarRegistrosOperacionAndTitular] inicio...");

    int contador = 1;
    Tmct0CnmvEjecuciones ejecucion;
    OperacionesRtRecordBean operacionTrBean;
    TitularesTrRecordBean titularTrRecordBean;
    PageRequest page = new PageRequest(0, 1000);
    for (BigInteger id : listIds) {
      ejecucion = tmct0CnmvEjecucionesDao.findOne(id);

      List<Tmct0CnmvDesgloses> listDesglosesEjecucion = null;

      // Se recogen los desglose que se tienen que enviar que son aquellos que
      // tienen un estado desglose distinto a X y estado envio pendiente
      listDesglosesEjecucion = tmct0CnmvDesglosesDao.findPendientesEnvioByEjecucion(ejecucion.getId(),
          EstadosEnumerados.H47.PTE_ENVIAR.getId(), page);

      // Ejecucion con un único desglose
      if (listDesglosesEjecucion.size() == 1 && posicionInicio == 0) {
        contador = 1;
        Tmct0CnmvDesgloses entDesglose = listDesglosesEjecucion.get(0);
        operacionTrBean = new OperacionesRtRecordBean(entDesglose);
        operacionTrBean.setCampo5(ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora));
        operacionTrBean.setCampo6(ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid));
        operacionTrBean.setCampo51(ValidatorFieldOperacionesTr.validarTipoIdEntTransmisora(paramTipoIdEntTransmisora));
        listaOperaciones.add(operacionTrBean);
        listDesglosesTr.add(entDesglose);

        // Si es un Alta generamos registros el fichero con los titulares.
        if (entDesglose.getEstadoDesglose().equals('A')) {
          // Se crea el primer registro. Titular extra
          titularTrRecordBean = new TitularesTrRecordBean(listDesglosesEjecucion.get(0)); // Contrapartida
          listaTitularesOperacionesTr.add(titularTrRecordBean);
          contador++;
          // Se recorren el resto de titulares y se escriben

          List<Tmct0CnmvTitulares> listaTitulares = tmct0CnmvTitularesDao.findByDesglose(listDesglosesEjecucion.get(0)
              .getId());

          for (Tmct0CnmvTitulares tmct0CnmvTitular : listaTitulares) {
            titularTrRecordBean = new TitularesTrRecordBean(listDesglosesEjecucion.get(0), tmct0CnmvTitular, contador);
            listaTitularesOperacionesTr.add(titularTrRecordBean);
            contador++;
          }
        }

      }
      else
      // Ejecucion con varios desgloses.
      if (listDesglosesEjecucion.size() > 1 || posicionInicio > 0 && !listDesglosesEjecucion.isEmpty()) {

        // Datos del registro extra de la operacion, a partir de la ejecución
        // y de uno de sus desgloses.

        // Modificacion DDR 3.2 Solo se genera si ESTADO_OPERACION de
        // bsnbpsql.tmct0_cnmv_ejecuciones es “A” y FECHA_ENVIO_ALTA de
        // bsnbpsql.tmct0_cnmv_ejecuciones no está informado (es null),
        // o si ESTADO_OPERACION de bsnbpsql.tmct0_cnmv_ejecuciones es “B” y
        // FECHA_ENVIO_BAJA de bsnbpsql.tmct0_cnmv_ejecuciones no está
        // informado (es null):
        if ((ejecucion.getEstadoOperacion() == 'A' && ejecucion.getfEnvioAlta() == null)
            || (ejecucion.getEstadoOperacion() == 'B' && ejecucion.getfEnvioBaja() == null)) {
          // Datos de la operacion
          operacionTrBean = new OperacionesRtRecordBean(ejecucion, listDesglosesEjecucion.get(0));
          operacionTrBean.setCampo5(ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora));
          operacionTrBean.setCampo6(ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid));
          operacionTrBean
              .setCampo51(ValidatorFieldOperacionesTr.validarTipoIdEntTransmisora(paramTipoIdEntTransmisora));
          listaOperaciones.add(operacionTrBean);

        }

        // Datos titulares extra de la operacion.
        // Modificacion DDR 3.2 Solo se genera si ESTADO_OPERACION de
        // bsnbpsql.tmct0_cnmv_ejecuciones es “A” y FECHA_ENVIO_ALTA de
        // bsnbpsql.tmct0_cnmv_ejecuciones no está informado (es null),
        if (ejecucion.getEstadoOperacion() == 'A' && ejecucion.getfEnvioAlta() == null) {
          // Datos del Primer titular extra - Vendedor
          titularTrRecordBean = new TitularesTrRecordBean(ejecucion); // Contrapartida
          listaTitularesOperacionesTr.add(titularTrRecordBean);
          // Datos del Segundo titular extra - Comprador
          titularTrRecordBean = new TitularesTrRecordBean(ejecucion, paramCuentaAgregada, paramTipoCuentaAgregada); // Contrapartida
          listaTitularesOperacionesTr.add(titularTrRecordBean);

        }

        // Se recorren los desgloses y se añaden los datos de la operacion y
        // sus titulares.
        for (Tmct0CnmvDesgloses entDesglose : listDesglosesEjecucion) {
          contador = 1;

          // Datos de la operacion
          operacionTrBean = new OperacionesRtRecordBean(entDesglose);
          operacionTrBean.setCampo5(ValidatorFieldOperacionesTr.ValidarIdEntEjecutora(paramIdEntEjecutora));
          operacionTrBean.setCampo6(ValidatorFieldOperacionesTr.validarEntMiFid(paramEntidadMiFid));
          operacionTrBean
              .setCampo51(ValidatorFieldOperacionesTr.validarTipoIdEntTransmisora(paramTipoIdEntTransmisora));
          listaOperaciones.add(operacionTrBean);
          listDesglosesTr.add(entDesglose);

          // Datos de los titulares , Solo se escriben si la operacion es un
          // alta.
          // Si es un Alta generamos registros el fichero con los titulares.
          if (entDesglose.getEstadoDesglose().equals('A')) {
            // Se crea el primer registro. Titular extra del desglose actual
            titularTrRecordBean = new TitularesTrRecordBean(entDesglose); // Contrapartida
            listaTitularesOperacionesTr.add(titularTrRecordBean);
            contador++;
            // Se recorren el resto de titulares y se escriben
            List<Tmct0CnmvTitulares> listaTitulares = tmct0CnmvTitularesDao.findByDesglose(entDesglose.getId());
            for (Tmct0CnmvTitulares tmct0CnmvTitular : listaTitulares) {
              titularTrRecordBean = new TitularesTrRecordBean(entDesglose, tmct0CnmvTitular, contador);
              listaTitularesOperacionesTr.add(titularTrRecordBean);
              contador++;
            }
          }
        }

      }
      if (posicionInicio == 0) {
        listEjecucionesTr.add(ejecucion);
      }
      if (listDesglosesEjecucion.size() == page.getPageSize()) {
        break;
      }

      posicionUltimaEjecucion++;
    }
    LOG.debug("[generarRegistrosOperacionAndTitular] fin...");
    return posicionUltimaEjecucion;
  }

  private List<String> readLines() throws SIBBACBusinessException {
    String linea = null;
    List<String> lineas = new ArrayList<String>();
    synchronized (origenDatos) {
      LOG.debug("[readLines] syncronized read lines...");
      for (int i = 0; i < transactionSize; i++) {
        try {
          linea = origenDatos.readLine();
        }
        catch (IOException e) {
          throw new SIBBACBusinessException("No se ha podido leer del origen de datos.", e);
        }
        if (linea != null) {
          lineas.add(linea);
        }
      }

    }
    return lineas;
  }

  private List<BigInteger> convertToEjecuciones(List<String> lineas) throws SIBBACBusinessException {
    LOG.debug("[convertToEjecuciones] inicio...");
    List<BigInteger> listEjecuciones = new ArrayList<BigInteger>();
    for (String st : lineas) {
      try {
        if (st != null && !st.isEmpty())
          listEjecuciones.add(new BigInteger(st.trim().toString()));
      }
      catch (Exception e) {
        throw new SIBBACBusinessException("No se ha podido mapear los datos de la linea: " + st, e);
      }
    }
    LOG.debug("[convertToEjecuciones] fin...");
    return listEjecuciones;
  }

  /**
   * Metodo que después de haber generado los ficheros de MiFIDII, actualiza los
   * estados de las ejecuciones tratadas al estado "ENVIADA BME"
   * 
   * @param listaOperaciones
   * 
   */
  private void actualizarEstadoEnvios(List<Tmct0CnmvEjecuciones> listOperacionesTr,
      List<Tmct0CnmvDesgloses> listDesglosesTr) {
    LOG.debug("[actualizarEstadoEnvios] inicio...");

    // Se actualizan las ejecuciones escritas en los ficheros.
    for (Tmct0CnmvEjecuciones ejecucion : listOperacionesTr) {

      // if(listDesglosesDto.size() == 1) {
      switch (ejecucion.getEstadoOperacion()) {
        case 'A':
          if (ejecucion.getfEnvioAlta() == null) {
            ejecucion.setfEnvioAlta(new Date());
          }

          break;
        case 'B':
          if (ejecucion.getfEnvioBaja() == null) {
            ejecucion.setfEnvioBaja(new Date());
          }

        default:
          break;
      }
      ejecucion.setCdEstadoEnvio(new Tmct0estado(EstadosEnumerados.H47.ENVIADA.getId()));

      // }
    }

    // Se actualizan los desgloses escritos en los ficheros.
    for (Tmct0CnmvDesgloses desglose : listDesglosesTr) {

      // if(listDesglosesDto.size() == 1) {
      switch (desglose.getEstadoDesglose()) {
        case 'A':
          desglose.setfEnvioAlta(new Date());
          break;
        case 'B':
          desglose.setfEnvioBaja(new Date());
        default:
          break;
      }

      desglose.setCdEstadoEnvio(EstadosEnumerados.H47.ENVIADA.getId());

      // }
    }
    tmct0CnmvEjecucionesDao.save(listOperacionesTr);
    tmct0CnmvDesglosesDao.save(listDesglosesTr);

    LOG.debug("[actualizarEstadoEnvios] final...");
  }

  private void addContadores(List<OperacionesRtRecordBean> listaOperaciones, Tmct0CnmvLog logProceso) {
    LOG.debug("[addContadores] inicio...");
    // Una vez escrito bien los ficheros sin ningun problema, se
    // actualizan los contadores y las fechas de envio.
    synchronized (logProceso) {
      for (OperacionesRtRecordBean operacionTrBeanEnviada : listaOperaciones) {
        // En el campo 3 se incluye el estado de la operacion, lo usuamos
        // para incrementar el contador de altas o bajas.
        if (operacionTrBeanEnviada.getCampo3().equals("A")) {
          logProceso.setContadorAltas(logProceso.getContadorAltas().add(BigInteger.ONE));
        }

        if (operacionTrBeanEnviada.getCampo3().equals("B")) {
          logProceso.setContadorBajas(logProceso.getContadorBajas().add(BigInteger.ONE));
        }
      }
    }

    LOG.debug("[addContadores] fin...");
  }
}
