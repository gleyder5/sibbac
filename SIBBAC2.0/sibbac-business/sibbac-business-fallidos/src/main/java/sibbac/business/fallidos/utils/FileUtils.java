package sibbac.business.fallidos.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileUtils {

  private static final int BUFFER = 4096;

  public static void unzipFile(String outputDir, File zipFile) throws IOException {

    ZipFile zip = new ZipFile(zipFile);
    Enumeration<? extends ZipEntry> zipEntries = zip.entries();

    while (zipEntries.hasMoreElements()) {

      ZipEntry entry = zipEntries.nextElement();
      String fileName = entry.getName();

      Path filePath = Paths.get(outputDir, fileName);

      if (!entry.isDirectory()) {

        BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
        int currentByte;

        byte data[] = new byte[BUFFER];

        FileOutputStream fos = new FileOutputStream(filePath.toString());
        BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);

        while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
          dest.write(data, 0, currentByte);
        }

        dest.flush();
        dest.close();
        is.close();

      } else {

        Files.createDirectories(filePath);
      }
    }

    zip.close();
  }

  public static void moveFile(Path source, Path target) throws IOException {
    Files.move(source, target, StandardCopyOption.ATOMIC_MOVE);
  }

  public static void changeExtenxion(Path tmpFile, String suffix) throws IOException {
    String newNameString = getNameChangedExtension(tmpFile.toString(), suffix);
    Path dir = tmpFile.getParent();
    Path fn = tmpFile.getFileSystem().getPath(newNameString);
    Path target = (dir == null) ? fn : dir.resolve(fn);
    FileUtils.moveFile(tmpFile, target);
  }

  public static String getNameChangedExtension(String tmpFile, String suffix) throws IOException {
    String newNameString = tmpFile.toString();
    newNameString = newNameString.substring(0, newNameString.indexOf(".")) + suffix;
    return newNameString;
  }

  public static InputStream getZipFileInputStream(File zipFile) throws IOException {
    ZipFile zip = new ZipFile(zipFile);
    Enumeration<? extends ZipEntry> zipEntries = zip.entries();
    InputStream isFile = null;

    while (zipEntries.hasMoreElements()) {
      ZipEntry entry = zipEntries.nextElement();

      if (!entry.isDirectory()) {
        isFile = zip.getInputStream(entry);
      }
    }

    zip.close();

    return isFile;
  }

  public static void gzFile(Path txt, Path fgz) throws IOException {
    InputStream is = new FileInputStream(txt.toFile());
    String filename = fgz.getFileName().toString();

    if (!filename.endsWith(".gz")) {
      filename += ".gz";
    }
    Path parent = fgz.getParent();

    fgz = parent.resolve(filename);
    GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(fgz.toFile()));
    byte[] buffer = new byte[1024];
    int len = 0;
    while ((len = is.read(buffer)) > 0) {
      gzos.write(buffer, 0, len);
    }
    gzos.close();
    is.close();
  }

  public static void unGzFile(Path fgz, Path txt) throws IOException {
    GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(fgz.toFile()));
    FileOutputStream fos = new FileOutputStream(txt.toFile(), true);

    byte[] buffer = new byte[1024];
    int len = 0;
    while ((len = gzis.read(buffer)) > 0) {
      fos.write(buffer, 0, len);
    }
    fos.close();
    gzis.close();
  }

}
