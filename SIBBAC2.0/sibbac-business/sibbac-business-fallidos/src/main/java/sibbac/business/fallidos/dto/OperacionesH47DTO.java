package sibbac.business.fallidos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.common.SIBBACPTICommons;

/**
 * @version 1.0
 * @author XI316153
 */
public class OperacionesH47DTO implements Serializable {

  protected static final Logger LOG = LoggerFactory.getLogger(OperacionesH47DTO.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -5361282147063755527L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  private Long id;
  private Character estoper;
  private String estenv;
  private Character sentido;
  private String isin;
  private BigDecimal titulos;
  private String titularPrincipal;
  private String nbooking;
  private String nucnfclt;
  private String nucnfliq;
  // Antiguo detalle
  private BigDecimal precioUnitario;
  private BigDecimal cantidadTotal;
  private BigDecimal efectivoTotal;
  private String identificadorTransaccion;
  private String fhNegociacion;
  private String hNegociacion;
  private String fhValor;
  private Character capacidadNegociacion;
  private String mensajeError;
  private String cdEstadoAsig;
  private String alias;
  private String feejeliq;
  private String nombreIsin;
  private String nombreAlias;
  private String fContratacion;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /** Constructor I. Constructor por defecto. */
  public OperacionesH47DTO() {
  } // OperacionesH47DTO

  /**
   * Constructor II. Genera un objeto de la clase a partir de una entidad de base de datos.
   * 
   * @param operacion OperaciónH47 obtenida de la base de datos.
   */
  public OperacionesH47DTO(OperacionH47 operacion) {
    this.id = operacion.getId();
    this.estoper = operacion.getEstOperacion();
    if (operacion.getEstadoEnvio() != null) {
      this.estenv = operacion.getEstadoEnvio().getNombre();
    }
    this.sentido = operacion.getSentido();
    this.isin = operacion.getIdInstrumento();
    this.titulos = operacion.getTitulos();
    this.titularPrincipal = operacion.getTitularPrincipal();
    this.nbooking = operacion.getNbooking();
    this.nucnfclt = operacion.getNucnfclt();
    if (operacion.getNucnfliq() != null) {
      this.nucnfliq = operacion.getNucnfliq().toString();
    }
    this.precioUnitario = operacion.getPrecioUnitario();
    this.cantidadTotal = operacion.getCantidadTotal();
    this.efectivoTotal = operacion.getEfectivoTotal();
    this.identificadorTransaccion = operacion.getIdentificadorTransaccion();
    if (operacion.getFhNegociacion() != null) {
      this.fhNegociacion = SIBBACPTICommons.convertDateToString(operacion.getFhNegociacion(),
                                                                SIBBACPTICommons.DATE_FORMAT);
    }
    if (operacion.gethNegociacion() != null) {
      this.hNegociacion = SIBBACPTICommons.convertDateToString(operacion.gethNegociacion(), "HHmmss");
    }
    if (operacion.getFhValor() != null) {
      this.fhValor = SIBBACPTICommons.convertDateToString(operacion.getFhValor(), SIBBACPTICommons.DATE_FORMAT);
    }
    this.capacidadNegociacion = operacion.getCapacidadNegociacion();
    if (operacion.getError() != null) {
      mensajeError = operacion.getError().getDescripcion();
    }
  } // OperacionesH47DTO

  /**
   * Constructor III. Genera un objeto de la clase a partir de los parámetros especificados.
   * 
   * @param tmct0alc Alc.
   * @param tmct0alo Alo.
   * @param titularPrincipal Titular peincipal-
   * @param titulos Número de títulos del alc.
   * @param efectivo Efectivo del alc.
   * @param estadoMovimientosH47 Estado de los movimientos procesados.
   * @param alias Alias del Alc.
   * @param estadoAsigName Nombre del estado de asignación del alc.
   */
  public OperacionesH47DTO(Tmct0alc tmct0alc,
                           Tmct0alo tmct0alo,
                           Tmct0afi titularPrincipal,
                           BigDecimal titulos,
                           BigDecimal efectivo,
                           String estadoMovimientosH47,
                           String alias,
                           String estadoAsigName) {
    LOG.debug("OperacionesH47DTO - Se van a calcular los datos para mostrar en pantalla");
    BigDecimal precio = BigDecimal.ZERO;

    if (efectivo != null && titulos != null && titulos.signum() > 0) {
      precio = efectivo.divide(titulos, 5, RoundingMode.HALF_UP);
    }
    this.titulos = titulos;
    this.efectivoTotal = efectivo.setScale(2, BigDecimal.ROUND_HALF_UP);
    this.precioUnitario = precio;

    this.sentido = tmct0alo.getCdtpoper();

    this.alias = alias;
    this.nombreAlias = tmct0alo.getDescrali();

    this.isin = tmct0alo.getCdisin();
    this.nombreIsin = tmct0alo.getNbvalors();

    StringBuilder infoTitular = new StringBuilder();
    if (titularPrincipal.getNbclient() != null) {
      infoTitular.append(titularPrincipal.getNbclient().trim() + " ");
    }
    if (titularPrincipal.getNbclient1() != null) {
      infoTitular.append(titularPrincipal.getNbclient1().trim() + " ");
    }
    if (titularPrincipal.getNbclient2() != null) {
      infoTitular.append(titularPrincipal.getNbclient2().trim() + " ");
    }
    if (titularPrincipal.getNudnicif() != null) {
      infoTitular.append("," + titularPrincipal.getNudnicif().trim());
    }
    this.titularPrincipal = infoTitular.toString();

    this.nbooking = tmct0alc.getId().getNbooking();
    this.nucnfclt = tmct0alc.getId().getNucnfclt();
    this.nucnfliq = Short.toString(tmct0alc.getId().getNucnfliq());
    this.cdEstadoAsig = estadoAsigName;

    // this.id = movimiento.getIdmovimiento();

    // this.feejeliq = SIBBACPTICommons.convertDateToString(alc.getFeejeliq(), SIBBACPTICommons.DATE_FORMAT);
    this.feejeliq = SIBBACPTICommons.convertDateToString(tmct0alc.getFeejeliq(), FormatDataUtils.DATE_FORMAT);
  } // OperacionesH47DTO

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the estoper
   */
  public Character getEstoper() {
    return estoper;
  }

  /**
   * @return the estenv
   */
  public String getEstenv() {
    return estenv;
  }

  /**
   * @return the sentido
   */
  public Character getSentido() {
    return sentido;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the titulos
   */
  public BigDecimal getTitulos() {
    return titulos;
  }

  /**
   * @return the titularPrincipal
   */
  public String getTitularPrincipal() {
    return titularPrincipal;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public String getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @return the precioUnitario
   */
  public BigDecimal getPrecioUnitario() {
    return precioUnitario;
  }

  /**
   * @return the cantidadTotal
   */
  public BigDecimal getCantidadTotal() {
    return cantidadTotal;
  }

  /**
   * @return the efectivoTotal
   */
  public BigDecimal getEfectivoTotal() {
    return efectivoTotal;
  }

  /**
   * @return the identificadorTransaccion
   */
  public String getIdentificadorTransaccion() {
    return identificadorTransaccion;
  }

  /**
   * @return the fhNegociacion
   */
  public String getFhNegociacion() {
    return fhNegociacion;
  }

  /**
   * @return the hNegociacion
   */
  public String gethNegociacion() {
    return hNegociacion;
  }

  /**
   * @return the fhValor
   */
  public String getFhValor() {
    return fhValor;
  }

  /**
   * @return the capacidadNegociacion
   */
  public Character getCapacidadNegociacion() {
    return capacidadNegociacion;
  }

  /**
   * @return the mensajeError
   */
  public String getMensajeError() {
    return mensajeError;
  }

  /**
   * @return the cdEstadoAsig
   */
  public String getCdEstadoAsig() {
    return cdEstadoAsig;
  }

  /**
   * @return the alias
   */
  public String getAlias() {
    return alias;
  }

  /**
   * @return the feejeliq
   */
  public String getFeejeliq() {
    return feejeliq;
  }

  /**
   * @return the nombreIsin
   */
  public String getNombreIsin() {
    return nombreIsin;
  }

  /**
   * @return the nombreAlias
   */
  public String getNombreAlias() {
    return nombreAlias;
  }

  /**
   * @return the fContratacion
   */
  public String getfContratacion() {
    return fContratacion;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @param estoper the estoper to set
   */
  public void setEstoper(Character estoper) {
    this.estoper = estoper;
  }

  /**
   * @param estenv the estenv to set
   */
  public void setEstenv(String estenv) {
    this.estenv = estenv;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param titulos the titulos to set
   */
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  /**
   * @param titularPrincipal the titularPrincipal to set
   */
  public void setTitularPrincipal(String titularPrincipal) {
    this.titularPrincipal = titularPrincipal;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(String nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @param precioUnitario the precioUnitario to set
   */
  public void setPrecioUnitario(BigDecimal precioUnitario) {
    this.precioUnitario = precioUnitario;
  }

  /**
   * @param cantidadTotal the cantidadTotal to set
   */
  public void setCantidadTotal(BigDecimal cantidadTotal) {
    this.cantidadTotal = cantidadTotal;
  }

  /**
   * @param efectivoTotal the efectivoTotal to set
   */
  public void setEfectivoTotal(BigDecimal efectivoTotal) {
    this.efectivoTotal = efectivoTotal;
  }

  /**
   * @param identificadorTransaccion the identificadorTransaccion to set
   */
  public void setIdentificadorTransaccion(String identificadorTransaccion) {
    this.identificadorTransaccion = identificadorTransaccion;
  }

  /**
   * @param fhNegociacion the fhNegociacion to set
   */
  public void setFhNegociacion(String fhNegociacion) {
    this.fhNegociacion = fhNegociacion;
  }

  /**
   * @param hNegociacion the hNegociacion to set
   */
  public void sethNegociacion(String hNegociacion) {
    this.hNegociacion = hNegociacion;
  }

  /**
   * @param fhValor the fhValor to set
   */
  public void setFhValor(String fhValor) {
    this.fhValor = fhValor;
  }

  /**
   * @param capacidadNegociacion the capacidadNegociacion to set
   */
  public void setCapacidadNegociacion(Character capacidadNegociacion) {
    this.capacidadNegociacion = capacidadNegociacion;
  }

  /**
   * @param mensajeError the mensajeError to set
   */
  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  /**
   * @param cdEstadoAsig the cdEstadoAsig to set
   */
  public void setCdEstadoAsig(String cdEstadoAsig) {
    this.cdEstadoAsig = cdEstadoAsig;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(String feejeliq) {
    this.feejeliq = feejeliq;
  }

  /**
   * @param nombreIsin the nombreIsin to set
   */
  public void setNombreIsin(String nombreIsin) {
    this.nombreIsin = nombreIsin;
  }

  /**
   * @param nombreAlias the nombreAlias to set
   */
  public void setNombreAlias(String nombreAlias) {
    this.nombreAlias = nombreAlias;
  }

  /**
   * @param fContratacion the fContratacion to set
   */
  public void setfContratacion(String fContratacion) {
    this.fContratacion = fContratacion;
  }

} // OperacionesH47DTO
