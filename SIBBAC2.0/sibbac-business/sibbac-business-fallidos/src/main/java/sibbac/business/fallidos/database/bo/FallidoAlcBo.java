package sibbac.business.fallidos.database.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.FallidoAlcDao;
import sibbac.business.fallidos.database.model.FallidoAlc;
import sibbac.database.bo.AbstractBo;

/**
 * @author XI316153
 */
@Service
public class FallidoAlcBo extends AbstractBo<FallidoAlc, Long, FallidoAlcDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FallidoAlcBo.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

} // FallidoAlcBo
