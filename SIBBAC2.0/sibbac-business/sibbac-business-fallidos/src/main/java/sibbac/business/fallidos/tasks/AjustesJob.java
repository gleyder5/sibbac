package sibbac.business.fallidos.tasks;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.database.bo.AjustesFixmlFileService;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;

/**
 * Contiene la lógica de inicialización de la tarea de control de operaciones de ajuste.
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author XI316153
 */
// @SIBBACJob(group = Task.GROUP_FALLIDOS.NAME, name = Task.GROUP_FALLIDOS.JOB_AJUSTES, interval = 3, delay = 0,
// intervalUnit = DateBuilder.IntervalUnit.HOUR)
public class AjustesJob extends FallidosTaskConcurrencyPrevent {
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(AjustesJob.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private AjustesFixmlFileService ajusteFixmlFileService;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Inicializa los atributos de la clase.
   */
  public AjustesJob() {
    LOG.debug("[AjustesJob<Construct>] time: " + new Date());
  } // AjustesJob

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void executeTask() {
    LOG.debug("[AjustesJob<execute>] Start time: " + new Date());

    try {
      ajusteFixmlFileService.processAjustesFixmlFiles(this.jobPk);
      LOG.debug("[AjustesJob<execute>] Tarea ejecutada correctamente. ");
    } catch (Exception ex) {
      LOG.error(ex.getMessage(), ex);
      throw ex;
    }
    LOG.debug("[AjustesJob<execute>] End time: " + new Date());
  } // execute

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.FALLIDO_AJUSTES;
  }

} // AjustesJob
