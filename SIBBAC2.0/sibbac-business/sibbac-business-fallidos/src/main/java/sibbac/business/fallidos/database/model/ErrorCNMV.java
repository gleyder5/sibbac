package sibbac.business.fallidos.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.model.MasterData;
import sibbac.database.DBConstants;


/**
 * Class that represents a {@link sibbac.database.model.ConstanteH47 }.
 * Entity: "ConstanteH47".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.FALLIDOS.H47_ERRORCNMV )
@Entity
@Component
public class ErrorCNMV extends MasterData< ErrorCNMV > {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer( "ErrorCNMV" );
		sb.append( " [COD==" + this.getCodigo() + "]" );
		sb.append( " [DESCRIPCION==" + this.getDescripcion() + "]" );
		return sb.toString();
	}


}
