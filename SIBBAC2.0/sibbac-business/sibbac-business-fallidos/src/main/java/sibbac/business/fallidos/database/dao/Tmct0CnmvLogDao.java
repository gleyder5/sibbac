package sibbac.business.fallidos.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.Tmct0CnmvLog;
import sibbac.business.fallidos.dto.ResultadoEjecucionDTO;

@Repository
public interface Tmct0CnmvLogDao extends JpaRepository<Tmct0CnmvLog, BigInteger>{

  @Query( " SELECT new  sibbac.business.fallidos.dto.ResultadoEjecucionDTO(log) FROM Tmct0CnmvLog log WHERE log.auditDate BETWEEN :fEjecucionDesde  AND :fEjecucionHasta order by log.auditDate ")
  List<ResultadoEjecucionDTO> findByFechasEjecucion (@Param("fEjecucionDesde") Date fEjecucionDesde, @Param("fEjecucionHasta") Date fEjecucionHasta);
  
}
