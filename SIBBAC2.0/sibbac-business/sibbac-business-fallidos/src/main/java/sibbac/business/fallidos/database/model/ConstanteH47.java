package sibbac.business.fallidos.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.database.model.ConstanteH47 }.
 * Entity: "ConstanteH47".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
@Table( name = DBConstants.FALLIDOS.H47_CONSTANTES )
@Entity
public class ConstanteH47 extends ATable< ConstanteH47 >{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1483744032422577218L;
	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
	@Column( name = "CAMPO", length = 25 )
	private String			campo;
	@Column( name = "VALOR", length = 255 )
	private String			valor;

	public ConstanteH47() {
		super();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer( "H47_CONSTANTE" );
		sb.append( " [CAMPO==" + this.campo + "]" );
		sb.append( " [VALOR==" + this.valor + "]" );
		return sb.toString();
	}
	
	public String getCampo() {
		return campo;
	}


	
	public void setCampo( String campo ) {
		this.campo = campo;
	}


	
	public String getValor() {
		return valor;
	}


	
	public void setValor( String valor ) {
		this.valor = valor;
	}

}
