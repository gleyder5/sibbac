package sibbac.business.fallidos.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.fallidos.database.model.TipoMovimientosFallidos}.
 * 
 * @version 1.0
 * @author XI316153
 */
@Table( name = DBConstants.FALLIDOS.TIPO_MOVIMIENTOS_FALLIDOS )
@Entity
@XmlRootElement
@Audited
public class TipoMovimientosFallidos extends ATableAudited< TipoMovimientosFallidos > {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Identificador para la serialización de objetos. */
	private static final long	serialVersionUID	= 8590161774006066393L;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	/**
	 * Código del movimiento.
	 */
	@Column( name = "CODIGO", nullable = false, unique = true, length = 3 )
	@XmlAttribute
	protected String			codigo;

	/**
	 * Descripción del movimiento.
	 */
	@Column( name = "DESCRIPCION", nullable = false, length = 40 )
	@XmlAttribute
	protected String			descripcion;

	/**
	 * Modo de afectación del tipo de movimiento en títulos.
	 */
	@Column( name = "AFECTACION_TITULOS_CONTADO", nullable = false, length = 1 )
	@XmlAttribute
	protected String			afectacionTitulos;

	/**
	 * Modo de afectación del tipo de movimiento en saldo.
	 */
	@Column( name = "AFECTACION_SALDO_EFECTIVO", nullable = false, length = 1 )
	@XmlAttribute
	protected String			afectacionSaldo;

	/**
	 * Modo de afectación del tipo de movimiento en saldo.
	 */
	@Column( name = "AFECTACION_OTROS_TITULOS", nullable = false, length = 1 )
	@XmlAttribute
	protected String			afectacionOtrosTitulos;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public TipoMovimientosFallidos() {
	} // TipoMovimientosFallidos

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	} // getCodigo

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	} // getDescripcion

	/**
	 * @return the afectacionTitulos
	 */
	public String getAfectacionTitulos() {
		return afectacionTitulos;
	} // getAfectacionTitulos

	/**
	 * @return the afectacionSaldo
	 */
	public String getAfectacionSaldo() {
		return afectacionSaldo;
	} // getAfectacionSaldo

	/**
	 * @return the afectacionOtrosTitulos
	 */
	public String getAfectacionOtrosTitulos() {
		return afectacionOtrosTitulos;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	} // setCodigo

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	} // setDescripcion

	/**
	 * @param afectacionTitulos the afectacionTitulos to set
	 */
	public void setAfectacionTitulos( String afectacionTitulos ) {
		this.afectacionTitulos = afectacionTitulos;
	} // setAfectacionTitulos

	/**
	 * @param afectacionSaldo the afectacionSaldo to set
	 */
	public void setAfectacionSaldo( String afectacionSaldo ) {
		this.afectacionSaldo = afectacionSaldo;
	} // setAfectacionSaldo

	/**
	 * @param afectacionOtrosTitulos the afectacionOtrosTitulos to set
	 */
	public void setAfectacionOtrosTitulos( String afectacionOtrosTitulos ) {
		this.afectacionOtrosTitulos = afectacionOtrosTitulos;
	}

} // TipoMovimientosFallidos
