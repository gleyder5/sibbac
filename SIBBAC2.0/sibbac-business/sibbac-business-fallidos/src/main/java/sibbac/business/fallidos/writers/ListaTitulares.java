package sibbac.business.fallidos.writers;

import java.util.ArrayList;


public class ListaTitulares<E> extends ArrayList<E>{



	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8120540074514979452L;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(E element:this){
			sb.append( element.toString() );
		}
		return sb.toString();
	}

}
