package sibbac.business.fallidos.database.bo;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.OperacionH47Dao;
import sibbac.business.fallidos.database.dao.specifications.OperacionH47Specifications;
import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.database.bo.AbstractBo;

/**
 * Business object de la clase OperacionH47.
 * 
 * @author XI316153
 * @see AbstractBo
 * @see OperacionH47Dao
 * @see OperacionH47
 */
@Service
public class OperacionH47Bo extends AbstractBo<OperacionH47, Long, OperacionH47Dao> {

  /**
   * @param isin
   * @param fhLiquidacionDe
   * @param fhLiquidacionA
   * @param fhEjecucionDe
   * @param fhEjecucionA
   * @param estadoOperacion
   * @param estadoEnvio
   * @return
   */
  public List<OperacionH47> findViaSpecifications(String isin,
                                                  Date fhLiquidacionDe,
                                                  Date fhLiquidacionA,
                                                  final Date fhEjecucionDe,
                                                  final Date fhEjecucionA,
                                                  Character estadoOperacion,
                                                  String estadoEnvio) {
    return dao.findAll(OperacionH47Specifications.listadoFriltrado(isin, fhLiquidacionDe, fhLiquidacionA,
                                                                   fhEjecucionDe, fhEjecucionA, estadoOperacion,
                                                                   estadoEnvio));
  } // findViaSpecifications

  /**
   * Genera una operación de sentido contrario a la operación especificada.
   * 
   * @param operacion Operación de la que generar una operación de sentido contrario.
   * @param nudnicifBanco CIF del banco.
   * @return <code>OperacionH47 - </code>Operación de sentido contrario creada.
   */
  public OperacionH47 createOperacionContraria(OperacionH47 operacion, String nudnicifBanco) {
    OperacionH47 operacionContraria = new OperacionH47(operacion);
    operacionContraria.setTitularPrincipal(nudnicifBanco);
    operacionContraria.setCapacidadNegociacion('P');
    if (operacionContraria.getSentido().equals('B')) {
      operacionContraria.setSentido('S');
    } else {
      operacionContraria.setSentido('B');
    }
    return operacionContraria;
  } // createOperacionContraria

  /**
   * Busca todas las operaciones H47 que estén en estado pendiente de enviar .
   * 
   * @return <code>List<OperacionH47> - </code>Lista de opereaciones encontradas.
   */
  public List<OperacionH47> findAllForFile() {
    return dao.findAll(OperacionH47Specifications.listadoFriltrado(null,
                                                                   null,
                                                                   null,
                                                                   null,
                                                                   null,
                                                                   null,
                                                                   String.valueOf(EstadosEnumerados.H47.PTE_ENVIAR.getId())));
  } // findAllForFile

  /**
   * 
   * @param identificadorTransaccion
   * @param fhNegociacion
   * @param sentido
   * @return
   */
  public OperacionH47 findFirstByIdentificadorTransaccionAndFhNegociacionAndSentido(String identificadorTransaccion,
                                                                                    Date fhNegociacion,
                                                                                    Character sentido) {
    return dao.findFirstByIdentificadorTransaccionAndFhNegociacionAndSentido(identificadorTransaccion, fhNegociacion,
                                                                             sentido);
  } // findFirstByIdentificadorTransaccionAndFhNegociacionAndSentido

  /**
   * 
   * @param id
   * @param fhNegociacion
   * @return
   */
  public OperacionH47 findByIdAndFhNegociacion(Long id, Date fhNegociacion) {
    return dao.findByIdAndFhNegociacion(id, fhNegociacion);
  } // findByIdAndFhNegociacion

  public BigInteger findNumTitulosDesgloseActuales(final String nbooking,
                                                   final Short nucnfliq,
                                                   final String nucnfclt,
                                                   final Character capacidadNegociadora,
                                                   final Character estadoOperacion) {
    return dao.findNumTitulosDesgloseActuales(nbooking, nucnfliq, nucnfclt, capacidadNegociadora, estadoOperacion);
  } // findNumTitulosDesgloseActuales

  public BigInteger findNumTitulosDesgloseMovimiento(final String nbooking,
                                                     final Short nucnfliq,
                                                     final String nucnfclt,
                                                     final Character capacidadNegociadora,
                                                     final Character estadoOperacion,
                                                     final Long idMovimiento) {
    return dao.findNumTitulosDesgloseMovimiento(nbooking, nucnfliq, nucnfclt, capacidadNegociadora, estadoOperacion,
                                                idMovimiento);
  } // findNumTitulosDesgloseActuales

} // OperacionH47Bo
