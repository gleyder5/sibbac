package sibbac.business.fallidos.database.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


  
  /**
   * Entidad para la gestion de TMCT0_CNMV_TITULARES
   * 
   * @author Neoris
   *
   */
  @Entity
  @Table(name = "TMCT0_CNMV_LOG")
  public class Tmct0CnmvLog {

    private static final Logger LOG = LoggerFactory
        .getLogger(Tmct0CnmvEjecuciones.class);

    /** Campos de la entidad */

    /**
     * Identificador del registro en la tabla
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", length = 8, nullable = false)
    private BigInteger id;

    /**
     * Fecha en la que actual de la creación o última modificación
     */
    @Column(name = "AUDIT_DATE", length = 10, nullable = false)
    private Date auditDate;

    /**
     * identificador del usuario que creó o modificó por última vez la factura
     */
    @Column(name = "AUDIT_USER", length = 20, nullable = false)
    private String auditUser;
  
    
    /**
     * Proceso que genera el registro
     */
    @Column(name = "PROCESO", length = 30, nullable = false)
    private String proceso;
    
    /**
     * Numero de altas de ejecuciones
     */
    
    @Column(name = "CONT_ALTA_EJECUCIONES", length = 8, nullable = false)
    private BigInteger contadorAltas;
    
    
    /**
     * Numero de bajas de ejecuciones
     */
    
    @Column(name = "CONT_BAJA_EJECUCIONES", length = 8, nullable = false)
    private BigInteger contadorBajas;    
    
    
    /**
     * Incidencia : S / N 
     */
    @Column(name = "INCIDENCIA", length = 1, nullable = false)
    private Character incidencia;
    
    /**
     * Datos de la incidencia producida
     */
    @Column(name = "COMENTARIO", length = 1000 )
    private String comentario;
    
    
    /**
     * Constructor
     */
    public Tmct0CnmvLog() {
      super();
    }


    /**
     * Constructor con parametros
     */
    
    public Tmct0CnmvLog(BigInteger id,
                        Date auditDate,
                        String auditUser,
                        String proceso,
                        BigInteger contadorAltas,
                        BigInteger contadorBajas,
                        Character incidencia,
                        String comentario) {
      super();
      this.id = id;
      this.auditDate = auditDate;
      this.auditUser = auditUser;
      this.proceso = proceso;
      this.contadorAltas = contadorAltas;
      this.contadorBajas = contadorBajas;
      this.incidencia = incidencia;
      this.comentario = comentario;
    }


    /**
     * @return the id
     */
    public BigInteger getId() {
      return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(BigInteger id) {
      this.id = id;
    }


    /**
     * @return the auditDate
     */
    public Date getAuditDate() {
      return auditDate;
    }


    /**
     * @param auditDate the auditDate to set
     */
    public void setAuditDate(Date auditDate) {
      this.auditDate = auditDate;
    }


    /**
     * @return the auditUser
     */
    public String getAuditUser() {
      return auditUser;
    }


    /**
     * @param auditUser the auditUser to set
     */
    public void setAuditUser(String auditUser) {
      this.auditUser = auditUser;
    }


    /**
     * @return the proceso
     */
    public String getProceso() {
      return proceso;
    }


    /**
     * @param proceso the proceso to set
     */
    public void setProceso(String proceso) {
      this.proceso = proceso;
    }


    /**
     * @return the contadorAltas
     */
    public BigInteger getContadorAltas() {
      return contadorAltas;
    }


    /**
     * @param contadorAltas the contadorAltas to set
     */
    public void setContadorAltas(BigInteger contadorAltas) {
      this.contadorAltas = contadorAltas;
    }


    /**
     * @return the contadorBajas
     */
    public BigInteger getContadorBajas() {
      return contadorBajas;
    }


    /**
     * @param contadorBajas the contadorBajas to set
     */
    public void setContadorBajas(BigInteger contadorBajas) {
      this.contadorBajas = contadorBajas;
    }


    /**
     * @return the incidencia
     */
    public Character getIncidencia() {
      return incidencia;
    }


    /**
     * @param incidencia the incidencia to set
     */
    public void setIncidencia(Character incidencia) {
      this.incidencia = incidencia;
    }


    /**
     * @return the comentario
     */
    public String getComentario() {
      return comentario;
    }


    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
      this.comentario = comentario;
    }    
    
    
    
}
