package sibbac.business.fallidos.writers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;
import sibbac.business.fallidos.database.model.Tmct0CnmvTitulares;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fallidos.utils.ValidatorFieldTitulares;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.common.utils.FormatDataUtils;

public class TitularesTrRecordBean extends RecordBean {
	
	/** Identificador para la serialización de objetos. */
	private static final long serialVersionUID = 9108693445970231700L;
	
	/** Referencia para la inserción de logs. */
	private static final Logger LOG = LoggerFactory.getLogger(OperacionesRtRecordBean.class);

	/** Número máximo de titulares que se pueden enviar en una línea. */
	public static final Integer MAX_HOLDERS = 19;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
	String campo1;
	String campo2;
	int campo3;
	String campo4;  
	String campo5;
	String campo6;
	String campo7;
	String campo8;
	String campo9;
	String campo10;
	String campo11;
	String campo12;
	String campo13;
	String campo14;
	String campo15;
	String campo16;
	String campo17;
	String campo18;
	String campo19;
	String campo20;
	String campo21;
	String campo22;
	String campo23;
	String campo24;
	String campo25;
	String campo26;
	String campo27;

	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES
	// Constructor del titular
	public TitularesTrRecordBean(Tmct0CnmvDesgloses entDesglose, Tmct0CnmvTitulares cnmvTitular, int iContador) {
		
		this.campo1 = ValidatorFieldTitulares.validarTipoRegistro(Constantes.TIPO_REGISTRO_TITULARES);
		if (entDesglose.getRefOperacion()!= null ){
		  this.campo2 = ValidatorFieldTitulares.validarRefOperacion(entDesglose.getRefOperacion().trim());  
		}
		

		// Campo 3, contador, 
		this.campo3 = iContador;
		
		// Datos del comprador Campo 4 - 13  y 24-25 
		if(entDesglose.getEjecucion().getSentido().equals('C')) {
		  //Identificador
		  
		  if (cnmvTitular.getIdentificacion() != null){
		    this.campo4 = ValidatorFieldTitulares.validarIdentificacion(cnmvTitular.getIdentificacion().trim());  
		  }
			

			// Pais de la sucursal
			if (cnmvTitular.getCdPaisSucCliente() != null){
	      this.campo5  = ValidatorFieldTitulares.validarPaisSucursal(cnmvTitular.getCdPaisSucCliente().toString().trim());			  
			}

      // Tipo persona
			if (cnmvTitular.getTipoPersona() != null){
	      this.campo6 = ValidatorFieldTitulares.validarTipoPersona(cnmvTitular.getTipoPersona().toString().trim());			  
			}

			// Nombre
			if (cnmvTitular.getNombre() != null  ){
			  this.campo7 = ValidatorFieldTitulares.validarNombreApellido(cnmvTitular.getNombre().trim());  
			}
		  
			// Apellidos
			this.campo8 = ValidatorFieldTitulares.validarNombreApellido(ValidatorFieldTitulares.componerApellidos(cnmvTitular.getApellido1(), cnmvTitular.getApellido2()));
			// Fecha de nacimiento
			this.campo9 = ValidatorFieldTitulares.validarFecNac(FormatDataUtils.convertDateToString(cnmvTitular.getfNacimiento(), FormatDataUtils.DATE_FORMAT_1));
			//Identificador responsable decision
			this.campo10= ValidatorFieldTitulares.validarIdRespInversion(cnmvTitular.getIdentificacionResp());
			// Nombre responsable decision
			this.campo11 = ValidatorFieldTitulares.validarNombreApellido(cnmvTitular.getNombreResp());
	     // Apellisdos responsable decision
			this.campo12 = ValidatorFieldTitulares.validarNombreApellido(ValidatorFieldTitulares.componerApellidos(cnmvTitular.getApellido1Resp(), cnmvTitular.getApellido2Resp()));
	     // Fecha de nacimiento responsable decision 
			this.campo13 = ValidatorFieldTitulares.validarFecNac(FormatDataUtils.convertDateToString(cnmvTitular.getfNacimientoResp(), FormatDataUtils.DATE_FORMAT_1));

      // Tipo de identificador 
      if (cnmvTitular.getTpidenti() != null){			
        this.campo24 = ValidatorFieldTitulares.validarTipoIdentificacionResponsable(cnmvTitular.getTpidenti().toString().trim());        
      }

      // Tipo de identificador   responsable decision      
			if (cnmvTitular.getTpidentiResp() != null){
	      this.campo25 = ValidatorFieldTitulares.validarTipoIdentificacionResponsable(cnmvTitular.getTpidentiResp().toString().trim());        
      }


			
		}
		
		if(entDesglose.getEjecucion().getSentido().equals('V')) {
      //Identificador
		  if (cnmvTitular.getIdentificacion() != null){
		    this.campo14 = ValidatorFieldTitulares.validarIdentificacion(cnmvTitular.getIdentificacion().trim());  
		  }
      
      
      // Pais de la sucursal
      if (cnmvTitular.getCdPaisSucCliente() != null){
        this.campo15  = ValidatorFieldTitulares.validarPaisSucursal(cnmvTitular.getCdPaisSucCliente().toString());       
      }

      // Tipo persona
      
      if (cnmvTitular.getTipoPersona() != null){
        this.campo16 = ValidatorFieldTitulares.validarTipoPersona(cnmvTitular.getTipoPersona().toString());        
      }

      // Nombre
      if (cnmvTitular.getNombre() != null  ){
        this.campo17 = ValidatorFieldTitulares.validarNombreApellido(cnmvTitular.getNombre().trim());  
      }
      
      // Apellidos
      this.campo18 = ValidatorFieldTitulares.validarNombreApellido(ValidatorFieldTitulares.componerApellidos(cnmvTitular.getApellido1(), cnmvTitular.getApellido2()));
      // Fecha de nacimiento
      this.campo19 = ValidatorFieldTitulares.validarFecNac(FormatDataUtils.convertDateToString(cnmvTitular.getfNacimiento(), FormatDataUtils.DATE_FORMAT_1));
      //Identificador responsable decision
      this.campo20= ValidatorFieldTitulares.validarIdRespInversion(cnmvTitular.getIdentificacionResp());
      // Nombre responsable decision
      this.campo21 = ValidatorFieldTitulares.validarNombreApellido(cnmvTitular.getNombreResp());
       // Apellisdos responsable decision
      this.campo22 = ValidatorFieldTitulares.validarNombreApellido(ValidatorFieldTitulares.componerApellidos(cnmvTitular.getApellido1Resp(), cnmvTitular.getApellido2Resp()));
       // Fecha de nacimiento responsable decision 
      this.campo23 = ValidatorFieldTitulares.validarFecNac(FormatDataUtils.convertDateToString(cnmvTitular.getfNacimientoResp(), FormatDataUtils.DATE_FORMAT_1));

      // Tipo de identificador 
      if (cnmvTitular.getTpidenti() != null){     
        this.campo26 = ValidatorFieldTitulares.validarTipoIdentificacionResponsable(cnmvTitular.getTpidenti().toString());        
      }

      // Tipo de identificador   responsable decision      
      if (cnmvTitular.getTpidentiResp() != null){
        this.campo27 = ValidatorFieldTitulares.validarTipoIdentificacionResponsable(cnmvTitular.getTpidentiResp().toString());        
      }

		}

	}
	
	/* Constructor  - Primer Registro inicial extra para los titulares de un desglose - Se rellena con los datos a partir del desglose */
	public TitularesTrRecordBean(Tmct0CnmvDesgloses entDesglose) {

		this.campo1 = ValidatorFieldTitulares.validarTipoRegistro(Constantes.TIPO_REGISTRO_TITULARES);
		
		if (entDesglose.getRefOperacion() != null){
		  this.campo2 = ValidatorFieldTitulares.validarRefOperacion(entDesglose.getRefOperacion().trim());  
		}
		
		this.campo3 = 1;
		
		// Identificacion del comprador
		if(entDesglose.getEjecucion().getSentido().equals('V')  && entDesglose.getEjecucion().getIdEntContrapartida() != null  ) {
			this.campo4 = ValidatorFieldTitulares.validarIdentificacion(entDesglose.getEjecucion().getIdEntContrapartida().trim());
		}else {
			this.campo4 = "";
		}

		// Campos 5-13 Vacios
		
		
    // Identificacion del vendedor
		if(entDesglose.getEjecucion().getSentido().equals('C')  && entDesglose.getEjecucion().getIdEntContrapartida() != null ) {
			this.campo14 = ValidatorFieldTitulares.validarIdentificacion(entDesglose.getEjecucion().getIdEntContrapartida().trim());
		}else {
			this.campo14 = "";
		}

    // Campos 15-23 Vacios
		
		// Campo 24 tipo identificador del comprador.
    if(entDesglose.getEjecucion().getSentido().equals('V')  && entDesglose.getEjecucion().getTipoIdEntContrapartida() != null ) {
      this.campo24 = ValidatorFieldTitulares.validarTipoIdentificacion(entDesglose.getEjecucion().getTipoIdEntContrapartida().trim());
    }else {
      this.campo24 = "";
    }
		
    // Campo 25 Tipo de indentificacion del responsable del comprador  Vacio.
    

    // Campo 26 tipo identificador del vendedor.
    if(entDesglose.getEjecucion().getSentido().equals('C')  && entDesglose.getEjecucion().getTipoIdEntContrapartida() != null ) {
      this.campo26 = ValidatorFieldTitulares.validarTipoIdentificacion(entDesglose.getEjecucion().getTipoIdEntContrapartida().trim());
    }else {
      this.campo26 = "";
    }
    
    // Campo 27 Tipo de indentificacion del responsable del vendedor  Vacio.

	}
	
	
  /* Constructor  - Titulares primer registro extra  - Se rellena con los datos a partir de la ejecucion */
  public TitularesTrRecordBean(Tmct0CnmvEjecuciones entEjecucion) {

    this.campo1 = ValidatorFieldTitulares.validarTipoRegistro(Constantes.TIPO_REGISTRO_TITULARES);
    
    
    if ( entEjecucion.getRefOperacion() != null){
      this.campo2 = ValidatorFieldTitulares.validarRefOperacion(entEjecucion.getRefOperacion().trim());      
    }    
    

    this.campo3 = 1;
    
    // Identificacion del comprador  // parametro
    if(entEjecucion.getSentido().equals('V') && entEjecucion.getIdEntContrapartida() != null) {
      this.campo4 = ValidatorFieldTitulares.validarIdentificacion(entEjecucion.getIdEntContrapartida().trim());
    }else {
      this.campo4 = "";
    }

    // Campos 5-13 Vacios
    
    
    // Identificacion del vendedor
    if(entEjecucion.getSentido().equals('C') && entEjecucion.getIdEntContrapartida() != null) {
      this.campo14 = ValidatorFieldTitulares.validarIdentificacion(entEjecucion.getIdEntContrapartida().trim());
    }else {
      this.campo14 = "";
    }

    // Campos 15-23 Vacios
    
    // Campo 24 tipo identificador del comprador.
    if(entEjecucion.getSentido().equals('V')&& entEjecucion.getTipoIdEntContrapartida() != null) {
      this.campo24 = ValidatorFieldTitulares.validarTipoIdentificacion(entEjecucion.getTipoIdEntContrapartida().trim());
    }else {
      this.campo24 = "";
    }
    
    // Campo 25 Tipo de indentificacion del responsable del comprador  Vacio.
    

    // Campo 26 tipo identificador del vendedor.
    if(entEjecucion.getSentido().equals('C') && entEjecucion.getTipoIdEntContrapartida() != null) {
      this.campo26 = ValidatorFieldTitulares.validarTipoIdentificacion(entEjecucion.getTipoIdEntContrapartida().trim());
    }else {
      this.campo26 = "";
    }
    
    // Campo 27 Tipo de indentificacion del responsable del vendedor  Vacio.

  }
	
  
  /* Constructor  - Titulares Segundo registro extra  - Se rellena con los datos a partir de la ejecucion y la cuenta agregada tr */
  public TitularesTrRecordBean(Tmct0CnmvEjecuciones entEjecucion, String paramCuentaAgredada,  String paramTipoCuentaAgreda) {

    this.campo1 = ValidatorFieldTitulares.validarTipoRegistro(Constantes.TIPO_REGISTRO_TITULARES);
    
    if (entEjecucion.getRefOperacion() != null){
      this.campo2 = ValidatorFieldTitulares.validarRefOperacion(entEjecucion.getRefOperacion().trim());  
    }
    
    this.campo3 = 2;
    
    // Identificacion del comprador  // parametro
    if(entEjecucion.getSentido().equals('V')) {
      this.campo4 = paramCuentaAgredada;
    }else {
      this.campo4 = "";
    }

    // Campos 5-13 Vacios
    
    
    // Identificacion del vendedor
    if(entEjecucion.getSentido().equals('C')) {
      this.campo14 = paramCuentaAgredada;
    }else {
      this.campo14 = "";
    }

    // Campos 15-23 Vacios
    
    // Campo 24 tipo identificador del comprador.
    if(entEjecucion.getSentido().equals('V')) {
      this.campo24 = paramTipoCuentaAgreda;
    }else {
      this.campo24 = "";
    }
    
    // Campo 25 Tipo de indentificacion del responsable del comprador  Vacio.
    

    // Campo 26 tipo identificador del vendedor.
    if(entEjecucion.getSentido().equals('C')) {
      this.campo26 = paramTipoCuentaAgreda;
    }else {
      this.campo26 = "";
    }
    
    // Campo 27 Tipo de indentificacion del responsable del vendedor  Vacio.

  }  

  /**
   * @return the campo1
   */
  public String getCampo1() {
    return campo1;
  }

  /**
   * @param campo1 the campo1 to set
   */
  public void setCampo1(String campo1) {
    this.campo1 = campo1;
  }

  /**
   * @return the campo2
   */
  public String getCampo2() {
    return campo2;
  }

  /**
   * @param campo2 the campo2 to set
   */
  public void setCampo2(String campo2) {
    this.campo2 = campo2;
  }

  /**
   * @return the campo3
   */
  public int getCampo3() {
    return campo3;
  }

  /**
   * @param campo3 the campo3 to set
   */
  public void setCampo3(int campo3) {
    this.campo3 = campo3;
  }

  /**
   * @return the campo4
   */
  public String getCampo4() {
    return campo4;
  }

  /**
   * @param campo4 the campo4 to set
   */
  public void setCampo4(String campo4) {
    this.campo4 = campo4;
  }

  /**
   * @return the campo5
   */
  public String getCampo5() {
    return campo5;
  }

  /**
   * @param campo5 the campo5 to set
   */
  public void setCampo5(String campo5) {
    this.campo5 = campo5;
  }

  /**
   * @return the campo6
   */
  public String getCampo6() {
    return campo6;
  }

  /**
   * @param campo6 the campo6 to set
   */
  public void setCampo6(String campo6) {
    this.campo6 = campo6;
  }

  /**
   * @return the campo7
   */
  public String getCampo7() {
    return campo7;
  }

  /**
   * @param campo7 the campo7 to set
   */
  public void setCampo7(String campo7) {
    this.campo7 = campo7;
  }

  /**
   * @return the campo8
   */
  public String getCampo8() {
    return campo8;
  }

  /**
   * @param campo8 the campo8 to set
   */
  public void setCampo8(String campo8) {
    this.campo8 = campo8;
  }

  /**
   * @return the campo9
   */
  public String getCampo9() {
    return campo9;
  }

  /**
   * @param campo9 the campo9 to set
   */
  public void setCampo9(String campo9) {
    this.campo9 = campo9;
  }

  /**
   * @return the campo10
   */
  public String getCampo10() {
    return campo10;
  }

  /**
   * @param campo10 the campo10 to set
   */
  public void setCampo10(String campo10) {
    this.campo10 = campo10;
  }

  /**
   * @return the campo11
   */
  public String getCampo11() {
    return campo11;
  }

  /**
   * @param campo11 the campo11 to set
   */
  public void setCampo11(String campo11) {
    this.campo11 = campo11;
  }

  /**
   * @return the campo12
   */
  public String getCampo12() {
    return campo12;
  }

  /**
   * @param campo12 the campo12 to set
   */
  public void setCampo12(String campo12) {
    this.campo12 = campo12;
  }

  /**
   * @return the campo13
   */
  public String getCampo13() {
    return campo13;
  }

  /**
   * @param campo13 the campo13 to set
   */
  public void setCampo13(String campo13) {
    this.campo13 = campo13;
  }

  /**
   * @return the campo14
   */
  public String getCampo14() {
    return campo14;
  }

  /**
   * @param campo14 the campo14 to set
   */
  public void setCampo14(String campo14) {
    this.campo14 = campo14;
  }

  /**
   * @return the campo15
   */
  public String getCampo15() {
    return campo15;
  }

  /**
   * @param campo15 the campo15 to set
   */
  public void setCampo15(String campo15) {
    this.campo15 = campo15;
  }

  /**
   * @return the campo16
   */
  public String getCampo16() {
    return campo16;
  }

  /**
   * @param campo16 the campo16 to set
   */
  public void setCampo16(String campo16) {
    this.campo16 = campo16;
  }

  /**
   * @return the campo17
   */
  public String getCampo17() {
    return campo17;
  }

  /**
   * @param campo17 the campo17 to set
   */
  public void setCampo17(String campo17) {
    this.campo17 = campo17;
  }

  /**
   * @return the campo18
   */
  public String getCampo18() {
    return campo18;
  }

  /**
   * @param campo18 the campo18 to set
   */
  public void setCampo18(String campo18) {
    this.campo18 = campo18;
  }

  /**
   * @return the campo19
   */
  public String getCampo19() {
    return campo19;
  }

  /**
   * @param campo19 the campo19 to set
   */
  public void setCampo19(String campo19) {
    this.campo19 = campo19;
  }

  /**
   * @return the campo20
   */
  public String getCampo20() {
    return campo20;
  }

  /**
   * @param campo20 the campo20 to set
   */
  public void setCampo20(String campo20) {
    this.campo20 = campo20;
  }

  /**
   * @return the campo21
   */
  public String getCampo21() {
    return campo21;
  }

  /**
   * @param campo21 the campo21 to set
   */
  public void setCampo21(String campo21) {
    this.campo21 = campo21;
  }

  /**
   * @return the campo22
   */
  public String getCampo22() {
    return campo22;
  }

  /**
   * @param campo22 the campo22 to set
   */
  public void setCampo22(String campo22) {
    this.campo22 = campo22;
  }

  /**
   * @return the campo23
   */
  public String getCampo23() {
    return campo23;
  }

  /**
   * @param campo23 the campo23 to set
   */
  public void setCampo23(String campo23) {
    this.campo23 = campo23;
  }

  /**
   * @return the campo24
   */
  public String getCampo24() {
    return campo24;
  }

  /**
   * @param campo24 the campo24 to set
   */
  public void setCampo24(String campo24) {
    this.campo24 = campo24;
  }

  /**
   * @return the campo25
   */
  public String getCampo25() {
    return campo25;
  }

  /**
   * @param campo25 the campo25 to set
   */
  public void setCampo25(String campo25) {
    this.campo25 = campo25;
  }

  /**
   * @return the campo26
   */
  public String getCampo26() {
    return campo26;
  }

  /**
   * @param campo26 the campo26 to set
   */
  public void setCampo26(String campo26) {
    this.campo26 = campo26;
  }

  /**
   * @return the campo27
   */
  public String getCampo27() {
    return campo27;
  }

  /**
   * @param campo27 the campo27 to set
   */
  public void setCampo27(String campo27) {
    this.campo27 = campo27;
  }

	
	
}
