package sibbac.business.fallidos.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.Tmct0CnmvEjecuciones;

@Repository
public interface Tmct0CnmvEjecucionesDao extends JpaRepository<Tmct0CnmvEjecuciones, BigInteger> {

  @Query(" SELECT eje FROM Tmct0CnmvEjecuciones eje WHERE eje.idEjecucion = :idEjecucion AND eje.fEjecucion = :fEjecucion AND eje.sistemaNegociacion = :sistemaNegociacion  AND eje.sentido = :sentido and eje.estadoOperacion = 'A' ")
  Tmct0CnmvEjecuciones findByIdEjecucion(@Param("idEjecucion") String idEjecucion,
      @Param("fEjecucion") Date fEjecucion, @Param("sistemaNegociacion") String sistemaNegociacion,
      @Param("sentido") Character sentido);

  @Query(" SELECT eje FROM Tmct0CnmvEjecuciones eje "
      + "    where COALESCE( eje.cdEstadoEnvio.idestado , 0 ) < :estadoEnvio  and  eje.titulosEjecucion = eje.titulosDesgloses and eje.estadoOperacion != 'X'")
  List<Tmct0CnmvEjecuciones> findPendientesActualizar(@Param("estadoEnvio") Integer estadoEnvio);

  @Query(" SELECT eje FROM Tmct0CnmvEjecuciones eje "
      + "    where  eje.cdEstadoEnvio.idestado = :estadoEnvio  and eje.estadoOperacion != 'X'  AND  eje.sistemaNegociacion NOT IN (:camaras) AND eje.refOperacion IS NULL ")
  List<Tmct0CnmvEjecuciones> findPendientesEnviar(@Param("estadoEnvio") Integer estadoEnvio,
      @Param("camaras") List<String> camaras);

  // Operaciones TR MiFIDII Pendientes de enviar con refOperacion no nula las
  // otras son de H47 y el numero de titulos de la ejecucion es igual al de los
  // desgloses
  @Query(" SELECT eje FROM Tmct0CnmvEjecuciones eje "
      + " WHERE eje.estadoOperacion != 'X' AND eje.cdEstadoEnvio.idestado = :estado  AND eje.refOperacion IS NOT NULL AND  eje.titulosEjecucion = eje.titulosDesgloses ")
  List<Tmct0CnmvEjecuciones> findOperacionesTr(@Param("estado") Integer estado, Pageable page);

  // Operaciones TR MiFIDII Pendientes de enviar con refOperacion no nula las
  // otras son de H47 y el numero de titulos de la ejecucion es igual al de los
  // desgloses
  @Query(" SELECT eje.id FROM Tmct0CnmvEjecuciones eje "
      + " WHERE eje.estadoOperacion != 'X' AND eje.cdEstadoEnvio.idestado = :estado  AND eje.refOperacion IS NOT NULL AND  eje.titulosEjecucion = eje.titulosDesgloses ")
  List<BigInteger> findAllIdsOperacionesTr(@Param("estado") Integer estado, Pageable page);

  @Query(" SELECT eje FROM Tmct0CnmvEjecuciones eje ")
  public List<Tmct0CnmvEjecuciones> findOperacionesTr();

}
