package sibbac.business.fallidos.threads;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fallidos.database.bo.OperacionesH47MtfBo;




/**
 * Representa un hilo que procesa las Operaciones MTF de un dia en concreto
 * 
 * @author Neoris
 *
 */
public class ProcesaOperacionesMTFDiaRunnable implements Runnable {

	  /** The Constant LOG. */
	  private static final Logger LOG = LoggerFactory.getLogger(ProcesaOperacionesMTFDiaRunnable.class);
	
	  // Bo con la logica de las operaciones
	  private OperacionesH47MtfBo generarAutoH47MtfBo = null;
	  
	  private Date fechaEjecucion = null;
	  
	  
	// Constructor  
	public ProcesaOperacionesMTFDiaRunnable(
			OperacionesH47MtfBo generarAutoH47MtfBo, Date fechaEjecucion) {
		this.generarAutoH47MtfBo = generarAutoH47MtfBo;
		this.fechaEjecucion = fechaEjecucion;
	}




	@Override
	public void run() {
		generarAutoH47MtfBo.generaOperacionesDiaMTFs(fechaEjecucion);
	}



}
