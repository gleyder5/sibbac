package sibbac.business.fallidos.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fallidos.database.model.Tmct0CnmvDesgloses;
import sibbac.business.fallidos.dto.OperacionMtfDTO;
import sibbac.business.fallidos.dto.OperacionTrDTO;

@Repository
public interface Tmct0CnmvDesglosesDao extends JpaRepository<Tmct0CnmvDesgloses, BigInteger> {

  @Query("SELECT de from Tmct0CnmvDesgloses de WHERE de.ejecucion.id = :idEjecucion")
  public List<Tmct0CnmvDesgloses> findByEjecucion(@Param("idEjecucion") BigInteger idEjecucion);

  @Query("SELECT de from Tmct0CnmvDesgloses de WHERE de.ejecucion.id = :idEjecucion and de.estadoDesglose = 'A'")
  public List<Tmct0CnmvDesgloses> findActivosByEjecucion(@Param("idEjecucion") BigInteger idEjecucion);

  // Desgloses de una ejecucion pendientes de enviar ordenados por el campo
  // estadoDesglose para procesar primero las bajas. Se excluyen los que el
  // estado es X
  @Query("SELECT de from Tmct0CnmvDesgloses de WHERE de.ejecucion.id = :idEjecucion and de.estadoDesglose <> 'X' and de.cdEstadoEnvio = :estadoEnvio  order by de.estadoDesglose DESC ")
  public List<Tmct0CnmvDesgloses> findPendientesEnvioByEjecucion(@Param("idEjecucion") BigInteger idEjecucion,
      @Param("estadoEnvio") Integer estadoEnvio, Pageable page);

  @Query(value = " SELECT  alc.nuorden,    alc.nbooking,    alc.nucnfclt,    alc.nucnfliq,    alc.feejeliq,    dc.feliquidacion,    alc.cdestadoasig,  CASE cli.cdsentido  WHEN 'C' THEN 'B' WHEN 'V' THEN 'S'   END AS sentido  ,  "
      + " cli.cd_plataforma_negociacion,    cli.nurefexemkt,       dc.isin,    cli.imtitulos,    cli.imprecio,    cli.imefectivo,    cli.cddivisa,    dc.iddesglosecamara  "
      + " FROM  BSNBPSQL.TMCT0alc alc,    TMCT0DESGLOSECAMARA dc,    TMCT0_CAMARA_COMPENSACION cc,    TMCT0DESGLOSEcliTIT cli "
      + " WHERE   dc.NUORDEN = alc.NUORDEN "
      + "   AND   dc.NBOOKING = alc.NBOOKING "
      + "   AND   dc.NUCNFCLT = alc.NUCNFCLT "
      + "   AND   dc.NUCNFLIQ = alc.NUCNFLIQ "
      + "   AND   cc.ID_CAMARA_COMPENSACION = dc.IDCAMARA "
      + "   AND   cc.CD_CODIGO IN (:camaras) "
      + "   AND   alc.FEEJELIQ = :feejeliq "
      + "   AND   cli.IDDESGLOSECAMARA = dc.IDDESGLOSECAMARA "
      + "   AND EXISTS (SELECT 1  FROM TMCT0AFI AFI	WHERE AFI.NUORDEN = ALC.NUORDEN AND AFI.NBOOKING = ALC.NBOOKING AND AFI.NUCNFCLT = ALC.NUCNFCLT AND AFI.NUCNFLIQ = ALC.NUCNFLIQ AND AFI.CDINACTI = 'A' ) "
      + "  ORDER BY alc.NUORDEN,    alc.NBOOKING,    alc.NUCNFCLT,  alc.NUCNFLIQ, cli.CD_PLATAFORMA_NEGOCIACION, cli.NUREFEXEMKT  ", nativeQuery = true)
  List<Object[]> findDesglosesProcesoAutomaticoMTFs(@Param("feejeliq") Date feejeliq,
      @Param("camaras") List<String> camaras);

  @Query(" SELECT new sibbac.business.fallidos.dto.OperacionMtfDTO  ( alc.id.nuorden,    alc.id.nbooking,    alc.id.nucnfclt,    alc.id.nucnfliq,    cli.feejecuc,  cli.hoejecuc,  dc.feliquidacion,    alc.cdestadoasig,  cli.cdsentido,  "
      + " cli.cdPlataformaNegociacion,    cli.nurefexemkt,       dc.isin,    cli.imtitulos,    cli.imprecio,    cli.imefectivo,    cli.cddivisa,    dc.iddesglosecamara)"
      + " FROM  Tmct0alc alc,    Tmct0desglosecamara dc,    Tmct0CamaraCompensacion cc,    Tmct0desgloseclitit cli "
      + " WHERE  dc.tmct0alc = alc "
      + "   AND  dc.tmct0CamaraCompensacion = cc "
      + "   AND  cc.cdCodigo IN (:camaras) "
      + "   AND  alc.feejeliq = :feejeliq "
      + "   AND  cli.tmct0desglosecamara = dc "
      + "   AND EXISTS (SELECT afi.id FROM Tmct0afi afi WHERE afi.id.nuorden = alc.id.nuorden AND afi.id.nbooking = alc.id.nbooking AND afi.id.nucnfclt = alc.id.nucnfclt AND afi.id.nucnfliq = alc.id.nucnfliq AND afi.cdinacti = 'A' ) ")
  List<OperacionMtfDTO> findDesglosesProcesoAutomaticoMTFsDtos(@Param("feejeliq") Date feejeliq,
      @Param("camaras") List<String> camaras);

  @Query(" SELECT new sibbac.business.fallidos.dto.OperacionTrDTO  ( alc.id.nuorden,    alc.id.nbooking,    alc.id.nucnfclt,    alc.id.nucnfliq,    cli.feejecuc,  cli.hoejecuc,  dc.feliquidacion,    alc.cdestadoasig,  cli.cdsentido,  "
      + " cli.cdPlataformaNegociacion,    cli.nurefexemkt,       dc.isin,    cli.imtitulos,    cli.imprecio,    cli.imefectivo,    cli.cddivisa,    dc.iddesglosecamara, dc.estadoCNMV, eje.fechaHoraEjec, dc.tmct0CamaraCompensacion.codigoLei)"
      + " FROM  Tmct0alc alc,    Tmct0desglosecamara dc,   Tmct0desgloseclitit cli, Tmct0ord ord, Tmct0eje eje "
      + " WHERE  dc.tmct0alc = alc "
      + "   AND  ( dc.estadoCNMV IS NULL  OR dc.estadoCNMV <> 9 ) "
      + "   AND  dc.fecontratacion = :fecontratacion "
      + "   AND  cli.tmct0desglosecamara = dc "
      + " AND alc.id.nuorden = ord.nuorden AND ord.isscrdiv = :isscrdiv "
      + " AND ord.nuorden = eje.id.nuorden AND alc.id.nbooking = eje.id.nbooking "
      + " AND cli.nuejecuc = eje.id.nuejecuc ")
  List<OperacionTrDTO> findDesglosesProcesoAutomaticoRtDtos(@Param("fecontratacion") Date feejeliq,
      @Param("isscrdiv") char isscrdiv, Pageable page);

  @Query(" SELECT des  FROM Tmct0CnmvDesgloses des WHERE des.nuorden = :nuorden AND des.nbooking = :nbooking  AND des.nucnfclt = :nucnfclt  AND des.nucnfliq = :nucnfliq AND des.idDesgloseCamara = :idDesgloseCamara AND des.estadoDesglose = 'A' ")
  public Tmct0CnmvDesgloses findByIdAlc(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq,
      @Param("idDesgloseCamara") Long idDesgloseCamara);

  @Query(" SELECT des  FROM Tmct0CnmvDesgloses des WHERE des.nuorden = :nuorden AND des.nbooking = :nbooking  AND des.nucnfclt = :nucnfclt  AND des.nucnfliq = :nucnfliq AND des.idDesgloseCamara = :idDesgloseCamara AND des.estadoDesglose = 'A' ")
  public Tmct0CnmvDesgloses findByIdAlc2(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq,
      @Param("idDesgloseCamara") Long idDesgloseCamara);

  @Query(" SELECT des.id  FROM Tmct0CnmvDesgloses des WHERE des.nuorden = :nuorden AND des.nbooking = :nbooking  AND des.nucnfclt = :nucnfclt  AND des.nucnfliq = :nucnfliq AND des.idDesgloseCamara = :idDesgloseCamara AND des.estadoDesglose = 'A' AND des.ejecucion.id = :idRegEjecucion ")
  public BigInteger findIdByDatosAlc(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq,
      @Param("idDesgloseCamara") Long idDesgloseCamara, @Param("idRegEjecucion") BigInteger idRegEjecucion);

  @Query(" SELECT des.id  FROM Tmct0CnmvDesgloses des WHERE des.nuorden = :nuorden AND des.nbooking = :nbooking  AND des.nucnfclt = :nucnfclt  AND des.nucnfliq = :nucnfliq AND des.idDesgloseCamara = :idDesgloseCamara AND des.estadoDesglose = 'B' AND des.ejecucion.id = :idRegEjecucion  AND des.cdEstadoEnvio = :estadoPte ")
  public BigInteger findIdByDatosAlcDeBajaPte(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq,
      @Param("idDesgloseCamara") Long idDesgloseCamara, @Param("idRegEjecucion") BigInteger idRegEjecucion,
      @Param("estadoPte") Integer estadoPte);

  // @Query(
  // " SELECT alc.id.nuorden,    alc.id.nbooking,    alc.id.nucnfclt,    alc.id.nucnfliq,    alc.feejeliq,    dc.feliquidacion,    alc.cdestadoasig,    dc.sentido,  "
  // +
  // " cli.cdPlataformaNegociacion,    cli.nurefexemkt,       dc.isin,    cli.imtitulos,    cli.imprecio,    cli.imefectivo,    cli.cddivisa,    dc.iddesglosecamara"
  // +
  // " FROM  Tmct0alc alc,    Tmct0desglosecamara dc,    Tmct0CamaraCompensacion cc,    Tmct0desgloseclitit cli "
  // +
  // " WHERE  dc.tmct0alc = alc " +
  // "   AND  dc.tmct0CamaraCompensacion = cc " +
  // "   AND  cc.cdCodigo IN (:camaras) " +
  // "   AND  alc.feejeliq = :feejeliq " +
  // "   AND  cli.tmct0desglosecamara = dc " +
  // "   AND  EXISTS (SELECT afi.id FROM Tmct0afi afi WHERE afi.id.nuorden = alc.id.nuorden AND afi.id.nbooking = alc.id.nbooking AND afi.id.nucnfclt = alc.id.nucnfclt AND afi.id.nucnfliq = alc.id.nucnfliq AND afi.cdinacti = 'A' ) ")
  // List<Object[]> findDesglosesProcesoAutomaticoMTFsModel (@Param("feejeliq")
  // Date feejeliq,
  // @Param("camaras") List<String> camaras);

}
