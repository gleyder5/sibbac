package sibbac.business.fallidos.database.bo;


import sibbac.tasks.database.JobPK;


public interface AjustesFixmlFileService {

	public void processAjustesFixmlFiles( JobPK jobPk );

}
