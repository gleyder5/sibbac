package sibbac.business.fallidos.database.bo;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.FallidoDao;
import sibbac.business.fallidos.database.dao.InstruccionLiquidacionFallidaDao;
import sibbac.business.fallidos.database.model.Fallido;
import sibbac.business.fallidos.database.model.InstruccionLiquidacionFallida;
import sibbac.business.fallidos.utils.Constantes;
import sibbac.business.fixml.database.dao.FixmlFileDao;
import sibbac.database.bo.AbstractBo;
import sibbac.fixml.classes.AbstractMessageT;
import sibbac.fixml.classes.BMEStipulationTypeEnumT;
import sibbac.fixml.classes.PartiesBlockT;
import sibbac.fixml.classes.StipulationsBlockT;
import sibbac.fixml.classes.TradeCaptureReportMessageT;
import sibbac.fixml.classes.TrdCapRptSideGrpBlockT;

@Service
public class InstruccionLiquidacionFallidaBo extends
                                            AbstractBo<InstruccionLiquidacionFallida, Long, InstruccionLiquidacionFallidaDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(InstruccionLiquidacionFallidaBo.class);

  private static final BigInteger RPTSIDEPTYR_CC_INTEGER = new BigInteger("38");

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0_OPERACION_FALLIDO</code>. */
  @Autowired
  private FallidoDao fallidoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_FIXMLFILES</code>. */
  @Autowired
  private FixmlFileDao fixmlFileDao;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Procesa las entradas del los ficheros <code>CPENDINGINST</code> que contienen las instrucciones de liquidación
   * pendientes de liquidar.
   * 
   * @param lm Lista de JAXB de un fichero CPENDINGINST.C0.XML.
   * @param zip Fichero ZIP que contiene el XML a tratar.
   * @param tipoFichero Tipo de fichero XML a tratar.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void procesarCPENDINGINST(List<JAXBElement<? extends AbstractMessageT>> lm, String zip, String tipoFichero) {
    LOG.debug("[procesarCPENDINGINST] Inicio ...");

    TradeCaptureReportMessageT am = null;

    String trdId = null;
    Date settlDt = null;
    String isin = null;
    String codigoCuentaCompensacion = null;
    String iLiqDcv = null;

    InstruccionLiquidacionFallida instruccionLiquidacionFallida = null;
    List<Fallido> ofList = null;

    // Se recorre la lista generando y persistiendo las instrucciones necesarias
    for (JAXBElement<? extends AbstractMessageT> b : lm) {
      am = (TradeCaptureReportMessageT) b.getValue();

      trdId = am.getTrdID();
      settlDt = am.getSettlDt().toGregorianCalendar().getTime();
      isin = am.getInstrmt().getID();

      if (am.getExchTrdTyp().compareTo(Constantes.FIXML_GENERACION_IL) == 0) {

        for (TrdCapRptSideGrpBlockT rptSide : am.getRptSide()) {
          // LOG.debug("[procesarCPENDINGINST] TrdCapRptSideGrpBlockT rptSide ...");

          for (PartiesBlockT pty : rptSide.getPty()) {
            // LOG.debug("[procesarCPENDINGINST] PartiesBlockT pty ...");
            if (pty.getR().compareTo(RPTSIDEPTYR_CC_INTEGER) == 0) {
              codigoCuentaCompensacion = pty.getID();
            } // if
          } // for (PartiesBlockT pty : rptSide.getPty())

          for (StipulationsBlockT stip : rptSide.getStip()) {
            // LOG.debug("[procesarCPENDINGINST] StipulationsBlockT stip ...");
            if (stip.getTyp().compareTo(BMEStipulationTypeEnumT.ILIQDCV) == 0) {
              iLiqDcv = stip.getVal();
              break;
            } // if
          } // for (StipulationsBlockT stip : rptSide.getStip())
        } // for (TrdCapRptSideGrpBlockT rptSide : am.getRptSide())

        LOG.debug("[procesarCPENDINGINST] Buscando ventas fallidas para settlDt: '{}', isin: '{}', codigoCuentaCompensacion: '{}'",
                  settlDt, isin, codigoCuentaCompensacion);
        ofList = fallidoDao.findByFeopeliqIsinAndCC(settlDt, isin, codigoCuentaCompensacion);

        if (ofList.isEmpty()) {
          LOG.debug("[procesarCPENDINGINST] La ILIQDCV no es fallida");
        } else {
          LOG.debug("[procesarCPENDINGINST] La ILIQDCV es fallida ... Comprobando si existe ILIQECC en el sistema ...");
          instruccionLiquidacionFallida = dao.findByNumeroOperacionEcc(am.getTrdID());

          if (instruccionLiquidacionFallida == null) {
            LOG.debug("[procesarCPENDINGINST] No se ha encontrado la ILIQECC fallida ... Insertando ...");
            instruccionLiquidacionFallida = new InstruccionLiquidacionFallida();

            // RAIZ
            instruccionLiquidacionFallida.setNumeroOperacionEcc(am.getTrdID()); // Identificador de registro de la ECC -
                                                                                // 3
            instruccionLiquidacionFallida.setTipoOperacionEcc(am.getExchTrdTyp()); // Tipo de operación de la ECC
            instruccionLiquidacionFallida.setVolumenInstruccion(am.getLastQty()); // Volumen de la instrucción
            instruccionLiquidacionFallida.setVolumenVivoInstruccion(am.getLeavesQty()); // Volumen vivo de la
                                                                                        // instrucción
            instruccionLiquidacionFallida.setPrecioInstruccion(am.getLastPx()); // Precio del valor
            instruccionLiquidacionFallida.setDivisa(am.getCcy()); // Código de divisa
            instruccionLiquidacionFallida.setFechaLiquidacion(am.getSettlDt().toGregorianCalendar().getTime()); // FTL
            instruccionLiquidacionFallida.setImporteEfectivoInstruccion(am.getGrossTrdAmt()); // Importe Efectivo

            // HDR
            instruccionLiquidacionFallida.setTipoMensaje(am.getHdr().getMsgTyp()); // Tipo de mensaje
            instruccionLiquidacionFallida.setIdEntidadMensaje(am.getHdr().getSID()); // Entidad que envía el mensaje
            instruccionLiquidacionFallida.setSegmento(am.getHdr().getSSub()); // Segmento de la ECC

            // INSTRMNT
            instruccionLiquidacionFallida.setIsin(am.getInstrmt().getID()); // ISIN

            // AMT
            instruccionLiquidacionFallida.setImporteEfectivoVivoOperacion(am.getAmt().get(0).getAmt());

            // RPTSIDE
            instruccionLiquidacionFallida.setSentido(am.getRptSide().get(0).getSide()); // Sentido
            instruccionLiquidacionFallida.setIndicadorPosicion(am.getRptSide().get(0).getPosEfct().value()); // Tipo

            dao.save(instruccionLiquidacionFallida);

            // Se actualiza la operación fallida con el número de instrucción de liquidación de la ECC
            LOG.debug("[procesarCPENDINGINST] Actualizando campo ILIQECC de las operaciones fallidas relacionadas");
            for (Fallido f : ofList) {
              f.setiLiqEcc(trdId);
              f.setiLiqDcv(iLiqDcv);
            }
            fallidoDao.save(ofList);
            LOG.debug("[procesarCPENDINGINST] Operaciones fallidas actualizadas corectamente");

          } else {
            LOG.debug("[procesarCPENDINGINST] La ILIQECC fallida ya existe ... Se ignora ...");
          } // else
        } // else
      } // if (am.getExchTrdTyp().compareTo(Constantes.FIXML_GENERACION_IL) == 0)
    } // for (JAXBElement<? extends AbstractMessageT> b : lm)

    fixmlFileDao.updateProcesadoByZipAndTipo(zip, tipoFichero);
    LOG.debug("[procesarCPENDINGINST] Fin ...");
  } // procesarCPENDINGINST

} // InstruccionLiquidacionFallidaBo
