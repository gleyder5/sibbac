package sibbac.business.fallidos.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.dto.CamaraDTO;
import sibbac.business.wrappers.database.dao.Tmct0CamaraCompensacionDao;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * @author XI316153
 */
@SIBBACService
public class SIBBACServiceCamara implements SIBBACServiceBean {

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	/** Referencia para la inserción de logs. */
	protected static final Logger		LOG			= LoggerFactory.getLogger( SIBBACServiceCamara.class );

	/** Comando para el listado de las camaras. */
	private static final String			CMD_LIST	= "listCamara";

	/** Contenido de la respuesta para el comando CMD_LIST. */
	private static final String			RESULT_LIST	= "listadoCamara";

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

	@Autowired
	private Tmct0CamaraCompensacionDao	tmct0CamaraCompensacionDao;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

	/**
	 * Constructor I. Constructor por defecto.
	 */
	public SIBBACServiceCamara() {
	} // SIBBACServiceCamara

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.debug( "[SIBBACServiceCamara :: process] Processing a web request ..." );

		final String command = webRequest.getAction();
		LOG.debug( "[SIBBACServiceCamara :: process] Command: [{}]", command );

		final WebResponse webResponse = new WebResponse();

		List< Tmct0CamaraCompensacion > listaCamaraCompensacion = null;
		List< CamaraDTO > listaCamara = null;

		switch ( command ) {
			case CMD_LIST:
				listaCamaraCompensacion = tmct0CamaraCompensacionDao.findAll();

				final Map< String, List< CamaraDTO >> valores = new HashMap< String, List< CamaraDTO >>();
				listaCamara = new ArrayList< CamaraDTO >();

				for ( Tmct0CamaraCompensacion camara : listaCamaraCompensacion ) {
					listaCamara.add( new CamaraDTO( camara ) );
				} // for

				valores.put( RESULT_LIST, listaCamara );

				webResponse.setResultados( valores );

				break;
			default:
				webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
		} // switch
		return webResponse;
	} // process

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LIST );
		return commands;
	} // getAvailableCommands

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	} // getFields

} // SIBBACServiceCamara
