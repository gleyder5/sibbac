package sibbac.business.fallidos.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import sibbac.business.fallidos.utils.SentidoEcc;

public class AjusteCliente {
	
	private String alias;
	
	private SortedSet<String> subCuentas;
	
	private BigDecimal porcentaje;
	
	private BigDecimal titulos;
	
	private BigDecimal efectivo;
	
	private BigDecimal titulosFallidosCliente;
	
	private BigDecimal titulosFallidosIL;
	
	private SentidoEcc sentido;
	
	private SortedSet<String> holders;
	
	
	public AjusteCliente(){
		subCuentas = new TreeSet<>();
		holders = new TreeSet<>();
		titulos = BigDecimal.ZERO;
		efectivo = BigDecimal.ZERO;
		titulosFallidosCliente = BigDecimal.ZERO;
		titulosFallidosIL = BigDecimal.ZERO;
	}
	
	/**
	 * Total de titulos de fallidos del cliente en un sentido
	 * Se utiliza para calcular el porcentaje del ajuste que le corresponde
	 * @param titulosAdd
	 */
	public void addTtitulosFallidosCliente(BigDecimal titulosAdd){
		titulosFallidosCliente = titulosFallidosCliente.add(titulosAdd);
	}
	
	
	/**
	 * Para calcular el porcentaje que le corresponde al cliente
	 * @return
	 */
	public BigDecimal getPorcentaje(){
		
		if (porcentaje == null){
			
			porcentaje = titulosFallidosCliente.divide(titulosFallidosIL, 6, RoundingMode.HALF_UP);
		}
		
		return porcentaje;
	}
	
	
//	public BigDecimal getPorcentajeNeto(){
//		
//	}
	
	public BigDecimal calculateEfectivoCliente(BigDecimal efectivoAjuste){
		return getPorcentaje().multiply(efectivoAjuste).setScale(2, RoundingMode.HALF_UP);
	}
	
	public BigDecimal calculateTitulosCliente(BigDecimal titulosAjuste){
		return getPorcentaje().multiply(titulosAjuste).setScale(0, RoundingMode.HALF_UP);
	}
	
	// G y S

	public String getAlias() {
		return alias;
	}

	public SortedSet<String> getHolders() {
		return holders;
	}

	public SortedSet<String> getSubCuentas() {
		return subCuentas;
	}

	public BigDecimal getTitulosFallidosCliente() {
		return titulosFallidosCliente;
	}

	public BigDecimal getTitulosFallidosIL() {
		return titulosFallidosIL;
	}
	
	public SentidoEcc getSentido() {
		return sentido;
	}

	///////////////////////////////////////////////
	
	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setSubCuentas(SortedSet<String> subCuentas) {
		this.subCuentas = subCuentas;
	}

	public void setTitulosFallidosCliente(BigDecimal titulosFallidosCliente) {
		this.titulosFallidosCliente = titulosFallidosCliente;
	}

	public void setTitulosFallidosIL(BigDecimal titulosFallidosIL) {
		this.titulosFallidosIL = titulosFallidosIL;
	}	
	
	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public void setHolders(SortedSet<String> holders) {
		this.holders = holders;
	}

	public void setSentido(SentidoEcc sentido) {
		this.sentido = sentido;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setTitulos(BigDecimal titulos) {
		this.titulos = titulos;
	}

	public void setEfectivo(BigDecimal efectivo) {
		this.efectivo = efectivo;
	}

	@Override
	public String toString() {
		return "AjusteCliente [alias=" + alias + ", subCuentas=" + getSetToString(subCuentas, "subCuentas")
				+ ", porcentaje=" + getPorcentaje() + ", titulos=" + titulos
				+ ", efectivo=" + efectivo + ", titulosFallidosCliente="
				+ titulosFallidosCliente + ", titulosFallidosIL="
				+ titulosFallidosIL + ", sentido=" + sentido + ", holders="
				+ getSetToString(holders, "holders")  + "]";
	}
	
	public String getSetToString(Set<String> set, String name){
		StringBuilder result = new StringBuilder(name).append("[");
		for (String value:set){
			result.append(value);
			result.append(",");
		}
		result.append("]");
		return result.toString();
	}
	
	
	
}
