package sibbac.business.fallidos.writers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.bo.OperacionesTrBo;
import sibbac.business.fallidos.database.dao.Tmct0CnmvEjecucionesDao;
import sibbac.business.fallidos.database.dao.Tmct0CnmvLogDao;
import sibbac.business.wrappers.common.fileWriter.SibbacFileWriter;
import sibbac.common.SIBBACBusinessException;

/**
 * Escribe el fichero de Titulares MiFIDII.
 * 
 */

@Component
public class TitularesRtWriter extends SibbacFileWriter<TitularesTrRecordBean> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES
  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(OperacionesRtWriter.class);

  /** Delimitador de líneas. */
  private DelimitedLineAggregator<TitularesTrRecordBean> delimitedLineAggregator;

  /** Mapeador de campos del tipo de registros a escribir en el fichero. */
  private BeanWrapperFieldExtractor<TitularesTrRecordBean> fieldExtractorTitularesRt;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
  @Autowired
  Tmct0CnmvEjecucionesDao tmct0CnmvEjecucionesDao;

  @Autowired
  private OperacionesTrBo operacionesTrBo;

  @Autowired
  Tmct0CnmvLogDao tmct0CnmvLogDao;

  /** Carpeta donde almacenar el fichero generado de operaciones temporal. */
  @Value("${nombre_titulares_tmp.tr.sending}")
  private String sFileNameTmp;

  /** Carpeta donde almacenar el fichero generado de operaciones definitivo. */
  @Value("${nombre_titulares.tr.sending}")
  private String sFinalFileNameTitular;

  /** fichero temporal. */
  private File tempFileTitulares;

  /** fichero Definitivo. */
  private File fileTitulares;

  /** Writer para escribir en el fichero. */
  private FlatFileItemWriter<TitularesTrRecordBean> writer;

  /**
   * @param listTitularesTr the listTitularesTr to set
   */
  public void setListTitularesTr(List<TitularesTrRecordBean> listTitularesTr) {
    this.listTitularesTr = listTitularesTr;
  }

  private List<TitularesTrRecordBean> listTitularesTr;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES
  public TitularesRtWriter() {
  }

  @Autowired
  public TitularesRtWriter(@Value("${nombre_titulares_tmp.tr.sending}") String fileTitularesName) {

    super(fileTitularesName);
    initalizeFieldExtractorTitularesRt();
    initializeDelimitedLineAggregator();
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLMENTADOS
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { SIBBACBusinessException.class, Exception.class })
  @Override
  public boolean writeIntoFile() throws SIBBACBusinessException {

    Boolean bResultado = Boolean.TRUE;

    if (listTitularesTr != null && !listTitularesTr.isEmpty()) {

      try {
        writer = this.getNewWriter();
        writer.write(listTitularesTr);
        writer.close();
      }
      catch (IOException e) {
        bResultado = Boolean.FALSE;
        LOG.error("[writeIntoFile] excepción no controlada. " + e.getMessage());
        writer.close();
      }
      catch (Exception e) {
        bResultado = Boolean.FALSE;
        LOG.error("[writeIntoFile] excepción no controlada. " + e.getMessage());
        writer.close();
      }

    }
    else {
      bResultado = Boolean.FALSE;
    }

    LOG.debug("[writeIntoFile] Fin ...");

    return bResultado;
  }

  public FlatFileItemWriter<TitularesTrRecordBean> getNewWriter() throws SIBBACBusinessException {

    return this.getNewWriter(new ExecutionContext());

  }

  public FlatFileItemWriter<TitularesTrRecordBean> getNewWriter(ExecutionContext ctx) throws SIBBACBusinessException {

    try {
      tempFileTitulares = new File(sFileNameTmp);
      tempFileTitulares.getParentFile().mkdirs();
      tempFileTitulares.createNewFile();
      return this.getNewWriter(ctx, tempFileTitulares);
    }
    catch (IOException e) {
      throw new SIBBACBusinessException("No se ha podido crear el writer para los titulares: " + e.getMessage(), e);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("No se ha podido crear el writer para los titulares: " + e.getMessage(), e);
    }

  }

  public FlatFileItemWriter<TitularesTrRecordBean> getNewWriter(File file) {
    return this.getNewWriter(new ExecutionContext(), file);
  }

  public FlatFileItemWriter<TitularesTrRecordBean> getNewWriter(ExecutionContext ctx, File file) {

    FlatFileItemWriter<TitularesTrRecordBean> writer = new FlatFileItemWriter<TitularesTrRecordBean>();
    writer.setEncoding(StandardCharsets.ISO_8859_1.name());
    writer.setResource(new FileSystemResource(file));
    writer.setLineAggregator(delimitedLineAggregator);
    writer.setTransactional(false);
    writer.setForceSync(true);
    writer.setShouldDeleteIfExists(true);
    writer.setShouldDeleteIfEmpty(true);
    writer.open(ctx);
    LOG.debug("[getNewWriter] Fin ...");
    return writer;

  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void closeWriter(FlatFileItemWriter<TitularesTrRecordBean> writer, ExecutionContext ctx) {
    if (writer != null) {
      if (ctx != null) {
        writer.update(ctx);
      }
      writer.close();
    }
  }

  /**
   * Inicializa el array de campos del fichero.
   */
  private void initalizeFieldExtractorTitularesRt() {
    LOG.debug("[initalizeFieldExtractorOperacionesRt] Inicio ...");

    fieldExtractorTitularesRt = new BeanWrapperFieldExtractor<TitularesTrRecordBean>();
    fieldExtractorTitularesRt.setNames(new String[] { "campo1", "campo2", "campo3", "campo4", "campo5", "campo6",
        "campo7", "campo8", "campo9", "campo10", "campo11", "campo12", "campo13", "campo14", "campo15", "campo16",
        "campo17", "campo18", "campo19", "campo20", "campo21", "campo22", "campo23", "campo24", "campo25", "campo26",
        "campo27" });

    LOG.debug("[initalizeFieldExtractorOperacionesRt] Fin ...");
  }

  /**
   * Inicializa el delimitador de líneas.
   */
  private void initializeDelimitedLineAggregator() {
    LOG.debug("[initializeDelimitedLineAggregator] Inicio ...");
    delimitedLineAggregator = new DelimitedLineAggregator<TitularesTrRecordBean>();
    delimitedLineAggregator.setDelimiter("|");
    delimitedLineAggregator.setFieldExtractor(fieldExtractorTitularesRt);
    LOG.debug("[initializeDelimitedLineAggregator] Fin ...");
  }

  /**
   * Inicializa el writer para escribir en el fichero.
   * 
   * @param file Fichero en el que escribir.
   * @throws SIBBACBusinessException
   */
  private void initalizeWriter(File file) {
    LOG.debug("[initalizeWriter] Inicio ...");

    writer = this.getNewWriter(file);
    LOG.debug("[initalizeWriter] Fin ...");
  }

  public void renameTempFile() throws SIBBACBusinessException {

    try {
      LOG.debug("[renameTempFile] Inicio ...");
      String sFinalFileNameTitularAux = sFinalFileNameTitular;
      // variable temporal para pruebas
      // String entorno = "c:";

      if (tempFileTitulares.exists()) {
        sFinalFileNameTitularAux = operacionesTrBo.componerNombreFichero(sFinalFileNameTitularAux);
        // fileTitulares = new File(entorno.concat(sFinalFileNameTitularAux));
        fileTitulares = new File(sFinalFileNameTitularAux);
        tempFileTitulares.renameTo(fileTitulares);
      }

      LOG.debug("[renameTempFile] Fin ...");

    }
    catch (Exception e) {
      LOG.warn("[renameTempFile] excepción no controlada. " + e.getMessage());
    }

  }

  public String deleteTempFile() {

    String resultado = null;

    try {
      if (tempFileTitulares.exists()) {
        tempFileTitulares.delete();
      }
    }
    catch (Exception e) {
      LOG.warn("[deleteTempFile] excepción no controlada. " + e.getMessage());
      resultado = e.getMessage();
    }

    return resultado;
  }

}
