package sibbac.business.fallidos.utils;

public class ValidatorFieldOperacionesTr {

	public static String validarTipoRegistro(String tipoRegistro) {
		
		String tipoRegistroValidado = "";
		
		if(tipoRegistro != null && tipoRegistro.length() > 2) {
			tipoRegistroValidado = tipoRegistro.substring(0, 2);
		}else {
			return tipoRegistro;
		}
		
		return tipoRegistroValidado;
	}
	public static String validarRefOperacion(String refOperacion) {
		
		String refOperacionValidado = "";
		
		if(refOperacion != null && refOperacion.length() > 52) {
			refOperacionValidado = refOperacion.substring(0, 52);
		}else {
			return refOperacion;
		}
		
		return refOperacionValidado;
	}
	
	public static String validarEstado(String estado) {
		
		String estadoValidado = "";
		
		if(estado != null && estado.length() > 1) {
			estadoValidado = estado.substring(0, 1);
		}else {
			return estado;
		}
		
		return estadoValidado;
	}
	
	public static String validarIdEjecucion(String idEjecucion) {
		
		String idEjecucionValidado = "";
		
		if(idEjecucion != null && idEjecucion.length() > 52) {
			idEjecucionValidado = idEjecucion.substring(0, 52);
		}else {
			return idEjecucion;
		}
		
		return idEjecucionValidado;
	}
	
	public static String ValidarIdEntEjecutora(String idEntEjecutora) {
		
		String idEntEjecutoraValidado = "";
		
		if(idEntEjecutora != null && idEntEjecutora.length() > 20) {
			idEntEjecutoraValidado = idEntEjecutora.substring(0, 20);
		}else {
			return idEntEjecutora;
		}
		return idEntEjecutoraValidado;
	}
	
	public static String validarEntMiFid(String entMifid) {
		
		String entMifidValidado = "";
		
		if(entMifid != null && entMifid.length() > 1) {
			entMifidValidado = entMifid.substring(0, 1);
		}else {
			return entMifid;
		}
		
		return entMifidValidado;
	}
	
	public static String validarSentidoOperacion(String sentidoOperacion) {
		
		String sentidoOperacionValidado = "";
		
		
		if(sentidoOperacion != null && sentidoOperacion.length() > 1) {
			sentidoOperacionValidado = sentidoOperacion.substring(0, 1);
		}else {
			return sentidoOperacion;
		}
		return sentidoOperacionValidado;
	}
	
	public static String validarIndicadorTransmision(String indicadorTransmision) {
		
		String indicadorTransmisionValidado = "";
		
		if(indicadorTransmision != null && indicadorTransmision.length() > 1) {
			indicadorTransmisionValidado = indicadorTransmision.substring(0, 1);
		}else {
			return indicadorTransmision;
		}
		return indicadorTransmisionValidado;
	}
	
	public static String validarIdEntTransmisora (String idEntTransmisora) {
		
		String idEntTransmisoraValidado = "";
		
		if(idEntTransmisora != null && idEntTransmisora.length() > 20) {
			idEntTransmisoraValidado = idEntTransmisora.substring(0, 20);
		}else {
			return idEntTransmisora;
		}
		
		return idEntTransmisoraValidado;
	}
	
	public static String validarFechaHoraNegociacion(String fechaHora) {
		
		String fechaHoraNegociacionValidado = "";
		
		if(fechaHora != null && fechaHora.length() > 27) {
			fechaHoraNegociacionValidado = fechaHora.substring(0, 27);
		}else {
			return fechaHora;
		}
		
		return fechaHoraNegociacionValidado;
	}
	
	public static String validarIndCapacidad(String indCapacidad) {
		
		String indCapacidadValidado = "";
		
		if(indCapacidad != null && indCapacidad.length() > 4) {
			indCapacidadValidado = indCapacidad.substring(0, 4);
		}else {
			return indCapacidad;
		}
		
		return indCapacidadValidado;
	}
	
	public static String validarPrecio(String precio) {
		
		String precioValidado = "";
		//precio = precio.replace(".", "");
		if(precio != null && precio.length() > 18 && (precio.contains(".") || !precio.contains("."))) {
			precioValidado = precio.substring(0, 18);
		}else {
			return precio;
		}
		return precioValidado;
	}
	
	public static String validarDivisa(String divisa) {
		
		String divisaValidada = "";
		
		if(divisa != null && divisa.length() > 3) {
			divisaValidada = divisa.substring(0, 3);
		}else {
			return divisa;
		}
		
		return divisaValidada;
	}
	
	public static String validarCentroNegociacion(String centroNegociacion) {
		
		String centroNegociacionValidado = "";
		
		if(centroNegociacion != null && centroNegociacion.length() > 4) {
			centroNegociacionValidado = centroNegociacion.substring(0, 4);
		}else {
			return centroNegociacion;
		}
		
		return centroNegociacionValidado;
	}
	
	public static String validarCdPais(String cdPaisSucursal) {
		
		String cdPaisSucursalValidado = "";
		
		if(cdPaisSucursal != null && cdPaisSucursal.length() > 2) {
			cdPaisSucursalValidado = cdPaisSucursal.substring(0, 2);
		}else {
			return cdPaisSucursal;
		}
		
		return cdPaisSucursalValidado;
	}
	
	public static String validarIdDecisorInversion(String idDecisorInversion) {
		
		String idDecisorInversionValidado = "";
		
		if(idDecisorInversion != null && idDecisorInversion.length() > 50) {
			idDecisorInversionValidado = idDecisorInversion.substring(0, 50);
		}else {
			return idDecisorInversion;
		}
		
		return idDecisorInversionValidado;
	}
	
	public static String validarIdEjecutor(String idDecisorEjecutor) {
		
		String idEjecutor= "";
		
		if(idDecisorEjecutor != null && idDecisorEjecutor.length() > 50) {
			idEjecutor = idDecisorEjecutor.substring(0, 50);
		}else {
			return idDecisorEjecutor;
		}
		
		return idEjecutor;
	}
	
	public static String validarIndicadorExcencion(String indicador) {
		
		String indicadorValidado = "";
		
		if(indicador != null && indicador.length() > 24) {
			indicadorValidado = indicador.substring(0, 24);
		}else {
			return indicador;
		}
		
		return indicadorValidado;
	}
	
	public static String validarIndicadorVentaCorto(String ventaCorto) {
		
		String indVentaCortoValidado = "";
		
		if(ventaCorto != null && ventaCorto.length() > 4) {
			indVentaCortoValidado = ventaCorto.substring(0, 4);
		}else {
			return ventaCorto;
		}
		
		return indVentaCortoValidado;
	}
	
	public static String validarIndicadorPostNegociacion(String indPostNegociacion) {
		
		String indPostNegociacionValidado = "";
		
		if(indPostNegociacion != null && indPostNegociacion.length() > 24) {
			indPostNegociacionValidado = indPostNegociacion.substring(0, 24);
		}else {
			return indPostNegociacion;
		}
		
		return indPostNegociacionValidado;
	}
	
	public static String validarReportable(String reportable){
		
		String reportableValidado = "";
		
		if(reportable != null && reportable.length() > 3){
			reportableValidado = reportable.substring(0, 3);
		}else {
			return reportable;
		}
		
		return reportableValidado;
	}
	
	public static String validarMotivoNoReportable(String motivoNoReportable) {
		
		String motivoNoReportableValidado = "";
		
		if(motivoNoReportable != null && motivoNoReportable.length() > 50) {
			motivoNoReportableValidado = motivoNoReportable.substring(0, 50);
		}else {
			return motivoNoReportable;
		}
		
		return motivoNoReportableValidado;
	}
	
	public static String validarRefCruceTitulares(String refCruceTitulares) {
		
		String refCruceTitularesValidado = "";
		
		if(refCruceTitulares != null && refCruceTitulares.length() > 20) {
			refCruceTitularesValidado = refCruceTitulares.substring(0, 20);
		}else {
			return refCruceTitulares;
		}
		
		return refCruceTitularesValidado;
	}
	
	public static String validarTipoIdEntTransmisora(String idEntidadTransmisora) {
		
		String tipoIdEntTransmisoraValidado = "";
		
		if(idEntidadTransmisora != null && idEntidadTransmisora.length() > 3) {
			tipoIdEntTransmisoraValidado = idEntidadTransmisora.substring(0, 3);
		}else {
			return idEntidadTransmisora;
		}
		
		return tipoIdEntTransmisoraValidado;
	}
	
	public static String validarTipoIdEntTransmisoraCliente(String idEntidadTransmisoraCliente) {
		
		String tipoIdEntTransmisoraClienteValidado = "";
		
		if(idEntidadTransmisoraCliente != null && idEntidadTransmisoraCliente.length() > 3) {
			tipoIdEntTransmisoraClienteValidado = idEntidadTransmisoraCliente.substring(0, 3);
		}else {
			return idEntidadTransmisoraCliente;
		}
		
		return tipoIdEntTransmisoraClienteValidado;
	}
	
	public static String validarTipoCantidad(String tipoCantidad) {
		
		String tipoCantidadValidado = "";
		
		if(tipoCantidad != null && tipoCantidad.length() > 4) {
			tipoCantidadValidado = tipoCantidad.substring(0, 4);
		}else {
			return tipoCantidad;
		}
		
		return tipoCantidadValidado;
	}
	
	public static String validarTipoPrecio(String tipoPrecio) {
		
		String tipoPrecioValidado = "";
		
		if(tipoPrecio != null && tipoPrecio.length() > 4) {
			tipoPrecioValidado = tipoPrecio.substring(0, 4);
		}else {
			return tipoPrecio;
		}
		
		return tipoPrecioValidado;
	}
	
	public static String validarIndNoPrecio(String noPrecio) {
		
		String noPrecioValidado = "";
		
		if(noPrecio != null && noPrecio.length() > 4) {
			noPrecioValidado = noPrecio.substring(0, 4);
		}else {
			return noPrecio;
		}
		
		return noPrecioValidado;
	}
	
	public static String validarTipoIdDecisor(String tipoIdDecisor) {
		
		String tipoIdDecisorValidado = "";
		
		if(tipoIdDecisor != null && tipoIdDecisor.length() > 3) {
			tipoIdDecisorValidado = tipoIdDecisor.substring(0, 3);
		}else {
			return tipoIdDecisor;
		}
		
		return tipoIdDecisorValidado;
	}
	
	public static String validarTipoIdEjecutor(String tipoIdEjecutor) {
		
		String tipoIdEjecutorValidado = "";
		
		if(tipoIdEjecutor != null && tipoIdEjecutor.length() > 3) {
			tipoIdEjecutorValidado = tipoIdEjecutor.substring(0, 3);
		}else {
			return tipoIdEjecutor;
		}
		
		return tipoIdEjecutorValidado;
	}
	
	public static String validarAuxCdTransmisora(String auxCdTransmisora) {
		
		String auxCdTransmisoraValidado = "";
		
		if(auxCdTransmisora != null && auxCdTransmisora.length() > 20) {
			auxCdTransmisoraValidado = auxCdTransmisora.substring(0, 20);
		}else {
			return auxCdTransmisora;
		}
		
		return auxCdTransmisoraValidado;
	}
	
	public static String validarTipoAuxCdTransmisora(String tipoAuxCdTransmisora) {
		
		String tipoAuxCdTransmisoraValidado = "";
		
		if(tipoAuxCdTransmisora != null && tipoAuxCdTransmisora.length() > 3) {
			tipoAuxCdTransmisoraValidado = tipoAuxCdTransmisora.substring(0, 3);
		}else {
			return tipoAuxCdTransmisora;
		}
		
		return tipoAuxCdTransmisoraValidado;
	}
}
