package sibbac.business.fallidos.dto;


import java.math.BigDecimal;

import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fallidos.database.model.TitularesH47;


public class TitularesH47DTO {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
	private Long		id;
	private Character	tpsocied;
	private Character	tpidenti;
	private String		identificacion;
	private String		nombre;
	private String		apellido1;
	private String		apellido2;
	private Character	repTpidenti;
	private String		repIdentificacion;
	private String		repNombre;
	private String		repApellido1;
	private String		repApellido2;
	private BigDecimal	cantidadValores;

	public TitularesH47DTO() {
		super();
	}

	public TitularesH47DTO( Long id, Character tpsocied, Character tpidenti, String identificacion, String nombre, String apellido1,
			String apellido2, Character repTpsocied, Character repTpidenti, String repIdentificacion, String repNombre,
			String repApellido1, String repApellido2, BigDecimal cantidadValores, OperacionH47 operacionH47 ) {
		super();
		this.id = id;
		this.tpsocied = tpsocied;
		this.tpidenti = tpidenti;
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.repTpidenti = repTpidenti;
		this.repIdentificacion = repIdentificacion;
		this.repNombre = repNombre;
		this.repApellido1 = repApellido1;
		this.repApellido2 = repApellido2;
		this.cantidadValores = cantidadValores;
	}

	public TitularesH47DTO( TitularesH47 titular ) {
		super();
		this.id = titular.getId();
		this.tpsocied = titular.getTpsocied();
		this.tpidenti = titular.getTpidenti();
		this.identificacion = titular.getIdentificacion();
		this.nombre = titular.getNombre();
		this.apellido1 = titular.getApellido1();
		this.apellido2 = titular.getApellido2();
		this.repTpidenti = titular.getRepTpidenti();
		this.repIdentificacion = titular.getRepIdentificacion();
		this.repNombre = titular.getRepNombre();
		this.repApellido1 = titular.getRepApellido1();
		this.repApellido2 = titular.getRepApellido2();
		this.cantidadValores = titular.getCantidadValores();
	}
	
	public Long getId() {
		return id;
	}

	
	public void setId( Long id ) {
		this.id = id;
	}

	public Character getTpsocied() {
		return tpsocied;
	}

	public void setTpsocied( Character tpsocied ) {
		this.tpsocied = tpsocied;
	}

	public Character getTpidenti() {
		return tpidenti;
	}

	public void setTpidenti( Character tpidenti ) {
		this.tpidenti = tpidenti;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion( String identificacion ) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1( String apellido1 ) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2( String apellido2 ) {
		this.apellido2 = apellido2;
	}

	public Character getRepTpidenti() {
		return repTpidenti;
	}

	public void setRepTpidenti( Character repTpidenti ) {
		this.repTpidenti = repTpidenti;
	}

	public String getRepIdentificacion() {
		return repIdentificacion;
	}

	public void setRepIdentificacion( String repIdentificacion ) {
		this.repIdentificacion = repIdentificacion;
	}

	public String getRepNombre() {
		return repNombre;
	}

	public void setRepNombre( String repNombre ) {
		this.repNombre = repNombre;
	}

	public String getRepApellido1() {
		return repApellido1;
	}

	public void setRepApellido1( String repApellido1 ) {
		this.repApellido1 = repApellido1;
	}

	public String getRepApellido2() {
		return repApellido2;
	}

	public void setRepApellido2( String repApellido2 ) {
		this.repApellido2 = repApellido2;
	}

	public BigDecimal getCantidadValores() {
		return cantidadValores;
	}

	public void setCantidadValores( BigDecimal cantidadValores ) {
		this.cantidadValores = cantidadValores;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer( "H47_TITULARES" );
		// sb.append( " [ISIN==" + this.isin + "]" );
		return sb.toString();
	}

}
