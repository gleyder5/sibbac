package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

@Table(name = DBConstants.FALLIDOS.FALLIDOS_ALC)
@Entity
public class FallidoAlc extends ATable<FallidoAlc> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -6728242241609382163L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Identificador de un alc. */
  private Tmct0alcId tmct0alcId;

  /** Referencia enviada a S3 para el alc. */
  @Column(name = "REFERENCIAS3", nullable = false, length = 20)
  private String referenciaS3;

  /** Títulos fallidos. */
  @Column(name = "TITULOS_FALLIDOS", nullable = false, precision = 18, scale = 6)
  private BigDecimal titulosFallidos;

  @ManyToOne(optional = true, fetch = FetchType.LAZY)
  @JoinColumn(name = "fallido_id", referencedColumnName = "ID")
  private Fallido fallido_id;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /** Constructor I. Inicializa los atributos de la clase. */
  public FallidoAlc() {
    titulosFallidos = BigDecimal.ZERO;
  } // FallidoAlc

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "FallidoAlc [tmct0alcId=" + tmct0alcId + ", referenciaS3=" + referenciaS3 + ", titulosFallidos="
           + titulosFallidos + ", fallido_id=" + fallido_id + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the tmct0alcId
   */
  public Tmct0alcId getTmct0alcId() {
    return tmct0alcId;
  }

  /**
   * @return the referenciaS3
   */
  public String getReferenciaS3() {
    return referenciaS3;
  }

  /**
   * @return the titulosFallidos
   */
  public BigDecimal getTitulosFallidos() {
    return titulosFallidos;
  }

  /**
   * @return the fallido_id
   */
  public Fallido getFallido_id() {
    return fallido_id;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param tmct0alcId the tmct0alcId to set
   */
  public void setTmct0alcId(Tmct0alcId tmct0alcId) {
    this.tmct0alcId = tmct0alcId;
  }

  /**
   * @param referenciaS3 the referenciaS3 to set
   */
  public void setReferenciaS3(String referenciaS3) {
    this.referenciaS3 = referenciaS3;
  }

  /**
   * @param titulosFallidos the titulosFallidos to set
   */
  public void setTitulosFallidos(BigDecimal titulosFallidos) {
    this.titulosFallidos = titulosFallidos;
  }

  /**
   * @param fallido_id the fallido_id to set
   */
  public void setFallido_id(Fallido fallido_id) {
    this.fallido_id = fallido_id;
  }

} // FallidoAlc
