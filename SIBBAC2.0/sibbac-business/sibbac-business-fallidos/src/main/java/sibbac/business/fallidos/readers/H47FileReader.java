package sibbac.business.fallidos.readers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.bo.ErrorCNMVBo;
import sibbac.business.fallidos.database.bo.OperacionH47Bo;
import sibbac.business.fallidos.database.model.ErrorCNMV;
import sibbac.business.fallidos.database.model.OperacionH47;
import sibbac.business.fallidos.writers.H47FieldSetMapper;
import sibbac.business.fallidos.writers.H47RecordBean;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.common.fileReader.SibbacFixedLengthFileReader;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.common.SIBBACPTICommons;

@Service
public class H47FileReader extends SibbacFixedLengthFileReader<H47RecordBean> {

  private static final Logger LOG = LoggerFactory.getLogger(H47FileReader.class);

  @Autowired
  private OperacionH47Bo operacionBo;
  @Autowired
  private ErrorCNMVBo errorBo;
  @Autowired
  private Tmct0estadoBo estadoBo;

  public H47FileReader() {
    super();
  }

  @Autowired
  public H47FileReader(@Value("${sibbac.wrappers.BancaPrivadaFile}") String fileName) {
    super(fileName);
    // final FixedLengthTokenizer lineTokenizer = new FixedLengthTokenizer();
    // lineTokenizer.setNames(H47FieldSetMapper.H47Fields.getFieldNames());
    // lineTokenizer.setColumns(this.convertLengthsIntoRanges(H47FieldSetMapper.H47Fields.getH47Lengths()));
    // lineTokenizer.setStrict(false);
    // this.lineMapper = new DefaultLineMapper<H47RecordBean>();
    // ((DefaultLineMapper<H47RecordBean>) this.lineMapper).setLineTokenizer(lineTokenizer);
    // ((DefaultLineMapper<H47RecordBean>) this.lineMapper).setFieldSetMapper(new H47FieldSetMapper());
    // LOG.trace("[H47FileReader :: Constructor] FileName: " + fileName);
  }

  @Override
  protected Map<String, LineTokenizer> initializeLineTokenizers() {
    LOG.debug("[H47FileReader :: initializeLineTokenizers] Init");
    final Map<String, LineTokenizer> tokenizers = new HashMap<String, LineTokenizer>();
    final FixedLengthTokenizer lineTokenizer = new FixedLengthTokenizer();
    lineTokenizer.setNames(H47FieldSetMapper.H47Fields.getFieldNames());
    lineTokenizer.setColumns(this.convertLengthsIntoRanges(H47FieldSetMapper.H47Fields.getH47Lengths()));
    lineTokenizer.setStrict(false);
    tokenizers.put("*", lineTokenizer);
    LOG.debug("[H47FileReader :: initializeLineTokenizers] Fin");
    return tokenizers;
  }

  @Override
  protected Map<String, FieldSetMapper<H47RecordBean>> initializeFieldSetMappers() {
    LOG.debug("[H47FileReader :: initializeFieldSetMappers] Init");
    final Map<String, FieldSetMapper<H47RecordBean>> fieldSetMappers = new HashMap<String, FieldSetMapper<H47RecordBean>>();
    fieldSetMappers.put("*", new H47FieldSetMapper());
    LOG.debug("[H47FileReader :: initializeFieldSetMappers] Fin");
    return fieldSetMappers;
  }

  @Override
  protected boolean processLine(Map<String, List<H47RecordBean>> group, final H47RecordBean bean, final int lineNumber) throws SIBBACBusinessException {
    LOG.trace("[H47FileReader :: processLine] Fin");
    try {
      String date = bean.getIcnrefme().substring(0, 8);
      Date fNegociacion = SIBBACPTICommons.convertStringToDate(date, SIBBACPTICommons.DATE_FORMAT);
      OperacionH47 operacion = operacionBo.findByIdAndFhNegociacion(Long.parseLong(bean.getIcnrefme().substring(8)),
                                                                    fNegociacion);

      if (bean.getIccerrme() != null && StringUtils.isNotEmpty(StringUtils.trimToEmpty(bean.getIccerrme()))) {
        ErrorCNMV error = errorBo.findFirstByCodigo(bean.getIccerrme());
        if (error != null) {
          operacion.setError(error);
        }
        operacion.setEstadoEnvio(estadoBo.findById(EstadosEnumerados.H47.RECHAZADA.getId()));
      } else {
        operacion.setEstadoEnvio(estadoBo.findById(EstadosEnumerados.H47.ACEPTADA_BME.getId()));
        operacion.setError(null);
      }

      operacionBo.save(operacion);
    } catch (Exception e) {
      LOG.error("[H47FileReader :: processLine] Error procesando línea " + lineNumber + " con ICNREFME="
                + bean.getIcnrefme() + ": " + e.getMessage());
    }
    LOG.trace("[H47FileReader :: processLine] Fin");
    return true;
  }

  @Override
  protected void initBlockToProcess() {
    // TODO Auto-generated method stub

  }

  @Override
  protected void finishBlockToProcess() {
    // TODO Auto-generated method stub

  }

  @Override
  protected boolean postProcessFile(Map<String, List<H47RecordBean>> group) throws SIBBACBusinessException {
    // TODO Auto-generated method stub
    return false;
  }

}
