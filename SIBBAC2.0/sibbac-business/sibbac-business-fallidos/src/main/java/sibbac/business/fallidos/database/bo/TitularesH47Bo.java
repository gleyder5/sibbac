package sibbac.business.fallidos.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.fallidos.database.dao.TitularesH47Dao;
import sibbac.business.fallidos.database.model.TitularesH47;
import sibbac.database.bo.AbstractBo;


@Service
public class TitularesH47Bo extends AbstractBo< TitularesH47, Long, TitularesH47Dao > {
	public TitularesH47 createTitularBanco(String nudnicif, String nombreBanco){
		TitularesH47 titular = new TitularesH47( 'J', 'C', nudnicif, nombreBanco, "", "", null, null, null, null, null, null,null, null );
		return titular;
	}
}
