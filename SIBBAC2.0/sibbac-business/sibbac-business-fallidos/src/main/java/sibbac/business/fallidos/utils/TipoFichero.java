package sibbac.business.fallidos.utils;


public enum TipoFichero {

	AJUSTES( "CFAILSADJUST.C0" ), LIQUIDACION_EFECTIVO( "CFAILCASHSETTL.C0" );

	private String	fileName;

	private TipoFichero( String fileName ) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return this.fileName;
	}

}
