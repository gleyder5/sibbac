package sibbac.business.fallidos.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_CNMV_DESGLOSES
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CNMV_DESGLOSES")
public class Tmct0CnmvDesgloses {

	private static final Logger LOG = LoggerFactory
			.getLogger(Tmct0CnmvEjecuciones.class);

	/** Campos de la entidad */

	/**
	 * Identificador del registro en la tabla
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;

	/**
	 * Fecha en la que actual de la creación o última modificación
	 */
	@Column(name = "AUDIT_DATE", length = 10, nullable = false)
	private Date auditDate;

	/**
	 * identificador del usuario que creó o modificó por última vez la factura
	 */
	@Column(name = "AUDIT_USER", length = 20, nullable = false)
	private String auditUser;

	/**
	 * Datos de la ejecucion a la que pertenece:
	 */
	@OneToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "ID_REG_EJECUCION" ,referencedColumnName = "ID")
	private Tmct0CnmvEjecuciones ejecucion;

	@Column(name = "NUORDEN", nullable = false, length = 20)
	private String nuorden;

	@Column(name = "NBOOKING", nullable = false, length = 20)
	private String nbooking;

	@Column(name = "NUCNFCLT", nullable = false, length = 20)
	private String nucnfclt;

	@Column(name = "NUCNFLIQ", nullable = false, precision = 4)
	private Short nucnfliq;



	@Column(name = "IDDESGLOSECAMARA", nullable = false)
    private Long idDesgloseCamara;
	
	/**
	 * Títulos del desglose
	 */
	@Column(name = "TITULOS_DESGLOSE", precision = 18, scale = 6, nullable = false)
	private BigDecimal titulosDesgloses;

	/**
	 * A Alta / B Baja
	 */

	@Column(name = "ESTADO_DESGLOSE", length = 1, nullable = false)
	private Character estadoDesglose;
	
	
	 @Column(name = "REFOPERACION", length = 52)
	  private String refOperacion;
	 
	  @Column(name = "INDICADOR_VENTA_CORTO", length = 4)
	  private String indicadorVentaCorto;

	  @Column(name = "REF_CRUCE_TITULARES", length = 20)
	  private String refCruceTitulares;
	  
	  @Column(name = "REPORTABLE", length = 3)
    private String reportable;
	  
    
    @Column(name = "MOTIVO_NO_REPORTABLE", length = 50)
    private String motivoNoReportable;	  
	  
    @Column(name = "TIPO_ID_TRANSMISORA", length = 3)
    private String tipoIdTransmisora;    
    

    @Column(name = "ID_TRANSMISORA", length = 20)
    private String idTransmisora;    
    
    @Column(name = "TIPO_AUX_ID_TRANSMISORA", length = 3)
    private String tipoAuxIdTransmisora;       
    
    @Column(name = "AUX_ID_TRANSMISORA", length = 20)
    private String auxIdTransmisora;      
        
    @Column(name = "CAPACIDADNEGOCIACION", length = 4)
    private String capacidadNegociacion;

    @Column(name = "EFECTIVO_DESGLOSE", precision = 18, scale = 5)
    private BigDecimal efectivoDesglose;
    
    @Column(name = "TIPO_ID_DECISOR_INVERSION", length = 4)
    private String tipoIdDecisorInversion;    
    
    
    @Column(name = "ID_DECISOR_INVERSION", length = 50)
    private String idDecisorInversion;        
    
    @Column(name = "CD_PAIS_DECISOR", length = 2)
    private String cdPaisDecisor;         
    
    @Column(name = "CDESTADOENVIO", length = 2)
    private Integer cdEstadoEnvio;      
    
    @Column(name = "ORIGEN_CRUCE", length = 4)
    private String origenCruce;  
    

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO_ALTA")
    private Date fEnvioAlta;

    /**
     * Fecha de envío de la baja de la ejecución a CNMV
     */

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ENVIO_BAJA")
    private Date fEnvioBaja;

  @OneToMany(mappedBy = "desglose", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Tmct0CnmvTitulares> tmct0CnmvTitularesList;


	/**
	 * @return the tmct0CnmvTitularesList
	 */
	public List<Tmct0CnmvTitulares> getTmct0CnmvTitularesList() {
		return tmct0CnmvTitularesList;
	}

	/**
	 * @param tmct0CnmvTitularesList the tmct0CnmvTitularesList to set
	 */
	public void setTmct0CnmvTitularesList(
			List<Tmct0CnmvTitulares> tmct0CnmvTitularesList) {
		this.tmct0CnmvTitularesList = tmct0CnmvTitularesList;
	}	
	

	
	
	/**
   * @return the reportable
   */
  public String getReportable() {
    return reportable;
  }

  /**
   * @param reportable the reportable to set
   */
  public void setReportable(String reportable) {
    this.reportable = reportable;
  }

  /**
   * @return the motivoNoReportable
   */
  public String getMotivoNoReportable() {
    return motivoNoReportable;
  }

  /**
   * @param motivoNoReportable the motivoNoReportable to set
   */
  public void setMotivoNoReportable(String motivoNoReportable) {
    this.motivoNoReportable = motivoNoReportable;
  }

  /**
   * @return the tipoIdTransmisora
   */
  public String getTipoIdTransmisora() {
    return tipoIdTransmisora;
  }

  /**
   * @param tipoIdTransmisora the tipoIdTransmisora to set
   */
  public void setTipoIdTransmisora(String tipoIdTransmisora) {
    this.tipoIdTransmisora = tipoIdTransmisora;
  }

  /**
   * @return the idTransmisora
   */
  public String getIdTransmisora() {
    return idTransmisora;
  }

  /**
   * @param idTransmisora the idTransmisora to set
   */
  public void setIdTransmisora(String idTransmisora) {
    this.idTransmisora = idTransmisora;
  }

  /**
   * @return the tipoAuxIdTransmisora
   */
  public String getTipoAuxIdTransmisora() {
    return tipoAuxIdTransmisora;
  }

  /**
   * @param tipoAuxIdTransmisora the tipoAuxIdTransmisora to set
   */
  public void setTipoAuxIdTransmisora(String tipoAuxIdTransmisora) {
    this.tipoAuxIdTransmisora = tipoAuxIdTransmisora;
  }

  /**
   * @return the auxIdTransmisora
   */
  public String getAuxIdTransmisora() {
    return auxIdTransmisora;
  }

  /**
   * @param auxIdTransmisora the auxIdTransmisora to set
   */
  public void setAuxIdTransmisora(String auxIdTransmisora) {
    this.auxIdTransmisora = auxIdTransmisora;
  }

  /**
   * @return the capacidadNegociacion
   */
  public String getCapacidadNegociacion() {
    return capacidadNegociacion;
  }

  /**
   * @param capacidadNegociacion the capacidadNegociacion to set
   */
  public void setCapacidadNegociacion(String capacidadNegociacion) {
    this.capacidadNegociacion = capacidadNegociacion;
  }

  /**
   * @return the efectivoDesglose
   */
  public BigDecimal getEfectivoDesglose() {
    return efectivoDesglose;
  }

  /**
   * @param efectivoDesglose the efectivoDesglose to set
   */
  public void setEfectivoDesglose(BigDecimal efectivoDesglose) {
    this.efectivoDesglose = efectivoDesglose;
  }

  /**
   * @return the tipoIdDecisorInversion
   */
  public String getTipoIdDecisorInversion() {
    return tipoIdDecisorInversion;
  }

  /**
   * @param tipoIdDecisorInversion the tipoIdDecisorInversion to set
   */
  public void setTipoIdDecisorInversion(String tipoIdDecisorInversion) {
    this.tipoIdDecisorInversion = tipoIdDecisorInversion;
  }

  /**
   * @return the idDecisorInversion
   */
  public String getIdDecisorInversion() {
    return idDecisorInversion;
  }

  /**
   * @param idDecisorInversion the idDecisorInversion to set
   */
  public void setIdDecisorInversion(String idDecisorInversion) {
    this.idDecisorInversion = idDecisorInversion;
  }

  /**
   * @return the cdPaisDecisor
   */
  public String getCdPaisDecisor() {
    return cdPaisDecisor;
  }

  /**
   * @param cdPaisDecisor the cdPaisDecisor to set
   */
  public void setCdPaisDecisor(String cdPaisDecisor) {
    this.cdPaisDecisor = cdPaisDecisor;
  }

  /**
   * @return the cdEstadoEnvio
   */
  public Integer getCdEstadoEnvio() {
    return cdEstadoEnvio;
  }

  /**
   * @param cdEstadoEnvio the cdEstadoEnvio to set
   */
  public void setCdEstadoEnvio(Integer cdEstadoEnvio) {
    this.cdEstadoEnvio = cdEstadoEnvio;
  }

  /**
	 * Constructor
	 */
	public Tmct0CnmvDesgloses() {
		super();
	}



	/**
	 * Constructor con parametros
	 */

	

  public Tmct0CnmvDesgloses(BigInteger id,
                            Date auditDate,
                            String auditUser,
                            Tmct0CnmvEjecuciones ejecucion,
                            String nuorden,
                            String nbooking,
                            String nucnfclt,
                            Short nucnfliq,
                            Long idDesgloseCamara,
                            BigDecimal titulosDesgloses,
                            Character estadoDesglose,
                            String refOperacion,
                            String indicadorVentaCorto,
                            String refCruceTitulares,
                            String reportable,
                            String motivoNoReportable,
                            String tipoIdTransmisora,
                            String idTransmisora,
                            String tipoAuxIdTransmisora,
                            String auxIdTransmisora,
                            String capacidadNegociacion,
                            BigDecimal efectivoDesglose,
                            String tipoIdDecisorInversion,
                            String idDecisorInversion,
                            String cdPaisDecisor,
                            Integer cdEstadoEnvio,
                            List<Tmct0CnmvTitulares> tmct0CnmvTitularesList,
                            String origenCruce,
                            Date fEnvioAlta,
                            Date fEnvioBaja) {
    super();
    this.id = id;
    this.auditDate = auditDate;
    this.auditUser = auditUser;
    this.ejecucion = ejecucion;
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.idDesgloseCamara = idDesgloseCamara;
    this.titulosDesgloses = titulosDesgloses;
    this.estadoDesglose = estadoDesglose;
    this.refOperacion = refOperacion;
    this.indicadorVentaCorto = indicadorVentaCorto;
    this.refCruceTitulares = refCruceTitulares;
    this.reportable = reportable;
    this.motivoNoReportable = motivoNoReportable;
    this.tipoIdTransmisora = tipoIdTransmisora;
    this.idTransmisora = idTransmisora;
    this.tipoAuxIdTransmisora = tipoAuxIdTransmisora;
    this.auxIdTransmisora = auxIdTransmisora;
    this.capacidadNegociacion = capacidadNegociacion;
    this.efectivoDesglose = efectivoDesglose;
    this.tipoIdDecisorInversion = tipoIdDecisorInversion;
    this.idDecisorInversion = idDecisorInversion;
    this.cdPaisDecisor = cdPaisDecisor;
    this.cdEstadoEnvio = cdEstadoEnvio;
    this.tmct0CnmvTitularesList = tmct0CnmvTitularesList;
    this.origenCruce = origenCruce;
    this.fEnvioAlta = fEnvioAlta;
    this.fEnvioBaja = fEnvioBaja;
  }

	

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}



  /**
	 * @param id
	 *            the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate
	 *            the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser
	 *            the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * @return the ejecucion
	 */
	public Tmct0CnmvEjecuciones getEjecucion() {
		return this.ejecucion;
	}

	/**
	 * @param ejecucion
	 *            the ejecucion to set
	 */
	public void setEjecucion(Tmct0CnmvEjecuciones ejecucion) {
		this.ejecucion = ejecucion;
	}

	/**
	 * @return the norden
	 */
	public String getNuorden() {
		return nuorden;
	}

	/**
	 * @param norden
	 *            the norden to set
	 */
	public void setNuorden(String norden) {
		this.nuorden = norden;
	}

	/**
	 * @return the nbooking
	 */
	public String getNbooking() {
		return nbooking;
	}

	/**
	 * @param nbooking
	 *            the nbooking to set
	 */
	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}

	/**
	 * @return the nucnfclt
	 */
	public String getNucnfclt() {
		return nucnfclt;
	}

	/**
	 * @param nucnfclt
	 *            the nucnfclt to set
	 */
	public void setNucnfclt(String nucnfclt) {
		this.nucnfclt = nucnfclt;
	}

	/**
	 * @return the nucnfliq
	 */
	public Short getNucnfliq() {
		return nucnfliq;
	}

	/**
	 * @param nucnfliq
	 *            the nucnfliq to set
	 */
	public void setNucnfliq(Short nucnfliq) {
		this.nucnfliq = nucnfliq;
	}



	/**
	 * @return the titulosDesgloses
	 */
	public BigDecimal getTitulosDesgloses() {
		return titulosDesgloses;
	}

	/**
	 * @param titulosDesgloses
	 *            the titulosDesgloses to set
	 */
	public void setTitulosDesgloses(BigDecimal titulosDesgloses) {
		this.titulosDesgloses = titulosDesgloses;
	}

	/**
	 * @return the estadoDesglose
	 */
	public Character getEstadoDesglose() {
		return estadoDesglose;
	}

	/**
	 * @param estadoDesglose
	 *            the estadoDesglose to set
	 */
	public void setEstadoDesglose(Character estadoDesglose) {
		this.estadoDesglose = estadoDesglose;
	}

	/**
	 * @return the idDesgloseCamara
	 */
	public Long getIdDesgloseCamara() {
		return idDesgloseCamara;
	}

	/**
	 * @param idDesgloseCamara the idDesgloseCamara to set
	 */
	public void setIdDesgloseCamara(Long idDesgloseCamara) {
		this.idDesgloseCamara = idDesgloseCamara;
	}

  /**
   * @return the indicadorVentaCorto
   */
  public String getIndicadorVentaCorto() {
    return indicadorVentaCorto;
  }

  /**
   * @param indicadorVentaCorto
   *            the indicadorVentaCorto to set
   */
  public void setIndicadorVentaCorto(String indicadorVentaCorto) {
    this.indicadorVentaCorto = indicadorVentaCorto;
  }

  /**
   * @return the refOperacion
   */
  public String getRefOperacion() {
    return refOperacion;
  }

  /**
   * @param refOperacion the refOperacion to set
   */
  public void setRefOperacion(String refOperacion) {
    this.refOperacion = refOperacion;
  }

  /**
   * @return the refCruceTitulares
   */
  public String getRefCruceTitulares() {
    return refCruceTitulares;
  }

  /**
   * @param refCruceTitulares the refCruceTitulares to set
   */
  public void setRefCruceTitulares(String refCruceTitulares) {
    this.refCruceTitulares = refCruceTitulares;
  }	

  /**
   * @return the origenCruce
   */
  public String getOrigenCruce() {
    return origenCruce;
  }

  /**
   * @param origenCruce the origenCruce to set
   */
  public void setOrigenCruce(String origenCruce) {
    this.origenCruce = origenCruce;
  }  

  /**
   * @return the fEnvioAlta
   */
  public Date getfEnvioAlta() {
    return fEnvioAlta;
  }

  /**
   * @param fEnvioAlta the fEnvioAlta to set
   */
  public void setfEnvioAlta(Date fEnvioAlta) {
    this.fEnvioAlta = fEnvioAlta;
  }

  /**
   * @return the fEnvioBaja
   */
  public Date getfEnvioBaja() {
    return fEnvioBaja;
  }

  /**
   * @param fEnvioBaja the fEnvioBaja to set
   */
  public void setfEnvioBaja(Date fEnvioBaja) {
    this.fEnvioBaja = fEnvioBaja;
  }
  
}

