package sibbac.business.contabilidad.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_CONTABILIDAD.NAME,
           name = Task.GROUP_CONTABILIDAD.JOB_APUNTES,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "04:00:00")
public class TaskApuntes extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private ApunteContableBo apunteContableBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void execute() {
    apunteContableBo.put();
  } // execute

} // TaskApuntes
