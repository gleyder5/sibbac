package sibbac.business.contabilidad.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43FicheroDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.model.Norma43Fichero;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.contabilidad.database.model.CuentaBancaria;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE;
import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums;

/**
 * Realiza los apuntes contables de cobros de cánones y corretajes.
 * 
 * @author XI316153
 */
@Service
public class CobrosRouting {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosRouting.class);

  private static final List<Integer> tiposImporteList = new ArrayList<Integer>();

  private static final BigDecimal CIEN_BIG_DECIMAL = new BigDecimal(100);

  /** Indica que un registro norma43 es de adeudo. */
  private static final String ADEUDO = "1";

  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_ROUTING";
  private static final String DESCUADRE_SINREF = "Referencia vacia o nula";
  private static final String DESCUADRE_BARRIDO_NUMOP = "Número de operaciones cobradas en CSV distinto del número de operaciones en estado pendiente de cobro";

  public static final String CSV_CANON = "02";
  public static final String CSV_CORRETAJE = "04";
  public static final String CSV_FECHA = "csvFecha";
  public static final String CSV_NUREFORD_LIST = "listaNureford";
  public static final String ALC_CANON = "alcCanon";
  public static final String ALC_CORRETAJE = "alcCorretaje";
  public static final String ALC_ALIAS = "alcAlias";
  public static final String ALC_FECHA = "alcFecha";
  public static final String NORMA43_CANON = "norma43Canon";
  public static final String NORMA43_CORRETAJE = "norma43Corretaje";
  public static final String NORMA43_LISTAMOV = "norma43ListaMovimientos";
  public static final String CC_CANON_KEY = "CC_CANON";
  public static final String CC_CORRETAJE_KEY = "CC_CORRETAJE";
  public static final String CC_SAN_CANON = "ES6900495033552116069742";
  public static final String CC_SAN_CORRETAJE = "ES1200495033532316069734";
  public static final String CC_OPN_CANON = "ES5400730100510503973028";
  public static final String CC_OPN_CORRETAJE = "ES0600730100500503973165";

  public static final String LIQBARRI_SAN_FILE = "LIQBARRI_SAN";
  public static final String LIQBARRI_OPN_FILE = "LIQBARRI_OPN";
  public static final String LIQCOMIS_SAN_FILE = "LIQCOMIS_SAN";
  public static final String LIQCOMIS_OPN_FILE = "LIQCOMIS_OPN";
  public static final String TST_FILE_EXT = ".TST";
  public static final String CSV_FILE_EXT = ".CSV";

  private Date now;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Campo <code>application</code> de la tabla <code>tmct0cfg</code>. */
  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  /** Margen de diferencia entre el importe Norma43 y calculado de los alc a partir del cual se generará descuadre. */
  @Value("${norma43.margen.descuadres}")
  private BigDecimal margenDescuadres;

  /** Número de elementos a tratar en una transacción. */
  @Value("${cobros.routing.transaction.size}")
  private Integer transactionSize;

  /** Ruta de entrada de ficheros. */
  @Value("${conciliacion.norma43.source.folder}")
  private String sourcePath;

  /** Ruta de destino de ficheros. */
  @Value("${conciliacion.norma43.destination.folder}")
  private String destFolder;

  /** Bussines object para la tabla <code>TMCT0_ESTADO</code>. */
  @Autowired
  private Tmct0estadoBo tmct0estadoBo;

  /** Objeto para el acceso a la tabla <code>TMCT0CFG</code>. */
  @Autowired
  private Tmct0cfgDao tmct0cfgDao;

  @Autowired
  private CobrosRoutingSubList cobrosRoutingSubList;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private CobrosRoutingBarridos cobrosRoutingBarridos;

  /** Objeto para el acceso a la tabla <code>TMCT0_NORMA43_FICHERO</code>. */
  @Autowired
  private Norma43FicheroDao norma43FicheroDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_NORMA43_MOVIMIENTO</code>. */
  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_DESCUADRES_NORMA_43</code>. */
  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  /** Mapa de estados a utilizar durante la ejecución. */
  private HashMap<String, Tmct0estado> estadosMap = new HashMap<String, Tmct0estado>();

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICA

  static {
    tiposImporteList.add(TIPO_IMPORTE.Canon.getTipo());
    tiposImporteList.add(TIPO_IMPORTE.Corretaje.getTipo());
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera los apuntes contables de cobro de operaciones de routing minorista y mayorista.
   * 
   * @param lEstadosAsig Lista de estados en los que pueden estar los ALC a procesar.
   * @throws Exception Si ocurre algún error durante la operación.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void generateCobrosRouting(List<Integer> lEstadosAsig) throws Exception {
    LOG.debug("[generateCobrosRouting] Inicio ...");
    final long startTime = System.currentTimeMillis();

    try {
      loadEstados();
      now = new Date();

      String tmct0cfgAuxiliarPorDefectoRouting = tmct0cfgDao.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                                                  SibbacEnums.Tmct0cfgConfig.CONTABILIDAD_AUXILIAR_CONTABLE_ROUTING.getProcess(),
                                                                                                  SibbacEnums.Tmct0cfgConfig.CONTABILIDAD_AUXILIAR_CONTABLE_ROUTING.getKeyname())
                                                            .getKeyvalue();

      LOG.debug("[generateCobrosRouting] Preparando datos Norma43 ...");
      // Referencia#TipoAbono / CuentaBancaria#Importe
      HashMap<String, HashMap<String, List<Norma43Movimiento>>> listaNorma43PretratadosBarridos = new HashMap<String, HashMap<String, List<Norma43Movimiento>>>();

      HashMap<String, HashMap<String, List<Norma43Movimiento>>> listaNorma43Pretratados = prepareNorma43Data(listaNorma43PretratadosBarridos);

      List<Map.Entry<String, HashMap<String, List<Norma43Movimiento>>>> list = new ArrayList<Map.Entry<String, HashMap<String, List<Norma43Movimiento>>>>(
                                                                                                                                                          listaNorma43Pretratados.entrySet());

      List<Norma43Fichero> ficherosNorma43List = new ArrayList<Norma43Fichero>();
      List<Map.Entry<String, HashMap<String, List<Norma43Movimiento>>>> subList;

      moverFicheroTratado(LIQCOMIS_OPN_FILE);
      moverFicheroTratado(LIQCOMIS_SAN_FILE);

      int size = list.size();
      for (int i = 0; i <= size; i += transactionSize) {
        subList = list.subList(i, Math.min(i + transactionSize, size));
        cobrosRoutingSubList.generateRoutingCobrosSubList(subList, lEstadosAsig, estadosMap,
                                                          tmct0cfgAuxiliarPorDefectoRouting, tiposImporteList,
                                                          margenDescuadres);

        // TODO Esto es un poco BASTANTE chapucilla, habría que mejorarlo
        for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : subList) {
          for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet()) {
            for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
              if (!ficherosNorma43List.contains(norma43Movimiento.getFichero())) {
                ficherosNorma43List.add(norma43Movimiento.getFichero());
              }
            } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
          } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())
        } // for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : list)

      } // for (int i = 0; true; i += transactionSize)

      try {
        this.procesarCobrosBarridos(listaNorma43PretratadosBarridos, lEstadosAsig, tmct0cfgAuxiliarPorDefectoRouting);

        // TODO Esto es un poco BASTANTE chapucilla, habría que mejorarlo
        for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : listaNorma43PretratadosBarridos.entrySet()) {
          for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet()) {
            for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
              if (!ficherosNorma43List.contains(norma43Movimiento.getFichero()) && (norma43Movimiento.isProcesado())) {
                ficherosNorma43List.add(norma43Movimiento.getFichero());
              }
            } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
          } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())
        } // for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : list)
      } catch (Exception e) {
        LOG.error("[generateCobrosRouting] Error generando los apuntes de cobros de barridos routing Norma43 ... "
                      + e.getMessage(), e);
      }

      for (Norma43Fichero norma43Fichero : ficherosNorma43List) {
        norma43Fichero.setProcesado(Boolean.TRUE);
        norma43FicheroDao.save(norma43Fichero);
      }

    } catch (Exception e) {
      LOG.error("[generateCobrosRouting] Error generando los apuntes de cobros de routing Norma43 ... "
                    + e.getMessage(), e);
      throw e;
    } finally {
      final long endTime = System.currentTimeMillis();
      LOG.debug("[generateCobrosRouting] Finalizada generacion de apuntes de cobros de routing Norma43 ..."
                + String.format(" [ %d min y %d sec ]",
                                TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))));
    } // finally
  } // generateCobrosRouting

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Prepara los datos Norma43 no contabilizados para generar los apuntes contables necesarios.
   *
   * @return <code>HashMap<String, HashMap<String, List<Norma43Movimiento>>> - </code>Mapa de datos Norma43 pretratados.
   */
  private HashMap<String, HashMap<String, List<Norma43Movimiento>>> prepareNorma43Data(HashMap<String, HashMap<String, List<Norma43Movimiento>>> listaNorma43PretratadosBarridos) {
    LOG.debug("[prepareNorma43Data] Inicio ...");

    List<Norma43Fichero> listaFicheroNorma43 = null;
    try {
      listaFicheroNorma43 = norma43FicheroDao.findByProcesadoAndTipologiaAndTipoImporteOrderByFechaInicial(Boolean.FALSE,
                                                                                                           TIPOLOGIA.Routing.getTipo(),
                                                                                                           tiposImporteList);
    } catch (Exception e) {
      LOG.error("[prepareNorma43Data] Error obteniendo datos Norma43 de la base de datos ... " + e.getMessage(), e);
      throw e;
    } // catch

    HashMap<String, HashMap<String, List<Norma43Movimiento>>> listaNorma43Pretratados = new HashMap<String, HashMap<String, List<Norma43Movimiento>>>();
    String cuenta = null;
    String referencia = null;

    String encontrado = "";

    String tmpCuenta = null;
    BigDecimal tmpImporte;

    HashMap<String, List<Norma43Movimiento>> tmpMapaNorma43Pretratado;
    List<Norma43Movimiento> tmpListaFicheroNorma43 = null;

    Boolean todosProcesados = Boolean.TRUE;

    for (Norma43Fichero norma43Fichero : listaFicheroNorma43) {
      try {
        todosProcesados = Boolean.TRUE;

        cuenta = new CuentaBancaria(norma43Fichero.getEntidad(), norma43Fichero.getOficina(),
                                    norma43Fichero.getCuenta()).getIban();
        LOG.info("[prepareNorma43Data] Procesando cuenta: '{}'", cuenta);

        for (Norma43Movimiento norma43Movimiento : norma43Fichero.getMovimientoNorma43()) {
          if (!norma43Movimiento.isProcesado()) {
            todosProcesados = Boolean.FALSE;
            try {
              if (norma43Movimiento.getDescReferencia() == null
                  || norma43Movimiento.getDescReferencia().trim().isEmpty()) {
                LOG.debug("[prepareNorma43Data] El movimiento '{}' tiene descReferencia nula", norma43Movimiento);
                generarDescuadre(norma43Movimiento, DESCUADRE_SINREF);
                continue;
              } // if

              referencia = norma43Movimiento.getDebe().concat("#").concat(norma43Movimiento.getDescReferencia().trim());
              LOG.debug("[prepareNorma43Data] referencia: " + referencia);

              if (!(referencia.indexOf("ORMA") != -1 || referencia.indexOf("ORAC") != -1
                    || referencia.indexOf("TRMA") != -1 || referencia.indexOf("CAMA") != -1)) {
                if (Character.isLetter(referencia.charAt(2))) {
                  // Minorista ISIN y Fecha. En principio no hay que acumular
                  // referencia = referencia.concat("#").concat(norma43Movimiento.getReferencia()
                  // .substring(norma43Movimiento.getReferencia().length() - 8));

                  LOG.debug("[prepareNorma43Data] referenciaIsinFechaMinorista: " + referencia);

                  tmpListaFicheroNorma43 = new ArrayList<Norma43Movimiento>();
                  tmpListaFicheroNorma43.add(norma43Movimiento);

                  if (listaNorma43PretratadosBarridos.containsKey(referencia)) {
                    listaNorma43PretratadosBarridos.get(referencia)
                                                   .put(cuenta.concat("#")
                                                              .concat(norma43Movimiento.getImporte()
                                                                                       .divide(CIEN_BIG_DECIMAL)
                                                                                       .toString()),
                                                        tmpListaFicheroNorma43);
                  } else {
                    tmpMapaNorma43Pretratado = new HashMap<String, List<Norma43Movimiento>>();
                    tmpMapaNorma43Pretratado.put(cuenta.concat("#").concat(norma43Movimiento.getImporte()
                                                                                            .divide(CIEN_BIG_DECIMAL)
                                                                                            .toString()),
                                                 tmpListaFicheroNorma43);
                    listaNorma43PretratadosBarridos.put(referencia, tmpMapaNorma43Pretratado);
                  } // else

                  continue;
                } else {
                  // La referencia es por nureford, hay que acumular los importes por cuenta si hay varios
                  // Se le quitan los dos últimos caracteres a la referencia pues son un contador
                  referencia = referencia.substring(0, referencia.length() - 2);
                  LOG.debug("[prepareNorma43Data] referencia Nureford: " + referencia);
                }
              }

              // En todos los demás casos puede que llegue la misma referencia más de una vez por lo que hay que
              // acumular
              if (listaNorma43Pretratados.containsKey(referencia)) {
                tmpMapaNorma43Pretratado = listaNorma43Pretratados.get(referencia);

                for (Map.Entry<String, List<Norma43Movimiento>> entry : tmpMapaNorma43Pretratado.entrySet()) {
                  tmpCuenta = entry.getKey().split("#")[0];
                  if (tmpCuenta.equals(cuenta)) {
                    tmpListaFicheroNorma43 = entry.getValue();
                    tmpListaFicheroNorma43.add(norma43Movimiento);

                    tmpImporte = new BigDecimal(entry.getKey().split("#")[1]);

                    tmpMapaNorma43Pretratado.put(tmpCuenta.concat("#")
                                                          .concat(tmpImporte.add(norma43Movimiento.getImporte()
                                                                                                  .divide(CIEN_BIG_DECIMAL))
                                                                            .toString()), tmpListaFicheroNorma43);

                    // Ya se ha encontrado una cuenta igual, se sale del bucle marcando el elemento modificado
                    encontrado = tmpCuenta.concat("#").concat(tmpImporte.toString());
                    break;
                  } // if (tmpCuenta.equals(cuenta))
                } // for (Map.Entry<String, List<Norma43Fichero>> entry : temp.entrySet())

                if ("".equals(encontrado)) {
                  tmpListaFicheroNorma43 = new ArrayList<Norma43Movimiento>();
                  tmpListaFicheroNorma43.add(norma43Movimiento);
                  tmpMapaNorma43Pretratado.put(cuenta.concat("#").concat(norma43Movimiento.getImporte()
                                                                                          .divide(CIEN_BIG_DECIMAL)
                                                                                          .toString()),
                                               tmpListaFicheroNorma43);
                } else {
                  tmpMapaNorma43Pretratado.remove(encontrado);
                  encontrado = "";
                } // else
              } else {
                tmpListaFicheroNorma43 = new ArrayList<Norma43Movimiento>();
                tmpListaFicheroNorma43.add(norma43Movimiento);

                tmpMapaNorma43Pretratado = new HashMap<String, List<Norma43Movimiento>>();
                tmpMapaNorma43Pretratado.put(cuenta.concat("#").concat(norma43Movimiento.getImporte()
                                                                                        .divide(CIEN_BIG_DECIMAL)
                                                                                        .toString()),
                                             tmpListaFicheroNorma43);
                listaNorma43Pretratados.put(referencia, tmpMapaNorma43Pretratado);
              } // else
            } catch (Exception e) {
              LOG.warn("[prepareNorma43Data] Incidencia pretratando norma43Movimiento ... '" + norma43Movimiento
                       + "' ... " + e.getMessage());
              LOG.warn("[prepareNorma43Data] Stacktrace ... ", e);
              LOG.warn("[prepareNorma43Data] Se genera descuadre ...");

              generarDescuadre(norma43Movimiento, e.getCause().getMessage());

              LOG.warn("[prepareNorma43Data] Se continua con el siguiente movimiento ...");
            } // catch
          } // if (!norma43Movimiento.isProcesado())
        } // for (Norma43Movimiento movimientoNorma43 : ficheroNorma43.getMovimientoNorma43())

        if (todosProcesados) {
          norma43Fichero.setProcesado(Boolean.TRUE);
          norma43FicheroDao.save(norma43Fichero);
        }
      } catch (Exception e) {
        LOG.warn("[prepareNorma43Data] Incidencia pretratando norma43Fichero ... '" + norma43Fichero + "' ... "
                 + e.getMessage());
        LOG.warn("[prepareNorma43Data] Stacktrace ... ", e);
        LOG.warn("[prepareNorma43Data] Se continua con el siguiente fichero ...");
      } // catch
    } // for (Norma43Fichero ficheroNorma43 : listaFicheroNorma43)

    if(listaNorma43Pretratados != null)
        LOG.debug("[prepareNorma43Data] Datos Norma43 pretratados ... {}", listaNorma43Pretratados.toString());

    LOG.debug("[prepareNorma43Data] Fin ...");
    return listaNorma43Pretratados;
  } // prepareNorma43Data

  /**
   * Genera un descuadre con los datos especificados.
   * 
   * @param norma43Movimiento Movimiento Norma43.
   */
  private void generarDescuadre(Norma43Movimiento norma43Movimiento, String comentario) {
    LOG.debug("[generarDescuadre] Inicio ...");
    Norma43DescuadresDTO descuadreNorma43DTO = new Norma43DescuadresDTO();

    descuadreNorma43DTO.setCuenta(String.format("%04d", norma43Movimiento.getFichero().getEntidad())
                                        .concat(String.format("%04d", norma43Movimiento.getFichero().getOficina()))
                                        .concat(String.format("%010d", norma43Movimiento.getFichero().getCuenta())));
    descuadreNorma43DTO.setRefFecha(norma43Movimiento.getReferencia());
    descuadreNorma43DTO.setRefOrden(norma43Movimiento.getDescReferencia() == null ? " "
                                                                                 : norma43Movimiento.getDescReferencia());
    descuadreNorma43DTO.setSentido(norma43Movimiento.getDebe());
    descuadreNorma43DTO.setTradeDate(norma43Movimiento.getTradedate());
    descuadreNorma43DTO.setSettlementDate(norma43Movimiento.getSettlementdate());
    descuadreNorma43DTO.setImporteNorma43(norma43Movimiento.getImporte().divide(CIEN_BIG_DECIMAL));

    descuadreNorma43DTO.setTipoImporte(DESCUADRE_TIPO_IMPORTE);
    descuadreNorma43DTO.setComentario(comentario.length() > 200 ? comentario.substring(0, 200) : comentario);

    norma43DescuadresDao.save(descuadreNorma43DTO.getDescuadreNorma43());

    norma43Movimiento.setProcesado(1);
    norma43MovimientoDao.save(norma43Movimiento);
    LOG.debug("[generarDescuadre] Fin ...");
  } // generarDescuadre

  /**
   * Carga en un mapa los estados utilizados durante el proceso.
   */
  private void loadEstados() {
    LOG.debug("[loadEstados] Fin ...");
    estadosMap.put("estadoContEnCurso", tmct0estadoBo.findById(CONTABILIDAD.EN_CURSO.getId()));
    estadosMap.put("estadoContDevengadaPdteCobro", tmct0estadoBo.findById(CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId()));
    estadosMap.put("estadoContRetrocedida", tmct0estadoBo.findById(CONTABILIDAD.CONTABILIDAD_RETROCEDIDA.getId()));
    estadosMap.put("estadoContCobrado", tmct0estadoBo.findById(CONTABILIDAD.COBRADA.getId()));
    LOG.debug("[loadEstados] Fin ...");
  } // loadEstados

  /**
   * Lee de disco el fichero CSV especificado. <br>
   * Si el fichero TST no existe el método devolverá null para indicar que el CSV, si existiera, no se debe procesar y
   * se debe dejar donde está.<br>
   * Si el fichero TST existe y el CSV, si existiera, contiene sólo la cabecera, se devuelve un mapa vacío para indicar
   * que el TST se debe eliminar y el CSV mover a la carpeta 'tratados'.
   * 
   * @param csvFile Ruta absoluta al fichero a leer.
   * @return <code>Map<String, Map<String, Object>> - </code>Mapa con los datos leídos del fichero.
   * @throws Exception Si ocurre algún error al procesar el fichero.
   */
  public Map<String, Map<String, Object>> readCSV(String file) throws Exception {
    LOG.debug("[readCSV] Inicio ...");
    LOG.info("[readCSV] Leyendo fichero {} ...", file);
    LOG.info("[readCSV] Ruta de lectura: {} ...", sourcePath);

    BufferedReader br = null;
    String line = "";
    String csvSplitBy = ";";

    // Isin-("2" o "4" o "listaNureford")#Objeto
    Map<String, Map<String, Object>> mapaBarrido = new HashMap<String, Map<String, Object>>();
    Map<String, Object> mapaIsin;
    // List<String> nurefordList;

    File tstFile;

    try {
      String[] registro;

      tstFile = new File(sourcePath + System.getProperty("file.separator") + file + TST_FILE_EXT);
      if (tstFile.exists()) {

        br = new BufferedReader(new FileReader(sourcePath + System.getProperty("file.separator") + file + CSV_FILE_EXT));
        while ((line = br.readLine()) != null) {
          if (Character.isDigit(line.charAt(0))) {
            registro = line.split(csvSplitBy);

            if ((mapaIsin = mapaBarrido.get(registro[5])) != null) {
              if (registro[7].compareTo(CSV_CANON) == 0) {
                mapaIsin.put(CSV_CANON,
                             ((BigDecimal) mapaIsin.get(CSV_CANON)).add(new BigDecimal(registro[3].toString()
                                                                                                  .replace(',', '.'))));
              } else if (registro[7].compareTo(CSV_CORRETAJE) == 0) {
                mapaIsin.put(CSV_CORRETAJE,
                             ((BigDecimal) mapaIsin.get(CSV_CORRETAJE)).add(new BigDecimal(
                                                                                           registro[3].toString()
                                                                                                      .replace(',', '.'))));
              } else {
                throw new Exception("Error leyendo registro. Tipo de importe no reconocido [" + registro[7] + "]");
              }

              // nurefordList = (ArrayList<String>) mapaIsin.get(CSV_NUREFORD_LIST);
              // if (!nurefordList.contains(registro[0])) {
              // nurefordList.add(registro[0]);
              // }
              if (!String.valueOf(mapaIsin.get(CSV_NUREFORD_LIST)).contains(registro[0])) {
                mapaIsin.put(CSV_NUREFORD_LIST,
                             String.valueOf(mapaIsin.get(CSV_NUREFORD_LIST))
                                   .concat(",'".concat(String.valueOf(registro[0]).concat("'"))));
              }

            } else {
              mapaIsin = new HashMap<String, Object>(3);

              if (registro[7].compareTo(CSV_CANON) == 0) {
                mapaIsin.put(CSV_CANON, new BigDecimal(registro[3].toString().replace(',', '.')));
                mapaIsin.put(CSV_CORRETAJE, BigDecimal.ZERO);
              } else if (registro[7].compareTo(CSV_CORRETAJE) == 0) {
                mapaIsin.put(CSV_CORRETAJE, new BigDecimal(registro[3].toString().replace(',', '.')));
                mapaIsin.put(CSV_CANON, BigDecimal.ZERO);
              } else {
                throw new Exception("Error leyendo registro. Tipo de importe no reconocido [" + registro[7] + "]");
              }

              mapaIsin.put(CSV_FECHA, registro[2]); // Solo la almacenamos para el primer registro, se supone que todos
                                                    // tienen la misma

              // nurefordList = new ArrayList<String>();
              // nurefordList.add(registro[0]);
              // mapaIsin.put(CSV_NUREFORD_LIST, nurefordList);
              mapaIsin.put(CSV_NUREFORD_LIST, "'".concat(registro[0]).concat("'"));

              mapaBarrido.put(registro[5], mapaIsin);
            }
          } else {
            LOG.info("[readCSV] El primer caracter no es numerico ... {} ", line);
          } // else
        } // while ((line = br.readLine()) != null)
      } else {
        return null;
      } // else

      return mapaBarrido;
    } catch (FileNotFoundException fnfe) {
      throw fnfe;
    } catch (IOException ioe) {
      throw ioe;
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          LOG.warn("[readCSV] Error al cerrar reader: {}", e.getMessage());
        }
      }

      if(mapaBarrido != null)
        LOG.debug("[readCSV] Datos CSV pretratados ... {}", mapaBarrido.toString());

      LOG.debug("[readCSV] Fin ...");
    } // finally
  } // readCSV

  /**
   * 
   * @param listaNorma43PretratadosBarridos
   * @param lEstadosAsig
   * @param tmct0cfgAuxiliarPorDefectoRouting
   * @throws Exception
   */
  @SuppressWarnings("unchecked")
private void procesarCobrosBarridos(HashMap<String, HashMap<String, List<Norma43Movimiento>>> listaNorma43PretratadosBarridos,
                                      List<Integer> lEstadosAsig,
                                      String tmct0cfgAuxiliarPorDefectoRouting) throws Exception {
    LOG.debug("[procesarCobrosBarridos] Inicio ...");

    if (listaNorma43PretratadosBarridos.isEmpty()) {
      moverFicheroTratado(LIQBARRI_OPN_FILE);
      moverFicheroTratado(LIQBARRI_SAN_FILE);
    } else {
      String[] clave;
      Boolean isAbono;
      String isin;
      // Date fechaBarrido;
      String cuenta;
      BigDecimal importeNorma43;

      // HashMap<String, BigDecimal> cuentasimporteMap = new HashMap<String, BigDecimal>();

      Boolean cargarFicheroSan = Boolean.TRUE;
      Boolean consultarIsinSan = Boolean.TRUE;
      Map<String, Map<String, Object>> mapaBarridoSan = null;
      Object[] alcsBarridoSan = null;

      Boolean cargarFicheroOpb = Boolean.TRUE;
      Boolean consultarIsinOpb = Boolean.TRUE;
      Map<String, Map<String, Object>> mapaBarridoOpb = null;
      Object[] alcsBarridoOpb = null;

      // String nurefords = "";

      String estadosAsig = "";
      for (Integer estadoAsig : lEstadosAsig) {
        estadosAsig += String.valueOf(estadoAsig) + ",";
      }
      estadosAsig = estadosAsig.substring(0, estadosAsig.length() - 1);

      List<Norma43Movimiento> tmpList;

      for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : listaNorma43PretratadosBarridos.entrySet()) {
        LOG.debug("[procesarCobrosBarridos] Procesando norma43 {} ...", entry.getKey());
        consultarIsinSan = Boolean.TRUE;
        consultarIsinOpb = Boolean.TRUE;

        clave = entry.getKey().split("#");
        isAbono = clave[0].compareTo(ADEUDO) == 0 ? Boolean.FALSE : Boolean.TRUE;
        LOG.debug("[procesarCobrosBarridos] isAbono: {}", isAbono);
        isin = clave[1];
        LOG.debug("[procesarCobrosBarridos] isin: {}", isin);
        // fechaBarrido = FormatDataUtils.convertStringToDate(clave[2], "yyyyMMdd");
        // LOG.debug("[generateRoutingCobrosBarrido] fechaBarrido: {}", fechaBarrido);

        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet()) {
          LOG.debug("[procesarCobrosBarridos] Procesando cuentaBancaria#importe-norma43 {} ...", valueEntry.getKey());
          clave = valueEntry.getKey().split("#");
          // cuentasimporteMap.put(clave[0], new BigDecimal(clave[1])); // cuentaBancaria-importe
          cuenta = clave[0];
          LOG.debug("[procesarCobrosBarridos] cuenta: {}", cuenta);
          importeNorma43 = new BigDecimal(clave[1]);
          LOG.debug("[procesarCobrosBarridos] importeNorma43: {}", importeNorma43);

          if (clave[0].compareTo(CC_SAN_CANON) == 0 || clave[0].compareTo(CC_SAN_CORRETAJE) == 0) {
            // Barrido de cánones o corretajes SAN
            if (consultarIsinSan) {
              LOG.debug("[procesarCobrosBarridos] Procesando cuenta SAN ...");
              if (cargarFicheroSan) {
                mapaBarridoSan = readCSV(LIQBARRI_SAN_FILE);

                if (mapaBarridoSan == null || mapaBarridoSan.isEmpty()) {
                  continue;
                } else {
                  cargarFicheroSan = Boolean.FALSE;
                }
              } // if

              alcsBarridoSan = tmct0alcDaoImp.sumCanonAndCorretaje(String.valueOf(mapaBarridoSan.get(isin)
                                                                                                .get(CSV_NUREFORD_LIST)),
                                                                   estadosAsig);

              if (Integer.valueOf(alcsBarridoSan[3].toString())
                         .compareTo(String.valueOf(mapaBarridoSan.get(isin).get(CSV_NUREFORD_LIST)).split(",").length) == 0) {
                // El número de operaciones cobradas coinciden con el número de operaciones en estado pendiente de cobro
                mapaBarridoSan.get(isin).put(ALC_CANON, new BigDecimal(alcsBarridoSan[0].toString()));
                mapaBarridoSan.get(isin)
                              .put(ALC_CORRETAJE,
                                   new BigDecimal(alcsBarridoSan[1].toString()).add(new BigDecimal(
                                                                                                   alcsBarridoSan[2].toString())));
                mapaBarridoSan.get(isin).put(ALC_ALIAS, new BigDecimal(alcsBarridoSan[4].toString().trim()));
                mapaBarridoSan.get(isin).put(ALC_FECHA, alcsBarridoSan[5].toString());

                if (clave[0].compareTo(CC_SAN_CANON) == 0) {
                  mapaBarridoSan.get(isin).put(NORMA43_CANON, importeNorma43);
                } else {
                  mapaBarridoSan.get(isin).put(NORMA43_CORRETAJE, importeNorma43);
                }

                mapaBarridoSan.get(isin).put(NORMA43_LISTAMOV, valueEntry.getValue());
                mapaBarridoSan.get(isin).put(CC_CANON_KEY, CC_SAN_CANON);
                mapaBarridoSan.get(isin).put(CC_CORRETAJE_KEY, CC_SAN_CORRETAJE);

                consultarIsinSan = Boolean.FALSE;
              } else {
                for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
                  this.generarDescuadre(norma43Movimiento, DESCUADRE_BARRIDO_NUMOP);
                } // for
                mapaBarridoSan.remove(isin);
              } // else
            } else {
              if (clave[0].compareTo(CC_SAN_CANON) == 0) {
                mapaBarridoSan.get(isin).put(NORMA43_CANON, importeNorma43);
              } else {
                mapaBarridoSan.get(isin).put(NORMA43_CORRETAJE, importeNorma43);
              }

              tmpList = (ArrayList<Norma43Movimiento>) mapaBarridoSan.get(isin).get(NORMA43_LISTAMOV);
              tmpList.addAll(valueEntry.getValue());
              mapaBarridoSan.get(isin).put(NORMA43_LISTAMOV, tmpList);
            } // else
          } else if (clave[0].compareTo(CC_OPN_CANON) == 0 || clave[0].compareTo(CC_OPN_CORRETAJE) == 0) {
            if (consultarIsinOpb) {
              LOG.debug("[procesarCobrosBarridos] Procesando cuenta OPB ...");
              if (cargarFicheroOpb) {
                mapaBarridoOpb = readCSV(LIQBARRI_OPN_FILE);

                if (mapaBarridoOpb == null || mapaBarridoOpb.isEmpty()) {
                  continue;
                } else {
                  cargarFicheroOpb = Boolean.FALSE;
                }
              } // if

              alcsBarridoOpb = tmct0alcDaoImp.sumCanonAndCorretaje(String.valueOf(mapaBarridoOpb.get(isin)
                                                                                                .get(CSV_NUREFORD_LIST)),
                                                                   estadosAsig);

              if (Integer.valueOf(alcsBarridoOpb[3].toString())
                         .compareTo(String.valueOf(mapaBarridoOpb.get(isin).get(CSV_NUREFORD_LIST)).split(",").length) == 0) {
                mapaBarridoOpb.get(isin).put(ALC_CANON, new BigDecimal(alcsBarridoOpb[0].toString()));
                mapaBarridoOpb.get(isin)
                              .put(ALC_CORRETAJE,
                                   new BigDecimal(alcsBarridoOpb[1].toString()).add(new BigDecimal(
                                                                                                   alcsBarridoOpb[2].toString())));
                mapaBarridoOpb.get(isin).put(ALC_ALIAS, new BigDecimal(alcsBarridoOpb[4].toString().trim()));
                mapaBarridoOpb.get(isin).put(ALC_FECHA, alcsBarridoOpb[5].toString());

                if (clave[0].compareTo(CC_OPN_CANON) == 0) {
                  mapaBarridoOpb.get(isin).put(NORMA43_CANON, importeNorma43);
                } else {
                  mapaBarridoOpb.get(isin).put(NORMA43_CORRETAJE, importeNorma43);
                }

                mapaBarridoOpb.get(isin).put(NORMA43_LISTAMOV, valueEntry.getValue());
                mapaBarridoOpb.get(isin).put(CC_CANON_KEY, CC_OPN_CANON);
                mapaBarridoOpb.get(isin).put(CC_CORRETAJE_KEY, CC_OPN_CORRETAJE);

                consultarIsinOpb = Boolean.FALSE;
              } else {
                for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
                  this.generarDescuadre(norma43Movimiento, DESCUADRE_BARRIDO_NUMOP);
                } // for
                mapaBarridoOpb.remove(isin);
              } // else
            } else {
              if (clave[0].compareTo(CC_OPN_CANON) == 0) {
                mapaBarridoOpb.get(isin).put(NORMA43_CANON, importeNorma43);
              } else {
                mapaBarridoOpb.get(isin).put(NORMA43_CORRETAJE, importeNorma43);
              }

              tmpList = (ArrayList<Norma43Movimiento>) mapaBarridoOpb.get(isin).get(NORMA43_LISTAMOV);
              tmpList.addAll(valueEntry.getValue());
              mapaBarridoOpb.get(isin).put(NORMA43_LISTAMOV, tmpList);
            } // else
          } // else if

        } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())
      } // for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry :
        // listaNorma43PretratadosBarridos.entrySet())

      if (mapaBarridoSan != null) {
        for (Map.Entry<String, Map<String, Object>> entry : mapaBarridoSan.entrySet()) {
          cobrosRoutingBarridos.generateCobrosRoutingBarrido(entry, lEstadosAsig, estadosMap,
                                                             tmct0cfgAuxiliarPorDefectoRouting, tiposImporteList);
        } // for (Map.Entry<String, Map<String, Object>> entry : mapaBarridoSan.entrySet())
        moverFicheroTratado(LIQBARRI_SAN_FILE);
      } // if (mapaBarridoSan != null)

      if (mapaBarridoOpb != null) {
        for (Map.Entry<String, Map<String, Object>> entry : mapaBarridoOpb.entrySet()) {
          cobrosRoutingBarridos.generateCobrosRoutingBarrido(entry, lEstadosAsig, estadosMap,
                                                             tmct0cfgAuxiliarPorDefectoRouting, tiposImporteList);
        } // for (Map.Entry<String, Map<String, Object>> entry : mapaBarridoOpb.entrySet())
        moverFicheroTratado(LIQBARRI_OPN_FILE);
      } // if (mapaBarridoOpb != null)
    } // else
  } // procesarCobrosBarridos

  /**
   * Mueve el fichero especificado a la carpeta de ficheros tratados.
   * 
   * @param fichero Fichero a mover.
   * @throws IOException Si ocurre algún error al mover el fichero.
   */
  private void moverFicheroTratado(String file) throws IOException {
    LOG.debug("[moverFicheroTratado] Inicio ...");

    File tstFile = new File(sourcePath, (file + TST_FILE_EXT));
    File sourceFile = new File(sourcePath, (file + CSV_FILE_EXT));
    File destFile = new File(destFolder, (file + CSV_FILE_EXT + "_" + FormatDataUtils.convertDateToFileName(now)));

    if (sourceFile.exists()) {
      if (!sourceFile.renameTo(destFile)) {
        LOG.error("[moverFicheroTratado] Incidencia al mover el fichero renombrado '{}' ", destFile.getAbsolutePath());
        throw new IOException("[moverFicheroTratado] Incidencia al mover el fichero renombrado '"
                              + destFile.getAbsolutePath() + "'");
      } else {
        if (tstFile.exists()) {
          tstFile.delete();
        } // if (tstFile.exists())
      } // else
    } // if (sourceFile.exists())
    LOG.debug("[moverFicheroTratado] Fin ...");
  } // moverFicheroTratado

} // CobrosRouting
