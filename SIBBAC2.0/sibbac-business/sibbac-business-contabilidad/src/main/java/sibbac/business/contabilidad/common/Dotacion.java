package sibbac.business.contabilidad.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.CierreContableDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CierreContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.Tmct0alc;

@Component
public class Dotacion {

	private static final Logger				LOG						= LoggerFactory.getLogger( Dotacion.class );
	private static final List< Integer >	lEstadosAsigDotacion	= new ArrayList< Integer >();
	private static final DateFormat			dF1						= new SimpleDateFormat( "MM/yyyy" );

	@Autowired
	private ApunteContableBo				apunteBo;

	@Autowired
	private ApunteContableAlcDaoImp	apunteAlcDao;

	@Autowired
	private Tmct0alcDao						alcDao;

	@Autowired
	private CuentaContableDao cuentaDao;

	@Autowired
	private CierreContableDao cierreDao;

	static {
		for ( ASIGNACIONES value : ASIGNACIONES.values() ) {
			if (value.getId()>ASIGNACIONES.RECHAZADA.getId() && value.getId()<ASIGNACIONES.LIQUIDADA_TEORICA.getId()) {
				lEstadosAsigDotacion.add( value.getId() );
			}
		}
	}

	private void add(ApunteContable apunteD, Tmct0alc alc) {
		apunteD.add(alc,alc.getImefeagr());
	}

	private void putApunte(ApunteContable apunteD, ApunteContable apunteH) {
		if (apunteD.getImporte().signum()!=0) {
			apunteH.setImporte(apunteD.getImporte().negate());
			apunteBo.save(apunteD);
			apunteBo.save(apunteH);
			for (ApunteContableAlc alc: apunteD.getAlcs()) {
				apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(),
						alc.getAlc().getNucnfclt(), apunteD.getId(), alc.getImporte());
				apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(),
						alc.getAlc().getNucnfclt(), apunteH.getId(), alc.getImporte());
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW) public void put(Calendar hoy, CierreContable cierre) {
		ApunteContable apunteCompraD;
		ApunteContable apunteCompraH;
		ApunteContable apunteVentaD;
		ApunteContable apunteVentaH;
		Map< String, Object > params = new HashMap< String, Object >();
		params.put( "operativa", "N" );
		params.put( "contabilizado", false );
		params.put( "TIPO_COMPROBANTE", TIPO_COMPROBANTE.DOTACION );
		params.put("TIPO_APUNTE", TIPO_APUNTE.EFECTIVO_BRUTO.getId());
		params.put("fechFactura", hoy.getTime());
		params.put( "sApunte", "D" );
		params.put( "concepto", "Compras pendientes de liquidar" );
		params.put( "descripcion", "Compras/" + dF1.format( hoy.getTime() ) );
		params.put( "cuenta", cuentaDao.findByCuenta( 6021103 ) );
		apunteCompraD = new ApunteContable(params);
		params.put( "sApunte", "H" );
		params.put( "cuenta", cuentaDao.findByCuenta( 8011101 ) );
		apunteCompraH = new ApunteContable(params);
		params.put( "sApunte", "D" );
		params.put( "concepto", "Ventas pendientes de liquidar" );
		params.put( "descripcion", "Ventas/" + dF1.format( hoy.getTime() ) );
		params.put( "cuenta", cuentaDao.findByCuenta( 6031103 ) );
		apunteVentaD = new ApunteContable(params);
		params.put( "sApunte", "H" );
		params.put( "cuenta", cuentaDao.findByCuenta( 8011101 ) );
		apunteVentaH = new ApunteContable(params);
		LOG.trace( "Se lanza el método para crear la dotación" );
//		for ( Tmct0alc alc : alcDao.findByEstadosAsig(lEstadosAsigDotacion, new PageRequest(0, 100))) {
		for ( Tmct0alc alc : alcDao.findByEstadosAsig(lEstadosAsigDotacion)) {
			if (alc.getTmct0alo().getCdtpoper()=='C') {
				add(apunteCompraD, alc);
			} else {
				add(apunteVentaD, alc);
			}
		}
		putApunte(apunteCompraD, apunteCompraH);
		putApunte(apunteVentaD, apunteVentaH);
		cierre.setDotado(true);
		cierreDao.save(cierre);
	}
}
