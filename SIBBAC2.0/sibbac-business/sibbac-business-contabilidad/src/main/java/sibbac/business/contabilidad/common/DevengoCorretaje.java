package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.SIBBACBusinessException;

@Component public class DevengoCorretaje {
	protected static final Logger LOG = LoggerFactory.getLogger(DevengoCorretaje.class);
	private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");

	@Autowired private ApunteContableBo apunteBo;
	@Autowired private Devengo devengo;
	@Autowired private ApunteContableAlcDaoImp apunteAlcDao;
	@Autowired private CuentaContableDao cuentaDao;
	@Autowired private Tmct0alcDaoImp alcDao;

	private void putApunte(ApunteContable apunte) {
		if (apunte.getImporte().signum()!=0) {
			BigDecimal multiplicand = "D".equals(apunte.getApunte())? BigDecimal.ONE: BigDecimal.ONE.negate();
			apunteBo.save(apunte);
			for (ApunteContableAlc alc: apunte.getAlcs()) {
				apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(),
						alc.getAlc().getNucnfclt(), apunte.getId(), alc.getImporte().multiply(multiplicand));
			}
		}
	}

	public void save(Map<String, Object> params) throws SIBBACBusinessException {
		BigDecimal comision;
		ApunteContable apunteD;
		ApunteContable apunteH;
		String sFeejeliq;
		CuentaContable cta_1122111 = cuentaDao.findByCuenta(1122111);
		CuentaContable cta_4021101 = cuentaDao.findByCuenta(4021101);
		params.remove("auxiliar");
		params.put("TIPO_COMPROBANTE",TIPO_COMPROBANTE.DEVENGO_FACTURACION);
		params.put("TIPO_APUNTE",TIPO_APUNTE.CORRETAJE.getId());
		params.put("concepto","Devengo Comision");
		for (Entry<Date,List<Tmct0alc>> entry: ((Map<Date, List<Tmct0alc>>) params.get("mBME")).entrySet()) {
			params.put("fechFactura", apunteBo.getFechaContabilizacion((Calendar) params.get("hoy"), entry.getKey()));
			params.put("feejeliq", entry.getKey());
			params.put("sFeejeliq", sFeejeliq = dF1.format(entry.getKey()));
			params.put("descripcion","Corretajes (" + sFeejeliq + ")");
			params.put("sApunte","D");
			params.put("cuenta",cta_1122111);
			(apunteD = new ApunteContable(params)).setImporte(BigDecimal.ZERO);;
			params.put("sApunte","H");
			params.put("cuenta",cta_4021101);
			(apunteH = new ApunteContable(params)).setImporte(BigDecimal.ZERO);;
			for (Tmct0alc alc: entry.getValue()) {
				LOG.debug("Procesando el ALC de corretajes BME {}/{}/{}/{}",alc.getNuorden(),
						alc.getNbooking(),alc.getNucnfclt(),alc.getNucnfliq());
				apunteD.add(alc,comision = devengo.getComision(alc));
				apunteH.add(alc,comision.negate());
				alcDao.escalaAlcDesglose(alc,CONTABILIDAD.DEVENGADA_PDTE_COBRO_BME.getId(),
						TIPO_ESTADO.CORRETAJE_O_CANON);
			}
			putApunte(apunteD);
			putApunte(apunteH);
		}
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void put(Map<String, Object> params) throws Exception {
		Date feejeliq;
		List<Tmct0alc> lBean;
		Map< Date, Map< String, List<Tmct0alc> > > mCliente = new TreeMap< Date, Map<String,List<Tmct0alc>> >();
		Map<Date, List<Tmct0alc>> mBME = new TreeMap<Date,List<Tmct0alc>>();
		Map< String, List<Tmct0alc> > mDia;
		params.put("contabilizado", false);
		params.put("operativa", "N");
		for (Tmct0alc alc: (List<Tmct0alc>) params.get("list")) {
			LOG.debug("Procesando el ALC {}/{}/{}/{}",alc.getNuorden(),
					alc.getNbooking(),alc.getNucnfclt(),alc.getNucnfliq());

			/*
			 * En el caso que se haya cerrado el mes, los apuntes contables deben entrar con fecha del mes
			 * en curso, pero en la descripción del apunte contable conservar la fecha Trade Date
			 */
			if ((lBean = mBME.get(feejeliq = alc.getFeejeliq()))==null) {
				mBME.put(feejeliq, lBean = new ArrayList<Tmct0alc>());
			}
			lBean.add(alc);
			LOG.debug("Procesado el ALC {}/{}/{}/{}",alc.getNuorden(),alc.getNbooking(),alc.getNucnfclt(),alc.getNucnfliq());
		}
		params.put("mBME", mBME);
		save(params);
	}
}
