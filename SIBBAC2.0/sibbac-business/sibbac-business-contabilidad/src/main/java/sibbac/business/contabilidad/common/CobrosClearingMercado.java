package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.ApunteContableDao;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.wrappers.database.dao.Tmct0desgloseCamaraDao;

@Component
public class CobrosClearingMercado {
  private static final Logger LOG = LoggerFactory.getLogger(CobrosClearingMercado.class);
  private static final DateFormat dF = new SimpleDateFormat("yyyy-MM-dd");

  @Autowired
  private ApunteContableDao apunteContableDao;
  @Autowired
  private Norma43MovimientoDao norma43Dao;
  @Autowired
  private Tmct0desgloseCamaraDao desgloseDao;
  @Autowired
  private CuentaContableDao cuentaDao;
  @Autowired
  private ApunteContableAlcDaoImp apunteAlcDao;
  @Autowired
  private AuxiliarDao auxiliarDao;
  private Map<Integer, CuentaContable> mCuenta;

  private CuentaContable find(int cuenta) {
    CuentaContable salida;
    if ((salida = mCuenta.get(cuenta)) == null) {
      mCuenta.put(cuenta, salida = cuentaDao.findByCuenta(cuenta));
    }
    return salida;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void put(Map<String, Object> params) {
    Date date;
    String descReferencia;
    BigDecimal importe;
    BigDecimal imefectivo;
    int titulos;
    Auxiliar auxiliar;
    BigInteger idFichero;
    mCuenta = new HashMap<Integer, CuentaContable>();
    Map<BigInteger, Auxiliar> mAuxiliar = new HashMap<BigInteger, Auxiliar>();
    Map<Long, Long> mFichero = (Map<Long, Long>) params.get("mFichero");
    List<Object[]> list = (List<Object[]>) params.get("list");
    params.put("operativa", "N");
    params.put("contabilizado", false);
    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.LIQUIDACION_CLEARING);
    for (Object[] objects : list) {
      params.put("fechFactura", date = (Date) objects[0]);
      params.put("TIPO_APUNTE", (((BigInteger) objects[1]).intValue() == 1 ? TIPO_APUNTE.EFECTIVO_BRUTO : TIPO_APUNTE.EFECTIVO_NETO).getId());
      params.put("nuorden", objects[4]);
      params.put("nbooking", objects[5]);
      params.put("nucnfclt", objects[6]);
      params.put("nucnfliq", objects[7]);
      LOG.debug("Procesando el cobro de descReferencia: {} y alc: {}/{}/{}/{}", (descReferencia = (String) objects[2]), objects[4], objects[5],
                objects[6], objects[7]);
      importe = (BigDecimal) objects[3];
      imefectivo = (BigDecimal) objects[11];
      titulos = importe.multiply((BigDecimal) objects[9]).divide(imefectivo, 0, BigDecimal.ROUND_HALF_UP).intValue();
      if ((auxiliar = mAuxiliar.get(idFichero = (BigInteger) objects[12])) == null) {
        mAuxiliar.put(idFichero, auxiliar = auxiliarDao.findOne(mFichero.get(idFichero.longValue())));
      }
      if ('C' == (Character) objects[10]) {
        params.put("concepto", "Compras liquidadas mercado");
        params.put("descripcion", descReferencia);
        putApunte(params, "H", find(1021101), importe.negate(), auxiliar);
        params.put("descripcion", dF.format(date) + "/" + titulos + "/Compra/" + objects[8]);
        if (auxiliar.getNombre().compareTo("78") == 0) {
          putApunte(params, "D", find(2012101), importe, null);
        } else {
          putApunte(params, "D", find(1133104), importe, null);
        }
      } else {
        params.put("concepto", "Ventas liquidadas mercado");
        params.put("descripcion", descReferencia);
        putApunte(params, "D", find(1021101), importe, auxiliar);
        params.put("descripcion", dF.format(date) + "/" + titulos + "/Venta/" + objects[8]);
        if (auxiliar.getNombre().compareTo("78") == 0) {
          putApunte(params, "H", find(1022102), importe.negate(), null);
        } else {
          putApunte(params, "H", find(2144601), importe.negate(), null);
        }
      }
      desgloseDao.update(((BigInteger) objects[13]).longValue(), CONTABILIDAD.COBRADA.getId());
      norma43Dao.update(((BigInteger) objects[14]).longValue(), 1);
    }
  }

  private void putApunte(Map<String, Object> params, String sApunte, CuentaContable cuenta, BigDecimal importe, Auxiliar auxiliar) {
    params.put("sApunte", sApunte);
    params.put("cuenta", cuenta);
    params.put("auxiliar", auxiliar);
    ApunteContable apunte = new ApunteContable(params);
    apunte.setImporte(importe);
    apunteContableDao.save(apunte);
    apunteAlcDao.insert((String) params.get("nbooking"), (String) params.get("nuorden"), ((BigDecimal) params.get("nucnfliq")).shortValue(),
                        (String) params.get("nucnfclt"), apunte.getId(), importe);
  }
}
