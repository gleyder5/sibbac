package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.AlcOrdenesDao;
import sibbac.business.wrappers.database.dao.NettingDao;
import sibbac.business.wrappers.database.dao.Tmct0CuentasDeCompensacionDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;

/**
 * @author XI316153
 */

@Component
public class CobrosClearingClienteNetting {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosClearingClienteNetting.class);

  /** Indica si una operación es de una cuenta bruta. */
  private static final String ALC_CUENTA_BRUTA = "2";

  /** Indica si una operacion es de compra. */
  private static final String TPOPER_COMPRA = "C";

  /** Formato de las fechas que se leen del fichero. Ej: 2015-12-24 */
  private static final DateFormat FILE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

  /** Array de tipos de estado que se actualizan durante el proceso. */
  private static TIPO_ESTADO[] tiposEstado;

  private static final String CONCEPTO_APUNTE_COMISION = "Pago comision netting";
  private static final String CONCEPTO_APUNTE_EFECTIVO_COMPRAS = "Compras liquidadas cliente netting";
  private static final String CONCEPTO_APUNTE_EFECTIVO_VENTAS = "Ventas liquidadas cliente netting";
  private static final String CONCEPTO_APUNTE_EFECTIVO = "Liquidacion cliente netting";
  private static final String CONCEPTO_APUNTE_DIFERENCIA = "Diferencias Norma43 netting";
  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_NETTING";
  
  private static final String DESCUACRE_COMENTARIO_DIFMAYOR = "Diferencia entre lo devengado y lo cobrado mayor que margen de tolerancia";
  private static final String DESCUACRE_COMENTARIO_NETEOCERO = "El importe resultante del neteo es cero";
  private static final String DESCUACRE_COMENTARIO_TAMREFNOVALIDA = "La referencia no es de 16 posiciones";
  private static final String DESCUACRE_COMENTARIO_REFNONETTING = "La referencia no es de un cobro de netting";
  private static final String DESCUACRE_COMENTARIO_NETTINGNOENCONTRADO = "Netting no encontrado";
  private static final String DESCUACRE_COMENTARIO_ALCORDENNOENCONTRADOS = "Lista de ALC_ORDEN no encontrado para la referencia recibida";
  private static final String DESCUACRE_COMENTARIO_REFNORECONOCIDA = "Referencia no reconocida";

  private static final Integer CUENTACONTABLE_1133104 = 1133104;
  private static final Integer CUENTACONTABLE_1021101 = 1021101;
  private static final Integer CUENTACONTABLE_2144601 = 2144601;
  private static final Integer CUENTACONTABLE_1122108 = 1122108;
  private static final Integer CUENTACONTABLE_4021102 = 4021102;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private ApunteContableAlcDao apunteContableAlcDao;

  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  @Autowired
  private AlcOrdenesDao alcOrdenesDao;

  @Autowired
  private Tmct0logBo tmct0logBo;

  @Autowired
  private Tmct0CuentasDeCompensacionDao tmct0CuentasDeCompensacionDao;

  @Autowired
  private NettingDao nettingDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;
  
  @Autowired
  private Tmct0alcBo alcBo;

  /** Mapa de cuentas contables. */
  private Map<Integer, CuentaContable> cuentaContableMap;

  /** Lista de descuadres a generar. */
  private List<Norma43DescuadresDTO> norma43DescuadresDTOList;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICO

  static {
    tiposEstado = new TIPO_ESTADO[] { TIPO_ESTADO.CORRETAJE_O_CANON, TIPO_ESTADO.EFECTIVO
    };
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera los apuntes contables de cobro de operaciones de netting de los movimientos norma43 especificados.
   * 
   * @param norma43MovimientoList Lista de movimientos norma43 a procesar.
   * @param mFichero Mapa de auxiliares contables por fichero norma43 a procesar.
   * @param estadosMap Mapa de estados contables.
   * @param flagNorma43ProcesadoTrue Indica que un movimiento norma43 está procesado.
   * @param cienBigDecimal 100 en BigDecimal.
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void processMovimientosNetting(List<Norma43MovimientoDTO> norma43MovimientoList,
                                        Map<Long, Long> mFichero,
                                        Map<Long, BigDecimal> tolerancePorFichero,
                                        HashMap<String, Tmct0estado> estadosMap,
                                        Integer flagNorma43ProcesadoTrue,
                                        BigDecimal cienBigDecimal) throws Exception {
    LOG.debug("[processMovimientosNetting] Inicio ...");

    init();

    Map<Long, Auxiliar> mAuxiliar = new HashMap<Long, Auxiliar>();
    Auxiliar auxiliar;

    String referencia = null;

    Long idNetting = null;
    List<AlcOrdenes> alcOrdenesList = null;
    Netting netting = null;

    for (Norma43MovimientoDTO norma43MovimientoDto : norma43MovimientoList) {
      try {
        if ((auxiliar = mAuxiliar.get(norma43MovimientoDto.getIdFichero())) == null) {
          mAuxiliar.put(norma43MovimientoDto.getIdFichero(), auxiliar = auxiliarDao.findOne(mFichero.get(norma43MovimientoDto.getIdFichero())));
        }

        referencia = norma43MovimientoDto.getDescReferencia().trim();
        LOG.debug("[processMovimientosNetting] Procesando referencia: {}", referencia);
        if (referencia.length() == 16) {
          if (referencia.charAt(0) == 'N') {
            try {
              idNetting = Long.valueOf(referencia.substring(1));
            } catch (Exception e) {
              LOG.warn("[processMovimientosNetting] Referencia no numeria despues de la 'N'. Se genera descuadre ...");
              norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                            DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_REFNORECONOCIDA));
              continue;
            }
            LOG.debug("[processMovimientosNetting] Procesando idNetting: {}", idNetting);

            alcOrdenesList = alcOrdenesDao.findByIdNetting(idNetting);

            if (alcOrdenesList.isEmpty()) {
              LOG.warn("[processMovimientosNetting] No se han encontrado alcOrdenes para el idNetting. Se genera descuadre ...");
              norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                            DESCUADRE_TIPO_IMPORTE, null,
                                                                            DESCUACRE_COMENTARIO_ALCORDENNOENCONTRADOS));
            } else {
              netting = nettingDao.findOne(idNetting);
              if (netting == null) {
                LOG.warn("[processMovimientosNetting] No se han encontrado netting para el idNetting. Se genera descuadre ...");
                norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                              DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_NETTINGNOENCONTRADO));
              } else {
                processNetting(alcOrdenesList, norma43MovimientoDto, auxiliar, tolerancePorFichero.get(norma43MovimientoDto.getIdFichero()),
                               estadosMap, netting, cienBigDecimal);
                nettingDao.save(netting);
              } // else
            } // else
          } else {
            LOG.warn("[processMovimientosNetting] La referencia no es de un cobro de netting. Se genera descuadre ...");
            norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                          DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_REFNONETTING));
          } // else
        } else {
          LOG.warn("[processMovimientosNetting] La referencia no tiene 16 posiciones. Se genera descuadre ...");
          norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                        DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_TAMREFNOVALIDA));
        } // else
      } catch (Exception e) {
        LOG.warn("[processMovimientosNetting] Incidencia generando los apuntes de cobro netting pata cliente ... ", e);
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                      DESCUADRE_TIPO_IMPORTE, null, e.getCause().getMessage()));
        LOG.info("[processMovimientosNetting] Se continua con el siguiente movimiento ...");
      } finally {
        //
      } // finally
    } // for (Norma43MovimientoDTO norma43MovimientoDto : norma43MovimientoList)

    for (Norma43DescuadresDTO norma43DescuadresDTO : norma43DescuadresDTOList) {
      norma43DescuadresDao.save(norma43DescuadresDTO.getDescuadreNorma43());
    } // for

    // Se actualiza el estado del movimiento a procesado
    for (Norma43MovimientoDTO norma43Movimiento : norma43MovimientoList) {
      norma43MovimientoDao.update(norma43Movimiento.getId(), flagNorma43ProcesadoTrue);
    } // for

    LOG.debug("[processMovimientosNetting] Inicio ...");
  } // processMovimientosNetting

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Inicializa los objetos que se utilzan durante el proceso.
   */
  private void init() {
    cuentaContableMap = new HashMap<Integer, CuentaContable>();
    norma43DescuadresDTOList = new ArrayList<Norma43DescuadresDTO>();
  } // init

  /**
   * Procesa el movimiento Norma43 de netting especificado.
   * 
   * @param alcOrdenesList Lista de ALC_ORDEN a tratar.
   * @param norma43MovimientoDto Movimiento Norma43 a tratar.
   * @param auxiliar Auxiliar contable para los apuntes a la cuenta de bancos.
   * @param tolerance Margen de tolerancia para la cuenta de bancos del apunte norma43.
   * @param estadosMap Mapa de estados contables.
   * @param netting Objeto Netting para la referencia del movimiento Norma43 a tratar.
   * @param cienBigDecimal 100 en BigDecimal.
   * @throws ParseException Si ocurre algún error al generar la fecha de los apuntes contables.
   * @throws SIBBACBusinessException Si ocurre algún error al establecer el estado contable del ALC.
   */
  private void processNetting(List<AlcOrdenes> alcOrdenesList,
                              Norma43MovimientoDTO norma43MovimientoDto,
                              Auxiliar auxiliar,
                              BigDecimal tolerance,
                              HashMap<String, Tmct0estado> estadosMap,
                              Netting netting,
                              BigDecimal cienBigDecimal) throws ParseException, SIBBACBusinessException {
    LOG.debug("[processNetting] Inicio ...");
    LOG.debug("[processNetting] tolerance: '{}'", tolerance);

    String descReferencia = norma43MovimientoDto.getDescReferencia().trim();

    // Importe recibido
    BigDecimal importeNorma43 = norma43MovimientoDto.getImporte().divide(cienBigDecimal, 6, RoundingMode.HALF_UP);
    BigDecimal porApuntarNorma43 = importeNorma43;
    LOG.debug("[processNetting] importeNorma43: '{}'", importeNorma43);

    // Efectivo a cobrar
    BigDecimal efectivoAlcsCompras = BigDecimal.ZERO;
    BigDecimal efectivoAlcsVentas = BigDecimal.ZERO;

    // Comisión a cobrar
    BigDecimal comisionAlcsCompras = BigDecimal.ZERO;
    BigDecimal comisionAlcsVentas = BigDecimal.ZERO;

    // BigDecimal yaCobradoAlcs = BigDecimal.ZERO;

    Tmct0alo tmct0alo = null;
    Tmct0alc tmct0alc = null;
    Tmct0alcId alcId;
    List<Tmct0alc> tmct0alcList = new ArrayList<Tmct0alc>();
    List<Tmct0alc> tmct0alcListCompras = new ArrayList<Tmct0alc>();
    List<Tmct0alc> tmct0alcListVentas = new ArrayList<Tmct0alc>();

    for (AlcOrdenes alcOrden : alcOrdenesList) {
      LOG.debug("[processNetting] Procesando ALC_ORDEN: '{}'", alcOrden.getId());
      alcId = alcOrden.createAlcIdFromFields();
      tmct0alc = alcBo.findById(alcId);
      if (tmct0alc != null) {
        LOG.debug("[processNetting] El ALC esta en estado: '{}'", tmct0alc.getEstadoentrec().getNombre());
        tmct0alo = tmct0alc.getTmct0alo();
        if (TPOPER_COMPRA.equals(String.valueOf(tmct0alo.getCdtpoper()))) {
          tmct0alcListCompras.add(tmct0alc);
          efectivoAlcsCompras = efectivoAlcsCompras.add(tmct0alc.getImefeagr());
          comisionAlcsCompras = comisionAlcsCompras.add(tmct0alc.getImfinsvb());
        } else {
          tmct0alcListVentas.add(tmct0alc);
          efectivoAlcsVentas = efectivoAlcsVentas.add(tmct0alc.getImefeagr());
          comisionAlcsVentas = comisionAlcsVentas.add(tmct0alc.getImfinsvb());
        } // else
        // yaApuntadoAlcs = yaApuntadoAlcs.add(tmct0alc.getImcobrado() != null ? tmct0alc.getImcobrado() : BigDecimal.ZERO);
        tmct0alcList.add(tmct0alc);
      } else {
        // FIXME Es esto lo que hay que hacer o hay que generar descuadre para todo el netting y pasar al siguiente ???
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                      DESCUADRE_TIPO_IMPORTE, null, "El ALC_ORDEN '" + alcOrden.getId()
                                                                                                    + "' no referencia a ningun ALC"));
      } // else
    } // for (AlcOrdenes alcOrden : alcOrdenesList)

    // tmct0alcList.addAll(tmct0alcListCompras);
    // tmct0alcList.addAll(tmct0alcListVentas);
    LOG.debug("[processNetting] efectivoAlcsCompras (imefeagr): '{}'", efectivoAlcsCompras);
    LOG.debug("[processNetting] comisionAlcsCompras (imfinsvb): '{}'", comisionAlcsCompras);
    LOG.debug("[processNetting] efectivoAlcsVentas (imefeagr): '{}'", efectivoAlcsVentas);
    LOG.debug("[processNetting] comisionAlcsVentas (imfinsvb): '{}'", comisionAlcsVentas);
    // LOG.debug("[processNetting] yaApuntadoAlcs: '{}'", yaApuntadoAlcs);

    // Isin de las operaciones
    String isin = tmct0alo.getCdisin();
    LOG.debug("[processNetting] isin: '{}'", isin);

    // Indica si el neteo está liquidada totalmente
    Boolean isLiquidado = netting == null ? Boolean.FALSE : netting.getLiquidado().compareTo('S') == 0 ? Boolean.TRUE : Boolean.FALSE;
    // O
    // Boolean isLiquidado = Boolean.FALSE;
    // for (Tmct0alc tmct0alcTmp : tmct0alcList) {
    // if (tmct0alcTmp.getEstadoentrec().getIdestado().compareTo(CLEARING.LIQUIDADA_PARCIAL.getId()) != 0) {
    // isLiquidado = Boolean.TRUE;
    // } else {
    // isLiquidado = Boolean.FALSE;
    // } // else
    // } // for
    LOG.debug("[processNetting] isLiquidado: '{}'", isLiquidado);

    // Fecha de los apuntes
    Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), FILE_DATE_FORMAT.parse(norma43MovimientoDto.getTradedate()));
    LOG.debug("[processNetting] fechaApunte: '{}'", fechaApunte);

    // Importe ya apuntado anteriormente
    BigDecimal yaApuntadoNetting = netting.getImcobrado() != null ? netting.getImcobrado() : BigDecimal.ZERO;
    LOG.debug("[processNetting] yaApuntadoNetting: '{}'", yaApuntadoNetting);

    // Total a apuntar de las operaciones de compra (efectivo + comisión)
    BigDecimal totalAApuntarCompras = efectivoAlcsCompras.add(comisionAlcsCompras);
    LOG.debug("[processNetting] totalAApuntarCompras: '{}'", totalAApuntarCompras);

    // Total a apuntar de las operaciones de venta (efectivo - comisión)
    BigDecimal totalAApuntarVentas = efectivoAlcsVentas.subtract(comisionAlcsVentas);
    LOG.debug("[processNetting] totalAApuntarVentas: '{}'", totalAApuntarVentas);

    // Total a apuntar netting
    BigDecimal totalAApuntarNetting = BigDecimal.ZERO;
    totalAApuntarNetting = totalAApuntarCompras.subtract(totalAApuntarVentas);
    LOG.debug("[processNetting] totalAApuntarNetting: '{}'", totalAApuntarNetting);

    // Indica si el neteo resulta en una operación de compra o de venta
    Boolean isCompra;
    if (totalAApuntarNetting.signum() > 0) {
      isCompra = Boolean.TRUE;
    } else if (totalAApuntarNetting.signum() < 0) {
      isCompra = Boolean.FALSE;
    } else {
      // FIXME Es esto lo que hay que hacer ???
      for (Tmct0alc tmct0alcTmp : tmct0alcList) {
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, totalAApuntarNetting,
                                                                      estadoDao.findOne(tmct0alcTmp.getCdestadoasig()).getNombre(),
                                                                      tmct0alcTmp.getEstadocont().getNombre(), tmct0alcTmp.getEstadoentrec()
                                                                                                                          .getNombre(),
                                                                      tmct0alcTmp.getNuorden(), tmct0alcTmp.getNbooking(), tmct0alcTmp.getNucnfclt(),
                                                                      tmct0alcTmp.getNucnfliq(), DESCUADRE_TIPO_IMPORTE, null,
                                                                      DESCUACRE_COMENTARIO_NETEOCERO));
      } // for
      return;
    } // else
    LOG.debug("[processNetting] isCompra: '{}'", isCompra);

    // Efectivo a apuntar
    BigDecimal efectivoAlcs = efectivoAlcsCompras.subtract(efectivoAlcsVentas);
    LOG.debug("[processNetting] efectivoAlcs: '{}'", efectivoAlcs);

    // Comisión a apuntar
    BigDecimal comisionAlcs = comisionAlcsCompras.add(comisionAlcsVentas);
    LOG.debug("[processNetting] comisionAlcs: '{}'", comisionAlcs);

    // Estado de cobros
    // Si el neteo resulta en una operación de venta la variable efectivoAlcs puede tener un número negativo. Aunque sea negativo es un importe a
    // apuntar y lo quiero positivo
    HashMap<String, BigDecimal> estadoCobros = obtenerEstadoCobros(yaApuntadoNetting, efectivoAlcs.abs(), comisionAlcsCompras, comisionAlcsVentas);

    // Total recibido en el norma43 actual más los posibles norma43 parciales anteriores
    BigDecimal totalRecibidoNorma43 = porApuntarNorma43.add(yaApuntadoNetting);
    LOG.debug("[processMovimiento] totalRecibidoNorma43: '{}'", totalRecibidoNorma43);

    // Positivo si el cliente paga de más
    // Negativo si el cliente paga de menos
    BigDecimal diferencia = totalRecibidoNorma43.subtract(totalAApuntarNetting);
    LOG.debug("[processMovimiento] diferencia: '{}'", diferencia);

    String tipoCuentaCompensacion = String.valueOf(tmct0CuentasDeCompensacionDao.findByCdCodigo(netting.getCdcuentacompensacion())
                                                                                .getTipoNeteoTitulo().getId());

    if (diferencia.signum() >= 0 || isLiquidado) {
      // Si nos han pagado de más o la operación ya está liquidada
      if (diferencia.abs().compareTo(tolerance) > 0) {
        // Si la diferencia supera el margen de tolerancia por cuenta bancaria
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, totalAApuntarNetting.subtract(yaApuntadoNetting),
                                                                      estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre(),
                                                                      tmct0alc.getEstadocont().getNombre(), tmct0alc.getEstadoentrec().getNombre(),
                                                                      tmct0alc.getNuorden(), tmct0alc.getNbooking(), tmct0alc.getNucnfclt(),
                                                                      tmct0alc.getNucnfliq(), DESCUADRE_TIPO_IMPORTE, diferencia.abs(),
                                                                      DESCUACRE_COMENTARIO_DIFMAYOR));
        return;
      } // if (diferencia.abs().compareTo(tolerance) > 0)
      generateApuntesEfectivo(fechaApunte, porApuntarNorma43, isin, auxiliar, descReferencia, isCompra, estadoCobros.get("porCobrarEfectivo"),
                              estadoCobros.get("yaCobradoEfectivo").signum() > 0 ? BigDecimal.ZERO : isCompra ? efectivoAlcsVentas
                                                                                                             : efectivoAlcsCompras,
                              tmct0alcListCompras, tmct0alcListVentas, tipoCuentaCompensacion);
      generateApuntesComision(fechaApunte, estadoCobros.get("porCobrarComisionCompras").negate(), tmct0alcListCompras,
                              estadoCobros.get("porCobrarComisionVentas").negate(), tmct0alcListVentas, descReferencia);
      generateApunteDiferencia(diferencia, descReferencia, fechaApunte, tipoCuentaCompensacion, tmct0alcList, isCompra);
    } else {
      // Si no nos han pagado todo y la operación no está liquidada
      if (porApuntarNorma43.compareTo(estadoCobros.get("porCobrarEfectivo")) > 0) {
        // Si con lo cobrado da para cobrar todo el efectivo pendiente de cobrar
        generateApuntesEfectivo(fechaApunte, porApuntarNorma43, isin, auxiliar, descReferencia, isCompra, estadoCobros.get("porCobrarEfectivo"),
                                estadoCobros.get("yaCobradoEfectivo").signum() > 0 ? BigDecimal.ZERO : isCompra ? efectivoAlcsVentas
                                                                                                               : efectivoAlcsCompras,
                                tmct0alcListCompras, tmct0alcListVentas, tipoCuentaCompensacion);

        porApuntarNorma43 = porApuntarNorma43.subtract(estadoCobros.get("porCobrarEfectivo"));

        if (porApuntarNorma43.compareTo(estadoCobros.get("porCobrarComisionCompras")) <= 0) {
          // Hay que controlar el signo de la comisión porque la resta anterior siempre va a ser un número positivo
          generateApuntesComision(fechaApunte, comisionAlcsCompras.signum() < 0 ? porApuntarNorma43 : porApuntarNorma43.negate(),
                                  tmct0alcListCompras, BigDecimal.ZERO, tmct0alcListVentas, descReferencia);
        } else {
          generateApuntesComision(fechaApunte, estadoCobros.get("porCobrarComisionCompras"), tmct0alcListCompras, BigDecimal.ZERO,
                                  tmct0alcListVentas, descReferencia);

          porApuntarNorma43 = porApuntarNorma43.subtract(estadoCobros.get("porCobrarComisionCompras").abs());
          generateApuntesComision(fechaApunte, BigDecimal.ZERO, tmct0alcListCompras, comisionAlcsVentas.signum() < 0 ? porApuntarNorma43
                                                                                                                    : porApuntarNorma43.negate(),
                                  tmct0alcListVentas, descReferencia);
        }
      } else {
        // Si con lo cobrado no da para cobrar todo el efectivo pendiente de cobrar
        generateApuntesEfectivo(fechaApunte, porApuntarNorma43, isin, auxiliar, descReferencia, isCompra, porApuntarNorma43,
                                estadoCobros.get("yaCobradoEfectivo").signum() > 0 ? BigDecimal.ZERO : isCompra ? efectivoAlcsVentas
                                                                                                               : efectivoAlcsCompras,
                                tmct0alcListCompras, tmct0alcListVentas, tipoCuentaCompensacion);
        // porApuntarNorma43 = BigDecimal.ZERO; // No hace falta
      } // else
    } // else

    // TODO hasta aqui

    // TODO Hay que revisar los métodos de actualización de estados en la contabilidad para usar la tabla TMCT0_ALC_ESTADOCONT fallan más que una
    // escopeta de feria. Con el insertCdestadocont funciona
    if (isLiquidado) {
      LOG.debug("[processMovimiento] Estableciendo estado CONTABILIDAD.COBRADA ...");
      for (Tmct0alc tmct0alc2 : tmct0alcList) {
        // tmct0alc.setImcobrado(yaCobradoAlc.add(importeNorma43));
        tmct0alcDaoImp.escalaEstadoAlc(tmct0alc2, estadosMap.get("estadoContCobrada"), estadosMap.get("enCurso"), tiposEstado);
        alcEstadoContDaoImp.updateOrInsertCdestadocont(estadosMap.get("estadoContCobrada").getIdestado(), tmct0alc2.getId().getNbooking(),
                                               tmct0alc2.getId().getNuorden(), tmct0alc2.getId().getNucnfliq(), tmct0alc2.getId().getNucnfclt(),
                                               TIPO_ESTADO.EFECTIVO.getId());
        try {
          tmct0logBo.insertarRegistro(tmct0alc2,
                                      null,
                                      null,
                                      null,
                                      tmct0alc2.getEstadoentrec(),
                                      "Recibido Norma43. Cta: "
                                          + String.format("%04d", norma43MovimientoDto.getEntidad())
                                                  .concat(String.format("%04d", norma43MovimientoDto.getOficina()))
                                                  .concat(String.format("%010d", norma43MovimientoDto.getCuenta())) + ". Importe Norma43: '"
                                          + importeNorma43 + "'");
        } catch (Exception ex) {
          LOG.warn("[processMovimiento] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } // for
    } else {
      LOG.debug("[processMovimiento] Estableciendo estado CONTABILIDAD.PARCIALMENTE_COBRADA ...");
      for (Tmct0alc tmct0alc2 : tmct0alcList) {
        tmct0alcDaoImp.escalaEstadoAlc(tmct0alc2, estadosMap.get("estadoContParcialmenteCobrada"), estadosMap.get("enCurso"), tiposEstado);
        alcEstadoContDaoImp.updateOrInsertCdestadocont(estadosMap.get("estadoContParcialmenteCobrada").getIdestado(), tmct0alc2.getId().getNbooking(),
                                               tmct0alc2.getId().getNuorden(), tmct0alc2.getId().getNucnfliq(), tmct0alc2.getId().getNucnfclt(),
                                               TIPO_ESTADO.EFECTIVO.getId());
        try {
          tmct0logBo.insertarRegistro(tmct0alc2,
                                      null,
                                      null,
                                      null,
                                      tmct0alc2.getEstadoentrec(),
                                      "Recibido Norma43. Cta: "
                                          + String.format("%04d", norma43MovimientoDto.getEntidad())
                                                  .concat(String.format("%04d", norma43MovimientoDto.getOficina()))
                                                  .concat(String.format("%010d", norma43MovimientoDto.getCuenta())) + ". Importe Norma43: '"
                                          + importeNorma43 + "");
        } catch (Exception ex) {
          LOG.warn("[processMovimiento] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } // for
    } // else

    netting.setImcobrado(yaApuntadoNetting.add(importeNorma43));

    LOG.debug("[processMovimiento] Fin ...");
  } // processMovimiento

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Genera los apuntes contables de efectivo.
   * 
   * @param fecha Fecha del apunte contable.
   * @param importeCobrado Importe cobrado.
   * @param isin Isin de la operación.
   * @param auxiliar Auxiliar contable para los apuntes a la cuenta de bancos.
   * @param descReferencia Referencia S3 de la operación.
   * @param isCompra Indica si el netting resulta en una operación de compra.
   * @param importeEfectivoRestante Importe a devengar.
   * @param importeDevengadoMenor Importe a devengar de las operaciones con menor importe.
   * @param tmct0alcComprasList Lista de ALC de las operaciones de compra.
   * @param tmct0alcVentasList Lista de ALC de las operaciones de venta.
   * @param tipoCuentaCompensacion Tipo de cuenta de compensación.
   */
  private void generateApuntesEfectivo(Date fecha,
                                       BigDecimal importeCobrado,
                                       String isin,
                                       Auxiliar auxiliar,
                                       String descReferencia,
                                       Boolean isCompra,
                                       BigDecimal importeEfectivoRestante,
                                       BigDecimal importeEfectivoMenor,
                                       List<Tmct0alc> tmct0alcComprasList,
                                       List<Tmct0alc> tmct0alcVentasList,
                                       String tipoCuentaCompensacion) {
    LOG.debug("[generateApuntesEfectivo] Inicio ...");
    LOG.debug("[generateApuntesEfectivo] Apuntando: [ fecha: '{}'; importeCobrado: '{}'; importeEfectivoRestante: '{}'; importeEfectivoMenor: '{}' ]",
              fecha, importeCobrado, importeEfectivoRestante, importeEfectivoMenor);

    Tmct0alc tmct0alcTmp = tmct0alcComprasList.get(0);
    BigDecimal efectivo;

    ApunteContable apunteEfectivoCompras = new ApunteContable();
    ApunteContable apunteEfectivoVentas = new ApunteContable();
    ApunteContable apunteNeto = new ApunteContable();

    apunteEfectivoCompras.setFecha(fecha);
    apunteEfectivoCompras.setContabilizado(Boolean.FALSE);
    apunteEfectivoCompras.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteEfectivoCompras.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
    apunteEfectivoCompras.setTipoMovimiento(ALC_CUENTA_BRUTA.compareTo(tipoCuentaCompensacion) == 0 ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                                   : TIPO_APUNTE.EFECTIVO_NETO.getId());

    apunteEfectivoVentas.setFecha(fecha);
    apunteEfectivoVentas.setContabilizado(Boolean.FALSE);
    apunteEfectivoVentas.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteEfectivoVentas.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
    apunteEfectivoVentas.setTipoMovimiento(ALC_CUENTA_BRUTA.compareTo(tipoCuentaCompensacion) == 0 ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                                  : TIPO_APUNTE.EFECTIVO_NETO.getId());

    apunteNeto.setFecha(fecha);
    apunteNeto.setContabilizado(Boolean.FALSE);
    apunteNeto.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteNeto.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
    apunteNeto.setTipoMovimiento(ALC_CUENTA_BRUTA.compareTo(tipoCuentaCompensacion) == 0 ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                        : TIPO_APUNTE.EFECTIVO_NETO.getId());

    if (isCompra) {
      if (importeCobrado.signum() != 0) {
        apunteNeto.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
        apunteNeto.setAuxiliar(auxiliar);
        apunteNeto.setDescripcion(descReferencia);
        apunteNeto.setConcepto(CONCEPTO_APUNTE_EFECTIVO);
        apunteNeto.setImporte(importeCobrado);
        apunteNeto.setApunte(TIPO_CUENTA.DEBE.getId());
        apunteContableBo.save(apunteNeto);

        for (Tmct0alc tmct0alc : tmct0alcComprasList) {
          apunteContableAlcDao.save(new ApunteContableAlc(apunteNeto, tmct0alc, importeCobrado));
        }

        for (Tmct0alc tmct0alc : tmct0alcVentasList) {
          apunteContableAlcDao.save(new ApunteContableAlc(apunteNeto, tmct0alc, importeCobrado));
        }
      } // if (importeCobrado.signum() != 0)

      if (importeEfectivoMenor.signum() != 0) {
        apunteEfectivoVentas.setCuentaContable(findCuentaContable(CUENTACONTABLE_2144601));
        apunteEfectivoVentas.setDescripcion(tmct0alcTmp.getFeejeliq() + "/Ventas/" + isin);
        apunteEfectivoVentas.setConcepto(CONCEPTO_APUNTE_EFECTIVO_VENTAS);
        apunteEfectivoVentas.setImporte(importeEfectivoMenor);
        apunteEfectivoVentas.setApunte(TIPO_CUENTA.DEBE.getId());
        apunteContableBo.save(apunteEfectivoVentas);
        for (Tmct0alc tmct0alc : tmct0alcVentasList) {
          apunteContableAlcDao.save(new ApunteContableAlc(apunteEfectivoVentas, tmct0alc, importeEfectivoMenor));
        }

        efectivo = importeEfectivoMenor.add(importeEfectivoRestante).negate();
        apunteEfectivoCompras.setImporte(efectivo);

      } else {
        efectivo = importeEfectivoRestante.negate();
        apunteEfectivoCompras.setImporte(efectivo);
      } // else

      apunteEfectivoCompras.setCuentaContable(findCuentaContable(CUENTACONTABLE_1133104));
      apunteEfectivoCompras.setDescripcion(tmct0alcTmp.getFeejeliq() + "/Compras/" + isin);
      apunteEfectivoCompras.setConcepto(CONCEPTO_APUNTE_EFECTIVO_COMPRAS);
      apunteEfectivoCompras.setApunte(TIPO_CUENTA.HABER.getId());
      apunteContableBo.save(apunteEfectivoCompras);
      for (Tmct0alc tmct0alc : tmct0alcComprasList) {
        apunteContableAlcDao.save(new ApunteContableAlc(apunteEfectivoCompras, tmct0alc, efectivo));
      }
    } else {
      if (importeCobrado.signum() != 0) {
        apunteNeto.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
        apunteNeto.setAuxiliar(auxiliar);
        apunteNeto.setDescripcion(descReferencia);
        apunteNeto.setConcepto(CONCEPTO_APUNTE_EFECTIVO_VENTAS);
        apunteNeto.setImporte(importeCobrado.negate());
        apunteNeto.setApunte(TIPO_CUENTA.HABER.getId());
        apunteContableBo.save(apunteNeto);

        for (Tmct0alc tmct0alc : tmct0alcComprasList) {
          apunteContableAlcDao.save(new ApunteContableAlc(apunteNeto, tmct0alc, importeCobrado.negate()));
        }

        for (Tmct0alc tmct0alc : tmct0alcVentasList) {
          apunteContableAlcDao.save(new ApunteContableAlc(apunteNeto, tmct0alc, importeCobrado.negate()));
        }
      } // if (importeCobrado.signum() != 0)

      if (importeEfectivoMenor.signum() != 0) {
        apunteEfectivoCompras.setCuentaContable(findCuentaContable(CUENTACONTABLE_1133104));
        apunteEfectivoCompras.setDescripcion(tmct0alcTmp.getFeejeliq() + "/Compras/" + isin);
        apunteEfectivoCompras.setConcepto(CONCEPTO_APUNTE_EFECTIVO_COMPRAS);
        apunteEfectivoCompras.setImporte(importeEfectivoMenor.negate());
        apunteEfectivoCompras.setApunte(TIPO_CUENTA.HABER.getId());
        apunteContableBo.save(apunteEfectivoCompras);
        for (Tmct0alc tmct0alc : tmct0alcComprasList) {
          apunteContableAlcDao.save(new ApunteContableAlc(apunteEfectivoCompras, tmct0alc, importeEfectivoMenor.negate()));
        }

        efectivo = importeEfectivoMenor.add(importeEfectivoRestante);
        apunteEfectivoVentas.setImporte(efectivo);
      } else {
        efectivo = importeEfectivoRestante;
        apunteEfectivoVentas.setImporte(efectivo);
      }

      apunteEfectivoVentas.setCuentaContable(findCuentaContable(CUENTACONTABLE_2144601));
      apunteEfectivoVentas.setDescripcion(tmct0alcTmp.getFeejeliq() + "/Ventas/" + isin);
      apunteEfectivoVentas.setConcepto(CONCEPTO_APUNTE_EFECTIVO_VENTAS);
      apunteEfectivoVentas.setApunte(TIPO_CUENTA.DEBE.getId());
      apunteContableBo.save(apunteEfectivoVentas);
      for (Tmct0alc tmct0alc : tmct0alcVentasList) {
        apunteContableAlcDao.save(new ApunteContableAlc(apunteEfectivoVentas, tmct0alc, efectivo));
      }
    } // else
    LOG.debug("[generateApuntesEfectivo] Fin ...");
  } // generateApuntesEfectivo

  /**
   * Genera el apunte contable de comisión.
   * 
   * @param fecha Fecha del apunte contable.
   * @param importeComisionCompras Importe a contabilizar de las operaciones de compra.
   * @param tmct0alcComprasList Lista de ALC de las operaciones de compra.
   * @param importeComisionVentas Importe a contabilizar de las operaciones de venta.
   * @param tmct0alcVentasList Lista de ALC de las operaciones de venta.
   * @param descReferencia Referencia S3 de la operación.
   */
  private void generateApuntesComision(Date fecha,
                                       BigDecimal importeComisionCompras,
                                       List<Tmct0alc> tmct0alcComprasList,
                                       BigDecimal importeComisionVentas,
                                       List<Tmct0alc> tmct0alcVentasList,
                                       String descReferencia) {
    LOG.debug("[generateApuntesComision] Inicio ...");

    ApunteContable apunte_haber;

    if (importeComisionCompras.signum() != 0) {
      LOG.debug("[generateApuntesComision] Apuntando: [ fecha: '{}'; importeComisionCompras: '{}'; descReferencia: '{}']", fecha,
                importeComisionCompras, descReferencia);
      apunte_haber = new ApunteContable();
      apunte_haber.setCuentaContable(findCuentaContable(CUENTACONTABLE_1122108));
      apunte_haber.setConcepto(CONCEPTO_APUNTE_COMISION);
      apunte_haber.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
      apunte_haber.setDescripcion(descReferencia);
      apunte_haber.setFecha(fecha);
      apunte_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_haber.setContabilizado(Boolean.FALSE);
      apunte_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_haber.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
      apunte_haber.setImporte(importeComisionCompras);
      apunteContableBo.save(apunte_haber);
      for (Tmct0alc tmct0alc : tmct0alcComprasList) {
        apunteContableAlcDao.save(new ApunteContableAlc(apunte_haber, tmct0alc, importeComisionCompras));
      }
    } // if

    if (importeComisionVentas.signum() != 0) {
      LOG.debug("[generateApuntesComision] Apuntando: [ fecha: '{}'; importeComisionVentas: '{}'; descReferencia: '{}']", fecha,
                importeComisionVentas, descReferencia);
      apunte_haber = new ApunteContable();
      apunte_haber.setCuentaContable(findCuentaContable(CUENTACONTABLE_1122108));
      apunte_haber.setConcepto(CONCEPTO_APUNTE_COMISION);
      apunte_haber.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
      apunte_haber.setDescripcion(descReferencia);
      apunte_haber.setFecha(fecha);
      apunte_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_haber.setContabilizado(Boolean.FALSE);
      apunte_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_haber.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
      apunte_haber.setImporte(importeComisionVentas);
      apunteContableBo.save(apunte_haber);
      for (Tmct0alc tmct0alc : tmct0alcVentasList) {
        apunteContableAlcDao.save(new ApunteContableAlc(apunte_haber, tmct0alc, importeComisionVentas));
      }
    } // if

    LOG.debug("[generateApuntesComision] Fin ...");
  } // generateApuntesComision

  /**
   * Genera el apunte de diferencia entre lo cobrado y lo que se tenía que cobrar.
   * 
   * @param diferencia Importe del apunte.
   * @param descReferencia Referencia del apunte.
   * @param fecha Fecha del apunte.
   * @param tipoCuentaCompensacion Tipo de cuenta de compensación.
   * @param tmct0alcList Lista de ALC.
   * @param isCompra Inidica si la operación es de compra.
   */
  private void generateApunteDiferencia(BigDecimal diferencia,
                                        String descReferencia,
                                        Date fecha,
                                        String tipoCuentaCompensacion,
                                        List<Tmct0alc> tmct0alcList,
                                        Boolean isCompra) {
    LOG.debug("[generateApunteDiferencia] Inicio ...");
    if (diferencia.signum() != 0) {
      LOG.debug("[generateApunteDiferencia] Apuntando: [ fecha: '{}'; diferencia: '{}'; descReferencia: '{}'; isCompra: '{}' ]", fecha, diferencia,
                descReferencia, isCompra);

      ApunteContable apunteDiferencia = new ApunteContable();
      apunteDiferencia.setCuentaContable(findCuentaContable(CUENTACONTABLE_4021102));
      apunteDiferencia.setDescripcion(descReferencia);
      apunteDiferencia.setConcepto(CONCEPTO_APUNTE_DIFERENCIA);
      apunteDiferencia.setFecha(fecha);
      apunteDiferencia.setContabilizado(Boolean.FALSE);
      apunteDiferencia.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteDiferencia.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
      apunteDiferencia.setTipoMovimiento(ALC_CUENTA_BRUTA.equals(tipoCuentaCompensacion) ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                        : TIPO_APUNTE.EFECTIVO_NETO.getId());

      // En COMPRAS al DEBE en positivo si el cliente paga de menos (el importe Norma43 es menor que la suma de imefeagr+imfinsvb)
      // En COMPRAS al HABER en negativo si el cliente paga de más (el importe Norma43 es mayor que la suma de imefeagr+imfinsvb)
      // En VENTAS al HABER en negativo si el cliente paga de menos (el importe Norma43 es menor que la menos de imefeagr-imfinsvb)
      // En VENTAS al DEBE en positivo si el cliente paga de más (el importe Norma43 es mayor que la menos de imefeagr-imfinsvb)
      if (isCompra) {
        if (diferencia.signum() > 0) {
          apunteDiferencia.setImporte(diferencia.negate());
          apunteDiferencia.setApunte(TIPO_CUENTA.HABER.getId());
        } else if (diferencia.signum() < 0) {
          apunteDiferencia.setImporte(diferencia.negate());
          apunteDiferencia.setApunte(TIPO_CUENTA.DEBE.getId());
        }
      } else {
        if (diferencia.signum() > 0) {
          apunteDiferencia.setImporte(diferencia);
          apunteDiferencia.setApunte(TIPO_CUENTA.DEBE.getId());
        } else if (diferencia.signum() < 0) {
          apunteDiferencia.setImporte(diferencia);
          apunteDiferencia.setApunte(TIPO_CUENTA.HABER.getId());
        }
      }
      LOG.debug("[generateApunteDiferencia] diferencia a apuntar: " + diferencia);

      apunteContableBo.save(apunteDiferencia);

      // apunteContableAlcDao.save(new ApunteContableAlc(apunteDiferencia, tmct0alc, apunteDiferencia.getImporte()));
      for (Tmct0alc tmct0alc : tmct0alcList) {
        apunteContableAlcDao.save(new ApunteContableAlc(apunteDiferencia, tmct0alc, apunteDiferencia.getImporte()));
      }
    } // if (diferencia.signum() != 0)

    LOG.debug("[generateApunteDiferencia] Fin ...");
  } // generateApunteDiferencia

  /**
   * Obtiene un mapa con el estado de cobro de los distintos importes a cobrar.
   * 
   * @param yaCobradoAlcs Importe ya cobrado.
   * @param efectivoAlcs Importe de efectivo a cobrar.
   * @param comisionAlcsCompras Importe de comisión a cobrar de las operaciones de compra.
   * @param comisionAlcsVentas Importe de comisión a cobrar de las operaciones de venta.
   * @return <code>java.util.HashMap<String, BigDecimal> - </code>Mapa con los estados de los cobros.
   */
  private HashMap<String, BigDecimal> obtenerEstadoCobros(BigDecimal yaCobradoAlcs,
                                                          BigDecimal efectivoAlcs,
                                                          BigDecimal comisionAlcsCompras,
                                                          BigDecimal comisionAlcsVentas) {
    LOG.debug("[obtenerEstadoCobros] Inicio ...");
    HashMap<String, BigDecimal> estadoCobros = new HashMap<String, BigDecimal>(6);

    if (yaCobradoAlcs.compareTo(efectivoAlcs) <= 0) {
      estadoCobros.put("yaCobradoEfectivo", yaCobradoAlcs);
      estadoCobros.put("yaCobradoComisionCompras", BigDecimal.ZERO);
      estadoCobros.put("yaCobradoComisionVentas", BigDecimal.ZERO);

      estadoCobros.put("porCobrarEfectivo", efectivoAlcs.subtract(yaCobradoAlcs));
      estadoCobros.put("porCobrarComisionCompras", comisionAlcsCompras);
      estadoCobros.put("porCobrarComisionVentas", comisionAlcsVentas);
    } else {
      estadoCobros.put("yaCobradoEfectivo", efectivoAlcs);
      estadoCobros.put("porCobrarEfectivo", BigDecimal.ZERO);

      if (comisionAlcsCompras.signum() != 0 && yaCobradoAlcs.compareTo(efectivoAlcs.add(comisionAlcsCompras.abs())) < 0) {
        // Si hay comision para cobrar y no la he cobrado toda
        estadoCobros.put("yaCobradoComisionCompras", yaCobradoAlcs.subtract(efectivoAlcs));

        if (comisionAlcsCompras.signum() > 0) {
          // Si la comisión es positiva
          estadoCobros.put("porCobrarComisionCompras", comisionAlcsCompras.subtract(estadoCobros.get("yaCobradoComisionCompras")));
        } else {
          // Si la comisión es negativa
          estadoCobros.put("porCobrarComisionCompras", comisionAlcsCompras.add(estadoCobros.get("yaCobradoComisionCompras")));
        } // else
      } else {
        estadoCobros.put("yaCobradoComisionCompras", comisionAlcsCompras);
        estadoCobros.put("porCobrarComisionCompras", BigDecimal.ZERO);
      } // else

      if (comisionAlcsVentas.signum() != 0 && yaCobradoAlcs.compareTo(efectivoAlcs.add(comisionAlcsCompras.abs().add(comisionAlcsVentas.abs()))) < 0) {
        // Si hay comision para cobrar y no la he cobrado toda
        estadoCobros.put("yaCobradoComisionVentas", yaCobradoAlcs.subtract(efectivoAlcs.add(comisionAlcsCompras.abs())));

        if (comisionAlcsVentas.signum() > 0) {
          // Si la comisión es positiva
          estadoCobros.put("porCobrarComisionVentas", comisionAlcsVentas.subtract(estadoCobros.get("yaCobradoComisionVentas")));
        } else {
          // Si la comisión es negativa
          estadoCobros.put("porCobrarComisionVentas", comisionAlcsVentas.add(estadoCobros.get("yaCobradoComisionVentas")));
        } // else
      } else {
        estadoCobros.put("yaCobradoComisionVentas", comisionAlcsVentas);
        estadoCobros.put("porCobrarComisionVentas", BigDecimal.ZERO);
      } // else
    } // else

    LOG.debug("[obtenerEstadoCobros] estadoCobros: '{}'", estadoCobros.toString());
    return estadoCobros;
  } // obtenerEstadoCobros

  /**
   * Busca el objeto <code>CuentaContable</code> cuyo número de cuenta se ha especificado.
   *
   * @param cuenta Número de cuenta a buscar.
   * @return <code>CuentaContable - </code>Cuenta contable obtenida.
   */
  private CuentaContable findCuentaContable(Integer cuenta) {
    CuentaContable salida;
    if ((salida = cuentaContableMap.get(cuenta)) == null) {
      cuentaContableMap.put(cuenta, salida = cuentaContableDao.findByCuenta(cuenta));
    } // if
    return salida;
  } // findCuentaContable

} // CobrosClearingCliente
