package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.utils.FormatDataUtils;

/**
 * Realiza los apuntes contables de cobros de cánones y corretajes de barridos de routing.
 * 
 * @author XI316153
 */
@Service
public class CobrosComisionesBMESubList {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosComisionesBMESubList.class);

  private static final Integer CUENTACONTABLE_1021101 = 1021101;
  private static final Integer CUENTACONTABLE_1122111 = 1122111;
  private static final Integer CUENTACONTABLE_4021102 = 4021102;

  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_COMISIONES_BME";
  private static final String DESCUACRE_COMENTARIO_DIFMAYOR = "Diferencia entre lo devengado y lo cobrado mayor que margen de tolerancia";
  private static final String DESCUACRE_COMENTARIO_SINALCS = "No se han encontrado ALCS en los estados esperados para el movimiento norma43";
  private static final String DESCUACRE_COMENTARIO_REFNOVALIDA = "La referencia no es B3";
  private static final String DESCUACRE_COMENTARIO_REFVACIA = "La referencia está vacía";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  /** Mapa de cuentas contables. */
  private Map<Integer, CuentaContable> cuentaContableMap;

  /** Lista de descuadres a generar. */
  private List<Norma43DescuadresDTO> norma43DescuadresDTOList;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * @param barrido
   * @param lEstadosAsig
   * @param estadosMap
   * @param tmct0cfgAuxiliarPorDefectoRouting
   * @param tiposImporteList
   * @param margenDescuadres
   * @throws Exception
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void generateApuntes(Map.Entry<String, List<Norma43MovimientoDTO>> datosNorma43,
                              String estadosAsig,
                              Long auxiliarContableId,
                              BigDecimal tolerancePorFichero,
                              HashMap<String, Tmct0estado> estadosMap,
                              Integer flagNorma43ProcesadoTrue,
                              BigDecimal cienBigDecimal) throws Exception {
    LOG.debug("[generateApuntes] Inicio ...");

    init();

    if (datosNorma43.getKey() == null || datosNorma43.getKey().trim().length() == 0) {
      for (Norma43MovimientoDTO norma43MovimientoDTO : datosNorma43.getValue()) {
        norma43DescuadresDTOList.add(CobrosComisionesBME.generateDescuadre(norma43MovimientoDTO, null, null, null, null, null, null, null, null,
                                                                           DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_REFVACIA));
      } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
    } else if (!datosNorma43.getKey().startsWith(CobrosComisionesBME.LITERAL_COBROS_BME)) {
      for (Norma43MovimientoDTO norma43MovimientoDTO : datosNorma43.getValue()) {
        norma43DescuadresDTOList.add(CobrosComisionesBME.generateDescuadre(norma43MovimientoDTO, null, null, null, null, null, null, null, null,
                                                                           DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_REFNOVALIDA));
      } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
    } else {
      String key = datosNorma43.getKey().substring(2);

      Date feopeliq = FormatDataUtils.convertStringToDate(key, FormatDataUtils.DATE_FORMAT);

      // 1.- Se buscan los alc a tratar
      List<Object[]> alcList = null;
      alcList = tmct0alcDaoImp.findAlcsForCorretajesBME(estadosAsig, estadosMap.get("estadoContDevengada").getIdestado(), feopeliq);

      Date alcFeejeliq = null;
      BigDecimal alcsCorretaje = BigDecimal.ZERO;

      List<Tmct0alcId> tmct0alcIdList = new ArrayList<Tmct0alcId>();
      // String nurefordList = "";

      if (alcList != null && !alcList.isEmpty()) {
        for (Object[] alc : alcList) {
          tmct0alcIdList.add(new Tmct0alcId(String.valueOf(alc[1]), String.valueOf(alc[0]), Short.valueOf(String.valueOf(alc[3])),
                                            String.valueOf(alc[2])));

          alcsCorretaje = alcsCorretaje.add(new BigDecimal(String.valueOf(alc[4])));
          alcFeejeliq = (Date) alc[5];
          // nurefordList += String.valueOf(alc[2]) + ",";
        } // for (Object[] alc : alcList)
        // nurefordList = nurefordList.substring(0, nurefordList.length() - 1);

        // 2.- Se calcula lo cobrado
        BigDecimal norma43Corretaje = BigDecimal.ZERO;
        for (Norma43MovimientoDTO norma43MovimientoDTO : datosNorma43.getValue()) {
          norma43Corretaje = norma43Corretaje.add(norma43MovimientoDTO.getImporte().divide(cienBigDecimal));
        }

        // 3.- Se calcula la diferencia entre lo devengado y cobrado y se comprueba si está dentro del margen de tolerancia o se debe generar
        // descuadre
        BigDecimal diferenciaCorretaje = alcsCorretaje.subtract(norma43Corretaje);
        if (diferenciaCorretaje.abs().compareTo(tolerancePorFichero) > 0) {
          for (Norma43MovimientoDTO norma43MovimientoDTO : datosNorma43.getValue()) {
            norma43DescuadresDTOList.add(CobrosComisionesBME.generateDescuadre(norma43MovimientoDTO, alcsCorretaje, null, null, null, null, null,
                                                                               null, null, DESCUADRE_TIPO_IMPORTE, diferenciaCorretaje,
                                                                               DESCUACRE_COMENTARIO_DIFMAYOR));
          } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
        } else {
          // Se hace los apuntes de canones

          Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), alcFeejeliq);

          // generateApuntesContables(norma43Corretaje, alcsCorretaje, diferenciaCorretaje, fechaApunte, /* auxiliar, */
          // auxiliarDao.findOne(auxiliarContableId), CobrosComisionesBME.LITERAL_COBROS_BME.concat(datosNorma43.getKey()),
          // FormatDataUtils.convertDateToString(alcFeejeliq, FormatDataUtils.DATE_FORMAT));
          generateApuntesContables(norma43Corretaje, alcsCorretaje, diferenciaCorretaje, fechaApunte, /* auxiliar, */
                                   auxiliarDao.findOne(auxiliarContableId), datosNorma43.getKey(),
                                   FormatDataUtils.convertDateToString(feopeliq, FormatDataUtils.DATE_FORMAT));
        } // else

        LOG.debug("[generateApuntes] Actualizando estado contable ALC-ALO-BOK ...");
        tmct0alcDaoImp.updateEstadoContByClave(tmct0alcIdList, estadosMap.get("estadoContCobrada").getIdestado(), estadosMap.get("estadoContEnCurso")
                                                                                                                            .getIdestado());
        LOG.debug("[generateApuntes] Estado contable ALC-ALO-BOK actualizado ...");

        // TODO Falta la actualización de la relacion 
        // LOG.debug("[generateApuntes] Actualizando esdadoContableAlc de comisiones BME ...");
        // alcEstadoContDaoImp.mergeCdestadoContByClave(TIPO_ESTADO.CORRETAJE_O_CANON.getId(), estadosMap.get("estadoContDevengada")
        // .getIdestado(),
        // estadosMap.get("estadoContCobrada").getIdestado(), tmct0alcIdList);
        // LOG.debug("[generateApuntes] EsdadoContableAlc de comisiones BME actualizado ...");

      } else {
        for (Norma43MovimientoDTO norma43MovimientoDTO : datosNorma43.getValue()) {
          norma43DescuadresDTOList.add(CobrosComisionesBME.generateDescuadre(norma43MovimientoDTO, null, null, null, null, null, null, null, null,
                                                                             DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_SINALCS));
        } // for
      } // else
    } // else

    if (norma43DescuadresDTOList != null && !norma43DescuadresDTOList.isEmpty()) {
      for (Norma43DescuadresDTO norma43DescuadresDTO : norma43DescuadresDTOList) {
        norma43DescuadresDao.save(norma43DescuadresDTO.getDescuadreNorma43());
      } // for
    }

    LOG.debug("[generateApuntes] Actualizando movimientos norma43 ...");
    for (Norma43MovimientoDTO norma43MovimientoDTO : datosNorma43.getValue()) {
      norma43MovimientoDao.update(norma43MovimientoDTO.getId(), flagNorma43ProcesadoTrue);
    }

    // for (Norma43Movimiento mN43 : mN43List) {
    // norma43MovimientoDao.update(mN43.getId(), 1);
    //
    // // Para que a la vuelta del método cuando se comprueba si se han procesado correctamente los movimientos para actualizar el flag de procesado
    // // del fichero estos estén modificados, porque al ser una query de actualiazación nativa el objeto de hibernate no se modifica
    // mN43.setProcesado(Boolean.TRUE);
    // }
    LOG.debug("[generateApuntes] Movimientos norma43 actualizados ...");

    LOG.debug("[generateApuntes] Fin ...");
  } // generateApuntes

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Inicializa los objetos que se utilzan durante el proceso.
   */
  private void init() {
    cuentaContableMap = new HashMap<Integer, CuentaContable>();
    norma43DescuadresDTOList = new ArrayList<Norma43DescuadresDTO>();
  } // init

  /**
   * Busca el objeto <code>CuentaContable</code> cuyo número de cuenta se ha especificado.
   *
   * @param cuenta Número de cuenta a buscar.
   * @return <code>CuentaContable - </code>Cuenta contable obtenida.
   */
  private CuentaContable findCuentaContable(Integer cuenta) {
    CuentaContable salida;
    if ((salida = cuentaContableMap.get(cuenta)) == null) {
      cuentaContableMap.put(cuenta, salida = cuentaContableDao.findByCuenta(cuenta));
    } // if
    return salida;
  } // findCuentaContable

  /**
   * Genera los apuntes contables de cobro de corretaje.
   * 
   * @param importeCobrado Importe cobrado en los csv.
   * @param importeDevengado Sumatorio de los importes devengados de los alc.
   * @param diferencia Diferencia entre lo devengado y lo cobrado.
   * @param fechaApunte Fecha a establecer en el apunte contable.
   * @param auxiliar Auxiliar por alias para el apunte en la cuenta 1122111.
   * @param auxiliarCuentaBanco Auxiliar por cuenta bancaria para el apunte en la cuenta 1021101.
   * @param descripcionCuentaBancos Descripción del apunte de la cuenta 1021101.
   * @param fechaDescripcion Fecha a añadir a la descripción del apunte de la cuenta 1122111.
   */
  private void generateApuntesContables(BigDecimal importeCobrado, BigDecimal importeDevengado, BigDecimal diferencia, Date fechaApunte,
  /* Auxiliar auxiliar, */
  Auxiliar auxiliarCuentaBanco, String descripcionCuentaBancos, String fechaDescripcion) {
    LOG.debug("[generateApuntesContables] Inicio ...");
    if (importeCobrado.signum() != 0) {
      ApunteContable apunte_corretaje_debe = new ApunteContable();
      apunte_corretaje_debe.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
      apunte_corretaje_debe.setDescripcion(descripcionCuentaBancos);
      apunte_corretaje_debe.setConcepto("Cobro comision BME");
      apunte_corretaje_debe.setImporte(importeCobrado);
      apunte_corretaje_debe.setFecha(fechaApunte);
      apunte_corretaje_debe.setApunte(TIPO_CUENTA.DEBE.getId());
      apunte_corretaje_debe.setContabilizado(Boolean.FALSE);
      apunte_corretaje_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_debe.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_BME.getTipo());
      apunte_corretaje_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
      apunte_corretaje_debe.setAuxiliar(auxiliarCuentaBanco);
      apunteContableBo.save(apunte_corretaje_debe);
    } // if (importeCobrado.signum() != 0)

    if (importeDevengado.signum() != 0) {
      ApunteContable apunte_corretaje_haber = new ApunteContable();
      apunte_corretaje_haber.setCuentaContable(findCuentaContable(CUENTACONTABLE_1122111));
      apunte_corretaje_haber.setDescripcion("Comision BME (" + fechaDescripcion + ")");
      apunte_corretaje_haber.setConcepto("Cobro comision BME");
      apunte_corretaje_haber.setImporte(importeDevengado.negate());
      apunte_corretaje_haber.setFecha(fechaApunte);
      apunte_corretaje_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_corretaje_haber.setContabilizado(Boolean.FALSE);
      apunte_corretaje_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_haber.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_BME.getTipo());
      apunte_corretaje_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
      // apunte_corretaje_haber.setAuxiliar(auxiliar);
      apunteContableBo.save(apunte_corretaje_haber);
    } // if (importeDevengado.signum() != 0)

    if (diferencia.signum() != 0) {
      ApunteContable apunte_corretaje_diff = new ApunteContable();
      apunte_corretaje_diff.setCuentaContable(findCuentaContable(CUENTACONTABLE_4021102));
      apunte_corretaje_diff.setDescripcion("Diferencia comision BME (" + fechaDescripcion + ")");
      apunte_corretaje_diff.setConcepto("Diferencia cobro comision BME");
      apunte_corretaje_diff.setFecha(fechaApunte);
      apunte_corretaje_diff.setContabilizado(Boolean.FALSE);
      apunte_corretaje_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_diff.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_BME.getTipo());
      apunte_corretaje_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CORRETAJE.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_corretaje_diff);
    } // if (diferencia.signum() != 0)

    LOG.debug("[generateApuntesContables] Fin ...");
  } // generateApuntesContables

} // CobrosComisionesBMESubList
