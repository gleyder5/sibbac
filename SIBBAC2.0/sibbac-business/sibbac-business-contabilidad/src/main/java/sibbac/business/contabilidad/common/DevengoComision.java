package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.ApunteContableDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;

@Service
public class DevengoComision {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(DevengoComision.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private ApunteContableDao apunteContableDao;

  @Autowired
  private ApunteContableAlcDao apunteContableAlcDao;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * @param params
   * @throws SIBBACBusinessException
   */
  @Transactional
  public void save(Map<String, Object> params) throws SIBBACBusinessException {
    LOG.debug("[DevengoComision::save] Inicio ...");
    Tmct0alc alc = (Tmct0alc) params.get("alc");

    Tmct0alo alo = alc.getTmct0alo();
    Boolean isGrupo = alo.getTmct0bok().isGrupo();
    if (isGrupo == null) {
      isGrupo = Boolean.FALSE;
    } // if

    CuentaContable cuentaContableDebe = cuentaContableDao.findByCuenta(isGrupo ? 3026104 : 3026103);
    CuentaContable cuentaContableHaber = cuentaContableDao.findByCuenta(isGrupo ? 2015208 : 2015207);

    ApunteContable apunte;
    BigDecimal importe;

    params.put("descripcion", params.get("sFeejeliq") + "/" + alo.getCdalias() + "/" + alc.getNbtitliq() + "/" + alc.getId().getNbooking());
    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.DEVENGO_COMISION);

    if ((importe = alc.getImcomdvo()) != null && importe.signum() != 0) {
      LOG.debug("[DevengoComision::save] Devengando comision ordenante ...");
      params.put("concepto", "Devengo Com. Ordenante");
      params.put("TIPO_APUNTE", TIPO_APUNTE.COMISION_ORDENANTE.getId());
      params.put("sApunte", "D");
      params.put("cuenta", cuentaContableDebe);
      (apunte = new ApunteContable(params)).setImporte(importe);
      apunteContableDao.save(apunte);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte, alc, importe));

      params.put("sApunte", "H");
      params.put("cuenta", cuentaContableHaber);
      (apunte = new ApunteContable(params)).setImporte(importe.negate());
      apunteContableDao.save(apunte);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte, alc, importe));

      tmct0alcDaoImp.escalaAlcDesglose(alc, CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(), TIPO_ESTADO.COMISION_ORDENANTE);
      alcEstadoContDaoImp.updateOrInsertCdestadocont(CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(), alc.getId().getNbooking(), alc.getId().getNuorden(),
                                             alc.getId().getNucnfliq(), alc.getId().getNucnfclt(), TIPO_ESTADO.COMISION_ORDENANTE.getId());
    } // if ((importe = alc.getImcomdvo()) != null && importe.signum() != 0)

    if ((importe = alc.getImcombrk()) != null && importe.signum() != 0) {
      LOG.debug("[DevengoComision::save] Devengando comision de devolucion ...");
      params.put("concepto", "Devengo Com. Devolución");
      params.put("TIPO_APUNTE", TIPO_APUNTE.COMISION_DEVOLUCION.getId());
      params.put("sApunte", "D");
      params.put("cuenta", cuentaContableDebe);
      (apunte = new ApunteContable(params)).setImporte(importe);
      apunteContableDao.save(apunte);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte, alc, importe));

      params.put("sApunte", "H");
      params.put("cuenta", cuentaContableHaber);
      (apunte = new ApunteContable(params)).setImporte(importe.negate());
      apunteContableDao.save(apunte);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte, alc, importe));

      tmct0alcDaoImp.escalaAlcDesglose(alc, CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(), TIPO_ESTADO.COMISION_DEVOLUCION);
      alcEstadoContDaoImp.updateOrInsertCdestadocont(CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(), alc.getId().getNbooking(), alc.getId().getNuorden(),
                                             alc.getId().getNucnfliq(), alc.getId().getNucnfclt(), TIPO_ESTADO.COMISION_ORDENANTE.getId());
    } // if ((importe = alc.getImcombrk()) != null && importe.signum() != 0)

    LOG.debug("[DevengoComision::save] Fin ...");
  } // save

} // DevengoComision
