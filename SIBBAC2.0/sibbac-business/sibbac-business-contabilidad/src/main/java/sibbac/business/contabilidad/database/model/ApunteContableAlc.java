package sibbac.business.contabilidad.database.model;


import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.contabilidad.ApunteContable}.
 */
@Table( name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.APUNTE_CONTABLE_ALC )
@Entity
@Component
@XmlRootElement
public class ApunteContableAlc extends ATable< ApunteContableAlc > {

	private static final long	serialVersionUID	= 6816098284606542395L;

	/**
	 * Apunte Contable
	 */
	@ManyToOne( optional = false, cascade = {
			CascadeType.PERSIST, CascadeType.MERGE
	}, fetch = FetchType.LAZY )
	@JoinColumn( name = "APUNTE", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private ApunteContable		apunte;

	/**
	 * TMCT0ALC
	 */
	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( {
			@JoinColumn( name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = true ),
			@JoinColumn( name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = true ),
			@JoinColumn( name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = true ),
			@JoinColumn( name = "NUCNFLIQ", referencedColumnName = "NUCNFLIQ", nullable = true )
	} )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	private Tmct0alc			alc;

	public ApunteContableAlc() {
	}

	public ApunteContableAlc(ApunteContable apunte,Tmct0alc alc,BigDecimal importe) {
		this.apunte = apunte;
		this.alc = alc;
		this.importe = importe;
	}

	/**
	 * Importe
	 */
	@Column( name = "IMPORTE", precision = 15, scale = 2 )
	@XmlAttribute
	protected BigDecimal		importe;

	public ApunteContable getApunte() {
		return apunte;
	}

	public void setApunte( ApunteContable apunte ) {
		this.apunte = apunte;
	}

	public Tmct0alc getAlc() {
		return alc;
	}

	public void setAlc( Tmct0alc alc ) {
		this.alc = alc;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}
}
