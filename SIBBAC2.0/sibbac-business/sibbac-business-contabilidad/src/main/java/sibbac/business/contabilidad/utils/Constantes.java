package sibbac.business.contabilidad.utils;

/**
 * The Class Constantes.
 */
public class Constantes {

  /** The Constant CMD_LIST. */
  public static final String CMD_LIST = "list";

  /** The Constant CMD_ADD. */
  public static final String CMD_ADD = "add";

  /** The Constant CMD_UPDATE. */
  public static final String CMD_UPDATE = "update";

  /** The Constant CMD_DELETE. */
  public static final String CMD_DELETE = "delete";

  /** The Constant CMD_INIT. */
  public static final String CMD_INIT = "init";

  /** The Constant CMD_FIND. */
  public static final String CMD_FIND = "filtra";
  public static final String CMD_CUENTAS = "getCuentas";
  public static final String CMD_AUXILIARES = "getAuxiliares";

  public static final String OPERATIVA_APUNTE = "N";
  
}
