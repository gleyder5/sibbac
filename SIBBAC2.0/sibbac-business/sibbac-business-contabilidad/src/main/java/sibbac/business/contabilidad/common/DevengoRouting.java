package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.SIBBACBusinessException;

@Component
public class DevengoRouting {
  protected static final Logger LOG = LoggerFactory.getLogger(DevengoRouting.class);
  private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");
  public static final String application = "SBRMV";
  public static final String process = "CONTABILIDAD";
  public static final String keynameAux = "clientes.devengo.routing.auxiliar";

  @Autowired
  private ApunteContableAlcDaoImp apunteAlcDao;
  @Autowired
  private CuentaContableDao cuentaDao;
  @Autowired
  private Tmct0alcDaoImp alcDao;
  @Autowired
  private DevengoComision devengoComision;
  @Autowired
  private Tmct0cfgDao cfgDao;
  @Autowired
  private Devengo devengo;
  @Autowired
  private AuxiliarDao auxiliarDao;
  @Autowired
  private ApunteContableBo apunteBo;
  private Map<String, Auxiliar> mAuxiliar;

  public Auxiliar getAuxiliar(Map<String, Object> params) {
    Auxiliar auxiliar = null;
    Tmct0alc alc = (Tmct0alc) params.get("alc");
    String cdalias = alc.getTmct0alo().getCdalias();
    if ((auxiliar = mAuxiliar.get(cdalias)) == null) {
      if (alc.getAuxiliar() != null) {
        auxiliar = auxiliarDao.findOne(alc.getAuxiliar());
      }
      if (auxiliar == null) {
        LOG.debug("Al alias " + cdalias + " se le asigna el auxiliar por defecto");
        mAuxiliar.put(cdalias, auxiliar = (Auxiliar) params.get("auxiliarPorDefecto"));
      } else {
        mAuxiliar.put(cdalias, auxiliar);
      }
    }
    return auxiliar;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void put(Map<String, Object> params) throws Exception {
    LOG.debug("Se inicia la transacción de un bloque de devengos de routing");
    Date feejeliq;
    List<Tmct0alc> lAlc;
    Date fechFactura;
    Auxiliar auxiliar;
    mAuxiliar = new HashMap<String, Auxiliar>();
    Map<Date, Map<Auxiliar, List<Tmct0alc>>> mRouting = new TreeMap<Date, Map<Auxiliar, List<Tmct0alc>>>();
    Map<Auxiliar, List<Tmct0alc>> mDia;
    Map<Date, Date> mFechaContabilizacion = new HashMap<Date, Date>();
    Calendar hoy = (Calendar) params.get("hoy");
    params.put("contabilizado", false);
    params.put("operativa", "N");
    params.put("auxiliarPorDefecto",
               auxiliarDao.findByNombre(cfgDao.findByAplicationAndProcessAndKeyname(application, process, keynameAux).getKeyvalue()));
    for (Tmct0alc alc : (List<Tmct0alc>) params.get("list")) {

      /*
       * En el caso que se haya cerrado el mes, los apuntes contables deben entrar con fecha del mes en curso, pero en la descripcion del apunte
       * contable conservar la fecha Trade Date
       */
      if ((fechFactura = mFechaContabilizacion.get(feejeliq = alc.getFeejeliq())) == null) {
        mFechaContabilizacion.put(feejeliq, fechFactura = apunteBo.getFechaContabilizacion(hoy, feejeliq));
      }
      params.put("fechFactura", fechFactura);
      params.put("feejeliq", feejeliq);
      params.put("nuorden", alc.getNuorden());
      params.put("nbooking", alc.getNbooking());
      params.put("nucnfclt", alc.getNucnfclt());
      params.put("alc", alc);
      if ((mDia = mRouting.get(feejeliq)) == null) {
        mRouting.put(feejeliq, mDia = new HashMap<Auxiliar, List<Tmct0alc>>());
      }
      if ((lAlc = mDia.get(auxiliar = getAuxiliar(params))) == null) {
        mDia.put(auxiliar, lAlc = new ArrayList<Tmct0alc>());
      }
      lAlc.add(alc);
    }
    for (Entry<Date, Map<Auxiliar, List<Tmct0alc>>> entry : mRouting.entrySet()) {
      params.put("fechFactura", apunteBo.getFechaContabilizacion(hoy, entry.getKey()));
      params.put("feejeliq", entry.getKey());
      params.put("sFeejeliq", dF1.format(entry.getKey()));
      params.put("entry.value", entry.getValue());
      save(params);
    }
    LOG.debug("Finaliza la transacción de un bloque de devengos de routing");
  }

  public void putApunte(ApunteContable apunteD, ApunteContable apunteH) {
    if (!apunteD.getAlcs().isEmpty()) {
      apunteBo.save(apunteD);
      apunteH.setImporte(apunteH.getImporte().subtract(apunteD.getImporte()));
      for (ApunteContableAlc alc : apunteD.getAlcs()) {
        apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(), alc.getAlc().getNucnfclt(),
                            apunteD.getId(), alc.getImporte());
        apunteH.getAlcs().add(alc);
      }
    }
  }

  public void putApunteH(ApunteContable apunteH) {
    if (!apunteH.getAlcs().isEmpty()) {
      apunteBo.save(apunteH);
      for (ApunteContableAlc alc : apunteH.getAlcs()) {
        apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(), alc.getAlc().getNucnfclt(),
                            apunteH.getId(), alc.getImporte().negate());
      }
    }
  }

  @Transactional
  public void save(Map<String, Object> params) throws SIBBACBusinessException {
    ApunteContable apunteCorretajeD;
    ApunteContable apunteCanonD;
    BigDecimal corretaje;
    BigDecimal canon;
    Auxiliar auxiliar;
    String sFeejeliq = (String) params.get("sFeejeliq");
    Map<String, Object> params2 = new HashMap<String, Object>();
    LOG.debug("Procesando el feejeliq: " + sFeejeliq);
    params2.put("sFeejeliq", sFeejeliq);
    params.put("contabilizado", false);
    params2.put("contabilizado", false);
    params.put("operativa", "N");
    params2.put("operativa", "N");
    params2.put("fechFactura", params.get("fechFactura"));
    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.DEVENGO_ROUTING);
    params.put("cuenta", cuentaDao.findByCuenta(4021101));
    params.put("sApunte", "H");
    params.put("concepto", "Devengo Corretaje");
    params.put("descripcion", "Corretajes (" + sFeejeliq + ")");
    params.put("TIPO_APUNTE", TIPO_APUNTE.CORRETAJE.getId());
    ApunteContable apunteCorretajeH = new ApunteContable(params);
    params.put("concepto", "Devengo Canon");
    params.put("descripcion", "Canones (" + sFeejeliq + ")");
    params.put("TIPO_APUNTE", TIPO_APUNTE.CANON.getId());
    ApunteContable apunteCanonH = new ApunteContable(params);
    params.put("cuenta", cuentaDao.findByCuenta(1122106));
    params.put("sApunte", "D");
    for (Entry<Auxiliar, List<Tmct0alc>> entry : ((Map<Auxiliar, List<Tmct0alc>>) params.get("entry.value")).entrySet()) {
      params.put("auxiliar", auxiliar = entry.getKey());
      LOG.debug("Procesando el feejeliq " + sFeejeliq + " y auxiliar " + auxiliar.getNombre());
      params.put("concepto", "Devengo Corretaje");
      params.put("descripcion", "Corretajes (" + sFeejeliq + ")");
      params.put("TIPO_APUNTE", TIPO_APUNTE.CORRETAJE.getId());
      apunteCorretajeD = new ApunteContable(params);
      params.put("concepto", "Devengo Canon");
      params.put("descripcion", "Canones (" + sFeejeliq + ")");
      params.put("TIPO_APUNTE", TIPO_APUNTE.CANON.getId());
      apunteCanonD = new ApunteContable(params);
      for (Tmct0alc alc : entry.getValue()) {
        LOG.debug("Procesando alc con nuorden: " + alc.getNuorden() + " nbooking: " + alc.getNbooking() + " nucnfclt: " + alc.getNucnfclt()
                  + " y nucnfliq: " + alc.getNucnfliq());
//        alcDao.updateCdestadocont(CONTABILIDAD.CONTABILIZANDOSE.getId(), alc.getNbooking(), alc.getNuorden(), alc.getNucnfliq(), alc.getNucnfclt());
        params.put("alc", alc);
        params2.put("alc", alc);
        devengo.putCorretajeAndCanon(params);
        if ((corretaje = (BigDecimal) params.get("corretaje")).signum() != 0) {
          apunteCorretajeD.add(alc, corretaje);
        }
        if ((canon = (BigDecimal) params.get("canon")).signum() != 0) {
          apunteCanonD.add(alc, canon);
        }
        devengoComision.save(params2);
        alcDao.escalaAlcDesglose(alc, CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(), TIPO_ESTADO.CORRETAJE_O_CANON);
      }
      LOG.debug("Fin del procesado de feejeliq " + sFeejeliq + " y auxiliar " + auxiliar.getNombre());
      putApunte(apunteCorretajeD, apunteCorretajeH);
      putApunte(apunteCanonD, apunteCanonH);
    }
    LOG.debug("Fin del procesado de feejeliq: " + sFeejeliq);
    devengo.putApunte(apunteCorretajeH);
    devengo.putApunte(apunteCanonH);
    LOG.debug("Fin de la escritura de apuntes al haber de feejeliq: " + sFeejeliq);
  }
}
