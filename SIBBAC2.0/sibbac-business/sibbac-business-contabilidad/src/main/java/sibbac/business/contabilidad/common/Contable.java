package sibbac.business.contabilidad.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.common.FtpSibbac;

@Component
public class Contable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para escribir en el log. */
  protected static final Logger LOG = LoggerFactory.getLogger(Contable.class);

  private static final DateFormat dF3 = new SimpleDateFormat("yyMMdd_HHmmss");
  private static final DateFormat dF2 = new SimpleDateFormat("yy");
  private static final DateFormat dF4 = new SimpleDateFormat("yyMMdd");
  private static final NumberFormat nF1 = new DecimalFormat("00");
  private static final NumberFormat nF2 = new DecimalFormat("00000");
  private static final NumberFormat nF5 = new DecimalFormat("0000000000000.00", new DecimalFormatSymbols(new Locale("es", "ES")));
  private static final NumberFormat nF6 = new DecimalFormat("000000000000.00", new DecimalFormatSymbols(new Locale("es", "ES")));
  private static final String REGISTRO_CABECERA = "1";
  private static final String REGISTRO_DETALLE = "2";
  private static final String CODIGO_COMPANYIA = "31";
  private static final String STRING1 = "        ";
  private static final String STRING5 = "               ";
  private static final String STRING2 = "                                    ";
  private static final String STRING3 = "                                                                                                                                                            ";
  private static final String FILLER = "000000000000000  0000000000000000000                                 000000000000000000000000  ";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private ApunteContableDao apunteContableDao;

  @Autowired
  private FtpSibbac ftpSibbac;

  @Value("${ftp.folder}")
  private String folderFtp;

  @Value("${contabilidad.folder.tratado}")
  private String folderTratado;

  @Value("${contabilidad.folder.out}")
  private String folderOut;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Busca y escribe los apuntes contables a enviar en el fichero.
   * 
   * @param tipoComprobante Tipo de comprobante a buscar.
   * @return <code>java.lang.Boolean - </code><code>true</code> si el fichero se generó y envió correctamente; <code>false</code> en caso contrario.
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Boolean putContables(TIPO_COMPROBANTE tipoComprobante) throws Exception {
    LOG.debug("[putContables] Inicio ...");
    Calendar cal = new GregorianCalendar();
    Date hoy = cal.getTime();
    cal.add(Calendar.DATE, 1);
    LOG.debug("[putContables] Consultando para la fecha: {}", hoy);
    return putApuntes(apunteContableDao.findByTipoComprobanteAndContabilizadoAndFechaBeforeAndImporteNotOrderByPeriodoComprobanteAsc(tipoComprobante.getTipo(),
                                                                                                                                     Boolean.FALSE,
                                                                                                                                     cal.getTime(),
                                                                                                                                     BigDecimal.ZERO),
                      tipoComprobante, hoy);
  } // putContables

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * 
   * @param list
   * @param tipoComprobante
   * @param fecha
   * @return
   * @throws IOException
   */
  private Boolean putApuntes(List<ApunteContable> list, TIPO_COMPROBANTE tipoComprobante, Date fecha) throws IOException {
    LOG.debug("[putApuntes] Inicio ...");
    if (!ftpSibbac.connect(folderFtp)) {
      LOG.debug("[putApuntes] No se ha podido conectar con la maquina destino ...");
      return Boolean.FALSE;
    } // if
    LOG.debug("[putApuntes] Conectado ...");

    List<File> files = put(list, tipoComprobante, fecha);
    if (!files.isEmpty()) {
      Object[] objects;
      List<Object[]> listSend = new ArrayList<Object[]>();
      for (File file1 : files) {
        listSend.add(objects = new Object[3]);
        objects[0] = file1;
        objects[1] = objects[2] = file1.getName();
      } // for (File file1 : files)

      LOG.debug("[putApuntes] Enviando y moviendo a tratados ...");
      LOG.debug("[putApuntes] Resultado del envio: {}", ftpSibbac.send(listSend, folderTratado));
      LOG.debug("[putApuntes] Enviado ...");
    } // if (!files.isEmpty())
    LOG.debug("[putApuntes] Saliendo y devolviendo true siempre ...");
    return Boolean.TRUE;
  } // putApuntes

  /**
   * Genera los ficheros con los datos a enviar.
   *
   * @param list Lista de apuntes contables a enviar.
   * @param tipoComprobante Tipo de comprobante de los apuntes contables.
   * @return <code>List<File> - </code>Lista de ficheros generados a enviar.
   * @throws IOException Si ocurre algún error durante la escritura del fichero.
   */
  private List<File> put(List<ApunteContable> list, TIPO_COMPROBANTE tipoComprobante, Date fecha) throws IOException {
    LOG.debug("[put] Inicio ...");
    List<File> files = new ArrayList<File>();
    if (list.isEmpty()) {
      LOG.debug("[put] No hay apuntes contables a enviar. Saliendo ...");
      return files;
    } // if
    LOG.debug("[put] Escribiendo {} apuntes contables ...", list.size());

    OutputStream oS = null;
    File file = null;
    PrintWriter writer = null;
    Date dateAux = null;
    int periodoAux = -1;

    // Se obtiene el periodo comprobante para la fecha especificada
    int periodoActual = ApunteContableBo.getPeriodoComprobante(fecha);
    LOG.debug("[put] Periodo comprobante para la fecha '{}': {}", fecha, periodoActual);

    // Máximo número de comprobante existente enviado previamente
    int numeroComprobante = apunteContableDao.findNumeroComprobante(tipoComprobante.getTipo());
    LOG.debug("[put] Numero de comprobante para el tipo comprobante '{}': {}", tipoComprobante.getTipo(), numeroComprobante);

    // Se crea el directorio donde dejar los ficheros
    new File(folderOut).mkdirs();

    for (ApunteContable apunte : list) {
      LOG.debug("[put] Procesando apunte contable ...", apunte.toString());
      LOG.debug("[put] periodoAux-apunte.getPeriodoComprobante(): {}-{} ...", periodoAux, apunte.getPeriodoComprobante());
      if (periodoAux != apunte.getPeriodoComprobante()) {
        LOG.debug("[put] Iniciando periodo comprobante: {}", apunte.getPeriodoComprobante());
        closeFile(oS, writer);

        numeroComprobante++;
        periodoAux = apunte.getPeriodoComprobante();

        files.add(file = new File(folderOut, tipoComprobante.getSiglas() + "_" + nF1.format(periodoAux) + "_" + dF3.format(fecha) + ".txt"));
        writer = new PrintWriter(oS = new FileOutputStream(file));
        LOG.debug("[put] Creando fichero: {}", file.getAbsolutePath());
        LOG.debug("[put] Creando fichero: {}", file.getName());

        apunte.setNumeroComprobante(numeroComprobante);
        dateAux = periodoActual == periodoAux ? fecha : ApunteContableBo.getFechaPeriodoComprobante(periodoAux, fecha.getYear() + 1900).getTime();
        LOG.debug("[put] dateAux: {}", dateAux);
        writeHeader(writer, apunte, dateAux);
      } // if

      apunte.setNumeroComprobante(numeroComprobante);
      apunte.setContabilizado(Boolean.TRUE);
      writeLine(writer, apunte);
    } // for (ApunteContable apunte : list)

    closeFile(oS, writer);
    apunteContableDao.save(list);
    LOG.debug("[put] Actualizando estado contabilizado de los apuntes contables ...");

    return files;
  } // put

  /**
   * Escribe la cabecera del fichero.
   *
   * @param writer Writer.
   * @param apunte Apunte contable.
   * @param date Fecha.
   */
  private void writeHeader(PrintWriter writer, ApunteContable apunte, Date date) {
    LOG.debug("[writeHeader] Inicio ...");
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(apunte.getFecha());
    int century = ((calendar.get(Calendar.YEAR) / 100) + 1) % 10;
    writer.print(REGISTRO_CABECERA + CODIGO_COMPANYIA);
    writer.print(dF2.format(apunte.getFecha()));
    writer.print(nF1.format(apunte.getPeriodoComprobante()));
    writer.print(nF1.format(apunte.getTipoComprobante()));
    writer.print(nF2.format(apunte.getNumeroComprobante()));
    writer.print(century);
    writer.print(dF4.format(date));
    writer.println(STRING3);
    LOG.debug("[writeHeader] Fin ...");
  } // writeHeader

  /**
   * Escribe una línea en el fichero.
   *
   * @param writer Writer.
   * @param apunte Apunte contable a escribir.
   */
  private void writeLine(PrintWriter writer, ApunteContable apunte) {
    LOG.debug("[writeLine] Inicio ...");
    Auxiliar auxiliar = apunte.getAuxiliar();
    BigDecimal bD = apunte.getImporte();
    writer.print(REGISTRO_DETALLE + CODIGO_COMPANYIA);
    writer.print(dF2.format(apunte.getFecha()));
    writer.print(nF1.format(apunte.getPeriodoComprobante()));
    writer.print(nF1.format(apunte.getTipoComprobante()));
    writer.print(nF2.format(apunte.getNumeroComprobante()));
    writer.print((apunte.getCuentaContable().getCuenta() + STRING5).substring(0, 15));
    writer.print(((auxiliar == null ? "" : auxiliar.getNombre()) + STRING1).substring(0, 8));
    writer.print((apunte.getDescripcion() + STRING2).substring(0, 36));
    writer.print(bD.signum() >= 0 ? nF5.format(bD) : nF6.format(bD));
    writer.print(apunte.getApunte());
    writer.println(FILLER);
    LOG.debug("[writeLine] Fin ...");
  } // writeLine

  /**
   * Cierra el fichero.
   *
   * @param oS Stream de datos a cerrar.
   * @param writer Writter a cerrar.
   * @throws IOException Si ocurre algún error al cerrar el fichero.
   */
  private void closeFile(OutputStream oS, PrintWriter writer) throws IOException {
    LOG.debug("[closeFile] Inicio ...");
    if (oS != null) {
      writer.close();
      oS.close();
    } // if
    LOG.debug("[closeFile] Fin ...");
  } // closeFile

} // Contable
