package sibbac.business.contabilidad.database.dao;


import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.wrappers.database.dao.Tmct0CuentaLiquidacionDao;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;


@Service
public class CuentaContableDaoImp extends AbstractBo< CuentaContable, Long, CuentaContableDao > {
	private static String QUERY = "select a from CuentaContable a where 1=1";

	@PersistenceContext private EntityManager em;
	@Autowired private AuxiliarDao auxiliarDao;
	@Autowired private Tmct0CuentaLiquidacionDao ctaLiqDao;
	@Autowired private ApunteContableDao apunteDao;

	private Map<Integer, CuentaContable> mCuenta = new HashMap<Integer, CuentaContable>();

	public List<CuentaContable> find( Map< String, String > filters ) throws ParseException {
		String value;
		StringBuffer sB = new StringBuffer(QUERY);
		Map< String, Object > parameters = new HashMap< String, Object >();
		if (!(value = filters.get( "cuenta" )).isEmpty()) {
			parameters.put( "cuenta", value);
			sB.append( " and id=:idCuenta" );
		}
		if (!(value = filters.get( "operativa" )).isEmpty()) {
			parameters.put( "operativa", value);
			sB.append( " and operativa=:operativa" );
		}
		if (!(value = filters.get( "tipo" )).isEmpty()) {
			parameters.put( "tipo", value);
			sB.append( " and tipo=:tipo" );
		}
		if (!(value = filters.get( "withAuxiliar" )).isEmpty()) {
			parameters.put( "withAuxiliar", value);
			sB.append( " and with_auxiliar=:withAuxiliar" );
		}
		Query query = em.createQuery( sB.toString() );
		for ( Entry< String, Object > entry : parameters.entrySet() ) {
			query.setParameter( entry.getKey(), entry.getValue() );
		}
		return query.getResultList();
	}

	public CuentaContable insert(Map<String, String > filters) throws SIBBACBusinessException {
		CuentaContable cuenta;
		int numero = Integer.parseInt(filters.get("cuenta"));
		Tmct0CuentaLiquidacion ctaLiq = ctaLiqDao.findOne(Long.decode(filters.get("ctaLiq")));
		if (ctaLiq==null) {
			throw new SIBBACBusinessException("No existe la cuenta de liquidación");
		}
		String sId = filters.get("id");
		if (sId.isEmpty()) {

//			Es un alta
			if (dao.countByCuenta(numero)!=0) {
				throw new SIBBACBusinessException("Ya hay una cuenta contable con ese número");
			}
			cuenta = new CuentaContable(filters.get("operativa"), filters.get("tipo"), numero,
					filters.get("nombre"), Boolean.getBoolean(filters.get("withAuxiliar")), ctaLiq);
		} else {

//			Es una modificacion
			cuenta = dao.findOne(Long.decode(sId));
			cuenta.setOperativa(filters.get("operativa"));
			cuenta.setTipo(filters.get("tipo"));
			cuenta.setCuenta(numero);
			cuenta.setNombre(filters.get("nombre"));
			cuenta.setWithAuxiliar(Boolean.getBoolean(filters.get("withAuxiliar")));
			cuenta.setCtaLiq(ctaLiq);
		}
		return save(cuenta);
	}

	public void delete( Map< String, String > filters ) throws SIBBACBusinessException {
		Long id = Long.decode(filters.get("id"));
		if (apunteDao.countByCuentaContableId(id)==0) {
			delete(id);
		} else {
			throw new SIBBACBusinessException("Esta cuenta contable tiene apuntes asociados");
		}
	}

	@Transactional public CuentaContable find(int cuenta) {
		CuentaContable result;
		if ((result = mCuenta.get(cuenta))==null) {
			mCuenta.put(cuenta, result = dao.findByCuenta(cuenta));
		}
		return result;
	}
}
