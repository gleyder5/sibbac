package sibbac.business.contabilidad.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.contabilidad.ApunteContable}.
 */
@Table( name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.APUNTE_CONTABLE )
@Entity
@Component
@XmlRootElement
@Audited
public class ApunteContable extends ATableAudited< ApunteContable > {
	private static final long	serialVersionUID	= 2123721615845922271L;
	protected static final Logger LOG = LoggerFactory.getLogger(ApunteContable.class);

	/**
	 * Tipo de Operativa (N = Nacional, I = Internacional)
	 */
	@Column( name = "OPERATIVA", length = 1 )
	@XmlAttribute
	protected String			operativa;

	/**
	 * sibbac.business.contabilidad.common.Enumerados.TIPO_APUNTE.id
	 */
	@Column(name = "TIPO_MOVIMIENTO", columnDefinition="SMALLINT")
	@XmlAttribute
	protected Integer			tipoMovimiento;

	/**
	 * Tipo de apunte en cuenta (D = Debe, H = Haber)
	 */
	@Column( name = "APUNTE", length = 1 )
	@XmlAttribute
	protected String			apunte;

	/**
	 * Cuenta Contable
	 */
	@ManyToOne( optional = false, cascade = {
			CascadeType.PERSIST, CascadeType.MERGE
	}, fetch = FetchType.LAZY )
	@JoinColumn( name = "CUENTA", nullable = false )
	@XmlAttribute
	private CuentaContable		cuentaContable;

	/**
	 * Auxiliar Contable
	 */
	@ManyToOne( optional = true, cascade = {
			CascadeType.PERSIST, CascadeType.MERGE
	}, fetch = FetchType.LAZY )
	@JoinColumn( name = "NUMERO", nullable = true )
	@XmlAttribute
	private Auxiliar			auxiliar;

	/**
	 * Concepto Movimiento
	 */
	@Column( name = "CONCEPTO", length = 36 )
	@XmlAttribute
	protected String			concepto;

	/**
	 * Descripción Movimiento
	 */
	@Column( name = "DESCRIPCION" )
	@XmlAttribute
	protected String			descripcion;

	/**
	 * Importe
	 */
	@Column( name = "IMPORTE", precision = 15, scale = 2 )
	@XmlAttribute
	protected BigDecimal		importe = BigDecimal.ZERO;

	/**
	 * Fh. Movimiento Contable
	 */
	@Temporal( TemporalType.DATE )
	@Column( name = "FECHA" )
	@XmlAttribute
	protected Date				fecha;

	/**
	 * Enviado a Contabilidad (true=Sí, false=No)
	 */
	@Column( name = "CONTABILIZADO" )
	@XmlAttribute
	protected boolean			contabilizado;

	/**
	 * Factura
	 */
	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "FACTURA", referencedColumnName = "ID", nullable = true )
	private Factura				factura;

	/**
	 * Periodo del Comprobante
	 */
	@Column( name = "PERIODO_COMPROBANTE", length = 2 )
	@XmlAttribute
	protected int				periodoComprobante;

	/**
	 * Tipo de Comprobante
	 */
	@Column( name = "TIPO_COMPROBANTE", length = 2 )
	@XmlAttribute
	protected int				tipoComprobante;

	/**
	 * Número de Comprobante
	 */
	@Column( name = "NUMERO_COMPROBANTE", length = 5 )
	@XmlAttribute
	protected int				numeroComprobante;

	@OneToMany (mappedBy="apunte")
	//@JoinColumn(name = "ID", referencedColumnName="CUENTA")
	private List<ApunteContableAlc> lAlc = new ArrayList< ApunteContableAlc >();

	public ApunteContable() {
	}

	public ApunteContable(Map<String, Object> params) {
		setOperativa((String) params.get("operativa"));
		setTipoMovimiento((int) params.get("TIPO_APUNTE"));
		setApunte(( String ) params.get("sApunte"));
		String string = (String) params.get("concepto");
		setConcepto(string.substring(0, Math.min(36,string.length())));
		string = (String) params.get("descripcion");
		setDescripcion(string.substring(0, Math.min(255,string.length())));
		setFecha((Date) params.get("fechFactura"));
		setContabilizado((boolean) params.get("contabilizado"));
		setCuentaContable((CuentaContable) params.get("cuenta"));
		setAuxiliar((Auxiliar) params.get("auxiliar"));
		setTipoComprobante(((TIPO_COMPROBANTE) params.get("TIPO_COMPROBANTE")).getTipo());
	}

	@Override
	public ApunteContable clone() {
		ApunteContable apunte = new ApunteContable();
		apunte.setFecha( getFecha() );
		apunte.setPeriodoComprobante( getPeriodoComprobante() );
		apunte.setTipoComprobante( getTipoComprobante() );
		apunte.setNumeroComprobante( getNumeroComprobante() );
		apunte.setCuentaContable( getCuentaContable() );
		apunte.setAuxiliar( getAuxiliar() );
		apunte.setDescripcion( getDescripcion() );
		apunte.setImporte( getImporte() );
		apunte.setApunte( getApunte() );
		return apunte;
	}

	public String getOperativa() {
		return operativa;
	}

	public void setOperativa( String operativa ) {
		this.operativa = operativa;
	}

	public Integer getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento( Integer tipoMovimiento ) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getApunte() {
		return apunte;
	}

	public void setApunte( String apunte ) {
		this.apunte = apunte;
	}

	public CuentaContable getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable( CuentaContable cuentaContable ) {
		this.cuentaContable = cuentaContable;
		if (cuentaContable.isWithAuxiliar()) {
			Auxiliar auxiliar = cuentaContable.getAuxiliar();
			if (auxiliar!=null) {
				this.auxiliar = auxiliar;
			}
		}
	}

	public Auxiliar getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar( Auxiliar auxiliar ) {
		if (cuentaContable==null) {
			this.auxiliar = auxiliar;
		} else {
			if (cuentaContable.isWithAuxiliar()) {
				if ((this.auxiliar = auxiliar)==null) {
					Auxiliar auxiliarCta = cuentaContable.getAuxiliar();
					this.auxiliar = auxiliarCta==null? auxiliar: auxiliarCta;
				}
			} else {
				this.auxiliar = null;
			}
		}
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto( String concepto ) {
		this.concepto = concepto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	public void add(BigDecimal importe ) {
		this.importe = this.importe.add(importe);
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha( Date fecha ) {
		this.fecha = fecha;
		periodoComprobante = ApunteContableBo.getPeriodoComprobante( fecha );
	}

	public boolean isContabilizado() {
		return contabilizado;
	}

	public void setContabilizado( boolean contabilizado ) {
		this.contabilizado = contabilizado;
	}

	public int getPeriodoComprobante() {
		return periodoComprobante;
	}

	public void setPeriodoComprobante( int periodoComprobante ) {
		this.periodoComprobante = periodoComprobante;
	}

	public int getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante( int tipoComprobante ) {
		this.tipoComprobante = tipoComprobante;
	}

	public int getNumeroComprobante() {
		return numeroComprobante;
	}

	public void setNumeroComprobante( int numeroComprobante ) {
		this.numeroComprobante = numeroComprobante;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura( Factura factura ) {
		this.factura = factura;
	}

	public List< ApunteContableAlc > getAlcs() {
		return lAlc;
	}

	public void setAlcs( List< ApunteContableAlc > lAlc ) {
		this.lAlc = lAlc;
	}

	public void add(Tmct0alc alc, BigDecimal importe) {
		setImporte(getImporte().add(importe));
		ApunteContableAlc apunteAlc = new ApunteContableAlc(this, alc, importe);
		if (getAlcs().contains(apunteAlc)) {
			LOG.error("Ya existe el alc de nuorden: " + alc.getNuorden() + ", nbooking: "
					+ alc.getNbooking() + ", nucnfclt: " + alc.getNucnfclt() + ", nucnfliq: " + alc.getNucnfliq());
		} else {
			getAlcs().add(apunteAlc);
		}
	}
}
