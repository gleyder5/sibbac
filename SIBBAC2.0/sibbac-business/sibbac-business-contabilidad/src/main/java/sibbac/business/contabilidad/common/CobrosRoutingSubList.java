package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaAuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaAuxiliar;
import sibbac.business.contabilidad.database.model.CuentaBancaria;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.AlcEstadoContDao;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.SIBBACBusinessException;

/**
 * Realiza los apuntes contables de cobros de cánones y corretajes.
 * 
 * @author XI316153
 */
@Service
public class CobrosRoutingSubList {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosRoutingSubList.class);

  /** Indica que un registro norma43 es de adeudo. */
  private static final String ADEUDO = "1";

  /** Indica que un registro norma43 es de abono. */
  private static final String ABONO = "2";

  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_ROUTING";
  private static final String DESCUACRE_COMENTARIO_ALCNOENCONTRADO = "ALC no encontrado para referencia";
  private static final String DESCUACRE_COMENTARIO_DIFMAYOR = "Diferencia entre lo devengado y lo cobrado mayor que margen de tolerancia";

  private static final String PARTENON_NUCNFCLT = "PARTENON";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0_ESTADO</code>. */
  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0ALC</code>. */
  @Autowired
  private Tmct0alcDao tmct0alcDao;

  /** Bussiness object para el acceso a la tabla <code>TMCT0ALC</code>. */
  @Autowired
  private Tmct0alcBo tmct0alcBo;

  /** Objeto para el acceso a la tabla <code>TMCT0_CUENTA_CONTABLE</code>. */
  @Autowired
  private CuentaContableDao cuentaContableDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_APUNTE_CONTABLE</code>. */
  @Autowired
  private ApunteContableBo apunteContableBo;

  /** Objeto para el acceso a la tabla <code>TMCT0_APUNTE_CONTABLE_ALC</code>. */
  @Autowired
  private ApunteContableAlcDao apunteContableAlcDao;

  /** Bussiness object para el acceso a la tabla <code>TMCT0_ALC_ESTADOCONT</code>. */
  @Autowired
  private Tmct0alcDaoImp alcEstadoContBo;

  /** Objeto para el acceso a la tabla <code>TMCT0_DESCUADRES_NORMA_43</code>. */
  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_NORMA43_MOVIMIENTO</code>. */
  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_TMCT0LOG</code>. */
  @Autowired
  private Tmct0logBo tmct0logBo;

  /** Objeto para el acceso a la tabla <code>TMCT0_AUXILIAR</code>. */
  @Autowired
  private AuxiliarDao auxiliarDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_AUXILIAR</code>. */
  @Autowired
  private CuentaAuxiliarDao cuentaAuxiliarDao;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  @Autowired
  private AlcEstadoContDao alcEstadoContDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_CUENTA_AUXILIAR</code>. */
  private HashMap<String, CuentaAuxiliar> auxiliarPorCuenta;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>1021101</code>. */
  private CuentaContable cuentaContable_1021101 = null;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>1122106</code>. */
  private CuentaContable cuentaContable_1122106 = null;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>4021101</code>. */
  private CuentaContable cuentaContable_4021101 = null;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>4021102</code>. */
  private CuentaContable cuentaContable_4021102 = null;

  /** Auxiliar contable por defecto para los apuntes de routing. */
  private Auxiliar auxiliarRoutingPorDefecto = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void generateRoutingCobrosSubList(List<Entry<String, HashMap<String, List<Norma43Movimiento>>>> list,
                                           List<Integer> lEstadosAsig,
                                           HashMap<String, Tmct0estado> estadosMap,
                                           String tmct0cfgAuxiliarPorDefectoRouting,
                                           List<Integer> tiposImporteList,
                                           BigDecimal margenDescuadres) throws Exception {
    LOG.debug("[generateRoutingCobrosSubList] Inicio ...");
    LOG.debug("[generateRoutingCobrosSubList] Procesando {} referencias ...", list.size());
    String referencia = null;
    List<Tmct0alc> alcList = null;
    Boolean generarDescuadre = Boolean.FALSE;

    // Auxiliar por defecto para routing
    auxiliarRoutingPorDefecto = auxiliarDao.findByNombre(tmct0cfgAuxiliarPorDefectoRouting);

    auxiliarPorCuenta = new HashMap<String, CuentaAuxiliar>();
    for (CuentaAuxiliar ctaAuxiliar : cuentaAuxiliarDao.find(TIPOLOGIA.Routing.getTipo(), tiposImporteList)) {
      auxiliarPorCuenta.put(ctaAuxiliar.getCuenta().getIban(), ctaAuxiliar);
    } // for

    LOG.debug("Consultas de cuentas contables ...");
    cuentaContable_1021101 = cuentaContableDao.findByCuenta(1021101);
    cuentaContable_1122106 = cuentaContableDao.findByCuenta(1122106);
    cuentaContable_4021101 = cuentaContableDao.findByCuenta(4021101);
    cuentaContable_4021102 = cuentaContableDao.findByCuenta(4021102);

    for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : list) {
      try {
        generarDescuadre = Boolean.FALSE;
        referencia = entry.getKey().split("#")[1];
        LOG.debug("Procesando referencia: " + referencia);

        // Se buscan los ALC a tartar
        if (referencia.indexOf("ORMA") != -1) {
          // Minorista Nuorden.
          alcList = tmct0alcDao.findAlcByOrdenNuorden(referencia, lEstadosAsig);
        } else if (referencia.indexOf("ORAC") != -1) {
          // Minorista Nuorden.
          alcList = tmct0alcDao.findAlcByOrdenNuorden(referencia, lEstadosAsig);
        } else if (referencia.indexOf("TRMA") != -1) {
          // Minorista Nbooking.
          alcList = tmct0alcDao.findAlcByOrdenNbooking(referencia, lEstadosAsig);
        } else if (referencia.indexOf("CAMA") != -1) {
          // Minorista Nucnfclt.
          alcList = tmct0alcDao.findAlcByOrdenNucnfclt(referencia, lEstadosAsig);
          // } else if (Character.isLetter(referencia.charAt(0))) {
          // // Minorista ISIN Y Fecha. Varios ALC
          // alcList = tmct0alcDao.findAlcByCdisinAndFecontraAndCdCuenta(referencia,
          // FormatDataUtils.convertStringToDate(entry.getKey().split("#")[2], "yyyyMMdd"),
          // lEstadosAsig);
        } else {
          // Minorista Nureford.
          alcList = tmct0alcDao.findAlcByOrdenNureford(referencia, lEstadosAsig);

          // ... o no (ñapa, a eliminar ñapa cuando nureford esté a nivel de alc). Inicio ...
          try {
            if (alcList != null) {
              if (alcList.size() > 1) {
                LOG.debug("Obtenidos '" + alcList.size() + "' alcs para la referencia. nyapeando ...");
                List<Tmct0alc> nurefordAlcList = new ArrayList<Tmct0alc>();
                List<Integer> nurefordList = new ArrayList<Integer>();
                Integer contadorNureford = 0;
                String descReferencia;

                for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet()) {
                  LOG.debug("Procesando cuenta '" + valueEntry.getKey() + "' ...");
                  for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
                    LOG.debug("Procesando movimiento '" + norma43Movimiento.toString() + "' ...");
                    descReferencia = norma43Movimiento.getDescReferencia().trim();
                    contadorNureford = Integer.valueOf(descReferencia.substring(descReferencia.length() - 2, descReferencia.length()));
                    LOG.debug("contadorNureford: '" + contadorNureford + "' ...");
                    if (!nurefordList.contains(contadorNureford)) {
                      LOG.debug("contadorNureford no contenido en la lista ...");
                      if (alcList.size() >= contadorNureford) {
                        LOG.debug("Numero de alcs encontrados mayor o igual a contadorNureford. Anyadiendo a la lista ...");
                        nurefordList.add(contadorNureford);
                        nurefordAlcList.add(alcList.get(contadorNureford - 1));
                        LOG.debug("Alc anyadido: " + alcList.get(contadorNureford - 1).getId().toString());
                      } else {
                        LOG.debug("Numero de alcs encontrados menor a contadorNureford. alcList = null ...");
                        // FIXME
                        // Hay que eliminar de la estructura de datos pretratados todo rastro a esta referencia y generar
                        // un descuadre para ella sin alc
                        alcList = null;
                      } // else
                    } else {
                      LOG.debug("contadorNureford ya contenido en la lista ...");
                    } // else

                    // alcList = alcList.subList(contadorNureford - 1, contadorNureford);
                    // LOG.debug("Nyapeando, se pasa el alc " + alcList.get(0).toString());
                  } // for (Norma43Movimiento norma43Movimiento : nurefordN43MList)
                } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())

                alcList = nurefordAlcList;
              } // if (alcList.size() > 1)
            } else {
              // Minorista Nucnfclt: PARTENON+Nureford
              alcList = tmct0alcDao.findAlcByOrdenNucnfclt(PARTENON_NUCNFCLT.concat(referencia), lEstadosAsig);
            } // else
          } catch (Exception e) {
            LOG.debug("Error nyapeando ... '" + e.getMessage() + "' ... ", e);
          } // catch
          // ... o no (ñapa, a eliminar ñapa cuando nureford esté a nivel de alc). Fin ...
        } // else

        if (alcList != null && !alcList.isEmpty()) {
          processMinorista(alcList, entry, referencia, estadosMap, margenDescuadres);
        } else {
          LOG.debug("No se han encontrado alcs para la referencia ... Se genera descuadre ...");
          generarDescuadre = Boolean.TRUE;
        }

        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet()) {
          for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
            if (generarDescuadre) {
              generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], null, null, null, DESCUACRE_COMENTARIO_ALCNOENCONTRADO);
            }

            norma43MovimientoDao.update(norma43Movimiento.getId(), 1);
            // norma43Movimiento.setProcesado(Boolean.TRUE);
            // norma43MovimientoDao.save(norma43Movimiento);
          } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
        } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())

      } catch (Exception e) {
        LOG.error("Error generando apuntes de cobros Norma43 para '" + entry.getKey() + "' ... ", e);
        throw e;
      } // catch
    } // for (Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entry : list)

    LOG.debug("[generateRoutingCobrosSubList] Fin ...");
  } // generateRoutingCobrosSubList

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Devuelve el auxiliar contable del alias especificado.
   * 
   * @param cdalias Código del alias.
   * @return <code>Auxiliar - </code>Auxiliar contable del alias especificado; el auxiliar por defecto para routing en el caso de que el alias no
   *         tenga uno especificado.
   */
  private Auxiliar getAuxiliar(String cdalias) {
    Auxiliar auxiliar = auxiliarDao.findByCdalias(cdalias);
    if (auxiliar == null) {
      LOG.debug("Al alias '" + cdalias + "' se le asigna el auxiliar por defecto");
      return auxiliarRoutingPorDefecto;
    } else {
      return auxiliar;
    } // else
  } // getAuxiliar

  /**
   * Genera un descuadre con los datos especificados y lo almacena en base de datos.
   * 
   * @param norma43Movimiento Movimiento Norma43.
   * @param tmct0alc Registro de la tabla Tmct0alc que genera el descuadre. Puede ser null.
   * @param importeSibbac Importe en Sibbac.
   * @param tipoImporte Tipo de operación que ha generado el descuadre.
   */
  private void generarDescuadre(Norma43Movimiento norma43Movimiento,
                                String cuenta,
                                Tmct0alc tmct0alc,
                                BigDecimal importeSibbac,
                                String tipoImporte,
                                String comentario) {
    LOG.debug("[generarDescuadre] Inicio ...");
    Norma43DescuadresDTO descuadreNorma43DTO = new Norma43DescuadresDTO();

    CuentaBancaria cc = new CuentaBancaria(cuenta);

    descuadreNorma43DTO.setCuenta(String.format("%04d", cc.getEntidad()).concat(String.format("%04d", cc.getSucursal()))
                                        .concat(String.format("%010d", cc.getCuenta())));
    descuadreNorma43DTO.setRefFecha(norma43Movimiento.getReferencia());
    descuadreNorma43DTO.setRefOrden(norma43Movimiento.getDescReferencia() == null ? " " : norma43Movimiento.getDescReferencia());
    descuadreNorma43DTO.setSentido(norma43Movimiento.getDebe());
    descuadreNorma43DTO.setTradeDate(norma43Movimiento.getTradedate());
    descuadreNorma43DTO.setSettlementDate(norma43Movimiento.getSettlementdate());
    descuadreNorma43DTO.setImporteNorma43(norma43Movimiento.getImporte().divide(new BigDecimal(100)));

    descuadreNorma43DTO.setComentario(comentario.length() > 200 ? comentario.substring(0, 200) : comentario);

    if (tmct0alc != null) {
      descuadreNorma43DTO.setImporteSibbac(importeSibbac);
      descuadreNorma43DTO.setTipoImporte(tipoImporte);
      // TODO Se podria quitar esta consulta y sacar el número del estado?
      descuadreNorma43DTO.setNombreEstadoAsignacionAlc(tmct0estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre());
      descuadreNorma43DTO.setNombreEstadoContabilidadAlc(tmct0alc.getEstadocont().getNombre());
      descuadreNorma43DTO.setNombreEstadoEntrecAlc(tmct0alc.getEstadoentrec().getNombre());
      descuadreNorma43DTO.setNuorden(tmct0alc.getNuorden());
      descuadreNorma43DTO.setNbooking(tmct0alc.getNbooking());
      descuadreNorma43DTO.setNucnfclt(tmct0alc.getNucnfclt());
      descuadreNorma43DTO.setNucnfliq(tmct0alc.getNucnfliq());
    } else {
      descuadreNorma43DTO.setTipoImporte(DESCUADRE_TIPO_IMPORTE);
    }

    norma43DescuadresDao.save(descuadreNorma43DTO.getDescuadreNorma43());

    LOG.debug("[generarDescuadre] Fin ...");
  } // generarDescuadre

  /**
   * Procesa los ALC especificados y genera los apuntes contables que se necesiten.
   *
   * @param listaAlc Lista de ALC a procesar.
   * @param entryNorma43Pretratada Información pretratada de Norma43.
   * @throws SIBBACBusinessException
   */
  private void processMinorista(List<Tmct0alc> listaAlc,
                                Map.Entry<String, HashMap<String, List<Norma43Movimiento>>> entryNorma43Pretratada,
                                String referencia,
                                HashMap<String, Tmct0estado> estadosMap,
                                BigDecimal margenDescuadres) throws SIBBACBusinessException {
    LOG.debug("[processMinorista] Inicio ...");

    BigDecimal sumaCorretajeAlcs = BigDecimal.ZERO;
    BigDecimal sumaCanonAlcs = BigDecimal.ZERO;

    Tmct0alc alcAnterior = null;

    Boolean cuadraCanon = false;
    Boolean retrocesionCanon = false;
    Boolean segundoApunteCanon = false;
    Boolean cuadraCorretaje = false;
    Boolean retrocesionCorretaje = false;
    Boolean segundoApunteCorretaje = false;

    CuentaAuxiliar ctaAuxiliar;

    Integer estadoCanon = null;
    Integer estadoCorretaje = null;
    Integer estadoCanonCorretaje = null;
    Integer estadoCanonFinal = null;
    Integer estadoCorretajeFinal = null;
    List<Object[]> alcEstadoContList;

    Boolean isAbono = Boolean.FALSE;
    Boolean generarApuntesCanon = Boolean.FALSE;
    Boolean generarApuntesCorretaje = Boolean.FALSE;

    // Se calcula el importe a apuntar y se comprueba que todos los alc estén en el mismo estado
    for (Tmct0alc alc : listaAlc) {
      isAbono = entryNorma43Pretratada.getKey().split("#")[0].compareTo(ABONO) == 0;

      sumaCorretajeAlcs = sumaCorretajeAlcs.add(alc.getImajusvb() == null ? alc.getImcomisn() : alc.getImajusvb().add(alc.getImcomisn()));
      sumaCanonAlcs = sumaCanonAlcs.add(alc.getImcanoncontreu());

      estadoCanon = null;
      estadoCorretaje = null;
      estadoCanonCorretaje = null;

      alcEstadoContList = alcEstadoContDao.findByAlc2(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(),
                                                      alc.getId().getNucnfliq());
      if (alcEstadoContList != null && !alcEstadoContList.isEmpty()) {
        for (Object[] alcEstadoCont : alcEstadoContList) {
          if (Integer.valueOf(String.valueOf(alcEstadoCont[0])).compareTo(TIPO_ESTADO.CANON.getId()) == 0) {
            estadoCanon = Integer.valueOf(String.valueOf(alcEstadoCont[1]));
          }
          if (Integer.valueOf(String.valueOf(alcEstadoCont[0])).compareTo(TIPO_ESTADO.CORRETAJE.getId()) == 0) {
            estadoCorretaje = Integer.valueOf(String.valueOf(alcEstadoCont[1]));
          }
          if (Integer.valueOf(String.valueOf(alcEstadoCont[0])).compareTo(TIPO_ESTADO.CORRETAJE_O_CANON.getId()) == 0) {
            estadoCanonCorretaje = Integer.valueOf(String.valueOf(alcEstadoCont[1]));
          }
        } // for

        // for (AlcEstadoCont alcEstadoCont : alc.getEstadosContables()) {
        // if (alcEstadoCont.getTipoEstado().compareTo(TIPO_ESTADO.CANON) == 0) {
        // estadoCanon = alcEstadoCont.getEstadoCont();
        // }
        // if (alcEstadoCont.getTipoEstado().compareTo(TIPO_ESTADO.CORRETAJE) == 0) {
        // estadoCorretaje = alcEstadoCont.getEstadoCont();
        // }
      } // if (alcEstadoContList != null && !alcEstadoContList.isEmpty())

      if (estadoCanonCorretaje != null) {
        // La operación está devengada
        if (estadoCanon == null) {
          estadoCanon = estadosMap.get("estadoContDevengadaPdteCobro").getIdestado();
        }
        if (estadoCorretaje == null) {
          estadoCorretaje = estadosMap.get("estadoContDevengadaPdteCobro").getIdestado();
        }

        estadoCanonFinal = estadoCanon;
        estadoCorretajeFinal = estadoCorretaje;

        if (alcAnterior == null) {
          if (isAbono) { // Es un abono
            // Cánones
            if (estadoCanon.compareTo(estadosMap.get("estadoContDevengadaPdteCobro").getIdestado()) == 0) {
              cuadraCanon = true;
            } else if (estadoCanon.compareTo(estadosMap.get("estadoContRetrocedida").getIdestado()) == 0) {
              cuadraCanon = true;
              segundoApunteCanon = true;
            } else {
              cuadraCanon = false;
            } // else

            // Corretajes
            if (estadoCorretaje.compareTo(estadosMap.get("estadoContDevengadaPdteCobro").getIdestado()) == 0) {
              cuadraCorretaje = true;
            } else if (estadoCorretaje.compareTo(estadosMap.get("estadoContRetrocedida").getIdestado()) == 0) {
              cuadraCorretaje = true;
              segundoApunteCorretaje = true;
            } else {
              cuadraCorretaje = false;
            } // else
          } else { // Es un adeudo
            // Cánones
            if (estadoCanon.compareTo(estadosMap.get("estadoContCobrado").getIdestado()) == 0) {
              cuadraCanon = true;
              retrocesionCanon = true;
            } else {
              cuadraCanon = false;
            } // else

            // Corretajes
            if (estadoCorretaje.compareTo(estadosMap.get("estadoContCobrado").getIdestado()) == 0) {
              cuadraCorretaje = true;
              retrocesionCorretaje = true;
            } else {
              cuadraCorretaje = false;
            } // else
          } // else

          alcAnterior = alc;
        } else {
          if (cuadraCanon) {
            if (retrocesionCanon) {
              if (estadoCanon.compareTo(estadosMap.get("estadoContCobrado").getIdestado()) != 0) {
                cuadraCanon = false;
              } // if
            } else if (segundoApunteCanon) {
              if (estadoCanon.compareTo(estadosMap.get("estadoContRetrocedida").getIdestado()) != 0) {
                cuadraCanon = false;
              } // if
            } else {
              if (estadoCanon.compareTo(estadosMap.get("estadoContDevengadaPdteCobro").getIdestado()) != 0) {
                cuadraCanon = false;
              } // if
            } // else
          } // if (cuadraCanon)

          if (cuadraCorretaje) {
            if (retrocesionCorretaje) {
              if (estadoCorretaje.compareTo(estadosMap.get("estadoContCobrado").getIdestado()) != 0) {
                cuadraCorretaje = false;
              } // if
            } else if (segundoApunteCorretaje) {
              if (estadoCorretaje.compareTo(estadosMap.get("estadoContRetrocedida").getIdestado()) != 0) {
                cuadraCorretaje = false;
              } // if
            } else {
              if (estadoCorretaje.compareTo(estadosMap.get("estadoContDevengadaPdteCobro").getIdestado()) != 0) {
                cuadraCorretaje = false;
              } // if
            } // else
          } // if (cuadraCanon)

          alcAnterior = alc;
        } // else
      } else {
        // La operación no está devengada
        cuadraCanon = false;
        cuadraCorretaje = false;
        if (alcAnterior == null) {
          alcAnterior = alc;
        }
      } // else
    } // for (Tmct0alc alc : listaAlc)

    if (cuadraCanon) {
      LOG.debug("Procesando canon ...");
      Tmct0alc alcATratar = listaAlc.get(0);
      Date fecha = alcATratar.getFeejeliq();
      Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), fecha);
      Auxiliar auxiliar = this.getAuxiliar(alcATratar.getTmct0alo().getTmct0bok().getCdalias().trim());

      if (retrocesionCanon) {
        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);

          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Canon.getTipo()) {
            LOG.debug("El/Los ALC se encuentra(n) en estado '" + estadosMap.get("estadoContCobrado").getNombre()
                      + "'. Generando apuntes de retrocesion de canon del adeudo: '" + entryNorma43Pretratada + "' ...");
            generateApuntesRetrocesionCanon(referencia, valueEntry, alcATratar, fecha, fechaApunte, auxiliar, sumaCanonAlcs,
                                            ctaAuxiliar.getAuxiliar(), margenDescuadres);
            generarApuntesCanon = Boolean.TRUE;
            estadoCanonFinal = estadosMap.get("estadoContRetrocedida").getIdestado();
          } // if
        } // for (Map.Entry<String, BigDecimal> valueEntry : entryNorma43Pretratada.getValue().entrySet())
      } else if (segundoApunteCanon) {
        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Canon.getTipo()) {
            LOG.debug("El/Los ALC se encuentra(n) en estado '" + estadosMap.get("estadoContRetrocedida").getNombre()
                      + "'. Generando segundos apuntes de cobbro de canon del abono: '" + entryNorma43Pretratada + "' ...");
            generateApuntesCanonSegundo(referencia, valueEntry, alcATratar, fecha, fechaApunte, auxiliar, sumaCanonAlcs, ctaAuxiliar.getAuxiliar(),
                                        margenDescuadres);
            generarApuntesCanon = Boolean.TRUE;
            estadoCanonFinal = estadosMap.get("estadoContCobrado").getIdestado();
          } // if
        } // for (Map.Entry<String, BigDecimal> valueEntry : entryNorma43Pretratada.getValue().entrySet())
      } else {
        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Canon.getTipo()) {
            LOG.debug("El/Los ALC se encuentra(n) en estado '" + estadosMap.get("estadoContDevengadaPdteCobro").getNombre()
                      + "'. Generando apuntes de cobro de canon del abono: '" + entryNorma43Pretratada + "' ...");
            generateApuntesCanon(referencia, valueEntry, alcATratar, fecha, fechaApunte, auxiliar, sumaCanonAlcs, ctaAuxiliar.getAuxiliar(),
                                 margenDescuadres);
            generarApuntesCanon = Boolean.TRUE;
            estadoCanonFinal = estadosMap.get("estadoContCobrado").getIdestado();
          } // if
        } // for (Map.Entry<String, BigDecimal> valueEntry : entryNorma43Pretratada.getValue().entrySet())
      } // else

      if (generarApuntesCanon) {
        for (Tmct0alc alc : listaAlc) {
          alcEstadoContDaoImp.updateOrInsertCdestadocont(estadoCanonFinal, alc.getId().getNbooking(), alc.getId().getNuorden(), alc.getId()
                                                                                                                                   .getNucnfliq(),
                                                         alc.getId().getNucnfclt(), TIPO_ESTADO.CANON.getId());
        } // for
      } // if (generarApuntesCanon)
    } else {
      LOG.debug("El/Los ALC se encuentra(n) en estado '" + alcAnterior.getEstadocont().getNombre() + "'. Se genera un descuadre.");

      for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
        for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Canon.getTipo()) {
            generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], alcAnterior, sumaCanonAlcs, TIPO_IMPORTE.Canon.getDescripcion(),
                             "El ALC se encuentra en estado '" + alcAnterior.getEstadocont().getNombre() + "'");
          } // else
        } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
      } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())
    } // else if (cuadraCanon)

    if (cuadraCorretaje) {
      LOG.info("Procesando corretaje ...");
      Tmct0alc alcATratar = listaAlc.get(0);
      Date fecha = alcATratar.getFeejeliq();
      Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), fecha);
      Auxiliar auxiliar = this.getAuxiliar(alcATratar.getTmct0alo().getTmct0bok().getCdalias().trim());

      if (retrocesionCorretaje) {
        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Corretaje.getTipo()) {
            LOG.debug("El/Los ALC se encuentra(n) en estado '" + estadosMap.get("estadoContCobrado").getNombre()
                      + "'. Generando apuntes de retrocesion de corretaje del adeudo: '" + entryNorma43Pretratada + "' ...");
            generateApuntesRetrocesionCorretaje(referencia, valueEntry, alcATratar, fecha, fechaApunte, auxiliar, sumaCorretajeAlcs,
                                                ctaAuxiliar.getAuxiliar(), margenDescuadres);
            generarApuntesCorretaje = Boolean.TRUE;
            estadoCorretajeFinal = estadosMap.get("estadoContRetrocedida").getIdestado();
          } // if
        } // for (Map.Entry<String, BigDecimal> valueEntry : entryNorma43Pretratada.getValue().entrySet())
      } else if (segundoApunteCorretaje) {
        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Corretaje.getTipo()) {
            LOG.debug("El/Los ALC se encuentra(n) en estado '" + estadosMap.get("estadoContRetrocedida").getNombre()
                      + "'. Generando segundos apuntes de cobro de corretaje del abono: '" + entryNorma43Pretratada + "' ...");
            generateApuntesCorretajeSegundo(referencia, valueEntry, alcATratar, fecha, fechaApunte, auxiliar, sumaCorretajeAlcs,
                                            ctaAuxiliar.getAuxiliar(), margenDescuadres);
            generarApuntesCorretaje = Boolean.TRUE;
            estadoCorretajeFinal = estadosMap.get("estadoContCobrado").getIdestado();
          } // if
        } // for (Map.Entry<String, BigDecimal> valueEntry : entryNorma43Pretratada.getValue().entrySet())
      } else {
        for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Corretaje.getTipo()) {
            LOG.debug("El/Los ALC se encuentra(n) en estado '" + estadosMap.get("estadoContDevengadaPdteCobro").getNombre()
                      + "'. Generando apuntes de cobro de corretaje del abono: '" + entryNorma43Pretratada + "' ...");
            generateApuntesCorretaje(referencia, valueEntry, alcATratar, fecha, fechaApunte, auxiliar, sumaCorretajeAlcs, ctaAuxiliar.getAuxiliar(),
                                     margenDescuadres);
            generarApuntesCorretaje = Boolean.TRUE;
            estadoCorretajeFinal = estadosMap.get("estadoContCobrado").getIdestado();
          } // if
        } // for (Map.Entry<String, BigDecimal> valueEntry : entryNorma43Pretratada.getValue().entrySet())
      } // else

      if (generarApuntesCorretaje) {
        for (Tmct0alc alc : listaAlc) {
          alcEstadoContDaoImp.updateOrInsertCdestadocont(estadoCorretajeFinal, alc.getId().getNbooking(), alc.getId().getNuorden(),
                                                         alc.getId().getNucnfliq(), alc.getId().getNucnfclt(), TIPO_ESTADO.CORRETAJE.getId());
        } // for
      } // if (generarApuntesCorretaje)
    } else {
      LOG.debug("El/Los ALC se encuentra(n) en estado '" + alcAnterior.getEstadocont().getNombre() + "'. Se genera un descuadre.");

      for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entryNorma43Pretratada.getValue().entrySet()) {
        for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
          ctaAuxiliar = auxiliarPorCuenta.get(valueEntry.getKey().split("#")[0]);
          if (ctaAuxiliar.getTipoImporte() == TIPO_IMPORTE.Corretaje.getTipo()) {
            generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], alcAnterior, sumaCorretajeAlcs,
                             TIPO_IMPORTE.Corretaje.getDescripcion(), "El ALC se encuentra en estado '" + alcAnterior.getEstadocont().getNombre()
                                                                      + "'");
          }
        } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
      } // for (Map.Entry<String, List<Norma43Movimiento>> valueEntry : entry.getValue().entrySet())
    } // else if (cuadraCorretaje)

    if (!generarApuntesCanon && sumaCanonAlcs.signum() == 0) {
      estadoCanonFinal = estadoCorretajeFinal;
    }

    if (!generarApuntesCorretaje && sumaCorretajeAlcs.signum() == 0) {
      estadoCorretajeFinal = estadoCanonFinal;
    }

    LOG.debug("Almacenando estado contable del ALC ...");
    LOG.debug("estadoCanonFinal: " + estadoCanonFinal);
    LOG.debug("estadoCorretajeFinal: " + estadoCorretajeFinal);
    if (estadoCanonFinal.compareTo(estadoCorretajeFinal) == 0) {
      LOG.debug("Mismo estado final para canones y corretajes ...");
      Tmct0estado estFinal = null;
      for (Tmct0alc alc : listaAlc) {
        LOG.debug("Actualizando estado del ALC ... " + alc.getId().toString() + " ... a estado " + estadoCanonFinal);

        if (estFinal == null) {
          for (Map.Entry<String, Tmct0estado> estado : estadosMap.entrySet()) {
            if (estado.getValue().getIdestado().compareTo(estadoCanonFinal) == 0) {
              estFinal = estado.getValue();
            } // if
          } // for
        } // if (estFinal == null)

        tmct0alcBo.setEstadocont(alc, estFinal);
        LOG.debug("Estado del ALC actualizado ...");
      } // for
    } // if (estadoCanon.getIdestado().compareTo(estadoCorretaje.getIdestado()) == 0)

    LOG.debug("[processMinorista] Fin ...");
  } // processAlcMinorista

  /**
   * Genera y almacena los apuntes de canones necesarios.
   * 
   * @param valueEntry
   * @param tmct0alc
   * @param fecha
   * @param fechaApunte
   * @param sumaCanonAlcs
   */
  private void generateApuntesCanon(String referencia,
                                    Map.Entry<String, List<Norma43Movimiento>> valueEntry,
                                    Tmct0alc tmct0alc,
                                    Date fecha,
                                    Date fechaApunte,
                                    Auxiliar auxiliar,
                                    BigDecimal sumaCanonAlcs,
                                    Auxiliar auxiliarCuentaBanco,
                                    BigDecimal margenDescuadres) {
    LOG.debug("[generateApuntesCanon] Inicio ...");

    // Importe del apunte
    BigDecimal importeApunte = new BigDecimal(valueEntry.getKey().split("#")[1]);
    // Diferencia con los canones de los alc
    BigDecimal diferencia = sumaCanonAlcs.subtract(importeApunte);

    // Apunte al debe
    ApunteContable apunte_canon_debe = new ApunteContable();

    apunte_canon_debe.setCuentaContable(cuentaContable_1021101);
    apunte_canon_debe.setDescripcion(referencia);
    apunte_canon_debe.setConcepto("Cobro canon + Com MCG");
    apunte_canon_debe.setImporte(importeApunte);
    apunte_canon_debe.setFecha(fechaApunte);
    apunte_canon_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_canon_debe.setContabilizado(Boolean.FALSE);
    apunte_canon_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_canon_debe.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_canon_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // Los apuntes a la cuenta 1021101 llevan auxiliar por cuenta bancaria
    apunte_canon_debe.setAuxiliar(auxiliarCuentaBanco);

    // Apunte al haber
    ApunteContable apunte_canon_haber = new ApunteContable();

    apunte_canon_haber.setCuentaContable(cuentaContable_1122106);
    apunte_canon_haber.setDescripcion("Cobro canones (" + fecha + ")");
    apunte_canon_haber.setConcepto("Cobro canon + Com MCG");
    apunte_canon_haber.setImporte(sumaCanonAlcs.negate());
    apunte_canon_haber.setFecha(fechaApunte);
    apunte_canon_haber.setApunte(TIPO_CUENTA.HABER.getId());
    apunte_canon_haber.setContabilizado(Boolean.FALSE);
    apunte_canon_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_canon_haber.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_canon_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // Los apuntes a la cuenta 1122106 llevan auxiliar por alias
    apunte_canon_haber.setAuxiliar(auxiliar);

    apunteContableBo.save(apunte_canon_debe);
    apunteContableBo.save(apunte_canon_haber);

    apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_debe, tmct0alc, importeApunte));
    apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_haber, tmct0alc, importeApunte.negate()));

    if (diferencia.compareTo(BigDecimal.ZERO) != 0) {
      ApunteContable apunte_canon_diff = new ApunteContable();

      apunte_canon_diff.setCuentaContable(cuentaContable_4021102);
      apunte_canon_diff.setDescripcion("Diferencia canones  (" + fecha + ")");
      apunte_canon_diff.setConcepto("Diferencia cobro canon + Com MCG");
      apunte_canon_diff.setFecha(fechaApunte);
      apunte_canon_diff.setContabilizado(Boolean.FALSE);
      apunte_canon_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_canon_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_canon_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CANON.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_canon_diff.setImporte(diferencia);
        apunte_canon_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_canon_diff.setImporte(diferencia);
        apunte_canon_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_canon_diff);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_diff, tmct0alc, diferencia));

      if (diferencia.compareTo(margenDescuadres) > 0) {
        LOG.debug("El/Los ALC tiene(n) una imcanoncontreu de '" + sumaCanonAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. Se genera un descuadre.");

        for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
          generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], tmct0alc, sumaCanonAlcs, TIPO_IMPORTE.Canon.getDescripcion(),
                           DESCUACRE_COMENTARIO_DIFMAYOR);
        } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())

        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadrado.");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCanon] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } else {
        LOG.debug("El/Los ALC tiene(n) una imcanoncontreu de '" + sumaCanonAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. No se genera un descuadre.");
        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadre menor que margen ( Importe: "
                                          + valueEntry.getKey().split("#")[1] + ").");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCanon] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } // else
    } else {
      // Se insertan los registros de la Tmct0log
      try {
        tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                    "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Cuadrado ( Importe: "
                                        + valueEntry.getKey().split("#")[1] + ").");
      } catch (Exception ex) {
        LOG.warn("[generateApuntesCanon] Error insertando en Tmct0log ... ", ex.getMessage());
      } // catch
    } // else

    LOG.debug("[generateApuntesCanon] Fin ...");
  } // generateApuntesCanon

  /**
   * Genera y almacena los apuntes de corretaje necesarios.
   * 
   * @param valueEntry
   * @param tmct0alc
   * @param fecha
   * @param fechaApunte
   * @param sumaCorretajeAlcs
   */
  private void generateApuntesCorretaje(String referencia,
                                        Map.Entry<String, List<Norma43Movimiento>> valueEntry,
                                        Tmct0alc tmct0alc,
                                        Date fecha,
                                        Date fechaApunte,
                                        Auxiliar auxiliar,
                                        BigDecimal sumaCorretajeAlcs,
                                        Auxiliar auxiliarCuentaBanco,
                                        BigDecimal margenDescuadres) {
    LOG.debug("[generateApuntesCorretaje] Inicio ...");

    // Importe del apunte
    BigDecimal importeApunte = new BigDecimal(valueEntry.getKey().split("#")[1]);
    // Diferencia con los corretajes de los alc
    BigDecimal diferencia = sumaCorretajeAlcs.subtract(importeApunte);

    // Apunte al debe
    ApunteContable apunte_corretaje_debe = new ApunteContable();

    apunte_corretaje_debe.setCuentaContable(cuentaContable_1021101);
    apunte_corretaje_debe.setDescripcion(referencia);
    apunte_corretaje_debe.setConcepto("Cobro corretaje");
    apunte_corretaje_debe.setImporte(importeApunte);
    apunte_corretaje_debe.setFecha(fechaApunte);
    apunte_corretaje_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_corretaje_debe.setContabilizado(Boolean.FALSE);
    apunte_corretaje_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_corretaje_debe.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_corretaje_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // Los apuntes a la cuenta 1021101 llevan auxiliar por cuenta bancaria
    apunte_corretaje_debe.setAuxiliar(auxiliarCuentaBanco);

    // Apunte al haber
    ApunteContable apunte_corretaje_haber = new ApunteContable();

    apunte_corretaje_haber.setCuentaContable(cuentaContable_1122106);
    apunte_corretaje_haber.setDescripcion("Cobro corretajes (" + fecha + ")");
    apunte_corretaje_haber.setConcepto("Cobro corretaje");
    apunte_corretaje_haber.setImporte(sumaCorretajeAlcs.negate());
    apunte_corretaje_haber.setFecha(fechaApunte);
    apunte_corretaje_haber.setApunte(TIPO_CUENTA.HABER.getId());
    apunte_corretaje_haber.setContabilizado(Boolean.FALSE);
    apunte_corretaje_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_corretaje_haber.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_corretaje_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // Los apuntes a la cuenta 1122106 llevan auxiliar por alias
    apunte_corretaje_haber.setAuxiliar(auxiliar);

    apunteContableBo.save(apunte_corretaje_debe);
    apunteContableBo.save(apunte_corretaje_haber);

    apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_debe, tmct0alc, importeApunte));
    apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_haber, tmct0alc, importeApunte.negate()));

    if (diferencia.compareTo(BigDecimal.ZERO) != 0) {
      ApunteContable apunte_corretaje_diff = new ApunteContable();

      apunte_corretaje_diff.setCuentaContable(cuentaContable_4021102);
      apunte_corretaje_diff.setDescripcion("Diferencia corretajes (" + fecha + ")");
      apunte_corretaje_diff.setConcepto("Diferencia cobro corretaje");
      apunte_corretaje_diff.setFecha(fechaApunte);
      apunte_corretaje_diff.setContabilizado(Boolean.FALSE);
      apunte_corretaje_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_corretaje_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CORRETAJE.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_corretaje_diff);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_diff, tmct0alc, diferencia));

      if (diferencia.compareTo(margenDescuadres) > 0) {
        LOG.debug("El/Los ALC tiene(n) una imcomisn de '" + sumaCorretajeAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. Se genera un descuadre.");

        for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
          generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], tmct0alc, sumaCorretajeAlcs,
                           TIPO_IMPORTE.Corretaje.getDescripcion(), DESCUACRE_COMENTARIO_DIFMAYOR);
        } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())

        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadrado.");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCorretaje] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } else {
        LOG.debug("El/Los ALC tiene(n) una imcomisn de '" + sumaCorretajeAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. No se genera un descuadre.");
        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadre menor que margen ( Importe: "
                                          + valueEntry.getKey().split("#")[1] + ").");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCorretaje] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } // else
    } else {
      // Se insertan los registros de la Tmct0log
      try {
        tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                    "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Cuadrado ( Importe: "
                                        + valueEntry.getKey().split("#")[1] + ").");
      } catch (Exception ex) {
        LOG.warn("[generateApuntesCorretaje] Error insertando en Tmct0log ... ", ex.getMessage());
      } // catch
    } // else

    LOG.debug("[generateApuntesCorretaje] Fin ...");
  }// generateApuntesCorretaje

  /**
   * Genera y almacena los apuntes de canones necesarios.
   * 
   * @param valueEntry
   * @param tmct0alc
   * @param fecha
   * @param fechaApunte
   * @param sumaCanonAlcs
   */
  private void generateApuntesRetrocesionCanon(String referencia,
                                               Map.Entry<String, List<Norma43Movimiento>> valueEntry,
                                               Tmct0alc tmct0alc,
                                               Date fecha,
                                               Date fechaApunte,
                                               Auxiliar auxiliar,
                                               BigDecimal sumaCanonAlcs,
                                               Auxiliar auxiliarCuentaBanco,
                                               BigDecimal margenDescuadres) {
    LOG.debug("[generateApuntesRetrocesionCanon] Inicio ...");

    // Fecha del apunte
    // Date fecha = listaAlc.get(0).getFeejeliq();
    // Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), fecha);
    // Importe del apunte
    BigDecimal importeApunte = new BigDecimal(valueEntry.getKey().split("#")[1]);
    // Diferencia con los canones de los alc
    BigDecimal diferencia = sumaCanonAlcs.subtract(importeApunte);

    // Apunte al debe
    ApunteContable apunte_canon_debe = new ApunteContable();

    apunte_canon_debe.setCuentaContable(cuentaContable_4021101);
    apunte_canon_debe.setDescripcion("Retrocesion canon (" + fecha + ")");
    apunte_canon_debe.setConcepto("Retrocesion canon");
    apunte_canon_debe.setImporte(sumaCanonAlcs);
    apunte_canon_debe.setFecha(fechaApunte);
    apunte_canon_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_canon_debe.setContabilizado(Boolean.FALSE);
    apunte_canon_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_canon_debe.setTipoComprobante(TIPO_COMPROBANTE.RETROCESION_ROUTING.getTipo());
    apunte_canon_debe.setTipoMovimiento(TIPO_APUNTE.CANON_RETROCEDIDO.getId());
    // // Los apuntes a la cuenta 1122106 llevan auxiliar por alias
    // apunte_canon_debe.setAuxiliar(auxiliar);

    // Apunte al haber
    ApunteContable apunte_canon_haber = new ApunteContable();

    apunte_canon_haber.setCuentaContable(cuentaContable_1021101);
    apunte_canon_haber.setDescripcion(referencia);
    apunte_canon_haber.setConcepto("Retrocesion canon");
    apunte_canon_haber.setImporte(importeApunte.negate());
    apunte_canon_haber.setFecha(fechaApunte);
    apunte_canon_haber.setApunte(TIPO_CUENTA.HABER.getId());
    apunte_canon_haber.setContabilizado(Boolean.FALSE);
    apunte_canon_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_canon_haber.setTipoComprobante(TIPO_COMPROBANTE.RETROCESION_ROUTING.getTipo());
    apunte_canon_haber.setTipoMovimiento(TIPO_APUNTE.CANON_RETROCEDIDO.getId());
    // Los apuntes a la cuenta 1021101 llevan auxiliar por cuenta bancaria
    apunte_canon_haber.setAuxiliar(auxiliarCuentaBanco);

    apunteContableBo.save(apunte_canon_debe);
    apunteContableBo.save(apunte_canon_haber);

    apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_debe, tmct0alc, importeApunte));
    apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_haber, tmct0alc, importeApunte.negate()));

    // if (diferencia.compareTo(BigDecimal.ZERO) != 0) {
    // ApunteContable apunte_canon_diff = new ApunteContable();
    //
    // apunte_canon_diff.setCuentaContable(cuentaContable_4021102);
    // apunte_canon_diff.setDescripcion("Diferencia canon (" + fecha + ")");
    // apunte_canon_diff.setConcepto("Diferencia retrocesion canon");
    // apunte_canon_diff.setFecha(fechaApunte);
    // apunte_canon_diff.setContabilizado(Boolean.FALSE);
    // apunte_canon_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
    // apunte_canon_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    // apunte_canon_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CANON.getId());
    //
    // if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
    // apunte_canon_diff.setImporte(diferencia.negate());
    // apunte_canon_diff.setApunte(TIPO_CUENTA.DEBE.getId());
    // } else { // Apunte de diferencias al debe
    // apunte_canon_diff.setImporte(diferencia.negate());
    // apunte_canon_diff.setApunte(TIPO_CUENTA.HABER.getId());
    // } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)
    //
    // apunteContableBo.save(apunte_canon_diff);
    // apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_diff, tmct0alc, diferencia.negate()));
    //
    // if (diferencia.compareTo(margenDescuadres) > 0) {
    // LOG.debug("El/Los ALC tiene(n) una imcanoncontreu de '" + sumaCanonAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
    // + "'. Margen='" + margenDescuadres + "'. Se genera un descuadre.");
    //
    // for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
    // generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], tmct0alc, sumaCanonAlcs, "Retrocesión comisión canon");
    // } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
    //
    // // Se insertan los registros de la Tmct0log
    // // for (Tmct0alc alc : listaAlc) {
    // try {
    // tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
    // "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadrado.");
    // } catch (Exception ex) {
    // LOG.warn("[generateApuntesRetrocesionCanon] Error insertando en Tmct0log ... ", ex.getMessage());
    // } // catch
    // // } // for
    // } else {
    // LOG.debug("El/Los ALC tiene(n) una imcanoncontreu de '" + sumaCanonAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
    // + "'. Margen='" + margenDescuadres + "'. No se genera un descuadre.");
    // // Se insertan los registros de la Tmct0log
    // // for (Tmct0alc alc : listaAlc) {
    // try {
    // tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
    // "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadre menor que margen ( Importe: "
    // + valueEntry.getKey().split("#")[1] + ").");
    // } catch (Exception ex) {
    // LOG.warn("[generateApuntesRetrocesionCanon] Error insertando en Tmct0log ... ", ex.getMessage());
    // } // catch
    // // } // for
    // } // else
    // } else {
    // Se insertan los registros de la Tmct0log
    // for (Tmct0alc alc : listaAlc) {
    try {
      tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(), "Recibido Norma43. Cta: "
                                                                                          + valueEntry.getKey().split("#")[0]
                                                                                          + ". Cuadrado ( Importe: "
                                                                                          + valueEntry.getKey().split("#")[1] + ").");
    } catch (Exception ex) {
      LOG.warn("[generateApuntesRetrocesionCanon] Error insertando en Tmct0log ... ", ex.getMessage());
    } // catch
    // } // for
    // } // else
    LOG.debug("[generateApuntesRetrocesionCanon] Fin ...");
  } // generateApuntesRetrocesionCanon

  /**
   * Genera y almacena los apuntes de corretaje necesarios.
   * 
   * @param valueEntry
   * @param tmct0alc
   * @param fecha
   * @param fechaApunte
   * @param sumaCorretajeAlcs
   */
  private void generateApuntesRetrocesionCorretaje(String referencia,
                                                   Map.Entry<String, List<Norma43Movimiento>> valueEntry,
                                                   Tmct0alc tmct0alc,
                                                   Date fecha,
                                                   Date fechaApunte,
                                                   Auxiliar auxiliar,
                                                   BigDecimal sumaCorretajeAlcs,
                                                   Auxiliar auxiliarCuentaBanco,
                                                   BigDecimal margenDescuadres) {
    LOG.debug("[generateApuntesRetrocesionCorretaje] Inicio ...");

    // Importe del apunte
    BigDecimal importeApunte = new BigDecimal(valueEntry.getKey().split("#")[1]);
    // Diferencia con los corretajes de los alc
    BigDecimal diferencia = sumaCorretajeAlcs.subtract(importeApunte);

    // Apunte al debe
    ApunteContable apunte_corretaje_debe = new ApunteContable();

    apunte_corretaje_debe.setCuentaContable(cuentaContable_4021101);
    apunte_corretaje_debe.setDescripcion("Retrocesion corretaje (" + fecha + ")");
    apunte_corretaje_debe.setConcepto("Retrocesion corretaje");
    apunte_corretaje_debe.setImporte(sumaCorretajeAlcs);
    apunte_corretaje_debe.setFecha(fechaApunte);
    apunte_corretaje_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_corretaje_debe.setContabilizado(Boolean.FALSE);
    apunte_corretaje_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_corretaje_debe.setTipoComprobante(TIPO_COMPROBANTE.RETROCESION_ROUTING.getTipo());
    apunte_corretaje_debe.setTipoMovimiento(TIPO_APUNTE.CORRETAJE_RETROCEDIDO.getId());
    // // Los apuntes a la cuenta 1122106 llevan auxiliar por alias
    // apunte_corretaje_debe.setAuxiliar(auxiliar);

    // Apunte al haber
    ApunteContable apunte_corretaje_haber = new ApunteContable();

    apunte_corretaje_haber.setCuentaContable(cuentaContable_1021101);
    apunte_corretaje_haber.setDescripcion(referencia);
    apunte_corretaje_haber.setConcepto("Retrocesion corretaje");
    apunte_corretaje_haber.setImporte(importeApunte.negate());
    apunte_corretaje_haber.setFecha(fechaApunte);
    apunte_corretaje_haber.setApunte(TIPO_CUENTA.HABER.getId());
    apunte_corretaje_haber.setContabilizado(Boolean.FALSE);
    apunte_corretaje_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_corretaje_haber.setTipoComprobante(TIPO_COMPROBANTE.RETROCESION_ROUTING.getTipo());
    apunte_corretaje_haber.setTipoMovimiento(TIPO_APUNTE.CORRETAJE_RETROCEDIDO.getId());
    // Los apuntes a la cuenta 1021101 llevan auxiliar por cuenta bancaria
    apunte_corretaje_haber.setAuxiliar(auxiliarCuentaBanco);

    apunteContableBo.save(apunte_corretaje_debe);
    apunteContableBo.save(apunte_corretaje_haber);

    apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_debe, tmct0alc, importeApunte));
    apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_haber, tmct0alc, importeApunte.negate()));

    // if (diferencia.compareTo(BigDecimal.ZERO) != 0) {
    // ApunteContable apunte_corretaje_diff = new ApunteContable();
    //
    // apunte_corretaje_diff.setCuentaContable(cuentaContable_4021102);
    // apunte_corretaje_diff.setDescripcion("Diferencia corretaje (" + fecha + ")");
    // apunte_corretaje_diff.setConcepto("Diferencia retrocesion corretaje");
    // apunte_corretaje_diff.setFecha(fechaApunte);
    // apunte_corretaje_diff.setContabilizado(Boolean.FALSE);
    // apunte_corretaje_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
    // apunte_corretaje_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    // apunte_corretaje_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CORRETAJE.getId());
    //
    // if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
    // apunte_corretaje_diff.setImporte(diferencia.negate());
    // apunte_corretaje_diff.setApunte(TIPO_CUENTA.DEBE.getId());
    // } else { // Apunte de diferencias al debe
    // apunte_corretaje_diff.setImporte(diferencia.negate());
    // apunte_corretaje_diff.setApunte(TIPO_CUENTA.HABER.getId());
    // } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)
    //
    // apunteContableBo.save(apunte_corretaje_diff);
    // apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_diff, tmct0alc, diferencia.negate()));
    //
    // if (diferencia.compareTo(margenDescuadres) > 0) {
    // LOG.debug("El/Los ALC tiene(n) una imcomisn de '" + sumaCorretajeAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
    // + "'. Margen='" + margenDescuadres + "'. Se genera un descuadre.");
    //
    // for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
    // generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], tmct0alc, sumaCorretajeAlcs, "Retrocesión corretaje");
    // } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
    //
    // // Se insertan los registros de la Tmct0log
    // try {
    // tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
    // "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadrado.");
    // } catch (Exception ex) {
    // LOG.warn("[generateApuntesRetrocesionCorretaje] Error insertando en Tmct0log ... ", ex.getMessage());
    // } // catch
    // } else {
    // LOG.debug("El/Los ALC tiene(n) una imcomisn de '" + sumaCorretajeAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
    // + "'. Margen='" + margenDescuadres + "'. No se genera un descuadre.");
    // // Se insertan los registros de la Tmct0log
    // try {
    // tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
    // "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadre menor que margen ( Importe: "
    // + valueEntry.getKey().split("#")[1] + ").");
    // } catch (Exception ex) {
    // LOG.warn("[generateApuntesRetrocesionCorretaje] Error insertando en Tmct0log ... ", ex.getMessage());
    // } // catch
    // } // else
    // } else {
    // Se insertan los registros de la Tmct0log
    try {
      tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(), "Recibido Norma43. Cta: "
                                                                                          + valueEntry.getKey().split("#")[0]
                                                                                          + ". Cuadrado ( Importe: "
                                                                                          + valueEntry.getKey().split("#")[1] + ").");
    } catch (Exception ex) {
      LOG.warn("[generateApuntesRetrocesionCorretaje] Error insertando en Tmct0log ... ", ex.getMessage());
    } // catch
    // } // else

    LOG.debug("[generateApuntesRetrocesionCorretaje] Fin ...");
  } // generateApuntesRetrocesionCorretaje

  /**
   * Genera y almacena los apuntes de canones necesarios.
   * 
   * @param valueEntry
   * @param tmct0alc
   * @param fecha
   * @param fechaApunte
   * @param sumaCanonAlcs
   */
  private void generateApuntesCanonSegundo(String referencia,
                                           Map.Entry<String, List<Norma43Movimiento>> valueEntry,
                                           Tmct0alc tmct0alc,
                                           Date fecha,
                                           Date fechaApunte,
                                           Auxiliar auxiliar,
                                           BigDecimal sumaCanonAlcs,
                                           Auxiliar auxiliarCuentaBanco,
                                           BigDecimal margenDescuadres) {
    LOG.debug("[generateApuntesCanonSegundo] Inicio ...");

    // Importe del apunte
    BigDecimal importeApunte = new BigDecimal(valueEntry.getKey().split("#")[1]);
    // Diferencia con los canones de los alc
    BigDecimal diferencia = sumaCanonAlcs.subtract(importeApunte);

    // Apunte al debe
    ApunteContable apunte_canon_debe = new ApunteContable();

    apunte_canon_debe.setCuentaContable(cuentaContable_1021101);
    apunte_canon_debe.setDescripcion(referencia);
    apunte_canon_debe.setConcepto("Abono 2 canon");
    apunte_canon_debe.setImporte(importeApunte);
    apunte_canon_debe.setFecha(fechaApunte);
    apunte_canon_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_canon_debe.setContabilizado(Boolean.FALSE);
    apunte_canon_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_canon_debe.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_canon_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // Los apuntes a la cuenta 1021101 llevan auxiliar por cuenta bancaria
    apunte_canon_debe.setAuxiliar(auxiliarCuentaBanco);

    // Apunte al haber
    ApunteContable apunte_canon_haber = new ApunteContable();

    apunte_canon_haber.setCuentaContable(cuentaContable_4021101);
    apunte_canon_haber.setDescripcion("Abono 2 canon (" + fecha + ")");
    apunte_canon_haber.setConcepto("Abono 2 canon");
    apunte_canon_haber.setImporte(sumaCanonAlcs.negate());
    apunte_canon_haber.setFecha(fechaApunte);
    apunte_canon_haber.setApunte(TIPO_CUENTA.HABER.getId());
    apunte_canon_haber.setContabilizado(Boolean.FALSE);
    apunte_canon_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_canon_haber.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_canon_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // // Los apuntes a la cuenta 1122106 llevan auxiliar por alias
    // apunte_canon_haber.setAuxiliar(auxiliar);

    apunteContableBo.save(apunte_canon_debe);
    apunteContableBo.save(apunte_canon_haber);

    apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_debe, tmct0alc, importeApunte));
    apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_haber, tmct0alc, importeApunte.negate()));

    if (diferencia.compareTo(BigDecimal.ZERO) != 0) {
      ApunteContable apunte_canon_diff = new ApunteContable();

      apunte_canon_diff.setCuentaContable(cuentaContable_4021102);
      apunte_canon_diff.setDescripcion("Diferencia canon (" + fecha + ")");
      apunte_canon_diff.setConcepto("Diferencia abono 2 canon");
      apunte_canon_diff.setFecha(fechaApunte);
      apunte_canon_diff.setContabilizado(Boolean.FALSE);
      apunte_canon_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_canon_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_canon_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CANON.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_canon_diff.setImporte(diferencia);
        apunte_canon_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_canon_diff.setImporte(diferencia);
        apunte_canon_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_canon_diff);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte_canon_diff, tmct0alc, diferencia));

      if (diferencia.compareTo(margenDescuadres) > 0) {
        LOG.debug("El/Los ALC tiene(n) una imcanoncontreu de '" + sumaCanonAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. Se genera un descuadre.");

        for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
          generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], tmct0alc, sumaCanonAlcs, "Abono 2 canon",
                           DESCUACRE_COMENTARIO_DIFMAYOR);
        } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())

        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadrado.");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCanonSegundo] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } else {
        LOG.debug("El/Los ALC tiene(n) una imcanoncontreu de '" + sumaCanonAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. No se genera un descuadre.");
        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadre menor que margen ( Importe: "
                                          + valueEntry.getKey().split("#")[1] + ").");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCanonSegundo] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } // else
    } else {
      // Se insertan los registros de la Tmct0log
      try {
        tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                    "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Cuadrado ( Importe: "
                                        + valueEntry.getKey().split("#")[1] + ").");
      } catch (Exception ex) {
        LOG.warn("[generateApuntesCanonSegundo] Error insertando en Tmct0log ... ", ex.getMessage());
      } // catch
    } // else

    LOG.debug("[generateApuntesCanonSegundo] Fin ...");
  } // generateApuntesCanon

  /**
   * Genera y almacena los apuntes de corretaje necesarios.
   * 
   * @param valueEntry
   * @param tmct0alc
   * @param fecha
   * @param fechaApunte
   * @param sumaCorretajeAlcs
   */
  private void generateApuntesCorretajeSegundo(String referencia,
                                               Map.Entry<String, List<Norma43Movimiento>> valueEntry,
                                               Tmct0alc tmct0alc,
                                               Date fecha,
                                               Date fechaApunte,
                                               Auxiliar auxiliar,
                                               BigDecimal sumaCorretajeAlcs,
                                               Auxiliar auxiliarCuentaBanco,
                                               BigDecimal margenDescuadres) {
    LOG.debug("[generateApuntesCorretajeSegundo] Inicio ...");

    // Importe del apunte
    BigDecimal importeApunte = new BigDecimal(valueEntry.getKey().split("#")[1]);
    // Diferencia con los corretajes de los alc
    BigDecimal diferencia = sumaCorretajeAlcs.subtract(importeApunte);

    // Apunte al debe
    ApunteContable apunte_corretaje_debe = new ApunteContable();

    apunte_corretaje_debe.setCuentaContable(cuentaContable_1021101);
    apunte_corretaje_debe.setDescripcion(referencia);
    apunte_corretaje_debe.setConcepto("Abono 2 corretaje");
    apunte_corretaje_debe.setImporte(importeApunte);
    apunte_corretaje_debe.setFecha(fechaApunte);
    apunte_corretaje_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_corretaje_debe.setContabilizado(Boolean.FALSE);
    apunte_corretaje_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_corretaje_debe.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_corretaje_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // Los apuntes a la cuenta 1021101 llevan auxiliar por cuenta bancaria
    apunte_corretaje_debe.setAuxiliar(auxiliarCuentaBanco);

    // Apunte al haber
    ApunteContable apunte_corretaje_haber = new ApunteContable();

    apunte_corretaje_haber.setCuentaContable(cuentaContable_4021101);
    apunte_corretaje_haber.setDescripcion("Abono 2 corretaje (" + fecha + ")");
    apunte_corretaje_haber.setConcepto("Abono 2 corretaje");
    apunte_corretaje_haber.setImporte(sumaCorretajeAlcs.negate());
    apunte_corretaje_haber.setFecha(fechaApunte);
    apunte_corretaje_haber.setApunte(TIPO_CUENTA.HABER.getId());
    apunte_corretaje_haber.setContabilizado(Boolean.FALSE);
    apunte_corretaje_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_corretaje_haber.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
    apunte_corretaje_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
    // // Los apuntes a la cuenta 1122106 llevan auxiliar por alias
    // apunte_corretaje_haber.setAuxiliar(auxiliar);

    apunteContableBo.save(apunte_corretaje_debe);
    apunteContableBo.save(apunte_corretaje_haber);

    apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_debe, tmct0alc, importeApunte));
    apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_haber, tmct0alc, importeApunte.negate()));

    if (diferencia.compareTo(BigDecimal.ZERO) != 0) {
      ApunteContable apunte_corretaje_diff = new ApunteContable();

      apunte_corretaje_diff.setCuentaContable(cuentaContable_4021102);
      apunte_corretaje_diff.setDescripcion("Diferencia corretaje (" + fecha + ")");
      apunte_corretaje_diff.setConcepto("Diferencia abono 2 corretaje");
      apunte_corretaje_diff.setFecha(fechaApunte);
      apunte_corretaje_diff.setContabilizado(Boolean.FALSE);
      apunte_corretaje_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_corretaje_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CORRETAJE.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_corretaje_diff);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte_corretaje_diff, tmct0alc, diferencia));

      if (diferencia.abs().compareTo(margenDescuadres) > 0) {
        LOG.debug("El/Los ALC tiene(n) una imcomisn de '" + sumaCorretajeAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. Se genera un descuadre.");

        for (Norma43Movimiento norma43Movimiento : valueEntry.getValue()) {
          generarDescuadre(norma43Movimiento, valueEntry.getKey().split("#")[0], tmct0alc, sumaCorretajeAlcs, "Abono 2 corretaje",
                           DESCUACRE_COMENTARIO_DIFMAYOR);
        } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())

        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadrado.");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCorretajeSegundo] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } else {
        LOG.debug("El/Los ALC tiene(n) una imcomisn de '" + sumaCorretajeAlcs + "' y el fichero Norma43 um importe de '" + importeApunte
                  + "'. Margen='" + margenDescuadres + "'. No se genera un descuadre.");
        // Se insertan los registros de la Tmct0log
        try {
          tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                      "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Descuadre menor que margen ( Importe: "
                                          + valueEntry.getKey().split("#")[1] + ").");
        } catch (Exception ex) {
          LOG.warn("[generateApuntesCorretajeSegundo] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
      } // else
    } else {
      // Se insertan los registros de la Tmct0log
      try {
        tmct0logBo.insertarRegistro(tmct0alc, null, null, null, tmct0alc.getCdestadoasig(),
                                    "Recibido Norma43. Cta: " + valueEntry.getKey().split("#")[0] + ". Cuadrado ( Importe: "
                                        + valueEntry.getKey().split("#")[1] + ").");
      } catch (Exception ex) {
        LOG.warn("[generateApuntesCanon] Error insertando en Tmct0log ... ", ex.getMessage());
      } // catch
    } // else
    LOG.debug("[generateApuntesCorretajeSegundo] Fin ...");
  } // generateApuntesCorretajeSegundo

} // CobrosRouting
