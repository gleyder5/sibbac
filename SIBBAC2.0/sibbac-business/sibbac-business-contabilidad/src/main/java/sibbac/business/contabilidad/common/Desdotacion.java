package sibbac.business.contabilidad.common;


import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.CierreContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.ApunteContableDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CierreContable;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;


/**
 * The Class Desdotacion.
 */
@Component
public class Desdotacion {
	protected static final Logger LOG = LoggerFactory.getLogger(Desdotacion.class);

	@Autowired private ApunteContableDao apunteDao;
	@Autowired private ApunteContableAlcDaoImp apunteAlcDao;
	@Autowired private CuentaContableDao cuentaDao;
	@Autowired private CierreContableBo cierreBo;

	private void put(Map< String, Object > params) {
		ApunteContable apunteD;
		ApunteContable apunteH;
		CuentaContable cuenta = cuentaDao.findByCuenta((int) params.get("numCuenta"));
		for ( ApunteContable bean: apunteDao.findByCuentaContableAndFecha(
				cuenta, (Date) params.get("mesPasado")) ) {
			params.put( "sApunte", "H" );
			params.put( "concepto", bean.getConcepto() );
			params.put( "descripcion", bean.getDescripcion() );
			params.put( "cuenta", cuenta);
			(apunteH = new ApunteContable(params)).setImporte(bean.getImporte().negate());
			apunteDao.save(apunteH);
			params.put( "sApunte", "D" );
			params.put( "cuenta", cuentaDao.findByCuenta( 8011101 ) );
			(apunteD = new ApunteContable(params)).setImporte(bean.getImporte());
			apunteDao.save(apunteD);
			for (ApunteContableAlc alc: bean.getAlcs()) {
				apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(),
						alc.getAlc().getNucnfclt(), apunteD.getId(), alc.getImporte());
				apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(),
						alc.getAlc().getNucnfclt(), apunteH.getId(), alc.getImporte());
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW) public void put(Calendar hoy, Calendar mesPasado) {
		try {
			CierreContable cierre = cierreBo.findByMes(mesPasado);
			Map< String, Object > params = new HashMap< String, Object >();
			params.put("mesPasado", mesPasado.getTime());
			params.put("operativa", "N" );
			params.put("contabilizado", false );
			params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.DOTACION);
			params.put("TIPO_APUNTE", TIPO_APUNTE.EFECTIVO_BRUTO.getId());
			params.put("fechFactura", hoy.getTime());
			params.put("numCuenta", 6021103);
			put(params);
			params.put("numCuenta", 6031103);
			put(params);
			cierre.setDesdotado(true);
			cierreBo.save(cierre);
		} catch (Exception e) {
			LOG.error("Error al realizar la desdotacion",e);
		}
	}
}
