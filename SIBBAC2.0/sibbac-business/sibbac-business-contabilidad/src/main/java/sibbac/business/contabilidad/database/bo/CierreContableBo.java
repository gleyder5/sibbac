package sibbac.business.contabilidad.database.bo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.contabilidad.database.dao.CierreContableDao;
import sibbac.business.contabilidad.database.model.CierreContable;
import sibbac.database.bo.AbstractBo;

@Service
public class CierreContableBo extends AbstractBo<CierreContable, Long, CierreContableDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(CierreContableBo.class);
  private static final DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");
  
  @PersistenceContext
  private EntityManager em;

  /**
   * 
   * @param anyo
   * @param mes
   * @param dia
   * @return
   */
  public boolean isCerrado(int anyo, int mes, int dia) {
    LOG.info("[isCerrado] año: " + anyo + ", mes: " + mes + ", día: " + dia);
    boolean cerrado = false;
    try {
      CierreContable cierre = dao.findByMes(anyo * 100 + mes);

      // cerrado = cierre != null && dia < cierre.getDia();
      if (cierre != null) {
        cerrado = Boolean.TRUE;
      }

    } catch (Exception e) {
      LOG.error("Error al mirar el cierre del año: " + anyo + ", mes: " + mes + ", día: " + dia, e);
    }
    return cerrado;
  }

  /**
   * 
   * @param cMes
   * @return
   */
  public CierreContable findByMes(Calendar cMes) {
    int mes = cMes.get(Calendar.YEAR) * 100 + cMes.get(Calendar.MONTH) + 1;
    CierreContable cierre = dao.findByMes(mes);
    if (cierre == null) {
      cierre = new CierreContable();
      cierre.setMes(mes);
    }
    return cierre;
  } // findByMes

  /**
   * 
   * @param sFechaCierre
   * @return
   * @throws ParseException
   */
  public Integer update(String sFechaCierre) throws ParseException {
    Calendar calCierre = new GregorianCalendar();
    calCierre.setTime(dF.parse(sFechaCierre));
    Calendar mesCierre = (Calendar) calCierre.clone();
    mesCierre.add(Calendar.MONTH, -1);
    int mes = mesCierre.get(Calendar.YEAR) * 100 + mesCierre.get(Calendar.MONTH) + 1;
    CierreContable cierre = dao.findByMes(mes);
    if (cierre == null) {
      cierre = new CierreContable(mes, calCierre.get(Calendar.DATE));
    } else {
      if (cierre.getDia() > 0) {
        return cierre.getDia();
      }
      cierre.setDia(calCierre.get(Calendar.DATE));
    }
    dao.save(cierre);
    
    String sQuery = "UPDATE bsnbpsql.TMCT0_CONTA_PERIODOS P SET P.CERRADO_PERIODO = 1  WHERE  P.FHINIPERIODO BETWEEN ?1 AND ?2";
    
    calCierre.add(Calendar.DAY_OF_YEAR, -calCierre.get(Calendar.DAY_OF_MONTH));
    Calendar calInicioCierre = (Calendar) calCierre.clone();
    calInicioCierre.add(Calendar.DAY_OF_YEAR, -(calCierre.get(Calendar.DAY_OF_YEAR)+1));
    
    
    Query q = em.createNativeQuery(sQuery);
    q.setParameter(1, calInicioCierre.getTime());
    q.setParameter(2, calCierre.getTime() );
    
    q.executeUpdate();
    
    
    
    return null;
  } // update

} // CierreContableBo
