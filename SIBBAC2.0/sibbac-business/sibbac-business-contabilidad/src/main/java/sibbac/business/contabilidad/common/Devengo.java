package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dto.RechazoApunteDto;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.SIBBACBusinessException;

@Component
public class Devengo {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para escribir en el log. */
  private static final Logger LOG = LoggerFactory.getLogger(Devengo.class);

  /** Texto a añadir al campo concepto de los apuntes contables retrocedidos. */
  private static final String CONCEPTO_RETROCESION = "Ret. ";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private ApunteContableAlcDaoImp apunteAlcDao;

  @Autowired
  private Tmct0alcDaoImp alcDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  public void putApunte(ApunteContable apunte) {
    apunteContableBo.save(apunte);
    for (ApunteContableAlc alc : apunte.getAlcs()) {
      apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(), alc.getAlc().getNucnfclt(),
                          apunte.getId(), alc.getImporte());
    }
  }

  public void putCorretajeAndCanon(Map<String, Object> params) {
    BigDecimal subtrahend = BigDecimal.ZERO;
    BigDecimal canon = BigDecimal.ZERO;
    Tmct0alc alc = (Tmct0alc) params.get("alc");
    if (alc.getEstadocont().getIdestado() == CONTABILIDAD.PDTE_AJUSTE.getId()) {

      // Si el alc está en estado ajuste, hay que borrar la contabilidad previa de corretaje que haya para él
      subtrahend = apunteAlcDao.sumDevengoCorretaje(alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
    } else {

      // Solo hay que contabilizar el canon si el alc no esta en estado de ajuste
      if (alc.getTmct0alo().getCanoncontrpagoclte() == 1) {
        if (alc.getImcanoncompeu() != null) {
          canon = alc.getImcanoncompeu();
        }
        if (alc.getImcanoncontreu() != null) {
          canon = canon.add(alc.getImcanoncontreu());
        }
        if (alc.getImcanonliqeu() != null) {
          canon = canon.add(alc.getImcanonliqeu());
        }
      }
    }
    params.put("corretaje", alc.getImcomisn().add(alc.getImajusvb() == null ? BigDecimal.ZERO : alc.getImajusvb()).subtract(subtrahend));
    params.put("canon", canon);
  }

  public BigDecimal getComision(Tmct0alc alc) {
    BigDecimal comision = alc.getImfinsvb();
    if (alc.getEstadocont().getIdestado() == CONTABILIDAD.PDTE_AJUSTE.getId()) {

      // Si el alc está en estado ajuste, hay que borrar la contabilidad previa de comisión que haya para él
      comision = comision.subtract(apunteAlcDao.sumDevengoCorretaje(alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq()));
    }
    return comision;
  }

  // private void putCta(int numCta, Map<Integer, CuentaContable> cta) {
  // CuentaContable cuenta = cuentaContableDao.findByCuenta(numCta);
  // cta.put(cuenta.getId().intValue(), cuenta);
  // }

  // private void putTipoAlc(Map<Integer, Map<Integer, Map<Tmct0alcId, List<Map<String, Object>>>>> mCtaTipoAlc, Map<String, Object> bean) {
  // Map<Integer, Map<Tmct0alcId, List<Map<String, Object>>>> mTipoAlc;
  // Map<Tmct0alcId, List<Map<String, Object>>> mAlc;
  // List<Map<String, Object>> lApuntes;
  // Integer cuenta;
  // Integer tipoMovimiento;
  // Tmct0alcId alcId;
  // if ((mTipoAlc = mCtaTipoAlc.get(cuenta = ((CuentaContable) bean.get("cuenta")).getCuenta())) == null) {
  // mCtaTipoAlc.put(cuenta, mTipoAlc = new HashMap<Integer, Map<Tmct0alcId, List<Map<String, Object>>>>());
  // }
  // if ((mAlc = mTipoAlc.get(tipoMovimiento = (int) bean.get("tipo_movimiento"))) == null) {
  // mTipoAlc.put(tipoMovimiento, mAlc = new HashMap<Tmct0alcId, List<Map<String, Object>>>());
  // }
  // if ((lApuntes = (mAlc.get(alcId = (Tmct0alcId) bean.get("alcId")))) == null) {
  // mAlc.put(alcId, lApuntes = new ArrayList<Map<String, Object>>());
  // }
  // lApuntes.add(bean);
  // }

  // private void put(Map<String, Object> params) {
  // Tmct0alcId alcId;
  // ApunteContable apunte = null;
  // BigDecimal suma = BigDecimal.ZERO;
  // boolean primero = true;
  // List<Map<String, Object>> list = (List<Map<String, Object>>) params.get("value");
  // for (Map<String, Object> bean : list) {
  // if (primero) {
  // primero = false;
  // params.put("operativa", bean.get("operativa"));
  // params.put("TIPO_APUNTE", bean.get("tipo_movimiento"));
  // params.put("sApunte", bean.get("apunte"));
  // params.put("concepto", CONCEPTO_RETROCESION + bean.get("concepto"));
  // params.put("descripcion", CONCEPTO_RETROCESION + bean.get("description"));
  // params.put("contabilizado", false);
  // params.put("cuenta", bean.get("cuenta"));
  // params.put("auxiliar", bean.get("auxiliar"));
  // params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.get((int) bean.get("tipo_comprobante")));
  // params.put("fechFactura", bean.get("fechFactura"));
  // apunte = new ApunteContable(params);
  // }
  // params.put("suma", (suma = suma.add((BigDecimal) bean.get("importe"))));
  // LOG.debug("Procesada retrocesion " + (alcId = (Tmct0alcId) bean.get("alcId")).getNuorden() + "/" + alcId.getNbooking() + "/"
  // + alcId.getNucnfclt() + "/" + alcId.getNucnfliq());
  // }
  // if (suma.signum() != 0) {
  // apunte.setImporte(suma.negate());
  // apunteContableBo.save(apunte);
  // apunteAlcDao.insert(apunte.getId(), list);
  // }
  // }

  /**
   * Genera los apuntes contables de retrocesión de los apuntes contables de devengos para las operaciones devengadas y rechazadas posteriormente.
   * 
   * @param rechazosList Lista de apuntes contables a retroceder.
   * @throws SIBBACBusinessException Si ocurre algún error al almacenar el estado contable de los alc.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void generateRetrocesionesDevengos(List<RechazoApunteDto> rechazosList) throws SIBBACBusinessException {
    LOG.debug("[generateRetrocesionesDevengos] Inicio ...");

    ApunteContable apunte;
    Calendar hoy = Calendar.getInstance();

    Map<Long, CuentaContable> mapaCuentasContables = new HashMap<Long, CuentaContable>();
    CuentaContable cuenta;

    Map<Long, Auxiliar> mapaAuxiliaresContables = new HashMap<Long, Auxiliar>();
    Auxiliar auxiliar;

    Map<Date, Date> mapaFechasContabilizacion = new HashMap<Date, Date>();
    Date fechaApunte;

    List<Tmct0alcId> listaAlcs = new ArrayList<Tmct0alcId>();

    // Campo IMPORTE de la tabla TMCT0_APUNTE_CONTABLE_ALC
    // DevengoRouting -> El campo tiene el mismo signo que el del alc (imcomisn+imajusvb o imcanoncontreu+imcanoncompeu+imcanonliqeu)
    // DevengoClearing -> El campo tiene el signo del apunte
    // DevengoCorretajes -> El campo tiene el signo del apunte
    // DevengoFacturacion -> El campo tiene el signo del apunte
    // DevengoComision (Devolución u Ordenante) -> El campo tiene el mismo signo que el del alc (imcomdvo o imcombrk)

    for (RechazoApunteDto rechazo : rechazosList) {
      LOG.debug("[generateRetrocesionesDevengos] Procesando rechazo: {}", rechazo.toString());

      if (!listaAlcs.contains(rechazo.getTmct0alcId())) {
        listaAlcs.add(rechazo.getTmct0alcId());
      } // if

      apunte = new ApunteContable();

      if ((cuenta = mapaCuentasContables.get(rechazo.getCuentaContable())) == null) {
        mapaCuentasContables.put(rechazo.getCuentaContable(), cuenta = cuentaContableDao.findOne(rechazo.getCuentaContable()));
      } // if
      apunte.setCuentaContable(cuenta);

      apunte.setDescripcion(rechazo.getDescripcion());
      apunte.setConcepto(CONCEPTO_RETROCESION.concat(rechazo.getConcepto()));
      // apunte.setImporte(rechazo.getImporte().negate());

      if ((fechaApunte = mapaFechasContabilizacion.get(rechazo.getFecha())) == null) {
        mapaFechasContabilizacion.put(rechazo.getFecha(), fechaApunte = apunteContableBo.getFechaContabilizacion(hoy, rechazo.getFecha()));
      } // if
      apunte.setFecha(fechaApunte);

      apunte.setApunte(rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0 ? TIPO_CUENTA.HABER.getId() : TIPO_CUENTA.DEBE.getId());
      apunte.setContabilizado(Boolean.FALSE);
      apunte.setOperativa(rechazo.getOperativa());
      apunte.setTipoComprobante(rechazo.getTipoComprobante());
      apunte.setTipoMovimiento(rechazo.getTipoMovimiento());

      if (rechazo.getAuxiliarContable() != null) {
        if ((auxiliar = mapaAuxiliaresContables.get(rechazo.getAuxiliarContable())) == null) {
          mapaAuxiliaresContables.put(rechazo.getAuxiliarContable(), auxiliar = auxiliarDao.findOne(Long.valueOf(rechazo.getAuxiliarContable())));
        } // if
        apunte.setAuxiliar(auxiliar);
      } // if

      if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_ROUTING.getTipo()) {
        if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
          apunte.setImporte(rechazo.getImporteApunteAlc()); // Apunte al haber
        } else {
          apunte.setImporte(rechazo.getImporteApunteAlc().negate()); // Apunte al debe
        } // else

        apunteContableBo.save(apunte);
        apunteAlcDao.insert(rechazo.getTmct0alcId().getNbooking(), rechazo.getTmct0alcId().getNuorden(), rechazo.getTmct0alcId().getNucnfliq(),
                            rechazo.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazo.getImporteApunteAlc());
      } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CLEARING.getTipo()) {
        apunte.setImporte(rechazo.getImporteApunteAlc().negate());

        apunteContableBo.save(apunte);
        apunteAlcDao.insert(rechazo.getTmct0alcId().getNbooking(), rechazo.getTmct0alcId().getNuorden(), rechazo.getTmct0alcId().getNucnfliq(),
                            rechazo.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazo.getImporteApunteAlc().negate());
      } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CORRETAJE.getTipo()) {
        apunte.setImporte(rechazo.getImporteApunteAlc().negate());

        apunteContableBo.save(apunte);
        apunteAlcDao.insert(rechazo.getTmct0alcId().getNbooking(), rechazo.getTmct0alcId().getNuorden(), rechazo.getTmct0alcId().getNucnfliq(),
                            rechazo.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazo.getImporteApunteAlc().negate());
      } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_FACTURACION.getTipo()) {
        apunte.setImporte(rechazo.getImporteApunteAlc().negate());

        apunteContableBo.save(apunte);
        apunteAlcDao.insert(rechazo.getTmct0alcId().getNbooking(), rechazo.getTmct0alcId().getNuorden(), rechazo.getTmct0alcId().getNucnfliq(),
                            rechazo.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazo.getImporteApunteAlc().negate());
      } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_COMISION.getTipo()) {
        if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
          apunte.setImporte(rechazo.getImporteApunteAlc()); // Apunte al haber
        } else {
          apunte.setImporte(rechazo.getImporteApunteAlc().negate()); // Apunte al debe
        } // else

        apunteContableBo.save(apunte);
        apunteAlcDao.insert(rechazo.getTmct0alcId().getNbooking(), rechazo.getTmct0alcId().getNuorden(), rechazo.getTmct0alcId().getNucnfliq(),
                            rechazo.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazo.getImporteApunteAlc());
        alcEstadoContDaoImp.updateOrInsertCdestadocont(CONTABILIDAD.RECHAZADA.getId(),
                                               rechazo.getTmct0alcId().getNbooking(),
                                               rechazo.getTmct0alcId().getNuorden(),
                                               rechazo.getTmct0alcId().getNucnfliq(),
                                               rechazo.getTmct0alcId().getNucnfclt(),
                                               rechazo.getTipoMovimiento() == TIPO_APUNTE.COMISION_ORDENANTE.getId() ? TIPO_ESTADO.COMISION_ORDENANTE.getId()
                                                                                                                    : TIPO_ESTADO.COMISION_DEVOLUCION.getId());
      } // else if
    } // for (RechazoApunteDto rechazo : rechazosList)

    for (Tmct0alcId alcId : listaAlcs) {
      alcDao.updateCdestadocont(CONTABILIDAD.RECHAZADA.getId(), alcId);
    } // for

    LOG.debug("[generateRetrocesionesDevengos] Fin ...");
  } // generateRetrocesionesDevengos

  /**
   * Actualiza el estado contable del ALC especificado a estado CONTABILIDAD.RECHAZADA.
   * 
   * @param nuorden Número de orden.
   * @param nbooking Número de booking.
   * @param nucnfclt Número de desglose a cliente.
   * @param nucnfliq Número de desglose de liquidación.
   * @throws SIBBACBusinessException Si ocurre algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void changeCdestadocontWithoutDevengo(List<Tmct0alcId> listaAlcsSinApuntes) throws SIBBACBusinessException {
    for (Tmct0alcId tmct0alcId : listaAlcsSinApuntes) {
      alcDao.updateCdestadocont(CONTABILIDAD.RECHAZADA.getId(), tmct0alcId);
    } // for
  } // changeCdestadoContWithoutDevengo

  // /**
  // * Genera los apuntes contables de retrocesión de los apuntes contables de devengos para las operaciones devengadas y rechazadas posteriormente.
  // *
  // * @param rechazosList Lista de apuntes contables a retroceder.
  // * @throws SIBBACBusinessException Si ocurre algún error al almacenar el estado contable de los alc.
  // */
  // @Transactional(propagation = Propagation.REQUIRES_NEW)
  // public void generateRetrocesion(List<RechazoApunteDto> rechazosList) throws SIBBACBusinessException {
  // LOG.debug("[generateRechazos] Inicio ...");
  //
  // ApunteContable apunte;
  // Calendar hoy = Calendar.getInstance();
  //
  // Map<Long, CuentaContable> mapaCuentasContables = new HashMap<Long, CuentaContable>();
  // CuentaContable cuenta;
  //
  // Map<Long, Auxiliar> mapaAuxiliaresContables = new HashMap<Long, Auxiliar>();
  // Auxiliar auxiliar;
  //
  // Map<Date, Date> mapaFechasContabilizacion = new HashMap<Date, Date>();
  // Date fechaApunte;
  //
  // List<Tmct0alcId> listaAlcs = new ArrayList<Tmct0alcId>();
  //
  // // Campo IMPORTE de la tabla TMCT0_APUNTE_CONTABLE_ALC
  // // DevengoRouting -> El campo tiene el mismo signo que el del alc (imcomisn+imajusvb o imcanoncontreu+imcanoncompeu+imcanonliqeu)
  // // DevengoClearing -> El campo tiene el signo del apunte
  // // DevengoCorretajes -> El campo tiene el signo del apunte
  // // DevengoFacturacion -> El campo tiene el signo del apunte
  // // DevengoComision(Devolución u Ordenante) -> El campo tiene el signo del apunte
  //
  // BigDecimal importeApunteAlcSum = BigDecimal.ZERO;
  // for (RechazoApunteDto rechazo : rechazosList) {
  // importeApunteAlcSum = importeApunteAlcSum.add(rechazo.getImporteApunteAlc());
  //
  // if (!listaAlcs.contains(rechazo.getTmct0alcId())) {
  // listaAlcs.add(rechazo.getTmct0alcId());
  // } // if
  // } // for
  //
  // RechazoApunteDto rechazo = rechazosList.get(0);
  //
  // apunte = new ApunteContable();
  //
  // if ((cuenta = mapaCuentasContables.get(rechazo.getCuentaContable())) == null) {
  // mapaCuentasContables.put(rechazo.getCuentaContable(), cuenta = cuentaContableDao.findOne(rechazo.getCuentaContable()));
  // } // if
  // apunte.setCuentaContable(cuenta);
  //
  // apunte.setDescripcion(rechazo.getDescripcion());
  // apunte.setConcepto(CONCEPTO_RETROCESION.concat(rechazo.getConcepto()));
  // // apunte.setImporte(rechazo.getImporte().negate());
  //
  // if ((fechaApunte = mapaFechasContabilizacion.get(rechazo.getFecha())) == null) {
  // mapaFechasContabilizacion.put(rechazo.getFecha(), fechaApunte = apunteContableBo.getFechaContabilizacion(hoy, rechazo.getFecha()));
  // } // if
  // apunte.setFecha(fechaApunte);
  //
  // apunte.setApunte(rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0 ? TIPO_CUENTA.HABER.getId() : TIPO_CUENTA.DEBE.getId());
  // apunte.setContabilizado(Boolean.FALSE);
  // apunte.setOperativa(rechazo.getOperativa());
  // apunte.setTipoComprobante(rechazo.getTipoComprobante());
  // apunte.setTipoMovimiento(rechazo.getTipoMovimiento());
  //
  // if (rechazo.getAuxiliarContable() != null) {
  // if ((auxiliar = mapaAuxiliaresContables.get(rechazo.getAuxiliarContable())) == null) {
  // mapaAuxiliaresContables.put(rechazo.getAuxiliarContable(), auxiliar = auxiliarDao.findOne(Long.valueOf(rechazo.getAuxiliarContable())));
  // } // if
  // apunte.setAuxiliar(auxiliar);
  // } // if
  //
  // if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_ROUTING.getTipo()) {
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(importeApunteAlcSum.negate());
  // } else {
  // apunte.setImporte(importeApunteAlcSum);
  // } // else
  //
  // apunteContableBo.save(apunte);
  // for (RechazoApunteDto rechazoTmp : rechazosList) {
  // apunteAlcDao.insert(rechazoTmp.getTmct0alcId().getNbooking(), rechazoTmp.getTmct0alcId().getNuorden(), rechazoTmp.getTmct0alcId()
  // .getNucnfliq(),
  // rechazoTmp.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazoTmp.getImporteApunteAlc());
  // } // for
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CLEARING.getTipo()) {
  // apunte.setImporte(importeApunteAlcSum.negate());
  //
  // apunteContableBo.save(apunte);
  // for (RechazoApunteDto rechazoTmp : rechazosList) {
  // apunteAlcDao.insert(rechazoTmp.getTmct0alcId().getNbooking(), rechazoTmp.getTmct0alcId().getNuorden(),
  // rechazoTmp.getTmct0alcId().getNucnfliq(), rechazoTmp.getTmct0alcId().getNucnfclt(), apunte.getId(),
  // rechazoTmp.getImporteApunteAlc().negate());
  // } // for
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CORRETAJE.getTipo()) {
  // apunte.setImporte(importeApunteAlcSum.negate());
  //
  // apunteContableBo.save(apunte);
  // for (RechazoApunteDto rechazoTmp : rechazosList) {
  // apunteAlcDao.insert(rechazoTmp.getTmct0alcId().getNbooking(), rechazoTmp.getTmct0alcId().getNuorden(),
  // rechazoTmp.getTmct0alcId().getNucnfliq(), rechazoTmp.getTmct0alcId().getNucnfclt(), apunte.getId(),
  // rechazoTmp.getImporteApunteAlc().negate());
  // } // for
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_FACTURACION.getTipo()) {
  // apunte.setImporte(importeApunteAlcSum.negate());
  //
  // apunteContableBo.save(apunte);
  // for (RechazoApunteDto rechazoTmp : rechazosList) {
  // apunteAlcDao.insert(rechazoTmp.getTmct0alcId().getNbooking(), rechazoTmp.getTmct0alcId().getNuorden(),
  // rechazoTmp.getTmct0alcId().getNucnfliq(), rechazoTmp.getTmct0alcId().getNucnfclt(), apunte.getId(),
  // rechazoTmp.getImporteApunteAlc().negate());
  // } // for
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_COMISION_DEVOLUCION.getTipo()) {
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(importeApunteAlcSum.negate());
  // } else {
  // apunte.setImporte(importeApunteAlcSum);
  // } // else
  //
  // apunteContableBo.save(apunte);
  // for (RechazoApunteDto rechazoTmp : rechazosList) {
  // apunteAlcDao.insert(rechazoTmp.getTmct0alcId().getNbooking(), rechazoTmp.getTmct0alcId().getNuorden(), rechazoTmp.getTmct0alcId()
  // .getNucnfliq(),
  // rechazoTmp.getTmct0alcId().getNucnfclt(), apunte.getId(), rechazoTmp.getImporteApunteAlc());
  // } // for
  // }
  //
  // for (Tmct0alcId alcId : listaAlcs) {
  // alcDao.updateCdestadocont(CONTABILIDAD.RECHAZADA.getId(), alcId);
  // } // for
  //
  // LOG.debug("[generateRechazos] Fin ...");
  // } // generateRechazos

  // @Transactional(propagation = Propagation.REQUIRES_NEW)
  // public void putRechazos(Map<String, Object> params) throws SIBBACBusinessException {
  // CuentaContable cuenta;
  // Integer tipoMovimiento;
  // Date fecha;
  // Date fechFactura;
  // Integer auxi;
  // Auxiliar auxiliar;
  // String alias;
  // Tmct0alcId alcId;
  // Map<Integer, Map<Date, List<Map<String, Object>>>> mTipoFecha;
  // Map<Date, List<Map<String, Object>>> mFecha;
  // Map<Date, Map<Integer, List<Map<String, Object>>>> mFechaAuxiliar;
  // Map<Date, Map<String, List<Map<String, Object>>>> mFechaAlias;
  // Map<Integer, List<Map<String, Object>>> mAuxi;
  // Map<String, List<Map<String, Object>>> mAlias;
  // List<Map<String, Object>> lApuntes;
  //
  // // cuenta, tipo_movimiento, fecha, lista apt_alc
  // Map<Integer, Map<Integer, Map<Date, List<Map<String, Object>>>>> mCtaTipoFecha = new HashMap<Integer, Map<Integer, Map<Date, List<Map<String,
  // Object>>>>>();
  //
  // // cuenta, tipo_movimiento, idAlc, lista apt_alc
  // Map<Integer, Map<Integer, Map<Tmct0alcId, List<Map<String, Object>>>>> mTipoAlc = new HashMap<Integer, Map<Integer, Map<Tmct0alcId,
  // List<Map<String, Object>>>>>();
  //
  // // tipo_movimiento, fecha, auxiliar, lista apt_alc
  // Map<Integer, Map<Date, Map<Integer, List<Map<String, Object>>>>> mTipoFechaAuxiliar = new HashMap<Integer, Map<Date, Map<Integer,
  // List<Map<String, Object>>>>>();
  //
  // // tipo_movimiento, fecha, alias, lista apt_alc
  // Map<Integer, Map<Date, Map<String, List<Map<String, Object>>>>> mTipoFechaAlias = new HashMap<Integer, Map<Date, Map<String, List<Map<String,
  // Object>>>>>();
  // Map<Integer, CuentaContable> mCta = new HashMap<Integer, CuentaContable>();
  // Map<Integer, Auxiliar> mAuxiliar = new HashMap<Integer, Auxiliar>();
  // Map<Date, Date> mFech = new HashMap<Date, Date>();
  // Calendar hoy = (Calendar) params.get("hoy");
  // putCta(4021101, mCta);
  // putCta(2015208, mCta);
  // putCta(3026104, mCta);
  // putCta(1122108, mCta);
  // putCta(1122107, mCta);
  // putCta(1122106, mCta);
  // for (Map<String, Object> bean : (List<Map<String, Object>>) params.get("list")) {
  // bean.put("cuenta", cuenta = mCta.get((int) bean.get("cuenta")));
  // if (cuenta == null) {
  // continue;
  // }
  // alcDao.updateCdestadocont(CONTABILIDAD.CONTABILIZANDOSE.getId(), (alcId = (Tmct0alcId) bean.get("alcId")).getNbooking(), alcId.getNuorden(),
  // alcId.getNucnfliq(), alcId.getNucnfclt());
  // if ((auxi = (Integer) bean.get("auxiliar")) != null) {
  // if ((auxiliar = mAuxiliar.get(auxi)) == null) {
  // mAuxiliar.put(auxi, auxiliar = auxiliarDao.findOne(Long.valueOf(auxi)));
  // }
  // bean.put("auxiliar", auxiliar);
  // }
  // alcDao.updateCdestadocont(CONTABILIDAD.RECHAZADA.getId(), alcId);
  // if ((fechFactura = mFech.get(fecha = (Date) bean.get("fecha"))) == null) {
  // mFech.put(fecha, fechFactura = apunteContableBo.getFechaContabilizacion(hoy, fecha));
  // }
  // bean.put("fechFactura", fechFactura);
  // switch (cuenta.getCuenta()) {
  // case 4021101:
  // if (!"H".equals(bean.get("apunte"))) {
  // continue;
  // }
  // switch ((int) bean.get("tipo_comprobante")) {
  // case 70:
  // case 75:
  //
  // // Uno por tipo_movimiento y fecha
  // if ((mTipoFecha = mCtaTipoFecha.get(cuenta.getCuenta())) == null) {
  // mCtaTipoFecha.put(cuenta.getCuenta(), mTipoFecha = new HashMap<Integer, Map<Date, List<Map<String, Object>>>>());
  // }
  // if ((mFecha = mTipoFecha.get(tipoMovimiento = (int) bean.get("tipo_movimiento"))) == null) {
  // mTipoFecha.put(tipoMovimiento, mFecha = new HashMap<Date, List<Map<String, Object>>>());
  // }
  // if ((lApuntes = (mFecha.get(fecha))) == null) {
  // mFecha.put(fecha, lApuntes = new ArrayList<Map<String, Object>>());
  // }
  // lApuntes.add(bean);
  // break;
  // case 72:
  //
  // // Uno por tipo_movimiento y alc
  // putTipoAlc(mTipoAlc, bean);
  // break;
  // }
  // break;
  // case 2015208:
  // if (!"H".equals(bean.get("apunte"))) {
  // continue;
  // }
  //
  // // Uno por tipo_movimiento y alc
  // putTipoAlc(mTipoAlc, bean);
  // break;
  // case 3026104:
  // case 1122108:
  // if (!"D".equals(bean.get("apunte"))) {
  // continue;
  // }
  //
  // // Uno por tipo_movimiento y alc
  // putTipoAlc(mTipoAlc, bean);
  // break;
  // case 1122106:
  // if (!"D".equals(bean.get("apunte"))) {
  // continue;
  // }
  //
  // // Uno por tipo_movimiento y auxiliar
  // if ((mFechaAuxiliar = mTipoFechaAuxiliar.get(tipoMovimiento = (int) bean.get("tipo_movimiento"))) == null) {
  // mTipoFechaAuxiliar.put(tipoMovimiento, mFechaAuxiliar = new HashMap<Date, Map<Integer, List<Map<String, Object>>>>());
  // }
  // if ((mAuxi = mFechaAuxiliar.get(fecha)) == null) {
  // mFechaAuxiliar.put(fecha, mAuxi = new HashMap<Integer, List<Map<String, Object>>>());
  // }
  // if ((lApuntes = mAuxi.get(auxi)) == null) {
  // mAuxi.put(auxi, lApuntes = new ArrayList<Map<String, Object>>());
  // }
  // lApuntes.add(bean);
  // break;
  // case 1122107:
  // if (!"D".equals(bean.get("apunte"))) {
  // continue;
  // }
  //
  // // Uno por tipo_movimiento y alias
  // if ((mFechaAlias = mTipoFechaAlias.get(tipoMovimiento = (int) bean.get("tipo_movimiento"))) == null) {
  // mTipoFechaAlias.put(tipoMovimiento, mFechaAlias = new HashMap<Date, Map<String, List<Map<String, Object>>>>());
  // }
  // if ((mAlias = mFechaAlias.get(fecha)) == null) {
  // mFechaAlias.put(fecha, mAlias = new HashMap<String, List<Map<String, Object>>>());
  // }
  // if ((lApuntes = mAlias.get(alias = (String) bean.get("alias"))) == null) {
  // mAlias.put(alias, lApuntes = new ArrayList<Map<String, Object>>());
  // }
  // lApuntes.add(bean);
  // break;
  // default:
  // continue;
  // }
  // }
  // for (Entry<Integer, Map<Integer, Map<Date, List<Map<String, Object>>>>> entryCta : mCtaTipoFecha.entrySet()) {
  // for (Entry<Integer, Map<Date, List<Map<String, Object>>>> entryTipo : entryCta.getValue().entrySet()) {
  // for (Entry<Date, List<Map<String, Object>>> entryFecha : entryTipo.getValue().entrySet()) {
  // params.put("value", entryFecha.getValue());
  // put(params);
  // }
  // }
  // }
  // for (Entry<Integer, Map<Integer, Map<Tmct0alcId, List<Map<String, Object>>>>> entryCta : mTipoAlc.entrySet()) {
  // for (Entry<Integer, Map<Tmct0alcId, List<Map<String, Object>>>> entryTipo : entryCta.getValue().entrySet()) {
  // for (Entry<Tmct0alcId, List<Map<String, Object>>> entryAlc : entryTipo.getValue().entrySet()) {
  // params.put("value", entryAlc.getValue());
  // put(params);
  // }
  // }
  // }
  // for (Entry<Integer, Map<Date, Map<Integer, List<Map<String, Object>>>>> entryTipo : mTipoFechaAuxiliar.entrySet()) {
  // for (Entry<Date, Map<Integer, List<Map<String, Object>>>> entryFecha : entryTipo.getValue().entrySet()) {
  // for (Entry<Integer, List<Map<String, Object>>> entryAuxiliar : entryFecha.getValue().entrySet()) {
  // params.put("value", entryAuxiliar.getValue());
  // put(params);
  // }
  // }
  // }
  // for (Entry<Integer, Map<Date, Map<String, List<Map<String, Object>>>>> entryTipo : mTipoFechaAlias.entrySet()) {
  // for (Entry<Date, Map<String, List<Map<String, Object>>>> entryFecha : entryTipo.getValue().entrySet()) {
  // for (Entry<String, List<Map<String, Object>>> entryAlias : entryFecha.getValue().entrySet()) {
  // params.put("value", entryAlias.getValue());
  // put(params);
  // }
  // }
  // }
  // } // putRechazos

  // /**
  // * Genera los apuntes contables de retrocesión de los apuntes contables de devengos para las operaciones devengadas y rechazadas posteriormente.
  // *
  // * @param rechazosList Lista de apuntes contables a retroceder.
  // * @throws SIBBACBusinessException Si ocurre algún error al almacenar el estado contable de los alc.
  // */
  // @Transactional(propagation = Propagation.REQUIRES_NEW)
  // public void generateRetrocesion(List<RechazoApunteDto> rechazosList) throws SIBBACBusinessException {
  // LOG.debug("[generateRechazos] Inicio ...");
  //
  // // TODO Si nos piden agrupar según están hechos los devengos descomentar y terminar
  // // Map<String, Auxiliar> auxiliarMap = new HashMap<String, Auxiliar>();
  // // Map<Date, Date> fechaContabilizacionMap = new HashMap<Date, Date>();
  // //
  // // Map<Date, Map<Long, List<RechazoApunteDto>>> routingMap = new TreeMap<Date, Map<Long, List<RechazoApunteDto>>>();
  // // Map<Long, List<RechazoApunteDto>> diaMap;
  // //
  // // List<RechazoApunteDto> rechazosApuntesDtoList;
  // //
  // // for (RechazoApunteDto rechazo : rechazosList) {
  // // if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_ROUTING.getTipo()) {
  // //
  // // if ((diaMap = routingMap.get(rechazo.getFecha())) == null) {
  // // routingMap.put(rechazo.getFecha(), diaMap = new HashMap<Long, List<RechazoApunteDto>>());
  // // }
  // //
  // // if ((rechazosApuntesDtoList = diaMap.get(rechazo.getAuxiliarContable())) == null) {
  // // diaMap.put(rechazo.getAuxiliarContable(), rechazosApuntesDtoList = new ArrayList<RechazoApunteDto>());
  // // }
  // // rechazosApuntesDtoList.add(rechazo);
  // //
  // // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CLEARING.getTipo()) {
  // //
  // // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CORRETAJE.getTipo()) {
  // //
  // // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_FACTURACION.getTipo()) {
  // //
  // // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_COMISION_DEVOLUCION.getTipo()) {
  // //
  // // }
  // // } // for (RechazoApunteDto rechazo : rechazosList)
  //
  // Map<String, Object> params = new HashMap<String, Object>();
  // Tmct0alo tmct0alo;
  // Tmct0alc tmct0alc;
  // BigDecimal corretaje;
  // BigDecimal canon;
  // BigDecimal comision;
  //
  // ApunteContable apunte;
  // Calendar hoy = Calendar.getInstance();
  //
  // Map<Long, CuentaContable> mapaCuentasContables = new HashMap<Long, CuentaContable>();
  // CuentaContable cuenta;
  //
  // Map<Long, Auxiliar> mapaAuxiliaresContables = new HashMap<Long, Auxiliar>();
  // Auxiliar auxiliar;
  //
  // Map<Date, Date> mapaFechasContabilizacion = new HashMap<Date, Date>();
  // Date fechaApunte;
  //
  // List<Tmct0alcId> listaAlcs = new ArrayList<Tmct0alcId>();
  //
  // for (RechazoApunteDto rechazo : rechazosList) {
  // apunte = new ApunteContable();
  //
  // if ((cuenta = mapaCuentasContables.get(rechazo.getCuentaContable())) == null) {
  // mapaCuentasContables.put(rechazo.getCuentaContable(), cuenta = cuentaContableDao.findOne(rechazo.getCuentaContable()));
  // } // if
  // apunte.setCuentaContable(cuenta);
  //
  // apunte.setDescripcion(rechazo.getDescripcion());
  // apunte.setConcepto(CONCEPTO_RETROCESION.concat(rechazo.getConcepto()));
  // // apunte.setImporte(rechazo.getImporte().negate());
  //
  // if ((fechaApunte = mapaFechasContabilizacion.get(rechazo.getFecha())) == null) {
  // mapaFechasContabilizacion.put(rechazo.getFecha(), fechaApunte = apunteContableBo.getFechaContabilizacion(hoy, rechazo.getFecha()));
  // } // if
  // apunte.setFecha(fechaApunte);
  //
  // apunte.setApunte(rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0 ? TIPO_CUENTA.HABER.getId() : TIPO_CUENTA.DEBE.getId());
  // apunte.setContabilizado(Boolean.FALSE);
  // apunte.setOperativa(rechazo.getOperativa());
  // apunte.setTipoComprobante(rechazo.getTipoComprobante());
  // apunte.setTipoMovimiento(rechazo.getTipoMovimiento());
  //
  // if (rechazo.getAuxiliarContable() != null) {
  // if ((auxiliar = mapaAuxiliaresContables.get(rechazo.getAuxiliarContable())) == null) {
  // mapaAuxiliaresContables.put(rechazo.getAuxiliarContable(), auxiliar = auxiliarDao.findOne(Long.valueOf(rechazo.getAuxiliarContable())));
  // } // if
  // apunte.setAuxiliar(auxiliar);
  // } // if
  //
  // if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_ROUTING.getTipo()) {
  // params.clear();
  // params.put("alc", tmct0alc = new Tmct0alc(rechazo.getTmct0alcId()));
  // tmct0alc.setTmct0alo(tmct0alo = new Tmct0alo());
  // tmct0alc.setEstadocont(new Tmct0estado(rechazo.getCdestadocont()));
  // tmct0alo.setCanoncontrpagoclte(rechazo.getCanoncontrpagoclte());
  // tmct0alc.setImcanoncompeu(rechazo.getImcanoncompeu());
  // tmct0alc.setImcanoncontreu(rechazo.getImcanoncontreu());
  // tmct0alc.setImcanonliqeu(rechazo.getImcanonliqeu());
  // tmct0alc.setImcomisn(rechazo.getImcomisn());
  // tmct0alc.setImajusvb(rechazo.getImajusvb());
  // this.putCorretajeAndCanon(params);
  //
  // if (rechazo.getTipoMovimiento() == TIPO_APUNTE.CORRETAJE.getId()) {
  // if ((corretaje = (BigDecimal) params.get("corretaje")).signum() != 0) {
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(corretaje.negate());
  // } else {
  // apunte.setImporte(corretaje);
  // } // else
  // } // if
  // } // if (rechazo.getTipoMovimiento() == TIPO_APUNTE.CORRETAJE.getId())
  //
  // if (rechazo.getTipoMovimiento() == TIPO_APUNTE.CANON.getId()) {
  // if ((canon = (BigDecimal) params.get("canon")).signum() != 0) {
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(canon.negate());
  // } else {
  // apunte.setImporte(canon);
  // } // else
  // } // if
  // } // if (rechazo.getTipoMovimiento() == TIPO_APUNTE.CANON.getId())
  //
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CLEARING.getTipo()) {
  // params.clear();
  // params.put("alc", tmct0alc = new Tmct0alc(rechazo.getTmct0alcId()));
  // tmct0alc.setEstadocont(new Tmct0estado(rechazo.getCdestadocont()));
  // tmct0alc.setImfinsvb(rechazo.getImfinsvb());
  //
  // comision = this.getComision(tmct0alc);
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(comision.negate());
  // } else {
  // apunte.setImporte(comision);
  // }
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_CORRETAJE.getTipo()) {
  // params.clear();
  // params.put("alc", tmct0alc = new Tmct0alc(rechazo.getTmct0alcId()));
  // tmct0alc.setEstadocont(new Tmct0estado(rechazo.getCdestadocont()));
  // tmct0alc.setImfinsvb(rechazo.getImfinsvb());
  //
  // comision = this.getComision(tmct0alc);
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(comision.negate());
  // } else {
  // apunte.setImporte(comision);
  // }
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_FACTURACION.getTipo()) {
  // params.clear();
  // params.put("alc", tmct0alc = new Tmct0alc(rechazo.getTmct0alcId()));
  // tmct0alc.setEstadocont(new Tmct0estado(rechazo.getCdestadocont()));
  // tmct0alc.setImfinsvb(rechazo.getImfinsvb());
  //
  // comision = this.getComision(tmct0alc);
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(comision.negate());
  // } else {
  // apunte.setImporte(comision);
  // } // else
  // } else if (rechazo.getTipoComprobante() == TIPO_COMPROBANTE.DEVENGO_COMISION_DEVOLUCION.getTipo()) {
  // if (rechazo.getApunte().compareTo(TIPO_CUENTA.DEBE.getId()) == 0) {
  // apunte.setImporte(rechazo.getImporte().negate());
  // } else {
  // apunte.setImporte(rechazo.getImporte());
  // } // else
  // } // else if
  //
  // apunteContableBo.save(apunte);
  // apunteAlcDao.insert(rechazo.getTmct0alcId().getNbooking(), rechazo.getTmct0alcId().getNuorden(), rechazo.getTmct0alcId().getNucnfliq(),
  // rechazo.getTmct0alcId().getNucnfclt(), apunte.getId(), apunte.getImporte());
  //
  // if (!listaAlcs.contains(rechazo.getTmct0alcId())) {
  // listaAlcs.add(rechazo.getTmct0alcId());
  // } // if
  // } // for (RechazoApunteDto rechazo : rechazosList)
  //
  // for (Tmct0alcId alcId : listaAlcs) {
  // alcDao.updateCdestadocont(CONTABILIDAD.RECHAZADA.getId(), alcId);
  // } // for
  //
  // LOG.debug("[generateRechazos] Fin ...");
  // } // generateRechazos
}
