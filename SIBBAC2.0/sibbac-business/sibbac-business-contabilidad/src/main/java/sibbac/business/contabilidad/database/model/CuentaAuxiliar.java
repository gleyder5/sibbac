package sibbac.business.contabilidad.database.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.CuentaAuxiliar}.
 */
@Table(name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.CUENTA_AUXILIAR,
uniqueConstraints=
@UniqueConstraint(columnNames={"TIPOLOGIA","TIPO_IMPORTE","CUENTA"})
)
@Entity
@Component
@XmlRootElement
@Audited
public class CuentaAuxiliar extends ATableAudited<CuentaAuxiliar> {
	private static final long serialVersionUID = 4816090872272022250L;

	/**
	 * Tipologia. Responde a lo definido en {@link sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA}
	 */
	@Column(name="TIPOLOGIA", length=2) private int tipologia;

	/**
	 * Tipo_importe. Responde a lo definido en {@link sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE}
	 */
	@Column(name="TIPO_IMPORTE", length=2) private int tipoImporte;

	@ManyToOne(optional=false, cascade={CascadeType.PERSIST, CascadeType.MERGE}, fetch=FetchType.LAZY)
	@JoinColumn(name = "CUENTA", nullable = false)
	private CuentaBancaria cuenta;

	@ManyToOne(optional=false, cascade={CascadeType.PERSIST, CascadeType.MERGE}, fetch=FetchType.LAZY)
	@JoinColumn(name = "AUXILIAR", nullable = false)
	private Auxiliar auxiliar;

	public CuentaAuxiliar() {}

	public CuentaAuxiliar(CuentaBancaria cuenta, int tipologia, int tipoImporte, Auxiliar auxiliar) {
		this.cuenta = cuenta;
		this.tipologia = tipologia;
		this.tipoImporte = tipoImporte;
		this.auxiliar = auxiliar;
	}

	public int getTipologia() {
		return tipologia;
	}

	public void setTipologia( int tipologia ) {
		this.tipologia = tipologia;
	}

	public int getTipoImporte() {
		return tipoImporte;
	}

	public void setTipoImporte( int tipoImporte ) {
		this.tipoImporte = tipoImporte;
	}

	public CuentaBancaria getCuenta() {
		return cuenta;
	}

	public void setCuenta( CuentaBancaria cuenta ) {
		this.cuenta = cuenta;
	}

	public Auxiliar getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar( Auxiliar auxiliar ) {
		this.auxiliar = auxiliar;
	}
}
