package sibbac.business.contabilidad.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

@Entity @Component @Audited
@Table(name = DBConstants.TABLE_PREFIX+DBConstants.CONTABLES.APUNTE_FECHA)
public class ApunteFecha extends ATableAudited<ApunteFecha> {
	private static final long serialVersionUID = -4854925555457611863L;

//	Id de la table ApunteDetalle
	@Column(name="ID_DETALLE", nullable=false, unique=true) private Long idDetalle;

//	Fecha en la que se realiza el apunte contable
	@Column(name="FECHA") @Temporal(TemporalType.DATE) private Date fecha;

//	Estado contable en el que queda después del apunte contable implicado
	@Column(name="CDESTADOCONT") private Integer cdestadocont;

//	Importe cobrado hasta el momento. Suma de imputaciones en la cuenta de bancos (1021101)
	@Column(name="COBRADO", precision=18, scale=4) private BigDecimal cobrado = BigDecimal.ZERO;
}
