package sibbac.business.contabilidad.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.model.CuentaContable;


@Component
public interface CuentaContableDao extends JpaRepository< CuentaContable, Long > {
	public CuentaContable findByCuenta(int cuenta );

	@Query(value = "select id,cuenta,nombre from tmct0_cuenta_contable where INSTR(UPPER (trim(cuenta)||'-'||"
			+ "trim(nombre)),upper(:text))<>0 FETCH FIRST 12 ROWS ONLY", nativeQuery = true )
	List< Object[] > findByText(@Param( "text" ) String text);

	@Query("SELECT cuentaCont FROM CuentaContable cuentaCont order by cuentaCont.nombre asc")
	public List<CuentaContable> findCuentaContOrderByNombre();

	int countByCuenta(int cuenta);
}
