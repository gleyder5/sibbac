package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.AlcEstadoCont;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.SIBBACBusinessException;


@Component
public class PagoComision {
	private static DateFormat dF = new SimpleDateFormat("yyyyMMdd");

	@Autowired
	private ApunteContableBo apunteBo;

	@Autowired
	private ApunteContableAlcDao apunteAlcDao;

	@Autowired
	private CuentaContableDao cuentaDao;

	@Autowired
	private Tmct0estadoDao estadoDao;

	@Autowired
	private Tmct0alcDaoImp alcEstadoContBo;

	@Autowired
	private AuxiliarDao auxiliarDao;

	@Autowired
	private Tmct0alcDao alcDao;

	@Transactional
    public void generatePayLetter(Map<String, Object> params) throws SIBBACBusinessException {
  	  TIPO_ESTADO tipoEstado = ( TIPO_ESTADO ) params.get("TIPO_ESTADO");
  	  params.put("hoy", new GregorianCalendar());
  	  params.put("operativa", "N");
  	  params.put("contabilizado", false);
  	  params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.PAGO_COMISION_DEVOLUCION);
  	  for (Tmct0alc alc: alcDao.findAlc((String) params.get("alias"), (Date) params.get("fechIni"),
  			  (Date) params.get("fechFin"))) {
  		  for (AlcEstadoCont alcEstado: alc.getEstadosContables()) {
  			  if (alcEstado.getTipoEstado()==tipoEstado && alcEstado.getEstadoCont().equals(
  					  estadoDao.findOne(CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId()))) {
  				  params.put( "alc", alc);
  				  save(params);
  				  break;
  			  }
  		  }
  	  }
    }

	public void save(Map<String, Object> params) throws SIBBACBusinessException {
		ApunteContable apunte;
		BigDecimal importe;
		Tmct0alc alc = ( Tmct0alc ) params.get( "alc" );
		Date feejeliq = alc.getFeejeliq();
		Tmct0estado cobrada = estadoDao.findOne(CONTABILIDAD.COBRADA.getId());
		TIPO_ESTADO tipoEstado = (TIPO_ESTADO) params.get("TIPO_ESTADO");
		params.put("fechFactura", apunteBo.getFechaContabilizacion((Calendar) params.get("hoy"), feejeliq));
		params.put("descripcion", alc.getTmct0alo().getCdalias().trim() + "/"
				+ dF.format(feejeliq) + "/" + alc.getNbtitliq());
		params.put("auxiliar", auxiliarDao.findByNuorden(alc.getId().getNuorden()));
		if (TIPO_ESTADO.COMISION_DEVOLUCION==tipoEstado) {

//			Pagos de comisión de devolución
			params.put("TIPO_APUNTE", TIPO_APUNTE.COMISION_DEVOLUCION.getId());
			params.put("concepto", "Pago Com Devolución");
			importe = alc.getImcomdvo();
		} else {

//			Pagos de comisión de ordenante
			params.put("TIPO_APUNTE", TIPO_APUNTE.COMISION_ORDENANTE.getId());
			params.put("concepto", "Pago Com Ordenante");
			importe = alc.getImcombrk();
		}
		params.put("sApunte", "D");
		params.put("cuenta", cuentaDao.findByCuenta(2015208));
		(apunte = new ApunteContable(params)).setImporte(importe);
		apunteBo.save(apunte);
		apunteAlcDao.save(new ApunteContableAlc(apunte,alc,importe));
		params.put("sApunte", "H");
		params.put("cuenta", cuentaDao.findByCuenta(1021101));
		(apunte = new ApunteContable(params)).setImporte(importe.negate());
		apunteBo.save(apunte);
		apunteAlcDao.save(new ApunteContableAlc(apunte,alc,importe));
		alcEstadoContBo.setEstadocont(alc, cobrada, tipoEstado);
	}
}
