package sibbac.business.contabilidad.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.model.CierreContable;


@Component
public interface CierreContableDao extends JpaRepository< CierreContable, Long > {
	public CierreContable findByMes( int mes );
}
