package sibbac.business.contabilidad.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDaoImp;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.wrappers.database.dao.Tmct0CuentaLiquidacionDao;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceCuentaContable implements SIBBACServiceBean {

	private static final Logger							LOG					= LoggerFactory.getLogger( SIBBACServiceCuentaContable.class );
	private static final List< String >					commands			= new ArrayList< String >();
	private static String								sCommands			= "";
	private static final Map< String, Object >	mStatico	= new HashMap< String, Object >();

	@Autowired
	private CuentaContableDao dao;

	@Autowired
	private CuentaContableDaoImp daoImp;

	@Autowired
	private AuxiliarDao auxiliarDao;

	@Autowired
	private Tmct0CuentaLiquidacionDao ctaLiqDao;

	static {
		Vector< String > vector;
		List<Vector< String >> list;
		commands.add( Constantes.CMD_INIT );
		commands.add( Constantes.CMD_CUENTAS );
		commands.add( Constantes.CMD_FIND);
		commands.add( Constantes.CMD_ADD);
		commands.add( Constantes.CMD_DELETE);
		for ( String aC : commands ) {
			sCommands += aC + ", ";
		}
		sCommands = sCommands.substring( 0, sCommands.length() - 2 );
		list = new ArrayList< Vector< String > >();
		list.add(vector = new Vector< String >());
		vector.add("");
		vector.add("");
		list.add(vector = new Vector< String >());
		vector.add("N");
		vector.add("Nacional");
		list.add(vector = new Vector< String >());
		vector.add("I");
		vector.add("Internacional");
		mStatico.put("operativa", list);
		list = new ArrayList< Vector< String > >();
		list.add(vector = new Vector< String >());
		vector.add("");
		vector.add("");
		list.add(vector = new Vector< String >());
		vector.add("A");
		vector.add("Activo");
		list.add(vector = new Vector< String >());
		vector.add("P");
		vector.add("Pasivo");
		list.add(vector = new Vector< String >());
		vector.add("X");
		vector.add("PyG");
		mStatico.put( "tipo", list);
		list = new ArrayList< Vector< String > >();
		list.add(vector = new Vector< String >());
		vector.add("");
		vector.add("");
		list.add(vector = new Vector< String >());
		vector.add("1");
		vector.add("Si");
		list.add(vector = new Vector< String >());
		vector.add("0");
		vector.add("No");
		mStatico.put( "withAuxiliar", list);
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
	    LOG.trace("[SIBBACServiceCuentaContable :: process] Command: " + command);
		try {
			switch ( command ) {
				case Constantes.CMD_INIT:
					webResponse.setResultados( init() );
					break;
				case Constantes.CMD_CUENTAS:
					webResponse.setResultados( getCuentas(webRequest.getFilters()) );
					break;
				case Constantes.CMD_FIND:
					webResponse.setResultados( filtra(webRequest.getFilters()) );
					break;
				case Constantes.CMD_ADD:
					daoImp.insert(webRequest.getFilters());
					break;
				case Constantes.CMD_DELETE:
					daoImp.delete(webRequest.getFilters());
					break;
				default:
					webResponse.setError( "An action must be set. Valid actions are: " + sCommands );
			}
		} catch ( Exception e ) {
			Map< String, Object > map = new HashMap< String, Object >();
			map.put( "error", e.getMessage() );
			webResponse.setResultados( map );
			LOG.error("Error en el funcionamiento del proceso SIBBACServiceCuentaContable", e);
		}
		return webResponse;
	}

	private Map< String, Object > init() throws Exception {
		Map<String, Object> mSalida = new HashMap<String, Object>();
		Map<String, Object> map;
		List<Map<String, Object>> list;
		mSalida.putAll(mStatico);
		mSalida.put("lCtaLiq", list = new ArrayList<Map<String,Object>>());
		for (Tmct0CuentaLiquidacion bean: ctaLiqDao.findAll()) {
			list.add(map = new HashMap<String, Object>());
			map.put("id", bean.getId());
			map.put("cdCodigo", bean.getCdCodigo());
		}
		mSalida.put( "lAuxiliar", list = new ArrayList<Map<String,Object>>());
		for ( Auxiliar bean : auxiliarDao.findAll()) {
			list.add(map = new HashMap<String, Object>());
			map.put("id", bean.getId());
			map.put("nombre", bean.getNombre().trim());
		}
		return mSalida;
	}

	private Map< String, Object > getCuentas(Map< String, String > filters) throws Exception {
		List< Map< String, Object > > list = new ArrayList< Map<String,Object> >(); 
		Map< String, Object > map;
		Map< String, Object > mSalida = new LinkedHashMap< String, Object >();
		for ( Object[] objects : dao.findByText(filters.get( "text" ))) {
			list.add( map = new HashMap< String, Object >());
			map.put( "id", objects[ 0 ]);
			map.put( "cuenta", objects[ 1 ]);
			map.put( "nombre", ((String) objects[ 2 ]).trim());
		}
		mSalida.put( "cuentas", list);
		return mSalida;
	}

	public Map<String, Object> filtra( Map< String, String > filters ) throws Exception {
		Auxiliar auxiliar;
		Map< String, Object > mSalida = new HashMap< String, Object >();
		List< Map< String, Object >> lCuenta = new ArrayList< Map< String, Object >>();
		Map< String, Object > mDato;
		for (CuentaContable cuenta : daoImp.find(filters)) {
			lCuenta.add(mDato = new HashMap<String, Object>());
			mDato.put("id", cuenta.getId());
			mDato.put("cuenta", cuenta.getCuenta());
			mDato.put("nombre", cuenta.getNombre());
        	mDato.put("ctaLiq", cuenta.getCtaLiq().getCdCodigo());
        	mDato.put("operativa", cuenta.getOperativa());
        	mDato.put("tipo", cuenta.getTipo());
        	mDato.put("withAuxiliar", cuenta.isWithAuxiliar());
        	mDato.put("auxiliar", (auxiliar = cuenta.getAuxiliar())==null? "": auxiliar.getNombre());
		}
		mSalida.put("cuentas", lCuenta);
		return mSalida;
	}

	@Override
	public List< String > getAvailableCommands() {
		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}
}
