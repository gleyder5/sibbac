package sibbac.business.contabilidad.rest;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.contabilidad.database.bo.CierreContableBo;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceCierreContable implements SIBBACServiceBean {
	private static final Logger LOG = LoggerFactory.getLogger( SIBBACServiceCierreContable.class );
	private static final List< String >	commands = new ArrayList< String >();
	private static String sCommands	= "";

	@Autowired
	private CierreContableBo cierreBo;

	static {
		commands.add( Constantes.CMD_UPDATE );
		for ( String aC : commands ) {
			sCommands += aC + ", ";
		}
		sCommands = sCommands.substring( 0, sCommands.length() - 2 );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		String error;
		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
		LOG.trace( "Command: " + command );
		try {
			switch ( command ) {
				case Constantes.CMD_UPDATE:
					if ((error = update(webRequest.getFilters()))!=null) {
						webResponse.setError(error);
					}
					break;
				default:
					webResponse.setError( "An action must be set. Valid actions are: " + sCommands );
			}
		} catch ( Exception e ) {
			webResponse.setError( e.getMessage() );
		}
		return webResponse;
	}

	private String update( Map< String, String > filters ) throws Exception {
		LOG.trace( "Modificando cierre contable" );
		Integer dia = cierreBo.update(filters.get( "fechaCierre"));
		if (dia!=null) {
			return "Este mes ya está cerrado con el día: " + dia;
		}
		return null;
	}

	@Override
	public List< String > getAvailableCommands() {
		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}
}
