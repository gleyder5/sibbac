package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fallidos.database.dao.FixmlFallidoDao;
import sibbac.business.fallidos.database.model.FixmlFallido;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.model.Tmct0estado;

/**
 * @author XI316153
 */
@Service
public class CobrosClearingFallidos {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosClearingFallidos.class);

  /** Formato de las fechas que se leen del fichero. Ej: 2015-12-24 */
  private static final DateFormat FILE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

  private static final String TIPO_OPERACION_COU = "COU";
  private static final String TIPO_OPERACION_CAU = "CAU";
  private static final String TIPO_OPERACION_COL = "COL";

  private static final String CONCEPTO_ALTACOLATERAL = "Alta colateral";
  private static final String CONCEPTO_BAJACOLATERAL = "Baja colateral";
  private static final String CONCEPTO_AJUSTECOLATERAL = "Ajuste colateral";
  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_CLEARING";
  private static final String DESCUACRE_REF_NOENCONTRADA = "Referencia no encontrada";
  private static final String DESCUACRE_TIPO_OP_NOVALIDA = "La referencia no es de un cobro de fallidos";
  private static final String DESCUACRE_COMENTARIO_DIFMAYOR = "Diferencia entre lo cobrado y lo esperado mayor que margen de tolerancia";

  private static final Integer CUENTACONTABLE_1025401 = 1025401;
  private static final Integer CUENTACONTABLE_1021101 = 1021101;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  @Autowired
  private FixmlFallidoDao fixmlFallidoDao;

  /** Margen de diferencia aceptado entre el importe Norma43 y el de los movimientos fallidos. */
  @Value("${norma43.margen.descuadres.fallidos}")
  private BigDecimal margenDescuadres;

  /** Mapa de cuentas contables. */
  private Map<Integer, CuentaContable> cuentaContableMap;

  /** Lista de descuadres a generar. */
  private List<Norma43DescuadresDTO> norma43DescuadresDTOList;

  /** Mapa de auxiliares contables. */
  private Map<Long, Auxiliar> auxiliaresContablesMap;

  /** Mapa de fechas de contabilización. */
  private Map<String, Date> fechasContabilizacionMap;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera los apuntes contables de cobro de operaciones de clearing de los movimientos norma43 especificados.
   * 
   * @param norma43MovimientoList Lista de movimientos norma43 a procesar.
   * @param mFichero Mapa de auxiliares contables por fichero norma43 a procesar.
   * @param estadosMap Mapa de estados contables.
   * @param flagNorma43ProcesadoTrue Indica que un movimiento norma43 está procesado.
   * @param cienBigDecimal 100 en BigDecimal.
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void processMovimientosFallidos(List<Norma43MovimientoDTO> norma43MovimientoList,
                                         Map<Long, Long> mFichero,
                                         Map<Long, BigDecimal> tolerancePorFichero,
                                         HashMap<String, Tmct0estado> estadosMap,
                                         Integer flagNorma43ProcesadoTrue,
                                         BigDecimal cienBigDecimal) throws Exception {
    LOG.debug("[processMovimientosFallidos] Inicio ...");

    init();

    Calendar hoy = Calendar.getInstance();
    Auxiliar auxiliar;
    Date fechaApunte;
    String referencia = null;
    BigDecimal importeNorma43;
    BigDecimal diferencia;

    for (Norma43MovimientoDTO norma43MovimientoDto : norma43MovimientoList) {
      try {
        LOG.debug("[processMovimientosFallidos] Procesando movimiento: {}", norma43MovimientoDto.toString());

        if ((auxiliar = auxiliaresContablesMap.get(norma43MovimientoDto.getIdFichero())) == null) {
          auxiliaresContablesMap.put(norma43MovimientoDto.getIdFichero(),
                                     auxiliar = auxiliarDao.findOne(mFichero.get(norma43MovimientoDto.getIdFichero())));
        }

        if ((fechaApunte = fechasContabilizacionMap.get(norma43MovimientoDto.getTradedate())) == null) {
          fechasContabilizacionMap.put(norma43MovimientoDto.getTradedate(),
                                       fechaApunte = apunteContableBo.getFechaContabilizacion(hoy,
                                                                                              FILE_DATE_FORMAT.parse(norma43MovimientoDto.getTradedate())));
        } // if

        referencia = norma43MovimientoDto.getDescReferencia().trim();

        FixmlFallido fallidoColateral = fixmlFallidoDao.findFixmlForMn43(referencia);

        if (fallidoColateral != null) {
          importeNorma43 = norma43MovimientoDto.getImporte().divide(cienBigDecimal, 6, RoundingMode.HALF_UP);
          diferencia = fallidoColateral.getImporteEfectivoInstruccion().subtract(importeNorma43).abs();
          if (diferencia.compareTo(margenDescuadres) > 0) {
            norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, fallidoColateral.getImporteEfectivoInstruccion(),
                                                                          null, null, null, null, null, null, null, DESCUADRE_TIPO_IMPORTE,
                                                                          diferencia, DESCUACRE_COMENTARIO_DIFMAYOR));
          } else {
            if (referencia.startsWith(TIPO_OPERACION_COU)) {
              generarApuntesAltaColateral(norma43MovimientoDto, fallidoColateral, fechaApunte, auxiliar, importeNorma43);
            } else if (referencia.startsWith(TIPO_OPERACION_CAU)) {
              generarApuntesBajaColateral(norma43MovimientoDto, fallidoColateral, fechaApunte, auxiliar, importeNorma43);
            } else if (referencia.startsWith(TIPO_OPERACION_COL)) {
              generarApuntesAjusteColateral(norma43MovimientoDto, fallidoColateral, fechaApunte, auxiliar, importeNorma43);
            } else {
              norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                            DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_TIPO_OP_NOVALIDA));
            } // else
          } // else
        } else {
          norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                        DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_REF_NOENCONTRADA));
        } // else
      } catch (Exception e) {
        LOG.warn("[processMovimientosFallidos] Incidencia generando los apuntes de cobro clearing de fallidos ... ", e);
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                      DESCUADRE_TIPO_IMPORTE, null, e.getCause().getMessage()));
        LOG.info("[processMovimientosFallidos] Se continua con el siguiente movimiento ...");
      } finally {
        // TODO
      } // finally
    } // for (Norma43MovimientoDTO norma43MovimientoDto : norma43MovimientoList)

    for (Norma43DescuadresDTO norma43DescuadresDTO : norma43DescuadresDTOList) {
      norma43DescuadresDao.save(norma43DescuadresDTO.getDescuadreNorma43());
    } // for

    // Se actualiza el estado del movimiento a procesado
    for (Norma43MovimientoDTO norma43Movimiento : norma43MovimientoList) {
      norma43MovimientoDao.update(norma43Movimiento.getId(), flagNorma43ProcesadoTrue);
    } // for

    LOG.debug("[processMovimientosFallidos] Fin ...");
  } // processMovimientosFallidos

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Inicializa los objetos que se utilzan durante el proceso.
   */
  private void init() {
    cuentaContableMap = new HashMap<Integer, CuentaContable>();
    norma43DescuadresDTOList = new ArrayList<Norma43DescuadresDTO>();
    auxiliaresContablesMap = new HashMap<Long, Auxiliar>();
    fechasContabilizacionMap = new HashMap<String, Date>();
  } // init

  /**
   * Busca el objeto <code>CuentaContable</code> cuyo número de cuenta se ha especificado.
   *
   * @param cuenta Número de cuenta a buscar.
   * @return <code>CuentaContable - </code>Cuenta contable obtenida.
   */
  private CuentaContable findCuentaContable(Integer cuenta) {
    CuentaContable salida;
    if ((salida = cuentaContableMap.get(cuenta)) == null) {
      cuentaContableMap.put(cuenta, salida = cuentaContableDao.findByCuenta(cuenta));
    } // if
    return salida;
  } // findCuentaContable

  /**
   * Genera los apuntes contables de alta de colateral.
   * 
   */
  private void generarApuntesAltaColateral(Norma43MovimientoDTO norma43MovimientoDto,
                                           FixmlFallido fallidoColateral,
                                           Date fechaApunte,
                                           Auxiliar auxiliar,
                                           BigDecimal importeNorma43) {
    LOG.debug("[generarApuntesAltaColateral] Inicio ...");
    ApunteContable apunteAltaColateral;

    apunteAltaColateral = new ApunteContable();
    apunteAltaColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1025401));
    apunteAltaColateral.setDescripcion(norma43MovimientoDto.getTradedate() + "/" + fallidoColateral.getIsin());
    apunteAltaColateral.setConcepto(CONCEPTO_ALTACOLATERAL);
    apunteAltaColateral.setImporte(importeNorma43);
    apunteAltaColateral.setFecha(fechaApunte);
    apunteAltaColateral.setApunte(TIPO_CUENTA.DEBE.getId());
    apunteAltaColateral.setContabilizado(Boolean.FALSE);
    apunteAltaColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteAltaColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
    apunteAltaColateral.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
    apunteContableBo.save(apunteAltaColateral);

    apunteAltaColateral = new ApunteContable();
    apunteAltaColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
    apunteAltaColateral.setDescripcion(norma43MovimientoDto.getDescReferencia());
    apunteAltaColateral.setConcepto(CONCEPTO_ALTACOLATERAL);
    apunteAltaColateral.setImporte(importeNorma43.negate());
    apunteAltaColateral.setFecha(fechaApunte);
    apunteAltaColateral.setApunte(TIPO_CUENTA.HABER.getId());
    apunteAltaColateral.setContabilizado(Boolean.FALSE);
    apunteAltaColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteAltaColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
    apunteAltaColateral.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
    apunteAltaColateral.setAuxiliar(auxiliar);
    apunteContableBo.save(apunteAltaColateral);

    LOG.debug("[generarApuntesAltaColateral] Fin ...");
  } // generarApuntesAltaColateral

  /**
   * Genera los apuntes contables de baja de colateral.
   * 
   */
  private void generarApuntesBajaColateral(Norma43MovimientoDTO norma43MovimientoDto,
                                           FixmlFallido fallidoColateral,
                                           Date fechaApunte,
                                           Auxiliar auxiliar,
                                           BigDecimal importeNorma43) {
    LOG.debug("[generarApuntesBajaColateral] Inicio ...");
    ApunteContable apunteBajaColateral;

    apunteBajaColateral = new ApunteContable();
    apunteBajaColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
    apunteBajaColateral.setDescripcion(norma43MovimientoDto.getDescReferencia());
    apunteBajaColateral.setConcepto(CONCEPTO_BAJACOLATERAL);
    apunteBajaColateral.setImporte(importeNorma43);
    apunteBajaColateral.setFecha(fechaApunte);
    apunteBajaColateral.setApunte(TIPO_CUENTA.DEBE.getId());
    apunteBajaColateral.setContabilizado(Boolean.FALSE);
    apunteBajaColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteBajaColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
    apunteBajaColateral.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
    apunteBajaColateral.setAuxiliar(auxiliar);
    apunteContableBo.save(apunteBajaColateral);

    apunteBajaColateral = new ApunteContable();
    apunteBajaColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1025401));
    apunteBajaColateral.setDescripcion(norma43MovimientoDto.getTradedate() + "/" + fallidoColateral.getIsin());
    apunteBajaColateral.setConcepto(CONCEPTO_BAJACOLATERAL);
    apunteBajaColateral.setImporte(importeNorma43.negate());
    apunteBajaColateral.setFecha(fechaApunte);
    apunteBajaColateral.setApunte(TIPO_CUENTA.HABER.getId());
    apunteBajaColateral.setContabilizado(Boolean.FALSE);
    apunteBajaColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteBajaColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
    apunteBajaColateral.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
    apunteContableBo.save(apunteBajaColateral);

    LOG.debug("[generarApuntesBajaColateral] Fin ...");
  } // generarApuntesBajaColateral

  /**
   * Genera los apuntes contables de ajuste de colateral.
   * 
   * @param importeMovimientoNorma43 Importe recibido en el fichero Norma43.
   * @param importeMovimientoFallido Importe resultante del calculo del ajuste del colateral de los movimientos fallidos.
   * @param movimientoFallido Movimiento fallido.
   */
  private void generarApuntesAjusteColateral(Norma43MovimientoDTO norma43MovimientoDto,
                                             FixmlFallido fallidoColateral,
                                             Date fechaApunte,
                                             Auxiliar auxiliar,
                                             BigDecimal importeNorma43) {
    LOG.debug("[generarApuntesAjusteColateral] Inicio ...");
    ApunteContable apunteAjusteColateral;

    if (CobrosClearing.ADEUDO.compareTo(norma43MovimientoDto.getDebe()) == 0) {
      // Nos cargan más colateral
      apunteAjusteColateral = new ApunteContable();
      apunteAjusteColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
      apunteAjusteColateral.setDescripcion(norma43MovimientoDto.getDescReferencia());
      apunteAjusteColateral.setConcepto(CONCEPTO_AJUSTECOLATERAL);
      apunteAjusteColateral.setImporte(importeNorma43.negate());
      apunteAjusteColateral.setFecha(fechaApunte);
      apunteAjusteColateral.setApunte(TIPO_CUENTA.HABER.getId());
      apunteAjusteColateral.setContabilizado(Boolean.FALSE);
      apunteAjusteColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteAjusteColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
      apunteAjusteColateral.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
      apunteAjusteColateral.setAuxiliar(auxiliar);
      apunteContableBo.save(apunteAjusteColateral);

      apunteAjusteColateral = new ApunteContable();
      apunteAjusteColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1025401));
      apunteAjusteColateral.setDescripcion(norma43MovimientoDto.getTradedate() + "/" + fallidoColateral.getIsin());
      apunteAjusteColateral.setConcepto(CONCEPTO_AJUSTECOLATERAL);
      apunteAjusteColateral.setImporte(importeNorma43);
      apunteAjusteColateral.setFecha(fechaApunte);
      apunteAjusteColateral.setApunte(TIPO_CUENTA.DEBE.getId());
      apunteAjusteColateral.setContabilizado(Boolean.FALSE);
      apunteAjusteColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteAjusteColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
      apunteAjusteColateral.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
      apunteContableBo.save(apunteAjusteColateral);
    } else if (CobrosClearing.ABONO.compareTo(norma43MovimientoDto.getDebe()) == 0) {
      // Nos devuelven colateral
      apunteAjusteColateral = new ApunteContable();
      apunteAjusteColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
      apunteAjusteColateral.setDescripcion(norma43MovimientoDto.getDescReferencia());
      apunteAjusteColateral.setConcepto(CONCEPTO_AJUSTECOLATERAL);
      apunteAjusteColateral.setImporte(importeNorma43);
      apunteAjusteColateral.setFecha(fechaApunte);
      apunteAjusteColateral.setApunte(TIPO_CUENTA.DEBE.getId());
      apunteAjusteColateral.setContabilizado(Boolean.FALSE);
      apunteAjusteColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteAjusteColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
      apunteAjusteColateral.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
      apunteAjusteColateral.setAuxiliar(auxiliar);
      apunteContableBo.save(apunteAjusteColateral);

      apunteAjusteColateral = new ApunteContable();
      apunteAjusteColateral.setCuentaContable(findCuentaContable(CUENTACONTABLE_1025401));
      apunteAjusteColateral.setDescripcion(norma43MovimientoDto.getTradedate() + "/" + fallidoColateral.getIsin());
      apunteAjusteColateral.setConcepto(CONCEPTO_AJUSTECOLATERAL);
      apunteAjusteColateral.setImporte(importeNorma43.negate());
      apunteAjusteColateral.setFecha(fechaApunte);
      apunteAjusteColateral.setApunte(TIPO_CUENTA.HABER.getId());
      apunteAjusteColateral.setContabilizado(Boolean.FALSE);
      apunteAjusteColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteAjusteColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
      apunteAjusteColateral.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
      apunteContableBo.save(apunteAjusteColateral);
    } // else if

    LOG.debug("[generarApuntesAjusteColateral] Fin ...");
  } // generarApuntesAjusteColateral

} // CobrosClearingCliente
