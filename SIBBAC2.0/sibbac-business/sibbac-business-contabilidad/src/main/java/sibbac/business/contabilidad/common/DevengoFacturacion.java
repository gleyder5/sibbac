package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.SIBBACBusinessException;

/**
 * The Class DevengoCliente.
 */
@Component
public class DevengoFacturacion {
  protected static final Logger LOG = LoggerFactory.getLogger(DevengoFacturacion.class);
  private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");

  @Autowired
  private CuentaContableDao cuentaDao;
  @Autowired
  private Tmct0alcDaoImp alcDao;
  @Autowired
  private DevengoComision devengoComision;
  @Autowired
  private Devengo devengo;
  @Autowired
  private AuxiliarDao auxiliarDao;
  @Autowired
  private ApunteContableBo apunteBo;
  private Map<Date, Date> mFechasContabilizacion = new HashMap<Date, Date>();
  private Map<String, Auxiliar> mAuxiliar = new HashMap<String, Auxiliar>();

  @Transactional
  public void save(Map<String, Object> params) throws SIBBACBusinessException {
    ApunteContable apunteD;
    BigDecimal comision;
    String cdalias;
    String nbtitliq;
    Map<String, Object> params2 = new HashMap<String, Object>();
    String sFeejeliq = (String) params.get("sFeejeliq");
    params2.put("sFeejeliq", sFeejeliq);
    params.put("contabilizado", false);
    params2.put("contabilizado", false);
    params.put("operativa", "N");
    params2.put("operativa", "N");
    params2.put("fechFactura", params.get("fechFactura"));
    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.DEVENGO_FACTURACION);
    params.put("cuenta", cuentaDao.findByCuenta(4021101));
    params.put("sApunte", "H");
    params.put("concepto", "Devengo Comision");
    params.put("descripcion", "Comision (" + sFeejeliq + ")");
    params.put("TIPO_APUNTE", TIPO_APUNTE.CORRETAJE.getId());
    ApunteContable apunteH = new ApunteContable(params);
    params.put("cuenta", cuentaDao.findByCuenta(1122107));
    params.put("sApunte", "D");
    params.put("concepto", "Devengo Comision");
    params.put("TIPO_APUNTE", TIPO_APUNTE.CORRETAJE_Y_CANON.getId());
    for (Entry<String, List<Tmct0alc>> entry : ((Map<String, List<Tmct0alc>>) params.get("entry.value")).entrySet()) {
      nbtitliq = entry.getValue().get(0).getNbtitliq();
      params.put("cdalias", cdalias = entry.getKey());
      params.put("auxiliar", mAuxiliar.get(cdalias));
      params.put("descripcion", "Comision (" + sFeejeliq + ")(" + cdalias + ")(" + nbtitliq + ")");
      apunteD = new ApunteContable(params);
      for (Tmct0alc alc : entry.getValue()) {
        LOG.debug("Procesando el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
//        alcDao.updateCdestadocont(CONTABILIDAD.CONTABILIZANDOSE.getId(), alc.getNbooking(), alc.getNuorden(), alc.getNucnfliq(), alc.getNucnfclt());
        params.put("alc", alc);
        params2.put("alc", alc);
        params.put("comision", devengo.getComision(alc));
        if ((comision = (BigDecimal) params.get("comision")).signum() != 0) {
          apunteH.add(alc, comision.negate());
          apunteD.add(alc, comision);
        }
        devengoComision.save(params2);
        alcDao.escalaAlcDesglose(alc, CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId(), TIPO_ESTADO.CORRETAJE_O_CANON);
        LOG.debug("Procesado el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
      }
      devengo.putApunte(apunteD);
    }
    devengo.putApunte(apunteH);
  }

  private Date getFechaContabilizacion(Calendar hoy, Date fecha) {
    Date fechContabilizacion = mFechasContabilizacion.get(fecha);
    if (fechContabilizacion == null) {
      mFechasContabilizacion.put(fecha, fechContabilizacion = apunteBo.getFechaContabilizacion(hoy, fecha));
    }
    return fechContabilizacion;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void put(Map<String, Object> params) throws Exception {
    Date feejeliq;
    List<Tmct0alc> lBean;
    String cdalias = "";
    Calendar hoy = (Calendar) params.get("hoy");
    Map<Date, Map<String, List<Tmct0alc>>> mCliente = new TreeMap<Date, Map<String, List<Tmct0alc>>>();
    Map<String, List<Tmct0alc>> mDia;
    params.put("contabilizado", false);
    params.put("operativa", "N");
    for (Tmct0alc alc : (List<Tmct0alc>) params.get("list")) {
      LOG.debug("Procesando el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());

      /*
       * En el caso que se haya cerrado el mes, los apuntes contables deben entrar con fecha del mes en curso, pero en la descripción del apunte
       * contable conservar la fecha Trade Date
       */
      feejeliq = alc.getFeejeliq();
      params.put("nuorden", alc.getNuorden());
      params.put("nbooking", alc.getNbooking());
      params.put("nucnfclt", alc.getNucnfclt());
      params.put("cdalias", cdalias = alc.getTmct0alo().getCdalias());
      params.put("alc", alc);
      if (!mAuxiliar.containsKey(cdalias)) {
        mAuxiliar.put(cdalias, alc.getAuxiliar() == null ? null : auxiliarDao.findOne(alc.getAuxiliar()));
      }
      if ((mDia = mCliente.get(feejeliq)) == null) {
        mCliente.put(feejeliq, mDia = new HashMap<String, List<Tmct0alc>>());
      }
      if ((lBean = mDia.get(cdalias)) == null) {
        mDia.put(cdalias, lBean = new ArrayList<Tmct0alc>());
      }
      lBean.add(alc);
      LOG.debug("Procesado el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
    }
    for (Entry<Date, Map<String, List<Tmct0alc>>> entry : mCliente.entrySet()) {
      params.put("fechFactura", apunteBo.getFechaContabilizacion(hoy, entry.getKey()));
      params.put("feejeliq", entry.getKey());
      params.put("sFeejeliq", dF1.format(entry.getKey()));
      params.put("entry.value", entry.getValue());
      save(params);
    }
  }
}
