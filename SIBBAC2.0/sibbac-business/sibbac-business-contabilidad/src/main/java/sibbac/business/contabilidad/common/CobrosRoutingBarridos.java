package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaAuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaAuxiliar;
import sibbac.business.contabilidad.database.model.CuentaBancaria;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.utils.FormatDataUtils;

/**
 * Realiza los apuntes contables de cobros de cánones y corretajes de barridos de routing.
 * 
 * @author XI316153
 */
@Service
public class CobrosRoutingBarridos {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosRoutingBarridos.class);

  private static final Integer CUENTACONTABLE_1021101 = 1021101;
  private static final Integer CUENTACONTABLE_1122106 = 1122106;
  private static final Integer CUENTACONTABLE_4021102 = 4021102;

  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_ROUTING";
  private static final String DESCUACRE_COMENTARIO_DIFMAYOR = "Diferencia entre lo devengado y lo cobrado mayor que margen de tolerancia";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private CuentaAuxiliarDao cuentaAuxiliarDao;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  /** Mapa con la relación entre una cuenta bancaria y su margen de tolerancia. */
  private Map<String, BigDecimal> tolerancePorCuenta;

  /** Mapa con la relación entre una cuenta bancaria y el auxiliar contable a usar en los appuntes a la cuenta 1021101. */
  private HashMap<String, CuentaAuxiliar> auxiliarPorCuenta;

  // /** Mapa con la relación entre un alias y su auxiliar contable a usar en los apuntes contables a la cuenta 1122106.
  // */
  // private HashMap<String, Auxiliar> auxiliarPorAlias;

  // /** Auxiliar contable por defecto para los apuntes contables a la cuenta 1122106 cuando el alias de la operación no
  // tiene definido ninguno. */
  // private Auxiliar auxiliarRoutingPorDefecto = null;

  /** Mapa de cuentas contables. */
  private Map<Integer, CuentaContable> cuentaContableMap;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * @param barrido
   * @param lEstadosAsig
   * @param estadosMap
   * @param tmct0cfgAuxiliarPorDefectoRouting
   * @param tiposImporteList
   * @param margenDescuadres
   * @throws Exception
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void generateCobrosRoutingBarrido(Map.Entry<String, Map<String, Object>> datosBarrido,
                                           List<Integer> lEstadosAsig,
                                           HashMap<String, Tmct0estado> estadosMap,
                                           String tmct0cfgAuxiliarPorDefectoRouting,
                                           List<Integer> tiposImporteList) throws Exception {
    LOG.debug("[generateRoutingCobrosBarrido] Inicio ...");

    init(tmct0cfgAuxiliarPorDefectoRouting, tiposImporteList);

    String isin = datosBarrido.getKey();
    Map<String, Object> barrido = datosBarrido.getValue();

    BigDecimal csvCanon = new BigDecimal(barrido.get(CobrosRouting.CSV_CANON).toString());
    BigDecimal csvCorretaje = new BigDecimal(barrido.get(CobrosRouting.CSV_CORRETAJE).toString());
    // Date csvFecha = FormatDataUtils.convertStringToDate(String.valueOf(barrido.get(CobrosRouting.CSV_FECHA)),
    // FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);

    // List<String> csvList = (ArrayList<String>) barrido.get(CobrosRouting.CSV_NUREFORD_LIST);
    BigDecimal alcCanon = new BigDecimal(barrido.get(CobrosRouting.ALC_CANON).toString());
    BigDecimal alcCorretaje = new BigDecimal(barrido.get(CobrosRouting.ALC_CORRETAJE).toString());
    Date alcFecha = FormatDataUtils.convertStringToDate(String.valueOf(barrido.get(CobrosRouting.ALC_FECHA)),
                                                        FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);

    List<Norma43Movimiento> mN43List = (ArrayList<Norma43Movimiento>) barrido.get(CobrosRouting.NORMA43_LISTAMOV);
    // BigDecimal norma43Canon = new BigDecimal(barrido.get(CobrosRouting.NORMA43_CANON).toString());
    // BigDecimal norma43Corretaje = new BigDecimal(barrido.get(CobrosRouting.NORMA43_CORRETAJE).toString());

    String ccCanon = String.valueOf(barrido.get(CobrosRouting.CC_CANON_KEY));
    String ccCorretaje = String.valueOf(barrido.get(CobrosRouting.CC_CORRETAJE_KEY));

    BigDecimal diferenciaCanon = alcCanon.subtract(csvCanon);
    BigDecimal diferenciaCorretaje = alcCorretaje.subtract(csvCorretaje);

    Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), alcFecha);

    String alias = String.valueOf(barrido.get(CobrosRouting.ALC_ALIAS));
    // Auxiliar auxiliar = getAuxiliar(alias);
    Auxiliar auxiliar = auxiliarDao.findByCdalias(alias);

    if (diferenciaCanon.compareTo(tolerancePorCuenta.get(ccCanon)) > 0) {
      for (Norma43Movimiento norma43Movimiento : mN43List) {
        generarDescuadre(norma43Movimiento, ccCanon, null, alcCanon, TIPO_IMPORTE.Canon.getDescripcion(),
                         DESCUACRE_COMENTARIO_DIFMAYOR);
      } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
    } else {
      // Se hace los apuntes de canones
      // generateApuntesCanon(csvCanon, alcCanon, diferenciaCanon, fechaApunte, auxiliar,
      // auxiliarPorCuenta.get(ccCanon).getAuxiliar(),
      // "Cobro Canon Barrido " + alias, String.valueOf(barrido.get(CobrosRouting.ALC_FECHA)));
      generateApuntesCanon(csvCanon, alcCanon, diferenciaCanon, fechaApunte, auxiliar, auxiliarPorCuenta.get(ccCanon)
                                                                                                        .getAuxiliar(),
                           "Cobro Canon Barrido " + isin, String.valueOf(barrido.get(CobrosRouting.ALC_FECHA)));
    } // else

    if (diferenciaCorretaje.compareTo(tolerancePorCuenta.get(ccCorretaje)) > 0) {
      for (Norma43Movimiento norma43Movimiento : mN43List) {
        generarDescuadre(norma43Movimiento, ccCorretaje, null, alcCorretaje, TIPO_IMPORTE.Corretaje.getDescripcion(),
                         DESCUACRE_COMENTARIO_DIFMAYOR);
      } // for (Norma43Movimiento movimientoNorma43 : valueEntry.getValue())
    } else {
      // Se hacen los apuntes de corretajes
      // generateApuntesCorretaje(csvCorretaje, alcCorretaje, diferenciaCorretaje, fechaApunte, auxiliar,
      // auxiliarPorCuenta.get(ccCorretaje)
      // .getAuxiliar(),
      // "Cobro Corretaje Barrido " + alias, String.valueOf(barrido.get(CobrosRouting.ALC_FECHA)));
      generateApuntesCorretaje(csvCorretaje, alcCorretaje, diferenciaCorretaje, fechaApunte, auxiliar,
                               auxiliarPorCuenta.get(ccCorretaje).getAuxiliar(), "Cobro Corretaje Barrido " + isin,
                               String.valueOf(barrido.get(CobrosRouting.ALC_FECHA)));
    } // else

    LOG.debug("[generateRoutingCobrosBarrido] Actualizando movimientos norma43 ...");
    for (Norma43Movimiento mN43 : mN43List) {
      norma43MovimientoDao.update(mN43.getId(), 1);

      // Para que a la vuelta del método cuando se comprueba si se han procesado correctamente los movimientos para
      // actualizar el flag de procesado
      // del fichero estos estén modificados, porque al ser una query de actualiazación nativa el objeto de hibernate no
      // se modifica
      mN43.setProcesado(1);
    }
    LOG.debug("[generateRoutingCobrosBarrido] Movimientos norma43 actualizados ...");

    LOG.debug("[generateRoutingCobrosBarrido] Actualizando estado contable ALC-ALO-BOK ...");
    tmct0alcDaoImp.updateEstadoCont(String.valueOf(barrido.get(CobrosRouting.CSV_NUREFORD_LIST)),
                                    estadosMap.get("estadoContCobrado").getIdestado(),
                                    estadosMap.get("estadoContEnCurso").getIdestado());
    LOG.debug("[generateRoutingCobrosBarrido] Estado contable ALC-ALO-BOK actualizado ...");

    LOG.debug("[generateRoutingCobrosBarrido] Actualizando esdadoContableAlc de canones ...");
    alcEstadoContDaoImp.mergeCdestadoCont(TIPO_ESTADO.CANON.getId(), estadosMap.get("estadoContDevengadaPdteCobro")
                                                                               .getIdestado(),
                                          estadosMap.get("estadoContCobrado").getIdestado(),
                                          String.valueOf(barrido.get(CobrosRouting.CSV_NUREFORD_LIST)));
    LOG.debug("[generateRoutingCobrosBarrido] EsdadoContableAlc de canones actualizado ...");

    LOG.debug("[generateRoutingCobrosBarrido] Actualizando esdadoContableAlc de corretajes ...");
    alcEstadoContDaoImp.mergeCdestadoCont(TIPO_ESTADO.CORRETAJE.getId(), estadosMap.get("estadoContDevengadaPdteCobro")
                                                                                   .getIdestado(),
                                          estadosMap.get("estadoContCobrado").getIdestado(),
                                          String.valueOf(barrido.get(CobrosRouting.CSV_NUREFORD_LIST)));
    LOG.debug("[generateRoutingCobrosBarrido] EsdadoContableAlc de corretajes actualizado ...");

    LOG.debug("[generateRoutingCobrosBarrido] Fin ...");
  } // generateRoutingCobrosBarrido

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Inicializa los objetos que se utilzan durante el proceso.
   */
  private void init(String tmct0cfgAuxiliarPorDefectoRouting, List<Integer> tiposImporteList) {
    cuentaContableMap = new HashMap<Integer, CuentaContable>();

    // norma43DescuadresDTOList = new ArrayList<Norma43DescuadresDTO>();

    // Auxiliar por defecto para routing
    // auxiliarRoutingPorDefecto = auxiliarDao.findByNombre(tmct0cfgAuxiliarPorDefectoRouting);

    auxiliarPorCuenta = new HashMap<String, CuentaAuxiliar>();
    tolerancePorCuenta = new HashMap<String, BigDecimal>();
    for (CuentaAuxiliar ctaAuxiliar : cuentaAuxiliarDao.find(TIPOLOGIA.Routing.getTipo(), tiposImporteList)) {
      auxiliarPorCuenta.put(ctaAuxiliar.getCuenta().getIban(), ctaAuxiliar);
      tolerancePorCuenta.put(ctaAuxiliar.getCuenta().getIban(), ctaAuxiliar.getCuenta().getTolerance());
    } // for
  } // init

  /**
   * Busca el objeto <code>CuentaContable</code> cuyo número de cuenta se ha especificado.
   *
   * @param cuenta Número de cuenta a buscar.
   * @return <code>CuentaContable - </code>Cuenta contable obtenida.
   */
  private CuentaContable findCuentaContable(Integer cuenta) {
    CuentaContable salida;
    if ((salida = cuentaContableMap.get(cuenta)) == null) {
      cuentaContableMap.put(cuenta, salida = cuentaContableDao.findByCuenta(cuenta));
    } // if
    return salida;
  } // findCuentaContable

  // /**
  // * Devuelve el auxiliar contable del alias especificado.
  // *
  // * @param cdalias Código del alias.
  // * @return <code>Auxiliar - </code>Auxiliar contable del alias especificado; el auxiliar por defecto para routing en
  // el caso de que el alias no
  // * tenga uno especificado.
  // */
  // private Auxiliar getAuxiliar(String cdalias) {
  // Auxiliar auxiliar;
  // if ((auxiliar = auxiliarPorAlias.get(cdalias)) == null) {
  // if ((auxiliar = auxiliarDao.findByCdalias(cdalias)) == null) {
  // auxiliarPorAlias.put(cdalias, auxiliarRoutingPorDefecto);
  // return auxiliarRoutingPorDefecto;
  // } else {
  // auxiliarPorAlias.put(cdalias, auxiliar);
  // } // else
  // } // if
  // return auxiliar;
  // } // getAuxiliar

  /**
   * Genera un descuadre con los datos especificados y lo almacena en base de datos.
   * 
   * @param norma43Movimiento Movimiento Norma43.
   * @param tmct0alc Registro de la tabla Tmct0alc que genera el descuadre. Puede ser null.
   * @param importeSibbac Importe en Sibbac.
   * @param tipoImporte Tipo de operación que ha generado el descuadre.
   */
  private void generarDescuadre(Norma43Movimiento norma43Movimiento,
                                String cuenta,
                                Tmct0alc tmct0alc,
                                BigDecimal importeSibbac,
                                String tipoImporte,
                                String comentario) {
    LOG.debug("[generarDescuadre] Inicio ...");
    Norma43DescuadresDTO descuadreNorma43DTO = new Norma43DescuadresDTO();

    CuentaBancaria cc = new CuentaBancaria(cuenta);

    descuadreNorma43DTO.setCuenta(String.format("%04d", cc.getEntidad())
                                        .concat(String.format("%04d", cc.getSucursal()))
                                        .concat(String.format("%010d", cc.getCuenta())));
    descuadreNorma43DTO.setRefFecha(norma43Movimiento.getReferencia());
    descuadreNorma43DTO.setRefOrden(norma43Movimiento.getDescReferencia() == null ? " "
                                                                                 : norma43Movimiento.getDescReferencia());
    descuadreNorma43DTO.setSentido(norma43Movimiento.getDebe());
    descuadreNorma43DTO.setTradeDate(norma43Movimiento.getTradedate());
    descuadreNorma43DTO.setSettlementDate(norma43Movimiento.getSettlementdate());
    descuadreNorma43DTO.setImporteNorma43(norma43Movimiento.getImporte().divide(new BigDecimal(100)));

    descuadreNorma43DTO.setComentario(comentario.length() > 200 ? comentario.substring(0, 200) : comentario);

    descuadreNorma43DTO.setImporteSibbac(importeSibbac);

    if (tmct0alc != null) {

      descuadreNorma43DTO.setTipoImporte(tipoImporte);
      // TODO Se podria quitar esta consulta y sacar el número del estado?
      descuadreNorma43DTO.setNombreEstadoAsignacionAlc(tmct0estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre());
      descuadreNorma43DTO.setNombreEstadoContabilidadAlc(tmct0alc.getEstadocont().getNombre());
      descuadreNorma43DTO.setNombreEstadoEntrecAlc(tmct0alc.getEstadoentrec().getNombre());
      descuadreNorma43DTO.setNuorden(tmct0alc.getNuorden());
      descuadreNorma43DTO.setNbooking(tmct0alc.getNbooking());
      descuadreNorma43DTO.setNucnfclt(tmct0alc.getNucnfclt());
      descuadreNorma43DTO.setNucnfliq(tmct0alc.getNucnfliq());
    } else {
      descuadreNorma43DTO.setTipoImporte(DESCUADRE_TIPO_IMPORTE);
    }

    norma43DescuadresDao.save(descuadreNorma43DTO.getDescuadreNorma43());

    LOG.debug("[generarDescuadre] Fin ...");
  } // generarDescuadre

  /**
   * Genera los apuntes contables de cobro de canon.
   * 
   * @param importeCobrado Importe cobrado en los csv.
   * @param importeDevengado Sumatorio de los importes devengados de los alc.
   * @param diferencia Diferencia entre lo devengado y lo cobrado.
   * @param fechaApunte Fecha a establecer en el apunte contable.
   * @param auxiliar Auxiliar por alias para el apunte en la cuenta 1122106.
   * @param auxiliarCuentaBanco Auxiliar por cuenta bancaria para el apunte en la cuenta 1021101.
   * @param descripcionCuentaBancos Descripción del apunte de la cuenta 1021101.
   * @param fechaDescripcion Fecha a añadir a la descripción del apunte de la cuenta 1122106.
   */
  private void generateApuntesCanon(BigDecimal importeCobrado,
                                    BigDecimal importeDevengado,
                                    BigDecimal diferencia,
                                    Date fechaApunte,
                                    Auxiliar auxiliar,
                                    Auxiliar auxiliarCuentaBanco,
                                    String descripcionCuentaBancos,
                                    String fechaDescripcion) {
    LOG.debug("[generateApuntesCanon] Inicio ...");

    if (importeCobrado.signum() != 0) {
      ApunteContable apunte_canon_debe = new ApunteContable();
      apunte_canon_debe.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
      apunte_canon_debe.setDescripcion(descripcionCuentaBancos);
      apunte_canon_debe.setConcepto("Cobro canon + Com MCG");
      apunte_canon_debe.setImporte(importeCobrado);
      apunte_canon_debe.setFecha(fechaApunte);
      apunte_canon_debe.setApunte(TIPO_CUENTA.DEBE.getId());
      apunte_canon_debe.setContabilizado(Boolean.FALSE);
      apunte_canon_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_canon_debe.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_canon_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
      apunte_canon_debe.setAuxiliar(auxiliarCuentaBanco);
      apunteContableBo.save(apunte_canon_debe);
    } // if (importeCobrado.signum() != 0)

    if (importeDevengado.signum() != 0) {
      ApunteContable apunte_canon_haber = new ApunteContable();
      apunte_canon_haber.setCuentaContable(findCuentaContable(CUENTACONTABLE_1122106));
      apunte_canon_haber.setDescripcion("Cobro canones (" + fechaDescripcion + ")");
      apunte_canon_haber.setConcepto("Cobro canon + Com MCG");
      apunte_canon_haber.setImporte(importeDevengado.negate());
      apunte_canon_haber.setFecha(fechaApunte);
      apunte_canon_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_canon_haber.setContabilizado(Boolean.FALSE);
      apunte_canon_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_canon_haber.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_canon_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
      apunte_canon_haber.setAuxiliar(auxiliar);
      apunteContableBo.save(apunte_canon_haber);
    } // if (importeDevengado.signum() != 0)

    if (diferencia.signum() != 0) {
      ApunteContable apunte_canon_diff = new ApunteContable();
      apunte_canon_diff.setCuentaContable(findCuentaContable(CUENTACONTABLE_4021102));
      apunte_canon_diff.setDescripcion("Diferencia canones  (" + fechaDescripcion + ")");
      apunte_canon_diff.setConcepto("Diferencia cobro canon + Com MCG");
      apunte_canon_diff.setFecha(fechaApunte);
      apunte_canon_diff.setContabilizado(Boolean.FALSE);
      apunte_canon_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_canon_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_canon_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CANON.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_canon_diff.setImporte(diferencia);
        apunte_canon_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_canon_diff.setImporte(diferencia);
        apunte_canon_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_canon_diff);
    } // if (diferencia.signum() != 0)

    LOG.debug("[generateApuntesCanon] Fin ...");
  } // generateApuntesCanon

  /**
   * Genera los apuntes contables de cobro de corretaje.
   * 
   * @param importeCobrado Importe cobrado en los csv.
   * @param importeDevengado Sumatorio de los importes devengados de los alc.
   * @param diferencia Diferencia entre lo devengado y lo cobrado.
   * @param fechaApunte Fecha a establecer en el apunte contable.
   * @param auxiliar Auxiliar por alias para el apunte en la cuenta 1122106.
   * @param auxiliarCuentaBanco Auxiliar por cuenta bancaria para el apunte en la cuenta 1021101.
   * @param descripcionCuentaBancos Descripción del apunte de la cuenta 1021101.
   * @param fechaDescripcion Fecha a añadir a la descripción del apunte de la cuenta 1122106.
   */
  private void generateApuntesCorretaje(BigDecimal importeCobrado,
                                        BigDecimal importeDevengado,
                                        BigDecimal diferencia,
                                        Date fechaApunte,
                                        Auxiliar auxiliar,
                                        Auxiliar auxiliarCuentaBanco,
                                        String descripcionCuentaBancos,
                                        String fechaDescripcion) {
    LOG.debug("[generateApuntesCorretaje] Inicio ...");
    if (importeCobrado.signum() != 0) {
      ApunteContable apunte_corretaje_debe = new ApunteContable();
      apunte_corretaje_debe.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
      apunte_corretaje_debe.setDescripcion(descripcionCuentaBancos);
      apunte_corretaje_debe.setConcepto("Cobro corretaje");
      apunte_corretaje_debe.setImporte(importeCobrado);
      apunte_corretaje_debe.setFecha(fechaApunte);
      apunte_corretaje_debe.setApunte(TIPO_CUENTA.DEBE.getId());
      apunte_corretaje_debe.setContabilizado(Boolean.FALSE);
      apunte_corretaje_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_debe.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_corretaje_debe.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
      apunte_corretaje_debe.setAuxiliar(auxiliarCuentaBanco);
      apunteContableBo.save(apunte_corretaje_debe);
    } // if (importeCobrado.signum() != 0)

    if (importeDevengado.signum() != 0) {
      ApunteContable apunte_corretaje_haber = new ApunteContable();
      apunte_corretaje_haber.setCuentaContable(findCuentaContable(CUENTACONTABLE_1122106));
      apunte_corretaje_haber.setDescripcion("Cobro corretajes (" + fechaDescripcion + ")");
      apunte_corretaje_haber.setConcepto("Cobro corretaje");
      apunte_corretaje_haber.setImporte(importeDevengado.negate());
      apunte_corretaje_haber.setFecha(fechaApunte);
      apunte_corretaje_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_corretaje_haber.setContabilizado(Boolean.FALSE);
      apunte_corretaje_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_haber.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_corretaje_haber.setTipoMovimiento(TIPO_APUNTE.COBRO_NORMA43.getId());
      apunte_corretaje_haber.setAuxiliar(auxiliar);
      apunteContableBo.save(apunte_corretaje_haber);
    } // if (importeDevengado.signum() != 0)

    if (diferencia.signum() != 0) {
      ApunteContable apunte_corretaje_diff = new ApunteContable();
      apunte_corretaje_diff.setCuentaContable(findCuentaContable(CUENTACONTABLE_4021102));
      apunte_corretaje_diff.setDescripcion("Diferencia corretajes (" + fechaDescripcion + ")");
      apunte_corretaje_diff.setConcepto("Diferencia cobro corretaje");
      apunte_corretaje_diff.setFecha(fechaApunte);
      apunte_corretaje_diff.setContabilizado(Boolean.FALSE);
      apunte_corretaje_diff.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_corretaje_diff.setTipoComprobante(TIPO_COMPROBANTE.COBRO_ROUTING.getTipo());
      apunte_corretaje_diff.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CORRETAJE.getId());

      if (diferencia.compareTo(BigDecimal.ZERO) < 0) { // Apunte de diferencias al haber
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.HABER.getId());
      } else { // Apunte de diferencias al debe
        apunte_corretaje_diff.setImporte(diferencia);
        apunte_corretaje_diff.setApunte(TIPO_CUENTA.DEBE.getId());
      } // else if (diferencia.compareTo(BigDecimal.ZERO) > 0)

      apunteContableBo.save(apunte_corretaje_diff);
    } // if (diferencia.signum() != 0)

    LOG.debug("[generateApuntesCorretaje] Fin ...");
  } // generateApuntesCorretaje

} // CobrosRoutingBarridos
