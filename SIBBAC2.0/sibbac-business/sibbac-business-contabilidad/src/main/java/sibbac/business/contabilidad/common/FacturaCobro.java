package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.dao.CobrosDao;
import sibbac.business.wrappers.database.model.AlcOrdenDetalleFactura;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Cobros;
import sibbac.business.wrappers.database.model.LineaDeCobro;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Component
public class FacturaCobro {
	private static final String CONCEPTO_D = "ACTIVO Bancos";
	private static final String CONCEPTO_H = "ACTIVO Créditos Clientes Fras Pte Cobro";
	private static final String CONCEPTO_PYG = "PYG Dif/Ajustes Negativas Corretajes";

	@Autowired
	private ApunteContableBo apunteBo;

	@Autowired
	private CobrosDao cobroDao;

	@Autowired
	private CuentaContableDao cuentaDao;

	@Autowired
	private ApunteContableAlcDao apunteAlcDao;
	
	@Autowired
	private Tmct0alcBo alcBo;

	@Transactional(propagation = Propagation.REQUIRES_NEW) public void put(
			List<Cobros> lCobro, Calendar hoy) throws Exception {
		if (lCobro.isEmpty()) {
			return;
		}
		List<LineaDeCobro> lLinea;
		Alias alias;
		String cdAlias;
		String nombre;
		ApunteContable apunteD;
		ApunteContable apunteH;
		ApunteContable apuntePYG = null;
		Tmct0alcId alcId;
		Tmct0alc alc;
		BigDecimal importe;
		boolean hayComision;
		String descripcion;
		Integer estadoasig;
		Map<String, Object> params = new HashMap<String, Object>();
		CuentaContable cuentaD = cuentaDao.findByCuenta(1021101);
		CuentaContable cuentaH = cuentaDao.findByCuenta(1032103);
		CuentaContable cuentaPYG = cuentaDao.findByCuenta(4021102);
		params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.COBRO_FACTURA);
		params.put("TIPO_APUNTE", TIPO_APUNTE.COBRO.getId());
		params.put("operativa", "N");
		params.put("contabilizado", false);
		for (Cobros cobro: lCobro) {
			lLinea = cobro.getLineasDeCobro();
			alias = cobro.getAlias();
			nombre = alias.getCdbrocli().trim();
			cdAlias = alias.getCdaliass().trim();
			params.put("fechFactura", apunteBo.getFechaContabilizacion(hoy, cobro.getFecha()));
			descripcion = lLinea.size()==1?
					"Cobro nº " + lLinea.get(0).getFactura().getNumeroFactura() + "/" + cdAlias + "/" + nombre:
					"Cobro múltiples facturas/" + cdAlias + "/" + nombre;
			if (hayComision = cobro.getComisiones().signum()!=0) {
				params.put("descripcion", "DIF " + descripcion);
				params.put("sApunte", "H");
				params.put("concepto", CONCEPTO_PYG);
				params.put("cuenta", cuentaPYG);
				(apuntePYG = new ApunteContable(params)).setImporte(cobro.getComisiones().negate());
				apunteBo.save(apuntePYG);
			}
			params.put("descripcion", descripcion);
			params.put("sApunte", "D");
			params.put("concepto", CONCEPTO_D);
			params.put("cuenta", cuentaD);
			(apunteD = new ApunteContable(params)).setImporte(cobro.getImporte());
			apunteBo.save(apunteD);
			for (LineaDeCobro linea: lLinea) {
				params.put("sApunte", "H");
				params.put("concepto", CONCEPTO_H);
				params.put("cuenta", cuentaH);
				(apunteH = new ApunteContable(params)).setImporte(linea.getImporteAplicado().negate());
				apunteBo.save(apunteH);
				for (AlcOrdenDetalleFactura alcODF: linea.getFactura().getAlcOrdenFacturaDetalles()) {
				    alcId = alcODF.getAlcOrden().createAlcIdFromFields();
				    alc = alcBo.findById(alcId);
					if (alc != null
					        && (estadoasig = alc.getCdestadoasig()) != null
							&& estadoasig >= ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
						apunteAlcDao.save(new ApunteContableAlc(apunteD,alc,importe = alc.getImcomisn()));
						apunteAlcDao.save(new ApunteContableAlc(apunteH,alc,importe));
						if (hayComision) {
							apunteAlcDao.save(new ApunteContableAlc(apuntePYG, alc, importe));
						}
					}
				}
			}
			cobro.setContabilizado(true);
			cobroDao.save(cobro);
		}
	}
}
