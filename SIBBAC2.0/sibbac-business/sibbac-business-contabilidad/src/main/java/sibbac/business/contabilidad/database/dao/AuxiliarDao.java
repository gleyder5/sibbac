package sibbac.business.contabilidad.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;

@Component
public interface AuxiliarDao extends JpaRepository<Auxiliar, Long> {

  public Auxiliar findByNumero(String numero);

  @Query("SELECT A FROM Auxiliar A, Tmct0ali B, Tmct0ord C WHERE A.id=B.auxiliar "
         + "AND B.id.cdaliass=C.cdcuenta and B.fhfinal = 99991231 AND nuorden=:nuorden")
  public Auxiliar findByNuorden(@Param("nuorden") String nuorden);

  @Query("SELECT A FROM Auxiliar A, Tmct0CuentaLiquidacion B WHERE A.id=B.cdCodigo AND B.id=:cta_liq")
  public Auxiliar findByCuentaLiquidacion(@Param("cta_liq") String cta_liq);

  @Query(value = "select id,numero,nombre from tmct0_auxiliar where INSTR(UPPER (trim(numero)||'-'||"
                 + "trim(nombre)),upper(:text))<>0 FETCH FIRST 12 ROWS ONLY", nativeQuery = true)
  List<Object[]> findByText(@Param("text") String text);

  public Auxiliar findByNombre(String nombre);

  public Auxiliar findByNombreAndCuentaContable(String nombre, CuentaContable cuenta);

  @Query("SELECT A FROM Auxiliar A, Tmct0ali B WHERE A.id=B.auxiliar and B.fhfinal = 99991231 and B.id.cdaliass=:cdalias")
  public Auxiliar findByCdalias(@Param("cdalias") String cdalias);
}
