package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43FicheroDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDaoImpl;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE;
import sibbac.business.fase0.database.model.Tmct0estado;

/**
 * Realiza los apuntes contables de las comisiones de BME.
 * 
 * @author XI316153
 */
@Service
public class CobrosComisionesBME {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosComisionesBME.class);

  /** Flag que indica que un registro ha sido procesado. */
  private static final Integer FLAG_NORMA43_PROCESADO_TRUE = 1;

  /** Cien en BigDecimal. */
  private static final BigDecimal CIEN_BIGDECIMAL = new BigDecimal(100);

  /** Indica que un registro norma43 es de adeudo. */
  public static final String ADEUDO = "1";

  /** Indica que un registro norma43 es de abono. */
  public static final String ABONO = "2";

  public static final String LITERAL_COBROS_BME = "B3";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Norma43FicheroDao norma43FicheroDao;

  @Autowired
  private Norma43MovimientoDaoImpl norma43MovimientoDaoImpl;

  @Autowired
  private CobrosComisionesBMESubList cobrosComisionesBMESubList;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera los apuntes contables de cobro de comisiones de BME.
   * 
   * @param transactionSize Número de registros a procesar por cada transacción.
   * @param estadosMap Estados usados durante el proceso.
   * @throws Exception Se ha ocurrido algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void generateApuntesComisionesBME(Integer transactionSize, HashMap<String, Tmct0estado> estadosMap, List<Integer> lEstadosAsig) throws Exception {
    LOG.debug("[generateApuntesComisionesBME] Iniciando generacion de apuntes contables de cobros de comisiones de BME ...");
    final long startTime = System.currentTimeMillis();

    List<Object[]> norma43FicheroList;

    try {
      // 1.- Se buscan los registros de la tabla TMCT0_NORMA43_FICHERO de cobros de comisiones de BME que estén sin procesar
      LOG.debug("[generateApuntesComisionesBME] Buscando registros TMCT0_NORMA43_FICHERO de tipologia BME y tipo de importe ComisionBME sin procesar ...");
      norma43FicheroList = norma43FicheroDao.findByProcesadoAndTipologiaAndTipoImporte(Boolean.FALSE, TIPOLOGIA.BME.getTipo(),
                                                                                       TIPO_IMPORTE.ComisionBME.getTipo());
      if (norma43FicheroList.isEmpty()) {
        LOG.debug("[generateApuntesComisionesBME] No se han encontrado ficheros norma43 para procesar ...");
        return;
      } // if
      LOG.debug("[generateApuntesComisionesBME] Encontrados {} ficheros norma43 para procesar ...", norma43FicheroList.size());

      // 2.- Se procesan los cobros
      // String ids = "";
      Map<Long, Long> mFichero = new HashMap<Long, Long>();
      Map<Long, BigDecimal> tolerancePorFichero = new HashMap<Long, BigDecimal>();
      List<String> idList = new ArrayList<String>();

      for (Object[] objects : norma43FicheroList) {
        // ids += objects[0] + ",";
        mFichero.put((Long) objects[0], (Long) objects[1]);
        tolerancePorFichero.put((Long) objects[0], new BigDecimal(String.valueOf(objects[2])));
        idList.add(String.valueOf(objects[0]));
      } // for
      // ids = ids.substring(0, ids.length() - 1);

      List<Norma43MovimientoDTO> norma43MovimientoList;
      // try {
      // LOG.debug("[generateApuntesComisionesBME] Iniciando procesamiento de los cobros de comisiones de BME ...");
      // while (!(norma43MovimientoList = norma43MovimientoDaoImpl.findMovimientosComisionesBME(ids, transactionSize)).isEmpty()) {
      // LOG.debug("[generateApuntesComisionesBME] Encontrados " + norma43MovimientoList.size() + " movimientos norma43 de comisiones de BME ...");
      // cobrosClearingCliente.processMovimientosClearing(norma43MovimientoList, mFichero, tolerancePorFichero, estadosMap,
      // FLAG_NORMA43_PROCESADO_TRUE, CIEN_BIGDECIMAL);
      // }
      // LOG.debug("[generateApuntesComisionesBME] Finalizado procesamiento de comisiones de BME ...");
      // } catch (Exception e) {
      // LOG.debug("[generateApuntesComisionesBME] Error en el procesamiento de comisiones de BME ...", e);
      // } // catch

      List<Long> ficherosNorma43OKList = new ArrayList<Long>();
      HashMap<String, List<Norma43MovimientoDTO>> movsFicheroIdPorFechaMap = new HashMap<String, List<Norma43MovimientoDTO>>();
      String fecha;
      List<Norma43MovimientoDTO> tmpList;

      String estadosAsig = "";
      for (Integer estados : lEstadosAsig) {
        estadosAsig += estados + ",";
      }
      estadosAsig = estadosAsig.substring(0, estadosAsig.length() - 1);

      Boolean hayError = Boolean.FALSE;

      try {
        LOG.debug("[generateApuntesComisionesBME] Iniciando procesamiento de los cobros de comisiones de BME ...");
        for (String id : idList) {
          try {
            LOG.debug("[generateApuntesComisionesBME] Tratando TMCT0_NORMA43_FICHERO: " + id);
            if (!(norma43MovimientoList = norma43MovimientoDaoImpl.findMovimientosComisionesBME(id, transactionSize)).isEmpty()) {
              LOG.debug("[generateApuntesComisionesBME] Encontrados " + norma43MovimientoList.size()
                        + " movimientos norma43. Agrupando movimientos por fecha ...");
              for (Norma43MovimientoDTO norma43MovimientoDTO : norma43MovimientoList) {
                if (norma43MovimientoDTO.getDescReferencia() == null || norma43MovimientoDTO.getDescReferencia().trim().length() == 0) {
                  fecha = "";
                } else if (!norma43MovimientoDTO.getDescReferencia().startsWith(LITERAL_COBROS_BME)) {
                  fecha = norma43MovimientoDTO.getDescReferencia().trim();
                } else {
                  fecha = norma43MovimientoDTO.getDescReferencia().substring(0, 10);
                }

                if (movsFicheroIdPorFechaMap.containsKey(fecha)) {
                  movsFicheroIdPorFechaMap.get(fecha).add(norma43MovimientoDTO);
                } else {
                  tmpList = new ArrayList<Norma43MovimientoDTO>();
                  tmpList.add(norma43MovimientoDTO);
                  movsFicheroIdPorFechaMap.put(fecha, tmpList);
                }
              } // for (Norma43MovimientoDTO norma43MovimientoDTO : norma43MovimientoList)

              for (Map.Entry<String, List<Norma43MovimientoDTO>> entry : movsFicheroIdPorFechaMap.entrySet()) {
                try {
                  cobrosComisionesBMESubList.generateApuntes(entry, estadosAsig, mFichero.get(Long.valueOf(id)),
                                                             tolerancePorFichero.get(Long.valueOf(id)), estadosMap, FLAG_NORMA43_PROCESADO_TRUE,
                                                             CIEN_BIGDECIMAL);
                } catch (Exception e) {
                  LOG.debug("[generateApuntesComisionesBME] Error en el procesamiento de las referencias: '" + entry.getKey() + "' ... ", e);
                  LOG.debug("[generateApuntesComisionesBME] Se continua con la siguiente referencia ...");
                  hayError = Boolean.TRUE;
                }
              }
            } // if (!(norma43MovimientoList = norma43MovimientoDaoImpl.findMovimientosComisionesBME(ids, transactionSize)).isEmpty())

            // Si se ha procesado el fichero completo se marca para luego actualizar su flag de procesado
            if (!hayError) {
              ficherosNorma43OKList.add(Long.valueOf(id));
            }

          } catch (Exception e) {
            LOG.debug("[generateApuntesComisionesBME] Error en el procesamiento del TMCT0_NORMA43_FICHERO: '" + id + "' ... ", e);
            LOG.debug("[generateApuntesComisionesBME] Se continua con el siguiente TMCT0_NORMA43_FICHERO ...");
          } finally {
            movsFicheroIdPorFechaMap.clear();
            hayError = Boolean.FALSE;
          } // finally
        } // for (String id : idList)

        // Se actualiza el flag de procesado para aquellos registros norma43 fichero que han sido procesados correctamente
        for (Long idFichero : ficherosNorma43OKList) {
          norma43FicheroDao.updateProcesadoFlag(idFichero, FLAG_NORMA43_PROCESADO_TRUE);
        }

        LOG.debug("[generateApuntesComisionesBME] Finalizado procesamiento de comisiones de BME ...");
      } catch (Exception e) {
        LOG.debug("[generateApuntesComisionesBME] Error en el procesamiento de comisiones de BME ...", e);
      } // catch

    } catch (Exception e) {
      LOG.warn("[generateApuntesComisionesBME] Error ..." + e.getMessage() + " ... ", e);
      throw e;
    } finally {
      final long endTime = System.currentTimeMillis();
      LOG.debug("[generateApuntesComisionesBME] Finalizada generacion de apuntes contables de cobros de comisiones de BME ..."
                + String.format(" [ %d min y %d sec ]",
                                TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))));
    } // finally
  }// generateApuntesComisionesBME

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PROTEGIDOS

  /**
   * Genera un descuadre con los datos especificados.
   * 
   * @param norma43Movimiento Movimiento Norma43.
   * @param importeAlc Importe a cobrar.
   * @param estadoAsigAlc Estado de asignación del ALC que genera el descuadre.
   * @param estadoContAlc Estado de contabilidad del ALC que genera el descuadre.
   * @param estadoEntrecAlc Estado de entrega/recepción del ALC que genera el descuadre.
   * @param nuorden Campo <code>nuorden</code> del ALC que genera el descuadre.
   * @param nbooking Campo <code>nbooking</code> del ALC que genera el descuadre.
   * @param nucnfclt Campo <code>nucnfclt</code> del ALC que genera el descuadre.
   * @param nucnfliq Campo <code>nucnfliq</code> del ALC que genera el descuadre.
   * @param tipoImporte Tipo de proceso que genera el descuadre.
   * @param diferencia Diferencia entre lo cobrado y lo que se esperaba cobrar.
   * @param comentario Comentario descriptivo del descuadre.
   * @return <code>Norma43DescuadresDTO - </code> Descuadre generado.
   */
  protected static Norma43DescuadresDTO generateDescuadre(Norma43MovimientoDTO norma43Movimiento,
                                                          BigDecimal importeAlc,
                                                          String estadoAsigAlc,
                                                          String estadoContAlc,
                                                          String estadoEntrecAlc,
                                                          String nuorden,
                                                          String nbooking,
                                                          String nucnfclt,
                                                          Short nucnfliq,
                                                          String tipoImporte,
                                                          BigDecimal diferencia,
                                                          String comentario) {
    LOG.debug("[generateDescuadre] Inicio ...");
    Norma43DescuadresDTO descuadreNorma43DTO = new Norma43DescuadresDTO();

    descuadreNorma43DTO.setCuenta(String.format("%04d", norma43Movimiento.getEntidad()).concat(String.format("%04d", norma43Movimiento.getOficina()))
                                        .concat(String.format("%010d", norma43Movimiento.getCuenta())));
    descuadreNorma43DTO.setRefFecha(norma43Movimiento.getReferencia());
    descuadreNorma43DTO.setRefOrden(norma43Movimiento.getDescReferencia());
    descuadreNorma43DTO.setSentido(norma43Movimiento.getDebe());
    try {
      descuadreNorma43DTO.setTradeDate(new SimpleDateFormat("yyyy-MM-dd").parse(norma43Movimiento.getTradedate()));
    } catch (ParseException e) {
    }
    try {
      descuadreNorma43DTO.setSettlementDate(new SimpleDateFormat("yyyy-MM-dd").parse(norma43Movimiento.getSettlementdate()));
    } catch (ParseException e) {
    }
    descuadreNorma43DTO.setImporteNorma43(norma43Movimiento.getImporte().divide(CIEN_BIGDECIMAL, 6, RoundingMode.HALF_UP));
    descuadreNorma43DTO.setTipoImporte(tipoImporte);

    descuadreNorma43DTO.setDiferencia(diferencia);
    descuadreNorma43DTO.setComentario(comentario.length() > 200 ? comentario.substring(0, 200) : comentario);

    descuadreNorma43DTO.setImporteSibbac(importeAlc);
    descuadreNorma43DTO.setNombreEstadoAsignacionAlc(estadoAsigAlc);
    descuadreNorma43DTO.setNombreEstadoContabilidadAlc(estadoContAlc);
    descuadreNorma43DTO.setNombreEstadoEntrecAlc(estadoEntrecAlc);
    descuadreNorma43DTO.setNuorden(nuorden);
    descuadreNorma43DTO.setNbooking(nbooking);
    descuadreNorma43DTO.setNucnfclt(nucnfclt);
    descuadreNorma43DTO.setNucnfliq(nucnfliq);

    // if (tmct0alc != null) {
    // descuadreNorma43DTO.setImporteSibbac(tmct0alc.getImefeagr().subtract(tmct0alc.getImcobrado() != null ? tmct0alc.getImcobrado()
    // : BigDecimal.ZERO));
    // descuadreNorma43DTO.setNombreEstadoAsignacionAlc(estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre());
    // descuadreNorma43DTO.setNombreEstadoContabilidadAlc(tmct0alc.getEstadocont().getNombre());
    // descuadreNorma43DTO.setNombreEstadoEntrecAlc(tmct0alc.getEstadoentrec().getNombre());
    // descuadreNorma43DTO.setNuorden(tmct0alc.getNuorden());
    // descuadreNorma43DTO.setNbooking(tmct0alc.getNbooking());
    // descuadreNorma43DTO.setNucnfclt(tmct0alc.getNucnfclt());
    // descuadreNorma43DTO.setNucnfliq(tmct0alc.getNucnfliq());
    // } // if (tmct0alc != null)
    //
    // norma43DescuadresDao.save(descuadreNorma43DTO.getDescuadreNorma43());

    LOG.debug("[generateDescuadre] Fin ...");
    return descuadreNorma43DTO;
  } // generateDescuadre

} // CobrosComisionesBME
