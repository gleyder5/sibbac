package sibbac.business.contabilidad.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.CierreContable}.
 * 
 * @author XI316153
 */
@Table(name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.CIERRE_CONTABLE)
@Entity
@Audited
public class CierreContable extends ATableAudited<CierreContable> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 5254952158417462917L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Mes cerrado contablemente. (Año*100) + Mes. */
  @Column(name = "MES")
  private Integer mes;

  /** Día de cierre del mes (será un día del mes siguiente). */
  @Column(name = "DIA")
  private Integer dia;

  /** Indica si se ha realizado la dotación de las operaciones del mes cerrado. */
  @Column(name = "DOTADO")
  private Boolean dotado;

  /** Indica si se ha realizado la desdotación de las operaciones del mes cerrado. */
  @Column(name = "DESDOTADO")
  private Boolean desdotado;

  /** Indica si se ha realizado el devengo de los cánones de BME. */
  @Column(name = "CANONES")
  private Boolean canones;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /** Constructor I. Constructor por defecto. */
  public CierreContable() {
    dia = 0; // TODO XI316153 ¿Para qué?
    dotado = Boolean.FALSE;
    desdotado = Boolean.FALSE;
    canones = Boolean.FALSE;
  } // CierreContable

  /**
   * Constructor II. Inicializa los atributos especificados de la clase.
   * 
   * @param mes Mes a cerrar. (Año*100) + Mes.
   * @param dia Día, del mes en el que se cierra el anterior, en el que se produce el cierre.
   */
  public CierreContable(int mes, int dia) {
    this();
    this.mes = mes;
    this.dia = dia;
  } // CierreContable

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "CierreContable [mes=" + mes + ", dia=" + dia + ", dotado=" + dotado + ", desdotado=" + desdotado + ", canones=" + canones + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the mes
   */
  public Integer getMes() {
    return mes;
  }

  /**
   * @return the dia
   */
  public Integer getDia() {
    return dia;
  }

  /**
   * @return the dotado
   */
  public Boolean isDotado() {
    return dotado;
  }

  /**
   * @return the desdotado
   */
  public Boolean isDesdotado() {
    return desdotado;
  }

  /**
   * @return the canones
   */
  public Boolean isCanones() {
    return canones;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param mes the mes to set
   */
  public void setMes(Integer mes) {
    this.mes = mes;
  }

  /**
   * @param dia the dia to set
   */
  public void setDia(Integer dia) {
    this.dia = dia;
  }

  /**
   * @param dotado the dotado to set
   */
  public void setDotado(Boolean dotado) {
    this.dotado = dotado;
  }

  /**
   * @param desdotado the desdotado to set
   */
  public void setDesdotado(Boolean desdotado) {
    this.desdotado = desdotado;
  }

  /**
   * @param canones the canones to set
   */
  public void setCanones(Boolean canones) {
    this.canones = canones;
  }

} // CierreContable
