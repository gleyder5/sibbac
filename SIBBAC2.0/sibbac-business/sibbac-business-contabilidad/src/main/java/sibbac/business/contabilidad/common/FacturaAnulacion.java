package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Component
public class FacturaAnulacion {
	private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");
	private static final String CONCEPTO_D = "ACTIVO Com Devengadas Ptes de Facturar";
	private static final String CONCEPTO_H = "ACTIVO Créditos Clientes Fras Pte Cobro";

	@Autowired
	private ApunteContableBo apunteBo;

	@Autowired
	private FacturaDao facturaDao;

	@Autowired
	private Tmct0estadoDao estadoDao;

	@Autowired
	private Tmct0alcDaoImp alcEstadoContBo;

	@Autowired
	private CuentaContableDao cuentaDao;

	@Autowired
	private ApunteContableAlcDao apunteAlcDao;
	
	@Autowired
	private Tmct0alcBo tmct0alcBo;

	@Transactional(propagation = Propagation.REQUIRES_NEW) public void put(Calendar hoy) throws Exception {
		List<Factura> lFactura = facturaDao.findByEstadocont(estadoDao.findOne(CONTABILIDAD.ANULADA.getId()));
		if (lFactura.isEmpty()) {
			return;
		}
		ApunteContable apunteD;
		ApunteContable apunteH;
		Alias alias;
		BigDecimal importe;
		Integer estadoasig;
		Map<String, Object> params = new HashMap<String, Object>();
		List<ApunteContable> lApuntes = new ArrayList<ApunteContable>();
		Tmct0estado estadoContFact = estadoDao.findOne(CONTABILIDAD.RECHAZADA.getId());
		Tmct0estado estadoContAlc = estadoDao.findOne(CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId());
		CuentaContable cuentaD = cuentaDao.findByCuenta(1122107);
		CuentaContable cuentaH = cuentaDao.findByCuenta(1032103);
		params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.EMISION_FACTURA);
		params.put("TIPO_APUNTE", TIPO_APUNTE.DIFERENCIA_EFECTIVO.getId());
		params.put("lApuntes", lApuntes);
		params.put("operativa", "N");
		params.put("contabilizado", false);
		for (Factura factura: lFactura) {
			alias = factura.getAlias();
			params.put("descripcion", factura.getNumeroFactura() + "/" + dF1.format(factura.getFechaInicio()) + "-"
					+ dF1.format(factura.getFechaInicio()) + "/" + alias.getCdaliass().trim() + "/"
					+ alias.getCdbrocli().trim());
			params.put("fechFactura", apunteBo.getFechaContabilizacion(hoy, factura.getFechaFactura()));
			params.put("sApunte", "D");
			params.put("concepto", CONCEPTO_D);
			params.put("cuenta", cuentaD);
			(apunteD = new ApunteContable(params)).setImporte(factura.getImporteTotal());
			apunteBo.save(apunteD);
			params.put("sApunte", "H");
			params.put("concepto", CONCEPTO_H);
			params.put("cuenta", cuentaH);
			(apunteH = new ApunteContable(params)).setImporte(factura.getImporteTotal().negate());
			apunteBo.save(apunteH);
			Tmct0alc alc;
			for (Tmct0alcId alcId : factura.getAlcIds()) {
			    alc = tmct0alcBo.findById(alcId);
				if (alc != null && (estadoasig = alc.getCdestadoasig())!=null && estadoasig>=ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
					alcEstadoContBo.setEstadocont(alc, estadoContAlc, TIPO_ESTADO.CORRETAJE_O_CANON);
					apunteAlcDao.save(new ApunteContableAlc(apunteD, alc, importe = alc.getEstadocont().getIdestado()==
							CONTABILIDAD.PDTE_AJUSTE.getId()? alc.getImajusvb(): alc.getImajusvb()==null?
							alc.getImcomisn(): alc.getImajusvb().add(alc.getImcomisn())));
					apunteAlcDao.save(new ApunteContableAlc(apunteH, alc, importe));
				}
			}
			factura.setEstadocont(estadoContFact);
			facturaDao.save(factura);
		}
	}
	
}
