package sibbac.business.contabilidad.utils.barridos;

import java.math.BigDecimal;

/**
 * @author XI316153
 */
public class BarridosRoutingDto {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  private String nureford;

  private BigDecimal importe;

  private String isin;

  private Integer tipoAPunte;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public BarridosRoutingDto() {
  } // BarridosRoutingDto

  public BarridosRoutingDto(String nureford, BigDecimal importe, String isin, Integer tipoAPunte) {
    this.nureford = nureford;
    this.importe = importe;
    this.isin = isin;
    this.tipoAPunte = tipoAPunte;
  } // BarridosRoutingDto

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODFOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "BarridosRoutingDto [nureford=" + nureford + ", importe=" + importe + ", isin=" + isin + ", tipoAPunte=" + tipoAPunte + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the nureford
   */
  public String getNureford() {
    return nureford;
  }

  /**
   * @return the importe
   */
  public BigDecimal getImporte() {
    return importe;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the tipoAPunte
   */
  public Integer getTipoAPunte() {
    return tipoAPunte;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param nureford the nureford to set
   */
  public void setNureford(String nureford) {
    this.nureford = nureford;
  }

  /**
   * @param importe the importe to set
   */
  public void setImporte(BigDecimal importe) {
    this.importe = importe;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param tipoAPunte the tipoAPunte to set
   */
  public void setTipoAPunte(Integer tipoAPunte) {
    this.tipoAPunte = tipoAPunte;
  }

} // BarridosRoutingDto

