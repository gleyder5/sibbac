package sibbac.business.contabilidad.rest;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTAB_INFS_PREFIJADOS;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceContaInformes implements SIBBACServiceBean {

	private static final Logger					LOG			= LoggerFactory.getLogger( SIBBACServiceContaInformes.class );
	private static final List< String >			commands	= new ArrayList< String >();
	private static String						sCommands	= "";
	private static final Map< String, Object >	mStatico	= new HashMap< String, Object >();
	private static final DateFormat dF = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	protected CuentaContableDao					cuentaDao;

	@Autowired
	private Tmct0alcDaoImp						alcDao;

	static {
		Map< String, String > mInformes = new LinkedHashMap< String, String >();
		Map< String, Object > mTipoOrden = new LinkedHashMap< String, Object >();
		Map< String, String > mTipologia = new LinkedHashMap< String, String >();
		Map< String, String > mTipoApunte = new LinkedHashMap< String, String >();
		Map< String, String > mEstadoContable = new LinkedHashMap< String, String >();
		Map< String, String > mEstadoLiquidacion = new LinkedHashMap< String, String >();
		commands.add( Constantes.CMD_INIT );
		commands.add( Constantes.CMD_FIND );
		for ( String aC : commands ) {
			sCommands += aC + ", ";
		}
		sCommands = sCommands.substring( 0, sCommands.length() - 2 );
		for ( CONTAB_INFS_PREFIJADOS informe : CONTAB_INFS_PREFIJADOS.values() ) {
			mInformes.put( informe.name(), informe.getDescripcion() );
		}
		mStatico.put( "prefijadas", mInformes );
		mTipologia.put( "70", "Routing" );
		mTipologia.put( "72", "Clientes facturables" );
		mTipologia.put( "75", "Clearing" );
		mStatico.put( "tipologia", mTipologia );
		mTipoOrden.put( "ALL", "Todos" );
		mTipoOrden.put( "TER", "Terceros" );
		mTipoOrden.put( "CTP", "Cuenta Propia" );
		mStatico.put( "tipoOrden", mTipoOrden );
		mTipoApunte.put( "", "Todos" );
		mTipoApunte.put( "C", "Compras" );
		mTipoApunte.put( "V", "Ventas" );
		mTipoApunte.put( "E", "Entregas" );
		mTipoApunte.put( "R", "Recepciones" );
		mStatico.put( "tipoApunte", mTipoApunte );
		mEstadoContable.put( "SD", "Sin devengar" );
		mEstadoContable.put( "DV", "Devengado" );
		mEstadoContable.put( "FC", "Facturado pdte. cobro" );
		mEstadoContable.put( "CB", "Cobrado" );
		mStatico.put( "estadoContable", mEstadoContable );
		mEstadoLiquidacion.put( "SLQ", "Sin Liquidar" );
		mEstadoLiquidacion.put( "MRC", "Liquidado Mercado" );
		mEstadoLiquidacion.put( "CLT", "Liquidado Cliente" );
		mEstadoLiquidacion.put( "CMP", "Liquidado Completamente" );
		mEstadoLiquidacion.put( "PRC", "Liquidado parcialmente" );
		mStatico.put( "estadoLiquidacion", mEstadoLiquidacion );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
		LOG.trace( "Inicio command: " + command );
		try {
			switch ( command ) {
				case Constantes.CMD_INIT:
					webResponse.setResultados( init() );
					break;
				case Constantes.CMD_FIND:
					webResponse.setResultados( filtra( webRequest.getFilters() ) );
					break;
				default:
					webResponse.setError( "An action must be set. Valid actions are: " + sCommands );
			}
			LOG.trace( "Fin command: " + command );
		} catch ( Exception e ) {
			webResponse.setError( e.getMessage() );
			LOG.error( "Error en contaInforme, comando: " + command, e );
		}
		return webResponse;
	}
	
	private Map< String, Object > init() throws Exception {
    LOG.trace( "Iniciando servicio de informes contables" );
    Map< String, Object > mSalida = new HashMap< String, Object >();
    Map< String, Object > map = new HashMap< String, Object >();
    Map<Integer, Long> mCuentas = new TreeMap<Integer, Long>();
    map.putAll( mStatico );
    for ( CuentaContable cuenta : cuentaDao.findAll() ) {
      mCuentas.put(cuenta.getCuenta(), cuenta.getId());
    }
    map.put( "cuentas", mCuentas );
    mSalida.put( "datosFiltro", map );
    return mSalida;
  }

	@Override
	public List< String > getAvailableCommands() {
		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}

	public Map< String, Object > filtra( Map< String, String > filters ) throws Exception {
		LOG.trace( "Generando informes contables" );
		Map< String, Object > mSalida = new HashMap< String, Object >();
		List< Map< String, Object >> lCuenta = new ArrayList< Map< String, Object >>();
		Map< String, Object > mDato;
		for ( Object[] objects : alcDao.findInformeContable( filters ) ) {
			lCuenta.add( mDato = new HashMap< String, Object >() );
//			trim(bok.cdalias)
			mDato.put( "alias", objects[ 0 ] );
//			trim(ali.descrali)
			mDato.put( "descrali", objects[ 1 ] );
//			alc.feejeliq
			mDato.put( "fhContratacion", objects[ 2 ] );
//			alc.feopeliq
			mDato.put( "fhLiquidacion", objects[ 3 ] );
//			trim(bok.cdtpoper)
			mDato.put( "cv", objects[ 4 ] );
//			alc.nutitliq
			mDato.put( "titulos", objects[ 5 ] );
//			bok.imcbmerc
			mDato.put( "cambio", objects[ 6 ] );
//			alc.imefeagr
			mDato.put( "efecBruto", objects[ 7 ] );
//			coalesce(alc.imcanoncompeu,0)+coalesce(alc.imcanoncontreu,0)+coalesce(alc.imcanonliqeu,0)
			mDato.put( "canon", objects[ 8 ] );
//			alc.imcomisn
			mDato.put( "imcomisn", objects[ 9 ] );
//			alc.imnfiliq
			mDato.put( "efecNeto", objects[ 10 ] );
//			trim(bok.cdisin)
			mDato.put( "isin", objects[ 11 ] );
//			trim(bok.nbooking)
			mDato.put( "booking", objects[ 12 ] );
//			trim(bok.nuorden)
			mDato.put( "orden", objects[ 13 ] );
//			trim(alc.nucnfclt)
			mDato.put( "desglose", objects[ 14 ] );
//			trim(alc.nucnfliq)
			mDato.put( "desgTitular", objects[ 15 ] );
//			est.nombre
			mDato.put( "estadoCont", objects[ 16 ] );
//			apt.audit_date
			mDato.put( "fhDevengo", objects[ 17 ] instanceof Timestamp? dF.format((Timestamp) objects[ 17 ]): "" );
//			alc.imajusvb
			mDato.put( "ajusteComision", objects[ 18 ] );
//			cobrado
			mDato.put( "cobrado", objects[ 19 ] );
			mDato.put( "gastos", ((int) objects[20])==0? "No": "Si");
//			alc.imfinsvb
			mDato.put("imfinsvb", objects[21]);
//			alc.imcomdvo
			mDato.put("imcomdvo", objects[22]);
//			alc.imcombrk
			mDato.put("imcombrk", objects[23]);
//			alc.imcombco
			mDato.put("imcombco", objects[24]);
//			alc.imntbrok
			mDato.put("imntbrok", objects[25]);
//			ord.nureford
			mDato.put("nureford", objects[26]);
		}
		mSalida.put( "datosCuenta", lCuenta );
		return mSalida;
	}
}
