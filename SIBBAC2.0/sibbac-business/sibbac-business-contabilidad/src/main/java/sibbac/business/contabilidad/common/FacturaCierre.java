package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.FACTURA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.AlcOrdenDetalleFactura;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Component
public class FacturaCierre {
	private static final String CONCEPTO = "PYG Dif/Ajustes Negativas Corretajes";

	@Autowired
	private ApunteContableBo apunteBo;

	@Autowired
	private FacturaDao facturaDao;

	@Autowired
	private Tmct0alcDaoImp alcEstadoContBo;

	@Autowired
	private Tmct0estadoDao estadoDao;

	@Autowired
	private ApunteContableAlcDao apunteAlcDao;

	@Autowired
	private CuentaContableDao cuentaDao;
	
	@Autowired
	private Tmct0alcBo alcBo;

	@Transactional(propagation = Propagation.REQUIRES_NEW) public void put(Calendar hoy) throws Exception {
		List<Factura> lFactura = facturaDao.findByEstadoAndEstadocont(estadoDao.findOne(FACTURA.COBRADO_TOTALMENTE.getId()),
        estadoDao.findOne(CONTABILIDAD.FACTURADA_PDTE_COBRO.getId()));
		if (lFactura.isEmpty()) {
			return;
		}
		BigDecimal importe;
		ApunteContable apunteD;
		ApunteContable apunteH;
		Alias alias;
		Integer estadoasig;
        Tmct0alcId alcPorOrdenId;
		Tmct0alc alc2;
		Map<String, Object> params = new HashMap<String, Object>();
		Tmct0estado estadoCont = estadoDao.findOne(CONTABILIDAD.COBRADA.getId());
		CuentaContable cuentaD = cuentaDao.findByCuenta(4021102);
		CuentaContable cuentaH = cuentaDao.findByCuenta(1032103);
		params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.COBRO_FACTURA);
		params.put("TIPO_APUNTE", TIPO_APUNTE.DIFERENCIA_EFECTIVO.getId());
		params.put("operativa", "N");
		params.put("contabilizado", false);
		params.put("concepto", CONCEPTO);
		for (Factura factura: lFactura) {
			for (Tmct0alcId alcId: factura.getAlcIds()) {
			    Tmct0alc alc = alcBo.findById(alcId);
				if ((estadoasig = alc.getCdestadoasig())!=null && estadoasig>=ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
					alcEstadoContBo.setEstadocont(alc, estadoCont, TIPO_ESTADO.CORRETAJE_O_CANON);
				}
			}
			factura.setEstadocont(estadoCont);
			facturaDao.save(factura);
			if ((importe = factura.getImportePendiente()).signum()!=0) {
				alias = factura.getAlias();
				params.put("descripcion", "DIF Cobro nº " + factura.getNumeroFactura()
						+ "/" + alias.getCdaliass() + "/" + alias.getCdbrocli().trim());
				params.put("fechFactura", apunteBo.getFechaContabilizacion(hoy, factura.getFechaFactura()));
				params.put("sApunte", "D");
				params.put("cuenta", cuentaD);
				(apunteD = new ApunteContable(params)).setImporte(importe);
				apunteBo.save(apunteD);
				params.put("sApunte", "H");
				params.put("cuenta", cuentaH);
				(apunteH = new ApunteContable(params)).setImporte(importe.negate());
				apunteBo.save(apunteH);
				for (AlcOrdenDetalleFactura alcODF: factura.getAlcOrdenFacturaDetalles()) {
				    alcPorOrdenId = alcODF.getAlcOrden().createAlcIdFromFields();
				    alc2 = alcBo.findById(alcPorOrdenId);
					if (alc2 != null 
					        && (estadoasig = alc2.getCdestadoasig())!=null
							&& estadoasig>=ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
						apunteAlcDao.save(new ApunteContableAlc(apunteD,alc2,alc2.getImcomisn()));
						apunteAlcDao.save(new ApunteContableAlc(apunteH,alc2,alc2.getImcomisn()));
					}
				}
			}
		}
	}
	
}
