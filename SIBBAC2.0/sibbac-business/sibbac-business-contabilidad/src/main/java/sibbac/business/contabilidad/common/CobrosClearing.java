package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43FicheroDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDaoImpl;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE;
import sibbac.business.fase0.database.model.Tmct0estado;

/**
 * Realiza los apuntes contables de cobros de efectivo y comisión SVB de la pata mercado y la pata cliente de las operaciones de clearing.
 * 
 * @author XI316153
 */
@Service
public class CobrosClearing {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosClearing.class);

  /** Flag que indica que un registro ha sido procesado. */
  private static final Integer FLAG_NORMA43_PROCESADO_TRUE = 1;

  /** Cien en BigDecimal. */
  private static final BigDecimal CIEN_BIGDECIMAL = new BigDecimal(100);
  
  /** Indica que un registro norma43 es de adeudo. */
  public static final String ADEUDO = "1";

  /** Indica que un registro norma43 es de abono. */
  public static final String ABONO = "2";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Norma43FicheroDao norma43FicheroDao;

  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  private Norma43MovimientoDaoImpl norma43MovimientoDaoImpl;

  @Autowired
  private CobrosClearingMercado cobrosClearingMercado;

  @Autowired
  private CobrosClearingCliente cobrosClearingCliente;

  @Autowired
  private CobrosClearingClienteNetting cobrosClearingClienteNetting;
  
  @Autowired
  private CobrosClearingFallidos cobrosClearingFallidos;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera los apuntes contables de cobro de operaciones de clearing.
   * 
   * @param transactionSize Número de registros a procesar por cada transacción.
   * @param estadosMap Estados usados durante el proceso.
   * @throws Exception Se ha ocurrido algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void generateApuntesClearing(Integer transactionSize, HashMap<String, Tmct0estado> estadosMap) throws Exception {
    LOG.debug("[generateApuntesClearing] Iniciando generacion de apuntes contables de cobros Norma43 clearing ...");
    final long startTime = System.currentTimeMillis();

    List<Object[]> norma43FicheroList;
    Boolean hayError = Boolean.FALSE;
    String errorMsg = "";

    try {
      // 1.- Se buscan los registros de la tabla TMCT0_NORMA43_FICHERO de cobros de clearing que estén sin procesar
      LOG.debug("[generateApuntesClearing] Buscando registros TMCT0_NORMA43_FICHERO de tipologia clearing y tipo de importe efectivo sin procesar ...");
      norma43FicheroList = norma43FicheroDao.findByProcesadoAndTipologiaAndTipoImporte(Boolean.FALSE, TIPOLOGIA.Clearing.getTipo(),
                                                                                       TIPO_IMPORTE.Efectivo.getTipo());
      if (norma43FicheroList.isEmpty()) {
        LOG.debug("[generateApuntesClearing] No se han encontrado ficheros norma43 para procesar ...");
        return;
      } // if
      LOG.debug("[generateApuntesClearing] Encontrados {} ficheros norma43 para procesar ...", norma43FicheroList.size());

      // 2.- Se procesan los cobros de patas mercado
      String ids = "";
      List<Object[]> list;
      Map<Long, Long> mFichero = new HashMap<Long, Long>();
      Map<String, Object> params = new HashMap<String, Object>();

      Map<Long, BigDecimal> tolerancePorFichero = new HashMap<Long, BigDecimal>();

      for (Object[] objects : norma43FicheroList) {
        ids += objects[0] + ",";
        mFichero.put((Long) objects[0], (Long) objects[1]);
        tolerancePorFichero.put((Long) objects[0], new BigDecimal(String.valueOf(objects[2])));
      } // for
      ids = ids.substring(0, ids.length() - 1);
      params.put("mFichero", mFichero);
      params.put("tolerancePorFichero", tolerancePorFichero);

      try {
        LOG.debug("[generateApuntesClearing] Iniciando procesamiento de las patas mercado ...");
        // if ((size = norma43MovimientoDaoImpl.count(ids)) == 0) {
        // LOG.debug("[generateApuntesClearing] No se han encontrado movimientos norma43 pata mercado para procesar ...");
        // } else {
        // int num = size / (size / transactionSize + 1) + 1;
        // LOG.debug("[generateApuntesClearing] Encontrados {} movimientos norma43 pata mercado para procesar ...", size);
        //
        // for (int fromIndex = 1; fromIndex < size; fromIndex += num) {
        // params.put("list", list = norma43MovimientoDaoImpl.find(ids, fromIndex, fromIndex + num - 1));
        // LOG.debug("[generateApuntesClearing] Se van a procesar {} movimientos norma43 pata mercado ...", list.size());
        // cobrosClearingMercado.put(params);
        // } // for
        // } // else

        while (!(list = norma43MovimientoDaoImpl.findMovimientosPatasMercado(ids, transactionSize)).isEmpty()) {
          LOG.debug("[generateApuntesClearing] Se van a procesar {} movimientos norma43 pata mercado ...", list.size());
          params.put("list", list);
          cobrosClearingMercado.put(params);
        } // while

        LOG.debug("[generateApuntesClearing] Finalizado procesamiento de las patas mercado ...");
      } catch (Exception e) {
        hayError = Boolean.TRUE;
        errorMsg = "[generateApuntesClearing] Error en el procesamiento de las patas mercado ... " + e.getMessage();
        LOG.debug("[generateApuntesClearing] Error en el procesamiento de las patas mercado ...", e);
      } // catch

      // 3.- Se procesan los cobros/abonos de movimientos de fallidos
      List<Norma43MovimientoDTO> norma43MovimientoList;
      try {
        LOG.debug("[generateApuntesClearing] Iniciando procesamiento de los movimientos de fallidos ...");
        while (!(norma43MovimientoList = norma43MovimientoDaoImpl.findMovimientosFallidos(ids, transactionSize)).isEmpty()) {
          LOG.debug("[generateApuntesClearing] Encontrados " + norma43MovimientoList.size() + " movimientos norma43 de fallidos ...");
          cobrosClearingFallidos.processMovimientosFallidos(norma43MovimientoList, mFichero, tolerancePorFichero, estadosMap,
                                                           FLAG_NORMA43_PROCESADO_TRUE, CIEN_BIGDECIMAL);
        }
        LOG.debug("[generateApuntesClearing] Finalizado procesamiento de los movimientos de fallidos ...");
      } catch (Exception e) {
        hayError = Boolean.TRUE;
        errorMsg = errorMsg.concat(System.lineSeparator())
                           .concat("[generateApuntesClearing] Error en el procesamiento de los movimientos de fallidos ..." + e.getMessage());
        LOG.debug("[generateApuntesClearing] Error en el procesamiento de los movimientos de fallidos ...", e);
      } // catch

      // 4.- Se procesan los cobros de patas cliente para alias que NO hacen netting
      try {
        LOG.debug("[generateApuntesClearing] Iniciando procesamiento de las patas cliente no netting ...");
        while (!(norma43MovimientoList = norma43MovimientoDaoImpl.findMovimientosPatasCliente(Boolean.FALSE, ids, transactionSize, Boolean.TRUE,
                                                                                              Boolean.FALSE)).isEmpty()) {
          LOG.debug("[generateApuntesClearing] Encontrados " + norma43MovimientoList.size() + " movimientos norma43 pata cliente no netting ...");
          cobrosClearingCliente.processMovimientosClearing(norma43MovimientoList, mFichero, tolerancePorFichero, estadosMap,
                                                           FLAG_NORMA43_PROCESADO_TRUE, CIEN_BIGDECIMAL);
        }
        LOG.debug("[generateApuntesClearing] Finalizado procesamiento de las patas cliente no netting ...");
      } catch (Exception e) {
        hayError = Boolean.TRUE;
        errorMsg = errorMsg.concat(System.lineSeparator())
                           .concat("[generateApuntesClearing] Error en el procesamiento de las patas cliente no netting ..." + e.getMessage());
        LOG.debug("[generateApuntesClearing] Error en el procesamiento de las patas cliente no netting ...", e);
      } // catch

      // 5.- Se procesan los cobros de patas cliente para alias que SI hacen netting
      try {
        LOG.debug("[generateApuntesNetting] Iniciando procesamiento de las patas cliente netting ...");
        while (!(norma43MovimientoList = norma43MovimientoDaoImpl.findMovimientosPatasCliente(Boolean.FALSE, ids, transactionSize, Boolean.TRUE,
                                                                                              Boolean.TRUE)).isEmpty()) {
          LOG.debug("[generateApuntesNetting] Encontrados " + norma43MovimientoList.size() + " movimientos norma43 pata cliente netting ...");
          cobrosClearingClienteNetting.processMovimientosNetting(norma43MovimientoList, mFichero, tolerancePorFichero, estadosMap,
                                                                 FLAG_NORMA43_PROCESADO_TRUE, CIEN_BIGDECIMAL);
        }
        LOG.debug("[generateApuntesNetting] Finalizado procesamiento de las patas cliente netting ...");
      } catch (Exception e) {
        hayError = Boolean.TRUE;
        errorMsg = errorMsg.concat(System.lineSeparator())
                           .concat("[generateApuntesNetting] Error en el procesamiento de las patas cliente netting ..." + e.getMessage());
        LOG.debug("[generateApuntesNetting] Error en el procesamiento de las patas cliente netting ...", e);
      } // catch

      // 6.- Se procesan los cobros recibidos que no son nada de lo anterior para generar descuadres
      try {
        LOG.debug("[generateApuntesClearing] Iniciando procesamiento de las referencias no reconocidas ...");
        LOG.debug("[generateApuntesClearing] Sin hacer ..");
        LOG.debug("[generateApuntesClearing] Finalizado procesamiento de las referencias no reconocidas ...");
      } catch (Exception e) {
        LOG.debug("[generateApuntesClearing] Error en el procesamiento de las referencias no reconocidas ...", e);
      } // catch

      if (hayError) {
        LOG.warn("[generateApuntesClearing] Se han producido errores durante el proceso. Se dejara el proceso en estado de error.");
        LOG.warn("[generateApuntesClearing] Error(es) producido(s): " + errorMsg);
        throw new Exception(errorMsg);
      } else {
        // 6.- Se marcan los registros de la tabla TMCT0_NORMA43_FICHERO como procesados
        LOG.debug("[generateApuntesClearing] Iniciando actualizacion del flag procesado de los registros TMCT0_NORMA43_FICHERO ...");
        // for (Object[] norma43FicheroObject : norma43FicheroList) {
        // norma43FicheroDao.updateProcesadoFlag(Long.valueOf(norma43FicheroObject[0].toString()), FLAG_NORMA43_PROCESADO_TRUE);
        // } // for
        LOG.debug("[generateApuntesClearing] Se omite la actualizacion hasta completar la contabilidad ..");
        LOG.debug("[generateApuntesClearing] Finalizada actualizacion del flag procesado de los registros TMCT0_NORMA43_FICHERO finalizada");
      }

    } catch (Exception e) {
      LOG.warn("[generateApuntesClearing] Error ..." + e.getMessage() + " ... ", e);
      throw e;
    } finally {
      final long endTime = System.currentTimeMillis();
      LOG.debug("Finalizada generacion de apuntes contables de cobros Norma43 clearing ..."
                + String.format(" [ %d min y %d sec ]",
                                TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))));
    } // finally
  }// generateApuntesClearing

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PROTEGIDOS

  /**
   * Genera un descuadre con los datos especificados.
   * 
   * @param norma43Movimiento Movimiento Norma43.
   * @param importeAlc Importe a cobrar.
   * @param estadoAsigAlc Estado de asignación del ALC que genera el descuadre.
   * @param estadoContAlc Estado de contabilidad del ALC que genera el descuadre.
   * @param estadoEntrecAlc Estado de entrega/recepción del ALC que genera el descuadre.
   * @param nuorden Campo <code>nuorden</code> del ALC que genera el descuadre.
   * @param nbooking Campo <code>nbooking</code> del ALC que genera el descuadre.
   * @param nucnfclt Campo <code>nucnfclt</code> del ALC que genera el descuadre.
   * @param nucnfliq Campo <code>nucnfliq</code> del ALC que genera el descuadre.
   * @param tipoImporte Tipo de proceso que genera el descuadre.
   * @param diferencia Diferencia entre lo cobrado y lo que se esperaba cobrar.
   * @param comentario Comentario descriptivo del descuadre.
   * @return <code>Norma43DescuadresDTO - </code> Descuadre generado.
   */
  protected static Norma43DescuadresDTO generateDescuadre(Norma43MovimientoDTO norma43Movimiento,
                                                          BigDecimal importeAlc,
                                                          String estadoAsigAlc,
                                                          String estadoContAlc,
                                                          String estadoEntrecAlc,
                                                          String nuorden,
                                                          String nbooking,
                                                          String nucnfclt,
                                                          Short nucnfliq,
                                                          String tipoImporte,
                                                          BigDecimal diferencia,
                                                          String comentario) {
    LOG.debug("[generateDescuadre] Inicio ...");
    Norma43DescuadresDTO descuadreNorma43DTO = new Norma43DescuadresDTO();

    descuadreNorma43DTO.setCuenta(String.format("%04d", norma43Movimiento.getEntidad()).concat(String.format("%04d", norma43Movimiento.getOficina()))
                                        .concat(String.format("%010d", norma43Movimiento.getCuenta())));
    descuadreNorma43DTO.setRefFecha(norma43Movimiento.getReferencia());
    descuadreNorma43DTO.setRefOrden(norma43Movimiento.getDescReferencia());
    descuadreNorma43DTO.setSentido(norma43Movimiento.getDebe());
    try {
      descuadreNorma43DTO.setTradeDate(new SimpleDateFormat("yyyy-MM-dd").parse(norma43Movimiento.getTradedate()));
    } catch (ParseException e) {
    }
    try {
      descuadreNorma43DTO.setSettlementDate(new SimpleDateFormat("yyyy-MM-dd").parse(norma43Movimiento.getSettlementdate()));
    } catch (ParseException e) {
    }
    descuadreNorma43DTO.setImporteNorma43(norma43Movimiento.getImporte().divide(CIEN_BIGDECIMAL, 6, RoundingMode.HALF_UP));
    descuadreNorma43DTO.setTipoImporte(tipoImporte);

    descuadreNorma43DTO.setDiferencia(diferencia);
    descuadreNorma43DTO.setComentario(comentario.length() > 200 ? comentario.substring(0, 200) : comentario);

    descuadreNorma43DTO.setImporteSibbac(importeAlc);
    descuadreNorma43DTO.setNombreEstadoAsignacionAlc(estadoAsigAlc);
    descuadreNorma43DTO.setNombreEstadoContabilidadAlc(estadoContAlc);
    descuadreNorma43DTO.setNombreEstadoEntrecAlc(estadoEntrecAlc);
    descuadreNorma43DTO.setNuorden(nuorden);
    descuadreNorma43DTO.setNbooking(nbooking);
    descuadreNorma43DTO.setNucnfclt(nucnfclt);
    descuadreNorma43DTO.setNucnfliq(nucnfliq);

    // if (tmct0alc != null) {
    // descuadreNorma43DTO.setImporteSibbac(tmct0alc.getImefeagr().subtract(tmct0alc.getImcobrado() != null ? tmct0alc.getImcobrado()
    // : BigDecimal.ZERO));
    // descuadreNorma43DTO.setNombreEstadoAsignacionAlc(estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre());
    // descuadreNorma43DTO.setNombreEstadoContabilidadAlc(tmct0alc.getEstadocont().getNombre());
    // descuadreNorma43DTO.setNombreEstadoEntrecAlc(tmct0alc.getEstadoentrec().getNombre());
    // descuadreNorma43DTO.setNuorden(tmct0alc.getNuorden());
    // descuadreNorma43DTO.setNbooking(tmct0alc.getNbooking());
    // descuadreNorma43DTO.setNucnfclt(tmct0alc.getNucnfclt());
    // descuadreNorma43DTO.setNucnfliq(tmct0alc.getNucnfliq());
    // } // if (tmct0alc != null)
    //
    // norma43DescuadresDao.save(descuadreNorma43DTO.getDescuadreNorma43());

    LOG.debug("[generateDescuadre] Fin ...");
    return descuadreNorma43DTO;
  } // generateDescuadre

} // CobrosClearing
