package sibbac.business.contabilidad.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Tmct0alc;

@Service
public class FacturaEmision {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FacturaEmision.class);

  /** Formato de las fechas del campo descrpción de los apuntes contables. */
  private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");

  /** Concepto de los apuntes al Debe. */
  private static final String CONCEPTO_D = "ACTIVO Créditos Clientes Fras Pte Cobro";

  /** Concepto de los apuntes al Haber. */
  private static final String CONCEPTO_H = "ACTIVO Com Devengadas Ptes de Facturar";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private FacturaDao facturaDao;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private Tmct0alcDao tmct0alcDao;

  // @Autowired
  // private ApunteContableAlcDao apunteContableAlcDao;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  // /**
  // * Realiza los apuntes contables de las facturas emitidas.
  // *
  // * @param hoy Ficha del momento de la ejecución.
  // * @throws Exception Si ocurre algún error durante la generación de los apuntes contables.
  // */
  // @Transactional(propagation = Propagation.REQUIRES_NEW)
  // public void put(Calendar hoy) throws Exception {
  // LOG.debug("[FacturaEmision :: put] Inicio ...");
  // List<Factura> lFactura = facturaDao.findByEstadocont(estadoDao.findOne(CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId()));
  //
  // if (lFactura.isEmpty()) {
  // return;
  // } // if
  //
  // Map<String, Object> params = new HashMap<String, Object>();
  // Alias alias;
  // Tmct0alc alc;
  // ApunteContable apunteD;
  // ApunteContable apunteH;
  // Integer estadoasig;
  // Tmct0estado estadoCont = estadoDao.findOne(CONTABILIDAD.FACTURADA_PDTE_COBRO.getId());
  // CuentaContable cuentaD = cuentaContableDao.findByCuenta(1032103);
  // CuentaContable cuentaH = cuentaContableDao.findByCuenta(1122107);
  //
  // Map<Date, Date> mapaFechasContabilizacion = new HashMap<Date, Date>();
  // Date fechaApunte;
  //
  // params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.EMISION_FACTURA);
  // params.put("TIPO_APUNTE", TIPO_APUNTE.FACTURA.getId());
  // params.put("operativa", "N");
  // params.put("contabilizado", false);
  //
  // for (Factura factura : lFactura) {
  // LOG.debug("[FacturaEmision :: put] Procesando factura: {}", factura.getId());
  // alias = factura.getAlias();
  // params.put("descripcion", factura.getNumeroFactura() + "/" + dF1.format(factura.getFechaInicio()) + "-" + dF1.format(factura.getFechaFin())
  // + "/" + alias.getCdaliass().trim() + "/" + alias.getCdbrocli().trim());
  //
  // // params.put("fechFactura", apunteContableBo.getFechaContabilizacion(hoy, factura.getFechaFactura()));
  // if ((fechaApunte = mapaFechasContabilizacion.get(factura.getFechaFactura())) == null) {
  // mapaFechasContabilizacion.put(factura.getFechaFactura(),
  // fechaApunte = apunteContableBo.getFechaContabilizacion(hoy, factura.getFechaFactura()));
  // } // if
  // params.put("fechFactura", fechaApunte);
  //
  // params.put("sApunte", "D");
  // params.put("concepto", CONCEPTO_D);
  // params.put("cuenta", cuentaD);
  // (apunteD = new ApunteContable(params)).setImporte(factura.getImporteTotal());
  // apunteContableBo.save(apunteD);
  //
  // params.put("sApunte", "H");
  // params.put("concepto", CONCEPTO_H);
  // params.put("cuenta", cuentaH);
  // (apunteH = new ApunteContable(params)).setImporte(factura.getImporteTotal().negate());
  // apunteContableBo.save(apunteH);
  //
  // for (AlcOrdenDetalleFactura alcODF : factura.getAlcOrdenFacturaDetalles()) {
  // if ((estadoasig = (alc = alcODF.getAlcOrden().getTmct0alc()).getCdestadoasig()) != null && estadoasig >= ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
  // // apunteContableAlcDao.save(new ApunteContableAlc(apunteD, alc, alc.getImcomisn()));
  // // apunteContableAlcDao.save(new ApunteContableAlc(apunteH, alc, alc.getImcomisn()));
  // tmct0alcDaoImp.setEstadocont(alc, estadoCont, TIPO_ESTADO.CORRETAJE_O_CANON);
  // } // if
  // } // for
  //
  // factura.setEstadocont(estadoCont);
  // facturaDao.save(factura);
  // } // for (Factura factura : lFactura)
  //
  // LOG.debug("[FacturaEmision :: put] Fin ...");
  // } // put

  /**
   * Actualiza el estado contable de los ALCS que componen la factura especificada.
   * 
   * @param facturaId
   * @param transactionSize
   * @param estadoContPdteCobro
   * @return
   * @throws Exception
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Boolean updateAlcEstadoCont(Long facturaId, Integer transactionSize, Tmct0estado estadoContPdteCobro) throws Exception {
    LOG.debug("[updateAlcEstadoCont] Inicio ...");
    Boolean result = Boolean.FALSE;

    List<Tmct0alc> tmct0alcList = tmct0alcDao.findAlcsByFactura(facturaId, ASIGNACIONES.PDTE_LIQUIDAR.getId(), estadoContPdteCobro.getIdestado(),
                                                                new PageRequest(0, transactionSize));

    if (tmct0alcList != null && !tmct0alcList.isEmpty()) {
      for (Tmct0alc tmct0alc : tmct0alcList) {
        // if (tmct0alc.getCdestadoasig() != null && tmct0alc.getCdestadoasig() >= ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
        tmct0alcDaoImp.setEstadocont(tmct0alc, estadoContPdteCobro, TIPO_ESTADO.CORRETAJE_O_CANON);
        result = Boolean.TRUE;
        // } // if
      } // for (Tmct0alc tmct0alc : tmct0alcList)
    } // if (tmct0alcList != null && !tmct0alcList.isEmpty())

    LOG.debug("[updateAlcEstadoCont] Fin ...");
    return result;
  } // updateAlcEstadoCont

  /**
   * 
   * @param facturaId
   * @param hoy
   * @param estadoContPdteCobro
   * @throws Exception
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void putApuntesContables(Long facturaId, Calendar hoy, Tmct0estado estadoContPdteCobro) throws Exception {
    LOG.debug("[putApuntesContables] Inicio ...");
    Factura factura = facturaDao.findOne(facturaId);

    Map<String, Object> params = new HashMap<String, Object>();
    Alias alias = factura.getAlias();

    CuentaContable cuentaDebe = cuentaContableDao.findByCuenta(1032103);
    CuentaContable cuentaHaber = cuentaContableDao.findByCuenta(1122107);

    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.EMISION_FACTURA);
    params.put("TIPO_APUNTE", TIPO_APUNTE.FACTURA.getId());
    params.put("operativa", "N");
    params.put("contabilizado", Boolean.FALSE);
    params.put("descripcion", factura.getNumeroFactura() + "/" + dF1.format(factura.getFechaInicio()) + "-" + dF1.format(factura.getFechaFin()) + "/"
                              + alias.getCdaliass().trim() + "/" + alias.getCdbrocli().trim());
    params.put("fechFactura", apunteContableBo.getFechaContabilizacion(hoy, factura.getFechaFactura()));

    // Apunte al Debe
    params.put("sApunte", "D");
    params.put("concepto", CONCEPTO_D);
    params.put("cuenta", cuentaDebe);
    apunteContableBo.save(new ApunteContable(params)).setImporte(factura.getImporteTotal());

    params.put("sApunte", "H");
    params.put("concepto", CONCEPTO_H);
    params.put("cuenta", cuentaHaber);
    apunteContableBo.save(new ApunteContable(params)).setImporte(factura.getImporteTotal().negate());

    factura.setEstadocont(estadoContPdteCobro);
    facturaDao.save(factura);

    LOG.debug("[putApuntesContables] Inicio ...");
  } // putApuntesContables

} // FacturaEmision
