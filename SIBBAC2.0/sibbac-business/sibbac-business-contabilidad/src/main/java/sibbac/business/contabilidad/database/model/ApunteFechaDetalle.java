package sibbac.business.contabilidad.database.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

@Entity @Component @Audited
@Table(name = DBConstants.TABLE_PREFIX+DBConstants.CONTABLES.APUNTE_FECHA_DETALLE)
public class ApunteFechaDetalle extends ATableAudited<ApunteFechaDetalle> {
	private static final long serialVersionUID = -2801145414835925536L;

	@Column(name="ID_FECHA", nullable=false, unique=true) private Long idFecha;
	@Column(name="ID_APUNTE", nullable=false, unique=true) private Long idApunte;
	@Column(name="IMPORTE", precision=15, scale=2) private BigDecimal importe = BigDecimal.ZERO;
}
