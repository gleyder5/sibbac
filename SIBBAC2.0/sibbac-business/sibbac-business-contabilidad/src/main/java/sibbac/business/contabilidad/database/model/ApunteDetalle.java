package sibbac.business.contabilidad.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

@Table(name = DBConstants.TABLE_PREFIX+DBConstants.CONTABLES.APUNTE_DETALLE)
@Entity
@Component
@Audited
public class ApunteDetalle extends ATableAudited< ApunteDetalle > {
	private static final long serialVersionUID = 3491582508661806195L;

	@ManyToOne(fetch=FetchType.LAZY,optional=true)
	@JoinColumns({

//		trim(alc.nuorden)
		@JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = true ),

//		trim(alc.nbooking)
		@JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = true ),

//		trim(alc.nucnfclt)
		@JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = true ),

//		trim(alc.nucnfliq)
		@JoinColumn(name = "NUCNFLIQ", referencedColumnName = "NUCNFLIQ", nullable = true )
	})
	@Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED)
	private Tmct0alc alc;

//	Fecha en la que se devengó
	@Column(name="DEVENGO", length=10) @Temporal(TemporalType.DATE) private Date devengo;

//	Tipo de orden: Tercero o Cuenta Propia
	@Column(name="TERCERO") private boolean tercero;

//	Tipo apunte: C-Compra, E-Entrega, V-Venta, R-Recepción.
//	trim(bok.cdtpoper)=='C' => Compra: alc.cdestadoentrec is null. Entrega: alc.cdestadoentrec is not null.
//	trim(bok.cdtpoper)=='V' => Venta: alc.cdestadoentrec is null. Recepción: alc.cdestadoentrec is not null
	@Column(name="TIPO_APUNTE", length=1) private String tipoApunte;

	/**
	 * <b>Estado liquidación</b>
	 * @see sibbac.business.fase0.database.dao.EstadosEnumerados.LIQUIDACION
	 */
	@Column(name="LIQUIDACION", length=3) private int liquidacion;

	public Tmct0alc getAlc() {
		return alc;
	}

	public void setAlc(Tmct0alc alc) {
		this.alc = alc;
	}

	public Date getDevengo() {
		return devengo;
	}

	public void setDevengo(Date devengo) {
		this.devengo = devengo;
	}

	public boolean isTercero() {
		return tercero;
	}

	public void setTercero(boolean tercero) {
		this.tercero = tercero;
	}

	public String getTipoApunte() {
		return tipoApunte;
	}

	public void setTipoApunte(String tipoApunte) {
		this.tipoApunte = tipoApunte;
	}

	public int getLiquidacion() {
		return liquidacion;
	}

	public void setLiquidacion(int liquidacion) {
		this.liquidacion = liquidacion;
	}

//	Fecha de contratación: alc.feejeliq
//	@Column(name="FEEJELIQ", length=10) @Temporal(TemporalType.DATE) private Date feejeliq;

//	Fecha de liquidación: alc.feopeliq
//	@Column(name="FEOPELIQ", length=10) @Temporal(TemporalType.DATE) private Date feopeliq;

//	Número de títulos: alc.nutitliq
//	@Column(name="NUTITLIQ") private int nutitliq;

//	Efectivo bruto: alc.imefeagr
//	@Column(name="IMEFEAGR", precision=18, scale=4) private BigDecimal imefeagr;

//	Canon: coalesce(alc.imcanoncompeu,0)+coalesce(alc.imcanoncontreu,0)+coalesce(alc.imcanonliqeu,0)
//	@Column(name="CANON", precision=18, scale=8) private BigDecimal canon;

//	Corretaje: alc.imcomisn
//	@Column(name="IMCOMISN", nullable=false, precision=18, scale=8) private BigDecimal imcomisn = BigDecimal.ZERO;

//	Ajuste corretaje: alc.imajusvb
//	@Column(name="IMAJUSVB", precision=18, scale=4) private BigDecimal imajusvb;

//	Efectivo neto: alc.imnfiliq
//	@Column(name = "IMNFILIQ", precision=18, scale=4) private BigDecimal imnfiliq;

//	El cliente asume los gastos: alo.canoncontrpagoclte==1
//	@Column(name="GASTOS") private boolean gastos;

//	Código de alias: trim(bok.cdalias)
//	@Column(name="CDALIAS", length=20) private String cdalias;

//	Nombre del cliente: trim(bok.descrali)
//	@Column(name="DESCRALI", length=133) private String descrali;

//	Cambio medio: bok.imcbmerc
//	@Column(name="IMCBMERC", precision=18, scale=8) private BigDecimal imcbmerc = BigDecimal.ZERO;

//	Código ISIN: trim(bok.cdisin)
//	@Column(name="CDISIN", length=12) private String cdisin = " ";
}
