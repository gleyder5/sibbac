package sibbac.business.contabilidad.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaAuxiliar;

@Component
public interface CuentaAuxiliarDao extends JpaRepository<CuentaAuxiliar, Long> {
  public List<CuentaAuxiliar> findByTipologiaAndTipoImporte(int tipologia, int tipoImporte);

  @Query("SELECT ca.auxiliar FROM CuentaAuxiliar ca WHERE ca.tipologia=:tipologia AND ca.cuenta.iban=:iban")
  public Auxiliar findByTipologiaAndCuentaIban(@Param("tipologia") int tipologia, @Param("iban") String iban);

  @Query("SELECT ca.auxiliar FROM CuentaAuxiliar ca WHERE ca.tipologia=:tipologia AND ca.tipoImporte=:tipoImporte AND ca.cuenta.iban=:iban")
  public Auxiliar findByTipologiaAndTipoImporteAndCuentaIban(@Param("tipologia") int tipologia,
                                                             @Param("tipoImporte") int tipoImporte,
                                                             @Param("iban") String iban);

  @Query("select a from CuentaAuxiliar a, CuentaBancaria b where a.cuenta=b.id "
  		+ "and a.tipologia=:tipologia and a.tipoImporte in (:lTipoImporte)")
  public List<CuentaAuxiliar> find(@Param("tipologia") int tipologia,@Param("lTipoImporte") List<Integer> lTipoImporte);

  public List<CuentaAuxiliar> findByTipologia(int tipologia);
}
