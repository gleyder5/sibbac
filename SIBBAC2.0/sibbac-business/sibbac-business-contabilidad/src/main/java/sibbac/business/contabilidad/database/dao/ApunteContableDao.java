package sibbac.business.contabilidad.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.CuentaContable;

@Component
public interface ApunteContableDao extends JpaRepository<ApunteContable, Long> {
  List<ApunteContable> findByTipoComprobanteAndContabilizadoOrderByPeriodoComprobanteAsc(int tipoComprobante, boolean contabilizado);

  List<ApunteContable> findByTipoComprobanteAndContabilizadoAndFechaBeforeAndImporteNotOrderByPeriodoComprobanteAsc(Integer tipoComprobante,
                                                                                                                    Boolean contabilizado,
                                                                                                                    Date fecha,
                                                                                                                    BigDecimal importe);

  public List<ApunteContable> findByCuentaContableAndFecha(CuentaContable cuentaContable, Date fecha);

  public List<ApunteContable> findByPeriodoComprobanteAndTipoComprobanteAndNumeroComprobanteAndFechaBefore(int periodo,
                                                                                                           int tipo,
                                                                                                           int numero,
                                                                                                           Date fecha);

  @Query("SELECT max(A.numeroComprobante) FROM ApunteContable A WHERE A.tipoComprobante=:tipoComprobante")
  public Integer findNumeroComprobante(@Param("tipoComprobante") int tipoComprobante);

  int countByCuentaContableId(Long id);

  @Query(nativeQuery = true, value = "select count(*) from tmct0_Apunte_Contable A where A.tipo_Comprobante="
                                     + ":tipoComprobante and TRUNC(fecha,'MONTH')=:hoy")
  int countByTipoMovimientoAndMes(@Param("tipoComprobante") int tipoComprobante, @Param("hoy") Date hoy);
}
