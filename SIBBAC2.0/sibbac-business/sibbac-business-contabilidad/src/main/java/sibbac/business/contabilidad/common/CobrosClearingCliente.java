package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.AlcOrdenesDao;
import sibbac.business.wrappers.database.dao.Tmct0CuentasDeCompensacionDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;

/**
 * @author XI316153
 */
@Service
public class CobrosClearingCliente {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosClearingCliente.class);

  /** Indica si una operación es de una cuenta bruta. */
  private static final String ALC_CUENTA_BRUTA = "2";

  /** Indica si una operacion es de compra. */
  private static final String TPOPER_COMPRA = "C";

  /** Formato de las fechas que se leen del fichero. Ej: 2015-12-24 */
  private static final DateFormat FILE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

  /** Array de tipos de estado que se actualizan durante el proceso. */
  private static TIPO_ESTADO[] tiposEstado;

  private static final String CONCEPTO_APUNTE_COMISION = "Pago comision";
  private static final String CONCEPTO_APUNTE_EFECTIVO_COMPRAS = "Compras liquidadas cliente";
  private static final String CONCEPTO_APUNTE_EFECTIVO_VENTAS = "Ventas liquidadas cliente";
  private static final String CONCEPTO_APUNTE_DIFERENCIA = "Diferencias Norma43";
  private static final String DESCUADRE_TIPO_IMPORTE = "LIQUIDACION_CLEARING";

  private static final String DESCUACRE_COMENTARIO_DIFMAYOR = "Diferencia entre lo devengado y lo cobrado mayor que margen de tolerancia";
  private static final String DESCUACRE_COMENTARIO_TAMREFNOVALIDA = "La referencia no es de 16 posiciones";
  private static final String DESCUACRE_COMENTARIO_REFNOCLEARING = "La referencia no es de un cobro de clearing";
  private static final String DESCUACRE_COMENTARIO_ALCYACOBRADO = "ALC ya cobrado";
  private static final String DESCUACRE_COMENTARIO_ALCRECHAZADO = "ALC rechazado";
  private static final String DESCUACRE_COMENTARIO_ALCNOENCONTRADO = "ALC no encontrado para el ALC_ORDEN obtenido";
  private static final String DESCUACRE_COMENTARIO_ALCORDENNOENCONTRADO = "ALC_ORDEN no encontrado para la referencia recibida";

  private static final Integer CUENTACONTABLE_1133104 = 1133104;
  private static final Integer CUENTACONTABLE_1021101 = 1021101;
  private static final Integer CUENTACONTABLE_2144601 = 2144601;
  private static final Integer CUENTACONTABLE_1122108 = 1122108;
  private static final Integer CUENTACONTABLE_4021102 = 4021102;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private ApunteContableBo apunteContableBo;

  @Autowired
  private ApunteContableAlcDao apunteContableAlcDao;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private AlcOrdenesDao alcOrdenesDao;

  @Autowired
  private Tmct0logBo tmct0logBo;

  @Autowired
  private Tmct0CuentasDeCompensacionDao tmct0CuentasDeCompensacionDao;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;
  
  @Autowired
  private Tmct0alcBo alcBo;

  /** Mapa de cuentas contables. */
  private Map<Integer, CuentaContable> cuentaContableMap;

  /** Lista de descuadres a generar. */
  private List<Norma43DescuadresDTO> norma43DescuadresDTOList;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICO

  static {
    tiposEstado = new TIPO_ESTADO[] { TIPO_ESTADO.CORRETAJE_O_CANON, TIPO_ESTADO.EFECTIVO
    };
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera los apuntes contables de cobro de operaciones de clearing de los movimientos norma43 especificados.
   * 
   * @param norma43MovimientoList Lista de movimientos norma43 a procesar.
   * @param mFichero Mapa de auxiliares contables por fichero norma43 a procesar.
   * @param estadosMap Mapa de estados contables.
   * @param flagNorma43ProcesadoTrue Indica que un movimiento norma43 está procesado.
   * @param cienBigDecimal 100 en BigDecimal.
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void processMovimientosClearing(List<Norma43MovimientoDTO> norma43MovimientoList,
                                         Map<Long, Long> mFichero,
                                         Map<Long, BigDecimal> tolerancePorFichero,
                                         HashMap<String, Tmct0estado> estadosMap,
                                         Integer flagNorma43ProcesadoTrue,
                                         BigDecimal cienBigDecimal) throws Exception {
    LOG.debug("[processMovimientosClearing] Inicio ...");

    init();

    Map<Long, Auxiliar> mAuxiliar = new HashMap<Long, Auxiliar>();
    Auxiliar auxiliar;

    String referencia = null;
    Tmct0alcId alcId;
    Tmct0alc tmct0alc;
    AlcOrdenes alcOrdenes = null;

    for (Norma43MovimientoDTO norma43MovimientoDto : norma43MovimientoList) {
      try {
        LOG.debug("[processMovimientosClearing] Procesando movimiento: {}", norma43MovimientoDto.toString());

        if ((auxiliar = mAuxiliar.get(norma43MovimientoDto.getIdFichero())) == null) {
          mAuxiliar.put(norma43MovimientoDto.getIdFichero(), auxiliar = auxiliarDao.findOne(mFichero.get(norma43MovimientoDto.getIdFichero())));
        }

        referencia = norma43MovimientoDto.getDescReferencia().trim();

        if (referencia.length() == 16) {
          if (referencia.charAt(0) == 'C') {
            alcOrdenes = alcOrdenesDao.findByReferenciaS3(referencia);
            if (alcOrdenes == null) {
              LOG.warn("[processMovimientosClearing] No se han encontrado alcOrdenes para la referencia. Se genera descuadre ...");
              // referenciasDescuadres.put(norma43MovimientoDto, null);
              norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                            DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_ALCORDENNOENCONTRADO));
            } else {
              alcId = alcOrdenes.createAlcIdFromFields();
              tmct0alc = alcBo.findById(alcId);

              if (tmct0alc == null) {
                LOG.warn("[processMovimientosClearing] No se ha encontrado alc para el alcOrden. Se genera descuadre ...");
                // referenciasDescuadres.put(norma43MovimientoDto, null);
                norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                              DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_ALCNOENCONTRADO));
              } else {
                if (tmct0alc.getCdestadoasig().compareTo(ASIGNACIONES.RECHAZADA.getId()) == 0) {
                  LOG.info("[processMovimientosClearing] La referencia pertenece a un ALC rechazado. Se genera descuadre ...");
                  norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto,
                                                                                tmct0alc.getImefeagr()
                                                                                        .subtract(tmct0alc.getImcobrado() != null ? tmct0alc.getImcobrado()
                                                                                                                                 : BigDecimal.ZERO),
                                                                                estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre(),
                                                                                tmct0alc.getEstadocont().getNombre(), tmct0alc.getEstadoentrec()
                                                                                                                              .getNombre(),
                                                                                tmct0alc.getNuorden(), tmct0alc.getNbooking(),
                                                                                tmct0alc.getNucnfclt(), tmct0alc.getNucnfliq(),
                                                                                DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_ALCRECHAZADO));
                } else if (tmct0alc.getEstadocont().getIdestado().compareTo(CONTABILIDAD.COBRADA.getId()) == 0) {
                  LOG.info("[processMovimientosClearing] La referencia ya se encuentra cobrada. Se genera descuadre ...");
                  // referenciasDescuadres.put(norma43MovimientoDto, tmct0alc);
                  norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto,
                                                                                tmct0alc.getImefeagr()
                                                                                        .subtract(tmct0alc.getImcobrado() != null ? tmct0alc.getImcobrado()
                                                                                                                                 : BigDecimal.ZERO),
                                                                                estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre(),
                                                                                tmct0alc.getEstadocont().getNombre(), tmct0alc.getEstadoentrec()
                                                                                                                              .getNombre(),
                                                                                tmct0alc.getNuorden(), tmct0alc.getNbooking(),
                                                                                tmct0alc.getNucnfclt(), tmct0alc.getNucnfliq(),
                                                                                DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_ALCYACOBRADO));
                } else {
                  processMovimiento(referencia, tmct0alc, norma43MovimientoDto, auxiliar,
                                    tolerancePorFichero.get(norma43MovimientoDto.getIdFichero()), estadosMap, cienBigDecimal);
                } // else
              } // else
            } // else
          } else {
            LOG.warn("[processMovimientosClearing] La referencia no es de un cobro de clearing. Se genera descuadre ...");
            // referenciasDescuadres.put(norma43MovimientoDto, null);
            norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                          DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_REFNOCLEARING));
          } // else
        } else {
          LOG.warn("[processMovimientosClearing] La referencia no tiene 16 posiciones. Se genera descuadre ...");
          // referenciasDescuadres.put(norma43MovimientoDto, null);
          norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                        DESCUADRE_TIPO_IMPORTE, null, DESCUACRE_COMENTARIO_TAMREFNOVALIDA));
        } // else if
      } catch (Exception e) {
        LOG.warn("[processMovimientosClearing] Incidencia generando los apuntes de cobro clearing pata cliente ... ", e);
        // referenciasDescuadres.put(norma43MovimientoDto, tmct0alc);
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, null, null, null, null, null, null, null, null,
                                                                      DESCUADRE_TIPO_IMPORTE, null, e.getCause().getMessage()));
        LOG.info("[processMovimientosClearing] Se continua con el siguiente movimiento ...");
      } finally {
        tmct0alc = null;
      } // finally
    } // for (Norma43MovimientoDTO norma43MovimientoDto : norma43MovimientoList)

    // for (Map.Entry<Norma43MovimientoDTO, Tmct0alc> referenciaDescuadre : referenciasDescuadres.entrySet()) {
    // generateDescuadre(referenciaDescuadre.getKey(), referenciaDescuadre.getValue());
    // } // for
    for (Norma43DescuadresDTO norma43DescuadresDTO : norma43DescuadresDTOList) {
      norma43DescuadresDao.save(norma43DescuadresDTO.getDescuadreNorma43());
    } // for

    // Se actualiza el estado del movimiento a procesado
    for (Norma43MovimientoDTO norma43Movimiento : norma43MovimientoList) {
      norma43MovimientoDao.update(norma43Movimiento.getId(), flagNorma43ProcesadoTrue);
    } // for

    LOG.debug("[processMovimientosClearing] Fin ...");
  } // processMovimientosClearing

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Inicializa los objetos que se utilzan durante el proceso.
   */
  private void init() {
    cuentaContableMap = new HashMap<Integer, CuentaContable>();
    // cuentaContableMap.put(CUENTACONTABLE_1133104, cuentaContableDao.findByCuenta(CUENTACONTABLE_1133104));
    // cuentaContableMap.put(CUENTACONTABLE_1021101, cuentaContableDao.findByCuenta(CUENTACONTABLE_1021101));
    // cuentaContableMap.put(CUENTACONTABLE_2144601, cuentaContableDao.findByCuenta(CUENTACONTABLE_2144601));
    // cuentaContableMap.put(CUENTACONTABLE_1122108, cuentaContableDao.findByCuenta(CUENTACONTABLE_1122108));
    // cuentaContableMap.put(CUENTACONTABLE_4021102, cuentaContableDao.findByCuenta(CUENTACONTABLE_4021102));

    norma43DescuadresDTOList = new ArrayList<Norma43DescuadresDTO>();
  } // init

  /**
   * Procesa el movimiento Norma43 de clearing especificado.
   * 
   * @param descReferencia Referencia a cobrar.
   * @param tmct0alc ALC a tratar.
   * @param norma43MovimientoDto Movimiento Norma43 a tratar.
   * @param auxiliar Auxiliar contable para los apuntes a la cuenta de bancos.
   * @param tolerance Margen de tolerancia para la cuenta de bancos del apunte norma43.
   * @param estadosMap Mapa de estados contables.
   * @param cienBigDecimal 100 en BigDecimal.
   * @throws ParseException Si ocurre algún error al generar la fecha de los apuntes contables.
   * @throws SIBBACBusinessException Si ocurre algún error al establecer el estado contable del ALC.
   */
  private void processMovimiento(String descReferencia,
                                 Tmct0alc tmct0alc,
                                 Norma43MovimientoDTO norma43MovimientoDto,
                                 Auxiliar auxiliar,
                                 BigDecimal tolerance,
                                 HashMap<String, Tmct0estado> estadosMap,
                                 BigDecimal cienBigDecimal) throws ParseException, SIBBACBusinessException {
    LOG.debug("[processMovimiento] Inicio ...");

    // Importe recibido
    BigDecimal importeNorma43 = norma43MovimientoDto.getImporte().divide(cienBigDecimal, 6, RoundingMode.HALF_UP);
    BigDecimal porApuntarNorma43 = importeNorma43;
    LOG.debug("[processMovimiento] importeNorma43: '{}'", importeNorma43);
    if (porApuntarNorma43.signum() <= 0) {
      throw new SIBBACBusinessException("El importe del movimiento es menor o igual de cero");
    } // if

    Tmct0alo tmct0alo = tmct0alc.getTmct0alo();

    // Isin de la operación
    String isin = tmct0alo.getCdisin();
    LOG.debug("[processMovimiento] isin: '{}'", isin);

    // Indica si la operación es de compra
    Boolean isCompra = TPOPER_COMPRA.equals(String.valueOf(tmct0alo.getCdtpoper()));
    LOG.debug("[processMovimiento] isCompra: '{}'", isCompra);

    // Indica si la operación está liquidada totalmente
    Boolean isLiquidado = tmct0alc.getEstadoentrec().getIdestado().compareTo(CLEARING.LIQUIDADA_PARCIAL.getId()) != 0;
    LOG.debug("[processMovimiento] isLiquidado: '{}'", isLiquidado);

    // Fecha de los apuntes
    Date fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), FILE_DATE_FORMAT.parse(norma43MovimientoDto.getTradedate()));
    LOG.debug("[processMovimiento] fechaApunte: '{}'", fechaApunte);

    // Importe ya apuntado anteriormente
    BigDecimal yaApuntadoAlc = tmct0alc.getImcobrado() != null ? tmct0alc.getImcobrado() : BigDecimal.ZERO;
    LOG.debug("[processMovimiento] yaApuntadoAlc: '{}'", yaApuntadoAlc);

    // Efectivo a apuntar
    BigDecimal efectivoAlc = tmct0alc.getImefeagr();
    LOG.debug("[processMovimiento] efectivoAlc (imefeagr): '{}'", efectivoAlc);

    // Comisión a apuntar
    BigDecimal comisionAlc = tmct0alc.getImfinsvb();
    LOG.debug("[processMovimiento] comisionAlc (imfinsvb): '{}'", comisionAlc);

    // Total a cobrar/pagar
    // En compras es efectivo + comisión
    // En ventas es efectivo - comisión
    BigDecimal totalAApuntarAlc = BigDecimal.ZERO;
    if (isCompra) {
      totalAApuntarAlc = efectivoAlc.add(comisionAlc);
    } else {
      totalAApuntarAlc = efectivoAlc.subtract(comisionAlc);
    }
    LOG.debug("[processMovimiento] totalAApuntarAlc: '{}'", totalAApuntarAlc);

    // Estado de cobros
    HashMap<String, BigDecimal> estadoCobros = obtenerEstadoCobros(yaApuntadoAlc, efectivoAlc, comisionAlc);

    // Total recibido en el norma43 actual más los posibles norma43 parciales anteriores
    BigDecimal totalRecibidoNorma43 = porApuntarNorma43.add(yaApuntadoAlc);
    LOG.debug("[processMovimiento] totalRecibidoNorma43: '{}'", totalRecibidoNorma43);

    // Positivo si el cliente paga de más
    // Negativo si el cliente paga de menos
    BigDecimal diferencia = totalRecibidoNorma43.subtract(totalAApuntarAlc);
    LOG.debug("[processMovimiento] diferencia: '{}'", diferencia);

    String tipoCuentaCompensacion = String.valueOf(tmct0CuentasDeCompensacionDao.findByCdCodigo(tmct0alc.getTmct0desglosecamaras().get(0)
                                                                                                        .getTmct0infocompensacion()
                                                                                                        .getCdctacompensacion()).getTipoNeteoTitulo()
                                                                                .getId());
    if (diferencia.signum() >= 0 || isLiquidado) {
      // Si nos han pagado de más o la operación ya está liquidada
      if (diferencia.abs().compareTo(tolerance) > 0) {
        // Si la diferencia supera el margen de tolerancia por cuenta bancaria
        norma43DescuadresDTOList.add(CobrosClearing.generateDescuadre(norma43MovimientoDto, totalAApuntarAlc.subtract(yaApuntadoAlc),
                                                                      estadoDao.findOne(tmct0alc.getCdestadoasig()).getNombre(),
                                                                      tmct0alc.getEstadocont().getNombre(), tmct0alc.getEstadoentrec().getNombre(),
                                                                      tmct0alc.getNuorden(), tmct0alc.getNbooking(), tmct0alc.getNucnfclt(),
                                                                      tmct0alc.getNucnfliq(), DESCUADRE_TIPO_IMPORTE, diferencia.abs(),
                                                                      DESCUACRE_COMENTARIO_DIFMAYOR));
        return;
      } // if (diferencia.abs().compareTo(tolerance) > 0)
      generateApuntesEfectivo(fechaApunte, porApuntarNorma43, tmct0alc, isin, isCompra, auxiliar, descReferencia,
                              estadoCobros.get("porCobrarEfectivo"), tipoCuentaCompensacion);
      generateApuntesComision(fechaApunte, estadoCobros.get("porCobrarComision").negate(), tmct0alc, descReferencia);
      generateApunteDiferencia(diferencia, descReferencia, fechaApunte, tipoCuentaCompensacion, tmct0alc, isCompra);
    } else {
      // Si no nos han pagado todo y la operación no está liquidada
      if (porApuntarNorma43.compareTo(estadoCobros.get("porCobrarEfectivo")) > 0) {
        // Si con lo cobrado da para cobrar todo el efectivo pendiente de cobrar
        generateApuntesEfectivo(fechaApunte, porApuntarNorma43, tmct0alc, isin, isCompra, auxiliar, descReferencia,
                                estadoCobros.get("porCobrarEfectivo"), tipoCuentaCompensacion);
        porApuntarNorma43 = porApuntarNorma43.subtract(estadoCobros.get("porCobrarEfectivo"));
        // Hay que controlar el signo de la comisión porque la resta anterior siempre va a ser un número positivo
        generateApuntesComision(fechaApunte, comisionAlc.signum() < 0 ? porApuntarNorma43 : porApuntarNorma43.negate(), tmct0alc, descReferencia);
      } else {
        // Si con lo cobrado no da para cobrar todo el efectivo pendiente de cobrar
        generateApuntesEfectivo(fechaApunte, porApuntarNorma43, tmct0alc, isin, isCompra, auxiliar, descReferencia, porApuntarNorma43,
                                tipoCuentaCompensacion);
        // porApuntarNorma43 = BigDecimal.ZERO; // No hace falta
      } // else
    } // else

    // TODO Hay que revisar los métodos de actualización de estados en la contabilidad para usar la tabla TMCT0_ALC_ESTADOCONT porque fallan más que
    // una escopeta de feria. Con el insertCdestadocont funciona
    if (isLiquidado) {
      LOG.debug("[processMovimiento] Estableciendo estado CONTABILIDAD.COBRADA ...");
      tmct0alcDaoImp.escalaEstadoAlc(tmct0alc, estadosMap.get("estadoContCobrada"), estadosMap.get("enCurso"), tiposEstado);
      alcEstadoContDaoImp.updateOrInsertCdestadocont(estadosMap.get("estadoContCobrada").getIdestado(), tmct0alc.getId().getNbooking(),
                                             tmct0alc.getId().getNuorden(), tmct0alc.getId().getNucnfliq(), tmct0alc.getId().getNucnfclt(),
                                             TIPO_ESTADO.EFECTIVO.getId());
    } else {
      LOG.debug("[processMovimiento] Estableciendo estado CONTABILIDAD.PARCIALMENTE_COBRADA ...");
      tmct0alcDaoImp.escalaEstadoAlc(tmct0alc, estadosMap.get("estadoContParcialmenteCobrada"), estadosMap.get("enCurso"), tiposEstado);
      alcEstadoContDaoImp.updateOrInsertCdestadocont(estadosMap.get("estadoContParcialmenteCobrada").getIdestado(), tmct0alc.getId().getNbooking(),
                                             tmct0alc.getId().getNuorden(), tmct0alc.getId().getNucnfliq(), tmct0alc.getId().getNucnfclt(),
                                             TIPO_ESTADO.EFECTIVO.getId());
    } // else

    tmct0alc.setImcobrado(yaApuntadoAlc.add(importeNorma43));

    try {
      tmct0logBo.insertarRegistro(tmct0alc,
                                  null,
                                  null,
                                  null,
                                  tmct0alc.getEstadoentrec(),
                                  "Recibido Norma43. Cta: "
                                      + String.format("%04d", norma43MovimientoDto.getEntidad())
                                              .concat(String.format("%04d", norma43MovimientoDto.getOficina()))
                                              .concat(String.format("%010d", norma43MovimientoDto.getCuenta())) + ". Importe Norma43: '"
                                      + importeNorma43 + "'. Total cobrado: '" + tmct0alc.getImcobrado() + "'");
    } catch (Exception ex) {
      LOG.warn("[processMovimiento] Error insertando en Tmct0log ... ", ex.getMessage());
    } // catch

    LOG.debug("[processMovimiento] Fin ...");
  } // processMovimiento

  /**
   * Genera los apuntes contables de efectivo.
   * 
   * @param fecha Fecha del apunte contable.
   * @param importeCobrado Importe cobrado.
   * @param tmct0alc ALC al que asignar los apuntes contables creados.
   * @param isin Isin de la operación.
   * @param isCompra Indica si la operación es una compra.
   * @param auxiliar Auxiliar contable para los apuntes a la cuenta de bancos.
   * @param descReferencia Referencia S3 de la operación.
   * @param importeDevengado Importe devengado.
   * @param tipoCuentaCompensacion Tipo de cuenta de compensación.
   */
  private void generateApuntesEfectivo(Date fecha,
                                       BigDecimal importeCobrado,
                                       Tmct0alc tmct0alc,
                                       String isin,
                                       Boolean isCompra,
                                       Auxiliar auxiliar,
                                       String descReferencia,
                                       BigDecimal importeDevengado,
                                       String tipoCuentaCompensacion) {
    LOG.debug("[generateApuntesEfectivo] Inicio ...");
    LOG.debug("[generateApuntesEfectivo] Apuntando: [ fecha: '{}'; importeCobrado: '{}'; importeDevengado: '{}'; ALC: '{}' ]", fecha, importeCobrado,
              importeDevengado, tmct0alc.getId().toString());

    ApunteContable apunteEfectivo = new ApunteContable();
    ApunteContable apunteNeto = new ApunteContable();

    apunteEfectivo.setFecha(fecha);
    apunteEfectivo.setContabilizado(Boolean.FALSE);
    apunteEfectivo.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteEfectivo.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
    apunteEfectivo.setTipoMovimiento(ALC_CUENTA_BRUTA.compareTo(tipoCuentaCompensacion) == 0 ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                            : TIPO_APUNTE.EFECTIVO_NETO.getId());

    apunteNeto.setFecha(fecha);
    apunteNeto.setContabilizado(Boolean.FALSE);
    apunteNeto.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteNeto.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
    apunteNeto.setTipoMovimiento(ALC_CUENTA_BRUTA.compareTo(tipoCuentaCompensacion) == 0 ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                        : TIPO_APUNTE.EFECTIVO_NETO.getId());

    if (isCompra) {
      if (importeDevengado.signum() != 0) {
        apunteEfectivo.setCuentaContable(findCuentaContable(CUENTACONTABLE_1133104));
        apunteEfectivo.setDescripcion(tmct0alc.getFeejeliq() + "/Compra/" + isin);
        apunteEfectivo.setConcepto(CONCEPTO_APUNTE_EFECTIVO_COMPRAS);
        apunteEfectivo.setImporte(importeDevengado.negate());
        apunteEfectivo.setApunte(TIPO_CUENTA.HABER.getId());
        apunteContableBo.save(apunteEfectivo);
        apunteContableAlcDao.save(new ApunteContableAlc(apunteEfectivo, tmct0alc, importeDevengado.negate()));
      }

      if (importeCobrado.signum() != 0) {
        apunteNeto.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
        apunteNeto.setAuxiliar(auxiliar);
        apunteNeto.setDescripcion(descReferencia);
        apunteNeto.setConcepto(CONCEPTO_APUNTE_EFECTIVO_COMPRAS);
        apunteNeto.setImporte(importeCobrado);
        apunteNeto.setApunte(TIPO_CUENTA.DEBE.getId());
        apunteContableBo.save(apunteNeto);
        apunteContableAlcDao.save(new ApunteContableAlc(apunteNeto, tmct0alc, importeCobrado));
      }
    } else {
      if (importeDevengado.signum() != 0) {
        apunteEfectivo.setCuentaContable(findCuentaContable(CUENTACONTABLE_2144601));
        apunteEfectivo.setDescripcion(tmct0alc.getFeejeliq() + "/Venta/" + isin);
        apunteEfectivo.setConcepto(CONCEPTO_APUNTE_EFECTIVO_VENTAS);
        apunteEfectivo.setImporte(importeDevengado);
        apunteEfectivo.setApunte(TIPO_CUENTA.DEBE.getId());
        apunteContableBo.save(apunteEfectivo);
        apunteContableAlcDao.save(new ApunteContableAlc(apunteEfectivo, tmct0alc, importeDevengado));
      }

      if (importeCobrado.signum() != 0) {
        apunteNeto.setCuentaContable(findCuentaContable(CUENTACONTABLE_1021101));
        apunteNeto.setAuxiliar(auxiliar);
        apunteNeto.setDescripcion(descReferencia);
        apunteNeto.setConcepto(CONCEPTO_APUNTE_EFECTIVO_VENTAS);
        apunteNeto.setImporte(importeCobrado.negate());
        apunteNeto.setApunte(TIPO_CUENTA.HABER.getId());
        apunteContableBo.save(apunteNeto);
        apunteContableAlcDao.save(new ApunteContableAlc(apunteNeto, tmct0alc, importeCobrado.negate()));
      }
    } // else
    LOG.debug("[generateApuntesEfectivo] Fin ...");
  } // generateApuntesEfectivo

  /**
   * Genera el apunte contable de comisión.
   * 
   * @param fecha Fecha del apunte contable.
   * @param importeComision Importe a contabilizar.
   * @param tmct0alc ALC al que asignar los apuntes contables creados.
   * @param descReferencia Referencia S3 de la operación.
   */
  private void generateApuntesComision(Date fecha, BigDecimal importeComision, Tmct0alc tmct0alc, String descReferencia) {
    LOG.debug("[generateApuntesComision] Inicio ...");

    if (importeComision.signum() != 0) {
      LOG.debug("[generateApuntesComision] Apuntando: [ fecha: '{}'; importeComision: '{}'; ALC: '{}'; descReferencia: '{}']", fecha,
                importeComision, tmct0alc.getId().toString(), descReferencia);

      ApunteContable apunte_haber = new ApunteContable();
      apunte_haber.setCuentaContable(findCuentaContable(CUENTACONTABLE_1122108));
      apunte_haber.setConcepto(CONCEPTO_APUNTE_COMISION);
      apunte_haber.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
      apunte_haber.setDescripcion(descReferencia);
      apunte_haber.setFecha(fecha);
      apunte_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_haber.setContabilizado(Boolean.FALSE);
      apunte_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_haber.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
      apunte_haber.setImporte(importeComision);
      apunteContableBo.save(apunte_haber);
      apunteContableAlcDao.save(new ApunteContableAlc(apunte_haber, tmct0alc, importeComision));
    } // if

    LOG.debug("[generateApuntesComision] Fin ...");
  } // generateApuntesComision

  /**
   * Genera el apunte de diferencia entre lo cobrado y lo que se tenía que cobrar.
   * 
   * @param diferencia Importe del apunte.
   * @param descReferencia Referencia del apunte.
   * @param fecha Fecha del apunte.
   * @param tipoCuentaCompensacion Tipo de cuenta de compensación.
   * @param tmct0alc ALC.
   * @param isCompra Inidica si la operación es de compra.
   */
  private void generateApunteDiferencia(BigDecimal diferencia,
                                        String descReferencia,
                                        Date fecha,
                                        String tipoCuentaCompensacion,
                                        Tmct0alc tmct0alc,
                                        Boolean isCompra) {
    LOG.debug("[generateApunteDiferencia] Inicio ...");
    if (diferencia.signum() != 0) {
      LOG.debug("[generateApunteDiferencia] Apuntando: [ fecha: '{}'; diferencia: '{}'; descReferencia: '{}'; ALC: '{}'; isCompra: '{}' ]", fecha,
                diferencia, descReferencia, tmct0alc.getId().toString(), isCompra);

      ApunteContable apunteDiferencia = new ApunteContable();
      apunteDiferencia.setCuentaContable(findCuentaContable(CUENTACONTABLE_4021102));
      apunteDiferencia.setDescripcion(descReferencia);
      apunteDiferencia.setConcepto(CONCEPTO_APUNTE_DIFERENCIA);
      apunteDiferencia.setFecha(fecha);
      apunteDiferencia.setContabilizado(Boolean.FALSE);
      apunteDiferencia.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteDiferencia.setTipoComprobante(TIPO_COMPROBANTE.LIQUIDACION_CLEARING.getTipo());
      apunteDiferencia.setTipoMovimiento(ALC_CUENTA_BRUTA.equals(tipoCuentaCompensacion) ? TIPO_APUNTE.EFECTIVO_BRUTO.getId()
                                                                                        : TIPO_APUNTE.EFECTIVO_NETO.getId());

      // En COMPRAS al DEBE en positivo si el cliente paga de menos (el importe Norma43 es menor que la suma de imefeagr+imfinsvb)
      // En COMPRAS al HABER en negativo si el cliente paga de más (el importe Norma43 es mayor que la suma de imefeagr+imfinsvb)
      // En VENTAS al HABER en negativo si el cliente paga de menos (el importe Norma43 es menor que la menos de imefeagr-imfinsvb)
      // En VENTAS al DEBE en positivo si el cliente paga de más (el importe Norma43 es mayor que la menos de imefeagr-imfinsvb)
      if (isCompra) {
        if (diferencia.signum() > 0) {
          apunteDiferencia.setImporte(diferencia.negate());
          apunteDiferencia.setApunte(TIPO_CUENTA.HABER.getId());
        } else if (diferencia.signum() < 0) {
          apunteDiferencia.setImporte(diferencia.negate());
          apunteDiferencia.setApunte(TIPO_CUENTA.DEBE.getId());
        }
      } else {
        if (diferencia.signum() > 0) {
          apunteDiferencia.setImporte(diferencia);
          apunteDiferencia.setApunte(TIPO_CUENTA.DEBE.getId());
        } else if (diferencia.signum() < 0) {
          apunteDiferencia.setImporte(diferencia);
          apunteDiferencia.setApunte(TIPO_CUENTA.HABER.getId());
        }
      }
      LOG.debug("[generateApunteDiferencia] diferencia a apuntar: " + diferencia);

      apunteContableBo.save(apunteDiferencia);
      apunteContableAlcDao.save(new ApunteContableAlc(apunteDiferencia, tmct0alc, apunteDiferencia.getImporte()));
    } // if (diferencia.signum() != 0)

    LOG.debug("[generateApunteDiferencia] Fin ...");
  } // generateApunteDiferencia

  /**
   * Obtiene un mapa con el estado de cobro de los distintos importes a cobrar.
   * 
   * @param yaApuntadoAlc Importe ya cobrado.
   * @param efectivoAlc Importe de efectivo a cobrar.
   * @param comisionAlc Importe de comisión a cobrar.
   * @return <code>java.util.HashMap<String, BigDecimal> - </code>Mapa con los estados de los cobros.
   */
  private HashMap<String, BigDecimal> obtenerEstadoCobros(BigDecimal yaApuntadoAlc, BigDecimal efectivoAlc, BigDecimal comisionAlc) {
    LOG.debug("[obtenerEstadoCobros] Inicio ...");
    HashMap<String, BigDecimal> estadoCobros = new HashMap<String, BigDecimal>(4);

    if (yaApuntadoAlc.compareTo(efectivoAlc) <= 0) {
      estadoCobros.put("yaCobradoEfectivo", yaApuntadoAlc);
      estadoCobros.put("yaCobradoComision", BigDecimal.ZERO);

      estadoCobros.put("porCobrarEfectivo", efectivoAlc.subtract(yaApuntadoAlc));
      estadoCobros.put("porCobrarComision", comisionAlc);
    } else {
      estadoCobros.put("yaCobradoEfectivo", efectivoAlc);
      estadoCobros.put("porCobrarEfectivo", BigDecimal.ZERO);

      if (comisionAlc.signum() != 0 && yaApuntadoAlc.compareTo(efectivoAlc.add(comisionAlc.abs())) < 0) {
        // Si hay comision para cobrar y no la he cobrado toda
        estadoCobros.put("yaCobradoComision", yaApuntadoAlc.subtract(efectivoAlc));
        if (comisionAlc.signum() > 0) {
          // Si la comisión es positiva
          estadoCobros.put("porCobrarComision", comisionAlc.subtract(estadoCobros.get("yaCobradoComision")));
        } else {
          // Si la comisión es negativa
          estadoCobros.put("porCobrarComision", comisionAlc.add(estadoCobros.get("yaCobradoComision")));
        } // else
      } else {
        estadoCobros.put("yaCobradoComision", comisionAlc);
        estadoCobros.put("porCobrarComision", BigDecimal.ZERO);
      } // else
    } // else

    LOG.debug("[obtenerEstadoCobros] estadoCobros: '{}'", estadoCobros.toString());
    return estadoCobros;
  } // obtenerEstadoCobros

  /**
   * Busca el objeto <code>CuentaContable</code> cuyo número de cuenta se ha especificado.
   *
   * @param cuenta Número de cuenta a buscar.
   * @return <code>CuentaContable - </code>Cuenta contable obtenida.
   */
  private CuentaContable findCuentaContable(Integer cuenta) {
    CuentaContable salida;
    if ((salida = cuentaContableMap.get(cuenta)) == null) {
      cuentaContableMap.put(cuenta, salida = cuentaContableDao.findByCuenta(cuenta));
    } // if
    return salida;
  } // findCuentaContable

} // CobrosClearingCliente
