package sibbac.business.contabilidad.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.CuentaBancaria}.
 */
@Table(name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.CUENTA_BANCARIA)
@Entity
@Component
@XmlRootElement
@Audited
public class CuentaBancaria extends ATableAudited<CuentaBancaria> {
	private static final long serialVersionUID = 4268362214822547118L;
	private static final NumberFormat nF2 = new DecimalFormat("00");
	private static final NumberFormat nF4 = new DecimalFormat("0000");
	private static final NumberFormat nF10 = new DecimalFormat("0000000000");
	private static final int[] tablaPesos = {6,3,7,9,10,5,8,4,2,1};
    private static final BigInteger BI97 = new BigInteger("97");

	
	@Column(name="IBAN", length=34, unique=true)
	private String iban;
	@Transient Integer entidad;
	@Transient Integer sucursal;
	@Transient Long cuenta;
	 
	@Column(name="TOLERANCE", precision = 18, scale = 6)
	private BigDecimal tolerance;

	public CuentaBancaria() {
	}

	public CuentaBancaria( String iban ) {
		this.iban = iban;
	}

	public CuentaBancaria( Integer entidad, Integer sucursal, Long cuenta ) {
		setEntidad( entidad );
		setSucursal( sucursal );
		setCuenta( cuenta );
	}

	public String getIban() {
	  if (iban == null){
	    putIban();
	  }
		return iban;
	}

	public String getCCC() {
	  String iban = getIban();
	  if ("ES".equals(iban.substring(0,2))) {
	  	return iban.substring(4);
	  }
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Integer getEntidad() {
		return iban.startsWith("ES")? Integer.valueOf(iban.substring(4, 8)): null;
	}

	public void setEntidad( Integer entidad ) {
		this.entidad = entidad;
		putIban();
	}

	public Integer getSucursal() {
		return iban.startsWith("ES")? Integer.valueOf(iban.substring(8, 12)): null;
	}

	public void setSucursal( Integer sucursal ) {
		this.sucursal = sucursal;
		putIban();
	}

	public Long getCuenta() {
		return iban.startsWith("ES")? Long.valueOf(iban.substring(14, 24)): null;
	}

	public void setCuenta(Long cuenta) {
		this.cuenta = cuenta;
		putIban();
	}

	private static String calculaDCParcial(String cadena) {
		int dcParcial = 0;
		int suma = 0;
		int i;
		for(i=0; i<cadena.length(); i++) {
			suma += (cadena.charAt(cadena.length()-1-i)-48)*tablaPesos[i];
		}
		dcParcial = 11 - suma % 11;
		if(dcParcial==11)
			dcParcial=0;
		else if(dcParcial==10)
			dcParcial=1;
		return String.valueOf(dcParcial);
	}

	void putIban() {
		if (entidad!=null && sucursal!=null && cuenta!=null) {
			iban = getIban("ES", entidad, sucursal, cuenta);
		}
	}

	static String getCCC(Integer entidad,Integer sucursal,Long cuenta) {
		String sEntidad = nF4.format(entidad);
		String sSucursal = nF4.format(sucursal);
		String sCuenta = nF10.format(cuenta);
		return sEntidad + sSucursal + calculaDCParcial(sEntidad + sSucursal) + calculaDCParcial(sCuenta) + sCuenta;
	}

	static String getIban(String codigoPais,Integer entidad,Integer sucursal,Long cuenta) {
		String ccc = getCCC(entidad,sucursal,cuenta);
		return codigoPais + nF2.format(98 - new BigInteger(ccc + damePesoIBAN(codigoPais.charAt(0))
				+ damePesoIBAN(codigoPais.charAt(1)) + "00").mod(BI97).intValue()) + ccc;
	}

    static String damePesoIBAN(char letra) {
        return String.valueOf(Character.toUpperCase(letra)-55);
    }

    /**
     * @return the tolerance
     */
    public BigDecimal getTolerance() {
      return tolerance;
    }

    /**
     * @param tolerance the tolerance to set
     */
    public void setTolerance(BigDecimal tolerance) {
      this.tolerance = tolerance;
    }
}
