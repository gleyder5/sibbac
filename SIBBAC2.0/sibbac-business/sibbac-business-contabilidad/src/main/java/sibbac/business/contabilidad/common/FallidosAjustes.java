package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.CuentaAuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.CuentaAuxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fallidos.database.dao.FixmlFallidoDao;
import sibbac.business.fallidos.database.dao.MovimientoFallidoDao;
import sibbac.business.fallidos.database.model.MovimientoFallido;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPOLOGIA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_IMPORTE;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.MovimientosFallidos;

/**
 * Contiene la lógica de generación de apuntes contables de movimientos de fallidos, afectaciones y ajustes por eventos financieros.
 * 
 * @author XI316153
 */
@Component
public class FallidosAjustes {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FallidosAjustes.class);

  private static final List<Integer> tiposImporteList = new ArrayList<Integer>();

  /** FIXME No habría que hardcodear esta cuenta. Para salir del paso hasta que sepamos como vamos a recibir los cobros de alta de colateral. */
  private static final String CUENTABANCARIA_COBRO_COLATERAL = "ES5200385777180016008720";

  private static final String ILIQDCV_START_CHARS = "IBO";
  private static final String CONCEPTO_ALTACOLATERAL = "Alta colateral";
  private static final String CONCEPTO_BAJACOLATERAL = "Baja colateral";
  private static final String CONCEPTO_AJUSTECOLATERAL = "Ajuste colateral";
  private static final String CONCEPTO_DEVENGO_INC = "Devengo intereses colateral";
  private static final String CONCEPTO_DEVENGO_INP = "Devengo intereses prestamo";
  private static final String CONCEPTO_DEVENGO_INP_CLIENTE = "Devengo intereses prestamo cliente";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0_NORMA43_MOVIMIENTO</code>. */
  @Autowired
  private Norma43MovimientoDao norma43MovimientoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_FIXML_FALLIDO</code>. */
  @Autowired
  private FixmlFallidoDao fixmlFallidoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_CUENTA_CONTABLE</code>. */
  @Autowired
  private CuentaContableDao cuentaContableDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_APUNTE_CONTABLE</code>. */
  @Autowired
  private ApunteContableBo apunteContableBo;

  /** Objeto para el acceso a la tabla <code>TMCT0_MOVIMIENTOS_FALLIDOS</code>. */
  @Autowired
  private MovimientoFallidoDao movimientoFallidoDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_CUENTA_AUXILIAR</code>. */
  @Autowired
  private CuentaAuxiliarDao cuentaAuxiliarDao;

  /** Objeto para el acceso a la tabla <code>TMCT0_CUENTA_AUXILIAR</code>. */
  private HashMap<String, CuentaAuxiliar> auxiliarPorCuenta;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>1021101</code>. */
  private CuentaContable cuentaContable_1021101;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>1025401</code>. */
  private CuentaContable cuentaContable_1025401;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>1027105</code>. */
  private CuentaContable cuentaContable_1027105;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>4012105</code>. */
  private CuentaContable cuentaContable_4012105;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>3019104</code>. */
  private CuentaContable cuentaContable_3019104;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>2132104</code>. */
  private CuentaContable cuentaContable_2132104;

  /** Objeto <code>CuentaContable</code> para la cuenta <code>1133106</code>. */
  private CuentaContable cuentaContable_1133106;

  /** Margen de diferencia aceptado entre el importe Norma43 y el de los movvimientos fallidos. */
  @Value("${norma43.margen.descuadres}")
  private BigDecimal margenDescuadres;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICA

  static {
    tiposImporteList.add(TIPO_IMPORTE.Efectivo.getTipo());
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Procesa los movimientos de fallidos, afectaciones y ajustes por eventos financieros de las operaciones fallidas liquidadas y no contabilizadas.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void generateApuntesFallidosAjustes() {
    LOG.debug("[generateApuntesFallidosAjustes] Inicio ...");

    cuentaContable_1021101 = cuentaContableDao.findByCuenta(1021101);
    cuentaContable_1025401 = cuentaContableDao.findByCuenta(1025401);
    cuentaContable_1027105 = cuentaContableDao.findByCuenta(1027105);
    cuentaContable_4012105 = cuentaContableDao.findByCuenta(4012105);
    cuentaContable_3019104 = cuentaContableDao.findByCuenta(3019104);
    cuentaContable_2132104 = cuentaContableDao.findByCuenta(2132104);
    cuentaContable_1133106 = cuentaContableDao.findByCuenta(1133106);

    auxiliarPorCuenta = new HashMap<String, CuentaAuxiliar>();
    for (CuentaAuxiliar ctaAuxiliar : cuentaAuxiliarDao.find(TIPOLOGIA.Clearing.getTipo(), tiposImporteList)) {
      auxiliarPorCuenta.put(ctaAuxiliar.getCuenta().getIban(), ctaAuxiliar);
    } // for

    List<String> tipoMovimientosList = new ArrayList<String>();

    // 1.- Se procesan las altas de PUI para generar los apuntes contables de alta de colateral
    // tipoMovimientosList.add(MovimientosFallidos.APU.getTipo());
    // tipoMovimientosList.add(MovimientosFallidos.ACO.getTipo());
    // procesarAltaColateral(tipoMovimientosList);

    // 2.- Se procesan las bajas de PUI para generar los apuntes contables de baja de colateral
    // procesarBajaColateral();

    // 3.- Devengo de los intereses del colateral y comisiones prestamo
    tipoMovimientosList.clear();
    tipoMovimientosList.add(MovimientosFallidos.INC.getTipo());
    tipoMovimientosList.add(MovimientosFallidos.INP.getTipo());
    generarApuntesDevengoIntereses(tipoMovimientosList);

    // 3.- Se procesan los movimientos norma43 de fallidos sin procesar
    // List<Norma43Movimiento> norma43MovimientosList = null;
    // try {
    // norma43MovimientosList = norma43MovimientoDao.findByProcesadoAndTipologiaAndTipoImporteOrderByFechaInicial(Boolean.FALSE,
    // TIPOLOGIA.Clearing.getTipo(),
    // TIPO_IMPORTE.Efectivo.getTipo());
    // } catch (Exception e) {
    // LOG.error("[generateApuntesFallidosAjustes] Error obteniendo datos Norma43 de la base de datos ... " + e.getMessage(), e);
    // throw e;
    // } // catch
    //
    // String iliqecc;
    // BigDecimal importeMovimientoFallido;
    // BigDecimal importeMovimientoNorma43;
    //
    // List<MovimientoFallido> movimientosFallidosList;
    //
    // for (Norma43Movimiento norma43Movimiento : norma43MovimientosList) {
    // try {
    // LOG.info("[generateApuntesFallidosAjustes] Procesando movimiento norma43: " + norma43Movimiento.toString());
    //
    // importeMovimientoNorma43 = norma43Movimiento.getImporte().divide(new BigDecimal(100));
    //
    // if (norma43Movimiento.getDescReferencia().startsWith(MovimientosFallidos.ACO.getTipo())) {
    // // Movimiento de cobro/pago por actualización del colateral
    // iliqecc = fixmlFallidoDao.findByIliqdcv(ILIQDCV_START_CHARS.concat(norma43Movimiento.getDescReferencia().substring(3)));
    // if (iliqecc != null && !iliqecc.trim().isEmpty()) {
    // tipoMovimientosList.clear();
    // tipoMovimientosList.add(MovimientosFallidos.ACO.getTipo());
    // tipoMovimientosList.add(MovimientosFallidos.BCO.getTipo());
    //
    // movimientosFallidosList = movimientoFallidoDao.findByTipoMovimientoContabilizadoAndIliqecc(Boolean.FALSE, iliqecc, tipoMovimientosList);
    // if (movimientosFallidosList.size() >= 2) {
    // // Hay por lo menos dos movimientos en parejas BCO-ACO, se procesan los dos primeros que haya devuelto la consulta
    // if (movimientosFallidosList.get(0).getCdtipo().compareTo(MovimientosFallidos.BCO.getTipo()) == 0) {
    // // Todo parece correcto, se hacen los pauntes
    // importeMovimientoFallido = movimientosFallidosList.get(0).getSaldoAfectado().abs()
    // .subtract(movimientosFallidosList.get(1).getSaldoAfectado().abs());
    // generarApuntesAjusteColateral(importeMovimientoNorma43, importeMovimientoFallido, movimientosFallidosList.get(0));
    // } else {
    // // TODO Generar descuadre
    // } // else
    // } else {
    // // TODO Generar descuadre
    // } // else
    // } else {
    // // TODO Generar descuadre
    // } // else
    // }
    // } catch (Exception e) {
    // LOG.warn("[generateApuntesFallidosAjustes] Incidencia procesando movimiento Norma43 '" + norma43Movimiento + "' ... " + e.getMessage()
    // + " ... ", e);
    // // TODO Generar descuadre
    // }
    // } // for (Norma43Movimiento norma43Movimiento : norma43MovimientosList)

    LOG.debug("[generateApuntesFallidosAjustes] Fin ...");
  } // generateApuntesFallidosAjustes

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Procesa los movimientos fallidos necesarios para generar los apuntes contables de alta de colateral.
   * 
   * @param tipoMovimientosList Lista de movimientos de fallidos a procesar.
   */
  private void procesarAltaColateral(List<String> tipoMovimientosList) {
    LOG.debug("[procesarAltaColateral] Inicio ...");

    // Se obtendrán 1 movimiento APU y n movimientos ACO, el primero de los ACO, que será el siguiente al APU, es el de alta de colateral que es el
    // que se debe procesar
    List<MovimientoFallido> movimientosFallidosList = movimientoFallidoDao.findByTipoMovimientoAndContabilizado(Boolean.FALSE, tipoMovimientosList);

    if (movimientosFallidosList != null && !movimientosFallidosList.isEmpty()) {
      Boolean checkACO = Boolean.FALSE;

      for (MovimientoFallido movimientoFallido : movimientosFallidosList) {
        LOG.debug("[procesarAltaColateral] Procesando movimiento: {}", movimientoFallido);
        if (movimientoFallido.getCdtipo().compareTo(MovimientosFallidos.APU.getTipo()) == 0) {
          LOG.debug("[procesarAltaColateral] Contabilizando APU ...");
          movimientoFallido.setContabilizado(Boolean.TRUE);
          movimientoFallidoDao.save(movimientoFallido);
          checkACO = Boolean.TRUE;
        } else if (movimientoFallido.getCdtipo().compareTo(MovimientosFallidos.ACO.getTipo()) == 0 && checkACO) {
          LOG.debug("[procesarAltaColateral] Contabilizando ACO ...");
          generarApuntesAltaColateral(movimientoFallido);
          movimientoFallido.setContabilizado(Boolean.TRUE);
          movimientoFallidoDao.save(movimientoFallido);
          checkACO = Boolean.FALSE;
        }
      } // for (MovimientoFallido movimientoFallido : movimientosFallidosList)

    } // if (movFallidos != null && !movFallidos.isEmpty())
    LOG.debug("[procesarAltaColateral] Fin ...");
  } // procesarAltaColateral

  /**
   * Genera los apuntes contables de alta de colateral.
   * 
   * @param movimientoFallido Movimiento fallido generado con el alta del colateral del que extraer los datos necesarios para realizar los apuntes
   *          contables.
   */
  private void generarApuntesAltaColateral(MovimientoFallido movimientoFallido) {
    LOG.debug("[generarApuntesAltaColateral] Inicio ...");
    ApunteContable apunteAltaColateral;

    apunteAltaColateral = new ApunteContable();
    apunteAltaColateral.setCuentaContable(cuentaContable_1025401);
    apunteAltaColateral.setDescripcion(FormatDataUtils.convertDateToString(movimientoFallido.getTradeDt(), FormatDataUtils.DATE_FORMAT) + "/"
                                       + movimientoFallido.getIsin());
    apunteAltaColateral.setConcepto(CONCEPTO_ALTACOLATERAL);
    apunteAltaColateral.setImporte(movimientoFallido.getSaldoAfectado().abs());
    apunteAltaColateral.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), movimientoFallido.getTrxTm()));
    apunteAltaColateral.setApunte(TIPO_CUENTA.DEBE.getId());
    apunteAltaColateral.setContabilizado(Boolean.FALSE);
    apunteAltaColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteAltaColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
    apunteAltaColateral.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
    apunteContableBo.save(apunteAltaColateral);

    apunteAltaColateral = new ApunteContable();
    apunteAltaColateral.setCuentaContable(cuentaContable_1021101);
    apunteAltaColateral.setDescripcion(FormatDataUtils.convertDateToString(movimientoFallido.getTradeDt(), FormatDataUtils.DATE_FORMAT) + "/"
                                       + movimientoFallido.getIsin());
    apunteAltaColateral.setConcepto(CONCEPTO_ALTACOLATERAL);
    apunteAltaColateral.setImporte(movimientoFallido.getSaldoAfectado());
    apunteAltaColateral.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), movimientoFallido.getTrxTm()));
    apunteAltaColateral.setApunte(TIPO_CUENTA.HABER.getId());
    apunteAltaColateral.setContabilizado(Boolean.FALSE);
    apunteAltaColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunteAltaColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
    apunteAltaColateral.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
    apunteAltaColateral.setAuxiliar(auxiliarPorCuenta.get(CUENTABANCARIA_COBRO_COLATERAL).getAuxiliar());
    apunteContableBo.save(apunteAltaColateral);

    LOG.debug("[generarApuntesAltaColateral] Fin ...");
  } // generarApuntesAltaColateral

  /**
   * Procesa los movimientos fallidos necesarios para generar los apuntes contables de devengo de los intereses del colateral y las comisiones del
   * prestamo.
   * 
   * @param tipoMovimientosList Lista de movimientos de fallidos a procesar.
   */
  private void generarApuntesDevengoIntereses(List<String> tipoMovimientosList) {
    LOG.debug("[generarApuntesDevengoIntereses] Inicio ...");

    // // Los apuntes de devengo de intereses se hacen para el primer día del mes siguiente
    // Calendar fechaApuntesDevengo = GregorianCalendar.getInstance();
    // fechaApuntesDevengo.setLenient(true);
    // fechaApuntesDevengo.set(Calendar.DAY_OF_MONTH, fechaApuntesDevengo.getActualMaximum(Calendar.DAY_OF_MONTH));
    //
    // // Los apuntes de cobro de intereses se hacen para el primer día del mes siguiente
    // Calendar fechaApuntesCobro = GregorianCalendar.getInstance();
    // fechaApuntesCobro.setLenient(true);
    // if (fechaApuntesCobro.get(Calendar.MONTH) == 11) {
    // // Estamos en Diciembre, fecha a establecer 1 de Enero del año siguiente
    // fechaApuntesCobro.add(Calendar.YEAR, 1);
    // fechaApuntesCobro.set(Calendar.MONTH, 0);
    // fechaApuntesCobro.set(Calendar.DAY_OF_MONTH, 1);
    // } else {
    // fechaApuntesCobro.add(Calendar.MONTH, 1);
    // fechaApuntesCobro.set(Calendar.DAY_OF_MONTH, 1);
    // } // else

    List<MovimientoFallido> movimientosFallidosList = movimientoFallidoDao.findByTipoMovimientoAndContabilizado(Boolean.FALSE, tipoMovimientosList);

    if (movimientosFallidosList != null && !movimientosFallidosList.isEmpty()) {

      ApunteContable apunteIntereses;
      String descripcionMovimiento = null;
      Date fechaApunte;

      for (MovimientoFallido movimientoFallido : movimientosFallidosList) {
        descripcionMovimiento = FormatDataUtils.convertDateToString(movimientoFallido.getTradeDt(), FormatDataUtils.DATE_FORMAT) + "/"
                                + movimientoFallido.getIsin() + "/" + movimientoFallido.getAliasCliente() + "/" + movimientoFallido.getDescrali()
                                + "/" + movimientoFallido.getNumero_operacion_fallida();
        fechaApunte = apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), movimientoFallido.getTrxTm());

        if (movimientoFallido.getCdtipo().compareTo(MovimientosFallidos.INC.getTipo()) == 0) {
          // Apuntes contables pata mercado
          apunteIntereses = new ApunteContable();
          apunteIntereses.setCuentaContable(cuentaContable_1027105);
          apunteIntereses.setDescripcion(descripcionMovimiento);
          apunteIntereses.setConcepto(CONCEPTO_DEVENGO_INC);
          apunteIntereses.setImporte(movimientoFallido.getSaldoAfectado());
          apunteIntereses.setFecha(fechaApunte);
          if (movimientoFallido.getSaldoAfectado().signum() >= 0) {
            apunteIntereses.setApunte(TIPO_CUENTA.DEBE.getId());
          } else {
            apunteIntereses.setApunte(TIPO_CUENTA.HABER.getId());
          }
          apunteIntereses.setContabilizado(Boolean.FALSE);
          apunteIntereses.setOperativa(Constantes.OPERATIVA_APUNTE);
          apunteIntereses.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
          apunteIntereses.setTipoMovimiento(TIPO_APUNTE.INTERES_COLATERAL.getId());
          apunteContableBo.save(apunteIntereses);

          apunteIntereses = new ApunteContable();
          apunteIntereses.setCuentaContable(cuentaContable_4012105);
          apunteIntereses.setDescripcion(descripcionMovimiento);
          apunteIntereses.setConcepto(CONCEPTO_DEVENGO_INC);
          apunteIntereses.setImporte(movimientoFallido.getSaldoAfectado().negate());
          apunteIntereses.setFecha(fechaApunte);
          if (movimientoFallido.getSaldoAfectado().negate().signum() >= 0) {
            apunteIntereses.setApunte(TIPO_CUENTA.DEBE.getId());
          } else {
            apunteIntereses.setApunte(TIPO_CUENTA.HABER.getId());
          }
          apunteIntereses.setContabilizado(Boolean.FALSE);
          apunteIntereses.setOperativa(Constantes.OPERATIVA_APUNTE);
          apunteIntereses.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
          apunteIntereses.setTipoMovimiento(TIPO_APUNTE.INTERES_COLATERAL.getId());
          apunteContableBo.save(apunteIntereses);
        } else if (movimientoFallido.getCdtipo().compareTo(MovimientosFallidos.INP.getTipo()) == 0) {
          // Apuntes contables pata mercado
          apunteIntereses = new ApunteContable();
          apunteIntereses.setCuentaContable(cuentaContable_3019104);
          apunteIntereses.setDescripcion(descripcionMovimiento);
          apunteIntereses.setConcepto(CONCEPTO_DEVENGO_INP);
          apunteIntereses.setImporte(movimientoFallido.getSaldoAfectado().abs());
          apunteIntereses.setFecha(fechaApunte);
          apunteIntereses.setApunte(TIPO_CUENTA.DEBE.getId());
          apunteIntereses.setContabilizado(Boolean.FALSE);
          apunteIntereses.setOperativa(Constantes.OPERATIVA_APUNTE);
          apunteIntereses.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
          apunteIntereses.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
          apunteContableBo.save(apunteIntereses);

          apunteIntereses = new ApunteContable();
          apunteIntereses.setCuentaContable(cuentaContable_2132104);
          apunteIntereses.setDescripcion(descripcionMovimiento);
          apunteIntereses.setConcepto(CONCEPTO_DEVENGO_INP);
          apunteIntereses.setImporte(movimientoFallido.getSaldoAfectado());
          apunteIntereses.setFecha(fechaApunte);
          apunteIntereses.setApunte(TIPO_CUENTA.HABER.getId());
          apunteIntereses.setContabilizado(Boolean.FALSE);
          apunteIntereses.setOperativa(Constantes.OPERATIVA_APUNTE);
          apunteIntereses.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
          apunteIntereses.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
          apunteContableBo.save(apunteIntereses);

          // Apuntes contables pata cliente
          apunteIntereses = new ApunteContable();
          apunteIntereses.setCuentaContable(cuentaContable_1133106);
          apunteIntereses.setDescripcion(descripcionMovimiento);
          apunteIntereses.setConcepto(CONCEPTO_DEVENGO_INP_CLIENTE);
          apunteIntereses.setImporte(movimientoFallido.getSaldoAfectado().abs());
          apunteIntereses.setFecha(fechaApunte);
          apunteIntereses.setApunte(TIPO_CUENTA.DEBE.getId());
          apunteIntereses.setContabilizado(Boolean.FALSE);
          apunteIntereses.setOperativa(Constantes.OPERATIVA_APUNTE);
          apunteIntereses.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
          apunteIntereses.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
          apunteContableBo.save(apunteIntereses);

          apunteIntereses = new ApunteContable();
          apunteIntereses.setCuentaContable(cuentaContable_3019104);
          apunteIntereses.setDescripcion(descripcionMovimiento);
          apunteIntereses.setConcepto(CONCEPTO_DEVENGO_INP_CLIENTE);
          apunteIntereses.setImporte(movimientoFallido.getSaldoAfectado());
          apunteIntereses.setFecha(fechaApunte);
          apunteIntereses.setApunte(TIPO_CUENTA.HABER.getId());
          apunteIntereses.setContabilizado(Boolean.FALSE);
          apunteIntereses.setOperativa(Constantes.OPERATIVA_APUNTE);
          apunteIntereses.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
          apunteIntereses.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
          apunteContableBo.save(apunteIntereses);
        } // else

        movimientoFallido.setContabilizado(Boolean.TRUE);
        movimientoFallidoDao.save(movimientoFallido);
      } // for (MovimientoFallido movimientoFallido : movimientosFallidosList)
    } // if (movimientosFallidosList != null && !movimientosFallidosList.isEmpty())

    LOG.debug("[generarApuntesDevengoIntereses] Fin ...");
  } // generarApuntesDevengoIntereses

  /**
   * Genera los apuntes contables de ajuste de colateral.
   * 
   * @param importeMovimientoNorma43 Importe recibido en el fichero Norma43.
   * @param importeMovimientoFallido Importe resultante del calculo del ajuste del colateral de los movimientos fallidos.
   * @param movimientoFallido Movimiento fallido.
   */
  private void generarApuntesAjusteColateral(BigDecimal importeMovimientoNorma43,
                                             BigDecimal importeMovimientoFallido,
                                             MovimientoFallido movimientoFallido) {
    LOG.debug("[generarApuntesAjusteColateral] Inicio ...");

    BigDecimal diferenciaImportes = importeMovimientoNorma43.subtract(importeMovimientoFallido);
    LOG.debug("[generarApuntesAjusteColateral] Diferencia: {}", diferenciaImportes);

    if (diferenciaImportes.abs().compareTo(margenDescuadres) <= 0) {

      ApunteContable apunteAjusteColateral;

      apunteAjusteColateral = new ApunteContable();
      apunteAjusteColateral.setCuentaContable(cuentaContable_1021101);
      apunteAjusteColateral.setDescripcion(FormatDataUtils.convertDateToString(movimientoFallido.getTradeDt(), FormatDataUtils.DATE_FORMAT) + "/"
                                           + movimientoFallido.getIsin());
      apunteAjusteColateral.setConcepto(CONCEPTO_AJUSTECOLATERAL);
      apunteAjusteColateral.setImporte(importeMovimientoNorma43);
      apunteAjusteColateral.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), movimientoFallido.getTrxTm()));
      apunteAjusteColateral.setApunte(TIPO_CUENTA.HABER.getId());
      apunteAjusteColateral.setContabilizado(Boolean.FALSE);
      apunteAjusteColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteAjusteColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
      apunteAjusteColateral.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
      apunteContableBo.save(apunteAjusteColateral);

      apunteAjusteColateral = new ApunteContable();
      apunteAjusteColateral.setCuentaContable(cuentaContable_1025401);
      apunteAjusteColateral.setDescripcion(FormatDataUtils.convertDateToString(movimientoFallido.getTradeDt(), FormatDataUtils.DATE_FORMAT) + "/"
                                           + movimientoFallido.getIsin());
      apunteAjusteColateral.setConcepto(CONCEPTO_AJUSTECOLATERAL);
      apunteAjusteColateral.setImporte(importeMovimientoNorma43);
      apunteAjusteColateral.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), movimientoFallido.getTrxTm()));
      apunteAjusteColateral.setApunte(TIPO_CUENTA.DEBE.getId());
      apunteAjusteColateral.setContabilizado(Boolean.FALSE);
      apunteAjusteColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunteAjusteColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
      apunteAjusteColateral.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
      apunteContableBo.save(apunteAjusteColateral);

      if (diferenciaImportes.signum() != 0) {
        // TODO Apunte de diferencia, si no la contabilidad no cuadrará
      } // if
    } else {
      // TODO Generar descuadre
    } // else
    LOG.debug("[generarApuntesAjusteColateral] Fin ...");
  } // generarApuntesAjusteColateral

  // TODO

  // /**
  // * Procesa los movimientos de fallidos y afectaciones.
  // *
  // * @param mfList Lista de movimientos fallidos.
  // * @param of Operación fallida.
  // */
  // private void processMovimientosFallidos(List<MovimientoFallido> mfList, OperacionFallido of) {
  // LOG.debug("## processMovimientosFallidos ## Inicio");
  //
  // // BigDecimal titulosPuiInicial = BigDecimal.ZERO;
  // // BigDecimal titulosPuiFinal = BigDecimal.ZERO;
  //
  // Boolean checkAco = true;
  // MovimientoFallido acoInicial = null;
  // MovimientoFallido acoFinal = null;
  //
  // HashMap<Date, MovimientoFallido> interesesColateral = new HashMap<Date, MovimientoFallido>();
  // HashMap<Date, MovimientoFallido> interesesPrestamo = new HashMap<Date, MovimientoFallido>();
  //
  // MovimientoFallido recompraTitulos = null;
  // MovimientoFallido recompraEfectivo = null;
  //
  // MovimientoFallido comisionConstitucionPrestamo = null;
  // MovimientoFallido costeAdministrativoRecompra = null;
  //
  // for (MovimientoFallido mFallido : mfList) {
  // switch (mFallido.getCdtipo()) {
  // case "APU":
  // // titulosPuiInicial = mFallido.getTitulosAfectados();
  // mFallido.setContabilizado(true);
  // break;
  // case "BPU":
  // // titulosPuiFinal = titulosPuiFinal.add(mFallido.getTitulosAfectados());
  // mFallido.setContabilizado(true);
  // break;
  // case "ACO":
  // if (checkAco) {
  // acoInicial = mFallido;
  // checkAco = false;
  // }
  // mFallido.setContabilizado(true);
  // break;
  // case "BCO":
  // acoFinal = mFallido;
  // mFallido.setContabilizado(true);
  // break;
  // case "INC":
  // interesesColateral.put(mFallido.getTrxTm(), mFallido);
  // mFallido.setContabilizado(true);
  // break;
  // case "INP":
  // interesesPrestamo.put(mFallido.getTrxTm(), mFallido);
  // mFallido.setContabilizado(true);
  // break;
  // case "RPA":
  // recompraTitulos = mFallido;
  // mFallido.setContabilizado(true);
  // break;
  // case "RPE":
  // recompraEfectivo = mFallido;
  // mFallido.setContabilizado(true);
  // break;
  // case "CCP":
  // comisionConstitucionPrestamo = mFallido;
  // mFallido.setContabilizado(true);
  // break;
  // case "CAR":
  // costeAdministrativoRecompra = mFallido;
  // mFallido.setContabilizado(true);
  // break;
  // case "CPU":
  // mFallido.setContabilizado(true);
  // break;
  // default:
  // LOG.warn("Tipo de movimiento de fallidos no reconocido: '" + mFallido.getCdtipo() + "'");
  // } // switch
  // } // for (MovimientoFallido mFallido : mfList)
  //
  // if (acoInicial != null && acoFinal != null) {
  // generateApuntesContablesACO(acoInicial, acoFinal);
  // }
  //
  // if (!interesesColateral.isEmpty() && !interesesPrestamo.isEmpty()) {
  // generateApuntesContablesIntereses(interesesColateral, interesesPrestamo);
  // }
  //
  // if (recompraEfectivo != null) {
  // generateApuntesContablesRecompraEfectivo(recompraEfectivo, recompraTitulos, comisionConstitucionPrestamo, costeAdministrativoRecompra, of);
  // } else if (recompraTitulos != null) {
  // generateApuntesContablesRecompraTitulos(recompraTitulos, comisionConstitucionPrestamo, costeAdministrativoRecompra, of);
  // contabilizarMovimientosAfectaciones(of);
  // } else {
  // contabilizarMovimientosAfectaciones(of);
  // } // else
  //
  // // Se contabilizan los movimientos de fallidos
  // movimientoFallidoDao.save(mfList);
  //
  // LOG.debug("## processMovimientosFallidos ## Fin");
  // } // processMovimientosFallidos
  //
  // /**
  // * Genera los apuntes contables del colateral de los préstamos de última instancia.
  // *
  // * @param acoInicial Movimiento fallido de establecimiento del colateral inicial.
  // * @param acoFinal Movimiento fallido de baja del colateral.
  // */
  // private void generateApuntesContablesACO(MovimientoFallido acoInicial, MovimientoFallido acoFinal) {
  // LOG.debug("## generateApuntesContablesACO ## Inicio");
  // // Diferencia entre el colateral inicial y el final
  // BigDecimal acoAjuste = acoInicial.getSaldoAfectado().abs().subtract(acoFinal.getSaldoAfectado());
  //
  // ApunteContable apunte_acoFinal;
  //
  // apunte_acoFinal = new ApunteContable();
  // apunte_acoFinal.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_acoFinal.setDescripcion(dF1.format(acoFinal.getTradeDt()) + " / " + acoFinal.getIsin());
  // apunte_acoFinal.setConcepto("Baja colateral");
  // apunte_acoFinal.setImporte(acoFinal.getSaldoAfectado());
  // apunte_acoFinal.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), acoFinal.getTrxTm()));
  // apunte_acoFinal.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_acoFinal.setContabilizado(Boolean.FALSE);
  // apunte_acoFinal.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_acoFinal.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_acoFinal.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
  // apunteContableBo.save(apunte_acoFinal);
  //
  // apunte_acoFinal = new ApunteContable();
  // apunte_acoFinal.setCuentaContable(cuentaContableDao.findByCuenta(1025401));
  // apunte_acoFinal.setDescripcion(dF1.format(acoFinal.getTradeDt()) + " / " + acoFinal.getIsin());
  // apunte_acoFinal.setConcepto("Baja colateral");
  // apunte_acoFinal.setImporte(acoFinal.getSaldoAfectado().negate());
  // apunte_acoFinal.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), acoFinal.getTrxTm()));
  // apunte_acoFinal.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_acoFinal.setContabilizado(Boolean.FALSE);
  // apunte_acoFinal.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_acoFinal.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_acoFinal.setTipoMovimiento(TIPO_APUNTE.COLATERAL.getId());
  // apunteContableBo.save(apunte_acoFinal);
  //
  // if (acoAjuste.compareTo(BigDecimal.ZERO) != 0) {
  // ApunteContable apunte_acoAjuste = null;
  // if (acoAjuste.compareTo(BigDecimal.ZERO) > 0) {
  // // Si devuelven colateral ingresando dinero irá al DEBE con signo más
  // apunte_acoAjuste = new ApunteContable();
  // apunte_acoAjuste.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_acoAjuste.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_acoAjuste.setImporte(acoAjuste);
  // apunte_acoAjuste.setDescripcion(dF1.format(acoFinal.getTradeDt()) + " / " + acoFinal.getIsin());
  // apunte_acoAjuste.setConcepto("Ajuste colateral");
  // apunte_acoAjuste.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), acoFinal.getTrxTm()));
  // apunte_acoAjuste.setContabilizado(Boolean.FALSE);
  // apunte_acoAjuste.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_acoAjuste.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_acoAjuste.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
  // apunteContableBo.save(apunte_acoAjuste);
  //
  // apunte_acoAjuste = new ApunteContable();
  // apunte_acoAjuste.setCuentaContable(cuentaContableDao.findByCuenta(1025401));
  // apunte_acoAjuste.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_acoAjuste.setImporte(acoAjuste.negate());
  // apunte_acoAjuste.setDescripcion(dF1.format(acoFinal.getTradeDt()) + " / " + acoFinal.getIsin());
  // apunte_acoAjuste.setConcepto("Ajuste colateral");
  // apunte_acoAjuste.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), acoFinal.getTrxTm()));
  // apunte_acoAjuste.setContabilizado(Boolean.FALSE);
  // apunte_acoAjuste.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_acoAjuste.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_acoAjuste.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
  // apunteContableBo.save(apunte_acoAjuste);
  // } else {
  // // Si cargan más importe del colateral irá al HABER con signo menos
  // apunte_acoAjuste = new ApunteContable();
  // apunte_acoAjuste.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_acoAjuste.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_acoAjuste.setImporte(acoAjuste);
  // apunte_acoAjuste.setDescripcion(dF1.format(acoFinal.getTradeDt()) + " / " + acoFinal.getIsin());
  // apunte_acoAjuste.setConcepto("Ajuste colateral");
  // apunte_acoAjuste.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), acoFinal.getTrxTm()));
  // apunte_acoAjuste.setContabilizado(Boolean.FALSE);
  // apunte_acoAjuste.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_acoAjuste.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_acoAjuste.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
  // apunteContableBo.save(apunte_acoAjuste);
  //
  // apunte_acoAjuste = new ApunteContable();
  // apunte_acoAjuste.setCuentaContable(cuentaContableDao.findByCuenta(1025401));
  // apunte_acoAjuste.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_acoAjuste.setImporte(acoAjuste.abs());
  // apunte_acoAjuste.setDescripcion(dF1.format(acoFinal.getTradeDt()) + " / " + acoFinal.getIsin());
  // apunte_acoAjuste.setConcepto("Ajuste colateral");
  // apunte_acoAjuste.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), acoFinal.getTrxTm()));
  // apunte_acoAjuste.setContabilizado(Boolean.FALSE);
  // apunte_acoAjuste.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_acoAjuste.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_acoAjuste.setTipoMovimiento(TIPO_APUNTE.AJUSTE_COLATERAL.getId());
  // apunteContableBo.save(apunte_acoAjuste);
  // } // else
  // } // if (acoAjuste.compareTo(BigDecimal.ZERO) == 0)
  //
  // LOG.debug("## generateApuntesContablesACO ## Fin");
  // } // generateApuntesContablesACO
  //
  // /**
  // * Genera los apuntes contables de los movimientos de intereses del colateral y de los préstamos de última instancia.
  // *
  // * @param interesesColateral Mapa de movimientos de intereses del colateral por fecha.
  // * @param interesesPrestamo Mapa de movimientos de intereses del prestamo de última instancia por fecha.
  // */
  // private void generateApuntesContablesIntereses(HashMap<Date, MovimientoFallido> interesesColateral,
  // HashMap<Date, MovimientoFallido> interesesPrestamo) {
  // LOG.debug("## generateApuntesContablesIntereses ## Inicio");
  //
  // // Los apuntes de devengo de intereses se hacen para el primer día del mes siguiente
  // Calendar fechaApuntesDevengo = GregorianCalendar.getInstance();
  // fechaApuntesDevengo.setLenient(true);
  // fechaApuntesDevengo.set(Calendar.DAY_OF_MONTH, fechaApuntesDevengo.getActualMaximum(Calendar.DAY_OF_MONTH));
  //
  // // Los apuntes de cobro de intereses se hacen para el primer día del mes siguiente
  // Calendar fechaApuntesCobro = GregorianCalendar.getInstance();
  // fechaApuntesCobro.setLenient(true);
  // if (fechaApuntesCobro.get(Calendar.MONTH) == 11) {
  // // Estamos en Diciembre, fecha a establecer 1 de Enero del año siguiente
  // fechaApuntesCobro.add(Calendar.YEAR, 1);
  // fechaApuntesCobro.set(Calendar.MONTH, 0);
  // fechaApuntesCobro.set(Calendar.DAY_OF_MONTH, 1);
  // } else {
  // fechaApuntesCobro.add(Calendar.MONTH, 1);
  // fechaApuntesCobro.set(Calendar.DAY_OF_MONTH, 1);
  // } // else
  //
  // ApunteContable apunte_interesesColateral;
  // ApunteContable apunte_interesesPrestamo;
  //
  // String descripcionMovimiento = null;
  // MovimientoFallido movimientoFallidoInteresesPrestamo = null;
  //
  // // Diferencia entre los intereses del colateral y los del préstamo
  // BigDecimal ajusteIntereses = BigDecimal.ZERO;
  //
  // for (Entry<Date, MovimientoFallido> entry : interesesColateral.entrySet()) {
  // descripcionMovimiento = dF1.format(entry.getKey()) + " / " + entry.getValue().getIsin() + " / " + entry.getValue().getAliasCliente() + " / "
  // + entry.getValue().getDescrali() + " / " + entry.getValue().getNumero_operacion_fallida();
  //
  // movimientoFallidoInteresesPrestamo = interesesPrestamo.get(entry.getKey());
  //
  // // Apuntes contables para mercado
  // apunte_interesesColateral = new ApunteContable();
  // apunte_interesesColateral.setCuentaContable(cuentaContableDao.findByCuenta(1027105));
  // apunte_interesesColateral.setDescripcion(descripcionMovimiento);
  // apunte_interesesColateral.setConcepto("Devengo intereses colateral");
  // apunte_interesesColateral.setImporte(entry.getValue().getSaldoAfectado());
  // apunte_interesesColateral.setFecha(new Date(fechaApuntesDevengo.getTimeInMillis()));
  // apunte_interesesColateral.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_interesesColateral.setContabilizado(Boolean.FALSE);
  // apunte_interesesColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesColateral.setTipoMovimiento(TIPO_APUNTE.INTERES_COLATERAL.getId());
  // apunteContableBo.save(apunte_interesesColateral);
  //
  // apunte_interesesColateral = new ApunteContable();
  // apunte_interesesColateral.setCuentaContable(cuentaContableDao.findByCuenta(4012105));
  // apunte_interesesColateral.setDescripcion(descripcionMovimiento);
  // apunte_interesesColateral.setConcepto("Devengo intereses colateral");
  // apunte_interesesColateral.setImporte(entry.getValue().getSaldoAfectado().negate());
  // apunte_interesesColateral.setFecha(new Date(fechaApuntesDevengo.getTimeInMillis()));
  // apunte_interesesColateral.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_interesesColateral.setContabilizado(Boolean.FALSE);
  // apunte_interesesColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesColateral.setTipoMovimiento(TIPO_APUNTE.INTERES_COLATERAL.getId());
  // apunteContableBo.save(apunte_interesesColateral);
  //
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(3019104));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Devengo intereses préstamo");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado().abs());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesDevengo.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(2132104));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Devengo intereses préstamo");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesDevengo.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // apunte_interesesColateral = new ApunteContable();
  // apunte_interesesColateral.setCuentaContable(cuentaContableDao.findByCuenta(1027105));
  // apunte_interesesColateral.setDescripcion(descripcionMovimiento);
  // apunte_interesesColateral.setConcepto("Liquidación intereses");
  // apunte_interesesColateral.setImporte(entry.getValue().getSaldoAfectado().negate());
  // apunte_interesesColateral.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_interesesColateral.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_interesesColateral.setContabilizado(Boolean.FALSE);
  // apunte_interesesColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesColateral.setTipoMovimiento(TIPO_APUNTE.INTERES_COLATERAL.getId());
  // apunteContableBo.save(apunte_interesesColateral);
  //
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(2132104));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Liquidación intereses");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado().abs());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // // Estos apuntes dependen del sentido, si la SV cobra irá al DEBE, si la SV paga irá al HABER
  // ajusteIntereses = movimientoFallidoInteresesPrestamo.getSaldoAfectado().abs().subtract(entry.getValue().getSaldoAfectado());
  //
  // if (ajusteIntereses.compareTo(BigDecimal.ZERO) != 0) {
  // ApunteContable apunte_ajsuteInteresesMercado = new ApunteContable();
  // ApunteContable apunte_ajsuteInteresesCliente = new ApunteContable();
  //
  // if (ajusteIntereses.compareTo(BigDecimal.ZERO) > 0) {
  // // Apunte contable ajuste de préstamos pata mercado
  // apunte_ajsuteInteresesMercado.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_ajsuteInteresesMercado.setImporte(ajusteIntereses.negate());
  // apunte_ajsuteInteresesMercado.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_ajsuteInteresesMercado.setDescripcion(descripcionMovimiento);
  // apunte_ajsuteInteresesMercado.setConcepto("Liquidación intereses");
  // apunte_ajsuteInteresesMercado.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_ajsuteInteresesMercado.setContabilizado(Boolean.FALSE);
  // apunte_ajsuteInteresesMercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ajsuteInteresesMercado.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ajsuteInteresesMercado.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_COMISIONES_PRESTAMO.getId());
  // apunteContableBo.save(apunte_ajsuteInteresesMercado);
  //
  // // Apunte contable ajuste de préstamos pata cliente
  // apunte_ajsuteInteresesCliente.setCuentaContable(cuentaContableDao.findByCuenta(4029406));
  // apunte_ajsuteInteresesCliente.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_ajsuteInteresesCliente.setImporte(ajusteIntereses);
  // apunte_ajsuteInteresesCliente.setDescripcion(descripcionMovimiento);
  // apunte_ajsuteInteresesCliente.setConcepto("Liquidación intereses cliente");
  // apunte_ajsuteInteresesCliente.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_ajsuteInteresesCliente.setContabilizado(Boolean.FALSE);
  // apunte_ajsuteInteresesCliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ajsuteInteresesCliente.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ajsuteInteresesCliente.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_COMISIONES_PRESTAMO.getId());
  // apunteContableBo.save(apunte_ajsuteInteresesCliente);
  // } else {
  // // Apunte contable ajuste de préstamos pata mercado
  // apunte_ajsuteInteresesMercado.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_ajsuteInteresesMercado.setImporte(ajusteIntereses.abs());
  // apunte_ajsuteInteresesMercado.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_ajsuteInteresesMercado.setDescripcion(descripcionMovimiento);
  // apunte_ajsuteInteresesMercado.setConcepto("Liquidación intereses");
  // apunte_ajsuteInteresesMercado.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_ajsuteInteresesMercado.setContabilizado(Boolean.FALSE);
  // apunte_ajsuteInteresesMercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ajsuteInteresesMercado.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ajsuteInteresesMercado.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_COMISIONES_PRESTAMO.getId());
  // apunteContableBo.save(apunte_ajsuteInteresesMercado);
  //
  // // Apunte contable ajuste de préstamos pata cliente
  // apunte_ajsuteInteresesCliente.setCuentaContable(cuentaContableDao.findByCuenta(4029406));
  // apunte_ajsuteInteresesCliente.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_ajsuteInteresesCliente.setImporte(ajusteIntereses);
  // apunte_ajsuteInteresesCliente.setDescripcion(descripcionMovimiento);
  // apunte_ajsuteInteresesCliente.setConcepto("Liquidación intereses cliente");
  // apunte_ajsuteInteresesCliente.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_ajsuteInteresesCliente.setContabilizado(Boolean.FALSE);
  // apunte_ajsuteInteresesCliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ajsuteInteresesCliente.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ajsuteInteresesCliente.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_COMISIONES_PRESTAMO.getId());
  // apunteContableBo.save(apunte_ajsuteInteresesCliente);
  // } // else
  //
  // // Apunte contable ajuste de préstamos pata mercado
  // apunte_ajsuteInteresesMercado.setImporte(ajusteIntereses);
  // apunte_ajsuteInteresesMercado.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_ajsuteInteresesMercado.setDescripcion(descripcionMovimiento);
  // apunte_ajsuteInteresesMercado.setConcepto("Liquidación intereses cliente");
  // apunte_ajsuteInteresesMercado.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_ajsuteInteresesMercado.setContabilizado(Boolean.FALSE);
  // apunte_ajsuteInteresesMercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ajsuteInteresesMercado.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ajsuteInteresesMercado.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_COMISIONES_PRESTAMO.getId());
  // apunteContableBo.save(apunte_ajsuteInteresesMercado);
  //
  // // Apunte contable ajuste de préstamos pata cliente
  // apunte_ajsuteInteresesCliente.setImporte(ajusteIntereses);
  // apunte_ajsuteInteresesCliente.setCuentaContable(cuentaContableDao.findByCuenta(4029406));
  // apunte_ajsuteInteresesCliente.setDescripcion(descripcionMovimiento);
  // apunte_ajsuteInteresesCliente.setConcepto("Liquidación intereses cliente");
  // apunte_ajsuteInteresesCliente.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_ajsuteInteresesCliente.setContabilizado(Boolean.FALSE);
  // apunte_ajsuteInteresesCliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ajsuteInteresesCliente.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ajsuteInteresesCliente.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_COMISIONES_PRESTAMO.getId());
  // apunteContableBo.save(apunte_ajsuteInteresesCliente);
  // } // if (ajusteIntereses.compareTo(BigDecimal.ZERO) != 0)
  //
  // // Apuntes contables para cliente
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(1133106));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Devengo intereses préstamo cliente");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado().abs());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesDevengo.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(3019104));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Devengo intereses préstamo cliente");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesDevengo.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Liquidación intereses cliente");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado().abs());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // apunte_interesesPrestamo = new ApunteContable();
  // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(1133106));
  // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // apunte_interesesPrestamo.setConcepto("Liquidación intereses cliente");
  // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado());
  // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // apunteContableBo.save(apunte_interesesPrestamo);
  //
  // // apunte_interesesColateral = new ApunteContable();
  // // apunte_interesesColateral.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // // apunte_interesesColateral.setDescripcion(descripcionMovimiento);
  // // apunte_interesesColateral.setConcepto("Liquidación intereses cliente");
  // // apunte_interesesColateral.setImporte(entry.getValue().getSaldoAfectado());
  // // apunte_interesesColateral.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // // apunte_interesesColateral.setApunte(TIPO_CUENTA.DEBE.getId());
  // // apunte_interesesColateral.setContabilizado(Boolean.FALSE);
  // // apunte_interesesColateral.setOperativa(Constantes.OPERATIVA_APUNTE);
  // // apunte_interesesColateral.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // // apunte_interesesColateral.setTipoMovimiento(TIPO_APUNTE.INTERES_COLATERAL.getId());
  // // apunteContableBo.save(apunte_interesesColateral);
  // //
  // // apunte_interesesPrestamo = new ApunteContable();
  // // apunte_interesesPrestamo.setCuentaContable(cuentaContableDao.findByCuenta(1133106));
  // // apunte_interesesPrestamo.setDescripcion(descripcionMovimiento);
  // // apunte_interesesPrestamo.setConcepto("Liquidación intereses cliente");
  // // apunte_interesesPrestamo.setImporte(movimientoFallidoInteresesPrestamo.getSaldoAfectado());
  // // apunte_interesesPrestamo.setFecha(new Date(fechaApuntesCobro.getTimeInMillis()));
  // // apunte_interesesPrestamo.setApunte(TIPO_CUENTA.HABER.getId());
  // // apunte_interesesPrestamo.setContabilizado(Boolean.FALSE);
  // // apunte_interesesPrestamo.setOperativa(Constantes.OPERATIVA_APUNTE);
  // // apunte_interesesPrestamo.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // // apunte_interesesPrestamo.setTipoMovimiento(TIPO_APUNTE.COMISION_PRESTAMO.getId());
  // // apunteContableBo.save(apunte_interesesPrestamo);
  // } // for (Entry<Date, MovimientoFallido> entry : interesesColateral.entrySet())
  //
  // LOG.debug("## generateApuntesContablesIntereses ## Fin");
  // } // generateApuntesContablesIntereses
  //
  // /**
  // * Genera los apuntes contables de los movimientos de recompra de títulos.
  // *
  // * @param mfRPA Movimiento de recompra.
  // * @param mfCCP Movimiento de comisión por constitución de préstamo.
  // * @param mfCAR Movimiento de coste administrativo de la recompra.
  // * @param of Operación fallida.
  // */
  // private void generateApuntesContablesRecompraTitulos(MovimientoFallido mfRPA, MovimientoFallido mfCCP, MovimientoFallido mfCAR, OperacionFallido
  // of) {
  // LOG.debug("## generateApuntesContablesRecompraTitulos ## Inicio");
  //
  // ApunteContable apunte_recompra;
  // ApunteContable apunte_anulacionCompra;
  // ApunteContable apunte_diferencia;
  //
  // // Coste de la recompra más gastos
  // BigDecimal saldoAfectado = mfRPA.getSaldoAfectado().add(mfCCP.getSaldoAfectado().add(mfCAR.getSaldoAfectado())).abs();
  //
  // // Diferencia entre el coste total recompra menos importe de la compra fallida
  // BigDecimal diferenciaRecompra = saldoAfectado.subtract(of.getImporteEfectivoFallido());
  //
  // String descripcionMovimiento = dF1.format(mfRPA.getTradeDt()) + " / " + mfRPA.getAliasCliente() + " / " + mfRPA.getNumero_operacion_fallida()
  // + mfRPA.getDescrali();
  //
  // apunte_recompra = new ApunteContable();
  // apunte_recompra.setCuentaContable(cuentaContableDao.findByCuenta(1133104));
  // apunte_recompra.setDescripcion(descripcionMovimiento);
  // apunte_recompra.setConcepto("Recompras títulos mercado (RPA)");
  // apunte_recompra.setImporte(saldoAfectado);
  // apunte_recompra.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPA.getTrxTm()));
  // apunte_recompra.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_recompra.setContabilizado(Boolean.FALSE);
  // apunte_recompra.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_recompra.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_recompra.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_recompra);
  //
  // apunte_recompra = new ApunteContable();
  // apunte_recompra.setCuentaContable(cuentaContableDao.findByCuenta(1133104));
  // apunte_recompra.setDescripcion(descripcionMovimiento);
  // apunte_recompra.setConcepto("Recompras liquidadas cliente");
  // apunte_recompra.setImporte(saldoAfectado.negate());
  // apunte_recompra.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPA.getTrxTm()));
  // apunte_recompra.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_recompra.setContabilizado(Boolean.FALSE);
  // apunte_recompra.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_recompra.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_recompra.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_recompra);
  //
  // apunte_anulacionCompra = new ApunteContable();
  // apunte_anulacionCompra.setCuentaContable(cuentaContableDao.findByCuenta(2144601));
  // apunte_anulacionCompra.setDescripcion(descripcionMovimiento);
  // apunte_anulacionCompra.setConcepto("Recompras liquidadas cliente");
  // apunte_anulacionCompra.setImporte(of.getImporteEfectivoFallido());
  // apunte_anulacionCompra.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_anulacionCompra.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_anulacionCompra.setContabilizado(Boolean.FALSE);
  // apunte_anulacionCompra.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_anulacionCompra.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_anulacionCompra.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_VENTA_FALLIDA.getId());
  // apunteContableBo.save(apunte_anulacionCompra);
  //
  // apunte_anulacionCompra = new ApunteContable();
  // apunte_anulacionCompra.setCuentaContable(cuentaContableDao.findByCuenta(2144601));
  // apunte_anulacionCompra.setDescripcion(descripcionMovimiento);
  // apunte_anulacionCompra.setConcepto("Recompras títulos mercado (RPA)");
  // apunte_anulacionCompra.setImporte(of.getImporteEfectivoFallido().negate());
  // apunte_anulacionCompra.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_anulacionCompra.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_anulacionCompra.setContabilizado(Boolean.FALSE);
  // apunte_anulacionCompra.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_anulacionCompra.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_anulacionCompra.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_VENTA_FALLIDA.getId());
  // apunteContableBo.save(apunte_anulacionCompra);
  //
  // if (of.getCorretaje().compareTo(BigDecimal.ZERO) != 0) {
  // ApunteContable apunte_corretaje;
  //
  // apunte_corretaje = new ApunteContable();
  // apunte_corretaje.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_corretaje.setDescripcion(descripcionMovimiento);
  // apunte_corretaje.setConcepto("Recompras liquidadas cliente");
  // apunte_corretaje.setImporte(of.getCorretaje());
  // apunte_corretaje.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_corretaje.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_corretaje.setContabilizado(Boolean.FALSE);
  // apunte_corretaje.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_corretaje.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_corretaje.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
  // apunteContableBo.save(apunte_corretaje);
  //
  // apunte_corretaje = new ApunteContable();
  // apunte_corretaje.setCuentaContable(cuentaContableDao.findByCuenta(1122108));
  // apunte_corretaje.setDescripcion(descripcionMovimiento);
  // apunte_corretaje.setConcepto("Recompras liquidadas cliente");
  // apunte_corretaje.setImporte(of.getCorretaje().negate());
  // apunte_corretaje.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_corretaje.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_corretaje.setContabilizado(Boolean.FALSE);
  // apunte_corretaje.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_corretaje.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_corretaje.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
  // apunteContableBo.save(apunte_corretaje);
  // } // if (of.getCorretaje().compareTo(BigDecimal.ZERO) != 0)
  //
  // apunte_diferencia = new ApunteContable();
  // apunte_diferencia.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_diferencia.setDescripcion(descripcionMovimiento);
  // apunte_diferencia.setConcepto("Recompras liquidadas cliente");
  // apunte_diferencia.setImporte(diferenciaRecompra);
  // apunte_diferencia.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPA.getTrxTm()));
  // apunte_diferencia.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_diferencia.setContabilizado(Boolean.FALSE);
  // apunte_diferencia.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_diferencia.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_diferencia.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_diferencia);
  //
  // apunte_diferencia = new ApunteContable();
  // apunte_diferencia.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_diferencia.setDescripcion(descripcionMovimiento);
  // apunte_diferencia.setConcepto("Recompras Títulos mercado (RPA)");
  // apunte_diferencia.setImporte(diferenciaRecompra.negate());
  // apunte_diferencia.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPA.getTrxTm()));
  // apunte_diferencia.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_diferencia.setContabilizado(Boolean.FALSE);
  // apunte_diferencia.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_diferencia.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_diferencia.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_diferencia);
  //
  // LOG.debug("## generateApuntesContablesRecompraTitulos ## Fin");
  // } // generateApuntesContablesRecompraTitulos
  //
  // /**
  // * Genera los apuntes contables de los movimientos de recompra en efectivo.
  // * <p>
  // * Si mfRPA es distinto de nulo se trata de un movimiento de recompra de títulos tras la anulación de una compra por estar afectada por la
  // operación
  // * fallida.
  // *
  // * @param mfRPE Movimiento de recompra en efectivo.
  // * @param mfRPA Movimiento de recompra por afectación a compras del mismo cliente fallido.
  // * @param mfCCP Movimiento de comisión por constitución de préstamo.
  // * @param mfCAR Movimiento de coste administrativo de la recompra.
  // * @param of Operación fallida.
  // */
  // private void generateApuntesContablesRecompraEfectivo(MovimientoFallido mfRPE,
  // MovimientoFallido mfRPA,
  // MovimientoFallido mfCCP,
  // MovimientoFallido mfCAR,
  // OperacionFallido of) {
  // LOG.debug("## generateApuntesContablesRecompraEfectivo ## Inicio");
  //
  // // Coste de la recompra más gastos
  // BigDecimal saldoAfectado = mfRPE.getSaldoAfectado().add(mfCCP.getSaldoAfectado().add(mfCAR.getSaldoAfectado())).abs();
  //
  // // Descripción de los apuntes a generar
  // String descripcionMovimiento = dF1.format(mfRPE.getTradeDt()) + " / " + mfRPE.getAliasCliente() + " / " + mfRPE.getNumero_operacion_fallida()
  // + mfRPE.getDescrali();
  //
  // // Apuntes contables de la recompra en efectivo
  // ApunteContable apunte_recompra;
  //
  // apunte_recompra = new ApunteContable();
  // apunte_recompra.setCuentaContable(cuentaContableDao.findByCuenta(1133107));
  // apunte_recompra.setDescripcion(descripcionMovimiento);
  // apunte_recompra.setConcepto("Liquidación en efectivo ECC");
  // apunte_recompra.setImporte(saldoAfectado);
  // apunte_recompra.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_recompra.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_recompra.setContabilizado(Boolean.FALSE);
  // apunte_recompra.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_recompra.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_recompra.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_recompra);
  //
  // apunte_recompra = new ApunteContable();
  // apunte_recompra.setCuentaContable(cuentaContableDao.findByCuenta(1133107));
  // apunte_recompra.setDescripcion(descripcionMovimiento);
  // apunte_recompra.setConcepto("Liquidacion en efectivo cliente");
  // apunte_recompra.setImporte(saldoAfectado.negate());
  // apunte_recompra.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_recompra.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_recompra.setContabilizado(Boolean.FALSE);
  // apunte_recompra.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_recompra.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_recompra.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_recompra);
  //
  // // Apuntes contables de la venta fallida
  // ApunteContable apunte_ventaFallida;
  //
  // apunte_ventaFallida = new ApunteContable();
  // apunte_ventaFallida.setCuentaContable(cuentaContableDao.findByCuenta(2144601));
  // apunte_ventaFallida.setDescripcion(descripcionMovimiento);
  // apunte_ventaFallida.setConcepto("Liquidacion en efectivo cliente");
  // apunte_ventaFallida.setImporte(of.getImporteEfectivoFallido());
  // apunte_ventaFallida.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_ventaFallida.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_ventaFallida.setContabilizado(Boolean.FALSE);
  // apunte_ventaFallida.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ventaFallida.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ventaFallida.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_VENTA_FALLIDA.getId());
  // apunteContableBo.save(apunte_ventaFallida);
  //
  // apunte_ventaFallida = new ApunteContable();
  // apunte_ventaFallida.setCuentaContable(cuentaContableDao.findByCuenta(2144601));
  // apunte_ventaFallida.setDescripcion(descripcionMovimiento);
  // apunte_ventaFallida.setConcepto("Liquidación en efectivo ECC");
  // apunte_ventaFallida.setImporte(of.getImporteEfectivoFallido().negate());
  // apunte_ventaFallida.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_ventaFallida.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_ventaFallida.setContabilizado(Boolean.FALSE);
  // apunte_ventaFallida.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_ventaFallida.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_ventaFallida.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_VENTA_FALLIDA.getId());
  // apunteContableBo.save(apunte_ventaFallida);
  //
  // // Apuntes contables del corretaje
  // BigDecimal saldoCorretaje = BigDecimal.ZERO;
  // if (of.getCorretaje().compareTo(BigDecimal.ZERO) != 0) {
  // ApunteContable apunte_corretaje;
  //
  // saldoCorretaje = of.getCorretaje();
  //
  // apunte_corretaje = new ApunteContable();
  // apunte_corretaje.setCuentaContable(cuentaContableDao.findByCuenta(1122108));
  // apunte_corretaje.setDescripcion(descripcionMovimiento);
  // apunte_corretaje.setConcepto("Recompras liquidadas cliente");
  // apunte_corretaje.setImporte(of.getCorretaje().negate());
  // apunte_corretaje.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_corretaje.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_corretaje.setContabilizado(Boolean.FALSE);
  // apunte_corretaje.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_corretaje.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_corretaje.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
  // apunteContableBo.save(apunte_corretaje);
  //
  // apunte_corretaje = new ApunteContable();
  // apunte_corretaje.setCuentaContable(cuentaContableDao.findByCuenta(1122108));
  // apunte_corretaje.setDescripcion(descripcionMovimiento);
  // apunte_corretaje.setConcepto("Recompras liquidadas cliente");
  // apunte_corretaje.setImporte(of.getCorretaje().negate());
  // apunte_corretaje.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), of.getFeejecuc()));
  // apunte_corretaje.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_corretaje.setContabilizado(Boolean.FALSE);
  // apunte_corretaje.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_corretaje.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_corretaje.setTipoMovimiento(TIPO_APUNTE.CORRETAJE.getId());
  // apunteContableBo.save(apunte_corretaje);
  // } // if (of.getCorretaje().compareTo(BigDecimal.ZERO) != 0)
  //
  // ApunteContable apunte_compraAnulada;
  // ApunteContable apunte_RecompraAfectacion;
  // ApunteContable apunte_diferenciaSaldosAfectacion;
  // ApunteContable apunte_diferenciaSaldosAfectacionCorretaje;
  //
  // List<MovimientoFallido> movFallidosAfectaciones = null;
  //
  // BigDecimal diferenciaRecompra = BigDecimal.ZERO;
  // BigDecimal saldoRecompra = BigDecimal.ZERO;
  // BigDecimal saldoAnulacionCompra = BigDecimal.ZERO;
  //
  // String[] aliass = null;
  // String alias = null;
  // String subcuenta = null;
  // for (Afectacion afectacion : of.getAfectaciones()) {
  // aliass = afectacion.getCdalias().split("\\|");
  // if (aliass.length == 1) {
  // alias = aliass[0];
  // subcuenta = "";
  // } else {
  // alias = aliass[1];
  // subcuenta = aliass[0];
  // } // else
  //
  // movFallidosAfectaciones = movimientoFallidoDao.findNoContabilizadosByFields(alias, subcuenta, of.getIsin(), of.getCamara(),
  // of.getNumeroOperacionIlEcc(), tiposMovimientosAfectaciones);
  // for (MovimientoFallido mFallidoAfectacion : movFallidosAfectaciones) {
  // switch (mFallidoAfectacion.getCdtipo()) {
  // case "AFA":
  // mFallidoAfectacion.setContabilizado(true);
  // break;
  // case "BFA":
  // mFallidoAfectacion.setContabilizado(true);
  // break;
  // case "APA":
  // saldoAnulacionCompra = saldoAnulacionCompra.add(mFallidoAfectacion.getSaldoAfectado());
  // mFallidoAfectacion.setContabilizado(true);
  // break;
  // case "RPA":
  // saldoRecompra = saldoRecompra.add(mFallidoAfectacion.getSaldoAfectado().abs());
  // mFallidoAfectacion.setContabilizado(true);
  // break;
  // default:
  // LOG.warn("Tipo de movimiento de recompra de afectación no reconocido: '" + mFallidoAfectacion.getCdtipo() + "'");
  // } // switch
  // } // for (MovimientoFallido mFallido : mf)
  //
  // movimientoFallidoDao.save(movFallidosAfectaciones);
  //
  // } // for (Afectacion afectacion : of.getAfectaciones()) {
  //
  // diferenciaRecompra = saldoRecompra.subtract(saldoAnulacionCompra).abs();
  //
  // apunte_compraAnulada = new ApunteContable();
  // apunte_compraAnulada.setCuentaContable(cuentaContableDao.findByCuenta(1133104));
  // apunte_compraAnulada.setDescripcion(descripcionMovimiento);
  // apunte_compraAnulada.setConcepto("Liquidación en efectivo ECC");
  // apunte_compraAnulada.setImporte(saldoAnulacionCompra);
  // apunte_compraAnulada.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_compraAnulada.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_compraAnulada.setContabilizado(Boolean.FALSE);
  // apunte_compraAnulada.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_compraAnulada.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_compraAnulada.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_COMPRA_AFECTADA.getId());
  // apunteContableBo.save(apunte_compraAnulada);
  //
  // apunte_compraAnulada = new ApunteContable();
  // apunte_compraAnulada.setCuentaContable(cuentaContableDao.findByCuenta(1133104));
  // apunte_compraAnulada.setDescripcion(descripcionMovimiento);
  // apunte_compraAnulada.setConcepto("Liquidacion en efectivo cliente");
  // apunte_compraAnulada.setImporte(saldoAnulacionCompra.negate());
  // apunte_compraAnulada.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_compraAnulada.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_compraAnulada.setContabilizado(Boolean.FALSE);
  // apunte_compraAnulada.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_compraAnulada.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_compraAnulada.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_COMPRA_AFECTADA.getId());
  // apunteContableBo.save(apunte_compraAnulada);
  //
  // apunte_RecompraAfectacion = new ApunteContable();
  // apunte_RecompraAfectacion.setCuentaContable(cuentaContableDao.findByCuenta(2144602));
  // apunte_RecompraAfectacion.setDescripcion(descripcionMovimiento);
  // apunte_RecompraAfectacion.setConcepto("Liquidacion en efectivo cliente");
  // apunte_RecompraAfectacion.setImporte(saldoAnulacionCompra.add(diferenciaRecompra));
  // apunte_RecompraAfectacion.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_RecompraAfectacion.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_RecompraAfectacion.setContabilizado(Boolean.FALSE);
  // apunte_RecompraAfectacion.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_RecompraAfectacion.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_RecompraAfectacion.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_RecompraAfectacion);
  //
  // apunte_RecompraAfectacion = new ApunteContable();
  // apunte_RecompraAfectacion.setCuentaContable(cuentaContableDao.findByCuenta(2144602));
  // apunte_RecompraAfectacion.setDescripcion(descripcionMovimiento);
  // apunte_RecompraAfectacion.setConcepto("Liquidación en efectivo ECC");
  // apunte_RecompraAfectacion.setImporte(saldoAnulacionCompra.add(diferenciaRecompra).negate());
  // apunte_RecompraAfectacion.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_RecompraAfectacion.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_RecompraAfectacion.setContabilizado(Boolean.FALSE);
  // apunte_RecompraAfectacion.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_RecompraAfectacion.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_RecompraAfectacion.setTipoMovimiento(TIPO_APUNTE.EFECTIVO_RECOMPRA.getId());
  // apunteContableBo.save(apunte_RecompraAfectacion);
  //
  // apunte_diferenciaSaldosAfectacionCorretaje = new ApunteContable();
  // apunte_diferenciaSaldosAfectacionCorretaje.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_diferenciaSaldosAfectacionCorretaje.setDescripcion(descripcionMovimiento);
  // apunte_diferenciaSaldosAfectacionCorretaje.setConcepto("Liquidacion en efectivo cliente");
  // apunte_diferenciaSaldosAfectacionCorretaje.setImporte(diferenciaRecompra.add(saldoCorretaje).add(saldoCorretaje));
  // apunte_diferenciaSaldosAfectacionCorretaje.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_diferenciaSaldosAfectacionCorretaje.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_diferenciaSaldosAfectacionCorretaje.setContabilizado(Boolean.FALSE);
  // apunte_diferenciaSaldosAfectacionCorretaje.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_diferenciaSaldosAfectacionCorretaje.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_diferenciaSaldosAfectacionCorretaje.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_CORRETAJE.getId());
  // apunteContableBo.save(apunte_diferenciaSaldosAfectacionCorretaje);
  //
  // apunte_diferenciaSaldosAfectacion = new ApunteContable();
  // apunte_diferenciaSaldosAfectacion.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_diferenciaSaldosAfectacion.setDescripcion(descripcionMovimiento);
  // apunte_diferenciaSaldosAfectacion.setConcepto("Liquidación en efectivo ECC");
  // apunte_diferenciaSaldosAfectacion.setImporte(diferenciaRecompra.negate());
  // apunte_diferenciaSaldosAfectacion.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfRPE.getTrxTm()));
  // apunte_diferenciaSaldosAfectacion.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_diferenciaSaldosAfectacion.setContabilizado(Boolean.FALSE);
  // apunte_diferenciaSaldosAfectacion.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_diferenciaSaldosAfectacion.setTipoComprobante(TIPO_COMPROBANTE.FALLIDOS.getTipo());
  // apunte_diferenciaSaldosAfectacion.setTipoMovimiento(TIPO_APUNTE.DIFERENCIA_EFECTIVO.getId());
  // apunteContableBo.save(apunte_diferenciaSaldosAfectacion);
  //
  // LOG.debug("## generateApuntesContablesRecompraEfectivo ## Fin");
  // } // generateApuntesContablesRecompraEfectivo
  //
  // /**
  // * Establece el estado de los movimientos de afectaciones de la operación fallida especificada a contabilizado.
  // *
  // * @param of Operación fallida.
  // */
  // private void contabilizarMovimientosAfectaciones(OperacionFallido of) {
  // LOG.debug("## contabilizarMovimientosAfectaciones ## Inicio");
  // List<MovimientoFallido> movFallidosAfectaciones = null;
  //
  // String[] aliass = null;
  // String alias = null;
  // String subcuenta = null;
  //
  // for (Afectacion afectacion : of.getAfectaciones()) {
  // aliass = afectacion.getCdalias().split("\\|");
  // if (aliass.length == 1) {
  // alias = aliass[0];
  // subcuenta = "";
  // } else {
  // alias = aliass[1];
  // subcuenta = aliass[0];
  // } // else
  //
  // movFallidosAfectaciones = movimientoFallidoDao.findNoContabilizadosByFields(alias, subcuenta, of.getIsin(), of.getCamara(),
  // of.getNumeroOperacionIlEcc(), tiposMovimientosAfectaciones);
  //
  // for (MovimientoFallido mFallidoAfectacion : movFallidosAfectaciones) {
  // switch (mFallidoAfectacion.getCdtipo()) {
  // case "AFA":
  // mFallidoAfectacion.setContabilizado(true);
  // break;
  // case "BFA":
  // mFallidoAfectacion.setContabilizado(true);
  // break;
  // default:
  // LOG.warn("Tipo de movimiento de afectación no reconocido: '" + mFallidoAfectacion.getCdtipo() + "'");
  // } // switch
  // } // for (MovimientoFallido mFallidoAfectacion : movFallidosAfectaciones)
  //
  // // Se contabilizan los mmovimientos de afectaciones
  // movimientoFallidoDao.save(movFallidosAfectaciones);
  //
  // } // for (Afectacion afectacion : of.getAfectaciones())
  //
  // LOG.debug("## contabilizarMovimientosAfectaciones ## Fin");
  // } // contabilizarMovimientosAfectaciones
  //
  // /**
  // * Procesa los movimientos de ajustes por eventos financieros.
  // *
  // * @param mfAjustesList Lista de movimientos de ajustes por eventos financieros.
  // * @param of Operación fallida.
  // */
  // private void processMovimientosAjustes(List<MovimientoFallido> mfAjustesList, OperacionFallido of) {
  // LOG.debug("## processMovimientosAjustes ## Inicio");
  // for (MovimientoFallido mfAjuste : mfAjustesList) {
  // switch (mfAjuste.getCdtipo()) {
  // case "ADE":
  // generateApuntesContablesAjustes(mfAjuste);
  // mfAjuste.setContabilizado(true);
  // break;
  // case "EDE":
  // generateApuntesContablesAjustes(mfAjuste);
  // mfAjuste.setContabilizado(true);
  // break;
  // case "ADV":
  // mfAjuste.setContabilizado(true);
  // break;
  // case "EDV":
  // mfAjuste.setContabilizado(true);
  // break;
  // case "ADP":
  // mfAjuste.setContabilizado(true);
  // break;
  // case "APJ":
  // mfAjuste.setContabilizado(true);
  // break;
  // case "DPA":
  // mfAjuste.setContabilizado(true);
  // break;
  // case "BPC":
  // mfAjuste.setContabilizado(true);
  // break;
  // case "ACP":
  // generateApuntesContablesAjustes(mfAjuste);
  // mfAjuste.setContabilizado(true);
  // break;
  // case "FDV":
  // generateApuntesContablesAjustes(mfAjuste);
  // mfAjuste.setContabilizado(true);
  // break;
  // case "AFV":
  // generateApuntesContablesAjustes(mfAjuste);
  // mfAjuste.setContabilizado(true);
  // break;
  // default:
  // LOG.warn("Tipo de movimiento de ajuste por evento financiero no reconocido: '" + mfAjuste.getCdtipo() + "'");
  // } // switch
  // } // for (MovimientoFallido mfAjuste : mfAjustesList)
  //
  // // Se establecem los movimientos de ajustes a estado contabilizado
  // movimientoFallidoDao.save(mfAjustesList);
  // LOG.debug("## processMovimientosAjustes ## Fin");
  // } // processMovimientosAjustes
  //
  // /**
  // * Genera los apuntes contables de los movimientos de ajustes por eventos financieros.
  // *
  // * @param mfAjuste Movimiento de ajuste por evento financiero.
  // */
  // private void generateApuntesContablesAjustes(MovimientoFallido mfAjuste) {
  // LOG.debug("## generateApuntesContablesAjustes ## Inicio");
  //
  // ApunteContable apunte_mercado;
  // ApunteContable apunte_cliente;
  //
  // String descripcionMovimiento = dF1.format(mfAjuste.getTradeDt()) + " / " + mfAjuste.getAliasCliente() + " / "
  // + mfAjuste.getNumero_operacion_fallida() + mfAjuste.getDescrali() + " / " + mfAjuste.getIsin();
  //
  // if (mfAjuste.getSaldoAfectado().compareTo(BigDecimal.ZERO) > 0) {
  // // Ajuste positivo mercado
  // apunte_mercado = new ApunteContable();
  // apunte_mercado.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_mercado.setDescripcion(descripcionMovimiento);
  // apunte_mercado.setConcepto("Ajuste positivo por evento");
  // apunte_mercado.setImporte(mfAjuste.getSaldoAfectado());
  // apunte_mercado.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_mercado.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_mercado.setContabilizado(Boolean.FALSE);
  // apunte_mercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_mercado.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_mercado.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_mercado);
  //
  // apunte_mercado = new ApunteContable();
  // apunte_mercado.setCuentaContable(cuentaContableDao.findByCuenta(2144603));
  // apunte_mercado.setDescripcion(descripcionMovimiento);
  // apunte_mercado.setConcepto("Ajuste positivo por evento");
  // apunte_mercado.setImporte(mfAjuste.getSaldoAfectado().negate());
  // apunte_mercado.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_mercado.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_mercado.setContabilizado(Boolean.FALSE);
  // apunte_mercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_mercado.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_mercado.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_mercado);
  //
  // // Ajuste positivo cliente
  // apunte_cliente = new ApunteContable();
  // apunte_cliente.setCuentaContable(cuentaContableDao.findByCuenta(2144603));
  // apunte_cliente.setDescripcion(descripcionMovimiento);
  // apunte_cliente.setConcepto("Ajuste positivo por evento cliente");
  // apunte_cliente.setImporte(mfAjuste.getSaldoAfectado());
  // apunte_cliente.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_cliente.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_cliente.setContabilizado(Boolean.FALSE);
  // apunte_cliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_cliente.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_cliente.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_cliente);
  //
  // apunte_cliente = new ApunteContable();
  // apunte_cliente.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_cliente.setDescripcion(descripcionMovimiento);
  // apunte_cliente.setConcepto("Ajuste positivo por evento cliente");
  // apunte_cliente.setImporte(mfAjuste.getSaldoAfectado().negate());
  // apunte_cliente.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_cliente.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_cliente.setContabilizado(Boolean.FALSE);
  // apunte_cliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_cliente.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_cliente.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_cliente);
  // } else if (mfAjuste.getSaldoAfectado().compareTo(BigDecimal.ZERO) < 0) {
  // // Ajuste negativo mercado
  // apunte_mercado = new ApunteContable();
  // apunte_mercado.setCuentaContable(cuentaContableDao.findByCuenta(1133105));
  // apunte_mercado.setDescripcion(descripcionMovimiento);
  // apunte_mercado.setConcepto("Ajuste negativo por evento");
  // apunte_mercado.setImporte(mfAjuste.getSaldoAfectado().abs());
  // apunte_mercado.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_mercado.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_mercado.setContabilizado(Boolean.FALSE);
  // apunte_mercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_mercado.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_mercado.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_mercado);
  //
  // apunte_mercado = new ApunteContable();
  // apunte_mercado.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_mercado.setDescripcion(descripcionMovimiento);
  // apunte_mercado.setConcepto("Ajuste negativo por evento");
  // apunte_mercado.setImporte(mfAjuste.getSaldoAfectado());
  // apunte_mercado.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_mercado.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_mercado.setContabilizado(Boolean.FALSE);
  // apunte_mercado.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_mercado.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_mercado.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_mercado);
  //
  // // Ajuste negativo cliente
  // apunte_cliente = new ApunteContable();
  // apunte_cliente.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
  // apunte_cliente.setDescripcion(descripcionMovimiento);
  // apunte_cliente.setConcepto("Ajuste negativo por evento cliente");
  // apunte_cliente.setImporte(mfAjuste.getSaldoAfectado().abs());
  // apunte_cliente.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_cliente.setApunte(TIPO_CUENTA.DEBE.getId());
  // apunte_cliente.setContabilizado(Boolean.FALSE);
  // apunte_cliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_cliente.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_cliente.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_cliente);
  //
  // apunte_cliente = new ApunteContable();
  // apunte_cliente.setCuentaContable(cuentaContableDao.findByCuenta(1133105));
  // apunte_cliente.setDescripcion(descripcionMovimiento);
  // apunte_cliente.setConcepto("Ajuste negativo por evento cliente");
  // apunte_cliente.setImporte(mfAjuste.getSaldoAfectado());
  // apunte_cliente.setFecha(apunteContableBo.getFechaContabilizacion(Calendar.getInstance(), mfAjuste.getTrxTm()));
  // apunte_cliente.setApunte(TIPO_CUENTA.HABER.getId());
  // apunte_cliente.setContabilizado(Boolean.FALSE);
  // apunte_cliente.setOperativa(Constantes.OPERATIVA_APUNTE);
  // apunte_cliente.setTipoComprobante(TIPO_COMPROBANTE.EVENTOS.getTipo());
  // apunte_cliente.setTipoMovimiento(TIPO_APUNTE.AJUSTE_EVENTO.getId());
  // apunteContableBo.save(apunte_cliente);
  // } // else
  //
  // LOG.debug("## generateApuntesContablesAjustes ## Fin");
  // } // generateApuntesContablesAjustes

} // FallidosAjustes
