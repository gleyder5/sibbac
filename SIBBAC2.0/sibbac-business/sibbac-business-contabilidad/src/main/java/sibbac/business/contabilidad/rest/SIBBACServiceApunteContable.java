package sibbac.business.contabilidad.rest;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.contabilidad.common.PagoComision;
import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dto.PayLetterDTO;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceApunteContable implements SIBBACServiceBean {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceApunteContable.class);

  /** Lista de comandos disponibles para el servicio. */
  private static final List<String> AVAILABLE_COMMANDS = new ArrayList<String>();

  /** Mapa de campos a inicializar en la carga de la página. */
  private static final Map<String, Object> INIT_FIELDS_VALUES = new HashMap<String, Object>();

  /** Tipos de registros que se pueden dar de alta. */
  private static final List<String> SELECT_ACTION_TYPE = new ArrayList<String>();

  /** Lista de conceptos para cada tipo de acción. */
  private static final List<String> CONCEPT_FIELD_VALUES = new ArrayList<String>();

  /** Parámetro que contiene la acción realizada por el usuario. */
  private static final String PARAM_ACTION_TYPE = "actionType";

  /** Parámetro que contiene la fecha del apunte a crear. */
  private static final String PARAM_FECHA_APUNTE = "fechaApunte";

  /** Parámetro que contiene el alias para el que generar el apunte. */
  private static final String PARAM_ALIAS = "alias";

  /** Parámetro que contiene el concepto del apunte. */
  private static final String PARAM_CONCEPTO_APUNTE = "concepto";

  /** Parámetro que contiene el importe del apunte. */
  private static final String PARAM_IMPORTE_APUNTE = "importe";

  /** Parámetro que contiene el responsable que ha autorizado del apunte. */
  private static final String PARAM_RESPONSABLE = "responsable";

  /** Parámetro que contiene el El campo que chekea si los quieres cobrados o pendientes. */
  private static final String PARAM_SEARCHTYPE = "searchType";

  /** Indica que una operación ha finalizado correctamente. */
  private static final String RESULTADO_SUCCESS = "SUCCESS";

  /** Indica que una operación no ha finalizado correctamente. */
  private static final String RESULTADO_FAIL = "FAIL";

  /** Nombre de la clave que se envia en el mapa de resultados con el resultado de la operación. */
  private static final String RESULTADO = "resultado";

  /** Nombre de la clave que se envia en el mapa de resultados con el mensaje del error producido. */
  private static final String ERROR_MESSAGE = "error_message";

  /** Formateardor de números. */
  private static final NumberFormat nF = new DecimalFormat("#0.00", new DecimalFormatSymbols(new Locale("es", "ES")));

  private static final DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");

  /** Tipos de registros que se pueden dar de alta. */
  private static final List<String> SELECT_COMISSION_TYPE = new ArrayList<String>();

  /** Parámetro que contiene la fecha del apunte a crear. */
  private static final String PARAM_COMISSION_TYPE = "comissionType";

  /** Parámetro que contiene la fecha del apunte a crear. */
  private static final String PARAM_FECHA_DESDE = "fechaDesde";

  /** Parámetro que contiene la fecha del apunte a crear. */
  private static final String PARAM_FECHA_HASTA = "fechaHasta";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>TMCT0_APUNTE_CONTABLE</code>. */
  @Autowired
  private ApunteContableBo apunteContableBo;

  /** Business object para la tabla <code>TMCT0ALC</code>. */
  @Autowired
  private Tmct0alcBo tmct0alcBo;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private PagoComision pagoComision;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICO

  static {
    // Tipos de comandos disponibles para el servicio
    AVAILABLE_COMMANDS.add(Constantes.CMD_INIT);
    AVAILABLE_COMMANDS.add(Constantes.CMD_ADD);
    AVAILABLE_COMMANDS.add("initPayLetter");
    AVAILABLE_COMMANDS.add("findPayLetter");
    AVAILABLE_COMMANDS.add("generatePayLetter");

    // Tipos de acciones disponibles
    SELECT_ACTION_TYPE.add("Devolución a cliente (decisión comercial)");
    SELECT_ACTION_TYPE.add("Devolución a cliente (incidencia tecnológica o error manual)");
    SELECT_ACTION_TYPE.add("Abono de cliente a fin de periodo");
    SELECT_ACTION_TYPE.add("Liquidación de intereses");
    // SELECT_ACTION_TYPE.add("Generar cartas de pago");

    // Concepto de cada acción
    CONCEPT_FIELD_VALUES.add("Retrocesión comisión");
    CONCEPT_FIELD_VALUES.add("Retrocesión comisión");
    CONCEPT_FIELD_VALUES.add("Ingreso de comisiones");
    CONCEPT_FIELD_VALUES.add("Liquidación intereses cliente");

    // Tipos de comisión
    SELECT_COMISSION_TYPE.add("Comisión de devolución");
    SELECT_COMISSION_TYPE.add("Comisión ordenante");
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public SIBBACServiceApunteContable() {
  } // SIBBACServiceApunteContable

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
   */
  @Override
  public WebResponse process(WebRequest webRequest) {
    WebResponse webResponse = new WebResponse();
    String command = webRequest.getAction();
    LOG.debug("[SIBBACServiceApunteContable :: process] Command: " + command);
    try {
      switch (command) {
        case Constantes.CMD_INIT:
          webResponse.setResultados(formFieldsInitialization());
          break;
        case Constantes.CMD_ADD:
          webResponse.setResultados(add(webRequest.getFilters()));
          break;
        case "initPayLetter":
          webResponse.setResultados(initPayLetter());
          break;
        case "findPayLetter":
          webResponse.setResultados(findPayLetter(webRequest.getFilters()));
          break;
        // case "generatePayLetter":
        // generatePayLetter(webRequest.getFilters());
        // webResponse.setResultados(findPayLetter(webRequest.getFilters()));
        // break;
        default:
          webResponse.setError("A valid action must be set. Valid actions are: " + this.getAvailableCommands());
      } // switch
    } catch (Exception e) {
      LOG.debug("[SIBBACServiceApunteContable :: process] Error: " + e);
      LOG.debug("[SIBBACServiceApunteContable :: process] Error Message: " + e.getMessage());

      Map<String, Object> resultado = new HashMap<String, Object>();

      resultado.put(RESULTADO, RESULTADO_FAIL);
      resultado.put(ERROR_MESSAGE, e.getMessage());

      webResponse.setResultados(resultado);
    } // catch

    return webResponse;
  } // process

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    return AVAILABLE_COMMANDS;
  } // getAvailableCommands

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  @Override
  public List<String> getFields() {
    return null;
  } // getFields

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Devuelve un mapa con los datos de inicialización de los campos del formulario.<br>
   * Los objetos que se devuelven son:<br>
   * 1.- actionType. Lista que contiene las posibles acciones a realizar.<br>
   * 2.- actionConcept. Lista que contiene los conceptos de cada una de las acciones anteriores.
   * 
   * @return <code>Map<String, Object> - </code>Datos para la inicialización de los campos del formulario. El nombre del objeto que devuelve es
   *         <code>datosInicializacion</code>.
   */
  private Map<String, Object> formFieldsInitialization() {
    LOG.debug("Inicializando campos del formulario ...");

    // Mapa de valores de inicialización de los campos del formulario
    INIT_FIELDS_VALUES.put("actionType", SELECT_ACTION_TYPE);
    INIT_FIELDS_VALUES.put("actionConcept", CONCEPT_FIELD_VALUES);

    Map<String, Object> datosInicializacion = new HashMap<String, Object>();
    datosInicializacion.put("datosInicializacion", INIT_FIELDS_VALUES);

    return datosInicializacion;
  } // formFieldsInitialization

  /**
   * Crea un nuevo apunte contable con los datos recibidos.
   * 
   * @param list Lista con los datos de los apuntes a realizar.
   * @return <code>Map<String, Object> - </code>Resultado de la operación.
   * @throws Exception Se lanza si la acción recibida no es válida o no se han recibidos los filtros necesarios.
   */
  private Map<String, Object> add(Map<String, String> filters) throws Exception {
    LOG.debug("[SIBBACServiceApunteContable :: add] Insertando un apunte contable ...");

    if (MapUtils.isNotEmpty(filters)) {
      apunteContableBo.generateAndSaveApunteContableManual(NumberUtils.createInteger(filters.get(PARAM_ACTION_TYPE)).intValue(),
                                                           dF.parse(filters.get(PARAM_FECHA_APUNTE)), filters.get(PARAM_ALIAS),
                                                           filters.get(PARAM_CONCEPTO_APUNTE),
                                                           new BigDecimal(nF.parse(filters.get(PARAM_IMPORTE_APUNTE)).toString()),
                                                           filters.get(PARAM_RESPONSABLE));
    } else {
      LOG.error("[SIBBACServiceApunteContable :: add] Filtros no recibidos ...");
      throw new Exception("Filtros no recibidos");
    } // else

    Map<String, Object> resultado = new HashMap<String, Object>();
    resultado.put(RESULTADO, RESULTADO_SUCCESS);

    return resultado;
  } // add

  /**
   * Devuelve un mapa con los datos de inicialización de los campos del formulario.<br>
   * Los objetos que se devuelven son:<br>
   * 1.- actionType. Lista que contiene las posibles acciones a realizar.<br>
   * 2.- actionConcept. Lista que contiene los conceptos de cada una de las acciones anteriores.
   * 
   * @return <code>Map<String, Object> - </code>Datos para la inicialización de los campos del formulario. El nombre del objeto que devuelve es
   *         <code>datosInicializacion</code>.
   */
  private Map<String, Object> initPayLetter() {
    LOG.debug("Inicializando campos del formulario ...");

    // Mapa de valores de inicialización de los campos del formulario
    INIT_FIELDS_VALUES.put("comissionType", SELECT_COMISSION_TYPE);

    Map<String, Object> datosInicializacion = new HashMap<String, Object>();
    datosInicializacion.put("datosInicializacion", INIT_FIELDS_VALUES);

    return datosInicializacion;
  } // formFieldsInitialization

  /**
   * 
   * @param filters
   * @return
   * @throws Exception
   */
  private Map<String, List<PayLetterDTO>> findPayLetter(Map<String, String> filters) throws Exception {
    LOG.debug("[findPayLetter] Inicio ...");

    Map<String, List<PayLetterDTO>> resultado = null;
    List<PayLetterDTO> listaValores = null;

    if (MapUtils.isNotEmpty(filters)) {
      if (NumberUtils.createInteger(filters.get(PARAM_COMISSION_TYPE)).intValue() == 0) { // Comisión de devolución
        if (filters.get(PARAM_SEARCHTYPE) != null && filters.get(PARAM_SEARCHTYPE).compareTo("true") == 0) {
          // Se buscan las ya cobradas
          listaValores = tmct0alcBo.findAllDataAndBrkByFechaAndAlias(filters.get(PARAM_ALIAS), dF.parse(filters.get(PARAM_FECHA_DESDE)),
                                                                      dF.parse(filters.get(PARAM_FECHA_HASTA)),
                                                                      TIPO_ESTADO.COMISION_DEVOLUCION.getId(), CONTABILIDAD.COBRADA.getId());
        } else {
          // Se buscan las no cobradas
          listaValores = tmct0alcBo.findAllDataAndBrkByFechaAndAlias(filters.get(PARAM_ALIAS), dF.parse(filters.get(PARAM_FECHA_DESDE)),
                                                                      dF.parse(filters.get(PARAM_FECHA_HASTA)),
                                                                      TIPO_ESTADO.COMISION_DEVOLUCION.getId(),
                                                                      CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId());
        } // else
      } else if (NumberUtils.createInteger(filters.get(PARAM_COMISSION_TYPE)).intValue() == 1) { // Comisión ordenante
        if (filters.get(PARAM_SEARCHTYPE) != null && filters.get(PARAM_SEARCHTYPE).compareTo("true") == 0) {
          // Se buscan las ya cobradas
          listaValores = tmct0alcBo.findAllDataAndDvoByFechaAndAlias(filters.get(PARAM_ALIAS), dF.parse(filters.get(PARAM_FECHA_DESDE)),
                                                                      dF.parse(filters.get(PARAM_FECHA_HASTA)),
                                                                      TIPO_ESTADO.COMISION_ORDENANTE.getId(), CONTABILIDAD.COBRADA.getId());
        } else {
          // Se buscan las no cobradas
          listaValores = tmct0alcBo.findAllDataAndDvoByFechaAndAlias(filters.get(PARAM_ALIAS), dF.parse(filters.get(PARAM_FECHA_DESDE)),
                                                                      dF.parse(filters.get(PARAM_FECHA_HASTA)),
                                                                      TIPO_ESTADO.COMISION_ORDENANTE.getId(),
                                                                      CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId());
        } // else
      } else {
        throw new Exception("Tipo de comisión no válida: [" + filters.get(PARAM_COMISSION_TYPE) + "]");
      } // else

      resultado = new HashMap<String, List<PayLetterDTO>>();
      resultado.put(RESULTADO, listaValores);

    } else {
      LOG.error("[findPayLetter] Filtros no recibidos ...");
      throw new Exception("Filtros no recibidos");
    } // else

    return resultado;
  } // findPayLetter

} // SIBBACServiceApunteContable
