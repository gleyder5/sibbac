package sibbac.business.contabilidad.database.dao;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Repository
public class ApunteContableAlcDaoImp {
	private static final String queryInsert = "insert into tmct0_apunte_contable_alc (nbooking,nuorden,nucnfliq,nucnfclt,"
			+ "apunte,importe) values (:nbooking,:nuorden,:nucnfliq,:nucnfclt,:apunte,:importe)";
	private static final String sumDevengoCorretaje = "SELECT sum(apt_alc.importe) FROM tmct0_apunte_contable_alc "
			+ "apt_alc,tmct0_apunte_contable apt where apt_alc.APUNTE=apt.ID and apt.tipo_comprobante in (70,72,75) "
			+ "and tipo_movimiento=1 and apt.apunte='D' and apt_alc.NUORDEN=:nuorden and apt_alc.NBOOKING=:nbooking "
			+ "and apt_alc.NUCNFCLT=:nucnfclt and apt_alc.NUCNFLIQ=:nucnfliq";
	private static final String sumCuentaTipo = "SELECT sum(a.importe) FROM TMCT0_APUNTE_CONTABLE_ALC a,"
			+ "TMCT0_APUNTE_CONTABLE b,TMCT0_CUENTA_CONTABLE c WHERE a.apunte=b.id and b.cuenta=c.id and c.cuenta=:cuenta "
			+ "and b.TIPO_COMPROBANTE=:tipo_comprobante and a.NUORDEN=:nuroden and a.NBOOKING=:nbooking "
			+ "and a.NUCNFCLT=:nucnfclt and a.NUCNFLIQ=:nucnfliq";

	@PersistenceContext
	private EntityManager	em;

	public void insert(String nbooking, String nuorden, Short nucnfliq, String nucnfclt, Long apunte, BigDecimal importe) {
		Query query = em.createNativeQuery(queryInsert);
		query.setParameter("nbooking", nbooking);
		query.setParameter("nuorden", nuorden);
		query.setParameter("nucnfliq", nucnfliq);
		query.setParameter("nucnfclt", nucnfclt);
		query.setParameter("apunte", apunte);
		query.setParameter("importe", importe);
		query.executeUpdate();
	}

	public void insert(Long apunte, List<Map<String,Object>> list) {
		Tmct0alcId alcId;
		for (Map<String,Object> bean: list) {
			alcId = (Tmct0alcId) bean.get("alcId");
			insert(alcId.getNbooking(), alcId.getNuorden(), alcId.getNucnfliq(),
					alcId.getNucnfclt(), apunte, (BigDecimal) bean.get("importe"));
		}
	}

	public BigDecimal sumDevengoCorretaje(String nuorden, String nbooking, String nucnfclt, Short nucnfliq) {
		Query query = em.createNativeQuery(sumDevengoCorretaje);
		query.setParameter("nuorden", nuorden);
		query.setParameter("nbooking", nbooking);
		query.setParameter("nucnfclt", nucnfclt);
		query.setParameter("nucnfliq", nucnfliq);
		BigDecimal bD = (BigDecimal) query.getSingleResult();
		return bD==null? BigDecimal.ZERO: bD;
	}

	public BigDecimal sumCuentaTipo(int cuenta, int tipo_comprobante, Tmct0alc alc) {
		Query query = em.createNativeQuery(sumCuentaTipo);
		query.setParameter("nuorden", alc.getNuorden());
		query.setParameter("nbooking", alc.getNbooking());
		query.setParameter("nucnfclt", alc.getNucnfclt());
		query.setParameter("nucnfliq", alc.getNucnfliq());
		query.setParameter("cuenta", cuenta);
		query.setParameter("tipo_comprobante", tipo_comprobante);
		BigDecimal bD = (BigDecimal) query.getSingleResult();
		return bD==null? BigDecimal.ZERO: bD.setScale(4, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal sumDevengosCliente(Tmct0alc alc) {
		return sumCuentaTipo(1122107, 72, alc);
	}
}
