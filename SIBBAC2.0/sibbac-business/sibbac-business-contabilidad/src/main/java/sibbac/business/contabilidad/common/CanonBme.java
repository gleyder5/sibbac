package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Enumerados.BOLSA;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;

@Service
public class CanonBme {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CanonBme.class);

  @Autowired
  private Tmct0alcDaoImp alcDao;

  @Autowired
  private CuentaContableDao cuentaDao;

  @Autowired
  private Devengo devengo;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  // public void put(Calendar mesPasado, Calendar hoy) {
  public void put(Calendar primerDiaMes, Calendar ultimoDiaMes, Calendar hoy) {
    LOG.debug("[CanonBme :: put] Inicio ...");
    List<String> cuentaPropiaList = new ArrayList<String>(2);
    cuentaPropiaList.add("00P");
    cuentaPropiaList.add("01P");

    BOLSA bolsa;
    Auxiliar auxiliar = null;
    List<Object[]> list;
    ApunteContable apunte;
    BigDecimal suma;
    // BigDecimal importe;
    Long idAuxiliar;

    CuentaContable cuentaContable;

    Map<BOLSA, List<Object[]>> mClientes = new HashMap<BOLSA, List<Object[]>>();
    Map<Auxiliar, List<Object[]>> mCartera = new HashMap<Auxiliar, List<Object[]>>();
    Map<BOLSA, List<Object[]>> mBolsa = new HashMap<BOLSA, List<Object[]>>();

    Map<String, Object> params = new HashMap<String, Object>();
    Map<Long, Auxiliar> mAuxiliar = new HashMap<Long, Auxiliar>();
    Map<BOLSA, Auxiliar> mAuxiliarBolsa = new HashMap<BOLSA, Auxiliar>();

    for (Object[] objects : alcDao.findCanonBme(cuentaPropiaList, Tmct0ali.fhfinActivos, 9, primerDiaMes.getTime(), ultimoDiaMes.getTime(),
                                                ASIGNACIONES.PDTE_LIQUIDAR.getId())) {
      bolsa = BOLSA.find(Integer.parseInt((String) objects[3]));
      if (((int) objects[0]) == 0) {
        if ((list = mClientes.get(bolsa)) == null) {
          mClientes.put(bolsa, list = new ArrayList<Object[]>());
        }
        list.add(objects);
      } else {
        if (mAuxiliar.containsKey(idAuxiliar = objects[8] != null && objects[8] != "" ? Long.valueOf(objects[8].toString()) : null)) {
          auxiliar = mAuxiliar.get(idAuxiliar);
        } else {
          mAuxiliar.put(idAuxiliar, auxiliar = (idAuxiliar != null ? auxiliarDao.findOne(idAuxiliar) : null));
        }

        if ((list = mCartera.get(auxiliar)) == null) {
          mCartera.put(auxiliar, list = new ArrayList<Object[]>());
        }
        list.add(objects);
      } // else

      if ((list = mBolsa.get(bolsa)) == null) {
        mBolsa.put(bolsa, list = new ArrayList<Object[]>());
      }
      list.add(objects);
    } // for

    // Clientes
    params.put("contabilizado", false);
    params.put("operativa", "N");
    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.CANONES);
    params.put("fechFactura", hoy.getTime());
    params.put("sApunte", "D");
    params.put("concepto", "Gastos Canones BME");
    params.put("TIPO_APUNTE", TIPO_APUNTE.CANON_BME_CLIENTE.getId());
    params.put("cuenta", cuentaContable = cuentaDao.findByCuenta(3021210));
    for (Entry<BOLSA, List<Object[]>> entry : mClientes.entrySet()) {
      params.put("descripcion", "Canon " + entry.getKey().name());
      apunte = new ApunteContable(params);
      suma = BigDecimal.ZERO;
      for (Object[] objects : entry.getValue()) {
        // suma = suma.add(importe = (BigDecimal) objects[2]);
        // apunte.add(new Tmct0alc(new Tmct0alcId((String) objects[5], (String) objects[4], ((BigDecimal) objects[7]).shortValue(), (String)
        // objects[6])),
        // importe);
        suma = suma.add((BigDecimal) objects[2]);
      }
      apunte.setImporte(suma);
      devengo.putApunte(apunte);
    }

    // Cartera
    params.put("TIPO_APUNTE", TIPO_APUNTE.CANON_BME_CARTERAS.getId());
    params.put("cuenta", cuentaContable = cuentaDao.findByCuenta(3034101));
    for (Entry<Auxiliar, List<Object[]>> entry : mCartera.entrySet()) {
      params.put("descripcion", "Canon cuenta propia " + ((auxiliar = entry.getKey()) != null ? auxiliar.getNombre() : ""));
      params.put("auxiliar", auxiliar);
      apunte = new ApunteContable(params);
      suma = BigDecimal.ZERO;
      for (Object[] objects : entry.getValue()) {
        // suma = suma.add(importe = (BigDecimal) objects[2]);
        // apunte.add(new Tmct0alc(new Tmct0alcId((String) objects[5], (String) objects[4], ((BigDecimal) objects[7]).shortValue(), (String)
        // objects[6])),
        // importe);
        suma = suma.add((BigDecimal) objects[2]);
      }
      apunte.setImporte(suma);
      devengo.putApunte(apunte);
    }

    // Clientes + cartera
    params.put("sApunte", "H");
    params.put("concepto", "Canones BME");
    params.put("TIPO_APUNTE", TIPO_APUNTE.CANON.getId());
    params.put("cuenta", cuentaContable = cuentaDao.findByCuenta(2132105));
    for (Entry<BOLSA, List<Object[]>> entry : mBolsa.entrySet()) {
      params.put("descripcion", "Canon " + (bolsa = entry.getKey()).name());

      if ((auxiliar = mAuxiliarBolsa.get(bolsa)) == null) {
        mAuxiliarBolsa.put(bolsa, auxiliar = auxiliarDao.findByNombreAndCuentaContable(bolsa.getNombreAuxiliar(), cuentaContable));
      }
      params.put("auxiliar", auxiliar);

      apunte = new ApunteContable(params);
      suma = BigDecimal.ZERO;
      for (Object[] objects : entry.getValue()) {
        // suma = suma.add(importe = (BigDecimal) objects[2]);
        // apunte.add(new Tmct0alc(new Tmct0alcId((String) objects[5], (String) objects[4], ((BigDecimal) objects[7]).shortValue(), (String)
        // objects[6])),
        // importe.negate());
        suma = suma.add((BigDecimal) objects[2]);
      }
      apunte.setImporte(suma.negate());
      devengo.putApunte(apunte);
    }

    LOG.debug("[CanonBme :: put] Fin ...");
  } // put

} // CanonBme
