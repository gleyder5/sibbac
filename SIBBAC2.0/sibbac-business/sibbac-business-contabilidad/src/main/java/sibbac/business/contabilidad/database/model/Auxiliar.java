package sibbac.business.contabilidad.database.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.contabilidad.Auxiliar}.
 */
@Table( name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.AUXILIAR )
@Entity
@Component
@XmlRootElement
@Audited
public class Auxiliar extends ATableAudited< Auxiliar > {

	private static final long		serialVersionUID	= 927963211853488035L;

	/**
	 * Numeración de auxiliar
	 */
	@Column( name = "NUMERO", length = 45, unique=true )
	@XmlAttribute
	private String					numero;

	/**
	 * Nombre de auxiliar
	 */
	@Column( name = "NOMBRE" )
	@XmlAttribute
	private String					nombre;

	/**
	 * Cuenta Contable asociada
	 */
	@ManyToOne( targetEntity = CuentaContable.class, cascade = CascadeType.PERSIST )
	@JoinColumn( name = "CUENTA", nullable = false )
	@XmlAttribute
	private CuentaContable			cuentaContable;

	public Auxiliar() {
	}

	public Auxiliar( String numero, String nombre, CuentaContable cuentaContable ) {
		this.numero = numero;
		this.nombre = nombre;
		this.cuentaContable = cuentaContable;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero( String numero ) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public CuentaContable getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable( CuentaContable cuentaContable ) {
		this.cuentaContable = cuentaContable;
	}
}
