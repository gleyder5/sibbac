package sibbac.business.contabilidad.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.model.CuentaBancaria;

@Component
public interface CuentaBancariaDao extends JpaRepository<CuentaBancaria, Long> {
	public CuentaBancaria findByIban(String iban);
}
