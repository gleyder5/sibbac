package sibbac.business.contabilidad.database.bo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43FicheroDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDaoImpl;
import sibbac.business.contabilidad.common.CanonBme;
import sibbac.business.contabilidad.common.CobrosClearing;
import sibbac.business.contabilidad.common.CobrosClearingCliente;
import sibbac.business.contabilidad.common.CobrosClearingMercado;
import sibbac.business.contabilidad.common.CobrosComisionesBME;
import sibbac.business.contabilidad.common.CobrosRouting;
import sibbac.business.contabilidad.common.Contable;
import sibbac.business.contabilidad.common.Desdotacion;
import sibbac.business.contabilidad.common.Devengo;
import sibbac.business.contabilidad.common.DevengoClearing;
import sibbac.business.contabilidad.common.DevengoCorretaje;
import sibbac.business.contabilidad.common.DevengoFacturacion;
import sibbac.business.contabilidad.common.DevengoRouting;
import sibbac.business.contabilidad.common.Dotacion;
import sibbac.business.contabilidad.common.FacturaAnulacion;
import sibbac.business.contabilidad.common.FacturaCierre;
import sibbac.business.contabilidad.common.FacturaCobro;
import sibbac.business.contabilidad.common.FacturaEmision;
import sibbac.business.contabilidad.common.FallidosAjustes;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.ApunteContableDao;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CierreContable;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_CUENTA;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.AlcEstadoContBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dao.AlcEstadoContDaoImp;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.business.wrappers.database.dao.CobrosDao;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dto.PayLetterDTO;
import sibbac.business.wrappers.database.dto.RechazoApunteDto;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

/**
 * Business object de la tala ApunteContable.
 * 
 * @author XI316153
 * @see ApunteContable
 * @see ApunteContableDao
 */
@Service
public class ApunteContableBo extends AbstractBo<ApunteContable, Long, ApunteContableDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Valor del campo <code>TMCT0CFG_APPLICATION</code> de la tabla <code>tmct0cfg</code> para la obtención de valores de la misma. */
  public static final String TMCT0CFG_APPLICATION = "SBRMV";

  /** Valor del campo <code>TMCT0CFG_PROCESS</code> de la tabla <code>tmct0cfg</code> para la obtención de valores de la misma. */
  public static final String TMCT0CFG_PROCESS = "CONTABILIDAD";

  /** Valor del campo <code>keyname</code> de la tabla <code>tmct0cfg</code> para la obtención de valores de la misma. */
  public static final String TMCT0CFG_MIEMBROCOMPENSADOR = "clientes.devengo.routing";

  /** Lista de estados de asignación a procesar. */
  public static final List<Integer> ESTADOSASIG = new ArrayList<Integer>();

  /** Lista de estados de contabilidad a procesar. */
  public static final List<Integer> ESTADOSCONT_PDTEDEVENGO = new ArrayList<Integer>();

  /** Formato de las fechas a procesar. */
  private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");

  /** Contenido del campo <code>cdestliq</code> de los ALCs a devengar. */
  private static final String ALC_CDESTLIQ = "A";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para acceder a la tabla <code>tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  private AlcEstadoContBo alcEstadoBo;

  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDaoImp;

  @Autowired
  private AliasDao aliasDao;

  @Autowired
  private AuxiliarDao auxiliarDao;

  @Autowired
  private ApunteContableAlcDao apunteAlcDao;

  @Autowired
  private ApunteContableAlcDaoImp apunteAlcDaoImp;

  @Autowired
  private CobrosDao cobroDao;

  @Autowired
  private Contable contable;

  @Autowired
  private CuentaContableDao cuentaContableDao;

  @Autowired
  private CobrosClearingMercado cobrosClearingMercado;

  @Autowired
  private CierreContableBo cierreBo;

  @Autowired
  private CobrosClearingCliente cobrosClearingCliente;

  @Autowired
  private CanonBme canonBme;

  @Autowired
  private DevengoClearing devengoClearing;

  @Autowired
  private DevengoCorretaje devengoCorretaje;

  @Autowired
  private DevengoFacturacion devengoFacturacion;

  @Autowired
  private DevengoRouting devengoRouting;

  @Autowired
  private Devengo devengo;

  @Autowired
  private Dotacion dotacion;

  @Autowired
  private Desdotacion desdotacion;

  @Autowired
  private FacturaDao facturaDao;

  @Autowired
  private FacturaEmision facturaEmision;

  @Autowired
  private FacturaCobro facturaCobro;

  @Autowired
  private FacturaCierre facturaCierre;

  @Autowired
  private FacturaAnulacion facturaAnulacion;

  @Autowired
  private FallidosAjustes fallidosAjustes;

  @Autowired
  private Norma43FicheroDao n43FicheroDao;

  @Autowired
  private Norma43MovimientoDaoImpl n43MovimientoDao;

  @Autowired
  private CobrosRouting cobrosRouting;

  @Autowired
  private CobrosClearing cobrosClearing;

  @Autowired
  private CobrosComisionesBME cobrosComisionesBME;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0alcDaoImp alcDaoImp;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private Tmct0cfgBo cfgBo;

  /** Margen de diferencia entre el importe Norma43 y calculado de los alc a partir del cual se generará descuadre. */
  @Value("${norma43.margen.descuadres}")
  private BigDecimal margen_descuadres;

  /** Número máximo de ALCS a procesar en una transacción. */
  @Value("${devengos.transaction.size}")
  private int transaction_size;

  /** Estados que se utilizan durante el proceso. */
  private HashMap<String, Tmct0estado> estadosMap;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICO

  static {
    for (ASIGNACIONES value : ASIGNACIONES.values()) {
      if (value.getId() >= ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
        ESTADOSASIG.add(value.getId());
      }
    } // for

    ESTADOSCONT_PDTEDEVENGO.add(CONTABILIDAD.PDTE_DEVENGO.getId());
    ESTADOSCONT_PDTEDEVENGO.add(CONTABILIDAD.PDTE_AJUSTE.getId());
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * 
   */
  @Transactional
  public void put() {
    try {
      LOG.debug("[put] Iniciando tarea de contabilidad ...");
      Tmct0men tmct0men = tmct0menBo.putEstadoMEN(EstadosEnumerados.TIPO_APUNTE.CONTABILIDAD, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);

      try {
        put(EstadosEnumerados.TIPO_APUNTE.FACTURAS_EMITIDAS);
        put(EstadosEnumerados.TIPO_APUNTE.FACTURAS_COBROS);
        put(EstadosEnumerados.TIPO_APUNTE.FACTURAS_CERRADAS);
        put(EstadosEnumerados.TIPO_APUNTE.FACTURAS_ANULADAS);
        put(EstadosEnumerados.TIPO_APUNTE.EMISION_DEVENGOS_ROUTING);
        put(EstadosEnumerados.TIPO_APUNTE.EMISION_DEVENGOS_CLEARING);
        put(EstadosEnumerados.TIPO_APUNTE.EMISION_DEVENGOS_CORRETAJES);
        put(EstadosEnumerados.TIPO_APUNTE.EMISION_DEVENGOS_FACTURACION);
        put(EstadosEnumerados.TIPO_APUNTE.EMISION_RETROCESIONES_DEVENGOS);
        put(EstadosEnumerados.TIPO_APUNTE.COBROS_ROUTING);
        put(EstadosEnumerados.TIPO_APUNTE.COBROS_CLEARING_NETTING_MERCADO);
        put(EstadosEnumerados.TIPO_APUNTE.COBROS_CLEARING_NETTING_CLIENTE);
        put(EstadosEnumerados.TIPO_APUNTE.FALLIDOS_AJUSTES);
        put(EstadosEnumerados.TIPO_APUNTE.DOTACION);
        put(EstadosEnumerados.TIPO_APUNTE.DESDOTACION);
        put(EstadosEnumerados.TIPO_APUNTE.CANON_BME);
        put(EstadosEnumerados.TIPO_APUNTE.COBROS_COMISIONES_BME);

        putAS400();

        tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.LISTO_GENERAR);
        LOG.debug("[put] Finalizada correctamente la tarea de contabilidad ...");
      } catch (Exception e) {
        tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.EN_ERROR);
        throw e;
      } // catch
    } catch (Exception e) {
      if (e instanceof SIBBACBusinessException && e.getMessage().startsWith("No hay registros en Men del tipo ")) {
        LOG.warn("[put] " + e.getMessage());
      } else {
        LOG.error("[put] Error ejecutando la tarea de contabilidad ... ", e);
      } // else
    } // catch
  } // put

  /**
   * Realiza los apuntes contables de cobro del tipo de comisión especificada (devolución u ordenante).
   * 
   * @param alias Alias para e que buscar las operaciones a cobrar.
   * @param tipoComision Tipo de comisión a cobrar.
   * @param fechaInicio Fecha desde la que buscar las operaciones a cobrar.
   * @param fechaFinal Fecha hasta la qu buscar las operaciones a cobrar.
   * @return <code>List<Tmct0alc> - </code>Lista de operaciones cobradas.
   * @throws SIBBACBusinessException Si ocurre algún error durante el proceso.
   */
  @Transactional
  public List<PayLetterDTO> generatePayLetter(String alias, String tipoComision, Date fechaInicio, Date fechaFinal) throws SIBBACBusinessException {
    LOG.debug("[generatePayLetter] Inicio ...");
    ApunteContable apunte_contable_debe;
    ApunteContable apunte_contable_haber;

    Date fechaApunte;
    String descripcionApunte;
    Auxiliar auxiliarContable;
    Calendar hoy = Calendar.getInstance();

    CuentaContable cuentaContable_2015208 = cuentaContableDao.findByCuenta(2015208);
    CuentaContable cuentaContable_1021101 = cuentaContableDao.findByCuenta(1021101);

    Integer tipoComprobante = "0".equals(tipoComision) ? TIPO_COMPROBANTE.PAGO_COMISION_DEVOLUCION.getTipo()
                                                      : TIPO_COMPROBANTE.PAGO_COMISION_DEVOLUCION.getTipo();
    TIPO_ESTADO tipoEstado = "0".equals(tipoComision) ? TIPO_ESTADO.COMISION_DEVOLUCION : TIPO_ESTADO.COMISION_ORDENANTE;
    Tmct0estado cobrada = estadoDao.findOne(CONTABILIDAD.COBRADA.getId());
    Tmct0estado devengadaPdteCobro = estadoDao.findOne(CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId());

    List<PayLetterDTO> payLetterDTOList = null;
    if ("0".equals(tipoComision)) { // Comisión de devolución
      payLetterDTOList = alcBo.findAllDataAndBrkByFechaAndAlias(alias, fechaInicio, fechaFinal, TIPO_ESTADO.COMISION_DEVOLUCION.getId(),
                                                                devengadaPdteCobro.getIdestado());
    } else {
      payLetterDTOList = alcBo.findAllDataAndDvoByFechaAndAlias(alias, fechaInicio, fechaFinal, TIPO_ESTADO.COMISION_ORDENANTE.getId(),
                                                                devengadaPdteCobro.getIdestado());
    } // else

    for (PayLetterDTO payLetterDTO : payLetterDTOList) {
      fechaApunte = getFechaContabilizacion(hoy, payLetterDTO.getFeejeliq());
      descripcionApunte = alias.trim() + "/" + dF1.format(payLetterDTO.getFeejeliq()) + "/" + payLetterDTO.getNbtitliq();
      auxiliarContable = auxiliarDao.findByNuorden(payLetterDTO.getNuorden());

      // Apunte al debe
      apunte_contable_debe = new ApunteContable();
      apunte_contable_debe.setCuentaContable(cuentaContable_2015208);
      apunte_contable_debe.setDescripcion(descripcionApunte);
      apunte_contable_debe.setFecha(fechaApunte);
      apunte_contable_debe.setApunte(TIPO_CUENTA.DEBE.getId());
      apunte_contable_debe.setContabilizado(Boolean.FALSE);
      apunte_contable_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_contable_debe.setTipoComprobante(tipoComprobante);
      apunte_contable_debe.setAuxiliar(auxiliarContable);

      // Apunte al haber
      apunte_contable_haber = new ApunteContable();
      apunte_contable_haber.setCuentaContable(cuentaContable_1021101);
      apunte_contable_haber.setDescripcion(descripcionApunte);
      apunte_contable_haber.setFecha(fechaApunte);
      apunte_contable_haber.setApunte(TIPO_CUENTA.HABER.getId());
      apunte_contable_haber.setContabilizado(Boolean.FALSE);
      apunte_contable_haber.setOperativa(Constantes.OPERATIVA_APUNTE);
      apunte_contable_haber.setTipoComprobante(tipoComprobante);
      apunte_contable_haber.setAuxiliar(auxiliarContable);

      if (TIPO_ESTADO.COMISION_DEVOLUCION == tipoEstado) {
        // Pagos de comisión de devolución
        apunte_contable_debe.setTipoMovimiento(TIPO_APUNTE.COMISION_DEVOLUCION.getId());
        apunte_contable_haber.setTipoMovimiento(TIPO_APUNTE.COMISION_DEVOLUCION.getId());
        apunte_contable_debe.setConcepto("Pago Com. Devolución");
        apunte_contable_haber.setConcepto("Pago Com. Devolución");
        // importeApunte = alc.getImcombrk();
      } else {
        // Pagos de comisión de ordenante
        apunte_contable_debe.setTipoMovimiento(TIPO_APUNTE.COMISION_ORDENANTE.getId());
        apunte_contable_haber.setTipoMovimiento(TIPO_APUNTE.COMISION_ORDENANTE.getId());
        apunte_contable_debe.setConcepto("Pago Com. Ordenante");
        apunte_contable_haber.setConcepto("Pago Com. Ordenante");
        // importeApunte = alc.getImcomdvo();
      }

      apunte_contable_debe.setImporte(payLetterDTO.getDevolucion());
      apunte_contable_haber.setImporte(payLetterDTO.getDevolucion().negate());

      dao.save(apunte_contable_debe);
      apunteAlcDaoImp.insert(payLetterDTO.getNbooking(), payLetterDTO.getNuorden(), payLetterDTO.getNucnfliq(), payLetterDTO.getNucnfclt(),
                             apunte_contable_debe.getId(), apunte_contable_debe.getImporte());

      dao.save(apunte_contable_haber);
      apunteAlcDaoImp.insert(payLetterDTO.getNbooking(), payLetterDTO.getNuorden(), payLetterDTO.getNucnfliq(), payLetterDTO.getNucnfclt(),
                             apunte_contable_haber.getId(), apunte_contable_haber.getImporte());

      alcEstadoContDaoImp.updateOrInsertCdestadocont(cobrada.getIdestado(), payLetterDTO.getNbooking(), payLetterDTO.getNuorden(),
                                                     payLetterDTO.getNucnfliq(), payLetterDTO.getNucnfclt(), tipoEstado.getId());
      alcDaoImp.updateCdestadocont(cobrada.getIdestado(),
                                   new Tmct0alcId(payLetterDTO.getNbooking(), payLetterDTO.getNuorden(), payLetterDTO.getNucnfliq(),
                                                  payLetterDTO.getNucnfclt()));

    } // for (Tmct0alc alc : alcBo.getDao().findAlc(alias, fechaInicio, fechaFinal))

    LOG.debug("[generatePayLetter] Fin ...");
    return payLetterDTOList;
  } // generatePayLetter

  /**
   * Realiza los apuntes contables de los datos recibidos.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  @Transactional
  public void generateAndSaveApunteContableManual(int accion, Date fecha, String alias, String concepto, BigDecimal importe, String responsable) throws Exception {
    LOG.debug("[generateAndSaveApunteContableManual] Inicio ...");
    ApunteContable apunte_contable_debe = new ApunteContable();
    ApunteContable apunte_contable_haber = new ApunteContable();

    String descrali = aliasDao.findByCdaliass(alias).getDescrali().trim();

    switch (accion) {
      case 0:
        apunte_contable_debe.setCuentaContable(cuentaContableDao.findByCuenta(4021102));
        apunte_contable_debe.setDescripcion("Retroc. Com." + descrali + (responsable.isEmpty() ? "" : "(" + responsable + ")"));
        apunte_contable_debe.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_DEVOLUCION.getId());

        apunte_contable_haber.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
        apunte_contable_haber.setDescripcion("Retroc. Com." + descrali + (responsable.isEmpty() ? "" : "(" + responsable + ")"));
        apunte_contable_haber.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_DEVOLUCION.getId());
        break;
      case 1:
        apunte_contable_debe.setCuentaContable(cuentaContableDao.findByCuenta(3021501));
        apunte_contable_debe.setDescripcion("Retroc. Com." + descrali + (responsable.isEmpty() ? "" : "(" + responsable + ")"));
        apunte_contable_debe.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_DEVOLUCION.getId());

        apunte_contable_haber.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
        apunte_contable_haber.setDescripcion("Retroc. Com." + descrali + (responsable.isEmpty() ? "" : "(" + responsable + ")"));
        apunte_contable_haber.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_DEVOLUCION.getId());
        break;
      case 2:
        apunte_contable_debe.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
        apunte_contable_debe.setDescripcion(alias + " / Concepto Pago / " + descrali);
        apunte_contable_debe.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_ABONO.getId());

        apunte_contable_haber.setCuentaContable(cuentaContableDao.findByCuenta(4021101));
        apunte_contable_haber.setDescripcion(alias + " / Concepto Pago / " + descrali);
        apunte_contable_haber.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_ABONO.getId());
        break;
      case 3:
        apunte_contable_debe.setCuentaContable(cuentaContableDao.findByCuenta(1021101));
        apunte_contable_debe.setDescripcion(FormatDataUtils.convertDateToString(fecha, "yyyyMMdd") + " / " + alias + descrali);
        apunte_contable_debe.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_LIQUIDACION.getId());

        apunte_contable_haber.setCuentaContable(cuentaContableDao.findByCuenta(4029406));
        apunte_contable_haber.setDescripcion(FormatDataUtils.convertDateToString(fecha, "yyyyMMdd") + " / " + alias + descrali);
        apunte_contable_haber.setTipoMovimiento(TIPO_APUNTE.APUNTE_MANUAL_LIQUIDACION.getId());
        break;
      default:
        throw new Exception("La acción '" + accion + "' no existe.");
    } // switch

    apunte_contable_debe.setConcepto(concepto);
    apunte_contable_haber.setConcepto(concepto);

    apunte_contable_debe.setImporte(importe);
    apunte_contable_haber.setImporte(importe.negate());

    apunte_contable_debe.setFecha(fecha);
    apunte_contable_haber.setFecha(fecha);

    apunte_contable_debe.setApunte(TIPO_CUENTA.DEBE.getId());
    apunte_contable_haber.setApunte(TIPO_CUENTA.HABER.getId());

    apunte_contable_debe.setContabilizado(Boolean.FALSE);
    apunte_contable_haber.setContabilizado(Boolean.FALSE);

    apunte_contable_debe.setOperativa(Constantes.OPERATIVA_APUNTE);
    apunte_contable_haber.setOperativa(Constantes.OPERATIVA_APUNTE);

    apunte_contable_debe.setTipoComprobante(TIPO_COMPROBANTE.DEVOLUCION_COMERCIAL.getTipo());
    apunte_contable_haber.setTipoComprobante(TIPO_COMPROBANTE.DEVOLUCION_COMERCIAL.getTipo());

    dao.save(apunte_contable_debe);
    dao.save(apunte_contable_haber);

    LOG.debug("[generateAndSaveApunteContableManual] Inicio ...");
  } // generateAndSaveApunteContableManual

  /**
   * Calcula el periodo comprobante de la fecha especificada.
   * 
   * @param fecha Fecha de la que calcular el periodo comprobante.
   * @return <code>int - </code> Periodo comprobante.
   */
  public static int getPeriodoComprobante(final Date fecha) {
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(fecha);

    if (calendar.get(Calendar.MONTH) == 11 && calendar.get(Calendar.DATE) == 31) {
      return 74;
    }
    int periodoComprobante = (calendar.get(Calendar.DAY_OF_MONTH) - 1) / 5 + 1;
    return calendar.get(Calendar.MONTH) * 6 + (periodoComprobante > 6 ? 6 : periodoComprobante);
  } // getPeriodoComprobante

  /**
   * Devuelve la fecha de un periodo comprbante.
   * 
   * @param periodoComprobante
   * @param year
   * @return
   */
  public static Calendar getFechaPeriodoComprobante(final int periodoComprobante, int year) {
    return new GregorianCalendar(year, periodoComprobante / 6, (periodoComprobante % 6) * 5);
  } // getFechaPeriodoComprobante

  /**
   * Obtiene la fecha a insertar en un apunte contable en función de si el periodo contable está cerrado o no.
   * 
   * @param hoy Hoy.
   * @param fecha Fecha a comprobar.
   * @return <code>java.util.Date - </code>Fecha a insertar en el apunte contable.
   */
  public Date getFechaContabilizacion(Calendar hoy, Date fecha) {
    Calendar cLiq = (Calendar) hoy.clone();
    cLiq.setTime(fecha);
    return cierreBo.isCerrado(cLiq.get(Calendar.YEAR), cLiq.get(Calendar.MONTH) + 1, cLiq.get(Calendar.DAY_OF_MONTH)) ? hoy.getTime()
                                                                                                                     : (Date) fecha.clone();
  } // getFechaContabilizacion

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * 
   * @param tipoApunte
   */
  @Transactional
  private void put(EstadosEnumerados.TIPO_APUNTE tipoApunte) {
    try {
      LOG.info("[put] Iniciando la contabilidad de " + tipoApunte.getDescripcion());
      Tmct0men tmct0men = tmct0menBo.putEstadoMEN(tipoApunte, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);
      try {
        switch (tipoApunte) {
          case FACTURAS_EMITIDAS:
            putFacturasEmitidas();
            break;
          case FACTURAS_COBROS:
            putFacturasCobros();
            break;
          case FACTURAS_CERRADAS:
            putFacturasCierres();
            break;
          case FACTURAS_ANULADAS:
            putFacturasAnuladas();
            break;
          case EMISION_DEVENGOS_ROUTING:
            putDevengosRouting();
            break;
          case EMISION_DEVENGOS_CLEARING:
            putDevengosClearing();
            break;
          case EMISION_DEVENGOS_CORRETAJES:
            putDevengosCorretaje();
            break;
          case EMISION_DEVENGOS_FACTURACION:
            putDevengosFacturacion();
            break;
          case EMISION_RETROCESIONES_DEVENGOS:
            putRetrocesionesDevengos();
            break;
          case COBROS_ROUTING:
            generateCobrosRouting();
            break;
          case COBROS_CLEARING_NETTING_MERCADO:
            // putCobrosClearingMercado();
            generateCobrosClearing();
            break;
          // case COBROS_CLEARING_NETTING_CLIENTE:
          // putCobrosClearingCliente();
          // break;
          case FALLIDOS_AJUSTES:
            generateApuntesFallidosAjustes();
            break;
          case DOTACION:
            putDotacion();
            break;
          case DESDOTACION:
            putDesdotacion();
            break;
          case CANON_BME:
            putCanonBme();
            break;
          case COBROS_COMISIONES_BME:
            putCobrosComisionesBme();
            break;
          default:
            break;
        } // switch

        tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.LISTO_GENERAR);
        LOG.debug("[put] Finalizada la contabilidad de " + tipoApunte.getDescripcion());
      } catch (Exception e) {
        tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.EN_ERROR);
        throw e;
      } // catch
    } catch (Exception e) {
      LOG.error("[put] Error ejecutando la contabilidad de " + tipoApunte.getDescripcion(), e);
    } // catch
  } // put

  /**
   * Realiza la contabilidad de las facturas emitidas.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putFacturasEmitidas() throws Exception {
    LOG.debug("[putFacturasEmitidas] Inicio ...");
    // facturaEmision.put(Calendar.getInstance());

    Boolean updateAlcs = Boolean.FALSE;

    List<Long> facturasList = facturaDao.findIdByEstadocont(CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId());
    LOG.debug("[putFacturasEmitidas] Encontradas {} facturas a contabilizar ...", facturasList.size());

    if (facturasList != null && !facturasList.isEmpty()) {
      Tmct0estado estadoContPdteCobro = estadoDao.findOne(CONTABILIDAD.FACTURADA_PDTE_COBRO.getId());
      Calendar hoy = Calendar.getInstance();

      for (Long facturaId : facturasList) {
        LOG.debug("[putFacturasEmitidas] Contabilizando factura con Id: {}", facturaId);
        do {
          updateAlcs = facturaEmision.updateAlcEstadoCont(facturaId, transaction_size, estadoContPdteCobro);
        } while (updateAlcs);

        facturaEmision.putApuntesContables(facturaId, hoy, estadoContPdteCobro);
      } // for (Factura factura : facturasList)
    } // if (facturasList != null && !facturasList.isEmpty())

    LOG.debug("[putFacturasEmitidas] Fin ...");
  } // putFacturasEmitidas

  /**
   * Realiza la contabilidad de las fc turas cobradas.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putFacturasCobros() throws Exception {
    facturaCobro.put(cobroDao.findByContabilizado(false), Calendar.getInstance());
  } // putFacturasCobros

  /**
   * Realiza la contabilidad de las facturas cerradas.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putFacturasCierres() throws Exception {
    facturaCierre.put(Calendar.getInstance());
  } // putFacturasCierres

  /**
   * Realiza la contabilidad de las facturas anuladas.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putFacturasAnuladas() throws Exception {
    facturaAnulacion.put(Calendar.getInstance());
  } // putFacturasAnuladas

  /**
   * Realiza los apuntes contables de devengo de operaciones de routing.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putDevengosRouting() throws Exception {
    LOG.debug("[putDevengosRouting] Inicio ... ");
    Map<String, Object> params = putDevengos(TIPO_COMPROBANTE.DEVENGO_ROUTING);

    List<Tmct0alc> list;
    while (!(list = alcDaoImp.findAlcParaDevengar(TIPO_COMPROBANTE.DEVENGO_ROUTING, ALC_CDESTLIQ, ESTADOSASIG, ESTADOSCONT_PDTEDEVENGO,
                                                  (String) params.get("keyvalue"), (List<String>) params.get("cdAuxili"),
                                                  (Date) params.get("dCierre"), (Integer) params.get("numrows"))).isEmpty()) {
      LOG.debug("[putDevengosRouting] Encontradas " + list.size() + " operaciones de routing para devengar ...");
      params.put("list", list);
      devengoRouting.put(params);
    } // while

    LOG.debug("[putDevengosRouting] Fin ... ");
  } // putDevengosRouting

  /**
   * Realiza los apuntes contables de devengo de operaciones de clearing.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putDevengosClearing() throws Exception {
    LOG.debug("[putDevengosClearing] Inicio ... ");
    Map<String, Object> params = putDevengos(TIPO_COMPROBANTE.DEVENGO_CLEARING);

    List<Tmct0alc> list;
    while (!(list = alcDaoImp.findAlcParaDevengar(TIPO_COMPROBANTE.DEVENGO_CLEARING, ALC_CDESTLIQ, ESTADOSASIG, ESTADOSCONT_PDTEDEVENGO,
                                                  (String) params.get("keyvalue"), (List<String>) params.get("cdAuxili"),
                                                  (Date) params.get("dCierre"), (Integer) params.get("numrows"))).isEmpty()) {
      LOG.debug("[putDevengosClearing] Encontradas " + list.size() + " operaciones de clearing para devengar ...");
      params.put("list", list);
      devengoClearing.put(params);
    } // while

    LOG.debug("[putDevengosClearing] Inicio ... ");
  } // putDevengosClearing

  /**
   * Realiza los apuntes contables de devengo operaciones de corretajes BME.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putDevengosCorretaje() throws Exception {
    LOG.debug("[putDevengosCorretaje] Inicio ...");
    Map<String, Object> params = putDevengos(TIPO_COMPROBANTE.DEVENGO_CORRETAJE);

    List<Tmct0alc> list;
    while (!(list = alcDaoImp.findAlcParaDevengar(TIPO_COMPROBANTE.DEVENGO_CORRETAJE, ALC_CDESTLIQ, ESTADOSASIG, ESTADOSCONT_PDTEDEVENGO,
                                                  (String) params.get("keyvalue"), (List<String>) params.get("cdAuxili"),
                                                  (Date) params.get("dCierre"), (Integer) params.get("numrows"))).isEmpty()) {
      LOG.debug("[putDevengosCorretaje] Encontradas " + list.size() + " operaciones de corretajes BME para devengar ...");
      params.put("list", list);
      devengoCorretaje.put(params);
    } // while

    LOG.debug("[putDevengosCorretaje] Fin ...");
  } // putDevengosCorretaje

  /**
   * Realiza los apuntes contables de devengo de operaciones de facturación.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putDevengosFacturacion() throws Exception {
    LOG.debug("[putDevengosFacturacion] Inicio ...");
    Map<String, Object> params = putDevengos(TIPO_COMPROBANTE.DEVENGO_FACTURACION);

    List<Tmct0alc> list;
    while (!(list = alcDaoImp.findAlcParaDevengar(TIPO_COMPROBANTE.DEVENGO_FACTURACION, ALC_CDESTLIQ, ESTADOSASIG, ESTADOSCONT_PDTEDEVENGO,
                                                  (String) params.get("keyvalue"), (List<String>) params.get("cdAuxili"),
                                                  (Date) params.get("dCierre"), (Integer) params.get("numrows"))).isEmpty()) {
      LOG.debug("[putDevengosFacturacion] Encontradas " + list.size() + " operaciones de facturacion para devengar ...");
      params.put("list", list);
      devengoFacturacion.put(params);
    } // while

    LOG.debug("[putDevengosFacturacion] Fin ...");
  } // putDevengosFacturacion

  private Map<String, Object> putDevengos(TIPO_COMPROBANTE tipoComprobante) throws Exception {
    Map<String, Object> params = new HashMap<String, Object>();
    Calendar hoy = Calendar.getInstance();

    Tmct0cfg cfg = cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION, TMCT0CFG_PROCESS, TMCT0CFG_MIEMBROCOMPENSADOR);
    List<String> cdAuxili = Arrays.asList(cfg.getCdauxili().trim().split(","));

    Date dCierre = tipoComprobante == TIPO_COMPROBANTE.DEVENGO_CLEARING || tipoComprobante == TIPO_COMPROBANTE.DEVENGO_FACTURACION ? getDCierre(hoy).getTime()
                                                                                                                                  : null;
    int size = alcDaoImp.contAlcParaDevengar(tipoComprobante, ALC_CDESTLIQ, ESTADOSASIG, ESTADOSCONT_PDTEDEVENGO, cfg.getKeyvalue(), cdAuxili,
                                             dCierre);
    int numrows = size / (size / transaction_size + 1) + 1;
    params.put("hoy", hoy);
    params.put("dCierre", dCierre);
    params.put("keyvalue", cfg.getKeyvalue());
    params.put("cdAuxili", Arrays.asList(cfg.getCdauxili().trim().split(",")));
    params.put("numrows", numrows);
    LOG.debug("Hay " + size + " ALCs de " + tipoComprobante.getDescripcion());
    return params;
  }

  /**
   * Genera los apuntes contables de retrocesión de los apuntes contables de devengos para las operaciones devengadas y rechazadas posteriormente.
   * 
   * @throws Exception Si ocurre algún error durante el proceso.
   */
  private void putRetrocesionesDevengos() throws Exception {
    LOG.debug("[putRetrocesionesDevengos] Inicio ...");

    // List<RechazoApunteDto> rechazosList;
    // while (!(rechazosList = alcDaoImp.findRechazos(transaction_size)).isEmpty()) {
    // LOG.debug("[putRetrocesionesDevengos] Encontrados " + rechazosList.size() + " apuntes contables de devengos a retroceder ...");
    // devengo.generateRetrocesionesDevengos(rechazosList);
    // } // while

    List<Object[]> rechazosList;
    List<RechazoApunteDto> rechazosApuntesList;

    int initialPosition = 0;
    int finalPosition = 0;

    List<Tmct0alcId> listaAlcsSinApuntes = new ArrayList<Tmct0alcId>();

    while (!(rechazosList = alcDaoImp.findAlcsDevengadosRechazados(transaction_size)).isEmpty()) {
      LOG.debug("[putRetrocesionesDevengos] Encontrados " + rechazosList.size() + " ALCs devengados rechazados ...");

      for (Object[] alcDevengadoRechazado : rechazosList) {
        LOG.debug("[putRetrocesionesDevengos] Procesando Tmct0alcId [nuorden=" + alcDevengadoRechazado[0] + ", nbooking=" + alcDevengadoRechazado[1]
                  + ", nucnfclt=" + alcDevengadoRechazado[2] + ", nucnfliq=" + alcDevengadoRechazado[3] + "]");

        rechazosApuntesList = alcDaoImp.findRechazos(String.valueOf(alcDevengadoRechazado[0]).trim(),
                                                     String.valueOf(alcDevengadoRechazado[1]).trim(),
                                                     String.valueOf(alcDevengadoRechazado[2]).trim(),
                                                     Short.valueOf(alcDevengadoRechazado[3].toString().trim()));
        LOG.debug("[putRetrocesionesDevengos] Encontrados " + rechazosApuntesList.size() + " apuntes contables de devengos a retroceder ...");

        if (!rechazosApuntesList.isEmpty()) {

          initialPosition = 0;
          finalPosition = transaction_size > rechazosApuntesList.size() ? finalPosition = rechazosApuntesList.size() : transaction_size;

          while (initialPosition < finalPosition) {
            LOG.debug("[putRetrocesionesDevengos] Procesando sublist [{}-{}] de [{}]", initialPosition, finalPosition, rechazosApuntesList.size());
            devengo.generateRetrocesionesDevengos(rechazosApuntesList.subList(initialPosition, finalPosition));

            initialPosition = finalPosition;
            if (rechazosApuntesList.size() < initialPosition + transaction_size) {
              finalPosition = rechazosApuntesList.size();
            } else {
              finalPosition += transaction_size;
            } // else
          } // while (initialPosition < finalPosition)

        } else {
          LOG.debug("[putRetrocesionesDevengos] El Tmct0alcId no tiene apuntes contables a retroceder, estableciendo estado contable rechazado ...");
          listaAlcsSinApuntes.add(new Tmct0alcId(String.valueOf(alcDevengadoRechazado[1]).trim(), String.valueOf(alcDevengadoRechazado[0]).trim(),
                                                 Short.valueOf(alcDevengadoRechazado[3].toString().trim()), String.valueOf(alcDevengadoRechazado[2])
                                                                                                                  .trim()));
        } // else

        rechazosApuntesList.clear();
      } // for (Object[] alcDevengadoRechazado : rechazosList)

      if (!listaAlcsSinApuntes.isEmpty()) {
        devengo.changeCdestadocontWithoutDevengo(listaAlcsSinApuntes);
        listaAlcsSinApuntes.clear();
      }
    } // while (!(rechazosList = alcDaoImp.findAlcsDevengadosRechazados(transaction_size)).isEmpty())

    LOG.debug("[putRetrocesionesDevengos] Fin ...");
  } // putRetrocesionesDevengos

  /**
   * Realiza los apuntes contables de cobro de operaciones de routing.
   * 
   * @throws Exception Si ocurre algún error durante la operación.
   */
  private void generateCobrosRouting() throws Exception {
    LOG.debug("[generateCobrosRouting] Inicio ...");
    cobrosRouting.generateCobrosRouting(ESTADOSASIG);
    LOG.debug("[generateCobrosRouting] Fin ...");
  } // generateCobrosRouting

  /**
   * Genera los apuntes contables de cobros de operaciones de clearing.
   * 
   * @throws Exception Si ocurre algún error durante la operación.
   */
  private void generateCobrosClearing() throws Exception {
    LOG.debug("[generateCobrosClearing] Inicio ...");

    // TODO Lo suyo sería crear el mapa al inicio de la contabilidad con todos los estados a usar y usarlo en todos los subprocesos
    estadosMap = new HashMap<String, Tmct0estado>();
    estadosMap.put("estadoContParcialmenteCobrada", estadoDao.findOne(CONTABILIDAD.PARCIALMENTE_COBRADA.getId()));
    estadosMap.put("estadoContCobrada", estadoDao.findOne(CONTABILIDAD.COBRADA.getId()));
    estadosMap.put("enCurso", estadoDao.findOne(CONTABILIDAD.EN_CURSO.getId()));

    // FIXME A cambiar por variable
    cobrosClearing.generateApuntesClearing(transaction_size, estadosMap);
    LOG.debug("[generateCobrosClearing] Fin ...");
  } // generateCobrosClearing

  private void putCobrosComisionesBme() throws Exception {
    LOG.debug("[putCobrosComisionesBme] Inicio ...");

    // TODO Lo suyo sería crear el mapa al inicio de la contabilidad con todos los estados a usar y usarlo en todos los subprocesos
    estadosMap = new HashMap<String, Tmct0estado>();
    estadosMap.put("estadoContDevengada", estadoDao.findOne(CONTABILIDAD.DEVENGADA_PDTE_COBRO_BME.getId()));
    estadosMap.put("estadoContParcialmenteCobrada", estadoDao.findOne(CONTABILIDAD.PARCIALMENTE_COBRADA.getId()));
    estadosMap.put("estadoContCobrada", estadoDao.findOne(CONTABILIDAD.COBRADA.getId()));
    estadosMap.put("estadoContEnCurso", estadoDao.findOne(CONTABILIDAD.EN_CURSO.getId()));

    cobrosComisionesBME.generateApuntesComisionesBME(transaction_size, estadosMap, ESTADOSASIG);
    LOG.debug("[putCobrosComisionesBme] Fin ...");
  } // putCobrosCorretajeBme

  // /**
  // * Realiza los apuntes contables de cobro de la pata mercado de operaciones de clearing.
  // *
  // * @throws Exception Si ocurre algún error durante la operación.
  // */
  // private void putCobrosClearingMercado() throws Exception {
  // LOG.debug("[putCobrosClearingMercado] Inicio ...");
  // int size = 0;
  // String ids = "";
  // List<Object[]> list;
  // Map<String, Object> params = new HashMap<String, Object>();
  // Map<Long, Long> mFichero = new HashMap<Long, Long>();
  //
  // List<Object[]> lFichero = n43FicheroDao.findByProcesadoAndTipologiaAndTipoImporte(Boolean.FALSE, TIPOLOGIA.Clearing.getTipo(),
  // TIPO_IMPORTE.Efectivo.getTipo());
  // if (lFichero.isEmpty()) {
  // LOG.debug("[putCobrosClearingMercado] No hay ficheros norma43 para procesar ...");
  // return;
  // } // if
  // LOG.debug("[putCobrosClearingMercado] Encontrados {} ficheros norma43 para procesar ...", lFichero.size());
  //
  // for (Object[] objects : lFichero) {
  // ids += objects[0] + ",";
  // mFichero.put((Long) objects[0], (Long) objects[1]);
  // } // for
  // ids = ids.substring(0, ids.length() - 1);
  // params.put("mFichero", mFichero);
  //
  // if ((size = n43MovimientoDao.count(ids)) == 0) {
  // LOG.debug("[putCobrosClearingMercado] No hay movimientos norma43 pata mercado para procesar ...");
  // } else {
  // int num = size / (size / transaction_size + 1) + 1;
  // LOG.debug("[putCobrosClearingMercado] Encontrados {} movimientos norma43 pata mercado para procesar ...", size);
  //
  // for (int fromIndex = 1; fromIndex < size; fromIndex += num) {
  // params.put("list", list = n43MovimientoDao.find(ids, fromIndex, fromIndex + num - 1));
  // LOG.debug("[putCobrosClearingMercado] Se van a procesar {} movimientos norma43 pata mercado ...", list.size());
  // cobrosClearingMercado.put(params);
  // } // for
  // } // else
  // LOG.debug("[putCobrosClearingMercado] Fin ...");
  // } // putCobrosClearingMercado

  // /**
  // * Realiza los apuntes contables de cobro de la pata cliente de operaciones de clearing.
  // *
  // * @throws Exception Si ocurre algún error durante la operación.
  // */
  // private void putCobrosClearingCliente() {
  // LOG.debug("[putCobrosClearingCliente] Inicio ... NO hay nada");
  //
  // estadosMap = new HashMap<String, Tmct0estado>();
  // estadosMap.put("estadoContParcialmenteCobrada", estadoDao.findOne(CONTABILIDAD.PARCIALMENTE_COBRADA.getId()));
  // estadosMap.put("estadoContCobrada", estadoDao.findOne(CONTABILIDAD.COBRADA.getId()));
  // estadosMap.put("enCurso", estadoDao.findOne(CONTABILIDAD.EN_CURSO.getId()));
  //
  // cobrosClearingCliente.generateCobrosNorma43Mayorista(estadosMap);
  // LOG.debug("[putCobrosClearingCliente] Fin ...");
  // } // putCobrosClearingCliente

  /**
   * Realiza los apuntes contables de fallidos y ajustes.
   * 
   * @throws Exception Si ocurre algún error durante la operación.
   */
  private void generateApuntesFallidosAjustes() throws Exception {
    fallidosAjustes.generateApuntesFallidosAjustes();
  } // generateFallidosAjustes

  /**
   * Realiza los apuntes contables de dotación.
   * 
   * @throws Exception Si ocurre algún error durante la operación.
   */
  private void putDotacion() throws Exception {
    LOG.debug("[putDotacion] Inicio ...");
    Calendar hoy = Calendar.getInstance();
    CierreContable cierre;

    // Si no está dotado el mes pasado, hay que dotarlo
    Calendar mesPasado = (Calendar) hoy.clone();
    mesPasado.add(Calendar.MONTH, -1);
    cierre = cierreBo.findByMes(mesPasado);
    if (!cierre.isDotado()) {
      mesPasado.set(Calendar.DATE, mesPasado.getActualMaximum(Calendar.DATE));
      dotacion.put(mesPasado, cierre);
    }

    // Si estamos en el último día del mes y el mes actual no está dotado, hay que dotarlo
    cierre = cierreBo.findByMes(hoy);
    if (hoy.get(Calendar.DATE) == hoy.getActualMaximum(Calendar.DATE) && !cierre.isDotado()) {
      dotacion.put(hoy, cierre);
    }

    LOG.debug("[putDotacion] Fin ...");
  } // putDotacion

  /**
   * Realiza los apuntes contables de desdotación.
   * 
   * @throws Exception Si ocurre algún error durante la operación.
   */
  private void putDesdotacion() throws Exception {
    LOG.debug("[putDesdotacion] Inicio ...");
    Calendar hoy = Calendar.getInstance();

    // Si el mes pasado está dotado y no desdotado, hay que desdotarlo
    Calendar mesPasado = (Calendar) hoy.clone();
    mesPasado.set(Calendar.DAY_OF_MONTH, 1);
    mesPasado.add(Calendar.DAY_OF_MONTH, -1);
    CierreContable cierre = cierreBo.findByMes(mesPasado);
    if (cierre.isDotado() && !cierre.isDesdotado()) {
      hoy.set(Calendar.DATE, 1);
      desdotacion.put(hoy, mesPasado);
    }

    LOG.debug("[putDesdotacion] Inicio ...");
  } // putDesdotacion

  /**
   * Realiza los apuntes de devengo de los cánones de contratación de BME para las operación de los meses cerrados contablemente cuyos cánones de
   * contratación no hayan sido procesados.
   */
  private void putCanonBme() {
    LOG.debug("[putCanonBme] Inicio ...");

    Calendar hoy2 = Calendar.getInstance();
    Calendar mesPasado2 = Calendar.getInstance();
    mesPasado2.add(Calendar.MONTH, -1);
    // mesPasado2 = DateUtils.truncate(mesPasado2, Calendar.MONTH);

    LOG.debug("[putCanonBme] Dia a buscar: " + mesPasado2.get(Calendar.DAY_OF_MONTH));
    LOG.debug("[putCanonBme] Mes a buscar: " + (mesPasado2.get(Calendar.MONTH) + 1));
    LOG.debug("[putCanonBme] Anyo a buscar: " + mesPasado2.get(Calendar.YEAR));

    CierreContable cierre2 = cierreBo.findByMes(mesPasado2);

    if (cierre2 != null) {
      LOG.debug("[putCanonBme] El mes '{}' ha sido cerrado, comprobando si ya se han devengado los canones de BME ...",
                (mesPasado2.get(Calendar.MONTH) + 1));
      if (cierre2.isCanones() != null && cierre2.isCanones()) {
        LOG.debug("[putCanonBme] Los canones de BME ya han sido devengados para el mes '{}'. Se omite el proceso ...",
                  (mesPasado2.get(Calendar.MONTH) + 1));
      } else {
        LOG.debug("[putCanonBme] Los canones de BME aun no han sido devengados para el mes '{}'. Devengando ...",
                  (mesPasado2.get(Calendar.MONTH) + 1));

        Calendar primerDiaMes = Calendar.getInstance();
        primerDiaMes.set(Calendar.DAY_OF_MONTH, mesPasado2.getActualMinimum(Calendar.DAY_OF_MONTH));
        primerDiaMes.set(Calendar.MONTH, mesPasado2.get(Calendar.MONTH));

        Calendar ultimoDiaMes = Calendar.getInstance();
        ultimoDiaMes.set(Calendar.DAY_OF_MONTH, mesPasado2.getActualMaximum(Calendar.DAY_OF_MONTH));
        ultimoDiaMes.set(Calendar.MONTH, mesPasado2.get(Calendar.MONTH));

        // canonBme.put(mesPasado2, hoy2);
        canonBme.put(primerDiaMes, ultimoDiaMes, hoy2);
        cierre2.setCanones(Boolean.TRUE);
      } // else
    } else {
      LOG.debug("[putCanonBme] El mes '{}' aun no ha sido cerrado, no se pueden devengar los canones de BME ...",
                (mesPasado2.get(Calendar.MONTH) + 1));
    } // else

    LOG.debug("[putCanonBme] Fin ...");
  } // putCanonBme

  /**
   * Genera los ficheros a enviar a AS400 y los envía.
   */
  private void putAS400() {
    for (TIPO_COMPROBANTE tipoComprobante : TIPO_COMPROBANTE.values()) {
      if (!putContables(tipoComprobante, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION, TMCT0MSC.EN_ERROR)) {
        break;
      } // if
    } // for (TIPO_COMPROBANTE tipoComprobante : TIPO_COMPROBANTE.values())
  } // putAS400

  private boolean putContables(TIPO_COMPROBANTE tipoComprobante, TMCT0MSC estadoIni, TMCT0MSC estadoExe, TMCT0MSC estadoError) {
    boolean hayFTP = true;
    try {
      LOG.debug("Se lanza la tarea de creacion del fichero para AS400 de " + tipoComprobante.getDescripcion());
      Tmct0men tmct0men = tmct0menBo.putEstadoMEN(tipoComprobante, estadoIni, estadoExe);
      try {
        hayFTP = contable.putContables(tipoComprobante);
        tmct0menBo.putEstadoMENAndIncrement(tmct0men, estadoIni);
        LOG.debug("Generado el fichero para AS400 de " + tipoComprobante.getDescripcion());
      } catch (Exception e) {
        tmct0menBo.putEstadoMENAndIncrement(tmct0men, estadoError);
        throw e;
      }
    } catch (Exception e) {
      LOG.error("Fichero para AS400 de " + tipoComprobante.getDescripcion(), e);
    }
    return hayFTP;
  } // putContables

  public Calendar getDCierre(Calendar hoy) throws ParseException {
    // Si el día no es laboral, se considera que estamos lanzando el último día
    // laborable a última hora
    List<Integer> workDays = cfgBo.getWorkDaysOfWeek();
    List<Date> holidays = cfgBo.getHolidays();
    Calendar hoy2 = (Calendar) hoy.clone();
    if (DateHelper.isWorkDayOfWeek(hoy, workDays) && !DateHelper.isHoliday(hoy, holidays)
        && hoy.get(Calendar.HOUR_OF_DAY) < 20) {

      // Hay que irse al anterior dia laboral
      hoy2 = DateHelper.getPreviousWorkDate(hoy2, 1, workDays, holidays);
    }
    // Nos situamos a las 00:00h de hoy2
    Calendar hoyIni = DateUtils.truncate(hoy2, Calendar.DAY_OF_MONTH);

    // Nos situarnos a la última hora de ayer, y a su último milisegundo
    Calendar ayer = (Calendar) hoyIni.clone();
    ayer.add(Calendar.MILLISECOND, -1);

    // Y obtenemos el último día laborar anterior a hoy, y a su último
    // milisegundo
    return DateHelper.getPreviousWorkDate(ayer, 0, workDays, holidays);
  } // getDCierre

} // ApunteContableBo
