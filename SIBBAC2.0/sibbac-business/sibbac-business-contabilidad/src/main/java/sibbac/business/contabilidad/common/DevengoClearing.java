package sibbac.business.contabilidad.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.contabilidad.database.bo.ApunteContableBo;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDaoImp;
import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.ApunteContable;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.utils.Enumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.SIBBACBusinessException;

/**
 * The Class DevengoClearing.
 */
@Component
public class DevengoClearing {
  protected static final Logger LOG = LoggerFactory.getLogger(DevengoClearing.class);
  private static final DateFormat dF1 = new SimpleDateFormat("yyyyMMdd");

  @Autowired
  private ApunteContableBo apunteBo;
  @Autowired
  private ApunteContableAlcDaoImp apunteAlcDao;
  @Autowired
  private CuentaContableDao cuentaDao;
  @Autowired
  private Tmct0alcDaoImp alcDao;
  @Autowired
  private DevengoComision devengoComision;
  @Autowired
  private Devengo devengo;
  @Autowired
  private AuxiliarDao auxiliarDao;
  private Map<Date, Date> mFechasContabilizacion = new HashMap<Date, Date>();
  private Map<String, Auxiliar> mAuxiliar = new HashMap<String, Auxiliar>();

  private Date getFechaContabilizacion(Calendar hoy, Date fecha) {
    Date fechContabilizacion = mFechasContabilizacion.get(fecha);
    if (fechContabilizacion == null) {
      mFechasContabilizacion.put(fecha, fechContabilizacion = apunteBo.getFechaContabilizacion(hoy, fecha));
    }
    return fechContabilizacion;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void put(Map<String, Object> params) throws Exception {
    Date feejeliq;
    List<Tmct0alc> lBean;
    String cdalias = "";
    Calendar hoy = (Calendar) params.get("hoy");
    Map<Date, Map<String, List<Tmct0alc>>> mCliente = new TreeMap<Date, Map<String, List<Tmct0alc>>>();
    Map<Date, List<Tmct0alc>> mBME = new TreeMap<Date, List<Tmct0alc>>();
    Map<String, List<Tmct0alc>> mDia;
    params.put("contabilizado", false);
    params.put("operativa", "N");
    for (Tmct0alc alc : (List<Tmct0alc>) params.get("list")) {
      LOG.debug("Procesando el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());

      /*
       * En el caso que se haya cerrado el mes, los apuntes contables deben entrar con fecha del mes en curso, pero en la descripción del apunte
       * contable conservar la fecha Trade Date
       */
      feejeliq = alc.getFeejeliq();
      params.put("nuorden", alc.getNuorden());
      params.put("nbooking", alc.getNbooking());
      params.put("nucnfclt", alc.getNucnfclt());
      params.put("cdalias", cdalias = alc.getTmct0alo().getCdalias());
      params.put("alc", alc);
      if (!mAuxiliar.containsKey(cdalias)) {
        mAuxiliar.put(cdalias, alc.getAuxiliar() == null ? null : auxiliarDao.findOne(alc.getAuxiliar()));
      }
      params.put("fechFactura", getFechaContabilizacion(hoy, feejeliq));
      params.put("feejeliq", feejeliq);
      params.put("sFeejeliq", dF1.format(feejeliq));
      save(params);
      LOG.debug("Procesado el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
    }
  }

  public void save(Map<String, Object> params) throws SIBBACBusinessException {
    Map<String, Object> params2 = new HashMap<String, Object>();
    params2.put("sFeejeliq", params.get("sFeejeliq"));
    Tmct0alc alc = (Tmct0alc) params.get("alc");
    params2.put("alc", alc);
    params2.put("contabilizado", false);
    params2.put("operativa", "N");
    params2.put("fechFactura", params.get("fechFactura"));
    LOG.debug("Procesando el ALC clearing {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
//    alcDao.updateCdestadocont(CONTABILIDAD.CONTABILIZANDOSE.getId(), alc.getNbooking(), alc.getNuorden(), alc.getNucnfliq(), alc.getNucnfclt());
    params.put("auxiliar", mAuxiliar.get(alc.getTmct0alo().getCdalias()));
    LOG.debug("Se procesando el alc clearing {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
    params.put("comision", devengo.getComision(alc));
    if ('C'==alc.getTmct0alo().getCdtpoper()) {
      params.put("concepto", "Compras pendientes de liquidar");
    } else {
      params.put("concepto", "Ventas pendientes de liquidar");
    }
    params.put("TIPO_COMPROBANTE", TIPO_COMPROBANTE.DEVENGO_CLEARING);
    params.put("TIPO_APUNTE", TIPO_APUNTE.CORRETAJE.getId());
    params.put("importe", params.get("comision"));
    params.put("descripcion", "Comision " + ((Tmct0alc) params.get("alc")).getFeejeliq() + "/" + params.get("cdalias") + "/" + alc.getNbooking()
                              + "/" + ((Tmct0alc) params.get("alc")).getNbtitliq());
    putClearingDet(params);
    devengoComision.save(params2);
    alcDao.escalaAlcDesglose(alc, CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(), TIPO_ESTADO.CORRETAJE_O_CANON);
    LOG.debug("Se procesado el alc clearing {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
  }

  private void putApunte(ApunteContable apunte) {
    if (apunte.getImporte().signum() != 0) {
      apunteBo.save(apunte);
      for (ApunteContableAlc alc : apunte.getAlcs()) {
        apunteAlcDao.insert(alc.getAlc().getNbooking(), alc.getAlc().getNuorden(), alc.getAlc().getNucnfliq(), alc.getAlc().getNucnfclt(),
                            apunte.getId(), alc.getImporte());
      }
    }
  }

  /**
   * Put clearing det.
   *
   * @param params the params
   */
  private void putClearingDet(Map<String, Object> params) {
    ApunteContable apunte;
    Tmct0alc alc = (Tmct0alc) params.get("alc");
    BigDecimal importe = (BigDecimal) params.get("importe");
    if (importe == null || importe.signum() == 0) {
      return;
    }
    params.put("cuenta", cuentaDao.findByCuenta(1122108));
    params.put("sApunte", "D");
    (apunte = new ApunteContable(params)).add(alc, importe);
    putApunte(apunte);
    params.put("cuenta", cuentaDao.findByCuenta(4021101));
    params.put("sApunte", "H");
    (apunte = new ApunteContable(params)).add(alc, importe.negate());
    putApunte(apunte);
  }
}
