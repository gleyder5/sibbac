package sibbac.business.contabilidad.utils;

public interface Enumerados {

  public enum TIPO_APUNTE {
    CORRETAJE(1),
    CANON(2),
    CORRETAJE_Y_CANON(3),
    FACTURA(4),
    COBRO(5),
    EFECTIVO_BRUTO(6),
    EFECTIVO_NETO(7),
    COMISION_ORDENANTE(8),
    COMISION_DEVOLUCION(9),
    EFECTIVO_BRUTO_NETEADO(10),
    EFECTIVO_NETO_NETEADO(11),
    INTERES_CLEARING(12),
    CANON_BME_CLIENTE(13),
    CANON_BME_CARTERAS(14),
    COBRO_NORMA43(15),
    CORRETAJE_RETROCEDIDO(16),
    CANON_RETROCEDIDO(17),
    COLATERAL(18),
    AJUSTE_COLATERAL(19),
    INTERES_COLATERAL(20),
    COMISION_PRESTAMO(21),
    INTERES_FINANCIACION(22),
    EFECTIVO_RECOMPRA(23),
    EFECTIVO_VENTA_FALLIDA(24),
    EFECTIVO_COMPRA_AFECTADA(25),
    AJUSTE_EVENTO(26),
    APUNTE_MANUAL_DEVOLUCION(27),
    APUNTE_MANUAL_ABONO(28),
    APUNTE_MANUAL_LIQUIDACION(29),
    DIFERENCIA_CORRETAJE(30),
    DIFERENCIA_CANON(31),
    DIFERENCIA_EFECTIVO(32),
    DIFERENCIA_COMISIONES_PRESTAMO(33);

    private int id;

    private TIPO_APUNTE(int id) {
      this.id = id;
    }

    public int getId() {
      return id;
    }
  }

  public enum BOLSA {
    // FIXME XI316153 Mal pensado, como alguna vez cambien estos valores a recompilar sería mejor sacarlos a la tabla tmct0cfg
    Madrid("BMEMADRID", "MAD", 9838),
    Barcelona("BMEBARCELONA", "BCN", 9472),
    Bilbao("BMEBILBAO", "BIL", 9577),
    Valencia("BMEVALENCIA", "VAL", 9370);

    private String cdbroker;
    private String nombreAuxiliar;
    private int cdmiembromkt;

    private BOLSA(String cdbroker, String nombreAuxiliar, int cdmiembromkt) {
      this.cdbroker = cdbroker;
      this.nombreAuxiliar = nombreAuxiliar;
      this.cdmiembromkt = cdmiembromkt;
    }

    public static BOLSA find(String cdbroker) {
      for (BOLSA bolsa : values()) {
        if (bolsa.cdbroker.equals(cdbroker)) {
          return bolsa;
        }
      }
      return null;
    }

    public static BOLSA find(int cdmiembromkt) {
      for (BOLSA bolsa : values()) {
        if (bolsa.cdmiembromkt == cdmiembromkt) {
          return bolsa;
        }
      }
      return null;
    }

    public String getNombreAuxiliar() {
      return nombreAuxiliar;
    }

    public int getCdmiembromkt() {
      return cdmiembromkt;
    }
  }

  /**
   * Tipo de cuentas contables.
   * 
   * @author XI316153
   */
  public enum TIPO_CUENTA {
    DEBE("D"),
    HABER("H");

    private String id;

    private TIPO_CUENTA(String id) {
      this.id = id;
    } // TIPO_CUENTA

    public String getId() {
      return id;
    } // getId
    
  } // TIPO_CUENTA

} // Enumerados
