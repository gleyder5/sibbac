package sibbac.business.contabilidad.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.contabilidad.database.dao.AuxiliarDao;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.database.bo.AbstractBo;


@Service
public class AuxiliarBo extends AbstractBo< Auxiliar, Long, AuxiliarDao > {

	public Auxiliar save( Auxiliar auxiliar ) {
		return dao.save( auxiliar );
	}

	public Auxiliar findByNumero( String numero ) {
		return dao.findByNumero( numero );
	}

	public Auxiliar findByNuorden( String nuorden ) {
		return dao.findByNuorden( nuorden );
	}
}
