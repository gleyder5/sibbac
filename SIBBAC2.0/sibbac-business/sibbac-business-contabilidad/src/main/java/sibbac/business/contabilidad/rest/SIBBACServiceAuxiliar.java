package sibbac.business.contabilidad.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.contabilidad.database.bo.AuxiliarBo;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.Auxiliar;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.contabilidad.utils.Constantes;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceAuxiliar implements SIBBACServiceBean {

	private static final Logger			LOG			= LoggerFactory.getLogger( SIBBACServiceAuxiliar.class );
	private static final List< String >	commands	= new ArrayList< String >();
	private static String				sCommands	= "";

	@Autowired
	protected AuxiliarBo				auxiliarBo;

	@Autowired
	protected CuentaContableDao			cuentaBo;

	static {
		commands.add( Constantes.CMD_LIST );
		commands.add( Constantes.CMD_UPDATE );
		for ( String aC : commands ) {
			sCommands += aC + ", ";
		}
		sCommands = sCommands.substring( 0, sCommands.length() - 2 );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
		LOG.trace( "Command: " + command );
		try {
			switch ( command ) {
				case Constantes.CMD_LIST:
					webResponse.setResultados( getList() );
					break;
				case Constantes.CMD_ADD:
					webResponse.setResultados( add( webRequest.getParams() ) );
					break;
				case Constantes.CMD_UPDATE:
					webResponse.setResultados( update( webRequest.getParams() ) );
					break;
				case Constantes.CMD_DELETE:
					webResponse.setResultados( delete( webRequest.getParams() ) );
					break;
				default:
					webResponse.setError( "An action must be set. Valid actions are: " + sCommands );
			}
		} catch ( Exception e ) {
			webResponse.setError( e.getMessage() );
		}
		return webResponse;
	}

	private Map< String, Object > getList() {
		LOG.trace( "Recuperamos todas los auxiliares" );
		Map< String, Object > map = new HashMap< String, Object >();
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > bean;
		map.put( "list", list );
		for ( Auxiliar reg : auxiliarBo.findAll() ) {
			list.add( bean = new HashMap< String, Object >() );
			bean.put( "id", reg.getId() );
			bean.put( "numero", reg.getNumero() );
			bean.put( "nombre", reg.getNombre() );
			bean.put( "cuenta", reg.getCuentaContable().getId() );
		}
		map.put( "select cuenta", list = new ArrayList< Map< String, Object >>() );
		for ( CuentaContable reg : cuentaBo.findAll() ) {
			list.add( bean = new HashMap< String, Object >() );
			bean.put( "key", reg.getId() );
			bean.put( "value", reg.getCuenta() );
		}
		return map;
	}

	private Map< String, Object > add( List< Map< String, String >> list ) throws Exception {
		LOG.trace( "Insertando un auxiliar" );
		Map< String, String > mParam = list.get( 0 );
		CuentaContable cuenta = cuentaBo.findByCuenta(Integer.valueOf( mParam.get( "cuenta" ) ) );
		if ( cuenta == null ) {
			throw new Exception( "No existe la cuenta" );
		}
		Auxiliar bean = new Auxiliar( mParam.get( "numero" ), mParam.get( "nombre" ), cuenta );
		bean = auxiliarBo.save( bean );
		Map< String, Object > value = new HashMap< String, Object >();
		value.put( "id", bean.getId() );
		value.put( "numero", bean.getNumero() );
		value.put( "nombre", bean.getNombre() );
		value.put( "cuenta", bean.getCuentaContable().getId() );
		Map< String, Object > mSalida = new HashMap< String, Object >();
		mSalida.put( "resultado", value );
		return mSalida;
	}

	private Map< String, Object > update( List< Map< String, String >> list ) throws Exception {
		LOG.trace( "Modificando auxiliar" );
		Map< String, String > mParam = list.get( 0 );
		Auxiliar bean = auxiliarBo.findById( Long.parseLong( mParam.get( "id" ) ) );
		if ( bean == null ) {
			throw new Exception( "No existe el auxiliar" );
		}
		CuentaContable cuenta = cuentaBo.findByCuenta(Integer.valueOf( mParam.get( "cuenta" ) ) );
		if ( cuenta == null ) {
			throw new Exception( "No existe la cuenta" );
		}
		bean.setNumero( mParam.get( "numero" ) );
		bean.setNombre( mParam.get( "nombre" ) );
		bean.setCuentaContable( cuenta );
		auxiliarBo.save( bean );
		Map< String, Object > value = new HashMap< String, Object >();
		value.put( "id", bean.getId() );
		value.put( "numero", bean.getNumero() );
		value.put( "nombre", bean.getNombre() );
		value.put( "cuenta", bean.getCuentaContable().getId() );
		Map< String, Object > mSalida = new HashMap< String, Object >();
		mSalida.put( "resultado", value );
		return mSalida;
	}

	private Map< String, Object > delete( List< Map< String, String >> list ) throws Exception {
		LOG.trace( "Eliminando cuenta contable" );
		Map< String, String > mParam = list.get( 0 );
		Auxiliar bean = auxiliarBo.findById( Long.parseLong( mParam.get( "id" ) ) );
		if ( bean == null ) {
			throw new Exception( "No existe el auxiliar" );
		}
		auxiliarBo.delete( bean );
		Map< String, Object > value = new HashMap< String, Object >();
		value.put( "id", bean.getId() );
		Map< String, Object > mSalida = new HashMap< String, Object >();
		mSalida.put( "resultado", value );
		return mSalida;
	}

	@Override
	public List< String > getAvailableCommands() {
		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}

}
