package sibbac.business.contabilidad.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.wrappers.database.model.Tmct0alc;

@Component
public interface ApunteContableAlcDao extends JpaRepository<ApunteContableAlc, Long> {

  /**
   * Busca todos los registros del ALC especificado.
   * 
   * @param tmct0alc
   * @return
   */
  public List<ApunteContableAlc> findAllByAlc(Tmct0alc tmct0alc);

} // ApunteContableAlcDao
