package sibbac.business.contabilidad.database.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.CuentaContable}.
 */
@Table( name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.CUENTA_CONTABLE )
@Entity
@Component
@XmlRootElement
@Audited
public class CuentaContable extends ATableAudited< CuentaContable > {

	private static final long		serialVersionUID	= 7228553055114154535L;

	/**
	 * Tipo de Operativa (N = Nacional, I = Internacional)
	 */
	@Column( name = "OPERATIVA", length = 1 )
	private String					operativa;

	/**
	 * Tipo de cuenta (A = Activo, P = Pasivo, X = PyG)
	 */
	@Column( name = "TIPO", length = 1 )
	private String					tipo;

	/**
	 * Numeración de cuenta
	 */
	@Column( name = "CUENTA", unique = true )
	private int						cuenta;

	/**
	 * Nombre de cuenta
	 */
	@Column( name = "NOMBRE" )
	private String					nombre;

	/**
	 * Tiene que tener un Auxiliar Contable
	 */
	@Column( name = "WITH_AUXILIAR" )
	private boolean	withAuxiliar;

	/**
	 * Auxiliar Contable, si la cuenta está asociada a uno unico
	 */
	@ManyToOne( optional = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY )
	@JoinColumn( name = "AUXILIAR", nullable = true )
	private Auxiliar auxiliar;

	/**
	 * Cuenta Bancaria
	 */
	@JoinColumn( name = "CTA_LIQ", referencedColumnName = "ID", nullable = false )
	@ManyToOne( optional = false, fetch = FetchType.LAZY )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	private Tmct0CuentaLiquidacion	ctaLiq;

	public CuentaContable() {
	}

	public CuentaContable( String operativa, String tipo, int cuenta,
			String nombre, boolean withAuxiliar, Tmct0CuentaLiquidacion ctaLiq ) {
		this.operativa = operativa;
		this.tipo = tipo;
		this.cuenta = cuenta;
		this.nombre = nombre;
		this.withAuxiliar = withAuxiliar;
		this.ctaLiq = ctaLiq;
	}

	public String getOperativa() {
		return operativa;
	}

	public String getTipo() {
		return tipo;
	}

	public int getCuenta() {
		return cuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setOperativa( String operativa ) {
		this.operativa = operativa;
	}

	public void setTipo( String tipo ) {
		this.tipo = tipo;
	}

	public void setCuenta( int cuenta ) {
		this.cuenta = cuenta;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public Tmct0CuentaLiquidacion getCtaLiq() {
		return ctaLiq;
	}

	public void setCtaLiq( Tmct0CuentaLiquidacion ctaLiq ) {
		this.ctaLiq = ctaLiq;
	}

	
	public boolean isWithAuxiliar() {
		return withAuxiliar;
	}

	
	public void setWithAuxiliar( boolean withAuxiliar ) {
		this.withAuxiliar = withAuxiliar;
	}

	
	public Auxiliar getAuxiliar() {
		return auxiliar;
	}

	
	public void setAuxiliar( Auxiliar auxiliar ) {
		this.auxiliar = auxiliar;
	}
}
