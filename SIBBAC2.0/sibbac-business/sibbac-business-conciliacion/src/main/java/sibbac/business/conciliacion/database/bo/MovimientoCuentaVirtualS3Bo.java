package sibbac.business.conciliacion.database.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.MovimientoCuentaVirtualS3DAOImpl;
import sibbac.business.conciliacion.database.dao.MovimientoCuentaVirtualS3Dao;
import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualS3Data;
import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtualS3;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.database.bo.AbstractBo;

@Service
public class MovimientoCuentaVirtualS3Bo extends 
		AbstractBo<MovimientoCuentaVirtualS3, Long, MovimientoCuentaVirtualS3Dao > {

	private static final Logger LOG = getLogger(MovimientoCuentaVirtualS3Bo.class);
	
	private static final String	FICHERO_S3	= "conciliacion.fichero.s3";

	@Autowired
	private MovimientoCuentaVirtualS3DAOImpl	daoImpl;

	@Value( "${" + FICHERO_S3 + "}" )
	private String path;
	
	@Value("${conciliacion.s3.destination.folder}")
	private String destinationFolder;
	
	@Value("${conciliacion.s3.fichero.nombre}")
	private String nombreFicheroS3;
	
	@Value("${conciliacion.s3.fichero.extension}")
	private String extensionFicheroS3;

	@Autowired
	private Tmct0CuentaLiquidacionBo	boCuentaLiquidacion;

	public List< MovimientoCuentaVirtualS3 > getMovimientosCuentaVirtual() {
		return dao.getMovimientosCuentaVirtual();
	}

	public List< MovimientoCuentaVirtualS3Data > findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion( long idCuentaLiq,
			Map< String, Serializable > filtros ) {
		List< MovimientoCuentaVirtualS3Data > result = daoImpl.findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion( idCuentaLiq,
				filtros );
		return result;
	}

	/**
	 * @param fichero
	 * @throws NumberFormatException
	 */
	@Transactional
	public void procesaArchivo(File fichero) {
		String linea;
		try {
			try (FileReader fr = new FileReader(fichero); BufferedReader br = new BufferedReader(fr)) {
				while ((linea = br.readLine()) != null) {
					LOG.trace("[procesaArchivo] Linea [{}]...", linea);
					// Leemos el fichero...
					// es necesario saber el valos de los distintos tipo del fichero y
					// crear sus varibles estaticas
					String tipo = linea.substring(0, 2);
					switch (tipo) {
					case "02":
						String strCuentaS3 = linea.substring(12, 22);
						String fechaStr = linea.substring(22, 32);
						String isin = linea.substring(32, 44);
						String titulosStr = linea.substring(200, 225);
						String precioStr = linea.substring(225, 240);
						String efectivoStr = linea.substring(262, 287);
						String descrIsin = linea.substring(44, 134);
						String mercado = linea.substring(134, 137);
						String codDepositario = linea.substring(137, 157);
						String subCuenta = linea.substring(157, 192);
						String tipoPos = linea.substring(192, 196);
						String unidad = linea.substring(197, 200);
						String tipoPrecio = linea.substring(255, 259);
						String divisaPrecio = linea.substring(259, 262);
						String tasaCambioStr = linea.substring(287, 302);
						String divisaBase = linea.substring(302, 305);
						String valorBaseStr = linea.substring(305, 330);

						LOG.trace("[procesaArchivo] Guardada la informacion en variables ....  ");

						// La fecha
						int dia = Integer.valueOf(fechaStr.substring(0, 2));
						int mes = Integer.valueOf(fechaStr.substring(3, 5));
						int año = Integer.valueOf(fechaStr.substring(6, 10));
						Calendar cal = GregorianCalendar.getInstance();
						cal.set(año, mes - 1, dia);
						Date fecha = new Date(cal.getTimeInMillis());
						// Numero de titulos
						String parteentera = titulosStr.substring(0, 17);
						String decimales = titulosStr.substring(17, 25);
						titulosStr = parteentera + "." + decimales;
						BigDecimal nuTitulos = new BigDecimal(titulosStr);
						// Precio
						parteentera = precioStr.substring(0, 7);
						decimales = precioStr.substring(7, 15);
						precioStr = parteentera + "." + decimales;
						BigDecimal precio = new BigDecimal(precioStr);
						// Efectivo
						parteentera = efectivoStr.substring(0, 21);
						decimales = efectivoStr.substring(21, 25);
						precioStr = parteentera + "." + decimales;
						BigDecimal efectivo = new BigDecimal(precioStr);
						// tasa cambio
						String cambio = tasaCambioStr.substring(0, 7).concat(".").concat(tasaCambioStr.substring(7, 15));
						BigDecimal tasaCambio = new BigDecimal(cambio);
						// valos base
						String base = valorBaseStr.substring(0, 21).concat(".").concat(valorBaseStr.substring(21, 25));
						BigDecimal valorBase = new BigDecimal(base);

						LOG.trace("[procesaArchivo] Localizando la cuenta de liquidacion correspondiente a la cuenta S3: [{}/{}]",
						    strCuentaS3, subCuenta);
						Tmct0CuentaLiquidacion cuentaDeLiquidacion = boCuentaLiquidacion.findByCuentaIberclear(subCuenta);
						if (cuentaDeLiquidacion == null) {
							LOG.debug(
							    "[procesaArchivo] No se ha localizado la cuenta de liquidacion correspondiente a la cuenta S3: [{}/{}]",
							    strCuentaS3, subCuenta);
							String cuentaCustodio = linea.substring(2, 22);
							cuentaDeLiquidacion = boCuentaLiquidacion
							    .findByNumeroCuentaSubcustodioCuentaDeCompensacion(cuentaCustodio);
							if (cuentaDeLiquidacion == null) {
								LOG.warn("[procesaArchivo] {} {}",
								    "No se ha localizado la cuenta de liquidacion correspondiente a cuentaCustodio", cuentaCustodio);
							}
						}
						LOG.trace("[procesaArchivo] Realizando el mapeo de las variables");
						// Almacenamos
						MovimientoCuentaVirtualS3 movimientoCVS3 = new MovimientoCuentaVirtualS3();
						movimientoCVS3.setCuentaS3(strCuentaS3);
						movimientoCVS3.setIdcuentaliq(cuentaDeLiquidacion != null ? cuentaDeLiquidacion.getId() : -1L);
						movimientoCVS3.setCdcodigocliq(cuentaDeLiquidacion != null ? cuentaDeLiquidacion.getCdCodigo() : null);
						movimientoCVS3.setIsin(isin);
						movimientoCVS3.setSettlementdate(fecha);
						movimientoCVS3.setTitulos(nuTitulos);
						movimientoCVS3.setPrecio(precio);
						movimientoCVS3.setEfectivo(efectivo);
						movimientoCVS3.setDescrIsin(descrIsin);
						movimientoCVS3.setMercado(mercado);
						movimientoCVS3.setCodDepositario(codDepositario);
						movimientoCVS3.setSubDepositario(subCuenta);
						movimientoCVS3.setTipoPosicion(tipoPos);
						movimientoCVS3.setUnidad(unidad);
						movimientoCVS3.setTipoPrecio(tipoPrecio);
						movimientoCVS3.setDivisaPrecio(divisaPrecio);
						movimientoCVS3.setTasaCambio(tasaCambio);
						movimientoCVS3.setDivisaBase(divisaBase);
						movimientoCVS3.setValorBase(valorBase);
						try {
							LOG.trace("[procesaArchivo] Salvando ....  ");
							this.save(movimientoCVS3);
						} 
						catch (RuntimeException e) {
							LOG.error("[procesaArchivo] Error no controlado al intentar guardar un nuevo moviento S3", e);
						}
						break;
					}
				}
			}
			LOG.trace("[procesaArchivo]  preparando renombrado de fichero...");
			ConciliacionUtil.moveFileToProcessed(destinationFolder, path, nombreFicheroS3, extensionFicheroS3);
			LOG.debug("[procesaArchivo] Archivo [{}] procesado.", path);
		} 
		catch (IOException e) {
			LOG.error("ERROR de entrada salida", e);
		}
	}
	
	public String getPath(){
		return path;
	}

}
