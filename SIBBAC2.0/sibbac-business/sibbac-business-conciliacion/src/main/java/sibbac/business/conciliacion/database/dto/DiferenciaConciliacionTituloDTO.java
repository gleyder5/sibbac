package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class DiferenciaConciliacionTituloDTO {

	private long		id;
	private String		tradeDate;
	private String		fechaLiquidacion;
	private long		idCuentaCompensacion;
	private String		isin;
	private BigDecimal	TSV;					// SALDO CUENTA PROPIA ANOTACIONECC
	private BigDecimal	TS3;					// SALDO CUENTA TITULO
	private BigDecimal	diferencia;
	private String		sentido;

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate( String tradeDate ) {
		this.tradeDate = tradeDate;
	}

	public String getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	public void setFechaLiquidacion( String fechaLiquidacion ) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getTSV() {
		return TSV;
	}

	public void setTSV( BigDecimal eSV ) {
		TSV = eSV;
	}

	public BigDecimal getTS3() {
		return TS3;
	}

	public void setTS3( BigDecimal eS3 ) {
		TS3 = eS3;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido( String sentido ) {
		this.sentido = sentido;
	}

	public long getIdCuentaCompensacion() {
		return idCuentaCompensacion;
	}

	public void setIdCuentaCompensacion( long idCuentaCompensacion ) {
		this.idCuentaCompensacion = idCuentaCompensacion;
	}

	public BigDecimal getDiferencia() {
		return diferencia;
	}

	public void setDiferencia( BigDecimal diferencia ) {
		this.diferencia = diferencia;
	}

}
