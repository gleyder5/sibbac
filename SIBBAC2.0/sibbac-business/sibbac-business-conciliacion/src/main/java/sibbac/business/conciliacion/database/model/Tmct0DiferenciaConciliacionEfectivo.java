package sibbac.business.conciliacion.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_DIFERENCIA_CONCILIACION_EFECTIVO/*																 */)
@XmlRootElement
public class Tmct0DiferenciaConciliacionEfectivo extends ATable< Tmct0DiferenciaConciliacionEfectivo > implements Serializable {

	/**
	 * 
	 */
	private static final long				serialVersionUID	= 1L;
	@Column( name = "TRADE_DATE" )
	private Date							tradeDate;
	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_CUENTA_COMPENSACION", referencedColumnName = "ID_CUENTA_COMPENSACION" )
	protected Tmct0CuentasDeCompensacion	tmct0CuentasDeCompensacion;
	@Column( name = "ISIN" )
	private String							isin;
	@Column( name = "ESV" )
	private BigDecimal						ESV;						// SALDO CUENTA PROPIA ANOTACIONECC
	@Column( name = "ES3" )
	private BigDecimal						ES3;						// SALDO CUENTA TITULO
	@Column( name = "SENTIDO", length = 1, nullable = false )
	private String							sentido;
	@Column( name = "FECHA_LIQUIDACION" )
	private Date							fechaLiquidacion;

	public Date getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate( Date tradeDate ) {
		this.tradeDate = tradeDate;
	}

	public Tmct0CuentasDeCompensacion getTmct0CuentasDeCompensacion() {
		return tmct0CuentasDeCompensacion;
	}

	public void setTmct0CuentasDeCompensacion( Tmct0CuentasDeCompensacion cuenta ) {
		this.tmct0CuentasDeCompensacion = cuenta;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getESV() {
		return ESV;
	}

	public void setESV( BigDecimal eSV ) {
		ESV = eSV;
	}

	public BigDecimal getES3() {
		return ES3;
	}

	public void setES3( BigDecimal eS3 ) {
		ES3 = eS3;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido( String sentido ) {
		this.sentido = sentido;
	}

	public Date getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	public void setFechaLiquidacion( Date fechaLiquidacion ) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

}
