package sibbac.business.conciliacion.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.Tmct0SaldoEfectivoCuentaDAO;
import sibbac.business.conciliacion.database.model.Tmct0SaldoEfectivoCuenta;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0SaldoEfectivoCuentaBO extends AbstractBo< Tmct0SaldoEfectivoCuenta, Long, Tmct0SaldoEfectivoCuentaDAO > {

	public List< Object[] > findAllGroupByIsinAndTradedateAndSentidoAndCuentaCompensacion() {
		return dao.findAllGroupByIsinAndTradedateAndSentidoAndCuentaCompensacion();
	}

	/**
	 * 
	 * @return
	 */
	public List< Object[] > findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion() {
		return dao.findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion();
	}
}
