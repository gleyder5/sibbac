package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public interface IAnotacionECCDTO {

	String getCdisin();

	BigDecimal getImefectivo();

	BigDecimal getNutitulos();

	String getCdCuenta();

	String getIsinDescripcion();

}
