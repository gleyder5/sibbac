package sibbac.business.conciliacion.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.conciliacion.database.model.Tmct0SaldoEfectivoCuenta;


public interface Tmct0SaldoEfectivoCuentaDAO extends JpaRepository< Tmct0SaldoEfectivoCuenta, Long > {

	/**
	 * 
	 * @return
	 */
	@Query( "SELECT o, sum(o.saldo) as total FROM Tmct0SaldoEfectivoCuenta o group by o.isin, o.sentido, o.tradedate, o.cuentaCompensacion" )
			List< Object[] > findAllGroupByIsinAndTradedateAndSentidoAndCuentaCompensacion();

	/**
	 * 
	 * @return
	 */
	@Query( "SELECT o, sum(o.saldo) as total FROM Tmct0SaldoEfectivoCuenta o group by o.isin, o.sentido, o.tradedate, o.settlementdate, o.cuentaCompensacion" )
			List< Object[] > findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion();
}
