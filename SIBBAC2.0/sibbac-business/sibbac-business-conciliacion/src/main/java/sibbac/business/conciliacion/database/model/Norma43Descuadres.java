package sibbac.business.conciliacion.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * @author XI316153
 */
@Entity
@Table(name = DBConstants.CONCILIACION.TMCT0_NORMA43_DESCUADRES)
public class Norma43Descuadres extends ATable<Norma43Descuadres> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -7882261915037356844L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Column(name = "CUENTA", nullable = false, length = 26)
  private String cuenta;

  @Column(name = "REF_FECHA", nullable = false, length = 12)
  private String refFecha;

  @Column(name = "REF_ORDEN", nullable = false, length = 16)
  private String refOrden;

  /**
   * Sentido de la operación. 1.- Deudor 2.- Acreedor
   */
  @Column(name = "SENTIDO", nullable = false, length = 1)
  private String sentido;

  @Column(name = "TRADE_DATE", nullable = false)
  private Date tradeDate;

  @Column(name = "SETTLEMENT_DATE", nullable = false)
  private Date settlementDate;

  @Column(name = "IMPORTE_NORMA43", nullable = false, precision = 15, scale = 2)
  private BigDecimal importeNorma43;

  @Column(name = "IMPORTE_SIBBAC", nullable = true, precision = 15, scale = 2)
  private BigDecimal importeSibbac;

  @Column(name = "TIPO_IMPORTE", nullable = true)
  private String tipoImporte;

  /** Estado de asignación del ALC. */
  @Column(name = "ESTADO_ASIG_ALC", nullable = true)
  private String nombreEstadoAsignacionAlc;

  /** Estado contable del ALC. */
  @Column(name = "ESTADO_CONT_ALC", nullable = true)
  private String nombreEstadoContabilidadAlc;

  /** Estado de entrega/recepción del ALC. */
  @Column(name = "ESTADO_ENTREC_ALC", nullable = true)
  private String nombreEstadoEntrecAlc;

  /** Identificador de la orden a la que pertence el ALC. */
  @Column(name = "NUORDEN", nullable = true, length = 20)
  private String nuorden;

  /** Identificador del booking al que pertenece el ALC. */
  @Column(name = "NBOOKING", nullable = true, length = 20)
  private String nbooking;

  /** Identificador del ALO al que pertenece el ALC. */
  @Column(name = "NUCNFCLT", nullable = true, length = 20)
  private String nucnfclt;

  /** Identificador del ALC. */
  @Column(name = "NUCNFLIQ", nullable = true, precision = 4, scale = 0)
  private Short nucnfliq;

  /** Diferencia entre lo devengado y lo cobrado. */
  @Column(name = "DIFERENCIA", nullable = true, precision = 18, scale = 4)
  private BigDecimal diferencia;

  /** Comentario explicativo sobre porque se ha generado el descuadre. */
  @Column(name = "NBCOMENTARIO", nullable = true, length = 200)
  private String comentario;

  /** Indica si el registro ha sido procesado. */
  @Column(name = "PROCESADO", nullable = true)
  private Boolean procesado;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public Norma43Descuadres() {
  } // Norma43Descuadres

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Norma43Descuadres [cuenta=" + cuenta + ", refFecha=" + refFecha + ", refOrden=" + refOrden + ", sentido=" + sentido + ", tradeDate="
           + tradeDate + ", settlementDate=" + settlementDate + ", importeNorma43=" + importeNorma43 + ", importeSibbac=" + importeSibbac
           + ", tipoImporte=" + tipoImporte + ", nombreEstadoAsignacionAlc=" + nombreEstadoAsignacionAlc + ", nombreEstadoContabilidadAlc="
           + nombreEstadoContabilidadAlc + ", nombreEstadoEntrecAlc=" + nombreEstadoEntrecAlc + ", nuorden=" + nuorden + ", nbooking=" + nbooking
           + ", nucnfclt=" + nucnfclt + ", nucnfliq=" + nucnfliq + ", diferencia=" + diferencia + ", comentario=" + comentario + ", procesado="
           + procesado + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the cuenta
   */
  public String getCuenta() {
    return cuenta;
  }

  /**
   * @return the refFecha
   */
  public String getRefFecha() {
    return refFecha;
  }

  /**
   * @return the refOrden
   */
  public String getRefOrden() {
    return refOrden;
  }

  /**
   * @return the sentido
   */
  public String getSentido() {
    return sentido;
  }

  /**
   * @return the tradeDate
   */
  public Date getTradeDate() {
    return tradeDate;
  }

  /**
   * @return the settlementDate
   */
  public Date getSettlementDate() {
    return settlementDate;
  }

  /**
   * @return the importeNorma43
   */
  public BigDecimal getImporteNorma43() {
    return importeNorma43;
  }

  /**
   * @return the importeSibbac
   */
  public BigDecimal getImporteSibbac() {
    return importeSibbac;
  }

  /**
   * @return the tipoImporte
   */
  public String getTipoImporte() {
    return tipoImporte;
  }

  /**
   * @return the nombreEstadoAsignacionAlc
   */
  public String getNombreEstadoAsignacionAlc() {
    return nombreEstadoAsignacionAlc;
  }

  /**
   * @return the nombreEstadoContabilidadAlc
   */
  public String getNombreEstadoContabilidadAlc() {
    return nombreEstadoContabilidadAlc;
  }

  /**
   * @return the nombreEstadoEntrecAlc
   */
  public String getNombreEstadoEntrecAlc() {
    return nombreEstadoEntrecAlc;
  }

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @return the diferencia
   */
  public BigDecimal getDiferencia() {
    return diferencia;
  }

  /**
   * @return the comentario
   */
  public String getComentario() {
    return comentario;
  }

  /**
   * @return the procesado
   */
  public Boolean getProcesado() {
    return procesado;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param cuenta the cuenta to set
   */
  public void setCuenta(String cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * @param refFecha the refFecha to set
   */
  public void setRefFecha(String refFecha) {
    this.refFecha = refFecha;
  }

  /**
   * @param refOrden the refOrden to set
   */
  public void setRefOrden(String refOrden) {
    this.refOrden = refOrden;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(String sentido) {
    this.sentido = sentido;
  }

  /**
   * @param tradeDate the tradeDate to set
   */
  public void setTradeDate(Date tradeDate) {
    this.tradeDate = tradeDate;
  }

  /**
   * @param settlementDate the settlementDate to set
   */
  public void setSettlementDate(Date settlementDate) {
    this.settlementDate = settlementDate;
  }

  /**
   * @param importeNorma43 the importeNorma43 to set
   */
  public void setImporteNorma43(BigDecimal importeNorma43) {
    this.importeNorma43 = importeNorma43;
  }

  /**
   * @param importeSibbac the importeSibbac to set
   */
  public void setImporteSibbac(BigDecimal importeSibbac) {
    this.importeSibbac = importeSibbac;
  }

  /**
   * @param tipoImporte the tipoImporte to set
   */
  public void setTipoImporte(String tipoImporte) {
    this.tipoImporte = tipoImporte;
  }

  /**
   * @param nombreEstadoAsignacionAlc the nombreEstadoAsignacionAlc to set
   */
  public void setNombreEstadoAsignacionAlc(String nombreEstadoAsignacionAlc) {
    this.nombreEstadoAsignacionAlc = nombreEstadoAsignacionAlc;
  }

  /**
   * @param nombreEstadoContabilidadAlc the nombreEstadoContabilidadAlc to set
   */
  public void setNombreEstadoContabilidadAlc(String nombreEstadoContabilidadAlc) {
    this.nombreEstadoContabilidadAlc = nombreEstadoContabilidadAlc;
  }

  /**
   * @param nombreEstadoEntrecAlc the nombreEstadoEntrecAlc to set
   */
  public void setNombreEstadoEntrecAlc(String nombreEstadoEntrecAlc) {
    this.nombreEstadoEntrecAlc = nombreEstadoEntrecAlc;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @param diferencia the diferencia to set
   */
  public void setDiferencia(BigDecimal diferencia) {
    this.diferencia = diferencia;
  }

  /**
   * @param comentario the comentario to set
   */
  public void setComentario(String comentario) {
    this.comentario = comentario;
  }

  /**
   * @param procesado the procesado to set
   */
  public void setProcesado(Boolean procesado) {
    this.procesado = procesado;
  }

} // Norma43Descuadres
