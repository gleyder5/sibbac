package sibbac.business.conciliacion.util;


import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData;
import sibbac.business.conciliacion.database.dto.AnotacionECCDTO;
import sibbac.business.conciliacion.database.dto.BalanceMovimientoCuentaVirtualDTO;
import sibbac.business.conciliacion.database.dto.BalanceMovimientoCuentaVirtualISIN_DTO;
import sibbac.business.conciliacion.database.dto.ConciliacionDiariaDTO;
import sibbac.business.conciliacion.database.dto.DiferenciaConciliacionEfectivoDTO;
import sibbac.business.conciliacion.database.dto.DiferenciaConciliacionTituloDTO;
import sibbac.business.conciliacion.database.dto.GrupoBalanceCliente;
import sibbac.business.conciliacion.database.dto.GrupoBalanceISIN;
import sibbac.business.conciliacion.database.dto.MovimientoCuentaVirtualDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtual;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionEfectivo;
import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionTitulo;
import sibbac.business.wrappers.database.data.AnotacioneccData;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.common.utils.FormatDataUtils;


public class ConciliacionUtil {

	private static final Logger		LOG	= LoggerFactory.getLogger( ConciliacionUtil.class );
	private transient static ConciliacionUtil	instance;

	private ConciliacionUtil() {
		//
	}

	public static ConciliacionUtil getInstance() {
		if ( instance == null ) {
			synchronized(ConciliacionUtil.class){
				if( instance == null ){
					instance = new ConciliacionUtil();
				}
			}
			
		}
		return instance;
	}

	public Date parseStringToDate( String strDate ) throws ParseException {
		Date date;
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );
		date = sdf.parse( strDate );
		return date;
	}

	public String parseDateToString( Date fecha ) {
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );

		return ( fecha != null ) ? sdf.format( fecha ) : "";
	}

	public List< DiferenciaConciliacionEfectivoDTO > entityEfListToDTO( List< Tmct0DiferenciaConciliacionEfectivo > entities ) {
		List< DiferenciaConciliacionEfectivoDTO > resultList = new LinkedList< DiferenciaConciliacionEfectivoDTO >();
		for ( Tmct0DiferenciaConciliacionEfectivo entity : entities ) {
			DiferenciaConciliacionEfectivoDTO dto = entityEfToDTO( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	public List< ConciliacionDiariaDTO > entityDiListToDTO( List< Object[] > entities ) {
		List< ConciliacionDiariaDTO > resultList = new LinkedList< ConciliacionDiariaDTO >();
		for ( Object[] entity : entities ) {
			ConciliacionDiariaDTO dto = entityToDiariaDTO( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	public List< ConciliacionDiariaDTO > entityDiListToDTOCdOperacion( List< Object[] > entities ) {
		List< ConciliacionDiariaDTO > resultList = new LinkedList< ConciliacionDiariaDTO >();
		for ( Object[] entity : entities ) {
			ConciliacionDiariaDTO dto = entityToDiariaDTOCdOperacion( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	public DiferenciaConciliacionEfectivoDTO entityEfToDTO( Tmct0DiferenciaConciliacionEfectivo entity ) {
		DiferenciaConciliacionEfectivoDTO dto = new DiferenciaConciliacionEfectivoDTO();
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );
		//
		if ( entity != null ) {
			dto.setIdCuentaCompensacion( entity.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion() );
			dto.setES3( entity.getES3() );
			dto.setESV( entity.getESV() );
			dto.setId( entity.getId() );
			dto.setIsin( entity.getIsin() );
			dto.setSentido( entity.getSentido() );
			dto.setTradeDate( ( entity.getTradeDate() != null ) ? sdf.format( entity.getTradeDate() ) : "" );
			dto.setFechaLiquidacion( ( entity.getFechaLiquidacion() != null ) ? sdf.format( entity.getFechaLiquidacion() ) : "" );
			// calculo diferencia
			BigDecimal dif = BigDecimal.ZERO;
			if ( dto.getESV() != null && dto.getES3() != null ) {
				BigDecimal esv = dto.getESV();
				BigDecimal es3 = dto.getES3();
				dif = esv.add( es3.negate() );
			}
			dto.setDiferencia( dif );
		}
		return dto;
	}

	public List< DiferenciaConciliacionTituloDTO > entityTTListToDTO( List< Tmct0DiferenciaConciliacionTitulo > entities ) {
		List< DiferenciaConciliacionTituloDTO > resultList = new LinkedList< DiferenciaConciliacionTituloDTO >();
		for ( Tmct0DiferenciaConciliacionTitulo entity : entities ) {
			DiferenciaConciliacionTituloDTO dto = entityTTToDTO( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	public DiferenciaConciliacionTituloDTO entityTTToDTO( Tmct0DiferenciaConciliacionTitulo entity ) {
		DiferenciaConciliacionTituloDTO dto = new DiferenciaConciliacionTituloDTO();
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );
		//
		if ( entity != null ) {
			dto.setIdCuentaCompensacion( entity.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion() );
			dto.setTS3( entity.getTS3() );
			dto.setTSV( entity.getTSV() );
			dto.setId( entity.getId() );
			dto.setIsin( entity.getIsin() );
			dto.setSentido( entity.getSentido() );
			dto.setTradeDate( ( entity.getTradeDate() != null ) ? sdf.format( entity.getTradeDate() ) : "" );
			dto.setFechaLiquidacion( ( entity.getFechaLiquidacion() != null ) ? sdf.format( entity.getFechaLiquidacion() ) : "" );
			// calculo diferencia
			BigDecimal dif = BigDecimal.ZERO;
			if ( dto.getTSV() != null && dto.getTS3() != null ) {
				BigDecimal esv = dto.getTSV();
				BigDecimal es3 = dto.getTS3();
				dif = esv.add( es3.negate() );
			}
			dto.setDiferencia( dif );
		}
		return dto;
	}

	public ConciliacionDiariaDTO entityToDiariaDTO( Object[] entity ) {
		ConciliacionDiariaDTO dto = null;
		if ( entity != null ) {
			BigDecimal nuTitulos = ( BigDecimal ) entity[ 0 ];
			String isin = entity[ 1 ].toString();
			Date tradedate = ( Date ) entity[ 2 ];
			Date settlementdate = ( Date ) entity[ 3 ];
			Character sentido = entity[ 4 ].toString().charAt( 0 );
			BigDecimal precio = ( BigDecimal ) entity[ 5 ];
			String camara = entity[ 6 ].toString();
			String bolsa = entity[ 7 ].toString();
			String nuordenmkt = entity[ 8 ].toString();
			String nuexemkt = entity[ 9 ].toString();
			Date feMkt = ( Date ) entity[ 10 ];
			String estado = "NO CASADO";
			if ( entity.length == 12 )
				estado = entity[ 11 ].toString();

			dto = new ConciliacionDiariaDTO( nuTitulos, isin, parseDateToString( tradedate ), parseDateToString( settlementdate ), sentido,
					precio, camara, bolsa, nuordenmkt, nuexemkt, feMkt, estado );
		}
		return dto;
	}

	public ConciliacionDiariaDTO entityToDiariaDTOCdOperacion( Object[] entity ) {
		ConciliacionDiariaDTO dto = null;
		if ( entity != null ) {
			BigDecimal nuTitulos = ( BigDecimal ) entity[ 0 ];
			BigDecimal efectivo = ( BigDecimal ) entity[ 1 ];
			String isin = entity[ 2 ].toString();
			Date tradedate = ( Date ) entity[ 3 ];
			Date settlementdate = ( Date ) entity[ 4 ];
			Character sentido = entity[ 5 ].toString().charAt( 0 );
			BigDecimal precio = ( BigDecimal ) entity[ 6 ];
			String camara = entity[ 7 ].toString();
			String bolsa = entity[ 8 ].toString();
			String nuordenmkt = entity[ 9 ].toString();
			String nuexemkt = entity[ 10 ].toString();
			Date feMkt = ( Date ) entity[ 11 ];
			String cdOperacion = entity[ 12 ].toString();
			String descIsin = "-";
			if ( entity[ 13 ] != null ) {
				descIsin = entity[ 13 ].toString();
			}
			String estado = "NO CASADO";
			if ( entity.length == 15 )
				estado = entity[ 14 ].toString();

			dto = new ConciliacionDiariaDTO( nuTitulos, isin, parseDateToString( tradedate ), parseDateToString( settlementdate ), sentido,
					precio, camara, bolsa, nuordenmkt, nuexemkt, feMkt, estado );
			dto.setCdoperacionecc( cdOperacion );
			dto.setImefectivo( efectivo );
			dto.setDescIsin( descIsin );
		}
		return dto;
	}

	public AnotacionECCDTO entityToDTO( Tmct0anotacionecc entity ) {
		AnotacionECCDTO dto = new AnotacionECCDTO();
		if ( entity != null ) {
			dto.setCdcamara( ( entity.getCdcamara() != null ) ? entity.getCdcamara() : "" );
			dto.setCdisin( entity.getCdisin() );
			dto.setCdmiembromkt( ( entity.getCdmiembromkt() == null ) ? null : entity.getCdmiembromkt() );
			// dto.setCdoperacion((entity.getCdoperacion() == null));
			dto.setNuordenmkt( entity.getNuordenmkt() );
			dto.setImtitulosdisponibles( entity.getImtitulosdisponibles() );
			dto.setCdoperacionmkt(entity.getCdoperacionmkt());
			dto.setNuexemkt( entity.getNuexemkt() );
			dto.setCdoperacionecc(( entity.getCdoperacionecc() == null ) ? null : entity.getCdoperacionecc() );
			dto.setCdsentido( entity.getCdsentido() );
			dto.setFcontratacion( ( entity.getFcontratacion() != null ) ? parseDateToString( entity.getFcontratacion() ) : null );
			dto.setFliquidacion( ( entity.getFliqteorica() == null ) ? null : parseDateToString( entity.getFliqteorica() ) );
			dto.setIdanotacionecc( ( entity.getIdanotacionecc() == null ) ? -1 : entity.getIdanotacionecc() );
			dto.setImefectivo( entity.getImefectivo() );
			dto.setImprecio( ( entity.getImprecio() == null ) ? BigDecimal.ZERO : entity.getImprecio() );
			dto.setNutitulos( entity.getNutitulos() );
			dto.setCdCuenta( entity.getTmct0CuentasDeCompensacion().getCdCodigo() );
			dto.setCodigoCuentaCompensacion( entity.getTmct0CuentasDeCompensacion().getCdCuentaCompensacion() );
			dto.setNuoperacioninic( entity.getNuoperacioninic() );
			dto.setDiferencia( BigDecimal.ZERO );
		}
		return dto;
	}
	
	public AnotacionECCDTO entityToDTO( Tmct0operacioncdecc entity ) {
		AnotacionECCDTO dto = new AnotacionECCDTO();
		if ( entity != null ) {
			dto.setCdcamara( ( entity.getCdcamara() != null ) ? entity.getCdcamara() : "" );
			dto.setCdisin( entity.getCdisin() );
			dto.setCdmiembromkt( ( entity.getCdmiembromkt() == null ) ? null : entity.getCdmiembromkt() );
			// dto.setCdoperacion((entity.getCdoperacion() == null));
			dto.setNuordenmkt( entity.getNuordenmkt() );
			dto.setImtitulosdisponibles( entity.getImtitulosdisponibles() );
			dto.setCdoperacionmkt(entity.getCdoperacionmkt());
			dto.setNuexemkt( entity.getNuexemkt() );
			dto.setCdoperacionecc(( entity.getCdoperacionecc() == null ) ? null : entity.getCdoperacionecc() );
			dto.setCdsentido( entity.getCdsentido() );
			dto.setFcontratacion( ( entity.getFcontratacion() != null ) ? parseDateToString( entity.getFcontratacion() ) : null );
			dto.setFliquidacion( ( entity.getFliqteorica() == null ) ? null : parseDateToString( entity.getFliqteorica() ) );
			dto.setImefectivo( entity.getImefectivo() );
			dto.setImprecio( ( entity.getImprecio() == null ) ? BigDecimal.ZERO : entity.getImprecio() );
			dto.setNutitulos( entity.getImtitulosdisponibles());
			dto.setCdCuenta( entity.getTmct0CuentasDeCompensacion().getCdCodigo() );
			dto.setCodigoCuentaCompensacion( entity.getTmct0CuentasDeCompensacion().getCdCuentaCompensacion() );
			dto.setNuoperacioninic( entity.getNuoperacioninic() );
			dto.setDiferencia( BigDecimal.ZERO );
		}
		return dto;
	}
	
	public List< AnotacionECCDTO > entitiesToDTOList( List< Tmct0anotacionecc > objetos ) {
		List< AnotacionECCDTO > resultList = new ArrayList< AnotacionECCDTO >();

		for ( Tmct0anotacionecc object : objetos ) {
			AnotacionECCDTO dto = entityToDTO( object );
			resultList.add( dto );
		}
		
		return resultList;
	}

	public List< AnotacionECCDTO > opEntitiesToDTOList( List< Tmct0operacioncdecc > objetos ) {
		List< AnotacionECCDTO > resultList = new ArrayList< AnotacionECCDTO >();

		for ( Tmct0operacioncdecc object : objetos ) {
			AnotacionECCDTO dto = entityToDTO( object );
			resultList.add( dto );
		}
		
		return resultList;
	}

	public List< AnotacionECCDTO > entitiesListToAnDTOList( List< AnotacioneccData > objetos ) {
		List< AnotacionECCDTO > resultList = new ArrayList< AnotacionECCDTO >();

		for ( AnotacioneccData object : objetos ) {

			Tmct0anotacionecc entity = ( Tmct0anotacionecc ) object.getAnotacionEcc();
			Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion = new Tmct0CuentasDeCompensacion();
			tmct0CuentasDeCompensacion.setCdCodigo( object.getCodCuentaCompensacion() );
			entity.setTmct0CuentasDeCompensacion( tmct0CuentasDeCompensacion );
			BigDecimal titulos = object.getTitulos();
			BigDecimal efectivos = object.getEfectivo();
			AnotacionECCDTO dto = entityToDTO( entity );
			dto.setIsinDescripcion( object.getIsinDescripcion() );
			dto.setNutitulos( titulos );
			dto.setImefectivo( efectivos );
			dto.setImprecio( object.getImprecio() );
			resultList.add( dto );
		}
		return resultList;
	}

	// ///////////////////////////////////////////////
	/* MovimientoCuentaVirtual */
	// ///////////////////////////////////////////////
	/**
	 * se manda el dto porque si no es nulo significa que hay que complementar
	 * los datos
	 * 
	 * @param mvdto
	 * @param dto
	 * @return
	 */
	public BalanceMovimientoCuentaVirtualDTO entityToDTO( MovimientoCuentaVirtualDTO mvdto, BalanceMovimientoCuentaVirtualDTO dto,
			BigDecimal efectivos, BigDecimal titulos ) {
		// si el dto trae idClient 0 es que no se ha encontrado
		// en la lista de dtos disponibles en la lista, por tanto
		// se cumplimentan todos los datos extraidos de la entidad
		if ( mvdto.getAlias() != null && !"".equals( mvdto.getAlias() ) ) {
			dto.setAlias( mvdto.getAlias() );
			dto.setDescralias( mvdto.getDescrAli() );
		}
		GrupoBalanceCliente grp = new GrupoBalanceCliente();
		grp.setEfectivo( efectivos );
		grp.setTitulos( titulos );
		grp.setIsin( mvdto.getIsin() );
		grp.setDescrisin( mvdto.getDescrIsin() );
		boolean valido = true;
		if ( mvdto.getSettlementdate() != null && !"".equals( mvdto.getSettlementdate() ) ) {
			Date fliquidacion = FormatDataUtils.convertStringToDate( mvdto.getSettlementdate() );
			// si la fecha de liquidacion es antes del dia actual
			// no es valido
			if ( fliquidacion.compareTo( new Date() ) < 0 ) {
				valido = false;
			}
		}
		grp.setValido( valido );
		grp.setFliquidacion( mvdto.getSettlementdate() );
		//
		dto.addGrupoBalanceCliente( grp );
		//
		return dto;
	}

	/**
	 * Cumplimenta el dto con nuevos datos
	 * 
	 * @param mvdto
	 * @param dto
	 * @param efectivos
	 * @param titulos
	 * @return
	 */
	public BalanceMovimientoCuentaVirtualISIN_DTO entityToDTO( MovimientoCuentaVirtualDTO mvdto,
			BalanceMovimientoCuentaVirtualISIN_DTO dto, BigDecimal efectivos, BigDecimal titulos ) {

		if ( mvdto.getIsin() != null && !"".equals( mvdto.getIsin() ) ) {
			dto.setIsin( mvdto.getIsin() );
			dto.setDescrisin( mvdto.getDescrIsin() );
		}
		GrupoBalanceISIN grp = new GrupoBalanceISIN();
		grp.setEfectivo( efectivos );
		grp.setTitulos( titulos );
		grp.setAlias( mvdto.getAlias() );
		grp.setDescAli( mvdto.getDescrAli() );
		boolean valido = true;
		if ( mvdto.getSettlementdate() != null && !"".equals( mvdto.getSettlementdate() ) ) {
			Date fliquidacion = FormatDataUtils.convertStringToDate( mvdto.getSettlementdate() );
			// si la fecha de liquidacion es antes del dia actual
			// no es valido
			if ( fliquidacion.compareTo( new Date() ) < 0 ) {
				valido = false;
			}
		}
		grp.setValido( valido );
		grp.setFliquidacion( mvdto.getSettlementdate() );
		//
		dto.addGrupoBalanceISIN( grp );
		//
		return dto;
	}

	/**
	 * Convierte los valores de cada elemento en el mapa a Serializable para que
	 * la query lo pueda castear automaticamente al valor que corresponda y no
	 * lo tome siempre como un String
	 * 
	 * @param filtros
	 * @return
	 */
	public Map< String, Serializable > normalizeFilters( Map< String, String > filtros ) {
		Map< String, Serializable > params = new HashMap< String, Serializable >();
		for ( Map.Entry< String, String > entry : filtros.entrySet() ) {
			if ( entry.getKey() != null && !"".equals( entry.getKey() ) && entry.getValue() != null && !"".equals( entry.getValue() ) ) {
				if ( entry.getKey().startsWith( "f" ) ) {
					Date fecha = FormatDataUtils.convertStringToDate( entry.getValue() );
					params.put( entry.getKey(), fecha );
				} else if ( entry.getKey().startsWith( "id" ) ) {
					long id = Long.valueOf( entry.getValue() );
					params.put( entry.getKey(), id );
				} else if ( entry.getKey().startsWith( "cuadrados" ) ) {
					LOG.debug( "Se elimina el valor de cuadrados:" );
				} else {
					params.put( entry.getKey(), entry.getValue() );
				}
			}
		}
		return params;
	}

	public MovimientoCuentaVirtualDTO entityToDTO( MovimientoCuentaVirtual entity ) {
		MovimientoCuentaVirtualDTO dto = new MovimientoCuentaVirtualDTO();
		dto.setAlias( entity.getAlias() == null ? "" : entity.getAlias().trim() );
		dto.setDescrAli( ( entity.getDescrali() == null ) ? "" : entity.getDescrali() );
		dto.setCamara( entity.getCamara() );
		dto.setCdcodigoccomp( ( entity.getCdcodigoccomp() == null ) ? "" : entity.getCdcodigoccomp() );
		dto.setCdcodigocliq( ( entity.getCdcodigocliq() == null ) ? "" : entity.getCdcodigocliq() );
		dto.setEfectivo( entity.getEfectivo() );
		dto.setId( ( entity.getId() == null ) ? 0 : entity.getId() );
		dto.setIdcuentaliq( entity.getIdcuentaliq() );
		dto.setIsin( entity.getIsin() );
		dto.setDescrIsin( ( entity.getDescripcionisin() == null ) ? "" : entity.getDescripcionisin() );
		dto.setTitulos( entity.getTitulos() );
		dto.setSentido( entity.getSentido() );
		dto.setSentidocuentavirtual( entity.getSentidocuentavirtual() );
		dto.setSettlementdate( ( entity.getSettlementdate() != null ) ? parseDateToString( entity.getSettlementdate() ) : null );
		dto.setTradedate( ( entity.getTradedate() != null ) ? parseDateToString( entity.getTradedate() ) : null );
		return dto;
	}

	public List< MovimientoCuentaVirtualDTO > movimientosToDTO( List< MovimientoCuentaVirtualData > resultList ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );

		List< MovimientoCuentaVirtualDTO > dtos = new LinkedList< MovimientoCuentaVirtualDTO >();
		for ( MovimientoCuentaVirtualData object : resultList ) {
			MovimientoCuentaVirtual entity = ( MovimientoCuentaVirtual ) object.getMovimientoCuentaVirtual();
			MovimientoCuentaVirtualDTO entityToDTO = new MovimientoCuentaVirtualDTO();
			if ( entity != null ) {
				entityToDTO = entityToDTO( entity );
			} else {
				entityToDTO.setIsin( object.getIsin() );
				entityToDTO.setSettlementdate( dateFormat.format( object.getSettlementdate() ) );
				entityToDTO.setDescrIsin( object.getDescripcionisin() );
			}
			try {
				// se cambian los valores de los titulos y efectivos por los
				// agrupados
				entityToDTO.setTitulos( object.getTitulos() );
				entityToDTO.setEfectivo( object.getEfectivo() );
				entityToDTO.setCdCodigo( object.getCdCodigo() );
				entityToDTO.setSistemaLiq( object.getSistemaLiq() );
			} catch ( NumberFormatException ex ) {
				LOG.error( ex.getMessage(), ex );
			}
			dtos.add( entityToDTO );
		}
		return dtos;
	}

	public Norma43MovimientoDTO entityToDTO( Norma43Movimiento entity ) {
		Norma43MovimientoDTO dto = new Norma43MovimientoDTO();
		if ( entity != null ) {
			dto.setCuenta( entity.getFichero().getCuenta() );
			dto.setDebe( entity.getDebe() );
			dto.setDebeFich( entity.getFichero().getDebe() );
			dto.setDivisa( entity.getFichero().getDivisa() );
			dto.setDocumento( entity.getDocumento() );
			dto.setEntidad( entity.getFichero().getEntidad() );
			dto.setId( entity.getId() );
			dto.setIdFichero( entity.getFichero().getId() );
			dto.setImporte( entity.getImporte() );
			dto.setOficina( entity.getFichero().getOficina() );
			dto.setSaldofinal( entity.getFichero().getSaldofinal() );
			dto.setSaldoinicial( entity.getFichero().getSaldoinicial() );
			dto.setSettlementdate( parseDateToString( entity.getSettlementdate() ) );
			dto.setTradedate( parseDateToString( entity.getTradedate() ) );
		}
		return dto;
	}

	public List< Norma43MovimientoDTO > entitiesToDTO( List< Norma43Movimiento > entities ) {
		List< Norma43MovimientoDTO > dtoList = new LinkedList< Norma43MovimientoDTO >();
		for ( Norma43Movimiento entity : entities ) {
			dtoList.add( entityToDTO( entity ) );
		}
		return dtoList;
	}

	/**
	 * Netea por sentido venta (V o C) y sentido movimiento virtual (M o C)
	 * teniendo en cuenta fechas de liquidacion
	 * 
	 * @param anotaciones
	 * @param resultList
	 * @return
	 */
	public List< MovimientoCuentaVirtualDTO > neteoMercadoCliente( List< MovimientoCuentaVirtualDTO > anotaciones,
			List< MovimientoCuentaVirtualData > resultList ) {
		List< MovimientoCuentaVirtualDTO > primerNeteo = neteoMercadoClientePorSentidoVirtual( anotaciones, resultList );
		List< MovimientoCuentaVirtualDTO > segundoNeteo = neteoMercadoClientePorSentido( primerNeteo );
		return segundoNeteo;
	}

	/**
	 * Netea por sentido venta (V o C) y sentido movimiento virtual (M o C)
	 * obviando las fechas
	 * 
	 * @param anotaciones
	 * @param resultList
	 * @param obviaFechas
	 * @return
	 */
	public List< MovimientoCuentaVirtualDTO > neteoMercadoCliente( List< MovimientoCuentaVirtualDTO > anotaciones,
			List< MovimientoCuentaVirtualData > resultList, boolean obviaFechas, boolean distinctClients ) {
		List< MovimientoCuentaVirtualDTO > primerNeteo = neteoMercadoClientePorSentidoVirtual( anotaciones, resultList, obviaFechas,
				distinctClients );
		List< MovimientoCuentaVirtualDTO > segundoNeteo = neteoMercadoClientePorSentido( primerNeteo, obviaFechas, distinctClients );
		return segundoNeteo;
	}

	/**
	 * 
	 * @param anotaciones
	 * @param resultList
	 * @param obviaFechas
	 * @return
	 */
	public List< MovimientoCuentaVirtualDTO > neteoMercadoClientePorSentidoVirtual( List< MovimientoCuentaVirtualDTO > anotaciones,
			List< MovimientoCuentaVirtualData > resultList, boolean obviaFechas, boolean distinctClients ) {

		List< MovimientoCuentaVirtualDTO > resultado = new ArrayList<>();
		boolean cazado = false;

		for ( int i = 0; i < anotaciones.size(); i++ ) {
			MovimientoCuentaVirtualDTO anC = anotaciones.get( i );
			for ( int z = i + 1; z < resultList.size(); z++ ) {
				MovimientoCuentaVirtualData obj = resultList.get( z );
				MovimientoCuentaVirtual entity = ( MovimientoCuentaVirtual ) obj.getMovimientoCuentaVirtual();
				MovimientoCuentaVirtualDTO anV = entityToDTO( entity );
				BigDecimal sumTitulos = ( BigDecimal ) obj.getTitulos();
				BigDecimal sumEfectivo = ( BigDecimal ) obj.getEfectivo();
				anV.setTitulos( sumTitulos );
				anV.setEfectivo( sumEfectivo );
				// Si la anotacion con la que se va a comparar es sentido cuenta
				// virtual contrario y tiene el mismo sentido C o V
				if ( anV.getSentidocuentavirtual() != anC.getSentidocuentavirtual() ) {
					if ( anV.getIsin().equals( anC.getIsin() ) && anV.getSentido() == anC.getSentido()
							&& anV.getIdcuentaliq() == anC.getIdcuentaliq() ) {
						// Si tiene que comprobar las fechas es decir
						// obviaFechas = false
						// pasa por aqui, si no son iguales pasa al siguiente
						// objeto dentro
						// del for
						if ( !obviaFechas ) {
							if ( anV != null && anV.getSettlementdate() != null
									&& !anV.getSettlementdate().equals( anC.getSettlementdate() ) ) {
								continue;
							}
						}
						// clientecdbrocli y clientenumsec es la clave del
						// cliente
						if ( distinctClients ) {
							if ( !anV.getAlias().trim().equals( anC.getAlias().trim() ) ) {
								continue;
							}
						}
						// Se averigua cual de los dos objetos es el de
						// sentidocuentavirutal
						// (M "Mercado" o C "Cliente"), Mercado son compras y
						// Cliente son ventas
						MovimientoCuentaVirtualDTO mercado = null;
						MovimientoCuentaVirtualDTO cliente = null;
						if ( anV.getSentidocuentavirtual() == 'M' ) {
							mercado = anV;
							cliente = anC;
						} else {
							mercado = anC;
							cliente = anV;
						}

						cazado = true;
						MovimientoCuentaVirtualDTO dif = new MovimientoCuentaVirtualDTO();
						rellenarMovimientoDTONeteadoMercadoOCliente( mercado, cliente, dif );

						if ( !checkIfExistObjectInDTOListSCV( dif, resultado, obviaFechas, distinctClients ) ) {
							resultado.add( dif );
						}

					}
				}
			}
			if ( !cazado && anC.getTitulos().doubleValue() > 0 ) {
				if ( !checkIfExistObjectInDTOListSCV( anC, resultado, obviaFechas, distinctClients ) ) {
					resultado.add( anC );
				}
			} else {
				cazado = false;
			}
		}
		return resultado;
	}

	/**
	 * Rellena un objeto MovimientoCuentaVirtualDTO segun los valores de los
	 * parametros entrantes
	 * 
	 * @param mercado
	 *            - MovimientoCuentaVirtualDTO
	 * @param cliente
	 *            - MovimientoCuentaVirtualDTO
	 * @param dif
	 *            - MovimientoCuentaVirtualDTO
	 */
	private void rellenarMovimientoDTONeteadoMercadoOCliente( MovimientoCuentaVirtualDTO mercado, MovimientoCuentaVirtualDTO cliente,
			MovimientoCuentaVirtualDTO dif ) {
		int signo = cliente.getTitulos().compareTo( mercado.getTitulos() );
		boolean isSentidoVenta = ( signo > 0 ) ? true : false;
		// Se toma el id del sentido venta (Cliente).
		long id = ( isSentidoVenta ) ? cliente.getId() : mercado.getId();
		dif.setId( id );
		dif.setCamara( cliente.getCamara() );
		dif.setCdcodigoccomp( cliente.getCdcodigoccomp() );
		dif.setIsin( cliente.getIsin() );
		dif.setDescrIsin( cliente.getDescrIsin() );
		dif.setSettlementdate( cliente.getSettlementdate() );
		dif.setTradedate( cliente.getTradedate() );
		dif.setIdcuentaliq( cliente.getIdcuentaliq() );
		dif.setDescrAli( cliente.getDescrAli() );
		dif.setAlias( cliente.getAlias() );
		// el objeto diferencia tendra los titulos de
		// compra
		// y los efectivos de venta
		BigDecimal titulos = mercado.getTitulos().subtract( cliente.getTitulos() ).abs();
		dif.setTitulos( titulos );
		dif.setEfectivo( cliente.getEfectivo() );
		char sentido = ( isSentidoVenta ) ? 'C' : 'M';
		dif.setSentidocuentavirtual( sentido );
		dif.setSentido( mercado.getSentido() );
		dif.setPrecio( mercado.getPrecio() );
		// calcula diferencia de titulos ventas y
		// compras
		BigDecimal difTitulos = mercado.getTitulos().add( cliente.getTitulos().negate() );
		// introduce la diferencia de titulos
		dif.setTitulos( BigDecimal.valueOf( Math.abs( difTitulos.floatValue() ) ) );
		// calcula diferencia de efectivos
		BigDecimal difEfectivos = mercado.getEfectivo().subtract( cliente.getEfectivo() ).abs();
		dif.setEfectivo( difEfectivos );
	}

	/**
	 * 
	 * @param anotaciones
	 * @param obviaFechas
	 * @return
	 */
	public List< MovimientoCuentaVirtualDTO > neteoMercadoClientePorSentido( List< MovimientoCuentaVirtualDTO > anotaciones,
			boolean obviaFechas, boolean distinctClients ) {
		List< MovimientoCuentaVirtualDTO > resultado = new CopyOnWriteArrayList<>();
		List< MovimientoCuentaVirtualDTO > resultadoFinal = new CopyOnWriteArrayList<>();

		resultado.addAll( anotaciones );

		boolean cazado = false;
		for ( int i = 0; i < anotaciones.size(); i++ ) {
			MovimientoCuentaVirtualDTO anC = anotaciones.get( i );
			for ( int z = i + 1; z < resultado.size(); z++ ) {
				MovimientoCuentaVirtualDTO anV = resultado.get( z );

				// Si la anotacion con la que se va a comparar es sentido
				// venta
				if ( anV.getSentido() != anC.getSentido() ) {
					if ( anV.getIsin().equals( anC.getIsin() ) && anV.getSentidocuentavirtual() == anC.getSentidocuentavirtual()
							&& anV.getIdcuentaliq() == anC.getIdcuentaliq() ) {
						// Si tiene que comprobar las fechas es decir
						// obviaFechas = false
						// pasa por aqui, si no son iguales pasa al siguiente
						// objeto dentro
						// del for
						if ( !obviaFechas ) {
							if ( !anV.getSettlementdate().equals( anC.getSettlementdate() ) ) {
								continue;
							}
						}
						// clientecdbrocli y clientenumsec es la clave del
						// cliente
						if ( distinctClients ) {
							if ( !anV.getAlias().trim().equals( anC.getAlias().trim() ) ) {
								continue;
							}
						}
						MovimientoCuentaVirtualDTO compra = null;
						MovimientoCuentaVirtualDTO venta = null;
						if ( anV.getSentido() == 'C' ) {
							compra = anV;
							venta = anC;
						} else {
							compra = anC;
							venta = anV;
						}
						int signo = venta.getTitulos().compareTo( compra.getTitulos() );
						boolean isSentidoVenta = ( signo > 0 ) ? true : false;

						cazado = true;
						if ( 0 != signo ) {
							MovimientoCuentaVirtualDTO dif = new MovimientoCuentaVirtualDTO();
							rellenarMovimientoCuentaVirtualDTOPorSentidoVenta( compra, venta, isSentidoVenta, dif );

							if ( !checkIfExistObjectInDTOListSC( dif, resultadoFinal, obviaFechas, distinctClients ) ) {
								resultadoFinal.add( dif );
							}

						}
					}
				}
			}
			if ( !cazado && anC.getTitulos().doubleValue() > 0 ) {
				if ( !checkIfExistObjectInDTOListSC( anC, resultadoFinal, obviaFechas, distinctClients ) ) {
					resultadoFinal.add( anC );
				}
			} else {
				cazado = false;
			}
		}
		return resultadoFinal;
	}

	private void rellenarMovimientoCuentaVirtualDTOPorSentidoVenta( MovimientoCuentaVirtualDTO compra, MovimientoCuentaVirtualDTO venta,
			boolean isSentidoVenta, MovimientoCuentaVirtualDTO dif ) {
		// Se toma el id del objeto que haya obtenido mayor
		// resultado en la diferencia de titulos
		long id = ( isSentidoVenta ) ? venta.getId() : compra.getId();
		dif.setId( id );
		dif.setCamara( venta.getCamara() );
		dif.setCdcodigoccomp( venta.getCdcodigoccomp() );
		dif.setIsin( venta.getIsin() );
		dif.setDescrIsin( venta.getDescrIsin() );
		dif.setSettlementdate( venta.getSettlementdate() );
		dif.setTradedate( venta.getTradedate() );
		dif.setIdcuentaliq( venta.getIdcuentaliq() );
		dif.setAlias( venta.getAlias() );
		dif.setDescrAli( venta.getDescrAli() );
		// el objeto diferencia tendra los titulos de
		// compra y los efectivos de venta
		BigDecimal titulos = venta.getTitulos().subtract( compra.getTitulos() ).abs();
		dif.setTitulos( titulos );
		dif.setEfectivo( venta.getEfectivo() );
		char sentido = ( isSentidoVenta ) ? 'V' : 'C';
		dif.setSentido( sentido );
		dif.setSentidocuentavirtual( compra.getSentidocuentavirtual() );
		dif.setPrecio( compra.getPrecio() );
		// calcula diferencia de titulos ventas y
		// compras
		BigDecimal difTitulos = compra.getTitulos().subtract( venta.getTitulos() );
		// introduce la diferencia de titulos
		dif.setTitulos( difTitulos.abs() );
		// calcula diferencia de efectivos
		BigDecimal difEfectivos = compra.getEfectivo().subtract( venta.getEfectivo() ).abs();
		dif.setEfectivo( difEfectivos );
	}

	/**
	 * 
	 * @param anotaciones
	 * @param resultList
	 * @return
	 */
	public List< MovimientoCuentaVirtualDTO > neteoMercadoClientePorSentidoVirtual( List< MovimientoCuentaVirtualDTO > anotaciones,
			List< MovimientoCuentaVirtualData > resultList ) {
		return neteoMercadoClientePorSentidoVirtual( anotaciones, resultList, false, false );
	}

	/**
	 * 
	 * @param anotaciones
	 * @return
	 */
	public List< MovimientoCuentaVirtualDTO > neteoMercadoClientePorSentido( List< MovimientoCuentaVirtualDTO > anotaciones ) {
		boolean obviarFechas = false;
		return neteoMercadoClientePorSentido( anotaciones, obviarFechas, false );
	}

	/**
	 * 
	 * @param dif
	 * @param resultado
	 * @param obviarFechas
	 * @return
	 */
	public boolean checkIfExistObjectInDTOListSCV( MovimientoCuentaVirtualDTO dif, List< MovimientoCuentaVirtualDTO > resultado,
			boolean obviarFechas, boolean distinctClients ) {
		boolean result = false;
		for ( int i = 0; i < resultado.size(); i++ ) {
			MovimientoCuentaVirtualDTO dto = resultado.get( i );
			if ( dto.getIsin().equals( dif.getIsin() ) && dto.getSentido() == dif.getSentido()
					&& dto.getIdcuentaliq() == dif.getIdcuentaliq() ) {
				// Si no coinciden las fechas empieza por el siguiente elemento
				if ( dto != null && dto.getSettlementdate() != null && dif != null && dif.getSettlementdate() != null && !obviarFechas
						&& !dto.getSettlementdate().equals( dif.getSettlementdate() ) ) {
					continue;
				}
				// si es balance por cliente solo se añaden clientes diferentes
				// clientecdbrocli y clientenumsec es la clave del cliente
				if ( distinctClients ) {
					if ( dto.getAlias() != null && dif.getAlias() != null && !dto.getAlias().equals( dif.getAlias() ) ) {
						continue;
					}
				}
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * 
	 * @param dif
	 * @param resultado
	 * @param obviarFechas
	 * @return
	 */
	public boolean checkIfExistObjectInDTOListSC( MovimientoCuentaVirtualDTO dif, List< MovimientoCuentaVirtualDTO > resultado,
			boolean obviarFechas, boolean distinctClients ) {
		boolean result = false;
		for ( int i = 0; i < resultado.size(); i++ ) {
			MovimientoCuentaVirtualDTO dto = resultado.get( i );
			if ( dto.getIsin().equals( dif.getIsin() ) && dto.getSentidocuentavirtual() == dif.getSentidocuentavirtual()
					&& dif.getIdcuentaliq() == dto.getIdcuentaliq() ) {
				if ( !obviarFechas && !dto.getSettlementdate().equals( dif.getSettlementdate() ) ) {
					continue;
				}
				//
				if ( distinctClients ) {
					if ( dto.getAlias() != null && dif.getAlias() != null && !dto.getAlias().equals( dif.getAlias() ) ) {
						continue;
					}
				}
				result = true;
				break;
			}
		}
		return result;
	}

	public void normalizeDates( Map< String, Serializable > params ) {
		if ( params.get( "fliquidacionA" ) != null && params.get( "fliquidacionDe" ) == null ) {
			// Si no hay fecha desde y hay fecha hasta ponemos el principio de
			// los tiempos en fecha desde
			Date bigBang = new Date( Long.MIN_VALUE );
			params.put( "fliquidacionDe", bigBang );
		} else if ( params.get( "fliquidacionA" ) == null && params.get( "fliquidacionDe" ) != null ) {
			// Si hay fecha desde y no hay fecha hasta ponemos el final de los
			// tiempos en fecha desde
			SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );

			// Date bigCrunch = new Date(Long.MAX_VALUE);
			Date bigCrunch = null;
			try {
				bigCrunch = sdf.parse( "29880608" );
			} catch ( ParseException e ) {
				LOG.debug( e.getMessage() + "Error parsing when no fliquidacionA" );
			}
			params.put( "fliquidacionA", bigCrunch );
		}
	}

	public void normalizeDatesContratacion( Map< String, Serializable > params ) {
		if ( params.get( "fcontratacionA" ) != null && params.get( "fcontratacionDe" ) == null ) {
			// Si no hay fecha desde y hay fecha hasta ponemos el principio de
			// los tiempos en fecha desde
			Date bigBang = new Date( Long.MIN_VALUE );
			params.put( "fcontratacionDe", bigBang );
		} else if ( params.get( "fcontratacionA" ) == null && params.get( "fcontratacionDe" ) != null ) {
			// Si hay fecha desde y no hay fecha hasta ponemos el final de los
			// tiempos en fecha desde
			SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );

			// Date bigCrunch = new Date(Long.MAX_VALUE);
			Date bigCrunch = null;
			try {
				bigCrunch = sdf.parse( "29880608" );
			} catch ( ParseException e ) {
				LOG.debug( e.getMessage() + "Error parsing when no fcontratacionA" );
			}
			params.put( "fcontratacionA", bigCrunch );
		}
	}

	public boolean checkIfExistParam( String key, Map< String, Serializable > normalizedParams ) {
		boolean exist = false;
		if ( normalizedParams != null ) {
			for ( Map.Entry< String, Serializable > entry : normalizedParams.entrySet() ) {
				if ( entry.getValue() != null ) {
					if ( entry.getKey() != null && !"".equals( entry.getKey() ) && key.equals( entry.getKey() ) ) {
						exist = true;
						break;
					}
				}
			}
		}
		return exist;
	}
	
	/**
	 * Compone el nombre de fichero añadiendo la fecha en que se ha procesado 
	 * para norma43 y S3 dado que los ficheros que se reciben se llaman siempre
	 * igual 
	 * @param nombreFichero
	 * @param extension
	 * @return 
	 */
	public static String getNombreFicheroTratado(String nombreFichero, String extension){
		Calendar fechaActual = Calendar.getInstance();
		String fecha = FormatDataUtils.convertDateToFileName(fechaActual.getTime());
		String nombreficheroProcesado = new StringBuilder(nombreFichero).append("_")
				.append(fecha).append(extension).toString();
		return nombreficheroProcesado;
	}
	
	public static void moveFileToProcessed(String destinationFolder, String path, String nombreFichero, String extensionFichero) 
			throws IOException{
		Path tratados = Paths.get(destinationFolder);
		Path source = Paths.get(path);
		String nombreficheroProcesado = ConciliacionUtil.getNombreFicheroTratado(nombreFichero, extensionFichero);
		Path ficheroDestino = tratados.resolve(nombreficheroProcesado);
		Files.move(source, ficheroDestino, StandardCopyOption.REPLACE_EXISTING);
		LOG.info("Archivo [{}] procesado [nuevo nombre: {}].", source.toString(), ficheroDestino.toString() );
	}
	
	

}
