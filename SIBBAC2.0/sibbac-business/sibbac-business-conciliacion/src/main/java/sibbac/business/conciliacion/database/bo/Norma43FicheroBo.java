package sibbac.business.conciliacion.database.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.Norma43FicheroDao;
import sibbac.business.conciliacion.database.dao.Norma43FicheroDaoImpl;
import sibbac.business.conciliacion.database.data.Norma43FicheroData;
import sibbac.business.conciliacion.database.model.Norma43Concepto;
import sibbac.business.conciliacion.database.model.Norma43Fichero;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.conciliacion.exceptions.Norma43FileLoadException;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.FtpSibbac;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

/**
 * @author XI316153
 */
@Service
public class Norma43FicheroBo extends AbstractBo<Norma43Fichero, Long, Norma43FicheroDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(Norma43FicheroBo.class);

  /** Caracteres que identifican una línea como línea de cuenta. */
  private static final String CUENTA = "11";

  /** Caracteres que identifican una línea como línea de movimiento. */
  private static final String MOVIMIENTO = "22";

  /** Caracteres que identifican una línea como línea de concepto. */
  private static final String CONCEPTO = "23";

  /** Caracteres que identifican una línea como línea de información de equivalencia del importe del apunte. */
  private static final String INFO_EQUIV = "24";

  /** Caracteres que identifican una línea como línea de final de cuenta. */
  private static final String FINAL_DE_CUENTA = "33";

  /** Caracteres que identifican una línea como línea de final de fichero. */
  private static final String FIN_DE_FICHERO = "88";

  /** Formato de las fechas que se leen del fichero. */
  private static final DateFormat FILE_DATE_FORMAT = new SimpleDateFormat("yyMMdd");
  private static final DateFormat FILE_DATE_FORMAT2 = new SimpleDateFormat("yyyyMMdd");
  private static final DateFormat DATE_FORMAT = new SimpleDateFormat("_yyyyMMdd_HHmmss");

  // private static final String PROCESS_USER = "process";
  // private static final String FICHERO_NORMA_43 = "conciliacion.fichero.norma.43";
  // private static final int YEAR_INICIAL = 2000; // si no han cambiado la app pal 2100 tamos muertos

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${conciliacion.norma43.source.folder}") private String sourcePath;
  @Value("${conciliacion.norma43.minorista.SAN.file}") private String norma43MinoristaSanFileName;
  @Value("${conciliacion.norma43.minorista.OPEN.file}") private String norma43MinoristaOpenFileName;
  @Value("${conciliacion.norma43.mayorista.file}") private String norma43MayoristaFileName;
  @Value("${conciliacion.norma43.file.extension}") private String norma43FileExtension;
  @Value("${conciliacion.norma43.destination.folder}") private String destinationFolder;
  @Value("${ftp.url}") private String url;
  @Value("${ftp.port}") private String port;
  @Value("${ftp.username}") private String username;
  @Value("${ftp.password}") private String password;
  @Value("${as400.filename.SAN}") private String filenameSan;
  @Value("${as400.filename.OPEN}") private String filenameOpen;
  @Value("${as400.filename.mayorista}") private String filenameMayorista;
  @Value("${as400.destination.folder}") private String folderAs400;

  @Autowired
  private Norma43FicheroDao norma43FicheroDao;

  @Autowired
  private Norma43FicheroDaoImpl daoImpl;

  @Autowired private Tmct0menBo menBo;

  @Autowired private FtpSibbac ftp;

  private void init() {
  	if (!norma43MinoristaSanFileName.endsWith(norma43FileExtension)) {
    	norma43MinoristaSanFileName = norma43MinoristaSanFileName.concat(norma43FileExtension);
    	norma43MinoristaOpenFileName = norma43MinoristaOpenFileName.concat(norma43FileExtension);
    	norma43MayoristaFileName = norma43MayoristaFileName.concat(norma43FileExtension);
  	}
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Procesa los ficheros norma43 pendientes de procesar que se encuentren en el directorio de entrada.
   * 
   * @param sourcePath Ruta al directorio de entrada de ficheros norma43.
   * @throws Norma43FileLoadException
   * @throws IOException 
   */
  public void procesarFicherosNorma43(String sourcePath) throws Norma43FileLoadException {
  	File ficheroATratar;
    LOG.debug("[procesarFicherosNorma43] Inicio ...");
    init();
    List<String> errores = new ArrayList<String>();
    // Se buscan los ficheros a tratar
    List<Object[]> listaFicherosPendientes = buscarFicheros(sourcePath);

    if (!listaFicherosPendientes.isEmpty()) {
      LOG.debug("[procesarFicherosNorma43] Ficheros a tratar: " + listaFicherosPendientes.size());

//    Enviamos por ftp todos los ficheros encontrados
      try {
		if (!ftp.connect(folderAs400)) {
		      LOG.warn("No se ha podido conectar al FTP de AS400");
		      throw new Norma43FileLoadException("No se ha podido conectar al FTP de AS400");
		  }
      } catch (IOException e) {
    	  LOG.warn("No se ha podido conectar al FTP de AS400",e);
    	  throw new Norma43FileLoadException("No se ha podido conectar al FTP de AS400",e);
      }
      try {
		if (!ftp.send(listaFicherosPendientes)) {
		      LOG.warn("No se ha podido enviar los ficheros a AS400");
		      throw new Norma43FileLoadException("No se ha podido enviar los ficheros a AS400");
		  }
      } catch (IOException e) {
    	  LOG.warn("No se ha podido enviar los ficheros a AS400",e);
    	  throw new Norma43FileLoadException("No se ha podido enviar los ficheros a AS400",e);
      }

      new File(destinationFolder).mkdirs();
      for (Object[] objects : listaFicherosPendientes) {
      	ficheroATratar = (File) objects[0];
        LOG.debug("[procesarFicherosNorma43] Tratando el fichero: {}",ficheroATratar.getName());
        try {
          processFile(objects);
          LOG.debug("[procesarFicherosNorma43] Procesando correctamente el fichero: {}",ficheroATratar.getName());
        } catch (Exception e) {
          LOG.debug("[procesarFicherosNorma43] Error procesando el fichero {}",ficheroATratar.getAbsolutePath(),e);
          errores.add(ficheroATratar.getAbsolutePath());
        }

//      Movemos el fichero a la carpeta de tratados
        try {
            Files.move(((File) objects[0]).toPath(), new File(destinationFolder, (String) objects[2]).toPath());
            LOG.debug("[procesarFicherosNorma43] Movido correctamente el fichero: {}",ficheroATratar.getName());
          } catch (Exception e) {
              LOG.debug("[procesarFicherosNorma43] Error moviendo el fichero {}",ficheroATratar.getAbsolutePath(),e);
              errores.add(ficheroATratar.getAbsolutePath());
          }
      }
    }

    if (errores != null && !errores.isEmpty()) {
      throw new Norma43FileLoadException(
    		  "Han ocurrido errores durante el procesamiento de los ficheros [" + errores.toString() + "]");
    } // if

    LOG.debug("[procesarFicherosNorma43] Fin ...");
  } // procesarFicherosNorma43

  /**
   * 
   * @param filters
   * @return
   */
  public List<Norma43FicheroData> findAllFicheroNorma43(Map<String, Serializable> filters) {
    return daoImpl.findAllFicheroNorma43(filters);
  } // findAllFicheroNorma43

  /**
   * 
   * @return
   */
  public java.util.Date findDateOfLastFileEntry() {
    java.util.Date lastDate = norma43FicheroDao.findDateOfLastFileEntry();
    return lastDate;
  } // findDateOfLastFileEntry

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Procesa el fichero especificado insertando en base de datos los registros recibidos.
   * 
   * @param fichero Fichero a procesar.
   * @throws IOException
   * @throws ParseException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private void processFile(Object[] objects) throws IOException, ParseException {
	  FileReader fr = null;
	  try {
		  File fichero = (File) objects[0];
		  fr = new FileReader(fichero);
		  BufferedReader br = new BufferedReader(fr);
		  String linea = null;

		  try {
			  Norma43Fichero ficheroN43 = null;
			  Norma43Movimiento movimientoN43 = null;

			  List<Norma43Movimiento> movimientos = null;
			  List<Norma43Concepto> conceptos = null;

			  String tipoLinea = null;

			  while ((linea = br.readLine()) != null) {
				  LOG.debug("[procesarFicherosNorma43] Procesando línea [{}] ...", linea);

				  tipoLinea = linea.substring(0, 2);
				  switch (tipoLinea) {
				  case CUENTA:
					  LOG.debug("[procesarFicherosNorma43] Procesando cuenta ...");
					  ficheroN43 = new Norma43Fichero();
					  ficheroN43.setEntidad(new Integer(linea.substring(2, 6)));
					  ficheroN43.setOficina(new Integer(linea.substring(6, 10)));
					  ficheroN43.setCuenta(new Long(linea.substring(10, 20)));
					  try {
						  ficheroN43.setFechaInicial(new java.sql.Date(FILE_DATE_FORMAT.parse(linea.substring(20, 26)).getTime()));
					  } catch (ParseException pe) {
						  LOG.error("[procesarFicherosNorma43] ERROR ({}): {}", pe.getClass().getName(), pe.getMessage());
						  throw pe;
					  } // catch
					  try {
						  ficheroN43.setFechaFinal(new java.sql.Date(FILE_DATE_FORMAT.parse(linea.substring(26, 32)).getTime()));
					  } catch (ParseException pe) {
						  LOG.error("[procesarFicherosNorma43] ERROR ({}): {}", pe.getClass().getName(), pe.getMessage());
						  throw pe;
					  } // catch

					  ficheroN43.setDebe(linea.substring(32, 33));
					  ficheroN43.setSaldoinicial(new Long(linea.substring(33, 47)));
					  ficheroN43.setDivisa(new Integer(linea.substring(47, 50)));
					  ficheroN43.setModaInfo(new Integer(linea.substring(50, 51)));
					  
					  ficheroN43.setNombreAbrev(linea.substring(51, 71));
					  if (linea.length()>71) {
						  ficheroN43.setLibre(linea.substring(71));
					  } // if

					  movimientos = new ArrayList<Norma43Movimiento>();
					  break;
				  case MOVIMIENTO:
					  LOG.debug("[procesarFicherosNorma43] Procesando movimiento ...");
					  movimientoN43 = new Norma43Movimiento();
					  try {
						  movimientoN43.setSettlementdate(new java.sql.Date(FILE_DATE_FORMAT.parse(linea.substring(10, 16))
								  .getTime()));
					  } catch (ParseException pe) {
						  LOG.error("[procesarFicherosNorma43] ERROR ({}): {}", pe.getClass().getName(), pe.getMessage());
						  throw pe;
					  } // catch
					  try {
						  movimientoN43.setTradedate(new java.sql.Date(FILE_DATE_FORMAT.parse(linea.substring(16, 22)).getTime()));
					  } catch (ParseException pe) {
						  LOG.error("[procesarFicherosNorma43] ERROR ({}): {}", pe.getClass().getName(), pe.getMessage());
						  throw pe;
					  } // catch

					  movimientoN43.setConceptoComun(new Integer(linea.substring(22, 24)));
					  movimientoN43.setConceptoPropio(new Integer(linea.substring(24, 27)));
					  movimientoN43.setDebe(linea.substring(27, 28));
					  movimientoN43.setImporte(new BigDecimal(linea.substring(28, 42)));
					  movimientoN43.setDocumento(new BigDecimal(linea.substring(42, 52)));
					  movimientoN43.setReferencia(linea.substring(52, 64));

					  if (linea.length() > 64) {
						  movimientoN43.setDescReferencia(linea.substring(64));
					  } // if

					  movimientoN43.setFichero(ficheroN43);
					  movimientos.add(movimientoN43);

					  conceptos = new ArrayList<Norma43Concepto>();
					  break;
				  case CONCEPTO:
					  LOG.debug("[procesarFicherosNorma43] Procesando concepto ...");
					  Norma43Concepto concepto = new Norma43Concepto();
					  concepto.setDato(Integer.valueOf(linea.substring(2, 4)));
					  if (linea.length()>42) {
						  concepto.setDesc(linea.substring(4, 42));
						  concepto.setDescAdicional(linea.substring(42));
					  } else {
						  concepto.setDesc(linea.substring(4));
					  }
					  concepto.setMovimiento(movimientoN43);
					  conceptos.add(concepto);
					  movimientoN43.setConceptos(conceptos);
					  break;
				  case INFO_EQUIV:
					  LOG.debug("[procesarFicherosNorma43] Procesando información de equivalencia del importe del apunte ...");
					  break;
				  case FINAL_DE_CUENTA:
					  LOG.debug("[procesarFicherosNorma43] Procesando final de cuenta ...");
					  if (ficheroN43 != null) {
						  ficheroN43.setSaldofinal(Long.valueOf(linea.substring(59, 73)));
						  ficheroN43.setMovimientoNorma43(movimientos);
						  ficheroN43.setProcesado(Boolean.FALSE);

						  LOG.debug("[procesarFicherosNorma43] Salvando fichero ...");
						  this.save(ficheroN43);
					  } // if
					  break;
				  case FIN_DE_FICHERO:
					  LOG.debug("[procesarFicherosNorma43] Procesando final de fichero ...");
					  break;
				  default:
					  LOG.error("[procesarFicherosNorma43] Linea desconocida [{}]", tipoLinea);
				  } // switch (tipoLinea)
			  } // while ((linea = br.readLine()) != null)

			  // Se cierra el buffer reader para poder renombrar el fichero
			  br.close();
			  LOG.debug("[procesarFicherosNorma43] Archivo [{}] procesado.", fichero.getAbsolutePath());
		  } catch (IOException ioe) {
			  LOG.error("[procesarFicherosNorma43] ERROR ({}): {}", ioe.getClass().getName(), ioe.getMessage());
			  throw ioe;
		  } // catch
	  } catch (FileNotFoundException fnfe) {
		  LOG.error("[procesarFicherosNorma43] ERROR ({}): {}", fnfe.getClass().getName(), fnfe.getMessage());
		  throw fnfe;
	  } // catch
  } // processFile

  /**
   * Busca los ficheros a tratar.
   * 
   * @return Array de ficheros a tratar.
 * @throws Norma43FileLoadException 
 * @throws IOException 
 * @throws ParseException 
 * @throws SIBBACBusinessException 
   */
  private List<Object[]> buscarFicheros(final String sourcePath) throws Norma43FileLoadException {
	  LOG.debug("[buscarFicheros] Inicio ... Se entra con sourcePath: " + sourcePath);
	  LOG.debug("[buscarFicheros] patrones: {}, {}, {}", norma43MinoristaSanFileName, norma43MinoristaOpenFileName, norma43MayoristaFileName);
	  Object[] objects;
	  Date date;
	  String linea;
	  BufferedReader br = null;
	  List<Object[]> listaFicherosEncontrados = new ArrayList<Object[]>();
	  File searchFolder = new File(sourcePath);
	  if (searchFolder.isDirectory()) {
		  LOG.debug("[buscarFicheros] Buscando ficheros norma 43 ...");
		  for (File file: searchFolder.listFiles()) {
			  LOG.debug("[buscarFicheros] Estudiando el fichero " + file.getName());
			  objects = new Object[3];
			  if (!file.isFile()) {
				  LOG.debug("[buscarFicheros] El file es un directorio");
				  continue;
			  }
			  if (file.getName().equals(norma43MinoristaSanFileName)) {
				  objects[1] = filenameSan;
			  } else if (file.getName().equals(norma43MinoristaOpenFileName)) { 
				  objects[1] = filenameOpen;
			  } else if (file.getName().equals(norma43MayoristaFileName)) {
				  objects[1] = filenameMayorista;
			  } else {
				  LOG.debug("[buscarFicheros] El file no responde a ningun patron");
				  continue;
			  }
			  try {
				  br = new BufferedReader(new FileReader(file));
				  linea = br.readLine();
			  } catch (IOException e) {
				  throw new Norma43FileLoadException("El fichero " + file.getName() + " no empieza por una linea de tipo CUENTA", e);
			  } finally {
				  try {
					  br.close();
				  } catch (IOException e) {
					  throw new Norma43FileLoadException("No se puede cerrar el fichero " + file.getName(), e);
				  }
			  }
			  if (!linea.startsWith(CUENTA)) {
				  throw new Norma43FileLoadException("El fichero " + file.getName() + " no empieza por una linea de CUENTA");
			  }
			  try {
				  date = FILE_DATE_FORMAT.parse(linea.substring(20, 26));
			  } catch (ParseException e) {
				  throw new Norma43FileLoadException("La cadena " + linea.substring(20, 26) + " del fichero " + file.getName()  + " no es una fecha valida" , e);
			  }
			  objects[1] = FILE_DATE_FORMAT2.format(date) + objects[1];
			  listaFicherosEncontrados.add(objects);
			  objects[0] = file;
			  objects[2] = FilenameUtils.removeExtension(file.getName()) + DATE_FORMAT.format(new Date()) + norma43FileExtension;
		  }
	  } else {
		  LOG.error("[buscarFicheros] La ruta no es un directorio: {} ", sourcePath);
	  } // else
	  LOG.debug("[buscarFicheros] Fin ...");
	  return listaFicherosEncontrados;
  } // buscarFicheros

  public void put(TIPO_TAREA tipoApunte, TMCT0MSC estadoIni, TMCT0MSC estadoExe,
		  TMCT0MSC estadoError) throws Norma43FileLoadException {
	  Tmct0men tmct0men;
	  try {
		  LOG.debug("Se lanza la tarea de " + tipoApunte.getDescripcion());
		  tmct0men = menBo.putEstadoMEN(tipoApunte, estadoIni, estadoExe);
		  try {
			  procesarFicherosNorma43(sourcePath);
			  menBo.putEstadoMENAndIncrement(tmct0men, estadoIni);
			  LOG.debug("Terminada la tarea de " + tipoApunte.getDescripcion());
		  } catch (Exception e) {
			  menBo.putEstadoMENAndIncrement(tmct0men, estadoError);
			  throw e;
		  }
	  } catch (Exception e) {
		  LOG.error("Error en la tarea de " + tipoApunte.getDescripcion(), e);
	  }
  }
} // Norma43FicheroBo