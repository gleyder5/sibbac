package sibbac.business.conciliacion.database.dao;


import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtual;


@Repository
public interface MovimientoCuentaVirtualDao extends JpaRepository< MovimientoCuentaVirtual, Long > {

	/**
	 * 
	 * @return
	 */
	@Query( "Select m from MovimientoCuentaVirtual m group by m.isin, m.sentido, m.camara" )
	List< MovimientoCuentaVirtual > getMovimientosCuentaVirtual();

	/**
	 * suma todos los efectivos de un cliente dado su alias
	 * 
	 * @param alias
	 * @return
	 */
	@Query( "SELECT sum(m.efectivo) FROM MovimientoCuentaVirtual m WHERE m.alias =:alias" )
	BigDecimal sumarEfectivosCliente( @Param( "alias" ) String alias );

	/**
	 * Busca movimientos y los ordena por alias
	 * 
	 * @return
	 */
	@Query( "SELECT m, sum(m.titulos) as titulos, sum(m.efectivo) as efectivos  FROM MovimientoCuentaVirtual m order by m.alias asc" )
	List< Object[] > findMovimientosOrderByClient();

	/**
	 * 
	 * @return
	 */
	@Query( "SELECT m, sum(m.titulos) as titulos, sum(m.efectivo) as efectivos  FROM MovimientoCuentaVirtual m group by m.camara, m.idcuentaliq, m.alias, m.isin, m.sentido order by m.alias asc" )
			List< Object[] > findMovimientosGrupoByIsinAndIdClienteOrderByClient();

	@Query( "Select m from MovimientoCuentaVirtual m  where m.idcuentaliq=:idCuenta group by m.isin, m.sentido, m.camara" )
	List< MovimientoCuentaVirtual > getMovimientosCuentaVirtualByCuentaLiquidacion( @Param( "idCuenta" ) long idCuenta );

	/****
	 * Query que devuelve los movimientosCuentaVirtual cuyos valores coinciden con los pasados como prámetros o NULL
	 * 
	 * @param isin
	 * @param feContrata
	 * @param sentido
	 * @param cdCodigoCtaComp
	 * @param alias
	 * @param cdcamara
	 * @param sentidocuentavirtual
	 * @return
	 */
	@Query( "SELECT m FROM MovimientoCuentaVirtual m "
			+ " WHERE m.isin = :isin AND m.tradedate = :tradedate AND m.settlementdate = :settlementdate AND m.sentido = :sentido AND "
			+ " m.cdcodigoccomp = :cdcodigoccomp AND m.alias = :alias AND m.camara = :cdcamara AND "
			+ " m.sentidocuentavirtual = :sentidocuentavirtual " )
	MovimientoCuentaVirtual findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamara(
			@Param( value = "isin" ) String isin, @Param( value = "tradedate" ) java.sql.Date tradedate,
			@Param( value = "settlementdate" ) java.sql.Date settlementdate, @Param( value = "sentido" ) Character sentido, @Param(
					value = "cdcodigoccomp" ) String cdcodigoccomp, @Param( value = "alias" ) String alias,
			@Param( value = "cdcamara" ) String cdcamara, @Param( value = "sentidocuentavirtual" ) Character sentidocuentavirtual );

	/****
	 * Query que devuelve los movimientosCuentaVirtual cuyos valores coinciden con los pasados como prámetros o NULL
	 * 
	 * @param isin
	 * @param feContrata
	 * @param sentido
	 * @param cdCodigoCtaComp
	 * @param alias
	 * @param cdcamara
	 * @param sentidocuentavirtual
	 * @return
	 */
	@Query( "SELECT m FROM MovimientoCuentaVirtual m "
			+ " WHERE m.isin = :isin AND m.tradedate = :tradedate AND m.settlementdate = :settlementdate AND m.sentido = :sentido AND "
			+ " m.cdcodigoccomp = :cdcodigoccomp AND m.alias = :alias AND m.camara = :cdcamara AND "
			+ " m.sentidocuentavirtual = :sentidocuentavirtual " )
	List< MovimientoCuentaVirtual > findAllByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamara(
			@Param( value = "isin" ) String isin, @Param( value = "tradedate" ) java.sql.Date tradedate,
			@Param( value = "settlementdate" ) java.sql.Date settlementdate, @Param( value = "sentido" ) Character sentido, @Param(
					value = "cdcodigoccomp" ) String cdcodigoccomp, @Param( value = "alias" ) String alias,
			@Param( value = "cdcamara" ) String cdcamara, @Param( value = "sentidocuentavirtual" ) Character sentidocuentavirtual );

	@Query( "SELECT m FROM MovimientoCuentaVirtual m "
			+ " WHERE m.isin = :isin AND m.tradedate= :tradedate AND m.settlementdate = :settlementdate AND m.sentido = :sentido AND "
			+ " m.cdcodigocliq = :cdCodigoCtaLiq  AND m.camara = :cdcamara AND " + " m.sentidocuentavirtual = :sentidoCV " )
	MovimientoCuentaVirtual findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaLiqAndSentidoCVAndCamara(
			@Param( value = "isin" ) String isin, @Param( value = "tradedate" ) java.sql.Date tradeDate,
			@Param( value = "settlementdate" ) java.sql.Date settlementDate, @Param( value = "sentido" ) Character sentido, @Param(
					value = "cdCodigoCtaLiq" ) String cdCodigoCtaLiq, @Param( value = "sentidoCV" ) char sentidoCV, @Param(
					value = "cdcamara" ) String cdcamara );

	/****
	 * Query que devuelve los movimientosCuentaVirtual cuyos valores coinciden con los pasados como prámetros o NULL
	 * 
	 * @param isin
	 * @param feContrata
	 * @param sentido
	 * @param cdCodigoCtaComp
	 * @param alias
	 * @param cdcamara
	 * @param sentidocuentavirtual
	 * @param idLiquidacionParcial
	 * @return
	 */
	@Query( "SELECT m FROM MovimientoCuentaVirtual m "
			+ " WHERE m.isin = :isin AND m.tradedate = :tradedate AND m.settlementdate = :settlementdate AND m.sentido = :sentido AND "
			+ " m.cdcodigoccomp = :cdcodigoccomp AND m.alias = :alias AND m.camara = :cdcamara AND "
			+ " m.sentidocuentavirtual = :sentidocuentavirtual AND m.idliquidacionparcial = :idliquidacionparcial" )
	MovimientoCuentaVirtual findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamaraAndIdLiquidacionParcial( @Param(
			value = "isin" ) String isin, @Param( value = "tradedate" ) java.sql.Date tradedate,
			@Param( value = "settlementdate" ) java.sql.Date settlementdate, @Param( value = "sentido" ) Character sentido, @Param(
					value = "cdcodigoccomp" ) String cdcodigoccomp, @Param( value = "alias" ) String alias,
			@Param( value = "cdcamara" ) String cdcamara, @Param( value = "sentidocuentavirtual" ) Character sentidocuentavirtual, @Param(
					value = "idliquidacionparcial" ) Character idliquidacionparcial );
}
