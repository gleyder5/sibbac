package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


/**
 * The Class Balance.
 */
public class BalanceBeanDTO {

	/** The isin. */
	private final String		isin;

	/** The descripcionisin. */
	private final String		descripcionisin;

	/** The titulos. */
	private final BigDecimal	titulos;

	/** The cd codigo. */
	private final String		cdCodigo;

	/**
	 * Instantiates a new balance bean.
	 *
	 * @param isin
	 *            the isin
	 * @param descripcionisin
	 *            the descripcionisin
	 * @param titulos
	 *            the titulos
	 * @param cdCodigo
	 *            the cd codigo
	 */
	public BalanceBeanDTO( String isin, String descripcionisin, BigDecimal titulos, String cdCodigo ) {
		super();
		this.isin = isin;
		this.descripcionisin = descripcionisin;
		this.titulos = titulos;
		this.cdCodigo = cdCodigo;
	}

	/**
	 * Gets the isin.
	 *
	 * @return the isin
	 */
	public String getIsin() {
		return isin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BalanceBean [isin=" + isin + ", descripcionisin=" + descripcionisin + ", titulos=" + titulos + ", cdCodigo=" + cdCodigo
				+ "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( cdCodigo == null ) ? 0 : cdCodigo.hashCode() );
		result = prime * result + ( ( descripcionisin == null ) ? 0 : descripcionisin.hashCode() );
		result = prime * result + ( ( isin == null ) ? 0 : isin.hashCode() );
		result = prime * result + ( ( titulos == null ) ? 0 : titulos.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		BalanceBeanDTO other = ( BalanceBeanDTO ) obj;
		if ( cdCodigo == null ) {
			if ( other.cdCodigo != null )
				return false;
		} else if ( !cdCodigo.equals( other.cdCodigo ) )
			return false;
		if ( descripcionisin == null ) {
			if ( other.descripcionisin != null )
				return false;
		} else if ( !descripcionisin.equals( other.descripcionisin ) )
			return false;
		if ( isin == null ) {
			if ( other.isin != null )
				return false;
		} else if ( !isin.equals( other.isin ) )
			return false;
		if ( titulos == null ) {
			if ( other.titulos != null )
				return false;
		} else if ( !titulos.equals( other.titulos ) )
			return false;
		return true;
	}

	/**
	 * Gets the descripcionisin.
	 *
	 * @return the descripcionisin
	 */
	public String getDescripcionisin() {
		return descripcionisin;
	}

	/**
	 * Gets the titulos.
	 *
	 * @return the titulos
	 */
	public BigDecimal getTitulos() {
		return titulos;
	}

	/**
	 * Gets the cd codigo.
	 *
	 * @return the cd codigo
	 */
	public String getCdCodigo() {
		return cdCodigo;
	}

}
