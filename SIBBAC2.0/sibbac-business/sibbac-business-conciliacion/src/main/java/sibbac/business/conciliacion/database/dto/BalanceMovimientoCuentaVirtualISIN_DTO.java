package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;


public class BalanceMovimientoCuentaVirtualISIN_DTO {

	private String						isin;
	private String						descrisin;
	private List< GrupoBalanceISIN >	grupoBalanceISINList;
	private BigDecimal					totalEfectivos;

	public void addGrupoBalanceISIN( GrupoBalanceISIN grupo ) {
		if ( this.grupoBalanceISINList == null ) {
			grupoBalanceISINList = new LinkedList< GrupoBalanceISIN >();
		}
		this.grupoBalanceISINList.add( grupo );
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescrisin() {
		return descrisin;
	}

	public void setDescrisin( String descrisin ) {
		this.descrisin = descrisin;
	}

	public List< GrupoBalanceISIN > getGrupoBalanceISINList() {
		return grupoBalanceISINList;
	}

	public void setGrupoBalanceISINList( List< GrupoBalanceISIN > grupoBalanceISINList ) {
		this.grupoBalanceISINList = grupoBalanceISINList;
	}

	public BigDecimal getTotalEfectivos() {
		return totalEfectivos;
	}

	public void setTotalEfectivos( BigDecimal totalEfectivos ) {
		this.totalEfectivos = totalEfectivos;
	}

}
