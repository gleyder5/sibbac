package sibbac.business.conciliacion.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dto.CamaraCompensacionDTO;
import sibbac.business.wrappers.database.dto.ValoresDTO;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceConciliacion implements SIBBACServiceBean {

	// LOGGER
	private static final Logger			LOG				= LoggerFactory.getLogger( SIBBACServiceConciliacion.class );

	@Autowired
	private Tmct0ValoresBo				valoresBo;
	@Autowired
	private Tmct0CamaraCompensacionBo	camaraBo;

	// Consulta
	private static final String			CMD_GET_ISIN	= "getIsin";
	private static final String			CMD_GET_CAMARA	= "getCamara";

	// Resultados
	public static final String			RESULT_ISIN		= "result_isin";
	public static final String			RESULT_CAMARA	= "result_camara";

	public SIBBACServiceConciliacion() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.debug( "ConciliacionService:Processing a web request..." );
		WebResponse response = new WebResponse();
		String command = webRequest.getAction();
		List< Tmct0Valores > result = new ArrayList< Tmct0Valores >();
		LOG.debug( "ConciliacionService: Command.: [{}]", command );
		switch ( command ) {
			case CMD_GET_ISIN:
				Iterable< Tmct0Valores > itIsin = valoresBo.findAll();
				Iterator< Tmct0Valores > it = itIsin.iterator();
				while ( it.hasNext() ) {
					Tmct0Valores isin = it.next();
					result.add( isin );
				}
				List< ValoresDTO > dtoList = valoresBo.entitiesListToAnDTOList( result );
				Map< String, Object > resultado = new HashMap< String, Object >();
				resultado.put( RESULT_ISIN, dtoList );
				response.setResultados( resultado );
				break;

			case CMD_GET_CAMARA:
				List< Tmct0CamaraCompensacion > camaras = camaraBo.findAll();
				Map< String, Object > resultadoCamara = new HashMap< String, Object >();
				List< CamaraCompensacionDTO > dtoListcamara = camaraBo.entitiesListToAnDTOList( camaras );
				resultadoCamara.put( RESULT_CAMARA, dtoListcamara );
				response.setResultados( resultadoCamara );
				break;
		}

		return response;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_GET_ISIN );
		commands.add( CMD_GET_CAMARA );
		return commands;
	}

	@Override
	public List< String > getFields() {
		// TODO Auto-generated method stub
		return null;
	}

}
