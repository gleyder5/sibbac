package sibbac.business.conciliacion.tasks;


/**
 * Esta clase contiene la logica necesaria para insergar o modificar registros
 * de las tablas TMCT0_DIFERENCIA_CONCILIACION_EFECTIVO y/o
 * TMCT0_DIFERENCIA_CONCILIACION_TITULO usando datos extraidos de las tablas
 * TMCT0_ANOTACIONECC y TMCT0_SALDO_TITULO_CUENTA y TMCT0_SALDO_EFECTIVO_CUENTA
 * 
 * @author fjarquellada
 *
 */
// @SIBBACJob( name = Task.GROUP_CONCILIACION.JOB_GESTION_DIFERENCIA_CONCILIACION, group = Task.GROUP_CONCILIACION.NAME, interval = 3600 )
public class GestionDiferenciaConciliacionTask /* extends SIBBACTask */{
	/*
	 * @Autowired
	 * private Environment env;
	 * private static final int LIQUIDA_MERCADO = EstadosEnumerados.ASIGNACIONES.LIQUIDADA_TEORICA.getId(); // estadoAsig
	 * private static final int DEVENGADO_PDTE_COBRO = EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_COBRO.getId(); // estadoCont
	 * private static final Logger LOG = LoggerFactory
	 * .getLogger( GestionDiferenciaConciliacionTask.class );
	 * 
	 * @Autowired
	 * private Tmct0movimientoeccBo movimientoEccBO;
	 * 
	 * @Autowired
	 * private Tmct0SaldoTituloCuentaBO stcBO;
	 * 
	 * @Autowired
	 * private Tmct0SaldoEfectivoCuentaBO secBO;
	 * 
	 * @Autowired
	 * private Tmct0CuentasDeCompensacionBo ccBO;
	 * 
	 * @Autowired
	 * private Tmct0DiferenciaConciliacionEfectivoBO difConcEfecBO;
	 * 
	 * @Autowired
	 * private Tmct0DiferenciaConciliacionTituloBO difConcTituloBO;
	 */
	/**
	 * El proceso debe ejecutarse una vez al dia y consultar
	 */

	// @Override
	/*
	 * public void execute() {
	 * // Extrae lista de titulos agrupados por cuenta, isin, tradeDate
	 * List< Object[] > titulosS3 = stcBO.findAllGroupByIsinAndAndSentidoAndCuentaCompensacionAndConciliado( false );
	 * for ( Object[] obj : titulosS3 ) {
	 * Tmct0SaldoTituloCuenta stc = ( Tmct0SaldoTituloCuenta ) obj[ 0 ];
	 * String isin = stc.getIsin();
	 * Tmct0CuentasDeCompensacion cuenta = stc.getCuentaCompensacion();
	 * String sentido = stc.getSentido();
	 * BigDecimal saldo = ( BigDecimal ) obj[ 1 ];
	 * Object[] resultObj = movimientoEccBO.findByIsinAndIdEstadoAsigAndIdEstadoContAndCodCuenta( isin, sentido, LIQUIDA_MERCADO,
	 * DEVENGADO_PDTE_COBRO, cuenta.getCdCodigo() );
	 * Tmct0movimientoecc mov = ( Tmct0movimientoecc ) resultObj[ 0 ];
	 * BigDecimal tsv = BigDecimal.valueOf( ( float ) resultObj[ 1 ] );
	 * BigDecimal esv = BigDecimal.valueOf( ( float ) resultObj[ 2 ] );
	 * }
	 * // Extrae lista de efectivos agrupados por cuenta, isin, tradeDate
	 * List< Object[] > efectivoS3 = secBO.findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion();
	 * 
	 * //LOG.debug( "[GestionDiferenciaConciliacion::execute] [{}] time: [{}]", this.jobDataMap.get( "name" ), new Date() );
	 * }
	 */

}
