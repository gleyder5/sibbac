package sibbac.business.conciliacion.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.text.MessageFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

@Entity
@Table(name = DBConstants.CONCILIACION.FICHERO_CUENTA_ECC)
public class FicheroCuentaECC extends ATable<FicheroCuentaECC> implements
	Serializable {

    /**
     * Número de serie por defecto
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "ISIN", nullable = false, length = 12)
    private String isin;
    
    @ManyToOne
    @JoinColumn(name = "ID_CUENTA_LIQUIDACION", nullable = true, referencedColumnName= "ID")
    private Tmct0CuentaLiquidacion cuentaLiquidacion;

    @Column(name = "SETTLEMENTDATE", nullable = true)
    private Date settlementdate;

    @Column(name = "TITULOS", nullable = false, precision = 25, scale = 8)
    private BigDecimal titulos;

    @Column(name = "EFECTIVO", nullable = false, precision = 25, scale = 4)
    private BigDecimal efectivo;

    @Column(name = "DESCR_ISIN", length = 90)
    private String descrIsin;

    @Column(name = "MERCADO", nullable = false, length = 3)
    private String mercado;
    
    @Column(name = "SIGNO_ANOTACION", nullable = false, length = 1)
    private Character signoAnotacion;
    
    @Column(name = "FLAG_ERROR", nullable = false, length = 1)
    private Character flagError;

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public Tmct0CuentaLiquidacion getCuentaLiquidacion() {
        return cuentaLiquidacion;
    }

    public void setCuentaLiquidacion(Tmct0CuentaLiquidacion cuentaLiquidacion) {
        this.cuentaLiquidacion = cuentaLiquidacion;
    }

    public Date getSettlementdate() {
        return settlementdate;
    }

    public void setSettlementdate(java.util.Date date) {
        this.settlementdate = date;
    }

    public BigDecimal getTitulos() {
        return titulos;
    }

    public void setTitulos(BigDecimal titulos) {
        this.titulos = titulos;
    }

    public BigDecimal getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(BigDecimal efectivo) {
        this.efectivo = efectivo;
    }

    public String getDescrIsin() {
        return descrIsin;
    }

    public void setDescrIsin(String descrIsin) {
        this.descrIsin = descrIsin;
    }

    public String getMercado() {
        return mercado;
    }

    public void setMercado(String mercado) {
        this.mercado = mercado;
    }
    
    public Character getSignoAnotacion() {
        return signoAnotacion;
    }

    public void setSignoAnotacion(Character signoAnotacion) {
        this.signoAnotacion = signoAnotacion;
    }
    
    public Character getFlagError() {
        return flagError;
    }

    public void setFlagError(Character flagError) {
        this.flagError = flagError;
    }
    
    @Override
    public String toString() {
      return MessageFormat.format("{0}/{1}/{2}/{3}", isin,
          cuentaLiquidacion == null ? "null" : cuentaLiquidacion.getCdCodigo(),
          settlementdate, signoAnotacion);
    }
    
}
