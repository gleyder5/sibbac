package sibbac.business.conciliacion.database.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import sibbac.business.conciliacion.database.model.Norma43Fichero;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;

public class Norma43FicheroData implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  private Integer entidad;
  private Integer oficina;
  private Long cuenta;
  private String debe;
  private Long saldoinicial;
  private Integer divisa;
  private Long saldofinal;
  private String libre;
  private Integer modaInfo;
  private String nombreAbrev;
  private Date fechaInicial;
  private Date fechaFinal;
  private Integer apuntesDebe;
  private Integer importesDebe;
  private Integer apuntesHaber;
  private Integer importesHaber;
  private List<Norma43MovimientoData> movimientos;
  private String codigoCuenta;

  public Norma43FicheroData(Norma43Fichero fichero) {
    this.entidad = fichero.getEntidad();
    this.oficina = fichero.getOficina();
    this.cuenta = fichero.getCuenta();
    this.debe = fichero.getDebe();
    this.saldoinicial = fichero.getSaldoinicial();
    this.divisa = fichero.getDivisa();
    this.saldofinal = fichero.getSaldofinal();
    this.libre = fichero.getLibre();
    this.modaInfo = fichero.getModaInfo();
    this.nombreAbrev = fichero.getNombreAbrev();
    this.fechaInicial = fichero.getFechaInicial();
    this.fechaFinal = fichero.getFechaFinal();
    this.apuntesDebe = fichero.getApuntesDebe();
    this.importesDebe = fichero.getImportesDebe();
    this.apuntesHaber = fichero.getApuntesHaber();
    this.importesHaber = fichero.getImportesHaber();
    this.movimientos = entityListToDataList(fichero.getMovimientoNorma43());

    Comparator<Norma43MovimientoData> comparator = new Comparator<Norma43MovimientoData>() {

      public int compare(Norma43MovimientoData c1, Norma43MovimientoData c2) {
        // return c2.getImporte() - c1.getImporte(); // use your logic
        return c2.getImporte().compareTo(c1.getImporte());
      }
    };

    Collections.sort(this.movimientos, comparator);
  }

  public Norma43FicheroData(int entidad,
                            int oficina,
                            long cuenta,
                            String debe,
                            long saldoinicial,
                            int divisa,
                            long saldofinal,
                            String libre,
                            int modaInfo,
                            String nombreAbrev,
                            Date fechaInicial,
                            Date fechaFinal,
                            int apuntesDebe,
                            int importesDebe,
                            int apuntesHaber,
                            int importesHaber,
                            Collection<Norma43Movimiento> movimientos) {
    this.entidad = entidad;
    this.oficina = oficina;
    this.cuenta = cuenta;
    this.debe = debe;
    this.saldoinicial = saldoinicial;
    this.divisa = divisa;
    this.saldofinal = saldofinal;
    this.libre = libre;
    this.modaInfo = modaInfo;
    this.nombreAbrev = nombreAbrev;
    this.fechaInicial = fechaInicial;
    this.fechaFinal = fechaFinal;
    this.apuntesDebe = apuntesDebe;
    this.importesDebe = importesDebe;
    this.apuntesHaber = apuntesHaber;
    this.importesHaber = importesHaber;
    this.movimientos = entityListToDataList((List<Norma43Movimiento>) movimientos);
  }

  public List<Norma43MovimientoData> getMovimientos() {
    return movimientos;
  }

  public void setMovimientos(List<Norma43MovimientoData> movimientos) {
    this.movimientos = movimientos;
  }

  public Integer getEntidad() {
    return entidad;
  }

  public void setEntidad(Integer entidad) {
    this.entidad = entidad;
  }

  public Integer getOficina() {
    return oficina;
  }

  public void setOficina(Integer oficina) {
    this.oficina = oficina;
  }

  public Long getCuenta() {
    return cuenta;
  }

  public void setCuenta(Long cuenta) {
    this.cuenta = cuenta;
  }

  public String getDebe() {
    return debe;
  }

  public void setDebe(String debe) {
    this.debe = debe;
  }

  public Long getSaldoinicial() {
    return saldoinicial;
  }

  public void setSaldoinicial(Long saldoinicial) {
    this.saldoinicial = saldoinicial;
  }

  public Integer getDivisa() {
    return divisa;
  }

  public void setDivisa(Integer divisa) {
    this.divisa = divisa;
  }

  public Long getSaldofinal() {
    return saldofinal;
  }

  public void setSaldofinal(Long saldofinal) {
    this.saldofinal = saldofinal;
  }

  public String getLibre() {
    return libre;
  }

  public void setLibre(String libre) {
    this.libre = libre;
  }

  public Integer getModaInfo() {
    return modaInfo;
  }

  public void setModaInfo(Integer modaInfo) {
    this.modaInfo = modaInfo;
  }

  public String getNombreAbrev() {
    return nombreAbrev;
  }

  public void setNombreAbrev(String nombreAbrev) {
    this.nombreAbrev = nombreAbrev;
  }

  public Date getFechaInicial() {
    return fechaInicial;
  }

  public void setFechaInicial(Date fechaInicial) {
    this.fechaInicial = fechaInicial;
  }

  public Date getFechaFinal() {
    return fechaFinal;
  }

  public void setFechaFinal(Date fechaFinal) {
    this.fechaFinal = fechaFinal;
  }

  public Integer getApuntesDebe() {
    return apuntesDebe;
  }

  public void setApuntesDebe(Integer apuntesDebe) {
    this.apuntesDebe = apuntesDebe;
  }

  public Integer getImportesDebe() {
    return importesDebe;
  }

  public void setImportesDebe(Integer importesDebe) {
    this.importesDebe = importesDebe;
  }

  public Integer getApuntesHaber() {
    return apuntesHaber;
  }

  public void setApuntesHaber(Integer apuntesHaber) {
    this.apuntesHaber = apuntesHaber;
  }

  public Integer getImportesHaber() {
    return importesHaber;
  }

  public void setImportesHaber(Integer importesHaber) {
    this.importesHaber = importesHaber;
  }

  public String getCodigoCuenta() {
    return codigoCuenta;
  }

  public void setCodigoCuenta(String codigoCuenta) {
    this.codigoCuenta = codigoCuenta;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  private Norma43MovimientoData entityToData(Norma43Movimiento entity) {
    Norma43MovimientoData data = new Norma43MovimientoData();
    data.setAuditdate(entity.getAuditDate());
    data.setIdcuentaliq(data.getIdcuentaliq());
    data.setImporte(entity.getImporte());
    data.setSettlementdate(entity.getSettlementdate());
    data.setTradedate(entity.getTradedate());
    return data;
  }

  private List<Norma43MovimientoData> entityListToDataList(List<Norma43Movimiento> entities) {
    List<Norma43MovimientoData> dataList = new ArrayList<Norma43MovimientoData>();
    for (Norma43Movimiento entity : entities) {
      Norma43MovimientoData data = entityToData(entity);
      dataList.add(data);
    }
    return dataList;
  }

}
