package sibbac.business.conciliacion.rest.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import sibbac.business.conciliacion.database.filter.FicheroCuentaECCFilter;

public class FicheroCuentaECCFilterImpl implements FicheroCuentaECCFilter {
  
  private final Map<String, Serializable> filtros;
  
  private final boolean codigoCuentaLiquidacionEq;
  
  private final String codigoCuentaLiquidacion;
  
  public FicheroCuentaECCFilterImpl(Map<String, Serializable> filtros) {
    final String flcodigo;
    
    this.filtros = filtros;
    flcodigo = (String) filtros.get("cdcodigocuentaliq");
    Objects.requireNonNull(flcodigo, "El código de cuenta es requerido");
    codigoCuentaLiquidacionEq = !flcodigo.startsWith("#");
    codigoCuentaLiquidacion = codigoCuentaLiquidacionEq ? flcodigo : flcodigo.replaceFirst("#", "");
  }

  @Override
  public Date getFechaDesde() {
    return (Date) filtros.get("fliquidacionDe");
  }

  @Override
  public Date getFechaHasta() {
    return (Date) filtros.get("fliquidacionA");
  }

  @Override
  public String getCodigoCuentaLiquidacion() {
    return codigoCuentaLiquidacion;
  }

  @Override
  public String getIsin() {
    return (String) filtros.get("isin");
  }

  @Override
  public boolean isCodigoCuentaLiquidacionEq() {
    return codigoCuentaLiquidacionEq;
  }

  @Override
  public char getSignoAnotacion() {
    final String signo;
    
    signo = (String) filtros.get("signoAnotacion");
    if(signo != null && signo.trim().length() > 0) {
      return signo.charAt(0);
    }
    return 0;
  }

}
