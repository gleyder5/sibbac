package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class DiferenciaConciliacionEfectivoDTO {

	private long		id;
	private String		tradeDate;
	private String		fechaLiquidacion;
	private long		idCuentaCompensacion;
	private String		isin;
	private BigDecimal	ESV;					// SALDO CUENTA PROPIA ANOTACIONECC
	private BigDecimal	ES3;					// SALDO CUENTA TITULO
	private BigDecimal	diferencia;			// DIFERENCIA ES3 Y ESV
	private String		sentido;

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate( String tradeDate ) {
		this.tradeDate = tradeDate;
	}

	public String getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	public void setFechaLiquidacion( String fechaLiquidacion ) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getESV() {
		return ESV;
	}

	public void setESV( BigDecimal eSV ) {
		ESV = eSV;
	}

	public BigDecimal getES3() {
		return ES3;
	}

	public void setES3( BigDecimal eS3 ) {
		ES3 = eS3;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido( String sentido ) {
		this.sentido = sentido;
	}

	public long getIdCuentaCompensacion() {
		return idCuentaCompensacion;
	}

	public void setIdCuentaCompensacion( long idCuentaCompensacion ) {
		this.idCuentaCompensacion = idCuentaCompensacion;
	}

	public BigDecimal getDiferencia() {
		return diferencia;
	}

	public void setDiferencia( BigDecimal diferencia ) {
		this.diferencia = diferencia;
	}

}
