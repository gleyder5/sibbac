package sibbac.business.conciliacion.database.filter;

import java.util.List;

import sibbac.database.AbstractQueryFilter;
import sibbac.database.DynamicColumn;

public class FicheroCuentaECCQuery extends AbstractQueryFilter<FicheroCuentaECCFilter> {
  
  private static final String FIELD_SETTLEMENT = "m.settlementdate";
  
  private static final String FIELD_CUENTA_LIQUIDACION = "m.cuentaLiquidacion.cdCodigo";
  
  private static final String FIELD_SIGNO_ANOTACION = "m.signoAnotacion";
  
  private static final String FIELD_ISIN = "m.isin";
  
  private static final String QUERY_COUNT = "SELECT COUNT(m), m.settlementdate ";
  
  private static final String QUERY_SELECT = 
      "SELECT new sibbac.business.conciliacion.database.data.FicheroCuentaECCData("
      + "m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo, "
      + "SUM(m.titulos), SUM(m.efectivo)) ";
  
  private static final String QUERY_FROM = "FROM FicheroCuentaECC m, Tmct0Valores v ";
  
  private static final String QUERY_WHERE = "AND m.isin=v.codigoDeValor ";
  
  private static final String QUERY_GROUP_BY =
      "m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo ";

  public FicheroCuentaECCQuery(FicheroCuentaECCFilter filter) {
    super(filter, FIELD_SETTLEMENT);
  }

  @Override
  public boolean isCountNecessary() {
    if(filter.getFechaDesde().equals(filter.getFechaHasta())) {
      singleDate(filter.getFechaDesde());
      return false;
    }
    return true;
  }

  @Override
  protected void buildSelectQuery() {
    selectBuilder.append(QUERY_SELECT);
    fromBuilder.append(QUERY_FROM);
    whereBuilder.append(QUERY_WHERE);
    splitMainDateWithFounded();
    where();
    groupByBuilder.append(QUERY_GROUP_BY);
    orderByBuilder.append(QUERY_GROUP_BY);
  }

  @Override
  protected boolean buildCountQuery() {
    selectBuilder.append(QUERY_COUNT);
    fromBuilder.append(QUERY_FROM);
    whereBuilder.append(QUERY_WHERE);
    splitMainDate(filter.getFechaDesde(), filter.getFechaHasta());
    where();
    groupByBuilder.append(FIELD_SETTLEMENT);
    return true;
  }

  private void where() {
    equals(FIELD_CUENTA_LIQUIDACION, filter.getCodigoCuentaLiquidacion(), filter.isCodigoCuentaLiquidacionEq());
    equalsIfPresent(FIELD_ISIN, filter.getIsin());
    equalsIfPresent(FIELD_SIGNO_ANOTACION, filter.getSignoAnotacion());
  }

  @Override
  public boolean isIsoFormat() {
    return true;
  }

  @Override
  public String getSingleFilter() {
    return null;
  }

  @Override
  public List<DynamicColumn> getColumns() {
    return null;
  }

}
