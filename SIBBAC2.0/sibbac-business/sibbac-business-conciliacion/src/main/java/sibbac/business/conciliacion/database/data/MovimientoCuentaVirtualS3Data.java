package sibbac.business.conciliacion.database.data;


import java.math.BigDecimal;
import java.sql.Date;

import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtualS3;


public class MovimientoCuentaVirtualS3Data {

	private MovimientoCuentaVirtualS3	movimientoCuentaVirtualS3;
	private String						camara;
	private long						idcuentaliq;
	private String						alias;
	private String						descrali;
	private String						isin;
	private char						sentidocuentavirtual;
	private char						sentido;
	private String						descrIsin;
	private BigDecimal					titulos;
	private BigDecimal					efectivo;
	private Date						settlementdate;
	private java.util.Date				auditDate;

	//
	public MovimientoCuentaVirtualS3Data( MovimientoCuentaVirtualS3 movimientoCuentaVirtualS3, BigDecimal titulos, BigDecimal efectivo ) {
		this.movimientoCuentaVirtualS3 = movimientoCuentaVirtualS3;
		this.titulos = titulos;
		this.efectivo = efectivo;
	}

	public MovimientoCuentaVirtualS3Data( String camara, long idcuentaliq, String alias, String descrali, String isin, String descrIsin,
			char sentidocuentavirtual, char sentido, BigDecimal titulos, BigDecimal efectivo ) {
		this.camara = camara;
		this.idcuentaliq = idcuentaliq;
		this.alias = alias;
		this.isin = isin;
		this.descrIsin = descrIsin;
		this.sentidocuentavirtual = sentidocuentavirtual;
		this.sentido = sentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.descrali = descrali;
		initMovimientoCuentaVirtualS3();

	}

	public MovimientoCuentaVirtualS3Data( java.util.Date auditDate, long idcuentaliq, java.util.Date settlementdate, String isin,
			String descrIsin, BigDecimal titulos, BigDecimal efectivo ) {
		this.idcuentaliq = idcuentaliq;
		this.isin = isin;
		this.descrIsin = descrIsin;
		this.titulos = titulos;
		this.efectivo = efectivo;
		if ( settlementdate != null ) {
			this.settlementdate = new java.sql.Date( settlementdate.getTime() );
		} else {
			this.settlementdate = null;
		}
		if ( auditDate != null ) {
			this.auditDate = auditDate;
		} else {
			this.auditDate = new java.util.Date();
		}

		initMovimientoCuentaVirtualS3();

	}

	public MovimientoCuentaVirtualS3Data( String camara, long idcuentaliq, String alias, String descrali, String isin, String descrIsin,
			char sentidocuentavirtual, char sentido, java.util.Date settlementdate, BigDecimal titulos, BigDecimal efectivo ) {
		this.camara = camara;
		this.idcuentaliq = idcuentaliq;
		this.alias = alias;
		this.isin = isin;
		this.descrIsin = descrIsin;
		this.sentidocuentavirtual = sentidocuentavirtual;
		this.sentido = sentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.descrali = descrali;
		if ( settlementdate != null ) {
			this.settlementdate = new java.sql.Date( settlementdate.getTime() );
		} else {
			this.settlementdate = null;
		}
		initMovimientoCuentaVirtualS3();

	}

	private void initMovimientoCuentaVirtualS3() {
		this.movimientoCuentaVirtualS3 = new MovimientoCuentaVirtualS3();
		this.movimientoCuentaVirtualS3.setAuditDate( auditDate );
		this.movimientoCuentaVirtualS3.setIdcuentaliq( idcuentaliq );
		this.movimientoCuentaVirtualS3.setDescrIsin( descrIsin );
		this.movimientoCuentaVirtualS3.setIsin( isin );
		this.movimientoCuentaVirtualS3.setTitulos( titulos );
		this.movimientoCuentaVirtualS3.setEfectivo( efectivo );
		this.movimientoCuentaVirtualS3.setSettlementdate( settlementdate );
	}

	//

	public BigDecimal getTitulos() {
		return titulos;
	}

	public MovimientoCuentaVirtualS3 getMovimientoCuentaVirtual() {
		return movimientoCuentaVirtualS3;
	}

	public MovimientoCuentaVirtualS3 getMovimientoCuentaVirtualS3() {
		return movimientoCuentaVirtualS3;
	}

	public String getDescrIsin() {
		return descrIsin;
	}

	public java.util.Date getAuditDate() {
		return auditDate;
	}

	public void setMovimientoCuentaVirtualS3( MovimientoCuentaVirtualS3 movimientoCuentaVirtualS3 ) {
		this.movimientoCuentaVirtualS3 = movimientoCuentaVirtualS3;
	}

	public void setDescrIsin( String descrIsin ) {
		this.descrIsin = descrIsin;
	}

	public void setAuditDate( java.util.Date auditDate ) {
		this.auditDate = auditDate;
	}

	public void setMovimientoCuentaVirtual( MovimientoCuentaVirtualS3 movimientoCuentaVirtualS3 ) {
		this.movimientoCuentaVirtualS3 = movimientoCuentaVirtualS3;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public long getIdcuentaliq() {
		return idcuentaliq;
	}

	public void setIdcuentaliq( long idcuentaliq ) {
		this.idcuentaliq = idcuentaliq;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public char getSentidocuentavirtual() {
		return sentidocuentavirtual;
	}

	public void setSentidocuentavirtual( char sentidocuentavirtual ) {
		this.sentidocuentavirtual = sentidocuentavirtual;
	}

	public char getSentido() {
		return sentido;
	}

	public void setSentido( char sentido ) {
		this.sentido = sentido;
	}

	public String getDescripcionisin() {
		return descrIsin;
	}

	public void setDescripcionisin( String descrIsin ) {
		this.descrIsin = descrIsin;
	}

	public String getDescrali() {
		return descrali;
	}

	public void setDescrali( String descrali ) {
		this.descrali = descrali;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

}
