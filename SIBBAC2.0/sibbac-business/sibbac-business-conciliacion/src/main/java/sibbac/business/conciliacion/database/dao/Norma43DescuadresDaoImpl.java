package sibbac.business.conciliacion.database.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;

@Repository
public class Norma43DescuadresDaoImpl {

  @PersistenceContext
  private EntityManager em;

  public List<Norma43DescuadresDTO> findAllByCuentaAndBetweenTradeDate(Date fechaDesde,
                                                                       Date fechaHasta,
                                                                       String cuenta,
                                                                       String sentido,
                                                                       String refOrden,
                                                                       String importeDesde,
                                                                       String importeHasta) {

    String query = "select dn43 from Norma43Descuadres dn43 where "
                   + "dn43.tradeDate BETWEEN :fechaDesde AND :fechaHasta AND dn43.importeNorma43>=:importeDesde ";

    if (!"".equals(cuenta)) {
      query += "AND dn43.cuenta= :cuenta ";
    }

    if (!"".equals(importeHasta)) {
      query += "AND dn43.importeNorma43<=:importeHasta ";
    }

    if (!"Todos".equals(sentido)) {
      query += "AND dn43.sentido = :sentido ";
    }

    if (!"Todos".equals(refOrden)) {
      query += "AND dn43.refOrden = :refOrden ";
    }

    TypedQuery<Norma43Descuadres> typedQuery = em.createQuery(query, Norma43Descuadres.class);
    typedQuery.setParameter("fechaDesde", fechaDesde);
    typedQuery.setParameter("fechaHasta", fechaHasta);

    if (!"".equals(cuenta)) {
      typedQuery.setParameter("cuenta", cuenta);
    }

    typedQuery.setParameter("importeDesde", BigDecimal.valueOf(stringToDouble(importeDesde)));

    if (!"".equals(importeHasta)) {
      typedQuery.setParameter("importeHasta", BigDecimal.valueOf(stringToDouble(importeHasta)));
    }

    if (!"Todos".equals(sentido)) {
      typedQuery.setParameter("sentido", sentido);
    }

    if (!"Todos".equals(refOrden)) {
      typedQuery.setParameter("refOrden", refOrden);
    }

    List<Norma43Descuadres> norma43DescuadresList = typedQuery.getResultList();
    List<Norma43DescuadresDTO> norma43DescuadresDTOList = new ArrayList<Norma43DescuadresDTO>();
    Norma43DescuadresDTO norma43DescuadresDTO;

    for (Norma43Descuadres norma43Descuadres : norma43DescuadresList) {
      norma43DescuadresDTO = new Norma43DescuadresDTO();
      norma43DescuadresDTO.setCuenta(norma43Descuadres.getCuenta());
      norma43DescuadresDTO.setRefFecha(norma43Descuadres.getRefFecha());
      norma43DescuadresDTO.setRefOrden(norma43Descuadres.getRefOrden());
      norma43DescuadresDTO.setSentido(norma43Descuadres.getSentido());
      norma43DescuadresDTO.setTradeDate(norma43Descuadres.getTradeDate());
      norma43DescuadresDTO.setSettlementDate(norma43Descuadres.getSettlementDate());
      norma43DescuadresDTO.setImporteNorma43(norma43Descuadres.getImporteNorma43());
      norma43DescuadresDTO.setImporteSibbac(norma43Descuadres.getImporteSibbac());
      norma43DescuadresDTO.setTipoImporte(norma43Descuadres.getTipoImporte());
      norma43DescuadresDTO.setNombreEstadoAsignacionAlc(norma43Descuadres.getNombreEstadoContabilidadAlc());
      norma43DescuadresDTO.setNombreEstadoContabilidadAlc(norma43Descuadres.getNombreEstadoContabilidadAlc());
      norma43DescuadresDTO.setNombreEstadoEntrecAlc(norma43Descuadres.getNombreEstadoEntrecAlc());
      norma43DescuadresDTO.setNuorden(norma43Descuadres.getNuorden());
      norma43DescuadresDTO.setNbooking(norma43Descuadres.getNbooking());
      norma43DescuadresDTO.setNucnfclt(norma43Descuadres.getNucnfclt());
      norma43DescuadresDTO.setNucnfliq(norma43Descuadres.getNucnfliq());

      norma43DescuadresDTOList.add(norma43DescuadresDTO);
    }

    return norma43DescuadresDTOList;
  }

  private Double stringToDouble(String cantidad) {
    Double resultado = 0.0;
    String[] arrcantidad = cantidad.split("\\.");
    if (arrcantidad.length > 0) {
      String cantidadEntera = arrcantidad[0];
      String cantidadDecimal = "0";
      // si tiene parte decimal
      if (arrcantidad.length > 1) {
        cantidadDecimal = arrcantidad[1];
      }
      if (cantidadEntera != null) {
        if (!cantidadEntera.equals("")) {
          resultado = Double.parseDouble(cantidadEntera);
        }
      }
      if (cantidadDecimal != null) {
        if (cantidadDecimal.equals("")) {
          cantidadDecimal = "0";
        }
        int contDecimales_ot = cantidadDecimal.length();
        resultado += Double.parseDouble(cantidadDecimal) / Math.pow(10, contDecimales_ot);
      }
    }
    return resultado;
  }
}
