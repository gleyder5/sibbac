package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;
import java.util.Date;


public class ConciliacionDiariaDTO {

	private BigDecimal	nuTitulos;
	private String		isin;
	private String		tradedate;
	private String		settlementdate;
	private Character	sentido;
	private BigDecimal	precio;
	private BigDecimal	imefectivo;
	private String		camara;
	private String		bolsa;
	private String		nuordenmkt;
	private String		nuexemkt;
	private Date		feMkt;
	private String		cdoperacionecc;
	private String		estado;
	private String		descrAlias;
	private String		cdAliass;
	private String		descIsin;
	private String		cdCuenta;

	public ConciliacionDiariaDTO( BigDecimal nuTitulos, String isin, String tradedate, String settlementdate, Character sentido,
			BigDecimal precio, String camara, String bolsa, String nuordenmkt, String nuexemkt, Date feMkt ) {
		super();
		this.nuTitulos = nuTitulos;
		this.isin = isin;
		this.tradedate = tradedate;
		this.settlementdate = settlementdate;
		this.sentido = sentido;
		this.precio = precio;
		this.camara = camara;
		this.bolsa = bolsa;
		this.nuordenmkt = nuordenmkt;
		this.nuexemkt = nuexemkt;
		this.feMkt = feMkt;
		this.estado = "No casado";
	}

	public ConciliacionDiariaDTO( BigDecimal nuTitulos, String isin, String tradedate, String settlementdate, Character sentido,
			BigDecimal precio, String camara, String bolsa, String nuordenmkt, String nuexemkt, Date feMkt, String estado ) {
		super();
		this.nuTitulos = nuTitulos;
		this.isin = isin;
		this.tradedate = tradedate;
		this.settlementdate = settlementdate;
		this.sentido = sentido;
		this.precio = precio;
		this.camara = camara;
		this.bolsa = bolsa;
		this.nuordenmkt = nuordenmkt;
		this.nuexemkt = nuexemkt;
		this.feMkt = feMkt;
		this.estado = estado;
	}

	public ConciliacionDiariaDTO( ConciliacionDiariaDTO toCopy ) {
		super();
		this.nuTitulos = toCopy.nuTitulos;
		this.isin = toCopy.isin;
		this.tradedate = toCopy.tradedate;
		this.settlementdate = toCopy.settlementdate;
		this.sentido = toCopy.sentido;
		this.precio = toCopy.precio;
		this.camara = toCopy.camara;
		this.bolsa = toCopy.bolsa;
		this.nuordenmkt = toCopy.nuordenmkt;
		this.nuexemkt = toCopy.nuexemkt;
		this.feMkt = toCopy.feMkt;
		this.estado = toCopy.estado;
		this.cdoperacionecc = toCopy.cdoperacionecc;
		this.descIsin = toCopy.descIsin;
		this.cdCuenta = toCopy.cdCuenta;
	}

	public BigDecimal getNuTitulos() {
		return nuTitulos;
	}

	public void setNuTitulos( BigDecimal nuTitulos ) {
		this.nuTitulos = nuTitulos;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getTradedate() {
		return tradedate;
	}

	public void setTradedate( String tradedate ) {
		this.tradedate = tradedate;
	}

	public String getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( String settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public Character getSentido() {
		return sentido;
	}

	public void setSentido( Character sentido ) {
		this.sentido = sentido;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio( BigDecimal precio ) {
		this.precio = precio;
	}

	public BigDecimal getImefectivo() {
		return imefectivo;
	}

	public void setImefectivo( BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public String getBolsa() {
		return bolsa;
	}

	public void setBolsa( String bolsa ) {
		this.bolsa = bolsa;
	}

	public String getNuordenmkt() {
		return nuordenmkt;
	}

	public void setNuordenmkt( String nuordenmkt ) {
		this.nuordenmkt = nuordenmkt;
	}

	public String getNuexemkt() {
		return nuexemkt;
	}

	public void setNuexemkt( String nuexemkt ) {
		this.nuexemkt = nuexemkt;
	}

	public String getCdoperacionecc() {
		return cdoperacionecc;
	}

	public void setCdoperacionecc( String cdoperacionecc ) {
		this.cdoperacionecc = cdoperacionecc;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado( String estado ) {
		this.estado = estado;
	}

	public Date getFeMkt() {
		return feMkt;
	}

	public void setFeMkt( Date feMkt ) {
		this.feMkt = feMkt;
	}

	public String getDescrAlias() {
		return descrAlias;
	}

	public void setDescrAlias( String descrAlias ) {
		this.descrAlias = descrAlias;
	}

	public String getCdAliass() {
		return cdAliass;
	}

	public void setCdAliass( String cdAliass ) {
		this.cdAliass = cdAliass;
	}

	public String getDescIsin() {
		return descIsin;
	}

	public void setDescIsin( String descIsin ) {
		this.descIsin = descIsin;
	}

	public String getCdCuenta() {
		return cdCuenta;
	}

	public void setCdCuenta(String cdCuenta) {
		this.cdCuenta = cdCuenta;
	}

}
