package sibbac.business.conciliacion.database.data;


import java.math.BigDecimal;
import java.util.Date;


public class Norma43MovimientoData {

	private Date	tradedate;
	private Date	settlementdate;
	private Date	auditdate;
	private long	idcuentaliq;
	private BigDecimal		importe;

	//
	public Norma43MovimientoData( Date tradedate, Date settlementdate, Date auditdate, long idcuentaliq, BigDecimal importe ) {
		this.tradedate = tradedate;
		this.settlementdate = settlementdate;
		this.auditdate = auditdate;
		this.idcuentaliq = idcuentaliq;
		this.importe = importe;
	}

	public Norma43MovimientoData() {
		//
	}

	public Date getTradedate() {
		return tradedate;
	}

	public void setTradedate( Date tradedate ) {
		this.tradedate = tradedate;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public Date getAuditdate() {
		return auditdate;
	}

	public void setAuditdate( Date auditdate ) {
		this.auditdate = auditdate;
	}

	public long getIdcuentaliq() {
		return idcuentaliq;
	}

	public void setIdcuentaliq( long idcuentaliq ) {
		this.idcuentaliq = idcuentaliq;
	}

}
