package sibbac.business.conciliacion.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtualS3;
import sibbac.business.wrappers.database.dto.ConciliacionERData;

@Repository
public interface MovimientoCuentaVirtualS3Dao extends JpaRepository<MovimientoCuentaVirtualS3, Long> {

  @Query("Select m from MovimientoCuentaVirtual m group by m.isin, m.sentido, m.camara")
  List<MovimientoCuentaVirtualS3> getMovimientosCuentaVirtual();

  @Query("SELECT new sibbac.business.wrappers.database.dto.ConciliacionERData(m.settlementdate, m.isin, m.descrIsin, "
      + "m.mercado, m.cdcodigocliq, SUM(m.titulos), SUM(m.efectivo), 0) "
      + "FROM MovimientoCuentaVirtualS3 m "
      // parametros
      + "WHERE m.settlementdate = :fechadesde AND m.cdcodigocliq = :codigoCuentaLiquidacion "
      + "AND m.isin = :isin "
      + "GROUP BY m.settlementdate, m.isin, m.descrIsin, m.mercado, m.cdcodigocliq "
      + "ORDER BY m.settlementdate, m.isin, m.descrIsin, m.mercado, m.cdcodigocliq")
  public List<ConciliacionERData> getInfoConciliacionS3(@Param("fechadesde") Date fechadesde,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion,
      @Param("isin") String isin);

  @Query("SELECT new sibbac.business.wrappers.database.dto.ConciliacionERData(m.settlementdate, m.isin, m.descrIsin, "
      + "m.mercado, m.cdcodigocliq, SUM(m.titulos), SUM(m.efectivo), 0) "
      + "FROM MovimientoCuentaVirtualS3 m "
      // parametros
      + "WHERE m.settlementdate = :fechadesde AND m.cdcodigocliq = :codigoCuentaLiquidacion "
      + "GROUP BY m.settlementdate, m.isin, m.descrIsin, m.mercado, m.cdcodigocliq "
      + "ORDER BY m.settlementdate, m.isin, m.descrIsin, m.mercado, m.cdcodigocliq")
  public List<ConciliacionERData> getInfoConciliacionS3(@Param("fechadesde") Date fechadesde,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);
  
}