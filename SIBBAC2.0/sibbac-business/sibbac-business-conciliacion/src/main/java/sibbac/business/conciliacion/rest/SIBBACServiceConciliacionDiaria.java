package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dto.AnotacionECCDTO;
import sibbac.business.conciliacion.database.dto.ConciliacionDiariaDTO;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.conciliacion.util.OperacionEccKeyMap;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.data.Tmct0desgloseclititData;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0caseoperacionejeBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.data.Tmct0caseoperacionejeData;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceConciliacionDiaria implements SIBBACServiceBean {

    private static final Logger LOG = LoggerFactory
	    .getLogger(SIBBACServiceConciliacionDiaria.class);

    // Util
    private ConciliacionUtil util = ConciliacionUtil.getInstance();

    @Autowired
    private Tmct0operacioncdeccBo opBo;
    @Autowired
    private Tmct0desgloseclititBo dcBo;
    @Autowired
    private Tmct0caseoperacionejeBo coBo;
    @Autowired
    Tmct0aliBo aliBo;
    @Autowired
    Tmct0ordBo ordBo;
    @Autowired
    Tmct0ValoresBo valoresBo;
    @Autowired
    GestorPeriodicidades gestorPeriodicidades;
    @Autowired
    Tmct0anotacioneccBo anotacionEccBo;
    @Autowired
    Tmct0ejeBo ejeBo;

    // protected static String simpleName = "SIBBACServiceConciliacionDiaria";

    public static final String KEY_COMMAND = "command";

    // Consulta
    public static final String CMD_GET_OPERACIONES_DIARIA = "getOperacionesDiaria";
    public static final String CMD_GET_FECHA_INICIAL = "getFechaInicial";

    // Parametros

    public static final String PRM_ISIN = "isin";
    public static final String PRM_SENTIDO = "sentido";
    public static final String PRM_CAMARA = "camara";
    public static final String PRM_ESTADO = "estado";
    public static final String PRM_FCONTRATACIONDE = "fcontratacionDe";
    public static final String PRM_FCONTRATACIONA = "fcontratacionA";
    public static final String PRM_FLIQUIDACIONDE = "fliquidacionDe";
    public static final String PRM_FLIQUIDACIONA = "fliquidacionA";
    public static final String PRM_ID_CUENTA_LIQUIDACION = "idCuentaLiquidacion";
    private static final DateFormat format = new SimpleDateFormat("yyyyMMdd");

    public SIBBACServiceConciliacionDiaria() {
    }

    @Override
    public WebResponse process(WebRequest webRequest) {
	String prefix = "[SIBBACServiceConciliacionDiaria::process] ";
	LOG.debug(prefix + "Processing a web request...");
	WebResponse webResponse = new WebResponse();
	// extrae parametros del httpRequest
	// List<Map<String,String>> filtros = webRequest.getParams();
	// Map<String, String> params = resolveParams(filtros);
	Map<String, String> params = new HashMap<String, String>();
	if (webRequest.getFilters() != null)
	    params = webRequest.getFilters();
	HashMap<String, Serializable> filtro = new HashMap<String, Serializable>();
	String command = webRequest.getAction();
	LOG.debug(prefix + "+ Command.: [{}]", command);
	switch (command) {
	case CMD_GET_FECHA_INICIAL:
	    Date ahora = new Date();
	    java.sql.Date ini = gestorPeriodicidades
		    .getLaborableAnterior(new java.sql.Date(ahora.getTime()));
	    Map<String, Object> resultadoFecha = new HashMap<String, Object>();
	    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");

	    resultadoFecha.put("fcontratacionIni", formater.format(ini));
	    webResponse.setResultados(resultadoFecha);
	    break;
	case CMD_GET_OPERACIONES_DIARIA:

	    List<String> cdoperacionescc = new ArrayList<String>();
	    List<String> isins = new ArrayList<String>();

	    if (params != null && params.size() > 0) {

		try {
		    resolveParams(params, filtro);
		} catch (ParseException e) {
		    LOG.error(e.getMessage(), e);
		    webResponse.setError(e.getMessage());
		    return webResponse;
		}

	    }
	    LOG.debug("Se van a obtener las operaciones de la tabla Tmct0operacionesecc ....");
	    List<Tmct0operacioncdecc> resultadosOp = null;
	    try {
		resultadosOp = opBo
			.findOperacionesForDiaria(
				(Date) filtro.get("fcontratacionDe"),
				(Date) filtro.get("fcontratacionA"),
				(Date) filtro.get("fliquidacionDe"),
				(Date) filtro.get("fliquidacionA"),
				(String) filtro.get("cdcamara"),
				(String) filtro.get("cdisin"),(String)filtro.get(PRM_ID_CUENTA_LIQUIDACION));
		if(resultadosOp != null){
		    LOG.debug("Se han obtenido "+resultadosOp.size());
		}
	    } catch (Exception ex) {
		LOG.error(ex.getMessage(), ex);
		webResponse.setError(ex.getMessage());
		return webResponse;
	    }
	    LOG.debug("Pasando a DTO los resultados ....");
	    List<AnotacionECCDTO> resultados = util.opEntitiesToDTOList(resultadosOp);
	    LOG.debug("Se van a restar los dClitit ....");
	    // Resto los dCliTit
	    List<ConciliacionDiariaDTO> conciliacionAnotaciones00D = anotacionesECCDTOToConciliacionDiaria(resultados);
	    LOG.debug("Se van a consultar los desgloseclitit ....");
	    // Traigo los desglosesclitit
	    List<Tmct0desgloseclititData> operacionesdc = dcBo
		    .findForDiaria(filtro);
	    LOG.debug("Se obtuvieron "+ (operacionesdc != null ?operacionesdc.size() : 0) + " desgloses.");
	    Map<String, String> listaDescrIsines = new HashMap<String, String>();
	    Map<String, String> listaDescrAlias = new HashMap<String, String>();
	    LOG.debug("Entra en la función restarCliTitConKeyMap ....");
	    List<ConciliacionDiariaDTO> aMostrar = restarCliTitConKeyMap(
		    operacionesdc, conciliacionAnotaciones00D, cdoperacionescc,
		    isins, listaDescrAlias);
	    LOG.debug("Se va a agrupar lo que se va a mostrar por pantalla.....");
	    // Agrupo lo que se va a mostrar
	    aMostrar = agruparConciliacionDiariaDTO(aMostrar);
	    List<Object> resultadosFinal = obtenerDatosAMostrar(filtro,
		    listaDescrIsines, aMostrar);

	    List<ConciliacionDiariaDTO> casadosYNoCasados = new ArrayList<ConciliacionDiariaDTO>();
	    try {
		if (cdoperacionescc != null && isins != null
			&& cdoperacionescc.size() > 0 && isins.size() > 0) {
		    LOG.debug("Entra en el método: resultadoConEstadoKeyMap ....");
		    resultadoConEstadoKeyMap(conciliacionAnotaciones00D,
			    casadosYNoCasados, cdoperacionescc, isins);
		}
		LOG.debug("Va a agrupar los resultados .....");
		casadosYNoCasados = agruparConciliacionDiariaDTO(casadosYNoCasados);
	    } catch (Exception ex) {
		LOG.error(ex.getMessage(), ex);
		webResponse.setError(ex.getMessage());
		return webResponse;
	    }
	    
	    try {
		LOG.debug("Entra en el método: tratarCasadosYNoCasados");
		tratarCasadosYNoCasados(filtro, listaDescrIsines,
			resultadosFinal, casadosYNoCasados);
	    } catch (Exception ex) {
		LOG.error(ex.getMessage(), ex);
		webResponse.setError(ex.getMessage());
		return webResponse;
	    }

	    Map<String, Object> response = new HashMap<String, Object>();

	    response.put("resultados_diaria", resultadosFinal);
	    webResponse.setResultados(response);
	    break;
	}

	return webResponse;
    }

    private void tratarCasadosYNoCasados(HashMap<String, Serializable> filtro,
	    Map<String, String> listaDescrIsines, List<Object> resultadosFinal,
	    List<ConciliacionDiariaDTO> casadosYNoCasados) {
	for (ConciliacionDiariaDTO dclitit : casadosYNoCasados) {
	    Map<String, String> resultadoParcial = new HashMap<String, String>();
	    if (!filtro.containsKey("estado")
		    || (filtro.containsKey("estado") && ((String) filtro
			    .get("estado")).trim().equals(
			    dclitit.getEstado().trim()))) {
		resultadoParcial.put("idanotacionecc",
			dclitit.getCamara());
		resultadoParcial.put("nuTitulos", String
			.valueOf(dclitit.getNuTitulos().intValue()));
		resultadoParcial.put("precio", dclitit.getPrecio()
			.toString());
		resultadoParcial.put("isin", dclitit.getIsin());
		resultadoParcial.put(
			"descIsin",
			findDescrisin(listaDescrIsines,
				dclitit.getIsin()));
		resultadoParcial.put("tradedate",
			dclitit.getTradedate());
		resultadoParcial.put("settlementdate",
			dclitit.getSettlementdate());
		resultadoParcial.put("alias", dclitit.getDescrAlias());
		resultadoParcial.put("idAlias", dclitit.getCdAliass());
		resultadoParcial.put("camara", dclitit.getCamara());
		resultadoParcial.put("sentido", dclitit.getSentido()
			+ "");
		resultadoParcial.put("estado", dclitit.getEstado());
		resultadoParcial.put("cdCuenta", dclitit.getCdCuenta());

		resultadosFinal.add(resultadoParcial);
	    }
	}
    }

	private List<Object> obtenerDatosAMostrar(HashMap<String, Serializable> filtro,
			Map<String, String> listaDescrIsines, List<ConciliacionDiariaDTO> aMostrar) {
		List<Object> resultadosFinal = new ArrayList<Object>();
		for (ConciliacionDiariaDTO dclitit : aMostrar) {
			if (!filtro.containsKey("estado") || (filtro.containsKey("estado")
					&& ((String) filtro.get("estado")).trim().equals(dclitit.getEstado().trim()))) {
				Map<String, String> resultadoParcial = new HashMap<String, String>();
				resultadoParcial.put("idanotacionecc", dclitit.getCamara());
				resultadoParcial.put("nuTitulos", String.valueOf(dclitit.getNuTitulos().intValue()));
				resultadoParcial.put("precio", dclitit.getPrecio().toString());
				resultadoParcial.put("isin", dclitit.getIsin());
				resultadoParcial.put("alias", dclitit.getDescrAlias());
				resultadoParcial.put("idAlias", dclitit.getCdAliass());
				resultadoParcial.put("descIsin", findDescrisin(listaDescrIsines, dclitit.getIsin()));
				resultadoParcial.put("tradedate", dclitit.getTradedate());
				resultadoParcial.put("settlementdate", dclitit.getSettlementdate());
				resultadoParcial.put("camara", dclitit.getCamara());
				resultadoParcial.put("sentido", dclitit.getSentido() + "");
				resultadoParcial.put("estado", dclitit.getEstado());
				resultadoParcial.put("cdCuenta", dclitit.getCdCuenta());
				resultadosFinal.add(resultadoParcial);
			}
		}
		return resultadosFinal;
	}

    private void resolveParams(Map<String, String> params,
	    HashMap<String, Serializable> filtro) throws ParseException {
	String fcontratacionDe;
	String fcontratacionA;
	String fliquidacionDe;
	String fliquidacionA;
	String isin = null;
	String camara = null;
	Character sentido = null;
	String estado = null;

	if (params.get(PRM_ISIN) != null) {
	    isin = params.get(PRM_ISIN);
	    if (!isin.equals(""))
		filtro.put("cdisin", isin);
	}

	if (params.get(PRM_CAMARA) != null) {
	    camara = params.get(PRM_CAMARA);
	    if (!camara.equals(""))
		filtro.put("cdcamara", camara);
	}

	if (params.get(PRM_SENTIDO) != null) {
	    if (!params.get(PRM_SENTIDO).equals("")) {
		sentido = params.get(PRM_SENTIDO).charAt(0);
		if (!sentido.equals(""))
		    filtro.put("cdsentido", sentido);
	    }
	}

	if (params.get(PRM_ESTADO) != null) {
	    if (!params.get(PRM_ESTADO).equals("")) {
		estado = params.get(PRM_ESTADO);

		if (!estado.equals(""))
		    filtro.put("estado", estado);
	    }
	}

	if (params.get(PRM_FCONTRATACIONDE) != null) {
	    fcontratacionDe = params.get(PRM_FCONTRATACIONDE);
	    if (!fcontratacionDe.equals(""))
		filtro.put("fcontratacionDe", format.parse(fcontratacionDe));
	}

	if (params.get(PRM_FCONTRATACIONA) != null) {
	    fcontratacionA = params.get(PRM_FCONTRATACIONA);
	    if (!fcontratacionA.equals("")) {
		filtro.put("fcontratacionA", format.parse(fcontratacionA));
	    } else if (filtro.containsKey("fcontratacionDe")) {
		// Si viene solamente el DE pues ponemos el A como el futuro
		Date fechaFinal = (params.get(PRM_FCONTRATACIONA) != null && !params
			.get(PRM_FCONTRATACIONA).equals("")) ? (Date) format
			.parse(params.get(PRM_FCONTRATACIONA))
			: obtenerFuturo();
		filtro.put("fcontratacionA", fechaFinal);
	    }
	}

	if (params.get(PRM_FLIQUIDACIONDE) != null) {
	    fliquidacionDe = params.get(PRM_FLIQUIDACIONDE);
	    if (!fliquidacionDe.equals(""))
		filtro.put("fliquidacionDe", format.parse(fliquidacionDe));
	}

	if (params.get(PRM_FLIQUIDACIONA) != null) {
	    fliquidacionA = params.get(PRM_FLIQUIDACIONA);
	    if (!fliquidacionA.equals("")) {
		filtro.put("fliquidacionA", format.parse(fliquidacionA));
	    } else if (filtro.containsKey("fliquidacionDe")) {
		// Si viene solamente el DE pues ponemos el A como el futuro
		Date fechaFinal = (params.get(PRM_FLIQUIDACIONA) != null && !params
			.get(PRM_FLIQUIDACIONA).equals("")) ? (Date) format
			.parse(params.get(PRM_FLIQUIDACIONA)) : obtenerFuturo();
		filtro.put("fliquidacionA", fechaFinal);
	    }
	}
	
	if (params.get(PRM_ID_CUENTA_LIQUIDACION) != null) {
	    String idCuentaLiquidacion = params.get(PRM_ID_CUENTA_LIQUIDACION);
	    if (!idCuentaLiquidacion.equals("")) {
	    	filtro.put(PRM_ID_CUENTA_LIQUIDACION, idCuentaLiquidacion);
	    } 
	}
    }

    @SuppressWarnings("unused")
    private void restarAnotaciones(List<AnotacionECCDTO> dtoAnotaciones00D,
	    List<AnotacionECCDTO> dtoAnotacionesOtras) {
	for (AnotacionECCDTO anotacion00D : dtoAnotaciones00D) {
	    for (AnotacionECCDTO anotacionOtra : dtoAnotacionesOtras) {
		if (anotacion00D.getImprecio().compareTo(
			anotacionOtra.getImprecio()) == 0
			&& anotacion00D.getCdsentido() == anotacionOtra
				.getCdsentido()
			&& anotacion00D
				.getCdoperacionecc()
				.trim()
				.equals(anotacionOtra.getCdoperacionecc()
					.trim())
			&& anotacionOtra.getCdisin().trim()
				.equals(anotacion00D.getCdisin().trim())) {
		    anotacion00D.setNutitulos(anotacion00D.getNutitulos()
			    .subtract(anotacionOtra.getNutitulos()));
		}
	    }
	}
    }

    // Puede que sirva o puede que no porque puede que haya repetidas...
    private void restarAnotacionesKeyMap(
	    Map<OperacionEccKeyMap, ConciliacionDiariaDTO> dtoAnotaciones00D,
	    Map<OperacionEccKeyMap, ConciliacionDiariaDTO> dtoAnotacionesOtras) {
	for (OperacionEccKeyMap key : dtoAnotaciones00D.keySet()) {
	    ConciliacionDiariaDTO anotacion00D = dtoAnotaciones00D.get(key);
	    ConciliacionDiariaDTO anotacionOtra = dtoAnotacionesOtras.get(key);
	    if (anotacion00D != null && anotacionOtra != null) {
		anotacion00D.setNuTitulos(anotacion00D.getNuTitulos().subtract(
			anotacionOtra.getNuTitulos()));
	    }
	}
    }

    private void cruzarExternas(List<AnotacionECCDTO> dtoAnotaciones00D,
	    List<AnotacionECCDTO> dtoAnotacionesOtras) {
	List<AnotacionECCDTO> toRemove = new ArrayList<AnotacionECCDTO>();
	// Buscamos en las <>00D las que tienen misma operacion inicial que las
	// 00D.
	for (AnotacionECCDTO anotacion00D : dtoAnotaciones00D) {
	    AnotacionECCDTO anotacionMenosTitulos = null;
	    for (AnotacionECCDTO anotacionOtra : dtoAnotacionesOtras) {
		// Tenemos que buscar aquella que tenga el numero de titulos
		// disponibles mas bajo
		if (anotacion00D.getNuexemkt().trim()
			.equals(anotacionOtra.getNuexemkt().trim())
			&& anotacion00D.getNuordenmkt().trim()
				.equals(anotacionOtra.getNuordenmkt().trim())
			&& anotacion00D.getCdsentido() != anotacionOtra
				.getCdsentido()
			&& anotacion00D
				.getNuoperacioninic()
				.trim()
				.equals(anotacionOtra.getNuoperacioninic()
					.trim())
			&& anotacionOtra.getCdisin().trim()
				.equals(anotacion00D.getCdisin().trim())) {
		    if (anotacionMenosTitulos == null) {
			anotacionMenosTitulos = anotacionOtra;
		    } else {
			if (anotacionMenosTitulos
				.getImtitulosdisponibles()
				.compareTo(
					anotacionOtra.getImtitulosdisponibles()) > 0) {
			    anotacionMenosTitulos = anotacionOtra;
			}
		    }
		}
	    }
	    if (anotacionMenosTitulos != null) {
		anotacion00D.setNutitulos(anotacionMenosTitulos
			.getImtitulosdisponibles());
		toRemove.add(anotacionMenosTitulos);
	    }
	}
	dtoAnotaciones00D.removeAll(toRemove);
    }

    private List<ConciliacionDiariaDTO> cruzarConClitit(
	    List<ConciliacionDiariaDTO> dtoAnotaciones00D,
	    List<Tmct0desgloseclititData> cliTits,
	    Map<String, String> listaDescrAlias) {
	Map<OperacionEccKeyMap, Tmct0desgloseclititData> mapaClitits = getkeyMapDesgloseClitit(cliTits);
	Map<String, String> listaNuordenAlias = new HashMap<String, String>();
	List<ConciliacionDiariaDTO> listaAMostrar = new ArrayList<ConciliacionDiariaDTO>();

	for (Iterator<Tmct0desgloseclititData> iterator = cliTits.iterator(); iterator
		.hasNext();) {
	    Tmct0desgloseclititData dclitit = iterator.next();
	    if (dclitit.getIdestado() >= 65 || dclitit.getIdestado() == 0) {
		// Borramos los que tienen estado >= 65 o cero
		iterator.remove();
	    } else {
		// Buscamos la conciliacion diaria que corresponde para coger
		// sus datos
		for (ConciliacionDiariaDTO resultado : dtoAnotaciones00D) {
		    if (dclitit.getImprecio().compareTo(resultado.getPrecio()) == 0
			    && resultado.getSentido() == dclitit.getCdsentido()
			    && resultado.getCdoperacionecc().trim()
				    .equals(dclitit.getCdoperacion().trim())
			    && dclitit.getCdisin().trim()
				    .equals(resultado.getIsin().trim())) {
			ConciliacionDiariaDTO aMostrar = new ConciliacionDiariaDTO(
				resultado);
			aMostrar.setNuTitulos(dclitit.getImtitulos());
			// aMostrar.setImefectivo(dclitit.getImefectivo());
			aMostrar.setSentido(dclitit.getCdsentido());
			aMostrar.setEstado(dclitit.getNombreestado());
			aMostrar.setPrecio(dclitit.getImprecio());

			if (!listaNuordenAlias
				.containsKey(dclitit.getNuorden())) {
			    Tmct0ali ali = getAliasFromNuorden(dclitit
				    .getNuorden());
			    if (ali != null) {
				aMostrar.setDescrAlias(ali.getDescrali());
				aMostrar.setCdAliass(ali.getCdaliass());
				listaNuordenAlias.put(dclitit.getNuorden(),
					ali.getCdaliass());
				listaDescrAlias.put(ali.getCdaliass(),
					ali.getDescrali());
			    } else {
				aMostrar.setDescrAlias("-");
				aMostrar.setCdAliass("-");
			    }
			} else {
			    String alias = listaNuordenAlias.get(dclitit
				    .getNuorden());
			    aMostrar.setDescrAlias(listaDescrAlias.get(alias));
			    aMostrar.setCdAliass(alias);
			}
			listaAMostrar.add(aMostrar);
		    }
		}
	    }
	}

	for (Iterator<ConciliacionDiariaDTO> iteratorDel = dtoAnotaciones00D
		.iterator(); iteratorDel.hasNext();) {
	    ConciliacionDiariaDTO res = iteratorDel.next();
	    if (res.getNuTitulos().intValue() == 0) {
		// Borramos los que se han quedado a cero
		iteratorDel.remove();
	    }
	}
	return listaAMostrar;
    }

    private List<ConciliacionDiariaDTO> restarCliTit(
	    List<Tmct0desgloseclititData> desglosesclitit,
	    List<ConciliacionDiariaDTO> resultados,
	    List<String> cdoperacionescc, List<String> isins,
	    Map<String, String> listaDescrAlias) {
	List<ConciliacionDiariaDTO> listaAMostrar = new ArrayList<ConciliacionDiariaDTO>();
	Map<String, String> listaNuordenAlias = new HashMap<String, String>();
	try {
	    for (ConciliacionDiariaDTO resultado : resultados) {
		if (!cdoperacionescc.contains(resultado.getCdoperacionecc())) {
		    cdoperacionescc.add(resultado.getCdoperacionecc());
		}
		if (!isins.contains(resultado.getIsin())) {
		    isins.add(resultado.getIsin());
		}
		for (Tmct0desgloseclititData dclitit : desglosesclitit) {
		    if (dclitit.getImprecio().compareTo(resultado.getPrecio()) == 0
			    && resultado.getSentido() == dclitit.getCdsentido()
			    && resultado.getCdoperacionecc().trim()
				    .equals(dclitit.getCdoperacion().trim())
			    && dclitit.getCdisin().trim()
				    .equals(resultado.getIsin().trim())
			    && dclitit.getIdestado() < 65
			    && dclitit.getIdestado() != 0) {
			// Si son del mismo sentido se restan y se deja el
			// sentido
			// que tiene
			resultado.setNuTitulos(resultado.getNuTitulos()
				.subtract(dclitit.getImtitulos()));
			// resultado.setImefectivo(resultado.getImefectivo().subtract(dclitit.getImefectivo()));
		    }
		}
	    }
	} catch (Exception e) {
	    e.getCause();
	}

	for (Iterator<Tmct0desgloseclititData> iterator = desglosesclitit
		.iterator(); iterator.hasNext();) {
	    Tmct0desgloseclititData dclitit = iterator.next();
	    if (dclitit.getIdestado() >= 65 || dclitit.getIdestado() == 0) {
		// Borramos los que tienen estado >= 65 o cero
		iterator.remove();
	    } else {
		// Buscamos la conciliacion diaria que corresponde para coger
		// sus datos
		for (ConciliacionDiariaDTO resultado : resultados) {
		    if (dclitit.getImprecio().compareTo(resultado.getPrecio()) == 0
			    && resultado.getSentido() == dclitit.getCdsentido()
			    && resultado.getCdoperacionecc().trim()
				    .equals(dclitit.getCdoperacion().trim())
			    && dclitit.getCdisin().trim()
				    .equals(resultado.getIsin().trim())) {
			ConciliacionDiariaDTO aMostrar = new ConciliacionDiariaDTO(
				resultado);
			aMostrar.setNuTitulos(dclitit.getImtitulos());
			// aMostrar.setImefectivo(dclitit.getImefectivo());
			aMostrar.setSentido(dclitit.getCdsentido());
			aMostrar.setEstado(dclitit.getNombreestado());
			aMostrar.setPrecio(dclitit.getImprecio());

			if (!listaNuordenAlias
				.containsKey(dclitit.getNuorden())) {
			    Tmct0ali ali = getAliasFromNuorden(dclitit
				    .getNuorden());
			    if (ali != null) {
				aMostrar.setDescrAlias(ali.getDescrali());
				aMostrar.setCdAliass(ali.getCdaliass());
				listaNuordenAlias.put(dclitit.getNuorden(),
					ali.getCdaliass());
				listaDescrAlias.put(ali.getCdaliass(),
					ali.getDescrali());
			    } else {
				aMostrar.setDescrAlias("-");
				aMostrar.setCdAliass("-");
			    }
			} else {
			    String alias = listaNuordenAlias.get(dclitit
				    .getNuorden());
			    aMostrar.setDescrAlias(listaDescrAlias.get(alias));
			    aMostrar.setCdAliass(alias);
			}
			listaAMostrar.add(aMostrar);
		    }
		}
	    }
	}

	for (Iterator<ConciliacionDiariaDTO> iteratorDel = resultados
		.iterator(); iteratorDel.hasNext();) {
	    ConciliacionDiariaDTO res = iteratorDel.next();
	    if (res.getNuTitulos().intValue() == 0) {
		// Borramos los que se han quedado a cero
		iteratorDel.remove();
	    }
	}
	return listaAMostrar;
    }

    private List<ConciliacionDiariaDTO> restarCliTitConKeyMap(
	    List<Tmct0desgloseclititData> desglosesclitit,
	    List<ConciliacionDiariaDTO> resultados,
	    List<String> cdoperacionescc, List<String> isins,
	    Map<String, String> listaDescrAlias) {
	Map<OperacionEccKeyMap, ConciliacionDiariaDTO> keyMapConciliacion = this
		.getkeyMapConciliacionDiariaDTO(resultados);
	Map<OperacionEccKeyMap, Tmct0desgloseclititData> keyMapDesglose = this
		.getkeyMapDesgloseClitit(desglosesclitit);
	List<ConciliacionDiariaDTO> listaAMostrar = new ArrayList<ConciliacionDiariaDTO>();
	Map<String, String> listaNuordenAlias = new HashMap<String, String>();

	for (OperacionEccKeyMap key : keyMapConciliacion.keySet()) {
	    ConciliacionDiariaDTO resultado = keyMapConciliacion.get(key);
	    if (!cdoperacionescc.contains(resultado.getCdoperacionecc())) {
		cdoperacionescc.add(resultado.getCdoperacionecc());
	    }
	    if (!isins.contains(resultado.getIsin())) {
		isins.add(resultado.getIsin());
	    }

	    Tmct0desgloseclititData dclitit = keyMapDesglose.get(key);
	    if (dclitit != null) {
		if (dclitit.getIdestado() < 65 && dclitit.getIdestado() != 0) {
		    resultado.setNuTitulos(resultado.getNuTitulos().subtract(
			    dclitit.getImtitulos()));

		    ConciliacionDiariaDTO aMostrar = new ConciliacionDiariaDTO(
			    resultado);
		    aMostrar.setNuTitulos(dclitit.getImtitulos());
		    // aMostrar.setImefectivo(dclitit.getImefectivo());
		    aMostrar.setSentido(dclitit.getCdsentido());
		    aMostrar.setEstado(dclitit.getNombreestado());
		    aMostrar.setPrecio(dclitit.getImprecio());

		    if (!listaNuordenAlias.containsKey(dclitit.getNuorden())) {
			Tmct0ali ali = getAliasFromNuorden(dclitit.getNuorden());
			if (ali != null) {
			    aMostrar.setDescrAlias(ali.getDescrali());
			    aMostrar.setCdAliass(ali.getCdaliass());
			    listaNuordenAlias.put(dclitit.getNuorden(),
				    ali.getCdaliass());
			    listaDescrAlias.put(ali.getCdaliass(),
				    ali.getDescrali());
			} else {
			    aMostrar.setDescrAlias("-");
			    aMostrar.setCdAliass("-");
			}
		    } else {
			String alias = listaNuordenAlias.get(dclitit
				.getNuorden());
			aMostrar.setDescrAlias(listaDescrAlias.get(alias));
			aMostrar.setCdAliass(alias);
		    }
		    listaAMostrar.add(aMostrar);

		}
	    }
	}

	resultados = new ArrayList<ConciliacionDiariaDTO>();

	for (OperacionEccKeyMap key : keyMapConciliacion.keySet()) {
	    ConciliacionDiariaDTO res = keyMapConciliacion.get(key);
	    if (res.getNuTitulos().compareTo(BigDecimal.ZERO) != 0) {
		resultados.add(res);
	    }
	}

	return listaAMostrar;
    }

    private List<ConciliacionDiariaDTO> agruparConciliacionDiariaDTO(
	    List<ConciliacionDiariaDTO> dtos) {
	CopyOnWriteArrayList<ConciliacionDiariaDTO> resultado = new CopyOnWriteArrayList<ConciliacionDiariaDTO>();
	boolean[] cazado = new boolean[dtos.size()];
	LOG.debug("ordenado dtos por precio ....");
	Collections.sort(dtos, new Comparator<ConciliacionDiariaDTO>() {
	    @Override
	    public int compare(ConciliacionDiariaDTO o1,
		    ConciliacionDiariaDTO o2) {
		return o1.getPrecio().compareTo(o2.getPrecio());
	    }
	});
	LOG.debug("Se han ordenado los dtos por precio correctamente: "+dtos.size());
	
	for (int i = 0; i < cazado.length; i++) {
	    cazado[i] = false;
	}
	LOG.debug("Comparando datos .....");
	for (int i = 0; i < dtos.size(); i++) {
	    if (cazado[i] == false) {
		// Si no lo hemos metido lo metemos
		resultado.add(dtos.get(i));
		cazado[i] = true;
		for (int j = i+1; j < dtos.size(); j++) {
		    if (cazado[j] == false
			    && dtos.get(i).getEstado()
				    .equals(dtos.get(j).getEstado())
			    && dtos.get(i).getPrecio()
				    .equals(dtos.get(j).getPrecio())
			    && dtos.get(i).getSentido()
				    .equals(dtos.get(j).getSentido())
			    && dtos.get(i).getIsin()
				    .equals(dtos.get(j).getIsin())
			    && dtos.get(i).getTradedate()
				    .equals(dtos.get(j).getTradedate())) {
			// Hay que agrupar el numero de titulos
			dtos.get(i).setNuTitulos(
				dtos.get(j).getNuTitulos()
					.add(dtos.get(i).getNuTitulos()));
			// dtos.get(i).setImefectivo(dtos.get(j).getImefectivo().add(dtos.get(i).getImefectivo()));
			cazado[j] = true;
		    }
		}
	    }
	}
	LOG.debug("Termina de comparar datos, resultados: "+resultado.size());
	return resultado;
    }

    /**
     * Lista de operciones desglosada por estados
     * 
     * @param resultado
     * @return resultadoFinal
     */
    public List<ConciliacionDiariaDTO> resultadoConEstadoKeyMap(
	    List<ConciliacionDiariaDTO> resultado,
	    List<ConciliacionDiariaDTO> resultadoFinal,
	    List<String> cdoperacionescc, List<String> isins) throws Exception {
	LOG.debug("Dentro del método resultadoConEstadoKeyMap llamando a getkeyMapCaseOperacion obteniendo operaciones con isin ....");
	List<Tmct0caseoperacionejeData> findOperacionesDataWithIns = (List<Tmct0caseoperacionejeData>) coBo
		.findOperacionesDataWithIns(cdoperacionescc, isins);
	LOG.debug("Se han obtenido "+ (findOperacionesDataWithIns != null ? findOperacionesDataWithIns.size() : 0) + " operaciones.");
	LOG.debug("Llama a getkeyMapCaseOperacion con los resultados obtenidos anteriormente ...");
	Map<OperacionEccKeyMap, Tmct0caseoperacionejeData> keyMapCase = this
		.getkeyMapCaseOperacion(findOperacionesDataWithIns);
	LOG.debug("llamando al método: getkeyMapConciliacionDiariaDTOSinPrecio pasando parámetro resultado: "+resultado.size());
	Map<OperacionEccKeyMap, ConciliacionDiariaDTO> keyMapConciliacion = this
		.getkeyMapConciliacionDiariaDTOSinPrecio(resultado);
	LOG.debug("Va a procesar un for recorriendo los valores del Map keyMapConciliacion entradas: "+ keyMapConciliacion.size() +" ....");
	for (Map.Entry<OperacionEccKeyMap, ConciliacionDiariaDTO> entry : keyMapConciliacion.entrySet()) {
	    ConciliacionDiariaDTO cdDTO = entry.getValue();
	    Tmct0caseoperacionejeData noCasado = keyMapCase.get(entry.getKey());
	    if (cdDTO != null && noCasado != null) {
		unificarDatos(cdDTO, noCasado, resultadoFinal);
	    }
	}
	LOG.debug("Sale del for y devuelve la varialbe resultadoFinal: "+resultadoFinal);
	return resultadoFinal;
    }

    public List<ConciliacionDiariaDTO> resultadoConEstado(
	    List<ConciliacionDiariaDTO> resultado,
	    List<ConciliacionDiariaDTO> resultadoFinal,
	    List<String> cdoperacionescc, List<String> isins) throws Exception {

	List<Tmct0caseoperacionejeData> noCasadosLista = (List<Tmct0caseoperacionejeData>) coBo
		.findOperacionesDataWithIns(cdoperacionescc, isins);

	// obtengo los no casados
	for (ConciliacionDiariaDTO cdDTO : resultado) {

	    for (Tmct0caseoperacionejeData noCasado : noCasadosLista) {
		if (cdDTO.getCdoperacionecc().equals(
			noCasado.getCdoperacionecc())
			&& cdDTO.getIsin().equals(noCasado.getCdisin())
			&& cdDTO.getSentido() == noCasado.getCdsentido()) {
		    unificarDatos(cdDTO, noCasado, resultadoFinal);
		}
	    }
	}

	return resultadoFinal;

    }

    @Transactional
    private void unificarDatos(ConciliacionDiariaDTO cdDTO,
	    Tmct0caseoperacionejeData noCasado,
	    List<ConciliacionDiariaDTO> resultadoFinal) {
	//LOG.debug("unificarDatos noCasado: "+ (noCasado == null ? "nulo" : "no nulo"));
	if (noCasado == null) {
	    cdDTO.setDescrAlias("-");
	    cdDTO.setCdAliass("-");
	    creaYanhade(cdDTO, cdDTO.getNuTitulos(), "NO CASADO",
		    resultadoFinal);
	} else {
	    BigDecimal titulosNoCasado = noCasado.getNutitpendcase();
	    BigDecimal titulosTotales = noCasado.getNutitulostotal();
	    BigDecimal titulos = cdDTO.getNuTitulos();

	    if (titulosNoCasado != null
		    && titulos != null
		    && titulos.intValue() >= titulosNoCasado.intValue()) {

		// si el numero de titulos es igual
		if (titulos.compareTo(titulosNoCasado) == 0) {

		    creaYanhade(cdDTO, cdDTO.getNuTitulos(), cdDTO.getEstado(),
			    resultadoFinal);

		}
		// si el num titulos de diaria es mayor que los titulos no
		// casados
		else if (titulos.compareTo(titulosNoCasado) == 1) {

		    // añado los no casados
		    creaYanhade(cdDTO, titulosNoCasado, cdDTO.getEstado(),
			    resultadoFinal);

		    // añado los casados o pendientes de desglosar

		    BigDecimal titulosCasados = titulos
			    .subtract(titulosNoCasado);
		    String estado = "CASADO PDTE. DESGLOSAR";
		    //LOG.debug("Extrayendo ejes .....");
		   
		    List<Tmct0eje> tmct0ejes = ejeBo.findByCaseId(noCasado
			    .getIdcase());
		    //LOG.debug("ejes extraidos: "+(tmct0ejes != null ? tmct0ejes.size() : 0));
		    if (tmct0ejes != null) {
			//LOG.debug("Tratando ejes ....");
			for (Tmct0eje eje : tmct0ejes) {
			    if (eje.getCdalias() != null
				    && !eje.getCdalias().equals("")) {
				cdDTO.setCdAliass(eje.getCdalias());
				this.fillDescali(cdDTO);
				break;
			    }
			}
			//LOG.debug(tmct0ejes.size() +" ejes tratados");
		    }
		    //LOG.debug("Llama a creaYanhade ....");
		    creaYanhade(cdDTO, titulosCasados, estado, resultadoFinal);
		}

	    } else {
		LOG.error("SIBBACServiceConciliacionDiaria:Datos no Concordantes> El numero de titulos de la tabla:"
			+ titulos
			+ " El numero de titulos de la tabla TMCT0CASEOPERACIONEJE es:"
			+ titulosNoCasado);
	    }

	}
    }

    public void creaYanhade(ConciliacionDiariaDTO cdDTO, BigDecimal titulos,
	    String estado, List<ConciliacionDiariaDTO> resultadoFinal) {

	ConciliacionDiariaDTO cd = new ConciliacionDiariaDTO(titulos,
		cdDTO.getIsin(), cdDTO.getTradedate(),
		cdDTO.getSettlementdate(), cdDTO.getSentido(),
		cdDTO.getPrecio(), cdDTO.getCamara(), cdDTO.getBolsa(),
		cdDTO.getNuordenmkt(), cdDTO.getNuexemkt(), cdDTO.getFeMkt(),
		estado);
	cd.setCdoperacionecc(cdDTO.getCdoperacionecc());
	cd.setCdCuenta(cdDTO.getCdCuenta());
	if (estado.equals("CASADO PDTE. DESGLOSAR")) {
	    cd.setDescrAlias(cdDTO.getDescrAlias());
	    cd.setCdAliass(cdDTO.getCdAliass());
	} else {
	    cd.setDescrAlias("-");
	    cd.setCdAliass("-");
	}
	// cd.setImefectivo(cdDTO.getImefectivo());
	if (cd.getNuTitulos().doubleValue() > 0) {
	    resultadoFinal.add(cd);
	}

    }

    public Tmct0ali getAliasFromNuorden(String nuorden) {
	Date now = new Date();
	DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	Integer fechaActual = Integer.parseInt(formatter.format(now));
	Tmct0ali returned = null;
	try {
	    Tmct0ord ord = ordBo.findByNuorden(nuorden);
	    returned = aliBo.findByCdaliassFechaActual(ord.getCdcuenta(),
		    fechaActual);
	} catch (Exception e) {
	    return null;
	}
	return returned;
    }

    @Override
    public List<String> getAvailableCommands() {
	List<String> commands = new ArrayList<String>();
	commands.add(CMD_GET_OPERACIONES_DIARIA);
	return commands;
    }

    @Override
    public List<String> getFields() {
	// TODO Auto-generated method stub
	return null;
    }

    private String findDescrisin(Map<String, String> listaIsines, String isin) {
	if (!listaIsines.containsKey(isin)) {
	    List<Tmct0Valores> listaVal = valoresBo
		    .findByCodigoDeValorOrderByFechaEmisionDesc(isin);
	    if (listaVal != null && listaVal.size() > 0) {
		listaIsines.put(isin, listaVal.get(0).getDescripcion());
	    } else {
		listaIsines.put(isin, "-");
	    }
	}
	return listaIsines.get(isin);
    }

    private Date obtenerFuturo() {
	Calendar cal = Calendar.getInstance();
	cal.set(2200, 11, 11);
	return cal.getTime();
    }

    private List<ConciliacionDiariaDTO> anotacionesECCDTOToConciliacionDiaria(
	    List<AnotacionECCDTO> anotaciones) {
	List<ConciliacionDiariaDTO> listaDtos = new ArrayList<ConciliacionDiariaDTO>();
	for (AnotacionECCDTO anotacion : anotaciones) {
	    ConciliacionDiariaDTO dto = new ConciliacionDiariaDTO(
		    anotacion.getNutitulos(), anotacion.getCdisin(),
		    anotacion.getFcontratacion(), anotacion.getFliquidacion(),
		    anotacion.getCdsentido(), anotacion.getImprecio(),
		    anotacion.getCdcamara().trim(), null, null, null, null);
	    dto.setCdoperacionecc(anotacion.getCdoperacionecc());
	    dto.setCdCuenta(anotacion.getCdCuenta());
	    listaDtos.add(dto);
	}
	return listaDtos;
    }

    private Map<OperacionEccKeyMap, ConciliacionDiariaDTO> getkeyMapConciliacionDiariaDTO(
	    List<ConciliacionDiariaDTO> resultados) {
	Map<OperacionEccKeyMap, ConciliacionDiariaDTO> mapResultados = new HashMap<OperacionEccKeyMap, ConciliacionDiariaDTO>();
	OperacionEccKeyMap keyMap = null;
	for (ConciliacionDiariaDTO resultado : resultados) {
	    keyMap = new OperacionEccKeyMap(null, null, resultado.getIsin()
		    .trim(), resultado.getCdoperacionecc().trim(), null, null,
		    null, null, resultado.getSentido(), resultado.getPrecio(),resultado.getCdCuenta());
	    mapResultados.put(keyMap, resultado);
	}
	return mapResultados;
    }

    private Map<OperacionEccKeyMap, Tmct0desgloseclititData> getkeyMapDesgloseClitit(
	    List<Tmct0desgloseclititData> resultados) {
	Map<OperacionEccKeyMap, Tmct0desgloseclititData> mapResultados = new HashMap<OperacionEccKeyMap, Tmct0desgloseclititData>();
	OperacionEccKeyMap keyMap = null;
	for (Tmct0desgloseclititData dclitit : resultados) {
	    keyMap = new OperacionEccKeyMap(null, null, dclitit.getCdisin()
		    .trim(), dclitit.getCdoperacion().trim(), null, null, null,
		    null, dclitit.getCdsentido(), dclitit.getImprecio());
	    mapResultados.put(keyMap, dclitit);
	}
	return mapResultados;
    }

    private Map<OperacionEccKeyMap, ConciliacionDiariaDTO> getkeyMapConciliacionDiariaDTOSinPrecio(
	    List<ConciliacionDiariaDTO> resultados) {
	Map<OperacionEccKeyMap, ConciliacionDiariaDTO> mapResultados = new HashMap<OperacionEccKeyMap, ConciliacionDiariaDTO>();
	OperacionEccKeyMap keyMap = null;
	for (ConciliacionDiariaDTO resultado : resultados) {
	    keyMap = new OperacionEccKeyMap(null, null, resultado.getIsin()
		    .trim(), resultado.getCdoperacionecc().trim(), null, null,
		    null, null, resultado.getSentido(), null);
	    mapResultados.put(keyMap, resultado);
	}
	return mapResultados;
    }

    private Map<OperacionEccKeyMap, Tmct0caseoperacionejeData> getkeyMapCaseOperacion(
	    List<Tmct0caseoperacionejeData> resultados) {
	Map<OperacionEccKeyMap, Tmct0caseoperacionejeData> mapResultados = new HashMap<OperacionEccKeyMap, Tmct0caseoperacionejeData>();
	OperacionEccKeyMap keyMap = null;
	for (Tmct0caseoperacionejeData caseOp : resultados) {
	    keyMap = new OperacionEccKeyMap(null, null, caseOp.getCdisin()
		    .trim(), caseOp.getCdoperacionecc().trim(), null, null,
		    null, null, caseOp.getCdsentido(), null);
	    mapResultados.put(keyMap, caseOp);
	}
	return mapResultados;
    }

    public void fillDescali(ConciliacionDiariaDTO cdDTO) {
	Tmct0ali alias = aliBo.findByCdaliass(cdDTO.getCdAliass(),
		Tmct0ali.fhfinActivos);
	if (alias != null) {
	    cdDTO.setDescrAlias(alias.getDescrali());
	} else {
	    cdDTO.setDescrAlias(cdDTO.getCdAliass());
	}
    }

}
