package sibbac.business.conciliacion.database.dao;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;

@Repository
public class Norma43MovimientoDaoImpl {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  private static final String FIND_PATAMERCADO = "SELECT mN43.tradedate, cc.tipo_neteo_titulo, mN43.desc_referencia, mN43.importe/100,"
                                                 + "     dc.nuorden, dc.nbooking, dc.nucnfclt, dc.nucnfliq, isin, nutitulos, alo.cdtpoper,"
                                                 + "     (SELECT SUM(imefectivo) FROM tmct0desgloseclitit WHERE dc.iddesglosecamara=iddesglosecamara),"
                                                 + "     mN43.fichero_id, dc.iddesglosecamara, mN43.id"
                                                 + " FROM tmct0_norma43_movimiento mN43, tmct0desglosecamara dc,tmct0alo alo, tmct0infocompensacion ic,"
                                                 + "      tmct0_cuentas_de_compensacion cc"
                                                 + " WHERE dc.iddesglosecamara = CASE WHEN TRANSLATE(SUBSTR(mN43.desc_referencia, 3, 14), ''*'', '' 0123456789'') = '''' THEN SUBSTR(mN43.desc_referencia, 3, 14) ELSE 0 END"
                                                 + "       AND dc.nuorden=alo.nuorden AND dc.nbooking=alo.nbooking AND dc.nucnfclt=alo.nucnfclt"
                                                 + "       AND dc.idinfocompensacion=ic.idinfocomp AND ic.cdctacompensacion=cc.cd_codigo"
                                                 + "       AND mN43.fichero_id IN ({0}) AND mN43.desc_referencia LIKE ''M%'' AND procesado=0";

  private static final String FETCH = " FETCH FIRST {0,number,#} ROWS ONLY";

  private static final String FIND_PATACLIENTE_FIELDS = "mN43.desc_referencia, mN43.fichero_id, mN43.tradedate, mN43.importe, mN43.id, fN43.entidad,"
                                                        + " fN43.oficina, fN43.cuenta, mN43.referencia, mN43.debe, mN43.settlementdate";

  private static final String FIND_PATACLIENTE_LIKE_FALLIDOS = " (mN43.desc_referencia LIKE 'CAU%' OR mN43.desc_referencia LIKE 'COL%' OR mN43.desc_referencia LIKE 'COU%')";

  // private static final String FIND_PATACLIENTE_LIKE_CLEARING = " mN43.desc_referencia LIKE 'C%'";
  // private static final String FIND_PATACLIENTE_LIKE_CLEARING = " mN43.desc_referencia LIKE 'C%' AND"
  // +
  // " (0 <> CASE WHEN TRANSLATE(SUBSTR(mN43.desc_referencia, 3, 14), '*', ' 0123456789') = '' THEN SUBSTR(mN43.desc_referencia, 3, 14) ELSE 0 END)";
  private static final String FIND_PATACLIENTE_LIKE_CLEARING = " mN43.desc_referencia LIKE 'C%' AND mN43.desc_referencia NOT LIKE 'CAU%' AND mN43.desc_referencia NOT LIKE 'COL%' AND mN43.desc_referencia NOT LIKE 'COU%'";

  private static final String FIND_PATACLIENTE_LIKE_NETTING = " mN43.desc_referencia LIKE 'N%'";
  private static final String FIND_PATACLIENTE_NOTLIKE_ALL = " mN43.desc_referencia NOT LIKE '[MCN]%'";

  private static final String FIND_PATACLIENTE_QUERY = "SELECT {0} FROM tmct0_norma43_movimiento mN43, tmct0_norma43_fichero fN43"
                                                       + " WHERE fN43.id = mN43.fichero_id AND mN43.procesado = 0 AND mN43.fichero_id IN ({1}) AND {2}";

  private static final String FIND_PATACLIENTE_COMISIONESBME = "SELECT {0} FROM tmct0_norma43_movimiento mN43, tmct0_norma43_fichero fN43"
                                                               + " WHERE fN43.id = mN43.fichero_id AND mN43.procesado = 0 AND mN43.fichero_id IN ({1})"/*
                                                                                                                                                        * AND
                                                                                                                                                        * {
                                                                                                                                                        * 2
                                                                                                                                                        * }
                                                                                                                                                        * "
                                                                                                                                                        */;
  // private static final String FIND_PATACLIENTE_LIKE_COMISIONESBME = " mN43.desc_referencia LIKE 'B3%'";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @PersistenceContext
  private EntityManager entityManager;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  public List<Norma43Movimiento> findByIdCuentaGroupByTradeDateOrderByImporteDesc(int codigoCuentaCompensacion, Map<String, Serializable> filtros) {
    List<Norma43Movimiento> resultList = new ArrayList<Norma43Movimiento>();
    String select = "SELECT m FROM Norma43Movimiento m WHERE m.fichero.cuenta =:codigoCuentaCompensacion ";
    String orderBy = "order by m.importe desc ";
    String groupBy = "group by m.tradedate, m.fichero.id ";
    String fcontratacionBtw = "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
    //
    StringBuilder sb = new StringBuilder(select);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null
          && !"".equals(entry.getKey())
          && (!entry.getKey().equals("fcontratacionDe") && !entry.getKey().equals("fcontratacionA") && !entry.getKey().equals("fliquidacionDe")
              && !entry.getKey().equals("fliquidacionA") && !entry.getKey().trim().equals("codigoCuentaCompensacion"))) {
        sb.append(" AND m.").append(entry.getKey()).append("=:").append(entry.getKey());
      } else if (entry.getKey().equals("fcontratacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fcontratacionA", filtros)) {
          sb.append(fcontratacionBtw);
        } else {
          sb.append("AND m.tradedate >=:fcontratacionDe ");
        }
      } else if (entry.getKey().equals("fliquidacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fliquidacionA", filtros)) {
          sb.append(fliquidacionBtw);
        } else {
          sb.append("AND a.settlementdate >=:fliquidacionDe ");
        }
      }
    }
    sb.append(groupBy).append(orderBy);
    Query query = entityManager.createQuery(sb.toString());
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    query.setParameter("codigoCuentaCompensacion", codigoCuentaCompensacion);
    resultList.addAll(query.getResultList());
    return resultList;
  } // findByIdCuentaGroupByTradeDateOrderByImporteDesc

  // public List<Object[]> find(String ids, int rowIni, int rowFin) {
  // Query query = entityManager.createNativeQuery(MessageFormat.format(FIND_PATAMERCADO, ids));
  // query.setParameter("rowIni", rowIni);
  // query.setParameter("rowFin", rowFin);
  // return query.getResultList();
  // } // find

  // public int count(String ids) {
  // return (int) entityManager.createNativeQuery(MessageFormat.format(COUNT_PATAMERCADO, ids)).getSingleResult();
  // } // count

  public List<Object[]> findMovimientosPatasMercado(String ids, Integer transactionSize) {
    return entityManager.createNativeQuery(MessageFormat.format(FIND_PATAMERCADO, ids) + MessageFormat.format(FETCH, transactionSize))
                        .getResultList();
  } // findPatasMercado

  public List<Norma43MovimientoDTO> findMovimientosPatasCliente(Boolean procesado, String ids, Integer transactionSize, Boolean like, Boolean netting) {
    Query query = entityManager.createNativeQuery(MessageFormat.format(FIND_PATACLIENTE_QUERY, FIND_PATACLIENTE_FIELDS, ids,
                                                                       like ? (netting ? FIND_PATACLIENTE_LIKE_NETTING
                                                                                      : FIND_PATACLIENTE_LIKE_CLEARING)
                                                                           : FIND_PATACLIENTE_NOTLIKE_ALL)
                                                  + MessageFormat.format(FETCH, transactionSize));
    Norma43MovimientoDTO norma43MovimientoDTO;
    List<Norma43MovimientoDTO> norma43MovimientoDTOList = new ArrayList<Norma43MovimientoDTO>();
    for (Object[] movimientoN43 : (List<Object[]>) query.getResultList()) {
      norma43MovimientoDTO = new Norma43MovimientoDTO(movimientoN43);
      norma43MovimientoDTOList.add(norma43MovimientoDTO);
    } // for

    return norma43MovimientoDTOList;
  } // findPatasCliente

  public List<Norma43MovimientoDTO> findMovimientosFallidos(String ids, Integer transactionSize) {
    Query query = entityManager.createNativeQuery(MessageFormat.format(FIND_PATACLIENTE_QUERY, FIND_PATACLIENTE_FIELDS, ids,
                                                                       FIND_PATACLIENTE_LIKE_FALLIDOS) + MessageFormat.format(FETCH, transactionSize));
    Norma43MovimientoDTO norma43MovimientoDTO;
    List<Norma43MovimientoDTO> norma43MovimientoDTOList = new ArrayList<Norma43MovimientoDTO>();
    for (Object[] movimientoN43 : (List<Object[]>) query.getResultList()) {
      norma43MovimientoDTO = new Norma43MovimientoDTO(movimientoN43);
      norma43MovimientoDTOList.add(norma43MovimientoDTO);
    } // for

    return norma43MovimientoDTOList;
  } // findMovimientosFallidos

  public List<Norma43MovimientoDTO> findMovimientosComisionesBME(String ids, Integer transactionSize) {
    Query query = entityManager.createNativeQuery(MessageFormat.format(FIND_PATACLIENTE_COMISIONESBME, FIND_PATACLIENTE_FIELDS, ids/*
                                                                                                                                    * ,
                                                                                                                                    * FIND_PATACLIENTE_LIKE_COMISIONESBME
                                                                                                                                    */)/*
                                                                                                                                        * +
                                                                                                                                        * MessageFormat
                                                                                                                                        * .
                                                                                                                                        * format(FETCH
                                                                                                                                        * ,
                                                                                                                                        * transactionSize
                                                                                                                                        * )
                                                                                                                                        */);
    Norma43MovimientoDTO norma43MovimientoDTO;
    List<Norma43MovimientoDTO> norma43MovimientoDTOList = new ArrayList<Norma43MovimientoDTO>();
    for (Object[] movimientoN43 : (List<Object[]>) query.getResultList()) {
      norma43MovimientoDTO = new Norma43MovimientoDTO(movimientoN43);
      norma43MovimientoDTOList.add(norma43MovimientoDTO);
    } // for

    return norma43MovimientoDTOList;
  } // findMovimientosComisionesBME

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  private boolean checkIfKeyExist(String key, Map<String, Serializable> params) {
    boolean exist = false;
    if (params.get(key) != null && !params.get(key).equals("")) {
      exist = true;
    }
    return exist;
  } // checkIfKeyExist

} // Norma43MovimientoDaoImpl
