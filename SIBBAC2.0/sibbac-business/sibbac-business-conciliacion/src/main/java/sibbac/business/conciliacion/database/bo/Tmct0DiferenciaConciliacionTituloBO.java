package sibbac.business.conciliacion.database.bo;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.Tmct0DiferenciaConciliacionTituloDAO;
import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionTitulo;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0DiferenciaConciliacionTituloBO extends
		AbstractBo< Tmct0DiferenciaConciliacionTitulo, Long, Tmct0DiferenciaConciliacionTituloDAO > {

	public List< Tmct0DiferenciaConciliacionTitulo > findByTradeDateGreaterThanEqualOrderByTradeDate( Date tradeDate ) {
		List< Tmct0DiferenciaConciliacionTitulo > resultList = null;
		if ( tradeDate != null ) {
			resultList = dao.findByTradeDateGreaterThanEqualOrderByTradeDate( tradeDate );
		}
		return resultList;
	}
}
