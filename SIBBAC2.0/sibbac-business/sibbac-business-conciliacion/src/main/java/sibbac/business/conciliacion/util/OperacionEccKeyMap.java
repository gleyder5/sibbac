package sibbac.business.conciliacion.util;

import java.math.BigDecimal;

public class OperacionEccKeyMap {

	private final String cdmiembromkt, segmento,  cdisin,
			cdoperacionecc, nuordenmkt, nuexemkt, estado, alias, cdCuenta;
	private final Character sentido;
	private final BigDecimal precio;
	/**
	 * @param cdmiembromkt
	 * @param segmento
	 * @param cdisin
	 * @param cdoperacionecc
	 * @param nuordenmkt
	 * @param nuexemkt
	 * @param estado
	 * @param alias
	 * @param sentido
	 * @param precio
	 */
	public OperacionEccKeyMap( String cdmiembromkt, String segmento, String cdisin, String cdoperacionecc, String nuordenmkt,
			String nuexemkt, String estado, String alias, Character sentido, BigDecimal precio ) {
		super();
		this.cdmiembromkt = cdmiembromkt;
		this.segmento = segmento;
		this.cdisin = cdisin;
		this.cdoperacionecc = cdoperacionecc;
		this.nuordenmkt = nuordenmkt;
		this.nuexemkt = nuexemkt;
		this.estado = estado;
		this.alias = alias;
		this.sentido = sentido;
		this.precio = precio;
		this.cdCuenta = null;
	}
	
	public OperacionEccKeyMap( String cdmiembromkt, String segmento, String cdisin, String cdoperacionecc, String nuordenmkt,
			String nuexemkt, String estado, String alias, Character sentido, BigDecimal precio, String cdCuenta ) {
		super();
		this.cdmiembromkt = cdmiembromkt;
		this.segmento = segmento;
		this.cdisin = cdisin;
		this.cdoperacionecc = cdoperacionecc;
		this.nuordenmkt = nuordenmkt;
		this.nuexemkt = nuexemkt;
		this.estado = estado;
		this.alias = alias;
		this.sentido = sentido;
		this.precio = precio;
		this.cdCuenta = cdCuenta;
	}
	
	/**
	 * @return the cdmiembromkt
	 */
	public String getCdmiembromkt() {
		return cdmiembromkt;
	}
	
	/**
	 * @return the segmento
	 */
	public String getSegmento() {
		return segmento;
	}
	
	/**
	 * @return the cdisin
	 */
	public String getCdisin() {
		return cdisin;
	}
	
	/**
	 * @return the cdoperacionecc
	 */
	public String getCdoperacionecc() {
		return cdoperacionecc;
	}
	
	/**
	 * @return the nuordenmkt
	 */
	public String getNuordenmkt() {
		return nuordenmkt;
	}
	
	/**
	 * @return the nuexemkt
	 */
	public String getNuexemkt() {
		return nuexemkt;
	}
	
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	
	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}
	
	/**
	 * @return the sentido
	 */
	public Character getSentido() {
		return sentido;
	}
	
	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}
	
	/**
	 * retorna el código de cuenta compensación
	 * 
	 * @return cdCuenta
	 */
	public String getCdCuenta() {
		return cdCuenta;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( alias == null ) ? 0 : alias.hashCode() );
		result = prime * result + ( ( cdisin == null ) ? 0 : cdisin.hashCode() );
		result = prime * result + ( ( cdmiembromkt == null ) ? 0 : cdmiembromkt.hashCode() );
		result = prime * result + ( ( cdoperacionecc == null ) ? 0 : cdoperacionecc.hashCode() );
		result = prime * result + ( ( estado == null ) ? 0 : estado.hashCode() );
		result = prime * result + ( ( nuexemkt == null ) ? 0 : nuexemkt.hashCode() );
		result = prime * result + ( ( nuordenmkt == null ) ? 0 : nuordenmkt.hashCode() );
		result = prime * result + ( ( precio == null ) ? 0 : precio.hashCode() );
		result = prime * result + ( ( segmento == null ) ? 0 : segmento.hashCode() );
		result = prime * result + ( ( sentido == null ) ? 0 : sentido.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		OperacionEccKeyMap other = ( OperacionEccKeyMap ) obj;
		if ( alias == null ) {
			if ( other.alias != null )
				return false;
		} else if ( !alias.equals( other.alias ) )
			return false;
		if ( cdisin == null ) {
			if ( other.cdisin != null )
				return false;
		} else if ( !cdisin.equals( other.cdisin ) )
			return false;
		if ( cdmiembromkt == null ) {
			if ( other.cdmiembromkt != null )
				return false;
		} else if ( !cdmiembromkt.equals( other.cdmiembromkt ) )
			return false;
		if ( cdoperacionecc == null ) {
			if ( other.cdoperacionecc != null )
				return false;
		} else if ( !cdoperacionecc.equals( other.cdoperacionecc ) )
			return false;
		if ( estado == null ) {
			if ( other.estado != null )
				return false;
		} else if ( !estado.equals( other.estado ) )
			return false;
		if ( nuexemkt == null ) {
			if ( other.nuexemkt != null )
				return false;
		} else if ( !nuexemkt.equals( other.nuexemkt ) )
			return false;
		if ( nuordenmkt == null ) {
			if ( other.nuordenmkt != null )
				return false;
		} else if ( !nuordenmkt.equals( other.nuordenmkt ) )
			return false;
		if ( precio == null ) {
			if ( other.precio != null )
				return false;
		} else if ( !precio.equals( other.precio ) )
			return false;
		if ( segmento == null ) {
			if ( other.segmento != null )
				return false;
		} else if ( !segmento.equals( other.segmento ) )
			return false;
		if ( sentido == null ) {
			if ( other.sentido != null )
				return false;
		} else if ( !sentido.equals( other.sentido ) )
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OperacionEccKeyMap [cdmiembromkt=" + cdmiembromkt + ", segmento=" + segmento + ", cdisin=" + cdisin + ", cdoperacionecc="
				+ cdoperacionecc + ", nuordenmkt=" + nuordenmkt + ", nuexemkt=" + nuexemkt + ", estado=" + estado + ", alias=" + alias
				+ ", sentido=" + sentido + ", precio=" + precio + "]";
	}

	
	
	
	
	
	
	
	

}
