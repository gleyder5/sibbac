package sibbac.business.conciliacion.database.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionTitulo;


@Repository
public interface Tmct0DiferenciaConciliacionTituloDAO extends JpaRepository< Tmct0DiferenciaConciliacionTitulo, Long > {

	/**
	 * 
	 * @param tradeDate
	 * @return
	 */
	List< Tmct0DiferenciaConciliacionTitulo > findByTradeDateGreaterThanEqualOrderByTradeDate( Date tradeDate );
}
