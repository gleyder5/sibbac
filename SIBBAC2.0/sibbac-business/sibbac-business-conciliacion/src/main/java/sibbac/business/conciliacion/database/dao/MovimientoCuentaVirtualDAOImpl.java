package sibbac.business.conciliacion.database.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData;
import sibbac.business.conciliacion.database.dto.IsinDTO;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;

@Repository
public class MovimientoCuentaVirtualDAOImpl {

  private static final String TAG = MovimientoCuentaVirtualDAOImpl.class.getName();
  private static final Logger LOG = LoggerFactory.getLogger(TAG);
  @PersistenceContext
  private EntityManager em;
  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentasCompensacionBO;

  /**
   * Devuelve los movimientos de todas las cuentas
   * 
   * @param filtros
   * @return
   */
  public List<MovimientoCuentaVirtualData> findMovimientosGrupoByIsinAndIdClienteOrderByClient(
      Map<String, Serializable> filtros, String customCriteria) {
    return findMovimientosGrupoByIsinAndIdClienteOrderByClient(filtros, false, customCriteria);
  }

  @SuppressWarnings("unchecked")
  public List<MovimientoCuentaVirtualData> findMovimientosGrupoByIsinAndIdClienteOrderByClient(
      Map<String, Serializable> filtros, boolean isClearing, String customCriteria) {
    List<MovimientoCuentaVirtualData> resultList = new ArrayList<MovimientoCuentaVirtualData>();
    String select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData(m.camara, m.idcuentaliq, m.alias, m.descrali, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate, sum(m.titulos) as titulos, sum(m.efectivo) as efectivo) FROM MovimientoCuentaVirtual m ";
    //
    String selectIdsClearing = "SELECT cc.idCuentaCompensacion FROM Tmct0CuentasDeCompensacion cc WHERE cc.cuentaClearing = 'S'";
    String groupBy = "group by m.camara, m.idcuentaliq, m.alias, m.descrali, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate ";
    String orderBy = "order by m.alias asc";
    boolean addedWhere = false;
    StringBuilder sb = new StringBuilder(select);

    if (isClearing) {
      addedWhere = true;
      String where = "WHERE m.idcuentacomp in (" + selectIdsClearing + ") ";
      sb.append(where);
    }
    if (customCriteria != null && !customCriteria.equals("")) {
      String where = "";
      if (!addedWhere) {
        where = "WHERE " + customCriteria + " ";
        addedWhere = true;
      }
      else {
        where = customCriteria + " ";
      }
      sb.append(where);
    }
    // se construye la sql segun los parametros recibidos
    if (filtros != null) {
      for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
        if (entry.getValue() != null && !"".equals(entry.getValue().toString())) {
          if (!addedWhere) {
            sb.append("WHERE ").append("m.").append(entry.getKey()).append(" =:").append(entry.getKey()).append(" ");
            addedWhere = true;
          }
          else {
            sb.append("AND ").append("m.").append(entry.getKey()).append(" =:").append(entry.getKey()).append(" ");
          }
        }
      }
    }

    sb.append(groupBy);
    sb.append(orderBy);

    Query query = null;
    try {
      query = em.createQuery(sb.toString());
    }
    catch (PersistenceException ex) {
      LOG.error(ex.getMessage(), ex);
      return resultList;
    }
    // se introducen los parametros
    if (filtros != null) {
      for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
        if (entry.getValue() != null && !"".equals(entry.getValue().toString())) {
          query.setParameter(entry.getKey(), entry.getValue());
        }
      }
    }
    LOG.debug(TAG + ": A punto de recuperar Movimientos de las cuentas clearing agrupadas por cliente ");

    try {
      resultList.addAll(query.getResultList());
    }
    catch (PersistenceException ex) {
      LOG.error(ex.getMessage(), ex);
    }
    return resultList;
  }

  @SuppressWarnings("unchecked")
  public List<MovimientoCuentaVirtualData> findByIdCuentaGroupByIsinAndSentidoAndFContratacion(long idCuentaComp,
      Map<String, Serializable> filtros) {
    List<MovimientoCuentaVirtualData> resultList = new ArrayList<>();
    String select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData(m, sum(m.titulos) as titulos, sum(m.efectivo) as efectivo) FROM MovimientoCuentaVirtual m WHERE m.idcuentacomp =:idCuentaComp ";
    String groupBy = "group by m.isin, m.camara, m.sentido, m.tradedate, m.id ";
    String orderBy = "order by m.efectivo desc";
    String fcontratacionBtw = "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
    StringBuilder sb = new StringBuilder(select);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null
          && !"".equals(entry.getKey())
          && (!entry.getKey().equals("fcontratacionDe") && !entry.getKey().equals("fcontratacionA")
              && !entry.getKey().equals("fliquidacionDe") && !entry.getKey().equals("fliquidacionA") && !entry.getKey()
              .trim().equals("idCuentaDeCompensacion"))) {
        sb.append(" AND m.").append(entry.getKey()).append("=:").append(entry.getKey());
      }
      else if (entry.getKey().equals("fcontratacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fcontratacionA", filtros)) {
          sb.append(fcontratacionBtw);
        }
        else {
          sb.append("AND m.tradedate >=:fcontratacionDe ");
        }
      }
      else if (entry.getKey().equals("fliquidacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fliquidacionA", filtros)) {
          sb.append(fliquidacionBtw);
        }
        else {
          sb.append("AND a.settlementdate >=:fliquidacionDe ");
        }
      }
    }
    sb.append(groupBy).append(orderBy);
    Query query = em.createQuery(sb.toString());
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    query.setParameter("idCuentaComp", idCuentaComp);
    try {
      resultList.addAll(query.getResultList());
    }
    catch (PersistenceException ex) {
      LOG.error(ex.getMessage(), ex);
    }
    return resultList;
  }

  @SuppressWarnings("unchecked")
  public List<Object[]> findByIdCuentaClearingGroupByIsinAndSentidoAndFContratacion(long[] idCuentasCompClearing,
      Map<String, Serializable> filtros) {
    List<Object[]> resultList = new ArrayList<Object[]>();
    String select = "SELECT m, sum(m.titulos) as titulos, sum(m.efectivo) as efectivos, sum(m.corretaje) FROM MovimientoCuentaVirtual m WHERE m.idcuentacomp in :cuentasClearing ";
    String groupBy = "group by m.isin, m.camara, m.sentido, m.tradedate ";
    String orderBy = "order by m.efectivo desc";
    String fcontratacionBtw = "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
    StringBuilder sb = new StringBuilder(select);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null
          && !"".equals(entry.getKey())
          && (!entry.getKey().equals("fcontratacionDe") && !entry.getKey().equals("fcontratacionA")
              && !entry.getKey().equals("fliquidacionDe") && !entry.getKey().equals("fliquidacionA") && !entry.getKey()
              .trim().equals("idCuentaDeCompensacion"))) {
        sb.append(" AND m.").append(entry.getKey()).append("=:").append(entry.getKey());
      }
      else if (entry.getKey().equals("fcontratacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fcontratacionA", filtros)) {
          sb.append(fcontratacionBtw);
        }
        else {
          sb.append("AND m.tradedate >=:fcontratacionDe ");
        }
      }
      else if (entry.getKey().equals("fliquidacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fliquidacionA", filtros)) {
          sb.append(fliquidacionBtw);
        }
        else {
          sb.append("AND a.settlementdate >=:fliquidacionDe ");
        }
      }
    }
    sb.append(groupBy).append(orderBy);
    Query query = em.createQuery(sb.toString());
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    try {
      query.setParameter("cuentasClearing", idCuentasCompClearing);
    }
    catch (RuntimeException ex) {
      LOG.error(ex.getMessage(), ex);
    }
    resultList.addAll(query.getResultList());
    return resultList;
  }

  @SuppressWarnings("unchecked")
  public List<MovimientoCuentaVirtualData> findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion(
      long idCuentaLiq, Map<String, Serializable> filtros) {
    List<MovimientoCuentaVirtualData> resultList = new ArrayList<>();
    String select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData(m.camara, m.idcuentaliq, m.alias, m.descrali, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate, sum(m.titulos) as titulos, sum(m.efectivo) as efectivo)  FROM MovimientoCuentaVirtual m WHERE m.idcuentaliq=:idCuentaLiq ";

    // Opcion en la que son todas las idcuentaliq
    if (idCuentaLiq == 0) {
      select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData(m.camara, m.idcuentaliq, m.alias, m.descrali, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate, sum(m.titulos) as titulos, sum(m.efectivo) as efectivo)  FROM MovimientoCuentaVirtual m WHERE m.idcuentaliq!=:idCuentaLiq ";
    }

    String groupBy = "group by m.camara, m.idcuentaliq, m.alias, m.descrali, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate ";
    // String orderBy = " order by m.efectivo desc";
    String orderBy = "";
    // String fcontratacionBtw =
    // "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
    StringBuilder sb = new StringBuilder(select);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null
          && !"".equals(entry.getValue())
          && !"".equals(entry.getKey())
          && (!entry.getKey().equals("fliquidacionDe") && !entry.getKey().equals("fliquidacionA") && !entry.getKey()
              .trim().equals("idCuentaLiq"))) {
        sb.append(" AND m.").append(entry.getKey()).append("=:").append(entry.getKey()).append(" ");
      }
      else if (entry.getKey().equals("fliquidacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fliquidacionA", filtros)) {
          sb.append(fliquidacionBtw);
        }
        else {
          sb.append("AND a.settlementdate >=:fliquidacionDe ");
        }
      }
    }
    sb.append(groupBy).append(orderBy);
    Query query = em.createQuery(sb.toString());
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    query.setParameter("idCuentaLiq", idCuentaLiq);
    resultList.addAll(query.getResultList());
    return resultList;
  }

  @SuppressWarnings("unchecked")
  public List<MovimientoCuentaVirtualData> findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacionSinAlias(
      long idCuentaLiq, Map<String, Serializable> filtros) {
    List<MovimientoCuentaVirtualData> resultList = new ArrayList<>();
    String select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData(m.camara, m.idcuentaliq, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate, sum(m.titulos) as titulos, sum(m.efectivo) as efectivo)  FROM MovimientoCuentaVirtual m WHERE m.idcuentaliq=:idCuentaLiq ";

    // Opcion en la que son todas las idcuentaliq
    if (idCuentaLiq == 0) {
      select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData(m.camara, m.idcuentaliq, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate, sum(m.titulos) as titulos, sum(m.efectivo) as efectivo)  FROM MovimientoCuentaVirtual m WHERE m.idcuentaliq!=:idCuentaLiq ";
    }

    String groupBy = "group by m.camara, m.idcuentaliq, m.isin, m.descripcionisin, m.sentidocuentavirtual, m.sentido, m.settlementdate ";
    // String orderBy = " order by m.efectivo desc";
    String orderBy = "";
    // String fcontratacionBtw =
    // "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
    StringBuilder sb = new StringBuilder(select);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null
          && !"".equals(entry.getValue())
          && !"".equals(entry.getKey())
          && (!entry.getKey().equals("fliquidacionDe") && !entry.getKey().equals("fliquidacionA") && !entry.getKey()
              .trim().equals("idCuentaLiq"))) {
        sb.append(" AND m.").append(entry.getKey()).append("=:").append(entry.getKey()).append(" ");
      }
      else if (entry.getKey().equals("fliquidacionDe") && entry.getValue() != null) {
        // si vienen las dos fechas se hace un between
        if (checkIfKeyExist("fliquidacionA", filtros)) {
          sb.append(fliquidacionBtw);
        }
        else {
          sb.append("AND a.settlementdate >=:fliquidacionDe ");
        }
      }
    }
    sb.append(groupBy).append(orderBy);
    Query query = em.createQuery(sb.toString());
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    query.setParameter("idCuentaLiq", idCuentaLiq);
    resultList.addAll(query.getResultList());
    return resultList;
  }

  private boolean checkIfKeyExist(String key, Map<String, Serializable> params) {
    boolean exist = false;
    if (params.get(key) != null && !params.get(key).equals("")) {
      exist = true;
    }
    return exist;
  }

  @SuppressWarnings("unchecked")
  public List<MovimientoCuentaVirtualData> findByIsinAndSettlementdateAndIdCuenta(String isin, Date settlementdateTo,
      Date settlementdateFrom, Long idcuentaliq) {
    List<MovimientoCuentaVirtualData> result = null;
    Query query = null;
    StringBuffer stringBuffer = new StringBuffer(
        "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData( m.camara, m.isin, m.sentidocuentavirtual, m.sentido, sum(m.titulos), sum(m.efectivo), t.cdCodigo, m.descripcionisin ) FROM ");
    stringBuffer.append("MovimientoCuentaVirtual m ");
    stringBuffer.append(" ,Tmct0CuentaLiquidacion t ");
    stringBuffer.append(" WHERE m.idcuentaliq = t.id");

    if (!"".equals(isin)) {
      stringBuffer.append(" AND m.isin = :isin");
    }
    if (!(new Long(0)).equals(idcuentaliq)) {
      stringBuffer.append(" AND m.idcuentaliq = :idcuentaliq");
    }
    if (settlementdateTo != null && settlementdateFrom != null) {
      stringBuffer.append(" AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA");
    }
    if (settlementdateTo != null && settlementdateFrom == null) {
      stringBuffer.append("AND m.settlementdate >=:fliquidacionDe ");
    }

    stringBuffer.append(" group by m.camara, m.isin, m.sentidocuentavirtual, m.sentido, t.cdCodigo, m.descripcionisin");
    try {
      query = em.createQuery(stringBuffer.toString());
      if (!"".equals(isin)) {
        query.setParameter("isin", isin);
      }
      if (!(new Long(0)).equals(idcuentaliq)) {
        query.setParameter("idcuentaliq", idcuentaliq);
      }
      if (settlementdateTo != null && settlementdateFrom != null) {
        query.setParameter("fliquidacionA", settlementdateTo);
        query.setParameter("fliquidacionDe", settlementdateFrom);
      }
      if (settlementdateTo != null && settlementdateFrom == null) {
        query.setParameter("fliquidacionDe", settlementdateFrom);
      }
      result = query.getResultList();
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);

    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public List<IsinDTO> getIsin() {
    Set<IsinDTO> result = null;
    Query query = null;
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer
        .append("SELECT new sibbac.business.conciliacion.database.dto.IsinDTO( m.isin, m.descripcionisin)  FROM ");
    stringBuffer.append("MovimientoCuentaVirtual m ");
    try {
      query = em.createQuery(stringBuffer.toString());
      result = new HashSet<IsinDTO>(query.getResultList());
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }
    return new ArrayList<IsinDTO>(result);
  }
}
