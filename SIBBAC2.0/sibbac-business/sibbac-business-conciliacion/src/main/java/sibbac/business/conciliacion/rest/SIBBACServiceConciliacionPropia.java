package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.conciliacion.database.bo.MovimientoCuentaVirtualBo;
import sibbac.business.conciliacion.database.bo.Norma43MovimientoBo;
import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData;
import sibbac.business.conciliacion.database.dto.AnotacionECCDTO;
import sibbac.business.conciliacion.database.dto.AnotacionExportECCDTO;
import sibbac.business.conciliacion.database.dto.MovimientoCuentaVirtualDTO;
import sibbac.business.conciliacion.database.dto.Norma43MovimientoDTO;
import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtual;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoManualBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.data.AnotacioneccData;
import sibbac.business.wrappers.database.model.Tmct0MovimientoManual;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceConciliacionPropia implements SIBBACServiceBean {

  // CLAVE RESULTADO
  private static final String RESULT_KEY = "result_cuenta_propia";
  private static final String RESULT_KEY_MOVIMIENTOS_CUENTA_PROPIA = "result_movimientos_propia";
  private static final String RESULT_KEY_MOVIMIENTO_NORMA_43 = "result_movimiento_norma_43";
  private static final String RESULT_KEY_INITIAL_DATE_BETWEEN = "result_date_between";
  // nombre propiedades en conciliacion.properties
  private static final String CONCILIACION_CUENTA_PROPIA = "conciliacion.cuenta.propia";
  private static final String CONCILIACION_CUENTA_INDIVIDUAL = "conciliacion.cuenta.individual";
  private static final String CONCILIACION_ID_CUENTA_COMPENSACION_PROPIA = "conciliacion.id.cuenta.compensacion.propia";
  private static final String CONCILIACION_CODIGO_CUENTA_PROPIA = "conciliacion.codigo.cuenta.propia";
  // Comandos
  private static final String CMD_GET_ANOTACIONESECC_BY_TIPO_CUENTA = "getAnotacioneseccByTipoCuenta";
  private static final String CMD_GET_ANOTACIONESECC_BY_TIPO_CUENTA_EXPORT = "getAnotacioneseccByTipoCuentaExport";
  private static final String CMD_GET_MOVIMIENTOS_CUENTA_VIRTUAL_PROPIA = "getMovimientosCuentaVirtualPropia";
  private static final String CMD_GET_MOVIMIENTOS_FICHERO_N43 = "getMovimientosFicheroNorma43";
  private static final String CMD_GET_FECHAS_CONTRATACION_INTERVALO = "getFContratacionInterval";
  // LOGGER
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceConciliacionPropia.class);
  // Util
  private ConciliacionUtil util = ConciliacionUtil.getInstance();
  @Autowired
  private Tmct0anotacioneccBo anotacioneccBo;
  @Autowired
  private MovimientoCuentaVirtualBo movBo;
  @Autowired
  private Norma43MovimientoBo norma43MovimientoBo;
  @Autowired
  private Tmct0ValoresBo valoresBo;
  @Autowired
  private Tmct0MovimientoManualBo movManualBo;
  // PROPIADEDADES
  @Value("${" + CONCILIACION_CUENTA_PROPIA + "}")
  private long idTipoCuentaPropia;
  @Value("${" + CONCILIACION_ID_CUENTA_COMPENSACION_PROPIA + "}")
  private long idCuentaPropia;
  @Value("${" + CONCILIACION_CUENTA_INDIVIDUAL + "}")
  private long idTipoCuentaIndividual;
  @Value("${" + CONCILIACION_CODIGO_CUENTA_PROPIA + "}")
  private int codigoCuentaPropia;

  @Override
  public WebResponse process(WebRequest webRequest) {
    WebResponse response = new WebResponse();
    final String action = webRequest.getAction();
    Map<String, Object> resultado = new HashMap<String, Object>();
    Map<String, String> filtros = webRequest.getFilters();
    Map<String, Serializable> params = null;
    if (filtros != null) {
      params = util.normalizeFilters(filtros);
    }
    else {
      params = new HashMap<String, Serializable>();
    }
    LOG.debug("Entrando en el servicio conciliacion propia para ejecutar action: " + action);
    switch (action) {
      case CMD_GET_ANOTACIONESECC_BY_TIPO_CUENTA:
        resultado = getAnotacioneseccByTipoCuenta(params);
        break;
      case CMD_GET_ANOTACIONESECC_BY_TIPO_CUENTA_EXPORT:
        resultado = getAnotacioneseccByTipoCuentaExport(params);
        break;
      case CMD_GET_MOVIMIENTOS_CUENTA_VIRTUAL_PROPIA:
        resultado = getMovimientosCuentaVirtualPropia(webRequest.getFilters());
        break;
      case CMD_GET_MOVIMIENTOS_FICHERO_N43:
        resultado = getMovimientosFicheroNorma43(webRequest.getFilters());
        break;
      case CMD_GET_FECHAS_CONTRATACION_INTERVALO:
        resultado = getFContratacionInterval();
        break;
    }
    response.setResultados(resultado);
    LOG.debug("saliendo del servicio conciliacion propia para mostrar resultados en pantalla ... ");

    return response;
  }

  private Map<String, Object> getFContratacionInterval() {
    Map<String, String> fechas = new HashMap<String, String>();
    Map<String, Object> result = new HashMap<String, Object>();
    result.put(RESULT_KEY_INITIAL_DATE_BETWEEN, fechas);
    return result;
  }

  private Map<String, Object> getAnotacioneseccByTipoCuenta(Map<String, Serializable> filtros) {
    Map<String, Object> result = new HashMap<String, Object>();

    List<AnotacioneccData> resultList = null;

    int maxResult = -1;
    int startPosition = -1;
    resultList = anotacioneccBo.findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentidoWithPagination(
        idTipoCuentaPropia, idTipoCuentaIndividual, filtros, startPosition, maxResult);
    // Se extraen los movimientos manuales y se acoplan al array resultante;
    filtros.put("idTipoCuentaPropia", idTipoCuentaPropia);
    filtros.put("idTipoCuentaIndividual", idTipoCuentaIndividual);
    //
    List<Tmct0MovimientoManual> listaMovimientosManuales = movManualBo.findAll(filtros);
    if (resultList != null) {

      ArrayList<AnotacionECCDTO> dtoList = new ArrayList<AnotacionECCDTO>(util.entitiesListToAnDTOList(resultList));
      if (listaMovimientosManuales != null) {
        for (Tmct0MovimientoManual entity : listaMovimientosManuales) {
          AnotacionECCDTO dto = new AnotacionECCDTO();
          dto.setCdcamara(entity.getCamara());
          dto.setCdCuenta(entity.getCuentaLiquidacion());
          dto.setCdisin(entity.getIsin());
          char sentido = entity.getSentido().charAt(0);
          sentido = sentido == 'E' ? 'C' : 'V';
          dto.setCdsentido(sentido);
          if (entity.getCuentaLiquidacion() == null || entity.getCuentaLiquidacion().equals("")) {
            dto.setCdCuenta(entity.getCuentaLiquidacionEfectivo());
            dto.setCodigoCuentaCompensacion(entity.getCuentaLiquidacionEfectivo());
          }
          else {
            dto.setCodigoCuentaCompensacion(entity.getCuentaCompensacion());
          }
          dto.setFcontratacion(FormatDataUtils.convertDateToString(entity.getFechaContratacion()));
          dto.setFliquidacion(FormatDataUtils.convertDateToString(entity.getFechaLiquidacion()));
          dto.setImefectivo(entity.getEfectivo());
          dto.setNutitulos(entity.getTitulos());
          dto.setIsinDescripcion(entity.getDescripcionIsin());
          BigDecimal precio = entity.getPrecio();
          dto.setImprecio(precio.abs());
          agruparMovimiento(dtoList, dto);
        }
      }
      List<AnotacionECCDTO> conciliados = conciliarAnotacionesECC(dtoList);
      result.put(RESULT_KEY, conciliados);
    }
    return result;
  }

  private void agruparMovimiento(ArrayList<AnotacionECCDTO> dtoList, AnotacionECCDTO dto) {
    boolean casado = false;
    for (AnotacionECCDTO inDto : dtoList) {
      if (inDto.getCdisin().equals(dto.getCdisin()) && inDto.getCdCuenta().equals(dto.getCdCuenta())
          && inDto.getImprecio().compareTo(dto.getImprecio()) == 0) {

        inDto.setImefectivo(inDto.getImefectivo().add(dto.getImefectivo()));
        inDto.setNutitulos(inDto.getNutitulos().add(dto.getNutitulos()));
        casado = true;
        break;
      }
    }
    if (!casado) {
      dtoList.add(dto);
    }
  }

  private Map<String, Object> getAnotacioneseccByTipoCuentaExport(Map<String, Serializable> filtros) {
    Map<String, Object> anotacionesByTipoCuenta = getAnotacioneseccByTipoCuenta(filtros);
    Map<String, Object> result = new HashMap<String, Object>();
    Iterator<Entry<String, Object>> it = anotacionesByTipoCuenta.entrySet().iterator();
    Entry<String, Object> key;
    List<AnotacionECCDTO> AnotacionECCDTOs;
    List<AnotacionExportECCDTO> anotacionExportECCDTOs = new ArrayList<AnotacionExportECCDTO>();
    while (it.hasNext()) {
      key = it.next();
      AnotacionECCDTOs = (List<AnotacionECCDTO>) key.getValue();
      for (AnotacionECCDTO anotacionECCDTO : AnotacionECCDTOs) {
        anotacionExportECCDTOs.add(new AnotacionExportECCDTO(anotacionECCDTO.getCdisin(), anotacionECCDTO
            .getIsinDescripcion(), anotacionECCDTO.getCdCuenta(), anotacionECCDTO.getNutitulos(), anotacionECCDTO
            .getImefectivo()));
      }
      result.put(key.getKey(), anotacionExportECCDTOs);
    }
    return result;

  }

  private List<AnotacionECCDTO> conciliarAnotacionesECC(ArrayList<AnotacionECCDTO> anotaciones) {
    ArrayList<AnotacionECCDTO> resultado = new ArrayList<>();
    CopyOnWriteArrayList<AnotacionECCDTO> copyAnotaciones = new CopyOnWriteArrayList<AnotacionECCDTO>(anotaciones);
    boolean cazado = false;
    for (int i = 0; i < anotaciones.size(); i++) {
      AnotacionECCDTO anC = anotaciones.get(i);
      // Si el elmento ya esta agrupado se salta al siguiente
      if (checkIfExistObjectInDTOList(anC, resultado)) {
        continue;
      }
      for (int z = i + 1; z < copyAnotaciones.size(); z++) {

        AnotacionECCDTO anV = copyAnotaciones.get(z);
        // Si la anotacion con la que se va a comparar es sentido
        // contrario
        if (anV.getCdsentido() != anC.getCdsentido()) {
          // Si los objetos tienen distinto sentido, mismo isin,
          // misma cuenta y mismo precio, entra para agrupar
          if (anV.getCdisin().equals(anC.getCdisin()) && anV.getCdCuenta().equals(anC.getCdCuenta())
              && anV.getImprecio().compareTo(anC.getImprecio()) == 0) {
            // Se detectan los sentidos y se establecen los objetos
            final AnotacionECCDTO compra = (anV.getCdsentido() == 'C') ? anV : anC;
            final AnotacionECCDTO venta = (anC.getCdsentido() == 'C') ? anV : anC;
            //
            int signo = venta.getNutitulos().compareTo(compra.getNutitulos());
            boolean isSentidoVenta = (signo > 0) ? true : false;
            cazado = true;
            final AnotacionECCDTO dif = extracDiference(compra, venta, isSentidoVenta);
            // Si no esta en la lista y los titulos no son 0, se
            // añade
            if (!checkIfExistObjectInDTOList(dif, resultado)) {
              List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(venta.getCdisin());
              Tmct0Valores valor = (valores != null && valores.size() > 0) ? valores.get(0) : null;
              if (valor != null) {
                dif.setIsinDescripcion(valor.getDescripcion());
              }
              resultado.add(dif);
              break;
            }
          }
        } /*
           * else { if (anV.getCdisin().equals(anC.getCdisin()) &&
           * anV.getCdCuenta().equals(anC.getCdCuenta()) &&
           * anV.getImprecio().compareTo(anC.getImprecio()) == 0) {
           * 
           * anC.setImefectivo(anC.getImefectivo().add( anV.getImefectivo()));
           * anC.setNutitulos(anC.getNutitulos().add( anV.getNutitulos())); } }
           */

      }
      if (cazado) {
        cazado = false;
        continue;
      }
      // if ( anC.getNutitulos().floatValue() > 0F ) {
      if (!checkIfExistObjectInDTOList(anC, resultado)) {
        List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(anC.getCdisin());
        Tmct0Valores valor = (valores != null && valores.size() > 0) ? valores.get(0) : null;
        if (valor != null) {
          anC.setIsinDescripcion(valor.getDescripcion());
        }
        // Si solo hay una linea y es venta, los titulos son
        // negativos
        if (anC.getCdsentido() == 'V') {
          anC.setNutitulos(anC.getNutitulos().abs().negate());
          anC.setImefectivo(anC.getImefectivo().abs());
        }
        else {
          anC.setNutitulos(anC.getNutitulos().abs());
          anC.setImefectivo(anC.getImefectivo().abs().negate());
        }
        resultado.add(anC);
      }
      // }
    }

    // List<AnotacionECCDTO> listResultado = agruparPorPrecio(resultado);

    return resultado;
  }

  private AnotacionECCDTO extracDiference(final AnotacionECCDTO compra, final AnotacionECCDTO venta,
      boolean isSentidoVenta) {
    final AnotacionECCDTO dif = new AnotacionECCDTO();
    dif.setCdCuenta(venta.getCdCuenta());
    dif.setCdisin(venta.getCdisin());

    dif.setCodigoCuentaCompensacion(venta.getCodigoCuentaCompensacion());
    // NEW
    dif.setImprecio(venta.getImprecio());
    // el objeto diferencia tendra los titulos de
    // compra
    // y los efectivos de venta
    BigDecimal titulos = compra.getNutitulos().subtract(venta.getNutitulos());
    // calcula diferencia de efectivos
    BigDecimal difEfectivos = compra.getImefectivo().subtract(venta.getImefectivo()).abs();
    // si es sentido venta, los titulos son negativos porque salen
    // en cambio el efectivo es positivo porque entra
    if (isSentidoVenta) {
      titulos = titulos.abs().negate();
      difEfectivos = difEfectivos.abs();
    }
    else {
      difEfectivos = difEfectivos.abs().negate();
      titulos = titulos.abs();
    }
    dif.setNutitulos(titulos);
    dif.setImefectivo(difEfectivos);
    char sentdio = (isSentidoVenta) ? 'V' : 'C';
    dif.setCdsentido(sentdio);

    return dif;
  }

  // private List<AnotacionECCDTO> agruparPorPrecio(List<AnotacionECCDTO>
  // resultado) {
  // //ORDENAMOS RESULTADO POR EL ISIN
  // // Collections.sort(resultado, new Comparator<AnotacionECCDTO>() {
  // // @Override
  // // public int compare(AnotacionECCDTO p1, AnotacionECCDTO p2) {
  // // return new Integer(p1.getCdisin()).compareTo(new
  // Integer(p2.getCdisin()));
  // // }
  // // });
  //
  // //RECORRO LOS RESULTADOS
  // for(int x=0; x < resultado.size(); x++){
  //
  // int nextPos = x+1;
  //
  // AnotacionECCDTO first = resultado.get(x);
  // AnotacionECCDTO next = resultado.get(nextPos);
  //
  // if(first.getCdisin() == next.getCdisin() && first.getImprecio() ==
  // next.getImprecio()){
  //
  // BigDecimal b = first.getNutitulos();
  // BigDecimal v = next.getNutitulos();
  //
  //
  // first.setNutitulos(b.add(v));
  // resultado.remove(nextPos);
  // }
  // }
  // return resultado;
  // }

  private boolean checkIfExistObjectInDTOList(AnotacionECCDTO dif, List<AnotacionECCDTO> resultado) {
    boolean result = false;
    for (int i = 0; i < resultado.size(); i++) {
      AnotacionECCDTO dto = resultado.get(i);
      if (dto.getCdisin().equals(dif.getCdisin()) && dto.getCdCuenta().equals(dif.getCdCuenta())
          && dto.getImprecio().compareTo(dif.getImprecio()) == 0) {
        result = true;
        break;
      }
    }
    return result;
  }

  private Map<String, Object> getMovimientosCuentaVirtualPropia(Map<String, String> filtros) {
    Map<String, Object> resultMap = new HashMap<String, Object>();
    Map<String, Serializable> normalizedFilters = new HashMap<String, Serializable>();
    if (filtros != null) {
      normalizedFilters = util.normalizeFilters(filtros);
    }
    List<MovimientoCuentaVirtualData> resultList = movBo.findByIdCuentaGroupByIsinAndSentidoAndFContratacion(
        idCuentaPropia, normalizedFilters);
    List<MovimientoCuentaVirtualDTO> dtoList = util.movimientosToDTO(resultList);
    List<MovimientoCuentaVirtualDTO> neteados = netearMovimientos(dtoList, resultList);
    resultMap.put(RESULT_KEY_MOVIMIENTOS_CUENTA_PROPIA, neteados);
    return resultMap;
  }

  private List<MovimientoCuentaVirtualDTO> netearMovimientos(List<MovimientoCuentaVirtualDTO> dtoList,
      List<MovimientoCuentaVirtualData> entities) {
    List<MovimientoCuentaVirtualDTO> neteados = new ArrayList<MovimientoCuentaVirtualDTO>();
    for (int i = 0; i < entities.size(); i++) {
      MovimientoCuentaVirtualData object = entities.get(i);
      MovimientoCuentaVirtual entity = (MovimientoCuentaVirtual) object.getMovimientoCuentaVirtual();
      // compara registros por pares, una vez comprobados
      // si el registro posterior no es el mismo con diferente
      // sentido, se avanza 1 posicion, si si se avanzan 2
      // pero antes se netea
      for (int z = i + 1; z < dtoList.size();) {
        MovimientoCuentaVirtualDTO dto = dtoList.get(z);
        // SE AÑADEN LAS DOS LINEAS A LA LISTA SIN NETEAR
        /*
         * MovimientoCuentaVirtualDTO entryDTO = util.entityToDTO(entity);
         * if(!neteados.contains(entryDTO)){ neteados.add(entryDTO); }
         * 
         * 
         * int index = ((i - 1) >= 0) ? i - 1 : 0; if (index > 0) {
         * MovimientoCuentaVirtual auxEntity = (MovimientoCuentaVirtual)
         * entities .get(index)[0]; MovimientoCuentaVirtualDTO auxDTO = util
         * .entityToDTO(auxEntity); if(!neteados.contains(auxDTO)){
         * neteados.add(auxDTO); } }
         */
        // si el objeto actual que es el siguiente en la lista, es igual
        // que el anterior en la lista pero con sentido contrario, se
        // netea
        // y se avanza dos posiciones en el for principal.
        String entityTradeDate = util.parseDateToString(entity.getTradedate());
        if (entity.getIsin().equals(dto.getIsin()) && entityTradeDate.equals(dto.getTradedate())
            && entity.getCamara().equals(dto.getCamara())) {
          if (dto.getSentidocuentavirtual() != entity.getSentidocuentavirtual()) {
            BigDecimal titV = BigDecimal.ZERO;
            BigDecimal titC = BigDecimal.ZERO;
            BigDecimal efV = BigDecimal.ZERO;
            BigDecimal efC = BigDecimal.ZERO;
            char sentidoDTO = dto.getSentidocuentavirtual();
            // si el objeto dto del for actual es de sentido compra
            // C
            // se rellenan los titulos y efectivos de acuerdo al
            // sentido
            // de los dos objetos comparados entity(V) y dto(C)
            if (sentidoDTO == 'C') {
              titC = dto.getTitulos();
              efC = dto.getEfectivo();
              titV = entity.getTitulos();
              efV = entity.getEfectivo();
            }
            else {
              titV = entity.getTitulos();
              efV = entity.getEfectivo();
              titC = dto.getTitulos();
              efC = dto.getEfectivo();
            }
            BigDecimal titResult;
            BigDecimal efResult;

            // el sentidocuentavirtual siempre es M porque no puede
            // salir mas de lo que entra.

            // el resultado debe ser positivo por tanto
            // se saca el valor absoluto
            efResult = efV.subtract(efC).abs();
            titResult = titV.subtract(titC).abs();
            // se modifica el registro y se añade a la lista
            dto.setEfectivo(efResult);
            dto.setTitulos(titResult);
            // dto.setSentidocuentavirtual(sentido);
            dto.setSentidocuentavirtual('M');
            neteados.add(dto);
            // avanza for principal 2 posiciones
            break;
          }

        }
        z += 1;
      }
    }
    return neteados;
  }

  private Map<String, Object> getMovimientosFicheroNorma43(Map<String, String> filtros) {
    Map<String, Object> resultMap = new HashMap<String, Object>();
    Map<String, Serializable> params = util.normalizeFilters(filtros);
    List<Norma43Movimiento> entities = norma43MovimientoBo.findByIdCuentaGroupByTradeDateOrderByImporteDesc(
        codigoCuentaPropia, params);
    List<Norma43MovimientoDTO> resultDTOList = new LinkedList<Norma43MovimientoDTO>();
    if (entities != null) {
      resultDTOList.addAll(util.entitiesToDTO(entities));
      resultMap.put(RESULT_KEY_MOVIMIENTO_NORMA_43, resultDTOList);
    }
    return resultMap;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new LinkedList<String>();
    commands.add(CMD_GET_ANOTACIONESECC_BY_TIPO_CUENTA);
    commands.add(CMD_GET_MOVIMIENTOS_CUENTA_VIRTUAL_PROPIA);
    commands.add(CMD_GET_MOVIMIENTOS_FICHERO_N43);
    return commands;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

}
