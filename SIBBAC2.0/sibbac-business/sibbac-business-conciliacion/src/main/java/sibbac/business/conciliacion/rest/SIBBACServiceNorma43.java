package sibbac.business.conciliacion.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43DescuadresDaoImpl;
import sibbac.business.conciliacion.database.dao.Tmct0CuentaBancariaDao;
import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.model.Tmct0CuentaBancaria;
import sibbac.business.conciliacion.util.CuentaBancariaUtil;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceNorma43 implements SIBBACServiceBean {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceNorma43.class);

  /** Lista de comandos disponibles para el servicio. */
  private static final List<String> AVAILABLE_COMMANDS = new ArrayList<String>();

  /** Comando de inicialización de la página de descuadres. */
  private static final String COMMAND_INIT_DESCUADRES = "initDescuadres";

  /** Comando de búsqueda de descuadres. */
  private static final String COMMAND_SEARCH_DESCUADRES = "searchDescuadres";

  /** Mapa de campos a inicializar en la carga de la página. */
  private static final Map<String, Object> INIT_FIELDS_VALUES = new HashMap<String, Object>();

  /** Parámetro que contiene la lista de datos de inicialización del formulario de búsqueda de descuadres. */
  private static final String PARAM_INIT_DATA = "initData";

  /** Parámetro que contiene la lista de descuadres encontrada. */
  private static final String PARAM_DESCUADRES_LIST = "descuadresList";

  /** Parámetro que contiene la lista de cuentas. */
  private static final String PARAM_CUENTAS_LIST = "cuentas";

  /** Parámetro que contiene la lista de sentidos. */
  private static final String PARAM_SENTIDO_LIST = "sentido";

  /** Parámetro que contiene la lista de referencias orden. */
  private static final String PARAM_REFORDEN_LIST = "refOrden";

  /** Parámetro que contiene la cuenta de las búsquedas de descuadres. */
  private static final String PARAM_CUENTA = "cuenta";

  /** Parámetro que contiene la cuenta de las búsquedas de descuadres. */
  private static final String PARAM_SENTIDO = "sentido";

  /** Parámetro que contiene la cuenta de las búsquedas de descuadres. */
  private static final String PARAM_REFORDEN = "refOrden";

  /** Parámetro que contiene la fecha desde de las búsquedas de descuadres. */
  private static final String PARAM_FECHA_DESDE = "fechaDesde";

  /** Parámetro que contiene la fecha hasta de las búsquedas de descuadres. */
  private static final String PARAM_FECHA_HASTA = "fechaHasta";

  /** Parámetro que contiene la fecha desde de las búsquedas de descuadres. */
  private static final String PARAM_IMPORTE_DESDE = "importeDesde";

  /** Parámetro que contiene la fecha hasta de las búsquedas de descuadres. */
  private static final String PARAM_IMPORTE_HASTA = "importeHasta";

  /** Indica que una operación ha finalizado correctamente. */
  private static final String RESULTADO_SUCCESS = "SUCCESS";

  /** Indica que una operación no ha finalizado correctamente. */
  private static final String RESULTADO_FAIL = "FAIL";

  /** Nombre de la clave que se envia en el mapa de resultados con el resultado de la operación. */
  private static final String RESULTADO = "resultado";

  /** Nombre de la clave que se envia en el mapa de resultados con el mensaje del error producido. */
  private static final String ERROR_MESSAGE = "error_message";

  // /** Formateardor de números. */
  // private static final NumberFormat nF = new DecimalFormat("#0.00", new DecimalFormatSymbols(new Locale("es",
  // "ES")));
  //
  // /** Formateardor de números. */
  // private static final DateFormat dF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>TMCT0_FICHERO_NORMA43</code>. */
  @Autowired
  private Norma43DescuadresDao norma43DescuadresDao;

  /** Business object para la tabla <code>TMCT0_CUENTA_BANCARIA</code>. */
  @Autowired
  private Tmct0CuentaBancariaDao cuentaBancariaDao;

  @Autowired
  private Norma43DescuadresDaoImpl norma43DescuadresImpl;

  CuentaBancariaUtil cuentaBancariaUtil = CuentaBancariaUtil.getInstance();

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLOQUE DE INICIALIZACIÓN ESTÁTICO

  static {
    // Tipos de comandos disponibles para el servicio
    AVAILABLE_COMMANDS.add(COMMAND_INIT_DESCUADRES);
    AVAILABLE_COMMANDS.add(COMMAND_SEARCH_DESCUADRES);
  } // static

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public SIBBACServiceNorma43() {
  } // SIBBACServiceNorma43

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
   */
  @Override
  public WebResponse process(WebRequest webRequest) {
    WebResponse webResponse = new WebResponse();
    String command = webRequest.getAction();
    LOG.debug("[SIBBACServiceNorma43 :: process] Command: " + command);
    try {
      switch (command) {
        case COMMAND_INIT_DESCUADRES:
          webResponse.setResultados(initDescuadresFields());
          break;
        case COMMAND_SEARCH_DESCUADRES:
          webResponse.setResultados(searchDescuadres(webRequest.getFilters()));
          break;
        default:
          webResponse.setError("A valid action must be set. Valid actions are: " + this.getAvailableCommands());
      } // switch
    } catch (Exception e) {
      LOG.debug("[SIBBACServiceNorma43 :: process] Error: " + e);
      LOG.debug("[SIBBACServiceNorma43 :: process] Error Message: " + e.getMessage());

      Map<String, Object> resultado = new HashMap<String, Object>();
      resultado.put(RESULTADO, RESULTADO_FAIL);
      resultado.put(ERROR_MESSAGE, e.getMessage());

      webResponse.setResultados(resultado);
    } // catch

    return webResponse;
  } // process

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    return AVAILABLE_COMMANDS;
  } // getAvailableCommands

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  @Override
  public List<String> getFields() {
    return null;
  } // getFields

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Devuelve un mapa con los datos de inicialización de los campos del formulario.<br>
   * 
   * @return <code>Map<String, Object> - </code>Datos para la inicialización de los campos del formulario. El nombre del
   *         objeto que devuelve es <code>inicializationData</code>.
   */
  private Map<String, Object> initDescuadresFields() {
    LOG.debug("Inicializando campos del formulario ...");

    List<String> listaCuentas = new ArrayList<String>();
    List<String> listaCuentasIni = norma43DescuadresDao.findDistinctCuenta();

    if (listaCuentasIni.isEmpty()) {
      listaCuentas.add(":: Ningún descuadre en la base de datos ::");
    } else {
      // Se obtiene la descripción de la cuenta bancaria a través del IBAN
      for (Iterator iterator = listaCuentasIni.iterator(); iterator.hasNext();) {
        String cuentaDes43 = (String) iterator.next();

        // Se extrae la entidad
        String entidad = cuentaDes43.substring(0, 4);
        // Se extrae la sucursal
        String sucursal = cuentaDes43.substring(4, 8);
        // Se extrae la cuenta
        int cuentaDes43Len = cuentaDes43.length();
        String cuenta = cuentaDes43.substring(cuentaDes43Len - 10, cuentaDes43Len);

        // Se calcula el iban de la cuenta obtenida
        String iban = cuentaBancariaUtil.getIban("ES", Integer.parseInt(entidad), Integer.parseInt(sucursal),
                                                 Long.parseLong(cuenta));
        Tmct0CuentaBancaria cuentaBancaria = cuentaBancariaDao.findByIban(iban);
        if (cuentaBancaria != null) {
          listaCuentas.add(cuentaDes43 + " | " + cuentaBancaria.getDescCorta());
        } else {
          listaCuentas.add(cuentaDes43);
        }
      }
    }
    INIT_FIELDS_VALUES.put(PARAM_CUENTAS_LIST, listaCuentas);

    List<String> listaSentido = norma43DescuadresDao.findDistinctSentido();

    if (listaSentido.isEmpty()) {
      listaSentido.add(":: Ningún descuadre en la base de datos ::");
    } // if
    INIT_FIELDS_VALUES.put(PARAM_SENTIDO_LIST, listaSentido);

    List<String> listaRefOrden = norma43DescuadresDao.findDistinctRefOrden();

    if (listaRefOrden.isEmpty()) {
      listaRefOrden.add(":: Ningún descuadre en la base de datos ::");
    } // if
    INIT_FIELDS_VALUES.put(PARAM_REFORDEN_LIST, listaRefOrden);

    Map<String, Object> initData = new HashMap<String, Object>();
    initData.put(RESULTADO, RESULTADO_SUCCESS);
    initData.put(PARAM_INIT_DATA, INIT_FIELDS_VALUES);

    return initData;
  } // initDescuadresFields

  /**
   * Busca los descuadres Norma 43 que encagen con los criterios de búsqueda especificados.
   * 
   * @param filters Filtros para la búsqueda de descuadres.
   * @return Mapa de resultados.
   * @throws Exception Si ocurre algún error.
   */
  private Map<String, Object> searchDescuadres(Map<String, String> filters) throws Exception {
    LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] Buscando descuadres ...");

    Map<String, Object> resultado = null;
    Date fechaDesde;
    Date fechahasta;
    String importeDesde;
    String importeHasta;

    if (MapUtils.isNotEmpty(filters)) {
      LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] Filter fecha desde: " + filters.get(PARAM_FECHA_DESDE));
      if (filters.get(PARAM_FECHA_DESDE) == "") {
        fechaDesde = FormatDataUtils.convertStringToDate("01/01/2016", "dd/MM/yyyy");
      } else {
        fechaDesde = FormatDataUtils.convertStringToDate(filters.get(PARAM_FECHA_DESDE), "dd/MM/yyyy");
      }
      LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] Fecha desde formateada: " + fechaDesde);

      LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] Filter fecha desde: " + filters.get(PARAM_FECHA_HASTA));
      if (filters.get(PARAM_FECHA_HASTA) == "") {
        fechahasta = new Date();
      } else {
        fechahasta = FormatDataUtils.convertStringToDate(filters.get(PARAM_FECHA_HASTA), "dd/MM/yyyy");
      }
      LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] Fecha desde formateada: " + fechahasta);

      LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] Filter importe desde: " + filters.get(PARAM_IMPORTE_DESDE));
      if (filters.get(PARAM_IMPORTE_DESDE) == "") {
        importeDesde = "0";
      } else {
        importeDesde = filters.get(PARAM_IMPORTE_DESDE);
      }
      LOG.debug("[SIBBACServiceNorma43 :: searchDescuadres] importe desde formateado: " + importeDesde);

      List<Norma43DescuadresDTO> listaValores = norma43DescuadresImpl.findAllByCuentaAndBetweenTradeDate(fechaDesde,
                                                                                                         fechahasta,
                                                                                                         filters.get(PARAM_CUENTA),
                                                                                                         filters.get(PARAM_SENTIDO),
                                                                                                         filters.get(PARAM_REFORDEN),
                                                                                                         importeDesde,
                                                                                                         filters.get(PARAM_IMPORTE_HASTA));
      resultado = new HashMap<String, Object>();
      resultado.put(RESULTADO, RESULTADO_SUCCESS);
      resultado.put(PARAM_DESCUADRES_LIST, listaValores);

    } else {
      LOG.error("[SIBBACServiceNorma43 :: add] Filtros no recibidos ...");
      throw new Exception("Filtros no recibidos");
    } // else

    return resultado;
  } // searchDescuadres

} // SIBBACServiceNorma43
