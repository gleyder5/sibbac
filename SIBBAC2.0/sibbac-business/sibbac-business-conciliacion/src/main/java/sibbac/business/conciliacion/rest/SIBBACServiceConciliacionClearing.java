package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.util.MultiValueMap;

import sibbac.business.conciliacion.database.bo.ConciliacionS3Bo;
import sibbac.business.conciliacion.database.bo.MovimientoCuentaVirtualS3Bo;
import sibbac.business.conciliacion.database.bo.Tmct0DiferenciaConciliacionEfectivoBO;
import sibbac.business.conciliacion.database.bo.Tmct0DiferenciaConciliacionTituloBO;
import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData;
import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualS3Data;
import sibbac.business.conciliacion.database.dto.DiferenciaConciliacionEfectivoDTO;
import sibbac.business.conciliacion.database.dto.DiferenciaConciliacionTituloDTO;
import sibbac.business.conciliacion.database.dto.MovimientoCuentaVirtualDTO;
import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionEfectivo;
import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionTitulo;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0SaldoInicialCuentaBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.data.SaldoInicial;
import sibbac.business.wrappers.database.data.SaldoInicialIsin;
import sibbac.business.wrappers.database.data.SaldoInicialPorCliente;
import sibbac.business.wrappers.database.data.SaldoInicialPorIsin;
import sibbac.business.wrappers.database.data.Tmct0desgloseCamaraData;
import sibbac.business.wrappers.database.data.TuplaIsinAlias;
import sibbac.business.wrappers.database.dto.ConciliacionERDTO;
import sibbac.business.wrappers.database.dto.CuentaLiquidacionDTO;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;
import sibbac.business.wrappers.database.model.Tmct0SaldoInicialCuenta;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
@SuppressWarnings("unchecked")
public class SIBBACServiceConciliacionClearing implements SIBBACServiceBean {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceConciliacionClearing.class);
  // Util
  private ConciliacionUtil util = ConciliacionUtil.getInstance();
  @Autowired
  private Tmct0DiferenciaConciliacionEfectivoBO diferenciaEfectivoBO;
  @Autowired
  private Tmct0DiferenciaConciliacionTituloBO diferenciaTituloBO;
  @Autowired
  private MovimientoCuentaVirtualS3Bo movCVS3Bo;
  @Autowired
  private Tmct0CuentasDeCompensacionBo ccBo;
  @Autowired
  private Tmct0CuentaLiquidacionBo cuentaLiquidacionBo;
  @Autowired
  private Tmct0desgloseCamaraBo desgloseCamaraBo;
  @Autowired
  private GestorPeriodicidades gestorPeriodicidades;
  @Autowired
  private Tmct0SaldoInicialCuentaBo saldoBo;
  @Autowired
  private Tmct0MovimientoCuentaAliasBo movCuentaAliasBo;
  @Autowired
  private Tmct0ValoresBo valoresBo;
  @Autowired
  private Tmct0aliBo aliasBo;
  @Autowired
  private ConciliacionS3Bo conciliacionS3Bo;

  protected static final String TAG = "SIBBACServiceConciliacionClearing";

  public static final String KEY_COMMAND = "command";

  // Consulta
  private static final String CMD_GET_DIFERENCIAS_TITULO = "getDiferenciasTitulo";
  private static final String CMD_GET_DIFERENCIAS_EFECTIVO = "getDiferenciasEfectivo";
  private static final String CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL = "getBalanceMovimientosCuentaVirtual";
  private static final String CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL_ISIN = "getBalanceMovimientosCuentaVirtualPorIsin";
  private static final String CMD_GET_DIFERENCIAS_S3 = "getDiferenciasS3";
  private static final String CMD_GET_CUENTAS_LIQUIDACION = "getCuentasLiquidacion";
  private static final String CMD_GET_DESGLOSE_CAMARA_BY_SENTIDO_CUENTAS_CLEARING = "getDesgloseCamaraBySentidoCuentasClearing";
  private static final String CMD_GET_CUENTAS_LIQUIDACION_CLEARING = "getCuentasLiquidacionClearing";
  private static final String CMD_GET_CUENTAS_LIQUIDACION_CLEARING_NO_CP_NO_CI = "getCuentasLiquidacionClearingNoPropiaNoIndividual";
  private static final String CMD_GET_FLIQUIDACION_BETWEEN = "getInitialFliquidacionBetween";
  private static final String CMD_GET_MOV_UNIFICADO_EFECTIVO = "getMovimientoUnificadoEfectivo";
  private static final String CMD_GET_CONCILIACION_TITULOS_ER = "getConciliacionTitulosER";
  private static final String CMD_GET_CONCILIACION_EFECTIVO_ER = "getConciliacionEfectivoER";
  private static final String CMD_GET_CUENTAS_LIQUIDACION_FILTRADO = "getCuentasLiquidacionFiltrado";

  // Parametros
  public static final String PRM_TRADE_DATE = "tradeDate";
  public static final String PRM_ID_CUENTA_LIQ = "idCuentaLiq";

  // Resultados
  public static final String RESULT_DIFERENCIAS_TITULO = "result_diferencias_titulo";
  public static final String RESULT_DIFERENCIAS_EFECTIVO = "result_diferencias_efectivo";
  public static final String RESULT_BALANCE_CLEARING = "result_balance_clearing";
  public static final String RESULT_BALANCE_CLEARING_ISIN = "result_balance_clearing_isin";
  private static final String RESTUL_PROVISION_EFECTIVO = "result_provision_efectivo";
  private static final String RESULT_CUENTAS_LIQUIDACION_CLEARING = "result_cuentas_liquidacion_clearing";
  private static final String RESULT_KEY_INITIAL_FCONTRATACION_BETWEEN = "result_fcontratacion_between";
  private static final String RESULT_MOV_UNIFICADOS_EFECTIVO = "result_mov_unificados_efectivo";
  // PROPIEDADES
  private static final String CONCILIACION_PROVISION_DIAS_RESTAR_FLIQUIDACION_DE = "conciliacion.provision.dias.restar.fliquidacion.de";
  private static final String CONCILIACION_PROVISION_DIAS_RESTAR_FLIQUIDACION_A = "conciliacion.provision.dias.restar.fliquidacion.a";

  @Value("${" + CONCILIACION_PROVISION_DIAS_RESTAR_FLIQUIDACION_DE + "}")
  private int diasARestarFechaDe;

  @Value("${" + CONCILIACION_PROVISION_DIAS_RESTAR_FLIQUIDACION_A + "}")
  private int diasARestarFechaA;

  @Value("${conciliacion.codigo.operacion.compra}")
  private String operacionCompra;

  @Value("${conciliacion.codigo.operacion.venta}")
  private String operacionVenta;

  public SIBBACServiceConciliacionClearing() {
  }

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    List<ConciliacionERDTO> movsConciliacionER;

    String prefix = "[SIBBACConciliacionClearingService::process] ";
    LOG.debug(prefix + "Processing a web request...");
    // prepara webResponse
    WebResponse webResponse = new WebResponse();
    // extrae parametros del httpRequest
    Map<String, String> params = webRequest.getFilters();
    // Este sera el mapa de filtros que se manda al dao extraido de los
    // filtros
    Map<String, Serializable> parametros = null;
    if (params != null) {
      parametros = util.normalizeFilters(params);
    }
    // Resultado
    Map<String, Object> resultMap = new HashMap<String, Object>();
    // obtiene comandos(paths)
    String command = webRequest.getAction();
    LOG.debug(prefix + "+ Command.: [{}]", command);
    // Se prepara el tradeDate por defecto para que extraiga resultados
    // desde la fecha actual menos 60 dias
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -60);
    Date tradeDate = cal.getTime();
    switch (command) {
      case CMD_GET_DIFERENCIAS_EFECTIVO:
        if (params.get(PRM_TRADE_DATE) != null) {
          String strTradeDate = params.get(PRM_TRADE_DATE).trim();
          try {
            tradeDate = FormatDataUtils.convertStringToDate(strTradeDate);
          }
          catch (Exception e) {
            webResponse.setError(e.getMessage());
            LOG.error(e.getMessage(), e);
            return webResponse;
          }
        }
        List<DiferenciaConciliacionEfectivoDTO> listaDifEfectivo = util
            .entityEfListToDTO(findDiferenciasEfectivoByTradeDate(tradeDate));
        Map<String, Object> resultMapEf = new HashMap<String, Object>();
        resultMapEf.put(RESULT_DIFERENCIAS_EFECTIVO, listaDifEfectivo);
        webResponse.setResultados(resultMapEf);
        break;
      case CMD_GET_DIFERENCIAS_TITULO:
        if (params.get(PRM_TRADE_DATE) != null) {
          String strTradeDate = params.get(PRM_TRADE_DATE).trim();
          try {
            tradeDate = FormatDataUtils.convertStringToDate(strTradeDate);
          }
          catch (Exception e) {
            LOG.error(e.getMessage(), e);
            webResponse.setError("Error al convertir el parámetro " + strTradeDate + " a fecha");
          }
        }
        List<DiferenciaConciliacionTituloDTO> listaDifTitulo = util
            .entityTTListToDTO(findDiferenciasTitulosByTradeDate(tradeDate));
        Map<String, Object> resultMapTt = new HashMap<String, Object>();
        resultMapTt.put(RESULT_DIFERENCIAS_TITULO, listaDifTitulo);
        webResponse.setResultados(resultMapTt);
        break;
      // Balance agrupado por Cliente
      case CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL:
        List<SaldoInicialPorCliente> balancePorCliente = new LinkedList<SaldoInicialPorCliente>();
        try {
          balancePorCliente = getSaldoInicialPorCliente(parametros);
        }
        catch (NoSuchMethodError ex) {
          LOG.error(ex.getMessage(), ex);
          webResponse.setError(ex.getMessage());
        }
        resultMap.put(RESULT_BALANCE_CLEARING, balancePorCliente);
        webResponse.setResultados(resultMap);
        break;
      // Balance agrupado por ISIN
      case CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL_ISIN:
        List<SaldoInicialPorIsin> balancePorIsin = getSaldoInicialPorIsin(parametros);
        resultMap.put(RESULT_BALANCE_CLEARING, balancePorIsin);
        webResponse.setResultados(resultMap);
        break;
      case CMD_GET_DIFERENCIAS_S3:
        LOG.debug(prefix + "Inside: " + CMD_GET_DIFERENCIAS_S3);
        Map<String, Serializable> parametrosDiferenciasS3 = new HashMap<String, Serializable>();
        Long idCuentaLiq = new Long(0);
        String codCuentaLiq = null;
        if (params.get(PRM_ID_CUENTA_LIQ) != null) {
          idCuentaLiq = Long.parseLong(params.get(PRM_ID_CUENTA_LIQ));
          codCuentaLiq = cuentaLiquidacionBo.findById(idCuentaLiq).getCdCodigo().trim();
        }
        else {
          LOG.debug(prefix + " " + PRM_ID_CUENTA_LIQ + " not indicated");
        }
        parametrosDiferenciasS3 = util.normalizeFilters(params);
        util.normalizeDates(parametrosDiferenciasS3);
        Date fechaDesde = (Date) parametrosDiferenciasS3.get("fliquidacionDe");
        Date fechaHasta = (Date) parametrosDiferenciasS3.get("fliquidacionA");
        String isinFiltro = (String) parametrosDiferenciasS3.get("isin");
        List<MovimientoCuentaVirtualData> movimientosByCuenta = new ArrayList<MovimientoCuentaVirtualData>();
        List<MovimientoCuentaAliasData> movsAliasData = new ArrayList<MovimientoCuentaAliasData>();

        Map<String, Object> isinDesc = new HashMap<String, Object>();

        // Traemos los movimientos cuenta alias segun los filtros para el
        // lado izquierdo
        if (isinFiltro == null || isinFiltro.equals("")) {
          movsAliasData = movCuentaAliasBo
              .findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinalAndTitulosNotZero(fechaDesde, fechaHasta,
                  codCuentaLiq);
        }
        else {
          movsAliasData = movCuentaAliasBo
              .findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinalAndTitulosNotZero(fechaDesde,
                  fechaHasta, codCuentaLiq, isinFiltro);
        }
        for (MovimientoCuentaAliasData mov : movsAliasData) {
          MovimientoCuentaVirtualData toInsert = new MovimientoCuentaVirtualData(mov.getMovimientoCuentaAlias()
              .getIsin(), mov.getMovimientoCuentaAlias().getTitulos(), mov.getMovimientoCuentaAlias()
              .getCdcodigocuentaliq(), new java.sql.Date(mov.getMovimientoCuentaAlias().getSettlementdate().getTime()));
          toInsert.setDescripcionisin(mov.getDescrIsin());
          toInsert.setSistemaLiq(mov.getMovimientoCuentaAlias().getSistemaLiquidacion());
          movimientosByCuenta.add(toInsert);
        }
        // Fin Cuenta alias
        List<MovimientoCuentaVirtualDTO> movimientosByCuentaDTO = util.movimientosToDTO(movimientosByCuenta);
        List<MovimientoCuentaVirtualS3Data> movimientosByCuentaS3 = movCVS3Bo
            .findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion(idCuentaLiq, parametrosDiferenciasS3);

        webResponse.setResultados(compareVirtualWithVirtualS3(movimientosByCuentaDTO, movimientosByCuentaS3,
            (String) params.get("cuadrados")));
        break;
      case CMD_GET_CUENTAS_LIQUIDACION:
        LOG.debug(prefix + "Inside: " + CMD_GET_CUENTAS_LIQUIDACION);
        List<Map<String, String>> cuentasLiquidacion = new LinkedList<Map<String, String>>();
        try {
          cuentasLiquidacion.addAll(getCuentasLiquidacion());
        }
        catch (PersistenceException | JpaSystemException ex) {
          LOG.error(ex.getMessage(), ex);
          webResponse.setError(ex.getMessage());
        }
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("cuentasLiquidacion", cuentasLiquidacion);
        webResponse.setResultados(result);
        break;
      case CMD_GET_DESGLOSE_CAMARA_BY_SENTIDO_CUENTAS_CLEARING:
        List<Tmct0desgloseCamaraData> desgloses = getDesgloseCamaraBySentidoCuentasClearing(params);
        resultMap = new HashMap<String, Object>();
        resultMap.put(RESTUL_PROVISION_EFECTIVO, desgloses);
        webResponse.setResultados(resultMap);
        break;
      case CMD_GET_CUENTAS_LIQUIDACION_CLEARING:
        List<CuentaLiquidacionDTO> cuentas = getCuentasLiquidacionClearing();
        resultMap = new HashMap<String, Object>();
        resultMap.put(RESULT_CUENTAS_LIQUIDACION_CLEARING, cuentas);
        webResponse.setResultados(resultMap);
        break;
      case CMD_GET_CUENTAS_LIQUIDACION_CLEARING_NO_CP_NO_CI:
        List<CuentaLiquidacionDTO> cuentasNoPropiasNoIndividuales = getCuentasLiquidacionClearingNoPropiaNoIndividuales();
        resultMap = new HashMap<String, Object>();
        resultMap.put(RESULT_CUENTAS_LIQUIDACION_CLEARING, cuentasNoPropiasNoIndividuales);
        webResponse.setResultados(resultMap);
        break;
      case CMD_GET_FLIQUIDACION_BETWEEN:
        Map<String, String> fechas = getInitialFliquidacionBetween();
        resultMap = new HashMap<String, Object>();
        resultMap.put(RESULT_KEY_INITIAL_FCONTRATACION_BETWEEN, fechas);
        webResponse.setResultados(resultMap);
        break;

      case CMD_GET_MOV_UNIFICADO_EFECTIVO:
        parametros.put("operacionCompra", operacionCompra);
        parametros.put("operacionVenta", operacionVenta);
        List<Tmct0CuentaLiquidacion> cuentasLiq = cuentaLiquidacionBo
            .findAllByCuentaDeCompensacionIsPropiaOrIndividual();
        String codigos = "(";
        for (Tmct0CuentaLiquidacion cuentaLiq : cuentasLiq) {
          if (codigos.equals("(")) {
            codigos = codigos + "'" + cuentaLiq.getCdCodigo() + "'";
          }
          else {
            codigos = codigos + ",'" + cuentaLiq.getCdCodigo() + "'";
          }
        }
        codigos = codigos + ") ";
        parametros.put("cuenta", codigos);
        List<?> list = movCuentaAliasBo
            .findAllSumByIsinAndCuentaLiquidacionAndFechaGroupByIsinAndCuentaLiquidacion(parametros);
        resultMap = new HashMap<String, Object>();
        resultMap.put(RESULT_MOV_UNIFICADOS_EFECTIVO, list);
        webResponse.setResultados(resultMap);
        break;

      case CMD_GET_CONCILIACION_TITULOS_ER:
        LOG.debug("{} Inside: {}", prefix, CMD_GET_CONCILIACION_TITULOS_ER);
        codCuentaLiq = null;
        if (params.get(PRM_ID_CUENTA_LIQ) != null) {
          idCuentaLiq = Long.parseLong(params.get(PRM_ID_CUENTA_LIQ));
          codCuentaLiq = cuentaLiquidacionBo.findById(idCuentaLiq).getCdCodigo().trim();
        }
        else {
          LOG.debug(prefix + " " + PRM_ID_CUENTA_LIQ + " not indicated");
        }
        parametrosDiferenciasS3 = util.normalizeFilters(params);
        util.normalizeDates(parametrosDiferenciasS3);
        fechaDesde = (Date) parametrosDiferenciasS3.get("fliquidacionDe");
        isinFiltro = (String) parametrosDiferenciasS3.get("isin");
        isinDesc = new HashMap<String, Object>();
        movsConciliacionER = conciliacionS3Bo.conciliacionDeTitulos(fechaDesde, codCuentaLiq, isinFiltro,
            (String) params.get("cuadrados"));
        isinDesc.put(WebResponse.KEY_DATA, movsConciliacionER);
        webResponse.setResultados(isinDesc);
        break;
      case CMD_GET_CONCILIACION_EFECTIVO_ER:
        LOG.debug("{} Inside: {}", prefix, CMD_GET_CONCILIACION_EFECTIVO_ER);
        codCuentaLiq = null;
        if (params.get(PRM_ID_CUENTA_LIQ) != null) {
          idCuentaLiq = Long.parseLong(params.get(PRM_ID_CUENTA_LIQ));
          codCuentaLiq = cuentaLiquidacionBo.findById(idCuentaLiq).getCdCodigo().trim();
        }
        else {
          LOG.debug(prefix + " " + PRM_ID_CUENTA_LIQ + " not indicated");
        }
        parametrosDiferenciasS3 = util.normalizeFilters(params);
        util.normalizeDates(parametrosDiferenciasS3);
        fechaDesde = (Date) parametrosDiferenciasS3.get("fliquidacionDe");
        isinFiltro = (String) parametrosDiferenciasS3.get("isin");
        isinDesc = new HashMap<String, Object>();
        movsConciliacionER = conciliacionS3Bo.conciliacionDeEfectivo(fechaDesde, codCuentaLiq, isinFiltro,
            (String) params.get("cuadrados"));
        isinDesc.put(WebResponse.KEY_DATA, movsConciliacionER);
        webResponse.setResultados(isinDesc);

        break;
      case CMD_GET_CUENTAS_LIQUIDACION_FILTRADO:
        List<String> cuentasLiquidacionFiltrado = getCuentasDeCompensacionFiltrado(parametros);
        resultMap = new HashMap<>();
        resultMap.put("cuentasLiquidacion", cuentasLiquidacionFiltrado);
        webResponse.setResultados(resultMap);
        break;
    }

    return webResponse;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL);
    commands.add(CMD_GET_DESGLOSE_CAMARA_BY_SENTIDO_CUENTAS_CLEARING);
    commands.add(CMD_GET_CUENTAS_LIQUIDACION_CLEARING);
    commands.add(CMD_GET_FLIQUIDACION_BETWEEN);
    commands.add(CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL_ISIN);
    commands.add(CMD_GET_DIFERENCIAS_TITULO);
    commands.add(CMD_GET_DIFERENCIAS_EFECTIVO);
    commands.add(CMD_GET_DIFERENCIAS_S3);
    commands.add(CMD_GET_CUENTAS_LIQUIDACION);
    commands.add(CMD_GET_MOV_UNIFICADO_EFECTIVO);
    commands.add(CMD_GET_CONCILIACION_TITULOS_ER);
    commands.add(CMD_GET_CUENTAS_LIQUIDACION_FILTRADO);

    return commands;
  }

  @Override
  public List<String> getFields() {
    List<String> fields = new LinkedList<String>();
    fields.add("tradeDate");
    return fields;
  }

  /**
   * 
   * @param filtros
   * @return List<SaldoInicialPorCliente>
   */
  private List<SaldoInicialPorCliente> getSaldoInicialPorCliente(Map<String, Serializable> filtros)
      throws NoSuchMethodError {
    //
    boolean incluirMovs = false;

    String alias = null;
    if (filtros.get("alias") != null) {
      alias = (String) filtros.get("alias");
    }
    String isin = null;
    if (filtros.get("isin") != null) {
      isin = (String) filtros.get("isin");
    }
    Date fecha = new Date();
    if (filtros.get("fecha") != null) {
      fecha = (Date) filtros.get("fecha");
    }
    String cdcodigocuentaliq = null;
    if (filtros.get("cdcodigocuentaliq") != null) {
      cdcodigocuentaliq = (String) filtros.get("cdcodigocuentaliq");
    }

    List<TuplaIsinAlias> isinList = new ArrayList<TuplaIsinAlias>();
    List<TuplaIsinAlias> isinIndependienteList = new ArrayList<TuplaIsinAlias>();

    Date now = new Date();
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    try {
      now = formatter.parse(formatter.format(now));
      fecha = formatter.parse(formatter.format(fecha));
    }
    catch (ParseException e) {
      LOG.error("[getSaldoInicialPorcliente] Error en el parseo de fecha", e);
    }

    // Dependiendo de los parametros que se reciban de los filtros se
    // obtienen los alias e isines a buscar
    isinList = getTuplas(alias, isin, cdcodigocuentaliq, now);
    if (fecha.equals(now)) {
      isinIndependienteList = getTuplasIndependientes(alias, isin, cdcodigocuentaliq, now);
    }
    List<SaldoInicialPorCliente> dtoList = new ArrayList<SaldoInicialPorCliente>();

    // Dependiendo de la fecha hay distintos casos
    if (fecha.before(now)) {
      // Caso en el que se muestran los saldos iniciales
      dtoList = traerPorCliente(isinList, incluirMovs, fecha, cdcodigocuentaliq);
    }
    else if (fecha.equals(now)) {
      // Caso en el que se muestran los saldos iniciales y movimientos que
      // iran a esos saldos
      incluirMovs = true;
      dtoList = traerPorCliente(isinList, incluirMovs, fecha, cdcodigocuentaliq);
      if (isinIndependienteList != null && isinIndependienteList.size() > 0) {
        // Incluimos aquellos movs que todavia no tienen el saldo
        // generado
        dtoList.addAll(traerIndependientesPorCliente(isinIndependienteList, fecha, cdcodigocuentaliq));
      }
    }
    else if (fecha.after(now)) {
      // Caso en el que se muestran los movimientos que se produciran
      LOG.warn("[SIBBACServiceConciliacionClearing::getSaldoInicialPorCliente] Descartado!!!");
      // dtoList = traerPorClienteFuturo( isinList, fecha );
    }

    return dtoList;
  }

  /**
   * 
   * @param filtros
   * @return List<SaldoInicialPorCliente>
   */
  private List<SaldoInicialPorIsin> getSaldoInicialPorIsin(Map<String, Serializable> filtros) {
    //
    boolean incluirMovs = false;

    String alias = null;
    if (filtros.get("alias") != null) {
      alias = (String) filtros.get("alias");
    }
    String isin = null;
    if (filtros.get("isin") != null) {
      isin = (String) filtros.get("isin");
    }
    String cdcodigocuentaliq = null;
    if (filtros.get("cdcodigocuentaliq") != null) {
      cdcodigocuentaliq = (String) filtros.get("cdcodigocuentaliq");
    }
    Date fecha = new Date();
    if (filtros.get("fecha") != null) {
      fecha = (Date) filtros.get("fecha");
    }
    List<TuplaIsinAlias> isinList = new ArrayList<TuplaIsinAlias>();
    List<TuplaIsinAlias> isinIndependienteList = new ArrayList<TuplaIsinAlias>();

    Date now = new Date();
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    try {
      now = formatter.parse(formatter.format(now));
      fecha = formatter.parse(formatter.format(fecha));
    }
    catch (ParseException e) {
      LOG.warn("Database access problem: {}. Error de parseo: {}", CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL_ISIN,
          e.getMessage());
    }

    // Dependiendo de los parametros que se reciban de los filtros se
    // obtienen los alias e isines a buscar
    isinList = getTuplas(alias, isin, cdcodigocuentaliq, now);

    if (fecha.equals(now)) {
      isinIndependienteList = getTuplasIndependientes(alias, isin, cdcodigocuentaliq, now);
    }

    List<SaldoInicialPorIsin> dtoList = new ArrayList<SaldoInicialPorIsin>();
    // Dependiendo de la fecha hay distintos casos
    if (fecha.before(now)) {
      // Caso en el que se muestran los saldos iniciales
      dtoList = traerPorIsin(isinList, incluirMovs, fecha, cdcodigocuentaliq);
    }
    else if (fecha.equals(now)) {
      // Caso en el que se muestran los saldos iniciales y movimientos que
      // iran a esos saldos
      incluirMovs = true;
      dtoList = traerPorIsin(isinList, incluirMovs, fecha, cdcodigocuentaliq);
      if (isinIndependienteList != null && isinIndependienteList.size() > 0) {
        // Incluimos aquellos movs que todavia no tienen el saldo
        // generado
        dtoList.addAll(traerIndependientesPorIsin(isinIndependienteList, fecha, cdcodigocuentaliq));
      }
    }
    else if (fecha.after(now)) {
      // Caso en el que se muestran los movimientos que se produciran
      LOG.warn("[SIBBACServiceConciliacionClearing::getSaldoInicialPorIsin] Descartado!!!");
      // dtoList = traerPorIsinFuturo( isinList, fecha );
    }

    return dtoList;
  }

  public List<SaldoInicialPorCliente> traerPorCliente(List<TuplaIsinAlias> listaIsins, boolean incluirMovs,
      Date fechaSolicitada, String cdcodigocuentaliq) {

    List<SaldoInicialPorCliente> isinSaldos = new ArrayList<SaldoInicialPorCliente>();
    Date fecha = fechaSolicitada;
    List<Tmct0CuentaLiquidacion> cuentasLiq = new ArrayList<Tmct0CuentaLiquidacion>();
    Date bigCrunch = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    try {
      bigCrunch = sdf.parse("29880608");
    }
    catch (ParseException e) {
      LOG.debug(e.getMessage() + "Error parsing bigcrunch");
    }

    /*
     * Dependiendo de la fecha... Con esto nos da la fecha para la cual
     * buscaremos el saldo inicial a fin de dia.
     * 
     * Saldo inicial (d+1) == Saldo final (d)
     * 
     * "Salvo" si la fecha es la de hoy, que entonces hay que incluir los
     * movimientos con saldo inicial el del dia porque no hay saldo inicial de
     * mañana.
     */
    if (!incluirMovs) {
      fecha = DateUtils.addDays(fecha, +1);
    }

    if (cdcodigocuentaliq.equals("-1")) {
      cuentasLiq = cuentaLiquidacionBo.findAllClearingNoPropias();
    }
    else {
      Tmct0CuentaLiquidacion liq = new Tmct0CuentaLiquidacion();
      cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
      liq.setCdCodigo(cdcodigocuentaliq);
      cuentasLiq.add(liq);
    }
    for (TuplaIsinAlias isin : listaIsins) {
      Tmct0SaldoInicialCuenta saldoIni = null;
      SaldoInicialPorCliente saldoFin = null;
      for (Tmct0CuentaLiquidacion cliq : cuentasLiq) {
        // Obtenemos el saldo inicial para ese isin, cuenta, alias y
        // fecha
        saldoIni = saldoBo.findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(
            isin.getIsin(), cliq.getCdCodigo().trim(), isin.getCdaliass(), fecha);

        if (saldoIni != null) {
          if (saldoFin == null) {
            boolean tocado = false;
            for (SaldoInicialPorCliente chequeo : isinSaldos) {
              if (!tocado && chequeo.getAlias().trim().equals(saldoIni.getCdaliass().trim())) {
                SaldoInicial ini = rellenarSaldoInicial(saldoIni);
                // Quitamos de la lista los que no tienen ni
                // titulos ni efectivo.
                if (!ini.tieneTitulosOrEfectivo()) {
                  continue;
                }
                if (!ini.tieneTitulos()) {
                  continue;
                }
                // Comprobar si ya existe un grupo con ese isin
                boolean incluido = false;
                for (SaldoInicial candidatoACopia : chequeo.getGrupoBalanceClienteList()) {
                  if (candidatoACopia.getIsin().trim().equals(ini.getIsin().trim())) {
                    candidatoACopia.setEfectivo(candidatoACopia.getEfectivo().add(ini.getEfectivo()));
                    candidatoACopia.setTitulos(candidatoACopia.getTitulos().add(ini.getTitulos()));
                    chequeo.setTotalEfectivos(chequeo.getTotalEfectivos().add(ini.getEfectivo()));
                    incluido = true;
                    break;
                  }
                }
                if (!incluido) {
                  if (incluirMovs) {
                    List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(
                        fecha, bigCrunch, ini.getIsin(), chequeo.getAlias(), cliq.getCdCodigo().trim());
                    for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
                      ini.setEfectivo(ini.getEfectivo().add(movAli.getImefectivo()));
                      ini.setTitulos(ini.getTitulos().add(movAli.getTitulos()));
                    }
                  }
                  // Rellenamos la descripcion del isin
                  if (isin.getDescrisin() == null) {
                    List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(isin.getIsin());
                    isin.fillDescrisin(valores);
                  }
                  ini.setDescrisin(isin.getDescrisin());
                  chequeo.addGrupoBalanceCliente(ini);
                }
                chequeo.setTotalEfectivos(chequeo.getTotalEfectivos().add(saldoIni.getImefectivo()));
                tocado = true;
                break;
              }
            }
            if (!tocado) {
              // No existe un SaldoInicialPorCliente con ese Alias
              saldoFin = new SaldoInicialPorCliente();
              saldoFin.setAlias(saldoIni.getCdaliass());
              saldoFin.setDescralias(aliasBo.findDescraliByCdAliass(saldoIni.getCdaliass()));
              SaldoInicial ini = rellenarSaldoInicial(saldoIni);
              // Quitamos de la lista los que no tienen ni titulos
              // ni efectivo.
              if (!ini.tieneTitulosOrEfectivo()) {
                continue;
              }
              if (!ini.tieneTitulos()) {
                continue;
              }
              if (incluirMovs) {
                List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(
                    fecha, bigCrunch, ini.getIsin(), saldoFin.getAlias(), cliq.getCdCodigo().trim());
                for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
                  ini.setEfectivo(ini.getEfectivo().add(movAli.getImefectivo()));
                  ini.setTitulos(ini.getTitulos().add(movAli.getTitulos()));
                }
              }
              saldoFin.setTotalEfectivos(ini.getEfectivo());
              // Rellenamos la descripcion del isin
              if (isin.getDescrisin() == null) {
                List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(isin.getIsin());
                isin.fillDescrisin(valores);
              }
              ini.setDescrisin(isin.getDescrisin());

              saldoFin.addGrupoBalanceCliente(ini);
            }
          }
          else {
            SaldoInicial ini = rellenarSaldoInicial(saldoIni);
            // Quitamos de la lista los que no tienen ni titulos ni
            // efectivo.
            if (!ini.tieneTitulosOrEfectivo()) {
              continue;
            }
            if (!ini.tieneTitulos()) {
              continue;
            }
            // Comprobar si ya existe un grupo con ese isin
            boolean incluido = false;
            for (SaldoInicial candidatoACopia : saldoFin.getGrupoBalanceClienteList()) {
              if (candidatoACopia.getIsin().trim().equals(ini.getIsin().trim())) {
                candidatoACopia.setEfectivo(candidatoACopia.getEfectivo().add(ini.getEfectivo()));
                candidatoACopia.setTitulos(candidatoACopia.getTitulos().add(ini.getTitulos()));
                incluido = true;
                break;
              }
            }
            if (!incluido) {
              if (incluirMovs) {
                List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(
                    fecha, bigCrunch, ini.getIsin(), saldoFin.getAlias(), cliq.getCdCodigo().trim());
                for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
                  ini.setEfectivo(ini.getEfectivo().add(movAli.getImefectivo()));
                  ini.setTitulos(ini.getTitulos().add(movAli.getTitulos()));
                }
              }
              saldoFin.addGrupoBalanceCliente(ini);
            }
            saldoFin.setTotalEfectivos(saldoFin.getTotalEfectivos().add(saldoIni.getImefectivo()));
          }
        }
      }
      if (saldoFin != null && saldoFin.getTotalEfectivos() != null && saldoFin.getGrupoBalanceClienteList() != null) {
        // Rellenamos la descripcion del alias
        if (isin.getDescAli() == null) {
          Tmct0ali alias = aliasBo.findByCdaliass(isin.getCdaliass(), Tmct0ali.fhfinActivos);
          isin.fillDescali(alias);
        }
        saldoFin.setDescralias(isin.getDescAli());
        isinSaldos.add(saldoFin);
      }
    }
    return isinSaldos;
  }

  public List<SaldoInicialPorCliente> traerPorClienteFuturo(List<TuplaIsinAlias> isin, Date now) {
    Map<String, Serializable> serializedFilters = new HashMap<String, Serializable>();
    serializedFilters.put("fliquidacionDe", now);
    serializedFilters.put("fliquidacionA", DateUtils.setYears(now, 2100));

    List<String> listaAlias = new ArrayList<String>();
    List<String> listaIsin = new ArrayList<String>();
    for (TuplaIsinAlias tupla : isin) {
      listaAlias.add(tupla.getCdaliass());
      listaIsin.add(tupla.getIsin());
    }

    List<Tmct0MovimientoCuentaAlias> listaMovis = null;
    try {
      listaMovis = (List<Tmct0MovimientoCuentaAlias>) movCuentaAliasBo
          .findAllBySettlementdateGreaterThanEqualAndCdaliassInAndIsinIn(now, listaAlias, listaIsin);
    }
    catch (RuntimeException e) {
      LOG.warn("Database access problem: {}. Error inesperado {}", CMD_GET_BALANCE_MOVIMIENTOS_CUENTA_VIRTUAL,
          e.getMessage());
    }

    List<SaldoInicialPorCliente> isinSaldos = new ArrayList<SaldoInicialPorCliente>();

    for (Tmct0MovimientoCuentaAlias mov : listaMovis) {
      if (isinSaldos.size() == 0) {
        // Es el primero
        SaldoInicialPorCliente iniPorCliente = new SaldoInicialPorCliente();
        iniPorCliente.setAlias(mov.getCdaliass());

        SaldoInicial ini = new SaldoInicial();
        ini.setIsin(mov.getIsin());
        ini.setDescrisin(valoresBo.findDescIsinByIsin(mov.getIsin()));
        ini.setEfectivo(mov.getImefectivo());
        ini.setTitulos(mov.getTitulos());
        iniPorCliente.addGrupoBalanceCliente(ini);
        iniPorCliente.setTotalEfectivos(ini.getEfectivo());
        isinSaldos.add(iniPorCliente);
      }
      else {
        boolean tocado = false;
        for (SaldoInicialPorCliente saldo : isinSaldos) {
          if (mov.getCdaliass().trim().equals(saldo.getAlias().trim())) {
            // Ya esta metido ese alias
            boolean tocadoIsin = false;
            for (SaldoInicial inicialesIsin : saldo.getGrupoBalanceClienteList()) {
              if (inicialesIsin.getIsin().equals(mov.getIsin())) {
                // Tiene el mismo isin
                inicialesIsin.setIsin(mov.getIsin());
                inicialesIsin.setDescrisin(valoresBo.findDescIsinByIsin(mov.getIsin()));
                inicialesIsin.setEfectivo(mov.getImefectivo().add(inicialesIsin.getEfectivo()));
                inicialesIsin.setTitulos(mov.getTitulos().add(inicialesIsin.getTitulos()));
                saldo.setTotalEfectivos(mov.getImefectivo().add(saldo.getTotalEfectivos()));
                tocadoIsin = true;
                tocado = true;
              }
            }
            if (!tocadoIsin) {
              SaldoInicial ini = new SaldoInicial();
              ini.setIsin(mov.getIsin());
              ini.setDescrisin(valoresBo.findDescIsinByIsin(mov.getIsin()));
              ini.setEfectivo(mov.getImefectivo());
              ini.setTitulos(mov.getTitulos());
              saldo.addGrupoBalanceCliente(ini);
              saldo.setTotalEfectivos(ini.getEfectivo().add(saldo.getTotalEfectivos()));
              tocado = true;
            }
          }
        }
        if (!tocado) {
          SaldoInicialPorCliente iniPorCliente = new SaldoInicialPorCliente();
          iniPorCliente.setAlias(mov.getCdaliass());
          iniPorCliente.setDescralias(aliasBo.findDescraliByCdAliass(mov.getCdaliass()));

          SaldoInicial ini = new SaldoInicial();
          ini.setIsin(mov.getIsin());
          ini.setDescrisin(valoresBo.findDescIsinByIsin(mov.getIsin()));
          ini.setEfectivo(mov.getImefectivo());
          ini.setTitulos(mov.getTitulos());
          iniPorCliente.addGrupoBalanceCliente(ini);
          iniPorCliente.setTotalEfectivos(ini.getEfectivo());
          isinSaldos.add(iniPorCliente);
        }
      }
    }

    return isinSaldos;
  }

  public List<SaldoInicialPorIsin> traerPorIsin(List<TuplaIsinAlias> listaIsins, boolean incluirMovs,
      Date fechaSolicitada, String cdcodigocuentaliq) {
    List<SaldoInicialPorIsin> isinSaldos = new ArrayList<SaldoInicialPorIsin>();
    List<Tmct0CuentaLiquidacion> cuentasLiq = new ArrayList<Tmct0CuentaLiquidacion>();
    Date bigCrunch = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    Date fecha = fechaSolicitada;

    try {
      bigCrunch = sdf.parse("29880608");
    }
    catch (ParseException e) {
      LOG.warn(e.getMessage() + "Error parsing bigcrunch");
    }

    /*
     * Dependiendo de la fecha... Con esto nos da la fecha para la cual
     * buscaremos el saldo inicial a fin de dia.
     * 
     * Saldo inicial (d+1) == Saldo final (d)
     * 
     * "Salvo" si la fecha es la de hoy, que entonces hay que incluir los
     * movimientos con saldo inicial el del dia porque no hay saldo inicial de
     * mañana.
     */
    if (!incluirMovs) {
      fecha = DateUtils.addDays(fecha, +1);
    }

    if (cdcodigocuentaliq.equals("-1")) {
      cuentasLiq = cuentaLiquidacionBo.findAllClearingNoPropias();
    }
    else {
      Tmct0CuentaLiquidacion liq = new Tmct0CuentaLiquidacion();
      cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
      liq.setCdCodigo(cdcodigocuentaliq);
      cuentasLiq.add(liq);
    }

    for (TuplaIsinAlias isin : listaIsins) {
      Tmct0SaldoInicialCuenta saldoIni = null;
      SaldoInicialPorIsin saldoFin = null;
      for (Tmct0CuentaLiquidacion cliq : cuentasLiq) {
        // Obtenemos el saldo inicial para ese isin, cuenta, alias y
        // fecha
        saldoIni = saldoBo.findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(
            isin.getIsin(), cliq.getCdCodigo().trim(), isin.getCdaliass(), fecha);
        if (saldoIni != null) {
          if (saldoFin == null) {
            boolean tocado = false;
            for (SaldoInicialPorIsin chequeo : isinSaldos) {
              if (!tocado && chequeo.getIsin().trim().equals(saldoIni.getIsin().trim())) {
                SaldoInicialIsin ini = rellenarSaldoInicialIsin(saldoIni);
                // Quitamos de la lista los que no tienen ni
                // titulos ni efectivo.
                if (!ini.tieneTitulosOrEfectivo()) {
                  continue;
                }
                if (!ini.tieneTitulos()) {
                  continue;
                }
                // Comprobar si ya existe un grupo con ese alias
                boolean incluido = false;
                for (SaldoInicialIsin candidatoACopia : chequeo.getGrupoBalanceISINList()) {
                  if (candidatoACopia.getAlias().trim().equals(ini.getAlias().trim())) {
                    candidatoACopia.setEfectivo(candidatoACopia.getEfectivo().add(ini.getEfectivo()));
                    candidatoACopia.setTitulos(candidatoACopia.getTitulos().add(ini.getTitulos()));
                    incluido = true;
                    break;
                  }
                }
                if (!incluido) {
                  if (incluirMovs) {
                    List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(
                        fecha, bigCrunch, chequeo.getIsin(), ini.getAlias(), cliq.getCdCodigo().trim());
                    for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
                      ini.setEfectivo(ini.getEfectivo().add(movAli.getImefectivo()));
                      ini.setTitulos(ini.getTitulos().add(movAli.getTitulos()));
                    }
                  }
                  // Rellenamos la descripcion del alias
                  if (isin.getDescAli() == null) {
                    Tmct0ali alias = aliasBo.findByCdaliass(isin.getCdaliass(), Tmct0ali.fhfinActivos);
                    isin.fillDescali(alias);
                  }
                  ini.setDescAli(isin.getDescAli());
                  chequeo.addGrupoBalanceISIN(ini);
                }
                chequeo.setTotalEfectivos(chequeo.getTotalEfectivos().add(saldoIni.getImefectivo()));
                tocado = true;
                break;
              }
            }
            if (!tocado) {
              // No existe un SaldoInicialPorIsin con ese Isin
              saldoFin = new SaldoInicialPorIsin();
              saldoFin.setIsin(saldoIni.getIsin());
              saldoFin.setDescrisin(valoresBo.findDescIsinByIsin(saldoIni.getIsin()));
              SaldoInicialIsin ini = rellenarSaldoInicialIsin(saldoIni);
              // Quitamos de la lista los que no tienen ni titulos
              // ni efectivo.
              if (!ini.tieneTitulosOrEfectivo()) {
                continue;
              }
              if (!ini.tieneTitulos()) {
                continue;
              }
              if (incluirMovs) {
                List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(
                    fecha, bigCrunch, saldoFin.getIsin(), ini.getAlias(), cliq.getCdCodigo().trim());
                for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
                  ini.setEfectivo(ini.getEfectivo().add(movAli.getImefectivo()));
                  ini.setTitulos(ini.getTitulos().add(movAli.getTitulos()));

                }
              }
              saldoFin.setTotalEfectivos(ini.getEfectivo());
              // Rellenamos la descripcion del alias
              if (isin.getDescAli() == null) {
                Tmct0ali alias = aliasBo.findByCdaliass(isin.getCdaliass(), Tmct0ali.fhfinActivos);
                isin.fillDescali(alias);
              }
              ini.setDescAli(isin.getDescAli());
              saldoFin.addGrupoBalanceISIN(ini);
            }
          }
          else {
            SaldoInicialIsin ini = rellenarSaldoInicialIsin(saldoIni);
            // Quitamos de la lista los que no tienen ni titulos ni
            // efectivo.
            if (!ini.tieneTitulosOrEfectivo()) {
              continue;
            }
            if (!ini.tieneTitulos()) {
              continue;
            }
            // Comprobar si ya existe un grupo con ese alias
            boolean incluido = false;
            for (SaldoInicialIsin candidatoACopia : saldoFin.getGrupoBalanceISINList()) {
              if (candidatoACopia.getAlias().trim().equals(ini.getAlias().trim())) {
                candidatoACopia.setEfectivo(candidatoACopia.getEfectivo().add(ini.getEfectivo()));
                candidatoACopia.setTitulos(candidatoACopia.getTitulos().add(ini.getTitulos()));
                incluido = true;
                break;
              }
            }
            if (!incluido) {
              if (incluirMovs) {
                List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(
                    fecha, bigCrunch, saldoFin.getIsin(), ini.getAlias(), cliq.getCdCodigo().trim());
                for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
                  ini.setEfectivo(ini.getEfectivo().add(movAli.getImefectivo()));
                  ini.setTitulos(ini.getTitulos().add(movAli.getTitulos()));
                }
              }
              saldoFin.addGrupoBalanceISIN(ini);
            }
            saldoFin.setTotalEfectivos(saldoFin.getTotalEfectivos().add(saldoIni.getImefectivo()));
          }
        }
      }
      if (saldoFin != null && saldoFin.getTotalEfectivos() != null && saldoFin.getGrupoBalanceISINList() != null) {
        if (saldoFin.getTotalTitulos() == null) {
          saldoFin.setTotalTitulos(BigDecimal.ZERO);
        }
        // Rellenamos la descripcion del isin
        if (isin.getDescrisin() == null) {
          List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(isin.getIsin());
          isin.fillDescrisin(valores);
        }
        saldoFin.setDescrisin(isin.getDescrisin());
        isinSaldos.add(saldoFin);
      }
    }
    return isinSaldos;
  }

  public List<SaldoInicialPorIsin> traerPorIsinFuturo(List<TuplaIsinAlias> isin, Date now) {
    Map<String, Serializable> serializedFilters = new HashMap<String, Serializable>();
    serializedFilters.put("fliquidacionDe", now);
    serializedFilters.put("fliquidacionA", DateUtils.setYears(now, 2100));

    List<String> listaAlias = new ArrayList<String>();
    List<String> listaIsin = new ArrayList<String>();
    for (TuplaIsinAlias tupla : isin) {
      listaAlias.add(tupla.getCdaliass());
      listaIsin.add(tupla.getIsin());
    }

    List<Tmct0MovimientoCuentaAlias> listaMovis = null;
    try {
      listaMovis = (List<Tmct0MovimientoCuentaAlias>) movCuentaAliasBo
          .findAllBySettlementdateGreaterThanEqualAndCdaliassInAndIsinIn(now, listaAlias, listaIsin);
    }
    catch (RuntimeException e) {
      LOG.error(
          "[traerPorIsinFuturo] Error inesperado al consultar findAllBySettlementdateGreaterThanEqualAndCdaliassInAndIsinIn",
          e);
    }

    List<SaldoInicialPorIsin> isinSaldos = new ArrayList<SaldoInicialPorIsin>();

    for (Tmct0MovimientoCuentaAlias mov : listaMovis) {
      if (isinSaldos.size() == 0) {
        // Es el primero
        SaldoInicialPorIsin iniPorCliente = new SaldoInicialPorIsin();
        iniPorCliente.setIsin(mov.getIsin());
        iniPorCliente.setDescrisin(valoresBo.findDescIsinByIsin(mov.getIsin()));
        SaldoInicialIsin ini = new SaldoInicialIsin();
        ini.setAlias(mov.getCdaliass());
        ini.setDescAli(aliasBo.findDescraliByCdAliass(mov.getCdaliass()));
        ini.setEfectivo(mov.getImefectivo());
        ini.setTitulos(mov.getTitulos());
        iniPorCliente.addGrupoBalanceISIN(ini);
        iniPorCliente.setTotalEfectivos(ini.getEfectivo());
        isinSaldos.add(iniPorCliente);
      }
      else {
        boolean tocado = false;
        for (SaldoInicialPorIsin saldo : isinSaldos) {
          if (mov.getIsin().trim().equals(saldo.getIsin().trim())) {
            // Ya esta metido ese isin
            boolean tocadoIsin = false;
            for (SaldoInicialIsin inicialesIsin : saldo.getGrupoBalanceISINList()) {
              if (inicialesIsin.getAlias().trim().equals(mov.getCdaliass().trim())) {
                // Tiene el mismo alias
                inicialesIsin.setAlias(mov.getCdaliass());
                inicialesIsin.setDescAli(aliasBo.findDescraliByCdAliass(mov.getCdaliass()));
                inicialesIsin.setEfectivo(mov.getImefectivo().add(inicialesIsin.getEfectivo()));
                inicialesIsin.setTitulos(mov.getTitulos().add(inicialesIsin.getTitulos()));
                saldo.setTotalEfectivos(mov.getImefectivo().add(saldo.getTotalEfectivos()));
                tocadoIsin = true;
                tocado = true;
              }
            }
            if (!tocadoIsin) {
              SaldoInicialIsin ini = new SaldoInicialIsin();
              ini.setAlias(mov.getCdaliass());
              ini.setDescAli(aliasBo.findDescraliByCdAliass(mov.getCdaliass()));
              ini.setEfectivo(mov.getImefectivo());
              ini.setTitulos(mov.getTitulos());
              saldo.addGrupoBalanceISIN(ini);
              saldo.setTotalEfectivos(ini.getEfectivo().add(saldo.getTotalEfectivos()));
              tocado = true;
            }
          }
        }
        if (!tocado) {
          SaldoInicialPorIsin iniPorIsin = new SaldoInicialPorIsin();
          iniPorIsin.setIsin(mov.getIsin());
          iniPorIsin.setDescrisin(valoresBo.findDescIsinByIsin(mov.getIsin()));

          SaldoInicialIsin ini = new SaldoInicialIsin();
          ini.setAlias(mov.getCdaliass());
          ini.setDescAli(aliasBo.findDescraliByCdAliass(mov.getCdaliass()));
          ini.setEfectivo(mov.getImefectivo());
          ini.setTitulos(mov.getTitulos());
          iniPorIsin.addGrupoBalanceISIN(ini);
          iniPorIsin.setTotalEfectivos(ini.getEfectivo());
          isinSaldos.add(iniPorIsin);
        }
      }
    }

    return isinSaldos;
  }

  private SaldoInicial rellenarSaldoInicial(Tmct0SaldoInicialCuenta saldoIni) {
    SaldoInicial ini = new SaldoInicial();
    ini.setIsin(saldoIni.getIsin());
    ini.setEfectivo(saldoIni.getImefectivo());
    ini.setTitulos(saldoIni.getTitulos());
    ini.setDescrisin(saldoIni.getIsin());
    ini.setFliquidacion(saldoIni.getFecha().toString());
    return ini;
  }

  private SaldoInicialIsin rellenarSaldoInicialIsin(Tmct0SaldoInicialCuenta saldoIni) {
    SaldoInicialIsin ini = new SaldoInicialIsin();
    ini.setAlias(saldoIni.getCdaliass());
    ini.setEfectivo(saldoIni.getImefectivo());
    ini.setTitulos(saldoIni.getTitulos());
    ini.setFliquidacion(saldoIni.getFecha().toString());
    return ini;
  }

  public Map<String, MultiValueMap<String, Serializable>> conciliarCuentaClearing() {
    Map<String, MultiValueMap<String, Serializable>> resultSet = new HashMap<String, MultiValueMap<String, Serializable>>();
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -60);
    Date tradeDate = cal.getTime();
    List<Tmct0DiferenciaConciliacionTitulo> diferencias = diferenciaTituloBO
        .findByTradeDateGreaterThanEqualOrderByTradeDate(tradeDate);

    Date td = null;
    String isin = "";
    String sentido = "";
    Tmct0CuentasDeCompensacion cuenta = null;
    //
    for (Tmct0DiferenciaConciliacionTitulo dif : diferencias) {
      td = dif.getTradeDate();
      isin = dif.getIsin();
      sentido = dif.getSentido();
      cuenta = dif.getTmct0CuentasDeCompensacion();

      Tmct0DiferenciaConciliacionEfectivo difEf = diferenciaEfectivoBO
          .findByTradeDateAndIsinAndTmct0CuentasDeCompensacionAndSentido(td, isin, cuenta, sentido);
      if (difEf != null) {
        // LOG.debug(String.valueOf(difEf.getId()));

      }
    }
    return resultSet;
  }

  public List<Tmct0DiferenciaConciliacionTitulo> findDiferenciasTitulosByTradeDate(Date tradeDate) {
    List<Tmct0DiferenciaConciliacionTitulo> resultList = null;
    resultList = diferenciaTituloBO.findByTradeDateGreaterThanEqualOrderByTradeDate(tradeDate);
    return resultList;
  }

  public List<Tmct0DiferenciaConciliacionEfectivo> findDiferenciasEfectivoByTradeDate(Date tradeDate) {
    List<Tmct0DiferenciaConciliacionEfectivo> resultList = null;
    resultList = diferenciaEfectivoBO.findByTradeDateGreaterThanEqualOrderByTradeDate(tradeDate);
    return resultList;
  }

  public List<Map<String, String>> getCuentasLiquidacion() throws PersistenceException, JpaSystemException {
    List<Tmct0CuentaLiquidacion> resultList;
    List<Map<String, String>> resultados = new ArrayList<Map<String, String>>();

    resultList = cuentaLiquidacionBo.findAll();
    // Se ordena por cdCodigo
    Collections.sort(resultList, new Comparator<Tmct0CuentaLiquidacion>() {

      @Override
      public int compare(Tmct0CuentaLiquidacion o1, Tmct0CuentaLiquidacion o2) {

        return o1.getCdCodigo().compareTo(o2.getCdCodigo());
      }
    });
    for (Tmct0CuentaLiquidacion result : resultList) {
      Map<String, String> mapeo = new HashMap<String, String>();
      mapeo.put("id", Long.toString(result.getId()));
      mapeo.put("name", result.getCdCodigo());
      resultados.add(mapeo);
    }

    return resultados;
  }

  public Map<String, Object> compareVirtualWithVirtualS3(List<MovimientoCuentaVirtualDTO> virtualDTO,
      List<MovimientoCuentaVirtualS3Data> movimientosByCuentaS3, String diferencia) {
    List<Map<String, String>> listaTotal = new ArrayList<Map<String, String>>();
    Map<String, Object> result = new HashMap<String, Object>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
    Date newestDate = new Date(Long.MIN_VALUE);
    boolean[] copiadoS3 = new boolean[movimientosByCuentaS3.size()];
    boolean[] copiadosVirtual = new boolean[virtualDTO.size()];

    for (int i = 0; i < copiadosVirtual.length; i++) {
      copiadosVirtual[i] = false;
    }
    for (int i = 0; i < copiadoS3.length; i++) {
      copiadoS3[i] = false;
    }

    for (int i = 0; i < virtualDTO.size(); i++) {
      MovimientoCuentaVirtualDTO cuentaVirtual = virtualDTO.get(i);
      for (int j = 0; j < movimientosByCuentaS3.size(); j++) {
        if (newestDate.before(movimientosByCuentaS3.get(j).getAuditDate())) {
          newestDate = (movimientosByCuentaS3.get(j)).getAuditDate();
        }

        String settlementS3 = dateFormat.format(movimientosByCuentaS3.get(j).getSettlementdate());
        if (!copiadoS3[j]) {
          if (cuentaVirtual.getIsin().equals(movimientosByCuentaS3.get(j).getIsin())
              && cuentaVirtual.getSettlementdate().equals(settlementS3)) {
            // Como la cuenta viene obligada no hay que compararla
            // && cuentaVirtual.getIdcuentaliq() ==
            // movimientosByCuentaS3.get( j ).getIdcuentaliq() ) {

            copiadosVirtual[i] = true;
            copiadoS3[j] = true;

            boolean sameTitulos = false;
            Integer titulosIzqda = cuentaVirtual.getTitulos().intValue();
            Integer titulosDerecha = movimientosByCuentaS3.get(j).getTitulos().intValue();

            if (cuentaVirtual.getSentido() == 'V') {
              titulosIzqda = titulosIzqda * (-1);
            }

            if (titulosIzqda.equals(titulosDerecha)) {
              sameTitulos = true;
            }

            Map<String, String> total = new HashMap<String, String>();

            total.put("sistemaLiq", cuentaVirtual.getSistemaLiq());
            total.put("isin", cuentaVirtual.getIsin());
            total.put("descIsin", cuentaVirtual.getDescrIsin());
            total.put("settlementdate", cuentaVirtual.getSettlementdate());
            Tmct0CuentaLiquidacion cuentaLiq = cuentaLiquidacionBo.findById(virtualDTO.get(i).getIdcuentaliq());
            try {
              total.put("cuentaliq", cuentaLiq.getCdCodigo());
            }
            catch (NullPointerException e) {
              total.put("cuentaliq", virtualDTO.get(i).getCdCodigo());
              LOG.error(e.getMessage(), e);
              LOG.error("compareVirtualWithVirtualS3: CuentaLiq not found");
            }
            total.put("titulosVirtual", String.valueOf(titulosIzqda));

            if (sameTitulos) {
              total.put("iguales", "true");
              total.put("diferencia", "0");
            }
            else {
              total.put("iguales", "false");
              total.put("diferencia", String.valueOf(titulosIzqda - titulosDerecha));
            }
            total.put("titulosCuentasS3", String.valueOf((movimientosByCuentaS3.get(j).getTitulos().intValue())));
            listaTotal.add(total);
            break;
          }
        }
      }
    }

    // Ahora vamos a comprobar los desemparejados
    for (int i = 0; i < virtualDTO.size(); i++) {
      if (!copiadosVirtual[i]) {
        Map<String, String> total = new HashMap<String, String>();

        total.put("sistemaLiq", virtualDTO.get(i).getSistemaLiq());
        total.put("isin", virtualDTO.get(i).getIsin());
        total.put("descIsin", virtualDTO.get(i).getDescrIsin());
        total.put("settlementdate", virtualDTO.get(i).getSettlementdate());
        /*
         * Tmct0CuentaLiquidacion cuentaLiq = cuentaLiquidacionBo.findById(
         * virtualDTO.get( i ).getIdcuentaliq() ); try { izqda.put( "cuentaliq",
         * cuentaLiq.getCdCodigo() ); } catch ( NullPointerException e ) {
         * izqda.put( "cuentaliq", virtualDTO.get( i ).getCdCodigo() );
         * LOG.error( e.getMessage(), e ); LOG.error(
         * "compareVirtualWithVirtualS3: CuentaLiq not found" ); }
         */
        total.put("cuentaliq", virtualDTO.get(i).getCdCodigo());
        Integer titulosIzqda = virtualDTO.get(i).getTitulos().intValue();

        if (virtualDTO.get(i).getSentido() == 'V') {
          titulosIzqda = titulosIzqda * (-1);
        }
        total.put("titulosVirtual", String.valueOf(titulosIzqda));
        total.put("titulosS3", "-");
        total.put("iguales", "false");
        total.put("diferencia", "-");

        listaTotal.add(total);
      }
    }

    for (int i = 0; i < movimientosByCuentaS3.size(); i++) {
      if (!copiadoS3[i]) {

        Map<String, String> total = new HashMap<String, String>();

        String settlementS3 = dateFormat.format(movimientosByCuentaS3.get(i).getSettlementdate());
        total.put("sistemaLiq", "-");
        total.put("cuentaliq", "-");
        total.put("titulosVirtual", "-");
        total.put("iguales", "false");
        total.put("diferencia", "-");

        total.put("isin", movimientosByCuentaS3.get(i).getIsin());
        total.put("descIsin", movimientosByCuentaS3.get(i).getDescrIsin());
        total.put("settlementdate", settlementS3);
        Tmct0CuentaLiquidacion cuentaLiq = cuentaLiquidacionBo.findById(movimientosByCuentaS3.get(i).getIdcuentaliq());
        try {
          total.put("cuentaliq", cuentaLiq.getCdCodigo());
        }
        catch (NullPointerException e) {
          LOG.error(e.getMessage(), e);
          LOG.error("compareVirtualWithVirtualS3: CuentaLiq not found");
        }
        total.put("titulosS3", String.valueOf(((BigDecimal) movimientosByCuentaS3.get(i).getTitulos()).intValue()));
        total.put("iguales", "false");
        total.put("diferencia", "-");

        listaTotal.add(total);
        copiadoS3[i] = true;
      }
    }
    result.put("total", listaTotal);

    Date oldDate = new Date(Long.MIN_VALUE);

    if (newestDate.compareTo(oldDate) == 0) {
      result.put("newestDate", "");
      result.put("newestHour", "");
    }
    else {
      result.put("newestDate", dateFormat.format(newestDate));
      result.put("newestHour", hourFormat.format(newestDate));
    }
    return getDiferencia(result, diferencia);
  }

  private Map<String, Object> getDiferencia(final Map<String, Object> input, final String tipoDiferencia) {
    Map<String, Object> result = input;
    List<HashMap<String, String>> lst;
    List<HashMap<String, String>> tmp;
    String diferencia;
    Integer number;
    if ("cuadrados".equals(tipoDiferencia)) {
      lst = (List<HashMap<String, String>>) input.get("total");
      tmp = new ArrayList<HashMap<String, String>>();
      for (HashMap<String, String> hashMap : lst) {
        diferencia = hashMap.get("diferencia");
        if (diferencia.matches("-?\\d+(\\.\\d+)?")) {
          number = Integer.parseInt(diferencia);
          if (number.equals(0)) {
            tmp.add(hashMap);
          }
        }
      }
      input.put("total", tmp);
    }
    else if ("desCuadrados".equals(tipoDiferencia)) {
      lst = (List<HashMap<String, String>>) input.get("total");
      tmp = new ArrayList<HashMap<String, String>>();
      for (HashMap<String, String> hashMap : lst) {
        diferencia = hashMap.get("diferencia");
        if (diferencia.matches("-?\\d+(\\.\\d+)?")) {
          number = Integer.parseInt(diferencia);
          if (!number.equals(0)) {
            tmp.add(hashMap);
          }
        }
        else {
          tmp.add(hashMap);
        }
      }
      input.put("total", tmp);
    }
    return result;
  }

  private List<Tmct0desgloseCamaraData> getDesgloseCamaraBySentidoCuentasClearing(Map<String, String> filtros) {
    final Map<String, Serializable> normalizedParams;

    normalizedParams = filtros != null ? util.normalizeFilters(filtros) : new HashMap<String, Serializable>();
    if (normalizedParams.get("fcontratacionDe") != null && normalizedParams.get("fcontratacionA") == null) {
      Date bigCrunch = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      try {
        bigCrunch = sdf.parse("29880608");
      }
      catch (ParseException e) {
        LOG.debug(e.getMessage() + "Error parsing bigcrunch");
      }
      normalizedParams.put("fcontratacionA", bigCrunch);
    }
    compruebaFecha(normalizedParams, "fcontratacionDe", diasARestarFechaDe);
    compruebaFecha(normalizedParams, "fcontratacionA", diasARestarFechaA);
    return desgloseCamaraBo.getDesglosesCamaraGroupByIsin('C', ASIGNACIONES.PDTE_LIQUIDAR.getId(),
        ASIGNACIONES.IMPUTADA_S3.getId(), normalizedParams);
  }

  private void compruebaFecha(Map<String, Serializable> normalizedParams, String queFecha, int cuantosDiasRetrocede) {
    if (!util.checkIfExistParam(queFecha, normalizedParams)) {
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, -1 * cuantosDiasRetrocede);
      java.sql.Date dateDe = new java.sql.Date(cal.getTimeInMillis());
      while (!gestorPeriodicidades.isLaborable(dateDe)) {
        cal.setTimeInMillis(dateDe.getTime());
        cal.add(Calendar.DATE, -1);
        dateDe = new java.sql.Date(cal.getTimeInMillis());
      }
      normalizedParams.put(queFecha, cal.getTime());
    }
  }

  private List<CuentaLiquidacionDTO> getCuentasLiquidacionClearing() {
    List<CuentaLiquidacionDTO> cuentasDTO = null;
    List<Tmct0CuentaLiquidacion> entities = cuentaLiquidacionBo.findAllByCuentaDeCompensacionIsClearing();
    cuentasDTO = cuentaLiquidacionBo.entitiesToDTOList(entities);
    return cuentasDTO;
  }

  private List<CuentaLiquidacionDTO> getCuentasLiquidacionClearingNoPropiaNoIndividuales() {
    List<CuentaLiquidacionDTO> cuentasDTO = null;
    List<Tmct0CuentaLiquidacion> entities = cuentaLiquidacionBo
        .findAllByCuentaDeCompensacionIsClearingNoPropiaNoIndividual();
    cuentasDTO = cuentaLiquidacionBo.entitiesToDTOList(entities);
    return cuentasDTO;
  }

  private List<String> getCuentasDeCompensacionFiltrado(Map<String, Serializable> params) {
    final String cdCodigo;
    cdCodigo = (String) params.get("cdCodigo");
    return ccBo.findByTipoCuenta(cdCodigo);
  }

  private Map<String, String> getInitialFliquidacionBetween() {
    // Se construye un mapa auxiliar
    Map<String, Serializable> auxFechas = new HashMap<String, Serializable>();
    auxFechas.put("fcontratacionDe", null);
    auxFechas.put("fcontratacionA", null);
    // Se obtienen las fechas
    compruebaFecha(auxFechas, "fcontratacionDe", diasARestarFechaDe);
    compruebaFecha(auxFechas, "fcontratacionA", diasARestarFechaA);
    // Se pasan las fechas a String y se meten en un nuevo mapa tipo String,
    // String
    Map<String, String> fechasBtw = new HashMap<String, String>();
    fechasBtw.put("fcontratacionDe", util.parseDateToString(new Date()));
    auxFechas = null;
    // Se devuelve el resultado
    return fechasBtw;
  }

  private List<TuplaIsinAlias> getTuplas(String alias, String isin, String cdcodigocuentaliq, Date now) {
    List<TuplaIsinAlias> isinList = new ArrayList<TuplaIsinAlias>();

    if (alias != null && isin != null) {
      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        if (isin.indexOf("#") != -1 && alias.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          alias = alias.replace("#", "");
          isinList = saldoBo.getTuplasByNotIsinAndNotAlias(now, isin, alias);
        }
        else if (isin.indexOf("#") != -1 && alias.indexOf("#") == -1) {
          isin = isin.replace("#", "");
          isinList = saldoBo.getTuplasByNotIsinAndAlias(now, isin, alias);
        }
        else if (isin.indexOf("#") == -1 && alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          isinList = saldoBo.getTuplasByIsinAndNotAlias(now, isin, alias);
        }
        else
          isinList = saldoBo.getTuplasByIsinAndAlias(now, isin, alias);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          if (isin.indexOf("#") != -1 && alias.indexOf("#") != -1) {
            isin = isin.replace("#", "");
            alias = alias.replace("#", "");
            isinList = saldoBo.getTuplasByNotIsinAndNotAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") != -1 && alias.indexOf("#") == -1) {
            isin = isin.replace("#", "");
            isinList = saldoBo.getTuplasByNotIsinAndAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") == -1 && alias.indexOf("#") != -1) {
            alias = alias.replace("#", "");
            isinList = saldoBo.getTuplasByIsinAndNotAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else
            isinList = saldoBo.getTuplasByIsinAndAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);

        }
        else {
          if (isin.indexOf("#") != -1 && alias.indexOf("#") != -1) {
            isin = isin.replace("#", "");
            alias = alias.replace("#", "");
            isinList = saldoBo.getTuplasByNotIsinAndNotAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") != -1 && alias.indexOf("#") == -1) {
            isin = isin.replace("#", "");
            isinList = saldoBo.getTuplasByNotIsinAndAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") == -1 && alias.indexOf("#") != -1) {
            alias = alias.replace("#", "");
            isinList = saldoBo.getTuplasByIsinAndNotAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else
            isinList = saldoBo.getTuplasByIsinAndAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);

        }

      }
    }
    else if (alias != null && isin == null) {
      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        if (alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          isinList = saldoBo.getTuplasByNotAlias(now, alias);
        }
        else
          isinList = saldoBo.getTuplasByAlias(now, alias);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1 && alias.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          alias = alias.replace("#", "");
          isinList = saldoBo.getTuplasByNotAliasAndNotCuenta(now, alias, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") != -1 && alias.indexOf("#") == -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isinList = saldoBo.getTuplasByAliasAndNotCuenta(now, alias, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") == -1 && alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          isinList = saldoBo.getTuplasByNotAliasAndCuenta(now, alias, cdcodigocuentaliq);
        }
        else
          isinList = saldoBo.getTuplasByAliasAndCuenta(now, alias, cdcodigocuentaliq);
      }

    }
    else if (alias == null && isin != null) {

      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        if (isin.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          isinList = saldoBo.getTuplasByNotIsin(now, isin);
        }
        else
          isinList = saldoBo.getTuplasByIsin(now, isin);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1 && isin.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isin = isin.replace("#", "");
          isinList = saldoBo.getTuplasByNotIsinAndNotCuenta(now, isin, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") != -1 && isin.indexOf("#") == -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isinList = saldoBo.getTuplasByIsinAndNotCuenta(now, isin, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") == -1 && isin.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          isinList = saldoBo.getTuplasByNotIsinAndCuenta(now, isin, cdcodigocuentaliq);
        }
        else
          isinList = saldoBo.getTuplasByIsinAndCuenta(now, isin, cdcodigocuentaliq);
      }
    }
    else if (alias == null && isin == null) {
      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        isinList = saldoBo.getTuplas(now);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isinList = saldoBo.getTuplasByNotCuenta(now, cdcodigocuentaliq);

        }
        isinList = saldoBo.getTuplasByCuenta(now, cdcodigocuentaliq);
      }
    }

    /*
     * if ( alias != null && isin != null ) { if ( cdcodigocuentaliq == null ||
     * cdcodigocuentaliq.equals( "-1" ) ) { isinList =
     * saldoBo.getTuplasByIsinAndAlias( now, isin, alias ); } else { isinList =
     * saldoBo.getTuplasByIsinAndAliasAndCuenta( now, isin, alias,
     * cdcodigocuentaliq ); } } else if ( alias != null && isin == null ) { if (
     * cdcodigocuentaliq == null || cdcodigocuentaliq.equals( "-1" ) ) {
     * isinList = saldoBo.getTuplasByAlias( now, alias ); } else { isinList =
     * saldoBo.getTuplasByAliasAndCuenta( now, alias, cdcodigocuentaliq ); } }
     * else if ( alias == null && isin != null ) { if ( cdcodigocuentaliq ==
     * null || cdcodigocuentaliq.equals( "-1" ) ) { isinList =
     * saldoBo.getTuplasByIsin( now, isin ); } else { isinList =
     * saldoBo.getTuplasByIsinAndCuenta( now, isin, cdcodigocuentaliq ); } }
     * else if ( alias == null && isin == null ) { if ( cdcodigocuentaliq ==
     * null || cdcodigocuentaliq.equals( "-1" ) ) { isinList =
     * saldoBo.getTuplas( now ); } else { isinList = saldoBo.getTuplasByCuenta(
     * now, cdcodigocuentaliq ); } }
     */
    return isinList;
  }

  private List<TuplaIsinAlias> getTuplasIndependientes(String alias, String isin, String cdcodigocuentaliq, Date now) {
    List<TuplaIsinAlias> isinList = new ArrayList<TuplaIsinAlias>();

    if (alias != null && isin != null) {
      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        if (isin.indexOf("#") != -1 && alias.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          alias = alias.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotIsinAndNotAlias(now, isin, alias);
        }
        else if (isin.indexOf("#") != -1 && alias.indexOf("#") == -1) {
          isin = isin.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotIsinAndAlias(now, isin, alias);
        }
        else if (isin.indexOf("#") == -1 && alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByIsinAndNotAlias(now, isin, alias);
        }
        else
          isinList = movCuentaAliasBo.getTuplasByIsinAndAlias(now, isin, alias);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          if (isin.indexOf("#") != -1 && alias.indexOf("#") != -1) {
            isin = isin.replace("#", "");
            alias = alias.replace("#", "");
            isinList = movCuentaAliasBo.getTuplasByNotIsinAndNotAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") != -1 && alias.indexOf("#") == -1) {
            isin = isin.replace("#", "");
            isinList = movCuentaAliasBo.getTuplasByNotIsinAndAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") == -1 && alias.indexOf("#") != -1) {
            alias = alias.replace("#", "");
            isinList = movCuentaAliasBo.getTuplasByIsinAndNotAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else
            isinList = movCuentaAliasBo.getTuplasByIsinAndAliasAndNotCuenta(now, isin, alias, cdcodigocuentaliq);

        }
        else {
          if (isin.indexOf("#") != -1 && alias.indexOf("#") != -1) {
            isin = isin.replace("#", "");
            alias = alias.replace("#", "");
            isinList = movCuentaAliasBo.getTuplasByNotIsinAndNotAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") != -1 && alias.indexOf("#") == -1) {
            isin = isin.replace("#", "");
            isinList = movCuentaAliasBo.getTuplasByNotIsinAndAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else if (isin.indexOf("#") == -1 && alias.indexOf("#") != -1) {
            alias = alias.replace("#", "");
            isinList = movCuentaAliasBo.getTuplasByIsinAndNotAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);
          }
          else
            isinList = movCuentaAliasBo.getTuplasByIsinAndAliasAndCuenta(now, isin, alias, cdcodigocuentaliq);

        }

      }
    }
    else if (alias != null && isin == null) {
      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        if (alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotAlias(now, alias);
        }
        else
          isinList = movCuentaAliasBo.getTuplasByAlias(now, alias);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1 && alias.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          alias = alias.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotAliasAndNotCuenta(now, alias, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") != -1 && alias.indexOf("#") == -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByAliasAndNotCuenta(now, alias, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") == -1 && alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotAliasAndCuenta(now, alias, cdcodigocuentaliq);
        }
        else
          isinList = movCuentaAliasBo.getTuplasByAliasAndCuenta(now, alias, cdcodigocuentaliq);
      }

    }
    else if (alias == null && isin != null) {

      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        if (isin.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotIsin(now, isin);
        }
        else
          isinList = movCuentaAliasBo.getTuplasByIsin(now, isin);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1 && isin.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isin = isin.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotIsinAndNotCuenta(now, isin, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") != -1 && isin.indexOf("#") == -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByIsinAndNotCuenta(now, isin, cdcodigocuentaliq);
        }
        else if (cdcodigocuentaliq.indexOf("#") == -1 && isin.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotIsinAndCuenta(now, isin, cdcodigocuentaliq);
        }
        else
          isinList = movCuentaAliasBo.getTuplasByIsinAndCuenta(now, isin, cdcodigocuentaliq);
      }
    }
    else if (alias == null && isin == null) {
      if (cdcodigocuentaliq == null || cdcodigocuentaliq.equals("-1")) {
        isinList = movCuentaAliasBo.getTuplas(now);
      }
      else {
        if (cdcodigocuentaliq.indexOf("#") != -1) {
          cdcodigocuentaliq = cdcodigocuentaliq.replace("#", "");
          isinList = movCuentaAliasBo.getTuplasByNotCuenta(now, cdcodigocuentaliq);

        }
        isinList = movCuentaAliasBo.getTuplasByCuenta(now, cdcodigocuentaliq);
      }
    }
    return isinList;
  }

  public List<SaldoInicialPorCliente> traerIndependientesPorCliente(List<TuplaIsinAlias> isinIndependiente, Date now,
      String cdcodigocuentaliq) {
    List<SaldoInicialPorCliente> independientesDto = new ArrayList<SaldoInicialPorCliente>();
    Date bigBang = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    try {
      bigBang = sdf.parse("10000608");
    }
    catch (ParseException e) {
      LOG.warn(e.getMessage() + "Error parsing bigBang");
    }
    for (TuplaIsinAlias tupla : isinIndependiente) {
      SaldoInicialPorCliente saldoActual = new SaldoInicialPorCliente();
      saldoActual.setTotalEfectivos(BigDecimal.ZERO);
      BigDecimal titulos = BigDecimal.ZERO;

      List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(bigBang,
          DateUtils.addDays(now, 1), tupla.getIsin(), tupla.getCdaliass(), cdcodigocuentaliq);

      for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
        saldoActual.setTotalEfectivos(saldoActual.getTotalEfectivos().add(movAli.getImefectivo()));
        titulos = titulos.add(movAli.getTitulos());
      }

      if (listaMovAli.size() > 0) {
        Tmct0ali alias = aliasBo.findByCdaliass(tupla.getCdaliass(), Tmct0ali.fhfinActivos);
        tupla.fillDescali(alias);
        List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(tupla.getIsin());
        tupla.fillDescrisin(valores);
        SaldoInicial cliente = new SaldoInicial();
        cliente.setIsin(tupla.getIsin());
        cliente.setDescrisin(tupla.getDescrisin());
        cliente.setEfectivo(saldoActual.getTotalEfectivos());
        cliente.setTitulos(titulos);
        saldoActual.addGrupoBalanceCliente(cliente);
        saldoActual.setAlias(tupla.getCdaliass());
        saldoActual.setDescralias(tupla.getDescAli());
        // Hack: 2015.09.25: Petición de F. Leyún: Quitar los titulos==0.
        // if ( titulos.compareTo( BigDecimal.ZERO ) != 0 ||
        // saldoActual.getTotalEfectivos().compareTo( BigDecimal.ZERO ) != 0 ) {
        if (titulos.compareTo(BigDecimal.ZERO) != 0) {
          independientesDto.add(saldoActual);
        }
      }
    }

    return independientesDto;
  }

  private List<SaldoInicialPorIsin> traerIndependientesPorIsin(List<TuplaIsinAlias> isinIndependiente, Date now,
      String cdcodigocuentaliq) {
    List<SaldoInicialPorIsin> independientesDto = new ArrayList<SaldoInicialPorIsin>();
    Date bigBang = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    try {
      bigBang = sdf.parse("10000608");
    }
    catch (ParseException e) {
      LOG.warn(e.getMessage() + "Error parsing bigBang");
    }
    for (TuplaIsinAlias tupla : isinIndependiente) {
      SaldoInicialPorIsin saldoActual = new SaldoInicialPorIsin();
      saldoActual.setTotalEfectivos(BigDecimal.ZERO);
      saldoActual.setTotalTitulos(BigDecimal.ZERO);
      List<Tmct0MovimientoCuentaAlias> listaMovAli = movCuentaAliasBo.findAllByRangeFechaAndIsinAndAlias(bigBang,
          DateUtils.addDays(now, 1), tupla.getIsin(), tupla.getCdaliass(), cdcodigocuentaliq);

      for (Tmct0MovimientoCuentaAlias movAli : listaMovAli) {
        saldoActual.setTotalEfectivos(saldoActual.getTotalEfectivos().add(movAli.getImefectivo()));
        saldoActual.setTotalTitulos(saldoActual.getTotalTitulos().add(movAli.getTitulos()));
      }

      if (listaMovAli.size() > 0) {
        Tmct0ali alias = aliasBo.findByCdaliass(tupla.getCdaliass(), Tmct0ali.fhfinActivos);
        tupla.fillDescali(alias);
        List<Tmct0Valores> valores = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc(tupla.getIsin());
        tupla.fillDescrisin(valores);
        SaldoInicialIsin cliente = new SaldoInicialIsin();
        cliente.setAlias(tupla.getCdaliass());
        cliente.setDescAli(tupla.getDescAli());
        cliente.setEfectivo(saldoActual.getTotalEfectivos());
        cliente.setTitulos(saldoActual.getTotalTitulos());
        saldoActual.addGrupoBalanceISIN(cliente);
        saldoActual.setDescrisin(tupla.getDescrisin());
        saldoActual.setIsin(tupla.getIsin());
        // Hack: 2015.09.25: Petición de F. Leyún: Quitar los titulos==0.
        // if ( saldoActual.getTotalTitulos().compareTo( BigDecimal.ZERO ) != 0
        // || saldoActual.getTotalEfectivos().compareTo(
        // BigDecimal.ZERO ) != 0 ) {
        if (saldoActual.getTotalTitulos().compareTo(BigDecimal.ZERO) != 0) {
          independientesDto.add(saldoActual);
        }
      }
    }

    return independientesDto;
  }
}