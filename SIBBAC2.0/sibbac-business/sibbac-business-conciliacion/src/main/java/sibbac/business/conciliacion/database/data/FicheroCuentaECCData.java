package sibbac.business.conciliacion.database.data;

import static sibbac.common.utils.FormatDataUtils.convertDateToString;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.conciliacion.database.model.FicheroCuentaECC;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;


public class FicheroCuentaECCData {
	
	private static final Logger						LOG																= LoggerFactory
			.getLogger( FicheroCuentaECCData.class );
	
    private Long id;
    private String isin;
    private Long idCuentaLiquidacion;
    private String codCtaLiquidacion;
    private Date settlementdate;
    private BigDecimal titulos;
    private BigDecimal efectivo;
    private String descrIsin;
    private String mercado;
    private Character signoAnotacion;
    private Character flagError;
    private String fliquidacion;
    @Autowired
    private Tmct0ValoresBo valoresBO;
    
    public FicheroCuentaECCData(){
	//
    }
    
    public FicheroCuentaECCData(FicheroCuentaECC ficheroCuentaECC){
    	this.setCodCtaLiquidacion(ficheroCuentaECC.getCuentaLiquidacion().getCdCodigo());
    	String sIsin = ficheroCuentaECC.getIsin();
    	this.setIsin(sIsin);
    	String descripcionIsin = ficheroCuentaECC.getDescrIsin();
    	if (descripcionIsin == null || "".equals(descripcionIsin.trim()) || "null".equals(descripcionIsin))
    	{
    		try
    		{
    			descripcionIsin = valoresBO.findDescIsinByIsin(sIsin);
    		}catch (Exception ex)
    		{
    			LOG.error(ex.getMessage(), ex);
    			descripcionIsin = "";
    		}
    	}	
    	this.setDescrIsin(descripcionIsin);
    	this.setEfectivo(ficheroCuentaECC.getEfectivo());
    	this.setFlagError(ficheroCuentaECC.getFlagError());
    	this.setId(ficheroCuentaECC.getId());
    	this.setMercado(ficheroCuentaECC.getMercado());
    	this.setSettlementdate(ficheroCuentaECC.getSettlementdate());
    	this.setSignoAnotacion(ficheroCuentaECC.getSignoAnotacion());
    	this.setTitulos(ficheroCuentaECC.getTitulos());
    }
    
    public FicheroCuentaECCData(String mercado, Date settlementdate, String isin, String descIsin, String codCtaLiq, 
	    BigDecimal titulos, BigDecimal efectivo){
	this.settlementdate = settlementdate;
	this.isin = isin;
	this.descrIsin = descIsin;
	this.codCtaLiquidacion = codCtaLiq;
	this.titulos = titulos;
	this.efectivo = efectivo;
	this.mercado = mercado;
  fliquidacion = convertDateToString(settlementdate);
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getIsin() {
        return isin;
    }
    public void setIsin(String isin) {
        this.isin = isin;
    }
    public Long getIdCuentaLiquidacion() {
        return idCuentaLiquidacion;
    }
    public void setIdCuentaLiquidacion(Long idCuentaLiquidacion) {
        this.idCuentaLiquidacion = idCuentaLiquidacion;
    }
    
    public String getCodCtaLiquidacion() {
        return codCtaLiquidacion;
    }
    public void setCodCtaLiquidacion(String codCtaLiquidacion) {
        this.codCtaLiquidacion = codCtaLiquidacion;
    }
    public Date getSettlementdate() {
        return settlementdate;
    }
    public void setSettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }
    public BigDecimal getTitulos() {
        return titulos;
    }
    public void setTitulos(BigDecimal titulos) {
        this.titulos = titulos;
    }
    public BigDecimal getEfectivo() {
        return efectivo;
    }
    public void setEfectivo(BigDecimal efectivo) {
        this.efectivo = efectivo;
    }
    public String getDescrIsin() {
        return descrIsin;
    }
    public void setDescrIsin(String descrIsin) {
        this.descrIsin = descrIsin;
    }
    public String getMercado() {
        return mercado;
    }
    public void setMercado(String mercado) {
        this.mercado = mercado;
    }
	public Character getSignoAnotacion() {
		return signoAnotacion;
	}
	public void setSignoAnotacion(Character signoAnotacion) {
		this.signoAnotacion = signoAnotacion;
	}
	public Character getFlagError() {
		return flagError;
	}
	public void setFlagError(Character flagError) {
		this.flagError = flagError;
	}
  
	public String getFliquidacion() {
	  return fliquidacion;
	}
	
}