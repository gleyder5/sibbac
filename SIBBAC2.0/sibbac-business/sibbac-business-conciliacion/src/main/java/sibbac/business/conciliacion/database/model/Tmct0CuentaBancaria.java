package sibbac.business.conciliacion.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * author: NEORIS
 */
@Entity
@Table(name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.CUENTA_BANCARIA)
public class Tmct0CuentaBancaria extends ATable<Tmct0CuentaBancaria> {
  private static final long serialVersionUID = 4268362214822547118L;

  @Column(name = "IBAN", length = 34, unique = true)
  private String iban;

  @Column(name = "DESC_SHORT", length = 25, nullable = true)
  private String descCorta;

  /**
   * @return Devuelve la descripción corta de la cuenta bancaria
   */
  public String getDescCorta() {
    return descCorta;
  }

  /**
   * @param Descripción corta de la cuenta bancaria
   */
  public void setDescCorta(String descCorta) {
    this.descCorta = descCorta;
  }
}
