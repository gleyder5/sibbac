package sibbac.business.conciliacion.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.dao.FicheroCuentaECCRepository;
import sibbac.business.conciliacion.database.dao.specifications.FicheroCuentaEccSpecifications;
import sibbac.business.conciliacion.database.data.FicheroCuentaECCData;
import sibbac.business.conciliacion.database.filter.FicheroCuentaECCFilter;
import sibbac.business.conciliacion.database.filter.FicheroCuentaECCQuery;
import sibbac.business.conciliacion.database.model.FicheroCuentaECC;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bo.QueryBo;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R00;
import sibbac.pti.messages.an.R04;

@Service
public class FicheroCuentaECCBo extends AbstractBo<FicheroCuentaECC, Long, FicheroCuentaECCRepository> {

  private static final Logger LOG = LoggerFactory.getLogger(FicheroCuentaECCBo.class);

  @Value("${sibbac.nacional.defaults.cta.compensacion}")
  private String codCuentaCompensacion;

  @Autowired
  private Tmct0CuentaLiquidacionBo cuentaLiquidacionBo;
  
  @Autowired
  private Tmct0ValoresBo valoresBO;
  
  @Autowired
  private QueryBo queryBo;
  
  private String mercado;

  @Autowired
  public void setCfg(Tmct0cfgBo cfgBo) {
    mercado = cfgBo.getIdPti();
  }

  @Transactional
  public List<FicheroCuentaECCData> findMovimientos(Date fechadesde, Date fechahasta, String codigoCuentaLiquidacion,
      String isin, String camaraCompensacion) {
    List<FicheroCuentaECCData> listaData = new ArrayList<FicheroCuentaECCData>();
    List<FicheroCuentaECC> listaFicheroCuentaEcc = this.dao.findAll(
        FicheroCuentaEccSpecifications.listadoFiltrado(fechadesde, fechahasta, codigoCuentaLiquidacion, isin, 
            camaraCompensacion), new Sort(Sort.Direction.ASC, "isin"));
    for (FicheroCuentaECC ficheroCuentaEcc : listaFicheroCuentaEcc) {
      listaData.add(new FicheroCuentaECCData(ficheroCuentaEcc));
    }
    return listaData;
  }

  public List<FicheroCuentaECCData> findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacionAndIsin(
      Date fechadesde, Date fechahasta, String codigoCuentaLiquidacion, String isin) {
    List<FicheroCuentaECCData> resultList = dao
        .findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacionAndIsin(fechadesde, fechahasta,
            codigoCuentaLiquidacion, isin);
    return resultList;
  }

  public List<FicheroCuentaECCData> findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacion(Date fechadesde,
      Date fechahasta, String codigoCuentaLiquidacion) {
    try {
      return dao.findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacion(fechadesde, fechahasta,
          codigoCuentaLiquidacion);
    }
    catch (PersistenceException | InvalidDataAccessResourceUsageException ex) {
      LOG.error(ex.getMessage(), ex);
      return Collections.emptyList();
    }
  }

  /**
   * Procesa una línea AN de un fichero PTI
   * @param an Línea AN
   * @param version Versión del formato del fichero PTI
   */
  @Transactional
  public void procesarAnSaldoCuenta(AN an, PTIMessageVersion version) { 
    final List<PTIMessageRecord> listR04, listR00;
    final int s;
    FicheroCuentaECC movimientoEntrada, movimientoSalida;
    String descripcionIsin;
    Tmct0CuentaLiquidacion cl;
    R00 r00;
    R04 r04;
    
    LOG.debug("[procesarAnSaldoCuenta] Inicio");
    listR00 = an.getAll(PTIMessageRecordType.R00);
    listR04 = an.getAll(PTIMessageRecordType.R04);
    s = listR00.size();
    for(int i = 0; i < s; i++) {
      r00 = (R00) listR00.get(i);
      r04 = (R04) listR04.get(i);
      cl = recuperaCuentaLiquidacion(r00.getCuentaLiquidacion());
      movimientoEntrada = movimientoSalida = null;
      if(cl != null) {
        //Si no se encuentra la cuenta de liquidación, no se buscan movimientos para actualizar
        movimientoEntrada = findFicheroCuentaECCFechaLiq(r00.getCodigoDeValor(), 
            cl.getId(), r04.getFechaLiquidacion(), 'E');
        movimientoSalida = findFicheroCuentaECCFechaLiq(r00.getCodigoDeValor(), 
            cl.getId(), r04.getFechaLiquidacion(), 'S');
      }
      if(movimientoEntrada == null || movimientoSalida == null) {
        //Se crean los movimientos en caso de no existir.
        descripcionIsin = valoresBO.findDescIsinByIsin(r00.getCodigoDeValor());
        if(movimientoEntrada == null) {
          movimientoEntrada = generaMovimientoFicheroCuentaECC(r00, r04, descripcionIsin, cl, 'E');
        }
        if(movimientoSalida == null) {
          movimientoSalida = generaMovimientoFicheroCuentaECC(r00, r04, descripcionIsin, cl, 'S');
        }
      }
      if(aplicaSaldos(movimientoEntrada,
          r04.getSaldoNetoCompradorValoresNominal(), 
          r04.getEfectivoDelSaldoNetoCompradorDeValores(), 
          r04.getSaldoBrutoCompradorValoresNominal(), 
          r04.getEfectivoDelSaldoBrutoCompradorDeValores(), 'E')) {
        save(movimientoEntrada);
      }
      if(aplicaSaldos(movimientoSalida,
          r04.getSaldoNetoVendedorValoresNominal(),
          r04.getEfectivoDelSaldoNetoVendedorDeValores(),
          r04.getSaldoBrutoVendedorValoresNominal(),
          r04.getEfectivoDelSaldoBrutoVendedorDeValores(), 'S')) {
        save(movimientoSalida);
      }
    }
    LOG.info("[procesarAnSaldoCuenta] Procesados {} registros", s);
  }
  

  public List<FicheroCuentaECCData> findByFilter(FicheroCuentaECCFilter filter) throws SIBBACBusinessException {
    return queryBo.executeQueryNative(new FicheroCuentaECCQuery(filter), FicheroCuentaECCData.class);
  }
  
  /**
   * Se aplican los saldos al movimiento
   * @param movimientoCVECC movimiento al que se le aplican los saldos, puede ser nuevo o existente previamente.
   * @param titulosNeto neto de titulos
   * @param efectivoNeto neto de efectivo
   * @param titulosBruto bruto de titulo, se utiliza si los valores de neto son nulos o iguales a cero.
   * @param efectivoBruto bruto de efectivo, se utiliza si los valores de neto son nulos o iguales a cero.
   * @param signoAnotacion Signo de la anotación: 'E' Entrada o 'S' salida
   * @return verdadero en caso de que se puedan asignar los saldos al movimiento, falso en caso contrario.
   */
  private boolean aplicaSaldos(final FicheroCuentaECC movimientoCVECC, BigDecimal titulosNeto, BigDecimal efectivoNeto,
      BigDecimal titulosBruto, BigDecimal efectivoBruto, char signoAnotacion) {
    final BigDecimal titulos, efectivo;

    LOG.debug("[aplicaSaldos] Inicio para {}", movimientoCVECC);
    if(titulosNeto != null && !titulosNeto.equals(BigDecimal.ZERO)
        && efectivoNeto != null && !efectivoNeto.equals(BigDecimal.ZERO)) {
      titulos = signoAnotacion == 'S' ? titulosNeto.negate() : titulosNeto;
      efectivo = signoAnotacion == 'E' ? efectivoNeto.negate() : efectivoNeto;
    }
    else if(titulosBruto != null && !titulosBruto.equals(BigDecimal.ZERO) &&
        efectivoBruto != null && !efectivoBruto.equals(BigDecimal.ZERO)){
      titulos = signoAnotacion == 'S' ? titulosBruto.negate() : titulosBruto;
      efectivo =  signoAnotacion == 'E' ? efectivoBruto.negate() : efectivoBruto;
    }
    else {
      LOG.warn("[aplicaSaldos] Se omite la operacion {}", movimientoCVECC);
      return false;
    }
    movimientoCVECC.setTitulos(titulos);
    movimientoCVECC.setEfectivo(efectivo);
    LOG.debug("[aplicaSaldos] Se aplicaron saldos exitosamente a {}: titulos: {}, efectivo: {}",
        movimientoCVECC, titulos, efectivo);
    return true;
  }

  /**
   * Genera un objeto de tipo FicheroCuentaECC a partir de la información proveniente del fichero PTI
   * @param r00 Valores de los campos de registro R00
   * @param r04 Valores de los campos de registro R04
   * @param descripcionIsin texto descriptivo del nombre del valor
   * @param cuentaLiquidacion Objeto que enlaza a la cuenta de liquidación a utilizar
   * @param signoAnotacion Signo de la anotación: 'E' entrada, 'S' salida
   * @return un nuevo objeto inicializado, que deberá ser persistido.
   */
  private FicheroCuentaECC generaMovimientoFicheroCuentaECC(R00 r00, R04 r04,
      String descripcionIsin, Tmct0CuentaLiquidacion cuentaLiquidacion, char signoAnotacion) {
    final FicheroCuentaECC movimientoCVECC;
    
    movimientoCVECC = new FicheroCuentaECC();
    movimientoCVECC.setIsin(r00.getCodigoDeValor());
    movimientoCVECC.setDescrIsin(descripcionIsin);
    movimientoCVECC.setCuentaLiquidacion(cuentaLiquidacion);
    movimientoCVECC.setSettlementdate(r04.getFechaLiquidacion());
    movimientoCVECC.setMercado(mercado);
    movimientoCVECC.setSignoAnotacion(signoAnotacion);
    movimientoCVECC.setFlagError(cuentaLiquidacion == null ? '1' : '0'); // flag de error.
    return movimientoCVECC;
  }
  
  /**
   * Intenta recuperar la cuenta de liquidacion con codigo sCuentaLiquidacion. En 
   * caso de no encontrarla entonces retorna un nulo
   * @param sCuentaLiquidacion codigo de la cuenta de liquidacion
   * @return la cuenta de liquidacion buscada si existe, o la cuenta de liquidacion por 
   * defecto si el parametro sCuentaLiquidacion es vacío, o nulo si no aplica ninguno 
   * de los dos casos
   */
  private Tmct0CuentaLiquidacion recuperaCuentaLiquidacion(String sCuentaLiquidacion) {
    Tmct0CuentaLiquidacion cuentaDeLiquidacion = null;
    Tmct0CuentasDeCompensacion objTmct0CuentasDeCompensacion;
    
    if (!sCuentaLiquidacion.trim().isEmpty()) {
      cuentaDeLiquidacion = cuentaLiquidacionBo.findByCuentaIberclear(sCuentaLiquidacion);
    } 
    else {
      objTmct0CuentasDeCompensacion = cuentaLiquidacionBo.findByCdCodigo(codCuentaCompensacion);
      cuentaDeLiquidacion = objTmct0CuentasDeCompensacion.getTmct0CuentaLiquidacion();
    }
    if(cuentaDeLiquidacion != null) {
      return cuentaDeLiquidacion;
    }
    else {
      LOG.info(
          "[recuperaCuentaLiquidacion] No se ha localizado la cuenta de liquidacion correspondiente a la cuenta AN: [{}] ", 
          sCuentaLiquidacion);
      return null;
    }
  }
  
  /**
   * Recupera solo el primer movimiento en caso de haber varios. Se incluye por robustez.
   * @param isin codigo del valor
   * @param codigoCuentaLiquidacion cuenta de liquidación del movimiento
   * @param fechaLiquidacion fecha de la liquidacion
   * @param signoAnotacion signo de la anotacion
   * @return El movimiento, o nulo si no existe
   */
  private FicheroCuentaECC findFicheroCuentaECCFechaLiq(String isin, long codigoCuentaLiquidacion, 
      Date fechaLiquidacion, char signoAnotacion) {
    final List<FicheroCuentaECC> l;
    
    l = dao.findFicheroCuentaECCFechaLiq(isin, codigoCuentaLiquidacion, fechaLiquidacion, signoAnotacion);
    if(l.isEmpty()) {
      return null;
    }
    return l.get(0);
  }
}
