package sibbac.business.conciliacion.tasks;

import java.io.File;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.conciliacion.database.bo.MovimientoCuentaVirtualS3Bo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_CONCILIACION.NAME, name = Task.GROUP_CONCILIACION.JOB_FICHERO_S3_INPUT, interval = 1, delay = 0, intervalUnit = IntervalUnit.MINUTE)
public class TaskFicheroS3 extends WrapperTaskConcurrencyPrevent {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

    /** Referencia para la inserción de logs. */
    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskFicheroS3.class);

    /** Nombre del fichero de configuración del proceso. */
    private static final String FICHERO_S3 = "conciliacion.fichero.s3";
    private static final String TAG = TaskFicheroS3.class.getName();

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

    /** Ruta al fichero de configuración del proceso. */
    @Value("${" + FICHERO_S3 + "}")
    private String path;

    /**
     * Business object para la tabla
     * <code>TMCT0_MOVIMIENTO_CUENTA_VIRTUAL_S3</code>.
     */
    @Autowired
    private MovimientoCuentaVirtualS3Bo mcvs3Bo;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.tasks.SIBBACTask#execute()
     */
    @Override
    public void executeTask() {
	LOG.info("[TaskFicheroS3] Path: [{}]", path);
	try {
	    if (path == null || path.trim().length() == 0) {
		LOG.warn("[TaskFicheroS3] No se ha detectado ninguna ruta (path) valida.");
	    } else {
		File fichero = new File(path);
		if (fichero == null || !fichero.exists()) {
		    LOG.debug("[TaskFicheroS3] No se ha detectado ningún fichero.");
		} else {
		    LOG.debug("[TaskFicheroS3] Procesando el archivo [{}]...",
			    path);
		    try {
			mcvs3Bo.procesaArchivo(fichero);
			LOG.debug(TAG
				+ " :: execute Tarea cargar Fichero S3 terminada.");
		    } catch (Exception e) {
			LOG.error("[TaskFicheroS3] ERROR({}): {}", e.getClass()
				.getName(), e.getMessage());
			for (StackTraceElement ste : e.getStackTrace()) {
			    LOG.error("[TaskFicheroS3] {}", ste);
			} // for
			throw e;
		    } // catch
		} // else
	    } // else

	} catch (Exception ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw ex;
	}
    } // execute

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.CONCILIACION_S3_FILE_LOAD;
	
    }

} // TaskFicheroS3
