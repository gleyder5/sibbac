package sibbac.business.conciliacion.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.Norma43ConceptoDao;
import sibbac.business.conciliacion.database.model.Norma43Concepto;
import sibbac.database.bo.AbstractBo;

@Service
public class Norma43ConceptoBo extends AbstractBo<Norma43Concepto, Long, Norma43ConceptoDao> {

} // Norma43ConceptoBo
