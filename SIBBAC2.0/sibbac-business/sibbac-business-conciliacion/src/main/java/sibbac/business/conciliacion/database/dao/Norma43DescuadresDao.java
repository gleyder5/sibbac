package sibbac.business.conciliacion.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;

@Repository
public interface Norma43DescuadresDao extends JpaRepository<Norma43Descuadres, Long> {

  @Query( "SELECT DISTINCT descuadres.cuenta FROM Norma43Descuadres descuadres ORDER BY descuadres.cuenta ASC")
  public List<String> findDistinctCuenta();
  
  @Query( "SELECT DISTINCT descuadres.sentido FROM Norma43Descuadres descuadres ORDER BY descuadres.sentido ASC")
  public List<String> findDistinctSentido();
  
  @Query( "SELECT DISTINCT descuadres.refOrden FROM Norma43Descuadres descuadres ORDER BY descuadres.refOrden ASC")
  public List<String> findDistinctRefOrden();
  
  @Query("SELECT new sibbac.business.conciliacion.database.dto.Norma43DescuadresDTO(dn43.cuenta, dn43.refFecha, dn43.refOrden, dn43.sentido, dn43.tradeDate, dn43.settlementDate, dn43.importeNorma43, dn43.importeSibbac, dn43.tipoImporte, dn43.nombreEstadoAsignacionAlc, dn43.nombreEstadoContabilidadAlc, dn43.nombreEstadoEntrecAlc, dn43.nuorden, dn43.nbooking, dn43.nucnfclt, dn43.nucnfliq, dn43.diferencia, dn43.comentario, dn43.procesado) FROM Norma43Descuadres dn43 WHERE dn43.cuenta = :cuenta AND dn43.tradeDate BETWEEN :fechaDesde AND :fechaHasta")
  public List<Norma43DescuadresDTO> findAllByCuentaAndBetweenTradeDate(@Param("cuenta") String cuenta,
                                                                       @Param("fechaDesde") Date fechaDesde,
                                                                       @Param("fechaHasta") Date fechaHasta); 
} 
