package sibbac.business.conciliacion.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.Norma43Concepto;


@Repository
public interface Norma43ConceptoDao extends JpaRepository< Norma43Concepto, Long > {
}
