package sibbac.business.conciliacion.database.bo;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.Tmct0DiferenciaConciliacionEfectivoDAO;
import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionEfectivo;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0DiferenciaConciliacionEfectivoBO extends
		AbstractBo< Tmct0DiferenciaConciliacionEfectivo, Long, Tmct0DiferenciaConciliacionEfectivoDAO > {

	public List< Tmct0DiferenciaConciliacionEfectivo > findByTradeDateGreaterThanEqualOrderByTradeDate( Date tradeDate ) {
		List< Tmct0DiferenciaConciliacionEfectivo > resultList = null;
		if ( tradeDate != null ) {
			resultList = dao.findByTradeDateGreaterThanEqualOrderByTradeDate( tradeDate );
		}
		return resultList;
	}

	public Tmct0DiferenciaConciliacionEfectivo findByTradeDateAndIsinAndTmct0CuentasDeCompensacionAndSentido( Date tradeDate, String isin,
			Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion, String sentido ) {
		Tmct0DiferenciaConciliacionEfectivo diferencia = null;
		if ( tradeDate != null && tmct0CuentasDeCompensacion != null && isin != null && !"".equals( isin ) && sentido != null
				&& !"".equals( isin ) ) {
			diferencia = dao.findByTradeDateAndIsinAndTmct0CuentasDeCompensacionAndSentido( tradeDate, isin, tmct0CuentasDeCompensacion,
					sentido );
		}
		return diferencia;
	}
}
