package sibbac.business.conciliacion.database.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDaoImpl;
import sibbac.business.conciliacion.database.data.Norma43MovimientoData;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.database.bo.AbstractBo;

@Service
public class Norma43MovimientoBo extends AbstractBo<Norma43Movimiento, Long, Norma43MovimientoDao> {

  @Autowired
  private Norma43MovimientoDaoImpl daoImpl;

  public List<Norma43Movimiento> findByIdCuentaGroupByTradeDateOrderByImporteDesc(int codigoCuentaCompensacion,
                                                                                  Map<String, Serializable> filtros) {
    List<Norma43Movimiento> resultList = null;
    if (codigoCuentaCompensacion != 0) {
      resultList = daoImpl.findByIdCuentaGroupByTradeDateOrderByImporteDesc(codigoCuentaCompensacion, filtros);
    }
    return resultList;
  }

  public List<Norma43MovimientoData> findAllIntoData() {
    return this.dao.findAllIntoData();
  }

  public List<Norma43MovimientoData> findAllIntoDataContratacion(Date fcontratacionDe, Date fcontratacionA) {
    return this.dao.findAllIntoDataContratacion(fcontratacionDe, fcontratacionA);
  }

  public List<Norma43MovimientoData> findAllIntoDataLiquidacion(Date fliquidacionDe, Date fliquidacionA) {
    return this.dao.findAllIntoDataLiquidacion(fliquidacionDe, fliquidacionA);
  }

  public List<Norma43MovimientoData> findAllIntoDataFechas(Date fcontratacionDe,
                                                           Date fcontratacionA,
                                                           Date fliquidacionDe,
                                                           Date fliquidacionA) {
    return this.dao.findAllIntoDataFechas(fliquidacionDe, fliquidacionA, fcontratacionDe, fcontratacionA);
  }

} // Norma43MovimientoBo
