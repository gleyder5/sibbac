package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.conciliacion.database.bo.MovimientoCuentaVirtualBo;
import sibbac.business.conciliacion.database.dto.IsinDTO;
import sibbac.business.conciliacion.database.dto.MovimientoCuentaVirtualDTO;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0SaldoInicialCuentaBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.data.SaldoInicialCuentaData;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * The Class SIBBACServiceConciliacionBalance.
 */
@SIBBACService
public class SIBBACServiceConciliacionBalance implements SIBBACServiceBean,
	ISIBBACServiceConciliacionBalance {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory
	    .getLogger(SIBBACServiceConciliacionBalance.class);

    @Autowired
    private MovimientoCuentaVirtualBo movimientoCuentaVirtualBo;
    @Autowired
    private Tmct0MovimientoCuentaAliasBo movimientoCuentaAliasBo;
    @Autowired
    private Tmct0CuentaLiquidacionBo cuentaLiqBo;
    @Autowired
    private Tmct0SaldoInicialCuentaBo saldoBo;
    @Autowired
    private Tmct0ValoresBo valoresBo;
    ConciliacionUtil util = ConciliacionUtil.getInstance();

    /*
     * (non-Javadoc)
     * 
     * @see
     * sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.
     * WebRequest)
     */
    @Override
    public WebResponse process(WebRequest webRequest)
	    throws SIBBACBusinessException {
	LOG.debug("ConciliacionBalance:Processing a web request...");

	String command = webRequest.getAction();
	Map<String, String> params = webRequest.getFilters();
	Map<String, Object> result = null;
	WebResponse response = new WebResponse();
	LOG.debug("ConciliacionBalance: Command.: [{}]", command);
	switch (command) {
	case CMD_GET_BALANCE:
	    // result = getSaldoInicialPorIsin(params);
	    try {
		result = getBalance(params);
	    } catch (PersistenceException | IllegalArgumentException ex) {
		response.setError(ex.getMessage());
	    }
	    break;
	case CMD_GET_ISIN:
	    result = getIsin();
	    break;
	default:
	    if (result == null) {
		result = new HashMap<String, Object>();
	    }
	}
	response.setResultados(result);
	return response;
    }

    private Map<String, Object> getBalance(Map<String, String> params)
	    throws PersistenceException, IllegalArgumentException {
    	
    String ctaLiq = (String) params.get(FIELD_BALANCE_CUENTA_LIQUIDACION_INT);
    if(ctaLiq.indexOf("#") != -1){
    	params.remove(FIELD_BALANCE_CUENTA_LIQUIDACION_INT);
    	params.put(FIELD_BALANCE_CUENTA_LIQUIDACION_INT, ctaLiq.replace("#", ""));
    }
    	            
	Map<String, Serializable> filtros = util.normalizeFilters(params);
	Map<String, Object> result = new HashMap<String, Object>();

	Date fecha = null;
	String isinFiltro = null;
	String cdcodigocuentaliq = null;
	fecha = (Date) filtros.get(FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT);
	isinFiltro = (String) filtros.get(FIELD_BALANCE_ISIN_IN);

	Long idCtaLiq = null;
	
	if(ctaLiq != null && !ctaLiq.equals("")){
		if(ctaLiq.indexOf("#") != -1)
			idCtaLiq = Long.valueOf(ctaLiq.replace("#", ""));
		else
			idCtaLiq = (Long) filtros.get(FIELD_BALANCE_CUENTA_LIQUIDACION_INT);
		
		cdcodigocuentaliq = cuentaLiqBo.findById(idCtaLiq).getCdCodigo().trim();
	}
	
	
	if(ctaLiq.indexOf("#") != -1)
		cdcodigocuentaliq = "#" + cdcodigocuentaliq;
	
	filtros.remove(FIELD_BALANCE_CUENTA_LIQUIDACION_INT);
	filtros.put(FIELD_BALANCE_COD_CUENTA_LIQUIDACION, cdcodigocuentaliq);

	Date now = new Date();
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	// solo se necesita el dia el mes y el año
	try {
	    now = formatter.parse(formatter.format(now));
	    fecha = formatter.parse(formatter.format(fecha));
	} catch (ParseException e) {
	    LOG.error(e.getMessage(), e);
	}

	// Dependiendo de los parametros que se reciban de los filtros se
	// obtienen los alias e isines a buscar
	Date fechaAumentada = DateUtils.addDays(fecha, 1);
	// Se añade el filtro fechaLiquidacion con el aumento de un dia, para
	// extraer el saldo inicial
	filtros.put(FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT, fechaAumentada);
	List<SaldoInicialCuentaData> listaSaldos = saldoBo
		.findAllGroupByIsinAndCodCuentaliqAndFLiquidacionOrderByIsinAndFecha(filtros);
	// Se vuelve a introducir la fecha sin aumentar, para extraer los
	// movimientos que no hayan sido procesados.
	filtros.put(FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT, fecha);
	List<MovimientoCuentaAliasData> listaMov = movimientoCuentaAliasBo
		.findAllMovGroupBySentido(filtros, "");
	// resultado
	List<MovimientoCuentaVirtualDTO> dtoList = new ArrayList<MovimientoCuentaVirtualDTO>();
	if (!CollectionUtils.isEmpty(listaSaldos)
		&& CollectionUtils.isEmpty(listaMov)) {
	    for (SaldoInicialCuentaData saldo : listaSaldos) {
			MovimientoCuentaVirtualDTO dto = new MovimientoCuentaVirtualDTO();
			if (saldo.getTitulos().longValue() != 0) {
			    addSaldo(dtoList, saldo, dto, fecha);
			}

	    }
	} else if (CollectionUtils.isEmpty(listaSaldos)
		&& !CollectionUtils.isEmpty(listaMov)) {
	    for (MovimientoCuentaAliasData data : listaMov) {
			MovimientoCuentaVirtualDTO dto = new MovimientoCuentaVirtualDTO();
			if (data.getTitulos().longValue() != 0) {
			    addMovimiento(dtoList, data, dto, fecha);
			}
	    }
	} else if (!CollectionUtils.isEmpty(listaSaldos)
		&& !CollectionUtils.isEmpty(listaMov)) {
	    conjugarSaldoInicialYMovimientos(listaSaldos, listaMov, dtoList,
		    fecha);
	}
	result.put(RESULT_BALANCE, dtoList);
	return result;
    }

    /**
     * agrupa los saldos iniciales con los movimientos que todavía no han sido
     * integrados con el saldo inicial.
     * 
     * @param listaSaldos
     * @param listaMov
     * @param dtoList
     * @param fecha
     */
    private void conjugarSaldoInicialYMovimientos(
	    List<SaldoInicialCuentaData> listaSaldos,
	    List<MovimientoCuentaAliasData> listaMov,
	    List<MovimientoCuentaVirtualDTO> dtoList, Date fecha) {
	Iterator<MovimientoCuentaAliasData> itData = listaMov.iterator();
	List<MovimientoCuentaAliasData> listMovCasados = new LinkedList<MovimientoCuentaAliasData>();
	for (SaldoInicialCuentaData saldo : listaSaldos) {
	    boolean encontrado = false;
	    while (itData.hasNext() && !encontrado) {
		MovimientoCuentaAliasData data = itData.next();
		if (saldo.getCodcuentaliq().equals(data.getCdcodigocuentaliq())
			&& saldo.getIsin().equals(data.getIsin())) {
		    encontrado = true;
		    MovimientoCuentaVirtualDTO dto = new MovimientoCuentaVirtualDTO();
		    dto.setIsin(saldo.getIsin());
		    dto.setCdCodigo(saldo.getCodcuentaliq());
		    dto.setTitulos(saldo.getTitulos().add(data.getTitulos()));
		    dto.setEfectivo(saldo.getEfectivo().add(data.getEfectivo()));
		    dto.setDescrIsin(data.getDescrIsin());
		    dto.setSettlementdate(FormatDataUtils
			    .convertDateToString(fecha));
		    if (dto.getTitulos().longValue() != 0) {
			dtoList.add(dto);
		    }
		}
		if (encontrado) {
		    listMovCasados.add(data);
		    itData.remove();
		}
	    }
	    // Si el saldo no se conjuga con ningún movimiento, se añade a la
	    // lista
	    if (!encontrado && saldo.getTitulos().longValue() != 0) {
		addSaldo(dtoList, saldo, new MovimientoCuentaVirtualDTO(),
			fecha);
	    }
	    encontrado = false;
	}
	// Si queda algún movimiento sin conjugar, hay que meterlo también en
	// la lista resultante
	for (MovimientoCuentaAliasData data : listaMov) {
	    if (!listMovCasados.contains(data)) {
		MovimientoCuentaVirtualDTO dto = new MovimientoCuentaVirtualDTO();
		if (data.getTitulos().longValue() != 0) {
		    addMovimiento(dtoList, data, dto, fecha);
		}
	    }
	}
    }

    /**
     * Añade un movimiento a la lista de balance resultante.
     * 
     * @param dtoList
     * @param data
     * @param dto
     * @param fecha
     */
    private void addMovimiento(List<MovimientoCuentaVirtualDTO> dtoList,
	    MovimientoCuentaAliasData data, MovimientoCuentaVirtualDTO dto,
	    Date fecha) {
	dto.setCdCodigo(data.getCdcodigocuentaliq());
	dto.setSettlementdate(FormatDataUtils.convertDateToString(fecha));
	dto.setIsin(data.getIsin());
	dto.setDescrIsin(data.getDescrIsin());
	dto.setTitulos(data.getTitulos());
	dto.setEfectivo(data.getEfectivo());
	dtoList.add(dto);
    }

    /**
     * Añade un saldo inicial a la lista resultante de balance
     * 
     * @param dtoList
     * @param saldo
     * @param dto
     * @param fecha
     */
    private void addSaldo(List<MovimientoCuentaVirtualDTO> dtoList,
	    SaldoInicialCuentaData saldo, MovimientoCuentaVirtualDTO dto,
	    Date fecha) {
	dto.setCdCodigo(saldo.getCodcuentaliq());
	dto.setSettlementdate(FormatDataUtils.convertDateToString(fecha));
	dto.setIsin(saldo.getIsin());
	dto.setTitulos(saldo.getTitulos());
	dto.setEfectivo(saldo.getEfectivo());
	dto.setDescrIsin(saldo.getDescisin());
	dtoList.add(dto);
    }

    /**
     * Obtiene una lista de isines.
     * 
     * @return
     */
    public Map<String, Object> getIsin() {
	Map<String, Object> result = new HashMap<String, Object>();
	List<IsinDTO> isinDTOs = movimientoCuentaVirtualBo.getIsin();
	result.put(RESULT_ISIN, isinDTOs);
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
     */
    @Override
    public List<String> getAvailableCommands() {
	return Arrays.asList(new String[] { CMD_GET_BALANCE });
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
     */
    @Override
    public List<String> getFields() {
	return Arrays.asList(new String[] { FIELD_BALANCE_ISIN_OUT,
		FIELD_BALANCE_ISIN_DESCRIPCION_OUT, FIELD_BALANCE_CUENTA_OUT,
		FIELD_BALANCE_TITULOS_OUT });
    }

}
