package sibbac.business.conciliacion.rest;


// TODO: Auto-generated Javadoc
/**
 * The Interface ISIBBACServiceConciliacionBalance.
 */
public interface ISIBBACServiceConciliacionBalance {

	/** The Constant CMD_GET_BALANCE. */
	public static final String	CMD_GET_BALANCE							= "getBalance";

	/** The Constant CMD_GET_ISIN. */
	public static final String	CMD_GET_ISIN							= "getIsin";

	/** The Constant RESULT_BALANCE. */
	public static final String	RESULT_BALANCE							= "result_balance";

	/** The Constant RESULT_GET. */
	public static final String	RESULT_ISIN								= "result_isin";

	/** The Constant FIELD_BALANCE_ISIN. */
	public static final String	FIELD_BALANCE_ISIN_OUT					= "cdisin";

	/** The Constant FIELD_BALANCE_ISIN_DESCRIPCION. */
	public static final String	FIELD_BALANCE_ISIN_DESCRIPCION_OUT		= "IsinDescripcion";

	/** The Constant FIELD_BALANCE_CUENTA. */
	public static final String	FIELD_BALANCE_CUENTA_OUT				= "cuentaLiquidacion";

	/** The Constant FIELD_BALANCE_TITULOS. */
	public static final String	FIELD_BALANCE_TITULOS_OUT				= "titulos";

	/** The Constant FIELD_BALANCE_ISIN_IN. */
	public static final String	FIELD_BALANCE_ISIN_IN					= "isin";

	/** The Constant FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT. */
	public static final String	FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT	= "fliquidacionDe";

	/** The Constant FIELD_BALANCE_FECHA_LIQUIDACION_DE_A_INT. */
	public static final String	FIELD_BALANCE_FECHA_LIQUIDACION_A_INT	= "fliquidacionA";

	/** The Constant FIELD_BALANCE_CUENTA_LIQUIDACION_INT. */
	public static final String	FIELD_BALANCE_CUENTA_LIQUIDACION_INT	= "idCuentaLiquidacion";
	public static final String 	FIELD_BALANCE_COD_CUENTA_LIQUIDACION = "cdcodigocuentaliq";

}
