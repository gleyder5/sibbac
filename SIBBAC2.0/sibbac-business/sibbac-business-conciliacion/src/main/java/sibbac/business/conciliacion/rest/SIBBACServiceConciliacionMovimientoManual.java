package sibbac.business.conciliacion.rest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoManualBo;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;
import sibbac.business.wrappers.database.model.Tmct0MovimientoManual;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * The Class SIBBACServiceConciliacionMovimientoManual.
 */
@SIBBACService
public class SIBBACServiceConciliacionMovimientoManual implements
  SIBBACServiceBean {

    private static final String SENTIDO_SALIDA = "S";

    private static final String SENTIDO_ENTRADA = "E";

    // nombre propiedades en conciliacion.properties
    /** The Constant RESULT_INSERT_MOV_MANUAL. */
    private static final String CMD_INSERT_MOV_MANUAL = "insertMovimientoManual";
    private static final String RESULT_INSERT_MOV_MANUAL = "result_movimiento_manual";

    /** The Constant MAN. */
    private static final String MAN = "MAN";

    // LOGGER
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory
      .getLogger(SIBBACServiceConciliacionMovimientoManual.class);

    /** The movmanual bo. */
    @Autowired
    private Tmct0MovimientoManualBo movmanualBo;

    /** The gestor periodicidades. */
    @Autowired
    private GestorPeriodicidades gestorPeriodicidades;

    /** The compensacion bo. */
    @Autowired
    private Tmct0CuentasDeCompensacionBo compensacionBo;

    /** The liquidacion bo. */
    @Autowired
    private Tmct0CuentaLiquidacionBo liquidacionBo;

    /** The mov cuenta alias bo. */
    @Autowired
    private Tmct0MovimientoCuentaAliasBo movCuentaAliasBo;

    /**
     * Instantiates a new SIBBAC service conciliacion movimiento manual.
     */
    public SIBBACServiceConciliacionMovimientoManual() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.
     * WebRequest)
     */
    @Override
    public WebResponse process(WebRequest webRequest) {
  LOG.debug("ConciliacionMovimientoManualService:Processing a web request...");
  WebResponse response = new WebResponse();
  final String action = webRequest.getAction();
  Map<String, Object> resultado = new HashMap<String, Object>();
  List<Map<String, String>> filtros = webRequest.getParams();
  Tmct0MovimientoManual entidad = null;

  if (filtros != null) {
      entidad = resolveParams(filtros);
      comprobarSigno(entidad);
  }
  if (entidad != null && action.equalsIgnoreCase(CMD_INSERT_MOV_MANUAL)) {
      resultado.put(RESULT_INSERT_MOV_MANUAL,
        insertMovimientoManualWrapper(entidad));
      response.setResultados(resultado);
  }

  return response;
    }

    private Tmct0MovimientoCuentaAlias convertTmct0MovimientoManual2Tmct0MovimientoCuentaAlias(
      Tmct0MovimientoManual mov, boolean titulos, boolean generaSaldo) {
  if (titulos) {
      return new Tmct0MovimientoCuentaAlias(mov.getFechaContratacion(),
        mov.getFechaLiquidacion(), MAN, mov.getIsin(),
        mov.getCamara(), mov.getTitulos(), mov.getEfectivo(), mov
          .getSentido().charAt(0), mov.getAlias(), "",
        mov.getDescripcionCuentaLiquidacion(), generaSaldo);
  } else {
      return new Tmct0MovimientoCuentaAlias(mov.getFechaContratacion(),
        mov.getFechaLiquidacion(), MAN, mov.getIsin(),
        mov.getCamara(), mov.getTitulos(), mov.getEfectivo(), mov
          .getSentido().charAt(0), mov.getAlias(), "",
        mov.getDescripcionCuentaLiquidacionEfectivo(), generaSaldo);
  }
    }

    /**
     * Insert movimiento manual wrapper.
     *
     * @param entidad
     *            the entidad
     * @return the string
     */
    private String insertMovimientoManualWrapper(Tmct0MovimientoManual entidad) {
  Boolean result = Boolean.TRUE;
  Boolean generaSaldo = true;

  // Si el movimiento tiene settlementdate posterior a hoy no hara nada
  // con saldo inicial
  // Se quedara en movimiento cuenta alias con su check de guardado en
  // saldo a cero
  // Se procesara cuando llegue el dia
  if (entidad.getFechaLiquidacion().after(new Date())) {
      generaSaldo = false;
  }

  try {
      if ((!"0".equals(entidad.getCuentaLiquidacion()) || !""
        .equals(entidad.getCuentaLiquidacion()))
        && ("0".equals(entidad.getCuentaLiquidacionEfectivo()) || ""
          .equals(entidad.getCuentaLiquidacionEfectivo()))) {
    // El movimiento es de TITULOS
    Tmct0MovimientoManual mm = new Tmct0MovimientoManual(entidad);
    mm.setCuentaLiquidacionEfectivo("");
    mm.setDescripcionCuentaLiquidacionEfectivo("");
    mm.setEfectivo(BigDecimal.ZERO);
    mm.setPrecio(BigDecimal.ZERO);
    insertMovimientoManual(mm, generaSaldo, false, false);
    Tmct0MovimientoCuentaAlias mca = convertTmct0MovimientoManual2Tmct0MovimientoCuentaAlias(
      mm, true, generaSaldo);
    movCuentaAliasBo.save(mca);
      } else if ((("0".equals(entidad.getCuentaLiquidacion()) || ""
        .equals(entidad.getCuentaLiquidacion())))
        && (!"0".equals(entidad.getCuentaLiquidacionEfectivo()) || !""
          .equals(entidad.getCuentaLiquidacionEfectivo()))) {
    // El movimiento es de EFECTIVO
    Tmct0MovimientoManual mm = new Tmct0MovimientoManual(entidad);
    mm.setCuentaLiquidacion("");
    mm.setDescripcionCuentaLiquidacion("");
    mm.setTitulos(BigDecimal.ZERO);
    mm.setPrecio(BigDecimal.ZERO);
    insertMovimientoManual(mm, generaSaldo, true, false);
    Tmct0MovimientoCuentaAlias mca = convertTmct0MovimientoManual2Tmct0MovimientoCuentaAlias(
      mm, false, generaSaldo);
    movCuentaAliasBo.save(mca);

      } else {

    // Caso en que metemos los dos
    if (entidad.getCuentaLiquidacion().trim()
      .equals(entidad.getCuentaLiquidacionEfectivo().trim())) {
        // El movimiento es de TITULOS Y EFECTIVO
        Tmct0MovimientoManual mm = new Tmct0MovimientoManual(
          entidad);
        mm.setPrecio(BigDecimal.ZERO);
        if (entidad.getTitulos().compareTo(BigDecimal.ZERO) != 0) {
      mm.setPrecio(entidad
        .getEfectivo()
        .divide(entidad.getTitulos(), 2,
          RoundingMode.HALF_UP).abs());
        }
        insertMovimientoManual(mm, generaSaldo, false, true);
        Tmct0MovimientoCuentaAlias mca = convertTmct0MovimientoManual2Tmct0MovimientoCuentaAlias(
          mm, true, generaSaldo);
        movCuentaAliasBo.save(mca);

    } else {
        // Se hacen dos movimientos.

        // TITULOS
        Tmct0MovimientoManual mmTitulo = new Tmct0MovimientoManual(
          entidad);
        mmTitulo.setCuentaLiquidacionEfectivo("");
        mmTitulo.setDescripcionCuentaLiquidacionEfectivo("");
        mmTitulo.setEfectivo(BigDecimal.ZERO);
        mmTitulo.setPrecio(BigDecimal.ZERO);
        if (entidad.getTitulos().compareTo(BigDecimal.ZERO) != 0) {
      mmTitulo.setPrecio(entidad
        .getEfectivo()
        .divide(entidad.getTitulos(), 2,
          RoundingMode.HALF_UP).abs());
        }
        insertMovimientoManual(mmTitulo, generaSaldo, false, false);
        Tmct0MovimientoCuentaAlias mcaTitulo = convertTmct0MovimientoManual2Tmct0MovimientoCuentaAlias(
          mmTitulo, true, generaSaldo);
        movCuentaAliasBo.save(mcaTitulo);

        // EFECTIVO
        Tmct0MovimientoManual mmEfectivo = new Tmct0MovimientoManual(
          entidad);
        mmEfectivo.setCuentaLiquidacion("");
        mmEfectivo.setDescripcionCuentaLiquidacion("");
        mmEfectivo.setTitulos(BigDecimal.ZERO);
        mmEfectivo.setPrecio(BigDecimal.ZERO);
        if (entidad.getTitulos().compareTo(BigDecimal.ZERO) != 0) {
      mmEfectivo.setPrecio(entidad
        .getEfectivo()
        .divide(entidad.getTitulos(), 2,
          RoundingMode.HALF_UP).abs());
        }
        insertMovimientoManual(mmEfectivo, generaSaldo, true, false);
        Tmct0MovimientoCuentaAlias mcaEfectivo = convertTmct0MovimientoManual2Tmct0MovimientoCuentaAlias(
          mmEfectivo, false, generaSaldo);
        movCuentaAliasBo.save(mcaEfectivo);

    }

      }
  } catch (PersistenceException | DataIntegrityViolationException
    | NoSuchMethodError ex) {
      LOG.error(ex.getMessage(), ex);
      result = Boolean.FALSE;
  }
  return result ? "Datos introducidos correctamente"
    : "Se ha producido un error";
    }

    /**
     * Insert movimiento manual.
     *
     * @param entidad
     *            the entidad
     * @return the string
     */
    private void insertMovimientoManual(Tmct0MovimientoManual entidad,
      boolean generaSaldo, boolean esSoloEfectivo, boolean mismaCuenta) {
  LOG.debug("ConciliacionMovimientoManualService:Insert Mov Manual...");
  Tmct0MovimientoManual mov = movmanualBo.insertMovimientoManual(entidad);
  if (generaSaldo) {
      if (esSoloEfectivo) {
    mov.setDescripcionCuentaLiquidacion(mov
      .getCuentaLiquidacionEfectivo());
      }
      if (!mismaCuenta) {
    // Si son de la misma cuenta solo actualizan saldo una vez
    movmanualBo.updateMovManual(mov);
      }
  }
    }

    /**
     * Insert movimiento manual.
     *
     * @param entidad
     *            the entidad
     * @return the string
     */
    private void updateSaldoMismaCuenta(Tmct0MovimientoManual entidad) {
  LOG.debug("ConciliacionMovimientoManualService:updateSaldoMismaCuenta...");

  // Si son de la misma cuenta solo actualizan saldo una vez
  movmanualBo.updateMovManual(entidad);
    }

    private void comprobarSigno(Tmct0MovimientoManual mov) {
  BigDecimal titulos = mov.getTitulos();
  BigDecimal efectivo = mov.getEfectivo();
  if (mov.getSentido().equals(SENTIDO_ENTRADA)) {
      if (titulos.compareTo(BigDecimal.ZERO) != 0
        && efectivo.compareTo(BigDecimal.ZERO) != 0) {
    // Si vienen los dos se pone titulos en positivo y efectivo en
    // negativo
    titulos = titulos.abs();
    efectivo = efectivo.negate();
      } else {
    // Si solamente devuelve uno de los dos iran en positivo
    if (titulos.compareTo(BigDecimal.ZERO) != 0) {
        titulos = titulos.abs();
    }
    if (efectivo.compareTo(BigDecimal.ZERO) != 0) {
        efectivo = efectivo.abs();
    }
      }
  } else if (mov.getSentido().equals(SENTIDO_SALIDA)) {
      if (titulos.compareTo(BigDecimal.ZERO) != 0
        && efectivo.compareTo(BigDecimal.ZERO) != 0) {
    // Si vienen los dos se pone titulos en negativo y efectivo en
    // positivo
    titulos = titulos.negate();
    efectivo = efectivo.abs();
      } else {
    // Si solamente devuelve uno de los dos iran en negativo
    if (titulos.compareTo(BigDecimal.ZERO) != 0) {
        titulos = titulos.negate();
    }
    if (efectivo.compareTo(BigDecimal.ZERO) != 0) {
        efectivo = efectivo.negate();
    }
      }
  }
  mov.setEfectivo(efectivo);
  mov.setTitulos(titulos);
    }

    /**
     * Resolve params.
     *
     * @param filtros
     *            the filtros
     * @return the tmct0 movimiento manual
     */
    private Tmct0MovimientoManual resolveParams(
      List<Map<String, String>> filtros) {
  Tmct0MovimientoManual entidad = new Tmct0MovimientoManual();
  for (Map<String, String> item : filtros) {
      for (Map.Entry<String, String> entry : item.entrySet()) {
    Tmct0CuentaLiquidacion cLiquidacion = null;
    if (entry.getKey() != null) {
        // Se convierten las fechas de String a Date, date es
        // opcional si no viene informado se coge la fecha actual
        if (entry.getKey() != null
          && (entry.getKey().equals("fliquidacion"))) {
      if (entry.getValue() != null
        && !"".equals(entry.getValue())) {
          java.sql.Date fecha = new java.sql.Date(
            FormatDataUtils.convertStringToDate(
              entry.getValue()).getTime());
          entidad.setFechaLiquidacion(fecha);
      } else {
          entidad.setFechaLiquidacion(new java.sql.Date(
            new Date().getTime()));
      }
        }

        // fecha contratacion
        if (entry.getKey() != null
          && (entry.getKey().equals("fcontratacion"))) {
      if (entry.getValue() != null
        && !"".equals(entry.getValue())) {
          java.sql.Date fecha = new java.sql.Date(
            FormatDataUtils.convertStringToDate(
              entry.getValue()).getTime());
          entidad.setFechaContratacion(fecha);
      }
        }

        // titulos
        if (entry.getKey() != null
          && (entry.getKey().equals("titulos"))) {
      if (entry.getValue() != null && entry.getValue() != "") {
          BigDecimal titulos = BigDecimal.valueOf(Double
            .valueOf(entry.getValue().trim()));
          entidad.setTitulos(titulos);
      } else {
          entidad.setTitulos(BigDecimal.ZERO);
      }
        }

        // isin es obligatorio
        if (entry.getKey() != null
          && (entry.getKey().equals("cdisin"))) {
      if (entry.getValue() != null) {
          String isin = (entry.getValue().toString()
            .indexOf("-") != -1) ? entry
            .getValue()
            .substring(0, entry.getValue().indexOf("-"))
            : entry.getValue().toString().trim();
          entidad.setIsin(isin);
          String descripcion = (entry.getValue().indexOf("-") != -1) ? entry
            .getValue().substring(
              entry.getValue().indexOf("-") + 1)
            : entry.getValue().toString().trim();
          entidad.setDescripcionIsin(descripcion);
      } else {
          entidad.setIsin("");
          entidad.setDescripcionIsin("");
      }
        }

        // camara es obligatorio
        if (entry.getKey() != null
          && (entry.getKey().equals("cdcamara"))) {
      if (entry.getValue() != null && entry.getValue() != "") {
          entidad.setCamara(entry
            .getValue()
            .trim()
            .substring(0, entry.getValue().indexOf("-")).trim());
          entidad.setDescripcionCamara(entry.getValue()
            .substring(entry.getValue().length() - 4)
            .trim());
      } else {
          entidad.setCamara("");
          entidad.setDescripcionCamara("");
      }
        }

        // efectivo
        if (entry.getKey() != null
          && (entry.getKey().equals("efectivo"))) {
      if (entry.getValue() != null
        && !"".equals(entry.getValue().toString())) {
          BigDecimal efectivo = BigDecimal.valueOf(Double
            .valueOf(entry.getValue().trim()));
          entidad.setEfectivo(efectivo);
      } else {
          entidad.setEfectivo(BigDecimal.ZERO);
      }

      // sentido es obligatorio
        } else if (entry.getKey() != null
          && (entry.getKey().equals("sentido"))) {
      // compra o venta
      if (entry.getValue() != null
        && entry.getValue().equals("1")) {
          entidad.setSentido(SENTIDO_ENTRADA);
      } else if (entry.getValue() != null
        && entry.getValue().equals("2")) {
          entidad.setSentido(SENTIDO_SALIDA);
      } else {
          entidad.setSentido("");
      }

        }
        /*
         * TODO CAMBIO RELACION CUENTA LIQUIDACION - CUENTA
         * COMPESACION HECHO
         */
        // cuenta liquidacion titulo
        if (entry.getKey() != null
          && entry.getKey().equals("idCuentaLiq")) {
      if (entry.getValue() != null
        && !entry.getValue().equals("0")) {
          entidad.setCuentaLiquidacion(entry.getValue()
            .trim());
          cLiquidacion = new Tmct0CuentaLiquidacion();
          String cdCodigo = entry.getValue().trim();
          Tmct0CuentasDeCompensacion cc = liquidacionBo
            .findByCdCodigo(cdCodigo);
          if (cc == null) {
        throw new EntityNotFoundException(
          "No hay C. Comp. asociada a la C. Liq. TITULOS: '"
            + cdCodigo + "'");
          }
          cLiquidacion = cc.getTmct0CuentaLiquidacion();
      } else {
          entidad.setCuentaLiquidacion("");
      }
        }

        // descripcion cuenta titulo
        if (entry.getKey() != null
          && (entry.getKey().equals("desCuentaLiq"))) {
      if (entry.getValue() != null
        && entidad.getCuentaLiquidacion() != null) {
          entidad.setDescripcionCuentaLiquidacion(entry
            .getValue());
      } else {
          entidad.setDescripcionCuentaLiquidacion("");
      }
        }

        // cuenta liquidacion efectivo
        /*
         * TODO CAMBIO RELACION CUENTA LIQUIDACION - CUENTA
         * COMPESACION HECHO
         */
        if (entry.getKey() != null
          && entry.getKey().equals("idCuentaLiqEfectivo")) {
      if (entry.getValue() != null
        && !entry.getValue().equals("0")) {
          entidad.setCuentaLiquidacionEfectivo(entry
            .getValue().trim());
          cLiquidacion = new Tmct0CuentaLiquidacion();
          String cdCodigo = entry.getValue().trim();
          Tmct0CuentasDeCompensacion cc = liquidacionBo
            .findByCdCodigo(cdCodigo);
          if (cc == null) {
        throw new EntityNotFoundException(
          "No hay C. Comp. asociada a la C. Liq. EFECTIVO: '"
            + cdCodigo + "'");
          }
          cLiquidacion = cc.getTmct0CuentaLiquidacion();
      } else {
          entidad.setCuentaLiquidacionEfectivo("");
      }
        }

        // descripcion cuenta Efectivo
        if (entry.getKey() != null
          && (entry.getKey().equals("desCuentaLiqEfectivo"))) {
      if (entry.getValue() != null
        && entidad.getCuentaLiquidacionEfectivo() != null) {
          entidad.setDescripcionCuentaLiquidacionEfectivo(entry
            .getValue());
      } else {
          entidad.setDescripcionCuentaLiquidacionEfectivo("");
      }
        }

        // alias
        if (entry.getKey() != null
          && (entry.getKey().equals("alias"))) {
      if (entry.getValue() != null && entry.getValue() != "") {
          entidad.setAlias(entry
            .getValue()
            .trim()
            .substring(0, entry.getValue().indexOf("-")));
          entidad.setDescripcionAlias(entry
            .getValue()
            .substring(
              entry.getValue().indexOf("-") + 1)
            .trim());
      } else {
          entidad.setAlias("");
          entidad.setDescripcionAlias("");
      }
        }

        // procesado
        entidad.setProcesadoCv(false);
    }
      }
  }

  if (entidad.getFechaContratacion() == null) {
      entidad.setFechaContratacion(entidad.getFechaLiquidacion());
  }

  return entidad;

    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
     */
    @Override
    public List<String> getAvailableCommands() {
  return Arrays.asList(new String[] { CMD_INSERT_MOV_MANUAL });
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
     */
    @Override
    public List<String> getFields() {
  return Arrays.asList(new String[] {});
    }

}
