package sibbac.business.conciliacion.util;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CuentaBancariaUtil {

  private static final Logger LOG = LoggerFactory.getLogger(CuentaBancariaUtil.class);
  private transient static CuentaBancariaUtil instance;

  private static final NumberFormat nF2 = new DecimalFormat("00");
  private static final NumberFormat nF4 = new DecimalFormat("0000");
  private static final NumberFormat nF10 = new DecimalFormat("0000000000");
  private static final int[] tablaPesos = { 6, 3, 7, 9, 10, 5, 8, 4, 2, 1
  };
  private static final BigInteger BI97 = new BigInteger("97");

  private CuentaBancariaUtil() {
    //
  }

  public static CuentaBancariaUtil getInstance() {
    if (instance == null) {
      synchronized (CuentaBancariaUtil.class) {
        if (instance == null) {
          instance = new CuentaBancariaUtil();
        }
      }

    }
    return instance;
  }

  public String getIban(String codigoPais, Integer entidad, Integer sucursal, Long cuenta) {
    String ccc = getCCC(entidad, sucursal, cuenta);
    return codigoPais
           + nF2.format(98 - new BigInteger(ccc + damePesoIBAN(codigoPais.charAt(0))
                                            + damePesoIBAN(codigoPais.charAt(1)) + "00").mod(BI97).intValue()) + ccc;
  }

  private String getCCC(Integer entidad, Integer sucursal, Long cuenta) {
    String sEntidad = nF4.format(entidad);
    String sSucursal = nF4.format(sucursal);
    String sCuenta = nF10.format(cuenta);
    return sEntidad + sSucursal + calculaDCParcial(sEntidad + sSucursal) + calculaDCParcial(sCuenta) + sCuenta;
  }

  private String damePesoIBAN(char letra) {
    return String.valueOf(Character.toUpperCase(letra) - 55);
  }

  private String calculaDCParcial(String cadena) {
    int dcParcial = 0;
    int suma = 0;
    int i;
    for (i = 0; i < cadena.length(); i++) {
      suma += (cadena.charAt(cadena.length() - 1 - i) - 48) * tablaPesos[i];
    }
    dcParcial = 11 - suma % 11;
    if (dcParcial == 11)
      dcParcial = 0;
    else if (dcParcial == 10)
      dcParcial = 1;
    return String.valueOf(dcParcial);
  }
}
