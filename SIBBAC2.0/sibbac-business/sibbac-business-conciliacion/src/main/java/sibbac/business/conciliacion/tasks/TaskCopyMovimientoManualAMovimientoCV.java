package sibbac.business.conciliacion.tasks;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.conciliacion.database.bo.MovimientoCuentaVirtualBo;
import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtual;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoManualBo;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0MovimientoManual;

/**
 * 
 * @author ldeblas
 *
 */
@Deprecated
public class TaskCopyMovimientoManualAMovimientoCV extends ConciliacionTaskConcurrencyPrevent {

	@Autowired
	private MovimientoCuentaVirtualBo	movCVirtualBO;
	
	@Autowired
	private Tmct0MovimientoManualBo		movManualBO;

	@Autowired
	private Tmct0CuentaLiquidacionBo ctaLiqBo;

	@Override
	public void executeTask() {
		// Extraer los movimientos manuales que no hayan sido tratados por esta tarea -> procesadoCv = FALSE
		List< Tmct0MovimientoManual > movimientos = new ArrayList< Tmct0MovimientoManual >();
		movimientos = movManualBO.findAllNoTratadosACuentaVirtual();
		if ( movimientos == null || movimientos.size() == 0 ) {
			LOG.debug( "[TaskCopyMovimientoManualAMovimientoCV::execute] No hay movimientos manuales pendientes de copiar a Cuenta Virtual." );
		} else {
			for ( Tmct0MovimientoManual movM : movimientos ) {
				if ( movM != null ) {
					LOG.debug( "INI [TaskCopyMovimientoManualAMovimientoCV::execute] Tratando movimiento manual con id: {}", movM.getId() );

					// Los siguientes campos salen directamente desde Tmct0MovimientoManual
					Character sentido = ( movM.getSentido() != null ) ? movM.getSentido().charAt( 0 ) : null;
					BigDecimal imtitulos = movM.getTitulos();
					// Long idCuentaLiq = movM.getIdcuentaliq();
					Date settlementDate = convertJavaDateToSqlDate( movM.getFechaLiquidacion() );
					BigDecimal imefectivo = movM.getEfectivo();
					String isin = movM.getIsin();
					String descripcionisin = movM.getDescripcionIsin();
					// FIXME: SENTIDOCV A NULL??????????????????????????????
					Character sentidoCV = ( movM.getSentidoCV() != null ) ? movM.getSentidoCV().charAt( 0 ) : null;
					String cdCodigoCtaLiq = movM.getCuentaLiquidacion();
					String cdcamara = movM.getCamara();

					// FIXME: meter idCamara?????

					Tmct0CuentasDeCompensacion ctaLiq = ctaLiqBo.findByCdCodigo( cdCodigoCtaLiq );
					String cdCodigoCtaComp = "";
					Long idCuentaComp = new Long( 0 );
					if ( ctaLiq != null ) {
						/*
						 * TODO CAMBIO RELACION CUENTA LIQUIDACION - CUENTA COMPESACION
						 * HECHO
						 */
						// Tmct0CuentasDeCompensacion ctaComp = ctaLiq.getTmct0CuentasDeCompensacion();
						if ( ctaLiq != null ) {
							cdCodigoCtaComp = ctaLiq.getCdCuentaCompensacion();
							idCuentaComp = ctaLiq.getIdCuentaCompensacion();
						}
					} else {

						LOG.warn( "[TaskCopyMovimientoManualAMovimientoCV::execute] No fue posible recuperar los datos de la cuenta de compensacion." );
						continue;
					}

					// FIXME: Los siguientes campos NO sabemos de dónde cogerlos a partir de movManual
					String alias = "";
					String descrali = "";
					Date tradeDate = new Date( 1, 1, 1 );

					// Comprobamos los campos para la búsqueda de MovimientoCV. en caso de que alguno sea NULL o vacio, avisamos por LOG y
					// pasamos al siguiente movECC
					// if(! (isin != null && !isin.equals("") && feLiq != null && sentido != null && cdCodigoCtaLiq != null &&
					// !cdCodigoCtaLiq.equals("")
					// && sentidoCV != null && cdcamara != null && !cdcamara.equals("")))
					// {
					// if(isin == null || isin.equals(""))
					// LOG.error("[TaskCopyMovimientoManualAMovimientoCV::execute] .El isin viene a NULL o vacio.");
					// if(feLiq == null)
					// LOG.error("[TaskCopyMovimientoManualAMovimientoCV::execute] .La fecha de liquidacion viene a NULL.");
					// if(sentido == null)
					// LOG.error("[TaskCopyMovimientoManualAMovimientoCV::execute] .El sentido viene a NULL.");
					// if(cdCodigoCtaLiq == null || cdCodigoCtaLiq.equals(""))
					// LOG.error("[TaskCopyMovimientoManualAMovimientoCV::execute] .El codigo de cuenta liquidacion viene a NULL o vacio.");
					// if(sentidoCV == null || sentidoCV.equals(""))
					// LOG.error("[TaskCopyMovimientoManualAMovimientoCV::execute] .El sentidoCV viene a NULL o vacio.");
					// if(cdcamara == null || cdcamara.equals(""))
					// LOG.error("[TaskCopyMovimientoManualAMovimientoCV::execute] .La camara viene a NULL o vacio.");
					// continue;
					// }

					// Buscamos una entrada en movimientoCuentaVirtual para la tupla (isin, fechaLiquidacion, sentido, ctaLiq, sentidoCV,
					// camara)
					MovimientoCuentaVirtual movCV = movCVirtualBO
							.findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaLiqAndSentidoCVAndCamara( isin, tradeDate,
									settlementDate, sentido, cdCodigoCtaLiq, sentidoCV, cdcamara );

					if ( movCV != null ) {
						LOG.debug(
								"[TaskCopyMovimientoManualAMovimientoCV::execute] Ya hay una entrada en TMCT0_MOVIMIENTO_CUENTA_VIRTUAL para el isin {} , settlementDate {} , sentido {} , ctaLiq {} , sentidoCV {} , camara {}",
								isin, tradeDate, settlementDate, sentido, cdCodigoCtaLiq, sentidoCV, cdcamara );
						// Ya hay una entrada en TMCT0_MOVIMIENTO_CUENTA_VIRTUAL para
						// ese (isin, fechaLiquidacion, sentido, ctaLiq, sentidoCV, camara)
						// Se actualiza imefectivo y imtitulos en la fila de TMCT0_MOVIMIENTO_CUENTA_VIRTUAL
						movCV.setTitulos( imtitulos );
						movCV.setEfectivo( imefectivo );
						movCVirtualBO.saveAndFlush( movCV );
					} else {
						LOG.debug(
								"[TaskCopyMovimientoManualAMovimientoCV::execute] Se creara una nueva entrada en TMCT0_MOVIMIENTO_CUENTA_VIRTUAL para el isin {} , settlementDate {} , sentido {} , ctaLiq {} , sentidoCV {} , camara {}",
								isin, tradeDate, settlementDate, sentido, cdCodigoCtaLiq, sentidoCV, cdcamara );
						// No existia en TMCT0_MOVIMIENTO_CUENTA_VIRTUAL para ese (isin, fechaLiq, sentido, ctaLiq, sentidoCV, camara)
						// Se creará nuevo registro en TMCT0_MOVIMIENTO_CUENTA_VIRTUAL
						MovimientoCuentaVirtual newmovCV = new MovimientoCuentaVirtual();
						newmovCV.setAlias( alias );
						newmovCV.setDescrali( descrali );
						newmovCV.setCamara( cdcamara );
						// FIXME ????????????????????????????????????????????????????????????????????????
						newmovCV.setCdcodigoccomp( cdCodigoCtaComp.substring( 0, 15 ) );
						newmovCV.setCdcodigocliq( cdCodigoCtaLiq );
						newmovCV.setEfectivo( imefectivo );
						newmovCV.setIdcuentacomp( idCuentaComp );
						// newmovCV.setIdcuentaliq(idCuentaLiq);
						newmovCV.setIsin( isin );
						newmovCV.setDescripcionisin( descripcionisin );
						newmovCV.setSentido( sentido );
						newmovCV.setSettlementdate( ( java.sql.Date ) settlementDate );
						newmovCV.setTitulos( imtitulos );
						newmovCV.setTradedate( ( java.sql.Date ) tradeDate );
						newmovCV.setSentidocuentavirtual( sentidoCV );

						// FIXME: Los siguientes campos desaparecerán
						newmovCV.setPrecio( new BigDecimal( 0 ) );
						newmovCV.setNcliente( "" );

						// Insertar la nueva entrada en TMCT0_MOVIMIENTO_CUENTA_VIRTUAL
						movCVirtualBO.saveAndFlush( newmovCV );
					}
					// Seteamos el valor del movimientoManual.procesadoCV para indicar que ya lo hemos tratado con esta Task
					// "TaskCopyMovimientoManualAMovimientoCV"
					movM.setProcesadoCv( true );
					movManualBO.saveAndFlush( movM );
					LOG.debug( "[TaskCopyMovimientoManualAMovimientoCV::execute] Se termina de tratar el movimiento manual con id {}.",
							movM.getId() );
				} else {
					LOG.debug( "[TaskCopyMovimientoManualAMovimientoCV::execute] El movimiento manual viene a NULL.",
							this.jobDataMap.get( "name" ), new java.util.Date() );
				}
			}// fin for movimientos
		}
		LOG.debug( "FIN [TaskCopyMovimientoManualAMovimientoCV::execute] " );
	}

	public java.sql.Date convertJavaDateToSqlDate( java.util.Date date ) {
		return new java.sql.Date( date.getTime() );
	}

	@Override
	public TIPO_APUNTE determinarTipoApunte() {
	    return null;
	}

}
