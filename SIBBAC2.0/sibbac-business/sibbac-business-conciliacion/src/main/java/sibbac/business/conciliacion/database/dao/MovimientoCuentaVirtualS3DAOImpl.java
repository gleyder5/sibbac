package sibbac.business.conciliacion.database.dao;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualS3Data;


@Repository
public class MovimientoCuentaVirtualS3DAOImpl {

	@PersistenceContext
	private EntityManager	em;

	public List< MovimientoCuentaVirtualS3Data > findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion( long idCuentaLiq,
			Map< String, Serializable > filtros ) {
		List< MovimientoCuentaVirtualS3Data > resultList = new ArrayList< MovimientoCuentaVirtualS3Data >();
		String select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualS3Data( m.auditDate, m.idcuentaliq, m.settlementdate, m.isin, m.descrIsin, sum(m.titulos) as titulos, sum(m.efectivo) as efectivos) FROM MovimientoCuentaVirtualS3 m WHERE m.idcuentaliq=:idCuentaLiq ";
		if ( idCuentaLiq == 0 ) {
			select = "SELECT new sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualS3Data( m.auditDate, m.idcuentaliq, m.settlementdate, m.isin, m.descrIsin, sum(m.titulos) as titulos, sum(m.efectivo) as efectivos) FROM MovimientoCuentaVirtualS3 m WHERE m.idcuentaliq!=:idCuentaLiq ";
		}
		String groupBy = " group by m.isin, m.settlementdate, m.auditDate, m.idcuentaliq, m.descrIsin ";
		String orderBy = " ";
		// String fcontratacionBtw = "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
		String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
		StringBuilder sb = new StringBuilder( select );
		// se construye la query segun los filtros
		for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
			if ( entry.getValue() != null
					&& !"".equals( entry.getKey() )
					&& ( !entry.getKey().equals( "fliquidacionDe" ) && !entry.getKey().equals( "fliquidacionA" ) && !entry.getKey().trim()
							.equals( "idCuentaLiq" ) ) ) {
				sb.append( " AND m." ).append( entry.getKey() ).append( "=:" ).append( entry.getKey() );
			} else if ( entry.getKey().equals( "fliquidacionDe" ) && entry.getValue() != null ) {
				// si vienen las dos fechas se hace un between
				if ( checkIfKeyExist( "fliquidacionA", filtros ) ) {
					sb.append( fliquidacionBtw );
				} else {
					sb.append( "AND a.settlementdate >=:fliquidacionDe " );
				}
			}
		}
		sb.append( groupBy ).append( orderBy );
		Query query = em.createQuery( sb.toString() );
		for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
			query.setParameter( entry.getKey(), entry.getValue() );
		}
		query.setParameter( "idCuentaLiq", idCuentaLiq );
		resultList.addAll( query.getResultList() );
		return resultList;
	}

	private boolean checkIfKeyExist( String key, Map< String, Serializable > params ) {
		boolean exist = false;
		if ( params.get( key ) != null && !params.get( key ).equals( "" ) ) {
			exist = true;
		}
		return exist;
	}
}
