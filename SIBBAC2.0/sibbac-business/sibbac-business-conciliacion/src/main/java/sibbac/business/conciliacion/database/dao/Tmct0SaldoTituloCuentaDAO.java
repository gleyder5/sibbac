package sibbac.business.conciliacion.database.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.Tmct0SaldoTituloCuenta;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;


@Repository
public interface Tmct0SaldoTituloCuentaDAO extends JpaRepository< Tmct0SaldoTituloCuenta, Long > {

	/**
	 * 
	 * @param cuentaCompensacion
	 * @param tradedate
	 * @param isin
	 * @return
	 */
	List< Tmct0SaldoTituloCuenta > findByCuentaCompensacionAndTradedateAndIsin( Tmct0CuentasDeCompensacion cuentaCompensacion,
			Date tradedate, String isin );

	/**
	 * 
	 * @param cuentaCompensacion
	 * @param tradedate
	 * @param isin
	 * @param idMovimientoecc
	 * @return
	 */
	Tmct0SaldoTituloCuenta findByCuentaCompensacionAndTradedateAndIsinAndIdMovimientoecc( Tmct0CuentasDeCompensacion cuentaCompensacion,
			Date tradedate, String isin, Tmct0movimientoecc idMovimientoecc );

	/**
	 * 
	 * @return
	 */
	@Query( "SELECT o, sum(o.saldo) as total FROM Tmct0SaldoTituloCuenta o group by o.isin, o.sentido, o.tradedate, o.cuentaCompensacion" )
	List< Object[] > findAllGroupByIsinAndTradedateAndSentidoAndCuentaCompensacion();

	/**
	 * 
	 * @return List<Tmct0SaldoTituloCuenta>
	 */
	@Query( "SELECT o, sum(o.saldo) as total FROM Tmct0SaldoTituloCuenta o group by o.isin, o.sentido, o.tradedate, o.settlementdate, o.cuentaCompensacion" )
			List< Object[] > findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion();

	/**
	 * Recupera las entradas de titulos agrupados por isin, cuenta de compensacion y sentido
	 * filtrado por conciliacion
	 * 
	 * @param conciliado
	 * @return List<Object[]>
	 */
	@Query( "SELECT o, sum(o.saldo) as total FROM Tmct0SaldoTituloCuenta o where o.conciliado =:conciliado group by o.isin, o.sentido, o.cuentaCompensacion" )
			List< Object[] > findAllGroupByIsinAndAndSentidoAndCuentaCompensacionAndConciliado(
					@Param( value = "conciliado" ) boolean conciliado );
}
