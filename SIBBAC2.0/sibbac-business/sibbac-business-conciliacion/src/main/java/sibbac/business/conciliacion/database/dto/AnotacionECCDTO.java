package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class AnotacionECCDTO implements IAnotacionECCDTO {

	private Long		idanotacionecc;
	private char		cdsentido;
	private String		fcontratacion;
	private String		fliquidacion;
	private BigDecimal	nutitulos;
	private BigDecimal	imtitulosdisponibles;
	private BigDecimal	imefectivo;
	private BigDecimal	imprecio;
	private char		cdoperacion;
	private String		cdoperacionecc;
	private String		cdoperacionmkt;
	private String		nuexemkt;
	private String		nuordenmkt;
	private String		cdmiembromkt;
	private String		cdisin;
	private String		isinDescripcion;
	private String		cdCuenta;
	private String		cdcamara;
	private String		nuoperacioninic;
	private BigDecimal	diferencia;
	private String		codigoCuentaCompensacion;
	private long		idCuentaDeCompensacion;
	private boolean		cazado;

	public AnotacionECCDTO() {
		super();
	}

	public Long getIdanotacionecc() {
		return idanotacionecc;
	}

	public void setIdanotacionecc( Long idanotacionecc ) {
		this.idanotacionecc = idanotacionecc;
	}

	public char getCdsentido() {
		return cdsentido;
	}

	public void setCdsentido( char cdsentido ) {
		this.cdsentido = cdsentido;
	}

	public String getFcontratacion() {
		return fcontratacion;
	}

	public void setFcontratacion( String fcontratacion ) {
		this.fcontratacion = fcontratacion;
	}

	public String getFliquidacion() {
		return fliquidacion;
	}

	public void setFliquidacion( String fliquidacion ) {
		this.fliquidacion = fliquidacion;
	}

	public BigDecimal getImefectivo() {
		return imefectivo;
	}

	public void setImefectivo( BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	public char getCdoperacion() {
		return cdoperacion;
	}

	public void setCdoperacion( char cdoperacion ) {
		this.cdoperacion = cdoperacion;
	}

	public String getCdoperacionmkt() {
		return cdoperacionmkt;
	}

	public void setCdoperacionmkt( String cdoperacionmkt ) {
		this.cdoperacionmkt = cdoperacionmkt;
	}

	public String getCdmiembromkt() {
		return cdmiembromkt;
	}

	public void setCdmiembromkt( String cdmiembromkt ) {
		this.cdmiembromkt = cdmiembromkt;
	}

	public String getCdisin() {
		return cdisin;
	}

	public void setCdisin( String cdisin ) {
		this.cdisin = cdisin;
	}

	public BigDecimal getNutitulos() {
		return nutitulos;
	}

	public void setNutitulos( BigDecimal nutitulos ) {
		this.nutitulos = nutitulos;
	}

	public String getCdcamara() {
		return cdcamara;
	}

	public void setCdcamara( String cdcamara ) {
		this.cdcamara = cdcamara;
	}

	public BigDecimal getImprecio() {
		return imprecio;
	}

	public void setImprecio( BigDecimal imprecio ) {
		this.imprecio = imprecio;
	}

	public BigDecimal getDiferencia() {
		return diferencia;
	}

	public void setDiferencia( BigDecimal diferencia ) {
		this.diferencia = diferencia;
	}

	public String getCdCuenta() {
		return cdCuenta;
	}

	public void setCdCuenta( String cdCuenta ) {
		this.cdCuenta = cdCuenta;
	}

	public String getCodigoCuentaCompensacion() {
		return codigoCuentaCompensacion;
	}

	public void setCodigoCuentaCompensacion( String codigoCuentaCompensacion ) {
		this.codigoCuentaCompensacion = codigoCuentaCompensacion;
	}

	public long getIdCuentaDeCompensacion() {
		return idCuentaDeCompensacion;
	}

	public void setIdCuentaDeCompensacion( long idCuentaDeCompensacion ) {
		this.idCuentaDeCompensacion = idCuentaDeCompensacion;
	}

	public boolean isCazado() {
		return cazado;
	}

	public void setCazado( boolean cazado ) {
		this.cazado = cazado;
	}

	public String getIsinDescripcion() {
		return isinDescripcion;
	}

	public void setIsinDescripcion( String isinDescripcion ) {
		this.isinDescripcion = isinDescripcion;
	}

	public String getCdoperacionecc() {
		return cdoperacionecc;
	}

	public void setCdoperacionecc( String cdoperacionecc ) {
		this.cdoperacionecc = cdoperacionecc;
	}

	public String getNuoperacioninic() {
		return nuoperacioninic;
	}

	public void setNuoperacioninic( String nuoperacioninic ) {
		this.nuoperacioninic = nuoperacioninic;
	}

	public String getNuexemkt() {
		return nuexemkt;
	}

	public void setNuexemkt( String nuexemkt ) {
		this.nuexemkt = nuexemkt;
	}

	
	public BigDecimal getImtitulosdisponibles() {
		return imtitulosdisponibles;
	}

	
	public void setImtitulosdisponibles( BigDecimal imtitulosdisponibles ) {
		this.imtitulosdisponibles = imtitulosdisponibles;
	}

	
	public String getNuordenmkt() {
		return nuordenmkt;
	}

	
	public void setNuordenmkt( String nuordenmkt ) {
		this.nuordenmkt = nuordenmkt;
	}

}
