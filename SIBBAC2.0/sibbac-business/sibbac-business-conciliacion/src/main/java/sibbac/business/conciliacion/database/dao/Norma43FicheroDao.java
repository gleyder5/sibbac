package sibbac.business.conciliacion.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.Norma43Fichero;

@Repository
public interface Norma43FicheroDao extends JpaRepository<Norma43Fichero, Long> {

  @Query("SELECT MAX(f.auditDate) FROM Norma43Fichero f")
  public Date findDateOfLastFileEntry();

  /**
   * Busca los registros filtrando por el campo <code>procesado</code>
   * 
   * @param procesado
   * @return
   * @author XI316153
   */
  public List<Norma43Fichero> findAllByProcesado(Boolean procesado);

  @Query("SELECT fN43 FROM Norma43Fichero fN43 WHERE fN43.procesado = :procesado ORDER BY fN43.fechaInicial ASC")
  public List<Norma43Fichero> findByProcesadoOrderByFechaInicial(@Param("procesado") Boolean procesado);

  @Query("SELECT fN43 FROM Norma43Fichero fN43, CuentaBancaria cb, CuentaAuxiliar ca" + " WHERE fN43.procesado = :procesado"
         + "       AND (fN43.entidad, fN43.oficina, fN43.cuenta) = (substr(cb.iban, 5, 4), substr(cb.iban, 9, 4), substr(cb.iban, 15, 10))"
         + "       AND cb.id = ca.cuenta AND ca.tipologia = :tipologia AND ca.tipoImporte IN (:tiposImporteList)" + " ORDER BY fN43.fechaInicial ASC")
  public List<Norma43Fichero> findByProcesadoAndTipologiaAndTipoImporteOrderByFechaInicial(@Param("procesado") Boolean procesado,
                                                                                           @Param("tipologia") Integer tipologia,
                                                                                           @Param("tiposImporteList") List<Integer> tiposImporteList);

  @Query("SELECT fN43.id, ca.auxiliar.id, cb.tolerance FROM Norma43Fichero fN43, CuentaBancaria cb, CuentaAuxiliar ca"
         + " WHERE fN43.procesado = :procesado"
         + "       AND (fN43.entidad, fN43.oficina, fN43.cuenta) = (substr(cb.iban, 5, 4), substr(cb.iban, 9, 4), substr(cb.iban, 15, 10))"
         + "       AND cb.id = ca.cuenta AND ca.tipologia = :tipologia AND ca.tipoImporte = :tipoImporte")
  public List<Object[]> findByProcesadoAndTipologiaAndTipoImporte(@Param("procesado") Boolean procesado,
                                                                  @Param("tipologia") Integer tipologia,
                                                                  @Param("tipoImporte") Integer tipoImporte);

  @Modifying
  @Query(value = "UPDATE tmct0_norma43_fichero SET procesado=:procesado WHERE id=:id", nativeQuery = true)
  public int updateProcesadoFlag(@Param("id") Long id, @Param("procesado") int procesado);

} // Norma43FicheroDao
