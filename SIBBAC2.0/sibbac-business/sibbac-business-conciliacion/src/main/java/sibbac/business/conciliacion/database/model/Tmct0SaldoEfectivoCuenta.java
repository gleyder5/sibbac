package sibbac.business.conciliacion.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_SALDO_EFECTIVO_CUENTA )
@XmlRootElement
public class Tmct0SaldoEfectivoCuenta extends ATable< Tmct0SaldoEfectivoCuenta > implements Serializable {

	/**
	 * 
	 */
	private static final long			serialVersionUID	= 1L;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_CUENTA_COMPENSACION", nullable = false )
	private Tmct0CuentasDeCompensacion	cuentaCompensacion;

	@Column( name = "TRADEDATE", nullable = false )
	@XmlAttribute
	private Date						tradedate;

	@Column( name = "SETTLEMENTDATE", nullable = false )
	@XmlAttribute
	private Date						settlementdate;

	@Column( name = "MIEMBRO", length = 4, nullable = false )
	@XmlAttribute
	private String						miembro;

	@Column( name = "ISIN", length = 12, nullable = true )
	@XmlAttribute
	private String						isin;

	@Column( name = "SENTIDO", length = 1, nullable = false )
	@XmlAttribute
	private String						sentido;

	@Column( name = "ENTIDAD", length = 11, nullable = false )
	@XmlAttribute
	private String						entidad;

	@Column( name = "SALDO", scale = 6, precision = 18 )
	@XmlAttribute
	private BigDecimal					saldo;

	public Tmct0CuentasDeCompensacion getCuentaCompensacion() {
		return cuentaCompensacion;
	}

	public void setCuentaCompensacion( Tmct0CuentasDeCompensacion cuentaCompensacion ) {
		this.cuentaCompensacion = cuentaCompensacion;
	}

	public Date getTradedate() {
		return tradedate;
	}

	public void setTradedate( Date tradedate ) {
		this.tradedate = tradedate;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public String getMiembro() {
		return miembro;
	}

	public void setMiembro( String miembro ) {
		this.miembro = miembro;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido( String sentido ) {
		this.sentido = sentido;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad( String entidad ) {
		this.entidad = entidad;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo( BigDecimal saldo ) {
		this.saldo = saldo;
	}

}
