package sibbac.business.conciliacion.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.Tmct0CuentaBancaria;

@Repository
public interface Tmct0CuentaBancariaDao extends JpaRepository<Tmct0CuentaBancaria, Long> {
  public Tmct0CuentaBancaria findByIban(String iban);
}
