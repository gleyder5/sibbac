package sibbac.business.conciliacion.database.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionEfectivo;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;


@Repository
public interface Tmct0DiferenciaConciliacionEfectivoDAO extends JpaRepository< Tmct0DiferenciaConciliacionEfectivo, Long > {

	/**
	 * 
	 * @param tradeDate
	 * @return
	 */
	List< Tmct0DiferenciaConciliacionEfectivo > findByTradeDateGreaterThanEqualOrderByTradeDate( Date tradeDate );

	/**
	 * 
	 * @param tradeDate
	 * @return
	 */
	Tmct0DiferenciaConciliacionEfectivo findByTradeDateAndIsinAndTmct0CuentasDeCompensacionAndSentido( Date tradeDate, String isin,
			Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion, String sentido );
}
