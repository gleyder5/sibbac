package sibbac.business.conciliacion.database.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.MovimientoCuentaVirtualS3Dao;
import sibbac.business.conciliacion.database.data.FicheroCuentaECCData;
import sibbac.business.conciliacion.rest.ISIBBACServiceConciliacionBalance;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0SaldoInicialCuentaBo;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.data.SaldoInicialCuentaData;
import sibbac.business.wrappers.database.dto.ConciliacionERDTO;
import sibbac.business.wrappers.database.dto.ConciliacionERData;
import sibbac.common.utils.FormatDataUtils;

@Service
public class ConciliacionS3Bo {

  protected static final Logger LOG = LoggerFactory.getLogger(ConciliacionS3Bo.class);

  @Autowired
  private MovimientoCuentaVirtualS3Dao s3TitulosDao;

  @Autowired
  private FicheroCuentaECCBo ficheroCuentaECCBo;
  
  @Autowired
  private Tmct0SaldoInicialCuentaBo saldoBo;
  
  @Autowired
  private Tmct0MovimientoCuentaAliasBo movimientoCuentaAliasBo;

  // calcular el saldo inicial del dia.
  public List<ConciliacionERDTO> conciliacionDeTitulos(final Date fechaDesde, final String codigoCuentaLiquidacion,
      String isin, final String tipoDiferencia) {
    final List<ConciliacionERData> svInfo;
    final List<ConciliacionERData> s3Info;
    final List<FicheroCuentaECCData> ECCDataList;
    
    LOG.debug("[conciliacionDeTitulos] [fecha: {}] [isin: {}] [Cta: {}]", fechaDesde, isin, codigoCuentaLiquidacion);
    svInfo = getSvInfo(fechaDesde, codigoCuentaLiquidacion.trim(), isin);
    if (isin != null && !"".equals(isin.trim())) {
      s3Info = s3TitulosDao.getInfoConciliacionS3(fechaDesde, codigoCuentaLiquidacion, isin);
      ECCDataList = ficheroCuentaECCBo.findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacionAndIsin(
          fechaDesde, fechaDesde, codigoCuentaLiquidacion, isin);
    }
    else {
      s3Info = s3TitulosDao.getInfoConciliacionS3(fechaDesde, codigoCuentaLiquidacion);
      ECCDataList = ficheroCuentaECCBo.findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacion(fechaDesde,
          fechaDesde, codigoCuentaLiquidacion);
    }
    return conciliaTitulos(svInfo, s3Info, ECCDataList, tipoDiferencia);
  }// movimientos acumulados

  public List<ConciliacionERDTO> conciliacionDeEfectivo(final Date fechaDesde, final String codigoCuentaLiquidacion, 
      final String isin, final String tipoDiferencia) {
    final List<ConciliacionERData> svInfo;
    final List<ConciliacionERData> s3Info;
    final List<FicheroCuentaECCData> ECCDataList;
    
    LOG.debug("[conciliacionDeEfectivo] [fecha: {}] [isin: {}] [Cta: {}]", fechaDesde, isin, codigoCuentaLiquidacion);
    svInfo = getSvInfo(fechaDesde, codigoCuentaLiquidacion.trim(), isin);
    if (isin != null && !"".equals(isin.trim())) {
      s3Info = s3TitulosDao.getInfoConciliacionS3(fechaDesde, codigoCuentaLiquidacion, isin);
      ECCDataList = ficheroCuentaECCBo.findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacionAndIsin(
          fechaDesde, fechaDesde, codigoCuentaLiquidacion, isin);
    }
    else {
      s3Info = s3TitulosDao.getInfoConciliacionS3(fechaDesde, codigoCuentaLiquidacion);
      ECCDataList = ficheroCuentaECCBo.findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacion(fechaDesde,
          fechaDesde, codigoCuentaLiquidacion);
    }
    return this.conciliaEfectivo(svInfo, s3Info, ECCDataList, tipoDiferencia);
  }
  
  private List<ConciliacionERData> getSvInfo(final Date fechaDesde, final String cdcodigocuentaliq,
    String isin) {
    final Map<String, Serializable> filtros;
    final List<SaldoInicialCuentaData> listaSaldos;
    final List<MovimientoCuentaAliasData> listaMov;
    final List<ConciliacionERData> svInfo;
    final DateFormat formatter;
    Date fecha = null, fechaAumentada;

    svInfo = new ArrayList<>();
    filtros = new HashMap<String, Serializable>();
    if (isin != null && !"".equals(isin.trim())) {
      filtros.put(ISIBBACServiceConciliacionBalance.FIELD_BALANCE_ISIN_IN, isin);
    }
    filtros.put(ISIBBACServiceConciliacionBalance.FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT, fecha);
    filtros.put(ISIBBACServiceConciliacionBalance.FIELD_BALANCE_COD_CUENTA_LIQUIDACION, cdcodigocuentaliq);
    formatter = new SimpleDateFormat("dd/MM/yyyy");
    // solo se necesita el dia el mes y el año
    try {
      fecha = formatter.parse(formatter.format(fechaDesde));
    }
    catch (ParseException e) {
      LOG.error(e.getMessage(), e);
    }
    // Dependiendo de los parametros que se reciban de los filtros se
    // obtienen los alias e isines a buscar
    fechaAumentada = DateUtils.addDays(fecha, 1);
    // Se añade el filtro fechaLiquidacion con el aumento de un dia, para
    // extraer el saldo inicial
    filtros.put(ISIBBACServiceConciliacionBalance.FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT, fechaAumentada);
    listaSaldos = saldoBo.findAllGroupByIsinAndCodCuentaliqAndFLiquidacionOrderByIsinAndFecha(filtros);
    // Se vuelve a introducir la fecha sin aumentar, para extraer los
    // movimientos que no hayan sido procesados.
    filtros.put(ISIBBACServiceConciliacionBalance.FIELD_BALANCE_FECHA_LIQUIDACION_DE_INT, fecha);
    listaMov = movimientoCuentaAliasBo.findAllMovGroupBySentido(filtros, "");
    if (!CollectionUtils.isEmpty(listaSaldos) && CollectionUtils.isEmpty(listaMov)) {
      for (SaldoInicialCuentaData saldo : listaSaldos) {
        ConciliacionERData dto = new ConciliacionERData();
        if (saldo.getTitulos().longValue() != 0) {
          addSaldo(svInfo, saldo, dto, fecha);
        }
      }
    }
    else if (CollectionUtils.isEmpty(listaSaldos) && !CollectionUtils.isEmpty(listaMov)) {
      for (MovimientoCuentaAliasData data : listaMov) {
        ConciliacionERData dto = new ConciliacionERData();
        if (data.getTitulos().longValue() != 0) {
          addMovimiento(svInfo, data, dto, fecha);
        }
      }
    }
    else if (!CollectionUtils.isEmpty(listaSaldos) && !CollectionUtils.isEmpty(listaMov)) {
      conjugarSaldoInicialYMovimientos(listaSaldos, listaMov, svInfo, fecha);
    }
    return svInfo;
  }

  /**
   * Añade un saldo inicial a la lista resultante de balance
   * 
   * @param dtoList
   * @param saldo
   * @param dto
   * @param fecha
   */
  private void addSaldo(List<ConciliacionERData> dtoList, SaldoInicialCuentaData saldo, ConciliacionERData dto,
      Date fecha) {
    dto.setCodigoCuentaLiquidacion(saldo.getCodcuentaliq());
    dto.setCorretaje(BigDecimal.ZERO);
    dto.setDescripcion(saldo.getDescisin());
    dto.setEfectivo(saldo.getEfectivo());
    dto.setIsin(saldo.getIsin());
    dto.setSettlementDate(fecha);
    dto.setTitulos(saldo.getTitulos());
    dtoList.add(dto);
  }

  /**
   * Añade un movimiento a la lista de balance resultante.
   * 
   * @param dtoList
   * @param data
   * @param dto
   * @param fecha
   */
  private void addMovimiento(List<ConciliacionERData> dtoList, MovimientoCuentaAliasData data, ConciliacionERData dto,
      Date fecha) {
    dto.setCodigoCuentaLiquidacion(data.getCdcodigocuentaliq());
    dto.setCorretaje(BigDecimal.ZERO);
    dto.setDescripcion(data.getDescrIsin());
    dto.setIsin(data.getIsin());
    dto.setTitulos(data.getTitulos());
    dto.setEfectivo(data.getEfectivo());
    dtoList.add(dto);
  }

  /**
   * agrupa los saldos iniciales con los movimientos que todavía no han sido
   * integrados con el saldo inicial.
   * 
   * @param listaSaldos
   * @param listaMov
   * @param dtoList
   * @param fecha
   */
  private void conjugarSaldoInicialYMovimientos(List<SaldoInicialCuentaData> listaSaldos,
      List<MovimientoCuentaAliasData> listaMov, List<ConciliacionERData> dtoList, Date fecha) {
    Iterator<MovimientoCuentaAliasData> itData = listaMov.iterator();
    List<MovimientoCuentaAliasData> listMovCasados = new LinkedList<MovimientoCuentaAliasData>();
    for (SaldoInicialCuentaData saldo : listaSaldos) {
      boolean encontrado = false;
      while (itData.hasNext() && !encontrado) {
        MovimientoCuentaAliasData data = itData.next();
        if (saldo.getCodcuentaliq().equals(data.getCdcodigocuentaliq()) && saldo.getIsin().equals(data.getIsin())) {
          encontrado = true;
          ConciliacionERData dto = new ConciliacionERData();
          dto.setIsin(saldo.getIsin());
          dto.setCodigoCuentaLiquidacion(saldo.getCodcuentaliq());
          dto.setTitulos(saldo.getTitulos().add(data.getTitulos()));
          dto.setEfectivo(saldo.getEfectivo().add(data.getEfectivo()));
          dto.setDescripcion(data.getDescrIsin());
          dto.setSettlementDate(fecha);
          if (dto.getTitulos().longValue() != 0) {
            dtoList.add(dto);
          }
        }
        if (encontrado) {
          listMovCasados.add(data);
          itData.remove();
        }
      }
      // Si el saldo no se conjuga con ningún movimiento, se añade a la
      // lista
      if (!encontrado && saldo.getTitulos().longValue() != 0) {
        addSaldo(dtoList, saldo, new ConciliacionERData(), fecha);
      }
      encontrado = false;
    }
    // Si queda algún movimiento sin conjugar, hay que meterlo también en
    // la lista resultante
    for (MovimientoCuentaAliasData data : listaMov) {
      if (!listMovCasados.contains(data)) {
        ConciliacionERData dto = new ConciliacionERData();
        if (data.getTitulos().longValue() != 0) {
          addMovimiento(dtoList, data, dto, fecha);
        }
      }
    }
  }

  private List<ConciliacionERDTO> conciliaTitulos(List<ConciliacionERData> svInfo,
      List<ConciliacionERData> custodioInfo, List<FicheroCuentaECCData> camaraInfo, String tipoDiferencia) {
    String PREFIX = "[ConciliacionS3Bo::concilia] ";
    LOG.debug(PREFIX + "[tipoDiferencia: {} ]", tipoDiferencia);

    List<ConciliacionERDTO> result = new LinkedList<ConciliacionERDTO>();

    Iterator<ConciliacionERData> iteratorSVInfo = svInfo.iterator();

    mergeInfoCustodioTitulos(custodioInfo, result, iteratorSVInfo, tipoDiferencia);
    result = mergeECCInfoTitulos(result, camaraInfo, tipoDiferencia);
    mergeInfoECCTitulos(svInfo, result, camaraInfo, tipoDiferencia);

    if ("desCuadrados".equals(tipoDiferencia) || "todos".equals(tipoDiferencia)) {
      iteratorSVInfo = svInfo.iterator();
      while (iteratorSVInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorSVInfo.next();
        if (!esNuloTitulos(objConciliacionERData)) {
          addSVInfo(result, objConciliacionERData);
          iteratorSVInfo.remove();
        }
      } // while(iteratorSVInfo.hasNext()) {
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      while (iteratorCustodioInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorCustodioInfo.next();
        if (!esNuloTitulos(objConciliacionERData)) {
          addCustodioInfo(result, objConciliacionERData);
          iteratorCustodioInfo.remove();
        }
      }
      // añade info camara fichero ECC
      Iterator<FicheroCuentaECCData> iteratorCamaraInfo = camaraInfo.iterator();
      while (iteratorCamaraInfo.hasNext()) {
        FicheroCuentaECCData objFicheroCuentaECCData = iteratorCamaraInfo.next();
        if (!esNuloTitulos(objFicheroCuentaECCData)) {
          addCamaraInfo(result, objFicheroCuentaECCData);
          iteratorCamaraInfo.remove();
        }
      }
    }

    if ("cuadrados".equals(tipoDiferencia) || "todos".equals(tipoDiferencia)) {
      iteratorSVInfo = svInfo.iterator();
      while (iteratorSVInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorSVInfo.next();
        if (esNuloTitulos(objConciliacionERData)) {
          addSVInfo(result, objConciliacionERData);
        }
      } // while(iteratorSVInfo.hasNext()) {
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      while (iteratorCustodioInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorCustodioInfo.next();
        if (esNuloTitulos(objConciliacionERData)) {
          addCustodioInfo(result, objConciliacionERData);
        }
      }
      // añade info camara fichero ECC
      Iterator<FicheroCuentaECCData> iteratorCamaraInfo = camaraInfo.iterator();
      while (iteratorCamaraInfo.hasNext()) {
        FicheroCuentaECCData objFicheroCuentaECCData = iteratorCamaraInfo.next();
        if (esNuloTitulos(objFicheroCuentaECCData)) {
          addCamaraInfo(result, objFicheroCuentaECCData);
        }
      }
    }
    Collections.sort(result, new ConciliacionERDTO());

    return result;
  }

  private List<ConciliacionERDTO> conciliaEfectivo(List<ConciliacionERData> svInfo,
      List<ConciliacionERData> custodioInfo, List<FicheroCuentaECCData> camaraInfo, String tipoDiferencia) {
    String PREFIX = "[ConciliacionS3Bo::concilia] ";
    LOG.debug(PREFIX + "[tipoDiferencia: {} ]", tipoDiferencia);

    List<ConciliacionERDTO> result = new LinkedList<ConciliacionERDTO>();

    Iterator<ConciliacionERData> iteratorSVInfo = svInfo.iterator();

    mergeInfoCustodioEfectivo(custodioInfo, result, iteratorSVInfo, tipoDiferencia);
    result = mergeECCInfoEfectivo(result, camaraInfo, tipoDiferencia);
    mergeInfoECCEfectivo(svInfo, result, camaraInfo, tipoDiferencia);

    if ("desCuadrados".equals(tipoDiferencia) || "todos".equals(tipoDiferencia)) {
      iteratorSVInfo = svInfo.iterator();
      while (iteratorSVInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorSVInfo.next();
        if (!esNuloEfectivo(objConciliacionERData)) {
          addSVInfo(result, objConciliacionERData);
          iteratorSVInfo.remove();
        }
      } // while(iteratorSVInfo.hasNext()) {
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      while (iteratorCustodioInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorCustodioInfo.next();
        if (!esNuloEfectivo(objConciliacionERData)) {
          addCustodioInfo(result, objConciliacionERData);
          iteratorCustodioInfo.remove();
        }
      }
      // añade info camara fichero ECC
      Iterator<FicheroCuentaECCData> iteratorCamaraInfo = camaraInfo.iterator();
      while (iteratorCamaraInfo.hasNext()) {
        FicheroCuentaECCData objFicheroCuentaECCData = iteratorCamaraInfo.next();
        if (!esNuloEfectivo(objFicheroCuentaECCData)) {
          addCamaraInfo(result, objFicheroCuentaECCData);
          iteratorCamaraInfo.remove();
        }
      }
    }

    if ("cuadrados".equals(tipoDiferencia) || "todos".equals(tipoDiferencia)) {
      iteratorSVInfo = svInfo.iterator();
      while (iteratorSVInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorSVInfo.next();
        if (esNuloEfectivo(objConciliacionERData)) {
          addSVInfo(result, objConciliacionERData);
        }
      } // while(iteratorSVInfo.hasNext()) {
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      while (iteratorCustodioInfo.hasNext()) {
        ConciliacionERData objConciliacionERData = iteratorCustodioInfo.next();
        if (esNuloEfectivo(objConciliacionERData)) {
          addCustodioInfo(result, objConciliacionERData);
        }
      }
      // añade info camara fichero ECC
      Iterator<FicheroCuentaECCData> iteratorCamaraInfo = camaraInfo.iterator();
      while (iteratorCamaraInfo.hasNext()) {
        FicheroCuentaECCData objFicheroCuentaECCData = iteratorCamaraInfo.next();
        if (esNuloEfectivo(objFicheroCuentaECCData)) {
          addCamaraInfo(result, objFicheroCuentaECCData);
        }
      }
    }
    Collections.sort(result, new ConciliacionERDTO());

    return result;
  }

  private void mergeInfoECCTitulos(List<ConciliacionERData> listSVInfo, List<ConciliacionERDTO> result,
      List<FicheroCuentaECCData> camaraInfo, String tipoDiferencia) {
    Iterator<FicheroCuentaECCData> iteratorCamara = camaraInfo.iterator();
    while (iteratorCamara.hasNext()) {
      FicheroCuentaECCData elementFicheroCuentaECCData = iteratorCamara.next();
      Iterator<ConciliacionERData> iteratorSVInfo = listSVInfo.iterator();
      boolean bEncontrado = false;
      while (iteratorSVInfo.hasNext() && !bEncontrado) {
        ConciliacionERData elementSVInfo = iteratorSVInfo.next();
        String isinSVInfo = elementFicheroCuentaECCData.getIsin().trim();
        String sFechaLiquidacionSV = FormatDataUtils
            .convertDateToString(elementFicheroCuentaECCData.getSettlementdate(), FormatDataUtils.DATE_FORMAT);
        String isinCustodioInfo = elementSVInfo.getIsin().trim();
        String sFechaLiquidacionCustodio = FormatDataUtils.convertDateToString(elementSVInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        if (isinSVInfo.equals(isinCustodioInfo) && sFechaLiquidacionSV.equals(sFechaLiquidacionCustodio)) {

          BigDecimal titulosSV = elementSVInfo.getTitulos();
          if (titulosSV == null) {
            titulosSV = new BigDecimal(0);
          }
          BigDecimal titulosECC = elementFicheroCuentaECCData.getTitulos();
          if (titulosECC == null) {
            titulosECC = new BigDecimal(0);
          }
          BigDecimal efectivoSV = elementSVInfo.getEfectivo();
          if (efectivoSV == null) {
            efectivoSV = new BigDecimal(0);
          }
          BigDecimal efectivoECC = elementFicheroCuentaECCData.getEfectivo();
          if (efectivoECC == null) {
            efectivoECC = new BigDecimal(0);
          }
          ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
          conciliacionERDTO.setSettlementDate(elementFicheroCuentaECCData.getSettlementdate());
          conciliacionERDTO.setIsin(elementFicheroCuentaECCData.getIsin());
          conciliacionERDTO.setDescripcion(elementFicheroCuentaECCData.getDescrIsin());
          conciliacionERDTO.setMercado(elementFicheroCuentaECCData.getMercado());
          conciliacionERDTO.setCodigoCuentaLiquidacion(elementFicheroCuentaECCData.getCodCtaLiquidacion());
          conciliacionERDTO.setTitulosCamara(titulosECC);
          conciliacionERDTO.setEfectivoCamara(efectivoECC);
          conciliacionERDTO.setTitulosSV(titulosSV);
          conciliacionERDTO.setEfectivoSV(efectivoSV);
          conciliacionERDTO.setCorretajeSV(elementSVInfo.getCorretaje());
          conciliacionERDTO.setCorretajeCustodio(new BigDecimal(0.0));
          conciliacionERDTO.setEfectivoCustodio(new BigDecimal(0.0));
          conciliacionERDTO.setTitulosCustodio(new BigDecimal(0.0));
          if ("cuadrados".equals(tipoDiferencia)) {
            if (titulosSV.compareTo(titulosECC) == 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCamara.remove();
              iteratorSVInfo.remove();
            }
          }
          else if ("desCuadrados".equals(tipoDiferencia)) {
            if (titulosSV.compareTo(titulosECC) != 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCamara.remove();
              iteratorSVInfo.remove();
            }
          }
          else {
            result.add(conciliacionERDTO);
            bEncontrado = true;
            iteratorCamara.remove();
            iteratorSVInfo.remove();
          }

        }
      }
    }

  }

  private void mergeInfoECCEfectivo(List<ConciliacionERData> listSVInfo, List<ConciliacionERDTO> result,
      List<FicheroCuentaECCData> camaraInfo, String tipoDiferencia) {
    Iterator<FicheroCuentaECCData> iteratorCamara = camaraInfo.iterator();
    while (iteratorCamara.hasNext()) {
      FicheroCuentaECCData elementFicheroCuentaECCData = iteratorCamara.next();
      Iterator<ConciliacionERData> iteratorSVInfo = listSVInfo.iterator();
      boolean bEncontrado = false;
      while (iteratorSVInfo.hasNext() && !bEncontrado) {
        ConciliacionERData elementSVInfo = iteratorSVInfo.next();
        String isinSVInfo = elementFicheroCuentaECCData.getIsin().trim();
        String sFechaLiquidacionSV = FormatDataUtils
            .convertDateToString(elementFicheroCuentaECCData.getSettlementdate(), FormatDataUtils.DATE_FORMAT);
        String isinCustodioInfo = elementSVInfo.getIsin().trim();
        String sFechaLiquidacionCustodio = FormatDataUtils.convertDateToString(elementSVInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        if (isinSVInfo.equals(isinCustodioInfo) && sFechaLiquidacionSV.equals(sFechaLiquidacionCustodio)) {

          BigDecimal titulosSV = elementSVInfo.getTitulos();
          if (titulosSV == null) {
            titulosSV = new BigDecimal(0);
          }
          BigDecimal titulosECC = elementFicheroCuentaECCData.getTitulos();
          if (titulosECC == null) {
            titulosECC = new BigDecimal(0);
          }
          BigDecimal efectivoSV = elementSVInfo.getEfectivo();
          if (efectivoSV == null) {
            efectivoSV = new BigDecimal(0);
          }
          BigDecimal efectivoECC = elementFicheroCuentaECCData.getEfectivo();
          if (efectivoECC == null) {
            efectivoECC = new BigDecimal(0);
          }

          bEncontrado = true;
          ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
          conciliacionERDTO.setSettlementDate(elementFicheroCuentaECCData.getSettlementdate());
          conciliacionERDTO.setIsin(elementFicheroCuentaECCData.getIsin());
          conciliacionERDTO.setDescripcion(elementFicheroCuentaECCData.getDescrIsin());
          conciliacionERDTO.setMercado(elementFicheroCuentaECCData.getMercado());
          conciliacionERDTO.setCodigoCuentaLiquidacion(elementFicheroCuentaECCData.getCodCtaLiquidacion());
          conciliacionERDTO.setTitulosCamara(titulosECC);
          conciliacionERDTO.setEfectivoCamara(efectivoECC);
          conciliacionERDTO.setTitulosSV(titulosSV);
          conciliacionERDTO.setEfectivoSV(efectivoSV);
          conciliacionERDTO.setCorretajeSV(elementSVInfo.getCorretaje());
          conciliacionERDTO.setCorretajeCustodio(new BigDecimal(0.0));
          conciliacionERDTO.setEfectivoCustodio(new BigDecimal(0.0));
          conciliacionERDTO.setTitulosCustodio(new BigDecimal(0.0));

          if ("cuadrados".equals(tipoDiferencia)) {
            if (efectivoSV.compareTo(efectivoECC) == 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCamara.remove();
              iteratorSVInfo.remove();
            }
          }
          else if ("desCuadrados".equals(tipoDiferencia)) {
            if (efectivoSV.compareTo(efectivoECC) != 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCamara.remove();
              iteratorSVInfo.remove();
            }
          }
          else {
            result.add(conciliacionERDTO);
            bEncontrado = true;
            iteratorCamara.remove();
            iteratorSVInfo.remove();
          }

        }
      }
    }

  }

  private void mergeInfoCustodioTitulos(List<ConciliacionERData> custodioInfo, List<ConciliacionERDTO> result,
      Iterator<ConciliacionERData> iteratorSVInfo, String tipoDiferencia) {

    while (iteratorSVInfo.hasNext()) {
      ConciliacionERData elementSVInfo = iteratorSVInfo.next();
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      boolean bEncontrado = false;
      while (iteratorCustodioInfo.hasNext() && !bEncontrado) {
        ConciliacionERData elementCustodioInfo = iteratorCustodioInfo.next();
        String isinSVInfo = elementSVInfo.getIsin().trim();
        String sFechaLiquidacionSV = FormatDataUtils.convertDateToString(elementSVInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        String isinCustodioInfo = elementCustodioInfo.getIsin().trim();
        String sFechaLiquidacionCustodio = FormatDataUtils.convertDateToString(elementCustodioInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        if (isinSVInfo.equals(isinCustodioInfo) && sFechaLiquidacionSV.equals(sFechaLiquidacionCustodio)) {

          ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
          conciliacionERDTO.setSettlementDate(elementSVInfo.getSettlementDate());
          conciliacionERDTO.setIsin(elementSVInfo.getIsin());
          conciliacionERDTO.setDescripcion(elementSVInfo.getDescripcion());
          conciliacionERDTO.setMercado(elementSVInfo.getMercado());
          conciliacionERDTO.setCodigoCuentaLiquidacion(elementSVInfo.getCodigoCuentaLiquidacion());
          BigDecimal titulosSV = elementSVInfo.getTitulos();
          if (titulosSV == null) {
            titulosSV = new BigDecimal(0);
          }
          BigDecimal titulosCustodio = elementCustodioInfo.getTitulos();
          if (titulosCustodio == null) {
            titulosCustodio = new BigDecimal(0);
          }
          BigDecimal efectivoSV = elementSVInfo.getEfectivo();
          if (efectivoSV == null) {
            efectivoSV = new BigDecimal(0);
          }
          BigDecimal efectivoCustodio = elementCustodioInfo.getEfectivo();
          if (efectivoCustodio == null) {
            efectivoCustodio = new BigDecimal(0);
          }
          conciliacionERDTO.setTitulosSV(titulosSV);
          conciliacionERDTO.setEfectivoSV(efectivoSV);
          conciliacionERDTO.setCorretajeSV(elementSVInfo.getCorretaje());
          conciliacionERDTO.setTitulosCustodio(titulosCustodio);
          conciliacionERDTO.setEfectivoCustodio(efectivoCustodio);
          conciliacionERDTO.setCorretajeCustodio(elementCustodioInfo.getCorretaje());
          if ("cuadrados".equals(tipoDiferencia)) {
            if (titulosSV.compareTo(titulosCustodio) == 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCustodioInfo.remove();
              iteratorSVInfo.remove();
            }
          }
          else if ("desCuadrados".equals(tipoDiferencia)) {
            if (titulosSV.compareTo(titulosCustodio) != 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCustodioInfo.remove();
              iteratorSVInfo.remove();
            }
          }
          else {
            result.add(conciliacionERDTO);
            bEncontrado = true;
            iteratorCustodioInfo.remove();
            iteratorSVInfo.remove();
          }
        }
      }
    }

    // Codigo cambiado para cuando no hay SV
    if (custodioInfo != null && !custodioInfo.isEmpty()) {
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      while (iteratorCustodioInfo.hasNext()) {
        ConciliacionERData elementCustodioInfo = iteratorCustodioInfo.next();

        ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
        conciliacionERDTO.setSettlementDate(elementCustodioInfo.getSettlementDate());
        conciliacionERDTO.setIsin(elementCustodioInfo.getIsin());
        conciliacionERDTO.setDescripcion(elementCustodioInfo.getDescripcion());
        conciliacionERDTO.setMercado(elementCustodioInfo.getMercado());
        conciliacionERDTO.setCodigoCuentaLiquidacion(elementCustodioInfo.getCodigoCuentaLiquidacion());
        BigDecimal titulosSV = BigDecimal.ZERO;

        BigDecimal titulosCustodio = elementCustodioInfo.getTitulos();
        if (titulosCustodio == null) {
          titulosCustodio = BigDecimal.ZERO;
        }
        BigDecimal efectivoSV = BigDecimal.ZERO;
        BigDecimal efectivoCustodio = elementCustodioInfo.getEfectivo();
        if (efectivoCustodio == null) {
          efectivoCustodio = BigDecimal.ZERO;
        }
        conciliacionERDTO.setTitulosSV(titulosSV);
        conciliacionERDTO.setEfectivoSV(efectivoSV);
        conciliacionERDTO.setCorretajeSV(BigDecimal.ZERO);
        conciliacionERDTO.setTitulosCustodio(titulosCustodio);
        conciliacionERDTO.setEfectivoCustodio(efectivoCustodio);
        conciliacionERDTO.setCorretajeCustodio(elementCustodioInfo.getCorretaje());
        if ("cuadrados".equals(tipoDiferencia)) {
          if (esNuloTitulos(elementCustodioInfo)) {
            result.add(conciliacionERDTO);
            iteratorCustodioInfo.remove();
          }
        }
        else if ("desCuadrados".equals(tipoDiferencia)) {
          if (!esNuloTitulos(elementCustodioInfo)) {
            result.add(conciliacionERDTO);
            iteratorCustodioInfo.remove();
          }
        }
        else {
          result.add(conciliacionERDTO);
          iteratorCustodioInfo.remove();
        }
      } // while (iteratorCustodioInfo.hasNext()) {
    } // if (custodioInfo != null && !custodioInfo.isEmpty())
      // Codigo cambiado para cuando no hay SV
  }

  private void mergeInfoCustodioEfectivo(List<ConciliacionERData> custodioInfo, List<ConciliacionERDTO> result,
      Iterator<ConciliacionERData> iteratorSVInfo, String tipoDiferencia) {

    while (iteratorSVInfo.hasNext()) {
      ConciliacionERData elementSVInfo = iteratorSVInfo.next();
      Iterator<ConciliacionERData> iteratorCustodioInfo = custodioInfo.iterator();
      boolean bEncontrado = false;
      while (iteratorCustodioInfo.hasNext() && !bEncontrado) {
        ConciliacionERData elementCustodioInfo = iteratorCustodioInfo.next();
        String isinSVInfo = elementSVInfo.getIsin().trim();
        String sFechaLiquidacionSV = FormatDataUtils.convertDateToString(elementSVInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        String isinCustodioInfo = elementCustodioInfo.getIsin().trim();
        String sFechaLiquidacionCustodio = FormatDataUtils.convertDateToString(elementCustodioInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        if (isinSVInfo.equals(isinCustodioInfo) && sFechaLiquidacionSV.equals(sFechaLiquidacionCustodio)) {

          ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
          conciliacionERDTO.setSettlementDate(elementSVInfo.getSettlementDate());
          conciliacionERDTO.setIsin(elementSVInfo.getIsin());
          conciliacionERDTO.setDescripcion(elementSVInfo.getDescripcion());
          conciliacionERDTO.setMercado(elementSVInfo.getMercado());
          conciliacionERDTO.setCodigoCuentaLiquidacion(elementSVInfo.getCodigoCuentaLiquidacion());
          BigDecimal titulosSV = elementSVInfo.getTitulos();
          if (titulosSV == null) {
            titulosSV = new BigDecimal(0);
          }
          BigDecimal titulosCustodio = elementCustodioInfo.getTitulos();
          if (titulosCustodio == null) {
            titulosCustodio = new BigDecimal(0);
          }
          BigDecimal efectivoSV = elementSVInfo.getEfectivo();
          if (efectivoSV == null) {
            efectivoSV = new BigDecimal(0);
          }
          BigDecimal efectivoCustodio = elementCustodioInfo.getEfectivo();
          if (efectivoCustodio == null) {
            efectivoCustodio = new BigDecimal(0);
          }
          conciliacionERDTO.setTitulosSV(titulosSV);
          conciliacionERDTO.setEfectivoSV(efectivoSV);
          conciliacionERDTO.setCorretajeSV(elementSVInfo.getCorretaje());
          conciliacionERDTO.setTitulosCustodio(titulosCustodio);
          conciliacionERDTO.setEfectivoCustodio(efectivoCustodio);
          conciliacionERDTO.setCorretajeCustodio(elementCustodioInfo.getCorretaje());
          if ("cuadrados".equals(tipoDiferencia)) {
            if (efectivoSV.compareTo(efectivoCustodio) == 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCustodioInfo.remove();
              iteratorSVInfo.remove();
            }
          }
          else if ("desCuadrados".equals(tipoDiferencia)) {
            if (efectivoSV.compareTo(efectivoCustodio) != 0) {
              result.add(conciliacionERDTO);
              bEncontrado = true;
              iteratorCustodioInfo.remove();
              iteratorSVInfo.remove();
            }
          }
          else {
            result.add(conciliacionERDTO);
            bEncontrado = true;
            iteratorCustodioInfo.remove();
            iteratorSVInfo.remove();
          }

        }
      }
    }
  }

  private boolean esNuloTitulos(FicheroCuentaECCData objFicheroCuentaECCData) {
    boolean bResultado = true;
    BigDecimal bTitulos = objFicheroCuentaECCData.getTitulos();
    if (bTitulos == null) {
      bTitulos = BigDecimal.ZERO;
    }
    if (bTitulos.compareTo(BigDecimal.ZERO) != 0) {
      return false;
    }

    return bResultado;
  }

  private boolean esNuloEfectivo(FicheroCuentaECCData objFicheroCuentaECCData) {
    boolean bResultado = true;
    BigDecimal bEfectivo = objFicheroCuentaECCData.getEfectivo();
    if (bEfectivo == null) {
      bEfectivo = BigDecimal.ZERO;
    }
    if (bEfectivo.compareTo(BigDecimal.ZERO) != 0) {
      return false;
    }
    return bResultado;
  }

  private void addCamaraInfo(List<ConciliacionERDTO> result, FicheroCuentaECCData elementCamaraInfo) {

    ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
    conciliacionERDTO.setSettlementDate(elementCamaraInfo.getSettlementdate());
    conciliacionERDTO.setIsin(elementCamaraInfo.getIsin());

    conciliacionERDTO.setMercado(elementCamaraInfo.getMercado());
    conciliacionERDTO.setCodigoCuentaLiquidacion(elementCamaraInfo.getCodCtaLiquidacion());
    conciliacionERDTO.setTitulosSV(new BigDecimal(0));
    conciliacionERDTO.setDescripcion(elementCamaraInfo.getDescrIsin());
    conciliacionERDTO.setMercado(elementCamaraInfo.getMercado());
    conciliacionERDTO.setEfectivoSV(new BigDecimal(0));
    conciliacionERDTO.setCorretajeSV(new BigDecimal(0));
    conciliacionERDTO.setTitulosCustodio(new BigDecimal(0));
    conciliacionERDTO.setEfectivoCustodio(new BigDecimal(0));
    conciliacionERDTO.setCorretajeCustodio(new BigDecimal(0));
    conciliacionERDTO.setTitulosCamara(elementCamaraInfo.getTitulos());
    conciliacionERDTO.setEfectivoCamara(elementCamaraInfo.getEfectivo());
    result.add(conciliacionERDTO);

  }

  private boolean esNuloTitulos(ConciliacionERData objConciliacionERData) {
    boolean bResultado = true;
    BigDecimal bTitulos = objConciliacionERData.getTitulos();
    if (bTitulos == null) {
      bTitulos = BigDecimal.ZERO;
    }
    if (bTitulos.compareTo(BigDecimal.ZERO) != 0) {
      return false;
    }
    return bResultado;
  }

  private boolean esNuloEfectivo(ConciliacionERData objConciliacionERData) {
    boolean bResultado = true;
    BigDecimal bEfectivo = objConciliacionERData.getEfectivo();
    if (bEfectivo == null) {
      bEfectivo = BigDecimal.ZERO;
    }
    if (bEfectivo.compareTo(BigDecimal.ZERO) != 0) {
      return false;
    }
    return bResultado;
  }

  private void addSVInfo(List<ConciliacionERDTO> result, ConciliacionERData elementSVInfo) {

    ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
    conciliacionERDTO.setSettlementDate(elementSVInfo.getSettlementDate());
    conciliacionERDTO.setIsin(elementSVInfo.getIsin());
    conciliacionERDTO.setDescripcion(elementSVInfo.getDescripcion());
    conciliacionERDTO.setMercado(elementSVInfo.getMercado());
    conciliacionERDTO.setCodigoCuentaLiquidacion(elementSVInfo.getCodigoCuentaLiquidacion());
    conciliacionERDTO.setTitulosSV(elementSVInfo.getTitulos());
    conciliacionERDTO.setEfectivoSV(elementSVInfo.getEfectivo());
    conciliacionERDTO.setCorretajeSV(elementSVInfo.getCorretaje());
    conciliacionERDTO.setTitulosCustodio(new BigDecimal(0));
    conciliacionERDTO.setEfectivoCustodio(new BigDecimal(0));
    conciliacionERDTO.setCorretajeCustodio(new BigDecimal(0));
    conciliacionERDTO.setTitulosCamara(new BigDecimal(0));
    conciliacionERDTO.setEfectivoCamara(new BigDecimal(0));
    result.add(conciliacionERDTO);
  }

  private void addCustodioInfo(List<ConciliacionERDTO> result, ConciliacionERData elementCustodioInfo) {
    ConciliacionERDTO conciliacionERDTO = new ConciliacionERDTO();
    conciliacionERDTO.setSettlementDate(elementCustodioInfo.getSettlementDate());
    conciliacionERDTO.setIsin(elementCustodioInfo.getIsin());
    conciliacionERDTO.setDescripcion(elementCustodioInfo.getDescripcion());
    conciliacionERDTO.setMercado(elementCustodioInfo.getMercado());
    conciliacionERDTO.setCodigoCuentaLiquidacion(elementCustodioInfo.getCodigoCuentaLiquidacion());
    conciliacionERDTO.setTitulosSV(new BigDecimal(0));
    conciliacionERDTO.setEfectivoSV(new BigDecimal(0));
    conciliacionERDTO.setCorretajeSV(new BigDecimal(0));
    conciliacionERDTO.setTitulosCustodio(elementCustodioInfo.getTitulos());
    conciliacionERDTO.setEfectivoCustodio(elementCustodioInfo.getEfectivo());
    conciliacionERDTO.setCorretajeCustodio(elementCustodioInfo.getCorretaje());
    conciliacionERDTO.setTitulosCamara(new BigDecimal(0));
    conciliacionERDTO.setEfectivoCamara(new BigDecimal(0));
    result.add(conciliacionERDTO);
  }

  private List<ConciliacionERDTO> mergeECCInfoTitulos(final List<ConciliacionERDTO> result,
      List<FicheroCuentaECCData> camaraInfo, String tipoDiferencia) {

    Iterator<ConciliacionERDTO> itER = result.iterator();

    Iterator<FicheroCuentaECCData> itECC = camaraInfo.iterator();

    while (itER.hasNext()) {
      ConciliacionERDTO elementSVInfo = itER.next();
      boolean bEncontrado = false;
      while (itECC.hasNext() && !bEncontrado) {
        FicheroCuentaECCData ecc = itECC.next();

        String isinSVInfo = elementSVInfo.getIsin().trim();
        String sFechaLiquidacionSV = FormatDataUtils.convertDateToString(elementSVInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        String isinECCInfo = ecc.getIsin().trim();
        String sFechaLiquidacionECC = FormatDataUtils.convertDateToString(ecc.getSettlementdate(),
            FormatDataUtils.DATE_FORMAT);
        if (isinSVInfo.equals(isinECCInfo) && sFechaLiquidacionSV.equals(sFechaLiquidacionECC)) {

          BigDecimal titulosSV = elementSVInfo.getTitulosSV();
          if (titulosSV == null) {
            titulosSV = new BigDecimal(0);
          }
          BigDecimal titulosECC = ecc.getTitulos();
          if (titulosECC == null) {
            titulosECC = new BigDecimal(0);
          }
          BigDecimal efectivoSV = elementSVInfo.getEfectivoSV();
          if (efectivoSV == null) {
            efectivoSV = new BigDecimal(0);
          }
          BigDecimal efectivoECC = ecc.getEfectivo();
          if (efectivoECC == null) {
            efectivoECC = new BigDecimal(0);
          }
          bEncontrado = true;
          elementSVInfo.setTitulosCamara(titulosECC);
          elementSVInfo.setEfectivoCamara(efectivoECC);
          if ("cuadrados".equals(tipoDiferencia)) {

            itECC.remove();
            if (titulosSV.compareTo(titulosECC) == 0) {

            }
            else {
              itER.remove();
            }
          }
          else if ("desCuadrados".equals(tipoDiferencia)) {
            if (titulosSV.compareTo(titulosECC) != 0) {
              itECC.remove();
            }

          }
          else {

            itECC.remove();
          }
        }
      }
    }
    return result;
  }

  private List<ConciliacionERDTO> mergeECCInfoEfectivo(final List<ConciliacionERDTO> result,
      List<FicheroCuentaECCData> camaraInfo, String tipoDiferencia) {

    Iterator<ConciliacionERDTO> itER = result.iterator();

    Iterator<FicheroCuentaECCData> itECC = camaraInfo.iterator();

    while (itER.hasNext()) {
      ConciliacionERDTO elementSVInfo = itER.next();
      boolean bEncontrado = false;
      while (itECC.hasNext() && !bEncontrado) {
        FicheroCuentaECCData ecc = itECC.next();

        String isinSVInfo = elementSVInfo.getIsin().trim();
        String sFechaLiquidacionSV = FormatDataUtils.convertDateToString(elementSVInfo.getSettlementDate(),
            FormatDataUtils.DATE_FORMAT);
        String isinECCInfo = ecc.getIsin().trim();
        String sFechaLiquidacionECC = FormatDataUtils.convertDateToString(ecc.getSettlementdate(),
            FormatDataUtils.DATE_FORMAT);
        if (isinSVInfo.equals(isinECCInfo) && sFechaLiquidacionSV.equals(sFechaLiquidacionECC)) {

          BigDecimal titulosSV = elementSVInfo.getTitulosSV();
          if (titulosSV == null) {
            titulosSV = new BigDecimal(0);
          }
          BigDecimal titulosECC = ecc.getTitulos();
          if (titulosECC == null) {
            titulosECC = new BigDecimal(0);
          }
          BigDecimal efectivoSV = elementSVInfo.getEfectivoSV();
          if (efectivoSV == null) {
            efectivoSV = new BigDecimal(0);
          }
          BigDecimal efectivoECC = ecc.getEfectivo();
          if (efectivoECC == null) {
            efectivoECC = new BigDecimal(0);
          }

          bEncontrado = true;
          elementSVInfo.setTitulosCamara(titulosECC);
          elementSVInfo.setEfectivoCamara(efectivoECC);
          if ("cuadrados".equals(tipoDiferencia)) {

            itECC.remove();
            if (efectivoSV.compareTo(efectivoECC) == 0) {

            }
            else {
              itER.remove();
            }
          }
          else if ("desCuadrados".equals(tipoDiferencia)) {
            if (efectivoSV.compareTo(efectivoECC) != 0) {
              itECC.remove();
            }

          }
          else {

            itECC.remove();
          }

        }
      }
    }
    return result;
  }

}