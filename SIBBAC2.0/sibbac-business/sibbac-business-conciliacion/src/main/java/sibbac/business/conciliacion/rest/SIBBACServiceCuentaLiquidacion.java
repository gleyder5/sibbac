package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.MercadoBo;
import sibbac.business.wrappers.database.bo.RelacionCuentaMercadoBo;
import sibbac.business.wrappers.database.bo.SistemaLiquidacionBo;
import sibbac.business.wrappers.database.bo.TipoCuentaConciliacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0EntidadRegistroBo;
import sibbac.business.wrappers.database.bo.Tmct0TipoNeteoBo;
import sibbac.business.wrappers.database.data.CuentaDeCompensacionData;
import sibbac.business.wrappers.database.data.CuentaLiquidacionData;
import sibbac.business.wrappers.database.data.TipoCuentaConciliacionData;
import sibbac.business.wrappers.database.data.TipoNeteoData;
import sibbac.business.wrappers.database.model.RelacionCuentaMercado;
import sibbac.business.wrappers.database.model.SistemaLiquidacion;
import sibbac.business.wrappers.database.model.TipoCuentaConciliacion;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;
import sibbac.business.wrappers.database.model.Tmct0Mercado;
import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * The Class SIBBACServiceCuentaLiquidacion.
 */
@SIBBACService
public class SIBBACServiceCuentaLiquidacion implements SIBBACServiceBean {

    private static final String TAG = SIBBACServiceCuentaLiquidacion.class
	    .getName();

    /** The Constant CMD_GET_CUENTA_LIQUIDACION. */
    private static final String CMD_GET_CUENTA_LIQUIDACION = "getCuentaLiquidacion";

    /** The Constant CMD_ADD_CUENTA_LIQUIDACION. */
    private static final String CMD_ADD_CUENTA_LIQUIDACION = "addCuentaLiquidacion";

    /** The Constant CMD_GET_TIPO_CUENTA. */
    private static final String CMD_GET_TIPO_CUENTA = "getTipoCuenta";

    /** The Constant CMD_GET_TIPO_NETEO. */
    private static final String CMD_GET_TIPO_NETEO = "getTipoNeteo";

    /** The Constant CMD_GET_CUENTA_RELACIONADA. */
    private static final String CMD_GET_CUENTA_RELACIONADA = "getCuentaRelacionada";

    /** The Constant CMD_GET_SISTEMAS_LIQUIDACION. */
    private static final String CMD_GET_SISTEMAS_LIQUIDACION = "getSistemasLiquidacion";

    /** The Constant CMD_GET_TIPO_FECHA. */
    private static final String CMD_GET_TIPO_FICHERO = "getTipoFichero";

    /** The Constant CMD_GET_TIPO_FECHA. */
    private static final String CMD_GET_CUENTA_COMPENSACION = "getCuentaCompensacion";
    /** The Constant CMD_GET_TIPO_FECHA. */
    private static final String CMD_GET_TIPO_ER = "getTipoER";
    /** Obtiene las relaciones entre cuentas de liquidacion y mercados */
    private static final String CMD_GET_LINKED_CUENTA_MERCADO = "getCuentasMercado";
    // RESULTADOS CONSTANTS
    private static final String RESULT_CUENTAS_LIQUIDACION = "result_cuentas_liquidacion";
    private static final String RESULT_CUENTAS_MERCADOS = "result_cuentas_mercados";
    private static final String RESULT_CUENTAS_COMPENSACION = "result_cuentas_compensacion";
    private static final String RESULT_SISTEMAS_LIQUIDACION = "result_sistemas_liquidacion";

    // util
    // private ConciliacionUtil util = ConciliacionUtil.getInstance();

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    // Servicios
    @Autowired
    private Tmct0CuentaLiquidacionBo bo;
    @Autowired
    private Tmct0TipoNeteoBo tpNeteoBo;
    @Autowired
    private TipoCuentaConciliacionBo tipoCuentaBo;
    @Autowired
    private RelacionCuentaMercadoBo relacionCuentaMercadoBo;
    @Autowired
    private MercadoBo mercadoBo;
    @Autowired
    private Tmct0CuentasDeCompensacionBo ccBo;
    @Autowired
    private Tmct0EntidadRegistroBo entidadRegistroBo;
    @Autowired
    private SistemaLiquidacionBo sistemaBo;
    @Autowired
    private SistemaLiquidacionBo sistemaLiquidacionBo;

    /*
     * (non-Javadoc)
     * 
     * @see
     * sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.
     * WebRequest)
     */
    @Override
    public WebResponse process(WebRequest webRequest)
	    throws SIBBACBusinessException {

	WebResponse webResponse = new WebResponse();
	String prefix = "[SIBBACServiceCuentaLiquidacion ::process] ";
	String command = webRequest.getAction();
	Map<String, Object> result = null;
	Map<String, String> params;

	if (webRequest.getFilters() != null) {
	    params = webRequest.getFilters();
	} else {
	    params = new HashMap<String, String>();
	}
	Map<String, Serializable> filters = normalizeParams(params);
	if (command == null) {
	    command = "";
	}

	LOG.debug(prefix + "Processing a web request...");
	LOG.debug(prefix + "+ Command.: [{}]", command);

	switch (command) {
	case CMD_GET_CUENTA_LIQUIDACION:
	    result = getCuentaLiquidacion(filters);
	    break;
	case CMD_ADD_CUENTA_LIQUIDACION:
	    try {
		result = addCuentaLiquidacion(filters);
	    } catch (NumberFormatException | ClassCastException
		    | SQLIntegrityConstraintViolationException
		    | PersistenceException | NullPointerException ex) {
		LOG.error(ex.getMessage(), ex);
		webResponse.setError(ex.getMessage());
	    }
	    break;
	case CMD_GET_TIPO_CUENTA:
	    result = getTipoCuenta(params);
	    break;
	case CMD_GET_TIPO_NETEO:
	    result = getTipoNeteo(params);
	    break;
	case CMD_GET_CUENTA_RELACIONADA:
	    result = getCuentaRelacionada(params);
	    break;
	case CMD_GET_TIPO_FICHERO:
	    result = getTipoFichero(params);
	    break;
	case CMD_GET_CUENTA_COMPENSACION:
	    result = getCuentaCompensacion(params);
	    break;
	case CMD_GET_TIPO_ER:// se usa directamente la del servicio
	    // EntidadRegistro
	    result = getTipoER(params);
	    break;
	case CMD_GET_LINKED_CUENTA_MERCADO:
	    result = getCuentasMercado(params);
	    break;
	case CMD_GET_SISTEMAS_LIQUIDACION:
	    result = getSistemasLiquidacion();
	    break;
	}

	if (result == null) {
	    result = new HashMap<String, Object>();
	}
	webResponse.setResultados(result);

	return webResponse;
    }

    private Map<String, Object> getSistemasLiquidacion() {
	Map<String, Object> result = new HashMap<String, Object>();
	result.put(RESULT_SISTEMAS_LIQUIDACION, sistemaBo.findAll());
	return result;
    }

    /**
     * Convierte el tipo de los valores de los filtros a tipos Serializables
     * 
     * @param params
     * @return
     */
    private Map<String, Serializable> normalizeParams(Map<String, String> params) {
	Map<String, Serializable> result = new HashMap<String, Serializable>();
	for (Map.Entry<String, String> entry : params.entrySet()) {
	    if (entry.getValue() != null && !"".equals(entry.getValue())
		    && !entry.getValue().equals("0")) {
		if (entry.getKey().contains(".id")
			&& !entry.getKey().equals(
				"tmct0CuentaLiquidacion.idMercado")) {
		    result.put(entry.getKey(), Long.valueOf(entry.getValue()));
		} else if (entry.getKey().equals("idMercado")) {
		    String[] ids = entry.getValue().split(",");
		    List<Long> idList = new LinkedList<Long>();
		    for (String strId : ids) {
			try {
			    parseLong(idList, strId);
			} catch (NumberFormatException ex) {
			    LOG.error(ex.getMessage(), ex);
			}
		    }
		    result.put(entry.getKey(), (Serializable) idList);
		} else if (entry.getKey().equals(
			"tmct0CuentaLiquidacion.frecuenciaEnvioExtracto")
			|| entry.getKey().startsWith(
				"tmct0CuentaLiquidacion.id")
			|| entry.getKey().startsWith("id")) {
		    long frec = 0L;
		    try {
			frec = Long.parseLong(entry.getValue().trim());
		    } catch (NumberFormatException ex) {
			LOG.error(ex.getMessage(), ex);
		    }
		    result.put(entry.getKey(), frec);
		} else {
		    result.put(entry.getKey(), entry.getValue());
		}
	    }
	}
	return result;
    }

    private void parseLong(List<Long> idList, String strId)
	    throws NumberFormatException {
	long id = Long.parseLong(strId.trim());
	idList.add(id);
    }

    /**
     * Gets the tipo fecha.
     *
     * @param params
     *            the params
     * @return the tipo fecha
     */
    private Map<String, Object> getTipoFichero(Map<String, String> params) {

	Map<String, Object> result = new HashMap<String, Object>();
	String[] tFechas = { "norma 43", "a medida" };
	result.put("result_tipo_fichero", tFechas);
	return result;
    }

    /**
     * Gets the cuenta relacionada.
     *
     * @param params
     *            the params
     * @return the cuenta relacionada
     */
    private Map<String, Object> getCuentaRelacionada(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<CuentaDeCompensacionData> cuentas = ccBo.findAllData();
	result.put(RESULT_CUENTAS_COMPENSACION, cuentas);
	return result;
    }

    /**
     * Gets the tipo neteo.
     *
     * @param params
     *            the params
     * @return the tipo neteo
     */
    private Map<String, Object> getTipoNeteo(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<TipoNeteoData> neteos = tpNeteoBo.findAllData();

	result.put("result_tipo_Neteo", neteos);
	return result;
    }

    /**
     * Gets the tipo cuenta.
     *
     * @param params
     *            the params
     * @return the tipo cuenta
     */
    private Map<String, Object> getTipoCuenta(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<TipoCuentaConciliacionData> tipos = tipoCuentaBo.findAllData();
	result.put("result_tipo_cuenta", tipos);
	return result;
    }

    /**
     * Adds the cuenta liquidacion.
     *
     * @param params
     *            the params
     * @return the map
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> addCuentaLiquidacion(
	    Map<String, Serializable> params) throws NumberFormatException,
	    ClassCastException, SQLIntegrityConstraintViolationException,
	    PersistenceException {
	Map<String, Object> result = new HashMap<String, Object>();
	if (params.size() > 0) {
	    Tmct0CuentaLiquidacion cl = null;
	    if (params.get("idCuentaLiquidacion") != null && !params.get("idCuentaLiquidacion").equals("")) {
		String val = params.get("idCuentaLiquidacion").toString();
		long idCl = Long.parseLong(val);
		cl = bo.findById(idCl);
	    } else {
		cl = new Tmct0CuentaLiquidacion();

	    }
	    List<Tmct0Mercado> mercados = new LinkedList<Tmct0Mercado>();
	    List<Tmct0CuentasDeCompensacion> ccList = new LinkedList<Tmct0CuentasDeCompensacion>();
	    for (Map.Entry<String, Serializable> entry : params.entrySet()) {
		if (entry.getValue() != null && !"".equals(entry.getKey())) {
		    final String nameParam = entry.getKey();
		    final Serializable valor = entry.getValue();
		    switch (nameParam) {
		    case ("frecuenciaEnvioExtracto"):
			long frec = Long.parseLong(valor.toString().trim());
			cl.setFrecuenciaEnvioExtracto(frec);
			break;
		    case ("entidadRegistro"):
			long idER = Long.parseLong(valor.toString().trim());
			Tmct0EntidadRegistro er = entidadRegistroBo
				.findById(idER);
			cl.setEntidadRegistro(er);
			break;
		    case ("idMercado"):
			mercados = mercadoBo.findByIdIn((List<Long>) valor);
			break;
		    case ("numCuentaER"):
			long numCta = 0L;
			numCta = Long.parseLong(valor.toString().trim());
			cl.setNumCuentaER(numCta);
			break;
		    case ("esCuentaPropia"):
			// por el campo esCuentaPropia se sabe tambien el tipo
			// de cuenta ya que contiene el id del tipo de cuenta,
			// el cual si
			// tiene el valor 1 es propia
			if (valor.equals("1")) {
			    cl.setEsCuentaPropia(true);
			} else {
			    cl.setEsCuentaPropia(false);
			}
			long idTp = Long.valueOf(valor.toString().trim());
			TipoCuentaConciliacion tpCta = tipoCuentaBo
				.findById(idTp);
			cl.setTipoCuentaConciliacion(tpCta);
			break;
		    case ("tipoNeteoTitulo"):
			long idTpNto = Long.parseLong(valor.toString().trim());
			Tmct0TipoNeteo tpNto = tpNeteoBo.findById(idTpNto);
			cl.setTipoNeteoTitulo(tpNto);
			break;
		    case "tipoNeteoEfectivo":
			long idTpNtoEf = Long
				.parseLong(valor.toString().trim());
			Tmct0TipoNeteo tpNtoEf = tpNeteoBo.findById(idTpNtoEf);
			cl.setTipoNeteoEfectivo(tpNtoEf);
			break;
		    case "cdCodigo":
			String cdCodigo = valor.toString().trim();
			cl.setCdCodigo(cdCodigo);
			break;
		    case "idCtaCompensacion":
			ccList = ccBo
				.findByidCuentaCompensacionIn((List<Long>) valor);
			/*
			 * TODO CAMBIO RELACION CUENTA LIQUIDACION - CUENTA
			 * COMPESACION
			 */
			// cl.setTmct0CuentasDeCompensacion(cc);
			break;
		    case "tipoFicheroConciliacion":
			cl.setTipoFicheroConciliacion(valor.toString());
			break;
		    case "idSistemaLiquidacion":
			long idSL = Long.parseLong(valor.toString());
			SistemaLiquidacion sl = sistemaLiquidacionBo
				.findById(idSL);
			cl.setSistemaLiquidacion(sl);
			break;
		    case "cuentaIberclear":
			String cuentaIberclear = valor.toString();
			cl.setCuentaIberclear(cuentaIberclear);
			break;
		    }
		}
	    }
	    try {
		cl = bo.save(cl);
		guardarCuentaMercados(mercados, cl);
	    } catch (Exception ex) {
		LOG.error(ex.getMessage(), ex);
	    }
	    /**
	     * En principio las cuentas de liquidacion se crearan primero y
	     * luego en la pantalla del CRUD de cuentas de compesnacion de la
	     * fase1 se crearan las ctas de compensacion y se le añadian las de
	     * liquidacion
	     **/
	    // guardarCuentasDeCompensacion(ccList, cl);
	}
	return result;
    }

    @SuppressWarnings("unused")
    private void guardarCuentasDeCompensacion(
	    List<Tmct0CuentasDeCompensacion> ccList, Tmct0CuentaLiquidacion cl) {
	for (Tmct0CuentasDeCompensacion cc : ccList) {
	    cc.setTmct0CuentaLiquidacion(cl);
	}
	ccBo.save(ccList);
    }

    /**
     * Añade relaciones entre la cuenta de liquidacion y el mercado
     * especificados por parametro
     * 
     * @param mercados
     * @param cl
     */
    private void guardarCuentaMercados(List<Tmct0Mercado> mercados,
	    Tmct0CuentaLiquidacion cl) throws PersistenceException {
	// Primero se eliminan los mercados asociados a la cuenta
	// por si se trata de una modificacion y se quieren quitar algunos
	// mercados habra que eliminarlos todos antes
	relacionCuentaMercadoBo.deleteByIdCuentaLiquidacion(cl);
	for (Tmct0Mercado mkt : mercados) {
	    RelacionCuentaMercado rel = new RelacionCuentaMercado(mkt, cl);
	    relacionCuentaMercadoBo.save(rel);
	}
    }

    /**
     * Gets the cuenta liquidacion.
     *
     * @param filters
     *            the params
     * @return the cuenta liquidacion
     */
    private Map<String, Object> getCuentaLiquidacion(
	    Map<String, Serializable> filters) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<CuentaLiquidacionData> listResult = bo.findAllFiltered(filters);
	result.put(RESULT_CUENTAS_LIQUIDACION, listResult);
	return result;
    }

    /**
     * Gets the tipo fecha.
     *
     * @param params
     *            the params
     * @return the tipo fecha
     */
    private Map<String, Object> getCuentaCompensacion(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	String[] cc = { "5421456452145642", "5421567861456654",
		"5478964521456654", "5523652656145675" };
	result.put("result_cuentas_compensatorias", cc);
	return result;
    }

    /**
     * Obtine tipos Entidad Registro
     * 
     * @param params
     * @return
     */
    private Map<String, Object> getTipoER(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	String[] tER = { "DCV", "ECC", "Custodio" };
	result.put("result_tipo_ER", tER);
	return result;
    }

    private Map<String, Object> getCuentasMercado(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	result.put(RESULT_CUENTAS_MERCADOS, mercadoBo.findAllData());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
     */
    @Override
    public List<String> getAvailableCommands() {
	return Arrays.asList(new String[] { CMD_GET_CUENTA_LIQUIDACION,
		CMD_ADD_CUENTA_LIQUIDACION, CMD_GET_TIPO_CUENTA,
		CMD_GET_TIPO_NETEO, CMD_GET_CUENTA_RELACIONADA,
		CMD_GET_TIPO_FICHERO, CMD_GET_CUENTA_COMPENSACION,
		CMD_GET_TIPO_ER, CMD_GET_LINKED_CUENTA_MERCADO });
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
     */
    @Override
    public List<String> getFields() {
	return Arrays.asList(new String[] {});
    }

}
