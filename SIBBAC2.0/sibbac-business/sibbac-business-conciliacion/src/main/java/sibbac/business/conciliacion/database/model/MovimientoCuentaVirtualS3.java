/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.conciliacion.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 *
 * @author jmazorin
 */
@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_MOVIMIENTO_CUENTA_VIRTUAL_S3/*
																		 * ,
																		 * indexes={@Index(columnList="ER"),
																		 * 
																		 * @Index(columnList="NUMCUENTAER")}
																		 */)
@XmlRootElement
public class MovimientoCuentaVirtualS3 extends ATable< MovimientoCuentaVirtualS3 > implements Serializable {

	private static final long	serialVersionUID	= 1L;

	@XmlAttribute
	@Column( name = "ISIN", nullable = false, length = 12 )
	private String				isin;

	@XmlAttribute
	@Column( name = "CAMARA", nullable = true, length = 4 )
	private String				camara;

	@XmlAttribute
	@Column( name = "IDCUENTACOMP", nullable = true )
	private long				idcuentacomp;

	@XmlAttribute
	@Column( name = "CDCODIGOCCOMP", nullable = true, length = 16 )
	private String				cdcodigoccomp;

	@XmlAttribute
	@Column( name = "IDCUENTALIQ", nullable = true )
	private long				idcuentaliq;

	@XmlAttribute
	@Column( name = "CDCODIGOCLIQ", nullable = true, length = 16 )
	private String				cdcodigocliq;

	@XmlAttribute
	@Column( name = "TRADEDATE", nullable = true )
	private Date				tradedate;

	@XmlAttribute
	@Column( name = "SETTLEMENTDATE", nullable = true )
	private Date				settlementdate;

	@XmlAttribute
	@Column( name = "PRECIO", nullable = false, precision = 15, scale = 8 )
	private BigDecimal			precio;

	@XmlAttribute
	@Column( name = "TITULOS", nullable = false, precision = 25, scale = 8 )
	private BigDecimal			titulos;

	@XmlAttribute
	@Column( name = "EFECTIVO", nullable = false, precision = 25, scale = 4 )
	private BigDecimal			efectivo;

	@XmlAttribute
	@Column( name = "DESCR_ISIN", length = 90 )
	private String				descrIsin;

	@XmlAttribute
	@Column( name = "MERCADO", nullable = false, length = 3 )
	private String				mercado;

	@XmlAttribute
	@Column( name = "COD_DEPOSITARIO", nullable = false, length = 20 )
	private String				codDepositario;

	@XmlAttribute
	@Column( name = "SUBCUENTA_DEPOSITARIO", length = 35 )
	private String				subDepositario;

	@XmlAttribute
	@Column( name = "TIPO_POSICION", nullable = false, length = 4 )
	private String				tipoPosicion;

	@XmlAttribute
	@Column( name = "UNIDAD", nullable = false, length = 4 )
	private String				unidad;

	@XmlAttribute
	@Column( name = "TIPO_PRECIO", length = 4 )
	private String				tipoPrecio;

	@XmlAttribute
	@Column( name = "DIVISA_PRECIO", length = 4 )
	private String				divisaPrecio;

	@XmlAttribute
	@Column( name = "TASA_CAMBIO", precision = 15, scale = 8 )
	private BigDecimal			tasaCambio;

	@XmlAttribute
	@Column( name = "DIVISA_BASE", length = 3 )
	private String				divisaBase;

	@XmlAttribute
	@Column( name = "VALOR_BASE", precision = 25, scale = 4 )
	private BigDecimal			valorBase;

	@XmlAttribute
	@Column( name = "CD_CUENTA_S3", length = 10 )
	private String				cuentaS3;

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public long getIdcuentacomp() {
		return idcuentacomp;
	}

	public void setIdcuentacomp( long idcuentacomp ) {
		this.idcuentacomp = idcuentacomp;
	}

	public String getCdcodigoccomp() {
		return cdcodigoccomp;
	}

	public void setCdcodigoccomp( String cdcodigoccomp ) {
		this.cdcodigoccomp = cdcodigoccomp;
	}

	public long getIdcuentaliq() {
		return idcuentaliq;
	}

	public void setIdcuentaliq( long idcuentaliq ) {
		this.idcuentaliq = idcuentaliq;
	}

	public String getCdcodigocliq() {
		return cdcodigocliq;
	}

	public void setCdcodigocliq( String cdcodigocliq ) {
		this.cdcodigocliq = cdcodigocliq;
	}

	public Date getTradedate() {
		return tradedate;
	}

	public void setTradedate( Date tradedate ) {
		this.tradedate = tradedate;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio( BigDecimal precio ) {
		this.precio = precio;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public String getDescrIsin() {
		return descrIsin;
	}

	public void setDescrIsin( String descrIsin ) {
		this.descrIsin = descrIsin;
	}

	public String getMercado() {
		return mercado;
	}

	public void setMercado( String mercado ) {
		this.mercado = mercado;
	}

	public String getCodDepositario() {
		return codDepositario;
	}

	public void setCodDepositario( String codDepositario ) {
		this.codDepositario = codDepositario;
	}

	public String getSubDepositario() {
		return subDepositario;
	}

	public void setSubDepositario( String subDepositario ) {
		this.subDepositario = subDepositario;
	}

	public String getTipoPosicion() {
		return tipoPosicion;
	}

	public void setTipoPosicion( String tipoPosicion ) {
		this.tipoPosicion = tipoPosicion;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad( String unidad ) {
		this.unidad = unidad;
	}

	public String getTipoPrecio() {
		return tipoPrecio;
	}

	public void setTipoPrecio( String tipoPrecio ) {
		this.tipoPrecio = tipoPrecio;
	}

	public String getDivisaPrecio() {
		return divisaPrecio;
	}

	public void setDivisaPrecio( String divisaPrecio ) {
		this.divisaPrecio = divisaPrecio;
	}

	public BigDecimal getTasaCambio() {
		return tasaCambio;
	}

	public void setTasaCambio( BigDecimal tasaCambio ) {
		this.tasaCambio = tasaCambio;
	}

	public String getDivisaBase() {
		return divisaBase;
	}

	public void setDivisaBase( String divisaBase ) {
		this.divisaBase = divisaBase;
	}

	public BigDecimal getValorBase() {
		return valorBase;
	}

	public void setValorBase( BigDecimal valorBase ) {
		this.valorBase = valorBase;
	}

	public String getCuentaS3() {
		return this.cuentaS3;
	}

	public void setCuentaS3( String cuentaS3 ) {
		this.cuentaS3 = cuentaS3;
	}

}
