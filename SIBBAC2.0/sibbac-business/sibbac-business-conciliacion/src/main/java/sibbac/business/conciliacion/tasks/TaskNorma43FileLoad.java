package sibbac.business.conciliacion.tasks;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import sibbac.business.conciliacion.database.bo.Norma43FicheroBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@SIBBACJob(group = Task.GROUP_CONCILIACION.NAME,
           name = Task.GROUP_CONCILIACION.JOB_NORMA43_FILE_LOAD,
           jobType = SIBBACJobType.CRON_JOB,
           cronExpression = "0 0/5 * ? * MON-FRI")
public class TaskNorma43FileLoad extends WrapperTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskNorma43FileLoad.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Ruta de entrada de ficheros. */
  @Value("${conciliacion.norma43.source.folder}")
  private String sourcePath;

  /** Business object para la tabla <code>TMCT0_FICHERO_NORMA_43</code>. */
  @Autowired
  private Norma43FicheroBo ficheroN43Bo;

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

@Override
public void executeTask() throws Exception {
	// TODO Auto-generated method stub
	   final Long startTime = System.currentTimeMillis();

	    LOG.debug("[TaskNorma43FileLoad :: execute] Iniciando la tarea de carga de ficheros norma43 ...");

	    Tmct0men tmct0men;
	    try {

	      if (sourcePath == null || sourcePath.trim().length() == 0) {
	        LOG.error("[TaskNorma43FileLoad :: execute] No se ha podido leer el directorio de entrada de la configuración");
	        LOG.info("[TaskNorma43FileLoad :: execute] Estableciendo estado TMCT0MSC.LISTO_GENERAR");
	      } else {
	        try {
	          ficheroN43Bo.procesarFicherosNorma43(sourcePath);
	          LOG.info("[TaskNorma43FileLoad :: execute] Tarea terminada correctamente");
	          LOG.info("[TaskNorma43FileLoad :: execute] Estableciendo estado TMCT0MSC.LISTO_GENERAR");


	        } catch (Exception e) {
	          LOG.error("[TaskNorma43FileLoad :: execute] Error al ejecutar la tarea ... ", e.getMessage());
	          LOG.info("[TaskNorma43FileLoad :: execute] Estableciendo estado TMCT0MSC.EN_ERROR");
	        } // catch
	      } // else
	    } finally {
	      	final Long endTime = System.currentTimeMillis();
	      	LOG.info("[TaskNorma43FileLoad :: execute] Finalizada la tarea de carga de ficheros norma43 ... [" + String.format("%d min, %d sec",
					TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
	                TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
	                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
	               + "]");
	      } // finally
		}


	@Override
	public TIPO determinarTipoApunte() {
		return TIPO_APUNTE.TASK_CONCILIACION_NORMA43_FILE_LOAD;

	}

} // TaskNorma43FileLoad
