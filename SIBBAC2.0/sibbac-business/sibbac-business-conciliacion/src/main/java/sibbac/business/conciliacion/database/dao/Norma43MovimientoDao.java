package sibbac.business.conciliacion.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.data.Norma43MovimientoData;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;

@Repository
public interface Norma43MovimientoDao extends JpaRepository<Norma43Movimiento, Long> {

  @Query("SELECT new sibbac.business.conciliacion.database.data.Norma43MovimientoData( m.tradedate, m.settlementdate, m.fichero.auditDate, m.fichero.cuenta, m.importe) "
         + "FROM Norma43Movimiento m WHERE m.fichero.id=(SELECT id FROM Norma43Fichero WHERE auditDate = (SELECT MAX(auditDate) FROM Norma43Fichero)) ORDER BY m.importe DESC ")
  public List<Norma43MovimientoData> findAllIntoData();

  @Query("SELECT new sibbac.business.conciliacion.database.data.Norma43MovimientoData( m.tradedate, m.settlementdate, m.fichero.auditDate, m.fichero.cuenta, m.importe) "
         + "FROM Norma43Movimiento m WHERE m.settlementdate >=:fcontratacionDe AND m.settlementdate <= :fcontratacionA AND m.fichero.id=(SELECT id FROM Norma43Fichero WHERE auditDate = (SELECT MAX(auditDate) FROM Norma43Fichero)) ORDER BY m.importe DESC ")
  public List<Norma43MovimientoData> findAllIntoDataContratacion(@Param("fcontratacionDe") Date fcontratacionDe,
                                                                 @Param("fcontratacionA") Date fcontratacionA);

  @Query("SELECT new sibbac.business.conciliacion.database.data.Norma43MovimientoData( m.tradedate, m.settlementdate, m.fichero.auditDate, m.fichero.cuenta, m.importe) "
         + "FROM Norma43Movimiento m WHERE m.tradedate >=:fliquidacionDe AND m.tradedate <= :fliquidacionA AND  m.fichero.id=(SELECT id FROM Norma43Fichero WHERE auditDate = (SELECT MAX(auditDate) FROM Norma43Fichero)) ORDER BY m.importe DESC ")
  public List<Norma43MovimientoData> findAllIntoDataLiquidacion(@Param("fliquidacionDe") Date fliquidacionDe,
                                                                @Param("fliquidacionA") Date fliquidacionA);

  @Query("SELECT new sibbac.business.conciliacion.database.data.Norma43MovimientoData( m.tradedate, m.settlementdate, m.fichero.auditDate, m.fichero.cuenta, m.importe) "
         + "FROM Norma43Movimiento m WHERE m.tradedate >=:fliquidacionDe AND m.tradedate <= :fliquidacionA AND m.settlementdate >=:fcontratacionDe AND m.settlementdate <= :fcontratacionA AND "
         + "m.fichero.id=(SELECT id FROM Norma43Fichero WHERE auditDate = (SELECT MAX(auditDate) FROM Norma43Fichero)) ORDER BY m.importe DESC ")
  public List<Norma43MovimientoData> findAllIntoDataFechas(@Param("fliquidacionDe") Date fliquidacionDe,
                                                           @Param("fliquidacionA") Date fliquidacionA,
                                                           @Param("fcontratacionDe") Date fcontratacionDe,
                                                           @Param("fcontratacionA") Date fcontratacionA);

  @Modifying
  @Query(value = "UPDATE tmct0_norma43_movimiento set procesado=:procesado where id=:id", nativeQuery = true)
  public int update(@Param("id") Long id, @Param("procesado") int procesado);

  @Query("SELECT mN43 FROM Norma43Movimiento mN43, Norma43Fichero fN43, CuentaBancaria cb, CuentaAuxiliar ca"
         + " WHERE mN43.procesado = :procesado AND mN43.fichero = fN43.id"
         + "       AND (fN43.entidad, fN43.oficina, fN43.cuenta) = (substr(cb.iban, 5, 4), substr(cb.iban, 9, 4), substr(cb.iban, 15, 10))"
         + "       AND cb.id = ca.cuenta AND ca.tipologia = :tipologia AND ca.tipoImporte = :tipoImporte" + " ORDER BY mN43.id ASC")
  public List<Norma43Movimiento> findByProcesadoAndTipologiaAndTipoImporteOrderByFechaInicial(@Param("procesado") Boolean procesado,
                                                                                              @Param("tipologia") Integer tipologia,
                                                                                              @Param("tipoImporte") Integer tipoImporte);

  @Query("SELECT mN43 FROM Norma43Movimiento mN43 WHERE mN43.descReferencia = :descReferencia AND mN43.procesado in (0,1,2)")
  public List<Norma43Movimiento> findByDescReferenciaAndProcesado(
		  @Param("descReferencia") String descReferencia);
  
  @Query("SELECT mN43 FROM Norma43Movimiento mN43 WHERE mN43.id = :id AND mN43.importe = :importe")
  public Norma43Movimiento findByIdAndImporte(
		  @Param("id") Long id, @Param("importe") BigDecimal importe);
  
  @Query("SELECT mN43 FROM Norma43Movimiento mN43 WHERE mN43.descReferencia = :descReferencia and mN43.procesado in (0,1,2)")
  public List<Norma43Movimiento> findByDescReferencia(
		  @Param("descReferencia") String descReferencia);

  @Query("SELECT mN43 FROM Norma43Movimiento mN43 WHERE concat(mN43.referencia, mN43.descReferencia) = :refTheir")
  public List<Norma43Movimiento> findByReferenciaDescReferenciaConcat(
		  @Param("refTheir") String refTheir);
  
  @Query("SELECT mN43 FROM Norma43Movimiento mN43 WHERE concat(mN43.referencia, mN43.descReferencia) = :refTheir AND mN43.procesado in (0,1,2)")
  public List<Norma43Movimiento> findByReferenciaDescReferenciaConcatAndProcesado(
		  @Param("refTheir") String refTheir);
  
} // Norma43MovimientoDao
