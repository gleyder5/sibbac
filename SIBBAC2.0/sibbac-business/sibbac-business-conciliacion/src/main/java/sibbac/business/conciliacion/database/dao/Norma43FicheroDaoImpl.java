package sibbac.business.conciliacion.database.dao;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.conciliacion.database.data.Norma43FicheroData;
import sibbac.business.conciliacion.database.data.Norma43MovimientoData;
import sibbac.business.wrappers.database.dao.Tmct0CuentaLiquidacionDao;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;


@Repository
public class Norma43FicheroDaoImpl {

	@PersistenceContext
	private EntityManager		em;
	@Autowired
	Tmct0CuentaLiquidacionDao	cuentaLiquidacionDao;

	private static final String	TAG	= Norma43FicheroDaoImpl.class.getName();
	private static final Logger	LOG	= LoggerFactory.getLogger( TAG );

	@SuppressWarnings( "unchecked" )
	public List< Norma43FicheroData > findAllFicheroNorma43( Map< String, Serializable > filters ) {
		List< Norma43FicheroData > resultList = new ArrayList< Norma43FicheroData >();
		StringBuilder select = new StringBuilder( "SELECT new sibbac.business.conciliacion.database.data.Norma43FicheroData(" ).append(
				"f) " ).append( "FROM Norma43Fichero f " );
		String defaultWhere = "WHERE f.auditDate = (SELECT MAX(auditDate) FROM Norma43Fichero) ";
		StringBuilder queryBuilder = new StringBuilder( select );
		boolean assignedWhere = false;
		if ( !assignedWhere ) {
			queryBuilder.append( defaultWhere );
		}
		Query query = null;
		try {
			query = em.createQuery( queryBuilder.toString() );
			resultList.addAll( query.getResultList() );
		} catch ( IllegalArgumentException ex ) {
			LOG.error( ex.getMessage(), ex );
		}
		if ( resultList.size() > 0 && filters != null ) {
			try {
				resultList = filterResult( resultList, filters );
			} catch ( NonUniqueResultException ex ) {
				LOG.error( ex.getMessage(), ex );
			}
		}
		return resultList;
	}

	/**
	 * Elimina de la lista los objetos que no coincidan con los criterios de
	 * cusquda otorgados por los filtros.
	 * 
	 * @param resultList
	 *            - List<Norma43FicheroData>
	 * @param filters
	 *            - Map<String, Serializable> filters
	 */
	private List< Norma43FicheroData > filterResult( List< Norma43FicheroData > resultList, Map< String, Serializable > filters )
			throws NonUniqueResultException {
		List< Norma43FicheroData > resultado = new LinkedList< Norma43FicheroData >();
		boolean filterCodCtaLiq = false;
		boolean filterBetweenFLiq = false;
		boolean filterByFliqDe = false;
		boolean filterByFliqA = false;
		Date fLiquidacionDe = null;
		Date fLiquidacionA = null;
		String cdCtaLiquidacion = "";
		for ( Map.Entry< String, Serializable > entry : filters.entrySet() ) {
			if ( !"".equals( entry.getKey() ) && entry.getValue() != null && !"".equals( entry.getValue().toString() ) ) {
				final String criteria = entry.getKey();
				switch ( criteria ) {
					case "cdcodigocuentaliq":
						filterCodCtaLiq = true;
						cdCtaLiquidacion = entry.getValue().toString();
						break;
					case "fliquidacionDe":
						fLiquidacionDe = ( Date ) entry.getValue();
						if ( filters.get( "fliquidacionA" ) != null ) {
							filterBetweenFLiq = true;
						} else {
							filterByFliqDe = true;
						}
						break;
					case "fliquidacionA":
						fLiquidacionA = ( Date ) entry.getValue();
						if ( filters.get( "fliquidacionDe" ) != null ) {
							filterBetweenFLiq = true;
						} else {
							filterByFliqA = true;
						}
						break;

				}
			}
		}
		String codCtaLiq = "";
		for ( Norma43FicheroData fich : resultList ) {
			if ( filterCodCtaLiq ) {
				if ( fich.getCuenta() != null ) {
					codCtaLiq = extractCodCtaLiquidacion( fich );
					if(cdCtaLiquidacion.indexOf("#") !=-1){  // CFF 23/02/2016.
						cdCtaLiquidacion = cdCtaLiquidacion.replace("#", "");
						if ( codCtaLiq.equals( cdCtaLiquidacion ) ) {
							// resultList.remove(fich);
							continue;
						}						
					}else{
						if ( !codCtaLiq.equals( cdCtaLiquidacion ) ) {
							// resultList.remove(fich);
							continue;
						}
					}
				}
			}
			List< Norma43MovimientoData > movimientos = fich.getMovimientos();
			List< Norma43MovimientoData > newMovs = new ArrayList< Norma43MovimientoData >();
			for ( Norma43MovimientoData mov : movimientos ) {
				if ( filterBetweenFLiq ) {
					if ( !( fLiquidacionDe.compareTo( mov.getSettlementdate() ) <= 0 && fLiquidacionA.compareTo( mov.getSettlementdate() ) >= 0 ) ) {
						// movimientos.remove(mov);
						continue;
					}
				} else if ( filterByFliqA ) {
					if ( !( fLiquidacionA.compareTo( mov.getSettlementdate() ) >= 0 ) ) {
						// movimientos.remove(mov);
						continue;
					}
				} else if ( filterByFliqDe ) {
					if ( !( fLiquidacionDe.compareTo( mov.getSettlementdate() ) <= 0 ) ) {
						// movimientos.remove(mov);
						continue;
					}
				}
				newMovs.add( mov );
			}
			fich.setMovimientos( newMovs );
			resultado.add( fich );
		}
		return resultado;
	}

	/*
	 * TODO CAMBIO RELACION CUENTA LIQUIDACION - CUENTA COMPESACION
	 * HECHO
	 */
	private String extractCodCtaLiquidacion( Norma43FicheroData fich ) {
		String codCtaLiq = "";
		List< Tmct0CuentasDeCompensacion > cuentasDeCompensacion = cuentaLiquidacionDao.findByCuentaNorma43( fich.getCuenta() );
		if ( cuentasDeCompensacion != null && cuentasDeCompensacion.size() > 0 ) {
			codCtaLiq = cuentasDeCompensacion.get( 0 ).getCdCodigo();
		}
		return codCtaLiq;
	}

}
