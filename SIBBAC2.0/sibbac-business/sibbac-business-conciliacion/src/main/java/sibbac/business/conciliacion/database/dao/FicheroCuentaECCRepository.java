package sibbac.business.conciliacion.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.conciliacion.database.data.FicheroCuentaECCData;
import sibbac.business.conciliacion.database.model.FicheroCuentaECC;

public interface FicheroCuentaECCRepository
    extends JpaRepository<FicheroCuentaECC, Long>, JpaSpecificationExecutor<FicheroCuentaECC> {
  
  @Query("SELECT new sibbac.business.conciliacion.database.data.FicheroCuentaECCData(m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo, SUM(m.titulos), SUM(m.efectivo)) "
      + "FROM FicheroCuentaECC m, Tmct0Valores v "
      // enlaces
      + "WHERE m.isin=v.codigoDeValor "
      // parametros
      + "AND m.settlementdate BETWEEN :fechadesde AND :fechahasta "
      + "AND m.cuentaLiquidacion.cdCodigo = :codigoCuentaLiquidacion " + "AND m.isin = :isin "
      + "GROUP BY m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo "
      + "ORDER BY m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo")
  public List<FicheroCuentaECCData> findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacionAndIsin(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion, @Param("isin") String isin);

  @Query("SELECT new sibbac.business.conciliacion.database.data.FicheroCuentaECCData(m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo, SUM(m.titulos), SUM(m.efectivo)) "
      + "FROM FicheroCuentaECC m, Tmct0Valores v "
      // enlaces
      + "WHERE m.isin=v.codigoDeValor "
      // parametros
      + "AND m.settlementdate BETWEEN :fechadesde AND :fechahasta "
      + "AND m.cuentaLiquidacion.cdCodigo = :codigoCuentaLiquidacion "
      + "GROUP BY m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo "
      + "ORDER BY m.mercado, m.settlementdate, m.isin, v.descripcion, m.cuentaLiquidacion.cdCodigo")
  public List<FicheroCuentaECCData> findMovimientosCuentaECCBySettlementdateAndCodigoCuentaLiquidacion(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);
  
  /**
   * Recupera un movimiento individual, debe ser único o nulo. En caso contrario
   * es correcto que se marque una incidencia.
   * @param isin codigo del valor
   * @param codigoCuentaLiquidacion cuenta de liquidación del movimiento
   * @param fechaLiquidacion fecha de la liquidacion
   * @param signoAnotacion signo de la anotacion
   * @return El movimiento, o nulo si no existe
   */
  @Query("SELECT m FROM FicheroCuentaECC m "
      + "WHERE m.isin = :isin "
      + "AND m.cuentaLiquidacion.id = :codigoCuentaLiquidacion "
      + "AND m.settlementdate = :fechaLiquidacion "
      + "AND m.signoAnotacion = :signoAnotacion")
  public List<FicheroCuentaECC> findFicheroCuentaECCFechaLiq(@Param("isin") String isin, 
      @Param("codigoCuentaLiquidacion") long codigoCuentaLiquidacion, 
      @Param("fechaLiquidacion") Date fechaLiquidacion,
      @Param("signoAnotacion") char signoAnotacion);
  
}
