package sibbac.business.conciliacion.database.bo;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.Tmct0SaldoTituloCuentaDAO;
import sibbac.business.conciliacion.database.model.Tmct0SaldoTituloCuenta;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0SaldoTituloCuentaBO extends AbstractBo< Tmct0SaldoTituloCuenta, Long, Tmct0SaldoTituloCuentaDAO > {

	/**
	 * 
	 * @param cuentaCompensacion
	 * @param tradedate
	 * @param isin
	 * @return
	 */
	public List< Tmct0SaldoTituloCuenta > findByCuentaCompensacionAndTradedateAndIsin( Tmct0CuentasDeCompensacion cuentaCompensacion,
			Date tradedate, String isin ) {
		List< Tmct0SaldoTituloCuenta > resultList = new LinkedList< Tmct0SaldoTituloCuenta >();
		if ( cuentaCompensacion != null && tradedate != null && isin != null && !"".equals( isin ) ) {
			resultList = dao.findByCuentaCompensacionAndTradedateAndIsin( cuentaCompensacion, tradedate, isin );
		}
		return resultList;
	}

	/**
	 * 
	 * @param cuentaCompensacion
	 * @param tradedate
	 * @param isin
	 * @param idMovimientoecc
	 * @return
	 */
	public Tmct0SaldoTituloCuenta findByCuentaCompensacionAndTradedateAndIsinAndIdMovimientoecc(
			Tmct0CuentasDeCompensacion cuentaCompensacion, Date tradedate, String isin, Tmct0movimientoecc idMovimientoecc ) {
		Tmct0SaldoTituloCuenta result = null;
		if ( cuentaCompensacion != null && tradedate != null && isin != null && !"".equals( isin ) && idMovimientoecc != null ) {
			result = dao.findByCuentaCompensacionAndTradedateAndIsinAndIdMovimientoecc( cuentaCompensacion, tradedate, isin,
					idMovimientoecc );
		}
		return result;
	}

	public List< Object[] > findAllGroupByIsinAndTradedateAndSentidoAndCuentaCompensacion() {
		return dao.findAllGroupByIsinAndTradedateAndSentidoAndCuentaCompensacion();
	}

	public List< Object[] > findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion() {
		return dao.findAllGroupByIsinAndTradedateAndSettlementdateAndSentidoAndCuentaCompensacion();
	}

	/**
	 * Recupera las entradas de titulos agrupados por isin, cuenta de compensacion y sentido
	 * filtrado por conciliacion
	 * 
	 * @param boolean conciliado
	 * @return List<Object[]>
	 */
	public List< Object[] > findAllGroupByIsinAndAndSentidoAndCuentaCompensacionAndConciliado( boolean conciliado ) {
		return dao.findAllGroupByIsinAndAndSentidoAndCuentaCompensacionAndConciliado( conciliado );
	}

}
