package sibbac.business.conciliacion.database.bo;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.conciliacion.database.dao.MovimientoCuentaVirtualDAOImpl;
import sibbac.business.conciliacion.database.dao.MovimientoCuentaVirtualDao;
import sibbac.business.conciliacion.database.data.MovimientoCuentaVirtualData;
import sibbac.business.conciliacion.database.dto.IsinDTO;
import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtual;
import sibbac.database.bo.AbstractBo;


@Service
public class MovimientoCuentaVirtualBo extends AbstractBo< MovimientoCuentaVirtual, Long, MovimientoCuentaVirtualDao > {

	@Autowired
	private MovimientoCuentaVirtualDAOImpl	daoImpl;

	/**
	 * 
	 * @return
	 */
	public List< MovimientoCuentaVirtual > getMovimientosCuentaVirtual() {
		return dao.getMovimientosCuentaVirtual();
	}

	/**
	 * suma todos los efectivos de un cliente dado su id
	 * 
	 * @param idCliente
	 * @param string
	 * @return
	 */
	public BigDecimal sumarEfectivosCliente( String alias ) {
		return dao.sumarEfectivosCliente( alias );
	}

	/**
	 * Busca movimientos y los ordena por id de cliente
	 * 
	 * @return
	 */
	public List< Object[] > findMovimientosOrderByClient() {
		return dao.findMovimientosOrderByClient();
	}

	/**
	 * 
	 * @return
	 */
	public List< Object[] > findMovimientosGrupoByIsinAndIdClienteOrderByClient() {
		return dao.findMovimientosGrupoByIsinAndIdClienteOrderByClient();
	}

	/**
	 * 
	 * @param filtros
	 * @return
	 */
	public List< MovimientoCuentaVirtualData > findMovimientosGrupoByIsinAndIdClienteOrderByClient( Map< String, Serializable > filtros,
			String customCriteria ) {
		return daoImpl.findMovimientosGrupoByIsinAndIdClienteOrderByClient( filtros, customCriteria );
	}

	/**
	 * Obtiene los movimientos de cuentas clearing (o no dependiendo del parametro isClearing) agrupados por cliente.
	 * 
	 * @param filtros
	 * @param isClearing
	 * @return
	 */
	public List< MovimientoCuentaVirtualData > findMovimientosGrupoByIsinAndIdClienteOrderByClient( Map< String, Serializable > filtros,
			boolean isClearing, String customCriteria ) {
		return daoImpl.findMovimientosGrupoByIsinAndIdClienteOrderByClient( filtros, isClearing, customCriteria );
	}

	/**
	 * Obtiene los movimientos realizados por el cliente en la cuenta clearing
	 * 
	 * @param filtros
	 * @return
	 */
	public List< MovimientoCuentaVirtualData > findMovimientosCuentaClearingGrupoByIsinAndIdClienteOrderByClient(
			Map< String, Serializable > filtros, String customCriteria ) {
		return daoImpl.findMovimientosGrupoByIsinAndIdClienteOrderByClient( filtros, true, customCriteria );
	}

	public List< MovimientoCuentaVirtualData > findByIdCuentaGroupByIsinAndSentidoAndFContratacion( long idCuentaComp,
			Map< String, Serializable > filtros ) {
		return daoImpl.findByIdCuentaGroupByIsinAndSentidoAndFContratacion( idCuentaComp, filtros );
	}

	public List< Object[] > findByIdCuentaClearingGroupByIsinAndSentidoAndFContratacion( long[] idCuentasCompClearing,
			Map< String, Serializable > filtros ) {
		List< Object[] > resutl = daoImpl.findByIdCuentaClearingGroupByIsinAndSentidoAndFContratacion( idCuentasCompClearing, filtros );
		return resutl;
	}

	/***
	 * Funcion que devuelve los MovimientosCuentaVirtual de 'M'ercado que coinciden con:
	 * ISIN; FechaContratacion, Sentido, Cuenta, Alias, Camara y Precio
	 */
	public MovimientoCuentaVirtual
			findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamaraParaMercado( String isin, java.sql.Date tradeDate,
					java.sql.Date settlementDate, Character sentido, String cdCodigoCtaComp, String alias, String cdcamara ) {
		return dao.findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamara( isin, tradeDate, settlementDate, sentido,
				cdCodigoCtaComp, alias, cdcamara, 'M' );
	}

	public MovimientoCuentaVirtual
			findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamaraParaCliente( String isin, java.sql.Date tradeDate,
					java.sql.Date settlementDate, Character sentido, String cdCodigoCtaComp, String alias, String cdcamara ) {
		return dao.findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamara( isin, tradeDate, settlementDate, sentido,
				cdCodigoCtaComp, alias, cdcamara, 'C' );
	}

	public List< MovimientoCuentaVirtual > getMovimientosCuentaVirtualByCuentaLiquidacion( long idCuentaLiq ) {
		return dao.getMovimientosCuentaVirtualByCuentaLiquidacion( idCuentaLiq );
	}

	public List< MovimientoCuentaVirtualData > findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion( long idCuentaLiq,
			Map< String, Serializable > filtros ) {
		List< MovimientoCuentaVirtualData > resutl = daoImpl.findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacion( idCuentaLiq,
				filtros );
		return resutl;
	}

	public List< MovimientoCuentaVirtualData > findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacionSinAlias( long idCuentaLiq,
			Map< String, Serializable > filtros ) {
		List< MovimientoCuentaVirtualData > resutl = daoImpl.findByIdCuentaLiquidacionGroupByIsinAndSentidoAndFLiquidacionSinAlias(
				idCuentaLiq, filtros );
		return resutl;
	}

	public MovimientoCuentaVirtual
			findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaLiqAndSentidoCVAndCamara( String isin, java.sql.Date tradeDate,
					java.sql.Date settlementDate, Character sentido, String cdCodigoCtaLiq, char sentidoCV, String cdcamara ) {
		return dao.findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaLiqAndSentidoCVAndCamara( isin, tradeDate, settlementDate,
				sentido, cdCodigoCtaLiq, sentidoCV, cdcamara );
	}

	public List< MovimientoCuentaVirtual > findAllByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamaraParaMercado(
			String isin, java.sql.Date tradeDate, java.sql.Date settlementDate, Character sentido, String cdCodigoCtaComp, String alias,
			String cdcamara ) {
		return dao.findAllByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamara( isin, tradeDate, settlementDate,
				sentido, cdCodigoCtaComp, alias, cdcamara, 'M' );
	}

	public MovimientoCuentaVirtual
			findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamaraAndIdLiquidacionParcialParaCliente( String isin,
					java.sql.Date tradeDate, java.sql.Date settlementDate, Character sentido, String cdCodigoCtaComp, String alias,
					String cdcamara ) {
		return dao.findByIsinAndTradeDateAndSettlementDateAndSentidoAndCuentaAndAliasAndCamaraAndIdLiquidacionParcial( isin, tradeDate,
				settlementDate, sentido, cdCodigoCtaComp, alias, cdcamara, 'C', 'P' );
	}

	public List< MovimientoCuentaVirtualData > findByIsinAndSettlementdateAndIdCuenta( String isin, Date settlementdateTo,
			Date settlementdateFrom, Long idcuentaliq ) {
		return daoImpl.findByIsinAndSettlementdateAndIdCuenta( isin, settlementdateTo, settlementdateFrom, idcuentaliq );
	}

	public List< IsinDTO > getIsin() {
		return daoImpl.getIsin();
	}
}
