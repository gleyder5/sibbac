package sibbac.business.conciliacion.database.filter;

import java.util.Date;

public interface FicheroCuentaECCFilter {
  
  Date getFechaDesde();
  
  Date getFechaHasta();
  
  String getCodigoCuentaLiquidacion();
  
  String getIsin();
  
  boolean isCodigoCuentaLiquidacionEq();
  
  char getSignoAnotacion();

}
