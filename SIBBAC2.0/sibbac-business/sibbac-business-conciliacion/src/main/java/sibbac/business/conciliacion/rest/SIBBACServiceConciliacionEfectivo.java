package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.conciliacion.database.bo.FicheroCuentaECCBo;
import sibbac.business.conciliacion.database.data.FicheroCuentaECCData;
import sibbac.business.conciliacion.database.filter.FicheroCuentaECCFilter;
import sibbac.business.conciliacion.rest.filter.FicheroCuentaECCFilterImpl;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceConciliacionEfectivo implements SIBBACServiceBean {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceConciliacionEfectivo.class);

  /**
   * Acción Consulta
   */
  private static final String CMD_GET_OPERACIONES_EFECTIVO = "getOperacionesEfectivo";

  private static final String sortBy = "m.imefectivo DESC";

  /**
   *  Util
   */
  private ConciliacionUtil util = ConciliacionUtil.getInstance();

  @Autowired
  private Tmct0MovimientoCuentaAliasBo movCtaAliasBo;
  
  @Autowired
  private FicheroCuentaECCBo ficheroCuentaECCBo;
  
  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_GET_OPERACIONES_EFECTIVO);
    return commands;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    String prefix = "[SIBBACSIBBACServiceConciliacionEfectivo ::process] ";
    LOG.debug(prefix + "Processing a web request...");
    WebResponse webResponse = new WebResponse();
    Map<String, Object> response = new HashMap<String, Object>();
    //
    Map<String, String> params = new HashMap<String, String>();
    if (webRequest.getFilters() != null) {
      params = webRequest.getFilters();
    }
    Map<String, Serializable> filtro = new HashMap<String, Serializable>();
    String command = webRequest.getAction();
    LOG.debug(prefix + "+ Command.: [{}]", command);
    switch (command) {
      case CMD_GET_OPERACIONES_EFECTIVO:
        if (params != null && params.size() > 0) {
          filtro = util.normalizeFilters(params);
        }
        response.put("derecha", getFicheroCuentaECC(filtro));
        response.put("izquierda", getMovimientosCuentaAlias(filtro));
        webResponse.setResultados(response);
        break;
    }
    return webResponse;
  }
  
  private List<FicheroCuentaECCData> getFicheroCuentaECC(Map<String, Serializable> filtros) throws SIBBACBusinessException {
    final FicheroCuentaECCFilter filter;
    
    filter = new FicheroCuentaECCFilterImpl(filtros);
    return ficheroCuentaECCBo.findByFilter(filter);
  }

  private List<MovimientoCuentaAliasData> getMovimientosCuentaAlias(Map<String, Serializable> filtros) {
    final List<MovimientoCuentaAliasData> movimientos;
    final List<MovimientoCuentaAliasData> movimientosNoZero;

    movimientosNoZero = new LinkedList<MovimientoCuentaAliasData>();
    movimientos = movCtaAliasBo.findAll(filtros, sortBy, null);
    for (MovimientoCuentaAliasData mov : movimientos) {
      if (mov.getEfectivo().compareTo(BigDecimal.ZERO) != 0) {
        movimientosNoZero.add(mov);
      }
    }
    return movimientosNoZero;
  }
  

}
