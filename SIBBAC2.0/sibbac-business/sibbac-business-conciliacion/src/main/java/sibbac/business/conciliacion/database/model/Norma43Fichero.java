package sibbac.business.conciliacion.database.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 *
 * @author XI316153
 */
@Entity
@Table(name = DBConstants.CONCILIACION.TMCT0_NORMA43_FICHERO)
@XmlRootElement
public class Norma43Fichero extends ATable<Norma43Fichero> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -474903617082529622L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @XmlAttribute
  @Column(name = "ENTIDAD", nullable = false, length = 4)
  private Integer entidad;

  @XmlAttribute
  @Column(name = "OFICINA", nullable = false, length = 4)
  private Integer oficina;

  @XmlAttribute
  @Column(name = "CUENTA", nullable = false, length = 10)
  private Long cuenta;

  @XmlAttribute
  @Column(name = "DEBE", nullable = false, length = 1)
  private String debe;

  @XmlAttribute
  @Column(name = "SALDO_INICIAL", nullable = false, length = 14)
  private Long saldoinicial;

  @XmlAttribute
  @Column(name = "DIVISA", nullable = false, length = 3)
  private Integer divisa;

  @XmlAttribute
  @Column(name = "SALDO_FINAL", nullable = false, length = 14)
  private Long saldofinal;

  @XmlAttribute
  @Column(name = "LIBRE", nullable = true, length = 4)
  private String libre;

  @XmlAttribute
  @Column(name = "MODALIDAD_INFO", nullable = true, length = 1)
  private Integer modaInfo;

  @XmlAttribute
  @Column(name = "NOMBRE_ABREVIADO", nullable = true, length = 26)
  private String nombreAbrev;

  @XmlAttribute
  @Column(name = "FECHA_INICIAL", nullable = true)
  private Date fechaInicial;

  @XmlAttribute
  @Column(name = "FECHA_FINAL", nullable = true)
  private Date fechaFinal;

  @XmlAttribute
  @Column(name = "APUNTES_DEBE", nullable = true, length = 14)
  private Integer apuntesDebe;

  @XmlAttribute
  @Column(name = "IMPORTES_DEBE", nullable = true, length = 14)
  private Integer importesDebe;

  @XmlAttribute
  @Column(name = "APUNTES_HABER", nullable = true, length = 5)
  private Integer apuntesHaber;

  @XmlAttribute
  @Column(name = "IMPORTES_HABER", nullable = true, length = 14)
  private Integer importesHaber;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fichero", fetch = FetchType.LAZY)
  private List<Norma43Movimiento> norma43Movimiento;

  @XmlAttribute
  @Column(name = "PROCESADO", nullable = false)
  private Boolean procesado;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public Norma43Fichero() {
    procesado = false;
  } // Norma43Fichero

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Norma43Fichero [entidad=" + entidad + ", oficina=" + oficina + ", cuenta=" + cuenta + ", debe=" + debe + ", saldoinicial="
        + saldoinicial + ", divisa=" + divisa + ", saldofinal=" + saldofinal + ", libre=" + libre + ", modaInfo=" + modaInfo
        + ", nombreAbrev=" + nombreAbrev + ", fechaInicial=" + fechaInicial + ", fechaFinal=" + fechaFinal + ", apuntesDebe=" + apuntesDebe
        + ", importesDebe=" + importesDebe + ", apuntesHaber=" + apuntesHaber + ", importesHaber=" + importesHaber + ", procesado="
        + procesado + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the entidad
   */
  public Integer getEntidad() {
    return entidad;
  }

  /**
   * @return the oficina
   */
  public Integer getOficina() {
    return oficina;
  }

  /**
   * @return the cuenta
   */
  public Long getCuenta() {
    return cuenta;
  }

  /**
   * @return the debe
   */
  public String getDebe() {
    return debe;
  }

  /**
   * @return the saldoinicial
   */
  public Long getSaldoinicial() {
    return saldoinicial;
  }

  /**
   * @return the divisa
   */
  public Integer getDivisa() {
    return divisa;
  }

  /**
   * @return the saldofinal
   */
  public Long getSaldofinal() {
    return saldofinal;
  }

  /**
   * @return the libre
   */
  public String getLibre() {
    return libre;
  }

  /**
   * @return the modaInfo
   */
  public Integer getModaInfo() {
    return modaInfo;
  }

  /**
   * @return the nombreAbrev
   */
  public String getNombreAbrev() {
    return nombreAbrev;
  }

  /**
   * @return the fechaInicial
   */
  public Date getFechaInicial() {
    return fechaInicial;
  }

  /**
   * @return the fechaFinal
   */
  public Date getFechaFinal() {
    return fechaFinal;
  }

  /**
   * @return the apuntesDebe
   */
  public Integer getApuntesDebe() {
    return apuntesDebe;
  }

  /**
   * @return the importesDebe
   */
  public Integer getImportesDebe() {
    return importesDebe;
  }

  /**
   * @return the apuntesHaber
   */
  public Integer getApuntesHaber() {
    return apuntesHaber;
  }

  /**
   * @return the importesHaber
   */
  public Integer getImportesHaber() {
    return importesHaber;
  }

  /**
   * @return the norma43Movimiento
   */
  public List<Norma43Movimiento> getMovimientoNorma43() {
    return norma43Movimiento;
  }

  /**
   * @return the procesado
   */
  public Boolean getProcesado() {
    return procesado;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param entidad the entidad to set
   */
  public void setEntidad(Integer entidad) {
    this.entidad = entidad;
  }

  /**
   * @param oficina the oficina to set
   */
  public void setOficina(Integer oficina) {
    this.oficina = oficina;
  }

  /**
   * @param cuenta the cuenta to set
   */
  public void setCuenta(Long cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * @param debe the debe to set
   */
  public void setDebe(String debe) {
    this.debe = debe;
  }

  /**
   * @param saldoinicial the saldoinicial to set
   */
  public void setSaldoinicial(Long saldoinicial) {
    this.saldoinicial = saldoinicial;
  }

  /**
   * @param divisa the divisa to set
   */
  public void setDivisa(Integer divisa) {
    this.divisa = divisa;
  }

  /**
   * @param saldofinal the saldofinal to set
   */
  public void setSaldofinal(Long saldofinal) {
    this.saldofinal = saldofinal;
  }

  /**
   * @param libre the libre to set
   */
  public void setLibre(String libre) {
    this.libre = libre;
  }

  /**
   * @param modaInfo the modaInfo to set
   */
  public void setModaInfo(Integer modaInfo) {
    this.modaInfo = modaInfo;
  }

  /**
   * @param nombreAbrev the nombreAbrev to set
   */
  public void setNombreAbrev(String nombreAbrev) {
    this.nombreAbrev = nombreAbrev;
  }

  /**
   * @param fechaInicial the fechaInicial to set
   */
  public void setFechaInicial(Date fechaInicial) {
    this.fechaInicial = fechaInicial;
  }

  /**
   * @param fechaFinal the fechaFinal to set
   */
  public void setFechaFinal(Date fechaFinal) {
    this.fechaFinal = fechaFinal;
  }

  /**
   * @param apuntesDebe the apuntesDebe to set
   */
  public void setApuntesDebe(Integer apuntesDebe) {
    this.apuntesDebe = apuntesDebe;
  }

  /**
   * @param importesDebe the importesDebe to set
   */
  public void setImportesDebe(Integer importesDebe) {
    this.importesDebe = importesDebe;
  }

  /**
   * @param apuntesHaber the apuntesHaber to set
   */
  public void setApuntesHaber(Integer apuntesHaber) {
    this.apuntesHaber = apuntesHaber;
  }

  /**
   * @param importesHaber the importesHaber to set
   */
  public void setImportesHaber(Integer importesHaber) {
    this.importesHaber = importesHaber;
  }

  /**
   * @param norma43Movimiento the norma43Movimiento to set
   */
  public void setMovimientoNorma43(List<Norma43Movimiento> norma43Movimiento) {
    this.norma43Movimiento = norma43Movimiento;
  }

  /**
   * @param procesado the procesado to set
   */
  public void setProcesado(Boolean procesado) {
    this.procesado = procesado;
  }

} // Norma43Fichero
