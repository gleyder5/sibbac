package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class GrupoBalanceCliente {

	private String		isin;
	private BigDecimal	titulos;
	private BigDecimal	efectivo;
	private String		descrisin;
	private String		fliquidacion;
	private boolean		valido;

	//
	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getDescrisin() {
		return descrisin;
	}

	public void setDescrisin( String descrisin ) {
		this.descrisin = descrisin;
	}

	public String getFliquidacion() {
		return fliquidacion;
	}

	public void setFliquidacion( String fliquidacion ) {
		this.fliquidacion = fliquidacion;
	}

	public boolean getValido() {
		return valido;
	}

	public void setValido( boolean valido ) {
		this.valido = valido;
	}

}
