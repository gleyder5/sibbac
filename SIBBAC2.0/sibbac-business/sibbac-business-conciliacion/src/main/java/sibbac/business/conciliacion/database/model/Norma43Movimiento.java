package sibbac.business.conciliacion.database.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 *
 * @author XI316153
 */
@Entity
@Table(name = DBConstants.CONCILIACION.TMCT0_NORMA43_MOVIMIENTO)
@XmlRootElement
public class Norma43Movimiento extends ATable<Norma43Movimiento> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -474903617082529622L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @XmlAttribute
  @Column(name = "TRADEDATE", nullable = false)
  private Date tradedate;

  @XmlAttribute
  @Column(name = "SETTLEMENTDATE", nullable = false)
  private Date settlementdate;

  @XmlAttribute
  @Column(name = "DEBE", nullable = false, length = 1)
  private String debe;

  @XmlAttribute
  @Column(name = "IMPORTE", nullable = false, precision = 14)
  private BigDecimal importe;

  @XmlAttribute
  @Column(name = "DOCUMENTO", nullable = false, precision = 10)
  private BigDecimal documento;

  @XmlAttribute
  @Column(name = "LBRE", nullable = true, length = 4)
  private String libre;

  @XmlAttribute
  @Column(name = "CONCEPTO_COMUN", nullable = true, length = 2)
  private Integer conceptoComun;

  @XmlAttribute
  @Column(name = "CONCEPTO_PROPIO", nullable = true, length = 3)
  private Integer conceptoPropio;

  @XmlAttribute
  @Column(name = "REFERENCIA", nullable = true, length = 12)
  private String referencia;

  @XmlAttribute
  @Column(name = "DESC_REFERENCIA", nullable = true, length = 16)
  private String descReferencia;

  @XmlAttribute
  @Column(name = "PROCESADO", nullable = false)
  private int procesado;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  protected Norma43Fichero fichero;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "movimiento", fetch = FetchType.LAZY)
  protected List<Norma43Concepto> conceptos;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public Norma43Movimiento() {
  } // Norma43Movimiento

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Norma43Movimiento [tradedate=" + tradedate + ", settlementdate=" + settlementdate + ", debe=" + debe
           + ", importe=" + importe + ", documento=" + documento + ", libre=" + libre + ", conceptoComun="
           + conceptoComun + ", conceptoPropio=" + conceptoPropio + ", referencia=" + referencia + ", descReferencia="
           + descReferencia + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLIICOS

  // public void addConcepto(Norma43Concepto concepto) {
  // if (this.conceptos == null) {
  // this.conceptos = new ArrayList<Norma43Concepto>();
  // }
  // this.conceptos.add(concepto);
  // }
  //
  // public boolean hasConceptos() {
  // return (this.conceptos != null && !this.conceptos.isEmpty());
  // }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  public Date getTradedate() {
    return tradedate;
  }

  public Date getSettlementdate() {
    return settlementdate;
  }

  public String getDebe() {
    return debe;
  }

  public BigDecimal getImporte() {
    return importe;
  }

  public BigDecimal getDocumento() {
    return documento;
  }

  public Norma43Fichero getFichero() {
    return fichero;
  }

  public List<Norma43Concepto> getConceptos() {
    return conceptos;
  }

  public String getLibre() {
    return libre;
  }

  public Integer getConceptoComun() {
    return conceptoComun;
  }

  public Integer getConceptoPropio() {
    return conceptoPropio;
  }

  public String getReferencia() {
    return referencia;
  }

  public String getDescReferencia() {
    return descReferencia;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  public void setTradedate(Date tradedate) {
    this.tradedate = tradedate;
  }

  public void setSettlementdate(Date settlementdate) {
    this.settlementdate = settlementdate;
  }

  public void setDebe(String debe) {
    this.debe = debe;
  }

  public void setImporte(BigDecimal importe) {
    this.importe = importe;
  }

  public void setDocumento(BigDecimal documento) {
    this.documento = documento;
  }

  public void setFichero(Norma43Fichero fichero) {
    this.fichero = fichero;
  }

  public void setConceptos(List<Norma43Concepto> conceptos) {
    this.conceptos = conceptos;
  }

  public void setLibre(String libre) {
    this.libre = libre;
  }

  public void setConceptoComun(Integer conceptoComun) {
    this.conceptoComun = conceptoComun;
  }

  public void setConceptoPropio(Integer conceptoPropio) {
    this.conceptoPropio = conceptoPropio;
  }

  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }

  public void setDescReferencia(String descReferencia) {
    this.descReferencia = descReferencia;
  }

  public boolean isProcesado() {
    if (procesado > 0) {
      return true;
    } else {
      return false;
    }
  }

  public void setProcesado(Integer procesado) {
    if(procesado == null) {
      this.procesado = 0;
    }
    this.procesado = procesado;
  }
  
  public Integer getProcesado() {
    return procesado;
  }

}
