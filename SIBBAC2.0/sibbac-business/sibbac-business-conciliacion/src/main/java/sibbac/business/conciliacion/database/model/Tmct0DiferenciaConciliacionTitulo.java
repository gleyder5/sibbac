package sibbac.business.conciliacion.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_DIFERENCIA_CONCILIACION_TITULO/*
																			 * , indexes={@Index(columnList=
																			 * "TRADE_DATE, ISIN, ID_CUENTA_COMPENSACION, SENTIDO"),
																			 * 
																			 * @Index(columnList=
																			 * "TRADE_DATE, ISIN, ID_CUENTA_COMPENSACION, SENTIDO")}
																			 */)
@XmlRootElement
public class Tmct0DiferenciaConciliacionTitulo extends ATable< Tmct0DiferenciaConciliacionTitulo > implements Serializable {

	/**
	 * 
	 */
	private static final long				serialVersionUID	= 1L;
	@Column( name = "TRADE_DATE" )
	private Date							tradeDate;
	@ManyToOne( fetch = FetchType.EAGER )
	@JoinColumn( name = "ID_CUENTA_COMPENSACION", referencedColumnName = "ID_CUENTA_COMPENSACION" )
	protected Tmct0CuentasDeCompensacion	tmct0CuentasDeCompensacion;
	@Column( name = "ISIN" )
	private String							isin;
	@Column( name = "TSV" )
	private BigDecimal						TSV;						// SALDO CUENTA PROPIA ANOTACIONECC
	@Column( name = "TS3" )
	private BigDecimal						TS3;						// SALDO CUENTA TITULO
	@Column( name = "SENTIDO", length = 1, nullable = false )
	private String							sentido;
	@Column( name = "FECHA_LIQUIDACION" )
	private Date							fechaLiquidacion;

	public Date getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate( Date tradeDate ) {
		this.tradeDate = tradeDate;
	}

	public Tmct0CuentasDeCompensacion getTmct0CuentasDeCompensacion() {
		return tmct0CuentasDeCompensacion;
	}

	public void setTmct0CuentasDeCompensacion( Tmct0CuentasDeCompensacion cuenta ) {
		this.tmct0CuentasDeCompensacion = cuenta;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getTSV() {
		return TSV;
	}

	public void setTSV( BigDecimal tSV ) {
		TSV = tSV;
	}

	public BigDecimal getTS3() {
		return TS3;
	}

	public void setTS3( BigDecimal tS3 ) {
		TS3 = tS3;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido( String sentido ) {
		this.sentido = sentido;
	}

	public Date getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	public void setFechaLiquidacion( Date fechaLiquidacion ) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

}
