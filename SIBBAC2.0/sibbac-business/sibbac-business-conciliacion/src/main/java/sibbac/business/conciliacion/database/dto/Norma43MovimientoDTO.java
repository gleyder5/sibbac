package sibbac.business.conciliacion.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class Norma43MovimientoDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -5660962170505688058L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  // Campos de un registros movimiento Norma43
  private Long id;
  private String descReferencia;
  private String referencia;
  private String tradedate;
  private String settlementdate;
  private String debe;
  private BigDecimal importe;
  private BigDecimal documento;

  // Campos de un registro fichero Norma43
  private Long idFichero;
  private Integer entidad;
  private Integer oficina;
  private Long cuenta;
  private Long saldoinicial;
  private Integer divisa;
  private Long saldofinal;
  private String debeFich;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * Constructor I. Constructor por defecto.
   */
  public Norma43MovimientoDTO() {
  } // Norma43MovimientoDTO

  /**
   * Constructor II. Crea un nuevo objeto de la clase con los datos del array especificado.
   * 
   * @param movimientoNorma43 Datos del nuevo movimiento a crear.
   */
  public Norma43MovimientoDTO(Object[] movimientoNorma43Arg) {
	Object[] movimientoNorma43 = Arrays.copyOf(movimientoNorma43Arg, movimientoNorma43Arg.length);
	this.descReferencia = String.valueOf(movimientoNorma43[0]);
	this.idFichero = Long.valueOf(movimientoNorma43[1].toString());
	this.tradedate = new SimpleDateFormat("yyyy-MM-dd").format(movimientoNorma43[2]);
	this.importe = new BigDecimal(movimientoNorma43[3].toString());
	this.id = Long.valueOf(movimientoNorma43[4].toString());
	this.entidad = Integer.valueOf(movimientoNorma43[5].toString());
	this.oficina = Integer.valueOf(movimientoNorma43[6].toString());
	this.cuenta = Long.valueOf(movimientoNorma43[7].toString());
	this.referencia = String.valueOf(movimientoNorma43[8]);
	this.debe = String.valueOf(movimientoNorma43[9]);
	this.settlementdate = new SimpleDateFormat("yyyy-MM-dd").format(movimientoNorma43[10]);
  } // Norma43MovimientoDTO

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Norma43MovimientoDTO [id=" + id + ", descReferencia=" + descReferencia + ", referencia=" + referencia + ", tradedate=" + tradedate
           + ", settlementdate=" + settlementdate + ", debe=" + debe + ", importe=" + importe + ", documento=" + documento + ", idFichero="
           + idFichero + ", entidad=" + entidad + ", oficina=" + oficina + ", cuenta=" + cuenta + ", saldoinicial=" + saldoinicial + ", divisa="
           + divisa + ", saldofinal=" + saldofinal + ", debeFich=" + debeFich + "]";
  } // toString

  /*
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cuenta == null) ? 0 : cuenta.hashCode());
    result = prime * result + ((debe == null) ? 0 : debe.hashCode());
    result = prime * result + ((debeFich == null) ? 0 : debeFich.hashCode());
    result = prime * result + ((descReferencia == null) ? 0 : descReferencia.hashCode());
    result = prime * result + ((divisa == null) ? 0 : divisa.hashCode());
    result = prime * result + ((documento == null) ? 0 : documento.hashCode());
    result = prime * result + ((entidad == null) ? 0 : entidad.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((idFichero == null) ? 0 : idFichero.hashCode());
    result = prime * result + ((importe == null) ? 0 : importe.hashCode());
    result = prime * result + ((oficina == null) ? 0 : oficina.hashCode());
    result = prime * result + ((referencia == null) ? 0 : referencia.hashCode());
    result = prime * result + ((saldofinal == null) ? 0 : saldofinal.hashCode());
    result = prime * result + ((saldoinicial == null) ? 0 : saldoinicial.hashCode());
    result = prime * result + ((settlementdate == null) ? 0 : settlementdate.hashCode());
    result = prime * result + ((tradedate == null) ? 0 : tradedate.hashCode());
    return result;
  } // hashCode

  /*
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Norma43MovimientoDTO other = (Norma43MovimientoDTO) obj;
    if (cuenta == null) {
      if (other.cuenta != null)
        return false;
    } else if (!cuenta.equals(other.cuenta))
      return false;
    if (debe == null) {
      if (other.debe != null)
        return false;
    } else if (!debe.equals(other.debe))
      return false;
    if (debeFich == null) {
      if (other.debeFich != null)
        return false;
    } else if (!debeFich.equals(other.debeFich))
      return false;
    if (descReferencia == null) {
      if (other.descReferencia != null)
        return false;
    } else if (!descReferencia.equals(other.descReferencia))
      return false;
    if (divisa == null) {
      if (other.divisa != null)
        return false;
    } else if (!divisa.equals(other.divisa))
      return false;
    if (documento == null) {
      if (other.documento != null)
        return false;
    } else if (!documento.equals(other.documento))
      return false;
    if (entidad == null) {
      if (other.entidad != null)
        return false;
    } else if (!entidad.equals(other.entidad))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (idFichero == null) {
      if (other.idFichero != null)
        return false;
    } else if (!idFichero.equals(other.idFichero))
      return false;
    if (importe == null) {
      if (other.importe != null)
        return false;
    } else if (!importe.equals(other.importe))
      return false;
    if (oficina == null) {
      if (other.oficina != null)
        return false;
    } else if (!oficina.equals(other.oficina))
      return false;
    if (referencia == null) {
      if (other.referencia != null)
        return false;
    } else if (!referencia.equals(other.referencia))
      return false;
    if (saldofinal == null) {
      if (other.saldofinal != null)
        return false;
    } else if (!saldofinal.equals(other.saldofinal))
      return false;
    if (saldoinicial == null) {
      if (other.saldoinicial != null)
        return false;
    } else if (!saldoinicial.equals(other.saldoinicial))
      return false;
    if (settlementdate == null) {
      if (other.settlementdate != null)
        return false;
    } else if (!settlementdate.equals(other.settlementdate))
      return false;
    if (tradedate == null) {
      if (other.tradedate != null)
        return false;
    } else if (!tradedate.equals(other.tradedate))
      return false;
    return true;
  } // equals

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the descReferencia
   */
  public String getDescReferencia() {
    return descReferencia;
  }

  /**
   * @return the referencia
   */
  public String getReferencia() {
    return referencia;
  }

  /**
   * @return the tradedate
   */
  public String getTradedate() {
    return tradedate;
  }

  /**
   * @return the settlementdate
   */
  public String getSettlementdate() {
    return settlementdate;
  }

  /**
   * @return the debe
   */
  public String getDebe() {
    return debe;
  }

  /**
   * @return the importe
   */
  public BigDecimal getImporte() {
    return importe;
  }

  /**
   * @return the documento
   */
  public BigDecimal getDocumento() {
    return documento;
  }

  /**
   * @return the idFichero
   */
  public Long getIdFichero() {
    return idFichero;
  }

  /**
   * @return the entidad
   */
  public Integer getEntidad() {
    return entidad;
  }

  /**
   * @return the oficina
   */
  public Integer getOficina() {
    return oficina;
  }

  /**
   * @return the cuenta
   */
  public Long getCuenta() {
    return cuenta;
  }

  /**
   * @return the saldoinicial
   */
  public Long getSaldoinicial() {
    return saldoinicial;
  }

  /**
   * @return the divisa
   */
  public Integer getDivisa() {
    return divisa;
  }

  /**
   * @return the saldofinal
   */
  public Long getSaldofinal() {
    return saldofinal;
  }

  /**
   * @return the debeFich
   */
  public String getDebeFich() {
    return debeFich;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @param descReferencia the descReferencia to set
   */
  public void setDescReferencia(String descReferencia) {
    this.descReferencia = descReferencia;
  }

  /**
   * @param referencia the referencia to set
   */
  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }

  /**
   * @param tradedate the tradedate to set
   */
  public void setTradedate(String tradedate) {
    this.tradedate = tradedate;
  }

  /**
   * @param settlementdate the settlementdate to set
   */
  public void setSettlementdate(String settlementdate) {
    this.settlementdate = settlementdate;
  }

  /**
   * @param debe the debe to set
   */
  public void setDebe(String debe) {
    this.debe = debe;
  }

  /**
   * @param importe the importe to set
   */
  public void setImporte(BigDecimal importe) {
    this.importe = importe;
  }

  /**
   * @param documento the documento to set
   */
  public void setDocumento(BigDecimal documento) {
    this.documento = documento;
  }

  /**
   * @param idFichero the idFichero to set
   */
  public void setIdFichero(Long idFichero) {
    this.idFichero = idFichero;
  }

  /**
   * @param entidad the entidad to set
   */
  public void setEntidad(Integer entidad) {
    this.entidad = entidad;
  }

  /**
   * @param oficina the oficina to set
   */
  public void setOficina(Integer oficina) {
    this.oficina = oficina;
  }

  /**
   * @param cuenta the cuenta to set
   */
  public void setCuenta(Long cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * @param saldoinicial the saldoinicial to set
   */
  public void setSaldoinicial(Long saldoinicial) {
    this.saldoinicial = saldoinicial;
  }

  /**
   * @param divisa the divisa to set
   */
  public void setDivisa(Integer divisa) {
    this.divisa = divisa;
  }

  /**
   * @param saldofinal the saldofinal to set
   */
  public void setSaldofinal(Long saldofinal) {
    this.saldofinal = saldofinal;
  }

  /**
   * @param debeFich the debeFich to set
   */
  public void setDebeFich(String debeFich) {
    this.debeFich = debeFich;
  }

} // Norma43MovimientoDTO
