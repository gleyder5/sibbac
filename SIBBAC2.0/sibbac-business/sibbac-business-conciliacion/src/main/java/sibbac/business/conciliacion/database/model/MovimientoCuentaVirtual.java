/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.conciliacion.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 *
 * @author jmazorin
 */
@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_MOVIMIENTO_CUENTA_VIRTUAL )
@XmlRootElement
@Audited
public class MovimientoCuentaVirtual extends ATableAudited< MovimientoCuentaVirtual > implements Serializable {

	private static final long	serialVersionUID	= 1L;

	@XmlAttribute
	@Column( name = "ISIN", nullable = false, length = 12 )
	protected String			isin;						// movECC.isin

	@XmlAttribute
	@Column( name = "DESCRIPCIONISIN", nullable = false, length = 80 )
	protected String			descripcionisin;			// sale de la tabla TMCT0_VALORES

	@XmlAttribute
	@Column( name = "SENTIDO", nullable = false, length = 1 )
	protected char				sentido;					// movECC.sentido

	@XmlAttribute
	@Column( name = "CAMARA", nullable = false, length = 4 )
	protected String			camara;					// camra_compensacion.cd_codigo

	@XmlAttribute
	@Column( name = "IDCUENTACOMP", nullable = true )
	private long				idcuentacomp;				// cuentas_de_compensacion.id_cuenta_compensacion

	@XmlAttribute
	@Column( name = "CDCODIGOCCOMP", nullable = true, length = 16 )
	private String				cdcodigoccomp;				// cuentas_de_compensacion.cdctacompensacion

	@XmlAttribute
	@Column( name = "IDCUENTALIQ", nullable = true )
	private long				idcuentaliq;				// _cuenta_liquidacion.id

	@XmlAttribute
	@Column( name = "CDCODIGOCLIQ", nullable = true, length = 16 )
	protected String			cdcodigocliq;				// _cuenta_liquidacion.num_cuenta_ER

	@XmlAttribute
	@Column( name = "TRADEDATE", nullable = true )
	protected Date				tradedate;					// desglosecamara.fecontratacion

	@XmlAttribute
	@Column( name = "SETTLEMENTDATE" )
	protected Date				settlementdate;			// desglosecamara.feliquidacion

	@XmlAttribute
	@Column( name = "TITULOS", nullable = false, precision = 18, scale = 6 )
	protected BigDecimal		titulos;					// movECC.imtitulos

	@XmlAttribute
	@Column( name = "EFECTIVO", nullable = false, precision = 17, scale = 2 )
	protected BigDecimal		efectivo;					// movECC.imtitulos

	@XmlAttribute
	@Column( name = "PRECIO", nullable = true, precision = 13, scale = 6 )
	protected BigDecimal		precio;					// desgloseclitit.imprecio

	@XmlAttribute
	@Column( name = "SENTIDOCUENTAVIRTUAL", nullable = false, length = 1 )
	protected char				sentidocuentavirtual;		// 'M' -> Mercado , 'C' -> Cliente

	@XmlAttribute
	@Column( name = "ALIAS", nullable = true, length = 20 )
	protected String			alias;						// alo.cdalias

	@XmlAttribute
	@Column( name = "DESCRALI", nullable = true, length = 80 )
	protected String			descrali;					// ali.descrali

	@XmlAttribute
	@Column( name = "NCLIENTE", nullable = true, length = 120 )
	protected String			ncliente;

	@XmlAttribute
	@Column( name = "IDLIQUIDACIONPARCIAL", nullable = true, length = 1 )
	protected Character			idliquidacionparcial;

	@XmlAttribute
	@Column( name = "IDCAMARA", nullable = true, length = 8 )
	protected Long				idCamara;

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescripcionisin() {
		return descripcionisin;
	}

	public void setDescripcionisin( String descripcionisin ) {
		this.descripcionisin = descripcionisin;
	}

	public char getSentido() {
		return sentido;
	}

	public void setSentido( char sentido ) {
		this.sentido = sentido;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public long getIdcuentacomp() {
		return idcuentacomp;
	}

	public void setIdcuentacomp( long idcuentacomp ) {
		this.idcuentacomp = idcuentacomp;
	}

	public String getCdcodigoccomp() {
		return cdcodigoccomp;
	}

	public void setCdcodigoccomp( String cdcodigoccomp ) {
		this.cdcodigoccomp = cdcodigoccomp;
	}

	public long getIdcuentaliq() {
		return idcuentaliq;
	}

	public void setIdcuentaliq( long idcuentaliq ) {
		this.idcuentaliq = idcuentaliq;
	}

	public String getCdcodigocliq() {
		return cdcodigocliq;
	}

	public void setCdcodigocliq( String cdcodigocliq ) {
		this.cdcodigocliq = cdcodigocliq;
	}

	public Date getTradedate() {
		return tradedate;
	}

	public void setTradedate( Date tradedate ) {
		this.tradedate = tradedate;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio( BigDecimal precio ) {
		this.precio = precio;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public char getSentidocuentavirtual() {
		return sentidocuentavirtual;
	}

	public void setSentidocuentavirtual( char sentidocuentavirtual ) {
		this.sentidocuentavirtual = sentidocuentavirtual;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getDescrali() {
		return descrali;
	}

	public void setDescrali( String descrali ) {
		this.descrali = descrali;
	}

	public String getNcliente() {
		return ncliente;
	}

	public void setNcliente( String ncliente ) {
		this.ncliente = ncliente;
	}

	public Character getIdLiquidacionParcial() {
		return idliquidacionparcial;
	}

	public void setIdLiquidacionParcial( Character idLiquidacionParcial ) {
		this.idliquidacionparcial = idLiquidacionParcial;
	}

	public Long getIdCamara() {
		return idCamara;
	}

	public void setIdCamara( Long idCamara ) {
		this.idCamara = idCamara;
	}
}
