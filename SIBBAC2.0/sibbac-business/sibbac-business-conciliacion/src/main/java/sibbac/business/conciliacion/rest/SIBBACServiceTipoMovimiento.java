package sibbac.business.conciliacion.rest;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fallidos.database.bo.TipoMovimientosFallidosBo;
import sibbac.business.fallidos.database.model.TipoMovimientosFallidos;
import sibbac.business.wrappers.database.bo.Tmct0tipoMovimientoConciliacionBo;
import sibbac.business.wrappers.database.model.Tmct0TipoMovimientoConciliacion;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class SIBBACServiceTipoMovimiento.
 */
@SIBBACService
public class SIBBACServiceTipoMovimiento implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger					LOG							= LoggerFactory.getLogger( SIBBACServiceTipoMovimiento.class );

	/** The tmc bo. */
	@Autowired
	private Tmct0tipoMovimientoConciliacionBo	tmcBo;

	/** The tmf bo. */
	@Autowired
	private TipoMovimientosFallidosBo			tmfBo;

	/** The Constant KEY_COMMAND. */
	public static final String					KEY_COMMAND					= "command";

	// Consulta
	/** The Constant CMD_GET_TIPO_MOVIMIENTOS. */
	public static final String					CMD_GET_TIPO_MOVIMIENTOS	= "getTipoMovimiento";

	// Parametros

	/** The Constant PRM_CODIGO. */
	public static final String					PRM_CODIGO					= "codigo";

	/** The Constant PRM_DESCRIPCION. */
	public static final String					PRM_DESCRIPCION				= "descripcion";

	/** The Constant PRM_AFECTA_TITULOS. */
	public static final String					PRM_AFECTA_TITULOS			= "afecta_titulos";

	/** The Constant PRM_AFECTA_EFECTIVO. */
	public static final String					PRM_AFECTA_EFECTIVO			= "afecta_efectivo";

	/**
	 * Instantiates a new SIBBAC service tipo movimiento.
	 */
	public SIBBACServiceTipoMovimiento() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process( WebRequest webRequest ) {
		String prefix = "[SIBBACServiceTipoMovimiento::process] ";
		LOG.debug( prefix + "Processing a web request..." );
		WebResponse webResponse = new WebResponse();

		String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );
		switch ( command ) {
			case CMD_GET_TIPO_MOVIMIENTOS:
				// Traigo los desglosesclitit
				List< Tmct0TipoMovimientoConciliacion > tipomovimientosconciliacion = ( List< Tmct0TipoMovimientoConciliacion > ) tmcBo
						.findAll();
				List< TipoMovimientosFallidos > tipomovimientosfallidos = ( List< TipoMovimientosFallidos > ) tmfBo.findAll();

				List< Object > resultadosFinal = new ArrayList< Object >();
				resultadosFinal.addAll( tipomovimientosconciliacion );
				resultadosFinal.addAll( tipomovimientosfallidos );
				Map< String, Object > response = new HashMap< String, Object >();

				response.put( "result_tipo_movimiento", resultadosFinal );
				webResponse.setResultados( response );
				break;
		}
		return webResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		return Arrays.asList( new String[] {
			CMD_GET_TIPO_MOVIMIENTOS
		} );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return Arrays.asList( new String[] {} );
	}
}
