package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class AnotacionExportECCDTO implements IAnotacionECCDTO {

	private final String		cdisin;

	private final String		isinDescripcion;

	private final String		cdCuenta;

	private final BigDecimal	nutitulos;

	private final BigDecimal	imefectivo;

	public AnotacionExportECCDTO( String cdisin, String isinDescripcion, String cdCuenta, BigDecimal nutitulos, BigDecimal imefectivo ) {
		super();
		this.cdisin = cdisin;
		this.isinDescripcion = isinDescripcion;
		this.cdCuenta = cdCuenta;
		this.nutitulos = nutitulos;
		this.imefectivo = imefectivo;
	}

	@Override
	public String getCdisin() {
		return cdisin;
	}

	@Override
	public String getIsinDescripcion() {
		return isinDescripcion;
	}

	@Override
	public String getCdCuenta() {
		return cdCuenta;
	}

	@Override
	public BigDecimal getNutitulos() {
		return nutitulos;
	}

	@Override
	public BigDecimal getImefectivo() {
		return imefectivo;
	}

}
