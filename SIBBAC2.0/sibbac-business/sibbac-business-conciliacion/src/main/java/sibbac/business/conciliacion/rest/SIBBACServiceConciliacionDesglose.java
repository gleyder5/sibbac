package sibbac.business.conciliacion.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dto.XasDTO;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.PartenonRecordBeanBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dto.DesgloseDTO;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoCambio;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoOperacion;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebRequestForDataTable;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.beans.WebResponseDataTable;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceConciliacionDesglose implements SIBBACServiceBean {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceConciliacionDesglose.class);
  // Util
  private ConciliacionUtil util = ConciliacionUtil.getInstance();

  @Value("${conciliacion.desglose.step}")
  Integer step;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Autowired
  private PartenonRecordBeanBo partenonRecordBeanBo;

  @Autowired
  private Tmct0ValoresBo valoresBo;

  @Autowired
  private Tmct0xasBo tmct0xasBo;

  protected static final String TAG = "SIBBACServiceConciliacionDesglose";

  public static final String KEY_COMMAND = "command";

  // filtros
  private static final String FILTER_FECHA_DESDE = "fechaDesde";
  private static final String FILTER_FECHA_HASTA = "fechaHasta";
  private static final String FILTER_ISIN = "isin";
  private static final String FILTER_TIPO_REG = "tipoReg";
  private static final String FILTER_REFRE = "refre";
  private static final String FILTER_TITULOS = "titulos";
  private static final String FILTER_NOMINEJ = "nominej";
  private static final String FILTER_ENTIDAD = "entidad";
  private static final String FILTER_EMP_CON_VAL = "empConVal";
  private static final String FILTER_NUM_CTO_OFICINA = "numCtoOficina";
  private static final String FILTER_NUM_CTR_VAL = "numCtrVal";
  private static final String FILTER_NUM_ORDEN = "numOrden";
  private static final String FILTER_COD_SV = "codsv";
  private static final String FILTER_F_EJECUCION = "fejecucion";
  private static final String FILTER_BOLSA = "bolsa";
  private static final String FILTER_TIPO_OPERACION = "tipoOperacion";
  private static final String FILTER_CAMBIO_OPERACION = "cambioOperacion";
  private static final String FILTER_NIF_BIC = "nif_bic";
  private static final String FILTER_TIPO_SALDO = "tipoSaldo";
  private static final String FILTER_REF_MOD = "refMod";
  private static final String FILTER_SEGMENTO = "segmento";
  private static final String FILTER_ORD_COM = "ordCom";
  private static final String FILTER_TIPO_CAMBIO = "tipoCambio";
  private static final String FILTER_CCV = "ccv";
  private static final String FILTER_CODEMPR = "codempr";
  private static final String FILTER_CODBIC = "codbic";
  private static final String FILTER_DISTRITO = "distrito";
  private static final String FILTER_DOMICILI = "domicili";
  private static final String FILTER_IDSENC = "idsenc";
  private static final String FILTER_IDNAC = "nidnac";
  private static final String FILTER_NACLIDAD = "naclidad";
  private static final String FILTER_NIFBIC = "nifbic";
  private static final String FILTER_NOMAPELL = "nomapell";
  private static final String FILTER_PAIS = "pais";
  private static final String FILTER_PAIS_RE = "pasiRe";
  private static final String FILTER_PARTICIP = "particip";
  private static final String FILTER_POBLACIO = "poblacio";
  private static final String FILTER_PROVINCIA = "provincia";
  private static final String FILTER_TIPO_DOC = "tipodoc";
  private static final String FILTER_TIP_PER = "tipper";
  private static final String FILTER_TIP_TITU = "tiptitu";
  private static final String FILTER_ID_DESGLOSE_RECORD_BEAN = "idDesgloseRecordBean";

  private static final String FILTER_ID = "id";

  private static final String FILTER_BOLSA_MERCADO = "bolsaMercado";

  // Consulta
  private static final String CMD_GET_DESGLOSES_COUNT = "getDesglosesCount";
  private static final String CMD_GET_DESGLOSES_PAGEABLE = "getDesglosesPageable";
  private static final String CMD_GET_DESGLOSES_LIST = "getDesglosesSinPaginacion";
  private static final String CMD_GET_DESGLOSES_EXPORT = "getDesglosesExport";
  private static final String CMD_MARCAR_VALIDADO = "marcarValidado";
  private static final String CMD_ALTA_DESGLOSE = "altaDesglose";
  private static final String CMD_EDITAR_DESGLOSE = "editarDesglose";
  private static final String CMD_DELETE_DESGLOSE = "borrarDesglose";
  private static final String CMD_GET_DESGLOSES_HUERFANOS_COUNT = "getDesglosesHuerfanosCount";
  private static final String CMD_GET_DESGLOSES_HUERFANOS_PAGEABLE = "getDesglosesHuerfanosPageable";
  private static final String CMD_GET_DESGLOSES_HUERFANOS_LIST = "getDesglosesHuerfanosSinPaginacion";
  private static final String CMD_GET_DESGLOSES_HUERFANOS_EXPORT = "getDesglosesHuerfanoExport";
  private static final String CMD_GET_LISTA_BOLSA_MERCADO = "getListaBolsaMercado";
  private static final String CMD_ALTA_TITULAR = "altaTitular";
  private static final String CMD_EDITAR_TITULAR = "editarTitular";
  private static final String CMD_DELETE_TITULAR = "borrarTitular";

  // resultados
  private static final String RESULT_DESGLOSES_LIST = "desglosesList";
  private static final String RESULT_DESGLOSES_LIST_EXPORT = "desglosesListExport";
  private static final String RESULT_BOLSA_MERCADO_LIST = "bolsaMercadoList";

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    String prefix = "[" + TAG + "::process] ";
    LOG.debug(prefix + "Processing a web request...");
    // prepara webResponse
    WebResponse webResponse = new WebResponse();
    // extrae parametros del httpRequest
    Map<String, String> params = webRequest.getFilters();
    // Este sera el mapa de filtros que se manda al dao extraido de los
    // filtros
    if (params != null) {
      util.normalizeFilters(params);
    }
    // Resultado
    // obtiene comandos(paths)
    String command = webRequest.getAction();
    LOG.debug(prefix + "+ Command.: [{}]", command);

    List<String> parametrosTabla;
    switch (command) {
      case CMD_GET_DESGLOSES_COUNT:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          this.getDesglosesCount(webRequest, webResponse);
        }
        break;
      case CMD_GET_DESGLOSES_PAGEABLE:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          webResponse = this.getDesglosesPageable((WebRequestForDataTable) webRequest);
        }
        break;
      case CMD_GET_DESGLOSES_LIST:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          this.getDesglosesList(webRequest, webResponse);
        }
        break;
      case CMD_MARCAR_VALIDADO:
        parametrosTabla = webRequest.getList();
        webResponse.setResultados(marcarValidado(parametrosTabla));
        break;
      case CMD_ALTA_DESGLOSE:
        if (this.altaDesgloseListCheckFilters(webRequest, webResponse)) {
          this.createDesglose(webRequest, webResponse);
        }
        break;
      case CMD_EDITAR_DESGLOSE:
        if (this.editarDesgloseListCheckFilters(webRequest, webResponse)) {
          this.editarDesglose(webRequest, webResponse);
        }
        break;
      case CMD_DELETE_DESGLOSE:
        if (this.eliminarDesgloseListCheckFilters(webRequest, webResponse)) {
          this.eliminarDesglose(webRequest, webResponse);
        }
        break;
      case CMD_GET_DESGLOSES_HUERFANOS_COUNT:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          this.getDesglosesHuerfanosCount(webRequest, webResponse);
        }
        break;
      case CMD_GET_DESGLOSES_HUERFANOS_PAGEABLE:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          webResponse = this.getDesglosesHuerfanosPageable((WebRequestForDataTable) webRequest);
        }
        break;
      case CMD_GET_DESGLOSES_HUERFANOS_LIST:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          this.getDesglosesHuerfanosList(webRequest, webResponse);
        }
        break;
      case CMD_GET_DESGLOSES_EXPORT:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          this.getDesglosesExport(webRequest, webResponse);
        }
        break;
      case CMD_GET_DESGLOSES_HUERFANOS_EXPORT:
        if (this.getDesglosesCheckFilters(webRequest, webResponse)) {
          this.getDesglosesHuerfanosExport(webRequest, webResponse);
        }
        break;
      case CMD_GET_LISTA_BOLSA_MERCADO:
        this.getBolsaMercadoList(webRequest, webResponse);
        break;
      case CMD_ALTA_TITULAR:
        if (this.altaTitularListCheckFilters(webRequest, webResponse)) {
          this.createTitular(webRequest, webResponse);
        }
        break;
      case CMD_EDITAR_TITULAR:
        if (this.editarTitularListCheckFilters(webRequest, webResponse)) {
          this.editarTitular(webRequest, webResponse);
        }
        break;
      case CMD_DELETE_TITULAR:
        if (this.eliminarTitularListCheckFilters(webRequest, webResponse)) {
          this.eliminarTitular(webRequest, webResponse);
        }
        break;

      default:
        webResponse.setError(command + " is not a valid action. Valid actions are: " + this.getAvailableCommands());
        break;
    }
    return webResponse;
  }

  private void getDesglosesCount(WebRequest webRequest, WebResponse webResponse) {
    Map<String, String> filters = webRequest.getFilters();
    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    List<String> isinVector = new ArrayList<String>();
    try {

      if (StringUtils.isNotEmpty(filters.get(FILTER_ISIN))) {
        String[] parts = filters.get(FILTER_ISIN).split(";");
        for (String part : parts) {
          isinVector.add(part);
        }
      }
    }
    catch (RuntimeException e1) {
      LOG.warn("[getDesglosesCount] Error inesperado", e1);
    }

    int count = desgloseRecordBeanBo.countFindAllDTOPageable(fechaDesde, fechaHasta, isinVector);

    webResponse.setError("");
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("registros", count);
    webResponse.setResultados(result);
  }

  /**
   * @param filtros
   * @return
   */
  private Date convertirStringADate(final String fechaString) {
    Date fecha = null;
    if (StringUtils.isNotEmpty(fechaString)) {
      fecha = FormatDataUtils.convertStringToDate(fechaString);
    }
    return fecha;
  }

  private boolean getDesglosesCheckFilters(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: getDesglosesCheckFilters] Init");

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    final String strFechaDesde = filtros.get(FILTER_FECHA_DESDE);
    if (strFechaDesde != null) {
      if (StringUtils.isNotEmpty(filtros.get(FILTER_FECHA_DESDE))) {
        final Date fechaDesde = convertirStringADate(filtros.get(FILTER_FECHA_DESDE));
        if (fechaDesde == null) {
          errorMsg = errorMsg + "The '" + FILTER_FECHA_DESDE + "' field must have format: YYYYMMDD. ";
          filtersRight = false;
        }
      }
    }// if (strFechaDesde != null)
    else {
      filtersRight = false;
    }

    final String strFechaHasta = filtros.get(FILTER_FECHA_HASTA);
    if (strFechaHasta != null) {
      if (StringUtils.isNotEmpty(filtros.get(FILTER_FECHA_HASTA))) {
        final Date fechaHasta = convertirStringADate(filtros.get(FILTER_FECHA_HASTA));
        if (fechaHasta == null) {
          errorMsg = errorMsg + "The '" + FILTER_FECHA_HASTA + "' field must have format: YYYYMMDD. ";
          filtersRight = false;
        }
      }
    }// if (fechaHasta != null)

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
      LOG.error("[" + TAG + " :: getDesglosesCheckFilters] Error: " + errorMsg);
    }

    LOG.debug("[" + TAG + " :: getDesglosesCheckFilters] Fin");
    return filtersRight;
  }// private boolean getDesglosesCheckFilters( WebRequest webRequest,
   // WebResponse webResponse ) {

  private WebResponseDataTable<DesgloseDTO> getDesglosesPageable(WebRequestForDataTable webRequest)
      throws SIBBACBusinessException {
    WebResponseDataTable<DesgloseDTO> webResponse = new WebResponseDataTable<DesgloseDTO>();

    LOG.debug("[" + TAG + " :: getDesglosesPageable] Init");
    Map<String, String> filters = webRequest.getFilters();
    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    List<String> isinVector = new ArrayList<String>();

    try {

      if (StringUtils.isNotEmpty(filters.get(FILTER_ISIN))) {
        String[] parts = filters.get(FILTER_ISIN).split(";");
        for (String part : parts) {
          isinVector.add(part);
        }
      }

    }
    catch (RuntimeException e1) {
      LOG.warn("[getDesglosesPageable] Error inesperado", e1);
    }

    List<DesgloseDTO> listaDesgloses = new ArrayList<DesgloseDTO>();

    /* Parámetros enviados por el objeto datatable */
    final int iDisplayStart = webRequest.getStart();
    final int iDisplayLength = webRequest.getLength();

    LOG.debug("[" + TAG + " :: getDesglosesPageable] ");
    try {
      // Resultados paginados por el DataTable JQuery
      int count = desgloseRecordBeanBo.countFindAllDTOPageable(fechaDesde, fechaHasta, isinVector);
      listaDesgloses = desgloseRecordBeanBo.findAllDTOPageable(fechaDesde, fechaHasta, isinVector, iDisplayStart,
          iDisplayLength);

      webResponse.setColumns(webRequest.getColumns());
      webResponse.setDraw(Integer.valueOf(webRequest.getDraw()));
      webResponse.setRecordsTotal(webResponse.getRecordsTotal() + count);
      webResponse.setRecordsFiltered(webResponse.getRecordsFiltered() + count);
      webResponse.addData(new ArrayList<DesgloseDTO>(listaDesgloses));
      // return webResponse;
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: getDesglosesPageable] Excepción consulta " + e.getMessage());
    }
    LOG.debug("[" + TAG + " :: getDesglosesPageable] Terminando consulta. Obtenidos " + listaDesgloses.size()
        + " registros");

    LOG.debug("[" + TAG + " :: getDesglosesPageable] Fin ");
    return webResponse;
  }

  private void getDesglosesList(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    LOG.debug("[" + TAG + " :: getDesglosesList] Init");
    Map<String, String> filters = webRequest.getFilters();
    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    List<String> isinVector = new ArrayList<String>();

    try {

      if (StringUtils.isNotEmpty(filters.get(FILTER_ISIN))) {
        String[] parts = filters.get(FILTER_ISIN).split(";");
        for (String part : parts) {
          isinVector.add(part);
        }
      }

    }
    catch (RuntimeException e1) {
      LOG.warn("[getDesglosesList] Error inesperado al analizar filtro", e1);
    }
    final Map<String, List<DesgloseDTO>> resultados = new HashMap<String, List<DesgloseDTO>>();
    List<DesgloseDTO> listaDesgloses = new ArrayList<DesgloseDTO>();

    // Obteniendo erroneos
    LOG.debug("[" + TAG + " :: getDesglosesList] Comenzando consulta");
    try {
      listaDesgloses = desgloseRecordBeanBo.findDTOForService(fechaDesde, fechaHasta, isinVector);
      LOG.debug("[" + TAG + " :: getDesglosesList] Terminando consulta. Obtenidos " + listaDesgloses.size()
          + " registros");
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: getDesglosesList] Excepción consulta" + e.getMessage());
    }
    resultados.put(RESULT_DESGLOSES_LIST, listaDesgloses);
    webResponse.setResultados(resultados);
    LOG.debug("[" + TAG + " :: getDesglosesList] Fin ");
  }

  private Map<String, Object> marcarValidado(List<String> params) {

    LOG.debug("[" + TAG + "::marcarValidado] Entra en el metodo marcarValidado");

    Map<String, Object> list = new HashMap<String, Object>();

    try {
      Integer nCont = 0;
      Iterator<String> it = params.iterator();

      while (it.hasNext()) {

        Object temp = it.next();
        String numOrden = temp.toString();

        List<DesgloseRecordDTO> listaDesglosesRecordDTO = desgloseRecordBeanBo.findByNumOrden(numOrden,
            EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_ERRONEOS.getId());
        if (listaDesglosesRecordDTO != null && !listaDesglosesRecordDTO.isEmpty()) {

          Iterator<DesgloseRecordDTO> iterator = listaDesglosesRecordDTO.iterator();
          while (iterator.hasNext()) {
            DesgloseRecordDTO desglose = iterator.next();
            DesgloseRecordBean desgloseRecordBean = desgloseRecordBeanBo.findById(desglose.getId());
            desgloseRecordBean
                .setPorcesado(EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS
                    .getId());
            desgloseRecordBeanBo.save(desgloseRecordBean);
            nCont++;
          }// while (iterator.hasNext())
        }// if (listaDesglosesRecordDTO != null &&
         // !listaDesglosesRecordDTO.isEmpty())
      }// while(it.hasNext()){

      if (nCont > 0) {
        list.put("resultado", "Se han realizado " + nCont + " validaciones correctamente.");
      }
      else {
        throw new SIBBACBusinessException("No se ha realizado NINGUN envio de validación.");
      }

    }
    catch (Exception e) {
      list.put("error", e);
    }
    return list;

  }

  private boolean altaDesgloseListCheckFilters(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    String tipoReg = processString(filtros.get(FILTER_TIPO_REG));
    if (StringUtils.isNotEmpty(tipoReg)) {
      int maxLength = 2;
      if (tipoReg.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_REG + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String refre = processString(filtros.get(FILTER_REFRE));
    if (StringUtils.isNotEmpty(refre)) {
      int maxLength = 10;
      if (refre.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_REFRE + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String titulos = processString(filtros.get(FILTER_TITULOS));
    if (StringUtils.isNotEmpty(titulos)) {
      int maxLength = 11;
      if (titulos.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TITULOS + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nominej = processString(filtros.get(FILTER_NOMINEJ));
    if (StringUtils.isNotEmpty(nominej)) {
      int maxLength = 18;
      if (nominej.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NOMINEJ + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String entidad = processString(filtros.get(FILTER_ENTIDAD));
    if (StringUtils.isNotEmpty(entidad)) {
      int maxLength = 4;
      if (entidad.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_EMP_CON_VAL + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String empConVal = processString(filtros.get(FILTER_EMP_CON_VAL));
    if (StringUtils.isNotEmpty(empConVal)) {
      int maxLength = 4;
      if (empConVal.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_EMP_CON_VAL + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String numCtoOficina = processString(filtros.get(FILTER_NUM_CTO_OFICINA));
    if (StringUtils.isNotEmpty(numCtoOficina)) {
      int maxLength = 4;
      if (numCtoOficina.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NUM_CTO_OFICINA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String numOrden = processString(filtros.get(FILTER_NUM_ORDEN));
    if (StringUtils.isNotEmpty(numOrden)) {
      int maxLength = 20;
      if (numOrden.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NUM_ORDEN + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codSV = processString(filtros.get(FILTER_COD_SV));
    if (StringUtils.isNotEmpty(codSV)) {
      int maxLength = 4;
      if (codSV.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_COD_SV + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String isin = processString(filtros.get(FILTER_ISIN));
    if (StringUtils.isNotEmpty(isin)) {
      int maxLength = 12;
      if (isin.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ISIN + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String fejecucion = processString(filtros.get(FILTER_F_EJECUCION));
    if (StringUtils.isNotEmpty(fejecucion)) {
      int maxLength = 8;
      if (fejecucion.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_F_EJECUCION + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String bolsa = processString(filtros.get(FILTER_BOLSA));
    if (StringUtils.isNotEmpty(bolsa)) {
      int maxLength = 3;
      if (bolsa.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_BOLSA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tipoOperacion = processString(filtros.get(FILTER_TIPO_OPERACION));
    if (StringUtils.isNotEmpty(tipoOperacion)) {
      int maxLength = 1;
      if (tipoOperacion.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_OPERACION + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String cambioOperacion = processString(filtros.get(FILTER_CAMBIO_OPERACION));
    if (StringUtils.isNotEmpty(cambioOperacion)) {
      int maxLength = 11;
      if (cambioOperacion.indexOf(".") != -1) {
        String entera = cambioOperacion.substring(0, cambioOperacion.indexOf("."));
        String decimal = cambioOperacion.substring(cambioOperacion.indexOf(".") + 1);
        decimal = decimal.substring(0, 4);
        cambioOperacion = entera + "." + decimal;
      }
      if (cambioOperacion.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CAMBIO_OPERACION + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nif_bic = processString(filtros.get(FILTER_NIF_BIC));
    if (StringUtils.isNotEmpty(nif_bic)) {
      int maxLength = 11;
      if (nif_bic.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NIF_BIC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tipoSaldo = processString(filtros.get(FILTER_TIPO_SALDO));
    if (StringUtils.isNotEmpty(tipoSaldo)) {
      int maxLength = 1;
      if (tipoSaldo.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_SALDO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String refMod = processString(filtros.get(FILTER_REF_MOD));
    if (StringUtils.isNotEmpty(refMod)) {
      int maxLength = 35;
      if (refMod.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_REF_MOD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String segmento = processString(filtros.get(FILTER_SEGMENTO));
    if (StringUtils.isNotEmpty(segmento)) {
      int maxLength = 3;
      if (segmento.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_SEGMENTO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String ordCom = processString(filtros.get(FILTER_ORD_COM));
    if (StringUtils.isNotEmpty(ordCom)) {
      int maxLength = 4;
      if (ordCom.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ORD_COM + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tipoCambio = processString(filtros.get(FILTER_TIPO_CAMBIO));
    if (StringUtils.isNotEmpty(tipoCambio)) {
      int maxLength = 1;
      if (tipoCambio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_CAMBIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
    }
    return filtersRight;

  }

  private boolean altaTitularListCheckFilters(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    String bolsa = processString(filtros.get(FILTER_BOLSA));

    if (StringUtils.isNotEmpty(bolsa)) {
      if (bolsa.indexOf("string:") != -1) {
        bolsa = bolsa.substring(bolsa.indexOf("string:") + "string:".length());
        if (bolsa.length() == 3) {
          bolsa = bolsa.substring(1, 3);
        }
        filtros.put(FILTER_BOLSA, bolsa);
      }

      int maxLength = 2;
      if (bolsa.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_BOLSA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String ccv = processString(filtros.get(FILTER_CCV));
    if (StringUtils.isNotEmpty(ccv)) {
      int maxLength = 30;
      if (ccv.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CCV + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codempr = processString(filtros.get(FILTER_CODEMPR));
    if (StringUtils.isNotEmpty(codempr)) {
      int maxLength = 4;
      if (codempr.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CODEMPR + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codbic = processString(filtros.get(FILTER_CODBIC));
    if (StringUtils.isNotEmpty(codbic)) {
      int maxLength = 11;
      if (codbic.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CODBIC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String distrito = processString(filtros.get(FILTER_DISTRITO));
    if (StringUtils.isNotEmpty(distrito)) {
      int maxLength = 5;
      if (distrito.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_DISTRITO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String domicili = processString(filtros.get(FILTER_DOMICILI));
    if (StringUtils.isNotEmpty(domicili)) {
      int maxLength = 40;
      if (domicili.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_DOMICILI + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String idsenc = processString(filtros.get(FILTER_IDSENC));
    if (StringUtils.isNotEmpty(idsenc)) {
      int maxLength = 2;
      if (idsenc.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_IDSENC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String idnac = processString(filtros.get(FILTER_IDNAC));
    if (StringUtils.isNotEmpty(idnac)) {

      if (idnac.indexOf("string:") != -1) {
        idnac = idnac.substring(idnac.indexOf("string:") + "string:".length());

        filtros.put(FILTER_IDNAC, idnac);
      }

      int maxLength = 1;
      if (idnac.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_IDNAC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String naclidad = processString(filtros.get(FILTER_NACLIDAD));
    if (StringUtils.isNotEmpty(naclidad)) {
      if (naclidad.indexOf("string:") != -1) {
        naclidad = naclidad.substring(naclidad.indexOf("string:") + "string:".length());

        filtros.put(FILTER_NACLIDAD, naclidad);
      }
      int maxLength = 3;
      if (naclidad.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NACLIDAD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String nifbic = processString(filtros.get(FILTER_NIFBIC));
    if (StringUtils.isNotEmpty(nifbic)) {
      int maxLength = 11;
      if (nifbic.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NIFBIC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String nomapell = processString(filtros.get(FILTER_NOMAPELL));
    if (StringUtils.isNotEmpty(nomapell)) {
      int maxLength = 40;
      if (nomapell.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NOMAPELL + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String pais = processString(filtros.get(FILTER_PAIS));
    if (StringUtils.isNotEmpty(pais)) {
      int maxLength = 20;
      if (pais.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PAIS + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String paisRe = processString(filtros.get(FILTER_PAIS_RE));
    if (StringUtils.isNotEmpty(paisRe)) {
      if (paisRe.indexOf("string:") != -1) {
        paisRe = paisRe.substring(paisRe.indexOf("string:") + "string:".length());

        filtros.put(FILTER_PAIS_RE, paisRe);
      }
      int maxLength = 3;
      if (paisRe.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PAIS_RE + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String particip = processString(filtros.get(FILTER_PARTICIP));
    if (StringUtils.isNotEmpty(particip)) {
      int maxLength = 5;
      if (particip.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PARTICIP + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String poblacio = processString(filtros.get(FILTER_POBLACIO));
    if (StringUtils.isNotEmpty(poblacio)) {
      int maxLength = 20;
      if (poblacio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_POBLACIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String provincia = processString(filtros.get(FILTER_PROVINCIA));
    if (StringUtils.isNotEmpty(provincia)) {
      int maxLength = 20;
      if (provincia.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PROVINCIA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String tipodoc = processString(filtros.get(FILTER_TIPO_DOC));
    if (StringUtils.isNotEmpty(tipodoc)) {
      tipodoc = eliminarStringParametro(filtros, tipodoc, FILTER_TIPO_DOC);
      int maxLength = 1;
      if (tipodoc.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_DOC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String tipper = processString(filtros.get(FILTER_TIP_PER));
    if (StringUtils.isNotEmpty(tipper)) {
      if (tipper.indexOf("string:") != -1) {
        tipper = tipper.substring(tipper.indexOf("string:") + "string:".length());

        filtros.put(FILTER_TIP_PER, tipper);
      }
      int maxLength = 1;
      if (tipper.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIP_PER + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String tiptitu = processString(filtros.get(FILTER_TIP_TITU));
    if (StringUtils.isNotEmpty(tiptitu)) {
      tiptitu = eliminarStringParametro(filtros, tiptitu, FILTER_TIP_TITU);
      int maxLength = 1;
      if (tiptitu.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIP_TITU + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String idDesgloseRecordBean = processString(filtros.get(FILTER_ID_DESGLOSE_RECORD_BEAN));
    if (StringUtils.isNotEmpty(idDesgloseRecordBean)) {
      int maxLength = 8;
      if (idDesgloseRecordBean.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ID_DESGLOSE_RECORD_BEAN + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String numOrden = processString(filtros.get(FILTER_NUM_ORDEN));
    if (StringUtils.isNotEmpty(numOrden)) {
      int maxLength = 20;
      if (numOrden.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NUM_ORDEN + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (!filtersRight) {
      webResponse.setError(errorMsg);
    }
    return filtersRight;

  }

  private String eliminarStringParametro(Map<String, String> filtros, String parametro, String claveParametro) {
    if (parametro.indexOf("string:") != -1) {
      parametro = parametro.substring(parametro.indexOf("string:") + "string:".length());

      filtros.put(claveParametro, parametro);
    }
    return parametro;
  }

  private boolean editarTitularListCheckFilters(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    String id = processString(filtros.get(FILTER_ID));
    if (StringUtils.isNotEmpty(id)) {
      int maxLength = 8;
      if (id.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ID + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String bolsa = processString(filtros.get(FILTER_BOLSA));
    if (StringUtils.isNotEmpty(bolsa)) {

      if (bolsa.indexOf("string:") != -1) {
        bolsa = bolsa.substring(bolsa.indexOf("string:") + "string:".length());
        if (bolsa.length() == 3) {
          bolsa = bolsa.substring(1, 3);
        }
        filtros.put(FILTER_BOLSA, bolsa);
      }
      int maxLength = 2;
      if (bolsa.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_BOLSA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String ccv = processString(filtros.get(FILTER_CCV));
    if (StringUtils.isNotEmpty(ccv)) {
      int maxLength = 30;
      if (ccv.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CCV + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codempr = processString(filtros.get(FILTER_CODEMPR));
    if (StringUtils.isNotEmpty(codempr)) {
      int maxLength = 4;
      if (codempr.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CODEMPR + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codbic = processString(filtros.get(FILTER_CODBIC));
    if (StringUtils.isNotEmpty(codbic)) {
      int maxLength = 11;
      if (codbic.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CODBIC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String distrito = processString(filtros.get(FILTER_DISTRITO));
    if (StringUtils.isNotEmpty(distrito)) {
      int maxLength = 5;
      if (distrito.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_DISTRITO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String domicili = processString(filtros.get(FILTER_DOMICILI));
    if (StringUtils.isNotEmpty(domicili)) {
      int maxLength = 40;
      if (domicili.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_DOMICILI + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String idsenc = processString(filtros.get(FILTER_IDSENC));
    if (StringUtils.isNotEmpty(idsenc)) {
      int maxLength = 2;
      if (idsenc.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_IDSENC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String idnac = processString(filtros.get(FILTER_IDNAC));
    if (StringUtils.isNotEmpty(idnac)) {
      idnac = eliminarStringParametro(filtros, idnac, FILTER_IDNAC);
      int maxLength = 1;
      if (idnac.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_IDNAC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String naclidad = processString(filtros.get(FILTER_NACLIDAD));
    if (StringUtils.isNotEmpty(naclidad)) {
      naclidad = eliminarStringParametro(filtros, naclidad, FILTER_NACLIDAD);
      int maxLength = 3;
      if (naclidad.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NACLIDAD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String nifbic = processString(filtros.get(FILTER_NIFBIC));
    if (StringUtils.isNotEmpty(nifbic)) {
      int maxLength = 11;
      if (nifbic.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NIFBIC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String nomapell = processString(filtros.get(FILTER_NOMAPELL));
    if (StringUtils.isNotEmpty(nomapell)) {
      int maxLength = 40;
      if (nomapell.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NOMAPELL + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String pais = processString(filtros.get(FILTER_PAIS));
    if (StringUtils.isNotEmpty(pais)) {
      int maxLength = 20;
      if (pais.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PAIS + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String paisRe = processString(filtros.get(FILTER_PAIS_RE));
    if (StringUtils.isNotEmpty(paisRe)) {
      int maxLength = 3;
      paisRe = eliminarStringParametro(filtros, paisRe, FILTER_PAIS_RE);
      if (paisRe.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PAIS_RE + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String particip = processString(filtros.get(FILTER_PARTICIP));
    if (StringUtils.isNotEmpty(particip)) {
      int maxLength = 5;
      if (particip.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PARTICIP + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String poblacio = processString(filtros.get(FILTER_POBLACIO));
    if (StringUtils.isNotEmpty(poblacio)) {
      int maxLength = 20;
      if (poblacio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_POBLACIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String provincia = processString(filtros.get(FILTER_PROVINCIA));
    if (StringUtils.isNotEmpty(provincia)) {
      int maxLength = 20;
      if (provincia.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PROVINCIA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String tipodoc = processString(filtros.get(FILTER_TIPO_DOC));
    if (StringUtils.isNotEmpty(tipodoc)) {
      tipodoc = eliminarStringParametro(filtros, tipodoc, FILTER_TIPO_DOC);
      int maxLength = 1;
      if (tipodoc.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_DOC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String tipper = processString(filtros.get(FILTER_TIP_PER));
    if (StringUtils.isNotEmpty(tipper)) {
      tipper = eliminarStringParametro(filtros, tipper, FILTER_TIP_PER);
      int maxLength = 1;
      if (tipper.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIP_PER + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String tiptitu = processString(filtros.get(FILTER_TIP_TITU));
    if (StringUtils.isNotEmpty(tiptitu)) {
      tiptitu = eliminarStringParametro(filtros, tiptitu, FILTER_TIP_TITU);
      int maxLength = 1;
      if (tiptitu.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIP_TITU + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }
    String idDesgloseRecordBean = processString(filtros.get(FILTER_ID_DESGLOSE_RECORD_BEAN));
    if (StringUtils.isNotEmpty(idDesgloseRecordBean)) {
      int maxLength = 8;
      if (idDesgloseRecordBean.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ID_DESGLOSE_RECORD_BEAN + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (!filtersRight) {
      webResponse.setError(errorMsg);
    }
    return filtersRight;

  }

  private boolean editarDesgloseListCheckFilters(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    String id = processString(filtros.get(FILTER_ID));
    if (StringUtils.isNotEmpty(id)) {
      int maxLength = 8;
      if (id.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ID + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String fejecucion = processString(filtros.get(FILTER_F_EJECUCION));
    if (StringUtils.isNotEmpty(fejecucion)) {
      int maxLength = 8;
      if (fejecucion.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_F_EJECUCION + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tipoOperacion = processString(filtros.get(FILTER_TIPO_OPERACION));
    if (StringUtils.isNotEmpty(tipoOperacion)) {
      int maxLength = 1;
      if (tipoOperacion.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_OPERACION + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String bolsa = processString(filtros.get(FILTER_BOLSA));
    if (StringUtils.isNotEmpty(bolsa)) {
      if (bolsa.indexOf("string:") != -1) {
        bolsa = bolsa.substring(bolsa.indexOf("string:") + "string:".length());
        filtros.put(FILTER_BOLSA, bolsa);
      }

      int maxLength = 3;
      if (bolsa.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_BOLSA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String isin = processString(filtros.get(FILTER_ISIN));
    if (StringUtils.isNotEmpty(isin)) {
      int maxLength = 12;
      if (isin.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ISIN + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String titulos = processString(filtros.get(FILTER_TITULOS));
    if (StringUtils.isNotEmpty(titulos)) {
      int maxLength = 11;
      if (titulos.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TITULOS + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nominej = processString(filtros.get(FILTER_NOMINEJ));
    if (StringUtils.isNotEmpty(nominej)) {
      int maxLength = 16;
      if (nominej.indexOf(".") != -1) {
        String entera = nominej.substring(0, nominej.indexOf("."));
        String decimal = nominej.substring(nominej.indexOf(".") + 1);
        decimal = decimal.substring(0, 2);
        nominej = entera + "." + decimal;
      }
      if (nominej.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NOMINEJ + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tipoSaldo = processString(filtros.get(FILTER_TIPO_SALDO));
    if (StringUtils.isNotEmpty(tipoSaldo)) {
      int maxLength = 1;
      if (tipoSaldo.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_SALDO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nif_bic = processString(filtros.get(FILTER_NIF_BIC));
    if (StringUtils.isNotEmpty(nif_bic)) {
      int maxLength = 11;
      if (nif_bic.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NIF_BIC + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String entidad = processString(filtros.get(FILTER_ENTIDAD));
    if (StringUtils.isNotEmpty(entidad)) {
      int maxLength = 4;
      if (entidad.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_EMP_CON_VAL + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tipoCambio = processString(filtros.get(FILTER_TIPO_CAMBIO));
    if (StringUtils.isNotEmpty(tipoCambio)) {
      int maxLength = 1;
      if (tipoCambio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TIPO_CAMBIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String CCV = processString(filtros.get(FILTER_CCV));
    if (StringUtils.isNotEmpty(CCV)) {
      int maxLength = 17;
      if (CCV.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CCV + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
    }
    return filtersRight;

  }

  private boolean eliminarDesgloseListCheckFilters(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    String id = processString(filtros.get(FILTER_ID));
    if (StringUtils.isNotEmpty(id)) {
      int maxLength = 8;
      if (id.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ID + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
    }
    return filtersRight;

  }

  private boolean eliminarTitularListCheckFilters(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    String id = processString(filtros.get(FILTER_ID));
    if (StringUtils.isNotEmpty(id)) {
      int maxLength = 8;
      if (id.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_ID + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
    }
    return filtersRight;

  }

  /**
   * Metodo que recibe los datos para crear desgloses nuevos.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void createDesglose(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: createDesglose] Init");
    int numDesgloses = 0;
    final Map<String, Integer> resultados = new HashMap<String, Integer>();

    LOG.debug("[" + TAG + " :: createDesglose] Se intentarán crear un desglose");

    Map<String, String> filtros = webRequest.getFilters();

    String tipoReg = processString(filtros.get(FILTER_TIPO_REG));
    String refre = processString(filtros.get(FILTER_REFRE));
    BigDecimal titulos = processBigDecimal(filtros.get(FILTER_TITULOS));
    BigDecimal nominej = processBigDecimal(filtros.get(FILTER_NOMINEJ));
    String entidad = processString(filtros.get(FILTER_ENTIDAD));
    String empConVal = processString(filtros.get(FILTER_EMP_CON_VAL));
    String numCtoOficina = processString(filtros.get(FILTER_NUM_CTO_OFICINA));
    String numCtrVal = processString(filtros.get(FILTER_NUM_CTR_VAL));
    String numOrden = processString(filtros.get(FILTER_NUM_ORDEN));
    String codSV = processString(filtros.get(FILTER_COD_SV));
    String isin = processString(filtros.get(FILTER_ISIN));
    String desIsin = valoresBo.findDescIsinByIsin(isin);
    Date fejecucion = convertirStringADate(processString(filtros.get(FILTER_F_EJECUCION)));
    String bolsa = processString(filtros.get(FILTER_BOLSA));
    Character tipoOperacion = processCharacter(filtros.get(FILTER_TIPO_OPERACION));
    BigDecimal cambioOperacion = processBigDecimal(filtros.get(FILTER_CAMBIO_OPERACION));
    String nif_bic = processString(filtros.get(FILTER_NIF_BIC));
    Character tipoSaldo = processCharacter(filtros.get(FILTER_TIPO_SALDO));
    String refMod = processString(filtros.get(FILTER_REF_MOD));
    String segmento = processString(filtros.get(FILTER_SEGMENTO));
    String ordCom = processString(filtros.get(FILTER_ORD_COM));
    String tipoCambio = processString(filtros.get(FILTER_TIPO_CAMBIO));

    DesgloseRecordBean desglose = new DesgloseRecordBean(Integer.parseInt(tipoReg), refre, titulos, nominej, entidad,
        empConVal, numCtoOficina, numCtrVal, numOrden, codSV, isin, desIsin, fejecucion, bolsa, tipoOperacion,
        cambioOperacion, nif_bic, tipoSaldo, refMod, segmento, ordCom, tipoCambio);
    try {
      desglose.setAuditDate(new Date());
      desglose.setFechaProcesamiento(new Date());
      desglose.setPorcesado(0);
      desglose.setNucnfliq(new Short("1"));
      desgloseRecordBeanBo.save(desglose);
      numDesgloses++;
      resultados.put("desgloseCreated", numDesgloses);
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: createDesglose] Error dando de alta el desglose ");
      webResponse.setError("alta");
    }
    LOG.debug("[" + TAG + " :: createDesglose] Fin");
  }

  /**
   * Metodo que recibe los datos para crear el titular nuevo.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void createTitular(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: createTitular] Init");
    int numDesgloses = 0;
    final Map<String, Integer> resultados = new HashMap<String, Integer>();

    LOG.debug("[" + TAG + " :: createTitular] Se intentarán crear un desglose");

    Map<String, String> filtros = webRequest.getFilters();
    PartenonRecordBean partenonRecordBean = new PartenonRecordBean();
    partenonRecordBean.setAuditDate(new Date());
    partenonRecordBean.setVersion(new Long(0));
    partenonRecordBean.setBolsa(processString(filtros.get(FILTER_BOLSA)));
    partenonRecordBean.setCcv(processString(filtros.get(FILTER_CCV)));
    partenonRecordBean.setCodeMpr(processString(filtros.get(FILTER_CODEMPR)));

    partenonRecordBean.setDistrito(processString(filtros.get(FILTER_DISTRITO)));
    partenonRecordBean.setDomicili(processString(filtros.get(FILTER_DOMICILI)));
    partenonRecordBean.setIdSenc(processBigDecimal(filtros.get(FILTER_IDSENC)));
    partenonRecordBean.setIndNac(processCharacter(filtros.get(FILTER_IDNAC)));
    partenonRecordBean.setNacionalidad(processString(filtros.get(FILTER_NACLIDAD)));
    partenonRecordBean.setNifbic(processString(filtros.get(FILTER_NIFBIC)));
    partenonRecordBean.setNomapell(processString(filtros.get(FILTER_NOMAPELL)));
    String numOrden = processString(filtros.get(FILTER_NUM_ORDEN));
    if (numOrden != null) {
      if (numOrden.length() > 10) {
        numOrden = numOrden.substring(numOrden.length() - 10, numOrden.length());
      }
    }
    partenonRecordBean.setNumOrden(numOrden);
    partenonRecordBean.setPais(processString(filtros.get(FILTER_PAIS)));
    partenonRecordBean.setPaisResidencia(processString(filtros.get(FILTER_PAIS_RE)));
    partenonRecordBean.setParticip(processBigDecimal(filtros.get(FILTER_PARTICIP)));
    partenonRecordBean.setPoblacion(processString(filtros.get(FILTER_POBLACIO)));
    partenonRecordBean.setProvincia(processString(filtros.get(FILTER_PROVINCIA)));
    partenonRecordBean.setTipoDocumento(processCharacter(filtros.get(FILTER_TIPO_DOC)));
    partenonRecordBean.setTipoPersona(processCharacter(filtros.get(FILTER_TIP_PER)));
    partenonRecordBean.setTiptitu(processCharacter(filtros.get(FILTER_TIP_TITU)));
    partenonRecordBean.setTipoRegistro("20");
    Long id = Long.parseLong(processString(filtros.get(FILTER_ID_DESGLOSE_RECORD_BEAN)));
    DesgloseRecordBean desglose = desgloseRecordBeanBo.findById(id);
    partenonRecordBean.setDesgloseRecordBean(desglose);
    try {
      partenonRecordBeanBo.save(partenonRecordBean);
      numDesgloses++;
      resultados.put("desgloseCreated", numDesgloses);
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: createTitular] Error dando de alta el titular ");
      webResponse.setError("alta");
    }
    LOG.debug("[" + TAG + " :: createTitular] Fin");
  }

  /**
   * Metodo que recibe los datos para editar el titular.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void editarTitular(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: createTitular] Init");
    int numDesgloses = 0;
    final Map<String, Integer> resultados = new HashMap<String, Integer>();

    LOG.debug("[" + TAG + " :: createTitular] Se intentarán crear un desglose");

    Map<String, String> filtros = webRequest.getFilters();
    Long id = Long.parseLong(processString(filtros.get(FILTER_ID)));
    PartenonRecordBean partenonRecordBean = partenonRecordBeanBo.findById(id);
    partenonRecordBean.setAuditDate(new Date());
    partenonRecordBean.setBolsa(processString(filtros.get(FILTER_BOLSA)));
    partenonRecordBean.setCcv(processString(filtros.get(FILTER_CCV)));
    partenonRecordBean.setCodeMpr(processString(filtros.get(FILTER_CODEMPR)));
    partenonRecordBean.setDistrito(processString(filtros.get(FILTER_DISTRITO)));
    partenonRecordBean.setDomicili(processString(filtros.get(FILTER_DOMICILI)));
    partenonRecordBean.setIdSenc(processBigDecimal(filtros.get(FILTER_IDSENC)));
    partenonRecordBean.setIndNac(processCharacter(filtros.get(FILTER_IDNAC)));
    partenonRecordBean.setNacionalidad(processString(filtros.get(FILTER_NACLIDAD)));
    partenonRecordBean.setNifbic(processString(filtros.get(FILTER_NIFBIC)));
    partenonRecordBean.setNomapell(processString(filtros.get(FILTER_NOMAPELL)));
    partenonRecordBean.setPais(processString(filtros.get(FILTER_PAIS)));
    partenonRecordBean.setPaisResidencia(processString(filtros.get(FILTER_PAIS_RE)));
    partenonRecordBean.setParticip(processBigDecimal(filtros.get(FILTER_PARTICIP)));
    partenonRecordBean.setPoblacion(processString(filtros.get(FILTER_POBLACIO)));
    partenonRecordBean.setProvincia(processString(filtros.get(FILTER_PROVINCIA)));
    partenonRecordBean.setTipoDocumento(processCharacter(filtros.get(FILTER_TIPO_DOC)));
    partenonRecordBean.setTipoPersona(processCharacter(filtros.get(FILTER_TIP_PER)));
    partenonRecordBean.setTiptitu(processCharacter(filtros.get(FILTER_TIP_TITU)));
    try {
      partenonRecordBeanBo.save(partenonRecordBean);
      numDesgloses++;
      resultados.put("desgloseCreated", numDesgloses);
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: createTitular] Error editando el titular ");
      webResponse.setError("alta");
    }
    LOG.debug("[" + TAG + " :: editarTitular] Fin");
  }

  /**
   * Metodo que recibe los datos para editar un desglose.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void editarDesglose(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: editarDesglose] Init");
    final Map<String, Integer> resultados = new HashMap<String, Integer>();

    LOG.debug("[" + TAG + " :: editarDesglose] Se intentarán editar desglose");

    Map<String, String> filtros = webRequest.getFilters();

    Long id = Long.parseLong(processString(filtros.get(FILTER_ID)));

    Date fejecucion = convertirStringADate(processString(filtros.get(FILTER_F_EJECUCION)));
    Character tipoOperacion = processCharacter(filtros.get(FILTER_TIPO_OPERACION));
    String bolsa = processString(filtros.get(FILTER_BOLSA));
    String isin = processString(filtros.get(FILTER_ISIN));
    BigDecimal titulos = processBigDecimal(filtros.get(FILTER_TITULOS));
    BigDecimal nominej = processBigDecimal(filtros.get(FILTER_NOMINEJ));
    BigDecimal precio = processBigDecimal(filtros.get(FILTER_CAMBIO_OPERACION));
    Character tipoSaldo = processCharacter(filtros.get(FILTER_TIPO_SALDO));
    String ccv = processString(filtros.get(FILTER_CCV));
    String[] arrayCCV = ccv.split("/");

    String emprctva = arrayCCV[0];
    String centctva = arrayCCV[1];
    String dptectva = arrayCCV[2];

    String nif_bic = processString(filtros.get(FILTER_NIF_BIC));
    String entidad = processString(filtros.get(FILTER_ENTIDAD));
    Character tipoCambio = processCharacter(filtros.get(FILTER_TIPO_CAMBIO));

    try {

      DesgloseRecordBean desglose = desgloseRecordBeanBo.findById(id);
      desglose.setAuditDate(new Date());
      desglose.setFechaEjecucion(fejecucion);
      desglose.setTipoOperacion(TipoOperacion.getTipoOperacion(tipoOperacion.toString()));
      desglose.setCodigoBolsaEjecucion(bolsa);
      desglose.setCodigoValorISO(isin);
      String descripcion = valoresBo.findDescIsinByIsin(isin);
      desglose.setDescripcionValor(descripcion);
      desglose.setTitulosEjecutados(titulos);
      desglose.setNominalEjecutado(nominej);
      desglose.setCambioOperacion(precio);
      desglose.setTipoSaldo(tipoSaldo.toString());
      desglose.setBicTitular(nif_bic);
      desglose.setCodEmprl(entidad);
      desglose.setTipoCambio(TipoCambio.getTipoCambio(tipoCambio.toString()));
      desglose.setEmprCTVA(emprctva);
      desglose.setCentCTVA(centctva);
      desglose.setDpteCTVA(dptectva);

      desgloseRecordBeanBo.save(desglose);
      resultados.put("editarDesglose", 1);
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: editarDesglose] Error edtando el desglose ");
      webResponse.setError("editando");
    }

    LOG.debug("[" + TAG + " :: editarDesglose] Fin");
  }

  /**
   * Metodo que recibe los datos para eliminar un desglose.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void eliminarDesglose(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: eliminarDesglose] Init");
    final Map<String, Integer> resultados = new HashMap<String, Integer>();

    LOG.debug("[" + TAG + " :: eliminarDesglose] Se intentarán eliminar el  desglose");

    Map<String, String> filtros = webRequest.getFilters();

    Long id = Long.parseLong(processString(filtros.get(FILTER_ID)));

    try {
      desgloseRecordBeanBo.delete(id);
      resultados.put("eliminarDesglose", 1);
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: eliminarDesglose] Error edtando el desglose ");
      webResponse.setError("eliminando");
    }

    LOG.debug("[" + TAG + " :: eliminarDesglose] Fin");
  }

  /**
   * Metodo que recibe los datos para eliminar un desglose.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void eliminarTitular(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + TAG + " :: eliminarTitular] Init");
    final Map<String, Integer> resultados = new HashMap<String, Integer>();

    LOG.debug("[" + TAG + " :: eliminarTitular] Se intentarán eliminar el  titular");

    Map<String, String> filtros = webRequest.getFilters();

    Long id = Long.parseLong(processString(filtros.get(FILTER_ID)));

    try {
      desgloseRecordBeanBo.delete(id);
      resultados.put("eliminarTitular", 1);
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: eliminarTitular] Error eliminando el titular ");
      webResponse.setError("eliminando");
    }

    LOG.debug("[" + TAG + " :: eliminarTitular] Fin");
  }

  /**
   * @param filtros
   * @return
   */
  private String processString(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    return processedString;
  }

  private Character processCharacter(String filterString) {
    Character processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString.trim().charAt(0);
    }
    return processedString;
  }

  private BigDecimal processBigDecimal(String filterString) {
    BigDecimal processedBigDecimal = null;
    if (StringUtils.isNotEmpty(filterString)) {
      if (filterString.trim().equals("<null>")) {
        processedBigDecimal = new BigDecimal("-1");
      }
      else {
        processedBigDecimal = new BigDecimal(filterString);
      }
    }
    return processedBigDecimal;
  }

  private void getDesglosesHuerfanosCount(WebRequest webRequest, WebResponse webResponse) {
    Map<String, String> filters = webRequest.getFilters();
    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    List<String> isinVector = new ArrayList<String>();
    try {

      if (StringUtils.isNotEmpty(filters.get(FILTER_ISIN))) {
        String[] parts = filters.get(FILTER_ISIN).split(";");
        for (String part : parts) {
          isinVector.add(part);
        }
      }
    }
    catch (RuntimeException e1) {
      LOG.warn("[getDesglosesHuerfanosCount] Error inesperado al analizar filtro", e1);
    }

    int count = desgloseRecordBeanBo.countFindHuerfanosAllDTOPageable(fechaDesde, fechaHasta, isinVector);

    webResponse.setError("");
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("registros", count);
    webResponse.setResultados(result);
  }

  private WebResponseDataTable<DesgloseDTO> getDesglosesHuerfanosPageable(WebRequestForDataTable webRequest)
      throws SIBBACBusinessException {
    WebResponseDataTable<DesgloseDTO> webResponse = new WebResponseDataTable<DesgloseDTO>();

    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosPageable] Init");
    Map<String, String> filters = webRequest.getFilters();
    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    List<String> isinVector = new ArrayList<String>();

    try {

      if (StringUtils.isNotEmpty(filters.get(FILTER_ISIN))) {
        String[] parts = filters.get(FILTER_ISIN).split(";");
        for (String part : parts) {
          isinVector.add(part);
        }
      }

    }
    catch (RuntimeException e1) {
      LOG.warn("[getDesglosesHuerfanosPageable] Error inesperado al parsear filtro", e1);
    }

    List<DesgloseDTO> listaDesgloses = new ArrayList<DesgloseDTO>();

    /* Parámetros enviados por el objeto datatable */
    final int iDisplayStart = webRequest.getStart();
    final int iDisplayLength = webRequest.getLength();

    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosPageable] ");
    try {
      // Resultados paginados por el DataTable JQuery
      int count = desgloseRecordBeanBo.countFindHuerfanosAllDTOPageable(fechaDesde, fechaHasta, isinVector);
      listaDesgloses = desgloseRecordBeanBo.findAllHuerfanoDTOPageable(fechaDesde, fechaHasta, isinVector,
          iDisplayStart, iDisplayLength);

      webResponse.setColumns(webRequest.getColumns());
      webResponse.setDraw(Integer.valueOf(webRequest.getDraw()));
      webResponse.setRecordsTotal(webResponse.getRecordsTotal() + count);
      webResponse.setRecordsFiltered(webResponse.getRecordsFiltered() + count);
      webResponse.addData(new ArrayList<DesgloseDTO>(listaDesgloses));
      // return webResponse;
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: getDesglosesHuerfanosPageable] Excepción consulta " + e.getMessage());
    }
    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosPageable] Terminando consulta. Obtenidos " + listaDesgloses.size()
        + " registros");

    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosPageable] Fin ");
    return webResponse;
  }

  private void getDesglosesHuerfanosList(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosList] Init");
    Map<String, String> filters = webRequest.getFilters();
    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    List<String> isinVector = new ArrayList<String>();

    try {

      if (StringUtils.isNotEmpty(filters.get(FILTER_ISIN))) {
        String[] parts = filters.get(FILTER_ISIN).split(";");
        for (String part : parts) {
          isinVector.add(part);
        }
      }

    }
    catch (RuntimeException e1) {
      LOG.warn("[getDesglosesHuerfanosList] Error inesperado al parsear filtros", e1);
    }
    final Map<String, List<DesgloseDTO>> resultados = new HashMap<String, List<DesgloseDTO>>();
    List<DesgloseDTO> listaDesgloses = new ArrayList<DesgloseDTO>();

    // Obteniendo erroneos
    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosList] Comenzando consulta");
    try {
      listaDesgloses = desgloseRecordBeanBo.findHuerfanoDTOForService(fechaDesde, fechaHasta, isinVector);
      LOG.debug("[" + TAG + " :: getDesglosesHuerfanosList] Terminando consulta. Obtenidos " + listaDesgloses.size()
          + " registros");
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: getDesglosesHuerfanosList] Excepción consulta" + e.getMessage());
    }
    resultados.put(RESULT_DESGLOSES_LIST, listaDesgloses);
    webResponse.setResultados(resultados);
    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosList] Fin ");
  }

  @SuppressWarnings("unchecked")
  private void getDesglosesExport(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    LOG.debug("[" + TAG + " :: getDesglosesExport] Init");
    this.getDesglosesList(webRequest, webResponse);

    final Map<String, List<DesgloseDTO>> resultados = (Map<String, List<DesgloseDTO>>) webResponse.getResultados();
    List<DesgloseDTO> listaDesgloses = resultados.get(RESULT_DESGLOSES_LIST);
    resultados.put(RESULT_DESGLOSES_LIST, null);
    List<HashMap<String, Object>> listaDesglosesResult = new ArrayList<HashMap<String, Object>>();
    for (DesgloseDTO desgloseDTO : listaDesgloses) {
      listaDesglosesResult.add(desgloseDTO.getHashMapExport(null));
    }
    Map<String, List<HashMap<String, Object>>> result = new HashMap<String, List<HashMap<String, Object>>>();
    result.put(RESULT_DESGLOSES_LIST_EXPORT, listaDesglosesResult);
    webResponse.setResultados(result);
    LOG.debug("[" + TAG + " :: getDesglosesExport] Fin ");
  }

  @SuppressWarnings("unchecked")
  private void getDesglosesHuerfanosExport(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException {
    LOG.debug("[" + TAG + " :: getDesglosesHuerfanosExport] Init");
    this.getDesglosesHuerfanosList(webRequest, webResponse);

    final Map<String, List<DesgloseDTO>> resultados = (Map<String, List<DesgloseDTO>>) webResponse.getResultados();
    List<DesgloseDTO> listaDesgloses = resultados.get(RESULT_DESGLOSES_LIST);
    resultados.put(RESULT_DESGLOSES_LIST, null);
    List<HashMap<String, Object>> listaDesglosesResult = new ArrayList<HashMap<String, Object>>();
    for (DesgloseDTO desgloseDTO : listaDesgloses) {
      listaDesglosesResult.add(desgloseDTO.getHashMapExport(null));
    }
    Map<String, List<HashMap<String, Object>>> result = new HashMap<String, List<HashMap<String, Object>>>();
    result.put(RESULT_DESGLOSES_LIST_EXPORT, listaDesglosesResult);
    webResponse.setResultados(result);
    LOG.debug("[" + TAG + " :: getTitularesListExport] Fin ");
  }

  private void getBolsaMercadoList(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    LOG.debug("[" + TAG + " :: getBolsaMercadoList] Init");
    Map<String, String> filters = webRequest.getFilters();
    final String bolsaMercado = filters.get(FILTER_BOLSA_MERCADO);

    final Map<String, List<XasDTO>> resultados = new HashMap<String, List<XasDTO>>();
    List<XasDTO> listaBolsaMercado = null;

    // Obteniendo erroneos
    LOG.debug("[" + TAG + " :: getBolsaMercadoList] Comenzando consulta");
    try {
      listaBolsaMercado = tmct0xasBo.findDTOForService(bolsaMercado);
      LOG.debug("[" + TAG + " :: getBolsaMercadoList] Terminando consulta. Obtenidos " + listaBolsaMercado.size()
          + " registros");
    }
    catch (Exception e) {
      LOG.error("[" + TAG + " :: getBolsaMercadoList] Excepción consulta" + e.getMessage());
    }
    resultados.put(RESULT_BOLSA_MERCADO_LIST, listaBolsaMercado);
    webResponse.setResultados(resultados);
    LOG.debug("[" + TAG + " :: getBolsaMercadoList] Fin ");
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_GET_DESGLOSES_COUNT);
    commands.add(CMD_GET_DESGLOSES_PAGEABLE);
    commands.add(CMD_GET_DESGLOSES_LIST);
    commands.add(CMD_MARCAR_VALIDADO);
    commands.add(CMD_ALTA_DESGLOSE);
    commands.add(CMD_EDITAR_DESGLOSE);
    commands.add(CMD_DELETE_DESGLOSE);
    commands.add(CMD_GET_DESGLOSES_HUERFANOS_COUNT);
    commands.add(CMD_GET_DESGLOSES_HUERFANOS_PAGEABLE);
    commands.add(CMD_GET_DESGLOSES_HUERFANOS_LIST);
    commands.add(CMD_GET_DESGLOSES_EXPORT);
    commands.add(CMD_GET_DESGLOSES_HUERFANOS_EXPORT);
    commands.add(CMD_GET_LISTA_BOLSA_MERCADO);
    commands.add(CMD_ALTA_TITULAR);
    commands.add(CMD_EDITAR_TITULAR);
    commands.add(CMD_DELETE_TITULAR);

    return commands;
  }

  @Override
  public List<String> getFields() {

    return null;
  }

}
