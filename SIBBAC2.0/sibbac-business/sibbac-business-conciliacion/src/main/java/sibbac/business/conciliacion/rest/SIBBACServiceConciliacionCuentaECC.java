package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.conciliacion.database.bo.FicheroCuentaECCBo;
import sibbac.business.conciliacion.database.data.FicheroCuentaECCData;
import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceConciliacionCuentaECC implements SIBBACServiceBean {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceConciliacionCuentaECC.class);

  private static final String PRM_ID_CUENTA_LIQ = "idCuentaLiq";

  /** 
   * Comando consulta 
   */
  private static final String CMD_GET_MOVIMIENTOS_ECC = "getMovimientosECC";
  
  private static final String CMD_GET_CAMARAS_COMPENSACION = "getCamarasCompensacion";

  /**
   * Objeto de utilidades.
   */
  private ConciliacionUtil util = ConciliacionUtil.getInstance();
  
  @Autowired
  private Tmct0CuentaLiquidacionBo cuentaLiquidacionBo;
  
  @Autowired
  private FicheroCuentaECCBo ficheroCuentaECCBo;
  
  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    final WebResponse webResponse;
    final String command;
    
    LOG.debug("[process ]Processing a web request...");
    command = webRequest.getAction();
    LOG.debug("[process] Command.: [{}]", command);
    switch (command) {
      case CMD_GET_MOVIMIENTOS_ECC:
        webResponse = getMovimientosECC(webRequest);
        break;
      case CMD_GET_CAMARAS_COMPENSACION:
        webResponse = getCamarasCompensacion();
        break;
      default:
        webResponse = new WebResponse();
        webResponse.setError("Comando no reconocido");
    }
    return webResponse;
  }

  @Override
  public List<String> getAvailableCommands() {
    final List<String> commands;
    
    commands = new ArrayList<>();
    commands.add(CMD_GET_MOVIMIENTOS_ECC);
    commands.add(CMD_GET_CAMARAS_COMPENSACION);
    return commands;
  }

  @Override
  public List<String> getFields() {
    return null;
  }
  
  private WebResponse getMovimientosECC(WebRequest webRequest) {
    final WebResponse webResponse;
    final Map<String, Serializable> parametrosMovimientosECC;
    final Map<String, String> params;
    final List<FicheroCuentaECCData> movsFicheroCuentaECC;
    final String codCuentaLiq, isinFiltro, camaraCompensacion;
    final Long idCuentaLiq;
    final Date fechaDesde, fechaHasta;

    LOG.debug("[getMovimientosECC] Inicio...");
    webResponse = new WebResponse();
    params = webRequest.getFilters();
    if (!params.containsKey(PRM_ID_CUENTA_LIQ)) {
      LOG.debug("[getMovimientosECC] parámetro  [{}] cuenta de liquidacion not indicated ", PRM_ID_CUENTA_LIQ);
      webResponse.setError("La cuenta de liquidación es obligatoria");
      return webResponse;
    }
    parametrosMovimientosECC =  util.normalizeFilters(params);
    idCuentaLiq = (Long)  parametrosMovimientosECC.get(PRM_ID_CUENTA_LIQ);
    codCuentaLiq = cuentaLiquidacionBo.findById(idCuentaLiq).getCdCodigo().trim();
    util.normalizeDates(parametrosMovimientosECC);
    fechaDesde = (Date) parametrosMovimientosECC.get("fliquidacionDe");
    fechaHasta = (Date) parametrosMovimientosECC.get("fliquidacionA");
    isinFiltro = (String) parametrosMovimientosECC.get("isin");
    camaraCompensacion = (String) parametrosMovimientosECC.get("camaraCompensacion");
    movsFicheroCuentaECC = ficheroCuentaECCBo.findMovimientos(fechaDesde, fechaHasta, codCuentaLiq, isinFiltro, 
        camaraCompensacion);
    webResponse.setResultados( Collections.singletonMap(WebResponse.KEY_DATA, movsFicheroCuentaECC));
    return webResponse;
  }
  
  private WebResponse getCamarasCompensacion() {
    final WebResponse response;
    
    response = new WebResponse();
    response.setResultados(Collections.singletonMap(WebResponse.KEY_DATA, camaraCompensacionBo.labelValues()));
    return response;
  }

}