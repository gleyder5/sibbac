/**
 * # Norma43FileLoadException.java
 * # Fecha de creación: 30/11/2015
 */
package sibbac.business.conciliacion.exceptions;

/**
 * Se lanza si ocurre algún error al cargar los ficheros de norma43.
 * 
 * @author XI316153
 * @version 1.0
 * @since SIBBAC2.0
 */
public class Norma43FileLoadException extends Exception {

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTANTES ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 3705661353827800476L;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Constructor I. Genera una nueva excepción con el mensaje especificado.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   */
  public Norma43FileLoadException(String mensaje) {
    super(mensaje);
  } // Norma43FileLoadException

  /**
   * Constructor II. Genera una nueva excepción con el mensaje especificado y la excepción producida.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   * @param causa Causa por la que se ha lanzado la excepción.
   */
  public Norma43FileLoadException(String mensaje, Throwable causa) {
    super(mensaje, causa);
  } // Norma43FileLoadException

} // Norma43FileLoadException
