package sibbac.business.conciliacion.database.data;


import java.math.BigDecimal;
import java.sql.Date;

import sibbac.business.conciliacion.database.model.MovimientoCuentaVirtual;


public class MovimientoCuentaVirtualData {

	private MovimientoCuentaVirtual	movimientoCuentaVirtual;
	private String					camara;
	private long					idcuentaliq;
	private String					alias;
	private String					descrali;
	private String					isin;
	private char					sentidocuentavirtual;
	private char					sentido;
	private String					descripcionisin;
	private BigDecimal				titulos;
	private BigDecimal				efectivo;
	private Date					settlementdate;
	private String					cdCodigo;
	private String					sistemaLiq;

	public MovimientoCuentaVirtualData( MovimientoCuentaVirtual movimientoCuentaVirtual, String cdCodigo, BigDecimal titulos,
			BigDecimal efectivo ) {
		this.movimientoCuentaVirtual = movimientoCuentaVirtual;
		this.cdCodigo = cdCodigo;
		this.titulos = titulos;
		this.efectivo = efectivo;
	}

	public MovimientoCuentaVirtualData( String camara, String isin, Character sentidoCuentaVirtual, Character sentido, BigDecimal titulos,
			BigDecimal efectivo, String cdCodigo, String descripcionisin ) {
		this.camara = camara;
		this.isin = isin;
		this.sentidocuentavirtual = sentidoCuentaVirtual;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.cdCodigo = cdCodigo;
		this.descripcionisin = descripcionisin;
		initMovimientoCuentaVirtual();

	}

	public MovimientoCuentaVirtualData( String isin, BigDecimal titulos, String cdCodigo, Date settlementdate ) {
		this.isin = isin;
		this.titulos = titulos;
		this.cdCodigo = cdCodigo;
		this.settlementdate = settlementdate;
		this.efectivo = BigDecimal.ZERO;
	}

	public MovimientoCuentaVirtualData( String camara, long idcuentaliq, String alias, String descrali, String isin,
			String descripcionisin, char sentidocuentavirtual, char sentido, Date settlementdate, BigDecimal titulos, BigDecimal efectivo ) {
		this.camara = camara;
		this.idcuentaliq = idcuentaliq;
		this.alias = alias;
		this.isin = isin;
		this.descripcionisin = descripcionisin;
		this.sentidocuentavirtual = sentidocuentavirtual;
		this.sentido = sentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.descrali = descrali;
		if ( settlementdate != null ) {
			this.settlementdate = new java.sql.Date( settlementdate.getTime() );
		}
		initMovimientoCuentaVirtual();

	}

	public MovimientoCuentaVirtualData( String camara, long idcuentaliq, String alias, String descrali, String isin,
			String descripcionisin, char sentidocuentavirtual, char sentido, java.util.Date settlementdate, BigDecimal titulos,
			BigDecimal efectivo ) {
		this.camara = camara;
		this.idcuentaliq = idcuentaliq;
		this.alias = alias;
		this.isin = isin;
		this.descripcionisin = descripcionisin;
		this.sentidocuentavirtual = sentidocuentavirtual;
		this.sentido = sentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.descrali = descrali;
		if ( settlementdate != null ) {
			this.settlementdate = new java.sql.Date( settlementdate.getTime() );
		} else {
			this.settlementdate = null;
		}
		initMovimientoCuentaVirtual();

	}

	public MovimientoCuentaVirtualData( String camara, long idcuentaliq, String isin, String descripcionisin, char sentidocuentavirtual,
			char sentido, java.util.Date settlementdate, BigDecimal titulos, BigDecimal efectivo ) {
		this.camara = camara;
		this.idcuentaliq = idcuentaliq;
		this.alias = "-";
		this.isin = isin;
		this.descripcionisin = descripcionisin;
		this.sentidocuentavirtual = sentidocuentavirtual;
		this.sentido = sentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.descrali = "-";
		if ( settlementdate != null ) {
			this.settlementdate = new java.sql.Date( settlementdate.getTime() );
		} else {
			this.settlementdate = null;
		}
		initMovimientoCuentaVirtual();

	}

	private void initMovimientoCuentaVirtual() {
		this.movimientoCuentaVirtual = new MovimientoCuentaVirtual();
		this.movimientoCuentaVirtual.setCamara( camara );
		this.movimientoCuentaVirtual.setIdcuentaliq( idcuentaliq );
		this.movimientoCuentaVirtual.setAlias( alias );
		this.movimientoCuentaVirtual.setDescrali( descrali );
		this.movimientoCuentaVirtual.setIsin( isin );
		this.movimientoCuentaVirtual.setSentidocuentavirtual( sentidocuentavirtual );
		this.movimientoCuentaVirtual.setSentido( sentido );
		this.movimientoCuentaVirtual.setTitulos( titulos );
		this.movimientoCuentaVirtual.setEfectivo( efectivo );
		this.movimientoCuentaVirtual.setDescripcionisin( descripcionisin );
		this.movimientoCuentaVirtual.setSettlementdate( settlementdate );
	}

	//

	public BigDecimal getTitulos() {
		return titulos;
	}

	public MovimientoCuentaVirtual getMovimientoCuentaVirtual() {
		return movimientoCuentaVirtual;
	}

	public void setMovimientoCuentaVirtual( MovimientoCuentaVirtual movimientoCuentaVirtual ) {
		this.movimientoCuentaVirtual = movimientoCuentaVirtual;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public long getIdcuentaliq() {
		return idcuentaliq;
	}

	public void setIdcuentaliq( long idcuentaliq ) {
		this.idcuentaliq = idcuentaliq;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public char getSentidocuentavirtual() {
		return sentidocuentavirtual;
	}

	public void setSentidocuentavirtual( char sentidocuentavirtual ) {
		this.sentidocuentavirtual = sentidocuentavirtual;
	}

	public char getSentido() {
		return sentido;
	}

	public void setSentido( char sentido ) {
		this.sentido = sentido;
	}

	public String getDescripcionisin() {
		return descripcionisin;
	}

	public void setDescripcionisin( String descripcionisin ) {
		this.descripcionisin = descripcionisin;
	}

	public String getDescrali() {
		return descrali;
	}

	public void setDescrali( String descrali ) {
		this.descrali = descrali;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public String getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	
	public String getSistemaLiq() {
		return sistemaLiq;
	}

	
	public void setSistemaLiq( String sistemaLiq ) {
		this.sistemaLiq = sistemaLiq;
	}

}
