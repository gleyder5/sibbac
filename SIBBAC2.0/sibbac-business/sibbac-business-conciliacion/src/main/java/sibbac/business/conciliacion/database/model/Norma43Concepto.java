package sibbac.business.conciliacion.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 *
 * @author XI316153
 */
@Entity
@Table(name = DBConstants.CONCILIACION.TMCT0_NORMA43_CONCEPTO)
@XmlRootElement
public class Norma43Concepto extends ATable<Norma43Concepto> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -474903617082529622L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @XmlAttribute
  @Column(name = "DATO", nullable = false, length = 2)
  private Integer dato;

  @XmlAttribute
  @Column(name = "DESCRIPCION", nullable = true, length = 38)
  private String desc;

  @XmlAttribute
  @Column(name = "DESCADICIONAL", nullable = true, length = 38)
  private String descAdicional;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "MOVIMIENTO_ID", referencedColumnName = "ID")
  protected Norma43Movimiento movimiento;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public Norma43Concepto() {
  } // Norma43Concepto

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Norma43Concepto [dato=" + dato + ", desc=" + desc + ", descAdicional=" + descAdicional + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ gETTERS

  /**
   * @return the dato
   */
  public Integer getDato() {
    return dato;
  }

  /**
   * @return the desc
   */
  public String getDesc() {
    return desc;
  }

  /**
   * @return the descAdicional
   */
  public String getDescAdicional() {
    return descAdicional;
  }

  /**
   * @return the movimiento
   */
  public Norma43Movimiento getMovimiento() {
    return movimiento;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param dato the dato to set
   */
  public void setDato(Integer dato) {
    this.dato = dato;
  }

  /**
   * @param desc the desc to set
   */
  public void setDesc(String desc) {
    this.desc = desc;
  }

  /**
   * @param descAdicional the descAdicional to set
   */
  public void setDescAdicional(String descAdicional) {
    this.descAdicional = descAdicional;
  }

  /**
   * @param movimiento the movimiento to set
   */
  public void setMovimiento(Norma43Movimiento movimiento) {
    this.movimiento = movimiento;
  }

} // Norma43Concepto
