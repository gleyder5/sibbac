package sibbac.business.conciliacion.database.dao.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.conciliacion.database.model.FicheroCuentaECC;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;


public class FicheroCuentaEccSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( FicheroCuentaEccSpecifications.class );

	public static Specification< FicheroCuentaECC > fechaLiquidacionDesde( final Date fecha ) {
		return new Specification< FicheroCuentaECC >() {

			public Predicate toPredicate( Root< FicheroCuentaECC > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[FicheroCuentaEccSpecifications::fechaLiquidacionDesde] [settlementdate=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "settlementdate" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}
	
	public static Specification< FicheroCuentaECC > fechaLiquidacionHasta( final Date fecha ) {
		return new Specification< FicheroCuentaECC >() {

			public Predicate toPredicate( Root< FicheroCuentaECC > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[FicheroCuentaEccSpecifications::fechaLiquidacionHasta] [settlementdate=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "settlementdate" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}
	
	public static Specification< FicheroCuentaECC > isin( final String isin ) {
		return new Specification< FicheroCuentaECC >() {

			public Predicate toPredicate( Root< FicheroCuentaECC > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate isinPredicate = null;
				LOG.trace( "[FicheroCuentaEccSpecifications::isin] [isin=={}]", isin );
				if ( isin != null ) {
					isinPredicate = builder.equal( root.< String > get( "isin" ), isin );
				}
				return isinPredicate;
			}
		};
	}
	
	public static Specification< FicheroCuentaECC > codCuenta( final String codCuenta ) {
		return new Specification< FicheroCuentaECC >() {

			public Predicate toPredicate( Root< FicheroCuentaECC > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate codCuentaPredicate = null;
				LOG.trace( "[FicheroCuentaEccSpecifications::cuentaLiquidacion] [cuentaLiquidacion=={}]", codCuenta );
				if ( codCuenta != null ) {
					codCuentaPredicate = builder.equal( root.< Tmct0CuentaLiquidacion > get( "cuentaLiquidacion" ).<String> get("cdCodigo"), codCuenta );
				}
				return codCuentaPredicate;
			}
		};
	}
	
	public static Specification<FicheroCuentaECC> camaraCompensacion(final String camaraCompensacion) {
	  return new Specification<FicheroCuentaECC>() {
      @Override
      public Predicate toPredicate(Root<FicheroCuentaECC> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate camaraCompensacionPredicate = null;
        LOG.trace("[camaraCompensacion] [camaraCompensacion=={}]", camaraCompensacion);
        if (camaraCompensacion != null) {
          camaraCompensacionPredicate = builder.equal(root.<String>get("mercado"), camaraCompensacion);
        }
        return camaraCompensacionPredicate;
      }
	  };
	}
	
	public static Specification< FicheroCuentaECC > listadoFiltrado( final Date fechaDesde, final Date fechaHasta,  
	    final String codCuenta, final String isin, final String camaraCompensacion) {
		return new Specification< FicheroCuentaECC >() {

			public Predicate toPredicate( Root< FicheroCuentaECC > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< FicheroCuentaECC > specs = null;
				if ( fechaDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaLiquidacionDesde( fechaDesde ) );
					}

				}
				if ( fechaHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaLiquidacionHasta( fechaHasta ) );
					} else {
						specs = specs.and( fechaLiquidacionHasta( fechaHasta ) );
					}

				}
				
				if ( isin != null ) {
					if ( specs == null ) {
						specs = Specifications.where( isin( isin ) );
					} else {
						specs = specs.and( isin( isin ) );
					}

				}
				
				if ( codCuenta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( codCuenta( codCuenta ) );
					} else {
						specs = specs.and( codCuenta( codCuenta ) );
					}

				}
				if ( camaraCompensacion != null) {
				  if (specs == null) {
				    specs = Specifications.where( camaraCompensacion(camaraCompensacion));
				  }
				  else {
				    specs = specs.and( camaraCompensacion(camaraCompensacion));
				  }
				}
				
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}
}
