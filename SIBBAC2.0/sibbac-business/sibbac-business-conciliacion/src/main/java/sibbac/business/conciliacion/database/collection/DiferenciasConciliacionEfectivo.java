package sibbac.business.conciliacion.database.collection;


import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.business.conciliacion.database.model.Tmct0DiferenciaConciliacionEfectivo;


@XmlRootElement( name = "diferencias-efectivo" )
public class DiferenciasConciliacionEfectivo {

	private List< Tmct0DiferenciaConciliacionEfectivo >	diferenciasEfectivoList;

	public DiferenciasConciliacionEfectivo( List< Tmct0DiferenciaConciliacionEfectivo > diferenciasEfectivoList ) {
		this.diferenciasEfectivoList = diferenciasEfectivoList;
	}

	@XmlElement( name = "diferencia" )
	public List< Tmct0DiferenciaConciliacionEfectivo > getDiferenciasEfectivoList() {
		return diferenciasEfectivoList;
	}

	public void setDiferenciasEfectivoList( List< Tmct0DiferenciaConciliacionEfectivo > diferenciasEfectivoList ) {
		this.diferenciasEfectivoList = diferenciasEfectivoList;
	}

}
