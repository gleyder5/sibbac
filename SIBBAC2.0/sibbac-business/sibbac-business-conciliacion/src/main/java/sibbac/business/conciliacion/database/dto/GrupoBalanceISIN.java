package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class GrupoBalanceISIN {

	private String		alias;
	private String		descAli;
	private BigDecimal	titulos;
	private BigDecimal	efectivo;
	private String		fliquidacion;
	private boolean		valido;

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getDescAli() {
		return descAli;
	}

	public void setDescAli( String descAli ) {
		this.descAli = descAli;
	}

	public String getFliquidacion() {
		return fliquidacion;
	}

	public void setFliquidacion( String fliquidacion ) {
		this.fliquidacion = fliquidacion;
	}

	public boolean getValido() {
		return valido;
	}

	public void setValido( boolean valido ) {
		this.valido = valido;
	}

}
