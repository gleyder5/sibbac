package sibbac.business.conciliacion.database.dto;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.conciliacion.database.model.Norma43Descuadres;

/**
 * @author XI316153
 */
public class Norma43DescuadresDTO {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  private String cuenta;

  private String refFecha;

  private String refOrden;

  private String sentido;

  private Date tradeDate;

  private Date settlementDate;

  private BigDecimal importeNorma43;

  private BigDecimal importeSibbac;

  /** Estado contable del ALC. */
  private String tipoImporte;

  /** Estado de asignación del ALC. */
  private String nombreEstadoAsignacionAlc;

  /** Estado contable del ALC. */
  private String nombreEstadoContabilidadAlc;

  /** Estado de entrega/recepción del ALC. */
  private String nombreEstadoEntrecAlc;

  /** Identificador de la orden a la que pertence el ALC. */
  private String nuorden;

  /** Identificador del booking al que pertenece el ALC. */
  private String nbooking;

  /** Identificador del ALO al que pertenece el ALC. */
  private String nucnfclt;

  /** Identificador del ALC. */
  private Short nucnfliq;

  /** Diferencia entre lo devengado y lo cobrado. */
  private BigDecimal diferencia;

  /** Comentario explicativo sobre porque se ha generado el descuadre. */
  private String comentario;

  /** Indica si el registro ha sido procesado. */
  private Boolean procesado;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Constructor por defecto.
   */
  public Norma43DescuadresDTO() {
  } // Norma43DescuadresDTO

  /**
   * @param cuenta
   * @param refFecha
   * @param refOrden
   * @param sentido
   * @param tradeDate
   * @param settlementDate
   * @param importeNorma43
   * @param importeSibbac
   * @param tipoImporte
   * @param nombreEstadoAsignacionAlc
   * @param nombreEstadoContabilidadAlc
   * @param nombreEstadoEntrecAlc
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   */
  public Norma43DescuadresDTO(String cuenta,
                              String refFecha,
                              String refOrden,
                              String sentido,
                              Date tradeDate,
                              Date settlementDate,
                              BigDecimal importeNorma43,
                              BigDecimal importeSibbac,
                              String tipoImporte,
                              String nombreEstadoAsignacionAlc,
                              String nombreEstadoContabilidadAlc,
                              String nombreEstadoEntrecAlc,
                              String nuorden,
                              String nbooking,
                              String nucnfclt,
                              Short nucnfliq,
                              BigDecimal diferencia,
                              String comentario,
                              Boolean procesado) {
    this.cuenta = cuenta;
    this.refFecha = refFecha;
    this.refOrden = refOrden;
    this.sentido = sentido;
    this.tradeDate = tradeDate;
    this.settlementDate = settlementDate;
    this.importeNorma43 = importeNorma43;
    this.importeSibbac = importeSibbac;
    this.tipoImporte = tipoImporte;
    this.nombreEstadoAsignacionAlc = nombreEstadoAsignacionAlc;
    this.nombreEstadoContabilidadAlc = nombreEstadoContabilidadAlc;
    this.nombreEstadoEntrecAlc = nombreEstadoEntrecAlc;
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
  } // Norma43DescuadresDTO

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODFOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Norma43DescuadresDTO [cuenta=" + cuenta + ", refFecha=" + refFecha + ", refOrden=" + refOrden + ", sentido=" + sentido + ", tradeDate="
           + tradeDate + ", settlementDate=" + settlementDate + ", importeNorma43=" + importeNorma43 + ", importeSibbac=" + importeSibbac
           + ", tipoImporte=" + tipoImporte + ", nombreEstadoAsignacionAlc=" + nombreEstadoAsignacionAlc + ", nombreEstadoEntrecAlc="
           + nombreEstadoEntrecAlc + ", nombreEstadoContabilidadAlc=" + nombreEstadoContabilidadAlc + ", nuorden=" + nuorden + ", nbooking="
           + nbooking + ", nucnfclt=" + nucnfclt + ", nucnfliq=" + nucnfliq + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Genera un objeto de la entidad <code>Norma43Descuadres</code> a partir del DTO actual.
   * 
   * @return <code>Norma43Descuadres - </code>Entidad Norma43Descuadres.
   */
  public Norma43Descuadres getDescuadreNorma43() {
    Norma43Descuadres norma43Descuadres = new Norma43Descuadres();

    norma43Descuadres.setCuenta(cuenta);
    norma43Descuadres.setRefFecha(refFecha);
    norma43Descuadres.setRefOrden(refOrden);
    norma43Descuadres.setSentido(sentido);
    norma43Descuadres.setTradeDate(tradeDate);
    norma43Descuadres.setSettlementDate(settlementDate);
    norma43Descuadres.setImporteNorma43(importeNorma43);
    norma43Descuadres.setImporteSibbac(importeSibbac);
    norma43Descuadres.setTipoImporte(tipoImporte);
    norma43Descuadres.setNombreEstadoContabilidadAlc(nombreEstadoContabilidadAlc);
    norma43Descuadres.setNombreEstadoAsignacionAlc(nombreEstadoAsignacionAlc);
    norma43Descuadres.setNombreEstadoEntrecAlc(nombreEstadoEntrecAlc);
    norma43Descuadres.setNuorden(nuorden);
    norma43Descuadres.setNbooking(nbooking);
    norma43Descuadres.setNucnfclt(nucnfclt);
    norma43Descuadres.setNucnfliq(nucnfliq);

    norma43Descuadres.setDiferencia(diferencia);
    norma43Descuadres.setComentario(comentario);
    norma43Descuadres.setProcesado(procesado);

    return norma43Descuadres;
  } // getDescuadresNorma43

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the cuenta
   */
  public String getCuenta() {
    return cuenta;
  }

  /**
   * @return the refFecha
   */
  public String getRefFecha() {
    return refFecha;
  }

  /**
   * @return the refOrden
   */
  public String getRefOrden() {
    return refOrden;
  }

  /**
   * @return the sentido
   */
  public String getSentido() {
    return sentido;
  }

  /**
   * @return the tradeDate
   */
  public Date getTradeDate() {
    return tradeDate;
  }

  /**
   * @return the settlementDate
   */
  public Date getSettlementDate() {
    return settlementDate;
  }

  /**
   * @return the importeNorma43
   */
  public BigDecimal getImporteNorma43() {
    return importeNorma43;
  }

  /**
   * @return the importeSibbac
   */
  public BigDecimal getImporteSibbac() {
    return importeSibbac;
  }

  /**
   * @return the tipoImporte
   */
  public String getTipoImporte() {
    return tipoImporte;
  }

  /**
   * @return the nombreEstadoAsignacionAlc
   */
  public String getNombreEstadoAsignacionAlc() {
    return nombreEstadoAsignacionAlc;
  }

  /**
   * @return the nombreEstadoContabilidadAlc
   */
  public String getNombreEstadoContabilidadAlc() {
    return nombreEstadoContabilidadAlc;
  }

  /**
   * @return the nombreEstadoEntrecAlc
   */
  public String getNombreEstadoEntrecAlc() {
    return nombreEstadoEntrecAlc;
  }

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @return the diferencia
   */
  public BigDecimal getDiferencia() {
    return diferencia;
  }

  /**
   * @return the comentario
   */
  public String getComentario() {
    return comentario;
  }

  /**
   * @return the procesado
   */
  public Boolean getProcesado() {
    return procesado;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param cuenta the cuenta to set
   */
  public void setCuenta(String cuenta) {
    this.cuenta = cuenta;
  }

  /**
   * @param refFecha the refFecha to set
   */
  public void setRefFecha(String refFecha) {
    this.refFecha = refFecha;
  }

  /**
   * @param refOrden the refOrden to set
   */
  public void setRefOrden(String refOrden) {
    this.refOrden = refOrden;
  }

  /**
   * @param sentido the sentido to set
   */
  public void setSentido(String sentido) {
    this.sentido = sentido;
  }

  /**
   * @param tradeDate the tradeDate to set
   */
  public void setTradeDate(Date tradeDate) {
    this.tradeDate = tradeDate;
  }

  /**
   * @param settlementDate the settlementDate to set
   */
  public void setSettlementDate(Date settlementDate) {
    this.settlementDate = settlementDate;
  }

  /**
   * @param importeNorma43 the importeNorma43 to set
   */
  public void setImporteNorma43(BigDecimal importeNorma43) {
    this.importeNorma43 = importeNorma43;
  }

  /**
   * @param importeSibbac the importeSibbac to set
   */
  public void setImporteSibbac(BigDecimal importeSibbac) {
    this.importeSibbac = importeSibbac;
  }

  /**
   * @param tipoImporte the tipoImporte to set
   */
  public void setTipoImporte(String tipoImporte) {
    this.tipoImporte = tipoImporte;
  }

  /**
   * @param nombreEstadoAsignacionAlc the nombreEstadoAsignacionAlc to set
   */
  public void setNombreEstadoAsignacionAlc(String nombreEstadoAsignacionAlc) {
    this.nombreEstadoAsignacionAlc = nombreEstadoAsignacionAlc;
  }

  /**
   * @param nombreEstadoContabilidadAlc the nombreEstadoContabilidadAlc to set
   */
  public void setNombreEstadoContabilidadAlc(String nombreEstadoContabilidadAlc) {
    this.nombreEstadoContabilidadAlc = nombreEstadoContabilidadAlc;
  }

  /**
   * @param nombreEstadoEntrecAlc the nombreEstadoEntrecAlc to set
   */
  public void setNombreEstadoEntrecAlc(String nombreEstadoEntrecAlc) {
    this.nombreEstadoEntrecAlc = nombreEstadoEntrecAlc;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @param diferencia the diferencia to set
   */
  public void setDiferencia(BigDecimal diferencia) {
    this.diferencia = diferencia;
  }

  /**
   * @param comentario the comentario to set
   */
  public void setComentario(String comentario) {
    this.comentario = comentario;
  }

  /**
   * @param procesado the procesado to set
   */
  public void setProcesado(Boolean procesado) {
    this.procesado = procesado;
  }

} // Norma43DescuadresDTO

