package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;


public class MovimientoCuentaVirtualDTO {

	private long		id;
	private String		alias;
	private String		descrAli;
	private String		isin;
	private String		descrIsin;
	private char		sentido;
	private String		camara;
	private long		clientenumsec;
	private String		clientecdbrocli;
	private String		ncliente;
	private String		cdcodigoccomp;
	private long		idcuentaliq;
	private String		cdcodigocliq;
	private String		tradedate;
	private String		settlementdate;
	private BigDecimal	precio;
	private BigDecimal	titulos;
	private BigDecimal	efectivo;
	private BigDecimal	corretaje;
	private char		sentidocuentavirtual;
	private BigDecimal	neto;
	private String		sistemaLiq;
	private String		cdCodigo;

	public String getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescrIsin() {
		return descrIsin;
	}

	public void setDescrIsin( String descrIsin ) {
		this.descrIsin = descrIsin;
	}

	public char getSentido() {
		return sentido;
	}

	public void setSentido( char sentido ) {
		this.sentido = sentido;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	public long getClientenumsec() {
		return clientenumsec;
	}

	public void setClientenumsec( long clientenumsec ) {
		this.clientenumsec = clientenumsec;
	}

	public String getClientecdbrocli() {
		return clientecdbrocli;
	}

	public void setClientecdbrocli( String clientecdbrocli ) {
		this.clientecdbrocli = clientecdbrocli;
	}

	public String getNcliente() {
		return ncliente;
	}

	public void setNcliente( String ncliente ) {
		this.ncliente = ncliente;
	}

	public String getCdcodigoccomp() {
		return cdcodigoccomp;
	}

	public void setCdcodigoccomp( String cdcodigoccomp ) {
		this.cdcodigoccomp = cdcodigoccomp;
	}

	public long getIdcuentaliq() {
		return idcuentaliq;
	}

	public void setIdcuentaliq( long idcuentaliq ) {
		this.idcuentaliq = idcuentaliq;
	}

	public String getCdcodigocliq() {
		return cdcodigocliq;
	}

	public void setCdcodigocliq( String cdcodigocliq ) {
		this.cdcodigocliq = cdcodigocliq;
	}

	public String getTradedate() {
		return tradedate;
	}

	public void setTradedate( String tradedate ) {
		this.tradedate = tradedate;
	}

	public String getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( String settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio( BigDecimal precio ) {
		this.precio = precio;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public BigDecimal getCorretaje() {
		return corretaje;
	}

	public void setCorretaje( BigDecimal corretaje ) {
		this.corretaje = corretaje;
	}

	public char getSentidocuentavirtual() {
		return sentidocuentavirtual;
	}

	public void setSentidocuentavirtual( char sentidocuentavirtual ) {
		this.sentidocuentavirtual = sentidocuentavirtual;
	}

	public BigDecimal getNeto() {
		return neto;
	}

	public void setNeto( BigDecimal neto ) {
		this.neto = neto;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getDescrAli() {
		return descrAli;
	}

	public void setDescrAli( String descrAli ) {
		this.descrAli = descrAli;
	}

	
	public String getSistemaLiq() {
		return sistemaLiq;
	}

	
	public void setSistemaLiq( String sistemaLiq ) {
		this.sistemaLiq = sistemaLiq;
	}

	@Override
	public String toString() {
		return "MovimientoCuentaVirtualDTO [id=" + id + ", alias=" + alias + ", descrAli=" + descrAli + ", isin=" + isin + ", descrIsin="
				+ descrIsin + ", sentido=" + sentido + ", camara=" + camara + ", clientenumsec=" + clientenumsec + ", clientecdbrocli="
				+ clientecdbrocli + ", ncliente=" + ncliente + ", cdcodigoccomp=" + cdcodigoccomp + ", idcuentaliq=" + idcuentaliq
				+ ", cdcodigocliq=" + cdcodigocliq + ", tradedate=" + tradedate + ", settlementdate=" + settlementdate + ", precio="
				+ precio + ", titulos=" + titulos + ", efectivo=" + efectivo + ", corretaje=" + corretaje + ", sentidocuentavirtual="
				+ sentidocuentavirtual + ", neto=" + neto + "]";
	}

}
