package sibbac.business.conciliacion.database.dto;


import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;


public class BalanceMovimientoCuentaVirtualDTO {

	private String						alias;
	private String						descralias;
	private List< GrupoBalanceCliente >	grupoBalanceClienteList;
	private BigDecimal					totalEfectivos;

	public List< GrupoBalanceCliente > getGrupoBalanceClienteList() {
		return grupoBalanceClienteList;
	}

	public void addGrupoBalanceCliente( GrupoBalanceCliente grupo ) {
		if ( this.grupoBalanceClienteList == null ) {
			grupoBalanceClienteList = new LinkedList< GrupoBalanceCliente >();
		}
		this.grupoBalanceClienteList.add( grupo );
	}

	public void setGrupoBalanceClienteList( List< GrupoBalanceCliente > grupoBalanceClienteList ) {
		this.grupoBalanceClienteList = grupoBalanceClienteList;
	}

	public BigDecimal getTotalEfectivos() {
		return totalEfectivos;
	}

	public void setTotalEfectivos( BigDecimal totalEfectivos ) {
		this.totalEfectivos = totalEfectivos;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getDescralias() {
		return descralias;
	}

	public void setDescralias( String descralias ) {
		this.descralias = descralias;
	}

}
