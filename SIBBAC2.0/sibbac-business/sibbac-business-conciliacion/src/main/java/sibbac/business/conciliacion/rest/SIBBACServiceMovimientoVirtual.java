package sibbac.business.conciliacion.rest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.conciliacion.util.ConciliacionUtil;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0SaldoInicialCuentaBo;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.model.Tmct0SaldoInicialCuenta;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceMovimientoVirtual implements SIBBACServiceBean {

  private static final String CONCILIACION_TIPOS_MOVIMIENTOS_PARA_FILTRAR_FECHA_ACTUALIZACION = "conciliacion.tipos.movimientos.para.filtrar.fecha.actualizacion";
  private static final String TAG = SIBBACServiceMovimientoVirtual.class.getName();
  private static final Logger LOG = LoggerFactory.getLogger(TAG);
  // Util
  private ConciliacionUtil util = ConciliacionUtil.getInstance();
  // COMANDOS
  private static final String CMD_GET_MOVIMIENTOS_VIRTUALES = "getMovimientosVirtuales";
  private static final String CMD_GET_MOVIMIENTOS_ESPEJO = "getMovimientosEspejo";
  private static final String CMD_GET_FECHA_LIQUIDACION_PREVIA = "getFechaPreviaLiquidacion";
  private static final String CMD_GET_ULTIMA_FECHA_ACTUALIZACION = "getUpdateLastTime";
  // RESULTADOS
  private static final String RESULT_CUENTAS_VIRTUALES = "result_cuentas_virtuales";
  private static final String RESULT_LAST_TIME_UPDATE = "result_last_time_update";

  private static final String[] OPERACIONES1 = new String[] { "CPA", "VTA" };

  private static final String[] OPERACIONES2 = new String[] { "ENT", "REC" };

  private static final String[] OPERACIONES_RESTO = new String[] { "APJ", "APU", "BCO", "BDV", "BFA", "BIN", "BPC",
      "BPJ", "BPU", "CAR", "CCP", "CON", "CPU", "DCA", "DIV", "DPA", "EDE", "EDV", "ENC", "ENL", "ENP", "FDV", "GDE",
      "IGU", "INC", "INP", "INT", "MAN", "PAJ", "RCP", "REL", "REP", "RPA", "RPE", "SUS", "TRA", "ACO", "ACP", "ADE",
      "ADP", "ADS", "ADV", "AFA", "AFV", "AMT", "APA", "APC", "API", "PDF" };

  // SERVICIOS
  @Autowired
  private Tmct0MovimientoCuentaAliasBo movimientoCuentaAliasBo;
  @Autowired
  private GestorPeriodicidades gestorPeriodicidades;
  @Autowired
  private Tmct0SaldoInicialCuentaBo saldoCuentaInicialBo;

  // propiedades
  @Value("${" + CONCILIACION_TIPOS_MOVIMIENTOS_PARA_FILTRAR_FECHA_ACTUALIZACION + "}")
  private String[] operaciones;

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    WebResponse response = new WebResponse();
    Map<String, Object> resultMap = new HashMap<String, Object>();
    Map<String, String> filters = webRequest.getFilters();
    Map<String, Serializable> serializedFilters = new HashMap<String, Serializable>();
    // Normalizar filtros
    if (filters != null) {
      serializedFilters = util.normalizeFilters(filters);
    }
    final String command = webRequest.getAction();
    switch (command) {
      case CMD_GET_MOVIMIENTOS_VIRTUALES:

        final List<MovimientoCuentaAliasData> resultList;
        try {
          resultList = getMovimientosVirtuales(serializedFilters);
          resultMap.put(RESULT_CUENTAS_VIRTUALES, resultList);
          response.setResultados(resultMap);
        }
        catch (Exception e) {
          response.setError(e.getMessage());
          LOG.error(e.getMessage(), e);
        }
        break;
      case CMD_GET_MOVIMIENTOS_ESPEJO:

        List<MovimientoCuentaAliasData> resultEspejo = new LinkedList<MovimientoCuentaAliasData>();
        try {
          resultEspejo = getMovimientosEspejo(serializedFilters);
        }
        catch (Exception e) {
          response.setError(e.getMessage());
          LOG.error(e.getMessage(), e);
        }
        resultMap.put(RESULT_CUENTAS_VIRTUALES, resultEspejo);
        response.setResultados(resultMap);
        break;
      case CMD_GET_FECHA_LIQUIDACION_PREVIA:
        List<String> fechas = getFechaPreviaLiquidacion();
        resultMap.put("result_fecha_previa_laboral", fechas);
        response.setResultados(resultMap);
        break;
      case CMD_GET_ULTIMA_FECHA_ACTUALIZACION:
        Date lastTime = getUpdateLastTime();
        resultMap.put(RESULT_LAST_TIME_UPDATE, FormatDataUtils.convertDateTimeToString(lastTime));
        response.setResultados(resultMap);
        break;
      default:
        break;
    }
    return response;
  }

  /**
   * Devuelve la fecha del ultimo MovimientoCuentaAlias modificado cuya
   * operacion corresponda con una de las que se pasan por parametros, obtenidas
   * del fichero de configuracion sibbac-config -> main.resources ->
   * sibbac.conciliacion.properties
   * 
   * @return
   */
  private Date getUpdateLastTime() {
    Date fecha = movimientoCuentaAliasBo.findLastUpdateDateTimeInOperaciones(operaciones);
    return fecha;
  }

  private List<MovimientoCuentaAliasData> getListaMovimientosVirtuales(Map<String, Serializable> serializedFilters) {
    if (serializedFilters.get("fcontratacionDe") == null) {
      serializedFilters.put("fcontratacionDe", movimientoCuentaAliasBo.findMinTradedate());
    }
    Date fcontratacionDe = (Date) serializedFilters.get("fcontratacionDe");
    Date fcontratacionA = (Date) serializedFilters.get("fcontratacionA");

    // para los movimientos de mercado, se deja la fecha de contratación
    List<MovimientoCuentaAliasData> resultMovMercado = movimientoCuentaAliasBo.findAll(serializedFilters, "",
        OPERACIONES1);
    // se establece la fecha de registro usada para la ordenación de los
    // registros
    for (MovimientoCuentaAliasData mov : resultMovMercado) {
      mov.setFechaRegistro(FormatDataUtils.convertStringToDate(mov.getFcontratacion()));
      mov.setPrioridadOperacion(getPrioridadOperacion(mov.getCdoperacion()));
    }

    // para los movimientos de cliente, se toma la fecha de liquidación
    if (fcontratacionDe != null) {
      serializedFilters.put("fliquidacionDe", fcontratacionDe);
      serializedFilters.remove("fcontratacionDe");
    }
    if (fcontratacionA != null) {
      serializedFilters.put("fliquidacionA", fcontratacionA);
      serializedFilters.remove("fcontratacionA");
    }
    List<MovimientoCuentaAliasData> resultMovCliente = movimientoCuentaAliasBo.findAll(serializedFilters, "",
        OPERACIONES2);
    for (MovimientoCuentaAliasData mov : resultMovCliente) {
      mov.setFechaRegistro(FormatDataUtils.convertStringToDate(mov.getFliquidacion()));
      mov.setPrioridadOperacion(getPrioridadOperacion(mov.getCdoperacion()));
    }
    // ordenarPorEntregaRecepcion(resultMovCliente);

    // para los movimientos de cliente, se toma la fecha de liquidación
    List<MovimientoCuentaAliasData> resultMovAuxiliar = movimientoCuentaAliasBo.findAll(serializedFilters, "",
        OPERACIONES_RESTO);
    for (MovimientoCuentaAliasData mov : resultMovAuxiliar) {
      mov.setFechaRegistro(FormatDataUtils.convertStringToDate(mov.getFliquidacion()));
      mov.setPrioridadOperacion(getPrioridadOperacion(mov.getCdoperacion()));
    }
    // ordenarPorEntregaRecepcion(resultMovCliente);

    /*
     * Fin de la ñapa
     */

    List<MovimientoCuentaAliasData> result = new ArrayList<MovimientoCuentaAliasData>();

    result.addAll(resultMovCliente);
    result.addAll(resultMovMercado);
    result.addAll(resultMovAuxiliar);
    ordenarColumnas(result);
    return result;
  }

  private List<MovimientoCuentaAliasData> getMovimientosVirtuales(Map<String, Serializable> serializedFilters)
      throws Exception {

    if (serializedFilters.get("fcontratacionDe") == null) {
      serializedFilters.put("fcontratacionDe", FormatDataUtils.convertStringToDate("20160101"));
    }
    Date fcontratacionDe = (Date) serializedFilters.get("fcontratacionDe");
    Date fcontratacionA = (Date) serializedFilters.get("fcontratacionA");

    List<MovimientoCuentaAliasData> result = this.getListaMovimientosVirtuales(serializedFilters);

    Calendar cal = Calendar.getInstance();
    fcontratacionDe = fcontratacionDe != null ? fcontratacionDe : cal.getTime();

    Date fechaFinal = fcontratacionA != null ? fcontratacionA : obtenerFuturo();
    List<MovimientoCuentaAliasData> finalResult = new LinkedList<MovimientoCuentaAliasData>();
    MovimientoCuentaAliasData next = null;
    BigDecimal titulosFinal = BigDecimal.ZERO;
    BigDecimal efectivoFinal = BigDecimal.ZERO;
    int total = result.size();
    String isin = null;
    String cdaliass = null;
    String cdcodigocuentaliq = null;
    MovimientoCuentaAliasData mv = null;

    for (int i = 0; i < total; i++) {
      mv = result.get(i);
      isin = mv.getIsin();
      cdaliass = mv.getCdalias();
      cdcodigocuentaliq = mv.getCdcodigocuentaliq();
      if (!checkIfExistInListResult(finalResult, isin, cdaliass, cdcodigocuentaliq)) {
        // Saldo Inicial
        MovimientoCuentaAliasData mData = obtenerSaldoInicial(fcontratacionDe, isin, cdaliass, cdcodigocuentaliq,
            "Sald.Inicial");
        finalResult.add(mData);
        titulosFinal = titulosFinal.add(mData.getTitulos());
        efectivoFinal = efectivoFinal.add(mData.getEfectivo());
        if (i + 1 < total) {
          next = result.get(i + 1);
          if (!checkIfExistInListResult(finalResult, next.getIsin(), next.getCdalias(), next.getCdcodigocuentaliq())) {
            // Saldo Final
            titulosFinal = titulosFinal.add(mv.getTitulos());
            efectivoFinal = efectivoFinal.add(mv.getEfectivo());
            MovimientoCuentaAliasData mFinal = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
                cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
            finalResult.add(mv);
            finalResult.add(mFinal);
            // Reinicio los titulos y efectivo finales
            titulosFinal = BigDecimal.ZERO;
            efectivoFinal = BigDecimal.ZERO;
            continue;
          }
        }
        else {
          if (i + 1 == total) {
            // Solamente hay un registro asi que hay que incluirlo
            finalResult.add(mv);
            titulosFinal = titulosFinal.add(mv.getTitulos());
            efectivoFinal = efectivoFinal.add(mv.getEfectivo());
          }
          // No hay mas registros se añade el saldo final
          MovimientoCuentaAliasData mFinal = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
              cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
          finalResult.add(mFinal);
          // Reinicio los titulos y efectivo finales
          titulosFinal = BigDecimal.ZERO;
          efectivoFinal = BigDecimal.ZERO;
          continue;
        }
      }
      else {
        if (i + 1 < total) {
          // Si existe y el siguiente no existe, se añade la linea con
          // el saldo final
          next = result.get(i + 1);
          if (!checkIfExistInListResult(finalResult, next.getIsin(), next.getCdalias(), next.getCdcodigocuentaliq())) {
            // Saldo Final
            titulosFinal = titulosFinal.add(mv.getTitulos());
            efectivoFinal = efectivoFinal.add(mv.getEfectivo());
            MovimientoCuentaAliasData mData = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
                cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
            // Añade el movimiento acutal a la lista, luego el saldo
            // final
            // y pasa a la siguiente iteracion.
            finalResult.add(mv);
            finalResult.add(mData);
            // Reinicio los titulos y efectivo finales
            titulosFinal = BigDecimal.ZERO;
            efectivoFinal = BigDecimal.ZERO;
            continue;
          }
        }
        else {
          // si es el ultimo, se añade el saldo final
          next = mv;
          // Saldo Final
          titulosFinal = titulosFinal.add(mv.getTitulos());
          efectivoFinal = efectivoFinal.add(mv.getEfectivo());
          MovimientoCuentaAliasData mData = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
              cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
          // Añade el movimiento acutal a la lista, luego el saldo
          // final
          // y pasa a la siguiente iteracion.
          finalResult.add(mv);
          finalResult.add(mData);
          // Reinicio los titulos y efectivo finales
          titulosFinal = BigDecimal.ZERO;
          efectivoFinal = BigDecimal.ZERO;
          continue;
        }

      }
      titulosFinal = titulosFinal.add(mv.getTitulos());
      efectivoFinal = efectivoFinal.add(mv.getEfectivo());
      finalResult.add(mv);
    }
    return finalResult;
  }

  private void ordenarColumnas(List<MovimientoCuentaAliasData> result) {
    ordenarPorTitulos(result);
    ordenarPorCdOperacion(result);
    ordenarPorFechaRegistro(result);
    ordenarPorIsin(result);
    ordenarPorAlias(result);
  }

  /**
   * @param fechaInicial
   * @param isin
   * @param cdaliass
   * @param cdcodigocuentaliq
   * @param title
   * @return
   */
  private MovimientoCuentaAliasData obtenerSaldoInicial(Date fcontratacionDe, String isin, String cdaliass,
      String cdcodigocuentaliq, String title) {

    //
    List<Object[]> titulosNoLiq;
    List<Object[]> titulosLiquidados;
    Date fechaInicial = obtenerDiaAnterior(fcontratacionDe);
    MovimientoCuentaAliasData movSaldo = new MovimientoCuentaAliasData();

    movSaldo.setIsin(isin);
    movSaldo.setCdalias(cdaliass);
    movSaldo.setCdoperacion(title);
    movSaldo.setTitulos(BigDecimal.ZERO);
    movSaldo.setEfectivo(BigDecimal.ZERO);
    movSaldo.setCdcodigocuentaliq(cdcodigocuentaliq);

    Tmct0SaldoInicialCuenta saldoInicial = saldoCuentaInicialBo
        .findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(isin, cdcodigocuentaliq,
            cdaliass, fechaInicial);
    if (saldoInicial != null && saldoInicial.getTitulos() != null) {
      movSaldo.setTitulos(saldoInicial.getTitulos());
      movSaldo.setEfectivo(saldoInicial.getImefectivo());
      fechaInicial = saldoInicial.getFecha();
    }
    else {
      fechaInicial = null;
    }
    // titulos no liquidados
    titulosNoLiq = movimientoCuentaAliasBo
        .findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionNoLiquidados(fcontratacionDe, isin,
            cdaliass, cdcodigocuentaliq);

    this.addTitulosAndEfectivo(titulosNoLiq, movSaldo);

    // titulos liquidados entre fecha saldo inicial y la fecha de contratacion
    // que buscamos en pantalla
    if (fechaInicial == null) {
      titulosLiquidados = movimientoCuentaAliasBo
          .findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
              fcontratacionDe, isin, cdaliass, cdcodigocuentaliq, OPERACIONES1);
      this.addTitulosAndEfectivo(titulosLiquidados, movSaldo);
      titulosLiquidados = movimientoCuentaAliasBo
          .findSumTitulosBySettlementdateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
              fcontratacionDe, isin, cdaliass, cdcodigocuentaliq, OPERACIONES2);
      this.addTitulosAndEfectivo(titulosLiquidados, movSaldo);
      titulosLiquidados = movimientoCuentaAliasBo
          .findSumTitulosBySettlementdateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
              fcontratacionDe, isin, cdaliass, cdcodigocuentaliq, OPERACIONES_RESTO);
      this.addTitulosAndEfectivo(titulosLiquidados, movSaldo);

    }
    else {
      titulosLiquidados = movimientoCuentaAliasBo
          .findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
              fechaInicial, fcontratacionDe, isin, cdaliass, cdcodigocuentaliq, OPERACIONES1);
      this.addTitulosAndEfectivo(titulosLiquidados, movSaldo);
      titulosLiquidados = movimientoCuentaAliasBo
          .findSumTitulosBySettlementdateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
              fechaInicial, fcontratacionDe, isin, cdaliass, cdcodigocuentaliq, OPERACIONES2);
      this.addTitulosAndEfectivo(titulosLiquidados, movSaldo);
      titulosLiquidados = movimientoCuentaAliasBo
          .findSumTitulosBySettlementdateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
              fechaInicial, fcontratacionDe, isin, cdaliass, cdcodigocuentaliq, OPERACIONES_RESTO);
      this.addTitulosAndEfectivo(titulosLiquidados, movSaldo);
    }

    return movSaldo;
  }

  private void addTitulosAndEfectivo(List<Object[]> titulosSaldo, MovimientoCuentaAliasData mov) {
    if (titulosSaldo != null && !titulosSaldo.isEmpty()) {
      if (titulosSaldo.get(0).length > 0 && titulosSaldo.get(0)[0] != null) {
        mov.setTitulos(mov.getTitulos().add((BigDecimal) titulosSaldo.get(0)[0]));
      }
      if (titulosSaldo.get(0).length > 1 && titulosSaldo.get(0)[1] != null) {
        mov.setEfectivo(mov.getEfectivo().add((BigDecimal) titulosSaldo.get(0)[1]));
      }
    }
  }

  /**
   * Ordena por titulos de menos a mas
   * @param resultMovMercado
   */
  private void ordenarPorTitulos(List<MovimientoCuentaAliasData> resultMovMercado) {
    Collections.sort(resultMovMercado, new Comparator<MovimientoCuentaAliasData>() {

      @Override
      public int compare(MovimientoCuentaAliasData mov1, MovimientoCuentaAliasData mov2) {

        int lastCmp = 0;

        lastCmp = mov1.getTitulos().compareTo(mov2.getTitulos());

        return lastCmp;
      }
    });
  }

  /**
   * Ordena por fechas Ascendente
   * @param result
   */
  private void ordenarPorFechaRegistro(List<MovimientoCuentaAliasData> result) {
    Collections.sort(result, new Comparator<MovimientoCuentaAliasData>() {

      @Override
      public int compare(MovimientoCuentaAliasData mov1, MovimientoCuentaAliasData mov2) {
        int lastCmp = mov1.getFechaRegistro().compareTo(mov2.getFechaRegistro());
        return lastCmp;
      }
    });
  }

  private void ordenarPorIsin(List<MovimientoCuentaAliasData> result) {
    Collections.sort(result, new Comparator<MovimientoCuentaAliasData>() {

      @Override
      public int compare(MovimientoCuentaAliasData mov1, MovimientoCuentaAliasData mov2) {
        int lastCmp = mov1.getIsin().compareTo(mov2.getIsin());
        return lastCmp;
      }
    });
  }

  private void ordenarPorAlias(List<MovimientoCuentaAliasData> result) {
    Collections.sort(result, new Comparator<MovimientoCuentaAliasData>() {

      @Override
      public int compare(MovimientoCuentaAliasData mov1, MovimientoCuentaAliasData mov2) {
        int lastCmp = mov1.getCdalias().compareTo(mov2.getCdalias());
        return lastCmp;
      }
    });
  }

  private void ordenarPorCdOperacion(List<MovimientoCuentaAliasData> result) {
    Collections.sort(result, new Comparator<MovimientoCuentaAliasData>() {

      @Override
      public int compare(MovimientoCuentaAliasData mov1, MovimientoCuentaAliasData mov2) {
        int lastCmp = 0;

        lastCmp = mov1.getPrioridadOperacion().compareTo(mov2.getPrioridadOperacion());
        return lastCmp;
      }
    });
  }

  private Integer getPrioridadOperacion(final String cdope) {
    Integer priority = 0;
    switch (cdope) {
      case "CPA":
        priority = 3;
        break;
      case "VTA":
        priority = 4;
        break;
      case "REC":
        priority = 1;
        break;
      case "ENT":
        priority = 2;
        break;
    }
    return priority;
  }

  private List<MovimientoCuentaAliasData> getMovimientosEspejo(Map<String, Serializable> serializedFilters)
      throws Exception {

    List<MovimientoCuentaAliasData> result = movimientoCuentaAliasBo.findAll(serializedFilters, null);
    Calendar cal = Calendar.getInstance();

    Date fechaInicial = (serializedFilters.get("fliquidacionDe") != null) ? (Date) obtenerDiaAnterior((Date) serializedFilters
        .get("fliquidacionDe")) : obtenerDiaAnterior(cal.getTime());
    Date fechaFinal = (serializedFilters.get("fliquidacionA") != null) ? (Date) (Date) serializedFilters
        .get("fliquidacionA") : obtenerFuturo();
    List<MovimientoCuentaAliasData> finalResult = new LinkedList<MovimientoCuentaAliasData>();
    MovimientoCuentaAliasData next = null;
    BigDecimal titulosFinal = BigDecimal.ZERO;
    BigDecimal efectivoFinal = BigDecimal.ZERO;
    String isinAnterior = new String();
    String aliasAnterior = new String();
    MovimientoCuentaAliasData saldoInicialAnterior = new MovimientoCuentaAliasData();
    for (int i = 0; i < result.size(); i++) {
      MovimientoCuentaAliasData mv = result.get(i);
      String isin = mv.getIsin().trim();
      String cdaliass = mv.getCdalias().trim();
      String cdcodigocuentaliq = mv.getCdcodigocuentaliq();
      if (!checkIfExistInListEspejo(finalResult, isin, cdcodigocuentaliq)) {
        // Saldo Inicial
        MovimientoCuentaAliasData mData = obtenerSaldoInicial(fechaInicial, isin, cdaliass, cdcodigocuentaliq,
            "Sald.Inicial");
        saldoInicialAnterior = mData;
        finalResult.add(mData);
        titulosFinal = titulosFinal.add(mData.getTitulos());
        efectivoFinal = efectivoFinal.add(mData.getEfectivo());
        if (i + 1 < result.size()) {
          next = result.get(i + 1);
          if (!checkIfExistInListEspejo(finalResult, next.getIsin(), next.getCdcodigocuentaliq())) {
            // Saldo Final
            titulosFinal = titulosFinal.add(mv.getTitulos());
            efectivoFinal = efectivoFinal.add(mv.getEfectivo());
            MovimientoCuentaAliasData mFinal = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
                cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
            finalResult.add(mv);
            finalResult.add(mFinal);
            isinAnterior = isin;
            aliasAnterior = cdaliass;
            // Reinicio los titulos y efectivo finales
            titulosFinal = BigDecimal.ZERO;
            efectivoFinal = BigDecimal.ZERO;
            continue;
          }
        }
        else {
          if (i + 1 == result.size()) {
            // Solamente hay un registro asi que hay que incluirlo
            finalResult.add(mv);
            isinAnterior = isin;
            aliasAnterior = cdaliass;

            titulosFinal = titulosFinal.add(mv.getTitulos());
            efectivoFinal = efectivoFinal.add(mv.getEfectivo());
          }
          // No hay mas registros se añade el saldo final
          MovimientoCuentaAliasData mFinal = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
              cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
          finalResult.add(mFinal);
          // Reinicio los titulos y efectivo finales
          titulosFinal = BigDecimal.ZERO;
          efectivoFinal = BigDecimal.ZERO;
          continue;
        }
      }
      else {
        if (!cdaliass.equals(aliasAnterior) && isin.equals(isinAnterior)) {
          // Si tiene distinto alias y mismo isin que el anterior hay
          // que meter el saldo inicial
          MovimientoCuentaAliasData saldoAux = obtenerSaldoInicial(fechaInicial, isin, cdaliass, cdcodigocuentaliq,
              "Sald.Inicial");
          saldoInicialAnterior.setEfectivo(saldoInicialAnterior.getEfectivo().add(saldoAux.getEfectivo()));
          saldoInicialAnterior.setTitulos(saldoInicialAnterior.getTitulos().add(saldoAux.getTitulos()));
          saldoInicialAnterior.setCdalias("");
          saldoInicialAnterior.setDescali("");
          efectivoFinal = efectivoFinal.add(saldoAux.getEfectivo());
          titulosFinal = titulosFinal.add(saldoAux.getTitulos());
        }

        if (i + 1 < result.size()) {
          // Si existe y el siguiente no existe, se añade la linea con
          // el saldo final
          next = result.get(i + 1);
          if (!checkIfExistInListEspejo(finalResult, next.getIsin(), next.getCdcodigocuentaliq())) {
            // Saldo Final
            titulosFinal = titulosFinal.add(mv.getTitulos());
            efectivoFinal = efectivoFinal.add(mv.getEfectivo());
            MovimientoCuentaAliasData mData = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
                cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
            mData.setCdalias(saldoInicialAnterior.getCdalias());
            mData.setDescali(saldoInicialAnterior.getDescali());

            // Añade el movimiento acutal a la lista, luego el saldo
            // final
            // y pasa a la siguiente iteracion.
            finalResult.add(mv);
            isinAnterior = isin;
            aliasAnterior = cdaliass;
            finalResult.add(mData);
            // Reinicio los titulos y efectivo finales
            titulosFinal = BigDecimal.ZERO;
            efectivoFinal = BigDecimal.ZERO;
            continue;
          }
        }
        else {
          // si es el ultimo, se añade el saldo final
          next = mv;
          // Saldo Final
          titulosFinal = titulosFinal.add(mv.getTitulos());
          efectivoFinal = efectivoFinal.add(mv.getEfectivo());
          MovimientoCuentaAliasData mData = calcularSaldoFinalParaPantalla(fechaFinal, mv, isin, cdaliass,
              cdcodigocuentaliq, "Sald.Final", titulosFinal, efectivoFinal);
          mData.setCdalias(saldoInicialAnterior.getCdalias());
          mData.setDescali(saldoInicialAnterior.getDescali());
          // Añade el movimiento acutal a la lista, luego el saldo
          // final
          // y pasa a la siguiente iteracion.
          finalResult.add(mv);
          isinAnterior = isin;
          aliasAnterior = cdaliass;
          finalResult.add(mData);
          // Reinicio los titulos y efectivo finales
          titulosFinal = BigDecimal.ZERO;
          efectivoFinal = BigDecimal.ZERO;
          continue;
        }

      }
      titulosFinal = titulosFinal.add(mv.getTitulos());
      efectivoFinal = efectivoFinal.add(mv.getEfectivo());
      isinAnterior = isin;
      aliasAnterior = cdaliass;
      finalResult.add(mv);
    }

    return finalResult;
  }

  private MovimientoCuentaAliasData calcularSaldoFinalParaPantalla(Date fecha, MovimientoCuentaAliasData mv,
      String isin, String cdaliass, String cdcodigocuentaliq, String sentido, BigDecimal titulosFinal,
      BigDecimal efectivoFinal) {
    MovimientoCuentaAliasData mData = new MovimientoCuentaAliasData(cdcodigocuentaliq, cdaliass, null, isin,
        mv.getDescrIsin(), mv.getCamara(), sentido, "", ' ', null, titulosFinal, efectivoFinal, mv.getCorretaje(),
        mv.getAuditUser(), mv.getAuditDate());
    return mData;
  }

  private boolean checkIfExistInListResult(List<MovimientoCuentaAliasData> aVerificar, String isin, String alias,
      String codCuenta) {
    boolean exist = false;
    for (MovimientoCuentaAliasData data : aVerificar) {
      if (data.getIsin().trim().equals(isin) && data.getCdalias().trim().equals(alias)
          && data.getCdcodigocuentaliq().trim().equals(codCuenta)) {
        exist = true;
        break;
      }
    }
    return exist;
  }

  private boolean checkIfExistInListEspejo(List<MovimientoCuentaAliasData> aVerificar, String isin, String codCuenta) {
    boolean exist = false;
    for (MovimientoCuentaAliasData data : aVerificar) {
      if (data.getIsin().equals(isin) && data.getCdcodigocuentaliq().equals(codCuenta)) {
        exist = true;
        break;
      }

    }
    return exist;
  }

  private Date obtenerDiaAnterior(Date time) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(time);
    cal.add(Calendar.DATE, -1 * 1);
    java.sql.Date dateDe = new java.sql.Date(cal.getTimeInMillis());
    while (!gestorPeriodicidades.isLaborable(dateDe)) {
      cal.setTimeInMillis(dateDe.getTime());
      cal.add(Calendar.DATE, -1);
      dateDe = new java.sql.Date(cal.getTimeInMillis());
    }
    return cal.getTime();
  }

  private Date obtenerFuturo() {
    Calendar cal = Calendar.getInstance();
    cal.set(2200, 11, 11);
    return cal.getTime();
  }

  /**
   * Devuelve la fecha previa en String
   * 
   * @return
   */
  private List<String> getFechaPreviaLiquidacion() {
    List<String> fechas = new LinkedList<String>();
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1 * 1);
    java.sql.Date dateDe = new java.sql.Date(cal.getTimeInMillis());
    while (!gestorPeriodicidades.isLaborable(dateDe)) {
      cal.setTimeInMillis(dateDe.getTime());
      cal.add(Calendar.DATE, -1);
      dateDe = new java.sql.Date(cal.getTimeInMillis());
    }
    String fechaStr = util.parseDateToString(cal.getTime());
    fechas.add(fechaStr);
    return fechas;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new LinkedList<String>();
    commands.add(CMD_GET_MOVIMIENTOS_VIRTUALES);
    commands.add(CMD_GET_FECHA_LIQUIDACION_PREVIA);
    commands.add(CMD_GET_ULTIMA_FECHA_ACTUALIZACION);
    return commands;
  }

  @Override
  public List<String> getFields() {
    // TODO Auto-generated method stub
    return null;
  }

}
