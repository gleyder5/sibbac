package sibbac.business.accounting.enums;

public enum SubtipoTipoPlantCobrosBarridosEnum {
	
	SUBTIPO_CANON("CANON"),
	SUBTIPO_CORRETAJE("CORRETAJE");
	
	private String subtipoTipoPlantCobrosBarridos;

	private SubtipoTipoPlantCobrosBarridosEnum(String subtipoTipoPlantCobrosBarridos) {
		this.subtipoTipoPlantCobrosBarridos = subtipoTipoPlantCobrosBarridos;
	}

	public String getSubtipoTipoPlantCobrosBarridos() {
		return this.subtipoTipoPlantCobrosBarridos;
	}

	public void setSubtipoTipoPlantCobrosBarridos(
			String subtipoTipoPlantCobrosBarridos) {
		this.subtipoTipoPlantCobrosBarridos = subtipoTipoPlantCobrosBarridos;
	}
	
}
