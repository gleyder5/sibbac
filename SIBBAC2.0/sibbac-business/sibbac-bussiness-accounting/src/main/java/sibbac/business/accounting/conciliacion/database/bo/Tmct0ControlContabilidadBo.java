package sibbac.business.accounting.conciliacion.database.bo;

import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaOurDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dto.DatosApuntesExcelDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurApuntesDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ApuntesAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ControlContabilidadAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0PantallaConciliacionOURAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

/**
 *	Bo para control de contabilidad. 
 */
@Service
public class Tmct0ControlContabilidadBo {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ControlContabilidadBo.class);

	@Autowired
	Tmct0PantallaConciliacionOURAssembler tmct0PantallaConciliacionOURAssembler;

	@Autowired
	Tmct0ContaApuntesAuditDao tmct0ContaApuntesAuditDao;

	@Autowired
	Tmct0ContaApuntesPndtsDao tmct0ContaApuntesPndtsDao;

	@Autowired
	Tmct0ContaPartidasCasadasDAO tmct0ContaPartidasCasadasDAO;

	@Autowired
	Tmct0ContaOurDAO tmct0ContaOurDAO;

	@Autowired
	private Tmct0ApuntesAssembler tmct0ApuntesAssembler;

	@Autowired
	Tmct0ControlContabilidadAssembler tmct0ControlContabilidadAssembler;
	
	@Autowired
	Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;
	
	@Autowired
	Tmct0UsersBo tmct0UsersBo;

	@Autowired
	private SendMail sendMail;

	@Value("${accounting.contabilidad.folder.in}")
	private String folderOut;

	@Transactional(
			propagation = Propagation.REQUIRED,
			isolation = Isolation.DEFAULT,
			readOnly = true)
	public void aprobarApuntes (Map<String, Object> paramsObject) throws Exception {
		LOG.info("INICIO - BO - aprobarApuntes");
		Map<String, Object> result = new HashMap<String, Object>();
		SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyy");
		SimpleDateFormat formatHour = new SimpleDateFormat("HHmmss");
		List<GrillaTLMDTO> partidas = new ArrayList<GrillaTLMDTO>();
		String file = null;

		try {
			List<LinkedHashMap> listaApuntesAprobar = (List<LinkedHashMap>) paramsObject.get("listApuntesAprobados");
			List<BigInteger> listIdsApuntesAprobar = new ArrayList<BigInteger>();
			
			String concepto = null;

			if (CollectionUtils.isNotEmpty(listaApuntesAprobar)) {

				// Por cada apunte a aprobar guardamos su id en una lista para posteriormente eliminarlos
				// Se guarda tambien un listado de las entidades apuntesAudit
				for (int i = 0; i < listaApuntesAprobar.size(); i++) {
					
					String fechaActual = formatDate.format(new Date());
					String horaActual = formatHour.format(new Date());
					List<Tmct0ContaPartidasCasadas> listasPartidasCasadas = new ArrayList<Tmct0ContaPartidasCasadas>();
					List<Tmct0ContaPartidasCasadas> listaPartidasCasadas = new ArrayList<Tmct0ContaPartidasCasadas>();
					List<Tmct0ContaOur> listaOur = new ArrayList<Tmct0ContaOur>();
					List<Tmct0ContaOur> listasOur = new ArrayList<Tmct0ContaOur>();

					// Se guardan todos los id's para realizar posteriormente el borrado de la tabla TMCT0_CONTA_APUNTES_PNDTS
					LinkedHashMap map = listaApuntesAprobar.get(i);
					map.put("auditUser", paramsObject.get("auditUser"));
					Integer id = map.get("id") != null ? (Integer) map.get("id") : null;
					listIdsApuntesAprobar.add(new BigInteger(String.valueOf(id)));

					concepto = (String) map.get("concepto");
					// Se mapean los objetos de tipo Tmct0ContaApuntesPndts a Tmct0ContaApuntesAudit
					Tmct0ContaApuntesAudit contaApunteAudit = tmct0ControlContabilidadAssembler.getTmct0ContaApuntesAudit(map,true);
					
					this.tmct0ContaApuntesAuditDao.save(contaApunteAudit);
					
					// Recuperamos las partidas casadas cuyos ID_APUNTES_PNDTS sean igual a los apuntes aprobados de la tabla TMCT0_CONTA_APUNTES_PNDTS
					listaPartidasCasadas = tmct0ContaPartidasCasadasDAO.findById(BigInteger.valueOf((Integer) id));

					// Por cada partida casada, eliminamos su ID_APUNTES_PNDTS y le asignamos el ID_APUNTES_AUDIT
					for (int j = 0; j < listaPartidasCasadas.size(); j++) {
						listaPartidasCasadas.get(j).setApuntesPndts(null);
						Tmct0ContaApuntesAudit contaApuntesAudit = new Tmct0ContaApuntesAudit();
						contaApuntesAudit.setId(contaApunteAudit.getId());
						listaPartidasCasadas.get(j).setApuntesAudit(contaApuntesAudit);
						listasPartidasCasadas.add(listaPartidasCasadas.get(j));
					}
					
					this.tmct0ContaPartidasCasadasDAO.save(listaPartidasCasadas);

					// Recuperamos las our cuyos ID_APUNTES_PNDTS sean igual a los apuntes aprobados de la tabla TMCT0_CONTA_APUNTES_PNDTS
					listaOur = tmct0ContaOurDAO.findById(BigInteger.valueOf((Integer) id));
					
					//Se recupera un lista de auxiliares bancarios
					List<String> listAux = new ArrayList<String> ();
					for (int j = 0; j < listaOur.size(); j++) {
						Tmct0ContaOur our = listaOur.get(j);
						if(!listAux.contains(our.getAuxBancario())){
							listAux.add(our.getAuxBancario());
						}
					}
					
					for(String aux : listAux){

						String codigoPlatilla1 = "";
						BigDecimal total1 = BigDecimal.ZERO;
						for (int j = 0; j < listaOur.size(); j++) {
							Tmct0ContaOur our = listaOur.get(j);
							if((!"".equals(codigoPlatilla1) || codigoPlatilla1.equals(our.getCodPlantilla())) && aux.equals(our.getAuxBancario())){
								codigoPlatilla1 = our.getCodPlantilla();
								total1 = total1.add(our.getImporte());
							}
						}
						
						String codigoPlatilla2 = "";
						BigDecimal total2 = BigDecimal.ZERO;
						for (int j = 0; j < listaOur.size(); j++) {
							Tmct0ContaOur our = listaOur.get(j);
							if(!codigoPlatilla1.equals(our.getCodPlantilla()) && aux.equals(our.getAuxBancario())){
								codigoPlatilla2 = our.getCodPlantilla();
								total2 = total2.add(our.getImporte());
							}
						}
						
						for (int j = 0; j < listaOur.size(); j++) {
							Tmct0ContaOur our = listaOur.get(j);
							if(aux.equals(our.getAuxBancario())){
								if(total2.compareTo(BigDecimal.ZERO)!=0){
									if(total1.compareTo(total2) == 1){
										our.setCodPlantilla(codigoPlatilla1);
									}else{
										our.setCodPlantilla(codigoPlatilla2);
									}
								}
								our.setApuntesPndts(null);
								Tmct0ContaApuntesAudit contaApuntesAudit = new Tmct0ContaApuntesAudit();
								contaApuntesAudit.setId(contaApunteAudit.getId());
								our.setApuntesAudit(contaApuntesAudit);
								listasOur.add(our);
							}
						}
					}
				
					this.tmct0ContaOurDAO.save(listaOur);

					// Se generará un fichero Excel del tipo apuntes_sibbac.xls para su posterior tratamiento en la contabilidad de apuntes manuales.
					String titulo = "apunte_sibbac_" + fechaActual + "_" + horaActual + ".xlsx";
	
					partidas = tmct0ApuntesAssembler.getGrillaTLMDTOs(listasPartidasCasadas);
	
					List<Tmct0ContaOurApuntesDTO> camposClave = tmct0PantallaConciliacionOURAssembler.getTmct0ContaOurApuntesDTO(listasOur);
					List<DatosApuntesExcelDTO> filasGrilla = AccountingHelper.generarFilasGrilla(partidas, camposClave, concepto);
					XSSFWorkbook wb = AccountingHelper.generarXLSApuntesSIBBAC(filasGrilla);
	
					file = folderOut + titulo;
					FileOutputStream stream = new FileOutputStream(file);
					wb.write(stream);
					wb.close();
					stream.close();
				
				}
				
				// Se eliminarán todos los apuntes seleccionados de la tabla TMCT0_CONTA_APUNTES_PNDTS.
				this.tmct0ContaApuntesPndtsDao.deleteFromId(listIdsApuntesAprobar);	

			} else {
				throw new SIBBACBusinessException("Error: No hay apuntes a aprobar.");
			}
		} catch (SIBBACBusinessException e) {
			LOG.error("Error metodo aprobarApuntes -  " + e.getMessage(), e);
			throw(e);
		} catch (Exception e) {
			LOG.error("Error metodo aprobarApuntes -  " + e.getMessage(), e);
			result.put("status", "KO");
			throw(e);
		}
	}

	@Transactional(
			propagation = Propagation.REQUIRED,
			isolation = Isolation.DEFAULT,
			readOnly = true)
	public void rechazarApuntes (Map<String, Object> paramsObject) throws Exception {
		LOG.info("INICIO - SERVICIO - rechazarApuntes");

		try {
			List<LinkedHashMap> listaApuntesRechazar = (List<LinkedHashMap>) paramsObject.get("listApuntesRechazados");
			List<BigInteger> listIdsApuntesAprobar = new ArrayList<BigInteger>();
			List<Tmct0ContaApuntesAudit> listaApuntesAudit = new ArrayList<Tmct0ContaApuntesAudit>();
			List<BigInteger> listIdsGin = new ArrayList<BigInteger>();
			List<BigDecimal> listImporte = new ArrayList<BigDecimal>();
			List<Tmct0ContaPartidasPndts> listaPartidasPndts = new ArrayList<Tmct0ContaPartidasPndts>();

			if (CollectionUtils.isNotEmpty(listaApuntesRechazar)) {

				// Se eliminarán todos los apuntes seleccionados de la tabla TMCT0_CONTA_APUNTES_PNDTS.
				for (int i = 0; i < listaApuntesRechazar.size(); i++) {

					// Se guardan todos los id's para realizar posteriormente el borrado de la tabla TMCT0_CONTA_APUNTES_PNDTS
					LinkedHashMap map = listaApuntesRechazar.get(i);
					map.put("auditUser", paramsObject.get("auditUser"));
					Integer id = map.get("id") != null ? (Integer) map.get("id") : null;
					listIdsApuntesAprobar.add(new BigInteger(String.valueOf(id)));

					// Se mapean los objetos de tipo Tmct0ContaApuntesPndts a Tmct0ContaApuntesAudit
					Tmct0ContaApuntesAudit contaApunteAudit = tmct0ControlContabilidadAssembler.getTmct0ContaApuntesAudit(map,false);
					listaApuntesAudit.add(contaApunteAudit);
				}

				// El sistema guardará registro histórico de la aprobación de los apuntes por parte del usuario en tabla TMCT0_CONTA_APUNTES_AUDIT.
				this.tmct0ContaApuntesAuditDao.save(listaApuntesAudit);		

				// Por cada partida casada, eliminamos su ID_APUNTES_PNDTS y le asignamos el ID_APUNTES_AUDIT
				for (int i = 0; i < listaApuntesRechazar.size(); i++) {

					LinkedHashMap map = listaApuntesRechazar.get(i);
					Integer id = map.get("id") != null ? (Integer) map.get("id") : null;

					// Recuperamos las partidas casadas cuyos ID_APUNTES_PNDTS sean igual a los apuntes aprobados de la tabla TMCT0_CONTA_APUNTES_PNDTS
					List<Tmct0ContaPartidasCasadas> listaPartidasCasadas = tmct0ContaPartidasCasadasDAO.findById(BigInteger.valueOf((Integer) id));

					// Por cada partida casada, eliminamos su ID_APUNTES_PNDTS y le asignamos el ID_APUNTES_AUDIT
					for (int j = 0; j < listaPartidasCasadas.size(); j++) {
						listaPartidasCasadas.get(j).setApuntesPndts(null);
						Tmct0ContaApuntesAudit contaApuntesAudit = new Tmct0ContaApuntesAudit();
						contaApuntesAudit.setId(listaApuntesAudit.get(i).getId());
						listaPartidasCasadas.get(j).setApuntesAudit(contaApuntesAudit);
						listIdsGin.add(listaPartidasCasadas.get(j).getGin());
						listImporte.add(listaPartidasCasadas.get(j).getImporte());
					}
					this.tmct0ContaPartidasCasadasDAO.save(listaPartidasCasadas);
				}

				// Por cada our, eliminamos su ID_APUNTES_PNDTS y le asignamos el ID_APUNTES_AUDIT
				for (int i = 0; i < listaApuntesRechazar.size(); i++) {

					LinkedHashMap map = listaApuntesRechazar.get(i);
					Integer id = map.get("id") != null ? (Integer) map.get("id") : null;

					// Recuperamos las our cuyos ID_APUNTES_PNDTS sean igual a los apuntes aprobados de la tabla TMCT0_CONTA_APUNTES_PNDTS
					List<Tmct0ContaOur> listaOur = tmct0ContaOurDAO.findById(BigInteger.valueOf((Integer) id));

					// Por cada our, eliminamos su ID_APUNTES_PNDTS y le asignamos el ID_APUNTES_AUDIT
					for (int j = 0; j < listaOur.size(); j++) {
						listaOur.get(j).setApuntesPndts(null);
						Tmct0ContaApuntesAudit contaApuntesAudit = new Tmct0ContaApuntesAudit();
						contaApuntesAudit.setId(listaApuntesAudit.get(i).getId());
						listaOur.get(j).setApuntesAudit(contaApuntesAudit);
					}

					this.tmct0ContaOurDAO.save(listaOur);
				}


				// Recuperamos los registros de la tabla TMCT0_CONTA_PARTIDAS_PNDTS que corresponden a los GIN e IMPORTE de la TMCT0_CONTA_PARTIDAS_CASADAS
				for (int i=0;i<listIdsGin.size();i++) {
					List<Tmct0ContaPartidasPndts> listPartidasPndts = tmct0ContaPartidasPndtsDao.findByGinAndImporte(listIdsGin.get(i),listImporte.get(i));
					listaPartidasPndts.add(listPartidasPndts.get(0));
				}

				// Por cada registro de la tabla TMCT0_CONTA_PARTIDAS_PNDTS recuperado seteamos su campo ESTADO a 0.
				for (int i = 0; i < listaPartidasPndts.size(); i++) {
					listaPartidasPndts.get(i).setEstado(0);
				}

				// Guardamos los cambios realizados en el ESTADO de la TMCT0_CONTA_PARTIDAS_PNDTS
				this.tmct0ContaPartidasPndtsDao.save(listaPartidasPndts);

				// Se eliminarán todos los apuntes seleccionados de la tabla TMCT0_CONTA_APUNTES_PNDTS.
				this.tmct0ContaApuntesPndtsDao.deleteFromId(listIdsApuntesAprobar);		

				ListMultimap<String, Tmct0ContaApuntesPndts> mapaApuntesPndts = ArrayListMultimap.create();

				try {
					// Se le envía un correo al usuario informandole de los rechazos realizados
					for (int i = 0; i < listaApuntesRechazar.size(); i++) {
						LinkedHashMap map = listaApuntesRechazar.get(i);
						Tmct0ContaApuntesPndts contaApuntePndts = tmct0ControlContabilidadAssembler.getTmct0ContaApuntesPndts(map);
						mapaApuntesPndts.put(contaApuntePndts.getUsuApunte(), contaApuntePndts);
					}

					Set keySet = mapaApuntesPndts.keySet();
					Iterator keyIterator = keySet.iterator();
					while (keyIterator.hasNext() ) {
						String key = (String) keyIterator.next();
						List<Tmct0ContaApuntesPndts> listaApuntesPndts = mapaApuntesPndts.get(key);
						String email = tmct0UsersBo.getByUsername(key).getEmail();
						StringBuilder body = new StringBuilder();
						body.append("Se han rechazado los siguientes apuntes que podrás verlos en la tabla TMCT0_CONTA_APUNTES_AUDIT:");
						body.append("\n");
						body.append("FEAPUNTE|USUAPUNTE|CONCEPTO|IMPORTEDEBE|IMPORTEHABER|IMPORTEPYG|ESTADO");
						for (int i=0;i<listaApuntesPndts.size();i++) {
							body.append("\n");
							body.append(listaApuntesPndts.get(i).getFeApunte()+
									"|"+listaApuntesPndts.get(i).getUsuApunte()+
									"|"+listaApuntesPndts.get(i).getConcepto().getId()+
									"|"+listaApuntesPndts.get(i).getImporteDebe()+
									"|"+listaApuntesPndts.get(i).getImporteHaber()+
									"|"+listaApuntesPndts.get(i).getImportePyG()+
									"|0");
						}

						sendMail.sendMail(email, "subject mail", body.toString());
					}
				} catch (Exception e) {
					LOG.error("Error al enviar mail", e);
				}
			} else {
				throw new SIBBACBusinessException("Error: No hay apuntes a rechazar.");
			}
		} catch (SIBBACBusinessException e) {
			LOG.error("Error metodo rechazarApuntes -  " + e.getMessage(), e);
			throw(e);
		} catch (Exception e) {
			LOG.error("Error metodo rechazarApuntes -  " + e.getMessage(), e);
			throw(e);
		}
		LOG.info("FIN - SERVICIO - rechazarApuntes");
	}
}
