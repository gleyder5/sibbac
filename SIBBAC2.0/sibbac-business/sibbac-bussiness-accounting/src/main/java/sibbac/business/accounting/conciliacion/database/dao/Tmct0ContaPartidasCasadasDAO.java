package sibbac.business.accounting.conciliacion.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ContaPartidasCasadasDAO extends JpaRepository<Tmct0ContaPartidasCasadas, BigInteger> {
	
	@Query( "SELECT partidasCasadas FROM Tmct0ContaPartidasCasadas partidasCasadas WHERE partidasCasadas.apuntesPndts.id = :id" )
	public List<Tmct0ContaPartidasCasadas> findById(@Param( "id" ) BigInteger id);

	@Query( "SELECT partidasCasadas FROM Tmct0ContaPartidasCasadas partidasCasadas WHERE partidasCasadas.conciliacionAudit.id = :idConciliacionAudit" )
	public List<Tmct0ContaPartidasCasadas> findByIdConciliacionAudit(@Param( "idConciliacionAudit" ) BigInteger id);
	
	@Query( "SELECT partidasCasadas FROM Tmct0ContaPartidasCasadas partidasCasadas WHERE partidasCasadas.apuntesAudit.id = :idApunteAudit" )
	public List<Tmct0ContaPartidasCasadas> findByIdApunteAudit(@Param( "idApunteAudit" ) BigInteger id);
	
}
