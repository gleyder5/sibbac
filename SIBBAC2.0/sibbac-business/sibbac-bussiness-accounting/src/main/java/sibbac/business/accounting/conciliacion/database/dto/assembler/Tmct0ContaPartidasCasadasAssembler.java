/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.sql.Date;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaPartidasCasadasDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;

/**
 * @author lucio.vilar
 *
 */
@Service
public class Tmct0ContaPartidasCasadasAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaConciliacionAuditAssembler.class);

	public Tmct0ContaPartidasCasadasAssembler() {
		super();
	}

	public Tmct0ContaPartidasCasadasDTO getTmct0ContaPartidasCasadasDTO(Tmct0ContaPartidasCasadas entidad) throws ParseException {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaPartidasCasadasDTO");
		Tmct0ContaPartidasCasadasDTO dto = new Tmct0ContaPartidasCasadasDTO();
		try {
			dto.setAuxiliar(entidad.getAuxBancario());
			if (null != entidad.getConciliacionAudit() && null != entidad.getConciliacionAudit().getConcepto()) {
				dto.setConcepto(entidad.getConciliacionAudit().getConcepto().getConcepto());
			}
			dto.setImporte(entidad.getImporte());
			if (null != entidad.getConciliacionAudit()) {
				dto.setReferencia(entidad.getReferencia());
			}
			dto.setTipoMovimiento(entidad.getTipo());
			dto.setGin(entidad.getGin());
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaPartidasCasadasDTO -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FIN - ASSEMBLER - getTmct0ContaPartidasCasadasDTO");
		return dto;
	}

	public Tmct0ContaPartidasCasadas getTmct0ContaPartidasCasadas(Tmct0ContaPartidasPndts pend, Tmct0ContaConciliacionAudit conciliacionAudit) throws ParseException {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaPartidasCasadas");
		Tmct0ContaPartidasCasadas casada = new Tmct0ContaPartidasCasadas();
		try {
			casada.setAntiguedad(pend.getAntiguedad());
			casada.setAuxBancario(pend.getAuxBancario());
			Long bookingdate = pend.getBookingDate().getTime();
			Date fechaBooking = new Date(bookingdate);
			casada.setBookingDate(fechaBooking);
			casada.setComentario(pend.getComentario());
			casada.setComentarioUsuario(pend.getComentarioUsuario());
			casada.setConciliacionAudit(conciliacionAudit);
			casada.setCurrency(pend.getCurrency());
			casada.setDepartamento(pend.getDepartamento());
			casada.setGin(pend.getGin());
			casada.setImporte(pend.getImporte());
			casada.setNumDocumento(pend.getNumDocumento());
			casada.setNumDocumento2(pend.getNumDocumento2());
			casada.setReferencia(pend.getReferencia());
			casada.setReferencia2(pend.getReferencia2());
			casada.setTipo(pend.getTipo());
			Long date = pend.getValueDate().getTime();
			Date fecha = new Date(date);
			casada.setValueDate(fecha);
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaPartidasCasadas -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FINAL - ASSEMBLER - getTmct0ContaPartidasCasadas");
		return casada;

	}
}
