package sibbac.business.accounting.enums;

/**
 * Clase con las enumeraciones para la validacion de accounting.
 */
public class ValidacionAccountingEnum {
	
	/** Tipos de plantilla. */
	public static enum TipoPlantillaEnum {

		APUNTE_MANUAL("APUNTE MANUAL"),
		COBROS_RETROCESIONES("COBROS/RETROCESIONES"),
		COBRO_BARRIDOS("COBRO BARRIDOS"),
		DOTACION_DESDOTACION("DOTACION/DESDOTACION"),
		DEVENGOS_ANULACION("DEVENGOS/ANULACION"),
		DEVENGO_FACTURAS("DEVENGO FACTURAS"),
		DEVENGO_FALLIDOS("DEVENGO FALLIDOS"),
		EMISION_FACTURA("EMISION FACTURA"),
		LIQUIDACION("LIQUIDACION"),
		LIQUIDACION_CON_ACENTO("LIQUIDACIÓN"),
		P_Y_G("PyG");

		/** Tipo de plantilla. */
		private String tipo;

		/**
		 * @param tipo
		 */
		private TipoPlantillaEnum(String tipo) {
			this.tipo = tipo;
		}

		public String getTipo() {
			return this.tipo;
		}
	}
	
	/** Valores que forman parte de la clave de la plantilla. */
	public static enum ClavePlantillaEnum {

		LITERALES_QUERIES("LITERALES_QUERIES"),
		NUM_QUERY_BLOQUEO("NUM_QUERY_BLOQUEO"),
		NUM_QUERY_COBRO("NUM_QUERY_COBRO"),
		NUM_QUERY_DETALLE("NUM_QUERY_DETALLE"),
		NUM_QUERY_ORDEN("NUM_QUERY_ORDEN"),
		QUERY_BLOQUEO("QUERY_BLOQUEO"),
		QUERY_COBRO("QUERY_COBRO"),
		QUERY_APUNTE_DETALLE("QUERY_APUNTE_DETALLE"),
		QUERY_ORDEN("QUERY_ORDEN"),
		TABLA_MAESTRA("TABLA_MAESTRA");

		/** Clave de plantilla. */
		private String clave;

		/**
		 * @param clave
		 */
		private ClavePlantillaEnum(String clave) {
			this.clave = clave;
		}

		public String getClave() {
			return this.clave;
		}
	}
	
	/** Constantes de error. */
	public static enum ConstantesError {

		ERROR_FICHERO_PLANTILLA("El campo FICHERO de la plantilla debe estar relleno y tener longitud 2"),
		ERROR_TIPO_COMPROBANTE_PLANTILLA("El campo TIPO_COMPROBANTE de la plantilla debe estar relleno y tener valor numérico"),
		ERROR_CAMPO_ESTADO_PLANTILLA("El campo CAMPO_ESTADO de la plantilla debe estar relleno"),
		ERROR_QUERY_APUNTE_DETALLE_PLANTILLA("El campo QUERY_APUNTE_DETALLE de la plantilla debe estar relleno"),
		ERROR_CAMPO_ESTADO_EN_TABLA("El campo CAMPO_ESTADO de la plantilla debe ser una columna existente en la tabla "),
		ERROR_ESTADO_INICIO_PLANTILLA("El campo ESTADO_INICIO de la plantilla debe estar relleno"),
		ERROR_ESTADO_INICIO_PLANTILLA_EN_TABLA_ESTADOS("El campo ESTADO_INICIO de la plantilla debe ser un estado existente en la tabla TMCT0ESTADO"),
		ERROR_ESTADO_FINAL_PLANTILLA("El campo ESTADO_FINAL de la plantilla debe estar relleno"),
		ERROR_ESTADO_FINAL_PLANTILLA_EN_TABLA_ESTADOS("El campo ESTADO_FINAL de la plantilla debe ser un estado existente en la tabla TMCT0ESTADO"),
		ERROR_ESTADO_PARCIAL_PLANTILLA("El campo ESTADO_PARCIAL de la plantilla debe estar relleno"),
		ERROR_ESTADO_PARCIAL_PLANTILLA_EN_TABLA_ESTADOS("El campo ESTADO_PARCIAL de la plantilla debe ser un estado existente en la tabla TMCT0ESTADO"),
		ERROR_QUERY_ORDEN_PLANTILLA("El campo QUERY_ORDEN de la plantilla debe estar relleno"),
		ERROR_GRABA_DETALLE_PLANTILLA("El campo GRABA_DETALLE de la plantilla no debe ser nulo y valer ' ', 'A','D' o 'H'"),
		ERROR_QUERY_BLOQUEO_PLANTILLA("El campo QUERY_BLOQUEO de la plantilla debe estar relleno"),
		ERROR_CUENTAS_AUXILIARES_PLANTILLA("Los campos CUENTA_DEBE, CUENTA_HABER, AUX_DEBE y AUX_HABER deben contener el mismo número de parámetros separados por ','"),
		ERROR_INTERROGACION_SIMPLE_QUERY_ORDEN("La QUERY_ORDEN debe contener únicamente un valor ?"),
		ERROR_INTERROGACIONES_QUERY_ORDEN("La QUERY_ORDEN debe contener el parámetro: "),
		ERROR_LITERAL_QUERY_ORDEN("La QUERY_ORDEN debe contener uno de los siguientes literales: "),
		ERROR_LITERAL_QUERY_COBRO("La QUERY_COBRO debe contener uno de los siguientes literales: "),
		ERROR_UNICO_LITERAL_QUERY_ORDEN("La QUERY_ORDEN debe contener únicamente uno de los siguientes literales: "),
		ERROR_UNICO_LITERAL_QUERY_COBRO("La QUERY_COBRO debe contener únicamente uno de los siguientes literales: "),
		ERROR_ORDEN_QUERY_BLOQUEO("El orden de los campos del SELECT de QUERY_BLOQUEO debe de ser el siguiente: ["),
		ERROR_GROUPBY_QUERY_BLOQUEO("La QUERY_BLOQUEO no debe contener GROUP BY"),
		ERROR_HAVING_QUERY_BLOQUEO("La QUERY_BLOQUEO no debe contener HAVING"),
		ERROR_GROUPBY_QUERY_APUNTE_DETALLE("La QUERY_APUNTE_DETALLE no debe contener GROUP BY"),
		ERROR_HAVING_QUERY_APUNTE_DETALLE("La QUERY_APUNTE_DETALLE no debe contener HAVING"),
		ERROR_INTERROGACION_QUERY_BLOQUEO("La QUERY_BLOQUEO debe contener el parámetro: "),
		ERROR_INTERROGACIONES_QUERY_BLOQUEO("La QUERY_BLOQUEO debe contener únicamente un valor ?1"),
		ERROR_INTERROGACION_AUX(" cuyo valor corresponde al valor de: "),
		ERROR_ORDEN_QUERY_APUNTE_DETALLE("El orden de los campos del SELECT de QUERY_APUNTE_DETALLE debe de ser el siguiente: ["),
		ERROR_INTERROGACIONES_QUERY_APUNTE_DETALLE("La QUERY_APUNTE_DETALLE debe contener el parámetro: "),
		ERROR_ORDEN_QUERY_ORDEN("El orden de los campos del SELECT de QUERY_ORDEN debe de ser el siguiente: ["),
		ERROR_ORDEN_QUERY_COBRO("El orden de los campos del SELECT de QUERY_COBRO debe de ser el siguiente: ["),
		ERROR_QUERY_ORDEN_LITERAL_EXCLUYENTE_DESC_REFER_SETTDATE("La QUERY_ORDEN podrá llevar los textos DESC_REFERENCIA o el SETTDATE, en ningún caso los dos a la vez"),
		ERROR_QUERY_BLOQUEO_FORMATO_DEVENGO_FACTURAS("El campo QUERY_BLOQUEO debe tener al menos un UPDATE y un SELECT entre paréntesis"),
		ERROR_SUM_QUERY_APUNTE_DETALLE("La QUERY_APUNTE_DETALLE no debe contener SUM"),
		ERROR_QUERY_COBRO_PLANTILLA("El campo QUERY_COBRO de la plantilla debe estar relleno"),
		ERROR_ORDEN_INTRODUCIDO("]. El orden de los campos del SELECT introducidos ha sido el siguiente: "),
		ERROR_CAMPO_PLANTILLA("El campo CAMPO de la plantilla debe estar relleno");

		/** Constante de error. */
		private String error;

		/**
		 * @param error
		 */
		private ConstantesError(String error) {
			this.error = error;
		}

		public String getError() {
			return this.error;
		}
	}
	
}
