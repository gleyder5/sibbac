package sibbac.business.accounting.threads;

import sibbac.business.accounting.database.bo.AbstractEjecucionPlantilla;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;

/**
 * Representa un hilo que realiza los bloqueos de los ALCs involucrados en la
 * generación de apuntes contables.
 * 
 * @author Neoris
 *
 */
public class EjecucionPlantillaIndividualRunnable implements Runnable {

	private AbstractEjecucionPlantilla ejecucionPlantilla;
	private Tmct0ContaPlantillas plantilla;

	/**
	 * Constructor.
	 * 
	 * @param concepto
	 *            Concepto de la query orden.
	 * @param query_bloqueo
	 *            Query de bloqueo.
	 * @param estado_bloqueado
	 *            Estado bloqueado.
	 * @param abstractEjecucionPlantilla
	 *            Instancia de ejecución de plantilla.
	 */
	public EjecucionPlantillaIndividualRunnable(Tmct0ContaPlantillas plantilla,
			AbstractEjecucionPlantilla abstractEjecucionPlantilla) {
		this.plantilla = plantilla;
		this.ejecucionPlantilla = abstractEjecucionPlantilla;
	}

	@Override
	public void run() {

		try {
			ejecucionPlantilla.anadirPlantillas(ejecucionPlantilla.ejecutarPlantillaIndividual(plantilla));
			ejecucionPlantilla.shutdownRunnablePlantillaIndividual(this, false);
		} catch (Exception e) {
			ejecucionPlantilla.shutdownRunnablePlantillaIndividual(this, true);
		} finally {
			ejecucionPlantilla.shutdownRunnablePlantillaIndividual(this, false);
		}

	}

	public Tmct0ContaPlantillas getPlantilla() {
		return plantilla;
	}

	public void setPlantilla(Tmct0ContaPlantillas plantilla) {
		this.plantilla = plantilla;
	}

}
