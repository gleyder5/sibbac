package sibbac.business.accounting.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDaoImp;
import sibbac.business.accounting.database.dto.AlcDTO;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.CobroDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ConstantesError;


@Service
public class EjecucionLiquidacionesApuntesManuales {
	
	 @Autowired
	 Tmct0Norma43MovimientoBo tmct0Norma43MovimientoBo;
	 
	 @Autowired
	 Tmct0ContaPlantillasDaoImp tmct0ContaPlantillasDaoImp;
	 
	 @Autowired
	 Tmct0ContaApuntesBo tmct0ContaApuntesBo;
	 
	 @Autowired
	 Tmct0ContaApunteDetalleBo tmct0ContaApunteDetalleBo;

	
	  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla, 
			  List<ApuntesManualesExcelDTO> listaAMExcelDTO) throws EjecucionPlantillaException {

		  Loggers.logDebugInicioFinAccounting(plantilla != null ? plantilla.getCodigo() : null, true);

		  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();
		  
		  String whereOrden = AccountingHelper.concatWhereModificadoByListaALCsAM(listaAMExcelDTO);
		  String whereCobro = AccountingHelper.concatWhereModificadoByReferenciasTheirAM(listaAMExcelDTO.get(0));
		  
		  Object cobroResult = null;
		  try {
			  String queryCobro = AccountingHelper.modificarConsultaQUERY_COBRO(plantilla.getQuery_cobro(), whereCobro);
			  cobroResult = ejecutaQueryCobroAM(queryCobro, plantilla.getCuenta_bancaria(), plantilla.getCodigo());
		  } catch (Exception ex) {
		      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO.getValue() + ": " + ex.getMessage());

		      throw new EjecucionPlantillaException();
		  }
		  
		  if(cobroResult!=null){
			  CobroDTO cobro = new CobroDTO("", (BigDecimal) cobroResult, "", null, null, "", new Date(), new Date());
			  
			  //Ejecuta query ordenes
			  List<Object[]> ordenes = null;
	          if (plantilla.getQuery_orden() != null) {
	            try {
	              if (plantilla.getQuery_orden() != null) {
	            	String queryOrden = AccountingHelper.modificarConsultaQUERY_ORDEN(plantilla.getQuery_orden(), whereOrden);
	            	Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Buscando ordenes para el cobro: CobroDTO: " + cobro.toString() + " [ejecutarPlantillaCobrosRetrocesiones]" + "[" + new Date() + "]");
	                ordenes = this.ejecutaQueryOrden(queryOrden, plantilla.getCodigo());
	              }
	              if(null == ordenes || ordenes.isEmpty()){
					  throw new EjecucionPlantillaException(); 
				  }
	            } catch (Exception ex) {
	              Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
	              throw new EjecucionPlantillaException("ordenes"); 
	            }
	          }
	          
	          String queryDetalle = AccountingHelper.sustituirWhere(plantilla.getQuery_apunte_detalle()) + whereOrden;
	          plantilla.setQuery_apunte_detalle(queryDetalle);
	          
	       // Se persisten los datos necesarios.
	          this.tmct0ContaApuntesBo.persistirLiquidacionesApuntesManuales(ordenes, plantilla, cobro, listaAMExcelDTO, whereCobro, whereOrden);
		  }else{
			  throw new EjecucionPlantillaException("cobro"); 
		  }
		  return plantillasSinBalance;
	  }
	  
	  private Object ejecutaQueryCobroAM(String query, String cuentaBancaria, String codigoPlantilla) throws EjecucionPlantillaException {

	    Loggers.logDebugQueriesAccounting(codigoPlantilla,SibbacEnums.ConstantesQueries.QUERY_COBRO.getValue(), null);
	    String queryCobro = query.replaceAll("<CUENTA_BANCARIA DE LA PLANTILLA>",cuentaBancaria.replaceAll("\"", "'")).replace(";", "");
	    
	    return tmct0ContaPlantillasDaoImp.executeQuerySingleResult(queryCobro, codigoPlantilla);
	  }
	  
	  public List<Object[]> ejecutaQueryOrden(String queryOrden,
              				String codigoPlantilla) throws EjecucionPlantillaException {

		  return tmct0ContaPlantillasDaoImp.executeQuery(queryOrden, codigoPlantilla);

	  }	
	  
}

