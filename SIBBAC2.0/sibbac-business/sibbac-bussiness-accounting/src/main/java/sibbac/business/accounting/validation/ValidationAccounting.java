package sibbac.business.accounting.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dao.Tmct0AccountingValidationDao;
import sibbac.business.accounting.database.dao.Tmct0AccountingValidationDaoImp;
import sibbac.business.accounting.database.model.Tmct0AccountingValidation;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.enums.ValidacionAccountingEnum.ClavePlantillaEnum;
import sibbac.business.accounting.enums.ValidacionAccountingEnum.ConstantesError;
import sibbac.business.accounting.enums.ValidacionAccountingEnum.TipoPlantillaEnum;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SibbacEnums.ConstantesQueries;

import com.google.common.base.Strings;


@Service
public class ValidationAccounting {
	
	protected static final Logger LOG = LoggerFactory.getLogger(ValidationAccounting.class);

	@Autowired
	Tmct0estadoDao estadoDao;

	@Autowired
	Tmct0AccountingValidationDao accountingValidationDao;

	@Autowired
	Tmct0AccountingValidationDaoImp accountingValidationDaoImp;

	/**
	 * Valida todos los campos obligatorios de las plantillas.
	 * 
	 * @param plantillas: plantillas a validar
	 * @return boolean
	 */
	public void validarCamposObligatorios(Tmct0ContaPlantillas plantilla, String tipoPlantilla) {

		try {

			//Validamos el campo FICHERO
			if (Strings.isNullOrEmpty(plantilla.getFichero()) || plantilla.getFichero().length()>2) {
				Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_FICHERO_PLANTILLA.getError());
			}

			//Validamos el campo TIPO_COMPROBANTE
			if (Strings.isNullOrEmpty(plantilla.getTipo_comprobante()) || !StringUtils.isNumeric(plantilla.getTipo_comprobante())) {
				Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_TIPO_COMPROBANTE_PLANTILLA.getError());
			}

			//Validamos el campo GRABA_DETALLE
			if (null==plantilla.getGrabaDetalle() || (!plantilla.getGrabaDetalle().equals(' ') && !plantilla.getGrabaDetalle().equals('A') && !plantilla.getGrabaDetalle().equals('D') && !plantilla.getGrabaDetalle().equals('H'))) {
				Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_GRABA_DETALLE_PLANTILLA.getError());
			}

			//Validamos que CUENTA_DEBE, AUX_DEBE, CUANTA_HABER, AUX_HABER tienen el mismo número de comas
			String[] cuentasDebe = StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(),",");
			String[] cuentasHaber = StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(),",");
			String[] auxsDebe = StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(),",");
			String[] auxsHaber = StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(),",");
			if (!AccountingHelper.isPlantillaCuentasAuxConsistentes(cuentasDebe, cuentasHaber, auxsDebe, auxsHaber)) {
				Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_CUENTAS_AUXILIARES_PLANTILLA.getError());
			}
		} catch (Exception e) {
			LOG.error("Error método validarCamposObligatorios: " + e.getMessage());
		}
	}

	/**
	 * Se devuelve una lista de cadena de caracteres los nombres de los campos a partir de una query.
	 * 
	 * @param query: query a tomar los campos
	 * @return lista de cadena de caracteres
	 */
	public static List<String> getListCamposQuery(String query) {

		List<String> lista = new ArrayList<>();
		int iAux = 0;
		String sAux;
		String sCadenaCampos = null;

		try {

			// Se obtiene la subCadena de la query que contiene los campos entre primer select y su from correspondiente


			int iPosicionSelect = -1;
			if (query.trim().toUpperCase().startsWith("select".toUpperCase())){ // Si se trata de una consulta tiene que empezar por select y no coger otro select de una subconsulta
				iPosicionSelect = query.toUpperCase().indexOf("select".toUpperCase());	
			}


			if (iPosicionSelect >= 0){
				// Se busca su from correspondiente 
				int iPosicionFrom = 0;

				iPosicionFrom = query.toUpperCase().indexOf("from".toUpperCase());

				if (iPosicionFrom > 0){
					sAux = query.substring(iPosicionSelect + 6 ,iPosicionFrom).toUpperCase();

					// Se comprueba si dentro de los campos hay alguna subselect y el from correspondiente no es su from.
					while (sAux.toUpperCase().indexOf("select".toUpperCase()) > 0){
						iAux = query.toUpperCase().indexOf("from".toUpperCase(), iPosicionFrom +1 );  // Se busca el siguiente from.
						sAux = query.substring(iPosicionFrom ,iAux).toUpperCase();
						iPosicionFrom = iAux;
					}

					sCadenaCampos = query.substring(iPosicionSelect + 6,iPosicionFrom).toUpperCase();

				}
			}

			// Una vez que se tiene la cadena con los campos, se quita todo lo que este contenido entre parentesis ya que estos pueden contener alguna coma
			// que al final va a ser el separador de mis campos finales.

			// Se comprueba que se tengan los mismos parentesis de abrir que de cerrar sino esta mal formado.
			if (sCadenaCampos != null){
				int iNumParentesisAbrir = StringUtils.countMatches(sCadenaCampos, "(");
				int iNumParentesisCerrar = StringUtils.countMatches(sCadenaCampos, ")");

				if (iNumParentesisAbrir == iNumParentesisCerrar){
					int iPosParentesisAbrir = 0;
					int iPosParentesisCerrar = 0;


					iPosParentesisAbrir = sCadenaCampos.indexOf("(");
					int contParentesis = 0;
					while (iPosParentesisAbrir > 0) {
						iPosParentesisCerrar = sCadenaCampos.indexOf(")", iPosParentesisAbrir );
						sAux = sCadenaCampos.substring(iPosParentesisAbrir + 1,iPosParentesisCerrar);
						iAux = StringUtils.countMatches(sAux, "(");
						contParentesis = contParentesis + iAux;

						while (contParentesis > 0){
							iAux = iPosParentesisCerrar;
							iPosParentesisCerrar = sCadenaCampos.indexOf(")", iPosParentesisCerrar + 1 );
							if (iPosParentesisCerrar > 0){
								sAux = sCadenaCampos.substring(iAux + 1,iPosParentesisCerrar);
								iAux = StringUtils.countMatches(sAux, "(");
								contParentesis = contParentesis - 1 + iAux; // Se resta el parentesis de cerrar que he encontrado y se suman los de abrir.

							}else{
								contParentesis = 0;
							}
						}
						String sCadenaEntreParentesis = sCadenaCampos.substring(iPosParentesisAbrir, iPosParentesisCerrar + 1);
						sCadenaCampos = StringUtils.remove(sCadenaCampos,sCadenaEntreParentesis );
						iPosParentesisAbrir = sCadenaCampos.indexOf("("); 
					}

				}


				// Ya se tienen separados los campos.
				StringTokenizer st = new StringTokenizer(sCadenaCampos,",");

				while (st.hasMoreElements()) {
					String sCampo = (String) st.nextElement();

					// Puede ser que no se haya eliminado todo y haya mas de una información separa por espacios,  Nos quedamos con la última palabra
					StringTokenizer st2 = new StringTokenizer(sCampo," ");

					while (st2.hasMoreElements()) {
						sCampo = (String) st2.nextElement();
					}
					lista.add(sCampo);	
				}

			}
		} catch (Exception e) {
			LOG.error("Error método getListCamposQuery: " + e.getMessage());
		}
		return lista;
	}

	/**
	 * Validaciones de las plantillas de tipo DEVENGO y ANULACION.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosDevengosAnulacion(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosImportes = new ArrayList<String>();
		List<String> elementosAuxiliares = new ArrayList<String>();
		List<String> elementosAuxiliaresSinComillas = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();
		List<String> literalesQuery = new ArrayList<String>();
		int tamanioImportes = 0;
		int tamanioAuxiliares = 0;

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla, TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo());

				//Se valida CAMPO_ESTADO
				validarCampoEstado(plantilla);
				
				//Se valida ESTADO_FINAL
				validarCampoEstadoFinal(plantilla);

				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {

					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenDevengoAnulacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null!=ordenQueryOrdenDevengoAnulacion) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryOrdenDevengoAnulacion, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() + (ordenQueryOrdenDevengoAnulacion != null ? StringHelper.safeNull(ordenQueryOrdenDevengoAnulacion.getValor()) : "") 
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_ORDEN contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_ORDEN.getValue());

					//Recuperamos los literales que debe de tener la QUERY_ORDEN
					Tmct0AccountingValidation literalesDevengoAnulacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo(), ClavePlantillaEnum.LITERALES_QUERIES.getClave());
					if (null!=literalesDevengoAnulacion) {
						literalesQuery = Arrays.asList(literalesDevengoAnulacion.getValor().split(","));
					}

					//Validamos que los literales recuperados estén en la QUERY_ORDEN
					if (!AccountingHelper.hayUnLiteral(plantilla.getQuery_orden(), literalesQuery)) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_LITERAL_QUERY_ORDEN.getError()+literalesQuery);
					}

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}


				/*************************************************************************************************************************/
				/******************************************************QUERY_BLOQUEO******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_BLOQUEO
				if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_BLOQUEO
					Tmct0AccountingValidation ordenQueryBloqueoDevengoAnulacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo(), ClavePlantillaEnum.QUERY_BLOQUEO.getClave());
					if (null != ordenQueryBloqueoDevengoAnulacion) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryBloqueoDevengoAnulacion, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_BLOQUEO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_bloqueo());

					//Validamos que los elementos estén en el orden indicado de la QUERY_BLOQUEO
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect, elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_BLOQUEO.getError() 
							+ (ordenQueryBloqueoDevengoAnulacion != null ? StringHelper.safeNull(ordenQueryBloqueoDevengoAnulacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_BLOQUEO no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "FECHA_EJECUCION", ConstantesQueries.QUERY_BLOQUEO.getValue());
					// MFG se comenta la siguiente comprobación ya que se ha cambiado y el parametro no viene en la query y se forma dinamicamente.
					//this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
				}


				/*************************************************************************************************************************/
				/**************************************************QUERY_APUNTE_DETALLE***************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_APUNTE_DETALLE
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation ordenQueryApunteDetalleDevengoAnulacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
					if (null!=ordenQueryApunteDetalleDevengoAnulacion) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryApunteDetalleDevengoAnulacion, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_APUNTE_DETALLE
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

					//Validamos que los elementos estén en el orden indicado de la QUERY_APUNTE_DETALLE
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_APUNTE_DETALLE.getError() + (ordenQueryApunteDetalleDevengoAnulacion != null ? StringHelper.safeNull(ordenQueryApunteDetalleDevengoAnulacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
					this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE contenga una ?1, ?2 y ?3
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDAPUNTE", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?3", "FECHA_EJECUCION", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarEspecificosDevengosAnulacion: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo EMISION_FACTURA.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosEmisionFactura(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();

		try {

			if (CollectionUtils.isNotEmpty(plantillas)) {
				for (Tmct0ContaPlantillas plantilla : plantillas) {
					//Se validan los campos obligatorios comunes
					validarCamposObligatorios(plantilla,TipoPlantillaEnum.EMISION_FACTURA.getTipo());
					
					//Se valida ESTADO_FINAL
					validarCampoEstadoFinal(plantilla);

					//Se valida CAMPO_ESTADO
					validarCampoEstado(plantilla);

					/*************************************************************************************************************************/
					/*******************************************************QUERY_ORDEN*******************************************************/
					/*************************************************************************************************************************/
					//Validamos el campo QUERY_ORDEN
					if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
					} else {
						//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
						Tmct0AccountingValidation ordenQueryOrdenEmisionFactura = accountingValidationDao.findByTipoAndClave(
								TipoPlantillaEnum.EMISION_FACTURA.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
						if (null != ordenQueryOrdenEmisionFactura) {
							elementosSelect = Arrays.asList(ordenQueryOrdenEmisionFactura.getValor().split(","));
						}

						//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
						elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

						//Validamos que los elementos estén en el orden indicado
						AccountingHelper.validarOrden(
								plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
								ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() 
								+ (ordenQueryOrdenEmisionFactura != null ? StringHelper.safeNull(ordenQueryOrdenEmisionFactura.getValor()) : "")
								+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

						//Validamos que la QUERY_ORDEN contenga una ?
						this.validarParamInterrogQueryByTipoQuery(plantilla, "?", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_ORDEN.getValue());

						//Reiniciamos las listas de elementos
						elementosSelect = new ArrayList<String>();
						elementosSelectQuery = new ArrayList<String>();
					}

					/*************************************************************************************************************************/
					/******************************************************QUERY_BLOQUEO******************************************************/
					/*************************************************************************************************************************/
					//Validamos el campo QUERY_BLOQUEO
					if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
					} else {
						//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_BLOQUEO
						Tmct0AccountingValidation ordenQueryBloqueoEmisionFactura = accountingValidationDao.findByTipoAndClave(
								TipoPlantillaEnum.EMISION_FACTURA.getTipo(), ClavePlantillaEnum.QUERY_BLOQUEO.getClave());
						if (null != ordenQueryBloqueoEmisionFactura) {
							elementosSelect = Arrays.asList(ordenQueryBloqueoEmisionFactura.getValor().split(","));
						}

						//Recuperamos el orden de los elementos del SELECT de la QUERY_BLOQUEO
						elementosSelectQuery = getListCamposQuery(plantilla.getQuery_bloqueo());

						//Validamos que los elementos estén en el orden indicado
						AccountingHelper.validarOrden(
								plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
								ConstantesError.ERROR_ORDEN_QUERY_BLOQUEO.getError()
								+ (ordenQueryBloqueoEmisionFactura != null ? StringHelper.safeNull(ordenQueryBloqueoEmisionFactura.getValor()) : "")
								+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

						//Validamos que la QUERY_BLOQUEO no contenga GROUP BY
						this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

						//Validamos que la QUERY_BLOQUEO no contenga HAVING
						this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

						//Validamos que la QUERY_BLOQUEO contenga una ?1
						this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "ESTADO_INICIO", ConstantesQueries.QUERY_BLOQUEO.getValue());
						// MFG se comenta la siguiente comprobación ya que se ha cambiado y el parametro no viene en la query y se forma dinamicamente.
						//this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_BLOQUEO.getValue());

						//Reiniciamos las listas de elementos
						elementosSelect = new ArrayList<String>();
						elementosSelectQuery = new ArrayList<String>();
					}

					/*************************************************************************************************************************/
					/**************************************************QUERY_APUNTE_DETALLE***************************************************/
					/*************************************************************************************************************************/
					//Validamos el campo QUERY_APUNTE_DETALLE
					if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
					} else {
						//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
						Tmct0AccountingValidation ordenQueryApunteDetalleEmisionFactura = accountingValidationDao.findByTipoAndClave(
								TipoPlantillaEnum.EMISION_FACTURA.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
						if (null != ordenQueryApunteDetalleEmisionFactura) {
							elementosSelect = Arrays.asList(ordenQueryApunteDetalleEmisionFactura.getValor().split(","));
						}

						//Recuperamos el orden de los elementos del SELECT de la QUERY_APUNTE_DETALLE
						elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

						//Validamos que los elementos estén en el orden indicado
						AccountingHelper.validarOrden(
								plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
								ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() 
								+ (ordenQueryApunteDetalleEmisionFactura != null ? StringHelper.safeNull(ordenQueryApunteDetalleEmisionFactura.getValor()) : "")
								+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

						// Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
						this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

						// Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
						this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

						//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
						this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

						//Validamos que la QUERY_APUNTE_DETALLE contenga una ?1, ?2 y ?3
						this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDAPUNTE",ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
						this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "ESTADO_BLOQUEADO",ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
						this.validarParamInterrogQueryByTipoQuery(plantilla, "?3", "NBDOCNUMERO",ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					}

				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarEspecificosEmisionFactura: " + e.getMessage());
		}
	}


	/**
	 * Validaciones de las plantillas de tipo DEVENGO_FACTURAS.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosDevengoFacturas(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla,TipoPlantillaEnum.DEVENGO_FACTURAS.getTipo());

				//Se valida ESTADO_FINAL
				validarCampoEstadoFinal(plantilla);

				//Se valida CAMPO_ESTADO
				validarCampoEstado(plantilla);

				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenDevengoFacturas = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.DEVENGO_FACTURAS.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null != ordenQueryOrdenDevengoFacturas) {
						elementosSelect = Arrays.asList(ordenQueryOrdenDevengoFacturas.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() + (ordenQueryOrdenDevengoFacturas != null ? StringHelper.safeNull(ordenQueryOrdenDevengoFacturas.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/			
				/******************************************************QUERY_BLOQUEO******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_BLOQUEO
				if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
				} else {
					/**
					 *	Validar que sea un UPDATE y que tenga al menos un SELECT entre parentesis 
					 */
					if (!(plantilla.getQuery_bloqueo() != null 
							&& plantilla.getQuery_bloqueo().toUpperCase().contains("UPDATE") 
							&& (plantilla.getQuery_bloqueo().toUpperCase().contains("(SELECT")) || plantilla.getQuery_bloqueo().toUpperCase().contains("( SELECT"))) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_FORMATO_DEVENGO_FACTURAS.getError());
					}

					//Validamos que la QUERY_BLOQUEO no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());
				}

				/*************************************************************************************************************************/
				/**************************************************QUERY_APUNTE_DETALLE***************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_APUNTE_DETALLE
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					// Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation ordenQueryApunteDetalleDevengoFacturas = accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.DEVENGO_FACTURAS.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
					if (null != ordenQueryApunteDetalleDevengoFacturas) {
						elementosSelect = Arrays.asList(ordenQueryApunteDetalleDevengoFacturas.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_APUNTE_DETALLE.getError() 
							+ (ordenQueryApunteDetalleDevengoFacturas != null ? StringHelper.safeNull(ordenQueryApunteDetalleDevengoFacturas.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					// Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					// Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
					this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
				}

			}
		} catch (Exception e) {
			LOG.error("Error método validarEspecificosDevengoFacturas: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo DOTACION y DESDOTACION.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosDotacionDesdotacion(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {

				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla,TipoPlantillaEnum.DOTACION_DESDOTACION.getTipo());

				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenDotacionDesdotacion = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.DOTACION_DESDOTACION.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null != ordenQueryOrdenDotacionDesdotacion) {
						elementosSelect = Arrays.asList(ordenQueryOrdenDotacionDesdotacion.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError()
							+ (ordenQueryOrdenDotacionDesdotacion != null ? StringHelper.safeNull(ordenQueryOrdenDotacionDesdotacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarEspecificosDotacionDesdotacion: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo COBRO y RETROCESION.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosCobroRetrocesion(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();
		List<String> literalesQuery = new ArrayList<String>();
		List<String> elementosImportes = new ArrayList<String>();
		List<String> elementosAuxiliares = new ArrayList<String>();
		List<String> elementosAuxiliaresSinComillas = new ArrayList<String>();

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla, TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo());

				//Se valida ESTADO_FINAL
				validarCampoEstadoFinal(plantilla);

				//Se valida CAMPO_ESTADO
				validarCampoEstado(plantilla);

				//Se valida el CAMPO
				if (Strings.isNullOrEmpty(plantilla.getCampo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_CAMPO_PLANTILLA.getError());
				}
				
				//Validamos el campo ESTADO_PARCIAL
				// MFG este campo ya no se utiliza en la plantilla.
//				if (Strings.isNullOrEmpty(plantilla.getEstado_parcial())) {
//					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_PARCIAL_PLANTILLA.getError());
//				} else {
//					//Comprobamos que el estado exista en la tambla TMCT0ESTADO
//					Tmct0estado estado = this.estadoDao.findByNombre(plantilla.getEstado_parcial());
//					if (null==estado) {
//						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_PARCIAL_PLANTILLA_EN_TABLA_ESTADOS.getError());
//					}
//				}

				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					int tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));						
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					int tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenCobroRetrocesion = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null != ordenQueryOrdenCobroRetrocesion) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryOrdenCobroRetrocesion, tamanioImportes, tamanioAuxiliares);
					}
					
					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError()
							+ (ordenQueryOrdenCobroRetrocesion != null ? StringHelper.safeNull(ordenQueryOrdenCobroRetrocesion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Recuperamos los literales que debe de tener la QUERY_ORDEN
					Tmct0AccountingValidation literalesCobroRetrocesion = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo(), ClavePlantillaEnum.LITERALES_QUERIES.getClave());
					if (null != literalesCobroRetrocesion) {
						literalesQuery = new ArrayList<String>(Arrays.asList(literalesCobroRetrocesion.getValor().split(",")));
					}
					
					//Eliminamos el literal <CUENTA_BANCARIA DE LA PLANTILLA> de la lista (ES DE LA QUERY_COBRO) para que solo valide si existe uno de los otros dos literales
					int elementoAEliminar = 0;
					for (int i=0; i<literalesQuery.size();i++) {
						if  (literalesQuery.get(i).contains("<CUENTA_BANCARIA DE LA PLANTILLA>")) {
							elementoAEliminar = i;
						}
					}
					literalesQuery.remove(elementoAEliminar);

					//Validamos que los literales recuperados estén en la QUERY_ORDEN
					if (!AccountingHelper.hayUnLiteral(plantilla.getQuery_orden(), literalesQuery)) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_LITERAL_QUERY_ORDEN.getError() + literalesQuery);
					}

					// Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					elementosImportes = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
					elementosAuxiliaresSinComillas = new ArrayList<String>();
					literalesQuery = new ArrayList<String>();
					
				}

				/*************************************************************************************************************************/
				/*******************************************************QUERY_COBRO*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_COBRO
				if (Strings.isNullOrEmpty(plantilla.getQuery_cobro())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_COBRO
					Tmct0AccountingValidation ordenQueryCobroCobroRetrocesion = accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo(), ClavePlantillaEnum.QUERY_COBRO.getClave());
					if (null != ordenQueryCobroCobroRetrocesion) {
						elementosSelect = Arrays.asList(ordenQueryCobroCobroRetrocesion.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_COBRO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_cobro());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_COBRO.getError() + (ordenQueryCobroCobroRetrocesion != null ? StringHelper.safeNull(ordenQueryCobroCobroRetrocesion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());
					
					//Validamos que el literal <CUENTA_BANCARIA DE LA PLANTILLA> exista en la QUERY_COBRO
					literalesQuery = new ArrayList<String>();
					literalesQuery.add("<CUENTA_BANCARIA DE LA PLANTILLA>");
					if (!AccountingHelper.hayUnLiteral(plantilla.getQuery_cobro(), literalesQuery)) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_LITERAL_QUERY_COBRO.getError() + literalesQuery);
					}

					// Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/******************************************************QUERY_BLOQUEO******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_BLOQUEO
				if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_BLOQUEO
					Tmct0AccountingValidation ordenQueryBloqueoCobroRetrocesion = accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo(), ClavePlantillaEnum.QUERY_BLOQUEO.getClave());
					if (null != ordenQueryBloqueoCobroRetrocesion) {
						elementosSelect = Arrays.asList(ordenQueryBloqueoCobroRetrocesion.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_BLOQUEO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_bloqueo());

					//Validamos que los elementos estén en el orden indicado de la QUERY_BLOQUEO
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_BLOQUEO.getError()
							+ (ordenQueryBloqueoCobroRetrocesion != null ? StringHelper.safeNull(ordenQueryBloqueoCobroRetrocesion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					// Validamos que la QUERY_BLOQUEO no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					// Validamos que la QUERY_BLOQUEO no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "DESC_REFERENCIA", ConstantesQueries.QUERY_BLOQUEO.getValue());

					// Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/**************************************************QUERY_APUNTE_DETALLE***************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_APUNTE_DETALLE
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation ordenQueryApunteDetalleCobrosRetroc = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
					if (null != ordenQueryApunteDetalleCobrosRetroc) {
						elementosSelect = Arrays.asList(ordenQueryApunteDetalleCobrosRetroc.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_APUNTE_DETALLE
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_APUNTE_DETALLE.getError() 
							+ (ordenQueryApunteDetalleCobrosRetroc != null ? StringHelper.safeNull(ordenQueryApunteDetalleCobrosRetroc.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					// Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					// Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
					this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE contenga una ?1, ?2 y ?3
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDAPUNTE",ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "DESC_REFERENCIA",ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
				}
			}

		} catch (Exception e) {
			LOG.error("Error método validarEspecificosCobroRetrocesion: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo LIQUIDACION.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosLiquidacion(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosImportes = new ArrayList<String>();
		List<String> elementosAuxiliares = new ArrayList<String>();
		List<String> elementosAuxiliaresSinComillas = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();
		List<String> literalesQuery = new ArrayList<String>();
		int tamanioImportes = 0;
		int tamanioAuxiliares = 0;

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla,TipoPlantillaEnum.LIQUIDACION.getTipo());

				//Se valida ESTADO_FINAL
				validarCampoEstadoFinal(plantilla);

				//Se valida CAMPO_ESTADO
				validarCampoEstado(plantilla);

				//Se valida el CAMPO
				if (Strings.isNullOrEmpty(plantilla.getCampo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_CAMPO_PLANTILLA.getError());
				}

				//Se realizan las validaciones específicas de este tipo de plantilla
				//Validamos el campo ESTADO_PARCIAL
				// MFG no se valida, es un estado que ya no se utiliza
//				if (Strings.isNullOrEmpty(plantilla.getEstado_parcial())) {
//					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_PARCIAL_PLANTILLA.getError());
//				} else {
//					//Comprobamos que el estado exista en la tambla TMCT0ESTADO
//					Tmct0estado estado = estadoDao.findByNombre(plantilla.getEstado_parcial());
//					if (null==estado) {
//						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_PARCIAL_PLANTILLA_EN_TABLA_ESTADOS.getError());
//					}
//				}


				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenLiquidacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.LIQUIDACION.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null!=ordenQueryOrdenLiquidacion) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryOrdenLiquidacion, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() + (ordenQueryOrdenLiquidacion != null ? StringHelper.safeNull(ordenQueryOrdenLiquidacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Recuperamos los literales que debe de tener la QUERY_ORDEN
					Tmct0AccountingValidation literalesLiquidacionOrden = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.LIQUIDACION.getTipo(), ClavePlantillaEnum.LITERALES_QUERIES.getClave());
					if (null!=literalesLiquidacionOrden) {
						literalesQuery = Arrays.asList(literalesLiquidacionOrden.getValor().split(","));
					}

					//Validamos que uno de los literales recuperados estén en la QUERY_ORDEN pero en ningún caso los dos
					if (!AccountingHelper.haySoloUnLiteral(plantilla.getQuery_orden(), literalesQuery)) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_UNICO_LITERAL_QUERY_ORDEN.getError()+literalesQuery);
					}

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosImportes = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
					elementosAuxiliaresSinComillas = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					literalesQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/*******************************************************QUERY_COBRO*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_COBRO
				if (Strings.isNullOrEmpty(plantilla.getQuery_cobro())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_COBRO
					Tmct0AccountingValidation ordenQueryCobroLiquidacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.LIQUIDACION.getTipo(), ClavePlantillaEnum.QUERY_COBRO.getClave());
					if (null!=ordenQueryCobroLiquidacion) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryCobroLiquidacion, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_COBRO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_cobro());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_COBRO.getError() + (ordenQueryCobroLiquidacion != null ? StringHelper.safeNull(ordenQueryCobroLiquidacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosImportes = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					literalesQuery = new ArrayList<String>();
				}


				/*************************************************************************************************************************/
				/******************************************************QUERY_BLOQUEO******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_BLOQUEO
				if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_BLOQUEO
					Tmct0AccountingValidation ordenQueryBloqueoLiquidacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.LIQUIDACION.getTipo(), ClavePlantillaEnum.QUERY_BLOQUEO.getClave());
					if (null!=ordenQueryBloqueoLiquidacion) {
						elementosSelect = Arrays.asList(ordenQueryBloqueoLiquidacion.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_BLOQUEO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_bloqueo());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_BLOQUEO.getError() 
							+ (ordenQueryBloqueoLiquidacion != null ? StringHelper.safeNull(ordenQueryBloqueoLiquidacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_BLOQUEO no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "DESC_REFERENCIA", ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosImportes = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					literalesQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/**************************************************QUERY_APUNTE_DETALLE***************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_APUNTE_DETALLE
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation ordenQueryApunteDetalleLiquidacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.LIQUIDACION.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
					if (null!=ordenQueryApunteDetalleLiquidacion) {
						elementosSelect = Arrays.asList(ordenQueryApunteDetalleLiquidacion.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_APUNTE_DETALLE
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_APUNTE_DETALLE.getError() 
							+ (ordenQueryApunteDetalleLiquidacion != null ? StringHelper.safeNull(ordenQueryApunteDetalleLiquidacion.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
					this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE contenga una ?1, ?2
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDAPUNTE", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "DESC_REFERENCIA", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
				}
			}

		} catch (Exception e) {
			LOG.error("Error método validarEspecificosLiquidacion: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo PERDIDAS_GANANCIAS.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosPerdidasGanancias(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosImportes = new ArrayList<String>();
		List<String> elementosAuxiliares = new ArrayList<String>();
		List<String> elementosAuxiliaresSinComillas = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {

				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla,TipoPlantillaEnum.P_Y_G.getTipo());

				/*************************************************************************************************************************/
				/*******************************************************QUERY_COBRO*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_COBRO
				if (Strings.isNullOrEmpty(plantilla.getQuery_cobro())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					int tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					int tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_COBRO
					Tmct0AccountingValidation ordenQueryCobroPyG = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.P_Y_G.getTipo(), ClavePlantillaEnum.QUERY_COBRO.getClave());
					if (null!=ordenQueryCobroPyG) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryCobroPyG, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_COBRO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_cobro());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_COBRO.getError() + (ordenQueryCobroPyG != null ? StringHelper.safeNull(ordenQueryCobroPyG.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());
				}

			}

		} catch (Exception e) {
			LOG.error("Error método validarEspecificosPerdidasGanancias: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo DEVENGO_FALLIDOS.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosDevengosFallidos(List<Tmct0ContaPlantillas> plantillas) {
		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosImportes = new ArrayList<String>();
		List<String> elementosAuxiliares = new ArrayList<String>();
		List<String> elementosAuxiliaresSinComillas = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();
		List<String> literalesQuery = new ArrayList<String>();
		int tamanioImportes = 0;
		int tamanioAuxiliares = 0;
		
		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla, TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo());

				//Se valida ESTADO_FINAL
				validarCampoEstadoFinal(plantilla);

				//Se valida CAMPO_ESTADO
				validarCampoEstado(plantilla);
				
				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenDevengoFallidos = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGO_FALLIDOS.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null!=ordenQueryOrdenDevengoFallidos) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryOrdenDevengoFallidos, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() + (ordenQueryOrdenDevengoFallidos != null ? StringHelper.safeNull(ordenQueryOrdenDevengoFallidos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_ORDEN contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_ORDEN.getValue());

					//Recuperamos los literales que debe de tener la QUERY_ORDEN
					Tmct0AccountingValidation literalesDevengoAnulacion = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGO_FALLIDOS.getTipo(), ClavePlantillaEnum.LITERALES_QUERIES.getClave());
					if (null!=literalesDevengoAnulacion) {
						literalesQuery = Arrays.asList(literalesDevengoAnulacion.getValor().split(","));
					}

					//Validamos que los literales recuperados estén en la QUERY_ORDEN
					if (!AccountingHelper.hayUnLiteral(plantilla.getQuery_orden(), literalesQuery)) {
						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_LITERAL_QUERY_ORDEN.getError()+literalesQuery);
					}

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}


				/*************************************************************************************************************************/
				/******************************************************QUERY_BLOQUEO******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_BLOQUEO
				if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_BLOQUEO
					Tmct0AccountingValidation ordenQueryBloqueoDevengoFallidos = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGO_FALLIDOS.getTipo(), ClavePlantillaEnum.QUERY_BLOQUEO.getClave());
					if (null != ordenQueryBloqueoDevengoFallidos) {
						elementosSelect = Arrays.asList(ordenQueryBloqueoDevengoFallidos.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_BLOQUEO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_bloqueo());

					//Validamos que los elementos estén en el orden indicado de la QUERY_BLOQUEO
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_BLOQUEO.getError()
							+ (ordenQueryBloqueoDevengoFallidos != null ? StringHelper.safeNull(ordenQueryBloqueoDevengoFallidos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_BLOQUEO no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "FECHA_EJECUCION", ConstantesQueries.QUERY_BLOQUEO.getValue());
					// MFG se comenta la siguiente comprobación ya que se ha cambiado y el parametro no viene en la query y se forma dinamicamente.
					//this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					elementosImportes = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
					elementosAuxiliaresSinComillas = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/**************************************************QUERY_APUNTE_DETALLE***************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_APUNTE_DETALLE
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation ordenQueryApunteDetalleDevengoFallidos = accountingValidationDao.findByTipoAndClave(TipoPlantillaEnum.DEVENGO_FALLIDOS.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
					if (null!=ordenQueryApunteDetalleDevengoFallidos) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryApunteDetalleDevengoFallidos, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_APUNTE_DETALLE
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

					//Validamos que los elementos estén en el orden indicado de la QUERY_APUNTE_DETALLE
					AccountingHelper.validarOrden(plantilla.getCodigo(),elementosSelect,elementosSelectQuery,elementosSelect.size(),
							ConstantesError.ERROR_ORDEN_QUERY_APUNTE_DETALLE.getError() 
							+ (ordenQueryApunteDetalleDevengoFallidos != null ? StringHelper.safeNull(ordenQueryApunteDetalleDevengoFallidos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
					this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE contenga una ?1, ?2, ?3, ?4 y fall.ID = ?4
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDAPUNTE", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "ESTADO_BLOQUEADO", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?3", "FECHA_EJECUCION", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?4", "FALL.ID", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());	
				}

			}
		} catch (Exception e) {
			LOG.error("Error método validarEspecificosDevengosFallidos: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo COBRO_BARRIDOS.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosCobrosBarridos(List<Tmct0ContaPlantillas> plantillas) {

		List<String> elementosSelect = new ArrayList<String>();
		List<String> elementosSelectQuery = new ArrayList<String>();
		List<String> literalesQuery = new ArrayList<String>();
		List<String> elementosImportes = new ArrayList<String>();
		List<String> elementosAuxiliares = new ArrayList<String>();
		List<String> elementosAuxiliaresSinComillas = new ArrayList<String>();

		try {

			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				validarCamposObligatorios(plantilla,TipoPlantillaEnum.COBRO_BARRIDOS.getTipo());

				//Se valida ESTADO_FINAL
				validarCampoEstadoFinal(plantilla);

				//Se valida CAMPO_ESTADO
				validarCampoEstado(plantilla);

				//Se realizan las validaciones específicas de este tipo de plantilla
				//Validamos el campo ESTADO_PARCIAL
				// No se valida,  es un estado que ya no se utiliza
//				if (Strings.isNullOrEmpty(plantilla.getEstado_parcial())) {
//					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_PARCIAL_PLANTILLA.getError());
//				} else {
//					//Comprobamos que el estado exista en la tambla TMCT0ESTADO
//					Tmct0estado estado = estadoDao.findByNombre(plantilla.getEstado_parcial());
//					if (null==estado) {
//						Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_PARCIAL_PLANTILLA_EN_TABLA_ESTADOS.getError());
//					}
//				}

				/*************************************************************************************************************************/
				/*******************************************************QUERY_ORDEN*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_ORDEN
				if (Strings.isNullOrEmpty(plantilla.getQuery_orden())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					int tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					int tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_ORDEN
					Tmct0AccountingValidation ordenQueryOrdenCobrosBarridos = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBRO_BARRIDOS.getTipo(), ClavePlantillaEnum.QUERY_ORDEN.getClave());
					if (null != ordenQueryOrdenCobrosBarridos) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryOrdenCobrosBarridos, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_ORDEN
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_orden());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_ORDEN.getError() 
							+ (ordenQueryOrdenCobrosBarridos != null ? StringHelper.safeNull(ordenQueryOrdenCobrosBarridos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Recuperamos los literales que debe de tener la QUERY_ORDEN
					Tmct0AccountingValidation literalesCobrosBarridos = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBRO_BARRIDOS.getTipo(), ClavePlantillaEnum.LITERALES_QUERIES.getClave());
					if (null != literalesCobrosBarridos) {
						literalesQuery = Arrays.asList(literalesCobrosBarridos.getValor().split(","));
					}

					//Validamos que los literales recuperados estén en la QUERY_ORDEN
					if (!AccountingHelper.hayUnLiteral(plantilla.getQuery_orden(), literalesQuery)) {
						Loggers.logErrorValidationAccounting(
								plantilla.getCodigo(), ConstantesError.ERROR_LITERAL_QUERY_ORDEN.getError() + literalesQuery);
					}

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
					elementosImportes = new ArrayList<String>();
					elementosAuxiliares = new ArrayList<String>();
					elementosAuxiliaresSinComillas = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/*******************************************************QUERY_COBRO*******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_COBRO
				if (Strings.isNullOrEmpty(plantilla.getQuery_cobro())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_COBRO
					Tmct0AccountingValidation ordenQueryCobroCobrosBarridos = accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBRO_BARRIDOS.getTipo(), ClavePlantillaEnum.QUERY_COBRO.getClave());
					if (null != ordenQueryCobroCobrosBarridos) {
						elementosSelect = Arrays.asList(ordenQueryCobroCobrosBarridos.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_COBRO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_cobro());

					//Validamos que los elementos estén en el orden indicado
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_COBRO.getError() + (ordenQueryCobroCobrosBarridos != null ? StringHelper.safeNull(ordenQueryCobroCobrosBarridos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/******************************************************QUERY_BLOQUEO******************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_BLOQUEO
				if (Strings.isNullOrEmpty(plantilla.getQuery_bloqueo())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO_PLANTILLA.getError());
				} else {
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_BLOQUEO
					Tmct0AccountingValidation ordenQueryBloqueoCobrosBarridos = accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBRO_BARRIDOS.getTipo(), ClavePlantillaEnum.QUERY_BLOQUEO.getClave());
					if (null != ordenQueryBloqueoCobrosBarridos) {
						elementosSelect = Arrays.asList(ordenQueryBloqueoCobrosBarridos.getValor().split(","));
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_BLOQUEO
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_bloqueo());

					//Validamos que los elementos estén en el orden indicado de la QUERY_BLOQUEO
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_BLOQUEO.getError()
							+ (ordenQueryBloqueoCobrosBarridos != null ? StringHelper.safeNull(ordenQueryBloqueoCobrosBarridos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					//Validamos que la QUERY_BLOQUEO no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Validamos que la QUERY_BLOQUEO contenga una ?1
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDMOVIMIENTO", ConstantesQueries.QUERY_BLOQUEO.getValue());

					//Reiniciamos las listas de elementos
					elementosSelect = new ArrayList<String>();
					elementosSelectQuery = new ArrayList<String>();
				}

				/*************************************************************************************************************************/
				/**************************************************QUERY_APUNTE_DETALLE***************************************************/
				/*************************************************************************************************************************/
				//Validamos el campo QUERY_APUNTE_DETALLE
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					//Recuperamos el número de importes que va a haber
					elementosImportes =  Arrays.asList(plantilla.getCuenta_debe().split(","));
					int tamanioImportes = elementosImportes.size() == 0?1:elementosImportes.size();
					//Recuperamos el número de auxiliares que va a haber
					if (plantilla.getAux_debe() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_debe().split(",")));
					}
					if (plantilla.getAux_haber() != null) {
						elementosAuxiliares.addAll(Arrays.asList(plantilla.getAux_haber().split(",")));	
					}
					//Eliminamos los auxiliares que vayan entrecomillados
					for (int i=0;i<elementosAuxiliares.size();i++) {
						if ((!elementosAuxiliares.get(i).startsWith("\"") || !elementosAuxiliares.get(i).endsWith("\"")) && !elementosAuxiliares.get(i).equals("")) {
							elementosAuxiliaresSinComillas.add(elementosAuxiliares.get(i));
						}
					}
					int tamanioAuxiliares = elementosAuxiliaresSinComillas.size();
					//Recuperamos el orden que debe tener los elementos del SELECT de QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation ordenQueryApunteDetalleCobrosBarridos = this.accountingValidationDao.findByTipoAndClave(
							TipoPlantillaEnum.COBRO_BARRIDOS.getTipo(), ClavePlantillaEnum.QUERY_APUNTE_DETALLE.getClave());
					if (null != ordenQueryApunteDetalleCobrosBarridos) {
						elementosSelect = rellenarListaElementosSelect(ordenQueryApunteDetalleCobrosBarridos, tamanioImportes, tamanioAuxiliares);
					}

					//Recuperamos el orden de los elementos del SELECT de la QUERY_APUNTE_DETALLE
					elementosSelectQuery = getListCamposQuery(plantilla.getQuery_apunte_detalle());

					//Validamos que los elementos estén en el orden indicado de la QUERY_APUNTE_DETALLE
					AccountingHelper.validarOrden(
							plantilla.getCodigo(), elementosSelect, elementosSelectQuery, elementosSelect.size(), 
							ConstantesError.ERROR_ORDEN_QUERY_APUNTE_DETALLE.getError() + (ordenQueryApunteDetalleCobrosBarridos != null ? StringHelper.safeNull(ordenQueryApunteDetalleCobrosBarridos.getValor()) : "")
							+ ConstantesError.ERROR_ORDEN_INTRODUCIDO.getError() + elementosSelectQuery.toString());

					// Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
					this.validarGroupByQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					// Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
					this.validarHavingQueryByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					//Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
					this.validarSumByTipoQuery(plantilla, ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					// Validamos que la QUERY_APUNTE_DETALLE contenga una ?1, ?2
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?1", "IDAPUNTE", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());
					this.validarParamInterrogQueryByTipoQuery(plantilla, "?2", "IDMOVIMIENTO", ConstantesQueries.QUERY_APUNTE_DETALLE.getValue());

					elementosImportes = new ArrayList<String>();
				}

			}

		} catch (Exception e) {
			LOG.error("Error método validarEspecificosCobrosBarridos: " + e.getMessage());
		}
	}

	/**
	 * Validaciones de las plantillas de tipo APUNTE_MANUAL.
	 * 
	 * @param plantillas: plantillas a validar
	 */
	public void validarEspecificosApunteManual(List<Tmct0ContaPlantillas> plantillas) {

		try {
			for (Tmct0ContaPlantillas plantilla : plantillas) {
				//Se validan los campos obligatorios comunes
				//validarCamposObligatorios(plantilla);

				//Se realizan las validaciones específicas de este tipo de plantilla

			}

		} catch (Exception e) {
			LOG.error("Error método validarEspecificosApunteManual: " + e.getMessage());
		}
	}

	/**
	 * 	Se valida que la query que se le pasa no contenga GROUP BY segun su tipo.
	 * 	@param plantilla: plantilla propiamente dicha
	 * 	@param tipoQuery: puede ser de bloqueo, apunte detalle, etc.
	 */
	private void validarGroupByQueryByTipoQuery(Tmct0ContaPlantillas plantilla, String tipoQuery) {
		try {
			if (tipoQuery.equals(ConstantesQueries.QUERY_BLOQUEO.getValue())) {
				// Validamos que la QUERY_BLOQUEO no contenga GROUP BY
				int numeroGroupByBloqueo = AccountingHelper.contarCaracteres(plantilla.getQuery_bloqueo(), "GROUP BY");
				if (numeroGroupByBloqueo > 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), ConstantesError.ERROR_GROUPBY_QUERY_BLOQUEO.getError());
				}
			} else if (tipoQuery.equals(ConstantesQueries.QUERY_APUNTE_DETALLE.getValue())) {
				// Validamos que la QUERY_APUNTE_DETALLE no contenga GROUP BY
				int numeroGroupByApunteDetalle = AccountingHelper.contarCaracteres(plantilla.getQuery_apunte_detalle(), "GROUP BY");
				if (numeroGroupByApunteDetalle > 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), ConstantesError.ERROR_GROUPBY_QUERY_APUNTE_DETALLE.getError());
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarGroupByQueryByTipoQuery: " + e.getMessage());
		}
	}

	/**
	 * 	Se valida que la query que se le pasa no contenga HAVING segun su tipo.
	 * 	@param plantilla: plantilla propiamente dicha
	 * 	@param tipoQuery: puede ser de bloqueo, apunte detalle, etc.
	 */
	private void validarHavingQueryByTipoQuery(Tmct0ContaPlantillas plantilla, String tipoQuery) {
		try {
			if (tipoQuery.equals(ConstantesQueries.QUERY_BLOQUEO.getValue())) {
				// Validamos que la QUERY_BLOQUEO no contenga HAVING
				int numeroHavingBloqueo = AccountingHelper.contarCaracteres(
						plantilla.getQuery_bloqueo(), "HAVING");
				if (numeroHavingBloqueo > 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), ConstantesError.ERROR_HAVING_QUERY_BLOQUEO.getError());
				}
			} else if (tipoQuery.equals(ConstantesQueries.QUERY_APUNTE_DETALLE.getValue())) {
				// Validamos que la QUERY_APUNTE_DETALLE no contenga HAVING
				int numeroHavingApunteDetalle = AccountingHelper.contarCaracteres(
						plantilla.getQuery_apunte_detalle(), "HAVING");
				if (numeroHavingApunteDetalle > 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), ConstantesError.ERROR_HAVING_QUERY_APUNTE_DETALLE.getError());
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarHavingQueryByTipoQuery: " + e.getMessage());
		}
	}
	
	/**
	 * 	Se valida que la query que se le pasa no contenga SUM( ni SUM ( segun su tipo.
	 * 	@param plantilla: plantilla propiamente dicha
	 * 	@param tipoQuery: puede ser de bloqueo, apunte detalle, etc.
	 */
	private void validarSumByTipoQuery(Tmct0ContaPlantillas plantilla, String tipoQuery) {
		try {
			if (tipoQuery.equals(ConstantesQueries.QUERY_APUNTE_DETALLE.getValue())) {
				// Validamos que la QUERY_APUNTE_DETALLE no contenga SUM( ni SUM (
				int numeroSumApunteDetalle = AccountingHelper.contarCaracteres(plantilla.getQuery_apunte_detalle(), "SUM(");
				int numeroSumEspacioApunteDetalle = AccountingHelper.contarCaracteres(plantilla.getQuery_apunte_detalle(), "SUM (");
				if (numeroSumApunteDetalle > 0 || numeroSumEspacioApunteDetalle>0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), ConstantesError.ERROR_SUM_QUERY_APUNTE_DETALLE.getError());
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarSumByTipoQuery: " + e.getMessage());
		}
	}

	/**
	 *	Valida el parametro de interrogacion informando en una traza por tipo de query.
	 *	@param plantilla: plantilla
	 *	@param param: el parametro de interrogacion que se pasa
	 * 	@param tipoQuery: puede ser de bloqueo, apunte detalle, etc.
	 */
	private void validarParamInterrogQueryByTipoQuery(
			Tmct0ContaPlantillas plantilla, String param, String valor, String tipoQuery) {

		try {

			if (tipoQuery.equals(ConstantesQueries.QUERY_BLOQUEO.getValue())) {
				int numeroInterrogacionesBloqueo = AccountingHelper.contarCaracteres(plantilla.getQuery_bloqueo(), param);
				if (numeroInterrogacionesBloqueo == 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), 
							ConstantesError.ERROR_INTERROGACION_QUERY_BLOQUEO.getError() + param + ConstantesError.ERROR_INTERROGACION_AUX.getError() + valor);
				}
			} else if (tipoQuery.equals(ConstantesQueries.QUERY_APUNTE_DETALLE.getValue())) {	
				int numeroInterrogacionesApunteDetalle = AccountingHelper.contarCaracteres(
						plantilla.getQuery_apunte_detalle(), param);
				if (numeroInterrogacionesApunteDetalle == 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), 
							ConstantesError.ERROR_INTERROGACIONES_QUERY_APUNTE_DETALLE.getError() + param + ConstantesError.ERROR_INTERROGACION_AUX.getError() + valor);
				}
			} else if (tipoQuery.equals(ConstantesQueries.QUERY_ORDEN.getValue())) {
				int numeroInterrogacionesApunteDetalle = AccountingHelper.contarCaracteres(
						plantilla.getQuery_orden(), param);
				if (numeroInterrogacionesApunteDetalle == 0) {
					Loggers.logErrorValidationAccounting(
							plantilla.getCodigo(), 
							ConstantesError.ERROR_INTERROGACIONES_QUERY_ORDEN.getError() + param + ConstantesError.ERROR_INTERROGACION_AUX.getError() + valor);
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarParamInterrogQueryByTipoQuery: " + e.getMessage());
		}
	}

	/**
	 *	Devuelve una lista de elementos que debe tener el SELECT de la query en función de los valores
	 *	obtenidos de BBDD + los importes que apliquen
	 *	@param ordenQuery: Listado de valores del SELECT obtenidos de la tabla de TMCT0_CONTA_VALIDATION
	 *	@param tamanioImportes: número de importes que debe tener el SELECT
	 * 	@return elementosSelect: Listado de elementos que debe tener el SELECT
	 */
	private List<String> rellenarListaElementosSelect (Tmct0AccountingValidation ordenQuery, int tamanioImportes, int tamanioAuxiliares) {
		List<String> elementosSelect = new ArrayList<String>();
		try {
			List<String> elementosSelectBBDD = Arrays.asList(ordenQuery.getValor().split(","));
			for (int i=0;i<elementosSelectBBDD.size();i++){
				if (elementosSelectBBDD.get(i).trim().equals("IMPORTE")) {
					for (int j=0; j<tamanioImportes;j++) {
						elementosSelect.add("IMPORTE");
					}
				} else if (elementosSelectBBDD.get(i).trim().equals("AUX")){
					for (int k=0; k<tamanioAuxiliares;k++) {
						elementosSelect.add("AUX");
					}
				} else {
					elementosSelect.add(elementosSelectBBDD.get(i));
				}
			}
		} catch (Exception e) {
			LOG.error("Error método rellenarListaElementosSelect: " + e.getMessage());
		}
		return elementosSelect;
	}

	/**
	 *	Valida si el CAMPO_ESTADO está relleno y en caso contrario muestra un error en trazas
	 *	@param plantilla: plantilla en la que comprueba el CAMPO_ESTADO
	 */
	private void validarCampoEstado(Tmct0ContaPlantillas plantilla) {
		try {
			//Validamos el campo CAMPO_ESTADO
			if (Strings.isNullOrEmpty(plantilla.getCampo_estado())) {
				Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_CAMPO_ESTADO_PLANTILLA.getError());
			} else {
				//Validamos el campo QUERY_APUNTE_DETALLE ya que lo necesitamos para encontrar en el el CAMPO_ESTADO
				if (Strings.isNullOrEmpty(plantilla.getQuery_apunte_detalle())) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_APUNTE_DETALLE_PLANTILLA.getError());
				} else {
					//Obtenemos el nombre de la tabla a la que hace el FROM la QUERY_APUNTE_DETALLE
					Tmct0AccountingValidation datosDevengo = accountingValidationDao.findByTipoAndClave(getTipoPlantillaValidacion(plantilla.getTipo()), ClavePlantillaEnum.TABLA_MAESTRA.getClave());

					//Comprobamos si el CAMPO_ESTADO es una columna de la tabla obtenida por properties
					if (null!=datosDevengo) {
						List<Object> listaTabla = accountingValidationDaoImp.consultarColumna(plantilla.getCampo_estado(), datosDevengo.getValor());
						if (null==listaTabla || listaTabla.size()==0) {
							Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_CAMPO_ESTADO_EN_TABLA.getError()+datosDevengo.getValor());
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarCampoEstado: " + e.getMessage());
		}
	}

	/**
	 *	Valida si el ESTADO_FINAL está relleno y en caso contrario muestra un error en trazas
	 *	@param plantilla: plantilla en la que comprueba el ESTADO_FINAL
	 */
	private void validarCampoEstadoFinal(Tmct0ContaPlantillas plantilla) {
		try {
			//Validamos el campo ESTADO_FINAL
			if (Strings.isNullOrEmpty(plantilla.getEstado_final())) {
				Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_FINAL_PLANTILLA.getError());
			} else {
				//Comprobamos que el estado exista en la tambla TMCT0ESTADO
				Tmct0estado estado = estadoDao.findByNombre(plantilla.getEstado_final());
				if (null==estado) {
					Loggers.logErrorValidationAccounting(plantilla.getCodigo(), ConstantesError.ERROR_ESTADO_FINAL_PLANTILLA_EN_TABLA_ESTADOS.getError());
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarCampoEstadoFinal: " + e.getMessage());
		}
	}
		
	/**
	 *	Obtiene el tipo de validacion de la plantilla a partir del tipo de plantilla
	 *	@param tipo_plantilla: Tipo de la plantilla
	 */
    public String getTipoPlantillaValidacion (String sTipoPlantilla) {
    	
    	switch (sTipoPlantilla) {
		case Constantes.DEVENGO:
			return TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo();
		case Constantes.ANULACION:
			return TipoPlantillaEnum.DEVENGOS_ANULACION.getTipo();
		case Constantes.COBRO:
			return TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo();
		case Constantes.RETROCESION:
			return TipoPlantillaEnum.COBROS_RETROCESIONES.getTipo();
		case Constantes.DOTACION:
			return TipoPlantillaEnum.DOTACION_DESDOTACION.getTipo();
		case Constantes.DESDOTACION:
			return TipoPlantillaEnum.DOTACION_DESDOTACION.getTipo();
		case Constantes.DEVENGO_FALLIDOS:
			return TipoPlantillaEnum.DEVENGO_FALLIDOS.getTipo();
		case Constantes.EMISION_FACTURA:
			return TipoPlantillaEnum.EMISION_FACTURA.getTipo();		
		case Constantes.LIQUIDACION_TOTAL:
			return TipoPlantillaEnum.LIQUIDACION.getTipo();		
		case Constantes.LIQUIDACION_PARCIAL:
			return TipoPlantillaEnum.LIQUIDACION.getTipo();		
		case Constantes.PERDIDAS_GANANCIAS:
			return TipoPlantillaEnum.P_Y_G.getTipo();		
		case Constantes.COBRO_BARRIDOS:
			return TipoPlantillaEnum.COBRO_BARRIDOS.getTipo();		
		case Constantes.DEVENGO_FACTURAS:
			return TipoPlantillaEnum.DEVENGO_FACTURAS.getTipo();		
		default:
			return sTipoPlantilla;  // El tipo es el mismo
		}   
    	
    }
	
}
