/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;

/**
 * @author lucio.vilar
 *
 */
@Repository
public interface Tmct0ContaConciliacionAuditDao extends JpaRepository<Tmct0ContaConciliacionAudit, Integer> {

}
