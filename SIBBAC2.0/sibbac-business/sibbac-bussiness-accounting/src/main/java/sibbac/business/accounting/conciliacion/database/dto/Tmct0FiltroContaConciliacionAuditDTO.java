/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dto;

import java.util.Date;
import java.util.List;

/**
 * @author lucio.vilar
 *
 */
public class Tmct0FiltroContaConciliacionAuditDTO {

	/**
	 * fecha desde
	 */
	private Date fechaDesde;

	/**
	 * fecha hasta
	 */
	private Date fechaHasta;
	
	/**
	 * lista de fechas
	 */
	private List<Date> fechas;

	/**
	 * usuario
	 */
	private String user;

	public Tmct0FiltroContaConciliacionAuditDTO() {
		
	}
	
	public Tmct0FiltroContaConciliacionAuditDTO(Date desde, Date hasta, String usuario) {
		this.fechaDesde = desde;
		this.fechaHasta = hasta;
		this.user = usuario;
	}

	/**
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return fechaDesde;
	}

	/**
	 * @param fechaDesde the fechaDesde to set
	 */
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * @param fechaHasta the fechaHasta to set
	 */
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the fechas
	 */
	public List<Date> getFechas() {
		return fechas;
	}

	/**
	 * @param fechas the fechas to set
	 */
	public void setFechas(List<Date> fechas) {
		this.fechas = fechas;
	}
}
