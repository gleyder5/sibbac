package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entidad para la gestion de TMCT0_CONTA_PARTIDAS_CASADAS.
 */
@Entity
@Table(name = "TMCT0_CONTA_PARTIDAS_CASADAS")
public class Tmct0ContaPartidasCasadas {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_CONCILIACION_AUDIT")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaConciliacionAudit conciliacionAudit;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_APUNTES_PNDTS")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaApuntesPndts apuntesPndts;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_APUNTES_AUDIT")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaApuntesAudit apuntesAudit;
	
	@Column(name = "AUX_BANCARIO", length = 10, nullable = true)
	private String auxBancario;
	
	@Column(name = "BOOKING_DATE", length = 10, nullable = true)
	private Date bookingDate;
	
	@Column(name = "VALUE_DATE", length = 10, nullable = true)
	private Date valueDate;
	
	@Column(name = "CURRENCY", length = 10, nullable = true)
	private String currency;

	@Column(name = "TIPO", length = 100, nullable = true)
	private String tipo;
	
	@Column(name = "COMENTARIO", length = 500, nullable = true)
	private String comentario;
	
	@Column(name = "DEPARTAMENTO", length = 200, nullable = true)
	private String departamento;
	
	@Column(name = "REFERENCIA", length = 100, nullable = true)
	private String referencia;
	
	@Column(name = "REFERENCIA2", length = 100, nullable = true)
	private String referencia2;
	
	@Column(name = "NUM_DOCUMENTO", length = 10, nullable = true)
	private String numDocumento;
	
	@Column(name = "NUM_DOCUMENTO2", length = 10, nullable = true)
	private String numDocumento2;
	
	@Column(name = "GIN", length = 8, nullable = true)
	private BigInteger gin;
	
	@Column(name = "COMENTARIO_USUARIO", length = 500, nullable = true)
	private String comentarioUsuario;
	
	@Column(name = "ANTIGUEDAD", length = 10, nullable = true)
	private String antiguedad;
	
	@Column(name = "IMPORTE", nullable = true, precision = 18, scale = 4)
	private BigDecimal importe;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaPartidasCasadas() {
		super();
	}

	/**
	 * @param id
	 * @param codigo
	 * @param tipo
	 * @param subtipo
	 * @param auxiliar
	 * @param tolerance
	 * @param selectQuery
	 * @param fromQuery
	 * @param whereQuery
	 * @param havingQuery
	 * @param groupBy
	 * @param orderBy
	 */
	public Tmct0ContaPartidasCasadas(BigInteger id, Tmct0ContaConciliacionAudit conciliacionAudit,
			Tmct0ContaApuntesPndts apuntesPndts, Tmct0ContaApuntesAudit apuntesAudit, String auxBancario,
			Date bookingDate, Date valueDate, String currency, String tipo, String comentario,
			String departamento, String referencia, String referencia2, String numDocumento,
			String numDocumento2, BigInteger gin, String comentarioUsuario,	String antiguedad, BigDecimal importe) {
		super();
		this.id = id;
		this.conciliacionAudit = conciliacionAudit;
		this.apuntesPndts = apuntesPndts;
		this.apuntesAudit = apuntesAudit;
		this.auxBancario = auxBancario;
		this.bookingDate = bookingDate;
		this.valueDate = valueDate;
		this.currency = currency;
		this.tipo = tipo;
		this.comentario = comentario;
		this.departamento = departamento;
		this.referencia = referencia;
		this.referencia2 = referencia2;
		this.numDocumento = numDocumento;
		this.numDocumento2 = numDocumento2;
		this.gin = gin;
		this.comentarioUsuario = comentarioUsuario;
		this.antiguedad = antiguedad;
		this.importe = importe;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the conciliacionAudit
	 */
	public Tmct0ContaConciliacionAudit getConciliacionAudit() {
		return conciliacionAudit;
	}

	/**
	 * @param conciliacionAudit the conciliacionAudit to set
	 */
	public void setConciliacionAudit(Tmct0ContaConciliacionAudit conciliacionAudit) {
		this.conciliacionAudit = conciliacionAudit;
	}

	/**
	 * @return the apuntesPndts
	 */
	public Tmct0ContaApuntesPndts getApuntesPndts() {
		return apuntesPndts;
	}

	/**
	 * @param apuntesPndts the apuntesPndts to set
	 */
	public void setApuntesPndts(Tmct0ContaApuntesPndts apuntesPndts) {
		this.apuntesPndts = apuntesPndts;
	}

	/**
	 * @return the apuntesAudit
	 */
	public Tmct0ContaApuntesAudit getApuntesAudit() {
		return apuntesAudit;
	}

	/**
	 * @param apuntesAudit the apuntesAudit to set
	 */
	public void setApuntesAudit(Tmct0ContaApuntesAudit apuntesAudit) {
		this.apuntesAudit = apuntesAudit;
	}

	/**
	 * @return the auxBancario
	 */
	public String getAuxBancario() {
		return auxBancario;
	}

	/**
	 * @param auxBancario the auxBancario to set
	 */
	public void setAuxBancario(String auxBancario) {
		this.auxBancario = auxBancario;
	}

	/**
	 * @return the bookingDate
	 */
	public Date getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate the bookingDate to set
	 */
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @return the valueDate
	 */
	public Date getValueDate() {
		return valueDate;
	}

	/**
	 * @param valueDate the valueDate to set
	 */
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the referencia2
	 */
	public String getReferencia2() {
		return referencia2;
	}

	/**
	 * @param referencia2 the referencia2 to set
	 */
	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	/**
	 * @return the numDocumento
	 */
	public String getNumDocumento() {
		return numDocumento;
	}

	/**
	 * @param numDocumento the numDocumento to set
	 */
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	/**
	 * @return the numDocumento2
	 */
	public String getNumDocumento2() {
		return numDocumento2;
	}

	/**
	 * @param numDocumento2 the numDocumento2 to set
	 */
	public void setNumDocumento2(String numDocumento2) {
		this.numDocumento2 = numDocumento2;
	}

	/**
	 * @return the gin
	 */
	public BigInteger getGin() {
		return gin;
	}

	/**
	 * @param gin the gin to set
	 */
	public void setGin(BigInteger gin) {
		this.gin = gin;
	}

	/**
	 * @return the comentarioUsuario
	 */
	public String getComentarioUsuario() {
		return comentarioUsuario;
	}

	/**
	 * @param comentarioUsuario the comentarioUsuario to set
	 */
	public void setComentarioUsuario(String comentarioUsuario) {
		this.comentarioUsuario = comentarioUsuario;
	}

	/**
	 * @return the antiguedad
	 */
	public String getAntiguedad() {
		return antiguedad;
	}

	/**
	 * @param antiguedad the antiguedad to set
	 */
	public void setAntiguedad(String antiguedad) {
		this.antiguedad = antiguedad;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	
	
}
