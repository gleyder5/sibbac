package sibbac.business.accounting.enums;

public enum EstadosProcesadoCuadreEcc {

  PENDIENTE_VALIDAR_TITULOS(2000),
  TITULOS_ERRONEOS(2010),
  TITULOS_OK(2020);

  public int id;

  private EstadosProcesadoCuadreEcc(int id) {
    this.id = id;
  }

}
