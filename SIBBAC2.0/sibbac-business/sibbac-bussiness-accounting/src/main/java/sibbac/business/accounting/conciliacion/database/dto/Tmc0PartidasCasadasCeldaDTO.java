package sibbac.business.accounting.conciliacion.database.dto;

/**
 * DTO para cargar resultados de celdas
 * 
 * @author neoris
 *
 */
public class Tmc0PartidasCasadasCeldaDTO {

  /** Nombre de columna */
  private String columna;

  /** Valor del elemento */
  private Object contenido;

  /** Formato de la celda si es especial */
  private String formato;

  public Tmc0PartidasCasadasCeldaDTO(String columna, Object contenido, String tipo) {
    this.columna = columna;
    this.contenido = contenido;
    this.formato = tipo;
  }

  public String getValor() {
    return String.valueOf(contenido);
  }

  public String getColumna() {
    return columna;
  }

  public void setColumna(String columna) {
    this.columna = columna;
  }

  public Object getContenido() {
    return contenido;
  }

  public void setContenido(Object contenido) {
    this.contenido = contenido;
  }

  public String getFormato() {
    return formato;
  }

  public void setFormato(String formato) {
    this.formato = formato;
  }

}
