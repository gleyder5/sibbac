package sibbac.business.accounting.tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;

import sibbac.business.accounting.database.bo.EjecucionApuntesManuales;
import sibbac.business.accounting.database.bo.EjecucionCobrosBarridos;
import sibbac.business.accounting.database.bo.EjecucionCobrosRetrocesiones;
import sibbac.business.accounting.database.bo.EjecucionDevengoAnulacion;
import sibbac.business.accounting.database.bo.EjecucionDevengoFallidos;
import sibbac.business.accounting.database.bo.EjecucionDotacionDesdotacion;
import sibbac.business.accounting.database.bo.EjecucionEmisionFactura;
import sibbac.business.accounting.database.bo.EjecucionFacturasManuales;
import sibbac.business.accounting.database.bo.EjecucionLiquidacionesParciales;
import sibbac.business.accounting.database.bo.EjecucionLiquidacionesTotales;
import sibbac.business.accounting.database.bo.EjecucionPerdidasGanancias;
import sibbac.business.accounting.database.bo.Tmct0ContaApuntesBo;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDaoImp;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.exception.SendMailException;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.accounting.validation.ValidationAccounting;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.FtpSibbacContabilidad;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums.ActivacionContaPlantillas;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ACCOUNTING.NAME,
           name = Task.GROUP_ACCOUNTING.JOB_DESGLOSE,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "04:00:00")
public class TaskActualizacionDesglose extends SIBBACTask {

  @Value("${sibbac.accounting.cuerpo.mail.desgloseclitit}")
  private String bodyMail;

  @Value("${sibbac.accounting.cuerpo.mail.desgloseclitit.diferentesValores}")
  private String bodyMailIdRepetidos;

  @Value("${sibbac.accounting.cuerpo.mail.desgloseclitit.valoresVacios}")
  private String bodyMailValoresVacios;

  @Value("${sibbac.accounting.destinatario.mail.desgloseclitit}")
  private String toMail;

  @Value("${sibbac.accounting.cuerpo.mail.erroresFicheeros}")
  private String bodyMailErroresFicheros;

  @Value("${sibbac.accounting.destinatario.mail.plantillas}")
  private String toMailPlantillas;

  @Value("${sibbac.accounting.subject.mail.plantillas}")
  private String subjectMailPlantillas;
  
  @Value("${sibbac.accounting.subject.mail.plantillas.error}")
  private String subjectMailPlantillasConError;

  /* Lista para guardar todas las plantillas que no tengan un balance de 0 */
  private List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

  @Autowired
  private Tmct0movimientoeccBo movimientoeccBo;

  @Autowired
  private Tmct0ContaApuntesBo contaApuntesBo;

  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  private Tmct0desgloseclititBo desgloseBo;

  @Autowired
  private EjecucionDevengoAnulacion ejecucionDevengo;

  @Autowired
  private EjecucionEmisionFactura ejecucionEmisionFactura;

  @Autowired
  private EjecucionDotacionDesdotacion ejecucionDotacionDesdotacion;

  @Autowired
  private EjecucionPerdidasGanancias ejecucionPerdidasGanancias;

  @Autowired
  private EjecucionDevengoFallidos ejecucionDevengoFallidos;

  @Autowired
  private EjecucionCobrosRetrocesiones ejecucionCobrosRetrocesiones;

  @Autowired
  EjecucionLiquidacionesTotales ejecucionLiquidacionesTotales;
  
  @Autowired
  EjecucionLiquidacionesParciales ejecucionLiquidacionesParciales;

  @Autowired
  private EjecucionCobrosBarridos ejecucionCobrosBarridos;

  @Autowired
  private EjecucionApuntesManuales ejecucionApuntesManuales;
  
  @Autowired
  private EjecucionFacturasManuales ejecucionfacturasManuales;

  @Autowired
  private Tmct0ContaPlantillasDao contaPlantillasDao;

  @Autowired
  private Tmct0ContaPlantillasDaoImp contaPlantillasDaoImp;

  @Autowired
  private SendMail sendMail;
  
  @Autowired
  private ValidationAccounting validationAccounting;

  @Value("${accounting.contabilidad.folder.out}")
  private String folderOut;

  @Value("${sibbac.accounting.filler}")
  private String filler;
  
  @Autowired
  private FtpSibbacContabilidad ftpSibbacContabilidad;
  
  @Value("${ftp.folder.accounting}")
  private String folderFtpAccounting;
  
  @Value("${accounting.contabilidad.folder.out.tratados}")
  private String folderTratadoAccounting;

  private static final String TIPO_TRANSACCION_CABECERA = "1";
  private static final String TIPO_TRANSACCION_DETALLE = "2";
  private static final String CODIGO_COMP = "31";
  private static final String SIGLO = "1";

  // Lista para almacenar los nombres de los ficheros erroneos.
  private List<String> listaFicherosErrores = new ArrayList<String>();

  @Override
  public void execute() {
    LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Inicio ");
    LOG.trace("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":" + Task.GROUP_ACCOUNTING.JOB_DESGLOSE
              + "] Init");

    Tmct0men tmct0men = null;
    TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
    TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
    TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
    NumberFormat nF1 = new DecimalFormat("000");

    try {

      tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_ACTUALIZACION_DESGLOSE, estadoIni, estadoExe);

      // Actualizacion de todos los registros de DesglosesClitit en funcion de MovimientosECC
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Inicio actualizacionDesglosesClitit ");
      Tmct0men tmct0menDesgloses = tmct0menBo.findByCdmensa(nF1.format(TIPO_APUNTE.TASK_ACTUALIZACION_DATOS_COMPENSACION.getTipo()));
      String cdestado = tmct0menDesgloses.getTmct0sta().getId().getCdestado();
      if(estadoIni.getCdmensa().equals(cdestado)){
    	  this.actualizacionDesglosesClitit();
      }	
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Fin actualizacionDesglosesClitit ");

      // Se ejecuta la generación de apuntes para los distintos tipos de plantillas.
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Inicio ejecucion de plantillas ");
      ejecucionPlantillas();
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Fin ejecucion de plantillas ");

      // Generacion de ficheros de texto planto de toda la contabilidad
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Inicio Generacion de ficheros ");
      this.generacionFicheroTextoPlano();
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Fin Generacion de ficheros ");

      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Inicio Ejecucion querys finales ");
      this.ejecucionQueriesFinales();
      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Fin Ejecucion querys finales");

      tmct0menBo.putEstadoMEN(tmct0men, estadoIni);
      LOG.trace("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":" + Task.GROUP_ACCOUNTING.JOB_DESGLOSE
                + "] Fin");

      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Fin ");

    } catch (EjecucionPlantillaException | SendMailException e) {
      tmct0menBo.putEstadoMEN(tmct0men, estadoError);
      LOG.error("Error Task actualizacion desglose ", e);
    } catch (SIBBACBusinessException e) {
      LOG.warn("[TaskActualizacionDesglose :: execute] Error intentando bloquear el semaforo ... " + e.getMessage());
      tmct0menBo.putEstadoMEN(tmct0men, estadoError);
    }
  }

  private void actualizacionDesglosesClitit() throws SendMailException {

    List<Object[]> listMovimientoecc = movimientoeccBo.findDistinctIdDesglose();

    desgloseBo.updateDatosCompensacionDesgloseClitit(listMovimientoecc);

    List<Object[]> listaErroneos = movimientoeccBo.findMismoIdDesgloseDistintosValoresError();
    List<String> listaErroneosVacios = desgloseBo.getListaErroneos();

    if (listaErroneos.size() > 0 || listaErroneosVacios.size() > 0) {

      sendMail(toMail, "Errores desglose clitit",
               bodyMail + bodyMailIdRepetidos + StringHelper.getListaStringIdDesgloseCamara(listaErroneos)
                   + bodyMailValoresVacios + listaErroneosVacios.toString());
    }

  }
  
	/**
	 * Agrupa los apuntes por fichero y periodo
	 * 
	 * @param apuntes
	 * @return
	 */
	private Map<String, List<Tmct0ContaApuntes>> agruparPorFicheroPeriodo(List<Tmct0ContaApuntes> apuntes) {
		Map<String, List<Tmct0ContaApuntes>> agrupados = new HashMap<String, List<Tmct0ContaApuntes>>();
		if (apuntes != null && !apuntes.isEmpty()) {
			for (Tmct0ContaApuntes apunte : apuntes) {
				String key = apunte.getFichero() + "##" + apunte.getPeriodo_comprobante() + "##" + apunte.getFecha_comprobante();
				if (!agrupados.containsKey(key)) {
					agrupados.put(key, new ArrayList<Tmct0ContaApuntes>());
				}
				agrupados.get(key).add(apunte);
			}
		}
		return agrupados;
	}

	/**
	 * Genera fichero en texto plano.
	 *
	 * @throws SendMailException
	 *             SendMailException
	 * @throws EjecucionPlantillaException
	 *             EjecucionPlantillaException
	 * @throws IOException 
	 */
  private void generacionFicheroTextoPlano() throws SendMailException, EjecucionPlantillaException {
    String[] keys;
    String fichero;
    String periodo;
    Date fechaComprobante;
    Integer numComprobante;
    String fechaComprobanteSt;
    Integer ficheroGeneradoCount;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    List<Tmct0ContaApuntes> listContaApuntes = contaApuntesBo.getTmct0ContaApuntesGeneradoFichero();
    Map<String, List<Tmct0ContaApuntes>> agrupados = agruparPorFicheroPeriodo(listContaApuntes);
    
    List<File> files = new ArrayList<File>();
    for (String key : agrupados.keySet()) {

      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Generacion fichero - periodo: " + key + ". Inicio");

      keys = key.split("##");
      fichero = keys[0];
      periodo = keys[1];
      fechaComprobanteSt = keys[2];

      try {
        fechaComprobante = sdf.parse(fechaComprobanteSt);
      }
      catch (ParseException e) {
        throw new EjecucionPlantillaException(
            String.format("No se ha podido recuperar la fecha comprobante de generacion del fichero %s, msg:%s", key,
                e.getMessage()), e);
      }

      numComprobante = contaPlantillasDao.maxNumComprobanteByFicheroAndActivo(fichero);
      if (numComprobante == null) {
        numComprobante = 0;
      }
      numComprobante++;
      LOG.info("Generando Num.Comprobante: {} para fichero: {} key: {}", numComprobante, fichero, key);
      // Crear fichero y escribir en el.
      this.createFichero(numComprobante, agrupados.get(key), files);

      ficheroGeneradoCount = this.contaApuntesBo.updateDatosFicheroGeneradoNumComprobante(fichero, periodo,
          fechaComprobante, numComprobante);
      if (ficheroGeneradoCount != agrupados.get(key).size()) {
        LOG.error(
            "El numero de apuntes marcados como enviados {} no coincide con los generados en el fichero: {}, key: {} Num.Comprobante: {}",
            ficheroGeneradoCount, agrupados.get(key).size(), key, numComprobante);
      }
      else {
        LOG.info("Se ha generado el fichero fichero: {} key: {} Num.Comprobante: {} con {} registros", fichero, key,
            numComprobante, ficheroGeneradoCount);
      }

      LOG.debug(Loggers.getDatosComunesLogsInicial(null) + "Generacion fichero - periodo: " + key + ". Fin");

    }

    // Se envían los ficheros por FTP
    try {
      if (!ftpSibbacContabilidad.connect(folderFtpAccounting)) {
        LOG.debug("No se ha podido conectar con la maquina destino ...");
      }
      else {
        if (!files.isEmpty()) {
          Object[] objects;
          List<Object[]> listSend = new ArrayList<Object[]>();
          for (File file1 : files) {
            listSend.add(objects = new Object[3]);
            objects[0] = file1;
            objects[1] = objects[2] = file1.getName();
          }

          LOG.debug("[Accounting] Enviando y moviendo a tratados ...");
          LOG.debug("[Accounting] Resultado del envio: {}",
              ftpSibbacContabilidad.send(listSend, folderTratadoAccounting));
          LOG.debug("[Accounting] Enviado ...");
        }
      }
    }
    catch (IOException e) {
      LOG.debug("[Accounting] Error al intentar realizar la conexión FTP");
    }
  }

  /**
   * Creacion de fichero.
   *
   * @param listaAInsertar
   * @throws EjecucionPlantillaException
   */
  public boolean createFichero(Integer numComprobante, List<Tmct0ContaApuntes> listaAInsertar, List<File> files) throws EjecucionPlantillaException {

    SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyy");
    SimpleDateFormat formatHour = new SimpleDateFormat("HHmmss");
    Tmct0ContaApuntes itemParaConstruir = listaAInsertar.get(0);

    String fechaComprobante = formatDate.format(itemParaConstruir.getFecha_comprobante());
    String horaActual = formatHour.format(new Date());

    String titulo = itemParaConstruir.getFichero() + "_" + itemParaConstruir.getPeriodo_comprobante() + "_"
                    + fechaComprobante + "_" + horaActual + ".txt";

    LOG.debug("Generando fichero " + titulo);

    try {

      PrintWriter writer = new PrintWriter(folderOut + titulo, "UTF-8");
      writer.println(obtenerCabecera(numComprobante, itemParaConstruir));

      for (Tmct0ContaApuntes contaApunte : listaAInsertar) {
        writer.println(this.obtenerDetalleContaApunte(numComprobante, contaApunte));
      }

      writer.close();
      
      //Se añaden el fichero a la lista que luego será enviada por FTP
      files.add(new File(folderOut, titulo));

    } catch (FileNotFoundException e) {
      LOG.error("Ruta o fichero no encontrado ", e.getCause());
      listaFicherosErrores.add(itemParaConstruir.getFichero());
      // Se relanza en caso de problemas con el fichero.
      throw new EjecucionPlantillaException();
    } catch (UnsupportedEncodingException e) {
      LOG.error("Error en el codificado ", e.getCause());
      listaFicherosErrores.add(itemParaConstruir.getFichero());
      throw new EjecucionPlantillaException();
    } catch (Exception e) {
      LOG.error("Error a la hora de crear el fichero ", e.getCause());
      listaFicherosErrores.add(itemParaConstruir.getFichero());
      throw new EjecucionPlantillaException();
    }

    return true;
  }

  private String obtenerDetalleContaApunte(Integer numComprobante, Tmct0ContaApuntes contaApunte) {
    SimpleDateFormat formatAnioComprobante = new SimpleDateFormat("yy");

    BigDecimal importe = contaApunte.getImporte();

    importe = importe.setScale(2, BigDecimal.ROUND_DOWN);
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
    DecimalFormat df = (DecimalFormat) nf;
    df.setMaximumFractionDigits(2);
    df.setMinimumFractionDigits(2);
    df.setGroupingUsed(false);
    df.setMaximumIntegerDigits(13);

    if (importe.signum() == -1) {

      df.setMinimumIntegerDigits(12);
    } else {

      df.setMinimumIntegerDigits(13);
    }
    String cuentaContableSinComillas = contaApunte.getCuenta_contable().replaceAll("\"", "").trim();

    String importeString = df.format(importe);
    String cuentaContable = String.format("%-15s", cuentaContableSinComillas);
    String numeroComprobante = String.format("%05d", numComprobante);

    String detalle = TIPO_TRANSACCION_DETALLE
                     + CODIGO_COMP
                     + formatAnioComprobante.format(contaApunte.getFecha_comprobante())
                     + StringUtils.leftPad(StringHelper.safeSubString(contaApunte.getPeriodo_comprobante().trim(), 0, 2),
                                           2, '0')
                     + StringHelper.safeSubString(contaApunte.getTipo_comprobante().trim(), 0, 2) + numeroComprobante
                     + cuentaContable + String.format("%-8s", contaApunte.getAux_contable().trim())
                     + String.format("%-36s", contaApunte.getConcepto().trim()) + importeString
                     + contaApunte.getTipo_apunte() + filler;
    return detalle;
  }

  private String obtenerCabecera(Integer numComprobante, Tmct0ContaApuntes contaApunte) {
    SimpleDateFormat formatAnioComprobante = new SimpleDateFormat("yy");
    SimpleDateFormat formatDate = new SimpleDateFormat("yyMMdd");
    BigDecimal importe = contaApunte.getImporte();

    importe = importe.setScale(2, BigDecimal.ROUND_DOWN);
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
    DecimalFormat df = (DecimalFormat) nf;
    df.setMaximumFractionDigits(2);
    df.setMinimumFractionDigits(2);
    df.setMaximumIntegerDigits(13);
    df.setMinimumIntegerDigits(13);
    df.setGroupingUsed(false);

    String numeroComprobante = String.format("%05d", numComprobante);
    String fechaComprobante = formatDate.format(contaApunte.getFecha_comprobante());

    String detalle = TIPO_TRANSACCION_CABECERA
                     + CODIGO_COMP
                     + formatAnioComprobante.format(contaApunte.getFecha_comprobante())
                     + StringUtils.leftPad(StringHelper.safeSubString(contaApunte.getPeriodo_comprobante().trim(), 0, 2),
                                           2, '0')
                     + StringHelper.safeSubString(contaApunte.getTipo_comprobante().trim(), 0, 2) + numeroComprobante
                     + SIGLO + fechaComprobante;

    return String.format("%-177s", detalle);
  }

  private boolean isTipoSubtipoEjecutado(List<String> tipoSubtipoEjecutado, String tipo, String subtipo) {
    boolean banEjecutado = false;
    for (String tipoSubtipo : tipoSubtipoEjecutado) {
      if (tipoSubtipo.equals(tipo + "_" + subtipo)) {
        banEjecutado = true;
      }
    }
    return banEjecutado;
  }

  /* Ejecuta los distintos tipos de plantillas */
  private void ejecucionPlantillas() throws EjecucionPlantillaException, SendMailException {

    try {
      // Se ejecutan las plantillas para los distintos tipos y se guardan las que no hayan dado balance de 0.
      List<Tmct0ContaPlantillas> plantillasAll = contaPlantillasDao.getAll();
      List<Tmct0ContaPlantillas> plantillas = new ArrayList<Tmct0ContaPlantillas>();
      List<String> tipoSubtipoEjecutado = new ArrayList<>();

      for (Tmct0ContaPlantillas tmct0ContaPlantillas : plantillasAll) {

        if (!isTipoSubtipoEjecutado(tipoSubtipoEjecutado, tmct0ContaPlantillas.getTipo(),
                                    tmct0ContaPlantillas.getSubtipo())) {

          tipoSubtipoEjecutado.add(tmct0ContaPlantillas.getTipo() + "_" + tmct0ContaPlantillas.getSubtipo());

          plantillas = contaPlantillasDao.getPlantillasPorTipoSubtipo(tmct0ContaPlantillas.getTipo(),
                                                                      tmct0ContaPlantillas.getSubtipo());

          
          switch (tmct0ContaPlantillas.getTipo()) {

          case Constantes.DEVENGO:
        	  plantillasSinBalance.addAll(ejecucionDevengo.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.ANULACION:
        	  plantillasSinBalance.addAll(ejecucionDevengo.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.EMISION_FACTURA:
        	  plantillasSinBalance.addAll(ejecucionEmisionFactura.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.DEVENGO_FACTURAS:
        	  plantillasSinBalance.addAll(ejecucionfacturasManuales.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.DESDOTACION:
        	  plantillasSinBalance.addAll(ejecucionDotacionDesdotacion.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.DOTACION:
        	  plantillasSinBalance.addAll(ejecucionDotacionDesdotacion.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.COBRO:
        	  plantillasSinBalance.addAll(ejecucionCobrosRetrocesiones.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.RETROCESION:
        	  plantillasSinBalance.addAll(ejecucionCobrosRetrocesiones.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.LIQUIDACION_TOTAL:
            plantillasSinBalance.addAll(ejecucionLiquidacionesTotales.ejecutarPlantillasTipoSubtipo(plantillas));
            break;
          case Constantes.LIQUIDACION_PARCIAL:
        	  plantillasSinBalance.addAll(ejecucionLiquidacionesParciales.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.PERDIDAS_GANANCIAS:
        	  plantillasSinBalance.addAll(ejecucionPerdidasGanancias.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.DEVENGO_FALLIDOS:
        	  plantillasSinBalance.addAll(ejecucionDevengoFallidos.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.COBRO_BARRIDOS:
        	  plantillasSinBalance.addAll(ejecucionCobrosBarridos.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          case Constantes.APUNTE_MANUAL:
        	  plantillasSinBalance.addAll(ejecucionApuntesManuales.ejecutarPlantillasTipoSubtipo(plantillas));
        	  break;
          default:
        	  Loggers.logErrorAccounting(tmct0ContaPlantillas.getTipo(), ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + " : NO ES UNO DE LOS TIPOS DEFINIDOS EN EL PROCESO.  " );
        	  break;
          }
        }
      }
      
      // Se envía mail con información de las plantillas que no dieron balance de 0.
      this.sendMailPlantillas();
      
    }  catch (Exception e) {
      Loggers.logErrorAccounting(null, ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + " : " + e.getMessage());
  	  this.sendMailPlantillasError();
      throw new EjecucionPlantillaException();
    }
  }
  
	/** Envía el mail con el detalle de las plantillas que dieron error */
	private void sendMailPlantillasError() throws SendMailException {

		List<Tmct0ContaPlantillas> plantillasConError = contaPlantillasDao
				.findByActivo(ActivacionContaPlantillas.ERROR.getActivado());
		if (!CollectionUtils.isEmpty(plantillasConError)) {

			StringBuilder body = new StringBuilder();

			for (Tmct0ContaPlantillas plantilla : plantillasConError) {

				body.append("Error ejecutando la plantilla " + plantilla.getCodigo() + " - " + plantilla.getTipo()
						+ " - " + plantilla.getSubtipo() + " / " + plantilla.getProceso());
				body.append("\n");

			}

			sendMail(toMailPlantillas, subjectMailPlantillasConError, body.toString());

		}
		
	}

	/**
	 * Envía el mail con el detalle de las plantillas que no dieron balance de 0
	 */
	private void sendMailPlantillas() throws SendMailException {

		StringBuilder body = new StringBuilder();
		if (!CollectionUtils.isEmpty(plantillasSinBalance)) {

			for (List<Tmct0ContaPlantillasDTO> listApuntes : plantillasSinBalance) {
				if (listApuntes.get(0) != null && !listApuntes.get(0).getCodigo().equals("")
						&& listApuntes.get(0).isSinBalance() && !listApuntes.get(0).isErroresFichero()) {

					body.append("Asiento contable descuadrado (" + listApuntes.get(0).getCodigo() + "-"
							+ listApuntes.get(0).getTipo() + "/");
					body.append(listApuntes.get(0).getSubtipo() + "/" + listApuntes.get(0).getProceso() + ")");

					for (Tmct0ContaPlantillasDTO apunte : listApuntes) {
						if (apunte.getTipo().equals('D')) {
							body.append(", importe al DEBE: " + apunte.getImporteDebe());
						} else if (apunte.getTipo().equals('H')) {
							body.append(", importe al HABER: " + apunte.getImporteHaber());
						}
					}
					body.append("\n");
				}
			}
		}

		List<Tmct0ContaPlantillas> plantillasConError = contaPlantillasDao
				.findByActivo(ActivacionContaPlantillas.ERROR.getActivado());
		if (!CollectionUtils.isEmpty(plantillasConError)) {

			for (Tmct0ContaPlantillas plantilla : plantillasConError) {

				body.append("Error ejecutando la plantilla " + plantilla.getCodigo() + " - " + plantilla.getTipo()
						+ " - " + plantilla.getSubtipo() + " / " + plantilla.getProceso());
				body.append("\n");

			}

		}

		LOG.debug(body.toString());
		if (StringUtils.isNotEmpty(body.toString())) sendMail(toMailPlantillas, subjectMailPlantillas, body.toString());

	}

  private void sendMail(String toMail, String subjectMail, String bodyMail) throws SendMailException {

    try {
      sendMail.sendMail(toMail, subjectMail, bodyMail);
    } catch (IllegalArgumentException | MessagingException | IOException e) {
      LOG.error("Error al enviar mail", e);
      throw new SendMailException("Error al enviar mail", e);
    }
  }

  private void ejecucionQueriesFinales() throws EjecucionPlantillaException {
    contaPlantillasDaoImp.executeUpdateFinales();
  }

}
