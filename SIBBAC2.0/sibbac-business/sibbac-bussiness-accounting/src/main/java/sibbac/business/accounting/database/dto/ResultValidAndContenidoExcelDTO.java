package sibbac.business.accounting.database.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 	Data Transfer Object que contiene informacion de la validacion y
 * 	volcado de contenido del fichero Excel.
 */
public class ResultValidAndContenidoExcelDTO {
	
	private List<String> listaErrores;
	private List<ApuntesManualesExcelDTO> listaApuntesManualesExcelDTO;
	
	/**
	 *	Constructor por defecto. 
	 */
	public ResultValidAndContenidoExcelDTO() {
		this.listaErrores = new ArrayList<String>();
		this.listaApuntesManualesExcelDTO = new ArrayList<ApuntesManualesExcelDTO>();
	}

	public List<String> getListaErrores() {
		return this.listaErrores;
	}
	
	public void setListaErrores(List<String> listaErrores) {
		this.listaErrores = listaErrores;
	}
	
	public List<ApuntesManualesExcelDTO> getListaApuntesManualesExcelDTO() {
		return this.listaApuntesManualesExcelDTO;
	}
	
	public void setListaApuntesManualesExcelDTO(
			List<ApuntesManualesExcelDTO> listaApuntesManualesExcelDTO) {
		this.listaApuntesManualesExcelDTO = listaApuntesManualesExcelDTO;
	}
	
	public void addListaApuntesManualesExcelDTO(
		List<ApuntesManualesExcelDTO> listaApuntManualesExcelDTO) {
		if (this.listaApuntesManualesExcelDTO == null) {
			this.listaApuntesManualesExcelDTO = new ArrayList<ApuntesManualesExcelDTO>();
		}
		this.listaApuntesManualesExcelDTO.addAll(listaApuntManualesExcelDTO);
	}
	
	public void addListaErrores(List<String> errores) {
		if (this.listaErrores == null) {
			this.listaErrores = new ArrayList<String>();
		}
		this.listaErrores.addAll(errores);
	}

}