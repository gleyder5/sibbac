package sibbac.business.accounting.conciliacion.database.dto;

import java.util.List;

/**
 * @author lucio.vilar
 *
 */
public class DatosApuntesExcelDTO {
	
	private String auxiliar;
	
	private String plantilla;
	
	private String concepto;
	
	private List<String> referencias;
	
	private List<Tmct0ContaOurApuntesDTO> camposClave;

	/**
	 * @return the auxiliar
	 */
	public String getAuxiliar() {
		return auxiliar;
	}

	/**
	 * @param auxiliar the auxiliar to set
	 */
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}

	/**
	 * @return the plantilla
	 */
	public String getPlantilla() {
		return plantilla;
	}

	/**
	 * @param plantilla the plantilla to set
	 */
	public void setPlantilla(String plantilla) {
		this.plantilla = plantilla;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the referencias
	 */
	public List<String> getReferencias() {
		return referencias;
	}

	/**
	 * @param referencias the referencias to set
	 */
	public void setReferencias(List<String> referencias) {
		this.referencias = referencias;
	}

	/**
	 * @return the camposClave
	 */
	public List<Tmct0ContaOurApuntesDTO> getCamposClave() {
		return camposClave;
	}

	/**
	 * @param camposClave the camposClave to set
	 */
	public void setCamposClave(List<Tmct0ContaOurApuntesDTO> camposClave) {
		this.camposClave = camposClave;
	}

}
