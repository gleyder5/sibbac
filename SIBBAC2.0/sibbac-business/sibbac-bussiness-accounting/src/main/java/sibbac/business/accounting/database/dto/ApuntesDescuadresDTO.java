package sibbac.business.accounting.database.dto;

import java.util.ArrayList;
import java.util.List;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;

/**
 * Data Transfer Object para la gestión de Apuntes Manuales del Excel 
 * - apuntes y descuadres.
 */
public class ApuntesDescuadresDTO {
	
	ApuntesDiferenciaDTO apuntesDiferenciaDTO;
	List<Norma43Descuadres> listaNorma43Descuadres;

	/**
	 *	Constructor sin argumentos. 
	 */
	public ApuntesDescuadresDTO() {
		this.listaNorma43Descuadres = new ArrayList<Norma43Descuadres>();
	}
	
	/**
	 *	Constructor con argumentos. 
	 */
	public ApuntesDescuadresDTO(ApuntesDiferenciaDTO apuntesDiferenciaDTO,
			List<Norma43Descuadres> listaNorma43Descuadres) {
		super();
		this.apuntesDiferenciaDTO = apuntesDiferenciaDTO;
		this.listaNorma43Descuadres = listaNorma43Descuadres;
	}

	/**
	 *	@return apuntesDiferenciaDTO 
	 */
	public ApuntesDiferenciaDTO getApuntesDiferenciaDTO() {
		return this.apuntesDiferenciaDTO;
	}

	/**
	 *	@param apuntesDiferenciaDTO 
	 */
	public void setApuntesDiferenciaDTO(ApuntesDiferenciaDTO apuntesDiferenciaDTO) {
		this.apuntesDiferenciaDTO = apuntesDiferenciaDTO;
	}

	/**
	 *	@return listaNorma43Descuadres 
	 */
	public List<Norma43Descuadres> getListaNorma43Descuadres() {
		return this.listaNorma43Descuadres;
	}

	/**
	 *	@param listaNorma43Descuadres 
	 */
	public void setListaNorma43Descuadres(
			List<Norma43Descuadres> listaNorma43Descuadres) {
		this.listaNorma43Descuadres = listaNorma43Descuadres;
	}
	
}