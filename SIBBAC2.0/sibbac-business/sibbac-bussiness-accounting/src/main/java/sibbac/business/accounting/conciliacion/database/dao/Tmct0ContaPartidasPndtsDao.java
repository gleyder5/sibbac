package sibbac.business.accounting.conciliacion.database.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;

/**
 * Data access object para Conta Partidas pendientes.
 */
@Repository
public interface Tmct0ContaPartidasPndtsDao extends JpaRepository<Tmct0ContaPartidasPndts, Integer> {
	
	@Query("SELECT MAX(pdte.auditDate) FROM Tmct0ContaPartidasPndts pdte")
	public Date getMaxAuditDate();
	
	@Query("SELECT MAX(pdte.bookingDate) FROM Tmct0ContaPartidasPndts pdte")
	public Date getMaxBookingDate();

	@Query(value="SELECT VARCHAR_FORMAT(PNDTS.BOOKING_DATE, 'DD/MM/YYYY HH24:MI:SS') FROM TMCT0_CONTA_PARTIDAS_PNDTS PNDTS ORDER BY PNDTS.BOOKING_DATE DESC FETCH FIRST 1 ROW ONLY", nativeQuery = true)
	public String getMaxBookingDateFormatted();
	
	@Query(value="SELECT pdte.gin FROM Tmct0ContaPartidasPndts pdte WHERE pdte.estado = :idEstado")
	public List<String> findGINsByEstado(@Param( "idEstado" ) Integer idEstado);
	
	@Modifying
	@Query(value="DELETE FROM Tmct0ContaPartidasPndts pdte WHERE pdte.estado = :idEstado")
	public void deleteByEstado(@Param( "idEstado" ) Integer idEstado);
	
	@Query( "SELECT partidasPndts FROM Tmct0ContaPartidasPndts partidasPndts WHERE partidasPndts.gin = :gin AND partidasPndts.importe = :importe" )
	public List<Tmct0ContaPartidasPndts> findByGinAndImporte(@Param("gin") BigInteger gin,@Param("importe") BigDecimal importe);

	@Query(value="SELECT VARCHAR_FORMAT(PNDTS.AUDIT_DATE, 'DD/MM/YYYY HH24:MI:SS') CONCAT '||' CONCAT PNDTS.AUDIT_USER FROM TMCT0_CONTA_PARTIDAS_PNDTS PNDTS ORDER BY PNDTS.AUDIT_DATE DESC", nativeQuery = true)
	public List<String> findAuditDateAuditUser();
	
	@Query(value="SELECT partidasPndts FROM Tmct0ContaPartidasPndts partidasPndts WHERE partidasPndts.id = :id")
	public Tmct0ContaPartidasPndts findById(@Param("id")BigInteger id);
	
	//FIXME: sustituir por bucleFor 
	@Modifying @Query(value="UPDATE TMCT0_CONTA_PARTIDAS_PNDTS SET ESTADO = 1 WHERE ID IN :listId",nativeQuery=true)
	public void updateEstadoUnoFromId(@Param( "listId" ) List<BigInteger> listId);
}
