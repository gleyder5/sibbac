package sibbac.business.accounting.conciliacion.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesAudit;

/**
 * Data access object para Conta Partidas pendientes.
 */
@Repository
public interface Tmct0ContaApuntesAuditDao extends JpaRepository<Tmct0ContaApuntesAudit, Integer> {
	
}
