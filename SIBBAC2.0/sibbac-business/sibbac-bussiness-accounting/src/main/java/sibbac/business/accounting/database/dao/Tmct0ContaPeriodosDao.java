package sibbac.business.accounting.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.database.model.Tmct0ContaPeriodos;

@Repository
public interface Tmct0ContaPeriodosDao extends JpaRepository<Tmct0ContaPeriodos, Integer>  {
	
	 @Query( "SELECT m FROM Tmct0ContaPeriodos m order by m.fhFinPeriodo desc" )
	 public List<Tmct0ContaPeriodos> findContaPeriodosOrderByFhFinPeriodo();

}
