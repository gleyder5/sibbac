package sibbac.business.accounting.tasks;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.accounting.exception.SendMailException;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ACCOUNTING.NAME,
           name = Task.GROUP_ACCOUNTING.JOB_UPDATE_DATOS_COMPENSACION,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "00:00:00")
public class TaskUpdateDatosCompensacionClitit extends SIBBACTask {

  @Autowired
  Tmct0movimientoeccBo movimientoeccBo;

  @Autowired
  Tmct0menBo tmct0menBo;

  @Autowired
  Tmct0desgloseclititBo desgloseBo;

  @Autowired
  private SendMail sendMail;
  
  @Value("${sibbac.accounting.cuerpo.mail.desgloseclitit}")
  private String bodyMail;

  @Value("${sibbac.accounting.cuerpo.mail.desgloseclitit.diferentesValores}")
  private String bodyMailIdRepetidos;

  @Value("${sibbac.accounting.cuerpo.mail.desgloseclitit.valoresVacios}")
  private String bodyMailValoresVacios;

  @Value("${sibbac.accounting.destinatario.mail.desgloseclitit}")
  private String toMail;

  @Value("${sibbac.accounting.cuerpo.mail.erroresFicheeros}")
  private String bodyMailErroresFicheros;

  @Override
  public void execute() {
    LOG.debug("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":"
              + Task.GROUP_ACCOUNTING.JOB_UPDATE_DATOS_COMPENSACION + "] Init");

    Tmct0men tmct0men = null;
    TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
    TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
    TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;

    try {

      tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_ACTUALIZACION_DATOS_COMPENSACION, estadoIni, estadoExe);

      List<Object[]> listMovimientoecc = movimientoeccBo.findDistinctIdDesglose();

      desgloseBo.updateDatosCompensacionDesgloseClitit(listMovimientoecc);

      List<Object[]> listaErroneos = movimientoeccBo.findMismoIdDesgloseDistintosValoresError();
      List<String> listaErroneosVacios = desgloseBo.getListaErroneos();

      if (listaErroneos.size() > 0 || listaErroneosVacios.size() > 0) {

        sendMail(toMail, "Errores desglose clitit",
                 bodyMail + bodyMailIdRepetidos + StringHelper.getListaStringIdDesgloseCamara(listaErroneos)
                     + bodyMailValoresVacios + listaErroneosVacios.toString());
      }

      tmct0menBo.putEstadoMEN(tmct0men, estadoIni);
      LOG.debug("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":"
                + Task.GROUP_ACCOUNTING.JOB_UPDATE_DATOS_COMPENSACION + "] Fin");

  	} catch (SendMailException e) {
      tmct0menBo.putEstadoMEN(tmct0men, estadoError);
      LOG.error("Error Task actualizacion desglose ", e);
    } catch (SIBBACBusinessException e) {
      LOG.warn("[TaskUpdateDatosCompensacionClitit :: execute] Error intentando bloquear el semaforo ... " + e.getMessage());
      tmct0menBo.putEstadoMEN(tmct0men, estadoError);
    }

  }
  
  
  private void sendMail(String toMail, String subjectMail, String bodyMail) throws SendMailException {

	    try {
	      sendMail.sendMail(toMail, subjectMail, bodyMail);
	    } catch (IllegalArgumentException | MessagingException | IOException e) {
	      LOG.error("Error al enviar mail", e);
	      throw new SendMailException("Error al enviar mail", e);
	    }
  }

}
