package sibbac.business.accounting.database.model;

import java.math.BigDecimal;

public class Tmct0ArregloObject implements Comparable<Tmct0ArregloObject> {

  private String nuorden = "";
  private String nbooking = "";
  private String nucnfclt = "";
  private BigDecimal nucnfliq = null;

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public BigDecimal getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(BigDecimal nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nucnfclt == null) ? 0 : nucnfclt.hashCode());
    result = prime * result + ((nucnfliq == null) ? 0 : nucnfliq.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0ArregloObject other = (Tmct0ArregloObject) obj;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    }
    else if (!nbooking.equals(other.nbooking))
      return false;
    if (nucnfclt == null) {
      if (other.nucnfclt != null)
        return false;
    }
    else if (!nucnfclt.equals(other.nucnfclt))
      return false;
    if (nucnfliq == null) {
      if (other.nucnfliq != null)
        return false;
    }
    else if (!nucnfliq.equals(other.nucnfliq))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    }
    else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public int compareTo(Tmct0ArregloObject obj) {
    if (this == obj)
      return 0;
    if (obj == null)
      return 1;
    if (getClass() != obj.getClass())
      return 1;
    Tmct0ArregloObject other = (Tmct0ArregloObject) obj;
    if (nbooking == null) {
      if (other.nbooking != null)
        return 1;
    }
    else if (!nbooking.equals(other.nbooking))
      return 1;
    if (nucnfclt == null) {
      if (other.nucnfclt != null)
        return 1;
    }
    else if (!nucnfclt.equals(other.nucnfclt))
      return 1;
    if (nucnfliq == null) {
      if (other.nucnfliq != null)
        return 1;
    }
    else if (!nucnfliq.equals(other.nucnfliq))
      return 1;
    if (nuorden == null) {
      if (other.nuorden != null)
        return 1;
    }
    else if (!nuorden.equals(other.nuorden))
      return 1;
    return 0;
  }

}
