package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.Tmct0ApuntesAuditoriaDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ApuntesPndtsDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ToleranceAuditoriaDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaToleranceAudit;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_CONTA_APUNTES_PNDTS
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0ControlContabilidadAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ControlContabilidadAssembler.class);

	@Autowired
	private AliasDao aliasDao;
	
	@Autowired
	private Tmct0UsersDao tmct0UsersDao;

	@Autowired
	Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

	public Tmct0ControlContabilidadAssembler() {
		super();
	}

	/**
	 * Realiza la conversion entre la entidad y el dto Tmct0ApuntesPndtsTablaDTO
	 * 
	 * @param datosApuntesPndts -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0ApuntesPndtsDTO getTmct0ApuntesPndtsTablaDTO(Tmct0ContaApuntesPndts datosApuntesPndts) {

		LOG.info("INICIO - ASSEMBLER - getTmct0ApuntesPndtsTablaDTO");

		Tmct0ApuntesPndtsDTO apuntesPndtsDTO = new Tmct0ApuntesPndtsDTO();

		try {
			apuntesPndtsDTO.setId(datosApuntesPndts.getId());
			apuntesPndtsDTO.setFeApunte(datosApuntesPndts.getFeApunte());
			// Se muestra nombre y apellido en la grilla
			if (StringUtils.isNotBlank(datosApuntesPndts.getUsuApunte())) {
				apuntesPndtsDTO.setUsuApunte(
						this.tmct0UsersDao.findUserNameLastNameConcatByUserName(datosApuntesPndts.getUsuApunte()));
			}
			apuntesPndtsDTO.setConcepto(datosApuntesPndts.getConcepto().getConcepto());
			apuntesPndtsDTO.setIdConcepto(datosApuntesPndts.getConcepto().getId());
			apuntesPndtsDTO.setImporteDebe(datosApuntesPndts.getImporteDebe());
			apuntesPndtsDTO.setImporteHaber(datosApuntesPndts.getImporteHaber());
			apuntesPndtsDTO.setImportePyG(datosApuntesPndts.getImportePyG());
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ApuntesPndtsTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ApuntesPndtsTablaDTO");

		return apuntesPndtsDTO;

	}

	/**
	 * Realiza la conversion de tipo Tmct0ContaApuntesPndts a Tmct0ContaApuntesAudit
	 * 
	 * @param map -> Tmct0ContaApuntesPndts
	 * @return Tmct0ContaApuntesAudit
	 */
	public Tmct0ContaApuntesAudit getTmct0ContaApuntesAudit(LinkedHashMap map, boolean isAprobar) {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaApuntesAudit");

		Tmct0ContaApuntesAudit contaApunteAudit = new Tmct0ContaApuntesAudit();

		try {
			contaApunteAudit.setFeApunte(new Timestamp((Long)map.get("feApunte")));
			contaApunteAudit.setUsuApunte((String) map.get("auditUser"));
			contaApunteAudit.setConcepto(new Tmct0ContaConcepto());
			contaApunteAudit.getConcepto().setId(BigInteger.valueOf((Integer) map.get("idConcepto")));
			if (null != map.get("importeDebe")) {
				if (map.get("importeDebe") instanceof Double) {
					contaApunteAudit.setImporteDebe(BigDecimal.valueOf((Double) map.get("importeDebe")));
				} else if (map.get("importeDebe") instanceof Integer){
					contaApunteAudit.setImporteDebe(BigDecimal.valueOf((Integer) map.get("importeDebe")));
				} else {
					contaApunteAudit.setImporteDebe(null);
				}
			}
			if (null != map.get("importeHaber")) {
				if (map.get("importeHaber") instanceof Double) {
					contaApunteAudit.setImporteHaber(BigDecimal.valueOf((Double) map.get("importeHaber")));
				} else if (map.get("importeHaber") instanceof Integer){
					contaApunteAudit.setImporteHaber(BigDecimal.valueOf((Integer) map.get("importeHaber")));
				} else {
					contaApunteAudit.setImporteHaber(null);
				}
			}
			if (null != map.get("importePyG")) {
				if (map.get("importePyG") instanceof Double) {
					contaApunteAudit.setImportePyG(BigDecimal.valueOf((Double) map.get("importePyG")));
				} else if (map.get("importePyG") instanceof Integer){
					contaApunteAudit.setImportePyG(BigDecimal.valueOf((Integer) map.get("importePyG")));
				} else {
					contaApunteAudit.setImportePyG(null);
				}
			}
			if (isAprobar) {
				contaApunteAudit.setEstado(1);
			} else {
				contaApunteAudit.setEstado(0);
			}


		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaApuntesAudit -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaApuntesAudit");

		return contaApunteAudit;
	}

	/**
	 * Realiza la conversion de tipo map a Tmct0ContaApuntesPndts
	 * 
	 * @param map -> Tmct0ContaApuntesPndts
	 * @return Tmct0ContaApuntesPndts
	 */
	public Tmct0ContaApuntesPndts getTmct0ContaApuntesPndts(LinkedHashMap map) {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaApuntesPndts");

		Tmct0ContaApuntesPndts contaApuntePndts = new Tmct0ContaApuntesPndts();

		try {
			contaApuntePndts.setFeApunte(new Timestamp((Long)map.get("feApunte")));
			contaApuntePndts.setUsuApunte((String) map.get("usuApunte"));
			contaApuntePndts.setConcepto(new Tmct0ContaConcepto());
			contaApuntePndts.getConcepto().setConcepto((String) map.get("concepto"));
			contaApuntePndts.getConcepto().setId(BigInteger.valueOf((Integer) map.get("idConcepto")));
			if (map.get("importeDebe") instanceof Double) {
				contaApuntePndts.setImporteDebe(BigDecimal.valueOf((Double) map.get("importeDebe")));
			} else {
				contaApuntePndts.setImporteDebe(BigDecimal.valueOf((Integer) map.get("importeDebe")));
			}
			if (map.get("importeHaber") instanceof Double) {
				contaApuntePndts.setImporteHaber(BigDecimal.valueOf((Double) map.get("importeHaber")));
			} else {
				contaApuntePndts.setImporteHaber(BigDecimal.valueOf((Integer) map.get("importeHaber")));
			}
			if (map.get("importePyG") instanceof Double) {
				contaApuntePndts.setImportePyG(BigDecimal.valueOf((Double) map.get("importePyG")));
			} else {
				contaApuntePndts.setImportePyG(BigDecimal.valueOf((Integer) map.get("importePyG")));
			}
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaApuntesPndts -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaApuntesPndts");

		return contaApuntePndts;
	}

	/**
	 * Devuelve un objeto Tmct0ContaToleranceAudit
	 * 
	 * @param String codPlantilla, Integer tolerance, String auditUser
	 * @return Tmct0ContaToleranceAudit
	 */
	public Tmct0ContaToleranceAudit getTmct0ContaToleranceAudit(String codPlantilla, Integer tolerance, String auditUser) {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaToleranceAudit");

		Tmct0ContaToleranceAudit contaToleranceAudit = new Tmct0ContaToleranceAudit();

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.set(Calendar.MILLISECOND, 0);
			contaToleranceAudit.setAuditDate(new Timestamp(cal.getTimeInMillis()));
			contaToleranceAudit.setAuditUser(auditUser);
			contaToleranceAudit.setCodPlantilla(codPlantilla);
			contaToleranceAudit.setTolerance(tolerance);
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaToleranceAudit -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaToleranceAudit");

		return contaToleranceAudit;
	}

	/**
	 * Realiza la conversion entre la entidad y el dto Tmct0ApuntesAuditoriaTablaDTO
	 * 
	 * @param datosApuntesAuditoria -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0ApuntesAuditoriaDTO getTmct0ApuntesAuditoriaTablaDTO(Tmct0ContaApuntesAudit datosApuntesAuditoria) {

		LOG.info("INICIO - ASSEMBLER - getTmct0ApuntesAuditoriaTablaDTO");

		Tmct0ApuntesAuditoriaDTO apuntesAuditDTO = new Tmct0ApuntesAuditoriaDTO();

		try {

			apuntesAuditDTO.setId(datosApuntesAuditoria.getId());
			apuntesAuditDTO.setFeApunte(datosApuntesAuditoria.getFeApunte());
			apuntesAuditDTO.setUsuApunte(datosApuntesAuditoria.getUsuApunte());
			apuntesAuditDTO.setConcepto(datosApuntesAuditoria.getConcepto().getConcepto());
			apuntesAuditDTO.setIdConcepto(datosApuntesAuditoria.getConcepto().getId());
			apuntesAuditDTO.setImporteDebe(datosApuntesAuditoria.getImporteDebe());
			apuntesAuditDTO.setImporteHaber(datosApuntesAuditoria.getImporteHaber());
			apuntesAuditDTO.setImportePyG(datosApuntesAuditoria.getImportePyG());
			if (datosApuntesAuditoria.getEstado() == 1) {
				apuntesAuditDTO.setEstado("APROBADO");
			} else {
				apuntesAuditDTO.setEstado("RECHAZADO");
			}


		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ApuntesAuditoriaTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ApuntesAuditoriaTablaDTO");

		return apuntesAuditDTO;

	}
	
	/**
	 * Realiza la conversion entre la entidad y el dto Tmct0ToleranceAuditoriaTablaDTO
	 * 
	 * @param datosToleranceAuditoria -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0ToleranceAuditoriaDTO getTmct0ToleranceAuditoriaTablaDTO(Tmct0ContaToleranceAudit datosToleranceAuditoria) {

		LOG.info("INICIO - ASSEMBLER - getTmct0ToleranceAuditoriaTablaDTO");

		Tmct0ToleranceAuditoriaDTO toleranceAuditDTO = new Tmct0ToleranceAuditoriaDTO();

		try {

			toleranceAuditDTO.setId(datosToleranceAuditoria.getId());
			toleranceAuditDTO.setAuditDate(datosToleranceAuditoria.getAuditDate());
			toleranceAuditDTO.setAuditUser(datosToleranceAuditoria.getAuditUser());
			toleranceAuditDTO.setCodPlantilla(datosToleranceAuditoria.getCodPlantilla());
			toleranceAuditDTO.setTolerance(datosToleranceAuditoria.getTolerance());

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ToleranceAuditoriaTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ToleranceAuditoriaTablaDTO");

		return toleranceAuditDTO;

	}

}
