package sibbac.business.accounting.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaPeriodosAssembler;
import sibbac.business.accounting.database.bo.Tmct0ContaPeriodosBo;
import sibbac.business.accounting.database.dao.Tmct0ContaPeriodosDaoImp;
import sibbac.business.accounting.database.dto.ContaPeriodosDTO;
import sibbac.business.accounting.database.dto.ContaPeriodosFiltroDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPeriodos;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@Service
public class SIBBACServiceContaPeriodos implements SIBBACServiceBean {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceContaPeriodos.class);

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";
	
	@Autowired
	private Tmct0ContaPeriodosDaoImp tmct0ContaPeriodosDaoImp;
	
	@Autowired
	private Tmct0ContaPeriodosBo tmct0ContaPeriodosBo;
	
	@Autowired
	private Tmct0ContaPeriodosAssembler tmct0ContaPeriodosAssembler;
	

	/**
	 * The Enum ComandoCombosPantallaConciliacion.
	 */
	private enum ComandoPeriodos {

		/** Consultar auxiliares bancarios. */
		CONSULTAR_PERIODOS("consultarPeriodos"),

		/** Consultar tipos de movimiento. */
		CREAR_PERIODO("crearPeriodo"),

		SIN_COMANDO("");

		/** The command. */
		private String command;

		/**
		 * Instantiates a new ComandoCombosPantallaConciliacion.
		 *
		 * @param command
		 *            the command
		 */
		private ComandoPeriodos(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * Process.
	 *
	 * @param webRequest
	 *            the web request
	 * @return the web response
	 * @throws SIBBACBusinessException
	 *             the SIBBAC business exception
	 */
	@Override
	public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandoPeriodos command = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceCombosPantallaConciliacion");

			switch (command) {
			case CONSULTAR_PERIODOS:
				result.setResultados(this.consultarPeriodos(paramsObjects));
				break;
			case CREAR_PERIODO:
				result.setResultados(this.crearPeriodos());
				break;

			default:
				break;
			}

		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

		LOG.debug("Salgo del servicio SIBBACServiceCombosPantallaConciliacion");
		return result;
	}
	
	private Map<String, Object> consultarPeriodos(Map<String, Object> paramsObjects) throws Exception{
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try{
		 
			ContaPeriodosFiltroDTO contaPeriodosFiltroDTO = tmct0ContaPeriodosAssembler.getContaPeriodosFiltroDTOByMapParamObject(paramsObjects);
			
			List<Tmct0ContaPeriodos> listPeriodos = tmct0ContaPeriodosDaoImp.consultarPeriodos(contaPeriodosFiltroDTO);
			
			List<ContaPeriodosDTO>  listPeridosDTO = tmct0ContaPeriodosAssembler.getContaPeriodosDTOByTmct0ContaPeriodos(listPeriodos);
			
			result.put("listaPeriodos",listPeridosDTO);
			
    	} catch (Exception e) {
	        LOG.error("Error metodo consultarPeriodos -  " + e.getMessage(), e);
	        result.put("status", "KO");
	        return result;
    	}
		
		return result;
	}
	
	private Map<String, Object> crearPeriodos() throws SIBBACBusinessException{
		
		return tmct0ContaPeriodosBo.crearPeriodos();
	}
	
	/**
	 * Gets the command.
	 *
	 * @param command
	 *            the command
	 * @return the command
	 */
	private ComandoPeriodos getCommand(String command) {
		ComandoPeriodos[] commands = ComandoPeriodos
				.values();
		ComandoPeriodos result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandoPeriodos.SIN_COMANDO;
		}
		LOG.debug(new StringBuffer("Se selecciona la accion: ").append(
				result.getCommand()).toString());
		return result;
	}
	
	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

}
