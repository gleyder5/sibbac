package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * DTO para representar datos del filtro de Our con Their
 */
public class FiltroOurConTheirDTO {

	private List<String> listAux;
	private String auxContable;
	private List<String> listAlias;
	private List<String> listIsin;
	private String sentido;
	private BigDecimal importeDesde;
	private BigDecimal importeHasta;
	private String nBooking;
	private String refDesglose;
	private String refCliente;
	private String refPataMercado;
	private String refPataCliente;
	private List<Date> listFechasContratacion;
	private List<Date> listFechasLiquidacion;

	/**
	 * Constructor no-arg.
	 */
	public FiltroOurConTheirDTO() {
		super();
	}

	public FiltroOurConTheirDTO(List<String> listAux,String auxContable,List<String> listAlias,
			List<String> listIsin,String sentido,BigDecimal importeDesde,BigDecimal importeHasta,
			String nBooking,String refDesglose,String refCliente,String refPataMercado,
			String refPataCliente,List<Date> listFechasContratacion,List<Date> listFechasLiquidacion) {
		super();
		this.listAux=listAux;
		this.auxContable=auxContable;
		this.listAlias=listAlias;
		this.listIsin=listIsin;
		this.sentido=sentido;
		this.importeDesde=importeDesde;
		this.importeHasta=importeHasta;
		this.nBooking=nBooking;
		this.refDesglose=refDesglose;
		this.refCliente=refCliente;
		this.refPataMercado=refPataMercado;
		this.refPataCliente=refPataCliente;
		this.listFechasContratacion=listFechasContratacion;
		this.listFechasLiquidacion=listFechasLiquidacion;
	}

	/**
	 * @return the listAux
	 */
	public List<String> getListAux() {
		return listAux;
	}

	/**
	 * @param listAux the listAux to set
	 */
	public void setListAux(List<String> listAux) {
		this.listAux = listAux;
	}

	/**
	 * @return the auxContable
	 */
	public String getAuxContable() {
		return auxContable;
	}

	/**
	 * @param auxContable the auxContable to set
	 */
	public void setAuxContable(String auxContable) {
		this.auxContable = auxContable;
	}

	/**
	 * @return the listAlias
	 */
	public List<String> getListAlias() {
		return listAlias;
	}

	/**
	 * @param listAlias the listAlias to set
	 */
	public void setListAlias(List<String> listAlias) {
		this.listAlias = listAlias;
	}

	/**
	 * @return the listIsin
	 */
	public List<String> getListIsin() {
		return listIsin;
	}

	/**
	 * @param listIsin the listIsin to set
	 */
	public void setListIsin(List<String> listIsin) {
		this.listIsin = listIsin;
	}

	/**
	 * @return the sentido
	 */
	public String getSentido() {
		return sentido;
	}

	/**
	 * @param sentido the sentido to set
	 */
	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	/**
	 * @return the importeDesde
	 */
	public BigDecimal getImporteDesde() {
		return importeDesde;
	}

	/**
	 * @param importeDesde the importeDesde to set
	 */
	public void setImporteDesde(BigDecimal importeDesde) {
		this.importeDesde = importeDesde;
	}

	/**
	 * @return the importeHasta
	 */
	public BigDecimal getImporteHasta() {
		return importeHasta;
	}

	/**
	 * @param importeHasta the importeHasta to set
	 */
	public void setImporteHasta(BigDecimal importeHasta) {
		this.importeHasta = importeHasta;
	}

	/**
	 * @return the nBooking
	 */
	public String getnBooking() {
		return nBooking;
	}

	/**
	 * @param nBooking the nBooking to set
	 */
	public void setnBooking(String nBooking) {
		this.nBooking = nBooking;
	}

	/**
	 * @return the refDesglose
	 */
	public String getRefDesglose() {
		return refDesglose;
	}

	/**
	 * @param refDesglose the refDesglose to set
	 */
	public void setRefDesglose(String refDesglose) {
		this.refDesglose = refDesglose;
	}

	/**
	 * @return the refCliente
	 */
	public String getRefCliente() {
		return refCliente;
	}

	/**
	 * @param refCliente the refCliente to set
	 */
	public void setRefCliente(String refCliente) {
		this.refCliente = refCliente;
	}

	/**
	 * @return the refPataMercado
	 */
	public String getRefPataMercado() {
		return refPataMercado;
	}

	/**
	 * @param refPataMercado the refPataMercado to set
	 */
	public void setRefPataMercado(String refPataMercado) {
		this.refPataMercado = refPataMercado;
	}

	/**
	 * @return the refPataCliente
	 */
	public String getRefPataCliente() {
		return refPataCliente;
	}

	/**
	 * @param refPataCliente the refPataCliente to set
	 */
	public void setRefPataCliente(String refPataCliente) {
		this.refPataCliente = refPataCliente;
	}

	/**
	 * @return the listFechasContratacion
	 */
	public List<Date> getListFechasContratacion() {
		return listFechasContratacion;
	}

	/**
	 * @param listFechasContratacion the listFechasContratacion to set
	 */
	public void setListFechasContratacion(List<Date> listFechasContratacion) {
		this.listFechasContratacion = listFechasContratacion;
	}

	/**
	 * @return the listFechasLiquidacion
	 */
	public List<Date> getListFechasLiquidacion() {
		return listFechasLiquidacion;
	}

	/**
	 * @param listFechasLiquidacion the listFechasLiquidacion to set
	 */
	public void setListFechasLiquidacion(List<Date> listFechasLiquidacion) {
		this.listFechasLiquidacion = listFechasLiquidacion;
	}
}