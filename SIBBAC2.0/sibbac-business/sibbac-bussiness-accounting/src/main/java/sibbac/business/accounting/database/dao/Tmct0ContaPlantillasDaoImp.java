package sibbac.business.accounting.database.dao;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.dto.AlcDTO;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.PersistenciaCobrosBarridosDTO;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.TextosSustitucionPlantillas;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;

/**
 * Data access object de conta plantillas.
 * 
 * @author Neoris
 *
 */
@Repository
public class Tmct0ContaPlantillasDaoImp {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaPlantillasDaoImp.class);

  @PersistenceContext
  private EntityManager em;
  
  
  private final static String SELECT_APUNTE_DETALLE = "select DISTINCT nuorden,nbooking,nucnfclt from bsnbpsql.tmct0_conta_apunte_detalle where idapunte in " 
		  											  + "(select id from bsnbpsql.tmct0_conta_apuntes where audit_date >= current date - 1 days)";
  
  private final static String SELECT_FACTURAS = "select distinct nuorden,nbooking,nucnfclt from bsnbpsql.tmct0_alc_orden where id in (select ID_ALC_ORDEN" 
												+ " from bsnbpsql.TMCT0_ALC_ORDEN_FACTURA_DETALLE where ID_FACTURA_DETALLE in (SELECT id FROM BSNBPSQL.TMCT0_FACTURA_DETALLE" 
												+ " WHERE ID_FACTURASUGERIDA in (SELECT ID_FACTURA_SUGERIDA FROM BSNBPSQL.TMCT0_FACTURA WHERE ID in (select DISTINCT idfactura"
												+" from bsnbpsql.tmct0_conta_apunte_detalle where idapunte in (select id from bsnbpsql.tmct0_conta_apuntes where audit_date >= current date - 1 days)))))";
  
  private final static String UPDATE_ALO_APUNTE_DETALLE_FACTURAS = "update bsnbpsql.tmct0alo set cdestadocont=(select idestado from bsnbpsql.tmct0estado where nombre='EN CURSO' " 
		  															+ "and idtipoestado=3) where (nuorden,nbooking,nucnfclt) in (values(?1,?2,?3))";
		  
  private final static String UPDATE_BOK_APUNTE_DETALLE_FACTURAS = "update bsnbpsql.tmct0bok set cdestadocont=(select idestado from bsnbpsql.tmct0estado where nombre='EN CURSO' "
		  												  			+ "and idtipoestado=3) where (nuorden,nbooking) in (values(?1,?2))";
  




  /**
   * Ejecuta un SELECT nativo.
   * 
   * @param queryStr String con el query.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> executeQuery(String queryStr, String codigoPlantilla) {

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    return query.getResultList();

  }
  
  /**
   * Ejecuta un SELECT nativo.
   * 
   * @param queryStr String con el query.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public Object executeQuerySingleResult(String queryStr, String codigoPlantilla) {

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    return query.getSingleResult();

  }

  /**
   * Ejecuta un COUNT nativo.
   * 
   * @param queryStr: Query a la que se le realizara un conteo nativo
   * @param params: Parametros a pasar
   * @return Resultado unico con conteo
   */
  @Transactional
  public Long executeQueryCount(String queryStr, String codigoPlantilla, List params) {

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    StringBuffer conteoQuery = new StringBuffer();
    conteoQuery.append("SELECT COUNT(*) FROM (");
    conteoQuery.append(queryStr.replaceAll(";", ""));
    conteoQuery.append(")");

    Query query = em.createNativeQuery(conteoQuery.toString());

    if (CollectionUtils.isNotEmpty(params)) {
      for (int j = 0; j < params.size(); j++) {
        query.setParameter(j + 1, params.get(j));
      }
    }

    Integer resultadoConteo = (Integer) query.getSingleResult();
    if (resultadoConteo != null) {
      return new Long(resultadoConteo);
    } else {
      return null;
    }

  }

  /**
   * Ejecuta un SELECT nativo con un parámetro.
   * 
   * @param queryStr String con el query.
   * @param param Parámetro.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> executeQuery(String queryStr, String param, String codigoPlantilla) {
    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    if (StringUtils.isNotBlank(param)) {
      LOG.trace(" Parametro de la query " + param);
      query.setParameter(1, param);
    }
    return query.getResultList();
  }
  
  
  /**
   * Ejecuta un SELECT nativo con un parámetro.
   * 
   * @param queryStr String con el query.
   * @param param Parámetro.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> executeQueryParams(String queryStr, List<Object> paramsDetail, String codigoPlantilla) {
	  
    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    
    for(int i=0;i<paramsDetail.size();i++){
    	LOG.trace(" Parametro de la query " + paramsDetail.get(i));
    	query.setParameter(i+1, paramsDetail.get(i));
    }
    
    return query.getResultList();
  }

  /**
   * Ejecuta un SELECT nativo con un parámetro.
   * 
   * @param queryStr String con el query.
   * @param param Parámetro.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> executeQueryOrdenFacturas(String queryStr, String param, String codigoPlantilla) {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue(), null);

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    LOG.trace(" Parametro de la query " + param);
    query.setParameter(1, param);
    return query.getResultList();
  }

  /**
   * Ejecuta un SELECT nativo con un parámetro de tipo Date.
   * 
   * @param queryStr String con el query.
   * @param param Parámetro.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> executeQuery(String queryStr, Date param, String codigoPlantilla) {
    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    LOG.trace(" Parametro de la query " + param);
    query.setParameter(1, param);
    return query.getResultList();
  }

  /**
   * Ejecuta un SELECT nativo con un parÃ¡metro.
   * 
   * @param queryStr String con el query.
   * @param param ParÃ¡metro.
   * @return Resultado de la consulta.
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> executeQueryFechas(String queryStr, String estado, Date fechaEjecucion, String codigoPlantilla) {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue(),
                                      fechaEjecucion);

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr.replaceAll(";", ""), true);
    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));
    LOG.trace(" Parametro de la query " + estado);
    query.setParameter(1, estado);
    LOG.trace(" Parametro de la query " + fechaEjecucion);
    query.setParameter(2, fechaEjecucion);
    return query.getResultList();
  }
  
  public List<Object[]> executeQueryApuntesManualesOrden(String queryStr, String codigoPlantilla) {

	    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue(), new Date());
	    
	    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr.replaceAll(";", ""), true);
	    Query query = em.createNativeQuery(queryStr.replaceAll(";", ""));

	    return query.getResultList();
  }

  public String replaceFechasQueryOrden(String queryStr, String codigoPlantilla) {

    Loggers.logTraceQueriesAccounting(codigoPlantilla, " Inicio Metodo replaceFechasQueryOrden - query recibida"
                                                       + queryStr, null);

    if (queryStr.indexOf(TextosSustitucionPlantillas.ENTRE.getTexto()) > -1
        || queryStr.indexOf(TextosSustitucionPlantillas.DESDE_INICIO_ANIO.getTexto()) > -1
        || queryStr.indexOf(TextosSustitucionPlantillas.DIAS_A_EJECUTAR.getTexto()) > -1) {

      if (queryStr.indexOf(TextosSustitucionPlantillas.ENTRE.getTexto()) > -1) {

    	int posEntre = queryStr.lastIndexOf(TextosSustitucionPlantillas.DESDE.getTexto());
        String textoEntre = queryStr.substring(posEntre, posEntre + 39);
        queryStr = queryStr.replaceAll(textoEntre, "?2");

      } else if (queryStr.indexOf(TextosSustitucionPlantillas.DESDE_INICIO_ANIO.getTexto()) > -1) {
        queryStr = queryStr.replaceAll(TextosSustitucionPlantillas.DESDE_INICIO_ANIO.getTexto(), "?2");
      } else if (queryStr.indexOf(TextosSustitucionPlantillas.DIAS_A_EJECUTAR.getTexto()) > -1) {

        int posDiasInicial = queryStr.lastIndexOf(TextosSustitucionPlantillas.DIAS_A_EJECUTAR.getTexto());
        int posDiasFinal = posDiasInicial + 16;
        while (!queryStr.substring(posDiasFinal, posDiasFinal + 1).equals(" ")) {
          posDiasFinal++;
        }
        String textoDias = queryStr.substring(posDiasInicial, posDiasFinal);
        queryStr = queryStr.replaceAll(textoDias, "?2");
      }

    }

    Loggers.logTraceQueriesAccounting(codigoPlantilla, " Inicio Metodo replaceFechasQueryOrden - query formada"
                                                       + queryStr, null);
    return queryStr;
  }

  public String getFromQueryOrden(String queryStr, String tipoPlantilla, String codigoPlantilla) {

    LOG.trace(getFechaHora() + " Inicio Metodo getFromQueryOrden - query " + queryStr + "tipoPlantilla "
              + tipoPlantilla);

    Loggers.logTraceQueriesAccounting(codigoPlantilla, " Inicio Metodo getFromQueryOrden - query recibida" + queryStr
                                                       + "tipoPlantilla " + tipoPlantilla, null);

    String from = "";
    if (queryStr.toLowerCase().indexOf(" from ") > -1) {
      from = queryStr.substring(queryStr.toLowerCase().indexOf(" from ") + 1);
    }

    if (tipoPlantilla.equals(TipoContaPlantillas.DEVENGO_FALLIDOS.getTipo())) {
      for (int i = from.length() - " and ".length(); i > 0; i--) {
        if (from.substring(i, i + " and ".length()).toLowerCase().equals(" and ")) {
          from = from.substring(0, i);
          break;
        }
      }
    }

    Loggers.logTraceQueriesAccounting(codigoPlantilla, " Fin Metodo getFromQueryOrden - from: " + from, null);

    LOG.trace(getFechaHora() + " Fin Metodo getFromQueryOrden - from: " + from);
    return from;
  }

  /**
   * Ejecuta un UPDATE nativo.
   * 
   * @param queryStr String con el query.
   */
  @Transactional
  public void executeQueryUpdate(String queryStr, String codigoPlantilla) {

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    Query query = em.createNativeQuery(queryStr);

    query.executeUpdate();

  }

  /**
   * Ejecuta un UPDATE nativo con dos parámetros.
   * 
   * @param queryStr String con el query.
   * @param param1 Parámetro uno.
   * @param param2 Parámetro dos.
   */
  @Transactional
  public void executeBloqueofacturas(String queryStr,
                                     String estadoInicial,
                                     String estadoBloqueado,
                                     String campoEstado,
                                     String codigoPlantilla) {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_BLOQUEO.getValue(), null);

    String campoBloqueo = "";
    if (StringUtils.isNotBlank(campoEstado)) {

      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campoEstado, ",");
      for (int i = 0; i < campos.length; i++) {
        if (i > 0) {
          campoBloqueo = campoBloqueo + ", ";
        }
        campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = ?2) ";
      }

      String updateBloqueo = "UPDATE bsnbpsql.tmct0_factura SET " + campoBloqueo + " WHERE ID IN (" + queryStr + ")";

      Loggers.logTraceQueriesAccounting(codigoPlantilla, updateBloqueo, true);
      Query query = em.createNativeQuery(updateBloqueo);
      LOG.trace(" Parametro de la query " + estadoInicial);
      query.setParameter(1, estadoInicial);
      LOG.trace(" Parametro de la query " + estadoBloqueado);
      query.setParameter(2, estadoBloqueado);

      query.executeUpdate();
    }
  }

  /**
   * Ejecuta un UPDATE nativo con dos parÃ¡metros.
   * 
   * @param queryStr String con el query.
   * @param param1 ParÃ¡metro uno.
   * @param param2 ParÃ¡metro dos.
   */
  @Transactional
  public void executeQueryBloqueoByFechasDevengosAnulacion(List<Object[]> alcImplicadas,
                                                           String estadoBloqueado,
                                                           String campoEstado,
                                                           String codigoPlantilla) {

    String campoBloqueo = "";
    if (StringUtils.isNotBlank(campoEstado)) {

      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campoEstado, ",");
      for (int i = 0; i < campos.length; i++) {
        if (i > 0) {
          campoBloqueo = campoBloqueo + ", ";
        }
        campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = ?1) ";

      }

      String updateBloqueo = "";
      for(Object[] alc : alcImplicadas){
    	  
    	  updateBloqueo  = "UPDATE bsnbpsql.tmct0alc SET " + campoBloqueo + " WHERE NUORDEN = ?2 AND NBOOKING = ?3 AND NUCNFLIQ = ?4 AND NUCNFCLT = ?5";
	
	      Query query = em.createNativeQuery(updateBloqueo);
	
	      query.setParameter(1, estadoBloqueado);
	      query.setParameter(2, alc[1]);
	      query.setParameter(3, alc[0]);
	      query.setParameter(4, alc[2]);
	      query.setParameter(5, alc[3]);
	      
	      
	
	      query.executeUpdate();
      }
    }
  }

  /**
   * Ejecuta un UPDATE nativo con dos parÃ¡metros.
   * 
   * @param queryStr String con el query.
   * @param param1 ParÃ¡metro uno.
   * @param param2 ParÃ¡metro dos.
   */
  @Transactional
  public void executeQueryBloqueoByFechasDevengosFallidos(List<Object[]> alcImplicadas,
												          String estadoBloqueado,
												          String campoEstado,
												          String codigoPlantilla) {

    String campoBloqueo = "";
    if (StringUtils.isNotBlank(campoEstado)) {

      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campoEstado, ",");
      for (int i = 0; i < campos.length; i++) {
        if (i > 0) {
          campoBloqueo = campoBloqueo + ", ";
        }
        campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = ?1) ";

      }

      String updateBloqueo = "";
      for(Object[] alc : alcImplicadas){
    	  
    	  updateBloqueo  = "UPDATE bsnbpsql.tmct0alc SET " + campoBloqueo + " WHERE NUORDEN = ?2 AND NBOOKING = ?3 AND NUCNFLIQ = ?4 AND NUCNFCLT = ?5";
	
	      Query query = em.createNativeQuery(updateBloqueo);
	
	      query.setParameter(1, estadoBloqueado);
	      query.setParameter(2, alc[1]);
	      query.setParameter(3, alc[0]);
	      query.setParameter(4, alc[2]);
	      query.setParameter(5, alc[3]);
	      
	      
	
	      query.executeUpdate();
      }
    }
  }

  
  /**
   * Ejecuta un UPDATE nativo con dos parámetros.
   * 
   * @param queryStr String con el query.
   * @param param1 Parámetro uno.
   * @param param2 Parámetro dos.
   */
  @Transactional
  public void executeQueryDesbloqueoFacturasManuales(String queryStr,
		  							 String estadoInicial,
                                     String estadoFinal,
                                     String codigoPlantilla) {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_BLOQUEO.getValue(), null);

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    
    Query query = em.createNativeQuery(queryStr);
    
    LOG.trace(" Parametro de la query " + estadoFinal);
    query.setParameter(1, estadoFinal);
    query.setParameter(2, estadoInicial);

    query.executeUpdate();

  }

  /**
   * Se ejecuta query de desbloqueo en cobros barridos.
   *
   * @param from: campo query de desbloqueo en plantilla
   * @param idNorma43Movimiento: id de norma 43 movimiento
   * @param estadoFinal: estado final de las ALCs
   * @param campoEstado: campo de estado de la plantilla
   * @param codigoPlantilla: codigo de la plantilla
   * @param rangoInicio: rango de inicio del intervalo
   * @param rangoFin: rango de fin del intervalo
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void ejecutaQueryDesbloqueoCobrosBarridos(PersistenciaCobrosBarridosDTO pcbDTO,
                                                   Long idNorma43Movimiento,
                                                   String codigoPlantilla,
                                                   int rangoInicio,
                                                   int rangoFin) {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_DESBLOQUEO.getValue(), null);

    // campoEstado y from
    StringBuffer query_desbloqueoSb = new StringBuffer();

    String campoBloqueo = "";
    if (StringUtils.isNotBlank(pcbDTO.getTmct0ContaPlantilla().getCampo_estado())) {

      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(pcbDTO.getTmct0ContaPlantilla()
                                                                                          .getCampo_estado(), ",");
      for (int i = 0; i < campos.length; i++) {
        if (i > 0) {
          campoBloqueo = campoBloqueo + ", ";
        }
        campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = ?2) ";

      }

      query_desbloqueoSb.append("UPDATE bsnbpsql.tmct0alc SET ");
      query_desbloqueoSb.append(campoBloqueo);
      query_desbloqueoSb.append(" WHERE (NUORDEN, NBOOKING, NUCNFCLT, NUCNFLIQ) IN ( ");

      // Esto se aplica si hay un rango de fin mayor que 0
      if (rangoFin > 0) {
        // Se agrega logica para procesar intervalos de registros
        query_desbloqueoSb.append(AccountingHelper.getQueryDesbloqueoCBByRango(pcbDTO.getTmct0ContaPlantilla()
                                                                                     .getQuery_bloqueo().toString())
                                                  .toString());
        Loggers.logTraceGenericoAccounting(codigoPlantilla, "Rango de registros Query_bloqueo: " + rangoInicio + " -> "
                                                            + rangoFin);
      } else {
        query_desbloqueoSb.append(pcbDTO.getTmct0ContaPlantilla().getQuery_bloqueo() + ")");
      }

      Query query = em.createNativeQuery(query_desbloqueoSb.toString());

      Loggers.logTraceQueriesAccounting(codigoPlantilla, query_desbloqueoSb.toString(), true);

      // id de movimiento
      Loggers.logTraceQueriesAccounting(codigoPlantilla, String.valueOf(idNorma43Movimiento), false);
      query.setParameter(1, idNorma43Movimiento);
      // Estado final
      Loggers.logTraceQueriesAccounting(codigoPlantilla, pcbDTO.getTmct0ContaPlantilla().getEstado_final(), false);
      query.setParameter(2, pcbDTO.getTmct0ContaPlantilla().getEstado_final());

      // Esto se aplica si hay un rango de fin mayor que 0
      if (rangoFin > 0) {
        // Rango Inicio
        LOG.trace(" Parametro de la query " + rangoInicio);
        query.setParameter(3, rangoInicio);
        // Rango fin
        LOG.trace(" Parametro de la query " + rangoFin);
        query.setParameter(4, rangoFin);
      }

      query.executeUpdate();
    }
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void executeQueryQuerySumaImporteCampoAlc(String campo, AlcDTO alcDto, BigDecimal importeACobrar,
                                                   String estadoFinal, String campoEstado, String codigoPlantilla) {

	    String campoBloqueo = "";
	    if (null != campoEstado && StringUtils.isNotBlank(campoEstado)) {
	
	      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campoEstado, ",");
	      for (int i = 0; i < campos.length; i++) {
	        campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = ?6) ";
	        if (StringUtils.isNotBlank(campoBloqueo))
	            campoBloqueo = campoBloqueo + ",";
	      }
	
	    }
	
	    String campoQuery = "";
	    if (StringUtils.isNotBlank(campo)) {
	      campoQuery = campo + " = " + campo + "+ ?5";
	    }
	
	    Query query = em.createNativeQuery("UPDATE bsnbpsql.TMCT0ALC set "
	                                       + campoBloqueo
	                                       + campoQuery
	                                       + " where NUORDEN = ?1 and  NBOOKING = ?2 and NUCNFCLT = ?3 and NUCNFLIQ = ?4");
	
	    Loggers.logTraceQueriesAccounting(codigoPlantilla, query.toString(), true);
	    LOG.trace(" Parametro de la query " + alcDto.getNuorden());
	    query.setParameter(1, alcDto.getNuorden());
	    LOG.trace(" Parametro de la query " + alcDto.getNbooking());
	    query.setParameter(2, alcDto.getNbooking());
	    LOG.trace(" Parametro de la query " + alcDto.getNucnfclt());
	    query.setParameter(3, alcDto.getNucnfclt());
	    LOG.trace(" Parametro de la query " + alcDto.getNucnfliq());
	    query.setParameter(4, alcDto.getNucnfliq());
	    if (!"".equals(campoQuery)) {
	      LOG.trace(" Parametro de la query " + importeACobrar);
	      query.setParameter(5, importeACobrar);
	    }
	    if (StringUtils.isNotBlank(campoEstado)) {
	    	LOG.trace(" Parametro de la query " + estadoFinal);
	        query.setParameter(6, estadoFinal);
	    }
	
	    query.executeUpdate();
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void executeQueryQuerySumaCampoAlc(String campo,
                                            String nuorden,
                                            String nbooking,
                                            String nucnfclt,
                                            short nucnfliq,
                                            BigDecimal importe,
                                            String estadoFinal,
                                            String campoEstado,
                                            String codigoPlantilla) {

    String campoBloqueo = "";
    if (StringUtils.isNotBlank(campoEstado)) {

      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campoEstado, ",");
      for (int i = 0; i < campos.length; i++) {
        if (!campos[i].equals(SibbacEnums.EstadosContabilidadEnum.CDESTADOCONT.getTexto())) {
          campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = ?1), ";
        }
      }

    }

    String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campo, ",");

    String camposQuery = "";
    for (int i = 0; i < campos.length; i++) {
      camposQuery = camposQuery + campos[i] + " = " + campos[i] + "+?6, ";
    }

    Query query = em.createNativeQuery("UPDATE bsnbpsql.TMCT0ALC set "
                                       + campoBloqueo
                                       + camposQuery
                                       + " CDESTADOCONT = (SELECT e.idestado FROM bsnbpsql.tmct0estado e WHERE e.NOMBRE = ?1)"
                                       + " where NUORDEN = ?2 and  NBOOKING = ?3 and NUCNFCLT = ?4 and NUCNFLIQ = ?5");

    Loggers.logTraceQueriesAccounting(codigoPlantilla, query.toString(), true);
    LOG.trace(" Parametro de la query " + estadoFinal);
    query.setParameter(1, estadoFinal);
    LOG.trace(" Parametro de la query " + nuorden);
    query.setParameter(2, nuorden);
    LOG.trace(" Parametro de la query " + nbooking);
    query.setParameter(3, nbooking);
    LOG.trace(" Parametro de la query " + nucnfclt);
    query.setParameter(4, nucnfclt);
    LOG.trace(" Parametro de la query " + nucnfliq);
    query.setParameter(5, nucnfliq);
    if (!"".equals(camposQuery)) {
      LOG.trace(" Parametro de la query " + importe);
      query.setParameter(6, importe);
    }

    query.executeUpdate();
  }
  
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void updateEstadoContALC(String estado,
                                  String nuorden,
                                  String nbooking,
                                  String nucnfclt,
                                  BigDecimal nucnfliq,
                                  String campoEstado,
                                  String codigoPlantilla) {

    String campoBloqueo = "";
    if (StringUtils.isNotBlank(campoEstado)) {

      String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(campoEstado, ",");
      for (int i = 0; i < campos.length; i++) {
        if (StringUtils.isNotBlank(campoBloqueo))
          campoBloqueo = campoBloqueo + ", ";
        campoBloqueo = campoBloqueo + campos[i] + "=(select idestado from bsnbpsql.tmct0estado where nombre = :estado) ";
      }

    }

    StringBuilder queryStr = new StringBuilder();

    queryStr.append("UPDATE bsnbpsql.tmct0alc SET ");
    queryStr.append(campoBloqueo);
    queryStr.append("WHERE NBOOKING = :nbooking AND NUORDEN = :nuorden AND NUCNFLIQ = :nucnfliq AND NUCNFCLT = :nucnfclt ");

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr.toString(), true);
    Query query = em.createNativeQuery(queryStr.toString());

    LOG.trace(" Parametro de la query " + String.valueOf(estado).replaceAll("'", "").replaceAll("\"", ""));
    query.setParameter("estado", String.valueOf(estado).replaceAll("'", "").replaceAll("\"", ""));
    LOG.trace(" Parametro de la query " + nbooking);
    query.setParameter("nbooking", nbooking);
    LOG.trace(" Parametro de la query " + nucnfclt);
    query.setParameter("nucnfclt", nucnfclt);
    LOG.trace(" Parametro de la query " + nuorden);
    query.setParameter("nuorden", nuorden);
    LOG.trace(" Parametro de la query " + nucnfliq);
    query.setParameter("nucnfliq", nucnfliq);

    query.executeUpdate();

  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void updateEstadoEntrecALC(String estado,
                                    String nuorden,
                                    String nbooking,
                                    String nucnfclt,
                                    BigDecimal nucnfliq,
                                    String codigoPlantilla) {

    StringBuilder queryStr = new StringBuilder();

    queryStr.append("UPDATE bsnbpsql.tmct0alc SET CDESTADOENTREC = ");
    queryStr.append("(select idestado from bsnbpsql.tmct0estado where nombre = :estado) ");
    queryStr.append("WHERE NBOOKING = :nbooking AND NUORDEN = :nuorden AND NUCNFLIQ = :nucnfliq AND NUCNFCLT = :nucnfclt ");

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr.toString(), true);
    Query query = em.createNativeQuery(queryStr.toString());
    LOG.trace(" Parametro de la query " + String.valueOf(estado).replaceAll("'", "").replaceAll("\"", ""));
    query.setParameter("estado", String.valueOf(estado).replaceAll("'", "").replaceAll("\"", ""));
    LOG.trace(" Parametro de la query " + nbooking);
    query.setParameter("nbooking", nbooking);
    LOG.trace(" Parametro de la query " + nucnfclt);
    query.setParameter("nucnfclt", nucnfclt);
    LOG.trace(" Parametro de la query " + nuorden);
    query.setParameter("nuorden", nuorden);
    LOG.trace(" Parametro de la query " + nucnfclt);
    query.setParameter("nucnfclt", nucnfclt);
    LOG.trace(" Parametro de la query " + nucnfliq);
    query.setParameter("nucnfliq", nucnfliq);

    query.executeUpdate();

  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void updateEstadoDesgloseCamara(String estado,
                                         String nuorden,
                                         String nbooking,
                                         String nucnfclt,
                                         BigDecimal nucnfliq,
                                         String codigoPlantilla) {

    StringBuilder queryStr = new StringBuilder();

    queryStr.append("UPDATE bsnbpsql.TMCT0DESGLOSECAMARA SET CDESTADOENTREC = ");
    queryStr.append("(select idestado from bsnbpsql.tmct0estado where nombre = :estado) ");
    queryStr.append("WHERE NBOOKING = :nbooking AND NUORDEN = :nuorden AND NUCNFLIQ = :nucnfliq AND NUCNFCLT = :nucnfclt ");

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr.toString(), true);
    Query query = em.createNativeQuery(queryStr.toString());

    LOG.trace(" Parametro de la query " + String.valueOf(estado).replaceAll("'", "").replaceAll("\"", ""));
    query.setParameter("estado", String.valueOf(estado).replaceAll("'", "").replaceAll("\"", ""));
    LOG.trace(" Parametro de la query " + nbooking);
    query.setParameter("nbooking", nbooking);
    LOG.trace(" Parametro de la query " + nucnfclt);
    query.setParameter("nucnfclt", nucnfclt);
    LOG.trace(" Parametro de la query " + nuorden);
    query.setParameter("nuorden", nuorden);
    LOG.trace(" Parametro de la query " + nucnfclt);
    query.setParameter("nucnfclt", nucnfclt);
    LOG.trace(" Parametro de la query " + nucnfliq);
    query.setParameter("nucnfliq", nucnfliq);

    query.executeUpdate();

  }

  private String getFechaHora() {
    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    return formatoFecha.format(new Date());
  }

  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void executeUpdateFinales() {

    LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Ejecutando query " + SELECT_APUNTE_DETALLE);
    
    Query query = em.createNativeQuery(SELECT_APUNTE_DETALLE);
    List<Object[]> listApuntesDetalle = query.getResultList();
    
    LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Comienzao ejecucion query: " + UPDATE_ALO_APUNTE_DETALLE_FACTURAS);
    LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Comienzao ejecucion query: " + UPDATE_BOK_APUNTE_DETALLE_FACTURAS);
    
    for(Object[] apunteDetalle : listApuntesDetalle){
    	
        LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Ejecutando query " + UPDATE_ALO_APUNTE_DETALLE_FACTURAS);
        
        query = em.createNativeQuery(UPDATE_ALO_APUNTE_DETALLE_FACTURAS);
        query.setParameter(1, apunteDetalle[0]);
        query.setParameter(2, apunteDetalle[1]);
        query.setParameter(3, apunteDetalle[2]);
        query.executeUpdate();
        
        LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Ejecutando query " + UPDATE_BOK_APUNTE_DETALLE_FACTURAS);
        query = em.createNativeQuery(UPDATE_BOK_APUNTE_DETALLE_FACTURAS);
        query.setParameter(1, apunteDetalle[0]);
        query.setParameter(2, apunteDetalle[1]);
        query.executeUpdate();
    }
    
    
    
    //Actualizacion de registros de tipo factura
    LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Ejecutando query " + SELECT_FACTURAS);
    
    
    query = em.createNativeQuery(SELECT_FACTURAS);
    List<Object[]> listFacturas = query.getResultList();
    
    for(Object[] factura : listFacturas){
    	
        
        
        query = em.createNativeQuery(UPDATE_ALO_APUNTE_DETALLE_FACTURAS);
        query.setParameter(1, factura[0]);
        query.setParameter(2, factura[1]);
        query.setParameter(3, factura[2]);
        query.executeUpdate();
        
        
        query = em.createNativeQuery(UPDATE_BOK_APUNTE_DETALLE_FACTURAS);
        query.setParameter(1, factura[0]);
        query.setParameter(2, factura[1]);
        query.executeUpdate();
    }
    
    LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Fin ejecucion query " + UPDATE_ALO_APUNTE_DETALLE_FACTURAS);
    LOG.trace(Loggers.getDatosComunesLogsInicial(null) + " Fin ejecucion query " + UPDATE_BOK_APUNTE_DETALLE_FACTURAS);
    
  }

}
