package sibbac.business.accounting.enums;

/**
 *	Enumeracion para tipos de archivos para apuntes manuales
 *	en formato Excel.
 */
public enum TiposApuntesManualesExcelEnum {
	
	APUNTE_SALDO("APUNTE SALDO"),
	APUNTE_SIBBAC("APUNTE SIBBAC");
	
	private String tipoApunteManualExcel;

	private TiposApuntesManualesExcelEnum(String tipoApunteManualExcel) {
		this.tipoApunteManualExcel = tipoApunteManualExcel;
	}

	public String getTipoApunteManualExcel() {
		return this.tipoApunteManualExcel;
	}

	public void setTipoApunteManualExcel(String tipoApunteManualExcel) {
		this.tipoApunteManualExcel = tipoApunteManualExcel;
	}
	
}