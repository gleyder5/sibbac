package sibbac.business.accounting.exception;

/**
 * Exception utilizada para el manejo de errores en Ejecución de Plantillas.
 * 
 * @author Neoris
 *
 */
public class EjecucionPlantillaException extends Exception {

	/* Serial version UID */
	private static final long serialVersionUID = -8844662802647154136L;

	/** 
	 * Constructor por defecto.
	 */
	public EjecucionPlantillaException() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            Mensaje de error.
	 * @param cause
	 *            Causa del error.
	 */
	public EjecucionPlantillaException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            Mensaje de error.
	 */
	public EjecucionPlantillaException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause
	 *            Causa del error.
	 */
	public EjecucionPlantillaException(Throwable cause) {
		super(cause);
	}
}
