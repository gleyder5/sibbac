package sibbac.business.accounting.conciliacion.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0ContaPartidasPndtsBo;
import sibbac.business.accounting.conciliacion.database.bo.Tmct0ContaPlantillasConciliaBo;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConceptoDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPlantillasConciliaDao;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.contabilidad.database.dao.CuentaContableDao;
import sibbac.business.contabilidad.database.model.CuentaContable;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.business.utilities.utils.LoggerComun;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.Tmct0SelectsBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.model.Tmct0Users;

@SIBBACService
public class SIBBACServiceCombosPantallaConciliacion implements
SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SIBBACServiceCombosPantallaConciliacion.class);
	
	private final String CLASE = this.getClass().getSimpleName();

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	@Autowired
	private Tmct0SelectsBo tmct0SelectsBo;

	@Autowired
	private Tmct0ContaPartidasPndtsBo tmct0ContaPartidasPndtsBo;

	@Autowired
  private Tmct0ContaPlantillasConciliaDao tmct0ContaPlantillasConciliaDao;
	
	@Autowired
  private Tmct0ContaPlantillasDao tmct0ContaPlantillasDao;

	@Autowired
	Tmct0UsersDao tmct0UsersDao;

	@Autowired
	CuentaContableDao cuentaContableDao;

	@Autowired
	Tmct0ContaConceptoDAO tmct0ContaConceptoDAO;

	@Autowired
	Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

	@Autowired
	Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;

	/**
	 * The Enum ComandoCombosPantallaConciliacion.
	 */
	private enum ComandoCombosPantallaConciliacion {

		/** Consultar auxiliares bancarios. */
		CONSULTAR_AUXILIAR_BANCARIO("consultarAuxiliares"),

		/** Consultar tipos de movimiento. */
		TIPOS_DE_MOVIMIENTO("consultarTiposDeMovimiento"),

		/** Consultar Códigos Plantillas. */
		CONSULTAR_CODIGOS_PLANTILLAS("consultarCodigosPlantillas"),

		/** Consultar Usuarios. */
		CONSULTAR_USUARIOS("consultarUsuarios"),

		/** Consultar Cuentas Contables. */
		CONSULTAR_CUENTAS_CONTABLES("consultarCuentasContables"),
		
    /** Consultar Plantillas THEIR*/
    CONSULTAR_PLANTILLAS_THEIR("consultarPlantillasTheir"),

		/** Consultar Conceptos. */
		CONSULTAR_CONCEPTOS("consultarConceptos"),

		/** Consultar Usuarios. */
		CONSULTAR_CONCEPTO_MOVIMIENTO("consultarConceptoMovimiento"),

		/** Consultar Cuentas Contables. */
		CONSULTAR_NUMEROS_DOCUMENTO("consultarNumerosDocumento"),

		/** Consultar Conceptos. */
		CONSULTAR_COMENTARIOS("consultarComentarios"),

		/** Consultar Sentidos. */
		CONSULTAR_SENTIDOS("consultarSentidos"),

		/** Consultar Antiguedades. */
		CONSULTAR_ANTIGUEDADES("consultarAntiguedades"),

		SIN_COMANDO("");

		/** The command. */
		private String command;

		/**
		 * Instantiates a new ComandoCombosPantallaConciliacion.
		 *
		 * @param command
		 *            the command
		 */
		private ComandoCombosPantallaConciliacion(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * Process.
	 *
	 * @param webRequest
	 *            the web request
	 * @return the web response
	 * @throws SIBBACBusinessException
	 *             the SIBBAC business exception
	 */
	@Override
	public WebResponse process(WebRequest webRequest)
			throws SIBBACBusinessException {
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandoCombosPantallaConciliacion command = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceCombosPantallaConciliacion");

			switch (command) {
			case CONSULTAR_AUXILIAR_BANCARIO:
				result.setResultados(this.getAuxiliaresBancario());
				break;
			case CONSULTAR_COMENTARIOS:
				result.setResultados(this.consultarComentarios());
				break;
			case CONSULTAR_CONCEPTO_MOVIMIENTO:
				result.setResultados(this.consultarConceptosMovimiento());
				break;
			case CONSULTAR_NUMEROS_DOCUMENTO:
				result.setResultados(this.consultarNumerosDocumento());
				break;
			case TIPOS_DE_MOVIMIENTO:
				result.setResultados(this.getTiposDeMovimiento());
				break;
			case CONSULTAR_CODIGOS_PLANTILLAS:
				result.setResultados(this.consultarCodigosPlantillas());
				break;
			case CONSULTAR_USUARIOS:
				result.setResultados(this.consultarUsuarios());
				break;
			case CONSULTAR_CUENTAS_CONTABLES:
				result.setResultados(this.consultarCuentasContables());
				break;
			case CONSULTAR_CONCEPTOS:
				result.setResultados(this.consultarConceptos());
				break;
			case CONSULTAR_SENTIDOS:
				result.setResultados(this.consultarSentidos());
				break;
			case CONSULTAR_ANTIGUEDADES:
        result.setResultados(this.consultarAntiguedades());
        break;
			case CONSULTAR_PLANTILLAS_THEIR:
        result.setResultados(this.consultarPlantillasTheir());
        break;        
				
				//CONSULTAR_PLANTILLAS
			default:
				break;
			}

			// Se fijan los resultados.
		} catch (SIBBACBusinessException e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

		LOG.debug("Salgo del servicio SIBBACServiceCombosPantallaConciliacion");
		return result;
	}

	/**
	 *	Devuelve valores combo auxiliar bancario.
	 *	@throws SIBBACBusinessException
	 *	@return List<SelectDTO> 
	 *	@throws SIBBACBusinessException 
	 */
	private Map<String, Object> getAuxiliaresBancario() throws SIBBACBusinessException {
		LOG.info(LoggerComun.getLogInicioMetodo(CLASE, "getAuxiliaresBancario"));
		Map<String, Object> result = new HashMap<String, Object>();
		List<SelectDTO> auxiliarBancarioSelectDto = new ArrayList<SelectDTO>();
		try {

			auxiliarBancarioSelectDto = this.tmct0ContaPlantillasConciliaBo.getDTOCombosByColumnName(PantallaConciliacionEnum.NombreColumnasContaPlantillasConciliaEnum.AUXILIAR.getNombreColumna());

		} catch (SIBBACBusinessException e) {
			LOG.error(LoggerComun.getLogError(Constantes.FUNCIONALIDAD_CONCILIACION_BANCARIA, CLASE, "getAuxiliaresBancario"), e);
			result.put("status", "KO");
			return result;
		} catch (Exception e) {
			LOG.error(LoggerComun.getLogError(Constantes.FUNCIONALIDAD_CONCILIACION_BANCARIA, CLASE, "getAuxiliaresBancario"), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaAuxiliaresBancario", auxiliarBancarioSelectDto);

		LOG.info(LoggerComun.getLogFinMetodo(CLASE, "getAuxiliaresBancario"));

		return result;
	}

	/**
	 *	Devuelve valores combo tipos de movimiento.
	 *	@throws SIBBACBusinessException
	 *	@return List<SelectDTO> 
	 *	@throws SIBBACBusinessException 
	 */
	private Map<String, Object> getTiposDeMovimiento() throws SIBBACBusinessException {
		LOG.info("INICIO - SERVICIO - getTiposDeMovimiento");
		Map<String, Object> result = new HashMap<String, Object>();
		List<SelectDTO> tiposMovimientoSelectDto = new ArrayList<SelectDTO>();
		try {
			tiposMovimientoSelectDto = this.tmct0ContaPartidasPndtsBo.getDTOCombosByColumnName(PantallaConciliacionEnum.NombreColumnasContaPartidasPndtsEnum.TIPO.getNombreColumna());
		} catch (SIBBACBusinessException e) {
			LOG.error("Error metodo getTiposDeMovimiento -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		} catch (Exception e) {
			LOG.error("Error metodo getTiposDeMovimiento -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaTiposMovimiento", tiposMovimientoSelectDto);

		LOG.info("FIN - SERVICIO - getTiposDeMovimiento");

		return result;
	}

	/**
	 * Consulta de códigos de plantillas
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarCodigosPlantillas() throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - consultarCodigosPlantillas");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaCodigosPlantillas = new ArrayList<Object>();

		try {

			// Realizamos la llamada a la consulta de facturas manuales
			List<Object> lisCodigosPlantillas = tmct0ContaPlantillasConciliaDao.findAllDistinctCodigos();

			// Se transforman a lista de DTO
			for (Object object : lisCodigosPlantillas) {
				SelectDTO plantillaConciliaDTO = new SelectDTO(((Object[]) object)[0].toString(), ((Object[]) object)[1].toString());
				listaCodigosPlantillas.add(plantillaConciliaDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarCodigosPlantillas -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaCodigosPlantillas", listaCodigosPlantillas);

		LOG.info("FIN - SERVICIO - consultarCodigosPlantillas");

		return result;
	}
	

  /**
   * Consulta de códigos de plantillas
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarPlantillasTheir() throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarPlantillasTheir");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaCodigosPlantillasTheir = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta para obtener todas las plantillas
      List<Tmct0ContaPlantillas> listPlantillas = tmct0ContaPlantillasDao.getAllTheir();

      // Se transforman a lista de DTO
      for (Tmct0ContaPlantillas plantilla : listPlantillas) {
        Tmct0ContaPlantillasDTO contaPlantillaDTO = new Tmct0ContaPlantillasDTO();
        contaPlantillaDTO.setCodigo(plantilla.getCodigo());
        contaPlantillaDTO.setTolerance(plantilla.getTolerancia());
        listaCodigosPlantillasTheir.add(contaPlantillaDTO);
      }
    }
    catch (Exception e) {
      LOG.error("Error metodo consultarPlantillas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaCodigosPlantillasTheir", listaCodigosPlantillasTheir);
    LOG.info("FIN - SERVICIO - consultarCodigosPlantillas");
    return result;
  }

	/**
	 * Consulta de usuarios
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarUsuarios() throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - consultarUsuarios");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaUsuarios = new ArrayList<Object>();

		try {
			// Realizamos la llamada a la consulta de usuarios
			List<Tmct0Users> lisUsuarios = tmct0UsersDao.findUsersOrderByUsername();

			// Se transforman a lista de DTO
			for (Tmct0Users user : lisUsuarios) {
				SelectDTO userDTO = new SelectDTO(
					user.getUsername(), 
					StringHelper.safeNull(user.getName()) 
						+ " " + StringHelper.safeNull(user.getLastname()));
				listaUsuarios.add(userDTO);
			}
		} catch (Exception e) {
			LOG.error("Error metodo consultarUsuarios -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaUsuarios", listaUsuarios);

		LOG.info("FIN - SERVICIO - consultarUsuarios");

		return result;
	}

	/**
	 * Consulta de cuentas contables
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarCuentasContables() throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - consultarCuentasContables");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaCuentasContables = new ArrayList<Object>();

		try {

			// Realizamos la llamada a la consulta de cuentas contables
			List<CuentaContable> lisCuentasContables = cuentaContableDao.findCuentaContOrderByNombre();

			// Se transforman a lista de DTO
			for (CuentaContable cuentaContable : lisCuentasContables) {
				SelectDTO userDTO = new SelectDTO(String.valueOf(cuentaContable.getCuenta()), cuentaContable.getCuenta()+" - "+cuentaContable.getNombre(), String.valueOf(cuentaContable.isWithAuxiliar()) );
				listaCuentasContables.add(userDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarCuentasContables -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaCuentasContables", listaCuentasContables);

		LOG.info("FIN - SERVICIO - consultarCuentasContables");

		return result;
	}

	/**
	 * Consulta de conceptos
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarConceptos() throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - consultarConceptos");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaConceptos = new ArrayList<Object>();

		try {

			// Realizamos la llamada a la consulta de conceptos
			Set<Tmct0ContaConcepto> lisConceptos = tmct0ContaConceptoDAO.findConceptosOrderByConcepto();

			// Se transforman a lista de DTO
			for (Tmct0ContaConcepto concepto : lisConceptos) {
				SelectDTO userDTO = new SelectDTO(concepto.getId().longValue(), concepto.getConcepto());
				listaConceptos.add(userDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarConceptos -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaConceptos", listaConceptos);

		LOG.info("FIN - SERVICIO - consultarConceptos");

		return result;
	}

	/**
	 * Obtencion de los conceptos movimiento (referencia)
	 * @return Map<String, Object>
	 * @throws SIBBACBusinessException
	 */
	private Map<String, Object> consultarConceptosMovimiento() throws SIBBACBusinessException {

		LOG.trace("Iniciando consultarConceptosMovimiento");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SelectDTO> listaConceptoMov = new ArrayList<SelectDTO>();
			listaConceptoMov = this.tmct0ContaPartidasPndtsBo.getDTOCombosByColumnName(
					PantallaConciliacionEnum.NombreColumnasContaPartidasPndtsEnum.REFERENCIA.getNombreColumna());
			result.put("listaConceptoMov", listaConceptoMov);
		} catch (Exception e) {
			LOG.error("Error metodo consultarConceptosMovimiento -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}
		LOG.trace("Fin consultarConceptosMovimiento");
		return result;
	}

	/**
	 * Obtencion de los comentarios de la tabla de conta pendientes.
	 * @return Map<String, Object>
	 * @throws SIBBACBusinessException
	 */
	private Map<String, Object> consultarComentarios() throws SIBBACBusinessException {

		LOG.trace("Iniciando consultarComentarios");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SelectDTO> listaComentarios = new ArrayList<SelectDTO>();
			listaComentarios = this.tmct0ContaPartidasPndtsBo.getDTOCombosByColumnName(
					PantallaConciliacionEnum.NombreColumnasContaPartidasPndtsEnum.COMENTARIO.getNombreColumna());
			result.put("listaComentarios", listaComentarios);
		} catch (Exception e) {
			LOG.error("Error metodo consultarComentarios -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}
		LOG.trace("Fin consultarComentarios");
		return result;
	}

	/**
	 * Obtencion de los nros. de documento en tabla conta partidas pendientes.
	 * @return Map<String, Object>
	 * @throws SIBBACBusinessException
	 */
	private Map<String, Object> consultarNumerosDocumento() throws SIBBACBusinessException {

		LOG.trace("Iniciando consultarNumerosDocumento");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SelectDTO> listaNrosDocumento = new ArrayList<SelectDTO>();
			listaNrosDocumento = this.tmct0ContaPartidasPndtsBo.getDTOCombosByColumnName(
					PantallaConciliacionEnum.NombreColumnasContaPartidasPndtsEnum.NUM_DOCUMENTO.getNombreColumna());
			result.put("listaNrosDocumento", listaNrosDocumento);
		} catch (Exception e) {
			LOG.error("Error metodo consultarNumerosDocumento -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}
		LOG.trace("Fin consultarNumerosDocumento");
		return result;
	}

	/**
	 * Consulta de sentidos
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarSentidos() throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - consultarSentidos");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaSentidos = new ArrayList<Object>();
		String[] lisSentidosSplit = null;

		try {

			// Realizamos la llamada a la consulta de conceptos
			List<Tmct0SibbacConstantes> lisSentidos = tmct0SibbacConstantesDao.findByClave(Constantes.CLAVE_SENTIDOS);

			if (null!=lisSentidos && lisSentidos.size()>0) {
				lisSentidosSplit = StringUtils.splitPreserveAllTokens(lisSentidos.get(0).getValor(), ",");
			}

			// Se transforman a lista de DTO
			for (String sentido : lisSentidosSplit) {

				SelectDTO sentidoDTO = new SelectDTO(sentido, sentido);
				listaSentidos.add(sentidoDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarSentidos -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaSentidos", listaSentidos);

		LOG.info("FIN - SERVICIO - consultarSentidos");

		return result;
	}

	/**
	 * Consulta de Antiguedades
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarAntiguedades() throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - consultarAntiguedades");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaAntiguedades = new ArrayList<Object>();
		String[] lisAntiguedadesSplit = null;

		try {

			// Realizamos la llamada a la consulta de conceptos
			List<Tmct0SibbacConstantes> lisAntiguedades = tmct0SibbacConstantesDao.findByClave(Constantes.CLAVE_ANTIGUEDADES);

			if (null!=lisAntiguedades && lisAntiguedades.size()>0) {
				lisAntiguedadesSplit = StringUtils.splitPreserveAllTokens(lisAntiguedades.get(0).getValor(), ",");
			}

			// Se transforman a lista de DTO
			for (String antiguedad : lisAntiguedadesSplit) {
				SelectDTO antiguedadDTO = new SelectDTO(antiguedad, antiguedad);
				listaAntiguedades.add(antiguedadDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarAntiguedades -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaAntiguedades", listaAntiguedades);

		LOG.info("FIN - SERVICIO - consultarAntiguedades");

		return result;
	}

	/**
	 * Gets the command.
	 *
	 * @param command
	 *            the command
	 * @return the command
	 */
	private ComandoCombosPantallaConciliacion getCommand(String command) {
		ComandoCombosPantallaConciliacion[] commands = ComandoCombosPantallaConciliacion
				.values();
		ComandoCombosPantallaConciliacion result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandoCombosPantallaConciliacion.SIN_COMANDO;
		}
		LOG.debug(new StringBuffer("Se selecciona la accion: ").append(
				result.getCommand()).toString());
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

}
