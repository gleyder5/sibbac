package sibbac.business.accounting.database.dto;

import java.math.BigDecimal;

/**
 * Data Transfer Object para la gestión de Cobros.
 * 
 * @author Neoris
 *
 */
public class BalanceDTO {

	private BigDecimal importeDebe;
	private BigDecimal importeHaber;
	private BigDecimal balance;

	public BalanceDTO() {
		super();
		importeDebe = BigDecimal.ZERO;
		importeHaber = BigDecimal.ZERO;
		balance = BigDecimal.ZERO;
	}

	public BigDecimal getImporteDebe() {
		return importeDebe;
	}

	public void setImporteDebe(BigDecimal importeDebe) {
		this.importeDebe = importeDebe;
	}

	public BigDecimal getImporteHaber() {
		return importeHaber;
	}

	public void setImporteHaber(BigDecimal importeHaber) {
		this.importeHaber = importeHaber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BalanceDTO [importeDebe=" + importeDebe + ", importeHaber=" + importeHaber + ", balance=" + balance
				+ "]";
	}

	public boolean isCero() {
		return (BigDecimal.ZERO.compareTo(balance) != 0);
	}

}
