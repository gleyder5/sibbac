package sibbac.business.accounting.conciliacion.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPlantillasConcilia;

@Repository
public class Tmct0PantallaConciliacionOURDaoImpl {

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0PantallaConciliacionOURDaoImpl.class);

	@SuppressWarnings("unchecked")
	public List<Object> ejecutarOurConTheir(List<Tmct0ContaPlantillasConcilia> listaPlantillasConcilia, FiltroOurConTheirDTO filtro, List<String> listaWhere) {

		LOG.info("INICIO - DAOIMPL - ejecutarOurConTheir");

		List<Object> listOurConTheir = new ArrayList<Object>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		int sizeFechas = 1;
		boolean esFhContratacion = false;
		boolean esFhLiquidacion = false;

		try {

			if (null!=filtro.getListFechasContratacion() && filtro.getListFechasContratacion().size()>0) {
				sizeFechas=filtro.getListFechasContratacion().size();
				esFhContratacion=true;
			} else if (null!=filtro.getListFechasLiquidacion() && filtro.getListFechasLiquidacion().size()>0) {
				sizeFechas=filtro.getListFechasLiquidacion().size();
				esFhLiquidacion=true;
			}

			StringBuilder consulta = new StringBuilder("");

			for (int i=0;i<listaPlantillasConcilia.size();i++) {
				Tmct0ContaPlantillasConcilia plantilla = listaPlantillasConcilia.get(i);

				// Añadimos si aplica las fechas en el WHERE
				// El campo FH. CONTRATACION en la query es el campo ALC.FEEJELIQ
				if (esFhContratacion) {
					listaWhere.set(i, listaWhere.get(i).concat(" AND date(ALC.FEEJELIQ) = :feContratacion"));
					//plantilla.setWhereQuery(listaPlantillasConcilia.get(i).getWhereQuery()+" AND date(ALC.FEEJELIQ) = :feContratacion");
				}
				// El campo FH. LIQUIDACION en la query es el campo ALC.FEOPELIQ
				if (esFhLiquidacion) {
					listaWhere.set(i, listaWhere.get(i).concat(" AND date(ALC.FEOPELIQ) = :feLiquidacion"));
					//plantilla.setWhereQuery(listaPlantillasConcilia.get(i).getWhereQuery()+" AND date(ALC.FEOPELIQ) = :feLiquidacion");
				}	

				// Construimos la query con los campos SELECT, FROM, WHERE, HAVING, GROUPBY y ORDERBY
				consulta.append(plantilla.getSelectQuery());
				consulta.append(" ");
				consulta.append(plantilla.getFromQuery());
				consulta.append(" ");
				consulta.append(listaWhere.get(i));
				consulta.append(" ");
				consulta.append(plantilla.getHavingQuery());
				consulta.append(" ");
				consulta.append(plantilla.getGroupBy());
				consulta.append(" ");
				consulta.append(plantilla.getOrderBy());
				if (i!=listaPlantillasConcilia.size()-1) {
					consulta.append(" UNION ");
				}
			}

			for (int i=0;i<sizeFechas;i++) {

				if (esFhContratacion) {
					parameters.put("feContratacion", filtro.getListFechasContratacion().get(i));
					LOG.debug("Creamos y ejecutamos la query: " + consulta.toString() + " con fecha "+filtro.getListFechasContratacion().get(i));
				}

				if (esFhLiquidacion) {
					parameters.put("feLiquidacion", filtro.getListFechasLiquidacion().get(i));
					LOG.debug("Creamos y ejecutamos la query: " + consulta.toString() + " con fecha "+filtro.getListFechasLiquidacion().get(i));
				}	

				Query query = null;

				query = entityManager.createNativeQuery(consulta.toString());

				for (Entry<String, Object> entry : parameters.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}

				List<Object> resultList = null;

				resultList = query.getResultList();

				for (int j=0;j<resultList.size();j++) {
					listOurConTheir.add(resultList.get(j));
				}

			}

		} catch (Exception e) {
			LOG.error("Error metodo ejecutarOurConTheir -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - ejecutarOurConTheir");

		return listOurConTheir;
	}

}
