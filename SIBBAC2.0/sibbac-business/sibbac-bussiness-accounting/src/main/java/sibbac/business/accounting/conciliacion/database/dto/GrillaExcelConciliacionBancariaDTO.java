package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;

/**
 * @author lucio.vilar
 *
 */
public class GrillaExcelConciliacionBancariaDTO {

	private String plantilla;
	private String auxDebe;
	private String conceptoDebe;
	private BigDecimal importeDebe;
	private String auxHaber;
	private String conceptoHaber;
	private BigDecimal importeHaber;
	private BigDecimal importePyG;
	private String cuentaContableDebe;
	private String cuentaContableHaber;
	private boolean completa;
	private boolean credit;
	
	/**
	 * @return the plantilla
	 */
	public String getPlantilla() {
		return plantilla;
	}
	/**
	 * @param plantilla the plantilla to set
	 */
	public void setPlantilla(String plantilla) {
		this.plantilla = plantilla;
	}
	/**
	 * @return the auxDebe
	 */
	public String getAuxDebe() {
		return auxDebe;
	}
	/**
	 * @param auxDebe the auxDebe to set
	 */
	public void setAuxDebe(String auxDebe) {
		this.auxDebe = auxDebe;
	}
	/**
	 * @return the conceptoDebe
	 */
	public String getConceptoDebe() {
		return conceptoDebe;
	}
	/**
	 * @param conceptoDebe the conceptoDebe to set
	 */
	public void setConceptoDebe(String conceptoDebe) {
		this.conceptoDebe = conceptoDebe;
	}
	/**
	 * @return the importeDebe
	 */
	public BigDecimal getImporteDebe() {
		return importeDebe;
	}
	/**
	 * @param importeDebe the importeDebe to set
	 */
	public void setImporteDebe(BigDecimal importeDebe) {
		this.importeDebe = importeDebe;
	}
	/**
	 * @return the auxHaber
	 */
	public String getAuxHaber() {
		return auxHaber;
	}
	/**
	 * @param auxHaber the auxHaber to set
	 */
	public void setAuxHaber(String auxHaber) {
		this.auxHaber = auxHaber;
	}
	/**
	 * @return the conceptoHaber
	 */
	public String getConceptoHaber() {
		return conceptoHaber;
	}
	/**
	 * @param conceptoHaber the conceptoHaber to set
	 */
	public void setConceptoHaber(String conceptoHaber) {
		this.conceptoHaber = conceptoHaber;
	}
	/**
	 * @return the importeHaber
	 */
	public BigDecimal getImporteHaber() {
		return importeHaber;
	}
	/**
	 * @param importeHaber the importeHaber to set
	 */
	public void setImporteHaber(BigDecimal importeHaber) {
		this.importeHaber = importeHaber;
	}
	/**
	 * @return the importePyG
	 */
	public BigDecimal getImportePyG() {
		return importePyG;
	}
	/**
	 * @param importePyG the importePyG to set
	 */
	public void setImportePyG(BigDecimal importePyG) {
		this.importePyG = importePyG;
	}
	/**
	 * @return the cuentaContableDebe
	 */
	public String getCuentaContableDebe() {
		return cuentaContableDebe;
	}
	/**
	 * @param cuentaContableDebe the cuentaContableDebe to set
	 */
	public void setCuentaContableDebe(String cuentaContableDebe) {
		this.cuentaContableDebe = cuentaContableDebe;
	}
	/**
	 * @return the cuentaContableHaber
	 */
	public String getCuentaContableHaber() {
		return cuentaContableHaber;
	}
	/**
	 * @param cuentaContableHaber the cuentaContableHaber to set
	 */
	public void setCuentaContableHaber(String cuentaContableHaber) {
		this.cuentaContableHaber = cuentaContableHaber;
	}
	/**
	 * @return the completa
	 */
	public boolean isCompleta() {
		return completa;
	}
	/**
	 * @param completa the completa to set
	 */
	public void setCompleta(boolean completa) {
		this.completa = completa;
	}
	/**
	 * @return the credit
	 */
	public boolean isCredit() {
		return credit;
	}
	/**
	 * @param credit the credit to set
	 */
	public void setCredit(boolean credit) {
		this.credit = credit;
	}
	
}
