package sibbac.business.accounting.conciliacion.enums;

/**
 * Clase con las enumeraciones para la pantalla de conciliacion.
 */
public class PantallaConciliacionEnum {
	
	/** Estado conta partidas pendientes. */
	public static enum EstadoContaPartidasPndtsEnum {

		NO_CASADA(0),
		CASADA(1);

		/** Estado de la conta partida. */
		private Integer estadoPartidaPndte;

		/**
		 * @param estadoPartidaPndte
		 */
		private EstadoContaPartidasPndtsEnum(Integer estadoPartidaPndte) {
			this.estadoPartidaPndte = estadoPartidaPndte;
		}

		/**
		 *	@return estadoPartidaPndte	 
		 */
		public Integer getEstadoPartidaPndte() {
			return this.estadoPartidaPndte;
		}
	}
	
	/** Nombres de columna para tabla TMCT0_CONTA_PARTIDAS_PNDTS. */
	public static enum NombreColumnasContaPartidasPndtsEnum {

		AUX_BANCARIO("AUX_BANCARIO"),
		COMENTARIO("COMENTARIO"),
		NUM_DOCUMENTO("NUM_DOCUMENTO"),
		TIPO("TIPO"),
		REFERENCIA("REFERENCIA");

		/** nombre de columna de la conta partida. */
		private String nombreColumna;

		/**
		 * @param nombreColumna
		 */
		private NombreColumnasContaPartidasPndtsEnum(String nombreColumna) {
			this.nombreColumna = nombreColumna;
		}

		/**
		 *	@return nombreColumna	 
		 */
		public String getNombreColumna() {
			return this.nombreColumna;
		}
	}
	
	/** Nombres de columna para tabla TMCT0_CONTA_PLANTILLAS_CONCILIA. */
	public static enum NombreColumnasContaPlantillasConciliaEnum {

		AUXILIAR("AUXILIAR");

		/** nombre de columna de la conta plantillas concilia. */
		private String nombreColumna;

		/**
		 * @param nombreColumna
		 */
		private NombreColumnasContaPlantillasConciliaEnum(String nombreColumna) {
			this.nombreColumna = nombreColumna;
		}

		/**
		 *	@return nombreColumna	 
		 */
		public String getNombreColumna() {
			return this.nombreColumna;
		}
	}
	
	/** Nombres de funcionalidad para tabla TMCT0_SIBBAC_CONSTANTES. */
	public static enum ClavesSemaforoEnum {

		ESTADO_CARGA_EXCEL("EstadoCargaExcel");

		/** nombre de clave para TMCT0_SIBBAC_CONSTANTES.CLAVE. */
		private String clave;

		/**
		 * @param clave
		 */
		private ClavesSemaforoEnum(String clave) {
			this.clave = clave;
		}

		/**
		 *	@return clave	 
		 */
		public String getClave() {
			return this.clave;
		}
	}
	
}