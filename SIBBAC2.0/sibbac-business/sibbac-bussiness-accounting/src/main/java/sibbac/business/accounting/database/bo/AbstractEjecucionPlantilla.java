package sibbac.business.accounting.database.bo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.accounting.database.dao.Tmct0ContaApunteDetalleDaoImp;
import sibbac.business.accounting.database.dao.Tmct0ContaApuntesDao;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDaoImp;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.threads.EjecucionPlantillaIndividualRunnable;
import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ActivacionContaPlantillas;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.TextosSustitucionPlantillas;

/**
 * Clase de negocios común a la ejecución de plantillas.
 * 
 * @author Neoris
 *
 */
public abstract class AbstractEjecucionPlantilla {

  protected static final Logger LOG = LoggerFactory.getLogger(AbstractEjecucionPlantilla.class);

  /**
   * Tipos de plantilla.
   */
  protected String[] tiposPlantilla;

  /* Lista para guardar todas las plantillas que no tengan un balance de 0 */
  private List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

  @Value("${sibbac.accounting.bloqueosplantillas.numThreadsPool}")
  private Integer numThreadsPool;

  @Autowired
  Tmct0ContaApuntesBo tmct0ContaApuntesBo;

  @Autowired
  Tmct0ContaPlantillasDao contaPlantillasDao;

  @Autowired
  Tmct0ContaPlantillasDaoImp contaPlantillasDaoImp;

  @Autowired
  Tmct0ContaApuntesDao contaApuntesDao;

  @Autowired
  Tmct0ContaApunteDetalleDaoImp contaApunteDetalleDaoImp;

  @Autowired
  Tmct0ContaApunteDetalleBo tmct0ContaApunteDetalleBo;

  /* Lista con los threads de inserts generados */
  public List<EjecucionPlantillaIndividualRunnable> threadsEjecucionPlantillaIndividual = new ArrayList<EjecucionPlantillaIndividualRunnable>();

  /* Error en ejecución de hilos */
  protected Throwable e;

  /* True si algún thread falló */
  public Boolean threadEjecucionPlantillaIndividualError = false;

  @Autowired
  Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  Norma43DescuadresDao norma43DescuadreDao;

  public String getFechaHora() {
    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    return formatoFecha.format(new Date());
  }

  /**
   * Ejecuta la generación de apuntes para las plantillas.
   * 
   * @throws EjecucionPlantillaException Exception lanzada.
   */
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillasTipoSubtipo(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException {

    setTiposPlantilla();

    // Se ejecuta la generación de apuntes para las plantillas especificadas.
    if (plantillas != null && !plantillas.isEmpty()) {

      // Se actualiza la fecha de auditoria de las plantillas con las que se trabajará.
      contaPlantillasDao.update(new Date(), plantillas.get(0).getTipo(), plantillas.get(0).getSubtipo());

      plantillasSinBalance.addAll(ejecutarPlantillas(plantillas));

      // Se actualiza la fecha de auditoria de las plantillas con las que se trabajó.
      // contaPlantillasDao.update(new Date(), plantillas.get(0).getTipo(), plantillas.get(0).getSubtipo());
    }

    return plantillasSinBalance;
  }

  /**
   * Realiza el bloqueo de los ALCs involucrados en la generaciÃ³n de los apuntes contables.
   * 
   * @param concepto Concepto de la query orden.
   * @param query_bloqueo Query de bloqueo.
   * @param estado_bloqueado Estado bloqueado.
   */
  public void bloquearPlantillaFechasDevengosAnulacion(List<Object[]> alcImplicadas,
                                                       String estado_bloqueado,
                                                       String campoEstado,
                                                       String codigoPlantilla) {

    // Se bloquean los ALC con los que se va a trabajar.
    contaPlantillasDaoImp.executeQueryBloqueoByFechasDevengosAnulacion(alcImplicadas, estado_bloqueado,
                                                                       campoEstado, codigoPlantilla);
  }

  /**
   * Realiza el bloqueo de los ALCs involucrados en la generaciÃ³n de los apuntes contables.
   * 
   * @param concepto Concepto de la query orden.
   * @param query_bloqueo Query de bloqueo.
   * @param estado_bloqueado Estado bloqueado.
   */
  public void bloquearPlantillaFechasDevengosFallidos(List<Object[]> alcImplicadas,
											          String estado_bloqueado,
											          String campoEstado,
											          String codigoPlantilla) {

    // Se bloquean los ALC con los que se va a trabajar.
    contaPlantillasDaoImp.executeQueryBloqueoByFechasDevengosFallidos(alcImplicadas, estado_bloqueado,
            															campoEstado, codigoPlantilla);
  }

  /**
   * Remueve de la lista de threads actuales el thread especificado.
   * 
   * @param current Thread a remover.
   * @param tieneErrores True si el thread terminó con errores.
   */
  public void shutdownRunnablePlantillaIndividual(EjecucionPlantillaIndividualRunnable current, boolean tieneErrores) {
		if (tieneErrores) {
			current.getPlantilla().setActivo(ActivacionContaPlantillas.ERROR.getActivado());
			current.getPlantilla().setAudit_date(new Date());
			this.contaPlantillasDao.saveAndFlush(current.getPlantilla());
		}
		if (!threadEjecucionPlantillaIndividualError) {
			threadEjecucionPlantillaIndividualError = tieneErrores;
		}
		threadsEjecucionPlantillaIndividual.remove(current);
  }

  /**
   * Establece el valor de los tipos de plantilla.
   */
  protected abstract void setTiposPlantilla();

  /**
   * Ejecuta la generación de apuntes para las plantillas especificadas.
   */
  protected abstract List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException;

  /**
   * Añade las plantillas desbalanceadas devueltas por el hilo
   */
  public abstract void anadirPlantillas(List<List<Tmct0ContaPlantillasDTO>> lPlantillas);

  /**
   * Se agregan errores de fichero a la plantilla.
   *
   * @param erroresFichero erroresFichero
   */
  protected void agregarErroresFichero(List<String> erroresFichero) {
    for (String errorFichero : erroresFichero) {
      Tmct0ContaPlantillasDTO tmct0ContaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      tmct0ContaPlantillasDTO.setErroresFichero(true);
      tmct0ContaPlantillasDTO.setTextoErrorFichero(errorFichero);
      // plantillasSinBalance.add(tmct0ContaPlantillasDTO);
    }
  }

  /**
   * Divide las plantillas que tengan más de una cuenta, y en tal caso las marca como dividido=true. TODO - Usar el
   * generico.
   * 
   * @param plantilla Tmct0ContaPlantillas.
   * @param object Resultado de query orden.
   * @return Lista de Tmct0ContaPlantillasDTO.
   * @throws EjecucionPlantillaException
   */
  protected List<Tmct0ContaPlantillasDTO> dividirPlantillasPorCuentasApuntesManuales(Tmct0ContaPlantillas plantilla,
                                                                                     ApuntesManualesExcelDTO amExcelDTO) throws EjecucionPlantillaException {

    // Se separa por la "," los campos que pueden traer más de un valor por plantilla.
    String[] cuentasDebe = null;
    String[] cuentasHaber = null;

    if (org.apache.commons.lang3.StringUtils.isNotBlank(amExcelDTO.getSaldoCuentaContableDebe())) {
      cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(amExcelDTO.getSaldoCuentaContableDebe(),
                                                                                ",");
    } else {
      cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",");
    }

    if (org.apache.commons.lang3.StringUtils.isNotBlank(amExcelDTO.getSaldoCuentaContableHaber())) {
      cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(amExcelDTO.getSaldoCuentaContableHaber(),
                                                                                 ",");
    } else {
      cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(), ",");
    }

    // En caso de que haya un error de inconsistencia de cuentas y auxiliares se arroja error.
    String[] auxsDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(), ",");
    String[] auxsHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(), ",");

    // En caso de que haya un error de inconsistencia de cuentas y auxiliares se arroja error.
    if (!AccountingHelper.isPlantillaCuentasAuxConsistentes(cuentasDebe, cuentasHaber, auxsDebe, auxsHaber)) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_DIVIDIR_PLANTILLAS.getValue() + " : "
                                     + ConstantesError.ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX.getValue());
      throw new EjecucionPlantillaException();
    }

    List<Tmct0ContaPlantillasDTO> plantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
    Tmct0ContaPlantillasDTO contaPlantillasDTO = null;

    for (String cuentaDebe : cuentasDebe) {
      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      contaPlantillasDTO.setCuenta_diferencias(
    		  StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
      contaPlantillasDTO.setCuenta_debe(StringHelper.quitarComillasDobles(cuentaDebe));
      contaPlantillasDTO.setTipoCuentaDebe(true);
      plantillasDTO.add(contaPlantillasDTO);
    }

    for (String cuentaHaber : cuentasHaber) {
      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      contaPlantillasDTO.setCuenta_diferencias(
    		  StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
      contaPlantillasDTO.setCuenta_haber(StringHelper.quitarComillasDobles(cuentaHaber));
      contaPlantillasDTO.setTipoCuentaDebe(false);
      plantillasDTO.add(contaPlantillasDTO);
    }
    
    return plantillasDTO;
  }

  /**
   * Ejecucion de query de cobro.
   *
   * @param plantilla plantilla
   * @return List<Object[]> List<Object[]>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  protected List<Object[]> ejecutaQueryCobro(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    Loggers.logDebugQueriesAccounting(plantilla != null ? plantilla.getCodigo() : null,
                                      SibbacEnums.ConstantesQueries.QUERY_COBRO.getValue(), null);

    String queryCobro = plantilla.getQuery_cobro()
                                 .replaceAll("<CUENTA_BANCARIA DE LA PLANTILLA>",
                                             plantilla.getCuenta_bancaria().replaceAll("\"", "'")).replace(";", "");
    return contaPlantillasDaoImp.executeQuery(queryCobro, plantilla.getCodigo());
  }
  
  
  /**
   * Ejecucion de query de cobro Apuntes Manuales.
   *
   * @param plantilla plantilla
   * @return List<Object[]> List<Object[]>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  protected List<Object[]> ejecutaQueryCobroAM(String query, String cuentaBancaria, String codigoPlantilla) throws EjecucionPlantillaException {

    Loggers.logDebugQueriesAccounting(codigoPlantilla,SibbacEnums.ConstantesQueries.QUERY_COBRO.getValue(), null);
    String queryCobro = query.replaceAll("<CUENTA_BANCARIA DE LA PLANTILLA>",cuentaBancaria.replaceAll("\"", "'")).replace(";", "");
    
    return contaPlantillasDaoImp.executeQuery(queryCobro, codigoPlantilla);
  }

  public List<Date> getListFechasEjecucion(String query) {

    List<Date> lResultado = new ArrayList<Date>();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    try {

      if (query.indexOf(TextosSustitucionPlantillas.ENTRE.getTexto()) > -1
          || query.indexOf(TextosSustitucionPlantillas.DESDE_INICIO_ANIO.getTexto()) > -1
          || query.indexOf(TextosSustitucionPlantillas.DIAS_A_EJECUTAR.getTexto()) > -1) {

        Date fechaFin = new Date();
        Date fechaInicio = null;
        if (query.indexOf(TextosSustitucionPlantillas.ENTRE.getTexto()) > -1) {

          int posFechaInicio = query.lastIndexOf(TextosSustitucionPlantillas.ENTRE.getTexto()) + 6;
          int posFechaFin = query.lastIndexOf(TextosSustitucionPlantillas.HASTA.getTexto()) + 6;
          String textoFechaInicio = query.substring(posFechaInicio, posFechaInicio + 10);
          String textoFechaFin = query.substring(posFechaFin, posFechaFin + 10);
          fechaInicio = formatter.parse(textoFechaInicio);
          fechaFin = formatter.parse(textoFechaFin);

        } else if (query.indexOf(TextosSustitucionPlantillas.DESDE_INICIO_ANIO.getTexto()) > -1) {

          Calendar cal = new GregorianCalendar();
          cal.setTime(fechaFin);
          int anio = cal.get(Calendar.YEAR);
          fechaInicio = formatter.parse(anio + "-01-01");

        } else if (query.indexOf(TextosSustitucionPlantillas.DIAS_A_EJECUTAR.getTexto()) > -1) {

          int posDiasInicial = query.lastIndexOf(TextosSustitucionPlantillas.DIAS_A_EJECUTAR.getTexto()) + 16;
          int posDiasFinal = posDiasInicial;
          while (!query.substring(posDiasFinal, posDiasFinal + 1).equals(" ")) {
            posDiasFinal++;
          }
          String textoDias = query.substring(posDiasInicial, posDiasFinal);
          int dias = Integer.parseInt(textoDias);
          Calendar c = Calendar.getInstance();
          c.setTime(new Date());
          c.add(Calendar.DATE, -dias);
          fechaInicio = c.getTime();

        }

        Date fecha_siguiente = fechaInicio;
        Calendar calendar = Calendar.getInstance();
        lResultado.add(fecha_siguiente);
        while (fecha_siguiente.before(fechaFin)) {
          calendar.setTime(fecha_siguiente);
          calendar.add(Calendar.DAY_OF_YEAR, 1);
          fecha_siguiente = calendar.getTime();
          lResultado.add(fecha_siguiente);
        }

      }

    } catch (ParseException e) {
      LOG.error("Error de parseo.", e);
    }

    return lResultado;
  }

  public abstract List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException;

  private String getDateLogInfo() {
    return " [" + getFechaHora() + "] ";
  }

  private String getBaseLogInfo() {
    return "[" + this.getClass() + "] ";
  }

  /**
   * Logea mensajes con fecha y clase de ejecución
   * 
   * @param m
   */
  public void log(String m) {
    LOG.trace(getBaseLogInfo() + m + getDateLogInfo());
  }

  /**
   * getPlantillasSinBalance.
   * 
   * @return plantillasSinBalance plantillasSinBalance
   */
  public List<List<Tmct0ContaPlantillasDTO>> getPlantillasSinBalance() {
    return this.plantillasSinBalance != null ? new ArrayList<List<Tmct0ContaPlantillasDTO>>()
                                            : this.plantillasSinBalance;
  }

  /**
   * setPlantillasSinBalance.
   * 
   * @param plantillasSinBalance plantillasSinBalance
   */
  public void setPlantillasSinBalance(List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance) {
    this.plantillasSinBalance = plantillasSinBalance;
  }

}
