package sibbac.business.accounting.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidad para la gestion de TMCT0_CONTA_APUNTES
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CONTA_APUNTES")
public class Tmct0ContaApuntes {

	/** Campos de la entidad */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	@Column(name = "IDMOVIMIENTO", length = 8)
	private Integer idmovimiento;

	@Column(name = "COD_PLANTILLA", length = 20, nullable = false)
	private String cod_plantilla;

	@Column(name = "TIPO_TRANSACC", length = 1, nullable = false)
	private char tipo_transacc;

	@Column(name = "COD_COMP", length = 2, nullable = false)
	private String cod_comp;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_COMPROBANTE", length = 4)
	private Date fecha_comprobante;

	@Column(name = "PERIODO_COMPROBANTE", length = 2, nullable = false)
	private String periodo_comprobante;

	@Column(name = "TIPO_COMPROBANTE", length = 2, nullable = false)
	private String tipo_comprobante;

	@Column(name = "NUM_COMPROBANTE", length = 4, nullable = false)
	private Integer num_comprobante;

	@Column(name = "CUENTA_CONTABLE", length = 15, nullable = false)
	private String cuenta_contable;

	@Column(name = "AUX_CONTABLE", length = 10, nullable = false)
	private String aux_contable;

	@Column(name = "CONCEPTO", length = 100, nullable = false)
	private String concepto;

	@Column(name = "IMPORTE", nullable = false, precision = 18, scale = 4)
	private BigDecimal importe = BigDecimal.ZERO;

	@Column(name = "TIPO_APUNTE", length = 1, nullable = false)
	private char tipo_apunte;

	@Column(name = "FICHERO", length = 50, nullable = false)
	private String fichero;

	@Column(name = "GENERADO_FICHERO", length = 2, nullable = false)
	private Integer generado_fichero;

	@Column(name = "AUDIT_DATE")
	private Date audit_date;

	@Column(name = "AUDIT_USER", length = 255)
	private String audit_user;

	public Tmct0ContaApuntes() {
	};

	public Tmct0ContaApuntes(Integer id, Integer idmovimiento, String cod_plantilla, char tipo_transacc,
			String cod_comp, Date fecha_comprobante, String periodo_comprobante, String tipo_comprobante,
			Integer num_comprobante, String cuenta_contable, String aux_contable, String concepto, BigDecimal importe,
			char tipo_apunte, String fichero, Integer generado_fichero, Date audit_date, String audit_user) {
		this.id = id;
		this.idmovimiento = idmovimiento;
		this.cod_plantilla = cod_plantilla;
		this.tipo_transacc = tipo_transacc;
		this.cod_comp = cod_comp;
		this.fecha_comprobante = fecha_comprobante;
		this.periodo_comprobante = periodo_comprobante;
		this.tipo_comprobante = tipo_comprobante;
		this.num_comprobante = num_comprobante;
		this.cuenta_contable = cuenta_contable;
		this.aux_contable = aux_contable;
		this.concepto = concepto;
		this.importe = importe;
		this.tipo_apunte = tipo_apunte;
		this.fichero = fichero;
		this.generado_fichero = generado_fichero;
		this.audit_date = audit_date;
		this.audit_user = audit_user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdmovimiento() {
		return idmovimiento;
	}

	public void setIdmovimiento(Integer idmovimiento) {
		this.idmovimiento = idmovimiento;
	}

	public String getCod_plantilla() {
		return cod_plantilla;
	}

	public void setCod_plantilla(String cod_plantilla) {
		this.cod_plantilla = cod_plantilla;
	}

	public char getTipo_transacc() {
		return tipo_transacc;
	}

	public void setTipo_transacc(char tipo_transacc) {
		this.tipo_transacc = tipo_transacc;
	}

	public String getCod_comp() {
		return cod_comp;
	}

	public void setCod_comp(String cod_comp) {
		this.cod_comp = cod_comp;
	}

	public Date getFecha_comprobante() {
		return fecha_comprobante;
	}

	public void setFecha_comprobante(Date fecha_comprobante) {
		this.fecha_comprobante = fecha_comprobante;
	}

	public String getPeriodo_comprobante() {
		return periodo_comprobante;
	}

	public void setPeriodo_comprobante(String periodo_comprobante) {
		this.periodo_comprobante = periodo_comprobante;
	}

	public String getTipo_comprobante() {
		return tipo_comprobante;
	}

	public void setTipo_comprobante(String tipo_comprobante) {
		this.tipo_comprobante = tipo_comprobante;
	}

	public Integer getNum_comprobante() {
		return num_comprobante;
	}

	public void setNum_comprobante(Integer num_comprobante) {
		this.num_comprobante = num_comprobante;
	}

	public String getCuenta_contable() {
		return cuenta_contable;
	}

	public void setCuenta_contable(String cuenta_contable) {
		this.cuenta_contable = cuenta_contable;
	}

	public String getAux_contable() {
		return aux_contable;
	}

	public void setAux_contable(String aux_contable) {
		this.aux_contable = aux_contable;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public char getTipo_apunte() {
		return tipo_apunte;
	}

	public void setTipo_apunte(char tipo_apunte) {
		this.tipo_apunte = tipo_apunte;
	}

	public Integer getGenerado_fichero() {
		return generado_fichero;
	}

	public void setGenerado_fichero(Integer generado_fichero) {
		this.generado_fichero = generado_fichero;
	}

	public String getFichero() {
		return fichero;
	}

	public void setFichero(String fichero) {
		this.fichero = fichero;
	}

	public Date getAudit_date() {
		return audit_date;
	}

	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	}

	public String getAudit_user() {
		return audit_user;
	}

	public void setAudit_user(String audit_user) {
		this.audit_user = audit_user;
	}

	@Override
	public String toString() {
		return "Tmct0ContaApuntes [id=" + id + ", idmovimiento=" + idmovimiento + ", cod_plantilla=" + cod_plantilla
				+ ", tipo_transacc=" + tipo_transacc + ", cod_comp=" + cod_comp + ", fecha_comprobante="
				+ fecha_comprobante + ", periodo_comprobante=" + periodo_comprobante + ", tipo_comprobante="
				+ tipo_comprobante + ", num_comprobante=" + num_comprobante + ", cuenta_contable=" + cuenta_contable
				+ ", aux_contable=" + aux_contable + ", concepto=" + concepto + ", importe=" + importe
				+ ", tipo_apunte=" + tipo_apunte + ", fichero=" + fichero + ", generado_fichero=" + generado_fichero
				+ ", audit_date=" + audit_date + ", audit_user=" + audit_user + "]";
	}

}
