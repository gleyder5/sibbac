package sibbac.business.accounting.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_CONTA_VALIDATION.
 */
@Entity
@Table(name = "TMCT0_CONTA_VALIDATION")
public class Tmct0AccountingValidation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8657828008148744250L;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0AccountingValidation() {
		super();
	}
	
	@Id
	@Column(name = "ID", nullable = false, length = 8)
	private Long id;
	
	@Column(name = "TIPO", nullable = false, length = 50)
	private String tipo;
	
	@Column(name = "CLAVE", nullable = false, length = 50)
	private String clave;
	
	@Column(name = "VALOR", nullable = false, length = 250)
	private String valor;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
