package sibbac.business.accounting.conciliacion.rest;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConciliacionAuditDaoImp;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaOurDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaConciliacionAuditDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaPartidasCasadasDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0FiltroContaConciliacionAuditDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaConciliacionAuditAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaOurAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaPartidasCasadasAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceHistoricoPartidasCasadas implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SIBBACServiceHistoricoPartidasCasadas.class);

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	@Autowired
	Tmct0ContaConciliacionAuditAssembler tmct0ContaConciliacionAuditAssembler;

	@Autowired
	Tmct0ContaPartidasCasadasAssembler tmct0ContaPartidasCasadasAssembler;

	@Autowired
	Tmct0ContaConciliacionAuditDaoImp tmct0ContaConciliacionAuditDaoImp;

	@Autowired
	Tmct0ContaOurAssembler tmct0ContaOurAssembler;

	@Autowired
	Tmct0ContaPartidasCasadasDAO contaPartidasCasadasDAO;

	@Autowired
	Tmct0ContaOurDAO tmct0ContaOurDAO;

	@Autowired
	Tmct0alcDao tmct0alcDao;

	private enum ComandoHistoricoPartidasCasadas {
		CONSULTAR_HISTORICO("consultarHistorico"), 
		CONSULTAR_PARTIDAS_TLM("consultarPartidasTLM"), 
		CONSULTAR_DESGLOSES_SIBBAC("consultarDesglosesSIBBAC"), 
		EXPORTAR_EXCEL("exportarXLS"),
		SIN_COMANDO("");

		/** The command. */
		private String command;

		private ComandoHistoricoPartidasCasadas(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * Process.
	 *
	 * @param webRequest
	 *            the web request
	 * @return the web response
	 * @throws SIBBACBusinessException
	 *             the SIBBAC business exception
	 */
	@Override
	public WebResponse process(WebRequest webRequest)
			throws SIBBACBusinessException {
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandoHistoricoPartidasCasadas command = getCommand(webRequest
				.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceHistoricoPartidasCasadas");
			switch (command) {
			case CONSULTAR_HISTORICO:
				result.setResultados(consultarHistoricoPartidas(paramsObjects));
				break;
			case CONSULTAR_PARTIDAS_TLM:
				result.setResultados(consultarPartidasTLM(paramsObjects));
				break;
			case CONSULTAR_DESGLOSES_SIBBAC:
				result.setResultados(consultarDesglosesSIBBAC(paramsObjects));
				break;
			case EXPORTAR_EXCEL:
				result.setResultados(exportarAExcel(paramsObjects));
			default:
				result.setError("An action must be set. Valid actions are: " + command);
			}

		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

		LOG.debug("Salgo del servicio SIBBACServiceHistoricoPartidasCasadas");
		return result;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> exportarAExcel(Map<String, Object> paramsObjects) {
		LOG.info("INICIO - SERVICIO - exportarAExcel");
		Map<String, Object> result = new HashMap<String, Object>();
		List<Tmct0ContaPartidasCasadasDTO> listaTmct0ContaConciliacionAuditDTO = new ArrayList<Tmct0ContaPartidasCasadasDTO>();
		List<Tmct0ContaOurDTO> listaTmct0ContaOurDTO = new ArrayList<Tmct0ContaOurDTO>();

		try {
			LOG.debug("LLAMA A crearInformePorHojas SIMPLE");

			Map<String, Object> mapPartidasTLM = consultarPartidasTLM(paramsObjects);
			listaTmct0ContaConciliacionAuditDTO = (List<Tmct0ContaPartidasCasadasDTO>) mapPartidasTLM.get("partidasCasadas");
			Map<String, Object> mapPartidasSIBBAC = consultarDesglosesSIBBAC(paramsObjects);
			listaTmct0ContaOurDTO = (List<Tmct0ContaOurDTO>) mapPartidasSIBBAC.get("desglosesSIBBAC");

			if ((null != listaTmct0ContaConciliacionAuditDTO && !listaTmct0ContaConciliacionAuditDTO
					.isEmpty())
					|| (null != listaTmct0ContaOurDTO && !listaTmct0ContaOurDTO
					.isEmpty())) {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				XSSFWorkbook wb = AccountingHelper.generarExcelPartidasCasadasYOur(listaTmct0ContaConciliacionAuditDTO, listaTmct0ContaOurDTO);
				wb.write(bos);

				result.put("excelPartidasCasadas", bos.toByteArray());
			}

			LOG.debug("FIN crearInformePorHojas SIMPLE");
		} catch (Exception e) {
			LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		return result;
	}

	private Map<String, Object> consultarDesglosesSIBBAC(Map<String, Object> paramsObjects) {
		LOG.info("INICIO - SERVICIO - consultarDesglosesSIBBAC");
		Map<String, Object> result = new HashMap<String, Object>();
		List<Tmct0ContaOur> listaTmct0ContaOur = new ArrayList<Tmct0ContaOur>();
		List<Tmct0ContaOurDTO> listaTmct0ContaOurDTO = new ArrayList<Tmct0ContaOurDTO>();

		try {
			BigInteger idHistorico = new BigInteger(String.valueOf(paramsObjects.get("id")));
			listaTmct0ContaOur = tmct0ContaOurDAO.findByIdConciliacionAudit(idHistorico);
			if (null != listaTmct0ContaOur) {
				for (Tmct0ContaOur tmct0ContaOur : listaTmct0ContaOur) {
					listaTmct0ContaOurDTO.add(tmct0ContaOurAssembler.getTmct0ContaOurDTO(tmct0ContaOur));
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarDesglosesSIBBAC -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("desglosesSIBBAC", listaTmct0ContaOurDTO);

		return result;
	}

	private Map<String, Object> consultarPartidasTLM(Map<String, Object> paramsObjects) {
		LOG.info("INICIO - SERVICIO - consultarPartidasTLM");
		Map<String, Object> result = new HashMap<String, Object>();
		List<Tmct0ContaPartidasCasadasDTO> listaTmct0ContaConciliacionAuditDTO = new ArrayList<Tmct0ContaPartidasCasadasDTO>();
		List<Tmct0ContaPartidasCasadas> listaTmct0ContaPartidasCasadas = new ArrayList<Tmct0ContaPartidasCasadas>();

		try {
			BigInteger idHistorico = new BigInteger(String.valueOf(paramsObjects.get("id")));
			listaTmct0ContaPartidasCasadas = this.contaPartidasCasadasDAO.findByIdConciliacionAudit(idHistorico);

			if (null != listaTmct0ContaPartidasCasadas) {
				for (Tmct0ContaPartidasCasadas tmct0ContaPartidasCasadas : listaTmct0ContaPartidasCasadas) {
					listaTmct0ContaConciliacionAuditDTO.add(tmct0ContaPartidasCasadasAssembler.getTmct0ContaPartidasCasadasDTO(tmct0ContaPartidasCasadas));
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarPartidasTLM -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("partidasCasadas", listaTmct0ContaConciliacionAuditDTO);
		return result;
	}

	private Map<String, Object> consultarHistoricoPartidas(Map<String, Object> paramsObjects) {
		LOG.info("INICIO - SERVICIO - consultarHistoricoPartidas");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Tmct0ContaConciliacionAuditDTO> listaTmct0ContaConciliacionAuditDTO = new ArrayList<Tmct0ContaConciliacionAuditDTO>();
		List<Tmct0ContaConciliacionAudit> listaTmct0ContaConciliacionAudit = new ArrayList<Tmct0ContaConciliacionAudit>();
		Tmct0FiltroContaConciliacionAuditDTO filtro = null;

		try {
			filtro = tmct0ContaConciliacionAuditAssembler.getTmctFiltro0ContaConciliacionAuditDTO(paramsObjects);
			listaTmct0ContaConciliacionAudit = tmct0ContaConciliacionAuditDaoImp.consultarHistoricoPartidasCasadas(filtro);

			if (null != listaTmct0ContaConciliacionAudit) {
				for (Tmct0ContaConciliacionAudit tmct0ContaConciliacionAudit : listaTmct0ContaConciliacionAudit) {
					listaTmct0ContaConciliacionAuditDTO.add(this.tmct0ContaConciliacionAuditAssembler.getTmct0ContaConciliacionAuditDTO(tmct0ContaConciliacionAudit));
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarHistoricoPartidas -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listHistorico", listaTmct0ContaConciliacionAuditDTO);

		return result;
	}

	private ComandoHistoricoPartidasCasadas getCommand(String command) {
		ComandoHistoricoPartidasCasadas[] commands = ComandoHistoricoPartidasCasadas
				.values();
		ComandoHistoricoPartidasCasadas result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandoHistoricoPartidasCasadas.SIN_COMANDO;
		}
		LOG.debug(new StringBuffer("Se selecciona la accion: ").append(
				result.getCommand()).toString());
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

}
