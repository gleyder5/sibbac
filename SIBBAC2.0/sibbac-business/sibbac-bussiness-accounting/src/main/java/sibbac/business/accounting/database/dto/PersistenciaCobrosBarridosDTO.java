package sibbac.business.accounting.database.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.database.model.Tmct0Norma43Detalle;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;

/**
 * Data Transfer Object para persistencia en cobros barridos.
 */
public class PersistenciaCobrosBarridosDTO {
	

	private Set<Integer> idsMovimiento; 
	private Tmct0ContaPlantillas tmct0ContaPlantilla; 
	private Norma43Movimiento norma43Movimiento;
	private List<Norma43Descuadres> listaNorma43Descuadres;
	private List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle;
	private List<ContaApuntesCobrosBarridosDTO> listaContaApuntesCobrosBarridosDTO;
	private List<Tmct0ContaApuntes> listaApuntesDiferencia;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public PersistenciaCobrosBarridosDTO() {
		this.idsMovimiento = new HashSet<Integer>();
		this.listaNorma43Descuadres = new ArrayList<Norma43Descuadres>();
		this.listaTmct0Norma43Detalle = new ArrayList<Tmct0Norma43Detalle>();
		this.listaContaApuntesCobrosBarridosDTO = new ArrayList<ContaApuntesCobrosBarridosDTO>();
		this.listaApuntesDiferencia = new ArrayList<Tmct0ContaApuntes>();
	}

	/**
	 *	Constructor con argumentos. 
	 */
	public PersistenciaCobrosBarridosDTO(Set<Integer> idsMovimiento,
			Tmct0ContaPlantillas tmct0ContaPlantilla,
			Norma43Movimiento norma43Movimiento,
			List<Norma43Descuadres> listaNorma43Descuadres,
			List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle,
			List<ContaApuntesCobrosBarridosDTO> listaContaApuntesCobrosBarridosDTO,
			List<Tmct0ContaApuntes> listaApuntesDiferencia) {
		super();
		this.idsMovimiento = idsMovimiento;
		this.tmct0ContaPlantilla = tmct0ContaPlantilla;
		this.norma43Movimiento = norma43Movimiento;
		this.listaNorma43Descuadres = listaNorma43Descuadres;
		this.listaTmct0Norma43Detalle = listaTmct0Norma43Detalle;
		this.listaContaApuntesCobrosBarridosDTO = listaContaApuntesCobrosBarridosDTO;
		this.listaApuntesDiferencia = listaApuntesDiferencia;
	}

	public Set<Integer> getIdsMovimiento() {
		return this.idsMovimiento;
	}

	public void setIdsMovimiento(Set<Integer> idsMovimiento) {
		this.idsMovimiento = idsMovimiento;
	}

	public Tmct0ContaPlantillas getTmct0ContaPlantilla() {
		return this.tmct0ContaPlantilla;
	}

	public void setTmct0ContaPlantilla(Tmct0ContaPlantillas tmct0ContaPlantilla) {
		this.tmct0ContaPlantilla = tmct0ContaPlantilla;
	}

	public Norma43Movimiento getNorma43Movimiento() {
		return this.norma43Movimiento;
	}

	public void setNorma43Movimiento(Norma43Movimiento norma43Movimiento) {
		this.norma43Movimiento = norma43Movimiento;
	}

	public List<Norma43Descuadres> getListaNorma43Descuadres() {
		return this.listaNorma43Descuadres;
	}

	public void setListaNorma43Descuadres(
			List<Norma43Descuadres> listaNorma43Descuadres) {
		this.listaNorma43Descuadres = listaNorma43Descuadres;
	}

	public List<Tmct0Norma43Detalle> getListaTmct0Norma43Detalle() {
		return this.listaTmct0Norma43Detalle;
	}

	public void setListaTmct0Norma43Detalle(
			List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle) {
		this.listaTmct0Norma43Detalle = listaTmct0Norma43Detalle;
	}

	public List<ContaApuntesCobrosBarridosDTO> getListaContaApuntesCobrosBarridosDTO() {
		return this.listaContaApuntesCobrosBarridosDTO;
	}

	public void setListaContaApuntesCobrosBarridosDTO(
			List<ContaApuntesCobrosBarridosDTO> listaContaApuntesCobrosBarridosDTO) {
		this.listaContaApuntesCobrosBarridosDTO = listaContaApuntesCobrosBarridosDTO;
	}

	public List<Tmct0ContaApuntes> getListaApuntesDiferencia() {
		return this.listaApuntesDiferencia;
	}

	public void setListaApuntesDiferencia(
			List<Tmct0ContaApuntes> listaApuntesDiferencia) {
		this.listaApuntesDiferencia = listaApuntesDiferencia;
	}
	
}