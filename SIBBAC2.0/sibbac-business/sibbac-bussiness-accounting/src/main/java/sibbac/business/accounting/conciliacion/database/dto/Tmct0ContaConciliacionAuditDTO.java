package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigInteger;
import java.util.Date;

/**
 * DTO para la gestion de TMCT0_CONTA_CONCILIACION_AUDIT.
 * @author lucio.vilar
 *
 */
public class Tmct0ContaConciliacionAuditDTO {

	/**
	 * id
	 */
	private BigInteger id;
	
	/**
	 * auditDate
	 */
	private Date fechaAudioria;
	
	/**
	 * user
	 */
	private String usuario;
	
	/**
	 * origen
	 */
	private String origen;

	public Tmct0ContaConciliacionAuditDTO() {
		
	}
	
	public Tmct0ContaConciliacionAuditDTO(BigInteger anId, Date date, String user, String anOrigen) {
		this.id = anId;
		this.fechaAudioria = date;
		this.usuario = user;
		this.origen = anOrigen;
	}
	
	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fechaAudioria
	 */
	public Date getFechaAudioria() {
		return fechaAudioria;
	}

	/**
	 * @param fechaAudioria the fechaAudioria to set
	 */
	public void setFechaAudioria(Date fechaAudioria) {
		this.fechaAudioria = fechaAudioria;
	}

}
