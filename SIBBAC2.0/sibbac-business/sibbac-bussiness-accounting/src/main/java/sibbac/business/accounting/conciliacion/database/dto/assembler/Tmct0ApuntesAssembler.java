package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.GrillaApunteDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;

/**
 * @author lucio.vilar
 *
 */
@Service
public class Tmct0ApuntesAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ApuntesAssembler.class);

	public GrillaApunteDTO getGrillaApunteDTO(Map<String, Object> mapApunte) {
		LOG.info("INICIO - ASSEMBLER - getGrillaApunteDTO");

		GrillaApunteDTO apunteDTO = new GrillaApunteDTO();
		try {
			if(StringUtils.isNotBlank((String)mapApunte.get("auxiliar"))){
				apunteDTO.setAuxiliar((String) mapApunte.get("auxiliar"));
			}else{
				apunteDTO.setAuxiliar("");
			}
			apunteDTO.setConcepto((String) mapApunte.get("concepto"));
			apunteDTO.setCuentaContable((String) mapApunte.get("cuenta"));
			if (mapApunte.get("importe") instanceof Double) {
				apunteDTO.setImporte(BigDecimal.valueOf(Math.round((Double) mapApunte.get("importe") * 100.0) / 100.0));
			} else {
				apunteDTO.setImporte(BigDecimal.valueOf((Integer) mapApunte.get("importe")));
			}
		} catch (Exception e) {
			LOG.error("Error metodo getGrillaApunteDTO -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FIN - ASSEMBLER - getGrillaApunteDTO");

		return apunteDTO;
	}

	public GrillaTLMDTO getGrillaTLMDTO(Map<String, Object> mapTlm) {
		GrillaTLMDTO tlmDTO = new GrillaTLMDTO();

		try {
			tlmDTO.setId(new Long((Integer) mapTlm.get("id")));
			tlmDTO.setAntiguedad((String) mapTlm.get("antiguedad"));
			tlmDTO.setTipoDeMovimiento((String) mapTlm.get("tipoDeMovimiento"));
			tlmDTO.setConceptoMovimiento((String) mapTlm.get("conceptoMovimiento"));
			tlmDTO.setNumeroDocumento((String) mapTlm.get("numeroDocumento"));
			tlmDTO.setComentario((String) mapTlm.get("comentario"));
			if (mapTlm.get("importe") instanceof Double) {
				tlmDTO.setImporte(BigDecimal.valueOf((Double) mapTlm.get("importe")));
			} else {
				tlmDTO.setImporte(BigDecimal.valueOf((Integer) mapTlm.get("importe")));
			}
			tlmDTO.setAuxiliarBancario((String) mapTlm.get("auxiliarBancario"));
		} catch (Exception e) {
			LOG.error("Error metodo getGrillaTLMDTO -  " + e.getMessage(), e);
			throw (e);
		}

		return tlmDTO;
	}

	public List<GrillaTLMDTO> getGrillaTLMDTOs(List<Tmct0ContaPartidasCasadas> listaPartidasCasadas) throws Exception {
		List<GrillaTLMDTO> result = new ArrayList<GrillaTLMDTO>();

		try {

			for (Tmct0ContaPartidasCasadas partidasCasadas : listaPartidasCasadas) {
				GrillaTLMDTO tlmDTO = new GrillaTLMDTO();

				tlmDTO.setAntiguedad(partidasCasadas.getAntiguedad());
				tlmDTO.setTipoDeMovimiento(partidasCasadas.getTipo());
//				if (null != partidasCasadas.getApuntesAudit() && null != partidasCasadas.getApuntesAudit().getConcepto()) {
//					tlmDTO.setConceptoMovimiento(partidasCasadas.getApuntesAudit().getConcepto().getConcepto());
//				}
				tlmDTO.setConceptoMovimiento(partidasCasadas.getReferencia());
				tlmDTO.setNumeroDocumento(partidasCasadas.getNumDocumento());
				tlmDTO.setComentario(partidasCasadas.getComentario());
				//TODO: este campo no debería llegar como un entero
				tlmDTO.setImporte(partidasCasadas.getImporte());
				tlmDTO.setAuxiliarBancario(partidasCasadas.getAuxBancario());

				result.add(tlmDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaOurApuntesDTO -  " + e.getMessage(), e);
			throw (e);
		}

		return result;
	}
}
