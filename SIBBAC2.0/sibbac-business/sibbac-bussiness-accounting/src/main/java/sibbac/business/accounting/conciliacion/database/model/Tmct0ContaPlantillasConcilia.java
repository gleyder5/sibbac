package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_CONTA_PLANTILLAS_CONCILIA.
 */
@Entity
@Table(name = "TMCT0_CONTA_PLANTILLAS_CONCILIA")
public class Tmct0ContaPlantillasConcilia {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	@Column(name = "CODIGO", length = 50, nullable = true)
	private String codigo;
	
	@Column(name = "TIPO", length = 50, nullable = true)
	private String tipo;
	
	@Column(name = "SUBTIPO", length = 50, nullable = true)
	private String subtipo;
	
	@Column(name = "AUXILIAR", length = 100, nullable = true)
	private String auxiliar;
	
	@Column(name = "TOLERANCE", length = 8, nullable = true)
	private BigInteger tolerance;
	
	@Column(name = "SELECT_QUERY", length = 5000, nullable = true)
	private String selectQuery;
	
	@Column(name = "FROM_QUERY", length = 5000, nullable = true)
	private String fromQuery;

	@Column(name = "WHERE_QUERY", length = 5000, nullable = true)
	private String whereQuery;
	
	@Column(name = "HAVING_QUERY", length = 5000, nullable = true)
	private String havingQuery;
	
	@Column(name = "GROUPBY", length = 5000, nullable = true)
	private String groupBy;
	
	@Column(name = "ORDERBY", length = 5000, nullable = true)
	private String orderBy;
	
	@Column(name = "IMPORTE", length = 100, nullable = true)
	private String importe;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaPlantillasConcilia() {
		super();
	}

	/**
	 * @param id
	 * @param codigo
	 * @param tipo
	 * @param subtipo
	 * @param auxiliar
	 * @param tolerance
	 * @param selectQuery
	 * @param fromQuery
	 * @param whereQuery
	 * @param havingQuery
	 * @param groupBy
	 * @param orderBy
	 * @param importe
	 */
	public Tmct0ContaPlantillasConcilia(BigInteger id, String codigo,
			String tipo, String subtipo, String auxiliar, BigInteger tolerance,
			String selectQuery, String fromQuery, String whereQuery,
			String havingQuery, String groupBy, String orderBy, String importe) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.tipo = tipo;
		this.subtipo = subtipo;
		this.auxiliar = auxiliar;
		this.tolerance = tolerance;
		this.selectQuery = selectQuery;
		this.fromQuery = fromQuery;
		this.whereQuery = whereQuery;
		this.havingQuery = havingQuery;
		this.groupBy = groupBy;
		this.orderBy = orderBy;
		this.importe=importe;
	}

	/**
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the subtipo
	 */
	public String getSubtipo() {
		return subtipo;
	}

	/**
	 * @param subtipo the subtipo to set
	 */
	public void setSubtipo(String subtipo) {
		this.subtipo = subtipo;
	}

	/**
	 * @return the auxiliar
	 */
	public String getAuxiliar() {
		return auxiliar;
	}

	/**
	 * @param auxiliar the auxiliar to set
	 */
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}

	/**
	 * @return the tolerance
	 */
	public BigInteger getTolerance() {
		return tolerance;
	}

	/**
	 * @param tolerance the tolerance to set
	 */
	public void setTolerance(BigInteger tolerance) {
		this.tolerance = tolerance;
	}

	/**
	 * @return the selectQuery
	 */
	public String getSelectQuery() {
		return selectQuery;
	}

	/**
	 * @param selectQuery the selectQuery to set
	 */
	public void setSelectQuery(String selectQuery) {
		this.selectQuery = selectQuery;
	}

	/**
	 * @return the fromQuery
	 */
	public String getFromQuery() {
		return fromQuery;
	}

	/**
	 * @param fromQuery the fromQuery to set
	 */
	public void setFromQuery(String fromQuery) {
		this.fromQuery = fromQuery;
	}

	/**
	 * @return the whereQuery
	 */
	public String getWhereQuery() {
		return whereQuery;
	}

	/**
	 * @param whereQuery the whereQuery to set
	 */
	public void setWhereQuery(String whereQuery) {
		this.whereQuery = whereQuery;
	}

	/**
	 * @return the havingQuery
	 */
	public String getHavingQuery() {
		return havingQuery;
	}

	/**
	 * @param havingQuery the havingQuery to set
	 */
	public void setHavingQuery(String havingQuery) {
		this.havingQuery = havingQuery;
	}

	/**
	 * @return the groupBy
	 */
	public String getGroupBy() {
		return groupBy;
	}

	/**
	 * @param groupBy the groupBy to set
	 */
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	/**
	 * @return the orderBy
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * @param orderBy the orderBy to set
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	
}
