package sibbac.business.accounting.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;

/**
 * Data access object de conta plantilla.
 * 
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ContaPlantillasDao extends JpaRepository<Tmct0ContaPlantillas, Integer> {
  
  
  @Query("SELECT MAX(o.num_comprobante) FROM Tmct0ContaPlantillas o WHERE o.fichero = :fichero AND o.activo = 1")
  Integer maxNumComprobanteByFicheroAndActivo(@Param("fichero") String fichero);

  @Query("SELECT plant FROM Tmct0ContaPlantillas plant WHERE plant.activo = 1 "
         + "AND plant.fichero_cobro = :fichero_cobro")
  public Tmct0ContaPlantillas findActivoByFicheroCobro(@Param("fichero_cobro") String fichero_cobro);

  @Query("SELECT plant FROM Tmct0ContaPlantillas plant WHERE plant.codigo = :codigo AND plant.activo = :activo")
  public Tmct0ContaPlantillas findByActivoAndCodigo(@Param("codigo") String codigo, @Param("activo") Integer activo);

  @Query("SELECT plant FROM Tmct0ContaPlantillas plant WHERE plant.codigo = :codigo")
  public Tmct0ContaPlantillas findByCodigo(@Param("codigo") String codigo);

  @Query("SELECT o FROM Tmct0ContaPlantillas o WHERE o.activo = 1 ORDER BY o.id")
  List<Tmct0ContaPlantillas> getAll();
  
  @Query("SELECT o FROM Tmct0ContaPlantillas o WHERE o.activo = 1 AND codigo like 'THEIR_%'")
  List<Tmct0ContaPlantillas> getAllTheir();  
  
  @Query("SELECT o FROM Tmct0ContaPlantillas o WHERE o.activo = :activo ORDER BY o.id")
  List<Tmct0ContaPlantillas> findByActivo(@Param("activo") Integer activo);

  @Query("SELECT o FROM Tmct0ContaPlantillas o WHERE o.activo = 1 AND o.tipo=:tipo AND o.subtipo=:subtipo ORDER BY o.id")
  List<Tmct0ContaPlantillas> getPlantillasPorTipoSubtipo(@Param("tipo") String tipo, @Param("subtipo") String subtipo);

  @Modifying
  @Transactional
  @Query("UPDATE Tmct0ContaPlantillas o SET o.audit_date = :auditDate WHERE o.activo = 1 AND o.tipo=:tipo AND o.subtipo=:subtipo")
  Integer update(@Param("auditDate") Date auditDate, @Param("tipo") String tipo, @Param("subtipo") String subtipo);

  @Query("SELECT plant FROM Tmct0ContaPlantillas plant "
         + "WHERE plant.subtipo = :subtipo AND plant.fichero_cobro = :fichero_cobro")
  public Tmct0ContaPlantillas findBySubtipoAndFicheroCobro(@Param("subtipo") String subtipo,
                                                           @Param("fichero_cobro") String fichero_cobro);

  @Modifying
  @Transactional
  @Query("UPDATE Tmct0ContaPlantillas o SET o.num_comprobante= :numComprobante WHERE o.fichero =:fichero")
  public Integer updateNumComprobanteByFichero( @Param("numComprobante") Integer setnumComprobante, @Param("fichero") String fichero);
  
  @Modifying @Query(value="UPDATE TMCT0_CONTA_PLANTILLAS SET TOLERANCE = :tolerance WHERE CODIGO = :codigo",nativeQuery=true)
  public void modificarTolerance(@Param("tolerance") Integer tolerance,@Param("codigo") String codigo);
}
