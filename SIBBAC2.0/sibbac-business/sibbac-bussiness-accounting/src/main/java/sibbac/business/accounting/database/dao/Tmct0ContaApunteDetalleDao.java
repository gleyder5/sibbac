package sibbac.business.accounting.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.database.model.Tmct0ContaApunteDetalle;

/**
 * Data access object de conta plantilla.
 * 
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ContaApunteDetalleDao extends
		JpaRepository<Tmct0ContaApunteDetalle, Integer> {

}
