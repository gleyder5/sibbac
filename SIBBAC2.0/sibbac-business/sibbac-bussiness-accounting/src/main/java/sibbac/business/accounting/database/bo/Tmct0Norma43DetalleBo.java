package sibbac.business.accounting.database.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.dao.Tmct0Norma43MovimientoDaoImp;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0Norma43MovimientoBo extends AbstractBo<Norma43Movimiento, Long, Norma43MovimientoDao> {

  @Autowired
  Norma43MovimientoDao norma43MovimientoDao;
  
  @Autowired
  Tmct0Norma43MovimientoDaoImp tmct0Norma43MovimientoDaoImp;

  /**
   * Pasaje de cobro a procesado por idMovimiento.
   * 
   * @param idMovimiento idMovimiento
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void pasarCobroAProcesadobyIdMovimiento(Long idMovimiento, int procesado) throws EjecucionPlantillaException {
    // El registro del cobro se tiene que pasar a
    // PROCESADO=1 en la tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el
    // campo ID_MOV
    Norma43Movimiento movimiento = norma43MovimientoDao.findOne(idMovimiento);
    movimiento.setProcesado(procesado);
    LOG.trace("Pasando cobro a procesado: idMovimiento = " + idMovimiento + " [Metodo]" + "[" + new Date() + "]");
    this.norma43MovimientoDao.save(movimiento);
  }

  /**
   * Pasaje de cobro a procesado por desc_referencia.
   * 
   * @param sDescReferencia desc-´referencia de todos los movimientos que se tienen que procesar
   * @param iProcesado valor del campo procesado 1- sin descuadre 2- con descuadre
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
//  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
//  public void pasarCobroAProcesadobyDescReferencia(String sDescReferencia, Integer iProcesado) throws EjecucionPlantillaException {
//    // El registro del cobro se tiene que pasar al valor recibido en iProcesado buscnado los movimientos por
//    // desc-referencia y el valor de procesado se
//    // recibe por paramtro (1 sin descuadre, 2 con descuadre)
//    List<Norma43Movimiento> lMovimientos = this.norma43MovimientoDao.findByDescReferencia(sDescReferencia);
//    for (Norma43Movimiento movimiento : lMovimientos) {
//      movimiento.setProcesado(iProcesado);
//      LOG.trace("Pasando cobro a procesado: descReferencia = " + sDescReferencia + " [Metodo]" + "[" + new Date() + "]");
//      this.norma43MovimientoDao.save(movimiento);
//    }
//
//  }
  
  /**
   * 	Se actualizan movimientos una vez procesada la plantilla de cobro para apuntes manuales.
   * 	@param whereQueryCobro: where modificado de la query de cobro
   * 	@param plantilla: plantilla para recobrar datos necesarios
   * 	@throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void pasarCobroAProcesadoAM(String whereQueryCobro, Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {
	  // Se reemplaza la cuenta bancaria.
	  String whereQueryCobroModif = whereQueryCobro.replaceAll(
			  "<CUENTA_BANCARIA DE LA PLANTILLA>", plantilla.getCuenta_bancaria().replaceAll("\"", "'")).replace(";", "");
	  this.tmct0Norma43MovimientoDaoImp.updateByIdsWhereCobro(whereQueryCobroModif);
  }

  /**
   * Se obtiene lista de movimientos Norma43 por referencias THEIR para Cobro-Liquidacion.
   * 
   * @param amExcelDTO amExcelDTO
   * @return List<Norma43Movimiento> List<Norma43Movimiento>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Deprecated
  public List<Norma43Movimiento> getListaNorma43MovimientoByReferenciasTheir(ApuntesManualesExcelDTO amExcelDTO) throws EjecucionPlantillaException {
    List<Norma43Movimiento> listaNorma43Movimiento = null;
    String[] referenciasTheirMultiples = null;
    if (StringUtils.isNotBlank(amExcelDTO.getSibbacReferenciasTheirTotal())) {
      referenciasTheirMultiples = amExcelDTO.getSibbacReferenciasTheirTotal().split(Pattern.quote(","));
    }
    if (referenciasTheirMultiples != null && referenciasTheirMultiples.length > 0) {
      listaNorma43Movimiento = new ArrayList<Norma43Movimiento>();
      for (String refTheir : referenciasTheirMultiples) {
        if (StringUtils.isNotBlank(refTheir)) {
          listaNorma43Movimiento.addAll(this.norma43MovimientoDao.findByDescReferencia(refTheir.replaceAll("'","")));
        }
      }
    }
    return listaNorma43Movimiento;
  }

  /**
   * Se obtiene lista de errores en caso de no haber movimientos Norma43 por referencias THEIR - SIBBAC en apuntes
   * manuales.
   * 
   * @param amExcelDTO amExcelDTO
   * @return List<String> List<String>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  public List<String> getListaErroresNorma43MovimientoByReferenciasTheirSIBBAC(ApuntesManualesExcelDTO amExcelDTO) throws EjecucionPlantillaException {
    List<String> listaErroresNorma43 = new ArrayList<String>();
    String[] referenciasTheirMultiples = null;
    if (StringUtils.isNotBlank(amExcelDTO.getSibbacReferenciasTheirTotal())) {
      referenciasTheirMultiples = amExcelDTO.getSibbacReferenciasTheirTotal().split(Pattern.quote(","));
    }
	// Validacion obtener movimientos a partir de referencias their: se tiene en cuenta ademas procesado = 0
    if (referenciasTheirMultiples != null && referenciasTheirMultiples.length > 0) {
    	int indexColumn = 5; 
    	for (String refTheir : referenciasTheirMultiples) {
    		if (StringUtils.isNotBlank(refTheir)) {
    			refTheir = refTheir.replace("'", "");
    			List<Norma43Movimiento> listaNorma43 = null;
    			if (refTheir.length() <= Constantes.LONGITUD_DESC_REFERENCIA_TMCT0_NORMA43_MOVIMIENTO) {
    				listaNorma43 = this.norma43MovimientoDao.findByDescReferenciaAndProcesado(refTheir.trim());        	  
    			}
    			if (CollectionUtils.isEmpty(listaNorma43)) {
    				listaNorma43 = this.norma43MovimientoDao.findByReferenciaDescReferenciaConcatAndProcesado(refTheir.trim());
    			}
    			if (listaNorma43 == null || CollectionUtils.isEmpty(listaNorma43)) {
    				listaErroresNorma43.add("Error en cabecera. No existe el movimiento norma43(fila: " + (amExcelDTO.getSibbacNroFilaExcel()-1) + ", columna: " + indexColumn + ")");
    			}
    		}
    		indexColumn ++;
    	}
    }
    return listaErroresNorma43;
  }

  /**
   * Pasa el Norma43Movimiento a estado procesado para liquidaciones.
   * 
   * @param idMovimiento
   */

  // Es el mimso metodo que cobros y se ha creado solo uno pasarCobroAProcesaByDescReferencia o
  // pasarCobroAProcesabyIdMovimiento
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void pasarCobroAProcesadoCobroRetLiq(String sDescReferencia, Integer iProcesado) {
    // El registro del cobro se tiene que pasar a PROCESADO=1 en la tabla
    // TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV
    // MFG 18/04/2017 se modifica este metodo, se buscan los movimientos por desc-referencia y el valor de procesado se
    // recibe por paramtro (1 sin descuadre, 2 con descuadre)
    List<Norma43Movimiento> lMovimientos = this.norma43MovimientoDao.findByDescReferencia(sDescReferencia);
    for (Norma43Movimiento movimiento : lMovimientos) {
      movimiento.setProcesado(iProcesado);
      LOG.trace("Pasando cobro a procesado: idMovimiento = " + movimiento.getId() + " [Metodo]" + "[" + new Date()
                + "]");
      this.norma43MovimientoDao.save(movimiento);
    }
  }
}
