package sibbac.business.accounting.conciliacion.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.common.SIBBACBusinessException;

@Repository
public class Tmct0ContaPlantillasConciliaDaoImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaPlantillasConciliaDaoImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 *	Obtiene lista de valores distintos segun el nombre de la columna.
	 *	@param columnName
	 *	@return List<String>
	 *	@throws SIBBACBusinessException 
	 */
	@SuppressWarnings("unchecked")
	public List<String> getDistinctValuesByColumnName(String columnName) throws SIBBACBusinessException {

		LOG.info("INICIO - DAOIMPL - getDistinctValuesByColumnName");

		List<String> listaFields = new ArrayList<String>();

		try {
			StringBuilder consulta = new StringBuilder(
					"SELECT DISTINCT " + columnName + " FROM BSNBPSQL.TMCT0_CONTA_PLANTILLAS_CONCILIA");
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			listaFields = query.getResultList();
		} catch (Exception e) {
			LOG.error("Error metodo getDistinctValuesByColumnName -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - getDistinctValuesByColumnName");

		return listaFields;
	}

}