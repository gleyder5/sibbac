package sibbac.business.accounting.database.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.model.Tmct0Norma43Detalle;

/**
 * Data access object de conta Tmct0Norma43Detalle.
 *
 */
@Repository
public interface Tmct0Norma43DetalleDao extends JpaRepository<Tmct0Norma43Detalle, Integer> {

	/**
	 *	Obtiene lista de tnd.idmovimiento, tnd.tipoComision, tnd.importe agrupados por
	 *  tipoApunte y tipoComision
	 *  @param tipoApunte tipoApunte
	 *  @param tipoComision tipoComision
	 *  @param idsMovimiento idsMovimiento
	 *  @return List<Object[]> List<Object[]>    
	 */
	@Query("SELECT DISTINCT tnd.idmovimiento, SUM(tnd.importe) FROM Tmct0Norma43Detalle "
			+ "AS tnd WHERE tnd.tipoApunte = :tipoApunte AND tnd.tipoComision = :tipoComision "
			+ "GROUP BY tnd.idmovimiento, tnd.tipoComision, tnd.importe "
			+ "HAVING tnd.idmovimiento IN (:idsMovimiento)")
	public List<Object[]> findGroupedFieldsByTipoApunteAndTipoComisionAndIdsMovimiento(
		@Param("tipoApunte") String tipoApunte, @Param("tipoComision") String tipoComision, 
		@Param("idsMovimiento") List<Integer> idsMovimiento);
	
	/**
	 * 	Borra los registros segun IDMOVIMIENTO's y TIPO_APUNTE
	 * 	@param idmovimiento idmovimiento
	 * 	@param tipoApunte tipoApunte  
	 */
	@Transactional
	@Modifying
	@Query( "DELETE FROM Tmct0Norma43Detalle "
		+ "WHERE idmovimiento IN (:idsmovimiento) AND tipoApunte = :tipoApunte" )
	void deleteByIdsMovimientoAndTipoApunte( 
		@Param("idsmovimiento") Set<Integer> idsmovimiento, @Param("tipoApunte") String tipoApunte);
	
}
