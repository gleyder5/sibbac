package sibbac.business.accounting.conciliacion.database.bo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConciliacionAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaOurDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDaoImpl;
import sibbac.business.accounting.conciliacion.database.dto.DatosApuntesExcelDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurApuntesDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ApuntesAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0PantallaConciliacionOURAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums;

/**
 *	Bo para control de contabilidad. 
 */
@Service
public class Tmct0PantallaConciliacionOURBo {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0PantallaConciliacionOURBo.class);

	@Autowired
	private Tmct0ContaPartidasPndtsDaoImpl tmct0ContaPartidasPndtsDaoImpl;

	@Autowired
	private Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;

	@Autowired
	private Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

	@Autowired
	private Tmct0PantallaConciliacionOURAssembler tmct0PantallaConciliacionOURAssembler;

	@Autowired
	Tmct0ContaApuntesPndtsDao tmct0ContaApuntesPndtsDao;

	@Autowired
	private Tmct0ContaConciliacionAuditDao tmct0ContaConciliacionAuditDao;

	@Autowired
	private Tmct0ContaPartidasCasadasDAO tmct0ContaPartidasCasadasDAO;

	@Autowired
	Tmct0ContaOurDAO tmct0ContaOurDAO;

	@Autowired
	private Tmct0ApuntesAssembler tmct0ApuntesAssembler;
	
	@Autowired
	private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;

	@Value("${accounting.contabilidad.emails}")
	private String listaEmails;

	@Value("${accounting.contabilidad.folder.in}")
	private String folderOut;

	@Autowired
	private SendMail sendMail;

	@Transactional(
			propagation = Propagation.REQUIRED,
			isolation = Isolation.DEFAULT,
			readOnly = true)
	public void generarOurConTheir (Map<String, Object> paramsObjects) throws Exception {

		SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyy");
		SimpleDateFormat formatHour = new SimpleDateFormat("HHmmss");
		List<GrillaTLMDTO> partidas = new ArrayList<GrillaTLMDTO>();
		List<BigInteger> listIdsTLM = new ArrayList<BigInteger>();
		String file = null;

		try {
			// Se activa el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			
			List<Map<String,Object>> theirsSelected = (List<Map<String,Object>>) paramsObjects.get("listaPartidasTLMIzq");
			String fechaActual = formatDate.format(new Date());
			String horaActual = formatHour.format(new Date());

			// Separamos la lista de TLM y de OUR por AUX
			ArrayList<LinkedHashMap> lisTLM = (ArrayList<LinkedHashMap>) paramsObjects.get("listaPartidasTLMIzq");
			ArrayList<LinkedHashMap> lisOUR = (ArrayList<LinkedHashMap>) paramsObjects.get("listaDesglosesOUR");
			LinkedHashMap apunteFinal = (LinkedHashMap) paramsObjects.get("apunte");
			LinkedHashMap concepto = (LinkedHashMap) paramsObjects.get("concepto");
			String usuarioAudit = (String)paramsObjects.get("auditUser");

			// Se cambia por cada apunte lisTLM su estado=1 en la tabla TMCT0_CONTA_PARTIDAS_PNDTS
			for (LinkedHashMap listaTLM : lisTLM) {
				listIdsTLM.add(BigInteger.valueOf(((Integer)listaTLM.get("id")).longValue()));
			}
			tmct0ContaPartidasPndtsDao.updateEstadoUnoFromId(listIdsTLM);

			// Se comprueba si el concepto existe o no, si no existe se crea
			Tmct0ContaConcepto contaConcepto = tmct0ContaPlantillasConciliaBo.comprobarConcepto(concepto);

			BigDecimal importePyGTotal = null;
			if (apunteFinal.get("importePyGTotal") instanceof Double) {
				importePyGTotal = BigDecimal.valueOf((Double) apunteFinal.get("importePyGTotal"));
			} else {
				importePyGTotal = BigDecimal.valueOf((Integer) apunteFinal.get("importePyGTotal"));
			}
			
		    if (importePyGTotal.signum() < 0) {
		    	importePyGTotal = importePyGTotal.negate();
		    }

			// Se comprueba si el PYG global es mayor o menor del TOLERANCE
			if (tmct0ContaPlantillasConciliaBo.compararPyGConTolerance(((String)((LinkedHashMap)lisOUR.get(0)).get("codigoPlantilla")),importePyGTotal)) {
				// Si es mayor se guarda el apunte en CONTA_APUNTES_PNDTS. 
				Tmct0ContaApuntesPndts contaApuntesPndts = tmct0PantallaConciliacionOURAssembler.getTmct0ContaApuntesPndts(usuarioAudit, contaConcepto, apunteFinal);
				tmct0ContaApuntesPndtsDao.save(contaApuntesPndts);

				// Además se guarda un registro en la tabla de CONCILIA_AUDIT con ORIGEN=SIBBAC.
				Tmct0ContaConciliacionAudit conciliacionAudit = tmct0PantallaConciliacionOURAssembler.getTmct0ContaConciliacionAudit(usuarioAudit,contaConcepto);
				tmct0ContaConciliacionAuditDao.save(conciliacionAudit);

				// ListTLM se inserta en la de CASADAS con el ID_APUNTES y ID_CONCILIA_AUDIT del registro insertado anteriormente.
				List<Tmct0ContaPartidasCasadas> partidasCasadas = tmct0PantallaConciliacionOURAssembler.getTmct0ContaPartidasCasadas(lisTLM,contaApuntesPndts,conciliacionAudit);
				tmct0ContaPartidasCasadasDAO.save(partidasCasadas);

				// listOUR se inserta en a la tabla de OUR con el ID_APUNTES y ID_CONCILIA_AUDIT del registro insertado anteriormente.
				List<Tmct0ContaOur> partidasOur = tmct0PantallaConciliacionOURAssembler.getTmct0ContaPartidasOur(lisOUR,contaApuntesPndts,conciliacionAudit);
				tmct0ContaOurDAO.save(partidasOur);

				try {
					// Se le envía un correo al usuario informandole del apunte pendiente
					List<String> listEmails = new ArrayList<String>();
					String[] lisEmailsSplit = StringUtils.splitPreserveAllTokens(listaEmails, ";");
					for (String email : lisEmailsSplit) {
						listEmails.add(email);
					}

					StringBuilder body = new StringBuilder();
					body.append("Se ha generado el siguiente apunte pendiente de aprobar");
					body.append("\n");
					body.append("FEAPUNTE|USUAPUNTE|CONCEPTO|IMPORTEDEBE|IMPORTEHABER|IMPORTEPYG");
					body.append("\n");
					body.append(contaApuntesPndts.getFeApunte()+
							"|"+contaApuntesPndts.getUsuApunte()+
							"|"+contaApuntesPndts.getConcepto().getId()+
							"|"+contaApuntesPndts.getImporteDebe()+
							"|"+contaApuntesPndts.getImporteHaber()+
							"|"+contaApuntesPndts.getImportePyG());

					sendMail.sendMail(listEmails, "subject mail", body.toString());
				} catch (Exception e) {
					LOG.error("Error al enviar mail", e);
				}
			}
			else {

				Tmct0ContaConciliacionAudit conciliacionAudit = tmct0PantallaConciliacionOURAssembler.getTmct0ContaConciliacionAudit(usuarioAudit,contaConcepto);
				tmct0ContaConciliacionAuditDao.save(conciliacionAudit);

				// ListTLM se inserta en la de CASADAS con el ID_APUNTES y ID_CONCILIA_AUDIT del registro insertado anteriormente.
				List<Tmct0ContaPartidasCasadas> partidasCasadas = tmct0PantallaConciliacionOURAssembler.getTmct0ContaPartidasCasadas(lisTLM,null,conciliacionAudit);
				tmct0ContaPartidasCasadasDAO.save(partidasCasadas);

				// listOUR se inserta en a la tabla de OUR con el ID_APUNTES y ID_CONCILIA_AUDIT del registro insertado anteriormente.
				List<Tmct0ContaOur> partidasOur = tmct0PantallaConciliacionOURAssembler.getTmct0ContaPartidasOur(lisOUR,null,conciliacionAudit);
				tmct0ContaOurDAO.save(partidasOur);

			}

			// Se comprueba si el PYG global es mayor o menor del TOLERANCE
			if (!tmct0ContaPlantillasConciliaBo.compararPyGConTolerance(((String)((LinkedHashMap)lisOUR.get(0)).get("codigoPlantilla")),importePyGTotal)) {
				// Se genera el excel
				String titulo = "apunte_sibbac_" + fechaActual + "_" + horaActual + ".xlsx";
	
				if(null != theirsSelected) {
					for (Map<String,Object> tlm : theirsSelected) {
						partidas.add(tmct0ApuntesAssembler.getGrillaTLMDTO(tlm));
					}
				}
	
				List<Tmct0ContaOurApuntesDTO> camposClave = tmct0PantallaConciliacionOURAssembler.getTmct0ContaOurApuntesDTOs(lisOUR);
				
				//Se recupera un lista de auxiliares bancarios
				List<String> listAux = new ArrayList<String> ();
				for (int j = 0; j < camposClave.size(); j++) {
					Tmct0ContaOurApuntesDTO our = camposClave.get(j);
					if(!listAux.contains(our.getAuxiliar())){
						listAux.add(our.getAuxiliar());
					}
				}
				
				for(String aux : listAux){

					String codigoPlatilla1 = "";
					BigDecimal total1 = BigDecimal.ZERO;
					for (int j = 0; j < camposClave.size(); j++) {
						Tmct0ContaOurApuntesDTO our = camposClave.get(j);
						if((!"".equals(codigoPlatilla1) || codigoPlatilla1.equals(our.getPlantilla())) && aux.equals(our.getAuxiliar())){
							codigoPlatilla1 = our.getPlantilla();
							total1 = total1.add(our.getImporte());
						}
					}
					
					String codigoPlatilla2 = "";
					BigDecimal total2 = BigDecimal.ZERO;
					for (int j = 0; j < camposClave.size(); j++) {
						Tmct0ContaOurApuntesDTO our = camposClave.get(j);
						if(!codigoPlatilla1.equals(our.getPlantilla()) && aux.equals(our.getAuxiliar())){
							codigoPlatilla2 = our.getPlantilla();
							total2 = total2.add(our.getImporte());
						}
					}
					
					for (int j = 0; j < camposClave.size(); j++) {
						Tmct0ContaOurApuntesDTO our = camposClave.get(j);
						if(aux.equals(our.getAuxiliar())){
							if(total2.compareTo(BigDecimal.ZERO)!=0){
								if(total1.compareTo(total2) == 1){
									our.setPlantilla(codigoPlatilla1);
								}else{
									our.setPlantilla(codigoPlatilla2);
								}
							}
						}
					}
				}
				
				
				List<DatosApuntesExcelDTO> filasGrilla = AccountingHelper.generarFilasGrilla(partidas, camposClave, (String) concepto.get("value"));
				XSSFWorkbook wb = AccountingHelper.generarXLSApuntesSIBBAC(filasGrilla);
	
				file = folderOut + titulo;
				FileOutputStream stream = new FileOutputStream(file);
				wb.write(stream);
				wb.close();
				stream.close();
			}
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

		} catch (IOException e) {
			LOG.error("Error metodo generarOurConTheir -  " + e.getMessage(), e);
			throw(e);
		} catch (Exception e) {
			LOG.error("Error metodo generarOurConTheir -  " + e.getMessage(), e);
			throw(e);
		}

	}

}
