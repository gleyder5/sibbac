package sibbac.business.accounting.conciliacion.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0ContaPlantillasConciliaBo;
import sibbac.business.accounting.conciliacion.database.bo.Tmct0ControlContabilidadBo;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesAuditDaoImpl;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaOurDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPlantillasConciliaDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaToleranceAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaToleranceAuditDaoImpl;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ApuntesAuditoriaDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ApuntesPndtsDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaPartidasCasadasDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ToleranceAuditoriaDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaOurAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaPartidasCasadasAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ControlContabilidadAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaToleranceAudit;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.wrappers.helper.Helper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;

@SIBBACService
public class SIBBACServiceControlContabilidad implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceControlContabilidad.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0ContaApuntesPndtsDao tmct0ContaApuntesPndtsDao;

  @Autowired
  Tmct0ControlContabilidadAssembler tmct0ControlContabilidadAssembler;

  @Autowired
  Tmct0ContaApuntesAuditDao tmct0ContaApuntesAuditDao;

  @Autowired
  Tmct0ContaPartidasCasadasDAO tmct0ContaPartidasCasadasDAO;

  @Autowired
  Tmct0ContaOurDAO tmct0ContaOurDAO;

  @Autowired
  Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;

  @Autowired
  Tmct0UsersBo tmct0UsersBo;

  @Autowired
  private SendMail sendMail;

  @Autowired
  Tmct0ContaPlantillasConciliaDao tmct0ContaPlantillasConciliaDao;
  
  @Autowired
  Tmct0ContaPlantillasDao tmct0ContaPlantillasDao;

  @Autowired
  Tmct0ContaToleranceAuditDao tmct0ContaToleranceAuditDao;

  @Autowired
  Tmct0ContaApuntesAuditDaoImpl tmct0ContaApuntesAuditDaoImpl;

  @Autowired
  Tmct0ContaToleranceAuditDaoImpl tmct0ContaToleranceAuditDaoImpl;

  @Autowired
  Tmct0ContaPartidasCasadasDAO contaPartidasCasadasDAO;

  @Autowired
  Tmct0ContaPartidasCasadasAssembler tmct0ContaPartidasCasadasAssembler;

  @Autowired
  Tmct0ContaOurAssembler tmct0ContaOurAssembler;

  @Autowired
  Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

  @Autowired
  Tmct0ControlContabilidadBo Tmct0ControlContabilidadBo;

  /**
   * The Enum ComandoFacturasManuales.
   */
  private enum ComandoControlContabilidad {

    /** Consultar los apuntes pendientes */
    CONSULTAR_APUNTES_PENDIENTES("consultarApuntesPndts"),

    /** Aprobar apuntes */
    APROBAR_APUNTES("aprobarApuntes"),

    /** Rechazar apuntes */
    RECHAZAR_APUNTES("rechazarApuntes"),

    /** Modificar tolerance */
    MODIFICAR_TOLERANCE("modificarTolerance"),

    /** Consultar Apuntes Auditoria */
    CONSULTAR_AUDITORIA_APUNTES("consultarAuditoriaApuntes"),

    /** Consultar Tolerances Auditoria */
    CONSULTAR_AUDITORIA_TOLERANCE("consultarAuditoriaTolerance"),

    /** Consultar Partidas Casadas */
    CONSULTAR_PARTIDAS_CASADAS("consultarPartidasCasadas"),

    /** Consultar Our */
    CONSULTAR_OUR("consultarOur"),

    /** Exportar Excel */
    EXPORTAR_EXCEL("exportarXLS"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandoControlContabilidad(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandoControlContabilidad command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceControlContabilidad");

      switch (command) {
      case CONSULTAR_APUNTES_PENDIENTES:
        result.setResultados(consultarApuntesPndts(paramsObjects));
        break;
      case APROBAR_APUNTES:
        result.setResultados(aprobarApuntes(paramsObjects));
        break;
      case RECHAZAR_APUNTES:
        result.setResultados(rechazarApuntes(paramsObjects));
        break;
      case MODIFICAR_TOLERANCE:
        result.setResultados(modificarTolerance(paramsObjects));
        break;
      case CONSULTAR_AUDITORIA_APUNTES:
        result.setResultados(consultarApuntesAuditoria(paramsObjects));
        break;
      case CONSULTAR_AUDITORIA_TOLERANCE:
        result.setResultados(consultarToleranceAuditoria(paramsObjects));
        break;
      case CONSULTAR_PARTIDAS_CASADAS:
        result.setResultados(consultarPartidasCasadas(paramsObjects));
        break;
      case CONSULTAR_OUR:
        result.setResultados(consultarOur(paramsObjects));
        break;
      case EXPORTAR_EXCEL:
        result.setResultados(exportarAExcel(paramsObjects));
        break;
      default:
        result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceControlContabilidad");
    return result;
  }

  /**
   * Consulta de los apuntes pendientes
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarApuntesPndts(Map<String, Object> paramObjects) throws SIBBACBusinessException,
  ParseException {

    LOG.info("INICIO - SERVICIO - consultarApuntesPndts");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaApuntesPndtsTablaDTO = new ArrayList<Object>();
    List<Tmct0ContaApuntesPndts> listaApuntesPndts = new ArrayList<Tmct0ContaApuntesPndts>();

    try {
      // Realizamos la llamada a la consulta de apuntes pendientes
      listaApuntesPndts = tmct0ContaApuntesPndtsDao.findAll();

      // Se transforman a lista de tablaDTO
      for (int i = 0; i < listaApuntesPndts.size(); i++) {
        Tmct0ApuntesPndtsDTO apuntesPndtsTasblaDTO = tmct0ControlContabilidadAssembler.getTmct0ApuntesPndtsTablaDTO(listaApuntesPndts.get(i));
        listaApuntesPndtsTablaDTO.add(apuntesPndtsTasblaDTO);
      }

    } catch (Exception e) {
      LOG.error("Error metodo consultarApuntesPndts -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaApuntesPendientes", listaApuntesPndtsTablaDTO);

    LOG.info("FIN - SERVICIO - consultarApuntesPndts");

    return result;
  }

  /**
   * Aprueba los apuntes seleccionados.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> aprobarApuntes(Map<String, Object> paramsObject) throws SIBBACBusinessException {
    Map<String, Object> result = new HashMap<String, Object>();
    try {
      Tmct0ControlContabilidadBo.aprobarApuntes(paramsObject);
    }catch (Exception e) {
      LOG.error("Error metodo aprobarApuntes -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }
    return result;
  }

  /**
   * Rechaza los apuntes seleccionados.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> rechazarApuntes(Map<String, Object> paramsObject) throws SIBBACBusinessException {    Map<String, Object> result = new HashMap<String, Object>();
  try {
    Tmct0ControlContabilidadBo.rechazarApuntes(paramsObject);
  }catch (Exception e) {
    LOG.error("Error metodo rechazarApuntes -  " + e.getMessage(), e);
    result.put("status", "KO");
    return result;
  }
  return result;
  }

  /**
   * Modifica un tolerance de un código de plantilla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> modificarTolerance(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
  ParseException {

    LOG.info("INICIO - SERVICIO - modificarTolerance");

    Map<String, Object> result = new HashMap<String, Object>();

    try {
      
      Integer tolerance = (Integer)paramsObjects.get("tolerance");
      String codigoPlantillaConcilia = (String) paramsObjects.get("codigoPlantilla");
      
      // Modificamos el tolerance
      tmct0ContaPlantillasConciliaDao.modificarTolerance(tolerance, codigoPlantillaConcilia);
      
      modificarTolerancePlantilla(tolerance, codigoPlantillaConcilia);

      // Guardamos un registro de la modificación en la tabla de TMCT0_CONTA_TOLERANCE_AUDIT
      Tmct0ContaToleranceAudit contaToleranceAudit = tmct0ControlContabilidadAssembler.getTmct0ContaToleranceAudit((String)paramsObjects.get("codigoPlantilla"), (Integer)paramsObjects.get("tolerance"), (String)paramsObjects.get("auditUser"));
      tmct0ContaToleranceAuditDao.save(contaToleranceAudit);

      // Indicamos que el resultado de la modificación en la BBDD ha sido correcto
      result.put("status", "OK");

    } catch (Exception e) {
      LOG.error("Error metodo modificarTolerance -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - modificarTolerance");

    return result;
  }
  
  /**
   * Este método modifica el tolerance en la tabla TMCT0_CONTA_PLANTILLAS. Hay plantillas que tienen 
   * el mismo código en ambas tablas, pero hay otras que requieren una pequeña transformación para poder
   * obtener el código correspondiente por el cual vamos a realizar la actualización.
   * Ejemplo:
   * La plantilla LQEFECOMP.MANUAL.CTN existe en ambas tablas pero además, en la tabla  TMCT0_CONTA_PLANTILLAS existe la LQEFEMDOCOMP.CTN
   * si contiene EFE hay que buscar por EFEMDO y si tiene NET entonces por NETCLE
   * @param tolerance
   * @param codigoPlantillaConcilia
   */
  private void modificarTolerancePlantilla(Integer tolerance, String codigoPlantillaConcilia){
 

    String codigoPlantilla = "";
    //Si tiene la palabra MANUAL tenemos que reemplazar
    if(codigoPlantillaConcilia.contains("MANUAL")){
      
      codigoPlantilla = codigoPlantillaConcilia.replace(".MANUAL.", ".");
      
      //si pone EFE tu buscas por EFEMDO
      //y si tiene NET entonces por NETCLE      
      if(codigoPlantillaConcilia.contains("EFE")){
        codigoPlantilla = codigoPlantilla.replace("EFE", "EFEMDO");
      }
      else if(codigoPlantillaConcilia.contains("NET")){
        codigoPlantilla = codigoPlantilla.replace("NET", "NETCLE");
      }  
    }
    
    //Una vez hecha la transformación actualizamos tanto por codigoPlantilla como por codigoPlantillaConcilia
    tmct0ContaPlantillasDao.modificarTolerance(tolerance, codigoPlantillaConcilia);
    
    if(!codigoPlantilla.isEmpty())
      tmct0ContaPlantillasDao.modificarTolerance(tolerance, codigoPlantilla);   
  }

  /**
   * Consulta de los apuntes auditados
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarApuntesAuditoria(Map<String, Object> paramObjects) throws SIBBACBusinessException,
  ParseException {

    LOG.info("INICIO - SERVICIO - consultarApuntesAuditoria");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaApuntesAuditoriaTablaDTO = new ArrayList<Object>();
    List<Tmct0ContaApuntesAudit> listaApuntesAuditoria = new ArrayList<Tmct0ContaApuntesAudit>();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
    Date fechaDesde = null;
    Date fechaHasta = null;
    List<Date> listFechas = new ArrayList<Date>();
    String usuario = null;

    try {
      // Al haber fecha desde y hasta se consigue un listado de las fechas comprendidas para optimizar la consulta a la BBDD
      if (!"".equals(paramObjects.get("fechaApunteDesde"))) {
        fechaDesde = formatter.parse(String.valueOf(paramObjects.get("fechaApunteDesde")));
      }
      if (!"".equals(paramObjects.get("fechaApunteHasta"))) {
        fechaHasta = formatter.parse(String.valueOf(paramObjects.get("fechaApunteHasta")));
      }

      if (null == fechaDesde) {
        listFechas = null;
      } else {
        listFechas = Helper.separarFechas(fechaDesde,fechaHasta);
      }

      if (null!=paramObjects.get("usuario") && !"".equals(paramObjects.get("usuario"))) {
        usuario = (String)((LinkedHashMap)paramObjects.get("usuario")).get("key");
      }

      // Realizamos la llamada a la consulta de apuntes auditados
      listaApuntesAuditoria = tmct0ContaApuntesAuditDaoImpl.consultarApuntesAudit(usuario, listFechas);

      // Se transforman a lista de tablaDTO
      for (int i = 0; i < listaApuntesAuditoria.size(); i++) {
        try {
          Tmct0ApuntesAuditoriaDTO apuntesAuditoriaTasblaDTO = tmct0ControlContabilidadAssembler.getTmct0ApuntesAuditoriaTablaDTO(listaApuntesAuditoria.get(i));
          listaApuntesAuditoriaTablaDTO.add(apuntesAuditoriaTasblaDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarApuntesAuditoria - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de apuntes auditados a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarApuntesAuditoria -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarApuntesAuditoria -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaApuntesAuditoria", listaApuntesAuditoriaTablaDTO);

    LOG.info("FIN - SERVICIO - consultarApuntesAuditoria");

    return result;
  }

  /**
   * Consulta de los tolerances auditados
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarToleranceAuditoria(Map<String, Object> paramObjects) throws SIBBACBusinessException,
  ParseException {

    LOG.info("INICIO - SERVICIO - consultarToleranceAuditoria");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaTolerancesAuditoriaTablaDTO = new ArrayList<Object>();
    List<Tmct0ContaToleranceAudit> listaToleranceAuditoria = new ArrayList<Tmct0ContaToleranceAudit>();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
    Date fechaDesde = null;
    Date fechaHasta = null;
    List<Date> listFechas = new ArrayList<Date>();
    String usuario = null;
    String codigoPlantilla = null;

    try {
      // Al haber fecha desde y hasta se consigue un listado de las fechas comprendidas para optimizar la consulta a la BBDD
      if (!"".equals(paramObjects.get("fechaToleranceDesde"))) {
        fechaDesde = formatter.parse(String.valueOf(paramObjects.get("fechaToleranceDesde")));
      }
      if (!"".equals(paramObjects.get("fechaToleranceHasta"))) {
        fechaHasta = formatter.parse(String.valueOf(paramObjects.get("fechaToleranceHasta")));
      }

      if (null == fechaDesde) {
        listFechas = null;
      } else {
        listFechas = Helper.separarFechas(fechaDesde,fechaHasta);
      }

      if (null!=paramObjects.get("usuario") && !"".equals(paramObjects.get("usuario"))) {
        usuario = (String)((LinkedHashMap)paramObjects.get("usuario")).get("key");
      }

      if (null!=paramObjects.get("codigoPlantilla") && !"".equals(paramObjects.get("codigoPlantilla"))) {
        codigoPlantilla = (String)((LinkedHashMap)paramObjects.get("codigoPlantilla")).get("key");
      }

      // Realizamos la llamada a la consulta de apuntes auditados
      listaToleranceAuditoria = tmct0ContaToleranceAuditDaoImpl.consultarToleranceAudit(usuario, codigoPlantilla, listFechas);

      // Se transforman a lista de tablaDTO
      for (int i = 0; i < listaToleranceAuditoria.size(); i++) {
        try {
          Tmct0ToleranceAuditoriaDTO toleranceAuditoriaTasblaDTO = tmct0ControlContabilidadAssembler.getTmct0ToleranceAuditoriaTablaDTO(listaToleranceAuditoria.get(i));
          listaTolerancesAuditoriaTablaDTO.add(toleranceAuditoriaTasblaDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarToleranceAuditoria - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de tolerances auditados a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarToleranceAuditoria -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarToleranceAuditoria -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaToleranceAuditoria", listaTolerancesAuditoriaTablaDTO);

    LOG.info("FIN - SERVICIO - consultarToleranceAuditoria");

    return result;
  }

  /**
   * Consulta las partidas casadas
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarPartidasCasadas(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
  ParseException {

    LOG.info("INICIO - SERVICIO - consultarPartidasCasadas");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Tmct0ContaPartidasCasadasDTO> listaTmct0ContaConciliacionAuditTablaDTO = new ArrayList<Tmct0ContaPartidasCasadasDTO>();
    List<Tmct0ContaPartidasCasadas> listaContaPartidasCasadas = new ArrayList<Tmct0ContaPartidasCasadas>();

    try {
      BigInteger idApunteAudit = new BigInteger(String.valueOf(paramsObjects.get("id")));
      listaContaPartidasCasadas = this.contaPartidasCasadasDAO.findByIdApunteAudit(idApunteAudit);

      if (null != listaContaPartidasCasadas) {
        for (Tmct0ContaPartidasCasadas tmct0ContaPartidasCasadas : listaContaPartidasCasadas) {
          listaTmct0ContaConciliacionAuditTablaDTO.add(tmct0ContaPartidasCasadasAssembler.getTmct0ContaPartidasCasadasDTO(tmct0ContaPartidasCasadas));
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo consultarPartidasCasadas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaPartidasCasadas", listaTmct0ContaConciliacionAuditTablaDTO);

    LOG.info("FIN - SERVICIO - consultarPartidasCasadas");

    return result;
  }

  /**
   * Consulta Our
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarOur(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
  ParseException {

    LOG.info("INICIO - SERVICIO - consultarOur");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Tmct0ContaOur> listaTmct0ContaOur = new ArrayList<Tmct0ContaOur>();
    List<Tmct0ContaOurDTO> listaTmct0ContaOurDTO = new ArrayList<Tmct0ContaOurDTO>();

    try {
      BigInteger idApunteAudit = new BigInteger(String.valueOf(paramsObjects.get("id")));
      listaTmct0ContaOur = this.tmct0ContaOurDAO.findByIdApunteAudit(idApunteAudit);

      if (null != listaTmct0ContaOur) {
        for (Tmct0ContaOur tmct0ContaOur : listaTmct0ContaOur) {
          listaTmct0ContaOurDTO.add(tmct0ContaOurAssembler.getTmct0ContaOurDTO(tmct0ContaOur));
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo consultarOur -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaOur", listaTmct0ContaOurDTO);

    LOG.info("FIN - SERVICIO - consultarOur");

    return result;
  }

  @SuppressWarnings("unchecked")
  private Map<String, Object> exportarAExcel(Map<String, Object> paramsObjects) {
    LOG.info("INICIO - SERVICIO - exportarAExcel");
    Map<String, Object> result = new HashMap<String, Object>();
    List<Tmct0ContaPartidasCasadasDTO> listaTmct0ContaConciliacionAuditDTO = new ArrayList<Tmct0ContaPartidasCasadasDTO>();
    List<Tmct0ContaOurDTO> listaTmct0ContaOurDTO = new ArrayList<Tmct0ContaOurDTO>();

    try {
      LOG.debug("LLAMA A crearInformePorHojas SIMPLE");

      Map<String, Object> mapPartidasTLM = consultarPartidasCasadas(paramsObjects);
      listaTmct0ContaConciliacionAuditDTO = (List<Tmct0ContaPartidasCasadasDTO>) mapPartidasTLM.get("listaPartidasCasadas");
      Map<String, Object> mapPartidasSIBBAC = consultarOur(paramsObjects);
      listaTmct0ContaOurDTO = (List<Tmct0ContaOurDTO>) mapPartidasSIBBAC.get("listaOur");

      if ((null != listaTmct0ContaConciliacionAuditDTO && !listaTmct0ContaConciliacionAuditDTO
          .isEmpty())
          || (null != listaTmct0ContaOurDTO && !listaTmct0ContaOurDTO
          .isEmpty())) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XSSFWorkbook wb = AccountingHelper.generarExcelPartidasCasadasYOur(listaTmct0ContaConciliacionAuditDTO, listaTmct0ContaOurDTO);
        try {
          wb.write(bos);
        } catch (IOException e) {
          LOG.error(e.getMessage());
          LOG.error(e.toString());
        }

        result.put("excelPartidasCasadas", bos.toByteArray());
      }

      LOG.debug("FIN crearInformePorHojas SIMPLE");
    } catch (Exception e) {
      LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandoControlContabilidad getCommand(String command) {
    ComandoControlContabilidad[] commands = ComandoControlContabilidad.values();
    ComandoControlContabilidad result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandoControlContabilidad.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    return null;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

}
