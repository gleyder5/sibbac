package sibbac.business.accounting.conciliacion.database.bo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConciliacionAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dto.GrillaExcelConciliacionBancariaDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ApuntesAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaPartidasCasadasAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0PantallaConciliacionOURAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.common.utils.SibbacEnums;

/**
 *	Bo para control de contabilidad. 
 */
@Service
public class Tmct0PantallaConciliacionTLMBo {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0PantallaConciliacionTLMBo.class);

	@Autowired
	private Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;

	@Autowired
	private Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

	@Autowired
	private Tmct0PantallaConciliacionOURAssembler tmct0PantallaConciliacionOURAssembler;

	@Autowired
	private Tmct0ContaConciliacionAuditDao tmct0ContaConciliacionAuditDao;

	@Autowired
	private Tmct0ContaPartidasCasadasDAO tmct0ContaPartidasCasadasDAO;

	@Autowired
	private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;

	@Autowired
	private Tmct0ApuntesAssembler tmct0ApuntesAssembler;

	@Value("${accounting.contabilidad.folder.in}")
	private String folderOut;
	
	@Autowired
	private Tmct0ContaPartidasCasadasAssembler tmct0ContaPartidasCasadasAssembler;

	@Transactional(
			propagation = Propagation.REQUIRED,
			isolation = Isolation.DEFAULT,
			readOnly = true)
	public void generarApunteTLM (Map<String, Object> paramsObjects) throws Exception {

		List<GrillaTLMDTO> partidas = new ArrayList<GrillaTLMDTO>();
		List<GrillaExcelConciliacionBancariaDTO> filas = null;
		SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyy");
		SimpleDateFormat formatHour = new SimpleDateFormat("HHmmss");
		List<BigInteger> listIdsTLM = new ArrayList<BigInteger>();
		String file = null;

		try {
			List<Map<String,Object>> theirsSelected = (List<Map<String,Object>>) paramsObjects.get("listaPartidasTLMIzq");
			List<Map<String,Object>> theirsSelectedRigth = (List<Map<String,Object>>) paramsObjects.get("listaPartidasTLMDcha");
			theirsSelected.addAll(theirsSelectedRigth);
			LinkedHashMap concepto = (LinkedHashMap) paramsObjects.get("concepto");
			String usuarioAudit = (String)paramsObjects.get("auditUser");
			
			

			// TODO - Verificar que se haya colocado en el metodo correcto
			// Se activa el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

			// Se cambia por cada apunte lisTLM su estado=1 en la tabla TMCT0_CONTA_PARTIDAS_PNDTS
			for (Map<String,Object> listaTLMs : theirsSelected) {
				listIdsTLM.add(BigInteger.valueOf((Integer)listaTLMs.get("id")));
			}
			tmct0ContaPartidasPndtsDao.updateEstadoUnoFromId(listIdsTLM);

			// Se comprueba si el concepto existe o no, si no existe se crea
			Tmct0ContaConcepto contaConcepto = tmct0ContaPlantillasConciliaBo.comprobarConcepto(concepto);

			// Además se guarda un registro en la tabla de CONCILIA_AUDIT con ORIGEN=SIBBAC.
			Tmct0ContaConciliacionAudit conciliacionAudit = tmct0PantallaConciliacionOURAssembler.getTmct0ContaConciliacionAudit(usuarioAudit, contaConcepto);
			conciliacionAudit.setOrigen("TLM (pendientes)");
			tmct0ContaConciliacionAuditDao.save(conciliacionAudit);

			String fechaActual = formatDate.format(new Date());
			String horaActual = formatHour.format(new Date());

			String titulo = "apunte_saldo0_" + fechaActual + "_" + horaActual + ".xlsx";

			if(null != theirsSelected) {
				for (Map<String,Object> tlm : theirsSelected) {
					partidas.add(tmct0ApuntesAssembler.getGrillaTLMDTO(tlm));
				}
			}
			
			for (GrillaTLMDTO grillaTLMDTO : partidas) {
				Tmct0ContaPartidasPndts partidaPndt = tmct0ContaPartidasPndtsDao.findById(BigInteger.valueOf(grillaTLMDTO.getId().longValue()));
				Tmct0ContaPartidasCasadas partidaCasada = tmct0ContaPartidasCasadasAssembler.getTmct0ContaPartidasCasadas(partidaPndt, conciliacionAudit);
				tmct0ContaPartidasCasadasDAO.save(partidaCasada);
			}

			filas = AccountingHelper.generarFilasConciliacionBancaria(partidas, (String)concepto.get("value"));

			XSSFWorkbook wb = AccountingHelper.generarXLSApunte(filas);

			file = folderOut + titulo;
			FileOutputStream stream = new FileOutputStream(file);
			wb.write(stream);
			wb.close();
			stream.close();

			// TODO - Verificar que se haya colocado en el metodo correcto
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

		}catch (IOException e) {
			LOG.error("Error metodo generarApunteTLM -  " + e.getMessage(), e);
			throw(e);
		}
		catch (Exception e) {
			LOG.error("Error metodo generarApunteTLM -  " + e.getMessage(), e);
			throw(e);
		}

	}

}
