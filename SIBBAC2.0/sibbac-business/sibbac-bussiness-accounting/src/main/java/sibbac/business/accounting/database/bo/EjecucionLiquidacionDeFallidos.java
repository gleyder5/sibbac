package sibbac.business.accounting.database.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;

/**
 * Clase de negocios para la ejecuciÃ³n de plantillas de devengo.
 * 
 * @author Neoris
 *
 */
@Service
public class EjecucionLiquidacionDeFallidos extends AbstractEjecucionPlantilla {

  @Override
  protected void setTiposPlantilla() {
    // TODO Auto-generated method stub

  }

  @Override
  protected List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {
    // TODO Auto-generated method stub
    return null;
  }

  //
  // @Override
  // protected void setTiposPlantilla() {
  // super.tiposPlantilla = new String[] { TipoContaPlantillas.LIQUIDACION_FALLIDOS.getTipo() };
  // }
  //
  // /*
  // * (non-Javadoc)
  // *
  // * @see sibbac.business.accounting.database.bo.AbstractEjecucionPlantilla#
  // * ejecutarPlantillas()
  // */
  // @Override
  // public void ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) {
  //
  // for (Tmct0ContaPlantillas plantilla : plantillas) {
  //
  // // 1. Se ejecutan las querys de cobro y se obtienen los registros
  // List<Object[]> cobros = this.ejecutaQueryCobro(plantilla);
  //
  // if (cobros != null && !cobros.isEmpty()) {
  //
  // for (Object[] elcobro : cobros) {
  //
  // CobroDTO cobro = new CobroDTO(elcobro);
  //
  // // 2. Si se obtienen registros, por cada registro obtenido
  // // se debe ejecutar la query de las Ã³rdenes (QUERY_ORDEN)
  // // para localizar la orden,
  // // el literal <DESC_REFERENCIA DE LA QUERY_COBRO> de la
  // // query de las Ã³rdenes debe sustituirse por el valor
  // // DESC_REFERENCIA de cada registro
  // // obtenido de la query de cobros de forma que:
  // List<Object[]> ordenes = null;
  // if (plantilla.getQuery_orden() != null) {
  // ordenes = this.ejecutaQueryOrden(plantilla, cobro);
  // }
  // if (ordenes != null && !ordenes.isEmpty()) {
  //
  // int i = 0;
  // for (Object[] laOrden : ordenes) {
  //
  // OrdenDTO orden = new OrdenDTO((Date) laOrden[0], (Integer) laOrden[1], (String) laOrden[2],
  // (BigDecimal) laOrden[3]);
  //
  // // Se bloquean los ALC con los que se va a trabajar.
  // List<Object[]> movBloqueados = this.ejecutaQueryBloqueo(plantilla, cobro);
  //
  // int iImporte = 0;
  // for (BigDecimal importeOrden : orden.getImportes()) {
  //
  // String estadoCont = orden.getEstadocont();
  // BigDecimal importeCobro = cobro.getImporte();
  // BigDecimal diferencia = importeOrden.subtract(importeCobro);
  // Boolean isDiferenciaMayorQueToleranciaPlantilla = diferencia
  // .compareTo(plantilla.getTolerancia()) > 0;
  // Boolean isSinDiferencia = BigDecimal.ZERO.equals(diferencia);
  // Boolean isDiferenciaMenorQueToleranciaPlantilla = diferencia
  // .compareTo(plantilla.getTolerancia()) <= 0;
  //
  // // b. Si el campo ESTADOCONT de la orden no es
  // // uno de los que figuran en la plantilla (campo
  // // ESTADO_INICIO de la plantilla)
  // // y ya no hay mÃ¡s ordenes que procesar:
  // if (!plantilla.getEstado_inicio().equals(estadoCont) && ordenes.size() == i) {
  //
  // // i. El registro del cobro se tiene que
  // // pasar a PROCESADO=1 en la tabla
  // // TMCT0_MOVIMIENTO_NORMA43 buscando por el
  // // campo ID_MOV.
  // pasarCobroAProcesado(cobro.getIdMov());
  //
  // // ii. Se debe dar de alta un descuadre en
  // // la tabla de descuadres con el error
  // // â€œEL ESTADO DE LA ORDEN EN SIBBAC
  // // (ESTADOCONT) NO PERMITE CONTABILIZAR EL
  // // COBROâ€�.
  // crearDescuadre(
  // "EL ESTADO DE LA ORDEN EN SIBBAC " + estadoCont
  // + " NO PERMITE CONTABILIZAR EL COBRO",
  // plantilla, cobro, orden, importeOrden);
  //
  // }
  //
  // // c. Si la orden estÃ¡ en el estado correcto
  // // (ESTADOCONT = ESTADO_INICIO de la plantilla),
  // // pero la diferencia entre el IMPORTE de la
  // // query de orden y el IMPORTE de la query de
  // // cobro es superior al tolerance
  // // definido para la cuenta afectada (campo
  // // TOLERANCE de la plantilla):
  // if (plantilla.getEstado_inicio().equals(estadoCont)) {
  //
  // if (isDiferenciaMayorQueToleranciaPlantilla) {
  //
  // // i. El registro del cobro se tiene que
  // // pasar a PROCESADO=1 en la tabla
  // // TMCT0_MOVIMIENTO_NORMA43 buscando por
  // // el campo ID_MOV y
  // pasarCobroAProcesado(cobro.getIdMov());
  //
  // // ii. Se debe dar de alta un descuadre
  // // en la tabla de descuadres con el
  // // error
  // // â€œLA DIFERENCIA ENTRE EL IMPORTE
  // // RECIBIDO (IMPORTE_COBRO) Y EL PDTE.
  // // DE COBRO (IMPORTE_ORDEN) ES SUPERIOR
  // // AL
  // // TOLERANCE DEFINIDO PARA LA CUENTA
  // // (TOLERANCE)â€�.
  // crearDescuadre(
  // "LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO ("
  // + String.valueOf(importeCobro) + ") Y EL PDTE. DE COBRO ("
  // + String.valueOf(importeOrden)
  // + ") ES SUPERIOR AL TOLERANCE DEFINIDO PARA LA CUENTA ("
  // + String.valueOf(plantilla.getTolerancia()) + ")",
  // plantilla, cobro, orden, importeOrden);
  //
  // }
  //
  // }
  //
  // // d. Finalmente, si la orden estÃ¡ en el estado
  // // correcto (ESTADOCONT = campo ESTADO_INICIO de
  // // la plantilla) y el importe no tiene
  // // diferencias o la diferencia es menor o igual
  // // al tolerance (campo TOLERANCE de la
  // // plantilla):
  // if (plantilla.getEstado_inicio().equals(estadoCont)) {
  //
  // if (isSinDiferencia || isDiferenciaMenorQueToleranciaPlantilla) {
  //
  // // i. El registro del cobro se tiene que
  // // pasar av PROCESADO=1 en la tabla
  // // TMCT0_MOVIMIENTO_NORMA43 buscando por
  // // el campo ID_MOV
  // pasarCobroAProcesado(cobro.getIdMov());
  //
  // // ii. Se debe generar el apunte
  // // contable tal cual define la
  // // plantilla.
  // generarApuntesContables(importeOrden, iImporte, orden, cobro, plantilla,
  // movBloqueados);
  //
  // // iii. El importe del apunte se debe
  // // restar del importe pdte. de cobro en
  // // la tabla TMCT0ALC (campo CAMPOde la
  // // plantilla) de esta forma:
  // // 1. Si el IMPORTE de la query de orden
  // // era mayor o igual que el IMPORTE de
  // // la query de cobro se debe restar el
  // // IMPORTE de la query de cobro.
  // if (importeOrden.compareTo(cobro.getImporte()) > 0) {
  // ejecutaQueryRestarMontoCampoAlc(movBloqueados, plantilla.getCampo(),
  // cobro.getImporte());
  // }
  // // 2. Si el IMPORTE de la query de orden
  // // era menor que el IMPORTE de la query
  // // de cobro se debe restar el IMPORTE de
  // // la query de orden.
  // if (importeOrden.compareTo(cobro.getImporte()) <= 0) {
  // ejecutaQueryRestarMontoCampoAlc(movBloqueados, plantilla.getCampo(),
  // importeOrden);
  // }
  //
  // }
  //
  // }
  //
  // iImporte++;
  //
  // }
  // i++;
  //
  // this.ejecutaQueryBloqueo(plantilla, plantilla.getEstado_final(), orden, cobro);
  //
  // }
  //
  // } else { // a. Si no se encuentra una orden relacionada:
  //
  // // i. El registro del cobro se tiene que actualizar a
  // // PROCESADO=1 en la tabla TMCT0_MOVIMIENTO_NORMA43
  // // buscando por el campo ID_MOV.
  // pasarCobroAProcesado(cobro.getIdMov());
  //
  // // ii. Se debe dar de alta un descuadre (en el punto
  // // 2.3.4 se define cÃ³mo se crean los descuadres en la
  // // tabla de descuadres) en la tabla de descuadres con el
  // // error â€œNO SE HA ENCONTRADO ORDEN EN SIBBACâ€�.
  // crearDescuadre("NO SE HA ENCONTRADO RELACIÃ“N EN SIBBAC", plantilla, cobro, null,
  // BigDecimal.ZERO);
  //
  // }
  //
  // }
  //
  // }
  //
  // // Se desbloquean los ALC con los que se trabajÃ³.
  // contaPlantillasDaoImp.executeQuery(plantilla.getQuery_bloqueo(), plantilla.getEstado_final());
  //
  // }
  // }
  //
  // protected void ejecutaQueryRestarMontoCampoAlc(List<Object[]> alcBloqueados, String campo, BigDecimal montoRestar)
  // {
  //
  // if (alcBloqueados != null && alcBloqueados.size() > 0) {
  // for (Object[] alc : alcBloqueados) {
  // AlcDTO alcDto = new AlcDTO(alc);
  // contaPlantillasDaoImp.ejecutaQueryRestarMontoCampoAlc(campo, alcDto.getNuorden(), alcDto.getNbooking(),
  // alcDto.getNucnfclt(), alcDto.getNucnfliq(), montoRestar);
  // }
  //
  // }
  //
  // }
  //
  // private void generarApuntesContables(BigDecimal importeOrden, int iCuenta, OrdenDTO orden, CobroDTO cobro,
  // Tmct0ContaPlantillas plantilla, List<Object[]> movBloqueados) {
  //
  // // Se verifica que la plantilla tenga cuenta al debe
  // // y cuenta al
  // // haber.
  // if (plantilla.isPlantillaConCuentas()) {
  //
  // // Si el importe de la plantilla es 0, no se
  // // generan
  // // apuntes.
  // if (!BigDecimal.ZERO.equals(importeOrden)) {
  //
  // // Se genera y se guarda el apunte al
  // // debe.
  // Integer idApunteDebe = guardarApunteDebe(importeOrden, iCuenta, plantilla, orden, cobro);
  //
  // // Se genera y se guarda el apunte al
  // // haber.
  // Integer idApunteHaber = guardarApunteHaber(importeOrden, iCuenta, plantilla, orden, cobro);
  //
  // BigDecimal diferencia = importeOrden.subtract(cobro.getImporte());
  // if (diferencia != BigDecimal.ZERO) {
  //
  // guardarApunteDiferencia(diferencia, iCuenta, plantilla, orden, cobro);
  //
  // }
  //
  // List<BigInteger> idsFactura = new ArrayList<BigInteger>();
  // // TODO idsFacturas
  //
  // // Se genera y se guarda el detalle del apunte.
  // contaApunteDetalleDaoImp.insert(idApunteDebe, idApunteHaber, importeOrden, idsFactura,
  // plantilla.getGrabaDetalle());
  //
  // }
  //
  // }
  //
  // }
  //
  // /* Genera y guarda los apuntes al Debe */
  // private Integer guardarApunteDebe(BigDecimal importe, int iCuenta, Tmct0ContaPlantillas plantilla, OrdenDTO orden,
  // CobroDTO cobro) {
  //
  // Tmct0ContaApuntes apunteDebe = new Tmct0ContaApuntes();
  // apunteDebe.setCod_plantilla(plantilla.getCodigo());
  // apunteDebe.setTipo_transacc('2');
  // apunteDebe.setCod_comp("31");
  // apunteDebe.setFecha_comprobante(orden.getFecha());
  // apunteDebe.setPeriodo_comprobante(String.valueOf(orden.getPeriodo()));
  // apunteDebe.setTipo_comprobante(plantilla.getTipo_comprobante());
  // apunteDebe.setNum_comprobante(plantilla.getNum_comprobante() + 1);
  // apunteDebe.setCuenta_contable(plantilla.getCuentaDebe(iCuenta));
  // apunteDebe.setAux_contable(plantilla.getCuentaAuxDebe(iCuenta));
  // if ("1021101".equals(apunteDebe.getCuenta_contable())) {
  // apunteDebe.setConcepto(cobro.getDescReferencia());
  // apunteDebe.setImporte((BigDecimal) cobro.getImporte());
  // } else {
  // apunteDebe.setConcepto(orden.getConcepto());
  // apunteDebe.setImporte(importe);
  // }
  //
  // apunteDebe.setAudit_date(new Date());
  // apunteDebe.setAudit_user("SIBBAC");
  // if (importe.signum() == 1) {
  // apunteDebe.setTipo_apunte('D');
  // } else if (importe.signum() == -1) {
  // apunteDebe.setTipo_apunte('H');
  // }
  // apunteDebe.setFichero(plantilla.getFichero());
  // apunteDebe.setGenerado_fichero(0);
  //
  // LOG.trace("Creando apunte: " + apunteDebe.toString());
  // contaApuntesDao.save(apunteDebe);
  //
  // return apunteDebe.getId();
  //
  // }
  //
  // /* Genera y guarda los apuntes al Haber */
  // private Integer guardarApunteHaber(BigDecimal importe, int iCuenta, Tmct0ContaPlantillas plantilla, OrdenDTO orden,
  // CobroDTO cobro) {
  //
  // Tmct0ContaApuntes apunteHaber = new Tmct0ContaApuntes();
  // apunteHaber.setCod_plantilla(plantilla.getCodigo());
  // apunteHaber.setTipo_transacc('2');
  // apunteHaber.setCod_comp("31");
  // apunteHaber.setFecha_comprobante(orden.getFecha());
  // apunteHaber.setPeriodo_comprobante(String.valueOf(orden.getPeriodo()));
  // apunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
  // apunteHaber.setNum_comprobante(plantilla.getNum_comprobante() + 1);
  // apunteHaber.setCuenta_contable(plantilla.getCuentaDebe(iCuenta));
  // apunteHaber.setAux_contable(plantilla.getCuentaAuxDebe(iCuenta));
  //
  // if ("1021101".equals(apunteHaber.getCuenta_contable())) {
  // apunteHaber.setConcepto(cobro.getDescReferencia());
  // apunteHaber.setImporte((BigDecimal) cobro.getImporte());
  // } else {
  // apunteHaber.setConcepto(orden.getConcepto());
  // apunteHaber.setImporte(importe);
  // }
  //
  // apunteHaber.setAudit_date(new Date());
  // apunteHaber.setAudit_user("SIBBAC");
  // if (importe.signum() == 1) {
  // apunteHaber.setTipo_apunte('H');
  // } else if (importe.signum() == -1) {
  // apunteHaber.setTipo_apunte('D');
  // }
  // apunteHaber.setFichero(plantilla.getFichero());
  // apunteHaber.setGenerado_fichero(0);
  //
  // LOG.trace("Creando apunte: " + apunteHaber.toString());
  // contaApuntesDao.save(apunteHaber);
  //
  // return apunteHaber.getId();
  //
  // }
  //
  // /* Genera y guarda los apuntes al Haber */
  // private void guardarApunteDiferencia(BigDecimal diferencia, int iCuenta, Tmct0ContaPlantillas plantilla,
  // OrdenDTO orden, CobroDTO cobro) {
  //
  // Tmct0ContaApuntes apunteHaber = new Tmct0ContaApuntes();
  // apunteHaber.setCod_plantilla(plantilla.getCodigo());
  // apunteHaber.setTipo_transacc('2');
  // apunteHaber.setCod_comp("31");
  // apunteHaber.setFecha_comprobante(orden.getFecha());
  // apunteHaber.setPeriodo_comprobante(String.valueOf(orden.getPeriodo()));
  // apunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
  // apunteHaber.setNum_comprobante(plantilla.getNum_comprobante() + 1);
  // apunteHaber.setCuenta_contable(plantilla.getCuentaDebe(iCuenta));
  // apunteHaber.setAux_contable(null);
  // apunteHaber.setConcepto(orden.getConcepto());
  // apunteHaber.setImporte(diferencia);
  // apunteHaber.setAudit_date(new Date());
  // apunteHaber.setAudit_user("SIBBAC");
  // if (diferencia.signum() == 1) {
  // apunteHaber.setTipo_apunte('D');
  // } else if (diferencia.signum() == -1) {
  // apunteHaber.setTipo_apunte('H');
  // }
  // apunteHaber.setFichero(plantilla.getFichero());
  // apunteHaber.setGenerado_fichero(0);
  //
  // LOG.trace("Creando apunte: " + apunteHaber.toString());
  // contaApuntesDao.save(apunteHaber);
  //
  // }
  //
  // @Override
  // public void ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) {
  // // TODO Auto-generated method stub
  //
  // }

  @Override
  public void anadirPlantillas(List<List<Tmct0ContaPlantillasDTO>> lPlantillas) {
    // TODO Auto-generated method stub

  }

}
