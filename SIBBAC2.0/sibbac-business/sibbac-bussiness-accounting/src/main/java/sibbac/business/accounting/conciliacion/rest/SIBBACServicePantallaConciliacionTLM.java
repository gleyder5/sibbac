package sibbac.business.accounting.conciliacion.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0PantallaConciliacionTLMBo;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * @author lucio.vilar
 *
 */
@SIBBACService
public class SIBBACServicePantallaConciliacionTLM implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServicePantallaConciliacionTLM.class);

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	@Autowired
	private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;

	@Autowired
	private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;

	@Autowired
	private Tmct0PantallaConciliacionTLMBo tmct0PantallaConciliacionTLMBo;

	/**
	 * The Enum ComandoConciliacionACC.
	 */
	private enum ComandoConciliacionTLM {

		GENERAR_APUNTE("generarApuntes"),

		SIN_COMANDO("");

		/** The command. */
		private String command;

		private ComandoConciliacionTLM(String comando) {
			this.command = comando;
		}

		/**
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}	

	/* (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
	 */
	@Override
	public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();

		ComandoConciliacionTLM comando = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServicePantallaConciliacionTLM");

			switch (comando) {

			case GENERAR_APUNTE:
				result.setResultados(exportarAExcel(paramsObjects));

			default:
				result.setError("An action must be set. Valid actions are: " + comando);
			}
		} catch (Exception e) {
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}
		LOG.debug("Salio del servicio SIBBACServicePantallaConciliacionTLM sin errores");

		return result;
	}

	private Map<String, Object> exportarAExcel(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// Se valida que no haya otros usuarios cargando ficheros Excel TLM.
			Tmct0SibbacConstantes semafCargaExcelTLM = this.tmct0SibbacConstantesDao.findByClaveAndValor(
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave(),
					SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado());
			if (semafCargaExcelTLM != null) {
				result.put("status", "KOSemaforo");
				result.put("error", "Un nuevo excel está siendo cargado por otro usuario. Es posible que sus datos estén desactualizados, por favor vuelva a realizar la consulta en unos segundos.");
				LOG.error("No se pueden generar apuntes ya que hay usuarios cargando ficheros TLM.");
				return result;
			} 

			tmct0PantallaConciliacionTLMBo.generarApunteTLM(paramsObjects);

			result.put("status", "OK");
			
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

		}catch (Exception e) {
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}
		return result;
	}

	private ComandoConciliacionTLM getCommand(String action) {
		ComandoConciliacionTLM[] comandos = ComandoConciliacionTLM.values();
		ComandoConciliacionTLM result = null;

		for (int i = 0; i < comandos.length; i++) {
			if (comandos[i].getCommand().equalsIgnoreCase(action)) {
				result = comandos[i];
			}
		}

		if (result == null) {
			result = ComandoConciliacionTLM.SIN_COMANDO;
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	/* (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List<String> getFields() {
		return null;
	}

}
