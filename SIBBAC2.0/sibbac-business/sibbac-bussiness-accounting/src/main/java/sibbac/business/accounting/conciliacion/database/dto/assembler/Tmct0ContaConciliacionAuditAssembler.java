package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConciliacionAuditDaoImp;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaConciliacionAuditDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0FiltroContaConciliacionAuditDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.wrappers.helper.Helper;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.dao.Tmct0UsersDaoImp;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla
 * TMCT0_CONTA_CONCILIACION_AUDIT
 * 
 * @author lucio.vilar
 *
 */
@Service
public class Tmct0ContaConciliacionAuditAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(Tmct0ContaConciliacionAuditAssembler.class);

	@Autowired
	Tmct0ContaConciliacionAuditDaoImp contaConciliacionAuditDaoImp;

	@Autowired
	Tmct0UsersDaoImp tmct0UsersDaoImp;
	
	@Autowired
	private Tmct0UsersDao tmct0UsersDao;

	public Tmct0ContaConciliacionAuditAssembler() {
		super();
	}

	/**
	 * Realiza la conversion entre la entidad y el dto
	 * 
	 * @param datosFacturasManuales
	 *            -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0ContaConciliacionAuditDTO getTmct0ContaConciliacionAuditDTO(Tmct0ContaConciliacionAudit tmct0ContaConciliacionAudit) throws ParseException {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaConciliacionAuditDTO");
		Tmct0ContaConciliacionAuditDTO tmct0ContaConciliacionAuditDTO = new Tmct0ContaConciliacionAuditDTO();

		try {
			tmct0ContaConciliacionAuditDTO.setId(tmct0ContaConciliacionAudit.getId());
			tmct0ContaConciliacionAuditDTO.setFechaAudioria(tmct0ContaConciliacionAudit.getAuditDate());
			tmct0ContaConciliacionAuditDTO.setUsuario((String)tmct0ContaConciliacionAudit.getAuditUser());
			// Se muestra nombre y apellido en la grilla
			if (StringUtils.isNotBlank(tmct0ContaConciliacionAudit.getAuditUser())) {
				tmct0ContaConciliacionAuditDTO.setUsuario(
						this.tmct0UsersDao.findUserNameLastNameConcatByUserName(tmct0ContaConciliacionAudit.getAuditUser()));
			}
			tmct0ContaConciliacionAuditDTO.setOrigen(tmct0ContaConciliacionAudit.getOrigen());
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaConciliacionAuditDTO -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FINAL - ASSEMBLER - getTmct0ContaConciliacionAuditDTO");
		return tmct0ContaConciliacionAuditDTO;
	}

	/**
	 * Realiza la conversion entre la entidad y el dto
	 * 
	 * @param datosFacturasManuales
	 *            -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public Tmct0FiltroContaConciliacionAuditDTO getTmctFiltro0ContaConciliacionAuditDTO(Map<String, Object> paramsObjects) throws Exception {
		LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesTablaDTO");
		Tmct0FiltroContaConciliacionAuditDTO filtro = new Tmct0FiltroContaConciliacionAuditDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		Date fechaDesde = null;
		Date fechaHasta = null;
		List<Date> listFechas = new ArrayList<Date>();

		try {

			// Al haber fecha desde y hasta se consigue un listado de las fechas comprendidas para optimizar la consulta a la BBDD
			if (!"".equals(paramsObjects.get("fechaDesde"))) {
				fechaDesde = formatter.parse(String.valueOf(paramsObjects.get("fechaDesde")));
			}
			if (!"".equals(paramsObjects.get("fechaHasta"))) {
				fechaHasta = formatter.parse(String.valueOf(paramsObjects.get("fechaHasta")));
			}

			if (null == fechaDesde) {
				listFechas = null;
			} else {
				listFechas = Helper.separarFechas(fechaDesde,fechaHasta);
			}

			if (null != listFechas && !listFechas.isEmpty()) {
				filtro.setFechas(listFechas);
			}

			if (null != paramsObjects.get("usuario") && !paramsObjects.get("usuario").equals("")) {
				filtro.setUser((String)((LinkedHashMap)paramsObjects.get("usuario")).get("key"));
				
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FacturasManualesTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FINAL - ASSEMBLER - getTmct0FacturasManualesTablaDTO");
		return filtro;
	}

}
