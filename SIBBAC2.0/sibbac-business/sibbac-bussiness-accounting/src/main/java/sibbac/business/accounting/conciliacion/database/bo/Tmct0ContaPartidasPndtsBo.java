package sibbac.business.accounting.conciliacion.database.bo;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDaoImpl;
import sibbac.business.accounting.conciliacion.database.dto.DatosColumnaPartidasPndtsDTO;
import sibbac.business.accounting.conciliacion.database.dto.ErroresExcelTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.ListasProcesoExcelTLMDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum.EstadoContaPartidasPndtsEnum;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.common.ExcelProcessingHelper;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

/**
 *	Bo para conta partidas pendientes. 
 */
@Service
public class Tmct0ContaPartidasPndtsBo extends AbstractBo<Tmct0ContaPartidasPndts, Integer, Tmct0ContaPartidasPndtsDao> {

	private SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

	@Autowired
	private Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;

	@Autowired
	private Tmct0ContaPartidasPndtsDaoImpl tmct0ContaPartidasPndtsDaoImpl;

	@Autowired
	private Tmct0cfgBo configBo;
	@Value("${sibbac20.application.name}")
	private String sibbac20ApplicationName;


	public ListasProcesoExcelTLMDTO convertExcelRowsInEntityList(
			List<Row> rows, Map<String, DatosColumnaPartidasPndtsDTO> mapDatosColPartPndtsDTO,
			String userName, List<String> listaGINsPartidasPndts) throws Exception {

		ListasProcesoExcelTLMDTO listasProcesoExcelTLMDTO = new ListasProcesoExcelTLMDTO();
		List<Tmct0ContaPartidasPndts> listaPartidasPendientes = new ArrayList<Tmct0ContaPartidasPndts>();
		Date auditDate = new Date();
		List<ErroresExcelTLMDTO> listaErrsExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>(); 
		DatosColumnaPartidasPndtsDTO datosColPartDTO = null;
		/*Se guardan  los tipos de mov en una pequeña lista */
		final List<Tmct0cfg> cfgs = configBo.findByAplicationAndProcess(sibbac20ApplicationName,Constantes.PROCCESS_NAME );

		long inicioGenerarEntidadesTLM = System.currentTimeMillis();

		try {
			int rowSize = rows.size();
			for(int i=1;i<rowSize;i++){
				Row row = rows.get(i);
				if(!ExcelProcessingHelper.getCellValueTLM(row.getCell(0)).isEmpty()){
					Iterator<Cell> celdas = row.cellIterator();
					Tmct0ContaPartidasPndts tmct0ContaPartidasPndts = new Tmct0ContaPartidasPndts();
					tmct0ContaPartidasPndts.setAuditUser(userName);
					tmct0ContaPartidasPndts.setAuditDate(auditDate);
					tmct0ContaPartidasPndts.setEstado(
							PantallaConciliacionEnum.EstadoContaPartidasPndtsEnum.NO_CASADA.getEstadoPartidaPndte());
					while (celdas.hasNext()) {
						int columnIndex = celdas.next().getColumnIndex();
						Cell celda = row.getCell(columnIndex);
						datosColPartDTO = mapDatosColPartPndtsDTO.get(String.valueOf(columnIndex));
						if (datosColPartDTO != null) {
							switch (datosColPartDTO.getName()) {
							case Constantes.CONTA_CAB_EXC_AUX_BANCARIO:
								// Auxiliar bancario
								String mensaje0 = datosColPartDTO.checkMensajeTipoDatoByCadena(
										AccountingHelper.getAuxiliarBancarioFromAccount(
												ExcelProcessingHelper.getCellValueTLM(celda)));
								if (StringUtils.isNotBlank(mensaje0)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje0)); 
								} else {
									tmct0ContaPartidasPndts.setAuxBancario(
											AccountingHelper.getAuxiliarBancarioFromAccount(
													ExcelProcessingHelper.getCellValueTLM(celda)));
								}
								break;
							case Constantes.CONTA_CAB_EXC_BOOKING_DATE:
								// Booking Date
								// Valida Booking Date.
								String mensaje1 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje1)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje1)); 
								} else {
									tmct0ContaPartidasPndts.setBookingDate(
											this.formatter.parse(ExcelProcessingHelper.getCellValueTLM(celda)));
								}
								// Esta validacion restrictiva de booking Date pasa a estar cubierta por una validacion NO-restrictiva.
								// ErroresExcelTLMDTO errBDExcelTLMDTO = this.validarBookingDate(
								//		tmct0ContaPartidasPndts, row, columnIndex, 
								//		userName, maxAuditDate, maxBookingDateBBDD);
								// if (errBDExcelTLMDTO != null) {
								//	 listaErrsExcelTLMDTO.add(errBDExcelTLMDTO); 
								// }
								break;
							case Constantes.CONTA_CAB_EXC_VALUE_DATE:
								// Value Date.
								String mensaje2 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje2)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje2)); 
								} else {
									tmct0ContaPartidasPndts.setValueDate(
											this.formatter.parse(ExcelProcessingHelper.getCellValueTLM(celda)));
								}
								break;
							case Constantes.CONTA_CAB_EXC_CURRENCY:
								// Currency.
								String mensaje3 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje3)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje3)); 
								} else {
									tmct0ContaPartidasPndts.setCurrency(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_TIPO:
								// Item Type

								String mensaje5 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje5)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje5)); 
								} else {
									tmct0ContaPartidasPndts.setTipo(AccountingHelper.getTextTipoMovimiento(cfgs,ExcelProcessingHelper.getCellValueTLM(celda)));
								}
								break;
							case Constantes.CONTA_CAB_EXC_COMENTARIO:
								// Comment
								String mensaje6 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje6)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje6)); 
								} else {
									tmct0ContaPartidasPndts.setComentario(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_DEPARTAMENTO:
								// Departament
								String mensaje7 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje7)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje7)); 
								} else {
									tmct0ContaPartidasPndts.setDepartamento(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_REFERENCIA2:
								// Booking Text 2
								String mensaje8 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje8)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje8)); 
								} else {
									tmct0ContaPartidasPndts.setReferencia2(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_NUM_DOCUMENTO:
								// Our Reference 1
								String mensaje9 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje9)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje9)); 
								} else {
									tmct0ContaPartidasPndts.setNumDocumento(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_NUM_DOCUMENTO2:
								// Their Reference 2
								String mensaje10 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje10)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje10)); 
								} else {
									tmct0ContaPartidasPndts.setNumDocumento2(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_REFERENCIA:
								// Booking Text 1
								String mensaje11 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje11)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje11)); 
								} else {
									tmct0ContaPartidasPndts.setReferencia(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_GIN:
								// GIN
								String mensaje15 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje15)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje15)); 
								} else {
									tmct0ContaPartidasPndts.setGin(
											new BigInteger(ExcelProcessingHelper.getCellValueTLM(celda)));
								}
								break;
							case Constantes.CONTA_CAB_EXC_COMENTARIO_USUARIO:
								// Comment User
								String mensaje16 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje16)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje16)); 
								} else {
									tmct0ContaPartidasPndts.setComentarioUsuario(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							case Constantes.CONTA_CAB_EXC_IMPORTE:
								// Importe Signo
								String mensaje19 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje19)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje19)); 
								} else {
									tmct0ContaPartidasPndts.setImporte(
											FormatDataUtils.convertStringToBigDecimalDotAsDecimal(ExcelProcessingHelper.getCellValueTLM(celda)));
								}
								break;
							case Constantes.CONTA_CAB_EXC_ANTIGUEDAD:
								// Antiguedad
								String mensaje21 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
								if (StringUtils.isNotBlank(mensaje21)) {
									listaErrsExcelTLMDTO.add(this.getErroresExcelDTO(row, columnIndex, mensaje21)); 
								} else {
									tmct0ContaPartidasPndts.setAntiguedad(ExcelProcessingHelper.getCellValueTLM(celda));
								}
								break;
							default:
								break;
							}
						}
					}
				
					// Si el GIN de la partida se encuentra en la lista de GINs con partidas casadas
					// no se agrega a la lista de persistencia.
					if (CollectionUtils.isEmpty(listaGINsPartidasPndts) 
							|| (CollectionUtils.isNotEmpty(listaGINsPartidasPndts) && !listaGINsPartidasPndts.contains(tmct0ContaPartidasPndts.getGin()))) {
						listaPartidasPendientes.add(tmct0ContaPartidasPndts);					
					}
				}
			}
			listasProcesoExcelTLMDTO.setListaErrsExcelTLMDTO(listaErrsExcelTLMDTO);
			listasProcesoExcelTLMDTO.setListaPartidasPendientes(listaPartidasPendientes);
		} catch (Exception e) {
			LOG.error("Error metodo convertExcelRowsInEntityList -  " + e.getMessage(), e);
			throw(e);
		}
		long finGenerarEntidadesTLM = System.currentTimeMillis();
		LOG.info("Tiempo de generacion de entidades TLM (segs): " + (finGenerarEntidadesTLM - inicioGenerarEntidadesTLM)/1000);

		return listasProcesoExcelTLMDTO;
	}

	public Integer validateBookingDate(List<Row> rows, Map<String, DatosColumnaPartidasPndtsDTO> mapDatosColPartPndtsDTO) throws Exception {
		Integer result = null;
		Date maxBookingDateBBDD = this.tmct0ContaPartidasPndtsDao.getMaxBookingDate();
		Date maxBookingDateExcel = null;
		
		// Si la fecha de booking Date es null implica que no hay registros en la tabla.
		// Se devuelve 1 para que se pueda continuar con la carga.
		if (maxBookingDateBBDD == null) {
			return 1;
		}
		
		try {
			int rowSize = rows.size();
			for(int i=1;i<rowSize;i++){
				Row row = rows.get(i);
				DatosColumnaPartidasPndtsDTO datosColPartDTO = mapDatosColPartPndtsDTO.get(String.valueOf(1));
				Cell celda = row.getCell(1);
				// Booking Date
				// Valida Booking Date.
				String mensaje1 = datosColPartDTO.checkMensajeTipoDatoByCelda(celda);
				if (StringUtils.isBlank(mensaje1)) {
					Date aux = this.formatter.parse(ExcelProcessingHelper.getCellValueTLM(celda));
					if (null == maxBookingDateExcel) {
						maxBookingDateExcel = aux;
					} else if (maxBookingDateExcel.before(aux)) {
						maxBookingDateExcel = aux;
					}
				}
			}
			if (null != maxBookingDateExcel) {
				if (maxBookingDateExcel.after(maxBookingDateBBDD)) {
					result = 1;
				} else if (maxBookingDateExcel.before(maxBookingDateBBDD)) {
					result = -1;
				} else {
					result = 0;
				}
			}
		} catch (Exception e) {
			result = -1;
		}
		
		return result;
	}
	
	/**
	 *	Se persisten datos de TLM en la entidad Tmct0ContaPartidasPndts.
	 *	@param listasProcesoExcelTLMDTO
	 *	@return List<ErroresExcelTLMDTO>
	 *	@throws SIBBACBusinessException 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public List<ErroresExcelTLMDTO> persistTLMData(
			ListasProcesoExcelTLMDTO listasProcesoExcelTLMDTO) throws SIBBACBusinessException {

		long inicioPersistencia = System.currentTimeMillis();
		try {
			// Si hay info del Excel y no hay errores se procede a la persistencia.
			if (CollectionUtils.isNotEmpty(listasProcesoExcelTLMDTO.getListaPartidasPendientes()) 
					&& CollectionUtils.isEmpty(listasProcesoExcelTLMDTO.getListaErrsExcelTLMDTO())) {
				// Se borran los registros anteriores que tienen partidas casadas.
//				this.tmct0ContaPartidasPndtsDao.deleteByEstado(EstadoContaPartidasPndtsEnum.CASADA.getEstadoPartidaPndte());
				// Se borran todos los registros sin importar estado.	
				this.tmct0ContaPartidasPndtsDao.deleteAll();
				
				// Se persisten registros nuevos.
				//			this.tmct0ContaPartidasPndtsDaoImpl.insertsByNativeQuery(listasProcesoExcelTLMDTO.getListaPartidasPendientes());
				this.tmct0ContaPartidasPndtsDaoImpl.bulkSave(listasProcesoExcelTLMDTO.getListaPartidasPendientes());
				//			this.dao.save(listasProcesoExcelTLMDTO.getListaPartidasPendientes());
			}
		} catch (Exception e) {
			LOG.error("Error metodo persistTLMData -  " + e.getMessage(), e);
			throw(e);
		}
		long finPersistencia = System.currentTimeMillis();
		LOG.info("Tiempo de persistencia (segs): " + (finPersistencia - inicioPersistencia)/1000);
		return listasProcesoExcelTLMDTO.getListaErrsExcelTLMDTO();
	}

	/**
	 *	Validacion del booking date.
	 *	Devuelve un DTO con informacion del error fila y columna en caso de haberlo.
	 *	
	 *	Esta validacion restrictiva pasa a estar cubierta por una validacion NO-restrictiva.
	 *	
	 *	@param tmct0ContaPartidasPndts
	 *	@param row
	 *	@param columnIndex
	 *	@param userName
	 *	@param maxAuditDate
	 *	@param maxBookingDateBBDD
	 *	@return ErroresExcelTLMDTO  
	 */
	@Deprecated
	private ErroresExcelTLMDTO validarBookingDate(
			Tmct0ContaPartidasPndts tmct0ContaPartidasPndts, Row row, int columnIndex, 
			String userName, Date maxAuditDate, Date maxBookingDateBBDD) {
		// Si la fecha de la columna Booking Date del fichero Excel es igual o inferior a la fecha BOOKING_DATE 
		// mas actual cargada en la tabla TMCT0_CONTA_PARTIDAS_PNDTS se le mostrará un warning al usuario 
		// informándole que no puede realizar la carga y se cancelará
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		ErroresExcelTLMDTO errExcelTLMDTO = null;
		try {
			if (maxBookingDateBBDD != null && tmct0ContaPartidasPndts.getBookingDate().compareTo(maxBookingDateBBDD) <= 0) {
				errExcelTLMDTO = new ErroresExcelTLMDTO();
				errExcelTLMDTO.setNroFila(String.valueOf(row.getRowNum() + 1));
				errExcelTLMDTO.setNroColumna(String.valueOf(columnIndex + 1));
				errExcelTLMDTO.setDescripcionError(
						"El fichero de TLM que se intenta cargar es de una fecha Booking Date "
								+ "igual o anterior a la cargada actualmente: " + formatter.format(maxAuditDate) + " - " + userName);
			}
		} catch (Exception e) {
			LOG.error("Error metodo validarBookingDate -  " + e.getMessage(), e);
			throw(e);
		}
		return errExcelTLMDTO;
	}

	/**
	 *	Se devuelve representacion de error del tipo de dato convertido.
	 *	@param row
	 *	@param columnIndex
	 *	@param mensaje
	 *	@return ErroresExcelTLMDTO  	 
	 */
	private ErroresExcelTLMDTO getErroresExcelDTO(Row row, int columnIndex, String mensaje) {
		ErroresExcelTLMDTO errExcelTLMDTO = new ErroresExcelTLMDTO();
		try {
			errExcelTLMDTO.setNroFila(String.valueOf(row.getRowNum() + 1));
			errExcelTLMDTO.setNroColumna(String.valueOf(columnIndex + 1));
			errExcelTLMDTO.setDescripcionError(mensaje);
		} catch (Exception e) {
			LOG.error("Error metodo getErroresExcelDTO -  " + e.getMessage(), e);
			throw(e);
		}
		return errExcelTLMDTO;		
	}

	/**
	 *	Devuelve DTO para combos segun el nombre de columna de tabla que se le pase (valores distintos).
	 *	@param columnName
	 *	@return List<SelectDTO> 
	 *	@throws SIBBACBusinessException 
	 */
	public List<SelectDTO> getDTOCombosByColumnName(String columnName) throws SIBBACBusinessException {
		List<SelectDTO> catalogoList = new ArrayList<SelectDTO>();
		try {
			List<String> campos = this.tmct0ContaPartidasPndtsDaoImpl.getDistinctValuesByColumnName(columnName);
			if (CollectionUtils.isNotEmpty(campos)) {
				for (String campo : campos) {
					catalogoList.add(new SelectDTO(String.valueOf(campo).trim(), String.valueOf(campo).trim()));
				}
			}
		} catch (Exception e) {
			LOG.error("Error metodo getDTOCombosByColumnName -  " + e.getMessage(), e);
			throw(e);
		}
		return catalogoList;
	}

	/**
	 *	Obtener datos de la tabla segun nombre de tabla y columna.
	 *	@param colName
	 *	@param tableName
	 *	@return DatosColumnaPartidasPndtsDTO: nombre de columna, nombre de tabla, tipo de columna y longitud 
	 */
	public DatosColumnaPartidasPndtsDTO getDTODatosColsPartPndtsByTbNameAndColName(
			String colName, String tableName) {
		DatosColumnaPartidasPndtsDTO dcpPndts = null;
		try {
			List<Object[]> columnsData = this.tmct0ContaPartidasPndtsDaoImpl.findColumnDataByColumnName(
					colName, tableName);
			if (CollectionUtils.isNotEmpty(columnsData)) {
				dcpPndts = new DatosColumnaPartidasPndtsDTO();
				Object[] camposTabla = columnsData.get(0);
				dcpPndts.setName(
						(String) camposTabla[0] != null ? ((String) camposTabla[0]).trim(): (String) camposTabla[0]);
				dcpPndts.setTbName(
						(String) camposTabla[1] != null ? ((String) camposTabla[1]).trim(): (String) camposTabla[1]);
				dcpPndts.setColtype(
						(String) camposTabla[2] != null ? ((String) camposTabla[2]).trim(): (String) camposTabla[2]);
				dcpPndts.setLength((Short) camposTabla[3]);
			}
		} catch (Exception e) {
			LOG.error("Error metodo getDTODatosColsPartPndtsByTbNameAndColName -  " + e.getMessage(), e);
			throw(e);
		}
		return dcpPndts;
	}
}
