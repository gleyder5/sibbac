package sibbac.business.accounting.conciliacion.database.dao;

import java.math.BigInteger;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ContaConceptoDAO extends JpaRepository<Tmct0ContaConcepto, BigInteger> {

	@Query("SELECT concepto FROM Tmct0ContaConcepto concepto order by concepto.concepto asc")
	public Set<Tmct0ContaConcepto> findConceptosOrderByConcepto();
}
