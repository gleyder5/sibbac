package sibbac.business.accounting.database.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDaoImp;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.ResultValidAndContenidoExcelDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.enums.TiposApuntesManualesExcelEnum;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.threads.EjecucionFicheroIndividualRunnable;
import sibbac.business.accounting.threads.EjecucionPlantillaIndividualRunnable;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.fallidos.utils.FileUtils;
import sibbac.business.wrappers.common.ExcelProcessingHelper;
import sibbac.business.wrappers.common.FileHelper;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;

/**
 * Clase de negocios para la ejecucion de plantillas de Apuntes manuales.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EjecucionApuntesManuales extends AbstractEjecucionPlantilla {

  @Autowired
  Tmct0ContaPlantillasDao tmct0ContaPlantillasDao;

  @Autowired
  Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  Tmct0ContaApuntesBo tmct0ContaApuntesBo;

  @Autowired
  Tmct0Norma43MovimientoBo tmct0Norma43MovimientoBo;
  
  @Autowired
  Tmct0ContaPlantillasDaoImp tmct0ContaPlantillasDaoImp;
  
  @Autowired
  EjecucionLiquidacionesTotales ejecucionLiquidacionesTotales;
  
  @Autowired
  EjecucionLiquidacionesParciales ejecucionLiquidacionesParciales;

  @Autowired
  EjecucionDevengosApuntesManuales ejecucionDevengosApuntesManuales;
  
  @Autowired
  EjecucionCobrosApuntesManuales ejecucionCobrosApuntesManuales;
  
  @Autowired
  EjecucionLiquidacionesApuntesManuales ejecucionLiquidacionesApuntesManuales;
  
  @Autowired
  private SendMail sendMail;

  @Autowired
  Tmct0alcDao tmct0alcDao;

  @Value("${accounting.contabilidad.folder.in}")
  private String folderContabilidadIn;

  @Value("${accounting.contabilidad.folder.in.tratados}")
  private String folderContabilidadInTratados;
  
  @Value("${sibbac.accounting.destinatario.mail.apuntes.manuales}")
  private String destinatario;
  
  @Value("${sibbac.accounting.subject.mail.apuntes.manuales}")
  private String subject;
  
  @Value("${sibbac.accounting.body1.mail.apuntes.manuales}")
  private String body1;
  
  @Value("${sibbac.accounting.body2.mail.apuntes.manuales}")
  private String body2;
  
  @Value("${sibbac.accounting.body3.mail.apuntes.manuales}")
  private String body3;
  

  /* Lista con los threads de inserts generados */
  public List<EjecucionFicheroIndividualRunnable> threadsEjecucionFicheroIndividual = new ArrayList<EjecucionFicheroIndividualRunnable>();

  /* True si algún thread falló */
  public Boolean threadEjecucionFicheroIndividualError = false;

  private final static String APUNTE_MANUAL = "APUNTE MANUAL";

  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

  /**
   * Ejecucion de plantillas Apuntes manuales.
   * 
   * @param plantillas plantillas
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Override
  protected List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException {

    // Se buscan ficheros *.XLSX
    List<File> files = FileHelper.getListaArchivosEnDirectorioByExtension(folderContabilidadIn, "XLSX");

    // Se filtra por tipo de fichero.
    List<File> filesTipoSaldo = ExcelProcessingHelper.desdoblarFicherosPorTipo(files, "apunte_saldo0");
    List<File> filesTipoSIBBAC = ExcelProcessingHelper.desdoblarFicherosPorTipo(files, "apunte_sibbac");

    if (CollectionUtils.isNotEmpty(filesTipoSaldo)) {
      ExecutorService executorSaldo = Executors.newFixedThreadPool(filesTipoSaldo.size());
      for (File fileTipoSaldo : filesTipoSaldo) {
        EjecucionFicheroIndividualRunnable runnable = new EjecucionFicheroIndividualRunnable(fileTipoSaldo, this);
        executorSaldo.execute(runnable);
      }

      try {
        executorSaldo.shutdown();
        executorSaldo.awaitTermination(30, TimeUnit.MINUTES);
      } catch (InterruptedException e) {
        Loggers.logErrorAccounting(plantillas.get(0).getCodigo() != null ? plantillas.get(0).getCodigo() : null,
                                   ConstantesError.ERROR_EJECUCION_HILOS.getValue());
        throw new EjecucionPlantillaException("ERROR ejecutarPlantillas");
      }
    }

    if (CollectionUtils.isNotEmpty(filesTipoSIBBAC)) {
      ExecutorService executorSIBBAC = Executors.newFixedThreadPool(filesTipoSIBBAC.size());
      for (File fileTipoSIBBAC : filesTipoSIBBAC) {
        EjecucionFicheroIndividualRunnable runnable = new EjecucionFicheroIndividualRunnable(fileTipoSIBBAC, this);
        executorSIBBAC.execute(runnable);
      }

      try {
        executorSIBBAC.shutdown();
        executorSIBBAC.awaitTermination(2, TimeUnit.HOURS);
      } catch (InterruptedException e) {
        Loggers.logErrorAccounting(plantillas.get(0).getCodigo() != null ? plantillas.get(0).getCodigo() : null,
                                   ConstantesError.ERROR_EJECUCION_HILOS.getValue());
        throw new EjecucionPlantillaException("ERROR ejecutarPlantillas");
      }
    }

    return plantillasSinBalance;
  }

  /**
   * Generacion de apuntes contables.
   * 
   * @param contenidoYValidExcel contenidoYValidExcel
   * @param tmct0CPlantillaManual tmct0CPlantilla
   * @return boolean boolean
   * @throws ParseException ParseException
   * @throws EjecucionPlantillaException
   */
  private boolean generarApuntesContables(ResultValidAndContenidoExcelDTO contenidoYValidExcel,
                                          Tmct0ContaPlantillas tmct0CPlantillaManual) throws ParseException,
                                                                               EjecucionPlantillaException {

    List<ApuntesManualesExcelDTO> listaApuntesManualesExcelDTO = contenidoYValidExcel.getListaApuntesManualesExcelDTO();
    List<Tmct0ContaApuntes> listaTmct0ContaApuntes = new ArrayList<Tmct0ContaApuntes>();
    Tmct0ContaPlantillas plantillaEspecifica = null;

    if (CollectionUtils.isEmpty(contenidoYValidExcel.getListaErrores())
    		&& CollectionUtils.isNotEmpty(listaApuntesManualesExcelDTO)) {

    	// Lista que contendra los ALCs necesarios para la ejecucion de la plantilla por tipo
    	List<ApuntesManualesExcelDTO> amExcelDTOProcesoALCs = new ArrayList<ApuntesManualesExcelDTO>();
    	
    	for (int i = 0; i < listaApuntesManualesExcelDTO.size(); i++) {
    		ApuntesManualesExcelDTO amExcelDTO = listaApuntesManualesExcelDTO.get(i);

    		if (amExcelDTO.isExcelTipoApunteSaldo()) {
    			listaTmct0ContaApuntes.addAll(this.getApuntesContablesTipoSaldo(amExcelDTO, tmct0CPlantillaManual));
    		} else if (amExcelDTO.isExcelTipoApunteSIBBAC()) {

    			// Al procesar la plantilla se debe leer la Excel y por cada cabecera leida se
    			// debe ejecutar la plantilla que indica la segunda columna
    			// de la cabecera con todas las ordenes del detalle asociado, de forma que:
    			plantillaEspecifica = this.contaPlantillasDao.findByCodigo(amExcelDTO.getSibbacCodigoPlantilla());

    			if (plantillaEspecifica == null) {
    				Loggers.logErrorAccounting(amExcelDTO.getSibbacCodigoPlantilla(),
    						ConstantesError.ERROR_SIN_PLANTILLA.getValue());
    				throw new EjecucionPlantillaException();
    			}

    			Loggers.logDebugInicioFinAccounting(plantillaEspecifica != null ? plantillaEspecifica.getCodigo() : null, true);

    			/////////////////////////////////////////////////////////////////
    			// PROCESAMIENTO DE PLANTILLAS SEGUN TIPO Y CABECERA DEL EXCEL //	
    			/////////////////////////////////////////////////////////////////
    			if (listaApuntesManualesExcelDTO.size() > i + 1) {
					String codigoPlantillaNext = listaApuntesManualesExcelDTO.get(i+1).getSibbacCodigoPlantilla();
					if (codigoPlantillaNext.equals(amExcelDTO.getSibbacCodigoPlantilla())) {
						// La plantilla siguiente es del mismo tipo
						// Añadir elemento actual a la lista amExcelDTO
						amExcelDTOProcesoALCs.add(amExcelDTO);
					} else {
						// La plantilla siguiente es de distinto tipo
						// Añadir elemento actual a la lista amExcelDTO
						amExcelDTOProcesoALCs.add(amExcelDTO);
						// Llama al método que ejecuta la plantilla pasando la lista de amExcelDTO
						// y la plantilla.
						this.ejecutarPlantillaIndividualALCs(plantillaEspecifica, amExcelDTOProcesoALCs);
						// Se vacia lista.
						amExcelDTOProcesoALCs = new ArrayList<ApuntesManualesExcelDTO>(); 
					}
				} else {
					// Ultima plantilla a procesar 
					// Añadir elemento actual a la lista amExcelDTO
					amExcelDTOProcesoALCs.add(amExcelDTO);
					// Llama al método que ejecuta la plantilla pasando la lista de amExcelDTO
					// y la plantilla.
					this.ejecutarPlantillaIndividualALCs(plantillaEspecifica, amExcelDTOProcesoALCs);
					// Se vacia lista.
					amExcelDTOProcesoALCs = new ArrayList<ApuntesManualesExcelDTO>();
				}
    			Loggers.logDebugInicioFinAccounting(plantillaEspecifica != null ? plantillaEspecifica.getCodigo() : null, false);
    		}
    	}
    	
    	// Se persisten los apuntes contables necesarios
    	// para tipo APUNTE SIBBAC en el caso que los haya.
    	if (CollectionUtils.isNotEmpty(listaTmct0ContaApuntes) 
    			&& CollectionUtils.isNotEmpty(listaApuntesManualesExcelDTO) 
    			&& listaApuntesManualesExcelDTO.get(0).isExcelTipoApunteSaldo()) {
    		try {
    			if (plantillaEspecifica == null) {
        			plantillaEspecifica = this.contaPlantillasDao.findByCodigo(listaApuntesManualesExcelDTO.get(0).getSaldoCodigoPlantilla());
    			}
    			this.tmct0ContaApuntesBo.persistirListaContaApuntesSIBBAC(listaTmct0ContaApuntes, plantillaEspecifica);
    		} catch (Exception ex) {
    			Loggers.logErrorAccounting(plantillaEspecifica.getCodigo(),
    					ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
    			throw new EjecucionPlantillaException();
    		}
    	}
    	
    	return true;
    } else {
    	// Se asume que hay errores y se informa sobre ello.
    	this.loggearErroresFicheroExcel(contenidoYValidExcel, tmct0CPlantillaManual);
    	return false;
    }
    
  }
  
  /**
   *	Ejecuta una plantilla individual segun el tipo y en base a una lista de ALCs
   *	@param	plantilla
   *	@param 	amExcelDTOProcesoALCs: lista con ALCs a procesar
   * 	@throws EjecucionPlantillaException 
   */
  private void ejecutarPlantillaIndividualALCs(
		  Tmct0ContaPlantillas plantilla, List<ApuntesManualesExcelDTO> amExcelDTOProcesoALCs) 
				  throws EjecucionPlantillaException {
	  if (AccountingHelper.isCabeceraDevengoAnulacionAM(plantilla.getTipo())) {
		  this.ejecucionDevengosApuntesManuales.ejecutarPlantillaIndividual(plantilla, amExcelDTOProcesoALCs);
	  } else if (this.isCobroLiquidacionTotal(plantilla)) {
		  this.ejecucionCobrosApuntesManuales.ejecutarPlantillaIndividual(plantilla, amExcelDTOProcesoALCs);
	  }else if(this.isLiquidacionParcial(plantilla)){
		  this.ejecucionLiquidacionesApuntesManuales.ejecutarPlantillaIndividual(plantilla, amExcelDTOProcesoALCs);
	  }
  }
  
  /**
   * Loggeo de errores al procesar el fichero Excel.
   *
   * @param contenidoYValidExcel contenidoYValidExcel
   * @param tmct0CPlantilla tmct0CPlantilla
   */
  private void loggearErroresFicheroExcel(ResultValidAndContenidoExcelDTO contenidoYValidExcel,
                                          Tmct0ContaPlantillas tmct0CPlantilla) {
    // Se registra lista de errores en Excel
    if (CollectionUtils.isNotEmpty(contenidoYValidExcel.getListaErrores())) {
      for (String errorMsg : contenidoYValidExcel.getListaErrores()) {
        Loggers.logErrorAccounting(tmct0CPlantilla.getCodigo(),
                                   StringHelper.safeNull(errorMsg).replace(".procesando", ""));
      }
    }
    // Se registra el caso de que no haya informacion en el Excel
    if (CollectionUtils.isEmpty(contenidoYValidExcel.getListaApuntesManualesExcelDTO())) {
      Loggers.logErrorAccounting(tmct0CPlantilla.getCodigo(), ConstantesError.ERROR_SIN_DATOS_FICHERO_EXCEL.getValue());
    }
  }

  /**
   * De aqui se obtiene una lista de apuntes contables por cada fila del fichero Excel para el tipo APUNTE SALDO.
   * 
   * @param amExcelDTO amExcelDTO
   * @param tmct0CPlantillaManual tmct0CPlantilla
   * @return List<Tmct0ContaApuntes> List<Tmct0ContaApuntes>
   * @throws EjecucionPlantillaException
   */
  private List<Tmct0ContaApuntes> getApuntesContablesTipoSaldo(ApuntesManualesExcelDTO amExcelDTO,
                                                               Tmct0ContaPlantillas tmct0CPlantillaManual) throws EjecucionPlantillaException {

    Loggers.logDebugInicioFinAccounting(tmct0CPlantillaManual != null ? tmct0CPlantillaManual.getCodigo() : null, true);

    List<Tmct0ContaApuntes> listaTmct0ContaApuntes = new ArrayList<Tmct0ContaApuntes>();

    // se debe ejecutar la plantilla que indica la primera columna de forma que:
    // Se deben crear los apuntes con las cuentas y auxiliares que indica la plantilla,
    // el importe al DEBE ser el valor a continuaci�n de IMPORTE DEBE,
    // el importe al HABER ser el valor a continuaci�n de IMPORTE_HABER,
    // el importe de PYG ir al DEBE si es positivo o al HABER si es negativo.

    // - El concepto al DEBE o al HABER ser� el que indica la Excel en vez del de la plantilla.
    Tmct0ContaPlantillas plantilla = this.tmct0ContaPlantillasDao.findByCodigo(amExcelDTO.getSaldoCodigoPlantilla());

    List<Tmct0ContaPlantillasDTO> listaTmct0ContaPlantillasDTO = this.dividirPlantillasPorCuentasApuntesManuales(plantilla,
                                                                                                                 amExcelDTO);

    if (CollectionUtils.isNotEmpty(listaTmct0ContaPlantillasDTO)) {
      for (Tmct0ContaPlantillasDTO plantillaDTO : listaTmct0ContaPlantillasDTO) {

        if (plantillaDTO.isTipoCuentaDebe()) {
          // ----- Apunte al debe.
        	
	          Tmct0ContaApuntes contaApunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
	          this.popularDatosComunesApunteContable(amExcelDTO, tmct0CPlantillaManual, contaApunteDebe);
	
	          // La cuenta contable al DEBE será el que indica la Excel si
	          // está relleno, sino será el de la plantilla
	          contaApunteDebe.setCuenta_contable(plantillaDTO.getCuenta_debe());
	          contaApunteDebe.setFichero(plantilla.getFichero());
	          contaApunteDebe.setTipo_comprobante(plantilla.getTipo_comprobante());
	          contaApunteDebe.setNum_comprobante(plantilla.getNum_comprobante());
	
	          // El auxiliar al DEBE sera el que indica la Excel si est�
	          // relleno, sino sera el de la plantilla.
	          if (StringUtils.isNotBlank(amExcelDTO.getSaldoAuxDebe())) {
	            contaApunteDebe.setAux_contable(amExcelDTO.getSaldoAuxDebe());
	          } else {
	            if (tmct0CPlantillaManual != null) {
	              contaApunteDebe.setAux_contable(StringHelper.quitarComillasDobles(tmct0CPlantillaManual.getAux_debe()));
	            }
	          }
	
	          contaApunteDebe.setConcepto(amExcelDTO.getSaldoConceptoDebe());
	          contaApunteDebe.setImporte(new BigDecimal(amExcelDTO.getSaldoImporteDebe()));
	          if (contaApunteDebe.getImporte().signum() > 0) {
	            contaApunteDebe.setTipo_apunte('D');
	          } else if (contaApunteDebe.getImporte().signum() < 0) {
	            contaApunteDebe.setTipo_apunte('H');
	          }
	        if(contaApunteDebe.getImporte().compareTo(BigDecimal.ZERO)!=0){
	          listaTmct0ContaApuntes.add(contaApunteDebe);
        	}
        } else {
          // ----- Apunte al Haber.
	          Tmct0ContaApuntes contaApunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
	          this.popularDatosComunesApunteContable(amExcelDTO, tmct0CPlantillaManual, contaApunteHaber);
	
	          // La cuenta contable al HABER será el que indica la Excel
	          // si está relleno, sino será el de la plantilla
	          contaApunteHaber.setCuenta_contable(plantillaDTO.getCuenta_haber());
	          contaApunteHaber.setFichero(plantilla.getFichero());
	          contaApunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
	          contaApunteHaber.setNum_comprobante(plantilla.getNum_comprobante());
	          // El auxiliar al HABER sera el que indica la Excel si est�
	          // relleno, sino sera el de la plantilla.
	          if (StringUtils.isNotBlank(amExcelDTO.getSaldoAuxHaber())) {
	            contaApunteHaber.setAux_contable(amExcelDTO.getSaldoAuxHaber());
	          } else {
	            if (tmct0CPlantillaManual != null) {
	              contaApunteHaber.setAux_contable(StringHelper.quitarComillasDobles(tmct0CPlantillaManual.getAux_haber()));
	            }
	          }
	          contaApunteHaber.setConcepto(amExcelDTO.getSaldoConceptoHaber());
	          contaApunteHaber.setImporte((new BigDecimal(amExcelDTO.getSaldoImporteHaber())).negate());
	          if (contaApunteHaber.getImporte().signum() > 0) {
	            contaApunteHaber.setTipo_apunte('D');
	          } else if (contaApunteHaber.getImporte().signum() < 0) {
	            contaApunteHaber.setTipo_apunte('H');
	          }
	        if(contaApunteHaber.getImporte().compareTo(BigDecimal.ZERO)!=0){
	          listaTmct0ContaApuntes.add(contaApunteHaber);
            }
        }
      }
      
      List<Tmct0ContaApuntes> listaContaApuntePyG = this.generarApunteContablePyG(amExcelDTO, tmct0CPlantillaManual,
                                                                                    plantilla);
        if (CollectionUtils.isNotEmpty(listaContaApuntePyG)) {
          listaTmct0ContaApuntes.addAll(listaContaApuntePyG);
        }

    }

    Loggers.logDebugInicioFinAccounting(tmct0CPlantillaManual != null ? tmct0CPlantillaManual.getCodigo() : null, false);

    return listaTmct0ContaApuntes;
  }

  /**
   * Generacion apuntes contables PyG.
   * 
   * @param amExcelDTO amExcelDTO
   * @param tmct0CPlantilla tmct0CPlantilla
   * @param plantilla plantilla
   * @param plantillaDTO plantillaDTO
   * @param cuentasDebe cuentasDebe
   * @param cuentasHaber cuentasHaber
   * @return Tmct0ContaApuntes Tmct0ContaApuntes
   */
  private List<Tmct0ContaApuntes> generarApunteContablePyG(ApuntesManualesExcelDTO amExcelDTO,
                                                           Tmct0ContaPlantillas tmct0CPlantilla,
                                                           Tmct0ContaPlantillas plantilla) {
    // Importe PyG - respetar signos
    // ganancias -> al debe (PyG positivo)
    // perdidas -> al haber (PyG negativo)

	BigDecimal importePyG =  new BigDecimal(amExcelDTO.getSaldoImportePyG());
    List<Tmct0ContaApuntes> listaApuntesContablesPyG = new ArrayList<Tmct0ContaApuntes>();
    // Si no hay cuenta contable no se guarda el apunte de diferencias.
    if (importePyG.compareTo(BigDecimal.ZERO)!=0 && StringUtils.isNotBlank(plantilla.getCuenta_diferencias())) {
        Tmct0ContaApuntes contaApuntePyG = AccountingHelper.getInitializedTmct0ContaApuntes();
        this.popularDatosComunesApunteContable(amExcelDTO, tmct0CPlantilla, contaApuntePyG);
        // La cuenta contable para apuntes de PyG es el campo CUENTA_DIFERENCIAS de la plantilla
        contaApuntePyG.setCuenta_contable(StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
        contaApuntePyG.setFichero(plantilla.getFichero());
        contaApuntePyG.setTipo_comprobante(plantilla.getTipo_comprobante());
        contaApuntePyG.setNum_comprobante(plantilla.getNum_comprobante());
        contaApuntePyG.setConcepto(amExcelDTO.getSaldoConceptoDebe());
        contaApuntePyG.setImporte(importePyG);
        contaApuntePyG.setAux_contable("");
        if (new BigDecimal(amExcelDTO.getSaldoImportePyG()).compareTo(BigDecimal.ZERO) < 0) {
        	contaApuntePyG.setTipo_apunte('H');
        }else{
        	contaApuntePyG.setTipo_apunte('D');	
        }
        listaApuntesContablesPyG.add(contaApuntePyG);
    }

    return listaApuntesContablesPyG;
  }

  /**
   * Retorna true si la cabecera de la plantilla es COBRO o LIQUIDACION
   * 
   * @param tmct0ContaPlantillas tmct0ContaPlantillas
   * @return boolean boolean
   */
  private boolean isLiquidacionParcial(Tmct0ContaPlantillas tmct0ContaPlantillas) {
    return tmct0ContaPlantillas != null && (tmct0ContaPlantillas.isTipoPlantillaLiquidacionParcial());
  }
  
  
  private boolean isCobroLiquidacionTotal(Tmct0ContaPlantillas tmct0ContaPlantillas) {
    return tmct0ContaPlantillas != null
           && (tmct0ContaPlantillas.isTipoPlantillaCobro() || tmct0ContaPlantillas.isTipoPlantillaLiquidacionTotal());
  }


  /**
   * Se populan datos comunes del apunte contable.
   * 
   * @param amExcelDTO: DTO con datos del Excel
   * @param tmct0CPlantilla: la plantilla
   * @param contaApunte: objeto a popular
   */
  private void popularDatosComunesApunteContable(ApuntesManualesExcelDTO amExcelDTO,
                                                 Tmct0ContaPlantillas tmct0CPlantilla,
                                                 Tmct0ContaApuntes contaApunte) {

    contaApunte.setIdmovimiento(null);
    contaApunte.setCod_plantilla(amExcelDTO.getSaldoCodigoPlantilla());
    contaApunte.setFecha_comprobante(new Date());
    contaApunte.setFichero(tmct0CPlantilla.getFicheroConValor());
    contaApunte.setPeriodo_comprobante(this.getPeriodoComprobante());

  }

  /**
   * Obtener periodo del comprobante.
   * 
   * @return String String (month(current date - 1 month) * 6 + day(current date-1 day)/5) + 1 PERIODO
   */
  private String getPeriodoComprobante() {
    // (month(current date - 1 month)*6+day(current date-1 day)/5)+1
    java.util.Date fechaActual = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fechaActual);
    int mesActual = calendar.get(Calendar.MONTH) + 1;
    int diaActual = calendar.get(Calendar.DAY_OF_MONTH);
    return String.valueOf(((mesActual - 1) * 6 + (diaActual - 1) / 5) + 1);
  }

  @Override
  protected void setTiposPlantilla() {
    super.tiposPlantilla = new String[] { TipoContaPlantillas.APUNTE_MANUAL.getTipo()
    };
  }

  /**
   * Devuelve lista de errores del fichero Excel en caso de haber. Tambien datos de cada una de las filas para
   * procesamiento.
   * 
   * @param Sheet sheet
   * @param nombreFicheroExcel
   * @return List<String> lista de errores
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private ResultValidAndContenidoExcelDTO getExcelValido(Sheet sheet, String nombreFicheroExcel) throws EjecucionPlantillaException {

	  // Se crea una lista de errores.
	  List<String> listaErrores = new ArrayList<String>();
	  ResultValidAndContenidoExcelDTO resultValidAndContenidoExcelDTO = new ResultValidAndContenidoExcelDTO();

	  try {
		  // Se toma el header del Excel.
		  Iterator<Row> rows = sheet.rowIterator();

		  List<ApuntesManualesExcelDTO> listaApuntesManualesExcelDTO = new ArrayList<ApuntesManualesExcelDTO>();

		  if (nombreFicheroExcel.toLowerCase().startsWith("apunte_sibbac")) {

			  // Validacion de cabecera de fichero "apunte_sibbac"
			  String[] sibbacTipoPlantilla = new String[3];

			  // Numero de fila
			  String codigoPlantilla = "";
			  int nroFilaExcel = 1;

			  // Validacion de detalle de fichero "apunte_sibbac"
			  while (rows.hasNext()) {
				  Row row = (Row) rows.next();
				  Iterator<Cell> celdas = row.cellIterator();

				  ApuntesManualesExcelDTO apuntesManualesExcelDTO = new ApuntesManualesExcelDTO();
				  apuntesManualesExcelDTO.setSibbacNroFilaExcel(nroFilaExcel);

				  Cell primerCelda = row.getCell(0);
				  int conteoCabeceraAcumulada = 0;

				  if (primerCelda.getStringCellValue().trim().equalsIgnoreCase("PLANTILLA")) {
					  // Es cabecera
					  sibbacTipoPlantilla = this.checkCabeceraExcelApunteSIBBACValido(row, nombreFicheroExcel, listaErrores,
							  nroFilaExcel);
					  conteoCabeceraAcumulada = conteoCabeceraAcumulada + 1;
					  // Si hay mas de un registro de cabecera consecutivo se levanta un error.
					  if (conteoCabeceraAcumulada > 1) {
						  listaErrores.add("Error de formato en detalle. No existe detalle para la plantilla." + " El fichero "
								  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel)
								  + " no tiene registros de detalle.");

					  }
				  } else {
					  // Es detalle
					  // Se arma DTO de apunte SIBBAC.
					  
					  conteoCabeceraAcumulada = 0;
					  apuntesManualesExcelDTO.setTipoApunte(TiposApuntesManualesExcelEnum.APUNTE_SIBBAC.getTipoApunteManualExcel());
					  apuntesManualesExcelDTO.setSibbacCodigoPlantilla(sibbacTipoPlantilla[0]);
					  apuntesManualesExcelDTO.setSibbacConcepto(sibbacTipoPlantilla[1]);
					  apuntesManualesExcelDTO.setSibbacRefTheirDescRefTheirMayor12(sibbacTipoPlantilla[2]);
					  apuntesManualesExcelDTO.setSibbacReferenciasTheirMenor12(sibbacTipoPlantilla[3]);
					  apuntesManualesExcelDTO.setSibbacReferenciasTheirTotal(sibbacTipoPlantilla[4]);

					  while (celdas.hasNext()) {
						  int columnIndex = celdas.next().getColumnIndex();
						  Cell celda = row.getCell(columnIndex);
						  switch (columnIndex) {
						  case 0:
							  // La primera columna es la referencia NUORDEN de un ALC. OBLIGATORIO
							  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
								  listaErrores.add("No se encuentra referencia NUORDEN de un ALC en fichero: "
										  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
							  } else {
								  apuntesManualesExcelDTO.setSibbacNuorden(ExcelProcessingHelper.getCellValue(celda));
							  }
							  break;
						  case 1:
							  // La segunda columna es la referencia NBOOKING de un ALC
							  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
								  listaErrores.add("No se encuentra referencia NBOOKING de un ALC en fichero: "
										  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
							  } else {
								  apuntesManualesExcelDTO.setSibbacNbooking(ExcelProcessingHelper.getCellValue(celda));
							  }
							  break;
						  case 2:
							  // La tercera columna es la referencia NUCNFCLT de un ALC
							  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
								  listaErrores.add("No se encuentra referencia NUCNFCLT de un ALC en fichero: "
										  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
							  } else {
								  apuntesManualesExcelDTO.setSibbacNucnfclt(ExcelProcessingHelper.getCellValue(celda));
							  }
							  break;
						  case 3:
							  // La cuarta columna es la referencia NUCNFLIQ de una ALC
							  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
								  listaErrores.add("No se encuentra referencia NUCNFLIQ de una ALC en fichero: "
										  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
							  } else {
								  apuntesManualesExcelDTO.setSibbacNucnfliq(ExcelProcessingHelper.getCellValue(celda));
							  }
							  break;
						  }
					  }

					  // Se hace validacion adicional para correspondencia entre REFERENCIA THEIR
					  // y TIPO COBRO/LIQUIDACION
					  if(!codigoPlantilla.equalsIgnoreCase(apuntesManualesExcelDTO.getSibbacCodigoPlantilla())){
						  codigoPlantilla = apuntesManualesExcelDTO.getSibbacCodigoPlantilla();
						  this.validarTipoCobroLiquidReferenciaTheirSIBBAC(listaErrores, apuntesManualesExcelDTO);
					  }

					  // Valida si no existe ningun ALC para las referencias de una linea de detalle
					  this.validarALCsTipoSIBBAC(listaErrores, apuntesManualesExcelDTO);

					  // Si no hay errores se agrega el DTO.
					  if (CollectionUtils.isEmpty(listaErrores)) {
						  listaApuntesManualesExcelDTO.add(apuntesManualesExcelDTO);
					  }
				  }
				  // Se incrementa el nro. de fila.
				  nroFilaExcel++;
			  }
			  
			  

			  // Se agregan errores de fichero a la plantilla.
			  if (CollectionUtils.isNotEmpty(listaErrores)) {
				  this.agregarErroresFichero(listaErrores);
			  }

		  } else if (nombreFicheroExcel.toLowerCase().startsWith("apunte_saldo0")) {

			  Row rowHead = (Row) rows.next();

			  // Validacion de cabecera para tipos de fichero "apunte_saldo0"
			  this.checkCabeceraExcelApunteSaldoValido(rowHead, nombreFicheroExcel, listaErrores);

			  // Numero de fila
			  int nroFilaExcel = 2;

			  // Validacion de detalles de fichero "apunte_saldo0"
			  while (rows.hasNext()) {
				  Row row = (Row) rows.next();

				  // Se arma DTO de apunte saldo.
				  ApuntesManualesExcelDTO apuntesManualesExcelDTO = new ApuntesManualesExcelDTO();
				  apuntesManualesExcelDTO.setTipoApunte(TiposApuntesManualesExcelEnum.APUNTE_SALDO.getTipoApunteManualExcel());
				  apuntesManualesExcelDTO.setSaldoNroFilaExcel(nroFilaExcel);

				  Iterator<Cell> celdas = rowHead.cellIterator();
				  while (celdas.hasNext()) {
					  int columnIndex = celdas.next().getColumnIndex();
					  Cell celda = row.getCell(columnIndex);
					  switch (columnIndex) {
					  case 0:
						  // La primera columna (A) debe contener el c�digo de
						  // plantilla. OBLIGATORIO
						  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. Plantilla inexistente (fila " + nroFilaExcel + ") en fichero: "
									  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
						  } else {
							  Tmct0ContaPlantillas tmct0ContaPlantillas = this.tmct0ContaPlantillasDao.findByActivoAndCodigo(ExcelProcessingHelper.getCellValue(celda),
									  SibbacEnums.ActivacionContaPlantillas.ACTIVO.getActivado());
							  if (tmct0ContaPlantillas == null) {
								  listaErrores.add("Plantilla con código " + ExcelProcessingHelper.getCellValue(celda)
										  + " inactiva en la DB (fila " + nroFilaExcel + ") en fichero: " + nombreFicheroExcel);
							  }
							  apuntesManualesExcelDTO.setSaldoCodigoPlantilla(ExcelProcessingHelper.getCellValue(celda));
						  }
						  break;
					  case 1:
						  // La segunda columna (B) debe contener una referencia de texto libre. OPCIONAL
						  apuntesManualesExcelDTO.setSaldoAuxDebe(ExcelProcessingHelper.getCellValue(celda));
						  break;
					  case 2:
						  // La tercera columna (C) debe contener una referencia
						  // de texto libre.
						  // OBLIGATORIO
						  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. No existe referencia DEBE (fila " + nroFilaExcel + ") en fichero: "
									  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
						  } else {
							  apuntesManualesExcelDTO.setSaldoConceptoDebe(ExcelProcessingHelper.getCellValue(celda));
						  }
						  break;
					  case 3:
						  // La cuarta columna (D) debe tener un importe. OBLIGATORIO
						  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. No existe importe DEBE (fila " + nroFilaExcel + ") en fichero: "
									  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
						  } else if (!StringHelper.isNumericCeldaExcel(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. El importe de DEBE no es correcto (fila " + nroFilaExcel + ")");
						  } else {
							  apuntesManualesExcelDTO.setSaldoImporteDebe(ExcelProcessingHelper.getCellValue(celda));
						  }
						  break;
					  case 4:
						  apuntesManualesExcelDTO.setSaldoAuxHaber(ExcelProcessingHelper.getCellValue(celda));
						  break;
					  case 5:
						  // La sexta columna (F) debe contener una referencia de texto libre. OBLIGATORIO
						  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. No existe referencia HABER (fila " + nroFilaExcel + ") en fichero: "
									  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
						  } else {
							  apuntesManualesExcelDTO.setSaldoConceptoHaber(ExcelProcessingHelper.getCellValue(celda));
						  }
						  break;
					  case 6:
						  // La s�ptima columna (G) debe tener un importe. OBLIGATORIO
						  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. No existe importe HABER (fila " + nroFilaExcel + ") en fichero: "
									  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
						  } else if (!StringHelper.isNumericCeldaExcel(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. El importe de HABER no es correcto (fila " + nroFilaExcel + ")");
						  } else {
							  apuntesManualesExcelDTO.setSaldoImporteHaber(ExcelProcessingHelper.getCellValue(celda));
						  }
						  break;
					  case 7:
						  // La octava columna (H) debe tener un importe. OBLIGATORIO
						  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("No se encuentra campo importe PyG (fila " + nroFilaExcel + ") en fichero: "
									  + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
						  } else if (!StringHelper.isNumericCeldaExcel(ExcelProcessingHelper.getCellValue(celda))) {
							  listaErrores.add("Error de datos. El importe de PyG no es correcto (fila " + nroFilaExcel + ")");
						  } else {
							  apuntesManualesExcelDTO.setSaldoImportePyG(ExcelProcessingHelper.getCellValue(celda));
						  }
						  break;
					  case 8:
						  // La novena columna (I) tiene el literal “CUENTA CONTABLE DEBE”. OPCIONAL
						  apuntesManualesExcelDTO.setSaldoCuentaContableDebe(ExcelProcessingHelper.getCellValue(celda));
						  break;
					  case 9:
						  // La decima columna (J) tiene el literal “CUENTA CONTABLE HABER”. OPCIONAL
						  apuntesManualesExcelDTO.setSaldoCuentaContableHaber(ExcelProcessingHelper.getCellValue(celda));
						  break;
					  }
				  }

				  // Si no hay errores se agrega el DTO.
				  if (CollectionUtils.isEmpty(listaErrores)) {
					  listaApuntesManualesExcelDTO.add(apuntesManualesExcelDTO);
				  }

				  // Se incrementa nro. de fila del excel.
				  nroFilaExcel++;
			  }

			  // Se agregan errores de fichero a la plantilla.
			  if (CollectionUtils.isNotEmpty(listaErrores)) {
				  this.agregarErroresFichero(listaErrores);
			  }

		  }


		  // Se carga info necesaria.
		  resultValidAndContenidoExcelDTO.setListaErrores(listaErrores);
		  resultValidAndContenidoExcelDTO.setListaApuntesManualesExcelDTO(listaApuntesManualesExcelDTO);

	  } catch (Exception e) {
		  Loggers.logErrorAccounting("MANUAL", ConstantesError.ERROR_DATOS_FICHERO_EXCEL.getValue());
		  throw new EjecucionPlantillaException(e);
	  }

	  return resultValidAndContenidoExcelDTO;
  }

  /**
   * Valida correspondencia entre tipo cobro/liquidacion para tipos SIBBAC.
   * 
   * @param listaErrores listaErrores
   * @param apuntesManualesExcelDTO apuntesManualesExcelDTO
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private void validarTipoCobroLiquidReferenciaTheirSIBBAC(List<String> listaErrores,
                                                           ApuntesManualesExcelDTO apuntesManualesExcelDTO) throws EjecucionPlantillaException {
    Tmct0ContaPlantillas plantilla = this.contaPlantillasDao.findByCodigo(apuntesManualesExcelDTO.getSibbacCodigoPlantilla());

    if (this.isCobroLiquidacionTotal(plantilla) || this.isLiquidacionParcial(plantilla)) {
      // 8. Si la plantilla de la cabecera es de tipo Cobro (normal o barridos) o Liquidación (mercado o cliente)
      // y no existe ninguna REFERENCIA THEIR. Error en cabecera. No hay movimientos norma43 (fila nn)
      if (StringUtils.isBlank(apuntesManualesExcelDTO.getSibbacReferenciasTheirTotal())) {
        listaErrores.add("Error en cabecera. No hay movimientos norma43 (fila "
                         + apuntesManualesExcelDTO.getSibbacNroFilaExcel() + ")");
      }

      // 9. Si la plantilla de la cabecera es de tipo Cobro (normal o barridos) o Liquidación (mercado o cliente)
      // y alguna REFERENCIA THEIR no existe en TMCT0_NORMA43_MOVIMIENTO, buscando primero que el valor sea igual 
      // a campo DESC_REFERENCIA y si no se encuentra, buscando que sea igual a la concatenación de campos 
      // REFERENCIA+DESC_REFERENCIA Error en cabecera.
      // No existe el movimiento norma43 (fila nn, columna mm)
      List<String> listaErroresNorma43Movimiento = this.tmct0Norma43MovimientoBo.getListaErroresNorma43MovimientoByReferenciasTheirSIBBAC(
    		  apuntesManualesExcelDTO);
      if (CollectionUtils.isNotEmpty(listaErroresNorma43Movimiento)) {
        listaErrores.addAll(listaErroresNorma43Movimiento);
      }
    }
  }

  /**
   * Valida ALCs obtenidos del fichero contra la DB.
   * 
   * @param listaErrores listaErrores
   * @param apuntesManualesExcelDTO apuntesManualesExcelDTO
   */
  private void validarALCsTipoSIBBAC(List<String> listaErrores, ApuntesManualesExcelDTO apuntesManualesExcelDTO) {
    Tmct0ContaPlantillas plantilla = this.contaPlantillasDao.findByCodigo(apuntesManualesExcelDTO.getSibbacCodigoPlantilla());

    if (plantilla != null) {
      // Si no existe ningún ALC para las referencias de una línea de detalle.
      // Error en detalle. No existe ALC en la bbdd
      Tmct0alc tmct0alc = this.tmct0alcDao.findByNuordenAndNbookingAndNucnfcltAndNucnfliq(apuntesManualesExcelDTO.getSibbacNuorden(),
                                                                                          apuntesManualesExcelDTO.getSibbacNbooking(),
                                                                                          apuntesManualesExcelDTO.getSibbacNucnfclt(),
                                                                                          StringHelper.getShortNumeric(apuntesManualesExcelDTO.getSibbacNucnfliq()));
      if (tmct0alc == null) {
        listaErrores.add("Error en detalle. No existe ALC en la bbdd (fila "
                         + apuntesManualesExcelDTO.getSibbacNroFilaExcel() + ")");
      }
    } else {
      listaErrores.add("No se encontró plantilla en la bbdd.");
    }
  }

  /**
   * Chequea si es valida la cabecera del Excel tipo Apunte SIBBAC.
   * 
   * @param rowHead rowHead
   * @param nombreFicheroExcel nombreFicheroExcel
   * @param listaErrores listaErrores
   * @param nroFilaExcel nroFilaExcel
   * @return tipoPlantilla tipoPlantilla
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private String[] checkCabeceraExcelApunteSIBBACValido(Row rowHead,
                                                        String nombreFicheroExcel,
                                                        List<String> listaErrores,
                                                        int nroFilaExcel) throws EjecucionPlantillaException {

	  String[] datosPlantilla = new String[5];
	  try {
		  // En el 1er elemento va el tipo de plantilla
		  // En el 2do elemento va la REFERENCIA THEIR

		  // Validacion de cabecera para tipos de fichero "apunte_sibbac"
		  if (rowHead != null) {
			  Iterator<Cell> celdas = rowHead.cellIterator();
			  while (celdas.hasNext()) {
				  int columnIndex = celdas.next().getColumnIndex();
				  Cell celda = rowHead.getCell(columnIndex);
				  switch (columnIndex) {
				  case 0:
					  // La primera columna tiene el literal PLANTILLA.
					  // OBLIGATORIO
					  if (!"PLANTILLA".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
						  listaErrores.add("El encabezado del fichero no contiene el literal PLANTILLA (fila " + nroFilaExcel
								  + ") en fichero: " + nombreFicheroExcel);
					  }
					  break;
				  case 1:
					  // La segunda columna de la cabecera debe contener el
					  // codigo de plantilla. OBLIGATORIO
					  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
						  listaErrores.add("Error en cabecera. Plantilla inexistente (fila " + nroFilaExcel + ") en fichero: "
								  + nombreFicheroExcel);
					  } else {
						  Tmct0ContaPlantillas tmct0ContaPlantillas = this.tmct0ContaPlantillasDao.findByActivoAndCodigo(ExcelProcessingHelper.getCellValue(celda),
								  SibbacEnums.ActivacionContaPlantillas.ACTIVO.getActivado());
						  if (tmct0ContaPlantillas == null) {
							  listaErrores.add("Error: Plantilla con código " + ExcelProcessingHelper.getCellValue(celda)
									  + " inactiva en la DB (fila " + nroFilaExcel + ") en fichero: " + nombreFicheroExcel);
						  }
						  datosPlantilla[0] = ExcelProcessingHelper.getCellValue(celda);
					  }
					  break;
				  case 2:
					  // columna CONCEPTO.
					  // OBLIGATORIO
					  if (!"CONCEPTO".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
						  listaErrores.add("El encabezado del fichero no contiene el literal CONCEPTO (fila " + nroFilaExcel
								  + ") en fichero: " + nombreFicheroExcel);
					  }
					  break;
				  case 3:
					  // Valor del CONCEPTO - Solo aplica si hay mas de una referencia their indicada en el Excel.
					  datosPlantilla[1] = "";
					  if (StringUtils.isEmpty(ExcelProcessingHelper.getCellValue(celda))) {
						  listaErrores.add("El encabezado del fichero no contiene un valor de CONCEPTO (fila " + nroFilaExcel
								  + ") en fichero: " + nombreFicheroExcel);
					  } else {
						  datosPlantilla[1] = celda.getStringCellValue();
					  }
					  break;
				  }
			  }

			  // Si solo hay una referencia their en el Excel (columna con index 5 rellena y columna con index 6 sin rellenar), 
			  // el concepto sera dicha referencia their. Caso contrario, es el valor de la columna corresp. ya seteada arriba
//			  if (rowHead.getCell(6) == null 
//					  || (rowHead.getCell(6) != null && StringUtils.isBlank(rowHead.getCell(6).getStringCellValue()))) {
//				  datosPlantilla[1] = rowHead.getCell(5).getStringCellValue();
//			  }
			  
			  datosPlantilla[2] = "VALUES ";
			  datosPlantilla[3] = "";
			  datosPlantilla[4] = "";
			  if (StringUtils.isNotBlank(datosPlantilla[0])) {
				  Tmct0ContaPlantillas plantilla = this.tmct0ContaPlantillasDao.findByCodigo(datosPlantilla[0]);
				  if (this.isCobroLiquidacionTotal(plantilla) || this.isLiquidacionParcial(plantilla)) {
					  // Dinamico
					  for (int j = 5; j < 5 + 3000; j++) {
						  Cell celda = rowHead.getCell(j);
						  if (celda != null && StringUtils.isNotBlank(celda.getStringCellValue())) {
							  int length = celda.getStringCellValue().length();
							  String referenciaTheir = "";
							  String descReferenciaTheir = "";
							  if (length > 12) {
								  referenciaTheir = celda.getStringCellValue().substring(0 , 12);
								  descReferenciaTheir = celda.getStringCellValue().substring(12 , length);
								  // Almacena datos para armar la query de cobro con pares (referenciaTheir, descReferenciaTheir)
								  datosPlantilla[2] = datosPlantilla[2]  + "('" + referenciaTheir + "','" + descReferenciaTheir + "'),";
							  } else {
								  // Almacena datos para armar la query de cobro con solo referencias their
								  datosPlantilla[3] =  datosPlantilla[3]  + "'" + celda.getStringCellValue() + "',";
							  }
							  // Almacena datos para armar la query de cobro con solo referencias their
							  // sin importar la longitud
							  datosPlantilla[4] =  datosPlantilla[4]  + "'" + celda.getStringCellValue() + "',";
						  }
					  }

					  if(datosPlantilla[2] != null && !datosPlantilla[2].isEmpty()){
						  datosPlantilla[2] = datosPlantilla[2].substring(0,datosPlantilla[2].length()-1);
					  }
					  
					  if(datosPlantilla[3] != null && !datosPlantilla[3].isEmpty()){
						  datosPlantilla[3] = datosPlantilla[3].substring(0,datosPlantilla[3].length()-1);
					  }
					  
					  if(datosPlantilla[4] != null && !datosPlantilla[4].isEmpty()){
						  datosPlantilla[4] = datosPlantilla[4].substring(0,datosPlantilla[4].length()-1);
					  }
				  }
			  }
		  } else {
			  listaErrores.add("No se encuentra cabecera en fichero: " + nombreFicheroExcel);
		  }
	  } catch (Exception e) {
		  Loggers.logErrorAccounting("MANUAL", ConstantesError.ERROR_DATOS_FICHERO_EXCEL.getValue());
		  throw new EjecucionPlantillaException(e);
	  }
    return datosPlantilla;
  }

  /**
   * Chequea si es valida la cabecera del Excel tipo saldo.
   * 
   * @param rowHead rowHead
   * @param nombreFicheroExcel nombreFicheroExcel
   * @param listaErrores listaErrores
   */
  private void checkCabeceraExcelApunteSaldoValido(Row rowHead, String nombreFicheroExcel, List<String> listaErrores) {
    if (rowHead != null) {
      Iterator<Cell> celdas = rowHead.cellIterator();
      while (celdas.hasNext()) {
        int columnIndex = celdas.next().getColumnIndex();
        Cell celda = rowHead.getCell(columnIndex);
        switch (columnIndex) {
          case 0:
            // La primera columna tiene el literal PLANTILLA.
            // OBLIGATORIO
            if (!"PLANTILLA".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal PLANTILLA en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 1:
            // La primera columna tiene el literal AUXILIAR DEBE.
            // OBLIGATORIO
            if (!"AUXILIAR DEBE".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal AUXILIAR DEBE en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 2:
            // La primera columna tiene el literal CONCEPTO DEBE.
            // OBLIGATORIO
            if (!"CONCEPTO DEBE".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal CONCEPTO DEBE en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 3:
            // La primera columna tiene el literal �IMPORTE DEBE�.
            // OBLIGATORIO
            if (!"IMPORTE DEBE".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal IMPORTE DEBE en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 4:
            // La primera columna tiene el literal �AUXILIAR HABER�.
            // OBLIGATORIO
            if (!"AUXILIAR HABER".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal AUXILIAR HABER en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 5:
            // La primera columna tiene el literal �CONCEPTO HABER�.
            // OBLIGATORIO
            if (!"CONCEPTO HABER".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal CONCEPTO HABER en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 6:
            // La primera columna tiene el literal �IMPORTE HABER�.
            // OBLIGATORIO
            if (!"IMPORTE HABER".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal IMPORTE HABER en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
          case 7:
            // La primera columna tiene el literal �IMPORTE PYG�.
            // OBLIGATORIO
            if (!"IMPORTE PYG".equalsIgnoreCase(ExcelProcessingHelper.getCellValue(celda))) {
              listaErrores.add("El encabezado del fichero no contiene el literal IMPORTE PYG en fichero: "
                               + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
            }
            break;
        }
      }
    } else {
      listaErrores.add("Error de formato. No existe registro de cabecera en fichero: "
                       + FileHelper.getNombreFicheroFiltrado(nombreFicheroExcel));
    }
  }

  /**
   * Ejecucion de plantilla individual.
   * 
   * @param plantilla plantilla
   */
  @Override
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) {
    return new ArrayList<List<Tmct0ContaPlantillasDTO>>();
  }

  /**
   * Remueve de la lista de threads actuales el thread especificado.
   * 
   * @param current Thread a remover.
   * @param tieneErrores True si el thread terminó con errores.
   */
  public void shutdownFicheroIndividual(EjecucionFicheroIndividualRunnable current, boolean tieneErrores) {
    if (!threadEjecucionFicheroIndividualError) {
      threadEjecucionFicheroIndividualError = tieneErrores;
    }
    threadsEjecucionFicheroIndividual.remove(current);
  }

  /**
   * Remueve de la lista de threads actuales el thread especificado.
   * 
   * @param current Thread a remover.
   * @param tieneErrores True si el thread terminó con errores.
   */
  public void shutdownRunnablePlantillaIndividual(EjecucionPlantillaIndividualRunnable current, boolean tieneErrores) {
    if (!threadEjecucionPlantillaIndividualError) {
      threadEjecucionPlantillaIndividualError = tieneErrores;
    }
    threadsEjecucionPlantillaIndividual.remove(current);
  }

  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public void ejecutarFicheroIndividual(File ficheroExcel) throws EjecucionPlantillaException {

    File ficheroProcesando = null;
    XSSFWorkbook wbXLSX = null;
    Tmct0ContaPlantillas plantillaManual = null;

    try {
      // Si el fichero Excel existe en la ruta, antes de procesarla se debe renombrar
      // cambiando la extension a .procesando.
      FileUtils.moveFile(ficheroExcel.toPath(),
                         FileHelper.renombrarArchivo(ficheroExcel.toPath(), ficheroExcel.getName() + ".procesando"));

      ficheroProcesando = new File(ficheroExcel.getPath() + ".procesando");

      FileInputStream file = new FileInputStream(ficheroProcesando.getPath());
      wbXLSX = new XSSFWorkbook(file);
      Sheet sheet = wbXLSX.getSheetAt(0);

      ResultValidAndContenidoExcelDTO contenidoYValidExcel = this.getExcelValido(sheet, ficheroProcesando.getName());

      plantillaManual = this.tmct0ContaPlantillasDao.findByCodigo("MANUAL");

      // Se generan apuntes contables
      boolean exito = this.generarApuntesContables(contenidoYValidExcel, plantillaManual); 

      if (wbXLSX != null) {
        wbXLSX.close();
      }

      if (exito) {
        // finalizado el tratamiento de la Excel de forma exitosa, esta se debe mover
        // a la carpeta de tratados con el nombre nombreFichero_yyyymmdd_hhmmss.XLSX
        FileHelper.moveFile(ficheroProcesando, new File(folderContabilidadInTratados), ".procesando");
      } else {
        // En caso de que haya un error de validacion del fichero se vuelve al estado inicial.
        FileUtils.moveFile(ficheroProcesando.toPath(),
                           FileHelper.renombrarArchivo(ficheroProcesando.toPath(),
                                                       FileHelper.suprimirContenidoNombreFichero(ficheroProcesando.getName(),
                                                                                                 ".procesando")));
      }

    } catch (FileNotFoundException e) {
      Loggers.logErrorAccounting(plantillaManual.getCodigo() != null ? plantillaManual.getCodigo() : null,
                                 "Error al procesar fichero: " + e.getMessage());
      // Si el fichero Excel no existe en la ruta se debe actualizar la
      // AUDIT_DATE de la plantilla y seguir con la siguiente plantilla
      this.tmct0ContaPlantillasDao.update(new Date(), APUNTE_MANUAL, "");
      throw new EjecucionPlantillaException("Error al procesar fichero: " + e.getMessage());
    } catch (Exception e) {
      Loggers.logErrorAccounting(plantillaManual.getCodigo() != null ? plantillaManual.getCodigo() : null,
                                 "Error al procesar fichero: " + e.getMessage());
      
      try {
        FileUtils.moveFile(ficheroProcesando.toPath(),
                           FileHelper.renombrarArchivo(ficheroProcesando.toPath(),
                                                       FileHelper.suprimirContenidoNombreFichero(ficheroProcesando.getName(),
                                                                                                 ".procesando")));
        String body = "";
        if(e.getMessage().equals("cobro")){
        	body = body1;
        }else if(e.getMessage().equals("ordenes")){
        	body = body2;
        }else{
        	body = body3;
        }
        
        InputStream targetStream = new FileInputStream(ficheroExcel);
		List<InputStream> listAttachment = new ArrayList<InputStream>();
		listAttachment.add(targetStream);
		
		List<String> emails = new ArrayList<String>();
		List<String> nombreFicheros = new ArrayList<String>();
		nombreFicheros.add(ficheroExcel.getPath());
		
		String[] destinatarios = destinatario.split(";",-1);
		
		for(String dest : destinatarios){
			emails.add(dest);
		}
		

		sendMail.sendMail(emails, subject, body, listAttachment, nombreFicheros, null);
        
        
      } catch (Exception e1) {
        Loggers.logErrorAccounting(plantillaManual.getCodigo() != null ? plantillaManual.getCodigo() : null,
                                   "Error al procesar fichero: " + e1.getMessage());
        throw new EjecucionPlantillaException("Error al procesar fichero: " + e1.getMessage());
      }
      throw new EjecucionPlantillaException("Error al procesar fichero: " + e.getMessage());
    }
  }

  @Override
  public void anadirPlantillas(List<List<Tmct0ContaPlantillasDTO>> lPlantillas) {
    plantillasSinBalance.addAll(lPlantillas);
  }
}