package sibbac.business.accounting.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.accounting.database.bo.Tmct0AlcArreglosBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ACCOUNTING.NAME, 
           name = Task.GROUP_ACCOUNTING.JOB_MARCAR_COBRADO_OPERACIONES_NO_COBRABLES, 
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "07:00:00")
@DisallowConcurrentExecution
public class TaskArregloContabilidadOperacionesNoCobrables extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOG = LoggerFactory.getLogger(TaskArregloContabilidadOperacionesNoCobrables.class);

  @Autowired
  private Tmct0AlcArreglosBo tmct0AlcArreglosBo;

  @Override
  public void executeTask() throws Exception {

    try {
      tmct0AlcArreglosBo.procesarOperacionesNoCobrables();
    }
    catch (SIBBACBusinessException ex) {
      LOG.error("", ex);
    }
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_APUNTE.TASK_CONTA_ACTUALIZA_OPS_NO_COBRABLES;
  }

}
