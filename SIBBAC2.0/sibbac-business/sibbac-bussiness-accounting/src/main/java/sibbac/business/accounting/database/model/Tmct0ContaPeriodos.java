package sibbac.business.accounting.database.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Sort;

@Entity
@Table(name = "TMCT0_CONTA_PERIODOS")
public class Tmct0ContaPeriodos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
 
	@Column(name = "ANIO", length = 8, nullable = true)
	private Integer anio;
	
	@Column(name = "PERIODO", length = 8, nullable = true)
	private Integer periodo;
	
	@Column(name = "FHINIPERIODO", length = 10, nullable = true)
	private Date fhIniPeriodo;
	
	@Column(name = "FHFINPERIODO", length = 10, nullable = true)
	private Date fhFinPeriodo;
	
	@Column(name = "PROCESO", length = 1, nullable = true)
	private String proceso;
	
	@Column(name = "CERRADO_PERIODO", length = 2, nullable = false)
	private Integer cerradoPeriodo;
	
	
	
	public Tmct0ContaPeriodos(){
		
	}


	public Tmct0ContaPeriodos(BigInteger id, Integer anio, Integer periodo, Date fhIniPeriodo, Date fhFinPeriodo,
			String proceso) {
		super();
		this.id = id;
		this.anio = anio;
		this.periodo = periodo;
		this.fhIniPeriodo = fhIniPeriodo;
		this.fhFinPeriodo = fhFinPeriodo;
		this.proceso = proceso;
	}


	public BigInteger getId() {
		return id;
	}


	public void setId(BigInteger id) {
		this.id = id;
	}


	public Integer getAnio() {
		return anio;
	}


	public void setAnio(Integer anio) {
		this.anio = anio;
	}


	public Integer getPeriodo() {
		return periodo;
	}


	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}


	public Date getFhIniPeriodo() {
		return fhIniPeriodo;
	}


	public void setFhIniPeriodo(Date fhIniPeriodo) {
		this.fhIniPeriodo = fhIniPeriodo;
	}


	public Date getFhFinPeriodo() {
		return fhFinPeriodo;
	}


	public void setFhFinPeriodo(Date fhFinPeriodo) {
		this.fhFinPeriodo = fhFinPeriodo;
	}


	public String getProceso() {
		return proceso;
	}


	public void setProceso(String proceso) {
		this.proceso = proceso;
	}


	public Integer getCerradoPeriodo() {
		return cerradoPeriodo;
	}


	public void setCerradoPeriodo(Integer cerradoPeriodo) {
		this.cerradoPeriodo = cerradoPeriodo;
	}
			
}
