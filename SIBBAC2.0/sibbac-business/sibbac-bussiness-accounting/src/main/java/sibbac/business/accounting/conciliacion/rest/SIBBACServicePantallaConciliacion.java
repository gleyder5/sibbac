package sibbac.business.accounting.conciliacion.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Table;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0ContaPartidasPndtsBo;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaCabecerasExcelDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDaoImpl;
import sibbac.business.accounting.conciliacion.database.dto.DatosCabeceraExcelTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.DatosColumnaPartidasPndtsDTO;
import sibbac.business.accounting.conciliacion.database.dto.DatosNombreExcelTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.ErroresExcelTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.FiltroGrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.InformacionTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.ListasProcesoExcelTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaPartidasPndtsAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaCabecerasExcel;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum.EstadoContaPartidasPndtsEnum;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.business.wrappers.common.ExcelProcessingHelper;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.SibbacEnums;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.model.Tmct0Users;

/**
 * Servicio para pantalla de conciliacion.
 */
@SIBBACService
public class SIBBACServicePantallaConciliacion implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory
			.getLogger(SIBBACServicePantallaConciliacion.class);

	/** Carga fichero TLM. */
	private static final String CARGAR_FICHERO_TLM = "CargarFicheroTLM";

	/** Busqueda TLM. */
	private static final String BUSQUEDA_TLM = "BusquedaTLM";

	/** Informacion TLM. */
	private static final String INFORMACION_TLM = "InformacionTLM";

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";
	private static final String FILTER_FILE = "file";
	private static final String FILTER_FILENAME = "filename";

	@Autowired
	private Tmct0ContaPartidasPndtsBo tmct0ContaPartidasPndtsBo;

	@Autowired
	private Tmct0ContaCabecerasExcelDao tmct0ContaCabecerasExcelDao;

	@Autowired
	private Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;

	@Autowired
	private Tmct0UsersDao tmct0UsersDao;

	@Autowired
	private Tmct0ContaPartidasPndtsAssembler tmct0ContaPartsPndtsAssembler;

	@Autowired
	private Tmct0ContaPartidasPndtsDaoImpl tmct0ContaPartidasPndtsDaoImpl;

	@Autowired
	private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;

	@Autowired
	private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;
	
	@Autowired
	private Tmct0ContaApuntesPndtsDao tmct0ContaApuntesPndtsDao;
	
	/**
	 * The Enum Campos plantilla .
	 */
	private enum FieldsPlantilla {

		PLANTILLA("plantilla");

		/** The field. */
		private String field;

		/**
		 * Instantiates a new fields.
		 *
		 * @param field
		 *            the field
		 */
		private FieldsPlantilla(String field) {
			this.field = field;
		}

		/**
		 * Gets the field.
		 *
		 * @return the field
		 */
		public String getField() {
			return field;
		}
	}

	/**
	 * Process.
	 *
	 * @param webRequest
	 *            the web request
	 * @return the web response
	 * @throws SIBBACBusinessException
	 *             the SIBBAC business exception
	 */
	@Override
	public WebResponse process(WebRequest webRequest)
			throws SIBBACBusinessException {

		final WebResponse webResponse = new WebResponse();

		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		Map<String, Object> resultadosError = new HashMap<String, Object>();
		String command = webRequest.getAction();

		try {
			LOG.debug("Entro al servicio de Pantalla de Conciliacion");

			switch (command) {
			case CARGAR_FICHERO_TLM:
				resultadosError = this.cargarFicheroTLM(webRequest, webResponse);
				if (resultadosError != null && !resultadosError.isEmpty()) {
					result.setError("Una o más celdas del fichero TLM no son válidas");
					result.setResultadosError(resultadosError);
				}
				break;
			case BUSQUEDA_TLM:
				result.setResultados(this.busquedaTLM(webRequest, webResponse));
				break;
			case INFORMACION_TLM:
				result.setResultados(this.informacionTLM());
				break;
			default:
				result.setError("An action must be set. Valid actions are: "
						+ command);
				break;
			} // switch
		} catch (SIBBACBusinessException e) {
			// Se desactiva el semaforo de carga de fichero Excel.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			resultados.put("status", "KO");
			LOG.error("Error al ejecutar comando", e);
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			// Se desactiva el semaforo de carga de fichero Excel.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			resultados.put("status", "KO");
			LOG.error("Error al ejecutar comando", e);
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

		LOG.debug("Salgo del servicio de Pantalla de Conciliacion");

		return result;
	}

	/**
	 * Obtencion la informacion de TLM
	 * @return Map<String, Object>
	 * @throws SIBBACBusinessException
	 */
	private Map<String, Object> informacionTLM() throws SIBBACBusinessException {

		LOG.trace("Iniciando informacionTLM");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<String> datosUsuarioFecha = this.tmct0ContaPartidasPndtsDao.findAuditDateAuditUser();
			InformacionTLMDTO dtoInformacionTLM = InformacionTLMDTO.convertObjectToDTO(datosUsuarioFecha.get(0), this.tmct0ContaPartidasPndtsDao.getMaxBookingDateFormatted());
			Tmct0Users tmct0Users = tmct0UsersDao.getByUsername(dtoInformacionTLM.getUsuario());
			if (null != tmct0Users) {
				dtoInformacionTLM.setUsuario(dtoInformacionTLM.getUsuario() + " - " + tmct0Users.getName() + " " + tmct0Users.getLastname());
			} else {
				dtoInformacionTLM.setUsuario("No existe el usuario: "+ dtoInformacionTLM.getUsuario());
			}
			result.put("informacionTLM", dtoInformacionTLM);
		} catch (Exception e) {
			LOG.error("Error metodo informacionTLM -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}
		LOG.trace("Fin informacionTLM");
		return result;
	}	

	private Map<String, ? extends Object> busquedaTLM(WebRequest webRequest, WebResponse webResponse)
			throws SIBBACBusinessException {

		LOG.trace("Inicio la busquedaTLM");
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			final Map<String, Object> filtros = webRequest.getParamsObject();
			FiltroGrillaTLMDTO filtro = tmct0ContaPartsPndtsAssembler.getTmct0FiltroGrillaTLMDTO(filtros);
			List<GrillaTLMDTO> listaGrillaTLMDTO = new ArrayList<GrillaTLMDTO>();

			List<Tmct0ContaPartidasPndts> listaContaPartidasPndts =  this.tmct0ContaPartidasPndtsDaoImpl.consultarPartidasPndtsByFilter(filtro);

			if (CollectionUtils.isNotEmpty(listaContaPartidasPndts)) {
				for (Tmct0ContaPartidasPndts cpp : listaContaPartidasPndts) {
					GrillaTLMDTO dto =  this.tmct0ContaPartsPndtsAssembler.convertContaPndtsToGrillaTLMDTO(cpp);
					listaGrillaTLMDTO.add(dto);
				}
			}

			result.put("resultados", listaGrillaTLMDTO);

		} catch (Exception e) {
			LOG.error("Error metodo busquedaTLM -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.trace("Fin recuperación datos para busquedaTLM");

		return result;

	}

	/**
	 * Carga de fichero de TLM.
	 * 
	 * @param webRequest
	 * @param webResponse
	 * @return Map<String, Object>
	 * @throws SIBBACBusinessException
	 */
	private Map<String, Object> cargarFicheroTLM(WebRequest webRequest,
			WebResponse webResponse) throws SIBBACBusinessException {

		LOG.trace("Inicio Carga de fichero de TLM");
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, String> filters = webRequest.getFilters();
		try {
			List<ErroresExcelTLMDTO> erroresExcelTLM = null;
			final String excelFile = StringHelper.processString(filters.get(FILTER_FILE));
			final String excelFileName = StringHelper.processString(filters.get(FILTER_FILENAME));
			final String userName = (String) filters.get("userName");
			final String saltarValidacion = (String) filters.get("saltarValidacion");
			byte[] data = Base64.decodeBase64(excelFile);

			// Se extraen datos del nombre del fichero Excel.
			DatosNombreExcelTLMDTO dNExcelDTO = this.getDatosNombreExcelTLM(excelFileName);

			// Validaciones preliminares para la carga de fichero TLM.
			// Si hay errores no se continua procesando el fichero.
			ErroresExcelTLMDTO errExcelTLMDTO = this.getValidacionesPreliminares(
					excelFileName, dNExcelDTO, userName);
			if (errExcelTLMDTO != null) {
				erroresExcelTLM = new ArrayList<ErroresExcelTLMDTO>();
				erroresExcelTLM.add(errExcelTLMDTO);
				result.put("erroresExcelTLM", erroresExcelTLM);
				return result;
			}

			try(InputStream inputStream = new ByteArrayInputStream(data);
					Workbook workbook = ExcelProcessingHelper.openWorkbook(excelFileName, inputStream)) {
				DatosCabeceraExcelTLMDTO datosCabeceraExcelTLMDTO = 
						this.procesarFicheroTLM(workbook, dNExcelDTO.getFechaArchivo(), userName, Boolean.valueOf(saltarValidacion));
				if (datosCabeceraExcelTLMDTO != null) {
					result.put("erroresExcelTLM", datosCabeceraExcelTLMDTO.getListaErroresExcelTLMDTO());
				}
			} catch (IOException e) {
				LOG.error("Error en el procesamiento del fichero Excel del TLM", e);
				// Se desactiva el semaforo de carga de fichero Excel.
				this.tmct0SibbacConstantesBo.updateValorByClave(
						SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
						PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			}
		} catch (Exception e) {
			LOG.error("Error metodo cargarFicheroTLM -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.trace("Fin Carga de fichero de TLM");
		return result;
	}

	/**
	 *	Validaciones preliminares en la carga de ficheros TLM.
	 *	@param excelFileName: nombre del fichero Excel
	 *	@param dNExcelDTO: datos del Excel
	 *	@param userName: usuario de login
	 *	@return ErroresExcelTLMDTO		 
	 */
	private ErroresExcelTLMDTO getValidacionesPreliminares(
			String excelFileName, DatosNombreExcelTLMDTO dNExcelDTO, String userName) {
		ErroresExcelTLMDTO errExcelTLMDTO = null;
		try {
			
			// Si hay apuntes pendientes no se permite cargar el Excel de TLM
			Long conteoApuntesPndts = this.tmct0ContaApuntesPndtsDao.count();
			if (conteoApuntesPndts != null && conteoApuntesPndts > 0) {
				errExcelTLMDTO = new ErroresExcelTLMDTO(
						"0", "0", "No se puede cargar el fichero TLM ya que hay apuntes pendientes.");
				return errExcelTLMDTO;
			}
			
			// Se valida que no haya otros usuarios generando apuntes.
			Tmct0SibbacConstantes semafGenerarApuntes = this.tmct0SibbacConstantesDao.findByClaveAndValor(
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave(),
					SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado());
			if (semafGenerarApuntes != null) {
				errExcelTLMDTO = new ErroresExcelTLMDTO(
						"0", "0", "No se puede cargar el fichero TLM ya que hay usuarios generando apuntes.");
				return errExcelTLMDTO;
			}

			if (!dNExcelDTO.isEsFicheroTipoExcel()) {
				errExcelTLMDTO = new ErroresExcelTLMDTO(
						"0", "0", "El fichero a procesar contiene una extensión inválida");
				return errExcelTLMDTO;
			}

		} catch (Exception e) {
			LOG.error("Error metodo getValidacionesPreliminares -  " + e.getMessage(), e);
			throw(e);
		}
		return errExcelTLMDTO;
	}

	/**
	 *	Se obtienen datos del nombre del fichero Excel de TLM.
	 *	@param excelFileName
	 *	@return DatosNombreExcelTLMDTO  
	 */
	private DatosNombreExcelTLMDTO getDatosNombreExcelTLM(String excelFileName) {
		DatosNombreExcelTLMDTO dNExcelDTO = new DatosNombreExcelTLMDTO();
		List<String> extensionesFichero = new ArrayList<String>();
		try {
			extensionesFichero.add("xls");
			extensionesFichero.add("xlsx");
			dNExcelDTO.setEsFicheroTipoExcel(ExcelProcessingHelper.isExtensionValida(excelFileName, extensionesFichero));
			dNExcelDTO.setFechaArchivo(
					DateHelper.convertStringChainToDate(
							StringHelper.getNumerosConsecutivosEnString(excelFileName, 6)));
		} catch (Exception e) {
			LOG.error("Error metodo getDatosNombreExcelTLM -  " + e.getMessage(), e);
			throw(e);
		}
		return dNExcelDTO;
	}

	/**
	 * Gets the fields.
	 *
	 * @return the fields
	 */
	@Override
	public List<String> getFields() {
		List<String> result = new ArrayList<String>();
		try {
			FieldsPlantilla[] fields = FieldsPlantilla.values();
			for (int i = 0; i < fields.length; i++) {
				result.add(fields[i].getField());
			}
		} catch (Exception e) {
			LOG.error("Error metodo getFields -  " + e.getMessage(), e);
			throw(e);
		}
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		List<String> result = new ArrayList<String>();

		return result;
	}

	/**
	 * 	Procesa contenido del fichero de TLM.
	 *	@param wbXLSX
	 *	@param fechaArchivo
	 *	@param userName
	 *	@return DatosCabeceraExcelTLMDTO
	 * @throws Exception 
	 */
	private DatosCabeceraExcelTLMDTO procesarFicheroTLM(
			Workbook wbXLSX, Date fechaArchivo, String userName, boolean saltarValidacion) throws Exception {
		DatosCabeceraExcelTLMDTO datosCabeceraExcelTLMDTO = null;
		Tmct0SibbacConstantes constanteSheetTabPartidas = null;
		List<Tmct0SibbacConstantes> listaConstantes = this.tmct0SibbacConstantesDao.findByClave(
			Constantes.CLAVE_SHEET_TAB_PARTIDAS_ABIERTAS);
		if (CollectionUtils.isNotEmpty(listaConstantes)) {
			constanteSheetTabPartidas = listaConstantes.get(0);
		}
		
		// Se obtiene lista de GINs de partidas casadas.
		List<String> listaGINsPartidasPndts = this.tmct0ContaPartidasPndtsDao.findGINsByEstado(
				EstadoContaPartidasPndtsEnum.CASADA.getEstadoPartidaPndte());
		
		try {
			for (int i = 0; i < wbXLSX.getNumberOfSheets(); i++) {
				Sheet sheet = wbXLSX.getSheetAt(i);
				if (sheet != null && constanteSheetTabPartidas != null 
					&& sheet.getSheetName().equals(constanteSheetTabPartidas.getValor())) {
					// Se toma el header del Excel.
					
					List<Row> rows = IteratorUtils.toList((Iterator<Row>) sheet.rowIterator());
						// Se procesa cabecera.
					if(!rows.isEmpty()){
						datosCabeceraExcelTLMDTO = this.getCabeceraTLMInvalida(rows.get(0));
					}
					
					if (datosCabeceraExcelTLMDTO.isEmptyListaErroresExcelTLMDTO()) {
						Integer validacion = 1;
						if(!saltarValidacion) {
							validacion = this.tmct0ContaPartidasPndtsBo.validateBookingDate(rows, datosCabeceraExcelTLMDTO.getMapDatosColPartsPndtsDTO());
						}
						if (validacion < 0) {
							List<ErroresExcelTLMDTO> listaErroresExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>();
							ErroresExcelTLMDTO error = new ErroresExcelTLMDTO("0", "0", "El fichero contiene un fecha Booking Date menor a la ultima fecha Booking date de la base de datos");
							listaErroresExcelTLMDTO.add(error);
							datosCabeceraExcelTLMDTO.setListaErroresExcelTLMDTO(listaErroresExcelTLMDTO);
						} else if (validacion == 0) {
							List<ErroresExcelTLMDTO> listaErroresExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>();
							ErroresExcelTLMDTO error = new ErroresExcelTLMDTO("0", "0", "bookingIgual");
							listaErroresExcelTLMDTO.add(error);
							datosCabeceraExcelTLMDTO.setListaErroresExcelTLMDTO(listaErroresExcelTLMDTO );
						} else {
							// Se valida que no haya otros usuarios cargando ficheros Excel TLM.
							Tmct0SibbacConstantes semafCargaExcelTLM = this.tmct0SibbacConstantesDao.findByClaveAndValor(
									PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave(),
									SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado());
							if (semafCargaExcelTLM != null) {
								List<ErroresExcelTLMDTO> listaErroresExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>();
								ErroresExcelTLMDTO error = new ErroresExcelTLMDTO("0", "0", "No se puede cargar el fichero TLM ya que hay usuarios generando apuntes. Intentelo de nuevo mas tarde.");
								listaErroresExcelTLMDTO.add(error);
								datosCabeceraExcelTLMDTO.setListaErroresExcelTLMDTO(listaErroresExcelTLMDTO );
								LOG.error("No se puede cargar el fichero TLM ya que hay usuarios generando apuntes.");
							} else {

								// Se activa el semaforo de carga de fichero Excel.
								this.tmct0SibbacConstantesBo.updateValorByClave(
										SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado(), 
										PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());


								// Si no hay cabecera invalida se asume validacion exitosa.
								ListasProcesoExcelTLMDTO listasProcesoExcelTLMDTO = this.tmct0ContaPartidasPndtsBo.convertExcelRowsInEntityList(
										rows, datosCabeceraExcelTLMDTO.getMapDatosColPartsPndtsDTO(), userName, listaGINsPartidasPndts);

								List<ErroresExcelTLMDTO> listErrExcelTLMDTO = this.tmct0ContaPartidasPndtsBo.persistTLMData(listasProcesoExcelTLMDTO);
								if (CollectionUtils.isNotEmpty(listErrExcelTLMDTO)) {
									datosCabeceraExcelTLMDTO = new DatosCabeceraExcelTLMDTO();
									datosCabeceraExcelTLMDTO.getListaErroresExcelTLMDTO().addAll(listErrExcelTLMDTO);
								}

								// Se desactiva el semaforo de carga de fichero Excel.
								this.tmct0SibbacConstantesBo.updateValorByClave(
										SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
										PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
							}
						}
					} else {
						LOG.error("La cabecera del fichero Excel de TLM no es válida");
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error metodo procesarFicheroTLM -  " + e.getMessage(), e);
			throw(e);
		}
		return datosCabeceraExcelTLMDTO;
	}

	/**
	 *	Devuelve datos de cabecera de columna invalida y errores si la hay.
	 *	Caso contrario devuelve la lista vacia y se asume que la cabecera es valida.
	 *	@param row
	 *	@return DatosCabeceraExcelTLMDTO
	 *	@throws SIBBACBusinessException
	 */
	private DatosCabeceraExcelTLMDTO getCabeceraTLMInvalida(Row row) throws SIBBACBusinessException {
		List<ErroresExcelTLMDTO> listaErroresExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>();
		DatosCabeceraExcelTLMDTO datosCabeceraExcelTLMDTO = new DatosCabeceraExcelTLMDTO();
		try {
			Iterator<Cell> celdas = row.cellIterator();
			while (celdas.hasNext()) {
				int columnIndex = celdas.next().getColumnIndex();
				Cell celda = row.getCell(columnIndex);
				String celdaCabecera = ExcelProcessingHelper.getCellValueTLM(celda);
				// Se valida celda del Excel
				if (StringUtils.isNotBlank(celdaCabecera)) {
					Tmct0ContaCabecerasExcel tmct0ContaCabecerasExcel = 
							this.tmct0ContaCabecerasExcelDao.findByCeldaCabeceraExcel(celdaCabecera.trim());
					if (tmct0ContaCabecerasExcel == null) {
						ErroresExcelTLMDTO errExcelTLMDTO = new ErroresExcelTLMDTO();
						errExcelTLMDTO.setNroFila(String.valueOf(row.getRowNum() + 1));
						errExcelTLMDTO.setNroColumna(String.valueOf(columnIndex + 1));
						errExcelTLMDTO.setDescripcionError(celdaCabecera);
						listaErroresExcelTLMDTO.add(errExcelTLMDTO);
						datosCabeceraExcelTLMDTO.getListaErroresExcelTLMDTO().add(errExcelTLMDTO);
					} else {
						Class<?> entidadContaPartPndts = Tmct0ContaPartidasPndts.class;
						Table table = entidadContaPartPndts.getAnnotation(Table.class);
						DatosColumnaPartidasPndtsDTO datosColPartPndtsDTO = 
								this.tmct0ContaPartidasPndtsBo.getDTODatosColsPartPndtsByTbNameAndColName(
										tmct0ContaCabecerasExcel.getCampoTabla(), table.name());
						datosCabeceraExcelTLMDTO.getMapDatosColPartsPndtsDTO().put(
								String.valueOf(columnIndex), datosColPartPndtsDTO);	
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error metodo getCabeceraTLMInvalida -  " + e.getMessage(), e);
			throw(e);
		}
		return datosCabeceraExcelTLMDTO;
	}

}