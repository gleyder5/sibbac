package sibbac.business.accounting.database.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.threads.EjecucionPlantillaIndividualRunnable;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;

/**
 * Clase de negocios para la ejecuciÃ³n de plantillas de cobros.
 * 
 * @author Neoris
 */
@Service
public class EjecucionCobrosRetrocesiones extends AbstractEjecucionPlantilla {

  protected static final Logger LOG = LoggerFactory.getLogger(EjecucionCobrosRetrocesiones.class);

  @Override
  protected void setTiposPlantilla() {
    super.tiposPlantilla = new String[] { TipoContaPlantillas.COBRO.getTipo(), TipoContaPlantillas.RETROCESION.getTipo()
    };
  }

  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

  /*
   * (non-Javadoc)
   * @see sibbac.business.accounting.database.bo.AbstractEjecucionPlantilla# ejecutarPlantillas()
   */
  @Override
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException {

    if (plantillas != null && !plantillas.isEmpty()) {
      ExecutorService executor = Executors.newFixedThreadPool(plantillas.size());

      for (Tmct0ContaPlantillas plantilla : plantillas) {
        EjecucionPlantillaIndividualRunnable runnable = new EjecucionPlantillaIndividualRunnable(plantilla, this);
        executor.execute(runnable);
      }

      try {
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.HOURS);
      } catch (InterruptedException e) {
        Loggers.logErrorAccounting(plantillas.get(0).getCodigo() != null ? plantillas.get(0).getCodigo() : null,
                                   ConstantesError.ERROR_EJECUCION_HILOS.getValue());
        throw new EjecucionPlantillaException("ERROR ejecutarPlantillas");
      }
    }
    return plantillasSinBalance;
  }

  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

    Loggers.logDebugInicioFinAccounting(plantilla != null ? plantilla.getCodigo() : null, true);

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();

    List<Object[]> cobros = null;
    try {
      // 1. Se ejecutan las querys de cobro y se obtienen los
      // registros.
      cobros = ejecutaQueryCobro(plantilla);
    } catch (Exception ex) {
      // De haber algun error se para la ejecucion completa.
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO.getValue() + ": " + ex.getMessage());

      throw new EjecucionPlantillaException();
    }

    try {
      // Se procesa cada plantilla por fecha de forma transaccional.
        if (cobros != null && !cobros.isEmpty()) {
            for (Object[] cobro : cobros) {
            	contaPlantillasDTOs.addAll(tmct0ContaApuntesBo.ejecutarPlantillaCobrosRetrocesiones(plantilla, cobro));
            }
        }
    } catch (Exception ex) {
      // De haber algun error se para la ejecucion completa.
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    // Si la plantilla tiene query_final se ejecuta.
    if ((plantilla.getQuery_final() != null) && (!plantilla.getQuery_final().equals(""))) {
      try {
        contaPlantillasDaoImp.executeQueryUpdate(plantilla.getQuery_final().replace(";", ""), plantilla.getCodigo());
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_FINAL.getValue() + ": " + ex.getMessage());
        throw new EjecucionPlantillaException();
      }
    }

    Loggers.logDebugInicioFinAccounting(plantilla != null ? plantilla.getCodigo() : null, false);

    return plantillasSinBalance;
  }

  @Override
  public void anadirPlantillas(List<List<Tmct0ContaPlantillasDTO>> lPlantillas) {
    plantillasSinBalance.addAll(lPlantillas);
  }
}
