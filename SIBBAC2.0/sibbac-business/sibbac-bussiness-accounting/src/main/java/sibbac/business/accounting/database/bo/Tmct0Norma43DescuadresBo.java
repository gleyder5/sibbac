package sibbac.business.accounting.database.bo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.dto.CobroDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;
import sibbac.common.utils.SibbacEnums.ConstantesPersistencia;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0Norma43DescuadresBo extends AbstractBo<Norma43Descuadres, Long, Norma43DescuadresDao>{
	
	@Autowired
	private Norma43DescuadresDao norma43DescuadreDao;
	
	/**
	 *	Obtiene descuadre de Cobro liquidacion.
	 *	@param comentario comentario
	 *	@param plantilla plantilla
	 *	@param cobro cobro
	 *	@param orden orden
	 *	@param importe importe
	 *	@return Norma43Descuadres Norma43Descuadres
	 *	@throws EjecucionPlantillaException EjecucionPlantillaException 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Norma43Descuadres getDescuadreCobroLiquidacion(
		String comentario, Tmct0ContaPlantillas plantilla, CobroDTO cobro, 
		BigDecimal importe) throws EjecucionPlantillaException {
		
		Norma43Descuadres descuadre = AccountingHelper.getInitializedNorma43Descuadres();
		// 2.3.4 Generación de descuadres
		// Para la generación de descuadres se debe crear el correspondiente
		// registro en la tabla TMCT0_NORMA43_DESCUADRES de la siguiente forma:

		// 28. CUENTA – Cuenta Bancaria de la plantilla
		descuadre.setCuenta(plantilla.getCuenta_bancaria());
		// 29. REF_FECHA - El campo REFERENCIA de la query de cobro
		descuadre.setRefFecha(cobro.getReferencia());
		// 30. REF_ORDEN - El campo DESC_REFERENCIA de la query de cobro
		descuadre.setRefOrden(cobro.getDescReferencia());
		// 31. SENTIDO - El campo SENTIDO de la query de cobro
		descuadre.setSentido(cobro.getSentido());
		// 32. TRADE_DATE - El campo TRADEDATE de la query de cobro
		descuadre.setTradeDate(cobro.getTradeDate());
		// 33. SETTLEMENT_DATE – El campo SETTDATE de la query de cobro
		descuadre.setSettlementDate(cobro.getSetDate());
		// 34. IMPORTE_NORMA43 – El campo IMPORTE de la query de cobro
		BigDecimal importeCobro = cobro.getImporte();
		descuadre.setImporteNorma43(importeCobro);
		// 35. DIFERENCIA – El importe de diferencia restando IMPORTE de query
		// cobro de IMPORTE de query orden
		BigDecimal importeOrden = BigDecimal.ZERO;
		if (importe != null) {
			importeOrden = importe;
		}
		BigDecimal diferencia = importeCobro.subtract(importeOrden);
		descuadre.setDiferencia(diferencia);
		// 36. NBCOMENTARIO – El comentario del descuadre que corresponda
		descuadre.setComentario(comentario);
		// 37. TIPO_APUNTE – El campo CODIGO de la plantilla
		// TODO descuadre.setTipoApunte(plantilla.getCodigo());
		return descuadre;
	}
	
	/**
	 * Crea un descuadre para la plantilla/cobro/orden
	 * @param comentario
	 * @param plantilla
	 * @param cobro
	 * @param orden
	 * @param importe
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void crearDescuadre(String comentario, Tmct0ContaPlantillas plantilla, 
		CobroDTO cobro, BigDecimal importe) {
		Norma43Descuadres descuadre = AccountingHelper.getInitializedNorma43Descuadres();
		descuadre.setAuditUser(plantilla.getCodigo());
		// 28. CUENTA â€“ Cuenta Bancaria de la plantilla
		descuadre.setCuenta(plantilla.getCuenta_bancaria());
		// 29. REF_FECHA - El campo REFERENCIA de la query de cobro
		descuadre.setRefFecha(cobro.getReferencia());
		// 30. REF_ORDEN - El campo DESC_REFERENCIA de la query de cobro
		descuadre.setRefOrden(cobro.getDescReferencia());
		// 31. SENTIDO - El campo SENTIDO de la query de cobro
		descuadre.setSentido(cobro.getSentido());
		// 32. TRADE_DATE - El campo TRADEDATE de la query de cobro
		descuadre.setTradeDate(cobro.getTradeDate());
		// 33. SETTLEMENT_DATE â€“ El campo SETTDATE de la query de cobro
		descuadre.setSettlementDate(cobro.getSetDate());
		// 34. IMPORTE_NORMA43 â€“ El campo IMPORTE de la query de cobro
		BigDecimal importeCobro = cobro.getImporte();
		descuadre.setImporteNorma43(importeCobro);
		// 35. DIFERENCIA â€“ El importe de diferencia restando IMPORTE de query
		// cobro de IMPORTE de query orden
		BigDecimal importeOrden = BigDecimal.ZERO;
		if (importe != null) {
			importeOrden = importe;
		}
		BigDecimal diferencia = importeCobro.subtract(importeOrden);
		descuadre.setDiferencia(diferencia);
		// 36. NBCOMENTARIO â€“ El comentario del descuadre que corresponda
		descuadre.setComentario(comentario);
		// 37. TIPO_APUNTE â€“ El campo CODIGO de la plantilla
		//descuadre.setTipoApunte(plantilla.getCodigo());
		this.norma43DescuadreDao.save(descuadre);
		if (descuadre != null && descuadre.getId() != null) {
			Loggers.logTraceGenericoAccounting(
					plantilla.getCodigo(), 
					ConstantesPersistencia.CREADO_DESCUADRE_ID.getValue() + descuadre.getId());
		}
	}
}
