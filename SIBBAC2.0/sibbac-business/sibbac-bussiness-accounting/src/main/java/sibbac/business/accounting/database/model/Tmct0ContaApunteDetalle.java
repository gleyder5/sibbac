package sibbac.business.accounting.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;

/**
 * Entidad para la gestion de TMCT0_CONTA_APUNTE_DETALLE
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CONTA_APUNTE_DETALLE")
public class Tmct0ContaApunteDetalle {

	/** Campos de la entidad */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	@ManyToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "IDAPUNTE")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaApuntes tmct0ContaApuntes;

	@Column(name = "NUORDEN", length = 20)
	private String nuOrden;

	@Column(name = "NBOOKING", length = 20)
	private char nBooking;

	@Column(name = "NUCNFCLT", length = 20)
	private String nuCnfClt;

	@Column(name = "NUCNFLIQ", length = 2)
	private Integer nuCnfLiq;

	@ManyToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_FACTURA_M")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0FacturasManuales tmct0FacturasManuales;
	
	@ManyToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_FACTURA_MB")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0BajasManuales tmct0FacturasManualesBaja;
	
	@ManyToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_FACTURA_MRB")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0RectificativaBajasManuales tmct0RectificadasManualesBaja;

	@Column(name = "IMPORTE", length = 18, nullable = false)
	private BigDecimal tipo_comprobante;

	@Column(name = "AUDIT_DATE")
	private Date audit_date;

	@ManyToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_FACTURA_MR")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0RectificativaManual tmct0RectificadasManuales;

	public Tmct0ContaApunteDetalle() {
	}

	public Tmct0ContaApunteDetalle(Integer id,
			Tmct0ContaApuntes tmct0ContaApuntes, String nuOrden, char nBooking,
			String nuCnfClt, Integer nuCnfLiq,
			Tmct0FacturasManuales tmct0FacturasManuales,
			Tmct0BajasManuales tmct0FacturasManualesBaja,
			Tmct0RectificativaBajasManuales tmct0RectificadasManualesBaja,
			BigDecimal tipo_comprobante, Date audit_date,
			Tmct0RectificativaManual tmct0RectificadasManuales) {
		super();
		this.id = id;
		this.tmct0ContaApuntes = tmct0ContaApuntes;
		this.nuOrden = nuOrden;
		this.nBooking = nBooking;
		this.nuCnfClt = nuCnfClt;
		this.nuCnfLiq = nuCnfLiq;
		this.tmct0FacturasManuales = tmct0FacturasManuales;
		this.tipo_comprobante = tipo_comprobante;
		this.audit_date = audit_date;
		this.tmct0RectificadasManuales = tmct0RectificadasManuales;
		this.tmct0FacturasManualesBaja = tmct0FacturasManualesBaja;
		this.tmct0RectificadasManualesBaja = tmct0RectificadasManualesBaja;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tmct0ContaApuntes getTmct0ContaApuntes() {
		return this.tmct0ContaApuntes;
	}

	public void setTmct0ContaApuntes(Tmct0ContaApuntes tmct0ContaApuntes) {
		this.tmct0ContaApuntes = tmct0ContaApuntes;
	}

	public String getNuOrden() {
		return this.nuOrden;
	}

	public void setNuOrden(String nuOrden) {
		this.nuOrden = nuOrden;
	}

	public char getnBooking() {
		return this.nBooking;
	}

	public void setnBooking(char nBooking) {
		this.nBooking = nBooking;
	}

	public String getNuCnfClt() {
		return this.nuCnfClt;
	}

	public void setNuCnfClt(String nuCnfClt) {
		this.nuCnfClt = nuCnfClt;
	}

	public Integer getNuCnfLiq() {
		return this.nuCnfLiq;
	}

	public void setNuCnfLiq(Integer nuCnfLiq) {
		this.nuCnfLiq = nuCnfLiq;
	}

	public Tmct0FacturasManuales getTmct0FacturasManuales() {
		return this.tmct0FacturasManuales;
	}

	public void setTmct0FacturasManuales(Tmct0FacturasManuales tmct0FacturasManuales) {
		this.tmct0FacturasManuales = tmct0FacturasManuales;
	}

	public Tmct0RectificativaManual getTmct0RectificadasManuales() {
		return this.tmct0RectificadasManuales;
	}

	public void setTmct0RectificadasManuales(
			Tmct0RectificativaManual tmct0RectificadasManuales) {
		this.tmct0RectificadasManuales = tmct0RectificadasManuales;
	}

	public BigDecimal getTipo_comprobante() {
		return this.tipo_comprobante;
	}

	public void setTipo_comprobante(BigDecimal tipo_comprobante) {
		this.tipo_comprobante = tipo_comprobante;
	}

	public Date getAudit_date() {
		return this.audit_date;
	}

	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	};
	
	/**
	 * @return the tmct0FacturasManualesBaja
	 */
	public Tmct0BajasManuales getTmct0FacturasManualesBaja() {
		return tmct0FacturasManualesBaja;
	}

	/**
	 * @param tmct0FacturasManualesBaja the tmct0FacturasManualesBaja to set
	 */
	public void setTmct0FacturasManualesBaja(
			Tmct0BajasManuales tmct0FacturasManualesBaja) {
		this.tmct0FacturasManualesBaja = tmct0FacturasManualesBaja;
	}

	/**
	 * @return the tmct0RectificadasManualesBaja
	 */
	public Tmct0RectificativaBajasManuales getTmct0RectificadasManualesBaja() {
		return tmct0RectificadasManualesBaja;
	}

	/**
	 * @param tmct0RectificadasManualesBaja the tmct0RectificadasManualesBaja to set
	 */
	public void setTmct0RectificadasManualesBaja(
			Tmct0RectificativaBajasManuales tmct0RectificadasManualesBaja) {
		this.tmct0RectificadasManualesBaja = tmct0RectificadasManualesBaja;
	}

}
