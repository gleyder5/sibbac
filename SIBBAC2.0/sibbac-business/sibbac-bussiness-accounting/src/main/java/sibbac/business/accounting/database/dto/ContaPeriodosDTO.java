package sibbac.business.accounting.database.dto;

public class ContaPeriodosDTO {
	
	Integer anio;
	Integer periodo;
	String fhIniPeriodo;
	String fhFinPeriodo;
	public Integer getAnio() {
		return anio;
	}
	public void setAnio(Integer anio) {
		this.anio = anio;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public String getFhIniPeriodo() {
		return fhIniPeriodo;
	}
	public void setFhIniPeriodo(String fhIniPeriodo) {
		this.fhIniPeriodo = fhIniPeriodo;
	}
	public String getFhFinPeriodo() {
		return fhFinPeriodo;
	}
	public void setFhFinPeriodo(String fhFinPeriodo) {
		this.fhFinPeriodo = fhFinPeriodo;
	}
	
}
