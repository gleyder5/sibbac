package sibbac.business.accounting.conciliacion.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ContaOurDAO extends JpaRepository<Tmct0ContaOur, BigInteger> {
	
	@Query( "SELECT our FROM Tmct0ContaOur our WHERE our.apuntesPndts.id = :id" )
	public List<Tmct0ContaOur> findById(@Param( "id" ) BigInteger id);

	@Query( "SELECT our FROM Tmct0ContaOur our WHERE our.conciliacionAudit.id = :idConcilAudit" )
	public List<Tmct0ContaOur> findByIdConciliacionAudit(@Param( "idConcilAudit" ) BigInteger idConcilAudit);
	
	@Query( "SELECT our FROM Tmct0ContaOur our WHERE our.apuntesAudit.id = :idApunteAudit" )
	public List<Tmct0ContaOur> findByIdApunteAudit(@Param( "idApunteAudit" ) BigInteger idApunteAudit);
	
}
