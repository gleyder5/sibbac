package sibbac.business.accounting.database.bo;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.dao.Tmct0ContaPeriodosDao;
import sibbac.business.accounting.database.model.Tmct0ContaPeriodos;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.Task;

@Service
public class Tmct0ContaPeriodosBo extends AbstractBo<Tmct0ContaPeriodos, Integer, Tmct0ContaPeriodosDao> {
	
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaPeriodosBo.class);
	 
	@Autowired
	private Tmct0ContaPeriodosDao tmct0ContaPeriodosDao;
	
	@Transactional
	public Map<String, Object> crearPeriodos() throws SIBBACBusinessException{
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		Integer[] meses31 = {0,2,4,6,7,9,11};
		Integer[] meses30 = {3,5,8,10};
		
		List<Tmct0ContaPeriodos> listPeriodos = tmct0ContaPeriodosDao.findContaPeriodosOrderByFhFinPeriodo();
		
		if(listPeriodos!=null && !listPeriodos.isEmpty()){
			Date fechaFinPeriodo = listPeriodos.get(1).getFhFinPeriodo();
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(fechaFinPeriodo);
			
			
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			
			
			if(day > 25){
				day = 5;
				if(month == 11){
					month = 0;
					year = year + 1;
				}else{
					month = month + 1;
				}
			}else if(day < 25){
				day = day + 5;
			}else{
				if(Arrays.asList(meses31).contains(month)){
					day = 31;
				}else if(Arrays.asList(meses30).contains(month)){
					day = 30;
				}else{
					if(cal.isLeapYear(year)){
						day = 29;
					}else{
						day = 28;
					}
				}
			}
			
			
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.YEAR, year);
			
			try {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Tmct0ContaPeriodos periodoAnt = listPeriodos.get(0);
				periodoAnt.setFhFinPeriodo(sdf.parse(sdf.format(cal.getTime())));
				
				
				tmct0ContaPeriodosDao.save(periodoAnt);
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(periodoAnt.getFhFinPeriodo()); // Configuramos la fecha que se recibe
				calendar.add(Calendar.DAY_OF_YEAR, 1);
				
				int yearAnt = calendar.get(Calendar.YEAR);
				
				Tmct0ContaPeriodos periodoNuevo = new Tmct0ContaPeriodos();
				periodoNuevo.setAnio(yearAnt);
				if(year != yearAnt){
					periodoNuevo.setPeriodo(1);
				}else{
					periodoNuevo.setPeriodo(listPeriodos.get(0).getPeriodo() + 1);
				}
				
				periodoNuevo.setFhIniPeriodo(sdf.parse(sdf.format(calendar.getTime())));
				periodoNuevo.setFhFinPeriodo(sdf.parse("9999-12-31"));
				periodoNuevo.setProceso("M");
				periodoNuevo.setCerradoPeriodo(0);
				
				tmct0ContaPeriodosDao.save(periodoNuevo);
				
				result.put("OK", "El periodo " + periodoNuevo.getPeriodo() + 
						         " para el año " + periodoNuevo.getAnio() + " se genero correctamente");
				
			} catch (Exception e) {
				
				result.put("KO", "Error en la creacion del periodo. Contacte con el administrador.");
				return result;
			}
		
		}
		
		return result;
	}
	
	@Transactional
	public void crearPeriodosTask() throws SIBBACBusinessException{
		
		
		Integer[] meses31 = {0,2,4,6,7,9,11};
		Integer[] meses30 = {3,5,8,10};
		
		Tmct0ContaPeriodos periodoNuevo = new Tmct0ContaPeriodos();
		
		do{
		
			List<Tmct0ContaPeriodos> listPeriodos = tmct0ContaPeriodosDao.findContaPeriodosOrderByFhFinPeriodo();
			
			if(listPeriodos!=null && !listPeriodos.isEmpty()){
				
				Date fechaFinPeriodo = listPeriodos.get(1).getFhFinPeriodo();
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(fechaFinPeriodo);
				
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				
				
				if(day > 25){
					day = 5;
					if(month == 11){
						month = 0;
						year = year + 1;
					}else{
						month = month + 1;
					}
				}else if(day < 25){
					day = day + 5;
				}else{
					if(Arrays.asList(meses31).contains(month)){
						day = 31;
					}else if(Arrays.asList(meses30).contains(month)){
						day = 30;
					}else{
						if(cal.isLeapYear(year)){
							day = 29;
						}else{
							day = 28;
						}
					}
				}
				
				
				cal.set(Calendar.DAY_OF_MONTH, day);
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.YEAR, year);
				
				try {
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Tmct0ContaPeriodos periodoAnt = listPeriodos.get(0);
					periodoAnt.setFhFinPeriodo(sdf.parse(sdf.format(cal.getTime())));
					
					
					tmct0ContaPeriodosDao.save(periodoAnt);
					
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(periodoAnt.getFhFinPeriodo()); // Configuramos la fecha que se recibe
					calendar.add(Calendar.DAY_OF_YEAR, 1);
					
					int yearAnt = calendar.get(Calendar.YEAR);
					
					periodoNuevo = new Tmct0ContaPeriodos();
					
					periodoNuevo.setAnio(yearAnt);
					if(year != yearAnt){
						periodoNuevo.setPeriodo(1);
					}else{
						periodoNuevo.setPeriodo(listPeriodos.get(0).getPeriodo() + 1);
					}
					
					periodoNuevo.setFhIniPeriodo(sdf.parse(sdf.format(calendar.getTime())));
					periodoNuevo.setFhFinPeriodo(sdf.parse("9999-12-31"));
					periodoNuevo.setProceso("A");
					periodoNuevo.setCerradoPeriodo(0);
					
					tmct0ContaPeriodosDao.save(periodoNuevo);
					
					LOG.debug("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":" + Task.GROUP_ACCOUNTING.JOB_PERIODOS_CONTABILIDAD + "] Los periodos se generaron correctamente");
					
				} catch (Exception e) {
					
					LOG.error("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":" + Task.GROUP_ACCOUNTING.JOB_PERIODOS_CONTABILIDAD + "] Error en el formato de fechas");
				}
			
			}
			
		}while(periodoNuevo.getPeriodo()!=72);
		
	}
}
