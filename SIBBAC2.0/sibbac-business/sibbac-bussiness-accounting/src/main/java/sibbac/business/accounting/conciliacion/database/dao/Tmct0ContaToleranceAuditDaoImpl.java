package sibbac.business.accounting.conciliacion.database.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaToleranceAudit;

@Repository
public class Tmct0ContaToleranceAuditDaoImpl {

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaToleranceAuditDaoImpl.class);

	public List<Tmct0ContaToleranceAudit> consultarToleranceAudit(String usuario, String codPlantilla, List<Date> listFechas) {

		LOG.info("INICIO - DAOIMPL - consultarToleranceAudit");

		List<Tmct0ContaToleranceAudit> listaToleranceAudit = new ArrayList<Tmct0ContaToleranceAudit>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		int sizeFechas = 1;

		try {
			
			if (null!=listFechas) {
				sizeFechas = listFechas.size();
			}

			for (int i=0;i<sizeFechas;i++) {

				List<Tmct0ContaToleranceAudit> listToleranceAudit = new ArrayList<Tmct0ContaToleranceAudit>();

				StringBuilder consulta = new StringBuilder("SELECT toleranceAudit FROM Tmct0ContaToleranceAudit toleranceAudit "
						+ "WHERE 1=1");
				if (null != usuario) {
					consulta.append(" AND toleranceAudit.auditUser = :usuario");
					parameters.put("usuario", usuario);
				}
				if (null!=listFechas) {
					consulta.append(" AND date(toleranceAudit.auditDate) = :auditDate");
					parameters.put("auditDate", listFechas.get(i));
				}
				if (null!=codPlantilla) {
					consulta.append(" AND toleranceAudit.codPlantilla = :codPlantilla");
					parameters.put("codPlantilla", codPlantilla);
				}		

				LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
				TypedQuery<Tmct0ContaToleranceAudit> query = entityManager.createQuery(consulta.toString(),
						Tmct0ContaToleranceAudit.class);

				for (Entry<String, Object> entry : parameters.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}

				// Se forman todas las entidades factura manual con los datos recuperados.
				listToleranceAudit = query.getResultList();

				for (int j=0;j<listToleranceAudit.size();j++) {
					listaToleranceAudit.add(listToleranceAudit.get(j));
				}
			}


		} catch (Exception e) {
			LOG.error("Error metodo consultarToleranceAudit -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarToleranceAudit");

		return listaToleranceAudit;
	}

}
