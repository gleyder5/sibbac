package sibbac.business.accounting.tasks;

import java.util.Date;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.database.bo.Tmct0ContaPeriodosBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ACCOUNTING.NAME,
name = Task.GROUP_ACCOUNTING.JOB_PERIODOS_CONTABILIDAD,
interval = 1,
intervalUnit = IntervalUnit.YEAR,
startDate = "2018-12-30",
startTime = "05:00:00")
public class TaskContaPeriodos extends SIBBACTask {
	
	
	@Autowired
	Tmct0menBo tmct0menBo;
	
	@Autowired
	Tmct0ContaPeriodosBo tmct0ContaPeriodosBo;
	
	@Override
	public void execute() {
		 
		 LOG.trace("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":" + Task.GROUP_ACCOUNTING.JOB_PERIODOS_CONTABILIDAD + "] Init");

		 Tmct0men tmct0men = null;
		 TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
		 TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
		 TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
		    
		 try {
			 
			 tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_CONTA_PERIODOS, estadoIni, estadoExe);
		    	
			 tmct0ContaPeriodosBo.crearPeriodosTask();
			 
			 tmct0menBo.putEstadoMEN(tmct0men, estadoIni);
			 
	     } catch (SIBBACBusinessException e) {
	    	 LOG.warn("[TaskPeriodosContables :: execute] Error intentando bloquear el semaforo ... " + e.getMessage());
	    	 tmct0menBo.putEstadoMEN(tmct0men, estadoError);
	     }
		 
		 LOG.trace("[" + new Date() + "][" + Task.GROUP_ACCOUNTING.NAME + " : " + ":" + Task.GROUP_ACCOUNTING.JOB_PERIODOS_CONTABILIDAD + "] Fin");

	}
}
