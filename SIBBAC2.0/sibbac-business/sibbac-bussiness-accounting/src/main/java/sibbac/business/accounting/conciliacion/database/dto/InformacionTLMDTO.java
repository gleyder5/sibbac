package sibbac.business.accounting.conciliacion.database.dto;

import org.apache.commons.lang.StringUtils;

/**
 * DTO para representar datos de Informacion TLM
 */
public class InformacionTLMDTO {

	private String usuario;
	private String fechaUltimoFichero;
	private String fechaBooking;
	private boolean informacionExistente;
	
	/**
	 *	Constructor no-arg. 
	 */
	public InformacionTLMDTO() {
		super();
	}

	/**
	 *	@param usuario
	 *	@param fechaUltimoFichero
	 *	@param fechaBooking
	 *	@param informacionExistente 
	 */
	public InformacionTLMDTO(
		String usuario, String fechaUltimoFichero, String fechaBooking, 
		boolean informacionExistente) {
		super();
		this.usuario = usuario;
		this.fechaUltimoFichero = fechaUltimoFichero;
		this.fechaBooking = fechaBooking;
		this.informacionExistente = informacionExistente;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaUltimoFichero() {
		return this.fechaUltimoFichero;
	}

	public void setFechaUltimoFichero(String fechaUltimoFichero) {
		this.fechaUltimoFichero = fechaUltimoFichero;
	}

	public String getFechaBooking() {
		return this.fechaBooking;
	}

	public void setFechaBooking(String fechaBooking) {
		this.fechaBooking = fechaBooking;
	}
	
	public boolean isInformacionExistente() {
		return this.informacionExistente;
	}

	public void setInformacionExistente(boolean informacionExistente) {
		this.informacionExistente = informacionExistente;
	}

	/**
	 *	Se popula el DTO.
	 *	@param cadena
	 *	@param fechaBooking
	 *	@return InformacionTLMDTO  
	 */
	public static InformacionTLMDTO convertObjectToDTO(String cadena, String fechaBooking) {
		InformacionTLMDTO dto = new InformacionTLMDTO();
		if (StringUtils.isNotBlank(cadena)) {
			String[] arr = cadena.split("\\|\\|");
			if (arr != null && arr.length == 2) {
				dto.setFechaUltimoFichero(arr[0].trim());
				dto.setUsuario(arr[1].trim());
			}
			dto.setInformacionExistente(true);
		} else {
			dto.setInformacionExistente(false);
		}
		dto.setFechaBooking(fechaBooking);

		return dto;
	}

}