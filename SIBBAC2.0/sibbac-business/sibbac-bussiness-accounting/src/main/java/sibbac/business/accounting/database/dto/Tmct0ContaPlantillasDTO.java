package sibbac.business.accounting.database.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Data Transfer Object para la gestión de TMCT0_CONTA_PLANTILLAS.
 * 
 * @author Neoris
 *
 */
public class Tmct0ContaPlantillasDTO {

	/** Campos del DTO */

	private Integer id;

	private String codigo;

	private String tipo;

	private String subtipo;

	private String proceso;

	private String subproceso;

	private Integer activo;

	private String fichero;

	private String tipo_comprobante;

	private Integer num_comprobante;
	
	private Date fecha_comprobante;
	
	private String periodo_comprobante;

	private String campo;

	private String campo_estado;

	private String estado_inicio;

	private String estado_final;
	
	private String cuenta_debe;

	private String aux_debe;

	private String cuenta_haber;

	private String aux_haber;

	private String cuenta_diferencias;

	private String query_orden;

	private String query_cobro;

	private String query_final;

	private String fichero_orden;

	private String fichero_cobro;

	private String cuenta_bancaria;
	
	private Date audit_date;
	  
	private String query_bloqueo;
	
	private BigDecimal importe;
	
	private String concepto;
	
	private BigDecimal importeDebe;
	
	private BigDecimal importeHaber;
	
	private BigDecimal balance;
	private boolean isErroresFichero = false;
	
	private String textoErrorFichero;
	
	private boolean isTipoCuentaDebe = false;
	
	private boolean sinBalance = false;
	
	private List<Object> paramsDetail;
	
	private BigDecimal tolerance;

	public Tmct0ContaPlantillasDTO() {
	};

	@Override
	public String toString() {
		return "Tmct0ContaPlantillasDTO [codigo=" + codigo + ", tipo=" + tipo + ", campo=" + campo + ", campo_estado="
				+ campo_estado + ", cuenta_debe=" + cuenta_debe + ", aux_debe=" + aux_debe + ", cuenta_haber="
				+ cuenta_haber + ", aux_haber=" + aux_haber + ", importe=" + importe + ", concepto=" + concepto
				+ ", importeDebe=" + importeDebe + ", importeHaber=" + importeHaber + "]";
	}
	
	

	public BigDecimal getTolerance() {
    return tolerance;
  }

  public void setTolerance(BigDecimal tolerance) {
    this.tolerance = tolerance;
  }

  public Boolean isPlantillaSinBalance() {
		return (!BigDecimal.ZERO.equals(balance));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSubtipo() {
		return subtipo;
	}

	public void setSubtipo(String subtipo) {
		this.subtipo = subtipo;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public String getSubproceso() {
		return subproceso;
	}

	public void setSubproceso(String subproceso) {
		this.subproceso = subproceso;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public String getFichero() {
		return fichero;
	}

	public void setFichero(String fichero) {
		this.fichero = fichero;
	}

	public String getTipo_comprobante() {
		return tipo_comprobante;
	}

	public void setTipo_comprobante(String tipo_comprobante) {
		this.tipo_comprobante = tipo_comprobante;
	}

	public Integer getNum_comprobante() {
		return num_comprobante;
	}

	public void setNum_comprobante(Integer num_comprobante) {
		this.num_comprobante = num_comprobante;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getCampo_estado() {
		return campo_estado;
	}

	public void setCampo_estado(String campo_estado) {
		this.campo_estado = campo_estado;
	}

	public String getEstado_inicio() {
		return estado_inicio;
	}

	public void setEstado_inicio(String estado_inicio) {
		this.estado_inicio = estado_inicio;
	}

	public String getEstado_final() {
		return estado_final;
	}

	public void setEstado_final(String estado_final) {
		this.estado_final = estado_final;
	}
	
	public String getCuenta_debe() {
		return cuenta_debe;
	}

	public void setCuenta_debe(String cuenta_debe) {
		this.cuenta_debe = cuenta_debe;
	}

	public String getAux_debe() {
		return aux_debe;
	}

	public void setAux_debe(String aux_debe) {
		this.aux_debe = aux_debe;
	}

	public String getCuenta_haber() {
		return cuenta_haber;
	}

	public void setCuenta_haber(String cuenta_haber) {
		this.cuenta_haber = cuenta_haber;
	}

	public String getAux_haber() {
		return aux_haber;
	}

	public void setAux_haber(String aux_haber) {
		this.aux_haber = aux_haber;
	}

	public String getCuenta_diferencias() {
		return cuenta_diferencias;
	}

	public void setCuenta_diferencias(String cuenta_diferencias) {
		this.cuenta_diferencias = cuenta_diferencias;
	}

	public String getQuery_orden() {
		return query_orden;
	}

	public void setQuery_orden(String query_orden) {
		this.query_orden = query_orden;
	}

	public String getQuery_cobro() {
		return query_cobro;
	}

	public void setQuery_cobro(String query_cobro) {
		this.query_cobro = query_cobro;
	}

	public String getQuery_final() {
		return query_final;
	}

	public void setQuery_final(String query_final) {
		this.query_final = query_final;
	}

	public String getFichero_orden() {
		return fichero_orden;
	}

	public void setFichero_orden(String fichero_orden) {
		this.fichero_orden = fichero_orden;
	}

	public String getFichero_cobro() {
		return fichero_cobro;
	}

	public void setFichero_cobro(String fichero_cobro) {
		this.fichero_cobro = fichero_cobro;
	}

	public String getCuenta_bancaria() {
		return cuenta_bancaria;
	}

	public void setCuenta_bancaria(String cuenta_bancaria) {
		this.cuenta_bancaria = cuenta_bancaria;
	}

	public Date getAudit_date() {
		return audit_date;
	}

	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	}

	public String getQuery_bloqueo() {
		return query_bloqueo;
	}

	public void setQuery_bloqueo(String query_bloqueo) {
		this.query_bloqueo = query_bloqueo;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getPeriodo_comprobante() {
		return periodo_comprobante;
	}

	public void setPeriodo_comprobante(String periodo_comprobante) {
		this.periodo_comprobante = periodo_comprobante;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Date getFecha_comprobante() {
		return fecha_comprobante;
	}

	public void setFecha_comprobante(Date fecha_comprobante) {
		this.fecha_comprobante = fecha_comprobante;
	}

	public BigDecimal getImporteDebe() {
		return importeDebe;
	}

	public void setImporteDebe(BigDecimal importeDebe) {
		this.importeDebe = importeDebe;
	}

	public BigDecimal getImporteHaber() {
		return importeHaber;
	}

	public void setImporteHaber(BigDecimal importeHaber) {
		this.importeHaber = importeHaber;
	}
	
	public boolean isErroresFichero() {
		return this.isErroresFichero;
	}

	public void setErroresFichero(boolean isErroresFichero) {
		this.isErroresFichero = isErroresFichero;
	}

	public String getTextoErrorFichero() {
		return this.textoErrorFichero;
	}

	public void setTextoErrorFichero(String textoErrorFichero) {
		this.textoErrorFichero = textoErrorFichero;
	}

	public String getFicheroConValor() {
		if (StringUtils.isNotBlank(this.fichero)) {
			return this.fichero;
		} else {
			return "";
		}
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public boolean isSinBalance() {
		return this.sinBalance;
	}

	public void setSinBalance(boolean sinBalance) {
		this.sinBalance = sinBalance;
	}

	public boolean isTipoCuentaDebe() {
		return this.isTipoCuentaDebe;
	}

	public void setTipoCuentaDebe(boolean isTipoCuentaDebe) {
		this.isTipoCuentaDebe = isTipoCuentaDebe;
	}

	public List<Object> getParamsDetail() {
		return paramsDetail;
	}

	public void setParamsDetail(List<Object> paramsDetail) {
		this.paramsDetail = paramsDetail;
	}
}
