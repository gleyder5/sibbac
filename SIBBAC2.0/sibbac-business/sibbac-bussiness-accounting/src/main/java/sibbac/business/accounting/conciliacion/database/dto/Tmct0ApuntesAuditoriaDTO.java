package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

public class Tmct0ApuntesAuditoriaDTO {
	
	private BigInteger id;
	
	private Timestamp feApunte;
	
	private String usuApunte;
	
	private String concepto;
	
	private BigInteger idConcepto;
	
	private BigDecimal importeDebe;
	
	private BigDecimal importeHaber;
	
	private BigDecimal importePyG;
	
	private String estado;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ApuntesAuditoriaDTO() {
		super();
	}

	/**
	 * @param id
	 * @param feApunte
	 * @param usuApunte
	 * @param concepto
	 * @param importeDebe
	 * @param importeHaber
	 * @param importePyG
	 */
	public Tmct0ApuntesAuditoriaDTO(BigInteger id, Timestamp feApunte,
			String usuApunte, String concepto, BigInteger idConcepto, BigDecimal importeDebe,
			BigDecimal importeHaber, BigDecimal importePyG, String estado) {
		this.id = id;
		this.feApunte = feApunte;
		this.usuApunte = usuApunte;
		this.concepto = concepto;
		this.idConcepto=idConcepto;
		this.importeDebe = importeDebe;
		this.importeHaber = importeHaber;
		this.importePyG = importePyG;
		this.estado=estado;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the idConcepto
	 */
	public BigInteger getIdConcepto() {
		return idConcepto;
	}

	/**
	 * @param idConcepto the idConcepto to set
	 */
	public void setIdConcepto(BigInteger idConcepto) {
		this.idConcepto = idConcepto;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the feApunte
	 */
	public Timestamp getFeApunte() {
		return feApunte;
	}

	/**
	 * @param feApunte the feApunte to set
	 */
	public void setFeApunte(Timestamp feApunte) {
		this.feApunte = feApunte;
	}

	/**
	 * @return the usuApunte
	 */
	public String getUsuApunte() {
		return usuApunte;
	}

	/**
	 * @param usuApunte the usuApunte to set
	 */
	public void setUsuApunte(String usuApunte) {
		this.usuApunte = usuApunte;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the importeDebe
	 */
	public BigDecimal getImporteDebe() {
		return importeDebe;
	}

	/**
	 * @param importeDebe the importeDebe to set
	 */
	public void setImporteDebe(BigDecimal importeDebe) {
		this.importeDebe = importeDebe;
	}

	/**
	 * @return the importeHaber
	 */
	public BigDecimal getImporteHaber() {
		return importeHaber;
	}

	/**
	 * @param importeHaber the importeHaber to set
	 */
	public void setImporteHaber(BigDecimal importeHaber) {
		this.importeHaber = importeHaber;
	}

	/**
	 * @return the importePyG
	 */
	public BigDecimal getImportePyG() {
		return importePyG;
	}

	/**
	 * @param importePyG the importePyG to set
	 */
	public void setImportePyG(BigDecimal importePyG) {
		this.importePyG = importePyG;
	}

}
