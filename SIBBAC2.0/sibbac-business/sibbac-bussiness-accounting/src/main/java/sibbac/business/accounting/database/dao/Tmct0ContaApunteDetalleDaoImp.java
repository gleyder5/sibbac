package sibbac.business.accounting.database.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.TextosSustitucionPlantillas;

/**
 * Data access object de detalle de conta apunte.
 * 
 * @author Neoris
 *
 */
@Repository
public class Tmct0ContaApunteDetalleDaoImp {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaApunteDetalleDaoImp.class);

  @PersistenceContext
  private EntityManager em;
  
  @Value("${sibbac.accounting.intervalo.ejecuciones.queries}")
  private String nroIntervaloEjecuciones;

  /**
   * Ejecuta un insert select para generar los conta apunte detalles
   *
   * @param queryString queryString
   * @param idApunte idApunte
   * @param estado estado
   * @param fechaEjecucion fechaEjecucion
   * @param tipoPlantilla tipoPlantilla
   * @param isFromApuntesManuales: true si viene de la funcionalidad de apuntes manuales
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectDevengos(String queryString,
                                   Integer idApunte,
                                   String estado,
                                   List<String> listImportDet,
                                   String codigoPlantilla,
                                   List<Object> paramsDetail,
                                   int iImporte, 
                                   Boolean isFromApuntesManuales) throws EjecucionPlantillaException {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_APUNTE_DETALLE.getValue(), new Date());

    String querySustituir = getQuerySinImportes(queryString, iImporte).replaceAll(TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto(), listImportDet.get(iImporte));

    String insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, AUDIT_DATE, NBOOKING, NUORDEN, NUCNFLIQ, NUCNFCLT) " + querySustituir;

    Loggers.logTraceQueriesAccounting(codigoPlantilla, insert, true);

    Query query = em.createNativeQuery(insert);

    LOG.trace(" Parametro de la query " + idApunte);
    query.setParameter(1, idApunte);

    // Parametros necesarios solo si no se ejecutan con apuntes manuales
    if (!isFromApuntesManuales) {
    	
        LOG.trace(" Parametro de la query " + estado);
        query.setParameter(2, estado);

        for(int i=0;i<paramsDetail.size();i++){
        	LOG.trace(" Parametro de la query " + paramsDetail.get(i));
        	query.setParameter((i+3), paramsDetail.get(i));
        }
   	
    }
    query.executeUpdate();

  }

  /**
   * Ejecuta un insert select para generar los conta apunte detalles
   *
   * @param queryString queryString
   * @param idApunte idApunte
   * @param estado estado
   * @param fechaEjecucion fechaEjecucion
 * @param iImporte 
   * @param tipoPlantilla tipoPlantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectDevengosFallidos(String queryString,
                                           Integer idApunte,
                                           String estado,
                                           List<String> listImportDet,
                                           List<Object> paramsDetail,
                                           String codigoPlantilla, int iImporte) throws EjecucionPlantillaException {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_APUNTE_DETALLE.getValue(), new Date());

    String querySustituir = getQuerySinImportes(queryString, iImporte).replaceAll(TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto(), listImportDet.get(iImporte));

    String insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, IDFALLIDO, AUDIT_DATE) "
                    + querySustituir;

    Loggers.logTraceQueriesAccounting(codigoPlantilla, insert, true);

    Query query = em.createNativeQuery(insert);
    LOG.trace(" Parametro de la query " + idApunte);
    query.setParameter(1, idApunte);
    LOG.trace(" Parametro de la query " + estado);
    query.setParameter(2, estado);

    for(int i=0;i<paramsDetail.size();i++){
    	LOG.trace(" Parametro de la query " + paramsDetail.get(i));
    	query.setParameter((i+3), paramsDetail.get(i));
    }

    query.executeUpdate();

  }

	/**
	 * Obtiene la query sin las expresiones de los importes en el select y
	 * adiciona el condicional de importe > 0 en el where
	 * 
	 * @param query
	 * @param iImporte
	 * @return
	 */
	public String getQuerySinImportes(String query, int iImporte) {

		// Defino la select completa final
		String selectCompletaSinImportes = "";

		// Divido por los UNION
		String[] selects = query.toUpperCase().split((Pattern.quote("UNION")));

		// Por cada select entre los unions
		int iSelect = 0;
		for (String select : selects) {

			// Tomo el fragmento de select que contiene los importes
			select = select.toUpperCase().trim();
			int comienzoImportes = select.indexOf("SELECT") + 6;
			int finImportes = select.indexOf("?1");

			if (finImportes < 0)
				return "";
			String fragmentoImportes = select.substring(comienzoImportes, finImportes);

			// Obtengo todos los importes contenidos dentro de la select y me
			// quedo con la expresion que representa el indice de importe
			// procesado de la plantilla
			String expresionImporteProcesado = this.getExpresionImporte(fragmentoImportes, iImporte);
			String expresionCondicionalWhere = " (" + expresionImporteProcesado + ") != 0 AND ";

			String selectSinImportes = select.replace(fragmentoImportes,
					" " + TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto() + " ");

			int comienzoCondicionales = selectSinImportes.lastIndexOf("WHERE");
			selectSinImportes = this.insert(selectSinImportes, expresionCondicionalWhere, comienzoCondicionales + 5);

			if (iSelect == 0) {
				selectCompletaSinImportes += selectSinImportes;
			} else {
				selectCompletaSinImportes += " UNION " + selectSinImportes;
			}

			iSelect++;

		}
		return selectCompletaSinImportes;

	}

	private String insert(String bag, String marble, int index) {
		String bagBegin = bag.substring(0, index);
		String bagEnd = bag.substring(index);
		return bagBegin + marble + bagEnd;
	}

	private String getExpresionImporte(String exp, int i) {

		exp = exp.trim();
		if (",".equals(exp.substring(exp.length() - 1, exp.length()))) {
			exp = exp.substring(0, exp.length() - 1).trim();
		}

		String expImporte = exp.split((Pattern.quote("IMPORTE")))[i];
		if (",".equals(expImporte.substring(0, 1))) {
			expImporte = expImporte.substring(1, expImporte.length() - 1);
		} else if ("_".equals(expImporte.substring(0, 1))) {
			int posFinAliasAnterior = expImporte.indexOf(",");
			expImporte = expImporte.substring(posFinAliasAnterior + 1, expImporte.length());
		}

		return expImporte.trim();

	}

  /**
   * Ejecuta un insert select para generar los conta apunte detalles
   *
   * @param queryString queryString
   * @param idApunte idApunte
 * @param iImporte 
   * @param estado estado
   * @param fechaEjecucion fechaEjecucion
   * @param tipoPlantilla tipoPlantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectCobrosLiquidaciones(String queryString,
                                              Integer idApunte,
                                              List<Object> paramsDetail,
                                              List<String> listImportDet,
                                              String codigoPlantilla, int iImporte) throws EjecucionPlantillaException {

    String querySustituir = getQuerySinImportes(queryString, iImporte).replaceAll(TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto(), 
    		listImportDet.get(iImporte));


    String insert = null;
    insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, AUDIT_DATE, NBOOKING, NUORDEN, NUCNFLIQ, NUCNFCLT) "
             + querySustituir.replaceAll(";", "");

    Loggers.logTraceQueriesAccounting(codigoPlantilla, insert, true);
    Query query = em.createNativeQuery(insert);
    LOG.trace(" Parametro de la query " + idApunte);
    query.setParameter(1, idApunte);
    
    for(int i=0;i<paramsDetail.size();i++){
    	LOG.trace(" Parametro de la query " + paramsDetail.get(i));
    	query.setParameter((i+2), paramsDetail.get(i));
    }

    query.executeUpdate();

  }
  
  /**
   * Ejecuta un insert select para generar los conta apunte detalles
   *
   * @param queryString queryString
   * @param idApunte idApunte
 * @param iImporte 
   * @param estado estado
   * @param fechaEjecucion fechaEjecucion
   * @param tipoPlantilla tipoPlantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void generarApuntesDetalleLiquidacionesParciales(String queryString,
                                              Integer idApunte,
                                              List<Object> paramsDetail,
                                              BigDecimal importeCobroRest,
                                              List<String> listImportDet,
                                              String codigoPlantilla, int iImporte) throws EjecucionPlantillaException {

	    String querySustituir = getQuerySinImportes(queryString, iImporte).replaceAll(TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto(), 
	    		listImportDet.get(iImporte)).replaceAll(";", "").replaceAll("\\?1", String.valueOf(idApunte));
	
	    Loggers.logTraceQueriesAccounting(codigoPlantilla, querySustituir, true);
	    Query query = em.createNativeQuery(querySustituir);
	    
	    for(int i=0;i<paramsDetail.size();i++){
	    	LOG.trace(" Parametro de la query " + paramsDetail.get(i));
	    	query.setParameter((i+2), paramsDetail.get(i));
	    }
	
	    List<Object[]> listAlcs = query.getResultList();
	  
	    
	    String insertQuery = "";
	    BigDecimal importeCobroRestAlcs = importeCobroRest;
	    
	    for(int i=0;i<listAlcs.size();i++){
	    	
	    	if(importeCobroRestAlcs.compareTo(BigDecimal.ZERO)==1){
		    	insertQuery = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, AUDIT_DATE, NBOOKING, NUORDEN, NUCNFLIQ, NUCNFCLT) VALUES (?1,?2,?3,?4,?5,?6,?7)"; 
		    				 
		    	Query insert = em.createNativeQuery(insertQuery);
		    	BigDecimal importeALC =  importeCobroRestAlcs.compareTo((BigDecimal) listAlcs.get(i)[0])== 1?(BigDecimal) listAlcs.get(i)[0]:importeCobroRestAlcs;
		    	insert.setParameter(1, importeALC);
		    	insert.setParameter(2, listAlcs.get(i)[1]);
		    	insert.setParameter(3, listAlcs.get(i)[2]);
		    	insert.setParameter(4, listAlcs.get(i)[3]);
		    	insert.setParameter(5, listAlcs.get(i)[4]);
		    	insert.setParameter(6, listAlcs.get(i)[5]);
		    	insert.setParameter(7, listAlcs.get(i)[6]);
		    	
		    	insert.executeUpdate();
		    	
		    	importeCobroRestAlcs = importeCobroRestAlcs.subtract((BigDecimal) listAlcs.get(i)[0]);
	    	}
	    	
	    }

  }

  /**
   * Ejecuta un insert select para generar los conta apunte detalles
   *
   * @param queryString queryString
   * @param idApunte idApunte
   * @param estado estado
   * @param iImporte 
   * @param fechaEjecucion fechaEjecucion
   * @param tipoPlantilla tipoPlantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectFacturas(String queryString,
                                   Integer idApunte,
                                   String estado,
                                   List<Object> paramsDetail,
                                   List<String> listImportDet,
                                   String codigoPlantilla, int iImporte) throws EjecucionPlantillaException {

    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_APUNTE_DETALLE.getValue(), null);

    String querySustituir = getQuerySinImportes(queryString, iImporte).replaceAll(TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto(), listImportDet.get(iImporte));

    String insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, IDFACTURA, AUDIT_DATE) "
                    + querySustituir;

    Loggers.logTraceQueriesAccounting(codigoPlantilla, insert, true);

    Query query = em.createNativeQuery(insert);
    LOG.trace(" Parametro de la query " + idApunte);
    query.setParameter(1, idApunte);
    LOG.trace(" Parametro de la query " + estado);
    query.setParameter(2, estado);
    
    for(int i=0;i<paramsDetail.size();i++){
    	LOG.trace(" Parametro de la query " + paramsDetail.get(i));
    	query.setParameter((i+3), paramsDetail.get(i));
    }

    query.executeUpdate();

  }
  
  
  /**
   * Ejecuta un insert select para generar los conta apunte detalles
   *
   * @param queryString queryString
   * @param idApunte idApunte
   * @param estado estado
 * @param iImporte 
   * @param fechaEjecucion fechaEjecucion
   * @param tipoPlantilla tipoPlantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectFacturasManuales(String queryString,
                                   Integer idApunte,
                                   String estado,
                                   BigInteger nDocNumero,
                                   BigDecimal importe,
                                   String codigoPlantilla) throws EjecucionPlantillaException {

	    Loggers.logDebugQueriesAccounting(codigoPlantilla, SibbacEnums.ConstantesQueries.QUERY_APUNTE_DETALLE.getValue(),null);
	
	    String nbDocNumeroString = String.valueOf(nDocNumero.longValue());
	
	    String insert = ""; 
	    if(nbDocNumeroString.startsWith("9")){
	    	if(nbDocNumeroString.startsWith("98")){
	    		insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, ID_FACTURA_MB, AUDIT_DATE) ";
	    	}else{
	    		insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, ID_FACTURA_MRB, AUDIT_DATE) ";
	    	}
	    }else{
	    	if(nbDocNumeroString.startsWith("8")){
	    		insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, ID_FACTURA_M, AUDIT_DATE) ";
	    	}else{
	    		insert = "INSERT INTO bsnbpsql.TMCT0_CONTA_APUNTE_DETALLE (IMPORTE, IDAPUNTE, ID_FACTURA_MR, AUDIT_DATE) ";
	    	}
	    }
	    
	    insert = insert + queryString;
	
	    Loggers.logTraceQueriesAccounting(codigoPlantilla, insert, true);
	
	    Query query = em.createNativeQuery(insert);
	    LOG.trace(" Parametro de la query " + importe);
	    query.setParameter(1, importe);
	    LOG.trace(" Parametro de la query " + idApunte);
	    query.setParameter(2, idApunte);
	    LOG.trace(" Parametro de la query " + estado);
	    query.setParameter(3, estado);
	    LOG.trace(" Parametro de la query " + estado);
	    query.setParameter(4, nDocNumero);
	
	    query.executeUpdate();
	    
  }

  /** 
   * 	Ejecuta un insert select para generar los conta apunte detalles. 
 * @param iImporte 
   * 	@param queryString: Query a ejecutar
   * 	@param grabaDetalle: campo graba detalle
   * 	@param listImportDet: lista de importes
   * 	@param codigoPlantilla: codigo Plantilla
   * 	@param params: parametros necesarios para ejecutar la query
   * 	@throws EjecucionPlantillaException
   */
  @Modifying
  @Transactional
  public void insertSelect(String queryString,
                           Character grabaDetalle,
                           List<String> listImportDet,
                           String codigoPlantilla, int iImporte,
                           Object... params) throws EjecucionPlantillaException {
    try {
      if (!StringUtils.isEmpty(grabaDetalle)) {
        List<Integer> idsApuntes = new ArrayList<Integer>();

        String querySustituir = getQuerySinImportes(queryString, iImporte).replaceAll(TextosSustitucionPlantillas.SUSTITUIR_IMPORTE.getTexto(), listImportDet.get(iImporte));

        StringBuffer queryInsert = new StringBuffer();
        queryInsert.append("INSERT INTO TMCT0_CONTA_APUNTE_DETALLE(IMPORTE, IDAPUNTE, AUDIT_DATE, NBOOKING, NUORDEN, NUCNFLIQ, NUCNFCLT) ")
                   .append(querySustituir);

        Loggers.logTraceQueriesAccounting(codigoPlantilla, queryInsert.toString(), true);

        // Se fija intervalo de registros en que se ejecutara la query de desbloqueo
    	Integer nroIntervaloEjec = AccountingHelper.getNroIntervalosEjecucion(nroIntervaloEjecuciones);
        
        // Si graba detalle es Debe o Ambos, se guarda el detalle para el apunte del debe.
        Integer idApunteDebe = (Integer) params[0];
        if ((grabaDetalle.equals('D') || grabaDetalle.equals('A')) && idApunteDebe != null) {
        	idsApuntes.add(idApunteDebe);

	    	// De esta forma se ahorra la ejecucion de la query de conteo si se ingreso un valor
	    	// no apropiado en el fichero .properties
	    	if (nroIntervaloEjec > 0) {
	    		
	    		int resultadoConteo = this.getConteoApuntesDetalle(
	    			nroIntervaloEjec, idApunteDebe, (Long) params[2], codigoPlantilla, queryString);
	      		
	      		this.ejecutarQueryApunteDetallePorIntervalos(
	      			querySustituir, idApunteDebe, (Long) params[2], 
	      			nroIntervaloEjec, resultadoConteo, codigoPlantilla);
	    	} else {
      			// Inserta apuntes sin iteracion
	    		this.insertarApuntesDetalleIndividual(queryInsert, idApunteDebe, (Long) params[2], codigoPlantilla);
	    	}
        }

        // Si graba detalle es Haber o Ambos, se guarda el detalle para el apunte del haber.
        Integer idApunteHaber = (Integer) params[1];
        if ((grabaDetalle.equals('H') || grabaDetalle.equals('A')) && idApunteHaber != null) {
        	idsApuntes.add(idApunteHaber);

        	// De esta forma se ahorra la ejecucion de la query de conteo si se ingreso un valor
	    	// no apropiado en el fichero .properties
	    	if (nroIntervaloEjec > 0) {
	          	int resultadoConteo = this.getConteoApuntesDetalle(
		    			nroIntervaloEjec, idApunteHaber, (Long) params[2], codigoPlantilla, queryString);
	      		
	      		this.ejecutarQueryApunteDetallePorIntervalos(
	      			querySustituir, idApunteHaber, (Long) params[2], 
	      			nroIntervaloEjec, resultadoConteo, codigoPlantilla);
	    	} else {
	    		// Se inserta apunte sin rango - forma convencional
	    		this.insertarApuntesDetalleIndividual(queryInsert, idApunteHaber, (Long) params[2], codigoPlantilla);
	    	}
        }
      }
    } catch (Exception e) {
      Loggers.logErrorAccounting(codigoPlantilla,
    		  ConstantesError.ERROR_APUNTES_DETALLE.getValue());
      throw new EjecucionPlantillaException(ConstantesError.ERROR_APUNTES_DETALLE.getValue());

    }
  }
  
  /**
   * 	Se inserta apunte sin rango - forma convencional.
   * 	@param queryInsert: Query a procesar
   * 	@param idApunte: Identificador del apunte
   * 	@param idMovimiento: Id de movimiento
   * 	@param codigoPlantilla: Codigo de plantilla
   * 	@throws EjecucionPlantillaException 
   */
  @Modifying
  @Transactional
  public void insertarApuntesDetalleIndividual(
		  StringBuffer queryInsert, Integer idApunte, 
		  Long idMovimiento, String codigoPlantilla) 
		  throws EjecucionPlantillaException {
	  	// Inserta apuntes sin iteracion
		String queryInsertReempl = queryInsert.toString();
		Query query = em.createNativeQuery(queryInsertReempl);
		Loggers.logTraceQueriesAccounting(codigoPlantilla, String.valueOf(idApunte), false);
		query.setParameter(1, (Integer) idApunte);
		Loggers.logTraceQueriesAccounting(codigoPlantilla, String.valueOf(idMovimiento), false);
		query.setParameter(2, idMovimiento);
		query.executeUpdate();	  
  }
  
  /**
   *	Ejecucion por intervalos de la query de apunte de detalle.
   *	@param querySustituir: Query a procesar
   *	@param idApunte: Id de apunte al debe o haber segun el caso
   *	@param idMovimiento: Id de movimiento
   *	@param nroIntervaloEjec: nro de intervalo de ejecucion
   *	@param codigoPlantilla: codigo de plantilla 
   *	@param resultadoConteo: resultado del conteo
   *	@throws EjecucionPlantillaException  
   */
  @Modifying
  @Transactional
  private void ejecutarQueryApunteDetallePorIntervalos(
		  String querySustituir, Integer idApunte, Long idMovimiento, 
		  int nroIntervaloEjec, int resultadoConteo, 
		  String codigoPlantilla) throws EjecucionPlantillaException {

	  StringBuffer queryInsert = new StringBuffer();
	  StringBuffer querySustituirSb = new StringBuffer();

	  // Este es el intervalo de registros por cada iteracion
	  int intervalo = resultadoConteo / nroIntervaloEjec;
	  int indexFrom = querySustituir.toUpperCase().indexOf("FROM");

	  querySustituirSb
	  	.append(querySustituir.substring(0, indexFrom))
	  	.append(", ROW_NUMBER() OVER(ORDER BY alc.NBOOKING, alc.NUORDEN, alc.NUCNFLIQ, alc.NUCNFCLT) AS rownumber ")
	  	.append(querySustituir.substring(indexFrom, querySustituir.length()));
	  	
	  queryInsert
	  	.append("INSERT INTO TMCT0_CONTA_APUNTE_DETALLE(IMPORTE, IDAPUNTE, AUDIT_DATE, NBOOKING, NUORDEN, NUCNFLIQ, NUCNFCLT) ")
	  	.append("SELECT IMPORTE, ?1, CURRENT TIMESTAMP, alcin.NBOOKING, alcin.NUORDEN, alcin.NUCNFLIQ, alcin.NUCNFCLT FROM ( ")
	  	.append(querySustituirSb.toString())
	  	.append(" ) AS alcin WHERE alcin.rownumber BETWEEN ?3 AND ?4 ");
		
	  for (int j = 0; j < intervalo; j++) {
		  String queryInsertReempl = queryInsert.toString();
		  queryInsertReempl = queryInsertReempl
				  .replace("?1", String.valueOf(idApunte))
				  .replace("?2", String.valueOf(idMovimiento))
				  .replace("?3", String.valueOf(j * nroIntervaloEjec))		  
				  .replace("?4", String.valueOf(((j + 1) * nroIntervaloEjec) - 1));
		  
		  Loggers.logTraceGenericoAccounting(
				  codigoPlantilla, 
				  "Rango de registros a insertar en Query_Apunte_Detalle: " 
				  + String.valueOf(j * nroIntervaloEjec) + " -> " 
				  + String.valueOf(((j + 1) * nroIntervaloEjec) - 1));
		  Loggers.logTraceQueriesAccounting(
				  codigoPlantilla, String.valueOf(idApunte), false);
		  Loggers.logTraceQueriesAccounting(
				  codigoPlantilla, String.valueOf(idMovimiento), false);
		  Loggers.logTraceQueriesAccounting(
				  codigoPlantilla, String.valueOf(j * nroIntervaloEjec), false);
		  Loggers.logTraceQueriesAccounting(
				  codigoPlantilla, String.valueOf(((j + 1) * nroIntervaloEjec) - 1), false);
		  
          Query query = em.createNativeQuery(queryInsertReempl);
          query.executeUpdate();
	  }
	  
	  // Query adicional para el resto de la division
	  String queryInsertReempl = queryInsert.toString();
	  queryInsertReempl = queryInsertReempl
			.replace("?1", String.valueOf(idApunte))
			.replace("?2", String.valueOf(idMovimiento))
	  		.replace("?3", String.valueOf(intervalo * nroIntervaloEjec))		  
	  		.replace("?4", String.valueOf(resultadoConteo));
	  
	  Loggers.logTraceGenericoAccounting(
			  codigoPlantilla, 
			  "Rango de registros a insertar en Query_Apunte_Detalle: " 
			  + String.valueOf(intervalo * nroIntervaloEjec) + " -> " 
			  + String.valueOf(resultadoConteo));
	  Loggers.logTraceQueriesAccounting(
			  codigoPlantilla, String.valueOf(idApunte), false);
	  Loggers.logTraceQueriesAccounting(
			  codigoPlantilla, String.valueOf(idMovimiento), false);
	  Loggers.logTraceQueriesAccounting(
			  codigoPlantilla, String.valueOf(intervalo * nroIntervaloEjec), false);
	  Loggers.logTraceQueriesAccounting(
			  codigoPlantilla, String.valueOf(resultadoConteo), false);
	  
	  Query query = em.createNativeQuery(queryInsertReempl);
      query.executeUpdate();
  }
  
  /**
   *	Se obtiene conteo de apuntes de detalle.
   *	@param nroIntervaloEjec
   *	@param idApunte
   *	@param idMovimiento
   *	@param codigoPlantilla
   *	@param queryString
   *	@return int: Conteo
   *	@throws EjecucionPlantillaException 
   */
  private int getConteoApuntesDetalle(Integer nroIntervaloEjec,
		  Integer idApunte, Long idMovimiento, String codigoPlantilla,
		  String queryString) throws EjecucionPlantillaException {

		Long resultadoConteo = new Long(0);

		// De esta forma se ahorra la ejecucion de la query de conteo si se
		// ingreso un valor
		// no apropiado en el fichero .properties
		if (nroIntervaloEjec > 0) {
			// Parametros a pasar al conteo
			ArrayList parametros = new ArrayList<>();
			parametros.add((Integer) idApunte);
			parametros.add(idMovimiento);

			// Realiza conteo de la query de desbloqueo para calcular el rango
			// de ejecucion
			resultadoConteo = this.executeQueryCount(queryString, codigoPlantilla, parametros);
		}
		return (resultadoConteo != null ? resultadoConteo.intValue() : 0);
	}
  
  /**
   * 	Ejecuta un COUNT nativo.
   * 	@param queryStr: Query a la que se le realizara un conteo nativo
   * 	@param params: Parametros a pasar
   * 	@return Resultado unico con conteo
   * 	@throws EjecucionPlantillaException
   */
  @Transactional
  public Long executeQueryCount(String queryStr, String codigoPlantilla, List params) 
		  throws EjecucionPlantillaException {

    Loggers.logTraceQueriesAccounting(codigoPlantilla, queryStr, true);
    StringBuffer conteoQuery = new StringBuffer();
    conteoQuery.append("SELECT COUNT(*) FROM (");
    conteoQuery.append(queryStr.replaceAll(";", ""));
    conteoQuery.append(")");
    
    String conteoQueryReempl = conteoQuery.toString();

    if (CollectionUtils.isNotEmpty(params)) {
    	for (int j = 0; j < params.size(); j++) {
    		if (params.get(j).getClass() == String.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), "'" + (String) params.get(j) + "'");
    		} else if (params.get(j).getClass() == Integer.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), ((Integer) params.get(j)).toString());
    		} else if (params.get(j).getClass() == Long.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), ((Long) params.get(j)).toString());
    		} else if (params.get(j).getClass() == BigInteger.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), ((BigInteger) params.get(j)).toString());
    		} else if (params.get(j).getClass() == Boolean.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), ((Boolean) params.get(j)).toString());
    		} else if (params.get(j).getClass() == Character.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), "'" + (Character) params.get(j) + "'");
    		} else if (params.get(j).getClass() == BigDecimal.class) {
    			conteoQueryReempl = conteoQueryReempl.replace("?" + (j + 1), ((BigDecimal) params.get(j)).toString());
    		}
    	}
    }
    
    Query query = em.createNativeQuery(conteoQueryReempl);
    
    Integer resultadoConteo = (Integer) query.getSingleResult();
    if (resultadoConteo != null) {
        return new Long(resultadoConteo);
    } else {
    	return null;
    }
  }

}
