package sibbac.business.accounting.conciliacion.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.dto.Tmct0FiltroContaConciliacionAuditDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.database.dao.Tmct0ContaApunteDetalleDaoImp;

/**
 * @author lucio.vilar
 *
 */
@Repository
public class Tmct0ContaConciliacionAuditDaoImp {

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaApunteDetalleDaoImp.class);

	@PersistenceContext
	private EntityManager em;

	public List<Tmct0ContaConciliacionAudit> consultarHistoricoPartidasCasadas(Tmct0FiltroContaConciliacionAuditDTO filtro) {
		LOG.info("INICIO - DAOIMPL - consultarHistoricoPartidasCasadas");
		
		List<Tmct0ContaConciliacionAudit> resultado = new ArrayList<Tmct0ContaConciliacionAudit>();
		List<Tmct0ContaConciliacionAudit> lista = new ArrayList<Tmct0ContaConciliacionAudit>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		int sizeFechas = 1;

		try {

			if (null != filtro.getFechas()) {
				sizeFechas = filtro.getFechas().size();
			}

			for (int i=0;i<sizeFechas;i++) {
				StringBuilder consulta = new StringBuilder("SELECT pc FROM Tmct0ContaConciliacionAudit pc WHERE 1=1");
				
				if (null != filtro.getUser()) {
					consulta.append(" AND pc.auditUser = :usuario");
					parameters.put("usuario", filtro.getUser());
				}
				
				if (null != filtro.getFechas()) {
					consulta.append(" AND date(pc.auditDate) = :feAudit");
					parameters.put("feAudit", filtro.getFechas().get(i));
				}

				
				LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
				TypedQuery<Tmct0ContaConciliacionAudit> query = em.createQuery(consulta.toString(), Tmct0ContaConciliacionAudit.class);
				
				for (Entry<String, Object> entry : parameters.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}
				
				lista = query.getResultList();

				if (null != lista) {
					for (Tmct0ContaConciliacionAudit tmct0ContaConciliacionAudit : lista) {
						resultado.add(tmct0ContaConciliacionAudit);
					}
				}
				
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarHistoricoPartidasCasadas -  " + e.getMessage(), e);
			throw(e);
		}
		
		return resultado;
	}
	
}
