package sibbac.business.accounting.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.dao.Tmct0ContaApunteDetalleDaoImp;
import sibbac.business.accounting.database.dao.Tmct0ContaApuntesDao;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDaoImp;
import sibbac.business.accounting.database.dao.Tmct0Norma43DetalleDao;
import sibbac.business.accounting.database.dto.AlcDTO;
import sibbac.business.accounting.database.dto.ApuntesDescuadresCobroLiqAMDTO;
import sibbac.business.accounting.database.dto.ApuntesDescuadresDTO;
import sibbac.business.accounting.database.dto.ApuntesDiferenciaDTO;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.CobroDTO;
import sibbac.business.accounting.database.dto.ContaApuntesCobrosBarridosDTO;
import sibbac.business.accounting.database.dto.PersistenciaCobrosBarridosDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.database.model.Tmct0Norma43Detalle;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.conciliacion.database.dao.Norma43DescuadresDao;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.ConstantesPersistencia;
import sibbac.common.utils.SibbacEnums.CuentasBancarias;
import sibbac.common.utils.SibbacEnums.TipoApunte;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ContaApuntesBo extends AbstractBo<Tmct0ContaApuntes, Integer, Tmct0ContaApuntesDao> {

  @Autowired
  Tmct0ContaApunteDetalleBo tmct0ContaApunteDetalleBo;

  @Autowired
  Tmct0ContaApuntesDao tmct0ContaApuntesDao;

  @Autowired
  Norma43DescuadresDao norma43DescuadreDao;

  @Autowired
  Tmct0Norma43DescuadresBo tmct0Norma43DescuadreBo;

  @Autowired
  Tmct0Norma43DetalleDao tmct0Norma43DetalleDao;

  @Autowired
  Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  Tmct0ContaPlantillasDao contaPlantillasDao;

  @Autowired
  Tmct0ContaPlantillasDaoImp contaPlantillasDaoImp;

  @Autowired
  Tmct0ContaApunteDetalleDaoImp contaApunteDetalleDaoImp;

  @Autowired
  Tmct0Norma43MovimientoBo tmct0Norma43MovimientoBo;

  @Value("${sibbac.accounting.intervalo.ejecuciones.queries}")
  private String nroIntervaloEjecuciones;

  public List<Tmct0ContaApuntes> getTmct0ContaApuntesGeneradoFichero() {
    return dao.getTmct0ContaApuntesGeneradoFichero();
  }

  /**
   * Se actualiza el campo de generado_fichero y se incrementa el nro. de comprobante de la plantilla.
   *
   * @param ficheroContaApunte ficheroContaApunte
   * @param periodo
   * @param flagActivar flagActivar
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public Integer updateDatosFicheroGeneradoNumComprobante(String ficheroContaApunte, String periodo, Date fechaComprobante, Integer numComprobante) throws EjecucionPlantillaException {
    
    Integer generadoFichero = this.dao.updateGeneradoFicheroNumComprobanteByFicheroAndPeriodoComprobanteAndFechaComprobanteAndNotGeneradoFichero(numComprobante, ficheroContaApunte, periodo, fechaComprobante);
    this.contaPlantillasDao.updateNumComprobanteByFichero(numComprobante, ficheroContaApunte);
    return generadoFichero;
  }

  /**
   * Popula apuntes contables Cobro liquidacion AM.
   *
   * @param importeOrden importeOrden
   * @param iCuenta iCuenta
   * @param orden orden
   * @param cobro cobro
   * @param plantilla plantilla
   * @return ApuntesDiferenciaDTO ApuntesDiferenciaDTO
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public ApuntesDiferenciaDTO popularApuntesContablesCobroLiquidacionAM(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                                        CobroDTO cobro) throws EjecucionPlantillaException {

    List<Tmct0ContaApuntes> apuntes = new ArrayList<Tmct0ContaApuntes>();
    BigDecimal diferencia = BigDecimal.ZERO;

    // Se verifica que la plantilla tenga cuenta al debe
    // y cuenta al haber.
    // Si el importe de la plantilla es 0, no se generan apuntes.
    if (!BigDecimal.ZERO.equals(contaPlantillasDTO.getImporte())) {

      BigDecimal importeDebe = BigDecimal.ZERO;
      if (StringUtils.isNotEmpty(contaPlantillasDTO.getCuenta_debe())) {
        // Se genera y se guarda el apunte al debe.
        Tmct0ContaApuntes apunteDebe = this.popularApunteDebeAM(contaPlantillasDTO, cobro);
        apuntes.add(apunteDebe);
        importeDebe.add(apunteDebe.getImporte());
      }

      BigDecimal importeHaber = BigDecimal.ZERO;
      if (StringUtils.isNotEmpty(contaPlantillasDTO.getCuenta_haber())) {
        // Se genera y se guarda el apunte al haber.
        Tmct0ContaApuntes apuntehaber = this.popularApunteHaberAM(contaPlantillasDTO, cobro);
        apuntes.add(apuntehaber);
        importeHaber.add(apuntehaber.getImporte());
      }

      // Se retorna diferencia de los importes para cada apunte debe-haber generado.
      diferencia = importeDebe.subtract(importeHaber);
    }
    return new ApuntesDiferenciaDTO(apuntes, diferencia);
  }

  /**
   * Popula apunte debe apuntes manuales.
   * 
   * @param importe importe
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @param Integer Integer
   * @return Tmct0ContaApuntes Tmct0ContaApuntes
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public Tmct0ContaApuntes popularApunteDebeAM(Tmct0ContaPlantillasDTO contaPlantillasDTO, CobroDTO cobro) throws EjecucionPlantillaException {

    Tmct0ContaApuntes apunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
    apunteDebe.setCod_plantilla(contaPlantillasDTO.getCodigo());
    apunteDebe.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
    apunteDebe.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
    apunteDebe.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
    apunteDebe.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
    apunteDebe.setCuenta_contable(contaPlantillasDTO.getCuenta_debe());
    apunteDebe.setAux_contable(StringHelper.quitarComillasDobles(contaPlantillasDTO.getAux_debe()));
    apunteDebe.setFichero(contaPlantillasDTO.getFichero());
    String cuenta = CuentasBancarias.CUENTA_COBRO.getCuenta();
    if (cuenta.equals(StringHelper.quitarComillas(apunteDebe.getCuenta_contable()))) {
      apunteDebe.setConcepto(cobro.getDescReferencia());
      apunteDebe.setImporte((BigDecimal) cobro.getImporte());
    } else {
      apunteDebe.setConcepto(contaPlantillasDTO.getConcepto());
      apunteDebe.setImporte(contaPlantillasDTO.getImporte());
    }
    if (apunteDebe.getImporte().signum() > 0) {
      apunteDebe.setTipo_apunte('D');
    } else if (apunteDebe.getImporte().signum() < 0) {
      apunteDebe.setTipo_apunte('H');
    }
    return apunteDebe;
  }

  /**
   * Popula apunte haber apuntes manuales.
   * 
   * @param importe importe
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @return Tmct0ContaApuntes Tmct0ContaApuntes
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  private Tmct0ContaApuntes popularApunteHaberAM(Tmct0ContaPlantillasDTO contaPlantillasDTO, CobroDTO cobro) throws EjecucionPlantillaException {

    Tmct0ContaApuntes apunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
    apunteHaber.setCod_plantilla(contaPlantillasDTO.getCodigo());
    apunteHaber.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
    apunteHaber.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
    apunteHaber.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
    apunteHaber.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
    apunteHaber.setCuenta_contable(contaPlantillasDTO.getCuenta_haber());
    apunteHaber.setAux_contable(StringHelper.quitarComillasDobles(contaPlantillasDTO.getAux_haber()));
    apunteHaber.setFichero(contaPlantillasDTO.getFichero());
    String cuenta = CuentasBancarias.CUENTA_COBRO.getCuenta();
    if (cuenta.equals(StringHelper.quitarComillas(apunteHaber.getCuenta_contable()))) {
      apunteHaber.setConcepto(cobro.getDescReferencia());
      apunteHaber.setImporte((BigDecimal) cobro.getImporte());
    } else {
      apunteHaber.setConcepto(contaPlantillasDTO.getConcepto());
      apunteHaber.setImporte(contaPlantillasDTO.getImporte());
    }

    if (apunteHaber.getImporte().signum() > 0) {
      apunteHaber.setTipo_apunte('D');
    } else if (apunteHaber.getImporte().signum() < 0) {
      apunteHaber.setTipo_apunte('H');
    }
    return apunteHaber;
  }

  /**
   * Persiste lista conta apuntes para
   * apuntes manuales - ficheros tanto tipo SIBBAC.
   *
   * @param listaTmct0ContaApuntes listaTmct0ContaApuntes
   * @param plantillaEspecifica plantillaEspecifica
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void persistirListaContaApuntesSIBBAC(List<Tmct0ContaApuntes> listaTmct0ContaApuntes,
		  Tmct0ContaPlantillas plantillaEspecifica) 
				  throws EjecucionPlantillaException {

	  if (CollectionUtils.isNotEmpty(listaTmct0ContaApuntes)) {
		  this.tmct0ContaApuntesDao.save(listaTmct0ContaApuntes);
		  for (Tmct0ContaApuntes contaApuntes : listaTmct0ContaApuntes) {
			  if (contaApuntes != null && contaApuntes.getId() != null) {
				  Loggers.logTraceGenericoAccounting(plantillaEspecifica.getCodigo(),
						  ConstantesPersistencia.CREADO_APUNTE_ID.getValue() + contaApuntes.getId());
			  }
		  }
	  }
  }


  /**
   * Popula los apuntes al Debe - Apuntes Manuales.
   * 
   * @param plantilla plantilla
   * @param object object
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public Tmct0ContaApuntes popularApunteDebeAMDevengoAnulacion(Tmct0ContaPlantillasDTO plantilla) throws EjecucionPlantillaException {
    Tmct0ContaApuntes apunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
    apunteDebe.setCod_plantilla(plantilla.getCodigo());
    apunteDebe.setFecha_comprobante(plantilla.getFecha_comprobante());
    apunteDebe.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
    apunteDebe.setTipo_comprobante(plantilla.getTipo_comprobante());
    apunteDebe.setNum_comprobante(plantilla.getNum_comprobante());
    apunteDebe.setCuenta_contable(plantilla.getCuenta_debe());
    apunteDebe.setConcepto(plantilla.getConcepto());
    apunteDebe.setImporte(plantilla.getImporte());
    apunteDebe.setFichero(plantilla.getFicheroConValor());
    apunteDebe.setAux_contable(StringHelper.quitarComillasDobles(plantilla.getAux_debe()));
    if (apunteDebe.getImporte().signum() > 0) {
      apunteDebe.setTipo_apunte('D');
    } else if (apunteDebe.getImporte().signum() < 0) {
      apunteDebe.setTipo_apunte('H');
    }
    return apunteDebe;
  }

  /**
   * Popula los apuntes al Haber - Apuntes Manuales para cabecera Devengo - Anulacion.
   * 
   * @param plantilla plantilla
   * @param object object
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public Tmct0ContaApuntes popularApunteHaberAMDevengoAnulacion(Tmct0ContaPlantillasDTO plantilla) throws EjecucionPlantillaException {
    Tmct0ContaApuntes apunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
    apunteHaber.setCod_plantilla(plantilla.getCodigo());
    apunteHaber.setFecha_comprobante(plantilla.getFecha_comprobante());
    apunteHaber.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
    apunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
    apunteHaber.setNum_comprobante(plantilla.getNum_comprobante());
    apunteHaber.setCuenta_contable(plantilla.getCuenta_haber());
    apunteHaber.setAux_contable(StringHelper.quitarComillasDobles(plantilla.getAux_haber()));
    apunteHaber.setConcepto(plantilla.getConcepto());
    apunteHaber.setImporte(plantilla.getImporte().negate());
    apunteHaber.setFichero(plantilla.getFicheroConValor());
    if (apunteHaber.getImporte().signum() > 0) {
      apunteHaber.setTipo_apunte('D');
    } else if (apunteHaber.getImporte().signum() < 0) {
      apunteHaber.setTipo_apunte('H');
    }
    return apunteHaber;
  }

  /**
   * Genera y guarda los apuntes al Debe - Apuntes Manuales.
   * 
   * @param plantilla plantilla
   * @param object object
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public Integer guardarApunteDebe(Tmct0ContaPlantillasDTO plantilla) throws EjecucionPlantillaException {
    Tmct0ContaApuntes apunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
    apunteDebe.setCod_plantilla(plantilla.getCodigo());
    apunteDebe.setFecha_comprobante(plantilla.getFecha_comprobante());
    apunteDebe.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
    apunteDebe.setTipo_comprobante(plantilla.getTipo_comprobante());
    apunteDebe.setNum_comprobante(plantilla.getNum_comprobante());
    apunteDebe.setCuenta_contable(plantilla.getCuenta_debe().replaceAll("\"", ""));
    apunteDebe.setAux_contable(plantilla.getAux_debe().replaceAll("\"", ""));
    apunteDebe.setConcepto(plantilla.getConcepto());
    apunteDebe.setImporte(plantilla.getImporte());
    if (plantilla.isSinBalance()) {
      apunteDebe.setGenerado_fichero(9);
    } else {
      apunteDebe.setGenerado_fichero(0);
    }
    apunteDebe.setFichero(plantilla.getFicheroConValor());
    if (apunteDebe.getImporte().signum() > 0) {
      apunteDebe.setTipo_apunte('D');
    } else if (apunteDebe.getImporte().signum() < 0) {
      apunteDebe.setTipo_apunte('H');
    }
    this.tmct0ContaApuntesDao.save(apunteDebe);
    return apunteDebe.getId();
  }

  /**
   * Genera y guarda los apuntes al Haber - Apuntes Manuales.
   * 
   * @param plantilla plantilla
   * @param object object
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public Integer guardarApunteHaber(Tmct0ContaPlantillasDTO plantilla) throws EjecucionPlantillaException {
    Tmct0ContaApuntes apunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
    apunteHaber.setCod_plantilla(plantilla.getCodigo());
    apunteHaber.setFecha_comprobante(plantilla.getFecha_comprobante());
    apunteHaber.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
    apunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
    apunteHaber.setNum_comprobante(plantilla.getNum_comprobante());
    apunteHaber.setCuenta_contable(plantilla.getCuenta_haber().replaceAll("\"", ""));
    apunteHaber.setAux_contable(plantilla.getAux_haber().replaceAll("\"", ""));
    apunteHaber.setConcepto(plantilla.getConcepto());
    if ((!StringUtils.isEmpty(plantilla.getCuenta_debe()))) {
      apunteHaber.setImporte(plantilla.getImporte().negate());
    } else if ((StringUtils.isEmpty(plantilla.getCuenta_debe()))) {
      apunteHaber.setImporte(plantilla.getImporte());
    }
    if (plantilla.isSinBalance()) {
      apunteHaber.setGenerado_fichero(9);
    } else {
      apunteHaber.setGenerado_fichero(0);
    }
    apunteHaber.setFichero(plantilla.getFicheroConValor());
    if (apunteHaber.getImporte().signum() > 0) {
      apunteHaber.setTipo_apunte('D');
    } else if (apunteHaber.getImporte().signum() < 0) {
      apunteHaber.setTipo_apunte('H');
    }
    this.tmct0ContaApuntesDao.save(apunteHaber);
    return apunteHaber.getId();
  }

  /**
   * Genera apuntes o descuadres para tipo cobro liquidacion en Apuntes Manuales.
   *
   * @param adCLAMDTO adCLAMDTO
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public ApuntesDescuadresDTO generarApuntesDescuadresCobroLiqAM(ApuntesDescuadresCobroLiqAMDTO adCLAMDTO) throws EjecucionPlantillaException {

    List<Tmct0ContaApuntes> listaTmct0ContaApuntes = new ArrayList<Tmct0ContaApuntes>();
    List<Norma43Descuadres> listaNorma43Descuadres = new ArrayList<Norma43Descuadres>();

    if (adCLAMDTO.getIsDiferenciaMayorQueToleranciaPlantilla()) {
      // Generar descuadre.
      listaNorma43Descuadres.add(this.tmct0Norma43DescuadreBo.getDescuadreCobroLiquidacion("LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO ("
                                                                                               + String.valueOf(adCLAMDTO.getImporteCobro())
                                                                                               + ") Y EL PDTE. DE COBRO ("
                                                                                               + String.valueOf(adCLAMDTO.getContaPlantillasDTO()
                                                                                                                         .getImporte())
                                                                                               + ") ES SUPERIOR AL TOLERANCE DEFINIDO PARA LA CUENTA ("
                                                                                               + String.valueOf(adCLAMDTO.getPlantilla()
                                                                                                                         .getTolerancia())
                                                                                               + ")",
                                                                                           adCLAMDTO.getPlantilla(),
                                                                                           adCLAMDTO.getCobro(),
                                                                                           adCLAMDTO.getContaPlantillasDTO()
                                                                                                    .getImporte()));
    }

    ApuntesDiferenciaDTO apuntesDiferenciaDTO = null;
    if (adCLAMDTO.getIsSinDiferencia() || adCLAMDTO.getIsDiferenciaMenorQueToleranciaPlantilla()) {
      // Generar apuntes contables
      apuntesDiferenciaDTO = this.popularApuntesContablesCobroLiquidacionAM(adCLAMDTO.getContaPlantillasDTO(),
                                                                            adCLAMDTO.getCobro());
      listaTmct0ContaApuntes.addAll(apuntesDiferenciaDTO.getListaTmct0ContaApuntes());
    }
    return new ApuntesDescuadresDTO(apuntesDiferenciaDTO, listaNorma43Descuadres);
  }

  /**
   * Procesa cobros barridos y persiste las listas que se fueron creando - Devuelve true por exito.
   * 
   * @param pcbDTO pcbDTO
   * @return boolean boolean
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public boolean postPersistenciaCobrosBarridos(PersistenciaCobrosBarridosDTO pcbDTO, String codigoPlantilla) throws EjecucionPlantillaException {

    // Se persiste la lista en la transaccion.
    this.saveListaTmct0Norma43Detalle(pcbDTO.getListaTmct0Norma43Detalle(), codigoPlantilla);

    boolean isDescuadre = false;
    // Se persisten los descuadres pendientes.
    if (CollectionUtils.isNotEmpty(pcbDTO.getListaNorma43Descuadres())) {
      isDescuadre = true;
      for (Norma43Descuadres n43Descuadres : pcbDTO.getListaNorma43Descuadres()) {
        this.norma43DescuadreDao.save(n43Descuadres);
        if (n43Descuadres != null && n43Descuadres.getId() != null) {
          Loggers.logTraceGenericoAccounting(codigoPlantilla, ConstantesPersistencia.CREADO_DESCUADRE_ID.getValue()
                                                              + n43Descuadres.getId());
        }
      }
    }

    // Se deben actualizar todos los ALCs que componen el IMPORTE de QUERY_ORDEN al
    // estado correcto y grabar la tabla TMCT0_CONTA_APUNTE_DETALLE si asi se
    // especifica en la campo GRABA_DETALLE.
    // Se genera y se guarda el detalle del apunte.
    if (CollectionUtils.isNotEmpty(pcbDTO.getListaContaApuntesCobrosBarridosDTO())) {
      List<List<String>> listImportDetalle = AccountingHelper.getListImportesTextos(pcbDTO.getTmct0ContaPlantilla()
                                                                                          .getQuery_apunte_detalle());
      for (int i = 0; i < pcbDTO.getListaContaApuntesCobrosBarridosDTO().size(); i++) {
        ContaApuntesCobrosBarridosDTO arrayContaApuntes = pcbDTO.getListaContaApuntesCobrosBarridosDTO().get(i);
        this.persistirApuntesYDetallesCBbyTx(arrayContaApuntes, pcbDTO, listImportDetalle.get(0), codigoPlantilla, i);
      }
    }

    // Se deben actualizar todos los ALCs que componen el IMPORTE de
    // QUERY_ORDEN al estado correcto - Desbloqueo
    this.ejecutaQueryDesbloqueoCB(pcbDTO, pcbDTO.getNorma43Movimiento(), codigoPlantilla);

    // Procesado correctamente un barrido identificado por un IDMOVIMIENTO,se han generado
    // los apuntes al DEBE/HABER, se deben borrar de TMCT0_NORMA43_DETALLE todos los registros
    // que tengan ese mismo IDMOVIMIENTO y cuyo TIPO_APUNTE sea ABONO
    // (Si en fichero es H, en tabla se guarda ABONO)
    if (CollectionUtils.isNotEmpty(pcbDTO.getIdsMovimiento())) {
      // si el procesamiento dio error o se generÃ³ un descuadre, no se deben borrar los registros.
      if (!isDescuadre) {
        this.tmct0Norma43DetalleDao.deleteByIdsMovimientoAndTipoApunte(pcbDTO.getIdsMovimiento(), "ABONO");
      }

      for (Integer idMov : pcbDTO.getIdsMovimiento()) {
        this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(new Long(idMov), 1);
      }
    }
    return true;
  }

  /**
   * Persiste apuntes y apuntes detalle por transaccion nueva en cobros barridos.
   * 
   * @param contaApuntesCobrosBarridosDTO contaApuntesCobrosBarridosDTO
   * @param pcbDTO pcbDTO
   * @param importsDetText importsDetText
   * @param codigoPlantilla codigoPlantilla
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void persistirApuntesYDetallesCBbyTx(ContaApuntesCobrosBarridosDTO contaApuntesCobrosBarridosDTO,
                                              PersistenciaCobrosBarridosDTO pcbDTO,
                                              List<String> importsDetText,
                                              String codigoPlantilla,
                                              int i) throws EjecucionPlantillaException {

    Tmct0ContaApuntes apunteDebe = null;
    Tmct0ContaApuntes apunteHaber = null;

    if (contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes() != null) {
      if (contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes().length > 0) {
        apunteDebe = this.tmct0ContaApuntesDao.save(contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes()[0]);
        if (apunteDebe != null && apunteDebe.getId() != null) {
          Loggers.logTraceGenericoAccounting(codigoPlantilla, ConstantesPersistencia.CREADO_APUNTE_ID.getValue()
                                                              + apunteDebe.getId());
        }
      }
      if (contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes().length > 1) {
        apunteHaber = this.tmct0ContaApuntesDao.save(contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes()[1]);
        if (apunteHaber != null && apunteHaber.getId() != null) {
          Loggers.logTraceGenericoAccounting(codigoPlantilla, ConstantesPersistencia.CREADO_APUNTE_ID.getValue()
                                                              + apunteHaber.getId());
        }
      }
    }

    // Se persisten apuntes de diferencia en caso de que esten presentes.
    if (CollectionUtils.isNotEmpty(pcbDTO.getListaApuntesDiferencia())) {
      this.tmct0ContaApuntesDao.save(pcbDTO.getListaApuntesDiferencia());
      for (Tmct0ContaApuntes contaApuntes : pcbDTO.getListaApuntesDiferencia()) {
        if (contaApuntes != null && contaApuntes.getId() != null) {
          Loggers.logTraceGenericoAccounting(codigoPlantilla,
                                             ConstantesPersistencia.CREADO_APUNTE_DIFERENCIA_ID.getValue()
                                                 + contaApuntes.getId());
        }
      }
    }

    Object[] params = new Object[3];
    params[0] = apunteDebe != null ? (apunteDebe).getId() : null;
    params[1] = apunteHaber != null ? (apunteHaber).getId() : null;
    params[2] = pcbDTO.getNorma43Movimiento().getId();

    try {
      // Se genera y se guarda el detalle del apunte. Esto se realiza por orden (ALC)
      this.contaApunteDetalleDaoImp.insertSelect(pcbDTO.getTmct0ContaPlantilla().getQuery_apunte_detalle(),
                                                 pcbDTO.getTmct0ContaPlantilla().getGrabaDetalle(), importsDetText,
                                                 pcbDTO.getTmct0ContaPlantilla().getCodigo(), i, params);
    } catch (Exception ex) {
      Loggers.logErrorAccounting(pcbDTO.getTmct0ContaPlantilla().getCodigo(),
                                 ConstantesError.ERROR_APUNTES_DETALLE.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

  }

  /**
   * Wrapper para ejecucion de query bloqueo de cobros barridos - Actualiza ALC's.
   * 
   * @param pcbDTO pcbDTO
   * @param norma43Movimiento norma43Movimiento
   * @return List<Object[]> List<Object[]>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void ejecutaQueryDesbloqueoCB(PersistenciaCobrosBarridosDTO pcbDTO,
                                       Norma43Movimiento norma43Movimiento,
                                       String codigoPlantilla) throws EjecucionPlantillaException {

    // Se fija intervalo de registros en que se ejecutara la query de desbloqueo
    int intervalo = 0;
    Long resultadoConteo = new Long(0);
    Integer nroIntervaloEjec = AccountingHelper.getNroIntervalosEjecucion(nroIntervaloEjecuciones);

    // De esta forma se ahorra la ejecucion de la query de conteo si se ingreso un valor
    // no apropiado en el fichero .properties
    if (nroIntervaloEjec > 0) {
      // Parametros a pasar al conteo
      ArrayList params = new ArrayList<>();
      params.add(norma43Movimiento.getId());

      // Realiza conteo de la query de desbloqueo para calcular el rango de ejecucion
      resultadoConteo = this.contaPlantillasDaoImp.executeQueryCount(pcbDTO.getTmct0ContaPlantilla().getQuery_bloqueo(),
                                                                     codigoPlantilla, params);

      intervalo = (resultadoConteo != null ? resultadoConteo.intValue() : 0) / nroIntervaloEjec;
    }

    if (intervalo == 0) {
      this.contaPlantillasDaoImp.ejecutaQueryDesbloqueoCobrosBarridos(pcbDTO, norma43Movimiento.getId(),
                                                                      codigoPlantilla, 0, 0);
    } else {
      for (int j = 0; j < intervalo; j++) {
        // En caso que el campo TMCT0_CONTA_PLANTILLAS.CAMPO_ESTADO sea distinto a 'CDESTADOCONT'
        // se deben actualizar ambos campos 'CDESTADOCONT' y 'distinto a CDESTADOCONT' al valor del
        // campo 'CAMPO_FINAL'

        this.contaPlantillasDaoImp.ejecutaQueryDesbloqueoCobrosBarridos(pcbDTO, norma43Movimiento.getId(),
                                                                        codigoPlantilla, j * nroIntervaloEjec,
                                                                        ((j + 1) * nroIntervaloEjec) - 1);
      }
      // Complemento en caso de haber resto de la division
      // Auxiliar para el remanente de la division.
      this.contaPlantillasDaoImp.ejecutaQueryDesbloqueoCobrosBarridos(pcbDTO, norma43Movimiento.getId(),
                                                                      codigoPlantilla, intervalo * nroIntervaloEjec,
                                                                      resultadoConteo.intValue());

    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public List<Tmct0ContaPlantillasDTO> generarApuntes(Tmct0ContaPlantillas plantilla,
                                                      List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs,
                                                      String estado) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> plantillasSinBalance = new ArrayList<Tmct0ContaPlantillasDTO>();

    for (Tmct0ContaPlantillasDTO contaPlantillasDTO : contaPlantillasDTOs) {
      if (contaPlantillasDTO.isSinBalance()) {
        plantillasSinBalance = contaPlantillasDTOs;
      }
      // Si el importe de la plantilla es 0, no se generan apuntes.
      if ((contaPlantillasDTO.getImporte() != null)
          && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO) != 0)) {

        // Se genera y se guarda el apunte al debe.
        if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
          guardarApunteDebe(contaPlantillasDTO);
        }

        // Se genera y se guarda el apunte al haber.
        if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())) {
          guardarApunteHaber(contaPlantillasDTO);
        }
      }
    }
    return plantillasSinBalance;
  }

  /**
   * Generacion de apuntes para devengo-anulacion y devengo fallidos.
   * 
   * @param plantilla plantilla
   * @param fechaEjecucion fechaEjecucion
   * @param tipoPlantilla tipoPlantilla
   * @param isFromApuntesManuales: true si viene de la funcionalidad de apuntes manuales
   * @return List<Tmct0ContaPlantillasDTO> List<Tmct0ContaPlantillasDTO>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public List<Tmct0ContaPlantillasDTO> generarApuntes(Tmct0ContaPlantillas plantilla,
                                                      List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs,
                                                      String tipoPlantilla,
                                                      Boolean isFromApuntesManuales) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> plantillaSinBalance = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<String>> listImportDetalle = AccountingHelper.getListImportesTextos(plantilla.getQuery_apunte_detalle());

    for (int i = 0; i < contaPlantillasDTOs.size(); i++) {
      Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(i);

      if (contaPlantillasDTO.isSinBalance()) {
        // Plantillas sin balance.
        plantillaSinBalance = contaPlantillasDTOs;
      }

      // Si el importe de la plantilla es 0, no se generan apuntes.
      if ((contaPlantillasDTO.getImporte() != null)
          && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO) != 0)) {

        // Se genera y se guarda el apunte al debe.
        Integer idApunteDebe = null;
        if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
          idApunteDebe = guardarApunteDebe(contaPlantillasDTO);
          if ((plantilla.getGrabaDetalle().equals('D') || plantilla.getGrabaDetalle().equals('A')) && idApunteDebe != null) {
            if (TipoContaPlantillas.DEVENGO_FALLIDOS.getTipo().equals(tipoPlantilla)) {
              this.tmct0ContaApunteDetalleBo.insertSelectDevengosFallidos(plantilla, idApunteDebe, listImportDetalle.get(0), contaPlantillasDTO.getParamsDetail(), i);
            } else {
              this.tmct0ContaApunteDetalleBo.insertSelectDevengos(plantilla, idApunteDebe, listImportDetalle.get(0), contaPlantillasDTO.getParamsDetail(), i, isFromApuntesManuales);
            }
          }
        }

        // Se genera y se guarda el apunte al haber.
        Integer idApunteHaber = null;
        if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())) {
          idApunteHaber = guardarApunteHaber(contaPlantillasDTO);
          if ((plantilla.getGrabaDetalle().equals('H') || plantilla.getGrabaDetalle().equals('A')) && idApunteHaber != null) {
            if (TipoContaPlantillas.DEVENGO_FALLIDOS.getTipo().equals(tipoPlantilla)) {
              this.tmct0ContaApunteDetalleBo.insertSelectDevengosFallidos(plantilla, idApunteHaber, listImportDetalle.get(0), contaPlantillasDTO.getParamsDetail(), i);
            } else {
              this.tmct0ContaApunteDetalleBo.insertSelectDevengos(plantilla, idApunteHaber, listImportDetalle.get(0), contaPlantillasDTO.getParamsDetail(), i, isFromApuntesManuales);
            }
          }
        }
      }
    }
    return plantillaSinBalance;
  }

	/**
	 * Generacion de apuntes de plantilla Emision Factura.
	 * 
	 * @param contaPlantillasDTOs
	 *            contaPlantillasDTOs
	 * @param plantilla
	 *            plantilla
	 * @param ndocnumero
	 *            ndocnumero
	 * @return List<Tmct0ContaPlantillasDTO> List<Tmct0ContaPlantillasDTO>
	 * @throws EjecucionPlantillaException
	 *             EjecucionPlantillaException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	public List<Tmct0ContaPlantillasDTO> generarApuntes(List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs, Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

		List<Tmct0ContaPlantillasDTO> plantillaSinBalance = new ArrayList<Tmct0ContaPlantillasDTO>();
		List<List<String>> listImportDet = AccountingHelper.getListImportesTextos(plantilla.getQuery_apunte_detalle());

		for (int i = 0; i < contaPlantillasDTOs.size(); i++) {
			Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(i);

			if (contaPlantillasDTO.isSinBalance()) {
				// Plantillas sin balance.
				plantillaSinBalance = contaPlantillasDTOs;
			}

			// Si el importe de la plantilla es 0, no se generan apuntes.
			if ((contaPlantillasDTO.getImporte() != null)
					&& (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO) != 0)) {

				// Se genera y se guarda el apunte al debe.
				Integer idApunteDebe = null;
				if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
					idApunteDebe = guardarApunteDebe(contaPlantillasDTO);
					if ((plantilla.getGrabaDetalle().equals('D') || plantilla.getGrabaDetalle().equals('A')) && idApunteDebe != null) {
						this.tmct0ContaApunteDetalleBo.insertSelectFacturas(plantilla, contaPlantillasDTO.getParamsDetail(), idApunteDebe, listImportDet.get(0), i);
					}
				}

				// Se genera y se guarda el apunte al haber.
				Integer idApunteHaber = null;
				if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())) {
					idApunteHaber = guardarApunteHaber(contaPlantillasDTO);
					if ((plantilla.getGrabaDetalle().equals('H') || plantilla.getGrabaDetalle().equals('A')) && idApunteHaber != null) {
						this.tmct0ContaApunteDetalleBo.insertSelectFacturas(plantilla, contaPlantillasDTO.getParamsDetail(), idApunteHaber, listImportDet.get(0), i);
					}
				}
				// Se genera y se guarda el detalle del apunte.
			}
		}
		return plantillaSinBalance;
	}
	
	/**
	 * Generacion de apuntes para devengo-anulacion y devengo fallidos.
	 * 
	 * @param plantilla
	 *            plantilla
	 * @param fechaEjecucion
	 *            fechaEjecucion
	 * @param tipoPlantilla
	 *            tipoPlantilla
	 * @return List<Tmct0ContaPlantillasDTO> List<Tmct0ContaPlantillasDTO>
	 * @throws EjecucionPlantillaException
	 *             EjecucionPlantillaException
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<Tmct0ContaPlantillasDTO> generarApuntesFacturasManuales(Tmct0ContaPlantillas plantilla,
			List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs) throws EjecucionPlantillaException {

		List<Tmct0ContaPlantillasDTO> plantillaSinBalance = new ArrayList<Tmct0ContaPlantillasDTO>();

		for (int i = 0; i < contaPlantillasDTOs.size(); i++) {
			Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(i);

			if (contaPlantillasDTO.isSinBalance()) {
				// Plantillas sin balance.
				plantillaSinBalance = contaPlantillasDTOs;
			}

			// Si el importe de la plantilla es 0, no se generan apuntes.
			if ((contaPlantillasDTO.getImporte() != null) && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO) != 0)) {

		          BigInteger nbDocNumero = (BigInteger) contaPlantillasDTO.getParamsDetail().get(2);
		          BigDecimal impImpuesto = (BigDecimal) contaPlantillasDTO.getParamsDetail().get(1);
		          BigDecimal baseImponible = (BigDecimal) contaPlantillasDTO.getParamsDetail().get(0);

				// Se genera y se guarda el apunte al debe.
				Tmct0ContaApuntes apunteDebe = null;
				if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
					apunteDebe = guardarApunteDebeFM(contaPlantillasDTO);
					if ((plantilla.getGrabaDetalle().equals('D') || plantilla.getGrabaDetalle().equals('A')) && apunteDebe != null) {
						this.tmct0ContaApunteDetalleBo.insertSelectFacturasManuales(plantilla, apunteDebe.getId(), nbDocNumero, apunteDebe.getImporte());
					}
				}

				// Se genera y se guarda el apunte al haber.
				Tmct0ContaApuntes apunteHaber = null;
				if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())) {
					apunteHaber = guardarApunteHaberFM(contaPlantillasDTO, baseImponible, impImpuesto);
					if ((plantilla.getGrabaDetalle().equals('H') || plantilla.getGrabaDetalle().equals('A')) && apunteHaber != null) {
						this.tmct0ContaApunteDetalleBo.insertSelectFacturasManuales(plantilla, apunteHaber.getId(), nbDocNumero, apunteHaber.getImporte());
					}
				}

				// Se genera y se guarda el apunte al haber.
				Tmct0ContaApuntes apunteHaberIva = null;
				if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_diferencias())
						&& impImpuesto.compareTo(BigDecimal.ZERO) != 0) {
					apunteHaberIva = guardarApunteHaberIvaFM(contaPlantillasDTO, impImpuesto);
					if ((plantilla.getGrabaDetalle().equals('H') || plantilla.getGrabaDetalle().equals('A'))
							&& apunteHaberIva != null) {
						this.tmct0ContaApunteDetalleBo.insertSelectFacturasManuales(plantilla, apunteHaberIva.getId(),
								nbDocNumero, apunteHaberIva.getImporte());

					}
				}
			}
		}
		return plantillaSinBalance;
	}

  /**
   * Generacion de apuntes de plantilla PyG.
   * 
   * @param contaPlantillasDTOs contaPlantillasDTOs
   * @param idMov idMov
   * @return List<Tmct0ContaPlantillasDTO> List<Tmct0ContaPlantillasDTO>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public List<Tmct0ContaPlantillasDTO> generarApuntes(List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs, Long idMov) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> plantillaSinBalance = new ArrayList<Tmct0ContaPlantillasDTO>();

    for (Tmct0ContaPlantillasDTO cuentaPlantilla : contaPlantillasDTOs) {
      if (cuentaPlantilla.isSinBalance()) {
        // Plantillas sin balance.
        plantillaSinBalance = contaPlantillasDTOs;
      }
      // Si el importe de la plantilla es 0, no se generan apuntes.
      if ((cuentaPlantilla.getImporte() != null) && (cuentaPlantilla.getImporte().compareTo(BigDecimal.ZERO) != 0)) {

        // Se genera y se guarda el apunte al debe.
        if (!StringUtils.isEmpty(cuentaPlantilla.getCuenta_debe())) {
          guardarApunteDebe(cuentaPlantilla);
        }

        // Se genera y se guarda el apunte al haber.
        if (!StringUtils.isEmpty(cuentaPlantilla.getCuenta_haber())) {
          guardarApunteHaber(cuentaPlantilla);
        }
        this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(idMov, 1);
      }
    }
    return plantillaSinBalance;
  }

  /**
   * Almacena lista de norma 43 detalle para Cobros Barridos.
   * 
   * @param listaTmct0Norma43Detalle listaTmct0Norma43Detalle
   * @param codigoPlantilla: util para logs
   * @return List<Integer> List<Integer>
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void saveListaTmct0Norma43Detalle(List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle, String codigoPlantilla) throws EjecucionPlantillaException {
    try {
      // Se persiste lista si no esta vacia.
      if (CollectionUtils.isNotEmpty(listaTmct0Norma43Detalle)) {
        this.tmct0Norma43DetalleDao.save(listaTmct0Norma43Detalle);
        for (Tmct0Norma43Detalle n43D : listaTmct0Norma43Detalle) {
          if (n43D != null && n43D.getId() != null) {
            Loggers.logTraceGenericoAccounting(codigoPlantilla,
                                               ConstantesPersistencia.CREADO_NORMA43_DETALLE_ID.getValue()
                                                   + n43D.getId());
          }
        }
      }
    } catch (Exception ex) {
      Loggers.logErrorAccounting(codigoPlantilla, ConstantesError.ERROR_REGISTROS_DETALLE_NORMA43.getValue() + ": "
                                                  + ex.getMessage());
      throw new EjecucionPlantillaException(ConstantesError.ERROR_REGISTROS_DETALLE_NORMA43.getValue());
    }
  }

  /**
   * Procesamiento de cobro-Liquidacion - Apuntes Manuales.
   * 
   * @param ordenes ordenes
   * @param plantilla plantilla
   * @param amExcelDTO amExcelDTO
   * @param cobro cobro
   * @return ApuntesDescuadresDTO ApuntesDescuadresDTO
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  /**
   * Procesamiento de cobro-Liquidacion - Apuntes Manuales.
   *
   * @param ordenes ordenes
   * @param plantilla plantilla
   * @param amExcelDTO amExcelDTO
   * @param cobro cobro
   * @return ApuntesDescuadresDTO ApuntesDescuadresDTO
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Deprecated
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public ApuntesDescuadresDTO procesarCobroLiquidacion(List<Object[]> restOrden,
                                                       Tmct0ContaPlantillas plantilla,
                                                       ApuntesManualesExcelDTO amExcelDTO,
                                                       CobroDTO cobro) throws EjecucionPlantillaException {

    List<Norma43Descuadres> listaNorma43Descuadres = new ArrayList<Norma43Descuadres>();
    ApuntesDescuadresDTO apuntesDescuadresDTO = new ApuntesDescuadresDTO();
    if (CollectionUtils.isNotEmpty(restOrden)) {

      for (Object[] importes : restOrden) {
        List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = dividirPlantillasPorCuentasCobrosLiquidaciones(plantilla,
                                                                                                           importes);

        BigDecimal balance = BigDecimal.ZERO;
        for (int j = 0; j < contaPlantillasDTOs.size(); j++) {
          Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(j);

          if (StringUtils.isNotBlank(amExcelDTO.getSibbacReferenciasTheirTotal())) {
            List<Norma43Movimiento> listaNorma43Movimiento = this.tmct0Norma43MovimientoBo.getListaNorma43MovimientoByReferenciasTheir(amExcelDTO);
            if (CollectionUtils.isNotEmpty(listaNorma43Movimiento)) {
              BigDecimal importeCobro = new BigDecimal(0);
              for (Norma43Movimiento norma43Mov : listaNorma43Movimiento) {
                importeCobro = importeCobro.add(norma43Mov.getImporte());
              }
              BigDecimal diferencia = contaPlantillasDTO.getImporte().subtract(importeCobro);
              Boolean isDiferenciaMayorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) > 0;
              Boolean isSinDiferencia = BigDecimal.ZERO.equals(diferencia);
              Boolean isDiferenciaMenorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) <= 0;

              ApuntesDescuadresCobroLiqAMDTO adCLAMDTO = new ApuntesDescuadresCobroLiqAMDTO(
                                                                                            isDiferenciaMayorQueToleranciaPlantilla,
                                                                                            isSinDiferencia,
                                                                                            isDiferenciaMenorQueToleranciaPlantilla,
                                                                                            importeCobro, plantilla,
                                                                                            cobro, contaPlantillasDTO);
              apuntesDescuadresDTO = this.generarApuntesDescuadresCobroLiqAM(adCLAMDTO);
              if (apuntesDescuadresDTO.getApuntesDiferenciaDTO() != null) {
                // Se asignan los apuntes de diferencia.
                balance = balance.add(apuntesDescuadresDTO.getApuntesDiferenciaDTO().getDiferencia());
              }
              listaNorma43Descuadres.addAll(apuntesDescuadresDTO.getListaNorma43Descuadres());
            }
          }
        }
        // Hace el apunte de diferencia para que cuadre en el caso de que el balance
        // sea distinto de 0
        if (balance.compareTo(BigDecimal.ZERO) != 0
            && CollectionUtils.isNotEmpty(apuntesDescuadresDTO.getApuntesDiferenciaDTO().getListaTmct0ContaApuntes())) {
          apuntesDescuadresDTO.getApuntesDiferenciaDTO().getListaTmct0ContaApuntes()
                              .add(this.getApunteDiferencia(balance.negate(), contaPlantillasDTOs.get(0), cobro));
        }
      }
    }
    return apuntesDescuadresDTO;
  }


  /**
   * Ejecucion de la query de orden.
   * 
   * @param plantilla plantilla
   * @param cobro cobro
   * @param amExcelDTO amExcelDTO solo para apuntes manuales
   * @return List<Object[]> List<Object[]>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  public List<Object[]> ejecutaQueryOrden(Tmct0ContaPlantillas plantilla,
		  								  Object[] cobro,
                                          ApuntesManualesExcelDTO amExcelDTO) throws EjecucionPlantillaException {
	  
	   String queryOrden = plantilla.getQuery_orden();  
	   
	   for(int i=0;i<cobro.length;i++){
		   if(cobro[i] instanceof BigInteger){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", cobro[i].toString());		
		   }else if(cobro[i] instanceof BigDecimal){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", cobro[i].toString());	
		   }else if(cobro[i] instanceof Time){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", "'" + cobro[i] + "'");
		   }else if(cobro[i] instanceof Date){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", "'" + cobro[i] + "'");
		   }else if(cobro[i] instanceof Timestamp){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", "'" + cobro[i] + "'");
		   }else if(cobro[i] instanceof Double){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", cobro[i].toString());	
		   }else if(cobro[i] instanceof Character){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", "'" + cobro[i] + "'");
		   }else if(cobro[i] instanceof Short){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", cobro[i].toString());			
		   }else if(cobro[i] instanceof String){
			   queryOrden = queryOrden.replaceAll("<" + i + " DE LA QUERY_COBRO>", "'" + cobro[i] + "'");
		   }
	  }
      
	  if (amExcelDTO != null) {
	      queryOrden = AccountingHelper.addAlcsToQuery(queryOrden,
	                                                   " AND (1 = 1) AND alc.NUORDEN = '" + amExcelDTO.getSibbacNuorden()
	                                                       + "' AND alc.NBOOKING = '" + amExcelDTO.getSibbacNbooking()
	                                                       + "' AND alc.NUCNFCLT = '" + amExcelDTO.getSibbacNucnfclt()
	                                                       + "' AND alc.NUCNFLIQ = " + amExcelDTO.getSibbacNucnfliq());
	  }

      Loggers.logDebugQueriesAccounting(plantilla != null ? plantilla.getCodigo() : null,
                                        SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue(), new Date());
      return this.contaPlantillasDaoImp.executeQuery(queryOrden.replace(";", ""), plantilla.getCodigo());

  }

  /**
   * Ejecucion de plantilla devengo fallidos por fecha.
   * 
   * @param plantilla plantilla
   * @param fechaEjecucion fechaEjecucion
   * @return List<Tmct0ContaPlantillasDTO>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaDevengoFallidosPorFecha(Tmct0ContaPlantillas plantilla,
                                                                                      Date fechaEjecucion, List<Object[]> alcImplicadas) throws EjecucionPlantillaException {
    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

    String queryOrden = null;
    List<Object[]> importes = null;

    try {
      queryOrden = this.contaPlantillasDaoImp.replaceFechasQueryOrden(plantilla.getQuery_orden().replace(";", ""),
                                                                      plantilla.getCodigo());
      importes = this.contaPlantillasDaoImp.executeQueryFechas(queryOrden, plantilla.getEstado_bloqueado(),
                                                               fechaEjecucion,
                                                               plantilla != null ? plantilla.getCodigo() : null);
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {
      if (!CollectionUtils.isEmpty(importes)) {
        for (Object[] importe : importes) {
          contaPlantillasDTOs = dividirPlantillasPorCuentas(plantilla, importe);

          contaPlantillasDTOs = generarApuntes(plantilla, contaPlantillasDTOs, TipoContaPlantillas.DEVENGO_FALLIDOS.getTipo(), Boolean.FALSE);
          if (!contaPlantillasDTOs.isEmpty()) {
            plantillasSinBalance.add(contaPlantillasDTOs);
          }
        }
      }
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {
    	
      this.contaPlantillasDaoImp.executeQueryBloqueoByFechasDevengosFallidos(alcImplicadas, plantilla.getEstado_final(), plantilla.getCampo_estado(), plantilla != null ? plantilla.getCodigo() : null);
      
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_DESBLOQUEO.getValue() + ": " + ex.getMessage());
    }
    return plantillasSinBalance;
  }

  /**
   * Genera registros transaccionales por fecha para devengo anulacion
   * 
   * @param plantilla plantilla
   * @param fechaEjecucion fechaEjecucion
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaDevengoAnulacionPorFecha(Tmct0ContaPlantillas plantilla, Date fechaEjecucion, List<Object[]> alcImplicadas) throws EjecucionPlantillaException {
	  
    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();
    String queryOrden = null;
    List<Object[]> importes = null;
    
    try {
    	
      queryOrden = this.contaPlantillasDaoImp.replaceFechasQueryOrden(plantilla.getQuery_orden().replace(";", ""), plantilla.getCodigo());
      importes = this.contaPlantillasDaoImp.executeQueryFechas(queryOrden, plantilla.getEstado_bloqueado(), fechaEjecucion, plantilla != null ? plantilla.getCodigo() : null);
      
    } catch (Exception ex) {
    	
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {
      if (!CollectionUtils.isEmpty(importes)) {

        for (Object[] importe : importes) {
          contaPlantillasDTOs = dividirPlantillasPorCuentas(plantilla, importe);
          contaPlantillasDTOs = generarApuntes(plantilla, contaPlantillasDTOs, SibbacEnums.TipoContaPlantillas.DEVENGO.getTipo(), Boolean.FALSE);
          if (!contaPlantillasDTOs.isEmpty()) {
            plantillasSinBalance.add(contaPlantillasDTOs);
          }
        }
      }
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {

      this.contaPlantillasDaoImp.executeQueryBloqueoByFechasDevengosAnulacion(alcImplicadas, plantilla.getEstado_final(), plantilla.getCampo_estado(), plantilla != null ? plantilla.getCodigo() : null);
      
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_DESBLOQUEO.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }
	    
    
    return plantillasSinBalance;
  }
  
	/**
   * Genera registros transaccionales por fecha para devengo anulacion
   * 
   * @param plantilla plantilla
   * @param fechaEjecucion fechaEjecucion
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaFacturasManuales(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

    List<Object[]> importes = null;

    try {

      importes = this.contaPlantillasDaoImp.executeQuery(plantilla.getQuery_orden(), plantilla.getEstado_inicio(),
                                                         plantilla != null ? plantilla.getCodigo() : null);

    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {
      if (!CollectionUtils.isEmpty(importes)) {

        for (Object[] importe : importes) {
          contaPlantillasDTOs = dividirPlantillasPorCuentas(plantilla, importe);
          contaPlantillasDTOs = generarApuntesFacturasManuales(plantilla, contaPlantillasDTOs);

          if (!contaPlantillasDTOs.isEmpty()) {
            plantillasSinBalance.add(contaPlantillasDTOs);
          }
        }
      }

    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {

      this.contaPlantillasDaoImp.executeQueryDesbloqueoFacturasManuales(plantilla.getQuery_bloqueo(),
                                                                        plantilla.getEstado_inicio(),
                                                                        plantilla.getEstado_final(),
                                                                        plantilla != null ? plantilla.getCodigo()
                                                                                         : null);

    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_DESBLOQUEO.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }
    return plantillasSinBalance;
  }

  /**
   * Divide las plantillas que tengan mÃ¡s de una cuenta, y en tal caso las marca como dividido=true.
   * 
   * @param plantilla Tmct0ContaPlantillas.
   * @param importe Resultado de query orden.
   * @return Lista de Tmct0ContaPlantillasDTO.
   * @throws EjecucionPlantillaException
   */
  private List<Tmct0ContaPlantillasDTO> dividirPlantillasPorCuentas(Tmct0ContaPlantillas plantilla, Object[] importe) throws EjecucionPlantillaException {

    Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "[" + AbstractEjecucionPlantilla.class
                                                              + " - INICIO dividirPlantillasPorCuentas]" + "[Metodo]"
                                                              + "[" + new Date() + "]");

    List<Tmct0ContaPlantillasDTO> plantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
    Tmct0ContaPlantillasDTO contaPlantillasDTO = null;
    BigDecimal balance = BigDecimal.ZERO;

    // Se separa por la "," los campos que pueden traer mÃ¡s de un valor por plantilla.
    String[] cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",");
    String[] cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(),
                                                                                        ",");
    String[] auxsDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(), ",");
    String[] auxsHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(), ",");

    // En caso de que haya un error de inconsistencia de cuentas y auxiliares se arroja error.
    if (!AccountingHelper.isPlantillaCuentasAuxConsistentes(cuentasDebe, cuentasHaber, auxsDebe, auxsHaber)) {
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_DIVIDIR_PLANTILLAS.getValue() + " : " + ConstantesError.ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX.getValue());
      throw new EjecucionPlantillaException();
    }

    int iCuenta = 0;
    int iImporte = 3;
    int iAux = iImporte + cuentasDebe.length; // Indice de las cuenta auxiliares.

    // Map con los valores AUX sacados de la Query
    Map<String, String> auxMap = new HashMap<String, String>();
    for (int i = 0; i < cuentasDebe.length; i++) {
      try {
        if (!auxsDebe[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsDebe[i])) {
          if (auxMap.get(auxsDebe[i]) == null) {
            auxMap.put(auxsDebe[i], (String) importe[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }

      try {
        if (!auxsHaber[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsHaber[i])) {
          if (auxMap.get(auxsHaber[i]) == null) {
            auxMap.put(auxsHaber[i], (String) importe[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }
    }
    
    List<Object> paramsDetail = new ArrayList<Object>();
    for(int i=iAux; i<importe.length;i++){
  	  paramsDetail.add(importe[i]);
    }

    for (String cuentaDebe : cuentasDebe) {

      // SafeNull para el importe - ya que la query de orden puede devolver importe NULL
      if (importe[iImporte] == null) {
        importe[iImporte] = BigDecimal.ZERO;
      }

      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      contaPlantillasDTO.setActivo(plantilla.getActivo());
      contaPlantillasDTO.setAudit_date(plantilla.getAudit_date());
      contaPlantillasDTO.setCampo(plantilla.getCampo());
      contaPlantillasDTO.setCampo_estado(plantilla.getCampo_estado());
      contaPlantillasDTO.setCodigo(plantilla.getCodigo());
      contaPlantillasDTO.setCuenta_bancaria(plantilla.getCuenta_bancaria());
      contaPlantillasDTO.setCuenta_debe(StringHelper.quitarComillasDobles(cuentaDebe));
      contaPlantillasDTO.setCuenta_diferencias(StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
      contaPlantillasDTO.setCuenta_haber(StringHelper.quitarComillasDobles(cuentasHaber[iCuenta]));
      contaPlantillasDTO.setEstado_final(plantilla.getEstado_final());
      contaPlantillasDTO.setEstado_inicio(plantilla.getEstado_inicio());
      contaPlantillasDTO.setFichero(plantilla.getFichero());
      contaPlantillasDTO.setFichero_cobro(plantilla.getFichero_cobro());
      contaPlantillasDTO.setFichero_orden(plantilla.getFichero_orden());
      contaPlantillasDTO.setId(plantilla.getId());
      contaPlantillasDTO.setNum_comprobante(plantilla.getNum_comprobante());
      contaPlantillasDTO.setProceso(plantilla.getProceso());
      contaPlantillasDTO.setQuery_bloqueo(plantilla.getQuery_bloqueo());
      contaPlantillasDTO.setQuery_cobro(plantilla.getQuery_cobro());
      contaPlantillasDTO.setQuery_final(plantilla.getQuery_final());
      contaPlantillasDTO.setQuery_orden(plantilla.getQuery_orden());
      contaPlantillasDTO.setSubproceso(plantilla.getSubproceso());
      contaPlantillasDTO.setSubtipo(plantilla.getSubtipo());
      contaPlantillasDTO.setTipo(plantilla.getTipo());
      contaPlantillasDTO.setTipo_comprobante(plantilla.getTipo_comprobante());
      contaPlantillasDTO.setImporte((BigDecimal) importe[iImporte]);
      contaPlantillasDTO.setFecha_comprobante((Date) importe[0]);
      contaPlantillasDTO.setPeriodo_comprobante(((Integer) importe[1]).toString());
      String concepto = (String) importe[2];
      if(concepto.length()>36){
    	  concepto =  concepto.substring(0,36);
      }
      contaPlantillasDTO.setConcepto(concepto);
      contaPlantillasDTO.setAux_debe(AccountingHelper.getAux(auxsDebe, auxMap, iCuenta));
      contaPlantillasDTO.setAux_haber(AccountingHelper.getAux(auxsHaber, auxMap, iCuenta));

      if (contaPlantillasDTO.getAux_debe() == null){
        contaPlantillasDTO.setAux_debe("");
      }
      if (contaPlantillasDTO.getAux_haber() == null){
        contaPlantillasDTO.setAux_haber("");
      }
      
      contaPlantillasDTO.setParamsDetail(paramsDetail);

      plantillasDTO.add(contaPlantillasDTO);

      iCuenta++;
      iImporte++;

      // Se envia la plantilla para sumarla al balance.
      balance = balance.add(AccountingHelper.realizarBalance(contaPlantillasDTO));
    }

    // Si el balance final es distinto de cero.
    if (balance.compareTo(BigDecimal.ZERO) != 0) {
      for (Tmct0ContaPlantillasDTO plantillaDTO : plantillasDTO) {
        plantillaDTO.setSinBalance(true);
      }
    }
    Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "[" + AbstractEjecucionPlantilla.class
                                                              + " - FIN dividirPlantillasPorCuentas]" + "[Metodo]"
                                                              + "[" + new Date() + "]");
    return plantillasDTO;
  }


  /**
   * Genera registros transaccionales para un conjunto de importes para Dotacion-Desdotacion.
   * 
   * @param plantilla plantilla
   * @return List<Tmct0ContaPlantillasDTO>
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaDotacionDesdotacion(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

    List<Object[]> importes = null;
    try {
      // Se obtienen los importes para la plantilla.
      Loggers.logDebugQueriesAccounting(plantilla.getCodigo(), SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue(),
                                        null);
      importes = this.contaPlantillasDaoImp.executeQuery(plantilla.getQuery_orden().replace(";", ""),
                                                         plantilla.getCodigo());
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    if (!CollectionUtils.isEmpty(importes)) {
      try {
        for (Object[] importe : importes) {
          contaPlantillasDTOs = dividirPlantillasPorCuentas(plantilla, importe);
          contaPlantillasDTOs = generarApuntes(plantilla, contaPlantillasDTOs, null);
          if (!contaPlantillasDTOs.isEmpty()) {
            plantillasSinBalance.add(contaPlantillasDTOs);
          }
        }
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": "
                                                          + ex.getMessage());
        throw new EjecucionPlantillaException();
      }

      try {
        // Se desbloquean los ALC con los que se trabajÃ³.
        this.contaPlantillasDaoImp.executeQueryUpdate(plantilla.getQuery_final().replace(";", ""),
                                                      plantilla.getCodigo());
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(),
                                   ConstantesError.ERROR_QUERY_FINAL.getValue() + ": " + ex.getMessage());
        throw new EjecucionPlantillaException();
      }
    }
    return plantillasSinBalance;
  }

  /**
   * Genera registros transaccionales para un conjunto de importes para Perdidas y ganancias.
   * 
   * @param plantilla plantilla
   * @return contaPlantillasDTOs
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaPyG(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();
    List<Object[]> importes = null;

    try {
      // Se obtienen los importes para la plantilla de la QUERY DE COBROS.
      Loggers.logDebugQueriesAccounting(plantilla.getCodigo(), SibbacEnums.ConstantesQueries.QUERY_COBRO.getValue(),
                                        null);
      importes = contaPlantillasDaoImp.executeQuery(plantilla.getQuery_cobro().replace(";", ""),
                                                         plantilla.getCodigo());
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_QUERY_COBRO.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {
      // Se recorren todos los importes que me devuelve la consulta
      for (Object[] importe : importes) {
        // Por cada conjunto de importes, genero los apuntes contables.
        // Se divide la plantilla con todos los datos de sus cuentas y los importes.
        contaPlantillasDTOs = dividirPlantillasPorCuentas(plantilla, importe);
        Long idMov = ((BigInteger) importe[importe.length - 1]).longValue();
        contaPlantillasDTOs = generarApuntes(contaPlantillasDTOs, idMov);

        if (!contaPlantillasDTOs.isEmpty()) {
          plantillasSinBalance.add(contaPlantillasDTOs);
        }
      }
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }
    return plantillasSinBalance;
  }

  /**
   * Genera registros transaccionales para un conjunto de importes para emision factura.
   * 
   * @param plantilla plantilla
   * @param estados estados
   * @return List<Tmct0ContaPlantillasDTO>
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaEmisionFactura(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {


    Loggers.logTraceGenericoAccounting(plantilla.getCodigo(),
                                       "[Método ejecutarPlantillas - Emision Factura] Se ejecuta la Query Orden");

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

    List<Object[]> importes = null;
    try {
      // Se obtienen los importes para la plantilla.
      importes = contaPlantillasDaoImp.executeQueryOrdenFacturas(plantilla.getQuery_orden().replace(";", ""),
                                                                 plantilla.getEstado_bloqueado(),
                                                                 plantilla != null ? plantilla.getCodigo() : null);
    } catch (Exception ex) {
      // De haber algun error se para la ejecucion completa.
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    if (!CollectionUtils.isEmpty(importes)) {
      try {
        for (Object[] importe : importes) {
          contaPlantillasDTOs = dividirPlantillasPorCuentas(plantilla, importe);
          contaPlantillasDTOs = generarApuntes(contaPlantillasDTOs, plantilla);
          if (!contaPlantillasDTOs.isEmpty()) {
            plantillasSinBalance.add(contaPlantillasDTOs);
          }
        }
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
        throw new EjecucionPlantillaException();
      }

      try {
        // Se desbloquean las Facturas con los que se ha trabajado
        contaPlantillasDaoImp.executeBloqueofacturas(plantilla.getQuery_bloqueo().replace(";", ""),
                                                     plantilla.getEstado_bloqueado(), plantilla.getEstado_final(),
                                                     plantilla.getCampo_estado(),
                                                     plantilla != null ? plantilla.getCodigo() : null);
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(),
                                   ConstantesError.ERROR_QUERY_DESBLOQUEO.getValue() + ": " + ex.getMessage());
        throw new EjecucionPlantillaException();
      }
    }
    return plantillasSinBalance;
  }

  /**
   * Division de plantilla por cuentas en cobros barridos.
   * 
   * @param plantilla plantilla
   * @param datoQueryOrden datoQueryOrden
   * @param norma43Movimiento norma43Movimiento
   * @return List<Tmct0ContaPlantillasDTO>
   */
  public List<Tmct0ContaPlantillasDTO> dividirPlantillasPorCuentasCB(Tmct0ContaPlantillas plantilla,
                                                                     Object[] datoQueryOrden,
                                                                     Norma43Movimiento norma43Movimiento) {

    List<Tmct0ContaPlantillasDTO> plantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
    Tmct0ContaPlantillasDTO contaPlantillasDTO = null;

    // Se separa por la "," los campos que pueden traer mÃ¡s de un valor por plantilla.
    String[] cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",");
    String[] cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(),
                                                                                        ",");
    String[] auxsDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(), ",");
    String[] auxsHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(), ",");

    int iCuenta = 0;
    int iImporte = 3;
    int iAux = iImporte + cuentasDebe.length; // Indice de las cuenta auxiliares.

    // Map con los valores AUX sacados de la Query
    Map<String, String> auxMap = new HashMap<String, String>();
    for (int i = 0; i < cuentasDebe.length; i++) {
      try {
        if (!auxsDebe[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsDebe[i])) {
          if (auxMap.get(auxsDebe[i]) == null) {
            auxMap.put(auxsDebe[i], (String) datoQueryOrden[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }

      try {
        if (!auxsHaber[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsHaber[i])) {
          if (auxMap.get(auxsHaber[i]) == null) {
            auxMap.put(auxsHaber[i], (String) datoQueryOrden[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }
    }

    for (String cuentaDebe : cuentasDebe) {

      // SafeNull para el importe - ya que la query de orden puede devolver importe NULL
      if (datoQueryOrden[iImporte] == null) {
        datoQueryOrden[iImporte] = BigDecimal.ZERO;
      }

      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      contaPlantillasDTO.setActivo(plantilla.getActivo());
      contaPlantillasDTO.setAudit_date(plantilla.getAudit_date());
      contaPlantillasDTO.setCampo(plantilla.getCampo());
      contaPlantillasDTO.setCampo_estado(plantilla.getCampo_estado());
      contaPlantillasDTO.setCodigo(plantilla.getCodigo());
      contaPlantillasDTO.setCuenta_bancaria(plantilla.getCuenta_bancaria());
      contaPlantillasDTO.setCuenta_debe(StringHelper.quitarComillasDobles(cuentaDebe));
      contaPlantillasDTO.setCuenta_diferencias(StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
      contaPlantillasDTO.setCuenta_haber(StringHelper.quitarComillasDobles(cuentasHaber[iCuenta]));
      contaPlantillasDTO.setEstado_final(plantilla.getEstado_final());
      contaPlantillasDTO.setEstado_inicio(plantilla.getEstado_inicio());
      contaPlantillasDTO.setFichero(plantilla.getFicheroConValor());
      contaPlantillasDTO.setFichero_cobro(plantilla.getFichero_cobro());
      contaPlantillasDTO.setFichero_orden(plantilla.getFichero_orden());
      contaPlantillasDTO.setId(plantilla.getId());
      contaPlantillasDTO.setNum_comprobante(plantilla.getNum_comprobante());
      contaPlantillasDTO.setProceso(plantilla.getProceso());
      contaPlantillasDTO.setQuery_bloqueo(plantilla.getQuery_bloqueo());
      contaPlantillasDTO.setQuery_cobro(plantilla.getQuery_cobro());
      contaPlantillasDTO.setQuery_final(plantilla.getQuery_final());
      contaPlantillasDTO.setQuery_orden(plantilla.getQuery_orden());
      contaPlantillasDTO.setSubproceso(plantilla.getSubproceso());
      contaPlantillasDTO.setSubtipo(plantilla.getSubtipo());
      contaPlantillasDTO.setTipo(plantilla.getTipo());
      contaPlantillasDTO.setTipo_comprobante(plantilla.getTipo_comprobante());
      contaPlantillasDTO.setImporteDebe((BigDecimal) datoQueryOrden[iImporte]);
      contaPlantillasDTO.setImporteHaber(norma43Movimiento.getImporte());
      contaPlantillasDTO.setFecha_comprobante((Date) datoQueryOrden[0]);
      contaPlantillasDTO.setPeriodo_comprobante(((Integer) datoQueryOrden[1]).toString());
      contaPlantillasDTO.setConcepto((String) datoQueryOrden[2]);
      contaPlantillasDTO.setAux_debe(AccountingHelper.getAux(auxsDebe, auxMap, iCuenta));
      contaPlantillasDTO.setAux_haber(AccountingHelper.getAux(auxsHaber, auxMap, iCuenta));

      if (contaPlantillasDTO.getAux_debe() == null)
        contaPlantillasDTO.setAux_debe("");
      if (contaPlantillasDTO.getAux_haber() == null)
        contaPlantillasDTO.setAux_haber("");

      plantillasDTO.add(contaPlantillasDTO);

      iCuenta++;
      iImporte++;

    }
    return plantillasDTO;
  }

  /***************************** Cobros-retrocesiones - Inicio **********************************************/

  /**
   * Obtiene apunte de diferencias.
   * 
   * @param diferencia diferencia
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @throws EjecucionPlantillaException
   */
  public Tmct0ContaApuntes getApunteDiferencia(BigDecimal diferencia,
                                               Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                               CobroDTO cobro) throws EjecucionPlantillaException {
    Tmct0ContaApuntes apunteDiferencia = AccountingHelper.getInitializedTmct0ContaApuntes();
    if (cobro.getIdMov() != null) {
      apunteDiferencia.setIdmovimiento(cobro.getIdMov().intValue());
    }
    apunteDiferencia.setCod_plantilla(contaPlantillasDTO.getCodigo());
    apunteDiferencia.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
    apunteDiferencia.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
    apunteDiferencia.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
    apunteDiferencia.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
    // Para diferencias se toma como cuenta contable cuenta diferencias de la plantilla.
    apunteDiferencia.setCuenta_contable(contaPlantillasDTO.getCuenta_diferencias());
    apunteDiferencia.setAux_contable("");
    apunteDiferencia.setConcepto(contaPlantillasDTO.getConcepto());
    apunteDiferencia.setImporte(diferencia);
    apunteDiferencia.setFichero(contaPlantillasDTO.getFichero());
    if (diferencia.signum() > 0) {
      apunteDiferencia.setTipo_apunte('D');
    } else if (diferencia.signum() < 0) {
      apunteDiferencia.setTipo_apunte('H');
    }
    return apunteDiferencia;
  }

  /**
   * Obtiene apunte de diferencias.
   * 
   * @param diferencia diferencia
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @throws EjecucionPlantillaException
   */
  public Tmct0ContaApuntes getApunteDiferenciaCL(BigDecimal diferencia,
                                                 Tmct0ContaPlantillasDTO contaPlantillasDTOs,
                                                 CobroDTO cobro) throws EjecucionPlantillaException {
    Tmct0ContaApuntes apunteDiferencia = AccountingHelper.getInitializedTmct0ContaApuntes();
    if (cobro.getIdMov() != null) {
      apunteDiferencia.setIdmovimiento(cobro.getIdMov().intValue());
    }
    apunteDiferencia.setCod_plantilla(contaPlantillasDTOs.getCodigo());
    apunteDiferencia.setFecha_comprobante(contaPlantillasDTOs.getFecha_comprobante());
    apunteDiferencia.setPeriodo_comprobante(String.valueOf(contaPlantillasDTOs.getPeriodo_comprobante()));
    apunteDiferencia.setTipo_comprobante(contaPlantillasDTOs.getTipo_comprobante());
    apunteDiferencia.setNum_comprobante(contaPlantillasDTOs.getNum_comprobante());
    // Para diferencias se toma como cuenta contable cuenta diferencias de la plantilla.
    apunteDiferencia.setCuenta_contable(contaPlantillasDTOs.getCuenta_diferencias().replaceAll("\"", ""));
    apunteDiferencia.setAux_contable("");
    apunteDiferencia.setConcepto(contaPlantillasDTOs.getConcepto());
    apunteDiferencia.setImporte(diferencia);
    apunteDiferencia.setFichero(contaPlantillasDTOs.getFichero());
    if (diferencia.signum() > 0) {
      apunteDiferencia.setTipo_apunte('D');
    } else if (diferencia.signum() < 0) {
      apunteDiferencia.setTipo_apunte('H');
    }
    return apunteDiferencia;
  }

  /**
   * Guarda apunte de diferencia para cobros retrocesiones / Liquidaciones.
   * 
   * @param diferencia diferencia
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void guardarApunteDiferencia(BigDecimal diferencia, Tmct0ContaPlantillasDTO contaPlantillasDTOs, CobroDTO cobro) throws EjecucionPlantillaException {
    this.tmct0ContaApuntesDao.save(this.getApunteDiferenciaCL(diferencia, contaPlantillasDTOs, cobro));
  }

  /**
   * Genera y guarda los apuntes al Haber para cobros retrocesiones.
   * 
   * @param importe importe
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @param importeApunte
   * @return Integer Integer
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  private Tmct0ContaApuntes guardarApunteHaber(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                               CobroDTO cobro,
                                               BigDecimal importeApunte) throws EjecucionPlantillaException {

    Tmct0ContaApuntes apunteHaber = this.generarApunteLiquidacionesCobros(contaPlantillasDTO, cobro, TipoApunte.HABER,
                                                                    importeApunte);
    if (!BigDecimal.ZERO.equals(apunteHaber.getImporte()) && apunteHaber.getCuenta_contable() != "") {
      Loggers.logTraceGenericoAccounting(contaPlantillasDTO.getCodigo(), "Creando apunte: " + apunteHaber.toString()
                                                                         + "[Metodo]" + "[" + new Date() + "]");
      this.tmct0ContaApuntesDao.save(apunteHaber);
      return apunteHaber;
    } else {
      Loggers.logTraceGenericoAccounting(contaPlantillasDTO.getCodigo(),
                                         "Sin cuenta o importe cero, no se crea apunte: " + apunteHaber.toString()
                                             + "[Metodo]" + "[" + new Date() + "]");
    }
    return null;

  }

  /**
   * Genera y guarda los apuntes al Debe para cobros retrocesiones.
   * 
   * @param importe importe
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @param importeApunte
   * @return Integer Integer
   * @@throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  private Tmct0ContaApuntes guardarApunteDebe(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                              CobroDTO cobro,
                                              BigDecimal importeApunte) throws EjecucionPlantillaException {

    Tmct0ContaApuntes apunteDebe = this.generarApunteLiquidacionesCobros(contaPlantillasDTO, cobro, TipoApunte.DEBE,importeApunte);
    if (!BigDecimal.ZERO.equals(apunteDebe.getImporte()) && apunteDebe.getCuenta_contable() != "") {
      Loggers.logTraceGenericoAccounting(contaPlantillasDTO.getCodigo(), "Creando apunte: " + apunteDebe.toString()
                                                                         + " [Metodo]" + "[" + new Date() + "]");
      this.tmct0ContaApuntesDao.save(apunteDebe);
      return apunteDebe;
    } else {
      Loggers.logTraceGenericoAccounting(contaPlantillasDTO.getCodigo(),
                                         "Sin cuenta o importe cero, no se crea apunte: " + apunteDebe.toString()
                                             + "[Metodo]" + "[" + new Date() + "]");
    }
    return null;

  }
  
  
  /**
   * Genera y guarda los apuntes al Haber para cobros retrocesiones.
   * 
   * @param importe importe
   * @param iCuenta iCuenta
   * @param plantilla plantilla
   * @param orden orden
   * @param cobro cobro
   * @param importeApunte
   * @return Integer Integer
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  private Tmct0ContaApuntes guardarApuntesLiqParciales(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                               CobroDTO cobro,
                                               BigDecimal importeCobroRest,
                                               TipoApunte tipoApunte) throws EjecucionPlantillaException {

    Tmct0ContaApuntes apunte = this.generarApunteLiqParciales(contaPlantillasDTO, cobro, tipoApunte, importeCobroRest);
    if (!BigDecimal.ZERO.equals(apunte.getImporte()) && apunte.getCuenta_contable() != "") {
      Loggers.logTraceGenericoAccounting(contaPlantillasDTO.getCodigo(), "Creando apunte: " + apunte.toString()
                                                                         + "[Metodo]" + "[" + new Date() + "]");
      this.tmct0ContaApuntesDao.save(apunte);
      return apunte;
    } else {
      Loggers.logTraceGenericoAccounting(contaPlantillasDTO.getCodigo(), "Sin cuenta o importe cero, no se crea apunte: " + apunte.toString() + "[Metodo]" + "[" + new Date() + "]");
    }
    return null;

  }

  /**
	 * Genera y guarda los apuntes al Debe - Apuntes Manuales.
	 * 
	 * @param plantilla
	 *            plantilla
	 * @param object
	 *            object
	 * @throws EjecucionPlantillaException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Tmct0ContaApuntes guardarApunteDebeFM(Tmct0ContaPlantillasDTO plantilla) throws EjecucionPlantillaException {

		Tmct0ContaApuntes apunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
		apunteDebe.setCod_plantilla(plantilla.getCodigo());
		apunteDebe.setFecha_comprobante(plantilla.getFecha_comprobante());
		apunteDebe.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
		apunteDebe.setTipo_comprobante(plantilla.getTipo_comprobante());
		apunteDebe.setNum_comprobante(plantilla.getNum_comprobante());
		apunteDebe.setCuenta_contable(plantilla.getCuenta_debe().replaceAll("\"", ""));
		apunteDebe.setAux_contable(plantilla.getAux_debe().replaceAll("\"", ""));
		String concepto = plantilla.getConcepto();
		concepto = concepto.replaceAll("\t"," ");
		concepto = concepto.replaceAll("\n"," ");
		if (concepto.length()>100) {
			apunteDebe.setConcepto(concepto.substring(0, 100));
		} else {
			apunteDebe.setConcepto(concepto);
		}
		apunteDebe.setImporte(plantilla.getImporte());
		if (plantilla.isSinBalance()) {
			apunteDebe.setGenerado_fichero(9);
		} else {
			apunteDebe.setGenerado_fichero(0);
		}
		apunteDebe.setFichero(plantilla.getFicheroConValor());
		apunteDebe.setTipo_apunte('D');

		this.tmct0ContaApuntesDao.save(apunteDebe);

		return apunteDebe;
	}

	/**
	 * Genera y guarda los apuntes al Haber - Apuntes Manuales.
	 * 
	 * @param plantilla
	 *            plantilla
	 * @param object
	 *            object
	 * @throws EjecucionPlantillaException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Tmct0ContaApuntes guardarApunteHaberFM(Tmct0ContaPlantillasDTO plantilla, BigDecimal baseImponible,
			BigDecimal impImpuesto) throws EjecucionPlantillaException {

		Tmct0ContaApuntes apunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
		apunteHaber.setCod_plantilla(plantilla.getCodigo());
		apunteHaber.setFecha_comprobante(plantilla.getFecha_comprobante());
		apunteHaber.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
		apunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
		apunteHaber.setNum_comprobante(plantilla.getNum_comprobante());
		apunteHaber.setCuenta_contable(plantilla.getCuenta_haber().replaceAll("\"", ""));
		apunteHaber.setAux_contable(plantilla.getAux_haber().replaceAll("\"", ""));
		String concepto = plantilla.getConcepto();
		concepto = concepto.replaceAll("\t"," ");
		concepto = concepto.replaceAll("\n"," ");
		if (concepto.length()>100) {
			apunteHaber.setConcepto(concepto.substring(0, 100));
		} else {
			apunteHaber.setConcepto(concepto);
		}
		if (impImpuesto.compareTo(BigDecimal.ZERO) == 0) {
			apunteHaber.setImporte(plantilla.getImporte().negate());
		} else {
			apunteHaber.setImporte(baseImponible.negate());
		}

		if (plantilla.isSinBalance()) {
			apunteHaber.setGenerado_fichero(9);
		} else {
			apunteHaber.setGenerado_fichero(0);
		}
		apunteHaber.setFichero(plantilla.getFicheroConValor());
		apunteHaber.setTipo_apunte('H');

		this.tmct0ContaApuntesDao.save(apunteHaber);

		return apunteHaber;
	}

	/**
	 * Genera y guarda los apuntes al Haber - Apuntes Manuales.
	 * 
	 * @param plantilla
	 *            plantilla
	 * @param object
	 *            object
	 * @throws EjecucionPlantillaException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Tmct0ContaApuntes guardarApunteHaberIvaFM(Tmct0ContaPlantillasDTO plantilla, BigDecimal impImpuesto)
			throws EjecucionPlantillaException {

		Tmct0ContaApuntes apunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
		apunteHaber.setCod_plantilla(plantilla.getCodigo());
		apunteHaber.setFecha_comprobante(plantilla.getFecha_comprobante());
		apunteHaber.setPeriodo_comprobante(plantilla.getPeriodo_comprobante());
		apunteHaber.setTipo_comprobante(plantilla.getTipo_comprobante());
		apunteHaber.setNum_comprobante(plantilla.getNum_comprobante());
		apunteHaber.setCuenta_contable(plantilla.getCuenta_diferencias().replaceAll("\"", ""));
		apunteHaber.setConcepto(plantilla.getConcepto());
		apunteHaber.setAux_contable("");
		apunteHaber.setImporte(impImpuesto.negate());

		if (plantilla.isSinBalance()) {
			apunteHaber.setGenerado_fichero(9);
		} else {
			apunteHaber.setGenerado_fichero(0);
		}

		apunteHaber.setFichero(plantilla.getFicheroConValor());
		apunteHaber.setTipo_apunte('H');

		this.tmct0ContaApuntesDao.save(apunteHaber);

		return apunteHaber;
	}
  
  /**
   * Genera y retorna el objeto del apunte con toda su información para liquidaciones totales y parciales
   * 
   * @param contaPlantillasDTO
   * @param cobro
   * @param tipoApunte
   * @param importeApunte
   * @return
   */
  private Tmct0ContaApuntes generarApunteLiquidacionesCobros(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                       CobroDTO cobro,
                                                       TipoApunte tipoApunte,
                                                       BigDecimal importeApunte) {

    if (TipoApunte.DEBE.equals(tipoApunte)) {

      Tmct0ContaApuntes apunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
      // No es obligatorio el id de movimiento.
      if (cobro.getIdMov() != null) {
          apunteDebe.setIdmovimiento(cobro.getIdMov().intValue());    	  
      }
      apunteDebe.setCod_plantilla(contaPlantillasDTO.getCodigo());
      apunteDebe.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
      apunteDebe.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
      apunteDebe.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
      apunteDebe.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
      apunteDebe.setCuenta_contable(contaPlantillasDTO.getCuenta_debe().replaceAll("\"", ""));
      apunteDebe.setAux_contable(contaPlantillasDTO.getAux_debe().replaceAll("\"", ""));

      if (CuentasBancarias.CUENTA_COBRO.getCuenta().equals(StringHelper.quitarComillas(apunteDebe.getCuenta_contable()))) {
        apunteDebe.setConcepto(StringUtils.isNotBlank(cobro.getDescReferencia())?cobro.getDescReferencia():contaPlantillasDTO.getConcepto());
        apunteDebe.setImporte((BigDecimal) cobro.getImporte());
      } else {
        apunteDebe.setConcepto(contaPlantillasDTO.getConcepto());
        apunteDebe.setImporte(contaPlantillasDTO.getImporte());
      }

      if (apunteDebe.getImporte().signum() > 0) {
        apunteDebe.setTipo_apunte('D');
      } else if (apunteDebe.getImporte().signum() < 0) {
        apunteDebe.setTipo_apunte('H');
      }

      apunteDebe.setFichero(contaPlantillasDTO.getFichero());

      return apunteDebe;

    } else {

      Tmct0ContaApuntes apunteHaber = new Tmct0ContaApuntes();
      // No es obligatorio el id de movimiento.
      if (cobro.getIdMov() != null) {
    	  apunteHaber.setIdmovimiento(cobro.getIdMov().intValue());
      }
      apunteHaber.setCod_plantilla(contaPlantillasDTO.getCodigo());
      apunteHaber.setTipo_transacc('2');
      apunteHaber.setCod_comp("31");
      apunteHaber.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
      apunteHaber.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
      apunteHaber.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
      apunteHaber.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
      apunteHaber.setCuenta_contable(contaPlantillasDTO.getCuenta_haber().replaceAll("\"", ""));
      apunteHaber.setAux_contable(contaPlantillasDTO.getAux_haber().replaceAll("\"", ""));

      if (CuentasBancarias.CUENTA_COBRO.getCuenta().equals(StringHelper.quitarComillas(apunteHaber.getCuenta_contable()))) {
    	apunteHaber.setConcepto(StringUtils.isNotBlank(cobro.getDescReferencia())?cobro.getDescReferencia():contaPlantillasDTO.getConcepto());
        apunteHaber.setImporte(cobro.getImporte().negate());
      } else {
        apunteHaber.setConcepto(contaPlantillasDTO.getConcepto());
        apunteHaber.setImporte(contaPlantillasDTO.getImporte().negate());
      }

      apunteHaber.setAudit_date(new Date());
      apunteHaber.setAudit_user("SIBBAC");

      if (apunteHaber.getImporte().signum() > 0) {
        apunteHaber.setTipo_apunte('D');
      } else if (apunteHaber.getImporte().signum() < 0) {
        apunteHaber.setTipo_apunte('H');
      }

      apunteHaber.setFichero(contaPlantillasDTO.getFichero());
      apunteHaber.setGenerado_fichero(0);

      return apunteHaber;

    }

  }
  
  
  /**
   * Genera y retorna el objeto del apunte con toda su información para liquidaciones totales y parciales
   * 
   * @param contaPlantillasDTO
   * @param cobro
   * @param tipoApunte
   * @param importeApunte
   * @return
   */
  private Tmct0ContaApuntes generarApunteLiqParciales(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                       CobroDTO cobro,
                                                       TipoApunte tipoApunte,
                                                       BigDecimal importeCobroRest) {
	  

	  if (TipoApunte.DEBE.equals(tipoApunte)) {

	      Tmct0ContaApuntes apunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
	      // No es obligatorio el id de movimiento.
	      if (cobro.getIdMov() != null) {
	          apunteDebe.setIdmovimiento(cobro.getIdMov().intValue());    	  
	      }
	      apunteDebe.setCod_plantilla(contaPlantillasDTO.getCodigo());
	      apunteDebe.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
	      apunteDebe.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
	      apunteDebe.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
	      apunteDebe.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
	      apunteDebe.setCuenta_contable(contaPlantillasDTO.getCuenta_debe().replaceAll("\"", ""));
	      apunteDebe.setAux_contable(contaPlantillasDTO.getAux_debe().replaceAll("\"", ""));
	
	      if (CuentasBancarias.CUENTA_COBRO.getCuenta().equals(StringHelper.quitarComillas(apunteDebe.getCuenta_contable()))) {
	        apunteDebe.setConcepto(StringUtils.isNotBlank(cobro.getDescReferencia())?cobro.getDescReferencia():contaPlantillasDTO.getConcepto());
	        apunteDebe.setImporte((BigDecimal) cobro.getImporte());
	      } else {
	        apunteDebe.setConcepto(contaPlantillasDTO.getConcepto());
	        if(contaPlantillasDTO.getImporte().compareTo(importeCobroRest)<0){
	        	apunteDebe.setImporte(contaPlantillasDTO.getImporte());
	        }else{
	        	apunteDebe.setImporte(importeCobroRest);
	        }
	      }
	
	      if (apunteDebe.getImporte().signum() > 0) {
	        apunteDebe.setTipo_apunte('D');
	      } else if (apunteDebe.getImporte().signum() < 0) {
	        apunteDebe.setTipo_apunte('H');
	      }
	
	      apunteDebe.setFichero(contaPlantillasDTO.getFichero());
	
	      return apunteDebe;

	  } else {

	      Tmct0ContaApuntes apunteHaber = new Tmct0ContaApuntes();
	      // No es obligatorio el id de movimiento.
	      if (cobro.getIdMov() != null) {
	    	  apunteHaber.setIdmovimiento(cobro.getIdMov().intValue());
	      }
	      apunteHaber.setCod_plantilla(contaPlantillasDTO.getCodigo());
	      apunteHaber.setTipo_transacc('2');
	      apunteHaber.setCod_comp("31");
	      apunteHaber.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
	      apunteHaber.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
	      apunteHaber.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
	      apunteHaber.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
	      apunteHaber.setCuenta_contable(contaPlantillasDTO.getCuenta_haber().replaceAll("\"", ""));
	      apunteHaber.setAux_contable(contaPlantillasDTO.getAux_haber().replaceAll("\"", ""));
	
	      if (CuentasBancarias.CUENTA_COBRO.getCuenta().equals(StringHelper.quitarComillas(apunteHaber.getCuenta_contable()))) {
	    	apunteHaber.setConcepto(StringUtils.isNotBlank(cobro.getDescReferencia())?cobro.getDescReferencia():contaPlantillasDTO.getConcepto());
	        apunteHaber.setImporte(cobro.getImporte().negate());
	      } else {
	    	  apunteHaber.setConcepto(contaPlantillasDTO.getConcepto());
	    	  if(contaPlantillasDTO.getImporte().compareTo(importeCobroRest)<0){
	    		  apunteHaber.setImporte(contaPlantillasDTO.getImporte().negate());
	    		  importeCobroRest = importeCobroRest.subtract(apunteHaber.getImporte());
	    	  }else{
	    		  apunteHaber.setImporte(importeCobroRest.negate());
	    		  importeCobroRest = BigDecimal.ZERO;
	    	  }
	      }
	
	      apunteHaber.setAudit_date(new Date());
	      apunteHaber.setAudit_user("SIBBAC");
	
	      if (apunteHaber.getImporte().signum() > 0) {
	        apunteHaber.setTipo_apunte('D');
	      } else if (apunteHaber.getImporte().signum() < 0) {
	        apunteHaber.setTipo_apunte('H');
	      }
	
	      apunteHaber.setFichero(contaPlantillasDTO.getFichero());
	      apunteHaber.setGenerado_fichero(0);
	
	      return apunteHaber;

	  }
  }

  /**
   * Generacion de apuntes contables para cobros-retrocesiones.
   * 
   * @param importeOrden importeOrden
   * @param iCuenta iCuenta
   * @param orden orden
   * @param cobro cobro
   * @param plantilla plantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public List<Tmct0ContaApuntes> generarApuntesContables(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                         CobroDTO cobro,
                                                         Tmct0ContaPlantillas plantilla,
                                                         List<String> importDetText,
                                                         int i,
                                                         BigDecimal importeApunte) throws EjecucionPlantillaException {

	  Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Se generan los apuntes para la plantilla " + plantilla.getCodigo() + ") [Metodo][" + new Date() + "]");

	  List<Tmct0ContaApuntes> apuntesGenerados = new ArrayList<Tmct0ContaApuntes>();
	  String cuenta = CuentasBancarias.CUENTA_COBRO.getCuenta();
	  if (StringUtils.isNotBlank(contaPlantillasDTO.getCuenta_debe()) && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO)!=0 || contaPlantillasDTO.getCuenta_debe().equals(cuenta))) {
		  // Se genera y se guarda el apunte al debe.
		  Tmct0ContaApuntes apunteDebe = this.guardarApunteDebe(contaPlantillasDTO, cobro, importeApunte);
		  if (apunteDebe != null) {
			  apuntesGenerados.add(apunteDebe);
		  }
		  if ((plantilla.getGrabaDetalle().equals('D') || plantilla.getGrabaDetalle().equals('A')) && apunteDebe.getId() != null) {
			  this.tmct0ContaApunteDetalleBo.insertSelectCobrosLiquidaciones(plantilla, apunteDebe.getId(), contaPlantillasDTO.getParamsDetail(), importDetText, i);
		  }
	  }

	  if (StringUtils.isNotBlank(contaPlantillasDTO.getCuenta_haber()) && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO)!=0 || contaPlantillasDTO.getCuenta_haber().equals(cuenta))) {
		  // Se genera y se guarda el apunte al haber.
		  Tmct0ContaApuntes apunteHaber = this.guardarApunteHaber(contaPlantillasDTO, cobro, importeApunte);
		  if (apunteHaber != null) {
			  apuntesGenerados.add(apunteHaber);
		  }
		  if ((plantilla.getGrabaDetalle().equals('H') || plantilla.getGrabaDetalle().equals('A')) && apunteHaber.getId() != null) {
			  this.tmct0ContaApunteDetalleBo.insertSelectCobrosLiquidaciones(plantilla, apunteHaber.getId(), contaPlantillasDTO.getParamsDetail(), importDetText, i);
		  }
	  }
	  
	  return apuntesGenerados;

  }
  
  
  /**
   * Generacion de apuntes contables para cobros-retrocesiones.
   * 
   * @param importeOrden importeOrden
   * @param iCuenta iCuenta
   * @param orden orden
   * @param cobro cobro
   * @param plantilla plantilla
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public List<Tmct0ContaApuntes> generarApuntesContablesLiqParcial(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                         CobroDTO cobro,
                                                         Tmct0ContaPlantillas plantilla,
                                                         List<String> importDetText,
                                                         int i,
                                                         BigDecimal importeCobroRest) throws EjecucionPlantillaException {

	  Loggers.logTraceGenericoAccounting(plantilla.getCodigo(),
                                       "Se generan los apuntes para la plantilla " + plantilla.getCodigo() + ")"
                                           + " [Metodo]" + "[" + new Date() + "]");

	  List<Tmct0ContaApuntes> apuntesGenerados = new ArrayList<Tmct0ContaApuntes>();

	  String cuenta = CuentasBancarias.CUENTA_COBRO.getCuenta();
	  if (StringUtils.isNotBlank(contaPlantillasDTO.getCuenta_debe()) && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO)!=0 || contaPlantillasDTO.getCuenta_debe().equals(cuenta))) {
		  // Se genera y se guarda el apunte al debe.
		  Tmct0ContaApuntes apunteDebe = this.guardarApuntesLiqParciales(contaPlantillasDTO, cobro, importeCobroRest, TipoApunte.DEBE);
		  if (apunteDebe != null) {
			  apuntesGenerados.add(apunteDebe);
		  }
		  if ((plantilla.getGrabaDetalle().equals('D') || plantilla.getGrabaDetalle().equals('A')) && apunteDebe.getId() != null) {
			  this.tmct0ContaApunteDetalleBo.generarApuntesDetalleLiquidacionesParciales(plantilla, apunteDebe.getId(), contaPlantillasDTO.getParamsDetail(), importeCobroRest, importDetText, i);
		  }

	  }
	  
	  if (StringUtils.isNotBlank(contaPlantillasDTO.getCuenta_haber()) && (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO)!=0 || contaPlantillasDTO.getCuenta_haber().equals(cuenta))) {
		  // Se genera y se guarda el apunte al haber.
		  Tmct0ContaApuntes apunteHaber = this.guardarApuntesLiqParciales(contaPlantillasDTO, cobro, importeCobroRest, TipoApunte.HABER);
		  if (apunteHaber != null) {
			  apuntesGenerados.add(apunteHaber);
		  }
		  if ((plantilla.getGrabaDetalle().equals('H') || plantilla.getGrabaDetalle().equals('A')) && apunteHaber.getId() != null) {
			  this.tmct0ContaApunteDetalleBo.generarApuntesDetalleLiquidacionesParciales(plantilla, apunteHaber.getId(), contaPlantillasDTO.getParamsDetail(),importeCobroRest, importDetText, i);
		  }
	  }

	  return apuntesGenerados;

  }
  

  /**
   * Obtiene ALCs para cobros-retrocesiones.
   * 
   * @param plantilla plantilla
   * @param cobro cobro
   * @return List<AlcDTO> List<AlcDTO>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  public List<AlcDTO> obtenerALCs(Tmct0ContaPlantillas plantilla, List<Object> paramsDetail) throws EjecucionPlantillaException {
	  
    List<AlcDTO> dtos = new ArrayList<AlcDTO>();
    List<Object[]> alcBloqueados = this.contaPlantillasDaoImp.executeQueryParams(plantilla.getQuery_bloqueo(), paramsDetail,plantilla.getCodigo());
    
    if (alcBloqueados != null && alcBloqueados.size() > 0) {
      for (Object[] alc : alcBloqueados) {
    	List<BigDecimal> importes = new ArrayList<BigDecimal>(); 
    	for(int i=4;i<alc.length;i++){
    		importes.add((BigDecimal) alc[i]);
    	}
        AlcDTO alcDto = new AlcDTO((String) alc[1], (String) alc[0], (String) alc[3], (BigDecimal) alc[2], importes);
        dtos.add(alcDto);
      }
    }
    return dtos;
  }
  
  public List<AlcDTO> obtenerALCsAM(String queryBloqueo, String codigoPlantilla) throws EjecucionPlantillaException {
	  
    List<AlcDTO> dtos = new ArrayList<AlcDTO>();
    List<Object[]> alcBloqueados = this.contaPlantillasDaoImp.executeQuery(queryBloqueo, codigoPlantilla);
    
    if (alcBloqueados != null && alcBloqueados.size() > 0) {
      for (Object[] alc : alcBloqueados) {
    	List<BigDecimal> importes = new ArrayList<BigDecimal>(); 
    	for(int i=4;i<alc.length;i++){
    		importes.add((BigDecimal) alc[i]);
    	}
        AlcDTO alcDto = new AlcDTO((String) alc[1], (String) alc[0], (String) alc[3], (BigDecimal) alc[2], importes);
        dtos.add(alcDto);
      }
    }
    return dtos;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  private List<Tmct0ContaPlantillasDTO> procesarOrdenes(List<Object[]> restOrden, Tmct0ContaPlantillas plantilla,
      CobroDTO cobro) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> listaTmct0ContaPlantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();

    // Se bloquean los ALC con los que se va a trabajar.
    for (int k = 0; k < restOrden.size(); k++) {

      Object[] importes = restOrden.get(k);

      BigDecimal balance = BigDecimal.ZERO;
      BigDecimal importeCobro = cobro.getImporte();
      BigDecimal importeOrdenTotal = BigDecimal.ZERO;

      // Suma de todos los importes de un resultado de la queryOrden
      int numImportes = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(),
          ",").length;

      for (int i = 0; i < numImportes; i++) {
        importeOrdenTotal = importeOrdenTotal.add((BigDecimal) importes[i + 3]);
      }

      BigDecimal diferencia = importeOrdenTotal.subtract(importeCobro);
      if (diferencia.signum() < 0) {
        diferencia = diferencia.negate();
      }
      Boolean isSinDiferencia = BigDecimal.ZERO.equals(diferencia);
      Boolean isDiferenciaMenorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) <= 0;
      Boolean isDiferenciaMayorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) > 0;

      // entre el IMPORTE de la query de orden y el IMPORTE de la query de cobro es superior 
      //al tolerance definido para la cuenta afectada (campo TOLERANCE de la plantilla):

      if (isDiferenciaMayorQueToleranciaPlantilla) {
        Loggers.logTraceGenericoAccounting(plantilla.getCodigo(),
            " La diferencia entre el IMPORTE de la query de orden y el IMPORTE de la query de cobro es superior al tolerance definido para la cuenta afectada (Tolerancia de plantilla: "
                + plantilla.getTolerancia() + ") [Metodo][" + new Date() + "]");

        // i. El registro del cobro se tiene que pasar a PROCESADO=2 en la tabla
        // TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV y
        // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc
        // referencia y procesado 2 porque generan descuadre.
        tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobro.getIdMov(), 2);

        try {
          // ii. Se debe dar de alta un descuadre en la tabla de descuadres con
          // el error LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO (IMPORTE_COBRO) Y EL PDTE. DE COBRO
          // (IMPORTE_ORDEN) ES SUPERIOR AL TOLERANCE DEFINIDO
          // PARA LA CUENTA (TOLERANCE).
          this.tmct0Norma43DescuadreBo.crearDescuadre(
              "LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO (" + String.valueOf(importeCobro) + ") Y EL PDTE. DE COBRO ("
                  + String.valueOf(importeOrdenTotal) + ") ES SUPERIOR AL TOLERANCE DEFINIDO PARA LA CUENTA ("
                  + String.valueOf(plantilla.getTolerancia()) + ")",
              plantilla, cobro, importeOrdenTotal);

        }
        catch (Exception ex) {
          Loggers.logErrorAccounting(plantilla.getCodigo(),
              ConstantesError.ERROR_DESCUADRES.getValue() + ": " + ex.getMessage());
          throw new EjecucionPlantillaException();
        }
      }

      // el importe no tiene diferencias o la diferencia es menor
      // o igual al tolerance (campo TOLERANCE de la plantilla):
      if (isSinDiferencia || isDiferenciaMenorQueToleranciaPlantilla) {

        List<AlcDTO> alcsImplicadas = new ArrayList<AlcDTO>();
        List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = dividirPlantillasPorCuentasCobrosLiquidaciones(plantilla,
            importes);

        for (int j = 0; j < contaPlantillasDTOs.size(); j++) {
          Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(j);

          Loggers.logTraceGenericoAccounting(plantilla.getCodigo(),
              "El importe no tiene diferencias o la diferencia es menor o igual al tolerance (campo TOLERANCE de la plantilla) [Metodo]["
                  + new Date() + "]");

          // i. El registro del cobro se tiene que pasar a PROCESADO=1 en la
          // tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV
          // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc
          // referencia y procesado 1 porque NO generan descuadre.
          tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobro.getIdMov(), 1);

          // ii. Se debe generar el apunte contable tal cual define la plantilla.
          List<List<String>> listImportDetalle = AccountingHelper
              .getListImportesTextos(plantilla.getQuery_apunte_detalle());

          try {
            balance = balance.add(getBalance(
                generarApuntesContables(contaPlantillasDTO, cobro, plantilla, listImportDetalle.get(0), j, null)));
          }
          catch (Exception ex) {
            Loggers.logErrorAccounting(plantilla.getCodigo(),
                ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
            throw new EjecucionPlantillaException();
          }

          // Cobros y retrocesiones
          alcsImplicadas = this.obtenerALCs(plantilla, contaPlantillasDTO.getParamsDetail());
          sumaImporteCampoAlc(alcsImplicadas, contaPlantillasDTO.getCampo(), j, plantilla);
        }

        if (balance.compareTo(BigDecimal.ZERO) != 0 && !contaPlantillasDTOs.isEmpty()) {
          guardarApunteDiferencia(balance.negate(), contaPlantillasDTOs.get(0), cobro);
        }
      }
    }

    return listaTmct0ContaPlantillasDTO;
  }

  /**
   * Genera registros transaccionales para un conjunto de importes para
   * cobros-retrocesiones.
   * 
   * @param plantilla plantilla
   * @param cobros cobros
   * @return contaPlantillasDTOs
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<Tmct0ContaPlantillasDTO> ejecutarPlantillaCobrosRetrocesiones(Tmct0ContaPlantillas plantilla,
      Object[] cobro) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    try {
      CobroDTO cobroDTO = new CobroDTO(cobro);

      // 2. Si se obtienen registros, por cada registro obtenido se debe ejecutar la query
      //de las ordenes (QUERY_ORDEN) para localizar la orden, el literal 
      //<DESC_REFERENCIA DE LA QUERY_COBRO> de la query de las ordenes debe 
      //sustituirse por el valor DESC_REFERENCIA de cada registro obtenido 
      //de la query de cobros de forma que:
      List<Object[]> ordenes = null;
      if (plantilla.getQuery_orden() != null) {
        try {
          if (plantilla.getQuery_orden() != null) {
            Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Buscando ordenes para el cobro: CobroDTO: "
                + cobro.toString() + " [ejecutarPlantillaCobrosRetrocesiones]" + "[" + new Date() + "]");
            ordenes = ejecutaQueryOrden(plantilla, cobro, null);
          }
        }
        catch (Exception ex) {
          Loggers.logErrorAccounting(plantilla.getCodigo(),
              ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
          throw new EjecucionPlantillaException();
        }
      }
      if (ordenes != null && !ordenes.isEmpty()) {

        Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Obtenidas " + ordenes.size()
            + " ordenes para el cobro." + " [ejecutarPlantillaCobrosRetrocesiones]" + "[" + new Date() + "]");
        contaPlantillasDTOs.addAll(procesarOrdenes(ordenes, plantilla, cobroDTO));

      }
      else { // a. Si no se encuentra una orden relacionada:

        Loggers.logTraceGenericoAccounting(plantilla.getCodigo(),
            "No se encuentra orden relacionada. El registro del cobro se tiene que actualizar a procesado = 2. "
                + "Se debe dar de alta un descuadre." + " [ejecutarPlantillaCobrosRetrocesiones]" + "[" + new Date()
                + "]");

        // i. El registro del cobro se tiene que actualizar a PROCESADO=1 en la
        // tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV.
        // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc
        // referencia y procesado 2 porque generan descuadre.
        tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobroDTO.getIdMov(), 2);

        try {
          // ii. Se debe dar de alta un descuadre (en el punto 2.3.4 se define
          // como se crean los descuadres en la tabla de descuadres) en la 
          //tabla de descuadres con el error NO SE HA ENCONTRADO ORDEN EN SIBBAC.
          tmct0Norma43DescuadreBo.crearDescuadre("NO SE HA ENCONTRADO ORDEN EN SIBBAC", plantilla, cobroDTO, null);
        }
        catch (Exception ex) {
          Loggers.logErrorAccounting(plantilla.getCodigo(),
              ConstantesError.ERROR_DESCUADRES.getValue() + ": " + ex.getMessage());
          throw new EjecucionPlantillaException();
        }
      }
    }
    catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo() != null ? plantilla.getCodigo() : null,
          ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + " : " + ex.getMessage());
      throw new EjecucionPlantillaException(ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue(), ex);
    }
    return contaPlantillasDTOs;
  }


  /**
   * Suma del campo de ALC.
   * 
   * @param alcBloqueados alcBloqueados
   * @param campo campo
   * @param importe importe
   * @param estadoFinal estadoFinal
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void sumaImporteCampoAlc(List<AlcDTO> alcBloqueados, String campo, int indexImporte,  Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {
    
    String estadoFinal = plantilla.getEstado_final();
    String campoEstado = plantilla.getCampo_estado();
    String codigoPlantilla = plantilla.getCodigo();
    
	
    if (alcBloqueados != null && alcBloqueados.size() > 0) {
      for (AlcDTO alcDto : alcBloqueados) {
        BigDecimal importe = alcDto.getImportes().get(indexImporte);
        String alc = alcDto.toString();
        
        if(plantilla.getTipo().equals(TipoContaPlantillas.RETROCESION.getTipo())) {
          importe = importe.multiply(new BigDecimal(("-1")));
        }
        Loggers.logTraceGenericoAccounting(codigoPlantilla, "Sumando importe " + importe + " al campo " + campo + " del ALC: " + alc + " [sumaImporteCampoAlc]" + "[" + new Date() + "]");
        	this.contaPlantillasDaoImp.executeQueryQuerySumaImporteCampoAlc(campo, alcDto, importe, estadoFinal, campoEstado, codigoPlantilla);
      }
    }
  }
  
  /**
   * Suma del campo de ALC.
   * 
   * @param alcBloqueados alcBloqueados
   * @param campo campo
   * @param importe importe
   * @param estadoFinal estadoFinal
   * @throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void sumaImporteCampoAlcLiqParcial(List<AlcDTO> alcBloqueados, String campo, int indexImporte, BigDecimal importeRestante, 
		  String estadoFinal, String estadoParcial, String campoEstado, String codigoPlantilla) throws EjecucionPlantillaException {
	
    if (alcBloqueados != null && alcBloqueados.size() > 0) {
      for (AlcDTO alcDto : alcBloqueados) {
    	  if(importeRestante.compareTo(BigDecimal.ZERO) == 1){
	    	  Loggers.logTraceGenericoAccounting(codigoPlantilla, "Sumando importe " + alcDto.getImportes().get(indexImporte) + " al campo " + campo + " del ALC: " + alcDto.toString() + " [sumaImporteCampoAlc]" + "[" + new Date() + "]");
	    	  BigDecimal importeACobrar = importeRestante.compareTo(alcDto.getImportes().get(indexImporte))==-1?importeRestante:alcDto.getImportes().get(indexImporte);
	    	  
	    	  if(importeACobrar.compareTo(alcDto.getImportes().get(indexImporte))==-1){
	    		  this.contaPlantillasDaoImp.executeQueryQuerySumaImporteCampoAlc(campo, alcDto, importeACobrar, estadoParcial, campoEstado, codigoPlantilla);
	    	  }else{
	    		  this.contaPlantillasDaoImp.executeQueryQuerySumaImporteCampoAlc(campo, alcDto, importeACobrar, estadoFinal, campoEstado, codigoPlantilla);
	    	  }
	    	  importeRestante = importeRestante.subtract(importeACobrar);
    	  }
      }
    }
  }

  /**
   * Procesamiento de ordenes para liquidaciones.
   * 
   * @param ordenes ordenes
   * @param plantilla plantilla
   * @param cobro cobro
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   * @return List<Tmct0ContaPlantillasDTO> List<Tmct0ContaPlantillasDTO>
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public List<Tmct0ContaPlantillasDTO> procesarOrdenesLiquidacionesTotales(List<Object[]> restOrden,
                                                                           Tmct0ContaPlantillas plantilla,
                                                                           CobroDTO cobro) throws EjecucionPlantillaException {
	  
	  	List<Tmct0ContaPlantillasDTO> listaTmct0ContaPlantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
	  	
  		for(int k=0;k<restOrden.size();k++){
  	
  			Object[] importes = restOrden.get(k);

	        BigDecimal balance = BigDecimal.ZERO;
	        BigDecimal importeCobro = cobro.getImporte();
	        BigDecimal importeOrdenTotal = BigDecimal.ZERO;
    
	        // Suma de todos los importes de un resultado de la queryOrden
    		int numImportes = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",").length;
    		
	       	for(int i=0;i<numImportes;i++){
	       		importeOrdenTotal = importeOrdenTotal.add((BigDecimal) importes[i+3]);
	       	}
    
		    BigDecimal diferencia = importeOrdenTotal.subtract(importeCobro);
		    if (diferencia.signum() < 0) {
		    	diferencia = diferencia.negate();
		    }
		    Boolean isSinDiferencia = BigDecimal.ZERO.equals(diferencia);
		    Boolean isDiferenciaMenorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) <= 0;
		    Boolean isDiferenciaMayorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) > 0;


	        // c. Si la diferencia entre el IMPORTE de la
	        // query de orden y el IMPORTE de la query de cobro es superior al tolerance
	        // definido para la cuenta afectada (campo TOLERANCE de la plantilla):
	    	if (isDiferenciaMayorQueToleranciaPlantilla) {
    			Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "El IMPORTE de la query de cobro es superior al tolerance definido para la cuenta afectada (Tolerancia de plantilla: "
                                           + plantilla.getTolerancia() + ") [procesarOrdenesLiquidacionesTotales]" + "[" + new Date() + "]");

	            // i. El registro del cobro se tiene que pasar a PROCESADO=1 en la
	            // tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV y
	            // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc referencia y procesado 2 porque generan
	            // descuadre.
    			this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobro.getIdMov(), 2);

	            // ii. Se debe dar de alta un descuadre en la tabla de descuadres con
	            // el error LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO (IMPORTE_COBRO) Y EL
	            // PDTE. DE COBRO (IMPORTE_ORDEN) ES SUPERIOR
	            // AL TOLERANCE DEFINIDO PARA LA CUENTA (TOLERANCE).
	            this.tmct0Norma43DescuadreBo.crearDescuadre("LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO (" + String.valueOf(importeCobro) + ") Y EL PDTE. DE COBRO ("
                                                        + String.valueOf(importeOrdenTotal) + ") ES SUPERIOR AL TOLERANCE DEFINIDO PARA LA CUENTA ("
                                                        + String.valueOf(plantilla.getTolerancia()) + ")", plantilla, cobro, importeOrdenTotal);
	    	}

	        // d. Finalmente, si el importe no tiene
	        // diferencias o la diferencia es menor o igual al tolerance (campo TOLERANCE de la plantilla):
		    if (isSinDiferencia || isDiferenciaMenorQueToleranciaPlantilla) {
		    		
		    	List<AlcDTO> alcsImplicadas = new ArrayList<AlcDTO>();
	    		List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = dividirPlantillasPorCuentasCobrosLiquidaciones(plantilla, importes);
        		
        		for (int j = 0; j < contaPlantillasDTOs.size(); j++) {
        			Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(j);

        			Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "El importe no tiene diferencias o la diferencia es menor o igual al tolerance (campo TOLERANCE de la plantilla)"
                                               + " [procesarOrdenesLiquidacionesTotales][" + new Date() + "]");

        			// i. El registro del cobro se tiene que pasar av PROCESADO=1 en la
        			// tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV
        			// MFG 18/04/2017 Se pasan todos los movimientos asociados a desc referencia y procesado 1 porque NO generan
        			// descuadre.
        			this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobro.getIdMov(), 1);

        			try {
		            	// ii. Se debe generar el apunte contable tal cual define la plantilla.
		            	List<List<String>> listImportDetalle = AccountingHelper.getListImportesTextos(plantilla.getQuery_apunte_detalle());
		            	balance = balance.add(getBalance((this.generarApuntesContables(contaPlantillasDTO, cobro, plantilla, listImportDetalle.get(0), j, null))));
        			} catch (Exception ex) {
        				// De haber algun error se para la ejecucion completa.
        				Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
        				throw new EjecucionPlantillaException();
        			}
            
        			alcsImplicadas = this.obtenerALCs(plantilla, contaPlantillasDTO.getParamsDetail());
        			this.sumaImporteCampoAlc(alcsImplicadas, contaPlantillasDTO.getCampo(), j, plantilla);

        		}
	    	
		    	if (balance.compareTo(BigDecimal.ZERO) != 0 && !contaPlantillasDTOs.isEmpty()) {
		            this.guardarApunteDiferencia(balance.negate(), contaPlantillasDTOs.get(0), cobro);
		        }
	    	}
		}

		return listaTmct0ContaPlantillasDTO;

  }

  /**
   * Procesamiento de ordenes para liquidaciones parciales.
   * 
   * @param ordenes ordenes
   * @param plantilla plantilla
   * @param cobro cobro
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   * @return List<Tmct0ContaPlantillasDTO> List<Tmct0ContaPlantillasDTO>
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public List<Tmct0ContaPlantillasDTO> procesarOrdenesLiquidacionesParciales(List<Object[]> restOrden,
                                                                             Tmct0ContaPlantillas plantilla,
                                                                             CobroDTO cobro) throws EjecucionPlantillaException {

	  
	  	List<Tmct0ContaPlantillasDTO> listaTmct0ContaPlantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
	  
	  	BigDecimal importeCobro = cobro.getImporte();
	  	BigDecimal importeCobroRestante = cobro.getImporte();
    

  		for(int k=0;k<restOrden.size();k++){
	
			Object[] importes = restOrden.get(k);

			BigDecimal importeOrdenTotal = BigDecimal.ZERO;

	        // Suma de todos los importes de un resultado de la queryOrden
			int numImportes = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",").length;
	       	for(int i=0;i<numImportes;i++){
	       		importeOrdenTotal = importeOrdenTotal.add((BigDecimal) importes[i+3]);
	       	}

		    BigDecimal diferencia = importeOrdenTotal.subtract(importeCobro);
		    if (diferencia.signum() < 0) {
		    	diferencia = diferencia.negate();
		    }
		    Boolean isSinDiferencia = BigDecimal.ZERO.compareTo(diferencia)==0;
		    Boolean isDiferenciaMayorQueToleranciaPlantilla = diferencia.compareTo(plantilla.getTolerancia()) > 0;
		    Boolean isImporteCobroMayorImporteOrden = (importeCobro.compareTo(importeOrdenTotal) > 0);
	        Boolean isImporteCobroMenorImporteOrden = (importeCobro.compareTo(importeOrdenTotal) < 0);
	        Boolean isDiferenciaMenorIgualQueToleranciaPlantilla = !isDiferenciaMayorQueToleranciaPlantilla;

	        Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Procesando importe de orden: " + importeOrdenTotal + ", importe de cobro: " 
	        									+ importeCobro + " [procesarOrdenesLiquidacionesParciales][" + new Date() + "]");


        	// c. Si el importe obtenido de la querycobro es superior
        	// al IMPORTE de la queryorden y la diferencia es superior al
        	// tolerance definido para la cuenta afectada (campo TOLERANCE
        	// de la plantilla)
        	if (isDiferenciaMayorQueToleranciaPlantilla) {
        			Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "El IMPORTE de la query de cobro es superior al tolerance definido para la cuenta afectada (Tolerancia de plantilla: "
                                               			+ plantilla.getTolerancia() + ") [procesarOrdenesLiquidacionesParciales][" + new Date()+ "]");

		            // i. El registro del cobro se tiene que pasar a PROCESADO=1 en la
		            // tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el campo ID_MOV y
		            // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc referencia y procesado 2 porque generan
		            // descuadre.
		            this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobro.getIdMov(), 2);

		            // ii. Se debe dar de alta un descuadre en la tabla de descuadres con
		            // el error LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO (IMPORTE_COBRO) Y EL
		            // PDTE. DE COBRO (IMPORTE_ORDEN) ES SUPERIOR
		            // AL TOLERANCE DEFINIDO PARA LA CUENTA (TOLERANCE).
		            this.tmct0Norma43DescuadreBo.crearDescuadre("LA DIFERENCIA ENTRE EL IMPORTE RECIBIDO (" + String.valueOf(importeCobro) + ") Y EL PDTE. DE COBRO ("
                                                        		+ String.valueOf(importeOrdenTotal) + ") ES SUPERIOR AL TOLERANCE DEFINIDO PARA LA CUENTA ("
                                                        		+ String.valueOf(plantilla.getTolerancia()) + ")", plantilla, cobro, importeOrdenTotal);
        	} else {

	            // i. El registro del cobro se tiene que pasar av
	            // PROCESADO=1 en la
	            // tabla TMCT0_MOVIMIENTO_NORMA43 buscando por el campo
	            // ID_MOV
	            // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc referencia y procesado 1 porque NO generan
	            // descuadre.
	            this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobro.getIdMov(), 1);

	            try {
    	
	            	BigDecimal balance = BigDecimal.ZERO;
	            	List<AlcDTO> alcsImplicadas = new ArrayList<AlcDTO>();
	            	  List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = dividirPlantillasPorCuentasCobrosLiquidaciones(plantilla, importes);
  	  
	            	for (int j = 0; j < contaPlantillasDTOs.size(); j++) {
    	  
	            		Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(j);
		                // ii. Se debe generar el apunte contable tal cual
		                // define la plantilla.
	            		alcsImplicadas = this.obtenerALCs(plantilla, contaPlantillasDTO.getParamsDetail());
	            		
		                List<List<String>> listImportDetalle = AccountingHelper.getListImportesTextos(plantilla.getQuery_apunte_detalle());

		                if (isSinDiferencia || isImporteCobroMenorImporteOrden) {
		                	if(importeCobroRestante.compareTo(BigDecimal.ZERO)==1){
		                		this.generarApuntesContablesLiqParcial(contaPlantillasDTO, cobro, plantilla, listImportDetalle.get(0), j, importeCobroRestante);
		                		this.sumaImporteCampoAlcLiqParcial(alcsImplicadas, contaPlantillasDTO.getCampo(), j, importeCobroRestante, plantilla.getEstado_final(), 
		                																  plantilla.getEstado_parcial(), plantilla.getCampo_estado(), plantilla.getCodigo());
		                	}
		                } else if (isImporteCobroMayorImporteOrden && isDiferenciaMenorIgualQueToleranciaPlantilla) {

		                	balance = balance.add(getBalance(this.generarApuntesContables(contaPlantillasDTO, cobro, plantilla, listImportDetalle.get(0), j, null)));
		                	this.sumaImporteCampoAlc(alcsImplicadas, contaPlantillasDTO.getCampo(), j, plantilla);
		                }
	            	}
      
		            if (balance.compareTo(BigDecimal.ZERO) != 0 && !contaPlantillasDTOs.isEmpty()) {
		                this.guardarApunteDiferencia(balance.negate(), contaPlantillasDTOs.get(0), cobro);
		            }
      
	            } catch (Exception ex) {
		            // De haber algun error se para la ejecucion completa.
		            Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
		            throw new EjecucionPlantillaException();
	            }
        	}
  		}

  		return listaTmct0ContaPlantillasDTO;
  }

  /**
   * Obtiene el monto de balance de los apuntes generados.
   * 
   * @param apuntes
   * @return
   */
  private BigDecimal getBalance(List<Tmct0ContaApuntes> apuntes) {

    BigDecimal balance = BigDecimal.ZERO;
    if (apuntes != null && !apuntes.isEmpty()) {
      for (Tmct0ContaApuntes apunte : apuntes) {
        balance = balance.add(apunte.getImporte());
      }
    }
    return balance;

  }

  /**
   * Genera registros transaccionales para un conjunto de importes para liquidaciones totales.
   * 
   * @param plantilla plantilla
   * @param cobros cobros
   * @return contaPlantillasDTOs
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false,
                 propagation = Propagation.REQUIRES_NEW,
                 rollbackFor = { Exception.class, EjecucionPlantillaException.class
                 })
  public List<Tmct0ContaPlantillasDTO> ejecutarPlantillaLiquidacionesTotales(Tmct0ContaPlantillas plantilla,
                                                                             Object[] cobro) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    try {
          CobroDTO cobroDTO = new CobroDTO(cobro);

          // 2. Si se obtienen registros, por cada registro obtenido se debe ejecutar la query de las ordenes
          // (QUERY_ORDEN) para localizar la orden, el literal <DESC_REFERENCIA DE LA QUERY_COBRO> de la
          // query de las ordenes debe sustituirse por el valor DESC_REFERENCIA de cada registro obtenido
          // de la query de cobros de forma que:
          List<Object[]> ordenes = null;
          if (plantilla.getQuery_orden() != null) {
            if (plantilla.getQuery_orden() != null) {
              try {
                ordenes = this.ejecutaQueryOrden(plantilla, cobro, null);
              } catch (Exception ex) {
                // De haber algun error se para la ejecucion completa.
                Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
                throw new EjecucionPlantillaException();
              }
            }
          }
          if (ordenes != null && !ordenes.isEmpty()) {
            Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Obtenidas " + ordenes.size() + " ordenes para el cobro: " + cobroDTO.toString());
            try {
              contaPlantillasDTOs.addAll(this.procesarOrdenesLiquidacionesTotales(ordenes, plantilla, cobroDTO));
            } catch (Exception ex) {
              Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
              throw new EjecucionPlantillaException();
            }
          } else { // a. Si no se encuentra una orden relacionada:

            Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "No se encuentra orden relacionada. El registro del cobro se tiene que actualizar a procesado = 2. "
                                               + "Se debe dar de alta un descuadre. [Metodo][" + new Date() + "]");

            // i. El registro del cobro se tiene que actualizar a PROCESADO=2 en la tabla TMCT0_MOVIMIENTO_NORMA43
            // buscando por el campo ID_MOV.
//            this.tmct0Norma43MovimientoBo.pasarCobroAProcesadoCobroRetLiq(cobro.getIdMov());
            // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc referencia y procesado 2 porque generan
            // descuadre.
            this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobroDTO.getIdMov(), 2);

            // ii. Se debe dar de alta un descuadre (en el punto 2.3.4 se define como se crean los descuadres en
            // la tabla de descuadres) en la tabla de descuadres con el error NO SE HA ENCONTRADO ORDEN EN SIBBAC.
            try {
              this.tmct0Norma43DescuadreBo.crearDescuadre("NO SE HA ENCONTRADO ORDEN EN SIBBAC", plantilla, cobroDTO, null);
            } catch (Exception ex) {
              Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_DESCUADRES.getValue() + ": " + ex.getMessage());
              throw new EjecucionPlantillaException();
            }
          }
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo() != null ? plantilla.getCodigo() : null,
                                 ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + " : " + ex.getMessage());
      throw new EjecucionPlantillaException(ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue(), ex);
    }
    return contaPlantillasDTOs;
  }

  /**
   * Genera registros transaccionales para un conjunto de importes para liquidaciones parciales.
   * 
   * @param plantilla plantilla
   * @param cobros cobros
   * @return contaPlantillasDTOs
   * @throws EjecucionPlantillaException
   */
  @Transactional(readOnly = false,
                 propagation = Propagation.REQUIRES_NEW,
                 rollbackFor = { Exception.class, EjecucionPlantillaException.class
                 })
  public List<Tmct0ContaPlantillasDTO> ejecutarPlantillaLiquidacionesParciales(Tmct0ContaPlantillas plantilla, Object[] cobro) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
    try {

    	  CobroDTO cobroDTO = new CobroDTO(cobro);
          // 2. Si se obtienen registros, por cada registro obtenido se debe ejecutar la query de las ordenes
          // (QUERY_ORDEN) para localizar la orden, el literal <DESC_REFERENCIA DE LA QUERY_COBRO> de la
          // query de las ordenes debe sustituirse por el valor DESC_REFERENCIA de cada registro obtenido
          // de la query de cobros de forma que:
          List<Object[]> ordenes = null;
          if (plantilla.getQuery_orden() != null) {
            if (plantilla.getQuery_orden() != null) {
              try {
                ordenes = this.ejecutaQueryOrden(plantilla, cobro, null);
              } catch (Exception ex) {
                // De haber algun error se para la ejecucion completa.
                Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": "
                                                                  + ex.getMessage());
                throw new EjecucionPlantillaException();
              }
            }
          }

          if (ordenes != null && !ordenes.isEmpty()) {

            Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "Obtenidas " + ordenes.size() + " ordenes para el cobro: " + cobroDTO.toString());
            
            try {
            	
              contaPlantillasDTOs.addAll(this.procesarOrdenesLiquidacionesParciales(ordenes, plantilla, cobroDTO));
              
            } catch (Exception ex) {
              Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
              throw new EjecucionPlantillaException();
            }

          } else { // a. Si no se encuentra una orden relacionada:

            Loggers.logTraceGenericoAccounting(plantilla.getCodigo(), "No se encuentra orden relacionada. El registro del cobro se tiene que actualizar a procesado = 2. "
                                               + "Se debe dar de alta un descuadre." + " [Metodo][" + new Date() + "]");

            // i. El registro del cobro se tiene que actualizar a PROCESADO=1 en la tabla TMCT0_MOVIMIENTO_NORMA43
            // buscando por el campo ID_MOV.
            // this.tmct0Norma43MovimientoBo.pasarCobroAProcesadoCobroRetLiq(cobro.getIdMov());
            // MFG 18/04/2017 Se pasan todos los movimientos asociados a desc referencia y procesado 2 porque generan
            // descuadre.
            this.tmct0Norma43MovimientoBo.pasarCobroAProcesadobyIdMovimiento(cobroDTO.getIdMov(), 2);

            // ii. Se debe dar de alta un descuadre (en el punto 2.3.4 se define como se crean los descuadres en
            // la tabla de descuadres) en la tabla de descuadres con el error NO SE HA ENCONTRADO ORDEN EN SIBBAC.
            try {
            	
              this.tmct0Norma43DescuadreBo.crearDescuadre("NO SE HA ENCONTRADO ORDEN EN SIBBAC", plantilla, cobroDTO, null);
              
            } catch (Exception ex) {
              Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_DESCUADRES.getValue() + ": " + ex.getMessage());
              throw new EjecucionPlantillaException();
            }
          }
          
    } catch (Exception ex) {
      Loggers.logErrorAccounting(plantilla.getCodigo() != null ? plantilla.getCodigo() : null, ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + " : " + ex.getMessage());
      throw new EjecucionPlantillaException(ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue(), ex);
    }
    return contaPlantillasDTOs;
  }

  /**
   * Divide las plantillas que tengan mas de una cuenta, y en tal caso las marca como dividido=true.
   * 
   * @param plantilla Tmct0ContaPlantillas.
   * @param object Resultado de query orden.
   * @return Lista de Tmct0ContaPlantillasDTO.
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private List<Tmct0ContaPlantillasDTO> dividirPlantillasPorCuentasCobrosLiquidaciones(Tmct0ContaPlantillas plantilla,
                                                                                      Object[] object) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> plantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
    Tmct0ContaPlantillasDTO contaPlantillasDTO = null;

    // Se separa por la "," los campos que pueden traer mÃ¡s de un valor por plantilla.
    String[] cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",");
    String[] cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(),
                                                                                        ",");
    String[] auxsDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(), ",");
    String[] auxsHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(), ",");

    String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCampo(), ",");

    // En caso de que haya un error de inconsistencia de cuentas y auxiliares se arroja error.
    if (!AccountingHelper.isPlantillaCuentasAuxCamposConsistentes(cuentasDebe, cuentasHaber, auxsDebe, auxsHaber,
                                                                  campos)) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_DIVIDIR_PLANTILLAS.getValue() + " : "
                                     + ConstantesError.ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX.getValue());
      throw new EjecucionPlantillaException();
    }

    int iCuenta = 0;
    int iImporte = 3;
    int iAux = iImporte + cuentasDebe.length; // Indice de las cuenta auxiliares.

    // Map con los valores AUX sacados de la Query
    Map<String, String> auxMap = new HashMap<String, String>();
    for (int i = 0; i < cuentasDebe.length; i++) {
      try {
        if (!auxsDebe[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsDebe[i])) {
          if (auxMap.get(auxsDebe[i]) == null) {
            auxMap.put(auxsDebe[i], (String) object[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }

      try {
        if (!auxsHaber[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsHaber[i])) {
          if (auxMap.get(auxsHaber[i]) == null) {
            auxMap.put(auxsHaber[i], (String) object[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }
    }
    
    List<Object> paramsDetail = new ArrayList<Object>();
    for(int i=iAux; i<object.length;i++){
  	  paramsDetail.add(object[i]);
    }

    for (String cuentaDebe : cuentasDebe) {

      // SafeNull para el importe - ya que la query de orden puede devolver importe NULL
      if (object[iImporte] == null) {
        object[iImporte] = BigDecimal.ZERO;
      }

      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      contaPlantillasDTO.setActivo(plantilla.getActivo());
      contaPlantillasDTO.setAudit_date(plantilla.getAudit_date());
      contaPlantillasDTO.setCampo(StringHelper.quitarComillasDobles(campos[iCuenta]));
      contaPlantillasDTO.setCampo_estado(plantilla.getCampo_estado());
      contaPlantillasDTO.setCodigo(plantilla.getCodigo());
      contaPlantillasDTO.setCuenta_bancaria(plantilla.getCuenta_bancaria());
      contaPlantillasDTO.setCuenta_debe(StringHelper.quitarComillasDobles(cuentaDebe));
      contaPlantillasDTO.setCuenta_diferencias(StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
      contaPlantillasDTO.setCuenta_haber(StringHelper.quitarComillasDobles(cuentasHaber[iCuenta]));
      contaPlantillasDTO.setEstado_final(plantilla.getEstado_final());
      contaPlantillasDTO.setEstado_inicio(plantilla.getEstado_inicio());
      contaPlantillasDTO.setFichero(plantilla.getFichero());
      contaPlantillasDTO.setFichero_cobro(plantilla.getFichero_cobro());
      contaPlantillasDTO.setFichero_orden(plantilla.getFichero_orden());
      contaPlantillasDTO.setId(plantilla.getId());
      contaPlantillasDTO.setNum_comprobante(plantilla.getNum_comprobante());
      contaPlantillasDTO.setProceso(plantilla.getProceso());
      contaPlantillasDTO.setQuery_bloqueo(plantilla.getQuery_bloqueo());
      contaPlantillasDTO.setQuery_cobro(plantilla.getQuery_cobro());
      contaPlantillasDTO.setQuery_final(plantilla.getQuery_final());
      contaPlantillasDTO.setQuery_orden(plantilla.getQuery_orden());
      contaPlantillasDTO.setSubproceso(plantilla.getSubproceso());
      contaPlantillasDTO.setSubtipo(plantilla.getSubtipo());
      contaPlantillasDTO.setTipo(plantilla.getTipo());
      contaPlantillasDTO.setTipo_comprobante(plantilla.getTipo_comprobante());
      contaPlantillasDTO.setImporte((BigDecimal) object[iImporte]);
      contaPlantillasDTO.setFecha_comprobante((Date) object[0]);
      contaPlantillasDTO.setPeriodo_comprobante(((Integer) object[1]).toString());
      contaPlantillasDTO.setConcepto((String) object[2]);
      contaPlantillasDTO.setAux_debe(AccountingHelper.getAux(auxsDebe, auxMap, iCuenta));
      contaPlantillasDTO.setAux_haber(AccountingHelper.getAux(auxsHaber, auxMap, iCuenta));

      if (contaPlantillasDTO.getAux_debe() == null){
        contaPlantillasDTO.setAux_debe("");
      }
      if (contaPlantillasDTO.getAux_haber() == null){
        contaPlantillasDTO.setAux_haber("");
      }
      
      contaPlantillasDTO.setParamsDetail(paramsDetail);

      plantillasDTO.add(contaPlantillasDTO);

      iCuenta++;
      iImporte++;
    }

    return plantillasDTO;
  }
  
  /**
   *	Persistencia para Cobros - Liquidacion total en apuntes manuales - fichero tipo SIBBAC.
   *	@param ordenes
   *	@param plantilla 
   * 	@param cobro
   * 	@param listaAMExcelDTO
   * 	@param whereCobro
   * 	@param alcsImplicadas
   * 	@throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public void persistirCobrosApuntesManuales(
		  List<Object[]> ordenes, Tmct0ContaPlantillas plantilla, CobroDTO cobro, List<ApuntesManualesExcelDTO> listaAMExcelDTO, 
		  String whereCobro, String whereOrden) throws EjecucionPlantillaException {
	  for (Object[] importes : ordenes) {
		  List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = dividirPlantillasPorCuentasDevengosCobrosLiquidaciones(plantilla, importes, listaAMExcelDTO.get(0).getSibbacConcepto());
		  BigDecimal balance = BigDecimal.ZERO;

		  for (int j = 0; j < contaPlantillasDTOs.size(); j++) {
			  Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(j);
			  
			  //	              Modifica y ejecuta la query de bloqueo
			  String queryBloqueo = AccountingHelper.sustituirWhere(plantilla.getQuery_bloqueo()) + whereOrden;
			  List<AlcDTO> alcsImplicadas = this.obtenerALCsAM(queryBloqueo, plantilla.getCodigo());
			  
			  //						  Cambiar metodo
			  if (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO) != 0) {
				  List<List<String>> listImportDetalle = AccountingHelper.getListImportesTextos(plantilla.getQuery_apunte_detalle());

				  try {
					  // Genera apuntes y ejecuta query detalle para los apuntes de detalle
					  balance = balance.add(getBalance(this.generarApuntesContables(contaPlantillasDTO, cobro, plantilla, listImportDetalle.get(0), j, null)));
				  } catch (Exception ex) {
					  Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
					  throw new EjecucionPlantillaException();
				  }

				  // Desbloquea estado y actualiza importes 
				  this.sumaImporteCampoAlc(alcsImplicadas, contaPlantillasDTO.getCampo(), j, plantilla);
			  }
		  }
		  if (balance.compareTo(BigDecimal.ZERO) != 0 && !contaPlantillasDTOs.isEmpty()) {
			  this.guardarApunteDiferencia(balance.negate(), contaPlantillasDTOs.get(0), cobro);
		  }
	  }
	  this.tmct0Norma43MovimientoBo.pasarCobroAProcesadoAM(whereCobro, plantilla);
  }
  
  /**
   *	Persistencia para Liquidacion parcial en apuntes manuales - fichero tipo SIBBAC.
   *	@param ordenes
   *	@param plantilla 
   * 	@param cobro
   * 	@param listaAMExcelDTO
   * 	@param whereCobro
   * 	@param alcsImplicadas
   * 	@throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public void persistirLiquidacionesApuntesManuales(List<Object[]> ordenes, Tmct0ContaPlantillas plantilla,
      CobroDTO cobro, List<ApuntesManualesExcelDTO> listaAMExcelDTO, String whereCobro, String whereOrden)
      throws EjecucionPlantillaException {

    BigDecimal importeCobro = cobro.getImporte();
    BigDecimal importeCobroRestante = cobro.getImporte();

    for (int k = 0; k < ordenes.size(); k++) {

      Object[] importes = ordenes.get(k);

      BigDecimal balance = BigDecimal.ZERO;
      BigDecimal importeOrdenTotal = BigDecimal.ZERO;

      // Suma de todos los importes de un resultado de la queryOrden
      int numImportes = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",").length;
      for (int i = 0; i < numImportes; i++) {
        importeOrdenTotal = importeOrdenTotal.add((BigDecimal) importes[i + 3]);
      }
      List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = dividirPlantillasPorCuentasDevengosCobrosLiquidaciones(
          plantilla, importes, listaAMExcelDTO.get(0).getSibbacConcepto());

      BigDecimal diferencia = importeCobro.subtract(importeOrdenTotal);
      Boolean isImporteCobroMayorImporteOrden = (importeCobro.compareTo(importeOrdenTotal) > 0);
      Boolean isImporteCobroMenorImporteOrden = (importeCobro.compareTo(importeOrdenTotal) < 0);
      Boolean isDiferenciaMayorQueToleranciaPlantilla = (diferencia.compareTo(plantilla.getTolerancia()) > 0);
      Boolean isSinDiferencia = BigDecimal.ZERO.compareTo(diferencia) == 0;
      Boolean isDiferenciaMenorIgualQueToleranciaPlantilla = !isDiferenciaMayorQueToleranciaPlantilla;

      try {

        for (int j = 0; j < contaPlantillasDTOs.size(); j++) {
          Tmct0ContaPlantillasDTO contaPlantillasDTO = contaPlantillasDTOs.get(j);
          // ii. Se debe generar el apunte contable tal cual
          // define la plantilla.
          if (contaPlantillasDTO.getImporte().compareTo(BigDecimal.ZERO) != 0) {
            List<List<String>> listImportDetalle = AccountingHelper.getListImportesTextos(plantilla
                .getQuery_apunte_detalle());

            String queryBloqueo = AccountingHelper.sustituirWhere(plantilla.getQuery_bloqueo()) + whereOrden;
            List<AlcDTO> alcsImplicadas = this.obtenerALCsAM(queryBloqueo, plantilla.getCodigo());

            if (isSinDiferencia || isImporteCobroMenorImporteOrden) {
              if (importeCobroRestante.compareTo(BigDecimal.ZERO) == 1) {
                this.generarApuntesContablesLiqParcial(contaPlantillasDTO, cobro, plantilla, listImportDetalle.get(0),
                    j, importeCobroRestante);
                this.sumaImporteCampoAlcLiqParcial(alcsImplicadas, contaPlantillasDTO.getCampo(), j,
                    importeCobroRestante, plantilla.getEstado_final(), plantilla.getEstado_parcial(),
                    plantilla.getCampo_estado(), plantilla.getCodigo());
              }
            }
            else if (isImporteCobroMayorImporteOrden || isDiferenciaMenorIgualQueToleranciaPlantilla) {
              balance = balance.add(getBalance(this.generarApuntesContables(contaPlantillasDTO, cobro, plantilla,
                  listImportDetalle.get(0), j, null)));
              this.sumaImporteCampoAlc(alcsImplicadas, contaPlantillasDTO.getCampo(), j, plantilla); 
            }
          }
        }

        if (balance.compareTo(BigDecimal.ZERO) != 0 && !contaPlantillasDTOs.isEmpty()) {
          this.guardarApunteDiferencia(balance.negate(), contaPlantillasDTOs.get(0), cobro);
        }

      }
      catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": "
            + ex.getMessage());
        throw new EjecucionPlantillaException();
      }
    }

    this.tmct0Norma43MovimientoBo.pasarCobroAProcesadoAM(whereCobro, plantilla);
  }
  
  /**
   *	Persistencia para Devengos en apuntes manuales - fichero tipo SIBBAC.
   *	Se ejecutan tambien queries de bloqueo - orden - apunte detalle - desbloqueo
   *	@param plantilla 
   * 	@param whereOrden
   * 	@param listaAMExcelDTO
   * 	@param plantillasSinBalance
   * 	@param contaPlantillasDTOs
   * 	@throws EjecucionPlantillaException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
  public List<List<Tmct0ContaPlantillasDTO>> persistirDevengosApuntesManuales(
		  Tmct0ContaPlantillas plantilla, String whereOrden, List<ApuntesManualesExcelDTO> listaAMExcelDTO, 
		  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance, List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs) throws EjecucionPlantillaException {

	  String queryBloqueo = AccountingHelper.sustituirWhere(plantilla.getQuery_bloqueo()) + whereOrden;

  	  List<Object[]> alcImplicadas = this.contaPlantillasDaoImp.executeQuery(queryBloqueo, "", plantilla != null ? plantilla.getCodigo() : null);
      this.contaPlantillasDaoImp.executeQueryBloqueoByFechasDevengosAnulacion(alcImplicadas, plantilla.getEstado_bloqueado(), plantilla.getCampo_estado(), plantilla != null ? plantilla.getCodigo() : null);

	  List<Object[]> importes = null;
	  try{
		  String queryOrden = AccountingHelper.modificarConsultaQUERY_ORDEN(plantilla.getQuery_orden(), whereOrden);
		  importes = this.contaPlantillasDaoImp.executeQuery(queryOrden, plantilla.getCodigo());
	  } catch (Exception ex) {
		  Loggers.logErrorAccounting(plantilla.getCodigo(),ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
		  throw new EjecucionPlantillaException();
	  }

	  try {
		  String queryDetalle = AccountingHelper.sustituirWhere(plantilla.getQuery_apunte_detalle()) + whereOrden;
		  plantilla.setQuery_apunte_detalle(queryDetalle);

		  if (!CollectionUtils.isEmpty(importes)) {
			  for (Object[] object : importes) {
				  contaPlantillasDTOs = dividirPlantillasPorCuentasDevengosCobrosLiquidaciones(plantilla, object, listaAMExcelDTO.get(0).getSibbacConcepto());
				  contaPlantillasDTOs = this.generarApuntes(plantilla, contaPlantillasDTOs, SibbacEnums.TipoContaPlantillas.DEVENGO.getTipo(), Boolean.TRUE);
				  if (!contaPlantillasDTOs.isEmpty()) {
					  plantillasSinBalance.add(contaPlantillasDTOs);
				  }
			  }
		  }
	  } catch (Exception ex) {
		  Loggers.logErrorAccounting(plantilla.getCodigo(),ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": " + ex.getMessage());
		  throw new EjecucionPlantillaException();
	  }

	  try {
		  
		  this.contaPlantillasDaoImp.executeQueryBloqueoByFechasDevengosAnulacion(alcImplicadas, plantilla.getEstado_final(), plantilla.getCampo_estado(), plantilla != null ? plantilla.getCodigo() : null);
		  
	  } catch (Exception ex) {
		  Loggers.logErrorAccounting(plantilla.getCodigo(),
				  ConstantesError.ERROR_QUERY_DESBLOQUEO.getValue() + ": " + ex.getMessage());
		  throw new EjecucionPlantillaException();
	  }
	  
	  return plantillasSinBalance;
  }

  /**
   * Divide las plantillas que tengan mÃ¡s de una cuenta, y en tal caso las marca como dividido=true.
   * 
   * @param plantilla Tmct0ContaPlantillas.
   * @param object Resultado de query orden.
   * @return Lista de Tmct0ContaPlantillasDTO.
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  public List<Tmct0ContaPlantillasDTO> dividirPlantillasPorCuentasDevengosCobrosLiquidaciones(Tmct0ContaPlantillas plantilla,
                                                                                      Object[] object, String concepto) throws EjecucionPlantillaException {

    List<Tmct0ContaPlantillasDTO> plantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
    Tmct0ContaPlantillasDTO contaPlantillasDTO = null;

    // Se separa por la "," los campos que pueden traer mÃ¡s de un valor por plantilla.
    String[] cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",");
    String[] cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(),
                                                                                        ",");
    String[] auxsDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(), ",");
    String[] auxsHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(), ",");

    String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCampo(), ",");

    // En caso de que haya un error de inconsistencia de cuentas y auxiliares se arroja error.
    if (!AccountingHelper.isPlantillaCuentasAuxCamposConsistentes(cuentasDebe, cuentasHaber, auxsDebe, auxsHaber,
                                                                  campos)) {
      Loggers.logErrorAccounting(plantilla.getCodigo(),
                                 ConstantesError.ERROR_DIVIDIR_PLANTILLAS.getValue() + " : "
                                     + ConstantesError.ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX.getValue());
      throw new EjecucionPlantillaException();
    }

    int iCuenta = 0;
    int iImporte = 2;
    int iAux = iImporte + cuentasDebe.length; // Indice de las cuenta auxiliares.

    // Map con los valores AUX sacados de la Query
    Map<String, String> auxMap = new HashMap<String, String>();
    for (int i = 0; i < cuentasDebe.length; i++) {
      try {
        if (!auxsDebe[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsDebe[i])) {
          if (auxMap.get(auxsDebe[i]) == null) {
            auxMap.put(auxsDebe[i], (String) object[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }

      try {
        if (!auxsHaber[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsHaber[i])) {
          if (auxMap.get(auxsHaber[i]) == null) {
            auxMap.put(auxsHaber[i], (String) object[iAux]);
            iAux++;
          }
        }
      } catch (Exception e) {
      }
    }

    for (String cuentaDebe : cuentasDebe) {

      // SafeNull para el importe - ya que la query de orden puede devolver importe NULL
      if (object[iImporte] == null) {
        object[iImporte] = BigDecimal.ZERO;
      }
      
      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
      contaPlantillasDTO.setActivo(plantilla.getActivo());
      contaPlantillasDTO.setAudit_date(plantilla.getAudit_date());
      // Para devengos puede no existir el campo CAMPO en plantilla
      if (campos != null && campos.length > 0) {
          contaPlantillasDTO.setCampo(StringHelper.quitarComillasDobles(campos[iCuenta]));    	  
      }
      contaPlantillasDTO.setCampo_estado(plantilla.getCampo_estado());
      contaPlantillasDTO.setCodigo(plantilla.getCodigo());
      contaPlantillasDTO.setCuenta_bancaria(plantilla.getCuenta_bancaria());
      contaPlantillasDTO.setCuenta_debe(StringHelper.quitarComillasDobles(cuentaDebe));
      contaPlantillasDTO.setCuenta_diferencias(StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
      contaPlantillasDTO.setCuenta_haber(StringHelper.quitarComillasDobles(cuentasHaber[iCuenta]));
      contaPlantillasDTO.setEstado_final(plantilla.getEstado_final());
      contaPlantillasDTO.setEstado_inicio(plantilla.getEstado_inicio());
      contaPlantillasDTO.setFichero(plantilla.getFichero());
      contaPlantillasDTO.setFichero_cobro(plantilla.getFichero_cobro());
      contaPlantillasDTO.setFichero_orden(plantilla.getFichero_orden());
      contaPlantillasDTO.setId(plantilla.getId());
      contaPlantillasDTO.setNum_comprobante(plantilla.getNum_comprobante());
      contaPlantillasDTO.setProceso(plantilla.getProceso());
      contaPlantillasDTO.setQuery_bloqueo(plantilla.getQuery_bloqueo());
      contaPlantillasDTO.setQuery_cobro(plantilla.getQuery_cobro());
      contaPlantillasDTO.setQuery_final(plantilla.getQuery_final());
      contaPlantillasDTO.setQuery_orden(plantilla.getQuery_orden());
      contaPlantillasDTO.setSubproceso(plantilla.getSubproceso());
      contaPlantillasDTO.setSubtipo(plantilla.getSubtipo());
      contaPlantillasDTO.setTipo(plantilla.getTipo());
      contaPlantillasDTO.setTipo_comprobante(plantilla.getTipo_comprobante());
      contaPlantillasDTO.setImporte((BigDecimal) object[iImporte]);
      contaPlantillasDTO.setFecha_comprobante((Date) object[0]);
      contaPlantillasDTO.setPeriodo_comprobante(((Integer) object[1]).toString());
      contaPlantillasDTO.setConcepto(concepto);
      contaPlantillasDTO.setAux_debe(AccountingHelper.getAux(auxsDebe, auxMap, iCuenta));
      contaPlantillasDTO.setAux_haber(AccountingHelper.getAux(auxsHaber, auxMap, iCuenta));

      if (contaPlantillasDTO.getAux_debe() == null)
        contaPlantillasDTO.setAux_debe("");
      if (contaPlantillasDTO.getAux_haber() == null)
        contaPlantillasDTO.setAux_haber("");
      
      contaPlantillasDTO.setParamsDetail(new ArrayList<Object>());

      plantillasDTO.add(contaPlantillasDTO);

      iCuenta++;
      iImporte++;
    }

    return plantillasDTO;
  }
  
  /******************************** Liquidaciones - Fin *****************************************************/
}
