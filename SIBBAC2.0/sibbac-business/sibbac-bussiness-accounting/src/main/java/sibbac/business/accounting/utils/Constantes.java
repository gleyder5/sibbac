package sibbac.business.accounting.utils;

/**
 * The Class Constantes para accounting.
 */
public class Constantes {

	public static final String  APP_NAME="SBRMV";
	public static final String  PROCCESS_NAME="CONTABILIDAD_TLM";
	/** Nombre del Sheet para el fichero Excel de TLM. */
	public static final String CLAVE_SHEET_TAB_PARTIDAS_ABIERTAS = "SheetTabPartidas";
	
	/** Abreviatura de auxiliar. */
	public static final String AUX = "AUX";
  
	/** Clave Sentidos de la tabla TMCT0_SIBBAC_CONTANTES. */
	public static final String CLAVE_SENTIDOS = "comboSentido";
	
	/** Clave Antiguedades de la tabla TMCT0_SIBBAC_CONTANTES. */
	public static final String CLAVE_ANTIGUEDADES = "comboAntiguedad";
	
	/** Constantes para las columnas de la tabla TMCT0_CONTA_CABECERAS_EXCEL. **/
	public static final String CONTA_CAB_EXC_AUX_BANCARIO = "AUX_BANCARIO";
	public static final String CONTA_CAB_EXC_BOOKING_DATE = "BOOKING_DATE";
	public static final String CONTA_CAB_EXC_VALUE_DATE = "VALUE_DATE";
	public static final String CONTA_CAB_EXC_CURRENCY = "CURRENCY";
	public static final String CONTA_CAB_EXC_IMPORTE = "IMPORTE";
	public static final String CONTA_CAB_EXC_TIPO = "TIPO";
	public static final String CONTA_CAB_EXC_COMENTARIO = "COMENTARIO";
	public static final String CONTA_CAB_EXC_DEPARTAMENTO = "DEPARTAMENTO";
	public static final String CONTA_CAB_EXC_REFERENCIA2 = "REFERENCIA2";
	public static final String CONTA_CAB_EXC_NUM_DOCUMENTO = "NUM_DOCUMENTO";
	public static final String CONTA_CAB_EXC_NUM_DOCUMENTO2 = "NUM_DOCUMENTO2";
	public static final String CONTA_CAB_EXC_REFERENCIA = "REFERENCIA";
	public static final String CONTA_CAB_EXC_GIN = "GIN";
	public static final String CONTA_CAB_EXC_COMENTARIO_USUARIO = "COMENTARIO_USUARIO";
	public static final String CONTA_CAB_EXC_ANTIGUEDAD = "ANTIGUEDAD";
	
	/** Constantes para mensajes de tipo de movimiento. **/
	public static final String OUR_DEBIT = "our_debit";
	public static final String OUR_CREDIT = "our_credit";
	public static final String THEIR_DEBIT = "their_debit";
	public static final String THEIR_CREDIT = "their_credit";


	/** Constante para indicar en el log la funcionalidad que se está ejecutando **/
	public static final String FUNCIONALIDAD_CONCILIACION_BANCARIA = "CONCILIACIÓN BANCARIA";

	/** Longitud del campo DESC_REFERENCIA de la tabla TMCT0_NORMA43_MOVIMIENTO. */
	public static final Integer LONGITUD_DESC_REFERENCIA_TMCT0_NORMA43_MOVIMIENTO = 16;
	
	/** Representacion de tipos de plantilla para identificar su ejecucion. */
	public final static String DEVENGO = "DEVENGO";
	public final static String ANULACION = "ANULACION";
	public final static String COBRO_BARRIDOS = "COBRO BARRIDOS";
	public final static String APUNTE_MANUAL = "APUNTE MANUAL";
	public final static String COBRO = "COBRO";
	public final static String RETROCESION = "RETROCESION";
	public final static String PERDIDAS_GANANCIAS = "PyG";
	public final static String DEVENGO_FALLIDOS = "DEVENGO FALLIDOS";
	public final static String EMISION_FACTURA = "EMISION FACTURA";
	public final static String DEVENGO_FACTURAS = "DEVENGO FACTURAS";
	public final static String DOTACION = "DOTACION";
	public final static String DESDOTACION = "DESDOTACION";
	public final static String LIQUIDACION_TOTAL = "LIQUIDACION TOTAL";
	public final static String LIQUIDACION_PARCIAL = "LIQUIDACION PARCIAL";
	// private final static String LIQUIDACION_FALLIDOS = "LIQUIDACIÓN FALLIDOS";

}