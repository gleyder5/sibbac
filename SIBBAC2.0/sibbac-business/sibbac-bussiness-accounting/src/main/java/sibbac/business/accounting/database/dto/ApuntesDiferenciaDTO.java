package sibbac.business.accounting.database.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;

/**
 * 	Data Transfer Object para Apuntes Manuales. 
 * 	Contiene principalmente una diferencia entre apuntes
 * 	y una lista de estos
 */
public class ApuntesDiferenciaDTO {
	
	private List<Tmct0ContaApuntes> listaTmct0ContaApuntes;
	private BigDecimal diferencia;

	/**
	 *	Constructor sin argumentos. 
	 */
	public ApuntesDiferenciaDTO() {
		this.listaTmct0ContaApuntes = new ArrayList<Tmct0ContaApuntes>();
	}
	
	/**
	 *	Constructor con argumentos. 
	 */
	public ApuntesDiferenciaDTO(List<Tmct0ContaApuntes> listaTmct0ContaApuntes,
		BigDecimal diferencia) {
		super();
		this.listaTmct0ContaApuntes = listaTmct0ContaApuntes;
		this.diferencia = diferencia;
	}

	/**
	 *	@return listaTmct0ContaApuntes 
	 */
	public List<Tmct0ContaApuntes> getListaTmct0ContaApuntes() {
		return this.listaTmct0ContaApuntes;
	}

	/**
	 *	@param listaTmct0ContaApuntes 
	 */
	public void setListaTmct0ContaApuntes(
			List<Tmct0ContaApuntes> listaTmct0ContaApuntes) {
		this.listaTmct0ContaApuntes = listaTmct0ContaApuntes;
	}

	/**
	 *	@return diferencia 
	 */
	public BigDecimal getDiferencia() {
		return this.diferencia;
	}

	/**
	 *	@param diferencia 
	 */
	public void setDiferencia(BigDecimal diferencia) {
		this.diferencia = diferencia;
	}
	
}