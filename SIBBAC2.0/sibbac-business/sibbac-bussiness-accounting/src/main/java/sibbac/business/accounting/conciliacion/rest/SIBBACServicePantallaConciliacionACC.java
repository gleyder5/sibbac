package sibbac.business.accounting.conciliacion.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0PantallaConciliacionACCBo;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServicePantallaConciliacionACC implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServicePantallaConciliacionACC.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  private Tmct0PantallaConciliacionACCBo tmct0PantallaConciliacionACCBo;
  
  @Autowired
  private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;
  
  @Autowired
  private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;

  /**
   * The Enum ComandoConciliacionACC.
   */
  private enum ComandoConciliacionACC {
    
    GENERAR_APUNTE("generarApuntes"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos ConciliacionACC.
     *
     * @param command the command
     */
    private ComandoConciliacionACC(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandoConciliacionACC command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceControlContabilidad");

      switch (command) {
      
      case GENERAR_APUNTE:
        Map<String, Object> resultadoGenerarApunteExcel = generarApunteExcel(paramsObjects);
        Map<String, Object> resultadoManageApuntesBelowTolerance = manageApuntesBelowTolerance(paramsObjects);
        List<Map<String, Object>> results = new ArrayList();
        results.add(resultadoGenerarApunteExcel);
        results.add(resultadoManageApuntesBelowTolerance);
        result.setArrayData(results);

      default:
        result.setError("An action must be set. Valid actions are: " + command);
      }
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    //aparentemente al meter en un array las respuestas de los 2 métodos, se jode el objeto resultado o algo por el estilo... mirar a ver....
    LOG.debug("Salgo del servicio SIBBACServiceControlContabilidad");
    return result;
  }

  private Map<String, Object> generarApunteExcel(Map<String, Object> paramsObjects) {
    Map<String, Object> result = new HashMap<String, Object>();
    try {
      // Se valida que no haya otros usuarios cargando ficheros Excel TLM.
      Tmct0SibbacConstantes semafCargaExcelTLM = this.tmct0SibbacConstantesDao.findByClaveAndValor(
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave(),
          SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado());
      if (semafCargaExcelTLM != null) {
        result.put("status", "KOSemaforo");
        result.put("error", "Un nuevo excel está siendo cargado por otro usuario. Es posible que sus datos "
            + "estén desactualizados, por favor vuelva a realizar la consulta en unos segundos.");
        LOG.error("No se pueden generar apuntes ya que hay usuarios cargando ficheros TLM.");
        return result;
      }
      
      tmct0PantallaConciliacionACCBo.generarApunteACCExcel(paramsObjects);
      
      result.put("status", "OK");

      // Se desactiva el semaforo de carga de fichero Excel.
      this.tmct0SibbacConstantesBo.updateValorByClave(SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(),
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

    }
    catch (Exception e) {
      LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
      // Se desactiva el semaforo de carga de fichero Excel.
      this.tmct0SibbacConstantesBo.updateValorByClave(SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(),
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
      result.put("status", "KO");
      return result;
    }
    return result;
  }
  
  private Map<String, Object> manageApuntesBelowTolerance(Map<String, Object> paramsObjects) {
    Map<String, Object> result = new HashMap<String, Object>();
    try {
      // Se valida que no haya otros usuarios cargando ficheros Excel TLM.
      Tmct0SibbacConstantes semafCargaExcelTLM = this.tmct0SibbacConstantesDao.findByClaveAndValor(
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave(),
          SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado());
      if (semafCargaExcelTLM != null) {
        result.put("status", "KOSemaforo");
        result.put("error", "Otro usuario está realizando operaciones con apuntes que pueden estar por "
            + "encima del tolerance permitido. Es posible que sus datos estén desactualizados, por "
            + "favor vuelva a realizar la consulta en unos segundos.");
        LOG.error("No se pueden generar apuntes ya que otro usuario está realizando operaciones con apuntes que pueden estar por "
            + "encima del tolerance permitido");
        return result;
      }
      
      tmct0PantallaConciliacionACCBo.manageApuntesBelowTolerance(paramsObjects);
      
      result.put("status", "OK");

      // Se desactiva el semaforo de carga de fichero Excel.
      this.tmct0SibbacConstantesBo.updateValorByClave(SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(),
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

    }
    catch (Exception e) {
      LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
      // Se desactiva el semaforo de carga de fichero Excel.
      this.tmct0SibbacConstantesBo.updateValorByClave(SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(),
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
      result.put("status", "KO");
      return result;
    }
    return result;
  }


  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandoConciliacionACC getCommand(String command) {
    ComandoConciliacionACC[] commands = ComandoConciliacionACC.values();
    ComandoConciliacionACC result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandoConciliacionACC.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    return null;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

}
