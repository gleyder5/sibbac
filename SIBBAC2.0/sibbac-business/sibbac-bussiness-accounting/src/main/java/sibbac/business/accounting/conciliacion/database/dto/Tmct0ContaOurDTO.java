/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;


/**
 * @author lucio.vilar
 *
 */
public class Tmct0ContaOurDTO {

	
	/**
	 * origen
	 */
	private String nuorden;
	
	/**
	 * tipo movimiento
	 */
	private String nbooking;
	
	/**
	 * auxiliar
	 */
	private String nucnfclt;
	
	/**
	 * concepto
	 */
	private BigDecimal nucnfliq;
	
	/**
	 * Sentido
	 */
	private String sentido;
	
	/**
	 * Importe
	 */
	private BigDecimal importe;
	
	public Tmct0ContaOurDTO() {
		
	}
	
	public Tmct0ContaOurDTO(String orden, String booking, String cnfclt, BigDecimal cnfliq, String unSentido, BigDecimal unImporte) {
		this.nbooking = booking;
		this.nucnfclt = cnfclt;
		this.nucnfliq = cnfliq;
		this.nuorden = orden;
		this.importe = unImporte;
		this.sentido = unSentido;
	}

	/**
	 * @return the nuorden
	 */
	public String getNuorden() {
		return nuorden;
	}

	/**
	 * @param nuorden the nuorden to set
	 */
	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}

	/**
	 * @return the nbooking
	 */
	public String getNbooking() {
		return nbooking;
	}

	/**
	 * @param nbooking the nbooking to set
	 */
	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}

	/**
	 * @return the nucnfclt
	 */
	public String getNucnfclt() {
		return nucnfclt;
	}

	/**
	 * @param nucnfclt the nucnfclt to set
	 */
	public void setNucnfclt(String nucnfclt) {
		this.nucnfclt = nucnfclt;
	}

	/**
	 * @return the sentido
	 */
	public String getSentido() {
		return sentido;
	}

	/**
	 * @param sentido the sentido to set
	 */
	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the nucnfliq
	 */
	public BigDecimal getNucnfliq() {
		return nucnfliq;
	}

	/**
	 * @param nucnfliq the nucnfliq to set
	 */
	public void setNucnfliq(BigDecimal nucnfliq) {
		this.nucnfliq = nucnfliq;
	}

}
