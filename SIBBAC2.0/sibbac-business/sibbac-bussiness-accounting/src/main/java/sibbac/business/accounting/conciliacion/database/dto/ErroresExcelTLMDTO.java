package sibbac.business.accounting.conciliacion.database.dto;

/**
 *	DTO para representar errores del Excel de TLM. 
 */
public class ErroresExcelTLMDTO {

	private String nroColumna;
	private String nroFila;
	private String descripcionError;
	
	/**
	 * 	Constructor no-arg.
	 */
	public ErroresExcelTLMDTO() {
		super();
	}
	
	/**
	 *	Constructor con args.
	 *	@param nroColumna
	 *	@param nroFila
	 *	@param descripcionError 
	 */
	public ErroresExcelTLMDTO(
		String nroColumna, String nroFila, String descripcionError) {
		super();
		this.nroColumna = nroColumna;
		this.nroFila = nroFila;
		this.descripcionError = descripcionError;
	}

	public String getNroColumna() {
		return this.nroColumna;
	}

	public void setNroColumna(String nroColumna) {
		this.nroColumna = nroColumna;
	}

	public String getNroFila() {
		return this.nroFila;
	}

	public void setNroFila(String nroFila) {
		this.nroFila = nroFila;
	}

	public String getDescripcionError() {
		return this.descripcionError;
	}

	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}

}