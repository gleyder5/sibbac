package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dto.ContaPeriodosDTO;
import sibbac.business.accounting.database.dto.ContaPeriodosFiltroDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPeriodos;

@Service
public class Tmct0ContaPeriodosAssembler {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ApuntesAssembler.class);

	public ContaPeriodosFiltroDTO getContaPeriodosFiltroDTOByMapParamObject(Map<String, Object> paramObject) throws Exception{
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		LOG.info("INICIO - ASSEMBLER - getContaPeriodosFiltroDTOByMapParamObject");

		ContaPeriodosFiltroDTO periodoFiltroDTO = new ContaPeriodosFiltroDTO();
		try {
			if(null!= paramObject.get("anio") && !"".equals(paramObject.get("anio").toString())){
				periodoFiltroDTO.setAnio(Integer.valueOf(paramObject.get("anio").toString()));
			}
			if(null!= paramObject.get("periodo") && !"".equals(paramObject.get("periodo").toString())){
				periodoFiltroDTO.setPeriodo(Integer.valueOf(paramObject.get("periodo").toString()));
			}
			if(null!= paramObject.get("fhIniDesde") && !"".equals(paramObject.get("fhIniDesde").toString())){
				periodoFiltroDTO.setFhIniDesde(sdf.parse(paramObject.get("fhIniDesde").toString()));
			}
			if(null!= paramObject.get("fhIniHasta") && !"".equals(paramObject.get("fhIniHasta").toString())){
				periodoFiltroDTO.setFhIniHasta(sdf.parse(paramObject.get("fhIniHasta").toString()));
			}
			
		} catch (Exception e) {
			LOG.error("Error metodo getContaPeriodosFiltroDTOByMapParamObject -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FIN - ASSEMBLER - getContaPeriodosFiltroDTOByMapParamObject");

		return periodoFiltroDTO;
	}

	public List<ContaPeriodosDTO> getContaPeriodosDTOByTmct0ContaPeriodos(List<Tmct0ContaPeriodos> listPeriodos) {
		
		LOG.info("INICIO - ASSEMBLER - getContaPeriodosDTOByTmct0ContaPeriodos");
		
		ContaPeriodosDTO periodosDTO; 
		List<ContaPeriodosDTO> listPeridosDTO = new ArrayList<ContaPeriodosDTO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try {
			
			for(Tmct0ContaPeriodos periodos : listPeriodos){
				periodosDTO = new ContaPeriodosDTO();
				periodosDTO.setAnio(periodos.getAnio());
				periodosDTO.setPeriodo(periodos.getPeriodo());
				periodosDTO.setFhIniPeriodo(sdf.format(periodos.getFhIniPeriodo()));
				periodosDTO.setFhFinPeriodo(sdf.format(periodos.getFhFinPeriodo()));
				listPeridosDTO.add(periodosDTO);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getContaPeriodosDTOByTmct0ContaPeriodos -  " + e.getMessage(), e);
			throw (e);
		}
		
		LOG.info("FIN - ASSEMBLER - getContaPeriodosDTOByTmct0ContaPeriodos");

		return listPeridosDTO;
	}

}
