package sibbac.business.accounting.conciliacion.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPlantillasConcilia;

/**
 * Data access object para Conta Partidas pendientes.
 */
@Repository
public interface Tmct0ContaPlantillasConciliaDao extends JpaRepository<Tmct0ContaPlantillasConcilia, Integer> {
	
	@Query(value="SELECT DISTINCT(CODIGO), TOLERANCE FROM TMCT0_CONTA_PLANTILLAS_CONCILIA ORDER BY CODIGO ASC",nativeQuery=true)
	public List<Object> findAllDistinctCodigos();
	
	@Modifying @Query(value="UPDATE TMCT0_CONTA_PLANTILLAS_CONCILIA SET TOLERANCE = :tolerance WHERE CODIGO = :codigo",nativeQuery=true)
	public void modificarTolerance(@Param("tolerance") Integer tolerance,@Param("codigo") String codigo);
	
	@Query( "SELECT plantConcilia FROM Tmct0ContaPlantillasConcilia plantConcilia WHERE plantConcilia.auxiliar in :listAux" )
	public List<Tmct0ContaPlantillasConcilia> findByAux(@Param( "listAux" ) List<String> listAux);
	
	@Query( "SELECT plantConcilia FROM Tmct0ContaPlantillasConcilia plantConcilia WHERE plantConcilia.codigo = :codPlantilla" )
	public List<Tmct0ContaPlantillasConcilia> findByCodPlantilla(@Param( "codPlantilla" ) String codPlantilla);
}
