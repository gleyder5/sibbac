package sibbac.business.accounting.database.dto;

import java.util.Arrays;
import java.util.List;
import java.math.BigDecimal;

/**
 * Data Transfer Object para la gestión de Ordenes
 * 
 * @author Neoris
 *
 */
public class AlcDTO {

	private String nbooking = "";
	private String nuorden = "";
	private BigDecimal nucnfliq = BigDecimal.ZERO;
	private String nucnfclt = "";
	private List<BigDecimal> importes;

	/**
	 *	Constructor con solo los datos identificativos
	 *	para la orden.
	 *	@param nuorden nuorden
	 *	@param nbooking nbooking
	 *	@param nucnfclt nucnfclt
	 *	@param nucnfliq nucnfliq  
	 */
	public AlcDTO(
		String nuorden, String nbooking, String nucnfclt, BigDecimal nucnfliq, List<BigDecimal> importes) {
		this.nuorden = nuorden;
		this.nbooking = nbooking;
		this.nucnfclt = nucnfclt;
		this.nucnfliq = nucnfliq;
		this.importes = importes;
	}

	@Override
	public String toString() {
		return "Alc [nbooking=" + nbooking + ", nuorden=" + nuorden + ", nucnfliq=" + nucnfliq + ", nucnfclt="
				+ nucnfclt + "]";
	}

	public String getNbooking() {
		return nbooking;
	}

	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}

	public String getNuorden() {
		return nuorden;
	}

	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}

	public BigDecimal getNucnfliq() {
		return nucnfliq;
	}

	public void setNucnfliq(BigDecimal nucnfliq) {
		this.nucnfliq = nucnfliq;
	}

	public String getNucnfclt() {
		return nucnfclt;
	}

	public void setNucnfclt(String nucnfclt) {
		this.nucnfclt = nucnfclt;
	}


	public List<BigDecimal> getImportes() {
		return importes;
	}

	public void setImportes(List<BigDecimal> importes) {
		this.importes = importes;
	}

}
