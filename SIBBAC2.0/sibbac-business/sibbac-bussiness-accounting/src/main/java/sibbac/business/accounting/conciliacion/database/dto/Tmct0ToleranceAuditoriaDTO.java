package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigInteger;
import java.sql.Timestamp;

public class Tmct0ToleranceAuditoriaDTO {
	
	private BigInteger id;
	
	private String codPlantilla;
	
	private Integer tolerance;
	
	private Timestamp auditDate;
	
	private String auditUser;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ToleranceAuditoriaDTO() {
		super();
	}

	/**
	 * @param id
	 * @param codPlantilla
	 * @param tolerance
	 * @param auditDate
	 * @param auditUser
	 */
	public Tmct0ToleranceAuditoriaDTO(BigInteger id, String codPlantilla, Integer tolerance, 
			Timestamp auditDate, String auditUser) {
		this.id = id;
		this.codPlantilla = codPlantilla;
		this.tolerance = tolerance;
		this.auditDate = auditDate;
		this.auditUser=auditUser;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the codPlantilla
	 */
	public String getCodPlantilla() {
		return codPlantilla;
	}

	/**
	 * @param codPlantilla the codPlantilla to set
	 */
	public void setCodPlantilla(String codPlantilla) {
		this.codPlantilla = codPlantilla;
	}

	/**
	 * @return the tolerance
	 */
	public Integer getTolerance() {
		return tolerance;
	}

	/**
	 * @param tolerance the tolerance to set
	 */
	public void setTolerance(Integer tolerance) {
		this.tolerance = tolerance;
	}

	/**
	 * @return the auditDate
	 */
	public Timestamp getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(Timestamp auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

}
