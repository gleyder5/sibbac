package sibbac.business.accounting.conciliacion.database.dto;

import java.util.Date;

/**
 *	DTO para representar datos del nombre del Excel de TLM. 
 */
public class DatosNombreExcelTLMDTO {

	private Date fechaArchivo;
	private boolean esFicheroTipoExcel;
	
	/**
	 * 	Constructor no-arg.
	 */
	public DatosNombreExcelTLMDTO() {
		super();
	}

	public Date getFechaArchivo() {
		return this.fechaArchivo;
	}

	public void setFechaArchivo(Date fechaArchivo) {
		this.fechaArchivo = fechaArchivo;
	}

	public boolean isEsFicheroTipoExcel() {
		return this.esFicheroTipoExcel;
	}

	public void setEsFicheroTipoExcel(boolean esFicheroTipoExcel) {
		this.esFicheroTipoExcel = esFicheroTipoExcel;
	}

}