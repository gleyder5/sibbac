package sibbac.business.accounting.helper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sibbac.business.accounting.conciliacion.database.dto.DatosApuntesExcelDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaApunteDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaExcelConciliacionBancariaDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurApuntesDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaPartidasCasadasDTO;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.utils.Constantes;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SibbacEnums;

import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import static sibbac.business.accounting.utils.Constantes.THEIR_DEBIT;

/**
 * Contiene metodos de utilidad para la funcionalidad de accounting.
 */
public class AccountingHelper {
	
   protected static final Logger LOG = LoggerFactory.getLogger(AccountingHelper.class);
	
   private static final String FORMATO_MONTOS = "#,##0.00";

  /**
   * Realiza la suma para las cuentas del debe y el haber, guarda el resultado en 'balance'
   * 
   * @param contaPlantillasDTO contaPlantillasDTO

   * @return BigDecimal BigDecimal
   */
  public static BigDecimal realizarBalance(Tmct0ContaPlantillasDTO contaPlantillasDTO) {

    BigDecimal balance = NumberUtils.createBigDecimal("0");
    if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
      balance = balance.add(contaPlantillasDTO.getImporte());
    }

    if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())
        && StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
      balance = balance.add(contaPlantillasDTO.getImporte());
    } else {
      if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())
          && !StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())) {
        // ACTUALIZO EL BALANCE NEGATIVO
        balance = balance.add(contaPlantillasDTO.getImporte().negate());
      }
    }
    return balance;
  }

  /**
   * Obtiene la cuenta aux del array de la plantilla o del objeto de query orden, si no retorna ''
   * 
   * @param auxs auxs
   * @param auxMap object
   * @param iCuenta iCuenta
   * @param iCuenta iAux
   * @return String String
   */
  public static String getAux(String[] auxs, Map<String, String> auxMap, int iCuenta) {
    String aux = "";
    try {
      // Si no se trae ningun aux, se devuelve vacío
      if (StringUtils.isEmpty(auxs[iCuenta])) {
        aux = "";
      } else {
        // si el valor de aux empieza con comillas, se usa el valor tal cual esta
        if (auxs[iCuenta].substring(0, 1).equals("\"")) {
          aux = auxs[iCuenta];
        }
        // si el valor no empieza con comillas, sale de la query, y el contador iAux se debe incrementar
        else {
          aux = auxMap.get(auxs[iCuenta]);
        }
      }
    } catch (Exception e) {
      aux = "";
    }
    return aux;
  }

  /**
   * Realiza la suma para las cuentas del debe y el haber, guarda el resultado en 'balance'
   * 
   * @param contaPlantillasDTO contaPlantillasDTO
   * @param balance balance
   * @param importeDebe importeDebe
   * @param importeHaber importeHaber
   * @return BigDecimal BigDecimal
   */
  public static BigDecimal realizarBalance(Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                           BigDecimal balance,
                                           BigDecimal importeDebe,
                                           BigDecimal importeHaber) {
    if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_debe())) {
      importeDebe.add(contaPlantillasDTO.getImporte());
      balance.add(contaPlantillasDTO.getImporte());
    }
    if (!StringUtils.isEmpty(contaPlantillasDTO.getCuenta_haber())) {
      importeHaber.add(contaPlantillasDTO.getImporte().negate());
      balance.add(contaPlantillasDTO.getImporte().negate());
    }
    return balance;
  }

  /**
   * Se devuelve una lista de listas de cadena de caracteres con ciertos importes a partir de una query.
   * 
   * @param query: query a tomar los importes
   * @return lista de listas de cadena de caracteres
   */
  public static List<List<String>> getListImportesTextos(String query) {

    List<List<String>> lista = new ArrayList<>();
    int inicio = 0;
    int fin = 0;
    int contParentesis = 0;

    for (int i = 0; i < query.length() - "union".length(); i++) {
      if ((query.substring(i, i + "union".length()).toLowerCase().equals("union") || 
    		  (query.substring(i,i + "select".length()).toLowerCase().equals("select") && inicio == 0))&& 
    		  contParentesis == 0) {

        List<String> importes = new ArrayList<>();

        while (!query.substring(i, i + "select".length()).toLowerCase().equals("select")) {
          i++;
        }
        inicio = i + "select".length();
        i = inicio;

        while (!query.substring(i, i + 1).equals("?")) {
          if (query.substring(i, i + "importe".length()).toLowerCase().equals("importe")) {
        	  fin = i + "importe".length();
            importes.add(query.substring(inicio, fin) + ",");
            while (!query.substring(fin, fin + 1).equals(",")) {
              fin++;
            }
            inicio = fin + 1;
            i = inicio - 1;
          }
          i++;
        }

        lista.add(importes);
      }
      if (query.substring(i, i + 1).equals("(")) {
        contParentesis++;
      }
      if (query.substring(i, i + 1).equals(")")) {
        contParentesis--;
      }
    }
    return lista;
  }

  /**
   * Retorna true si la cabecera de la plantilla es DEVENGO o ANULACION para apuntes manuales.
   * 
   * @param tipoPlantilla tipoPlantilla
   * @return boolean boolean
   */
  public static boolean isCabeceraDevengoAnulacionAM(String tipoPlantilla) {
    return tipoPlantilla != null
           && (tipoPlantilla.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.DEVENGO.getTipo()) || tipoPlantilla.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.ANULACION.getTipo()));
  }
  
  
  public static String sustituirWhere (String consulta) {
		
	  String consultaEditada = null;
	  String consultaUnion = null;
	  String from = "FROM";
	  String fromConst = " FROM bsnbpsql.TMCT0ALC alc ";
	  String union = "UNION";

	  if (null != consulta && StringUtils.containsIgnoreCase(consulta, union)) {
	   int indiceUnion = StringUtils.indexOfIgnoreCase(consulta, union);
	   consultaUnion = consulta.substring(0, indiceUnion);
	  } else {
		  consultaUnion = consulta;
	  }
	  if (null != consultaUnion && StringUtils.containsIgnoreCase(consultaUnion, from)) {
	   int indiceWhere = StringUtils.indexOfIgnoreCase(consultaUnion, from);
	   consultaEditada = consultaUnion.substring(0, indiceWhere);
	   consultaEditada = consultaEditada.concat(fromConst);
	   
	  }
	  
	  return consultaEditada;
	}
  
  public static String modificarConsultaQUERY_ORDEN(String consulta, String condicion) {
	  
		String consultaEditada = null;
		String select = null;
		String union = "UNION";

		if (null != consulta && StringUtils.containsIgnoreCase(consulta, union)) {
			int indiceUnion = StringUtils.indexOfIgnoreCase(consulta, union);
			consultaEditada = consulta.substring(0, indiceUnion);
		} else {
			consultaEditada = consulta;
		}
		
		int fromIndex = StringUtils.indexOfIgnoreCase(consultaEditada, "FROM");
		String colsSelect = consultaEditada.substring(6, fromIndex);
		String[] cols = colsSelect.split(",");
		String columna = "";
		String columnaTotalizadora = "";
		
		boolean continua = false;
		boolean continuaFecha = false;
		for (int i = cols.length -1; i >= 0; i--) {
			if(StringUtils.containsIgnoreCase(cols[i], "IMPORTE") || continua) {
				columna = cols[i] + columna;
				
				int cierra = StringUtils.countMatches(columna, ")");
				int abre = StringUtils.countMatches(columna, "(");
				
				if (abre != cierra) {
					continua = true;
					columna = ", " + columna;
				} else {
					continua = false;
					if (!StringUtils.startsWithIgnoreCase(StringUtils.trim(columna), "SUM") && !StringUtils.startsWithIgnoreCase(StringUtils.trim(columna), "0-SUM")) {
						int indiceImporte = StringUtils.lastIndexOfIgnoreCase(columna, "IMPORTE");
						columna = " SUM(" + StringUtils.substring(columna,0, indiceImporte) + ") " + StringUtils.substring(columna, indiceImporte);
					}
					columnaTotalizadora = ", " + columna + columnaTotalizadora;
					columna = "";
				}
			} else if(StringUtils.containsIgnoreCase(cols[i], "FECHA") || StringUtils.containsIgnoreCase(cols[i], "PERIODO") || continuaFecha) {
				columna = cols[i];
				
				int cierra = StringUtils.countMatches(columna, ")");
				int abre = StringUtils.countMatches(columna, "(");

				if (abre != cierra) {
					continuaFecha = true;
					columna = ", " + columna;
				} else {
					continuaFecha = false;
					columnaTotalizadora = ", " + columna + columnaTotalizadora;
					columna = "";
				}

				columnaTotalizadora = columna + columnaTotalizadora;
				columna = "";
			} 
		}
		
		columnaTotalizadora = StringUtils.substring(columnaTotalizadora, 1);
		select = "SELECT " + columnaTotalizadora + " ";

		int whereIndex = StringUtils.indexOfIgnoreCase(consultaEditada, "bsnbpsql.TMCT0ALC alc");
		String from = "";
		if (whereIndex != -1) {
			from = " FROM bsnbpsql.TMCT0ALC alc ";
		}
		
		consultaEditada = select + from + condicion;
		
		return consultaEditada;
	  
  }
  
  public static String modificarConsultaQUERY_COBRO(String consulta, String variableWhere) {
		String consultaEditada = null;
		String select = null;
		String union = "UNION";
		String where = "WHERE";

		if (null != consulta && StringUtils.containsIgnoreCase(consulta, union)) {
			int indiceUnion = StringUtils.indexOfIgnoreCase(consulta, union);
			consultaEditada = consulta.substring(0, indiceUnion);
		} else {
			consultaEditada = consulta;
		}
		
		int fromIndex = StringUtils.indexOfIgnoreCase(consultaEditada, "FROM");
		String colsSelect = consultaEditada.substring(6, fromIndex);
		String[] cols = colsSelect.split(",");
		String columna = "";
		boolean continua = false;
		for (int i = cols.length -1; i >= 0; i--) {
			if(StringUtils.containsIgnoreCase(cols[i], "IMPORTE") || continua) {
				columna = cols[i] + columna;
				
				int cierra = StringUtils.countMatches(columna, ")");
				int abre = StringUtils.countMatches(columna, "(");
				
				if (abre != cierra) {
					continua = true;
					columna = ", " + columna;
				} else {
					continua = false;
					if (!StringUtils.startsWithIgnoreCase(StringUtils.trim(columna), "SUM")) {
						int indiceImporte = StringUtils.lastIndexOfIgnoreCase(columna, "IMPORTE");
						
						columna = ", SUM(" + StringUtils.substring(columna,0, indiceImporte) + ") " + StringUtils.substring(columna, indiceImporte);
					}
				}
			}
		}
		
		if (StringUtils.startsWithIgnoreCase(columna, ",")) {
			columna = StringUtils.substring(columna, 1);
		}
		select = "SELECT " + columna + " ";

		int whereIndex = StringUtils.indexOfIgnoreCase(consultaEditada, where);
		String from = StringUtils.substring(consultaEditada, fromIndex, whereIndex);
		
		consultaEditada = select + from + variableWhere;
		
		return consultaEditada;
  }
  
  public static String modificarConsultaQUERY_BLOQUEO(String consulta, String variableWhere) {
		
	  return "";
  }
  
  
  /**
   *  	Devuelve true en caso de que haya la misma cantidad de cuentas 
   *  	y auxiliares al debe y haber de una plantilla. 
   * 	@param cuentasDebe
   * 	@param cuentasHaber
   * 	@param auxsDebe
   * 	@param auxsHaber
   * 	@return boolean 
   */
  public static boolean isPlantillaCuentasAuxConsistentes(
		String[] cuentasDebe, String[] cuentasHaber, String[] auxsDebe, String[] auxsHaber) {
	  
	  int cantCuentasDebe = cuentasDebe.length == 0?1:cuentasDebe.length;
	  int cantCuentasHaber = cuentasHaber.length == 0?1:cuentasHaber.length;
	  int cantAuxsDebe = auxsDebe.length == 0?1:auxsDebe.length;
	  int cantAuxsHaber = auxsHaber.length == 0?1:auxsHaber.length;
	  
	  Boolean existeAlMenosUnaCuentaAlDebe = !StringHelper.isArrayVacio(cuentasDebe);
	  Boolean existeAlMenosUnaCuentaAlHaber = !StringHelper.isArrayVacio(cuentasHaber);

	  Boolean cantidadCuentasConsistente = (cantCuentasDebe == cantCuentasHaber) 
		&& (cantCuentasHaber == cantAuxsDebe) 
		&& (cantAuxsDebe == cantAuxsHaber);
	
	  return cantidadCuentasConsistente && existeAlMenosUnaCuentaAlDebe && existeAlMenosUnaCuentaAlHaber;
			  
  }
  
  /**
   *  	Devuelve true en caso de que haya la misma cantidad de cuentas 
   *  	y auxiliares al debe y haber de una plantilla. 
   * 	@param cuentasDebe
   * 	@param cuentasHaber
   * 	@param auxsDebe
   * 	@param auxsHaber
   * 	@return boolean 
   */
  public static boolean isPlantillaCuentasAuxCamposConsistentes(
		String[] cuentasDebe, String[] cuentasHaber, String[] auxsDebe, String[] auxsHaber, String[] campos) {
	  
	  int cantCuentasDebe = cuentasDebe.length == 0?1:cuentasDebe.length;
	  int cantCuentasHaber = cuentasHaber.length == 0?1:cuentasHaber.length;
	  int cantAuxsDebe = auxsDebe.length == 0?1:auxsDebe.length;
	  int cantAuxsHaber = auxsHaber.length == 0?1:auxsHaber.length;
	  int cantCampos = campos.length == 0?1:campos.length;
	  
	  Boolean existeAlMenosUnaCuentaAlDebe = !StringHelper.isArrayVacio(cuentasDebe);
	  Boolean existeAlMenosUnaCuentaAlHaber = !StringHelper.isArrayVacio(cuentasHaber);

	  Boolean cantidadCuentasConsistente = (cantCuentasDebe == cantCuentasHaber) 
		&& (cantCuentasHaber == cantAuxsDebe) 
		&& (cantAuxsDebe == cantAuxsHaber)
		&& (cantCuentasDebe == cantCampos);
	  
	  return cantidadCuentasConsistente && existeAlMenosUnaCuentaAlDebe && existeAlMenosUnaCuentaAlHaber;
	  
  }
  
  /**
   *	Este metodo toma una query de orden y le anexa las condiciones
   *	necesarias en las clausulas WHERE para procesar solo esa ALC.
   *	Usado en Apuntes Manuales.
   *	@param query query de orden
   *	@param info condicional en where para identificar ALC
   *	@return String query de orden modificada para cada ALC 
   */
  public static String addAlcsToQuery(String query, String info){
      String result = "";
      int contParentesis = 0;
      int inicio = 0;
      
      for (int i = 0; i < query.length()-"where".length(); i++) {
          if(query.substring(i, i+"where".length()).toLowerCase().equals("where") && contParentesis == 0){
              boolean ban=true;
              while(ban) {
                    i++;
                  try {
                  if(query.substring(i, i+";".length()).toLowerCase().equals(";")){ban=false;}
                  else if(query.substring(i, i+"union".length()).toLowerCase().equals("union")){ban=false;}
                  else if(query.substring(i, i+"group by".length()).toLowerCase().equals("group by")){ban=false;}
                  } catch (Exception e) {
                  }
              }
              result=result+query.substring(inicio, i);
              result=result+" "+info.trim()+" ";
              
              inicio=i;
          }
           if(query.substring(i, i+1).equals("(")){contParentesis++;}
           if(query.substring(i, i+1).equals(")")){contParentesis--;}
      }
      result=result+query.substring(inicio, query.length());  
      return result;
  }
  
  /**
   *	Cobros Barridos - Modificacion de query de desbloqueo para ser procesada
   *	por intervalos de registros
   *	@param query: query de desbloqueo a procesar
   *	@return StringBuffer: query modificada con anexo operador BETWEEN		 
   */
  public static StringBuffer getQueryDesbloqueoCBByRango(String query) {
	  StringBuffer queryByRango = new StringBuffer();
	  
	  if (StringUtils.isNotBlank(query)) {
		  int indiceFrom = query.toUpperCase().indexOf("FROM");
		  if (indiceFrom > -1) {
			  query = query.substring(indiceFrom, query.length() - 1);
		  }
		  
		  // Se anexan campos para uso de operador BETWEEN
		  queryByRango
		  	.append(
				  " SELECT alcin.NUORDEN, alcin.NBOOKING, alcin.NUCNFCLT, alcin.NUCNFLIQ FROM ("
				  + "SELECT alc.NBOOKING, alc.NUORDEN, alc.NUCNFLIQ, alc.NUCNFCLT, "
				  + "ROW_NUMBER() OVER(ORDER BY alc.NBOOKING, alc.NUORDEN, alc.NUCNFLIQ, alc.NUCNFCLT) AS rownumber ")
			.append(query)
			.append(" ) ) AS alcin WHERE rownumber BETWEEN ?3 AND ?4)");		  
	  }
	  return queryByRango; 
  }
  
  /**
   *	Devuelve un valor acorde para el numero de intervalos de ejecucion
   *	con que se correria una query.
   *	@param nroIntervaloEjecuciones
   *	@return Integer	 
   */
  public static Integer getNroIntervalosEjecucion(String nroIntervaloEjecuciones) {
	  if (org.apache.commons.lang.StringUtils.isBlank(nroIntervaloEjecuciones) 
			  || !StringUtils.isNumeric(nroIntervaloEjecuciones)) {
		  return 0;
	  } else {
		  return StringUtils.isNumeric(nroIntervaloEjecuciones) ? Integer.valueOf(nroIntervaloEjecuciones) : 0;
	  }  
  }
  
  /**
   *	Factory - Se devuelve un objeto de tipo apunte contable con propiedades comunes inicializadas.
   *	@return Tmct0ContaApuntes: instancia inicializada  
   */
  public static Tmct0ContaApuntes getInitializedTmct0ContaApuntes() {
	  Tmct0ContaApuntes tmct0ContaApuntes = new Tmct0ContaApuntes();
	  tmct0ContaApuntes.setTipo_transacc('2');
	  tmct0ContaApuntes.setCod_comp("31");
	  tmct0ContaApuntes.setGenerado_fichero(0);
	  tmct0ContaApuntes.setAudit_date(new Date());
	  tmct0ContaApuntes.setAudit_user("SIBBAC");
	  return tmct0ContaApuntes;
  }
  
  /**
   *	Factory - Se devuelve un objeto de tipo descuadre con propiedades comunes inicializadas.
   *	@return Norma43Descuadres: instancia inicializada   
   */
  public static Norma43Descuadres getInitializedNorma43Descuadres() {
	  Norma43Descuadres descuadre = new Norma43Descuadres();
	  // 26. AUDIT_DATE â€“ Timestamp de la fecha del momento
	  descuadre.setAuditDate(new Date());
	  // 2.3.4 GeneraciÃ³n de descuadres
	  // Para la generaciÃ³n de descuadres se debe crear el correspondiente
	  // registro en la tabla TMCT0_NORMA43_DESCUADRES de la siguiente forma:
	  // 27. AUDIT_USER â€“ Usuario â€˜SIBBACâ€™
	  descuadre.setAuditUser("SIBBAC");
	  // 38. PROCESADO â€“ 0
	  descuadre.setProcesado(false);
	  return descuadre; 	
  }
  
  /***************************** INICIO - Metodos para funcionalidad Validaciones de plantilla. *****************************/
  
  /**
	 * Se comprueba si una cadena tiene sólo uno de los literales indicados
	 * 
	 * @param cadena: cadena en la que se buscarán los literales
	 * @param literales: literales a buscar en la cadena
	 */
	public static boolean haySoloUnLiteral(String cadena, List<String> literales) {
		int cont = 0;
		try {
			for (String literal : literales) {
				if (cadena.contains(literal)) {
					cont++;
				}
			}
		} catch (Exception e) {
			LOG.error("Error método haySoloUnLiteral: " + e.getMessage());
		}
		if (cont==1) {
			return true;
		}
		return false;
	}
	
	/**
	 * Se comprueba si una cadena tiene alguno de los literales indicados
	 * 
	 * @param cadena: cadena en la que se buscarán los literales
	 * @param literales: literales a buscar en la cadena
	 */
	public static boolean hayUnLiteral(String cadena, List<String> literales) {
		try {
			for (String literal : literales) {
				if (cadena.contains(literal.trim())) {
					return true;
				}
			}
		} catch (Exception e) {
			LOG.error("Error método hayUnLiteral: " + e.getMessage());
		}
		return false;
	}
  
	/**
	 * Se comprueba si una cadena tiene los literales indicados
	 * 
	 * @param codigoPlantilla: codigo de la plantilla
	 * @param cadena: cadena en la que se buscarán los literales
	 * @param literales: literales a buscar en la cadena
	 * @param error: valor a mostrar en caso de error
	 */
	public static void validarLiterales(String codigoPlantilla, String cadena, List<String> literales, String error) {
		try {
			for (String literal : literales) {
				if (!cadena.contains(literal)) {
					Loggers.logErrorValidationAccounting(codigoPlantilla, error+literal);
				}
			}
		} catch (Exception e) {
			LOG.error("Error método validarLiterales: " + e.getMessage());
		}
	}
	
	/**
	 * Se comprueba si la QUERY cumple con el orden definido
	 * 
	 * @param elementosSelect: orden de los elementos
	 * @param elementosSelectQuery: orden en la query de los elementos
	 * @param cont: número de campos a validar
	 * @param error: valor a mostrar en caso de error
	 */
	public static void validarOrden(String codigoPlantilla, List<String> elementosSelect,List<String> elementosSelectQuery, int cont, String error) {
		List<String> listaAux = new ArrayList<String>();
		
		// Logica safe para evitar una excepcion por exceso de indice.
		if (CollectionUtils.isNotEmpty(elementosSelectQuery) && cont > elementosSelectQuery.size()) {
			cont = elementosSelectQuery.size();
		} else if (CollectionUtils.isNotEmpty(elementosSelect) && cont > elementosSelect.size()) {
			cont = elementosSelect.size();
		}
		
		try {
			for (int i=0; i<cont; i++) {
				if (!elementosSelectQuery.get(i).trim().toUpperCase().contains(elementosSelect.get(i).trim().toUpperCase())) {
					listaAux.add(elementosSelectQuery.get(i).trim());
					
				}
			}
			if (listaAux.size()>0) {
				Loggers.logErrorValidationAccounting(codigoPlantilla, error);
			}
		} catch (Exception e) {
			LOG.error("Error método validarOrden: " + e.getMessage());
		}
	}
	
	public static int contarCaracteres (String cadena, String caracter) {
		int count = 0;
		try {
			count = StringUtils.countMatches(cadena.toUpperCase(), caracter.toUpperCase());
		} catch (Exception e) {
			LOG.error("Error método contarCaracteres: " + e.getMessage());
		}
		return count;
	}
	
	/**
	 * Indica si la cadena pasada por parámetro está entrecomillada o no.
	 * 
	 * @param cadena: cadena a validar
	 * @return boolean
	 */
	public static boolean isEntrecomillado (String cadena) {
		try {
			if (cadena.substring(0, 1).toString().equals("\"") && cadena.substring(cadena.length()-1, cadena.length()).toString().equals("\"")) {
				return true;
			}
		} catch (Exception e) {
			LOG.error("Error método isEntrecomillado: " + e.getMessage());
		}
		return false;
	}
  
  
  /***************************** FIN - Metodos para funcionalidad Validaciones de plantilla. *****************************/

	/**
	 * 	Se obtiene auxiliar bancario numerico a partir del campo account de 
	 * 	la cabecera del Excel.
	 * 	Ejemplo: 
	 * 	- Para un campo account: SVRMV - AUX 84 CTA 0038-5777-0016016609 - EUR
	 * 	- Lo que se devuelve es: 84 
	 * 							 456	
	 * 	@param account
	 * 	@return String con el auxiliar bancario		
	 */
	public static String getAuxiliarBancarioFromAccount(String account) {
		if (StringUtils.isNotBlank(account)) {
			int posicion = account.indexOf(Constantes.AUX);
			if (posicion != -1) {
				return account.substring(posicion + 4, posicion + 6);
			}
		}
		return null;
	}
	
	public static XSSFWorkbook generarExcelPartidasCasadasYOur(List<Tmct0ContaPartidasCasadasDTO> listaTmct0ContaConciliacionAuditDTO, List<Tmct0ContaOurDTO> listaTmct0ContaOurDTO) {
	    XSSFWorkbook workbook = new XSSFWorkbook();
	    // Estilo de la cabecera
	    XSSFCellStyle xssfCellStyleCabecera = workbook.createCellStyle();
	    
	    DataFormat format = workbook.createDataFormat();
	    XSSFCellStyle xssfCellStyleImportes = workbook.createCellStyle();


	    xssfCellStyleCabecera.setFillForegroundColor(new XSSFColor(Color.RED));
	    xssfCellStyleCabecera.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);

	    // Crear la fuente de la cabecera
	    XSSFFont xssfFont = workbook.createFont();
	    xssfFont.setFontHeightInPoints((short) 8);
	    xssfFont.setFontName("ARIAL");
	    xssfFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	    xssfFont.setColor(new XSSFColor(Color.WHITE));
	    xssfCellStyleCabecera.setFont(xssfFont);

	    xssfCellStyleCabecera.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	    xssfCellStyleCabecera.setBorderRight(XSSFCellStyle.BORDER_THIN);
	    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());

	    xssfCellStyleCabecera.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    xssfCellStyleCabecera.setVerticalAlignment(XSSFCellStyle.ALIGN_FILL);

	    if (null != listaTmct0ContaConciliacionAuditDTO && !listaTmct0ContaConciliacionAuditDTO.isEmpty()) {
		    // Hoja con el listado de TLM
		    XSSFSheet registros = workbook.createSheet("PARTIDAS CASADAS -TLM");
		    registros.setDefaultColumnWidth(25);
		    int rowMin = 0;
	
		    // Cabecera
		    XSSFRow row = registros.createRow(rowMin++);
		    row.setHeight((short) 600);
		    XSSFCell cell = null;
			
		    cell = row.createCell(0);
		    cell.setCellValue("REFERENCIA");
		    cell.setCellStyle(xssfCellStyleCabecera);
		    
		    cell = row.createCell(1);
		    cell.setCellValue("GIN");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(2);
		    cell.setCellValue("TIPO DE MOVIMIENTO");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(3);
		    cell.setCellValue("AUXILIAR BANCARIO");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(4);
		    cell.setCellValue("CONCEPTO MOVIMIENTO");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(5);
		    cell.setCellValue("IMPORTE");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    for (Tmct0ContaPartidasCasadasDTO partidasCasadasDTO : listaTmct0ContaConciliacionAuditDTO) {
		        row = registros.createRow(rowMin++);
		        
		        cell = row.createCell(0);
		        cell.setCellValue(partidasCasadasDTO.getReferencia());
		        
		        cell = row.createCell(1);
		        cell.setCellValue(partidasCasadasDTO.getGin().toString());
		        
		        cell = row.createCell(2);
		        cell.setCellValue(partidasCasadasDTO.getTipoMovimiento());
		        
		        cell = row.createCell(3);
		        cell.setCellValue(partidasCasadasDTO.getAuxiliar());
	
		        cell = row.createCell(4);
		        cell.setCellValue(partidasCasadasDTO.getConcepto());
	
		        cell = row.createCell(5);
		        xssfCellStyleImportes.setDataFormat(format.getFormat(FORMATO_MONTOS));
		        cell.setCellStyle(xssfCellStyleImportes);
		        cell.setCellValue(new Double(String.valueOf(partidasCasadasDTO.getImporte())));
	
			}
	    }
	    if (null != listaTmct0ContaOurDTO && !listaTmct0ContaOurDTO.isEmpty()) {
		    // Hoja con el listado de SIBBAC
		    XSSFSheet registros = workbook.createSheet("PARTIDAS CASADAS -SIBBAC");
		    registros.setDefaultColumnWidth(25);
		    int rowMin = 0;
	
		    // Cabecera
		    XSSFRow row = registros.createRow(rowMin++);
		    row.setHeight((short) 600);
		    XSSFCell cell = null;
			
		    cell = row.createCell(0);
		    cell.setCellValue("ORDEN");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(1);
		    cell.setCellValue("BOOKING");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(2);
		    cell.setCellValue("NUCNFCLT");
		    cell.setCellStyle(xssfCellStyleCabecera);
	
		    cell = row.createCell(3);
		    cell.setCellValue("NUCNFLIQ");
		    cell.setCellStyle(xssfCellStyleCabecera);

		    cell = row.createCell(4);
		    cell.setCellValue("SENTIDO");
		    cell.setCellStyle(xssfCellStyleCabecera);

		    cell = row.createCell(5);
		    cell.setCellValue("IMPORTE");
		    cell.setCellStyle(xssfCellStyleCabecera);
	    	
		    for (Tmct0ContaOurDTO ourDTO : listaTmct0ContaOurDTO) {
		    	row = registros.createRow(rowMin++);
		        
		        cell = row.createCell(0);
		        cell.setCellValue(ourDTO.getNuorden());
		        
		        cell = row.createCell(1);
		        cell.setCellValue(ourDTO.getNbooking());

		        cell = row.createCell(2);
		        cell.setCellValue(ourDTO.getNucnfclt());

		        cell = row.createCell(3);
		        cell.setCellValue(ourDTO.getNucnfliq().toString());

		        cell = row.createCell(4);
		        cell.setCellValue(ourDTO.getSentido());

		        cell = row.createCell(5);
		        xssfCellStyleImportes.setDataFormat(format.getFormat(FORMATO_MONTOS));
		        cell.setCellStyle(xssfCellStyleImportes);
		        cell.setCellValue(new Double(String.valueOf(ourDTO.getImporte())));

			}
	    }
		return workbook;
	}
	
	
	public static XSSFWorkbook generarXLSApunte(List<GrillaExcelConciliacionBancariaDTO> filasGrilla) throws ParseException{
	    XSSFWorkbook workbook = new XSSFWorkbook();
	    // Estilo de la cabecera
	    XSSFCellStyle xssfCellStyleCabecera = workbook.createCellStyle();

	    // Crear la fuente de la cabecera
	    XSSFFont xssfFont = workbook.createFont();
	    xssfFont.setFontHeightInPoints((short) 8);
	    xssfFont.setFontName("ARIAL");
	    xssfFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	    xssfFont.setColor(new XSSFColor(Color.BLACK));
	    xssfCellStyleCabecera.setFont(xssfFont);

	    xssfCellStyleCabecera.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	    xssfCellStyleCabecera.setBorderRight(XSSFCellStyle.BORDER_THIN);
	    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());

	    xssfCellStyleCabecera.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    xssfCellStyleCabecera.setVerticalAlignment(XSSFCellStyle.ALIGN_FILL);
		
	    // Hoja con el listado
	    XSSFSheet registros = workbook.createSheet("apuntes_saldo0");
	    registros.setDefaultColumnWidth(25);
	    int rowMin = 0;
		
	    // Cabecera
	    XSSFRow row = registros.createRow(rowMin++);
	    row.setHeight((short) 600);
	    XSSFCell cell = null;
		
	    cell = row.createCell(0);
	    cell.setCellValue("PLANTILLA");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(1);
	    cell.setCellValue("AUXILIAR DEBE");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(2);
	    cell.setCellValue("CONCEPTO DEBE");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(3);
	    cell.setCellValue("IMPORTE DEBE");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(4);
	    cell.setCellValue("AUXILIAR HABER");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(5);
	    cell.setCellValue("CONCEPTO HABER");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(6);
	    cell.setCellValue("IMPORTE HABER");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(7);
	    cell.setCellValue("IMPORTE PYG");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(8);
	    cell.setCellValue("CUENTA CONTABLE DEBE");
	    cell.setCellStyle(xssfCellStyleCabecera);
		
	    cell = row.createCell(9);
	    cell.setCellValue("CUENTA CONTABLE HABER");
	    cell.setCellStyle(xssfCellStyleCabecera);

	    for (GrillaExcelConciliacionBancariaDTO fila : filasGrilla) {
	    	row = registros.createRow(rowMin++);
	        
	        cell = row.createCell(0);
	        cell.setCellValue(fila.getPlantilla());
	        
	        cell = row.createCell(1);
	        cell.setCellValue(fila.getAuxDebe());
	        
	        cell = row.createCell(2);
	        cell.setCellValue(fila.getConceptoDebe());
	        
	        cell = row.createCell(3);
	        cell.setCellValue(fila.getImporteDebe().toString());
	        
	        cell = row.createCell(4);
	        cell.setCellValue(fila.getAuxHaber());
	        
	        cell = row.createCell(5);
	        cell.setCellValue(fila.getConceptoHaber());
	        
	        cell = row.createCell(6);
	        cell.setCellValue(fila.getImporteHaber().toString());
	        
	        cell = row.createCell(7);
	        cell.setCellValue(fila.getImporteDebe().subtract(fila.getImporteHaber()).negate().toString());
	        
	        cell = row.createCell(8);
	        cell.setCellValue(fila.getCuentaContableDebe());

	        cell = row.createCell(9);
	        cell.setCellValue(fila.getCuentaContableHaber());
	    	
		}
	    
		return workbook;
	}

	public static List<GrillaExcelConciliacionBancariaDTO> generarFilasConciliacionBancaria(List<GrillaTLMDTO> theirsSelected, String concepto) {
		List<GrillaExcelConciliacionBancariaDTO> filas = null;

		filas = obtenerFilasPorAuxiliar(theirsSelected, true, concepto);
		
		return filas;
	}
	
	public static List<GrillaExcelConciliacionBancariaDTO> generarFilasConciliacionBancaria(List<GrillaTLMDTO> theirsSelected, List<GrillaApunteDTO> apuntesDTO) {
		List<GrillaExcelConciliacionBancariaDTO> filas = null;

		filas = obtenerFilasPorAuxiliar(theirsSelected, false, null);
		
		filas = completarFilas(filas, apuntesDTO);
		
		return filas;
	}

	private static List<GrillaExcelConciliacionBancariaDTO> completarFilas(List<GrillaExcelConciliacionBancariaDTO> filas, List<GrillaApunteDTO> apuntesDTO) {
		
		if (null != filas) {
			int index = 0;
			for (; index < filas.size();index++) {
				GrillaExcelConciliacionBancariaDTO fila = filas.get(index);
				
				if (!fila.isCompleta()) {
					for (GrillaApunteDTO apunteDTO : apuntesDTO) {
						BigDecimal importe = null;
						if (apunteDTO.getImporte().equals(BigDecimal.ZERO)) {
							continue;
						}
						if (fila.isCredit()) {
							importe = fila.getImporteHaber();
							
							if (importe.compareTo(apunteDTO.getImporte()) <= 0) {
								//Completo los datos de la fila
								fila.setImporteDebe(importe);
								fila.setCuentaContableDebe(apunteDTO.getCuentaContable());
								fila.setAuxDebe(apunteDTO.getAuxiliar());
								fila.setConceptoDebe(apunteDTO.getConcepto());
								fila.setConceptoHaber(apunteDTO.getConcepto());
								//Marco la fila como completa
								fila.setCompleta(true);
								//Actualizo el importe del apunte
								BigDecimal resto = apunteDTO.getImporte().subtract(importe);
								apunteDTO.setImporte(resto);
							} else {
								//Completo los datos de la fila
								fila.setImporteHaber(apunteDTO.getImporte());
								fila.setImporteDebe(apunteDTO.getImporte());
								fila.setCuentaContableDebe(apunteDTO.getCuentaContable());
								fila.setAuxDebe(apunteDTO.getAuxiliar());
								fila.setConceptoDebe(apunteDTO.getConcepto());
								fila.setConceptoHaber(apunteDTO.getConcepto());
								//Marco la fila como completa
								fila.setCompleta(true);
								//Actualizo el importe del apunte a 0
								BigDecimal resto = importe.subtract(apunteDTO.getImporte());
								apunteDTO.setImporte(BigDecimal.ZERO);
								//Creo una nueva fila con el resto de la fila anterior
								GrillaTLMDTO tlmAux = new GrillaTLMDTO();
								tlmAux.setImporte(resto);
								tlmAux.setAuxiliarBancario(fila.getAuxHaber());
								tlmAux.setTipoDeMovimiento("Their Credit");
								GrillaExcelConciliacionBancariaDTO filaNueva = crearNuevaFila(tlmAux, false, null);
								filas.add(index + 1, filaNueva);
							}
							break;
						} else {
							importe = fila.getImporteDebe();
							
							if (importe.compareTo(apunteDTO.getImporte()) <= 0) {
								//Completo los datos de la fila
								fila.setImporteHaber(importe);
								fila.setCuentaContableHaber(apunteDTO.getCuentaContable());
								fila.setAuxHaber(apunteDTO.getAuxiliar());
								fila.setConceptoHaber(apunteDTO.getConcepto());
								fila.setConceptoDebe(apunteDTO.getConcepto());
								//Marco la fila como completa
								fila.setCompleta(true);
								//Actualizo el importe del apunte
								BigDecimal resto = apunteDTO.getImporte().subtract(importe);
								apunteDTO.setImporte(resto);
							} else {
								//Completo los datos de la fila
								fila.setImporteDebe(apunteDTO.getImporte());
								fila.setImporteHaber(apunteDTO.getImporte());
								fila.setCuentaContableHaber(apunteDTO.getCuentaContable());
								fila.setAuxHaber(apunteDTO.getAuxiliar());
								fila.setConceptoDebe(apunteDTO.getConcepto());
								fila.setConceptoHaber(apunteDTO.getConcepto());
								//Marco la fila como completa
								fila.setCompleta(true);
								//Actualizo el importe del apunte a 0
								BigDecimal resto = importe.subtract(apunteDTO.getImporte());
								apunteDTO.setImporte(BigDecimal.ZERO);
								//Creo una nueva fila con el resto de la fila anterior
								GrillaTLMDTO tlmAux = new GrillaTLMDTO();
								tlmAux.setImporte(resto);
								tlmAux.setAuxiliarBancario(fila.getAuxHaber());
								tlmAux.setTipoDeMovimiento("THEIR DEBIT");
								GrillaExcelConciliacionBancariaDTO filaNueva = crearNuevaFila(tlmAux, false, null);
								filas.add(index + 1, filaNueva);
							}
							break;
						}
					}
				}
			}
		}
		
		return filas;
	}
	

	/**
	 *	Obtiene el mensaje de tipo de movimiento segun el contenido de expresiones
	 *	en la cadena de caracteres.
	 *
	 *	30-01-2019
	 *	Nueva version fichero TLM
	 * 	En base de datos guardamos el keyname del la siguiente forma
	 * 	contabilidad.tlm.our_credit =  S CR
	 * 	contabilidad.tlm.our_debit =  S DR
	 *
	 * 	contabilidad.tlm.their_credit  =  L CR
	 * 	contabilidad.tlm.their_debit  =  L DR
	 *
	 * 	Lo Que hacemos es buscar el valor de la celda en la columna value y extraer el tipo de movimiento de la columna  key;
	 *
	 * 	el valor del keyname (despues del ultimo . ), coincide con sibbac.business.accounting.utils.Constantes.THEIR o OUR
	 *
	 *
	 *	@param tiposMovs listas de tipos de movimientos para tlm extraidos de Tmct0cfg
	 *	@param valorCelda valor de la celda
	 *	@return String
	 */


	public static String getTextTipoMovimiento(final List<Tmct0cfg> tiposMovs,String valorCelda){
		String mov_type  = null ;
		for(Tmct0cfg cfg :tiposMovs){
			if(cfg.getKeyvalue().toLowerCase().contains(valorCelda.toLowerCase())) {
				/*Nuestro mov type lo usamos desde el cdauxili del cfg .*/
				mov_type =  cfg.getCdauxili();
				return mov_type;
			}
		}
		return mov_type;
	}

	/**
	 *
	 *  retorna true si el tipo de movimiento es OUR es decir, their debit is our credit
	 * @param tiposMovs
	 * @param valorCelda
	 * @return
	 */

	public static boolean isTipoMovimientoOur(final List<Tmct0cfg> tiposMovs,String valorCelda){
		boolean  isOur = false;
		for(Tmct0cfg cfg :tiposMovs){
			if(cfg.getKeyvalue().toLowerCase().contains(valorCelda.toLowerCase())) {
				final String keyname =cfg.getId().getKeyname().substring(cfg.getId().getKeyname().lastIndexOf(".")+1) ;
				if (keyname.contains(Constantes.OUR_CREDIT)||keyname.contains(Constantes.THEIR_DEBIT)) {
					/*Nuestro mov type (mostrado por pantalla), lo usamos desde el cdauxili del cfg .*/
					isOur = true;
					break;
				}
			}
		}
		return isOur;
	}
	/**
	 *
	 *  retorna true si el tipo de movimiento es their, es decir their credit is our debit
	 * @param tiposMovs
	 * @param valorCelda
	 * @return
	 */
	public static boolean isTipoMovimientoTheir(final List<Tmct0cfg> tiposMovs,String valorCelda){
		boolean  isTheir = false;
		for(Tmct0cfg cfg :tiposMovs){
			if(cfg.getKeyvalue().toLowerCase().contains(valorCelda.toLowerCase())) {
				final String keyname =cfg.getId().getKeyname().substring(cfg.getId().getKeyname().lastIndexOf(".")+1) ;
				if (keyname.contains(Constantes.OUR_DEBIT)||keyname.contains(Constantes.THEIR_CREDIT)) {
					isTheir = true;
					break;
				}
			}
		}
		return isTheir;
	}

	/**
	 * Crea una fila por cada auxiliar
	 * @param theirsSelected
	 * @return
	 */
	private static List<GrillaExcelConciliacionBancariaDTO> obtenerFilasPorAuxiliar(List<GrillaTLMDTO> theirsSelected, boolean isTLMxls, String concepto) {
		List<GrillaExcelConciliacionBancariaDTO> filas = null;
		GrillaExcelConciliacionBancariaDTO filaNueva = null;
		boolean crearNueva = true;

		if (null != theirsSelected) {
			filas = new ArrayList<GrillaExcelConciliacionBancariaDTO>();
			//recorro las filas de la grilla de la izquierda
			for (GrillaTLMDTO tlmDTO : theirsSelected) {
				//creo la primer fila de la grilla para el primer auxiliar
				if (filas.isEmpty()) {
					filaNueva = crearNuevaFila(tlmDTO, isTLMxls, concepto);
					filas.add(filaNueva);
					continue;
				}
				//recorro las filas creadas para agrupar por auxiliar (de la grilla de la izquierda)

				Tmct0cfgBo tmct0cfgBo  = StaticContextAccessor.getBean(Tmct0cfgBo.class);
				List<Tmct0cfg> tiposMovimiento =  tmct0cfgBo.findByAplicationAndProcess(Constantes.APP_NAME,Constantes.PROCCESS_NAME);


 			for (GrillaExcelConciliacionBancariaDTO fila : filas) {
					if ((null != fila.getAuxDebe() && fila.getAuxDebe().equals(tlmDTO.getAuxiliarBancario())) || (null != fila.getAuxHaber() && fila.getAuxHaber().equals(tlmDTO.getAuxiliarBancario()))){
						crearNueva = false;

						//our
						if(isTipoMovimientoOur(tiposMovimiento, tlmDTO.getTipoDeMovimiento())) {
							fila.setImporteHaber(fila.getImporteHaber().add(tlmDTO.getImporte()));
							fila.setAuxHaber(tlmDTO.getAuxiliarBancario());
						} else {
							//their
							fila.setImporteDebe(fila.getImporteDebe().add(tlmDTO.getImporte()));
							fila.setAuxDebe(tlmDTO.getAuxiliarBancario());
						}
						break;
					} 
				}
				if (crearNueva) {
					filaNueva = crearNuevaFila(tlmDTO, isTLMxls, concepto);
					filas.add(filaNueva);
				}
				crearNueva = true;
			}
		}
		
		return filas;
	}

	private static GrillaExcelConciliacionBancariaDTO crearNuevaFila(GrillaTLMDTO tlmDTO, boolean isTLMxls, String concepto) {
		String THEIR_PREFIX = "THEIR_";
		String CUENTA_CONTABLE = "1021101";

		GrillaExcelConciliacionBancariaDTO filaNueva;
		filaNueva = new GrillaExcelConciliacionBancariaDTO();
		filaNueva.setPlantilla(THEIR_PREFIX + tlmDTO.getAuxiliarBancario());
		filaNueva.setImportePyG(BigDecimal.ZERO);
		filaNueva.setCompleta(false);
		
		if (null != tlmDTO) {
			final Tmct0cfgBo tmct0cfgBo  = StaticContextAccessor.getBean(Tmct0cfgBo.class);
			final  List<Tmct0cfg> tiposMovimiento =  tmct0cfgBo.findByAplicationAndProcess(Constantes.APP_NAME,Constantes.PROCCESS_NAME);

			if(isTipoMovimientoOur(tiposMovimiento,tlmDTO.getTipoDeMovimiento())) {
				filaNueva.setCredit(true);
				filaNueva.setImporteHaber(tlmDTO.getImporte());
				filaNueva.setImporteDebe(BigDecimal.ZERO);
				filaNueva.setAuxHaber(tlmDTO.getAuxiliarBancario());
				filaNueva.setCuentaContableHaber(CUENTA_CONTABLE);
				if (isTLMxls) {
					filaNueva.setCuentaContableDebe(CUENTA_CONTABLE);
					filaNueva.setConceptoHaber(concepto);
					filaNueva.setConceptoDebe(concepto);
				}
			} else {
				filaNueva.setCredit(false);
				filaNueva.setImporteDebe(tlmDTO.getImporte());
				filaNueva.setImporteHaber(BigDecimal.ZERO);
				filaNueva.setAuxDebe(tlmDTO.getAuxiliarBancario());
				filaNueva.setCuentaContableDebe(CUENTA_CONTABLE);
				if (isTLMxls) {
					filaNueva.setCuentaContableHaber(CUENTA_CONTABLE);
					filaNueva.setConceptoHaber(concepto);
					filaNueva.setConceptoDebe(concepto);
				}
			}
		}
		
		return filaNueva;
	}
	
	public static XSSFWorkbook generarXLSApuntesSIBBAC(List<DatosApuntesExcelDTO> filasGrilla){
	    XSSFWorkbook workbook = new XSSFWorkbook();
	    // Estilo de la cabecera
	    XSSFCellStyle xssfCellStyleCabecera = workbook.createCellStyle();

	    // Crear la fuente de la cabecera
	    XSSFFont xssfFont = workbook.createFont();
	    xssfFont.setFontHeightInPoints((short) 8);
	    xssfFont.setFontName("ARIAL");
	    xssfFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	    xssfFont.setColor(new XSSFColor(Color.BLACK));
	    xssfCellStyleCabecera.setFont(xssfFont);

	    xssfCellStyleCabecera.setBorderBottom(XSSFCellStyle.BORDER_THIN);
	    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	    xssfCellStyleCabecera.setBorderRight(XSSFCellStyle.BORDER_THIN);
	    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());

	    xssfCellStyleCabecera.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    xssfCellStyleCabecera.setVerticalAlignment(XSSFCellStyle.ALIGN_FILL);
		
	    // Hoja con el listado
	    XSSFSheet registros = workbook.createSheet("apuntes_sibbac");
	    registros.setDefaultColumnWidth(25);
	    int rowMin = 0;
		
	    for (DatosApuntesExcelDTO fila : filasGrilla) {
	    	XSSFRow row = registros.createRow(rowMin++);
	    	row.setHeight((short) 600);
	    	XSSFCell cell = null;

	    	cell = row.createCell(0);
		    cell.setCellValue("PLANTILLA");
		    cell.setCellStyle(xssfCellStyleCabecera);
		    
		    cell = row.createCell(1);
		    cell.setCellValue(fila.getPlantilla());
			
		    cell = row.createCell(2);
		    cell.setCellValue("CONCEPTO");
		    cell.setCellStyle(xssfCellStyleCabecera);
		    
		    cell = row.createCell(3);
		    cell.setCellValue(fila.getConcepto());
			
		    cell = row.createCell(4);
		    cell.setCellValue("REFERENCIA THEIR");
		    cell.setCellStyle(xssfCellStyleCabecera);

		    int countRef = 5;
		    for (String referencia : fila.getReferencias()) {
		        cell = row.createCell(countRef);
		        cell.setCellValue(referencia);
		        
		        countRef = countRef + 1;
			}
		    
		    for (Tmct0ContaOurApuntesDTO camposClave : fila.getCamposClave()) {
		    	XSSFRow rowCamposClave = registros.createRow(rowMin++);
		    	rowCamposClave.setHeight((short) 600);

			    cell = rowCamposClave.createCell(0);
			    cell.setCellValue(camposClave.getNuorden());
			    cell = rowCamposClave.createCell(1);
			    cell.setCellValue(camposClave.getNbooking());
			    cell = rowCamposClave.createCell(2);
			    cell.setCellValue(camposClave.getNucnfclt());
			    cell = rowCamposClave.createCell(3);
			    cell.setCellValue(camposClave.getNucnfliq().toString());
			}
	    	
		}
	    
		return workbook;
	}

	public static List<DatosApuntesExcelDTO> generarFilasGrilla(List<GrillaTLMDTO> listaTLMDTOs, List<Tmct0ContaOurApuntesDTO> camposClave, String concepto) {
		List<DatosApuntesExcelDTO> lista = new ArrayList<DatosApuntesExcelDTO>();
		
		lista = crearFilasEncabezado(listaTLMDTOs, concepto);
		
		lista = completarLista(lista, camposClave);
		
		return lista;
	}

	private static List<DatosApuntesExcelDTO> completarLista(List<DatosApuntesExcelDTO> lista,List<Tmct0ContaOurApuntesDTO> camposClave) {
		boolean agregarNueva = true;

		for (Tmct0ContaOurApuntesDTO camposClaveDTO : camposClave) {
			DatosApuntesExcelDTO tmct0ContaOurApuntesDTO = null;
			for (ListIterator<DatosApuntesExcelDTO> iterator = lista.listIterator(); iterator.hasNext();) {
				tmct0ContaOurApuntesDTO = (DatosApuntesExcelDTO) iterator.next();
				
				if (tmct0ContaOurApuntesDTO.getAuxiliar().equals(camposClaveDTO.getAuxiliar())) {
					if (null == tmct0ContaOurApuntesDTO.getPlantilla()) {
						tmct0ContaOurApuntesDTO.setPlantilla(camposClaveDTO.getPlantilla());
						tmct0ContaOurApuntesDTO.getCamposClave().add(camposClaveDTO);
						agregarNueva = false;
					} else if (tmct0ContaOurApuntesDTO.getPlantilla().equalsIgnoreCase(camposClaveDTO.getPlantilla())) {
						tmct0ContaOurApuntesDTO.getCamposClave().add(camposClaveDTO);
						agregarNueva = false;
					} 
				}
			}
			if (agregarNueva) {
				//genero una nueva fila para la plantilla
				DatosApuntesExcelDTO cabeceraAux = null;
				for (DatosApuntesExcelDTO datosApuntesExcelDTO : lista) {
					if (datosApuntesExcelDTO.getAuxiliar().equalsIgnoreCase(camposClaveDTO.getAuxiliar())) {
						cabeceraAux = datosApuntesExcelDTO;
						break;
					}
				}
				DatosApuntesExcelDTO nueva = copiarCabecera(cabeceraAux);
				nueva.setPlantilla(camposClaveDTO.getPlantilla());
				nueva.getCamposClave().add(camposClaveDTO);
				lista.add(nueva);
			}
			agregarNueva = true;
		}
		
		
		return lista;
	}

	private static List<DatosApuntesExcelDTO> crearFilasEncabezado(List<GrillaTLMDTO> listaTLMDTOs, String concepto) {
		List<DatosApuntesExcelDTO> lista = new ArrayList<DatosApuntesExcelDTO>();
		boolean crearNueva = true;
		if (null != listaTLMDTOs) {
			for (GrillaTLMDTO tlmdto : listaTLMDTOs) {
				if (lista.isEmpty()) {
					DatosApuntesExcelDTO fila = crearNuevaCabecera(tlmdto, concepto);
					lista.add(fila);
					continue;
				}
				
				for (DatosApuntesExcelDTO datosApuntesExcelDTO : lista) {
					
					if (datosApuntesExcelDTO.getAuxiliar().equals(tlmdto.getAuxiliarBancario())) {
						crearNueva = false;
						datosApuntesExcelDTO.getReferencias().add(tlmdto.getConceptoMovimiento());
						break;
					}
				}
				if (crearNueva) {
					DatosApuntesExcelDTO fila = crearNuevaCabecera(tlmdto, concepto);
					lista.add(fila);
				}
				crearNueva = true;
			}
		}
		
		return lista;
	}
	
	private static DatosApuntesExcelDTO crearNuevaCabecera(GrillaTLMDTO tlmdto, String concepto) {
		DatosApuntesExcelDTO fila = new DatosApuntesExcelDTO();
		fila.setAuxiliar(tlmdto.getAuxiliarBancario());
		//fila.setPlantilla(tlmdto);
		if(null != concepto) {
			fila.setConcepto(concepto);
		} else {
			fila.setConcepto(tlmdto.getConceptoMovimiento());
		}
		List<String> referencias = new ArrayList<String>();
		referencias.add(tlmdto.getConceptoMovimiento());
		fila.setReferencias(referencias);
		fila.setCamposClave(new ArrayList<Tmct0ContaOurApuntesDTO>());
		return fila;
	}

	private static DatosApuntesExcelDTO copiarCabecera(DatosApuntesExcelDTO origen) {
		DatosApuntesExcelDTO fila = new DatosApuntesExcelDTO();
		fila.setAuxiliar(origen.getAuxiliar());
		fila.setConcepto(origen.getConcepto());
		fila.setReferencias(origen.getReferencias());
		fila.setCamposClave(new ArrayList<Tmct0ContaOurApuntesDTO>());
		return fila;
	}

	/**
	 * 	APUNTES MANUALES
	 * 	Se arma una clausula Where modificada que será utilizada para modificar las siguientes
	 * 	queries de la plantilla: Query Orden, Query apunte Detalle y Query de bloqueo.
	 * 	
	 * 	@param listAmExcelDTO: lista con los ALCs a concatenar
	 *	@return String : clausula where modificada 			
	 */
	public static String concatWhereModificadoByListaALCsAM(
			List<ApuntesManualesExcelDTO> listAmExcelDTO) throws EjecucionPlantillaException {
		StringBuffer where = new StringBuffer();
		where.append(" WHERE ( nuorden, nbooking, nucnfclt, nucnfliq ) IN (VALUES ");
		for (int i = 0; i < listAmExcelDTO.size(); i++) {
			if (i == listAmExcelDTO.size() - 1) {
				where.append("('" + listAmExcelDTO.get(i).getSibbacNuorden() 
						+ "','" + listAmExcelDTO.get(i).getSibbacNbooking() + "','" 
						+  listAmExcelDTO.get(i).getSibbacNucnfclt() + "'," 
						+ listAmExcelDTO.get(i).getSibbacNucnfliq() +")) ");  
			} else {
				where.append("('" + listAmExcelDTO.get(i).getSibbacNuorden() + "','" 
						+ listAmExcelDTO.get(i).getSibbacNbooking() + "','" 
						+  listAmExcelDTO.get(i).getSibbacNucnfclt() + "'," 
						+ listAmExcelDTO.get(i).getSibbacNucnfliq() +"),"); 
			}
		}
		return where.toString();
	}

	/**
	 * 	APUNTES MANUALES
	 * 	Se arma una clausula Where modificada que será utilizada para modificar las siguientes
	 * 	queries de la plantilla: Query de cobro.   
	 *	@param amExcelDTO DTO con datos del Excel
	 * 	@return String con la clausula Where actualizada
	 * 	@throws EjecucionPlantillaException EjecucionPlantillaException
	 */
	public static String concatWhereModificadoByReferenciasTheirAM(ApuntesManualesExcelDTO amExcelDTO) 
			throws EjecucionPlantillaException {
		
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" WHERE fic.entidad = cast(substr(<CUENTA_BANCARIA DE LA PLANTILLA>,5,4) as float) "
				+ "and fic.oficina = cast(substr(<CUENTA_BANCARIA DE LA PLANTILLA>,9,4) as float) "
				+ "and fic.cuenta = cast(substr(<CUENTA_BANCARIA DE LA PLANTILLA>,15,10) as float) "
				+ "and (");
		
		if ((StringUtils.isNotBlank(amExcelDTO.getSibbacRefTheirDescRefTheirMayor12()) && !amExcelDTO.getSibbacRefTheirDescRefTheirMayor12().equals("VALUES")) 
			&& StringUtils.isNotBlank(amExcelDTO.getSibbacReferenciasTheirMenor12())) {
			// Longitud de referencias Their en Excel varias combinadas (menores a 12 y mayores o iguales a 12)
			sbQuery.append("(mov.referencia, mov.desc_referencia) in (" + amExcelDTO.getSibbacRefTheirDescRefTheirMayor12() 
				+ ") or mov.desc_referencia in (" + amExcelDTO.getSibbacReferenciasTheirMenor12() + ")");
		} else if (StringUtils.isNotBlank(amExcelDTO.getSibbacRefTheirDescRefTheirMayor12()) && !amExcelDTO.getSibbacRefTheirDescRefTheirMayor12().equals("VALUES")) {
			// Longitud de referencia their en el Excel mayor a 12
			sbQuery.append("(mov.referencia, mov.desc_referencia) in (" + amExcelDTO.getSibbacRefTheirDescRefTheirMayor12() + ")");
		} else if (StringUtils.isNotBlank(amExcelDTO.getSibbacReferenciasTheirMenor12())) {
			// Longitud de referencia their en el Excel menor o igual a 12
			sbQuery.append("mov.desc_referencia in (" + amExcelDTO.getSibbacReferenciasTheirMenor12() + ")");
		} 
		sbQuery.append(") and mov.procesado IN (0, 2)");
		return sbQuery.toString();
	}
}
