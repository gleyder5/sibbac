package sibbac.business.accounting.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.database.dto.ContaPeriodosFiltroDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPeriodos;
import sibbac.common.SIBBACBusinessException;

@Repository
public class Tmct0ContaPeriodosDaoImp {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaPeriodosDaoImp.class);
	
    @PersistenceContext
    private EntityManager em;
	
	public List<Tmct0ContaPeriodos> consultarPeriodos(ContaPeriodosFiltroDTO periodosFiltroDTO) throws Exception {

		LOG.info("INICIO - DAOIMPL - consultarPeriodos");

		List<Tmct0ContaPeriodos> listaPeriodos = new ArrayList<Tmct0ContaPeriodos>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT cp FROM Tmct0ContaPeriodos cp WHERE 1=1");

			if (null != periodosFiltroDTO.getAnio()) {
				consulta.append(" AND cp.anio = :anio");
				parameters.put("anio", periodosFiltroDTO.getAnio());
			}

			if (null != periodosFiltroDTO.getPeriodo()) {
				consulta.append(" AND cp.periodo = :periodo");
				parameters.put("periodo", periodosFiltroDTO.getPeriodo());
			}

			if ((null != periodosFiltroDTO.getFhIniDesde() && !periodosFiltroDTO.getFhIniDesde().equals(""))
					&& ((null != periodosFiltroDTO.getFhIniHasta() && !periodosFiltroDTO.getFhIniHasta().equals("")))) {
				consulta.append(" AND cp.fhIniPeriodo BETWEEN :fechaDesde AND :fechaHasta");
				parameters.put("fechaDesde", periodosFiltroDTO.getFhIniDesde());
				parameters.put("fechaHasta", periodosFiltroDTO.getFhIniHasta());
			} else if ((null != periodosFiltroDTO.getFhIniDesde() && !periodosFiltroDTO.getFhIniDesde().equals(""))
					&& ((null == periodosFiltroDTO.getFhIniHasta() || periodosFiltroDTO.getFhIniHasta().equals("")))) {
				consulta.append(" AND cp.fhIniPeriodo >= :fechaDesde");
				parameters.put("fechaDesde", periodosFiltroDTO.getFhIniDesde());
			} else if ((null == periodosFiltroDTO.getFhIniDesde() || periodosFiltroDTO.getFhIniDesde().equals(""))
					&& ((null != periodosFiltroDTO.getFhIniHasta() && !periodosFiltroDTO.getFhIniHasta().equals("")))) {
				consulta.append(" AND cp.fhIniPeriodo <= :fechaHasta");
				parameters.put("fechaHasta", periodosFiltroDTO.getFhIniHasta());
			}
			
			consulta.append(" ORDER BY cp.fhFinPeriodo");

			LOG.debug("Creamos y ejecutamos la query que recupera los periodos: " + consulta.toString());
			TypedQuery<Tmct0ContaPeriodos> query = em.createQuery(consulta.toString(), Tmct0ContaPeriodos.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades factura manual con los datos recuperados.
			listaPeriodos = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultar periodos contables -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarPeriodos");

		return listaPeriodos;
	}

}
