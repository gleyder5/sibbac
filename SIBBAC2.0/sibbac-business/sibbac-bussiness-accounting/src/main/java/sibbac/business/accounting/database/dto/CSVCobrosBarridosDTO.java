package sibbac.business.accounting.database.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Data Transfer Object para el CSV de Cobros-Barridos.
 */
public class CSVCobrosBarridosDTO {

	private Logger LOG = LoggerFactory.getLogger(CSVCobrosBarridosDTO.class);

	private String numOrde;
	private String numeJin;
	private Date fechCon;
	private String impDivo;
	private String indAbad;
	private String codVaiso;
	private String canal;
	private String tipComis;
	private boolean isEncabezado;

	public String getNumOrde() {
		return numOrde;
	}

	public void setNumOrde(String numOrde) {
		this.numOrde = numOrde;
	}

	public String getNumeJin() {
		return this.numeJin;
	}

	public void setNumeJin(String numeJin) {
		this.numeJin = numeJin;
	}

	public Date getFechCon() {
		return this.fechCon;
	}

	public void setFechCon(Date fechCon) {
		this.fechCon = fechCon;
	}

	public String getImpDivo() {
		return this.impDivo;
	}

	public void setImpDivo(String impDivo) {
		this.impDivo = impDivo;
	}

	public String getIndAbad() {
		return this.indAbad;
	}

	public void setIndAbad(String indAbad) {
		this.indAbad = indAbad;
	}

	public String getCodVaiso() {
		return this.codVaiso;
	}

	public void setCodVaiso(String codVaiso) {
		this.codVaiso = codVaiso;
	}

	public String getCanal() {
		return this.canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getTipComis() {
		return this.tipComis;
	}

	public void setTipComis(String tipComis) {
		this.tipComis = tipComis;
	}

	public boolean isEncabezado() {
		return this.isEncabezado;
	}

	public void setEncabezado(boolean isEncabezado) {
		this.isEncabezado = isEncabezado;
	}

	public void popularEntidad(String[] filaCSV) {
		this.setNumOrde(new String(filaCSV[0]));
		this.setNumeJin(new String(filaCSV[1]));
		this.setImpDivo(new String(filaCSV[3]));
		this.setIndAbad(new String(filaCSV[4]));
		this.setCodVaiso(new String(filaCSV[5]));
		this.setCanal(new String(filaCSV[6]));
		this.setTipComis(new String(filaCSV[7]));
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			this.setFechCon(formatter.parse(String.valueOf(filaCSV[2])));
		} catch (ParseException e) {
			// Se contempla formato alternativo.
			try {
				SimpleDateFormat newFormatter = new SimpleDateFormat("dd/MM/yyyy");
				this.setFechCon(newFormatter.parse(String.valueOf(filaCSV[2])));
			} catch (ParseException e1) {
				LOG.error("Error al formatear fecha.", e1);
			}
		}
	}

}
