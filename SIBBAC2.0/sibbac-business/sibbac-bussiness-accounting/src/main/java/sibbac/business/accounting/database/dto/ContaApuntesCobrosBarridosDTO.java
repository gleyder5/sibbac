package sibbac.business.accounting.database.dto;

import java.util.Arrays;

import sibbac.business.accounting.database.model.Tmct0ContaApuntes;

/**
 * Data Transfer Object para la gestión de apuntes contables en cobros barridos.
 */
public class ContaApuntesCobrosBarridosDTO {

	private Tmct0ContaApuntes[] arrayTmct0ContaApuntes;

	public ContaApuntesCobrosBarridosDTO() {
		super();
	}
	
	public Tmct0ContaApuntes[] getArrayTmct0ContaApuntes() {
		if(arrayTmct0ContaApuntes == null) { 
			return this.arrayTmct0ContaApuntes = new Tmct0ContaApuntes[0]; 
		} else { 
			return this.arrayTmct0ContaApuntes = Arrays.copyOf(
				arrayTmct0ContaApuntes, arrayTmct0ContaApuntes.length); 
		}
	}

	public void setArrayTmct0ContaApuntes(Tmct0ContaApuntes[] arrayTmct0ContaApuntes) { 
	  if(arrayTmct0ContaApuntes == null) { 
		  this.arrayTmct0ContaApuntes = new Tmct0ContaApuntes[0]; 
	  } else { 
		  this.arrayTmct0ContaApuntes = Arrays.copyOf(arrayTmct0ContaApuntes, arrayTmct0ContaApuntes.length); 
	  } 
	}

}
