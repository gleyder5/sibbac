package sibbac.business.accounting.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.util.StringUtils;

import sibbac.common.utils.SibbacEnums;

/**
 * Entidad para la gestion de TMCT0_CONTA_PLANTILLAS
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CONTA_PLANTILLAS")
public class Tmct0ContaPlantillas implements java.io.Serializable {

	private static final long serialVersionUID = 30481579960171622L;

	/** Campos de la entidad */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 4, nullable = false)
	private Integer id;

	@Column(name = "CODIGO", length = 50, nullable = false)
	private String codigo;

	@Column(name = "TIPO", length = 50, nullable = false)
	private String tipo;

	@Column(name = "SUBTIPO", length = 50)
	private String subtipo;

	@Column(name = "PROCESO", length = 50)
	private String proceso;

	@Column(name = "SUBPROCESO", length = 50)
	private String subproceso;

	@Column(name = "ACTIVO", length = 2, nullable = false)
	private Integer activo;

	@Column(name = "FICHERO", length = 2, nullable = false)
	private String fichero;

	@Column(name = "TIPO_COMPROBANTE", length = 2, nullable = false)
	private String tipo_comprobante;

	@Column(name = "NUM_COMPROBANTE", length = 4, nullable = false)
	private Integer num_comprobante;

	@Column(name = "CAMPO", length = 100, nullable = false)
	private String campo;

	@Column(name = "CAMPO_ESTADO", length = 50)
	private String campo_estado;

	@Column(name = "ESTADO_INICIO", length = 50)
	private String estado_inicio;

	@Column(name = "ESTADO_FINAL", length = 50)
	private String estado_final;

	@Column(name = "CUENTA_DEBE", length = 100)
	private String cuenta_debe;

	@Column(name = "AUX_DEBE", length = 100)
	private String aux_debe;

	@Column(name = "CUENTA_HABER", length = 100)
	private String cuenta_haber;

	@Column(name = "AUX_HABER", length = 100)
	private String aux_haber;

	@Column(name = "CUENTA_DIFERENCIAS", length = 15)
	private String cuenta_diferencias;

	@Column(name = "QUERY_ORDEN", length = 10000)
	private String query_orden;

	@Column(name = "QUERY_COBRO", length = 10000)
	private String query_cobro;

	@Column(name = "QUERY_FINAL", length = 10000)
	private String query_final;

	@Column(name = "FICHERO_ORDEN", length = 50)
	private String fichero_orden;

	@Column(name = "FICHERO_COBRO", length = 50)
	private String fichero_cobro;

	@Column(name = "CUENTA_BANCARIA", length = 24)
	private String cuenta_bancaria;
	
	@Column(name = "AUDIT_DATE")
	private Date audit_date;
	  
	@Column(name = "QUERY_BLOQUEO", length = 10000)
	private String query_bloqueo;

	@Column(name = "QUERY_APUNTE_DETALLE", length = 10000)
	private String query_apunte_detalle;
	
	@Column(name = "TOLERANCE", length = 1000)
	private BigDecimal tolerancia;
	
	@Column(name = "GRABA_DETALLE", length = 1)
	private Character grabaDetalle;
	
	@Column(name = "ESTADO_BLOQUEADO", length = 50)
	private String estado_bloqueado;

	@Column(name = "ESTADO_PARCIAL", length = 50)
	private String estado_parcial;

	public Tmct0ContaPlantillas() {
	};

	public Tmct0ContaPlantillas(Integer id, String codigo, String tipo, String subtipo, String proceso,
			String subproceso, Integer activo, String fichero, String tipo_comprobante, Integer num_comprobante,
			String campo, String campo_estado, String estado_inicio, String estado_final, String cuenta_debe,
			String aux_debe, String cuenta_haber, String aux_haber, String cuenta_diferencias, String query_orden,
			String query_cobro, String query_final, String fichero_orden, String fichero_cobro, String cuenta_bancaria,
			Date audit_date, String query_bloqueo, String query_apunte_detalle, BigDecimal tolerancia,
			Character grabaDetalle, String estado_bloqueado, String estado_parcial) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.tipo = tipo;
		this.subtipo = subtipo;
		this.proceso = proceso;
		this.subproceso = subproceso;
		this.activo = activo;
		this.fichero = fichero;
		this.tipo_comprobante = tipo_comprobante;
		this.num_comprobante = num_comprobante;
		this.campo = campo;
		this.campo_estado = campo_estado;
		this.estado_inicio = estado_inicio;
		this.estado_final = estado_final;
		this.cuenta_debe = cuenta_debe;
		this.aux_debe = aux_debe;
		this.cuenta_haber = cuenta_haber;
		this.aux_haber = aux_haber;
		this.cuenta_diferencias = cuenta_diferencias;
		this.query_orden = query_orden;
		this.query_cobro = query_cobro;
		this.query_final = query_final;
		this.fichero_orden = fichero_orden;
		this.fichero_cobro = fichero_cobro;
		this.cuenta_bancaria = cuenta_bancaria;
		this.audit_date = audit_date;
		this.query_bloqueo = query_bloqueo;
		this.query_apunte_detalle = query_apunte_detalle;
		this.tolerancia = tolerancia;
		this.grabaDetalle = grabaDetalle;
		this.estado_bloqueado = estado_bloqueado;
		this.estado_parcial = estado_parcial;
	}

	public String getEstado_parcial() {
		return estado_parcial;
	}

	public void setEstado_parcial(String estado_parcial) {
		this.estado_parcial = estado_parcial;
	}

	public Character getGrabaDetalle() {
		return grabaDetalle;
	}

	public void setGrabaDetalle(Character grabaDetalle) {
		this.grabaDetalle = grabaDetalle;
	}
	
	public Boolean isPlantillaConCuentas() {
		return (isCuentaDebe() || isCuentaAuxDebe()) && (isCuentaHaber() || isCuentaAuxHaber());
	}

	public Boolean isCuentaDebe() {
		return !StringUtils.isEmpty(getCuenta_debe());
	}

	public Boolean isCuentaAuxDebe() {
		return !StringUtils.isEmpty(getAux_debe());
	}

	public Boolean isCuentaHaber() {
		return !StringUtils.isEmpty(getCuenta_haber());
	}

	public Boolean isCuentaAuxHaber() {
		return !StringUtils.isEmpty(getAux_haber());
	}

	public String getCuentaDebe(int iCuenta) {
		String[] cuentas = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(getCuenta_debe(), ",");
		return getCuenta(cuentas, iCuenta);
	}

	public String getCuentaHaber(int iCuenta) {
		String[] cuentas = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(getCuenta_haber(), ",");
		return getCuenta(cuentas, iCuenta);
	}

	public String getCuentaAuxDebe(int iCuenta) {
		String[] cuentas = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(getAux_debe(), ",");
		return getCuenta(cuentas, iCuenta);
	}

	public String getCuentaAuxHaber(int iCuenta) {
		String[] cuentas = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(getAux_haber(), ",");
		return getCuenta(cuentas, iCuenta);
	}

	public String getCuenta(String[] cuentas, int iCuenta) {
		if (cuentas[iCuenta] != null) {
			return String.valueOf(cuentas[iCuenta]).trim();
		}
		return "";
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSubtipo() {
		return subtipo;
	}

	public void setSubtipo(String subtipo) {
		this.subtipo = subtipo;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public String getSubproceso() {
		return subproceso;
	}

	public void setSubproceso(String subproceso) {
		this.subproceso = subproceso;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public String getFichero() {
		return fichero;
	}

	public void setFichero(String fichero) {
		this.fichero = fichero;
	}

	public String getTipo_comprobante() {
		return tipo_comprobante;
	}

	public void setTipo_comprobante(String tipo_comprobante) {
		this.tipo_comprobante = tipo_comprobante;
	}

	public Integer getNum_comprobante() {
		return num_comprobante;
	}

	public void setNum_comprobante(Integer num_comprobante) {
		this.num_comprobante = num_comprobante;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getCampo_estado() {
		return campo_estado;
	}

	public void setCampo_estado(String campo_estado) {
		this.campo_estado = campo_estado;
	}

	public String getEstado_inicio() {
		return estado_inicio;
	}

	public void setEstado_inicio(String estado_inicio) {
		this.estado_inicio = estado_inicio;
	}

	public String getEstado_final() {
		return estado_final;
	}

	public void setEstado_final(String estado_final) {
		this.estado_final = estado_final;
	}

	public String getCuenta_debe() {
		return cuenta_debe;
	}

	public void setCuenta_debe(String cuenta_debe) {
		this.cuenta_debe = cuenta_debe;
	}

	public String getAux_debe() {
		return aux_debe;
	}

	public void setAux_debe(String aux_debe) {
		this.aux_debe = aux_debe;
	}

	public String getCuenta_haber() {
		return cuenta_haber;
	}

	public void setCuenta_haber(String cuenta_haber) {
		this.cuenta_haber = cuenta_haber;
	}

	public String getAux_haber() {
		return aux_haber;
	}

	public void setAux_haber(String aux_haber) {
		this.aux_haber = aux_haber;
	}

	public String getCuenta_diferencias() {
		return cuenta_diferencias;
	}

	public void setCuenta_diferencias(String cuenta_diferencias) {
		this.cuenta_diferencias = cuenta_diferencias;
	}

	public String getQuery_orden() {
		return query_orden;
	}

	public void setQuery_orden(String query_orden) {
		this.query_orden = query_orden;
	}

	public String getQuery_cobro() {
		return query_cobro;
	}

	public void setQuery_cobro(String query_cobro) {
		this.query_cobro = query_cobro;
	}

	public String getQuery_final() {
		return query_final;
	}

	public void setQuery_final(String query_final) {
		this.query_final = query_final;
	}

	public String getFichero_orden() {
		return fichero_orden;
	}

	public void setFichero_orden(String fichero_orden) {
		this.fichero_orden = fichero_orden;
	}

	public String getFichero_cobro() {
		return fichero_cobro;
	}

	public void setFichero_cobro(String fichero_cobro) {
		this.fichero_cobro = fichero_cobro;
	}

	public String getCuenta_bancaria() {
		return cuenta_bancaria;
	}

	public void setCuenta_bancaria(String cuenta_bancaria) {
		this.cuenta_bancaria = cuenta_bancaria;
	}

	public Date getAudit_date() {
		return audit_date;
	}

	public void setAudit_date(Date audit_date) {
		this.audit_date = audit_date;
	}

	public String getQuery_bloqueo() {
		return query_bloqueo;
	}

	public void setQuery_bloqueo(String query_bloqueo) {
		this.query_bloqueo = query_bloqueo;
	}

	public BigDecimal getTolerancia() {
		return tolerancia;
	}

	public void setTolerancia(BigDecimal tolerancia) {
		this.tolerancia = tolerancia;
	}

	public boolean isGrabaDetalleDebe() {
		return 'D' == getGrabaDetalle();
	}

	public boolean isGrabaDetalleHaber() {
		return 'H' == getGrabaDetalle();
	}

	public boolean isGrabaAmbosDetalles() {
		return 'A' == getGrabaDetalle();
	}

	public String getEstado_bloqueado() {
		return estado_bloqueado;
	}

	public void setEstado_bloqueado(String estado_bloqueado) {
		this.estado_bloqueado = estado_bloqueado;
	}

	public int getCantidadCuentas() {

		String[] cuentas = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(getCuenta_debe(), ",");
		return cuentas.length;
		
	}

	public String getQuery_apunte_detalle() {
		return query_apunte_detalle;
	}

	public void setQuery_apunte_detalle(String query_apunte_detalle) {
		this.query_apunte_detalle = query_apunte_detalle;
	}
	
	public String getFicheroConValor() {
		if (!StringUtils.isEmpty(this.fichero)) {
			return this.fichero;
		} else {
			return "";
		}
	}
	
	/**
	 *	Retorna true si el tipo de plantilla es COBRO.
	 *	@return boolean 
	 */
	public boolean isTipoPlantillaCobro() {
		return this.tipo != null 
			&& tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.COBRO.getTipo());
	}

	/**
	 *	Retorna true si el tipo de plantilla es COBRO BARRIDOS.
	 *	@return boolean 
	 */
	public boolean isTipoPlantillaCobroBarridos() {
		return this.tipo != null 
			&& tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.COBRO_BARRIDOS.getTipo());
	}

	/**
	 *	Retorna true si el tipo de plantilla es LIQUIDACION.
	 *	@return boolean 
	 */
	public boolean isTipoPlantillaLiquidacion() {
		return this.tipo != null 
			&& (tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.LIQUIDACION_PARCIAL.getTipo()) 
					|| tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.LIQUIDACION_TOTAL.getTipo()));
	}
	
	/**
	 *	Retorna true si el tipo de plantilla es LIQUIDACION MERCADO.
	 *	@return boolean 
	 */
	public boolean isTipoPlantillaLiquidacionTotal() {
		return this.tipo != null 
			&& tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.LIQUIDACION_TOTAL.getTipo());
	}
	
	/**
	 *	Retorna true si el tipo de plantilla es LIQUIDACION CLIENTE.
	 *	@return boolean 
	 */
	public boolean isTipoPlantillaLiquidacionParcial() {
		return this.tipo != null 
			&& tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.LIQUIDACION_PARCIAL.getTipo());
	}
	
	/**
	 *	Retorna true si el tipo de plantilla es LIQUIDACION FALLIDOS.
	 *	@return boolean 
	 */
	public boolean isTipoPlantillaLiquidacionFallidos() {
		return this.tipo != null 
			&& tipo.equalsIgnoreCase(SibbacEnums.TipoContaPlantillas.LIQUIDACION_FALLIDOS.getTipo());
	}

	public String getQuery_desbloqueo() {
		
		String query_desbloqueo = "";
		
		if(!StringUtils.isEmpty(query_bloqueo)) {
			query_desbloqueo = query_bloqueo.replace("'DEVENGADA PDTE. COBRO') est2 on est2.idestado=alc.cdestadocont",
					"'" + estado_bloqueado + "') est2 on est2.idestado=alc.cdestadocont");
			query_desbloqueo = query_bloqueo.replace(
					"alc.cdestadocont and est2.nombre in ('PDTE. DEVENGO','PDTE. AJUSTE')",
					"alc.cdestadocont and est2.nombre in ('" + estado_bloqueado + "')");
			query_desbloqueo = query_bloqueo.replace(
					"est2.idestado=alc.cdestadocont and est2.nombre in ('PDTE. DEVENGO')",
					"est2.idestado=alc.cdestadocont and est2.nombre in ('" + estado_bloqueado + "')");
			query_desbloqueo = query_bloqueo.replace("fall.estadocont and est2.nombre = 'PDTE. DEVENGO'",
					"fall.estadocont and est2.nombre = '" + estado_bloqueado + "'");
		}
		
		return query_desbloqueo;
	}

	@Override
	public String toString() {
		return "Tmct0ContaPlantillas [id=" + id + ", codigo=" + codigo + ", tipo=" + tipo + ", subtipo=" + subtipo
				+ ", proceso=" + proceso + ", subproceso=" + subproceso + ", activo=" + activo + ", fichero=" + fichero
				+ ", tipo_comprobante=" + tipo_comprobante + ", num_comprobante=" + num_comprobante + ", campo=" + campo
				+ ", campo_estado=" + campo_estado + ", estado_inicio=" + estado_inicio + ", estado_final="
				+ estado_final + ", cuenta_debe=" + cuenta_debe + ", aux_debe=" + aux_debe + ", cuenta_haber="
				+ cuenta_haber + ", aux_haber=" + aux_haber + ", cuenta_diferencias=" + cuenta_diferencias
				+ ", query_orden=" + query_orden + ", query_cobro=" + query_cobro + ", query_final=" + query_final
				+ ", fichero_orden=" + fichero_orden + ", fichero_cobro=" + fichero_cobro + ", cuenta_bancaria="
				+ cuenta_bancaria + ", audit_date=" + audit_date + ", query_bloqueo=" + query_bloqueo
				+ ", query_apunte_detalle=" + query_apunte_detalle + ", tolerancia=" + tolerancia + ", grabaDetalle="
				+ grabaDetalle + ", estado_bloqueado=" + estado_bloqueado + ", estado_parcial=" + estado_parcial + "]";
	}

}
