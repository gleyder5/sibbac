package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.utils.DateHelper;

/**
 * DTO para representar datos de la grilla TLM
 */
public class FiltroGrillaTLMDTO {

	private List<SelectDTO> tiposMovimientos;
	private List<SelectDTO> auxiliaresBancarios;
	private List<SelectDTO> conceptosMovimientos;
	private List<SelectDTO> numerosDocumentos;
	private List<SelectDTO> comentarios;
	private String operadorConceptoMovimiento;
	private String operadorComentario;
	private String operadorNumeroDocumento;
	private List<Date> listaBookingDate;
	private List<Date> listaValueDate;
	private BigDecimal importeDesde;
	private BigDecimal importeHasta;
	private List<String> listAantiguedad;
	private BigInteger gin;

	/**
	 * Constructor no-arg.
	 */
	public FiltroGrillaTLMDTO() {
		super();
	}

	public FiltroGrillaTLMDTO(List<SelectDTO> tiposMovimientos, List<SelectDTO> auxiliaresBancarios,
			List<SelectDTO> conceptosMovimientos, String operadorConceptoMovimiento, List<SelectDTO> numerosDocumentos, 
			String operadorNumeroDocumento, List<SelectDTO> comentarios, String operadorComentario,
			List<Date> listaBookingDate, List<Date> listaValueDate, String importeDesde, String importeHasta,List<String> listAantiguedad,
			BigInteger gin) {

		super();
		this.tiposMovimientos = tiposMovimientos;
		this.auxiliaresBancarios = auxiliaresBancarios;
		this.conceptosMovimientos = conceptosMovimientos;
		this.operadorConceptoMovimiento = operadorConceptoMovimiento;
		this.numerosDocumentos = numerosDocumentos;
		this.operadorNumeroDocumento = operadorNumeroDocumento;
		this.comentarios = comentarios;
		this.operadorComentario = operadorComentario;
		this.listaBookingDate = listaBookingDate;
		this.listaValueDate = listaValueDate;
		if (!StringUtils.isEmpty(importeDesde))
			this.importeDesde = new BigDecimal(importeDesde);
		if (!StringUtils.isEmpty(importeHasta))
			this.importeHasta = new BigDecimal(importeHasta);
		this.listAantiguedad=listAantiguedad;
		this.gin=gin;
	}

	/**
	 * @return the listAantiguedad
	 */
	public List<String> getListAantiguedad() {
		return listAantiguedad;
	}

	/**
	 * @param listAantiguedad the listAantiguedad to set
	 */
	public void setListAantiguedad(List<String> listAantiguedad) {
		this.listAantiguedad = listAantiguedad;
	}

	/**
	 * @return the gin
	 */
	public BigInteger getGin() {
		return gin;
	}

	/**
	 * @param gin the gin to set
	 */
	public void setGin(BigInteger gin) {
		this.gin = gin;
	}

	public String getOperadorConceptoMovimiento() {
		return operadorConceptoMovimiento;
	}

	public void setOperadorConceptoMovimiento(String operadorConceptoMovimiento) {
		this.operadorConceptoMovimiento = operadorConceptoMovimiento;
	}

	public String getOperadorComentario() {
		return operadorComentario;
	}

	public void setOperadorComentario(String operadorComentario) {
		this.operadorComentario = operadorComentario;
	}

	public List<Date> getListaBookingDate() {
		return this.listaBookingDate;
	}

	public void setListaBookingDate(List<Date> listaBookingDate) {
		this.listaBookingDate = listaBookingDate;
	}

	public List<Date> getListaValueDate() {
		return this.listaValueDate;
	}

	public void setListaValueDate(List<Date> listaValueDate) {
		this.listaValueDate = listaValueDate;
	}

	public BigDecimal getImporteDesde() {
		return importeDesde;
	}

	public void setImporteDesde(BigDecimal importeDesde) {
		this.importeDesde = importeDesde;
	}

	public BigDecimal getImporteHasta() {
		return importeHasta;
	}

	public void setImporteHasta(BigDecimal importeHasta) {
		this.importeHasta = importeHasta;
	}

	public List<SelectDTO> getTiposMovimientos() {
		return this.tiposMovimientos;
	}

	public void setTiposMovimientos(List<SelectDTO> tiposMovimientos) {
		this.tiposMovimientos = tiposMovimientos;
	}

	public List<SelectDTO> getAuxiliaresBancarios() {
		return this.auxiliaresBancarios;
	}

	public void setAuxiliaresBancarios(List<SelectDTO> auxiliaresBancarios) {
		this.auxiliaresBancarios = auxiliaresBancarios;
	}

	public List<SelectDTO> getConceptosMovimientos() {
		return this.conceptosMovimientos;
	}

	public void setConceptosMovimientos(List<SelectDTO> conceptosMovimientos) {
		this.conceptosMovimientos = conceptosMovimientos;
	}
	
	public List<SelectDTO> getComentarios() {
		return this.comentarios;
	}

	public void setComentarios(List<SelectDTO> comentarios) {
		this.comentarios = comentarios;
	}

	public List<SelectDTO> getNumerosDocumentos() {
		return this.numerosDocumentos;
	}

	public void setNumerosDocumentos(List<SelectDTO> numerosDocumentos) {
		this.numerosDocumentos = numerosDocumentos;
	}

	public String getOperadorNumeroDocumento() {
		return this.operadorNumeroDocumento;
	}

	public void setOperadorNumeroDocumento(String operadorNumeroDocumento) {
		this.operadorNumeroDocumento = operadorNumeroDocumento;
	}
	
}
