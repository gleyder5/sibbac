/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dto.assembler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;

/**
 * @author lucio.vilar
 *
 */
@Service
public class Tmct0ContaOurAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaOurAssembler.class);

	public Tmct0ContaOurAssembler() {
		super();
	}

	public Tmct0ContaOurDTO getTmct0ContaOurDTO(Tmct0ContaOur entidad) throws Exception {
		LOG.info("INICIO - ASSEMBLER - getTmct0ContaOurDTO");
		Tmct0ContaOurDTO dto = new Tmct0ContaOurDTO();

		try {
			dto.setNbooking(entidad.getnBooking());
			dto.setNucnfclt(entidad.getNuCnfclt());
			dto.setNucnfliq(entidad.getNuCnfliq());
			dto.setNuorden(entidad.getNuOrden());
			dto.setImporte(entidad.getImporte());
			dto.setSentido(entidad.getSentido());
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaOurDTO -  " + e.getMessage(), e);
			throw (e);
		}
		LOG.info("FINAL - ASSEMBLER - getTmct0ContaOurDTO");
		return dto;
	}

}
