package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


/**
 * Data Transfer Object para cada fila del fichero de TLM.
 */
public class FicheroTLMDTO {

	private String account;
	private Date bookingDate;
	private Date valueDate;
	private String currency;
	private BigDecimal amount;
	private String itemType;
	private String comment;
	private String deparment;
	private String bookingText2;
	private String ourReference1;
	private String theirReference2;
	private String bookingText1;
	private String status;
	private String notes;
	private Integer matchNo;
	private BigInteger gin;
	private String commentUser;
	private String lado;
	private String signo;
	private BigDecimal importeSigno;
	private Integer antiguedad;
	private String franja;
	private Date fecha;
	
	/**
	 * 	Constructor no-arg.
	 */
	public FicheroTLMDTO() {
		super();
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getBookingDate() {
		return this.bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getValueDate() {
		return this.valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getItemType() {
		return this.itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDeparment() {
		return this.deparment;
	}

	public void setDeparment(String deparment) {
		this.deparment = deparment;
	}

	public String getBookingText2() {
		return this.bookingText2;
	}

	public void setBookingText2(String bookingText2) {
		this.bookingText2 = bookingText2;
	}

	public String getOurReference1() {
		return this.ourReference1;
	}

	public void setOurReference1(String ourReference1) {
		this.ourReference1 = ourReference1;
	}

	public String getTheirReference2() {
		return this.theirReference2;
	}

	public void setTheirReference2(String theirReference2) {
		this.theirReference2 = theirReference2;
	}

	public String getBookingText1() {
		return this.bookingText1;
	}

	public void setBookingText1(String bookingText1) {
		this.bookingText1 = bookingText1;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getMatchNo() {
		return this.matchNo;
	}

	public void setMatchNo(Integer matchNo) {
		this.matchNo = matchNo;
	}

	public BigInteger getGin() {
		return this.gin;
	}

	public void setGin(BigInteger gin) {
		this.gin = gin;
	}

	public String getCommentUser() {
		return this.commentUser;
	}

	public void setCommentUser(String commentUser) {
		this.commentUser = commentUser;
	}

	public String getLado() {
		return this.lado;
	}

	public void setLado(String lado) {
		this.lado = lado;
	}

	public String getSigno() {
		return this.signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

	public BigDecimal getImporteSigno() {
		return this.importeSigno;
	}

	public void setImporteSigno(BigDecimal importeSigno) {
		this.importeSigno = importeSigno;
	}

	public Integer getAntiguedad() {
		return this.antiguedad;
	}

	public void setAntiguedad(Integer antiguedad) {
		this.antiguedad = antiguedad;
	}

	public String getFranja() {
		return this.franja;
	}

	public void setFranja(String franja) {
		this.franja = franja;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
}