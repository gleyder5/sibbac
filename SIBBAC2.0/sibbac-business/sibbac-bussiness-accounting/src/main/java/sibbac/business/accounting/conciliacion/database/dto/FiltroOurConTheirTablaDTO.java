package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO para representar datos de la tabla de Our con Their
 */
public class FiltroOurConTheirTablaDTO {
	
	private String auxContable; 
	private String sentido;
	private String codIsinNombIsin;
	private BigDecimal impOurPteGenerar;
	private Date fhContratacion;
	private Date fhLiquidacion;
	private String nBooking;
	private String nDesglose;
	private BigDecimal secDesglose;
	private String refCliente;
	private String refS3;
	private String codigoPlantilla;
	private String nuOrden;

	/**
	 * Constructor no-arg.
	 */
	public FiltroOurConTheirTablaDTO() {
		super();
	}

	public FiltroOurConTheirTablaDTO(String auxContable,String sentido,
			String codIsinNombIsin,BigDecimal impOurPteGenerar,Date fhContratacion,
			Date fhLiquidacion,String nBooking,String nDesglose,
			BigDecimal secDesglose,String refCliente,String refS3,String codigoPlantilla,String nuOrden) {
		super();
		this.auxContable=auxContable;
		this.sentido=sentido;
		this.codIsinNombIsin=codIsinNombIsin;
		this.impOurPteGenerar=impOurPteGenerar;
		this.fhContratacion=fhContratacion;
		this.fhLiquidacion=fhLiquidacion;
		this.nBooking=nBooking;
		this.nDesglose=nDesglose;
		this.secDesglose=secDesglose;
		this.refCliente=refCliente;
		this.refS3=refS3;
		this.codigoPlantilla=codigoPlantilla;
		this.nuOrden=nuOrden;
	}

	/**
	 * @return the nuOrden
	 */
	public String getNuOrden() {
		return nuOrden;
	}

	/**
	 * @param nuOrden the nuOrden to set
	 */
	public void setNuOrden(String nuOrden) {
		this.nuOrden = nuOrden;
	}

	/**
	 * @return the codigoPlantilla
	 */
	public String getCodigoPlantilla() {
		return codigoPlantilla;
	}

	/**
	 * @param codigoPlantilla the codigoPlantilla to set
	 */
	public void setCodigoPlantilla(String codigoPlantilla) {
		this.codigoPlantilla = codigoPlantilla;
	}

	/**
	 * @return the auxContable
	 */
	public String getAuxContable() {
		return auxContable;
	}

	/**
	 * @param auxContable the auxContable to set
	 */
	public void setAuxContable(String auxContable) {
		this.auxContable = auxContable;
	}

	/**
	 * @return the sentido
	 */
	public String getSentido() {
		return sentido;
	}

	/**
	 * @param sentido the sentido to set
	 */
	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	/**
	 * @return the codIsinNombIsin
	 */
	public String getCodIsinNombIsin() {
		return codIsinNombIsin;
	}

	/**
	 * @param codIsinNombIsin the codIsinNombIsin to set
	 */
	public void setCodIsinNombIsin(String codIsinNombIsin) {
		this.codIsinNombIsin = codIsinNombIsin;
	}

	/**
	 * @return the impOurPteGenerar
	 */
	public BigDecimal getImpOurPteGenerar() {
		return impOurPteGenerar;
	}

	/**
	 * @param impOurPteGenerar the impOurPteGenerar to set
	 */
	public void setImpOurPteGenerar(BigDecimal impOurPteGenerar) {
		this.impOurPteGenerar = impOurPteGenerar;
	}

	/**
	 * @return the fhContratacion
	 */
	public Date getFhContratacion() {
		return fhContratacion;
	}

	/**
	 * @param fhContratacion the fhContratacion to set
	 */
	public void setFhContratacion(Date fhContratacion) {
		this.fhContratacion = fhContratacion;
	}

	/**
	 * @return the fhLiquidacion
	 */
	public Date getFhLiquidacion() {
		return fhLiquidacion;
	}

	/**
	 * @param fhLiquidacion the fhLiquidacion to set
	 */
	public void setFhLiquidacion(Date fhLiquidacion) {
		this.fhLiquidacion = fhLiquidacion;
	}

	/**
	 * @return the nBooking
	 */
	public String getnBooking() {
		return nBooking;
	}

	/**
	 * @param nBooking the nBooking to set
	 */
	public void setnBooking(String nBooking) {
		this.nBooking = nBooking;
	}

	/**
	 * @return the nDesglose
	 */
	public String getnDesglose() {
		return nDesglose;
	}

	/**
	 * @param nDesglose the nDesglose to set
	 */
	public void setnDesglose(String nDesglose) {
		this.nDesglose = nDesglose;
	}

	/**
	 * @return the secDesglose
	 */
	public BigDecimal getSecDesglose() {
		return secDesglose;
	}

	/**
	 * @param secDesglose the secDesglose to set
	 */
	public void setSecDesglose(BigDecimal secDesglose) {
		this.secDesglose = secDesglose;
	}

	/**
	 * @return the refCliente
	 */
	public String getRefCliente() {
		return refCliente;
	}

	/**
	 * @param refCliente the refCliente to set
	 */
	public void setRefCliente(String refCliente) {
		this.refCliente = refCliente;
	}

	/**
	 * @return the refS3
	 */
	public String getRefS3() {
		return refS3;
	}

	/**
	 * @param refS3 the refS3 to set
	 */
	public void setRefS3(String refS3) {
		this.refS3 = refS3;
	}

}