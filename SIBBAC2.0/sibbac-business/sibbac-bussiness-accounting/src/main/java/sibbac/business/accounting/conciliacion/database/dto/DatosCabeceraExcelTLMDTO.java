package sibbac.business.accounting.conciliacion.database.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *	DTO para representar datos de la cabecera del Excel de TLM. 
 */
public class DatosCabeceraExcelTLMDTO {

	private List<ErroresExcelTLMDTO> listaErroresExcelTLMDTO = null; 
	private Map<String, DatosColumnaPartidasPndtsDTO> mapDatosColPartsPndtsDTO = null;
	
	/**
	 * 	Constructor no-arg.
	 */
	public DatosCabeceraExcelTLMDTO() {
		super();
		this.listaErroresExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>();
		this.mapDatosColPartsPndtsDTO = new HashMap<String, DatosColumnaPartidasPndtsDTO>();
	}

	public List<ErroresExcelTLMDTO> getListaErroresExcelTLMDTO() {
		return this.listaErroresExcelTLMDTO;
	}

	public void setListaErroresExcelTLMDTO(
			List<ErroresExcelTLMDTO> listaErroresExcelTLMDTO) {
		this.listaErroresExcelTLMDTO = listaErroresExcelTLMDTO;
	}
	
	public Map<String, DatosColumnaPartidasPndtsDTO> getMapDatosColPartsPndtsDTO() {
		return this.mapDatosColPartsPndtsDTO;
	}

	public void setMapDatosColPartsPndtsDTO(
			Map<String, DatosColumnaPartidasPndtsDTO> mapDatosColPartsPndtsDTO) {
		this.mapDatosColPartsPndtsDTO = mapDatosColPartsPndtsDTO;
	}

	/**
	 *	Devuelve true si la lista de errores no tiene elementos.
	 *	@return boolean  
	 */
	public boolean isEmptyListaErroresExcelTLMDTO() {
		return this.listaErroresExcelTLMDTO == null 
			|| (this.listaErroresExcelTLMDTO != null && this.listaErroresExcelTLMDTO.isEmpty());
	}
	
	/**
	 *	Devuelve true si la lista de datos de columna no tiene elementos.
	 *	@return boolean  
	 */
	public boolean isEmptyListaDatosColPartsPndtsDTO() {
		return this.mapDatosColPartsPndtsDTO == null 
			|| (this.mapDatosColPartsPndtsDTO != null && this.mapDatosColPartsPndtsDTO.isEmpty());
	}
	
}