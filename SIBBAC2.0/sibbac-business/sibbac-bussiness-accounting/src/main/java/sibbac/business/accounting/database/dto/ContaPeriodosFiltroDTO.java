package sibbac.business.accounting.database.dto;

import java.util.Date;

public class ContaPeriodosFiltroDTO {
	
	Integer anio;
	Integer periodo;
	Date fhIniHasta;
	Date fhIniDesde;
	
	
	public Integer getAnio() {
		return anio;
	}
	public void setAnio(Integer anio) {
		this.anio = anio;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public Date getFhIniHasta() {
		return fhIniHasta;
	}
	public void setFhIniHasta(Date fhIniHasta) {
		this.fhIniHasta = fhIniHasta;
	}
	public Date getFhIniDesde() {
		return fhIniDesde;
	}
	public void setFhIniDesde(Date fhIniDesde) {
		this.fhIniDesde = fhIniDesde;
	}
	
	
}
