package sibbac.business.accounting.conciliacion.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConceptoDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaOurDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPlantillasConciliaDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPlantillasConciliaDaoImpl;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0PantallaConciliacionOURDaoImpl;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirDTO;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirTablaDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ApuntesAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ControlContabilidadAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0PantallaConciliacionOURAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPlantillasConcilia;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

/**
 *	Bo para conta partidas concilia. 
 */
@Service
public class Tmct0ContaPlantillasConciliaBo extends AbstractBo<Tmct0ContaPlantillasConcilia, Integer, Tmct0ContaPlantillasConciliaDao> {

	@Autowired
	private Tmct0ContaPlantillasConciliaDaoImpl tmct0ContaPlantillasConciliaImpl;
	
	@Autowired
	private Tmct0PantallaConciliacionOURDaoImpl tmct0PantallaConciliacionOURDaoImpl;

	@Autowired
	Tmct0PantallaConciliacionOURAssembler tmct0PantallaConciliacionOURAssembler;

	@Autowired
	Tmct0ContaPlantillasConciliaDao tmct0ContaPlantillasConciliaDao;

	@Autowired
	Tmct0ContaConceptoDAO tmct0ContaConceptoDAO;
	
	@Autowired
	Tmct0ContaApuntesAuditDao tmct0ContaApuntesAuditDao;
	
	@Autowired
	Tmct0ContaApuntesPndtsDao tmct0ContaApuntesPndtsDao;
	
	@Autowired
	Tmct0ContaPartidasCasadasDAO tmct0ContaPartidasCasadasDAO;

	@Autowired
	Tmct0ContaOurDAO tmct0ContaOurDAO;
	
	@Autowired
	private Tmct0ApuntesAssembler tmct0ApuntesAssembler;
	
	@Autowired
	Tmct0ControlContabilidadAssembler tmct0ControlContabilidadAssembler;
	
	@Value("${accounting.contabilidad.folder.in}")
	private String folderOut;
	
	
	public List<FiltroOurConTheirTablaDTO> getResultadosOurTheir( List<Tmct0ContaPlantillasConcilia> listaPlantillasConcilia, 
										List<Tmct0ContaPlantillasConcilia> listaPlantillasConciliaDef, 
												FiltroOurConTheirDTO filtro) throws Exception{

		// Añadimos a cada WHERE de cada plantilla tantos AND como campos en el filtro hayan sido informados
		List<String> listaWhere = modificarFromPlantillasConcilia(listaPlantillasConciliaDef, filtro);
			
		if (listaPlantillasConcilia.size()>0) {
			// Llamamos al metodo del IMPL que realizará la UNION de todas las querys y las ejecutará
			List<Object> listaOurConTheir = tmct0PantallaConciliacionOURDaoImpl.ejecutarOurConTheir(listaPlantillasConciliaDef, filtro, listaWhere);

			// Transformamos la lista de objetos recuperada en FiltroOurConTheirTablaDTO
			return tmct0PantallaConciliacionOURAssembler.getFiltroOurConTheirTablaDTO(listaOurConTheir);
		}
		
		return new ArrayList<FiltroOurConTheirTablaDTO>();
	}

	public List<String> modificarFromPlantillasConcilia (List<Tmct0ContaPlantillasConcilia> listaPlantillasConcilia, FiltroOurConTheirDTO filtro) {

		// Comprobamos los campos del filtro que han sido rellenados y por cada uno de ellos relleno añadimos una condición AND a la cláusula WHERE de las plantillas

		StringBuilder clausulaFromComun = new StringBuilder("");
		List<String> listWhere = new ArrayList<String>();

		try {

			// El filtro de ALIAS, en la query es el campo ORD.CDCUENTA
			if (null!=filtro.getListAlias() && filtro.getListAlias().size()>0) {
				clausulaFromComun.append("AND ORD.CDCUENTA IN (");
				for (int i=0;i<filtro.getListAlias().size();i++) {
					if (i==filtro.getListAlias().size()-1) {
						clausulaFromComun.append("'"+filtro.getListAlias().get(i)+"'");
					} else {
						clausulaFromComun.append("'"+filtro.getListAlias().get(i)+"',");
					}
				}
				clausulaFromComun.append(") ");
			}

			// El filtro de ISIN, en la query es el campo ORD.CDISIN
			if (null!=filtro.getListIsin() && filtro.getListIsin().size()>0) {
				clausulaFromComun.append("AND ORD.CDISIN IN (");
				for (int i=0;i<filtro.getListIsin().size();i++) {
					if (i==filtro.getListIsin().size()-1) {
						clausulaFromComun.append("'"+filtro.getListIsin().get(i)+"'");
					} else {
						clausulaFromComun.append("'"+filtro.getListIsin().get(i)+"',");
					}
				}
				clausulaFromComun.append(") ");
			}

			// El campo NºBOOKING en la query es el campo ALC.NBOOKING
			if (null!=filtro.getnBooking() && !"".equals(filtro.getnBooking())) {
				clausulaFromComun.append("AND ALC.NBOOKING='"+filtro.getnBooking()+"' ");
			}

			// El campo REF.DESGLOSE en la query es el campo ALC.NUCNFCLT
			if (null!=filtro.getRefDesglose() && !"".equals(filtro.getRefDesglose())) {
				clausulaFromComun.append("AND ALC.NUCNFCLT='"+filtro.getRefDesglose()+"' ");
			}

			// El campo REF.CLIENTE en la query es el campo ALO.REF_CLIENTE
			if (null!=filtro.getRefCliente() && !"".equals(filtro.getRefCliente())) {
				clausulaFromComun.append("AND ALO.REF_CLIENTE='"+filtro.getRefCliente()+"' ");

			}

			for (int i=0; i<listaPlantillasConcilia.size();i++) {
				StringBuilder clausulaFromEspecifica = new StringBuilder("");
				// El campo SENTIDO en la query es el campo ORD.CDTPOPER
				// Cuando sea COMPRA o ENTREGA el campo ORD.CDTPOPER tiene que ser una “C”, cuando sea VENTA o RECEPCION el campo ORD.CDTPOPER tiene que ser una “V”
				if (null!=filtro.getSentido() && !"".equals(filtro.getSentido())) {
					// Si el usuario selecciona en este campo COMPRA o VENTA además de la condición del CDTPOPER la plantilla no puede ser de las que comienzan con “LQNET”
					if ((filtro.getSentido().equalsIgnoreCase("COMPRA") || filtro.getSentido().equalsIgnoreCase("VENTA")) || filtro.getSentido().equalsIgnoreCase("COMPRA/VENTA")) {
						if (listaPlantillasConcilia.get(i).getCodigo().startsWith("LQEFE")) {
							if (filtro.getSentido().equalsIgnoreCase("COMPRA")) {
								clausulaFromEspecifica.append("AND ORD.CDTPOPER="+"'C' ");
							} else if (filtro.getSentido().equalsIgnoreCase("VENTA")) {
								clausulaFromEspecifica.append("AND ORD.CDTPOPER="+"'V' ");
							} else if (filtro.getSentido().equalsIgnoreCase("COMPRA/VENTA")) {
								clausulaFromEspecifica.append("AND ORD.CDTPOPER IN ('C','V') ");
							}
						}
					} // Si el usuario selecciona en este campo ENTREGA o RECEPCION además de la condición del CDTPOPER la plantilla tiene que ser de las que comienzan con “LQNET”
					else if ((filtro.getSentido().equalsIgnoreCase("ENTREGA") || filtro.getSentido().equalsIgnoreCase("RECEPCIÓN") || filtro.getSentido().equalsIgnoreCase("ENTREGA/RECEPCIÓN"))) {
						if (listaPlantillasConcilia.get(i).getCodigo().startsWith("LQNET")) {
							if (filtro.getSentido().equalsIgnoreCase("ENTREGA")) {
								clausulaFromEspecifica.append("AND ORD.CDTPOPER="+"'C' ");
							} else if (filtro.getSentido().equalsIgnoreCase("RECEPCIÓN")) {
								clausulaFromEspecifica.append("AND ORD.CDTPOPER="+"'V' ");
							} else if (filtro.getSentido().equalsIgnoreCase("ENTREGA/RECEPCIÓN")) {
								clausulaFromEspecifica.append("AND ORD.CDTPOPER IN ('C','V') ");
							}
						}
					}
					
					if (!listaPlantillasConcilia.get(i).getCodigo().startsWith("LQNET") && !listaPlantillasConcilia.get(i).getCodigo().startsWith("LQEFE")) {
						if (filtro.getSentido().equalsIgnoreCase("COMPRA")) {
							clausulaFromEspecifica.append("AND ORD.CDTPOPER="+"'C' ");
						} else if (filtro.getSentido().equalsIgnoreCase("VENTA")) {
							clausulaFromEspecifica.append("AND ORD.CDTPOPER="+"'V' ");
						} else if (filtro.getSentido().equalsIgnoreCase("COMPRA/VENTA")) {
							clausulaFromEspecifica.append("AND ORD.CDTPOPER IN ('C','V') ");
						}
					}
				}

				// El campo REFERENCIA PATA MERCADO en la query es el campo ALR.REFERENCIAS3.
				// Si rellenan este filtro la plantilla debe ser de las que comienzan con “LIQEFE”.
				if (null!=filtro.getRefPataMercado() && !"".equals(filtro.getRefPataMercado())) {
					if (listaPlantillasConcilia.get(i).getCodigo().startsWith("LIQEFE")) {
						clausulaFromEspecifica.append("AND ALR.REFERENCIAS3='"+filtro.getRefPataMercado()+"' ");
					}
				}

				// El campo REFERENCIA PATA CLIENTE en la query es el campo ALR.REFERENCIAS3.
				// Si rellenan este filtro la plantilla debe ser de las que comienzan con “LQNET”.
				if (null!=filtro.getRefPataCliente() && !"".equals(filtro.getRefPataCliente())) {
					if (listaPlantillasConcilia.get(i).getCodigo().startsWith("LQNET")) {
						clausulaFromEspecifica.append("AND ALR.REFERENCIAS3='"+filtro.getRefPataCliente()+"' ");
					}
				}
				
				// IMPORTES DESDE y HASTA
				if (null!=filtro.getImporteDesde() &&!"".equals(filtro.getImporteDesde())) {
					clausulaFromEspecifica.append("AND ("+listaPlantillasConcilia.get(i).getImporte()+")>="+filtro.getImporteDesde()+" ");
				}
				
				if (null!=filtro.getImporteHasta() &&!"".equals(filtro.getImporteHasta())) {
					clausulaFromEspecifica.append("AND ("+listaPlantillasConcilia.get(i).getImporte()+")<="+filtro.getImporteHasta()+" ");
				}

				if (listaPlantillasConcilia.get(i).getWhereQuery().toUpperCase().trim().startsWith("WHERE")) {
					listWhere.add(listaPlantillasConcilia.get(i).getWhereQuery()+" "+clausulaFromComun+" "+clausulaFromEspecifica);
				} else {
					listWhere.add("WHERE 1=1 "+clausulaFromComun+" "+clausulaFromEspecifica);
				}

			}

		} catch (Exception e) {
			LOG.error("Error metodo getListMultiMapFiltroOurConTheirTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		return listWhere;
	}

	/**
	 *	Devuelve DTO para combos segun el nombre de columna de tabla que se le pase (valores distintos).
	 *	@param columnName
	 *	@return List<SelectDTO> 
	 *	@throws SIBBACBusinessException 
	 */
	public List<SelectDTO> getDTOCombosByColumnName(String columnName) throws SIBBACBusinessException {
		List<SelectDTO> catalogoList = new ArrayList<SelectDTO>();
		try {
			List<String> campos = this.tmct0ContaPlantillasConciliaImpl.getDistinctValuesByColumnName(columnName);
			if (CollectionUtils.isNotEmpty(campos)) {
				for (String campo : campos) {
					catalogoList.add(new SelectDTO(String.valueOf(campo).trim(), String.valueOf(campo).trim()));
				}
			}
		} catch (Exception e) {
			LOG.error("Error metodo getDTOCombosByColumnName -  " + e.getMessage(), e);
			throw (e);
		}
		return catalogoList;
	}

	/**
	 *	Devuelve una lista multimap.
	 *	@return List<SelectDTO> 
	 * @throws Exception 
	 */
	public ListMultimap<String, GrillaTLMDTO> getListMultiMapGrillaTLMDTO(ArrayList<LinkedHashMap> list) throws Exception {
		ListMultimap<String, GrillaTLMDTO> multiMapGrillaTLMDTO = ArrayListMultimap.create();
		try {
			for (int i = 0; i < list.size(); i++) {
				LinkedHashMap map = list.get(i);
				GrillaTLMDTO grillaTLMDTO = tmct0PantallaConciliacionOURAssembler.getGrillaTLMDTO(map);
				multiMapGrillaTLMDTO.put(grillaTLMDTO.getAuxiliarBancario(), grillaTLMDTO);
			}
		} catch (Exception e) {
			LOG.error("Error metodo getListMultiMapGrillaTLMDTO -  " + e.getMessage(), e);
			throw (e);
		}
		return multiMapGrillaTLMDTO;
	}

	/**
	 *	Devuelve una lista multimap.
	 *	@return List<SelectDTO> 
	 * @throws Exception 
	 */
	public ListMultimap<String, FiltroOurConTheirTablaDTO> getListMultiMapFiltroOurConTheirTablaDTO(ArrayList<LinkedHashMap> list) throws Exception {
		ListMultimap<String, FiltroOurConTheirTablaDTO> multiMapFiltroOurConTheirTablaDTO = ArrayListMultimap.create();
		try {
			for (int i = 0; i < list.size(); i++) {
				LinkedHashMap map = list.get(i);
				FiltroOurConTheirTablaDTO filtroOurConTheirTablaDTO = tmct0PantallaConciliacionOURAssembler.getFiltroOurConTheirTablaDTO(map);
				multiMapFiltroOurConTheirTablaDTO.put(filtroOurConTheirTablaDTO.getAuxContable(), filtroOurConTheirTablaDTO);
			}
		} catch (Exception e) {
			LOG.error("Error metodo getListMultiMapFiltroOurConTheirTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}
		return multiMapFiltroOurConTheirTablaDTO;
	}

	/**
	 *	Devuelve true o false.
	 *	@return List<SelectDTO> 
	 * @throws Exception 
	 */
	public boolean compararPyGConTolerance(String codPlantilla, BigDecimal pygTotal) throws Exception {
		boolean esMayorPyG = false;
		try {
			List<Tmct0ContaPlantillasConcilia> plantillaConcilia = tmct0ContaPlantillasConciliaDao.findByCodPlantilla(codPlantilla);
			BigDecimal pygTotalAbsoluto = pygTotal.abs();
			if (null!=plantillaConcilia && plantillaConcilia.size()>0) {
				if (pygTotalAbsoluto.compareTo(new BigDecimal(plantillaConcilia.get(0).getTolerance())) == 1) {
					esMayorPyG = true;
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo compararPyGConTolerance -  " + e.getMessage(), e);
			throw (e);
		}
		return esMayorPyG;
	}

	public Tmct0ContaConcepto comprobarConcepto(LinkedHashMap concepto) throws Exception {
		Tmct0ContaConcepto contaConcepto = new Tmct0ContaConcepto();
		try {
			if (null!=concepto && !"".equals((String)concepto.get("key"))) {
				// Si el concepto existe ya en BBDD
				contaConcepto = tmct0ContaConceptoDAO.findOne(new BigInteger((String)concepto.get("key")));
			} else {
				// Si el concepto no existe en BBDD lo creamos
				contaConcepto.setConcepto((String)concepto.get("value"));
				tmct0ContaConceptoDAO.save(contaConcepto);
			}

		} catch (Exception e) {
			LOG.error("Error metodo comprobarConcepto -  " + e.getMessage(), e);
			throw (e);
		}
		return contaConcepto;
	}
	
}
