package sibbac.business.accounting.conciliacion.database.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesAudit;
import sibbac.business.accounting.database.dao.Tmct0ContaApunteDetalleDaoImp;

@Repository
public class Tmct0ContaApuntesAuditDaoImpl {

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaApunteDetalleDaoImp.class);

	public List<Tmct0ContaApuntesAudit> consultarApuntesAudit(String usuario, List<Date> listFechas) {

		LOG.info("INICIO - DAOIMPL - consultarApuntesAudit");

		List<Tmct0ContaApuntesAudit> listaApuntesAudit = new ArrayList<Tmct0ContaApuntesAudit>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		int sizeFechas = 1;

		try {
			
			if (null!=listFechas) {
				sizeFechas = listFechas.size();
			}

			for (int i=0;i<sizeFechas;i++) {

				List<Tmct0ContaApuntesAudit> listApuntesAudit = new ArrayList<Tmct0ContaApuntesAudit>();

				StringBuilder consulta = new StringBuilder("SELECT apuntAudit FROM Tmct0ContaApuntesAudit apuntAudit "
						+ "WHERE 1=1");
				if (null != usuario) {
					consulta.append(" AND apuntAudit.usuApunte = :usuario");
					parameters.put("usuario", usuario);
				}
				if (null!=listFechas) {
					consulta.append(" AND date(apuntAudit.feApunte) = :feApunte");
					parameters.put("feApunte", listFechas.get(i));
				}

				LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
				TypedQuery<Tmct0ContaApuntesAudit> query = entityManager.createQuery(consulta.toString(),
						Tmct0ContaApuntesAudit.class);

				for (Entry<String, Object> entry : parameters.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}

				// Se forman todas las entidades factura manual con los datos recuperados.
				listApuntesAudit = query.getResultList();

				for (int j=0;j<listApuntesAudit.size();j++) {
					listaApuntesAudit.add(listApuntesAudit.get(j));
				}
			}


		} catch (Exception e) {
			LOG.error("Error metodo consultarApuntesAudit -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarApuntesAudit");

		return listaApuntesAudit;
	}

}
