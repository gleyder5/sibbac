/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author lucio.vilar
 *
 */
public class Tmct0ContaPartidasCasadasDTO {

	
	/**
	 * origen
	 */
	private String referencia;
	
	/**
	 * tipo movimiento
	 */
	private String tipoMovimiento;
	
	/**
	 * auxiliar
	 */
	private String auxiliar;
	
	/**
	 * concepto
	 */
	private String concepto;
	
	/**
	 * importe
	 */
	private BigDecimal importe;
	
	/**
	 * GIN
	 */
	private BigInteger gin;
	
	public Tmct0ContaPartidasCasadasDTO() {
		
	}
	
	public Tmct0ContaPartidasCasadasDTO(String referencia, String unTipoMovimiento, String unAuxiliar, String unConcepto, BigDecimal unImporte, BigInteger gin) {
		this.referencia = referencia;
		this.tipoMovimiento = unTipoMovimiento;
		this.auxiliar = unAuxiliar;
		this.concepto = unConcepto;
		this.importe = unImporte;
		this.gin=gin;
	}

	/**
	 * @return the gin
	 */
	public BigInteger getGin() {
		return gin;
	}

	/**
	 * @param gin the gin to set
	 */
	public void setGin(BigInteger gin) {
		this.gin = gin;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the tipoMovimiento
	 */
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	/**
	 * @param tipoMovimiento the tipoMovimiento to set
	 */
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	/**
	 * @return the auxiliar
	 */
	public String getAuxiliar() {
		return auxiliar;
	}

	/**
	 * @param auxiliar the auxiliar to set
	 */
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
}
