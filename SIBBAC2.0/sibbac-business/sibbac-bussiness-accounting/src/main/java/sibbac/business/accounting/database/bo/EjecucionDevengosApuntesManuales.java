package sibbac.business.accounting.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDaoImp;
import sibbac.business.accounting.database.dto.AlcDTO;
import sibbac.business.accounting.database.dto.ApuntesManualesExcelDTO;
import sibbac.business.accounting.database.dto.CobroDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;

@Service
public class EjecucionDevengosApuntesManuales {
	
	
	 @Autowired
	 Tmct0Norma43MovimientoBo tmct0Norma43MovimientoBo;
	 
	 @Autowired
	 Tmct0ContaPlantillasDaoImp tmct0ContaPlantillasDaoImp;
	 
	 @Autowired
	 Tmct0ContaApuntesBo tmct0ContaApuntesBo;
	 
	 @Autowired
	 Tmct0ContaApunteDetalleBo tmct0ContaApunteDetalleBo;
	 
	
	  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla, 
			  List<ApuntesManualesExcelDTO> listaAMExcelDTO) throws EjecucionPlantillaException {

		  Loggers.logDebugInicioFinAccounting(plantilla != null ? plantilla.getCodigo() : null, true);

		  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();
		  List<Tmct0ContaPlantillasDTO> contaPlantillasDTOs = new ArrayList<Tmct0ContaPlantillasDTO>();
		  
		  String whereOrden = AccountingHelper.concatWhereModificadoByListaALCsAM(listaAMExcelDTO);
		    
		 //	Persistencia para Devengos en apuntes manuales.
		 //	Se ejecutan tambien queries de bloqueo - orden - apunte detalle - desbloqueo
		 this.tmct0ContaApuntesBo.persistirDevengosApuntesManuales(
				 plantilla, whereOrden, listaAMExcelDTO, plantillasSinBalance, contaPlantillasDTOs);

		  return plantillasSinBalance;
	  }
	  
	  
	  /**
	   * Divide las plantillas que tengan mÃ¡s de una cuenta, y en tal caso las marca como dividido=true.
	   * 
	   * @param plantilla Tmct0ContaPlantillas.
	   * @param object Resultado de query orden.
	   * @return Lista de Tmct0ContaPlantillasDTO.
	   * @throws EjecucionPlantillaException EjecucionPlantillaException
	   */
	  public List<Tmct0ContaPlantillasDTO> dividirPlantillasPorCuentasDevengos(Tmct0ContaPlantillas plantilla, Object[] object, String concepto) throws EjecucionPlantillaException {

	    List<Tmct0ContaPlantillasDTO> plantillasDTO = new ArrayList<Tmct0ContaPlantillasDTO>();
	    Tmct0ContaPlantillasDTO contaPlantillasDTO = null;

	    // Se separa por la "," los campos que pueden traer mÃ¡s de un valor por plantilla.
	    String[] cuentasDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_debe(), ",");
	    String[] cuentasHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCuenta_haber(),
	                                                                                        ",");
	    String[] auxsDebe = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_debe(), ",");
	    String[] auxsHaber = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getAux_haber(), ",");

	    String[] campos = org.apache.commons.lang3.StringUtils.splitPreserveAllTokens(plantilla.getCampo(), ",");

	    // En caso de que haya un error de inconsistencia de cuentas y auxiliares se arroja error.
	    if (!AccountingHelper.isPlantillaCuentasAuxCamposConsistentes(cuentasDebe, cuentasHaber, auxsDebe, auxsHaber,
	                                                                  campos)) {
	      Loggers.logErrorAccounting(plantilla.getCodigo(),
	                                 ConstantesError.ERROR_DIVIDIR_PLANTILLAS.getValue() + " : "
	                                     + ConstantesError.ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX.getValue());
	      throw new EjecucionPlantillaException();
	    }

	    int iCuenta = 0;
	    int iImporte = 2;
	    int posEstadoCont = iImporte + cuentasDebe.length;
	    int iAux = iImporte + cuentasDebe.length + 1; // Indice de las cuenta auxiliares.

	    // Map con los valores AUX sacados de la Query
	    Map<String, String> auxMap = new HashMap<String, String>();
	    for (int i = 0; i < cuentasDebe.length; i++) {
	      try {
	        if (!auxsDebe[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsDebe[i])) {
	          if (auxMap.get(auxsDebe[i]) == null) {
	            auxMap.put(auxsDebe[i], (String) object[iAux]);
	            iAux++;
	          }
	        }
	      } catch (Exception e) {
	      }

	      try {
	        if (!auxsHaber[i].substring(0, 1).equals("\"") && !StringUtils.isEmpty(auxsHaber[i])) {
	          if (auxMap.get(auxsHaber[i]) == null) {
	            auxMap.put(auxsHaber[i], (String) object[iAux]);
	            iAux++;
	          }
	        }
	      } catch (Exception e) {
	      }
	    }

	    for (String cuentaDebe : cuentasDebe) {

	      // SafeNull para el importe - ya que la query de orden puede devolver importe NULL
	      if (object[iImporte] == null) {
	        object[iImporte] = BigDecimal.ZERO;
	      }

	      contaPlantillasDTO = new Tmct0ContaPlantillasDTO();
	      contaPlantillasDTO.setActivo(plantilla.getActivo());
	      contaPlantillasDTO.setAudit_date(plantilla.getAudit_date());
	      contaPlantillasDTO.setCampo(StringHelper.quitarComillasDobles(campos[iCuenta]));
	      contaPlantillasDTO.setCampo_estado(plantilla.getCampo_estado());
	      contaPlantillasDTO.setCodigo(plantilla.getCodigo());
	      contaPlantillasDTO.setCuenta_bancaria(plantilla.getCuenta_bancaria());
	      contaPlantillasDTO.setCuenta_debe(StringHelper.quitarComillasDobles(cuentaDebe));
	      contaPlantillasDTO.setCuenta_diferencias(StringHelper.quitarComillasDobles(plantilla.getCuenta_diferencias()));
	      contaPlantillasDTO.setCuenta_haber(StringHelper.quitarComillasDobles(cuentasHaber[iCuenta]));
	      contaPlantillasDTO.setEstado_final(plantilla.getEstado_final());
	      contaPlantillasDTO.setEstado_inicio(plantilla.getEstado_inicio());
	      contaPlantillasDTO.setFichero(plantilla.getFichero());
	      contaPlantillasDTO.setFichero_cobro(plantilla.getFichero_cobro());
	      contaPlantillasDTO.setFichero_orden(plantilla.getFichero_orden());
	      contaPlantillasDTO.setId(plantilla.getId());
	      contaPlantillasDTO.setNum_comprobante(plantilla.getNum_comprobante());
	      contaPlantillasDTO.setProceso(plantilla.getProceso());
	      contaPlantillasDTO.setQuery_bloqueo(plantilla.getQuery_bloqueo());
	      contaPlantillasDTO.setQuery_cobro(plantilla.getQuery_cobro());
	      contaPlantillasDTO.setQuery_final(plantilla.getQuery_final());
	      contaPlantillasDTO.setQuery_orden(plantilla.getQuery_orden());
	      contaPlantillasDTO.setSubproceso(plantilla.getSubproceso());
	      contaPlantillasDTO.setSubtipo(plantilla.getSubtipo());
	      contaPlantillasDTO.setTipo(plantilla.getTipo());
	      contaPlantillasDTO.setTipo_comprobante(plantilla.getTipo_comprobante());
	      contaPlantillasDTO.setImporte((BigDecimal) object[iImporte]);
	      contaPlantillasDTO.setFecha_comprobante((Date) object[0]);
	      contaPlantillasDTO.setPeriodo_comprobante(((Integer) object[1]).toString());
	      contaPlantillasDTO.setConcepto(concepto);
	      contaPlantillasDTO.setAux_debe(AccountingHelper.getAux(auxsDebe, auxMap, iCuenta));
	      contaPlantillasDTO.setAux_haber(AccountingHelper.getAux(auxsHaber, auxMap, iCuenta));

	      if (contaPlantillasDTO.getAux_debe() == null)
	        contaPlantillasDTO.setAux_debe("");
	      if (contaPlantillasDTO.getAux_haber() == null)
	        contaPlantillasDTO.setAux_haber("");

	      plantillasDTO.add(contaPlantillasDTO);

	      iCuenta++;
	      iImporte++;
	    }

	    return plantillasDTO;
	  }

}
