package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_CONTA_PARTIDAS_PNDTS.
 */
@Entity
@Table(name = "TMCT0_CONTA_PARTIDAS_PNDTS")
public class Tmct0ContaPartidasPndts {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	@Column(name = "AUX_BANCARIO", length = 10, nullable = true)
	private String auxBancario;
	
	@Column(name = "BOOKING_DATE", length = 10, nullable = true)
	private Date bookingDate;
	
	@Column(name = "VALUE_DATE", length = 10, nullable = true)
	private Date valueDate;
	
	@Column(name = "CURRENCY", length = 10, nullable = true)
	private String currency;
	
	@Column(name = "TIPO", length = 100, nullable = true)
	private String tipo;
	
	@Column(name = "COMENTARIO", length = 500, nullable = true)
	private String comentario;

	@Column(name = "DEPARTAMENTO", length = 200, nullable = true)
	private String departamento;
	
	@Column(name = "REFERENCIA", length = 100, nullable = true)
	private String referencia;
	
	@Column(name = "REFERENCIA2", length = 100, nullable = true)
	private String referencia2;
	
	@Column(name = "NUM_DOCUMENTO", length = 10, nullable = true)
	private String numDocumento;
	
	@Column(name = "NUM_DOCUMENTO2", length = 10, nullable = true)
	private String numDocumento2;
	
	@Column(name = "GIN", length = 8, nullable = true)
	private BigInteger gin;
	
	@Column(name = "COMENTARIO_USUARIO", length = 500, nullable = true)
	private String comentarioUsuario;
	
	@Column(name = "ANTIGUEDAD", length = 10, nullable = true)
	private String antiguedad;
	
	@Column(name = "IMPORTE", nullable = true, precision = 18, scale = 4)
	private BigDecimal importe;
	
	@Column(name = "ESTADO", columnDefinition="SMALLINT")
	private Integer	estado;
	
	@Column(name = "AUDIT_DATE", length = 10, nullable = true)
	private Date auditDate;

	@Column(name = "AUDIT_USER", length = 255, nullable = true)
	private String auditUser;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaPartidasPndts() {
		super();
	}

	/**
	 *	Constructor args.
	 *	@param id
	 *	@param auxBancario
	 *	@param bookingDate
	 *	@param valueDate  
	 *	@param currency  
	 *	@param tipo	 
	 *	@param comentario  
	 *	@param departamento	 
	 *	@param referencia  
	 *	@param referencia2
	 *	@param numDocumento	 
	 *	@param numDocumento2
	 *	@param gin
	 *	@param comentarioUsuario
	 *	@param antiguedad
	 *	@param importe
	 *	@param estado
	 *	@param auditDate
	 *	@param auditUser
	 */
	public Tmct0ContaPartidasPndts(BigInteger id, String auxBancario,
			Date bookingDate, Date valueDate, String currency,
			String tipo, String comentario, String departamento,
			String referencia, String referencia2, String numDocumento,
			String numDocumento2, BigInteger gin, String comentarioUsuario,
			String antiguedad, BigDecimal importe, Integer estado,
			Date auditDate, String auditUser) {
		super();
		this.id = id;
		this.auxBancario = auxBancario;
		this.bookingDate = bookingDate;
		this.valueDate = valueDate;
		this.currency = currency;
		this.tipo = tipo;
		this.comentario = comentario;
		this.departamento = departamento;
		this.referencia = referencia;
		this.referencia2 = referencia2;
		this.numDocumento = numDocumento;
		this.numDocumento2 = numDocumento2;
		this.gin = gin;
		this.comentarioUsuario = comentarioUsuario;
		this.antiguedad = antiguedad;
		this.importe = importe;
		this.estado = estado;
		this.auditDate = auditDate;
		this.auditUser = auditUser;
	}	

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getAuxBancario() {
		return this.auxBancario;
	}

	public void setAuxBancario(String auxBancario) {
		this.auxBancario = auxBancario;
	}

	public Date getBookingDate() {
		return this.bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getValueDate() {
		return this.valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getReferencia() {
		return this.referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getReferencia2() {
		return this.referencia2;
	}

	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	public String getNumDocumento() {
		return this.numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public String getNumDocumento2() {
		return this.numDocumento2;
	}

	public void setNumDocumento2(String numDocumento2) {
		this.numDocumento2 = numDocumento2;
	}

	public BigInteger getGin() {
		return this.gin;
	}

	public void setGin(BigInteger gin) {
		this.gin = gin;
	}

	public String getComentarioUsuario() {
		return this.comentarioUsuario;
	}

	public void setComentarioUsuario(String comentarioUsuario) {
		this.comentarioUsuario = comentarioUsuario;
	}

	public String getAntiguedad() {
		return this.antiguedad;
	}

	public void setAntiguedad(String antiguedad) {
		this.antiguedad = antiguedad;
	}

	public BigDecimal getImporte() {
		return this.importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getAuditDate() {
		return this.auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getAuditUser() {
		return this.auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}
	
}
