package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirDTO;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirTablaDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.Tmct0ContaOurApuntesDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaOur;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.business.wrappers.helper.Helper;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_CONTA_APUNTES_PNDTS
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0PantallaConciliacionOURAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0PantallaConciliacionOURAssembler.class);

	@Autowired
	private AliasDao aliasDao;

	@Autowired
	Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

	public Tmct0PantallaConciliacionOURAssembler() {
		super();
	}

	/**
	 * Realiza la conversion entre los parámetros del filtro y el dto FiltroOurConTheirDTO
	 * 
	 * @param paramObjects -> mapa con los datos del filtro
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public FiltroOurConTheirDTO getFiltroOurConTheirDTO(Map<String, Object> paramObjects, Map<String, String> filters) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getFiltroOurConTheirDTO");

		FiltroOurConTheirDTO filtroOurConTheirDTO = new FiltroOurConTheirDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 

		try {
			if (null!=paramObjects) {

				if (null != paramObjects.get("importeDesde")) {
					if (paramObjects.get("importeDesde") instanceof Double) {
						filtroOurConTheirDTO.setImporteDesde(BigDecimal.valueOf((Double) paramObjects.get("importeDesde")));
					} else if (paramObjects.get("importeDesde") instanceof Integer){
						filtroOurConTheirDTO.setImporteDesde(BigDecimal.valueOf((Integer) paramObjects.get("importeDesde")));
					} else {
						filtroOurConTheirDTO.setImporteDesde(null);
					}
				}

				if (null != paramObjects.get("importeHasta")) {
					if (paramObjects.get("importeHasta") instanceof Double) {
						filtroOurConTheirDTO.setImporteHasta(BigDecimal.valueOf((Double) paramObjects.get("importeHasta")));
					} else if (paramObjects.get("importeHasta") instanceof Integer){
						filtroOurConTheirDTO.setImporteHasta(BigDecimal.valueOf((Integer) paramObjects.get("importeHasta")));
					} else {
						filtroOurConTheirDTO.setImporteHasta(null);
					}
				}

				if (null!=paramObjects.get("aliasOur") && ((List<String>)paramObjects.get("aliasOur")).size()>0) {
					ArrayList<LinkedHashMap> arrayAlias = new ArrayList<LinkedHashMap>();
					arrayAlias = (ArrayList<LinkedHashMap>) paramObjects.get("aliasOur");
					List<String> listaAlias = new ArrayList<String>();
					for (int i=0;i<arrayAlias.size();i++) {
						listaAlias.add((String)((LinkedHashMap)arrayAlias.get(i)).get("key"));
					}
					filtroOurConTheirDTO.setListAlias(listaAlias);
				}
				if (null!=paramObjects.get("auxiliaresOur") && ((List<String>)paramObjects.get("auxiliaresOur")).size()>0) {
					filtroOurConTheirDTO.setListAux((List<String>)paramObjects.get("auxiliaresOur"));
				} else {
					filtroOurConTheirDTO.setListAux((List<String>)paramObjects.get("auxiliaresTLMOur"));
				}
				if (null!=paramObjects.get("isinOur") && ((List<String>)paramObjects.get("isinOur")).size()>0) {
					ArrayList<LinkedHashMap> arrayIsin = new ArrayList<LinkedHashMap>();
					arrayIsin = (ArrayList<LinkedHashMap>) paramObjects.get("isinOur");
					List<String> listaIsin = new ArrayList<String>();
					for (int i=0;i<arrayIsin.size();i++) {
						listaIsin.add((String)((LinkedHashMap)arrayIsin.get(i)).get("key"));
					}
					filtroOurConTheirDTO.setListIsin(listaIsin);
				}
				if (null!=paramObjects.get("nBooking")) {
					filtroOurConTheirDTO.setnBooking((String)paramObjects.get("nBooking"));
				}
				if (null!=paramObjects.get("refCliente")) {
					filtroOurConTheirDTO.setRefCliente((String)paramObjects.get("refCliente"));
				}
				if (null!=paramObjects.get("refDesglose")) {
					filtroOurConTheirDTO.setRefDesglose((String)paramObjects.get("refDesglose"));
				}
				if (null!=paramObjects.get("refPataCliente")) {
					filtroOurConTheirDTO.setRefPataCliente((String)paramObjects.get("refPataCliente"));
				}
				if (null!=paramObjects.get("refPataMercado")) {
					filtroOurConTheirDTO.setRefPataMercado((String)paramObjects.get("refPataMercado"));
				}
				if (null!=paramObjects.get("sentidoFilterOur")) {
					filtroOurConTheirDTO.setSentido((String) paramObjects.get("sentidoFilterOur"));
				}
				if (null!=paramObjects.get("fhContratacionDesde") && !"".equals(paramObjects.get("fhContratacionDesde"))) {
						filtroOurConTheirDTO.setListFechasContratacion(Helper.separarFechas(formatter.parse(String.valueOf(filters.get("fecha"))),formatter.parse(String.valueOf(filters.get("fecha")))));

				} else if (null!=paramObjects.get("fhLiquidacionDesde") && !"".equals(paramObjects.get("fhLiquidacionDesde"))) {
						filtroOurConTheirDTO.setListFechasLiquidacion(Helper.separarFechas(formatter.parse(String.valueOf(filters.get("fecha"))),formatter.parse(String.valueOf(filters.get("fecha")))));
				}

			}

		} catch (Exception e) {
			LOG.error("Error metodo getFiltroOurConTheirDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getFiltroOurConTheirDTO");

		return filtroOurConTheirDTO;

	}

	/**
	 * Realiza la conversion entre una lista de objetos y FiltroOurConTheirTablaDTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public List<FiltroOurConTheirTablaDTO> getFiltroOurConTheirTablaDTO(List<Object> listObject) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getFiltroOurConTheirTablaDTO");

		List<FiltroOurConTheirTablaDTO> listFiltroOurConTheirTablaDTO = new ArrayList<FiltroOurConTheirTablaDTO>();
		try {

			for (int i=0;i<listObject.size();i++) {
				FiltroOurConTheirTablaDTO datosOurConTheir = new FiltroOurConTheirTablaDTO();
				datosOurConTheir.setAuxContable((String)((Object[])listObject.get(i))[0]);
				if ('V'==((Character)((Object[])listObject.get(i))[1]) && ((String)((Object[])listObject.get(i))[12]).startsWith("LQEFE")) {
					datosOurConTheir.setSentido("VENTA");
				} else if ('C'==((Character)((Object[])listObject.get(i))[1]) && ((String)((Object[])listObject.get(i))[12]).startsWith("LQEFE")) {
					datosOurConTheir.setSentido("COMPRA");
				} else if ('V'==((Character)((Object[])listObject.get(i))[1]) && ((String)((Object[])listObject.get(i))[12]).startsWith("LQNET")) {
					datosOurConTheir.setSentido("RECEPCION");
				} else if ('C'==((Character)((Object[])listObject.get(i))[1]) && ((String)((Object[])listObject.get(i))[12]).startsWith("LQNET")) {
					datosOurConTheir.setSentido("ENTREGA");
				} else if ('C'==((Character)((Object[])listObject.get(i))[1]) && !((String)((Object[])listObject.get(i))[12]).startsWith("LQNET") && !((String)((Object[])listObject.get(i))[12]).startsWith("LQEFE")) {
					datosOurConTheir.setSentido("COMPRA");
				} else if ('V'==((Character)((Object[])listObject.get(i))[1]) && !((String)((Object[])listObject.get(i))[12]).startsWith("LQNET") && !((String)((Object[])listObject.get(i))[12]).startsWith("LQEFE")) {
					datosOurConTheir.setSentido("VENTA");
				} else {
					datosOurConTheir.setSentido("");
				}
				datosOurConTheir.setCodIsinNombIsin((String)((Object[])listObject.get(i))[2]);
				datosOurConTheir.setImpOurPteGenerar((BigDecimal)((Object[])listObject.get(i))[3]);
				datosOurConTheir.setFhContratacion((Date)((Object[])listObject.get(i))[4]);
				datosOurConTheir.setFhLiquidacion((Date)((Object[])listObject.get(i))[5]);
				datosOurConTheir.setNuOrden((String)((Object[])listObject.get(i))[6]);
				datosOurConTheir.setnBooking((String)((Object[])listObject.get(i))[7]);
				datosOurConTheir.setnDesglose((String)((Object[])listObject.get(i))[8]);
				datosOurConTheir.setSecDesglose((BigDecimal)((Object[])listObject.get(i))[9]);
				if (null!=((Object[])listObject.get(i))[10]) {
					datosOurConTheir.setRefCliente((String)((Object[])listObject.get(i))[10]);
				}
				datosOurConTheir.setRefS3((String)((Object[])listObject.get(i))[11]);
				datosOurConTheir.setCodigoPlantilla((String)((Object[])listObject.get(i))[12]);
				listFiltroOurConTheirTablaDTO.add(datosOurConTheir);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getFiltroOurConTheirTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getFiltroOurConTheirTablaDTO");

		return listFiltroOurConTheirTablaDTO;

	}

	/**
	 * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public FiltroOurConTheirTablaDTO getFiltroOurConTheirTablaDTO(LinkedHashMap map) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getFiltroOurConTheirTablaDTO");

		FiltroOurConTheirTablaDTO filtroOurConTheirTablaDTO = new FiltroOurConTheirTablaDTO();

		try {
			filtroOurConTheirTablaDTO.setAuxContable((String) map.get("auxContable"));
			filtroOurConTheirTablaDTO.setCodigoPlantilla((String) map.get("codigoPlantilla"));
			filtroOurConTheirTablaDTO.setCodIsinNombIsin((String) map.get("codIsinNombIsin"));
			filtroOurConTheirTablaDTO.setFhContratacion((new Date((Long) map.get("fhContratacion"))));
			filtroOurConTheirTablaDTO.setFhLiquidacion(new Date((Long) map.get("fhLiquidacion")));
			if (null != map.get("impOurPteGenerar")) {
				if (map.get("impOurPteGenerar") instanceof Double) {
					filtroOurConTheirTablaDTO.setImpOurPteGenerar(BigDecimal.valueOf((Double) map.get("impOurPteGenerar")));
				} else if (map.get("impOurPteGenerar") instanceof Integer){
					filtroOurConTheirTablaDTO.setImpOurPteGenerar(BigDecimal.valueOf((Integer) map.get("impOurPteGenerar")));
				} else {
					filtroOurConTheirTablaDTO.setImpOurPteGenerar(null);
				}
			}
			filtroOurConTheirTablaDTO.setnBooking((String) map.get("nBooking"));
			filtroOurConTheirTablaDTO.setnDesglose((String) map.get("nDesglose"));
			filtroOurConTheirTablaDTO.setRefCliente((String) map.get("refCliente"));
			filtroOurConTheirTablaDTO.setRefS3((String) map.get("refS3"));
			if (null != map.get("secDesglose")) {
				if (map.get("secDesglose") instanceof Double) {
					filtroOurConTheirTablaDTO.setSecDesglose(BigDecimal.valueOf((Double) map.get("secDesglose")));
				} else if (map.get("secDesglose") instanceof Integer){
					filtroOurConTheirTablaDTO.setSecDesglose(BigDecimal.valueOf((Integer) map.get("secDesglose")));
				} else {
					filtroOurConTheirTablaDTO.setSecDesglose(null);
				}
			}
			filtroOurConTheirTablaDTO.setSentido((String) map.get("sentido"));
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaApuntesPndts -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getFiltroOurConTheirTablaDTO");

		return filtroOurConTheirTablaDTO;

	}

	/**
	 * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public GrillaTLMDTO getGrillaTLMDTO(LinkedHashMap map) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getGrillaTLMDTO");

		GrillaTLMDTO grillaTLMDTO = new GrillaTLMDTO();

		try {
			grillaTLMDTO.setAntiguedad((String) map.get("auxContable"));
			grillaTLMDTO.setAuxiliarBancario((String) map.get("auxContable"));
			grillaTLMDTO.setBookingDate((new Date((Long) map.get("auxContable"))));
			grillaTLMDTO.setComentario((String) map.get("auxContable"));
			grillaTLMDTO.setConceptoMovimiento((String) map.get("auxContable"));
			grillaTLMDTO.setGin((Integer) map.get("auxContable"));
			grillaTLMDTO.setId((Long) map.get("auxContable"));
			if (null != map.get("impOurPteGenerar")) {
				if (map.get("impOurPteGenerar") instanceof Double) {
					grillaTLMDTO.setImporte(BigDecimal.valueOf((Double) map.get("impOurPteGenerar")));
				} else if (map.get("impOurPteGenerar") instanceof Integer){
					grillaTLMDTO.setImporte(BigDecimal.valueOf((Integer) map.get("impOurPteGenerar")));
				} else {
					grillaTLMDTO.setImporte(null);
				}
			}
			grillaTLMDTO.setNumeroDocumento((String) map.get("auxContable"));
			grillaTLMDTO.setTipoDeMovimiento((String) map.get("auxContable"));
			grillaTLMDTO.setValueDate((new Date((Long) map.get("auxContable"))));
		} catch (Exception e) {
			LOG.error("Error metodo getGrillaTLMDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getGrillaTLMDTO");

		return grillaTLMDTO;
	}

	/**
	 * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public Tmct0ContaConciliacionAudit getTmct0ContaConciliacionAudit(String auditUser, Tmct0ContaConcepto concepto) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getTmct0ContaConciliacionAudit");

		Tmct0ContaConciliacionAudit conciliacionAudit = new Tmct0ContaConciliacionAudit();

		try {
			conciliacionAudit.setAuditDate(new Date());
			conciliacionAudit.setAuditUser(auditUser);
			conciliacionAudit.setConcepto(concepto);
			conciliacionAudit.setOrigen("SIBBAC");
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaConciliacionAudit -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaConciliacionAudit");

		return conciliacionAudit;
	}

	/**
	 * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public Tmct0ContaApuntesPndts getTmct0ContaApuntesPndts(String auditUser, Tmct0ContaConcepto concepto, LinkedHashMap apunte) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getTmct0ContaApuntesPndts");

		Tmct0ContaApuntesPndts tmct0ContaApuntesPndts = new Tmct0ContaApuntesPndts();

		try {
			tmct0ContaApuntesPndts.setConcepto(concepto);
			tmct0ContaApuntesPndts.setFeApunte(new Timestamp(new Date().getTime()));
			if (null != apunte.get("importePyGTotal")) {
				if (apunte.get("importePyGTotal") instanceof Double) {
					tmct0ContaApuntesPndts.setImportePyG(BigDecimal.valueOf((Double) apunte.get("importePyGTotal")));
				} else if (apunte.get("importePyGTotal") instanceof Integer){
					tmct0ContaApuntesPndts.setImportePyG(BigDecimal.valueOf((Integer) apunte.get("importePyGTotal")));
				} else {
					tmct0ContaApuntesPndts.setImportePyG(null);
				}
			}

			if (null != apunte.get("importeHaberTotal")) {
				if (apunte.get("importeHaberTotal") instanceof Double) {
					tmct0ContaApuntesPndts.setImporteHaber(BigDecimal.valueOf((Double) apunte.get("importeHaberTotal")));
				} else if (apunte.get("importeHaberTotal") instanceof Integer){
					tmct0ContaApuntesPndts.setImporteHaber(BigDecimal.valueOf((Integer) apunte.get("importeHaberTotal")));
				} else {
					tmct0ContaApuntesPndts.setImporteHaber(null);
				}
			}

			if (null != apunte.get("importeDebeTotal")) {
				if (apunte.get("importeDebeTotal") instanceof Double) {
					tmct0ContaApuntesPndts.setImporteDebe(BigDecimal.valueOf((Double) apunte.get("importeDebeTotal")));
				} else if (apunte.get("importeDebeTotal") instanceof Integer){
					tmct0ContaApuntesPndts.setImporteDebe(BigDecimal.valueOf((Integer) apunte.get("importeDebeTotal")));
				} else {
					tmct0ContaApuntesPndts.setImporteDebe(null);
				}
			}

			tmct0ContaApuntesPndts.setUsuApunte(auditUser);

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaApuntesPndts -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaApuntesPndts");

		return tmct0ContaApuntesPndts;
	}
	
  /**
  * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
  * 
  * @param listObject -> lista de objetos
  * @return DTO con los datos de la entidad tratados
  * @throws Exception 
  */
 public Tmct0ContaApuntesPndts getTmct0ContaApuntesPndtsTolerance(String auditUser, Tmct0ContaConcepto concepto, LinkedHashMap apunte) throws Exception {

   LOG.info("INICIO - ASSEMBLER - getTmct0ContaApuntesPndtsTolerance");

   Tmct0ContaApuntesPndts tmct0ContaApuntesPndts = new Tmct0ContaApuntesPndts();

   try {
     tmct0ContaApuntesPndts.setConcepto(concepto);
     tmct0ContaApuntesPndts.setFeApunte(new Timestamp(new Date().getTime()));
     
     BigDecimal importe = new BigDecimal("0.0");
     
     if (null != apunte.get("importe")) {
       if (apunte.get("importe") instanceof Double) {
         importe = BigDecimal.valueOf((Double) apunte.get("importe"));
       } else if (apunte.get("importe") instanceof Integer){
         importe = BigDecimal.valueOf((Integer) apunte.get("importe"));
       } 
     }
     
     if(importe!= null){
       if(importe.doubleValue() > 0.0){
         //si es negativo va al debe
         tmct0ContaApuntesPndts.setImporteDebe(importe);
       }
       else{
         //si es positivo va al haber
         tmct0ContaApuntesPndts.setImporteHaber(importe);
       }
     }
     tmct0ContaApuntesPndts.setUsuApunte(auditUser);

   } catch (Exception e) {
     LOG.error("Error metodo getTmct0ContaApuntesPndts -  " + e.getMessage(), e);
     throw (e);
   }

   LOG.info("FIN - ASSEMBLER - getTmct0ContaApuntesPndts");

   return tmct0ContaApuntesPndts;
 }	

	/**
	 * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public List<Tmct0ContaPartidasCasadas> getTmct0ContaPartidasCasadas(List<LinkedHashMap> listaPartidasCasadas, Tmct0ContaApuntesPndts contaApuntesPndts, Tmct0ContaConciliacionAudit conciliacionAudit) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getTmct0ContaPartidasCasadas");

		List<Tmct0ContaPartidasCasadas> tmct0ContaPartidasCasadas = new ArrayList<Tmct0ContaPartidasCasadas>();

		try {
			for (LinkedHashMap listPartidaCasada : listaPartidasCasadas) {
				Tmct0ContaPartidasCasadas partidaCasada = new Tmct0ContaPartidasCasadas();
				partidaCasada.setAntiguedad((String)listPartidaCasada.get("antiguedad"));
				partidaCasada.setApuntesAudit(null);
				partidaCasada.setApuntesPndts(contaApuntesPndts);
				partidaCasada.setAuxBancario((String)listPartidaCasada.get("auxiliarBancario"));
				partidaCasada.setBookingDate((new Date((Long) listPartidaCasada.get("bookingDate"))));
				partidaCasada.setComentario((String)listPartidaCasada.get("comentario"));
				partidaCasada.setComentarioUsuario((String)listPartidaCasada.get("comentarioUsuario"));
				partidaCasada.setConciliacionAudit(conciliacionAudit);
				partidaCasada.setCurrency((String)listPartidaCasada.get("currency"));
				partidaCasada.setDepartamento((String)listPartidaCasada.get("departamento"));
				partidaCasada.setGin(BigInteger.valueOf(((Integer)listPartidaCasada.get("gin")).intValue()));
				if (null != listPartidaCasada.get("importe")) {
					if (listPartidaCasada.get("importe") instanceof Double) {
						partidaCasada.setImporte(BigDecimal.valueOf((Double) listPartidaCasada.get("importe")));
					} else if (listPartidaCasada.get("importe") instanceof Integer){
						partidaCasada.setImporte(BigDecimal.valueOf((Integer) listPartidaCasada.get("importe")));
					} else {
						partidaCasada.setImporte(null);
					}
				}
				partidaCasada.setNumDocumento((String)listPartidaCasada.get("numeroDocumento"));
				partidaCasada.setNumDocumento2((String)listPartidaCasada.get("numeroDocumento2"));
				partidaCasada.setReferencia((String)listPartidaCasada.get("referencia"));
				partidaCasada.setReferencia2((String)listPartidaCasada.get("referencia2"));
				partidaCasada.setTipo((String)listPartidaCasada.get("tipoDeMovimiento"));
				partidaCasada.setValueDate((new Date((Long) listPartidaCasada.get("valueDate"))));
				tmct0ContaPartidasCasadas.add(partidaCasada);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaPartidasCasadas -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaPartidasCasadas");

		return tmct0ContaPartidasCasadas;
	}

	/**
	 * Realiza la conversion entre una lista de registros de la tabla de TLM y un DTO
	 * 
	 * @param listObject -> lista de objetos
	 * @return DTO con los datos de la entidad tratados
	 * @throws Exception 
	 */
	public List<Tmct0ContaOur> getTmct0ContaPartidasOur(List<LinkedHashMap> listaOur, Tmct0ContaApuntesPndts contaApuntesPndts, Tmct0ContaConciliacionAudit conciliacionAudit) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getTmct0ContaPartidasOur");

		List<Tmct0ContaOur> tmct0ContaOur = new ArrayList<Tmct0ContaOur>();

		try {
			for (LinkedHashMap listOur : listaOur) {
				Tmct0ContaOur our = new Tmct0ContaOur();
				our.setApuntesAudit(null);
				our.setApuntesPndts(contaApuntesPndts);
				our.setConciliacionAudit(conciliacionAudit);
				our.setIdFallido(null);
				our.setnBooking((String)listOur.get("nBooking"));
				our.setNuCnfclt((String)listOur.get("nDesglose"));
				if (null != listOur.get("secDesglose")) {
					if (listOur.get("secDesglose") instanceof Double) {
						our.setNuCnfliq(BigDecimal.valueOf((Double) listOur.get("secDesglose")));
					} else if (listOur.get("secDesglose") instanceof Integer){
						our.setNuCnfliq(BigDecimal.valueOf((Integer) listOur.get("secDesglose")));
					} else {
						our.setNuCnfliq(null);
					}
				}
				our.setNuOrden((String)listOur.get("nuOrden"));
				if (null != listOur.get("impOurPteGenerar")) {
					if (listOur.get("impOurPteGenerar") instanceof Double) {
						our.setImporte(BigDecimal.valueOf((Double) listOur.get("impOurPteGenerar")));
					} else if (listOur.get("impOurPteGenerar") instanceof Integer){
						our.setImporte(BigDecimal.valueOf((Integer) listOur.get("impOurPteGenerar")));
					} else {
						our.setImporte(null);
					}
				}
				our.setSentido((String)listOur.get("sentido"));
				our.setAuxBancario((String)listOur.get("auxContable"));
				our.setCodPlantilla((String)listOur.get("codigoPlantilla"));
				tmct0ContaOur.add(our);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaPartidasOur -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0ContaPartidasOur");

		return tmct0ContaOur;
	}

	public List<Tmct0ContaOurApuntesDTO> getTmct0ContaOurApuntesDTOs(ArrayList<LinkedHashMap> listOur) throws Exception {
		List<Tmct0ContaOurApuntesDTO> result = new ArrayList<Tmct0ContaOurApuntesDTO>();

		try {
			for (LinkedHashMap conta : listOur) {
				Tmct0ContaOurApuntesDTO our = new Tmct0ContaOurApuntesDTO();
				our.setNbooking((String)conta.get("nBooking"));
				our.setNucnfclt((String)conta.get("nDesglose"));
				if (null != conta.get("secDesglose")) {
					if (conta.get("secDesglose") instanceof Double) {
						our.setNucnfliq(BigDecimal.valueOf((Double) conta.get("secDesglose")));
					} else if (conta.get("secDesglose") instanceof Integer){
						our.setNucnfliq(BigDecimal.valueOf((Integer) conta.get("secDesglose")));
					} else {
						our.setNucnfliq(null);
					}
				}
				our.setNuorden((String)conta.get("nuOrden"));
				our.setAuxiliar((String)conta.get("auxContable"));
				our.setPlantilla((String)conta.get("codigoPlantilla"));
				if (conta.get("impOurPteGenerar") instanceof Double) {
					our.setImporte(BigDecimal.valueOf((Double) conta.get("impOurPteGenerar")));
				} else if (conta.get("impOurPteGenerar") instanceof Integer){
					our.setImporte(BigDecimal.valueOf((Integer) conta.get("impOurPteGenerar")));
				}
				result.add(our);
			}
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaPartidasOur -  " + e.getMessage(), e);
			throw (e);
		}

		return result;
	}

	public List<Tmct0ContaOurApuntesDTO> getTmct0ContaOurApuntesDTO(List<Tmct0ContaOur> listaOur) throws Exception {
		List<Tmct0ContaOurApuntesDTO> result = new ArrayList<Tmct0ContaOurApuntesDTO>();

		try {
			for (Tmct0ContaOur tmct0ContaOur : listaOur) {
				Tmct0ContaOurApuntesDTO our = new Tmct0ContaOurApuntesDTO();
				our.setNbooking(tmct0ContaOur.getnBooking());
				our.setNucnfclt(tmct0ContaOur.getNuCnfclt());
				our.setNucnfliq(tmct0ContaOur.getNuCnfliq());
				our.setNuorden(tmct0ContaOur.getNuOrden());
				our.setAuxiliar(tmct0ContaOur.getAuxBancario());
				our.setPlantilla(tmct0ContaOur.getCodPlantilla());
				result.add(our);
			}
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0ContaOurApuntesDTO -  " + e.getMessage(), e);
			throw (e);
		}

		return result;
	}

}
