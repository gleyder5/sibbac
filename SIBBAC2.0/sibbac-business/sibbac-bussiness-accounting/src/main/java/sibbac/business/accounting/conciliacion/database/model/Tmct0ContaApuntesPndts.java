package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entidad para la gestion de TMCT0_CONTA_APUNTES_PNDTS.
 */
@Entity
@Table(name = "TMCT0_CONTA_APUNTES_PNDTS")
public class Tmct0ContaApuntesPndts {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	@Column(name = "FEAPUNTE", length = 10, nullable = true)
	private Timestamp feApunte;
	
	@Column(name = "USUAPUNTE", length = 255, nullable = true)
	private String usuApunte;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "CONCEPTO")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaConcepto concepto;
	
	@Column(name = "IMPORTEDEBE", nullable = true, precision = 18, scale = 4)
	private BigDecimal importeDebe;
	
	@Column(name = "IMPORTEHABER", nullable = true, precision = 18, scale = 4)
	private BigDecimal importeHaber;
	
	@Column(name = "IMPORTEPYG", nullable = true, precision = 18, scale = 4)
	private BigDecimal importePyG;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaApuntesPndts() {
		super();
	}

	/**
	 * @param id
	 * @param feApunte
	 * @param usuApunte
	 * @param concepto
	 * @param importeDebe
	 * @param importeHaber
	 * @param importePyG
	 */
	public Tmct0ContaApuntesPndts(BigInteger id, Timestamp feApunte,
			String usuApunte, Tmct0ContaConcepto concepto, BigDecimal importeDebe,
			BigDecimal importeHaber, BigDecimal importePyG) {
		super();
		this.id = id;
		this.feApunte = feApunte;
		this.usuApunte = usuApunte;
		this.concepto = concepto;
		this.importeDebe = importeDebe;
		this.importeHaber = importeHaber;
		this.importePyG = importePyG;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the feApunte
	 */
	public Timestamp getFeApunte() {
		return feApunte;
	}

	/**
	 * @param feApunte the feApunte to set
	 */
	public void setFeApunte(Timestamp feApunte) {
		this.feApunte = feApunte;
	}

	/**
	 * @return the usuApunte
	 */
	public String getUsuApunte() {
		return usuApunte;
	}

	/**
	 * @param usuApunte the usuApunte to set
	 */
	public void setUsuApunte(String usuApunte) {
		this.usuApunte = usuApunte;
	}

	/**
	 * @return the concepto
	 */
	public Tmct0ContaConcepto getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(Tmct0ContaConcepto concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the importeDebe
	 */
	public BigDecimal getImporteDebe() {
		return importeDebe;
	}

	/**
	 * @param importeDebe the importeDebe to set
	 */
	public void setImporteDebe(BigDecimal importeDebe) {
		this.importeDebe = importeDebe;
	}

	/**
	 * @return the importeHaber
	 */
	public BigDecimal getImporteHaber() {
		return importeHaber;
	}

	/**
	 * @param importeHaber the importeHaber to set
	 */
	public void setImporteHaber(BigDecimal importeHaber) {
		this.importeHaber = importeHaber;
	}

	/**
	 * @return the importePyG
	 */
	public BigDecimal getImportePyG() {
		return importePyG;
	}

	/**
	 * @param importePyG the importePyG to set
	 */
	public void setImportePyG(BigDecimal importePyG) {
		this.importePyG = importePyG;
	}
}
