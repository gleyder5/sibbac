package sibbac.business.accounting.conciliacion.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaCabecerasExcel;

/**
 * Data access object de Conta Cabeceras Excel.
 */
@Repository
public interface Tmct0ContaCabecerasExcelDao extends JpaRepository<Tmct0ContaCabecerasExcel, Integer> {

	/**
	 *	Devuelve objeto con la cabecera Excel que se le envie sin verificar mayusculas/minusculas.
	 *	@param celdaCabeceraExcel
	 *	@return Tmct0ContaCabecerasExcel 	
	 */
	@Query("SELECT cabExcel FROM Tmct0ContaCabecerasExcel cabExcel WHERE UPPER(cabExcel.cabeceraExcel) = UPPER(:celdaCabeceraExcel)")
	public Tmct0ContaCabecerasExcel findByCeldaCabeceraExcel(@Param("celdaCabeceraExcel") String celdaCabeceraExcel);

}
