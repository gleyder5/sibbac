package sibbac.business.accounting.conciliacion.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;

/**
 * Data access object para Conta Partidas pendientes.
 */
@Repository
public interface Tmct0ContaApuntesPndtsDao extends JpaRepository<Tmct0ContaApuntesPndts, Integer> {
	
	@Modifying @Query(value="DELETE FROM TMCT0_CONTA_APUNTES_PNDTS WHERE id IN :listId",nativeQuery=true)
	public void deleteFromId(@Param( "listId" ) List<BigInteger> listId);
	
}
