package sibbac.business.accounting.database.bo;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.database.dao.Tmct0ContaPlantillasDao;
import sibbac.business.accounting.database.dao.Tmct0Norma43DetalleDao;
import sibbac.business.accounting.database.dto.CSVCobrosBarridosDTO;
import sibbac.business.accounting.database.dto.CobroDTO;
import sibbac.business.accounting.database.dto.ContaApuntesCobrosBarridosDTO;
import sibbac.business.accounting.database.dto.PersistenciaCobrosBarridosDTO;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApuntes;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.database.model.Tmct0Norma43Detalle;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.threads.EjecucionPlantillaIndividualRunnable;
import sibbac.business.conciliacion.database.dao.Norma43MovimientoDao;
import sibbac.business.conciliacion.database.model.Norma43Descuadres;
import sibbac.business.conciliacion.database.model.Norma43Movimiento;
import sibbac.business.wrappers.common.ExcelProcessingHelper;
import sibbac.business.wrappers.common.FileHelper;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.ConstantesQueries;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;

import com.google.common.primitives.Ints;
import com.opencsv.CSVReader;

/**
 * Clase de negocios para la ejecucion de plantillas de Cobro Barridos.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EjecucionCobrosBarridos extends AbstractEjecucionPlantilla {

  private List<String> listaErrores = new ArrayList<String>();

  @Value("${accounting.conciliacion.folder.in}")
  private String folderConciliacionIn;

  @Value("${accounting.conciliacion.folder.in.tratados}")
  private String folderConciliacionInTratados;

  @Autowired
  Norma43MovimientoDao norma43MovimientoDao;

  @Autowired
  Tmct0Norma43DetalleDao tmct0Norma43DetalleDao;

  @Autowired
  Tmct0ContaPlantillasDao tmct0ContaPlantillasDao;

  @Autowired
  Tmct0ContaApuntesBo tmct0ContaApuntesBo;

  @Autowired
  private SendMail sendMail;

  @Value("${sibbac.accounting.destinatario.mail.plantillas}")
  private String toMailPlantillas;

  @Value("${sibbac.accounting.subject.mail.plantillas.cobros.barridos.error.importe}")
  private String subjectMailPlantillasError;

  @Value("${sibbac.accounting.cuerpo.mail.plantillas.cobros.barridos.error.importe}")
  private String cuerpoMailPlantillasError;

  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

  /**
   * Ejecucion de plantillas Cobros Barridos.
   * 
   * @param plantillas plantillas
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  @Override
  protected List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException {

    if (!CollectionUtils.isEmpty(plantillas)) {
      ExecutorService executor = Executors.newFixedThreadPool(plantillas.size());
      for (Tmct0ContaPlantillas plantilla : plantillas) {
        EjecucionPlantillaIndividualRunnable runnable = new EjecucionPlantillaIndividualRunnable(plantilla, this);
        executor.execute(runnable);
      }

      try {
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.HOURS);
      } catch (InterruptedException e) {
        Loggers.logErrorAccounting(plantillas.get(0).getCodigo() != null ? plantillas.get(0).getCodigo() : null,
                                   ConstantesError.ERROR_EJECUCION_HILOS.getValue());
        throw new EjecucionPlantillaException("ERROR ejecutarPlantillas");
      }
    }
    return plantillasSinBalance;
  }

  /**
   * Se procesan cada uno de los subtipos para generar apuntes contables o descuadres segun el caso
   * 
   * @param nombreFicheroCSV nombreFicheroCSV
   * @param idsMovimientoGenerados idsMovimientoGenerados
   * @return boolean boolean - Devuelve true si hay exito.
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private boolean procesarSubtiposCobrosBarridos(String nombreFicheroCSV, List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle) throws EjecucionPlantillaException {
    // Se obtiene plantillla a procesar.
    Tmct0ContaPlantillas tmct0ContaPlantilla = this.tmct0ContaPlantillasDao.findActivoByFicheroCobro(nombreFicheroCSV);

    Loggers.logDebugInicioFinAccounting(tmct0ContaPlantilla != null ? tmct0ContaPlantilla.getCodigo() : null, true);

    // Se persiste la lista norma 43 y se obtienen ids de movimiento involucrados.
    List<Integer> idsMovimientoGenerados = new ArrayList<Integer>();

    // Almacenamiento de descuadres (potenciales) a generarse.
    List<Norma43Descuadres> listaNorma43Descuadres = new ArrayList<Norma43Descuadres>();

    // Almacenamiento de contaapuntes en un DTO a generarse.
    List<ContaApuntesCobrosBarridosDTO> listaContaApuntesDebeHaberDTO = new ArrayList<ContaApuntesCobrosBarridosDTO>();

    if (CollectionUtils.isNotEmpty(listaTmct0Norma43Detalle)) {
      for (Tmct0Norma43Detalle norma43Detalle : listaTmct0Norma43Detalle) {
        idsMovimientoGenerados.add(norma43Detalle.getIdmovimiento());
      }
    }

    if (CollectionUtils.isNotEmpty(idsMovimientoGenerados)) {

      // Se debe totalizar el campo IMPORTE en la tabla TMCT0_NORMA43_DETALLE
      // agrupando por IDMOVIMIENTO, TIPO_COMISION
      List<Object[]> camposDetalle = this.tmct0Norma43DetalleDao.findGroupedFieldsByTipoApunteAndTipoComisionAndIdsMovimiento("ABONO",
                                                                                                                              tmct0ContaPlantilla.getSubtipo(),
                                                                                                                              idsMovimientoGenerados);

      if (CollectionUtils.isNotEmpty(camposDetalle)) {

        for (Object[] campoDetalle : camposDetalle) {
          // Con el IDMOVIMIENTO y el IMPORTE obtenido debemos consultar en la tabla TMCT0_NORMA43_MOVIMIENTO
          // y localizar el movimiento relacionado, si el importe acumulado en TMCT0_NORMA43_DETALLE
          // es el mismo que el importe obtenido
          Integer idmovimiento = (Integer) campoDetalle[0];
          BigDecimal importe = (BigDecimal) campoDetalle[1];

          Norma43Movimiento norma43Movimiento = null;
          if (idmovimiento != null && importe != null) {
            norma43Movimiento = this.norma43MovimientoDao.findByIdAndImporte(new Long(idmovimiento), importe);
          }

          if (norma43Movimiento != null) {
            // Si el importe es el mismo comparar
            // TMCT0_NORMA43_DETALLE.IMPORTE (acumulado) = TMCT0_NORMA43_MOVIMIENTO.IMPORTE
            // con IMPORTE DE QUERY_ORDEN

            if (tmct0ContaPlantilla != null) {
              List<Object[]> datosQueryCobro = null;
              List<Object[]> datosQueryOrden = null;
              try {
                datosQueryCobro = this.ejecutaQueryCobro(tmct0ContaPlantilla);
                if (CollectionUtils.isNotEmpty(datosQueryCobro)) {
                  Loggers.logTraceQueriesAccounting(tmct0ContaPlantilla.getCodigo(), ConstantesQueries.CANTIDAD_REGISTROS_OBTENIDOS.getValue()
                                                        + SibbacEnums.ConstantesQueries.QUERY_COBRO.getValue() + " : " + datosQueryCobro.size(), null);
                }
              } catch (Exception ex) {
                // De haber algun error se para la ejecucion completa.
                Loggers.logErrorAccounting(tmct0ContaPlantilla.getCodigo(), ConstantesError.ERROR_QUERY_COBRO.getValue() + ": " + ex.getMessage());
                throw new EjecucionPlantillaException();
              }

              try {
                // Se obtienen los importes para la plantilla en Query Orden.
                datosQueryOrden = this.ejecutaQueryOrden(tmct0ContaPlantilla, norma43Movimiento);
                if (CollectionUtils.isNotEmpty(datosQueryOrden)) {
                  Loggers.logTraceQueriesAccounting(tmct0ContaPlantilla.getCodigo(), ConstantesQueries.CANTIDAD_REGISTROS_OBTENIDOS.getValue()
                                                        + SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue() + " : " + datosQueryOrden.size(), null);
                }
              } catch (Exception ex) {
                // De haber algun error se para la ejecucion completa.
                Loggers.logErrorAccounting(tmct0ContaPlantilla.getCodigo(),
                                           ConstantesError.ERROR_QUERY_ORDEN.getValue() + ": " + ex.getMessage());
                throw new EjecucionPlantillaException();
              }
              for (Object[] cobro : datosQueryCobro) {
                if (CollectionUtils.isNotEmpty(datosQueryOrden)) {
                  CobroDTO cobroDTO = new CobroDTO(cobro);
                  Set<Integer> idsMovimiento = new HashSet<Integer>();
                  List<Tmct0ContaApuntes> listaApuntesDiferencia = new ArrayList<Tmct0ContaApuntes>();
                  for (Object[] datoQueryOrden : datosQueryOrden) {

                    List<Tmct0ContaPlantillasDTO> listaTmct0ContaPlantillasDTO = this.tmct0ContaApuntesBo.dividirPlantillasPorCuentasCB(tmct0ContaPlantilla,
                                                                                                                                        datoQueryOrden,
                                                                                                                                        norma43Movimiento);

                    // Esto contiene la suma de importes - inicializado.
                    BigDecimal sumaImportes = NumberUtils.createBigDecimal("0");

                    for (Tmct0ContaPlantillasDTO contaPlantillasDTO : listaTmct0ContaPlantillasDTO) {
                      if (this.isGenerarApunteContable(contaPlantillasDTO.getImporteDebe(), importe,
                                                       tmct0ContaPlantilla.getTolerancia())) {
                        // Generar apuntes contables.
                        ContaApuntesCobrosBarridosDTO contaApuntesCobrosBarridosDTO = this.generarApuntesContables(norma43Movimiento,
                                                                                                                   cobroDTO,
                                                                                                                   contaPlantillasDTO,
                                                                                                                   listaTmct0Norma43Detalle);

                        // Se suman los importes de todos los apuntes populados
                        // para verificar si se debe armar apunte de diferencia (PyG).
                        if (contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes() != null
                            && contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes().length > 0) {
                          for (Tmct0ContaApuntes contaApunte : contaApuntesCobrosBarridosDTO.getArrayTmct0ContaApuntes()) {
                            sumaImportes = sumaImportes.add(contaApunte.getImporte());
                          }
                        }

                        listaContaApuntesDebeHaberDTO.add(contaApuntesCobrosBarridosDTO);
                      } else {
                        // Generar descuadres.
                        listaNorma43Descuadres.add(this.crearDescuadreCB("NO SE HA ENCONTRADO ORDEN EN SIBBAC",
                                                                         contaPlantillasDTO, cobroDTO,
                                                                         listaTmct0Norma43Detalle));
                      }
                      // Se agrega el id de movimiento de la query de cobro.
                      idsMovimiento.add(Ints.checkedCast(cobroDTO.getIdMov()));
                    }

                    // Genera apunte Diferencias en caso necesario.
                    if (this.isGeneraApunteDiferencia(sumaImportes)) {
                      // OrdenDTO ordenDTO = new OrdenDTO(
                      // (Date) datoQueryOrden[0],
                      // (Integer) datoQueryOrden[1],
                      // (String) datoQueryOrden[2],
                      // (BigDecimal) datoQueryOrden[3]);
                      // Se pasa al apunte de diferencia la suma de importes negado
                      // para totalizar 0 entre todos los apuntes generados
                      listaApuntesDiferencia.add(this.tmct0ContaApuntesBo.getApunteDiferencia(sumaImportes.negate(),
                                                                                              listaTmct0ContaPlantillasDTO.get(0),
                                                                                              cobroDTO));
                    }
                  }

                  PersistenciaCobrosBarridosDTO pcbDTO = new PersistenciaCobrosBarridosDTO(
                                                                                           idsMovimiento,
                                                                                           tmct0ContaPlantilla,
                                                                                           norma43Movimiento,
                                                                                           listaNorma43Descuadres,
                                                                                           listaTmct0Norma43Detalle,
                                                                                           listaContaApuntesDebeHaberDTO,
                                                                                           listaApuntesDiferencia);

                  try {
                    // Se persiste lo que se fue generando
                    // Se borran registros de detalle innecesarios
                    // y se pasa el cobro a procesado.
                    return this.tmct0ContaApuntesBo.postPersistenciaCobrosBarridos(pcbDTO,
                                                                                   tmct0ContaPlantilla.getCodigo());
                  } catch (Exception ex) {
                    // De haber algun error se para la ejecucion completa.
                    Loggers.logErrorAccounting(tmct0ContaPlantilla.getCodigo(),
                                               ConstantesError.ERROR_APUNTES_CONTABLES.getValue() + ": "
                                                   + ex.getMessage());
                    throw new EjecucionPlantillaException();
                  }
                }
              }
            }
          } else {
            // Si el importe no es el mismo se debe generar un error informando con un mail
            // y continuar con el siguiente IDMOVIMIENTO
            this.listaErrores.add("Error: No hay coincidencia de importes en fichero: " + nombreFicheroCSV);
            Loggers.logErrorAccounting(tmct0ContaPlantilla.getCodigo(), "No hay coincidencia de importes en fichero: "
                                                                        + nombreFicheroCSV);
            this.agregarErroresFichero(this.listaErrores);
            LOG.debug(" +++++ NO COINCIDE +++++ ");

            try {
              this.sendMail.sendMail(toMailPlantillas, subjectMailPlantillasError, "Id's de TMCT0_NORMA43_MOVIMIENTO: "
                                                                                   + idsMovimientoGenerados.toString()
                                                                                   + " - " + cuerpoMailPlantillasError);
            } catch (Exception ex) {
              Loggers.logErrorAccounting(tmct0ContaPlantilla.getCodigo(), ConstantesError.ERROR_ENVIO_MAIL.getValue()
                                                                          + ": " + ex.getMessage());
            }

            // Si hay error retorna false
            return false;
          }
        }
      }
    }
    Loggers.logDebugInicioFinAccounting(tmct0ContaPlantilla != null ? tmct0ContaPlantilla.getCodigo() : null, false);
    return false;
  }

  /**
   * Evalua si se genera apunte de diferencia - Lo hace si la suma completa es distinto de cero.
   * 
   * @param sumaImportes sumaImportes
   * @return boolean boolean
   */
  private boolean isGeneraApunteDiferencia(BigDecimal sumaImportes) {
    return sumaImportes.compareTo(BigDecimal.ZERO) != 0;
  }

  /**
   * Generacion de apuntes contables Cobros Barridos.
   * 
   * @param tmct0ContaPlantilla tmct0ContaPlantilla
   * @param norma43Movimiento norma43Movimiento
   * @param cobroDTO cobroDTO
   * @param OrdenDTO ordenDTO
   * @param listaTmct0Norma43Detalle listaTmct0Norma43Detalle
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private ContaApuntesCobrosBarridosDTO generarApuntesContables(Norma43Movimiento norma43Movimiento,
                                                                CobroDTO cobroDTO,
                                                                Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                                List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle) throws EjecucionPlantillaException {
    ContaApuntesCobrosBarridosDTO apuntesDTO = this.generarApuntesContablesCB(norma43Movimiento, cobroDTO,
                                                                              contaPlantillasDTO,
                                                                              listaTmct0Norma43Detalle);
    return apuntesDTO;
  }

  /**
   * Wrapper para ejecucion de query orden de cobros barridos.
   * 
   * @param plantilla plantilla
   * @param norma43Movimiento norma43Movimiento
   * @return List<Object[]> List<Object[]>
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  private List<Object[]> ejecutaQueryOrden(Tmct0ContaPlantillas plantilla, Norma43Movimiento norma43Movimiento) throws EjecucionPlantillaException {

    Loggers.logDebugQueriesAccounting(plantilla != null ? plantilla.getCodigo() : null,
                                      SibbacEnums.ConstantesQueries.QUERY_ORDEN.getValue(), null);

    String queryOrden = plantilla.getQuery_orden()
                                 .replaceAll("<DESC_REFERENCIA DE NORMA43_MOVIMIENTO>",
                                             "'" + norma43Movimiento.getDescReferencia() + "'")
                                 .replaceAll("<ID DE NORMA43_MOVIMIENTO>", "'" + norma43Movimiento.getId() + "'");
    return contaPlantillasDaoImp.executeQuery(queryOrden.replace(";", ""), plantilla.getCodigo());
  }

  /**
   * Si coinciden importes o el importe de diferencia es inferior al tolerancia devuelve true.
   * 
   * @param importeQOrdenBigDec importeQOrdenBigDec
   * @param importe importe
   * @param tolerancia tolerancia
   * @return boolean boolean
   */
  private boolean isGenerarApunteContable(BigDecimal importeQOrdenBigDec, BigDecimal importe, BigDecimal tolerancia) {
    BigDecimal resta = importeQOrdenBigDec.subtract(importe);
    return importeQOrdenBigDec.compareTo(importe) == 0
           || (importeQOrdenBigDec.compareTo(importe) != 0 && (resta.abs().doubleValue() < tolerancia.doubleValue()));
  }

  /**
   * Se populan y persisten registros de entidad Tmct0Norma43Detalle
   * 
   * @param listaCSVCobrosBarridosDTO listaCSVCobrosBarridosDTO
   * @return List<Tmct0Norma43Detalle> List<Tmct0Norma43Detalle>
   */
  private List<Tmct0Norma43Detalle> getListadoNorma43Detalle(List<CSVCobrosBarridosDTO> listaCSVCobrosBarridosDTO) {
    List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle = new ArrayList<Tmct0Norma43Detalle>();
    // hay que guardar todas las filas ignorando la primera en
    // la tabla TMCT0_NORMA43_DETALLE
    if (CollectionUtils.isNotEmpty(listaCSVCobrosBarridosDTO)) {

      for (CSVCobrosBarridosDTO cSVCobrosBarridosDTO : listaCSVCobrosBarridosDTO) {
        Tmct0Norma43Detalle tmct0Norma43Detalle = new Tmct0Norma43Detalle();

        List<Norma43Movimiento> listaNorma43Movimiento = this.norma43MovimientoDao.findByDescReferenciaAndProcesado(
        		cSVCobrosBarridosDTO.getCodVaiso());

        if (CollectionUtils.isNotEmpty(listaNorma43Movimiento)) {
          // IDOMOVIMIENTO � el ID de la tabla TMCT0_NORMA43_MOVIMIENTO buscando
          // por la Cuenta Bancaria de la plantilla que tenga como DESC_REFERENCIA el valor de la
          // 6� columna del fichero y cuyo campo �procesado� sea 0
          tmct0Norma43Detalle.setIdmovimiento(listaNorma43Movimiento.get(0).getId().intValue());

          // El valor de la columna 1 del fichero - NUMORDE
          tmct0Norma43Detalle.setPropuesta(cSVCobrosBarridosDTO.getNumOrde());

          // FECHA - el valor de la columna 3 del fichero - FECHCON
          tmct0Norma43Detalle.setFecha(cSVCobrosBarridosDTO.getFechCon());

          // IMPORTE - el valor de la columna 4 del fichero - IMPDIVO
          tmct0Norma43Detalle.setImporte(ExcelProcessingHelper.getValorCeldaABigDecimalAdaptada(cSVCobrosBarridosDTO.getImpDivo()));

          // TIPO_APUNTE - el valor de la columna 5 del fichero - INDABAD
          // Si en fichero es �D�, en tabla se guarda ADEUDO
          // Si en fichero es �H�, en tabla se guarda ABONO
          if (StringUtils.isNotBlank(cSVCobrosBarridosDTO.getIndAbad())) {
            if ("D".equalsIgnoreCase(cSVCobrosBarridosDTO.getIndAbad().trim())) {
              tmct0Norma43Detalle.setTipoApunte("ADEUDO");
            } else if ("H".equalsIgnoreCase(cSVCobrosBarridosDTO.getIndAbad().trim())) {
              tmct0Norma43Detalle.setTipoApunte("ABONO");
            }
          }

          // ISIN - el valor de la columna 6 del fichero - CODVAISO
          tmct0Norma43Detalle.setIsin(cSVCobrosBarridosDTO.getCodVaiso());

          // TIPO_COMISION � el tipo de comisi�n de la columna 7 del fichero - TIP.COMIS.:
          // Si en fichero es �02�, en tabla se guarda CANON
          // Si en fichero es �04�, en tabla se guarda CORRETAJE
          if (StringUtils.isNotBlank(cSVCobrosBarridosDTO.getTipComis())) {
            if ("02".equals(cSVCobrosBarridosDTO.getTipComis()) || "2".equals(cSVCobrosBarridosDTO.getTipComis())) {
              tmct0Norma43Detalle.setTipoComision("CANON");
            } else if ("04".equals(cSVCobrosBarridosDTO.getTipComis())
                       || "4".equals(cSVCobrosBarridosDTO.getTipComis())) {
              tmct0Norma43Detalle.setTipoComision("CORRETAJE");
            }
          }

          // Se guarda todo en una lista para persistirse.
          listaTmct0Norma43Detalle.add(tmct0Norma43Detalle);
        }
      }

    }
    return listaTmct0Norma43Detalle;
  }

  /**
   * Generacion de apuntes contables Cobros Barridos.
   * 
   * @param tmct0ContaPlantilla tmct0ContaPlantilla
   * @param norma43Movimiento norma43Movimiento
   * @param cobroDTO cobroDTO
   * @param OrdenDTO ordenDTO
   * @return Tmct0ContaApuntes[] Tmct0ContaApuntes[]
   * @throws EjecucionPlantillaException
   */
  public ContaApuntesCobrosBarridosDTO generarApuntesContablesCB(Norma43Movimiento norma43Movimiento,
                                                                 CobroDTO cobroDTO,
                                                                 Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                                                 List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle) throws EjecucionPlantillaException {

    ContaApuntesCobrosBarridosDTO contaApuntesDTO = new ContaApuntesCobrosBarridosDTO();

    Tmct0ContaApuntes[] apuntes = new Tmct0ContaApuntes[2];

    // En la generaci�n del apunte contable se debe generar un solo
    // movimiento al DEBE
    // y un solo movimiento al HABER (AMBOS CON MISMO CONCEPTO)

    // El movimiento al HABER se realiza con el IMPORTE del registro de la
    // tabla
    // NORMA43_MOVIMIENTO, el CONCEPTO ser� el DESC_REFERENCIA de
    // NORMA43_MOVIMIENTO
    if (StringUtils.isNotBlank(contaPlantillasDTO.getCuenta_haber())) {
      Tmct0ContaApuntes contaApunteHaber = AccountingHelper.getInitializedTmct0ContaApuntes();
      this.popularDatosComunesApunteContable(contaApunteHaber, cobroDTO, contaPlantillasDTO);

      if (norma43Movimiento != null) {
        contaApunteHaber.setImporte(norma43Movimiento.getImporte());
      }

      contaApunteHaber.setConcepto(norma43Movimiento.getDescReferencia());
      contaApunteHaber.setCuenta_contable(contaPlantillasDTO.getCuenta_haber());
      contaApunteHaber.setAux_contable(StringHelper.quitarComillasDobles(contaPlantillasDTO.getAux_haber()));
      if (contaPlantillasDTO.getImporteHaber().signum() > 0) {
        contaApunteHaber.setTipo_apunte('D');
      } else if (contaPlantillasDTO.getImporteHaber().signum() < 0) {
        contaApunteHaber.setTipo_apunte('H');
      }
      apuntes[1] = contaApunteHaber;
    }

    // El movimiento al DEBE realiza con el IMPORTE del registro de
    // QUERY_ORDEN,
    // el CONCEPTO ser� el DESC_REFERENCIA de NORMA43_MOVIMIENTO
    if (StringUtils.isNotBlank(contaPlantillasDTO.getCuenta_debe())) {
      Tmct0ContaApuntes contaApunteDebe = AccountingHelper.getInitializedTmct0ContaApuntes();
      this.popularDatosComunesApunteContable(contaApunteDebe, cobroDTO, contaPlantillasDTO);
      if (contaPlantillasDTO != null) {
        if (contaPlantillasDTO.getImporteDebe() != null) {
          contaApunteDebe.setImporte(contaPlantillasDTO.getImporteDebe());
        }
      }
      contaApunteDebe.setConcepto(norma43Movimiento.getDescReferencia());
      contaApunteDebe.setCuenta_contable(contaPlantillasDTO.getCuenta_debe());
      contaApunteDebe.setAux_contable(StringHelper.quitarComillasDobles(contaPlantillasDTO.getAux_debe()));
      if (contaPlantillasDTO.getImporteDebe().signum() > 0) {
        contaApunteDebe.setTipo_apunte('D');
      } else if (contaPlantillasDTO.getImporteDebe().signum() < 0) {
        contaApunteDebe.setTipo_apunte('H');
      }
      apuntes[0] = contaApunteDebe;
    }

    contaApuntesDTO.setArrayTmct0ContaApuntes(apuntes);

    return contaApuntesDTO;
  }

  /**
   * Se populan datos comunes del apunte contable.
   * 
   * @param amExcelDTO: DTO con datos del Excel
   * @param tmct0CPlantilla: la plantilla
   * @param contaApunte: objeto a popular
   */
  private void popularDatosComunesApunteContable(Tmct0ContaApuntes contaApunte,
                                                 CobroDTO cobroDTO,
                                                 Tmct0ContaPlantillasDTO contaPlantillasDTO) {

    contaApunte.setFichero(contaPlantillasDTO.getFicheroConValor());
    if (contaPlantillasDTO != null) {
      contaApunte.setFecha_comprobante(contaPlantillasDTO.getFecha_comprobante());
      contaApunte.setPeriodo_comprobante(String.valueOf(contaPlantillasDTO.getPeriodo_comprobante()));
    }

    if (cobroDTO != null) {
      contaApunte.setIdmovimiento(Ints.checkedCast(cobroDTO.getIdMov()));
    }

    if (contaPlantillasDTO != null) {
      contaApunte.setNum_comprobante(contaPlantillasDTO.getNum_comprobante());
      contaApunte.setTipo_comprobante(contaPlantillasDTO.getTipo_comprobante());
      contaApunte.setCod_plantilla(contaPlantillasDTO.getCodigo());
    }
  }

  /**
   * Creacion de descuadre de cobros barridos. Difiere en que no se setea la info proveniente de query de cobro.
   * 
   * @param comentario comentario
   * @param plantilla plantilla
   * @param cobroDTO cobroDTO
   * @param importeQOrdenBigDec importeQOrdenBigDec
   * @param listaTmct0Norma43Detalle listaTmct0Norma43Detalle
   * @return Norma43Descuadres Norma43Descuadres
   * @throws EjecucionPlantillaException EjecucionPlantillaException
   */
  public Norma43Descuadres crearDescuadreCB(String comentario,
                                            Tmct0ContaPlantillasDTO contaPlantillasDTO,
                                            CobroDTO cobroDTO,
                                            List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle) throws EjecucionPlantillaException {

    Norma43Descuadres descuadre = AccountingHelper.getInitializedNorma43Descuadres();
    // 2.3.4 Generaci�n de descuadres
    // Para la generaci�n de descuadres se debe crear el correspondiente
    // registro en la tabla TMCT0_NORMA43_DESCUADRES de la siguiente forma:

    // 28. CUENTA � Cuenta Bancaria de la plantilla
    descuadre.setCuenta(contaPlantillasDTO.getCuenta_bancaria());
    // 36. NBCOMENTARIO � El comentario del descuadre que corresponda
    descuadre.setComentario(comentario);
    if (cobroDTO != null) {
      // 29. REF_FECHA - El campo REFERENCIA de la query de cobro
      descuadre.setRefFecha(cobroDTO.getReferencia());
      // 30. REF_ORDEN - El campo DESC_REFERENCIA de la query de cobro
      descuadre.setRefOrden(cobroDTO.getDescReferencia());
      // 31. SENTIDO - El campo SENTIDO de la query de cobro
      descuadre.setSentido(cobroDTO.getSentido());
      // 32. TRADE_DATE - El campo TRADEDATE de la query de cobro
      descuadre.setTradeDate(cobroDTO.getTradeDate());
      // 33. SETTLEMENT_DATE � El campo SETTDATE de la query de cobro
      descuadre.setSettlementDate(cobroDTO.getSetDate());
      // 34. IMPORTE_NORMA43 � El campo IMPORTE de la query de cobro
      descuadre.setImporteNorma43(cobroDTO.getImporte());
      // 35. DIFERENCIA � El importe de diferencia restando IMPORTE de query
      // cobro de IMPORTE de query orden
      descuadre.setDiferencia(cobroDTO.getImporte().subtract(contaPlantillasDTO.getImporteDebe()));
    }
    return descuadre;
  }

  @Override
  protected void setTiposPlantilla() {
    super.tiposPlantilla = new String[] { TipoContaPlantillas.COBRO_BARRIDOS.getTipo()
    };
  }

  /**
   * Ejecucion de plantilla individual.
   * 
   * @param plantilla plantilla
   * @throws EjecucionPlantillaException
   */
  @Override
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();
    // Se iteran los archivos encontrados.
    String nombreFicheroCSV = plantilla.getFichero_cobro();

    if (StringUtils.isNotBlank(nombreFicheroCSV)) {
      File ficheroProcesando = new File(folderConciliacionIn + nombreFicheroCSV);

      // Se leen datos de ficheros CSV coincidentes.
      CSVReader reader = null;
      try {
        reader = new CSVReader(new FileReader(folderConciliacionIn + nombreFicheroCSV), ';');
        String[] line;
        int count = 0;
        List<CSVCobrosBarridosDTO> listaCSVCobrosBarridosDTO = new ArrayList<CSVCobrosBarridosDTO>();
        // Se guarda en un listado de DTO's las filas con contenido util del CSV.
        while ((line = reader.readNext()) != null) {
          if (count > 0) {
            CSVCobrosBarridosDTO cSVCobrosBarridosDTO = new CSVCobrosBarridosDTO();
            cSVCobrosBarridosDTO.popularEntidad(line);
            listaCSVCobrosBarridosDTO.add(cSVCobrosBarridosDTO);
          }
          count++;
        }

        // hay que obtener todas las filas ignorando la primera en
        // la tabla TMCT0_NORMA43_DETALLE
        List<Tmct0Norma43Detalle> listaTmct0Norma43Detalle = this.getListadoNorma43Detalle(listaCSVCobrosBarridosDTO);

        // Se procesan cada uno de los subtipos para generar apuntes contables o descuadres
        // segun el caso
        boolean exito = this.procesarSubtiposCobrosBarridos(nombreFicheroCSV, listaTmct0Norma43Detalle);

        reader.close();

        if (exito) {
          // finalizado el tratamiento de la Excel de forma exitosa, este se debe mover
          // a la carpeta de tratados con el nombre nombreFichero_yyyymmdd_hhmmss.CSV
          FileHelper.moveFile(ficheroProcesando, new File(folderConciliacionInTratados.trim()), "");
        }
      } catch (IOException ex) {
        Loggers.logErrorAccounting(plantilla.getCodigo(),
                                   ConstantesError.ERROR_PROCESAR_FICHERO.getValue() + ": " + ex.getMessage());
        // throw new EjecucionPlantillaException(
        // "[EjecucionCobrosBarridos - ERROR ejecutarPlantillaIndividual - Cobros Barridos]" + "[Metodo]" + "[" + new
        // Date() + "]");
      } catch (Exception ex) {
        listaErrores.add("Ha ocurrido un error al procesar el fichero: " + nombreFicheroCSV);
        Loggers.logErrorAccounting(plantilla.getCodigo(),
                                   ConstantesError.ERROR_PROCESAR_FICHERO.getValue() + ": " + ex.getMessage());
      }

    } else {
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue());
    }

    // Si la plantilla tiene query_final se ejecuta.
    if ((plantilla.getQuery_final() != null) && (!plantilla.getQuery_final().equals(""))) {
      try {
        this.contaPlantillasDaoImp.executeQueryUpdate(plantilla.getQuery_final().replace(";", ""),
                                                      plantilla.getCodigo());
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(),
                                   ConstantesError.ERROR_QUERY_FINAL.getValue() + ": " + ex.getMessage());
        throw new EjecucionPlantillaException();
      }
    }

    return plantillasSinBalance;
  }

  @Override
  public void anadirPlantillas(List<List<Tmct0ContaPlantillasDTO>> lPlantillas) {
    plantillasSinBalance.addAll(lPlantillas);
  }
}
