package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entidad para la gestion de TMCT0_CONTA_CONCILIACION_AUDIT.
 */
@Entity
@Table(name = "TMCT0_CONTA_CONCILIACION_AUDIT")
public class Tmct0ContaConciliacionAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	@Column(name = "AUDIT_DATE", length = 10, nullable = true)
	private Date auditDate;
	
	@Column(name = "AUDIT_USER", length = 255, nullable = true)
	private String auditUser;
	
	@Column(name = "ORIGEN", length = 10, nullable = true)
	private String origen;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "CONCEPTO")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaConcepto concepto;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaConciliacionAudit() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param auditDate
	 * @param auditUser
	 * @param origen
	 * @param concepto
	 */
	public Tmct0ContaConciliacionAudit(BigInteger id, Date auditDate,
			String auditUser, String origen, Tmct0ContaConcepto concepto) {
		super();
		this.id = id;
		this.auditDate = auditDate;
		this.auditUser = auditUser;
		this.origen = origen;
		this.concepto=concepto;
	}

	/**
	 * @return the concepto
	 */
	public Tmct0ContaConcepto getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(Tmct0ContaConcepto concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}
}
