package sibbac.business.accounting.conciliacion.database.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.conciliacion.database.dto.FiltroGrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.helper.Helper;
import sibbac.common.SIBBACBusinessException;

@Repository
public class Tmct0ContaPartidasPndtsDaoImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaPartidasPndtsDaoImpl.class);

	@Value("${accounting.bulk.data.batch.size}")
	private int batchSize;

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 *	Obtiene lista de valores distintos segun el nombre de la columna.
	 *	@param columnName
	 *	@return List<String>
	 *	@throws SIBBACBusinessException 
	 */
	@SuppressWarnings("unchecked")
	public List<String> getDistinctValuesByColumnName(String columnName) throws SIBBACBusinessException {

		LOG.info("INICIO - DAOIMPL - getDistinctValuesByColumnName");

		List<String> listaFields = new ArrayList<String>();

		try {
			StringBuilder consulta = new StringBuilder(
					"SELECT DISTINCT " + columnName + " FROM BSNBPSQL.TMCT0_CONTA_PARTIDAS_PNDTS");
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			listaFields = query.getResultList();
		} catch (Exception e) {
			LOG.error("Error metodo getDistinctValuesByColumnName -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - getDistinctValuesByColumnName");

		return listaFields;
	}

	/**
	 * 	Se obtienen datos de las columnas de la tabla segun nombre de tabla y su columna.
	 * 	@param columnName
	 * 	@param tableName
	 * 	@return List<Object[]> - nombre de columna, nombre de tabla, tipo de columna y longitud
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findColumnDataByColumnName(String columnName, String tableName) {

		LOG.info("INICIO - DAOIMPL - findColumnDataByColumnName");

		List<Object[]> listaFields = new ArrayList<Object[]>();

		try {
			StringBuilder consulta = new StringBuilder(
					"SELECT SYSCOL.NAME, SYSCOL.TBNAME, SYSCOL.COLTYPE, SYSCOL.LENGTH FROM SYSIBM.SYSCOLUMNS SYSCOL "
							+ "WHERE UPPER(SYSCOL.TBNAME) = UPPER('" + tableName + "') "
							+ "AND UPPER(SYSCOL.NAME) = UPPER('" + columnName + "')");

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			listaFields = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo findColumnDataByColumnName -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - findColumnDataByColumnName");

		return listaFields;
	}

	/**
	 *	Consulta de busqueda de partidas pendientes por datos de TLM.
	 *	@param filtro
	 *	@return List<Tmct0ContaPartidasPndts> 
	 */
	public List<Tmct0ContaPartidasPndts> consultarPartidasPndtsByFilter(
			FiltroGrillaTLMDTO filtro) {

		LOG.info("INICIO - DAOIMPL - consultarPartidasPndtsByFilter");

		long inicioBusquedaTLM = System.currentTimeMillis();

		List<Tmct0ContaPartidasPndts> listaContaPartidasPndts = new ArrayList<Tmct0ContaPartidasPndts>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		int sizeFechas = 1;
		boolean esBookingDate = false;
		boolean esValueDate = false;

		try {

			if (CollectionUtils.isNotEmpty(filtro.getListaBookingDate())) {
				sizeFechas = filtro.getListaBookingDate().size();
				esBookingDate = true;
			} else if (CollectionUtils.isNotEmpty(filtro.getListaValueDate())) {
				sizeFechas = filtro.getListaValueDate().size();
				esValueDate = true;
			}

			StringBuilder consulta = new StringBuilder(
					"SELECT cpp FROM Tmct0ContaPartidasPndts cpp WHERE cpp.estado=0");

			// Auxiliares bancarios.
			if (CollectionUtils.isNotEmpty(filtro.getAuxiliaresBancarios())) {
				List<String> valoresAuxiliarBancario = new ArrayList<String>();
				for (SelectDTO selectDTO : filtro.getAuxiliaresBancarios()) {
					valoresAuxiliarBancario.add(selectDTO.getValue());
				}
				consulta.append(" AND cpp.auxBancario IN (:auxiliaresBancarios)");
				parameters.put("auxiliaresBancarios", valoresAuxiliarBancario);
			}

			// Tipos de movimiento.
			if (CollectionUtils.isNotEmpty(filtro.getTiposMovimientos())) {
				List<String> valoresTipoMovimiento = new ArrayList<String>();
				for (SelectDTO selectDTO : filtro.getTiposMovimientos()) {
					valoresTipoMovimiento.add(selectDTO.getValue());
				}
				consulta.append(" AND cpp.tipo IN (:tiposMovimientos)");
				parameters.put("tiposMovimientos", valoresTipoMovimiento);
			}

			// Nros. de documento.
			if (CollectionUtils.isNotEmpty(filtro.getNumerosDocumentos())) {
				int countIdentif = 0;
				String prefijo = " AND (";
				for (SelectDTO selectDTO : filtro.getNumerosDocumentos()) {
					if (countIdentif > 0) {
						prefijo = " OR ";
					}
					String[] valores = Helper.getQueryFragmentByOperator(
							":numDocumento" + countIdentif, 
							selectDTO.getDescription(), 
							selectDTO.getValue());
					consulta.append(
							prefijo + " cpp.numDocumento " + valores[0]);
					parameters.put("numDocumento" + countIdentif, valores[1]);
					countIdentif++;
				}
				consulta.append(")");
			}

			// Concepto Movimiento - REFERENCIA.
			if (CollectionUtils.isNotEmpty(filtro.getConceptosMovimientos())) {
				int countIdentif = 0;
				String prefijo = " AND (";
				for (SelectDTO selectDTO : filtro.getConceptosMovimientos()) {
					if (countIdentif > 0) {
						prefijo = " OR ";
					}
					String[] valores = Helper.getQueryFragmentByOperator(
							":conceptoMovimiento" + countIdentif, 
							selectDTO.getDescription(), 
							selectDTO.getValue());
					consulta.append(
							prefijo + " cpp.referencia " + valores[0]);
					parameters.put("conceptoMovimiento" + countIdentif, valores[1]);
					countIdentif++;
				}
				consulta.append(")");
			}

			// Comentario - COMENTARIO.
			if (CollectionUtils.isNotEmpty(filtro.getComentarios())) {
				int countIdentif = 0;
				String prefijo = " AND (";
				for (SelectDTO selectDTO : filtro.getComentarios()) {
					if (countIdentif > 0) {
						prefijo = " OR ";
					}
					String[] valores = Helper.getQueryFragmentByOperator(
							":comentario" + countIdentif, 
							selectDTO.getDescription(), 
							selectDTO.getValue());
					consulta.append(
							prefijo + " cpp.comentario " + valores[0]);
					parameters.put("comentario" + countIdentif, valores[1]);
					countIdentif++;
				}
				consulta.append(")");
			}

			// Importe Desde.
			if (filtro.getImporteDesde() != null) {
				consulta.append(" AND cpp.importe >= :importeDesde");
				parameters.put("importeDesde", filtro.getImporteDesde());
			}

			// TODO - Contemplar caso en el que importe hasta es 0 en el filtro.
			// Importe Hasta.
			if (filtro.getImporteHasta() != null) {
				consulta.append(" AND cpp.importe <= :importeHasta");
				parameters.put("importeHasta", filtro.getImporteHasta());
			}

			if (esBookingDate) {
				consulta.append( " AND date(cpp.bookingDate) = :bookingDate " );
			}
			// El campo FH. LIQUIDACION en la query es el campo ALC.FEOPELIQ
			if (esValueDate) {
				consulta.append( " AND date(cpp.valueDate) = :valueDate " );

			}

			// Antiguedad
			if (filtro.getListAantiguedad() != null && filtro.getListAantiguedad().size()>0) {
				String listAntiguedad = " AND cpp.antiguedad IN (";
				for (int i=0;i<filtro.getListAantiguedad().size();i++) {
					if (i==filtro.getListAantiguedad().size()-1) {
						listAntiguedad = listAntiguedad + "'"+filtro.getListAantiguedad().get(i).trim()+"'";
					} else {
						listAntiguedad = listAntiguedad + "'"+filtro.getListAantiguedad().get(i).trim()+"',";
					}
				}
				listAntiguedad = listAntiguedad.replace("vacío","");
				listAntiguedad = listAntiguedad + ") ";
				consulta.append(listAntiguedad);
			}

			// Gin
			if (filtro.getGin() != null) {
				consulta.append( " AND cpp.gin = :gin " );
				parameters.put("gin", filtro.getGin());
			}

			// El campo FH. LIQUIDACION en la query es el campo ALC.FEOPELIQ
			if (esValueDate) {
				consulta.append( " AND date(cpp.valueDate) = :valueDate " );
			}

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());

			for (int i = 0; i < sizeFechas; i++) {
				if (esBookingDate) {
					parameters.put("bookingDate", filtro.getListaBookingDate().get(i));
					LOG.info(" ++++++++++ Ejecucion para fecha booking date: " + filtro.getListaBookingDate().get(i) + "  ++++++++++ ");
				}
				if (esValueDate) {
					parameters.put("valueDate", filtro.getListaValueDate().get(i));
					LOG.info(" ++++++++++ Ejecucion para fecha value date: " + filtro.getListaValueDate().get(i) + "  ++++++++++ ");
				}
				TypedQuery<Tmct0ContaPartidasPndts> query = entityManager.createQuery(
						consulta.toString(), Tmct0ContaPartidasPndts.class);

				for (Entry<String, Object> entry : parameters.entrySet()) {
					query.setParameter(entry.getKey(), entry.getValue());
				}			

				List<Tmct0ContaPartidasPndts> resultList = query.getResultList();
				for (int j = 0;j < resultList.size(); j++) {
					listaContaPartidasPndts.add(resultList.get(j));
				}
			}
		} catch (Exception e) {
			LOG.error("Error metodo consultarPartidasPndtsByFilter -  "	+ e.getMessage(), e);
			throw (e);
		}

		long finBusquedaTLM = System.currentTimeMillis();

		LOG.info("Tiempo de busqueda TLM (segs): " + (finBusquedaTLM - inicioBusquedaTLM)/1000);

		LOG.info("FIN - DAOIMPL - consultarPartidasPndtsByFilter");
		return listaContaPartidasPndts;
	}

	/**
	 *	Persistencia de datos en bruto (bulk) para objeto tipado.
	 *	Requiere configurar bundle para determinar cada cuanto tiempo
	 *	hay que hacer flush y clear del entity manager
	 *	
	 *	TODO - Aplicarlo a nivel global una vez hecho el testeo exhaustivo
	 *
	 *	@param entities: lista de entidades
	 *	@return Collection: coleccion tipada  
	 */
	public <T extends Tmct0ContaPartidasPndts> Collection<T> bulkSave(Collection<T> entities) {
		final List<T> savedEntities = new ArrayList<T>(entities.size());
		int i = 0;
		for (T t : entities) {
			savedEntities.add(persistOrMerge(t));
			i++;
			if (i % batchSize == 0) {
				LOG.info(" +++++ Flush & Clear Entity Manager +++++ ");
				entityManager.flush();
				entityManager.clear();
			}
		}
		return savedEntities;
	}

	/**
	 *	Persistencia por tipado utilizado en la funcionalidad de persistencia bulk.  
	 *
	 *	TODO - Aplicarlo a nivel global una vez hecho el testeo exhaustivo
	 *	
	 *	@param t: entidad con tipado
	 *	@return T: entidad con tipado
	 */
	private <T extends Tmct0ContaPartidasPndts> T persistOrMerge(T t) {
		if (t.getId() == null) {
			entityManager.persist(t);
			return t;
		} else {
			return entityManager.merge(t);
		}
	}

}
