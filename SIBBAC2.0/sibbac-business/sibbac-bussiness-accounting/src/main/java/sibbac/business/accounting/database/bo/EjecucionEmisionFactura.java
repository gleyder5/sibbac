package sibbac.business.accounting.database.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.Loggers;
import sibbac.business.accounting.threads.EjecucionPlantillaIndividualRunnable;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.common.utils.SibbacEnums.TipoContaPlantillas;

/**
 * Clase de negocios para la ejecución de plantillas de emisión de factura.
 * 
 * @author Neoris
 *
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EjecucionEmisionFactura extends AbstractEjecucionPlantilla {

  @Override
  protected void setTiposPlantilla() {
    super.tiposPlantilla = new String[] { TipoContaPlantillas.EMISION_FACTURA.getTipo()
    };
  }

  List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

  /*
   * (non-Javadoc)
   * @see sibbac.business.accounting.database.bo.AbstractEjecucionPlantilla# ejecutarPlantillas()
   */
  @Override
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillas(List<Tmct0ContaPlantillas> plantillas) throws EjecucionPlantillaException {

    if (!CollectionUtils.isEmpty(plantillas)) {
      ExecutorService executor = Executors.newFixedThreadPool(plantillas.size());

      for (Tmct0ContaPlantillas plantilla : plantillas) {
        EjecucionPlantillaIndividualRunnable runnable = new EjecucionPlantillaIndividualRunnable(plantilla, this);
        executor.execute(runnable);
      }

      try {
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.HOURS);
      } catch (InterruptedException e) {
        Loggers.logErrorAccounting(plantillas.get(0).getCodigo() != null ? plantillas.get(0).getCodigo() : null,
                                   ConstantesError.ERROR_EJECUCION_HILOS.getValue());
        throw new EjecucionPlantillaException("ERROR ejecutarPlantillas");
      }
    }

    return plantillasSinBalance;
  }

  /**
   * Ejecucion de plantilla individual.
   *
   * @param plantilla plantilla
   * @return List<Tmct0ContaPlantillasDTO>
   * @throws EjecucionPlantillaException
   */
  @Override
  public List<List<Tmct0ContaPlantillasDTO>> ejecutarPlantillaIndividual(Tmct0ContaPlantillas plantilla) throws EjecucionPlantillaException {

    Loggers.logDebugInicioFinAccounting(plantilla != null ? plantilla.getCodigo() : null, true);

    List<List<Tmct0ContaPlantillasDTO>> plantillasSinBalance = new ArrayList<List<Tmct0ContaPlantillasDTO>>();

    try {
      contaPlantillasDaoImp.executeBloqueofacturas(plantilla.getQuery_bloqueo().replace(";", ""),
                                                   plantilla.getEstado_inicio(), plantilla.getEstado_bloqueado(),
                                                   plantilla.getCampo_estado(),
                                                   plantilla != null ? plantilla.getCodigo() : null);
    } catch (Exception ex) {
      // De haber algun error se para la ejecucion completa.
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_BLOQUEO.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    try {
      // Se procesa un grupo de importes en una sola transaccion.
      plantillasSinBalance.addAll(this.tmct0ContaApuntesBo.ejecutarPlantillaEmisionFactura(plantilla));
    } catch (Exception ex) {
      // De haber algun error se para la ejecucion completa.
      Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_PROCESAR_PLANTILLA.getValue() + ": " + ex.getMessage());
      throw new EjecucionPlantillaException();
    }

    // Si la plantilla tiene query_final se ejecuta.
    if ((plantilla.getQuery_final() != null) && (!plantilla.getQuery_final().equals(""))) {
      try {
        this.contaPlantillasDaoImp.executeQueryUpdate(plantilla.getQuery_final().replace(";", ""), plantilla.getCodigo());
      } catch (Exception ex) {
        // De haber algun error se para la ejecucion completa.
        Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_QUERY_FINAL.getValue() + ": " + ex.getMessage());
        throw new EjecucionPlantillaException();
      }
    }

    Loggers.logDebugInicioFinAccounting(plantilla != null ? plantilla.getCodigo() : null, false);

    return plantillasSinBalance;
  }

  public void anadirPlantillas(List<List<Tmct0ContaPlantillasDTO>> lPlantillas) {
    plantillasSinBalance.addAll(lPlantillas);
  }

}
