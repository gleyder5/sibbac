package sibbac.business.accounting.database.dto;

import org.apache.commons.lang.StringUtils;

import sibbac.business.accounting.enums.TiposApuntesManualesExcelEnum;

/**
 * Data Transfer Object para la gestión de Apuntes Manuales del Excel.
 */
public class ApuntesManualesExcelDTO {
	
	// Common.
	private String tipoApunte;
	private String nombreFichero;
	
	// apunte_sibbac - datos ALC
	private String sibbacNuorden;
	private String sibbacNbooking;
	private String sibbacNucnfclt;
	private String sibbacNucnfliq;
	private String sibbacConcepto;
	private String sibbacCodigoPlantilla;
	private String sibbacRefTheirDescRefTheirMayor12;
	private String sibbacReferenciasTheirMenor12;
	private String sibbacReferenciasTheirTotal;
	private int sibbacNroFilaExcel;

	// apunte_saldo0
	private String saldoCodigoPlantilla;
	private String saldoAuxDebe;
	private String saldoConceptoDebe;
	private String saldoImporteDebe;
	private String saldoAuxHaber;
	private String saldoConceptoHaber;
	private String saldoImporteHaber;
	private String saldoImportePyG;
	private String saldoCuentaContableDebe;
	private String saldoCuentaContableHaber;
	private int saldoNroFilaExcel;
	
	public String getNombreFichero() {
		return this.nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}
	
	public String getTipoApunte() {
		return this.tipoApunte;
	}
	
	public void setTipoApunte(String tipoApunte) {
		this.tipoApunte = tipoApunte;
	}
	
	public String getSibbacNuorden() {
		return this.sibbacNuorden;
	}
	
	public void setSibbacNuorden(String sibbacNuorden) {
		this.sibbacNuorden = sibbacNuorden;
	}
	
	public String getSibbacNbooking() {
		return this.sibbacNbooking;
	}
	
	public void setSibbacNbooking(String sibbacNbooking) {
		this.sibbacNbooking = sibbacNbooking;
	}
	
	public String getSibbacNucnfclt() {
		return this.sibbacNucnfclt;
	}
	
	public void setSibbacNucnfclt(String sibbacNucnfclt) {
		this.sibbacNucnfclt = sibbacNucnfclt;
	}
	
	public String getSibbacConcepto() {
		return this.sibbacConcepto;
	}

	public void setSibbacConcepto(String sibbacConcepto) {
		this.sibbacConcepto = sibbacConcepto;
	}

	public String getSibbacNucnfliq() {
		return this.sibbacNucnfliq;
	}
	
	public void setSibbacNucnfliq(String sibbacNucnfliq) {
		this.sibbacNucnfliq = sibbacNucnfliq;
	}
	
	public String getSaldoCodigoPlantilla() {
		return this.saldoCodigoPlantilla;
	}
	
	public void setSaldoCodigoPlantilla(String saldoCodigoPlantilla) {
		this.saldoCodigoPlantilla = saldoCodigoPlantilla;
	}
	
	public String getSaldoAuxDebe() {
		return this.saldoAuxDebe;
	}

	public void setSaldoAuxDebe(String saldoAuxDebe) {
		this.saldoAuxDebe = saldoAuxDebe;
	}
	
	public String getSaldoConceptoDebe() {
		return this.saldoConceptoDebe;
	}

	public void setSaldoConceptoDebe(String saldoConceptoDebe) {
		this.saldoConceptoDebe = saldoConceptoDebe;
	}

	public String getSaldoConceptoHaber() {
		return this.saldoConceptoHaber;
	}

	public void setSaldoConceptoHaber(String saldoConceptoHaber) {
		this.saldoConceptoHaber = saldoConceptoHaber;
	}

	public String getSaldoImporteDebe() {
		return this.saldoImporteDebe;
	}
	
	public void setSaldoImporteDebe(String saldoImporteDebe) {
		this.saldoImporteDebe = saldoImporteDebe;
	}
	
	public String getSaldoImporteHaber() {
		return this.saldoImporteHaber;
	}
	
	public void setSaldoImporteHaber(String saldoImporteHaber) {
		this.saldoImporteHaber = saldoImporteHaber;
	}
	
	public String getSaldoImportePyG() {
		return this.saldoImportePyG;
	}
	
	public void setSaldoImportePyG(String saldoImportePyG) {
		this.saldoImportePyG = saldoImportePyG;
	}

	public String getSaldoAuxHaber() {
		return this.saldoAuxHaber;
	}

	public void setSaldoAuxHaber(String saldoAuxHaber) {
		this.saldoAuxHaber = saldoAuxHaber;
	}

	public String getSibbacCodigoPlantilla() {
		return this.sibbacCodigoPlantilla;
	}

	public void setSibbacCodigoPlantilla(String sibbacCodigoPlantilla) {
		this.sibbacCodigoPlantilla = sibbacCodigoPlantilla;
	}

	public String getSibbacRefTheirDescRefTheirMayor12() {
		return this.sibbacRefTheirDescRefTheirMayor12;
	}

	public void setSibbacRefTheirDescRefTheirMayor12(
			String sibbacRefTheirDescRefTheirMayor12) {
		this.sibbacRefTheirDescRefTheirMayor12 = sibbacRefTheirDescRefTheirMayor12;
	}

	public String getSibbacReferenciasTheirMenor12() {
		return this.sibbacReferenciasTheirMenor12;
	}

	public void setSibbacReferenciasTheirMenor12(
			String sibbacReferenciasTheirMenor12) {
		this.sibbacReferenciasTheirMenor12 = sibbacReferenciasTheirMenor12;
	}

	public String getSibbacReferenciasTheirTotal() {
		return this.sibbacReferenciasTheirTotal;
	}

	public void setSibbacReferenciasTheirTotal(String sibbacReferenciasTheirTotal) {
		this.sibbacReferenciasTheirTotal = sibbacReferenciasTheirTotal;
	}

	public String getSaldoCuentaContableDebe() {
		return this.saldoCuentaContableDebe;
	}

	public void setSaldoCuentaContableDebe(String saldoCuentaContableDebe) {
		this.saldoCuentaContableDebe = saldoCuentaContableDebe;
	}

	public String getSaldoCuentaContableHaber() {
		return this.saldoCuentaContableHaber;
	}

	public void setSaldoCuentaContableHaber(String saldoCuentaContableHaber) {
		this.saldoCuentaContableHaber = saldoCuentaContableHaber;
	}

	public int getSibbacNroFilaExcel() {
		return this.sibbacNroFilaExcel;
	}

	public void setSibbacNroFilaExcel(int sibbacNroFilaExcel) {
		this.sibbacNroFilaExcel = sibbacNroFilaExcel;
	}

	public int getSaldoNroFilaExcel() {
		return this.saldoNroFilaExcel;
	}

	public void setSaldoNroFilaExcel(int saldoNroFilaExcel) {
		this.saldoNroFilaExcel = saldoNroFilaExcel;
	}
	
	/**
	 *	Devuelve true si el tipo de apunte del Excel es SALDO.
	 *	@return boolean boolean 
	 */
	public boolean isExcelTipoApunteSaldo() {
		return StringUtils.isNotBlank(this.getTipoApunte()) 
			&& this.getTipoApunte().equals(
				TiposApuntesManualesExcelEnum.APUNTE_SALDO.getTipoApunteManualExcel());
	}
	
	/**
	 *	Devuelve true si el tipo de apunte del Excel es SIBBAC.
	 *	@return boolean boolean 
	 */
	public boolean isExcelTipoApunteSIBBAC() {
		return StringUtils.isNotBlank(this.getTipoApunte()) 
			&& this.getTipoApunte().equals(
				TiposApuntesManualesExcelEnum.APUNTE_SIBBAC.getTipoApunteManualExcel());
	}
	
}