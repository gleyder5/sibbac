package sibbac.business.accounting.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.stereotype.Repository;

/**
 * Data access object de detalle de conta apunte.
 * 
 * @author Neoris
 *
 */
@Repository
public class Tmct0Norma43MovimientoDaoImp {

	@PersistenceContext
	private EntityManager em;

	public void updateProcesado(final Long idMovimiento, final int procesado) {
		
		em.unwrap(Session.class).doWork(new Work() {

			@Override
			public void execute(Connection conn) throws SQLException {
				
				PreparedStatement update = conn.prepareStatement("UPDATE TMCT0_NORMA43_MOVIMIENTO set procesado = ? where id = ?");
				update.setObject(0, procesado);
				update.setObject(1, idMovimiento);
				update.executeUpdate();

			}
			
		});
		
	}
	
	/**
	 *	Usado para contabilidad.
	 *	Actualiza los registros segun un lista de ids generados por un where relacionado
	 *	a ALCs.
	 *	@param whereQueryCobro whereQueryCobro
	 */
	public void updateByIdsWhereCobro(String whereQueryCobro) {

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("UPDATE TMCT0_NORMA43_MOVIMIENTO M SET M.PROCESADO = 1 WHERE M.ID IN ("
				+ "SELECT MOV.ID FROM TMCT0_NORMA43_MOVIMIENTO MOV INNER JOIN TMCT0_NORMA43_FICHERO FIC ON MOV.FICHERO_ID = FIC.ID ");
		sbQuery.append(whereQueryCobro);
		sbQuery.append(")");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.executeUpdate();
	}

}
