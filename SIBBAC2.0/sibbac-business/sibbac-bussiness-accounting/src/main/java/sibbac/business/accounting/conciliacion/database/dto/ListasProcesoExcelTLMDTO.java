package sibbac.business.accounting.conciliacion.database.dto;

import java.util.ArrayList;
import java.util.List;

import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;

/**
 *	DTO para representar listas del procesamiento Excel de TLM. 
 */
public class ListasProcesoExcelTLMDTO {

	private List<Tmct0ContaPartidasPndts> listaPartidasPendientes; 
	private List<ErroresExcelTLMDTO> listaErrsExcelTLMDTO;

	/**
	 * 	Constructor no-arg.
	 */
	public ListasProcesoExcelTLMDTO() {
		super();
		this.listaPartidasPendientes = new ArrayList<Tmct0ContaPartidasPndts>();
		this.listaErrsExcelTLMDTO = new ArrayList<ErroresExcelTLMDTO>();
	}

	/**
	 *	Constructor con args.
	 *	@param listaPartidasPendientes
	 *	@param listaErrsExcelTLMDTO
	 */
	public ListasProcesoExcelTLMDTO(
			List<Tmct0ContaPartidasPndts> listaPartidasPendientes,
			List<ErroresExcelTLMDTO> listaErrsExcelTLMDTO) {
		super();
		this.listaPartidasPendientes = listaPartidasPendientes;
		this.listaErrsExcelTLMDTO = listaErrsExcelTLMDTO;
	}

	public List<Tmct0ContaPartidasPndts> getListaPartidasPendientes() {
		return this.listaPartidasPendientes;
	}

	public void setListaPartidasPendientes(
			List<Tmct0ContaPartidasPndts> listaPartidasPendientes) {
		this.listaPartidasPendientes = listaPartidasPendientes;
	}

	public List<ErroresExcelTLMDTO> getListaErrsExcelTLMDTO() {
		return this.listaErrsExcelTLMDTO;
	}

	public void setListaErrsExcelTLMDTO(
			List<ErroresExcelTLMDTO> listaErrsExcelTLMDTO) {
		this.listaErrsExcelTLMDTO = listaErrsExcelTLMDTO;
	}

}