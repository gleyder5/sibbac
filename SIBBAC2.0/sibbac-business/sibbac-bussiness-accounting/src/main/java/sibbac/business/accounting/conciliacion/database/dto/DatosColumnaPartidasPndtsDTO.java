package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.common.ExcelProcessingHelper;
import sibbac.common.utils.SibbacEnums.TiposDatosColsTblsEnum;

/**
 *	DTO para representar datos de las columnas de partidas pendientes. 
 */
public class DatosColumnaPartidasPndtsDTO {
	
	private static final Logger LOG = LoggerFactory.getLogger(DatosColumnaPartidasPndtsDTO.class);

	private String name;
	private String tbName;
	private String coltype;
	private Short length;
	
	/**
	 * 	Constructor no-arg.
	 */
	public DatosColumnaPartidasPndtsDTO() {
		super();
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTbName() {
		return this.tbName;
	}

	public void setTbName(String tbName) {
		this.tbName = tbName;
	}

	public String getColtype() {
		return this.coltype;
	}

	public void setColtype(String coltype) {
		this.coltype = coltype;
	}

	public Short getLength() {
		return this.length;
	}

	public void setLength(Short length) {
		this.length = length;
	}

	/**
	 * 	En caso de que haya algun error en la conversion de tipo de
	 * 	dato devuelve un mensaje acorde, caso contrario devuelve null.
	 * 	
	 * 	Tipos de datos admisibles para conta partidas pendientes:
	 * 	BIGINT
	 * 	DECIMAL
	 * 	SMALLINT
	 * 	TIMESTMP	
	 * 	VARCHAR
	 * 
	 * 	@param celda
	 * 	@return String 	
	 */
	public String checkMensajeTipoDatoByCelda(Cell celda) {
		// Validacion de los datos de la celda del Excel.	
		String valorCelda = null;
		try {
			valorCelda = ExcelProcessingHelper.getCellValueTLM(celda);
		} catch (Exception e) {
			return "Error al tomar datos de la celda del Excel";
		}
		return this.checkMensajeTipoDatoByCadena(valorCelda);
	}
	
	/**
	 *	Chequeo de mensaje de tipo de dato por la cadena de caracteres.
	 *	@param valorCelda
	 *	@return String
	 */
	public String checkMensajeTipoDatoByCadena(String valorCelda) {
		// Validacion por tipo de dato y longitud.
		if (this.coltype.equals(TiposDatosColsTblsEnum.BIGINT.getTipo())) {
			try {
				BigInteger celdaBigInt = new BigInteger(valorCelda);
//				LOG.info("Conversion exitosa: " + celdaBigInt);
			} catch (Exception e) {
				return "Error al convertir expresión a BigInteger";
			}
		} else if (this.coltype.equals(TiposDatosColsTblsEnum.SMALLINT.getTipo())) {
			try {
				Integer celdaInteger = Integer.valueOf(valorCelda);
//				LOG.info("Conversion exitosa: " + celdaInteger);
			} catch (Exception e) {
				return "Error al convertir expresión a Integer";
			}
		} else if (this.coltype.equals(TiposDatosColsTblsEnum.DECIMAL.getTipo())) {
			try {
				BigDecimal celdaBigDecimal = new BigDecimal(valorCelda);
//				LOG.info("Conversion exitosa: " + celdaBigDecimal);
			} catch (Exception e) {
				return "Error al convertir expresión a BigDecimal";
			}
		} else if (this.coltype.equals(TiposDatosColsTblsEnum.TIMESTMP.getTipo())) {
			try {
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				df.parse(valorCelda);
			} catch (Exception e) {
				return "Error al convertir expresión a Date";
			}
		} else if (this.coltype.equals(TiposDatosColsTblsEnum.VARCHAR.getTipo())) {
			if (StringUtils.isNotBlank(valorCelda) && this.length < valorCelda.length()) {
				return "La celda del fichero Excel tiene una longitud demasiado larga";
			}
		}
		return null;
	}

}