package sibbac.business.accounting.threads;

import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0ContaPlantillasConciliaBo;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirDTO;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirTablaDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPlantillasConcilia;

public class EjecutaConciliaionOurTheir implements Callable<List<FiltroOurConTheirTablaDTO>> {


	  private final Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

	  private final List<Tmct0ContaPlantillasConcilia> listaPlantillasConcilia;
	  
	  private final List<Tmct0ContaPlantillasConcilia> listaPlantillasConciliaDef;
	  
	  private final FiltroOurConTheirDTO filtro;


	  public EjecutaConciliaionOurTheir( Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo,
			  								List<Tmct0ContaPlantillasConcilia> listaPlantillasConcilia,
			  									List<Tmct0ContaPlantillasConcilia> listaPlantillasConciliaDef, 
			  										FiltroOurConTheirDTO filtro) {
			this.tmct0ContaPlantillasConciliaBo = tmct0ContaPlantillasConciliaBo;
		    this.listaPlantillasConcilia = listaPlantillasConcilia;
		    this.listaPlantillasConciliaDef = listaPlantillasConciliaDef;
		    this.filtro = filtro;
	  }

	  @Override
	  public List<FiltroOurConTheirTablaDTO> call() throws Exception{
	      
	      return tmct0ContaPlantillasConciliaBo.getResultadosOurTheir(listaPlantillasConcilia, listaPlantillasConciliaDef, filtro);

	  }
}
