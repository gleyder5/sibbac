/**
 * 
 */
package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;


/**
 * @author lucio.vilar
 *
 */
public class Tmct0ContaOurApuntesDTO {

	
	/**
	 * origen
	 */
	private String nuorden;
	
	/**
	 * tipo movimiento
	 */
	private String nbooking;
	
	/**
	 * auxiliar
	 */
	private String nucnfclt;
	
	/**
	 * concepto
	 */
	private BigDecimal nucnfliq;
	
	private String auxiliar;
	
	private String plantilla;
	
	private BigDecimal importe;
	
	public Tmct0ContaOurApuntesDTO() {
		
	}
	
	public Tmct0ContaOurApuntesDTO(String orden, String booking, String cnfclt, BigDecimal cnfliq, String unaPlantilla, String unAux) {
		this.nbooking = booking;
		this.nucnfclt = cnfclt;
		this.nucnfliq = cnfliq;
		this.nuorden = orden;
		this.auxiliar = unAux;
		this.plantilla = unaPlantilla;
	}

	/**
	 * @return the nuorden
	 */
	public String getNuorden() {
		return nuorden;
	}

	/**
	 * @param nuorden the nuorden to set
	 */
	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}

	/**
	 * @return the nbooking
	 */
	public String getNbooking() {
		return nbooking;
	}

	/**
	 * @param nbooking the nbooking to set
	 */
	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}

	/**
	 * @return the nucnfclt
	 */
	public String getNucnfclt() {
		return nucnfclt;
	}

	/**
	 * @param nucnfclt the nucnfclt to set
	 */
	public void setNucnfclt(String nucnfclt) {
		this.nucnfclt = nucnfclt;
	}

	/**
	 * @return the nucnfliq
	 */
	public BigDecimal getNucnfliq() {
		return nucnfliq;
	}

	/**
	 * @param nucnfliq the nucnfliq to set
	 */
	public void setNucnfliq(BigDecimal nucnfliq) {
		this.nucnfliq = nucnfliq;
	}

	/**
	 * @return the auxiliar
	 */
	public String getAuxiliar() {
		return auxiliar;
	}

	/**
	 * @param auxiliar the auxiliar to set
	 */
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}

	/**
	 * @return the plantilla
	 */
	public String getPlantilla() {
		return plantilla;
	}

	/**
	 * @param plantilla the plantilla to set
	 */
	public void setPlantilla(String plantilla) {
		this.plantilla = plantilla;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

}
