package sibbac.business.accounting.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.database.model.Tmct0AccountingValidation;

/**
 * Data access object Impl de Tmct0AccountingValidation.
 */
@Repository
public class Tmct0AccountingValidationDaoImp {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0AccountingValidationDaoImp.class);

	@PersistenceContext
	private EntityManager em;

	public List<Object> consultarColumna(String nomColumna, String nomTabla) {

		LOG.info("INICIO - DAOIMPL - consultarColumna");
		Query query = null;
		List<Object> resultList = null;

		List<Tmct0AccountingValidation> listaTablasColumna = new ArrayList<Tmct0AccountingValidation>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT STRIP(TBNAME) "
					+ "FROM SYSIBM.SYSCOLUMNS "
					+ "WHERE NAME = :nomColumna "
					+ "AND TBNAME = :nomTabla");

			parameters.put("nomColumna", nomColumna);
			parameters.put("nomTabla", nomTabla);

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			query = em.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

		     resultList = query.getResultList();


		} catch (Exception e) {
			LOG.error("Error metodo consultarColumna -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarColumna");

		return resultList;
	}
}
