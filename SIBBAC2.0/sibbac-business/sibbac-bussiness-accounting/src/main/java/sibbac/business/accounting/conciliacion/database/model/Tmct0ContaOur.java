package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * Entidad para la gestion de TMCT0_CONTA_OUR.
 */
@Entity
@Table(name = "TMCT0_CONTA_OUR")
public class Tmct0ContaOur {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;

	@Column(name = "NUORDEN", length = 20, nullable = true)
	private String nuOrden;

	@Column(name = "NBOOKING", length = 20, nullable = true)
	private String nBooking;

	@Column(name = "NUCNFCLT", length = 20, nullable = true)
	private String nuCnfclt;

	@Column(name = "NUCNFLIQ", nullable = true, precision = 4)
	private BigDecimal nuCnfliq;

	@Column(name = "ID_FALLIDO", length = 8, nullable = true)
	private BigInteger idFallido;

	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_CONCILIACION_AUDIT")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaConciliacionAudit conciliacionAudit;

	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_APUNTES_PNDTS")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaApuntesPndts apuntesPndts;

	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_APUNTES_AUDIT")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0ContaApuntesAudit apuntesAudit;
	
	@Column(name = "IMPORTE", nullable = true, precision = 18, scale = 4)
	private BigDecimal importe;
	
	@Column(name = "SENTIDO", length = 20, nullable = true)
	private String sentido;
	
	@Column(name = "AUX_BANCARIO", length = 10, nullable = true)
	private String auxBancario;
	
	@Column(name = "CODPLANTILLA", length = 50, nullable = true)
	private String codPlantilla;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaOur() {
		super();
	}

	/**
	 * @param id
	 * @param codigo
	 * @param tipo
	 * @param subtipo
	 * @param auxiliar
	 * @param tolerance
	 * @param selectQuery
	 * @param fromQuery
	 * @param whereQuery
	 * @param havingQuery
	 * @param groupBy
	 * @param orderBy
	 */
	public Tmct0ContaOur(BigInteger id, String nuOrden, String nBooking,
			String nuCnfclt, BigDecimal nuCnfliq, BigInteger idFallido, Tmct0ContaConciliacionAudit conciliacionAudit,
			Tmct0ContaApuntesPndts apuntesPndts, Tmct0ContaApuntesAudit apuntesAudit, BigDecimal importe, String sentido,
			String auxBancario, String codPlantilla) {
		super();
		this.id = id;
		this.nuOrden = nuOrden;
		this.nBooking = nBooking;
		this.nuCnfclt = nuCnfclt;
		this.nuCnfliq = nuCnfliq;
		this.conciliacionAudit = conciliacionAudit;
		this.apuntesPndts = apuntesPndts;
		this.apuntesAudit = apuntesAudit;
		this.idFallido = idFallido;
		this.importe = importe;
		this.sentido = sentido;
		this.auxBancario = auxBancario;
		this.codPlantilla = codPlantilla;
	}

	/**
	 * @return the auxBancario
	 */
	public String getAuxBancario() {
		return auxBancario;
	}

	/**
	 * @param auxBancario the auxBancario to set
	 */
	public void setAuxBancario(String auxBancario) {
		this.auxBancario = auxBancario;
	}

	/**
	 * @return the codPlantilla
	 */
	public String getCodPlantilla() {
		return codPlantilla;
	}

	/**
	 * @param codPlantilla the codPlantilla to set
	 */
	public void setCodPlantilla(String codPlantilla) {
		this.codPlantilla = codPlantilla;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the sentido
	 */
	public String getSentido() {
		return sentido;
	}

	/**
	 * @param sentido the sentido to set
	 */
	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the nuOrden
	 */
	public String getNuOrden() {
		return nuOrden;
	}

	/**
	 * @param nuOrden the nuOrden to set
	 */
	public void setNuOrden(String nuOrden) {
		this.nuOrden = nuOrden;
	}

	/**
	 * @return the nBooking
	 */
	public String getnBooking() {
		return nBooking;
	}

	/**
	 * @param nBooking the nBooking to set
	 */
	public void setnBooking(String nBooking) {
		this.nBooking = nBooking;
	}

	/**
	 * @return the nuCnfclt
	 */
	public String getNuCnfclt() {
		return nuCnfclt;
	}

	/**
	 * @param nuCnfclt the nuCnfclt to set
	 */
	public void setNuCnfclt(String nuCnfclt) {
		this.nuCnfclt = nuCnfclt;
	}

	/**
	 * @return the nuCnfliq
	 */
	public BigDecimal getNuCnfliq() {
		return nuCnfliq;
	}

	/**
	 * @param nuCnfliq the nuCnfliq to set
	 */
	public void setNuCnfliq(BigDecimal nuCnfliq) {
		this.nuCnfliq = nuCnfliq;
	}

	/**
	 * @return the conciliacionAudit
	 */
	public Tmct0ContaConciliacionAudit getConciliacionAudit() {
		return conciliacionAudit;
	}

	/**
	 * @param conciliacionAudit the conciliacionAudit to set
	 */
	public void setConciliacionAudit(Tmct0ContaConciliacionAudit conciliacionAudit) {
		this.conciliacionAudit = conciliacionAudit;
	}

	/**
	 * @return the apuntesPndts
	 */
	public Tmct0ContaApuntesPndts getApuntesPndts() {
		return apuntesPndts;
	}

	/**
	 * @param apuntesPndts the apuntesPndts to set
	 */
	public void setApuntesPndts(Tmct0ContaApuntesPndts apuntesPndts) {
		this.apuntesPndts = apuntesPndts;
	}

	/**
	 * @return the apuntesAudit
	 */
	public Tmct0ContaApuntesAudit getApuntesAudit() {
		return apuntesAudit;
	}

	/**
	 * @param apuntesAudit the apuntesAudit to set
	 */
	public void setApuntesAudit(Tmct0ContaApuntesAudit apuntesAudit) {
		this.apuntesAudit = apuntesAudit;
	}

	/**
	 * @return the idFallido
	 */
	public BigInteger getIdFallido() {
		return idFallido;
	}

	/**
	 * @param idFallido the idFallido to set
	 */
	public void setIdFallido(BigInteger idFallido) {
		this.idFallido = idFallido;
	}

}
