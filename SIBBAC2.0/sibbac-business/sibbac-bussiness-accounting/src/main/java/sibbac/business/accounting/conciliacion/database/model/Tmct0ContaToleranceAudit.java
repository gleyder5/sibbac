package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_CONTA_TOLERANCE_AUDIT.
 */
@Entity
@Table(name = "TMCT0_CONTA_TOLERANCE_AUDIT")
public class Tmct0ContaToleranceAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
 
	@Column(name = "CODPLANTILLA", length = 50, nullable = true)
	private String codPlantilla;
	
	@Column(name = "TOLERANCE", length = 8, nullable = true)
	private Integer tolerance;
	
	@Column(name = "AUDIT_DATE", length = 10, nullable = true)
	private Timestamp auditDate;
	
	@Column(name = "AUDIT_USER", nullable = true, precision = 18, scale = 4)
	private String auditUser;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaToleranceAudit() {
		super();
	}

	/**
	 * @param id
	 * @param estado
	 * @param feApunte
	 * @param usuApunte
	 * @param concepto
	 * @param importeDebe
	 * @param importeHaber
	 * @param importePyG
	 */
	public Tmct0ContaToleranceAudit(BigInteger id, String codPlantilla,
			Integer tolerance, Timestamp auditDate, String auditUser) {
		super();
		this.id = id;
		this.codPlantilla=codPlantilla;
		this.tolerance = tolerance;
		this.auditDate = auditDate;
		this.auditUser = auditUser;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the codPlantilla
	 */
	public String getCodPlantilla() {
		return codPlantilla;
	}

	/**
	 * @param codPlantilla the codPlantilla to set
	 */
	public void setCodPlantilla(String codPlantilla) {
		this.codPlantilla = codPlantilla;
	}

	/**
	 * @return the tolerance
	 */
	public Integer getTolerance() {
		return tolerance;
	}

	/**
	 * @param tolerance the tolerance to set
	 */
	public void setTolerance(Integer tolerance) {
		this.tolerance = tolerance;
	}

	/**
	 * @return the auditDate
	 */
	public Timestamp getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(Timestamp auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

}
