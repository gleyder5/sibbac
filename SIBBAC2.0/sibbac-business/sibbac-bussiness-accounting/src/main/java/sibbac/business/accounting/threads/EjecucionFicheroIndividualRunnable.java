package sibbac.business.accounting.threads;

import java.io.File;

import sibbac.business.accounting.database.bo.EjecucionApuntesManuales;

/**
 *	Representa hilo que ejecuta un fichero Excel individual para 
 *	tipo apunte manual.
 */
public class EjecucionFicheroIndividualRunnable implements Runnable {

	private EjecucionApuntesManuales ejecucionApuntesManuales;
	private File fichero;

	/**
	 * 	Constructor.
	 *	@param fichero fichero	
	 * 	@param ejecucionApuntesManuales
	 *            Instancia de ejecución de fichero.
	 */
	public EjecucionFicheroIndividualRunnable(File fichero,
		EjecucionApuntesManuales ejecucionApuntesManuales) {
		this.fichero = fichero;
		this.ejecucionApuntesManuales = ejecucionApuntesManuales;
	}

	@Override
	public void run() {
		try {
			this.ejecucionApuntesManuales.ejecutarFicheroIndividual(fichero);
			this.ejecucionApuntesManuales.shutdownFicheroIndividual(this, false);
		} catch (Exception e) {
			this.ejecucionApuntesManuales.shutdownFicheroIndividual(this, true);
		}
	}
}