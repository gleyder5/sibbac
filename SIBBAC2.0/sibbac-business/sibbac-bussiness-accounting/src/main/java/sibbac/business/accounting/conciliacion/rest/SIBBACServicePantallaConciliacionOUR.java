package sibbac.business.accounting.conciliacion.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.accounting.conciliacion.database.bo.Tmct0ContaPlantillasConciliaBo;
import sibbac.business.accounting.conciliacion.database.bo.Tmct0PantallaConciliacionOURBo;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPlantillasConciliaDao;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirDTO;
import sibbac.business.accounting.conciliacion.database.dto.FiltroOurConTheirTablaDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0PantallaConciliacionOURAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPlantillasConcilia;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.threads.EjecutaConciliaionOurTheir;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServicePantallaConciliacionOUR implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServicePantallaConciliacionOUR.class);

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	@Autowired
	private Tmct0PantallaConciliacionOURAssembler tmct0PantallaConciliacionOURAssembler;

	@Autowired
	private Tmct0ContaPlantillasConciliaDao tmct0ContaPlantillasConciliaDao;

	@Autowired
	private Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

	@Autowired
	Tmct0PantallaConciliacionOURBo tmct0PantallaConciliacionOURBo;

	@Autowired
	private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;
	
	@Autowired
	private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;
	
	@Value("${generacioninformes.timeout}")
	private String timeout;

	/**
	 * The Enum ComandoFacturasManuales.
	 */
	private enum ComandoConciliacionOUR {

		/** Consultar los apuntes pendientes */
		CONSULTAR_OUR_THEIR("consultarOurConTheir"),

		GENERAR_APUNTE("generarApuntes"),

		SIN_COMANDO("");

		/** The command. */
		private String command;

		/**
		 * Instantiates a new comandos Plantillas.
		 *
		 * @param command the command
		 */
		private ComandoConciliacionOUR(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * Process.
	 *
	 * @param webRequest the web request
	 * @return the web response
	 * @throws SIBBACBusinessException the SIBBAC business exception
	 */
	@Override
	public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		Map<String, String> filters = webRequest.getFilters();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandoConciliacionOUR command = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceControlContabilidad");

			switch (command) {
			case CONSULTAR_OUR_THEIR:
				result.setResultados(consultarOurConTheir(paramsObjects, filters));
				break;
			case GENERAR_APUNTE:
				result.setResultados(exportarAExcel(paramsObjects));
				break;
			default:
				result.setError("An action must be set. Valid actions are: " + command);
			} // switch
		} catch (SIBBACBusinessException e) {
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

		LOG.debug("Salgo del servicio SIBBACServiceControlContabilidad");
		return result;
	}

	/**
	 * Consulta de Our para casar con Their
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarOurConTheir(Map<String, Object> paramObjects, Map<String, String> filters) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - consultarOurConTheir");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Tmct0ContaPlantillasConcilia> listaPlantillasConcilia = new ArrayList<Tmct0ContaPlantillasConcilia>();
		List<Tmct0ContaPlantillasConcilia> listaPlantillasConciliaDef = new ArrayList<Tmct0ContaPlantillasConcilia>();

		try {
			// Transformamos el filtro a objeto DTO para manejar los datos
			FiltroOurConTheirDTO filtro = tmct0PantallaConciliacionOURAssembler.getFiltroOurConTheirDTO(paramObjects, filters);

			// Recuperamos un listado de las querys de la tabla TMCT0_CONTA_PLANTILLAS_CONCILIA asociadas a la lista de auxiliares que nos llega
			listaPlantillasConcilia = tmct0ContaPlantillasConciliaDao.findByAux(filtro.getListAux());
			for(Tmct0ContaPlantillasConcilia contaPlantillasConcilia: listaPlantillasConcilia){
				if ((filtro.getSentido().equalsIgnoreCase("COMPRA") || filtro.getSentido().equalsIgnoreCase("VENTA")) || filtro.getSentido().equalsIgnoreCase("COMPRA/VENTA")) {
					if (contaPlantillasConcilia.getCodigo().startsWith("LQEFE")) {
						listaPlantillasConciliaDef.add(contaPlantillasConcilia);
					}
				}else if ((filtro.getSentido().equalsIgnoreCase("ENTREGA") || filtro.getSentido().equalsIgnoreCase("RECEPCIÓN") || filtro.getSentido().equalsIgnoreCase("ENTREGA/RECEPCIÓN"))) {
					if (contaPlantillasConcilia.getCodigo().startsWith("LQNET")) {
						listaPlantillasConciliaDef.add(contaPlantillasConcilia);
					}
				}
				if (!contaPlantillasConcilia.getCodigo().startsWith("LQNET") && !contaPlantillasConcilia.getCodigo().startsWith("LQEFE")) {
					if (filtro.getSentido().equalsIgnoreCase("COMPRA") || filtro.getSentido().equalsIgnoreCase("VENTA") || filtro.getSentido().equalsIgnoreCase("COMPRA/VENTA")){
						listaPlantillasConciliaDef.add(contaPlantillasConcilia);
					}
				}
			}
			
			if(!listaPlantillasConciliaDef.isEmpty()){
				
			    Callable<List<FiltroOurConTheirTablaDTO>> call = new EjecutaConciliaionOurTheir(tmct0ContaPlantillasConciliaBo,
			    																	listaPlantillasConcilia,
			    																	listaPlantillasConciliaDef, 
			    																	filtro);
			    ExecutorService executor = Executors.newFixedThreadPool(1);
			    Future<List<FiltroOurConTheirTablaDTO>> future = executor.submit(call);
			    executor.shutdown();
			    
			    try{
			    	result.put("listaOurConTheir", future.get(Long.valueOf(timeout), TimeUnit.MINUTES));
			    } catch (InterruptedException e) {
			    	result.put("timeout", "El timeout del servidor cortó la petición debido al alto volumen de datos en la ejecucion de la query para el dia " + filters.get("fecha") +".");
			    } catch (ExecutionException e) {
			    	result.put("timeout", "El timeout del servidor cortó la petición debido al alto volumen de datos en la ejecucion de la query para el dia " + filters.get("fecha") +".");
			    }catch(TimeoutException e){
			    	result.put("timeout", "El timeout del servidor cortó la petición debido al alto volumen de datos en la ejecucion de la query para el dia " + filters.get("fecha") +".");
			    }catch(Exception e){
			    	result.put("timeout", "El timeout del servidor cortó la petición debido al alto volumen de datos en la ejecucion de la query para el dia " + filters.get("fecha") +".");
			    }
			
			}
			result.put("status", "OK");
		} catch (SIBBACBusinessException e) {
			LOG.error("Error metodo consultarOurConTheir -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		} catch (Exception e) {
			LOG.error("Error metodo consultarOurConTheir -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - consultarOurConTheir");

		return result;
	}

	private Map<String, Object> exportarAExcel(Map<String, Object> paramsObjects) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// Se valida que no haya otros usuarios cargando ficheros Excel TLM.
			Tmct0SibbacConstantes semafCargaExcelTLM = this.tmct0SibbacConstantesDao.findByClaveAndValor(
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave(),
					SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado());
			if (semafCargaExcelTLM != null) {
				result.put("status", "KOSemaforo");
				result.put("error", "Un nuevo excel está siendo cargado por otro usuario. Es posible que sus datos estén desactualizados, por favor vuelva a realizar la consulta en unos segundos.");
				LOG.error("No se pueden generar apuntes ya que hay usuarios cargando ficheros TLM.");
				return result;
			}
			
			tmct0PantallaConciliacionOURBo.generarOurConTheir(paramsObjects);

			result.put("status", "OK");
			
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			
		}catch (Exception e) {
			// Se desactiva el semaforo de generacion de apuntes.
			this.tmct0SibbacConstantesBo.updateValorByClave(
					SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(), 
					PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());
			LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}
		return result;
	}

	/**
	 * Gets the command.
	 *
	 * @param command the command
	 * @return the command
	 */
	private ComandoConciliacionOUR getCommand(String command) {
		ComandoConciliacionOUR[] commands = ComandoConciliacionOUR.values();
		ComandoConciliacionOUR result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandoConciliacionOUR.SIN_COMANDO;
		}
		LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

}
