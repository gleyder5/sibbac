package sibbac.business.accounting.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_NORMA43_DETALLE
 */
@Entity
@Table(name = "TMCT0_NORMA43_DETALLE")
public class Tmct0Norma43Detalle {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;

	@Column(name = "IDMOVIMIENTO", length = 8, nullable = false)
	private Integer idmovimiento;

	@Column(name = "PROPUESTA", nullable = false, length = 10)
	private String propuesta;

	@Column(name = "FECHA", nullable = false)
	private Date fecha;

	@Column(name = "IMPORTE", nullable = false, precision = 18, scale = 4)
	private BigDecimal importe;

	@Column(name = "TIPO_APUNTE", nullable = false, length = 10)
	private String tipoApunte;
	
	@Column(name = "ISIN", nullable = false, length = 12)
	private String isin;
	
	@Column(name = "TIPO_COMISION", nullable = false, length = 10)
	private String tipoComision;

	public Tmct0Norma43Detalle() {
	}

	public Tmct0Norma43Detalle(Integer id, Integer idmovimiento,
			String propuesta, Date fecha, BigDecimal importe,
			String tipoApunte, String isin, String tipoComision) {
		super();
		this.id = id;
		this.idmovimiento = idmovimiento;
		this.propuesta = propuesta;
		this.fecha = fecha;
		this.importe = importe;
		this.tipoApunte = tipoApunte;
		this.isin = isin;
		this.tipoComision = tipoComision;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdmovimiento() {
		return this.idmovimiento;
	}

	public void setIdmovimiento(Integer idmovimiento) {
		this.idmovimiento = idmovimiento;
	}

	public String getPropuesta() {
		return this.propuesta;
	}

	public void setPropuesta(String propuesta) {
		this.propuesta = propuesta;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getImporte() {
		return this.importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getTipoApunte() {
		return this.tipoApunte;
	}

	public void setTipoApunte(String tipoApunte) {
		this.tipoApunte = tipoApunte;
	}

	public String getIsin() {
		return this.isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getTipoComision() {
		return this.tipoComision;
	}

	public void setTipoComision(String tipoComision) {
		this.tipoComision = tipoComision;
	}

}
