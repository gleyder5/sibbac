package sibbac.business.accounting.conciliacion.database.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO para representar datos de la grilla TLM
 */
public class GrillaTLMDTO {

	private Long id;
	private String antiguedad;
	private String tipoDeMovimiento;
	private String AuxiliarBancario;
	private String conceptoMovimiento;
	private String numeroDocumento;
	private String comentario;
	private Date bookingDate;
	private Date valueDate;
	private BigDecimal importe;
	private Integer gin;
	private String comentarioUsuario;
	private String currency;
	private String departamento;
	private String numeroDocumento2;
	private String referencia;
	private String referencia2;

	/**
	 * Constructor no-arg.
	 */
	public GrillaTLMDTO() {
		super();
	}

	public GrillaTLMDTO(Long id, String antiguedad, String tipoDeMovimiento, String auxiliarBancario,
			String conceptoMovimiento, String numeroDocumento, String comentario, Date bookingDate, Date valueDate,
			BigDecimal importe,Integer gin, String comentarioUsuario, String currency, String departamento,
			String numeroDocumento2, String referencia, String referencia2) {
		super();
		this.id = id;
		this.antiguedad = antiguedad;
		this.tipoDeMovimiento = tipoDeMovimiento;
		this.AuxiliarBancario = auxiliarBancario;
		this.conceptoMovimiento = conceptoMovimiento;
		this.numeroDocumento = numeroDocumento;
		this.comentario = comentario;
		this.bookingDate = bookingDate;
		this.valueDate = valueDate;
		this.importe = importe;
		this.gin=gin;
		this.comentarioUsuario=comentarioUsuario;
		this.currency=currency;
		this.departamento=departamento;
		this.numeroDocumento2=numeroDocumento2;
		this.referencia=referencia;
		this.referencia2=referencia2;
	}

	/**
	 * @return the comentarioUsuario
	 */
	public String getComentarioUsuario() {
		return comentarioUsuario;
	}

	/**
	 * @param comentarioUsuario the comentarioUsuario to set
	 */
	public void setComentarioUsuario(String comentarioUsuario) {
		this.comentarioUsuario = comentarioUsuario;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * @return the numeroDocumento2
	 */
	public String getNumeroDocumento2() {
		return numeroDocumento2;
	}

	/**
	 * @param numeroDocumento2 the numeroDocumento2 to set
	 */
	public void setNumeroDocumento2(String numeroDocumento2) {
		this.numeroDocumento2 = numeroDocumento2;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the referencia2
	 */
	public String getReferencia2() {
		return referencia2;
	}

	/**
	 * @param referencia2 the referencia2 to set
	 */
	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	/**
	 * @return the gin
	 */
	public Integer getGin() {
		return gin;
	}

	/**
	 * @param gin the gin to set
	 */
	public void setGin(Integer gin) {
		this.gin = gin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(String antiguedad) {
		this.antiguedad = antiguedad;
	}

	public String getTipoDeMovimiento() {
		return tipoDeMovimiento;
	}

	public void setTipoDeMovimiento(String tipoDeMovimiento) {
		this.tipoDeMovimiento = tipoDeMovimiento;
	}

	public String getAuxiliarBancario() {
		return AuxiliarBancario;
	}

	public void setAuxiliarBancario(String auxiliarBancario) {
		AuxiliarBancario = auxiliarBancario;
	}

	public String getConceptoMovimiento() {
		return conceptoMovimiento;
	}

	public void setConceptoMovimiento(String conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

}