package sibbac.business.accounting.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import sibbac.business.accounting.database.dao.Tmct0ContaApunteDetalleDao;
import sibbac.business.accounting.database.dao.Tmct0ContaApunteDetalleDaoImp;
import sibbac.business.accounting.database.dto.Tmct0ContaPlantillasDTO;
import sibbac.business.accounting.database.model.Tmct0ContaApunteDetalle;
import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;
import sibbac.business.accounting.exception.EjecucionPlantillaException;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.accounting.helper.Loggers;
import sibbac.common.utils.SibbacEnums.ConstantesError;
import sibbac.database.bo.AbstractBo;

/**
 * Business object de detalle de conta apunte.
 * 
 * @author aldo.saia
 *
 */
@Service
public class Tmct0ContaApunteDetalleBo extends AbstractBo<Tmct0ContaApunteDetalle, Integer, Tmct0ContaApunteDetalleDao> {

  @Autowired
  private Tmct0ContaApunteDetalleDaoImp tmct0ContaApunteDetalleDao;

  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectCobrosLiquidaciones(Tmct0ContaPlantillas plantilla,
                                              Integer idApunte,
                                              List<Object> paramsDetail,
                                              List<String> listImportDet, 
                                              int iImporte) throws EjecucionPlantillaException {

    try {

      // Se genera y se guarda el detalle del apunte.
      if (!StringUtils.isEmpty(plantilla.getGrabaDetalle())) {
        tmct0ContaApunteDetalleDao.insertSelectCobrosLiquidaciones(plantilla.getQuery_apunte_detalle(), idApunte, paramsDetail, listImportDet, plantilla.getCodigo(), iImporte);
      }

    } catch (PersistenceException ex) {
    	Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_DETALLE.getValue()
    			+ ": " + ex.getMessage());
    	throw new EjecucionPlantillaException(ConstantesError.ERROR_APUNTES_DETALLE.getValue(), ex);
    }
  }
  
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void generarApuntesDetalleLiquidacionesParciales(Tmct0ContaPlantillas plantilla,
                                              Integer idApunte,
                                              List<Object> paramsDetail,
                                              BigDecimal importeCobroRest,
                                              List<String> listImportDet, int iImporte) throws EjecucionPlantillaException {

    try {

      // Se genera y se guarda el detalle del apunte.
      if (!StringUtils.isEmpty(plantilla.getGrabaDetalle())) {
        tmct0ContaApunteDetalleDao.generarApuntesDetalleLiquidacionesParciales(plantilla.getQuery_apunte_detalle(), idApunte,
        																		paramsDetail, importeCobroRest, listImportDet, plantilla.getCodigo(), iImporte);
      }

    } catch (PersistenceException ex) {
    	Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_DETALLE.getValue()
    			+ ": " + ex.getMessage());
    	throw new EjecucionPlantillaException(ConstantesError.ERROR_APUNTES_DETALLE.getValue(), ex);
    }
  }

  /**
   *	Apuntes del detalle para funcionalidad Devengos.
   *	@param plantilla
   *	@param idApunte
   *	@param fechaEjecucion
   *	@param listImportDet
   *	@param iImporte
   *	@param isFromApuntesManuales: true si viene de la funcionalidad de apuntes manuales   
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectDevengos(Tmct0ContaPlantillas plantilla,
                                   Integer idApunte,
                                   List<String> listImportDet,
                                   List<Object> paramsDetail,
                                   int iImporte, 
                                   Boolean isFromApuntesManuales) throws EjecucionPlantillaException {

    try {

      // Se genera y se guarda el detalle del apunte.
      if (!StringUtils.isEmpty(plantilla.getGrabaDetalle())) {
        tmct0ContaApunteDetalleDao.insertSelectDevengos(plantilla.getQuery_apunte_detalle().replace(";", ""), idApunte,
                                                        plantilla.getEstado_bloqueado(), listImportDet,
                                                        plantilla != null ? plantilla.getCodigo() : null, paramsDetail, iImporte, isFromApuntesManuales);
      }

    } catch (PersistenceException ex) {
    	Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_DETALLE.getValue()
    			+ ": " + ex.getMessage());
    	throw new EjecucionPlantillaException(ConstantesError.ERROR_APUNTES_DETALLE.getValue(), ex);
    }
  }

  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectDevengosFallidos(Tmct0ContaPlantillas plantilla,
                                           Integer idApunte,
                                           List<String> listImportDet,
                                           List<Object> paramsDetail,
                                           int iImporte) throws EjecucionPlantillaException {

    try {

      // Se genera y se guarda el detalle del apunte.
      if (!StringUtils.isEmpty(plantilla.getGrabaDetalle())) {
        tmct0ContaApunteDetalleDao.insertSelectDevengosFallidos(plantilla.getQuery_apunte_detalle().replace(";", ""), idApunte, plantilla.getEstado_bloqueado(),
                                                                listImportDet, paramsDetail, plantilla != null ? plantilla.getCodigo() : null, iImporte);
      }

    } catch (PersistenceException ex) {
    	Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_DETALLE.getValue()
    			+ ": " + ex.getMessage());
    	throw new EjecucionPlantillaException("Error al insertar los apuntes detalles", ex);
    }
  }
  
  

  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectFacturas(Tmct0ContaPlantillas plantilla,
                                   List<Object> paramsDetail,
                                   Integer idApunte,
                                   List<String> listImportDet, int iImporte) throws EjecucionPlantillaException {

    try {

      // Se genera y se guarda el detalle del apunte.
      if (!StringUtils.isEmpty(plantilla.getGrabaDetalle())) {
        tmct0ContaApunteDetalleDao.insertSelectFacturas(plantilla.getQuery_apunte_detalle().replace(";", ""), idApunte,
                                                        plantilla.getEstado_bloqueado(), paramsDetail, listImportDet,
                                                        plantilla != null ? plantilla.getCodigo() : null, iImporte);
      }

    } catch (PersistenceException ex) {
    	Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_DETALLE.getValue()
    			+ ": " + ex.getMessage());
    	throw new EjecucionPlantillaException(ConstantesError.ERROR_APUNTES_DETALLE.getValue(), ex);
    }
  }
  
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public void insertSelectFacturasManuales(Tmct0ContaPlantillas plantilla,
                                   Integer idApunte, BigInteger ndocnumero,
                                   BigDecimal importe) throws EjecucionPlantillaException {

    try {

      // Se genera y se guarda el detalle del apunte.
      if (!StringUtils.isEmpty(plantilla.getGrabaDetalle())) {
        tmct0ContaApunteDetalleDao.insertSelectFacturasManuales(plantilla.getQuery_apunte_detalle().replace(";", ""), idApunte,
                                                        plantilla.getEstado_inicio(), ndocnumero, importe,
                                                        plantilla != null ? plantilla.getCodigo() : null);
      }

    } catch (PersistenceException ex) {
    	Loggers.logErrorAccounting(plantilla.getCodigo(), ConstantesError.ERROR_APUNTES_DETALLE.getValue()
    			+ ": " + ex.getMessage());
    	throw new EjecucionPlantillaException(ConstantesError.ERROR_APUNTES_DETALLE.getValue(), ex);
    }
  }
}
