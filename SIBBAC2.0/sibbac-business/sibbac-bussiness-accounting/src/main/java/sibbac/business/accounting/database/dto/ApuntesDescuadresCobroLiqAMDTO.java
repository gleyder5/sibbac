package sibbac.business.accounting.database.dto;

import java.math.BigDecimal;

import sibbac.business.accounting.database.model.Tmct0ContaPlantillas;

/**
 * Data Transfer Object para la generacion de Apuntes Descuadres.
 */
public class ApuntesDescuadresCobroLiqAMDTO {
	
	private Boolean isDiferenciaMayorQueToleranciaPlantilla;
	private Boolean isSinDiferencia;
	private Boolean isDiferenciaMenorQueToleranciaPlantilla;
	private BigDecimal importeCobro;
	private Tmct0ContaPlantillas plantilla;
	private CobroDTO cobro;
	private Tmct0ContaPlantillasDTO contaPlantillasDTO;

	/**
	 *	ApuntesDescuadresCobroLiqAMDTO. 
	 */
	public ApuntesDescuadresCobroLiqAMDTO() {
	}

	/**
	 *	ApuntesDescuadresCobroLiqAMDTO - Con argumentos. 
	 */
	public ApuntesDescuadresCobroLiqAMDTO(
			Boolean isDiferenciaMayorQueToleranciaPlantilla,
			Boolean isSinDiferencia,
			Boolean isDiferenciaMenorQueToleranciaPlantilla,
			BigDecimal importeCobro, 
			Tmct0ContaPlantillas plantilla, CobroDTO cobro, Tmct0ContaPlantillasDTO contaPlantillasDTO) {
		this.isDiferenciaMayorQueToleranciaPlantilla = isDiferenciaMayorQueToleranciaPlantilla;
		this.isSinDiferencia = isSinDiferencia;
		this.isDiferenciaMenorQueToleranciaPlantilla = isDiferenciaMenorQueToleranciaPlantilla;
		this.importeCobro = importeCobro;
		this.cobro = cobro;
		this.plantilla = plantilla;
		this.contaPlantillasDTO = contaPlantillasDTO;
	}
	
	public Boolean getIsDiferenciaMayorQueToleranciaPlantilla() {
		return this.isDiferenciaMayorQueToleranciaPlantilla;
	}
	
	public void setIsDiferenciaMayorQueToleranciaPlantilla(
			Boolean isDiferenciaMayorQueToleranciaPlantilla) {
		this.isDiferenciaMayorQueToleranciaPlantilla = isDiferenciaMayorQueToleranciaPlantilla;
	}
	
	public Boolean getIsSinDiferencia() {
		return this.isSinDiferencia;
	}
	
	public void setIsSinDiferencia(Boolean isSinDiferencia) {
		this.isSinDiferencia = isSinDiferencia;
	}
	
	public Boolean getIsDiferenciaMenorQueToleranciaPlantilla() {
		return this.isDiferenciaMenorQueToleranciaPlantilla;
	}
	
	public void setIsDiferenciaMenorQueToleranciaPlantilla(
			Boolean isDiferenciaMenorQueToleranciaPlantilla) {
		this.isDiferenciaMenorQueToleranciaPlantilla = isDiferenciaMenorQueToleranciaPlantilla;
	}
	
	public BigDecimal getImporteCobro() {
		return this.importeCobro;
	}
	
	public void setImporteCobro(BigDecimal importeCobro) {
		this.importeCobro = importeCobro;
	}
	
	public Tmct0ContaPlantillas getPlantilla() {
		return this.plantilla;
	}
	
	public void setPlantilla(Tmct0ContaPlantillas plantilla) {
		this.plantilla = plantilla;
	}
	
	public CobroDTO getCobro() {
		return this.cobro;
	}
	
	public void setCobro(CobroDTO cobro) {
		this.cobro = cobro;
	}

	public Tmct0ContaPlantillasDTO getContaPlantillasDTO() {
		return contaPlantillasDTO;
	}

	public void setContaPlantillasDTO(Tmct0ContaPlantillasDTO contaPlantillasDTO) {
		this.contaPlantillasDTO = contaPlantillasDTO;
	}
	
	
	
	
}