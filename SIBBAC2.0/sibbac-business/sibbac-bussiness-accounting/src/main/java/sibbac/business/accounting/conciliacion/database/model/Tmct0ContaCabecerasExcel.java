package sibbac.business.accounting.conciliacion.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_CONTA_CABECERAS_EXCEL.
 */
@Entity
@Table(name = "TMCT0_CONTA_CABECERAS_EXCEL")
public class Tmct0ContaCabecerasExcel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Integer id;
	
	@Column(name = "CABECERA_EXCEL", length = 100, nullable = false)
	private String cabeceraExcel;
	
	@Column(name = "CAMPO_TABLA", length = 100, nullable = false)
	private String campoTabla;
	
	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ContaCabecerasExcel() {
		super();
	}

	/**
	 *	Constructor args.
	 *	@param id
	 *	@param cabeceraExcel
	 *	@param campoTabla
	 */
	public Tmct0ContaCabecerasExcel(Integer id, String cabeceraExcel,
			String campoTabla) {
		super();
		this.id = id;
		this.cabeceraExcel = cabeceraExcel;
		this.campoTabla = campoTabla;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCabeceraExcel() {
		return this.cabeceraExcel;
	}

	public void setCabeceraExcel(String cabeceraExcel) {
		this.cabeceraExcel = cabeceraExcel;
	}

	public String getCampoTabla() {
		return this.campoTabla;
	}

	public void setCampoTabla(String campoTabla) {
		this.campoTabla = campoTabla;
	}
	
}
