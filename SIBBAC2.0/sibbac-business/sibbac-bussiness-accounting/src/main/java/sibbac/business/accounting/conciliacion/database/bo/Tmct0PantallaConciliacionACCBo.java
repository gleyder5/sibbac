package sibbac.business.accounting.conciliacion.database.bo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaApuntesPndtsDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaConciliacionAuditDao;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasCasadasDAO;
import sibbac.business.accounting.conciliacion.database.dao.Tmct0ContaPartidasPndtsDao;
import sibbac.business.accounting.conciliacion.database.dto.GrillaApunteDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaExcelConciliacionBancariaDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ApuntesAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0ContaPartidasCasadasAssembler;
import sibbac.business.accounting.conciliacion.database.dto.assembler.Tmct0PantallaConciliacionOURAssembler;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaApuntesPndts;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConcepto;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaConciliacionAudit;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasCasadas;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.accounting.conciliacion.enums.PantallaConciliacionEnum;
import sibbac.business.accounting.helper.AccountingHelper;
import sibbac.business.utilities.database.bo.Tmct0SibbacConstantesBo;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;
import sibbac.webapp.security.database.model.Tmct0Users;

/**
 * Bo para control de contabilidad.
 */
@Service
public class Tmct0PantallaConciliacionACCBo {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0PantallaConciliacionACCBo.class);

  @Autowired
  private Tmct0ContaPartidasPndtsDao tmct0ContaPartidasPndtsDao;

  @Autowired
  private Tmct0ContaPlantillasConciliaBo tmct0ContaPlantillasConciliaBo;

  @Autowired
  private Tmct0PantallaConciliacionOURAssembler tmct0PantallaConciliacionOURAssembler;

  @Autowired
  private Tmct0ContaConciliacionAuditDao tmct0ContaConciliacionAuditDao;

  @Autowired
  private Tmct0ContaPartidasCasadasDAO tmct0ContaPartidasCasadasDAO;

  @Autowired
  private Tmct0ApuntesAssembler tmct0ApuntesAssembler;

  @Value("${accounting.contabilidad.folder.in}")
  private String folderOut;

  @Autowired
  private Tmct0ContaPartidasCasadasAssembler tmct0ContaPartidasCasadasAssembler;

  @Autowired
  private Tmct0SibbacConstantesBo tmct0SibbacConstantesBo;
  
  @Autowired
  private Tmct0UsersBo tmct0UsersBo;
  
  @Autowired
  Tmct0ContaApuntesPndtsDao tmct0ContaApuntesPndtsDao;
  
  private final String CUENTA_GASTOS = "4021102";
  
  @Value("${accounting.contabilidad.emails}")
  private String listaEmails;
  
  @Autowired
  private SendMail sendMail;

  @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
  public void generarApunteACCExcel(Map<String, Object> paramsObjects) throws Exception {
    List<GrillaTLMDTO> partidas = new ArrayList<GrillaTLMDTO>();
    List<GrillaApunteDTO> apuntesDTO = new ArrayList<GrillaApunteDTO>();
    List<GrillaExcelConciliacionBancariaDTO> filas = null;
    SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyy");
    SimpleDateFormat formatHour = new SimpleDateFormat("HHmmss");
    List<BigInteger> listIdsTLM = new ArrayList<BigInteger>();
    String file = null;

    try {
      ArrayList<LinkedHashMap> lisTLM = (ArrayList<LinkedHashMap>) paramsObjects.get("listaPartidasTLM");
      List<Map<String, Object>> theirsSelected = (List<Map<String, Object>>) paramsObjects.get("listaPartidasTLM");
      List<Map<String, Object>> apuntes = (List<Map<String, Object>>) paramsObjects.get("listaApuntes");
      String usuarioAudit = (String) paramsObjects.get("auditUser");
      String fechaActual = formatDate.format(new Date());
      String horaActual = formatHour.format(new Date());
      String titulo = "apunte_saldo0_" + fechaActual + "_" + horaActual + ".xlsx";
      LinkedHashMap concepto = null;
      Tmct0ContaConcepto contaConcepto = null;

      // Se activa el semaforo de carga de fichero Excel.
      this.tmct0SibbacConstantesBo.updateValorByClave(SibbacEnums.EstadosSemaforoEnum.ACTIVO.getEstado(),
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

      if (null != apuntes) {
        for (Map<String, Object> apunte : apuntes) {
          String descrConcepto = (String) apunte.get("concepto");
          if (!"".equals(descrConcepto)) {
            concepto = new LinkedHashMap();
            concepto.put("key", "");
            concepto.put("value", descrConcepto);
          }
          else {
            concepto = (LinkedHashMap) apunte.get("conceptoSelect");
          }

          // Se comprueba si el concepto existe o no, si no existe se crea
          contaConcepto = tmct0ContaPlantillasConciliaBo.comprobarConcepto(concepto);

          //si no lo supera, lo introduce para generar la excel
          if(!apunteSuperaTolerance(apunte))
            apuntesDTO.add(tmct0ApuntesAssembler.getGrillaApunteDTO(apunte));
        }
      }

      // Se cambia por cada apunte lisTLM su estado=1 en la tabla
      // TMCT0_CONTA_PARTIDAS_PNDTS
      for (LinkedHashMap listaTLM : lisTLM) {
        listIdsTLM.add(BigInteger.valueOf(((Integer) listaTLM.get("id")).intValue()));
      }
      tmct0ContaPartidasPndtsDao.updateEstadoUnoFromId(listIdsTLM);

      // Además se guarda un registro en la tabla de CONCILIA_AUDIT con
      // ORIGEN=SIBBAC.
      Tmct0ContaConciliacionAudit conciliacionAudit = tmct0PantallaConciliacionOURAssembler
          .getTmct0ContaConciliacionAudit(usuarioAudit, contaConcepto);
      conciliacionAudit.setOrigen("ACC");
      tmct0ContaConciliacionAuditDao.save(conciliacionAudit);

      if (null != theirsSelected) {
        for (Map<String, Object> tlm : theirsSelected) {
          partidas.add(tmct0ApuntesAssembler.getGrillaTLMDTO(tlm));
        }
      }

      for (GrillaTLMDTO grillaTLMDTO : partidas) {
        Tmct0ContaPartidasPndts partidaPndt = tmct0ContaPartidasPndtsDao.findById(BigInteger.valueOf(grillaTLMDTO
            .getId().longValue()));
        Tmct0ContaPartidasCasadas partidaCasada = tmct0ContaPartidasCasadasAssembler.getTmct0ContaPartidasCasadas(
            partidaPndt, conciliacionAudit);
        tmct0ContaPartidasCasadasDAO.saveAndFlush(partidaCasada);
      }

      filas = AccountingHelper.generarFilasConciliacionBancaria(partidas, apuntesDTO);
      XSSFWorkbook wb = AccountingHelper.generarXLSApunte(filas);

      file = folderOut + titulo;
      FileOutputStream stream = new FileOutputStream(file);
      wb.write(stream);
      wb.close();
      stream.close();

      // Se desactiva el semaforo de carga de fichero Excel.
      this.tmct0SibbacConstantesBo.updateValorByClave(SibbacEnums.EstadosSemaforoEnum.INACTIVO.getEstado(),
          PantallaConciliacionEnum.ClavesSemaforoEnum.ESTADO_CARGA_EXCEL.getClave());

    }
    catch (IOException e) {
      LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
      throw (e);
    }
    catch (Exception e) {
      LOG.error("Error metodo exportarAExcel -  " + e.getMessage(), e);
      throw (e);
    }
  }
  
  @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = true)
  public Map<String, Object> manageApuntesBelowTolerance(Map<String, Object> paramsObjects) throws Exception {
    Map<String, Object> result = new HashMap<String, Object>();
    try {

      List<Map<String, Object>> apuntes = (List<Map<String, Object>>) paramsObjects.get("listaApuntes");
      List<Map<String, Object>> apuntesPndts = new ArrayList<Map<String, Object>>();

      if (null != apuntes) {
        for (Map<String, Object> apunte : apuntes) {
          if (apunteSuperaTolerance(apunte)) {
            apuntesPndts.add(apunte);
          }
        }
      }

      if (apuntesPndts != null && !apuntesPndts.isEmpty()) {
        // send emails and store in bbdd

        String usuarioAudit = (String) paramsObjects.get("auditUser");

        for (Map<String, Object> apunte : apuntesPndts) {
          LinkedHashMap concepto = null;
          Tmct0ContaConcepto contaConcepto = null;

          String descrConcepto = (String) apunte.get("concepto");
          if (!"".equals(descrConcepto)) {
            concepto = new LinkedHashMap();
            concepto.put("key", "");
            concepto.put("value", descrConcepto);
          }
          else {
            concepto = (LinkedHashMap) apunte.get("conceptoSelect");
          }

          // Se comprueba si el concepto existe o no, si no existe se crea
          contaConcepto = tmct0ContaPlantillasConciliaBo.comprobarConcepto(concepto);

          // ///////////////////////////////

          LinkedHashMap apunteFinal = new LinkedHashMap<String, Object>();
          apunteFinal.putAll(apunte);
          // Si es mayor se guarda el apunte en CONTA_APUNTES_PNDTS.
          Tmct0ContaApuntesPndts contaApuntesPndts = tmct0PantallaConciliacionOURAssembler
              .getTmct0ContaApuntesPndtsTolerance(usuarioAudit, contaConcepto, apunteFinal);
          tmct0ContaApuntesPndtsDao.save(contaApuntesPndts);

          // Además se guarda un registro en la tabla de CONCILIA_AUDIT con
          // ORIGEN=SIBBAC.
          Tmct0ContaConciliacionAudit conciliacionAudit = tmct0PantallaConciliacionOURAssembler
              .getTmct0ContaConciliacionAudit(usuarioAudit, contaConcepto);
          tmct0ContaConciliacionAuditDao.save(conciliacionAudit);

          Tmct0Users user = tmct0UsersBo.getByUsername(usuarioAudit);

          if (user != null && user.getEmail() != null) {

            try {
              // Se le envía un correo al usuario y a destinatarios definidos en sibbac.accounting.properties
              //con clave accounting.contabilidad.emails
              List<String> listEmails = new ArrayList<String>();
              String[] lisEmailsSplit = StringUtils.splitPreserveAllTokens(listaEmails, ";");
              for (String email : lisEmailsSplit) {
                listEmails.add(email);
              }
              listEmails.add(user.getEmail());

              StringBuilder body1 = new StringBuilder();

              body1.append("Hola, se ha generado el siguiente apunte pendiente de aprobar");
              body1.append("\n\n\n");
              body1.append("FECHA APUNTE: ");
              body1.append(contaApuntesPndts.getFeApunte());
              body1.append("\n");
              body1.append("USUARIO: ");
              body1.append(contaApuntesPndts.getUsuApunte());
              body1.append("\n");
              body1.append("CONCEPTO: ");
              body1.append(contaApuntesPndts.getConcepto().getConcepto());
              body1.append("\n");
              body1.append("IMPORTE DEBE: ");
              body1.append(contaApuntesPndts.getImporteDebe() == null ? "0.0 €" : contaApuntesPndts.getImporteDebe()
                  + " €");
              body1.append("\n");
              body1.append("IMPORTE HABER: ");
              body1.append(contaApuntesPndts.getImporteHaber() == null ? "0.0 €" : contaApuntesPndts.getImporteHaber()
                  + " €");
              body1.append("\n");
              body1.append("IMPORTE PYG: ");
              body1.append(contaApuntesPndts.getImportePyG() == null ? "0.0 €" : contaApuntesPndts.getImportePyG()
                  + " €");

              sendMail.sendMail(listEmails, "[SIBBAC 20] - Apunte con concepto '" + contaApuntesPndts.getConcepto().getConcepto()
                  + "' - pendiente de aprobar", body1.toString());
            }
            catch (Exception e) {
              LOG.error("Error al enviar mail", e);
            }
          }
        }
      }
      result.put("status", "OK");
      return result;
    }
    catch (Exception e) {
      LOG.error("Error metodo manageApuntesBelowTolerance -  ", e);
      result.put("status", "KO");
      return result;
    }
  }

  /**
   * This method indicates whether an apunte´s amount is below the defined tolerance 
   * @param apunte
   * @return
   */
  private boolean apunteSuperaTolerance(Map<String, Object> apunte) {
    String cuenta = (String) apunte.get("cuenta");
    Integer tolerance = (Integer) apunte.get("tolerance");
    Double importeD = getImporteFromApunte(apunte);

    if (importeD > 0 && cuenta.equals(CUENTA_GASTOS) && importeD > tolerance) {
      return true;
    }
    return false;
  }

  /**
   * This method retrieves the value of the key 'importe' from a given map
   * @param apunte
   * @return
   */
  private Double getImporteFromApunte(Map<String, Object> apunte) {
    Double importeD = 0d;
    Integer importeI = 0;

    try {
      if ((Double) apunte.get("importe") instanceof Double) {
        importeD = (Double) apunte.get("importe");
      }
    }
    catch (Exception ex) {
    }
    try {
      if ((Integer) apunte.get("importe") instanceof Integer) {

        importeI = (Integer) apunte.get("importe");
        importeD = importeI + 0.0;
      }
    }
    catch (Exception ex) {
    }

    return importeD;
  }

}
