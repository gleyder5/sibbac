package sibbac.business.accounting.conciliacion.database.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_CONTA_CONCEPTO.
 */
@Entity
@Table(name = "TMCT0_CONTA_CONCEPTO")
public class Tmct0ContaConcepto {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", length = 8, nullable = false)
  private BigInteger id;

  /**
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  @Column(name = "CONCEPTO", length = 200, nullable = true)
  private String concepto;

  /**
   * @return the concepto
   */
  public String getConcepto() {
    return concepto;
  }

  /**
   * @param concepto the concepto to set
   */
  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }

  /**
   * Constructor no-arg.
   */
  public Tmct0ContaConcepto() {
    super();
  }

  /**
   * @param id
   * @param concepto
   */
  public Tmct0ContaConcepto(BigInteger id, String concepto) {
    super();
    this.id = id;
    this.concepto = concepto;

  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((concepto == null) ? 0 : concepto.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0ContaConcepto other = (Tmct0ContaConcepto) obj;
    if (concepto == null) {
      if (other.concepto != null)
        return false;
    }
    else if (!concepto.equals(other.concepto))
      return false;
    return true;
  }

}
