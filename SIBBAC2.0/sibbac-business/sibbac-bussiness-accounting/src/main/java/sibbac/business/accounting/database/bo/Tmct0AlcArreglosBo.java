package sibbac.business.accounting.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

import sibbac.business.accounting.database.model.Tmct0ArregloObject;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0aloDao;
import sibbac.business.wrappers.database.dao.Tmct0bokDao;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;

@Service
public class Tmct0AlcArreglosBo {
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  Tmct0alcDao tmct0alcDao;

  @Autowired
  Tmct0aloDao tmct0aloDao;

  @Autowired
  Tmct0bokDao tmct0bokDao;

  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0AlcArreglosBo.class);

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public boolean procesarOperacionesNoCobrables() throws SIBBACBusinessException {

    try {
      Set<String> listaEstadosCanon = getListaEstadosCanon();
      Set<String> listaEstadosCorretaje = getListaEstadosCorretaje();
      Set<String> listaMiembrosCompensadorDestino = getListaMiembrosCompensadorDestino();
      Set<String> listaRefAsignacion = getListaRefAsignacion();
      Date dateIni = getDateIni();
      Date dateFin = new Date();

      List<Object[]> operacionesNoCobrablesCanon = new ArrayList<>();
      List<Object[]> operacionesNoCobrablesCorretaje = new ArrayList<>();

//      operacionesNoCobrablesCorretaje = tmct0alcDao.findAllTmct0alcOpsNoCobrablesCorretaje(listaEstadosCorretaje,
//          listaMiembrosCompensadorDestino, listaRefAsignacion, dateIni, dateFin);

      operacionesNoCobrablesCanon = tmct0alcDao.findAllTmct0alcOpsNoCobrablesCanon(listaEstadosCanon,
          listaMiembrosCompensadorDestino, listaRefAsignacion, dateIni, dateFin);

//      if (operacionesNoCobrablesCorretaje != null && !operacionesNoCobrablesCorretaje.isEmpty()) {
//        actualizarOperacion(convertList(operacionesNoCobrablesCorretaje), false, true);
//      }
      if (operacionesNoCobrablesCanon != null && !operacionesNoCobrablesCanon.isEmpty()) {
        actualizarOperacion(convertList(operacionesNoCobrablesCanon), true, false);
      }

      return true;
    }
    catch (Exception ex) {
      LOG.info("", ex);
    }
    return false;
  }

  private void actualizarOperacion(List<Tmct0ArregloObject> objetos, boolean actualizarEstadoCanon,
      boolean actualizarEstadoCorretaje) {

    if (objetos != null && !objetos.isEmpty()) {

      Tmct0estado estadoEnCurso = tmct0estadoDao.findOne(CONTABILIDAD.EN_CURSO.getId());
      Tmct0estado estadoCobrado = tmct0estadoDao.findOne(CONTABILIDAD.COBRADA.getId());

      for (Tmct0ArregloObject arreglo : objetos) {

        Tmct0estado estadoAsig = null;

        String nuorden = arreglo.getNuorden();
        String nbooking = arreglo.getNbooking();
        String nucnfclt = arreglo.getNucnfclt();
        BigDecimal nucnfliq = arreglo.getNucnfliq();

        Tmct0alc alc = tmct0alcDao.findByIdNuordenAndIdNbookingAndIdNucnfcltAndIdNucnfliq(nuorden, nbooking, nucnfclt,
            nucnfliq.shortValue());

        // Marcamos como cobrada la ALC
        if (actualizarEstadoCanon) {
          alc.setEstadocontCanon(estadoCobrado);
        }
        if (actualizarEstadoCorretaje) {
          alc.setEstadocont(estadoCobrado);
        }
        tmct0alcDao.save(alc);

        List<Tmct0bok> bookings = tmct0bokDao.findByNbooking(nbooking);
        if (bookings != null && !bookings.isEmpty()) {
          for (Tmct0bok book : bookings) {
            estadoAsig = book.getTmct0estadoByCdestadoasig();
            // marcamos como EN CURSO el booking
            book.setTmct0estadoByCdestadocont(estadoEnCurso);
            tmct0bokDao.save(book);
          }
        }
        Tmct0alo alo = tmct0aloDao.getAlo(nuorden, nbooking, nucnfclt);

        // Le asignamos al alo el estado de asignacion del booking
        alo.setTmct0estadoByCdestadoasig(estadoAsig);

        // le asignamos al alo el estado contable EN CURSO
        alo.setTmct0estadoByCdestadocont(estadoEnCurso);
        tmct0aloDao.save(alo);
      }
    }

  }

  private List<Tmct0ArregloObject> convertList(List<Object[]> operacionesNoCobrables) {
    List<Tmct0ArregloObject> osNoCobrables = new ArrayList();

    if ((operacionesNoCobrables != null) && (!operacionesNoCobrables.isEmpty())) {

      for (Object[] operacion : operacionesNoCobrables) {
        Tmct0ArregloObject arregloObject = new Tmct0ArregloObject();
        arregloObject.setNuorden((String) operacion[0]);
        arregloObject.setNbooking((String) operacion[1]);
        arregloObject.setNucnfclt((String) operacion[2]);
        arregloObject.setNucnfliq((BigDecimal) operacion[3]);
        osNoCobrables.add(arregloObject);
      }
    }
    return osNoCobrables;
  }

  /**
   * This method gets the 'estados canon' from data base that will be used to
   * filter
   * @return
   */
  private Set<String> getListaEstadosCanon() {
    Tmct0cfg estados = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_ESTADOS_CANON.getProcess(),
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_ESTADOS_CANON.getKeyname());

    String[] estadosCanonArray = null;
    Set<String> estadosCanonSet = null;
    if (estados != null) {
      estadosCanonArray = estados.getKeyvalue().trim().split(",");
      estadosCanonSet = Sets.newHashSet(estadosCanonArray);
    }
    return estadosCanonSet;
  }

  /**
   * This method gets the 'estados corretaje' from data base that will be used
   * to filter
   * @return
   */
  private Set<String> getListaEstadosCorretaje() {
    Tmct0cfg estados = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_EST_CORRETAJE.getProcess(),
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_EST_CORRETAJE.getKeyname());

    String[] estadosCanonArray = null;
    Set<String> estadosCanonSet = null;
    if (estados != null) {
      estadosCanonArray = estados.getKeyvalue().trim().split(",");
      estadosCanonSet = Sets.newHashSet(estadosCanonArray);
    }
    return estadosCanonSet;
  }

  /**
   * This method gets the 'referencias de asignacion' from data base that will
   * be used to filter
   * @return
   */
  private Set<String> getListaRefAsignacion() {
    Tmct0cfg referencias = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_REF_ASIGNACION.getProcess(),
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_REF_ASIGNACION.getKeyname());

    String[] referenciasArray = null;
    Set<String> referenciasSet = null;
    if (referencias != null) {
      referenciasArray = referencias.getKeyvalue().trim().split(",");
      referenciasSet = Sets.newHashSet(referenciasArray);
    }
    return referenciasSet;
  }

  /**
   * This method gets the 'miembros compensador destino' from data base that
   * will be used to filter
   * @return
   */
  private Set<String> getListaMiembrosCompensadorDestino() {
    Tmct0cfg miembros = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_MIEMBROS_COMP_DESTINO.getProcess(),
        Tmct0cfgConfig.OPERACIONES_NO_COBRABLES_MIEMBROS_COMP_DESTINO.getKeyname());

    String[] miembrosArray = null;
    Set<String> miembrosSet = null;
    if (miembros != null) {
      miembrosArray = miembros.getKeyvalue().trim().split(",");
      miembrosSet = Sets.newHashSet(miembrosArray);
    }
    return miembrosSet;
  }

  /**
   * This method gets from data base the date from which the query will filter
   * @return
   * @throws SIBBACBusinessException
   */
  private Date getDateIni() throws SIBBACBusinessException {
    Tmct0cfg aliasSpb = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.DATE_INI_OPERACIONES_NO_COBRABLES.getProcess(),
        Tmct0cfgConfig.DATE_INI_OPERACIONES_NO_COBRABLES.getKeyname());
    Calendar cal = Calendar.getInstance();

    if (aliasSpb != null) {
      Integer daysOff = Integer.valueOf(aliasSpb.getKeyvalue());
      cal.add(Calendar.DATE, -daysOff);
      return cal.getTime();
    }
    else {
      throw new SIBBACBusinessException(
          "No se ha podido recuperar la fecha de la tabla CFG de bbdd clave DATE_INI_OPERACIONES_NO_COBRABLES");
    }
  }
}
