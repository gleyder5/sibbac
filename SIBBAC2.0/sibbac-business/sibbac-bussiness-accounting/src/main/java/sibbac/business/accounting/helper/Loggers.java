package sibbac.business.accounting.helper;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.utils.DateHelper;
import sibbac.database.bo.AbstractBo;

/**
 * Helper de utilidad para representacion de loggers generico.
 */
public class Loggers {

  protected static final Logger LOG = LoggerFactory.getLogger(AbstractBo.class);

  // Inicio/Fin: LOG.trace("[NombreClase - INICIO/FIN nombreMetodo]" + "[Metodo]" + "[" + new Date() + "]");
  @Deprecated
  public static void logInicioFin(String nombreClase, Class<?> clase, boolean isInicio, String nombreMetodo) {
    String inicioFin;
    if (isInicio) {
      inicioFin = "INICIO";
    } else {
      inicioFin = "FIN";
    }
    LOG.trace("[" + nombreClase + " - " + clase + " - " + inicioFin + " " + nombreMetodo + "]" + "[Metodo]" + "["
              + new Date() + "]");
  }

  /**
   * Representacion de log generico de error.
   *
   * @param nombreMetodo: nombre del metodo en que ocurre excepcion
   * @param nombreClase: nombre de clase en que ocurre excepcion
   * @param errorMessage: mensaje de error identificativo como comentario
   * @param messageException: e.getMessage() de la excepcion atrapada
   */
  // Error: LOG.error("ERROR - nombreMetodo - NombreClase " + "[Metodo]" + "[" + new Date() + "]");
  @Deprecated
  public static void logError(String nombreMetodo, Class<?> nombreClase, String errorMessage, String messageException) {
    LOG.error("[ERROR " + errorMessage + " - " + messageException + "]" + "[Metodo " + nombreMetodo + "] [Clase "
              + nombreClase + "]" + "[" + new Date() + "]");
  }

  // Inicio de query: LOG.trace("[NombreClase - INICIO QueryXXX]" + "[Query]" + "[" + new Date() + "]");
  @Deprecated
  public static void logInicioQuery(String nombreQuery, Class<?> clase, String query) {
    LOG.trace("[" + clase + " - INICIO " + nombreQuery + "]" + "[" + query + "]" + "[" + new Date() + "]");
  }

  // Cantidad de registros ejecución query: LOG.trace("[NombreClase - Cantidad de Registros QueryXXX]" + "[" +
  // datosQueryXXX.size() + "]" );
  @Deprecated
  public static void logCantidadRegistros(String nombreClase, String nombreQuery, int cantidadRegistros) {
    LOG.trace("[" + nombreClase + " - Cantidad de Registros " + nombreQuery + "]" + "[" + cantidadRegistros + "]");
  }

  // Fin de query: LOG.trace("[NombreClase - FIN QueryXXX]" + "[Query]" + "[" + new Date() + "]");
  @Deprecated
  public static void logFinQuery(String nombreQuery, Class<?> clase, String query) {
    LOG.trace("[" + clase + " - FIN " + nombreQuery + "]" + "[" + query + "]" + "[" + new Date() + "]");
  }

  /**
   * Representacion de log de Debug para Accounting en el caso de queries.
   * 
   * @param message: mensaje de ejecucion preferentemente
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param fecha: fecha en que se esta procesando la query
   * 
   *          Ejemplo: ACCOUNTING. Plantilla XXXXXXX. Ejecución Query_bloqueo, Día de ejecución: dia1/mm/yyyy
   */
  public static void logDebugQueriesAccounting(String codigoPlantilla, String mensajeQuery, Date fechaEjecucion) {
    String fechaEjecucionString = DateHelper.convertDateToString(fechaEjecucion, "dd/MM/yyyy");

    StringBuffer sbLogQuery = getDatosComunesLogsInicial(codigoPlantilla);

    sbLogQuery.append(". Ejecución ");
    if (StringUtils.isNotBlank(mensajeQuery)) {
      sbLogQuery.append(mensajeQuery);
    }

    if (StringUtils.isNotBlank(fechaEjecucionString)) {
      sbLogQuery.append(", Día de ejecución: ");
      sbLogQuery.append(fechaEjecucionString);
    }

    LOG.debug(sbLogQuery.toString());
  }

  /**
   * Representacion de log de inicio / fin para Accounting.
   *
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param inicio: si es true se toma como inicio, caso contrario es false Ejemplo: ACCOUNTING. Plantilla XXXXXXX.
   *          Inicio
   */
  public static void logDebugInicioFinAccounting(String codigoPlantilla, boolean inicio) {
    StringBuffer sbLogQuery = getDatosComunesLogsInicial(codigoPlantilla);
    if (inicio) {
      sbLogQuery.append(". Inicio");
    } else {
      sbLogQuery.append(". Fin");
    }
    LOG.debug(sbLogQuery.toString());
  }

  /**
   * Representacion de log de Error para Accounting.
   * 
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param mensajeError: mensaje de error preferentemente o stack trace
   * 
   *          Ejemplo: ACCOUNTING. Plantilla XXXXXXX. Error: xxxx
   */
  public static void logErrorAccounting(String codigoPlantilla, String mensajeError) {

    StringBuffer sbLogQuery = getDatosComunesLogsInicial(codigoPlantilla);

    sbLogQuery.append(". Error ");
    if (StringUtils.isNotBlank(mensajeError)) {
      sbLogQuery.append(mensajeError);
    }
    LOG.error(sbLogQuery.toString());
  }

  /**
   * Representacion de log de Trace para Accounting - queries.
   * 
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param mensaje: mensaje de trace - puede ser query o parametro
   * @param isQuery: si es true es query, sino parametro de la query - opcional con null
   * 
   *          Ejemplo: ACCOUNTING. Plantilla XXXXXXX. Query: Mensaje
   */
  public static void logTraceQueriesAccounting(String codigoPlantilla, String mensaje, Boolean isQuery) {

    StringBuffer sbLogQuery = getDatosComunesLogsInicial(codigoPlantilla);

    if (isQuery != null) {
      if (isQuery) {
        sbLogQuery.append(". Query: ");
      } else {
        sbLogQuery.append(". Parámetro: ");
      }
    }

    if (StringUtils.isNotBlank(mensaje)) {
      sbLogQuery.append(mensaje);
    }
    LOG.trace(sbLogQuery.toString());
  }
  
  /**
   * Representacion de log de Trace para Accounting - Generico.
   * 
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param mensaje: mensaje de trace
   * 
   *          Ejemplo: ACCOUNTING. Plantilla XXXXXXX. Mensaje
   */
  public static void logDebugGenericoAccounting(String codigoPlantilla, String mensaje) {

    StringBuffer sbLogGenerico = getDatosComunesLogsInicial(codigoPlantilla);

    if (StringUtils.isNotBlank(mensaje)) {
    	sbLogGenerico.append(". " + mensaje);
    }
    LOG.debug(sbLogGenerico.toString());
  }
  
  /**
   * Representacion de log de Trace para Accounting - Generico.
   * 
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param mensaje: mensaje de trace
   * 
   *          Ejemplo: ACCOUNTING. Plantilla XXXXXXX. Mensaje
   */
  public static void logTraceGenericoAccounting(String codigoPlantilla, String mensaje) {

    StringBuffer sbLogGenerico = getDatosComunesLogsInicial(codigoPlantilla);

    if (StringUtils.isNotBlank(mensaje)) {
    	sbLogGenerico.append(". " + mensaje);
    }
    LOG.debug(sbLogGenerico.toString());
    LOG.trace(sbLogGenerico.toString());
  }

  /**
   * Datos iniciales de logs para niveles DEBUG, TRACE, ERROR, etc.
   *
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @return StringBuffer
   */
  public static StringBuffer getDatosComunesLogsInicial(String codigoPlantilla) {
    StringBuffer sbLogQuery = new StringBuffer();
    sbLogQuery.append("ACCOUNTING.");
    if (StringUtils.isNotBlank(codigoPlantilla)) {
      sbLogQuery.append(" Plantilla ");
      sbLogQuery.append(codigoPlantilla);
    }
    return sbLogQuery;
  }
  
  /**
   * Representacion de log de Error para Accounting.
   * 
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @param mensajeError: mensaje de error preferentemente o stack trace
   * 
   *          Ejemplo: ACCOUNTING. Plantilla XXXXXXX. Error: xxxx
   */
  public static void logErrorValidationAccounting(String codigoPlantilla, String mensajeError) {

    StringBuffer sbLogQuery = getDatosComunesLogsInicialValidationAccounting(codigoPlantilla);

    sbLogQuery.append(". ");
    if (StringUtils.isNotBlank(mensajeError)) {
      sbLogQuery.append(mensajeError);
    }
    LOG.error(sbLogQuery.toString());
  }
  
  /**
   * Datos iniciales de logs para niveles DEBUG, TRACE, ERROR, etc.
   *
   * @param codigoPlantilla: codigo plantilla actual que se procesa
   * @return StringBuffer
   */
  public static StringBuffer getDatosComunesLogsInicialValidationAccounting(String codigoPlantilla) {
    StringBuffer sbLogQuery = new StringBuffer();
    sbLogQuery.append("VALIDATION ACCOUNTING.");
    if (StringUtils.isNotBlank(codigoPlantilla)) {
      sbLogQuery.append(" Plantilla ");
      sbLogQuery.append(codigoPlantilla);
    }
    return sbLogQuery;
  }
}
