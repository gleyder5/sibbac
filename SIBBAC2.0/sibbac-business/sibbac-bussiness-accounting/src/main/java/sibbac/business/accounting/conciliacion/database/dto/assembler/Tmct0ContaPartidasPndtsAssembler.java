package sibbac.business.accounting.conciliacion.database.dto.assembler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.accounting.conciliacion.database.dto.FiltroGrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.dto.GrillaTLMDTO;
import sibbac.business.accounting.conciliacion.database.model.Tmct0ContaPartidasPndts;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.business.wrappers.helper.Helper;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_CONTA_PARTIDAS_PNDTS
 */
@Service
public class Tmct0ContaPartidasPndtsAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContaPartidasPndtsAssembler.class);

	@Autowired
	private AliasDao aliasDao;

	@Autowired
	Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

	public Tmct0ContaPartidasPndtsAssembler() {
		super();
	}

	/**
	 * Realiza la conversion entre los parámetros de la visual y un objeto filtroDTO
	 * 
	 * @param paramsObject -> parámetros que manda la visual
	 * @return Entidad con los datos de la visual tratados
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public FiltroGrillaTLMDTO getTmct0FiltroGrillaTLMDTO(
			Map<String, Object> paramsObject) throws Exception {

		LOG.info("INICIO - ASSEMBLER - getTmct0FiltroGrillaTLMDTO");

		FiltroGrillaTLMDTO filtroGrillaTLMDTO = new FiltroGrillaTLMDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SelectDTO selectDTO; 

		try {
			// Auxiliar bancario.
			List<SelectDTO> listaAuxiliaresBancarios  = new ArrayList<SelectDTO>();
			List<Map<String, String>> auxiliaresBancarios = (List<Map<String, String>>) paramsObject.get("auxiliares");
			if (auxiliaresBancarios != null && !auxiliaresBancarios.isEmpty()) {
				for(Map<String,String> auxiliarBancario : auxiliaresBancarios){
					selectDTO = new SelectDTO(
							String.valueOf(auxiliarBancario.get("key")), String.valueOf(auxiliarBancario.get("value")));
					listaAuxiliaresBancarios.add(selectDTO);
				}
				filtroGrillaTLMDTO.setAuxiliaresBancarios(listaAuxiliaresBancarios);				
			}

			// Tipos de movimiento.
			List<SelectDTO> listaTiposMovimientos  = new ArrayList<SelectDTO>();
			List<Map<String, String>> tiposMovimientos = (List<Map<String, String>>) paramsObject.get("tiposMovimientos");
			if (tiposMovimientos != null && !tiposMovimientos.isEmpty()) {
				for(Map<String,String> tipoMovimiento : tiposMovimientos){
					selectDTO = new SelectDTO(
							String.valueOf(tipoMovimiento.get("key")), String.valueOf(tipoMovimiento.get("value")));
					listaTiposMovimientos.add(selectDTO);
				}
				filtroGrillaTLMDTO.setTiposMovimientos(listaTiposMovimientos);				
			}

			// Numeros de documento.
			List<SelectDTO> listaNrosDocumentos  = new ArrayList<SelectDTO>();
			List<Map<String, String>> nrosDocumentos = (List<Map<String, String>>) paramsObject.get("numerosDocumentos");
			if (nrosDocumentos != null && !nrosDocumentos.isEmpty()) {
				for(Map<String,String> nroDocumento : nrosDocumentos){
					selectDTO = new SelectDTO(
							String.valueOf(nroDocumento.get("key")), 
							String.valueOf(nroDocumento.get("value")),
							String.valueOf(nroDocumento.get("oper")));
					listaNrosDocumentos.add(selectDTO);
				}
				filtroGrillaTLMDTO.setNumerosDocumentos(listaNrosDocumentos);				
			}

			// Conceptos movimiento.
			List<SelectDTO> listaConceptosMovimiento  = new ArrayList<SelectDTO>();
			List<Map<String, String>> conceptosMovimientos = (List<Map<String, String>>) paramsObject.get("conceptosMovimientos");
			if (conceptosMovimientos != null && !conceptosMovimientos.isEmpty()) {
				for(Map<String,String> conceptoMovimiento : conceptosMovimientos){
					selectDTO = new SelectDTO(
							String.valueOf(conceptoMovimiento.get("key")), 
							String.valueOf(conceptoMovimiento.get("value")),
							String.valueOf(conceptoMovimiento.get("oper")));
					listaConceptosMovimiento.add(selectDTO);
				}
				filtroGrillaTLMDTO.setConceptosMovimientos(listaConceptosMovimiento);				
			}

			// Comentario.
			List<SelectDTO> listaComentarios  = new ArrayList<SelectDTO>();
			List<Map<String, String>> comentarios = (List<Map<String, String>>) paramsObject.get("comentarios");
			if (comentarios != null && !comentarios.isEmpty()) {
				for(Map<String,String> comentario : comentarios){
					selectDTO = new SelectDTO(
							String.valueOf(comentario.get("key")), 
							String.valueOf(comentario.get("value")),
							String.valueOf(comentario.get("oper")));
					listaComentarios.add(selectDTO);
				}
				filtroGrillaTLMDTO.setComentarios(listaComentarios);				
			}

			if (paramsObject.get("operadorNumeroDocumento") != null) {
				filtroGrillaTLMDTO.setOperadorNumeroDocumento((String) paramsObject.get("operadorNumeroDocumento"));
			}
			if (paramsObject.get("operadorConceptoMovimiento") != null) {
				filtroGrillaTLMDTO.setOperadorConceptoMovimiento((String) paramsObject.get("operadorConceptoMovimiento"));
			}
			if (paramsObject.get("operadorComentario") != null) {
				filtroGrillaTLMDTO.setOperadorComentario((String) paramsObject.get("operadorComentario"));
			}

			if (null != paramsObject.get("bookingDateDesde") && !"".equals(paramsObject.get("bookingDateDesde"))) {
				if (null != paramsObject.get("bookingDateHasta") && !"".equals(paramsObject.get("bookingDateHasta"))) {
					filtroGrillaTLMDTO.setListaBookingDate(
							Helper.separarFechas(formatter.parse(String.valueOf(paramsObject.get("bookingDateDesde"))), 
									formatter.parse(String.valueOf(paramsObject.get("bookingDateHasta")))));
				} else {
					filtroGrillaTLMDTO.setListaBookingDate(
							Helper.separarFechas(formatter.parse(String.valueOf(paramsObject.get("bookingDateDesde"))), null));
				}

			} else if (null != paramsObject.get("valueDateDesde") && !"".equals(paramsObject.get("valueDateDesde"))) {
				if (null != paramsObject.get("valueDateHasta") && !"".equals(paramsObject.get("valueDateHasta"))) {
					filtroGrillaTLMDTO.setListaValueDate(
							Helper.separarFechas(formatter.parse(String.valueOf(paramsObject.get("valueDateDesde"))), 
									formatter.parse(String.valueOf(paramsObject.get("valueDateHasta")))));
				} else {
					filtroGrillaTLMDTO.setListaValueDate(
							Helper.separarFechas(formatter.parse(String.valueOf(paramsObject.get("valueDateDesde"))), null));
				}
			}

			if (paramsObject.get("importeDesde") != null) {
				if (paramsObject.get("importeDesde") instanceof Double) {
					filtroGrillaTLMDTO.setImporteDesde(BigDecimal.valueOf((Double) paramsObject.get("importeDesde")));
				} else if (paramsObject.get("importeDesde") instanceof Integer){
					filtroGrillaTLMDTO.setImporteDesde(BigDecimal.valueOf((Integer) paramsObject.get("importeDesde")));
				} else {
					filtroGrillaTLMDTO.setImporteDesde(null);
				}
			}

			if (paramsObject.get("importeHasta") != null) {
				if (paramsObject.get("importeHasta") instanceof Double) {
					filtroGrillaTLMDTO.setImporteHasta(BigDecimal.valueOf((Double) paramsObject.get("importeHasta")));
				} else if (paramsObject.get("importeHasta") instanceof Integer) {
					filtroGrillaTLMDTO.setImporteHasta(BigDecimal.valueOf((Integer) paramsObject.get("importeHasta")));
				} else {
					filtroGrillaTLMDTO.setImporteHasta(null);
				}
			}

			if (null!=paramsObject.get("antiguedades") && ((List<String>)paramsObject.get("antiguedades")).size()>0) {
				filtroGrillaTLMDTO.setListAantiguedad((List<String>)paramsObject.get("antiguedades"));
			}
			if (paramsObject.get("gin") != null
					&& !"".equals((String) paramsObject.get("gin").toString())) {
				filtroGrillaTLMDTO.setGin(BigInteger.valueOf(((Integer)paramsObject.get("gin")).intValue()));
			}
		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FiltroGrillaTLMDTO -  "	+ e.getMessage(), e);
			throw(e);
		}
		LOG.info("FIN - ASSEMBLER - getTmct0FiltroGrillaTLMDTO");
		return filtroGrillaTLMDTO;

	}

	/**
	 *	Convierte objeto Conta Pendientes al DTO de la grilla.
	 *	@param contaPartidasPndts
	 *	@return GrillaTLMDTO 
	 */
	public GrillaTLMDTO convertContaPndtsToGrillaTLMDTO(
			Tmct0ContaPartidasPndts contaPartidasPndts) {
		LOG.info("INICIO - ASSEMBLER - convertContaPndtsToGrillaTLMDTO");
		GrillaTLMDTO dto = new GrillaTLMDTO();
		try {
			dto.setId(contaPartidasPndts.getId() != null ? contaPartidasPndts.getId().longValue() : null);
			dto.setAntiguedad(contaPartidasPndts.getAntiguedad());
			dto.setTipoDeMovimiento(contaPartidasPndts.getTipo());
			dto.setAuxiliarBancario(contaPartidasPndts.getAuxBancario());
			dto.setConceptoMovimiento(contaPartidasPndts.getReferencia());
			dto.setNumeroDocumento(contaPartidasPndts.getNumDocumento());
			dto.setComentario(contaPartidasPndts.getComentario());
			dto.setBookingDate(contaPartidasPndts.getBookingDate());
			dto.setValueDate(contaPartidasPndts.getValueDate());
			dto.setImporte(contaPartidasPndts.getImporte().setScale(2, RoundingMode.CEILING));
			dto.setGin(contaPartidasPndts.getGin().intValue());
			dto.setComentarioUsuario(contaPartidasPndts.getComentarioUsuario());
			dto.setCurrency(contaPartidasPndts.getCurrency());
			dto.setDepartamento(contaPartidasPndts.getDepartamento());
			dto.setNumeroDocumento2(contaPartidasPndts.getNumDocumento2());
			dto.setReferencia(contaPartidasPndts.getReferencia());
			dto.setReferencia2(contaPartidasPndts.getReferencia2());
		} catch (Exception e) {
			LOG.error("Error metodo convertContaPndtsToGrillaTLMDTO -  "	+ e.getMessage(), e);
			throw(e);
		}
		LOG.info("FINAL - ASSEMBLER - convertContaPndtsToGrillaTLMDTO");
		return dto;
	}
}
