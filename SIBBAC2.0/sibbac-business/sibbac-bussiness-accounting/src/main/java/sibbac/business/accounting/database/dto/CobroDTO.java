package sibbac.business.accounting.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * Data Transfer Object para la gestión de Cobros.
 * 
 * @author Neoris
 *
 */
public class CobroDTO {

	private String descReferencia;
	private BigDecimal importe;
	private String sentido;
	private Long idFic;
	private Long idMov;
	private String referencia;
	private Date setDate;
	private Date tradeDate;

	public CobroDTO(String descReferencia, BigDecimal importe, String sentido, Long idFic, Long idMov,
			String referencia, Date setDate, Date tradeDate) {
		super();
		this.descReferencia = descReferencia;
		this.importe = importe;
		this.sentido = sentido;
		this.idFic = idFic;
		this.idMov = idMov;
		this.referencia = referencia;
		this.setDate = setDate;
		this.tradeDate = tradeDate;
	}

	public CobroDTO(Object[] elcobroArg) {
	    Object[] elcobro = Arrays.copyOf(elcobroArg, elcobroArg.length);
		this.descReferencia = (String) elcobro[0];
		this.importe = (BigDecimal) elcobro[1];
		try {
			this.sentido = (String) elcobro[2];
		} catch (ClassCastException ex) {
			// Si hay error de casting se prueba con Integer.
			this.sentido = String.valueOf(((Integer) elcobro[2]));
		}	
		try {
			this.idFic = new Long((Integer) elcobro[3]);
		} catch (ClassCastException ex) {
			// Si hay error de casting se prueba con BigInteger.
			BigInteger cobro3 = ((BigInteger) elcobro[3]);
			this.idFic = cobro3 != null ? cobro3.longValue() : NumberUtils.createLong("0");
		}
		try {
			this.idMov = new Long((Integer) elcobro[4]); 
		} catch (ClassCastException ex) {
			// Si hay error de casting se prueba con BigInteger.
			BigInteger cobro4 = ((BigInteger) elcobro[4]);
			this.idMov = cobro4 != null ? cobro4.longValue() : NumberUtils.createLong("0");
		}
		this.referencia = (String) elcobro[5];
		this.setDate = (Date) elcobro[6];
		this.tradeDate = (Date) elcobro[7];
	}

	public String getDescReferencia() {
		return descReferencia;
	}

	public void setDescReferencia(String descReferencia) {
		this.descReferencia = descReferencia;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	public Long getIdFic() {
		return idFic;
	}

	public void setIdFic(Long idFic) {
		this.idFic = idFic;
	}

	public Long getIdMov() {
		return idMov;
	}

	public void setIdMov(Long idMov) {
		this.idMov = idMov;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Date getSetDate() {
		return setDate;
	}

	public void setSetDate(Date setDate) {
		this.setDate = setDate;
	}

	public Date getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}

	@Override
	public String toString() {
		return "CobroDTO [descReferencia=" + descReferencia + ", importe=" + importe + ", sentido=" + sentido
				+ ", idFic=" + idFic + ", idMov=" + idMov + ", referencia=" + referencia + ", setDate=" + setDate
				+ ", tradeDate=" + tradeDate + "]";
	}

}
