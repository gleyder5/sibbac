package sibbac.business.accounting.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.accounting.database.model.Tmct0ContaApuntes;

/**
 * Data access object de conta apuntes.
 * 
 * @author aldo.saia
 *
 */
@Repository
public interface Tmct0ContaApuntesDao extends JpaRepository<Tmct0ContaApuntes, Integer> {

  @Query("SELECT d FROM Tmct0ContaApuntes d WHERE d.generado_fichero = 0 ORDER BY d.fichero, d.periodo_comprobante, d.id")
  public List<Tmct0ContaApuntes> getTmct0ContaApuntesGeneradoFichero();

  @Modifying
  @Transactional
  @Query(value = "UPDATE Tmct0ContaApuntes d SET d.generado_fichero=1, d.num_comprobante = :numComprobante WHERE d.fichero=:ficheroContaApunte and d.generado_fichero = 0 and d.periodo_comprobante = :periodo and d.fecha_comprobante = :fechaComprobante ")
  public Integer updateGeneradoFicheroNumComprobanteByFicheroAndPeriodoComprobanteAndFechaComprobanteAndNotGeneradoFichero(
      @Param("numComprobante") Integer numComprobante, @Param("ficheroContaApunte") String ficheroContaApunte,
      @Param("periodo") String periodo, @Param("fechaComprobante") Date fechaComprobante);

}
