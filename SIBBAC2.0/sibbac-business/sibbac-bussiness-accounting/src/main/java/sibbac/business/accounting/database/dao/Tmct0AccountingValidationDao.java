package sibbac.business.accounting.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.accounting.database.model.Tmct0AccountingValidation;

/**
 * Data access object de Tmct0AccountingValidation.
 */
@Repository
public interface Tmct0AccountingValidationDao extends JpaRepository<Tmct0AccountingValidation, Long> {

  @Query("SELECT tValid FROM Tmct0AccountingValidation tValid WHERE tValid.tipo = :tipo")
  public List<Tmct0AccountingValidation> findByTipo(@Param("tipo") String tipo);

  @Query("SELECT tValid FROM Tmct0AccountingValidation tValid WHERE tValid.tipo = :tipo AND tValid.clave = :clave")
  public Tmct0AccountingValidation findByTipoAndClave(@Param("tipo") String tipo, @Param("clave") String clave);

}
