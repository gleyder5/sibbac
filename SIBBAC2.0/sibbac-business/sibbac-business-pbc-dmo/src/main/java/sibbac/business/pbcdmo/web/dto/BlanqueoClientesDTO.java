package sibbac.business.pbcdmo.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import sibbac.business.pbcdmo.db.model.BlanqueoClientesEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractDTO;

/**
 * The Class TMCT0BlanqueoControlDocumentacionDTO.
 */
public class BlanqueoClientesDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1230413757285198066L;

  /** Clave primaria. */
  @NotNull
  private String id;

  /** ForeignKey de control efectivo. */
  @NotNull
  private Integer idControlEfectivo;

  /** ForeignKey de control documentacion. */
  @NotNull
  private String idControlDocumentacion;

  /** ForeignKey de control analisis. */
  private String idControlAnalisis;

  /** id de la tabla observacion. */
  private String idObservacion;

  /** Fecha de la revision. */
  private Date fechaRevision;

  /** The activo. */
  protected Integer activo;

  /** The fechaalta. */
  protected Date fechaAlta;

  /** The fechamod. */
  protected Date fechaMod;

  /** The fechabaja. */
  protected Date fechaBaja;

  /** The usuarioalta. */
  protected String usuarioAlta;

  /** The usuariomod. */
  protected String usuarioMod;

  /** The usuariobaja. */
  protected String usuarioBaja;

  /**
   * Instantiates a new blanqueo clientes DTO.
   */
  public BlanqueoClientesDTO() {
    super();
  }

  /**
   * Instantiates a new blanqueo clientes DTO.
   *
   * @param id the id
   * @param IdControlEfectivo the id control efectivo
   * @param IdControlDocumentacion the id control documentacion
   * @param IdControlAnalisis the id control analisis
   * @param idObservacion the id observacion
   * @param fechaRevision the fecha revision
   * @param activo the activo
   * @param fechaAlta the fecha alta
   * @param fechaMod the fecha mod
   * @param fechaBaja the fecha baja
   * @param usuarioAlta the usuario alta
   * @param usuarioMod the usuario mod
   * @param usuarioBaja the usuario baja
   */
  public BlanqueoClientesDTO(String id, Integer idControlEfectivo, String idControlDocumentacion,
      String idControlAnalisis, String idObservacion, Date fechaRevision, Integer activo, Date fechaAlta, Date fechaMod,
      Date fechaBaja, String usuarioAlta, String usuarioMod, String usuarioBaja) {
    super();
    this.id = id;
    this.idControlEfectivo = idControlEfectivo;
    this.idControlDocumentacion = idControlDocumentacion;
    this.idControlAnalisis = idControlAnalisis;
    this.idObservacion = idObservacion;
    this.fechaRevision = fechaRevision != null ? new Date(fechaRevision.getTime()) : null;
    this.activo = activo;
    this.fechaAlta = fechaAlta != null ? new Date(fechaAlta.getTime()) : null;
    this.fechaMod = fechaMod != null ? new Date(fechaMod.getTime()) : null;
    this.fechaBaja = fechaBaja != null ? new Date(fechaBaja.getTime()) : null;
    this.usuarioAlta = usuarioAlta;
    this.usuarioMod = usuarioMod;
    this.usuarioBaja = usuarioBaja;
  }

  /**
   * Instantiates a new blanqueo cliente DTO.
   *
   * @param entity the entity
   */
  public BlanqueoClientesDTO(BlanqueoClientesEntity entity) {
    this.id = entity.getId();
    this.idControlEfectivo = entity.getIdControlEfectivo().getId();
    this.idControlDocumentacion = entity.getIdControlDocumentacion().getId();
    this.idControlAnalisis = entity.getIdControlAnalisis();
    this.idObservacion = entity.getIdObservacion().getId();
    this.fechaRevision = entity.getFechaRevision();
    this.activo = entity.getActivo();
    this.fechaAlta = entity.getFechaAlta();
    this.fechaMod = entity.getFechaMod();
    this.fechaBaja = entity.getFechaBaja();
    this.usuarioAlta = entity.getUsuarioAlta();
    this.usuarioMod = entity.getUsuarioMod();
    this.usuarioBaja = entity.getUsuarioBaja();
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the id control efectivo.
   *
   * @return the id control efectivo
   */
  public Integer getIdControlEfectivo() {
    return idControlEfectivo;
  }

  /**
   * Sets the id control efectivo.
   *
   * @param IdControlEfectivo the new id control efectivo
   */
  public void setIdControlEfectivo(Integer idControlEfectivo) {
    this.idControlEfectivo = idControlEfectivo;
  }

  /**
   * Gets the id control documentacion.
   *
   * @return the id control documentacion
   */
  public String getIdControlDocumentacion() {
    return idControlDocumentacion;
  }

  /**
   * Sets the id control documentacion.
   *
   * @param IdControlDocumentacion the new id control documentacion
   */
  public void setIdControlDocumentacion(String idControlDocumentacion) {
    this.idControlDocumentacion = idControlDocumentacion;
  }

  /**
   * Gets the id control analisis.
   *
   * @return the id control analisis
   */
  public String getIdControlAnalisis() {
    return idControlAnalisis;
  }

  /**
   * Sets the id control analisis.
   *
   * @param IdControlAnalisis the new id control analisis
   */
  public void setIdControlAnalisis(String idControlAnalisis) {
    this.idControlAnalisis = idControlAnalisis;
  }

  /**
   * Gets the id observacion.
   *
   * @return the id observacion
   */
  public String getIdObservacion() {
    return idObservacion;
  }

  /**
   * Sets the id observacion.
   *
   * @param idObservacion the new id observacion
   */
  public void setIdObservacion(String idObservacion) {
    this.idObservacion = idObservacion;
  }

  /**
   * Gets the fecha revision.
   *
   * @return the fecha revision
   */
  public Date getFechaRevision() {
    return fechaRevision != null ? new Date(fechaRevision.getTime()) : null;
  }

  /**
   * Sets the fecha revision.
   *
   * @param fechaRevision the new fecha revision
   */
  public void setFechaRevision(Date fechaRevision) {
    this.fechaRevision = fechaRevision != null ? new Date(fechaRevision.getTime()) : null;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Integer getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(Integer activo) {
    this.activo = activo;
  }

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Date getFechaAlta() {
    return fechaAlta != null ? (Date) fechaAlta.clone() : null;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaAlta(Date fechaAlta) {
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Date getFechaMod() {
    return fechaMod != null ? (Date) fechaMod.clone() : null;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechaMod(Date fechaMod) {
    this.fechaMod = (fechaMod != null ? (Date) fechaMod.clone() : null);
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Date getFechaBaja() {
    return fechaBaja != null ? (Date) fechaBaja.clone() : null;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechaBaja(Date fechaBaja) {
    this.fechaBaja = (fechaBaja != null ? (Date) fechaBaja.clone() : null);
  }

  /**
   * Gets the usuarioalta.
   *
   * @return the usuarioalta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * Sets the usuarioalta.
   *
   * @param usuarioalta the new usuarioalta
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * Gets the usuariomod.
   *
   * @return the usuariomod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * Sets the usuariomod.
   *
   * @param usuariomod the new usuariomod
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * Gets the usuariobaja.
   *
   * @return the usuariobaja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * Sets the usuariobaja.
   *
   * @param usuariobaja the new usuariobaja
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "BlanqueoClientesDTO [id=" + id + ", idControlEfectivo=" + idControlEfectivo + ", idControlDocumentacion="
        + idControlDocumentacion + ", idControlAnalisis=" + idControlAnalisis + ", idObservacion=" + idObservacion
        + ", fechaRevision=" + fechaRevision + ", activo=" + activo + ", fechaAlta=" + fechaAlta + ", fechaMod="
        + fechaMod + ", fechaBaja=" + fechaBaja + ", usuarioAlta=" + usuarioAlta + ", usuarioMod=" + usuarioMod
        + ", usuarioBaja=" + usuarioBaja + "]";
  }

}
