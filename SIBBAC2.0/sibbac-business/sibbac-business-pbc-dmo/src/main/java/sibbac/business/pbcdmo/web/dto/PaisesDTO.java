package sibbac.business.pbcdmo.web.dto;

import java.util.Date;

import sibbac.business.pbcdmo.db.model.PaisesEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractDTO;

/**
 * The Class TMCT0PaisesDTO.
 */
public class PaisesDTO extends AbstractDTO {

  private static final long serialVersionUID = 5674183760286670445L;

  /** The nbpais. */
  private String nbpais;

  /** The cdisoalf 2. */
  private String cdisoalf2;

  /** The cdisoalf 3. */
  private String cdisoalf3;

  /** The cdisonum. */
  private String cdisonum;

  /** The cdhacienda. */
  private String cdhacienda;

  /** The cdcodkgr. */
  private String cdcodkgr;

  /** The cdestado. */
  private String cdestado;

  /** The audit date. */
  private Date auditDate;

  /** The audit user. */
  private String auditUser;

  /** The cd tipo id mifid. */
  private String cdTipoIdMifid;

  /** The indicadorDMO. */
  private String indicadorDMO;

  /** The fecInicioParFisc. */
  private Date fecInicioParFisc;

  /** The fecFinParFisc. */
  private Date fecFinParFisc;

  /**
   * Instantiates a new TMCT 0 paises DTO.
   *
   * @param entity the entity
   */
  public PaisesDTO(PaisesEntity entity) {
    this.entityToDto(entity);
  }

  /**
   * Instantiates a new TMCT 0 paises DTO.
   */
  public PaisesDTO() {
    super();
  }

  /**
   * Instantiates a new TMCT 0 paises DTO.
   *
   * @param nbpais the nbpais
   * @param cdisoalf2 the cdisoalf 2
   * @param cdisoalf3 the cdisoalf 3
   * @param cdisonum the cdisonum
   * @param cdhacienda the cdhacienda
   * @param cdcodkgr the cdcodkgr
   * @param cdestado the cdestado
   * @param auditDate the audit date
   * @param auditUser the audit user
   * @param cdTipoIdMifid the cd tipo id mifid
   */
  public PaisesDTO(String nbpais, String cdisoalf2, String cdisoalf3, String cdisonum, String cdhacienda,
      String cdcodkgr, String cdestado, Date auditDate, String auditUser, String cdTipoIdMifid, Date fecInicioParFisc,
      Date fecFinParFisc, String indicadorDMO) {
    super();
    this.nbpais = nbpais;
    this.cdisoalf2 = cdisoalf2;
    this.cdisoalf3 = cdisoalf3;
    this.cdisonum = cdisonum;
    this.cdhacienda = cdhacienda;
    this.cdcodkgr = cdcodkgr;
    this.cdestado = cdestado;
    this.auditDate = (Date) auditDate.clone();
    this.auditUser = auditUser;
    this.cdTipoIdMifid = cdTipoIdMifid;
    this.fecInicioParFisc = (Date) fecInicioParFisc.clone();
    this.fecFinParFisc = (Date) fecFinParFisc.clone();
    this.indicadorDMO = indicadorDMO;
  }

  /**
   * Entity to dto.
   *
   * @param entity the entity
   */
  private void entityToDto(PaisesEntity entity) {

    this.nbpais = entity.getNbpais();
    this.cdisoalf2 = entity.getPrimaryKey().getCdisoalf2();
    this.cdisoalf3 = entity.getPrimaryKey().getCdisoalf3();
    this.cdisonum = entity.getPrimaryKey().getCdisonum();
    this.cdhacienda = entity.getCdhacienda();
    this.cdcodkgr = entity.getCdcodkgr();
    this.cdestado = entity.getCdestado();
    this.auditDate = new Date(entity.getAuditDate().getTime());
    this.auditUser = entity.getAuditUser();
    this.cdTipoIdMifid = entity.getCdTipoIdMifid();
    this.fecInicioParFisc = new Date(entity.getFecInicioParFisc().getTime());
    this.fecFinParFisc = new Date(entity.getFecFinParFisc().getTime());
    this.indicadorDMO = entity.getIndicadorDMO();

  }

  /**
   * Gets the nbpais.
   *
   * @return the nbpais
   */
  public String getNbpais() {
    return nbpais;
  }

  /**
   * Sets the nbpais.
   *
   * @param nbpais the new nbpais
   */
  public void setNbpais(String nbpais) {
    this.nbpais = nbpais;
  }

  /**
   * Gets the cdisoalf 2.
   *
   * @return the cdisoalf 2
   */
  public String getCdisoalf2() {
    return cdisoalf2;
  }

  /**
   * Sets the cdisoalf 2.
   *
   * @param cdisoalf2 the new cdisoalf 2
   */
  public void setCdisoalf2(String cdisoalf2) {
    this.cdisoalf2 = cdisoalf2;
  }

  /**
   * Gets the cdisoalf 3.
   *
   * @return the cdisoalf 3
   */
  public String getCdisoalf3() {
    return cdisoalf3;
  }

  /**
   * Sets the cdisoalf 3.
   *
   * @param cdisoalf3 the new cdisoalf 3
   */
  public void setCdisoalf3(String cdisoalf3) {
    this.cdisoalf3 = cdisoalf3;
  }

  /**
   * Gets the cdisonum.
   *
   * @return the cdisonum
   */
  public String getCdisonum() {
    return cdisonum;
  }

  /**
   * Sets the cdisonum.
   *
   * @param cdisonum the new cdisonum
   */
  public void setCdisonum(String cdisonum) {
    this.cdisonum = cdisonum;
  }

  /**
   * Gets the cdhacienda.
   *
   * @return the cdhacienda
   */
  public String getCdhacienda() {
    return cdhacienda;
  }

  /**
   * Sets the cdhacienda.
   *
   * @param cdhacienda the new cdhacienda
   */
  public void setCdhacienda(String cdhacienda) {
    this.cdhacienda = cdhacienda;
  }

  /**
   * Gets the cdcodkgr.
   *
   * @return the cdcodkgr
   */
  public String getCdcodkgr() {
    return cdcodkgr;
  }

  /**
   * Sets the cdcodkgr.
   *
   * @param cdcodkgr the new cdcodkgr
   */
  public void setCdcodkgr(String cdcodkgr) {
    this.cdcodkgr = cdcodkgr;
  }

  /**
   * Gets the cdestado.
   *
   * @return the cdestado
   */
  public String getCdestado() {
    return cdestado;
  }

  /**
   * Sets the cdestado.
   *
   * @param cdestado the new cdestado
   */
  public void setCdestado(String cdestado) {
    this.cdestado = cdestado;
  }

  /**
   * Gets the audit date.
   *
   * @return the audit date
   */
  public Date getAuditDate() {
    if (auditDate != null) {
      this.auditDate = (Date) auditDate.clone();
    }
    return auditDate != null ? new Date(auditDate.getTime()) : null;
  }

  /**
   * Sets the audit date.
   *
   * @param auditDate the new audit date
   */
  public void setAuditDate(Date auditDate) {
    this.auditDate = (Date) auditDate.clone();
  }

  /**
   * Gets the audit user.
   *
   * @return the audit user
   */
  public String getAuditUser() {
    return auditUser;
  }

  /**
   * Sets the audit user.
   *
   * @param auditUser the new audit user
   */
  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * Gets the cd tipo id mifid.
   *
   * @return the cd tipo id mifid
   */
  public String getCdTipoIdMifid() {
    return cdTipoIdMifid;
  }

  /**
   * Sets the cd tipo id mifid.
   *
   * @param cdTipoIdMifid the new cd tipo id mifid
   */
  public void setCdTipoIdMifid(String cdTipoIdMifid) {
    this.cdTipoIdMifid = cdTipoIdMifid;
  }

  /**
   * Gets fecInicioParFisc.
   *
   * @return the fecInicioParFisc
   */
  public Date getFecInicioParFisc() {

    Date fechaIniPF;

    if (fecInicioParFisc == null) {
      fechaIniPF = null;
    }
    else {
      fechaIniPF = (Date) fecInicioParFisc.clone();
    }

    return fechaIniPF;
  }

  /**
   * Sets the fecInicioParFisc.
   *
   * @param cdTipoIdMifid the new fecInicioParFisc
   */
  public void setFecInicioParFisc(Date fecInicioParFisc) {
    this.fecInicioParFisc = (Date) fecInicioParFisc.clone();
  }

  /**
   * Gets the fecFinParFisc.
   *
   * @return the fecFinParFisc
   */
  public Date getFecFinParFisc() {

    Date fechaFinPF;

    if (fecFinParFisc == null) {
      fechaFinPF = null;
    }
    else {
      fechaFinPF = (Date) fecFinParFisc.clone();
    }

    return fechaFinPF;
  }

  /**
   * Sets the fecFinParFisc.
   *
   * @param cdTipoIdMifid the fecFinParFisc
   */
  public void setFecFinParFisc(Date fecFinParFisc) {
    this.fecFinParFisc = (Date) fecFinParFisc.clone();
  }

  /**
   * Gets the indicadorDMO.
   *
   * @return the indicadorDMO
   */
  public String getIndicadorDMO() {
    return indicadorDMO;
  }

  /**
   * Sets the indicadorDMO.
   *
   * @param indicadorDMO the indicadorDMO
   */
  public void setIndicadorDMO(String indicadorDMO) {
    this.indicadorDMO = indicadorDMO;
  }

  /**
   * Salida toString del objeto
   * 
   */
  @Override
  public String toString() {
    return "TMCT0PaisesDTO [nbpais=" + nbpais + ", cdisoalf2=" + cdisoalf2 + ", cdisoalf3=" + cdisoalf3 + ", cdisonum="
        + cdisonum + ", cdhacienda=" + cdhacienda + ", cdcodkgr=" + cdcodkgr + ", cdestado=" + cdestado + ", auditDate="
        + auditDate + ", auditUser=" + auditUser + ", cdTipoIdMifid=" + cdTipoIdMifid + ", indicadorDMO=" + indicadorDMO
        + ", fecInicioParFisc=" + fecInicioParFisc + ", fecFinParFisc=" + fecFinParFisc + "]";
  }

}
