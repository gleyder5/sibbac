package sibbac.business.pbcdmo.web.dto;

import java.util.Date;

import sibbac.business.pbcdmo.db.model.BlanqueoControlAnalisisEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractControlDTO;

/**
 * The Class TMCT0blanqueoControlAnalisisDTO.
 */
public class BlanqueoControlAnalisisDTO extends AbstractControlDTO<BlanqueoControlAnalisisEntity> {

  private static final long serialVersionUID = -358186853881391023L;

  /** The id. */
  private String id;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new TMCT 0 blanqueo control analisis DTO.
   */
  public BlanqueoControlAnalisisDTO() {
    super();
  }

  /**
   * Instantiates a new TMCT 0 blanqueo control analisis DTO.
   *
   * @param id the id
   * @param descripcion the descripcion
   * @param fechaAlta the fecha alta
   * @param fechaMod the fecha mod
   * @param fechaBaja the fecha baja
   * @param usuarioAlta the usuario alta
   * @param usuarioMod the usuario mod
   * @param usuarioBaja the usuario baja
   */
  public BlanqueoControlAnalisisDTO(String id, String descripcion, Date fechaAlta, Date fechaMod, Date fechaBaja,
      String usuarioAlta, String usuarioMod, String usuarioBaja) {
    super(fechaAlta, fechaMod, fechaBaja, usuarioAlta, usuarioMod, usuarioBaja);
    this.id = id;
    this.descripcion = descripcion;
  }

  /**
   * Instantiates a new TMCT 0 blanqueo control analisis DTO.
   *
   * @param entity the entity
   */
  public BlanqueoControlAnalisisDTO(BlanqueoControlAnalisisEntity entity) {
    this.toDto(entity);
  }

  @Override
  protected void toThisDto(BlanqueoControlAnalisisEntity entity) {
    this.id = entity.getId();
    this.descripcion = entity.getDescripcion();
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * S Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "TMCT0blanqueoControlAnalisisEntity [id=" + id + ", descripcion=" + descripcion + ", fechaAlta=" + fechaAlta
        + ", fechaMod=" + fechaMod + ", fechaBaja=" + fechaBaja + ", usuarioAlta=" + usuarioAlta + ", usuarioMod="
        + usuarioMod + ", usuarioBaja=" + usuarioBaja + "]";
  }
}
