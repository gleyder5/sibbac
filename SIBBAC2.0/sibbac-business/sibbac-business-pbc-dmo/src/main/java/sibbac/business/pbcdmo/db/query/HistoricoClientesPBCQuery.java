package sibbac.business.pbcdmo.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class ClientesPBCQuery.
 */
@Component("HistoricoClientesPBCQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HistoricoClientesPBCQuery extends AbstractDynamicPageQuery {

  /** The Constant ID_CLIENTE. */
  private static final String ID_CLIENTE = "blacli.ID_CLIENTE";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT blacli.ID_CLIENTE AS IDCLIENTE, cli.CDKGR AS CODKGR,"
      + "efe.ID AS IDEFE, efe.DESCRIPCION AS DESEFE, doc.ID AS IDDOC, doc.DESCRIPCION AS DESDOC, ana.ID AS IDANA,"
      + "ana.DESCRIPCION DESANA, bsnbpsql.TMCT0_BLANQUEO_OBSERVACION.DESCRIPCION DESOBS";

  /** The Constant FROM. */
  private static final String FROM = "FROM bsnbpsql.TMCT0_BLANQUEO_CLIENTE_HIST blacli"
      + "INNER JOIN bsnbpsql.TMCT0CLI cli ON cli.CDBROCLI = blacli.ID_CLIENTE"
      + "INNER JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_EFECTIVO efe ON efe.ID = blacli.ID_CONTROL_EFECTIVO"
      + "INNER JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_DOCUMENTACION doc ON doc.ID = blacli.ID_CONTROL_DOCUMENTACION"
      + "INNER JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_ANALISIS ana ON ana.ID = blacli.ID_CONTROL_ANALISIS"
      + "LEFT OUTER JOIN bsnbpsql.TMCT0_BLANQUEO_OBSERVACION ON blacli.ID_OBSERVACION = bsnbpsql.TMCT0_BLANQUEO_OBSERVACION.ID";

  /** The Constant WHERE. */
  private static final String WHERE = "WHERE blacli.FECHA_BAJA IS NULL ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("Cliente", "IDCLIENTE"),
      new DynamicColumn("Código KGR", "CODKGR"), new DynamicColumn("Control efectivo", "DESEFE"),
      new DynamicColumn("Control documentación", "DESDOC"), new DynamicColumn("Control análisis", "DESANA"),
      new DynamicColumn("Observación", "DESOBS") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter(ID_CLIENTE, "Cliente"));
    return res;
  }

  public HistoricoClientesPBCQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public HistoricoClientesPBCQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public HistoricoClientesPBCQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}
