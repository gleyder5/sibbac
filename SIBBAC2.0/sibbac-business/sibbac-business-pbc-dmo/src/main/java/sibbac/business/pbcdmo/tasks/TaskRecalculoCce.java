package sibbac.business.pbcdmo.tasks;

import java.util.Calendar;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.pbcdmo.bo.BlanqueoClientesBo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

//0 0 7 1 * ? Fire at 7:00am on the 1st day of every month
//0 0 0 1 1 * Fire the first day of every year
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_PBC.NAME, name = Task.GROUP_PBC.JOB_RECALCULO_CCE, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 0 1 JAN ? 2099")
public class TaskRecalculoCce extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private BlanqueoClientesBo blanqueoClientes;

  protected static final Logger LOG = LoggerFactory.getLogger(TaskRecalculoCce.class);

  @Override
  public void executeTask() throws Exception {
    LOG.trace("[TaskPbcRecalculoCCe :: executeTask] inicio");
    try {
      final Calendar prevYear = Calendar.getInstance();

      prevYear.add(Calendar.YEAR, -1);

      blanqueoClientes.procesoRecalculoCce(prevYear.get(Calendar.YEAR));

      LOG.info("[TaskPbcRecalculoCCe :: executeTask] Fin del proceso.");
    }
    catch (RuntimeException e) {
      LOG.error("[TaskPbcRecalculoCCe :: executeTask] Error en la ejecución de la operativa Recalculo CCE.", e);
    }

  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_PBC;
  }

}
