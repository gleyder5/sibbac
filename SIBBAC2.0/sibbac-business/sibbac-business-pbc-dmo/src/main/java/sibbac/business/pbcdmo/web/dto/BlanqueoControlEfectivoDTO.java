package sibbac.business.pbcdmo.web.dto;

import java.util.Date;

import sibbac.business.pbcdmo.db.model.BlanqueoControlEfectivoEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractControlDTO;

/**
 * The Class TMCT0BlanqueoControlEfectivoDTO.
 */
public class BlanqueoControlEfectivoDTO extends AbstractControlDTO<BlanqueoControlEfectivoEntity> {

  private static final long serialVersionUID = -7789449552727425167L;

  /** The id. */
  private Integer id;

  /** The descripcion. */
  private String descripcion;

  /** The num titulos max. */
  private String numTitulosMax;

  /** The importe efectivo max. */
  private String importeEfectivoMax;

  /** The per titulos max. */
  private String perTitulosMax;

  /** The per efectivo max. */
  private String perEfectivoMax;

  /**
   * Instantiates a new TMCT 0 blanqueo control efectivo DTO.
   */
  public BlanqueoControlEfectivoDTO() {
    super();
  }

  /**
   * Instantiates a new TMCT 0 blanqueo control efectivo DTO.
   *
   * @param id the id
   * @param descripcion the descripcion
   * @param numTitulosMax the num titulos max
   * @param importeEfectivoMax the importe efectivo max
   * @param perTitulosMax the per titulos max
   * @param perEfectivoMax the per efectivo max
   * @param fechaAlta the fecha alta
   * @param fechaMod the fecha mod
   * @param fechaBaja the fecha baja
   * @param usuarioAlta the usuario alta
   * @param usuarioMod the usuario mod
   * @param usuarioBaja the usuario baja
   */
  public BlanqueoControlEfectivoDTO(Integer id, String descripcion, String numTitulosMax, String importeEfectivoMax,
      String perTitulosMax, String perEfectivoMax, Date fechaAlta, Date fechaMod, Date fechaBaja, String usuarioAlta,
      String usuarioMod, String usuarioBaja) {
    super(fechaAlta, fechaMod, fechaBaja, usuarioAlta, usuarioMod, usuarioBaja);
    this.id = id;
    this.descripcion = descripcion;
    this.numTitulosMax = numTitulosMax;
    this.importeEfectivoMax = importeEfectivoMax;
    this.perTitulosMax = perTitulosMax;
    this.perEfectivoMax = perEfectivoMax;
  }

  /**
   * Instantiates a new TMCT 0 blanqueo control efectivo DTO.
   *
   * @param entity the entity
   */
  public BlanqueoControlEfectivoDTO(BlanqueoControlEfectivoEntity entity) {
    this.toDto(entity);
  }

  @Override
  public void toThisDto(BlanqueoControlEfectivoEntity entity) {
    this.id = entity.getId();
    this.descripcion = entity.getDescripcion();
    this.numTitulosMax = entity.getNumTitulosMax().toString();
    this.importeEfectivoMax = entity.getImporteEfectivoMax().toString();
    this.perTitulosMax = entity.getPerTitulosMax().toString();
    this.perEfectivoMax = entity.getPerEfectivoMax().toString();
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the num titulos max.
   *
   * @return the num titulos max
   */
  public String getNumTitulosMax() {
    return numTitulosMax;
  }

  /**
   * Sets the num titulos max.
   *
   * @param numTitulosMax the new num titulos max
   */
  public void setNumTitulosMax(String numTitulosMax) {
    this.numTitulosMax = numTitulosMax;
  }

  /**
   * Gets the importe efectivo max.
   *
   * @return the importe efectivo max
   */
  public String getImporteEfectivoMax() {
    return importeEfectivoMax;
  }

  /**
   * Sets the importe efectivo max.
   *
   * @param importeEfectivoMax the new importe efectivo max
   */
  public void setImporteEfectivoMax(String importeEfectivoMax) {
    this.importeEfectivoMax = importeEfectivoMax;
  }

  /**
   * Gets the per titulos max.
   *
   * @return the per titulos max
   */
  public String getPerTitulosMax() {
    return perTitulosMax;
  }

  /**
   * Sets the per titulos max.
   *
   * @param perTitulosMax the new per titulos max
   */
  public void setPerTitulosMax(String perTitulosMax) {
    this.perTitulosMax = perTitulosMax;
  }

  /**
   * Gets the per efectivo max.
   *
   * @return the per efectivo max
   */
  public String getPerEfectivoMax() {
    return perEfectivoMax;
  }

  /**
   * Sets the per efectivo max.
   *
   * @param perEfectivoMax the new per efectivo max
   */
  public void setPerEfectivoMax(String perEfectivoMax) {
    this.perEfectivoMax = perEfectivoMax;
  }

  /**
   * Salida toString del objeto
   * 
   */
  @Override
  public String toString() {
    return "[id=" + id + ", descripcion=" + descripcion + ", numTitulosMax=" + numTitulosMax + ", importeEfectivoMax="
        + importeEfectivoMax + ", perTitulosMax=" + perTitulosMax + ", perEfectivoMax=" + perEfectivoMax
        + ", fechaAlta=" + fechaAlta + ", fechaMod=" + fechaMod + ", fechaBaja=" + fechaBaja + ", usuarioAlta="
        + usuarioAlta + ", usuarioMod=" + usuarioMod + ", usuarioBaja=" + usuarioBaja + "]";
  }

}
