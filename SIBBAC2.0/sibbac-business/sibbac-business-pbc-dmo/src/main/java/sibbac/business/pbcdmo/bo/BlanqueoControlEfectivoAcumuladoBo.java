package sibbac.business.pbcdmo.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.pbcdmo.db.dao.BlanqueoEfectivoAcumuladoDAO;
import sibbac.business.pbcdmo.db.model.BlanqueoEfectivoAcumuladoEntity;
import sibbac.business.pbcdmo.tasks.TaskRecalculoCce;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlEfectivoAcumuladoDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.util.Constantes;
import sibbac.business.pbcdmo.web.util.Constantes.CargaEfectivo;
import sibbac.business.pbcdmo.web.util.Constantes.Controles;
import sibbac.business.pbcdmo.web.util.Constantes.Session;
import sibbac.business.pbcdmo.web.util.ConversionDtoToEntity;
import sibbac.common.CallableResultDTO;
import sibbac.database.bo.AbstractBo;

@Service("efectivoAcumuladoBo")
public class BlanqueoControlEfectivoAcumuladoBo
    extends AbstractBo<BlanqueoEfectivoAcumuladoEntity, BigInteger, BlanqueoEfectivoAcumuladoDAO>
    implements BaseBo<BlanqueoControlEfectivoAcumuladoDTO> {

  @Autowired
  private BlanqueoEfectivoAcumuladoDAO efectivoAcumulado;

  protected static final Logger LOG = LoggerFactory.getLogger(TaskRecalculoCce.class);

  private static final String dischargeUser = "CARGA_EFECTIVO";

  @Override
  public CallableResultDTO actionFindById(BlanqueoControlEfectivoAcumuladoDTO solicitud) {
    return null;
  }

  @Override
  public CallableResultDTO actionSave(BlanqueoControlEfectivoAcumuladoDTO solicitud) {
    return null;
  }

  @Override
  public CallableResultDTO actionUpdate(BlanqueoControlEfectivoAcumuladoDTO solicitud) {
    return null;
  }

  @Override
  public CallableResultDTO actionDelete(List<String> solicitud) {
    return null;
  }

  // TODO Eliminar comentarios
  public void effectiveCustomersLoad(int executionType) {
    Date referenceDate = new Date();
    Calendar c = Calendar.getInstance();
    c.setTime(referenceDate);
    Date dateFrom = null;
    Date dateTo = null;
    java.sql.Date sqldateFrom = null;
    java.sql.Date sqldateTo = null;

    // Ejecución mensual
    if (executionType == 0) {      
      // Mes anterior
      c.add(Calendar.MONTH, -1);
      // Primer día del mes
      c.set(Calendar.DAY_OF_MONTH, 1);
      dateFrom = c.getTime();
      // Último día del mes
      c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
      dateTo = c.getTime();
    }
    // Ejecución anual
    else {      

      // Año anterior
      c.add(Calendar.YEAR, -1);
      // Primer día del mes del año anterior
      c.set(Calendar.DAY_OF_MONTH, 1);
      dateFrom = c.getTime();
      // Último día del mes anterior a hoy
      // Mes anterior
      Calendar cal = Calendar.getInstance();
      cal.setTime(referenceDate);
      cal.add(Calendar.MONTH, -1);
      // Último día del mes
      cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
      dateTo = cal.getTime();
    }

    sqldateFrom = new java.sql.Date(dateFrom.getTime());
    sqldateTo = new java.sql.Date(dateTo.getTime());
    executeEffectiveCustomersLoad(sqldateFrom, sqldateTo);

  }

  /**
   * Ejecuta el proceso: Si es mensual únicamente inserta, si es anual actualiza
   * e inserta
   * @param sqldateFrom
   * @param sqldateTo
   */
  private void executeEffectiveCustomersLoad(java.sql.Date sqldateFrom, java.sql.Date sqldateTo) {
    List<Object[]> effectiveClientsOperationsList = null;
    try {
      // Baja lógica
      effectiveClientsOperationsList = efectivoAcumulado.findEffectiveClientsOperations(
          CargaEfectivo.CDESTADOASIGNADO0.value(), CargaEfectivo.CDESTADOASIGNADO65.value(), Controles.CDDEPREV.value(),
          Controles.CDKGR.value(), CargaEfectivo.NUTITLIQ.value(), sqldateFrom, sqldateTo);
      if (effectiveClientsOperationsList != null && !effectiveClientsOperationsList.isEmpty()
          && effectiveClientsOperationsList.size() != 0) {
        List<BlanqueoEfectivoAcumuladoEntity> beaeList = new ArrayList<>();
        for (Object[] resultList : effectiveClientsOperationsList) {
          // Baja lógica
          customersLogicUnsubscribe(resultList);
          // beaeList = saveEffectiveCustomer(resultList);
          beaeList.add(saveEffectiveCustomer(resultList));
        }
        // Insercción --> Se guarda la entity según el DTO
        dao.save(beaeList);
      }
    }
    catch (Exception e) {
      LOG.error("Error en la ejecución mensual del proceso PBC carga de efectivos", e);
    }
  }

  /**
   * Realiza el proceso de baja lógica
   * @param effectiveClientsOperationsList
   */
  private void customersLogicUnsubscribe(Object[] resultList) {

    /*
     * Comprobamos si existe el registro para en ese caso realizar una baja
     * lógica ACTIVO a “0” y actualizando los campos de auditoría FECHA_BAJA y
     * USUARIO_BAJA
     */
    int year = (int) resultList[0];
    int month = (int) resultList[1];
    String idCliente = resultList[3].toString();
    List<Object[]> effectiveAcumulatedList = null;
    effectiveAcumulatedList = efectivoAcumulado.findEffectiveAcumulated(idCliente, month, year);
    if (effectiveAcumulatedList != null && !effectiveAcumulatedList.isEmpty() && effectiveAcumulatedList.size() != 0) {
      // Baja lógica
      Timestamp dischargeDate = new Timestamp(System.currentTimeMillis());
      efectivoAcumulado.updateExistingClientsOperations(dischargeDate, dischargeUser, month, year, idCliente);
    }
  }

  /**
   * Crea el nuevo registro a insertar
   * @param effectiveClientsOperation
   * @return
   */
  private BlanqueoControlEfectivoAcumuladoDTO setBlanqueoEfectivoAcumuladoDTO(Object[] effectiveClientsOperation) {
    // ANYO = FHANOPRC
    int year = (int) effectiveClientsOperation[0];
    // MES = FHMESPRC
    int month = (int) effectiveClientsOperation[1];
    // ID_CLIENTE = CDBROCLI
    String idCliente = effectiveClientsOperation[3].toString();
    // Títulos acumulados --> NUCANACM
    BigDecimal accumulatedTitles = (BigDecimal) effectiveClientsOperation[4];
    // EFECTIVO_ACUMULADO --> ImporteAcumulado = IMEFEAC
    BigDecimal accumulatedEffective = (BigDecimal) effectiveClientsOperation[5];
    // FECHA_ULTIMA_OPERACION_PROC = FHMAX
    Date lastOperationdate = (Date) effectiveClientsOperation[6];
    // Fecha alta
    Timestamp registerTimestamp = new Timestamp(System.currentTimeMillis());
    Date registerDate = new Date(registerTimestamp.getTime());
    // USUARIO_ALTA = Si es una alta: ”CARGA_CLIENTES_PBC”

    BlanqueoControlEfectivoAcumuladoDTO beaDto = new BlanqueoControlEfectivoAcumuladoDTO(idCliente, month, year,
        accumulatedTitles, accumulatedEffective, lastOperationdate, registerDate, null, null,
        Session.USER_CLIENTE.value(), "", "");

    return beaDto;
  }

  /**
   * Almacena el elemento creado
   * @param effectiveClientsOperation
   */

  private BlanqueoEfectivoAcumuladoEntity saveEffectiveCustomer(Object[] effectiveClientsOperation) {
    BlanqueoControlEfectivoAcumuladoDTO bcea = setBlanqueoEfectivoAcumuladoDTO(effectiveClientsOperation);
    BlanqueoEfectivoAcumuladoEntity entity = new BlanqueoEfectivoAcumuladoEntity();
    ConversionDtoToEntity.DtoToEntityEfectivoAcumulado(entity, bcea);
    return entity;
  }

  @Override
  public CallableResultDTO update(ObservacionClienteDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }
}
