package sibbac.business.pbcdmo.web.dto;

/**
 * The Class AnalisisObservacionDTO.
 */
public class AnalisisObservacionDTO {

  /** The id. */
  private String id;

  /** The descripcion. */
  private String descripcion;

  /**
   * Instantiates a new analisis observacion DTO.
   *
   * @param id the id
   * @param descripcion the descripcion
   */
  public AnalisisObservacionDTO(String id, String descripcion) {
    super();
    this.id = id;
    this.descripcion = descripcion;
  }

  /**
   * Instantiates a new analisis observacion DTO.
   */
  public AnalisisObservacionDTO() {
    super();
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

}
