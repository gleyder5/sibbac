package sibbac.business.pbcdmo.web.dto;

import sibbac.business.pbcdmo.db.model.PaisesPK;
import sibbac.business.pbcdmo.web.dto.common.AbstractDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class PaisesPKDTO.
 */
public class PaisesPKDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3647049612181077873L;

  /** The cdisoalf 2. */
  private String cdisoalf2;

  /** The cdisoalf 3. */
  private String cdisoalf3;

  /** The cdisonum. */
  private String cdisonum;

  /**
   * Instantiates a new TMCT 0 paises DTO.
   *
   * @param entity the entity
   */
  public PaisesPKDTO(PaisesPK entity) {
    this.entityToDto(entity);
  }

  /**
   * Instantiates a new TMCT 0 paises DTO.
   */
  public PaisesPKDTO() {
    super();
  }

  /**
   * Instantiates a new TMCT 0 paises DTO.
   *
   * @param cdisoalf2 the cdisoalf 2
   * @param cdisoalf3 the cdisoalf 3
   * @param cdisonum the cdisonum
   */
  public PaisesPKDTO(String cdisoalf2, String cdisoalf3, String cdisonum) {
    super();
    this.cdisoalf2 = cdisoalf2;
    this.cdisoalf3 = cdisoalf3;
    this.cdisonum = cdisonum;
  }

  /**
   * Entity to dto.
   *
   * @param entity the entity
   */
  private void entityToDto(PaisesPK entity) {

    this.cdisoalf2 = entity.getCdisoalf2();
    this.cdisoalf3 = entity.getCdisoalf3();
    this.cdisonum = entity.getCdisonum();
  }

  /**
   * Gets the cdisoalf 2.
   *
   * @return the cdisoalf 2
   */
  public String getCdisoalf2() {
    return cdisoalf2;
  }

  /**
   * Sets the cdisoalf 2.
   *
   * @param cdisoalf2 the new cdisoalf 2
   */
  public void setCdisoalf2(String cdisoalf2) {
    this.cdisoalf2 = cdisoalf2;
  }

  /**
   * Gets the cdisoalf 3.
   *
   * @return the cdisoalf 3
   */
  public String getCdisoalf3() {
    return cdisoalf3;
  }

  /**
   * Sets the cdisoalf 3.
   *
   * @param cdisoalf3 the new cdisoalf 3
   */
  public void setCdisoalf3(String cdisoalf3) {
    this.cdisoalf3 = cdisoalf3;
  }

  /**
   * Gets the cdisonum.
   *
   * @return the cdisonum
   */
  public String getCdisonum() {
    return cdisonum;
  }

  /**
   * Sets the cdisonum.
   *
   * @param cdisonum the new cdisonum
   */
  public void setCdisonum(String cdisonum) {
    this.cdisonum = cdisonum;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  public String toString() {
    return "TMCT0PaisesDTO [cdisoalf2=" + cdisoalf2 + ", cdisoalf3=" + cdisoalf3 + ", cdisonum=" + cdisonum + "]";
  }

}
