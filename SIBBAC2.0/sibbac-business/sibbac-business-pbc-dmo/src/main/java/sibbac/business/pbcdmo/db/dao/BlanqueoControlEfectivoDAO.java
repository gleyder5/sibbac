package sibbac.business.pbcdmo.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.pbcdmo.db.model.BlanqueoControlEfectivoEntity;

/**
 * The Interface TMCT0BlanqueoControlEfectivoDAO.
 */
@Repository
public interface BlanqueoControlEfectivoDAO extends JpaRepository<BlanqueoControlEfectivoEntity, Integer> {

  @Query(value = "SELECT cf.id, cf.descripcion "
      + "FROM TMCT0_BLANQUEO_CONTROL_EFECTIVO cf WHERE cf.fecha_baja IS NULL ", nativeQuery = true)
  List<Object[]> findListEfectivo();

}