package sibbac.business.pbcdmo.exception;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import sibbac.common.CallableResultDTO;

/**
 * Sibbac Exception Handler
 * @author ajimenme
 *
 */
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {


  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
          MethodArgumentNotValidException ex,
          HttpHeaders headers, HttpStatus status,
          WebRequest request) {
    
      BindingResult bindingResult = ex
              .getBindingResult();
      
      CallableResultDTO result = new CallableResultDTO();
      
      List<FieldError> errors= bindingResult.getFieldErrors();
      
      if(errors!=null && errors.size()>0){
        for(FieldError fieldError:errors){
          
          if(validateField(fieldError, NotNull.class)){
              result.addErrorMessage("El campo " + fieldError.getField() + " es obligatorio");
          }else if(validateField(fieldError, Size.class)){
              result.addErrorMessage("El campo " + fieldError.getField() + " ha sido informado con un tamaño incorrecto");
          }else{
              result.addErrorMessage("Se ha producido un error en la validación del campo " + fieldError.getField());
          }
        }
      }

      return new ResponseEntity<Object>(result, HttpStatus.OK);
  }
  
  private boolean validateField(FieldError field, Class<?>... annotations) {
    List<String> annotationsName = new ArrayList<>();
    for (Class<?> annotation : annotations) {
        annotationsName.add(annotation.getSimpleName());
    }
    return annotationsName.contains(field.getCodes()[field.getCodes().length - 1]);
  }
  
}
  

