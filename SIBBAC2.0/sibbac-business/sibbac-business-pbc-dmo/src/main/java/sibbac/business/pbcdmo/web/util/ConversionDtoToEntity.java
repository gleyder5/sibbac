package sibbac.business.pbcdmo.web.util;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import sibbac.business.pbcdmo.db.model.BlanqueoClientesEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoControlAnalisisEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoControlDocumentacionEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoControlEfectivoEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoEfectivoAcumuladoEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoObservacionEntity;
import sibbac.business.pbcdmo.db.model.PaisesEntity;
import sibbac.business.pbcdmo.db.model.PaisesPK;
import sibbac.business.pbcdmo.web.dto.BlanqueoClientesDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlAnalisisDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlDocumentacionDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlEfectivoAcumuladoDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlEfectivoDTO;
import sibbac.business.pbcdmo.web.dto.PaisesDTO;
import sibbac.business.pbcdmo.web.dto.PaisesPKDTO;

public abstract class ConversionDtoToEntity {

  public static void DtoToEntityEfectivo(BlanqueoControlEfectivoEntity entity, BlanqueoControlEfectivoDTO dto) {

    if (dto.getId() != null) {
      entity.setId(dto.getId());
    }

    if (dto.getDescripcion() != null) {
      entity.setDescripcion(dto.getDescripcion());
    }

    if (dto.getNumTitulosMax() != null && !dto.getNumTitulosMax().isEmpty()) {
      entity.setNumTitulosMax(Integer.valueOf(dto.getNumTitulosMax()));
    }

    if (dto.getImporteEfectivoMax() != null) {
      entity.setImporteEfectivoMax(new BigDecimal(dto.getImporteEfectivoMax().replaceAll(",", ".")));
    }

    if (dto.getPerTitulosMax() != null && !dto.getPerTitulosMax().isEmpty()) {
      entity.setPerTitulosMax(new BigDecimal(dto.getPerTitulosMax().replaceAll(",", ".")));
    }

    if (dto.getPerEfectivoMax() != null) {
      entity.setPerEfectivoMax(new BigDecimal(dto.getPerEfectivoMax().replaceAll(",", ".")));
    }

    if (dto.getFechaAlta() != null) {
      entity.setFechaAlta(new Timestamp(dto.getFechaAlta().getTime()));
    }

    if (dto.getFechaBaja() != null) {
      entity.setFechaBaja(new Timestamp(dto.getFechaBaja().getTime()));
    }

    if (dto.getFechaMod() != null) {
      entity.setFechaMod(new Timestamp(dto.getFechaMod().getTime()));
    }

    if (dto.getUsuarioAlta() != null) {
      entity.setUsuarioAlta(dto.getUsuarioAlta());
    }

    if (dto.getUsuarioMod() != null) {
      entity.setUsuarioMod(dto.getUsuarioMod());
    }

    if (dto.getUsuarioBaja() != null) {
      entity.setUsuarioBaja(dto.getUsuarioBaja());
    }
  }

  public static void DtoToEntityDocument(BlanqueoControlDocumentacionEntity entity,
      BlanqueoControlDocumentacionDTO dto) {
    if (dto.getId() != null) {
      entity.setId(dto.getId());
    }
    if (dto.getDescripcion() != null) {
      entity.setDescripcion(dto.getDescripcion());
    }
    if (dto.getFechaAlta() != null) {
      entity.setFechaAlta(new Timestamp(dto.getFechaAlta().getTime()));
    }
    if (dto.getFechaMod() != null) {
      entity.setFechaMod(new Timestamp(dto.getFechaMod().getTime()));
    }
    if (dto.getFechaBaja() != null) {
      entity.setFechaBaja(new Timestamp(dto.getFechaBaja().getTime()));
    }
    if (dto.getUsuarioAlta() != null) {
      entity.setUsuarioAlta(dto.getUsuarioAlta());
    }
    if (dto.getUsuarioMod() != null) {
      entity.setUsuarioMod(dto.getUsuarioMod());
    }
    if (dto.getUsuarioBaja() != null) {
      entity.setUsuarioBaja(dto.getUsuarioBaja());
    }
  }

  public static void DtoToEntityAnalisis(BlanqueoControlAnalisisEntity entity, BlanqueoControlAnalisisDTO dto) {

    if (dto.getId() != null) {
      entity.setId(dto.getId());
    }
    if (dto.getDescripcion() != null) {
      entity.setDescripcion(dto.getDescripcion());
    }
    if (dto.getFechaAlta() != null) {
      entity.setFechaAlta(new Timestamp(dto.getFechaAlta().getTime()));
    }
    if (dto.getFechaMod() != null) {
      entity.setFechaMod(new Timestamp(dto.getFechaMod().getTime()));
    }
    if (dto.getFechaBaja() != null) {
      entity.setFechaBaja(new Timestamp(dto.getFechaBaja().getTime()));
    }
    if (dto.getUsuarioAlta() != null) {
      entity.setUsuarioAlta(dto.getUsuarioAlta());
    }
    if (dto.getUsuarioMod() != null) {
      entity.setUsuarioMod(dto.getUsuarioMod());
    }
    if (dto.getUsuarioBaja() != null) {
      entity.setUsuarioBaja(dto.getUsuarioBaja());
    }
  }

  public static void DtoToEntityPaises(PaisesEntity entity, PaisesDTO dto) {
    if (dto.getNbpais() != null) {
      entity.setNbpais(dto.getNbpais());
    }
    if (dto.getCdisoalf2() != null) {
      entity.getPrimaryKey().setCdisoalf2(dto.getCdisoalf2());
    }
    if (dto.getCdisoalf3() != null) {
      entity.getPrimaryKey().setCdisoalf3(dto.getCdisoalf3());
    }
    if (dto.getCdisonum() != null) {
      entity.getPrimaryKey().setCdisonum(dto.getCdisonum());
    }
    if (dto.getCdestado() != null) {
      entity.setCdestado(dto.getCdestado());
    }
    if (dto.getCdhacienda() != null) {
      entity.setCdhacienda(dto.getCdhacienda());
    }
    if (dto.getCdcodkgr() != null) {
      entity.setCdcodkgr(dto.getCdcodkgr());
    }
    if (dto.getAuditUser() != null) {
      entity.setAuditUser(dto.getAuditUser());
    }
    if (dto.getAuditDate() != null) {
      entity.setAuditDate(new Timestamp(dto.getAuditDate().getTime()));
    }
    if (dto.getCdcodkgr() != null) {
      entity.setCdcodkgr(dto.getCdcodkgr());
    }
    if (dto.getCdestado() != null) {
      entity.setCdestado(dto.getCdestado());
    }
    if (dto.getCdTipoIdMifid() != null) {
      entity.setCdTipoIdMifid(dto.getCdTipoIdMifid());
    }
    if (dto.getIndicadorDMO() != null) {
      entity.setIndicadorDMO(dto.getIndicadorDMO());
    }
    if (dto.getFecInicioParFisc() != null) {
      entity.setFecInicioParFisc(new Timestamp(dto.getFecInicioParFisc().getTime()));
    }
    if (dto.getFecFinParFisc() != null) {
      entity.setFecFinParFisc(new Timestamp(dto.getFecFinParFisc().getTime()));
    }
  }

  public static void DtoToEntityPaisesPK(PaisesPK entity, PaisesPKDTO dto) {
    if (dto.getCdisoalf2() != null) {
      entity.setCdisoalf2(dto.getCdisoalf2());
    }
    if (dto.getCdisoalf3() != null) {
      entity.setCdisoalf3(dto.getCdisoalf3());
    }
    if (dto.getCdisonum() != null) {
      entity.setCdisonum(dto.getCdisonum());
    }
  }

  public static void DtoToEntityCliente(BlanqueoClientesEntity entity, BlanqueoClientesDTO dto,
      BlanqueoControlEfectivoEntity efectivoEntity, BlanqueoControlDocumentacionEntity documentacionEntity,
      BlanqueoObservacionEntity observacionEntity) {
    if (dto.getId() != null) {
      entity.setId(dto.getId());
    }

    if (efectivoEntity != null) {
      entity.setIdControlEfectivo(efectivoEntity);
    }

    if (dto.getIdControlAnalisis() != null) {
      entity.setIdControlAnalisis(dto.getIdControlAnalisis());
    }

    if (documentacionEntity != null) {
      entity.setIdControlDocumentacion(documentacionEntity);
    }

    if (dto.getIdObservacion() != null) {
      entity.setIdObservacion(observacionEntity);
    }

    if (dto.getFechaRevision() != null) {
      entity.setFechaRevision(new Date(dto.getFechaRevision().getTime()));
    }

    if (dto.getActivo() != null) {
      entity.setActivo(dto.getActivo());
    }

    if (dto.getFechaAlta() != null) {
      entity.setFechaAlta(new Timestamp(dto.getFechaAlta().getTime()));
    }

    if (dto.getFechaBaja() != null) {
      entity.setFechaBaja(new Timestamp(dto.getFechaBaja().getTime()));
    }
    else {
      entity.setFechaBaja(null);
    }

    if (dto.getFechaMod() != null) {
      entity.setFechaMod(new Timestamp(dto.getFechaMod().getTime()));
    }

    if (dto.getUsuarioAlta() != null) {
      entity.setUsuarioAlta(dto.getUsuarioAlta());
    }

    if (dto.getUsuarioMod() != null) {
      entity.setUsuarioMod(dto.getUsuarioMod());
    }

    if (dto.getUsuarioBaja() != null) {
      entity.setUsuarioBaja(dto.getUsuarioBaja());
    }
  }

  /**
   * PBC: Carga de efectivos
   */

  public static void DtoToEntityEfectivoAcumulado(BlanqueoEfectivoAcumuladoEntity entity,
      BlanqueoControlEfectivoAcumuladoDTO dto) {

    if (dto.getIdCliente() != null) {
      entity.setIdCliente(dto.getIdCliente());
    }

    if (dto.getMes() != 0) {
      entity.setMes(dto.getMes());
    }

    if (dto.getAnio() != 0) {
      entity.setAnio(dto.getAnio());
    }

    entity.setTitulosAcumulados(dto.getTitulosAcumulados());

    entity.setImporteAcumulado(dto.getImporteAcumulado());

    if (dto.getFechaUltimaOperacionProc() != null) {
      entity.setFechaUltimaOperacionProc((Date) dto.getFechaUltimaOperacionProc());
    }

    if (dto.getFechaAlta() != null) {
      entity.setFechaAlta(new Timestamp(dto.getFechaAlta().getTime()));
    }

    if (dto.getFechaBaja() != null) {
      entity.setFechaBaja(new Timestamp(dto.getFechaBaja().getTime()));
    }

    if (dto.getFechaMod() != null) {
      entity.setFechaMod(new Timestamp(dto.getFechaMod().getTime()));
    }

    if (dto.getUsuarioAlta() != null) {
      entity.setUsuarioAlta(dto.getUsuarioAlta());
    }

    if (dto.getUsuarioMod() != null) {
      entity.setUsuarioMod(dto.getUsuarioMod());
    }

    if (dto.getUsuarioBaja() != null) {
      entity.setUsuarioBaja(dto.getUsuarioBaja());
    }
  }
}
