package sibbac.business.pbcdmo.web.dto;

/**
 * The Class ClientHistDTO.
 */
public class ClientHistDTO {

  /** The activo. */
  private Integer activo;

  /** The codkgr. */
  private String codkgr;

  /** The desana. */
  private String desana;

  /** The desdoc. */
  private String desdoc;

  /** The desefe. */
  private String desefe;

  /** The desobs. */
  private String desobs;

  /** The idana. */
  private String idana;

  /** The idcliente. */
  private String idcliente;

  /** The iddoc. */
  private String iddoc;

  /** The idefe. */
  private Integer idefe;

  /**
   * Instantiates a new client hist DTO.
   */
  public ClientHistDTO() {
    super();
  }

  /**
   * Instantiates a new client hist DTO.
   *
   * @param activo the activo
   * @param codkgr the codkgr
   * @param desana the desana
   * @param desdoc the desdoc
   * @param desefe the desefe
   * @param desobs the desobs
   * @param idana the idana
   * @param idcliente the idcliente
   * @param iddoc the iddoc
   * @param idefe the idefe
   */
  public ClientHistDTO(Integer activo, String codkgr, String desana, String desdoc, String desefe, String desobs,
      String idana, String idcliente, String iddoc, Integer idefe) {
    super();
    this.activo = activo;
    this.codkgr = codkgr;
    this.desana = desana;
    this.desdoc = desdoc;
    this.desefe = desefe;
    this.desobs = desobs;
    this.idana = idana;
    this.idcliente = idcliente;
    this.iddoc = iddoc;
    this.idefe = idefe;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Integer getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(Integer activo) {
    this.activo = activo;
  }

  /**
   * Gets the codkgr.
   *
   * @return the codkgr
   */
  public String getCodkgr() {
    return codkgr;
  }

  /**
   * Sets the codkgr.
   *
   * @param codkgr the new codkgr
   */
  public void setCodkgr(String codkgr) {
    this.codkgr = codkgr;
  }

  /**
   * Gets the desana.
   *
   * @return the desana
   */
  public String getDesana() {
    return desana;
  }

  /**
   * Sets the desana.
   *
   * @param desana the new desana
   */
  public void setDesana(String desana) {
    this.desana = desana;
  }

  /**
   * Gets the desdoc.
   *
   * @return the desdoc
   */
  public String getDesdoc() {
    return desdoc;
  }

  /**
   * Sets the desdoc.
   *
   * @param desdoc the new desdoc
   */
  public void setDesdoc(String desdoc) {
    this.desdoc = desdoc;
  }

  /**
   * Gets the desefe.
   *
   * @return the desefe
   */
  public String getDesefe() {
    return desefe;
  }

  /**
   * Sets the desefe.
   *
   * @param desefe the new desefe
   */
  public void setDesefe(String desefe) {
    this.desefe = desefe;
  }

  /**
   * Gets the desobs.
   *
   * @return the desobs
   */
  public String getDesobs() {
    return desobs;
  }

  /**
   * Sets the desobs.
   *
   * @param desobs the new desobs
   */
  public void setDesobs(String desobs) {
    this.desobs = desobs;
  }

  /**
   * Gets the idana.
   *
   * @return the idana
   */
  public String getIdana() {
    return idana;
  }

  /**
   * Sets the idana.
   *
   * @param idana the new idana
   */
  public void setIdana(String idana) {
    this.idana = idana;
  }

  /**
   * Gets the idcliente.
   *
   * @return the idcliente
   */
  public String getIdcliente() {
    return idcliente;
  }

  /**
   * Sets the idcliente.
   *
   * @param idcliente the new idcliente
   */
  public void setIdcliente(String idcliente) {
    this.idcliente = idcliente;
  }

  /**
   * Gets the iddoc.
   *
   * @return the iddoc
   */
  public String getIddoc() {
    return iddoc;
  }

  /**
   * Sets the iddoc.
   *
   * @param iddoc the new iddoc
   */
  public void setIddoc(String iddoc) {
    this.iddoc = iddoc;
  }

  /**
   * Gets the idefe.
   *
   * @return the idefe
   */
  public Integer getIdefe() {
    return idefe;
  }

  /**
   * Sets the idefe.
   *
   * @param idefe the new idefe
   */
  public void setIdefe(Integer idefe) {
    this.idefe = idefe;
  }
}
