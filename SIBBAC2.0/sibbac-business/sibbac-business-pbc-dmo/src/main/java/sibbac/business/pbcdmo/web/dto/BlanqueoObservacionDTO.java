package sibbac.business.pbcdmo.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import sibbac.business.pbcdmo.db.model.BlanqueoObservacionEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractControlDTO;

/**
 * The Class TMCT0BlanqueoControlDocumentacionDTO.
 */
public class BlanqueoObservacionDTO extends AbstractControlDTO<BlanqueoObservacionEntity> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1230413757285198066L;

  /** Clave primaria. */
  @NotNull
  private String id;

  /** Clave del control análisis. */
  @NotNull
  private String idControlAnalisis;

  /** Descripción de la observacion. */
  private String descripcion;

  /**
   * Instantiates a new TMCT 0 blanqueo observacion DTO.
   */
  public BlanqueoObservacionDTO() {
    super();
  }

  /**
   * Instantiates a new TMCT 0 blanqueo observacion DTO.
   *
   * @param id the id
   * @param idControlAnalisis the id control analisis
   * @param descripcion the descripcion
   * @param fechaAlta the fecha alta
   * @param fechaMod the fecha mod
   * @param fechaBaja the fecha baja
   * @param usuarioAlta the usuario alta
   * @param usuarioMod the usuario mod
   * @param usuarioBaja the usuario baja
   */
  public BlanqueoObservacionDTO(String id, String idControlAnalisis, String descripcion, Date fechaAlta, Date fechaMod,
      Date fechaBaja, String usuarioAlta, String usuarioMod, String usuarioBaja) {
    super(fechaAlta, fechaMod, fechaBaja, usuarioAlta, usuarioMod, usuarioBaja);
    this.id = id;
    this.idControlAnalisis = idControlAnalisis;
    this.descripcion = descripcion;
  }

  /**
   * Instantiates a new TMCT 0 blanqueo observacion DTO.
   *
   * @param entity the entity
   */
  public BlanqueoObservacionDTO(BlanqueoObservacionEntity entity) {
    this.toDto(entity);
  }

  /**
   * To this dto.
   *
   * @param entity the entity
   */
  @Override
  protected void toThisDto(BlanqueoObservacionEntity entity) {
    this.id = entity.getId();
    this.idControlAnalisis = entity.getIdControlAnalisis();
    this.descripcion = entity.getDescripcion();
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the id control analisis.
   *
   * @return the id control analisis
   */
  public String getIdControlAnalisis() {
    return idControlAnalisis;
  }

  /**
   * Sets the id control analisis.
   *
   * @param idControlAnalisis the new id control analisis
   */
  public void setIdControlAnalisis(String idControlAnalisis) {
    this.idControlAnalisis = idControlAnalisis;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }


  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "BlanqueoObservacionDTO [id=" + id + ", idControlAnalisis=" + idControlAnalisis + ", descripcion="
        + descripcion + ", fechaAlta=" + fechaAlta + ", fechaMod=" + fechaMod + ", fechaBaja=" + fechaBaja
        + ", usuarioAlta=" + usuarioAlta + ", usuarioMod=" + usuarioMod + ", usuarioBaja=" + usuarioBaja + "]";
  }

}
