package sibbac.business.pbcdmo.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DBConstants;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;

/**
 * The Class ControlEfectivosPorVolumenQuery.
 */
@Component("ControlEfectivosPorVolumenQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ControlEfectivosPorVolumenQuery extends AbstractDynamicPageQuery {

  /** The Constant ID. */
  private static final String ID = "ID";

  /** The Constant DESCRIPCION. */
  private static final String DESCRIPCION = "Descripción";

  /** The Constant NUMERO_TITULOS. */
  private static final String NUMERO_TITULOS = "Nº Títulos";

  /** The Constant IMPORTE_EFECTIVO. */
  private static final String IMPORTE_EFECTIVO = "Importe efectivo";

  /** The Constant PORCENTAJE_TITULOS. */
  private static final String PORCENTAJE_TITULOS = "% Títulos";

  /** The Constant PORCENTAJE_EFECTIVO. */
  private static final String PORCENTAJE_EFECTIVO = "% Efectivo";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT ID, DESCRIPCION, NUMERO_TITULOS_MAX, IMPORTE_EFECTIVO_MAX, "
      + "PERCENT_TITULOS_MAX, PERCENT_EFECTIVO_MAX, FECHA_ALTA, FECHA_MOD, FECHA_BAJA, USUARIO_ALTA, "
      + "USUARIO_MOD, USUARIO_BAJA";

  /** The Constant FROM. */
  private static final String FROM = " FROM " + DBConstants.PBCDMO.TMCT0_BLANQUEO_CONTROL_EFECTIVO;

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE FECHA_BAJA IS NULL ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn(ID, "ID"),
      new DynamicColumn(DESCRIPCION, "DESCRIPCION"), new DynamicColumn(NUMERO_TITULOS, "NUMERO_TITULOS_MAX"),
      new DynamicColumn(IMPORTE_EFECTIVO, "IMPORTE_EFECTIVO_MAX"),
      new DynamicColumn(PORCENTAJE_TITULOS, "PERCENT_TITULOS_MAX"),
      new DynamicColumn(PORCENTAJE_EFECTIVO, "PERCENT_EFECTIVO_MAX") };

  /**
   * Make filters.
   *
   * @return the list
   */
  private static List<SimpleDynamicFilter<?>> makeFilters() {
    return new ArrayList<>();
  }

  /**
   * Instantiates a new control efectivos por volumen query.
   */
  public ControlEfectivosPorVolumenQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }

}
