package sibbac.business.pbcdmo.web.util;

public interface Constantes<T> {

  /**
   * Mensajes de Logs
   * @author ipalacio
   *
   */
  public enum LoggingMessages implements Constantes<String> {

  /** */
  DEBUG_MESSAGE_OAP_REST_ENTER("Enter: En {}.{}() con los siguientes parametros = {}"),

  /** */
  DEBUG_MESSAGE_OAP_REST_EXIT("Exit: En {}.{}() return de los siguientes datos = {}"),

  /** */
  DEBUG_MESSAGE_OAP_REST_ERROR("Error: Argumento Ilegal: {} en {}.{}()"),

  /** */
  RUNTIME_EXCEPTION_MESSAGE("[executeAction] Error inesperado al intentar ejecutar la accion {}"),

  /** */
  ID_OBJECT_NOT_FIND("[executeAction] La primarykey del objeto mandado no existe"),

  /** */
  PK_OBJECT_NULL("[executeAction] La primarykey del objeto mandado no está completa."),

  /** */
  PK_NOT_COMPLETE("[executeAction] Compruebe que los datos del pais existen o se han rellenado correctamente."),

  /** */
  DOCUMENT_NOT_EXIST("[executeAction] No existe este documento"),

  /** */
  ANALYSIS_NOT_EXIST("[executeAction] No existe este análisis"),

  /** */
  MULTIPLE_ID_OBJECT_NOT_FIND("[executeAction] Los IDS del objeto mandado no existen"),

  /** */
  DOCUMENTACION_NULL("[executeAction] El ID de la documentacion mandado no existe"),

  /** */
  EFECTIVO_NULL("[executeAction] El ID del efectivo mandado no existe"),

  /** */
  ANALISIS_NULL("[executeAction] El ID del analisis mandado no existe"),

  /** */
  OBSERVACION_NULL("[executeAction] El ID de la observacion mandado no existe"),

  /** */
  OBSERVACION_DUPLICATED("[executeAction] El ID de la observacion mandado ya existe"),

  /** */
  BASIC_VALIDATION_ERROR("[executeAction] Los campos ID y Descripción no pueden estar vacio"),

  /** */
  OBJECT_NULL("El objeto recibido viene nulo");

    private String value;

    private LoggingMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Mensajes de Aplicacion
   * @author ipalacio
   *
   */
  public enum ApplicationMessages implements Constantes<String> {

    UNEXPECTED_MESSAGE("Error inesperado, consulte con administrador"),

    ADD_REQUEST_DATA("Error, los datos obligatorios no han sido rellenados"),

    ERROR_DATE("Error, la Fecha de Inicio no puede ser superior a la Fecha de Fin"),

    ADDED_DATA("Registro insertado correctamente"),

    MODIFIED_DATA("Registro modificado correctamente"),

    DELETED_DATA("Registro(s) borrado(s) correctamente"),

    MULTIPLE_NOT_EXIST("Error, compruebe que las ids {} existan en la base de datos"),

    DUPLICATED_ID("Error, id duplicada"),

    WRONG_CALL("Llamada a servicio equivocado, consulte con el administrador"),

    DATE_CHANGED("La fecha de revisión ha sido modificada en todos los elementos de la tabla.");

    private String value;

    private ApplicationMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return value;
    }
  }

  /**
   * 
   * @author ipalacio
   *
   */
  public enum Activo implements Constantes<Integer> {

    /** */
    SAVE(1),

    /** */
    DELETE(0);

    private Integer value;

    private Activo(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return this.value;
    }
  }

  /**
   * 
   * @author ipalacio
   *
   */
  public enum Session implements Constantes<String> {

    /** */
    USER_SESSION("UserSession"),

    /** */
    USER_CLIENTE("CARGA_CLIENTES_PBC"),

    USER_RECALCULO_CCE("RECALCULO_CCE");

    private String value;

    private Session(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }

  }

  /**
   * Constantes para los estados
   * @author ipalacio
   *
   */
  public enum Estado implements Constantes<String> {

    SAVE("A"),

    DELETE("B");

    private String value;

    private Estado(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public T value();

  /**
   * Constantes para los controles
   */

  public enum Controles implements Constantes<String> {

    CONTROL_DOCUMENTACION_ND("ND"),

    CONTROL_ANALISIS_ON("ON"),

    CONTROL_ANALISIS_01("01"),

    CDNIVEL4("UNB"),

    CDDEPREV("N"),

    CDKGR("9RED");

    private String value;

    private Controles(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum ControlEfectivo implements Constantes<Integer> {
    RESTRICTIVO(2),

    NO_RESTRICTIVO(0);

    private Integer value;

    private ControlEfectivo(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return this.value;
    }
  }

  /**
   * Constantes para formato fecha
   * @author jhurtagr
   *
   */
  public enum Fecha implements Constantes<String> {

    FORMATO("dd/MM/yyyy");

    private String value;

    private Fecha(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum CargaEfectivo implements Constantes<Integer> {
    CDESTADOASIGNADO0(0),

    CDESTADOASIGNADO65(65),

    NUTITLIQ(0),

    EJECUCION_MENSUAL(0),

    EJECUCION_ANUAL(1);

    private Integer value;

    private CargaEfectivo(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return this.value;
    }
  }

  /**
   * Constantes para cargar analisis en caso de observacion a NULL
   * @author jhurtagr
   *
   */
  public enum Analisis implements Constantes<String> {

    DEFECTO("ON");

    private String value;

    private Analisis(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  public enum Nivel4 implements Constantes<String> {

    REGISTROS("UNB");

    private String value;

    private Nivel4(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }
}
