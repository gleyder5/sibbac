package sibbac.business.pbcdmo.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cli;

/**
 * The Interface TMCT0CliDAO.
 */
@Repository
public interface TMCT0CliDAO extends JpaRepository<Tmct0cli, Long> {

  @Query(value = "SELECT * FROM TMCT0CLI WHERE CDBROCLI = :id", nativeQuery = true)
  List<Tmct0cli> findBycdbrocli(@Param("id") String id);
}