package sibbac.business.pbcdmo.db.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.pbcdmo.db.model.BlanqueoEfectivoAcumuladoEntity;

@Repository
public interface BlanqueoEfectivoAcumuladoDAO extends JpaRepository<BlanqueoEfectivoAcumuladoEntity, BigInteger> {

  /**
   * Caso año natural sin operar
   * @param anio
   * @return
   */
  
  @Query(value = "SELECT BEA.ID_CLIENTE,"
      + " MAX(BEA.IMPORTE_ACUMULADO) AS max_importe_operacion," 
      + " BC.ID_CONTROL_EFECTIVO,"
      + " BCE.IMPORTE_EFECTIVO_MAX" 
      + " FROM TMCT0_BLANQUEO_EFECTIVO_ACUMULADO AS BEA" 
      + " INNER JOIN TMCT0_BLANQUEO_CLIENTE AS BC"
      + " ON BEA.ID_CLIENTE = BC.ID_CLIENTE"
      + " INNER JOIN TMCT0_BLANQUEO_CONTROL_EFECTIVO AS BCE" 
      + " ON BCE.ID = BC.ID_CONTROL_EFECTIVO"
      + " AND BCE.FECHA_BAJA IS NULL"
      + " WHERE BEA.ANIO = :anio"
      + " AND BEA.FECHA_BAJA IS NULL" 
      + " AND BC.ID_CLIENTE = :idCliente"
      + " GROUP BY BEA.ID_CLIENTE, BC.ID_CONTROL_EFECTIVO, BCE.IMPORTE_EFECTIVO_MAX", nativeQuery = true) 
  List<Object[]> findCaseWithoutOperation(@Param("idCliente") String idCliente, @Param("anio") int anio); 

  /**
   * Update año natural sin operar
   * @param controlEfectivo
   * @param controlDocumentacion
   * @param controlAnalisis
   * @param fechaMod
   * @param usuarioMod
   * @param idCliente
   */
  @Transactional
  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE TMCT0_BLANQUEO_CLIENTE"
      + "SET ID_CONTROL_EFECTIVO = :controlEfectivo," 
      + "ID_CONTROL_DOCUMENTACION = :controlDocumentacion," 
      + "ID_CONTROL_ANALISIS = :controlAnalisis,"
      + "FECHA_MOD = :fechaMod,"
      + "USUARIO_MOD = :usuarioMod"
      + "WHERE ID_CLIENTE = :idCliente"
      + "AND FECHA_BAJA IS NOT NULL", nativeQuery = true)  
  void updateWithoutOperationList(@Param("controlEfectivo") int controlEfectivo,
      @Param("controlDocumentacion") String controlDocumentacion, @Param("controlAnalisis") String controlAnalisis,
      @Param("fechaMod") Timestamp fechaMod, @Param("usuarioMod") String usuarioMod,
      @Param("idCliente") String idCliente);

 
  /**
   * 
   * @param importeEfectivoMax
   * @return
   */
  @Query(value =  "SELECT"
      + "   ID,"
      + "   IMPORTE_EFECTIVO_MAX"
      + " FROM"
      + "   TMCT0_BLANQUEO_CONTROL_EFECTIVO"
      + " WHERE"
      + "   IMPORTE_EFECTIVO_MAX > :importeEfectivoMax"
      + "   AND FECHA_BAJA IS NOT NULL"
      + " ORDER BY"
      + "   IMPORTE_EFECTIVO_MAX ASC FETCH FIRST ROW ONLY", nativeQuery = true)
  List<Object[]> findAdjustmentByVolumeEffectiveControl(@Param("importeEfectivoMax") BigDecimal importeEfectivoMax);

  /**
   * 
   * @param idControlEfectivo
   * @param fechaMod
   * @param usuarioMod
   * @param idCliente
   */
  @Transactional
  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE TMCT0_BLANQUEO_CLIENTE"
      + " SET ID_CONTROL_EFECTIVO = :idControlEfectivo,"
      + " FECHA_MOD = :fechaMod,"
      + " USUARIO_MOD = :usuarioMod "
      + " WHERE ID_CLIENTE = :idCliente"
      + " AND FECHA_BAJA IS NOT NULL", nativeQuery = true)
  void updateAdjustmentByVolume(@Param("idControlEfectivo") String idControlEfectivo, @Param("fechaMod") Timestamp fechaMod, @Param("usuarioMod") String usuarioMod,
      @Param("idCliente") String idCliente);

  /**
   * Caso cliente research
   * @param idControlAnalisisSet
   * @param fechaMod
   * @param usuarioMod
   * @param idControlAnalisis
   * @param cdNivel4
   */
  @Transactional
  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE TMCT0_BLANQUEO_CLIENTE"
      + " SET"
      + "    ID_CONTROL_ANALISIS = :idControlAnalisisSet,"
      + "    FECHA_MOD = :fechaMod,"
      + "    USUARIO_MOD = :usuarioMod"
      + " WHERE"
      + "   ID_CONTROL_ANALISIS != :idControlAnalisis"
      + "   AND ID_CLIENTE IN"
      + "  ( SELECT bc.ID_CLIENTE"
      + "   FROM TMCT0_BLANQUEO_CLIENTE AS bc"
      + "   INNER JOIN tmct0ali AS ali ON bc.ID_CLIENTE= ali.cdbrocli"
      + "   AND ali.fhfinal>=to_char(CURRENT date,'yyyymmdd')"
      + "   INNER JOIN tmct0est AS est ON ali.cdaliass = est.cdaliass"
      + "    AND est.fhfinal>=to_char(CURRENT date,'yyyymmdd')"
      + "   WHERE (est.STLEV0LM,"
      + "     est.STLEV1LM,"
      + "     est.STLEV2LM,"
      + "     est.STLEV3LM,"
      + "     est.STLEV4LM) IN"
      + "   (SELECT nuorden0,"
      + "      nUorden1,"
      + "      nuorden2,"
      + "      nuorden3,"
      + "      nuorden4"
      + "   FROM bsnbpsql.tmct0oni"
      + "   WHERE cdnivel4 =:cdNivel4 ) )", nativeQuery = true)
  void updateResearchClient(@Param("idControlAnalisisSet") String idControlAnalisisSet, @Param("fechaMod") Timestamp fechaMod, @Param("usuarioMod") String usuarioMod,
      @Param("idControlAnalisis") String idControlAnalisis, @Param("cdNivel4") String cdNivel4);

  // Carga de efectivos
  @Query(value = "SELECT   year(alc.feejeliq) AS FHANOPRC, month(alc.feejeliq) AS FHMESPRC, cli.nuclient, ali.cdbrocli, sum(dct.imtitulos) AS nucaneje, sum(dct.imefectivo) AS imefecti, max(alc.FEEJELIQ) AS FHMAX "
      + "FROM  TMCT0ALC as alc "
      + "INNER JOIN TMCT0BOK as bok ON bok.nbooking = alc.nbooking and bok.cdestadoasig > :cdestadoasig0 "
      + "INNER JOIN tmct0est as est ON est.cdaliass = bok.cdalias and est.fhfinal >= to_char(current date, 'yyyymmdd') "
      + "INNER JOIN tmct0desglosecamara as dcm ON dcm.nuorden = alc.nuorden and dcm.nbooking = alc.nbooking and dcm.nucnfclt = alc.nucnfclt and dcm.nucnfliq = alc.nucnfliq and dcm.cdestadoasig >= :cdestadoasig65 "
      + "INNER JOIN tmct0desgloseclitit as dct ON dct.iddesglosecamara = dcm.iddesglosecamara "
      + "INNER JOIN tmct0ali AS ali on ali.cdaliass = bok.cdalias and ali.fhfinal >= to_char(current date, 'yyyymmdd') "
      + "INNER JOIN tmct0cli AS cli ON ali.cdbrocli = cli.cdbrocli AND cli.CDDEPREV = :CDDEPREV AND cli.CDKGR <> :CDKGR "
      + "WHERE alc.cdestadoasig >= :cdestadoasig65 and alc.nutitliq > :nutitliq AND alc.feejeliq >= :fechaDesde AND alc.FEEJELIQ <= :fechaHasta "
      + "AND (est.STLEV0LM,est.STLEV1LM,  est.STLEV2LM,est.STLEV3LM,est.STLEV4LM) IN "
      + "(select nuorden0,nuorden1, nuorden2, nuorden3, nuorden4 " + "FROM tmct0oni "
      + "WHERE cdnivel0 IN ('RVN', 'EXT')) "
      + "GROUP BY ali.cdbrocli,cli.nuclient,year(alc.feejeliq),month(alc.feejeliq) "
      + "ORDER BY cli.nuclient,ali.cdbrocli", nativeQuery = true)

  List<Object[]> findEffectiveClientsOperations(@Param("cdestadoasig0") int cdestadoasig0,
      @Param("cdestadoasig65") int cdestadoasig65, @Param("CDDEPREV") String CDDEPREV, @Param("CDKGR") String CDKGR,
      @Param("nutitliq") int nutitliq, @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);

  // Select efectivo acumulado
  @Query(value = "SELECT ID " + "FROM TMCT0_BLANQUEO_EFECTIVO_ACUMULADO "
      + "WHERE ID_CLIENTE = :idCliente AND MES = :mes AND ANIO = :anio", nativeQuery = true)
  List<Object[]> findEffectiveAcumulated(@Param("idCliente") String idCliente, @Param("mes") int mes,
      @Param("anio") int anio);

  @Transactional
  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE TMCT0_BLANQUEO_EFECTIVO_ACUMULADO " + "SET FECHA_BAJA = :fechaBaja, "
      + "USUARIO_BAJA = :usuarioBaja "
      + "WHERE MES = :mes AND ANIO = :anio AND ID_CLIENTE = :idCliente", nativeQuery = true)
  void updateExistingClientsOperations(@Param("fechaBaja") Timestamp fechaBaja,
      @Param("usuarioBaja") String usuarioBaja, @Param("mes") int mes, @Param("anio") int anio,
      @Param("idCliente") String idCliente);
 
}
