
package sibbac.business.pbcdmo.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.pbcdmo.db.model.common.BlanqueoControlEntity;
import sibbac.database.DBConstants;

/**
 * The Class TMCT0BlanqueoControlDocumentacionEntity.
 */
@Entity
@Table(name = DBConstants.PBCDMO.TMCT0_BLANQUEO_OBSERVACION)
public class BlanqueoObservacionEntity extends BlanqueoControlEntity {

  /** The id. */
  @Id
  @Column(name = "ID", length = 3)
  private String id;

  /** The id control analisis. */
  @Column(name = "ID_CONTROL_ANALISIS", length = 10, nullable = false)
  private String idControlAnalisis;

  /** The descripcion. */
  @Column(name = "DESCRIPCION", length = 256)
  private String descripcion;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the id control analisis.
   *
   * @return the id control analisis
   */
  public String getIdControlAnalisis() {
    return idControlAnalisis;
  }

  /**
   * Sets the id control analisis.
   *
   * @param idControlAnalisis the new id control analisis
   */
  public void setIdControlAnalisis(String idControlAnalisis) {
    this.idControlAnalisis = idControlAnalisis;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "BlanqueoObservacionEntity [id=" + id + ", idControlAnalisis=" + idControlAnalisis + ", descripcion="
        + descripcion + ", fechaAlta=" + fechaAlta + ", fechaMod=" + fechaMod + ", fechaBaja=" + fechaBaja
        + ", usuarioAlta=" + usuarioAlta + ", usuarioMod=" + usuarioMod + ", usuarioBaja=" + usuarioBaja + "]";
  }

}
