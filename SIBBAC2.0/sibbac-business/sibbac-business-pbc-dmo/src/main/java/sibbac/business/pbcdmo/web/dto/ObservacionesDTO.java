package sibbac.business.pbcdmo.web.dto;

import java.sql.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class ObservacionesDTO.
 */
public class ObservacionesDTO {

  /** The id cliente. */
  private String idObservacionCliente;

  /** The id cliente. */
  private String idCliente;

  /** The fecha alta. */
  private Date fechaAlta;

  /** The id control analisis. */
  private String idControlAnalisis;

  /** The des control analisis. */
  private String desControlAnalisis;

  /** The id observacion. */
  private String idObservacion;

  /** The des observacion. */
  private String desObservacion;

  /**
   * Instantiates a new observaciones DTO.
   *
   * @param idObservacionCliente the id observacion cliente
   * @param idCliente the id cliente
   * @param fechaAlta the fecha alta
   * @param idControlAnalisis the id control analisis
   * @param desControlAnalisis the des control analisis
   * @param idObservacion the id observacion
   * @param desObservacion the des observacion
   */
  public ObservacionesDTO(String idObservacionCliente, String idCliente, Date fechaDeAlta, String idControlAnalisis,
      String desControlAnalisis, String idObservacion, String desObservacion) {
    super();
    this.idObservacionCliente = idObservacionCliente;
    this.idCliente = idCliente;
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);
    this.idControlAnalisis = idControlAnalisis;
    this.desControlAnalisis = desControlAnalisis;
    this.idObservacion = idObservacion;
    this.desObservacion = desObservacion;
  }

  /**
   * Instantiates a new observaciones DTO.
   */
  public ObservacionesDTO() {
    super();
  }

  /**
   * Gets the id observacion cliente.
   *
   * @return the id observacion cliente
   */
  public String getIdObservacionCliente() {
    return idObservacionCliente;
  }

  /**
   * Sets the id observacion cliente.
   *
   * @param idObservacionCliente the new id observacion cliente
   */
  public void setIdObservacionCliente(String idObservacionCliente) {
    this.idObservacionCliente = idObservacionCliente;
  }

  /**
   * Gets the id cliente.
   *
   * @return the id cliente
   */
  public String getIdCliente() {
    return idCliente;
  }

  /**
   * Sets the id cliente.
   *
   * @param idCliente the new id cliente
   */
  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  /**
   * Gets the fecha alta.
   *
   * @return the fecha alta
   */
  public Date getFechaAlta() {
    return fechaAlta != null ? (Date) fechaAlta.clone() : null;
  }

  /**
   * Sets the fecha alta.
   *
   * @param fechaAlta the new fecha alta
   */
  public void setFechaAlta(Date fechaAlta) {
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);    
  }

  /**
   * Gets the id control analisis.
   *
   * @return the id control analisis
   */
  public String getIdControlAnalisis() {
    return idControlAnalisis;
  }

  /**
   * Sets the id control analisis.
   *
   * @param idControlAnalisis the new id control analisis
   */
  public void setIdControlAnalisis(String idControlAnalisis) {
    this.idControlAnalisis = idControlAnalisis;
  }

  /**
   * Gets the des control analisis.
   *
   * @return the des control analisis
   */
  public String getDesControlAnalisis() {
    return desControlAnalisis;
  }

  /**
   * Sets the des control analisis.
   *
   * @param desControlAnalisis the new des control analisis
   */
  public void setDesControlAnalisis(String desControlAnalisis) {
    this.desControlAnalisis = desControlAnalisis;
  }

  /**
   * Gets the id observacion.
   *
   * @return the id observacion
   */
  public String getIdObservacion() {
    return idObservacion;
  }

  /**
   * Sets the id observacion.
   *
   * @param idObservacion the new id observacion
   */
  public void setIdObservacion(String idObservacion) {
    this.idObservacion = idObservacion;
  }

  /**
   * Gets the des observacion.
   *
   * @return the des observacion
   */
  public String getDesObservacion() {
    return desObservacion;
  }

  /**
   * Sets the des observacion.
   *
   * @param desObservacion the new des observacion
   */
  public void setDesObservacion(String desObservacion) {
    this.desObservacion = desObservacion;
  }

}
