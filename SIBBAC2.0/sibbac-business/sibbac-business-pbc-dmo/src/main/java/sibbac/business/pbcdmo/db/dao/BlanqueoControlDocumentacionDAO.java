package sibbac.business.pbcdmo.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.pbcdmo.db.model.BlanqueoControlDocumentacionEntity;

/**
 * The Interface TMCT0BlanqueoControlDocumentacionDAO.
 */
@Repository
public interface BlanqueoControlDocumentacionDAO extends JpaRepository<BlanqueoControlDocumentacionEntity, String> {

  @Query(value = "SELECT cd.id, cd.descripcion "
      + "FROM TMCT0_BLANQUEO_CONTROL_DOCUMENTACION cd WHERE cd.fecha_Baja is NULL ", nativeQuery = true)
  List<Object[]> findListDocumentos();

}