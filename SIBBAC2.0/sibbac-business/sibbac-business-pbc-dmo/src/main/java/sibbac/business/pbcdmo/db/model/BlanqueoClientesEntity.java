
package sibbac.business.pbcdmo.db.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.database.DBConstants;

/**
 * The Class BlanqueoClientesEntity.
 */
@Entity
@Audited
@Table(name = DBConstants.PBCDMO.TMCT0_BLANQUEO_CLIENTE)
public class BlanqueoClientesEntity {

  /** The id. */
  @Id
  @Column(name = "ID_CLIENTE", length = 20)
  private String id;

  /** The id control efectivo. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CONTROL_EFECTIVO", nullable = false)
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  private BlanqueoControlEfectivoEntity idControlEfectivo;

  /** The id control documentacion. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CONTROL_DOCUMENTACION", nullable = false)
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  private BlanqueoControlDocumentacionEntity idControlDocumentacion;

  @Column(name = "ID_CONTROL_ANALISIS", length = 10)
  private String idControlAnalisis;

  /** The id observacion. */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_OBSERVACION")
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  private BlanqueoObservacionEntity idObservacion;

  /** The fecha revision. */
  @Column(name = "FECHA_REVISION", length = 10)
  private Date fechaRevision;

  /** The activo */
  @Column(name = "ACTIVO", nullable = false, length = 4)
  protected Integer activo;

  /** The fechaalta. */
  @Column(name = "FECHA_ALTA", nullable = false)
  protected Timestamp fechaAlta;

  /** The fechamod. */
  @Column(name = "FECHA_MOD", nullable = true)

  protected Timestamp fechaMod;
  /** The fechabaja. */
  @Column(name = "FECHA_BAJA", nullable = true)

  protected Timestamp fechaBaja;
  /** The usuarioalta. */
  @Column(name = "USUARIO_ALTA", nullable = false, length = 256)

  protected String usuarioAlta;
  /** The usuariomod. */
  @Column(name = "USUARIO_MOD", nullable = true, length = 256)

  protected String usuarioMod;
  /** The usuariobaja. */
  @Column(name = "USUARIO_BAJA", nullable = true, length = 256)

  protected String usuarioBaja;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the id control efectivo.
   *
   * @return the id control efectivo
   */
  public BlanqueoControlEfectivoEntity getIdControlEfectivo() {
    return idControlEfectivo;
  }

  /**
   * Sets the id control efectivo.
   *
   * @param IdControlEfectivo the new id control efectivo
   */
  public void setIdControlEfectivo(BlanqueoControlEfectivoEntity idControlEfectivo) {
    this.idControlEfectivo = idControlEfectivo;
  }

  /**
   * Gets the id control documentacion.
   *
   * @return the id control documentacion
   */
  public BlanqueoControlDocumentacionEntity getIdControlDocumentacion() {
    return idControlDocumentacion;
  }

  /**
   * Sets the id control documentacion.
   *
   * @param IdControlDocumentacion the new id control documentacion
   */
  public void setIdControlDocumentacion(BlanqueoControlDocumentacionEntity idControlDocumentacion) {
    this.idControlDocumentacion = idControlDocumentacion;
  }

  /**
   * Gets the id control analisis.
   *
   * @return the id control analisis
   */
  public String getIdControlAnalisis() {
    return idControlAnalisis;
  }

  /**
   * Sets the id control analisis.
   *
   * @param IdControlAnalisis the new id control analisis
   */
  public void setIdControlAnalisis(String idControlAnalisis) {
    this.idControlAnalisis = idControlAnalisis;
  }

  /**
   * Gets the id observacion.
   *
   * @return the id observacion
   */
  public BlanqueoObservacionEntity getIdObservacion() {
    return idObservacion;
  }

  /**
   * Sets the id observacion.
   *
   * @param idObservacion the new id observacion
   */
  public void setIdObservacion(BlanqueoObservacionEntity idObservacion) {
    this.idObservacion = idObservacion;
  }

  /**
   * Gets the fecha revision.
   *
   * @return the fecha revision
   */
  public Date getFechaRevision() {
    return fechaRevision != null ? (Date) fechaRevision.clone() : null;
  }

  /**
   * Sets the fecha revision.
   *
   * @param fechaRevision the new fecha revision
   */
  public void setFechaRevision(Date fechaRevision) {
    this.fechaRevision = fechaRevision != null ? new Date(fechaRevision.getTime()) : null;
  }

  /**
   * Gets the activo.
   *
   * @return the activo
   */
  public Integer getActivo() {
    return activo;
  }

  /**
   * Sets the activo.
   *
   * @param activo the new activo
   */
  public void setActivo(Integer activo) {
    this.activo = activo;
  }

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Timestamp getFechaAlta() {
    return fechaAlta != null ? (Timestamp) fechaAlta.clone() : null;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (Timestamp) fechaAlta.clone();
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Timestamp getFechaMod() {
    return fechaMod != null ? (Timestamp) fechaMod.clone() : null;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (Timestamp) fechaMod.clone();
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Timestamp getFechaBaja() {
    return fechaBaja != null ? (Timestamp) fechaBaja.clone() : null;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechaBaja(Timestamp fechaBaja) {
    if (null != fechaBaja) {
      this.fechaBaja = (Timestamp) fechaBaja.clone();
    }
    else {
      this.fechaBaja = null;
    }
  }

  /**
   * Gets the usuarioalta.
   *
   * @return the usuarioalta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * Sets the usuarioalta.
   *
   * @param usuarioalta the new usuarioalta
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * Gets the usuariomod.
   *
   * @return the usuariomod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * Sets the usuariomod.
   *
   * @param usuariomod the new usuariomod
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * Gets the usuariobaja.
   *
   * @return the usuariobaja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * Sets the usuariobaja.
   *
   * @param usuariobaja the new usuariobaja
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */

}
