
package sibbac.business.pbcdmo.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.pbcdmo.db.model.common.BlanqueoControlEntity;
import sibbac.database.DBConstants;

/**
 * The Class TMCT0BlanqueoControlDocumentacionEntity.
 */
@Entity
@Table(name = DBConstants.PBCDMO.TMCT0_BLANQUEO_CONTROL_DOCUMENTACION)
public class BlanqueoControlDocumentacionEntity extends BlanqueoControlEntity {

  /** Código de control de documentación. */
  @Id
  @Column(name = "ID", length = 10)
  private String id;

  /** Descripción del control de documentación. */
  @Column(name = "DESCRIPCION", length = 256, nullable = false)
  private String descripcion;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "TMCT0BlanqueoControlDocumentacionEntity [id=" + id + ", descripcion=" + descripcion + ", fechaAlta="
        + fechaAlta + ", fechaMod=" + fechaMod + ", fechaBaja=" + fechaBaja + ", usuarioAlta=" + usuarioAlta
        + ", usuarioMod=" + usuarioMod + ", usuarioBaja=" + usuarioBaja + "]";
  }

}
