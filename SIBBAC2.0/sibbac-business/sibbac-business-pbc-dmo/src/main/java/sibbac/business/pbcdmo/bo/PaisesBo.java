package sibbac.business.pbcdmo.bo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.pbcdmo.db.dao.PaisesDAO;
import sibbac.business.pbcdmo.db.model.PaisesEntity;
import sibbac.business.pbcdmo.db.model.PaisesPK;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.dto.PaisesDTO;
import sibbac.business.pbcdmo.web.dto.PaisesPKDTO;
import sibbac.business.pbcdmo.web.util.Constantes;
import sibbac.business.pbcdmo.web.util.ConversionDtoToEntity;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class TMCT0PaisesBo.
 */
@Service("paisesBo")
public class PaisesBo extends AbstractBo<PaisesEntity, PaisesPK, PaisesDAO> implements BaseBo<PaisesDTO> {

  /** The session. */
  @Autowired
  private HttpSession session;

  @Override
  public CallableResultDTO actionSave(PaisesDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();
    try {
      PaisesPK pk = new PaisesPK();
      pk.setCdisoalf2(solicitud.getCdisoalf2());
      pk.setCdisoalf3(solicitud.getCdisoalf3());
      pk.setCdisonum(solicitud.getCdisonum());
      PaisesEntity entitydb = dao.findOne(pk);
      if (entitydb != null) {
        result.addErrorMessage(Constantes.ApplicationMessages.DUPLICATED_ID.value());
        return result;
      }
      if (solicitud != null) {
        // Comprobamos que la fecha de fin sea superior a la fecha de inicio, o
        // que puedan venir nulas
        if (solicitud.getFecInicioParFisc() == null || solicitud.getFecFinParFisc() == null
            || !solicitud.getFecFinParFisc().before(solicitud.getFecInicioParFisc())) {
          // Comprobamos que recibimos valores obligatorios
          if (!solicitud.getCdisoalf2().isEmpty() && !solicitud.getCdisoalf3().isEmpty()
              && !solicitud.getCdisonum().isEmpty()) {

            PaisesEntity entity = new PaisesEntity();
            entity.setPrimaryKey(new PaisesPK());
            solicitud.setCdestado(Constantes.Estado.SAVE.value());
            solicitud.setAuditDate(new Timestamp(System.currentTimeMillis()));
            solicitud.setAuditUser(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
            ConversionDtoToEntity.DtoToEntityPaises(entity, solicitud);

            dao.save(entity);

            result.addMessage(Constantes.ApplicationMessages.ADDED_DATA.value());
          }
          else {
            throw new SIBBACBusinessException();
          }
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {

      LOG.error("[executeAction] Los campos Nombre de País, ISO Alfanumérico, ISO Alfanumérico(2), ISO Alfanumérico(3)"
          + "no pueden estar vacio. O Fecha Inicio Paraíso es inferior a Fecha Fin Paraiso", e);
      if (solicitud.getFecFinParFisc().before(solicitud.getFecInicioParFisc())) {
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_DATE.value());
      }
      else {
        result.addErrorMessage(Constantes.ApplicationMessages.ADD_REQUEST_DATA.value());
      }
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }

    return result;
  }

  @Override
  public CallableResultDTO actionUpdate(PaisesDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    PaisesEntity entity = new PaisesEntity();
    try {
      if (solicitud != null) {
        if (null != solicitud.getIndicadorDMO()) {
          if (solicitud.getIndicadorDMO().equalsIgnoreCase("false")) {
            solicitud.setIndicadorDMO("0");
          }
          else if (solicitud.getIndicadorDMO().equalsIgnoreCase("true")) {
            solicitud.setIndicadorDMO("1");
          }
        }
        // Comprobamos que la fecha de fin sea superior a la fecha de inicio o
        // que vengan nulas
        if (solicitud.getFecInicioParFisc() == null || solicitud.getFecFinParFisc() == null
            || !solicitud.getFecFinParFisc().before(solicitud.getFecInicioParFisc())) {
          if (!solicitud.getCdisoalf2().isEmpty() && !solicitud.getCdisoalf3().isEmpty()
              && !solicitud.getCdisonum().isEmpty()) {
            PaisesPK pk = new PaisesPK();
            pk.setCdisoalf2(solicitud.getCdisoalf2());
            pk.setCdisoalf3(solicitud.getCdisoalf3());
            pk.setCdisonum(solicitud.getCdisonum());
            entity.setPrimaryKey(pk);

            final PaisesEntity entityFind = dao.findOne(entity.getPrimaryKey());

            if (entityFind != null) {
              entityFind.setAuditDate(new Timestamp(System.currentTimeMillis()));
              entityFind.setAuditUser(StringUtils.substring(
                  ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
              ConversionDtoToEntity.DtoToEntityPaises(entityFind, solicitud);
              dao.save(entityFind);

              result.addMessage(Constantes.ApplicationMessages.MODIFIED_DATA.value());
            }
            else {
              throw new SIBBACBusinessException(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
            }
          }
          else {
            throw new SIBBACBusinessException(Constantes.LoggingMessages.PK_OBJECT_NULL.value());
          }
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
      if (solicitud.getFecFinParFisc().before(solicitud.getFecInicioParFisc())) {
        result.addErrorMessage(Constantes.ApplicationMessages.ERROR_DATE.value());
      }
      else {
        result.addErrorMessage(Constantes.LoggingMessages.PK_NOT_COMPLETE.value());
      }
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  public CallableResultDTO actionDelete(List<String> solicitudList) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  /**
   * Action delete paraiso.
   *
   * @param solicitudList the solicitud list
   * @return the callable result DTO
   */
  public CallableResultDTO actionDeleteParaiso(List<PaisesPKDTO> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      for (PaisesPKDTO solicitud : solicitudList) {
        if (solicitud != null) {

          PaisesPK pk = new PaisesPK();
          ConversionDtoToEntity.DtoToEntityPaisesPK(pk, solicitud);
          final PaisesEntity entityFind = dao.findOne(pk);

          if (entityFind != null) {
            entityFind.setAuditDate(new Timestamp(System.currentTimeMillis()));
            entityFind.setAuditUser(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
            entityFind.setCdestado(Constantes.Estado.DELETE.value());
            dao.save(entityFind);
          }
          else {
            err.add(String.valueOf(solicitud));
          }
        }
      }
      if (!err.isEmpty() || solicitudList.isEmpty()) {
        throw new SIBBACBusinessException();
      }
      else {
        result.addMessage(Constantes.ApplicationMessages.DELETED_DATA.value());
      }

    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(Constantes.LoggingMessages.MULTIPLE_ID_OBJECT_NOT_FIND.value());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  @Override
  public CallableResultDTO actionFindById(PaisesDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO update(ObservacionClienteDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

}
