package sibbac.business.pbcdmo.tasks;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.pbcdmo.bo.BlanqueoControlEfectivoAcumuladoBo;
import sibbac.business.pbcdmo.web.util.Constantes.CargaEfectivo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

//0 0 7 1 * ? Fire at 7:00am on the 1st day of every month
//* * * ? 1 ? Fire the first day of the month
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_PBC.NAME, name = Task.GROUP_PBC.JOB_EFECTIVO_MENSUAL, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 7 1 * ?")
public class TaskEfectivoClientesPBCMensual extends WrapperTaskConcurrencyPrevent{

  @Autowired
  private BlanqueoControlEfectivoAcumuladoBo efectivoAcumuladoBo;
  
  protected static final Logger LOG = LoggerFactory.getLogger(TaskRecalculoCce.class);
  
  @Override
  public void executeTask() throws Exception {
    LOG.trace("[TaskPbcCargaEfectivoMensual :: executeTask] inicio");    
    try
    {
      efectivoAcumuladoBo.effectiveCustomersLoad(CargaEfectivo.EJECUCION_MENSUAL.value());
    }
    catch(RuntimeException e)
    {
      LOG.error("[TaskPbcCargaEfectivoMensual :: executeTask] Error en la ejecución de la operativa Carga Efectivo mensual.", e);
    }
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_PCB_EFECTIVO_MENSUAL;
  }

}
