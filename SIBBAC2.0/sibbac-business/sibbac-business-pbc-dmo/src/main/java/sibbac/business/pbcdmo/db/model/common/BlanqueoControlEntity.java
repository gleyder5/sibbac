package sibbac.business.pbcdmo.db.model.common;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BlanqueoControlEntity {

  /** The fechaalta. */
  @Column(name = "FECHA_ALTA", nullable = false)
  protected Timestamp fechaAlta;

  /** The fechamod. */
  @Column(name = "FECHA_MOD", nullable = true)
  protected Timestamp fechaMod;

  /** The fechabaja. */
  @Column(name = "FECHA_BAJA", nullable = true)
  protected Timestamp fechaBaja;

  /** The usuarioalta. */
  @Column(name = "USUARIO_ALTA", nullable = false, length = 256)
  protected String usuarioAlta;

  /** The usuariomod. */
  @Column(name = "USUARIO_MOD", nullable = true, length = 256)
  protected String usuarioMod;

  /** The usuariobaja. */
  @Column(name = "USUARIO_BAJA", nullable = true, length = 256)
  protected String usuarioBaja;

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Timestamp getFechaAlta() {
    return fechaAlta != null ? (Timestamp) fechaAlta.clone() : null;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (Timestamp) fechaAlta.clone();
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Timestamp getFechaMod() {
    return fechaMod != null ? (Timestamp) fechaMod.clone() : null;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (Timestamp) fechaMod.clone();
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Timestamp getFechaBaja() {
    return fechaBaja != null ? (Timestamp) fechaBaja.clone() : null;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaBaja = (Timestamp) fechaBaja.clone();
  }

  /**
   * Gets the usuarioalta.
   *
   * @return the usuarioalta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * Sets the usuarioalta.
   *
   * @param usuarioalta the new usuarioalta
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * Gets the usuariomod.
   *
   * @return the usuariomod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * Sets the usuariomod.
   *
   * @param usuariomod the new usuariomod
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * Gets the usuariobaja.
   *
   * @return the usuariobaja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * Sets the usuariobaja.
   *
   * @param usuariobaja the new usuariobaja
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

}
