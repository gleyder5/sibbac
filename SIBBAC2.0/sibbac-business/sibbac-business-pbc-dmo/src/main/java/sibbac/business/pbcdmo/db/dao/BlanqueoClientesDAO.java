package sibbac.business.pbcdmo.db.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.pbcdmo.db.model.BlanqueoClientesEntity;

/**
 * The Interface ClientesDAO.
 */
@Repository
public interface BlanqueoClientesDAO extends JpaRepository<BlanqueoClientesEntity, String> {

  @Query(value = "SELECT blacli.ID_CLIENTE as cliente, cli.CDKGR as kgr, efe.ID as idControlEfectivo, efe.DESCRIPCION as desControlEfectivo, "
      + "doc.ID as idControlDocumentacion, doc.DESCRIPCION as desControlDocumentacion, ana.ID as idControlAnalisis, "
      + "ana.DESCRIPCION as desControlAnalisis, bsnbpsql.TMCT0_BLANQUEO_OBSERVACION.DESCRIPCION as observacion, blacli.FECHA_ALTA, blacli.FECHA_MOD "
      + "FROM bsnbpsql.TMCT0_BLANQUEO_CLIENTE_HIST blacli "
      + "INNER JOIN bsnbpsql.TMCT0CLI cli on cli.CDBROCLI = blacli.ID_CLIENTE "
      + "LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_EFECTIVO efe on efe.ID = blacli.ID_CONTROL_EFECTIVO "
      + "LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_DOCUMENTACION doc on doc.ID = blacli.ID_CONTROL_DOCUMENTACION "
      + "LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_ANALISIS ana on ana.ID = blacli.ID_CONTROL_ANALISIS "
      + "LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_OBSERVACION on blacli.ID_OBSERVACION = bsnbpsql.TMCT0_BLANQUEO_OBSERVACION.ID "
      + "WHERE blacli.FECHA_BAJA is null AND blacli.ID_CLIENTE = ?1 order by blacli.revision", nativeQuery = true)
  List<Object[]> getListadoHistorico(String id);

  @Query("SELECT bc.idControlAnalisis FROM BlanqueoClientesEntity bc WHERE bc.id = ?1")
  String findIdAnalisisCliente(String string);

  @Query("SELECT bc.idControlEfectivo.id FROM BlanqueoClientesEntity bc WHERE bc.id = ?1")
  String findIdControlEfectivo(String string);

  @Query("SELECT bc.idObservacion.id FROM BlanqueoClientesEntity bc WHERE bc.id = ?1")
  String findIdObservacion(String string);

  @Transactional
  @Modifying
  @Query("update BlanqueoClientesEntity cli set cli.fechaRevision = :fecActual where cli.fechaBaja is null")
  void updateBlanqueoClientesSetFechaRevisionForFechaBaja(@Param("fecActual") Date fecActual);

  @Query("SELECT c.fhRevRiesgoPbc FROM Tmct0cli c WHERE c.idClient = ?1")
  Date findFechaRevision(Long string);

  @Query("SELECT c.fhRevPrev FROM Tmct0cli c WHERE c.idClient = ?1")
  Date findFechaRevPrev(Long string);
  
  /**
   * Obtiene todos los alias y nivel 4 para un clien
   * @param idCliente
   * @return
   */
  @Query(value = "SELECT ALI.CDBROCLI, ALI.CDALIASS, ONI.CDNIVEL4"
      + " FROM TMCT0CLI AS bc"
      + " INNER JOIN tmct0ali AS ali"
      + " ON bc.CDBROCLI =  ali.cdbrocli"
      + " AND ali.fhfinal >= to_char(CURRENT date,'yyyymmdd')" 
      + " INNER JOIN tmct0est AS est"
      + " ON ali.cdaliass = est.cdaliass"
      + " AND est.fhfinal >= to_char(CURRENT date,'yyyymmdd')"
      + " INNER JOIN tmct0oni ONI"
      + " ON est.STLEV0LM=ONI.nuorden0"
      + " AND  est.STLEV1LM=ONI.nuorden1"
      + " AND  est.STLEV2LM=ONI.nuorden2"
      + " AND  est.STLEV3LM=ONI.nuorden3"
      + " AND  est.STLEV4LM=ONI.nuorden4"
      + " AND oni.cdnivel4 <> 'BPL'"
      + " WHERE ali.CDBROCLI IN (:idCliente)", nativeQuery = true)
  List<Object[]> getAliasClient(@Param("idCliente") String idCliente);
  
  /**
   * Obtiene todos los clientes activos de un año
   * @param anio
   * @return
   */
  @Query(value = "SELECT TMCT0_BLANQUEO_CLIENTE.ID_CLIENTE, TMCT0_BLANQUEO_CLIENTE.ID_CONTROL_EFECTIVO"
      + " FROM TMCT0_BLANQUEO_CLIENTE INNER JOIN TMCT0_BLANQUEO_EFECTIVO_ACUMULADO"
      + " ON TMCT0_BLANQUEO_CLIENTE.ID_CLIENTE = TMCT0_BLANQUEO_EFECTIVO_ACUMULADO.ID_CLIENTE"
      + " WHERE TMCT0_BLANQUEO_CLIENTE.FECHA_BAJA IS NULL AND TMCT0_BLANQUEO_EFECTIVO_ACUMULADO.ANIO = :anio", nativeQuery = true) 
  List<Object[]> findAllClientByYear(@Param("anio") int anio);
  
  
  @Transactional
  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE TMCT0_BLANQUEO_CLIENTE SET ID_CONTROL_EFECTIVO = :idControlEfectivo WHERE"
      + " ID_CLIENTE = :idCliente", nativeQuery = true)
  void updateClientIdControlEfectivo(@Param("idCliente") String idCliente, @Param("idControlEfectivo") String idControlEfectivo);
 
}