package sibbac.business.pbcdmo.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.pbcdmo.db.model.BlanqueoObservacionEntity;

/**
 * The Interface BlanqueoObservacionDAO.
 */
@Repository
public interface BlanqueoObservacionDAO extends JpaRepository<BlanqueoObservacionEntity, String> {

  @Query(value = "SELECT ob.id, ob.descripcion "
      + "FROM TMCT0_BLANQUEO_OBSERVACION ob WHERE ob.fecha_baja IS NULL ", nativeQuery = true)
  List<Object[]> findListComboObservaciones();

  @Query("SELECT bc.descripcion FROM BlanqueoObservacionEntity bc WHERE bc.id = ?1")
  String findDesObservacion(String string);

}