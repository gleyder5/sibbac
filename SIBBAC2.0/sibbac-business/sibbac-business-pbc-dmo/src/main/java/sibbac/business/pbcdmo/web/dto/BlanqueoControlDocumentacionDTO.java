package sibbac.business.pbcdmo.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import sibbac.business.pbcdmo.db.model.BlanqueoControlDocumentacionEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractControlDTO;

/**
 * The Class TMCT0BlanqueoControlDocumentacionDTO.
 */
public class BlanqueoControlDocumentacionDTO extends AbstractControlDTO<BlanqueoControlDocumentacionEntity> {

  private static final long serialVersionUID = -1230413757285198066L;

  /** Clave primaria. */
  @NotNull
  private String id;

  /** Descripción del control de documentación. */
  @NotNull
  private String descripcion;

  /**
   * Instantiates a new TMCT 0 blanqueo control documentacion DTO.
   */
  public BlanqueoControlDocumentacionDTO() {
    super();
  }

  /**
   * Instantiates a new TMCT 0 blanqueo control documentacion DTO.
   *
   * @param id the id
   * @param fechaAlta the fecha alta
   * @param fechaMod the fecha mod
   * @param fechaBaja the fecha baja
   * @param usuarioAlta the usuario alta
   * @param usuarioMod the usuario mod
   * @param usuarioBaja the usuario baja
   */
  public BlanqueoControlDocumentacionDTO(String id, String descripcion, Date fechaAlta, Date fechaMod, Date fechaBaja,
      String usuarioAlta, String usuarioMod, String usuarioBaja) {
    super(fechaAlta, fechaMod, fechaBaja, usuarioAlta, usuarioMod, usuarioBaja);
    this.id = id;
    this.descripcion = descripcion;
  }

  /**
   * Instantiates a new TMCT 0 blanqueo control documentacion DTO.
   *
   * @param entity the entity
   */
  public BlanqueoControlDocumentacionDTO(BlanqueoControlDocumentacionEntity entity) {
    this.toDto(entity);
  }

  @Override
  protected void toThisDto(BlanqueoControlDocumentacionEntity entity) {
    this.id = entity.getId();
    this.descripcion = entity.getDescripcion();
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "TMCT0BlanqueoControlDocumentacionDTO [id=" + id + ", fechaAlta=" + fechaAlta + ", fechaMod=" + fechaMod
        + ", fechaBaja=" + fechaBaja + ", usuarioAlta=" + usuarioAlta + ", usuarioMod=" + usuarioMod + ", usuarioBaja="
        + usuarioBaja + "]";
  }

}
