package sibbac.business.pbcdmo.bo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.pbcdmo.db.dao.BlanqueoControlEfectivoDAO;
import sibbac.business.pbcdmo.db.model.BlanqueoControlEfectivoEntity;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlEfectivoDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.util.Constantes;
import sibbac.business.pbcdmo.web.util.ConversionDtoToEntity;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class TMCT0BlanqueoControlEfectivoBo.
 */
@Service("controlEfectivoBo")
public class BlanqueoControlEfectivoBo
    extends AbstractBo<BlanqueoControlEfectivoEntity, Integer, BlanqueoControlEfectivoDAO>
    implements BaseBo<BlanqueoControlEfectivoDTO> {

  /** The session. */
  @Autowired
  private HttpSession session;

  @Override
  public CallableResultDTO actionSave(BlanqueoControlEfectivoDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    try {
      BlanqueoControlEfectivoEntity entitydb = dao.findOne(solicitud.getId());
      if (entitydb != null) {
        result.addErrorMessage(Constantes.ApplicationMessages.DUPLICATED_ID.value());
        return result;
      }
      if (solicitud != null) {
        if (solicitud.getId() != null && !solicitud.getDescripcion().isEmpty()
            && solicitud.getImporteEfectivoMax() != null && solicitud.getPerEfectivoMax() != null) {

          solicitud.setFechaAlta(new Timestamp(System.currentTimeMillis()));
          solicitud.setUsuarioAlta(StringUtils.substring(
              ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));

          BlanqueoControlEfectivoEntity entity = new BlanqueoControlEfectivoEntity();
          ConversionDtoToEntity.DtoToEntityEfectivo(entity, solicitud);

          dao.save(entity);

          result.addMessage(Constantes.ApplicationMessages.ADDED_DATA.value());
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error("[executeAction] Los campos ID, Descripción, Importe efectivo Maximo y Porcentaje Efectivo Maximo "
          + "no pueden estar vacio.", e);
      result.addErrorMessage(Constantes.ApplicationMessages.ADD_REQUEST_DATA.value());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }

    return result;
  }

  /**
   * Save efectivo.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @Override
  public CallableResultDTO actionUpdate(BlanqueoControlEfectivoDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    try {
      if (solicitud != null) {
        final BlanqueoControlEfectivoEntity entityFind = dao.findOne(solicitud.getId());

        if (entityFind != null) {
          if (solicitud.getDescripcion() != null) {
            entityFind.setDescripcion(solicitud.getDescripcion());
          }
          if (solicitud.getImporteEfectivoMax() != null) {
            entityFind.setImporteEfectivoMax(new BigDecimal(solicitud.getImporteEfectivoMax().replaceAll(",", ".")));
          }
          if (solicitud.getPerEfectivoMax() != null) {
            entityFind.setPerEfectivoMax(new BigDecimal(solicitud.getPerEfectivoMax().replaceAll(",", ".")));
          }

          entityFind.setFechaMod(new Timestamp(System.currentTimeMillis()));
          entityFind.setUsuarioMod(StringUtils.substring(
              ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));

          dao.save(entityFind);

          result.addMessage(Constantes.ApplicationMessages.MODIFIED_DATA.value());
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(
          "Error, compruebe que el id \"" + solicitud.getId() + "\" del objeto exista en la base de datos.");
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  /**
   * Delete efectivo.
   *
   * @param solicitudList the solicitud list
   * @return the callable result DTO
   */
  @Override
  public CallableResultDTO actionDelete(List<String> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      for (String solicitud : solicitudList) {
        if (solicitud != null) {
          final BlanqueoControlEfectivoEntity entityFind = dao.findOne(Integer.valueOf(solicitud));

          if (entityFind != null) {
            entityFind.setFechaBaja(new Timestamp(System.currentTimeMillis()));
            entityFind.setUsuarioBaja(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));

            dao.save(entityFind);
          }
          else {
            err.add(String.valueOf(solicitud));
          }
        }
      }
      if (!err.isEmpty()) {
        throw new SIBBACBusinessException();
      }
      else {
        result.addMessage(Constantes.ApplicationMessages.DELETED_DATA.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(
          MessageFormat.format(Constantes.ApplicationMessages.MULTIPLE_NOT_EXIST.value(), err.toString()));
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  @Override
  public CallableResultDTO actionFindById(BlanqueoControlEfectivoDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  public BlanqueoControlEfectivoDTO getDatosEfectivo(String id) {
    BlanqueoControlEfectivoDTO efectivo = new BlanqueoControlEfectivoDTO();
    if (id != null) {
      BlanqueoControlEfectivoEntity entityFind = dao.findOne(Integer.valueOf(id));

      if (entityFind != null) {
        efectivo = new BlanqueoControlEfectivoDTO(entityFind);
      }
    }
    return efectivo;
  }

  @Override
  public CallableResultDTO update(ObservacionClienteDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

}
