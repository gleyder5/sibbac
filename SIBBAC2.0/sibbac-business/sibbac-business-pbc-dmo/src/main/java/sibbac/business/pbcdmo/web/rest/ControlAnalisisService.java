package sibbac.business.pbcdmo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.pbcdmo.bo.BaseBo;
import sibbac.business.pbcdmo.db.query.ControlAnalisisQuery;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlAnalisisDTO;
import sibbac.common.CallableResultDTO;

/**
 * The Class SIBBACServiceControlAnalisis.
 */
@RestController
@RequestMapping("/datosMaestros/controlAnalisis")
public class ControlAnalisisService extends BaseService {

  /** The blanqueo control analisis bo. */
  @Autowired
  private BaseBo<BlanqueoControlAnalisisDTO> controlAnalisisBo;

  /**
   * Instantiates a new SIBBAC service control analisis.
   */
  public ControlAnalisisService() {
    super("Blanqueo Control Analisis", "blanqueoControlAnalisis", ControlAnalisisQuery.class.getSimpleName());
  }

  /**
   * Execute action post.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody BlanqueoControlAnalisisDTO solicitud) {
    return controlAnalisisBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody BlanqueoControlAnalisisDTO solicitud) {
    return controlAnalisisBo.actionUpdate(solicitud);
  }

  /**
   * Execute action delete List
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/{ids}", method = RequestMethod.DELETE)
  public CallableResultDTO executeActionDelete(@PathVariable("ids") List<String> solicitud) {
    return controlAnalisisBo.actionDelete(solicitud);
  }

}
