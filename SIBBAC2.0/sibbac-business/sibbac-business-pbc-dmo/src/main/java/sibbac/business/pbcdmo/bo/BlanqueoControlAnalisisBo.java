package sibbac.business.pbcdmo.bo;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.pbcdmo.db.dao.BlanqueoControlAnalisisDAO;
import sibbac.business.pbcdmo.db.model.BlanqueoControlAnalisisEntity;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlAnalisisDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.util.Constantes;
import sibbac.business.pbcdmo.web.util.ConversionDtoToEntity;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

@Service("controlAnalisisBo")
public class BlanqueoControlAnalisisBo
    extends AbstractBo<BlanqueoControlAnalisisEntity, String, BlanqueoControlAnalisisDAO>
    implements BaseBo<BlanqueoControlAnalisisDTO> {

  /** The session. */
  @Autowired
  private HttpSession session;

  /**
   * 
   */
  @Override
  public CallableResultDTO actionSave(BlanqueoControlAnalisisDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    try {
      BlanqueoControlAnalisisEntity entitydb = dao.findOne(solicitud.getId());
      if (entitydb != null) {
        result.addErrorMessage(Constantes.ApplicationMessages.DUPLICATED_ID.value());
        return result;
      }

      if (solicitud != null) {
        if (!solicitud.getId().isEmpty() && !solicitud.getDescripcion().isEmpty()) {
          solicitud.setUsuarioAlta(StringUtils.substring(
              ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
          solicitud.setFechaAlta(new Timestamp(System.currentTimeMillis()));

          BlanqueoControlAnalisisEntity entity = new BlanqueoControlAnalisisEntity();
          ConversionDtoToEntity.DtoToEntityAnalisis(entity, solicitud);

          dao.save(entity);

          result.addMessage(Constantes.ApplicationMessages.ADDED_DATA.value());
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error("[executeActionPost] Los campos ID y Descripción no pueden estar vacio.", e);
      result.addErrorMessage(Constantes.ApplicationMessages.ADD_REQUEST_DATA.value());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  /**
   * 
   */
  @Override
  public CallableResultDTO actionUpdate(BlanqueoControlAnalisisDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    try {
      if (solicitud != null) {
        BlanqueoControlAnalisisEntity entity = this.findById(solicitud.getId());

        if (entity != null) {
          if (solicitud.getDescripcion() != null) {
            entity.setDescripcion(solicitud.getDescripcion());
          }

          entity.setUsuarioMod(StringUtils.substring(
              ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
          entity.setFechaMod(new Timestamp(System.currentTimeMillis()));

          dao.save(entity);

          result.addMessage(Constantes.ApplicationMessages.MODIFIED_DATA.value());
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  /**
   * 
   */
  @Override
  public CallableResultDTO actionDelete(List<String> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      for (String solicitud : solicitudList) {
        if (solicitud != null) {
          BlanqueoControlAnalisisEntity entity = this.findById(solicitud);
          if (entity != null) {

            entity.setUsuarioBaja(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
            entity.setFechaBaja(new Timestamp(System.currentTimeMillis()));

            dao.save(entity);
          }
          else {
            err.add(solicitud);
          }
        }
      }
      if (!err.isEmpty()) {
        throw new SIBBACBusinessException();
      }
      else {
        result.addMessage(Constantes.ApplicationMessages.DELETED_DATA.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.MULTIPLE_ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(
          MessageFormat.format(Constantes.ApplicationMessages.MULTIPLE_NOT_EXIST.value(), err.toString()));
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  @Override
  public CallableResultDTO actionFindById(BlanqueoControlAnalisisDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO update(ObservacionClienteDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }
}
