package sibbac.business.pbcdmo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.pbcdmo.bo.BaseBo;
import sibbac.business.pbcdmo.bo.PaisesBo;
import sibbac.business.pbcdmo.db.query.ParaisosFiscalesQuery;
import sibbac.business.pbcdmo.web.dto.PaisesDTO;
import sibbac.business.pbcdmo.web.dto.PaisesPKDTO;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.FilterInfo;

/**
 * The Class SIBBACServiceParaisosFiscales.
 */
@RestController
@RequestMapping("/datosMaestros/pais")
public class ParaisosFiscalesService extends BaseService {

  /** The paraisos fiscales bo. */
  @Autowired
  private BaseBo<PaisesDTO> iPaisesBo;

  @Autowired
  private PaisesBo paisesBo;

  public ParaisosFiscalesService() {
    super("Paraísos Fiscales", "paraisosFiscales", ParaisosFiscalesQuery.class.getSimpleName());
  }

  @RequestMapping(value = "/{name}/filters", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<FilterInfo> getFilters(@PathVariable("name") String name) throws SIBBACBusinessException {

    try {
      final AbstractDynamicPageQuery query;

      query = getDynamicQuery(name);
      return query.getFiltersInfo();
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error obteniendo filtros", e);
    }
  }

  /**
   * Execute action post.
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody PaisesDTO solicitud) {
    return iPaisesBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody PaisesDTO solicitud) {
    return iPaisesBo.actionUpdate(solicitud);
  }

  /**
   * Execute action delete List
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/delete", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionDelete(@RequestBody List<PaisesPKDTO> ids) {
    return paisesBo.actionDeleteParaiso(ids);
  }

  /**
   * 
   * @param name
   * @return
   * @throws Exception
   */
  private AbstractDynamicPageQuery getDynamicQuery(String name) throws SIBBACBusinessException {
    return ctx.getBean(name, AbstractDynamicPageQuery.class);
  }
}
