package sibbac.business.pbcdmo.web.dto;

/**
 * The Class HistoricoClienteDTO.
 */
public class HistoricoClienteDTO {

  /** The fecha cambio. */
  private String fechaCambio;

  /** The cliente. */
  private HistoricoDTO cliente;

  /**
   * Instantiates a new historico cliente DTO.
   */
  public HistoricoClienteDTO() {
    super();
  }

  /**
   * Instantiates a new historico cliente DTO.
   *
   * @param fechaCambio the fecha cambio
   * @param cliente the cliente
   */
  public HistoricoClienteDTO(String fechaCambio, HistoricoDTO cliente) {
    super();
    this.fechaCambio = fechaCambio;
    this.cliente = cliente;
  }

  /**
   * Gets the fecha cambio.
   *
   * @return the fecha cambio
   */
  public String getFechaCambio() {
    return fechaCambio;
  }

  /**
   * Sets the fecha cambio.
   *
   * @param fechaCambio the new fecha cambio
   */
  public void setFechaCambio(String fechaCambio) {
    this.fechaCambio = fechaCambio;
  }

  /**
   * Gets the cliente.
   *
   * @return the cliente
   */
  public HistoricoDTO getCliente() {
    return cliente;
  }

  /**
   * Sets the cliente.
   *
   * @param cliente the new cliente
   */
  public void setCliente(HistoricoDTO cliente) {
    this.cliente = cliente;
  }

}
