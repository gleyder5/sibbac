package sibbac.business.pbcdmo.web.dto;

import sibbac.business.pbcdmo.web.dto.common.AbstractDTO;

/**
 * The Class ObservacionClienteDTO.
 */
public class ObservacionClienteDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1230413757285198066L;

  /** The id observacion. */
  private Long idObservacion;

  /** The cliente. */
  private BlanqueoClientesDTO cliente;

  public ObservacionClienteDTO() {
    super();
  }

  /**
   * Instantiates a new observacion cliente DTO.
   *
   * @param idObservacion the id observacion
   * @param cliente the cliente
   */
  public ObservacionClienteDTO(Long idObservacion, BlanqueoClientesDTO cliente) {
    super();
    this.idObservacion = idObservacion;
    this.cliente = cliente;
  }

  /**
   * Gets the id observacion.
   *
   * @return the id observacion
   */
  public Long getIdObservacion() {
    return idObservacion;
  }

  /**
   * Sets the id observacion.
   *
   * @param idObservacion the new id observacion
   */
  public void setIdObservacion(Long idObservacion) {
    this.idObservacion = idObservacion;
  }

  /**
   * Gets the cliente.
   *
   * @return the cliente
   */
  public BlanqueoClientesDTO getCliente() {
    return cliente;
  }

  /**
   * Sets the cliente.
   *
   * @param cliente the new cliente
   */
  public void setCliente(BlanqueoClientesDTO cliente) {
    this.cliente = cliente;
  }
}
