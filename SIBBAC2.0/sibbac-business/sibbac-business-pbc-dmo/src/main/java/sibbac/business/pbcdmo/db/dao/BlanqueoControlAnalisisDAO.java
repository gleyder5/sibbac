package sibbac.business.pbcdmo.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.pbcdmo.db.model.BlanqueoControlAnalisisEntity;

/**
 * The Interface TMCT0blanqueoControlAnalisisRepository.
 */
@Repository
public interface BlanqueoControlAnalisisDAO extends JpaRepository<BlanqueoControlAnalisisEntity, String> {

  @Query(value = "SELECT ca.id, ca.descripcion "
      + "FROM TMCT0_BLANQUEO_CONTROL_ANALISIS ca WHERE ca.fecha_baja IS NULL ", nativeQuery = true)
  List<Object[]> findListAnalisis();

  @Query(value = "SELECT obs.id_control_analisis, ana.DESCRIPCION FROM bsnbpsql.TMCT0_BLANQUEO_OBSERVACION obs "
      + "INNER JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_ANALISIS ana on ana.ID = obs.ID_CONTROL_ANALISIS "
      + "WHERE obs.id = ?1", nativeQuery = true)
  List<Object[]> findAnalisisByObservacion(String id);
}
