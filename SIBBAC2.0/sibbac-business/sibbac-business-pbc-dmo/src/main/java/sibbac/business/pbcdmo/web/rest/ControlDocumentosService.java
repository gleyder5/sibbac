package sibbac.business.pbcdmo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.pbcdmo.bo.BaseBo;
import sibbac.business.pbcdmo.db.query.ControlDocumentacionPageQuery;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlDocumentacionDTO;
import sibbac.common.CallableResultDTO;

/**
 * The Class SIBBACServiceDocumentos.
 */
@RestController
@RequestMapping("/datosMaestros/controlDocumentos")
public class ControlDocumentosService extends BaseService {

  @Autowired
  private BaseBo<BlanqueoControlDocumentacionDTO> controlDocumentacionBo;

  /**
   * Instantiates a new SIBBAC service documentos.
   */
  public ControlDocumentosService() {
    super("Control de documentacion", "controlDocumentacion", ControlDocumentacionPageQuery.class.getSimpleName());
  }

  /**
   * Execute action post.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody BlanqueoControlDocumentacionDTO solicitud) {
    return controlDocumentacionBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody BlanqueoControlDocumentacionDTO solicitud) {
    return controlDocumentacionBo.actionUpdate(solicitud);
  }
  
  /**
   * Execute action delete List
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/{ids}", method = RequestMethod.DELETE)
  public CallableResultDTO executeActionDelete(@PathVariable("ids") List<String> solicitud) {
    return controlDocumentacionBo.actionDelete(solicitud);
  }
}
