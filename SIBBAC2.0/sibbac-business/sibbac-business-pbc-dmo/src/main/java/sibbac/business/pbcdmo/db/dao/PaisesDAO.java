package sibbac.business.pbcdmo.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.pbcdmo.db.model.PaisesEntity;
import sibbac.business.pbcdmo.db.model.PaisesPK;

/**
 * The Interface TMCT0PaisesDAO.
 */
@Repository
public interface PaisesDAO extends JpaRepository<PaisesEntity, PaisesPK> {

}