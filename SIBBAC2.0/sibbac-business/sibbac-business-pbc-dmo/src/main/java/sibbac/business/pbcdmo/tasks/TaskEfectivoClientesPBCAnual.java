package sibbac.business.pbcdmo.tasks;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.pbcdmo.bo.BlanqueoControlEfectivoAcumuladoBo;
import sibbac.business.pbcdmo.web.util.Constantes.CargaEfectivo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

//0 0 7 1 * ? Fire at 7:00am on the 1st day of every month
//0 0 0 ? * Sat * Fire the first saturday of the month
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_PBC.NAME, name = Task.GROUP_PBC.JOB_EFECTIVO_ANUAL, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 7 1 * ?")
public class TaskEfectivoClientesPBCAnual extends WrapperTaskConcurrencyPrevent{

  @Autowired
  private BlanqueoControlEfectivoAcumuladoBo efectivoAcumuladoBo;
  
  @Override
  public void executeTask() throws Exception {
    efectivoAcumuladoBo.effectiveCustomersLoad(CargaEfectivo.EJECUCION_ANUAL.value()); 
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_PCB_EFECTIVO_ANUAL;
  }

}
