package sibbac.business.pbcdmo.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DBConstants;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class ParaisosFiscalesQuery.
 */
@Component("ParaisosFiscalesQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParaisosFiscalesQuery extends AbstractDynamicPageQuery {

  /** The Constant NBPAIS. */
  private static final String NBPAIS = "Nombre de país";

  /** The Constant CDISOALF2. */
  private static final String CDISOALF2 = "ISO Alfanumérico(2)";

  /** The Constant CDISOALF3. */
  private static final String CDISOALF3 = "ISO Alfanumérico(3)";

  /** The Constant CDISONUM. */
  private static final String CDISONUM = "ISO Numérico";

  /** The Constant CDHACIENDA. */
  private static final String CDHACIENDA = "Código de Hacienda";

  /** The Constant CDCODKGR. */
  private static final String CDCODKGR = "Código Global";

  /** The Constant CDCODKGR. */
  private static final String INDICADOR_DMO = "Indicador DMO";

  /** The Constant FECHA_INICIO_PAR_FISC. */
  private static final String FECHA_INICIO_PAR_FISC = "Fecha Inicio Paraíso";

  /** The Constant FECHA_FIN_PAR_FISC. */
  private static final String FECHA_FIN_PAR_FISC = "Fecha Fin Paraíso";

  /** The Constant CD_TIPO_ID_MIFID. */
  private static final String CD_TIPO_ID_MIFID = "Indicador MIFID";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT NBPAIS, CDISOALF2, CDISOALF3, CDISONUM, "
      + "CDHACIENDA, CDCODKGR, CDESTADO, INDICADOR_DMO, FECHA_INICIO_PAR_FISC, FECHA_FIN_PAR_FISC, CD_TIPO_ID_MIFID";

  /** The Constant FROM. */
  private static final String FROM = " FROM " + DBConstants.PBCDMO.TMCT0_PAISES;

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE CDESTADO = 'A'";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn(NBPAIS, "NBPAIS"),
      new DynamicColumn(CDISOALF2, "CDISOALF2"), new DynamicColumn(CDISOALF3, "CDISOALF3"),
      new DynamicColumn(CDISONUM, "CDISONUM"), new DynamicColumn(CDHACIENDA, "CDHACIENDA"),
      new DynamicColumn(CDCODKGR, "CDCODKGR"), new DynamicColumn(INDICADOR_DMO, "INDICADOR_DMO"),
      new DynamicColumn(FECHA_INICIO_PAR_FISC, "FECHA_INICIO_PAR_FISC", DynamicColumn.ColumnType.DATE),
      new DynamicColumn(FECHA_FIN_PAR_FISC, "FECHA_FIN_PAR_FISC", DynamicColumn.ColumnType.DATE),
      new DynamicColumn(CD_TIPO_ID_MIFID, "CD_TIPO_ID_MIFID") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter("NBPAIS", NBPAIS));
    return res;
  }

  public ParaisosFiscalesQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public ParaisosFiscalesQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public ParaisosFiscalesQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}
