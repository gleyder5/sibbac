package sibbac.business.pbcdmo.bo;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.pbcdmo.db.dao.BlanqueoClientesDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoControlAnalisisDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoControlDocumentacionDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoControlEfectivoDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoEfectivoAcumuladoDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoObservacionClienteDAO;
import sibbac.business.pbcdmo.db.dao.BlanqueoObservacionDAO;
import sibbac.business.pbcdmo.db.dao.TMCT0CliDAO;
import sibbac.business.pbcdmo.db.model.BlanqueoClientesEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoControlDocumentacionEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoControlEfectivoEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoObservacionClienteEntity;
import sibbac.business.pbcdmo.db.model.BlanqueoObservacionEntity;
import sibbac.business.pbcdmo.web.dto.AnalisisObservacionDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoClientesDTO;
import sibbac.business.pbcdmo.web.dto.HistoricoClienteDTO;
import sibbac.business.pbcdmo.web.dto.HistoricoDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionesDTO;
import sibbac.business.pbcdmo.web.util.Constantes;
import sibbac.business.pbcdmo.web.util.Constantes.Session;
import sibbac.business.pbcdmo.web.util.ConversionDtoToEntity;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class TMCT0BlanqueoControlDocumentacionBo.
 */
@Service("clientesBo")
public class BlanqueoClientesBo extends AbstractBo<BlanqueoClientesEntity, String, BlanqueoClientesDAO>
    implements BaseBo<BlanqueoClientesDTO> {

  /** The session. */
  @Autowired
  private HttpSession session;

  @Autowired
  private BlanqueoControlEfectivoDAO efectivoDAO;

  @Autowired
  private BlanqueoControlAnalisisDAO analisisDAO;

  @Autowired
  private BlanqueoControlDocumentacionDAO documentacionDAO;

  @Autowired
  private BlanqueoObservacionDAO observacionDAO;

  @Autowired
  private BlanqueoEfectivoAcumuladoDAO efectivoAcumulado;

  @Autowired
  private TMCT0CliDAO cliDAO;

  @Autowired
  private BlanqueoClientesDAO clientesDAO;

  @Autowired
  private BlanqueoObservacionClienteDAO observacionClientesDAO;

  /**
   * 
   */
  @Override
  // @Transactional
  public CallableResultDTO actionSave(BlanqueoClientesDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    final CallableResultDTO result = callableResultDTO;

    try {
      if (solicitud != null) {
        BlanqueoClientesEntity entitydb = dao.findOne(solicitud.getId());
        if (entitydb != null) {
          result.addErrorMessage(Constantes.ApplicationMessages.DUPLICATED_ID.value());
          return result;
        }
        if (!solicitud.getId().isEmpty() && !solicitud.getIdControlDocumentacion().isEmpty()
            && (null != solicitud.getIdControlEfectivo())) {

          BlanqueoControlEfectivoEntity efectivoEntity = efectivoDAO.findOne(solicitud.getIdControlEfectivo());
          // Comprobamos si ha devuelto datos, en caso contrario, devolveremos
          // un mensaje de error.
          if (null != efectivoEntity) {
            BlanqueoControlDocumentacionEntity documentacionEntity = documentacionDAO
                .findOne(solicitud.getIdControlDocumentacion());
            if (null != documentacionEntity) {

              BlanqueoObservacionEntity observacionEntity = new BlanqueoObservacionEntity();

              if (null != solicitud.getIdObservacion()) {
                observacionEntity = observacionDAO.findOne(solicitud.getIdObservacion());
              }
              solicitud.setUsuarioAlta(StringUtils.substring(
                  ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
              solicitud.setFechaAlta(new Timestamp(System.currentTimeMillis()));
              solicitud.setActivo(Constantes.Activo.SAVE.value());

              BlanqueoClientesEntity entity = new BlanqueoClientesEntity();
              ConversionDtoToEntity.DtoToEntityCliente(entity, solicitud, efectivoEntity, documentacionEntity,
                  observacionEntity);

              dao.save(entity);

              // Comprobamos si no trae una observacion. Si la trae, guardamos
              // la relacion de esa observacion con el
              // cliente en la tabla BLANQUEO_OBSERVACION_CLIENTE
              if (null != solicitud.getIdObservacion()) {
                guardarObservacionCliente(solicitud);
              }

              result.addMessage(Constantes.ApplicationMessages.ADDED_DATA.value());

            }
            else {
              throw new SIBBACBusinessException(Constantes.LoggingMessages.DOCUMENTACION_NULL.value());
            }
          }
          else {
            throw new SIBBACBusinessException(Constantes.LoggingMessages.EFECTIVO_NULL.value());
          }
        }
        else {
          throw new SIBBACBusinessException();
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.BASIC_VALIDATION_ERROR.value(), e);
      result.addErrorMessage(e.getMessage());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  private void guardarObservacionCliente(BlanqueoClientesDTO cliente) throws SIBBACBusinessException {
    BlanqueoObservacionClienteEntity entity = new BlanqueoObservacionClienteEntity();
    entity.setIdCliente(cliente.getId());
    entity.setIdObservacion(cliente.getIdObservacion());
    entity.setUsuarioAlta(StringUtils
        .substring(((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
    entity.setFechaAlta(new Timestamp(System.currentTimeMillis()));

    observacionClientesDAO.save(entity);
  }

  /**
   * 
   */
  @Override
  public CallableResultDTO update(ObservacionClienteDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();
    BlanqueoClientesDTO cliente = solicitud.getCliente();
    Long idObservacionCliente = solicitud.getIdObservacion();

    try {
      if (cliente != null) {
        BlanqueoClientesEntity entity = this.findById(cliente.getId());
        // Si encontramos el cliente que queremos modificar...
        if (entity != null) {
          if (!cliente.getId().isEmpty() && !cliente.getIdControlDocumentacion().isEmpty()
              && (null != cliente.getIdControlEfectivo())) {
            BlanqueoControlEfectivoEntity efectivoEntity = efectivoDAO.findOne(cliente.getIdControlEfectivo());
            // Comprobamos si ha devuelto datos para los campos obligatorios, en
            // caso contrario, devolveremos un mensaje de error.
            if (null != efectivoEntity) {
              BlanqueoControlDocumentacionEntity documentacionEntity = documentacionDAO
                  .findOne(cliente.getIdControlDocumentacion());
              if (null != documentacionEntity) {

                entity.setFechaMod(new Timestamp(System.currentTimeMillis()));
                entity.setUsuarioMod(StringUtils.substring(
                    ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
                entity.setActivo(cliente.getActivo());

                controlObservacionesCliente(cliente, idObservacionCliente);

                BlanqueoObservacionEntity observacionEntity = null;

                // Comprobamos la idObservacion del cliente, por si no viene
                // vacia
                if (null != cliente.getIdObservacion()) {
                  observacionEntity = observacionDAO.findOne(cliente.getIdObservacion());
                  cliente.setIdControlAnalisis(observacionEntity.getIdControlAnalisis());
                }
                // Si viene vacia, controlamos que el id de analisis vaya vacio.
                else {
                  entity.setIdControlAnalisis(null);
                }

                // Por último, guardamos el cliente con todas sus modificaciones
                ConversionDtoToEntity.DtoToEntityCliente(entity, cliente, efectivoEntity, documentacionEntity,
                    observacionEntity);
                dao.save(entity);
                result.addMessage(Constantes.ApplicationMessages.MODIFIED_DATA.value());
              }
              else {
                throw new SIBBACBusinessException(Constantes.LoggingMessages.DOCUMENTACION_NULL.value());
              }
            }
            else {
              throw new SIBBACBusinessException(Constantes.LoggingMessages.EFECTIVO_NULL.value());
            }
          }
          else {
            throw new SIBBACBusinessException(Constantes.LoggingMessages.PK_OBJECT_NULL.value());
          }
        }
        else {
          throw new SIBBACBusinessException(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
        }
      }
      else {
        throw new SIBBACBusinessException(Constantes.LoggingMessages.OBJECT_NULL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
      result.addErrorMessage(e.getMessage());
    }
    return result;
  }

  private void controlObservacionesCliente(BlanqueoClientesDTO cliente, Long idObservacionCliente)
      throws SIBBACBusinessException {
    // AHORA SE GUARDARA LOS DATOS DE LA OBSERVACION RELACIONADA CON
    // ESTE CLIENTE
    if (null != cliente.getIdObservacion() && !cliente.getIdObservacion().isEmpty()) {
      // CREAR-DUPLICAR-MODIFICAR
      if (null == idObservacionCliente) {
        // CREAR-DUPLICAR
        guardarObservacionCliente(cliente);
      }
      else {
        // MODIFICAR
        modificarObservacionCliente(cliente, idObservacionCliente);
      }
    }
    // SI EL CAMPO DE ID_OBSERVACION DEL OBJETO CLIENTE VIENE VACIO,
    // SIGNIFICA QUE QUEREMOS QUE UNA RELACION DE OBSERVACION CON
    // NUESTRO CLIENTE SEA BORRADA
    else if (null != idObservacionCliente) {
      eliminarObservacionCliente(cliente, idObservacionCliente);
    }
  }

  private void eliminarObservacionCliente(BlanqueoClientesDTO cliente, Long idObservacionCliente)
      throws SIBBACBusinessException {
    BlanqueoObservacionClienteEntity entity = observacionClientesDAO.findOne(idObservacionCliente);

    // Si trae datos, significa que existe y podemos darle la baja logica
    if (null != entity) {

      entity.setUsuarioBaja(StringUtils
          .substring(((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
      entity.setFechaBaja(new Timestamp(System.currentTimeMillis()));

      observacionClientesDAO.save(entity);

      List<Object[]> listadoTotal = observacionClientesDAO.getListadoObservacionesClientes(cliente.getId());
      List<ObservacionesDTO> listadoTotalObservaciones = new ArrayList<>();

      convertObjectToObservacionesDTO(listadoTotal, listadoTotalObservaciones);

      if (!listadoTotalObservaciones.isEmpty()) {
        cliente
            .setIdObservacion(listadoTotalObservaciones.get(listadoTotalObservaciones.size() - 1).getIdObservacion());
      }
      else {
        cliente.setIdObservacion(null);
      }

    }
    else {
      throw new SIBBACBusinessException(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
    }
  }

  private void modificarObservacionCliente(BlanqueoClientesDTO cliente, Long idObservacionCliente)
      throws SIBBACBusinessException {
    BlanqueoObservacionClienteEntity entity = observacionClientesDAO.findOne(idObservacionCliente);

    // Si trae datos, significa que existe y podemos modificarlo
    if (null != entity) {
      entity.setIdObservacion(cliente.getIdObservacion());
      entity.setUsuarioMod(StringUtils
          .substring(((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
      entity.setFechaMod(new Timestamp(System.currentTimeMillis()));

      observacionClientesDAO.save(entity);
    }
    else {
      throw new SIBBACBusinessException(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
    }
  }

  /**
   * 
   */
  @Override
  public CallableResultDTO actionDelete(List<String> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      for (String solicitud : solicitudList) {
        if (solicitud != null) {
          BlanqueoClientesEntity entity = this.findById(solicitud);
          if (entity != null) {

            entity.setUsuarioBaja(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
            entity.setFechaBaja(new Timestamp(System.currentTimeMillis()));
            entity.setActivo(Constantes.Activo.DELETE.value());

            dao.save(entity);

            List<Object[]> listadoObservacionesClientes = observacionClientesDAO
                .getListadoObservacionesClientes(solicitud);
            if (listadoObservacionesClientes.size() > 0) {
              observacionClientesDAO.updateObservacionClienteSetFechaBajaAndUsuarioBajaByIdClienteAndFechaBajaNull(
                  new Date(System.currentTimeMillis()),
                  StringUtils.substring(
                      ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10),
                  solicitud);
            }

          }
          else {
            err.add(solicitud);
          }
        }
      }
      if (!err.isEmpty()) {
        throw new SIBBACBusinessException();
      }
      else {
        result.addMessage(Constantes.ApplicationMessages.DELETED_DATA.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.MULTIPLE_ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(
          MessageFormat.format(Constantes.ApplicationMessages.MULTIPLE_NOT_EXIST.value(), err.toString()));
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  @Override
  public CallableResultDTO actionFindById(BlanqueoClientesDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  /**
   * Método que devuelve el listado de control de efectivos
   * @return
   */
  public List<LabelValueObject> getListEfectivo() {

    final List<LabelValueObject> list;
    final StringBuilder label;

    list = new ArrayList<>();
    list.add(0, new LabelValueObject("", ""));

    label = new StringBuilder();
    for (Object[] arr : efectivoDAO.findListEfectivo()) {
      int i = 0;
      if (arr != null) {
        for (Object a : arr) {

          if (i == 0) {
            label.append(a);
          }
          else if (i == 1) {
            label.append("-(").append(a).append(")");
          }
          else if (i == 2) {
            label.append("-").append(a);
          }
          i = i + 1;
        }

        if (arr[0] != null) {
          list.add(new LabelValueObject(label.toString(), arr[0].toString()));
        }
      }

      label.setLength(0);
    }
    return list;
  }

  /**
   * Método que devuelve el listado de control de analisis
   * @return
   */
  public List<LabelValueObject> getListAnalisis() {

    final List<LabelValueObject> list;
    final StringBuilder label;

    list = new ArrayList<>();
    list.add(0, new LabelValueObject("", ""));

    label = new StringBuilder();
    for (Object[] arr : analisisDAO.findListAnalisis()) {
      int i = 0;
      if (arr != null) {
        for (Object a : arr) {
          if (i == 0) {
            label.append(a);
          }
          else if (i == 1) {
            label.append("-(").append(a).append(")");
          }
          else if (i == 2) {
            label.append("-").append(a);
          }
          i = i + 1;
        }

        if (arr[0] != null) {
          list.add(new LabelValueObject(label.toString(), arr[0].toString()));
        }
      }

      label.setLength(0);
    }
    return list;
  }

  /**
   * Método que devuelve el listado de control de documentos
   * @return
   */
  public List<LabelValueObject> getListDocumentos() {

    final List<LabelValueObject> list;
    final StringBuilder label;

    list = new ArrayList<>();
    list.add(0, new LabelValueObject("", ""));

    label = new StringBuilder();
    for (Object[] arr : documentacionDAO.findListDocumentos()) {
      int i = 0;
      if (arr != null) {
        for (Object a : arr) {
          if (i == 0) {
            label.append(a);
          }
          else if (i == 1) {
            label.append("-(").append(a).append(")");
          }
          else if (i == 2) {
            label.append("-").append(a);
          }
          i = i + 1;
        }

        if (arr[0] != null) {
          list.add(new LabelValueObject(label.toString(), arr[0].toString()));
        }
      }

      label.setLength(0);
    }
    return list;
  }

  /**
   * Método que devuelve el listado de observaciones
   * @return
   */
  public List<LabelValueObject> getlistadoComboObservaciones() {

    final List<LabelValueObject> list;
    final StringBuilder label;

    list = new ArrayList<>();
    list.add(0, new LabelValueObject("", ""));

    label = new StringBuilder();
    for (Object[] arr : observacionDAO.findListComboObservaciones()) {
      int i = 0;
      if (arr != null) {
        for (Object a : arr) {
          if (i == 0) {
            label.append(a);
          }
          else if (i == 1) {
            label.append("-").append(a);
          }
          else if (i == 2) {
            label.append("-").append(a);
          }
          i = i + 1;
        }

        if (arr[0] != null) {
          list.add(new LabelValueObject(label.toString(), label.toString()));
        }
      }

      label.setLength(0);
    }
    return list;
  }

  /**
   * 
   * @param solicitud
   * @return
   */
  public AnalisisObservacionDTO getControlAnalisis(String solicitud) {

    List<Object[]> listadoTotal = analisisDAO.findAnalisisByObservacion(solicitud);

    AnalisisObservacionDTO analisis = new AnalisisObservacionDTO();

    if (listadoTotal.size() > 0) {
      Object[] element = listadoTotal.get(0);

      if (null != element[0] && null != element[1]) {
        analisis.setId(element[0].toString());
        analisis.setDescripcion(element[1].toString());
      }
    }

    return analisis;

  }

  /**
   * Método que actuliza masivamente la fecha de revisión
   * @param solicitud
   * @return
   */
  public CallableResultDTO updateFechaRevision() {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    final CallableResultDTO result = callableResultDTO;
    try {
      Calendar calendar = Calendar.getInstance();
      Date fecActual = new Date(calendar.getTime().getTime());
      dao.updateBlanqueoClientesSetFechaRevisionForFechaBaja(fecActual);

      result.addMessage(Constantes.ApplicationMessages.DATE_CHANGED.value());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  public List<HistoricoClienteDTO> getListadoHistorico(String idCliente) {
    try {
      // Nos traemos la lista de historico del cliente especifico
      List<Object[]> listadoTotal = clientesDAO.getListadoHistorico(idCliente);
      List<HistoricoDTO> listadoTotalHistorico = new ArrayList<>();

      convertObjectToHistoricoDTO(listadoTotal, listadoTotalHistorico);

      List<HistoricoClienteDTO> listadoClienteHistoricoConFechaCambio = new ArrayList<>();

      // Recorremos la lista de historico para asignarle una fecha de cambio que
      // dependerá de la fecha de modificacion o de la fecha de alta
      for (int i = 0; i < listadoTotalHistorico.size(); i++) {
        rellenarFechaCambioYCliente(listadoTotalHistorico.get(i), listadoClienteHistoricoConFechaCambio);
      }

      return listadoClienteHistoricoConFechaCambio;

    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      return null;
    }
  }

  private void convertObjectToHistoricoDTO(List<Object[]> listadoTotal, List<HistoricoDTO> listadoTotalHistorico) {
    for (Object[] element : listadoTotal) {
      HistoricoDTO hist = toDTO(element);
      listadoTotalHistorico.add(hist);
    }

  }

  private HistoricoDTO toDTO(Object[] element) {
    HistoricoDTO hist = new HistoricoDTO();
    if (null != element[0]) {
      hist.setCliente(element[0].toString());
    }
    if (null != element[1]) {
      hist.setKgr(element[1].toString());
    }
    if (null != element[2]) {
      hist.setIdControlEfectivo(Integer.valueOf(element[2].toString()));
    }
    if (null != element[3]) {
      hist.setDesControlEfectivo(element[3].toString());
    }
    if (null != element[4]) {
      hist.setIdControlDocumentacion(element[4].toString());
    }
    if (null != element[5]) {
      hist.setDesControlDocumentacion(element[5].toString());
    }
    if (null != element[6]) {
      hist.setIdControlAnalisis(element[6].toString());
    }
    if (null != element[7]) {
      hist.setDesControlAnalisis(element[7].toString());
    }
    if (null != element[8]) {
      hist.setObservacion(element[8].toString());
    }
    if (null != element[9]) {
      hist.setFechaAlta(stringToDate(element[9].toString()));
    }
    if (null != element[10]) {
      hist.setFechaMod(stringToDate(element[10].toString()));
    }
    return hist;
  }

  private Date stringToDate(String string) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    java.util.Date parsed = null;
    try {
      parsed = format.parse(string);

      return new java.sql.Date(parsed.getTime());
    }
    catch (ParseException e) {
      LOG.error("Error durante el parseo de fechas", e);
      return null;
    }
  }

  private void rellenarFechaCambioYCliente(HistoricoDTO clienteDatosHistorico,
      List<HistoricoClienteDTO> listadoClienteHistoricoConFechaCambio) {
    HistoricoClienteDTO cliente = new HistoricoClienteDTO();
    // Primero comprobamos si tiene fechamod
    if (null != clienteDatosHistorico.getFechaMod()) {
      cliente.setFechaCambio(dateToString(clienteDatosHistorico.getFechaMod()));
    }
    // Si no tiene fechamod, guardamos en fechacambio su fechaalta
    else {
      cliente.setFechaCambio(dateToString(clienteDatosHistorico.getFechaAlta()));
    }

    // Asignamos el resto de valores del cliente
    cliente.setCliente(new HistoricoDTO(clienteDatosHistorico.getCliente(), clienteDatosHistorico.getKgr(),
        clienteDatosHistorico.getIdControlEfectivo(), clienteDatosHistorico.getDesControlEfectivo(),
        clienteDatosHistorico.getIdControlAnalisis(), clienteDatosHistorico.getDesControlAnalisis(),
        clienteDatosHistorico.getIdControlDocumentacion(), clienteDatosHistorico.getDesControlDocumentacion(),
        clienteDatosHistorico.getObservacion(), clienteDatosHistorico.getFechaAlta(),
        clienteDatosHistorico.getFechaMod()));

    // Añadimos a la lista
    listadoClienteHistoricoConFechaCambio.add(cliente);

  }

  private String dateToString(Date fechaMod) {
    DateFormat df = new SimpleDateFormat(Constantes.Fecha.FORMATO.value());
    return df.format(fechaMod);
  }

  /**
   * Caso año natural sin operar. Checks for each client_id if the amount is = 0
   * to update TMCT0_BLANQUEO_CLIENTE
   */
  public void procesoRecalculoCce(int previousYear) {
    try {
      // Obtenemos todos los clientes activos en el año
      final List<Object[]> activeClientsInYear = clientesDAO.findAllClientByYear(previousYear);

      // Iniciamos el proceso si hay clientes activos
      if (activeClientsInYear != null && !activeClientsInYear.isEmpty()) {

        // Iteramos para cada cliente
        for (Object[] resultList : activeClientsInYear) {
          final String idCliente = (String) resultList[0];
          final Integer idControlEfectivo = (Integer) resultList[1];

          // Validamos si el cliente con 'idCliente' es Reseach
          if (isClienteResearch(idCliente)) {
            // Si cliente Research -> Validamos si su idControlEfectivo sea
            // distinto de 1
            if (!idControlEfectivo.equals("1")) {
              // Al ser distinto de 1 actualizamos
              clientesDAO.updateClientIdControlEfectivo(idCliente, "1");
            }
          }
          else {
            // Si no Research -> Validar si ha operado en el año
            final List<Object[]> efectivoAcumuladoCliente = efectivoAcumulado.findCaseWithoutOperation(idCliente,
                previousYear);
            // Comprobamos si obtenemos resultados
            if (efectivoAcumuladoCliente == null || efectivoAcumuladoCliente.isEmpty()) {
              // Si no tenemos resultados, validamos si su idControlEfectivo es
              // 2
              if (!idControlEfectivo.equals("2")) {
                // Si el idControlEfectivo no es 2, actualizamos
                clientesDAO.updateClientIdControlEfectivo(idCliente, "2");
              }
            }
            else {
              // Si tenemos resultados, el cliente ha operado, y justamos CCE
              // por volumetría de operativa
              adjustmentCeeByOperatingVolume(efectivoAcumuladoCliente);
            }
          }
        }
      }
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
    }
  }

  /**
   * Caso ajuste por volumen de operativa
   */

  public void adjustmentCeeByOperatingVolume(List<Object[]> efectivoAcumuladoCliente) {
    try {
      for (Object[] resultList : efectivoAcumuladoCliente) {
        final String idCliente = resultList[0].toString();
        final BigDecimal maxImporteOperacion = (java.math.BigDecimal) resultList[1];
        final BigDecimal importeEfectivoMax = (java.math.BigDecimal) resultList[3];
        final double importeOperacion = maxImporteOperacion.intValue();
        final double importeEfectivo = importeEfectivoMax.intValue();
        if (importeOperacion > importeEfectivo) {
          final List<Object[]> caseAdjustmentByVolumeEffectiveControl = efectivoAcumulado
              .findAdjustmentByVolumeEffectiveControl(importeEfectivoMax);

          if (caseAdjustmentByVolumeEffectiveControl != null && !caseAdjustmentByVolumeEffectiveControl.isEmpty()) {
            for (Object[] resultList2 : caseAdjustmentByVolumeEffectiveControl) {
              String idControlEfectivo = resultList2[0].toString();
              Timestamp dischargeDate = new Timestamp(System.currentTimeMillis());
              efectivoAcumulado.updateAdjustmentByVolume(idControlEfectivo, (Timestamp) dischargeDate.clone(),
                  Session.USER_RECALCULO_CCE.value(), idCliente);
            }
          }
        }
      }
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
    }
  }

  /**
   * Verifica si un cliente es research, para ello todos los alias con Nivel4
   * deben tener el valor UNB
   * @param idCliente
   * @return
   */
  private Boolean isClienteResearch(final String idCliente) {
    Boolean isResearch = true;

    final List<Object[]> aliasList = clientesDAO.getAliasClient(idCliente);

    for (Object[] objects : aliasList) {
      final String cdnivel4 = (String) objects[2];
      isResearch &= cdnivel4.equals("UNB");
      if (!isResearch) {
        break;
      }
    }
    return isResearch;
  }

  public Tmct0cli getDatosKGR(String id) {
    Tmct0cli cli = new Tmct0cli();
    try {
      if (id != null) {
        List<Tmct0cli> cliListado = cliDAO.findBycdbrocli(id);

        if (cliListado != null) {
          cli = cliListado.get(0);
        }
      }
      return cli;
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      cli.setCdkgr("-");
      return cli;
    }
  }

  public List<ObservacionesDTO> getListadoObservacionesClientes(String idCliente) {

    List<ObservacionesDTO> listadoTotalObservaciones = new ArrayList<>();
    try {
      // Nos traemos la lista de observaciones del cliente especifico
      List<Object[]> listadoTotal = observacionClientesDAO.getListadoObservacionesClientes(idCliente);

      convertObjectToObservacionesDTO(listadoTotal, listadoTotalObservaciones);

    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
    }

    return listadoTotalObservaciones;
  }

  private void convertObjectToObservacionesDTO(List<Object[]> listadoTotal,
      List<ObservacionesDTO> listadoTotalObservaciones) {
    for (Object[] element : listadoTotal) {
      ObservacionesDTO obs = toDTOObservaciones(element);
      listadoTotalObservaciones.add(obs);
    }
  }

  private ObservacionesDTO toDTOObservaciones(Object[] element) {
    ObservacionesDTO obs = new ObservacionesDTO();
    if (null != element[0]) {
      obs.setIdObservacionCliente(element[0].toString());
    }
    if (null != element[1]) {
      obs.setIdCliente(element[1].toString());
    }
    if (null != element[2]) {
      obs.setFechaAlta(stringToDate(element[2].toString()));
    }
    if (null != element[3]) {
      obs.setIdControlAnalisis(element[3].toString());
    }
    if (null != element[4]) {
      obs.setDesControlAnalisis(element[4].toString());
    }
    if (null != element[5]) {
      obs.setIdObservacion(element[5].toString());
    }
    if (null != element[6]) {
      obs.setDesObservacion(element[6].toString());
    }

    return obs;
  }

  @Override
  public CallableResultDTO actionUpdate(BlanqueoClientesDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }
}
