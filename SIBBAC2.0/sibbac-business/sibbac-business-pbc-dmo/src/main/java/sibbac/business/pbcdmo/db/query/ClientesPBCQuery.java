package sibbac.business.pbcdmo.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class ClientesPBCQuery.
 */
@Component("ClientesPBCQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ClientesPBCQuery extends AbstractDynamicPageQuery {

  /** The Constant ID_CLIENTE. */
  private static final String ID_CLIENTE = "blacli.ID_CLIENTE";

  /** The Constant CDKGR. */
  private static final String CDKGR = "cli.CDKGR";

  /** The Constant ACTIVO. */
  private static final String ACTIVO = "ACTIVO";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT DISTINCT blacli.ID_CLIENTE as IDCLIENTE, cli.CDKGR as CODKGR,"
      + " efe.ID as IDEFE, efe.DESCRIPCION as DESEFE, doc.ID as IDDOC, doc.DESCRIPCION as DESDOC,"
      + " ana.ID as IDANA, ana.DESCRIPCION as DESANA, bsnbpsql.TMCT0_BLANQUEO_OBSERVACION.DESCRIPCION DESOBS,"
      + " blacli.activo as ACTIVO ";

  /** The Constant FROM. */
  private static final String FROM = "FROM bsnbpsql.TMCT0_BLANQUEO_CLIENTE blacli"
      + "    INNER JOIN bsnbpsql.TMCT0CLI cli on cli.CDBROCLI = blacli.ID_CLIENTE"
      + "    LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_EFECTIVO efe on efe.ID = blacli.ID_CONTROL_EFECTIVO"
      + "    LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_DOCUMENTACION doc on doc.ID = blacli.ID_CONTROL_DOCUMENTACION"
      + "    LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_CONTROL_ANALISIS ana on ana.ID = blacli.ID_CONTROL_ANALISIS"
      + "    LEFT JOIN bsnbpsql.TMCT0_BLANQUEO_OBSERVACION on blacli.ID_OBSERVACION = bsnbpsql.TMCT0_BLANQUEO_OBSERVACION.ID";

  /** The Constant WHERE. */
  private static final String WHERE = "WHERE blacli.FECHA_BAJA is null ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("Cliente", "IDCLIENTE"),
      new DynamicColumn("Código KGR", "CODKGR"), new DynamicColumn("Control efectivo", "DESEFE"),
      new DynamicColumn("Control documentación", "DESDOC"), new DynamicColumn("Control análisis", "DESANA"),
      new DynamicColumn("Observación", "DESOBS") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter(ID_CLIENTE, "Cliente"));
    res.add(new StringDynamicFilter(CDKGR, "Código KGR"));
    res.add(new StringDynamicFilter(ACTIVO, "Activo"));
    return res;
  }

  public ClientesPBCQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public ClientesPBCQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public ClientesPBCQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}
