package sibbac.business.pbcdmo.db.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.pbcdmo.db.model.common.BlanqueoControlEntity;
import sibbac.database.DBConstants;

/**
 * The Class TMCT0BlanqueoControlEfectivoEntity.
 */
@Entity
@Table(name = DBConstants.PBCDMO.TMCT0_BLANQUEO_CONTROL_EFECTIVO)
public class BlanqueoControlEfectivoEntity extends BlanqueoControlEntity {

  /** The id. */
  @Id
  @Column(name = "ID", length = 10, nullable = false)
  private Integer id;

  /** The descripcion. */
  @Column(name = "DESCRIPCION", length = 256, nullable = false)
  private String descripcion;

  /** The num titulos max. */
  @Column(name = "NUMERO_TITULOS_MAX", length = 10)
  private Integer numTitulosMax;

  /** The importe efectivo max. */
  @Column(name = "IMPORTE_EFECTIVO_MAX", length = 31)
  private BigDecimal importeEfectivoMax;

  /** The per titulos max. */
  @Column(name = "PERCENT_TITULOS_MAX", length = 5)
  private BigDecimal perTitulosMax;

  /** The per efectivo max. */
  @Column(name = "PERCENT_EFECTIVO_MAX", length = 32)
  private BigDecimal perEfectivoMax;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Gets the descripcion.
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * Sets the descripcion.
   *
   * @param descripcion the new descripcion
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Gets the num titulos max.
   *
   * @return the num titulos max
   */
  public Integer getNumTitulosMax() {
    return numTitulosMax;
  }

  /**
   * Sets the num titulos max.
   *
   * @param numTitulosMax the new num titulos max
   */
  public void setNumTitulosMax(Integer numTitulosMax) {
    this.numTitulosMax = numTitulosMax;
  }

  /**
   * Gets the importe efectivo max.
   *
   * @return the importe efectivo max
   */
  public BigDecimal getImporteEfectivoMax() {
    return importeEfectivoMax;
  }

  /**
   * Sets the importe efectivo max.
   *
   * @param importeEfectivoMax the new importe efectivo max
   */
  public void setImporteEfectivoMax(BigDecimal importeEfectivoMax) {
    this.importeEfectivoMax = importeEfectivoMax;
  }

  /**
   * Gets the per titulos max.
   *
   * @return the per titulos max
   */
  public BigDecimal getPerTitulosMax() {
    return perTitulosMax;
  }

  /**
   * Sets the per titulos max.
   *
   * @param perTitulosMax the new per titulos max
   */
  public void setPerTitulosMax(BigDecimal perTitulosMax) {
    this.perTitulosMax = perTitulosMax;
  }

  /**
   * Gets the per efectivo max.
   *
   * @return the per efectivo max
   */
  public BigDecimal getPerEfectivoMax() {
    return perEfectivoMax;
  }

  /**
   * Sets the per efectivo max.
   *
   * @param perEfectivoMax the new per efectivo max
   */
  public void setPerEfectivoMax(BigDecimal perEfectivoMax) {
    this.perEfectivoMax = perEfectivoMax;
  }

  /**
   * Gets the fecha alta.
   *
   * @return the fecha alta
   */
  public Timestamp getFechaAlta() {
    return fechaAlta != null ? (Timestamp) fechaAlta.clone() : null;
  }

  /**
   * Sets the fecha alta.
   *
   * @param fechaAlta the new fecha alta
   */
  public void setFechaAlta(Timestamp fechaAlta) {
    this.fechaAlta = (Timestamp) fechaAlta.clone();
  }

  /**
   * Gets the fecha mod.
   *
   * @return the fecha mod
   */
  public Timestamp getFechaMod() {
    return fechaMod != null ? (Timestamp) fechaMod.clone() : null;
  }

  /**
   * Sets the fecha mod.
   *
   * @param fechaMod the new fecha mod
   */
  public void setFechaMod(Timestamp fechaMod) {
    this.fechaMod = (Timestamp) fechaMod.clone();
  }

  /**
   * Gets the fecha baja.
   *
   * @return the fecha baja
   */
  public Timestamp getFechaBaja() {
    return fechaBaja != null ? (Timestamp) fechaBaja.clone() : null;
  }

  /**
   * Sets the fecha baja.
   *
   * @param fechaBaja the new fecha baja
   */
  public void setFechaBaja(Timestamp fechaBaja) {
    this.fechaBaja = (Timestamp) fechaBaja.clone();
  }

  /**
   * Gets the usuario alta.
   *
   * @return the usuario alta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * Sets the usuario alta.
   *
   * @param usuarioAlta the new usuario alta
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * Gets the usuario mod.
   *
   * @return the usuario mod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * Sets the usuario mod.
   *
   * @param usuarioMod the new usuario mod
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * Gets the usuario baja.
   *
   * @return the usuario baja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * Sets the usuario baja.
   *
   * @param usuarioBaja the new usuario baja
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "TMCT0BlanqueoControlEfectivoEntity [id=" + id + ", descripcion=" + descripcion + ", numTitulosMax="
        + numTitulosMax + ", importeEfectivoMax=" + importeEfectivoMax + ", perTitulosMax=" + perTitulosMax
        + ", perEfectivoMax=" + perEfectivoMax + ", fechaAlta=" + fechaAlta + ", fechaMod=" + fechaMod + ", fechaBaja="
        + fechaBaja + ", usuarioAlta=" + usuarioAlta + ", usuarioMod=" + usuarioMod + ", usuarioBaja=" + usuarioBaja
        + "]";
  }

}