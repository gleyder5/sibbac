
package sibbac.business.pbcdmo.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.pbcdmo.db.model.common.BlanqueoControlEntity;
import sibbac.database.DBConstants;

/**
 * The Class BlanqueoObservacionClienteEntity.
 */
@Entity
@Table(name = DBConstants.PBCDMO.TMCT0_BLANQUEO_OBSERVACION_CLIENTE)
public class BlanqueoObservacionClienteEntity extends BlanqueoControlEntity {

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", nullable = false)
  private Long id;

  /** The idCliente. */
  @Column(name = "ID_CLIENTE", length = 20, nullable = false)
  private String idCliente;

  /** The id observacion. */
  @Column(name = "ID_OBSERVACION", length = 3, nullable = false)
  private String idObservacion;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIdCliente() {
    return idCliente;
  }

  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  public String getIdObservacion() {
    return idObservacion;
  }

  public void setIdObservacion(String idObservacion) {
    this.idObservacion = idObservacion;
  }
}
