package sibbac.business.pbcdmo.web.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import sibbac.business.pbcdmo.db.model.BlanqueoEfectivoAcumuladoEntity;
import sibbac.business.pbcdmo.web.dto.common.AbstractControlDTO;

public class BlanqueoControlEfectivoAcumuladoDTO extends AbstractControlDTO<BlanqueoEfectivoAcumuladoEntity> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private BigInteger id;

  private String idCliente;

  private int mes;

  private int anio;

  private BigDecimal titulosAcumulados;

  private BigDecimal importeAcumulado;

  private Date fechaUltimaOperacionProc;

  public BlanqueoControlEfectivoAcumuladoDTO() {
    super();
  }

  public BlanqueoControlEfectivoAcumuladoDTO(String idCliente, int mes, int anio, BigDecimal titulosAcumulados,
      BigDecimal importeAcumulado, Date fechaUltimaOperacionProc, Date fechaAlta, Date fechaMod, Date fechaBaja,
      String usuarioAlta, String usuarioMod, String usuarioBaja) {
    super(fechaAlta, fechaMod, fechaBaja, usuarioAlta, usuarioMod, usuarioBaja);
    this.idCliente = idCliente;
    this.mes = mes;
    this.anio = anio; 
    this.titulosAcumulados = titulosAcumulados;
    this.importeAcumulado = importeAcumulado;
    this.fechaUltimaOperacionProc = (fechaUltimaOperacionProc != null ? (Date) fechaUltimaOperacionProc.clone() : null);
  }

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getIdCliente() {
    return idCliente;
  }

  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  public int getMes() {
    return mes;
  }

  public void setMes(int mes) {
    this.mes = mes;
  }

  public int getAnio() {
    return anio;
  }

  public void setAnio(int anio) {
    this.anio = anio;
  }

  public BigDecimal getTitulosAcumulados() {
    return titulosAcumulados;
  }

  public void setTitulosAcumulados(BigDecimal titulosAcumulados) {
    this.titulosAcumulados = titulosAcumulados;
  }

  public BigDecimal getImporteAcumulado() {
    return importeAcumulado;
  }

  public void setImporteAcumulado(BigDecimal importeAcumulado) {
    this.importeAcumulado = importeAcumulado;
  }

  public Date getFechaUltimaOperacionProc() {
    return fechaUltimaOperacionProc != null ? (Date) fechaUltimaOperacionProc.clone() : null;
  }

  public void setFechaUltimaOperacionProc(Date fechaUltimaOperacionProc) {
    this.fechaUltimaOperacionProc = (fechaUltimaOperacionProc != null ? (Date) fechaUltimaOperacionProc.clone() : null);
  }


  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  @Override
  protected void toThisDto(BlanqueoEfectivoAcumuladoEntity entity) {

  }

}
