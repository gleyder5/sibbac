package sibbac.business.pbcdmo.web.rest;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.pbcdmo.bo.BaseBo;
import sibbac.business.pbcdmo.db.query.ControlEfectivosPorVolumenQuery;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlEfectivoDTO;
import sibbac.common.CallableResultDTO;

/**
 * The Class SIBBACServiceEfectivoCCE.
 */
@RestController
@RequestMapping("/datosMaestros/controlEfectivo")
public class ControlEfectivoCCEService extends BaseService {
  
  protected static final String DEFAULT_CHARSET = CharEncoding.UTF_8;

  protected static final String DEFAULT_APPLICATION_TYPE = MediaType.APPLICATION_JSON + ";charset=" + DEFAULT_CHARSET;

  @Autowired
  private BaseBo<BlanqueoControlEfectivoDTO> controlEfectivoBo;

  /**
   * Construcctor por defecto
   */
  public ControlEfectivoCCEService() {
    super("Control de efectivos por volumen", "controlEfectivosPorVolumen",
        ControlEfectivosPorVolumenQuery.class.getSimpleName());
  }

  /**
   * 
   * @param solicitud
   * @return
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody BlanqueoControlEfectivoDTO solicitud) {
    return controlEfectivoBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody BlanqueoControlEfectivoDTO solicitud) {
    return controlEfectivoBo.actionUpdate(solicitud);
  }
  
  /**
   * Execute action delete List
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/{ids}", method = RequestMethod.DELETE)
  public CallableResultDTO executeActionDelete(@PathVariable("ids") List<String> solicitud) {
    return controlEfectivoBo.actionDelete(solicitud);
  }
}
