package sibbac.business.pbcdmo.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.pbcdmo.bo.BaseBo;
import sibbac.business.pbcdmo.bo.BlanqueoClientesBo;
import sibbac.business.pbcdmo.bo.BlanqueoControlEfectivoBo;
import sibbac.business.pbcdmo.db.query.ClientesPBCQuery;
import sibbac.business.pbcdmo.web.dto.AnalisisObservacionDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoClientesDTO;
import sibbac.business.pbcdmo.web.dto.BlanqueoControlEfectivoDTO;
import sibbac.business.pbcdmo.web.dto.ClientHistDTO;
import sibbac.business.pbcdmo.web.dto.HistoricoClienteDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.dto.ObservacionesDTO;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.FilterInfo;

/**
 * The Class SIBBACServiceParaisosFiscales.
 */
@RestController
@RequestMapping("/gestionClientes/configuracionClientesPBC")
public class ClientesPBCService extends BaseService {

  /** The paraisos fiscales bo. */
  @Autowired
  private BaseBo<BlanqueoClientesDTO> clientesBo;

  /** The blanqueo clientes bo. */
  @Autowired
  private BlanqueoClientesBo blanqueoClientesBo;

  /** The efectivo bo. */
  @Autowired
  private BlanqueoControlEfectivoBo efectivoBo;

  /**
   * Instantiates a new clientes PBC service.
   */
  public ClientesPBCService() {
    super("Configuración de Clientes PBC", "configuracionClientesPBC", ClientesPBCQuery.class.getSimpleName());
  }

  @RequestMapping(value = "/{name}/filters", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<FilterInfo> getFilters(@PathVariable("name") String name) throws SIBBACBusinessException {

    try {

      final AbstractDynamicPageQuery query;

      query = getDynamicQuery(name);
      return query.getFiltersInfo();
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error obteniendo filtros", e);
    }
  }

  /**
   * Execute action post.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody BlanqueoClientesDTO solicitud) {
    return clientesBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionPut(@RequestBody ObservacionClienteDTO solicitud) {
    return clientesBo.update(solicitud);
  }

  /**
   * Execute action delete List.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/delete", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionDelete(@RequestBody List<String> solicitud) {
    return clientesBo.actionDelete(solicitud);
  }

  /**
   * Gets the dynamic query.
   *
   * @param name the name
   * @return the dynamic query
   * @throws Exception the exception
   */
  private AbstractDynamicPageQuery getDynamicQuery(String name) throws SIBBACBusinessException {
    return ctx.getBean(name, AbstractDynamicPageQuery.class);
  }

  /**
   * Método que devuelve el listado de control de efectivos.
   *
   * @return the list efectivo
   */
  @RequestMapping(value = "/listadoEfectivo", method = RequestMethod.GET)
  public List<LabelValueObject> getListEfectivo() {
    return blanqueoClientesBo.getListEfectivo();
  }

  /**
   * Método que devuelve el listado de control de analisis.
   *
   * @return //
   */
  @RequestMapping(value = "/listadoAnalisis", method = RequestMethod.GET)
  public List<LabelValueObject> getListAnalisis() {
    return blanqueoClientesBo.getListAnalisis();
  }

  /**
   * Método que devuelve el listado de control de documentos.
   *
   * @return the list documentos
   */
  @RequestMapping(value = "/listadoDocumentos", method = RequestMethod.GET)
  public List<LabelValueObject> getListDocumentos() {
    return blanqueoClientesBo.getListDocumentos();
  }

  /**
   * Método que devuelve el listado de observaciones.
   *
   * @return the listado combo observaciones
   */
  @RequestMapping(value = "/listadoComboObservaciones", method = RequestMethod.GET)
  public List<LabelValueObject> getlistadoComboObservaciones() {
    return blanqueoClientesBo.getlistadoComboObservaciones();
  }

  /**
   * Gets the control analisis.
   *
   * @param solicitud the solicitud
   * @return the control analisis
   */
  @RequestMapping(value = "/getControlAnalisis/{id}", method = RequestMethod.GET)
  public AnalisisObservacionDTO getControlAnalisis(@PathVariable("id") String solicitud) {
    return blanqueoClientesBo.getControlAnalisis(solicitud);
  }

  /**
   * Método que actualiza masivamente la fecha de revisión.
   *
   * @return the callable result DTO
   */
  @RequestMapping(value = "/updateFechaRevision", method = RequestMethod.PUT)
  public CallableResultDTO updateFechaRevision() {
    return blanqueoClientesBo.updateFechaRevision();
  }

  /**
   * Gets the listado historico.
   *
   * @param idCliente the id cliente
   * @return the listado historico
   */
  @RequestMapping(value = "/listadoHistorico", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public List<HistoricoClienteDTO> getListadoHistorico(@RequestBody ClientHistDTO cliente) {
    return blanqueoClientesBo.getListadoHistorico(cliente.getIdcliente());
  }

  /**
   * Gets the datos efectivo.
   *
   * @param id the id
   * @return the datos efectivo
   */
  @RequestMapping(value = "/getDatosEfectivo/{id}", method = RequestMethod.GET)
  public BlanqueoControlEfectivoDTO getDatosEfectivo(@PathVariable("id") String id) {
    return efectivoBo.getDatosEfectivo(id);
  }

  /**
   * Gets the datos KGR.
   *
   * @param id the id
   * @return the datos KGR
   */
  @RequestMapping(value = "/getDatosKGR", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public Tmct0cli getDatosKGR(@RequestBody BlanqueoClientesDTO cliente) {
    return blanqueoClientesBo.getDatosKGR(cliente.getId());
  }

  /**
   * Gets the listado observaciones clientes.
   *
   * @param idCliente the id cliente
   * @return the listado observaciones clientes
   */
  @RequestMapping(value = "/listadoObservacionesCliente", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public List<ObservacionesDTO> getListadoObservacionesClientes(@RequestBody BlanqueoClientesDTO cliente) {
    return blanqueoClientesBo.getListadoObservacionesClientes(cliente.getId());
  }
}
