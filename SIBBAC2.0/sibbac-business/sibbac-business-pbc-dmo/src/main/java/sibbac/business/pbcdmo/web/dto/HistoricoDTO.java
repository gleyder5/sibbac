package sibbac.business.pbcdmo.web.dto;

import java.sql.Date;

/**
 * The Class HistoricoDTO.
 */
public class HistoricoDTO {

  /** The cliente. */
  private String cliente;

  /** The kgr. */
  private String kgr;

  /** The id control efectivo. */
  private Integer idControlEfectivo;

  /** The control efectivo. */
  private String desControlEfectivo;

  /** The id control analisis. */
  private String idControlAnalisis;

  /** The control analisis. */
  private String desControlAnalisis;

  /** The id control documentacion. */
  private String idControlDocumentacion;

  /** The control documentacion. */
  private String desControlDocumentacion;

  /** The observacion. */
  private String observacion;

  /** The fecha alta. */
  private Date fechaAlta;

  /** The fecha baja. */
  private Date fechaMod;

  /**
   * Instantiates a new historico DTO.
   *
   * @param cliente the cliente
   * @param kgr the kgr
   * @param idControlEfectivo the id control efectivo
   * @param desControlEfectivo the des control efectivo
   * @param idControlAnalisis the id control analisis
   * @param desControlAnalisis the des control analisis
   * @param idControlDocumentacion the id control documentacion
   * @param desControlDocumentacion the des control documentacion
   * @param observacion the observacion
   * @param fechaAlta the fecha alta
   * @param fechaMod the fecha mod
   */
  public HistoricoDTO(String cliente, String kgr, Integer idControlEfectivo, String desControlEfectivo,
      String idControlAnalisis, String desControlAnalisis, String idControlDocumentacion,
      String desControlDocumentacion, String observacion, Date fechaAlta, Date fechaMod) {
    super();
    this.cliente = cliente;
    this.kgr = kgr;
    this.idControlEfectivo = idControlEfectivo;
    this.desControlEfectivo = desControlEfectivo;
    this.idControlAnalisis = idControlAnalisis;
    this.desControlAnalisis = desControlAnalisis;
    this.idControlDocumentacion = idControlDocumentacion;
    this.desControlDocumentacion = desControlDocumentacion;
    this.observacion = observacion;
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);
    this.fechaMod = (fechaMod != null ? (Date) fechaMod.clone() : null);
  }

  /**
   * Instantiates a new historico DTO.
   */
  public HistoricoDTO() {
    super();
  }

  /**
   * Gets the cliente.
   *
   * @return the cliente
   */
  public String getCliente() {
    return cliente;
  }

  /**
   * Sets the cliente.
   *
   * @param cliente the new cliente
   */
  public void setCliente(String cliente) {
    this.cliente = cliente;
  }

  /**
   * Gets the kgr.
   *
   * @return the kgr
   */
  public String getKgr() {
    return kgr;
  }

  /**
   * Sets the kgr.
   *
   * @param kgr the new kgr
   */
  public void setKgr(String kgr) {
    this.kgr = kgr;
  }

  /**
   * Gets the id control efectivo.
   *
   * @return the id control efectivo
   */
  public Integer getIdControlEfectivo() {
    return idControlEfectivo;
  }

  /**
   * Sets the id control efectivo.
   *
   * @param idControlEfectivo the new id control efectivo
   */
  public void setIdControlEfectivo(Integer idControlEfectivo) {
    this.idControlEfectivo = idControlEfectivo;
  }

  /**
   * Gets the des control efectivo.
   *
   * @return the des control efectivo
   */
  public String getDesControlEfectivo() {
    return desControlEfectivo;
  }

  /**
   * Sets the des control efectivo.
   *
   * @param desControlEfectivo the new des control efectivo
   */
  public void setDesControlEfectivo(String desControlEfectivo) {
    this.desControlEfectivo = desControlEfectivo;
  }

  /**
   * Gets the id control analisis.
   *
   * @return the id control analisis
   */
  public String getIdControlAnalisis() {
    return idControlAnalisis;
  }

  /**
   * Sets the id control analisis.
   *
   * @param idControlAnalisis the new id control analisis
   */
  public void setIdControlAnalisis(String idControlAnalisis) {
    this.idControlAnalisis = idControlAnalisis;
  }

  /**
   * Gets the des control analisis.
   *
   * @return the des control analisis
   */
  public String getDesControlAnalisis() {
    return desControlAnalisis;
  }

  /**
   * Sets the des control analisis.
   *
   * @param desControlAnalisis the new des control analisis
   */
  public void setDesControlAnalisis(String desControlAnalisis) {
    this.desControlAnalisis = desControlAnalisis;
  }

  /**
   * Gets the id control documentacion.
   *
   * @return the id control documentacion
   */
  public String getIdControlDocumentacion() {
    return idControlDocumentacion;
  }

  /**
   * Sets the id control documentacion.
   *
   * @param idControlDocumentacion the new id control documentacion
   */
  public void setIdControlDocumentacion(String idControlDocumentacion) {
    this.idControlDocumentacion = idControlDocumentacion;
  }

  /**
   * Gets the des control documentacion.
   *
   * @return the des control documentacion
   */
  public String getDesControlDocumentacion() {
    return desControlDocumentacion;
  }

  /**
   * Sets the des control documentacion.
   *
   * @param desControlDocumentacion the new des control documentacion
   */
  public void setDesControlDocumentacion(String desControlDocumentacion) {
    this.desControlDocumentacion = desControlDocumentacion;
  }

  /**
   * Gets the observacion.
   *
   * @return the observacion
   */
  public String getObservacion() {
    return observacion != null ? observacion : null;
  }

  /**
   * Sets the observacion.
   *
   * @param observacion the new observacion
   */
  public void setObservacion(String observacion) {
    this.observacion = observacion;
  }

  /**
   * Gets the fecha alta.
   *
   * @return the fecha alta
   */
  public Date getFechaAlta() {
    return fechaAlta != null ? (Date) fechaAlta.clone() : null;
  }

  /**
   * Sets the fecha alta.
   *
   * @param fechaAlta the new fecha alta
   */
  public void setFechaAlta(Date fechaAlta) {
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);
  }

  /**
   * Gets the fecha mod.
   *
   * @return the fecha mod
   */
  public Date getFechaMod() {
    return fechaMod != null ? (Date) fechaMod.clone() : null;
  }

  /**
   * Sets the fecha mod.
   *
   * @param fechaMod the new fecha mod
   */
  public void setFechaMod(Date fechaMod) {
    this.fechaMod = (fechaMod != null ? (Date) fechaMod.clone() : null);
  }
}
