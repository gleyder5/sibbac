package sibbac.business.pbcdmo.db.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.business.pbcdmo.db.model.common.BlanqueoControlEntity;
import sibbac.database.DBConstants;

/**
 * The Class BlanqueoEfectivoAcumuladoEntity.
 */
@Table(name = DBConstants.PBCDMO.TMCT0_BLANQUEO_EFECTIVO_ACUMULADO)
@Entity
public class BlanqueoEfectivoAcumuladoEntity extends BlanqueoControlEntity {
  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", nullable = false, unique = true, length = 8)
  private BigInteger id;

  /** The id cliente. */
  @Column(name = "ID_CLIENTE", nullable = false, length = 20)
  private String idCliente;

  /** The mes. */
  @Column(name = "MES", nullable = false, length = 4)
  private int mes;

  /** The anio. */
  @Column(name = "ANIO", nullable = false, length = 4)
  private int anio;

  /** The titulos acumulados. */
  @Column(name = "TITULOS_ACUMULADOS", nullable = false, length = 4)
  private BigDecimal titulosAcumulados;

  /** The importe acumulado. */
  @Column(name = "IMPORTE_ACUMULADO", nullable = false)
  private BigDecimal importeAcumulado;

  /** The fecha última operación. */
  @Column(name = "FECHA_ULTIMA_OPERACION_PROC", nullable = false)
  private Date fechaUltimaOperacionProc;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * Gets the id cliente.
   *
   * @return the id cliente
   */
  public String getIdCliente() {
    return idCliente;
  }

  /**
   * Sets the id cliente.
   *
   * @param idCliente the new id cliente
   */
  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  /**
   * Gets the mes.
   *
   * @return the mes
   */
  public int getMes() {
    return mes;
  }

  /**
   * Sets the mes.
   *
   * @param mes the new mes
   */
  public void setMes(int mes) {
    this.mes = mes;
  }

  /**
   * Gets the anio.
   *
   * @return the anio
   */
  public int getAnio() {
    return anio;
  }

  /**
   * Sets the anio.
   *
   * @param anio the new anio
   */
  public void setAnio(int anio) {
    this.anio = anio;
  }

  public BigDecimal getTitulosAcumulados() {
    return titulosAcumulados;
  }

  public void setTitulosAcumulados(BigDecimal titulosAcumulados) {
    this.titulosAcumulados = titulosAcumulados;
  }

  public BigDecimal getImporteAcumulado() {
    return importeAcumulado;
  }

  public void setImporteAcumulado(BigDecimal importeAcumulado) {
    this.importeAcumulado = importeAcumulado;
  }

  /**
   * Gets the fecha ultima operacion proc.
   *
   * @return the fecha ultima operacion proc
   */
  public Date getFechaUltimaOperacionProc() {
    return fechaUltimaOperacionProc != null ? (Date) fechaUltimaOperacionProc.clone() : null;
  }

  /**
   * Sets the fecha ultima operacion proc.
   *
   * @param fechaUltimaOperacionProc the new fecha ultima operacion proc
   */
  public void setFechaUltimaOperacionProc(Date fechaUltimaOperacionProc) {
    this.fechaUltimaOperacionProc = (Date) fechaUltimaOperacionProc.clone();
  }

}
