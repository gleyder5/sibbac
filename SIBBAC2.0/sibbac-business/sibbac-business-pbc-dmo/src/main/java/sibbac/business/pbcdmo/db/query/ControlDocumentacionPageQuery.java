package sibbac.business.pbcdmo.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DBConstants;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;

/**
 * The Class ControlDocumentacionPageQuery.
 */
@Component("ControlDocumentacionPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ControlDocumentacionPageQuery extends AbstractDynamicPageQuery {

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT "
      + "ID, DESCRIPCION, FECHA_ALTA, FECHA_MOD, FECHA_BAJA, USUARIO_ALTA, USUARIO_MOD, USUARIO_BAJA";

  /** The Constant FROM. */
  private static final String FROM = "FROM " + DBConstants.PBCDMO.TMCT0_BLANQUEO_CONTROL_DOCUMENTACION;

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE FECHA_BAJA IS NULL ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("ID", "ID"),
      new DynamicColumn("Descripción", "DESCRIPCION") };

  /**
   * Make filters.
   *
   * @return the list
   */
  private static List<SimpleDynamicFilter<?>> makeFilters() {
    return new ArrayList<>();
  }

  /**
   * Instantiates a new control documentacion page query.
   */
  public ControlDocumentacionPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {

    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }

}
