package sibbac.business.pbcdmo.bo;

import java.util.List;

import sibbac.business.pbcdmo.web.dto.ObservacionClienteDTO;
import sibbac.business.pbcdmo.web.dto.common.AbstractDTO;
import sibbac.common.CallableResultDTO;

public interface BaseBo<T extends AbstractDTO> {

  public CallableResultDTO actionFindById(T solicitud);

  public CallableResultDTO actionSave(T solicitud);

  public CallableResultDTO actionUpdate(T solicitud);

  public CallableResultDTO actionDelete(List<String> solicitud);

  CallableResultDTO update(ObservacionClienteDTO solicitud);

}
