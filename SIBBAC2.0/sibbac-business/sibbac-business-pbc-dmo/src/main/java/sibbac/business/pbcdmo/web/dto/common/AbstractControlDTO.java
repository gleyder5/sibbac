package sibbac.business.pbcdmo.web.dto.common;

import java.util.Date;

import sibbac.business.pbcdmo.db.model.common.BlanqueoControlEntity;

public abstract class AbstractControlDTO<T extends BlanqueoControlEntity> extends AbstractDTO {

  /**
   * Gerenated Serial UID
   */
  private static final long serialVersionUID = 7255154036045826621L;

  /** The fechaalta. */
  protected Date fechaAlta;

  /** The fechamod. */
  protected Date fechaMod;

  /** The fechabaja. */
  protected Date fechaBaja;

  /** The usuarioalta. */
  protected String usuarioAlta;

  /** The usuariomod. */
  protected String usuarioMod;

  /** The usuariobaja. */
  protected String usuarioBaja;

  /**
   * Constructor por defecto
   */
  protected AbstractControlDTO() {
    super();
  }

  /**
   * Constructor por parametros
   * @param activo
   * @param fechaAlta
   * @param fechaMod
   * @param fechaBaja
   * @param usuarioAlta
   * @param usuarioMod
   * @param usuarioBaja
   */
  protected AbstractControlDTO(Date fechaAlta, Date fechaMod, Date fechaBaja, String usuarioAlta, String usuarioMod,
      String usuarioBaja) {
    super();   
    this.usuarioAlta = usuarioAlta;
    this.usuarioMod = usuarioMod;
    this.usuarioBaja = usuarioBaja;
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);
    this.fechaMod = (fechaMod != null ? (Date) fechaMod.clone() : null);
    this.fechaMod = (fechaMod != null ? (Date) fechaMod.clone() : null);
  }

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Date getFechaAlta() {
    return fechaAlta != null ? (Date) fechaAlta.clone() : null;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaAlta(Date fechaAlta) {
    this.fechaAlta = (fechaAlta != null ? (Date) fechaAlta.clone() : null);
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Date getFechaMod() {
    return fechaMod != null ? (Date) fechaMod.clone() : null;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechaMod(Date fechaMod) {
    this.fechaMod = (fechaMod != null ? (Date) fechaMod.clone() : null);
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Date getFechaBaja() {
    return fechaBaja != null ? (Date) fechaBaja.clone() : null;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechaBaja(Date fechaBaja) {
    this.fechaBaja = (fechaBaja != null ? (Date) fechaBaja.clone() : null);
  }

  /**
   * Gets the usuarioalta.
   *
   * @return the usuarioalta
   */
  public String getUsuarioAlta() {
    return usuarioAlta;
  }

  /**
   * Sets the usuarioalta.
   *
   * @param usuarioalta the new usuarioalta
   */
  public void setUsuarioAlta(String usuarioAlta) {
    this.usuarioAlta = usuarioAlta;
  }

  /**
   * Gets the usuariomod.
   *
   * @return the usuariomod
   */
  public String getUsuarioMod() {
    return usuarioMod;
  }

  /**
   * Sets the usuariomod.
   *
   * @param usuariomod the new usuariomod
   */
  public void setUsuarioMod(String usuarioMod) {
    this.usuarioMod = usuarioMod;
  }

  /**
   * Gets the usuariobaja.
   *
   * @return the usuariobaja
   */
  public String getUsuarioBaja() {
    return usuarioBaja;
  }

  /**
   * Sets the usuariobaja.
   *
   * @param usuariobaja the new usuariobaja
   */
  public void setUsuarioBaja(String usuarioBaja) {
    this.usuarioBaja = usuarioBaja;
  }

  /**
   * Establece los datos por defecto de Auditoria
   * @param entity
   */
  protected void toDto(T entity) {
    this.toThisDto(entity);
    this.fechaAlta = entity.getFechaAlta();
    this.fechaMod = entity.getFechaMod();
    this.fechaBaja = entity.getFechaBaja();
    this.usuarioAlta = entity.getUsuarioAlta();
    this.usuarioMod = entity.getUsuarioMod();
    this.usuarioBaja = entity.getUsuarioBaja();
  }

  /**
   * Convierte la entidad en un DTO
   * @param entity
   */
  protected abstract void toThisDto(T entity);

}
