package sibbac.business.pbcdmo.db.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.pbcdmo.db.model.BlanqueoObservacionClienteEntity;

@Repository
public interface BlanqueoObservacionClienteDAO extends JpaRepository<BlanqueoObservacionClienteEntity, Long> {

  @Query(value = "select obscli.id, obscli.ID_CLIENTE, obscli.FECHA_ALTA, obs.ID_CONTROL_ANALISIS, ana.DESCRIPCION as DescripcionAnalisis, "
      + "obscli.ID_OBSERVACION obscli, obs.DESCRIPCION as DescripcionObservacion "
      + "FROM BSNBPSQL.TMCT0_BLANQUEO_OBSERVACION_CLIENTE obscli "
      + "INNER JOIN BSNBPSQL.TMCT0_BLANQUEO_OBSERVACION obs on obs.ID = obscli.ID_OBSERVACION "
      + "INNER JOIN BSNBPSQL.TMCT0_BLANQUEO_CONTROL_ANALISIS ana on ana.ID = obs.ID_CONTROL_ANALISIS "
      + "WHERE obscli.ID_CLIENTE = ?1 AND obscli.fecha_baja is null order by ID", nativeQuery = true)
  List<Object[]> getListadoObservacionesClientes(String idCliente);

  @Transactional
  @Modifying(clearAutomatically = false)
  @Query(value = "UPDATE BSNBPSQL.TMCT0_BLANQUEO_OBSERVACION_CLIENTE obscli set obscli.fecha_Baja = :fechaBaja, obscli.usuario_Baja = :usuarioBaja "
      + "WHERE obscli.ID_CLIENTE = :idCliente and obscli.fecha_baja is null", nativeQuery = true)
  void updateObservacionClienteSetFechaBajaAndUsuarioBajaByIdClienteAndFechaBajaNull(@Param("fechaBaja") Date fecActual,
      @Param("usuarioBaja") String usuarioBaja, @Param("idCliente") String idCliente);
}
