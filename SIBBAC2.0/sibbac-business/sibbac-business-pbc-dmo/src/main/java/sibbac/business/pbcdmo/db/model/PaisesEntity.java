
package sibbac.business.pbcdmo.db.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;

/**
 * The Class TMCT0PaisesEntity.
 */
@Table(name = DBConstants.PBCDMO.TMCT0_PAISES)
@Entity
public class PaisesEntity {

  /** The nbpais. */
  @EmbeddedId
  private PaisesPK primaryKey;

  /** The nbpais. */
  @Column(name = "NBPAIS", length = 100, nullable = false, unique = true)
  private String nbpais;

  /** The cdhacienda. */
  @Column(name = "CDHACIENDA", length = 6, nullable = true)
  private String cdhacienda;

  /** The cdcodkgr. */
  @Column(name = "CDCODKGR", length = 6, nullable = true)
  private String cdcodkgr;

  /** The cdestado. */
  @Column(name = "CDESTADO", length = 1, nullable = false)
  private String cdestado;

  /** The audit date. */
  @Column(name = "AUDIT_DATE", length = 26, nullable = true)
  private Timestamp auditDate;

  /** The audit user. */
  @Column(name = "AUDIT_USER", length = 255, nullable = true)
  private String auditUser;

  /** The cd tipo id mifid. */
  @Column(name = "CD_TIPO_ID_MIFID", nullable = true, length = 7)
  private String cdTipoIdMifid;

  /** The indicadorDMO. */
  @Column(name = "INDICADOR_DMO", nullable = true)
  private String indicadorDMO;

  /** The cd tipo id mifid. */
  @Column(name = "FECHA_INICIO_PAR_FISC", nullable = true)
  private Date fecInicioParFisc;

  /** The cd tipo id mifid. */
  @Column(name = "FECHA_FIN_PAR_FISC", nullable = true)
  private Timestamp fecFinParFisc;

  /**
   * Gets the primary key.
   *
   * @return the primary key
   */
  public PaisesPK getPrimaryKey() {
    return primaryKey;
  }

  /**
   * Sets the primary key.
   *
   * @param primaryKey the new primary key
   */
  public void setPrimaryKey(PaisesPK primaryKey) {
    this.primaryKey = primaryKey;
  }

  /**
   * Gets the nbpais.
   *
   * @return the nbpais
   */
  public String getNbpais() {
    return nbpais;
  }

  /**
   * Sets the nbpais.
   *
   * @param nbpais the new nbpais
   */
  public void setNbpais(String nbpais) {
    this.nbpais = nbpais;
  }

  /**
   * Gets the cdhacienda.
   *
   * @return the cdhacienda
   */
  public String getCdhacienda() {
    return cdhacienda;
  }

  /**
   * Sets the cdhacienda.
   *
   * @param cdhacienda the new cdhacienda
   */
  public void setCdhacienda(String cdhacienda) {
    this.cdhacienda = cdhacienda;
  }

  /**
   * Gets the cdcodkgr.
   *
   * @return the cdcodkgr
   */
  public String getCdcodkgr() {
    return cdcodkgr;
  }

  /**
   * Sets the cdcodkgr.
   *
   * @param cdcodkgr the new cdcodkgr
   */
  public void setCdcodkgr(String cdcodkgr) {
    this.cdcodkgr = cdcodkgr;
  }

  /**
   * Gets the cdestado.
   *
   * @return the cdestado
   */
  public String getCdestado() {
    return cdestado;
  }

  /**
   * Sets the cdestado.
   *
   * @param cdestado the new cdestado
   */
  public void setCdestado(String cdestado) {
    this.cdestado = cdestado;
  }

  /**
   * Gets the audit date.
   *
   * @return the audit date
   */
  public Timestamp getAuditDate() {
    return auditDate != null ? (Timestamp) auditDate.clone() : null;
  }

  /**
   * Sets the audit date.
   *
   * @param auditDate the new audit date
   */
  public void setAuditDate(Timestamp auditDate) {
    this.auditDate = (Timestamp) auditDate.clone();
  }

  /**
   * Gets the audit user.
   *
   * @return the audit user
   */
  public String getAuditUser() {
    return auditUser;
  }

  /**
   * Sets the audit user.
   *
   * @param auditUser the new audit user
   */
  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * Gets the cd tipo id mifid.
   *
   * @return the cd tipo id mifid
   */
  public String getCdTipoIdMifid() {
    return cdTipoIdMifid;
  }

  /**
   * Sets the cd tipo id mifid.
   *
   * @param cdTipoIdMifid the new cd tipo id mifid
   */
  public void setCdTipoIdMifid(String cdTipoIdMifid) {
    this.cdTipoIdMifid = cdTipoIdMifid;
  }

  /**
   * Gets the indicador DMO.
   *
   * @return the indicador DMO
   */
  public String getIndicadorDMO() {
    return indicadorDMO;
  }

  /**
   * Sets the indicador DMO.
   *
   * @param indicadorDMO the new indicador DMO
   */
  public void setIndicadorDMO(String indicadorDMO) {
    this.indicadorDMO = indicadorDMO;
  }

  /**
   * Gets the fec inicio par fisc.
   *
   * @return the fec inicio par fisc
   */
  public Date getFecInicioParFisc() {
    return fecInicioParFisc != null ? (Date) fecInicioParFisc.clone() : null;
  }

  /**
   * Sets the fec inicio par fisc.
   *
   * @param fecInicioParFisc the new fec inicio par fisc
   */
  public void setFecInicioParFisc(Date fecInicioParFisc) {
    this.fecInicioParFisc = fecInicioParFisc != null ? new Date(fecInicioParFisc.getTime()) : null;
  }

  /**
   * Gets the fec fin par fisc.
   *
   * @return the fec fin par fisc
   */
  public Timestamp getFecFinParFisc() {
    return fecFinParFisc != null ? (Timestamp) fecFinParFisc.clone() : null;
  }

  /**
   * Sets the fec fin par fisc.
   *
   * @param fecFinParFisc the new fec fin par fisc
   */
  public void setFecFinParFisc(Timestamp fecFinParFisc) {
    this.fecFinParFisc = fecFinParFisc != null ? new Timestamp(fecFinParFisc.getTime()) : null;
  }

  @Override
  public String toString() {
    return "PaisesEntity [primaryKey=" + primaryKey.toString() + ", nbpais=" + nbpais + ", cdhacienda=" + cdhacienda
        + ", cdcodkgr=" + cdcodkgr + ", cdestado=" + cdestado + ", auditDate=" + auditDate + ", auditUser=" + auditUser
        + ", cdTipoIdMifid=" + cdTipoIdMifid + ", indicadorDMO=" + indicadorDMO + ", fecInicioParFisc="
        + fecInicioParFisc + ", fecFinParFisc=" + fecFinParFisc + "]";
  }

}
