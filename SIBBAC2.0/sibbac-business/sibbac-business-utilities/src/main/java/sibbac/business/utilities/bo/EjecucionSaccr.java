package sibbac.business.utilities.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.utilities.database.bo.Tmct0SaccrEQBo;
import sibbac.business.utilities.tasks.TaskSccr;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;

/**
 * 	Clase de negocios para ejecucion del procesamiento 
 * 	de SACCR.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EjecucionSaccr {

	private static final Logger LOG = LoggerFactory.getLogger(EjecucionSaccr.class);

	@Autowired
	Tmct0SaccrEQBo tmct0SaccrEQBo;
	
	@Value("${utilities.saccr.folder.out}")
	private String folderOut;

	/**
	 * Ejecucion del procesamiento de SACCR.
	 * @throws SIBBACBusinessException
	 */
	public void ejecutarSaccr() throws SIBBACBusinessException {
		
		GenericLoggers.logDebugInicioFin(TaskSccr.class, Constantes.UTILITIES, "ejecutarSaccr", true);
		
		try {
			this.tmct0SaccrEQBo.generarDatosFromQueries(folderOut);
		} catch (Exception e) {
			GenericLoggers.logError(
				EjecucionSaccr.class, Constantes.UTILITIES, "ejecutarSaccr", "Task SACCR " + e.getMessage());
			throw new SIBBACBusinessException();
		}
		
		GenericLoggers.logDebugInicioFin(TaskSccr.class, Constantes.UTILITIES, "ejecutarSaccr", false);
	}
}
