package sibbac.business.utilities.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *	DTO para el tipo de fichero Equity Spot. 
 */
public class EquitySpotDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3205618420668478893L;

	private Date fhConcilia;
	private String instrument;
	private String nb;
	private String nbExt;
	private String nbInit;
	private Character tpBuy;
	private String cdkgr;
	private String tpCntrpfn;
	private Date tpDteexp;
	private Date tpDteflwf;
	private Date tpDteflwl;
	private Date tpDtefst;
	private Date tpDtefxgf;
	private Date tpDtefxgl;
	private Date tpDtelst;
	private Date tpDtepmt;
	private Date tpDtetrn;
	private String tpEntity;
	private Character tpInt;
	private Date tpIpay;
	private Date tpIpaycur;	
	private Date tpIpayDte;
	private BigDecimal tpIqty;
	private BigDecimal tpIqtys;
	private BigDecimal tpLqty2;
	private BigDecimal tpLqtys2;
	private BigDecimal tpmoplst;
	private String tpMoplstd;
	private String tpMoplstl;
	private String tpNomcur;
	private BigDecimal tpNominal; 
	private String tpFolio;
	private BigDecimal tpPrice;
	private BigDecimal tpQtyeq;
	private BigDecimal tpQtyeqs;
	private String nbValors;
	private String tpSeccod;
	private String tpSeccodu;
	private String tpSeccur;
	private String tpSeccuru;
	private Date tpSecfwsd;
	private String tpSeciss;
	private BigDecimal tpSeclot;
	private Integer tpSeclotu;
	private String tpSecmkt;
	private String tpStatus2;
	private BigDecimal tpStrike;
	private String tpStrtgy;
	private String tpTrader;
	private String trnFmly;
	private String trnGrp;
	private String trnType;

	/**
	 *	Constructor no-arg. 
	 */
	public EquitySpotDTO() {
	}

	/**
	 *	@return fhConcilia  
	 */
	public Date getFhConcilia() {
		return this.fhConcilia;
	}

	/**
	 *	@param fhConcilia 
	 */
	public void setFhConcilia(Date fhConcilia) {
		this.fhConcilia = fhConcilia;
	}

	/**
	 *	@return instrument  
	 */
	public String getInstrument() {
		return this.instrument;
	}

	/**
	 *	@param instrument 
	 */
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	/**
	 *	@return nb  
	 */
	public String getNb() {
		return this.nb;
	}

	/**
	 *	@param nb 
	 */
	public void setNb(String nb) {
		this.nb = nb;
	}

	/**
	 *	@return nbExt  
	 */
	public String getNbExt() {
		return this.nbExt;
	}

	/**
	 *	@param nbExt 
	 */
	public void setNbExt(String nbExt) {
		this.nbExt = nbExt;
	}

	/**
	 *	@return nbInit  
	 */
	public String getNbInit() {
		return this.nbInit;
	}

	/**
	 *	@param nbInit 
	 */
	public void setNbInit(String nbInit) {
		this.nbInit = nbInit;
	}

	/**
	 *	@return tpBuy  
	 */
	public Character getTpBuy() {
		return this.tpBuy;
	}

	/**
	 *	@param tpBuy 
	 */
	public void setTpBuy(Character tpBuy) {
		this.tpBuy = tpBuy;
	}

	public String getCdkgr() {
		return this.cdkgr;
	}

	public void setCdkgr(String cdkgr) {
		this.cdkgr = cdkgr;
	}

	public String getTpCntrpfn() {
		return this.tpCntrpfn;
	}

	public void setTpCntrpfn(String tpCntrpfn) {
		this.tpCntrpfn = tpCntrpfn;
	}

	public Date getTpDteexp() {
		return this.tpDteexp;
	}

	public void setTpDteexp(Date tpDteexp) {
		this.tpDteexp = tpDteexp;
	}

	public Date getTpDteflwf() {
		return this.tpDteflwf;
	}

	public void setTpDteflwf(Date tpDteflwf) {
		this.tpDteflwf = tpDteflwf;
	}

	public Date getTpDteflwl() {
		return this.tpDteflwl;
	}

	public void setTpDteflwl(Date tpDteflwl) {
		this.tpDteflwl = tpDteflwl;
	}

	public Date getTpDtefst() {
		return this.tpDtefst;
	}

	public void setTpDtefst(Date tpDtefst) {
		this.tpDtefst = tpDtefst;
	}

	public Date getTpDtefxgf() {
		return this.tpDtefxgf;
	}

	public void setTpDtefxgf(Date tpDtefxgf) {
		this.tpDtefxgf = tpDtefxgf;
	}

	public Date getTpDtefxgl() {
		return this.tpDtefxgl;
	}

	public void setTpDtefxgl(Date tpDtefxgl) {
		this.tpDtefxgl = tpDtefxgl;
	}

	public Date getTpDtelst() {
		return this.tpDtelst;
	}

	public void setTpDtelst(Date tpDtelst) {
		this.tpDtelst = tpDtelst;
	}

	public Date getTpDtepmt() {
		return this.tpDtepmt;
	}

	public void setTpDtepmt(Date tpDtepmt) {
		this.tpDtepmt = tpDtepmt;
	}

	public Date getTpDtetrn() {
		return this.tpDtetrn;
	}

	public void setTpDtetrn(Date tpDtetrn) {
		this.tpDtetrn = tpDtetrn;
	}

	public String getTpEntity() {
		return this.tpEntity;
	}

	public void setTpEntity(String tpEntity) {
		this.tpEntity = tpEntity;
	}

	public Character getTpInt() {
		return this.tpInt;
	}

	public void setTpInt(Character tpInt) {
		this.tpInt = tpInt;
	}

	public Date getTpIpay() {
		return this.tpIpay;
	}

	public void setTpIpay(Date tpIpay) {
		this.tpIpay = tpIpay;
	}

	public Date getTpIpaycur() {
		return this.tpIpaycur;
	}

	public void setTpIpaycur(Date tpIpaycur) {
		this.tpIpaycur = tpIpaycur;
	}

	public Date getTpIpayDte() {
		return this.tpIpayDte;
	}

	public void setTpIpayDte(Date tpIpayDte) {
		this.tpIpayDte = tpIpayDte;
	}

	public BigDecimal getTpIqty() {
		return this.tpIqty;
	}

	public void setTpIqty(BigDecimal tpIqty) {
		this.tpIqty = tpIqty;
	}

	public BigDecimal getTpIqtys() {
		return this.tpIqtys;
	}

	public void setTpIqtys(BigDecimal tpIqtys) {
		this.tpIqtys = tpIqtys;
	}

	public BigDecimal getTpLqty2() {
		return this.tpLqty2;
	}

	public void setTpLqty2(BigDecimal tpLqty2) {
		this.tpLqty2 = tpLqty2;
	}

	public BigDecimal getTpLqtys2() {
		return this.tpLqtys2;
	}

	public void setTpLqtys2(BigDecimal tpLqtys2) {
		this.tpLqtys2 = tpLqtys2;
	}

	public BigDecimal getTpmoplst() {
		return this.tpmoplst;
	}

	public void setTpmoplst(BigDecimal tpmoplst) {
		this.tpmoplst = tpmoplst;
	}

	public String getTpMoplstd() {
		return this.tpMoplstd;
	}

	public void setTpMoplstd(String tpMoplstd) {
		this.tpMoplstd = tpMoplstd;
	}

	public String getTpMoplstl() {
		return this.tpMoplstl;
	}

	public void setTpMoplstl(String tpMoplstl) {
		this.tpMoplstl = tpMoplstl;
	}

	public String getTpNomcur() {
		return this.tpNomcur;
	}

	public void setTpNomcur(String tpNomcur) {
		this.tpNomcur = tpNomcur;
	}

	public BigDecimal getTpNominal() {
		return this.tpNominal;
	}

	public void setTpNominal(BigDecimal tpNominal) {
		this.tpNominal = tpNominal;
	}

	public String getTpFolio() {
		return this.tpFolio;
	}

	public void setTpFolio(String tpFolio) {
		this.tpFolio = tpFolio;
	}

	public BigDecimal getTpPrice() {
		return this.tpPrice;
	}

	public void setTpPrice(BigDecimal tpPrice) {
		this.tpPrice = tpPrice;
	}

	public BigDecimal getTpQtyeq() {
		return this.tpQtyeq;
	}

	public void setTpQtyeq(BigDecimal tpQtyeq) {
		this.tpQtyeq = tpQtyeq;
	}

	public BigDecimal getTpQtyeqs() {
		return this.tpQtyeqs;
	}

	public void setTpQtyeqs(BigDecimal tpQtyeqs) {
		this.tpQtyeqs = tpQtyeqs;
	}

	public String getNbValors() {
		return this.nbValors;
	}

	public void setNbValors(String nbValors) {
		this.nbValors = nbValors;
	}

	public String getTpSeccod() {
		return this.tpSeccod;
	}

	public void setTpSeccod(String tpSeccod) {
		this.tpSeccod = tpSeccod;
	}

	public String getTpSeccodu() {
		return this.tpSeccodu;
	}

	public void setTpSeccodu(String tpSeccodu) {
		this.tpSeccodu = tpSeccodu;
	}

	public String getTpSeccur() {
		return this.tpSeccur;
	}

	public void setTpSeccur(String tpSeccur) {
		this.tpSeccur = tpSeccur;
	}

	public String getTpSeccuru() {
		return this.tpSeccuru;
	}

	public void setTpSeccuru(String tpSeccuru) {
		this.tpSeccuru = tpSeccuru;
	}

	public Date getTpSecfwsd() {
		return this.tpSecfwsd;
	}

	public void setTpSecfwsd(Date tpSecfwsd) {
		this.tpSecfwsd = tpSecfwsd;
	}

	/**
	 *	@return tpSeciss 
	 */
	public String getTpSeciss() {
		return this.tpSeciss;
	}

	/**
	 *	@param tpSeciss 
	 */
	public void setTpSeciss(String tpSeciss) {
		this.tpSeciss = tpSeciss;
	}

	/**
	 *	@return tpSeclot 
	 */
	public BigDecimal getTpSeclot() {
		return this.tpSeclot;
	}

	/**
	 *	@param tpSeclot 
	 */
	public void setTpSeclot(BigDecimal tpSeclot) {
		this.tpSeclot = tpSeclot;
	}

	/**
	 *	@return tpSeclotu 
	 */
	public Integer getTpSeclotu() {
		return this.tpSeclotu;
	}

	/**
	 *	@param tpSeclotu 
	 */
	public void setTpSeclotu(Integer tpSeclotu) {
		this.tpSeclotu = tpSeclotu;
	}

	/**
	 *	@return tpSecmkt 
	 */
	public String getTpSecmkt() {
		return this.tpSecmkt;
	}

	/**
	 *	@param tpSecmkt 
	 */
	public void setTpSecmkt(String tpSecmkt) {
		this.tpSecmkt = tpSecmkt;
	}

	/**
	 *	@return tpStatus2 
	 */
	public String getTpStatus2() {
		return this.tpStatus2;
	}

	/**
	 *	@param tpStatus2 
	 */
	public void setTpStatus2(String tpStatus2) {
		this.tpStatus2 = tpStatus2;
	}

	/**
	 *	@return tpStrike 
	 */
	public BigDecimal getTpStrike() {
		return this.tpStrike;
	}

	/**
	 *	@param tpStrike 
	 */
	public void setTpStrike(BigDecimal tpStrike) {
		this.tpStrike = tpStrike;
	}

	/**
	 *	@return tpStrtgy 
	 */
	public String getTpStrtgy() {
		return this.tpStrtgy;
	}

	/**
	 *	@param tpStrtgy 
	 */
	public void setTpStrtgy(String tpStrtgy) {
		this.tpStrtgy = tpStrtgy;
	}

	/**
	 *	@return tpTrader 
	 */
	public String getTpTrader() {
		return this.tpTrader;
	}

	/**
	 *	@param tpTrader 
	 */
	public void setTpTrader(String tpTrader) {
		this.tpTrader = tpTrader;
	}

	/**
	 *	@return trnFmly 
	 */
	public String getTrnFmly() {
		return this.trnFmly;
	}

	/**
	 *	@param trnFmly 
	 */
	public void setTrnFmly(String trnFmly) {
		this.trnFmly = trnFmly;
	}

	/**
	 *	@return trnGrp 
	 */
	public String getTrnGrp() {
		return this.trnGrp;
	}

	/**
	 *	@param trnGrp 
	 */
	public void setTrnGrp(String trnGrp) {
		this.trnGrp = trnGrp;
	}

	/**
	 *	@return trnType 
	 */
	public String getTrnType() {
		return this.trnType;
	}

	/**
	 *	@param trnType 
	 */
	public void setTrnType(String trnType) {
		this.trnType = trnType;
	}
	
}