package sibbac.business.utilities.bo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.text.MessageFormat;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.utilities.dto.ResultadoEnvioPartenonDTO;
import sibbac.business.utilities.dto.assembler.EnvioOrdenesPartenonAssembler;
import sibbac.business.utilities.threads.EnvioOrdenesPartenonDiaRunnable;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dto.AnticipoSituacionDTO;
import sibbac.business.wrappers.database.dto.DatosForQueryPartenonDTO;
import sibbac.business.wrappers.database.dto.ParamsFicheroEnvioPartenonDTO;
import sibbac.common.SIBBACBusinessException;

/**
 * Clase de negocios para ejecucion del procesamiento de Envio Ordenes Partenon.
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EjecucionEnvioOrdenesPartenon implements ThreadFactory {

  private static final Logger LOG = LoggerFactory.getLogger(EjecucionEnvioOrdenesPartenon.class);
  
  private static final String PATRON_NOMBRE_FICHERO = "Dyymmdd";
  
  private static final String FORMATO_NOMBRE_FICHERO = "D%1$ty%1$tm%1$td";

  /**
   * Lista que almacena los hilos.
   */
  private final List<Future<List<ResultadoEnvioPartenonDTO>>> misTthreads;

  /**
   * Lista con los datos resultados de la ejecucion de cada hilo.
   */
  private final List<ResultadoEnvioPartenonDTO> lDatosPorDias;
  
  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private EnvioOrdenesPartenonAssembler envioOrdenesPartenonAssembler;

  /**
   * Valor recuperado del propertis con el numero de hilos que se ejecutaran a
   * la vez
   */
  @Value("${partenon.numThreadsPool}")
  private Integer numThreadsPool;

  @Value("${nombretmp.fichero.anticipo_situacion}")
  private String nameFileAndDirTmp;

  @Value("${nombre.fichero.anticipo_situacion}")
  private String nameFileAndDirFinal;

  @Value("${partenon.tramo.update.alc}")
  private Integer partenonTramoUpdateAlc;
  
  @Value("${partenon.timeout : 5}")
  private int timeout;

  /**
   * Ejecutor de los hilos.
   */
  private ExecutorService executor;
  
  private int iNumJob;

  public EjecucionEnvioOrdenesPartenon() {
  	misTthreads = new ArrayList<>();
  	lDatosPorDias = new ArrayList<>();
  }

  /**
   * Ejecucion del procesamiento de Envio Ordenes Partenon.
   * 
   * @throws SIBBACBusinessException
   */
  public void ejecutarEnvioOrdenesPartenon() throws SIBBACBusinessException {
    LOG.debug("[ejecutarEnvioOrdenesPartenon] Inicio...");
    try {
      // Se obtiene la lista de fechas sobre la que se corre la ejecucion
      List<Date> listaFechasEjecucion = this.envioOrdenesPartenonAssembler.getListaFechasEjecucion();

      // Inicializaciones
      // Se van a crear un pool de hilos indicado en la variable. Son el numero
      // de hilos que se ejecutan a la vez.
      // Se realiza esto porque al ser el mismo objeto unos hilos estaban
      // cogiendo los filtros de otros
      executor = Executors.newFixedThreadPool(numThreadsPool, this);
      if (CollectionUtils.isNotEmpty(listaFechasEjecucion)) {
        for (int i = 0; i < listaFechasEjecucion.size(); i++) {
          // Relleno los datos necesarios para correr la query desde un
          // assembler.
          DatosForQueryPartenonDTO datosForQueryPartenonDTO = this.envioOrdenesPartenonAssembler
              .getDatosForQueryPartenon(listaFechasEjecucion.get(i));
          iNumJob = i;
          ejecutarHilo(datosForQueryPartenonDTO);
        }
        LOG.debug("[ejecutarEnvioOrdenesPartenon] Numero de threads creados - {}", misTthreads.size());
        executor.shutdown();
        try {
          executor.awaitTermination(timeout, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.error("[ejecutarEnvioOrdenesPartenon] {}",
              "Se ha interrumpido algún thread en el control de cuantos quedan vivos ", e);
          shutdownAllRunnable();
        }
        collectData();
        if (CollectionUtils.isNotEmpty(lDatosPorDias)) {
          // Se procesa el envio de ordenes Partenon
          procesarEnvioOrdenesPartenon();
        }
        else {
        	LOG.error("[ejecutarEnvioOrdenesPartenon] No se han encontrado órdenes Partenón en la ejecución de consulta");
        }
      }
      else {
      	LOG.error("[ejecutarEnvioOrdenesPartenon] {}",
            "No se han encontrado fechas de ejecucion sobre la cual correr la query de envío de órdenes Partenón");
      }
    }
    catch (RuntimeException e) {
    	LOG.error("[ejecutarEnvioOrdenesPartenon] Error inesperado en Task Envío Ordenes Partenón ", e);
      throw e;
    }
    finally {
	    lDatosPorDias.clear();
	    misTthreads.clear();
    }
    LOG.debug("[ejecutarEnvioOrdenesPartenon] Fin");
  }

  /**
   * Ejecuta query envio partenon.
   * @param datosForQueryPartenonDTO
   * @return List<ResultadoEnvioPartenonDTO>
   * @throws SIBBACBusinessException
   */
  public List<ResultadoEnvioPartenonDTO> ejecutaQueryEnvioPartenon(DatosForQueryPartenonDTO datosForQueryPartenonDTO)
      throws SIBBACBusinessException {
    // Se ejecuta query
    List<Object[]> ordenesPartenon = this.tmct0alcDaoImp.ejecutarQueryEnvioOrdenesPartenon(datosForQueryPartenonDTO);
    List<ResultadoEnvioPartenonDTO> listResultadoQueryEnvioPartenon = this.envioOrdenesPartenonAssembler
        .getDTOListFromQueryEnvioOrdPartenResults(ordenesPartenon);
    return listResultadoQueryEnvioPartenon;
  }

  @Override
	public Thread newThread(Runnable r) {
		final String name;
		
		name = String.format("EnvioPartenon-%d", iNumJob);
		return new Thread(r, name);
	}

	private void collectData() {
  	int terminados = 0, otroEstado = 0;
  	
    for(Future<List<ResultadoEnvioPartenonDTO>> future : misTthreads) {
    	if(future.isDone()) {
    		try {
					lDatosPorDias.addAll(future.get());
					terminados++;
					continue;
				} 
    		catch (InterruptedException e) {
    			LOG.error("[collectData] El hilo fue interrumpido", e);
				} 
    		catch (ExecutionException e) {
    			LOG.error("[collectData] Error de ejecución de hilo", e.getCause());
				}
    	}
    	otroEstado++;
    }
    LOG.info("[collectData] Hilos terminados: {}. En otro estado: {}", terminados, otroEstado);
  }

  /**
   * Elimina los jobs pendientes del pool.
   */
  private void shutdownAllRunnable() {
  	int terminados = 0, cancelados = 0, pendientes = 0;
  	
    LOG.error("ERROR NO CONTROLADO- SE CANCELAN TODOS LOS JOBS ");
    for (Future<?> miHilo : misTthreads) {
    	if(!miHilo.isDone()) {
        if(miHilo.cancel(false)) {
        	cancelados++;
        }
        else {
        	pendientes++;
        }
    	}
    	else {
    		terminados++;
    	}
    }
    LOG.info("[shutdownAllRunable] Se han cancelado {}. Se alcanzaron a terminar {}. Quedaron pendientes {}",
    		cancelados, terminados, pendientes);
  }

  /**
   * Crea un nuevo hilo y lo incorpora al pool
   * 
   * @param datosForQueryPartenonDTO
   * @param iNumHilo -- Numero identificativo del hilo
   */
  private void ejecutarHilo(DatosForQueryPartenonDTO datosForQueryPartenonDTO) {
  	Future<List<ResultadoEnvioPartenonDTO>> future;
  	final EnvioOrdenesPartenonDiaRunnable runnable;
  	
  	runnable = new EnvioOrdenesPartenonDiaRunnable(this, datosForQueryPartenonDTO, iNumJob);
    future = executor.submit(runnable);
    runnable.setFuture(future);
    misTthreads.add(future);
  }

  /**
   * Procesamiento para funcionalidad de envio de ordenes partenon.
   * 
   * @param ordenesPartenon
   * @param params
   * @throws SIBBACBusinessException
   */
  private void procesarEnvioOrdenesPartenon() throws SIBBACBusinessException {
    String message;
    final ParamsFicheroEnvioPartenonDTO params = envioOrdenesPartenonAssembler.getParamsFicheroEnvioPartenonDTO();

    // Se genera fichero en caso de que se devuelvan resultados
    if (CollectionUtils.isNotEmpty(lDatosPorDias) && StringUtils.isNotBlank(nameFileAndDirTmp)) {

      // Concatenacion de identificadores de ALCs
      StringBuffer identifALCsConcatSb = new StringBuffer();

      // Lista que contiene tramos de tetra identificadores de ALCs
      List<String> listaTramosIdsALCs = new ArrayList<>();
      try (PrintWriter writer = new PrintWriter(nameFileAndDirTmp, "ISO-8859-1")) {
        // Se iteran las alc's para armar una fila
        for (int x = 0; x < lDatosPorDias.size(); x++) {
          AnticipoSituacionDTO anticipoSituacionDTO = envioOrdenesPartenonAssembler
              .getLineaAnticipoSituacion(params, lDatosPorDias.get(x));
          writer.println(anticipoSituacionDTO.getLineaAnticipoSituacion());

          // Se suprime la ultima "," de la expresion armada para actualizar
          // ALCs.
          if (x % partenonTramoUpdateAlc == 0 || x == (lDatosPorDias.size() - 1)) {
            identifALCsConcatSb.append(lDatosPorDias.get(x).getGrupoIdentificadorALC().substring(0,
            		lDatosPorDias.get(x).getGrupoIdentificadorALC().length() - 2));
            // Se van agregando tramos de ids de ALC a la lista
            listaTramosIdsALCs.add(identifALCsConcatSb.toString());
            // Se vacia concatenador para comenzar un nuevo tramo
            identifALCsConcatSb = new StringBuffer();
          }
          else {
            // Se suprimen cuartetos de alc's repetidos en la query.
            if (!identifALCsConcatSb.toString().contains(lDatosPorDias.get(x).getGrupoIdentificadorALC())) {
              identifALCsConcatSb.append(lDatosPorDias.get(x).getGrupoIdentificadorALC());
            }

          }
        }
      }
      catch (FileNotFoundException e) {
      	message = MessageFormat.format("El fichero {0} no se ha encontrado", nameFileAndDirTmp);
      	LOG.error("[procesarEnvioOrdenesPartenon] {}", message, e);
        throw new SIBBACBusinessException(message, e);
      }
      catch (UnsupportedEncodingException e) {
        throw new SIBBACBusinessException("Error de encoding del fichero.", e);
      }

      // Finalizada la carga del fichero se renombra para representar estado
      // final
      if (StringUtils.isNotBlank(nameFileAndDirFinal)) {
        StringBuilder nombreReal = new StringBuilder(nameFileAndDirFinal);
        int pos = nombreReal.indexOf(PATRON_NOMBRE_FICHERO);
        if(pos >= 0) {
          nombreReal.replace(pos, pos + PATRON_NOMBRE_FICHERO.length(), 
              String.format(FORMATO_NOMBRE_FICHERO, new Date()));
        }
        File nombreFicheroTemp = new File(nameFileAndDirTmp);
        File nombreFicheroFinal = new File(nombreReal.toString());
        nombreFicheroTemp.renameTo(nombreFicheroFinal);
      }
      else {
        // En caso de que no se fije el parametro del nombre final del fichero
        // se informa
        // y no se actualizan los ALCs procesados
        LOG.error("[procesarEnvioOrdenesPartenon] Se debe fijar ruta y nombre del fichero final {}", nameFileAndDirTmp);
        return;
      }

      try {
        // Se utilizan los identificadores de ALCs para actualizar el campo
        // 'desenliq' de cada una de estas
        this.tmct0alcDaoImp.updateDesenliqALCs('S', listaTramosIdsALCs);
      }
      catch (Exception e) {
      	LOG.error("[procesarEnvioOrdenesPartenon] No se han podido actualizar las ALC's al estado correspondiente");
        try {
         
          // Se borra el fichero en caso de algun inconveniente de persistencia
          File ficheroGrabado = new File(nameFileAndDirTmp);
          Files.deleteIfExists(ficheroGrabado.toPath());
        }
        catch (IOException e1) {
        	LOG.warn("[procesarEnvioOrdenesPartenon] Error secundario al intentar eliminar el fichero {}", 
        			nameFileAndDirTmp, e1);
        }
        throw new SIBBACBusinessException("No se han podido actualizar las ALC's al estado correspondiente", e);
      }

    }
    else {
    	LOG.error("[procesarEnvioOrdenesPartenon] {} {}",
          "La query de de órdenes Partenón no ha devuelto resultados o la ruta y nombre de fichero temporal ",
          "no han sido especificadas en el fichero sibbac.partenon.properties");
    }
  }
}
