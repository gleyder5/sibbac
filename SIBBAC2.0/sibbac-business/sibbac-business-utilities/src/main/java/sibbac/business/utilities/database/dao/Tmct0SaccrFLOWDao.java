package sibbac.business.utilities.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.utilities.database.model.Tmct0SaccrFLOW;

/**
 * Data access object de Tmct0SaccrFLOW.
 */
@Repository
public interface Tmct0SaccrFLOWDao extends JpaRepository<Tmct0SaccrFLOW, Integer> {

}
