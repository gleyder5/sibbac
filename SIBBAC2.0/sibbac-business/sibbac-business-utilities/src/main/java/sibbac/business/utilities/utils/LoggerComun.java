package sibbac.business.utilities.utils;


public class LoggerComun {
	
	  /**
	   * Devuelve una cadena formada por funcionalidad + clase + método
	   * @param funcionalidad
	   * @param clase
	   * @param metodo
	   * @return String
	   */
	  public static String getLogError(String funcionalidad, String clase, String metodo) {
	    StringBuffer sbLogQuery = new StringBuffer();
	    sbLogQuery.append(funcionalidad);
	    sbLogQuery.append(" - ");
	    sbLogQuery.append(clase);
	    sbLogQuery.append(" - ");
	    sbLogQuery.append(metodo);
	    return sbLogQuery.toString();
	  }

	  /**
	   * Devuelve una cadena formada por INICIO + clase + método
	   * @param clase
	   * @param metodo
	   * @return String
	   */
	  public static String getLogInicioMetodo(String clase, String metodo) {
	    StringBuffer sbLogQuery = new StringBuffer();
	    sbLogQuery.append("INICIO");
	    sbLogQuery.append(" - ");
	    sbLogQuery.append(clase);
	    sbLogQuery.append(" - ");
	    sbLogQuery.append(metodo);
	    return sbLogQuery.toString();
	  }
	  
	  /**
	   * Devuelve una cadena formada por INICIO + clase + método
	   * @param clase
	   * @param metodo
	   * @return String
	   */
	  public static String getLogFinMetodo(String clase, String metodo) {
	    StringBuffer sbLogQuery = new StringBuffer();
	    sbLogQuery.append("FIN");
	    sbLogQuery.append(" - ");
	    sbLogQuery.append(clase);
	    sbLogQuery.append(" - ");
	    sbLogQuery.append(metodo);
	    return sbLogQuery.toString();
	  }
}
