package sibbac.business.utilities.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *	DTO para la query con datos base de SCCR. 
 */
public class QueryDatosBaseSCCRDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1491283280606188864L;

	private String nuorden;
	private String nbooking;
	private BigDecimal precioMedio;
	private Date fevalorr;
	private String cdmoniso;
	private String cdtoper;
	private Date fevalor;
	private BigDecimal efectivo;
	private BigDecimal precioRedondeo;
	
	/**
	 * 	Constructor no-arg.
	 */
	public QueryDatosBaseSCCRDTO() {}

	
	public String getNuorden() {
		return this.nuorden;
	}

	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}

	public String getNbooking() {
		return this.nbooking;
	}

	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}

	public BigDecimal getPrecioMedio() {
		return this.precioMedio;
	}

	public void setPrecioMedio(BigDecimal precioMedio) {
		this.precioMedio = precioMedio;
	}

	public Date getFevalorr() {
		return this.fevalorr;
	}

	public void setFevalorr(Date fevalorr) {
		this.fevalorr = fevalorr;
	}

	public String getCdmoniso() {
		return this.cdmoniso;
	}

	public void setCdmoniso(String cdmoniso) {
		this.cdmoniso = cdmoniso;
	}

	public String getCdtoper() {
		return this.cdtoper;
	}

	public void setCdtoper(String cdtoper) {
		this.cdtoper = cdtoper;
	}

	public Date getFevalor() {
		return this.fevalor;
	}

	public void setFevalor(Date fevalor) {
		this.fevalor = fevalor;
	}

	public BigDecimal getEfectivo() {
		return this.efectivo;
	}

	public void setEfectivo(BigDecimal efectivo) {
		this.efectivo = efectivo;
	}

	public BigDecimal getPrecioRedondeo() {
		return this.precioRedondeo;
	}

	public void setPrecioRedondeo(BigDecimal precioRedondeo) {
		this.precioRedondeo = precioRedondeo;
	}

}