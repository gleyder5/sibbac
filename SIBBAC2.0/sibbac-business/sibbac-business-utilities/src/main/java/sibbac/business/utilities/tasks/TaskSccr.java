package sibbac.business.utilities.tasks;

import java.util.Calendar;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.utilities.bo.EjecucionSaccr;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.DateUtilService;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_SACCR_IRIS.NAME,
           name = Task.GROUP_SACCR_IRIS.JOB_SACCR_IRIS,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "04:00:00")
public class TaskSccr extends SIBBACTask {
	
	@Autowired
	private Tmct0menBo tmct0menBo;

	@Autowired
	private EjecucionSaccr ejecucionSaccr;
	
	@Autowired
	private DateUtilService dateUtilService;
	
	@Override
	public void execute() {
		
		GenericLoggers.logDebugInicioFin(TaskSccr.class, Constantes.UTILITIES, "execute", true);
		
		Tmct0men tmct0men = null;
		Calendar cal = Calendar.getInstance();

		// Los ficheros son de generación diaria, salvo fines de semana y festivo
		if (!(DateHelper.isFechaFinDeSemana(cal) || this.dateUtilService.isFechaFestiva(cal.getTime()))) {
			try {
				tmct0men = this.tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_SACCR_IRIS, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);
				this.ejecucionSaccr.ejecutarSaccr();
				tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_SACCR_IRIS, TMCT0MSC.EN_EJECUCION, TMCT0MSC.LISTO_GENERAR);
			} catch (SIBBACBusinessException e) {
				this.tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
				 GenericLoggers.logError(
						 TaskSccr.class, Constantes.UTILITIES, "execute", "Ejecución Task SACCR " + e.getMessage());
			} catch (Exception ex) {
				this.tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
				 GenericLoggers.logError(
						 TaskSccr.class, Constantes.UTILITIES, "execute", "Ejecución Task SACCR " + ex.getMessage());
			}			
		} else {
			GenericLoggers.logDebug(
				TaskSccr.class, Constantes.UTILITIES, "execute", 
				"Dicho proceso no se ha ejecutado por ser festivo ó fin de semana.");
		}
		GenericLoggers.logDebugInicioFin(TaskSccr.class, Constantes.UTILITIES, "execute", false);
	}
}