package sibbac.business.utilities.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.utilities.database.model.Tmct0SaccrGLOBAL;

/**
 * Data access object de Tmct0SaccrGLOBAL.
 */
@Repository
public interface Tmct0SaccrGLOBALDao extends JpaRepository<Tmct0SaccrGLOBAL, Integer> {

}
