package sibbac.business.utilities.database.bo;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.utilities.database.dao.Tmct0SaccrEQDao;
import sibbac.business.utilities.database.dao.Tmct0SaccrEQDaoImpl;
import sibbac.business.utilities.database.dao.Tmct0SaccrFLOWDaoImpl;
import sibbac.business.utilities.database.dao.Tmct0SaccrGLOBALDaoImpl;
import sibbac.business.utilities.database.dao.Tmct0SaccrMVDaoImpl;
import sibbac.business.utilities.database.model.Tmct0SaccrEQ;
import sibbac.business.utilities.dto.QueryDatosEQSCCRDTO;
import sibbac.business.utilities.dto.QueryDatosMVSCCRDTO;
import sibbac.business.utilities.enums.UtilitiesEnums.PrefijosNombresFicherosSACCR;
import sibbac.business.utilities.utils.TratamientoDatos;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SaccrEQBo extends AbstractBo<Tmct0SaccrEQ, Integer, Tmct0SaccrEQDao> {

  @Autowired
  Tmct0SaccrEQDaoImpl tmct0SaccrEQDaoImpl;

  @Autowired
  Tmct0SaccrMVDaoImpl tmct0SaccrMVDaoImpl;

  @Autowired
  Tmct0SaccrFLOWDaoImpl tmct0SaccrFLOWDaoImpl;

  @Autowired
  Tmct0SaccrGLOBALDaoImpl tmct0SaccrGLOBALDaoImpl;

  @Autowired
  TratamientoDatos tratamientoDatos;

  /**
   * Logica transaccional para obtener informacion de las queries que permitiran
   * el armado de los ficheros.
   * @throws Exception
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public void generarDatosFromQueries(String folderOut) throws Exception {

    Map<String, List<String>> contenidoFicheros = new HashMap<String, List<String>>();

    try {
      // EQUITY SPOT
      List<String> contenidoEQ = new ArrayList<String>();
      // Se carga la cabecera del fichero
      List<Object[]> resultEQ = this.tmct0SaccrEQDaoImpl.consultarFieldsEQ();
      contenidoEQ.add(this.tratamientoDatos.convertirCadenaCabecera(resultEQ));
      // Se obtienen los valores de la query
      List<QueryDatosEQSCCRDTO> listQueryDatos = this.tmct0SaccrEQDaoImpl.getDatosEQSCCRByQuery();
      // Se convierten los datos recuperados a un String separado por | y se
      // añaden a la lista
      if (CollectionUtils.isNotEmpty(listQueryDatos)) {
        for (QueryDatosEQSCCRDTO qdEQSCCRDTO : listQueryDatos) {
          contenidoEQ.add(this.tratamientoDatos.convertirCadenaValoresEQ(qdEQSCCRDTO, resultEQ));
        }
      }
      contenidoFicheros.put(sibbac.business.utilities.utils.Constantes.FICHERO_EQUITY_SPOT, contenidoEQ);

      // MV
      List<String> contenidoMV = new ArrayList<String>();
      // Se carga la cabecera del fichero
      List<Object[]> resultMV = this.tmct0SaccrMVDaoImpl.consultarFieldsMV();
      contenidoMV.add(this.tratamientoDatos.convertirCadenaCabecera(resultMV));
      // Se obtienen los valores de la query
      List<QueryDatosMVSCCRDTO> listQueryDatosMV = this.tmct0SaccrMVDaoImpl.getDatosMVSCCRByQuery();
      // Se convierten los datos recuperados a un String separado por | y se
      // añaden a la lista
      if (CollectionUtils.isNotEmpty(listQueryDatosMV)) {
        for (QueryDatosMVSCCRDTO qdMVSCCRDTO : listQueryDatosMV) {
          contenidoMV.add(this.tratamientoDatos.convertirCadenaValoresMV(qdMVSCCRDTO, resultMV));
        }
      }
      contenidoFicheros.put(sibbac.business.utilities.utils.Constantes.FICHERO_MARKET_VALUE, contenidoMV);

      // FLOW
      // List<String> contenidoFLOW = new ArrayList<String>();
      // //Se carga la cabecera del fichero
      // List<Object[]> resultFLOW =
      // this.tmct0SaccrFLOWDaoImpl.consultarFieldsFLOW();
      // contenidoFLOW.add(this.tratamientoDatos.convertirCadenaCabecera(resultFLOW));
      // contenidoFicheros.put(sibbac.business.utilities.utils.Constantes.FICHERO_FLOW,
      // contenidoFLOW);

      // // UDF GLOBAL
      // List<String> contenidoGLOBAL = new ArrayList<String>();
      // //Se carga la cabecera del fichero
      // List<Object[]> resultGLOBAL =
      // this.tmct0SaccrGLOBALDaoImpl.consultarFieldsGLOBAL();
      // contenidoGLOBAL.add(this.tratamientoDatos.convertirCadenaCabecera(resultGLOBAL));
      // contenidoFicheros.put(sibbac.business.utilities.utils.Constantes.FICHERO_UDF_GLOBAL,
      // contenidoGLOBAL);

      this.procesarFicheros(contenidoFicheros, folderOut);

    }
    catch (Exception ex) {
      GenericLoggers.logError(Tmct0SaccrEQBo.class, Constantes.UTILITIES, "generarDatosFromQueries",
          "Error al obtener datos: " + ex.getMessage());
      throw new SIBBACBusinessException();
    }

  }

  /**
   * Creacion de ficheros.
   * @param listaFicherosContenido: lineas del fichero
   * @param folderOut: carpeta de salida
   * @return boolean
   * @throws Exception
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
  public boolean procesarFicheros(Map<String, List<String>> contenidoFicheros, String folderOut) throws Exception {

    try {
      if (contenidoFicheros != null && !contenidoFicheros.isEmpty()) {

        SimpleDateFormat formatDate = new SimpleDateFormat("ddMMyy");
        String fechaFormatted = formatDate.format(new Date());

        for (String key : contenidoFicheros.keySet()) {
          StringBuffer sbTituloFichero = new StringBuffer();
          List<String> contenido = contenidoFicheros.get(key);

          if (CollectionUtils.isNotEmpty(contenido)) {
            // Se carga prefijo del nombre del fichero
            switch (key) {
              case sibbac.business.utilities.utils.Constantes.FICHERO_EQUITY_SPOT:
                sbTituloFichero.append(PrefijosNombresFicherosSACCR.KAPITI_LAGO_TRADE_EQ.getDescription());
                break;
              case sibbac.business.utilities.utils.Constantes.FICHERO_MARKET_VALUE:
                sbTituloFichero.append(PrefijosNombresFicherosSACCR.KAPITI_LAGO_TMV.getDescription());
                break;
              case sibbac.business.utilities.utils.Constantes.FICHERO_FLOW:
                sbTituloFichero.append(PrefijosNombresFicherosSACCR.KAPITI_LAGO_TCS.getDescription());
                break;
              case sibbac.business.utilities.utils.Constantes.FICHERO_UDF_GLOBAL:
                sbTituloFichero.append(PrefijosNombresFicherosSACCR.KAPITI_LAGO_UDFEQD.getDescription());
                break;
              default:
                break;
            }
            sbTituloFichero.append(fechaFormatted + sibbac.business.utilities.utils.Constantes.SUFIJO_FICHEROS_SACCR);

            // Se escribe el fichero a generar
            PrintWriter writer = new PrintWriter(folderOut + sbTituloFichero.toString(), "UTF-8");
            for (String fila : contenido) {
              writer.println(fila);
            }
            writer.close();
          }
        }
      }

    }
    catch (Exception ex) {
      GenericLoggers.logError(Tmct0SaccrEQBo.class, Constantes.UTILITIES, "procesarFicheros",
          "Error al procesar ficheros: " + ex.getMessage());
      throw new SIBBACBusinessException();
    }
    return true;
  }
}