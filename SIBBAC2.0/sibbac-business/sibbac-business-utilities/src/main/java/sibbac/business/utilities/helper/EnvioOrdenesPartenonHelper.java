/**
 * 
 */
package sibbac.business.utilities.helper;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import sibbac.business.utilities.dto.ResultadoEnvioPartenonDTO;
import sibbac.business.wrappers.database.dto.ParamsFicheroEnvioPartenonDTO;

/**
 * @author lucio.vilar
 *
 */
public class EnvioOrdenesPartenonHelper {
	
	public static String obtenerProcedencia(ParamsFicheroEnvioPartenonDTO params, 
	    ResultadoEnvioPartenonDTO ordenPartenon) {
		final String primeraProcedencia, segundaProcedencia, cdCuenta;
	  
		primeraProcedencia = params.getProcedencia(ordenPartenon.getDcliCdRefAsignacion());
		if(primeraProcedencia != null) {
		  if(primeraProcedencia.equals(ordenPartenon.getDcliCdRefAsignacion())) {
		    return primeraProcedencia;
		  }
		  cdCuenta = Objects.requireNonNull(ordenPartenon.getOrdCdcuenta()).trim();
		  if(!primeraProcedencia.equals("*")) {
		    return String.format("%s-%s", primeraProcedencia, cdCuenta);
		  }
		  segundaProcedencia = params.getProcedencia2(cdCuenta);
		  if(segundaProcedencia == null) {
		    return String.format("BOLPLUS-%s", cdCuenta);
		  }
		  return segundaProcedencia;
		}
		return "<ERROR>";
	}

	public static String obtenerPropuesta(
		ParamsFicheroEnvioPartenonDTO params, ResultadoEnvioPartenonDTO ordenPartenon) {
		String result = null;
		
		String propAnticipoSituacion = params.getPropuestaAnticipoSituacion();
		String ordCuenta = StringUtils.trim(ordenPartenon.getOrdCdcuenta());
		String ordNuref = StringUtils.trim(ordenPartenon.getOrdNureford());
		String routingAlias = params.getRoutingAliasAnticipoSituacion();
		String especialAlias = params.getEspecialAliasAnticipoSituacion();
		String alcNucnfclt = StringUtils.stripEnd(ordenPartenon.getAlcNucnfclt(), " ");
		
		if (null != propAnticipoSituacion) {
			if (propAnticipoSituacion.equalsIgnoreCase("none")) {
				result = "          ";
			} else if (propAnticipoSituacion.equalsIgnoreCase("routing")) {
				if (StringUtils.containsIgnoreCase(routingAlias, ordCuenta)) {
					result = ordNuref;
				} else {
					result = "          ";
				}
			} else if (propAnticipoSituacion.equalsIgnoreCase("routing-especial")) {
				if (StringUtils.containsIgnoreCase(routingAlias, ordCuenta)) {
					result = ordNuref;
				} else if (StringUtils.containsIgnoreCase(especialAlias, ordCuenta)) {
					if (null != alcNucnfclt && alcNucnfclt.length() == 18 && StringUtils.startsWithIgnoreCase(alcNucnfclt, "PARTENON") 
							&& StringUtils.isNumeric(StringUtils.substringAfter(alcNucnfclt, "PARTENON"))) {
						result = StringUtils.substringAfter(alcNucnfclt, "PARTENON");
					} else {
						result = ordNuref;
					}
				} else {
					result = "          ";
				}
			} else {
				if (StringUtils.containsIgnoreCase(especialAlias, ordCuenta)) {
					if (null != alcNucnfclt && alcNucnfclt.length() == 18 && StringUtils.startsWithIgnoreCase(alcNucnfclt, "PARTENON") 
							&& StringUtils.isNumeric(StringUtils.substringAfter(alcNucnfclt, "PARTENON"))) {
						result = StringUtils.substringAfter(alcNucnfclt, "PARTENON");
					} else {
						result = ordNuref;
					}
				} else {
					result = ordNuref;
				}
			}
		}
		if(result != null && result.trim().length() != 10) {
		  return "";
		}
		return result;
	}
	
}
