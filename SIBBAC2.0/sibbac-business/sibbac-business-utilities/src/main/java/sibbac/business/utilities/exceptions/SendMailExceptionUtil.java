package sibbac.business.utilities.exceptions;

/**
 * Exception utilizada para el manejo de errores en Ejecución de Plantillas.
 * 
 * @author Neoris
 *
 */
public class SendMailExceptionUtil extends Exception {

	/* Serial version UID */
	private static final long serialVersionUID = -8844662802647154136L;

	/** 
	 * Constructor por defecto.
	 */
	public SendMailExceptionUtil() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            Mensaje de error.
	 * @param cause
	 *            Causa del error.
	 */
	public SendMailExceptionUtil(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            Mensaje de error.
	 */
	public SendMailExceptionUtil(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause
	 *            Causa del error.
	 */
	public SendMailExceptionUtil(Throwable cause) {
		super(cause);
	}
}
