package sibbac.business.utilities.database.bo;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SibbacConstantesBo extends AbstractBo<Tmct0SibbacConstantes, Integer, Tmct0SibbacConstantesDao> {

//	@Modifying
//	@Query(value = "UPDATE TMCT0_SIBBAC_CONSTANTES SET VALOR = :newValor WHERE CLAVE = :clave", nativeQuery = true)
//	public Integer updateValorByClave(@Param( "newValor" ) String newValor, @Param( "clave" ) String clave); 

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateValorByClave(String newValor, String clave) {
		List<Tmct0SibbacConstantes> listTmct0SibbacConstantes = this.dao.findByClave(clave);
		if (CollectionUtils.isNotEmpty(listTmct0SibbacConstantes)) {
			listTmct0SibbacConstantes.get(0).setValor(newValor);
			this.dao.save(listTmct0SibbacConstantes.get(0));
		} 
	}
	
	
}