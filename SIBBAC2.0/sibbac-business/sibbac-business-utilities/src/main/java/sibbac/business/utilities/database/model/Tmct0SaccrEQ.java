package sibbac.business.utilities.database.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_SACCR_EQ
 */
@Entity
@Table(name = "TMCT0_SACCR_EQ")
public class Tmct0SaccrEQ {


	private static final Logger LOG = LoggerFactory.getLogger(Tmct0SaccrEQ.class);

	/**
	 * Identificador del registro en la tabla
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	@Column(name = "FIELD", length = 30, nullable = true)
	private String field;
	
	@Column(name = "MUREX_DEFINITION", length = 100, nullable = true)
	private String murexDefinition;
	
	@Column(name = "COMMENT", length = 700, nullable = true)
	private String comment;
	
	@Column(name = "MANDATORY", columnDefinition="SMALLINT")
	private Integer	mandatory;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0SaccrEQ() {
		super();
	}

	/**
	 *	@return id. 
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 *	@param id. 
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 *	@return field. 
	 */
	public String getField() {
		return field;
	}

	/**
	 *	@param field. 
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 *	@return murexDefinition. 
	 */
	public String getMurexDefinition() {
		return murexDefinition;
	}

	/**
	 *	@param murexDefinition. 
	 */
	public void setMurexDefinition(String murexDefinition) {
		this.murexDefinition = murexDefinition;
	}

	/**
	 *	@return comment. 
	 */
	public String getComment() {
		return comment;
	}

	/**
	 *	@param comment. 
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 *	@return mandatory. 
	 */
	public Integer getMandatory() {
		return mandatory;
	}

	/**
	 *	@param mandatory. 
	 */
	public void setMandatory(Integer mandatory) {
		this.mandatory = mandatory;
	}
	
}
