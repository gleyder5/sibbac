package sibbac.business.utilities.database.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_SIBBAC_CONSTANTES.
 */
@Entity
@Table(name = "TMCT0_SIBBAC_CONSTANTES")
public class Tmct0SibbacConstantes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;

	@Column(name = "FUNCIONALIDAD", length = 250, nullable = true)
	private String funcionalidad;

	@Column(name = "CLAVE", length = 100, nullable = true)
	private String clave;
	
	@Column(name = "VALOR", length = 500, nullable = true)
	private String valor;
	
	@Column(name = "AUXILIAR", length = 500, nullable = true)
	private String auxiliar;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0SibbacConstantes() {
		super();
	}

	/**
	 * @param id
	 * @param funcionalidad
	 * @param clave
	 * @param valor
	 * @param auxiliar
	 */
	public Tmct0SibbacConstantes(BigInteger id, String funcionalidad,
			String clave, String valor, String auxiliar) {
		super();
		this.id = id;
		this.funcionalidad = funcionalidad;
		this.clave = clave;
		this.valor = valor;
		this.auxiliar = auxiliar;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the funcionalidad
	 */
	public String getFuncionalidad() {
		return funcionalidad;
	}

	/**
	 * @param funcionalidad the funcionalidad to set
	 */
	public void setFuncionalidad(String funcionalidad) {
		this.funcionalidad = funcionalidad;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the auxiliar
	 */
	public String getAuxiliar() {
		return auxiliar;
	}

	/**
	 * @param auxiliar the auxiliar to set
	 */
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}


}
