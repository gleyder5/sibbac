package sibbac.business.utilities.utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import sibbac.business.utilities.dto.QueryDatosEQSCCRDTO;
import sibbac.business.utilities.dto.QueryDatosMVSCCRDTO;
import sibbac.business.wrappers.common.StringHelper;

@Service
public class TratamientoDatos {

	public String convertirCadenaCabecera (List<Object[]> listaFields) {
		String cabecera = "";
		if (null!=listaFields && listaFields.size()>0) {
			for (int i=0; i<listaFields.size();i++) {
				if (i==0) {
					cabecera = cabecera + ((Object[])listaFields.get(i))[0].toString();
				} else {
					cabecera = cabecera + "|" + ((Object[])listaFields.get(i))[0].toString();
				}
			}
		}
		return cabecera;
	}
	
	public String convertirCadenaValoresEQ(QueryDatosEQSCCRDTO queryDatos, List<Object[]> listaCabecera) {
		String cadenaValores = "";
		
		boolean esPrimerLinea = true;
		if (null!=listaCabecera && listaCabecera.size()>0) {
			for (Object listCabecera : listaCabecera) {
				if (Integer.valueOf(String.valueOf(((Object[]) listCabecera)[1])) == 0) {
					if (esPrimerLinea) {
						cadenaValores = cadenaValores + " ";
						esPrimerLinea = false;
					} else {
						cadenaValores = cadenaValores + "| ";
					}
				} else {
			          switch (String.valueOf(((Object[]) listCabecera)[0])) {

			          case "FHCONCILIA":
			        	  cadenaValores = cadenaValores + "|" + new Date();
			        	  break;
			          case "INSTRUMENT":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getNbValors());
			        	  break;
			          case "NB":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getNbooking());
			        	  break;
			          case "NB_EXT":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "NB_INIT":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getNbooking());
			        	  break;
			          case "TP_BUY":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdtoper());
			        	  break;
			          case "TP_CNTRP":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdkgr());
			        	  break;
			          case "TP_CNTRPFN":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdbrocli());
			        	  break;
			          case "TP_DTEEXP":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_DTEFLWF":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_DTEFLWL":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_DTEFST":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_DTEFXGF":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getFeejecuc();
			        	  break;
			          case "TP_DTEFXGL":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getFeejecuc();
			        	  break;
			          case "TP_DTELST":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getFeejecuc();
			        	  break;
			          case "TP_DTEPMT":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_DTETRN":
			        	  // TP_DTETRN se corresponde con el campo FEVALORR
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_ENTITY":
			        	  cadenaValores = cadenaValores + "|BCH-MAD";
			        	  break;
			          case "TP_INT":
			        	  // Campo que indica si es cartera propia TP_INT, si en la tabla TMCT0ALI tiene 'S' al interface hay que mover 'Y'
			        	  Character tpInt = queryDatos.getOwnAccount();
			        	  if (tpInt != null && (Character.toUpperCase(tpInt)) == 'S') {
			        		  tpInt = 'Y';
			        	  }
			        	  cadenaValores = cadenaValores + "|" + tpInt;
			        	  break;
			          case "TP_IPAY":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getEfectivo();
			        	  break;
			          case "TP_IPAYCUR":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getCdmoniso();
			        	  break;
			          case "TP_IPAYDTE":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_IQTY":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getTitulos();
			        	  break;
			          case "TP_IQTYS":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getTitulos();
			        	  break;
			          case "TP_LQTY2":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getEfectivo();
			        	  break;
			          case "TP_LQTYS2":
			        	  // El signo debe salir en negativo cuando se trata de VENTAS
			        	  BigDecimal tpLqtys2 = queryDatos.getEfectivo();
			        	  if (StringUtils.isNotBlank(queryDatos.getCdtoper()) 
			        			  && StringHelper.getStringTrimmed(
			        					  queryDatos.getCdtoper()).equals(Constantes.CDTOPER_SALES_ABBR)) {
			        		  tpLqtys2 = tpLqtys2.negate();	
			        	  }
			        	  cadenaValores = cadenaValores + "|" + tpLqtys2;
			        	  break;
			          case "TP_MOPLST":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_MOPLSTD":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_MOPLSTL":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_NOMCUR":
			        	  cadenaValores = cadenaValores + "|EUR";
			        	  break;
			          case "TP_NOMINAL":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getEfectivo();
			        	  break;
			          case "TP_PFOLIO":
			        	  cadenaValores = cadenaValores + "|SVB";
			        	  break;
			          case "TP_PRICE":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getPrecioRedondeo();
			        	  break;
			          case "TP_QTYEQ":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_QTYEQS":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_SECCNT":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getNbValors());
			        	  break;
			          case "TP_SECCOD":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdisin());
			        	  break;
			          case "TP_SECCODU":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdisin());
			        	  break;
			          case "TP_SECCUR":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdmoniso());
			        	  break;
			          case "TP_SECCURU":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdmoniso());
			        	  break;
			          case "TP_SECFWSD":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_SECISS":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdkgr());
			        	  break;
			          case "TP_SECLOT":
			        	  cadenaValores = cadenaValores + "|1";
			        	  break;
			          case "TP_SECLOTU":
			        	  cadenaValores = cadenaValores + "|1";
			        	  break;
			          case "TP_SECMKT":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdmercad());
			        	  break;
			          case "TP_STATUS2":
			        	  cadenaValores = cadenaValores + "|LIVE";
			        	  break;
			          case "TP_STRIKE":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_STRTGY":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_TRADER":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdusuari());
			        	  break;
			          case "TRN_FMLY":
			        	  cadenaValores = cadenaValores + "|EQD";
			        	  break;
			          case "TRN_GRP":
			        	  cadenaValores = cadenaValores + "|EQUIT";
			        	  break;
			          case "TRN_TYPE":
			        	  cadenaValores = cadenaValores + "|     ";
			        	  break;
			          default:
			            cadenaValores = cadenaValores + "| ";
			        	  break;
			          }
				}
			}

		}
		
		return cadenaValores;
	}

	/**
	 *	Se obtiene contenido del fichero MV separado por delimitadores en base a una query.
	 *	@param queryDatos queryDatos
	 *	@param listaCabecera listaCabecera
	 *	@return String String  
	 */
	public String convertirCadenaValoresMV(QueryDatosMVSCCRDTO queryDatos, List<Object[]> listaCabecera) {
		String cadenaValores = "";
		
		boolean esPrimerLinea = true;
		if (CollectionUtils.isNotEmpty(listaCabecera)) {
			for (Object listCabecera : listaCabecera) {
				if (Integer.valueOf(String.valueOf(((Object[]) listCabecera)[1])) == 0) {
					if (esPrimerLinea) {
						cadenaValores = cadenaValores + " ";
						esPrimerLinea = false;
					} else {
						cadenaValores = cadenaValores + "| ";
					}
				} else {
			          switch (String.valueOf(((Object[]) listCabecera)[0])) {

			          case "NB":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getNbooking());
			        	  break;
			          case "PL_FMV2":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getPrecioRedondeo();
			        	  break;
			          case "PL_FPFCP2":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "PL_FPFRV2":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;  
			          case "PL_INSCUR":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdmoniso());
			        	  break;
			          case "PL_LQTY2":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "PLIRDCUR1":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "PLIRDCUR2":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "PLIRDFPV12":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "PLIRDFPV22":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_BUY":
			        	  cadenaValores = cadenaValores + "|" + StringHelper.getStringTrimmed(queryDatos.getCdtoper());
			        	  break;
			          case "TP_CMCFST":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_CMCLST":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;	  
			          case "TP_DTEPMT":
			        	  cadenaValores = cadenaValores + "|" + (queryDatos.getFevalorr()==null?queryDatos.getFevalor():queryDatos.getFevalorr());
			        	  break;
			          case "TP_LQTY2":
			        	  cadenaValores = cadenaValores + "|" + queryDatos.getEfectivo();
			        	  break;	  
			          case "TP_PFOLIO":
			        	  cadenaValores = cadenaValores + "|SVB";
			        	  break;
			          case "TP_RTAMC02":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          case "TP_RTAMC12":
			        	  cadenaValores = cadenaValores + "| ";
			        	  break;
			          default:
			            cadenaValores = cadenaValores + "| ";
			        	  break;
			          }
				}
			}
		}
		return cadenaValores;
	}
}