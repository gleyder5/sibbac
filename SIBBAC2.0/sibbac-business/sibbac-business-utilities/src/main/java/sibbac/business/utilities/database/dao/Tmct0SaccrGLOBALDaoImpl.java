package sibbac.business.utilities.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class Tmct0SaccrGLOBALDaoImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0SaccrGLOBALDaoImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Object[]> consultarFieldsGLOBAL() {

		LOG.info("INICIO - DAOIMPL - consultarFieldsGLOBAL");

		List<Object[]> listaFields = new ArrayList<Object[]>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT GL.FIELD, GL.MANDATORY FROM TMCT0_SACCR_UDF_GLOBAL GL");

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			listaFields = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFieldsGLOBAL -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFieldsGLOBAL");

		return listaFields;
	}
}
