package sibbac.business.utilities.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.utilities.database.dao.Tmct0SaccrMVDao;
import sibbac.business.utilities.database.model.Tmct0SaccrMV;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SaccrMVBo extends AbstractBo<Tmct0SaccrMV, Integer, Tmct0SaccrMVDao> {
 
}
