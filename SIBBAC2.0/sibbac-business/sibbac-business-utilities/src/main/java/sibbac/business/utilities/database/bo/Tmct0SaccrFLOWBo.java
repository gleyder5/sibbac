package sibbac.business.utilities.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.utilities.database.dao.Tmct0SaccrFLOWDao;
import sibbac.business.utilities.database.model.Tmct0SaccrFLOW;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SaccrFLOWBo extends AbstractBo<Tmct0SaccrFLOW, Integer, Tmct0SaccrFLOWDao> {
 
}
