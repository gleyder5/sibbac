package sibbac.business.utilities.enums;

/**
 * Clase con las enumeraciones utilizadas en Utilities.
 */
public class UtilitiesEnums {

	/**
	 * 	Enumeracion con posibles prefijos para nombres para ficheros de SACCR.
	 */
	public static enum PrefijosNombresFicherosSACCR {

		KAPITI_LAGO_TCS("KAPITI_LAGO_TCS_"),
		KAPITI_LAGO_TMV("KAPITI_LAGO_TMV_"),
		KAPITI_LAGO_TRADE_EQ("KAPITI_LAGO_TRADE_EQ_"),
		KAPITI_LAGO_UDFEQD("KAPITI_LAGO_UDFEQD_");

		private String description;

		private PrefijosNombresFicherosSACCR(String _description) {
			description = _description;
		}

		public String getDescription() {
			return description;
		}
	}
	  
}