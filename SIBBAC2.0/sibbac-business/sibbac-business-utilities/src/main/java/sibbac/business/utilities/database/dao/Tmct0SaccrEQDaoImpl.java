package sibbac.business.utilities.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.utilities.dto.QueryDatosEQSCCRDTO;

@Repository
public class Tmct0SaccrEQDaoImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0SaccrEQDaoImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Object[]> consultarFieldsEQ() {

		LOG.info("INICIO - DAOIMPL - consultarFieldsEQ");

		List<Object[]> listaFields = new ArrayList<Object[]>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT EQ.FIELD,EQ.MANDATORY FROM TMCT0_SACCR_EQ EQ");

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			listaFields = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFieldsEQ -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFieldsEQ");

		return listaFields;
	}
	
	/**
	 *	Se obtiene DTO con los datos de SCCR para EQ.
	 *	@return QueryDatosSCCRDTO
	 *	@throws Exception   
	 */
	public List<QueryDatosEQSCCRDTO> getDatosEQSCCRByQuery() throws Exception {
		List<QueryDatosEQSCCRDTO> queryDatosSCCRDTO = new ArrayList<QueryDatosEQSCCRDTO>();
		
		final StringBuilder queryString =  new StringBuilder();
		
		// Los importes que estamos moviendo del campo IMTOTBRU, han de ser del campo EUTOTBRU
		
		queryString.append(
			"SELECT bok.nuorden, bok.NBOOKING, bok.NBVALORS, cli.CDKGR, cli.cdbrocli, alo.fevalor, "
				+ "bok.FEEJECUC, alo.FEVALORR, ali.OWN_ACCOUNT, sum(alo.nutitcli) AS titulos, "
				+ "bok.cdisin, ord.cdmercad, ord.cdusuari, "
				+ "CASE WHEN min(alo.cdtpoper) ='C' THEN 'B' ELSE 'S' END AS CDTOPER, "
				+ "decimal(round(decimal(sum(alo.EUTOTBRU), 15, 2) / decimal(sum(nutitcli), 15, 0), 2), 15, 2) AS PRECIO_REDONDEO, "
				+ "alo.cdmoniso, sum(alo.EUTOTBRU) AS efectivo ");

		queryString.append(
			"FROM BSNBPSQL.TMCT0BOK AS BOK INNER JOIN BSNBPSQL.TMCT0ORD AS ORD ON bok.nuorden = ord.nuorden "
				 + "INNER JOIN bsnbpsql.tmct0alo AS alo ON bok.nuorden = alo.nuorden AND bok.NBOOKING = alo.nbooking "
				 + "INNER JOIN bsnbpsql.tmct0ali AS ali ON bok.cdalias = ali.cdaliass AND ali.FHFINAL >= to_char(current_date,'yyyymmdd') "
				 + "INNER JOIN bsnbpsql.tmct0cli AS cli ON ali.CDBROCLI = cli.cdbrocli and cli.FHFINAL >= to_char(current_date,'yyyymmdd') "
				 + "INNER JOIN bsnbpsql.tmct0est AS est ON est.cdaliass = bok.cdalias and est.fhfinal>=to_char(current date,'yyyymmdd') ");
		
		queryString.append(
			"WHERE ((ord.CDCLSMDO = 'E' AND bok.cdestado BETWEEN 300 AND 400) OR "
				+ "(ord.CDCLSMDO = 'N' AND bok.CDESTADOASIG > 0 and bok.CDESTADOASIG < 85)) "
				+ "	AND alo.fevalor > current_date "
				+ "	AND (est.STLEV1LM) in (select nUorden1 from bsnbpsql.tmct0oni where cdnivel1 <> 'MIN') "
		);
		
		queryString.append(
			"GROUP BY bok.nuorden, bok.nbooking, bok.NBVALORS, cli.CDKGR, cli.cdbrocli, alo.FEVALOR, bok.feejecuc, "
				+ "alo.fevalorr, ali.own_account, bok.cdisin, ord.cdmercad, ord.cdusuari, alo.CDTPOPER, alo.CDMONISO");
		
		
		Query query = this.entityManager.createNativeQuery(queryString.toString());
		
		List<Object[]> resultList;
		
		try{
			resultList = query.getResultList();
			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					QueryDatosEQSCCRDTO dto =  QueryDatosEQSCCRDTO.convertObjectToQueryDatosDTO((Object[]) obj);
					queryDatosSCCRDTO.add(dto);
				}
			}
		} catch(Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return queryDatosSCCRDTO;
	}
	
}