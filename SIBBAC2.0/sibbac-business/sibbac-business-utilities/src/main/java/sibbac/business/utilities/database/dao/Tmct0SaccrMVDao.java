package sibbac.business.utilities.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.utilities.database.model.Tmct0SaccrMV;

/**
 * Data access object de Tmct0SaccrMV.
 */
@Repository
public interface Tmct0SaccrMVDao extends JpaRepository<Tmct0SaccrMV, Integer> {

}
