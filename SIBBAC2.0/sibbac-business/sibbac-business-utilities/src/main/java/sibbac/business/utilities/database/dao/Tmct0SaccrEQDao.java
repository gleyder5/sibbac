package sibbac.business.utilities.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.utilities.database.model.Tmct0SaccrEQ;

/**
 * Data access object de Tmct0SaccrEQ.
 */
@Repository
public interface Tmct0SaccrEQDao extends JpaRepository<Tmct0SaccrEQ, Integer> {

}
