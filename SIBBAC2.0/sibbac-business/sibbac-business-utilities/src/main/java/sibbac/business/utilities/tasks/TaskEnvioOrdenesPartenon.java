package sibbac.business.utilities.tasks;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.utilities.bo.EjecucionEnvioOrdenesPartenon;
import sibbac.business.utilities.exceptions.SendMailExceptionUtil;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.model.Tmct0men;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ENVIO_ORDENES_PARTENON.NAME,
           name = Task.GROUP_ENVIO_ORDENES_PARTENON.JOB_ENVIO_ORDENES_PARTENON,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "20:30:00")
public class TaskEnvioOrdenesPartenon extends SIBBACTask {
  
  private static final Logger LOG = LoggerFactory.getLogger(TaskEnvioOrdenesPartenon.class);
	
	@Autowired
	Tmct0desgloseclititBo desgloseBo;

	@Autowired
	Tmct0movimientoeccBo movimientoeccBo;

	@Autowired
	private Tmct0menBo tmct0menBo;

	@Autowired
	private EjecucionEnvioOrdenesPartenon ejecucionEnvioOrdenesPartenon;
	
	@Autowired
	private SendMail sendMail;

	@Value("${sibbac.accounting.cuerpo.mail.desgloseclitit}")
	private String bodyMail;

	@Value("${sibbac.accounting.cuerpo.mail.desgloseclitit.diferentesValores}")
	private String bodyMailIdRepetidos;

	@Value("${sibbac.accounting.cuerpo.mail.desgloseclitit.valoresVacios}")
	private String bodyMailValoresVacios;

	@Value("${sibbac.accounting.destinatario.mail.desgloseclitit}")
	private String toMail;

	@Value("${sibbac.accounting.cuerpo.mail.erroresFicheeros}")
	private String bodyMailErroresFicheros;
	
  @Override
	public void execute() {

    LOG.debug("execute - TaskEnvioOrdenesPartenon - INICIO");
		Tmct0men tmct0men = null;
		
		try {
			tmct0men = this.tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_ENVIO_ORDENES_PARTENON, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);
			// Actualizacion de todos los registros de DesglosesClitit en funcion de MovimientosECC
			LOG.debug("Inicio actualizacionDesglosesClitit ");
			this.actualizacionDesglosesClitit();
			LOG.debug("Fin actualizacionDesglosesClitit ");
			
			ejecucionEnvioOrdenesPartenon.ejecutarEnvioOrdenesPartenon();
			tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_ENVIO_ORDENES_PARTENON, TMCT0MSC.EN_EJECUCION, TMCT0MSC.LISTO_GENERAR);				
		} catch (SIBBACBusinessException e) {
			tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
			LOG.error("execute - " + Constantes.UTILITIES + "- ERROR - Ejecución Task Envio Ordenes Partenón - ", e);
		} catch (Exception ex) {
			tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);		
			LOG.error("execute - TaskEnvioOrdenesPartenon - ERROR - Ejecución Task Envio Ordenes Partenón - ", ex);
			LOG.debug("execute - TaskEnvioOrdenesPartenon - FIN");
		}
		LOG.debug("execute - " + Constantes.UTILITIES + "- FIN - ");
	}
	
  /**
   * 
   * @throws SendMailExceptionUtil
   */
	private void actualizacionDesglosesClitit() throws SendMailExceptionUtil {

		List<Object[]> listMovimientoecc = movimientoeccBo.findDistinctIdDesglose();

		desgloseBo.updateDatosCompensacionDesgloseClitit(listMovimientoecc);

		List<Object[]> listaErroneos = movimientoeccBo.findMismoIdDesgloseDistintosValoresError();
		List<String> listaErroneosVacios = desgloseBo.getListaErroneos();

		if (listaErroneos.size() > 0 || listaErroneosVacios.size() > 0) {

			sendMailErroresClitit(toMail, "Errores desglose clitit",
					bodyMail + bodyMailIdRepetidos + StringHelper.getListaStringIdDesgloseCamara(listaErroneos)
					+ bodyMailValoresVacios + listaErroneosVacios.toString());
		}

	}
	
	/**
	 * Enviamos email con los errores que hayan ocurrido
	 * @param toMail
	 * @param subjectMail
	 * @param bodyMail
	 * @throws SendMailExceptionUtil
	 */
	private void sendMailErroresClitit(String toMail, String subjectMail, String bodyMail) throws SendMailExceptionUtil {

		try {
			sendMail.sendMail(toMail, subjectMail, bodyMail);
		} catch (IllegalArgumentException | MessagingException | IOException e) {
			LOG.error("Error al enviar mail", e);
			throw new SendMailExceptionUtil("Error al enviar mail", e);
		}
	}
	
}