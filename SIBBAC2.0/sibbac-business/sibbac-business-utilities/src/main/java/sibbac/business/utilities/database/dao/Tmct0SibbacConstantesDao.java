package sibbac.business.utilities.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;

/**
 * Data access object de Tmct0SibbacContantes.
 */
@Repository
public interface Tmct0SibbacConstantesDao extends JpaRepository<Tmct0SibbacConstantes, Integer> {
	
	@Query( "SELECT const FROM Tmct0SibbacConstantes const WHERE const.clave = :clave" )
	public List<Tmct0SibbacConstantes> findByClave(@Param( "clave" ) String clave);

	@Query( "SELECT const FROM Tmct0SibbacConstantes const WHERE const.clave = :clave AND const.valor = :valor" )
	public Tmct0SibbacConstantes findByClaveAndValor(@Param( "clave" ) String clave, @Param( "valor" ) String valor);
	
}
