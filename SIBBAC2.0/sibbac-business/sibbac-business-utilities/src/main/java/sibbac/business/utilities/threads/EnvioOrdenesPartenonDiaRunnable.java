package sibbac.business.utilities.threads;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.utilities.bo.EjecucionEnvioOrdenesPartenon;
import sibbac.business.utilities.dto.ResultadoEnvioPartenonDTO;
import sibbac.business.wrappers.database.dto.DatosForQueryPartenonDTO;

/**
 *	Clase que implementa Runnable para procesar por fecha. 
 */
public class EnvioOrdenesPartenonDiaRunnable implements Callable<List<ResultadoEnvioPartenonDTO>> {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(EnvioOrdenesPartenonDiaRunnable.class);
	
	private final DatosForQueryPartenonDTO datosForQueryPartenonDTO;
	
	/** Identificador de hilo que se esta creando. */
	private final int iNumJob;

	/** Servicio con los metodos que realizan las operaciones */
	private final EjecucionEnvioOrdenesPartenon ejecucionEnvioOrdenesPartenon;

	private Future<?> future;
	/**
	 *	Constructor args. 
	 */
	public EnvioOrdenesPartenonDiaRunnable(
			EjecucionEnvioOrdenesPartenon ejecucionEnvioOrdenesPartenon,
			DatosForQueryPartenonDTO datosForQueryPartenonDTO,
			int iNumJob) {
		this.ejecucionEnvioOrdenesPartenon = ejecucionEnvioOrdenesPartenon;
		this.datosForQueryPartenonDTO = datosForQueryPartenonDTO;
		this.iNumJob = iNumJob;
	}
	
	@Override
	public List<ResultadoEnvioPartenonDTO> call() throws Exception {
		final List<ResultadoEnvioPartenonDTO> dto;
		
		if(future != null && future.isCancelled()) {
			LOG.info("[call] NumJob {} para el dia {} ha sido cancelado", iNumJob, 
					datosForQueryPartenonDTO.getFechaEjecucion().toString());
			return null;
		}
		LOG.debug("[call] Inicio NumJob {} Para el dia - {}", iNumJob, 
				datosForQueryPartenonDTO.getFechaEjecucion().toString());
		dto = ejecucionEnvioOrdenesPartenon.ejecutaQueryEnvioPartenon(datosForQueryPartenonDTO);
		LOG.debug("[FIN] NumJob {} Para el dia - {}", iNumJob, 
				datosForQueryPartenonDTO.getFechaEjecucion().toString());
		return dto;
	}

	public void setFuture(Future<?> future) {
		this.future = future;
	}

}
