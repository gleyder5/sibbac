package sibbac.business.utilities.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *	DTO para el tipo de fichero MV. 
 */
public class MVDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5156732206744112108L;
	
	private String nb;
	private BigDecimal plFmv2;
	private BigDecimal plFpfcp2;
	private BigDecimal plFpfrv2;
	private String plInscur;
	private BigDecimal plLqty2;
	private String plirdcur1;
	private String plirdcur2;
	private BigDecimal plirdfpv12;
	private BigDecimal plirdfpv22;
	private Character tpBuy;
	private String tpCmcfst;
	private String tpCmclst;	
	private Date tpDtepmt;
	private BigDecimal tpLqty2;
	private String tpPfolio;
	private BigDecimal tpRtamc02;
	private BigDecimal tpRtamc12;

	/**
	 *	Constructor no-arg. 
	 */
	public MVDTO() {
	}

	public String getNb() {
		return this.nb;
	}

	public void setNb(String nb) {
		this.nb = nb;
	}

	public BigDecimal getPlFmv2() {
		return this.plFmv2;
	}

	public void setPlFmv2(BigDecimal plFmv2) {
		this.plFmv2 = plFmv2;
	}

	public BigDecimal getPlFpfcp2() {
		return this.plFpfcp2;
	}

	public void setPlFpfcp2(BigDecimal plFpfcp2) {
		this.plFpfcp2 = plFpfcp2;
	}

	public BigDecimal getPlFpfrv2() {
		return this.plFpfrv2;
	}

	public void setPlFpfrv2(BigDecimal plFpfrv2) {
		this.plFpfrv2 = plFpfrv2;
	}

	public String getPlInscur() {
		return this.plInscur;
	}

	public void setPlInscur(String plInscur) {
		this.plInscur = plInscur;
	}

	public BigDecimal getPlLqty2() {
		return this.plLqty2;
	}

	public void setPlLqty2(BigDecimal plLqty2) {
		this.plLqty2 = plLqty2;
	}

	public String getPlirdcur1() {
		return this.plirdcur1;
	}

	public void setPlirdcur1(String plirdcur1) {
		this.plirdcur1 = plirdcur1;
	}

	public String getPlirdcur2() {
		return this.plirdcur2;
	}

	public void setPlirdcur2(String plirdcur2) {
		this.plirdcur2 = plirdcur2;
	}

	public BigDecimal getPlirdfpv12() {
		return this.plirdfpv12;
	}

	public void setPlirdfpv12(BigDecimal plirdfpv12) {
		this.plirdfpv12 = plirdfpv12;
	}

	public BigDecimal getPlirdfpv22() {
		return this.plirdfpv22;
	}

	public void setPlirdfpv22(BigDecimal plirdfpv22) {
		this.plirdfpv22 = plirdfpv22;
	}

	public Character getTpBuy() {
		return this.tpBuy;
	}

	public void setTpBuy(Character tpBuy) {
		this.tpBuy = tpBuy;
	}

	public String getTpCmcfst() {
		return this.tpCmcfst;
	}

	public void setTpCmcfst(String tpCmcfst) {
		this.tpCmcfst = tpCmcfst;
	}

	public String getTpCmclst() {
		return this.tpCmclst;
	}

	public void setTpCmclst(String tpCmclst) {
		this.tpCmclst = tpCmclst;
	}

	public Date getTpDtepmt() {
		return this.tpDtepmt;
	}

	public void setTpDtepmt(Date tpDtepmt) {
		this.tpDtepmt = tpDtepmt;
	}

	public BigDecimal getTpLqty2() {
		return this.tpLqty2;
	}

	public void setTpLqty2(BigDecimal tpLqty2) {
		this.tpLqty2 = tpLqty2;
	}

	public String getTpPfolio() {
		return this.tpPfolio;
	}

	public void setTpPfolio(String tpPfolio) {
		this.tpPfolio = tpPfolio;
	}

	public BigDecimal getTpRtamc02() {
		return this.tpRtamc02;
	}

	public void setTpRtamc02(BigDecimal tpRtamc02) {
		this.tpRtamc02 = tpRtamc02;
	}

	public BigDecimal getTpRtamc12() {
		return this.tpRtamc12;
	}

	public void setTpRtamc12(BigDecimal tpRtamc12) {
		this.tpRtamc12 = tpRtamc12;
	}	
	
}