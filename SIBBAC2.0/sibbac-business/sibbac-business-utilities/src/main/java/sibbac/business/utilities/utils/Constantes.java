package sibbac.business.utilities.utils;

/**
 * The Class Constantes para business de Utilities.
 */
public class Constantes {

	/** Sufijo para el nombre de ficheros generados en SACCR. */
	public static final String SUFIJO_FICHEROS_SACCR = "_ES.dat";
	
	/** Tipos de ficheros para generar con SACCR. */
	public static final String FICHERO_EQUITY_SPOT = "FICHERO_EQUITY_SPOT";
	public static final String FICHERO_MARKET_VALUE = "FICHERO_MARKET_VALUE";
	public static final String FICHERO_FLOW = "FICHERO_FLOW";
	public static final String FICHERO_UDF_GLOBAL = "FICHERO_UDF_GLOBAL";
	
	/** Abreviacion de SALES para CDTOPER. */
	public static final String CDTOPER_SALES_ABBR = "S";
	
}