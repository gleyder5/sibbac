package sibbac.business.utilities.dto.assembler;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.utilities.dto.ResultadoEnvioPartenonDTO;
import sibbac.business.utilities.helper.EnvioOrdenesPartenonHelper;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dto.AnticipoSituacionDTO;
import sibbac.business.wrappers.database.dto.DatosForQueryPartenonDTO;
import sibbac.business.wrappers.database.dto.ParamsFicheroEnvioPartenonDTO;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;

/**
 * Clase que realiza la transformacion entre DTOs y datos obtenidos de bundles.
 */
@Service
public class EnvioOrdenesPartenonAssembler {

  final SimpleDateFormat formatDate = new SimpleDateFormat(FormatDataUtils.DATE_FORMAT_1);

  @Autowired
  private Tmct0cfgDao tmct0cfgDao;
  
  @Value("${dias.anticipo_situacion}")
  private Integer diasAnticipoSituacion;
  
  @Value("${dia_inicio.anticipo_situacion}")
  private String diaInicioAnticipoSituacion;
  
  @Value("${cdrefasignacion.anticipo_situacion}")
  private String cdrefasignacion;
  
  @Value("${cdmiembrocompensador.anticipo_situacion}")
  private String cdmiembrocompensador;
  
  @Value("${cdestadoasig.anticipo_situacion}")
  private Integer cdestadoasig;
  
  @Value("${cdestadotit.anticipo_situacion}")
  private Integer cdestadotit;
  
  @Value("${procedencia.anticipo_situacion}")
  private String procedenciaAnticipoSituacion;
  
  @Value("${procedencia2.anticipo_situacion}")
  private String procedencia2AnticipoSituacion;
  
  @Value("${propuesta.anticipo_situacion}")
  private String propuestaAnticipoSituacion;
  
  @Value("${routing.alias.anticipo_situacion}")
  private String routingAliasAnticipoSituacion;
  
  @Value("${especial.alias.anticipo_situacion}")
  private String especialAliasAnticipoSituacion;
  
  @Value("${cd_plataforma_negociacion.anticipo_situacion}")
  private String cdPlatafNegocAnticSituacion;
    
  public EnvioOrdenesPartenonAssembler() {
    super();
  }

  /**	
   * 	Se devuelve un DTO con los campos formateados para grabacion posterior.
   * 	@param el DTO con los datos necesarios para el formato
   * 	@param el DTO con datos que vienen de la query
   * 	@return el DTO con los campos listos para grabarse
   * 	@throws SIBBACBusinessException   
   */
  public AnticipoSituacionDTO getLineaAnticipoSituacion(
		  ParamsFicheroEnvioPartenonDTO params, ResultadoEnvioPartenonDTO ordenPartenon) throws SIBBACBusinessException {
	  AnticipoSituacionDTO linea = new AnticipoSituacionDTO();
	  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	  
	  //Seteo del campo 1
	  if (null != ordenPartenon.getDcliFeejecuc()) {
		  linea.setFechaEjec(sdf.format(ordenPartenon.getDcliFeejecuc()));
	  }
	  
	  //Seteo del campo 2
	  linea.setMiembro(ordenPartenon.getDcliCdmiembromkt());
	  
	  //Seteo del campo 3
	  linea.setSentido(ordenPartenon.getDcliCdsentido());
	  
	  //Seteo del campo 4
	  if (null != ordenPartenon.getDcliImtitulos()) {
		  linea.setTitulos(ordenPartenon.getDcliImtitulos().toBigIntegerExact().toString());
	  }
	  
	  //Seteo del campo 5
	  linea.setIsin(ordenPartenon.getDcliCdisin());
	  
	  //Seteo del campo 6
	  if (null != ordenPartenon.getDcliImprecio()) {
		  linea.setPrecio(ordenPartenon.getDcliImprecio());
	  }
	  
	  //Seteo del campo 7
	  if (null != ordenPartenon.getDcliNurefexemkt()) {
		  linea.setNumEjec(ordenPartenon.getDcliNurefexemkt());
	  }
	  
	  //Seteo del campo 8
	  linea.setProcedencia(EnvioOrdenesPartenonHelper.obtenerProcedencia(params, ordenPartenon));
	  
	  //Seteo del campo 9
	  linea.setPropuesta(EnvioOrdenesPartenonHelper.obtenerPropuesta(params, ordenPartenon));
	  
	  //Seteo del campo 10
	  linea.setRefAdisional(ordenPartenon.getRtReferenciaAdicional());
	  
	  //Seteo del campo 11
	  linea.setRefAsignacion(ordenPartenon.getDcliCdRefAsignacion());

	  return linea;
  }
  
  
  /**	
   * 	Se devuelve un DTO con los campos necesarios para correr la query.
   * 	@param fecha de ejecucion que viene con el hilo
   * 	@return el DTO con los datos necesarios para correr la query
   * 	@throws SIBBACBusinessException   
   */
  public DatosForQueryPartenonDTO getDatosForQueryPartenon(Date fechaEjecucion) throws SIBBACBusinessException {
	  
	  SimpleDateFormat formatter = new SimpleDateFormat(FormatDataUtils.DATE_FORMAT_1);
	  DatosForQueryPartenonDTO datosForQueryPartenonDTO = new DatosForQueryPartenonDTO();
	  datosForQueryPartenonDTO.setDiasAnticipoSituacion(diasAnticipoSituacion);
	  if (FormatDataUtils.isDateValid(diaInicioAnticipoSituacion, FormatDataUtils.DATE_FORMAT_1)) {
		  try {
			datosForQueryPartenonDTO.setDiaInicioAnticipoSituacion(formatter.parse(diaInicioAnticipoSituacion));
		} catch (ParseException e) {
			throw new SIBBACBusinessException(e); 
		}
	  }
	  datosForQueryPartenonDTO.setFechaEjecucion(fechaEjecucion);
	  datosForQueryPartenonDTO.setCdrefasignacion(cdrefasignacion);
	  datosForQueryPartenonDTO.setCdmiembrocompensador(cdmiembrocompensador);
	  datosForQueryPartenonDTO.setCdestadoasig(cdestadoasig);
	  datosForQueryPartenonDTO.setCdestadotit(cdestadotit);
	  datosForQueryPartenonDTO.setCdPlataformaNegociacion(cdPlatafNegocAnticSituacion);
	  return datosForQueryPartenonDTO;
  }
  
  	/**
	 * Devuelve una lista de fechas para ejecucion de la query ordenes partenon.
	 * 
	 * @return Lista de fechas de ejecucion
	 * @throws SIBBACBusinessException
	 */
	public List<Date> getListaFechasEjecucion() throws SIBBACBusinessException {

		List<Date> listaFechasEjecucion = new ArrayList<Date>();
		List<Date> listaFechasEjecucionValidas = new ArrayList<Date>();
		Date primerDiaTratable = null;

		// El número de días hacía atrás a procesar vendrá definido por el
		// parámetro dias.anticipo_situacion
		if (diasAnticipoSituacion != null && !diasAnticipoSituacion.equals(0)) {
			for (int j = 0; j < diasAnticipoSituacion; j++) {
				final Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -j);
				listaFechasEjecucion.add(cal.getTime());
			}
		} else {
			// 1- Si existe el parámetro dia_inicio.anticipo_situacion y tiene el
			// valor de una fecha en formato aaaa-mm-dd,
			// se cogerá este como primer día tratable posible.
			if (FormatDataUtils.isDateValid(diaInicioAnticipoSituacion, FormatDataUtils.DATE_FORMAT_1)) {
				try {
					primerDiaTratable = formatDate.parse(diaInicioAnticipoSituacion);
				} catch (ParseException e) {
					throw new SIBBACBusinessException(e); 
				}
			} else {
				// 2- Si no existe el parámetro anteriormente mencionado, o existiendo
				// su valor es distinto a una fecha en formato aaaa-mm-dd, se cogerá como primer
				// día tratable posible el día de la ejecución del proceso.
				primerDiaTratable = new Date();
			}
			// Se agrega el 1er dia tratable a la lista.
			listaFechasEjecucion.add(primerDiaTratable);
		}

		if (CollectionUtils.isNotEmpty(listaFechasEjecucion)) {
			for (Date fechaEjecucion : listaFechasEjecucion) {
				// 3-a. Si la fecha pertenece a un sábado o domingo no es un día válido para el proceso.
				// 3-b. Si la fecha es un festivo en el mercado español, no es un día válido para el proceso. 
				// Si la siguiente query devuelve algún registro será un festivo en mercado español 
				// (<fecha> llevará la fecha que estamos validando en formato aaaa-mm-dd)
				if (this.isDiaHabil(fechaEjecucion)) {
					listaFechasEjecucionValidas.add(fechaEjecucion);
					GenericLoggers.logDebug(
						EnvioOrdenesPartenonAssembler.class, Constantes.UTILITIES, "getListaFechasEjecucion", 
							"Fecha de ejecucion valida: " + fechaEjecucion);
				}
			}
		}

		if (diasAnticipoSituacion != null) {
			if (diasAnticipoSituacion == listaFechasEjecucionValidas.size()) {
				// 5-	Si ya hemos obtenido tantas fechas como indique el parámetro dias.anticipo_situacion, 
				// se finalizará la búsqueda de fechas a procesar.
				return listaFechasEjecucionValidas;			
			} else if (diasAnticipoSituacion > listaFechasEjecucionValidas.size()) {
				try {
					// 6-	Si todavía nos faltan fechas, se obtendrá el día anterior a la fecha que se acaba de validar 
					// y se volverá a realizar este proceso desde el punto 3.
					
					// Se obtiene el dia anterior a la fecha valida en la lista mas antigua.
					Date fechaAnteriorAAgregar = DateHelper.restaDias(
							Collections.min(listaFechasEjecucionValidas), 1);
					while (diasAnticipoSituacion > listaFechasEjecucionValidas.size()) {
						if (this.isDiaHabil(fechaAnteriorAAgregar)) {
							listaFechasEjecucionValidas.add(fechaAnteriorAAgregar);
							// Se loguea fecha de ejecucion calculada
							GenericLoggers.logDebug(
								EnvioOrdenesPartenonAssembler.class, Constantes.UTILITIES, 
									"getListaFechasEjecucion", "Fecha de ejecucion valida: " + fechaAnteriorAAgregar);
						}
						// Se sigue restando a la fecha anterior.
						fechaAnteriorAAgregar = DateHelper.restaDias(
							fechaAnteriorAAgregar, 1);
					}
				} catch (Exception e) {
					throw new SIBBACBusinessException(e);
				}
			}
		}
		return listaFechasEjecucionValidas;
	}
	
	/**
	 *	Devuelve true si se devuelve un dia habil (ni feriado ni festivo). 
	 * 	@return boolean
	 * 	@throws SIBBACBusinessException
	 */
	public boolean isDiaHabil(Date fechaEjecucion) throws SIBBACBusinessException {
		return !Utilidad.isWeekEnd(fechaEjecucion) 
			&& CollectionUtils.isEmpty(this.tmct0cfgDao.findByProcessKeyNameLikeAndKeyValue(
				"CONFIG", "%holydays.anual.days%", formatDate.format(fechaEjecucion)));
	}
	
	/**
	 * 	Se carga en un listado de DTOs los resultados de la query 
	 * 	de envio orden partenon.	
	 * 
	 * 	@param ordenesPartenon
	 * 	@return List<ResultadoEnvioPartenonDTO>
	 * 	@throws SIBBACBusinessException	
	 */
	public List<ResultadoEnvioPartenonDTO> getDTOListFromQueryEnvioOrdPartenResults(
			List<Object[]> ordenesPartenon) throws SIBBACBusinessException {
		List<ResultadoEnvioPartenonDTO> listResultadoQueryEnvioPartenon = new ArrayList<ResultadoEnvioPartenonDTO>();
		
		if (CollectionUtils.isNotEmpty(ordenesPartenon)) {
			for (Object[] ordenPartenon : ordenesPartenon) {
				ResultadoEnvioPartenonDTO resEnvPartenDTO = new ResultadoEnvioPartenonDTO();
				resEnvPartenDTO.setAlcNuorden((String) ordenPartenon[0]);
				resEnvPartenDTO.setAlcNbooking((String) ordenPartenon[1]);
				resEnvPartenDTO.setAlcNucnfclt((String) ordenPartenon[2]);
				resEnvPartenDTO.setAlcNucnfliq((BigDecimal) ordenPartenon[3]);
				resEnvPartenDTO.setDcliFeejecuc((Date) ordenPartenon[4]);
				resEnvPartenDTO.setDcliCdmiembromkt((String) ordenPartenon[5]);
				resEnvPartenDTO.setDcliCdsentido((Character) ordenPartenon[6]);
				resEnvPartenDTO.setDcliImtitulos((BigDecimal) ordenPartenon[7]);
				resEnvPartenDTO.setDcliCdisin((String) ordenPartenon[8]);
				resEnvPartenDTO.setDcliImprecio((BigDecimal) ordenPartenon[9]);
				resEnvPartenDTO.setDcliNurefexemkt((String) ordenPartenon[10]);
				resEnvPartenDTO.setOrdCdcuenta((String) ordenPartenon[11]);
				resEnvPartenDTO.setOrdNureford((String) ordenPartenon[12]);
				resEnvPartenDTO.setRtReferenciaAdicional((String) ordenPartenon[13]);
				resEnvPartenDTO.setDcliCdRefAsignacion((String) ordenPartenon[14]);
				listResultadoQueryEnvioPartenon.add(resEnvPartenDTO);
			}
		}
		return listResultadoQueryEnvioPartenon;
	}

	/**
	 *	Se obtienen parametros para el fichero de envio Partenon.
	 *	@return ParamsFicheroEnvioPartenonDTO 
	 *	@throws SIBBACBusinessException 
	 */
	public ParamsFicheroEnvioPartenonDTO getParamsFicheroEnvioPartenonDTO() throws SIBBACBusinessException {
		ParamsFicheroEnvioPartenonDTO params = new ParamsFicheroEnvioPartenonDTO();
		params.setProcedenciaAnticipoSituacion(procedenciaAnticipoSituacion);
		params.setProcedencia2AnticipoSituacion(procedencia2AnticipoSituacion);
		params.setPropuestaAnticipoSituacion(propuestaAnticipoSituacion);
		params.setRoutingAliasAnticipoSituacion(routingAliasAnticipoSituacion);
		params.setEspecialAliasAnticipoSituacion(especialAliasAnticipoSituacion);
		return params;
	}
}
