package sibbac.business.utilities.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class Tmct0SaccrFLOWDaoImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0SaccrFLOWDaoImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Object[]> consultarFieldsFLOW() {

		LOG.info("INICIO - DAOIMPL - consultarFieldsFLOW");

		List<Object[]> listaFields = new ArrayList<Object[]>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT SF.FIELD, SF.MANDATORY FROM TMCT0_SACCR_FLOW SF");

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			listaFields = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFieldsFLOW -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFieldsFLOW");

		return listaFields;
	}
}
