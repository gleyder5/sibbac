package sibbac.business.utilities.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *	DTO para la query con datos de SCCR para MV. 
 */
public class QueryDatosMVSCCRDTO extends QueryDatosBaseSCCRDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3502738954300996572L;

	private BigDecimal impTotBru;
	
	/**
	 * 	Constructor no-arg.
	 */
	public QueryDatosMVSCCRDTO() {}

	public BigDecimal getImpTotBru() {
		return this.impTotBru;
	}

	public void setImpTotBru(BigDecimal impTotBru) {
		this.impTotBru = impTotBru;
	}
	
	/**
	 *	Pasa un array de objetos al DTO.
	 *	@param obj
	 *	@return QueryDatosMVSCCRDTO 
	 */
	public static QueryDatosMVSCCRDTO convertObjectToQueryDatosDTO(Object[] obj) {
		
		QueryDatosMVSCCRDTO dto = new QueryDatosMVSCCRDTO();
		if (obj[0] != null) {
			dto.setNuorden(((String) obj[0]).trim());			
		}
		if (obj[1] != null) {
			dto.setNbooking(((String) obj[1]).trim());			
		}
		dto.setFevalor((Date) obj[2]);
		dto.setFevalorr((Date) obj[3]);		
		if (obj[4] != null) {
			dto.setCdtoper(((String) obj[4]).trim());			
		}
		dto.setPrecioRedondeo((BigDecimal) obj[5]);
		if (obj[6] != null) {
			dto.setCdmoniso(((String) obj[6]).trim());			
		}
		dto.setEfectivo((BigDecimal) obj[7]);
		return dto;
	}

}