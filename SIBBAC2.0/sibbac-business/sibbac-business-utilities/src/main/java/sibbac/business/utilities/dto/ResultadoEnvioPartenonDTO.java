package sibbac.business.utilities.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *	DTO con resultados de la ejecucion de la query de envio Partenon. 
 */
public class ResultadoEnvioPartenonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5885654809538300589L;
	
	private String alcNuorden;
	private String alcNbooking;
	
	// alc.nucnfclt (query) - Campo 9
	private String alcNucnfclt;
	private BigDecimal alcNucnfliq;
	
	// dcli.feejecuc (query) - Campo 1
	private Date dcliFeejecuc;
	
	// dcli.cdmiembromkt (query) - Campo 2
	private String dcliCdmiembromkt;
	
	// dcli.cdsentido (query) - Campo 3
	private Character dcliCdsentido;
	
	// dcli.imtitulos (query) - Campo 4
	private BigDecimal dcliImtitulos;
	
	// dcli.cdisin (query) - Campo 5 
	private String dcliCdisin;
	
	// dcli.imprecio (query) - Campo 6
	private BigDecimal dcliImprecio;
	
	// dcli.nurefexemkt (query) - Campo 7
	private String dcliNurefexemkt;
	
	// ord.cdcuenta (query) - Campo 8
	private String ordCdcuenta;
	
	// ord.nureford (query) - Campo 9
	private String ordNureford;
	
	// rt.referencia_adicional (query) - Campo 10
	private String rtReferenciaAdicional;
	
	// dcli.cd_ref_asignacion (query) - Campos 8 y 10
	private String dcliCdRefAsignacion;
	
	/**
	 *	Constructor no-arg. 
	 */
	public ResultadoEnvioPartenonDTO() {
		super();
	}

	/**
	 *	Constructor arg.
	 *	@param alcNuorden
	 *	@param alcNbooking
	 *	@param alcNucnfclt
	 *	@param alcNucnfliq
	 *	@param dcliFeejecuc
	 *	@param dcliCdmiembromkt
	 *	@param dcliCdsentido
	 *	@param dcliImtitulos
	 *	@param dcliCdisin
	 *	@param dcliImprecio
	 *	@param dcliNurefexemkt
	 *	@param dcliCdRefAsignacion
	 *	@param ordCdcuenta
	 *	@param ordNureford
	 *	@param rtReferenciaAdicional 
	 */
	public ResultadoEnvioPartenonDTO(String alcNuorden, String alcNbooking,
			String alcNucnfclt, BigDecimal alcNucnfliq, Date dcliFeejecuc,
			String dcliCdmiembromkt, Character dcliCdsentido,
			BigDecimal dcliImtitulos, String dcliCdisin,
			BigDecimal dcliImprecio, String dcliNurefexemkt,
			String ordCdcuenta, String ordNureford,
			String rtReferenciaAdicional, String dcliCdRefAsignacion) {
		super();
		this.alcNuorden = alcNuorden;
		this.alcNbooking = alcNbooking;
		this.alcNucnfclt = alcNucnfclt;
		this.alcNucnfliq = alcNucnfliq;
		this.dcliFeejecuc = dcliFeejecuc;
		this.dcliCdmiembromkt = dcliCdmiembromkt;
		this.dcliCdsentido = dcliCdsentido;
		this.dcliImtitulos = dcliImtitulos;
		this.dcliCdisin = dcliCdisin;
		this.dcliImprecio = dcliImprecio;
		this.dcliNurefexemkt = dcliNurefexemkt;
		this.ordCdcuenta = ordCdcuenta;
		this.ordNureford = ordNureford;
		this.rtReferenciaAdicional = rtReferenciaAdicional;
		this.dcliCdRefAsignacion = dcliCdRefAsignacion;
	}
	
	/**
	 *	@return  alcNuorden
	 */
	public String getAlcNuorden() {
		return this.alcNuorden;
	}

	/**
	 *	@param alcNuorden 
	 */
	public void setAlcNuorden(String alcNuorden) {
		this.alcNuorden = alcNuorden;
	}

	/**
	 *	@return  alcNbooking
	 */
	public String getAlcNbooking() {
		return this.alcNbooking;
	}

	/**
	 *	@param alcNbooking 
	 */
	public void setAlcNbooking(String alcNbooking) {
		this.alcNbooking = alcNbooking;
	}

	/**
	 *	@return  nucnfclt
	 */
	public String getAlcNucnfclt() {
		return this.alcNucnfclt;
	}

	/**
	 *	@param alcNucnfclt 
	 */
	public void setAlcNucnfclt(String alcNucnfclt) {
		this.alcNucnfclt = alcNucnfclt;
	}

	/**
	 *	@return  alcNucnfliq
	 */
	public BigDecimal getAlcNucnfliq() {
		return this.alcNucnfliq;
	}

	/**
	 *	@param alcNucnfliq 
	 */
	public void setAlcNucnfliq(BigDecimal alcNucnfliq) {
		this.alcNucnfliq = alcNucnfliq;
	}

	/**
	 *	@return  dcliFeejecuc
	 */
	public Date getDcliFeejecuc() {
		return this.dcliFeejecuc;
	}

	/**
	 *	@param dcliFeejecuc 
	 */
	public void setDcliFeejecuc(Date dcliFeejecuc) {
		this.dcliFeejecuc = dcliFeejecuc;
	}

	/**
	 *	@return  dcliCdmiembromkt
	 */
	public String getDcliCdmiembromkt() {
		return this.dcliCdmiembromkt;
	}

	/**
	 *	@param dcliCdmiembromkt 
	 */
	public void setDcliCdmiembromkt(String dcliCdmiembromkt) {
		this.dcliCdmiembromkt = dcliCdmiembromkt;
	}

	/**
	 *	@return  dcliCdsentido
	 */
	public Character getDcliCdsentido() {
		return this.dcliCdsentido;
	}

	/**
	 *	@param dcliCdsentido 
	 */
	public void setDcliCdsentido(Character dcliCdsentido) {
		this.dcliCdsentido = dcliCdsentido;
	}

	/**
	 *	@return  dcliImtitulos
	 */
	public BigDecimal getDcliImtitulos() {
		return this.dcliImtitulos;
	}

	/**
	 *	@param dcliImtitulos 
	 */
	public void setDcliImtitulos(BigDecimal dcliImtitulos) {
		this.dcliImtitulos = dcliImtitulos;
	}

	/**
	 *	@return  dcliCdisin
	 */
	public String getDcliCdisin() {
		return this.dcliCdisin;
	}

	/**
	 *	@param dcliCdisin 
	 */
	public void setDcliCdisin(String dcliCdisin) {
		this.dcliCdisin = dcliCdisin;
	}

	/**
	 *	@return  dcliImprecio
	 */
	public BigDecimal getDcliImprecio() {
		return this.dcliImprecio;
	}

	/**
	 *	@param dcliImprecio 
	 */
	public void setDcliImprecio(BigDecimal dcliImprecio) {
		this.dcliImprecio = dcliImprecio;
	}

	/**
	 *	@return  dcliNurefexemkt
	 */
	public String getDcliNurefexemkt() {
		return this.dcliNurefexemkt;
	}

	/**
	 *	@param dcliNurefexemkt 
	 */
	public void setDcliNurefexemkt(String dcliNurefexemkt) {
		this.dcliNurefexemkt = dcliNurefexemkt;
	}

	/**
	 *	@return  dcliCdRefAsignacion
	 */
	public String getDcliCdRefAsignacion() {
		return this.dcliCdRefAsignacion;
	}

	/**
	 *	@param dcliCdRefAsignacion 
	 */
	public void setDcliCdRefAsignacion(String dcliCdRefAsignacion) {
		this.dcliCdRefAsignacion = dcliCdRefAsignacion;
	}

	/**
	 *	@return  ordCdcuenta
	 */
	public String getOrdCdcuenta() {
		return this.ordCdcuenta;
	}

	/**
	 *	@param ordCdcuenta 
	 */
	public void setOrdCdcuenta(String ordCdcuenta) {
		this.ordCdcuenta = ordCdcuenta;
	}

	/**
	 *	@return  ordNureford
	 */
	public String getOrdNureford() {
		return this.ordNureford;
	}

	/**
	 *	@param ordNureford 
	 */
	public void setOrdNureford(String ordNureford) {
		this.ordNureford = ordNureford;
	}

	/**
	 *	@return  rtReferenciaAdicional
	 */
	public String getRtReferenciaAdicional() {
		return this.rtReferenciaAdicional;
	}

	/**
	 *	@param rtReferenciaAdicional 
	 */
	public void setRtReferenciaAdicional(String rtReferenciaAdicional) {
		this.rtReferenciaAdicional = rtReferenciaAdicional;
	}

	/**
	 *	Obtiene identificadores de ALC agrupados para la actualizacion.
	 *	@return String 
	 */
	public String getGrupoIdentificadorALC() {
		return "('" + this.alcNuorden + "', '" + this.alcNbooking + "', '" + this.alcNucnfclt + "', " + this.alcNucnfliq + "), "; 
	}
	
}