package sibbac.business.utilities.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *	DTO para la query con datos de SCCR. 
 */
public class QueryDatosEQSCCRDTO extends QueryDatosBaseSCCRDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2263488440846728444L;
	
	private String nbValors;
	private String cdkgr;
	private String cdbrocli;
	private Date feejecuc;
	private Character ownAccount;
	private BigDecimal titulos;
	private String cdisin;
	private String cdmercad;
	private String cdusuari;
	
	/**
	 * 	Constructor no-arg.
	 */
	public QueryDatosEQSCCRDTO() {}

	public String getNbValors() {
		return this.nbValors;
	}

	public void setNbValors(String nbValors) {
		this.nbValors = nbValors;
	}

	public String getCdkgr() {
		return this.cdkgr;
	}

	public void setCdkgr(String cdkgr) {
		this.cdkgr = cdkgr;
	}

	public String getCdbrocli() {
		return this.cdbrocli;
	}

	public void setCdbrocli(String cdbrocli) {
		this.cdbrocli = cdbrocli;
	}

	public Date getFeejecuc() {
		return this.feejecuc;
	}

	public void setFeejecuc(Date feejecuc) {
		this.feejecuc = feejecuc;
	}

	public Character getOwnAccount() {
		return this.ownAccount;
	}

	public void setOwnAccount(Character ownAccount) {
		this.ownAccount = ownAccount;
	}

	public BigDecimal getTitulos() {
		return this.titulos;
	}

	public void setTitulos(BigDecimal titulos) {
		this.titulos = titulos;
	}

	public String getCdisin() {
		return this.cdisin;
	}

	public void setCdisin(String cdisin) {
		this.cdisin = cdisin;
	}

	public String getCdmercad() {
		return this.cdmercad;
	}

	public void setCdmercad(String cdmercad) {
		this.cdmercad = cdmercad;
	}

	public String getCdusuari() {
		return this.cdusuari;
	}

	public void setCdusuari(String cdusuari) {
		this.cdusuari = cdusuari;
	}

	/**
	 *	Pasa un array de objetos al DTO.
	 *	@param obj
	 *	@return QueryDatosSCCRDTO 
	 */
	public static QueryDatosEQSCCRDTO convertObjectToQueryDatosDTO(Object[] obj) {
		QueryDatosEQSCCRDTO dto = new QueryDatosEQSCCRDTO();
		if (obj[0] != null) {
			dto.setNuorden(((String) obj[0]).trim());			
		}
		if (obj[1] != null) {
			dto.setNbooking(((String) obj[1]).trim());			
		}
		if (obj[2] != null) {
			dto.setNbValors(((String) obj[2]).trim());			
		}
		if (obj[3] != null) {
			dto.setCdkgr(((String) obj[3]).trim());			
		}
		if (obj[4] != null) {
			dto.setCdbrocli(((String) obj[4]).trim());			
		}
		dto.setFevalor((Date) obj[5]);
		dto.setFeejecuc((Date) obj[6]);
		dto.setFevalorr((Date) obj[7]);
		dto.setOwnAccount((Character) obj[8]);
		dto.setTitulos((BigDecimal) obj[9]);
		if (obj[10] != null) {
			dto.setCdisin(((String) obj[10]).trim());
		}
		if (obj[11] != null) {
			dto.setCdmercad(((String) obj[11]).trim());			
		}
		if (obj[12] != null) {
			dto.setCdusuari(((String) obj[12]).trim());			
		}
		if (obj[13] != null) {
			dto.setCdtoper(((String) obj[13]).trim());			
		}
		dto.setPrecioRedondeo((BigDecimal) obj[14]);
		if (obj[13] != null) {
			dto.setCdmoniso(((String) obj[15]).trim());			
		}
		dto.setEfectivo((BigDecimal) obj[16]);
		return dto;
	}

}