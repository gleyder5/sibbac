package sibbac.business.utilities.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.utilities.database.dao.Tmct0SaccrGLOBALDao;
import sibbac.business.utilities.database.model.Tmct0SaccrGLOBAL;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SaccrGLOBALBo extends AbstractBo<Tmct0SaccrGLOBAL, Integer, Tmct0SaccrGLOBALDao> {
 
}
