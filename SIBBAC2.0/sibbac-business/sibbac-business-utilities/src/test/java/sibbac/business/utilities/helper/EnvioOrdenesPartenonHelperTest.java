package sibbac.business.utilities.helper;

import org.junit.Test;

import sibbac.business.utilities.dto.ResultadoEnvioPartenonDTO;
import sibbac.business.wrappers.database.dto.ParamsFicheroEnvioPartenonDTO;
import sibbac.common.SIBBACBusinessException;

import org.junit.Before;

import static org.junit.Assert.*;

public class EnvioOrdenesPartenonHelperTest {
  
  private static final String PROCEDENCIA = "0049PI-TESORER,2898-*,2899-*,0049";
  
  private static final String PROCEDENCIAS2 = "4716-ROUSAN,22680-BARSAN,22672-ROUOPBK,23615-123OPBK,23573-123SAN,22271-DIR123,22272-CVSAN,21941-COMSAN,22431-OPAOPV,22432-OPAOPV,22433-RTMASAN,22434-RTMASAN,22444-RTMASAN,22673-RTMASAN,22435-RTBASAN,22436-RTBASAN,22437-RTBISAN,22438-RTBISAN,22439-RTVASAN,22440-RTVASAN,22441-REINSAN,3365-RRFSAN,4322-RINSSAN,20340-SPB,22645-SPBROU,22762-SPB,22764-SPB,22763-SPB,22765-SPB,22766-SPB,22767-SPB,4760-SPB,22446-SPB";
  
  private static final String ANTICIPO_SITUACION = "all";

  private final ParamsFicheroEnvioPartenonDTO params; 
  
  private ResultadoEnvioPartenonDTO ordenPartenon;
  
  public EnvioOrdenesPartenonHelperTest() {
    params = new ParamsFicheroEnvioPartenonDTO();
    params.setProcedenciaAnticipoSituacion(PROCEDENCIA);
    params.setProcedencia2AnticipoSituacion(PROCEDENCIAS2);
    params.setPropuestaAnticipoSituacion(ANTICIPO_SITUACION);
  }
  
  @Before
  public void before() {
    ordenPartenon = new ResultadoEnvioPartenonDTO();
  }
  
  @Test
  public void obtenerProcedenciaVaciaTest() throws SIBBACBusinessException {
    final String procedencia;
    
    procedencia = EnvioOrdenesPartenonHelper.obtenerProcedencia(params, ordenPartenon);
    assertEquals("<ERROR>", procedencia);
  }
  
  @Test
  public void obtenerProcedenciaSPBROU22645Test() throws SIBBACBusinessException {
    final String procedencia;

    ordenPartenon.setDcliCdRefAsignacion("2898");
    ordenPartenon.setOrdCdcuenta("22645");
    procedencia = EnvioOrdenesPartenonHelper.obtenerProcedencia(params, ordenPartenon);
    assertEquals("SPBROU-22645", procedencia);
  }

  @Test
  public void obtenerProcedenciaROUSAN4716Test() throws SIBBACBusinessException {
    final String procedencia;

    ordenPartenon.setDcliCdRefAsignacion("2898");
    ordenPartenon.setOrdCdcuenta("4716");
    procedencia = EnvioOrdenesPartenonHelper.obtenerProcedencia(params, ordenPartenon);
    assertEquals("ROUSAN-4716", procedencia);
  }
  
  @Test
  public void obtenerProcedencia9999Test() throws SIBBACBusinessException {
    final String procedencia;

    ordenPartenon.setDcliCdRefAsignacion("2898");
    ordenPartenon.setOrdCdcuenta("9999");
    procedencia = EnvioOrdenesPartenonHelper.obtenerProcedencia(params, ordenPartenon);
    assertEquals("BOLPLUS-9999", procedencia);
  }
  
}
