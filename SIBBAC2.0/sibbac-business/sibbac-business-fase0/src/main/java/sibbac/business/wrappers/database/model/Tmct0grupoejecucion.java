package sibbac.business.wrappers.database.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0GRUPOEJECUCION")
public class Tmct0grupoejecucion implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -3347829592501315178L;
  private Long idgrupoeje;
  private Tmct0alo tmct0alo;
  private BigDecimal nutitulos;
  private Date fevalor;
  private BigDecimal impreciomkt;
  private String cdtipoop;
  private String cdsettmkt;
  private String cdcamara;
  private Date auditFechaCambio;
  private String auditUser;
  private Character asigcomp;
  private String tradingvenue;
  private String nuagreje;
  private Date feejecuc;
  private List<Tmct0ejecucionalocation> tmct0ejecucionalocations = new ArrayList<Tmct0ejecucionalocation>(0);

  public Tmct0grupoejecucion() {
  }

  public Tmct0grupoejecucion(Tmct0grupoejecucion grupoejecucion) {
    this.idgrupoeje = null;
    this.tmct0alo = null;
    this.nutitulos = BigDecimal.ZERO;
    this.fevalor = grupoejecucion.getFevalor();
    this.impreciomkt = grupoejecucion.getImpreciomkt();
    this.cdtipoop = grupoejecucion.getCdtipoop();
    this.cdsettmkt = grupoejecucion.getCdsettmkt();
    this.cdcamara = grupoejecucion.getCdcamara();
    this.auditFechaCambio = new Date();
    this.auditUser = grupoejecucion.getAuditUser();
    this.asigcomp = grupoejecucion.getAsigcomp();
    this.tradingvenue = grupoejecucion.getTradingvenue();
    this.nuagreje = grupoejecucion.getNuagreje();
    this.feejecuc = grupoejecucion.getFeejecuc();
  }

  public Tmct0grupoejecucion(Long idgrupoeje, Tmct0alo tmct0alo, BigDecimal nutitulos, Date fevalor,
      BigDecimal impreciomkt, String cdtipoop, String cdsettmkt, String nuagreje) {
    this.idgrupoeje = idgrupoeje;
    this.tmct0alo = tmct0alo;
    this.nutitulos = nutitulos;
    this.fevalor = fevalor;
    this.impreciomkt = impreciomkt;
    this.cdtipoop = cdtipoop;
    this.cdsettmkt = cdsettmkt;
    this.nuagreje = nuagreje;
  }

  public Tmct0grupoejecucion(Long idgrupoeje, Tmct0alo tmct0alo, BigDecimal nutitulos, Date fevalor,
      BigDecimal impreciomkt, String cdtipoop, String cdsettmkt, String cdcamara, Date auditFechaCambio,
      String auditUser, Character asigcomp, String tradingvenue, String nuagreje, Date feejecuc,
      List<Tmct0ejecucionalocation> tmct0ejecucionalocations) {
    this.idgrupoeje = idgrupoeje;
    this.tmct0alo = tmct0alo;
    this.nutitulos = nutitulos;
    this.fevalor = fevalor;
    this.impreciomkt = impreciomkt;
    this.cdtipoop = cdtipoop;
    this.cdsettmkt = cdsettmkt;
    this.cdcamara = cdcamara;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.asigcomp = asigcomp;
    this.tradingvenue = tradingvenue;
    this.nuagreje = nuagreje;
    this.feejecuc = feejecuc;
    this.tmct0ejecucionalocations = tmct0ejecucionalocations;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDGRUPOEJE", unique = true, nullable = false)
  public Long getIdgrupoeje() {
    return this.idgrupoeje;
  }

  public void setIdgrupoeje(Long idgrupoeje) {
    this.idgrupoeje = idgrupoeje;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false) })
  public Tmct0alo getTmct0alo() {
    return this.tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  @Column(name = "NUTITULOS", nullable = false, precision = 13)
  public BigDecimal getNutitulos() {
    return this.nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", nullable = false, length = 10)
  public Date getFevalor() {
    return this.fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  @Column(name = "IMPRECIOMKT", nullable = false, precision = 13, scale = 3)
  public BigDecimal getImpreciomkt() {
    return this.impreciomkt;
  }

  public void setImpreciomkt(BigDecimal impreciomkt) {
    this.impreciomkt = impreciomkt;
  }

  @Column(name = "CDTIPOOP", nullable = false, length = 2)
  public String getCdtipoop() {
    return this.cdtipoop;
  }

  public void setCdtipoop(String cdtipoop) {
    this.cdtipoop = cdtipoop;
  }

  @Column(name = "CDSETTMKT", nullable = false, length = 3)
  public String getCdsettmkt() {
    return this.cdsettmkt;
  }

  public void setCdsettmkt(String cdsettmkt) {
    this.cdsettmkt = cdsettmkt;
  }

  @Column(name = "CDCAMARA", length = 20)
  public String getCdcamara() {
    return this.cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "ASIGCOMP", length = 1)
  public Character getAsigcomp() {
    return this.asigcomp;
  }

  public void setAsigcomp(Character asigcomp) {
    this.asigcomp = asigcomp;
  }

  @Column(name = "TRADINGVENUE", length = 20)
  public String getTradingvenue() {
    return this.tradingvenue;
  }

  public void setTradingvenue(String tradingvenue) {
    this.tradingvenue = tradingvenue;
  }

  @Column(name = "NUAGREJE", nullable = false, length = 20)
  public String getNuagreje() {
    return this.nuagreje;
  }

  public void setNuagreje(String nuagreje) {
    this.nuagreje = nuagreje;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC", length = 10)
  public Date getFeejecuc() {
    return this.feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0grupoejecucion")
  public List<Tmct0ejecucionalocation> getTmct0ejecucionalocations() {
    return this.tmct0ejecucionalocations;
  }

  public void setTmct0ejecucionalocations(List<Tmct0ejecucionalocation> tmct0ejecucionalocations) {
    this.tmct0ejecucionalocations = tmct0ejecucionalocations;
  }

  public static Tmct0grupoejecucion clone(Tmct0grupoejecucion origen, List<String> notSetThisFieldNames) {
    try {
      if (notSetThisFieldNames == null) {
        notSetThisFieldNames = new ArrayList<String>();
      }// if (notSetThisFieldNames == null)
      Tmct0grupoejecucion destino = (Tmct0grupoejecucion) origen.getClass().newInstance();
      Field[] fields = destino.getClass().getDeclaredFields();
      // Method[] metodos = origen.getClass().getMethods();
      if (fields != null && fields.length > 0) {
        for (int i = 0; i < fields.length; i++) {
          Field field = fields[i];
          String fieldName = field.getName();
          if (notSetThisFieldNames.contains(fieldName)) {
            continue;
          }// if ( notSetThisFieldNames.contains( fieldName ) ) {
          try {
            String fieldNameForMethod = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            Method set = destino.getClass().getMethod("set" + fieldNameForMethod, field.getType());
            Method get = destino.getClass().getMethod("get" + fieldNameForMethod);
            if (set != null && get != null) {
              Object value = get.invoke(origen);
              if (value != null) {
                if (value instanceof Timestamp) {
                  value = new Timestamp(((Timestamp) value).getTime());
                }
                else if (value instanceof Time) {
                  value = new Time(((Time) value).getTime());
                }
                else if (value instanceof java.sql.Date) {
                  value = new java.sql.Date(((java.sql.Date) value).getTime());
                }
                else if (value instanceof Date) {
                  value = new Date(((Date) value).getTime());
                }
                else if (value instanceof Collection) {
                  // value.
                }
                try {
                  set.invoke(destino, value);
                }
                catch (Exception e) {

                }

              }// if ( value != null ){
            }// if ( set != null && get != null )

          }
          catch (NoSuchMethodException e) {

          }
        }// for (int i=0;i<fields.length;i++)
      }// if (fields != null && fields.length >0)
      return destino;
    }
    catch (IllegalArgumentException e) {

    }
    catch (SecurityException e) {

    }
    catch (InstantiationException e) {

    }
    catch (IllegalAccessException e) {

    }
    catch (InvocationTargetException e) {

    }
    return null;
  }// public static Tmct0grupoejecucion clone( Tmct0grupoejecucion origen, List<
   // String > notSetThisFieldNames ) {

}
