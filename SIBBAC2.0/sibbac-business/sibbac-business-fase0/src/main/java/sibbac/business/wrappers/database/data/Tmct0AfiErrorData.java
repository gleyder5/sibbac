package sibbac.business.wrappers.database.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0AfiError;

public class Tmct0AfiErrorData {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0AfiErrorData.class);

  protected Long id;
  protected Long version = 0L;
  protected Date auditDate;
  protected String auditUser;
  protected String ccv;
  protected String cddepais;
  protected String cdnactit;
  protected String cdpostal;
  protected String cdreferenciatitular;
  protected String idmensaje;
  protected String nbciudad;
  protected String nbclient;
  protected String nbclient1;
  protected String nbclient2;
  protected String nbdomici;
  protected String nbprovin;
  protected String nudnicif;
  protected String nudomici;
  protected String nureford;
  protected Short nucnfliq;
  protected BigDecimal particip;
  protected String rftitaud;
  protected String tpdomici;
  protected Tmct0AloData tmct0AloData;
  protected Character tpidenti;
  protected Character tpnactit;
  protected Character tpsocied;
  protected Character tptiprep;
  private Set<CodigoErrorData> errors = new HashSet<CodigoErrorData>();

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() {
    return this.version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public Date getAuditDate() {
    return this.auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public String getCcv() {
    return this.ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  public String getCddepais() {
    return this.cddepais;
  }

  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  public String getCdnactit() {
    return this.cdnactit;
  }

  public void setCdnactit(String cdnactit) {
    this.cdnactit = cdnactit;
  }

  public String getCdpostal() {
    return this.cdpostal;
  }

  public void setCdpostal(String cdpostal) {
    this.cdpostal = cdpostal;
  }

  public String getCdreferenciatitular() {
    return this.cdreferenciatitular;
  }

  public void setCdreferenciatitular(String cdreferenciatitular) {
    this.cdreferenciatitular = cdreferenciatitular;
  }

  public String getIdmensaje() {
    return this.idmensaje;
  }

  public void setIdmensaje(String idmensaje) {
    this.idmensaje = idmensaje;
  }

  public String getNbciudad() {
    return this.nbciudad;
  }

  public void setNbciudad(String nbciudad) {
    this.nbciudad = nbciudad;
  }

  public String getNbclient() {
    return this.nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public String getNbclient1() {
    return this.nbclient1;
  }

  public void setNbclient1(String nbclient1) {
    this.nbclient1 = nbclient1;
  }

  public String getNbclient2() {
    return this.nbclient2;
  }

  public void setNbclient2(String nbclient2) {
    this.nbclient2 = nbclient2;
  }

  public String getNbdomici() {
    return this.nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  public String getNbprovin() {
    return this.nbprovin;
  }

  public void setNbprovin(String nbprovin) {
    this.nbprovin = nbprovin;
  }

  public String getNudnicif() {
    return this.nudnicif;
  }

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  public String getNudomici() {
    return this.nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  public String getNureford() {
    return this.nureford;
  }

  public void setNureford(String nureford) {
    this.nureford = nureford;
  }

  public Short getNucnfliq() {
    return this.nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public BigDecimal getParticip() {
    return this.particip;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  public String getRftitaud() {
    return this.rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  public String getTpdomici() {
    return this.tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  public Tmct0AloData getTmct0AloData() {
    return this.tmct0AloData;
  }

  public void setTmc0AloData(Tmct0AloData tmct0AloData) {
    this.tmct0AloData = tmct0AloData;
  }

  public Character getTpidenti() {
    return this.tpidenti;
  }

  public void setTpidenti(Character tpidenti) {
    this.tpidenti = tpidenti;
  }

  public Character getTpnactit() {
    return this.tpnactit;
  }

  public void setTpnactit(Character tpnactit) {
    this.tpnactit = tpnactit;
  }

  public Character getTpsocied() {
    return this.tpsocied;
  }

  public void setTpsocied(Character tpsocied) {
    this.tpsocied = tpsocied;
  }

  public Character getTptiprep() {
    return this.tptiprep;
  }

  public void setTptiprep(Character tptiprep) {
    this.tptiprep = tptiprep;
  }

  public Set<CodigoErrorData> getErrors() {
    return errors;
  }

  public void setErrors(Set<CodigoErrorData> errors) {
    this.errors = errors;
  }

  public void addError(CodigoErrorData error) {
    if (errors == null) {
      errors = new HashSet<CodigoErrorData>();
    }
    this.errors.add(error);
  }

  public Tmct0AfiErrorData(Tmct0AfiError tmct0AfiError) {
    entityToData(tmct0AfiError, this);
  }

  public Tmct0AfiErrorData() {
  }

  public static Tmct0AfiErrorData entityToData(Tmct0AfiError entity) {
    Tmct0AfiErrorData data = new Tmct0AfiErrorData();
    entityToData(entity, data);
    return data;
  }// public static Tmct0AfiErrorData entityToData(Tmct0AfiError entity){

  public static void entityToData(Tmct0AfiError entity, Tmct0AfiErrorData data) {
    data.setAuditDate(entity.getAuditDate());
    data.setAuditUser(entity.getAuditUser());
    data.setCcv(entity.getCcv());
    data.setCddepais(entity.getCddepais());
    data.setCdnactit(entity.getCdnactit());
    data.setCdpostal(entity.getCdpostal());
    data.setCdreferenciatitular(entity.getCdreferenciatitular());
    data.setErrors(CodigoErrorData.entityListToDataList(entity.getErrors()));
    data.setId(entity.getId());
    data.setIdmensaje(entity.getIdmensaje());
    data.setNbciudad(entity.getNbciudad());
    data.setNbclient(entity.getNbclient());
    data.setNbclient1(entity.getNbclient1());
    data.setNbclient2(entity.getNbclient2());
    data.setNbdomici(entity.getNbdomici());
    data.setNbprovin(entity.getNbprovin());
    data.setNudnicif(entity.getNudnicif());
    data.setNudomici(entity.getNudomici());
    data.setNureford(entity.getNureford());
    data.setNucnfliq(entity.getNucnfliq());
    data.setParticip(entity.getParticip());
    data.setRftitaud(entity.getRftitaud());
    data.setTpdomici(entity.getTpdomici());
    data.setTmc0AloData(Tmct0AloData.entityToData(entity.getTmct0alo()));
    data.setTpidenti(entity.getTpidenti());
    data.setTpnactit(entity.getTpnactit());
    data.setTpsocied(entity.getTpsocied());
    data.setTptiprep(entity.getTptiprep());
    data.setVersion(entity.getVersion());
  }// public static void entityToData(Tmct0AfiError entity, Tmct0AfiErrorData
   // data)

  public static void entityToData(PartenonRecordBean partenonRecordBean, Tmct0AfiErrorData tmct0afierrorData) {
    tmct0afierrorData.setIdmensaje(partenonRecordBean.getIdMensajeConcatenado());
    tmct0afierrorData.setNudnicif(partenonRecordBean.getNifbic());
    tmct0afierrorData.setNbclient(partenonRecordBean.getNbclient());
    tmct0afierrorData.setNbclient1(partenonRecordBean.getNbclient1());
    tmct0afierrorData.setNbclient2(partenonRecordBean.getNbclient2());
    tmct0afierrorData.setNureford(partenonRecordBean.getNumOrden());
    // Aqui hay una calle y numero en afi
    if (partenonRecordBean.getDomicili() != null && partenonRecordBean.getDomicili().trim().length() == 2) {
      tmct0afierrorData.setTpdomici(partenonRecordBean.getDomicili().substring(0, 2));
    }
    else if (partenonRecordBean.getDomicili() != null && partenonRecordBean.getDomicili().length() > 3) {
      tmct0afierrorData.setTpdomici(partenonRecordBean.getDomicili().substring(0, 2));
      tmct0afierrorData.setNbdomici(partenonRecordBean.getDomicili().substring(3));
    }
    else {
      tmct0afierrorData.setTpdomici(partenonRecordBean.getTpdomici());
      tmct0afierrorData.setNbdomici(partenonRecordBean.getNbdomici());
      tmct0afierrorData.setNudomici(partenonRecordBean.getNudomici());
    }
    tmct0afierrorData.setCdpostal(partenonRecordBean.getDistrito().toString());
    tmct0afierrorData.setNbciudad(partenonRecordBean.getPoblacion());
    tmct0afierrorData.setNbprovin(partenonRecordBean.getProvincia());
    if (partenonRecordBean.getNacionalidad() != null) {
      tmct0afierrorData.setCdnactit(partenonRecordBean.getNacionalidad().toString());
    }
    if (partenonRecordBean.getTiptitu() != null) {
      tmct0afierrorData.setTptiprep(partenonRecordBean.getTiptitu().toString().charAt(0));
    }
    tmct0afierrorData.setCddepais(partenonRecordBean.getPaisResidencia());
    if (partenonRecordBean.getTipoDocumento() != null) {
      tmct0afierrorData.setTpidenti(partenonRecordBean.getTipoDocumento().toString().charAt(0));
    }
    if (partenonRecordBean.getTipoPersona() != null) {
      tmct0afierrorData.setTpsocied(partenonRecordBean.getTipoPersona().toString().charAt(0));
    }
    if (partenonRecordBean.getIndNac() != null) {
      tmct0afierrorData.setTpnactit(partenonRecordBean.getIndNac().toString().charAt(0));
    }
    tmct0afierrorData.setCcv(partenonRecordBean.getCcv());
    tmct0afierrorData.setErrors(CodigoErrorData.entityListToDataList(partenonRecordBean.getErrorSet()));
    tmct0afierrorData.setParticip(partenonRecordBean.getParticip());
    tmct0afierrorData.setRftitaud(partenonRecordBean.getNifbic());
    tmct0afierrorData.setTmc0AloData(Tmct0AloData.entityToData(partenonRecordBean.getTmct0alo()));
    if (partenonRecordBean.getNucnfliq() == null) {
      tmct0afierrorData.setNucnfliq((short) 0);
    }
    else {
      tmct0afierrorData.setNucnfliq(partenonRecordBean.getNucnfliq());
    }

  }

  public boolean lengthsAreValid() {
    boolean lengthsAreValid = true;
    // this.tmct0alo = afiError.getTmct0alo();
    // this.nureford = afiError.getNureford();
    // this.nucnfliq = afiError.getNucnfliq();
    if (this.tpdomici != null && this.tpdomici.length() > 2) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] tpdomici is longer than expected ");
    }
    if (this.nbdomici != null && this.nbdomici.length() > 40) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbdomici is longer than expected ");
    }
    if (this.nudomici != null && this.nudomici.length() > 4) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nudomici is longer than expected ");
    }
    if (this.nbciudad != null && this.nbciudad.length() > 40) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbciudad is longer than expected ");
    }
    if (this.nbprovin != null && this.nbprovin.length() > 25) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbprovin is longer than expected ");
    }
    if (this.cdpostal != null && this.cdpostal.length() > 5) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cdpostal is longer than expected ");
    }
    if (this.cddepais != null && this.cddepais.length() > 3) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cddepais is longer than expected ");
    }
    if (this.nudnicif != null && this.nudnicif.length() > 11) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nudnicif is longer than expected ");
    }
    if (this.nbclient != null && this.nbclient.length() > 30) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbclient is longer than expected ");
    }
    if (this.nbclient1 != null && this.nbclient1.length() > 50) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbclient1 is longer than expected ");
    }
    if (this.nbclient2 != null && this.nbclient2.length() > 30) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbclient2 is longer than expected ");
    }
    if (this.cdnactit != null && this.cdnactit.length() > 3) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cdnactit is longer than expected ");
    }
    if (this.rftitaud != null && this.rftitaud.length() > 25) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] rftitaud is longer than expected ");
    }
    if (this.cdreferenciatitular != null && this.cdreferenciatitular.length() > 20) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cdreferenciatitular is longer than expected ");
    }
    if (this.ccv != null && this.ccv.length() > 20) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] ccv is longer than expected ");
    }
    if (this.idmensaje != null && this.idmensaje.length() > 30) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] idmensaje is longer than expected ");
    }
    return lengthsAreValid;
  }

  public static Tmct0AfiError dataToEntity(Tmct0AfiErrorData data) {
    Tmct0AfiError entity = new Tmct0AfiError();
    Tmct0AfiErrorData.dataToEntity(data, entity);
    return entity;
  }

  public static void dataToEntity(Tmct0AfiErrorData data, Tmct0AfiError entity) {
    Tmct0AfiErrorData.dataToEntityWithoutErrors(data, entity);
    entity.setErrors(CodigoErrorData.dataListToEntityList(data.getErrors()));
  }// public static void entityToData(Tmct0AfiError entity, Tmct0AfiErrorData
   // data)

  public static Tmct0AfiError dataToEntityWithoutErrors(Tmct0AfiErrorData data) {
    Tmct0AfiError entity = new Tmct0AfiError();
    Tmct0AfiErrorData.dataToEntityWithoutErrors(data, entity);
    return entity;
  }

  public static void dataToEntityWithoutErrors(final Tmct0AfiErrorData data, final Tmct0AfiError entity) {
    entity.setAuditDate(data.getAuditDate());
    entity.setAuditUser(data.getAuditUser());
    entity.setCcv(data.getCcv());
    entity.setCddepais(data.getCddepais());
    entity.setCdnactit(data.getCdnactit());
    entity.setCdpostal(data.getCdpostal());
    entity.setCdreferenciatitular(data.getCdreferenciatitular());
    entity.setId(data.getId());
    entity.setIdmensaje(data.getIdmensaje());
    entity.setNbciudad(data.getNbciudad());
    entity.setNbclient(data.getNbclient());
    entity.setNbclient1(data.getNbclient1());
    entity.setNbclient2(data.getNbclient2());
    entity.setNbdomici(data.getNbdomici());
    entity.setNbprovin(data.getNbprovin());
    entity.setNudnicif(data.getNudnicif());
    entity.setNudomici(data.getNudomici());
    entity.setNureford(data.getNureford());
    entity.setNucnfliq(data.getNucnfliq());
    entity.setParticip(data.getParticip());
    entity.setRftitaud(data.getRftitaud());
    if (data.getTpdomici() != null) {
      entity.setTpdomici(data.getTpdomici().trim());
    }
    entity.setTmct0alo(Tmct0AloData.dataToEntity(data.getTmct0AloData()));
    entity.setTpidenti(data.getTpidenti());
    entity.setTpnactit(data.getTpnactit());
    entity.setTpsocied(data.getTpsocied());
    entity.setTptiprep(data.getTptiprep());
    entity.setVersion(data.getVersion());
  }// public static void entityToData(Tmct0AfiError entity, Tmct0AfiErrorData
   // data)

}// public class Tmct0AfiErrorData {
