package sibbac.business.fase0.database.dto;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class LabelValueObject {

	private String	label;
	private String	value;

	public LabelValueObject() {

	}

	public LabelValueObject( final String label, final String value ) {
		super();
		this.label = label;
		this.value = value;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel( final String label ) {
		this.label = label;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue( final String value ) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		final ToStringBuilder tsb = new ToStringBuilder( ToStringStyle.JSON_STYLE );
		tsb.append( getValue() );
		tsb.append( getLabel() );
		return tsb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( getValue() );
		hcb.append( getLabel() );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object object ) {
		final LabelValueObject labelValueObject = ( LabelValueObject ) object;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( getValue(), labelValueObject.getValue() );
		eqb.append( getLabel(), labelValueObject.getLabel() );
		return eqb.isEquals();
	}

}
