package sibbac.business.canones.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_REGLAS_CANONES_CALCULO_AGRUPADO")
public class CanonAgrupadoRegla extends CanonEntity {

  /**
   * 
   */
  private static final long serialVersionUID = 2428754479755573651L;

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "ID_TIPO_CANON", nullable = false)
  private String tipoCanon;
  @Column(name = "ID_TIPO_CALCULO", nullable = false)
  private String tipoCalculo;

  // CRITERIOS MATCHING
  @Column(name = "FECHA_CONTRATACION", nullable = true)
  private Date fechaContratacion;
  @Column(name = "ID_MERCADO_CONTRATACION", nullable = true)
  private String mercadoContratacion;
  @Column(name = "TITULAR", nullable = true)
  private String titular;
  @Column(name = "ISIN", nullable = true)
  private String isin;
  @Column(name = "TIPO_OPERACION_BOLSA", nullable = true)
  private String tipoOperacionBolsa;
  @Column(name = "SENTIDO", nullable = true)
  private Character sentido;
  @Column(name = "PRECIO", nullable = true)
  private BigDecimal precio;
  @Column(name = "MIEMBRO_MERCADO", nullable = true)
  private String miembroMercado;
  @Column(name = "ORDEN_MERCADO", nullable = true)
  private String ordenMercado;
  @Column(name = "EJECUCION_MERCADO", nullable = true)
  private String ejecucionMercado;
  @Column(name = "ID_SEGMENTO_MERCADO_CONTRATACION", nullable = true)
  private String segmentoMercadoContratacion;
  @Column(name = "TIPO_SUBASTA", nullable = true)
  private String tipoSubasta;

  // COINDICIONES ESPECIALES
  @Column(name = "ORDEN_PUNTO_MEDIO", nullable = false)
  private char ordenPuntoMedio = 'N';
  @Column(name = "ORDEN_OCULTA", nullable = false)
  private char ordenOculta = 'N';
  @Column(name = "ORDEN_BLOQUE_COMBINADO", nullable = false)
  private char ordenBloqueCombinado = 'N';
  @Column(name = "ORDEN_VOLUMEN_OCULTO", nullable = false)
  private char ordenVolumenOculto = 'N';
  @Column(name = "RESTRICCION_ORDEN", nullable = false)
  private char restriccionOrden = 'N';
  @Column(name = "ORDEN_EJECUTADA_SUBASTA", nullable = false)
  private char ordenEjecutadaSubasta = 'N';

  // DATOS ECONOMICOS
  @Column(name = "IMPORTE_EFECTIVO", nullable = false)
  private BigDecimal importeEfectivo = BigDecimal.ZERO;
  @Column(name = "IMPORTE_CANON", nullable = false)
  private BigDecimal importeCanon = BigDecimal.ZERO;
  // @Column(name = "IMPORTE_CANON_ESPECIAL", nullable = false)
  // private BigDecimal importeCanonEspecial = BigDecimal.ZERO;

  @Column(name = "ID_REGLA_CANON_PARAMETRIZACION", nullable = false)
  private Long parametrizacion;

  public long getId() {
    return id;
  }

  public String getTipoCanon() {
    return tipoCanon;
  }

  public String getTipoCalculo() {
    return tipoCalculo;
  }

  public Date getFechaContratacion() {
    return fechaContratacion;
  }

  public String getMercadoContratacion() {
    return mercadoContratacion;
  }

  public String getTitular() {
    return titular;
  }

  public String getIsin() {
    return isin;
  }

  public String getTipoOperacionBolsa() {
    return tipoOperacionBolsa;
  }

  public Character getSentido() {
    return sentido;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public String getOrdenMercado() {
    return ordenMercado;
  }

  public String getEjecucionMercado() {
    return ejecucionMercado;
  }

  public String getSegmentoMercadoContratacion() {
    return segmentoMercadoContratacion;
  }

  public String getTipoSubasta() {
    return tipoSubasta;
  }

  public char getOrdenPuntoMedio() {
    return ordenPuntoMedio;
  }

  public char getOrdenOculta() {
    return ordenOculta;
  }

  public char getOrdenBloqueCombinado() {
    return ordenBloqueCombinado;
  }

  public char getOrdenVolumenOculto() {
    return ordenVolumenOculto;
  }

  public char getOrdenEjecutadaSubasta() {
    return ordenEjecutadaSubasta;
  }

  public BigDecimal getImporteEfectivo() {
    return importeEfectivo;
  }

  public BigDecimal getImporteCanon() {
    return importeCanon;
  }

  public Long getParametrizacion() {
    return parametrizacion;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setTipoCanon(String tipoCanon) {
    this.tipoCanon = tipoCanon;
  }

  public void setTipoCalculo(String tipoCalculo) {
    this.tipoCalculo = tipoCalculo;
  }

  public void setFechaContratacion(Date fechaContratacion) {
    this.fechaContratacion = fechaContratacion;
  }

  public void setMercadoContratacion(String mercadoContratacion) {
    this.mercadoContratacion = mercadoContratacion;
  }

  public void setTitular(String titular) {
    this.titular = titular;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setTipoOperacionBolsa(String tipoOperacionBolsa) {
    this.tipoOperacionBolsa = tipoOperacionBolsa;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setOrdenMercado(String ordenMercado) {
    this.ordenMercado = ordenMercado;
  }

  public void setEjecucionMercado(String ejecucionMercado) {
    this.ejecucionMercado = ejecucionMercado;
  }

  public void setSegmentoMercadoContratacion(String segmentoMercadoContratacion) {
    this.segmentoMercadoContratacion = segmentoMercadoContratacion;
  }

  public void setTipoSubasta(String tipoSubasta) {
    this.tipoSubasta = tipoSubasta;
  }

  public void setOrdenPuntoMedio(char ordenPuntoMedio) {
    this.ordenPuntoMedio = ordenPuntoMedio;
  }

  public void setOrdenOculta(char ordenOculta) {
    this.ordenOculta = ordenOculta;
  }

  public void setOrdenBloqueCombinado(char ordenBloqueCombinado) {
    this.ordenBloqueCombinado = ordenBloqueCombinado;
  }

  public void setOrdenVolumenOculto(char ordenVolumenOculto) {
    this.ordenVolumenOculto = ordenVolumenOculto;
  }

  public void setOrdenEjecutadaSubasta(char ejecucionSubasta) {
    this.ordenEjecutadaSubasta = ejecucionSubasta;
  }

  public void setImporteEfectivo(BigDecimal importeEfectivo) {
    this.importeEfectivo = importeEfectivo;
  }

  public void setImporteCanon(BigDecimal importeCanon) {
    this.importeCanon = importeCanon;
  }

  public void setParametrizacion(Long parametrizacion) {
    this.parametrizacion = parametrizacion;
  }

  public char getRestriccionOrden() {
    return restriccionOrden;
  }

  public void setRestriccionOrden(char restriccionOrden) {
    this.restriccionOrden = restriccionOrden;
  }

  public String getMiembroMercado() {
    return miembroMercado;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  @Override
  public String toString() {
    return String
        .format(
            "CanonAgrupadoRegla [id=%s, tipoCanon=%s, tipoCalculo=%s, fechaContratacion=%s, mercadoContratacion=%s, titular=%s, isin=%s, tipoOperacionBolsa=%s, sentido=%s, precio=%s, miembroMercado=%s, ordenMercado=%s, ejecucionMercado=%s, segmentoMercadoContratacion=%s, tipoSubasta=%s, ordenPuntoMedio=%s, ordenOculta=%s, ordenBloqueCombinado=%s, ordenVolumenOculto=%s, restriccionOrden=%s, ordenEjecutadaSubasta=%s, importeEfectivo=%s, importeCanon=%s, parametrizacion=%s]",
            id, tipoCanon, tipoCalculo, fechaContratacion, mercadoContratacion, titular, isin, tipoOperacionBolsa,
            sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, segmentoMercadoContratacion, tipoSubasta,
            ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto, restriccionOrden,
            ordenEjecutadaSubasta, importeEfectivo, importeCanon, parametrizacion);
  }

}
