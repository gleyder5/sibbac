package sibbac.business.fase0.database.model;

/**
 * This class represent the table TMCT0MFD. MIFID Documentación de la tablaTMCT0MFD <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 

 **/
public class Tmct0mfd {

  /**
   * Table Field CDBROCLI. CLIENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdbrocli;

  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.math.BigDecimal numsec;
  /**
   * Table Field CATEGORY. CATEGORY Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String category;
  /**
   * Table Field CDUSUAUD. AUDIT USER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. AUDIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.sql.Timestamp fhaudit;
  /**
   * Table Field FHINICIO. INIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.math.BigDecimal fhinicio;
  /**
   * Table Field FHFINAL. FINAL DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.math.BigDecimal fhfinal;

  /**
   * Set value of Field CDBROCLI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdbrocli value of the field
   * @see #cdbrocli

   **/
  public void setCdbrocli(java.lang.String _cdbrocli) {
    cdbrocli = _cdbrocli;
  }

  /**
   * Get value of Field CDBROCLI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdbrocli

   **/
  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  /**
   * Set value of Field NUMSEC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _numsec value of the field
   * @see #numsec

   **/
  public void setNumsec(java.math.BigDecimal _numsec) {
    numsec = _numsec;
  }

  /**
   * Get value of Field NUMSEC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #numsec

   **/
  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  /**
   * Set value of Field CATEGORY <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _category value of the field
   * @see #category

   **/
  public void setCategory(java.lang.String _category) {
    category = _category;
  }

  /**
   * Get value of Field CATEGORY <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #category

   **/
  public java.lang.String getCategory() {
    return category;
  }

  /**
   * Set value of Field CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdusuaud value of the field
   * @see #cdusuaud

   **/
  public void setCdusuaud(java.lang.String _cdusuaud) {
    cdusuaud = _cdusuaud;
  }

  /**
   * Get value of Field CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdusuaud

   **/
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * Set value of Field FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _fhaudit value of the field
   * @see #fhaudit

   **/
  public void setFhaudit(java.sql.Timestamp _fhaudit) {
    fhaudit = _fhaudit;
  }

  /**
   * Get value of Field FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #fhaudit

   **/
  public java.sql.Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * Set value of Field FHINICIO <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _fhinicio value of the field
   * @see #fhinicio

   **/
  public void setFhinicio(java.math.BigDecimal _fhinicio) {
    fhinicio = _fhinicio;
  }

  /**
   * Get value of Field FHINICIO <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #fhinicio

   **/
  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }

  /**
   * Set value of Field FHFINAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _fhfinal value of the field
   * @see #fhfinal

   **/
  public void setFhfinal(java.math.BigDecimal _fhfinal) {
    fhfinal = _fhfinal;
  }

  /**
   * Get value of Field FHFINAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #fhfinal

   **/
  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }

}
