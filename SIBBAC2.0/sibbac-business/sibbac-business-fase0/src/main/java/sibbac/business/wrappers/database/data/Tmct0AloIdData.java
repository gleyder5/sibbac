package sibbac.business.wrappers.database.data;

import java.io.Serializable;

import sibbac.business.fase0.database.model.Tmct0aloId;

public class Tmct0AloIdData implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String nucnfclt = "";
    private String nbooking = "";
    private String nuorden = "";
    public Tmct0AloIdData(){
	//
    }
    public Tmct0AloIdData(Tmct0aloId tmct0aloId){
	entityToData(tmct0aloId, this);
    }
    public static Tmct0AloIdData entityToData(Tmct0aloId entity){
	Tmct0AloIdData data = new Tmct0AloIdData();
	entityToData(entity, data);
	return data;
    }
    public static void entityToData(Tmct0aloId entity, Tmct0AloIdData data){
	data.setNbooking(entity.getNbooking());
	data.setNucnfclt(entity.getNucnfclt());
	data.setNuorden(entity.getNuorden());
    }
    public String getNucnfclt() {
        return nucnfclt;
    }
    public void setNucnfclt(String nucnfclt) {
        this.nucnfclt = nucnfclt;
    }
    public String getNbooking() {
        return nbooking;
    }
    public void setNbooking(String nbooking) {
        this.nbooking = nbooking;
    }
    public String getNuorden() {
        return nuorden;
    }
    public void setNuorden(String nuorden) {
        this.nuorden = nuorden;
    }
    
}
