package sibbac.business.wrappers.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ImpuestoId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2455579139157642765L;
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden;
  @Column(name = "NBOOKING", nullable = false, length = 20)
  private String nbooking;
  @Column(name = "NUAGREJE", nullable = false, length = 20)
  private String nuagreje;
  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  private String nucnfclt;
  @Column(name = "NOMBRE", nullable = false, length = 20)
  private String nombre;

  public ImpuestoId() {

  }

  public ImpuestoId(String nuorden, String nbooking, String nuagreje, String nucnfclt, String nombre) {
    super();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nuagreje = nuagreje;
    this.nucnfclt = nucnfclt;
    this.nombre = nombre;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNuagreje() {
    return nuagreje;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNuagreje(String nuagreje) {
    this.nuagreje = nuagreje;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
    result = prime * result + ((nuagreje == null) ? 0 : nuagreje.hashCode());
    result = prime * result + ((nucnfclt == null) ? 0 : nucnfclt.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ImpuestoId other = (ImpuestoId) obj;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    } else if (!nbooking.equals(other.nbooking))
      return false;
    if (nombre == null) {
      if (other.nombre != null)
        return false;
    } else if (!nombre.equals(other.nombre))
      return false;
    if (nuagreje == null) {
      if (other.nuagreje != null)
        return false;
    } else if (!nuagreje.equals(other.nuagreje))
      return false;
    if (nucnfclt == null) {
      if (other.nucnfclt != null)
        return false;
    } else if (!nucnfclt.equals(other.nucnfclt))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    } else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("ImpuestoId %s##%s##%s##%s##%s", nuorden, nbooking, nuagreje, nucnfclt, nombre);
  }

}
