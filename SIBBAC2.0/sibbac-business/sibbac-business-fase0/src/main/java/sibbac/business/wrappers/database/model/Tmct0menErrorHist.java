package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


@Entity	
@Table( name = "TMCT0MEN_ERROR_HIST" )
public class Tmct0menErrorHist implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	

	private static final long	serialVersionUID	= -7382602618392121755L;

	
	@ManyToOne( fetch = FetchType.EAGER )
	@JoinColumn( name = "CDMENSA", referencedColumnName = "CDMENSA", nullable = false )
	private Tmct0men cdmensa;

	@ManyToOne( fetch = FetchType.EAGER )
	@JoinColumns( {
			@JoinColumn( name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false ),
			@JoinColumn( name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false )
	} )
	private Tmct0sta tmct0sta;

	public Tmct0sta getTmct0sta() {
		return this.tmct0sta;
	}

	public Tmct0menErrorHist setTmct0sta(Tmct0sta tmct0sta) {
		this.tmct0sta = tmct0sta;
		return this;
	}

	@Column( name = "MESSAGE", nullable = false )
	private String message;
	
	@Column( name = "NUM_EXECUTION" )
	private Integer numExecution;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FHAUDIT", length = 26)
	private Date fhaudit = new Date();


	public Date getFhaudit() {
		return this.fhaudit;
	}

	public Tmct0menErrorHist setFhaudit(Date fhaudit) {
		this.fhaudit = fhaudit;
		return this;
	}

	public Integer getNumExecution() {
		return numExecution;
	}

	public Tmct0menErrorHist setNumExecution(Integer numExecution) {
		this.numExecution = numExecution;
		return this;
	}

	public String getMessage() {
		return message;
	}

	public Tmct0menErrorHist setMessage(String message) {
		this.message = message;
		return this;
	}

	public Long getId() {
		return id;
	}
	public Tmct0menErrorHist setId(Long id) {
		this.id = id;
		return this;
	}

	public Tmct0men getCdmensa() {
		return cdmensa;
	}

	public Tmct0menErrorHist setCdmensa(Tmct0men cdmensa) {
		this.cdmensa = cdmensa;
		return this;
	}

	
	
}
