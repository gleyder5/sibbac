package sibbac.business.wrappers.database.data;


import sibbac.business.wrappers.database.model.Tmct0Mercado;


public class MercadoData {

	private long	id;
	private String	codigo;
	private String	descripcion;

	public MercadoData( Tmct0Mercado mkt ) {
		this.id = mkt.getId();
		this.codigo = mkt.getCodigo();
		this.descripcion = mkt.getDescripcion();
	}

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
