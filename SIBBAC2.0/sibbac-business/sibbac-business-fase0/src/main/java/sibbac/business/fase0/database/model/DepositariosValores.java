package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_DEPOSITARIOS_VALORES" )
public class DepositariosValores extends MasterData< DepositariosValores > {

	private static final long	serialVersionUID	= 7771584076891896708L;

}
