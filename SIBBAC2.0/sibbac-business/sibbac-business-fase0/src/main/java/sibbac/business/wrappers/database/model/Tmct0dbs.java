package sibbac.business.wrappers.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;

@Entity
@Table(name = "TMCT0DBS")
public class Tmct0dbs implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 4512979251646654992L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDDBS", length = 8, nullable = false)
  private Long iddbs;
  private String ntabla;
  private Character tipo;
  private Short nupareje;
  private Integer nuoprout;
  private Integer nsec1;
  private Integer nsec2;
  private Date fhaudit;
  private Date fecha;
  private String cdusuaud;
  private Character cdestado;
  private String auxiliar;

  public Long getIddbs() {
    return this.iddbs;
  }

  public void setIddbs(Long iddbs) {
    this.iddbs = iddbs;
  }

  @Column(name = "NTABLA", nullable = false, length = 20)
  public String getNtabla() {
    return this.ntabla;
  }

  public void setNtabla(String ntabla) {
    this.ntabla = ntabla;
  }

  @Column(name = "TIPO", nullable = false, length = 1)
  public Character getTipo() {
    return this.tipo;
  }

  public void setTipo(Character tipo) {
    this.tipo = tipo;
  }

  @Column(name = "NUPAREJE", nullable = false, length = 4, scale = 0)
  public Short getNupareje() {
    return this.nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  @Column(name = "NUOPROUT", nullable = false, length = 9, scale = 0)
  public Integer getNuoprout() {
    return this.nuoprout;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  @Column(name = "NSEC1", nullable = false, length = 9, scale = 0)
  public Integer getNsec1() {
    return this.nsec1;
  }

  public void setNsec1(Integer nsec1) {
    this.nsec1 = nsec1;
  }

  @Column(name = "NSEC2", nullable = false, length = 9, scale = 0)
  public Integer getNsec2() {
    return this.nsec2;
  }

  public void setNsec2(Integer nsec2) {
    this.nsec2 = nsec2;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", nullable = false, length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "FECHA", nullable = false)
  @XmlAttribute
  @Temporal(TemporalType.DATE)
  public Date getFecha() {
    return this.fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  @Column(name = "CDUSUAUD", nullable = false, length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Column(name = "CDESTADO", nullable = false, length = 1)
  public Character getCdestado() {
    return this.cdestado;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  @Column(name = "AUXILIAR", nullable = false, length = 254)
  public String getAuxiliar() {
    return this.auxiliar;
  }

  public void setAuxiliar(String auxiliar) {
    this.auxiliar = auxiliar;
  }

  @Override
  public String toString() {
    return String.format(
        "Tmct0dbs [ntabla=%s, tipo=%s, nupareje=%s, nuoprout=%s, nsec1=%s, nsec2=%s, fecha=%s, auxiliar=%s]", ntabla,
        tipo, nupareje, nuoprout, nsec1, nsec2, fecha, auxiliar);
  }

}
