package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.business.wrappers.database.model.Tmct0alcId;

public class InformeOpercacionData implements Serializable{
   
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Date fechaOperación; 		//TMCT0ALC!FEEJELIQ 
    private Date fechaValor; 			//TMCT0ALC!FEOPELIQ
    private Tmct0alcId nuestraReferencia;	//TMCT0ALC!NBOOKING / NUCNFCLT / NUCNFLIQ
    private String compraOVenta;		//TMCT0ALO!CDTPOPER
    private String isin;			//TMCT0ALO!CDISIN
    private String nombreDelValor;		//TMCT0ALO? TMCT0BOK? 
    private String mercado;			//Centro de Contratación - ¿?
    private BigDecimal totalTitulos;		//ejecutados por orden – TMCT0ALC!NUTITLIQ
    private String titularOSubcuenta;		//asignado a cada ejecución o desglose – TMCT0ALC!NBTITLIQ
    private BigDecimal precioMedio;		//neto de la orden  - TMCT0ALC!IMCAMNET
    private BigDecimal corretaje;		//Total de la orden – TMCT0ALC!IMCOMISN
    private String estadoDeLaOperacion;		//(Código Discrepancia) – TMCT0ALC!CDESTADOENTREC + S3ERRORES!DSERROR (último registro de esta tabla buscando por S3LIQUIDACION!ID que a su vez se obtiene buscando por  NBOOKING+NUCNFCLT+NUCNFLIQ)
    private String datoDiscrepante;		//(Titular, Fecha, Neto, Cantidad títulos, Isin, etc.) – S3ERRORES!DATOERRORCLIENTE + DATOERRORSV
        
    public InformeOpercacionData(Date feejeliq, Date feopeliq, Tmct0alcId id, 
	    char cdtpoper, String isin, String nbvalors, 
	    BigDecimal nutitliq, String nbtitliq, BigDecimal imcamnet, BigDecimal imcomisn,
	    String cdestadoentrec){
	this.fechaOperación = feejeliq;
	this.fechaValor = feopeliq;
	this.nuestraReferencia = id;
	this.compraOVenta = String.valueOf(cdtpoper);
	this.totalTitulos = nutitliq;
	this.isin = isin;
	this.nombreDelValor = nbvalors;
	this.titularOSubcuenta = nbtitliq;
	this.precioMedio = imcamnet;
	this.corretaje = imcomisn;
	this.estadoDeLaOperacion = cdestadoentrec;
    }
    public void prepareErrorData(S3Errores err){
	this.estadoDeLaOperacion +=  err.getDserror();
	this.datoDiscrepante = err.getDatoErrorCliente() + err.getDatoErrorSV();
    }
    public Date getFechaOperación() {
        return fechaOperación;
    }
    public void setFechaOperación(Date fechaOperación) {
        this.fechaOperación = fechaOperación;
    }
    public Date getFechaValor() {
        return fechaValor;
    }
    public void setFechaValor(Date fechaValor) {
        this.fechaValor = fechaValor;
    }
    public Tmct0alcId getNuestraReferencia() {
        return nuestraReferencia;
    }
    public void setNuestraReferencia(Tmct0alcId nuestraReferencia) {
        this.nuestraReferencia = nuestraReferencia;
    }
    public String getCompraOVenta() {
        return compraOVenta;
    }
    public void setCompraOVenta(String compraOVenta) {
        this.compraOVenta = compraOVenta;
    }
    public String getIsin() {
        return isin;
    }
    public void setIsin(String isin) {
        this.isin = isin;
    }
    public String getNombreDelValor() {
        return nombreDelValor;
    }
    public void setNombreDelValor(String nombreDelValor) {
        this.nombreDelValor = nombreDelValor;
    }
    public String getMercado() {
        return mercado;
    }
    public void setMercado(String mercado) {
        this.mercado = mercado;
    }
    public BigDecimal getTotalTitulos() {
        return totalTitulos;
    }
    public void setTotalTitulos(BigDecimal totalTitulos) {
        this.totalTitulos = totalTitulos;
    }
    public String getTitularOSubcuenta() {
        return titularOSubcuenta;
    }
    public void setTitularOSubcuenta(String titularOSubcuenta) {
        this.titularOSubcuenta = titularOSubcuenta;
    }
    public BigDecimal getPrecioMedio() {
        return precioMedio;
    }
    public void setPrecioMedio(BigDecimal precioMedio) {
        this.precioMedio = precioMedio;
    }
    public BigDecimal getCorretaje() {
        return corretaje;
    }
    public void setCorretaje(BigDecimal corretaje) {
        this.corretaje = corretaje;
    }
    public String getEstadoDeLaOperacion() {
        return estadoDeLaOperacion;
    }
    public void setEstadoDeLaOperacion(String estadoDeLaOperacion) {
        this.estadoDeLaOperacion = estadoDeLaOperacion;
    }
    public String getDatoDiscrepante() {
        return datoDiscrepante;
    }
    public void setDatoDiscrepante(String datoDiscrepante) {
        this.datoDiscrepante = datoDiscrepante;
    }
    
    
}
