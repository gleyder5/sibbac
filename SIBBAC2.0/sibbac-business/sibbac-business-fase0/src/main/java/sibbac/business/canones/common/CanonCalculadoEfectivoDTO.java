package sibbac.business.canones.common;

import java.io.Serializable;
import java.math.BigDecimal;

public class CanonCalculadoEfectivoDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 734080941532725970L;
  private BigDecimal imcanon, imefectivo;

  public BigDecimal getImcanon() {
    return imcanon;
  }

  public BigDecimal getImefectivo() {
    return imefectivo;
  }

  public void setImcanon(BigDecimal imcanon) {
    this.imcanon = imcanon;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

}
