package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0Desglose;

public class DesgloseData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private Date auditDate;
    private String auditUser;
    private Character cdestado;
    private String nuorden;
    private String nbooking;
    private String nucnfclt;
    private Short nucnfliq;
    private BigDecimal nutitulos;
    private String subaccount;
    private String custodian;
    private String cdrefban;
    private String acctatcle;
    private Character desglose;
    private Character capacity;
    private BigDecimal pccombco;
    private BigDecimal imcombco;
    private BigDecimal pccomdev;
    private BigDecimal imcomdev;
    private BigInteger idRegla;
    private BigInteger idCompensador;
    private String cdReferenciaAsignacion;
    private BigInteger idCuentaCompensacion;
    private List<DesgloseTitularData> desgloseTitularDataList;
    private String referenciaTitular;
    
    public DesgloseData(){
	//
    }
    public DesgloseData(final Tmct0Desglose entity){
	this.setAcctatcle(entity.getAcctatcle());                                                                       
	this.setCapacity(entity.getCapacity());                                                                         
	this.setCdestado(entity.getCdestado());                                                                         
	this.setCdrefban(entity.getCdrefban());                                                                         
	this.setCdReferenciaAsignacion(entity.getCdReferenciaAsignacion());                                             
	this.setCustodian(entity.getCustodian());                                                                       
	this.setDesglose(entity.getDesglose());                                                                         
	this.setDesgloseTitularDataList(DesgloseTitularData.entityListToDataList(entity.getTmct0DesgloseTitularList()));
	this.setId(entity.getId());                                                                                     
	this.setIdCompensador(entity.getIdCompensador());                                                               
	this.setIdCuentaCompensacion(entity.getIdCuentaCompensacion());                                                 
	this.setIdRegla(entity.getIdRegla());                                                                           
	this.setImcombco(entity.getImcombco());                                                                         
	this.setImcomdev(entity.getImcomdev());                                                                         
	this.setNbooking(entity.getNbooking());                                                                         
	this.setNucnfclt(entity.getNucnfclt());                                                                         
	this.setNucnfliq(entity.getNucnfliq());                                                                         
	this.setNuorden(entity.getNuorden());                                                                           
	this.setNutitulos(entity.getNutitulos());                                                                       
	this.setPccombco(entity.getPccombco());                                                                         
	this.setPccomdev(entity.getPccomdev());                                                                         
	this.setReferenciaTitular(entity.getReferenciaTitular());                                                       
	this.setSubaccount(entity.getSubaccount());                                                                     
	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public Character getCdestado() {
        return cdestado;
    }

    public void setCdestado(Character cdestado) {
        this.cdestado = cdestado;
    }

    public String getNuorden() {
        return nuorden;
    }

    public void setNuorden(String nuorden) {
        this.nuorden = nuorden;
    }

    public String getNbooking() {
        return nbooking;
    }

    public void setNbooking(String nbooking) {
        this.nbooking = nbooking;
    }

    public String getNucnfclt() {
        return nucnfclt;
    }

    public void setNucnfclt(String nucnfclt) {
        this.nucnfclt = nucnfclt;
    }

    public Short getNucnfliq() {
        return nucnfliq;
    }

    public void setNucnfliq(Short nucnfliq) {
        this.nucnfliq = nucnfliq;
    }

    public BigDecimal getNutitulos() {
        return nutitulos;
    }

    public void setNutitulos(BigDecimal nutitulos) {
        this.nutitulos = nutitulos;
    }

    public String getSubaccount() {
        return subaccount;
    }

    public void setSubaccount(String subaccount) {
        this.subaccount = subaccount;
    }

    public String getCustodian() {
        return custodian;
    }

    public void setCustodian(String custodian) {
        this.custodian = custodian;
    }

    public String getCdrefban() {
        return cdrefban;
    }

    public void setCdrefban(String cdrefban) {
        this.cdrefban = cdrefban;
    }

    public String getAcctatcle() {
        return acctatcle;
    }

    public void setAcctatcle(String acctatcle) {
        this.acctatcle = acctatcle;
    }

    public Character getDesglose() {
        return desglose;
    }

    public void setDesglose(Character desglose) {
        this.desglose = desglose;
    }

    public Character getCapacity() {
        return capacity;
    }

    public void setCapacity(Character capacity) {
        this.capacity = capacity;
    }

    public BigDecimal getPccombco() {
        return pccombco;
    }

    public void setPccombco(BigDecimal pccombco) {
        this.pccombco = pccombco;
    }

    public BigDecimal getImcombco() {
        return imcombco;
    }

    public void setImcombco(BigDecimal imcombco) {
        this.imcombco = imcombco;
    }

    public BigDecimal getPccomdev() {
        return pccomdev;
    }

    public void setPccomdev(BigDecimal pccomdev) {
        this.pccomdev = pccomdev;
    }

    public BigDecimal getImcomdev() {
        return imcomdev;
    }

    public void setImcomdev(BigDecimal imcomdev) {
        this.imcomdev = imcomdev;
    }

    public BigInteger getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(BigInteger idRegla) {
        this.idRegla = idRegla;
    }

    public BigInteger getIdCompensador() {
        return idCompensador;
    }

    public void setIdCompensador(BigInteger idCompensador) {
        this.idCompensador = idCompensador;
    }

    public String getCdReferenciaAsignacion() {
        return cdReferenciaAsignacion;
    }

    public void setCdReferenciaAsignacion(String cdReferenciaAsignacion) {
        this.cdReferenciaAsignacion = cdReferenciaAsignacion;
    }

    public BigInteger getIdCuentaCompensacion() {
        return idCuentaCompensacion;
    }

    public void setIdCuentaCompensacion(BigInteger idCuentaCompensacion) {
        this.idCuentaCompensacion = idCuentaCompensacion;
    }

    public List<DesgloseTitularData> getDesgloseTitularDataList() {
        return desgloseTitularDataList;
    }

    public void setDesgloseTitularDataList(
    	List<DesgloseTitularData> desgloseTitularDataList) {
        this.desgloseTitularDataList = desgloseTitularDataList;
    }

    public String getReferenciaTitular() {
        return referenciaTitular;
    }

    public void setReferenciaTitular(String referenciaTitular) {
        this.referenciaTitular = referenciaTitular;
    }
    /**
     * Convierte un objeto Tmct0Desglose a DesgloseData
     * @param entity
     * @return
     */
    public static DesgloseData entityToData(Tmct0Desglose entity){
	DesgloseData data = new DesgloseData();
	data.setAcctatcle(entity.getAcctatcle());
	data.setCapacity(entity.getCapacity());
	data.setCdestado(entity.getCdestado());
	data.setCdrefban(entity.getCdrefban());
	data.setCdReferenciaAsignacion(entity.getCdReferenciaAsignacion());
	data.setCustodian(entity.getCustodian());
	data.setDesglose(entity.getDesglose());
	data.setDesgloseTitularDataList(DesgloseTitularData.entityListToDataList(entity.getTmct0DesgloseTitularList()));
	data.setId(entity.getId());
	data.setIdCompensador(entity.getIdCompensador());
	data.setIdCuentaCompensacion(entity.getIdCuentaCompensacion());
	data.setIdRegla(entity.getIdRegla());
	data.setImcombco(entity.getImcombco());
	data.setImcomdev(entity.getImcomdev());
	data.setNbooking(entity.getNbooking());
	data.setNucnfclt(entity.getNucnfclt());
	data.setNucnfliq(entity.getNucnfliq());
	data.setNuorden(entity.getNuorden());
	data.setNutitulos(entity.getNutitulos());
	data.setPccombco(entity.getPccombco());
	data.setPccomdev(entity.getPccomdev());
	data.setReferenciaTitular(entity.getReferenciaTitular());
	data.setSubaccount(entity.getSubaccount());
	
	return data;
    }
    
}
