package sibbac.business.canones.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.canones.model.ReglasCanones;

@Repository
public interface ReglasCanonesDao extends JpaRepository<ReglasCanones, BigInteger> {

  @Query("select r from ReglasCanones r where r.activo = '1' order by r.orden asc, r.id asc ")
  List<ReglasCanones> findAllReglasCanonesActivas();

}
