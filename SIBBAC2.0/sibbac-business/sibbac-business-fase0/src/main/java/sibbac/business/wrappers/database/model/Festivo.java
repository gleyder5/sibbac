package sibbac.business.wrappers.database.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;

/**
 * Entity: "Festivo".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table(name = DBConstants.PERIODICIDADES.FESTIVOS)
@Entity
// @Component
@XmlRootElement
@Audited
public class Festivo {

  // ------------------------------------------ Static Bean properties

  // ------------------------------------------------- Bean properties
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", nullable = false, unique = true)
  @XmlAttribute
  private Integer id;

  @Column(name = "FECHA", nullable = false, unique = true)
  @XmlAttribute
  private Date fecha;

  @Column(name = "DESCRIPCION", nullable = false, length = 64)
  @XmlAttribute
  private String descripcion;

  @Column(name = "CDMERCAD", nullable = true, length = 20)
  @XmlAttribute
  private String cdmercad;

  // ----------------------------------------------- Bean Constructors

  public Festivo() {

  }

  // ------------------------------------------- Bean methods: Getters

  /**
   * Getter for fecha
   *
   * @return the fecha
   */
  public Date getFecha() {
    return this.fecha;
  }

  /**
   * Getter for descripcion
   *
   * @return the descripcion
   */
  public String getDescripcion() {
    return this.descripcion;
  }

  /**
   * Getter for id
   *
   * @return the id
   */
  public Integer getId() {
    return this.id;
  }

  // ------------------------------------------- Bean methods: Setters

  /**
   * Setter for fecha
   *
   * @param fecha the fecha to set
   */
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  /**
   * Setter for descripcion
   *
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * Setter for id
   *
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  // ------------------------------------------------ Business Methods

  // ----------------------------------------------- Inherited Methods

  public String getCdmercad() {
    return cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "[FESTIVO==" + this.id + " fecha: " + this.fecha + " Descripción= " + this.descripcion + "]";
  }

}
