package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import static java.util.Objects.requireNonNull;

public class FechaMifidTest implements Serializable {

  /**
   * Número serial
   */
  private static final long serialVersionUID = -6585173312601044011L;

  
  private final BigInteger codPersona;
  private final Date fechaFin;

  public FechaMifidTest(BigInteger codPersona, Date fechaFin) {
    this.codPersona = codPersona;
    this.fechaFin = requireNonNull(fechaFin, "No se admite fecha de fin nula");
  }
  
  public FechaMifidTest(Long codPersonaL, Date fechaFin) {
    this.codPersona = new BigInteger(codPersonaL.toString());
    this.fechaFin = requireNonNull(fechaFin, "No se admite fecha de fin nula");
  }  

  public String getCDBDP() {
    return String.format("%09d", codPersona);
  }

  public Date getFechaActual() {
    return fechaFin;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codPersona == null) ? 0 : codPersona.hashCode());
    result = prime * result + ((fechaFin == null) ? 0 : fechaFin.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FechaMifidTest other = (FechaMifidTest) obj;
    if (codPersona == null) {
      if (other.codPersona != null)
        return false;
    }
    else if (!codPersona.equals(other.codPersona))
      return false;
    if (fechaFin == null) {
      if (other.fechaFin != null)
        return false;
    }
    else if (!fechaFin.equals(other.fechaFin))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("{codPersona : %d, fecha : %tF}", codPersona, fechaFin);
  }
  
}
