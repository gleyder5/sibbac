package sibbac.business.fase0.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_MENSAJES_R00")
public class Tmct0MensajesR00 implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1148828414689132501L;
  private Long idMensajeR00;
  private Tmct0Mensajes tmct0Mensajes;
  private String texto;

  public Tmct0MensajesR00() {
  }

  public Tmct0MensajesR00(Long idMensajeR00, Tmct0Mensajes tmct0Mensajes, String texto) {
    this.idMensajeR00 = idMensajeR00;
    this.tmct0Mensajes = tmct0Mensajes;
    this.texto = texto;
  }

  @Id
  @Column(name = "ID_MENSAJE_R00", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getIdMensajeR00() {
    return this.idMensajeR00;
  }

  public void setIdMensajeR00(Long idMensajeR00) {
    this.idMensajeR00 = idMensajeR00;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_MENSAJE", nullable = false)
  public Tmct0Mensajes getTmct0Mensajes() {
    return this.tmct0Mensajes;
  }

  public void setTmct0Mensajes(Tmct0Mensajes tmct0Mensajes) {
    this.tmct0Mensajes = tmct0Mensajes;
  }

  @Column(name = "TEXTO", nullable = false, length = 78)
  public String getTexto() {
    return this.texto;
  }

  public void setTexto(String texto) {
    this.texto = texto;
  }

}
