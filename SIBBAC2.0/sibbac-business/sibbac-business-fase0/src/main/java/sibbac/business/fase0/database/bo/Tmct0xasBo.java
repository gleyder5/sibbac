package sibbac.business.fase0.database.bo;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0xasDao;
import sibbac.business.fase0.database.dao.Tmct0xasDaoImpl;
import sibbac.business.fase0.database.dto.XasDTO;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.business.fase0.database.model.Tmct0xasId;
import sibbac.common.utils.SibbacEnums.Tmct0xasConversionKey;
import sibbac.database.bo.AbstractBo;

/**
 * Todos los metodos deprecated han pasado a Tmct0xasAndTmct0cfgConfig
 * @author xIS16630
 *
 */
@Service
public class Tmct0xasBo extends AbstractBo<Tmct0xas, Tmct0xasId, Tmct0xasDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0xasBo.class);

  private static final String CONVERT_KEY_SEPARATOR_TOKEN = "##";

  private Map<String, String> converts = new HashMap<String, String>();

  @Autowired
  private Tmct0xasDaoImpl xasDao;

  public List<Tmct0xas> findByNameAndInfor(final String nbcamxml, final String inforxml) {
    return this.dao.findByNameAndInfor(nbcamxml, inforxml);
  }

  public Tmct0xas findByNameAndInforAndNbcamas(final String nbcamxml, final String inforxml, final String nbcamas4) {
    return this.dao.findByNameAndInforAndNbcamas(nbcamxml, inforxml, nbcamas4);
  }

  public List<XasDTO> findDTOForService(String sBolsaMercado) {

    return xasDao.findDTOForService(sBolsaMercado);

  }// public List< XasDTO > findDTOForService( String sBolsaMercado ) {

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param clearingPlatform
   * @return
   */
  public String getClearingPlatformClearingPlatform(String clearingplatform) {
    LOG.trace("[Tmct0xasBo :: getCamaraCompensacionClearingPlatform] inicio");
    String res = convertFromChachedMap(Tmct0xasConversionKey.CLEARING_PLATFORM_TO_CLEARING_PLATFORM, clearingplatform);
    LOG.trace("[Tmct0xasBo :: getCamaraCompensacionClearingPlatform] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param clearingPlatform
   * @return
   */
  public String getSentidoSibbacSentidoPti(String sentidoSibbac) {
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] inicio");
    String res = convertFromChachedMap(Tmct0xasConversionKey.SENTIDO_SIBBAC_SENTIDO_PTI, sentidoSibbac);
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param sendtidoSibbac
   * @return
   */
  public String getSentidoSibbacSentidoEuroCcp(String sentidoSibbac) {
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoEuroCcp] inicio");
    String res = convertFromChachedMap(Tmct0xasConversionKey.SENTIDO_SIBBAC_SENTIDO_EUROCCP, sentidoSibbac);
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoEuroCcp] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param sendtidoSibbac
   * @return
   */
  public String getSentidoEuroCcpSentidoSibbac(String sentidoSibbac) {
    LOG.trace("[Tmct0xasBo :: getSentidoEuroCcpSentidoSibbac] inicio");
    String res = convertFromChachedMap(Tmct0xasConversionKey.SENTIDO_EUROCCP_SENTIDO_SIBBAC, sentidoSibbac);
    LOG.trace("[Tmct0xasBo :: getSentidoEuroCcpSentidoSibbac] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param clearingPlatform
   * @return
   */
  public String getSentidoPtiSentidoSibbac(String sentidoPti) {
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] inicio");

    String res = convertFromChachedMap(Tmct0xasConversionKey.SENTIDO_PTI_SENTIDO_SIBBAC, sentidoPti);
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] fin.");
    return res;
  }

  public Character getIndicadorCapacidadPtiIndicadorCapacidad(String indCapacidadPti) {
    LOG.trace("[Tmct0xasBo :: getIndicadorCapacidadPtiIndicadorCapacidad] inicio");
    Tmct0xas xas = findByNameAndInforAndNbcamas(Tmct0xasConversionKey.IND_CAPACIDAD_PTI_IND_CAPACIDAD.getNbcamxml(),
        indCapacidadPti, Tmct0xasConversionKey.IND_CAPACIDAD_PTI_IND_CAPACIDAD.getNbcamas4());
    String res = null;
    if (xas == null) {
      LOG.warn("[getIndicadorCapacidadPtiIndicadorCapacidad] no configurado: {} para valor: {}",
          Tmct0xasConversionKey.IND_CAPACIDAD_PTI_IND_CAPACIDAD, indCapacidadPti);
      return null;
    }
    else {
      res = xas.getInforas4();
      return res.charAt(0);
    }

  }

  public Character getIndicadorCapacidadIndicadorCapacidadPti(String indCapacidad) {
    LOG.trace("[Tmct0xasBo :: getIndicadorCapacidadIndicadorCapacidadPti] inicio");
    Tmct0xas xas = findByNameAndInforAndNbcamas(Tmct0xasConversionKey.IND_CAPACIDAD_IND_CAPACIDAD_PTI.getNbcamxml(),
        indCapacidad, Tmct0xasConversionKey.IND_CAPACIDAD_IND_CAPACIDAD_PTI.getNbcamas4());
    String res = null;
    if (xas == null) {
      LOG.warn("[getIndicadorCapacidadIndicadorCapacidadPti] no configurado: {} para valor: {}",
          Tmct0xasConversionKey.IND_CAPACIDAD_IND_CAPACIDAD_PTI, indCapacidad);
      return null;
    }
    else {
      res = xas.getInforas4();
      return res.charAt(0);
    }

  }

  public String getSettlementExchangeCdmercad(String settlementExchange) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] inicio");

    Tmct0xas xas = Objects.requireNonNull(dao.findByNameAndInforAndNbcamas(
        Tmct0xasConversionKey.SETTLEMENT_EXCHANGE_CDMERCAD.getNbcamxml(), settlementExchange,
        Tmct0xasConversionKey.SETTLEMENT_EXCHANGE_CDMERCAD.getNbcamas4()), MessageFormat.format(
        "Tmct0xas no configurado: {0}-{1}", Tmct0xasConversionKey.SETTLEMENT_EXCHANGE_CDMERCAD, settlementExchange));
    String res = Objects.requireNonNull(
        xas.getInforas4(),
        MessageFormat.format("Tmct0xas no configurado: {0}-{1}", Tmct0xasConversionKey.SETTLEMENT_EXCHANGE_CDMERCAD,
            settlementExchange)).trim();
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] fin.");
    return res;
  }

  public String getSettlementExchangeCdclsmdo(String settlementExchange) throws NullPointerException {
    String res = null;
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] inicio");
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDMERCAD_CDMERCAD.getNbcamxml(),
        settlementExchange, Tmct0xasConversionKey.CDMERCAD_CDMERCAD.getNbcamas4());
    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_CDMERCAD, settlementExchange);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_CDMERCAD, settlementExchange);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] fin.");
    return res;
  }

  public String getCdcleareClearer(String cdcleare) {

    LOG.trace("[Tmct0xasBo :: getCdcleareClearer] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDCLEARE_CLEARER.getNbcamxml(), cdcleare,
        Tmct0xasConversionKey.CDCLEARE_CLEARER.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCLEARE_CLEARER, cdcleare);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCLEARE_CLEARER, cdcleare);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdcleareClearer] fin.");
    return res;
  }

  public String getCdbrokerCdbolsas(String cdbroker) {

    LOG.trace("[Tmct0xasBo :: getCdbrokerCdbolsas] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDBROKER_CDBOLSAS.getNbcamxml(), cdbroker,
        Tmct0xasConversionKey.CDBROKER_CDBOLSAS.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDBROKER_CDBOLSAS, cdbroker);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDBROKER_CDBOLSAS, cdbroker);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdbrokerCdbolsas] fin.");
    return res;
  }

  public String getCdbrokerCdmiembro(String cdbroker) throws NullPointerException {

    LOG.trace("[Tmct0xasBo :: getCdbrokerCdmiembro] inicio");
    String res = null;
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDBROKER_CDMIEMBRO.getNbcamxml(), cdbroker,
        Tmct0xasConversionKey.CDBROKER_CDMIEMBRO.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDBROKER_CDMIEMBRO, cdbroker);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDBROKER_CDMIEMBRO, cdbroker);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdbrokerCdmiembro] fin.");
    return res;
  }

  public String getCdbrokerMtf(String cdbroker) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] inicio");
    String res = null;
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDBROKER_MTF.getNbcamxml(), cdbroker,
        Tmct0xasConversionKey.CDBROKER_MTF.getNbcamas4());
    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDBROKER_MTF, cdbroker);
    }
    else {
      res = Objects.requireNonNull(xas.getInforas4(),
          MessageFormat.format("Tmct0xas no configurado: {0}-{1}", Tmct0xasConversionKey.CDBROKER_MTF, cdbroker))
          .trim();
      LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] fin.");
    }
    return res;
  }

  public String getCdmercadMtf(String cdmercad) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] inicio");
    String res = null;
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDMERCAD_MTF.getNbcamxml(), cdmercad,
        Tmct0xasConversionKey.CDMERCAD_MTF.getNbcamas4());
    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_MTF, cdmercad);
    }
    else {
      res = Objects.requireNonNull(xas.getInforas4(),
          MessageFormat.format("Tmct0xas no configurado: {0}-{1}", Tmct0xasConversionKey.CDMERCAD_MTF, cdmercad))
          .trim();
    }

    LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] fin.");
    return res;
  }

  public String getCdmercadTpsubmer(String cdmercad) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getCdmercadTpsubmer] inicio");
    String res = null;
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDMERCAD_TPSUBMER.getNbcamxml(), cdmercad,
        Tmct0xasConversionKey.CDMERCAD_TPSUBMER.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_TPSUBMER, cdmercad);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_TPSUBMER, cdmercad);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }

    LOG.trace("[Tmct0xasBo :: getCdmercadTpsubmer] fin.");
    return res;
  }

  public String getCdcanalCdcanent(String cdcanal) {
    LOG.trace("[Tmct0xasBo :: getCdcanalCdcanent] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDCANAL_CDCANENT.getNbcamxml(), cdcanal,
        Tmct0xasConversionKey.CDCANAL_CDCANENT.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCANAL_CDCANENT, cdcanal);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCANAL_CDCANENT, cdcanal);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }

    LOG.trace("[Tmct0xasBo :: getCdcanalCdcanent] fin.");
    return res;
  }

  public String getGenpkbDesglose(String clave) {

    LOG.trace("[Tmct0xasBo :: getGenpkbDesglose] inicio");
    String res = null;
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.GENPKB_DESGLOSE.getNbcamxml(), clave,
        Tmct0xasConversionKey.GENPKB_DESGLOSE.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.GENPKB_DESGLOSE, clave);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.GENPKB_DESGLOSE, clave);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getGenpkbDesglose] fin.");
    return res;
  }

  public String getCdtipordTpordens(String cdtipord) {

    LOG.trace("[Tmct0xasBo :: getCdtipordTpordens] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDTIPORD_TPORDENS.getNbcamxml(), cdtipord,
        Tmct0xasConversionKey.CDTIPORD_TPORDENS.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDTIPORD_TPORDENS, cdtipord);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDTIPORD_TPORDENS, cdtipord);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdtipordTpordens] fin.");
    return res;
  }

  public String getCdcuentaCdorigen(String cdcuenta) {

    LOG.trace("[Tmct0xasBo :: getCdcuentaCdorigen] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDCUENTA_CDORIGEN.getNbcamxml(), cdcuenta,
        Tmct0xasConversionKey.CDCUENTA_CDORIGEN.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCUENTA_CDORIGEN, cdcuenta);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCUENTA_CDORIGEN, cdcuenta);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdcuentaCdorigen] fin.");
    return res;
  }

  public String getCdclenteCdorigen(String cdclente) {

    LOG.trace("[Tmct0xasBo :: getCdclenteCdorigen] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDCLENTE_CDORIGEN.getNbcamxml(), cdclente,
        Tmct0xasConversionKey.CDCLENTE_CDORIGEN.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCLENTE_CDORIGEN, cdclente);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDCLENTE_CDORIGEN, cdclente);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdclenteCdorigen] fin.");
    return res;
  }

  public String getCdorigenCdorigen(String cdorigen) {

    LOG.trace("[Tmct0xasBo :: getCdorigenCdorigen] inicio");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDORIGEN_CDORIGEN.getNbcamxml(), cdorigen,
        Tmct0xasConversionKey.CDORIGEN_CDORIGEN.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDORIGEN_CDORIGEN, cdorigen);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDORIGEN_CDORIGEN, cdorigen);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdorigenCdorigen] fin.");
    return res;
  }

  public Character getCdmercadTpmercad(String cdmercad) {

    LOG.trace("[Tmct0xasBo :: getCdmercadTpmercad] inicio");
    Character res = ' ';
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDMERCAD_TPMERCAD.getNbcamxml(), cdmercad,
        Tmct0xasConversionKey.CDMERCAD_TPMERCAD.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_TPMERCAD, cdmercad);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDMERCAD_TPMERCAD, cdmercad);
      }
      else {
        if (StringUtils.isNotBlank(xas.getInforas4()))
          res = xas.getInforas4().trim().charAt(0);
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdmercadTpmercad] fin.");
    return res;
  }

  public Character getCdtpcambCdtpcambio(Character cdtpcamb) {

    LOG.trace("[Tmct0xasBo :: getCdtpcambCdtpcambio] inicio");
    Character res = ' ';
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDTPCAMB_TPCAMBIO.getNbcamxml(), cdtpcamb
        + "", Tmct0xasConversionKey.CDTPCAMB_TPCAMBIO.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDTPCAMB_TPCAMBIO, cdtpcamb);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDTPCAMB_TPCAMBIO, cdtpcamb);
      }
      else {
        if (StringUtils.isNotBlank(xas.getInforas4()))
          res = xas.getInforas4().trim().charAt(0);
      }

    }
    LOG.trace("[Tmct0xasBo :: getCdtpcambCdtpcambio] fin.");
    return res;
  }

  public Character getCdtpcambTpcontra(Character cdtpcamb) {

    LOG.trace("[Tmct0xasBo :: getcdtpcambTpcontra] inicio");
    Character res = ' ';
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.CDTPCAMB_TPCONTRA.getNbcamxml(), cdtpcamb
        + "", Tmct0xasConversionKey.CDTPCAMB_TPCONTRA.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDTPCAMB_TPCONTRA, cdtpcamb);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.CDTPCAMB_TPCONTRA, cdtpcamb);
      }
      else {
        if (StringUtils.isNotBlank(xas.getInforas4()))
          res = xas.getInforas4().trim().charAt(0);
      }

    }
    LOG.trace("[Tmct0xasBo :: getcdtpcambTpcontra] fin.");
    return res;
  }

  public String getDarkPoolMicToMic(String darkPoolMic) {

    LOG.trace("[Tmct0xasBo :: getDarkPoolMicToMic] inicio- Mercado contratacion Mic");
    String res = "";
    Tmct0xas xas = dao.findByNameAndInforAndNbcamas(Tmct0xasConversionKey.DARK_POOL_MIC_TO_MIC.getNbcamxml(),
        darkPoolMic, Tmct0xasConversionKey.DARK_POOL_MIC_TO_MIC.getNbcamas4());

    if (xas == null) {
      LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.DARK_POOL_MIC_TO_MIC, darkPoolMic);
    }
    else {
      if (xas.getInforas4() == null) {
        LOG.debug("Tmct0xas no configurado: {}-{}", Tmct0xasConversionKey.DARK_POOL_MIC_TO_MIC, darkPoolMic);
      }
      else {
        res = xas.getInforas4().trim();
      }

    }
    LOG.trace("[Tmct0xasBo :: getDarkPoolMicToMic] fin- Mercado contratacion Mic");
    return res;
  }

  public Map<String, String> getEuroccpTxtConversion() {
    String key, valor;
    Map<String, String> res = new HashMap<String, String>();
    List<Tmct0xas> l = dao.findByNbvamxmlAndNbcamas4(
        Tmct0xasConversionKey.TXT_ORS_SIBBAC_TXT_ORS_EUROCCP.getNbcamxml(),
        Tmct0xasConversionKey.TXT_ORS_SIBBAC_TXT_ORS_EUROCCP.getNbcamas4());
    for (Tmct0xas tmct0xas : l) {
      if (tmct0xas.getId().getInforxml().trim().isEmpty()) {
        key = " ";
      }
      else {
        key = tmct0xas.getId().getInforxml().trim();
      }

      if (tmct0xas.getInforas4().trim().isEmpty()) {
        valor = " ";
      }
      else {
        valor = tmct0xas.getInforas4().trim();
      }
      res.put(key, valor);
    }
    return res;
  }

  @Deprecated
  private String convertFromChachedMap(Tmct0xasConversionKey key, String inforxml) {
    return convertFromChachedMap(key.getNbcamxml(), inforxml, key.getNbcamas4());
  }

  @Deprecated
  private String convertFromChachedMap(final String nbcamxml, final String inforxml, final String nbcamas4) {
    LOG.trace("[Tmct0xasBo :: convertirMap] inicio.");
    String keyMap = getConvertKey(nbcamxml, inforxml, nbcamas4);

    String res = converts.get(keyMap);
    if (res == null) {
      LOG.trace("[Tmct0xasBo :: convertirMap] valor no encontrado en el map, buscamos en bbdd.");
      synchronized (this) {
        res = converts.get(keyMap);
        if (res == null) {
          res = "";
          Tmct0xas xas = dao.findByNameAndInforAndNbcamas(nbcamxml, inforxml, nbcamas4);
          if (xas != null) {
            LOG.trace("[Tmct0xasBo :: convertirMap] valor en bbdd encontrado.");
            res = StringUtils.trim(xas.getInforas4());
          }
          else {
            res = inforxml;
            LOG.trace("[Tmct0xasBo :: convertirMap] valor en bbdd NO encontrado, devolveremos el mismo valor.");
            LOG.debug(
                "[Tmct0xasBo :: convertirMap] valor en bbdd NO encontrado, devolveremos el mismo valor [nbcamxml={}, inforxml={}, nbcamas4={}]={}.",
                nbcamxml, inforxml, nbcamas4, res);
          }
          converts.put(keyMap, res);
        }
      }// fin sincronized
    }
    LOG.trace("[Tmct0xasBo :: convertirMap] fin [nbcamxml={}, inforxml={}, nbcamas4={}]={}.", nbcamxml, inforxml,
        nbcamas4, res);
    return res;
  }

  // private String getConvertKey(Tmct0xasConversionKey key, String inforxml) {
  // return getConvertKey(key.getNbcamxml(), inforxml, key.getNbcamas4());
  // }
  @Deprecated
  private String getConvertKey(String nbcamxml, String inforxml, String nbcamas4) {
    return StringUtils.trim(nbcamxml) + CONVERT_KEY_SEPARATOR_TOKEN + StringUtils.trim(inforxml)
        + CONVERT_KEY_SEPARATOR_TOKEN + StringUtils.trim(nbcamas4);
  }

  private String[] getTokensFromConvertKey(String key) {
    if (key != null) {
      return key.split(CONVERT_KEY_SEPARATOR_TOKEN);
    }
    else {
      return null;
    }
  }

  public void close() {
    synchronized (this.converts) {
      this.converts.clear();
    }
  }

}
