package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_MIFIDCUESTIONARIO")
public class Tmct0MifidCuestionario implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Tmct0MifidCuestionarioId id;

	@Column(name = "FECINI", nullable = false, length = 4)
	private Date fecini;

	@Column(name = "TIPCUEST", nullable = false, length = 5)
	private String tipcuest;

	@Column(name = "SUBTIPCO", nullable = false, length = 2)
	private String subtipco;

	@Column(name = "IDPROD", nullable = false, length = 3)
	private String idprod;

	@Column(name = "IDSUBTIP", nullable = false, length = 3)
	private String idsubtip;

	@Column(name = "TIPOPERS", nullable = false, length = 1)
	private String tipopers;

	@Column(name = "TIPSERVI", nullable = false, length = 2)
	private String tipservi;

	@Column(name = "FECFIN", nullable = false, length = 4)
	private Date fecfin;

	@Column(name = "CONSULTA", nullable = false, length = 1)
	private String consulta;

	@Column(name = "USUALT", nullable = false, length = 8)
	private String usualt;

	@Column(name = "FECMODI", nullable = false, length = 4)
	private Date fecmodi;

	@Column(name = "USUMODI", nullable = false, length = 8)
	private String usumodi;

	public Tmct0MifidCuestionario() {
	}

	public Tmct0MifidCuestionario(Tmct0MifidCuestionarioId id, Date fecini,
			String tipcuest, String subtipco, String idprod, String idsubtip,
			String tipopers, String tipservi, Date fecfin, String consulta,
			String usualt, Date fecmodi, String usumodi) {
		super();
		this.id = id;
		this.fecini = fecini;
		this.tipcuest = tipcuest;
		this.subtipco = subtipco;
		this.idprod = idprod;
		this.idsubtip = idsubtip;
		this.tipopers = tipopers;
		this.tipservi = tipservi;
		this.fecfin = fecfin;
		this.consulta = consulta;
		this.usualt = usualt;
		this.fecmodi = fecmodi;
		this.usumodi = usumodi;
	}

	public Tmct0MifidCuestionarioId getId() {
		return id;
	}

	public void setId(Tmct0MifidCuestionarioId id) {
		this.id = id;
	}

	public Date getFecini() {
		return fecini;
	}

	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}

	public String getTipcuest() {
		return tipcuest;
	}

	public void setTipcuest(String tipcuest) {
		this.tipcuest = tipcuest;
	}

	public String getSubtipco() {
		return subtipco;
	}

	public void setSubtipco(String subtipco) {
		this.subtipco = subtipco;
	}

	public String getIdprod() {
		return idprod;
	}

	public void setIdprod(String idprod) {
		this.idprod = idprod;
	}

	public String getIdsubtip() {
		return idsubtip;
	}

	public void setIdsubtip(String idsubtip) {
		this.idsubtip = idsubtip;
	}

	public String getTipopers() {
		return tipopers;
	}

	public void setTipopers(String tipopers) {
		this.tipopers = tipopers;
	}

	public String getTipservi() {
		return tipservi;
	}

	public void setTipservi(String tipservi) {
		this.tipservi = tipservi;
	}

	public Date getFecfin() {
		return fecfin;
	}

	public void setFecfin(Date fecfin) {
		this.fecfin = fecfin;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getUsualt() {
		return usualt;
	}

	public void setUsualt(String usualt) {
		this.usualt = usualt;
	}

	public Date getFecmodi() {
		return fecmodi;
	}

	public void setFecmodi(Date fecmodi) {
		this.fecmodi = fecmodi;
	}

	public String getUsumodi() {
		return usumodi;
	}

	public void setUsumodi(String usumodi) {
		this.usumodi = usumodi;
	}

}
