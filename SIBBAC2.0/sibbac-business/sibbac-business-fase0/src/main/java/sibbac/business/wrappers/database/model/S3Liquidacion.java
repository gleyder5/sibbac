package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.S3Liquidacion}.
 */
@Entity
@Table(name = DBConstants.CLEARING.S3LIQUIDACION)
public class S3Liquidacion extends ATable<S3Liquidacion> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 8249329764425148113L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns(value = { @JoinColumn(name = "IDNETTING", referencedColumnName = "ID")
  })
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  private Netting idnetting;

  @Column(name = "IDFICHERO", nullable = false, length = 75)
  private String idFichero;

  @Column(name = "CDORIGEN", nullable = false, length = 1)
  private char cdorigen;

  @Column(name = "NBOOKING", length = 20)
  private String nbooking;

  @Column(name = "NUCNFCLT", length = 20)
  private String nucnfclt;

  @Column(name = "NUCNFLIQ", precision = 4, scale = 0)
  private short nucnfliq;

  @Column(name = "CDESTADO", nullable = false, length = 1)
  private char cdestado = 'A';

  @Column(name = "CDESTADOS3", nullable = false, length = 3)
  private String cdestadoS3;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAOPERACION", nullable = false, length = 4)
  private Date fechaOperacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAVALOR", nullable = false, length = 4)
  private Date fechaValor;

  @Column(name = "NUTIULOLIQUIDADOS", nullable = false, precision = 16, scale = 5)
  private BigDecimal nutituloLiquidados = BigDecimal.ZERO;

  @Column(name = "IMPORTENETOLIQUIDADO", nullable = false, precision = 18, scale = 4)
  private BigDecimal importeNetoLiquidado = BigDecimal.ZERO;

  @Column(name = "CONTABILIZADO", nullable = false, length = 1)
  private char contabilizado = 'N';

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHACONTABILIDAD", length = 4)
  private Date fechaContabilidad;

  @Column(name = "IMPORTEEFECTIVO", nullable = false, precision = 18, scale = 4)
  private BigDecimal importeEfectivo = BigDecimal.ZERO;

  @Column(name = "IMPORTECORRETAJE", nullable = false, precision = 18, scale = 4)
  private BigDecimal importeCorretaje = BigDecimal.ZERO;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_FICHERO", nullable = false, length = 4)
  private Date fechaFichero;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "HORAVALOR", nullable = true)
  private Date horaValor;

  /** Tipo de operación. CHAR(6). */
  @Column(name = "TIPOPERACION", nullable = true, length = 6)
  private String tipoOperacion;

  /** Instrucción de liquidación. */
  @Column(name = "ILIQ", nullable = true, length = 35)
  private String instruccionLiquidacion;

  /** Referencia enviada a Megara. */
  @Column(name = "POOLREFERENCE", nullable = true, length = 16)
  private String poolReference;

  /** Código Isin. */
  @Column(name = "ISIN", nullable = true, length = 12)
  private String isin;

  @Temporal(TemporalType.TIME)
  @Column(name = "HORAFICHERO", nullable = true)
  private Date horaFichero;

  /**
   * @return the horaFichero
   */
  public Date getHoraFichero() {
    return horaFichero;
  }

  /**
   * @param horaFichero the horaFichero to set
   */
  public void setHoraFichero(Date horaFichero) {
    this.horaFichero = horaFichero;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  public S3Liquidacion() {

  }

  public S3Liquidacion(String idFichero,
                       char cdorigen,
                       String nbooking,
                       String nucnfclt,
                       short nucnfliq,
                       char cdestado,
                       String cdestadoS3,
                       Date fechaOperacion,
                       Date fechaValor,
                       BigDecimal nutituloLiquidados,
                       BigDecimal importeNetoLiquidado,
                       BigDecimal importeEfectivo,
                       BigDecimal importeCorretaje,
                       Date fechaFichero) {
    super();
    this.idFichero = idFichero;
    this.cdorigen = cdorigen;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.cdestado = cdestado;
    this.cdestadoS3 = cdestadoS3;
    this.fechaOperacion = fechaOperacion;
    this.fechaValor = fechaValor;
    this.nutituloLiquidados = nutituloLiquidados;
    this.importeNetoLiquidado = importeNetoLiquidado;
    this.importeEfectivo = importeEfectivo;
    this.importeCorretaje = importeCorretaje;
    this.fechaFichero = fechaFichero;
  }

  public void setNetting(Netting idnetting) {
    this.idnetting = idnetting;
  }

  public Netting getNetting() {
    return this.idnetting;
  }

  public String getIdFichero() {
    return idFichero;
  }

  public void setIdFichero(String idFichero) {
    this.idFichero = idFichero;
  }

  public char getCdorigen() {
    return cdorigen;
  }

  public void setCdorigen(char cdorigen) {
    this.cdorigen = cdorigen;
  }

  public String getNbooking() {
    return nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public char getCdestado() {
    return cdestado;
  }

  public void setCdestado(char cdestado) {
    this.cdestado = cdestado;
  }

  public String getCdestadoS3() {
    return cdestadoS3;
  }

  public void setCdestadoS3(String cdestadoS3) {
    this.cdestadoS3 = cdestadoS3;
  }

  public Date getFechaOperacion() {
    return fechaOperacion;
  }

  public void setFechaOperacion(Date fechaOperacion) {
    this.fechaOperacion = fechaOperacion;
  }

  public Date getFechaValor() {
    return fechaValor;
  }

  public void setFechaValor(Date fechaValor) {
    this.fechaValor = fechaValor;
  }

  public BigDecimal getNutituloLiquidados() {
    return nutituloLiquidados;
  }

  public void setNutituloLiquidados(BigDecimal nutituloLiquidados) {
    this.nutituloLiquidados = nutituloLiquidados;
  }

  public BigDecimal getImporteNetoLiquidado() {
    return importeNetoLiquidado;
  }

  public void setImporteNetoLiquidado(BigDecimal importeNetoLiquidado) {
    this.importeNetoLiquidado = importeNetoLiquidado;
  }

  public char getContabilizado() {
    return contabilizado;
  }

  public void setContabilizado(char contabilizado) {
    this.contabilizado = contabilizado;
  }

  public Date getFechaContabilidad() {
    return fechaContabilidad;
  }

  public void setFechaContabilidad(Date fechaContabilidad) {
    this.fechaContabilidad = fechaContabilidad;
  }

  public BigDecimal getImporteEfectivo() {
    return importeEfectivo;
  }

  public void setImporteEfectivo(BigDecimal importeEfectivo) {
    this.importeEfectivo = importeEfectivo;
  }

  public BigDecimal getImporteCorretaje() {
    return importeCorretaje;
  }

  public void setImporteCorretaje(BigDecimal importeCorretaje) {
    this.importeCorretaje = importeCorretaje;
  }

  /**
   * @return the fechaFichero
   */
  public Date getFechaFichero() {
    return fechaFichero;
  }

  /**
   * @param fechaFichero the fechaFichero to set
   */
  public void setFechaFichero(Date fechaFichero) {
    this.fechaFichero = fechaFichero;
  }

  /**
   * @return the horaValor
   */
  public Date getHoraValor() {
    return horaValor;
  }

  /**
   * @param horaValor the horaValor to set
   */
  public void setHoraValor(Date horaValor) {
    this.horaValor = horaValor;
  }

  /**
   * @return the tipoOperacion
   */
  public String getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * @return the instruccionLiquidacion
   */
  public String getInstruccionLiquidacion() {
    return instruccionLiquidacion;
  }

  /**
   * @return the poolReference
   */
  public String getPoolReference() {
    return poolReference;
  }

  /**
   * @param tipoOperacion the tipoOperacion to set
   */
  public void setTipoOperacion(String tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * @param instruccionLiquidacion the instruccionLiquidacion to set
   */
  public void setInstruccionLiquidacion(String instruccionLiquidacion) {
    this.instruccionLiquidacion = instruccionLiquidacion;
  }

  /**
   * @param poolReference the poolReference to set
   */
  public void setPoolReference(String poolReference) {
    this.poolReference = poolReference;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof S3Liquidacion)) {
      return false;
    }
    S3Liquidacion other = (S3Liquidacion) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "sibbac.business.operativanetting.database.model.S3Liquidacion[ id=" + id + " ]";
  }

}
