package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;


public class SaldoInicialIsin {

	private String		alias;
	private String		descAli;
	private BigDecimal	titulos;
	private BigDecimal	efectivo;
	private String		fliquidacion;
	private boolean		valido;

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getDescAli() {
		return descAli;
	}

	public void setDescAli( String descAli ) {
		this.descAli = descAli;
	}

	public String getFliquidacion() {
		return fliquidacion;
	}

	public void setFliquidacion( String fliquidacion ) {
		this.fliquidacion = fliquidacion;
	}

	public boolean getValido() {
		return valido;
	}

	public void setValido( boolean valido ) {
		this.valido = valido;
	}

	public boolean tieneTitulosOrEfectivo() {
		boolean hayTitulos = ( this.titulos != null && this.titulos.compareTo( new BigDecimal( 0 ) ) != 0 );
		boolean hayEfectivo = ( this.efectivo != null && this.efectivo.compareTo( new BigDecimal( 0 ) ) != 0 );
		return hayTitulos || hayEfectivo;
	}

	public boolean tieneTitulos() {
		boolean hayTitulos = ( this.titulos != null && this.titulos.compareTo( new BigDecimal( 0 ) ) != 0 );
		return hayTitulos;
	}

}
