package sibbac.business.fase0.database.model;




/**
 * This class represent the table TMCT0TFI.
 * INSTRUCCIONES LIQUIDACION TITULARES FINALES
 * Documentación de la tablaTMCT0TFI
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 

 **/
public class Tmct0tfi  {

	/**
	 * Table Field CDALIASS.
	 * CODIGO ALIAS CUENTA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	cdaliass;

	/**
	 * Set value of Field CDALIASS
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _cdaliass value of the field
	 * @see #cdaliass
	
	 **/
	public void setCdaliass( java.lang.String _cdaliass ) {
		cdaliass = _cdaliass;
	}

	/**
	 * Get value of Field CDALIASS
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #cdaliass
	
	 **/
	public java.lang.String getCdaliass() {
		return cdaliass;
	}

	/**
	 * Table Field CDSUBCTA.
	 * CODIGO SUBCUENTA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	cdsubcta;

	/**
	 * Set value of Field CDSUBCTA
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _cdsubcta value of the field
	 * @see #cdsubcta
	
	 **/
	public void setCdsubcta( java.lang.String _cdsubcta ) {
		cdsubcta = _cdsubcta;
	}

	/**
	 * Get value of Field CDSUBCTA
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #cdsubcta
	
	 **/
	public java.lang.String getCdsubcta() {
		return cdsubcta;
	}

	/**
	 * Table Field CENTRO.
	 * CENTRO APLICACION
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	centro;

	/**
	 * Set value of Field CENTRO
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _centro value of the field
	 * @see #centro
	
	 **/
	public void setCentro( java.lang.String _centro ) {
		centro = _centro;
	}

	/**
	 * Get value of Field CENTRO
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #centro
	
	 **/
	public java.lang.String getCentro() {
		return centro;
	}

	/**
	 * Table Field CDDCLAVE.
	 * CODIGO CLAVE TITULAR FINAL
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	cddclave;

	/**
	 * Set value of Field CDDCLAVE
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _cddclave value of the field
	 * @see #cddclave
	
	 **/
	public void setCddclave( java.lang.String _cddclave ) {
		cddclave = _cddclave;
	}

	/**
	 * Get value of Field CDDCLAVE
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #cddclave
	
	 **/
	public java.lang.String getCddclave() {
		return cddclave;
	}

	/**
	 * Table Field NUMSEC.
	 * SECUENCIA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.math.BigDecimal	numsec;

	/**
	 * Set value of Field NUMSEC
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _numsec value of the field
	 * @see #numsec
	
	 **/
	public void setNumsec( java.math.BigDecimal _numsec ) {
		numsec = _numsec;
	}

	/**
	 * Get value of Field NUMSEC
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #numsec
	
	 **/
	public java.math.BigDecimal getNumsec() {
		return numsec;
	}

	/**
	 * Table Field TIPOTIT.
	 * TIPO TITULAR FINAL
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	tipotit;

	/**
	 * Set value of Field TIPOTIT
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _tipotit value of the field
	 * @see #tipotit
	
	 **/
	public void setTipotit( java.lang.String _tipotit ) {
		tipotit = _tipotit;
	}

	/**
	 * Get value of Field TIPOTIT
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #tipotit
	
	 **/
	public java.lang.String getTipotit() {
		return tipotit;
	}

	/**
	 * Table Field CDUSUAUD.
	 * USUARIO AUDITORIA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	cdusuaud;

	/**
	 * Set value of Field CDUSUAUD
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _cdusuaud value of the field
	 * @see #cdusuaud
	
	 **/
	public void setCdusuaud( java.lang.String _cdusuaud ) {
		cdusuaud = _cdusuaud;
	}

	/**
	 * Get value of Field CDUSUAUD
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #cdusuaud
	
	 **/
	public java.lang.String getCdusuaud() {
		return cdusuaud;
	}

	/**
	 * Table Field FHAUDIT.
	 * FECHA AUDITORIA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.sql.Timestamp	fhaudit;

	/**
	 * Set value of Field FHAUDIT
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _fhaudit value of the field
	 * @see #fhaudit
	
	 **/
	public void setFhaudit( java.sql.Timestamp _fhaudit ) {
		fhaudit = _fhaudit;
	}

	/**
	 * Get value of Field FHAUDIT
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #fhaudit
	
	 **/
	public java.sql.Timestamp getFhaudit() {
		return fhaudit;
	}

	/**
	 * Table Field FHINICIO.
	 * FECHA ALTA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.math.BigDecimal	fhinicio;

	/**
	 * Set value of Field FHINICIO
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _fhinicio value of the field
	 * @see #fhinicio
	
	 **/
	public void setFhinicio( java.math.BigDecimal _fhinicio ) {
		fhinicio = _fhinicio;
	}

	/**
	 * Get value of Field FHINICIO
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #fhinicio
	
	 **/
	public java.math.BigDecimal getFhinicio() {
		return fhinicio;
	}

	/**
	 * Table Field FHFINAL.
	 * FECHA BAJA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.math.BigDecimal	fhfinal;

	/**
	 * Set value of Field FHFINAL
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _fhfinal value of the field
	 * @see #fhfinal
	
	 **/
	public void setFhfinal( java.math.BigDecimal _fhfinal ) {
		fhfinal = _fhfinal;
	}

	/**
	 * Get value of Field FHFINAL
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #fhfinal
	
	 **/
	public java.math.BigDecimal getFhfinal() {
		return fhfinal;
	}

	/**
	 * Called before the values in the instance are cleared.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	public void jdoPreClear() {
	}

	/**
	 * Called before the instance is deleted.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	public void jdoPreDelete() {
	}

	/**
	 * Called after the values are loaded from the data store into this instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	public void jdoPostLoad() {
	}

	/**
	 * Called before the values are stored from this instance to the data store.
	 * 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	public void jdoPreStore() {

	}

	/**
	 * Table Field TPTIPREP.
	 * Titular/representante
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	tptiprep;

	/**
	 * Set value of Field TPTIPREP
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _tptiprep value of the field
	 * @see #tptiprep
	
	 **/
	public void setTptiprep( java.lang.String _tptiprep ) {
		tptiprep = _tptiprep;
	}

	/**
	 * Get value of Field TPTIPREP
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #tptiprep
	
	 **/
	public java.lang.String getTptiprep() {
		return tptiprep;
	}

	/**
	 * Table Field CDMERCAD.
	 * Columna importada CDMERCAD
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	
	 **/
	protected java.lang.String	cdmercad;

	/**
	 * Set value of Field CDMERCAD
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param _cdmercad value of the field
	 * @see #cdmercad
	
	 **/
	public void setCdmercad( java.lang.String _cdmercad ) {
		cdmercad = _cdmercad;
	}

	/**
	 * Get value of Field CDMERCAD
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return value of the field
	 * @see #cdmercad
	
	 **/
	public java.lang.String getCdmercad() {
		return cdmercad;
	}

}
