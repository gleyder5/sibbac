package sibbac.business.wrappers.database.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;

@Entity
@Table(name = "TMCT0CLI_HISTORICAL")
public class Tmct0cliHistorical implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2906252452837378382L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Long id;
	private String tpdata;
	private String data;
	private String valueprev;
	private String valuenew;

	@XmlAttribute
	@Temporal(TemporalType.TIMESTAMP)
	private Date fhmod;
	private String usumod;
	private Long idClient;
	
	public Tmct0cliHistorical() {
		super();
	}

	public Tmct0cliHistorical(Long id, String tpdata, String data,
			String valueprev, String valuenew, Date fhmod, String usumod,
			Long idClient) {
		super();
		this.id = id;
		this.tpdata = tpdata;
		this.data = data;
		this.valueprev = valueprev;
		this.valuenew = valuenew;
		this.fhmod = fhmod;
		this.usumod = usumod;
		this.idClient = idClient;
	}
	
	public Tmct0cliHistorical(String tpdata, String data, String valueprev,
			String valuenew, Date fhmod, String usumod, Long idClient) {
		super();
		this.tpdata = tpdata;
		this.data = data;
		this.valueprev = valueprev;
		this.valuenew = valuenew;
		this.fhmod = fhmod;
		this.usumod = usumod;
		this.idClient = idClient;
	}

	public String getFechaModificacion() {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return formatter.format(fhmod);
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "TPDATA", nullable = false, length = 10)
	public String getTpdata() {
		return this.tpdata;
	}

	public void setTpdata(String tpdata) {
		this.tpdata = tpdata;
	}

	@Column(name = "DATA", nullable = false, length = 50)
	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Column(name = "VALUEPREV", nullable = false, length = 200)
	public String getValueprev() {
		return this.valueprev;
	}

	public void setValueprev(String valueprev) {
		this.valueprev = valueprev;
	}

	@Column(name = "VALUENEW", nullable = false, length = 200)
	public String getValuenew() {
		return this.valuenew;
	}

	public void setValuenew(String valuenew) {
		this.valuenew = valuenew;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FHMOD", nullable = false, length = 26)
	public Date getFhmod() {
		return this.fhmod;
	}

	public void setFhmod(Date fhmod) {
		this.fhmod = fhmod;
	}

	@Column(name = "USUMOD", nullable = false, length = 40)
	public String getUsumod() {
		return this.usumod;
	}

	public void setUsumod(String usumod) {
		this.usumod = usumod;
	}

	@Column(name = "IDCLIENT", nullable = false, precision = 8)
	public Long getIdClient() {
		return this.idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}
	
}
