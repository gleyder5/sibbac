package sibbac.business.fase0.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0cfgId;

@Repository
public interface Tmct0cfgDao extends JpaRepository<Tmct0cfg, Tmct0cfgId> {

  @Query("select t from Tmct0cfg t where application = ?1 and process = ?2 and keyname = ?3  ")
  Tmct0cfg findByAplicationAndProcessAndKeyname(final String aplication, final String process, final String keyname);

  @Query("SELECT cfg FROM Tmct0cfg cfg WHERE application = :aplication and process = :process and keyname LIKE :keyname")
  List<Tmct0cfg> findByAplicationAndProcessAndLikeKeyname(@Param("aplication") String aplication,
                                                          @Param("process") String process,
                                                          @Param("keyname") String keyname);

  @Query("SELECT cfg FROM Tmct0cfg cfg WHERE application = 'SBRMV' and process = 'CONFIG' and keyname like 'holydays.anual.days.%'")
  List<Tmct0cfg> findHolidaysBolsa();

  @Modifying
  @Query("UPDATE Tmct0cfg cfg SET cfg.keyvalue=:keyvalue WHERE cfg.id.application=:application AND cfg.id.process=:process AND cfg.id.keyname=:keyname")
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Integer updateKeyvalue(@Param("application") String application,
                                @Param("process") String process,
                                @Param("keyname") String keyname,
                                @Param("keyvalue") String keyvalue);
  
  @Query("SELECT cfg FROM Tmct0cfg cfg WHERE process = :process AND keyname LIKE :keynameLike AND keyvalue = :keyvalue")
  List<Tmct0cfg> findByProcessKeyNameLikeAndKeyValue(
		  						@Param("process") String process, 
		  						@Param("keynameLike") String keynameLike, 
		  						@Param("keyvalue") String keyvalue);

  @Query("SELECT cfg FROM Tmct0cfg cfg WHERE process=:process  AND cfg.id.application=:application")
  List<Tmct0cfg> findByAplicationAndProcess(@Param("application") String application,@Param("process") String process);

} // Tmct0cfgDao
