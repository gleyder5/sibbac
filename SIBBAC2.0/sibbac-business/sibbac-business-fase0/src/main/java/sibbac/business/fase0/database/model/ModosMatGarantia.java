package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_MODOS_MAT_GARANTIAS" )
public class ModosMatGarantia extends MasterData< ModosMatGarantia > {

	private static final long	serialVersionUID	= 2053264655134085848L;

}
