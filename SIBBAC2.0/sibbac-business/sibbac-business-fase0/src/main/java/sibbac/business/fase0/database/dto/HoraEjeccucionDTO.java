package sibbac.business.fase0.database.dto;

import java.io.Serializable;

public class HoraEjeccucionDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 880655980685704991L;
  private final int dDiaDesde;
  private final int dDiaHasta;
  private final int horaInicioEjecucion;
  private final int horaFinEjecucion;

  public HoraEjeccucionDTO(int horaInicioEjecucion, int horaFinEjecucion, int dDiaDesde, int dDiaHasta) {
    super();
    this.dDiaDesde = dDiaDesde;
    this.dDiaHasta = dDiaHasta;
    this.horaInicioEjecucion = horaInicioEjecucion;
    this.horaFinEjecucion = horaFinEjecucion;
  }

  public int getdDiaDesde() {
    return dDiaDesde;
  }

  public int getdDiaHasta() {
    return dDiaHasta;
  }

  public int getHoraInicioEjecucion() {
    return horaInicioEjecucion;
  }

  public int getHoraFinEjecucion() {
    return horaFinEjecucion;
  }

  @Override
  public String toString() {
    return "HoraEjeccucionDTO [dDiaDesde=" + dDiaDesde + ", dDiaHasta=" + dDiaHasta + ", horaInicioEjecucion="
           + horaInicioEjecucion + ", horaFinEjecucion=" + horaFinEjecucion + "]";
  }
  
  

}
