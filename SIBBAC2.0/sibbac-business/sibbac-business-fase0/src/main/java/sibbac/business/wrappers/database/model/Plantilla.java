package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.AliasDireccion }. Entity:
 * "AliasDireccion".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = WRAPPERS.PLANTILLA )
@Entity
@XmlRootElement
@Audited
public class Plantilla extends ATableAudited< Plantilla > implements java.io.Serializable {

	/**
	 *
	 */
	private static final long	serialVersionUID	= -8631885964911134067L;

	@ManyToOne( fetch = FetchType.LAZY, optional = false )
	@JoinColumn( name = "ID_ALIAS", referencedColumnName = "ID", nullable = false )
	private Alias				alias;

	@Column( name = "NBNOMBRE", nullable = false, length = 150 )
	@XmlAttribute
	private String				nombre;

	@Column( name = "NBDESCRIPCION", nullable = false, length = 150 )
	@XmlAttribute
	private String				descripcion;

	public Plantilla() {

	}

	public Alias getAlias() {
		return alias;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setAlias( Alias alias ) {
		this.alias = alias;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
