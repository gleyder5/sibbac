package sibbac.business.canones.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity
@Table(name = "TMCT0_LISTA_VALORES")
public class ListaValores extends DescriptedCanonEntity<BigInteger> {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -5812318409007736061L;
  
  @Id
  @Column(name = ID_COLUMN_NAME)
  private BigInteger id;
  
  @Column(name = "CODIGO", length = 128)
  private String codigo;

  @Override
  public BigInteger getId() {
    return id;
  }

  @Override
  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    ListaValores other = (ListaValores) obj;
    if (codigo == null) {
      if (other.codigo != null)
        return false;
    } else if (!codigo.equals(other.codigo))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return toString("Valores de lista");
  }

}
