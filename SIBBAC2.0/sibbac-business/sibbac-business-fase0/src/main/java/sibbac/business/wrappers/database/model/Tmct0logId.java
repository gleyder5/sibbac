package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0logId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3979654424684523253L;
	private String				nuorden;
	private BigDecimal			lgnumsec;

	public Tmct0logId() {
	}

	public Tmct0logId( String nuorden, BigDecimal lgnumsec ) {
		this.nuorden = nuorden;
		this.lgnumsec = lgnumsec;
	}

	@Column( name = "NUORDEN", nullable = false, length = 20 )
	public String getNuorden() {
		return this.nuorden;
	}

	public void setNuorden( String nuorden ) {
		this.nuorden = nuorden;
	}

	@Column( name = "LGNUMSEC", nullable = false, precision = 20 )
	public BigDecimal getLgnumsec() {
		return this.lgnumsec;
	}

	public void setLgnumsec( BigDecimal lgnumsec ) {
		this.lgnumsec = lgnumsec;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0logId ) )
			return false;
		Tmct0logId castOther = ( Tmct0logId ) other;

		return ( ( this.getNuorden() == castOther.getNuorden() ) || ( this.getNuorden() != null && castOther.getNuorden() != null && this
				.getNuorden().equals( castOther.getNuorden() ) ) )
				&& ( ( this.getLgnumsec() == castOther.getLgnumsec() ) || ( this.getLgnumsec() != null && castOther.getLgnumsec() != null && this
						.getLgnumsec().equals( castOther.getLgnumsec() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getNuorden() == null ? 0 : this.getNuorden().hashCode() );
		result = 37 * result + ( getLgnumsec() == null ? 0 : this.getLgnumsec().hashCode() );
		return result;
	}

}
