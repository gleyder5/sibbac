package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table( name = "TMCT0CTG" )
public class Tmct0ctg implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1139225422512446426L;
	private Tmct0ctgId			id;
	private Tmct0cto			tmct0cto;
	private Tmct0sta			tmct0sta;
	private BigDecimal			imcbmerc;
	private BigDecimal			imcammed;
	private BigDecimal			imcamnet;
	private BigDecimal			imtotbru;
	private BigDecimal			imtotnet;
	private BigDecimal			eutotbru;
	private BigDecimal			eutotnet;
	private BigDecimal			imnetbrk;
	private BigDecimal			eunetbrk;
	private BigDecimal			imcomisn;
	private BigDecimal			imcomieu;
	private BigDecimal			imcomdev;
	private BigDecimal			eucomdev;
	private BigDecimal			imfibrdv;
	private BigDecimal			imfibreu;
	private BigDecimal			imsvb;
	private BigDecimal			imsvbeu;
	private BigDecimal			imbrmerc;
	private BigDecimal			imbrmreu;
	private BigDecimal			imgastos;
	private BigDecimal			imgatdvs;
	private BigDecimal			imimpdvs;
	private BigDecimal			imimpeur;
	private BigDecimal			imcconeu;
	private BigDecimal			imcliqeu;
	private BigDecimal			imdereeu;
	private BigDecimal			nutitagr;
	private BigDecimal			nutitpnr;
	private BigDecimal			imntbreu;
	private BigDecimal			imnetobr;
	private BigDecimal			imntcleu;
	private BigDecimal			imnetocl;
	private Character			cdtpoper;
	private String				cdisin;
	private String				nbvalors;
	private String				cdstcexc;
	private String				cdmoniso;
	private BigDecimal			imtpcamb;
	private BigDecimal			immarpri;
	private BigDecimal			podevolu;
	private BigDecimal			pccombrk;
	private Date				feconsol;
	private Date				fealta;
	private Date				hoalta;
	private Date				fecontra;
	private Date				feejecuc;
	private Date				hoejecuc;
	private Date				fevalide;
	private Date				fevalor;
	private Date				fevalorr;
	private Date				fetrade;
	private Character			cdconcil;
	private Character			cdenvcor;
	private Character			cddatliq;
	private Character			cdbloque;
	private Character			indatfis;
	private String				erdatfis;
	private String				rfparten;
	private Integer				pkbroker1;
	private String				pkentity;
	private String				cdalias;
	private String				descrali;
	private String				numerord;
	private String				dsobserv;
	private String				bacbenefi;
	private String				bacglocus;
	private String				bacloccus;
	private String				bcdbenbic;
	private String				bcdbenefi;
	private String				bcdglocus;
	private String				bcdloccus;
	private String				bgcusbic;
	private String				bidbenefi;
	private String				bidglocus;
	private String				bidloccus;
	private String				blcusbic;
	private String				cdusublo;
	private String				cdusuaud;
	private Date				fhaudit;
	private Integer				nuversion;
	private Character			inbokmul;
	private String				cdauxili;
	private String				idtr;
	private String				statr;
	private BigDecimal			imgasliqdv;
	private BigDecimal			imgasliqeu;
	private BigDecimal			imgasgesdv;
	private BigDecimal			imgasgeseu;
	private BigDecimal			imgastotdv;
	private BigDecimal			imgastoteu;
	private Character			cdindgas;
	private Character			cdmtfnet;
	private List< Tmct0cta >	tmct0ctas			= new ArrayList< Tmct0cta >( 0 );

	public Tmct0ctg() {
	}

	public Tmct0ctg( Tmct0ctgId id, Tmct0cto tmct0cto, Tmct0sta tmct0sta, String erdatfis, String pkentity, String numerord ) {
		this.id = id;
		this.tmct0cto = tmct0cto;
		this.tmct0sta = tmct0sta;
		this.erdatfis = erdatfis;
		this.pkentity = pkentity;
		this.numerord = numerord;
	}

	public Tmct0ctg( Tmct0ctgId id, Tmct0cto tmct0cto, Tmct0sta tmct0sta, BigDecimal imcbmerc, BigDecimal imcammed, BigDecimal imcamnet,
			BigDecimal imtotbru, BigDecimal imtotnet, BigDecimal eutotbru, BigDecimal eutotnet, BigDecimal imnetbrk, BigDecimal eunetbrk,
			BigDecimal imcomisn, BigDecimal imcomieu, BigDecimal imcomdev, BigDecimal eucomdev, BigDecimal imfibrdv, BigDecimal imfibreu,
			BigDecimal imsvb, BigDecimal imsvbeu, BigDecimal imbrmerc, BigDecimal imbrmreu, BigDecimal imgastos, BigDecimal imgatdvs,
			BigDecimal imimpdvs, BigDecimal imimpeur, BigDecimal imcconeu, BigDecimal imcliqeu, BigDecimal imdereeu, BigDecimal nutitagr,
			BigDecimal nutitpnr, BigDecimal imntbreu, BigDecimal imnetobr, BigDecimal imntcleu, BigDecimal imnetocl, Character cdtpoper,
			String cdisin, String nbvalors, String cdstcexc, String cdmoniso, BigDecimal imtpcamb, BigDecimal immarpri,
			BigDecimal podevolu, BigDecimal pccombrk, Date feconsol, Date fealta, Date hoalta, Date fecontra, Date feejecuc, Date hoejecuc,
			Date fevalide, Date fevalor, Date fevalorr, Date fetrade, Character cdconcil, Character cdenvcor, Character cddatliq,
			Character cdbloque, Character indatfis, String erdatfis, String rfparten, Integer pkbroker1, String pkentity, String cdalias,
			String descrali, String numerord, String dsobserv, String bacbenefi, String bacglocus, String bacloccus, String bcdbenbic,
			String bcdbenefi, String bcdglocus, String bcdloccus, String bgcusbic, String bidbenefi, String bidglocus, String bidloccus,
			String blcusbic, String cdusublo, String cdusuaud, Date fhaudit, Integer nuversion, Character inbokmul, String cdauxili,
			String idtr, String statr, BigDecimal imgasliqdv, BigDecimal imgasliqeu, BigDecimal imgasgesdv, BigDecimal imgasgeseu,
			BigDecimal imgastotdv, BigDecimal imgastoteu, Character cdindgas, Character cdmtfnet, List< Tmct0cta > tmct0ctas ) {
		this.id = id;
		this.tmct0cto = tmct0cto;
		this.tmct0sta = tmct0sta;
		this.imcbmerc = imcbmerc;
		this.imcammed = imcammed;
		this.imcamnet = imcamnet;
		this.imtotbru = imtotbru;
		this.imtotnet = imtotnet;
		this.eutotbru = eutotbru;
		this.eutotnet = eutotnet;
		this.imnetbrk = imnetbrk;
		this.eunetbrk = eunetbrk;
		this.imcomisn = imcomisn;
		this.imcomieu = imcomieu;
		this.imcomdev = imcomdev;
		this.eucomdev = eucomdev;
		this.imfibrdv = imfibrdv;
		this.imfibreu = imfibreu;
		this.imsvb = imsvb;
		this.imsvbeu = imsvbeu;
		this.imbrmerc = imbrmerc;
		this.imbrmreu = imbrmreu;
		this.imgastos = imgastos;
		this.imgatdvs = imgatdvs;
		this.imimpdvs = imimpdvs;
		this.imimpeur = imimpeur;
		this.imcconeu = imcconeu;
		this.imcliqeu = imcliqeu;
		this.imdereeu = imdereeu;
		this.nutitagr = nutitagr;
		this.nutitpnr = nutitpnr;
		this.imntbreu = imntbreu;
		this.imnetobr = imnetobr;
		this.imntcleu = imntcleu;
		this.imnetocl = imnetocl;
		this.cdtpoper = cdtpoper;
		this.cdisin = cdisin;
		this.nbvalors = nbvalors;
		this.cdstcexc = cdstcexc;
		this.cdmoniso = cdmoniso;
		this.imtpcamb = imtpcamb;
		this.immarpri = immarpri;
		this.podevolu = podevolu;
		this.pccombrk = pccombrk;
		this.feconsol = feconsol;
		this.fealta = fealta;
		this.hoalta = hoalta;
		this.fecontra = fecontra;
		this.feejecuc = feejecuc;
		this.hoejecuc = hoejecuc;
		this.fevalide = fevalide;
		this.fevalor = fevalor;
		this.fevalorr = fevalorr;
		this.fetrade = fetrade;
		this.cdconcil = cdconcil;
		this.cdenvcor = cdenvcor;
		this.cddatliq = cddatliq;
		this.cdbloque = cdbloque;
		this.indatfis = indatfis;
		this.erdatfis = erdatfis;
		this.rfparten = rfparten;
		this.pkbroker1 = pkbroker1;
		this.pkentity = pkentity;
		this.cdalias = cdalias;
		this.descrali = descrali;
		this.numerord = numerord;
		this.dsobserv = dsobserv;
		this.bacbenefi = bacbenefi;
		this.bacglocus = bacglocus;
		this.bacloccus = bacloccus;
		this.bcdbenbic = bcdbenbic;
		this.bcdbenefi = bcdbenefi;
		this.bcdglocus = bcdglocus;
		this.bcdloccus = bcdloccus;
		this.bgcusbic = bgcusbic;
		this.bidbenefi = bidbenefi;
		this.bidglocus = bidglocus;
		this.bidloccus = bidloccus;
		this.blcusbic = blcusbic;
		this.cdusublo = cdusublo;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
		this.inbokmul = inbokmul;
		this.cdauxili = cdauxili;
		this.idtr = idtr;
		this.statr = statr;
		this.imgasliqdv = imgasliqdv;
		this.imgasliqeu = imgasliqeu;
		this.imgasgesdv = imgasgesdv;
		this.imgasgeseu = imgasgeseu;
		this.imgastotdv = imgastotdv;
		this.imgastoteu = imgastoteu;
		this.cdindgas = cdindgas;
		this.cdmtfnet = cdmtfnet;
		this.tmct0ctas = tmct0ctas;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "nbooking", column = @Column( name = "NBOOKING", nullable = false, length = 20 ) ),
			@AttributeOverride( name = "cdbroker", column = @Column( name = "CDBROKER", nullable = false, length = 20 ) ),
			@AttributeOverride( name = "nuorden", column = @Column( name = "NUORDEN", nullable = false, length = 20 ) ),
			@AttributeOverride( name = "cdmercad", column = @Column( name = "CDMERCAD", nullable = false, length = 20 ) )
	} )
	public Tmct0ctgId getId() {
		return this.id;
	}

	public void setId( Tmct0ctgId id ) {
		this.id = id;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( {
			@JoinColumn( name = "CDMERCAD", referencedColumnName = "CDMERCAD", nullable = false, insertable = false, updatable = false ),
			@JoinColumn( name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false ),
			@JoinColumn( name = "CDBROKER", referencedColumnName = "CDBROKER", nullable = false, insertable = false, updatable = false )
	} )
	public Tmct0cto getTmct0cto() {
		return this.tmct0cto;
	}

	public void setTmct0cto( Tmct0cto tmct0cto ) {
		this.tmct0cto = tmct0cto;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( {
			@JoinColumn( name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false ),
			@JoinColumn( name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false )
	} )
	public Tmct0sta getTmct0sta() {
		return this.tmct0sta;
	}

	public void setTmct0sta( Tmct0sta tmct0sta ) {
		this.tmct0sta = tmct0sta;
	}

	@Column( name = "IMCBMERC", precision = 18, scale = 8 )
	public BigDecimal getImcbmerc() {
		return this.imcbmerc;
	}

	public void setImcbmerc( BigDecimal imcbmerc ) {
		this.imcbmerc = imcbmerc;
	}

	@Column( name = "IMCAMMED", precision = 18, scale = 8 )
	public BigDecimal getImcammed() {
		return this.imcammed;
	}

	public void setImcammed( BigDecimal imcammed ) {
		this.imcammed = imcammed;
	}

	@Column( name = "IMCAMNET", precision = 18, scale = 8 )
	public BigDecimal getImcamnet() {
		return this.imcamnet;
	}

	public void setImcamnet( BigDecimal imcamnet ) {
		this.imcamnet = imcamnet;
	}

	@Column( name = "IMTOTBRU", precision = 18, scale = 8 )
	public BigDecimal getImtotbru() {
		return this.imtotbru;
	}

	public void setImtotbru( BigDecimal imtotbru ) {
		this.imtotbru = imtotbru;
	}

	@Column( name = "IMTOTNET", precision = 18, scale = 8 )
	public BigDecimal getImtotnet() {
		return this.imtotnet;
	}

	public void setImtotnet( BigDecimal imtotnet ) {
		this.imtotnet = imtotnet;
	}

	@Column( name = "EUTOTBRU", precision = 18, scale = 8 )
	public BigDecimal getEutotbru() {
		return this.eutotbru;
	}

	public void setEutotbru( BigDecimal eutotbru ) {
		this.eutotbru = eutotbru;
	}

	@Column( name = "EUTOTNET", precision = 18, scale = 8 )
	public BigDecimal getEutotnet() {
		return this.eutotnet;
	}

	public void setEutotnet( BigDecimal eutotnet ) {
		this.eutotnet = eutotnet;
	}

	@Column( name = "IMNETBRK", precision = 18, scale = 8 )
	public BigDecimal getImnetbrk() {
		return this.imnetbrk;
	}

	public void setImnetbrk( BigDecimal imnetbrk ) {
		this.imnetbrk = imnetbrk;
	}

	@Column( name = "EUNETBRK", precision = 18, scale = 8 )
	public BigDecimal getEunetbrk() {
		return this.eunetbrk;
	}

	public void setEunetbrk( BigDecimal eunetbrk ) {
		this.eunetbrk = eunetbrk;
	}

	@Column( name = "IMCOMISN", precision = 18, scale = 8 )
	public BigDecimal getImcomisn() {
		return this.imcomisn;
	}

	public void setImcomisn( BigDecimal imcomisn ) {
		this.imcomisn = imcomisn;
	}

	@Column( name = "IMCOMIEU", precision = 18, scale = 8 )
	public BigDecimal getImcomieu() {
		return this.imcomieu;
	}

	public void setImcomieu( BigDecimal imcomieu ) {
		this.imcomieu = imcomieu;
	}

	@Column( name = "IMCOMDEV", precision = 18, scale = 8 )
	public BigDecimal getImcomdev() {
		return this.imcomdev;
	}

	public void setImcomdev( BigDecimal imcomdev ) {
		this.imcomdev = imcomdev;
	}

	@Column( name = "EUCOMDEV", precision = 18, scale = 8 )
	public BigDecimal getEucomdev() {
		return this.eucomdev;
	}

	public void setEucomdev( BigDecimal eucomdev ) {
		this.eucomdev = eucomdev;
	}

	@Column( name = "IMFIBRDV", precision = 18, scale = 8 )
	public BigDecimal getImfibrdv() {
		return this.imfibrdv;
	}

	public void setImfibrdv( BigDecimal imfibrdv ) {
		this.imfibrdv = imfibrdv;
	}

	@Column( name = "IMFIBREU", precision = 18, scale = 8 )
	public BigDecimal getImfibreu() {
		return this.imfibreu;
	}

	public void setImfibreu( BigDecimal imfibreu ) {
		this.imfibreu = imfibreu;
	}

	@Column( name = "IMSVB", precision = 18, scale = 8 )
	public BigDecimal getImsvb() {
		return this.imsvb;
	}

	public void setImsvb( BigDecimal imsvb ) {
		this.imsvb = imsvb;
	}

	@Column( name = "IMSVBEU", precision = 18, scale = 8 )
	public BigDecimal getImsvbeu() {
		return this.imsvbeu;
	}

	public void setImsvbeu( BigDecimal imsvbeu ) {
		this.imsvbeu = imsvbeu;
	}

	@Column( name = "IMBRMERC", precision = 18, scale = 8 )
	public BigDecimal getImbrmerc() {
		return this.imbrmerc;
	}

	public void setImbrmerc( BigDecimal imbrmerc ) {
		this.imbrmerc = imbrmerc;
	}

	@Column( name = "IMBRMREU", precision = 18, scale = 8 )
	public BigDecimal getImbrmreu() {
		return this.imbrmreu;
	}

	public void setImbrmreu( BigDecimal imbrmreu ) {
		this.imbrmreu = imbrmreu;
	}

	@Column( name = "IMGASTOS", precision = 18, scale = 8 )
	public BigDecimal getImgastos() {
		return this.imgastos;
	}

	public void setImgastos( BigDecimal imgastos ) {
		this.imgastos = imgastos;
	}

	@Column( name = "IMGATDVS", precision = 18, scale = 8 )
	public BigDecimal getImgatdvs() {
		return this.imgatdvs;
	}

	public void setImgatdvs( BigDecimal imgatdvs ) {
		this.imgatdvs = imgatdvs;
	}

	@Column( name = "IMIMPDVS", precision = 18, scale = 8 )
	public BigDecimal getImimpdvs() {
		return this.imimpdvs;
	}

	public void setImimpdvs( BigDecimal imimpdvs ) {
		this.imimpdvs = imimpdvs;
	}

	@Column( name = "IMIMPEUR", precision = 18, scale = 8 )
	public BigDecimal getImimpeur() {
		return this.imimpeur;
	}

	public void setImimpeur( BigDecimal imimpeur ) {
		this.imimpeur = imimpeur;
	}

	@Column( name = "IMCCONEU", precision = 18, scale = 8 )
	public BigDecimal getImcconeu() {
		return this.imcconeu;
	}

	public void setImcconeu( BigDecimal imcconeu ) {
		this.imcconeu = imcconeu;
	}

	@Column( name = "IMCLIQEU", precision = 18, scale = 8 )
	public BigDecimal getImcliqeu() {
		return this.imcliqeu;
	}

	public void setImcliqeu( BigDecimal imcliqeu ) {
		this.imcliqeu = imcliqeu;
	}

	@Column( name = "IMDEREEU", precision = 18, scale = 8 )
	public BigDecimal getImdereeu() {
		return this.imdereeu;
	}

	public void setImdereeu( BigDecimal imdereeu ) {
		this.imdereeu = imdereeu;
	}

	@Column( name = "NUTITAGR", precision = 16, scale = 5 )
	public BigDecimal getNutitagr() {
		return this.nutitagr;
	}

	public void setNutitagr( BigDecimal nutitagr ) {
		this.nutitagr = nutitagr;
	}

	@Column( name = "NUTITPNR", precision = 16, scale = 5 )
	public BigDecimal getNutitpnr() {
		return this.nutitpnr;
	}

	public void setNutitpnr( BigDecimal nutitpnr ) {
		this.nutitpnr = nutitpnr;
	}

	@Column( name = "IMNTBREU", precision = 18, scale = 8 )
	public BigDecimal getImntbreu() {
		return this.imntbreu;
	}

	public void setImntbreu( BigDecimal imntbreu ) {
		this.imntbreu = imntbreu;
	}

	@Column( name = "IMNETOBR", precision = 18, scale = 8 )
	public BigDecimal getImnetobr() {
		return this.imnetobr;
	}

	public void setImnetobr( BigDecimal imnetobr ) {
		this.imnetobr = imnetobr;
	}

	@Column( name = "IMNTCLEU", precision = 18, scale = 8 )
	public BigDecimal getImntcleu() {
		return this.imntcleu;
	}

	public void setImntcleu( BigDecimal imntcleu ) {
		this.imntcleu = imntcleu;
	}

	@Column( name = "IMNETOCL", precision = 18, scale = 8 )
	public BigDecimal getImnetocl() {
		return this.imnetocl;
	}

	public void setImnetocl( BigDecimal imnetocl ) {
		this.imnetocl = imnetocl;
	}

	@Column( name = "CDTPOPER", length = 1 )
	public Character getCdtpoper() {
		return this.cdtpoper;
	}

	public void setCdtpoper( Character cdtpoper ) {
		this.cdtpoper = cdtpoper;
	}

	@Column( name = "CDISIN", length = 12 )
	public String getCdisin() {
		return this.cdisin;
	}

	public void setCdisin( String cdisin ) {
		this.cdisin = cdisin;
	}

	@Column( name = "NBVALORS", length = 40 )
	public String getNbvalors() {
		return this.nbvalors;
	}

	public void setNbvalors( String nbvalors ) {
		this.nbvalors = nbvalors;
	}

	@Column( name = "CDSTCEXC", length = 3 )
	public String getCdstcexc() {
		return this.cdstcexc;
	}

	public void setCdstcexc( String cdstcexc ) {
		this.cdstcexc = cdstcexc;
	}

	@Column( name = "CDMONISO", length = 3 )
	public String getCdmoniso() {
		return this.cdmoniso;
	}

	public void setCdmoniso( String cdmoniso ) {
		this.cdmoniso = cdmoniso;
	}

	@Column( name = "IMTPCAMB", precision = 18, scale = 8 )
	public BigDecimal getImtpcamb() {
		return this.imtpcamb;
	}

	public void setImtpcamb( BigDecimal imtpcamb ) {
		this.imtpcamb = imtpcamb;
	}

	@Column( name = "IMMARPRI", precision = 18, scale = 8 )
	public BigDecimal getImmarpri() {
		return this.immarpri;
	}

	public void setImmarpri( BigDecimal immarpri ) {
		this.immarpri = immarpri;
	}

	@Column( name = "PODEVOLU", precision = 16, scale = 6 )
	public BigDecimal getPodevolu() {
		return this.podevolu;
	}

	public void setPodevolu( BigDecimal podevolu ) {
		this.podevolu = podevolu;
	}

	@Column( name = "PCCOMBRK", precision = 16, scale = 6 )
	public BigDecimal getPccombrk() {
		return this.pccombrk;
	}

	public void setPccombrk( BigDecimal pccombrk ) {
		this.pccombrk = pccombrk;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FECONSOL", length = 4 )
	public Date getFeconsol() {
		return this.feconsol;
	}

	public void setFeconsol( Date feconsol ) {
		this.feconsol = feconsol;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEALTA", length = 4 )
	public Date getFealta() {
		return this.fealta;
	}

	public void setFealta( Date fealta ) {
		this.fealta = fealta;
	}

	@Temporal( TemporalType.TIME )
	@Column( name = "HOALTA", length = 3 )
	public Date getHoalta() {
		return this.hoalta;
	}

	public void setHoalta( Date hoalta ) {
		this.hoalta = hoalta;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FECONTRA", length = 4 )
	public Date getFecontra() {
		return this.fecontra;
	}

	public void setFecontra( Date fecontra ) {
		this.fecontra = fecontra;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEEJECUC", length = 4 )
	public Date getFeejecuc() {
		return this.feejecuc;
	}

	public void setFeejecuc( Date feejecuc ) {
		this.feejecuc = feejecuc;
	}

	@Temporal( TemporalType.TIME )
	@Column( name = "HOEJECUC", length = 3 )
	public Date getHoejecuc() {
		return this.hoejecuc;
	}

	public void setHoejecuc( Date hoejecuc ) {
		this.hoejecuc = hoejecuc;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEVALIDE", length = 4 )
	public Date getFevalide() {
		return this.fevalide;
	}

	public void setFevalide( Date fevalide ) {
		this.fevalide = fevalide;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEVALOR", length = 4 )
	public Date getFevalor() {
		return this.fevalor;
	}

	public void setFevalor( Date fevalor ) {
		this.fevalor = fevalor;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEVALORR", length = 4 )
	public Date getFevalorr() {
		return this.fevalorr;
	}

	public void setFevalorr( Date fevalorr ) {
		this.fevalorr = fevalorr;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FETRADE", length = 4 )
	public Date getFetrade() {
		return this.fetrade;
	}

	public void setFetrade( Date fetrade ) {
		this.fetrade = fetrade;
	}

	@Column( name = "CDCONCIL", length = 1 )
	public Character getCdconcil() {
		return this.cdconcil;
	}

	public void setCdconcil( Character cdconcil ) {
		this.cdconcil = cdconcil;
	}

	@Column( name = "CDENVCOR", length = 1 )
	public Character getCdenvcor() {
		return this.cdenvcor;
	}

	public void setCdenvcor( Character cdenvcor ) {
		this.cdenvcor = cdenvcor;
	}

	@Column( name = "CDDATLIQ", length = 1 )
	public Character getCddatliq() {
		return this.cddatliq;
	}

	public void setCddatliq( Character cddatliq ) {
		this.cddatliq = cddatliq;
	}

	@Column( name = "CDBLOQUE", length = 1 )
	public Character getCdbloque() {
		return this.cdbloque;
	}

	public void setCdbloque( Character cdbloque ) {
		this.cdbloque = cdbloque;
	}

	@Column( name = "INDATFIS", length = 1 )
	public Character getIndatfis() {
		return this.indatfis;
	}

	public void setIndatfis( Character indatfis ) {
		this.indatfis = indatfis;
	}

	@Column( name = "ERDATFIS", nullable = false, length = 40 )
	public String getErdatfis() {
		return this.erdatfis;
	}

	public void setErdatfis( String erdatfis ) {
		this.erdatfis = erdatfis;
	}

	@Column( name = "RFPARTEN", length = 16 )
	public String getRfparten() {
		return this.rfparten;
	}

	public void setRfparten( String rfparten ) {
		this.rfparten = rfparten;
	}

	@Column( name = "PKBROKER1", precision = 9 )
	public Integer getPkbroker1() {
		return this.pkbroker1;
	}

	public void setPkbroker1( Integer pkbroker1 ) {
		this.pkbroker1 = pkbroker1;
	}

	@Column( name = "PKENTITY", nullable = false, length = 16 )
	public String getPkentity() {
		return this.pkentity;
	}

	public void setPkentity( String pkentity ) {
		this.pkentity = pkentity;
	}

	@Column( name = "CDALIAS", length = 20 )
	public String getCdalias() {
		return this.cdalias;
	}

	public void setCdalias( String cdalias ) {
		this.cdalias = cdalias;
	}

	@Column( name = "DESCRALI", length = 130 )
	public String getDescrali() {
		return this.descrali;
	}

	public void setDescrali( String descrali ) {
		this.descrali = descrali;
	}

	@Column( name = "NUMERORD", nullable = false, length = 20 )
	public String getNumerord() {
		return this.numerord;
	}

	public void setNumerord( String numerord ) {
		this.numerord = numerord;
	}

	@Column( name = "DSOBSERV", length = 200 )
	public String getDsobserv() {
		return this.dsobserv;
	}

	public void setDsobserv( String dsobserv ) {
		this.dsobserv = dsobserv;
	}

	@Column( name = "BACBENEFI", length = 34 )
	public String getBacbenefi() {
		return this.bacbenefi;
	}

	public void setBacbenefi( String bacbenefi ) {
		this.bacbenefi = bacbenefi;
	}

	@Column( name = "BACGLOCUS", length = 34 )
	public String getBacglocus() {
		return this.bacglocus;
	}

	public void setBacglocus( String bacglocus ) {
		this.bacglocus = bacglocus;
	}

	@Column( name = "BACLOCCUS", length = 34 )
	public String getBacloccus() {
		return this.bacloccus;
	}

	public void setBacloccus( String bacloccus ) {
		this.bacloccus = bacloccus;
	}

	@Column( name = "BCDBENBIC", length = 11 )
	public String getBcdbenbic() {
		return this.bcdbenbic;
	}

	public void setBcdbenbic( String bcdbenbic ) {
		this.bcdbenbic = bcdbenbic;
	}

	@Column( name = "BCDBENEFI", length = 146 )
	public String getBcdbenefi() {
		return this.bcdbenefi;
	}

	public void setBcdbenefi( String bcdbenefi ) {
		this.bcdbenefi = bcdbenefi;
	}

	@Column( name = "BCDGLOCUS", length = 146 )
	public String getBcdglocus() {
		return this.bcdglocus;
	}

	public void setBcdglocus( String bcdglocus ) {
		this.bcdglocus = bcdglocus;
	}

	@Column( name = "BCDLOCCUS", length = 146 )
	public String getBcdloccus() {
		return this.bcdloccus;
	}

	public void setBcdloccus( String bcdloccus ) {
		this.bcdloccus = bcdloccus;
	}

	@Column( name = "BGCUSBIC", length = 11 )
	public String getBgcusbic() {
		return this.bgcusbic;
	}

	public void setBgcusbic( String bgcusbic ) {
		this.bgcusbic = bgcusbic;
	}

	@Column( name = "BIDBENEFI", length = 43 )
	public String getBidbenefi() {
		return this.bidbenefi;
	}

	public void setBidbenefi( String bidbenefi ) {
		this.bidbenefi = bidbenefi;
	}

	@Column( name = "BIDGLOCUS", length = 43 )
	public String getBidglocus() {
		return this.bidglocus;
	}

	public void setBidglocus( String bidglocus ) {
		this.bidglocus = bidglocus;
	}

	@Column( name = "BIDLOCCUS", length = 43 )
	public String getBidloccus() {
		return this.bidloccus;
	}

	public void setBidloccus( String bidloccus ) {
		this.bidloccus = bidloccus;
	}

	@Column( name = "BLCUSBIC", length = 11 )
	public String getBlcusbic() {
		return this.blcusbic;
	}

	public void setBlcusbic( String blcusbic ) {
		this.blcusbic = blcusbic;
	}

	@Column( name = "CDUSUBLO", length = 10 )
	public String getCdusublo() {
		return this.cdusublo;
	}

	public void setCdusublo( String cdusublo ) {
		this.cdusublo = cdusublo;
	}

	@Column( name = "CDUSUAUD", length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "NUVERSION", precision = 4 )
	public Integer getNuversion() {
		return this.nuversion;
	}

	public void setNuversion( Integer nuversion ) {
		this.nuversion = nuversion;
	}

	@Column( name = "INBOKMUL", length = 1 )
	public Character getInbokmul() {
		return this.inbokmul;
	}

	public void setInbokmul( Character inbokmul ) {
		this.inbokmul = inbokmul;
	}

	@Column( name = "CDAUXILI", length = 20 )
	public String getCdauxili() {
		return this.cdauxili;
	}

	public void setCdauxili( String cdauxili ) {
		this.cdauxili = cdauxili;
	}

	@Column( name = "IDTR", length = 40 )
	public String getIdtr() {
		return this.idtr;
	}

	public void setIdtr( String idtr ) {
		this.idtr = idtr;
	}

	@Column( name = "STATR", length = 3 )
	public String getStatr() {
		return this.statr;
	}

	public void setStatr( String statr ) {
		this.statr = statr;
	}

	@Column( name = "IMGASLIQDV", precision = 18, scale = 8 )
	public BigDecimal getImgasliqdv() {
		return this.imgasliqdv;
	}

	public void setImgasliqdv( BigDecimal imgasliqdv ) {
		this.imgasliqdv = imgasliqdv;
	}

	@Column( name = "IMGASLIQEU", precision = 18, scale = 8 )
	public BigDecimal getImgasliqeu() {
		return this.imgasliqeu;
	}

	public void setImgasliqeu( BigDecimal imgasliqeu ) {
		this.imgasliqeu = imgasliqeu;
	}

	@Column( name = "IMGASGESDV", precision = 18, scale = 8 )
	public BigDecimal getImgasgesdv() {
		return this.imgasgesdv;
	}

	public void setImgasgesdv( BigDecimal imgasgesdv ) {
		this.imgasgesdv = imgasgesdv;
	}

	@Column( name = "IMGASGESEU", precision = 18, scale = 8 )
	public BigDecimal getImgasgeseu() {
		return this.imgasgeseu;
	}

	public void setImgasgeseu( BigDecimal imgasgeseu ) {
		this.imgasgeseu = imgasgeseu;
	}

	@Column( name = "IMGASTOTDV", precision = 18, scale = 8 )
	public BigDecimal getImgastotdv() {
		return this.imgastotdv;
	}

	public void setImgastotdv( BigDecimal imgastotdv ) {
		this.imgastotdv = imgastotdv;
	}

	@Column( name = "IMGASTOTEU", precision = 18, scale = 8 )
	public BigDecimal getImgastoteu() {
		return this.imgastoteu;
	}

	public void setImgastoteu( BigDecimal imgastoteu ) {
		this.imgastoteu = imgastoteu;
	}

	@Column( name = "CDINDGAS", length = 1 )
	public Character getCdindgas() {
		return this.cdindgas;
	}

	public void setCdindgas( Character cdindgas ) {
		this.cdindgas = cdindgas;
	}

	@Column( name = "CDMTFNET", length = 1 )
	public Character getCdmtfnet() {
		return this.cdmtfnet;
	}

	public void setCdmtfnet( Character cdmtfnet ) {
		this.cdmtfnet = cdmtfnet;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0ctg" )
	public List< Tmct0cta > getTmct0ctas() {
		return this.tmct0ctas;
	}

	public void setTmct0ctas( List< Tmct0cta > tmct0ctas ) {
		this.tmct0ctas = tmct0ctas;
	}

}
