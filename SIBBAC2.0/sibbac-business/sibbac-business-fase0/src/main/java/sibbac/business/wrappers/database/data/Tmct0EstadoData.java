package sibbac.business.wrappers.database.data;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.model.Tmct0estado;

public class Tmct0EstadoData implements Serializable {
  /**
     * 
     */
  private static final long serialVersionUID = 1L;
  private Integer idestado;
  private String nombre;
  private Integer idtipoestado;
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0EstadoData.class);

  public Tmct0EstadoData(Tmct0estado estado) {
    entityToData(estado, this);
  }

  public Tmct0EstadoData() {
    //
  }

  public Integer getIdestado() {
    return idestado;
  }

  public void setIdestado(Integer idestado) {
    this.idestado = idestado;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Integer getIdtipoestado() {
    return idtipoestado;
  }

  public void setIdtipoestado(Integer idtipoestado) {
    this.idtipoestado = idtipoestado;
  }

  public static Tmct0EstadoData entityToData(Tmct0estado entity) {
    LOG.debug("[entityToData(Tmct0estado entity)] Inicio ...");
    Tmct0EstadoData data = new Tmct0EstadoData();
    entityToData(entity, data);
    LOG.debug("[entityToData(Tmct0estado entity)] Fin ...");
    return data;
  }

  public static void entityToData(Tmct0estado entity, Tmct0EstadoData data) {
    LOG.debug("[entityToData(Tmct0estado entity, Tmct0EstadoData data)] Inicio ...");
    if (entity != null) {
      data.setIdestado(entity.getIdestado());
      data.setIdtipoestado(entity.getIdtipoestado());
      data.setNombre(entity.getNombre());
    }
    LOG.debug("[entityToData(Tmct0estado entity, Tmct0EstadoData data)] Fin ...");
  }

  public static Tmct0estado dataToEntity(Tmct0EstadoData data) {
    Tmct0estado entity = new Tmct0estado();
    dataToEntity(data, entity);
    return entity;
  }

  public static void dataToEntity(Tmct0EstadoData data, Tmct0estado entity) {
    if (data != null) {
      entity.setIdestado(data.getIdestado());
      entity.setIdtipoestado(data.getIdtipoestado());
      entity.setNombre(data.getNombre());
    }
  }

  public static Tmct0EstadoData copy(Tmct0EstadoData data) {
    Tmct0EstadoData copy = new Tmct0EstadoData();
    if (data != null) {
      copy.setIdestado(data.getIdestado());
      copy.setIdtipoestado(data.getIdtipoestado());
      copy.setNombre(data.getNombre());
    }
    return copy;
  }

}
