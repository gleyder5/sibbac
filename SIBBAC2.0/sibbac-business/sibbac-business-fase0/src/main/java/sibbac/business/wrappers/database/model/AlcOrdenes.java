package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATable;

/**
 * Class that represents a
 * {@link sibbac.business.wrappers.database.model.AlcOrdenes } . Entity:
 * "AlcOrdenes".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table(name = WRAPPERS.ALC_ORDEN)
@Entity
public class AlcOrdenes extends ATable<AlcOrdenes> {

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

    /** Identificador para la serialización de objetos. */
    private static final long serialVersionUID = 9081239442995540410L;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

    @Column(name = "NBOOKING", nullable = false, length = 20)
    private String nbooking = "";

    @Column(name = "NUORDEN", nullable = false, length = 20)
    private String nuorden = "";

    @Column(name = "NUCNFLIQ", nullable = false, precision = 4)
    private short nucnfliq = 0;

    @Column(name = "NUCNFCLT", nullable = false, length = 20)
    private String nucnfclt = "";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns(value = { @JoinColumn(name = "IDNETTING", referencedColumnName = "ID") })
    private Netting netting;

    @ManyToOne(optional = true)
    @JoinColumn(name = "ID_ESTADO", nullable = true, referencedColumnName = "IDESTADO")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Tmct0estado estado;

    @OneToMany(mappedBy = "alcOrden", fetch = FetchType.EAGER)
    private List<AlcOrdenDetalleFactura> alcOrdenDetallesFactura;

    @Column(name = "REFERENCIAS3", length = 20)
    private String referenciaS3;

    @NotAudited
    @Column(name = "CUENTA_COMPENSACION_DESTINO", length = 3)
    private String cuentaCompensacionDestino;

    @NotAudited
    @Column(name = "TITULOS_ENVIADOS")
    private BigDecimal titulosEnviados;

    @NotAudited
    @Column(name = "TITULOS_ASIGNADOS")
    private BigDecimal titulosAsignados;

    @NotAudited
    @Column(name = "ESTADO")
    private Character estadoInstruccion = 'A';

    @NotAudited
    @OneToMany(mappedBy = "alcOrdenes", cascade = CascadeType.ALL)
    private List<AlcOrdenMercado> ordenesMercado = new ArrayList<>();
    
    @NotAudited
    @Column(name = "IDCAMARA")
    private Long idCamaraCompensacion;
    
    @NotAudited
    @Column(name = "FELIQUIDACION")
    @Temporal(TemporalType.DATE)
    private Date feliquidacion;

    public AlcOrdenes() {
    } // AlcOrdenes

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

    public String getNbooking() {
        return nbooking;
    }

    public String getNuorden() {
        return nuorden;
    }

    public short getNucnfliq() {
        return nucnfliq;
    }

    public String getNucnfclt() {
        return nucnfclt;
    }

    public boolean isEstadoBaja() {
        if(estadoInstruccion != null)
            return estadoInstruccion.equals('B');
        return false;
    }

    public boolean isEstadoAlta() {
        if(estadoInstruccion != null)
            return estadoInstruccion.equals('A');
        return false;
    }

    public String getCuentaCompensacionDestino() {
        return cuentaCompensacionDestino;
    }

    public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
        this.cuentaCompensacionDestino = cuentaCompensacionDestino;
    }

    public BigDecimal getTitulosEnviados() {
        return titulosEnviados;
    }

    public void setTitulosEnviados(BigDecimal titulosEnviados) {
        this.titulosEnviados = titulosEnviados;
    }

    public BigDecimal getTitulosAsignados() {
        return titulosAsignados;
    }

    public void setTitulosAsignados(BigDecimal titulosAsignados) {
        this.titulosAsignados = titulosAsignados;
    }

    public Character getEstadoInstruccion() {
        return estadoInstruccion;
    }

    public void setEstadoInstruccion(Character estadoInstruccion) {
        this.estadoInstruccion = estadoInstruccion;
    }

    /**
     * @return
     */
    public Netting getNetting() {
        return this.netting;
    }

    /**
     * @return the estado
     */
    public Tmct0estado getEstado() {
        return estado;
    }

    /**
     * @return the alcOrdenDetallesFactura
     */
    public List<AlcOrdenDetalleFactura> getAlcOrdenDetallesFactura() {
        return alcOrdenDetallesFactura;
    }

    /**
     * @param referenciaS3
     *            the referenciaS3
     */
    public String getReferenciaS3() {
        return referenciaS3;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

    public void setNbooking(String nbooking) {
        this.nbooking = nbooking;
    }

    public void setNuorden(String nuorden) {
        this.nuorden = nuorden;
    }

    public void setNucnfliq(short nucnfliq) {
        this.nucnfliq = nucnfliq;
    }

    public void setNucnfclt(String nucnfclt) {
        this.nucnfclt = nucnfclt;
    }

    /**
     * @param netting
     */
    public void setNetting(Netting netting) {
        this.netting = netting;
    }

    /**
     * @param estado
     *            the estado to set
     */
    public void setEstado(Tmct0estado estado) {
        this.estado = estado;
    }

    /**
     * @param alcOrdenDetallesFactura
     *            the alcOrdenDetallesFactura to set
     */
    public void setAlcOrdenDetallesFactura(List<AlcOrdenDetalleFactura> alcOrdenDetallesFactura) {
        this.alcOrdenDetallesFactura = alcOrdenDetallesFactura;
    }

    /**
     * @return the referenciaS3
     */
    public void setReferenciaS3(String referenciaS3) {
        this.referenciaS3 = referenciaS3;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

    /**
     * @param alcOrden
     * @return
     */
    public boolean addFacturaDetalle(final FacturaDetalle facturaDetalle) {
        if (CollectionUtils.isEmpty(alcOrdenDetallesFactura)) {
            alcOrdenDetallesFactura = new ArrayList<AlcOrdenDetalleFactura>();
        }
        final AlcOrdenDetalleFactura alcOrdenDetalleFactura = new AlcOrdenDetalleFactura();
        alcOrdenDetalleFactura.setAlcOrden(this);
        alcOrdenDetalleFactura.setFacturaDetalle(facturaDetalle);
        return alcOrdenDetallesFactura.add(alcOrdenDetalleFactura);
    }

    public void setOrdenesMercado(List<AlcOrdenMercado> ordenesMercado) {
        this.ordenesMercado = ordenesMercado;
    }
    
    public List<AlcOrdenMercado> getOrdenesMercado() {
        return ordenesMercado;
    }
    
    public void setIdCamaraCompensacion(Long idCamaraCompensacion) {
        this.idCamaraCompensacion = idCamaraCompensacion;
    }
    
    public Long getIdCamaraCompensacion() {
        return idCamaraCompensacion;
    }
    
    public void setFeliquidacion(Date feliquidacion) {
      this.feliquidacion = feliquidacion;
    }
    
    public Date getFeliquidacion() {
      return feliquidacion;
    }
    
    @PostPersist
    public void assignS3Key() {
      final String format;
      
      if(getReferenciaS3() == null && nbooking != null) {
        format = nbooking.equals("0") ? "MN%014d" : "C%015d";
        setReferenciaS3(String.format(format, id));
      }
    }

    @Transient
    public Tmct0alcId createAlcIdFromFields() {
        return new Tmct0alcId(nbooking, nuorden, nucnfliq, nucnfclt);
    }
    
    @Transient
    public boolean aceptable(Tmct0desglosecamara desglose) {
      if(!desglose.getFeliquidacion().equals(feliquidacion)) {
        return false;
      }
      return true;
    }

    @Transient @Transactional(propagation=Propagation.REQUIRED, noRollbackFor=AnulacionSuperaAsignadosException.class)
    public boolean correspondeAnulacion(Tmct0desglosecamara baja) throws AnulacionSuperaAsignadosException {
        BigDecimal restantes;
        
        for (AlcOrdenMercado ordenMercado : getOrdenesMercado())
            if (ordenMercado.getDesgloseCamara().equals(baja)
                    && ordenMercado.isEstadoAlta()) {
                restantes = getTitulosAsignados().subtract(baja.getNutitulos());
                if(restantes.signum() < 0)
                    throw new AnulacionSuperaAsignadosException(restantes.abs());
                setTitulosAsignados(restantes);
                ordenMercado.registraBaja();
                return true;
            }
        return false;
    }

    @Transient @Transactional(propagation=Propagation.REQUIRED, noRollbackFor=PendientesSuperaRestantesException.class)
    public boolean aceptaPendienteEnvio(Tmct0desglosecamara pendiente) throws PendientesSuperaRestantesException {
        final AlcOrdenMercado ordenMercado;
        final Tmct0alcId alcId;
        final BigDecimal restantes;
        final Tmct0CamaraCompensacion camaraCompensacion;
        
        camaraCompensacion = pendiente.getTmct0CamaraCompensacion();
        if(camaraCompensacion == null)
            return false;
        if (Objects.equals(pendiente.getCuentaCompensacion(), cuentaCompensacionDestino) 
                && Objects.equals(camaraCompensacion.getIdCamaraCompensacion(), idCamaraCompensacion)) {
            restantes = titulosEnviados.subtract(titulosAsignados);
            if(pendiente.getNutitulos().compareTo(restantes) > 0)
                throw new PendientesSuperaRestantesException(restantes);
            alcId = pendiente.getTmct0alc().getId();
            ordenMercado = new AlcOrdenMercado();
            ordenMercado.setAlcOrdenes(this);
            ordenMercado.setnBooking(alcId.getNbooking());
            ordenMercado.setNuOrden(alcId.getNuorden());
            ordenMercado.setNuCnfLiq(alcId.getNucnfliq());
            ordenMercado.setNuCnfClt(alcId.getNucnfclt());
            ordenMercado.setTitulos(pendiente.getNutitulos());
            ordenMercado.setEstado('A');
            ordenMercado.setAuditDate(new Date());
            ordenMercado.setAuditUser("SIBBAC");
            ordenMercado.setDesgloseCamara(pendiente);
            ordenesMercado.add(ordenMercado);
            setTitulosAsignados(getTitulosAsignados().add(pendiente.getNutitulos()));
            return true;
        }
        return false;
    }

    @Transient
    public boolean yaEnviado(Tmct0desglosecamara pendiente) {
        for(AlcOrdenMercado ordenMercado : ordenesMercado)
            if(ordenMercado.getDesgloseCamara().equals(pendiente) &&
                    ordenMercado.isEstadoAlta())
                return true;
        return false;
    }

    /**
     * Verifica si por lo menos alguna orden mercado tiene un desglose camara con estado Pendiente Anulacion
     * @return verdadero en caso de la AlcOrden está marcada para anulacion.
     */
    @Transient @Transactional(propagation=Propagation.REQUIRED)
    public boolean conSolicitudAnulacionTotal() {
        int estadoEntrec;
        
        for(AlcOrdenMercado om : ordenesMercado) {
            estadoEntrec = om.getDesgloseCamara().getTmct0estadoByCdestadoentrec().getIdestado();
            if(estadoEntrec == CLEARING.PDTE_ENVIAR_ANULACION.getId())
                return true;
        }
        return false;
    }

    @Transient @Transactional(propagation=Propagation.MANDATORY)
    public void anulaTotalmente(Tmct0estado estadoAnulado) {
        setTitulosEnviados(BigDecimal.ZERO);
        setTitulosAsignados(BigDecimal.ZERO);
        cancelaTotalmente(estadoAnulado);
    }
    
    @Transient @Transactional(propagation=Propagation.MANDATORY)
    public void cancelaTotalmente(Tmct0estado estado) {
        setEstadoInstruccion('B');
        for(AlcOrdenMercado om : ordenesMercado) {
            om.setEstado('B');
            om.getDesgloseCamara().setTmct0estadoByCdestadoentrec(estado);
        }
    }
    
    @Transient 
    public Tmct0alc getAlcdeReferencia() {
        for(AlcOrdenMercado om : ordenesMercado) 
            if(om.isEstadoAlta())
                return om.getDesgloseCamara().getTmct0alc();
        return null;
    }

    @SuppressWarnings("serial")
    public class AnulacionSuperaAsignadosException extends Exception {
        
        private AnulacionSuperaAsignadosException(BigDecimal cantidad) {
            super(MessageFormat.format("No procede baja porque supera en {0} los titulos asignados", cantidad));
        }
        
    }
    
    @SuppressWarnings("serial")
    public class PendientesSuperaRestantesException extends Exception {
        
        private PendientesSuperaRestantesException(BigDecimal cantidad) {
            super(MessageFormat.format("No procede envio de pendientes porque se supera al restante {0}", cantidad));
        }
        
    }


} // AlcOrdenes
