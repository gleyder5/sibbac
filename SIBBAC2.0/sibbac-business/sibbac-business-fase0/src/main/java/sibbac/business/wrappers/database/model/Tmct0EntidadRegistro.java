/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 *
 * @author fjarquellada
 */
@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_ENTIDAD_REGISTRO )
@XmlRootElement
public class Tmct0EntidadRegistro extends ATable< Tmct0EntidadRegistro > implements Serializable {

	private static final long			serialVersionUID	= 1L;

	@Basic( optional = false )
	@Column( name = "BIC" )
	private String						bic;
	@Basic( optional = false )
	@Column( name = "NOMBRE" )
	private String						nombre;
	@Basic( optional = false )
	@ManyToOne
	@JoinColumn( name = "TIPOER", referencedColumnName = "ID" )
	private Tmct0TipoEr					tipoer;
	@OneToMany( cascade = CascadeType.ALL, mappedBy = "er", fetch = FetchType.LAZY )
	private List< Tmct0CuentaVirtual >	tmct0CuentaVirtualList;

	public Tmct0EntidadRegistro() {
	}

	public Tmct0EntidadRegistro( String bic, String nombre, Tmct0TipoEr tipoer ) {
		this.bic = bic;
		this.nombre = nombre;
		this.tipoer = tipoer;
	}

	public String getBic() {
		return bic;
	}

	public void setBic( String bic ) {
		this.bic = bic;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public Tmct0TipoEr getTipoer() {
		return tipoer;
	}

	public void setTipoer( Tmct0TipoEr tipoer ) {
		this.tipoer = tipoer;
	}

	@XmlTransient
	public List< Tmct0CuentaVirtual > getTmct0CuentaVirtualList() {
		return tmct0CuentaVirtualList;
	}

	public void setTmct0CuentaVirtualList( List< Tmct0CuentaVirtual > tmct0CuentaVirtualList ) {
		this.tmct0CuentaVirtualList = tmct0CuentaVirtualList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != null ? id.hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if ( !( object instanceof Tmct0EntidadRegistro ) ) {
			return false;
		}
		Tmct0EntidadRegistro other = ( Tmct0EntidadRegistro ) object;
		if ( ( id == null && other.id != null ) || ( this.id != null && !this.id.equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sibbac.business.conciliacion.database.model.Tmct0EntidadRegistro[ id=" + id + " ]";
	}

}
