package sibbac.business.wrappers.database.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import sibbac.business.wrappers.database.model.CodigoError;
import sibbac.business.wrappers.database.model.Tmct0AfiError;

public class CodigoErrorData {

  protected Long id;
  protected Long version = 0L;
  protected Date auditDate;
  protected String auditUser;
  protected String codigo;
  protected String descripcion;
  protected String grupo;

  public CodigoErrorData(Long id, String codigo, String descripcion, String grupo) {
    this.id = id;
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.grupo = grupo;
  }

  private Set<Tmct0AfiError> tmct0AfiError = null;

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() {
    return this.version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public Date getAuditDate() {
    return this.auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public String getCodigo() {
    return this.codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getDescripcion() {
    return this.descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getGrupo() {
    return this.grupo;
  }

  public void setGrupo(String grupo) {
    this.grupo = grupo;
  }

  public CodigoErrorData(CodigoError codigoError) {
    entityToData(codigoError, this);
  }

  public CodigoErrorData() {
  }

  public static CodigoErrorData entityToData(CodigoError entity) {
    CodigoErrorData data = new CodigoErrorData();
    entityToData(entity, data);
    return data;
  }// public static CodigoErrorData entityToData(CodigoError entity){

  public static void entityToData(CodigoError entity, CodigoErrorData data) {
    data.setAuditDate(entity.getAuditDate());
    data.setAuditUser(entity.getAuditUser());
    data.setCodigo(entity.getCodigo());
    data.setDescripcion(entity.getDescripcion());
    data.setGrupo(entity.getGrupo());
    data.setId(entity.getId());
    data.setVersion(entity.getVersion());
  }// public static void entityToData(CodigoError entity, CodigoErrorData data)

  public static Set<CodigoErrorData> entityListToDataList(Set<CodigoError> entityErrorSet) {
    Set<CodigoErrorData> dataErrorSet = null;
    if (entityErrorSet != null) {
      dataErrorSet = new HashSet<CodigoErrorData>();
      if (!entityErrorSet.isEmpty()) {
        Iterator<CodigoError> iterator = entityErrorSet.iterator();
        while (iterator.hasNext()) {
          CodigoError entity = iterator.next();
          dataErrorSet.add(CodigoErrorData.entityToData(entity));
        }// while (iterator.hasNext())
      }// if (!entityErrorSet.isEmpty())
    }// if (entityErrorSet != null)
    return dataErrorSet;
  }// public static Set<CodigoErrorData> entityListToDataList(Set<CodigoError> entityErrorSet) {

  public static Set<CodigoError> dataListToEntityList(Set<CodigoErrorData> dataErrorSet) {
    Set<CodigoError> entityErrorSet = null;
    if (dataErrorSet != null) {
      entityErrorSet = new HashSet<CodigoError>();
      if (!dataErrorSet.isEmpty()) {
        Iterator<CodigoErrorData> iterator = dataErrorSet.iterator();
        while (iterator.hasNext()) {
          CodigoErrorData data = iterator.next();
          entityErrorSet.add(CodigoErrorData.dataToEntity(data));
        }// while (iterator.hasNext())
      }// if (!dataErrorSet.isEmpty())
    }// if (dataErrorSet != null)
    return entityErrorSet;
  }// public static Set<CodigoError> dataListToEntityList(Set<CodigoErrorData> dataErrorSet) {

  public static CodigoError dataToEntity(CodigoErrorData data) {
    CodigoError entity = new CodigoError();
    dataToEntity(data, entity);
    return entity;
  }// public static CodigoError dataToEntity(CodigoErrorData data){

  public static void dataToEntity(CodigoErrorData data, CodigoError entity) {
    if (data.getAuditDate() == null) {
      entity.setAuditDate(new Date());
    } else {
      entity.setAuditDate(data.getAuditDate());
    }

    if (data.getAuditUser() == null) {
      entity.setAuditUser("MIDDLE");
    } else {
      entity.setAuditUser(data.getAuditUser());
    }
    entity.setCodigo(data.getCodigo());
    entity.setDescripcion(data.getDescripcion());
    entity.setGrupo(data.getGrupo());
    entity.setId(data.getId());
    entity.setVersion(data.getVersion());
  }// public static void dataToEntity(CodigoError entity, CodigoErrorData data)

  public void addTmct0AfiErrorData(Tmct0AfiErrorData afiError) {

    if (afiError != null) {
      if (this.tmct0AfiError == null) {
        tmct0AfiError = new HashSet<Tmct0AfiError>();
      }
      tmct0AfiError.add(Tmct0AfiErrorData.dataToEntityWithoutErrors(afiError));
    }

  }

}// public class CodigoErrorData {
