package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import sibbac.database.model.ATable;

@MappedSuperclass
public class RecordBean extends ATable< RecordBean > {

	private static final long	serialVersionUID	= 61668097840937588L;

	@Column( name = "TIPOREG", length = 4 )
	protected String	tipoRegistro;

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro( String tipoRegistro ) {
		this.tipoRegistro = tipoRegistro;
	}

	public enum TipoTitular {
		MENOR( 'M' ), REPRESENTANTE( 'R' ), USUFRUCTUARIO( 'U' ), NUDO( 'N' ), TITULAR( 'T' );

		private final Character	tipoTitular;

		/**
		 * @param tipoTitular
		 */
		private TipoTitular( final Character tipoTitular ) {
			this.tipoTitular = tipoTitular;
		}

		/**
		 * @return the tipoTitular
		 */
		public Character getTipoTitular() {
			return tipoTitular;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */

		@Override
		public String toString() {
			return tipoTitular.toString();
		}

		public static TipoTitular getTipoTitular( Character tipoTitular ) {
			TipoTitular[] fields = TipoTitular.values();
			TipoTitular tt = null;
			for ( int i = 0; i < fields.length; i++ ) {
				if ( fields[ i ].tipoTitular.equals( tipoTitular ) ) {
					tt = fields[ i ];
					break;
				}
			}
			return tt;
		}
	}

	public enum TipoDocumento {
		NIF( 'N' ), CIF( 'C' ), NIE( 'E' ), BIC( 'B' ), OTROS( 'O' );

		private final Character	tipoDocumento;

		/**
		 * @param tipoTitular
		 */
		private TipoDocumento( final Character tipoDocumento ) {
			this.tipoDocumento = tipoDocumento;
		}

		/**
		 * @return the tipoTitular
		 */
		public Character getTipoDocumento() {
			return tipoDocumento;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return tipoDocumento.toString();
		}

		public static TipoDocumento getTipoDocumento( Character tipoDocumento ) {
			TipoDocumento[] fields = TipoDocumento.values();
			TipoDocumento td = null;
			for ( int i = 0; i < fields.length; i++ ) {
				if ( fields[ i ].tipoDocumento.equals( tipoDocumento ) ) {
					td = fields[ i ];
					break;
				}
			}
			return td;
		}
	}

	public enum IndicadorNacionalidad {
		NACIONAL( 'N' ), EXTRANJERO( 'E' );

		private final Character	indicadorNacionalidad;

		/**
		 * @param tipoTitular
		 */
		private IndicadorNacionalidad( final Character indicadorNacionalidad ) {
			this.indicadorNacionalidad = indicadorNacionalidad;
		}

		/**
		 * @return the tipoTitular
		 */
		public Character getIndicadorNacionalidad() {
			return indicadorNacionalidad;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return indicadorNacionalidad.toString();
		}

		public static IndicadorNacionalidad getIndicadorNacionalidad( Character indicadorNacionalidad ) {
			IndicadorNacionalidad[] fields = IndicadorNacionalidad.values();
			IndicadorNacionalidad indNac = null;
			for ( int i = 0; i < fields.length; i++ ) {
				if ( fields[ i ].indicadorNacionalidad.equals( indicadorNacionalidad ) ) {
					indNac = fields[ i ];
					break;
				}
			}
			return indNac;
		}
	}

}
