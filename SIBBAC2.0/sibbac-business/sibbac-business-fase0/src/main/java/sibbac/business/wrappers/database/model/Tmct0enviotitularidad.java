package sibbac.business.wrappers.database.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0ENVIOTITULARIDAD")
public class Tmct0enviotitularidad implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -2940993311848318013L;
  private Long idenviotitularidad;
  private Tmct0referenciatitular tmct0referenciatitular;
  private Date feinicio = new Date();
  private Date fenvio;
  private Date frecepcion;
  private Character variosenvios;
  private Date auditFechaCambio = new Date();
  private String auditUser;
  private Character estado;
  private List<Tmct0desglosecamara> tmct0desglosecamaras = new ArrayList<Tmct0desglosecamara>(0);
  private List<Tmct0DesgloseCamaraEnvioRt> tmct0DesgloseCamaraEnvioRt = new ArrayList<Tmct0DesgloseCamaraEnvioRt>(0);

  private String cdmiembromkt;
  private Tmct0CamaraCompensacion camaraCompensacion;

  public Tmct0enviotitularidad() {
  }

  public Tmct0enviotitularidad(Long idenviotitularidad, Tmct0referenciatitular tmct0referenciatitular) {
    this.idenviotitularidad = idenviotitularidad;
    this.tmct0referenciatitular = tmct0referenciatitular;
  }

  public Tmct0enviotitularidad(Long idenviotitularidad, Tmct0referenciatitular tmct0referenciatitular, Date feinicio,
      Date fenvio, Date frecepcion, Character variosenvios, Date auditFechaCambio, String auditUser, Character estado,
      List<Tmct0desglosecamara> tmct0desglosecamaras) {
    this.idenviotitularidad = idenviotitularidad;
    this.tmct0referenciatitular = tmct0referenciatitular;
    this.feinicio = feinicio;
    this.fenvio = fenvio;
    this.frecepcion = frecepcion;
    this.variosenvios = variosenvios;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.estado = estado;
    this.tmct0desglosecamaras = tmct0desglosecamaras;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDENVIOTITULARIDAD", unique = true, nullable = false)
  public Long getIdenviotitularidad() {
    return this.idenviotitularidad;
  }

  public void setIdenviotitularidad(Long idenviotitularidad) {
    this.idenviotitularidad = idenviotitularidad;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDREFERENCIA", nullable = false)
  public Tmct0referenciatitular getTmct0referenciatitular() {
    return this.tmct0referenciatitular;
  }

  public void setTmct0referenciatitular(Tmct0referenciatitular tmct0referenciatitular) {
    this.tmct0referenciatitular = tmct0referenciatitular;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEINICIO", length = 10)
  public Date getFeinicio() {
    return this.feinicio;
  }

  public void setFeinicio(Date feinicio) {
    this.feinicio = feinicio;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FENVIO", length = 10)
  public Date getFenvio() {
    return this.fenvio;
  }

  public void setFenvio(Date fenvio) {
    this.fenvio = fenvio;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FRECEPCION", length = 10)
  public Date getFrecepcion() {
    return this.frecepcion;
  }

  public void setFrecepcion(Date frecepcion) {
    this.frecepcion = frecepcion;
  }

  @Column(name = "VARIOSENVIOS", length = 1)
  public Character getVariosenvios() {
    return this.variosenvios;
  }

  public void setVariosenvios(Character variosenvios) {
    this.variosenvios = variosenvios;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "ESTADO", length = 1)
  public Character getEstado() {
    return this.estado;
  }

  public void setEstado(Character estado) {
    this.estado = estado;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0enviotitularidad")
  public List<Tmct0desglosecamara> getTmct0desglosecamaras() {
    return this.tmct0desglosecamaras;
  }

  public void setTmct0desglosecamaras(List<Tmct0desglosecamara> tmct0desglosecamaras) {
    this.tmct0desglosecamaras = tmct0desglosecamaras;
  }

  @Column(name = "CDMIEMBROMKT", length = 10, nullable = false)
  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "envioTitularidad")
  public List<Tmct0DesgloseCamaraEnvioRt> getTmct0DesgloseCamaraEnvioRt() {
    return tmct0DesgloseCamaraEnvioRt;
  }

  public void setTmct0DesgloseCamaraEnvioRt(List<Tmct0DesgloseCamaraEnvioRt> tmct0DesgloseCamaraEnvioRt) {
    this.tmct0DesgloseCamaraEnvioRt = tmct0DesgloseCamaraEnvioRt;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDCAMARA", nullable = false)
  public Tmct0CamaraCompensacion getCamaraCompensacion() {
    return camaraCompensacion;
  }

  public void setCamaraCompensacion(Tmct0CamaraCompensacion camaraCompensacion) {
    this.camaraCompensacion = camaraCompensacion;
  }

  @Override
  public String toString() {
    return "Tmct0enviotitularidad [idenviotitularidad=" + idenviotitularidad + ", tmct0referenciatitular="
        + tmct0referenciatitular + ", feinicio=" + feinicio + ", estado=" + estado + ", cdmiembromkt=" + cdmiembromkt
        + ", camaraCompensacion=" + camaraCompensacion + "]";
  }

}
