package sibbac.business.wrappers.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table( name = "TMCT0_NEMOTECNICOS" )
public class Tmct0Nemotecnicos implements java.io.Serializable {


	private static final long serialVersionUID = 8389641057373340574L;
	
	private Long id_nemotecnico;
	private String nbnombre;
	private String nbNemotecnico;
	private Long id_compensador;
	private Date audit_fecha_cambio;
	private String audit_user;
	private String cd_referencia_asignacion;
	private String nbDescripcion;
	
	
	public Tmct0Nemotecnicos() {}


	public Tmct0Nemotecnicos(Long id_nemotecnico, String nbnombre,
			String nbNemotecnico, Long id_compensador,
			Date audit_fecha_cambio, String audit_user,
			String cd_referencia_asignacion, String nb_descripcion) {
		super();
		this.id_nemotecnico = id_nemotecnico;
		this.nbnombre = nbnombre;
		this.nbNemotecnico = nbNemotecnico;
		this.id_compensador = id_compensador;
		this.audit_fecha_cambio = audit_fecha_cambio;
		this.audit_user = audit_user;
		this.cd_referencia_asignacion = cd_referencia_asignacion;
		this.nbDescripcion = nbDescripcion;
	}

	@Id
	@Column( name = "ID_NEMOTECNICO", unique = true, nullable = false, length = 8 )
	public Long getId_nemotecnico() {
		return id_nemotecnico;
	}


	public void setId_nemotecnico(Long id_nemotecnico) {
		this.id_nemotecnico = id_nemotecnico;
	}

	@Column( name = "NB_NOMBRE", length = 16 )
	public String getNbnombre() {
		return nbnombre;
	}


	public void setNbnombre(String nbnombre) {
		this.nbnombre = nbnombre;
	}
	
	
	@Column( name = "NB_NEMOTECNICO", length = 64 )
	public String getNbNemotecnico() {
		return nbNemotecnico;
	}


	public void setNbNemotecnico(String nbNemotecnico) {
		this.nbNemotecnico = nbNemotecnico;
	}

	@Column( name = "ID_COMPENSADOR", nullable = false, length = 8 )
	public Long getId_compensador() {
		return id_compensador;
	}





	public void setId_compensador(Long id_compensador) {
		this.id_compensador = id_compensador;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "AUDIT_FECHA_CAMBIO", nullable = false, length = 10 )
	public Date getAudit_fecha_cambio() {
		return audit_fecha_cambio;
	}


	public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
		this.audit_fecha_cambio = audit_fecha_cambio;
	}

	@Column( name = "AUDIT_USER", length = 20 )
	public String getAudit_user() {
		return audit_user;
	}


	public void setAudit_user(String audit_user) {
		this.audit_user = audit_user;
	}

	@Column( name = "CD_REFERENCIA_ASIGNACION", length = 64 )
	public String getCd_referencia_asignacion() {
		return cd_referencia_asignacion;
	}


	public void setCd_referencia_asignacion(String cd_referencia_asignacion) {
		this.cd_referencia_asignacion = cd_referencia_asignacion;
	}

	@Column( name = "NB_DESCRIPCION", length = 255 )
	public String getNbDescripcion() {
		return nbDescripcion;
	}


	public void setNbDescripcion(String nb_descripcion) {
		this.nbDescripcion = nb_descripcion;
	}


	

}
