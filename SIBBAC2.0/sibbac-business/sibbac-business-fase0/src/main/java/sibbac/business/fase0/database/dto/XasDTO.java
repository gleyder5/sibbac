package sibbac.business.fase0.database.dto;

public class XasDTO {
	
	private String	codigo;
	private String	descripcion;

	public XasDTO( String codigo, String descripcion ) {
		this();
		this.codigo = ( codigo != null && !"".equals( codigo ) ) ? codigo.trim() : "";
		this.descripcion = ( descripcion != null && !"".equals( descripcion ) ) ? descripcion.trim() : "";
	}
	
	public XasDTO(  ) {
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
