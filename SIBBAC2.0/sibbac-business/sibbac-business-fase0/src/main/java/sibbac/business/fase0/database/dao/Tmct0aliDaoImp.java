package sibbac.business.fase0.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.Tmct0aliDTO;

@Repository
public class Tmct0aliDaoImp {

		private static final Logger LOG = LoggerFactory.getLogger(Tmct0aliDaoImp.class);
	
		@PersistenceContext
		private EntityManager	em;
		
		@SuppressWarnings("unchecked")
		
		public List<Tmct0aliDTO> findAliasByManyReglas(List<Long> idReglas){
			
			final StringBuilder select = new StringBuilder("select regla.id_regla, regla.cd_codigo, ali.cdaliass, ali.descrali" );

			final StringBuilder from = new StringBuilder( " from TMCT0ALI ali left join tmct0_regla regla on regla.ID_REGLA = ali.id_regla " );
			final StringBuilder where = new StringBuilder( "WHERE ali.ID_REGLA in ( :idReglas ) order by regla.activo DESC, regla.id_Regla"   );
			
			List<Object> resultList = null;
			
			
			final Query query = em.createNativeQuery( select.append( from.append( where ) )	.toString() );
			setParametersWithVectors( query, idReglas, "idReglas");
			List<Tmct0aliDTO> listaAlias = new ArrayList<Tmct0aliDTO>();
			
			try{
			    resultList = query.getResultList();
				if ( CollectionUtils.isNotEmpty( resultList ) ) {
					for ( Object obj : resultList ) {
						Tmct0aliDTO alias = Tmct0aliDTO.convertObjectToAliaDTO( ( Object[] ) obj );
						
						listaAlias.add( alias );
					}
				}
			}catch(Exception ex){
				LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
			}
			return listaAlias;
		}
		
		private void setParametersWithVectors( Query query,List<Long> idReglas, String idReglasParamName ) {
			if(idReglas != null && idReglas.size() > 0){
				query.setParameter(idReglasParamName, idReglas);
			}
		}

}
