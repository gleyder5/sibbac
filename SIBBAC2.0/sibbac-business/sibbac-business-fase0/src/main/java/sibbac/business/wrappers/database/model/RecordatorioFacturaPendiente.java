package sibbac.business.wrappers.database.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.AliasRecordatorio }. Entity:
 * "AliasRecordatorio".
 * 
 * @author pbellon
 * @version 1.0
 * @since 1.0
 */

//
@Table( name = WRAPPERS.FACTURAS_PENDIENTES_RECORDATORIO )
@Entity
@XmlRootElement
@Audited
public class RecordatorioFacturaPendiente extends ATableAudited< RecordatorioFacturaPendiente > implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -202389974463686303L;

	@OneToOne( fetch = FetchType.LAZY, optional = false )
	@JoinColumn( name = "ID_FACTURA", referencedColumnName = "ID", nullable = false )
	@XmlAttribute
	private Factura				factura;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ESTADO", nullable = false, referencedColumnName = "IDESTADO" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Tmct0estado			estado;

	@Column( name = "FECHA_RECORDATORIO" )
	@XmlAttribute
	private Date				fechaRecordatorio;

	public RecordatorioFacturaPendiente() {

	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura( Factura factura ) {
		this.factura = factura;
	}

	public Date getFechaRecordatorio() {
		return fechaRecordatorio;
	}

	public void setFechaRecordatorio( Date fechaRecordatorio ) {
		this.fechaRecordatorio = fechaRecordatorio;
	}

	public Tmct0estado getEstado() {
		return estado;
	}

	public void setEstado( Tmct0estado estado ) {
		this.estado = estado;
	}

}
