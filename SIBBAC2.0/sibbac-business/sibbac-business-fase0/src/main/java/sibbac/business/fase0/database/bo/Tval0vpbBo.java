package sibbac.business.fase0.database.bo;


import org.springframework.stereotype.Service;

import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnvasql.dao.Tval0vpbDao;
import sibbac.database.bsnvasql.model.Tval0vpb;


@Service
public class Tval0vpbBo extends AbstractBo< Tval0vpb, String, Tval0vpbDao > {

}
