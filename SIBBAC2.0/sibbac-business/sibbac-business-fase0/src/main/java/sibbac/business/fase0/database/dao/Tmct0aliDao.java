package sibbac.business.fase0.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0aliId;

@Repository
public interface Tmct0aliDao extends JpaRepository<Tmct0ali, Tmct0aliId> {

  public Tmct0ali findFirst1ByIdCdaliass(@Param("cdaliass") String cdaliass);

  public List<Tmct0ali> findByFhfinal(final Integer fhFinal);

  @Query("SELECT ALI.id.cdaliass||'|'||ALI.descrali FROM Tmct0ali ALI WHERE ALI.indNeting!='' GROUP BY ALI.id.cdaliass, ALI.descrali ORDER BY ALI.id.cdaliass")
  public List<String> findAliasNetting();

  @Query("select a from Tmct0ali a where a.id.cdaliass = :cdaliass AND a.fhfinal = :fhfinal")
  public Tmct0ali findByCdaliass(@Param("cdaliass") String cdaliass, @Param("fhfinal") Integer fhfinal);

  @Query("select a from Tmct0ali a where a.id.cdaliass = :cdaliass AND a.fhfinal >= :fecha AND a.fhinicio <= :fecha")
  public Tmct0ali findByCdaliassFechaActual(@Param("cdaliass") String cdaliass, @Param("fecha") Integer fecha);

  @Query("SELECT ALI.id.cdaliass||'|'||ALI.descrali FROM Tmct0ali ALI GROUP BY ALI.id.cdaliass, ALI.descrali  ORDER BY ALI.id.cdaliass  ")
  public List<String> findAlias();

  @Query("SELECT new sibbac.business.fase0.database.dto.Tmct0aliDTO(a.id.cdaliass, a.descrali) FROM Tmct0ali a WHERE CAST(a.fhinicio as integer) <= :fechaActivo AND CAST(a.fhfinal as integer)>= :fechaActivo ORDER BY a.id.cdaliass ASC")
  public List<Tmct0aliDTO> findAllAli(@Param("fechaActivo") Integer fechaActivacion);

  @Query("SELECT ALI FROM Tmct0ali ALI WHERE ALI.id.cdaliass=:cdaliass ORDER BY ALI.id.numsec DESC")
  public List<Tmct0ali> findByAliasOrder(@Param("cdaliass") String cdaliass);

  @Query(value = "select cdaliass,descrali from tmct0ali where fhfinal=99991231 and INSTR(UPPER (trim(cdaliass)||"
      + "'-'||trim(descrali)),upper(:text))<>0 and cdaliass not in (:alias) FETCH FIRST 12 ROWS ONLY", nativeQuery = true)
  List<Object[]> findByText(@Param("text") String text, @Param("alias") List<String> alias);

  @Query("SELECT ALI FROM Tmct0ali ALI WHERE ALI.idRegla=:id_regla ORDER BY ALI.id.cdaliass")
  public List<Tmct0ali> findCdAliassById_regla(@Param("id_regla") Long id_regla);

  @Query("SELECT ALI.id.cdaliass FROM Tmct0ali ALI WHERE ALI.id.cdaliass=:cdaliass")
  public List<String> existeAlias(@Param("cdaliass") String cdaliass);

  @Query("SELECT ALI FROM Tmct0ali ALI WHERE ALI.id.cdaliass=:cdaliass")
  public List<Tmct0ali> findByCdaliass(@Param("cdaliass") String cdaliass);

  @Query("SELECT new sibbac.business.fase0.database.dto.LabelValueObject("
      + "CONCAT(ALI.id.cdaliass,'-(',ALI.descrali,')'), ALI.id.cdaliass) " + "FROM Tmct0ali ALI")
  public List<LabelValueObject> labelValueList();

  @Query(value = "SELECT COUNT(*) FROM BSNBPSQL.TMCT0ALI AS ALI JOIN BSNBPSQL.TMCT0EST AS EST ON ALI.CDALIASS = EST.CDALIASS "
      + "AND EST.FHFINAL>=TO_CHAR(CURRENT DATE,'YYYYMMDD') WHERE ALI.FHFINAL>=TO_CHAR(CURRENT DATE,'YYYYMMDD')"
      + "AND (EST.STLEV0LM, EST.STLEV1LM, EST.STLEV2LM, EST.STLEV3LM, EST.STLEV4LM) IN "
      + "(SELECT NUORDEN0, NUORDEN1, NUORDEN2, NUORDEN3, NUORDEN4 FROM BSNBPSQL.TMCT0ONI WHERE CDNIVEL4 <> 'BPL') "
      + "AND ALI.CDBROCLI= ?1", nativeQuery = true)
  public Object[] getCheckOperativa(String idCliente);

  @Query(value = "SELECT CDNIVEL4 FROM BSNBPSQL.TMCT0ONI WHERE NUORDEN4 IN (SELECT EST.STLEV4LM\r\n"
      + "FROM BSNBPSQL.TMCT0ALI AS ALI\r\n"
      + "JOIN BSNBPSQL.TMCT0EST AS EST ON ALI.CDALIASS = EST.CDALIASS AND EST.FHFINAL>=TO_CHAR(CURRENT DATE,'YYYYMMDD')\r\n"
      + "WHERE ALI.FHFINAL>=TO_CHAR(CURRENT DATE,'YYYYMMDD')\r\n"
      + "AND (EST.STLEV0LM, EST.STLEV1LM, EST.STLEV2LM, EST.STLEV3LM, EST.STLEV4LM) IN \r\n"
      + "(SELECT NUORDEN0, NUORDEN1, NUORDEN2, NUORDEN3, NUORDEN4 FROM BSNBPSQL.TMCT0ONI WHERE CDNIVEL4 <>'BPL')\r\n"
      + "AND ALI.CDBROCLI= ?1)", nativeQuery = true)
  public List<Object[]> getlistadoNivel4(String cdbrocli);

}
