package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;


@Embeddable
public class Tmct0cliId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -840738104362504593L;
	//private String				cdbrocli;
	//private BigDecimal			numsec;
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column( name = "IDCLIENT", nullable = false, unique = true )
	@XmlAttribute
	
	private Long				idClient;
	
	

	public Tmct0cliId() {
	}

	public Tmct0cliId( Long idClient ) {
		this.idClient = idClient;
		
	}

	

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0cliId ) )
			return false;
		Tmct0cliId castOther = ( Tmct0cliId ) other;

		return ( ( this.getIdClient().equals(castOther.getIdClient() )));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getIdClient() == null ? 0 : this.getIdClient().hashCode() );
		
		return result;
	}

	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

}
