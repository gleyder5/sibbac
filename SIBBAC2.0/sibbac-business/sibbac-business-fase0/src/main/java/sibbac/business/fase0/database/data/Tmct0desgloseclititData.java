package sibbac.business.fase0.database.data;


import java.math.BigDecimal;
import java.util.Date;


public class Tmct0desgloseclititData implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8721882628649563988L;
	private Long				nudesglose;
	private Integer				idestado;
	private String				nombreestado;
	private String				cdoperacion;
	private String				cdcamara;
	private char				cdsentido;
	private BigDecimal			imtitulos;
	private BigDecimal			imprecio;
	private BigDecimal			imefectivo;
	private String				cdisin;
	private Date				feejecuc;
	private String				nuorden;

	public Tmct0desgloseclititData() {
	}

	public Tmct0desgloseclititData( long nudesglose, Integer idestado, String nombreestado, String cdoperacion, String cdcamara,
			char cdsentido, BigDecimal imtitulos, BigDecimal imprecio, BigDecimal imefectivo, String cdisin, Date feejecuc, String nuorden ) {
		this.nudesglose = nudesglose;
		this.idestado = idestado;
		this.nombreestado = nombreestado;
		this.cdoperacion = cdoperacion;
		this.cdcamara = cdcamara;
		this.cdsentido = cdsentido;
		this.imtitulos = imtitulos;
		this.imprecio = imprecio;
		this.imefectivo = imefectivo;
		this.cdisin = cdisin;
		this.feejecuc = feejecuc;
		this.nuorden = nuorden;
	}

	public Tmct0desgloseclititData( Integer idestado, String nombreestado, String cdoperacion, String cdcamara, char cdsentido,
			BigDecimal imtitulos, BigDecimal imprecio, String cdisin, Date feejecuc, String nuorden ) {
		this.idestado = idestado;
		this.nombreestado = nombreestado;
		this.cdoperacion = cdoperacion;
		this.cdcamara = cdcamara;
		this.cdsentido = cdsentido;
		this.imtitulos = imtitulos;
		this.imprecio = imprecio;
		this.cdisin = cdisin;
		this.feejecuc = feejecuc;
		this.nuorden = nuorden;
	}

	public Long getNudesglose() {
		return nudesglose;
	}

	public void setNudesglose( Long nudesglose ) {
		this.nudesglose = nudesglose;
	}

	public Integer getIdestado() {
		return idestado;
	}

	public void setIdestado( Integer idestado ) {
		this.idestado = idestado;
	}

	public String getNombreestado() {
		return nombreestado;
	}

	public void setNombreestado( String nombreestado ) {
		this.nombreestado = nombreestado;
	}

	public String getCdoperacion() {
		return cdoperacion;
	}

	public void setCdoperacion( String cdoperacion ) {
		this.cdoperacion = cdoperacion;
	}

	public String getCdcamara() {
		return cdcamara;
	}

	public void setCdcamara( String cdcamara ) {
		this.cdcamara = cdcamara;
	}

	public char getCdsentido() {
		return cdsentido;
	}

	public void setCdsentido( char cdsentido ) {
		this.cdsentido = cdsentido;
	}

	public BigDecimal getImtitulos() {
		return imtitulos;
	}

	public void setImtitulos( BigDecimal imtitulos ) {
		this.imtitulos = imtitulos;
	}

	public BigDecimal getImprecio() {
		return imprecio;
	}

	public void setImprecio( BigDecimal imprecio ) {
		this.imprecio = imprecio;
	}

	public BigDecimal getImefectivo() {
		return imefectivo;
	}

	public void setImefectivo( BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	public String getCdisin() {
		return cdisin;
	}

	public void setCdisin( String cdisin ) {
		this.cdisin = cdisin;
	}

	public Date getFeejecuc() {
		return feejecuc;
	}

	public void setFeejecuc( Date feejecuc ) {
		this.feejecuc = feejecuc;
	}

	public String getNuorden() {
		return nuorden;
	}

	public void setNuorden( String nuorden ) {
		this.nuorden = nuorden;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
