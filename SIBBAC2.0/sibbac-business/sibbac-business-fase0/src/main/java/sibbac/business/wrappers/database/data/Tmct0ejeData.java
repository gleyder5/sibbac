package sibbac.business.wrappers.database.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeId;

public class Tmct0ejeData implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String							nuorden;	
	private String							nbooking;
	private String							nuejecuc;
	private BigDecimal						nutiteje;
	private String							dsobserv;
	private String							cdalias;
	private String							cdbroker;
	private Character						cdcaptur;
	private String							cdclente;
	private String							cdisin;
	private String							cdmercad;
	private String							cdmoniso;
	private String							cdordctp;
	private Character						cdtpoper;
	private String							cdvia;
	private Date							feejecuc;
	private Date							hoejecuc;
	private Date							fevalor;
	private BigDecimal						imcbmerc;
	private String							nuopemer;
	private String							nurefere;
	private String							nuagreje;
	private String							cdusucon;
	private Short							nupareje;
	private BigDecimal						pccambio;
	private String							tpopebol;
	private BigDecimal						pcefecup;
	private BigDecimal						pclimcam;
	private String							cdusuaud;
	private Date							fhaudit;
	private Integer							nuversion;
	private char							cuadreau;
	private BigDecimal						rfcupon;
	private BigDecimal						imnetrf;
	private String							numiberclear;
	private String							platliqu;
	private String							platneg;
	private String							ctacontrap;
	private String							operamer;
	private String							refextorden;
	private String							entpart;
	private String							refclteorden;
	private String							tpactivo;
	private Integer							nuejeaud;
	private Character						casepti;
	private Date							feordenmkt;
	private Date							hoordenmkt;
	private String							cdoperacion;
	private String							segmenttradingvenue;
	private String							clearingplatform;
	private Integer							asignadocomp;
	private String							nemotecnico;
	private String							ctacomp;
	private String							miembrocomp;
	private String							refadicional;
	private Date							feintmer;
	private String							refasignacion;
	private String							ccv;
	private String							participante;
	private String							ctaliquidacion;
	private String							liqtiemporeal;
	private String							liqparcial;
	private String							cdMiembroMkt;
	private String							executionTradingVenue;
	private Character						indCotizacion;
	private Character						noEccRealTimeInd;
	private Character						noEccPartialSettleInd;
	private Date							noEccSettleDay;
	private String							noEccEntity;
	private String							noEccSettleAccount;
	private String							noEccSettleClientCcv;
	private List< Tmct0ejecucionalocationData >	tmct0ejecucionalocations	= new ArrayList< Tmct0ejecucionalocationData >( 0 );
	
	public Tmct0ejeData (){
		
	}//public Tmct0ejeData (){
	
	public Tmct0ejeData(Tmct0eje entity){
		entityToData(entity, this);
	}//public Tmct0ejeData(Tmct0eje entity){
	
	
	
	public static Tmct0ejeData entityToData(Tmct0eje entity){
		Tmct0ejeData data = new Tmct0ejeData();
    	entityToData(entity, data);
    	return data;
    }//public static Tmct0ejeData entityToData(Tmct0eje entity){
	
	public static void entityToData(Tmct0eje entity, Tmct0ejeData data) {
    	data.setAsignadocomp(entity.getAsignadocomp());
    	data.setCasepti(entity.getCasepti());
    	data.setCcv(entity.getCcv());
    	data.setCdalias(entity.getCdalias());
    	data.setCdbroker(entity.getCdbroker());
    	data.setCdcaptur(entity.getCdcaptur());
    	data.setCdclente(entity.getCdclente());
    	data.setCdisin(entity.getCdisin());
    	data.setCdmercad(entity.getCdmercad());
    	data.setCdMiembroMkt(entity.getCdMiembroMkt());
    	data.setCdmoniso(entity.getCdmoniso());
    	data.setCdoperacion(entity.getCdoperacion());
    	data.setCdordctp(entity.getCdordctp());
    	data.setCdtpoper(entity.getCdtpoper());
    	data.setCdusuaud(entity.getCdusuaud());
    	data.setCdusucon(entity.getCdusuaud());
    	data.setCdvia(entity.getCdvia());
    	data.setClearingplatform(entity.getClearingplatform());
    	data.setCtacomp(entity.getCtacomp());
    	data.setCtacontrap(entity.getCtacontrap());
    	data.setCtaliquidacion(entity.getCtaliquidacion());
    	data.setCuadreau(entity.getCuadreau());
    	data.setDsobserv(entity.getDsobserv());
    	data.setEntpart(entity.getDsobserv());
    	data.setExecutionTradingVenue(entity.getExecutionTradingVenue());
    	data.setFeejecuc(entity.getFeejecuc());
    	data.setFeintmer(entity.getFeintmer());
    	data.setFeordenmkt(entity.getFeordenmkt());
    	data.setFevalor(entity.getFevalor());
    	data.setFhaudit(entity.getFhaudit());
    	data.setHoejecuc(entity.getHoejecuc());
    	data.setHoordenmkt(entity.getHoordenmkt());
    	data.setImcbmerc(entity.getImcbmerc());
    	data.setImnetrf(entity.getImnetrf());
    	data.setIndCotizacion(entity.getIndCotizacion());
    	data.setLiqparcial(entity.getLiqparcial());
    	data.setLiqtiemporeal(entity.getLiqtiemporeal());
    	data.setMiembrocomp(entity.getMiembrocomp());
    	data.setNbooking(entity.getId().getNbooking());
    	data.setNemotecnico(entity.getNemotecnico());
    	data.setNoEccEntity(entity.getNoEccEntity());
    	data.setNoEccPartialSettleInd(entity.getNoEccPartialSettleInd());
    	data.setNoEccRealTimeInd(entity.getNoEccRealTimeInd());
    	data.setNoEccSettleAccount(entity.getNoEccSettleAccount());
    	data.setNoEccSettleClientCcv(entity.getNoEccSettleClientCcv());
    	data.setNoEccSettleDay(entity.getNoEccSettleDay());
    	data.setNuagreje(entity.getNuagreje());
    	data.setNuejeaud(entity.getNuejeaud());
    	data.setNuejecuc(entity.getId().getNuejecuc());
    	data.setNumiberclear(entity.getNumiberclear());
    	data.setNuopemer(entity.getNuopemer());
    	data.setNuorden(entity.getId().getNuorden());
    	data.setNupareje(entity.getNupareje());
    	data.setNurefere(entity.getNurefere());
    	data.setNutiteje(entity.getNutiteje());
    	data.setNuversion(entity.getNuversion());
    	data.setOperamer(entity.getOperamer());
    	data.setParticipante(entity.getParticipante());
    	data.setPccambio(entity.getPccambio());
    	data.setPcefecup(entity.getPcefecup());
    	data.setPclimcam(entity.getPclimcam());
    	data.setPlatliqu(entity.getPlatliqu());
    	data.setPlatneg(entity.getPlatneg());
    	data.setRefadicional(entity.getRefadicional());
    	data.setRefasignacion(entity.getRefasignacion());
    	data.setRefclteorden(entity.getRefclteorden());
    	data.setRefextorden(entity.getRefextorden());
    	data.setRfcupon(entity.getRfcupon());
    	data.setSegmenttradingvenue(entity.getSegmenttradingvenue());
    	data.setTpactivo(entity.getTpactivo());
    	data.setTpopebol(entity.getTpopebol());
    }//public static void entityToData(Tmct0ejecucionalocation entity, Tmct0ejecucionalocationData data) {
	
	public static Tmct0eje dataToEntity(Tmct0ejeData data){
		Tmct0eje entity = new Tmct0eje();
		dataToEntity(data, entity);
    	return entity;
    }//public static Tmct0eje dataToEntity(Tmct0ejeData data){
	
	public static void dataToEntity(Tmct0ejeData data, Tmct0eje entity){
		entity.setAsignadocomp(data.getAsignadocomp());
		entity.setCasepti(data.getCasepti());
		entity.setCcv(data.getCcv());
		entity.setCdalias(data.getCdalias());
		entity.setCdbroker(data.getCdbroker());
		entity.setCdcaptur(data.getCdcaptur());
		entity.setCdclente(data.getCdclente());
		entity.setCdisin(data.getCdisin());
		entity.setCdmercad(data.getCdmercad());
		entity.setCdMiembroMkt(data.getCdMiembroMkt());
    	entity.setCdmoniso(data.getCdmoniso());
    	entity.setCdoperacion(data.getCdoperacion());
    	entity.setCdordctp(data.getCdordctp());
    	entity.setCdtpoper(data.getCdtpoper());
    	entity.setCdusuaud(data.getCdusuaud());
    	entity.setCdusucon(data.getCdusuaud());
    	entity.setCdvia(data.getCdvia());
    	entity.setClearingplatform(data.getClearingplatform());
    	entity.setCtacomp(data.getCtacomp());
    	entity.setCtacontrap(data.getCtacontrap());
    	entity.setCtaliquidacion(data.getCtaliquidacion());
    	entity.setCuadreau(data.getCuadreau());
    	entity.setDsobserv(data.getDsobserv());
    	entity.setEntpart(data.getDsobserv());
    	entity.setExecutionTradingVenue(data.getExecutionTradingVenue());
    	entity.setFeejecuc(data.getFeejecuc());
    	entity.setFeintmer(data.getFeintmer());
    	entity.setFeordenmkt(data.getFeordenmkt());
    	entity.setFevalor(data.getFevalor());
    	entity.setFhaudit(data.getFhaudit());
    	entity.setHoejecuc(data.getHoejecuc());
    	entity.setHoordenmkt(data.getHoordenmkt());
    	entity.setImcbmerc(data.getImcbmerc());
    	entity.setImnetrf(data.getImnetrf());
    	entity.setIndCotizacion(data.getIndCotizacion());
    	entity.setLiqparcial(data.getLiqparcial());
    	entity.setLiqtiemporeal(data.getLiqtiemporeal());
    	entity.setMiembrocomp(data.getMiembrocomp());
    	if (entity.getId() == null)
    	{
    		entity.setId(new Tmct0ejeId());
    	}
    	entity.getId().setNbooking(data.getNbooking());
    	entity.setNemotecnico(data.getNemotecnico());
    	entity.setNoEccEntity(data.getNoEccEntity());
    	entity.setNoEccPartialSettleInd(data.getNoEccPartialSettleInd());
    	entity.setNoEccRealTimeInd(data.getNoEccRealTimeInd());
    	entity.setNoEccSettleAccount(data.getNoEccSettleAccount());
    	entity.setNoEccSettleClientCcv(data.getNoEccSettleClientCcv());
    	entity.setNoEccSettleDay(data.getNoEccSettleDay());
    	entity.setNuagreje(data.getNuagreje());
    	entity.setNuejeaud(data.getNuejeaud());
    	entity.getId().setNuejecuc(data.getNuejecuc());
    	entity.setNumiberclear(data.getNumiberclear());
    	entity.setNuopemer(data.getNuopemer());
    	entity.getId().setNuorden(data.getNuorden());
    	entity.setNupareje(data.getNupareje());
    	entity.setNurefere(data.getNurefere());
    	entity.setNutiteje(data.getNutiteje());
    	entity.setNuversion(data.getNuversion());
    	entity.setOperamer(data.getOperamer());
    	entity.setParticipante(data.getParticipante());
    	entity.setPccambio(data.getPccambio());
    	entity.setPcefecup(data.getPcefecup());
    	entity.setPclimcam(data.getPclimcam());
    	entity.setPlatliqu(data.getPlatliqu());
    	entity.setPlatneg(data.getPlatneg());
    	entity.setRefadicional(data.getRefadicional());
    	entity.setRefasignacion(data.getRefasignacion());
    	entity.setRefclteorden(data.getRefclteorden());
    	entity.setRefextorden(data.getRefextorden());
    	entity.setRfcupon(data.getRfcupon());
    	entity.setSegmenttradingvenue(data.getSegmenttradingvenue());
    	entity.setTpactivo(data.getTpactivo());
    	entity.setTpopebol(data.getTpopebol());
    }//public static void dataToEntity(Tmct0ejeData data, Tmct0eje entity){
	
	public String getNuorden() {
		return nuorden;
	}
	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}
	public String getNbooking() {
		return nbooking;
	}
	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}
	public String getNuejecuc() {
		return nuejecuc;
	}
	public void setNuejecuc(String nuejecuc) {
		this.nuejecuc = nuejecuc;
	}
	public BigDecimal getNutiteje() {
		return nutiteje;
	}
	public void setNutiteje(BigDecimal nutiteje) {
		this.nutiteje = nutiteje;
	}
	public String getDsobserv() {
		return dsobserv;
	}
	public void setDsobserv(String dsobserv) {
		this.dsobserv = dsobserv;
	}
	public String getCdalias() {
		return cdalias;
	}
	public void setCdalias(String cdalias) {
		this.cdalias = cdalias;
	}
	public String getCdbroker() {
		return cdbroker;
	}
	public void setCdbroker(String cdbroker) {
		this.cdbroker = cdbroker;
	}
	public Character getCdcaptur() {
		return cdcaptur;
	}
	public void setCdcaptur(Character cdcaptur) {
		this.cdcaptur = cdcaptur;
	}
	public String getCdclente() {
		return cdclente;
	}
	public void setCdclente(String cdclente) {
		this.cdclente = cdclente;
	}
	public String getCdisin() {
		return cdisin;
	}
	public void setCdisin(String cdisin) {
		this.cdisin = cdisin;
	}
	public String getCdmercad() {
		return cdmercad;
	}
	public void setCdmercad(String cdmercad) {
		this.cdmercad = cdmercad;
	}
	public String getCdmoniso() {
		return cdmoniso;
	}
	public void setCdmoniso(String cdmoniso) {
		this.cdmoniso = cdmoniso;
	}
	public String getCdordctp() {
		return cdordctp;
	}
	public void setCdordctp(String cdordctp) {
		this.cdordctp = cdordctp;
	}
	public Character getCdtpoper() {
		return cdtpoper;
	}
	public void setCdtpoper(Character cdtpoper) {
		this.cdtpoper = cdtpoper;
	}
	public String getCdvia() {
		return cdvia;
	}
	public void setCdvia(String cdvia) {
		this.cdvia = cdvia;
	}
	public Date getFeejecuc() {
		return feejecuc;
	}
	public void setFeejecuc(Date feejecuc) {
		this.feejecuc = feejecuc;
	}
	public Date getHoejecuc() {
		return hoejecuc;
	}
	public void setHoejecuc(Date hoejecuc) {
		this.hoejecuc = hoejecuc;
	}
	public Date getFevalor() {
		return fevalor;
	}
	public void setFevalor(Date fevalor) {
		this.fevalor = fevalor;
	}
	public BigDecimal getImcbmerc() {
		return imcbmerc;
	}
	public void setImcbmerc(BigDecimal imcbmerc) {
		this.imcbmerc = imcbmerc;
	}
	public String getNuopemer() {
		return nuopemer;
	}
	public void setNuopemer(String nuopemer) {
		this.nuopemer = nuopemer;
	}
	public String getNurefere() {
		return nurefere;
	}
	public void setNurefere(String nurefere) {
		this.nurefere = nurefere;
	}
	public String getNuagreje() {
		return nuagreje;
	}
	public void setNuagreje(String nuagreje) {
		this.nuagreje = nuagreje;
	}
	public String getCdusucon() {
		return cdusucon;
	}
	public void setCdusucon(String cdusucon) {
		this.cdusucon = cdusucon;
	}
	public Short getNupareje() {
		return nupareje;
	}
	public void setNupareje(Short nupareje) {
		this.nupareje = nupareje;
	}
	public BigDecimal getPccambio() {
		return pccambio;
	}
	public void setPccambio(BigDecimal pccambio) {
		this.pccambio = pccambio;
	}
	public String getTpopebol() {
		return tpopebol;
	}
	public void setTpopebol(String tpopebol) {
		this.tpopebol = tpopebol;
	}
	public BigDecimal getPcefecup() {
		return pcefecup;
	}
	public void setPcefecup(BigDecimal pcefecup) {
		this.pcefecup = pcefecup;
	}
	public BigDecimal getPclimcam() {
		return pclimcam;
	}
	public void setPclimcam(BigDecimal pclimcam) {
		this.pclimcam = pclimcam;
	}
	public String getCdusuaud() {
		return cdusuaud;
	}
	public void setCdusuaud(String cdusuaud) {
		this.cdusuaud = cdusuaud;
	}
	public Date getFhaudit() {
		return fhaudit;
	}
	public void setFhaudit(Date fhaudit) {
		this.fhaudit = fhaudit;
	}
	public Integer getNuversion() {
		return nuversion;
	}
	public void setNuversion(Integer nuversion) {
		this.nuversion = nuversion;
	}
	public char getCuadreau() {
		return cuadreau;
	}
	public void setCuadreau(char cuadreau) {
		this.cuadreau = cuadreau;
	}
	public BigDecimal getRfcupon() {
		return rfcupon;
	}
	public void setRfcupon(BigDecimal rfcupon) {
		this.rfcupon = rfcupon;
	}
	public BigDecimal getImnetrf() {
		return imnetrf;
	}
	public void setImnetrf(BigDecimal imnetrf) {
		this.imnetrf = imnetrf;
	}
	public String getNumiberclear() {
		return numiberclear;
	}
	public void setNumiberclear(String numiberclear) {
		this.numiberclear = numiberclear;
	}
	public String getPlatliqu() {
		return platliqu;
	}
	public void setPlatliqu(String platliqu) {
		this.platliqu = platliqu;
	}
	public String getPlatneg() {
		return platneg;
	}
	public void setPlatneg(String platneg) {
		this.platneg = platneg;
	}
	public String getCtacontrap() {
		return ctacontrap;
	}
	public void setCtacontrap(String ctacontrap) {
		this.ctacontrap = ctacontrap;
	}
	public String getOperamer() {
		return operamer;
	}
	public void setOperamer(String operamer) {
		this.operamer = operamer;
	}
	public String getRefextorden() {
		return refextorden;
	}
	public void setRefextorden(String refextorden) {
		this.refextorden = refextorden;
	}
	public String getEntpart() {
		return entpart;
	}
	public void setEntpart(String entpart) {
		this.entpart = entpart;
	}
	public String getRefclteorden() {
		return refclteorden;
	}
	public void setRefclteorden(String refclteorden) {
		this.refclteorden = refclteorden;
	}
	public String getTpactivo() {
		return tpactivo;
	}
	public void setTpactivo(String tpactivo) {
		this.tpactivo = tpactivo;
	}
	public Integer getNuejeaud() {
		return nuejeaud;
	}
	public void setNuejeaud(Integer nuejeaud) {
		this.nuejeaud = nuejeaud;
	}
	public Character getCasepti() {
		return casepti;
	}
	public void setCasepti(Character casepti) {
		this.casepti = casepti;
	}
	public Date getFeordenmkt() {
		return feordenmkt;
	}
	public void setFeordenmkt(Date feordenmkt) {
		this.feordenmkt = feordenmkt;
	}
	public Date getHoordenmkt() {
		return hoordenmkt;
	}
	public void setHoordenmkt(Date hoordenmkt) {
		this.hoordenmkt = hoordenmkt;
	}
	public String getCdoperacion() {
		return cdoperacion;
	}
	public void setCdoperacion(String cdoperacion) {
		this.cdoperacion = cdoperacion;
	}
	public String getSegmenttradingvenue() {
		return segmenttradingvenue;
	}
	public void setSegmenttradingvenue(String segmenttradingvenue) {
		this.segmenttradingvenue = segmenttradingvenue;
	}
	public String getClearingplatform() {
		return clearingplatform;
	}
	public void setClearingplatform(String clearingplatform) {
		this.clearingplatform = clearingplatform;
	}
	public Integer getAsignadocomp() {
		return asignadocomp;
	}
	public void setAsignadocomp(Integer asignadocomp) {
		this.asignadocomp = asignadocomp;
	}
	public String getNemotecnico() {
		return nemotecnico;
	}
	public void setNemotecnico(String nemotecnico) {
		this.nemotecnico = nemotecnico;
	}
	public String getCtacomp() {
		return ctacomp;
	}
	public void setCtacomp(String ctacomp) {
		this.ctacomp = ctacomp;
	}
	public String getMiembrocomp() {
		return miembrocomp;
	}
	public void setMiembrocomp(String miembrocomp) {
		this.miembrocomp = miembrocomp;
	}
	public String getRefadicional() {
		return refadicional;
	}
	public void setRefadicional(String refadicional) {
		this.refadicional = refadicional;
	}
	public Date getFeintmer() {
		return feintmer;
	}
	public void setFeintmer(Date feintmer) {
		this.feintmer = feintmer;
	}
	public String getRefasignacion() {
		return refasignacion;
	}
	public void setRefasignacion(String refasignacion) {
		this.refasignacion = refasignacion;
	}
	public String getCcv() {
		return ccv;
	}
	public void setCcv(String ccv) {
		this.ccv = ccv;
	}
	public String getParticipante() {
		return participante;
	}
	public void setParticipante(String participante) {
		this.participante = participante;
	}
	public String getCtaliquidacion() {
		return ctaliquidacion;
	}
	public void setCtaliquidacion(String ctaliquidacion) {
		this.ctaliquidacion = ctaliquidacion;
	}
	public String getLiqtiemporeal() {
		return liqtiemporeal;
	}
	public void setLiqtiemporeal(String liqtiemporeal) {
		this.liqtiemporeal = liqtiemporeal;
	}
	public String getLiqparcial() {
		return liqparcial;
	}
	public void setLiqparcial(String liqparcial) {
		this.liqparcial = liqparcial;
	}
	public String getCdMiembroMkt() {
		return cdMiembroMkt;
	}
	public void setCdMiembroMkt(String cdMiembroMkt) {
		this.cdMiembroMkt = cdMiembroMkt;
	}
	public String getExecutionTradingVenue() {
		return executionTradingVenue;
	}
	public void setExecutionTradingVenue(String executionTradingVenue) {
		this.executionTradingVenue = executionTradingVenue;
	}
	public Character getIndCotizacion() {
		return indCotizacion;
	}
	public void setIndCotizacion(Character indCotizacion) {
		this.indCotizacion = indCotizacion;
	}
	public Character getNoEccRealTimeInd() {
		return noEccRealTimeInd;
	}
	public void setNoEccRealTimeInd(Character noEccRealTimeInd) {
		this.noEccRealTimeInd = noEccRealTimeInd;
	}
	public Character getNoEccPartialSettleInd() {
		return noEccPartialSettleInd;
	}
	public void setNoEccPartialSettleInd(Character noEccPartialSettleInd) {
		this.noEccPartialSettleInd = noEccPartialSettleInd;
	}
	public Date getNoEccSettleDay() {
		return noEccSettleDay;
	}
	public void setNoEccSettleDay(Date noEccSettleDay) {
		this.noEccSettleDay = noEccSettleDay;
	}
	public String getNoEccEntity() {
		return noEccEntity;
	}
	public void setNoEccEntity(String noEccEntity) {
		this.noEccEntity = noEccEntity;
	}
	public String getNoEccSettleAccount() {
		return noEccSettleAccount;
	}
	public void setNoEccSettleAccount(String noEccSettleAccount) {
		this.noEccSettleAccount = noEccSettleAccount;
	}
	public String getNoEccSettleClientCcv() {
		return noEccSettleClientCcv;
	}
	public void setNoEccSettleClientCcv(String noEccSettleClientCcv) {
		this.noEccSettleClientCcv = noEccSettleClientCcv;
	}
	public List<Tmct0ejecucionalocationData> getTmct0ejecucionalocations() {
		return tmct0ejecucionalocations;
	}
	public void setTmct0ejecucionalocations(
			List<Tmct0ejecucionalocationData> tmct0ejecucionalocations) {
		this.tmct0ejecucionalocations = tmct0ejecucionalocations;
	}

	
	
}//public class Tmct0ejeData implements java.io.Serializable {
