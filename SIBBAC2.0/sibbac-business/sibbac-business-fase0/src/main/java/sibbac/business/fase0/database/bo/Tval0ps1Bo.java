package sibbac.business.fase0.database.bo;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnvasql.dao.Tval0ps1Dao;
import sibbac.database.bsnvasql.model.Tval0ps1;
import sibbac.database.bsnvasql.model.Tval0ps1Id;
import sibbac.database.bsnvasql.model.Tval0ps2;


@Service
public class Tval0ps1Bo extends AbstractBo< Tval0ps1, Tval0ps1Id, Tval0ps1Dao > {

	public Tval0ps1 findByCdhppais( String cdhppais ) {
		return dao.findOne( new Tval0ps1Id( cdhppais, 0 ) );
	}

	public List< Tval0ps1 > findAllActiveOrderByCdiso2po() {
		return dao.findAllActiveOrderByCdiso2po();
	}

	public Tval0ps1 findActiveByCdiso2po( String cdiso2po ) {
		return dao.findActiveByCdiso2po( cdiso2po );
	}

	/**
	 * Obtiene un pais en una entidad Tval0ps1 recibiendo una Tval0ps2 por medio del codigo de 2 posiciones
	 * 
	 * @param tval0ps2
	 * @return Devuelve el pais en Tval0ps1 si puede encontrarlo o null si no puede
	 */
	public Tval0ps1 getTval0ps1FromTval0ps2( Tval0ps2 tval0ps2 ) {
		try {
			return dao.findActiveByCdiso2po( tval0ps2.getCdiso2po() );
		} catch ( Exception e ) {
			return null;
		}
	}

	public List< String > findAllCodigosActivos() {
		List<String> codigos = new ArrayList< String >();
		try{
			List<Object> resultados =  this.dao.findAllCodigosActivos();
			for(Object resultado:resultados){
				codigos.add( (String) resultado );
			}
		}catch(Exception e){
			LOG.error( "Tval0ps1Bo -- Error creando lista de códigos de países "+e.getMessage() );
		}
		return codigos;
	}

}
