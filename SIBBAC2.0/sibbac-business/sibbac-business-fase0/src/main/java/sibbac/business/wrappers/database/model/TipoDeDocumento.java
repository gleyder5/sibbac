package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.database.model.TipoDeDocumentoBo }.
 * Entity: "TiposDocumentos".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table( name = WRAPPERS.TIPO_DOCUMENTO )
@Entity
@XmlRootElement
@Audited
public class TipoDeDocumento extends ATableAudited< TipoDeDocumento > implements java.io.Serializable {

	// ------------------------------------------ Static Bean properties

	public static final String	FACTURA				= "FACTURA";
	public static final String	RECTIFICATIVA		= "RECTIFICATIVA";
	public static final String	COBRO				= "COBRO";

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8087448721427512053L;

	@Column( name = "NBDESCRIPCION", nullable = false, length = 50 )
	@XmlAttribute
	private String				descripcion;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The {@link sibbac.database.model.facturacion.DocumentoTipo documentotipo} 's default constructor.
	 */
	public TipoDeDocumento() {
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion( final String descripcion ) {
		this.descripcion = descripcion;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

}
