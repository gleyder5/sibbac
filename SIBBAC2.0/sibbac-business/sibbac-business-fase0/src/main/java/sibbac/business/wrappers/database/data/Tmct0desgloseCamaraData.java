package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.Tmct0alcId;


public class Tmct0desgloseCamaraData {

	private long		idDesgloceCamara;
	private BigDecimal	efectivo				= BigDecimal.ZERO;
	private String		isin;
	private long		idCuentaLiq;
	private String		codCtaDeCompensacion;
	private Tmct0alcId	idAlc;
	private BigDecimal	imcanoncompeu			= BigDecimal.ZERO;
	private BigDecimal	imcanoncontreu			= BigDecimal.ZERO;
	private BigDecimal	imcanonliqeu			= BigDecimal.ZERO;
	private BigDecimal	totalCanones			= BigDecimal.ZERO;
	private BigDecimal	sumaCanonesYEfectivos	= BigDecimal.ZERO;
	private BigDecimal	corretaje				= BigDecimal.ZERO;
	private BigDecimal	totalCorretajeYEfectivo	= BigDecimal.ZERO;

	public Tmct0desgloseCamaraData( long idDesgloceCamara, BigDecimal efectivo, String isin, String codCtaDeCompensacion, Tmct0alcId idAlc,
			BigDecimal imcanoncompeu, BigDecimal imcanoncontreu, BigDecimal imcanonliqeu, BigDecimal corretaje ) {
		this.idDesgloceCamara = idDesgloceCamara;
		this.efectivo = efectivo;
		this.isin = isin;
		this.codCtaDeCompensacion = codCtaDeCompensacion;
		this.idAlc = idAlc;
		this.imcanoncompeu = imcanoncompeu;
		this.imcanoncontreu = imcanoncontreu;
		this.imcanonliqeu = imcanonliqeu;
		this.corretaje = corretaje;
	}

	public long getIdDesgloceCamara() {
		return idDesgloceCamara;
	}

	public void setIdDesgloceCamara( long idDesgloceCamara ) {
		this.idDesgloceCamara = idDesgloceCamara;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public long getIdCuentaLiq() {
		return idCuentaLiq;
	}

	public void setIdCuentaLiq( long idCuentaLiq ) {
		this.idCuentaLiq = idCuentaLiq;
	}

	public String getCodCtaDeCompensacion() {
		return codCtaDeCompensacion;
	}

	public void setCodCtaDeCompensacion( String codCtaDeCompensacion ) {
		this.codCtaDeCompensacion = codCtaDeCompensacion;
	}

	public BigDecimal getImcanoncompeu() {
		return imcanoncompeu;
	}

	public void setImcanoncompeu( BigDecimal imcanoncompeu ) {
		this.imcanoncompeu = imcanoncompeu;
	}

	public BigDecimal getImcanoncontreu() {
		return imcanoncontreu;
	}

	public void setImcanoncontreu( BigDecimal imcanoncontreu ) {
		this.imcanoncontreu = imcanoncontreu;
	}

	public BigDecimal getImcanonliqeu() {
		return imcanonliqeu;
	}

	public void setImcanonliqeu( BigDecimal imcanonliqeu ) {
		this.imcanonliqeu = imcanonliqeu;
	}

	public Tmct0alcId getIdAlc() {
		return idAlc;
	}

	public void setIdAlc( Tmct0alcId idAlc ) {
		this.idAlc = idAlc;
	}

	public BigDecimal getTotalCanones() {
		return totalCanones;
	}

	public void setTotalCanones( BigDecimal totalCanones ) {
		this.totalCanones = totalCanones;
	}

	public BigDecimal getSumaCanonesYEfectivos() {
		return sumaCanonesYEfectivos;
	}

	public void setSumaCanonesYEfectivos( BigDecimal sumaCanonesYEfectivos ) {
		this.sumaCanonesYEfectivos = sumaCanonesYEfectivos;
	}

	public BigDecimal getCorretaje() {
		return corretaje;
	}

	public void setCorretaje( BigDecimal corretaje ) {
		this.corretaje = corretaje;
	}

	public BigDecimal getTotalCorretajeYEfectivo() {
		return totalCorretajeYEfectivo;
	}

	public void setTotalCorretajeYEfectivo( BigDecimal totalCorretajeYEfectivo ) {
		this.totalCorretajeYEfectivo = totalCorretajeYEfectivo;
	}

}
