package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Texto }. Entity:
 * "Texto".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = WRAPPERS.TEXTO )
@Entity
@XmlRootElement
@Audited
public class Texto extends ATableAudited< Texto > implements java.io.Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 *
	 */
	private static final long	serialVersionUID	= -358840871265186997L;

	@Column( name = "NBDESCRIPCIONES", nullable = false, length = 200 )
	@XmlAttribute
	private String				descripcion;

	@Column( name = "TIPO", nullable = false, length = 60 )
	@XmlAttribute
	private String				tipo;

	// ----------------------------------------------- Bean
	// Constructors---------------------

	public Texto() {
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo( String tipo ) {
		this.tipo = tipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		final ToStringBuilder tsb = new ToStringBuilder( ToStringStyle.SHORT_PREFIX_STYLE );
		tsb.append( getId() );
		tsb.append( getDescripcion() );
		tsb.append( getTipo() );
		return tsb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.appendSuper( super.hashCode() );
		hcb.append( getTipo() );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object object ) {
		final Texto serviceTextoBean = ( Texto ) object;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.appendSuper( super.equals( object ) );
		eqb.append( getTipo(), serviceTextoBean.getTipo() );
		return eqb.isEquals();
	}
}
