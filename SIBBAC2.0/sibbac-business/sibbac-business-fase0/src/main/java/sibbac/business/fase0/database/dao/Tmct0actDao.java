package sibbac.business.fase0.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0actId;

@Repository
public interface Tmct0actDao extends JpaRepository<Tmct0act, Tmct0actId> {

  @Query("SELECT act FROM Tmct0act act, Tmct0ali ali "
      + "WHERE act.id.cdbrocli = ali.id.cdbrocli "
      + "AND act.id.cdaliass = ali.id.cdaliass "
      + "AND act.fhfinal = :fhfinal "
      + "AND ali.fhfinal = :fhfinal")
  public List<Tmct0act> findAliasSubuentasActivos(@Param("fhfinal") final Integer fhfinal);

  @Query("select act from Tmct0act act where act.id.cdaliass = :alias and act.id.cdsubcta = :subCta and act.fhfinal = :fhfinal")
  public List<Tmct0act> findByCdaliassAndCdsubcta(@Param("alias") final String alias,
      @Param("subCta") final String subCta, @Param("fhfinal") final Integer fhfinal);

  @Query("select act from Tmct0act act where act.id.cdaliass = :alias and act.id.cdsubcta = :subCta and act.fhfinal >= :fecha and act.fhinicio <= :fecha ")
  public List<Tmct0act> findByCdaliassAndCdsubctaAndFecha(@Param("alias") final String alias,
      @Param("subCta") final String subCta, @Param("fecha") final Integer fecha);

  @Query("select act from Tmct0act act, Tmct0ali ali "
      + "where act.id.cdbrocli = ali.id.cdbrocli and act.id.cdaliass = ali.id.cdaliass "
      + "and act.idRegla = :Id_regla")
  public List<Tmct0act> findSubctaById_regla(@Param("Id_regla") final Long Id_regla);

  @Query("select act from Tmct0act act where act.id.cdaliass = :alias and act.id.cdsubcta = :subCta")
  public Tmct0act findByCdaliassAndCdsubcta2(@Param("alias") final String alias, @Param("subCta") final String subCta);

}
