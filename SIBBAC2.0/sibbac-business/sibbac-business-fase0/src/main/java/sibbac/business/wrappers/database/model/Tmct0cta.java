package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0CTA")
public class Tmct0cta implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 4637250251908195860L;
  private Tmct0ctaId id;
  private Tmct0ctg tmct0ctg;
  private Tmct0sta tmct0sta;
  private String cdalias;
  private String descrali;
  private BigDecimal imcammed;
  private BigDecimal imcamnet;
  private BigDecimal imtotbru;
  private BigDecimal eutotbru;
  private BigDecimal imtotnet;
  private BigDecimal eutotnet;
  private BigDecimal imnetbrk;
  private BigDecimal eunetbrk;
  private BigDecimal imcomisn;
  private BigDecimal imcomieu;
  private BigDecimal imcomdev;
  private BigDecimal eucomdev;
  private BigDecimal imfibrdv;
  private BigDecimal imfibreu;
  private BigDecimal imsvb;
  private BigDecimal imsvbeu;
  private BigDecimal imbrmerc;
  private BigDecimal imbrmreu;
  private BigDecimal imgastos;
  private BigDecimal imgatdvs;
  private BigDecimal imimpdvs;
  private BigDecimal imimpeur;
  private BigDecimal imcconeu;
  private BigDecimal imcliqeu;
  private BigDecimal imdereeu;
  private BigDecimal nutitagr;
  private BigDecimal imnetobr;
  private BigDecimal podevolu;
  private BigDecimal potaxbrk;
  private BigDecimal pccombrk;
  private String cdcleare;
  private String acctatcle;
  private String tysetcle;
  private String cdbencle;
  private String acbencle;
  private String idbencle;
  private String gclecode;
  private String gcleacct;
  private String gcleid;
  private String lclecode;
  private String lcleacct;
  private String lcleid;
  private String pccleset;
  private Character sendbecle;
  private String tpsetcle;
  private String cdcustod;
  private String acctatcus;
  private String tysetcus;
  private String cdbencus;
  private String acbencus;
  private String idbencus;
  private String gcuscode;
  private String gcusacct;
  private String gcusid;
  private String lcuscode;
  private String lcusacct;
  private String lcusid;
  private String pccusset;
  private Character sendbecus;
  private String tpsetcus;
  private String cdentliq;
  private String cdrefban;
  private String nbtitula;
  private Date fevalor;
  private Date fevalorr;
  private Date fetrade;
  private String cdstcexc;
  private String cdmoniso;
  private BigDecimal imcamalo;
  private BigDecimal immarpri;
  private BigDecimal imclecha;
  private Integer pkbroker1;
  private String pkentity;
  private Character cdtpoper;
  private String cdisin;
  private String nbvalors;
  private Integer nuoprout;
  private Short nupareje;
  private Character inforcon;
  private Character insenmod;
  private Character insenfla;
  private Date fhsending;
  private Date hosending;
  private Character indatfis;
  private String erdatfis;
  private Character cddatliq;
  private Character cdbloque;
  private String dsobserv;
  private String cdusublo;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuversion;
  private BigDecimal imgasliqdv;
  private BigDecimal imgasliqeu;
  private BigDecimal imgasgesdv;
  private BigDecimal imgasgeseu;
  private BigDecimal imgastotdv;
  private BigDecimal imgastoteu;
  private Character cdindgas;

  private List<Tmct0ctd> tmct0ctds = new ArrayList<Tmct0ctd>(0);

  // private Set<Tmct0cte> tmct0ctes = new HashSet<Tmct0cte>(0);

  public Tmct0cta() {
  }

  public Tmct0cta(Tmct0ctaId id, Tmct0ctg tmct0ctg, Tmct0sta tmct0sta, String pkentity, String erdatfis) {
    this.id = id;
    this.tmct0ctg = tmct0ctg;
    this.tmct0sta = tmct0sta;
    this.pkentity = pkentity;
    this.erdatfis = erdatfis;
  }

  public Tmct0cta(Tmct0ctaId id, Tmct0ctg tmct0ctg, Tmct0sta tmct0sta, String cdalias, String descrali,
      BigDecimal imcammed, BigDecimal imcamnet, BigDecimal imtotbru, BigDecimal eutotbru, BigDecimal imtotnet,
      BigDecimal eutotnet, BigDecimal imnetbrk, BigDecimal eunetbrk, BigDecimal imcomisn, BigDecimal imcomieu,
      BigDecimal imcomdev, BigDecimal eucomdev, BigDecimal imfibrdv, BigDecimal imfibreu, BigDecimal imsvb,
      BigDecimal imsvbeu, BigDecimal imbrmerc, BigDecimal imbrmreu, BigDecimal imgastos, BigDecimal imgatdvs,
      BigDecimal imimpdvs, BigDecimal imimpeur, BigDecimal imcconeu, BigDecimal imcliqeu, BigDecimal imdereeu,
      BigDecimal nutitagr, BigDecimal imnetobr, BigDecimal podevolu, BigDecimal potaxbrk, BigDecimal pccombrk,
      String cdcleare, String acctatcle, String tysetcle, String cdbencle, String acbencle, String idbencle,
      String gclecode, String gcleacct, String gcleid, String lclecode, String lcleacct, String lcleid,
      String pccleset, Character sendbecle, String tpsetcle, String cdcustod, String acctatcus, String tysetcus,
      String cdbencus, String acbencus, String idbencus, String gcuscode, String gcusacct, String gcusid,
      String lcuscode, String lcusacct, String lcusid, String pccusset, Character sendbecus, String tpsetcus,
      String cdentliq, String cdrefban, String nbtitula, Date fevalor, Date fevalorr, Date fetrade, String cdstcexc,
      String cdmoniso, BigDecimal imcamalo, BigDecimal immarpri, BigDecimal imclecha, Integer pkbroker1,
      String pkentity, Character cdtpoper, String cdisin, String nbvalors, Integer nuoprout, Short nupareje,
      Character inforcon, Character insenmod, Character insenfla, Date fhsending, Date hosending, Character indatfis,
      String erdatfis, Character cddatliq, Character cdbloque, String dsobserv, String cdusublo, String cdusuaud,
      Date fhaudit, Integer nuversion, BigDecimal imgasliqdv, BigDecimal imgasliqeu, BigDecimal imgasgesdv,
      BigDecimal imgasgeseu, BigDecimal imgastotdv, BigDecimal imgastoteu, Character cdindgas/*
                                                                                              * ,
                                                                                              * Set
                                                                                              * <
                                                                                              * Tmct0ctd
                                                                                              * >
                                                                                              * tmct0ctds
                                                                                              * ,
                                                                                              * Set
                                                                                              * <
                                                                                              * Tmct0cte
                                                                                              * >
                                                                                              * tmct0ctes
                                                                                              */) {
    this.id = id;
    this.tmct0ctg = tmct0ctg;
    this.tmct0sta = tmct0sta;
    this.cdalias = cdalias;
    this.descrali = descrali;
    this.imcammed = imcammed;
    this.imcamnet = imcamnet;
    this.imtotbru = imtotbru;
    this.eutotbru = eutotbru;
    this.imtotnet = imtotnet;
    this.eutotnet = eutotnet;
    this.imnetbrk = imnetbrk;
    this.eunetbrk = eunetbrk;
    this.imcomisn = imcomisn;
    this.imcomieu = imcomieu;
    this.imcomdev = imcomdev;
    this.eucomdev = eucomdev;
    this.imfibrdv = imfibrdv;
    this.imfibreu = imfibreu;
    this.imsvb = imsvb;
    this.imsvbeu = imsvbeu;
    this.imbrmerc = imbrmerc;
    this.imbrmreu = imbrmreu;
    this.imgastos = imgastos;
    this.imgatdvs = imgatdvs;
    this.imimpdvs = imimpdvs;
    this.imimpeur = imimpeur;
    this.imcconeu = imcconeu;
    this.imcliqeu = imcliqeu;
    this.imdereeu = imdereeu;
    this.nutitagr = nutitagr;
    this.imnetobr = imnetobr;
    this.podevolu = podevolu;
    this.potaxbrk = potaxbrk;
    this.pccombrk = pccombrk;
    this.cdcleare = cdcleare;
    this.acctatcle = acctatcle;
    this.tysetcle = tysetcle;
    this.cdbencle = cdbencle;
    this.acbencle = acbencle;
    this.idbencle = idbencle;
    this.gclecode = gclecode;
    this.gcleacct = gcleacct;
    this.gcleid = gcleid;
    this.lclecode = lclecode;
    this.lcleacct = lcleacct;
    this.lcleid = lcleid;
    this.pccleset = pccleset;
    this.sendbecle = sendbecle;
    this.tpsetcle = tpsetcle;
    this.cdcustod = cdcustod;
    this.acctatcus = acctatcus;
    this.tysetcus = tysetcus;
    this.cdbencus = cdbencus;
    this.acbencus = acbencus;
    this.idbencus = idbencus;
    this.gcuscode = gcuscode;
    this.gcusacct = gcusacct;
    this.gcusid = gcusid;
    this.lcuscode = lcuscode;
    this.lcusacct = lcusacct;
    this.lcusid = lcusid;
    this.pccusset = pccusset;
    this.sendbecus = sendbecus;
    this.tpsetcus = tpsetcus;
    this.cdentliq = cdentliq;
    this.cdrefban = cdrefban;
    this.nbtitula = nbtitula;
    this.fevalor = fevalor;
    this.fevalorr = fevalorr;
    this.fetrade = fetrade;
    this.cdstcexc = cdstcexc;
    this.cdmoniso = cdmoniso;
    this.imcamalo = imcamalo;
    this.immarpri = immarpri;
    this.imclecha = imclecha;
    this.pkbroker1 = pkbroker1;
    this.pkentity = pkentity;
    this.cdtpoper = cdtpoper;
    this.cdisin = cdisin;
    this.nbvalors = nbvalors;
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
    this.inforcon = inforcon;
    this.insenmod = insenmod;
    this.insenfla = insenfla;
    this.fhsending = fhsending;
    this.hosending = hosending;
    this.indatfis = indatfis;
    this.erdatfis = erdatfis;
    this.cddatliq = cddatliq;
    this.cdbloque = cdbloque;
    this.dsobserv = dsobserv;
    this.cdusublo = cdusublo;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuversion = nuversion;
    this.imgasliqdv = imgasliqdv;
    this.imgasliqeu = imgasliqeu;
    this.imgasgesdv = imgasgesdv;
    this.imgasgeseu = imgasgeseu;
    this.imgastotdv = imgastotdv;
    this.imgastoteu = imgastoteu;
    this.cdindgas = cdindgas;
    // this.tmct0ctds = tmct0ctds;
    // this.tmct0ctes = tmct0ctes;
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)) })
  public Tmct0ctaId getId() {
    return this.id;
  }

  public void setId(Tmct0ctaId id) {
    this.id = id;
  }

  @Transient
  public String getNucnfclt() {
    String nucnfclt = null;
    final Tmct0ctaId id = this.getId();
    if (id != null) {
      nucnfclt = id.getNucnfclt();
    }
    return nucnfclt;
  }

  @Transient
  public String getCdmercad() {
    String cdmercad = null;
    final Tmct0ctaId id = this.getId();
    if (id != null) {
      cdmercad = id.getCdmercad();
    }
    return cdmercad;
  }

  @Transient
  public String getNuorden() {
    String nuorden = null;
    final Tmct0ctaId id = this.getId();
    if (id != null) {
      nuorden = id.getNuorden();
    }
    return nuorden;
  }

  @Transient
  public String getCdbroker() {
    String cdbroker = null;
    final Tmct0ctaId id = this.getId();
    if (id != null) {
      cdbroker = id.getCdbroker();
    }
    return cdbroker;
  }

  @Transient
  public String getNbooking() {
    String nbooking = null;
    final Tmct0ctaId id = this.getId();
    if (id != null) {
      nbooking = id.getNbooking();
    }
    return nbooking;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDBROKER", referencedColumnName = "CDBROKER", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDMERCAD", referencedColumnName = "CDMERCAD", nullable = false, insertable = false, updatable = false) })
  public Tmct0ctg getTmct0ctg() {
    return this.tmct0ctg;
  }

  public void setTmct0ctg(Tmct0ctg tmct0ctg) {
    this.tmct0ctg = tmct0ctg;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  public Tmct0sta getTmct0sta() {
    return this.tmct0sta;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Column(name = "CDALIAS", length = 20)
  public String getCdalias() {
    return this.cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  @Column(name = "DESCRALI", length = 130)
  public String getDescrali() {
    return this.descrali;
  }

  public void setDescrali(String descrali) {
    this.descrali = descrali;
  }

  @Column(name = "IMCAMMED", precision = 18, scale = 8)
  public BigDecimal getImcammed() {
    return this.imcammed;
  }

  public void setImcammed(BigDecimal imcammed) {
    this.imcammed = imcammed;
  }

  @Column(name = "IMCAMNET", precision = 18, scale = 8)
  public BigDecimal getImcamnet() {
    return this.imcamnet;
  }

  public void setImcamnet(BigDecimal imcamnet) {
    this.imcamnet = imcamnet;
  }

  @Column(name = "IMTOTBRU", precision = 18, scale = 8)
  public BigDecimal getImtotbru() {
    return this.imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  @Column(name = "EUTOTBRU", precision = 18, scale = 8)
  public BigDecimal getEutotbru() {
    return this.eutotbru;
  }

  public void setEutotbru(BigDecimal eutotbru) {
    this.eutotbru = eutotbru;
  }

  @Column(name = "IMTOTNET", precision = 18, scale = 8)
  public BigDecimal getImtotnet() {
    return this.imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  @Column(name = "EUTOTNET", precision = 18, scale = 8)
  public BigDecimal getEutotnet() {
    return this.eutotnet;
  }

  public void setEutotnet(BigDecimal eutotnet) {
    this.eutotnet = eutotnet;
  }

  @Column(name = "IMNETBRK", precision = 18, scale = 8)
  public BigDecimal getImnetbrk() {
    return this.imnetbrk;
  }

  public void setImnetbrk(BigDecimal imnetbrk) {
    this.imnetbrk = imnetbrk;
  }

  @Column(name = "EUNETBRK", precision = 18, scale = 8)
  public BigDecimal getEunetbrk() {
    return this.eunetbrk;
  }

  public void setEunetbrk(BigDecimal eunetbrk) {
    this.eunetbrk = eunetbrk;
  }

  @Column(name = "IMCOMISN", precision = 18, scale = 8)
  public BigDecimal getImcomisn() {
    return this.imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  @Column(name = "IMCOMIEU", precision = 18, scale = 8)
  public BigDecimal getImcomieu() {
    return this.imcomieu;
  }

  public void setImcomieu(BigDecimal imcomieu) {
    this.imcomieu = imcomieu;
  }

  @Column(name = "IMCOMDEV", precision = 18, scale = 8)
  public BigDecimal getImcomdev() {
    return this.imcomdev;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  @Column(name = "EUCOMDEV", precision = 18, scale = 8)
  public BigDecimal getEucomdev() {
    return this.eucomdev;
  }

  public void setEucomdev(BigDecimal eucomdev) {
    this.eucomdev = eucomdev;
  }

  @Column(name = "IMFIBRDV", precision = 18, scale = 8)
  public BigDecimal getImfibrdv() {
    return this.imfibrdv;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  @Column(name = "IMFIBREU", precision = 18, scale = 8)
  public BigDecimal getImfibreu() {
    return this.imfibreu;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  @Column(name = "IMSVB", precision = 18, scale = 8)
  public BigDecimal getImsvb() {
    return this.imsvb;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  @Column(name = "IMSVBEU", precision = 18, scale = 8)
  public BigDecimal getImsvbeu() {
    return this.imsvbeu;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  @Column(name = "IMBRMERC", precision = 18, scale = 8)
  public BigDecimal getImbrmerc() {
    return this.imbrmerc;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  @Column(name = "IMBRMREU", precision = 18, scale = 8)
  public BigDecimal getImbrmreu() {
    return this.imbrmreu;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  @Column(name = "IMGASTOS", precision = 18, scale = 8)
  public BigDecimal getImgastos() {
    return this.imgastos;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  @Column(name = "IMGATDVS", precision = 18, scale = 8)
  public BigDecimal getImgatdvs() {
    return this.imgatdvs;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  @Column(name = "IMIMPDVS", precision = 18, scale = 8)
  public BigDecimal getImimpdvs() {
    return this.imimpdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  @Column(name = "IMIMPEUR", precision = 18, scale = 8)
  public BigDecimal getImimpeur() {
    return this.imimpeur;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  @Column(name = "IMCCONEU", precision = 18, scale = 8)
  public BigDecimal getImcconeu() {
    return this.imcconeu;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  @Column(name = "IMCLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcliqeu() {
    return this.imcliqeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  @Column(name = "IMDEREEU", precision = 18, scale = 8)
  public BigDecimal getImdereeu() {
    return this.imdereeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  @Column(name = "NUTITAGR", precision = 16, scale = 5)
  public BigDecimal getNutitagr() {
    return this.nutitagr;
  }

  public void setNutitagr(BigDecimal nutitagr) {
    this.nutitagr = nutitagr;
  }

  @Column(name = "IMNETOBR", precision = 18, scale = 8)
  public BigDecimal getImnetobr() {
    return this.imnetobr;
  }

  public void setImnetobr(BigDecimal imnetobr) {
    this.imnetobr = imnetobr;
  }

  @Column(name = "PODEVOLU", precision = 16, scale = 6)
  public BigDecimal getPodevolu() {
    return this.podevolu;
  }

  public void setPodevolu(BigDecimal podevolu) {
    this.podevolu = podevolu;
  }

  @Column(name = "POTAXBRK", precision = 16, scale = 6)
  public BigDecimal getPotaxbrk() {
    return this.potaxbrk;
  }

  public void setPotaxbrk(BigDecimal potaxbrk) {
    this.potaxbrk = potaxbrk;
  }

  @Column(name = "PCCOMBRK", precision = 16, scale = 6)
  public BigDecimal getPccombrk() {
    return this.pccombrk;
  }

  public void setPccombrk(BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  @Column(name = "CDCLEARE", length = 25)
  public String getCdcleare() {
    return this.cdcleare;
  }

  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  @Column(name = "ACCTATCLE", length = 34)
  public String getAcctatcle() {
    return this.acctatcle;
  }

  public void setAcctatcle(String acctatcle) {
    this.acctatcle = acctatcle;
  }

  @Column(name = "TYSETCLE", length = 25)
  public String getTysetcle() {
    return this.tysetcle;
  }

  public void setTysetcle(String tysetcle) {
    this.tysetcle = tysetcle;
  }

  @Column(name = "CDBENCLE", length = 146)
  public String getCdbencle() {
    return this.cdbencle;
  }

  public void setCdbencle(String cdbencle) {
    this.cdbencle = cdbencle;
  }

  @Column(name = "ACBENCLE", length = 34)
  public String getAcbencle() {
    return this.acbencle;
  }

  public void setAcbencle(String acbencle) {
    this.acbencle = acbencle;
  }

  @Column(name = "IDBENCLE", length = 43)
  public String getIdbencle() {
    return this.idbencle;
  }

  public void setIdbencle(String idbencle) {
    this.idbencle = idbencle;
  }

  @Column(name = "GCLECODE", length = 146)
  public String getGclecode() {
    return this.gclecode;
  }

  public void setGclecode(String gclecode) {
    this.gclecode = gclecode;
  }

  @Column(name = "GCLEACCT", length = 34)
  public String getGcleacct() {
    return this.gcleacct;
  }

  public void setGcleacct(String gcleacct) {
    this.gcleacct = gcleacct;
  }

  @Column(name = "GCLEID", length = 43)
  public String getGcleid() {
    return this.gcleid;
  }

  public void setGcleid(String gcleid) {
    this.gcleid = gcleid;
  }

  @Column(name = "LCLECODE", length = 146)
  public String getLclecode() {
    return this.lclecode;
  }

  public void setLclecode(String lclecode) {
    this.lclecode = lclecode;
  }

  @Column(name = "LCLEACCT", length = 34)
  public String getLcleacct() {
    return this.lcleacct;
  }

  public void setLcleacct(String lcleacct) {
    this.lcleacct = lcleacct;
  }

  @Column(name = "LCLEID", length = 43)
  public String getLcleid() {
    return this.lcleid;
  }

  public void setLcleid(String lcleid) {
    this.lcleid = lcleid;
  }

  @Column(name = "PCCLESET", length = 43)
  public String getPccleset() {
    return this.pccleset;
  }

  public void setPccleset(String pccleset) {
    this.pccleset = pccleset;
  }

  @Column(name = "SENDBECLE", length = 1)
  public Character getSendbecle() {
    return this.sendbecle;
  }

  public void setSendbecle(Character sendbecle) {
    this.sendbecle = sendbecle;
  }

  @Column(name = "TPSETCLE", length = 25)
  public String getTpsetcle() {
    return this.tpsetcle;
  }

  public void setTpsetcle(String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  @Column(name = "CDCUSTOD", length = 25)
  public String getCdcustod() {
    return this.cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  @Column(name = "ACCTATCUS", length = 34)
  public String getAcctatcus() {
    return this.acctatcus;
  }

  public void setAcctatcus(String acctatcus) {
    this.acctatcus = acctatcus;
  }

  @Column(name = "TYSETCUS", length = 25)
  public String getTysetcus() {
    return this.tysetcus;
  }

  public void setTysetcus(String tysetcus) {
    this.tysetcus = tysetcus;
  }

  @Column(name = "CDBENCUS", length = 146)
  public String getCdbencus() {
    return this.cdbencus;
  }

  public void setCdbencus(String cdbencus) {
    this.cdbencus = cdbencus;
  }

  @Column(name = "ACBENCUS", length = 34)
  public String getAcbencus() {
    return this.acbencus;
  }

  public void setAcbencus(String acbencus) {
    this.acbencus = acbencus;
  }

  @Column(name = "IDBENCUS", length = 43)
  public String getIdbencus() {
    return this.idbencus;
  }

  public void setIdbencus(String idbencus) {
    this.idbencus = idbencus;
  }

  @Column(name = "GCUSCODE", length = 146)
  public String getGcuscode() {
    return this.gcuscode;
  }

  public void setGcuscode(String gcuscode) {
    this.gcuscode = gcuscode;
  }

  @Column(name = "GCUSACCT", length = 34)
  public String getGcusacct() {
    return this.gcusacct;
  }

  public void setGcusacct(String gcusacct) {
    this.gcusacct = gcusacct;
  }

  @Column(name = "GCUSID", length = 43)
  public String getGcusid() {
    return this.gcusid;
  }

  public void setGcusid(String gcusid) {
    this.gcusid = gcusid;
  }

  @Column(name = "LCUSCODE", length = 146)
  public String getLcuscode() {
    return this.lcuscode;
  }

  public void setLcuscode(String lcuscode) {
    this.lcuscode = lcuscode;
  }

  @Column(name = "LCUSACCT", length = 34)
  public String getLcusacct() {
    return this.lcusacct;
  }

  public void setLcusacct(String lcusacct) {
    this.lcusacct = lcusacct;
  }

  @Column(name = "LCUSID", length = 43)
  public String getLcusid() {
    return this.lcusid;
  }

  public void setLcusid(String lcusid) {
    this.lcusid = lcusid;
  }

  @Column(name = "PCCUSSET", length = 43)
  public String getPccusset() {
    return this.pccusset;
  }

  public void setPccusset(String pccusset) {
    this.pccusset = pccusset;
  }

  @Column(name = "SENDBECUS", length = 1)
  public Character getSendbecus() {
    return this.sendbecus;
  }

  public void setSendbecus(Character sendbecus) {
    this.sendbecus = sendbecus;
  }

  @Column(name = "TPSETCUS", length = 25)
  public String getTpsetcus() {
    return this.tpsetcus;
  }

  public void setTpsetcus(String tpsetcus) {
    this.tpsetcus = tpsetcus;
  }

  @Column(name = "CDENTLIQ", length = 4)
  public String getCdentliq() {
    return this.cdentliq;
  }

  public void setCdentliq(String cdentliq) {
    this.cdentliq = cdentliq;
  }

  @Column(name = "CDREFBAN", length = 32)
  public String getCdrefban() {
    return this.cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  @Column(name = "NBTITULA", length = 130)
  public String getNbtitula() {
    return this.nbtitula;
  }

  public void setNbtitula(String nbtitula) {
    this.nbtitula = nbtitula;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 4)
  public Date getFevalor() {
    return this.fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALORR", length = 4)
  public Date getFevalorr() {
    return this.fevalorr;
  }

  public void setFevalorr(Date fevalorr) {
    this.fevalorr = fevalorr;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FETRADE", length = 4)
  public Date getFetrade() {
    return this.fetrade;
  }

  public void setFetrade(Date fetrade) {
    this.fetrade = fetrade;
  }

  @Column(name = "CDSTCEXC", length = 3)
  public String getCdstcexc() {
    return this.cdstcexc;
  }

  public void setCdstcexc(String cdstcexc) {
    this.cdstcexc = cdstcexc;
  }

  @Column(name = "CDMONISO", length = 3)
  public String getCdmoniso() {
    return this.cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  @Column(name = "IMCAMALO", precision = 18, scale = 8)
  public BigDecimal getImcamalo() {
    return this.imcamalo;
  }

  public void setImcamalo(BigDecimal imcamalo) {
    this.imcamalo = imcamalo;
  }

  @Column(name = "IMMARPRI", precision = 18, scale = 8)
  public BigDecimal getImmarpri() {
    return this.immarpri;
  }

  public void setImmarpri(BigDecimal immarpri) {
    this.immarpri = immarpri;
  }

  @Column(name = "IMCLECHA", precision = 18, scale = 8)
  public BigDecimal getImclecha() {
    return this.imclecha;
  }

  public void setImclecha(BigDecimal imclecha) {
    this.imclecha = imclecha;
  }

  @Column(name = "PKBROKER1", precision = 9)
  public Integer getPkbroker1() {
    return this.pkbroker1;
  }

  public void setPkbroker1(Integer pkbroker1) {
    this.pkbroker1 = pkbroker1;
  }

  @Column(name = "PKENTITY", nullable = false, length = 16)
  public String getPkentity() {
    return this.pkentity;
  }

  public void setPkentity(String pkentity) {
    this.pkentity = pkentity;
  }

  @Column(name = "CDTPOPER", length = 1)
  public Character getCdtpoper() {
    return this.cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  @Column(name = "CDISIN", length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "NBVALORS", length = 40)
  public String getNbvalors() {
    return this.nbvalors;
  }

  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  @Column(name = "NUOPROUT", precision = 9)
  public Integer getNuoprout() {
    return this.nuoprout;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  @Column(name = "NUPAREJE", precision = 4)
  public Short getNupareje() {
    return this.nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  @Column(name = "INFORCON", length = 1)
  public Character getInforcon() {
    return this.inforcon;
  }

  public void setInforcon(Character inforcon) {
    this.inforcon = inforcon;
  }

  @Column(name = "INSENMOD", length = 1)
  public Character getInsenmod() {
    return this.insenmod;
  }

  public void setInsenmod(Character insenmod) {
    this.insenmod = insenmod;
  }

  @Column(name = "INSENFLA", length = 1)
  public Character getInsenfla() {
    return this.insenfla;
  }

  public void setInsenfla(Character insenfla) {
    this.insenfla = insenfla;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FHSENDING", length = 4)
  public Date getFhsending() {
    return this.fhsending;
  }

  public void setFhsending(Date fhsending) {
    this.fhsending = fhsending;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOSENDING", length = 3)
  public Date getHosending() {
    return this.hosending;
  }

  public void setHosending(Date hosending) {
    this.hosending = hosending;
  }

  @Column(name = "INDATFIS", length = 1)
  public Character getIndatfis() {
    return this.indatfis;
  }

  public void setIndatfis(Character indatfis) {
    this.indatfis = indatfis;
  }

  @Column(name = "ERDATFIS", nullable = false, length = 40)
  public String getErdatfis() {
    return this.erdatfis;
  }

  public void setErdatfis(String erdatfis) {
    this.erdatfis = erdatfis;
  }

  @Column(name = "CDDATLIQ", length = 1)
  public Character getCddatliq() {
    return this.cddatliq;
  }

  public void setCddatliq(Character cddatliq) {
    this.cddatliq = cddatliq;
  }

  @Column(name = "CDBLOQUE", length = 1)
  public Character getCdbloque() {
    return this.cdbloque;
  }

  public void setCdbloque(Character cdbloque) {
    this.cdbloque = cdbloque;
  }

  @Column(name = "DSOBSERV", length = 200)
  public String getDsobserv() {
    return this.dsobserv;
  }

  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }

  @Column(name = "CDUSUBLO", length = 10)
  public String getCdusublo() {
    return this.cdusublo;
  }

  public void setCdusublo(String cdusublo) {
    this.cdusublo = cdusublo;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION", precision = 4)
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "IMGASLIQDV", precision = 18, scale = 8)
  public BigDecimal getImgasliqdv() {
    return this.imgasliqdv;
  }

  public void setImgasliqdv(BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  @Column(name = "IMGASLIQEU", precision = 18, scale = 8)
  public BigDecimal getImgasliqeu() {
    return this.imgasliqeu;
  }

  public void setImgasliqeu(BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  @Column(name = "IMGASGESDV", precision = 18, scale = 8)
  public BigDecimal getImgasgesdv() {
    return this.imgasgesdv;
  }

  public void setImgasgesdv(BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  @Column(name = "IMGASGESEU", precision = 18, scale = 8)
  public BigDecimal getImgasgeseu() {
    return this.imgasgeseu;
  }

  public void setImgasgeseu(BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  @Column(name = "IMGASTOTDV", precision = 18, scale = 8)
  public BigDecimal getImgastotdv() {
    return this.imgastotdv;
  }

  public void setImgastotdv(BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  @Column(name = "IMGASTOTEU", precision = 18, scale = 8)
  public BigDecimal getImgastoteu() {
    return this.imgastoteu;
  }

  public void setImgastoteu(BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  @Column(name = "CDINDGAS", length = 1)
  public Character getCdindgas() {
    return this.cdindgas;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0cta")
  public List<Tmct0ctd> getTmct0ctds() {
    return tmct0ctds;
  }

  public void setTmct0ctds(List<Tmct0ctd> tmct0ctds) {
    this.tmct0ctds = tmct0ctds;
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0cta")
  // public Set<Tmct0ctd> getTmct0ctds() {
  // return this.tmct0ctds;
  // }
  //
  // public void setTmct0ctds(Set<Tmct0ctd> tmct0ctds) {
  // this.tmct0ctds = tmct0ctds;
  // }
  //
  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0cta")
  // public Set<Tmct0cte> getTmct0ctes() {
  // return this.tmct0ctes;
  // }
  //
  // public void setTmct0ctes(Set<Tmct0cte> tmct0ctes) {
  // this.tmct0ctes = tmct0ctes;
  // }

}
