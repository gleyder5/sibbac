package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Clase creada para representar la tabla IMPUESTO.
 **/
@Entity
@Table(name = "TMCT0IMPUESTO")
public class Impuesto implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1783422768135910334L;

//@formatter:off
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "nuorden",  column = @Column(name = "NUORDEN", nullable = false, length = 20)), 
                        @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)), 
                        @AttributeOverride(name = "nuagreje", column = @Column(name = "NUAGREJE", nullable = false, length = 20)), 
                        @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
                        @AttributeOverride(name = "nombre", column = @Column(name = "NOMBRE", nullable = false, length = 20))
  })
  private ImpuestoId id;
   /**
   * Relation 1..1 with table TMCT0GAL. Constraint de relación entre TMCT0GAE y TMCT0GAL <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = { @JoinColumn(name = "NUORDEN",  referencedColumnName = "NUORDEN",  nullable = false, insertable = false, updatable = false), 
                       @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
                       @JoinColumn(name = "NUAGREJE", referencedColumnName = "NUAGREJE", nullable = false, insertable = false, updatable = false),
                       @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false)
  })
 protected Tmct0gal tmct0gal;
//@formatter:on

  @Column(name = "PAGOCLTE")
  public Integer pagoClte;

  @Column(name = "PAGOMKT")
  public Integer pagoMkt;

  @Column(name = "IMPDV", length = 18, scale = 8)
  public BigDecimal impDv;

  @Column(name = "IMPEU", length = 18, scale = 8)
  public BigDecimal impEu;

  public ImpuestoId getId() {
    return id;
  }

  public Tmct0gal getTmct0gal() {
    return tmct0gal;
  }

  public Integer getPagoClte() {
    return pagoClte;
  }

  public Integer getPagoMkt() {
    return pagoMkt;
  }

  public BigDecimal getImpDv() {
    return impDv;
  }

  public BigDecimal getImpEu() {
    return impEu;
  }

  public void setId(ImpuestoId id) {
    this.id = id;
  }

  public void setTmct0gal(Tmct0gal tmct0gal) {
    this.tmct0gal = tmct0gal;
  }

  public void setPagoClte(Integer pagoClte) {
    this.pagoClte = pagoClte;
  }

  public void setPagoMkt(Integer pagoMkt) {
    this.pagoMkt = pagoMkt;
  }

  public void setImpDv(BigDecimal impDv) {
    this.impDv = impDv;
  }

  public void setImpEu(BigDecimal impEu) {
    this.impEu = impEu;
  }

}
