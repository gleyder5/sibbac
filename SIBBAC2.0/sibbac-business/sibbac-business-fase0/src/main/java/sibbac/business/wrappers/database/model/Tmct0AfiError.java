package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

@Entity
@Table(name = DBConstants.WRAPPERS.AFI_ERROR)
public class Tmct0AfiError extends ATable<Tmct0AfiError> implements java.io.Serializable {

  private static final long serialVersionUID = 7435988567955368916L;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = true),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = true),
      @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = true) })
  private Tmct0alo tmct0alo;

  @Column(name = "NUCNFLIQ", nullable = true, precision = 4, scale = 0)
  private Short nucnfliq;
  @Column(name = "NUREFORD", length = 32)
  private String nureford;
  @Column(name = "TPDOMICI", length = 2)
  private String tpdomici;
  @Column(name = "NBDOMICI", length = 40)
  private String nbdomici;
  @Column(name = "NUDOMICI", length = 4)
  private String nudomici;
  @Column(name = "NBCIUDAD", length = 40)
  private String nbciudad;
  @Column(name = "NBPROVIN", length = 25)
  private String nbprovin;
  @Column(name = "CDPOSTAL", length = 5)
  private String cdpostal;
  @Column(name = "CDDEPAIS", length = 3)
  private String cddepais;
  @Column(name = "NUDNICIF", length = 40)
  private String nudnicif;
  @Column(name = "NBCLIENT", length = 30)
  private String nbclient;
  @Column(name = "NBCLIENT1", length = 50)
  private String nbclient1;
  @Column(name = "NBCLIENT2", length = 30)
  private String nbclient2;
  @Column(name = "TPTIPREP", length = 1)
  private Character tptiprep;
  @Column(name = "CDNACTIT", length = 3)
  private String cdnactit;
  @Column(name = "TPIDENTI", length = 1)
  private Character tpidenti;
  @Column(name = "TPSOCIED", length = 1)
  private Character tpsocied;
  @Column(name = "TPNACTIT", length = 1)
  private Character tpnactit;
  @Column(name = "RFTITAUD", length = 16)
  private String rftitaud;
  @Column(name = "CDREFERENCIATITULAR", length = 20)
  private String cdreferenciatitular;
  @Column(name = "PARTICIP", precision = 5, scale = 2)
  private BigDecimal particip;
  @Column(name = "CCV", length = 20)
  private String ccv;

  @Column(name = "IDMENSAJE", length = 30, unique = true, nullable = false)
  private String idmensaje;

  @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
  @JoinTable(name = DBConstants.WRAPPERS.AFI_ERROR_CODIGO_ERROR, joinColumns = @JoinColumn(name = "ID_AFI_ERROR", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_CODIGO_ERROR", referencedColumnName = "ID"))
  private Set<CodigoError> errors = new HashSet<CodigoError>();

  public Tmct0AfiError() {
  }

  public Tmct0AfiError(Tmct0AfiError afiError) {
    if (afiError != null) {
      this.tmct0alo = afiError.getTmct0alo();
      this.nureford = afiError.getNureford();
      this.nucnfliq = afiError.getNucnfliq();
      this.tpdomici = afiError.getTpdomici();
      this.nbdomici = afiError.getNbdomici();
      this.nudomici = afiError.getNudomici();
      this.nbciudad = afiError.getNbciudad();
      this.nbprovin = afiError.getNbprovin();
      this.cdpostal = afiError.getCdpostal();
      this.cddepais = afiError.getCddepais();
      this.nudnicif = afiError.getNudnicif();
      this.nbclient = afiError.getNbclient();
      this.nbclient1 = afiError.getNbclient1();
      this.nbclient2 = afiError.getNbclient2();
      this.tptiprep = afiError.getTptiprep();
      this.cdnactit = afiError.getCdnactit();
      this.tpidenti = afiError.getTpidenti();
      this.tpsocied = afiError.getTpsocied();
      this.tpnactit = afiError.getTpnactit();
      this.rftitaud = afiError.getRftitaud();
      this.cdreferenciatitular = afiError.getCdreferenciatitular();
      this.particip = afiError.getParticip();
      this.ccv = afiError.getCcv();
      this.idmensaje = afiError.getIdmensaje();
      if (rftitaud == null) {
        this.rftitaud = afiError.getNudnicif();
      }

      this.errors = afiError.getErrors();
    }
  }

  public Tmct0AfiError(Tmct0alo tmct0alo, String nureford, Short nucnfliq, String tpdomici, String nbdomici,
      String nudomici, String nbciudad, String nbprovin, String cdpostal, String cddepais, String nudnicif,
      String nbclient, String nbclient1, String nbclient2, Character tptiprep, String cdnactit, Character tpidenti,
      Character tpsocied, Character tpnactit, String rftitaud, String cdreferenciatitular, BigDecimal particip,
      String ccv, String idmensaje, Set<CodigoError> errors) {
    this.tmct0alo = tmct0alo;
    this.nureford = nureford;
    this.nucnfliq = nucnfliq;
    this.tpdomici = tpdomici;
    this.nbdomici = nbdomici;
    this.nudomici = nudomici;
    this.nbciudad = nbciudad;
    this.nbprovin = nbprovin;
    this.cdpostal = cdpostal;
    this.cddepais = cddepais;
    if (nudnicif != null) {
      this.nudnicif = nudnicif;
    }
    else {
      this.nudnicif = " ";
    }
    this.nbclient = nbclient;
    this.nbclient1 = nbclient1;
    this.nbclient2 = nbclient2;
    this.tptiprep = tptiprep;
    this.cdnactit = cdnactit;
    this.tpidenti = tpidenti;
    this.tpsocied = tpsocied;
    this.tpnactit = tpnactit;
    this.rftitaud = rftitaud;
    this.cdreferenciatitular = cdreferenciatitular;
    this.particip = particip;
    this.ccv = ccv;
    this.idmensaje = idmensaje;
    if (rftitaud == null) {
      this.rftitaud = nudnicif;
    }

    this.errors = errors;
  }

  public Tmct0alo getTmct0alo() {
    return this.tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  public String getNureford() {
    return this.nureford;
  }

  public void setNureford(String nureford) {
    this.nureford = nureford;
  }

  public Short getNucnfliq() {
    return this.nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public String getTpdomici() {
    return this.tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  public String getNbdomici() {
    return this.nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  public String getNudomici() {
    return this.nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  public String getNbciudad() {
    return this.nbciudad;
  }

  public void setNbciudad(String nbciudad) {
    this.nbciudad = nbciudad;
  }

  public String getNbprovin() {
    return this.nbprovin;
  }

  public void setNbprovin(String nbprovin) {
    this.nbprovin = nbprovin;
  }

  public String getCdpostal() {
    return this.cdpostal;
  }

  public void setCdpostal(String cdpostal) {
    this.cdpostal = cdpostal;
  }

  public String getCddepais() {
    return this.cddepais;
  }

  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  public String getNudnicif() {
    return this.nudnicif;
  }

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  public String getNbclient() {
    return this.nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public String getNbclient1() {
    return this.nbclient1;
  }

  public void setNbclient1(String nbclient1) {
    this.nbclient1 = nbclient1;
  }

  public String getNbclient2() {
    return this.nbclient2;
  }

  public void setNbclient2(String nbclient2) {
    this.nbclient2 = nbclient2;
  }

  public Character getTptiprep() {
    return this.tptiprep;
  }

  public void setTptiprep(Character tptiprep) {
    this.tptiprep = tptiprep;
  }

  public String getCdnactit() {
    return this.cdnactit;
  }

  public void setCdnactit(String cdnactit) {
    this.cdnactit = cdnactit;
  }

  public Character getTpidenti() {
    return this.tpidenti;
  }

  public void setTpidenti(Character tpidenti) {
    this.tpidenti = tpidenti;
  }

  public Character getTpsocied() {
    return this.tpsocied;
  }

  public void setTpsocied(Character tpsocied) {
    this.tpsocied = tpsocied;
  }

  public Character getTpnactit() {
    return this.tpnactit;
  }

  public void setTpnactit(Character tpnactit) {
    this.tpnactit = tpnactit;
  }

  public String getRftitaud() {
    return this.rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  public String getCdreferenciatitular() {
    return this.cdreferenciatitular;
  }

  public void setCdreferenciatitular(String cdreferenciatitular) {
    this.cdreferenciatitular = cdreferenciatitular;
  }

  public BigDecimal getParticip() {
    return this.particip;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  public String getCcv() {
    return this.ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  public String getIdmensaje() {
    return idmensaje;
  }

  public void setIdmensaje(String idmensaje) {
    this.idmensaje = idmensaje;
  }

  public Set<CodigoError> getErrors() {
    return errors;
  }

  public void setErrors(Set<CodigoError> errors) {
    this.errors = errors;
  }

  public void addError(CodigoError error) {
    if (errors == null) {
      errors = new HashSet<CodigoError>();
    }
    this.errors.add(error);
  }

  public boolean lengthsAreValid() {
    boolean lengthsAreValid = true;
    // this.tmct0alo = afiError.getTmct0alo();
    // this.nureford = afiError.getNureford();
    // this.nucnfliq = afiError.getNucnfliq();
    if (this.tpdomici != null && this.tpdomici.length() > 2) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] tpdomici is longer than expected ");
    }
    if (this.nbdomici != null && this.nbdomici.length() > 40) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbdomici is longer than expected ");
    }
    if (this.nudomici != null && this.nudomici.length() > 4) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nudomici is longer than expected ");
    }
    if (this.nbciudad != null && this.nbciudad.length() > 40) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbciudad is longer than expected ");
    }
    if (this.nbprovin != null && this.nbprovin.length() > 25) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbprovin is longer than expected ");
    }
    if (this.cdpostal != null && this.cdpostal.length() > 5) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cdpostal is longer than expected ");
    }
    if (this.cddepais != null && this.cddepais.length() > 3) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cddepais is longer than expected ");
    }
    if (this.nudnicif != null && this.nudnicif.length() > 11) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nudnicif is longer than expected ");
    }
    if (this.nbclient != null && this.nbclient.length() > 30) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbclient is longer than expected ");
    }
    if (this.nbclient1 != null && this.nbclient1.length() > 50) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbclient1 is longer than expected ");
    }
    if (this.nbclient2 != null && this.nbclient2.length() > 30) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] nbclient2 is longer than expected ");
    }
    if (this.cdnactit != null && this.cdnactit.length() > 3) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cdnactit is longer than expected ");
    }
    if (this.rftitaud != null && this.rftitaud.length() > 25) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] rftitaud is longer than expected ");
    }
    if (this.cdreferenciatitular != null && this.cdreferenciatitular.length() > 20) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] cdreferenciatitular is longer than expected ");
    }
    if (this.ccv != null && this.ccv.length() > 20) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] ccv is longer than expected ");
    }
    if (this.idmensaje != null && this.idmensaje.length() > 30) {
      lengthsAreValid = false;
      LOG.debug("[" + "Tmct0AfiError" + " :: lengthsAreValid] idmensaje is longer than expected ");
    }
    return lengthsAreValid;
  }

}
