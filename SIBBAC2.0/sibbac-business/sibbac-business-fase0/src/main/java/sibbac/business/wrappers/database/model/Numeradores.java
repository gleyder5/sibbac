package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Numeradores }.
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
@Table( name = DBConstants.WRAPPERS.NUMERADOR )
@Entity
@XmlRootElement
@Audited
public class Numeradores extends ATableAudited< Numeradores > implements java.io.Serializable {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 4279311102555858515L;

	@Column( name = "INICIO", nullable = false )
	@XmlAttribute
	private Long				inicio;

	@Column( name = "FIN" )
	@XmlAttribute
	private Long				fin;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_IDENTIFICADOR", referencedColumnName = "ID" )
	@XmlAttribute
	private Identificadores		identificador;

	@Column( name = "SIGUIENTE" )
	@XmlAttribute
	private Long				siguiente;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_TIPODOCUMETO", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private TipoDeDocumento		tipoDeDocumento;

	public Numeradores() {

	}

	/**
	 * @param siguiente
	 *            the siguiente to set
	 */
	public void incrContador() {
		this.siguiente = new Long( ++siguiente );
	}

	/**
	 * @return the inicio
	 */
	public Long getInicio() {
		return inicio;
	}

	/**
	 * @param inicio
	 *            the inicio to set
	 */
	public void setInicio( Long inicio ) {
		this.inicio = inicio;
	}

	/**
	 * @return the fin
	 */
	public Long getFin() {
		return fin;
	}

	/**
	 * @param fin
	 *            the fin to set
	 */
	public void setFin( Long fin ) {
		this.fin = fin;
	}

	/**
	 * @return the identificador
	 */
	public Identificadores getIdentificador() {
		return identificador;
	}

	/**
	 * @param identificador
	 *            the identificador to set
	 */
	public void setIdentificador( Identificadores identificador ) {
		this.identificador = identificador;
	}

	/**
	 * @return the siguiente
	 */
	public Long getSiguiente() {
		return siguiente;
	}

	/**
	 * @param siguiente
	 *            the siguiente to set
	 */
	public void setSiguiente( Long siguiente ) {
		this.siguiente = siguiente;
	}

	/**
	 * @return the tipoDeDocumento
	 */
	public TipoDeDocumento getTipoDeDocumento() {
		return tipoDeDocumento;
	}

	/**
	 * @param tipoDeDocumento
	 *            the tipoDeDocumento to set
	 */
	public void setTipoDeDocumento( TipoDeDocumento tipoDeDocumento ) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	@Override
	public String toString() {
		return "Numeradores [inicio=" + inicio + ", fin=" + fin + ", identificador=" + identificador + ", siguiente=" + siguiente
				+ ", tipoDeDocumento=" + tipoDeDocumento + "]";
	}
}
