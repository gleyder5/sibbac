package sibbac.business.wrappers.database.data;

import java.io.Serializable;

import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;

public class Tmct0CamaraCompensacionData implements Serializable {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Long idCamaraCompensacion;
    private String cdCodigo;
    private String nbDescripcion;
    
    public Tmct0CamaraCompensacionData(){
	//
    }
    public Tmct0CamaraCompensacionData(Tmct0CamaraCompensacion entity){
	entityToData(this, entity);
    }
    
    public static void entityToData(Tmct0CamaraCompensacionData data, Tmct0CamaraCompensacion entity){
	data = entityToData(entity);
    }
    
    public static Tmct0CamaraCompensacionData entityToData(Tmct0CamaraCompensacion entity){
	Tmct0CamaraCompensacionData data = new Tmct0CamaraCompensacionData();
	data.setCdCodigo(entity.getCdCodigo());
	data.setIdCamaraCompensacion(entity.getIdCamaraCompensacion());
	data.setNbDescripcion(entity.getNbDescripcion());
	return data;
    }
    
    /*SETTERS AND GETTERS*/
    
    public Long getIdCamaraCompensacion() {
        return idCamaraCompensacion;
    }
    public void setIdCamaraCompensacion(Long idCamaraCompensacion) {
        this.idCamaraCompensacion = idCamaraCompensacion;
    }
    public String getCdCodigo() {
        return cdCodigo;
    }
    public void setCdCodigo(String cdCodigo) {
        this.cdCodigo = cdCodigo;
    }
    public String getNbDescripcion() {
        return nbDescripcion;
    }
    public void setNbDescripcion(String nbDescripcion) {
        this.nbDescripcion = nbDescripcion;
    }
    
    
}
