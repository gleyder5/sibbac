package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;
import java.util.Date;


public class Tmct0caseoperacionejeData implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4815157240289094741L;
	private String				cdoperacionecc;
	private BigDecimal			nutitpendcase;
	private char				cdsentido;
	private String				cdisin;
	private Date				auditFechaCambio;
	private String				cdalias;
	private String				descrali;
	private Long				idcase;
	private BigDecimal			nutitulostotal;
	

	
	public BigDecimal getNutitulostotal() {
		return nutitulostotal;
	}

	
	public void setNutitulostotal( BigDecimal nutitulostotal ) {
		this.nutitulostotal = nutitulostotal;
	}

	public Tmct0caseoperacionejeData() {
	}

	public Tmct0caseoperacionejeData( String cdoperacionecc, BigDecimal nutitpendcase, char cdsentido, String cdisin ) {
		this.cdoperacionecc = cdoperacionecc;
		this.nutitpendcase = nutitpendcase;
		this.cdsentido = cdsentido;
		this.cdisin = cdisin;
		this.cdalias = "-";
		/*
		 * if(tmct0ejes!=null){
		 * for(Tmct0eje eje: tmct0ejes){
		 * if(eje.getCdalias()!=null && eje.getCdalias().equals("")){
		 * this.cdalias = eje.getCdalias();
		 * }
		 * }
		 * }
		 */
	}

	public Tmct0caseoperacionejeData( String cdoperacionecc, BigDecimal nutitpendcase, char cdsentido, String cdisin, Long idcase ) {
		this.cdoperacionecc = cdoperacionecc;
		this.nutitpendcase = nutitpendcase;
		this.cdsentido = cdsentido;
		this.cdisin = cdisin;
		this.idcase = idcase;
	}

	public Tmct0caseoperacionejeData( String cdoperacionecc, BigDecimal nutitpendcase, char cdsentido, String cdisin, Long idcase,
			BigDecimal nutitulostotal ) {
		this.cdoperacionecc = cdoperacionecc;
		this.nutitpendcase = nutitpendcase;
		this.cdsentido = cdsentido;
		this.cdisin = cdisin;
		this.idcase = idcase;
		this.nutitulostotal = nutitulostotal;
	}

	public String getCdalias() {
		return cdalias;
	}

	public void setCdalias( String cdalias ) {
		this.cdalias = cdalias;
	}

	public String getCdoperacionecc() {
		return this.cdoperacionecc;
	}

	public void setCdoperacionecc( String cdoperacionecc ) {
		this.cdoperacionecc = cdoperacionecc;
	}

	public BigDecimal getNutitpendcase() {
		return this.nutitpendcase;
	}

	public void setNutitpendcase( BigDecimal nutitpendcase ) {
		this.nutitpendcase = nutitpendcase;
	}

	public char getCdsentido() {
		return this.cdsentido;
	}

	public void setCdsentido( char cdsentido ) {
		this.cdsentido = cdsentido;
	}

	public String getCdisin() {
		return this.cdisin;
	}

	public void setCdisin( String cdisin ) {
		this.cdisin = cdisin;
	}

	public Date getAuditFechaCambio() {
		return this.auditFechaCambio;
	}

	public void setAuditFechaCambio( Date auditFechaCambio ) {
		this.auditFechaCambio = auditFechaCambio;
	}

	public String getDescrali() {
		return descrali;
	}

	public void setDescrali( String descrali ) {
		this.descrali = descrali;
	}

	public Long getIdcase() {
		return idcase;
	}

	public void setIdcase( Long idcase ) {
		this.idcase = idcase;
	}
}
