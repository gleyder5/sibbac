package sibbac.business.fase0.database.bo;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.Tmct0ilqDao;
import sibbac.business.fase0.database.model.Tmct0ilq;
import sibbac.business.fase0.database.model.Tmct0ilqId;
import sibbac.database.bo.AbstractBo;

@Component
public class Tmct0ilqBo extends AbstractBo<Tmct0ilq, Tmct0ilqId, Tmct0ilqDao> {
  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0ilqBo.class);

  public Tmct0ilqBo() {
  }

  public List<Tmct0ilq> findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(String cdaliass,
                                                                                                           String cdsubcta,
                                                                                                           String centro,
                                                                                                           String cdmercad,
                                                                                                           BigDecimal fecha) {
    return dao.findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(cdaliass, cdsubcta,
                                                                                                  centro, cdmercad,
                                                                                                  fecha);
  }

  public List<Tmct0ilq> findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndFhfinal(String cdaliass,
                                                                                           String cdsubcta,
                                                                                           String centro,
                                                                                           String cdmercad,
                                                                                           BigDecimal fhfinal) {
    return dao.findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndFhfinal(cdaliass, cdsubcta, centro, cdmercad,
                                                                                  fhfinal);
  }

}
