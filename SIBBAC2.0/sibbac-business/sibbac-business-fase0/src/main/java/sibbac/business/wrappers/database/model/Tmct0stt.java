package sibbac.business.wrappers.database.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table( name = "TMCT0STT" )
public class Tmct0stt implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8054369811637455493L;
	private String				cdtipest			= "";
	private String				dstipest			= "";
	private String				cdusuaud			= "";
	private Date				fhaudit				= new Date();
	private Integer				nuversion			= 0;
	private List< Tmct0sta >	tmct0stas			= new ArrayList< Tmct0sta >( 0 );

	public Tmct0stt() {
	}

	public Tmct0stt( String cdtipest ) {
		this.cdtipest = cdtipest;
	}

	public Tmct0stt( String cdtipest, String dstipest, String cdusuaud, Date fhaudit, Integer nuversion, List< Tmct0sta > tmct0stas ) {
		this.cdtipest = cdtipest;
		this.dstipest = dstipest;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
		this.tmct0stas = tmct0stas;
	}

	@Id
	@Column( name = "CDTIPEST", unique = true, nullable = false, length = 10 )
	public String getCdtipest() {
		return this.cdtipest;
	}

	public void setCdtipest( String cdtipest ) {
		this.cdtipest = cdtipest;
	}

	@Column( name = "DSTIPEST", length = 40 )
	public String getDstipest() {
		return this.dstipest;
	}

	public void setDstipest( String dstipest ) {
		this.dstipest = dstipest;
	}

	@Column( name = "CDUSUAUD", length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "NUVERSION" )
	public Integer getNuversion() {
		return this.nuversion;
	}

	public void setNuversion( Integer nuversion ) {
		this.nuversion = nuversion;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0stt" )
	public List< Tmct0sta > getTmct0stas() {
		return this.tmct0stas;
	}

	public void setTmct0stas( List< Tmct0sta > tmct0stas ) {
		this.tmct0stas = tmct0stas;
	}

}
