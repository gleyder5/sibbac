package sibbac.business.wrappers.database.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_TIPO_COMPENSADOR" )
public class Tmct0TipoCompensador implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long			serialVersionUID	= 4660951898620842893L;
	private Long						idTipoCompensador;
	private String						cdCodigo;
	private String						nbDescripcion;
	private List< Tmct0Compensador >	tmct0Compensadors	= new ArrayList< Tmct0Compensador >( 0 );

	public Tmct0TipoCompensador() {
	}

	public Tmct0TipoCompensador( Long idTipoCompensador, String cdCodigo ) {
		this.idTipoCompensador = idTipoCompensador;
		this.cdCodigo = cdCodigo;
	}

	public Tmct0TipoCompensador( Long idTipoCompensador, String cdCodigo, String nbDescripcion, List< Tmct0Compensador > tmct0Compensadors ) {
		this.idTipoCompensador = idTipoCompensador;
		this.cdCodigo = cdCodigo;
		this.nbDescripcion = nbDescripcion;
		this.tmct0Compensadors = tmct0Compensadors;
	}

	@Id
	@Column( name = "ID_TIPO_COMPENSADOR", unique = true, nullable = false )
	public Long getIdTipoCompensador() {
		return this.idTipoCompensador;
	}

	public void setIdTipoCompensador( Long idTipoCompensador ) {
		this.idTipoCompensador = idTipoCompensador;
	}

	@Column( name = "CD_CODIGO", unique = true, nullable = false, length = 4 )
	public String getCdCodigo() {
		return this.cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	@Column( name = "NB_DESCRIPCION" )
	public String getNbDescripcion() {
		return this.nbDescripcion;
	}

	public void setNbDescripcion( String nbDescripcion ) {
		this.nbDescripcion = nbDescripcion;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0TipoCompensador" )
	public List< Tmct0Compensador > getTmct0Compensadors() {
		return this.tmct0Compensadors;
	}

	public void setTmct0Compensadors( List< Tmct0Compensador > tmct0Compensadors ) {
		this.tmct0Compensadors = tmct0Compensadors;
	}

}
