/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_SALDO_INICIAL_CUENTA )
@XmlRootElement
public class Tmct0SaldoInicialCuenta extends ATable< Tmct0SaldoInicialCuenta > implements Serializable {

	private static final long		serialVersionUID	= 1L;
	protected static final Logger	LOG					= LoggerFactory.getLogger( Tmct0SaldoInicialCuenta.class );

	@XmlAttribute
	@Temporal( TemporalType.DATE )
	@Column( name = "FECHA", nullable = true )
	protected Date					fecha;

	@XmlAttribute
	@Column( name = "ISIN", nullable = false, length = 12 )
	protected String				isin;

	@XmlAttribute
	@Column( name = "TITULOS", nullable = false, precision = 18, scale = 6 )
	protected BigDecimal			titulos;

	@XmlAttribute
	@Column( name = "IMEFECTIVO", precision = 13, scale = 2 )
	protected BigDecimal			imefectivo;

	@XmlAttribute
	@Column( name = "CDALIASS", nullable = false, length = 20 )
	protected String				cdaliass;

	@XmlAttribute
	@Column( name = "CD_CODIGO_CUENTA_LIQ" )
	private String					cdcodigocuentaliq;

	public Tmct0SaldoInicialCuenta() {

	}

	public Tmct0SaldoInicialCuenta( Date fecha, String isin, BigDecimal titulos, BigDecimal imefectivo, String cdaliass,
			String cdcodigocuentaliq ) {
		this.fecha = fecha;
		this.isin = isin;
		this.titulos = titulos;
		this.imefectivo = imefectivo;
		this.cdaliass = cdaliass;
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha( Date fecha ) {
		this.fecha = fecha;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getImefectivo() {
		return imefectivo;
	}

	public void setImefectivo( BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	public String getCdaliass() {
		return cdaliass;
	}

	public void setCdaliass( String cdaliass ) {
		this.cdaliass = cdaliass;
	}

	public String getCdcodigocuentaliq() {
		return cdcodigocuentaliq;
	}

	public void setCdcodigocuentaliq( String cdcodigocuentaliq ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

	@PrePersist
	public void prePersist() {
		if ( this.cdcodigocuentaliq == null ) {
			LOG.warn( "[PrePersist] WARNING!!! Este proceso (id:{}/cdaliass:{}) no recibe \"CD_CODIGO_CUENTA_LIQ\"; viene como NULL: [{}]",
					this.id, this.cdaliass, this.cdcodigocuentaliq );
		} else {
			if ( this.cdcodigocuentaliq.equalsIgnoreCase( "0" ) || this.cdcodigocuentaliq.equalsIgnoreCase( "1" )
					|| this.cdcodigocuentaliq.equalsIgnoreCase( "2" ) ) {
				LOG.warn( "[PrePersist] WARNING!!! Este proceso (id:{}/cdaliass:{}) esta poniendo \"CD_CODIGO_CUENTA_LIQ\" como: [{}]",
						this.id, this.cdaliass, this.cdcodigocuentaliq );
			}
		}
	}

	@Override
	public String toString() {
		return "sibbac.business.conciliacion.database.model.Tmct0SaldoInicialCuenta[ id=" + id + " ]";
	}

}
