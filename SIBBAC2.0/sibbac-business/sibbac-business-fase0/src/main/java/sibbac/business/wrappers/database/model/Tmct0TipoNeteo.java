/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;


/**
 *
 * @author fjarquellada
 */
@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_TIPO_NETEO )
@XmlRootElement
@NamedQueries( {
		@NamedQuery( name = "Tmct0TipoNeteo.findAll", query = "SELECT t FROM Tmct0TipoNeteo t" ),
		@NamedQuery( name = "Tmct0TipoNeteo.findById", query = "SELECT t FROM Tmct0TipoNeteo t WHERE t.id = :id" ),
		@NamedQuery( name = "Tmct0TipoNeteo.findByNombre", query = "SELECT t FROM Tmct0TipoNeteo t WHERE t.nombre = :nombre" )
} )
public class Tmct0TipoNeteo implements Serializable {

	private static final long	serialVersionUID	= 1L;
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Basic( optional = false )
	@Column( name = "ID" )
	private long				id;
	@Basic( optional = false )
	@Column( name = "NOMBRE" )
	private String				nombre;

	public Tmct0TipoNeteo() {
	}

	public Tmct0TipoNeteo( Integer id ) {
		this.id = id;
	}

	public Tmct0TipoNeteo( Integer id, String nombre ) {
		this.id = id;
		this.nombre = nombre;
	}

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != 0L ? Long.valueOf( id ).hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if ( !( object instanceof Tmct0TipoNeteo ) ) {
			return false;
		}
		Tmct0TipoNeteo other = ( Tmct0TipoNeteo ) object;
		if ( ( this.id == 0L && other.id != 0L ) || ( this.id != 0L && !Long.valueOf( this.id ).equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sibbac.business.conciliacion.database.model.Tmct0TipoNeteo[ id=" + id + " ]";
	}

}
