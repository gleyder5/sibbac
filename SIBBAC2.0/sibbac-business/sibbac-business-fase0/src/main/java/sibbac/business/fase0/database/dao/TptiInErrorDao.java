package sibbac.business.fase0.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.TptiInError;

@Repository
public interface TptiInErrorDao extends JpaRepository<TptiInError, Long> {

  @Query("select e from TptiInError e where e.tptiIn.id = :idPtiIn")
  List<TptiInError> findAllByIdTptiIn(@Param("idPtiIn") long idPtiIn);

}// public interface TptiInDao extends JpaRepository< TptiIn , Long> {
