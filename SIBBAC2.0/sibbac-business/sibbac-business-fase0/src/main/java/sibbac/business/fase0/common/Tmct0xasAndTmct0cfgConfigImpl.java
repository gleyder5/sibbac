package sibbac.business.fase0.common;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.dao.Tmct0xasDao;
import sibbac.business.fase0.database.dto.HoraEjeccucionDTO;
import sibbac.business.fase0.database.dto.PTIMessageVersionDateDTO;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;
import sibbac.common.utils.SibbacEnums.Tmct0xasConversionKey;
import sibbac.pti.PTIMessageVersion;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Tmct0xasAndTmct0cfgConfigImpl implements Tmct0xasAndTmct0cfgConfig {

  /**
   * 
   */
  private static final long serialVersionUID = 6519091498053067490L;

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0xasAndTmct0cfgConfigImpl.class);

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Autowired
  private Tmct0cfgDao cfgDao;

  @Autowired
  private Tmct0xasDao xasDao;

  private Map<String, String> mapConversiones;
  private Map<String, String> mapConfiguracion;

  public Tmct0xasAndTmct0cfgConfigImpl() {
    this.mapConversiones = new HashMap<String, String>();
    this.mapConfiguracion = new HashMap<String, String>();
  }

  protected String getConversion(final String nbcamxml, final String inforxml, final String nbcamas4) {
    LOG.trace("[getConversion] inicio: {}-{}-{}", nbcamxml, inforxml, nbcamas4);
    String keyMap = String.format("{nbcamxml=%s,inforxml=%s,nbcamas4=%s}", StringUtils.trim(nbcamxml),
        StringUtils.trim(inforxml), StringUtils.trim(nbcamas4));
    String res = this.mapConfiguracion.get(keyMap);
    if (res == null) {
      LOG.trace("[getConversion] valor no encontrado en Map...");
      synchronized (this.mapConversiones) {
        res = this.mapConfiguracion.get(keyMap);
        if (res == null) {
          LOG.trace("[getConversion]synchronized busqueda valor en bbdd...");
          Tmct0xas xas = xasDao.findByNameAndInforAndNbcamas(nbcamxml, inforxml, nbcamas4);
          if (xas != null) {
            LOG.trace("[getConversion]synchronized encontrado registro en bbdd...");
            res = xas.getInforas4();
            this.mapConversiones.put(keyMap, res);
          }
          else {

            LOG.warn("[getConversion]synchronized no configurado conversion: {}", keyMap);
          }
        }
        else {
          LOG.trace("[getConversion]synchronized encontrado valor en segunda comprobacion del map...");
        }
        // if (res == null) {
        // res = " ";
        // this.mapConversiones.put(keyMap, res);
        // }
      }
    }
    LOG.trace("[getConversion] fin: {}-{}-{} = {}", nbcamxml, inforxml, nbcamas4, res);
    return res;
  }

  protected String getConversion(Tmct0xasConversionKey key, String valor) {
    return this.getConversion(key.getNbcamxml(), valor, key.getNbcamas4());
  }

  private String getConfiguracionKey(String sibbac20ApplicationName, String process, String keyname) {
    return String.format("{application=%s,process=%s,keyname=%s}", sibbac20ApplicationName, process, keyname);
  }

  private String getConfiguracionKey(Tmct0cfgConfig key) {
    return this.getConfiguracionKey(sibbac20ApplicationName, key.getProcess(), key.getKeyname());
  }

  protected String getConfiguracion(Tmct0cfgConfig key) {
    LOG.trace("[getConfiguracion] inicio: {}-{}-{}", sibbac20ApplicationName, key.getProcess(), key.getKeyname());

    String keyMap = this.getConfiguracionKey(key);
    String res = this.mapConfiguracion.get(keyMap);
    if (res == null) {
      LOG.trace("[getConfiguracion] valor no encontrado en Map...");
      synchronized (this.mapConfiguracion) {
        res = this.mapConfiguracion.get(keyMap);
        if (res == null) {
          LOG.trace("[getConfiguracion]synchronized busqueda valor en bbdd...");
          Tmct0cfg cfg = cfgDao.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName, key.getProcess(),
              key.getKeyname());

          if (cfg != null) {
            LOG.trace("[getConfiguracion]synchronized encontrado registro en bbdd...");
            res = cfg.getKeyvalue();
            this.mapConfiguracion.put(keyMap, res);
          }
          else {
            LOG.warn("[getConfiguracion]synchronized no configurado cfg: {}", keyMap);
          }
        }
        else {
          LOG.trace("[getConfiguracion]synchronized encontrado valor en segunda comprobacion del map...");
        }
      }
    }
    LOG.trace("[getConfiguracion] fin: {}-{}-{} = {}", sibbac20ApplicationName, key.getProcess(), key.getKeyname(), res);
    return res;
  }

  private List<String> getListConfiguracionAux(String keyMap) {
    List<String> res = null;
    for (String keySet : this.mapConfiguracion.keySet()) {
      if (keySet.startsWith(keyMap)) {
        if (res == null) {
          res = new ArrayList<String>();
        }
        res.add(this.mapConfiguracion.get(keySet));
      }
    }

    return res;
  }

  protected List<String> getListConfiguracion(Tmct0cfgConfig key) {
    String keyMapAux;
    String keyMap = this.getConfiguracionKey(key);
    List<String> res = this.getListConfiguracionAux(keyMap);

    if (CollectionUtils.isEmpty(res)) {
      synchronized (this.mapConfiguracion) {
        res = this.getListConfiguracionAux(keyMap);
        if (CollectionUtils.isEmpty(res)) {
          res = new ArrayList<String>();
          List<Tmct0cfg> cfgs = cfgDao.findByAplicationAndProcessAndLikeKeyname(sibbac20ApplicationName,
              key.getProcess(), "%" + key.getKeyname() + "%");
          for (Tmct0cfg tmct0cfg : cfgs) {
            keyMapAux = this.getConfiguracionKey(sibbac20ApplicationName, tmct0cfg.getId().getProcess(), tmct0cfg
                .getId().getKeyname());
            this.mapConfiguracion.put(keyMapAux, tmct0cfg.getKeyvalue());
            res.add(tmct0cfg.getKeyvalue());
          }
        }
      }
    }
    return res;
  }

  /******************************************************************************** CONFIGURACION *********************************************************************/

  public List<Integer> getWorkDaysOfWeek() {
    List<Integer> workDaysOfWeekConfigList = new ArrayList<Integer>();
    String valueSt = getConfiguracion(Tmct0cfgConfig.CONFIG_WEEK_WORK_DAYS);
    if (valueSt != null)
      for (String workDay : valueSt.split(",")) {
        workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
      }
    return workDaysOfWeekConfigList;
  }

  public List<Date> getHolidays() throws ParseException {
    List<Date> holidaysConfigList = new ArrayList<Date>();
    SimpleDateFormat dF2 = new SimpleDateFormat("yyyy-MM-dd");
    for (String valueSt : getListConfiguracion(Tmct0cfgConfig.CONFIG_HOLIDAYS_DAYS)) {
      holidaysConfigList.add(dF2.parse(valueSt));
    }
    return holidaysConfigList;
  }

  /**
   * @return true if is config to load internationa xml's
   * CONFIG_LOAD_XML_INTERNACIONAL("CONFIG", "xml.load.internacional"),
   */
  public boolean isLoadXmlInternacional() {
    boolean res = false;
    String valueSt = getConfiguracion(Tmct0cfgConfig.CONFIG_LOAD_XML_INTERNACIONAL);
    if (valueSt != null && "S".equalsIgnoreCase(valueSt.trim())) {
      res = true;
    }
    return res;
  }

  public String getHoraInicioSessionPti() {

    String valueSt = getConfiguracion(Tmct0cfgConfig.AUD_INIT_DAY_HOUR_BEGIN);
    return valueSt;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Date findHoraInicioImputacionesS3() throws SIBBACBusinessException {
    Date res = null;
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI);

    if (valueSt != null) {
      res = FormatDataUtils.convertStringToDate(valueSt.trim(), FormatDataUtils.TIME_FORMAT);
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * para segmento mab
   * @throws SIBBACBusinessException
   */
  public Date findHoraInicioImputacionesMabS3() throws SIBBACBusinessException {
    Date res = null;
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI_MAB);
    if (valueSt != null) {
      res = FormatDataUtils.convertStringToDate(valueSt.trim(), FormatDataUtils.TIME_FORMAT);
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Date findHoraFinalImputacionesS3() throws SIBBACBusinessException {
    Date res = null;
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND);
    if (valueSt != null) {
      res = FormatDataUtils.convertStringToDate(valueSt.trim(), FormatDataUtils.TIME_FORMAT);
    }
    else {
      LOG.warn("[findHoraFinalImputacionesS3] la hora final de las imputaciones s3 no esta configurada {}",
          Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND);
      res = FormatDataUtils.convertStringToDate("235959", FormatDataUtils.TIME_FORMAT);
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * para segmento mab
   * @throws SIBBACBusinessException
   */
  public Date findHoraFinalImputacionesMabS3() throws SIBBACBusinessException {
    Date res = null;
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_MAB);

    if (valueSt != null) {
      res = FormatDataUtils.convertStringToDate(valueSt.trim(), FormatDataUtils.TIME_FORMAT);
    }
    else {
      LOG.warn("[findHoraFinalImputacionesMabS3] la hora final de las imputaciones s3 no esta configurada {}",
          Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_MAB);
      res = FormatDataUtils.convertStringToDate("235959", FormatDataUtils.TIME_FORMAT);
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Integer findDiasRevisionProcesoEnvioMovimientos() throws SIBBACBusinessException {
    Integer res = null;
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ENVIO_MO_SCAN_DAYS_PREV);
    if (valueSt != null) {
      res = Integer.parseInt(valueSt.trim());
    }
    return res;
  }

  /**
   * 
   * @return dias de activacion de procesos
   */
  public List<Integer> findDiasActivacionProcesos() {
    List<Integer> res = new ArrayList<Integer>();
    String valueSt = getConfiguracion(Tmct0cfgConfig.CONFIG_WEEK_PROCESS_ACTIVATION_DAYS);
    if (valueSt != null) {
      String[] diasArray = valueSt.trim().split(",");
      for (String dia : diasArray) {
        res.add(Integer.parseInt(dia.trim()));
      }
    }
    return res;
  }

  // **********************************************CONFIG MO'S
  // *************************************************

  /**
   * @return Estados ecc Vivos mas los estados internos vivos 6, 9, 20 etc
   */
  public List<String> findEstadosEccActivos() {
    List<String> res = new ArrayList<String>();
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESTADOS_ECC_ACTIVOS);

    if (valueSt != null) {
      res.addAll(Arrays.asList(valueSt.trim().split(",")));
    }
    return res;
  }

  /**
   * @return lista de tpopebol, cdmoperacionmkt que son sin ecc
   */
  public String getTpopebolsSinEcc() {
    return getConfiguracion(Tmct0cfgConfig.PTI_CDOPERACIONMKT_SIN_ECC);
  }

  public HoraEjeccucionDTO getIfIsHoraEjecucionFicheroSpb1819() {
    return getIfIsHoraEjecucion(Tmct0cfgConfig.SIBBAC20_SPB_HORAS_EJECUCION);
  }

  private List<HoraEjeccucionDTO> getHorasEjeccion(Tmct0cfgConfig config) {
    List<HoraEjeccucionDTO> res = new ArrayList<HoraEjeccucionDTO>();
    String valorCfg = getConfiguracion(config);
    if (StringUtils.isNotBlank(valorCfg)) {
      String[] horasArray = valorCfg.split(",");
      String[] valoresHoraEjecucionArray;
      if (horasArray != null && horasArray.length > 0) {
        for (String horaEjecucionSt : horasArray) {
          Integer horaDesde = null;
          Integer horaHasta = null;
          Integer diaDesde = null;
          Integer diaHasta = null;
          valoresHoraEjecucionArray = horaEjecucionSt.split("-");
          Integer valorHoraDia;
          for (String valorHoraDiaSt : valoresHoraEjecucionArray) {
            valorHoraDia = Integer.parseInt(StringUtils.trim(valorHoraDiaSt.replaceAll("\\D+", "")));
            if (valorHoraDiaSt.contains("H")) {
              // es una hora
              if (horaDesde == null) {
                horaDesde = valorHoraDia;
              }
              else {
                horaHasta = valorHoraDia;
              }
            }
            else if (valorHoraDiaSt.contains("D")) {
              // es un dia
              if (diaDesde == null) {
                diaDesde = valorHoraDia;
              }
              else {
                diaHasta = valorHoraDia;
              }
            }
            else {
              LOG.warn("INCIDENCIA- configuracion fecha y hora de ejecucion del proceso.{} ", config);
            }

          }
          if (horaHasta == null) {
            horaHasta = horaDesde;
          }
          if (diaHasta == null) {
            diaHasta = diaDesde;
          }
          res.add(new HoraEjeccucionDTO(horaDesde, horaHasta, diaDesde, diaHasta));
        }
      }
      Collections.sort(res, new Comparator<HoraEjeccucionDTO>() {
        @Override
        public int compare(HoraEjeccucionDTO arg0, HoraEjeccucionDTO arg1) {
          return arg0.getHoraInicioEjecucion() >= arg1.getHoraInicioEjecucion() ? 1
              : arg0.getHoraFinEjecucion() >= arg1.getHoraFinEjecucion() ? 1 : 0;
        }
      });
    }
    return res;
  }

  /**
   * * EJEMPLO:12H-21D se ejecuta a las 12 y 21 dias atras 12H-20H-21D-1D </br>
   * se ejecuta de 12 a 20 y desde dia hace 21 dias hasta 1 dia Devuelve si es
   * hora de ejecucion dada una configuracion
   * 
   * @param config
   * @return
   */
  private HoraEjeccucionDTO getIfIsHoraEjecucion(Tmct0cfgConfig config) {
    int hourOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    // TODO dotar a este metodo de mas inteligencia para que podamos configurar
    // las fechas y dias de la semana
    // int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    // int dayOfMoth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    // int dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
    HoraEjeccucionDTO horaEjecucion = null;
    List<HoraEjeccucionDTO> horasEjecucion = getHorasEjeccion(config);
    LOG.debug("{}Horas planificadas: {}", config, horasEjecucion);
    for (HoraEjeccucionDTO horaEjeccucionDTO : horasEjecucion) {
      if (hourOfDay == horaEjeccucionDTO.getHoraInicioEjecucion()
          || hourOfDay > horaEjeccucionDTO.getHoraInicioEjecucion()
          && hourOfDay < horaEjeccucionDTO.getHoraFinEjecucion()) {
        horaEjecucion = horaEjeccucionDTO;
        break;
      }
    }

    return horaEjecucion;
  }

  public String getPtiMessageVersion() {
    return getConfiguracion(Tmct0cfgConfig.PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S);
  }

  public String getPtiAppIdentityDataEntidad() {

    return getConfiguracion(Tmct0cfgConfig.PTI_APPIDENTITYDATA_ENTIDAD);
  }

  public String getPtiImputacionesDailyDeactivate() {
    return getConfiguracion(Tmct0cfgConfig.PTI_IMPUTACIONES_DAILY_DEACTIVATE);
  }

  public String getPtiUsuarioOrigenImputacionPropia() {
    return getConfiguracion(Tmct0cfgConfig.PTI_USUARIO_ORIGEN_IMPUTACION_PROPIA);
  }

  public PTIMessageVersion getPtiMessageVersionFromTmct0cfg(String messageVersionCfg, Date date)
      throws SIBBACBusinessException {
    PTIMessageVersion version = PTIMessageVersion.VERSION_1;
    ObjectMapper mapper = new ObjectMapper();
    mapper.setDateFormat(new SimpleDateFormat(FormatDataUtils.DATE_FORMAT_1));
    List<PTIMessageVersionDateDTO> values;
    try {
      values = mapper.readValue(messageVersionCfg,
          TypeFactory.defaultInstance().constructCollectionType(List.class, PTIMessageVersionDateDTO.class));
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    if (CollectionUtils.isNotEmpty(values)) {
      for (PTIMessageVersionDateDTO ptiMessageVersionDate : values) {
        if (date.compareTo(ptiMessageVersionDate.getInit()) >= 0
            && (ptiMessageVersionDate.getEnd() == null || date.compareTo(ptiMessageVersionDate.getEnd()) <= 0)) {
          version = ptiMessageVersionDate.getVersion();
          break;
        }
      }
    }
    return version;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public PTIMessageVersion getPtiMessageVersionFromTmct0cfg(Date date) throws SIBBACBusinessException {
    try {
      String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S);
      return getPtiMessageVersionFromTmct0cfg(valueSt, date);
    }
    catch (RuntimeException e) {
      String message = MessageFormat.format(
          "Error inesperado al recuperar la versión de parse de mensaje PTI con fecha {0}", date);
      LOG.error("[getPtiMessageVersionFromTmct0cfg] {}", message, e);
    }
    return PTIMessageVersion.VERSION_1;
  }

  public String getIdEuroCcp() {
    return getConfiguracion(Tmct0cfgConfig.EUROCCP_IDECC);
  }

  public String getIdPti() {
    return getConfiguracion(Tmct0cfgConfig.PTI_IDECC);
  }

  public String getCifSvb() {
    return getConfiguracion(Tmct0cfgConfig.SVB_CIF);
  }

  public String getAliasScriptDividend() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.SCRIPT_DIVIDEN_ALIAS_CONFIG);
    return valueSt;
  }

  public List<String> getPtiMessageTypesInOrden() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_DATABASE_MESSAGE_PROCESS_MESSAGE_TYPES_IN_ORDER);
    return Arrays.asList(valueSt.split(","));
  }

  public Long getMinutosEscaladoBokNoActivo() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESCALADOBOK_MINUTOS_NO_ACTIVOS);
    if (StringUtils.isNotBlank(valueSt))
      return Long.parseLong(valueSt.trim());
    else
      return null;
  }

  public Long getMinutosEscaladoAloNoActivo() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESCALADOALO_MINUTOS_NO_ACTIVOS);
    if (StringUtils.isNotBlank(valueSt))
      return Long.parseLong(valueSt.trim());
    else
      return null;
  }

  public Long getMinutosEscaladoAlcNoActivo() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESCALADOALC_MINUTOS_NO_ACTIVOS);
    if (StringUtils.isNotBlank(valueSt))
      return Long.parseLong(valueSt.trim());
    else
      return null;
  }

  public Integer getCdestadoasigGtLockUserActivity() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESTADOS_ASIGNACION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO);
    if (StringUtils.isNotBlank(valueSt))
      return Integer.parseInt(valueSt.trim());
    else
      return null;
  }

  public String getCdestadoasigLockUserActivity() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESTADOS_ASIGNACION_BLOQUEAN_ACTIVIDAD_USUARIO);
    return valueSt;
  }

  public List<Integer> getListCdestadoasigLockUserActivity() {
    String estados = getCdestadoasigLockUserActivity();
    return convertirAListaEstados(estados);
  }

  private List<Integer> convertirAListaEstados(String estados) {
    List<Integer> listEstados = new ArrayList<Integer>();
    if (estados != null && !estados.trim().isEmpty()) {
      String[] estadosArr = estados.split(",");
      for (String st : estadosArr) {
        listEstados.add(Integer.parseInt(st.trim()));
      }
    }
    return listEstados;
  }

  public Integer getCdestadoentrecGtLockUserActivity() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ESTADOS_ENTREGA_RECEPCION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO);
    if (StringUtils.isNotBlank(valueSt))
      return Integer.parseInt(valueSt.trim());
    else
      return null;
  }

  public BigDecimal getEfectivoMinimoDerechos() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.ECONOMIC_CUPON_EFFECTIVE_LESS_THAN);
    if (StringUtils.isNotBlank(valueSt))
      return new BigDecimal(valueSt.trim());
    else
      return null;
  }

  public BigDecimal getCanonEfectivoMinimoDerechos() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.ECONOMIC_CUPON_MANAGEMENT_RIGHTS);
    if (StringUtils.isNotBlank(valueSt))
      return new BigDecimal(valueSt.trim());
    else
      return null;
  }

  public Integer getDaysEnvioCe() {
    String valueSt = getConfiguracion(Tmct0cfgConfig.PTI_ENVIO_CE_SCAN_DAYS_PREV);
    if (StringUtils.isNotBlank(valueSt))
      return Integer.parseInt(valueSt.trim());
    else
      return null;

  }

  /**************************************************************************** FIN*CONFIGURACION *********************************************************************/

  /******************************************************************************** CONVERSIONES **********************************************************************/

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param clearingPlatform
   * @return
   */
  public String getClearingPlatformClearingPlatform(String clearingplatform) {
    LOG.trace("[Tmct0xasBo :: getCamaraCompensacionClearingPlatform] inicio");
    String res = getConversion(Tmct0xasConversionKey.CLEARING_PLATFORM_TO_CLEARING_PLATFORM, clearingplatform);
    LOG.trace("[Tmct0xasBo :: getCamaraCompensacionClearingPlatform] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param clearingPlatform
   * @return
   */
  public String getSentidoSibbacSentidoPti(String sentidoSibbac) {
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] inicio");
    String res = getConversion(Tmct0xasConversionKey.SENTIDO_SIBBAC_SENTIDO_PTI, sentidoSibbac);
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param sendtidoSibbac
   * @return
   */
  public String getSentidoSibbacSentidoEuroCcp(String sentidoSibbac) {
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoEuroCcp] inicio");
    String res = getConversion(Tmct0xasConversionKey.SENTIDO_SIBBAC_SENTIDO_EUROCCP, sentidoSibbac);
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoEuroCcp] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param sendtidoSibbac
   * @return
   */
  public String getSentidoEuroCcpSentidoSibbac(String sentidoSibbac) {
    LOG.trace("[Tmct0xasBo :: getSentidoEuroCcpSentidoSibbac] inicio");
    String res = getConversion(Tmct0xasConversionKey.SENTIDO_EUROCCP_SENTIDO_SIBBAC, sentidoSibbac);
    LOG.trace("[Tmct0xasBo :: getSentidoEuroCcpSentidoSibbac] fin.");
    return res;
  }

  /**
   * 
   * @see Tmct0xasConversionKey
   * 
   * @param clearingPlatform
   * @return
   */
  public Character getSentidoPtiSentidoSibbac(String sentidoPti) {
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] inicio");

    String res = getConversion(Tmct0xasConversionKey.SENTIDO_PTI_SENTIDO_SIBBAC, sentidoPti);
    LOG.trace("[Tmct0xasBo :: getSentidoSibbacSentidoPti] fin.");
    if (StringUtils.isNotBlank(res)) {
      return res.charAt(0);
    }
    else {
      return null;
    }
  }

  public Character getIndicadorCapacidadPtiIndicadorCapacidad(String indCapacidadPti) {
    LOG.trace("[Tmct0xasBo :: getIndicadorCapacidadPtiIndicadorCapacidad] inicio");
    String res = getConversion(Tmct0xasConversionKey.IND_CAPACIDAD_PTI_IND_CAPACIDAD, indCapacidadPti);
    Character resAux = ' ';
    if (StringUtils.isNotBlank(res)) {
      resAux = res.charAt(0);
    }
    return resAux;

  }

  public Character getIndicadorCapacidadIndicadorCapacidadPti(String indCapacidad) {
    LOG.trace("[Tmct0xasBo :: getIndicadorCapacidadIndicadorCapacidadPti] inicio");
    String res = getConversion(Tmct0xasConversionKey.IND_CAPACIDAD_IND_CAPACIDAD_PTI, indCapacidad);
    Character resAux = ' ';
    if (StringUtils.isNotBlank(res)) {
      resAux = res.charAt(0);
    }
    return resAux;
  }

  public String getSettlementExchangeCdmercad(String settlementExchange) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] inicio");
    String res = getConversion(Tmct0xasConversionKey.SETTLEMENT_EXCHANGE_CDMERCAD, settlementExchange);

    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] fin.");
    return res;
  }

  public String getSettlementExchangeCdclsmdo(String settlementExchange) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDMERCAD_CDMERCAD, settlementExchange);
    LOG.trace("[Tmct0xasBo :: getSettlementExchangeCdmercad] fin.");
    return res;
  }

  public String getCdcleareClearer(String cdcleare) {

    LOG.trace("[Tmct0xasBo :: getCdcleareClearer] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDCLEARE_CLEARER, cdcleare);
    LOG.trace("[Tmct0xasBo :: getCdcleareClearer] fin.");
    return res;
  }

  public String getCdbrokerCdbolsas(String cdbroker) {

    LOG.trace("[Tmct0xasBo :: getCdbrokerCdbolsas] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDBROKER_CDBOLSAS, cdbroker);

    LOG.trace("[Tmct0xasBo :: getCdbrokerCdbolsas] fin.");
    return res;
  }

  public String getCdbrokerCdmiembro(String cdbroker) throws NullPointerException {

    LOG.trace("[Tmct0xasBo :: getCdbrokerCdmiembro] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDBROKER_CDMIEMBRO, cdbroker);

    LOG.trace("[Tmct0xasBo :: getCdbrokerCdmiembro] fin.");
    return res;
  }

  public String getCdbrokerMtf(String cdbroker) {
    LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDBROKER_MTF, cdbroker);
    return res;
  }

  public String getCdmercadMtf(String cdmercad) {
    LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDMERCAD_MTF, cdmercad);

    LOG.trace("[Tmct0xasBo :: getCdbrokerMtf] fin.");
    return res;
  }

  public String getCdmercadTpsubmer(String cdmercad) throws NullPointerException {
    LOG.trace("[Tmct0xasBo :: getCdmercadTpsubmer] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDMERCAD_TPSUBMER, cdmercad);

    LOG.trace("[Tmct0xasBo :: getCdmercadTpsubmer] fin.");
    return res;
  }

  public String getCdcanalCdcanent(String cdcanal) {
    LOG.trace("[Tmct0xasBo :: getCdcanalCdcanent] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDCANAL_CDCANENT, cdcanal);

    LOG.trace("[Tmct0xasBo :: getCdcanalCdcanent] fin.");
    return res;
  }

  public String getGenpkbDesglose(String clave) {

    LOG.trace("[Tmct0xasBo :: getGenpkbDesglose] inicio");
    String res = getConversion(Tmct0xasConversionKey.GENPKB_DESGLOSE, clave);

    LOG.trace("[Tmct0xasBo :: getGenpkbDesglose] fin.");
    return res;
  }

  public String getCdtipordTpordens(String cdtipord) {

    LOG.trace("[Tmct0xasBo :: getCdtipordTpordens] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDTIPORD_TPORDENS, cdtipord);

    LOG.trace("[Tmct0xasBo :: getCdtipordTpordens] fin.");
    return res;
  }

  public String getCdcuentaCdorigen(String cdcuenta) {
    LOG.trace("[Tmct0xasBo :: getCdcuentaCdorigen] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDCUENTA_CDORIGEN, cdcuenta);
    LOG.trace("[Tmct0xasBo :: getCdcuentaCdorigen] fin.");
    return res;
  }

  public String getCdclenteCdorigen(String cdclente) {

    LOG.trace("[Tmct0xasBo :: getCdclenteCdorigen] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDCLENTE_CDORIGEN, cdclente);

    LOG.trace("[Tmct0xasBo :: getCdclenteCdorigen] fin.");
    return res;
  }

  public String getCdorigenCdorigen(String cdorigen) {

    LOG.trace("[Tmct0xasBo :: getCdorigenCdorigen] inicio");
    String res = getConversion(Tmct0xasConversionKey.CDORIGEN_CDORIGEN, cdorigen);
    LOG.trace("[Tmct0xasBo :: getCdorigenCdorigen] fin.");
    return res;
  }

  public Character getCdmercadTpmercad(String cdmercad) {

    LOG.trace("[Tmct0xasBo :: getCdmercadTpmercad] inicio");
    Character res = ' ';
    String resAux = getConversion(Tmct0xasConversionKey.CDMERCAD_TPMERCAD, cdmercad);
    if (StringUtils.isNotBlank(resAux)) {
      res = resAux.charAt(0);
    }
    LOG.trace("[Tmct0xasBo :: getCdmercadTpmercad] fin.");
    return res;
  }

  public Character getCdtpcambCdtpcambio(Character cdtpcamb) {

    LOG.trace("[Tmct0xasBo :: getCdtpcambCdtpcambio] inicio");
    Character res = ' ';
    String resAux = getConversion(Tmct0xasConversionKey.CDTPCAMB_TPCAMBIO, cdtpcamb + "");
    if (StringUtils.isNotBlank(resAux)) {
      res = resAux.charAt(0);
    }

    LOG.trace("[Tmct0xasBo :: getCdtpcambCdtpcambio] fin.");
    return res;
  }

  public Character getCdtpcambTpcontra(Character cdtpcamb) {

    LOG.trace("[Tmct0xasBo :: getcdtpcambTpcontra] inicio");
    Character res = ' ';
    String resAux = getConversion(Tmct0xasConversionKey.CDTPCAMB_TPCONTRA, cdtpcamb + "");
    if (StringUtils.isNotBlank(resAux)) {
      res = resAux.charAt(0);
    }

    LOG.trace("[Tmct0xasBo :: getcdtpcambTpcontra] fin.");
    return res;
  }

  /**************************************************************************** FIN*CONVERSIONES **********************************************************************/

  @Override
  public void close() throws IOException {
    this.mapConfiguracion.clear();
    this.mapConversiones.clear();
  }

}
