package sibbac.business.wrappers.database.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0_REGLA")
public class Tmct0Regla implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -587180156505155231L;
  private BigInteger idRegla;
  private String cdCodigo;
  private String nbDescripcion;
  private Date fechaCreacion;
  private Date auditFechaCambio;
  private String auditUser;
  private String activo;
  private List<Tmct0parametrizacion> tmct0parametrizacion = new ArrayList<Tmct0parametrizacion>();

  public Tmct0Regla() {
  }

  public Tmct0Regla(BigInteger idRegla, String cdCodigo, Date fechaCreacion, Date auditFechaCambio, String activo) {
    this.idRegla = idRegla;
    this.cdCodigo = cdCodigo;
    this.fechaCreacion = fechaCreacion;
    this.auditFechaCambio = auditFechaCambio;
    this.activo = activo;
  }

  public Tmct0Regla(BigInteger idRegla, String cdCodigo, String nbDescripcion, Date fechaCreacion,
      Date auditFechaCambio, String auditUser, String activo) {
    this.idRegla = idRegla;
    this.cdCodigo = cdCodigo;
    this.nbDescripcion = nbDescripcion;
    this.fechaCreacion = fechaCreacion;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.activo = activo;
    // this.tmct0Parametrizacions = tmct0Parametrizacions;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_REGLA", unique = true, nullable = false, length = 8)
  public BigInteger getIdRegla() {
    return this.idRegla;
  }

  public void setIdRegla(BigInteger idRegla) {
    this.idRegla = idRegla;
  }

  @Column(name = "CD_CODIGO", nullable = false, length = 16)
  public String getCdCodigo() {
    return this.cdCodigo;
  }

  public void setCdCodigo(String cdCodigo) {
    this.cdCodigo = cdCodigo;
  }

  @Column(name = "NB_DESCRIPCION")
  public String getNbDescripcion() {
    return this.nbDescripcion;
  }

  public void setNbDescripcion(String nbDescripcion) {
    this.nbDescripcion = nbDescripcion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_CREACION", nullable = false, length = 10)
  public Date getFechaCreacion() {
    return this.fechaCreacion;
  }

  public void setFechaCreacion(Date fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", nullable = false, length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "ACTIVO", nullable = false, length = 1)
  public String getActivo() {
    return this.activo;
  }

  public void setActivo(String activo) {
    this.activo = activo;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0Regla")
  public List<Tmct0parametrizacion> getTmct0parametrizacion() {
    return this.tmct0parametrizacion;
  }

  public void setTmct0parametrizacion(List<Tmct0parametrizacion> tmct0Parametrizacions) {
    this.tmct0parametrizacion = tmct0Parametrizacions;
  }
}
