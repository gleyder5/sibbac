package sibbac.business.fase0.database.model;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0brkId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8538242430097079471L;
	private String				cdbroker;
	private BigDecimal			numsec;

	public Tmct0brkId() {
	}

	public Tmct0brkId( String cdbroker, BigDecimal numsec ) {
		this.cdbroker = cdbroker;
		this.numsec = numsec;
	}

	@Column( name = "CDBROKER", nullable = false, length = 20 )
	public String getCdbroker() {
		return this.cdbroker;
	}

	public void setCdbroker( String cdbroker ) {
		this.cdbroker = cdbroker;
	}

	@Column( name = "NUMSEC", nullable = false, precision = 20 )
	public BigDecimal getNumsec() {
		return this.numsec;
	}

	public void setNumsec( BigDecimal numsec ) {
		this.numsec = numsec;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0brkId ) )
			return false;
		Tmct0brkId castOther = ( Tmct0brkId ) other;

		return ( ( this.getCdbroker() == castOther.getCdbroker() ) || ( this.getCdbroker() != null && castOther.getCdbroker() != null && this
				.getCdbroker().equals( castOther.getCdbroker() ) ) )
				&& ( ( this.getNumsec() == castOther.getNumsec() ) || ( this.getNumsec() != null && castOther.getNumsec() != null && this
						.getNumsec().equals( castOther.getNumsec() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getCdbroker() == null ? 0 : this.getCdbroker().hashCode() );
		result = 37 * result + ( getNumsec() == null ? 0 : this.getNumsec().hashCode() );
		return result;
	}

}
