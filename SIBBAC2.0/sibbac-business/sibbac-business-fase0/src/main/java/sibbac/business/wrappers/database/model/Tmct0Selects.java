package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *	Entidad que almacena los valores en la DB. 
 */
@Entity
@Table(name = "TMCT0_SELECTS")
public class Tmct0Selects implements java.io.Serializable {

	private static final long serialVersionUID = 7783121298739827117L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "IDSELECT", length = 8, nullable = false)
	private Long idSelect;
	private String catalogo;
	private String clave;
	private String valor;

	public Tmct0Selects() {
	}
	
	public Long getIdSelect() {
		return this.idSelect;
	}

	public void setIdSelect(Long idSelect) {
		this.idSelect = idSelect;
	}

	@Column(name = "CATALOGO", nullable = false, length = 130)
	public String getCatalogo() {
		return this.catalogo;
	}

	public void setCatalogo(String catalogo) {
		this.catalogo = catalogo;
	}

	@Column(name = "CLAVE", nullable = false, length = 130)
	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "VALOR", nullable = false, length = 130)
	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
