package sibbac.business.fase0.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.CodigoOperacion;

@Repository
public interface CodigoOperacionDao extends JpaRepository< CodigoOperacion, Long > {

}
