package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0ONI")
public class Tmct0oni implements java.io.Serializable {

  private static final long serialVersionUID = -5485963976580566143L;

  @Id
  private Integer id;

  @Column(name = "CDNIVEL0", length = 3, nullable = false)
  private Character cdNivel0;

  @Column(name = "NUORDEN0", length = 5, nullable = false)
  private Integer nuOrden0;

  @Column(name = "NUNIVEL0", length = 5, nullable = false)
  private Integer nuNivel0;

  @Column(name = "NBNIVEL0", length = 25, nullable = false)
  private String nbNivel0;

  @Column(name = "CDNIVEL1", length = 3, nullable = false)
  private Character cdNivel1;

  @Column(name = "NUORDEN1", length = 5, nullable = false)
  private Integer nuOrden1;

  @Column(name = "NUNIVEL1", length = 5, nullable = false)
  private Integer nuNivel1;

  @Column(name = "NBNIVEL1", length = 25, nullable = false)
  private String nbNivel1;

  @Column(name = "CDNIVEL2", length = 3, nullable = false)
  private Character cdNivel2;

  @Column(name = "NUORDEN2", length = 5, nullable = false)
  private Integer nuOrden2;

  @Column(name = "NUNIVEL2", length = 5, nullable = false)
  private Integer nuNivel2;

  @Column(name = "NBNIVEL2", length = 25, nullable = false)
  private String nbNivel2;

  @Column(name = "CDNIVEL3", length = 3, nullable = false)
  private Character cdNivel3;

  @Column(name = "NUORDEN3", length = 5, nullable = false)
  private Integer nuOrden3;

  @Column(name = "NUNIVEL3", length = 5, nullable = false)
  private Integer nuNivel3;

  @Column(name = "NBNIVEL3", length = 25, nullable = false)
  private String nbNivel3;

  @Column(name = "CDNIVEL4", length = 3, nullable = false)
  private Character cdNivel4;

  @Column(name = "NUORDEN4", length = 5, nullable = false)
  private Integer nuOrden4;

  @Column(name = "NUNIVEL4", length = 5, nullable = false)
  private Integer nuNivel4;

  @Column(name = "NBNIVEL4", length = 25, nullable = false)
  private String nbNivel4;

  public Tmct0oni() {
    super();
  }

  public Tmct0oni(Character cdNivel0, Integer nuOrden0, Integer nuNivel0, String nbNivel0, Character cdNivel1,
      Integer nuOrden1, Integer nuNivel1, String nbNivel1, Character cdNivel2, Integer nuOrden2, Integer nuNivel2,
      String nbNivel2, Character cdNivel3, Integer nuOrden3, Integer nuNivel3, String nbNivel3, Character cdNivel4,
      Integer nuOrden4, Integer nuNivel4, String nbNivel4) {
    super();
    this.cdNivel0 = cdNivel0;
    this.nuOrden0 = nuOrden0;
    this.nuNivel0 = nuNivel0;
    this.nbNivel0 = nbNivel0;
    this.cdNivel1 = cdNivel1;
    this.nuOrden1 = nuOrden1;
    this.nuNivel1 = nuNivel1;
    this.nbNivel1 = nbNivel1;
    this.cdNivel2 = cdNivel2;
    this.nuOrden2 = nuOrden2;
    this.nuNivel2 = nuNivel2;
    this.nbNivel2 = nbNivel2;
    this.cdNivel3 = cdNivel3;
    this.nuOrden3 = nuOrden3;
    this.nuNivel3 = nuNivel3;
    this.nbNivel3 = nbNivel3;
    this.cdNivel4 = cdNivel4;
    this.nuOrden4 = nuOrden4;
    this.nuNivel4 = nuNivel4;
    this.nbNivel4 = nbNivel4;
  }

  public Character getCdNivel0() {
    return cdNivel0;
  }

  public void setCdNivel0(Character cdNivel0) {
    this.cdNivel0 = cdNivel0;
  }

  public Integer getNuOrden0() {
    return nuOrden0;
  }

  public void setNuOrden0(Integer nuOrden0) {
    this.nuOrden0 = nuOrden0;
  }

  public Integer getNuNivel0() {
    return nuNivel0;
  }

  public void setNuNivel0(Integer nuNivel0) {
    this.nuNivel0 = nuNivel0;
  }

  public String getNbNivel0() {
    return nbNivel0;
  }

  public void setNbNivel0(String nbNivel0) {
    this.nbNivel0 = nbNivel0;
  }

  public Character getCdNivel1() {
    return cdNivel1;
  }

  public void setCdNivel1(Character cdNivel1) {
    this.cdNivel1 = cdNivel1;
  }

  public Integer getNuOrden1() {
    return nuOrden1;
  }

  public void setNuOrden1(Integer nuOrden1) {
    this.nuOrden1 = nuOrden1;
  }

  public Integer getNuNivel1() {
    return nuNivel1;
  }

  public void setNuNivel1(Integer nuNivel1) {
    this.nuNivel1 = nuNivel1;
  }

  public String getNbNivel1() {
    return nbNivel1;
  }

  public void setNbNivel1(String nbNivel1) {
    this.nbNivel1 = nbNivel1;
  }

  public Character getCdNivel2() {
    return cdNivel2;
  }

  public void setCdNivel2(Character cdNivel2) {
    this.cdNivel2 = cdNivel2;
  }

  public Integer getNuOrden2() {
    return nuOrden2;
  }

  public void setNuOrden2(Integer nuOrden2) {
    this.nuOrden2 = nuOrden2;
  }

  public Integer getNuNivel2() {
    return nuNivel2;
  }

  public void setNuNivel2(Integer nuNivel2) {
    this.nuNivel2 = nuNivel2;
  }

  public String getNbNivel2() {
    return nbNivel2;
  }

  public void setNbNivel2(String nbNivel2) {
    this.nbNivel2 = nbNivel2;
  }

  public Character getCdNivel3() {
    return cdNivel3;
  }

  public void setCdNivel3(Character cdNivel3) {
    this.cdNivel3 = cdNivel3;
  }

  public Integer getNuOrden3() {
    return nuOrden3;
  }

  public void setNuOrden3(Integer nuOrden3) {
    this.nuOrden3 = nuOrden3;
  }

  public Integer getNuNivel3() {
    return nuNivel3;
  }

  public void setNuNivel3(Integer nuNivel3) {
    this.nuNivel3 = nuNivel3;
  }

  public String getNbNivel3() {
    return nbNivel3;
  }

  public void setNbNivel3(String nbNivel3) {
    this.nbNivel3 = nbNivel3;
  }

  public Character getCdNivel4() {
    return cdNivel4;
  }

  public void setCdNivel4(Character cdNivel4) {
    this.cdNivel4 = cdNivel4;
  }

  public Integer getNuOrden4() {
    return nuOrden4;
  }

  public void setNuOrden4(Integer nuOrden4) {
    this.nuOrden4 = nuOrden4;
  }

  public Integer getNuNivel4() {
    return nuNivel4;
  }

  public void setNuNivel4(Integer nuNivel4) {
    this.nuNivel4 = nuNivel4;
  }

  public String getNbNivel4() {
    return nbNivel4;
  }

  public void setNbNivel4(String nbNivel4) {
    this.nbNivel4 = nbNivel4;
  }
}
