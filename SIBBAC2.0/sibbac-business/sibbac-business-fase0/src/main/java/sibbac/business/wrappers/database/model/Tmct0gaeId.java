package sibbac.business.wrappers.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0gaeId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2455579139157642765L;
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden;
  @Column(name = "NBOOKING", nullable = false, length = 20)
  private String nbooking;
  @Column(name = "NUAGREJE", nullable = false, length = 20)
  private String nuagreje;
  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  private String nucnfclt;
  @Column(name = "NUEJECUC", nullable = false, length = 20)
  private String nuejecuc;

  public Tmct0gaeId() {

  }

  public Tmct0gaeId(String nuorden, String nbooking, String nuagreje, String nucnfclt, String nuejecuc) {
    super();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nuagreje = nuagreje;
    this.nucnfclt = nucnfclt;
    this.nuejecuc = nuejecuc;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNuagreje() {
    return nuagreje;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNuagreje(String nuagreje) {
    this.nuagreje = nuagreje;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nuagreje == null) ? 0 : nuagreje.hashCode());
    result = prime * result + ((nucnfclt == null) ? 0 : nucnfclt.hashCode());
    result = prime * result + ((nuejecuc == null) ? 0 : nuejecuc.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0gaeId other = (Tmct0gaeId) obj;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    } else if (!nbooking.equals(other.nbooking))
      return false;
    if (nuagreje == null) {
      if (other.nuagreje != null)
        return false;
    } else if (!nuagreje.equals(other.nuagreje))
      return false;
    if (nucnfclt == null) {
      if (other.nucnfclt != null)
        return false;
    } else if (!nucnfclt.equals(other.nucnfclt))
      return false;
    if (nuejecuc == null) {
      if (other.nuejecuc != null)
        return false;
    } else if (!nuejecuc.equals(other.nuejecuc))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    } else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tmct0gaeId %s##%s##%s##%s##%s", nuorden, nbooking, nuagreje, nucnfclt, nuejecuc);
  }

}
