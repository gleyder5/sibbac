package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Etiquetas }. Entity:
 * "Etiquetas".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = DBConstants.WRAPPERS.ETIQUETA )
@Entity
@XmlRootElement
@Audited
public class Etiquetas extends ATableAudited< Etiquetas > implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6689574366409406444L;

	@Column( name = "NBETIQUETA", nullable = false, length = 50 )
	@XmlAttribute
	private String	etiqueta;

	@Column( name = "NBTABLA", nullable = false, length = 50 )
	@XmlAttribute
	private String	tabla;

	@Column( name = "NBCAMPO", nullable = false, length = 50 )
	@XmlAttribute
	private String	campo;

	public Etiquetas() {

	}

	/**
	 * @return the etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * @param etiqueta
	 *            the etiqueta to set
	 */
	public void setEtiqueta( String etiqueta ) {
		this.etiqueta = etiqueta;
	}

	/**
	 * @return the tabla
	 */
	public String getTabla() {
		return tabla;
	}

	/**
	 * @param tabla
	 *            the tabla to set
	 */
	public void setTabla( String tabla ) {
		this.tabla = tabla;
	}

	/**
	 * @return the campo
	 */
	public String getCampo() {
		return campo;
	}

	/**
	 * @param campo
	 *            the campo to set
	 */
	public void setCampo( String campo ) {
		this.campo = campo;
	}

	@Override
	public String toString() {
		return "Etiquetas [etiqueta=" + etiqueta + ", tabla=" + tabla + ", campo=" + campo + "]";
	}

}
