package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0anotacionecc;


public class AnotacioneccData {

	private Tmct0anotacionecc	anotacionEcc;
	private String				cdcamara;
	private String				cdisin;
	private String				isinDescripcion;
	private String				codCuentaCompensacion;
	private Date				fcontratacion;
	private Date				fliquidacion;
	private char				cdsentido;
	private long				idanotacionecc;
	private BigDecimal			titulos		= BigDecimal.ZERO;
	private BigDecimal			efectivo	= BigDecimal.ZERO;
	private BigDecimal			imprecio	= BigDecimal.ZERO;

	public AnotacioneccData( String cdcamara, String cdisin, String codCuentaCompensacion, Date fcontratacion, Date fliquidacion,
			char cdsentido, BigDecimal titulos, BigDecimal efectivo ) {
		this.cdcamara = cdcamara;
		this.cdisin = cdisin;
		this.codCuentaCompensacion = codCuentaCompensacion;
		this.fcontratacion = fcontratacion;
		this.fliquidacion = fliquidacion;
		this.cdsentido = cdsentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		initAnotacionEcc();
	}

	public AnotacioneccData( String cdisin, String codCuentaCompensacion, char cdsentido, BigDecimal titulos, BigDecimal efectivo ) {
		this.cdisin = cdisin;
		this.codCuentaCompensacion = codCuentaCompensacion;
		this.cdsentido = cdsentido;
		this.titulos = titulos;
		this.efectivo = efectivo;
		initAnotacionEcc();
	}

	public AnotacioneccData( String cdisin, String codCuentaCompensacion, char cdsentido, BigDecimal imprecio, BigDecimal titulos,
			BigDecimal efectivo ) {
		this.cdisin = cdisin;
		this.codCuentaCompensacion = codCuentaCompensacion;
		this.cdsentido = cdsentido;
		this.titulos = titulos;
		this.imprecio = imprecio;
		this.efectivo = efectivo;
		initAnotacionEcc();
	}

	private void initAnotacionEcc() {
		this.anotacionEcc = new Tmct0anotacionecc();

		this.anotacionEcc.setCdcamara( cdcamara );
		this.anotacionEcc.setCdisin( cdisin );
		this.anotacionEcc.setFcontratacion( fcontratacion );
		this.anotacionEcc.setFliqteorica( fliquidacion );
		this.anotacionEcc.setCdsentido( cdsentido );
		this.anotacionEcc.setIdanotacionecc( idanotacionecc );
		this.anotacionEcc.setNutitulos( titulos );
		this.anotacionEcc.setImefectivo( efectivo );
		this.anotacionEcc.setImprecio( imprecio );
	}

	public String getCodCuentaCompensacion() {
		return codCuentaCompensacion;
	}

	public void setCodCuentaCompensacion( String codCuentaCompensacion ) {
		this.codCuentaCompensacion = codCuentaCompensacion;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getCdcamara() {
		return cdcamara;
	}

	public void setCdcamara( String cdcamara ) {
		this.cdcamara = cdcamara;
	}

	public String getCdisin() {
		return cdisin;
	}

	public void setCdisin( String cdisin ) {
		this.cdisin = cdisin;
	}

	public Date getFcontratacion() {
		return fcontratacion;
	}

	public void setFcontratacion( Date fcontratacion ) {
		this.fcontratacion = fcontratacion;
	}

	public Date getFliquidacion() {
		return fliquidacion;
	}

	public void setFliquidacion( Date fliquidacion ) {
		this.fliquidacion = fliquidacion;
	}

	public char getCdsentido() {
		return cdsentido;
	}

	public void setCdsentido( char cdsentido ) {
		this.cdsentido = cdsentido;
	}

	public long getIdanotacionecc() {
		return idanotacionecc;
	}

	public void setIdanotacionecc( long idanotacionecc ) {
		this.idanotacionecc = idanotacionecc;
	}

	public Tmct0anotacionecc getAnotacionEcc() {
		return anotacionEcc;
	}

	public void setAnotacionEcc( Tmct0anotacionecc anotacionEcc ) {
		this.anotacionEcc = anotacionEcc;
	}

	public String getIsinDescripcion() {
		return isinDescripcion;
	}

	public void setIsinDescripcion( String isinDescripcion ) {
		this.isinDescripcion = isinDescripcion;
	}

	public BigDecimal getImprecio() {
		return imprecio;
	}

	public void setImprecio( BigDecimal imprecio ) {
		this.imprecio = imprecio;
	}
}
