package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0bokId implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -126834263383563575L;
  private String nbooking = "";
  private String nuorden = "";

  public Tmct0bokId() {
  }

  public Tmct0bokId(String nbooking, String nuorden) {
    this.nbooking = nbooking;
    this.nuorden = nuorden;
  }

  @Column(name = "NBOOKING", nullable = false, length = 20)
  public String getNbooking() {
    return this.nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  @Column(name = "NUORDEN", nullable = false, length = 20)
  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public boolean equals(Object other) {
    if ((this == other))
      return true;
    if ((other == null))
      return false;
    if (!(other instanceof Tmct0bokId))
      return false;
    Tmct0bokId castOther = (Tmct0bokId) other;

    return ((this.getNbooking() == castOther.getNbooking()) || (this.getNbooking() != null
        && castOther.getNbooking() != null && this.getNbooking().equals(castOther.getNbooking())))
        && ((this.getNuorden() == castOther.getNuorden()) || (this.getNuorden() != null
            && castOther.getNuorden() != null && this.getNuorden().equals(castOther.getNuorden())));
  }

  public int hashCode() {
    int result = 17;

    result = 37 * result + (getNbooking() == null ? 0 : this.getNbooking().hashCode());
    result = 37 * result + (getNuorden() == null ? 0 : this.getNuorden().hashCode());
    return result;
  }

  @Override
  public String toString() {
    return String.format("Tmct0bokId %s##%s", nuorden, nbooking);
  }

}
