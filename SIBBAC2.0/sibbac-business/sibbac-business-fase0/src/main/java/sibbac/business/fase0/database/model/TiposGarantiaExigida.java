package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_TIPOS_GARANTIA_EXIGIDA" )
public class TiposGarantiaExigida extends MasterData< TiposGarantiaExigida > {

	private static final long	serialVersionUID	= -415126369086068855L;

}
