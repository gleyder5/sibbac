package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0PKB. Confirmation sent to broker
 * Documentación para TMCT0PKB <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0PKB")
public class Tmct0pkb implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1627232898238263625L;

  /**
   * Table Field PKBROKER. PK sent Broker Documentación para PKBROKER <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Id
  @Column(name = "PKBROKER", length = 9, scale = 0)
  protected Integer pkbroker;

  /**
   * Relation 1..1 with table TMCT0STA. Constraint de relación entre TMCT0PKB y
   * TMCT0STA <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  protected Tmct0sta tmct0sta;
  /**
   * Relation 1..n with table TMCT0PKC. Constraint de relación entre TMCT0PKC y
   * TMCT0PKB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0pkb")
  protected List<Tmct0pkc> tmct0pkcs = new ArrayList<Tmct0pkc>(0);

  /**
   * Table Field CDBROKER. Broker Documentación para CDBROKER <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBROKER", length = 20)
  protected java.lang.String cdbroker;
  /**
   * Table Field CDMERCAD. Market Documentación para CDMERCAD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDMERCAD", length = 20)
  protected java.lang.String cdmercad;
  /**
   * Table Field CDTPOPER. Buy = C ; Sell = V Documentación para CDTPOPER <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDTPOPER", length = 21)
  protected Character cdtpoper;
  /**
   * Table Field CDISIN. Stock ISIN code Documentación para CDISIN <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDISIN", length = 12)
  protected java.lang.String cdisin;
  /**
   * Table Field NBVALORS. ISIN name Documentación para NBVALORS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NBVALORS", length = 40)
  protected java.lang.String nbvalors;
  /**
   * Table Field CDSTCEXC. Stock Exchange Documentación para CDSTCEXC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDSTCEXC", length = 3)
  protected java.lang.String cdstcexc;
  /**
   * Table Field NUQUANT. Quantity Documentación para NUQUANT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUQUANT", length = 16, scale = 5)
  protected java.math.BigDecimal nuquant;
  /**
   * Table Field CDMONISO. ISO currency code Documentación para CDMONISO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDMONISO", length = 3)
  protected java.lang.String cdmoniso;
  /**
   * Table Field FETRADE. Trade Date Documentación para FETRADE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FETRADE")
  protected Date fetrade;
  /**
   * Table Field FEVALOR. Settlement Date Documentación para FEVALOR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR")
  protected Date fevalor;
  /**
   * Table Field IMBRAPRE. Gross Average Price Documentación para IMBRAPRE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMBRAPRE", length = 18, scale = 8)
  protected java.math.BigDecimal imbrapre;
  /**
   * Table Field IMNTOBRK. Net Broker Documentación para IMNTOBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMNTOBRK", length = 18, scale = 8)
  protected java.math.BigDecimal imntobrk;
  /**
   * Table Field IMBRUBRK. Gross Amount Broker Documentación para IMBRUBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMBRUBRK", length = 18, scale = 8)
  protected java.math.BigDecimal imbrubrk;
  /**
   * Table Field IMCOMBRK. Commission Broker Documentación para IMCOMBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCOMBRK", length = 18, scale = 8)
  protected java.math.BigDecimal imcombrk;
  /**
   * Table Field IMSVB. Commission SVB Documentación para IMSVB <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMSVB", length = 18, scale = 8)
  protected java.math.BigDecimal imsvb;
  /**
   * Table Field IMMARTAX. Market Tax Documentación para IMMARTAX <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMMARTAX", length = 18, scale = 8)
  protected java.math.BigDecimal immartax;
  /**
   * Table Field IMMARCHA. Market Charge Documentación para IMMARCHA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMMARCHA", length = 18, scale = 8)
  protected java.math.BigDecimal immarcha;
  /**
   * Table Field INFORCON. Format confirmation Documentación para INFORCON <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "INFORCON", length = 1)
  protected Character inforcon;
  /**
   * Table Field INSENMOD. Sending mode Documentación para INSENMOD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "INSENMOD", length = 1)
  protected Character insenmod;
  /**
   * Table Field INSENFLA. Sent flag (S/N) Documentación para INSENFLA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "INSENFLA", length = 1)
  protected Character insenfla;
  /**
   * Table Field FHSENDING. Sending date Documentación para FHSENDING <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FHSENDING")
  protected Date fhsending;
  /**
   * Table Field HOSENDING. Sending time Documentación para HOSENDING <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIME)
  @Column(name = "HOSENDING")
  protected Date hosending;
  /**
   * Table Field NUAGRBRK. Broker grouping Documentación para NUAGRBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUAGRBRK", length = 9, scale = 0)
  protected Integer nuagrbrk;
  /**
   * Table Field CDCLEARE. Clearer Documentación para CDCLEARE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDCLEARE", length = 25)
  protected java.lang.String cdcleare;
  /**
   * Table Field TPSETCLE. Settlement Type of the clearer SettlementChain
   * Documentación para TPSETCLE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "TPSETCLE", length = 25)
  protected java.lang.String tpsetcle;
  /**
   * Table Field DSCLEARE. Clearer decription Documentación para DSCLEARE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "DSCLEARE", length = 50)
  protected java.lang.String dscleare;
  /**
   * Table Field NUQUAALO. Quantity Allocation Documentación para NUQUAALO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUQUAALO", length = 16, scale = 5)
  protected java.math.BigDecimal nuquaalo;
  /**
   * Table Field IMCAMALO. Client average price allocation Documentación para
   * IMCAMALO <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCAMALO", length = 18, scale = 8)
  protected java.math.BigDecimal imcamalo;
  /**
   * Table Field IMTOTBRU. Gross consideration Documentación para IMTOTBRU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMTOTBRU", length = 18, scale = 8)
  protected java.math.BigDecimal imtotbru;
  /**
   * Table Field IMTOTNET. Net consideration Documentación para IMTOTNET <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMTOTNET", length = 18, scale = 8)
  protected java.math.BigDecimal imtotnet;
  /**
   * Table Field LCUSBIC. Local custodian BIC Documentación para LCUSBIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "LCUSBIC", length = 11)
  protected java.lang.String lcusbic;
  /**
   * Table Field CDLOCCUS. Local custodian name Documentación para CDLOCCUS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDLOCCUS", length = 146)
  protected java.lang.String cdloccus;
  /**
   * Table Field ACLOCCUS. Local custodian account Documentación para ACLOCCUS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ACLOCCUS", length = 34)
  protected java.lang.String acloccus;
  /**
   * Table Field IDLOCCUS. Local custodian ID Documentación para IDLOCCUS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IDLOCCUS", length = 43)
  protected java.lang.String idloccus;
  /**
   * Table Field GCUSBIC. Global custodian BIC Documentación para GCUSBIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "", length = 20)
  protected java.lang.String gcusbic;
  /**
   * Table Field CDGLOCUS. Global custodian name Documentación para CDGLOCUS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDGLOCUS", length = 11)
  protected java.lang.String cdglocus;
  /**
   * Table Field ACGLOCUS. Global custodian account Documentación para ACGLOCUS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ACGLOCUS", length = 34)
  protected java.lang.String acglocus;
  /**
   * Table Field IDGLOCUS. Global custodian ID Documentación para IDGLOCUS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IDGLOCUS", length = 43)
  protected java.lang.String idglocus;
  /**
   * Table Field CDBENBIC. Beneficiary BIC Documentación para CDBENBIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBENBIC", length = 11)
  protected java.lang.String cdbenbic;
  /**
   * Table Field CDBENEFI. Beneficiary name Documentación para CDBENEFI <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBENEFI", length = 146)
  protected java.lang.String cdbenefi;
  /**
   * Table Field ACBENEFI. Beneficiary account Documentación para ACBENEFI <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ACBENEFI", length = 34)
  protected java.lang.String acbenefi;
  /**
   * Table Field IDBENEFI. Beneficiary ID Documentación para IDBENEFI <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IDBENEFI", length = 43)
  protected java.lang.String idbenefi;
  /**
   * Table Field BLCUSBIC. Broker local custodian BIC Documentación para
   * BLCUSBIC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BLCUSBIC", length = 11)
  protected java.lang.String blcusbic;
  /**
   * Table Field BCDLOCCUS. Broker local custodian name Documentación para
   * BCDLOCCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDLOCCUS", length = 146)
  protected java.lang.String bcdloccus;
  /**
   * Table Field BACLOCCUS. Broker local custodian account Documentación para
   * BACLOCCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BACLOCCUS", length = 34)
  protected java.lang.String bacloccus;
  /**
   * Table Field BIDLOCCUS. Broker local custodian ID Documentación para
   * BIDLOCCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BIDLOCCUS", length = 43)
  protected java.lang.String bidloccus;
  /**
   * Table Field BGCUSBIC. Broker global custodian BIC Documentación para
   * BGCUSBIC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BGCUSBIC", length = 11)
  protected java.lang.String bgcusbic;
  /**
   * Table Field BCDGLOCUS. Broker global custodian name Documentación para
   * BCDGLOCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDGLOCUS", length = 146)
  protected java.lang.String bcdglocus;
  /**
   * Table Field BACGLOCUS. Broker global custodian account Documentación para
   * BACGLOCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BACGLOCUS", length = 34)
  protected java.lang.String bacglocus;
  /**
   * Table Field BIDGLOCUS. Broker global custodian ID Documentación para
   * BIDGLOCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BIDGLOCUS", length = 43)
  protected java.lang.String bidglocus;
  /**
   * Table Field BCDBENBIC. Broker beneficiary BIC Documentación para BCDBENBIC
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDBENBIC", length = 11)
  protected java.lang.String bcdbenbic;
  /**
   * Table Field BCDBENEFI. Broker beneficiary name Documentación para BCDBENEFI
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDBENEFI", length = 146)
  protected java.lang.String bcdbenefi;
  /**
   * Table Field BACBENEFI. Broker beneficiary account Documentación para
   * BACBENEFI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BACBENEFI", length = 34)
  protected java.lang.String bacbenefi;
  /**
   * Table Field BIDBENEFI. Broker beneficiary ID Documentación para BIDBENEFI
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BIDBENEFI", length = 43)
  protected java.lang.String bidbenefi;
  /**
   * Table Field PKENTITY. PK sent entity Documentación para PKENTITY <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PKENTITY", length = 16)
  protected java.lang.String pkentity;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;

  /**
   * Table Field FHSENLIQ. Sending date Clearing Documentación para FHSENLIQ
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FHSENLIQ")
  protected Date fhsenliq;
  /**
   * Table Field HOSENLIQ. Sending time Clearing Documentación para HOSENLIQ
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIME)
  @Column(name = "HOSENLIQ")
  protected Date hosenliq;
  /**
   * Table Field INSENLIQ. Sending Clearing Flag Documentación para INSENLIQ
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "INSENLIQ", length = 1)
  protected Character insenliq;
  /**
   * Table Field CDAUXILI. Auxiliar Documentación para CDAUXILI <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDAUXILI", length = 25)
  protected java.lang.String cdauxili;
  /**
   * Table Field PCCLESET. Place of Settlement Documentación para PCCLESET <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PCCLESET", length = 43)
  protected java.lang.String pccleset;
  /**
   * Table Field NUAGRPKB. Pkb grouping Documentación para NUAGRPKB <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUAGRPKB", length = 9, scale = 0)
  protected Integer nuagrpkb;
  /**
   * Table Field ACCTATCLE. Account to clearer Documentación para ACCTATCLE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ACCTATCLE", length = 34)
  protected java.lang.String acctatcle;

  public Integer getPkbroker() {
    return pkbroker;
  }

  public Tmct0sta getTmct0sta() {
    return tmct0sta;
  }

  public List<Tmct0pkc> getTmct0pkcs() {
    return tmct0pkcs;
  }

  public java.lang.String getCdbroker() {
    return cdbroker;
  }

  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public java.lang.String getCdisin() {
    return cdisin;
  }

  public java.lang.String getNbvalors() {
    return nbvalors;
  }

  public java.lang.String getCdstcexc() {
    return cdstcexc;
  }

  public java.math.BigDecimal getNuquant() {
    return nuquant;
  }

  public java.lang.String getCdmoniso() {
    return cdmoniso;
  }

  public Date getFetrade() {
    return fetrade;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public java.math.BigDecimal getImbrapre() {
    return imbrapre;
  }

  public java.math.BigDecimal getImntobrk() {
    return imntobrk;
  }

  public java.math.BigDecimal getImbrubrk() {
    return imbrubrk;
  }

  public java.math.BigDecimal getImcombrk() {
    return imcombrk;
  }

  public java.math.BigDecimal getImsvb() {
    return imsvb;
  }

  public java.math.BigDecimal getImmartax() {
    return immartax;
  }

  public java.math.BigDecimal getImmarcha() {
    return immarcha;
  }

  public Character getInforcon() {
    return inforcon;
  }

  public Character getInsenmod() {
    return insenmod;
  }

  public Character getInsenfla() {
    return insenfla;
  }

  public Date getFhsending() {
    return fhsending;
  }

  public Date getHosending() {
    return hosending;
  }

  public Integer getNuagrbrk() {
    return nuagrbrk;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public java.lang.String getTpsetcle() {
    return tpsetcle;
  }

  public java.lang.String getDscleare() {
    return dscleare;
  }

  public java.math.BigDecimal getNuquaalo() {
    return nuquaalo;
  }

  public java.math.BigDecimal getImcamalo() {
    return imcamalo;
  }

  public java.math.BigDecimal getImtotbru() {
    return imtotbru;
  }

  public java.math.BigDecimal getImtotnet() {
    return imtotnet;
  }

  public java.lang.String getLcusbic() {
    return lcusbic;
  }

  public java.lang.String getCdloccus() {
    return cdloccus;
  }

  public java.lang.String getAcloccus() {
    return acloccus;
  }

  public java.lang.String getIdloccus() {
    return idloccus;
  }

  public java.lang.String getGcusbic() {
    return gcusbic;
  }

  public java.lang.String getCdglocus() {
    return cdglocus;
  }

  public java.lang.String getAcglocus() {
    return acglocus;
  }

  public java.lang.String getIdglocus() {
    return idglocus;
  }

  public java.lang.String getCdbenbic() {
    return cdbenbic;
  }

  public java.lang.String getCdbenefi() {
    return cdbenefi;
  }

  public java.lang.String getAcbenefi() {
    return acbenefi;
  }

  public java.lang.String getIdbenefi() {
    return idbenefi;
  }

  public java.lang.String getBlcusbic() {
    return blcusbic;
  }

  public java.lang.String getBcdloccus() {
    return bcdloccus;
  }

  public java.lang.String getBacloccus() {
    return bacloccus;
  }

  public java.lang.String getBidloccus() {
    return bidloccus;
  }

  public java.lang.String getBgcusbic() {
    return bgcusbic;
  }

  public java.lang.String getBcdglocus() {
    return bcdglocus;
  }

  public java.lang.String getBacglocus() {
    return bacglocus;
  }

  public java.lang.String getBidglocus() {
    return bidglocus;
  }

  public java.lang.String getBcdbenbic() {
    return bcdbenbic;
  }

  public java.lang.String getBcdbenefi() {
    return bcdbenefi;
  }

  public java.lang.String getBacbenefi() {
    return bacbenefi;
  }

  public java.lang.String getBidbenefi() {
    return bidbenefi;
  }

  public java.lang.String getPkentity() {
    return pkentity;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public Date getFhsenliq() {
    return fhsenliq;
  }

  public Date getHosenliq() {
    return hosenliq;
  }

  public Character getInsenliq() {
    return insenliq;
  }

  public java.lang.String getCdauxili() {
    return cdauxili;
  }

  public java.lang.String getPccleset() {
    return pccleset;
  }

  public Integer getNuagrpkb() {
    return nuagrpkb;
  }

  public java.lang.String getAcctatcle() {
    return acctatcle;
  }

  public void setPkbroker(Integer pkbroker) {
    this.pkbroker = pkbroker;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  public void setTmct0pkcs(List<Tmct0pkc> tmct0pkcs) {
    this.tmct0pkcs = tmct0pkcs;
  }

  public void setCdbroker(java.lang.String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public void setCdisin(java.lang.String cdisin) {
    this.cdisin = cdisin;
  }

  public void setNbvalors(java.lang.String nbvalors) {
    this.nbvalors = nbvalors;
  }

  public void setCdstcexc(java.lang.String cdstcexc) {
    this.cdstcexc = cdstcexc;
  }

  public void setNuquant(java.math.BigDecimal nuquant) {
    this.nuquant = nuquant;
  }

  public void setCdmoniso(java.lang.String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setFetrade(Date fetrade) {
    this.fetrade = fetrade;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setImbrapre(java.math.BigDecimal imbrapre) {
    this.imbrapre = imbrapre;
  }

  public void setImntobrk(java.math.BigDecimal imntobrk) {
    this.imntobrk = imntobrk;
  }

  public void setImbrubrk(java.math.BigDecimal imbrubrk) {
    this.imbrubrk = imbrubrk;
  }

  public void setImcombrk(java.math.BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  public void setImsvb(java.math.BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  public void setImmartax(java.math.BigDecimal immartax) {
    this.immartax = immartax;
  }

  public void setImmarcha(java.math.BigDecimal immarcha) {
    this.immarcha = immarcha;
  }

  public void setInforcon(Character inforcon) {
    this.inforcon = inforcon;
  }

  public void setInsenmod(Character insenmod) {
    this.insenmod = insenmod;
  }

  public void setInsenfla(Character insenfla) {
    this.insenfla = insenfla;
  }

  public void setFhsending(Date fhsending) {
    this.fhsending = fhsending;
  }

  public void setHosending(Date hosending) {
    this.hosending = hosending;
  }

  public void setNuagrbrk(Integer nuagrbrk) {
    this.nuagrbrk = nuagrbrk;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public void setTpsetcle(java.lang.String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  public void setDscleare(java.lang.String dscleare) {
    this.dscleare = dscleare;
  }

  public void setNuquaalo(java.math.BigDecimal nuquaalo) {
    this.nuquaalo = nuquaalo;
  }

  public void setImcamalo(java.math.BigDecimal imcamalo) {
    this.imcamalo = imcamalo;
  }

  public void setImtotbru(java.math.BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public void setImtotnet(java.math.BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public void setLcusbic(java.lang.String lcusbic) {
    this.lcusbic = lcusbic;
  }

  public void setCdloccus(java.lang.String cdloccus) {
    this.cdloccus = cdloccus;
  }

  public void setAcloccus(java.lang.String acloccus) {
    this.acloccus = acloccus;
  }

  public void setIdloccus(java.lang.String idloccus) {
    this.idloccus = idloccus;
  }

  public void setGcusbic(java.lang.String gcusbic) {
    this.gcusbic = gcusbic;
  }

  public void setCdglocus(java.lang.String cdglocus) {
    this.cdglocus = cdglocus;
  }

  public void setAcglocus(java.lang.String acglocus) {
    this.acglocus = acglocus;
  }

  public void setIdglocus(java.lang.String idglocus) {
    this.idglocus = idglocus;
  }

  public void setCdbenbic(java.lang.String cdbenbic) {
    this.cdbenbic = cdbenbic;
  }

  public void setCdbenefi(java.lang.String cdbenefi) {
    this.cdbenefi = cdbenefi;
  }

  public void setAcbenefi(java.lang.String acbenefi) {
    this.acbenefi = acbenefi;
  }

  public void setIdbenefi(java.lang.String idbenefi) {
    this.idbenefi = idbenefi;
  }

  public void setBlcusbic(java.lang.String blcusbic) {
    this.blcusbic = blcusbic;
  }

  public void setBcdloccus(java.lang.String bcdloccus) {
    this.bcdloccus = bcdloccus;
  }

  public void setBacloccus(java.lang.String bacloccus) {
    this.bacloccus = bacloccus;
  }

  public void setBidloccus(java.lang.String bidloccus) {
    this.bidloccus = bidloccus;
  }

  public void setBgcusbic(java.lang.String bgcusbic) {
    this.bgcusbic = bgcusbic;
  }

  public void setBcdglocus(java.lang.String bcdglocus) {
    this.bcdglocus = bcdglocus;
  }

  public void setBacglocus(java.lang.String bacglocus) {
    this.bacglocus = bacglocus;
  }

  public void setBidglocus(java.lang.String bidglocus) {
    this.bidglocus = bidglocus;
  }

  public void setBcdbenbic(java.lang.String bcdbenbic) {
    this.bcdbenbic = bcdbenbic;
  }

  public void setBcdbenefi(java.lang.String bcdbenefi) {
    this.bcdbenefi = bcdbenefi;
  }

  public void setBacbenefi(java.lang.String bacbenefi) {
    this.bacbenefi = bacbenefi;
  }

  public void setBidbenefi(java.lang.String bidbenefi) {
    this.bidbenefi = bidbenefi;
  }

  public void setPkentity(java.lang.String pkentity) {
    this.pkentity = pkentity;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  public void setFhsenliq(Date fhsenliq) {
    this.fhsenliq = fhsenliq;
  }

  public void setHosenliq(Date hosenliq) {
    this.hosenliq = hosenliq;
  }

  public void setInsenliq(Character insenliq) {
    this.insenliq = insenliq;
  }

  public void setCdauxili(java.lang.String cdauxili) {
    this.cdauxili = cdauxili;
  }

  public void setPccleset(java.lang.String pccleset) {
    this.pccleset = pccleset;
  }

  public void setNuagrpkb(Integer nuagrpkb) {
    this.nuagrpkb = nuagrpkb;
  }

  public void setAcctatcle(java.lang.String acctatcle) {
    this.acctatcle = acctatcle;
  }

  @Override
  public String toString() {
    return String.format("Tmct0pkb [id=%s]", this.pkbroker);
  }
}
