package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.bytecode.internal.javassist.FieldHandled;
import org.hibernate.bytecode.internal.javassist.FieldHandler;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0aliId;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Alias } . Entity: "Alias".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = WRAPPERS.ALIAS )
@Entity
@Audited
public class Alias extends ATableAudited< Alias > implements java.io.Serializable, FieldHandled {

	private FieldHandler			fieldHandler;

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 9081239442995540410L;
	@OneToOne( optional = false )
	@JoinColumns( value = {
			@JoinColumn( name = "CDBROCLI", referencedColumnName = "CDBROCLI", nullable = false ),
			@JoinColumn( name = "CDALIASS", referencedColumnName = "CDALIASS", nullable = false ),
			@JoinColumn( name = "NUMSEC", referencedColumnName = "NUMSEC", nullable = false )
	} )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	private Tmct0ali				tmct0ali;

	@ManyToOne( optional = true )
	@JoinColumn( name = "ID_ALIAS_FACTURACION", referencedColumnName = "ID", nullable = true )
	private Alias					aliasFacturacion;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_IDIOMA", referencedColumnName = "ID", nullable = false )
	private Idioma					idioma;

	@OneToMany( mappedBy = "alias" )
	private List< Contacto >		contactos;

	@OneToMany( mappedBy = "aliasFacturacion" )
	private List< AliasSubcuenta >	aliasSubcuentas;

	@OneToMany( mappedBy = "aliasFacturacion" )
	private List< Alias >			aliasFacturables;

	@OneToOne( fetch = FetchType.LAZY, optional = true, mappedBy = "alias" )
	@LazyToOne( LazyToOneOption.NO_PROXY )
	private AliasDireccion			aliasDireccion;

	@OneToOne( fetch = FetchType.LAZY, optional = true, mappedBy = "alias" )
	@LazyToOne( LazyToOneOption.NO_PROXY )
	private AliasRecordatorio		aliasRecordatorio;

 	// ---------------------------------------------- Bean Constructors -------------------------------

	protected Alias() {
	}

	public Alias( final Tmct0ali tmct0ali, final Idioma idioma ) {
		super();
		this.tmct0ali = tmct0ali;
		this.idioma = idioma;
	}

	public FieldHandler getFieldHandler() {
		return fieldHandler;
	}

	public void setFieldHandler( FieldHandler fieldHandler ) {
		this.fieldHandler = fieldHandler;
	}

	/**
	 * @param alias
	 */
	public String formarNombreAlias() {
		return Alias.formarNombreAlias( getCdaliass(), getDescrali() );
	}

	/**
	 * @param cdaliass
	 * @param descrali
	 * @return
	 */
	public static String formarNombreAlias( String cdaliass, String descrali ) {
		String nombre = "";
		cdaliass = StringUtils.trimToEmpty( cdaliass );
		descrali = StringUtils.trimToEmpty( descrali );
		if ( StringUtils.isNotEmpty( cdaliass ) ) {
			nombre = StringUtils.trimToEmpty( cdaliass );
		}
		if ( StringUtils.isNotEmpty( cdaliass ) && StringUtils.isNotEmpty( descrali ) ) {
			nombre = nombre + " - ";
		}
		if ( StringUtils.isNotEmpty( descrali ) ) {
			nombre = nombre + StringUtils.trimToEmpty( descrali );
		}
		return nombre;
	}

	public String getCodigoIdioma() {
		return this.idioma.getCodigo();
	}

	public Contacto getContactoDefecto() {
		Contacto contactoDefecto = null;
		if ( CollectionUtils.isNotEmpty( contactos ) ) {
			for ( Contacto contacto : contactos ) {
				if ( contacto.getDefecto().booleanValue() ) {
					contactoDefecto = contacto;
					break;
				}
			}
		}
		return contactoDefecto;
	}

	public List< Contacto > getContactosGestores() {
		List< Contacto > listaContactosGestores = null;
		if ( CollectionUtils.isNotEmpty( contactos ) ) {
			listaContactosGestores = new ArrayList< Contacto >();
			for ( Contacto contacto : contactos ) {
				if ( contacto.getGestor().booleanValue() ) {
					listaContactosGestores.add( contacto );
				}
			}
		}
		return listaContactosGestores;
	}

	/**
	 * @return the tmct0ali
	 */
	public Tmct0ali getTmct0ali() {
		return tmct0ali;
	}

	public boolean isFacturable() {
		boolean facturable = false;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			if ( tmct0ali.getFacturar().equals( 'S' ) ) {
				facturable = true;
			}
		}
		return facturable;
	}

	public boolean isActivo() {
		boolean activo = false;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			if ( tmct0ali.getFhfinal().intValue() == Tmct0ali.fhfinActivos ) {
				activo = true;
			}
		}
		return activo;
	}

	public Tmct0aliId getTmct0aliId() {
		Tmct0aliId tmct0aliId = null;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			tmct0aliId = tmct0ali.getId();
		}
		return tmct0aliId;
	}

	public String getCdbrocli() {
		String cdbrocli = null;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			cdbrocli = tmct0ali.getCdbrocli();
		}
		return cdbrocli;
	}

	public String getCdaliass() {
		String cdaliass = null;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			cdaliass = tmct0ali.getCdaliass();
		}
		return cdaliass;
	}

	public BigDecimal getNumsec() {
		BigDecimal numsec = null;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			numsec = tmct0ali.getNumsec();
		}
		return numsec;
	}

	public String getDescrali() {
		String descrali = null;
		final Tmct0ali tmct0ali = this.getTmct0ali();
		if ( tmct0ali != null ) {
			descrali = tmct0ali.getDescrali();
		}
		return descrali;
	}

	public String getDireccionCompleta() {
		String direccionCompleta = null;
		final AliasDireccion aliasDireccion = getAliasDireccion();
		if ( aliasDireccion != null ) {
			direccionCompleta = aliasDireccion.getDireccionCompleta();
		}
		return direccionCompleta;
	}

	/**
	 * @param tmct0ali
	 *            the tmct0ali to set
	 */
	public void setTmct0ali( Tmct0ali tmct0ali ) {
		this.tmct0ali = tmct0ali;
	}

	/**
	 * @return the contactos
	 */
	public List< Contacto > getContactos() {
		return contactos;
	}

	/**
	 * @param contactos
	 *            the contactos to set
	 */
	public void setContactos( List< Contacto > contactos ) {
		this.contactos = contactos;
	}

	/**
	 * @return the aliasSubcuentas
	 */
	public List< AliasSubcuenta > getAliasSubcuentas() {
		return aliasSubcuentas;
	}

	/**
	 * @param aliasSubcuentas the aliasSubcuentas to set
	 */
	public void setAliasSubcuentas( List< AliasSubcuenta > aliasSubcuentas ) {
		this.aliasSubcuentas = aliasSubcuentas;
	}

	/**
	 * @return the alias
	 */
	public List< Alias > getAliasFacturables() {
		return aliasFacturables;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAliasFacturables( List< Alias > aliasFacturables ) {
		this.aliasFacturables = aliasFacturables;
	}

	/**
	 * @return the aliasDireccion
	 */
	public AliasDireccion getAliasDireccion() {
		if ( fieldHandler != null ) {
			this.aliasDireccion = ( AliasDireccion ) fieldHandler.readObject( this, "aliasDireccion", aliasDireccion );
		}
		return aliasDireccion;
	}

	/**
	 * @param aliasDireccion
	 *            the aliasDireccion to set
	 */
	public void setAliasDireccion( AliasDireccion aliasDireccion ) {
		if ( fieldHandler != null ) {
			this.aliasDireccion = ( AliasDireccion ) fieldHandler.writeObject( this, "aliasDireccion", this.aliasDireccion, aliasDireccion );
		} else {
			this.aliasDireccion = aliasDireccion;
		}
	}

	/**
	 * @return the aliasFacturacion
	 */
	public Alias getAliasFacturacion() {
		return aliasFacturacion;
	}

	/**
	 * @param aliasFacturacion
	 *            the aliasFacturacion to set
	 */
	public void setAliasFacturacion( Alias aliasFacturacion ) {
		this.aliasFacturacion = aliasFacturacion;
	}

	/**
	 * @return
	 */
	public AliasRecordatorio getAliasRecordatorio() {
		if ( fieldHandler != null ) {
			this.aliasRecordatorio = ( AliasRecordatorio ) fieldHandler.readObject( this, "aliasRecordatorio", aliasRecordatorio );
		}
		return aliasRecordatorio;
	}

	/**
	 * @param aliasRecordatorio
	 */
	public void setAliasRecordatorio( AliasRecordatorio aliasRecordatorio ) {
		if ( fieldHandler != null ) {
			this.aliasRecordatorio = ( AliasRecordatorio ) fieldHandler.writeObject( this, "aliasRecordatorio", this.aliasRecordatorio,
					aliasRecordatorio );
		} else {
			this.aliasRecordatorio = aliasRecordatorio;
		}

	}

	/**
	 * @return the idioma
	 */
	public Idioma getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma the idioma to set
	 */
	public void setIdioma( Idioma idioma ) {
		this.idioma = idioma;
	}
}
