package sibbac.business.fase0.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.TiposGarantiaExigida;

@Repository
public interface TiposGarantiaExigidaDao extends JpaRepository< TiposGarantiaExigida, Long > {

}
