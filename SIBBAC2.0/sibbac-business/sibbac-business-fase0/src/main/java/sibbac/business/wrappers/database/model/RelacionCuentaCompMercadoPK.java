package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Embeddable
public class RelacionCuentaCompMercadoPK implements Serializable {

	@ManyToOne
	@JoinColumn( name = "ID_MERCADO", referencedColumnName = "id" )
	private Tmct0Mercado			idMercado;
	@ManyToOne
	@JoinColumn( name = "ID_CUENTA_COMPENSACION", referencedColumnName = "ID_CUENTA_COMPENSACION" )
	private Tmct0CuentasDeCompensacion	idCtaCompensacion;

	public RelacionCuentaCompMercadoPK() {
		//
	}

	public RelacionCuentaCompMercadoPK( Tmct0Mercado mercado, Tmct0CuentasDeCompensacion cta ) {
		this.idMercado = mercado;
		this.idCtaCompensacion = cta;
	}

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	@Override
	public boolean equals( Object other ) {
		if ( this == other )
			return true;
		if ( !( other instanceof RelacionCuentaCompMercadoPK ) )
			return false;
		RelacionCuentaCompMercadoPK castOther = ( RelacionCuentaCompMercadoPK ) other;
		return idCtaCompensacion.getIdCuentaCompensacion() == ( castOther.idCtaCompensacion.getIdCuentaCompensacion() ) && idMercado.getId() == castOther.idMercado.getId();
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idCtaCompensacion.hashCode();
		hash = hash * prime + this.idMercado.hashCode();
		return hash;
	}

	public Tmct0Mercado getIdMercado() {
		return idMercado;
	}

	public void setIdMercado( Tmct0Mercado idMercado ) {
		this.idMercado = idMercado;
	}

	public Tmct0CuentasDeCompensacion getIdCtaCompensacion() {
		return idCtaCompensacion;
	}

	public void setIdCtaCompensacion(Tmct0CuentasDeCompensacion idCtaCompensacion) {
		this.idCtaCompensacion = idCtaCompensacion;
	}
	

}
