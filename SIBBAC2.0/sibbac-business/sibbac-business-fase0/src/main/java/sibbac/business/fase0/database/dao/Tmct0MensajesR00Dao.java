package sibbac.business.fase0.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0MensajesR00;


@Repository
public interface Tmct0MensajesR00Dao extends JpaRepository< Tmct0MensajesR00, Long > {

}
