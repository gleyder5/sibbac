package sibbac.business.wrappers.database.data;


import java.io.Serializable;

import sibbac.business.wrappers.database.model.TipoCuentaConciliacion;


public class TipoCuentaConciliacionData implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private long				id;
	private String				name;

	public TipoCuentaConciliacionData() {
		//
	}

	public TipoCuentaConciliacionData( TipoCuentaConciliacion cta ) {
		this.id = cta.getId();
		this.name = cta.getName();
	}

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

}
