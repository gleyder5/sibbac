package sibbac.business.wrappers.database.model;


import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table( name = "TMCT0MEN" )
public class Tmct0men implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7382602618392121755L;
	private String				cdmensa;
	private Tmct0sta			tmct0sta;
	private String				dsmensa;
	private int					nuenvio;
	private Date				femensa;
	private Date				hrmensa;
	private String				cdversio;
	private String				cdauxili;
	private String				urlpath;
	private String				hostPermitidos;
	private String				nfile;
	private String				cdusuaud;
	private Date				fhaudit;
	private int					nuversion;
	private Time				horaInicio;
	private Time				horaFin;
	private Date				cdUserSessionOpen;
	private Character			cdGeneralPtiSessionStatus;
	private List< Tmct0msc >	tmct0mscs			= new ArrayList< Tmct0msc >( 0 );

	public Tmct0men() {
	}

	public Tmct0men( String cdmensa, Tmct0sta tmct0sta, String dsmensa, int nuenvio, Date femensa, Date hrmensa, String cdversio,
			String cdauxili, String urlpath, String cdusuaud, Date fhaudit, int nuversion ) {
		this.cdmensa = cdmensa;
		this.tmct0sta = tmct0sta;
		this.dsmensa = dsmensa;
		this.nuenvio = nuenvio;
		this.femensa = femensa;
		this.hrmensa = hrmensa;
		this.cdversio = cdversio;
		this.cdauxili = cdauxili;
		this.urlpath = urlpath;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
	}

	public Tmct0men( String cdmensa, Tmct0sta tmct0sta, String dsmensa, int nuenvio, Date femensa, Date hrmensa, String cdversio,
			String cdauxili, String urlpath, String nfile, String cdusuaud, Date fhaudit, int nuversion, Date cdUserSessionOpen,
			Character cdGeneralPtiSessionStatus, List< Tmct0msc > tmct0mscs ) {
		this.cdmensa = cdmensa;
		this.tmct0sta = tmct0sta;
		this.dsmensa = dsmensa;
		this.nuenvio = nuenvio;
		this.femensa = femensa;
		this.hrmensa = hrmensa;
		this.cdversio = cdversio;
		this.cdauxili = cdauxili;
		this.urlpath = urlpath;
		this.nfile = nfile;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
		this.cdUserSessionOpen = cdUserSessionOpen;
		this.cdGeneralPtiSessionStatus = cdGeneralPtiSessionStatus;
		this.tmct0mscs = tmct0mscs;
	}

	@Id
	@Column( name = "CDMENSA", unique = true, nullable = false, length = 3 )
	public String getCdmensa() {
		return this.cdmensa;
	}

	public void setCdmensa( String cdmensa ) {
		this.cdmensa = cdmensa;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( {
			@JoinColumn( name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false ),
			@JoinColumn( name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false )
	} )
	public Tmct0sta getTmct0sta() {
		return this.tmct0sta;
	}

	public void setTmct0sta( Tmct0sta tmct0sta ) {
		this.tmct0sta = tmct0sta;
	}

	@Column( name = "DSMENSA", nullable = false, length = 50 )
	public String getDsmensa() {
		return this.dsmensa;
	}

	public void setDsmensa( String dsmensa ) {
		this.dsmensa = dsmensa;
	}

	@Column( name = "NUENVIO", nullable = false, precision = 5 )
	public int getNuenvio() {
		return this.nuenvio;
	}

	public void setNuenvio( int nuenvio ) {
		this.nuenvio = nuenvio;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEMENSA", nullable = false, length = 10 )
	public Date getFemensa() {
		return this.femensa;
	}

	public void setFemensa( Date femensa ) {
		this.femensa = femensa;
	}

	@Temporal( TemporalType.TIME )
	@Column( name = "HRMENSA", nullable = false, length = 8 )
	public Date getHrmensa() {
		return this.hrmensa;
	}

	public void setHrmensa( Date hrmensa ) {
		this.hrmensa = hrmensa;
	}

	@Column( name = "CDVERSIO", nullable = false, length = 5 )
	public String getCdversio() {
		return this.cdversio;
	}

	public void setCdversio( String cdversio ) {
		this.cdversio = cdversio;
	}

	@Column( name = "CDAUXILI", nullable = false, length = 20 )
	public String getCdauxili() {
		return this.cdauxili;
	}

	public void setCdauxili( String cdauxili ) {
		this.cdauxili = cdauxili;
	}

	@Column( name = "URLPATH", nullable = false, length = 128 )
	public String getUrlpath() {
		return this.urlpath;
	}

	public void setUrlpath( String urlpath ) {
		this.urlpath = urlpath;
	}

	@Column( name = "NFILE", length = 50 )
	public String getNfile() {
		return this.nfile;
	}

	public void setNfile( String nfile ) {
		this.nfile = nfile;
	}

	@Column( name = "CDUSUAUD", nullable = false, length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", nullable = false, length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "NUVERSION", nullable = false )
	public int getNuversion() {
		return this.nuversion;
	}

	public void setNuversion( int nuversion ) {
		this.nuversion = nuversion;
	}

	@Temporal( TemporalType.TIME )
	@Column( name = "CD_USER_SESSION_OPEN", length = 8 )
	public Date getCdUserSessionOpen() {
		return this.cdUserSessionOpen;
	}

	public void setCdUserSessionOpen( Date cdUserSessionOpen ) {
		this.cdUserSessionOpen = cdUserSessionOpen;
	}

	@Column( name = "CD_GENERAL_PTI_SESSION_STATUS", length = 1 )
	public Character getCdGeneralPtiSessionStatus() {
		return this.cdGeneralPtiSessionStatus;
	}

	public void setCdGeneralPtiSessionStatus( Character cdGeneralPtiSessionStatus ) {
		this.cdGeneralPtiSessionStatus = cdGeneralPtiSessionStatus;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0men" )
	public List< Tmct0msc > getTmct0mscs() {
		return this.tmct0mscs;
	}

	public void setTmct0mscs( List< Tmct0msc > tmct0mscs ) {
		this.tmct0mscs = tmct0mscs;
	}
	@Column( name = "HOSTPERMITIDOS", nullable = true, length = 255 )
	public String getHostPermitidos() {
		return hostPermitidos;
	}

	public void setHostPermitidos(String hostPermitidos) {
		this.hostPermitidos = hostPermitidos;
	}


	public Time getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Time horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Time getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(Time horaFin) {
		this.horaFin = horaFin;
	}
}
