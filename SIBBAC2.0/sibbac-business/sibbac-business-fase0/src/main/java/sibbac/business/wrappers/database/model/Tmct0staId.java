package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0staId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -5100917571457022895L;
	private String				cdestado			= "";
	private String				cdtipest			= "";

	public Tmct0staId() {
	}

	public Tmct0staId( String cdestado, String cdtipest ) {
		this.cdestado = cdestado;
		this.cdtipest = cdtipest;
	}

	@Column( name = "CDESTADO", nullable = false, length = 3 )
	public String getCdestado() {
		return this.cdestado;
	}

	public void setCdestado( String cdestado ) {
		this.cdestado = cdestado;
	}

	@Column( name = "CDTIPEST", nullable = false, length = 10 )
	public String getCdtipest() {
		return this.cdtipest;
	}

	public void setCdtipest( String cdtipest ) {
		this.cdtipest = cdtipest;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0staId ) )
			return false;
		Tmct0staId castOther = ( Tmct0staId ) other;

		return ( ( this.getCdestado() == castOther.getCdestado() ) || ( this.getCdestado() != null && castOther.getCdestado() != null && this
				.getCdestado().equals( castOther.getCdestado() ) ) )
				&& ( ( this.getCdtipest() == castOther.getCdtipest() ) || ( this.getCdtipest() != null && castOther.getCdtipest() != null && this
						.getCdtipest().equals( castOther.getCdtipest() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getCdestado() == null ? 0 : this.getCdestado().hashCode() );
		result = 37 * result + ( getCdtipest() == null ? 0 : this.getCdtipest().hashCode() );
		return result;
	}

}
