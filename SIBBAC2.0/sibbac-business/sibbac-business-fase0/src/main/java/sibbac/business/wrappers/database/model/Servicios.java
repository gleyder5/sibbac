package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.database.DBConstants;
import sibbac.tasks.database.Job;


/**
 * Entity: "Servicios".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = DBConstants.SERVICIOS.SERVICIOS )
@Entity
@XmlRootElement
@Audited
public class Servicios {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties
	/**
	 * The entity id.
	 */
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column( name = "ID", nullable = false, unique = true )
	@XmlAttribute
	private Long	id;

	/** The nombre servicio. */
	@Column( name = "NOMBRESERVICIO", nullable = false, unique = true, length = 20 )
	@XmlAttribute
	private String	nombreServicio;

	/** The descripcion servicio. */
	@Column( name = "DESCRIPCIONSERVICIO", length = 255 )
	@XmlAttribute
	private String	descripcionServicio;

	/** The activo. */
	@Column( name = "ACTIVO" )
	@XmlAttribute
	private Boolean	activo;

	/** The configuracion alias. */
	@Column( name = "CONFIGURACIONALIAS" )
	private Boolean	configuracionAlias;

	/** The configuracion contactos. */
	@Column( name = "CONFIGURACIONCONTACTOS" )
	private Boolean	configuracionContactos;

	@OneToOne( fetch = FetchType.LAZY, optional = true )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@JoinColumns( value = {
			@JoinColumn( name = "JOB_GROUP", referencedColumnName = "JOB_GROUP", nullable = true ),
			@JoinColumn( name = "JOB_NAME", referencedColumnName = "JOB_NAME", nullable = true )
	} )
	private Job		job;

	// ----------------------------------------------- Bean Constructors

	/**
	 * Instantiates a new servicios.
	 */
	public Servicios() {
		configuracionAlias = Boolean.FALSE;
		configuracionContactos = Boolean.FALSE;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Getter for getNombreServicio.
	 *
	 * @return the getNombreServicio
	 */
	public String getNombreServicio() {
		return this.nombreServicio;
	}

	/**
	 * Getter for descripcionServicio.
	 *
	 * @return the descripcionServicio
	 */
	public String getDescripcionServicio() {
		return this.descripcionServicio;
	}

	/**
	 * Getter for activo.
	 *
	 * @return the activo
	 */
	public Boolean getActivo() {
		return this.activo;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for id.
	 *
	 * @param id the id to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	/**
	 * Setter for nombreSerivcio.
	 *
	 * @param nombreServicio the new nombre servicio
	 */
	public void setNombreServicio( String nombreServicio ) {
		this.nombreServicio = nombreServicio;
	}

	/**
	 * Setter for descripcionServicio.
	 *
	 * @param descripcionServicio the descripcionServicio to set
	 */
	public void setDescripcionServicio( String descripcionServicio ) {
		this.descripcionServicio = descripcionServicio;
	}

	/**
	 * Setter for activo.
	 *
	 * @param activo the activo to set
	 */
	public void setActivo( boolean activo ) {
		this.activo = activo;
	}

	/**
	 * Gets the configuracion alias.
	 *
	 * @return the configuracion alias
	 */
	public Boolean getConfiguracionAlias() {
		return configuracionAlias;
	}

	/**
	 * Sets the configuracion alias.
	 *
	 * @param configuracionAlias the new configuracion alias
	 */
	public void setConfiguracionAlias( Boolean configuracionAlias ) {
		this.configuracionAlias = configuracionAlias;
	}

	/**
	 * Gets the configuracion contactos.
	 *
	 * @return the configuracion contactos
	 */
	public Boolean getConfiguracionContactos() {
		return configuracionContactos;
	}

	/**
	 * Sets the configuracion contactos.
	 *
	 * @param configuracionContactos the new configuracion contactos
	 */
	public void setConfiguracionContactos( Boolean configuracionContactos ) {
		this.configuracionContactos = configuracionContactos;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Servicios [id=" + id + ", nombreServicio=" + nombreServicio + ", descripcionServicio=" + descripcionServicio + ", activo="
				+ activo + ", configuracionAlias=" + configuracionAlias + ", configuracionContactos=" + configuracionContactos + "]";
	}

}
