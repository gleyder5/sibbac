package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0PKC. Confirmation sent to broker Booking
 * Documentación para TMCT0PKC <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0PKC")
public class Tmct0pkc implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 2531754704546302063L;

  @AttributeOverrides({
      @AttributeOverride(name = "pkbroker", column = @Column(name = "PKBROKER", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER", nullable = false, length = 20)),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)) })
  @EmbeddedId
  private Tmct0pkcId id;

  /**
   * Relation 1..1 with table TMCT0PKB. Constraint de relación entre TMCT0PKC y
   * TMCT0PKB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PKBROKER", nullable = false, insertable = false, updatable = false)
  protected Tmct0pkb tmct0pkb;
  /**
   * Relation 1..n with table TMCT0PKD. Constraint de relación entre TMCT0PKD y
   * TMCT0PKC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0pkc")
  protected List<Tmct0pkd> tmct0pkds = new ArrayList<Tmct0pkd>(0);

  /**
   * Table Field DSCRIPT. Description Documentación para DSCRIPT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "DSCRIPT", length = 40)
  protected java.lang.String dscript;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;
  
  

  public static long getSerialversionuid() {
    return serialVersionUID;
  }



  public Tmct0pkcId getId() {
    return id;
  }



  public Tmct0pkb getTmct0pkb() {
    return tmct0pkb;
  }



  public List<Tmct0pkd> getTmct0pkds() {
    return tmct0pkds;
  }



  public java.lang.String getDscript() {
    return dscript;
  }



  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }



  public Date getFhaudit() {
    return fhaudit;
  }



  public java.lang.Integer getNuversion() {
    return nuversion;
  }



  public void setId(Tmct0pkcId id) {
    this.id = id;
  }



  public void setTmct0pkb(Tmct0pkb tmct0pkb) {
    this.tmct0pkb = tmct0pkb;
  }



  public void setTmct0pkds(List<Tmct0pkd> tmct0pkds) {
    this.tmct0pkds = tmct0pkds;
  }



  public void setDscript(java.lang.String dscript) {
    this.dscript = dscript;
  }



  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }



  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }



  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }



  @Override
  public String toString() {
    return String.format("Tmct0pkc [id=%s]", this.id);
  }

}
