package sibbac.business.canones.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Column;

@MappedSuperclass
public abstract class MercadoContratacionRefId implements Serializable {

  /**
   * Número de serie por defecto 
   */
  private static final long serialVersionUID = -1L;
  
  @Column(name = MercadoContratacion.REF_MERCADO, length = 4)
  private String mercadoContratacionId;
  
  public abstract String getIdField();
  
  public abstract void setIdField(String idField);
  
  public String getMercadoContratacionId() {
    return mercadoContratacionId;
  }
  
  public void setMercadoContratacionId(String mercadoContratacionId) {
    this.mercadoContratacionId = mercadoContratacionId;
  }
  
  @Override
  public String toString() {
    return String.format("id: {id: %s, mercadoContratacionId: %s}", getIdField(), mercadoContratacionId);
  }

}
