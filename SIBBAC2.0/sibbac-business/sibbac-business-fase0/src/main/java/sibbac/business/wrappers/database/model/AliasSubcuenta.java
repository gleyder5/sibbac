package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.facturacion.database.model.AliasPeriodicidad }. Entity:
 * "AliasPeriodicidad".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = WRAPPERS.ALIAS_SUBCUENTA )
@Entity
@XmlRootElement
@Audited
public class AliasSubcuenta extends ATableAudited< AliasSubcuenta > implements java.io.Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -9049049693440140778L;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ALIAS_FACTURACION", referencedColumnName = "ID", nullable = false )
	@XmlAttribute
	private Alias				aliasFacturacion;

	@OneToOne( optional = false )
	@JoinColumns( {
			@JoinColumn( name = "CDBROCLI", referencedColumnName = "CDBROCLI", nullable = false ),
			@JoinColumn( name = "CDALIASS", referencedColumnName = "CDALIASS", nullable = false ),
			@JoinColumn( name = "CDSUBCTA", referencedColumnName = "CDSUBCTA", nullable = false ),
			@JoinColumn( name = "NUMSEC", referencedColumnName = "NUMSEC", nullable = false )
	} )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	private Tmct0act			tmct0act;

	// ----------------------------------------------- Bean
	// Constructors---------------------

	protected AliasSubcuenta() {
		super();
	}

	public AliasSubcuenta( Alias aliasFacturacion, Tmct0act tmct0act ) {
		this();
		this.aliasFacturacion = aliasFacturacion;
		this.tmct0act = tmct0act;
	}

	public String getCdbrocli() {
		String cdbrocli = null;
		final Tmct0act tmct0act = this.getTmct0act();
		if ( tmct0act != null ) {
			cdbrocli = tmct0act.getCdbrocli();
		}
		return cdbrocli;
	}

	public String getCdaliass() {
		String cdaliass = null;
		final Tmct0act tmct0act = this.getTmct0act();
		if ( tmct0act != null ) {
			cdaliass = tmct0act.getCdaliass();
		}
		return cdaliass;
	}

	public String getCdsubcta() {
		String cdsubcta = null;
		final Tmct0act tmct0act = this.getTmct0act();
		if ( tmct0act != null ) {
			cdsubcta = tmct0act.getCdsubcta();
		}
		return cdsubcta;
	}

	public BigDecimal getNumsec() {
		BigDecimal numsec = null;
		final Tmct0act tmct0act = this.getTmct0act();
		if ( tmct0act != null ) {
			numsec = tmct0act.getNumsec();
		}
		return numsec;
	}

	public String getDescrali() {
		String descrali = null;
		final Tmct0act tmct0act = this.getTmct0act();
		if ( tmct0act != null ) {
			descrali = tmct0act.getDescrali();
		}
		return descrali;
	}

	/**
	 * @return the tmct0act
	 */
	public Tmct0act getTmct0act() {
		return tmct0act;
	}

	/**
	 * @param tmct0act
	 *            the tmct0act to set
	 */
	public void setTmct0act( Tmct0act tmct0act ) {
		this.tmct0act = tmct0act;
	}

	/**
	 * @return the aliasFacturacion
	 */
	public Alias getAliasFacturacion() {
		return aliasFacturacion;
	}

	/**
	 * @param aliasFacturacion
	 *            the aliasFacturacion to set
	 */
	public void setAliasFacturacion( Alias aliasFacturacion ) {
		this.aliasFacturacion = aliasFacturacion;
	}

}
