package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0MOVIMIENTOOPERACIONNUEVAECC")
public class Tmct0movimientooperacionnuevaecc implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 5744385632651441602L;
  private Long idmovimientooperacionnuevaec;

  private Tmct0movimientoecc tmct0movimientoecc;
  private Tmct0desglosecamara tmct0desglosecamara;
  
  private Tmct0desgloseclitit tmct0desgloseclitit;

  private String cdoperacionecc;
  private String nuoperacionprev;
  private String nuoperacioninic;
  private BigDecimal imtitulos;
  private BigDecimal imefectivo;
  private Date auditFechaCambio;
  private String auditUser;
  private BigDecimal imprecio;
  private Long nudesglose;
  private String cdmiembromkt;

  private Long idMovimientoFidessa;

  private Long idejecucionalocation;

  private BigDecimal corretaje = BigDecimal.ZERO;

  public Tmct0movimientooperacionnuevaecc() {

  }

  public Tmct0movimientooperacionnuevaecc(long idmovimientooperacionnuevaec, Tmct0movimientoecc tmct0movimientoecc) {
    this.idmovimientooperacionnuevaec = idmovimientooperacionnuevaec;
    this.tmct0movimientoecc = tmct0movimientoecc;
  }

  public Tmct0movimientooperacionnuevaecc(long idmovimientooperacionnuevaec,
                                          Tmct0desglosecamara tmct0desglosecamara,
                                          Tmct0movimientoecc tmct0movimientoecc,
                                          String cdoperacionecc,
                                          String nuoperacionprev,
                                          String nuoperacioninic,
                                          BigDecimal imtitulos,
                                          BigDecimal imefectivo,
                                          Date auditFechaCambio,
                                          String auditUser,
                                          BigDecimal imprecio,
                                          Long nudesglose,
                                          String cdmiembromkt) {
    this.idmovimientooperacionnuevaec = idmovimientooperacionnuevaec;
    this.tmct0desglosecamara = tmct0desglosecamara;
    this.tmct0movimientoecc = tmct0movimientoecc;
    this.cdoperacionecc = cdoperacionecc;
    this.nuoperacionprev = nuoperacionprev;
    this.nuoperacioninic = nuoperacioninic;
    this.imtitulos = imtitulos;
    this.imefectivo = imefectivo;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.imprecio = imprecio;
    this.nudesglose = nudesglose;
    this.cdmiembromkt = cdmiembromkt;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDMOVIMIENTOOPERACIONNUEVAEC", unique = true, nullable = false)
  public Long getIdmovimientooperacionnuevaec() {
    return this.idmovimientooperacionnuevaec;
  }

  public void setIdmovimientooperacionnuevaec(Long idmovimientooperacionnuevaec) {
    this.idmovimientooperacionnuevaec = idmovimientooperacionnuevaec;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDMOVIMIENTO", nullable = false)
  public Tmct0movimientoecc getTmct0movimientoecc() {
    return this.tmct0movimientoecc;
  }

  public void setTmct0movimientoecc(Tmct0movimientoecc tmct0movimientoecc) {
    this.tmct0movimientoecc = tmct0movimientoecc;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDDESGLOSECAMARA", nullable = true)
  public Tmct0desglosecamara getTmct0desglosecamara() {
    return this.tmct0desglosecamara;
  }

  public void setTmct0desglosecamara(Tmct0desglosecamara tmct0desglosecamara) {
    this.tmct0desglosecamara = tmct0desglosecamara;
  }

  @Column(name = "CDOPERACIONECC", length = 16)
  public String getCdoperacionecc() {
    return this.cdoperacionecc;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  @Column(name = "NUOPERACIONPREV", length = 16)
  public String getNuoperacionprev() {
    return this.nuoperacionprev;
  }

  public void setNuoperacionprev(String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  @Column(name = "NUOPERACIONINIC", length = 16)
  public String getNuoperacioninic() {
    return this.nuoperacioninic;
  }

  public void setNuoperacioninic(String nuoperacioninic) {
    this.nuoperacioninic = nuoperacioninic;
  }

  @Column(name = "IMTITULOS", precision = 18, scale = 6)
  public BigDecimal getImtitulos() {
    return this.imtitulos;
  }

  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  @Column(name = "IMEFECTIVO", precision = 13)
  public BigDecimal getImefectivo() {
    return this.imefectivo;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "IMPRECIO", precision = 18, scale = 8)
  public BigDecimal getImprecio() {
    return this.imprecio;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  @Column(name = "NUDESGLOSE")
  public Long getNudesglose() {
    return this.nudesglose;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  @Column(name = "CDMIEMBROMKT", length = 11)
  public String getCdmiembromkt() {
    return this.cdmiembromkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  @Column(name = "ID_MOVIMIENTO_FIDESSA", nullable = true)
  public Long getIdMovimientoFidessa() {
    return this.idMovimientoFidessa;
  }

  public void setIdMovimientoFidessa(Long idMovimientoFidessa) {
    this.idMovimientoFidessa = idMovimientoFidessa;
  }

  @Column(nullable = true)
  public Long getIdejecucionalocation() {
    return idejecucionalocation;
  }

  @Column(name = "CORRETAJE", length = 18, scale = 8, nullable = true)
  public BigDecimal getCorretaje() {
    return corretaje;
  }

  public void setIdejecucionalocation(Long idejecucionalocation) {
    this.idejecucionalocation = idejecucionalocation;
  }

  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }

  @Transient
  public Tmct0desgloseclitit getTmct0desgloseclitit() {
    return tmct0desgloseclitit;
  }

  public void setTmct0desgloseclitit(Tmct0desgloseclitit tmct0desgloseclitit) {
    this.tmct0desgloseclitit = tmct0desgloseclitit;
  }

}
