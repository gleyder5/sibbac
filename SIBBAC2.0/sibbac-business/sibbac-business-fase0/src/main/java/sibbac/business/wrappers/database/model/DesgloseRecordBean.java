package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import sibbac.database.DBConstants.WRAPPERS;



@Table( name = WRAPPERS.DESGLOSE_RECORD_BEAN )
@Entity
public class DesgloseRecordBean extends OperacionEspecialRecordBean {

	/**
	 * 
	 */
	private static final long			serialVersionUID	= 2660563472424978151L;

	@Column( name = "ID", length = 8 ,insertable =false, updatable=false)
	private Long						id;
	@Column( name = "TIPOREG", length = 2 ,insertable =false, updatable=false)
	private Integer						tipoReg;
	@Column( name = "CODEMPRL", length = 4 )
	private String						codEmprl;
	@Column( name = "EMPRCTVA", length = 4 )
	private String						emprCTVA;
	@Column( name = "CENTCTVA", length = 4 )
	private String						centCTVA;
	@Column( name = "DPTECTVA", length = 7 )
	private String						dpteCTVA;
	@Column( name = "NUMORDEN", length = 20, nullable = false )
	private String						numeroOrden;
	@Column( name = "NUREFORD", length = 10 )
	private String						numeroReferenciaOrden;
	@Column( name = "NUCNFLIQ", precision = 4 )
	private Short						nucnfliq;
	@Column( name = "FECHA_PROCESAMIENTO", nullable = false )
	@Temporal( TemporalType.TIMESTAMP )
	private Date						fechaProcesamiento;
	@Column( name = "OBCSAVB", length = 4 )
	private String						codigoSV;
	@Column( name = "FEJEC" )
	@Temporal( TemporalType.DATE )
	private Date						fechaEjecucion;
	@Column( name = "CODORGEJ", length = 3 )
	private String						codigoBolsaEjecucion;
	@Column( name = "CODVAISO", length = 12 )
	private String						codigoValorISO;
	@Transient
	private String						filler1;
	@Column( name = "TIPOPERAC", length = 1 )
	private TipoOperacion				tipoOperacion;
	@Column( name = "TIPCAMBI", length = 1 )
	private TipoCambio					tipoCambio;
	@Column( name = "ACCEJEC", precision = 11 )
	private BigDecimal					titulosEjecutados;
	@Column( name = "NOMINEJ", precision = 15, scale = 2 )
	private BigDecimal					nominalEjecutado;
	@Column( name = "CBOEJECU", precision = 10, scale = 4 )
	private BigDecimal					cambioOperacion;
	@Column( name = "DENOMENA", length = 32 )
	private String						descripcionValor;
	@Column( name = "NIF_BIC", length = 11 )
	private String						bicTitular;
	@Column( name = "TIPSALDO", length = 1 )
	private String						tipoSaldo;
	@Column( name = "REFMOD", length = 35 )
	private String						referenciaModelo;
	@Column( name = "SEGCLI", length = 3 )
	private String						segmento;
	
	@Column( name = "ORDCOM", length = 4 )
	private String				ordenComercial;
	@Column( name = "PROCESADO", nullable = false )
	private Integer						porcesado;
	@Transient
	private String						filler2;

	@OneToMany( mappedBy = "desgloseRecordBean", fetch=FetchType.LAZY )
	private Set< PartenonRecordBean >	titulares;
	
	//private OrdenComercial				ordenComercialAux;
	
	public DesgloseRecordBean()
	{
		super();
	}
	
	public DesgloseRecordBean(Integer tipoReg, String refre, BigDecimal titulos, BigDecimal nominej, String entidad, String empConVal, String numCtoOficina, 
							  String numCtrVal, String numOrden, String codSV, String isin, String desIsin, Date fejecucion, String bolsa, Character tipoOperacion, 
							  BigDecimal cambioOperacion, String nif_bic,  Character tipoSaldo, String refMod, String segmento, String ordCom, String tipoCambio) {
		super();
		this.tipoReg = tipoReg;
		this.setTipoRegistro(tipoReg.toString());
		this.numeroReferenciaOrden = refre;
		this.titulosEjecutados = titulos;
		this.nominalEjecutado = nominej;
		this.codEmprl = entidad;
		this.emprCTVA = empConVal;
		this.centCTVA = numCtoOficina;
		this.dpteCTVA = numCtrVal;
		this.numeroOrden = numOrden;
		this.codigoSV = codSV;
		this.codigoValorISO = isin;
		this.descripcionValor = desIsin;
		this.fechaEjecucion = fejecucion;
		this.codigoBolsaEjecucion = bolsa;
		this.setTipoOperacion(TipoOperacion.getTipoOperacion( tipoOperacion.toString() ));
		this.cambioOperacion = cambioOperacion;
		//PartenonRecordBean partenonRecordBean = new PartenonRecordBean();
		//partenonRecordBean.setNifbic(nif_bic);
		//this.titulares.add(partenonRecordBean);
		this.bicTitular = nif_bic;
		this.setTipoSaldo( tipoSaldo.toString());
		this.referenciaModelo = refMod;
		this.segmento = segmento;
		//this.ordenComercial = OrdenComercial.getOrdenComercial( ordCom );
		this.ordenComercial = ordCom;
		this.tipoCambio = TipoCambio.getTipoCambio(tipoCambio);
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param tipoReg the id to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	/**
	 * @return the tipoReg
	 */
	public Integer getTipoReg() {
		return tipoReg;
	}

	/**
	 * @param tipoReg the tipoReg to set
	 */
	public void setTipoReg( Integer tipoReg ) {
		this.tipoReg = tipoReg;
	}
	
	/**
	 * @return the codEmprl
	 */
	public String getCodEmprl() {
		return codEmprl;
	}

	/**
	 * @param codEmprl the codEmprl to set
	 */
	public void setCodEmprl( String codEmprl ) {
		this.codEmprl = codEmprl;
	}

	/**
	 * @return the emprCTVA
	 */
	public String getEmprCTVA() {
		return emprCTVA;
	}

	/**
	 * @param emprCTVA the emprCTVA to set
	 */
	public void setEmprCTVA( String emprCTVA ) {
		this.emprCTVA = emprCTVA;
	}

	/**
	 * @return the centCTVA
	 */
	public String getCentCTVA() {
		return centCTVA;
	}

	/**
	 * @param centCTVA the centCTVA to set
	 */
	public void setCentCTVA( String centCTVA ) {
		this.centCTVA = centCTVA;
	}

	/**
	 * @return the dpteCTVA
	 */
	public String getDpteCTVA() {
		return dpteCTVA;
	}

	/**
	 * @param dpteCTVA the dpteCTVA to set
	 */
	public void setDpteCTVA( String dpteCTVA ) {
		this.dpteCTVA = dpteCTVA;
	}

	/**
	 * @return the numeroOrden
	 */
	public String getNumeroOrden() {
		return numeroOrden;
	}

	/**
	 * @param numeroOrden the numeroOrden to set
	 */
	public void setNumeroOrden( String numeroOrden ) {
		this.numeroOrden = numeroOrden;
	}

	/**
	 * @return the numeroReferenciaOrden
	 */
	public String getNumeroReferenciaOrden() {
		return numeroReferenciaOrden;
	}

	/**
	 * @param numeroReferenciaOrden the numeroReferenciaOrden to set
	 */
	public void setNumeroReferenciaOrden( String numeroReferenciaOrden ) {
		this.numeroReferenciaOrden = numeroReferenciaOrden;
	}

	/**
	 * @return the nucnfliq
	 */
	public Short getNucnfliq() {
		return nucnfliq;
	}

	/**
	 * @param nucnfliq the nucnfliq to set
	 */
	public void setNucnfliq( Short nucnfliq ) {
		this.nucnfliq = nucnfliq;
	}

	/**
	 * @return the fechaProcesamiento
	 */
	public Date getFechaProcesamiento() {
		return fechaProcesamiento;
	}

	/**
	 * @param fechaProcesamiento the fechaProcesamiento to set
	 */
	public void setFechaProcesamiento( Date fechaProcesamiento ) {
		this.fechaProcesamiento = fechaProcesamiento;
	}

	/**
	 * @return the codigoSV
	 */
	public String getCodigoSV() {
		return codigoSV;
	}

	/**
	 * @param codigoSV the codigoSV to set
	 */
	public void setCodigoSV( String codigoSV ) {
		this.codigoSV = codigoSV;
	}

	/**
	 * @return the fechaEjecucion
	 */
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	/**
	 * @param fechaEjecucion the fechaEjecucion to set
	 */
	public void setFechaEjecucion( Date fechaEjecucion ) {
		this.fechaEjecucion = fechaEjecucion;
	}

	/**
	 * @return the codigoBolsaEjecucion
	 */
	public String getCodigoBolsaEjecucion() {
		return codigoBolsaEjecucion;
	}

	/**
	 * @param codigoBolsaEjecucion the codigoBolsaEjecucion to set
	 */
	public void setCodigoBolsaEjecucion( String codigoBolsaEjecucion ) {
		this.codigoBolsaEjecucion = codigoBolsaEjecucion;
	}

	/**
	 * @return the codigoValorISO
	 */
	public String getCodigoValorISO() {
		return codigoValorISO;
	}

	/**
	 * @param codigoValorISO the codigoValorISO to set
	 */
	public void setCodigoValorISO( String codigoValorISO ) {
		this.codigoValorISO = codigoValorISO;
	}

	/**
	 * @return the filler1
	 */
	public String getFiller1() {
		return filler1;
	}

	/**
	 * @param filler1 the filler1 to set
	 */
	public void setFiller1( String filler1 ) {
		this.filler1 = filler1;
	}

	/**
	 * @return the tipoOperacion
	 */
	public TipoOperacion getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion( TipoOperacion tipoOperacion ) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * @return the tipoCambio
	 */
	public TipoCambio getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * @param tipoCambio the tipoCambio to set
	 */
	public void setTipoCambio( TipoCambio tipoCambio ) {
		this.tipoCambio = tipoCambio;
	}

	/**
	 * @return the titulosEjecutados
	 */
	public BigDecimal getTitulosEjecutados() {
		return titulosEjecutados;
	}

	/**
	 * @param titulosEjecutados the titulosEjecutados to set
	 */
	public void setTitulosEjecutados( BigDecimal titulosEjecutados ) {
		this.titulosEjecutados = titulosEjecutados;
	}

	/**
	 * @return the nominalEjecutado
	 */
	public BigDecimal getNominalEjecutado() {
		return nominalEjecutado;
	}

	/**
	 * @param nominalEjecutado the nominalEjecutado to set
	 */
	public void setNominalEjecutado( BigDecimal nominalEjecutado ) {
		this.nominalEjecutado = nominalEjecutado;
	}

	/**
	 * @return the cambioOperacion
	 */
	public BigDecimal getCambioOperacion() {
		return cambioOperacion;
	}

	/**
	 * @param cambioOperacion the cambioOperacion to set
	 */
	public void setCambioOperacion( BigDecimal cambioOperacion ) {
		this.cambioOperacion = cambioOperacion;
	}

	/**
	 * @return the descripcionValor
	 */
	public String getDescripcionValor() {
		return descripcionValor;
	}

	/**
	 * @param descripcionValor the descripcionValor to set
	 */
	public void setDescripcionValor( String descripcionValor ) {
		this.descripcionValor = descripcionValor;
	}

	/**
	 * @return the bicTitular
	 */
	public String getBicTitular() {
		return bicTitular;
	}

	/**
	 * @param bicTitular the bicTitular to set
	 */
	public void setBicTitular( String bicTitular ) {
		this.bicTitular = bicTitular;
	}

	/**
	 * @return the tipoSaldo
	 */
	public String getTipoSaldo() {
		return tipoSaldo;
	}

	/**
	 * @param tipoSaldo the tipoSaldo to set
	 */
	public void setTipoSaldo( String tipoSaldo ) {
		this.tipoSaldo = tipoSaldo;
	}

	/**
	 * @return the referenciaModelo
	 */
	public String getReferenciaModelo() {
		return referenciaModelo;
	}

	/**
	 * @param referenciaModelo the referenciaModelo to set
	 */
	public void setReferenciaModelo( String referenciaModelo ) {
		this.referenciaModelo = referenciaModelo;
	}

	/**
	 * @return the segmento
	 */
	public String getSegmento() {
		return segmento;
	}

	/**
	 * @param segmento the segmento to set
	 */
	public void setSegmento( String segmento ) {
		this.segmento = segmento;
	}

	/**
	 * @return the ordenComercial
	 */
	public String getOrdenComercial() {
		return ordenComercial;
	}

	/**
	 * @param ordenComercial the ordenComercial to set
	 */
	public void setOrdenComercial( String ordenComercial ) {
		this.ordenComercial = ordenComercial;
	}
	
//	public OrdenComercial getOrdenComercialAux(){
//	    return this.ordenComercialAux;
//	}
//	
//	public void setOrdenComercialAux(OrdenComercial ordenComercial){
//	    this.ordenComercialAux = ordenComercial;
//	}

	/**
	 * @return the filler2
	 */
	public String getFiller2() {
		return filler2;
	}

	/**
	 * @param filler2 the filler2 to set
	 */
	public void setFiller2( String filler2 ) {
		this.filler2 = filler2;
	}

	/**
	 * @return the porcesado
	 */
	public Integer getPorcesado() {
		return porcesado;
	}

	/**
	 * @param porcesado the porcesado to set
	 */
	public void setPorcesado( Integer porcesado ) {
		this.porcesado = porcesado;
	}

	/**
	 * @return the titulares
	 */
	public Set< PartenonRecordBean > getTitulares() {
		return titulares;
	}

	/**
	 * @param titulares the titulares to set
	 */
	public void setTitulares( Set< PartenonRecordBean > titulares ) {
		this.titulares = titulares;
	}

	public enum OrdenComercial {
		REINVERSION_DIVIDENDO( "23" ),
		BONUS_DIRECTIVO( "24" ),
		COMPRA_VENTA_ACCIONES( "25" ),
		CAMPANIAS( "26" ),
		CAMPANIAS_ESPECIALES( "28" ),
		BARRIDO_DERECHOS("30"),
		EJECUTADA_DEPOSITO_ESTRUCTURADO( "91" ),
		EJECUTADA_MEFF( "95" );

		private final String	ordenComercial;

		/**
		 * @param ordenComercial
		 */
		private OrdenComercial( final String ordenComercial ) {
		    String result = null;
		    if (ordenComercial != null){
			result = ordenComercial.toString();
		    }
		    this.ordenComercial = result;
		}

		/**
		 * @return the ordenComercial
		 */
		public String getOrdenComercial() {
			return ordenComercial;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return ordenComercial;
		}

		public static OrdenComercial getOrdenComercial( String tipoPersona ) {
			OrdenComercial[] fields = OrdenComercial.values();
			OrdenComercial oc = null;
			for ( int i = 0; i < fields.length; i++ ) {
				if ( fields[ i ].ordenComercial.equals( tipoPersona ) ) {
					oc = fields[ i ];
					break;
				}
			}
			return oc;
		}
	}

	public enum TipoOperacion {
		COMPRA_CONTADO( "1" ), VENTA_CONTADO( "2" );

		private final String	tipoOperacion;

		/**
		 * @param tipoOperacion
		 */
		private TipoOperacion( final String tipoOperacion ) {
			this.tipoOperacion = tipoOperacion;
		}

		/**
		 * @return the text
		 */
		public String getText() {
			return tipoOperacion;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return tipoOperacion;
		}

		public static TipoOperacion getTipoOperacion( String tipoOperacion ) {
			TipoOperacion[] fields = TipoOperacion.values();
			TipoOperacion tipoOp = null;
			for ( int i = 0; i < fields.length; i++ ) {
				if ( fields[ i ].tipoOperacion.equals( tipoOperacion ) ) {
					tipoOp = fields[ i ];
					break;
				}
			}
			return tipoOp;
		}
	}

	public enum TipoCambio {
		PORCENTAJE( "1" ), EFECTIVO( "2" );

		private final String	tipoOperacion;

		/**
		 * @param tipoOperacion
		 */
		private TipoCambio( final String tipoOperacion ) {
			this.tipoOperacion = tipoOperacion;
		}

		/**
		 * @return the text
		 */
		public String getTipoOperacion() {
			return tipoOperacion;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return tipoOperacion;
		}

		public static TipoCambio getTipoCambio( String tipoCambio ) {
			TipoCambio[] fields = TipoCambio.values();
			TipoCambio tc = null;
			for ( int i = 0; i < fields.length; i++ ) {
				if ( fields[ i ].tipoOperacion.equals( tipoCambio ) ) {
					tc = fields[ i ];
					break;
				}
			}
			return tc;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( getNumeroOrden() );
		hcb.append( getNucnfliq() );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		final DesgloseRecordBean desgloseRecordBean = ( DesgloseRecordBean ) obj;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( this.getNumeroOrden(), desgloseRecordBean.getNumeroOrden() );
		eqb.append( this.getNucnfliq(), desgloseRecordBean.getNucnfliq() );
		return eqb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public String toString() {
		final ToStringBuilder tsb = new ToStringBuilder( this, ToStringStyle.SHORT_PREFIX_STYLE );
		tsb.append( this.getNumeroOrden() );
		tsb.append( this.getNucnfliq() );
		return tsb.toString();
	}
}
