package sibbac.business.wrappers.database.model;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Periodos }.
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = WRAPPERS.PERIODO )
@Entity
@XmlRootElement
@Audited
public class Periodos extends ATableAudited< Periodos > implements java.io.Serializable {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 6650124428798283912L;

	/** The nombre. */
	@Column( name = "NBNOMBRE", nullable = false, unique = true, length = 60 )
	@XmlAttribute
	private String				nombre;

	/** The fecha inicio periodo. */
	@Temporal( TemporalType.DATE )
	@Column( name = "FHFCINICIO", nullable = false )
	@XmlAttribute
	private Date				fechaInicio;

	/** The fecha final periodo. */
	@Temporal( TemporalType.DATE )
	@Column( name = "FHFCFINAL", nullable = false )
	@XmlAttribute
	private Date				fechaFinal;

	/* The identificador. */
	@OneToOne( targetEntity = Identificadores.class, cascade = CascadeType.ALL )
	@JoinColumn( name = "ID_IDENTIFICADORES", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private Identificadores		identificador;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ESTADO", nullable = false, referencedColumnName = "IDESTADO" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Tmct0estado			estado;

	/**
	 * Instantiates a new periodos.
	 */
	public Periodos() {

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNombre() {
		return nombre;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public Identificadores getIdentificador() {
		return identificador;
	}

	/**
	 * @return the estado
	 */
	public Tmct0estado getEstado() {
		return this.estado;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public void setFechaInicio( Date fechaInicio ) {
		this.fechaInicio = fechaInicio;
	}

	public void setFechaFinal( Date fechaFinal ) {
		this.fechaFinal = fechaFinal;
	}

	public void setIdentificador( Identificadores identificador ) {
		this.identificador = identificador;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado( Tmct0estado estado ) {
		this.estado = estado;
	}

}
