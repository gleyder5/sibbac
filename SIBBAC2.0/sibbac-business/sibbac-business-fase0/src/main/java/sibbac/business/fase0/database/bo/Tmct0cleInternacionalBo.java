package sibbac.business.fase0.database.bo;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0cleInternacionalDao;
import sibbac.business.fase0.database.dto.Tmct0cleDTO;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0cleIdInternacional;
import sibbac.business.fase0.database.model.Tmct0cleInternacional;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0cleInternacionalBo extends
    AbstractBo<Tmct0cleInternacional, Tmct0cleIdInternacional, Tmct0cleInternacionalDao> {

  public List<Tmct0cleInternacional> findByFechaAndCdcleare(final Integer fecha, final String cdcleare) {
    return dao.findByFechaAndCdcleare(fecha, cdcleare);
  }

  public Set<Tmct0cleDTO> findDistinctDscleareAndBiccleByFhfinal() {
    return dao.findDistinctDscleareAndBiccleByFhfinal(Tmct0ali.fhfinActivos);
  }

  public List<Tmct0cleInternacional> findAllTmct0cleByCdcleareAndCentroAndCdmercadAndCdaliassAndCdsubctaAndTpsettleAndFecha(
      final String cdcleare, final String centro, final String cdmercad, final String cdaliass, final String cdsubcta,
      final String tpsettle, final Integer fecha) {
    return dao.findAllTmct0cleByCdcleareAndCentroAndCdmercadAndCdaliassAndCdsubctaAndTpsettleAndFecha(cdcleare, centro,
        cdmercad, cdaliass, cdsubcta, tpsettle, fecha);
  }

}
