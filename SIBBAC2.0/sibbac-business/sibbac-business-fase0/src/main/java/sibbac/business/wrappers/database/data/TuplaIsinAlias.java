/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.data;

import java.util.Date;
import java.util.List;

import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.wrappers.database.model.Tmct0Valores;

public class TuplaIsinAlias {

  protected String isin;
  protected String descAli;
  protected String cdaliass;
  protected String descrisin;
  private Date fechaLiquidacion;

  public TuplaIsinAlias() {

  }

  public TuplaIsinAlias(String isin, String cdaliass) {
    this.isin = isin;
    this.cdaliass = cdaliass;
  }

  public String getIsin() {
    return isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public String getCdaliass() {
    return cdaliass;
  }

  public void setCdaliass(String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public String getDescrisin() {
    return descrisin;
  }

  public void setDescrisin(String descrisin) {
    this.descrisin = descrisin;
  }

  public String getDescAli() {
    return descAli;
  }

  public void setDescAli(String descAli) {
    this.descAli = descAli;
  }

  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  /**
   * Rellena la descripcion del isin. Si lo encuentra en valores utiliza
   * descripcion. Si no lo encuentra guarda el isin.
   * 
   * @param boValores
   */
  public void fillDescrisin(List<Tmct0Valores> valores) {
    if (valores != null && valores.size() > 0) {
      descrisin = valores.get(0).getDescripcion();
    }
    else {
      descrisin = isin;
    }
  }

  /**
   * Rellena la descripcion del isin. Si lo encuentra en valores utiliza
   * descripcion. Si no lo encuentra guarda el isin.
   * 
   * @param boValores
   */
  public void fillDescali(Tmct0ali alias) {
    if (alias != null) {
      descAli = alias.getDescrali();
    }
    else {
      descAli = cdaliass;
    }
  }
}
