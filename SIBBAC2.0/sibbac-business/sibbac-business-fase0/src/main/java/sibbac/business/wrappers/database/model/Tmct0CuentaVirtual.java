/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 *
 * @author fjarquellada
 */
@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_CUENTA_VIRTUAL/*
															 * ,
															 * indexes={@Index(columnList="ER"),
															 * 
															 * @Index(columnList="NUMCUENTAER")}
															 */)
@XmlRootElement
public class Tmct0CuentaVirtual extends ATable< Tmct0CuentaVirtual > implements Serializable {

	private static final long		serialVersionUID	= 1L;

	@Column( name = "CUSTODIO", nullable = false )
	private Integer					custodio;
	@Basic( optional = false )
	@Column( name = "NUMCUENTAER", nullable = false )
	private String					numcuentaer;
	@Column( name = "NUMCUENTASUBCUSTODIO" )
	private String					numcuentasubcustodio;
	@Column( name = "NIVELCOMPENSACION", nullable = false )
	private Boolean					nivelcompensacion;
	@Basic( optional = false )
	@Column( name = "TIPOCUENTA", nullable = false )
	private int						tipocuenta;
	@Basic( optional = false )
	@Column( name = "TITULOS", nullable = false )
	private boolean					titulos;
	@Basic( optional = false )
	@Column( name = "CUENTAER", nullable = false )
	private boolean					cuentaer;
	@Basic( optional = false )
	@Column( name = "CONCILIACIONINTERNA", nullable = false )
	private boolean					conciliacioninterna;
	@JoinColumn( name = "ER", referencedColumnName = "ID", nullable = false )
	@ManyToOne( optional = false, fetch = FetchType.LAZY )
	private Tmct0EntidadRegistro	er;
	@JoinColumn( name = "TIPOER", referencedColumnName = "ID", nullable = false )
	@ManyToOne( optional = false, fetch = FetchType.LAZY )
	private Tmct0TipoEr				tipoer;
	@JoinColumn( name = "MERCADO", referencedColumnName = "ID_CAMARA_COMPENSACION", nullable = false )
	@ManyToOne( optional = false, fetch = FetchType.LAZY )
	private Tmct0CamaraCompensacion	mercado;
	@JoinColumn( name = "NETEO", referencedColumnName = "ID", nullable = false )
	@ManyToOne( optional = false, fetch = FetchType.LAZY )
	private Tmct0TipoNeteo			neteo;

	public Tmct0CuentaVirtual() {
	}

	public Tmct0CuentaVirtual( String numcuentaer, int tipocuenta, boolean titulos, boolean cuentaer, boolean conciliacioninterna ) {
		this.numcuentaer = numcuentaer;
		this.tipocuenta = tipocuenta;
		this.titulos = titulos;
		this.cuentaer = cuentaer;
		this.conciliacioninterna = conciliacioninterna;
	}

	public Integer getCustodio() {
		return custodio;
	}

	public void setCustodio( Integer custodio ) {
		this.custodio = custodio;
	}

	public String getNumcuentaer() {
		return numcuentaer;
	}

	public void setNumcuentaer( String numcuentaer ) {
		this.numcuentaer = numcuentaer;
	}

	public String getNumcuentasubcustodio() {
		return numcuentasubcustodio;
	}

	public void setNumcuentasubcustodio( String numcuentasubcustodio ) {
		this.numcuentasubcustodio = numcuentasubcustodio;
	}

	public Boolean getNivelcompensacion() {
		return nivelcompensacion;
	}

	public void setNivelcompensacion( Boolean nivelcompensacion ) {
		this.nivelcompensacion = nivelcompensacion;
	}

	public int getTipocuenta() {
		return tipocuenta;
	}

	public void setTipocuenta( int tipocuenta ) {
		this.tipocuenta = tipocuenta;
	}

	public boolean getTitulos() {
		return titulos;
	}

	public void setTitulos( boolean titulos ) {
		this.titulos = titulos;
	}

	public boolean getCuentaer() {
		return cuentaer;
	}

	public void setCuentaer( boolean cuentaer ) {
		this.cuentaer = cuentaer;
	}

	public boolean getConciliacioninterna() {
		return conciliacioninterna;
	}

	public void setConciliacioninterna( boolean conciliacioninterna ) {
		this.conciliacioninterna = conciliacioninterna;
	}

	public Tmct0EntidadRegistro getEr() {
		return er;
	}

	public void setEr( Tmct0EntidadRegistro er ) {
		this.er = er;
	}

	public Tmct0TipoEr getTipoer() {
		return tipoer;
	}

	public void setTipoer( Tmct0TipoEr tipoer ) {
		this.tipoer = tipoer;
	}

	public Tmct0CamaraCompensacion getMercado() {
		return mercado;
	}

	public void setMercado( Tmct0CamaraCompensacion mercado ) {
		this.mercado = mercado;
	}

	public Tmct0TipoNeteo getNeteo() {
		return neteo;
	}

	public void setNeteo( Tmct0TipoNeteo neteo ) {
		this.neteo = neteo;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != null ? id.hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if ( !( object instanceof Tmct0CuentaVirtual ) ) {
			return false;
		}
		Tmct0CuentaVirtual other = ( Tmct0CuentaVirtual ) object;
		if ( ( this.id == null && other.id != null ) || ( this.id != null && !this.id.equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sibbac.business.conciliacion.database.model.Tmct0CuentaVirtual[ id=" + id + " ]";
	}

}
