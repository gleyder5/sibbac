package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Tabla Movimiento Manual
 * 
 * @author Cristina
 *
 */
@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_MOVIMIENTO_MANUAL )
@XmlRootElement
public class Tmct0MovimientoManual extends ATable< Tmct0MovimientoManual > implements Serializable {

	/**
	 *  
	 */

	// -TÃ­tulos
	// -Efectivo
	// -Isin
	// -DescripciÃ³n Isin
	// -Sentido(C/V)
	// -SentidoCV(M/C)
	// -Cta. LiquidaciÃ³n
	// -DescripciÃ³n Cta. LiquidaciÃ³n
	// -Settlement date
	// -CÃ¡mara

	private static final long	serialVersionUID	= 1L;

	@Column( name = "SENTIDO", length = 1, nullable = false )
	private String				sentido;

	@Column( name = "TITULOS", nullable = false, precision = 18, scale = 6 )
	private BigDecimal			titulos;

	@Column( name = "FECHA_LIQUIDACION" )
	private Date				fechaLiquidacion;

	@Column( name = "FECHA_CONTRATACION" )
	private Date				fechaContratacion;

	@Column( name = "PROCESADO_CV" )
	private boolean				procesadoCv;

	@Column( name = "EFECTIVO", nullable = false, precision = 17, scale = 2 )
	private BigDecimal			efectivo;

	@Column( name = "ISIN", length = 12 )
	private String				isin;

	@Column( name = "DESCRIPCION_ISIN", length = 40 )
	private String				descripcionIsin;

	@Column( name = "SENTIDO_CV", length = 1 )
	private String				sentidoCV;

	@Column( name = "CUENTA_LIQUIDACION", length = 35 )
	private String				cuentaLiquidacion;

	@Column( name = "DESCRIPCION_CUENTA_LIQUIDACION", length = 40 )
	private String				descripcionCuentaLiquidacion;

	@Column( name = "CUENTA_LIQUIDACION_EFECTIVO", length = 35 )
	private String				cuentaLiquidacionEfectivo;

	@Column( name = "DESCRIPCION_CUENTA_LIQUIDACION_EFECTIVO", length = 40 )
	private String				descripcionCuentaLiquidacionEfectivo;

	@Column( name = "CAMARA", length = 11 )
	private String				camara;

	@Column( name = "DESCRIPCION_CAMARA", length = 4 )
	private String				descripcionCamara;

	@Column( name = "CUENTA_COMPENSACION", length = 35 )
	private String				cuentaCompensacion;

	@Column( name = "DESCRIPCION_CUENTA_COMPENSACION", length = 40 )
	private String				descripcionCuentaCompensacion;

	@Column( name = "ALIAS" )
	private String				alias;

	@Column( name = "DESCRIPCION_ALIAS" )
	private String				descripcionAlias;

	@Column( name = "PRECIO", nullable = false, precision = 13, scale = 6 )
	BigDecimal					precio;

	public Tmct0MovimientoManual() {

	}

	public Tmct0MovimientoManual( Tmct0MovimientoManual tmct0MovimientoManual ) {
		this.alias = tmct0MovimientoManual.getAlias();
		this.camara = tmct0MovimientoManual.getCamara();
		this.cuentaCompensacion = tmct0MovimientoManual.getCuentaCompensacion();
		this.cuentaLiquidacion = tmct0MovimientoManual.getCuentaLiquidacion();
		this.cuentaLiquidacionEfectivo = tmct0MovimientoManual.getCuentaLiquidacionEfectivo();
		this.descripcionAlias = tmct0MovimientoManual.getDescripcionAlias();
		this.descripcionCamara = tmct0MovimientoManual.getDescripcionCamara();
		this.descripcionCuentaCompensacion = tmct0MovimientoManual.getDescripcionCuentaCompensacion();
		this.descripcionCuentaLiquidacion = tmct0MovimientoManual.getDescripcionCuentaLiquidacion();
		this.descripcionCuentaLiquidacionEfectivo = tmct0MovimientoManual.getDescripcionCuentaLiquidacionEfectivo();
		this.descripcionIsin = tmct0MovimientoManual.getDescripcionIsin();
		this.efectivo = tmct0MovimientoManual.getEfectivo();
		this.fechaContratacion = tmct0MovimientoManual.getFechaContratacion();
		this.fechaLiquidacion = tmct0MovimientoManual.getFechaLiquidacion();
		this.isin = tmct0MovimientoManual.getIsin();
		this.procesadoCv = tmct0MovimientoManual.isProcesadoCv();
		this.sentido = tmct0MovimientoManual.getSentido();
		this.sentidoCV = tmct0MovimientoManual.getSentidoCV();
		this.titulos = tmct0MovimientoManual.getTitulos();
		this.precio = tmct0MovimientoManual.getPrecio();
	}

	/**
	 * @return the fechaContratacion
	 */
	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @param fechaContratacion the fechaContratacion to set
	 */
	public void setFechaContratacion( Date fechaContratacion ) {
		this.fechaContratacion = fechaContratacion;
	}

	/**
	 * @return the descripcionCamara
	 */
	public String getDescripcionCamara() {
		return descripcionCamara;
	}

	/**
	 * @param descripcionCamara the descripcionCamara to set
	 */
	public void setDescripcionCamara( String descripcionCamara ) {
		this.descripcionCamara = descripcionCamara;
	}

	/**
	 * @return the cuentaCompensacion
	 */
	public String getCuentaCompensacion() {
		return cuentaCompensacion;
	}

	/**
	 * @param cuentaCompensacion the cuentaCompensacion to set
	 */
	public void setCuentaCompensacion( String cuentaCompensacion ) {
		this.cuentaCompensacion = cuentaCompensacion;
	}

	/**
	 * @return the descripcionCuentaCompensacion
	 */
	public String getDescripcionCuentaCompensacion() {
		return descripcionCuentaCompensacion;
	}

	/**
	 * @param descripcionCuentaCompensacion the descripcionCuentaCompensacion to set
	 */
	public void setDescripcionCuentaCompensacion( String descripcionCuentaCompensacion ) {
		this.descripcionCuentaCompensacion = descripcionCuentaCompensacion;
	}

	/**
	 * @return the procesadoCv
	 */
	public boolean isProcesadoCv() {
		return procesadoCv;
	}

	/**
	 * @param procesadoCv the procesadoCv to set
	 */
	public void setProcesadoCv( boolean procesadoCv ) {
		this.procesadoCv = procesadoCv;
	}

	/**
	 * @return the sentido
	 */
	public String getSentido() {
		return sentido;
	}

	/**
	 * @param sentido the sentido to set
	 */
	public void setSentido( String sentido ) {
		this.sentido = sentido;
	}

	/**
	 * @return the titulos
	 */
	public BigDecimal getTitulos() {
		return titulos;
	}

	/**
	 * @param titulos the titulos to set
	 */
	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	/**
	 * @return the fechaLiquidacion
	 */
	public Date getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	/**
	 * @param fechaLiquidacion the fechaLiquidacion to set
	 */
	public void setFechaLiquidacion( Date fechaLiquidacion ) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

	public BigDecimal getEfectivo() {
		return efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescripcionIsin() {
		return descripcionIsin;
	}

	public void setDescripcionIsin( String descripcionIsin ) {
		this.descripcionIsin = descripcionIsin;
	}

	public String getSentidoCV() {
		return sentidoCV;
	}

	public void setSentidoCV( String sentidoCV ) {
		this.sentidoCV = sentidoCV;
	}

	public String getCuentaLiquidacion() {
		return cuentaLiquidacion;
	}

	public void setCuentaLiquidacion( String cuentaLiquidacion ) {
		this.cuentaLiquidacion = cuentaLiquidacion;
	}

	public String getDescripcionCuentaLiquidacion() {
		return descripcionCuentaLiquidacion;
	}

	public void setDescripcionCuentaLiquidacion( String descripcionCuentaLiquidacion ) {
		this.descripcionCuentaLiquidacion = descripcionCuentaLiquidacion;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara( String camara ) {
		this.camara = camara;
	}

	/**
	 * @return the cuentaLiquidacionEfectivo
	 */
	public String getCuentaLiquidacionEfectivo() {
		return cuentaLiquidacionEfectivo;
	}

	/**
	 * @param cuentaLiquidacionEfectivo the cuentaLiquidacionEfectivo to set
	 */
	public void setCuentaLiquidacionEfectivo( String cuentaLiquidacionEfectivo ) {
		this.cuentaLiquidacionEfectivo = cuentaLiquidacionEfectivo;
	}

	/**
	 * @return the descripcionCuentaLiquidacionEfectivo
	 */
	public String getDescripcionCuentaLiquidacionEfectivo() {
		return descripcionCuentaLiquidacionEfectivo;
	}

	/**
	 * @param descripcionCuentaLiquidacionEfectivo the descripcionCuentaLiquidacionEfectivo to set
	 */
	public void setDescripcionCuentaLiquidacionEfectivo( String descripcionCuentaLiquidacionEfectivo ) {
		this.descripcionCuentaLiquidacionEfectivo = descripcionCuentaLiquidacionEfectivo;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias( String alias ) {
		this.alias = alias;
	}

	/**
	 * @return the descripcionAlias
	 */
	public String getDescripcionAlias() {
		return descripcionAlias;
	}

	/**
	 * @param descripcionAlias the descripcionAlias to set
	 */
	public void setDescripcionAlias( String descripcionAlias ) {
		this.descripcionAlias = descripcionAlias;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio( BigDecimal precio ) {
		this.precio = precio;
	}

}
