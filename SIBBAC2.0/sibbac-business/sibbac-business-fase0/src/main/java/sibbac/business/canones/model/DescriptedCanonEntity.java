package sibbac.business.canones.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Mapea el campo descripción y marca como necesario el campo Id para fungir como llave primaria.
 * @author XI326526
 *
 * @param <T> Tipo de la llave primaria
 */
@MappedSuperclass
public abstract class DescriptedCanonEntity<T extends Serializable> extends CanonEntity {
  
  static final String ID_COLUMN_NAME = "ID";
  
  /**
   * número de serie por defecto
   */
  private static final long serialVersionUID = 1L;

  @Column(name = "DESCRIPCION", length = 256)
  private String descripcion;
  
  public abstract T getId();
  
  public abstract void setId(T id);

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    DescriptedCanonEntity<?> other = (DescriptedCanonEntity<?>) obj;
    if (descripcion == null) {
      if (other.descripcion != null)
        return false;
    } else if (!descripcion.equals(other.descripcion))
      return false;
    return true;
  }

  String toString(String nombreEntidad) {
    return String.format("{%s: %s, %s}", nombreEntidad, getId(), descripcion);
  }

}
