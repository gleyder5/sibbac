package sibbac.business.wrappers.database.data;


import java.util.ArrayList;
import java.util.List;


/**
 * The Class ServiciosContactosData.
 */
public class CombinacionData {

	/** The id contacto. */
	private Long						idContacto;

	/** The servicio activo datas. */
	private List< ServicioActivoData >	servicioActivoDatas;

	/**
	 * Instantiates a new combinacion data.
	 *
	 * @param idContacto the id contacto
	 */
	public CombinacionData( Long idContacto ) {
		super();
		this.idContacto = idContacto;
	}

	/**
	 * Instantiates a new combinacion data.
	 *
	 * @param idContacto the id contacto
	 * @param servicioActivoDatas the servicio activo datas
	 */
	public CombinacionData( Long idContacto, List< ServicioActivoData > servicioActivoDatas ) {
		super();
		this.idContacto = idContacto;
		this.servicioActivoDatas = servicioActivoDatas;
	}

	public boolean add( ServicioActivoData servicioActivoData ) {
		return getServicioActivoDatas().add( servicioActivoData );
	}

	/**
	 * Gets the id contacto.
	 *
	 * @return the id contacto
	 */
	public final Long getIdContacto() {
		return idContacto;
	}

	/**
	 * Sets the id contacto.
	 *
	 * @param idContacto the new id contacto
	 */
	public final void setIdContacto( Long idContacto ) {
		this.idContacto = idContacto;
	}

	/**
	 * Gets the servicio activo datas.
	 *
	 * @return the servicio activo datas
	 */
	public final List< ServicioActivoData > getServicioActivoDatas() {
		if ( servicioActivoDatas == null ) {
			servicioActivoDatas = new ArrayList< ServicioActivoData >();
		}
		return servicioActivoDatas;
	}

	/**
	 * Sets the servicio activo datas.
	 *
	 * @param servicioActivoDatas the new servicio activo datas
	 */
	public final void setServicioActivoDatas( List< ServicioActivoData > servicioActivoDatas ) {
		this.servicioActivoDatas = servicioActivoDatas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( idContacto == null ) ? 0 : idContacto.hashCode() );
		result = prime * result + ( ( servicioActivoDatas == null ) ? 0 : servicioActivoDatas.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		CombinacionData other = ( CombinacionData ) obj;
		if ( idContacto == null ) {
			if ( other.idContacto != null )
				return false;
		} else if ( !idContacto.equals( other.idContacto ) )
			return false;
		if ( servicioActivoDatas == null ) {
			if ( other.servicioActivoDatas != null )
				return false;
		} else if ( !servicioActivoDatas.equals( other.servicioActivoDatas ) )
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CombinacionData [idContacto=" + idContacto + ", servicioActivoDatas=" + servicioActivoDatas + "]";
	}

}
