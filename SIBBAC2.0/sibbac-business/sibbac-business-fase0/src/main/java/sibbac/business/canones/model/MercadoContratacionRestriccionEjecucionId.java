package sibbac.business.canones.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MercadoContratacionRestriccionEjecucionId extends MercadoContratacionRefId {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -2765429409267474992L;

  @Column(name = MercadoContratacion.ID_COLUMN_NAME, length = 2, nullable = false)
  private String idField;

  @Override
  public String getIdField() {
    return idField;
  }
  
  @Override
  public void setIdField(String idField) {
    this.idField = idField;
  }
  
}
