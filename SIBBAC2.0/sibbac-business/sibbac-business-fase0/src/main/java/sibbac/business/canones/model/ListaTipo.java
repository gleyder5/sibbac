package sibbac.business.canones.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

/**
 * Tipo de lista
 */
@Entity
@Table(name = "TMCT0_LISTA_TIPO")
public class ListaTipo extends DescriptedCanonEntity<String> {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = 5707115087252480130L;
  
  @Id
  @Column(name = ID_COLUMN_NAME, length = 32)
  private String id;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    ListaTipo other = (ListaTipo) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return toString("Tipo de lista");
  }
  
}
