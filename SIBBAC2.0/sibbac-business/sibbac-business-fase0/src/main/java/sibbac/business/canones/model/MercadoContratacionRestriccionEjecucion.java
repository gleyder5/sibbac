package sibbac.business.canones.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;

/**
 * Restricción de Ejecución para un Mercado de Contratación.
 */
@Entity
@Table(name = "TMCT0_MERCADO_CONTRATACION_RESTRICCION_EJECUCION")
public class MercadoContratacionRestriccionEjecucion extends 
    DescriptedCanonEntity<MercadoContratacionRestriccionEjecucionId> {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = 2227913054918278395L;
  
  @EmbeddedId
  private MercadoContratacionRestriccionEjecucionId id;

  @Override
  public MercadoContratacionRestriccionEjecucionId getId() {
    return id;
  }
  
  @Override
  public void setId(MercadoContratacionRestriccionEjecucionId id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    MercadoContratacionRestriccionEjecucion other = (MercadoContratacionRestriccionEjecucion) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return toString("Restricción Ejecución");
  }

}
