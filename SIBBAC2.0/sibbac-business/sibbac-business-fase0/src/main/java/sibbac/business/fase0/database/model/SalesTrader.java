package sibbac.business.fase0.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0_SALES_TRADER")
public class SalesTrader implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8470824745458232268L;
  @Id
  @Column(unique = true)
  private String cdempleado;
  @Column(nullable = false)
  private String nbnombre;
  @Column(nullable = false)
  private String nbapellido1;
  @Column(nullable = false)
  private String nbapellido2;
  @Column(nullable = false)
  private String cdnacionalidad;
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_NACIMIENTO", nullable = false)
  private Date fechaNacimiento;
  @Column(name = "TIPO_ID_MIFID", nullable = false)
  private char tipoIdMifid;
  @Column(name = "ID_MIFID", nullable = false)
  private String idMifid;
  @Column(name = "CODIGO_CORTO", nullable = false)
  private int codigoCorto;
  @Column(name = "CD_TIPO_USUARIO", nullable = false)
  private char cdTipoUsuario;
  @Column(nullable = false)
  private Date fhinicio;
  @Column(nullable = false)
  private Date fhfinal;
  @Column(nullable = false)
  private String cdusuaud;

  public SalesTrader() {

  }

  public String getCdempleado() {
    return cdempleado;
  }

  public String getNbnombre() {
    return nbnombre;
  }

  public String getNbapellido1() {
    return nbapellido1;
  }

  public String getNbapellido2() {
    return nbapellido2;
  }

  public String getCdnacionalidad() {
    return cdnacionalidad;
  }

  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  public char getTipoIdMifid() {
    return tipoIdMifid;
  }

  public String getIdMifid() {
    return idMifid;
  }

  public int getCodigoCorto() {
    return codigoCorto;
  }

  public char getCdTipoUsuario() {
    return cdTipoUsuario;
  }

  public Date getFhinicio() {
    return fhinicio;
  }

  public Date getFhfinal() {
    return fhfinal;
  }

  

  public void setCdempleado(String cdempleado) {
    this.cdempleado = cdempleado;
  }

  public void setNbnombre(String nbnombre) {
    this.nbnombre = nbnombre;
  }

  public void setNbapellido1(String nbapellido1) {
    this.nbapellido1 = nbapellido1;
  }

  public void setNbapellido2(String nbapellido2) {
    this.nbapellido2 = nbapellido2;
  }

  public void setCdnacionalidad(String cdnacionalidad) {
    this.cdnacionalidad = cdnacionalidad;
  }

  public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  public void setTipoIdMifid(char tipoIdMifid) {
    this.tipoIdMifid = tipoIdMifid;
  }

  public void setIdMifid(String idMifid) {
    this.idMifid = idMifid;
  }

  public void setCodigoCorto(int codigoCorto) {
    this.codigoCorto = codigoCorto;
  }

  public void setCdTipoUsuario(char cdTipoUsuario) {
    this.cdTipoUsuario = cdTipoUsuario;
  }

  public void setFhinicio(Date fhinicio) {
    this.fhinicio = fhinicio;
  }

  public void setFhfinal(Date fhfinal) {
    this.fhfinal = fhfinal;
  }

 

  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Override
  public String toString() {
    return String
        .format(
            "SalesTrader [cdempleado=%s, nbnombre=%s, nbapellido1=%s, nbapellido2=%s, cdnacionalidad=%s, fechaNacimiento=%s, tipoIdMifid=%s, idMifid=%s, codigoCorto=%s, cdTipoUsuario=%s, fhinicio=%s, fhfinal=%s, cdusuaud=%s]",
            cdempleado, nbnombre, nbapellido1, nbapellido2, cdnacionalidad, fechaNacimiento, tipoIdMifid, idMifid,
            codigoCorto, cdTipoUsuario, fhinicio, fhfinal, cdusuaud);
  }

}
