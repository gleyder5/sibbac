package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_TIPOS_ACTIVO" )
public class TiposActivo extends MasterData< TiposActivo > {

	private static final long	serialVersionUID	= 1965825208136347124L;

}
