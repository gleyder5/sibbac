package sibbac.business.wrappers.database.data;


/**
 * The Class ServicioData.
 */
public class ServicioData {

	/** The id servicio. */
	private final Long		idServicio;

	/** The nombre servicio. */
	private final String	nombreServicio;

	/** The descripcion servicio. */
	private final String	descripcionServicio;

	/**
	 * Instantiates a new servicio data.
	 *
	 * @param idServicio the id servicio
	 * @param nombreServicio the nombre servicio
	 * @param descripcionServicio the descripcion servicio
	 */
	public ServicioData( Long idServicio, String nombreServicio, String descripcionServicio ) {
		super();
		this.idServicio = idServicio;
		this.nombreServicio = nombreServicio;
		this.descripcionServicio = descripcionServicio;
	}

	public ServicioData( ServicioData servicioData ) {
		super();
		this.idServicio = servicioData.getIdServicio();
		this.nombreServicio = servicioData.getNombreServicio();
		this.descripcionServicio = servicioData.getDescripcionServicio();
	}

	/**
	 * Gets the id servicio.
	 *
	 * @return the id servicio
	 */
	public final Long getIdServicio() {
		return idServicio;
	}

	/**
	 * Gets the nombre servicio.
	 *
	 * @return the nombre servicio
	 */
	public final String getNombreServicio() {
		return nombreServicio;
	}

	/**
	 * Gets the descripcion servicio.
	 *
	 * @return the descripcion servicio
	 */
	public final String getDescripcionServicio() {
		return descripcionServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( descripcionServicio == null ) ? 0 : descripcionServicio.hashCode() );
		result = prime * result + ( ( idServicio == null ) ? 0 : idServicio.hashCode() );
		result = prime * result + ( ( nombreServicio == null ) ? 0 : nombreServicio.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		ServicioData other = ( ServicioData ) obj;
		if ( descripcionServicio == null ) {
			if ( other.descripcionServicio != null )
				return false;
		} else if ( !descripcionServicio.equals( other.descripcionServicio ) )
			return false;
		if ( idServicio == null ) {
			if ( other.idServicio != null )
				return false;
		} else if ( !idServicio.equals( other.idServicio ) )
			return false;
		if ( nombreServicio == null ) {
			if ( other.nombreServicio != null )
				return false;
		} else if ( !nombreServicio.equals( other.nombreServicio ) )
			return false;
		return true;
	}

}
