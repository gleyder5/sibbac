package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;


@Entity
@Table( name = DBConstants.WRAPPERS.RELACION_CUENTA_MERCADO )
@XmlRootElement
public class RelacionCuentaMercado implements Serializable {

	/**
	 * 
	 */
	private static final long				serialVersionUID	= 1L;
	@EmbeddedId
	RelacionCuentaMercadoPK					relacionCuentaMercadoPK;
	@OneToMany( fetch = FetchType.LAZY )
	private List< Tmct0CuentaLiquidacion >	cuentaLiquidacionList;
	@OneToMany( fetch = FetchType.LAZY )
	private List< Tmct0Mercado >			mercadoList;

	public RelacionCuentaMercado() {
		// default constructor
	}

	public RelacionCuentaMercado( Tmct0Mercado mkt, Tmct0CuentaLiquidacion cl ) {
		if ( this.relacionCuentaMercadoPK == null ) {
			this.relacionCuentaMercadoPK = new RelacionCuentaMercadoPK();
		}
		this.relacionCuentaMercadoPK.setIdCtaLiquidacion( cl );
		this.relacionCuentaMercadoPK.setIdMercado( mkt );
	}

	public RelacionCuentaMercadoPK getRelacionCuentaMercadoPK() {
		return relacionCuentaMercadoPK;
	}

	public void setRelacionCuentaMercadoPK( RelacionCuentaMercadoPK relacionCuentaMercadoPK ) {
		this.relacionCuentaMercadoPK = relacionCuentaMercadoPK;
	}

	public List< Tmct0CuentaLiquidacion > getCuentaLiquidacionList() {
		return cuentaLiquidacionList;
	}

	public void setCuentaLiquidacionList( List< Tmct0CuentaLiquidacion > cuentaLiquidacionList ) {
		this.cuentaLiquidacionList = cuentaLiquidacionList;
	}

	public List< Tmct0Mercado > getMercadoList() {
		return mercadoList;
	}

	public void setMercadoList( List< Tmct0Mercado > mercadoList ) {
		this.mercadoList = mercadoList;
	}

}
