package sibbac.business.canones.model;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import static javax.persistence.TemporalType.DATE;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TMCT0_REGLAS_CANONES")
public class ReglasCanones extends DescriptedCanonEntity<BigInteger> {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -8226836070130392254L;

  @Id
  @Column(name = "ID", nullable = false)
  private BigInteger id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_MERCADO_CONTRATACION", referencedColumnName = "ID", nullable = false)
  private MercadoContratacion mercadoContratacion;

  @Column(name = "CODIGO", length = 32)
  private String codigo;

  @Column(name = "ORDEN", nullable = false)
  private int orden = 1;

  @Column(name = "FECHA_DESDE", nullable = false)
  @Temporal(DATE)
  private Date fechaDesde;

  @Column(name = "FECHA_HASTA")
  @Temporal(DATE)
  private Date fechaHasta;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_ALIAS_COMPARATOR", referencedColumnName = ID_COLUMN_NAME)
  private ListaComparator aliasComparator;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_ALIAS", referencedColumnName = ID_COLUMN_NAME)
  private Lista aliasLista;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_ISIN_COMPARATOR", referencedColumnName = ID_COLUMN_NAME)
  private ListaComparator isinComparator;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_ISIN", referencedColumnName = ID_COLUMN_NAME)
  private Lista isinLista;

  @OneToOne
  @JoinColumn(name = "ID_TIPO_CALCULO", referencedColumnName = ID_COLUMN_NAME)
  private ReglasCanonesTipoCalculo tipoCalculo;

  @OneToOne
  @JoinColumn(name = "ID_TIPO_CANON", referencedColumnName = ID_COLUMN_NAME)
  private ReglasCanonesTipoCanon tipoCanon;

  @Column(name = "ACTIVO", nullable = false)
  private char activo;

  @OneToMany
  @JoinColumn(name = "ID_REGLA_CANON", referencedColumnName = ID_COLUMN_NAME)
  private List<ReglasCanonesParametrizaciones> parametrizaciones;

  public ReglasCanones() {
    parametrizaciones = new ArrayList<>();
  }

  @Override
  public BigInteger getId() {
    return id;
  }

  @Override
  public void setId(BigInteger id) {
    this.id = id;
  }

  @JsonIgnore
  public MercadoContratacion getMercadoContratacion() {
    return mercadoContratacion;
  }

  public void setMercadoContratacion(MercadoContratacion mercadoContratacion) {
    this.mercadoContratacion = mercadoContratacion;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public int getOrden() {
    return orden;
  }

  public void setOrden(int orden) {
    this.orden = orden;
  }

  public Date getFechaDesde() {
    return fechaDesde;
  }

  public void setFechaDesde(Date fechaDesde) {
    this.fechaDesde = fechaDesde;
  }

  public Date getFechaHasta() {
    return fechaHasta;
  }

  public void setFechaHasta(Date fechaHasta) {
    this.fechaHasta = fechaHasta;
  }

  public List<ReglasCanonesParametrizaciones> getParametrizaciones() {
    return parametrizaciones;
  }

  public void setParametrizaciones(List<ReglasCanonesParametrizaciones> parametrizaciones) {
    this.parametrizaciones = parametrizaciones;
  }

  public char getActivo() {
    return activo;
  }

  public void setActivo(char activo) {
    this.activo = activo;
  }

  public ListaComparator getAliasComparator() {
    return aliasComparator;
  }

  public void setAliasComparator(ListaComparator aliasComparator) {
    this.aliasComparator = aliasComparator;
  }

  public Lista getAliasLista() {
    return aliasLista;
  }

  public void setAliasLista(Lista aliasLista) {
    this.aliasLista = aliasLista;
  }

  public ListaComparator getIsinComparator() {
    return isinComparator;
  }

  public void setIsinComparator(ListaComparator isinComparator) {
    this.isinComparator = isinComparator;
  }

  public Lista getIsinLista() {
    return isinLista;
  }

  public void setIsinLista(Lista isinLista) {
    this.isinLista = isinLista;
  }

  public ReglasCanonesTipoCalculo getTipoCalculo() {
    return tipoCalculo;
  }

  public void setTipoCalculo(ReglasCanonesTipoCalculo tipoCalculo) {
    this.tipoCalculo = tipoCalculo;
  }

  public ReglasCanonesTipoCanon getTipoCanon() {
    return tipoCanon;
  }

  public void setTipoCanon(ReglasCanonesTipoCanon tipoCanon) {
    this.tipoCanon = tipoCanon;
  }

  @Transient
  public boolean isActivo() {
    return activo == '1';
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + activo;
    result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
    result = prime * result + ((fechaDesde == null) ? 0 : fechaDesde.hashCode());
    result = prime * result + ((fechaHasta == null) ? 0 : fechaHasta.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + orden;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    ReglasCanones other = (ReglasCanones) obj;
    if (activo != other.activo)
      return false;
    if (codigo == null) {
      if (other.codigo != null)
        return false;
    }
    else if (!codigo.equals(other.codigo))
      return false;
    if (fechaDesde == null) {
      if (other.fechaDesde != null)
        return false;
    }
    else if (!fechaDesde.equals(other.fechaDesde))
      return false;
    if (fechaHasta == null) {
      if (other.fechaHasta != null)
        return false;
    }
    else if (!fechaHasta.equals(other.fechaHasta))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    }
    else if (!id.equals(other.id))
      return false;
    if (orden != other.orden)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return toString("Reglas Canones");
  }

}
