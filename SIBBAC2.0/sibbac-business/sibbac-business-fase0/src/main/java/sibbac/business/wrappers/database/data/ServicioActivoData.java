package sibbac.business.wrappers.database.data;


/**
 * The Class ServicioActivoData.
 */
public class ServicioActivoData {

	/** The id servicio. */
	private final Long		idServicio;

	/** The Active. */
	private final Boolean	Active;

	/**
	 * Instantiates a new servicio activo data.
	 *
	 * @param idServicio the id servicio
	 * @param active the active
	 */
	public ServicioActivoData( Long idServicio, Boolean active ) {
		super();
		this.idServicio = idServicio;
		Active = active;
	}

	/**
	 * Gets the id servicio.
	 *
	 * @return the id servicio
	 */
	public final Long getIdServicio() {
		return idServicio;
	}

	/**
	 * Gets the active.
	 *
	 * @return the active
	 */
	public final Boolean getActive() {
		return Active;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( Active == null ) ? 0 : Active.hashCode() );
		result = prime * result + ( ( idServicio == null ) ? 0 : idServicio.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		ServicioActivoData other = ( ServicioActivoData ) obj;
		if ( Active == null ) {
			if ( other.Active != null )
				return false;
		} else if ( !Active.equals( other.Active ) )
			return false;
		if ( idServicio == null ) {
			if ( other.idServicio != null )
				return false;
		} else if ( !idServicio.equals( other.idServicio ) )
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServicioActivoData [idServicio=" + idServicio + ", Active=" + Active + "]";
	}

}
