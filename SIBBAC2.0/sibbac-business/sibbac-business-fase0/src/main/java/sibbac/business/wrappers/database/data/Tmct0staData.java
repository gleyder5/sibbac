package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.database.model.Tmct0msc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.business.wrappers.database.model.Tmct0stt;

public class Tmct0staData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Tmct0staId id;
    private Tmct0stt tmct0stt;
    private String dsestado = "";
    private String cdestret = "";
    private String cdusuaud = "";
    private Date fhaudit = new Date();
    private Integer nuversion = 0;
    private List<Tmct0bok> tmct0boks = new ArrayList<Tmct0bok>(0);
    private List<Tmct0ord> tmct0ords = new ArrayList<Tmct0ord>(0);
    private List<Tmct0msc> tmct0mscs = new ArrayList<Tmct0msc>(0);
    private List<Tmct0alo> tmct0alos = new ArrayList<Tmct0alo>(0);
    private List<Tmct0alc> tmct0alcs = new ArrayList<Tmct0alc>(0);
    private List<Tmct0men> tmct0mens = new ArrayList<Tmct0men>(0);
    private List<Tmct0cta> tmct0ctas = new ArrayList<Tmct0cta>(0);
    private List<Tmct0ctg> tmct0ctgs = new ArrayList<Tmct0ctg>(0);
    private List<Tmct0cto> tmct0ctos = new ArrayList<Tmct0cto>(0);

    public Tmct0staData() {

    }

    public Tmct0staData(Tmct0sta entity) {
	entityToData(entity, this);
    }

    public static Tmct0staData entityToData(Tmct0sta entity) {
	Tmct0staData data = new Tmct0staData();
	entityToData(entity, data);
	return data;
    }

    public static Tmct0sta dataToEntity(Tmct0staData data) {
	Tmct0sta entity = new Tmct0sta();
	dataToEntity(data, entity);
	return entity;
    }

    private static void entityToData(Tmct0sta entity, Tmct0staData data) {
	data.setCdestret(entity.getCdestret());
	data.setCdusuaud(entity.getCdusuaud());
	data.setDsestado(entity.getDsestado());
	data.setFhaudit(entity.getFhaudit());
	data.setId(entity.getId());
	data.setNuversion(entity.getNuversion());
	data.setTmct0alcs(entity.getTmct0alcs());
	data.setTmct0alos(entity.getTmct0alos());
	data.setTmct0boks(entity.getTmct0boks());
	data.setTmct0ctas(entity.getTmct0ctas());
	data.setTmct0ctgs(entity.getTmct0ctgs());
	data.setTmct0ctos(entity.getTmct0ctos());
	data.setTmct0mens(entity.getTmct0mens());
	data.setTmct0mscs(entity.getTmct0mscs());
	data.setTmct0ords(entity.getTmct0ords());
	data.setTmct0stt(entity.getTmct0stt());
    }

    private static void dataToEntity(Tmct0staData data, Tmct0sta entity) {
	entity.setCdestret(data.getCdestret());
	entity.setCdusuaud(data.getCdusuaud());
	entity.setDsestado(data.getDsestado());
	entity.setFhaudit(data.getFhaudit());
	entity.setId(data.getId());
	entity.setNuversion(data.getNuversion());
	entity.setTmct0alcs(data.getTmct0alcs());
	entity.setTmct0alos(data.getTmct0alos());
	entity.setTmct0boks(data.getTmct0boks());
	entity.setTmct0ctas(data.getTmct0ctas());
	entity.setTmct0ctgs(data.getTmct0ctgs());
	entity.setTmct0ctos(data.getTmct0ctos());
	entity.setTmct0mens(data.getTmct0mens());
	entity.setTmct0mscs(data.getTmct0mscs());
	entity.setTmct0ords(data.getTmct0ords());
	entity.setTmct0stt(data.getTmct0stt());
    }

    public Tmct0staId getId() {
	return id;
    }

    public void setId(Tmct0staId id) {
	this.id = id;
    }

    public Tmct0stt getTmct0stt() {
	return tmct0stt;
    }

    public void setTmct0stt(Tmct0stt tmct0stt) {
	this.tmct0stt = tmct0stt;
    }

    public String getDsestado() {
	return dsestado;
    }

    public void setDsestado(String dsestado) {
	this.dsestado = dsestado;
    }

    public String getCdestret() {
	return cdestret;
    }

    public void setCdestret(String cdestret) {
	this.cdestret = cdestret;
    }

    public String getCdusuaud() {
	return cdusuaud;
    }

    public void setCdusuaud(String cdusuaud) {
	this.cdusuaud = cdusuaud;
    }

    public Date getFhaudit() {
	return fhaudit;
    }

    public void setFhaudit(Date fhaudit) {
	this.fhaudit = fhaudit;
    }

    public Integer getNuversion() {
	return nuversion;
    }

    public void setNuversion(Integer nuversion) {
	this.nuversion = nuversion;
    }

    public List<Tmct0bok> getTmct0boks() {
	return tmct0boks;
    }

    public void setTmct0boks(List<Tmct0bok> tmct0boks) {
	this.tmct0boks = tmct0boks;
    }

    public List<Tmct0ord> getTmct0ords() {
	return tmct0ords;
    }

    public void setTmct0ords(List<Tmct0ord> tmct0ords) {
	this.tmct0ords = tmct0ords;
    }

    public List<Tmct0msc> getTmct0mscs() {
	return tmct0mscs;
    }

    public void setTmct0mscs(List<Tmct0msc> tmct0mscs) {
	this.tmct0mscs = tmct0mscs;
    }

    public List<Tmct0alo> getTmct0alos() {
	return tmct0alos;
    }

    public void setTmct0alos(List<Tmct0alo> tmct0alos) {
	this.tmct0alos = tmct0alos;
    }

    public List<Tmct0alc> getTmct0alcs() {
	return tmct0alcs;
    }

    public void setTmct0alcs(List<Tmct0alc> tmct0alcs) {
	this.tmct0alcs = tmct0alcs;
    }

    public List<Tmct0men> getTmct0mens() {
	return tmct0mens;
    }

    public void setTmct0mens(List<Tmct0men> tmct0mens) {
	this.tmct0mens = tmct0mens;
    }

    public List<Tmct0cta> getTmct0ctas() {
	return tmct0ctas;
    }

    public void setTmct0ctas(List<Tmct0cta> tmct0ctas) {
	this.tmct0ctas = tmct0ctas;
    }

    public List<Tmct0ctg> getTmct0ctgs() {
	return tmct0ctgs;
    }

    public void setTmct0ctgs(List<Tmct0ctg> tmct0ctgs) {
	this.tmct0ctgs = tmct0ctgs;
    }

    public List<Tmct0cto> getTmct0ctos() {
	return tmct0ctos;
    }

    public void setTmct0ctos(List<Tmct0cto> tmct0ctos) {
	this.tmct0ctos = tmct0ctos;
    }

    public static long getSerialversionuid() {
	return serialVersionUID;
    }

    public static Tmct0staData copy(Tmct0staData data) {
	Tmct0staData copy = new Tmct0staData();
	copy.setCdestret(data.getCdestret());
	copy.setCdusuaud(data.getCdusuaud());
	copy.setDsestado(data.getDsestado());
	copy.setFhaudit(data.getFhaudit());
	copy.setId(new Tmct0staId());
	copy.getId().setCdestado(data.getId().getCdestado());
	copy.getId().setCdtipest(data.getId().getCdtipest());
	copy.setNuversion(data.getNuversion());
	copy.setTmct0alcs(data.getTmct0alcs());
	copy.setTmct0alos(data.getTmct0alos());
	copy.setTmct0boks(data.getTmct0boks());
	copy.setTmct0ctas(data.getTmct0ctas());
	copy.setTmct0ctgs(data.getTmct0ctgs());
	copy.setTmct0ctos(data.getTmct0ctos());
	copy.setTmct0mens(data.getTmct0mens());
	copy.setTmct0mscs(data.getTmct0mscs());
	copy.setTmct0ords(data.getTmct0ords());
	copy.setTmct0stt(data.getTmct0stt());
	return copy;
    }

}
