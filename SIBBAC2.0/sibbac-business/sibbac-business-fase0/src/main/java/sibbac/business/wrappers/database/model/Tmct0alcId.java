package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import sibbac.business.fase0.database.model.Tmct0aloId;

@Embeddable
public class Tmct0alcId implements java.io.Serializable, Comparable<Tmct0alcId> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  private static final long serialVersionUID = 3699004331398185394L;
  
  // MIEMBROS
  
  private String nbooking = "";
  private String nuorden = "";
  private short nucnfliq = 0;
  private String nucnfclt = "";

  public Tmct0alcId() {
  }

  public Tmct0alcId(String nbooking, String nuorden, short nucnfliq, String nucnfclt) {
    this.nbooking = nbooking;
    this.nuorden = nuorden;
    this.nucnfliq = nucnfliq;
    this.nucnfclt = nucnfclt;
  }

  public Tmct0alcId(Tmct0aloId idAlo, short nucnfliq) {
    this.nbooking = idAlo.getNbooking();
    this.nuorden = idAlo.getNuorden();
    this.nucnfliq = nucnfliq;
    this.nucnfclt = idAlo.getNucnfclt();
  }
  
  public Tmct0alcId(final Object[] arr) {
      final Object[] target;
      
      target = Arrays.copyOf(arr, arr.length);
      this.nbooking = (String) target[0];
      this.nuorden = (String) target[1];
      this.nucnfliq = ((BigDecimal) target[2]).shortValue();
      this.nucnfclt = (String) target[3];
  }
  
  @Column(name = "NBOOKING", nullable = false, length = 20)
  public String getNbooking() {
    return this.nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  @Column(name = "NUORDEN", nullable = false, length = 20)
  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  @Column(name = "NUCNFLIQ", nullable = false, precision = 4)
  public short getNucnfliq() {
    return this.nucnfliq;
  }

  public void setNucnfliq(short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  public String getNucnfclt() {
    return this.nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public boolean equals(Object other) {
    if ((this == other))
      return true;
    if ((other == null))
      return false;
    if (!(other instanceof Tmct0alcId))
      return false;
    Tmct0alcId castOther = (Tmct0alcId) other;

    return ((this.getNbooking() == castOther.getNbooking()) || (this.getNbooking() != null && castOther.getNbooking() != null && this.getNbooking()
                                                                                                                                     .equals(castOther.getNbooking())))
        && ((this.getNuorden() == castOther.getNuorden()) || (this.getNuorden() != null && castOther.getNuorden() != null && this.getNuorden()
                                                                                                                                 .equals(castOther.getNuorden())))
        && (this.getNucnfliq() == castOther.getNucnfliq())
        && ((this.getNucnfclt() == castOther.getNucnfclt()) || (this.getNucnfclt() != null && castOther.getNucnfclt() != null && this.getNucnfclt()
                                                                                                                                     .equals(castOther.getNucnfclt())));
  }

  public int hashCode() {
    int result = 17;

    result = 37 * result + (getNbooking() == null ? 0 : this.getNbooking().hashCode());
    result = 37 * result + (getNuorden() == null ? 0 : this.getNuorden().hashCode());
    result = 37 * result + this.getNucnfliq();
    result = 37 * result + (getNucnfclt() == null ? 0 : this.getNucnfclt().hashCode());
    return result;
  }

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return  MessageFormat.format("id[{0}/{1}/{2}/{3}]", nuorden, nbooking, nucnfclt, nucnfliq);
  }

  @Override
  public int compareTo(Tmct0alcId o) {
	  int result = getNuorden().compareTo(o.getNuorden());
	  if (result!=0) {
		  return result;
	  }
	  if ((result = getNbooking().compareTo(o.getNbooking()))!=0) {
		  return result;
	  }
	  if ((result = getNucnfclt().compareTo(o.getNucnfclt()))!=0) {
		  return result;
	  }
	  return getNucnfliq()-o.getNucnfliq();
  }
}
