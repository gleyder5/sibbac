package sibbac.business.wrappers.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table( name = "TMCT0_PARAMETRIZACION" )
public class Tmct0parametrizacion implements java.io.Serializable{
	
	private static final long				serialVersionUID		= -587180156505155231L;
	private Long							idParametrizacion;
	private Tmct0Regla						tmct0Regla;
	private String 							descripcion;
	private Date 							fecha_inicio;
	private Date 							fecha_fin;
	private String							activo;
	private Tmct0CamaraCompensacion			tmct0CamaraCompensacion;
	private Tmct0Segmento					tmct0Segmento;				
	private Tmct0ModeloDeNegocio			tmct0ModeloDeNegocio;
	private Tmct0IndicadorDeCapacidad		tmct0IndicadorCapacidad;
	private String							cdReferenciaAsignacion;
	private String							referenciaCliente;
	private String							referenciaExterna;
	private Tmct0Compensador				tmct0Compensador;
	private Tmct0Nemotecnicos				tmct0Nemotecnicos;
	private Tmct0CuentasDeCompensacion		tmct0CuentasDeCompensacion;
	private String							auditUsuario;
	private Date							auditFechaCambio;
	

	public Tmct0parametrizacion() {	}

	public Tmct0parametrizacion(Long idParametrizacion, Tmct0Regla tmct0Regla,
			String descripcion, Date fecha_inicio, Date fecha_fin,
			String activo, Tmct0CamaraCompensacion tmct0CamaraCompensacion,
			Tmct0Segmento tmct0Segmento,
			Tmct0ModeloDeNegocio tmct0ModeloDeNegocio,
			Tmct0IndicadorDeCapacidad tmct0IndicadorCapacidad,
			String cdReferenciaAsignacion, String referenciaCliente,
			String referenciaExterna, Tmct0Compensador tmct0Compensador,
			Tmct0Nemotecnicos tmct0Nemotecnicos,
			Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion,
			String auditUsuario, Date auditFechaCambio) {
		super();
		this.idParametrizacion = idParametrizacion;
		this.tmct0Regla = tmct0Regla;
		this.descripcion = descripcion;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.activo = activo;
		this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
		this.tmct0Segmento = tmct0Segmento;
		this.tmct0ModeloDeNegocio = tmct0ModeloDeNegocio;
		this.tmct0IndicadorCapacidad = tmct0IndicadorCapacidad;
		this.cdReferenciaAsignacion = cdReferenciaAsignacion;
		this.referenciaCliente = referenciaCliente;
		this.referenciaExterna = referenciaExterna;
		this.tmct0Compensador = tmct0Compensador;
		
		this.tmct0Nemotecnicos = tmct0Nemotecnicos;
		this.tmct0CuentasDeCompensacion = tmct0CuentasDeCompensacion;
		this.auditUsuario = auditUsuario;
		this.auditFechaCambio = auditFechaCambio;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column( name = "ID_PARAMETRIZACION", unique = true, nullable = false, length = 8 )
	public Long getIdParametrizacion() {
		return idParametrizacion;
	}

	public void setIdParametrizacion(Long idParametrizacion) {
		this.idParametrizacion = idParametrizacion;
	}
	
	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_REGLA" )
	//@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
	public Tmct0Regla getTmct0Regla() {
		return tmct0Regla;
	}

	public void setTmct0Regla(Tmct0Regla tmct0Regla) {
		this.tmct0Regla = tmct0Regla;
	}
	
	@Column( name = "DESCRIPCION", nullable = false, length = 255 )
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Temporal( TemporalType.DATE )
	@Column( name = "FECHA_INICIO", nullable = false, length = 4 )
	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHA_FIN", length = 4 )
	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	@Column( name = "ACTIVO", nullable = false, length = 1 )
	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_CAMARA" )
	public Tmct0CamaraCompensacion getTmct0CamaraCompensacion() {
		return tmct0CamaraCompensacion;
	}

	public void setTmct0CamaraCompensacion(Tmct0CamaraCompensacion tmct0CamaraCompensacion) {
		this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_SEGMENTO" )
	public Tmct0Segmento getTmct0Segmento() {
		return tmct0Segmento;
	}

	public void setTmct0Segmento(Tmct0Segmento tmct0Segmento) {
		this.tmct0Segmento = tmct0Segmento;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_MODELO_DE_NEGOCIO" )
	public Tmct0ModeloDeNegocio getTmct0ModeloDeNegocio() {
		return tmct0ModeloDeNegocio;
	}

	public void setTmct0ModeloDeNegocio(Tmct0ModeloDeNegocio tmct0ModeloDeNegocio) {
		this.tmct0ModeloDeNegocio = tmct0ModeloDeNegocio;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_INDICADOR_CAPACIDAD" )
	public Tmct0IndicadorDeCapacidad getTmct0IndicadorCapacidad() {
		return tmct0IndicadorCapacidad;
	}

	public void setTmct0IndicadorCapacidad(
			Tmct0IndicadorDeCapacidad tmct0IndicadorCapacidad) {
		this.tmct0IndicadorCapacidad = tmct0IndicadorCapacidad;
	}
	
	@Column( name = "CD_REFERENCIA_ASIGNACION", length = 64 )
	public String getCdReferenciaAsignacion() {
		return cdReferenciaAsignacion;
	}

	public void setCdReferenciaAsignacion(String cdReferenciaAsignacion) {
		this.cdReferenciaAsignacion = cdReferenciaAsignacion;
	}

	@Column( name = "REFERENCIA_CLIENTE", length = 45 )
	public String getReferenciaCliente() {
		return referenciaCliente;
	}

	public void setReferenciaCliente(String referenciaCliente) {
		this.referenciaCliente = referenciaCliente;
	}

	@Column( name = "REFERENCIA_EXTERNA", length = 45 )
	public String getReferenciaExterna() {
		return referenciaExterna;
	}

	public void setReferenciaExterna(String referenciaExterna) {
		this.referenciaExterna = referenciaExterna;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_COMPENSADOR" )
	public Tmct0Compensador getTmct0Compensador() {
		return tmct0Compensador;
	}

	public void setTmct0Compensador(Tmct0Compensador tmct0Compensador) {
		this.tmct0Compensador = tmct0Compensador;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_NEMOTECNICO" )
	public Tmct0Nemotecnicos getTmct0Nemotecnicos() {
		return tmct0Nemotecnicos;
	}

	public void setTmct0Nemotecnicos(Tmct0Nemotecnicos tmct0Nemotecnicos) {
		this.tmct0Nemotecnicos = tmct0Nemotecnicos;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_CUENTA_COMPENSACION" )
	public Tmct0CuentasDeCompensacion getTmct0CuentasDeCompensacion() {
		return tmct0CuentasDeCompensacion;
	}

	public void setTmct0CuentasDeCompensacion(
			Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion) {
		this.tmct0CuentasDeCompensacion = tmct0CuentasDeCompensacion;
	}

	@Column( name = "AUDIT_USER", nullable = false, length = 20 )
	public String getAuditUsuario() {
		return auditUsuario;
	}

	public void setAuditUsuario(String auditUsuario) {
		this.auditUsuario = auditUsuario;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "AUDIT_FECHA_CAMBIO", nullable = false, length = 10 )
	public Date getAuditFechaCambio() {
		return auditFechaCambio;
	}

	public void setAuditFechaCambio(Date auditFechaCambio) {
		this.auditFechaCambio = auditFechaCambio;
	}


}


