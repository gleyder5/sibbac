package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0CASEOPERACIONEJE")
public class Tmct0caseoperacioneje implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 4815157240289094741L;
  private Long idcase;
  private Tmct0anotacionecc tmct0anotacionecc;
  private Tmct0operacioncdecc tmct0operacioncdecc;
  private String cdoperacionecc;
  private BigDecimal nutitpendcase;
  private String nuexemkt;
  private String cdcamara;
  private Date feordenmkt;
  private Date hoordenmkt;
  private Date feexemkt;
  private Date hoexemkt;
  private String nuordenmkt;
  private char cdsentido;
  private String cdsegmento;
  private BigDecimal nutitulostotal;
  private String cdisin;
  private Date auditFechaCambio;
  private String auditUser;
  private String cdPlataformaNegociacion;
  private List<Tmct0eje> tmct0ejes;

  public Tmct0caseoperacioneje() {
  }

  public Tmct0caseoperacioneje(Long idcase,
                               String cdoperacionecc,
                               BigDecimal nutitpendcase,
                               String nuexemkt,
                               String cdcamara,
                               Date feordenmkt,
                               Date hoordenmkt,
                               Date feexemkt,
                               Date hoexemkt,
                               char cdsentido,
                               String cdsegmento,
                               BigDecimal nutitulostotal,
                               String cdisin,
                               String cdPlataformaNegociacion) {
    this.idcase = idcase;
    this.cdoperacionecc = cdoperacionecc;
    this.nutitpendcase = nutitpendcase;
    this.nuexemkt = nuexemkt;
    this.cdcamara = cdcamara;
    this.feordenmkt = feordenmkt;
    this.hoordenmkt = hoordenmkt;
    this.feexemkt = feexemkt;
    this.hoexemkt = hoexemkt;
    this.cdsentido = cdsentido;
    this.cdsegmento = cdsegmento;
    this.nutitulostotal = nutitulostotal;
    this.cdisin = cdisin;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  public Tmct0caseoperacioneje(Long idcase,
                               Tmct0anotacionecc tmct0anotacionecc,
                               Tmct0operacioncdecc tmct0operacioncdecc,
                               String cdoperacionecc,
                               BigDecimal nutitpendcase,
                               String nuexemkt,
                               String cdcamara,
                               Date feordenmkt,
                               Date hoordenmkt,
                               Date feexemkt,
                               Date hoexemkt,
                               String nuordenmkt,
                               char cdsentido,
                               String cdsegmento,
                               BigDecimal nutitulostotal,
                               String cdisin,
                               Date auditFechaCambio,
                               String auditUser,
                               String cdPlataformaNegociacion,
                               List<Tmct0eje> tmct0ejes) {
    this.idcase = idcase;
    this.tmct0anotacionecc = tmct0anotacionecc;
    this.tmct0operacioncdecc = tmct0operacioncdecc;
    this.cdoperacionecc = cdoperacionecc;
    this.nutitpendcase = nutitpendcase;
    this.nuexemkt = nuexemkt;
    this.cdcamara = cdcamara;
    this.feordenmkt = feordenmkt;
    this.hoordenmkt = hoordenmkt;
    this.feexemkt = feexemkt;
    this.hoexemkt = hoexemkt;
    this.nuordenmkt = nuordenmkt;
    this.cdsentido = cdsentido;
    this.cdsegmento = cdsegmento;
    this.nutitulostotal = nutitulostotal;
    this.cdisin = cdisin;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.tmct0ejes = tmct0ejes;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDCASE", unique = true, nullable = false, precision = 8)
  public Long getIdcase() {
    return this.idcase;
  }

  public void setIdcase(Long idcase) {
    this.idcase = idcase;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDANOTACIONECC")
  public Tmct0anotacionecc getTmct0anotacionecc() {
    return this.tmct0anotacionecc;
  }

  public void setTmct0anotacionecc(Tmct0anotacionecc tmct0anotacionecc) {
    this.tmct0anotacionecc = tmct0anotacionecc;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDOPERACIONECC")
  public Tmct0operacioncdecc getTmct0operacioncdecc() {
    return this.tmct0operacioncdecc;
  }

  public void setTmct0operacioncdecc(Tmct0operacioncdecc tmct0operacioncdecc) {
    this.tmct0operacioncdecc = tmct0operacioncdecc;
  }

  @Column(name = "CDOPERACIONECC", unique = true, nullable = false, length = 16)
  public String getCdoperacionecc() {
    return this.cdoperacionecc;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  @Column(name = "NUTITPENDCASE", nullable = false, precision = 18, scale = 6)
  public BigDecimal getNutitpendcase() {
    return this.nutitpendcase;
  }

  public void setNutitpendcase(BigDecimal nutitpendcase) {
    this.nutitpendcase = nutitpendcase;
  }

  @Column(name = "NUEXEMKT", nullable = false, length = 9)
  public String getNuexemkt() {
    return this.nuexemkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  @Column(name = "CDCAMARA", nullable = false, length = 4)
  public String getCdcamara() {
    return this.cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEORDENMKT", nullable = false, length = 4)
  public Date getFeordenmkt() {
    return this.feordenmkt;
  }

  public void setFeordenmkt(Date feordenmkt) {
    this.feordenmkt = feordenmkt;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOORDENMKT", nullable = false, length = 8)
  public Date getHoordenmkt() {
    return this.hoordenmkt;
  }

  public void setHoordenmkt(Date hoordenmkt) {
    this.hoordenmkt = hoordenmkt;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEXEMKT", nullable = false, length = 4)
  public Date getFeexemkt() {
    return this.feexemkt;
  }

  public void setFeexemkt(Date feexemkt) {
    this.feexemkt = feexemkt;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOEXEMKT", nullable = false, length = 3)
  public Date getHoexemkt() {
    return this.hoexemkt;
  }

  public void setHoexemkt(Date hoexemkt) {
    this.hoexemkt = hoexemkt;
  }

  @Column(name = "NUORDENMKT", length = 9)
  public String getNuordenmkt() {
    return this.nuordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  @Column(name = "CDSENTIDO", nullable = false, length = 1)
  public char getCdsentido() {
    return this.cdsentido;
  }

  public void setCdsentido(char cdsentido) {
    this.cdsentido = cdsentido;
  }

  @Column(name = "CDSEGMENTO", nullable = false, length = 2)
  public String getCdsegmento() {
    return this.cdsegmento;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  @Column(name = "NUTITULOSTOTAL", nullable = false, precision = 18, scale = 6)
  public BigDecimal getNutitulostotal() {
    return this.nutitulostotal;
  }

  public void setNutitulostotal(BigDecimal nutitulostotal) {
    this.nutitulostotal = nutitulostotal;
  }

  @Column(name = "CDISIN", nullable = false, length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "CD_PLATAFORMA_NEGOCIACION", nullable = false, length = 4)
  public String getCdPlataformaNegociacion() {
    return this.cdPlataformaNegociacion;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0caseoperacioneje")
  public List<Tmct0eje> getTmct0ejes() {
    return this.tmct0ejes;
  }

  public void setTmct0ejes(List<Tmct0eje> tmct0ejes) {
    this.tmct0ejes = tmct0ejes;
  }

}
