package sibbac.business.fase0.database.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0blqId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2833518261687171887L;

  /**
   * Table Field CDBROKER. BROKER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBROKER", length = 20)
  private java.lang.String cdbroker;

  /**
   * Table Field CENTRO. PLATAFORMA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CENTRO", length = 30)
  private java.lang.String centro;
  /**
   * Table Field CDMERCAD. MERCADO Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDMERCAD", length = 20)
  private java.lang.String cdmercad;

  /**
   * Table Field TPSETTLE. TIPO LIQUIDACION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "TPSETTLE", length = 25)
  private java.lang.String tpsettle;
  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUMSEC", length = 20, scale = 0)
  private java.math.BigDecimal numsec;

  public Tmct0blqId() {
  }

  public Tmct0blqId(String cdbroker, String centro, String cdmercad, String tpsettle, BigDecimal numsec) {
    super();
    this.cdbroker = cdbroker;
    this.centro = centro;
    this.cdmercad = cdmercad;
    this.tpsettle = tpsettle;
    this.numsec = numsec;
  }

  public java.lang.String getCdbroker() {
    return cdbroker;
  }

  public java.lang.String getCentro() {
    return centro;
  }

  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  public void setCdbroker(java.lang.String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCentro(java.lang.String centro) {
    this.centro = centro;
  }

  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  public void setNumsec(java.math.BigDecimal numsec) {
    this.numsec = numsec;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbroker == null) ? 0 : cdbroker.hashCode());
    result = prime * result + ((cdmercad == null) ? 0 : cdmercad.hashCode());
    result = prime * result + ((centro == null) ? 0 : centro.hashCode());
    result = prime * result + ((numsec == null) ? 0 : numsec.hashCode());
    result = prime * result + ((tpsettle == null) ? 0 : tpsettle.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0blqId other = (Tmct0blqId) obj;
    if (cdbroker == null) {
      if (other.cdbroker != null)
        return false;
    } else if (!cdbroker.equals(other.cdbroker))
      return false;
    if (cdmercad == null) {
      if (other.cdmercad != null)
        return false;
    } else if (!cdmercad.equals(other.cdmercad))
      return false;
    if (centro == null) {
      if (other.centro != null)
        return false;
    } else if (!centro.equals(other.centro))
      return false;
    if (numsec == null) {
      if (other.numsec != null)
        return false;
    } else if (!numsec.equals(other.numsec))
      return false;
    if (tpsettle == null) {
      if (other.tpsettle != null)
        return false;
    } else if (!tpsettle.equals(other.tpsettle))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tmct0blqId %s##%s##%s##%s##%s", cdbroker, cdmercad, centro, tpsettle, numsec);
  }

}
