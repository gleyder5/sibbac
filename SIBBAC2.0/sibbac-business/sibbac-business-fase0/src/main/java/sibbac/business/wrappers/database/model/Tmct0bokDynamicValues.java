package sibbac.business.wrappers.database.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0BOK_DYNAMIC_VALUES")
public class Tmct0bokDynamicValues extends DynamicValuesBase {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nombre", column = @Column(name = "NOMBRE", nullable = false, length = 32)) })
  private Tmct0bokDynamicValuesId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false) })
  private Tmct0bok tmct0bok;

  public Tmct0bokDynamicValuesId getId() {
    return id;
  }

  public void setId(Tmct0bokDynamicValuesId id) {
    this.id = id;
  }

  public Tmct0bok getTmct0bok() {
    return tmct0bok;
  }

  public void setTmct0bok(Tmct0bok tmct0bok) {
    this.tmct0bok = tmct0bok;
  }

}
