package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0tfiId implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  @Column(name = "CDALIASS")
  private String        cdaliass ="";
  
  @Column(name = "CDSUBCTA")
  private String        cdsubcta ="";
  
  @Column(name = "CENTRO") 
  private String        centro ="";

  @Column(name = "CDMERCAD") 
  private String        cdmercad ="";
  
  @Column(name = "CDDCLAVE")
  private String        cddclave ="";
  
  @Column(name = "NUMSEC")
  private BigDecimal numsec = BigDecimal.ZERO;
  
  public Tmct0tfiId() {

  }

  public Tmct0tfiId(String cdaliass, String cdsubcta, String centro, String cdmercad, String cddclave, BigDecimal numsec) {
    this.cdaliass = cdaliass;
    this.cdsubcta = cdsubcta;
    this.centro = centro;
    this.cdmercad = cdmercad;
    this.cddclave = cddclave;
    this.numsec = numsec;
  }

  public String getCdaliass() {
    return cdaliass;
  }

  public void setCdaliass(String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public String getCdsubcta() {
    return cdsubcta;
  }

  public void setCdsubcta(String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  public String getCentro() {
    return centro;
  }

  public void setCentro(String centro) {
    this.centro = centro;
  }

  public String getCdmercad() {
    return cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public String getCddclave() {
    return cddclave;
  }

  public void setCddclave(String cddclave) {
    this.cddclave = cddclave;
  }

  public BigDecimal getNumsec() {
    return numsec;
  }

  public void setNumsec(BigDecimal numsec) {
    this.numsec = numsec;
  }
  
  
  
  
  
  

}
