package sibbac.business.fase0.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dto.Tmct0estadoDTO;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceTmct0estado implements SIBBACServiceBean {

	@Autowired
	Tmct0estadoBo				tmct0estadoBo;

	private static final Logger	LOG										= LoggerFactory.getLogger( SIBBACServiceTmct0estado.class );

	private static final String	CMD_RECUPERAR_ESTADO_FACTURA			= "getEstadosFactura";
	private static final String	CMD_RECUPERAR_ESTADO_FACTURA_SUGERIDA	= "getEstadosFacturaSugerida";

	private static final String	RESULT_ESTADOS							= "estados";

	@Override
	public WebResponse process( WebRequest webRequest ) {
		final String prefix = "[SIBBACServiceEstado::process] ";
		LOG.debug( prefix + "Processing a web request..." );

		final String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );

		final WebResponse webResponse = new WebResponse();

		switch ( command ) {
			case CMD_RECUPERAR_ESTADO_FACTURA:
				this.getEstadosTipoEstado( webRequest, webResponse, Tmct0estado.FACTURA );
				break;
			case CMD_RECUPERAR_ESTADO_FACTURA_SUGERIDA:
				this.getEstadosFacturaSugeridaVisibles( webRequest, webResponse );
				break;

			default:
				webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
				break;
		}

		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {

		List< String > commands = new ArrayList< String >();
		commands.add( CMD_RECUPERAR_ESTADO_FACTURA );
		commands.add( CMD_RECUPERAR_ESTADO_FACTURA_SUGERIDA );

		return commands;
	}

	@Override
	public List< String > getFields() {

		return null;
	}

	private void getEstadosTipoEstado( WebRequest webRequest, WebResponse webResponse, final Integer idtipoestado ) {

		final Map< String, List< Tmct0estadoDTO >> resultado = new HashMap< String, List< Tmct0estadoDTO > >();

		final List< Tmct0estadoDTO > estados = tmct0estadoBo.findByIdtipoestadoOrderByNombreAsc( idtipoestado );
		if ( CollectionUtils.isNotEmpty( estados ) ) {
			resultado.put( RESULT_ESTADOS, estados );

		} else {
			webResponse.setError( "No se han encontrado estados con el id tipo estado: " + idtipoestado + "." );
		}
		webResponse.setResultados( resultado );
	}

	private void getEstadosFacturaSugeridaVisibles( WebRequest webRequest, WebResponse webResponse ) {
		final Map< String, List< Tmct0estadoDTO >> resultado = new HashMap< String, List< Tmct0estadoDTO > >();
		final List< Tmct0estadoDTO > estados = tmct0estadoBo.findEstadosSugueridasVisibles();
		if ( CollectionUtils.isNotEmpty( estados ) ) {
			resultado.put( RESULT_ESTADOS, estados );
		}
		webResponse.setResultados( resultado );
	}

}
