package sibbac.business.fase0.database.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0estId implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 2936591363049633132L;
  private String cdbrocli;
  private String cdaliass;
  private BigDecimal numsec;
  private String cdsubcta;

  public Tmct0estId() {
  }

  public Tmct0estId(String cdbrocli, String cdaliass, BigDecimal numsec, String cdsubcta) {
    this.cdbrocli = cdbrocli;
    this.cdaliass = cdaliass;
    this.numsec = numsec;
    this.cdsubcta = cdsubcta;
  }

  @Column(name = "CDSUBCTA", nullable = false, length = 20)
  public String getCdsubcta() {
    return cdsubcta;
  }

  public void setCdsubcta(String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  @Column(name = "CDBROCLI", nullable = false, length = 20)
  public String getCdbrocli() {
    return this.cdbrocli;
  }

  public void setCdbrocli(String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  @Column(name = "CDALIASS", nullable = false, length = 20)
  public String getCdaliass() {
    return this.cdaliass;
  }

  public void setCdaliass(String cdaliass) {
    this.cdaliass = cdaliass;
  }

  @Column(name = "NUMSEC", nullable = false, precision = 20)
  public BigDecimal getNumsec() {
    return this.numsec;
  }

  public void setNumsec(BigDecimal numsec) {
    this.numsec = numsec;
  }

  public boolean equals(Object other) {
    if ((this == other))
      return true;
    if ((other == null))
      return false;
    if (!(other instanceof Tmct0estId))
      return false;
    Tmct0estId castOther = (Tmct0estId) other;

    return ((this.getCdbrocli() == castOther.getCdbrocli()) || (this.getCdbrocli() != null
        && castOther.getCdbrocli() != null && this.getCdbrocli().equals(castOther.getCdbrocli())))
        && ((this.getCdaliass() == castOther.getCdaliass()) || (this.getCdaliass() != null
            && castOther.getCdaliass() != null && this.getCdaliass().equals(castOther.getCdaliass())))
        && ((this.getNumsec() == castOther.getNumsec()) || (this.getNumsec() != null && castOther.getNumsec() != null
            && this.getNumsec().equals(castOther.getNumsec())));
  }

  public int hashCode() {
    int result = 17;

    result = 37 * result + (getCdbrocli() == null ? 0 : this.getCdbrocli().hashCode());
    result = 37 * result + (getCdaliass() == null ? 0 : this.getCdaliass().hashCode());
    result = 37 * result + (getNumsec() == null ? 0 : this.getNumsec().hashCode());
    return result;
  }

}
