package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0ctoId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -21758318002051311L;
	private String				cdmercad;
	private String				nuorden;
	private String				cdbroker;

	public Tmct0ctoId() {
	}

	public Tmct0ctoId( String cdmercad, String nuorden, String cdbroker ) {
		this.cdmercad = cdmercad;
		this.nuorden = nuorden;
		this.cdbroker = cdbroker;
	}

	@Column( name = "CDMERCAD", nullable = false, length = 20 )
	public String getCdmercad() {
		return this.cdmercad;
	}

	public void setCdmercad( String cdmercad ) {
		this.cdmercad = cdmercad;
	}

	@Column( name = "NUORDEN", nullable = false, length = 20 )
	public String getNuorden() {
		return this.nuorden;
	}

	public void setNuorden( String nuorden ) {
		this.nuorden = nuorden;
	}

	@Column( name = "CDBROKER", nullable = false, length = 20 )
	public String getCdbroker() {
		return this.cdbroker;
	}

	public void setCdbroker( String cdbroker ) {
		this.cdbroker = cdbroker;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0ctoId ) )
			return false;
		Tmct0ctoId castOther = ( Tmct0ctoId ) other;

		return ( ( this.getCdmercad() == castOther.getCdmercad() ) || ( this.getCdmercad() != null && castOther.getCdmercad() != null && this
				.getCdmercad().equals( castOther.getCdmercad() ) ) )
				&& ( ( this.getNuorden() == castOther.getNuorden() ) || ( this.getNuorden() != null && castOther.getNuorden() != null && this
						.getNuorden().equals( castOther.getNuorden() ) ) )
				&& ( ( this.getCdbroker() == castOther.getCdbroker() ) || ( this.getCdbroker() != null && castOther.getCdbroker() != null && this
						.getCdbroker().equals( castOther.getCdbroker() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getCdmercad() == null ? 0 : this.getCdmercad().hashCode() );
		result = 37 * result + ( getNuorden() == null ? 0 : this.getNuorden().hashCode() );
		result = 37 * result + ( getCdbroker() == null ? 0 : this.getCdbroker().hashCode() );
		return result;
	}

}
