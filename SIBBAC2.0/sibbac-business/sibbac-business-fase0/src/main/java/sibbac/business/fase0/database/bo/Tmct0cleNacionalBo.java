package sibbac.business.fase0.database.bo;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0cleNacionalDao;
import sibbac.business.fase0.database.dto.Tmct0cleDTO;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0cleIdNacional;
import sibbac.business.fase0.database.model.Tmct0cleNacional;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0cleNacionalBo extends AbstractBo<Tmct0cleNacional, Tmct0cleIdNacional, Tmct0cleNacionalDao> {

  public List<Tmct0cleNacional> findByFechaAndCdcleare(final Date fecha, final String cdcleare) {
    Integer fecha_integer = new Integer(fecha.toString().replace("-", ""));
    return dao.findByFechaAndCdcleare(fecha_integer, cdcleare);
  }

  public List<Tmct0cleNacional> findByFechaAndCdcleare(final Date fecha, final String cdcleare, final String centro,
      final String cdmercad) {
    Integer fecha_integer = new Integer(fecha.toString().replace("-", ""));
    return dao.findByFechaAndCdcleare(fecha_integer, cdcleare, centro, cdmercad);
  }

  public Set<Tmct0cleDTO> findDistinctDscleareAndBiccleByFhfinal() {
    return dao.findDistinctDscleareAndBiccleByFhfinal(Tmct0ali.fhfinActivos);
  }

  public List<Tmct0cleNacional> findAllActiveNacional() {
    return dao.findAllActiveNacional();
  }

}
