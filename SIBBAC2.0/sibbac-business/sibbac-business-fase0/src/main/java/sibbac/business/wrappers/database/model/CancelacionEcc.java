package sibbac.business.wrappers.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "TMCT0CANCELACIONECC")
public class CancelacionEcc implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -702611007872504367L;

  @Id
  @Column(name = "IDCANCELACION", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected Long idcancelacion;

  @Column(name = "IDMOVIMIENTO", nullable = true)
  protected Long idmovimiento;
  
  @Column(name = "CDREFMOVIMIENTO", length = 30, nullable = true)
  protected java.lang.String cdrefmovimiento;
  
  @Column(name = "CDREFMOVIMIENTOECC", length = 20, nullable = true)
  protected java.lang.String cdrefmovimientoecc;
  
  @Column(name = "CDESTADOMOV", length = 2, nullable = false)
  protected java.lang.String cdestadomov;

  public Long getIdcancelacion() {
    return idcancelacion;
  }

  public void setIdcancelacion(Long idcancelacion) {
    this.idcancelacion = idcancelacion;
  }

  public Long getIdmovimiento() {
    return idmovimiento;
  }

  public void setIdmovimiento(Long idmovimiento) {
    this.idmovimiento = idmovimiento;
  }

  public java.lang.String getCdrefmovimiento() {
    return cdrefmovimiento;
  }

  public void setCdrefmovimiento(java.lang.String cdrefmovimiento) {
    this.cdrefmovimiento = cdrefmovimiento;
  }

  public java.lang.String getCdrefmovimientoecc() {
    return cdrefmovimientoecc;
  }

  public void setCdrefmovimientoecc(java.lang.String cdrefmovimientoecc) {
    this.cdrefmovimientoecc = cdrefmovimientoecc;
  }

  public java.lang.String getCdestadomov() {
    return cdestadomov;
  }

  public void setCdestadomov(java.lang.String cdestadomov) {
    this.cdestadomov = cdestadomov;
  }

}
