package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import sibbac.database.DBConstants;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table( name = DBConstants.WRAPPERS.ADDRESSES )
public class Tmct0Address  implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8970673967653451946L;
	
	 @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  @Column(name = "IDADDRESS", length = 8, nullable = false)
	  private Long idAddress;
	
	
	 @JsonIgnore
		@OneToOne( optional = true )
		@JoinColumn( name = "IDCLIENT", referencedColumnName = "IDCLIENT", nullable = true )
	private Tmct0cli			cliente;
	
	
	
	@Column( name = "CDDEPAIS", length = 3, nullable = false )
	private String				cdDePais;
	
	@Column( name = "TPDOMICI", length = 2, nullable = false )
	private String				tpDomici;
	
	@Column( name = "NBDOMICI", length = 40, nullable = false )
	private String				nbDomici;
	
	@Column( name = "NUDOMICI", length = 40, nullable = false )
	private String				nuDomici;
	
	@Column( name = "NBCIUDAD", length = 40, nullable = false )
	private String				nbCiudad;
	
	@Column( name = "NBPROVIN", length = 40, nullable = false )
	private String				nbProvin;
	
	@Column( name = "CDPOSTAL", length = 5, nullable = false )
	private String				cdPostal;
	
	
	

	public Tmct0Address() {
	
	}

	

	public Tmct0Address(Long idAddress, String cdDePais, String tpDomici,
			String nbDomici, String nuDomici, String nbCiudad, String nbProvin, String cdPostal) {
		super();
		this.idAddress = idAddress;
		this.cdDePais = cdDePais;
		this.tpDomici = tpDomici;
		this.nbDomici = nbDomici;
		this.nuDomici = nuDomici;
		this.nbCiudad = nbCiudad;
		this.nbProvin = nbProvin;
		this.cdPostal = cdPostal;
			
	}


	public String getdesProvincia() {
		return this.nbProvin;
	}



	
	public Tmct0cli getCliente() {
		return cliente;
	}



	public void setCliente(Tmct0cli cliente) {
		this.cliente = cliente;
	}



	public String getCdDePais() {
		return cdDePais;
	}



	public void setCdDePais(String cdDePais) {
		this.cdDePais = cdDePais;
	}



	public String getTpDomici() {
		return tpDomici;
	}



	public void setTpDomici(String tpDomici) {
		this.tpDomici = tpDomici;
	}



	public String getNbDomici() {
		return nbDomici;
	}



	public void setNbDomici(String nbDomici) {
		this.nbDomici = nbDomici;
	}



	public String getNuDomici() {
		return nuDomici;
	}



	public void setNuDomici(String nuDomici) {
		this.nuDomici = nuDomici;
	}



	public String getNbCiudad() {
		return nbCiudad;
	}



	public void setNbCiudad(String nbCiudad) {
		this.nbCiudad = nbCiudad;
	}



	public String getNbProvin() {
		return nbProvin;
	}



	public void setNbProvin(String nbProvin) {
		this.nbProvin = nbProvin;
	}



	public String getCdPostal() {
		return cdPostal;
	}



	public void setCdPostal(String cdPostal) {
		this.cdPostal = cdPostal;
	}



	/**
	 * @return the idAddress
	 */
	public Long getIdAddress() {
		return idAddress;
	}



	/**
	 * @param idAddress the idAddress to set
	 */
	public void setIdAddress(Long idAddress) {
		this.idAddress = idAddress;
	}



	public String getDescripcion() {
		return getNbDomici() + " " + getNuDomici();
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdDePais == null) ? 0 : cdDePais.hashCode());
		result = prime * result + ((cdPostal == null) ? 0 : cdPostal.hashCode());
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((idAddress == null) ? 0 : idAddress.hashCode());
		result = prime * result + ((nbCiudad == null) ? 0 : nbCiudad.hashCode());
		result = prime * result + ((nbDomici == null) ? 0 : nbDomici.hashCode());
		result = prime * result + ((nbProvin == null) ? 0 : nbProvin.hashCode());
		result = prime * result + ((nuDomici == null) ? 0 : nuDomici.hashCode());
		result = prime * result + ((tpDomici == null) ? 0 : tpDomici.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tmct0Address other = (Tmct0Address) obj;

		if (idAddress == null) {
			if (other.idAddress != null)
				return false;
		} else if (!idAddress.equals(other.idAddress))
			return false;
		
		if (cdDePais == null) {
			if (other.cdDePais != null)
				return false;
		} else if (!cdDePais.equals(other.cdDePais))
			return false;
		if (cdPostal == null) {
			if (other.cdPostal != null)
				return false;
		} else if (!cdPostal.equals(other.cdPostal))
			return false;
		if (nbCiudad == null) {
			if (other.nbCiudad != null)
				return false;
		} else if (!nbCiudad.equals(other.nbCiudad))
			return false;
		if (nbDomici == null) {
			if (other.nbDomici != null)
				return false;
		} else if (!nbDomici.equals(other.nbDomici))
			return false;
		if (nbProvin == null) {
			if (other.nbProvin != null)
				return false;
		} else if (!nbProvin.equals(other.nbProvin))
			return false;
		if (nuDomici == null) {
			if (other.nuDomici != null)
				return false;
		} else if (!nuDomici.equals(other.nuDomici))
			return false;
		if (tpDomici == null) {
			if (other.tpDomici != null)
				return false;
		} else if (!tpDomici.equals(other.tpDomici))
			return false;
		return true;
	}



	

	
	
	
	
	
	
	
	

}
