package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0EJE")
public class Tmct0eje implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 6047072124244780988L;
  private Tmct0ejeId id;
  private Tmct0bok tmct0bok;
  private Tmct0caseoperacioneje tmct0caseoperacioneje;
  private BigDecimal nutiteje = BigDecimal.ZERO;
  private String dsobserv;
  private String cdalias;
  private String cdbroker;
  private Character cdcaptur;
  private String cdclente;
  private String cdisin;
  private String cdmercad;
  private String cdmoniso;
  private String cdordctp;
  private Character cdtpoper;
  private String cdvia;
  private Date feejecuc;
  private Date hoejecuc;
  private Date fevalor;
  private BigDecimal imcbmerc;
  private String nuopemer;
  private String nurefere;
  private String nuagreje;
  private String cdusucon;
  private Short nupareje;
  private BigDecimal pccambio;
  private String tpopebol;
  private BigDecimal pcefecup;
  private BigDecimal pclimcam;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuversion;
  private char cuadreau;
  private BigDecimal rfcupon;
  private BigDecimal imnetrf;
  private String numiberclear;
  private String platliqu;
  private String platneg;
  private String ctacontrap;
  private String operamer;
  private String refextorden;
  private String entpart;
  private String refclteorden;
  private String tpactivo;
  private Integer nuejeaud;
  private Character casepti;
  private Date feordenmkt;
  private Date hoordenmkt;
  private String cdoperacion;
  private String segmenttradingvenue;
  private String clearingplatform;
  private Integer asignadocomp;
  private String nemotecnico;
  private String ctacomp;
  private String miembrocomp;
  private String refadicional;
  private Date feintmer;
  private String refasignacion;
  private String ccv;
  private String participante;
  private String ctaliquidacion;
  private String liqtiemporeal;
  private String liqparcial;
  private String cdMiembroMkt;
  private String executionTradingVenue;
  private Character indCotizacion;
  private Character noEccRealTimeInd;
  private Character noEccPartialSettleInd;
  private Date noEccSettleDay;
  private String noEccEntity;
  private String noEccSettleAccount;
  private String noEccSettleClientCcv;
  private BigDecimal nutittotal = BigDecimal.ZERO;

  protected BigDecimal imCanonCompDv = BigDecimal.ZERO;

  protected BigDecimal imCanonCompEu = BigDecimal.ZERO;

  protected BigDecimal imCanonContrDv = BigDecimal.ZERO;

  protected BigDecimal imCanonContrEu = BigDecimal.ZERO;

  protected BigDecimal imCanonLiqDv = BigDecimal.ZERO;

  protected BigDecimal imCanonLiqEu = BigDecimal.ZERO;

  private List<Tmct0ejecucionalocation> tmct0ejecucionalocations = new ArrayList<Tmct0ejecucionalocation>();

  private List<Tmct0ejeDynamicValues> dynamicValues = new ArrayList<Tmct0ejeDynamicValues>();

  private Date fechaHoraEjec;

  private Tmct0ejeFeeSchema tmct0ejeFeeSchema;

  private String realExecutionTradingVenue;

  public Tmct0eje() {
  }

  public Tmct0eje(Tmct0ejeId id, Tmct0bok tmct0bok, String nuagreje, char cuadreau) {
    this.id = id;
    this.tmct0bok = tmct0bok;
    this.nuagreje = nuagreje;
    this.cuadreau = cuadreau;
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nuejecuc", column = @Column(name = "NUEJECUC", nullable = false, length = 20)) })
  public Tmct0ejeId getId() {
    return this.id;
  }

  public void setId(Tmct0ejeId id) {
    this.id = id;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false) })
  public Tmct0bok getTmct0bok() {
    return this.tmct0bok;
  }

  public void setTmct0bok(Tmct0bok tmct0bok) {
    this.tmct0bok = tmct0bok;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDCASE", referencedColumnName = "IDCASE")
  public Tmct0caseoperacioneje getTmct0caseoperacioneje() {
    return this.tmct0caseoperacioneje;
  }

  public void setTmct0caseoperacioneje(Tmct0caseoperacioneje tmct0caseoperacioneje) {
    this.tmct0caseoperacioneje = tmct0caseoperacioneje;
  }

  @Column(name = "NUTITEJE", precision = 16, scale = 5)
  public BigDecimal getNutiteje() {
    return this.nutiteje;
  }

  public void setNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje;
  }

  @Column(name = "DSOBSERV", length = 200)
  public String getDsobserv() {
    return this.dsobserv;
  }

  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }

  @Column(name = "CDALIAS", length = 20)
  public String getCdalias() {
    return this.cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  @Column(name = "CDBROKER", length = 20)
  public String getCdbroker() {
    return this.cdbroker;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  @Column(name = "CDCAPTUR", length = 1)
  public Character getCdcaptur() {
    return this.cdcaptur;
  }

  public void setCdcaptur(Character cdcaptur) {
    this.cdcaptur = cdcaptur;
  }

  @Column(name = "CDCLENTE", length = 20)
  public String getCdclente() {
    return this.cdclente;
  }

  public void setCdclente(String cdclente) {
    this.cdclente = cdclente;
  }

  @Column(name = "CDISIN", length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "CDMERCAD", length = 20)
  public String getCdmercad() {
    return this.cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  @Column(name = "CDMONISO", length = 3)
  public String getCdmoniso() {
    return this.cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  @Column(name = "CDORDCTP", length = 4)
  public String getCdordctp() {
    return this.cdordctp;
  }

  public void setCdordctp(String cdordctp) {
    this.cdordctp = cdordctp;
  }

  @Column(name = "CDTPOPER", length = 1)
  public Character getCdtpoper() {
    return this.cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  @Column(name = "CDVIA", length = 4)
  public String getCdvia() {
    return this.cdvia;
  }

  public void setCdvia(String cdvia) {
    this.cdvia = cdvia;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC", length = 10)
  public Date getFeejecuc() {
    return this.feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOEJECUC", length = 8)
  public Date getHoejecuc() {
    return this.hoejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 10)
  public Date getFevalor() {
    return this.fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  @Column(name = "IMCBMERC", precision = 18, scale = 8)
  public BigDecimal getImcbmerc() {
    return this.imcbmerc;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  @Column(name = "NUOPEMER", length = 32)
  public String getNuopemer() {
    return this.nuopemer;
  }

  public void setNuopemer(String nuopemer) {
    this.nuopemer = nuopemer;
  }

  @Column(name = "NUREFERE", length = 100)
  public String getNurefere() {
    return this.nurefere;
  }

  public void setNurefere(String nurefere) {
    this.nurefere = nurefere;
  }

  @Column(name = "NUAGREJE", nullable = false, length = 20)
  public String getNuagreje() {
    return this.nuagreje;
  }

  public void setNuagreje(String nuagreje) {
    this.nuagreje = nuagreje;
  }

  @Column(name = "CDUSUCON", length = 8)
  public String getCdusucon() {
    return this.cdusucon;
  }

  public void setCdusucon(String cdusucon) {
    this.cdusucon = cdusucon;
  }

  @Column(name = "NUPAREJE", precision = 4)
  public Short getNupareje() {
    return this.nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  @Column(name = "PCCAMBIO", precision = 11, scale = 4)
  public BigDecimal getPccambio() {
    return this.pccambio;
  }

  public void setPccambio(BigDecimal pccambio) {
    this.pccambio = pccambio;
  }

  @Column(name = "TPOPEBOL", length = 2)
  public String getTpopebol() {
    return this.tpopebol;
  }

  public void setTpopebol(String tpopebol) {
    this.tpopebol = tpopebol;
  }

  @Column(name = "PCEFECUP", precision = 18, scale = 4)
  public BigDecimal getPcefecup() {
    return this.pcefecup;
  }

  public void setPcefecup(BigDecimal pcefecup) {
    this.pcefecup = pcefecup;
  }

  @Column(name = "PCLIMCAM", precision = 11, scale = 4)
  public BigDecimal getPclimcam() {
    return this.pclimcam;
  }

  public void setPclimcam(BigDecimal pclimcam) {
    this.pclimcam = pclimcam;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION")
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "CUADREAU", nullable = false, length = 1)
  public char getCuadreau() {
    return this.cuadreau;
  }

  public void setCuadreau(char cuadreau) {
    this.cuadreau = cuadreau;
  }

  @Column(name = "RFCUPON", precision = 18, scale = 8)
  public BigDecimal getRfcupon() {
    return this.rfcupon;
  }

  public void setRfcupon(BigDecimal rfcupon) {
    this.rfcupon = rfcupon;
  }

  @Column(name = "IMNETRF", precision = 18, scale = 8)
  public BigDecimal getImnetrf() {
    return this.imnetrf;
  }

  public void setImnetrf(BigDecimal imnetrf) {
    this.imnetrf = imnetrf;
  }

  @Column(name = "NUMIBERCLEAR", length = 20)
  public String getNumiberclear() {
    return this.numiberclear;
  }

  public void setNumiberclear(String numiberclear) {
    this.numiberclear = numiberclear;
  }

  @Column(name = "PLATLIQU", length = 20)
  public String getPlatliqu() {
    return this.platliqu;
  }

  public void setPlatliqu(String platliqu) {
    this.platliqu = platliqu;
  }

  @Column(name = "PLATNEG", length = 20)
  public String getPlatneg() {
    return this.platneg;
  }

  public void setPlatneg(String platneg) {
    this.platneg = platneg;
  }

  @Column(name = "CTACONTRAP", length = 4)
  public String getCtacontrap() {
    return this.ctacontrap;
  }

  public void setCtacontrap(String ctacontrap) {
    this.ctacontrap = ctacontrap;
  }

  @Column(name = "OPERAMER", length = 20)
  public String getOperamer() {
    return this.operamer;
  }

  public void setOperamer(String operamer) {
    this.operamer = operamer;
  }

  @Column(name = "REFEXTORDEN", length = 20)
  public String getRefextorden() {
    return this.refextorden;
  }

  public void setRefextorden(String refextorden) {
    this.refextorden = refextorden;
  }

  @Column(name = "ENTPART", length = 20)
  public String getEntpart() {
    return this.entpart;
  }

  public void setEntpart(String entpart) {
    this.entpart = entpart;
  }

  @Column(name = "REFCLTEORDEN", length = 20)
  public String getRefclteorden() {
    return this.refclteorden;
  }

  public void setRefclteorden(String refclteorden) {
    this.refclteorden = refclteorden;
  }

  @Column(name = "TPACTIVO", length = 2)
  public String getTpactivo() {
    return this.tpactivo;
  }

  public void setTpactivo(String tpactivo) {
    this.tpactivo = tpactivo;
  }

  @Column(name = "NUEJEAUD")
  public Integer getNuejeaud() {
    return this.nuejeaud;
  }

  public void setNuejeaud(Integer nuejeaud) {
    this.nuejeaud = nuejeaud;
  }

  @Column(name = "CASEPTI", length = 1)
  public Character getCasepti() {
    return this.casepti;
  }

  public void setCasepti(Character casepti) {
    this.casepti = casepti;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEORDENMKT", length = 10)
  public Date getFeordenmkt() {
    return this.feordenmkt;
  }

  public void setFeordenmkt(Date feordenmkt) {
    this.feordenmkt = feordenmkt;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOORDENMKT", length = 8)
  public Date getHoordenmkt() {
    return this.hoordenmkt;
  }

  public void setHoordenmkt(Date hoordenmkt) {
    this.hoordenmkt = hoordenmkt;
  }

  @Column(name = "CDOPERACION", length = 16)
  public String getCdoperacion() {
    return this.cdoperacion;
  }

  public void setCdoperacion(String cdoperacion) {
    this.cdoperacion = cdoperacion;
  }

  @Column(name = "SEGMENTTRADINGVENUE", length = 20)
  public String getSegmenttradingvenue() {
    return this.segmenttradingvenue;
  }

  public void setSegmenttradingvenue(String segmenttradingvenue) {
    this.segmenttradingvenue = segmenttradingvenue;
  }

  @Column(name = "CLEARINGPLATFORM", length = 20)
  public String getClearingplatform() {
    return this.clearingplatform;
  }

  public void setClearingplatform(String clearingplatform) {
    this.clearingplatform = clearingplatform;
  }

  @Column(name = "ASIGNADOCOMP")
  public Integer getAsignadocomp() {
    return this.asignadocomp;
  }

  public void setAsignadocomp(Integer asignadocomp) {
    this.asignadocomp = asignadocomp;
  }

  @Column(name = "NEMOTECNICO", length = 10)
  public String getNemotecnico() {
    return this.nemotecnico;
  }

  public void setNemotecnico(String nemotecnico) {
    this.nemotecnico = nemotecnico;
  }

  @Column(name = "CTACOMP", length = 3)
  public String getCtacomp() {
    return this.ctacomp;
  }

  public void setCtacomp(String ctacomp) {
    this.ctacomp = ctacomp;
  }

  @Column(name = "MIEMBROCOMP", length = 4)
  public String getMiembrocomp() {
    return this.miembrocomp;
  }

  public void setMiembrocomp(String miembrocomp) {
    this.miembrocomp = miembrocomp;
  }

  @Column(name = "REFADICIONAL", length = 20)
  public String getRefadicional() {
    return this.refadicional;
  }

  public void setRefadicional(String refadicional) {
    this.refadicional = refadicional;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEINTMER", length = 10)
  public Date getFeintmer() {
    return this.feintmer;
  }

  public void setFeintmer(Date feintmer) {
    this.feintmer = feintmer;
  }

  @Column(name = "REFASIGNACION", length = 30)
  public String getRefasignacion() {
    return this.refasignacion;
  }

  public void setRefasignacion(String refasignacion) {
    this.refasignacion = refasignacion;
  }

  @Column(name = "CCV", length = 35, nullable = true)
  public String getCcv() {
    return this.ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  @Column(name = "PARTICIPANTE", length = 35, nullable = true)
  public String getParticipante() {
    return participante;
  }

  public void setParticipante(String participante) {
    this.participante = participante;
  }

  @Column(name = "CTALIQUIDACION", length = 35, nullable = true)
  public String getCtaliquidacion() {
    return ctaliquidacion;
  }

  public void setCtaliquidacion(String ctaliquidacion) {
    this.ctaliquidacion = ctaliquidacion;
  }

  @Column(name = "LIQ_TIEMPOREAL", length = 35, nullable = true)
  public String getLiqtiemporeal() {
    return liqtiemporeal;
  }

  public void setLiqtiemporeal(String liqtiemporeal) {
    this.liqtiemporeal = liqtiemporeal;
  }

  @Column(name = "LIQ_PARCIAL", length = 35, nullable = true)
  public String getLiqparcial() {
    return liqparcial;
  }

  public void setLiqparcial(String liqparcial) {
    this.liqparcial = liqparcial;
  }

  /**
   * @return the cdMiembroMkt
   */
  @Column(name = "CDMIEMBROMKT", length = 10, nullable = true)
  public String getCdMiembroMkt() {
    return cdMiembroMkt;
  }

  /**
   * @param cdMiembroMkt the cdMiembroMkt to set
   */
  public void setCdMiembroMkt(String cdMiembroMkt) {
    this.cdMiembroMkt = cdMiembroMkt;
  }

  @Column(name = "EXECUTION_TRADING_VENUE", length = 20, nullable = true)
  public String getExecutionTradingVenue() {
    return executionTradingVenue;
  }

  public void setExecutionTradingVenue(String executionTradingVenue) {
    this.executionTradingVenue = executionTradingVenue;
  }

  @Column(name = "IND_COTIZACION", length = 1, nullable = true)
  public Character getIndCotizacion() {
    return indCotizacion;
  }

  public void setIndCotizacion(Character indCotizacion) {
    this.indCotizacion = indCotizacion;
  }

  @Column(name = "NO_ECC_REAL_TIME_IND", length = 1, nullable = true)
  public Character getNoEccRealTimeInd() {
    return noEccRealTimeInd;
  }

  public void setNoEccRealTimeInd(Character noEccRealTimeInd) {
    this.noEccRealTimeInd = noEccRealTimeInd;
  }

  @Column(name = "NO_ECC_PARTIAL_SETTLE_IND", length = 1, nullable = true)
  public Character getNoEccPartialSettleInd() {
    return noEccPartialSettleInd;
  }

  public void setNoEccPartialSettleInd(Character noEccPartialSettleInd) {
    this.noEccPartialSettleInd = noEccPartialSettleInd;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "NO_ECC_SETTLE_DATE", length = 10, nullable = true)
  public Date getNoEccSettleDay() {
    return noEccSettleDay;
  }

  public void setNoEccSettleDay(Date noEccSettleDay) {
    this.noEccSettleDay = noEccSettleDay;
  }

  @Column(name = "NO_ECC_ENTITY", length = 16, nullable = true)
  public String getNoEccEntity() {
    return noEccEntity;
  }

  public void setNoEccEntity(String noEccEntity) {
    this.noEccEntity = noEccEntity;
  }

  @Column(name = "NO_ECC_SETTLE_ACCOUNT", length = 64, nullable = true)
  public String getNoEccSettleAccount() {
    return noEccSettleAccount;
  }

  public void setNoEccSettleAccount(String noEccSettleAccount) {
    this.noEccSettleAccount = noEccSettleAccount;
  }

  @Column(name = "NO_ECC_SETTLE_CLIENT_CCV", length = 32, nullable = true)
  public String getNoEccSettleClientCcv() {
    return noEccSettleClientCcv;
  }

  public void setNoEccSettleClientCcv(String noEccSettleClientCcv) {
    this.noEccSettleClientCcv = noEccSettleClientCcv;
  }

  @Column(name = "NUTITTOTAL", length = 16, scale = 5, nullable = true)
  public BigDecimal getNutittotal() {
    return nutittotal;
  }

  public void setNutittotal(BigDecimal nutittotal) {
    this.nutittotal = nutittotal;
  }

  @Column(name = "IMCANONCOMPDV", length = 18, scale = 8)
  public BigDecimal getImCanonCompDv() {
    return imCanonCompDv;
  }

  @Column(name = "IMCANONCOMPEU", length = 18, scale = 8)
  public BigDecimal getImCanonCompEu() {
    return imCanonCompEu;
  }

  @Column(name = "IMCANONCONTRDV", length = 18, scale = 8)
  public BigDecimal getImCanonContrDv() {
    return imCanonContrDv;
  }

  @Column(name = "IMCANONCONTREU", length = 18, scale = 8)
  public BigDecimal getImCanonContrEu() {
    return imCanonContrEu;
  }

  @Column(name = "IMCANONLIQDV", length = 18, scale = 8)
  public BigDecimal getImCanonLiqDv() {
    return imCanonLiqDv;
  }

  @Column(name = "IMCANONLIQEU", length = 18, scale = 8)
  public BigDecimal getImCanonLiqEu() {
    return imCanonLiqEu;
  }

  public void setImCanonCompDv(BigDecimal imCanonCompDv) {
    this.imCanonCompDv = imCanonCompDv;
  }

  public void setImCanonCompEu(BigDecimal imCanonCompEu) {
    this.imCanonCompEu = imCanonCompEu;
  }

  public void setImCanonContrDv(BigDecimal imCanonContrDv) {
    this.imCanonContrDv = imCanonContrDv;
  }

  public void setImCanonContrEu(BigDecimal imCanonContrEu) {
    this.imCanonContrEu = imCanonContrEu;
  }

  public void setImCanonLiqDv(BigDecimal imCanonLiqDv) {
    this.imCanonLiqDv = imCanonLiqDv;
  }

  public void setImCanonLiqEu(BigDecimal imCanonLiqEu) {
    this.imCanonLiqEu = imCanonLiqEu;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0eje")
  public List<Tmct0ejecucionalocation> getTmct0ejecucionalocations() {
    return this.tmct0ejecucionalocations;
  }

  public void setTmct0ejecucionalocations(List<Tmct0ejecucionalocation> tmct0ejecucionalocations) {
    this.tmct0ejecucionalocations = tmct0ejecucionalocations;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0eje", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
  public List<Tmct0ejeDynamicValues> getDynamicValues() {
    return dynamicValues;
  }

  public void setDynamicValues(List<Tmct0ejeDynamicValues> dynamicValues) {
    this.dynamicValues = dynamicValues;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FECHA_HORA_EJEC")
  public Date getFechaHoraEjec() {
    return fechaHoraEjec;
  }

  public void setFechaHoraEjec(Date fechaHoraEjec) {
    this.fechaHoraEjec = fechaHoraEjec;
  }

  @OneToOne(fetch = FetchType.LAZY, mappedBy = "tmct0eje", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
  public Tmct0ejeFeeSchema getTmct0ejeFeeSchema() {
    return tmct0ejeFeeSchema;
  }

  public void setTmct0ejeFeeSchema(Tmct0ejeFeeSchema tmct0ejeFeeSchema) {
    this.tmct0ejeFeeSchema = tmct0ejeFeeSchema;
  }

  @Column(name = "REAL_EXECUTION_TRADING_VENUE", length = 20, nullable = true)
  public String getRealExecutionTradingVenue() {
    return realExecutionTradingVenue;
  }

  public void setRealExecutionTradingVenue(String realExecutionTradingVenue) {
    this.realExecutionTradingVenue = realExecutionTradingVenue;
  }

}
