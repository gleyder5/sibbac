package sibbac.business.wrappers.database.data;


import static sibbac.common.utils.FormatDataUtils.convertDateToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;


/**
 * The Class MovimientoCuentaAliasData.
 */
public class MovimientoCuentaAliasData implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long			serialVersionUID	= 1L;
	private long id;
	/** The movimiento cuenta alias. */
	public Tmct0MovimientoCuentaAlias	movimientoCuentaAlias;

	/** The cdcodigocuentaliq. */
	private String						cdcodigocuentaliq;

	/** The cdalias. */
	private String						cdalias;

	/** The descali. */
	private String						descali;

	/** The fliquidacion. */
	private String						fliquidacion;

	/** The isin. */
	private String						isin;

	/** The descr isin. */
	private String						descrIsin;

	/** The camara. */
	private String						camara;

	/** The cdoperacion. */
	private String						cdoperacion;

	/** The descripcion operacion. */
	private String						descripcionOperacion;

	/** The signo anotacion. */
	private Character					signoAnotacion;

	/** The fcontratacion. */
	private String						fcontratacion;

	/** The titulos. */
	private BigDecimal					titulos;

	/** The efectivo. */
	private BigDecimal					efectivo;

	/** The fhinicio. */
	private Integer						fhinicio;

	/** The fhfinal. */
	private Integer						fhfinal;

	/** The settlement date. */
	private Date						settlementDate;

	/** The corretaje. */
	private BigDecimal					corretaje;

	/** The audit user. */
	private String						auditUser;

	/** The audit date. */
	private String						auditDate;
	
	private String						sistemaLiq;
	private Date tradedate;
	private Date fechaRegistro;
	private Integer prioridadOperacion;
	
	public MovimientoCuentaAliasData() {
		super();
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param cdcodigocuentaliq
	 *            the cdcodigocuentaliq
	 * @param cdalias
	 *            the cdalias
	 * @param descali
	 *            the descali
	 * @param fliquidacion
	 *            the fliquidacion
	 * @param isin
	 *            the isin
	 * @param descrIsin
	 *            the descr isin
	 * @param camara
	 *            the camara
	 * @param cdoperacion
	 *            the cdoperacion
	 * @param descripcionesOperacion
	 *            the descripciones operacion
	 * @param signoAnotacion
	 *            the signo anotacion
	 * @param fcontratacion
	 *            the fcontratacion
	 * @param titulos
	 *            the titulos
	 * @param efectivo
	 *            the efectivo
	 * @param fhinicio
	 *            the fhinicio
	 * @param fhfinal
	 *            the fhfinal
	 * @param corretaje
	 *            the corretaje
	 */
	public MovimientoCuentaAliasData( String cdcodigocuentaliq, String cdalias, String descali, Date fliquidacion, String isin,
			String descrIsin, String camara, String cdoperacion, List< String > descripcionesOperacion, Character signoAnotacion,
			Date fcontratacion, BigDecimal titulos, BigDecimal efectivo, Integer fhinicio, int fhfinal, BigDecimal corretaje ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.descripcionOperacion = ( descripcionesOperacion != null && descripcionesOperacion.size() > 0 ) ? descripcionesOperacion
				.get( 0 ) : "";
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.descali = ( descali != null ) ? descali.trim() : "";
		this.fliquidacion = ( fliquidacion != null ) ? convertDateToString( fliquidacion ) : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.descrIsin = ( descrIsin != null ) ? descrIsin.trim() : "";
		this.camara = ( camara != null ) ? camara.trim() : "";
		this.cdoperacion = ( cdoperacion != null ) ? cdoperacion.trim() : "";
		this.signoAnotacion = signoAnotacion;
		this.fcontratacion = ( fcontratacion != null ) ? convertDateToString( fcontratacion ) : null;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.fhinicio = fhinicio;
		this.fhfinal = fhfinal;
		this.corretaje = ( corretaje == null ) ? BigDecimal.ZERO : corretaje;
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param cdcodigocuentaliq the cdcodigocuentaliq
	 * @param cdalias the cdalias
	 * @param fliquidacion the fliquidacion
	 * @param isin the isin
	 * @param descrIsin the descr isin
	 * @param camara the camara
	 * @param cdoperacion the cdoperacion
	 * @param descripcionesOperacion the descripciones operacion
	 * @param signoAnotacion the signo anotacion
	 * @param fcontratacion the fcontratacion
	 * @param titulos the titulos
	 * @param efectivo the efectivo
	 * @param corretaje the corretaje
	 * @param auditUser the audit user
	 * @param auditDate the audit date
	 */
	public MovimientoCuentaAliasData( String cdcodigocuentaliq, String cdalias, Date fliquidacion, String isin, String descrIsin,
			String camara, String cdoperacion, List< String > descripcionesOperacion, Character signoAnotacion, Date fcontratacion,
			BigDecimal titulos, BigDecimal efectivo, BigDecimal corretaje ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.fliquidacion = ( fliquidacion != null ) ? convertDateToString( fliquidacion ) : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.descrIsin = ( descrIsin != null ) ? descrIsin.trim() : "";
		this.camara = ( camara != null ) ? camara.trim() : "";
		this.cdoperacion = ( cdoperacion != null ) ? cdoperacion.trim() : "";
		this.descripcionOperacion = ( descripcionesOperacion != null && descripcionesOperacion.size() > 0 ) ? descripcionesOperacion
				.get( 0 ) : "";
		this.signoAnotacion = signoAnotacion;
		this.fcontratacion = ( fcontratacion != null ) ? convertDateToString( fcontratacion ) : null;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = ( corretaje == null ) ? BigDecimal.ZERO : corretaje;
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param cdcodigocuentaliq
	 *            the cdcodigocuentaliq
	 * @param cdalias
	 *            the cdalias
	 * @param fliquidacion
	 *            the fliquidacion
	 * @param isin
	 *            the isin
	 * @param descrIsin
	 *            the descr isin
	 * @param camara
	 *            the camara
	 * @param cdoperacion
	 *            the cdoperacion
	 * @param descripcionOperacion
	 *            the descripcion operacion
	 * @param signoAnotacion
	 *            the signo anotacion
	 * @param fcontratacion
	 *            the fcontratacion
	 * @param titulos
	 *            the titulos
	 * @param efectivo
	 *            the efectivo
	 * @param corretaje
	 *            the corretaje
	 */
	public MovimientoCuentaAliasData( String cdcodigocuentaliq, String cdalias, Date fliquidacion, String isin, String descrIsin,
			String camara, String cdoperacion, String descripcionOperacion, Character signoAnotacion, Date fcontratacion,
			BigDecimal titulos, BigDecimal efectivo, BigDecimal corretaje, String auditUser, String auditDate ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.fliquidacion = ( fliquidacion != null ) ? convertDateToString( fliquidacion ) : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.descrIsin = ( descrIsin != null ) ? descrIsin.trim() : "";
		this.camara = ( camara != null ) ? camara.trim() : "";
		this.cdoperacion = ( cdoperacion != null ) ? cdoperacion.trim() : "";
		this.descripcionOperacion = ( descripcionOperacion != null ) ? descripcionOperacion : "";
		this.signoAnotacion = signoAnotacion;
		this.fcontratacion = ( fcontratacion != null ) ? convertDateToString( fcontratacion ) : null;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = ( corretaje == null ) ? BigDecimal.ZERO : corretaje;
		this.auditUser = ( auditUser == null ) ? "" : auditUser;
		this.auditDate = auditDate;
	}
	
	public MovimientoCuentaAliasData( String cdcodigocuentaliq, String cdalias, Date fliquidacion, String isin, String descrIsin,
			String camara, String cdoperacion, String descripcionOperacion, Character signoAnotacion, Date fcontratacion,
			BigDecimal titulos, BigDecimal efectivo, BigDecimal corretaje, String auditUser, String auditDate, String sistemaLiq ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.fliquidacion = ( fliquidacion != null ) ? convertDateToString( fliquidacion ) : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.descrIsin = ( descrIsin != null ) ? descrIsin.trim() : "";
		this.camara = ( camara != null ) ? camara.trim() : "";
		this.cdoperacion = ( cdoperacion != null ) ? cdoperacion.trim() : "";
		this.descripcionOperacion = ( descripcionOperacion != null ) ? descripcionOperacion : "";
		this.signoAnotacion = signoAnotacion;
		this.fcontratacion = ( fcontratacion != null ) ? convertDateToString( fcontratacion ) : null;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = ( corretaje == null ) ? BigDecimal.ZERO : corretaje;
		this.auditUser = ( auditUser == null ) ? "" : auditUser;
		this.auditDate = auditDate;
		this.sistemaLiq = sistemaLiq;
	}

	public MovimientoCuentaAliasData( String cdcodigocuentaliq, String cdalias, Date fliquidacion, String isin, String descrIsin,
			String camara, String cdoperacion, String descripcionOperacion, Character signoAnotacion, Date fcontratacion,
			BigDecimal titulos, BigDecimal efectivo, BigDecimal corretaje, String auditUser, Date auditDate ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.fliquidacion = ( fliquidacion != null ) ? convertDateToString( fliquidacion ) : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.descrIsin = ( descrIsin != null ) ? descrIsin.trim() : "";
		this.camara = ( camara != null ) ? camara.trim() : "";
		this.cdoperacion = ( cdoperacion != null ) ? cdoperacion.trim() : "";
		this.descripcionOperacion = ( descripcionOperacion != null ) ? descripcionOperacion : "";
		this.signoAnotacion = signoAnotacion;
		this.fcontratacion = ( fcontratacion != null ) ? convertDateToString( fcontratacion ) : null;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = ( corretaje == null ) ? BigDecimal.ZERO : corretaje;
		this.auditUser = ( auditUser == null ) ? "" : auditUser;
		this.auditDate = convertDateToString( auditDate );
	}


	public MovimientoCuentaAliasData( String cdcodigocuentaliq, String cdalias, Date fliquidacion, String isin, String descrIsin,
			String camara, String cdoperacion, String descripcionOperacion, Character signoAnotacion, Date fcontratacion,
			BigDecimal titulos, BigDecimal efectivo, BigDecimal corretaje, String auditUser, Date auditDate, String sistemaLiq ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.fliquidacion = ( fliquidacion != null ) ? convertDateToString( fliquidacion ) : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.descrIsin = ( descrIsin != null ) ? descrIsin.trim() : "";
		this.camara = ( camara != null ) ? camara.trim() : "";
		this.cdoperacion = ( cdoperacion != null ) ? cdoperacion.trim() : "";
		this.descripcionOperacion = ( descripcionOperacion != null ) ? descripcionOperacion : "";
		this.signoAnotacion = signoAnotacion;
		this.fcontratacion = ( fcontratacion != null ) ? convertDateToString( fcontratacion ) : null;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = ( corretaje == null ) ? BigDecimal.ZERO : corretaje;
		this.auditUser = ( auditUser == null ) ? "" : auditUser;
		this.auditDate = convertDateToString( auditDate );
		this.sistemaLiq = sistemaLiq;
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param cdalias
	 *            the cdalias
	 * @param isin
	 *            the isin
	 * @param cdcodigocuentaliq
	 *            the cdcodigocuentaliq
	 * @param titulos
	 *            the titulos
	 * @param efectivo
	 *            the efectivo
	 */
	public MovimientoCuentaAliasData( String cdalias, String isin, String cdcodigocuentaliq, BigDecimal titulos, BigDecimal efectivo ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.titulos = titulos;
		this.efectivo = efectivo;
	}
	/**
	 * 
	 * @param cdalias
	 * @param isin
	 * @param cdcodigocuentaliq
	 * @param tradedate
	 * @param titulos
	 * @param efectivo
	 */
	public MovimientoCuentaAliasData( String cdalias, String isin, String cdcodigocuentaliq, Date settlementdate, BigDecimal titulos, BigDecimal efectivo ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.cdalias = ( cdalias != null ) ? cdalias.trim() : "";
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.settlementDate = settlementdate;
	}
	/**
	 * 
	 * @param cdcodigocuentaliq
	 * @param settlementdate
	 * @param isin
	 * @param descisin
	 * @param titulos
	 * @param efectivo
	 */
	public MovimientoCuentaAliasData(int difConstructor, String cdcodigocuentaliq, String isin, String descisin, BigDecimal titulos, BigDecimal efectivo ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.descrIsin = descisin;
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param movimientoCuentaAlias
	 *            the movimiento cuenta alias
	 * @param descrIsin
	 *            the descr isin
	 */
	public MovimientoCuentaAliasData( Tmct0MovimientoCuentaAlias movimientoCuentaAlias, String descrIsin ) {
		this.movimientoCuentaAlias = movimientoCuentaAlias;
		this.descrIsin = descrIsin;
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param isin
	 *            the isin
	 * @param cdcodigocuentaliq
	 *            the cdcodigocuentaliq
	 * @param titulos
	 *            the titulos
	 * @param efectivo
	 *            the efectivo
	 */
	public MovimientoCuentaAliasData( String isin, String cdcodigocuentaliq, BigDecimal titulos, BigDecimal efectivo ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.titulos = titulos;
		this.efectivo = efectivo;
	}

	/**
	 * Instantiates a new movimiento cuenta alias data.
	 *
	 * @param isin
	 *            the isin
	 * @param cdcodigocuentaliq
	 *            the cdcodigocuentaliq
	 * @param efectivo
	 *            the efectivo
	 * @param settlementdate
	 *            the settlementdate
	 */
	public MovimientoCuentaAliasData( String isin, String cdcodigocuentaliq, BigDecimal efectivo, Date settlementdate ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.isin = ( isin != null ) ? isin.trim() : "";
		this.efectivo = efectivo;
		this.settlementDate = settlementdate;
		// this.tradeDate = tradedate;
	}

	/**
	 * Gets the movimiento cuenta alias.
	 *
	 * @return the movimiento cuenta alias
	 */
	public Tmct0MovimientoCuentaAlias getMovimientoCuentaAlias() {
		return movimientoCuentaAlias;
	}

	/**
	 * Sets the movimiento cuenta alias.
	 *
	 * @param movimientoCuentaAlias
	 *            the new movimiento cuenta alias
	 */
	public void setMovimientoCuentaAlias( Tmct0MovimientoCuentaAlias movimientoCuentaAlias ) {
		this.movimientoCuentaAlias = movimientoCuentaAlias;
	}

	/**
	 * Gets the cdcodigocuentaliq.
	 *
	 * @return the cdcodigocuentaliq
	 */
	public String getCdcodigocuentaliq() {
		return cdcodigocuentaliq;
	}

	/**
	 * Sets the cdcodigocuentaliq.
	 *
	 * @param cdcodigocuentaliq
	 *            the new cdcodigocuentaliq
	 */
	public void setCdcodigocuentaliq( String cdcodigocuentaliq ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

	/**
	 * Gets the cdalias.
	 *
	 * @return the cdalias
	 */
	public String getCdalias() {
		return cdalias;
	}

	/**
	 * Sets the cdalias.
	 *
	 * @param cdalias
	 *            the new cdalias
	 */
	public void setCdalias( String cdalias ) {
		this.cdalias = cdalias;
	}

	/**
	 * Gets the descali.
	 *
	 * @return the descali
	 */
	public String getDescali() {
		return descali;
	}

	/**
	 * Sets the descali.
	 *
	 * @param descali
	 *            the new descali
	 */
	public void setDescali( String descali ) {
		this.descali = descali;
	}

	/**
	 * Gets the isin.
	 *
	 * @return the isin
	 */
	public String getIsin() {
		return isin;
	}

	/**
	 * Sets the isin.
	 *
	 * @param isin
	 *            the new isin
	 */
	public void setIsin( String isin ) {
		this.isin = isin;
	}

	/**
	 * Gets the descr isin.
	 *
	 * @return the descr isin
	 */
	public String getDescrIsin() {
		return descrIsin;
	}

	/**
	 * Sets the descr isin.
	 *
	 * @param descrIsin
	 *            the new descr isin
	 */
	public void setDescrIsin( String descrIsin ) {
		this.descrIsin = descrIsin;
	}

	/**
	 * Gets the camara.
	 *
	 * @return the camara
	 */
	public String getCamara() {
		return camara;
	}

	/**
	 * Sets the camara.
	 *
	 * @param camara
	 *            the new camara
	 */
	public void setCamara( String camara ) {
		this.camara = camara;
	}

	/**
	 * Gets the cdoperacion.
	 *
	 * @return the cdoperacion
	 */
	public String getCdoperacion() {
		return cdoperacion;
	}

	/**
	 * Sets the cdoperacion.
	 *
	 * @param cdoperacion
	 *            the new cdoperacion
	 */
	public void setCdoperacion( String cdoperacion ) {
		this.cdoperacion = cdoperacion;
	}

	/**
	 * Gets the signo anotacion.
	 *
	 * @return the signo anotacion
	 */
	public Character getSignoAnotacion() {
		return signoAnotacion;
	}

	/**
	 * Sets the signo anotacion.
	 *
	 * @param signoAnotacion
	 *            the new signo anotacion
	 */
	public void setSignoAnotacion( Character signoAnotacion ) {
		this.signoAnotacion = signoAnotacion;
	}

	/**
	 * Gets the titulos.
	 *
	 * @return the titulos
	 */
	public BigDecimal getTitulos() {
		return titulos;
	}

	/**
	 * Sets the titulos.
	 *
	 * @param titulos
	 *            the new titulos
	 */
	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	/**
	 * Gets the efectivo.
	 *
	 * @return the efectivo
	 */
	public BigDecimal getEfectivo() {
		return efectivo;
	}

	/**
	 * Sets the efectivo.
	 *
	 * @param efectivo
	 *            the new efectivo
	 */
	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	/**
	 * Gets the fliquidacion.
	 *
	 * @return the fliquidacion
	 */
	public String getFliquidacion() {
		return fliquidacion;
	}

	/**
	 * Sets the fliquidacion.
	 *
	 * @param fliquidacion
	 *            the new fliquidacion
	 */
	public void setFliquidacion( String fliquidacion ) {
		this.fliquidacion = fliquidacion;
	}

	/**
	 * Gets the fcontratacion.
	 *
	 * @return the fcontratacion
	 */
	public String getFcontratacion() {
		return fcontratacion;
	}

	/**
	 * Sets the fcontratacion.
	 *
	 * @param fcontratacion
	 *            the new fcontratacion
	 */
	public void setFcontratacion( String fcontratacion ) {
		this.fcontratacion = fcontratacion;
	}

	/**
	 * Gets the fhinicio.
	 *
	 * @return the fhinicio
	 */
	public Integer getFhinicio() {
		return fhinicio;
	}

	/**
	 * Sets the fhinicio.
	 *
	 * @param fhinicio
	 *            the new fhinicio
	 */
	public void setFhinicio( Integer fhinicio ) {
		this.fhinicio = fhinicio;
	}

	/**
	 * Gets the fhfinal.
	 *
	 * @return the fhfinal
	 */
	public Integer getFhfinal() {
		return fhfinal;
	}

	/**
	 * Sets the fhfinal.
	 *
	 * @param fhfinal
	 *            the new fhfinal
	 */
	public void setFhfinal( Integer fhfinal ) {
		this.fhfinal = fhfinal;
	}

	/**
	 * Gets the settlement date.
	 *
	 * @return the settlement date
	 */
	public Date getSettlementDate() {
		return settlementDate;
	}

	/**
	 * Sets the settlement date.
	 *
	 * @param settlementDate
	 *            the new settlement date
	 */
	public void setSettlementDate( Date settlementDate ) {
		this.settlementDate = settlementDate;
	}

	/**
	 * Gets the corretaje.
	 *
	 * @return the corretaje
	 */
	public BigDecimal getCorretaje() {
		return corretaje;
	}

	/**
	 * Sets the corretaje.
	 *
	 * @param corretaje
	 *            the new corretaje
	 */
	public void setCorretaje( BigDecimal corretaje ) {
		this.corretaje = corretaje;
	}

	/**
	 * Gets the descripcion operacion.
	 *
	 * @return the descripcion operacion
	 */
	public String getDescripcionOperacion() {
		return descripcionOperacion;
	}

	/**
	 * Sets the descripcion operacion.
	 *
	 * @param descripcionOperacion
	 *            the new descripcion operacion
	 */
	public void setDescripcionOperacion( String descripcionOperacion ) {
		this.descripcionOperacion = descripcionOperacion;
	}

	/**
	 * Gets the audit user.
	 *
	 * @return the audit user
	 */
	public final String getAuditUser() {
		return auditUser;
	}

	/**
	 * Sets the audit user.
	 *
	 * @param auditUser
	 *            the new audit user
	 */
	public final void setAuditUser( String auditUser ) {
		this.auditUser = auditUser;
	}

	/**
	 * Gets the audit date.
	 *
	 * @return the audit date
	 */
	public final String getAuditDate() {
		return auditDate;
	}

	/**
	 * Sets the audit date.
	 *
	 * @param auditDate
	 *            the new audit date
	 */
	public final void setAuditDate( String auditDate ) {
		this.auditDate = auditDate;
	}

	
	public String getSistemaLiq() {
		return sistemaLiq;
	}
	
	
	public long getId() {
	    return id;
	}

	public void setId(long id) {
	    this.id = id;
	}

	public void setSistemaLiq( String sistemaLiq ) {
		this.sistemaLiq = sistemaLiq;
	}
	

	public Date getTradedate() {
	    return tradedate;
	}

	public void setTradedate(Date tradedate) {
	    this.tradedate = tradedate;
	}

	
	public Date getFechaRegistro() {
	    return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
	    this.fechaRegistro = fechaRegistro;
	}
	
	

	public Integer getPrioridadOperacion() {
	    return prioridadOperacion;
	}

	public void setPrioridadOperacion(Integer prioridadOperacion) {
	    this.prioridadOperacion = prioridadOperacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( auditDate == null ) ? 0 : auditDate.hashCode() );
		result = prime * result + ( ( auditUser == null ) ? 0 : auditUser.hashCode() );
		result = prime * result + ( ( camara == null ) ? 0 : camara.hashCode() );
		result = prime * result + ( ( cdalias == null ) ? 0 : cdalias.hashCode() );
		result = prime * result + ( ( cdcodigocuentaliq == null ) ? 0 : cdcodigocuentaliq.hashCode() );
		result = prime * result + ( ( cdoperacion == null ) ? 0 : cdoperacion.hashCode() );
		result = prime * result + ( ( corretaje == null ) ? 0 : corretaje.hashCode() );
		result = prime * result + ( ( descali == null ) ? 0 : descali.hashCode() );
		result = prime * result + ( ( descrIsin == null ) ? 0 : descrIsin.hashCode() );
		result = prime * result + ( ( descripcionOperacion == null ) ? 0 : descripcionOperacion.hashCode() );
		result = prime * result + ( ( efectivo == null ) ? 0 : efectivo.hashCode() );
		result = prime * result + ( ( fcontratacion == null ) ? 0 : fcontratacion.hashCode() );
		result = prime * result + ( ( fhfinal == null ) ? 0 : fhfinal.hashCode() );
		result = prime * result + ( ( fhinicio == null ) ? 0 : fhinicio.hashCode() );
		result = prime * result + ( ( fliquidacion == null ) ? 0 : fliquidacion.hashCode() );
		result = prime * result + ( ( isin == null ) ? 0 : isin.hashCode() );
		result = prime * result + ( ( movimientoCuentaAlias == null ) ? 0 : movimientoCuentaAlias.hashCode() );
		result = prime * result + ( ( settlementDate == null ) ? 0 : settlementDate.hashCode() );
		result = prime * result + ( ( signoAnotacion == null ) ? 0 : signoAnotacion.hashCode() );
		result = prime * result + ( ( titulos == null ) ? 0 : titulos.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		MovimientoCuentaAliasData other = ( MovimientoCuentaAliasData ) obj;
		if ( auditDate == null ) {
			if ( other.auditDate != null )
				return false;
		} else if ( !auditDate.equals( other.auditDate ) )
			return false;
		if ( auditUser == null ) {
			if ( other.auditUser != null )
				return false;
		} else if ( !auditUser.equals( other.auditUser ) )
			return false;
		if ( camara == null ) {
			if ( other.camara != null )
				return false;
		} else if ( !camara.equals( other.camara ) )
			return false;
		if ( cdalias == null ) {
			if ( other.cdalias != null )
				return false;
		} else if ( !cdalias.equals( other.cdalias ) )
			return false;
		if ( cdcodigocuentaliq == null ) {
			if ( other.cdcodigocuentaliq != null )
				return false;
		} else if ( !cdcodigocuentaliq.equals( other.cdcodigocuentaliq ) )
			return false;
		if ( cdoperacion == null ) {
			if ( other.cdoperacion != null )
				return false;
		} else if ( !cdoperacion.equals( other.cdoperacion ) )
			return false;
		if ( corretaje == null ) {
			if ( other.corretaje != null )
				return false;
		} else if ( !corretaje.equals( other.corretaje ) )
			return false;
		if ( descali == null ) {
			if ( other.descali != null )
				return false;
		} else if ( !descali.equals( other.descali ) )
			return false;
		if ( descrIsin == null ) {
			if ( other.descrIsin != null )
				return false;
		} else if ( !descrIsin.equals( other.descrIsin ) )
			return false;
		if ( descripcionOperacion == null ) {
			if ( other.descripcionOperacion != null )
				return false;
		} else if ( !descripcionOperacion.equals( other.descripcionOperacion ) )
			return false;
		if ( efectivo == null ) {
			if ( other.efectivo != null )
				return false;
		} else if ( !efectivo.equals( other.efectivo ) )
			return false;
		if ( fcontratacion == null ) {
			if ( other.fcontratacion != null )
				return false;
		} else if ( !fcontratacion.equals( other.fcontratacion ) )
			return false;
		if ( fhfinal == null ) {
			if ( other.fhfinal != null )
				return false;
		} else if ( !fhfinal.equals( other.fhfinal ) )
			return false;
		if ( fhinicio == null ) {
			if ( other.fhinicio != null )
				return false;
		} else if ( !fhinicio.equals( other.fhinicio ) )
			return false;
		if ( fliquidacion == null ) {
			if ( other.fliquidacion != null )
				return false;
		} else if ( !fliquidacion.equals( other.fliquidacion ) )
			return false;
		if ( isin == null ) {
			if ( other.isin != null )
				return false;
		} else if ( !isin.equals( other.isin ) )
			return false;
		if ( movimientoCuentaAlias == null ) {
			if ( other.movimientoCuentaAlias != null )
				return false;
		} else if ( !movimientoCuentaAlias.equals( other.movimientoCuentaAlias ) )
			return false;
		if ( settlementDate == null ) {
			if ( other.settlementDate != null )
				return false;
		} else if ( !settlementDate.equals( other.settlementDate ) )
			return false;
		if ( signoAnotacion == null ) {
			if ( other.signoAnotacion != null )
				return false;
		} else if ( !signoAnotacion.equals( other.signoAnotacion ) )
			return false;
		if ( titulos == null ) {
			if ( other.titulos != null )
				return false;
		} else if ( !titulos.equals( other.titulos ) )
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MovimientoCuentaAliasData [movimientoCuentaAlias=" + movimientoCuentaAlias + ", cdcodigocuentaliq=" + cdcodigocuentaliq
				+ ", cdalias=" + cdalias + ", descali=" + descali + ", fliquidacion=" + fliquidacion + ", isin=" + isin + ", descrIsin="
				+ descrIsin + ", camara=" + camara + ", cdoperacion=" + cdoperacion + ", descripcionOperacion=" + descripcionOperacion
				+ ", signoAnotacion=" + signoAnotacion + ", fcontratacion=" + fcontratacion + ", titulos=" + titulos + ", efectivo="
				+ efectivo + ", fhinicio=" + fhinicio + ", fhfinal=" + fhfinal + ", settlementDate=" + settlementDate + ", corretaje="
				+ corretaje + ", auditUser=" + auditUser + ", auditDate=" + auditDate + "]";
	}
	
	public String agrupacion() {
	  return String.format("{cdaliass: %s, isin: %s, cdcodigocuentaliq: %s, settlementdate: %s", 
	      cdalias, isin, cdcodigocuentaliq, settlementDate);
	}

}
