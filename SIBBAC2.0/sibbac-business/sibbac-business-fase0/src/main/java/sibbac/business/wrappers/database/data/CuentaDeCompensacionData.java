package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Mercado;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;
import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;

public class CuentaDeCompensacionData implements Serializable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static final String TAG = CuentaDeCompensacionData.class.getName();
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    private long idCuentaCompensacion;
    private String cdCodigo;
    private String codigoS3;
    private String cdCuentaCompensacion;
    private char esClearing;
    private Character s3;
    private Long idCompensador;
    private Long idTipoCuenta;
    private TipoCuentaConciliacionData tipoCuentaConciliacion;
    private EntidadRegistroData entidadRegistro;
    private Tmct0TipoEr tipoER;
    private long idMercadoContratacion;
    private long numCuentaER;
    private boolean esCuentaPropia;
    private Tmct0TipoNeteo tipoNeteoTitulo;
    private Tmct0TipoNeteo tipoNeteoEfectivo;
    private long frecuenciaEnvioExtracto;
    private String tipoFicheroConciliacion;
    private long cuentaLiquidacionId;
    private String cuentaLiquidacionCodigo;
    private String subcustodio;
    private String numCtaSubcustodio;
    private List<MercadoData> relacionCuentaMercadoDataList = new LinkedList<MercadoData>();
    private SistemaLiquidacionData sistemaLiquidacion;

    public CuentaDeCompensacionData(Tmct0CuentasDeCompensacion cc) {
	try {
	    this.idCuentaCompensacion = cc.getIdCuentaCompensacion();
	    this.cdCodigo = cc.getCdCodigo();
	    this.codigoS3 = cc.getCdCodigoS3();
	    this.cdCuentaCompensacion = cc.getCdCuentaCompensacion();
	    this.esClearing = cc.getCuentaClearing();
	    this.s3 = cc.getS3();
	    this.idCompensador = cc.getTmct0Compensador().getIdCompensador();
	    this.idTipoCuenta = cc.getTmct0TipoCuentaDeCompensacion()
		    .getIdTipoCuenta();
	    if (cc.getEntidadRegistro() != null) {
		this.entidadRegistro = new EntidadRegistroData(
			cc.getEntidadRegistro());
	    }
	    this.tipoER = (entidadRegistro != null) ? entidadRegistro
		    .getTipoer() : null;
	    this.idMercadoContratacion = cc.getIdMercadoContratacion();
	    this.numCuentaER = cc.getNumCuentaER();
	    this.esCuentaPropia = cc.getEsCuentaPropia();
	    this.tipoNeteoTitulo = cc.getTipoNeteoTitulo();
	    this.tipoNeteoEfectivo = cc.getTipoNeteoEfectivo();
	    this.frecuenciaEnvioExtracto = cc.getFrecuenciaEnvioExtracto();
	    this.tipoFicheroConciliacion = cc.getTipoFicheroConciliacion();
	    this.subcustodio = cc.getSubcustodioDomestico();
	    //
	    if (cc.getSistemaLiquidacion() != null) {
		this.sistemaLiquidacion = new SistemaLiquidacionData(
			cc.getSistemaLiquidacion());
	    }
	    //
	    this.numCtaSubcustodio = cc.getNumeroCuentaSubcustodio();
	    if (cc.getTipoCuentaConciliacion() != null) {
		this.tipoCuentaConciliacion = new TipoCuentaConciliacionData();
		this.tipoCuentaConciliacion.setId(cc
			.getTipoCuentaConciliacion().getId());
		this.tipoCuentaConciliacion.setName(cc
			.getTipoCuentaConciliacion().getName());
	    }
	    try{
	    extractCuentasMercados(cc);
	    }catch(PersistenceException ex){
		LOG.error(ex.getMessage(), ex);
	    }
	    Tmct0CuentaLiquidacion cl = cc.getTmct0CuentaLiquidacion();
	    this.cuentaLiquidacionId = cl.getId();
	    this.cuentaLiquidacionCodigo = cl.getCdCodigo();
	} catch (Exception ex) {
	    LOG.error(ex.getMessage(), ex);
	}
    }

    private void extractCuentasMercados(Tmct0CuentasDeCompensacion cc)
	    throws PersistenceException {
	if (!CollectionUtils.isEmpty(cc.getMercados())) {
	    for (Tmct0Mercado mkt : cc.getMercados()) {
		MercadoData relData = new MercadoData(mkt);
		this.relacionCuentaMercadoDataList.add(relData);
	    }
	}
    }

    public long getIdCuentaCompensacion() {
	return idCuentaCompensacion;
    }

    public void setIdCuentaCompensacion(long idCuentaCompensacion) {
	this.idCuentaCompensacion = idCuentaCompensacion;
    }

    public String getCdCodigo() {
	return cdCodigo;
    }

    public void setCdCodigo(String cdCodigo) {
	this.cdCodigo = cdCodigo;
    }

    public String getCodigoS3() {
	return codigoS3;
    }

    public void setCodigoS3(String codigoS3) {
	this.codigoS3 = codigoS3;
    }

    public String getCdCuentaCompensacion() {
	return cdCuentaCompensacion;
    }

    public void setCdCuentaCompensacion(String cdCuentaCompensacion) {
	this.cdCuentaCompensacion = cdCuentaCompensacion;
    }

    public char getEsClearing() {
	return esClearing;
    }

    public void setEsClearing(char esClearing) {
	this.esClearing = esClearing;
    }

    public Character getS3() {
	return s3;
    }

    public void setS3(Character s3) {
	this.s3 = s3;
    }

    public Long getIdCompensador() {
	return idCompensador;
    }

    public void setIdCompensador(Long idCompensador) {
	this.idCompensador = idCompensador;
    }

    public Long getIdTipoCuenta() {
	return idTipoCuenta;
    }

    public void setIdTipoCuenta(Long idTipoCuenta) {
	this.idTipoCuenta = idTipoCuenta;
    }

    public long getCuentaLiquidacionId() {
	return cuentaLiquidacionId;
    }

    public void setCuentaLiquidacionId(long cuentaLiquidacionId) {
	this.cuentaLiquidacionId = cuentaLiquidacionId;
    }

    public String getCuentaLiquidacionCodigo() {
	return cuentaLiquidacionCodigo;
    }

    public void setCuentaLiquidacionCodigo(String cuentaLiquidacionCodigo) {
	this.cuentaLiquidacionCodigo = cuentaLiquidacionCodigo;
    }

    public TipoCuentaConciliacionData getTipoCuentaConciliacion() {
	return tipoCuentaConciliacion;
    }

    public void setTipoCuentaConciliacion(
	    TipoCuentaConciliacionData tipoCuentaConciliacion) {
	this.tipoCuentaConciliacion = tipoCuentaConciliacion;
    }

    public EntidadRegistroData getEntidadRegistro() {
	return entidadRegistro;
    }

    public void setEntidadRegistro(EntidadRegistroData entidadRegistro) {
	this.entidadRegistro = entidadRegistro;
    }

    public Tmct0TipoEr getTipoER() {
	return tipoER;
    }

    public void setTipoER(Tmct0TipoEr tipoER) {
	this.tipoER = tipoER;
    }

    public long getIdMercadoContratacion() {
	return idMercadoContratacion;
    }

    public void setIdMercadoContratacion(long idMercadoContratacion) {
	this.idMercadoContratacion = idMercadoContratacion;
    }

    public long getNumCuentaER() {
	return numCuentaER;
    }

    public void setNumCuentaER(long numCuentaER) {
	this.numCuentaER = numCuentaER;
    }

    public boolean isEsCuentaPropia() {
	return esCuentaPropia;
    }

    public void setEsCuentaPropia(boolean esCuentaPropia) {
	this.esCuentaPropia = esCuentaPropia;
    }

    public Tmct0TipoNeteo getTipoNeteoTitulo() {
	return tipoNeteoTitulo;
    }

    public void setTipoNeteoTitulo(Tmct0TipoNeteo tipoNeteoTitulo) {
	this.tipoNeteoTitulo = tipoNeteoTitulo;
    }

    public Tmct0TipoNeteo getTipoNeteoEfectivo() {
	return tipoNeteoEfectivo;
    }

    public void setTipoNeteoEfectivo(Tmct0TipoNeteo tipoNeteoEfectivo) {
	this.tipoNeteoEfectivo = tipoNeteoEfectivo;
    }

    public long getFrecuenciaEnvioExtracto() {
	return frecuenciaEnvioExtracto;
    }

    public void setFrecuenciaEnvioExtracto(long frecuenciaEnvioExtracto) {
	this.frecuenciaEnvioExtracto = frecuenciaEnvioExtracto;
    }

    public String getTipoFicheroConciliacion() {
	return tipoFicheroConciliacion;
    }

    public void setTipoFicheroConciliacion(String tipoFicheroConciliacion) {
	this.tipoFicheroConciliacion = tipoFicheroConciliacion;
    }

    public List<MercadoData> getRelacionCuentaMercadoDataList() {
	return relacionCuentaMercadoDataList;
    }

    public void setMercadoDataList(
	    List<MercadoData> relacionCuentaMercadoDataList) {
	this.relacionCuentaMercadoDataList = relacionCuentaMercadoDataList;
    }

    public String getSubcustodio() {
	return subcustodio;
    }

    public void setSubcustodio(String subcustodio) {
	this.subcustodio = subcustodio;
    }

    public String getNumCtaSubcustodio() {
	return numCtaSubcustodio;
    }

    public void setNumCtaSubcustodio(String numCtaSubcustodio) {
	this.numCtaSubcustodio = numCtaSubcustodio;
    }

    public void setRelacionCuentaMercadoDataList(
	    List<MercadoData> relacionCuentaMercadoDataList) {
	this.relacionCuentaMercadoDataList = relacionCuentaMercadoDataList;
    }

    public SistemaLiquidacionData getSistemaLiquidacion() {
	return sistemaLiquidacion;
    }

    public void setSistemaLiquidacion(SistemaLiquidacionData sistemaLiquidacion) {
	this.sistemaLiquidacion = sistemaLiquidacion;
    }

}
