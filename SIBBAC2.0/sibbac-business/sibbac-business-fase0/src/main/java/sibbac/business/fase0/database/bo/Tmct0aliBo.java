package sibbac.business.fase0.database.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dao.Tmct0aliDaoImp;
import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0aliId;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0aliBo extends AbstractBo<Tmct0ali, Tmct0aliId, Tmct0aliDao> {

  @Autowired
  private Tmct0aliDaoImp daoImp;

  /**
   * Constante para el campo fhfin que indica que el alias está activo.
   */
  @Transactional
  public Tmct0ali findFirst1ByCdaliass(final String cdaliass) {
    return this.dao.findFirst1ByIdCdaliass(cdaliass);
  }

  /**
   * Recupera la lista de alias activos
   */
  public List<Tmct0ali> findAlisActivos() {

    return this.dao.findByFhfinal(Tmct0ali.fhfinActivos);
  }

  public Tmct0ali findByCdaliass(String cdaliass, Integer fhfinal) {
    return dao.findByCdaliass(cdaliass, fhfinal);
  }

  public Tmct0ali findByCdaliassFechaActual(String cdaliass, Integer fhactual) {
    return dao.findByCdaliassFechaActual(cdaliass, fhactual);
  }

  public List<String> findAliasNetting() {
    return dao.findAliasNetting();
  }

  public List<String> findAlias() {
    return dao.findAlias();
  }

  public List<Tmct0aliDTO> findAllAli(Integer fechaActivo) {
    return dao.findAllAli(fechaActivo);
  }

  public boolean esClienteNetting(String cdaliass) {
    // Solo recuperamos el ultimo alias recuperado
    Tmct0ali datos_alias = findByFirstAliasOrder(cdaliass);
    if (datos_alias == null || datos_alias.getIndNeting().equals('N') || datos_alias.getIndNeting().equals(' ')) {
      return false;
    }
    return true;
  }

  public Tmct0ali findByFirstAliasOrder(String cdaliass) {
    List<Tmct0ali> lista_alias = dao.findByAliasOrder(cdaliass);
    if (lista_alias.size() == 0) {
      return null;
    }
    // Solo recuperamos el ultimo alias recuperado
    return lista_alias.get(0);
  }

  /**
   * Metodo que devuelve la descripcion de un cdaliass si la encuentra. En caso
   * de que no la encuentre devuelve el cdaliass.
   * 
   * @param cdaliass
   * @return
   */
  public String findDescraliByCdAliass(String cdaliass) {
    Tmct0ali alias = this.findByCdaliass(cdaliass, Tmct0ali.fhfinActivos);
    if (alias != null) {
      return alias.getDescrali();
    }
    else {
      return cdaliass;
    }
  }

  public List<Tmct0aliId> extractIds(final List<Tmct0ali> tmct0alis) {
    final List<Tmct0aliId> tmct0aliIds = new ArrayList<Tmct0aliId>();
    if (CollectionUtils.isNotEmpty(tmct0alis)) {
      for (final Tmct0ali tmct0ali : tmct0alis) {
        tmct0aliIds.add(tmct0ali.getId());
      }
    }
    return tmct0aliIds;
  }

  public List<Tmct0ali> findCdAliassById_regla(Long Id_regla) {
    return dao.findCdAliassById_regla(Id_regla);
  }

  public List<Tmct0aliDTO> findAliasByManyReglas(List<Long> idReglas) {
    return daoImp.findAliasByManyReglas(idReglas);
  }

  public boolean isAliasBonificado(String cdaliass) throws SIBBACBusinessException {
    boolean res = false;
    cdaliass = StringUtils.trim(cdaliass);
    if (cdaliass.contains("|")) {
      cdaliass = cdaliass.split("\\|")[1];
    }
    Tmct0ali ali = null;
    try {
      ali = findByCdaliass(cdaliass, Tmct0ali.fhfinActivos);
    }
    catch (Exception e1) {
      throw new SIBBACBusinessException(e1.getMessage(), e1);
    }
    if (ali != null) {
      try {
        res = ali.getBonificado() != null && ali.getBonificado() == 'S';
      }
      catch (Exception e) {
      }
    }
    else {
      throw new SIBBACBusinessException("INCIDENCIA- Alias de alta no econtrado en maestro de alias: " + cdaliass);
    }
    return res;
  }

  public boolean isAliasBonificado(Map<String, Boolean> isAliasBonificado, String cdaliass)
      throws SIBBACBusinessException {
    cdaliass = StringUtils.trim(cdaliass);
    if (cdaliass.contains("|")) {
      cdaliass = cdaliass.split("\\|")[1];
    }
    if (isAliasBonificado.get(cdaliass) == null) {

      synchronized (isAliasBonificado) {
        if (isAliasBonificado.get(cdaliass) == null) {
          try {
            isAliasBonificado.put(cdaliass, this.isAliasBonificado(cdaliass));
          }
          catch (Exception e) {
            isAliasBonificado.put(cdaliass, false);
            throw e;
          }
        }
      }

    }
    return isAliasBonificado.get(cdaliass);
  }

  public Character getCorretajePtiChached(Map<String, Character> chachedCorretajePti, String cdaliass)
      throws SIBBACBusinessException {
    cdaliass = StringUtils.trim(cdaliass);
    if (cdaliass.contains("|")) {
      cdaliass = cdaliass.split("\\|")[1];
    }
    if (chachedCorretajePti.get(cdaliass) == null) {
      synchronized (chachedCorretajePti) {
        if (chachedCorretajePti.get(cdaliass) == null) {
          Tmct0ali ali = findByCdaliass(cdaliass, Tmct0ali.fhfinActivos);
          if (ali != null) {
            try {
              chachedCorretajePti.put(cdaliass, ali.getCorretajePti() != null ? ali.getCorretajePti() : ' ');
            }
            catch (Exception e) {
              chachedCorretajePti.put(cdaliass, ' ');
            }
          }
          else {
            chachedCorretajePti.put(cdaliass, ' ');
            throw new SIBBACBusinessException("INCIDENCIA- Alias de alta no econtrado en maestro de alias: " + cdaliass);

          }
        }
      }
    }
    return chachedCorretajePti.get(cdaliass);
  }

  public List<Tmct0ali> findByCdAlias(String cdaliass) {
    return dao.findByCdaliass(cdaliass);
  }

  public List<LabelValueObject> labelValueList() {
    return dao.labelValueList();
  }

}
