package sibbac.business.wrappers.database.data;

import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;

public class EntidadRegistroData {

    private long id;
    private String bic;
    private String nombre;
    private Tmct0TipoEr tipoer;

    public EntidadRegistroData(Tmct0EntidadRegistro er) {
	if (er != null) {
	    this.id = er.getId();
	    this.bic = er.getBic();
	    this.nombre = er.getNombre();
	    if (er.getTipoer() != null) {
		tipoer = er.getTipoer();
	    }
	}
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getBic() {
	return bic;
    }

    public void setBic(String bic) {
	this.bic = bic;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public Tmct0TipoEr getTipoer() {
	return tipoer;
    }

    public void setTipoer(Tmct0TipoEr tipoer) {
	this.tipoer = tipoer;
    }

}
