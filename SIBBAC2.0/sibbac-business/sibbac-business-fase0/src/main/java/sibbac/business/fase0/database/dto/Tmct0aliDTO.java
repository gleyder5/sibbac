package sibbac.business.fase0.database.dto;

import java.math.BigInteger;


public class Tmct0aliDTO {

	private String	alias;
	private String	descripcion;
	private BigInteger    idRegla;
	private String  cdCodigoRegla;
	
	public Tmct0aliDTO(){
		
	}

	public Tmct0aliDTO( String alias, String descripcion ) {
		this.alias = ( alias != null && !"".equals( alias ) ) ? alias.trim() : "";
		this.descripcion = ( descripcion != null && !"".equals( descripcion ) ) ? descripcion.trim() : "";
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}
	
	
	public BigInteger getIdRegla() {
		return idRegla;
	}

	public void setIdRegla(BigInteger idRegla) {
		this.idRegla = idRegla;
	}

	public String getCdCodigoRegla() {
		return cdCodigoRegla;
	}

	public void setCdCodigoRegla(String cdCodigoRegla) {
		this.cdCodigoRegla = cdCodigoRegla;
	}

	public static Tmct0aliDTO convertObjectToAliaDTO( Object[] valuesFromQuery){
		
		Tmct0aliDTO alias = new Tmct0aliDTO();
		alias.setIdRegla((BigInteger) valuesFromQuery[0]);
		alias.setCdCodigoRegla((String) valuesFromQuery[1]);
		alias.setAlias((String) valuesFromQuery[2]);
		alias.setDescripcion((String) valuesFromQuery[3]);
		
		return(alias);
		
		
	}
	

}
