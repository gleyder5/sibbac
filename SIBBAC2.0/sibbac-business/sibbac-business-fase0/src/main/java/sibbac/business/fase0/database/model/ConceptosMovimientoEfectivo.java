package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_CONCEPTOS_MOV_EFECTIVO" )
public class ConceptosMovimientoEfectivo extends MasterData< ConceptosMovimientoEfectivo > {

	private static final long	serialVersionUID	= 3358443858379216589L;

}
