package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0MOVIMIENTOECC_FIDESSA")
public class Tmct0movimientoeccFidessa implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 2108175169028148743L;
  private Long idMovimientoFidessa;
  private String cdrefmovimiento;
  private String cdrefinternaasig;
  private String cdrefmovimientoecc;
  private String cdrefnotificacion;
  private String cdrefnotificacionprev;
  private String cdtipomov;
  private String cdestadomov;
  private Character cdindcotizacion;
  private Date fcontratacion;
  private Date fliquidacion;
  private String nuordenmkt;
  private String nuexemkt;
  private String miembroOrigen;
  private String usuarioOrigen;
  private String cdRefAsignacion;
  private String mnemotecnico;
  private String miembroDestino;
  private String usuarioDestino;
  private String miembroCompensadorDestino;
  private String cuentaCompensacionDestino;
  private BigDecimal imtitulos;
  private BigDecimal imtitulosPendientesAsignar;
  private BigDecimal precio;
  private BigDecimal imefectivo;
  private String plataforma;
  private String segmento;
  private Date fnegociacion;
  private String cdOperacionMercado;
  private String miembroMercado;
  private Date fechaOrden;
  private Date horaOrden;
  private Character indicadorCapacidad;
  private String cdisin;
  private Character sentido;
  private Date auditFechaCambio;
  private String auditUser;
  private Character envios3;

  protected java.lang.String cdoperacionecc;

  protected java.lang.String nuoperacionprev;

  protected java.lang.String cdrefasignacion;

  private List<Tmct0movimientoecc> tmct0movimientoeccs = new ArrayList<Tmct0movimientoecc>(0);

  private String cdcamara;

  private Character imputacionPropia;

  public Tmct0movimientoeccFidessa() {
  }

  public Tmct0movimientoeccFidessa(Long idMovimientoFidessa) {
    this.idMovimientoFidessa = idMovimientoFidessa;
  }

  public Tmct0movimientoeccFidessa(Long idMovimientoFidessa, String cdrefmovimiento, String cdrefinternaasig,
      String cdrefmovimientoecc, String cdrefnotificacion, String cdrefnotificacionprev, String cdtipomov,
      String cdestadomov, Character cdindcotizacion, Date fcontratacion, Date fliquidacion, String nuordenmkt,
      String nuexemkt, String miembroOrigen, String usuarioOrigen, String cdRefAsignacion, String mnemotecnico,
      String miembroDestino, String usuarioDestino, String miembroCompensadorDestino, String cuentaCompensacionDestino,
      BigDecimal imtitulos, BigDecimal imtitulosPendientesAsignar, BigDecimal precio, BigDecimal imefectivo,
      String plataforma, String segmento, Date fnegociacion, String cdOperacionMercado, String miembroMercado,
      Date fechaOrden, Date horaOrden, Character indicadorCapacidad, String cdisin, Character sentido,
      Date auditFechaCambio, String auditUser, List<Tmct0movimientoecc> tmct0movimientoeccs) {
    this.idMovimientoFidessa = idMovimientoFidessa;
    this.cdrefmovimiento = cdrefmovimiento;
    this.cdrefinternaasig = cdrefinternaasig;
    this.cdrefmovimientoecc = cdrefmovimientoecc;
    this.cdrefnotificacion = cdrefnotificacion;
    this.cdrefnotificacionprev = cdrefnotificacionprev;
    this.cdtipomov = cdtipomov;
    this.cdestadomov = cdestadomov;
    this.cdindcotizacion = cdindcotizacion;
    this.fcontratacion = fcontratacion;
    this.fliquidacion = fliquidacion;
    this.nuordenmkt = nuordenmkt;
    this.nuexemkt = nuexemkt;
    this.miembroOrigen = miembroOrigen;
    this.usuarioOrigen = usuarioOrigen;
    this.cdRefAsignacion = cdRefAsignacion;
    this.mnemotecnico = mnemotecnico;
    this.miembroDestino = miembroDestino;
    this.usuarioDestino = usuarioDestino;
    this.miembroCompensadorDestino = miembroCompensadorDestino;
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
    this.imtitulos = imtitulos;
    this.imtitulosPendientesAsignar = imtitulosPendientesAsignar;
    this.precio = precio;
    this.imefectivo = imefectivo;
    this.plataforma = plataforma;
    this.segmento = segmento;
    this.fnegociacion = fnegociacion;
    this.cdOperacionMercado = cdOperacionMercado;
    this.miembroMercado = miembroMercado;
    this.fechaOrden = fechaOrden;
    this.horaOrden = horaOrden;
    this.indicadorCapacidad = indicadorCapacidad;
    this.cdisin = cdisin;
    this.sentido = sentido;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.tmct0movimientoeccs = tmct0movimientoeccs;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID_MOVIMIENTO_FIDESSA", unique = true, nullable = false)
  public Long getIdMovimientoFidessa() {
    return this.idMovimientoFidessa;
  }

  public void setIdMovimientoFidessa(Long idMovimientoFidessa) {
    this.idMovimientoFidessa = idMovimientoFidessa;
  }

  @Column(name = "CDREFMOVIMIENTO", length = 30)
  public String getCdrefmovimiento() {
    return this.cdrefmovimiento;
  }

  public void setCdrefmovimiento(String cdrefmovimiento) {
    this.cdrefmovimiento = cdrefmovimiento;
  }

  @Column(name = "CDREFINTERNAASIG", length = 18)
  public String getCdrefinternaasig() {
    return this.cdrefinternaasig;
  }

  public void setCdrefinternaasig(String cdrefinternaasig) {
    this.cdrefinternaasig = cdrefinternaasig;
  }

  @Column(name = "CDREFMOVIMIENTOECC", length = 20)
  public String getCdrefmovimientoecc() {
    return this.cdrefmovimientoecc;
  }

  public void setCdrefmovimientoecc(String cdrefmovimientoecc) {
    this.cdrefmovimientoecc = cdrefmovimientoecc;
  }

  @Column(name = "CDREFNOTIFICACION", length = 20)
  public String getCdrefnotificacion() {
    return this.cdrefnotificacion;
  }

  public void setCdrefnotificacion(String cdrefnotificacion) {
    this.cdrefnotificacion = cdrefnotificacion;
  }

  @Column(name = "CDREFNOTIFICACIONPREV", length = 20)
  public String getCdrefnotificacionprev() {
    return this.cdrefnotificacionprev;
  }

  public void setCdrefnotificacionprev(String cdrefnotificacionprev) {
    this.cdrefnotificacionprev = cdrefnotificacionprev;
  }

  @Column(name = "CDTIPOMOV", length = 2)
  public String getCdtipomov() {
    return this.cdtipomov;
  }

  public void setCdtipomov(String cdtipomov) {
    this.cdtipomov = cdtipomov;
  }

  @Column(name = "CDESTADOMOV", length = 2)
  public String getCdestadomov() {
    return this.cdestadomov;
  }

  public void setCdestadomov(String cdestadomov) {
    this.cdestadomov = cdestadomov;
  }

  @Column(name = "CDINDCOTIZACION", length = 1)
  public Character getCdindcotizacion() {
    return this.cdindcotizacion;
  }

  public void setCdindcotizacion(Character cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FCONTRATACION", length = 10)
  public Date getFcontratacion() {
    return this.fcontratacion;
  }

  public void setFcontratacion(Date fcontratacion) {
    this.fcontratacion = fcontratacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FLIQUIDACION", length = 10)
  public Date getFliquidacion() {
    return this.fliquidacion;
  }

  public void setFliquidacion(Date fliquidacion) {
    this.fliquidacion = fliquidacion;
  }

  @Column(name = "NUORDENMKT", length = 32)
  public String getNuordenmkt() {
    return this.nuordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  @Column(name = "NUEXEMKT", length = 32)
  public String getNuexemkt() {
    return this.nuexemkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  @Column(name = "MIEMBRO_ORIGEN", length = 4)
  public String getMiembroOrigen() {
    return this.miembroOrigen;
  }

  public void setMiembroOrigen(String miembroOrigen) {
    this.miembroOrigen = miembroOrigen;
  }

  @Column(name = "USUARIO_ORIGEN", length = 3)
  public String getUsuarioOrigen() {
    return this.usuarioOrigen;
  }

  public void setUsuarioOrigen(String usuarioOrigen) {
    this.usuarioOrigen = usuarioOrigen;
  }

  @Column(name = "CD_REF_ASIGNACION", length = 18)
  public String getCdRefAsignacion() {
    return this.cdRefAsignacion;
  }

  public void setCdRefAsignacion(String cdRefAsignacion) {
    this.cdRefAsignacion = cdRefAsignacion;
  }

  @Column(name = "MNEMOTECNICO", length = 10)
  public String getMnemotecnico() {
    return this.mnemotecnico;
  }

  public void setMnemotecnico(String mnemotecnico) {
    this.mnemotecnico = mnemotecnico;
  }

  @Column(name = "MIEMBRO_DESTINO", length = 4)
  public String getMiembroDestino() {
    return this.miembroDestino;
  }

  public void setMiembroDestino(String miembroDestino) {
    this.miembroDestino = miembroDestino;
  }

  @Column(name = "USUARIO_DESTINO", length = 3)
  public String getUsuarioDestino() {
    return this.usuarioDestino;
  }

  public void setUsuarioDestino(String usuarioDestino) {
    this.usuarioDestino = usuarioDestino;
  }

  @Column(name = "MIEMBRO_COMPENSADOR_DESTINO", length = 4)
  public String getMiembroCompensadorDestino() {
    return this.miembroCompensadorDestino;
  }

  public void setMiembroCompensadorDestino(String miembroCompensadorDestino) {
    this.miembroCompensadorDestino = miembroCompensadorDestino;
  }

  @Column(name = "CUENTA_COMPENSACION_DESTINO", length = 3)
  public String getCuentaCompensacionDestino() {
    return this.cuentaCompensacionDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

  @Column(name = "IMTITULOS", precision = 12, scale = 6)
  public BigDecimal getImtitulos() {
    return this.imtitulos;
  }

  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  @Column(name = "IMTITULOS_PENDIENTES_ASIGNAR", precision = 12, scale = 6)
  public BigDecimal getImtitulosPendientesAsignar() {
    return this.imtitulosPendientesAsignar;
  }

  public void setImtitulosPendientesAsignar(BigDecimal imtitulosPendientesAsignar) {
    this.imtitulosPendientesAsignar = imtitulosPendientesAsignar;
  }

  @Column(name = "PRECIO", precision = 13, scale = 6)
  public BigDecimal getPrecio() {
    return this.precio;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  @Column(name = "IMEFECTIVO", precision = 13)
  public BigDecimal getImefectivo() {
    return this.imefectivo;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  @Column(name = "PLATAFORMA", length = 4)
  public String getPlataforma() {
    return this.plataforma;
  }

  public void setPlataforma(String plataforma) {
    this.plataforma = plataforma;
  }

  @Column(name = "SEGMENTO", length = 2)
  public String getSegmento() {
    return this.segmento;
  }

  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FNEGOCIACION", length = 10)
  public Date getFnegociacion() {
    return this.fnegociacion;
  }

  public void setFnegociacion(Date fnegociacion) {
    this.fnegociacion = fnegociacion;
  }

  @Column(name = "CD_OPERACION_MERCADO", length = 2)
  public String getCdOperacionMercado() {
    return this.cdOperacionMercado;
  }

  public void setCdOperacionMercado(String cdOperacionMercado) {
    this.cdOperacionMercado = cdOperacionMercado;
  }

  @Column(name = "MIEMBRO_MERCADO", length = 10)
  public String getMiembroMercado() {
    return this.miembroMercado;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_ORDEN", length = 10)
  public Date getFechaOrden() {
    return this.fechaOrden;
  }

  public void setFechaOrden(Date fechaOrden) {
    this.fechaOrden = fechaOrden;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HORA_ORDEN", length = 8)
  public Date getHoraOrden() {
    return this.horaOrden;
  }

  public void setHoraOrden(Date horaOrden) {
    this.horaOrden = horaOrden;
  }

  @Column(name = "INDICADOR_CAPACIDAD", length = 1)
  public Character getIndicadorCapacidad() {
    return this.indicadorCapacidad;
  }

  public void setIndicadorCapacidad(Character indicadorCapacidad) {
    this.indicadorCapacidad = indicadorCapacidad;
  }

  @Column(name = "CDISIN", length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "SENTIDO", length = 1)
  public Character getSentido() {
    return this.sentido;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * @return the envios3
   */
  @Column(name = "ENVIOS3", length = 1)
  public Character getEnvios3() {
    return envios3;
  }

  /**
   * @param envios3 the envios3 to set
   */
  public void setEnvios3(Character envios3) {
    this.envios3 = envios3;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0movimientoeccFidessa")
  public List<Tmct0movimientoecc> getTmct0movimientoeccs() {
    return this.tmct0movimientoeccs;
  }

  public void setTmct0movimientoeccs(List<Tmct0movimientoecc> tmct0movimientoeccs) {
    this.tmct0movimientoeccs = tmct0movimientoeccs;
  }

  @Column(name = "CDOPERACIONECC", length = 16, nullable = true)
  public java.lang.String getCdoperacionecc() {
    return cdoperacionecc;
  }

  @Column(name = "NUOPERACIONPREV", length = 16, nullable = true)
  public java.lang.String getNuoperacionprev() {
    return nuoperacionprev;
  }

  @Column(name = "CDREFASIGNACION", length = 18, nullable = true)
  public java.lang.String getCdrefasignacion() {
    return cdrefasignacion;
  }

  public void setCdoperacionecc(java.lang.String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public void setNuoperacionprev(java.lang.String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  public void setCdrefasignacion(java.lang.String cdrefasignacion) {
    this.cdrefasignacion = cdrefasignacion;
  }

  @Column(name = "CDCAMARA", nullable = false, length = 11)
  public String getCdcamara() {
    return this.cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  @Column(name = "IMPUTACION_PROPIA", length = 1, nullable = true)
  public Character getImputacionPropia() {
    return imputacionPropia;
  }

  public void setImputacionPropia(Character imputacionPropia) {
    this.imputacionPropia = imputacionPropia;
  }

}
