package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;

/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.AliasDireccion }. Entity: "AliasDireccion".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table(name = WRAPPERS.ALIAS_DIRECCION)
@Entity
@Component
@XmlRootElement
@Audited
public class AliasDireccion extends ATableAudited<AliasDireccion> implements java.io.Serializable {

  public static final String ESPACIO = " ";
  // public static final String RETORNO_CARRO = "\n";
  public static final String RETORNO_CARRO = System.getProperty("line.separator");
  public static final String PARANTENTESIS_IZQ = "(";
  public static final String PARANTENTESIS_DER = ")";
  public static final String COMA = ",";

  // ------------------------------------------------- Bean properties

  /**
	 * 
	 */
  private static final long serialVersionUID = 1128594360392817364L;

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "ID_ALIAS", referencedColumnName = "ID", nullable = false)
  @XmlAttribute
  private Alias alias;

  @Column(name = "NBCALLE", nullable = false, length = 150)
  @XmlAttribute
  private String calle;

  @Column(name = "NBNUMERO", nullable = false, length = 10)
  @XmlAttribute
  private String numero;

  @Column(name = "NBBLOQUE", length = 50)
  @XmlAttribute
  private String bloque;

  @Column(name = "NBPOBLACION", nullable = false, length = 100)
  @XmlAttribute
  private String poblacion;

  @Column(name = "NBCODPOSTAL", length = 25)
  @XmlAttribute
  private String codigoPostal;

  @Column(name = "NBPROVINCIA", length = 50)
  @XmlAttribute
  private String provincia;

  @Column(name = "NBPAIS", length = 50)
  @XmlAttribute
  private String pais;

  // ----------------------------------------------- Bean Constructors

  public AliasDireccion() {

  }

  public String getDireccionCompleta() {
    final StringBuffer direccionCompleta = new StringBuffer();
    direccionCompleta.append(getCalle());
    direccionCompleta.append(AliasDireccion.ESPACIO);
    direccionCompleta.append(getNumero());
    if (getBloque() != null && !getBloque().isEmpty()) {
      direccionCompleta.append(AliasDireccion.COMA);
      direccionCompleta.append(AliasDireccion.ESPACIO);
      direccionCompleta.append(getBloque());
    }
    direccionCompleta.append(AliasDireccion.RETORNO_CARRO);
    if (getCodigoPostal() != null && !getCodigoPostal().isEmpty()) {
      direccionCompleta.append(getCodigoPostal());
      direccionCompleta.append(AliasDireccion.ESPACIO);
    }
    direccionCompleta.append(getPoblacion());
    direccionCompleta.append(AliasDireccion.ESPACIO);
    direccionCompleta.append(AliasDireccion.PARANTENTESIS_IZQ + getProvincia() + AliasDireccion.PARANTENTESIS_DER);
    direccionCompleta.append(AliasDireccion.RETORNO_CARRO);
    direccionCompleta.append(getPais());
    // direccionCompleta.append(AliasDireccion.RETORNO_CARRO);
    return direccionCompleta.toString();
  }

  /**
   * @return the alias
   */
  public Alias getAlias() {
    return alias;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(Alias alias) {
    this.alias = alias;
  }

  /**
   * @return the calle
   */
  public String getCalle() {
    return calle;
  }

  /**
   * @param calle the calle to set
   */
  public void setCalle(String calle) {
    this.calle = calle;
  }

  /**
   * @return the numero
   */
  public String getNumero() {
    return numero;
  }

  /**
   * @param numero the numero to set
   */
  public void setNumero(String numero) {
    this.numero = numero;
  }

  /**
   * @return the bloque
   */
  public String getBloque() {
    return bloque;
  }

  /**
   * @param bloque the bloque to set
   */
  public void setBloque(String bloque) {
    this.bloque = bloque;
  }

  /**
   * @return the poblacion
   */
  public String getPoblacion() {
    return poblacion;
  }

  /**
   * @param poblacion the poblacion to set
   */
  public void setPoblacion(String poblacion) {
    this.poblacion = poblacion;
  }

  /**
   * @return the codigoPostal
   */
  public String getCodigoPostal() {
    return codigoPostal;
  }

  /**
   * @param codigoPostal the codigoPostal to set
   */
  public void setCodigoPostal(String codigoPostal) {
    this.codigoPostal = codigoPostal;
  }

  /**
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }

  /**
   * @param provincia the provincia to set
   */
  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  /**
   * @return the pais
   */
  public String getPais() {
    return pais;
  }

  /**
   * @param pais the pais to set
   */
  public void setPais(String pais) {
    this.pais = pais;
  }

}
