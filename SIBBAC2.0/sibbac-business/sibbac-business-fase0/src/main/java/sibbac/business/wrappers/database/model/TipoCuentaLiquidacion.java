package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import sibbac.database.DBConstants;


@Entity
@Table( name = DBConstants.WRAPPERS.TIOP_CUENTA_LIQUIDACION )
public class TipoCuentaLiquidacion implements Serializable {

	/**
	 * 
	 */
	private static final long				serialVersionUID	= 1L;
	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private long							id;
	@Column( name = "CD_CODIGO", unique = true, nullable = false, length = 4 )
	private String							cdCodigo;
	@Column( name = "NB_DESCRIPCION" )
	private String							nbDescripcion;
	@OneToMany
	private List< Tmct0CuentaLiquidacion >	cuentaLiquidacionList;

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	public String getNbDescripcion() {
		return nbDescripcion;
	}

	public void setNbDescripcion( String nbDescripcion ) {
		this.nbDescripcion = nbDescripcion;
	}

	public List< Tmct0CuentaLiquidacion > getCuentaLiquidacionList() {
		return cuentaLiquidacionList;
	}

	public void setCuentaLiquidacionList( List< Tmct0CuentaLiquidacion > cuentaLiquidacionList ) {
		this.cuentaLiquidacionList = cuentaLiquidacionList;
	}

}
