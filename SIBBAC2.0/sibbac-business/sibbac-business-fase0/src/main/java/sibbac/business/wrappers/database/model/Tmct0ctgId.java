package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0ctgId implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2173661790504569525L;
  private String nbooking;
  private String cdbroker;
  private String nuorden;
  private String cdmercad;

  public Tmct0ctgId() {
  }

  public Tmct0ctgId(String nbooking, String cdbroker, String nuorden, String cdmercad) {
    this.nbooking = nbooking;
    this.cdbroker = cdbroker;
    this.nuorden = nuorden;
    this.cdmercad = cdmercad;
  }

  @Column(name = "NBOOKING", nullable = false, length = 20)
  public String getNbooking() {
    return this.nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  @Column(name = "CDBROKER", nullable = false, length = 20)
  public String getCdbroker() {
    return this.cdbroker;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  @Column(name = "NUORDEN", nullable = false, length = 20)
  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  @Column(name = "CDMERCAD", nullable = false, length = 20)
  public String getCdmercad() {
    return this.cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public boolean equals(Object other) {
    if ((this == other))
      return true;
    if ((other == null))
      return false;
    if (!(other instanceof Tmct0ctdId))
      return false;
    Tmct0ctdId castOther = (Tmct0ctdId) other;

    return ((this.getNbooking() == castOther.getNbooking()) || (this.getNbooking() != null
                                                                && castOther.getNbooking() != null && this.getNbooking()
                                                                                                          .equals(castOther.getNbooking())))
           && ((this.getCdbroker() == castOther.getCdbroker()) || (this.getCdbroker() != null
                                                                   && castOther.getCdbroker() != null && this.getCdbroker()
                                                                                                             .equals(castOther.getCdbroker())))
           && ((this.getNuorden() == castOther.getNuorden()) || (this.getNuorden() != null
                                                                 && castOther.getNuorden() != null && this.getNuorden()
                                                                                                          .equals(castOther.getNuorden())))
           && ((this.getCdmercad() == castOther.getCdmercad()) || (this.getCdmercad() != null
                                                                   && castOther.getCdmercad() != null && this.getCdmercad()
                                                                                                             .equals(castOther.getCdmercad())));
  }

  public int hashCode() {
    int result = 17;

    result = 37 * result + (getNbooking() == null ? 0 : this.getNbooking().hashCode());
    result = 37 * result + (getCdbroker() == null ? 0 : this.getCdbroker().hashCode());
    result = 37 * result + (getNuorden() == null ? 0 : this.getNuorden().hashCode());
    result = 37 * result + (getCdmercad() == null ? 0 : this.getCdmercad().hashCode());
    return result;
  }

  @Override
  public String toString() {
    return String.format("Tmct0ctgId %s##%s##%s##%s", nuorden, cdbroker, cdmercad, nbooking );
  }
}
