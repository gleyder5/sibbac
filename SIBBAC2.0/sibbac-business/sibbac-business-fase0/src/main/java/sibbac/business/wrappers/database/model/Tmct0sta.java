package sibbac.business.wrappers.database.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table( name = "TMCT0STA" )
public class Tmct0sta implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3417531913085685240L;
	private Tmct0staId			id;
	private Tmct0stt			tmct0stt;
	private String				dsestado			= "";
	private String				cdestret			= "";
	private String				cdusuaud			= "";
	private Date				fhaudit				= new Date();
	private Integer				nuversion			= 0;
	private List< Tmct0bok >	tmct0boks			= new ArrayList< Tmct0bok >( 0 );
	private List< Tmct0ord >	tmct0ords			= new ArrayList< Tmct0ord >( 0 );
	private List< Tmct0msc >	tmct0mscs			= new ArrayList< Tmct0msc >( 0 );
	private List< Tmct0alo >	tmct0alos			= new ArrayList< Tmct0alo >( 0 );
	private List< Tmct0alc >	tmct0alcs			= new ArrayList< Tmct0alc >( 0 );
	private List< Tmct0men >	tmct0mens			= new ArrayList< Tmct0men >( 0 );
	private List< Tmct0cta >	tmct0ctas			= new ArrayList< Tmct0cta >( 0 );
	private List< Tmct0ctg >	tmct0ctgs			= new ArrayList< Tmct0ctg >( 0 );
	private List< Tmct0cto >	tmct0ctos			= new ArrayList< Tmct0cto >( 0 );

	// private List<Tmct0ctt> tmct0ctts = new ArrayList<Tmct0ctt>(0);
	// private List<Tmct0adi> tmct0adis = new ArrayList<Tmct0adi>(0);
	// private List<Tmct0pkb> tmct0pkbs = new ArrayList<Tmct0pkb>(0);
	// private List<Tmct0esd> tmct0esds = new ArrayList<Tmct0esd>(0);
	// private List<Tmct0det> tmct0dets = new ArrayList<Tmct0det>(0);
	// private List<Tmct0msd> tmct0msds = new ArrayList<Tmct0msd>(0);
	// private List<Tmct0cte> tmct0ctes = new ArrayList<Tmct0cte>(0);
	// private List<Tmct0msg> tmct0msgs = new ArrayList<Tmct0msg>(0);
	// private List<Tmct0ctd> tmct0ctds = new ArrayList<Tmct0ctd>(0);
	// private List<Tmct0bri> tmct0bris = new ArrayList<Tmct0bri>(0);
	// private List<Tmct0ale> tmct0ales = new ArrayList<Tmct0ale>(0);
	// private List<Tmct0ald> tmct0alds = new ArrayList<Tmct0ald>(0);
	// private List<Tmct0gdi> tmct0gdis = new ArrayList<Tmct0gdi>(0);
	// private List<Tmct0esg> tmct0esgs = new ArrayList<Tmct0esg>(0);

	public Tmct0sta() {
	}

	public Tmct0sta( Tmct0staId id) {
    this.id = id;
  }
	
	public Tmct0sta( Tmct0staId id, Tmct0stt tmct0stt ) {
		this.id = id;
		this.tmct0stt = tmct0stt;
	}

	public Tmct0sta( Tmct0staId id, Tmct0stt tmct0stt, String dsestado, String cdestret, String cdusuaud, Date fhaudit, Integer nuversion,
			List< Tmct0bok > tmct0boks, List< Tmct0ord > tmct0ords, List< Tmct0msc > tmct0mscs, List< Tmct0alo > tmct0alos,
			List< Tmct0alc > tmct0alcs, List< Tmct0men > tmct0mens, List< Tmct0cta > tmct0ctas, List< Tmct0ctg > tmct0ctgs,
			List< Tmct0cto > tmct0ctos
	/*
	 * , Set < Tmct0ctt > tmct0ctts , Set < Tmct0adi > tmct0adis , Set <
	 * Tmct0pkb > tmct0pkbs , Set < Tmct0esd > tmct0esds , Set < Tmct0det >
	 * tmct0dets , Set < Tmct0msd > tmct0msds , Set < Tmct0cte > tmct0ctes , Set
	 * < Tmct0msg > tmct0msgs , Set < Tmct0ctd > tmct0ctds , Set < Tmct0bri >
	 * tmct0bris, Set < Tmct0ale > tmct0ales , Set < Tmct0ald > tmct0alds , Set
	 * < Tmct0gdi > tmct0gdis , Set < Tmct0esg > tmct0esgs
	 */) {
		this.id = id;
		this.tmct0stt = tmct0stt;
		this.dsestado = dsestado;
		this.cdestret = cdestret;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
		this.tmct0boks = tmct0boks;
		this.tmct0ords = tmct0ords;
		this.tmct0mscs = tmct0mscs;
		this.tmct0alos = tmct0alos;
		this.tmct0mens = tmct0mens;
		this.tmct0ctas = tmct0ctas;
		this.tmct0ctgs = tmct0ctgs;
		this.tmct0ctos = tmct0ctos;
		// this.tmct0alcs = tmct0alc;
		// this.tmct0ctts = tmct0ctts;
		// this.tmct0adis = tmct0adis;
		// this.tmct0pkbs = tmct0pkbs;
		// this.tmct0esds = tmct0esds;
		// this.tmct0dets = tmct0dets;
		// this.tmct0msds = tmct0msds;
		// this.tmct0ctes = tmct0ctes;
		// this.tmct0msgs = tmct0msgs;
		// this.tmct0ctds = tmct0ctds;
		// this.tmct0bris = tmct0bris;
		// this.tmct0ales = tmct0ales;
		// this.tmct0alds = tmct0alds;
		// this.tmct0gdis = tmct0gdis;
		// this.tmct0esgs = tmct0esgs;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "cdestado", column = @Column( name = "CDESTADO", nullable = false, length = 3 ) ),
			@AttributeOverride( name = "cdtipest", column = @Column( name = "CDTIPEST", nullable = false, length = 10 ) )
	} )
	public Tmct0staId getId() {
		return this.id;
	}

	public void setId( Tmct0staId id ) {
		this.id = id;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "CDTIPEST", nullable = false, insertable = false, updatable = false )
	public Tmct0stt getTmct0stt() {
		return this.tmct0stt;
	}

	public void setTmct0stt( Tmct0stt tmct0stt ) {
		this.tmct0stt = tmct0stt;
	}

	@Column( name = "DSESTADO", length = 40 )
	public String getDsestado() {
		return this.dsestado;
	}

	public void setDsestado( String dsestado ) {
		this.dsestado = dsestado;
	}

	@Column( name = "CDESTRET", length = 3 )
	public String getCdestret() {
		return this.cdestret;
	}

	public void setCdestret( String cdestret ) {
		this.cdestret = cdestret;
	}

	@Column( name = "CDUSUAUD", length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "NUVERSION" )
	public Integer getNuversion() {
		return this.nuversion;
	}

	public void setNuversion( Integer nuversion ) {
		this.nuversion = nuversion;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0bok > getTmct0boks() {
		return this.tmct0boks;
	}

	public void setTmct0boks( List< Tmct0bok > tmct0boks ) {
		this.tmct0boks = tmct0boks;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0ord > getTmct0ords() {
		return this.tmct0ords;
	}

	public void setTmct0ords( List< Tmct0ord > tmct0ords ) {
		this.tmct0ords = tmct0ords;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0msc > getTmct0mscs() {
		return this.tmct0mscs;
	}

	public void setTmct0mscs( List< Tmct0msc > tmct0mscs ) {
		this.tmct0mscs = tmct0mscs;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0alo > getTmct0alos() {
		return this.tmct0alos;
	}

	public void setTmct0alos( List< Tmct0alo > tmct0alos ) {
		this.tmct0alos = tmct0alos;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0alc > getTmct0alcs() {
		return this.tmct0alcs;
	}

	public void setTmct0alcs( List< Tmct0alc > tmct0alcs ) {
		this.tmct0alcs = tmct0alcs;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0men > getTmct0mens() {
		return this.tmct0mens;
	}

	public void setTmct0mens( List< Tmct0men > tmct0mens ) {
		this.tmct0mens = tmct0mens;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0cta > getTmct0ctas() {
		return this.tmct0ctas;
	}

	public void setTmct0ctas( List< Tmct0cta > tmct0ctas ) {
		this.tmct0ctas = tmct0ctas;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0ctg > getTmct0ctgs() {
		return this.tmct0ctgs;
	}

	public void setTmct0ctgs( List< Tmct0ctg > tmct0ctgs ) {
		this.tmct0ctgs = tmct0ctgs;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0sta" )
	public List< Tmct0cto > getTmct0ctos() {
		return this.tmct0ctos;
	}

	public void setTmct0ctos( List< Tmct0cto > tmct0ctos ) {
		this.tmct0ctos = tmct0ctos;
	}

	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0ctt> getTmct0ctts() {
	// return this.tmct0ctts;
	// }
	//
	// public void setTmct0ctts(List<Tmct0ctt> tmct0ctts) {
	// this.tmct0ctts = tmct0ctts;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0adi> getTmct0adis() {
	// return this.tmct0adis;
	// }
	//
	// public void setTmct0adis(List<Tmct0adi> tmct0adis) {
	// this.tmct0adis = tmct0adis;
	// }
	//
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0pkb> getTmct0pkbs() {
	// return this.tmct0pkbs;
	// }
	//
	// public void setTmct0pkbs(List<Tmct0pkb> tmct0pkbs) {
	// this.tmct0pkbs = tmct0pkbs;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0esd> getTmct0esds() {
	// return this.tmct0esds;
	// }
	//
	// public void setTmct0esds(List<Tmct0esd> tmct0esds) {
	// this.tmct0esds = tmct0esds;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0det> getTmct0dets() {
	// return this.tmct0dets;
	// }
	//
	// public void setTmct0dets(List<Tmct0det> tmct0dets) {
	// this.tmct0dets = tmct0dets;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0msd> getTmct0msds() {
	// return this.tmct0msds;
	// }
	//
	// public void setTmct0msds(List<Tmct0msd> tmct0msds) {
	// this.tmct0msds = tmct0msds;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0cte> getTmct0ctes() {
	// return this.tmct0ctes;
	// }
	//
	// public void setTmct0ctes(List<Tmct0cte> tmct0ctes) {
	// this.tmct0ctes = tmct0ctes;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0msg> getTmct0msgs() {
	// return this.tmct0msgs;
	// }
	//
	// public void setTmct0msgs(List<Tmct0msg> tmct0msgs) {
	// this.tmct0msgs = tmct0msgs;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0ctd> getTmct0ctds() {
	// return this.tmct0ctds;
	// }
	//
	// public void setTmct0ctds(List<Tmct0ctd> tmct0ctds) {
	// this.tmct0ctds = tmct0ctds;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0bri> getTmct0bris() {
	// return this.tmct0bris;
	// }
	//
	// public void setTmct0bris(List<Tmct0bri> tmct0bris) {
	// this.tmct0bris = tmct0bris;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0ale> getTmct0ales() {
	// return this.tmct0ales;
	// }
	//
	// public void setTmct0ales(List<Tmct0ale> tmct0ales) {
	// this.tmct0ales = tmct0ales;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0ald> getTmct0alds() {
	// return this.tmct0alds;
	// }
	//
	// public void setTmct0alds(List<Tmct0ald> tmct0alds) {
	// this.tmct0alds = tmct0alds;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0gdi> getTmct0gdis() {
	// return this.tmct0gdis;
	// }
	//
	// public void setTmct0gdis(List<Tmct0gdi> tmct0gdis) {
	// this.tmct0gdis = tmct0gdis;
	// }
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0sta")
	// public List<Tmct0esg> getTmct0esgs() {
	// return this.tmct0esgs;
	// }
	//
	// public void setTmct0esgs(List<Tmct0esg> tmct0esgs) {
	// this.tmct0esgs = tmct0esgs;
	// }
}
