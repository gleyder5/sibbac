package sibbac.business.wrappers.database.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.CodigoError }. Entity: "CodigoError".
 * 
 * @version 1.0
 * @since 1.0
 */

//
@Table(name = WRAPPERS.CODIGO_ERROR)
@Entity
public class CodigoError extends ATable<CodigoError> implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 6342646785854082902L;
  /**
	 * 
	 */
  @Column(name = "CODIGO", nullable = false, length = 10)
  private String codigo;

  @Column(name = "DESCRIPCION", nullable = false, length = 255)
  private String descripcion;

  @Column(name = "GRUPO", nullable = false, length = 30)
  private String grupo;

  @ManyToMany(mappedBy = "errors",
              fetch = FetchType.LAZY,
              cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
              })
  private Set<Tmct0AfiError> tmct0AfiError = null;

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getGrupo() {
    return grupo;
  }

  public void setGrupo(String grupo) {
    this.grupo = grupo;
  }

  public Set<Tmct0AfiError> getTmct0AfiError() {
    return tmct0AfiError;
  }

  public void setTmct0AfiError(Set<Tmct0AfiError> tmct0AfiError) {
    this.tmct0AfiError = tmct0AfiError;
  }

  public void addTmct0AfiError(Tmct0AfiError error) {
    if (tmct0AfiError == null) {
      tmct0AfiError = new HashSet<Tmct0AfiError>();
    }
    tmct0AfiError.add(error);
  }
}
