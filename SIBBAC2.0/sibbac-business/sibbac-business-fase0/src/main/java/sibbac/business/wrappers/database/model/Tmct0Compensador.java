package sibbac.business.wrappers.database.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0_COMPENSADOR")
public class Tmct0Compensador implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 7566440379690501891L;
  private Long idCompensador;
  private Tmct0TipoCompensador tmct0TipoCompensador;
  private String nbNombre;
  private String nbDescripcion;
  private String nbDomicilio;
  private String nbEmail;
  private String nbTelefono;
  private Date auditFechaCambio;
  private String auditUser;
  private char compensadorInterno;
  private String cdBic;
  private String cdCif;
  private String cdAbreviatura;
  private Character corretajePti;
  private List<Tmct0CuentasDeCompensacion> tmct0CuentasDeCompensacions = new ArrayList<Tmct0CuentasDeCompensacion>(0);

  // private Set<Tmct0Nemotecnicos> tmct0Nemotecnicoses = new
  // ArrayList<Tmct0Nemotecnicos>(0);
  // private Set<Tmct0Parametrizacion> tmct0Parametrizacions = new
  // ArrayList<Tmct0Parametrizacion>(0);

  public Tmct0Compensador() {
  }

  public Tmct0Compensador(Long idCompensador,
                          Tmct0TipoCompensador tmct0TipoCompensador,
                          String nbNombre,
                          Date auditFechaCambio,
                          char compensadorInterno) {
    this.idCompensador = idCompensador;
    this.tmct0TipoCompensador = tmct0TipoCompensador;
    this.nbNombre = nbNombre;
    this.auditFechaCambio = auditFechaCambio;
    this.compensadorInterno = compensadorInterno;
  }

  public Tmct0Compensador(Long idCompensador,
                          Tmct0TipoCompensador tmct0TipoCompensador,
                          String nbNombre,
                          String nbDescripcion,
                          String nbDomicilio,
                          String nbEmail,
                          String nbTelefono,
                          Date auditFechaCambio,
                          String auditUser,
                          char compensadorInterno,
                          String cdBic,
                          String cdCif,
                          String cdAbreviatura,
                          List<Tmct0CuentasDeCompensacion> tmct0CuentasDeCompensacions/*
                                                                                       * , Set< Tmct0Nemotecnicos >
                                                                                       * tmct0Nemotecnicoses , Set<
                                                                                       * Tmct0Parametrizacion >
                                                                                       * tmct0Parametrizacions
                                                                                       */) {
    this.idCompensador = idCompensador;
    this.tmct0TipoCompensador = tmct0TipoCompensador;
    this.nbNombre = nbNombre;
    this.nbDescripcion = nbDescripcion;
    this.nbDomicilio = nbDomicilio;
    this.nbEmail = nbEmail;
    this.nbTelefono = nbTelefono;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.compensadorInterno = compensadorInterno;
    this.cdBic = cdBic;
    this.cdCif = cdCif;
    this.cdAbreviatura = cdAbreviatura;
    this.tmct0CuentasDeCompensacions = tmct0CuentasDeCompensacions;
    // this.tmct0Nemotecnicoses = tmct0Nemotecnicoses;
    // this.tmct0Parametrizacions = tmct0Parametrizacions;
  }

  @Id
  @Column(name = "ID_COMPENSADOR", unique = true, nullable = false)
  public Long getIdCompensador() {
    return this.idCompensador;
  }

  public void setIdCompensador(Long idCompensador) {
    this.idCompensador = idCompensador;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_TIPO_COMPENSADOR", nullable = false)
  public Tmct0TipoCompensador getTmct0TipoCompensador() {
    return this.tmct0TipoCompensador;
  }

  public void setTmct0TipoCompensador(Tmct0TipoCompensador tmct0TipoCompensador) {
    this.tmct0TipoCompensador = tmct0TipoCompensador;
  }

  @Column(name = "NB_NOMBRE", unique = true, nullable = false, length = 225)
  public String getNbNombre() {
    return this.nbNombre;
  }

  public void setNbNombre(String nbNombre) {
    this.nbNombre = nbNombre;
  }

  @Column(name = "NB_DESCRIPCION", length = 225)
  public String getNbDescripcion() {
    return this.nbDescripcion;
  }

  public void setNbDescripcion(String nbDescripcion) {
    this.nbDescripcion = nbDescripcion;
  }

  @Column(name = "NB_DOMICILIO")
  public String getNbDomicilio() {
    return this.nbDomicilio;
  }

  public void setNbDomicilio(String nbDomicilio) {
    this.nbDomicilio = nbDomicilio;
  }

  @Column(name = "NB_EMAIL")
  public String getNbEmail() {
    return this.nbEmail;
  }

  public void setNbEmail(String nbEmail) {
    this.nbEmail = nbEmail;
  }

  @Column(name = "NB_TELEFONO", length = 64)
  public String getNbTelefono() {
    return this.nbTelefono;
  }

  public void setNbTelefono(String nbTelefono) {
    this.nbTelefono = nbTelefono;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", nullable = false, length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "COMPENSADOR_INTERNO", nullable = false, length = 1)
  public char getCompensadorInterno() {
    return this.compensadorInterno;
  }

  public void setCompensadorInterno(char compensadorInterno) {
    this.compensadorInterno = compensadorInterno;
  }

  @Column(name = "CD_BIC", length = 16)
  public String getCdBic() {
    return this.cdBic;
  }

  public void setCdBic(String cdBic) {
    this.cdBic = cdBic;
  }

  @Column(name = "CD_CIF", length = 16)
  public String getCdCif() {
    return this.cdCif;
  }

  public void setCdCif(String cdCif) {
    this.cdCif = cdCif;
  }

  @Column(name = "CD_ABREVIATURA", length = 16)
  public String getCdAbreviatura() {
    return this.cdAbreviatura;
  }

  public void setCdAbreviatura(String cdAbreviatura) {
    this.cdAbreviatura = cdAbreviatura;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0Compensador")
  public List<Tmct0CuentasDeCompensacion> getTmct0CuentasDeCompensacions() {
    return this.tmct0CuentasDeCompensacions;
  }

  public void setTmct0CuentasDeCompensacions(List<Tmct0CuentasDeCompensacion> tmct0CuentasDeCompensacions) {
    this.tmct0CuentasDeCompensacions = tmct0CuentasDeCompensacions;
  }

  @Column(name = "CORRETAJE_PTI", length = 1)
  public Character getCorretajePti() {
    return corretajePti;
  }

  public void setCorretajePti(Character corretajePti) {
    this.corretajePti = corretajePti;
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0Compensador")
  // public Set<Tmct0Nemotecnicos> getTmct0Nemotecnicoses() {
  // return this.tmct0Nemotecnicoses;
  // }
  //
  // public void setTmct0Nemotecnicoses(
  // Set<Tmct0Nemotecnicos> tmct0Nemotecnicoses) {
  // this.tmct0Nemotecnicoses = tmct0Nemotecnicoses;
  // }
  //
  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0Compensador")
  // public Set<Tmct0Parametrizacion> getTmct0Parametrizacions() {
  // return this.tmct0Parametrizacions;
  // }
  //
  // public void setTmct0Parametrizacions(
  // Set<Tmct0Parametrizacion> tmct0Parametrizacions) {
  // this.tmct0Parametrizacions = tmct0Parametrizacions;
  // }

}
