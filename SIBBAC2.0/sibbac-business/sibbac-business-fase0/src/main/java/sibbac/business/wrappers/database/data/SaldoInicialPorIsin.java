package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class SaldoInicialPorIsin {

	private String						isin;
	private String						descrisin;
	private List< SaldoInicialIsin >	grupoBalanceISINList;
	private BigDecimal					totalEfectivos;
	private BigDecimal					totalTitulos;
	private Date fechaLiquidacion;
	public void addGrupoBalanceISIN( SaldoInicialIsin grupo ) {
		if ( this.grupoBalanceISINList == null ) {
			grupoBalanceISINList = new LinkedList< SaldoInicialIsin >();
		}
		this.grupoBalanceISINList.add( grupo );
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescrisin() {
		return descrisin;
	}

	public void setDescrisin( String descrisin ) {
		this.descrisin = descrisin;
	}

	public List< SaldoInicialIsin > getGrupoBalanceISINList() {
		return grupoBalanceISINList;
	}

	public void setGrupoBalanceISINList( List< SaldoInicialIsin > grupoBalanceISINList ) {
		this.grupoBalanceISINList = grupoBalanceISINList;
	}

	public BigDecimal getTotalEfectivos() {
		return totalEfectivos;
	}

	public void setTotalEfectivos( BigDecimal totalEfectivos ) {
		this.totalEfectivos = totalEfectivos;
	}

	public BigDecimal getTotalTitulos() {
		return totalTitulos;
	}

	public void setTotalTitulos( BigDecimal totalTitulos ) {
		this.totalTitulos = totalTitulos;
	}

	public Date getFechaLiquidacion() {
	    return fechaLiquidacion;
	}

	public void setFechaLiquidacion(Date fechaLiquidacion) {
	    this.fechaLiquidacion = fechaLiquidacion;
	}

}
