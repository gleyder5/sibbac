package sibbac.business.wrappers.database.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;

@Entity
@Table(name = "TMCT0ALO")
public class Tmct0alo implements java.io.Serializable {

  private static final long serialVersionUID = 7447495864395419381L;
  public static final String CTE_PARTENON = "PARTENON";
  public static final Integer CTE_OPERACION_ESPECIAL = 215;
  private Tmct0aloId id;
  private Tmct0bok tmct0bok;
  private Tmct0estado tmct0estadoByCdestadoasig;
  private Tmct0estado tmct0estadoByCdestadocont;
  private Tmct0sta tmct0sta;
  private String cdalias = " ";
  private String descrali = " ";
  private String cdusuges = " ";
  private Date fevalor = new Date();
  private Date feoperac = new Date();
  private Date hooperac = new Date();
  private Character cdtpoper = ' ';
  private String nbtitulr = " ";
  private BigDecimal nutitcli = new BigDecimal(0.0);
  private BigDecimal nutitpen = new BigDecimal(0.0);
  private String cdisin = " ";
  private String nbvalors = " ";
  private Character cdenvcnf = ' ';
  private Character cdenvoas = ' ';
  private Character cdenvseq = ' ';
  private Character cdestoas = ' ';
  private String cdrefoas = " ";
  private Date fhenvoas = new Date();
  private Date hoenvoas = new Date();
  private Date fhenvseq = new Date();
  private Date hoenvseq = new Date();
  private String cdcustod = " ";
  private String acctatcus = " ";
  private String cdbencus = " ";
  private String acbencus = " ";
  private String idbencus = " ";
  private String gcuscode = " ";
  private String gcusacct = " ";
  private String gcusid = " ";
  private String lcuscode = " ";
  private String lcusacct = " ";
  private String lcusid = " ";
  private String pccusset = " ";
  private Character sendbecus = ' ';
  private String tpsetcus = " ";
  private String tysetcus = " ";
  private String cdcleare = " ";
  private String acctatcle = " ";
  private String cdbencle = " ";
  private String acbencle = " ";
  private String idbencle = " ";
  private String gclecode = " ";
  private String gcleacct = " ";
  private String gcleid = " ";
  private String lclecode = " ";
  private String lcleacct = " ";
  private String lcleid = " ";
  private String pccleset = " ";
  private Character sendbecle = ' ';
  private String tpsetcle = " ";
  private String tysetcle = " ";
  private BigDecimal imcammed = new BigDecimal(0.0);
  private BigDecimal imcamnet = new BigDecimal(0.0);
  private BigDecimal imtotbru = new BigDecimal(0.0);
  private BigDecimal imtotnet = new BigDecimal(0.0);
  private BigDecimal eutotbru = new BigDecimal(0.0);
  private BigDecimal eutotnet = new BigDecimal(0.0);
  private BigDecimal imnetbrk = new BigDecimal(0.0);
  private BigDecimal eunetbrk = new BigDecimal(0.0);
  private BigDecimal imcomisn = new BigDecimal(0.0);
  private BigDecimal imcomieu = new BigDecimal(0.0);
  private BigDecimal pccomisn = new BigDecimal(0.0);
  private BigDecimal imcomdev = new BigDecimal(0.0);
  private BigDecimal eucomdev = new BigDecimal(0.0);
  private BigDecimal imfibrdv = new BigDecimal(0.0);
  private BigDecimal imfibreu = new BigDecimal(0.0);
  private BigDecimal imsvb = new BigDecimal(0.0);
  private BigDecimal imsvbeu = new BigDecimal(0.0);
  private BigDecimal imtpcamb = new BigDecimal(0.0);
  private String cdmoniso = " ";
  private Character cdindgas = ' ';
  private Character cdindimp = ' ';
  private Character cdreside = ' ';
  private BigDecimal imbrmerc = new BigDecimal(0.0);
  private BigDecimal imbrmreu = new BigDecimal(0.0);
  private BigDecimal imgastos = new BigDecimal(0.0);
  private BigDecimal imgatdvs = new BigDecimal(0.0);
  private BigDecimal imimpdvs = new BigDecimal(0.0);
  private BigDecimal imimpeur = new BigDecimal(0.0);
  private BigDecimal imcconeu = new BigDecimal(0.0);
  private BigDecimal imcliqeu = new BigDecimal(0.0);
  private BigDecimal imdereeu = new BigDecimal(0.0);
  private Character xtdcm = ' ';
  private Character xtderech = ' ';
  private String rfparten = " ";
  private Character tpcalcul = ' ';
  private Character indatfis = ' ';
  private String erdatfis = " ";
  private Character cddatliq = ' ';
  private Character cdbloque = ' ';
  private Integer nuoprout = 0;
  private Short nupareje = 0;
  private String dsobserv = " ";
  private String cdusublo = " ";
  private String cdusuaud = " ";
  private Date fhaudit = new Date();
  private Integer nuversion = 0;
  private Integer nuagrpkb = 0;
  private String pkentity = " ";
  private String biccus = " ";
  private String namecus = " ";
  private String lcusbic = " ";
  private String cdloccus = " ";
  private String gcusbic = " ";
  private String cdglocus = " ";
  private String bcusbic = " ";
  private String cdnbcus = " ";
  private String biccle = " ";
  private String namecle = " ";
  private String lclebic = " ";
  private String cdloccle = " ";
  private String gclebic = " ";
  private String cdglocle = " ";
  private String bclebic = " ";
  private String cdnbcle = " ";
  private Date fevalorr = new Date();
  private BigDecimal imliq = new BigDecimal(0.0);
  private BigDecimal imeurliq = new BigDecimal(0.0);
  private BigDecimal imajliq = new BigDecimal(0.0);
  private BigDecimal imajeliq = new BigDecimal(0.0);
  private BigDecimal imgasliqdv;
  private BigDecimal imgasliqeu;
  private BigDecimal imgasgesdv;
  private BigDecimal imgasgeseu;
  private BigDecimal imgastotdv;
  private BigDecimal imgastoteu;
  private String refbanif;
  private String refcltdes;
  private String cdholder;
  private Integer cdestadotit;
  private String reftitular;
  private BigDecimal imcorretajesvfinaldv = new BigDecimal(0.0);
  private BigDecimal imcomclteeu = new BigDecimal(0.0);
  private Integer canoncontrpagoclte;
  private Integer canoncontrpagomkt;
  private Integer canonliqpagoclte;
  private Integer canonliqpagomkt;
  private Integer canoncomppagoclte;
  private Integer canoncomppagomkt;
  private BigDecimal imcanoncompdv;
  private BigDecimal imcanoncompeu;
  private BigDecimal imcanoncontrdv;
  private BigDecimal imcanoncontreu;
  private BigDecimal imcanonliqdv;
  private BigDecimal imcanonliqeu;
  private List<Tmct0alc> tmct0alcs = new ArrayList<Tmct0alc>();

  /**
   * Número de títulos fallidos si el sentido de la operación es 'venta' y
   * número de títulos afectados si el sentido de la operación es 'compra'.
   */
  private BigDecimal nutitfallidos;

  private List<Tmct0grupoejecucion> tmct0grupoejecucions = new ArrayList<Tmct0grupoejecucion>();

  private List<Tmct0aloDynamicValues> dynamicValues = new ArrayList<Tmct0aloDynamicValues>();

  private String ccv;

  private String refCliente;

  public Tmct0alo() {
  }

  public Tmct0alo(Tmct0aloId id, Tmct0bok tmct0bok, Tmct0sta tmct0sta, String erdatfis) {
    this.id = id;
    this.tmct0bok = tmct0bok;
    this.tmct0sta = tmct0sta;
    this.erdatfis = erdatfis;
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)) })
  public Tmct0aloId getId() {
    return this.id;
  }

  public void setId(Tmct0aloId id) {
    this.id = id;
  }

  @Transient
  public String getCdmercad() {
    String cdmercad = null;
    final Tmct0bok tmct0bok = this.getTmct0bok();
    if (tmct0bok != null) {
      cdmercad = tmct0bok.getCdmercad();
    }
    return cdmercad;
  }

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false) })
  public Tmct0bok getTmct0bok() {
    return this.tmct0bok;
  }

  public void setTmct0bok(Tmct0bok tmct0bok) {
    this.tmct0bok = tmct0bok;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOASIG")
  public Tmct0estado getTmct0estadoByCdestadoasig() {
    return this.tmct0estadoByCdestadoasig;
  }

  public void setTmct0estadoByCdestadoasig(Tmct0estado tmct0estadoByCdestadoasig) {
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT")
  public Tmct0estado getTmct0estadoByCdestadocont() {
    return this.tmct0estadoByCdestadocont;
  }

  public void setTmct0estadoByCdestadocont(Tmct0estado tmct0estadoByCdestadocont) {
    this.tmct0estadoByCdestadocont = tmct0estadoByCdestadocont;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  public Tmct0sta getTmct0sta() {
    return this.tmct0sta;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Column(name = "CDALIAS", length = 20)
  public String getCdalias() {
    return this.cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  @Column(name = "DESCRALI", length = 130)
  public String getDescrali() {
    return this.descrali;
  }

  public void setDescrali(String descrali) {
    this.descrali = descrali;
  }

  @Column(name = "CDUSUGES", length = 10)
  public String getCdusuges() {
    return this.cdusuges;
  }

  public void setCdusuges(String cdusuges) {
    this.cdusuges = cdusuges;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 4)
  public Date getFevalor() {
    return this.fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEOPERAC", length = 4)
  public Date getFeoperac() {
    return this.feoperac;
  }

  public void setFeoperac(Date feoperac) {
    this.feoperac = feoperac;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOOPERAC", length = 3)
  public Date getHooperac() {
    return this.hooperac;
  }

  public void setHooperac(Date hooperac) {
    this.hooperac = hooperac;
  }

  @Column(name = "CDTPOPER", length = 1)
  public Character getCdtpoper() {
    return this.cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  @Column(name = "NBTITULR", length = 130)
  public String getNbtitulr() {
    return this.nbtitulr;
  }

  public void setNbtitulr(String nbtitulr) {
    this.nbtitulr = nbtitulr;
  }

  @Column(name = "NUTITCLI", precision = 16, scale = 5)
  public BigDecimal getNutitcli() {
    return this.nutitcli;
  }

  public void setNutitcli(BigDecimal nutitcli) {
    this.nutitcli = nutitcli;
  }

  @Column(name = "NUTITPEN", precision = 16, scale = 5)
  public BigDecimal getNutitpen() {
    return this.nutitpen;
  }

  public void setNutitpen(BigDecimal nutitpen) {
    this.nutitpen = nutitpen;
  }

  @Column(name = "CDISIN", length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "NBVALORS", length = 40)
  public String getNbvalors() {
    return this.nbvalors;
  }

  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  @Column(name = "CDENVCNF", length = 1)
  public Character getCdenvcnf() {
    return this.cdenvcnf;
  }

  public void setCdenvcnf(Character cdenvcnf) {
    this.cdenvcnf = cdenvcnf;
  }

  @Column(name = "CDENVOAS", length = 1)
  public Character getCdenvoas() {
    return this.cdenvoas;
  }

  public void setCdenvoas(Character cdenvoas) {
    this.cdenvoas = cdenvoas;
  }

  @Column(name = "CDENVSEQ", length = 1)
  public Character getCdenvseq() {
    return this.cdenvseq;
  }

  public void setCdenvseq(Character cdenvseq) {
    this.cdenvseq = cdenvseq;
  }

  @Column(name = "CDESTOAS", length = 1)
  public Character getCdestoas() {
    return this.cdestoas;
  }

  public void setCdestoas(Character cdestoas) {
    this.cdestoas = cdestoas;
  }

  @Column(name = "CDREFOAS", length = 15)
  public String getCdrefoas() {
    return this.cdrefoas;
  }

  public void setCdrefoas(String cdrefoas) {
    this.cdrefoas = cdrefoas;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FHENVOAS", length = 4)
  public Date getFhenvoas() {
    return this.fhenvoas;
  }

  public void setFhenvoas(Date fhenvoas) {
    this.fhenvoas = fhenvoas;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOENVOAS", length = 3)
  public Date getHoenvoas() {
    return this.hoenvoas;
  }

  public void setHoenvoas(Date hoenvoas) {
    this.hoenvoas = hoenvoas;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FHENVSEQ", length = 4)
  public Date getFhenvseq() {
    return this.fhenvseq;
  }

  public void setFhenvseq(Date fhenvseq) {
    this.fhenvseq = fhenvseq;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOENVSEQ", length = 3)
  public Date getHoenvseq() {
    return this.hoenvseq;
  }

  public void setHoenvseq(Date hoenvseq) {
    this.hoenvseq = hoenvseq;
  }

  @Column(name = "CDCUSTOD", length = 25)
  public String getCdcustod() {
    return this.cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  @Column(name = "ACCTATCUS", length = 34)
  public String getAcctatcus() {
    return this.acctatcus;
  }

  public void setAcctatcus(String acctatcus) {
    this.acctatcus = acctatcus;
  }

  @Column(name = "CDBENCUS", length = 146)
  public String getCdbencus() {
    return this.cdbencus;
  }

  public void setCdbencus(String cdbencus) {
    this.cdbencus = cdbencus;
  }

  @Column(name = "ACBENCUS", length = 34)
  public String getAcbencus() {
    return this.acbencus;
  }

  public void setAcbencus(String acbencus) {
    this.acbencus = acbencus;
  }

  @Column(name = "IDBENCUS", length = 43)
  public String getIdbencus() {
    return this.idbencus;
  }

  public void setIdbencus(String idbencus) {
    this.idbencus = idbencus;
  }

  @Column(name = "GCUSCODE", length = 146)
  public String getGcuscode() {
    return this.gcuscode;
  }

  public void setGcuscode(String gcuscode) {
    this.gcuscode = gcuscode;
  }

  @Column(name = "GCUSACCT", length = 34)
  public String getGcusacct() {
    return this.gcusacct;
  }

  public void setGcusacct(String gcusacct) {
    this.gcusacct = gcusacct;
  }

  @Column(name = "GCUSID", length = 43)
  public String getGcusid() {
    return this.gcusid;
  }

  public void setGcusid(String gcusid) {
    this.gcusid = gcusid;
  }

  @Column(name = "LCUSCODE", length = 146)
  public String getLcuscode() {
    return this.lcuscode;
  }

  public void setLcuscode(String lcuscode) {
    this.lcuscode = lcuscode;
  }

  @Column(name = "LCUSACCT", length = 34)
  public String getLcusacct() {
    return this.lcusacct;
  }

  public void setLcusacct(String lcusacct) {
    this.lcusacct = lcusacct;
  }

  @Column(name = "LCUSID", length = 43)
  public String getLcusid() {
    return this.lcusid;
  }

  public void setLcusid(String lcusid) {
    this.lcusid = lcusid;
  }

  @Column(name = "PCCUSSET", length = 43)
  public String getPccusset() {
    return this.pccusset;
  }

  public void setPccusset(String pccusset) {
    this.pccusset = pccusset;
  }

  @Column(name = "SENDBECUS", length = 1)
  public Character getSendbecus() {
    return this.sendbecus;
  }

  public void setSendbecus(Character sendbecus) {
    this.sendbecus = sendbecus;
  }

  @Column(name = "TPSETCUS", length = 25)
  public String getTpsetcus() {
    return this.tpsetcus;
  }

  public void setTpsetcus(String tpsetcus) {
    this.tpsetcus = tpsetcus;
  }

  @Column(name = "TYSETCUS", length = 25)
  public String getTysetcus() {
    return this.tysetcus;
  }

  public void setTysetcus(String tysetcus) {
    this.tysetcus = tysetcus;
  }

  @Column(name = "CDCLEARE", length = 25)
  public String getCdcleare() {
    return this.cdcleare;
  }

  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  @Column(name = "ACCTATCLE", length = 34)
  public String getAcctatcle() {
    return this.acctatcle;
  }

  public void setAcctatcle(String acctatcle) {
    this.acctatcle = acctatcle;
  }

  @Column(name = "CDBENCLE", length = 146)
  public String getCdbencle() {
    return this.cdbencle;
  }

  public void setCdbencle(String cdbencle) {
    this.cdbencle = cdbencle;
  }

  @Column(name = "ACBENCLE", length = 34)
  public String getAcbencle() {
    return this.acbencle;
  }

  public void setAcbencle(String acbencle) {
    this.acbencle = acbencle;
  }

  @Column(name = "IDBENCLE", length = 43)
  public String getIdbencle() {
    return this.idbencle;
  }

  public void setIdbencle(String idbencle) {
    this.idbencle = idbencle;
  }

  @Column(name = "GCLECODE", length = 146)
  public String getGclecode() {
    return this.gclecode;
  }

  public void setGclecode(String gclecode) {
    this.gclecode = gclecode;
  }

  @Column(name = "GCLEACCT", length = 34)
  public String getGcleacct() {
    return this.gcleacct;
  }

  public void setGcleacct(String gcleacct) {
    this.gcleacct = gcleacct;
  }

  @Column(name = "GCLEID", length = 43)
  public String getGcleid() {
    return this.gcleid;
  }

  public void setGcleid(String gcleid) {
    this.gcleid = gcleid;
  }

  @Column(name = "LCLECODE", length = 146)
  public String getLclecode() {
    return this.lclecode;
  }

  public void setLclecode(String lclecode) {
    this.lclecode = lclecode;
  }

  @Column(name = "LCLEACCT", length = 34)
  public String getLcleacct() {
    return this.lcleacct;
  }

  public void setLcleacct(String lcleacct) {
    this.lcleacct = lcleacct;
  }

  @Column(name = "LCLEID", length = 43)
  public String getLcleid() {
    return this.lcleid;
  }

  public void setLcleid(String lcleid) {
    this.lcleid = lcleid;
  }

  @Column(name = "PCCLESET", length = 43)
  public String getPccleset() {
    return this.pccleset;
  }

  public void setPccleset(String pccleset) {
    this.pccleset = pccleset;
  }

  @Column(name = "SENDBECLE", length = 1)
  public Character getSendbecle() {
    return this.sendbecle;
  }

  public void setSendbecle(Character sendbecle) {
    this.sendbecle = sendbecle;
  }

  @Column(name = "TPSETCLE", length = 25)
  public String getTpsetcle() {
    return this.tpsetcle;
  }

  public void setTpsetcle(String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  @Column(name = "TYSETCLE", length = 25)
  public String getTysetcle() {
    return this.tysetcle;
  }

  public void setTysetcle(String tysetcle) {
    this.tysetcle = tysetcle;
  }

  @Column(name = "IMCAMMED", precision = 18, scale = 8)
  public BigDecimal getImcammed() {
    return this.imcammed;
  }

  public void setImcammed(BigDecimal imcammed) {
    this.imcammed = imcammed;
  }

  @Column(name = "IMCAMNET", precision = 18, scale = 8)
  public BigDecimal getImcamnet() {
    return this.imcamnet;
  }

  public void setImcamnet(BigDecimal imcamnet) {
    this.imcamnet = imcamnet;
  }

  @Column(name = "IMTOTBRU", precision = 18, scale = 8)
  public BigDecimal getImtotbru() {
    return this.imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  @Column(name = "IMTOTNET", precision = 18, scale = 8)
  public BigDecimal getImtotnet() {
    return this.imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  @Column(name = "EUTOTBRU", precision = 18, scale = 8)
  public BigDecimal getEutotbru() {
    return this.eutotbru;
  }

  public void setEutotbru(BigDecimal eutotbru) {
    this.eutotbru = eutotbru;
  }

  @Column(name = "EUTOTNET", precision = 18, scale = 8)
  public BigDecimal getEutotnet() {
    return this.eutotnet;
  }

  public void setEutotnet(BigDecimal eutotnet) {
    this.eutotnet = eutotnet;
  }

  @Column(name = "IMNETBRK", precision = 18, scale = 8)
  public BigDecimal getImnetbrk() {
    return this.imnetbrk;
  }

  public void setImnetbrk(BigDecimal imnetbrk) {
    this.imnetbrk = imnetbrk;
  }

  @Column(name = "EUNETBRK", precision = 18, scale = 8)
  public BigDecimal getEunetbrk() {
    return this.eunetbrk;
  }

  public void setEunetbrk(BigDecimal eunetbrk) {
    this.eunetbrk = eunetbrk;
  }

  @Column(name = "IMCOMISN", precision = 18, scale = 8)
  public BigDecimal getImcomisn() {
    return this.imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  @Column(name = "IMCOMIEU", precision = 18, scale = 8)
  public BigDecimal getImcomieu() {
    return this.imcomieu;
  }

  public void setImcomieu(BigDecimal imcomieu) {
    this.imcomieu = imcomieu;
  }

  @Column(name = "PCCOMISN", precision = 16, scale = 6)
  public BigDecimal getPccomisn() {
    return this.pccomisn;
  }

  public void setPccomisn(BigDecimal pccomisn) {
    this.pccomisn = pccomisn;
  }

  @Column(name = "IMCOMDEV", precision = 18, scale = 8)
  public BigDecimal getImcomdev() {
    return this.imcomdev;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  @Column(name = "EUCOMDEV", precision = 18, scale = 8)
  public BigDecimal getEucomdev() {
    return this.eucomdev;
  }

  public void setEucomdev(BigDecimal eucomdev) {
    this.eucomdev = eucomdev;
  }

  @Column(name = "IMFIBRDV", precision = 18, scale = 8)
  public BigDecimal getImfibrdv() {
    return this.imfibrdv;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  @Column(name = "IMFIBREU", precision = 18, scale = 8)
  public BigDecimal getImfibreu() {
    return this.imfibreu;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  @Column(name = "IMSVB", precision = 18, scale = 8)
  public BigDecimal getImsvb() {
    return this.imsvb;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  @Column(name = "IMSVBEU", precision = 18, scale = 8)
  public BigDecimal getImsvbeu() {
    return this.imsvbeu;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  @Column(name = "IMTPCAMB", precision = 18, scale = 8)
  public BigDecimal getImtpcamb() {
    return this.imtpcamb;
  }

  public void setImtpcamb(BigDecimal imtpcamb) {
    this.imtpcamb = imtpcamb;
  }

  @Column(name = "CDMONISO", length = 3)
  public String getCdmoniso() {
    return this.cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  @Column(name = "CDINDGAS", length = 1)
  public Character getCdindgas() {
    return this.cdindgas;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }

  @Column(name = "CDINDIMP", length = 1)
  public Character getCdindimp() {
    return this.cdindimp;
  }

  public void setCdindimp(Character cdindimp) {
    this.cdindimp = cdindimp;
  }

  @Column(name = "CDRESIDE", length = 1)
  public Character getCdreside() {
    return this.cdreside;
  }

  public void setCdreside(Character cdreside) {
    this.cdreside = cdreside;
  }

  @Column(name = "IMBRMERC", precision = 18, scale = 8)
  public BigDecimal getImbrmerc() {
    return this.imbrmerc;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  @Column(name = "IMBRMREU", precision = 18, scale = 8)
  public BigDecimal getImbrmreu() {
    return this.imbrmreu;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  @Column(name = "IMGASTOS", precision = 18, scale = 8)
  public BigDecimal getImgastos() {
    return this.imgastos;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  @Column(name = "IMGATDVS", precision = 18, scale = 8)
  public BigDecimal getImgatdvs() {
    return this.imgatdvs;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  @Column(name = "IMIMPDVS", precision = 18, scale = 8)
  public BigDecimal getImimpdvs() {
    return this.imimpdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  @Column(name = "IMIMPEUR", precision = 18, scale = 8)
  public BigDecimal getImimpeur() {
    return this.imimpeur;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  @Column(name = "IMCCONEU", precision = 18, scale = 8)
  public BigDecimal getImcconeu() {
    return this.imcconeu;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  @Column(name = "IMCLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcliqeu() {
    return this.imcliqeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  @Column(name = "IMDEREEU", precision = 18, scale = 8)
  public BigDecimal getImdereeu() {
    return this.imdereeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  @Column(name = "XTDCM", length = 1)
  public Character getXtdcm() {
    return this.xtdcm;
  }

  public void setXtdcm(Character xtdcm) {
    this.xtdcm = xtdcm;
  }

  @Column(name = "XTDERECH", length = 1)
  public Character getXtderech() {
    return this.xtderech;
  }

  public void setXtderech(Character xtderech) {
    this.xtderech = xtderech;
  }

  @Column(name = "RFPARTEN", length = 16)
  public String getRfparten() {
    return this.rfparten;
  }

  public void setRfparten(String rfparten) {
    this.rfparten = rfparten;
  }

  @Column(name = "TPCALCUL", length = 1)
  public Character getTpcalcul() {
    return this.tpcalcul;
  }

  public void setTpcalcul(Character tpcalcul) {
    this.tpcalcul = tpcalcul;
  }

  @Column(name = "INDATFIS", length = 1)
  public Character getIndatfis() {
    return this.indatfis;
  }

  public void setIndatfis(Character indatfis) {
    this.indatfis = indatfis;
  }

  @Column(name = "ERDATFIS", nullable = false, length = 40)
  public String getErdatfis() {
    return this.erdatfis;
  }

  public void setErdatfis(String erdatfis) {
    this.erdatfis = erdatfis;
  }

  @Column(name = "CDDATLIQ", length = 1)
  public Character getCddatliq() {
    return this.cddatliq;
  }

  public void setCddatliq(Character cddatliq) {
    this.cddatliq = cddatliq;
  }

  @Column(name = "CDBLOQUE", length = 1)
  public Character getCdbloque() {
    return this.cdbloque;
  }

  public void setCdbloque(Character cdbloque) {
    this.cdbloque = cdbloque;
  }

  @Column(name = "NUOPROUT", precision = 9)
  public Integer getNuoprout() {
    return this.nuoprout;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  @Column(name = "NUPAREJE", precision = 4)
  public Short getNupareje() {
    return this.nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  @Column(name = "DSOBSERV", length = 200)
  public String getDsobserv() {
    return this.dsobserv;
  }

  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }

  @Column(name = "CDUSUBLO", length = 10)
  public String getCdusublo() {
    return this.cdusublo;
  }

  public void setCdusublo(String cdusublo) {
    this.cdusublo = cdusublo;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION", precision = 4)
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "NUAGRPKB", precision = 9)
  public Integer getNuagrpkb() {
    return this.nuagrpkb;
  }

  public void setNuagrpkb(Integer nuagrpkb) {
    this.nuagrpkb = nuagrpkb;
  }

  @Column(name = "PKENTITY", length = 16)
  public String getPkentity() {
    return this.pkentity;
  }

  public void setPkentity(String pkentity) {
    this.pkentity = pkentity;
  }

  @Column(name = "BICCUS", length = 11)
  public String getBiccus() {
    return this.biccus;
  }

  public void setBiccus(String biccus) {
    this.biccus = biccus;
  }

  @Column(name = "NAMECUS", length = 146)
  public String getNamecus() {
    return this.namecus;
  }

  public void setNamecus(String namecus) {
    this.namecus = namecus;
  }

  @Column(name = "LCUSBIC", length = 11)
  public String getLcusbic() {
    return this.lcusbic;
  }

  public void setLcusbic(String lcusbic) {
    this.lcusbic = lcusbic;
  }

  @Column(name = "CDLOCCUS", length = 146)
  public String getCdloccus() {
    return this.cdloccus;
  }

  public void setCdloccus(String cdloccus) {
    this.cdloccus = cdloccus;
  }

  @Column(name = "GCUSBIC", length = 11)
  public String getGcusbic() {
    return this.gcusbic;
  }

  public void setGcusbic(String gcusbic) {
    this.gcusbic = gcusbic;
  }

  @Column(name = "CDGLOCUS", length = 146)
  public String getCdglocus() {
    return this.cdglocus;
  }

  public void setCdglocus(String cdglocus) {
    this.cdglocus = cdglocus;
  }

  @Column(name = "BCUSBIC", length = 11)
  public String getBcusbic() {
    return this.bcusbic;
  }

  public void setBcusbic(String bcusbic) {
    this.bcusbic = bcusbic;
  }

  @Column(name = "CDNBCUS", length = 146)
  public String getCdnbcus() {
    return this.cdnbcus;
  }

  public void setCdnbcus(String cdnbcus) {
    this.cdnbcus = cdnbcus;
  }

  @Column(name = "BICCLE", length = 11)
  public String getBiccle() {
    return this.biccle;
  }

  public void setBiccle(String biccle) {
    this.biccle = biccle;
  }

  @Column(name = "NAMECLE", length = 146)
  public String getNamecle() {
    return this.namecle;
  }

  public void setNamecle(String namecle) {
    this.namecle = namecle;
  }

  @Column(name = "LCLEBIC", length = 11)
  public String getLclebic() {
    return this.lclebic;
  }

  public void setLclebic(String lclebic) {
    this.lclebic = lclebic;
  }

  @Column(name = "CDLOCCLE", length = 146)
  public String getCdloccle() {
    return this.cdloccle;
  }

  public void setCdloccle(String cdloccle) {
    this.cdloccle = cdloccle;
  }

  @Column(name = "GCLEBIC", length = 11)
  public String getGclebic() {
    return this.gclebic;
  }

  public void setGclebic(String gclebic) {
    this.gclebic = gclebic;
  }

  @Column(name = "CDGLOCLE", length = 146)
  public String getCdglocle() {
    return this.cdglocle;
  }

  public void setCdglocle(String cdglocle) {
    this.cdglocle = cdglocle;
  }

  @Column(name = "BCLEBIC", length = 11)
  public String getBclebic() {
    return this.bclebic;
  }

  public void setBclebic(String bclebic) {
    this.bclebic = bclebic;
  }

  @Column(name = "CDNBCLE", length = 146)
  public String getCdnbcle() {
    return this.cdnbcle;
  }

  public void setCdnbcle(String cdnbcle) {
    this.cdnbcle = cdnbcle;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALORR", length = 4)
  public Date getFevalorr() {
    return this.fevalorr;
  }

  public void setFevalorr(Date fevalorr) {
    this.fevalorr = fevalorr;
  }

  @Column(name = "IMLIQ", precision = 18, scale = 8)
  public BigDecimal getImliq() {
    return this.imliq;
  }

  public void setImliq(BigDecimal imliq) {
    this.imliq = imliq;
  }

  @Column(name = "IMEURLIQ", precision = 18, scale = 8)
  public BigDecimal getImeurliq() {
    return this.imeurliq;
  }

  public void setImeurliq(BigDecimal imeurliq) {
    this.imeurliq = imeurliq;
  }

  @Column(name = "IMAJLIQ", precision = 18, scale = 8)
  public BigDecimal getImajliq() {
    return this.imajliq;
  }

  public void setImajliq(BigDecimal imajliq) {
    this.imajliq = imajliq;
  }

  @Column(name = "IMAJELIQ", precision = 18, scale = 8)
  public BigDecimal getImajeliq() {
    return this.imajeliq;
  }

  public void setImajeliq(BigDecimal imajeliq) {
    this.imajeliq = imajeliq;
  }

  @Column(name = "IMGASLIQDV", precision = 18, scale = 8)
  public BigDecimal getImgasliqdv() {
    return this.imgasliqdv;
  }

  public void setImgasliqdv(BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  @Column(name = "IMGASLIQEU", precision = 18, scale = 8)
  public BigDecimal getImgasliqeu() {
    return this.imgasliqeu;
  }

  public void setImgasliqeu(BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  @Column(name = "IMGASGESDV", precision = 18, scale = 8)
  public BigDecimal getImgasgesdv() {
    return this.imgasgesdv;
  }

  public void setImgasgesdv(BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  @Column(name = "IMGASGESEU", precision = 18, scale = 8)
  public BigDecimal getImgasgeseu() {
    return this.imgasgeseu;
  }

  public void setImgasgeseu(BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  @Column(name = "IMGASTOTDV", precision = 18, scale = 8)
  public BigDecimal getImgastotdv() {
    return this.imgastotdv;
  }

  public void setImgastotdv(BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  @Column(name = "IMGASTOTEU", precision = 18, scale = 8)
  public BigDecimal getImgastoteu() {
    return this.imgastoteu;
  }

  public void setImgastoteu(BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  @Column(name = "REFBANIF", length = 16)
  public String getRefbanif() {
    return this.refbanif;
  }

  public void setRefbanif(String refbanif) {
    this.refbanif = refbanif;
  }

  @Column(name = "REFCLTDES", length = 20)
  public String getRefcltdes() {
    return this.refcltdes;
  }

  public void setRefcltdes(String refcltdes) {
    this.refcltdes = refcltdes;
  }

  @Column(name = "CDHOLDER", length = 20)
  public String getCdholder() {
    return this.cdholder;
  }

  public void setCdholder(String cdholder) {
    this.cdholder = cdholder;
  }

  @Column(name = "CDESTADOTIT", precision = 4)
  public Integer getCdestadotit() {
    return this.cdestadotit;
  }

  public void setCdestadotit(Integer cdestadotit) {
    this.cdestadotit = cdestadotit;
  }

  @Column(name = "REFTITULAR", length = 20)
  public String getReftitular() {
    return this.reftitular;
  }

  public void setReftitular(String reftitular) {
    this.reftitular = reftitular;
  }

  @Column(name = "IMCORRETAJESVFINALDV", precision = 18, scale = 8)
  public BigDecimal getImcorretajesvfinaldv() {
    return this.imcorretajesvfinaldv;
  }

  public void setImcorretajesvfinaldv(BigDecimal imcorretajesvfinaldv) {
    this.imcorretajesvfinaldv = imcorretajesvfinaldv;
  }

  @Column(name = "IMCOMCLTEEU", precision = 18, scale = 8)
  public BigDecimal getImcomclteeu() {
    return this.imcomclteeu;
  }

  public void setImcomclteeu(BigDecimal imcomclteeu) {
    this.imcomclteeu = imcomclteeu;
  }

  @Column(name = "CANONCONTRPAGOCLTE", precision = 4)
  public Integer getCanoncontrpagoclte() {
    return this.canoncontrpagoclte;
  }

  public void setCanoncontrpagoclte(Integer canoncontrpagoclte) {
    this.canoncontrpagoclte = canoncontrpagoclte;
  }

  @Column(name = "CANONCONTRPAGOMKT", precision = 4)
  public Integer getCanoncontrpagomkt() {
    return this.canoncontrpagomkt;
  }

  public void setCanoncontrpagomkt(Integer canoncontrpagomkt) {
    this.canoncontrpagomkt = canoncontrpagomkt;
  }

  @Column(name = "CANONLIQPAGOCLTE", precision = 4)
  public Integer getCanonliqpagoclte() {
    return this.canonliqpagoclte;
  }

  public void setCanonliqpagoclte(Integer canonliqpagoclte) {
    this.canonliqpagoclte = canonliqpagoclte;
  }

  @Column(name = "CANONLIQPAGOMKT", precision = 4)
  public Integer getCanonliqpagomkt() {
    return this.canonliqpagomkt;
  }

  public void setCanonliqpagomkt(Integer canonliqpagomkt) {
    this.canonliqpagomkt = canonliqpagomkt;
  }

  @Column(name = "CANONCOMPPAGOCLTE", precision = 4)
  public Integer getCanoncomppagoclte() {
    return this.canoncomppagoclte;
  }

  public void setCanoncomppagoclte(Integer canoncomppagoclte) {
    this.canoncomppagoclte = canoncomppagoclte;
  }

  @Column(name = "CANONCOMPPAGOMKT", precision = 4)
  public Integer getCanoncomppagomkt() {
    return this.canoncomppagomkt;
  }

  public void setCanoncomppagomkt(Integer canoncomppagomkt) {
    this.canoncomppagomkt = canoncomppagomkt;
  }

  @Column(name = "IMCANONCOMPDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncompdv() {
    return this.imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  @Column(name = "IMCANONCOMPEU", precision = 18, scale = 8)
  public BigDecimal getImcanoncompeu() {
    return this.imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  @Column(name = "IMCANONCONTRDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncontrdv() {
    return this.imcanoncontrdv;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  @Column(name = "IMCANONCONTREU", precision = 18, scale = 8)
  public BigDecimal getImcanoncontreu() {
    return this.imcanoncontreu;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  @Column(name = "IMCANONLIQDV", precision = 18, scale = 8)
  public BigDecimal getImcanonliqdv() {
    return this.imcanonliqdv;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  @Column(name = "IMCANONLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcanonliqeu() {
    return this.imcanonliqeu;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  // @OneToMany( cascade = { CascadeType.PERSIST }, fetch = FetchType.LAZY,
  // mappedBy = "tmct0alo")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0alo")
  public List<Tmct0alc> getTmct0alcs() {
    return this.tmct0alcs;
  }

  public void setTmct0alcs(List<Tmct0alc> tmct0alcs) {
    this.tmct0alcs = tmct0alcs;
  }

  @Transactional
  public boolean addTmct0alcs(final Tmct0alc alc) {
    if (CollectionUtils.isEmpty(tmct0alcs)) {
      this.tmct0alcs = new ArrayList<Tmct0alc>();
    }
    alc.setTmct0alo(this);

    return tmct0alcs.add(alc);

  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0alo")
  // public List<Tmct0afi> getTmct0afis() {
  // return this.tmct0afis;
  // }
  //
  // public void setTmct0afis(List<Tmct0afi> tmct0afis) {
  // this.tmct0afis = tmct0afis;
  // }
  //
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0alo")
  public List<Tmct0grupoejecucion> getTmct0grupoejecucions() {
    return this.tmct0grupoejecucions;
  }

  public void setTmct0grupoejecucions(List<Tmct0grupoejecucion> tmct0grupoejecucions) {
    this.tmct0grupoejecucions = tmct0grupoejecucions;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0alo", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
  public List<Tmct0aloDynamicValues> getDynamicValues() {
    return dynamicValues;
  }

  public void setDynamicValues(List<Tmct0aloDynamicValues> dynamicValues) {
    this.dynamicValues = dynamicValues;
  }

  @Column(name = "NUTITFALLIDOS", precision = 18, scale = 6)
  public BigDecimal getNutitfallidos() {
    return this.nutitfallidos;
  }

  public void setNutitfallidos(BigDecimal nutitfallidos) {
    this.nutitfallidos = nutitfallidos;
  }

  @Column(name = "CCV", length = 40, nullable = true)
  public String getCcv() {
    return ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  @Column(name = "REF_CLIENTE", length = 32, nullable = true)
  public String getRefCliente() {
    return refCliente;
  }

  public void setRefCliente(String refCliente) {
    this.refCliente = refCliente;
  }

  public static Tmct0alo clone(final Tmct0alo origen, List<String> notSetThisFieldNames) {
    try {
      if (notSetThisFieldNames == null) {
        notSetThisFieldNames = new ArrayList<String>();
      }// if (notSetThisFieldNames == null)
      Tmct0alo destino = (Tmct0alo) origen.getClass().newInstance();
      Field[] fields = destino.getClass().getDeclaredFields();
      // Method[] metodos = origen.getClass().getMethods();
      if (fields != null && fields.length > 0) {
        for (int i = 0; i < fields.length; i++) {
          Field field = fields[i];
          String fieldName = field.getName();
          if (notSetThisFieldNames.contains(fieldName)) {
            continue;
          }// if ( notSetThisFieldNames.contains( fieldName ) ) {
          try {
            String fieldNameForMethod = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            Method set = destino.getClass().getMethod("set" + fieldNameForMethod, field.getType());
            Method get = destino.getClass().getMethod("get" + fieldNameForMethod);
            if (set != null && get != null) {
              Object value = get.invoke(origen);
              if (value != null) {
                if (value instanceof Timestamp) {
                  value = new Timestamp(((Timestamp) value).getTime());
                }
                else if (value instanceof Time) {
                  value = new Time(((Time) value).getTime());
                }
                else if (value instanceof java.sql.Date) {
                  value = new java.sql.Date(((java.sql.Date) value).getTime());
                }
                else if (value instanceof Date) {
                  value = new Date(((Date) value).getTime());
                }
                else if (value instanceof Collection) {
                  // value.
                }
                try {
                  set.invoke(destino, value);
                }
                catch (Exception e) {

                }

              }// if ( value != null ){
            }// if ( set != null && get != null )

          }
          catch (NoSuchMethodException e) {

          }
        }// for (int i=0;i<fields.length;i++)
      }// if (fields != null && fields.length >0)
      return destino;
    }
    catch (IllegalArgumentException e) {

    }
    catch (SecurityException e) {

    }
    catch (InstantiationException e) {

    }
    catch (IllegalAccessException e) {

    }
    catch (InvocationTargetException e) {

    }
    return null;
  }// public static Tmct0alo clone( Tmct0alo origen, List< String >
   // notSetThisFieldNames ) {

}
