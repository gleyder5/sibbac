package sibbac.business.wrappers.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table( name = "TMCT0_MODELO_DE_NEGOCIO" )
public class Tmct0ModeloDeNegocio implements java.io.Serializable  {
	
	private static final long serialVersionUID = 937837479469169520L;
	private Long id_modelo_de_negocio;
	private String audit_user;
	private String nbDescripcion;
	private String cd_codigo;
	private Date audit_fecha_cambio;
	
	
	public Tmct0ModeloDeNegocio() {}


	public Tmct0ModeloDeNegocio(Long id_modelo_de_negocio, String audit_user,
			String nbDescripcion, String cd_codigo, Date audit_fecha_cambio) {
		super();
		this.id_modelo_de_negocio = id_modelo_de_negocio;
		this.audit_user = audit_user;
		this.nbDescripcion = nbDescripcion;
		this.cd_codigo = cd_codigo;
		this.audit_fecha_cambio = audit_fecha_cambio;
	}

	@Id
	@Column( name = "ID_MODELO_DE_NEGOCIO", unique = true, nullable = false, length = 8 )
	public Long getId_modelo_de_negocio() {
		return id_modelo_de_negocio;
	}


	public void setId_modelo_de_negocio(Long id_modelo_de_negocio) {
		this.id_modelo_de_negocio = id_modelo_de_negocio;
	}

	@Column( name = "AUDIT_USER", length = 20 )
	public String getAudit_user() {
		return audit_user;
	}


	public void setAudit_user(String audit_user) {
		this.audit_user = audit_user;
	}

	@Column( name = "NB_DESCRIPCION", length = 255 )
	public String getNbDescripcion() {
		return nbDescripcion;
	}


	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}

	@Column( name = "CD_CODIGO", nullable = false, length = 16 )
	public String getCd_codigo() {
		return cd_codigo;
	}


	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}

	@Temporal( TemporalType.TIMESTAMP)
	@Column( name = "AUDIT_FECHA_CAMBIO", length = 10 )
	public Date getAudit_fecha_cambio() {
		return audit_fecha_cambio;
	}


	public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
		this.audit_fecha_cambio = audit_fecha_cambio;
	}
	
	
	
	
	

}
