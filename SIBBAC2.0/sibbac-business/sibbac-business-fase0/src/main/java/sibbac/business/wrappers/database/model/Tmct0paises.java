package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;

@Entity
@Table( name = DBConstants.WRAPPERS.PAISES )
@Audited
public class Tmct0paises  implements java.io.Serializable {
	
	/**
	 * Número de serie 
	 */
	private static final long serialVersionUID = 8970673967653451946L;
	
  @EmbeddedId
	private Tmct0paisesId id;
	
	@Column( name = "NBPAIS", length = 100, nullable = false )
	private String				nbpais;
	
	@Column( name = "CDHACIENDA", length = 6, nullable = true )
	private String				cdhacienda;
	
	@Column( name = "CDCODKGR", length = 6, nullable = true )
	private String				cdcodkgr;
	
	@Column( name = "CD_TIPO_ID_MIFID", length = 7, nullable = true )
	private String cdTipoIdMifid;

	public Tmct0paises() {
	}

	public Tmct0paises(Tmct0paisesId id, String nbpais, String cdhacienda,
			String cdcodkgr) {
		super();
		this.id = id;
		this.nbpais = nbpais;
		this.cdhacienda = cdhacienda;
		this.cdcodkgr = cdcodkgr;
	}

	public Tmct0paisesId getId() {
		return id;
	}

	public void setId(Tmct0paisesId id) {
		this.id = id;
	}

	public String getNbpais() {
		return nbpais;
	}

	public void setNbpais(String nbpais) {
		this.nbpais = nbpais;
	}

	
	public String getCdhacienda() {
		return cdhacienda;
	}

	public void setCdhacienda(String cdhacienda) {
		this.cdhacienda = cdhacienda;
	}

	public String getCdcodkgr() {
		return cdcodkgr;
	}

	public void setCdcodkgr(String cdcodkgr) {
		this.cdcodkgr = cdcodkgr;
	}

  public String getCdTipoIdMifid() {
    return cdTipoIdMifid;
  }

  public void setCdTipoIdMifid(String cdTipoIdMifid) {
    this.cdTipoIdMifid = cdTipoIdMifid;
  }

}
