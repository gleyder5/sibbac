package sibbac.business.canones.model;

import java.util.List;
import java.util.ArrayList;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "TMCT0_LISTA")
public class Lista extends DescriptedCanonEntity<BigInteger> {

  /**
   * Número de serie;
   */
  private static final long serialVersionUID = -2048546408014576778L;
  
  @Id
  @Column(name = ID_COLUMN_NAME)
  private BigInteger id;
  
  @OneToOne
  @JoinColumn(name = "ID_TIPO_LISTA", referencedColumnName = ID_COLUMN_NAME)
  private ListaTipo tipo;
  
  @OneToMany
  @JoinColumn(name = "ID_LISTA", referencedColumnName = ID_COLUMN_NAME)
  private List<ListaValores> valores;
  
  public Lista() {
    valores = new ArrayList<>();
  }

  @Override
  public BigInteger getId() {
    return id;
  }

  @Override
  public void setId(BigInteger id) {
    this.id = id;
  }

  public ListaTipo getTipo() {
    return tipo;
  }

  public void setTipo(ListaTipo tipo) {
    this.tipo = tipo;
  }

  public List<ListaValores> getValores() {
    return valores;
  }

  public void setValores(List<ListaValores> valores) {
    this.valores = valores;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    Lista other = (Lista) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (tipo == null) {
      if (other.tipo != null)
        return false;
    } else if (!tipo.equals(other.tipo))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return toString("Lista");
  }
  
}
