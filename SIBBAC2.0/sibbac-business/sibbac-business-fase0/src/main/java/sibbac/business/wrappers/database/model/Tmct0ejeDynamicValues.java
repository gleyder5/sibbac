package sibbac.business.wrappers.database.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0EJE_DYNAMIC_VALUES")
public class Tmct0ejeDynamicValues extends DynamicValuesBase {

  /**
   * 
   */
  private static final long serialVersionUID = -7746120540067947074L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nuejecuc", column = @Column(name = "NUEJECUC", nullable = false, length = 20)),
      @AttributeOverride(name = "nombre", column = @Column(name = "NOMBRE", nullable = false, length = 32)) })
  private Tmct0ejeDynamicValuesId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUEJECUC", referencedColumnName = "NUEJECUC", nullable = false, insertable = false, updatable = false) })
  private Tmct0eje tmct0eje;

  public Tmct0ejeDynamicValuesId getId() {
    return id;
  }

  public void setId(Tmct0ejeDynamicValuesId id) {
    this.id = id;
  }

  public Tmct0eje getTmct0eje() {
    return tmct0eje;
  }

  public void setTmct0eje(Tmct0eje tmct0eje) {
    this.tmct0eje = tmct0eje;
  }

}
