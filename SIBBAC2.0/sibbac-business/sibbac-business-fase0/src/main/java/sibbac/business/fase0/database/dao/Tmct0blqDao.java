package sibbac.business.fase0.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0blq;
import sibbac.business.fase0.database.model.Tmct0blqId;

@Repository
public interface Tmct0blqDao extends JpaRepository<Tmct0blq, Tmct0blqId> {

  /**
   * list = (List<Tmct0blq>) JdoDBUtils.getListJdo(hpm, Tmct0blq.class, paramMap,
   * "cdbroker == _cdbroker && centro == _centro  && cdmercad == _cdmercad && tpsettle == _tpsettle && fhinicio <= _fetrade && fhfinal >= _fetrade"
   * , "cdbroker asc, cdmercad asc, tpsettle asc, fhfinal desc, numsec desc");
   */

  @Query("SELECT q FROM Tmct0blq q "
         + " WHERE q.id.cdbroker = :pcdbroker AND q.id.cdmercad = :pcdmercad"
         + " AND q.id.tpsettle = :ptpsettle AND q.fhfinal >= :fecha AND q.fhinicio <= :fecha "
         + " ORDER BY q.id.cdbroker ASC, q.id.cdmercad ASC, q.id.tpsettle ASC, q.fhfinal DESC, q.id.numsec DESC")
  List<Tmct0blq> findAllTmct0blqByCdbrokerAndCdmercadAndTpsettleAndFecha(@Param("pcdbroker") String cdbroker,
                                                                         @Param("pcdmercad") String cdmercad,
                                                                         @Param("ptpsettle") String tpsettle,
                                                                         @Param("fecha") BigDecimal fecha);

  @Query("SELECT q FROM Tmct0blq q "
         + " WHERE q.id.cdbroker = :pcdbroker AND q.id.centro = :pcentro AND q.id.cdmercad = :pcdmercad"
         + " AND q.id.tpsettle = :ptpsettle AND q.fhfinal >= :fecha AND q.fhinicio <= :fecha "
         + " ORDER BY q.id.cdbroker ASC, q.id.cdmercad ASC, q.id.tpsettle ASC, q.fhfinal DESC, q.id.numsec DESC")
  List<Tmct0blq> findAllTmct0blqByCdbrokerAndCentroAndCdmercadAndTpsettleAndFecha(@Param("pcdbroker") String cdbroker,
                                                                                  @Param("pcentro") String centro,
                                                                                  @Param("pcdmercad") String cdmercad,
                                                                                  @Param("ptpsettle") String tpsettle,
                                                                                  @Param("fecha") BigDecimal fecha);

}
