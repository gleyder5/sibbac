/**
 * Modelo para el cálculo de Canon.
 * <p>
 * La entidad que cumple la función de punto de entrada es {@link MercadoContratacion}, a partir
 * de la cual se pueden recuperar el resto de las entidades.
 * </p>
 * <p>
 * Se definen dos superclases de utilidad: {@link CanonEntity} y {@link DescriptedCanonEntity},
 * las cuales representan los campos comunes de las entidades.
 * </p>
 */
package sibbac.business.canones.model;