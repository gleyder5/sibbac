package sibbac.business.fase0.common;

import sibbac.business.fase0.database.dto.XasDTO;

public class DTOUtils {
	
	public static XasDTO convertObjectToXasDTO( Object[] valuesFromQuery ) {
		String sCodigo = (( String ) valuesFromQuery[ 1 ]).trim();
		String sDescripcion = (( String ) valuesFromQuery[ 0 ]).trim();
		return new XasDTO(sCodigo,sDescripcion);
	}

}
