package sibbac.business.fase0.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0brk;

@Repository
public interface Tmct0brkDao extends JpaRepository<Tmct0brk, Long> {

  @Query("select t from Tmct0brk t where cdbroker = ?1")
  Tmct0brk findByCdbroker(final String cdbroker);

  @Query("select t from Tmct0brk t where cdbroker = ?1 and t.fhfinal >= ?2 and t.fhinicio <= ?2 ")
  Tmct0brk findByCdbrokerAndFechaContratacion(final String cdbroker, final Integer fecha);

  @Query("select t.cocenliq from Tmct0brk t where cdbroker = ?1")
  String findCocenliqByCdbroker(final String cdbroker);

}
