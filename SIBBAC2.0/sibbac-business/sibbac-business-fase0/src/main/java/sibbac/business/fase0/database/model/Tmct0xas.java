package sibbac.business.fase0.database.model;


import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table( name = "TMCT0XAS" )
public class Tmct0xas implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7635069729850203190L;
	private Tmct0xasId			id;
	private String				inforas4			= " ";
	private String				cdusuaud			= " ";
	private Date				fhaudit				= new Date();
	private Integer				nuversion			= 0;

	public Tmct0xas() {
	}

	public Tmct0xas( Tmct0xasId id ) {
		this.id = id;
	}

	public Tmct0xas( Tmct0xasId id, String inforas4, String cdusuaud, Date fhaudit, Integer nuversion ) {
		this.id = id;
		this.inforas4 = inforas4;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "nbcamxml", column = @Column( name = "NBCAMXML", nullable = false, length = 30 ) ),
			@AttributeOverride( name = "inforxml", column = @Column( name = "INFORXML", nullable = false, length = 30 ) ),
			@AttributeOverride( name = "nbcamas4", column = @Column( name = "NBCAMAS4", nullable = false, length = 30 ) )
	} )
	public Tmct0xasId getId() {
		return this.id;
	}

	public void setId( Tmct0xasId id ) {
		this.id = id;
	}

	@Column( name = "INFORAS4", length = 30 )
	public String getInforas4() {
		return this.inforas4;
	}

	public void setInforas4( String inforas4 ) {
		this.inforas4 = inforas4;
	}

	@Column( name = "CDUSUAUD", length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "NUVERSION" )
	public Integer getNuversion() {
		return this.nuversion;
	}

	public void setNuversion( Integer nuversion ) {
		this.nuversion = nuversion;
	}

}
