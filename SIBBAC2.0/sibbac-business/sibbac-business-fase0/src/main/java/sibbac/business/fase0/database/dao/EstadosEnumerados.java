package sibbac.business.fase0.database.dao;

/**
 * Interface with all constants of the project.
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
public interface EstadosEnumerados {

  public enum ASIGNACIONES {
    /**/
    RECHAZADA(0),
    /**/
    ERROR_DATOS_ASIGNACION(1),
    /**/
    ERROR_DATOS_ANULACION(2),
    /**/
    RECHAZADO_COMPENSADOR(3),
    /**/
    INCIDENCIAS(4),
    /**/
    TRATANDOSE(5),
    /**/
    EN_CURSO(6),
    /**/
    PDTE_USUARIO_SIN_TITULAR(7),
    /**/
    PDTE_ENVIAR_SIN_TITULAR(8),
    /**/
    ENVIANDOSE_SIN_TITULAR(9),
    /**/
    PDTE_DESGLOSAR(10),
    /**/
    PDTE_OPERATIVA_ESPECIAL(11),
    /**/
    PDTE_OPERATIVA_ESPECIAL_TRATANDOSE(12),
    
    /**/
    DISOCIADA_PDTE_DESGLOSAR(14),
    /**/
    PDTE_DESGLOSE_MANUAL(15),
    /**/
    SIN_INSTRUCCIONES(20),
    /**/
    PDTE_REVISAR_USUARIO(25),
    
    /**/
    PDTE_ACTUALIZAR_CODIGOS_OPERACION(28),
    
   
    /**/
    PDTE_ENVIAR(30),
    /**/
    PDTE_ENVIAR_TRASPASO(32),
    /**/
    PDTE_CANCELAR(35),
    /**/
    ENVIANDOSE(40),
    /**/
    ENVIANDOSE_S3(45),
    /**/
    ENVIANDOSE_TRASPASO(47),
    /**/
    ENVIANDOSE_ANULACION(50),
    
    /**/
    ACTUALIZADOS_CODIGOS_OPERACION(52),
    /**/
    PDTE_ACEPTAR_COMPENSADOR(55),
    /**/
    PDTE_ACEPTAR_S3(60),
    /**/
    RECIBIENDO_ASIGNACIONES(62),
    
    /**/
    PDTE_REVISAR_USUARIO_ASIGNACIONES_DISTINTAS(63),
    
    PDTE_LIQUIDAR_ESPERANDO_OTRAS_CAMARAS(64),
    
    /**/
    PDTE_LIQUIDAR(65),
    /**/
    IMPUTADA_S3(70),
    /**/
    IMPUTADA_PROPIA(75),
    /**/
    LIQUIDADA_TEORICA(80),
    /**/
    LIQUIDADA(85),
    /**/
    LIQUIDADA_PROPIA(90),
    /**/
    LIQUIDADA_DEFINITIVA(95);
    
    

    private int id;

    private ASIGNACIONES(int id) {
      this.id = id;

    }

    public int getId() {
      return id;
    }

  }

  public enum TITULARIDADES {
    /**/
    RECHAZADA(200),
    /**/
    ERROR_DATOS_TITULARIDAD(201),
    /**/
    ERROR_DATOS_RECTIFICACION(202),
    /**/
    INCIDENCIAS(203),
    /**/
    TRATANDOSE(204),
    /**/
    EN_CURSO(205),
    /**/
    SIN_TITULARES(210),
    /**/
    PDTE_ENVIAR(215),
    /**/
    PDTE_ENVIAR_MODIFICACION(220),
    /**/
    PDTE_ENVIAR_RECTIFICACION(225),
    /**/
    ENVIANDOSE(230),
    /**/
    ENVIANDOSE_RECTIFICACION(235),
    /**/
    RECIBIENDO_TITULARES(237),
    /**/
    TITULARIDAD_ACEPTADA(240),
    /**/
    RECTIFICACION_ACEPTADA(245),
    /**/
    PDTE_IF(250),
    /**/
    PROVISIONAL_IF(252),
    /**/
    TRATANDOSE_IF(253),
    /**/
    CIERRE_IF(255),
    /**/
    TITULARIDAD_DEFINITIVA(260);

    private int id;

    private TITULARIDADES(int id) {
      this.id = id;

    }

    public int getId() {
      return id;
    }

  }

  public enum CONTABILIDAD {
    RECHAZADA(300),
    TRATANDOSE(301),
    EN_CURSO(302),
    INCIDENCIAS(303),
    CONTABILIZANDOSE(304),
    ANULADA(305),
    CONTABILIDAD_RETROCEDIDA(307),
    PDTE_DEVENGO(310),
    PDTE_AJUSTE(312),
    DEVENGADA_PDTE_GENERAR_FACTURA(315),
    DEVENGADA_PDTE_CONCILIACION(316),
    DEVENGADA_PDTE_COBRO(320),
    DEVENGADA_PDTE_COBRO_BME(323),
    FACTURADA_PDTE_COBRO(325),
    PARCIALMENTE_COBRADA(329),
    COBRADA(330),
    PARCIALMENTE_CONCILIADA(333),
    CONCILIADA(335);

    private int id;

    private CONTABILIDAD(int id) {
      this.id = id;

    }

    public int getId() {
      return id;
    }

  }

  public enum TIPO_ESTADO {
    CORRETAJE_O_CANON(10),
    COMISION_DEVOLUCION(30),
    COMISION_ORDENANTE(40),
    EFECTIVO(50),
    CANON(60),
    CORRETAJE(70);

    private Integer id;

    private TIPO_ESTADO(Integer id) {
      this.id = id;

    }

    public Integer getId() {
      return id;
    }

    // /**
    // * Utility method for creating enumeration from the db represented value
    // *
    // * @param value
    // * @return
    // */
    // public static TIPO_ESTADO recreateEnum(Integer value) {
    // TIPO_ESTADO enumVal = null;
    //
    // if (value != null) {
    // if (value == 10)
    // enumVal = TIPO_ESTADO.CORRETAJE_O_CANON;
    // else if (value == 30)
    // enumVal = TIPO_ESTADO.COMISION_DEVOLUCION;
    // else if (value == 40)
    // enumVal = TIPO_ESTADO.COMISION_ORDENANTE;
    // else if (value == 50)
    // enumVal = TIPO_ESTADO.EFECTIVO;
    // else if (value == 60)
    // enumVal = TIPO_ESTADO.CANON;
    // else if (value == 70)
    // enumVal = TIPO_ESTADO.CORRETAJE;
    // }
    //
    // return enumVal;
    // }
    //
    // /**
    // * Utility method for inserting string value in db from enumeration
    // *
    // * @return
    // */
    // public String recreateString() {
    // return tipo;
    // }

  }

  public enum CLEARING {
    /**/
    PDTE_FICHERO(400),
    /**/
    RECHAZADO_FICHERO(401),
    /**/
    ACEPTADA_ANULACION(402),
    /**/
    INCIDENCIA(403),
    /**/
    PDTE_ENVIAR_ANULACION_PARCIAL(404),
    /**/
    PDTE_ENVIAR_ANULACION(405),
    /**/
    ENVIADO_FICHERO_ANULACION(406),
    /**/
    ENVIADO_FICHERO(410),
    /**/
    ENVIADO_FICHERO_CUENTA_PROPIA(411),
    /**/
    ACEPTADO_FICHERO(415),
    /**/
    RECHAZADA_ANULACION(416),
    /**/
    PDTE_CASE(420),
    /**/
    NO_CASADA_FICHERO(425),
    /**/
    CASADA_PDTE_LIQUIDACION(430),
    /**/
    LIQUIDACION_FALLIDA(435),
    // /**/
    // PDTE_LIQUIDACION_PARCIAL( 440 ),
    // /**/
    // PDTE_LIQUIDACION_CLIENTE( 445 ),
    /**/
    LIQUIDADA_PARCIAL(450),
    /**/
    LIQUIDADA(455),
    /**/
    LIQUIDADA_PROPIA(460);

    private int id;

    private CLEARING(int id) {
      this.id = id;

    }

    public int getId() {
      return id;
    }

  }

  public enum FACTURA_SUGERIDA {
    /* ID TIPO ESTADO = 5 */
    /**/
    RECHAZADA(500),
    /**/
    SUGERIDA(501),
    /**/
    ACEPTADA(502),
    /**/
    FACTURADA(503),
    /**/
    PTE_INFORMACION(504);

    private Integer id;

    private FACTURA_SUGERIDA(Integer id) {
      this.id = id;

    }

    public Integer getId() {
      return id;
    }

  }

  public enum ALC_ORDENES {
    /* ID TIPO ESTADO = 6 */
    /**/
    SUGERIDO(520),
    /**/
    FACTURADO(521),
    /**/
    LIBRE(522),
    /**/
    SIN_FACTURA(525);

    private int id;

    private ALC_ORDENES(int id) {
      this.id = id;

    }

    public int getId() {
      return id;
    }

  }

  public enum PER_ESTADOS {
    /* ID TIPO ESTADO = 8 */
    /**/
    ABIERTO(580),
    /**/
    CERRADO(581);

    private Integer id;

    private PER_ESTADOS(Integer id) {
      this.id = id;

    }

    public Integer getId() {
      return id;
    }

  }

  /**
   * 
   * 
   * @author Vector ITC Group
   * @version 1.0
   * @since 1.0
   *
   */
  public enum FACTURA {
    /** Valor: 600 **/
    PTE_ENVIO(600),
    /** Valor: 601 **/
    PTE_COBRO(601),
    /** Valor: 602 **/
    COBRADO_PARCIALMENTE(602),
    /** Valor: 603 **/
    COBRADO_TOTALMENTE(603),
    /** Valor: 604 **/
    RECTIFICADA(604),
    /** Valor: 392 **/
    FACTURA_GUARDADA(392),
    /** Valor: 605 **/
    PTE_RECTIFICAR(605),
    /** Valor: 605 **/
    ACEPTADA_CON_ERRORES(606),
    
    COBRADA_330(330);

    private Integer id;

    private FACTURA(Integer id) {
      this.id = id;
    }

    public Integer getId() {
      return id;
    }

  }

  public enum FACTURA_RECTIFICATIVA {
    /** Valor: 620 **/
    PTE_ENVIAR(620),
    /** Valor: 621 **/
    ENVIADA(621);

    private Integer id;

    private FACTURA_RECTIFICATIVA(Integer id) {
      this.id = id;
    }

    public Integer getId() {
      return id;
    }
  }

  public enum H47 {
    /** Valor: 1000 **/
    PTE_ENVIAR(1000),
    /** Valor: 1010 **/
    ENVIADA(1010),
    /** Valor: 1020 **/
    ACEPTADA_BME(1020),
    /** Valor: 1030 **/
    RECHAZADA(1030),
    /** Valor: 1040 **/
    ACEPTADA_CNMV(1040);
    


    private Integer id;

    private H47(Integer id) {
      this.id = id;
    }

    public Integer getId() {
      return id;
    }
  }

  public enum FACTURA_PENDIENTE_RECORDATORIO {
    /** Valor: 700 **/
    PENDIENTE(700),
    /** Valor: 701 **/
    ENVIADO(701);

    private Integer id;

    private FACTURA_PENDIENTE_RECORDATORIO(Integer id) {
      this.id = id;
    }

    public Integer getId() {
      return id;
    }
  }

  public enum TMCT0MSC {
    /** Valor: 010 **/
    LISTO_GENERAR("010"),
    LISTO_PARA_TEST("025"),
    /** Valor: 900 **/
    EN_EJECUCION("900"),
    BLOQUEADO_USUARIO("999"),
    /** Valor: 000 **/
    EN_ERROR("000");

    private String cdmensa;

    private TMCT0MSC(String cdmensa) {
      this.cdmensa = cdmensa;
    }

    public String getCdmensa() {
      return cdmensa;
    }
  }

  public enum CONTAB_INFS_PREFIJADOS {
    GENE("GENÉRICO"),
    ROUT("ROUTING (ROUT)"),
    CDPF("CLIENTES DEVENGADOS PENDIENTES DE FACTURAR (CDPF)"),
    CFPC("CLIENTES FACTURADOS (CFPC)"),
    CLDP("CLIENTES CLEARING DEVENGADOS (CLDP)"),
    CLLM("CLIENTES CLEARING LIQUIDADOS MERCADO PERO NO LA PATA CLIENTE (CLLM)"),
    EPLO("EFECTIVOS CLIENTES PENDIENTES DE LIQUIDAR CUENTAS DE ORDEN (EPLO)"),
    DCG("DEVOLUCIÓN COMISIÓN MERCADO NACIONAL CLIENTES GRUPO"),
    DCNG("DEVOLUCIÓN COMISIÓN MERCADO NACIONAL CLIENTES NO GRUPO"),

    COLV("COLATERALES VIVOS (COLV)"),
    INTC("INTERESES COLATERALES (INTC)"),
    INDP("INTERESES DEUDORES PRESTAMO DE VALORES (INDP)"),
    INAP("INTERESES ACREEDORES PRESTAMO DE VALORES (INAP)"),
    RCPP("RECOMPRAS PASIVO (RCPP)"),
    RCPA("RECOMPRAS ACTIVO (RCPA)"),
    AECC("AJUSTES POR EVENTOS CORPORATIVOS CARGADOS"),
    AECA("AJUSTES POR EVENTOS CORPORATIVOS ABONADOS");

    private String descripcion;

    private CONTAB_INFS_PREFIJADOS(String descripcion) {
      this.descripcion = descripcion;
    }

    public String getDescripcion() {
      return descripcion;
    }
  }

  // A la hora de generar los ficheros para AS400, se generan primero los de devengos
  public enum TIPO_COMPROBANTE implements TIPO {
    DEVENGO_ROUTING(70, "Devengo y Rechazo Routing", "D", "RD"),
    DEVENGO_FACTURACION(72, "Devengo y Rechazo Facturacion", "D", "RF"),
    DEVENGO_CLEARING(75, "Devengo y Rechazo Clearing", "D", "RG"),
    DEVENGO_CORRETAJE(77, "Devengo y Rechazo Corretajes BME", "D", "RB"),
    DEVENGO_COMISION(80, "Devengo Com Devolución/Ordenante", "D", "RO"),
    COBRO_ROUTING(71, "Cobro y Diferencias Routing", "C", "RC"),
    EMISION_FACTURA(73, "Emisión Factura", null, "RE"),
    COBRO_FACTURA(74, "Cobro y Diferencias Facturación", "C", "RT"),
    LIQUIDACION_CLEARING(76, "Liquidación y Diferencias Clearing", "C", "RL"),
    LIQUIDACION_BME(78, "Liquidación y Diferencias Comisiones BME", "C", "RQ"),
    NETING_CLEARING(79, "Netting Clearing", "C", "RN"),
    PAGO_COMISION_DEVOLUCION(81, "Pago Com Devolución/Ordenante", "C", "RP"),
    RETROCESION_ROUTING(82, "Retrocesión Cobro routing", "C", "RR"),
    FALLIDOS(83, "Fallidos", null, "RK"),
    EVENTOS(84, "Eventos Corporativos", null, "RV"),
    DOTACION(85, "Dotación y Desdotación", null, "RI"),
    CANONES(86, "Contabilización Cánones BME", null, "RU"),
    DEVOLUCION_COMERCIAL(87, "Apunte manual de devolución", null, "RA");

    private int tipo;
    private String descripcion;
    private String tipoMovimiento;
    private String siglas;

    private TIPO_COMPROBANTE(int tipo, String descripcion, String tipoMovimiento, String siglas) {
      this.tipo = tipo;
      this.descripcion = descripcion;
      this.tipoMovimiento = tipoMovimiento;
      this.siglas = siglas;
    }

    public int getTipo() {
      return tipo;
    }

    public String getDescripcion() {
      return descripcion;
    }

    public String getTipoMovimiento() {
      return tipoMovimiento;
    }

    public String getSiglas() {
      return siglas;
    }

    public static TIPO_COMPROBANTE get(int tipo) {
      for (TIPO_COMPROBANTE bean : values()) {
        if (bean.getTipo() == tipo) {
          return bean;
        }
      }
      return null;
    }
  }

  public enum TIPO_APUNTE implements TIPO {
    /* ALIAS */
    TASK_ALIAS_SINCRONIZACION(980, "ALIAS: Task Sincronizacion"),
    /* CONCILIACION */
    CONCILIACION_SALDO_INICIAL(40, "Conciliacion: Calculo saldo inicial"),

    CONCILIACION_S3_FILE_LOAD(42, "CONCILIACION: Carga Fichero S3"),
    TASK_CONCILIACION_SALDOS_FIN_DIA_ECC(43, "CONCILIACION: Lectura fichero ECC"),
    /* FALLIDO */
    FALLIDO_AJUSTES(901, "FALLIDO: Tarea de Ajustes"),
    FALLIDO_FALLIDOS(902, "FALLIDO: Tarea Fallidos"),
    FALLIDO_GENERACIONH47(903, "FALLIDO: Generacion H47"),
    FALLIDO_LECTURAH47(904, "FALLIDO: Lectura H47"),
    FALLIDO_GENERACIONH47_MTF(905, "FALLIDO: Creacion Automatica OperacionesH47 Mtfs"),
    FALLIDO_GENERACIONH47_FALLIDOS_NACIONAL(906, "FALLIDO: Creacion Automatica OperacionesH47 Fallidos Nacional"),
    FALLIDO_GENERACIONTR(907, "FALLIDO: Creacion Automatica Operaciones TR"),
    
    /* FINANCIERO */
    TASK_CUADRE_ECC(130, "COMUNICACIONES ECC: Cuadre Ecc"),
    TASK_EURO_CCP(131, "COMUNICACIONES ECC: Euro Ccp"),
    /*LOAD XML FIDESSA*/
    TASK_OPEN_PTI_SESSION(199,"COMUNICACIONES ECC: Open Pti Session"),
    PTI_FIN_DIA(210,"COMUNICACIONES ECC: Pti Fin Dia"),
    TASK_LOAD_XML_FIDESSA(132, "COMUNICACIONES: Load Xml Fidessa"),
    TASK_LECTURA_PTI(133, "COMUNICACIONES ECC: Lectura PTI"),
    TASK_PROCESAMIENTO_MENSAJES_PTI(134, "COMUNICACIONES ECC: Procesamiento Mensajes PTI"),
    TASK_LECTURA_PTI_SD(135, "COMUNICACIONES ECC: Lectura PTI SD"),
    TASK_PROCESAMIENTO_MENSAJES_PTI_SD(136, "COMUNICACIONES ECC: Procesamiento Mensajes PTI SD"),
    TASK_ENVIO_CE(234, "COMUNICACIONES ECC: Envio CE"),
    TASK_DAEMONS_ESCALADO_ESTADO_ASIGNACION(231, "COMUNICACIONES ECC: Escalado estado de asignación"),
    TASK_DAEMONS_ESCALADO_ESTADO_TITULARIDAD(232, "COMUNICACIONES ECC: Escalado estado de titularidad"),
    TASK_DAEMONS_ESCALADO_ESTADO_CONTABILIDAD(233, "COMUNICACIONES ECC: Escalado estado de contabilidad"),
    TASK_DAEMONS_ESCALADO_ESTADO_ASIGNACION_SD(252, "COMUNICACIONES ECC: Escalado estado de asignación SD"),
    TASK_DAEMONS_ESCALADO_ESTADO_TITULARIDAD_SD(253, "COMUNICACIONES ECC: Escalado estado de titularidad SD"),
    TASK_DAEMONS_ESCALADO_ESTADO_CONTABILIDAD_SD(254, "COMUNICACIONES ECC: Escalado estado de contabilidad SD"),
    /* FIXML */
    FIXML_INPUT(910, "FIXML: Tarea FIXML Input"),
    /* MINIJOBS */
    MINIJOBS_PTI_FILES(921, "MINIJOBS: PTIFilesJob"),
    MINIJOBS_TASK_FILE_BACKUP(922, "MINIJOBS: TaskFileBackup"),
    MINIJOBS_TASK_PROCESAR_DESGLOSE(923, "MINIJOBS: TaskProcesarDesgloses"),
    TASK_WRITE_1015_FILES(924, "TaskWrite1015File"),
    /* OPRETATIVANETTING */
    TASK_CLEARING_INTERESES_FINANCIACION(55, "Netting: TaskInteresesFinanciacion"),
    /* FACTURAS */
    FACTURAS_EMITIDAS(61, "Facturas emitidas"),
    FACTURAS_COBROS(62, "Cobro de facturas"),
    FACTURAS_CERRADAS(63, "Facturas cerradas"),
    FACTURAS_ANULADAS(64, "Facturas anuladas"),
    TASK_FACTURAS_ENVIO_RECORDATORIO(930, "FACTURAS: Envio Recordatorio"),
    TASK_FACTURAS_ENVIO_FACTURA(931, "FACTURAS: Envio Factura"),
    TASK_FACTURAS_ENVIO_FACTURA_RECTIFICATIVA(932, "FACTURAS: Envio Rectificativa"),
    TASK_FACTURAS_FACTURA(933, "FACTURAS: Task Factura"),
    TASK_FACTURAS_FACTURA_RECTIFICATIVA(934, "FACTURAS: Task Factura Rectificativa"),
    TASK_FACTURAS_FACTURAS_PENDIENTE_RECORDATORIO(935, "FACTURAS: Pendientes Recordatorio"),
    TASK_FACTURAS_SUGERIDA(936, "FACTURAS: Task Factura Sugerida"),
    /* OPERACIONES */
    TASK_OPERACIONES_ENVIO_INFORME(950, "OPERACIONES: Envio Informes"),
    COBROS_ROUTING(65, "Generacion de cobros routing"),
    COBROS_CLEARING_NETTING_MERCADO(66, "Generacion de cobros clearing_netting_mercado"),
    COBROS_CLEARING_NETTING_CLIENTE(67, "Generacion de cobros clearing_netting_cliente"),
    TASK_COBROS_ENVIO_MAIL(940, "COBROS: Envio Mail"),
    FALLIDOS_AJUSTES(68, "Generacion de AC de fallidos_ajustes"),
    CONTABILIDAD(69, "Generacion y envío de los apuntes contables"),
    ACCOUNTING(150, "Actualizacion de campos en DESGLOSESCLITIT"),
    DOTACION(88, "Dotacion"),
    DESDOTACION(89, "Desdotacion"),
    EMISION_DEVENGOS_ROUTING(60, "Generacion de Devengos de Routing"),
    EMISION_DEVENGOS_CLEARING(90, "Generacion de Devengos de Clearing"),
    EMISION_DEVENGOS_CORRETAJES(93, "Generacion de Devengos de Corretajes BME"),
    EMISION_DEVENGOS_FACTURACION(94, "Generacion de Devengos de Facturacion"),
    EMISION_RETROCESIONES_DEVENGOS(91, "Generacion de Retrocesiones de devengos"),
    CANON_BME(92, "Contabilizacion Canones BME"),
    COBROS_COMISIONES_BME(95, "Generacion cobros de comisiones BME"),
    /* PTI */
    ENVIO_FICHEROS_PTI(925, "Envio Ficheros Pti"),
   
    TASK_OTROS_ESCRITURA_SPB(990, "OTROS: Escritura SPB"), //Santander Private Banking
    TASK_ROUTING_TITULARES(50, "Routing Titulares: TaskRoutingTitulares"),
    TASK_ROUTING_TITULARES_OPEN(51, "Routing Titulares: TaskRoutingTitularesOpen"),
    TASK_ROUTING_TITULARES_BANCA_PRIVADA(52, "Routing Titulares: TaskBancaPrivada"),
    TASK_ROUTING_TITULARES_ESPECIALES(56, "Routing Titulares: TaskTitularesPartenonEspeciales"),
    TASK_OPERACIONES_ESPECIALES(110, "Tratamiento Mensajería Partenon"),
    /* ACCOUNTING */
    TASK_ACTUALIZACION_DESGLOSE(150, "Actualizacion desglose clitit"),
    TASK_ACTUALIZACION_DATOS_COMPENSACION(151, "Actualizacion  datos de compensacion"),
    TASK_CONTA_PERIODOS(155, "Periodos Contabilidad"),
    
    TASK_CONTA_ACTUALIZA_OPS_NO_COBRABLES(156, "Actualizar estado contable operaciones no cobrables"),
    
    
    /* INTERFACE EMISION FACTURAS */
    TASK_EMISION_FACTURAS(152, "Emisión Facturas"),
    TASK_SACCR_IRIS(153, "SACCR IRIS"),
    /* ENVIO ORDENES PARTENON */
    TASK_ENVIO_ORDENES_PARTENON(154, "Envio ordenes Partenon"),
    /* GENERAR INFORMES */
    TASK_GENERAR_INFORMES_BATCH(160, "Generar Informes Batch"),
    /* COMUNICACIONES */
    TASK_COMUNICACIONES_MQ_IN(260, "Comunicaciones: MQ.partenon.in"),
    TASK_COMUNICACIONES_MQ_OUT(261, "Comunicaciones: MQ.partenon.out"),
    // TASK_COMUNICACIONES_MQ_ANALIZAR (262, "Comunicaciones: MQ.partenon.in.analizar" );

    /* MANTENIMIENTO */
    TASK_MANTENIMIENTO_MIFID(299, "Mantenimiento:TaskCargarMIFID"),
    TASK_DAEMONS_BATCH(961, "Daemons: Ejecución de Batch"),
    TASK_DAEMONS_ORK(962, "Daemons: Generación Ork"),
    TASK_CONCILIACION_NORMA43_FILE_LOAD(41, "Conciliación: Carga del fichero Norma43"),

    TASK_GENERAR_REPORTE_SPEECH_MINER(161,"Task Generar Fichero SpeechMiner");

    private int tipo;
    private String descripcion;

    private TIPO_APUNTE(int tipo, String descripcion) {
      this.tipo = tipo;
      this.descripcion = descripcion;
    }

    public int getTipo() {
      return tipo;
    }

    public String getDescripcion() {
      return descripcion;
    }
  }

  public enum TIPO_TAREA implements TIPO {

    TASK_REPARTO_FICHEROS_ESTATICOS(6, "Daemons: Reparto ficheros estaticos"),
    TASK_ENVIO_FICHEROS_ESTATICOS_AS400(7, "Daemons: Envio ficheros estaticos AS400"),
    TASK_PROCESAMIENTO_FICHEROS_ESTATICOS(8, "Daemons: Procesamiento ficheros estaticos"),
    TASK_ENVIO_FICHEROS_ESTATICOS_FDA_FIDESSA(9, "Daemons: Envio ficheros estaticos fda fidessa"),
    TASK_REPARTO_CALCULOS_ECONOMICOS_ALC_TO_DCT(10, "Daemons: Reparto calculos economicos alc to dct"),
    TASK_CONCILIACION_NORMA43_FILE_LOAD(41, "Conciliación: Carga del fichero Norma43"),
    TASK_ROUTING_TITULARES(50, "Routing Titulares: TaskRoutingTitulares"),
    TASK_ROUTING_TITULARES_OPEN(51, "Routing Titulares: TaskRoutingTitularesOpen"),
    TASK_ROUTING_TITULARES_BANCA_PRIVADA(52, "Routing Titulares: TaskBancaPrivada"),
    TASK_CLEARING_ENVIO_FICHERO_MEGARA(53, "Netting: TaskEnvioFicheroMegara"),
    TASK_CLEARING_RESPUESTA_FICHERO_MEGARA(54, "Netting: TaskRespuestaFicheroMegara"),
    TASK_CLEARING_INTERESES_FINANCIACION(55, "Netting: TaskInteresesFinanciacion"),
    TASK_DAEMONS(200, "Daemons: Padre"),
    TASK_DESGLOSE_AUTOMATICO(219, "Daemons: Desglose Automatico"),
    TASK_DAEMONS_CASE_EJECUCIONES(220, "Daemons: Case"),
    TASK_CREACION_MO_ASIGNACION_DESGLOSE_CAMARA(221, "Comunicaciones:Envio MO"),
    TASK_CREACION_TABLAS_TITULARES(223, "Daemons: Creacion Tablas Titulares"),
    TASK_CREACION_TABLAS_TITULARES_SD(244, "Daemons: Creacion Tablas Titulares SD"),
    TASK_DAEMONS_ESCALADO_ESTADO_TITULARIDAD(232, "Daemons: Escalado estado de titularidad"),
    TASK_DAEMONS_ESCALADO_ESTADO_CONTABILIDAD(233, "Daemons: Escalado estado de contabilidad"),
    TASK_IMPUTACION_S3_PTI(237, "Comunicaciones:Imputacion S3 Pti"),
    TASK_IMPUTACION_S3_EUROCCP(238, "Comunicaciones:Imputacion S3 EuroCcp"),
    TASK_DESGLOSE_AUTOMATICO_SD(240, "Daemons: Desglose Automatico SD"),
    TASK_ASIGNACION_ORDEN_SD(249, "Comunicaciones:Asignacion Orden SD"),


    // DWH
    TASK_FICHERO_EJECUCIONES(13, "DWHJob: generacion fichero de ejecuciones"),
    TASK_FICHERO_DATOS_ESTATICOS(14,"DWHJob: generacion fichero de datos estaticos"),
    TASK_FICHERO_TRADERS(15, "DWHJob: generacion fichero de traders y accounts"),
    TASK_FICHERO_CLIENTES(505, "DWHJob: generacion fichero de clientes"),
    TASK_FICHERO_NACIONAL(508, "DWHJob: generacion fichero de ejecuciones nacional"),
    TASK_PLANIFICACION_DWH(509, "DWHJob: planificador ficheros DWH"),
    TASK_FICHERO_VALORES(506, "DWHJob: Proceso de Ficheros de Valores"),
    TASK_CARGA_VALORES(507, "DWHJob: Proceso de Carga de Valores"),
    TASK_VALIDACIONES_VALORES(582, "DWHJob: Proceso de Validaciones de Valores"),

    // TASK CUCO
    TASK_CUCO(501, "Daemons: Envio ficheros estaticos CUCO"),

    // TASK IRIS
    TASK_IRIS(502, "Daemons: Envio ficheros estaticos IRIS"),
    
    
    TASK_DAEMONS_ENVIO_FICHERO_CC(258, "Daemons: Envio fichero corretajes"),
    TASK_CONFIRMACION_MINORISTA(960, "Daemons: Confirmacion Minoristas"),
    TASK_MINIJOBS_LIQ_STATE_CHANGE(920, "MiniJobs: LiqStateChange"),
    TASK_CALCULO_CANONES(11, "Daemons: Calculo Canones"),
    TASK_CALCULO_CANONES_SD(12, "Daemons: Calculo Canones SD"),
    
    // DMO
    TASK_DMO(16, "Dmo: Recopilación de datos"),
    
    // PBC
    TASK_PBC(17, "Pbc: Proceso de Recalculo de CCE"), 
    TASK_PCB_EFECTIVO_MENSUAL(18, "Pbc: Proceso de Carga de Efectivos Mensual"), 
    TASK_PCB_EFECTIVO_ANUAL(19, "Pbc: Proceso de Carga de Efectivos Anual"),
    // MCLAGAN
    
    TASK_MCLAGAN(503, "Daemons: Envio ficheros McLagan");


    private int tipo;
    private String descripcion;

    private TIPO_TAREA(int tipo, String descripcion) {
      this.tipo = tipo;
      this.descripcion = descripcion;
    }

    public int getTipo() {
      return tipo;
    }

    public String getDescripcion() {
      return descripcion;
    }
  }

  public enum TIPOLOGIA {
    Routing(10),
    Clearing(20),
    Facturacion(30),
    BME(40);

    private int tipo;

    private TIPOLOGIA(int tipo) {
      this.tipo = tipo;
    }

    public int getTipo() {
      return tipo;
    }
  }

  public interface TIPO {

    public int getTipo();

    public String getDescripcion();
  }

  public enum TIPO_IMPORTE {
    Efectivo(10, "Efectivo"),
    Canon(20, "Canon"),
    Corretaje(30, "Corretaje"),
    ComDevolucion(40, "Comisión devolución"),
    ComisionBME(50, "Comisión BME");
    private int tipo;
    private String descripcion;

    private TIPO_IMPORTE(int tipo, String descripcion) {
      this.tipo = tipo;
      this.descripcion = descripcion;
    }

    public int getTipo() {
      return tipo;
    }

    public String getDescripcion() {
      return descripcion;
    }
  }

  public enum LIQUIDACION {
    // Prueba
    SLQ(10, "Sin Liquidar desgloses"),
    MRC(20, "Liquidado Mercado desgloses"),
    CLT(30, "Liquidado Cliente desgloses"),
    PRC(40, "Liquidado parcialmente"),
    CMP(50, "Liquidado Completamente desgloses");

    private int id;
    private String descripcion;

    private LIQUIDACION(int id, String descripcion) {
      this.id = id;
      this.descripcion = descripcion;
    }

    public int getId() {
      return id;
    }

    public String getDescripcion() {
      return descripcion;
    }
  }

  public enum ESTADO_DESGLOSES_OPERACIONES_ESPECIALES {
    INICIO_PDTE_REVISAR_DESGLOSES(0, "INICIO PDTE REVISAR DESGLOSES"),
    DESGLOSES_ERRONEOS(1, "DESGLOSES ERRONEOS"),
    DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS(2, "DESGLOSES CORRECTOS PDTE EJECUTAR"),
    DESGLOSE_EJECUTADO_CORRECTO_ALLO_CREADO(3, "DESGLOSE EJECUTADO CORRECTO"),
    ENVIADO_10_15_PDTE_CARGAR_TITULAR(4, "ENVIADO 10 15 PDTE. CARGAR TITULAR"),
    TITULAR_CARGADO(5, "TITULAR CARGADO");
    private int id;
    private String descripcion;

    private ESTADO_DESGLOSES_OPERACIONES_ESPECIALES(int id, String descripcion) {
      this.id = id;
      this.descripcion = descripcion;
    }

    public int getId() {
      return id;
    }

    public String getDescripcion() {
      return descripcion;
    }
  }

  public enum ESTADO_PROCESADO_CUADRE_ECC {
    /** Valor: 2000 **/
    PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC(2000),
    /** Valor: 2002 **/
    PDTE_REVISAR_TITULOS_OPERACIONES_ECC(2002),
    /** Valor: 2005 **/
    TITULARES_ERRONEOS(2005),
    /** Valor: 2010 **/
    TITULOS_ERRONEO(2010),
    /** Valor: 2012 **/
    SIN_INSTRUCCIONES(2012),
    /** Valor: 2014 **/
    PDTE_REVISAR_DESGLOSE_USUARIO(2014),
    /** Valor: 2016 **/
    PDTE_ARREGLAR_DESGLOSES(2015),
    /** Valor: 2015 **/
    PDTE_REVISAR_CODIGOS_OPERACION_USUARIO(2016),
    /** Valor: 2017 **/
    PDTE_CARGAR_CODIGOS_OP_MOVIMIENTOS(2017),
    /** Valor: 2020 **/
    TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES(2020),
    /** Valor: 2030 **/
    PDTE_REVISAR_REPARTO_DESGLOSES_USUARIO(2030),
    /** Valor: 2060 **/
    PDTE_IMPORTAR_DESGLOSES_ECC(2040),
    /** Valor: 2070 **/
    ERROR_DESGLOSE_COBRADO(2070),
    /** Valor: 2080 **/
    PDTE_REEMPLAZAR_TITULARES_POR_ECC(2080),
    /** Valor: 2090 **/
    PDTE_REEMPLAZAR_TITULARES_POR_SIBBAC(2090),
    /** Valor: 2100 **/
    TITULARES_DESGLOSES_OK_PDTE_REVISAR_INFORMACION_ASIGNACION_MOVIMIENTOS(2100),
    /** Valor: 2105 **/
    MOVIMIENTOS_REVISADOS_ERROR(2105),
    /** Valor: 2110 **/
    MOVIMIENTOS_REVISADOS(2110);

    private Integer id;

    private ESTADO_PROCESADO_CUADRE_ECC(Integer id) {
      this.id = id;
    }

    public Integer getId() {
      return id;
    }
  }

  public enum ESTADO_PROCESADO_EURO_CCP {
    
    /** Valor: 3000 **/
    REGISTRO_CANCELADO(3000),
    /** Valor: 3100 **/
    PDTE_PROCESAR(3100),
    /** Valor: 3200 **/
    FICHERO_GENERADO(3200),
    /** Valor: 3300 **/
    PROCESADO_ENVIADO(3300),
    /** Valor: 3400 **/
    RECIBIENDO(3400),
    /** Valor: 3500 **/
    FICHERO_PROCESADO(3500);

    private Integer id;

    private ESTADO_PROCESADO_EURO_CCP(Integer id) {
      this.id = id;
    }

    public Integer getId() {
      return id;
    }
  }
  
  

}
