package sibbac.business.wrappers.database.model;


import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.fase0.database.model.Tmct0mscId;


@Entity
@Table( name = "TMCT0MSC" )
public class Tmct0msc implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -6301763174455582438L;
	private Tmct0mscId			id;
	private Tmct0men			tmct0men;
	private Tmct0sta			tmct0sta;
	private Date				hrmensa				= new Date();
	private String				cdversio			= "";
	private String				cdauxili			= "";
	private String				nfile				= " ";
	private String				cdusuaud			= "";
	private Date				fhaudit				= new Date();
	private int					nuversion			= 0;

	// private Set<Tmct0msg> tmct0msgs = new HashSet<Tmct0msg>(0);

	public Tmct0msc() {
	}

	public Tmct0msc( Tmct0mscId id, Tmct0sta tmct0sta, Date hrmensa, String cdversio, String cdauxili, String cdusuaud, Date fhaudit,
			int nuversion, Tmct0men tmct0men ) {
		this.id = id;
		this.tmct0sta = tmct0sta;
		this.hrmensa = hrmensa;
		this.cdversio = cdversio;
		this.cdauxili = cdauxili;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
		this.tmct0men = tmct0men;
	}

	public Tmct0msc( Tmct0mscId id, Tmct0sta tmct0sta, Date hrmensa, String cdversio, String cdauxili, String nfile, String cdusuaud,
			Date fhaudit, int nuversion, Tmct0men tmct0men/*
														 * , Set<Tmct0msg>
														 * tmct0msgs
														 */
	) {
		this.id = id;
		this.tmct0sta = tmct0sta;
		this.hrmensa = hrmensa;
		this.cdversio = cdversio;
		this.cdauxili = cdauxili;
		this.nfile = nfile;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.nuversion = nuversion;
		this.tmct0men = tmct0men;
		// this.tmct0msgs = tmct0msgs;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "cdmensa", column = @Column( name = "CDMENSA", nullable = false, length = 3 ) ),
			@AttributeOverride( name = "femensa", column = @Column( name = "FEMENSA", nullable = false, length = 10 ) ),
			@AttributeOverride( name = "nuenvio", column = @Column( name = "NUENVIO", nullable = false, precision = 5 ) )
	} )
	public Tmct0mscId getId() {
		return this.id;
	}

	public void setId( Tmct0mscId id ) {
		this.id = id;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( {
			@JoinColumn( name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false ),
			@JoinColumn( name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false )
	} )
	public Tmct0sta getTmct0sta() {
		return this.tmct0sta;
	}

	public void setTmct0sta( Tmct0sta tmct0sta ) {
		this.tmct0sta = tmct0sta;
	}

	@Temporal( TemporalType.TIME )
	@Column( name = "HRMENSA", nullable = false, length = 8 )
	public Date getHrmensa() {
		return this.hrmensa;
	}

	public void setHrmensa( Date hrmensa ) {
		this.hrmensa = hrmensa;
	}

	@Column( name = "CDVERSIO", nullable = false, length = 5 )
	public String getCdversio() {
		return this.cdversio;
	}

	public void setCdversio( String cdversio ) {
		this.cdversio = cdversio;
	}

	@Column( name = "CDAUXILI", nullable = false, length = 20 )
	public String getCdauxili() {
		return this.cdauxili;
	}

	public void setCdauxili( String cdauxili ) {
		this.cdauxili = cdauxili;
	}

	@Column( name = "NFILE", length = 50 )
	public String getNfile() {
		return this.nfile;
	}

	public void setNfile( String nfile ) {
		this.nfile = nfile;
	}

	@Column( name = "CDUSUAUD", nullable = false, length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", nullable = false, length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "NUVERSION", nullable = false )
	public int getNuversion() {
		return this.nuversion;
	}

	public void setNuversion( int nuversion ) {
		this.nuversion = nuversion;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "CDMENSA", nullable = false, insertable = false, updatable = false )
	public Tmct0men getTmct0men() {
		return this.tmct0men;
	}

	public void setTmct0men( Tmct0men tmct0men ) {
		this.tmct0men = tmct0men;
	}
	//
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0msc")
	// public Set<Tmct0msg> getTmct0msgs() {
	// return this.tmct0msgs;
	// }
	//
	// public void setTmct0msgs(Set<Tmct0msg> tmct0msgs) {
	// this.tmct0msgs = tmct0msgs;
	// }

}
