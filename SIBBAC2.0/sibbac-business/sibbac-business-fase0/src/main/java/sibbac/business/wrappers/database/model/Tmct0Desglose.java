package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.ToStringBuilder;

import sibbac.database.DBConstants;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author fjarquellada
 */
@Entity
@Table(name = DBConstants.WRAPPERS.DESGLOSE)
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Tmct0Desglose.findAll", query = "SELECT t FROM Tmct0Desglose t"), @NamedQuery(name = "Tmct0Desglose.findById",
                                                                                                                  query = "SELECT t FROM Tmct0Desglose t WHERE t.id = :id"), @NamedQuery(name = "Tmct0Desglose.findByAuditDate",
                                                                                                                                                                                         query = "SELECT t FROM Tmct0Desglose t WHERE t.auditDate = :auditDate"), @NamedQuery(name = "Tmct0Desglose.findByAuditUser",
                                                                                                                                                                                                                                                                              query = "SELECT t FROM Tmct0Desglose t WHERE t.auditUser = :auditUser"), @NamedQuery(name = "Tmct0Desglose.findByCdestado",
                                                                                                                                                                                                                                                                                                                                                                   query = "SELECT t FROM Tmct0Desglose t WHERE t.cdestado = :cdestado"), @NamedQuery(name = "Tmct0Desglose.findByNuorden",
                                                                                                                                                                                                                                                                                                                                                                                                                                                      query = "SELECT t FROM Tmct0Desglose t WHERE t.nuorden = :nuorden"), @NamedQuery(name = "Tmct0Desglose.findByNbooking",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       query = "SELECT t FROM Tmct0Desglose t WHERE t.nbooking = :nbooking"), @NamedQuery(name = "Tmct0Desglose.findByNucnfclt",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          query = "SELECT t FROM Tmct0Desglose t WHERE t.nucnfclt = :nucnfclt"), @NamedQuery(name = "Tmct0Desglose.findByNucnfliq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             query = "SELECT t FROM Tmct0Desglose t WHERE t.nucnfliq = :nucnfliq"), @NamedQuery(name = "Tmct0Desglose.findByNutitulos",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                query = "SELECT t FROM Tmct0Desglose t WHERE t.nutitulos = :nutitulos"), @NamedQuery(name = "Tmct0Desglose.findBySubaccount",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     query = "SELECT t FROM Tmct0Desglose t WHERE t.subaccount = :subaccount"), @NamedQuery(name = "Tmct0Desglose.findByCustodian",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            query = "SELECT t FROM Tmct0Desglose t WHERE t.custodian = :custodian"), @NamedQuery(name = "Tmct0Desglose.findByCdrefban",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 query = "SELECT t FROM Tmct0Desglose t WHERE t.cdrefban = :cdrefban"), @NamedQuery(name = "Tmct0Desglose.findByAcctatcle",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    query = "SELECT t FROM Tmct0Desglose t WHERE t.acctatcle = :acctatcle"), @NamedQuery(name = "Tmct0Desglose.findByDesglose",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         query = "SELECT t FROM Tmct0Desglose t WHERE t.desglose = :desglose"), @NamedQuery(name = "Tmct0Desglose.findByCapacity",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            query = "SELECT t FROM Tmct0Desglose t WHERE t.capacity = :capacity"), @NamedQuery(name = "Tmct0Desglose.findByPccombco",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               query = "SELECT t FROM Tmct0Desglose t WHERE t.pccombco = :pccombco"), @NamedQuery(name = "Tmct0Desglose.findByImcombco",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  query = "SELECT t FROM Tmct0Desglose t WHERE t.imcombco = :imcombco"), @NamedQuery(name = "Tmct0Desglose.findByPccomdev",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     query = "SELECT t FROM Tmct0Desglose t WHERE t.pccomdev = :pccomdev"), @NamedQuery(name = "Tmct0Desglose.findByImcomdev",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        query = "SELECT t FROM Tmct0Desglose t WHERE t.imcomdev = :imcomdev"), @NamedQuery(name = "Tmct0Desglose.findByIdRegla",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           query = "SELECT t FROM Tmct0Desglose t WHERE t.idRegla = :idRegla"), @NamedQuery(name = "Tmct0Desglose.findByIdCompensador",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            query = "SELECT t FROM Tmct0Desglose t WHERE t.idCompensador = :idCompensador"), @NamedQuery(name = "Tmct0Desglose.findByCdReferenciaAsignacion",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         query = "SELECT t FROM Tmct0Desglose t WHERE t.cdReferenciaAsignacion = :cdReferenciaAsignacion"), @NamedQuery(name = "Tmct0Desglose.findByIdCuentaCompensacion",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        query = "SELECT t FROM Tmct0Desglose t WHERE t.idCuentaCompensacion = :idCuentaCompensacion")
})
public class Tmct0Desglose implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Basic(optional = false)
  @Column(name = "ID")
  private Long id;
  @Column(name = "AUDIT_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date auditDate;
  @Column(name = "AUDIT_USER", length = 255)
  private String auditUser;
  @Basic(optional = false)
  @Column(name = "CDESTADO", nullable = false)
  private Character cdestado;
  @Basic(optional = false)
  @Column(name = "NUORDEN", length = 20, nullable = false)
  private String nuorden;
  @Basic(optional = false)
  @Column(name = "NBOOKING", length = 20, nullable = false)
  private String nbooking;
  @Basic(optional = false)
  @Column(name = "NUCNFCLT", length = 20, nullable = false)
  private String nucnfclt;
  @Column(name = "NUCNFLIQ")
  private Short nucnfliq;
  // @Max(value=?) @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce
  // field validation
  @Column(name = "NUTITULOS")
  private BigDecimal nutitulos;
  @Column(name = "SUBACCOUNT", length = 20)
  private String subaccount;
  @Column(name = "CUSTODIAN", length = 25)
  private String custodian;
  @Column(name = "CDREFBAN", length = 32)
  private String cdrefban;
  @Column(name = "ACCTATCLE", length = 34)
  private String acctatcle;
  @Column(name = "DESGLOSE")
  private Character desglose;
  @Column(name = "CAPACITY")
  private Character capacity;
  @Column(name = "PCCOMBCO")
  private BigDecimal pccombco;
  @Column(name = "IMCOMBCO")
  private BigDecimal imcombco;
  @Column(name = "PCCOMDEV")
  private BigDecimal pccomdev;
  @Column(name = "IMCOMDEV")
  private BigDecimal imcomdev;
  @Column(name = "ID_REGLA")
  private BigInteger idRegla;
  @Column(name = "ID_COMPENSADOR")
  private BigInteger idCompensador;
  @Column(name = "CD_REFERENCIA_ASIGNACION", length = 18)
  private String cdReferenciaAsignacion;
  @Column(name = "ID_CUENTA_COMPENSACION")
  private BigInteger idCuentaCompensacion;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDesglose")
  private List<Tmct0DesgloseTitular> tmct0DesgloseTitularList;
  @Column(name = "REFERENCIA_TITULAR", length = 20)
  private String referenciaTitular;
  @Column(name = "ID_PROCEDENCIA", length = 20, nullable = false)
  private String idProcedencia;

  @Column(name = "CORRETAJE_PTI", nullable = true)
  private Character corretajePti;
  @Column(name = "REFERENCIA_TITULAR_UNICA", nullable = true)
  private Character referenciaTitularUnica;

  public Tmct0Desglose() {
  }

  public Tmct0Desglose(Long id) {
    this.id = id;
  }

  public Tmct0Desglose(Long id, Character cdestado, String nuorden, String nbooking, String nucnfclt) {
    this.id = id;
    this.cdestado = cdestado;
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public Character getCdestado() {
    return cdestado;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public Short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public BigDecimal getNutitulos() {
    return nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  public String getSubaccount() {
    return subaccount;
  }

  public void setSubaccount(String subaccount) {
    this.subaccount = subaccount;
  }

  public String getCustodian() {
    return custodian;
  }

  public void setCustodian(String custodian) {
    this.custodian = custodian;
  }

  public String getCdrefban() {
    return cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public String getAcctatcle() {
    return acctatcle;
  }

  public void setAcctatcle(String acctatcle) {
    this.acctatcle = acctatcle;
  }

  public Character getDesglose() {
    return desglose;
  }

  public void setDesglose(Character desglose) {
    this.desglose = desglose;
  }

  public Character getCapacity() {
    return capacity;
  }

  public void setCapacity(Character capacity) {
    this.capacity = capacity;
  }

  public BigDecimal getPccombco() {
    return pccombco;
  }

  public void setPccombco(BigDecimal pccombco) {
    this.pccombco = pccombco;
  }

  public BigDecimal getImcombco() {
    return imcombco;
  }

  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  public BigDecimal getPccomdev() {
    return pccomdev;
  }

  public void setPccomdev(BigDecimal pccomdev) {
    this.pccomdev = pccomdev;
  }

  public BigDecimal getImcomdev() {
    return imcomdev;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  public BigInteger getIdRegla() {
    return idRegla;
  }

  public void setIdRegla(BigInteger idRegla) {
    this.idRegla = idRegla;
  }

  public BigInteger getIdCompensador() {
    return idCompensador;
  }

  public void setIdCompensador(BigInteger idCompensador) {
    this.idCompensador = idCompensador;
  }

  public String getCdReferenciaAsignacion() {
    return cdReferenciaAsignacion;
  }

  public void setCdReferenciaAsignacion(String cdReferenciaAsignacion) {
    this.cdReferenciaAsignacion = cdReferenciaAsignacion;
  }

  public BigInteger getIdCuentaCompensacion() {
    return idCuentaCompensacion;
  }

  public void setIdCuentaCompensacion(BigInteger idCuentaCompensacion) {
    this.idCuentaCompensacion = idCuentaCompensacion;
  }

  @XmlTransient
  @JsonIgnore
  public List<Tmct0DesgloseTitular> getTmct0DesgloseTitularList() {
    return tmct0DesgloseTitularList;
  }

  public void setTmct0DesgloseTitularList(List<Tmct0DesgloseTitular> tmct0DesgloseTitularList) {
    this.tmct0DesgloseTitularList = tmct0DesgloseTitularList;
  }

  public String getReferenciaTitular() {
    return referenciaTitular;
  }

  public void setReferenciaTitular(String referenciaTitular) {
    this.referenciaTitular = referenciaTitular;
  }

  public String getIdProcedencia() {
    return idProcedencia;
  }

  public void setIdProcedencia(String idProcedencia) {
    this.idProcedencia = idProcedencia;
  }

  public Character getCorretajePti() {
    return corretajePti;
  }

  public void setCorretajePti(Character corretajePti) {
    this.corretajePti = corretajePti;
  }

  public Character getReferenciaTitularUnica() {
    return referenciaTitularUnica;
  }

  public void setReferenciaTitularUnica(Character referenciaTitularUnica) {
    this.referenciaTitularUnica = referenciaTitularUnica;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Tmct0Desglose)) {
      return false;
    }
    Tmct0Desglose other = (Tmct0Desglose) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

}
