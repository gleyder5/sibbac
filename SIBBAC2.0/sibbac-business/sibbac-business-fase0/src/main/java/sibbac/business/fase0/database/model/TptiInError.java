package sibbac.business.fase0.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TPTI_IN_ERROR")
public class TptiInError implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5363118075886407903L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", nullable = false)
  private long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_TPTI_IN", nullable = false)
  private TptiIn tptiIn;

  @Column(name = "ERROR_CLASS", nullable = false)
  private String errorClass;

  @Column(name = "TRACE_ERROR", length = 32768, nullable = false)
  private String traceError;

  @Column(name = "AUDIT_USER", nullable = false)
  private String audit_user;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", nullable = true)
  private Date audit_fecha_cambio;

  public long getId() {
    return id;
  }

  public TptiIn getTptiIn() {
    return tptiIn;
  }

  public String getErrorClass() {
    return errorClass;
  }

  public String getTraceError() {
    return traceError;
  }

  public String getAudit_user() {
    return audit_user;
  }

  public Date getAudit_fecha_cambio() {
    return audit_fecha_cambio;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setTptiIn(TptiIn tptiIn) {
    this.tptiIn = tptiIn;
  }

  public void setErrorClass(String errorClass) {
    this.errorClass = errorClass;
  }

  public void setTraceError(String traceError) {
    this.traceError = traceError;
  }

  public void setAudit_user(String audit_user) {
    this.audit_user = audit_user;
  }

  public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
    this.audit_fecha_cambio = audit_fecha_cambio;
  }

}
