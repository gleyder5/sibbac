package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Idioma }. Entity: "Idiomas".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table( name = WRAPPERS.IDIOMA )
@Entity
@XmlRootElement
@Audited
public class Idioma extends ATableAudited< Idioma > implements java.io.Serializable {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 4003366287013847256L;

	public static final String	COD_IDIOMA_ES		= "ES";
	public static final String	COD_IDIOMA_EN		= "EN";
	public static final String	COD_IDIOMA_DE		= "DE";

	@Column( name = "NBCODIGO", nullable = false, length = 50 )
	@XmlAttribute
	private String				codigo;

	@Column( name = "NBDESCRIPCION", nullable = false, length = 50 )
	@XmlAttribute
	private String				descripcion;


	// ----------------------------------------------- Bean Constructors

	/**
	 * The {@link sibbac.business.wrappers.database.model.facturacion.Idioma}'s
	 * default constructor.
	 */
	public Idioma() {
	}

	// ------------------------------------------- Bean methods: Getters

	// /**
	// * @return the codigo
	// */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	/**
	 * Getter for descripcion
	 *
	 * @return descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Setter for descripcion
	 *
	 * @param descripcion
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
