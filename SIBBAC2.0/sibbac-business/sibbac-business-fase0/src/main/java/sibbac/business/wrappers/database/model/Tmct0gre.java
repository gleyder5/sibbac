package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0GRE. Executions Groups Documentación para TMCT0GRE <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 * @generated
 **/
@Entity
@Table(name = "TMCT0GRE")
public class Tmct0gre implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1161761645673030068L;

  // @formatter:off
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "nuorden",  column = @Column(name = "NUORDEN", nullable = false, length = 20)), 
                        @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)), 
                        @AttributeOverride(name = "nuagreje", column = @Column(name = "NUAGREJE", nullable = false, length = 20)) 
  })
  private Tmct0greId id;
  // @formatter:on
  /**
   * Relation 1..1 with table TMCT0BOK. Constraint de relación entre TMCT0GRE y TMCT0BOK <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  // @formatter:off
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = { @JoinColumn(name = "NUORDEN",  referencedColumnName = "NUORDEN",  nullable = false, insertable = false, updatable = false), 
                         @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false)
  })
  protected Tmct0bok tmct0bok;
  // @formatter:on

  /**
   * Table Field CDBROKER. Broker Documentación para CDBROKER <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBROKER", length = 20)
  protected String cdbroker;
  /**
   * Table Field CDMERCAD. For foreign markets: exchange; For Spanish markets: segment Documentación para CDMERCAD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDMERCAD", length = 20)
  protected String cdmercad;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;
  /**
   * Relation 1..n with table TMCT0GAL. Constraint de relación entre TMCT0GAL y TMCT0GRE <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0gre")
  protected java.util.List<Tmct0gal> tmct0gals = new ArrayList<Tmct0gal>(0);

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 10)
  private Date fevalor;

  @Column(name = "NUTITAGR", length = 16, scale = 5)
  private BigDecimal nutitagr;

  @Column(name = "PRECIOBRUTOMKT", length = 18, scale = 8)
  private BigDecimal precioBrutoMkt;

  @Column(name = "TPOPEBOL", length = 2)
  private String tpopebol;

  @Column(name = "ASIGNADOCOMP")
  private Boolean asignadoComp;

  public Tmct0greId getId() {
    return id;
  }

  public Tmct0bok getTmct0bok() {
    return tmct0bok;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public String getCdmercad() {
    return cdmercad;
  }

  public String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public java.util.List<Tmct0gal> getTmct0gals() {
    return tmct0gals;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public BigDecimal getNutitagr() {
    return nutitagr;
  }

  public BigDecimal getPrecioBrutoMkt() {
    return precioBrutoMkt;
  }

  public String getTpopebol() {
    return tpopebol;
  }

  public Boolean getAsignadoComp() {
    return asignadoComp;
  }

  public void setId(Tmct0greId id) {
    this.id = id;
  }

  public void setTmct0bok(Tmct0bok tmct0bok) {
    this.tmct0bok = tmct0bok;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  public void setTmct0gals(java.util.List<Tmct0gal> tmct0gals) {
    this.tmct0gals = tmct0gals;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setNutitagr(BigDecimal nutitagr) {
    this.nutitagr = nutitagr;
  }

  public void setPrecioBrutoMkt(BigDecimal precioBrutoMkt) {
    this.precioBrutoMkt = precioBrutoMkt;
  }

  public void setTpopebol(String tpopebol) {
    this.tpopebol = tpopebol;
  }

  public void setAsignadoComp(Boolean asignadoComp) {
    this.asignadoComp = asignadoComp;
  }

  @Override
  public String toString() {
    return "Tmct0gre [id=" + id + ", cdbroker=" + cdbroker + ", cdmercad=" + cdmercad + ", fevalor=" + fevalor
           + ", nutitagr=" + nutitagr + ", precioBrutoMkt=" + precioBrutoMkt + ", tpopebol=" + tpopebol + "]";
  }

}
