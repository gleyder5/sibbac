package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.Cobros;


public class FacturaLineaDeCobrosData {

	// linea de cobro
	private Long		idLineaDeCobro;
	private Cobros		cobro;
	private BigDecimal	importe;
	private BigDecimal	importeAplicado;

	public FacturaLineaDeCobrosData() {

	}

	public Cobros getCobro() {
		return cobro;
	}

	public void setCobro( Cobros cobro ) {
		this.cobro = cobro;
	}

	public Long getIdLineaDeCobro() {
		return idLineaDeCobro;
	}

	public void setIdLineaDeCobro( Long idLineaDeCobro ) {
		this.idLineaDeCobro = idLineaDeCobro;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	public BigDecimal getImporteAplicado() {
		return importeAplicado;
	}

	public void setImporteAplicado( BigDecimal importeAplicado ) {
		this.importeAplicado = importeAplicado;
	}

}
