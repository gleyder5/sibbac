package sibbac.business.wrappers.database.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0REFERENCIATITULAR_FIS")
public class Tmct0referenciatitularFis implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -5623075066418069887L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDREFTIT_FIS", nullable = false)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDREFERENCIA", nullable = false)
  private Tmct0referenciatitular referenciaTitular;

  @Column(name = "IDTITULAR")
  private String cdholder;

  @Column(name = "AUDIT_USER")
  private String auditUser;

  @Column(name = "AUDIT_FECHA_CAMBIO")
  private Date auditFechaCambio;

  public Long getId() {
    return id;
  }

  public Tmct0referenciatitular getReferenciaTitular() {
    return referenciaTitular;
  }

  public String getCdholder() {
    return cdholder;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public Date getAuditFechaCambio() {
    return auditFechaCambio;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setReferenciaTitular(Tmct0referenciatitular referenciaTitular) {
    this.referenciaTitular = referenciaTitular;
  }

  public void setCdholder(String cdholder) {
    this.cdholder = cdholder;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

}
