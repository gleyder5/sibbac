package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0aloDynamicValuesId implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -126834263383563575L;
  @Column(name = "NBOOKING", nullable = false, length = 20)
  private String nbooking = "";
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden = "";
  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  private String nucnfclt = "";
  @Column(name = "NOMBRE", nullable = false, length = 32)
  private String nombre = "";

  public Tmct0aloDynamicValuesId() {
  }

  public Tmct0aloDynamicValuesId(String nuorden, String nbooking, String nucnfclt, String nombre) {
    this.nbooking = nbooking;

    this.nuorden = nuorden;
    this.nucnfclt = nucnfclt;
    this.nombre = nombre;
  }

  public String getNbooking() {
    return this.nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0aloDynamicValuesId other = (Tmct0aloDynamicValuesId) obj;
    if (nombre == null) {
      if (other.nombre != null)
        return false;
    }
    else if (!nombre.equals(other.nombre))
      return false;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    }
    else if (!nbooking.equals(other.nbooking))
      return false;
    if (nucnfclt == null) {
      if (other.nucnfclt != null)
        return false;
    }
    else if (!nucnfclt.equals(other.nucnfclt))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    }
    else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nucnfclt == null) ? 0 : nucnfclt.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public String toString() {
    return String.format("Tmct0aloDynamicValuesId %s##%s##%s##%s", nuorden, nbooking, nucnfclt, nombre);
  }

}
