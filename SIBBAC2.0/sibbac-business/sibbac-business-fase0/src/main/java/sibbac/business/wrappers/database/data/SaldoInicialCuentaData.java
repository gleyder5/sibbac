package sibbac.business.wrappers.database.data;

import java.math.BigDecimal;
import java.util.Date;

public class SaldoInicialCuentaData {
    private Date fecha;
    private String isin;
    private String descisin;
    private String codcuentaliq;
    private BigDecimal titulos;
    private BigDecimal efectivo;
    
    public SaldoInicialCuentaData(){
	//
    }
    public SaldoInicialCuentaData(Date fecha, String isin, String descisin, String codcuentaliq, BigDecimal titulos, BigDecimal efectivo){
	this.fecha = fecha;
	this.isin = isin;
	this.descisin = descisin;
	this.codcuentaliq = codcuentaliq;
	this.titulos = titulos;
	this.efectivo = efectivo;
    }
    public SaldoInicialCuentaData(String isin, String descisin, String codcuentaliq, BigDecimal titulos, BigDecimal efectivo){
	this.isin = isin;
	this.descisin = descisin;
	this.codcuentaliq = codcuentaliq;
	this.titulos = titulos;
	this.efectivo = efectivo;
    }
    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public String getIsin() {
        return isin;
    }
    public void setIsin(String isin) {
        this.isin = isin;
    }
    
    public String getDescisin() {
        return descisin;
    }
    public void setDescisin(String descisin) {
        this.descisin = descisin;
    }
    public String getCodcuentaliq() {
        return codcuentaliq;
    }
    public void setCodcuentaliq(String codcuentaliq) {
        this.codcuentaliq = codcuentaliq;
    }
    public BigDecimal getTitulos() {
        return titulos;
    }
    public void setTitulos(BigDecimal titulos) {
        this.titulos = titulos;
    }
    public BigDecimal getEfectivo() {
        return efectivo;
    }
    public void setEfectivo(BigDecimal efectivo) {
        this.efectivo = efectivo;
    }
    
}
