package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.S3Errores}.
 */

@Entity
@Table( name = DBConstants.CLEARING.S3ERRORES )
@Audited
public class S3Errores extends ATableAudited< S3Errores > implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8251082730498873528L;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( value = {
		@JoinColumn( name = "IDLIQUIDACION", referencedColumnName = "ID" )
	} )
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private S3Liquidacion		s3liquidacion;

	@Column( name = "CDESTADO", nullable = false, length = 1 )
	private char				cdestado			= 'A';

	@Column( name = "DATOERRORSV", length = 60 )
	private String				datoErrorSV;

	@Column( name = "DATOERRORCLIENTE", length = 60 )
	private String				datoErrorCliente;

	@Column( name = "DSERROR", length = 60 )
	private String				dserror;

	public S3Errores() {

	}

	public S3Errores( S3Liquidacion s3liquidacion, char cdestado ) {
		super();
		this.s3liquidacion = s3liquidacion;
		this.cdestado = cdestado;
	}

	public S3Errores( S3Liquidacion s3liquidacion, char cdestado, String datoErrorSV, String datoErrorCliente, String dserror ) {
		super();
		this.s3liquidacion = s3liquidacion;
		this.cdestado = cdestado;
		this.datoErrorSV = datoErrorSV;
		this.datoErrorCliente = datoErrorCliente;
		this.dserror = dserror;
	}

	public S3Liquidacion getS3liquidacion() {
		return s3liquidacion;
	}

	public void setS3liquidacion( S3Liquidacion s3liquidacion ) {
		this.s3liquidacion = s3liquidacion;
	}

	public char getCdestado() {
		return cdestado;
	}

	public void setCdestado( char cdestado ) {
		this.cdestado = cdestado;
	}

	public String getDatoErrorSV() {
		return datoErrorSV;
	}

	public void setDatoErrorSV( String datoErrorSV ) {
		this.datoErrorSV = datoErrorSV;
	}

	public String getDatoErrorCliente() {
		return datoErrorCliente;
	}

	public void setDatoErrorCliente( String datoErrorCliente ) {
		this.datoErrorCliente = datoErrorCliente;
	}

	public String getDserror() {
		return dserror;
	}

	public void setDserror( String dserror ) {
		this.dserror = dserror;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != null ? id.hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if ( !( object instanceof S3Errores ) ) {
			return false;
		}
		S3Errores other = ( S3Errores ) object;
		if ( ( this.id == null && other.id != null ) || ( this.id != null && !this.id.equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sibbac.business.operativanetting.database.model.S3Errores[ id=" + id + " ]";
	}

}
