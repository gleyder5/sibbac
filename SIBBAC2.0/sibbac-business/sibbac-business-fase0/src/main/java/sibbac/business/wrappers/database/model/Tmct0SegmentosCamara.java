package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_SEGMENTOS_CAMARA" )
public class Tmct0SegmentosCamara implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 7252741276907410885L;
	private int						idSegmentoCamara;
	private Tmct0CamaraCompensacion	tmct0CamaraCompensacion;
	private String					cdCodigo;
	private String					nbDescripcion;

	public Tmct0SegmentosCamara() {
	}

	public Tmct0SegmentosCamara( int idSegmentoCamara, String cdCodigo ) {
		this.idSegmentoCamara = idSegmentoCamara;
		this.cdCodigo = cdCodigo;
	}

	public Tmct0SegmentosCamara( int idSegmentoCamara, Tmct0CamaraCompensacion tmct0CamaraCompensacion, String cdCodigo,
			String nbDescripcion ) {
		this.idSegmentoCamara = idSegmentoCamara;
		this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
		this.cdCodigo = cdCodigo;
		this.nbDescripcion = nbDescripcion;
	}

	@Id
	@Column( name = "ID_SEGMENTO_CAMARA", unique = true, nullable = false )
	public int getIdSegmentoCamara() {
		return this.idSegmentoCamara;
	}

	public void setIdSegmentoCamara( int idSegmentoCamara ) {
		this.idSegmentoCamara = idSegmentoCamara;
	}

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "ID_CAMARA" )
	public Tmct0CamaraCompensacion getTmct0CamaraCompensacion() {
		return this.tmct0CamaraCompensacion;
	}

	public void setTmct0CamaraCompensacion( Tmct0CamaraCompensacion tmct0CamaraCompensacion ) {
		this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
	}

	@Column( name = "CD_CODIGO", nullable = false, length = 4 )
	public String getCdCodigo() {
		return this.cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	@Column( name = "NB_DESCRIPCION" )
	public String getNbDescripcion() {
		return this.nbDescripcion;
	}

	public void setNbDescripcion( String nbDescripcion ) {
		this.nbDescripcion = nbDescripcion;
	}

}
