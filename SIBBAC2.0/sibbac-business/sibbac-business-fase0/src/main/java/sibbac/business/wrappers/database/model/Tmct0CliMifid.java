package sibbac.business.wrappers.database.model;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import sibbac.database.DBConstants;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = DBConstants.WRAPPERS.MIFID)
public class Tmct0CliMifid implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8970673967653451946L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "IDMIFID", length = 8, nullable = false)
	private BigInteger id;

	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "IDCLIENT", referencedColumnName = "IDCLIENT", nullable = false)
	// Se comenta ya que la auditoria busca una tabla no agregada de historial.
	// @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@LazyCollection(LazyCollectionOption.FALSE)
	private Tmct0cli cliente;

	@Column(name = "CATEGORY", length = 15, nullable = false)
	private String category;

	@Column(name = "TCONVEN", length = 15, nullable = false)
	private String tConven;

	@Column(name = "TIDONEIDAD", length = 15, nullable = false)
	private String tIdoneid;

	@Column(name = "FHCONV", nullable = true)
	@XmlAttribute
	@Temporal(TemporalType.DATE)
	private Date fhConv;

	@Column(name = "FHIDON", nullable = true)
	@XmlAttribute
	@Temporal(TemporalType.DATE)
	private Date fhIdon;

	public Tmct0CliMifid() {

	}

	public Tmct0CliMifid(BigInteger id, String category, String tConven, String tIdoneid, Date fhConv, Date fhIdon) {
		super();
		this.id = id;
		this.category = category;
		this.tConven = tConven;
		this.tIdoneid = tIdoneid;
		this.fhConv = fhConv;
		this.fhIdon = fhIdon;

	}
	
	public Boolean isEmpty() {
		return id == null && (category == null || category.isEmpty()) && (tConven == null || tIdoneid.isEmpty()) && (fhConv == null) && (fhIdon == null);
	}

	public Long getIdCliente() {
		return cliente.getIdCliente();
	}
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Tmct0cli getCliente() {
		return cliente;
	}

	public void setCliente(Tmct0cli cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		if (category != null) category = category.trim();
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the tConven
	 */
	public String gettConven() {
		if (tConven != null) tConven = tConven.trim();
		return tConven;
	}

	/**
	 * @param tConven
	 *            the tConven to set
	 */
	public void settConven(String tConven) {
		this.tConven = tConven;
	}

	/**
	 * @return the tIdoneid
	 */
	public String gettIdoneid() {
		if (tIdoneid != null) tIdoneid = tIdoneid.trim();
		return tIdoneid;
	}

	/**
	 * @param tIdoneid
	 *            the tIdoneid to set
	 */
	public void settIdoneid(String tIdoneid) {
		this.tIdoneid = tIdoneid;
	}

	/**
	 * @return the fhConv
	 */
	public Date getFhConv() {
		return fhConv;
	}

	/**
	 * @param fhConv
	 *            the fhConv to set
	 */
	public void setFhConv(Date fhConv) {
		this.fhConv = fhConv;
	}

	/**
	 * @return the fhIdon
	 */
	public Date getFhIdon() {
		return fhIdon;
	}

	/**
	 * @param fhIdon
	 *            the fhIdon to set
	 */
	public void setFhIdon(Date fhIdon) {
		this.fhIdon = fhIdon;
	}

	public String getNameAndDescClient() {

		String retorno = "";
		if (cliente.getCdbrocli() != null)
			retorno += cliente.getCdbrocli();
		if (cliente.getDescli() != null)
			retorno += " - " + cliente.getDescli();
		return retorno;

	}
	
	public String getFhConvFormateada() {
		if (fhConv != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			return formatter.format(fhConv);
		}
		return "";
	}
	
	public String getFhIdonFormateada() {
		if (fhIdon != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			return formatter.format(fhIdon);
		}
		return "";
	}

}
