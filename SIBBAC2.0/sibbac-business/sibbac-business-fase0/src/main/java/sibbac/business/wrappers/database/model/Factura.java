package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.formula.functions.T;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Factura }. Entity: "Factura".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = WRAPPERS.FACTURA )
@Entity
@XmlRootElement
@Audited
public class Factura extends ATableAudited< Factura > implements java.io.Serializable {

	private static final long		serialVersionUID	= 6220270947704636336L;

	@OneToOne( fetch = FetchType.LAZY, optional = false )
	@JoinColumn( name = "ID_FACTURA_SUGERIDA", referencedColumnName = "ID", nullable = false )
	private FacturaSugerida			facturaSugerida;

	@Column( name = "FHFECHAFAC", nullable = false )
	@XmlAttribute
	@Temporal( TemporalType.TIMESTAMP )
	private Date					fechaFactura;

	@Column( name = "NBDOCNUMERO", nullable = false, unique = true )
	@XmlAttribute
	private Long					numeroFactura;

	@Column( name = "NBNOMBREALIAS", nullable = false, length = 130 )
	@XmlAttribute
	private String					nombreAlias;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_TIPODOCUMENTO", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private TipoDeDocumento			tipoDeDocumento;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ESTADO", nullable = false, referencedColumnName = "IDESTADO" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Tmct0estado				estado;

	@Column( name = "FHFECHACRE", nullable = false )
	@XmlAttribute
	@Temporal( TemporalType.DATE )
	private Date					fechaCreacion;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_MONEDA", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private Monedas					moneda;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_PERIODO", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private Periodos				periodo;

	@Column( name = "NBCIF", nullable = false, length = 32 )
	@XmlAttribute
	private String					cif;

	@Column( name = "NBDIRECCION", nullable = false, length = 500 )
	@XmlAttribute
	private String					direccion;

	@Column( name = "NBCOMENTARIOS", length = 500 )
	@XmlAttribute
	private String					comentarios;

	@Column( name = "PCPORCENTAJE", scale = 6, precision = 19 )
	@XmlAttribute
	private Double					porcentaje;

	@ManyToOne( optional = false )
	@JoinColumn( name = "CDESTADOCONT" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	private Tmct0estado				estadocont;

	@OneToMany( mappedBy = "factura" )
	private List< LineaDeCobro >	lineasDeCobro;
	
	@Column( name = "ENVIADO" )
	@XmlAttribute
	protected Boolean enviado;

	public Factura() {
		super();
		this.fechaCreacion = new Date();
	}

	public Contacto getContactoDefecto() {
		Contacto contacto = null;
		final Alias alias = getAlias();
		if ( alias != null ) {
			contacto = alias.getContactoDefecto();
		}
		return contacto;
	}

	public List< Contacto > getContactosGestores() {
		List< Contacto > listaContactosGestores = null;
		final Alias alias = getAlias();
		if ( alias != null ) {
			listaContactosGestores = alias.getContactosGestores();
		}
		return listaContactosGestores;
	}

	@Transactional public List< AlcOrdenes > getAlcOrdenes() {
		List< AlcOrdenes > ordenes = new ArrayList< AlcOrdenes >();
		if ( facturaSugerida != null ) {
			ordenes.addAll( facturaSugerida.getAlcOrdenes() );
		}

		return ordenes;
	}

	public List< AlcOrdenDetalleFactura > getAlcOrdenFacturaDetalles() {
		List< AlcOrdenDetalleFactura > alcOrdenFacturaDetalles = new ArrayList< AlcOrdenDetalleFactura >();
		if ( facturaSugerida != null ) {
			alcOrdenFacturaDetalles.addAll( facturaSugerida.getAlcOrdenFacturaDetalles() );
		}

		return alcOrdenFacturaDetalles;
	}

	public List< Tmct0alcId > getAlcIds() {
		List< Tmct0alcId > alcs = new ArrayList<>();
		if ( facturaSugerida != null ) {
			alcs.addAll( facturaSugerida.getAlcIds() );
		}
		return alcs;
	}

	/**
	 * Calcula la base imponible
	 * 
	 * @return
	 */
	public BigDecimal getImfinsvb() {
		return facturaSugerida.getImfinsvb();
	}

	public Date getFechaInicio() {
		Date fechaInicio = null;
		final FacturaSugerida facturaSugerida = getFacturaSugerida();
		if ( facturaSugerida != null ) {
			fechaInicio = facturaSugerida.getFechaInicio();
		}
		return fechaInicio;
	}

	public Date getFechaFin() {
		Date fechaFin = null;
		final FacturaSugerida facturaSugerida = getFacturaSugerida();
		if ( facturaSugerida != null ) {
			fechaFin = facturaSugerida.getFechaFin();
		}
		return fechaFin;
	}

	/**
	 * Calcula el importe total de los impuestos
	 * 
	 * @return
	 */
	public BigDecimal getImporteImpuestos() {
		final BigDecimal imfinsvb = getImfinsvb();
		BigDecimal importeImpuestos = null;
		if ( imfinsvb != null && porcentaje != null ) {
			BigDecimal porcentajebd = new BigDecimal( porcentaje );
			importeImpuestos = imfinsvb.multiply( porcentajebd ).divide( new BigDecimal( 100 ) );
		}
		return importeImpuestos;
	}

	/**
	 * Añade una linea a la lista de facturas existentes
	 * 
	 * @param facturaDetalle
	 * @return
	 */
	public boolean addLineaDeCobro( final LineaDeCobro lineaDeCobro ) {
		if ( CollectionUtils.isEmpty( lineasDeCobro ) ) {
			lineasDeCobro = new ArrayList< LineaDeCobro >();
		}
		lineaDeCobro.setFactura( this );

		return lineasDeCobro.add( lineaDeCobro );
	}

	/**
	 * Calcula el importe total de la factura (con impuestos)
	 * 
	 * @return
	 */
	public BigDecimal getImporteTotal() {
		final BigDecimal importeTotal = getImfinsvb().add( getImporteImpuestos() );

		return importeTotal;
	}

	public BigDecimal getImportePagado() {
		BigDecimal importePagado = BigDecimal.ZERO;
		final List< LineaDeCobro > lineasDeCobro = getLineasDeCobro();
		if ( CollectionUtils.isNotEmpty( lineasDeCobro ) ) {
			BigDecimal importe = BigDecimal.ZERO;
			for ( final LineaDeCobro lineaDeCobro : lineasDeCobro ) {
				importe = lineaDeCobro.getImporteAplicado();
				importePagado = importePagado.add( importe );
			}
		}
		return importePagado;
	}

	public BigDecimal getImportePendiente() {
		return getImporteTotal().subtract( getImportePagado() );
	}

	/**
	 * @return
	 */
	public FacturaDetalle getDetalleFactura( final Integer numero ) {
		FacturaDetalle lineaEncontrada = null;
		final FacturaSugerida facturaSugerida = getFacturaSugerida();
		if ( facturaSugerida != null ) {
			lineaEncontrada = facturaSugerida.getDetalleFactura( numero );
		}
		return lineaEncontrada;
	}

	/**
	 * @return
	 */
	public List< FacturaDetalle > getDetallesFactura() {
		final FacturaSugerida facturaSugerida = getFacturaSugerida();
		List< FacturaDetalle > detallesFactura = new ArrayList< FacturaDetalle >();
		if ( facturaSugerida != null ) {
			detallesFactura = facturaSugerida.getDetallesFactura();
		}
		return detallesFactura;
	}

	/**
	 * @return the facturaSugerida
	 */
	public FacturaSugerida getFacturaSugerida() {
		return facturaSugerida;
	}

	/**
	 * @return the alias
	 */
	public Long getIdAlias() {
		Long idAlias = null;
		final Alias alias = getAlias();
		if ( alias != null ) {
			idAlias = alias.getId();
		}
		return idAlias;
	}

	/**
	 * @return the alias
	 */
	public String getCdaliass() {
		String cdaliass = null;
		final Alias alias = getAlias();
		if ( alias != null ) {
			cdaliass = alias.getCdaliass();
		}
		return cdaliass;
	}

	/**
	 * @return the alias
	 */
	public Alias getAlias() {
		Alias alias = null;
		if ( facturaSugerida != null ) {
			alias = facturaSugerida.getAlias();
		}
		return alias;
	}

	/**
	 * @return the fechaFactura
	 */
	public Date getFechaFactura() {
		return this.fechaFactura;
	}

	/**
	 * @return the numeroFactura
	 */
	public Long getNumeroFactura() {
		return this.numeroFactura;
	}

	/**
	 * @return the nombreAlias
	 */
	public String getNombreAlias() {
		return this.nombreAlias;
	}

	/**
	 * @return the tipoDeDocumento
	 */
	public TipoDeDocumento getTipoDeDocumento() {
		return this.tipoDeDocumento;
	}

	/**
	 * @return the estado
	 */
	public Tmct0estado getEstado() {
		return this.estado;
	}

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	/**
	 * @return the moneda
	 */
	public Monedas getMoneda() {
		return this.moneda;
	}

	/**
	 * @return the periodo
	 */
	public Periodos getPeriodo() {
		return this.periodo;
	}

	/**
	 * @return the cif
	 */
	public String getCif() {
		return this.cif;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return this.direccion;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return this.comentarios;
	}

	/**
	 * @return the porcentaje
	 */
	public Double getPorcentaje() {
		return this.porcentaje;
	}

	public Tmct0estado getEstadocont() {
		return estadocont;
	}

	/**
	 * @param facturaSugerida
	 *            the facturaSugerida to set
	 */
	public void setFacturaSugerida( FacturaSugerida facturaSugerida ) {
		this.facturaSugerida = facturaSugerida;
	}

	/**
	 * @param fechaFactura
	 *            the fechaFactura to set
	 */
	public void setFechaFactura( Date fechaFactura ) {
		this.fechaFactura = fechaFactura;
	}

	/**
	 * @param numeroFactura
	 *            the numeroFactura to set
	 */
	public void setNumeroFactura( Long numeroFactura ) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @param nombreAlias
	 *            the nombreAlias to set
	 */
	public void setNombreAlias( String nombreAlias ) {
		this.nombreAlias = nombreAlias;
	}

	/**
	 * @param tipoDeDocumento
	 *            the tipoDeDocumento to set
	 */
	public void setTipoDeDocumento( TipoDeDocumento tipoDeDocumento ) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado( Tmct0estado estado ) {
		this.estado = estado;
	}

	/**
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion( Date fechaCreacion ) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda( Monedas moneda ) {
		this.moneda = moneda;
	}

	/**
	 * @param periodo
	 *            the periodo to set
	 */
	public void setPeriodo( Periodos periodo ) {
		this.periodo = periodo;
	}

	/**
	 * @param cif
	 *            the cif to set
	 */
	public void setCif( String cif ) {
		this.cif = cif;
	}

	/**
	 * @param direccion
	 *            the direccion to set
	 */
	public void setDireccion( String direccion ) {
		this.direccion = direccion;
	}

	/**
	 * @param comentarios
	 *            the comentarios to set
	 */
	public void setComentarios( String comentarios ) {
		this.comentarios = comentarios;
	}

	/**
	 * @param porcentaje
	 *            the porcentaje to set
	 */
	public void setPorcentaje( Double porcentaje ) {
		this.porcentaje = porcentaje;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/**
	 * @return the lineasDeCobro
	 */
	public List< LineaDeCobro > getLineasDeCobro() {
		return lineasDeCobro;
	}

	/**
	 * @param lineasDeCobro
	 *            the lineasDeCobro to set
	 */
	public void setLineasDeCobro( List< LineaDeCobro > lineasDeCobro ) {
		this.lineasDeCobro = lineasDeCobro;
	}

	public void setEstadocont( Tmct0estado estadocont ) {
		this.estadocont = estadocont;
	}

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	@Override
	public int compareTo( final Factura other ) {
		// Si el "padre" ya es distinto...
		int father = super.compareTo( other );
		if ( father != 0 ) {
			return father;
		}
		// Entity concrete fields.
		int internal = 0;
		return ( internal == 0 ) ? 0 : -3;
	}

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[FACTURA==" + this.id + "]";
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}

	public boolean isRectificada() {
		return EstadosEnumerados.FACTURA.RECTIFICADA.getId().equals(estado);
	}

}
