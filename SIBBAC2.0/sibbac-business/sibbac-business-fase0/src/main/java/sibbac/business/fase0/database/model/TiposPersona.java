package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_TIPOS_PERSONA" )
public class TiposPersona extends MasterData< TiposPersona > {

	private static final long	serialVersionUID	= 8174769994380425582L;

}
