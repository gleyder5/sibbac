package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0MON. TABLA DE MONEDAS Documentación de la tablaTMCT0MON <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TMCT0MON")
public class Tmct0mon implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -3653535371714242276L;

  /**
   * Table Field CDMONISO. Columna importada CDMONISO Documentación columna <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Id
  protected java.lang.String cdmoniso;

  /**
   * Table Field DESCRMON. Columna importada DESCRMON Documentación columna <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(name = "DESCRMON", length = 50, nullable = false)
  protected java.lang.String descrmon;
  /**
   * Table Field CDESTADO. Columna importada CDESTADO Documentación columna <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(name = "CDESTADO", length = 1, nullable = false)
  protected Character cdestado;
  /**
   * Table Field CDNUMDEC. Columna importada CDNUMDEC Documentación columna <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(name = "CDNUMDEC", length = 2, scale = 0, nullable = false)
  protected Integer cdnumdec;
  /**
   * Table Field FHAUDIT. Columna importada FHAUDIT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field CDUSUAUD. Columna importada CDUSUAUD Documentación columna <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field NUVERSION. Columna importada NUVERSION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(name = "NUVERSION")
  protected Integer nuversion;

  public java.lang.String getCdmoniso() {
    return cdmoniso;
  }

  public java.lang.String getDescrmon() {
    return descrmon;
  }

  public Character getCdestado() {
    return cdestado;
  }

  public Integer getCdnumdec() {
    return cdnumdec;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Integer getNuversion() {
    return nuversion;
  }

  public void setCdmoniso(java.lang.String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setDescrmon(java.lang.String descrmon) {
    this.descrmon = descrmon;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  public void setCdnumdec(Integer cdnumdec) {
    this.cdnumdec = cdnumdec;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Override
  public String toString() {
    return String.format("Tmct0mon [cdmoniso=%s, descrmon=%s, cdnumdec=%s]", cdmoniso, descrmon, cdnumdec);
  }

}
