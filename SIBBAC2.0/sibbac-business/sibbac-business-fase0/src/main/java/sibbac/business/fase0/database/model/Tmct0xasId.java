package sibbac.business.fase0.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0xasId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 97971845779407492L;
	private String				nbcamxml			= "";
	private String				inforxml			= "";
	private String				nbcamas4			= "";

	public Tmct0xasId() {
	}

	public Tmct0xasId( String nbcamxml, String inforxml, String nbcamas4 ) {
		this.nbcamxml = nbcamxml;
		this.inforxml = inforxml;
		this.nbcamas4 = nbcamas4;
	}

	@Column( name = "NBCAMXML", nullable = false, length = 30 )
	public String getNbcamxml() {
		return this.nbcamxml;
	}

	public void setNbcamxml( String nbcamxml ) {
		this.nbcamxml = nbcamxml;
	}

	@Column( name = "INFORXML", nullable = false, length = 30 )
	public String getInforxml() {
		return this.inforxml;
	}

	public void setInforxml( String inforxml ) {
		this.inforxml = inforxml;
	}

	@Column( name = "NBCAMAS4", nullable = false, length = 30 )
	public String getNbcamas4() {
		return this.nbcamas4;
	}

	public void setNbcamas4( String nbcamas4 ) {
		this.nbcamas4 = nbcamas4;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0xasId ) )
			return false;
		Tmct0xasId castOther = ( Tmct0xasId ) other;

		return ( ( this.getNbcamxml() == castOther.getNbcamxml() ) || ( this.getNbcamxml() != null && castOther.getNbcamxml() != null && this
				.getNbcamxml().equals( castOther.getNbcamxml() ) ) )
				&& ( ( this.getInforxml() == castOther.getInforxml() ) || ( this.getInforxml() != null && castOther.getInforxml() != null && this
						.getInforxml().equals( castOther.getInforxml() ) ) )
				&& ( ( this.getNbcamas4() == castOther.getNbcamas4() ) || ( this.getNbcamas4() != null && castOther.getNbcamas4() != null && this
						.getNbcamas4().equals( castOther.getNbcamas4() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getNbcamxml() == null ? 0 : this.getNbcamxml().hashCode() );
		result = 37 * result + ( getInforxml() == null ? 0 : this.getInforxml().hashCode() );
		result = 37 * result + ( getNbcamas4() == null ? 0 : this.getNbcamas4().hashCode() );
		return result;
	}

}
