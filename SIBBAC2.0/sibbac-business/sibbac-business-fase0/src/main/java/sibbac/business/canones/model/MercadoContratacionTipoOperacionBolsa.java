package sibbac.business.canones.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;

@Entity
@Table(name = "TMCT0_MERCADO_CONTRATACION_TIPO_OPERACION_BOLSA")
public class MercadoContratacionTipoOperacionBolsa 
    extends DescriptedCanonEntity<MercadoContratacionTipoOperacionBolsaId> {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -7844881727710736944L;
  
  @EmbeddedId
  public MercadoContratacionTipoOperacionBolsaId id;
  
  @Override
  public MercadoContratacionTipoOperacionBolsaId getId() {
    return id;
  }
  
  @Override
  public void setId(MercadoContratacionTipoOperacionBolsaId id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    MercadoContratacionTipoOperacionBolsa other = (MercadoContratacionTipoOperacionBolsa) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return toString("tipo operacion bolsa");
  }

}
