package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0_MIFIDCATEGORIA")
public class Tmct0MifidCategoria implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private Tmct0MifidCategoriaId id;

	@Column(name = "FECINI", nullable = true, length = 4)
	private Date fecini;

	@Column(name = "FECFIN", nullable = true, length = 4)
	private Date fecfin;

	@Column(name = "CDSEGMEN", nullable = true, length = 2)
	private String cdsegmen;

	@Column(name = "CODSEG", nullable = true, length = 2)
	private String codseg;

	@Column(name = "CRITSEGM", nullable = true, length = 3)
	private String critsegm;

	@Column(name = "ESTADCG", nullable = true, length = 2)
	private String estadcg;

	@Column(name = "NRODOMIC", nullable = true, length = 8)
	private long nrodomic;

	@Column(name = "IDDISPO", nullable = true, length = 2)
	private String iddispo;

	@Column(name = "JNROSEC", nullable = true, length = 8)
	private long jnrosec;

	@Column(name = "COMENTS", nullable = true, length = 200)
	private String coments;

	@Column(name = "FECREVIS", nullable = true, length = 4)
	private Date fecrevis;

	@Column(name = "FECALTSI", nullable = true, length = 4)
	private Date fecaltsi;

	@Column(name = "USUULTAC", nullable = true, length = 8)
	private String usuualtac;

	@Column(name = "EMULTACT", nullable = true, length = 4)
	private String emultact;

	@Column(name = "CEULTACT", nullable = true, length = 4)
	private String ceultact;

	@Column(name = "FEULTACT", nullable = true, length = 4)
	private Date feultact;
	
	@Temporal( TemporalType.TIME )
	@Column(name = "TIMULTAC", nullable = true, length = 3)
	private Date timultac;

	public Tmct0MifidCategoria() {
	}

	public Tmct0MifidCategoria(Tmct0MifidCategoriaId id, Date fecini,
			Date fecfin, String cdsegmen, String codseg, String critsegm,
			String estadcg, long nrodomic, String iddispo, long jnrosec,
			String coments, Date fecrevis, Date fecaltsi, String usuualtac,
			String emultact, String ceultact, Date feultact, Date timultac) {
		super();
		this.id = id;
		this.fecini = fecini;
		this.fecfin = fecfin;
		this.cdsegmen = cdsegmen;
		this.codseg = codseg;
		this.critsegm = critsegm;
		this.estadcg = estadcg;
		this.nrodomic = nrodomic;
		this.iddispo = iddispo;
		this.jnrosec = jnrosec;
		this.coments = coments;
		this.fecrevis = fecrevis;
		this.fecaltsi = fecaltsi;
		this.usuualtac = usuualtac;
		this.emultact = emultact;
		this.ceultact = ceultact;
		this.feultact = feultact;
		this.timultac = timultac;
	}

	public Tmct0MifidCategoriaId getId() {
		return id;
	}

	public void setId(Tmct0MifidCategoriaId id) {
		this.id = id;
	}

	public Date getFecini() {
		return fecini;
	}

	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}

	public Date getFecfin() {
		return fecfin;
	}

	public void setFecfin(Date fecfin) {
		this.fecfin = fecfin;
	}

	public String getCdsegmen() {
		return cdsegmen;
	}

	public void setCdsegmen(String cdsegmen) {
		this.cdsegmen = cdsegmen;
	}

	public String getCodseg() {
		return codseg;
	}

	public void setCodseg(String codseg) {
		this.codseg = codseg;
	}

	public String getCritsegm() {
		return critsegm;
	}

	public void setCritsegm(String critsegm) {
		this.critsegm = critsegm;
	}

	public String getEstadcg() {
		return estadcg;
	}

	public void setEstadcg(String estadcg) {
		this.estadcg = estadcg;
	}

	public long getNrodomic() {
		return nrodomic;
	}

	public void setNrodomic(long nrodomic) {
		this.nrodomic = nrodomic;
	}

	public String getIddispo() {
		return iddispo;
	}

	public void setIddispo(String iddispo) {
		this.iddispo = iddispo;
	}

	public long getJnrosec() {
		return jnrosec;
	}

	public void setJnrosec(long jnrosec) {
		this.jnrosec = jnrosec;
	}

	public String getComents() {
		return coments;
	}

	public void setComents(String coments) {
		this.coments = coments;
	}

	public Date getFecrevis() {
		return fecrevis;
	}

	public void setFecrevis(Date fecrevis) {
		this.fecrevis = fecrevis;
	}

	public Date getFecaltsi() {
		return fecaltsi;
	}

	public void setFecaltsi(Date fecaltsi) {
		this.fecaltsi = fecaltsi;
	}

	public String getUsuualtac() {
		return usuualtac;
	}

	public void setUsuualtac(String usuualtac) {
		this.usuualtac = usuualtac;
	}

	public String getEmultact() {
		return emultact;
	}

	public void setEmultact(String emultact) {
		this.emultact = emultact;
	}

	public String getCeultact() {
		return ceultact;
	}

	public void setCeultact(String ceultact) {
		this.ceultact = ceultact;
	}

	public Date getFeultact() {
		return feultact;
	}

	public void setFeultact(Date feultact) {
		this.feultact = feultact;
	}

	public Date getTimultac() {
		return timultac;
	}

	public void setTimultac(Date timultac) {
		this.timultac = timultac;
	}

}
