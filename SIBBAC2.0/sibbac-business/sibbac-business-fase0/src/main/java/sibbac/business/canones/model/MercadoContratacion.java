package sibbac.business.canones.model;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Mercado de Contratación
 */
@Entity
@Table(name = "TMCT0_MERCADO_CONTRATACION")
public class MercadoContratacion extends DescriptedCanonEntity<String> {

  private static final String REF_AGRUPACION = "ID_MERCADO_AGRUPACION";

  static final String REF_MERCADO = "ID_MERCADO_CONTRATACION";

  /**
   * serial number
   */
  private static final long serialVersionUID = -7609509922415244864L;

  @Id
  @Column(name = ID_COLUMN_NAME, length = 4, nullable = false)
  private String id;

  @Column(name = "ADMITE_REGLAS_CANON", length = 1, nullable = false)
  private char admiteReglasCanon = 'S';

  @ManyToOne(optional = true)
  @JoinColumn(name = REF_AGRUPACION, referencedColumnName = ID_COLUMN_NAME)
  private MercadoContratacion mercadoAgrupacion;

  @OneToMany
  @JoinColumn(name = REF_MERCADO, referencedColumnName = ID_COLUMN_NAME)
  private List<MercadoContratacionSegmento> segmentos;

  @OneToMany
  @JoinColumn(name = REF_MERCADO, referencedColumnName = ID_COLUMN_NAME)
  private List<MercadoContratacionTipoOperacionBolsa> tiposOperacionBolsa;

  @OneToMany
  @JoinColumn(name = REF_MERCADO, referencedColumnName = ID_COLUMN_NAME)
  private List<MercadoContratacionTipoOrden> tiposOrden;

  @OneToMany
  @JoinColumn(name = REF_MERCADO, referencedColumnName = ID_COLUMN_NAME)
  private List<MercadoContratacionRestriccionEjecucion> restriccionesEjecucion;

  @OneToMany
  @JoinColumn(name = REF_MERCADO, referencedColumnName = ID_COLUMN_NAME)
  private List<ReglasCanones> reglasCanones;

  public MercadoContratacion() {
    segmentos = new ArrayList<>();
    tiposOperacionBolsa = new ArrayList<>();
    tiposOrden = new ArrayList<>();
    restriccionesEjecucion = new ArrayList<>();
    reglasCanones = new ArrayList<>();
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public MercadoContratacion getMercadoAgrupacion() {
    return mercadoAgrupacion;
  }

  public void setMercadoAgrupacion(MercadoContratacion mercadoAgrupacion) {
    this.mercadoAgrupacion = mercadoAgrupacion;
  }

  public List<MercadoContratacionSegmento> getSegmentos() {
    return segmentos;
  }

  public void setSegmentos(List<MercadoContratacionSegmento> segmentos) {
    this.segmentos = segmentos;
  }

  public List<MercadoContratacionTipoOperacionBolsa> getTiposOperacionBolsa() {
    return tiposOperacionBolsa;
  }

  public void setTiposOperacionBolsa(List<MercadoContratacionTipoOperacionBolsa> tiposOperacionBolsa) {
    this.tiposOperacionBolsa = tiposOperacionBolsa;
  }

  public List<MercadoContratacionRestriccionEjecucion> getRestriccionesEjecucion() {
    return restriccionesEjecucion;
  }

  public void setRestriccionesEjecucion(List<MercadoContratacionRestriccionEjecucion> restriccionesEjecucion) {
    this.restriccionesEjecucion = restriccionesEjecucion;
  }

  public List<ReglasCanones> getReglasCanones() {
    return reglasCanones;
  }

  public void setReglasCanones(List<ReglasCanones> reglasCanones) {
    this.reglasCanones = reglasCanones;
  }

  public char getAdmiteReglasCanon() {
    return admiteReglasCanon;
  }

  public void setAdmiteReglasCanon(char admiteReglasCanon) {
    this.admiteReglasCanon = admiteReglasCanon;
  }

  @Override
  public String toString() {
    return toString("Mercado contratación");
  }

}
