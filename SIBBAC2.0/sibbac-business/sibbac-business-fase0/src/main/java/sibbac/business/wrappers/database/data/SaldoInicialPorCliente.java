package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;


public class SaldoInicialPorCliente {

	private String					alias;
	private String					descralias;
	private List< SaldoInicial >	grupoBalanceClienteList;
	private BigDecimal				totalEfectivos;

	public List< SaldoInicial > getGrupoBalanceClienteList() {
		return grupoBalanceClienteList;
	}

	public void addGrupoBalanceCliente( SaldoInicial grupo ) {
		if ( this.grupoBalanceClienteList == null ) {
			grupoBalanceClienteList = new LinkedList< SaldoInicial >();
		}
		this.grupoBalanceClienteList.add( grupo );
	}

	public void setGrupoBalanceClienteList( List< SaldoInicial > grupoBalanceClienteList ) {
		this.grupoBalanceClienteList = grupoBalanceClienteList;
	}

	public BigDecimal getTotalEfectivos() {
		return totalEfectivos;
	}

	public void setTotalEfectivos( BigDecimal totalEfectivos ) {
		this.totalEfectivos = totalEfectivos;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias( String alias ) {
		this.alias = alias;
	}

	public String getDescralias() {
		return descralias;
	}

	public void setDescralias( String descralias ) {
		this.descralias = descralias;
	}

}
