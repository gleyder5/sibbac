
package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0ANOTACIONECC")
public class Tmct0anotacionecc implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 4588989991929166049L;
  private Long idanotacionecc;
  private Tmct0movimientoecc tmct0movimientoecc;
  private String cdoperacionecc;
  private char cdsentido;
  private char cdindposicion;
  private char cdoperacion;

  private Date fcontratacion;
  private Date fregistroecc;
  private BigDecimal imprecio;
  private BigDecimal imefectivo;
  private BigDecimal imtitulosdisponibles;
  private BigDecimal imefectivodisponible;
  private BigDecimal imtitulosretenidos;
  private BigDecimal imefectivoretenido;
  private String nuoperacionprev;
  private String nuoperacioninic;
  private String cdrefcomun;
  private String cdcamara;
  private String cdsegmento;
  private String nuexemkt;
  private String cdoperacionmkt;
  private String cdmiembromkt;
  private char cdindcotizacion;
  private String cdisin;
  private String nuordenmkt;
  private String cdrefcliente;
  private String cdrefextorden;
  private char cdindcapacidad;
  private String nbordenmkt;
  private Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion;
  private String cdmovimientoecc;
  private Date fliqteorica;
  private Date auditFechaCambio;
  private String auditUser;
  private Date hregistroecc;
  private Date hnegociacion;
  private Date fordenmkt;
  private Date hordenmkt;
  private Date fnegociacion;
  private BigDecimal nutitulos;
  private String cdPlataformaNegociacion;
  private BigDecimal corretaje;

  private String entidadParticipante;
  private String cuentaLiquidacion;

  /**
   * CUANDO ES UN GIVE UP
   * */
  private Tmct0Compensador compensador;
  private String cdrefasigext;

  private char envios3;

  public Tmct0anotacionecc() {
  }

  public Tmct0anotacionecc(Long idanotacionecc, String cdoperacionecc, char cdsentido, char cdindposicion,
      char cdoperacion, String cdrefasigext, Date fcontratacion, Date fregistroecc, BigDecimal imprecio,
      String nuoperacioninic, String cdrefcomun, String cdcamara, String cdsegmento, String nuexemkt,
      String cdoperacionmkt, String cdmiembromkt, char cdindcotizacion, String cdisin, String nuordenmkt,
      char cdindcapacidad, Date hregistroecc, Date hnegociacion, Date fordenmkt, Date hordenmkt, Date fnegociacion,
      String cdPlataformaNegociacion, BigDecimal corretaje) {
    this.idanotacionecc = idanotacionecc;
    this.cdoperacionecc = cdoperacionecc;
    this.cdsentido = cdsentido;
    this.cdindposicion = cdindposicion;
    this.cdoperacion = cdoperacion;
    this.cdrefasigext = cdrefasigext;
    this.fcontratacion = fcontratacion;
    this.fregistroecc = fregistroecc;
    this.imprecio = imprecio;
    this.nuoperacioninic = nuoperacioninic;
    this.cdrefcomun = cdrefcomun;
    this.cdcamara = cdcamara;
    this.cdsegmento = cdsegmento;
    this.nuexemkt = nuexemkt;
    this.cdoperacionmkt = cdoperacionmkt;
    this.cdmiembromkt = cdmiembromkt;
    this.cdindcotizacion = cdindcotizacion;
    this.cdisin = cdisin;
    this.nuordenmkt = nuordenmkt;
    this.cdindcapacidad = cdindcapacidad;
    this.hregistroecc = hregistroecc;
    this.hnegociacion = hnegociacion;
    this.fordenmkt = fordenmkt;
    this.hordenmkt = hordenmkt;
    this.fnegociacion = fnegociacion;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.corretaje = corretaje;
  }

  public Tmct0anotacionecc(Long idanotacionecc, Tmct0movimientoecc tmct0movimientoecc, String cdoperacionecc,
      char cdsentido, char cdindposicion, char cdoperacion, String cdrefasigext, Date fcontratacion, Date fregistroecc,
      BigDecimal imprecio, BigDecimal imefectivo, BigDecimal imtitulosdisponibles, BigDecimal imefectivodisponible,
      BigDecimal imtitulosretenidos, BigDecimal imefectivoretenido, String nuoperacionprev, String nuoperacioninic,
      String cdrefcomun, String cdcamara, String cdsegmento, String nuexemkt, String cdoperacionmkt,
      String cdmiembromkt, char cdindcotizacion, String cdisin, String nuordenmkt, String cdrefcliente,
      String cdrefextorden, char cdindcapacidad, String nbordenmkt, Tmct0CuentasDeCompensacion idcuentacomp,
      String cdmovimientoecc, Date fliqteorica, Date auditFechaCambio, String auditUser, Date hregistroecc,
      Date hnegociacion, Date fordenmkt, Date hordenmkt, Date fnegociacion, BigDecimal nutitulos,
      String cdPlataformaNegociacion, BigDecimal corretaje) {
    this.idanotacionecc = idanotacionecc;
    this.tmct0movimientoecc = tmct0movimientoecc;
    this.cdoperacionecc = cdoperacionecc;
    this.cdsentido = cdsentido;
    this.cdindposicion = cdindposicion;
    this.cdoperacion = cdoperacion;
    this.cdrefasigext = cdrefasigext;
    this.fcontratacion = fcontratacion;
    this.fregistroecc = fregistroecc;
    this.imprecio = imprecio;
    this.imefectivo = imefectivo;
    this.imtitulosdisponibles = imtitulosdisponibles;
    this.imefectivodisponible = imefectivodisponible;
    this.imtitulosretenidos = imtitulosretenidos;
    this.imefectivoretenido = imefectivoretenido;
    this.nuoperacionprev = nuoperacionprev;
    this.nuoperacioninic = nuoperacioninic;
    this.cdrefcomun = cdrefcomun;
    this.cdcamara = cdcamara;
    this.cdsegmento = cdsegmento;
    this.nuexemkt = nuexemkt;
    this.cdoperacionmkt = cdoperacionmkt;
    this.cdmiembromkt = cdmiembromkt;
    this.cdindcotizacion = cdindcotizacion;
    this.cdisin = cdisin;
    this.nuordenmkt = nuordenmkt;
    this.cdrefcliente = cdrefcliente;
    this.cdrefextorden = cdrefextorden;
    this.cdindcapacidad = cdindcapacidad;
    this.nbordenmkt = nbordenmkt;
    this.tmct0CuentasDeCompensacion = idcuentacomp;
    this.cdmovimientoecc = cdmovimientoecc;
    this.fliqteorica = fliqteorica;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.hregistroecc = hregistroecc;
    this.hnegociacion = hnegociacion;
    this.fordenmkt = fordenmkt;
    this.hordenmkt = hordenmkt;
    this.fnegociacion = fnegociacion;
    this.nutitulos = nutitulos;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.corretaje = corretaje;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDANOTACIONECC", unique = true, nullable = false, precision = 8)
  public Long getIdanotacionecc() {
    return this.idanotacionecc;
  }

  public void setIdanotacionecc(Long idanotacionecc) {
    this.idanotacionecc = idanotacionecc;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDMOVIMIENTOECC")
  public Tmct0movimientoecc getTmct0movimientoecc() {
    return this.tmct0movimientoecc;
  }

  public void setTmct0movimientoecc(Tmct0movimientoecc tmct0movimientoecc) {
    this.tmct0movimientoecc = tmct0movimientoecc;
  }

  @Column(name = "CDOPERACIONECC", unique = true, nullable = false, length = 16)
  public String getCdoperacionecc() {
    return this.cdoperacionecc;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  @Column(name = "CDSENTIDO", nullable = false, length = 1)
  public char getCdsentido() {
    return this.cdsentido;
  }

  public void setCdsentido(char cdsentido) {
    this.cdsentido = cdsentido;
  }

  @Column(name = "CDINDPOSICION", nullable = false, length = 1)
  public char getCdindposicion() {
    return this.cdindposicion;
  }

  public void setCdindposicion(char cdindposicion) {
    this.cdindposicion = cdindposicion;
  }

  @Column(name = "CDOPERACION", nullable = false, length = 1)
  public char getCdoperacion() {
    return this.cdoperacion;
  }

  public void setCdoperacion(char cdoperacion) {
    this.cdoperacion = cdoperacion;
  }

  @Column(name = "CDREFASIGEXT", nullable = false, length = 16)
  public String getCdrefasigext() {
    return this.cdrefasigext;
  }

  public void setCdrefasigext(String cdrefasigext) {
    this.cdrefasigext = cdrefasigext;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FCONTRATACION", nullable = false, length = 4)
  public Date getFcontratacion() {
    return this.fcontratacion;
  }

  public void setFcontratacion(Date fcontratacion) {
    this.fcontratacion = fcontratacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FREGISTROECC", nullable = false, length = 4)
  public Date getFregistroecc() {
    return this.fregistroecc;
  }

  public void setFregistroecc(Date fregistroecc) {
    this.fregistroecc = fregistroecc;
  }

  @Column(name = "IMPRECIO", nullable = false, precision = 13, scale = 6)
  public BigDecimal getImprecio() {
    return this.imprecio;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  @Column(name = "IMEFECTIVO", precision = 15, scale = 2)
  public BigDecimal getImefectivo() {
    return this.imefectivo;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  @Column(name = "IMTITULOSDISPONIBLES", precision = 18, scale = 6)
  public BigDecimal getImtitulosdisponibles() {
    return this.imtitulosdisponibles;
  }

  public void setImtitulosdisponibles(BigDecimal imtitulosdisponibles) {
    this.imtitulosdisponibles = imtitulosdisponibles;
  }

  @Column(name = "IMEFECTIVODISPONIBLE", precision = 15, scale = 2)
  public BigDecimal getImefectivodisponible() {
    return this.imefectivodisponible;
  }

  public void setImefectivodisponible(BigDecimal imefectivodisponible) {
    this.imefectivodisponible = imefectivodisponible;
  }

  @Column(name = "IMTITULOSRETENIDOS", precision = 18, scale = 6)
  public BigDecimal getImtitulosretenidos() {
    return this.imtitulosretenidos;
  }

  public void setImtitulosretenidos(BigDecimal imtitulosretenidos) {
    this.imtitulosretenidos = imtitulosretenidos;
  }

  @Column(name = "IMEFECTIVORETENIDO", precision = 15, scale = 2)
  public BigDecimal getImefectivoretenido() {
    return this.imefectivoretenido;
  }

  public void setImefectivoretenido(BigDecimal imefectivoretenido) {
    this.imefectivoretenido = imefectivoretenido;
  }

  @Column(name = "NUOPERACIONPREV", length = 16)
  public String getNuoperacionprev() {
    return this.nuoperacionprev;
  }

  public void setNuoperacionprev(String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  @Column(name = "NUOPERACIONINIC", nullable = false, length = 16)
  public String getNuoperacioninic() {
    return this.nuoperacioninic;
  }

  public void setNuoperacioninic(String nuoperacioninic) {
    this.nuoperacioninic = nuoperacioninic;
  }

  @Column(name = "CDREFCOMUN", nullable = false, length = 16)
  public String getCdrefcomun() {
    return this.cdrefcomun;
  }

  public void setCdrefcomun(String cdrefcomun) {
    this.cdrefcomun = cdrefcomun;
  }

  @Column(name = "CDCAMARA", nullable = false, length = 11)
  public String getCdcamara() {
    return this.cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  @Column(name = "CDSEGMENTO", nullable = false, length = 2)
  public String getCdsegmento() {
    return this.cdsegmento;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  @Column(name = "NUEXEMKT", nullable = false, length = 9)
  public String getNuexemkt() {
    return this.nuexemkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  @Column(name = "CDOPERACIONMKT", nullable = false, length = 2)
  public String getCdoperacionmkt() {
    return this.cdoperacionmkt;
  }

  public void setCdoperacionmkt(String cdoperacionmkt) {
    this.cdoperacionmkt = cdoperacionmkt;
  }

  @Column(name = "CDMIEMBROMKT", nullable = false, length = 10)
  public String getCdmiembromkt() {
    return this.cdmiembromkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  @Column(name = "CDINDCOTIZACION", nullable = false, length = 1)
  public char getCdindcotizacion() {
    return this.cdindcotizacion;
  }

  public void setCdindcotizacion(char cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  @Column(name = "CDISIN", nullable = false, length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "NUORDENMKT", nullable = false, length = 9)
  public String getNuordenmkt() {
    return this.nuordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  @Column(name = "CDREFCLIENTE", length = 16)
  public String getCdrefcliente() {
    return this.cdrefcliente;
  }

  public void setCdrefcliente(String cdrefcliente) {
    this.cdrefcliente = cdrefcliente;
  }

  @Column(name = "CDREFEXTORDEN", length = 16)
  public String getCdrefextorden() {
    return this.cdrefextorden;
  }

  public void setCdrefextorden(String cdrefextorden) {
    this.cdrefextorden = cdrefextorden;
  }

  @Column(name = "CDINDCAPACIDAD", nullable = false, length = 1)
  public char getCdindcapacidad() {
    return this.cdindcapacidad;
  }

  public void setCdindcapacidad(char cdindcapacidad) {
    this.cdindcapacidad = cdindcapacidad;
  }

  @Column(name = "NBORDENMKT", length = 80)
  public String getNbordenmkt() {
    return this.nbordenmkt;
  }

  public void setNbordenmkt(String nbordenmkt) {
    this.nbordenmkt = nbordenmkt;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDCUENTACOMP")
  public Tmct0CuentasDeCompensacion getTmct0CuentasDeCompensacion() {
    return this.tmct0CuentasDeCompensacion;
  }

  public void setTmct0CuentasDeCompensacion(Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion) {
    this.tmct0CuentasDeCompensacion = tmct0CuentasDeCompensacion;
  }

  @Column(name = "CDMOVIMIENTOECC", length = 30)
  public String getCdmovimientoecc() {
    return this.cdmovimientoecc;
  }

  public void setCdmovimientoecc(String cdmovimientoecc) {
    this.cdmovimientoecc = cdmovimientoecc;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FLIQTEORICA", length = 4)
  public Date getFliqteorica() {
    return this.fliqteorica;
  }

  public void setFliqteorica(Date fliqteorica) {
    this.fliqteorica = fliqteorica;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HREGISTROECC", nullable = false, length = 3, columnDefinition = "TIME DEFAULT CURRENT TIME")
  public Date getHregistroecc() {
    return this.hregistroecc;
  }

  public void setHregistroecc(Date hregistroecc) {
    this.hregistroecc = hregistroecc;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HNEGOCIACION", nullable = false, length = 3, columnDefinition = "TIME DEFAULT CURRENT TIME")
  public Date getHnegociacion() {
    return this.hnegociacion;
  }

  public void setHnegociacion(Date hnegociacion) {
    this.hnegociacion = hnegociacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FORDENMKT", nullable = false, length = 4, columnDefinition = "DATE DEFAULT CURRENT DATE")
  public Date getFordenmkt() {
    return this.fordenmkt;
  }

  public void setFordenmkt(Date fordenmkt) {
    this.fordenmkt = fordenmkt;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HORDENMKT", nullable = false, length = 3, columnDefinition = "TIME DEFAULT CURRENT TIME")
  public Date getHordenmkt() {
    return this.hordenmkt;
  }

  public void setHordenmkt(Date hordenmkt) {
    this.hordenmkt = hordenmkt;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FNEGOCIACION", nullable = false, length = 4, columnDefinition = "DATE DEFAULT CURRENT DATE")
  public Date getFnegociacion() {
    return this.fnegociacion;
  }

  public void setFnegociacion(Date fnegociacion) {
    this.fnegociacion = fnegociacion;
  }

  @Column(name = "NUTITULOS", precision = 18, scale = 6)
  public BigDecimal getNutitulos() {
    return this.nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  @Column(name = "CD_PLATAFORMA_NEGOCIACION", nullable = false, length = 4)
  public String getCdPlataformaNegociacion() {
    return this.cdPlataformaNegociacion;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  @Column(name = "CORRETAJE", nullable = false, precision = 15, scale = 2)
  public BigDecimal getCorretaje() {
    return this.corretaje;
  }

  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_COMPENSADOR", nullable = true)
  public Tmct0Compensador getCompensador() {
    return compensador;
  }

  public void setCompensador(Tmct0Compensador compensador) {
    this.compensador = compensador;
  }

  @Column(name = "CUENTA_LIQUIDACION", nullable = true, length = 35)
  public String getCuentaLiquidacion() {
    return cuentaLiquidacion;
  }

  public void setCuentaLiquidacion(String cuentaLiquidacion) {
    this.cuentaLiquidacion = cuentaLiquidacion;
  }

  @Column(name = "ENTIDAD_PARTICIPANTE", nullable = true, length = 11)
  public String getEntidadParticipante() {
    return entidadParticipante;
  }

  public void setEntidadParticipante(String entidadParticipante) {
    this.entidadParticipante = entidadParticipante;
  }

  public char getEnvios3() {
    return envios3;
  }

  public void setEnvios3(char envios3) {
    this.envios3 = envios3;
  }

}
