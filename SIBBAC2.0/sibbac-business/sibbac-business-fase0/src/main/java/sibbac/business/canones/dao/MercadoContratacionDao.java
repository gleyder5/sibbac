package sibbac.business.canones.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.canones.model.MercadoContratacion;

@Repository
public interface MercadoContratacionDao extends JpaRepository<MercadoContratacion, String> {
  
  @Query("select m from MercadoContratacion m where m.mercadoAgrupacion.id = :mercadoAgrupacion ")
  List<MercadoContratacion> findAllByMercadoAgrupacion( @Param("mercadoAgrupacion") String mercadoAgrupacion);

}
