package sibbac.business.fase0.database.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0clmId implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 6661870952404475890L;

  @Column(name = "CDCLEARE", nullable = false, length = 25)
  private String cdcleare;
  @Column(name = "NUMSEC", nullable = false, precision = 20, scale = 0)
  private BigDecimal numsec;

  public Tmct0clmId() {

  }

  public Tmct0clmId(String cdcleare, BigDecimal numsec) {
    super();
    this.cdcleare = cdcleare;
    this.numsec = numsec;
  }

  public String getCdcleare() {
    return cdcleare;
  }

  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public BigDecimal getNumsec() {
    return numsec;
  }

  public void setNumsec(BigDecimal numsec) {
    this.numsec = numsec;
  }

  @Override
  public int hashCode() {
    final int prime = 43;
    int result = 7;

    result = prime * result + ((cdcleare == null) ? 0 : cdcleare.hashCode());
    result = prime * result + ((numsec == null) ? 0 : numsec.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0clmId other = (Tmct0clmId) obj;

    if (cdcleare == null) {
      if (other.cdcleare != null)
        return false;
    }
    else if (!cdcleare.equals(other.cdcleare))
      return false;

    if (numsec == null) {
      if (other.numsec != null)
        return false;
    }
    else if (!numsec.equals(other.numsec))
      return false;

    return true;
  }

  @Override
  public String toString() {
    return String.format("Tmct0clmId %s##%s", cdcleare, numsec);

  }

}
