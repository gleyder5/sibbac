package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0MOVIMIENTOECC_S3")
public class MovimientoEccS3 implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6494585702250568161L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_MOVIMIENTO_S3", nullable = false)
  protected Long idMovimientoS3;

  @Column(name = "CDREFMOVIMIENTO", length = 30, nullable = true)
  protected java.lang.String cdrefmovimiento;

  @Column(name = "CDREFINTERNAASIG", length = 18, nullable = true)
  protected java.lang.String cdrefinternaasig;
  @Column(name = "CDREFMOVIMIENTOECC", length = 20, nullable = true)
  protected java.lang.String cdrefmovimientoecc;
  @Column(name = "CDREFNOTIFICACION", length = 20, nullable = true)
  protected java.lang.String cdrefnotificacion;
  @Column(name = "CDREFNOTIFICACIONPREV", length = 20, nullable = true)
  protected java.lang.String cdrefnotificacionprev;
  @Column(name = "CDTIPOMOV", length = 2, nullable = true)
  protected java.lang.String cdtipomov;
  @Column(name = "CDESTADOMOV", length = 2, nullable = true)
  protected java.lang.String cdestadomov;

  @Column(name = "CDINDCOTIZACION", length = 1, nullable = true)
  protected Character cdindcotizacion;

  @Column
  protected Date fcontratacion;

  @Column
  protected Date fliquidacion;

  @Column(length = 32)
  protected String nuordenmkt;

  @Column(length = 32)
  protected String nuexemkt;

  @Column(name = "MIEMBRO_ORIGEN", length = 4)
  protected String miembroOrigen;

  @Column(name = "USUARIO_ORIGEN", length = 3)
  protected String usuarioOrigen;

  @Column(name = "CD_REF_ASIGNACION", length = 18)
  protected String codigoReferenciaAsignacion;

  @Column(length = 10)
  protected String mnemotecnico;

  @Column(name = "MIEMBRO_DESTINO", length = 4)
  protected String miembroDestino;

  @Column(name = "USUARIO_DESTINO", length = 3)
  protected String usuarioDestino;

  @Column(name = "MIEMBRO_COMPENSADOR_DESTINO", length = 4)
  protected String miembroCompensadorDestino;

  @Column(name = "CUENTA_COMPENSACION_DESTINO", length = 3)
  protected String cuentaCompensacionDestino;

  @Column(name = "IMTITULOS", length = 18, scale = 6)
  protected java.math.BigDecimal imtitulos;

  @Column(name = "IMTITULOS_PENDIENTES_ASIGNAR", length = 18, scale = 6)
  protected java.math.BigDecimal imtitulosPendientesAsignar;

  @Column(length = 13, scale = 6)
  protected java.math.BigDecimal precio;
  // TODO PONER 15,2
  @Column(name = "IMEFECTIVO", length = 13, scale = 2, nullable = false)
  protected java.math.BigDecimal imefectivo;

  @Column(name = "CDOPERACIONECC", length = 16, nullable = true)
  protected java.lang.String cdoperacionecc;

  @Column(name = "NUOPERACIONPREV", length = 16, nullable = true)
  protected java.lang.String nuoperacionprev;

  @Column(length = 18, nullable = true)
  protected java.lang.String cdrefasignacion;

  @Column(length = 4)
  protected String plataforma;

  @Column(length = 2)
  protected String segmento;

  @Column
  protected Date fnegociacion;

  @Column(name = "CD_OPERACION_MERCADO", length = 2)
  protected String codigoOperacionMercado;

  @Column(name = "MIEMBRO_MERCADO", length = 4)
  protected String miembroMercado;

  @Column(name = "FECHA_ORDEN")
  protected Date fechaOrden;

  @Column(name = "HORA_ORDEN")
  protected Date horaOrden;

  @Column(name = "INDICADOR_CAPACIDAD", length = 1, nullable = true)
  protected Character indicadorCapacidad;

  @Column(length = 12)
  protected String cdisin;

  @Column(length = 1, nullable = true)
  protected Character sentido;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", nullable = true)
  protected Date audit_fecha_cambio;

  @Column(name = "AUDIT_USER", length = 20, nullable = true)
  protected java.lang.String audit_user;
  
  @Column(name = "CDCAMARA", length = 10, nullable = true)
  protected String cdcamara;

  // @Column
  // ( mappedBy = "fk_movimientoecc_fidessa" )
  // protected Collection< MovimientoEcc > fk_movimientoecc;

  public Long getIdMovimientoS3() {
    return idMovimientoS3;
  }

  public void setIdMovimientoS3(Long idMovimientoS3) {
    this.idMovimientoS3 = idMovimientoS3;
  }

  public java.lang.String getCdrefmovimiento() {
    return cdrefmovimiento;
  }

  public void setCdrefmovimiento(java.lang.String cdrefmovimiento) {
    this.cdrefmovimiento = cdrefmovimiento;
  }

  public java.lang.String getCdrefinternaasig() {
    return cdrefinternaasig;
  }

  public void setCdrefinternaasig(java.lang.String cdrefinternaasig) {
    this.cdrefinternaasig = cdrefinternaasig;
  }

  public java.lang.String getCdrefmovimientoecc() {
    return cdrefmovimientoecc;
  }

  public void setCdrefmovimientoecc(java.lang.String cdrefmovimientoecc) {
    this.cdrefmovimientoecc = cdrefmovimientoecc;
  }

  public java.lang.String getCdrefnotificacion() {
    return cdrefnotificacion;
  }

  public void setCdrefnotificacion(java.lang.String cdrefnotificacion) {
    this.cdrefnotificacion = cdrefnotificacion;
  }

  public java.lang.String getCdrefnotificacionprev() {
    return cdrefnotificacionprev;
  }

  public void setCdrefnotificacionprev(java.lang.String cdrefnotificacionprev) {
    this.cdrefnotificacionprev = cdrefnotificacionprev;
  }

  public java.lang.String getCdtipomov() {
    return cdtipomov;
  }

  public void setCdtipomov(java.lang.String cdtipomov) {
    this.cdtipomov = cdtipomov;
  }

  public java.lang.String getCdestadomov() {
    return cdestadomov;
  }

  public void setCdestadomov(java.lang.String cdestadomov) {
    this.cdestadomov = cdestadomov;
  }

  public Character getCdindcotizacion() {
    return cdindcotizacion;
  }

  public void setCdindcotizacion(Character cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  public Date getFcontratacion() {
    return fcontratacion;
  }

  public void setFcontratacion(Date fcontratacion) {
    this.fcontratacion = fcontratacion;
  }

  public Date getFliquidacion() {
    return fliquidacion;
  }

  public void setFliquidacion(Date fliquidacion) {
    this.fliquidacion = fliquidacion;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  public String getMiembroOrigen() {
    return miembroOrigen;
  }

  public void setMiembroOrigen(String miembroOrigen) {
    this.miembroOrigen = miembroOrigen;
  }

  public String getUsuarioOrigen() {
    return usuarioOrigen;
  }

  public void setUsuarioOrigen(String usuarioOrigen) {
    this.usuarioOrigen = usuarioOrigen;
  }

  public String getCodigoReferenciaAsignacion() {
    return codigoReferenciaAsignacion;
  }

  public void setCodigoReferenciaAsignacion(String codigoReferenciaAsignacion) {
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
  }

  public String getMnemotecnico() {
    return mnemotecnico;
  }

  public void setMnemotecnico(String mnemotecnico) {
    this.mnemotecnico = mnemotecnico;
  }

  public String getMiembroDestino() {
    return miembroDestino;
  }

  public void setMiembroDestino(String miembroDestino) {
    this.miembroDestino = miembroDestino;
  }

  public String getUsuarioDestino() {
    return usuarioDestino;
  }

  public void setUsuarioDestino(String usuarioDestino) {
    this.usuarioDestino = usuarioDestino;
  }

  public String getMiembroCompensadorDestino() {
    return miembroCompensadorDestino;
  }

  public void setMiembroCompensadorDestino(String miembroCompensadorDestino) {
    this.miembroCompensadorDestino = miembroCompensadorDestino;
  }

  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

  public java.math.BigDecimal getImtitulos() {
    return imtitulos;
  }

  public void setImtitulos(java.math.BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  public java.math.BigDecimal getImtitulosPendientesAsignar() {
    return imtitulosPendientesAsignar;
  }

  public void setImtitulosPendientesAsignar(java.math.BigDecimal imtitulosPendientesAsignar) {
    this.imtitulosPendientesAsignar = imtitulosPendientesAsignar;
  }

  public java.math.BigDecimal getPrecio() {
    return precio;
  }

  public void setPrecio(java.math.BigDecimal precio) {
    this.precio = precio;
  }

  public java.math.BigDecimal getImefectivo() {
    return imefectivo;
  }

  public void setImefectivo(java.math.BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  public java.lang.String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public void setCdoperacionecc(java.lang.String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public java.lang.String getNuoperacionprev() {
    return nuoperacionprev;
  }

  public void setNuoperacionprev(java.lang.String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  public java.lang.String getCdrefasignacion() {
    return cdrefasignacion;
  }

  public void setCdrefasignacion(java.lang.String cdrefasignacion) {
    this.cdrefasignacion = cdrefasignacion;
  }

  public String getPlataforma() {
    return plataforma;
  }

  public void setPlataforma(String plataforma) {
    this.plataforma = plataforma;
  }

  public String getSegmento() {
    return segmento;
  }

  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  public Date getFnegociacion() {
    return fnegociacion;
  }

  public void setFnegociacion(Date fnegociacion) {
    this.fnegociacion = fnegociacion;
  }

  public String getCodigoOperacionMercado() {
    return codigoOperacionMercado;
  }

  public void setCodigoOperacionMercado(String codigoOperacionMercado) {
    this.codigoOperacionMercado = codigoOperacionMercado;
  }

  public String getMiembroMercado() {
    return miembroMercado;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  public Date getFechaOrden() {
    return fechaOrden;
  }

  public void setFechaOrden(Date fechaOrden) {
    this.fechaOrden = fechaOrden;
  }

  public Date getHoraOrden() {
    return horaOrden;
  }

  public void setHoraOrden(Date horaOrden) {
    this.horaOrden = horaOrden;
  }

  public Character getIndicadorCapacidad() {
    return indicadorCapacidad;
  }

  public void setIndicadorCapacidad(Character indicadorCapacidad) {
    this.indicadorCapacidad = indicadorCapacidad;
  }

  public String getCdisin() {
    return cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public Character getSentido() {
    return sentido;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public Date getAudit_fecha_cambio() {
    return audit_fecha_cambio;
  }

  public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
    this.audit_fecha_cambio = audit_fecha_cambio;
  }

  public java.lang.String getAudit_user() {
    return audit_user;
  }

  public void setAudit_user(java.lang.String audit_user) {
    this.audit_user = audit_user;
  }

  public String getCdcamara() {
    return cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  // public Collection< MovimientoEcc > getFk_movimientoecc() {
  // return fk_movimientoecc;
  // }
  //
  //
  // public void setFk_movimientoecc( Collection< MovimientoEcc >
  // fk_movimientoecc ) {
  // this.fk_movimientoecc = fk_movimientoecc;
  // }

}
