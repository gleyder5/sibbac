package sibbac.business.fase0.database.dao;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.data.Tmct0desgloseclititData;


@Repository
public class Tmct0desgloseclititDaoImpl {

	private static final Logger	LOG	= LoggerFactory.getLogger( Tmct0desgloseclititDaoImpl.class );
	@PersistenceContext
	private EntityManager		em;

	@SuppressWarnings( "unchecked" )
	public List< Object[] > getTitulosCuentaDiariaConFiltro( List< String > operaciones, HashMap< String, Object > filtros ) {

		StringBuilder sb = new StringBuilder( "select sum(d.imtitulos) , d.cdisin, d.tmct0desglosecamara.fecontratacion, " )
				.append( " d.tmct0desglosecamara.feliquidacion, d.cdsentido ,  d.imprecio, " )
				.append( " d.cdcamara, d.cdmiembromkt, min(d.nuordenmkt), min(d.nurefexemkt),  min(d.feordenmkt), " )
				.append(
						" d.tmct0desglosecamara.tmct0estadoByCdestadoasig.nombre from Tmct0desgloseclitit d where d.cdoperacion in :operaciones" );
		if ( filtros.get( "isin" ) != null && !filtros.get( "isin" ).equals( "" ) )
			sb.append( " and d.cdisin = :isin" );

		if ( filtros.get( "camara" ) != null )
			sb.append( " and d.cdcamara = :camara" );

		if ( filtros.get( "sentido" ) != null )
			sb.append( " and d.cdsentido = :sentido" );

		if ( filtros.get( "bolsa" ) != null )
			sb.append( " and d.cdmiembromkt = :bolsa" );

		if ( filtros.get( "precio" ) != null )
			sb.append( " and d.imprecio = :precio" );

		if ( filtros.get( "estado" ) != null )
			sb.append( " and d.tmct0desglosecamara.tmct0estadoByCdestadoasig.nombre = :estado" );

		sb.append( " group by d.cdisin,  d.cdsentido , " ).append( "  d.imprecio, d.cdcamara, d.cdmiembromkt" );

		javax.persistence.Query query = em.createQuery( sb.toString() );

		query.setParameter( "operaciones", operaciones );

		if ( filtros.get( "isin" ) != null ) {
			String isin = filtros.get( "isin" ).toString();
			query.setParameter( "isin", isin );
		}

		if ( filtros.get( "camara" ) != null ) {
			String camara = filtros.get( "camara" ).toString();
			query.setParameter( "camara", camara );
		}

		if ( filtros.get( "sentido" ) != null ) {
			Character sentido = filtros.get( "sentido" ).toString().charAt( 0 );
			query.setParameter( "sentido", sentido );
		}

		// if(filtros.get( "bolsa" ) !=null ){
		// String bolsa = filtros.get( "bolsa" ).toString();
		// query.setParameter( "bolsa", bolsa );
		// }
		//
		// if(filtros.get( "precio" ) !=null ){
		// BigDecimal precio = (BigDecimal)filtros.get( "precio" );
		// query.setParameter( "precio", precio );
		// }

		if ( filtros.get( "estado" ) != null ) {
			String estado = filtros.get( "estado" ).toString();
			query.setParameter( "estado", estado );
		}

		return ( List< Object[] > ) query.getResultList();
	}

	@SuppressWarnings( "unchecked" )
	public List< Tmct0desgloseclititData > findForDiaria( Map< String, Serializable > filtros ) {
		String select = "SELECT new sibbac.business.fase0.database.data.Tmct0desgloseclititData(cam.tmct0estadoByCdestadoasig.idestado, cam.tmct0estadoByCdestadoasig.nombre, clitit.cdoperacion, clitit.cdcamara, clitit.cdsentido, sum(clitit.imtitulos) as imtitulos, clitit.imprecio , clitit.cdisin, clitit.feejecuc, alc.id.nuorden ) "
				+ "FROM Tmct0desgloseclitit clitit join clitit.tmct0desglosecamara cam join cam.tmct0alc alc WHERE cam.fecontratacion >=:fcontratacionDe AND cam.fecontratacion <:fcontratacionA AND cam.tmct0estadoByCdestadoasig.idestado<65 AND cam.tmct0estadoByCdestadoasig.idestado!=0 ";
		String groupBy = " group by clitit.cdisin, clitit.imprecio, clitit.cdoperacion, cam.tmct0estadoByCdestadoasig.idestado, cam.tmct0estadoByCdestadoasig.nombre, clitit.cdcamara, clitit.cdsentido, clitit.feejecuc, alc.id.nuorden ";
		String orderBy = " order by clitit.cdisin asc ";
		String prefix = "[Tmct0desgloseclititDaoImpl::findForDiaria] ";
		boolean addedWhere = true;
		StringBuilder sb = new StringBuilder( select );
		// se construye la sql segun los parametros recibidos
		if ( filtros != null ) {
			LOG.trace( prefix + "Examinando filtros..." );
			for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
				LOG.trace( prefix + "+ [{}=={}]", entry.getKey(), entry.getValue() );
				if ( entry.getValue() != null && !"".equals( entry.getValue().toString() ) && !entry.getKey().equals( "estado" )
						&& 'f' != entry.getKey().charAt( 0 ) ) {
					if (!"idCuentaLiquidacion".equals(entry.getKey()))
					{
						if ( !addedWhere ) {
							sb.append( "WHERE " ).append( "clitit." ).append( entry.getKey() ).append( " =:" ).append( entry.getKey() )
									.append( " " );
							addedWhere = true;
						} else {
							sb.append( "AND " ).append( "clitit." ).append( entry.getKey() ).append( " =:" ).append( entry.getKey() )
									.append( " " );
						}
					}//if (!"idCuentaLiquidacion".equals(entry.getKey()))
				} else if ( entry.getKey().equals( "fliquidacionDe" ) ) {
					sb.append( "AND cam.feliquidacion >=:fliquidacionDe AND cam.feliquidacion <:fliquidacionA " );
				}
			}
		}

		sb.append( groupBy );
		sb.append( orderBy );

		Query query = em.createQuery( sb.toString() );
		// se introducen los parametros
		if ( filtros != null ) {
			for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
				if ( /* 'f' != entry.getKey().charAt( 0 ) && */!entry.getKey().equals( "estado" ) && entry.getValue() != null
						&& !"".equals( entry.getValue().toString() ) ) {
					if ( entry.getKey().equals( "fcontratacionA" ) || entry.getKey().equals( "fliquidacionA" ) ) {
						query.setParameter( entry.getKey(), DateUtils.addDays( ( Date ) entry.getValue(), 1 ) );
					} else {
						if (!"idCuentaLiquidacion".equals(entry.getKey()))
						{
							query.setParameter( entry.getKey(), entry.getValue() );
						}
					}
				}
			}
		}

		List< Tmct0desgloseclititData > resultList = new ArrayList< Tmct0desgloseclititData >();
		resultList.addAll( query.getResultList() );
		return resultList;

	}

	@SuppressWarnings( "unchecked" )
	public List< Tmct0desgloseclititData > findForDiariaChica( Map< String, Serializable > filtros ) {
		String select = "SELECT new sibbac.business.fase0.database.data.Tmct0desgloseclititData(clitit.nudesglose, cam.tmct0estadoByCdestadoasig.idestado, cam.tmct0estadoByCdestadoasig.nombre, clitit.cdoperacion, clitit.cdcamara, clitit.cdsentido, clitit.cdisin, clitit.feejecuc ) FROM Tmct0desgloseclitit clitit join clitit.tmct0desglosecamara cam";
		String groupBy = " group by clitit.cdisin, clitit.cdsentido, clitit.cdoperacion, cam.tmct0estadoByCdestadoasig.idestado, clitit.feejecuc ";
		String orderBy = " order by clitit.cdisin asc";
		boolean addedWhere = false;
		StringBuilder sb = new StringBuilder( select );
		// se construye la sql segun los parametros recibidos
		if ( filtros != null ) {
			for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
				if ( entry.getValue() != null && !"".equals( entry.getValue().toString() ) ) {
					if ( !addedWhere ) {
						sb.append( "WHERE " ).append( "m." ).append( entry.getKey() ).append( " =:" ).append( entry.getKey() ).append( " " );
						addedWhere = true;
					} else {
						sb.append( "AND " ).append( "m." ).append( entry.getKey() ).append( " =:" ).append( entry.getKey() ).append( " " );
					}
				}
			}
		}

		sb.append( groupBy );
		sb.append( orderBy );

		Query query = em.createQuery( sb.toString() );
		// se introducen los parametros
		if ( filtros != null ) {
			for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
				if ( entry.getValue() != null && !"".equals( entry.getValue().toString() ) ) {
					query.setParameter( entry.getKey(), entry.getValue() );
				}
			}
		}

		List< Tmct0desgloseclititData > resultList = new ArrayList< Tmct0desgloseclititData >();
		resultList.addAll( query.getResultList() );
		return resultList;

	}

}
