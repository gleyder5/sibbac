package sibbac.business.canones.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MercadoContratacionTipoOrdenId extends MercadoContratacionRefId {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -5554602359203038837L;

  @Column(name = MercadoContratacion.ID_COLUMN_NAME, length = 2, nullable = false)
  public String idField;

  @Override
  public String getIdField() {
    return idField;
  }

  @Override
  public void setIdField(String idField) {
    this.idField = idField;
  }

}
