package sibbac.business.fase0.database.dto;


import java.io.Serializable;


public class EstadosEquivalentesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1259093735054067936L;
	private final int	id_estado1, id_estado2, id_estado_equivalente;
	
	public EstadosEquivalentesDTO( int id_estado1, int id_estado2, int id_estado_equivalente ) {
		super();
		this.id_estado1 = id_estado1;
		this.id_estado2 = id_estado2;
		this.id_estado_equivalente = id_estado_equivalente;
	}

	
	public int getId_estado1() {
		return id_estado1;
	}

	
	public int getId_estado2() {
		return id_estado2;
	}

	
	public int getId_estado_equivalente() {
		return id_estado_equivalente;
	}


	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_estado1;
		result = prime * result + id_estado2;
		result = prime * result + id_estado_equivalente;
		return result;
	}


	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		EstadosEquivalentesDTO other = ( EstadosEquivalentesDTO ) obj;
		if ( id_estado1 != other.id_estado1 )
			return false;
		if ( id_estado2 != other.id_estado2 )
			return false;
		if ( id_estado_equivalente != other.id_estado_equivalente )
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "EstadosEquivalentesDTO [id_estado1=" + id_estado1 + ", id_estado2=" + id_estado2 + ", id_estado_equivalente="
				+ id_estado_equivalente + "]";
	}
	
	

}
