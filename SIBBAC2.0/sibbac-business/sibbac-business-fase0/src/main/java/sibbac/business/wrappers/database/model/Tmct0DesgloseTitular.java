package sibbac.business.wrappers.database.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;

import sibbac.database.DBConstants;

/**
 *
 * @author
 * fjarquellada
 */
@Entity
@Table(name = DBConstants.WRAPPERS.DESGLOSE_TITULAR)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tmct0DesgloseTitular.findAll", query = "SELECT t FROM Tmct0DesgloseTitular t"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findById", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.id = :id"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByAuditDate", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.auditDate = :auditDate"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByAuditUser", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.auditUser = :auditUser"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByCdestado", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.cdestado = :cdestado"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByIdholder", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.idholder = :idholder"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByTptiprep", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.tptiprep = :tptiprep"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByTpidenti", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.tpidenti = :tpidenti"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbidenti", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbidenti = :nbidenti"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbclient", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbclient = :nbclient"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbclient1", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbclient1 = :nbclient1"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbclient2", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbclient2 = :nbclient2"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByCdnactit", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.cdnactit = :cdnactit"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByTpsocied", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.tpsocied = :tpsocied"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByTpnactit", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.tpnactit = :tpnactit"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByParticip", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.particip = :particip"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByTpdomici", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.tpdomici = :tpdomici"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbdomici", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbdomici = :nbdomici"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNudomici", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nudomici = :nudomici"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbciudad", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbciudad = :nbciudad"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByNbprovin", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.nbprovin = :nbprovin"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByCdpostal", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.cdpostal = :cdpostal"),
    @NamedQuery(name = "Tmct0DesgloseTitular.findByCddepais", query = "SELECT t FROM Tmct0DesgloseTitular t WHERE t.cddepais = :cddepais")})
public class Tmct0DesgloseTitular implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "AUDIT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditDate;
    @Column(name = "AUDIT_USER", length=255)
    private String auditUser;
    @Basic(optional = false)
    @Column(name = "CDESTADO", nullable=false)
    private Character cdestado;
    @Column(name = "IDHOLDER", length=40)
    private String idholder;
    @Column(name = "TPTIPREP")
    private Character tptiprep;
    @Column(name = "TPIDENTI")
    private Character tpidenti;
    @Column(name = "NBIDENTI", length=40)
    private String nbidenti;
    @Column(name = "NBCLIENT", length=30)
    private String nbclient;
    @Column(name = "NBCLIENT1", length=50)
    private String nbclient1;
    @Column(name = "NBCLIENT2", length=30)
    private String nbclient2;
    @Column(name = "CDNACTIT", length=3)
    private String cdnactit;
    @Column(name = "TPSOCIED")
    private Character tpsocied;
    @Column(name = "TPNACTIT")
    private Character tpnactit;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PARTICIP")
    private BigDecimal particip;
    @Column(name = "TPDOMICI", length=2)
    private String tpdomici;
    @Column(name = "NBDOMICI", length=40)
    private String nbdomici;
    @Column(name = "NUDOMICI", length=4)
    private String nudomici;
    @Column(name = "NBCIUDAD", length=40)
    private String nbciudad;
    @Column(name = "NBPROVIN", length=25)
    private String nbprovin;
    @Column(name = "CDPOSTAL", length=5)
    private String cdpostal;
    @Column(name = "CDDEPAIS", length=3)
    private String cddepais;
    @JoinColumn(name = "ID_DESGLOSE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Tmct0Desglose idDesglose;

    public Tmct0DesgloseTitular() {
    }

    public Tmct0DesgloseTitular(Long id) {
        this.id = id;
    }

    public Tmct0DesgloseTitular(Long id, Character cdestado) {
        this.id = id;
        this.cdestado = cdestado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public Character getCdestado() {
        return cdestado;
    }

    public void setCdestado(Character cdestado) {
        this.cdestado = cdestado;
    }

    public String getIdholder() {
        return idholder;
    }

    public void setIdholder(String idholder) {
        this.idholder = idholder;
    }

    public Character getTptiprep() {
        return tptiprep;
    }

    public void setTptiprep(Character tptiprep) {
        this.tptiprep = tptiprep;
    }

    public Character getTpidenti() {
        return tpidenti;
    }

    public void setTpidenti(Character tpidenti) {
        this.tpidenti = tpidenti;
    }

    public String getNbidenti() {
        return nbidenti;
    }

    public void setNbidenti(String nbidenti) {
        this.nbidenti = nbidenti;
    }

    public String getNbclient() {
        return nbclient;
    }

    public void setNbclient(String nbclient) {
        this.nbclient = nbclient;
    }

    public String getNbclient1() {
        return nbclient1;
    }

    public void setNbclient1(String nbclient1) {
        this.nbclient1 = nbclient1;
    }

    public String getNbclient2() {
        return nbclient2;
    }

    public void setNbclient2(String nbclient2) {
        this.nbclient2 = nbclient2;
    }

    public String getCdnactit() {
        return cdnactit;
    }

    public void setCdnactit(String cdnactit) {
        this.cdnactit = cdnactit;
    }

    public Character getTpsocied() {
        return tpsocied;
    }

    public void setTpsocied(Character tpsocied) {
        this.tpsocied = tpsocied;
    }

    public Character getTpnactit() {
        return tpnactit;
    }

    public void setTpnactit(Character tpnactit) {
        this.tpnactit = tpnactit;
    }

    public BigDecimal getParticip() {
        return particip;
    }

    public void setParticip(BigDecimal particip) {
        this.particip = particip;
    }

    public String getTpdomici() {
        return tpdomici;
    }

    public void setTpdomici(String tpdomici) {
        this.tpdomici = tpdomici;
    }

    public String getNbdomici() {
        return nbdomici;
    }

    public void setNbdomici(String nbdomici) {
        this.nbdomici = nbdomici;
    }

    public String getNudomici() {
        return nudomici;
    }

    public void setNudomici(String nudomici) {
        this.nudomici = nudomici;
    }

    public String getNbciudad() {
        return nbciudad;
    }

    public void setNbciudad(String nbciudad) {
        this.nbciudad = nbciudad;
    }

    public String getNbprovin() {
        return nbprovin;
    }

    public void setNbprovin(String nbprovin) {
        this.nbprovin = nbprovin;
    }

    public String getCdpostal() {
        return cdpostal;
    }

    public void setCdpostal(String cdpostal) {
        this.cdpostal = cdpostal;
    }

    public String getCddepais() {
        return cddepais;
    }

    public void setCddepais(String cddepais) {
        this.cddepais = cddepais;
    }

    public Tmct0Desglose getIdDesglose() {
        return idDesglose;
    }

    public void setIdDesglose(Tmct0Desglose idDesglose) {
        this.idDesglose = idDesglose;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tmct0DesgloseTitular)) {
            return false;
        }
        Tmct0DesgloseTitular other = (Tmct0DesgloseTitular) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
	return ToStringBuilder.reflectionToString(this);
    }
    
}

