package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.database.DBConstants.WRAPPERS;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.CodigoTpsocied }. Entity:
 * "CodigoTpsocied".
 * 
 * @version 1.0
 * @since 1.0
 */

//
@Table( name = WRAPPERS.CODIGO_TPSOCIED)
@Entity
public class CodigoTpsocied implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -5668031503577663012L;

	/**
	 * 
	 */
	@Id
	@Column( name = "CODIGO", nullable = false, length = 1 )
	private String					codigo;

	@Column( name = "DESCRIPCION", nullable = false, length = 20 )
	private String					descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}
}
