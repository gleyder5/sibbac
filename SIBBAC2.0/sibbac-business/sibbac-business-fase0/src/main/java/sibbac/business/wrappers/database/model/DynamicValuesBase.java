package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAttribute;

@MappedSuperclass
public class DynamicValuesBase implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -7107044859575603307L;

  @XmlAttribute
  @Column(length = 32)
  private String valorSt;
  @XmlAttribute
  @Column(length = 24, scale = 8)
  private BigDecimal valor;

  /**
   * The version of the record.
   */
  @Version
  @Column(name = "VERSION", nullable = false)
  @XmlAttribute
  protected Long version = 0L;

  /**
   * The user that performs the change.
   */
  @Column(name = "AUDIT_USER", nullable = false)
  @XmlAttribute
  protected String auditUser = "SIBBAC20";

  /**
   * The date instance when the change is performed.
   */
  @XmlAttribute
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_DATE", nullable = false)
  protected Date auditDate = new Date();

  public Long getVersion() {
    return version;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public String getValorSt() {
    return valorSt;
  }

  public BigDecimal getValor() {
    return valor;
  }

  public void setValorSt(String valorSt) {
    this.valorSt = valorSt;
  }

  public void setValor(BigDecimal valor) {
    this.valor = valor;
  }

}
