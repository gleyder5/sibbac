package sibbac.business.fase0.database.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.Tmct0cleDTO;
import sibbac.business.fase0.database.model.Tmct0cleIdNacional;
import sibbac.business.fase0.database.model.Tmct0cleNacional;

@Repository
public interface Tmct0cleNacionalDao extends JpaRepository<Tmct0cleNacional, Tmct0cleIdNacional> {

  @Query("SELECT C FROM Tmct0cleNacional C WHERE C.id.cdcleare = :cdcleare AND C.fhinicio <= :fecha AND C.fhfinal >= :fecha ")
  public List<Tmct0cleNacional> findByFechaAndCdcleare(@Param("fecha") final Integer fecha,
      @Param("cdcleare") final String cdcleare);

  @Query("SELECT C FROM Tmct0cleNacional C WHERE C.id.cdcleare = :cdcleare AND C.id.centro = :centro AND C.id.cdmercad = :cdmercad "
      + " AND C.fhinicio <= :fecha AND C.fhfinal >= :fecha ")
  public List<Tmct0cleNacional> findByFechaAndCdcleare(@Param("fecha") final Integer fecha,
      @Param("cdcleare") final String cdcleare, @Param("centro") final String centro,
      @Param("cdmercad") final String cdmercad);

  @Query("SELECT cle FROM Tmct0cleNacional cle WHERE cle.id.cdcleare=:cdcleare AND cle.fhfinal=99991231")
  public List<Tmct0cleNacional> findActiveByCdcleare(@Param("cdcleare") final String cdcleare);

  @Query("SELECT cle FROM Tmct0cleNacional cle WHERE cle.id.centro = '' and cle.id.cdmercad ='' "
      + " AND cle.fhfinal=99991231 ORDER BY cle.id.cdcleare ASC, cle.dscleare ASC")
  List<Tmct0cleNacional> findAllActiveNacional();

  @Query("select distinct new sibbac.business.fase0.database.dto.Tmct0cleDTO (  biccle, dscleare) from Tmct0cleNacional where fhfinal >= :fhFinal and centro != 'MMEE                          '")
  public Set<Tmct0cleDTO> findDistinctDscleareAndBiccleByFhfinal(@Param("fhFinal") final Integer fhFinal);

}
