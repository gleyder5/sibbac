package sibbac.business.wrappers.database.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;

public class Tmct0grupoejecucionData implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  private Long idgrupoeje;
  private Tmct0AloData tmct0alo;
  private BigDecimal nutitulos;
  private Date fevalor;
  private BigDecimal impreciomkt;
  private String cdtipoop;
  private String cdsettmkt;
  private String cdcamara;
  private Date auditFechaCambio;
  private String auditUser;
  private Character asigcomp;
  private String tradingvenue;
  private String nuagreje;
  private Date feejecuc;
  private List<Tmct0ejecucionalocationData> tmct0ejecucionalocations = new ArrayList<Tmct0ejecucionalocationData>(0);

  public Tmct0grupoejecucionData() {

  }

  public Tmct0grupoejecucionData(Tmct0grupoejecucion entity) {
    entityToData(entity, this);
  }

  public Tmct0grupoejecucionData(Tmct0grupoejecucionData entity) {
    this.setAsigcomp(entity.getAsigcomp());
    this.setAuditFechaCambio(entity.getAuditFechaCambio());
    this.setAuditUser(entity.getAuditUser());
    this.setCdcamara(entity.getCdcamara());
    this.setCdsettmkt(entity.getCdsettmkt());
    this.setCdtipoop(entity.getCdtipoop());
    this.setFeejecuc(entity.getFeejecuc());
    this.setFevalor(entity.getFevalor());
    this.setIdgrupoeje(null);
    this.setImpreciomkt(entity.getImpreciomkt());
    this.setNuagreje(entity.getNuagreje());
    this.setNutitulos(BigDecimal.ZERO);
    this.setTradingvenue(entity.getTradingvenue());
  }

  public static Tmct0grupoejecucionData entityToData(Tmct0grupoejecucion entity) {
    Tmct0grupoejecucionData data = new Tmct0grupoejecucionData();
    entityToData(entity, data);
    return data;
  }// public static Tmct0grupoejecucionData entityToData(Tmct0grupoejecucion
   // entity){

  private static void entityToData(Tmct0grupoejecucion entity, Tmct0grupoejecucionData data) {
    data.setAsigcomp(entity.getAsigcomp());
    data.setAuditFechaCambio(entity.getAuditFechaCambio());
    data.setAuditUser(entity.getAuditUser());
    data.setCdcamara(entity.getCdcamara());
    data.setCdsettmkt(entity.getCdsettmkt());
    data.setCdtipoop(entity.getCdtipoop());
    data.setFeejecuc(entity.getFeejecuc());
    data.setFevalor(entity.getFevalor());
    data.setIdgrupoeje(entity.getIdgrupoeje());
    data.setImpreciomkt(entity.getImpreciomkt());
    data.setNuagreje(entity.getNuagreje());
    data.setNutitulos(entity.getNutitulos());
    if (entity.getTmct0alo() != null) {
      data.setTmct0alo(Tmct0AloData.entityToData(entity.getTmct0alo()));
    }// if (entity.getTmct0alo() != null){
    data.setTradingvenue(entity.getTradingvenue());
  }// private static void entityToData(Tmct0grupoejecucion entity,
   // Tmct0grupoejecucionData data) {

  public static Tmct0grupoejecucion dataToEntity(Tmct0grupoejecucionData data) {
    Tmct0grupoejecucion entity = new Tmct0grupoejecucion();
    dataToEntity(data, entity);
    return entity;
  }// public static Tmct0grupoejecucion dataToEntity(Tmct0grupoejecucion
   // data){

  public static void dataToEntity(Tmct0grupoejecucionData data, Tmct0grupoejecucion entity) {
    entity.setAsigcomp(data.getAsigcomp());
    entity.setAuditFechaCambio(data.getAuditFechaCambio());
    entity.setAuditUser(data.getAuditUser());
    entity.setCdcamara(data.getCdcamara());
    entity.setCdsettmkt(data.getCdsettmkt());
    entity.setCdtipoop(data.getCdtipoop());
    entity.setFeejecuc(data.getFeejecuc());
    entity.setFevalor(data.getFevalor());
    entity.setIdgrupoeje(data.getIdgrupoeje());
    entity.setImpreciomkt(data.getImpreciomkt());
    entity.setNuagreje(data.getNuagreje());
    entity.setNutitulos(data.getNutitulos());
    if (data.getTmct0alo() != null) {
      entity.setTmct0alo(Tmct0AloData.dataToEntity(data.getTmct0alo()));
    }// if (data.getTmct0alo() != null) {
    entity.setTradingvenue(data.getTradingvenue());
    entity.setTmct0ejecucionalocations(Tmct0ejecucionalocationData.dataListToEntityList(data.getTmct0ejecucionalocations()));
  }// public static Tmct0grupoejecucion dataToEntity(Tmct0grupoejecucion
   // data){

  public Long getIdgrupoeje() {
    return idgrupoeje;
  }

  public void setIdgrupoeje(Long idgrupoeje) {
    this.idgrupoeje = idgrupoeje;
  }

  public Tmct0AloData getTmct0alo() {
    return tmct0alo;
  }

  public void setTmct0alo(Tmct0AloData tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  public BigDecimal getNutitulos() {
    return nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public BigDecimal getImpreciomkt() {
    return impreciomkt;
  }

  public void setImpreciomkt(BigDecimal impreciomkt) {
    this.impreciomkt = impreciomkt;
  }

  public String getCdtipoop() {
    return cdtipoop;
  }

  public void setCdtipoop(String cdtipoop) {
    this.cdtipoop = cdtipoop;
  }

  public String getCdsettmkt() {
    return cdsettmkt;
  }

  public void setCdsettmkt(String cdsettmkt) {
    this.cdsettmkt = cdsettmkt;
  }

  public String getCdcamara() {
    return cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  public Date getAuditFechaCambio() {
    return auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public Character getAsigcomp() {
    return asigcomp;
  }

  public void setAsigcomp(Character asigcomp) {
    this.asigcomp = asigcomp;
  }

  public String getTradingvenue() {
    return tradingvenue;
  }

  public void setTradingvenue(String tradingvenue) {
    this.tradingvenue = tradingvenue;
  }

  public String getNuagreje() {
    return nuagreje;
  }

  public void setNuagreje(String nuagreje) {
    this.nuagreje = nuagreje;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public List<Tmct0ejecucionalocationData> getTmct0ejecucionalocations() {
    return tmct0ejecucionalocations;
  }

  public void setTmct0ejecucionalocations(List<Tmct0ejecucionalocationData> tmct0ejecucionalocations) {
    this.tmct0ejecucionalocations = tmct0ejecucionalocations;
  }

}// public class Tmct0grupoejecucionData implements java.io.Serializable {
