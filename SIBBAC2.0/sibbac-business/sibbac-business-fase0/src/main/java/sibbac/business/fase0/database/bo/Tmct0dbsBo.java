package sibbac.business.fase0.database.bo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0dbsDao;
import sibbac.business.wrappers.database.model.Tmct0dbs;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cms;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0dir;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0eje;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0nom;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ord;
import sibbac.database.megbpdta.model.FmegEntityInterface;

@Service
public class Tmct0dbsBo extends AbstractBo<Tmct0dbs, Long, Tmct0dbsDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0dbsBo.class);

  private static final String TGRL0ORD = "TGRL0ORD";
  private static final String TGRL0EJE = "TGRL0EJE";
  private static final String TGRL0CMS = "TGRL0CMS";
  private static final String TGRL0DIR = "TGRL0DIR";
  private static final String TGRL0NOM = "TGRL0NOM";

  @Transactional(propagation = Propagation.REQUIRED)
  public void insertRegister(Tgrl0ord ord, boolean isNew) {
    this.insertRegister(TGRL0ORD, ord.getNuoprout(), (short) 0, 0, 0, isNew);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void insertRegister(Tgrl0eje eje, boolean isNew) {
    this.insertRegister(TGRL0EJE, eje.getId().getNuoprout(), eje.getId().getNupareje(), 0, 0, isNew);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void insertRegister(Tgrl0cms cms, boolean isNew) {
    this.insertRegister(TGRL0CMS, cms.getId().getNuoprout(), cms.getId().getNupareje(),
        (int) cms.getId().getNuregcom(), 0, isNew);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void insertRegister(Tgrl0dir dir, boolean isNew) {
    this.insertRegister(TGRL0DIR, dir.getId().getNuoprout(), dir.getId().getNupareje(),
        (int) dir.getId().getNusecnbr(), 0, isNew);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void insertRegister(Tgrl0nom nom, boolean isNew) {
    this.insertRegister(TGRL0NOM, nom.getId().getNuoprout(), nom.getId().getNupareje(),
        (int) nom.getId().getNusecnbr(), (int) nom.getId().getNusectit(), isNew);
  }

  private void insertRegister(String tabla, Integer nuoprout, short nupareje, int sec1, int sec2, boolean isNew) {
    Character tipo = 'M';
    if (isNew) {
      tipo = 'A';
    }
    Tmct0dbs dbs = null;
    Date fecha = new Date();
    LOG.debug("[Tmct0dbsBo :: insertRegister] buscando {}##{}##{}##{}##{}##{}##{}...", tabla, fecha, nuoprout,
        nupareje, sec1, sec2, isNew);
    List<Tmct0dbs> dbss = dao.findAllByNtablaAndFechaAndNuoproutAndNuparejeAndNsec1AndNsec2(tabla, fecha, nuoprout,
        nupareje, sec1, sec2);
    if (CollectionUtils.isNotEmpty(dbss)) {
      LOG.debug("[Tmct0dbsBo :: insertRegister] actualiznado antiguo {}##{}##{}##{}##{}##{}##{}...", tabla, fecha,
          nuoprout, nupareje, sec1, sec2, isNew);
      dbs = dbss.get(0);
    }
    else {
      LOG.debug("[Tmct0dbsBo :: insertRegister] creando nuevo {}##{}##{}##{}##{}##{}##{}...", tabla, fecha, nuoprout,
          nupareje, sec1, sec2, isNew);
      dbs = new Tmct0dbs();
      dbs.setNtabla(tabla);
      dbs.setFecha(fecha);
      dbs.setNuoprout(nuoprout);
      dbs.setNupareje(nupareje);
      dbs.setNsec1(sec1);
      dbs.setNsec2(sec2);
      dbs.setAuxiliar("");
    }
    dbs.setTipo(tipo);
    dbs.setCdestado('N');
    dbs.setFhaudit(new Date());
    dbs.setCdusuaud("SIBBAC20");

    save(dbs);
  }

  @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_UNCOMMITTED)
  public Tmct0dbs insertRegisterFmeg(FmegEntityInterface fei, boolean isNew) {
    return this.insertRegisterFmeg(fei.getNombreTabla(), fei.getSincronizeId(), isNew);
  }

  private Tmct0dbs insertRegisterFmeg(String tabla, String auxiliar, boolean isNew) {
    Character tipo = 'M';
    if (isNew) {
      tipo = 'A';
    }
    Tmct0dbs dbs = null;
    Date fecha = new Date();

    SimpleDateFormat sdf = new SimpleDateFormat("HHmmssSSS");
    final String out = sdf.format(new Date(System.currentTimeMillis()));

    LOG.debug("[Tmct0dbsBo :: insertRegister] buscando {}##{}##{}##{}...", tabla, fecha, auxiliar, isNew);
    List<Tmct0dbs> dbss = dao.findAllByNtablaAndFechaAndAuxiliar(tabla, fecha, auxiliar);
    if (CollectionUtils.isNotEmpty(dbss)) {
      LOG.debug("[Tmct0dbsBo :: insertRegister] actualiznado antiguo {}##{}##{}##{}...", tabla, fecha, auxiliar, isNew);
      dbs = dbss.get(0);
    }
    else {
      LOG.debug("[Tmct0dbsBo :: insertRegister] creando nuevo {}##{}##{}##{}...", tabla, fecha, auxiliar, isNew);
      dbs = new Tmct0dbs();
      dbs.setNtabla(tabla);
      dbs.setFecha(fecha);
      dbs.setNuoprout(Integer.parseInt(out));
      dbs.setNupareje((short) 1);
      dbs.setNsec1(1);
      dbs.setNsec2(0);
      dbs.setAuxiliar(auxiliar);
    }
    dbs.setTipo(tipo);
    dbs.setCdestado('N');
    dbs.setFhaudit(new Date());
    dbs.setCdusuaud("SIBBAC20");

    dbs = save(dbs);
    return dbs;
  }
}