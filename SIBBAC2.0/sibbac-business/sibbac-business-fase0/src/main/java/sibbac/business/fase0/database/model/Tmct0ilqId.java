package sibbac.business.fase0.database.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table TMCT0ILQ. INSTRUCCIONES LIQUIDACION Documentación de la tablaTMCT0ILQ <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 

 **/
@Embeddable
public class Tmct0ilqId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -5500563057639468645L;

  /**
   * Table Field CDALIASS. CODIGO ALIAS CUENTA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDALIASS", nullable = false, length = 20)
  protected java.lang.String cdaliass;

  /**
   * Table Field CDSUBCTA. CODIGO SUBCUENTA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDSUBCTA", nullable = false, length = 20)
  protected java.lang.String cdsubcta;

  /**
   * Table Field CDCLEARE. ENTIDAD LIQUIDADORA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDCLEARE", nullable = false, length = 25)
  protected java.lang.String cdcleare;

  /**
   * Table Field CENTRO. CENTRO APLICACION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CENTRO", nullable = false, length = 30)
  protected java.lang.String centro;

  /**
   * Table Field CDMERCAD. TIPO MERCADO APLICACION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDMERCAD", nullable = false, length = 20)
  protected java.lang.String cdmercad;

  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUMSEC", nullable = false, precision = 20, scale = 0)
  protected java.math.BigDecimal numsec;

  public Tmct0ilqId() {
    super();
  }

  public Tmct0ilqId(String cdaliass, String cdsubcta, String cdcleare, String centro, String cdmercad, BigDecimal numsec) {
    this();
    this.cdaliass = cdaliass;
    this.cdsubcta = cdsubcta;
    this.cdcleare = cdcleare;
    this.centro = centro;
    this.cdmercad = cdmercad;
    this.numsec = numsec;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public java.lang.String getCentro() {
    return centro;
  }

  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public void setCentro(java.lang.String centro) {
    this.centro = centro;
  }

  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public void setNumsec(java.math.BigDecimal numsec) {
    this.numsec = numsec;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdaliass == null) ? 0 : cdaliass.hashCode());
    result = prime * result + ((cdcleare == null) ? 0 : cdcleare.hashCode());
    result = prime * result + ((cdmercad == null) ? 0 : cdmercad.hashCode());
    result = prime * result + ((cdsubcta == null) ? 0 : cdsubcta.hashCode());
    result = prime * result + ((centro == null) ? 0 : centro.hashCode());
    result = prime * result + ((numsec == null) ? 0 : numsec.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0ilqId other = (Tmct0ilqId) obj;
    if (cdaliass == null) {
      if (other.cdaliass != null)
        return false;
    } else if (!cdaliass.equals(other.cdaliass))
      return false;
    if (cdcleare == null) {
      if (other.cdcleare != null)
        return false;
    } else if (!cdcleare.equals(other.cdcleare))
      return false;
    if (cdmercad == null) {
      if (other.cdmercad != null)
        return false;
    } else if (!cdmercad.equals(other.cdmercad))
      return false;
    if (cdsubcta == null) {
      if (other.cdsubcta != null)
        return false;
    } else if (!cdsubcta.equals(other.cdsubcta))
      return false;
    if (centro == null) {
      if (other.centro != null)
        return false;
    } else if (!centro.equals(other.centro))
      return false;
    if (numsec == null) {
      if (other.numsec != null)
        return false;
    } else if (!numsec.equals(other.numsec))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Tmct0ilqId [cdaliass=" + cdaliass + ", cdsubcta=" + cdsubcta + ", cdcleare=" + cdcleare + ", centro="
           + centro + ", cdmercad=" + cdmercad + ", numsec=" + numsec + "]";
  }

}
