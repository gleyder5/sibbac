package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0OPERACIONCDECC")
public class Tmct0operacioncdecc implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -1920055980260223355L;
  private Long idoperacioncdecc;
  private String cdoperacionecc;
  private char cdsentido;
  private char cdindposicion;
  private char cdtipooperacion;
  private String cdrefasigext;
  private Date fcontratacion;
  private Date fregistroecc;
  private Date hregistroecc;
  private BigDecimal imprecio;
  private BigDecimal imefectivo;
  private BigDecimal imtitulosdisponibles;
  private BigDecimal imefectivodisponible;
  private BigDecimal imtitulosretenidos;
  private BigDecimal imefectivoretenido;
  private String nuoperacionprev;
  private String nuoperacioninic;
  private String cdrefcomun;
  private String cdcamara;
  private String cdsegmento;
  private BigDecimal nutitulos;
  private String nuexemkt;
  private String cdoperacionmkt;
  private String cdmiembromkt;
  private char cdindcotizacion;
  private String cdisin;
  private String nuordenmkt;
  private String cdrefcliente;
  private String cdrefextorden;
  private char cdindcapacidad;
  private String nbordenmkt;
  private Date auditFechaCambio;
  private String auditUser;
  private Date fliqteorica;
  private Date fnegociacion;
  private Date fordenmkt;
  private Date hnegociacion;
  private Date hordenmkt;
  private Character ceenviado;
  private String cdPlataformaNegociacion;
  private BigDecimal corretaje;
  private Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion;

  private String envioS3;

  // private Set<Tmct0histOperacioncdecc> tmct0histOperacioncdeccs = new
  // HashSet<Tmct0histOperacioncdecc>(0);
  // private Set<Tmct0caseoperacioneje> tmct0caseoperacionejes = new
  // HashSet<Tmct0caseoperacioneje>(0);

  public Tmct0operacioncdecc() {
  }

  public Tmct0operacioncdecc(Long idoperacioncdecc,
                             String cdoperacionecc,
                             char cdsentido,
                             char cdindposicion,
                             char cdtipooperacion,
                             String cdrefasigext,
                             Date fcontratacion,
                             Date fregistroecc,
                             Date hregistroecc,
                             BigDecimal imprecio,
                             String nuoperacionprev,
                             String nuoperacioninic,
                             String cdrefcomun,
                             String cdcamara,
                             String cdsegmento,
                             String nuexemkt,
                             String cdoperacionmkt,
                             String cdmiembromkt,
                             char cdindcotizacion,
                             String cdisin,
                             String nuordenmkt,
                             char cdindcapacidad,
                             Date fnegociacion,
                             Date fordenmkt,
                             Date hnegociacion,
                             Date hordenmkt,
                             String cdPlataformaNegociacion,
                             BigDecimal corretaje) {
    this.idoperacioncdecc = idoperacioncdecc;
    this.cdoperacionecc = cdoperacionecc;
    this.cdsentido = cdsentido;
    this.cdindposicion = cdindposicion;
    this.cdtipooperacion = cdtipooperacion;
    this.cdrefasigext = cdrefasigext;
    this.fcontratacion = fcontratacion;
    this.fregistroecc = fregistroecc;
    this.hregistroecc = hregistroecc;
    this.imprecio = imprecio;
    this.nuoperacionprev = nuoperacionprev;
    this.nuoperacioninic = nuoperacioninic;
    this.cdrefcomun = cdrefcomun;
    this.cdcamara = cdcamara;
    this.cdsegmento = cdsegmento;
    this.nuexemkt = nuexemkt;
    this.cdoperacionmkt = cdoperacionmkt;
    this.cdmiembromkt = cdmiembromkt;
    this.cdindcotizacion = cdindcotizacion;
    this.cdisin = cdisin;
    this.nuordenmkt = nuordenmkt;
    this.cdindcapacidad = cdindcapacidad;
    this.fnegociacion = fnegociacion;
    this.fordenmkt = fordenmkt;
    this.hnegociacion = hnegociacion;
    this.hordenmkt = hordenmkt;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.corretaje = corretaje;
  }

  public Tmct0operacioncdecc(Long idoperacioncdecc,
                             String cdoperacionecc,
                             char cdsentido,
                             char cdindposicion,
                             char cdtipooperacion,
                             String cdrefasigext,
                             Date fcontratacion,
                             Date fregistroecc,
                             Date hregistroecc,
                             BigDecimal imprecio,
                             BigDecimal imefectivo,
                             BigDecimal imtitulosdisponibles,
                             BigDecimal imefectivodisponible,
                             BigDecimal imtitulosretenidos,
                             BigDecimal imefectivoretenido,
                             String nuoperacionprev,
                             String nuoperacioninic,
                             String cdrefcomun,
                             String cdcamara,
                             String cdsegmento,
                             BigDecimal nutitulos,
                             String nuexemkt,
                             String cdoperacionmkt,
                             String cdmiembromkt,
                             char cdindcotizacion,
                             String cdisin,
                             String nuordenmkt,
                             String cdrefcliente,
                             String cdrefextorden,
                             char cdindcapacidad,
                             String nbordenmkt,
                             Date auditFechaCambio,
                             String auditUser,
                             Date fliqteorica,
                             Date fnegociacion,
                             Date fordenmkt,
                             Date hnegociacion,
                             Date hordenmkt,
                             Character ceenviado,
                             String cdPlataformaNegociacion,
                             BigDecimal corretaje/*
                                                  * , Set< Tmct0histOperacioncdecc > tmct0histOperacioncdeccs , Set<
                                                  * Tmct0caseoperacioneje > tmct0caseoperacionejes
                                                  */) {
    this.idoperacioncdecc = idoperacioncdecc;
    this.cdoperacionecc = cdoperacionecc;
    this.cdsentido = cdsentido;
    this.cdindposicion = cdindposicion;
    this.cdtipooperacion = cdtipooperacion;
    this.cdrefasigext = cdrefasigext;
    this.fcontratacion = fcontratacion;
    this.fregistroecc = fregistroecc;
    this.hregistroecc = hregistroecc;
    this.imprecio = imprecio;
    this.imefectivo = imefectivo;
    this.imtitulosdisponibles = imtitulosdisponibles;
    this.imefectivodisponible = imefectivodisponible;
    this.imtitulosretenidos = imtitulosretenidos;
    this.imefectivoretenido = imefectivoretenido;
    this.nuoperacionprev = nuoperacionprev;
    this.nuoperacioninic = nuoperacioninic;
    this.cdrefcomun = cdrefcomun;
    this.cdcamara = cdcamara;
    this.cdsegmento = cdsegmento;
    this.nutitulos = nutitulos;
    this.nuexemkt = nuexemkt;
    this.cdoperacionmkt = cdoperacionmkt;
    this.cdmiembromkt = cdmiembromkt;
    this.cdindcotizacion = cdindcotizacion;
    this.cdisin = cdisin;
    this.nuordenmkt = nuordenmkt;
    this.cdrefcliente = cdrefcliente;
    this.cdrefextorden = cdrefextorden;
    this.cdindcapacidad = cdindcapacidad;
    this.nbordenmkt = nbordenmkt;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.fliqteorica = fliqteorica;
    this.fnegociacion = fnegociacion;
    this.fordenmkt = fordenmkt;
    this.hnegociacion = hnegociacion;
    this.hordenmkt = hordenmkt;
    this.ceenviado = ceenviado;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.corretaje = corretaje;
    // this.tmct0histOperacioncdeccs = tmct0histOperacioncdeccs;
    // this.tmct0caseoperacionejes = tmct0caseoperacionejes;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDOPERACIONCDECC", unique = true, nullable = false)
  public Long getIdoperacioncdecc() {
    return this.idoperacioncdecc;
  }

  public void setIdoperacioncdecc(Long idoperacioncdecc) {
    this.idoperacioncdecc = idoperacioncdecc;
  }

  @Column(name = "CDOPERACIONECC", nullable = false, length = 16)
  public String getCdoperacionecc() {
    return this.cdoperacionecc;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  @Column(name = "CDSENTIDO", nullable = false, length = 1)
  public char getCdsentido() {
    return this.cdsentido;
  }

  public void setCdsentido(char cdsentido) {
    this.cdsentido = cdsentido;
  }

  @Column(name = "CDINDPOSICION", nullable = false, length = 1)
  public char getCdindposicion() {
    return this.cdindposicion;
  }

  public void setCdindposicion(char cdindposicion) {
    this.cdindposicion = cdindposicion;
  }

  @Column(name = "CDTIPOOPERACION", nullable = false, length = 1)
  public char getCdtipooperacion() {
    return this.cdtipooperacion;
  }

  public void setCdtipooperacion(char cdtipooperacion) {
    this.cdtipooperacion = cdtipooperacion;
  }

  @Column(name = "CDREFASIGEXT", nullable = false, length = 16)
  public String getCdrefasigext() {
    return this.cdrefasigext;
  }

  public void setCdrefasigext(String cdrefasigext) {
    this.cdrefasigext = cdrefasigext;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FCONTRATACION", nullable = false, length = 10)
  public Date getFcontratacion() {
    return this.fcontratacion;
  }

  public void setFcontratacion(Date fcontratacion) {
    this.fcontratacion = fcontratacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FREGISTROECC", nullable = false, length = 10)
  public Date getFregistroecc() {
    return this.fregistroecc;
  }

  public void setFregistroecc(Date fregistroecc) {
    this.fregistroecc = fregistroecc;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HREGISTROECC", nullable = false, length = 8)
  public Date getHregistroecc() {
    return this.hregistroecc;
  }

  public void setHregistroecc(Date hregistroecc) {
    this.hregistroecc = hregistroecc;
  }

  @Column(name = "IMPRECIO", nullable = false, precision = 7, scale = 6)
  public BigDecimal getImprecio() {
    return this.imprecio;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  @Column(name = "IMEFECTIVO", precision = 13, scale = 2)
  public BigDecimal getImefectivo() {
    return this.imefectivo;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  @Column(name = "IMTITULOSDISPONIBLES", precision = 18, scale = 6)
  public BigDecimal getImtitulosdisponibles() {
    return this.imtitulosdisponibles;
  }

  public void setImtitulosdisponibles(BigDecimal imtitulosdisponibles) {
    this.imtitulosdisponibles = imtitulosdisponibles;
  }

  @Column(name = "IMEFECTIVODISPONIBLE", precision = 13, scale = 2)
  public BigDecimal getImefectivodisponible() {
    return this.imefectivodisponible;
  }

  public void setImefectivodisponible(BigDecimal imefectivodisponible) {
    this.imefectivodisponible = imefectivodisponible;
  }

  @Column(name = "IMTITULOSRETENIDOS", precision = 18, scale = 6)
  public BigDecimal getImtitulosretenidos() {
    return this.imtitulosretenidos;
  }

  public void setImtitulosretenidos(BigDecimal imtitulosretenidos) {
    this.imtitulosretenidos = imtitulosretenidos;
  }

  @Column(name = "IMEFECTIVORETENIDO", precision = 13, scale = 2)
  public BigDecimal getImefectivoretenido() {
    return this.imefectivoretenido;
  }

  public void setImefectivoretenido(BigDecimal imefectivoretenido) {
    this.imefectivoretenido = imefectivoretenido;
  }

  @Column(name = "NUOPERACIONPREV", nullable = false, length = 16)
  public String getNuoperacionprev() {
    return this.nuoperacionprev;
  }

  public void setNuoperacionprev(String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  @Column(name = "NUOPERACIONINIC", nullable = false, length = 16)
  public String getNuoperacioninic() {
    return this.nuoperacioninic;
  }

  public void setNuoperacioninic(String nuoperacioninic) {
    this.nuoperacioninic = nuoperacioninic;
  }

  @Column(name = "CDREFCOMUN", nullable = false, length = 16)
  public String getCdrefcomun() {
    return this.cdrefcomun;
  }

  public void setCdrefcomun(String cdrefcomun) {
    this.cdrefcomun = cdrefcomun;
  }

  @Column(name = "CDCAMARA", nullable = false, length = 11)
  public String getCdcamara() {
    return this.cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  @Column(name = "CDSEGMENTO", nullable = false, length = 2)
  public String getCdsegmento() {
    return this.cdsegmento;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  @Column(name = "NUTITULOS", precision = 12, scale = 6)
  public BigDecimal getNutitulos() {
    return this.nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  @Column(name = "NUEXEMKT", nullable = false, length = 9)
  public String getNuexemkt() {
    return this.nuexemkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  @Column(name = "CDOPERACIONMKT", nullable = false, length = 2)
  public String getCdoperacionmkt() {
    return this.cdoperacionmkt;
  }

  public void setCdoperacionmkt(String cdoperacionmkt) {
    this.cdoperacionmkt = cdoperacionmkt;
  }

  @Column(name = "CDMIEMBROMKT", nullable = false, length = 10)
  public String getCdmiembromkt() {
    return this.cdmiembromkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  @Column(name = "CDINDCOTIZACION", nullable = false, length = 1)
  public char getCdindcotizacion() {
    return this.cdindcotizacion;
  }

  public void setCdindcotizacion(char cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  @Column(name = "CDISIN", nullable = false, length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "NUORDENMKT", nullable = false, length = 9)
  public String getNuordenmkt() {
    return this.nuordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  @Column(name = "CDREFCLIENTE", length = 16)
  public String getCdrefcliente() {
    return this.cdrefcliente;
  }

  public void setCdrefcliente(String cdrefcliente) {
    this.cdrefcliente = cdrefcliente;
  }

  @Column(name = "CDREFEXTORDEN", length = 15)
  public String getCdrefextorden() {
    return this.cdrefextorden;
  }

  public void setCdrefextorden(String cdrefextorden) {
    this.cdrefextorden = cdrefextorden;
  }

  @Column(name = "CDINDCAPACIDAD", nullable = false, length = 1)
  public char getCdindcapacidad() {
    return this.cdindcapacidad;
  }

  public void setCdindcapacidad(char cdindcapacidad) {
    this.cdindcapacidad = cdindcapacidad;
  }

  @Column(name = "NBORDENMKT", length = 80)
  public String getNbordenmkt() {
    return this.nbordenmkt;
  }

  public void setNbordenmkt(String nbordenmkt) {
    this.nbordenmkt = nbordenmkt;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FLIQTEORICA", length = 10)
  public Date getFliqteorica() {
    return this.fliqteorica;
  }

  public void setFliqteorica(Date fliqteorica) {
    this.fliqteorica = fliqteorica;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FNEGOCIACION", nullable = false, length = 10)
  public Date getFnegociacion() {
    return this.fnegociacion;
  }

  public void setFnegociacion(Date fnegociacion) {
    this.fnegociacion = fnegociacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FORDENMKT", nullable = false, length = 10)
  public Date getFordenmkt() {
    return this.fordenmkt;
  }

  public void setFordenmkt(Date fordenmkt) {
    this.fordenmkt = fordenmkt;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HNEGOCIACION", nullable = false, length = 8)
  public Date getHnegociacion() {
    return this.hnegociacion;
  }

  public void setHnegociacion(Date hnegociacion) {
    this.hnegociacion = hnegociacion;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HORDENMKT", nullable = false, length = 8)
  public Date getHordenmkt() {
    return this.hordenmkt;
  }

  public void setHordenmkt(Date hordenmkt) {
    this.hordenmkt = hordenmkt;
  }

  @Column(name = "CEENVIADO", length = 1)
  public Character getCeenviado() {
    return this.ceenviado;
  }

  public void setCeenviado(Character ceenviado) {
    this.ceenviado = ceenviado;
  }

  @Column(name = "CD_PLATAFORMA_NEGOCIACION", nullable = false, length = 4)
  public String getCdPlataformaNegociacion() {
    return this.cdPlataformaNegociacion;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  @Column(name = "CORRETAJE", nullable = false, precision = 15)
  public BigDecimal getCorretaje() {
    return this.corretaje;
  }

  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDCUENTACOMP", referencedColumnName = "ID_CUENTA_COMPENSACION")
  public Tmct0CuentasDeCompensacion getTmct0CuentasDeCompensacion() {
    return tmct0CuentasDeCompensacion;
  }

  public void setTmct0CuentasDeCompensacion(Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion) {
    this.tmct0CuentasDeCompensacion = tmct0CuentasDeCompensacion;
  }

  @Column(name = "ENVIOS3", length = 1, nullable = true)
  public String getEnvioS3() {
    return envioS3;
  }

  public void setEnvioS3(String envioS3) {
    this.envioS3 = envioS3;
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0operacioncdecc")
  // public Set<Tmct0histOperacioncdecc> getTmct0histOperacioncdeccs() {
  // return this.tmct0histOperacioncdeccs;
  // }
  //
  // public void setTmct0histOperacioncdeccs(
  // Set<Tmct0histOperacioncdecc> tmct0histOperacioncdeccs) {
  // this.tmct0histOperacioncdeccs = tmct0histOperacioncdeccs;
  // }
  //
  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0operacioncdecc")
  // public Set<Tmct0caseoperacioneje> getTmct0caseoperacionejes() {
  // return this.tmct0caseoperacionejes;
  // }
  //
  // public void setTmct0caseoperacionejes(
  // Set<Tmct0caseoperacioneje> tmct0caseoperacionejes) {
  // this.tmct0caseoperacionejes = tmct0caseoperacionejes;
  // }

}
