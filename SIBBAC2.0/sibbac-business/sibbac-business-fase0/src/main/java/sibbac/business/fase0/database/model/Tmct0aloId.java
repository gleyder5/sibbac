package sibbac.business.fase0.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0aloId implements java.io.Serializable {

  private static final long serialVersionUID = 3010027472118714639L;
  private String nucnfclt = "";
  private String nbooking = "";
  private String nuorden = "";

  public Tmct0aloId() {
  }

  public Tmct0aloId(String nucnfclt, String nbooking, String nuorden) {
    this.nucnfclt = nucnfclt;
    this.nbooking = nbooking;
    this.nuorden = nuorden;
  }

  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  public String getNucnfclt() {
    return this.nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  @Column(name = "NBOOKING", nullable = false, length = 20)
  public String getNbooking() {
    return this.nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  @Column(name = "NUORDEN", nullable = false, length = 20)
  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public boolean equals(Object other) {
    if ((this == other))
      return true;
    if ((other == null))
      return false;
    if (!(other instanceof Tmct0aloId))
      return false;
    Tmct0aloId castOther = (Tmct0aloId) other;

    return ((this.getNucnfclt() == castOther.getNucnfclt()) || (this.getNucnfclt() != null
        && castOther.getNucnfclt() != null && this.getNucnfclt().equals(castOther.getNucnfclt())))
        && ((this.getNbooking() == castOther.getNbooking()) || (this.getNbooking() != null
            && castOther.getNbooking() != null && this.getNbooking().equals(castOther.getNbooking())))
        && ((this.getNuorden() == castOther.getNuorden()) || (this.getNuorden() != null
            && castOther.getNuorden() != null && this.getNuorden().equals(castOther.getNuorden())));
  }

  public int hashCode() {
    int result = 17;

    result = 37 * result + (getNucnfclt() == null ? 0 : this.getNucnfclt().hashCode());
    result = 37 * result + (getNbooking() == null ? 0 : this.getNbooking().hashCode());
    result = 37 * result + (getNuorden() == null ? 0 : this.getNuorden().hashCode());
    return result;
  }

  @Override
  public String toString() {
    return String.format("[Tmct0ALO:%s##%s##%s]", this.nuorden, this.nbooking, this.nucnfclt);
  }

}
