package sibbac.business.wrappers.database.data;


import sibbac.business.wrappers.database.model.TipoCuentaLiquidacion;


public class TipoCuentaLiquidacionData {

	private long	id;
	private String	name;
	private String	descripcion;

	public TipoCuentaLiquidacionData( TipoCuentaLiquidacion tpCta ) {
		this.id = tpCta.getId();
		this.name = tpCta.getCdCodigo();
		this.descripcion = tpCta.getNbDescripcion();
	}

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
