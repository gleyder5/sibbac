package sibbac.business.fase0.database.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TptiInDTO implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 5222413738262566840L;

  /**
   * Constructor I. Constructor por defecto.
   */
  public TptiInDTO() {
  } // TptiInDTO

  public TptiInDTO(Long id, String message) {
    this.setId(id);
    this.setMessage(message);
  } // TptiInDTO

  public TptiInDTO(Long id, String message, Character withoutError) {
    this.setId(id);
    this.setMessage(message);
    this.setWithoutError(withoutError);
  } // TptiInDTO

  public TptiInDTO(String messageType, Date messageDate) {
    this.messageType = messageType;
    this.messageDate = messageDate;
  }

  private Long id;
  private String member;
  private Date tradedate;
  private Date messageDate;
  private Date messageHour;
  private String messageType;
  private Integer messageLength;
  private Character scriptDividend;
  private String idBatch;
  private Integer batchOrder;
  private Character isLast;
  private String idMessage;
  private String groupData;
  private Character processed;
  private Date audtFechaCambio;
  private String audtUser;
  private String message;
  private Character withoutError;

  private List<Long> idsToProcess;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
    tsb.append("ID", id);
    tsb.append("MEMBER", member);
    tsb.append("TRADEDATE", tradedate);
    tsb.append("MESSAGE_DATE", messageDate);
    tsb.append("MESSAGE_HOUR", messageHour);
    tsb.append("MESSAGETYPE", messageType);
    tsb.append("MESSAGELENGTH", messageLength);
    tsb.append("SCRIPTDIVIDEND", scriptDividend);
    tsb.append("IDBATCH", idBatch);
    tsb.append("BATCH_ORDER", batchOrder);
    tsb.append("IS_LAST", isLast);
    tsb.append("IDMESSAGE", idMessage);
    tsb.append("GROUP_DATA", groupData);
    tsb.append("PROCESSED", processed);
    tsb.append("AUDIT_FECHA_CAMBIO", audtFechaCambio);
    tsb.append("AUDIT_USER", audtUser);
    tsb.append("MESSAGE", message);
    tsb.append("WITHOUT_ERROR", withoutError);
    return tsb.toString();
  }// public String toString() {

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMember() {
    return member;
  }

  public void setMember(String member) {
    this.member = member;
  }

  public Date getTradedate() {
    return tradedate;
  }

  public void setTradedate(Date tradedate) {
    this.tradedate = tradedate;
  }

  public Date getMessageDate() {
    return messageDate;
  }

  public void setMessageDate(Date messageDate) {
    this.messageDate = messageDate;
  }

  public Date getMessageHour() {
    return messageHour;
  }

  public void setMessageHour(Date messageHour) {
    this.messageHour = messageHour;
  }

  public String getMessageType() {
    return messageType;
  }

  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }

  public Integer getMessageLength() {
    return messageLength;
  }

  public void setMessageLength(Integer messageLength) {
    this.messageLength = messageLength;
  }

  public Character getScriptDividend() {
    return scriptDividend;
  }

  public void setScriptDividend(Character scriptDividend) {
    this.scriptDividend = scriptDividend;
  }

  public String getIdBatch() {
    return idBatch;
  }

  public void setIdBatch(String idBatch) {
    this.idBatch = idBatch;
  }

  public Integer getBatchOrder() {
    return batchOrder;
  }

  public void setBatchOrder(Integer batchOrder) {
    this.batchOrder = batchOrder;
  }

  public Character getIsLast() {
    return isLast;
  }

  public void setIsLast(Character isLast) {
    this.isLast = isLast;
  }

  public String getIdMessage() {
    return idMessage;
  }

  public void setIdMessage(String idMessage) {
    this.idMessage = idMessage;
  }

  public String getGroupData() {
    return groupData;
  }

  public void setGroupData(String groupData) {
    this.groupData = groupData;
  }

  public Character getProcessed() {
    return processed;
  }

  public void setProcessed(Character processed) {
    this.processed = processed;
  }

  public Date getAudtFechaCambio() {
    return audtFechaCambio;
  }

  public void setAudtFechaCambio(Date audtFechaCambio) {
    this.audtFechaCambio = audtFechaCambio;
  }

  public String getAudtUser() {
    return audtUser;
  }

  public void setAudtUser(String audtUser) {
    this.audtUser = audtUser;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Character getWithoutError() {
    return withoutError;
  }

  public void setWithoutError(Character withoutError) {
    this.withoutError = withoutError;
  }

  public List<Long> getIdsToProcess() {
    return idsToProcess;
  }

  public void setIdsToProcess(List<Long> idsToProcess) {
    this.idsToProcess = idsToProcess;
  }

}// public class TptiInDTO implements Serializable {
