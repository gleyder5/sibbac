package sibbac.business.wrappers.database.model;


public class OperacionEspecialRecordBean extends RecordBean {

  /**
	 * 
	 */
  private static final long serialVersionUID = 823238593813426693L;

  public enum TipoPersona {
    FISICA('F'),
    JURIDICA('J');

    private final Character tipoPersona;

    /**
     * @param tipoPersona
     */
    private TipoPersona(final Character tipoPersona) {
      this.tipoPersona = tipoPersona;
    }

    /**
     * @return the tipoPersona
     */
    public Character getTipoPersona() {
      return tipoPersona;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return tipoPersona.toString();
    }

    public static TipoPersona getTipoPersona(Character tipoPersona) {
      TipoPersona[] fields = TipoPersona.values();
      TipoPersona tp = null;
      for (int i = 0; i < fields.length; i++) {
        if (fields[i].tipoPersona.equals(tipoPersona)) {
          tp = fields[i];
          break;
        }
      }
      return tp;
    }
  }

}
