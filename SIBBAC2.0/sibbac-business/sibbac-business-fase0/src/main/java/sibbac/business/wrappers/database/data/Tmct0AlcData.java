package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;

public class Tmct0AlcData implements Serializable {
  /**
     * 
     */
  private static final long serialVersionUID = 1L;
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0AlcData.class);
  private static final String TAG = Tmct0AlcData.class.getName();
  private Tmct0alcId id;
  private Tmct0EstadoData estadoentrec;
  private Tmct0EstadoData estadocont;
  private Tmct0EstadoData estadotit;
  private BigDecimal nutitliq;
  private String cdentliq;
  private BigDecimal imcombco;
  private BigDecimal imcomdvo;
  private BigDecimal imcomsvb;
  private Date feejeliq;
  private Date feopeliq;
  private String cdestliq;
  private String cdrefban;
  private String cdmoniso;
  private BigDecimal imntbrok;
  private String cdmodord;
  private String cdenvmor;
  private String nbtitliq;
  private String cdniftit;
  private BigDecimal pccombco;
  private BigDecimal pccomdev;
  private BigDecimal pccombrk;
  private String cdcomisn;
  private String tpdsaldo;
  private BigDecimal imnetliq;
  private BigDecimal imnfiliq;
  private String cdenvliq;
  private String nurefbrk;
  private String cdentdep;
  private String cddepcle;
  private Character cdcleari;
  private String cdreftit;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuversion;
  private String desenliq;
  private String cdordtit;
  private String cdcustod;
  private String rftitaud;
  private String acctatcle;
  private String pkentity;
  private Character flagliq;
  private String ourpar;
  private String theirpar;
  private BigDecimal imefeagr;
  private BigDecimal imcombrk;
  private BigDecimal imfinsvb;
  private BigDecimal imajusvb;
  private BigDecimal imtotnet;
  private BigDecimal imtotbru;
  private BigDecimal pccomisn;
  private BigDecimal imcomisn;
  private BigDecimal imderliq;
  private BigDecimal imderges;
  private char cdtyp;
  private Character cdsntprt;
  private Long idinfocomp;
  private Integer cdestadoasig;
  private Character caseecc;
  private Character asigorden;
  private Character block;
  private Date fenvasignacion;
  private Date fenvtitular;
  private BigDecimal imcanoncompdv;
  private BigDecimal imcanoncompeu;
  private BigDecimal imcanoncontrdv;
  private BigDecimal imcanoncontreu;
  private BigDecimal imcanonliqdv;
  private BigDecimal imcanonliqeu;
  private Date fhenvios3;
  private Character enviadoCuentaVirtual;
  private Date concEfMercado;
  private Date concEfCliente;
  private Tmct0aloId aloId;

  public Tmct0AlcData(Tmct0alc tmct0alc) {
    entityToData(tmct0alc, this);
  }

  public Tmct0AlcData() {
    //
  }

  public static Tmct0AlcData entityToData(Tmct0alc entity) {
    LOG.debug("[entityToData(Tmct0alc entity)] Inicio ...");
    Tmct0AlcData data = new Tmct0AlcData();
    entityToData(entity, data);
    LOG.debug("[entityToData(Tmct0alc entity)] Fin ...");
    return data;
  }

  private static void entityToData(Tmct0alc entity, Tmct0AlcData data) {
    LOG.debug("[entityToData(Tmct0alc entity, Tmct0AlcData data)] Inicio ...");
    data.setAcctatcle(entity.getAcctatcle());
    data.setAsigorden(entity.getAsigorden());
    data.setBlock(entity.getBlock());
    data.setCaseecc(entity.getCaseecc());
    data.setCdcleari(entity.getCdcleari());
    data.setCdcomisn(entity.getCdcomisn());
    data.setCdcustod(entity.getCdcustod());
    data.setCddepcle(entity.getCddepcle());
    data.setCdentdep(entity.getCdentdep());
    data.setCdentliq(entity.getCdentliq());
    data.setCdenvliq(entity.getCdenvliq());
    data.setCdestadoasig(entity.getCdestadoasig());
    data.setCdenvmor(entity.getCdenvmor());
    data.setCdestliq(entity.getCdestliq());
    data.setCdmodord(entity.getCdmodord());
    data.setCdmoniso(entity.getCdmoniso());
    data.setCdniftit(entity.getCdniftit());
    data.setCdordtit(entity.getCdordtit());
    data.setCdrefban(entity.getCdrefban());
    data.setCdreftit(entity.getCdreftit());
    data.setCdsntprt(entity.getCdsntprt());
    data.setCdtyp(entity.getCdtyp());
    data.setCdusuaud(entity.getCdusuaud());
    data.setConcEfCliente(entity.getConcEfCliente());
    data.setConcEfMercado(entity.getConcEfMercado());
    data.setDesenliq(entity.getDesenliq());
    data.setEnviadoCuentaVirtual(entity.getEnviadoCuentaVirtual());
    if (entity.getEstadocont() != null) {
      data.setEstadocont(Tmct0EstadoData.entityToData(entity.getEstadocont()));
    }
    if (entity.getEstadoentrec() != null) {
      data.setEstadoentrec(Tmct0EstadoData.entityToData(entity.getEstadoentrec()));
    }
    if (entity.getEstadotit() != null) {
      data.setEstadotit(Tmct0EstadoData.entityToData(entity.getEstadotit()));
    }
    data.setFeejeliq(entity.getFeejeliq());
    data.setFenvasignacion(entity.getFenvasignacion());
    data.setFenvtitular(entity.getFenvtitular());
    data.setFeopeliq(entity.getFeopeliq());
    data.setFhaudit(entity.getFhaudit());
    data.setFhenvios3(entity.getFhenvios3());
    data.setFlagliq(entity.getFlagliq());
    data.setId(entity.getId());
    data.setIdinfocomp(entity.getIdinfocomp());
    data.setImajusvb(entity.getImajusvb());
    data.setImcanoncompdv(entity.getImcanoncompdv());
    data.setImcanoncompeu(entity.getImcanoncompeu());
    data.setImcanoncontrdv(entity.getImcanoncontrdv());
    data.setImcanoncontreu(entity.getImcanoncontreu());
    data.setImcanonliqeu(entity.getImcanonliqeu());
    data.setImcombco(entity.getImcombco());
    data.setImcombrk(entity.getImcombrk());
    data.setImcomdvo(entity.getImcomdvo());
    data.setImcomisn(entity.getImcomisn());
    data.setImcomsvb(entity.getImcomsvb());
    data.setImcomisn(entity.getImcomisn());
    data.setImcomsvb(entity.getImcomsvb());
    data.setImderges(entity.getImderges());
    data.setImderliq(entity.getImderliq());
    data.setImfinsvb(entity.getImfinsvb());
    data.setImnetliq(entity.getImnetliq());
    data.setImnfiliq(entity.getImnfiliq());
    data.setImntbrok(entity.getImntbrok());
    data.setImtotbru(entity.getImtotbru());
    data.setImtotnet(entity.getImtotnet());
    data.setNbtitliq(entity.getNbtitliq());
    data.setNurefbrk(entity.getNurefbrk());
    data.setNutitliq(entity.getNutitliq());
    data.setNuversion(entity.getNuversion());
    data.setOurpar(entity.getOurpar());
    data.setPccombco(entity.getPccombco());
    data.setPccombrk(entity.getPccombrk());
    data.setPccomdev(entity.getPccomdev());
    data.setPccomisn(entity.getPccomisn());
    data.setPkentity(entity.getPkentity());
    data.setRftitaud(entity.getRftitaud());
    data.setTheirpar(entity.getTheirpar());
    data.setTpdsaldo(entity.getTpdsaldo());
    if (entity.getTmct0alo() != null && entity.getTmct0alo().getId() != null) {
      LOG.debug(TAG + " :: entityToData insertando id del Alo en el AlcData ....");
      data.setAloId(entity.getTmct0alo().getId());
    }
    LOG.debug("[entityToData(Tmct0alc entity, Tmct0AlcData data)] Fin ...");
  } // entityToData

  public Tmct0alcId getId() {
    return id;
  }

  public void setId(Tmct0alcId id) {
    this.id = id;
  }

  public Tmct0EstadoData getEstadoentrec() {
    return estadoentrec;
  }

  public void setEstadoentrec(Tmct0EstadoData estadoentrec) {
    this.estadoentrec = estadoentrec;
  }

  public Tmct0EstadoData getEstadocont() {
    return estadocont;
  }

  public void setEstadocont(Tmct0EstadoData estadocont) {
    this.estadocont = estadocont;
  }

  public Tmct0EstadoData getEstadotit() {
    return estadotit;
  }

  public void setEstadotit(Tmct0EstadoData estadotit) {
    this.estadotit = estadotit;
  }

  public BigDecimal getNutitliq() {
    return nutitliq;
  }

  public void setNutitliq(BigDecimal nutitliq) {
    this.nutitliq = nutitliq;
  }

  public String getCdentliq() {
    return cdentliq;
  }

  public void setCdentliq(String cdentliq) {
    this.cdentliq = cdentliq;
  }

  public BigDecimal getImcombco() {
    return imcombco;
  }

  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  public BigDecimal getImcomdvo() {
    return imcomdvo;
  }

  public void setImcomdvo(BigDecimal imcomdvo) {
    this.imcomdvo = imcomdvo;
  }

  public BigDecimal getImcomsvb() {
    return imcomsvb;
  }

  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  public Date getFeejeliq() {
    return feejeliq;
  }

  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  public Date getFeopeliq() {
    return feopeliq;
  }

  public void setFeopeliq(Date feopeliq) {
    this.feopeliq = feopeliq;
  }

  public String getCdestliq() {
    return cdestliq;
  }

  public void setCdestliq(String cdestliq) {
    this.cdestliq = cdestliq;
  }

  public String getCdrefban() {
    return cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public String getCdmoniso() {
    return cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public BigDecimal getImntbrok() {
    return imntbrok;
  }

  public void setImntbrok(BigDecimal imntbrok) {
    this.imntbrok = imntbrok;
  }

  public String getCdmodord() {
    return cdmodord;
  }

  public void setCdmodord(String cdmodord) {
    this.cdmodord = cdmodord;
  }

  public String getCdenvmor() {
    return cdenvmor;
  }

  public void setCdenvmor(String cdenvmor) {
    this.cdenvmor = cdenvmor;
  }

  public String getNbtitliq() {
    return nbtitliq;
  }

  public void setNbtitliq(String nbtitliq) {
    this.nbtitliq = nbtitliq;
  }

  public String getCdniftit() {
    return cdniftit;
  }

  public void setCdniftit(String cdniftit) {
    this.cdniftit = cdniftit;
  }

  public BigDecimal getPccombco() {
    return pccombco;
  }

  public void setPccombco(BigDecimal pccombco) {
    this.pccombco = pccombco;
  }

  public BigDecimal getPccomdev() {
    return pccomdev;
  }

  public void setPccomdev(BigDecimal pccomdev) {
    this.pccomdev = pccomdev;
  }

  public BigDecimal getPccombrk() {
    return pccombrk;
  }

  public void setPccombrk(BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  public String getCdcomisn() {
    return cdcomisn;
  }

  public void setCdcomisn(String cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  public String getTpdsaldo() {
    return tpdsaldo;
  }

  public void setTpdsaldo(String tpdsaldo) {
    this.tpdsaldo = tpdsaldo;
  }

  public BigDecimal getImnetliq() {
    return imnetliq;
  }

  public void setImnetliq(BigDecimal imnetliq) {
    this.imnetliq = imnetliq;
  }

  public BigDecimal getImnfiliq() {
    return imnfiliq;
  }

  public void setImnfiliq(BigDecimal imnfiliq) {
    this.imnfiliq = imnfiliq;
  }

  public String getCdenvliq() {
    return cdenvliq;
  }

  public void setCdenvliq(String cdenvliq) {
    this.cdenvliq = cdenvliq;
  }

  public String getNurefbrk() {
    return nurefbrk;
  }

  public void setNurefbrk(String nurefbrk) {
    this.nurefbrk = nurefbrk;
  }

  public String getCdentdep() {
    return cdentdep;
  }

  public void setCdentdep(String cdentdep) {
    this.cdentdep = cdentdep;
  }

  public String getCddepcle() {
    return cddepcle;
  }

  public void setCddepcle(String cddepcle) {
    this.cddepcle = cddepcle;
  }

  public Character getCdcleari() {
    return cdcleari;
  }

  public void setCdcleari(Character cdcleari) {
    this.cdcleari = cdcleari;
  }

  public String getCdreftit() {
    return cdreftit;
  }

  public void setCdreftit(String cdreftit) {
    this.cdreftit = cdreftit;
  }

  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public Integer getNuversion() {
    return nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  public String getDesenliq() {
    return desenliq;
  }

  public void setDesenliq(String desenliq) {
    this.desenliq = desenliq;
  }

  public String getCdordtit() {
    return cdordtit;
  }

  public void setCdordtit(String cdordtit) {
    this.cdordtit = cdordtit;
  }

  public String getCdcustod() {
    return cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public String getRftitaud() {
    return rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  public String getAcctatcle() {
    return acctatcle;
  }

  public void setAcctatcle(String acctatcle) {
    this.acctatcle = acctatcle;
  }

  public String getPkentity() {
    return pkentity;
  }

  public void setPkentity(String pkentity) {
    this.pkentity = pkentity;
  }

  public Character getFlagliq() {
    return flagliq;
  }

  public void setFlagliq(Character flagliq) {
    this.flagliq = flagliq;
  }

  public String getOurpar() {
    return ourpar;
  }

  public void setOurpar(String ourpar) {
    this.ourpar = ourpar;
  }

  public String getTheirpar() {
    return theirpar;
  }

  public void setTheirpar(String theirpar) {
    this.theirpar = theirpar;
  }

  public BigDecimal getImefeagr() {
    return imefeagr;
  }

  public void setImefeagr(BigDecimal imefeagr) {
    this.imefeagr = imefeagr;
  }

  public BigDecimal getImcombrk() {
    return imcombrk;
  }

  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  public BigDecimal getImfinsvb() {
    return imfinsvb;
  }

  public void setImfinsvb(BigDecimal imfinsvb) {
    this.imfinsvb = imfinsvb;
  }

  public BigDecimal getImajusvb() {
    return imajusvb;
  }

  public void setImajusvb(BigDecimal imajusvb) {
    this.imajusvb = imajusvb;
  }

  public BigDecimal getImtotnet() {
    return imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public BigDecimal getImtotbru() {
    return imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public BigDecimal getPccomisn() {
    return pccomisn;
  }

  public void setPccomisn(BigDecimal pccomisn) {
    this.pccomisn = pccomisn;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public BigDecimal getImderliq() {
    return imderliq;
  }

  public void setImderliq(BigDecimal imderliq) {
    this.imderliq = imderliq;
  }

  public BigDecimal getImderges() {
    return imderges;
  }

  public void setImderges(BigDecimal imderges) {
    this.imderges = imderges;
  }

  public char getCdtyp() {
    return cdtyp;
  }

  public void setCdtyp(char cdtyp) {
    this.cdtyp = cdtyp;
  }

  public Character getCdsntprt() {
    return cdsntprt;
  }

  public void setCdsntprt(Character cdsntprt) {
    this.cdsntprt = cdsntprt;
  }

  public Long getIdinfocomp() {
    return idinfocomp;
  }

  public void setIdinfocomp(Long idinfocomp) {
    this.idinfocomp = idinfocomp;
  }

  public Integer getCdestadoasig() {
    return cdestadoasig;
  }

  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  public Character getCaseecc() {
    return caseecc;
  }

  public void setCaseecc(Character caseecc) {
    this.caseecc = caseecc;
  }

  public Character getAsigorden() {
    return asigorden;
  }

  public void setAsigorden(Character asigorden) {
    this.asigorden = asigorden;
  }

  public Character getBlock() {
    return block;
  }

  public void setBlock(Character block) {
    this.block = block;
  }

  public Date getFenvasignacion() {
    return fenvasignacion;
  }

  public void setFenvasignacion(Date fenvasignacion) {
    this.fenvasignacion = fenvasignacion;
  }

  public Date getFenvtitular() {
    return fenvtitular;
  }

  public void setFenvtitular(Date fenvtitular) {
    this.fenvtitular = fenvtitular;
  }

  public BigDecimal getImcanoncompdv() {
    return imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  public BigDecimal getImcanoncompeu() {
    return imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  public BigDecimal getImcanoncontrdv() {
    return imcanoncontrdv;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  public BigDecimal getImcanoncontreu() {
    return imcanoncontreu;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  public BigDecimal getImcanonliqdv() {
    return imcanonliqdv;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  public BigDecimal getImcanonliqeu() {
    return imcanonliqeu;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  public Date getFhenvios3() {
    return fhenvios3;
  }

  public void setFhenvios3(Date fhenvios3) {
    this.fhenvios3 = fhenvios3;
  }

  public Character getEnviadoCuentaVirtual() {
    return enviadoCuentaVirtual;
  }

  public void setEnviadoCuentaVirtual(Character enviadoCuentaVirtual) {
    this.enviadoCuentaVirtual = enviadoCuentaVirtual;
  }

  public Date getConcEfMercado() {
    return concEfMercado;
  }

  public void setConcEfMercado(Date concEfMercado) {
    this.concEfMercado = concEfMercado;
  }

  public Date getConcEfCliente() {
    return concEfCliente;
  }

  public void setConcEfCliente(Date concEfCliente) {
    this.concEfCliente = concEfCliente;
  }

  public Tmct0aloId getAloId() {
    return aloId;
  }

  public void setAloId(Tmct0aloId aloId) {
    this.aloId = aloId;
  }

  public static List<Tmct0AlcData> entityListToDataList(List<Tmct0alc> tmct0alcs) {
    LOG.debug("[entityListToDataList] Inicio ...");
    List<Tmct0AlcData> dataList = new ArrayList<Tmct0AlcData>();
    for (Tmct0alc entity : tmct0alcs) {
      LOG.debug("[entityListToDataList] Procesando alc ... " + entity.toString());
      dataList.add(entityToData(entity));
    }
    LOG.debug("[entityListToDataList] Fin ...");
    return dataList;
  }

  public static List<Tmct0alc> dataListToEntityList(List<Tmct0AlcData> tmct0AlcDataList) {
    List<Tmct0alc> listEntity = null;
    if (tmct0AlcDataList != null) {
      listEntity = new ArrayList<Tmct0alc>();
      if (!tmct0AlcDataList.isEmpty()) {
        Iterator<Tmct0AlcData> iterator = tmct0AlcDataList.iterator();
        while (iterator.hasNext()) {
          Tmct0AlcData data = iterator.next();
          listEntity.add(Tmct0AlcData.dataToEntity(data));
        }
      }
    }
    return listEntity;
  }

  private static Tmct0alc dataToEntity(Tmct0AlcData data) {
    Tmct0alc entity = new Tmct0alc();
    Tmct0AlcData.dataToEntity(data, entity);
    return entity;
  }

  private static void dataToEntity(Tmct0AlcData data, Tmct0alc entity) {
    entity.setAcctatcle(data.getAcctatcle());
    entity.setAsigorden(data.getAsigorden());
    entity.setBlock(data.getBlock());
    entity.setCaseecc(data.getCaseecc());
    entity.setCdcleari(data.getCdcleari());
    entity.setCdcomisn(data.getCdcomisn());
    entity.setCdcustod(data.getCdcustod());
    entity.setCddepcle(data.getCddepcle());
    entity.setCdentdep(data.getCdentdep());
    entity.setCdentliq(data.getCdentliq());
    entity.setCdenvliq(data.getCdenvliq());
    entity.setCdestadoasig(data.getCdestadoasig());
    entity.setCdenvmor(data.getCdenvmor());
    entity.setCdestliq(data.getCdestliq());
    entity.setCdmodord(data.getCdmodord());
    entity.setCdmoniso(data.getCdmoniso());
    entity.setCdniftit(data.getCdniftit());
    entity.setCdordtit(data.getCdordtit());
    entity.setCdrefban(data.getCdrefban());
    entity.setCdreftit(data.getCdreftit());
    entity.setCdsntprt(data.getCdsntprt());
    entity.setCdtyp(data.getCdtyp());
    entity.setCdusuaud(data.getCdusuaud());
    entity.setConcEfCliente(data.getConcEfCliente());
    entity.setConcEfMercado(data.getConcEfMercado());
    entity.setDesenliq(data.getDesenliq());
    entity.setEnviadoCuentaVirtual(data.getEnviadoCuentaVirtual());
    if (data.getEstadocont() != null) {
      entity.setEstadocont(Tmct0EstadoData.dataToEntity(data.getEstadocont()));
    }
    if (data.getEstadoentrec() != null) {
      entity.setEstadoentrec(Tmct0EstadoData.dataToEntity(data.getEstadoentrec()));
    }
    if (data.getEstadotit() != null) {
      entity.setEstadotit(Tmct0EstadoData.dataToEntity(data.getEstadotit()));
    }
    entity.setFeejeliq(data.getFeejeliq());
    entity.setFenvasignacion(data.getFenvasignacion());
    entity.setFenvtitular(data.getFenvtitular());
    entity.setFeopeliq(data.getFeopeliq());
    entity.setFhaudit(data.getFhaudit());
    entity.setFhenvios3(data.getFhenvios3());
    entity.setFlagliq(data.getFlagliq());
    entity.setId(data.getId());
    entity.setIdinfocomp(data.getIdinfocomp());
    entity.setImajusvb(data.getImajusvb());
    entity.setImcanoncompdv(data.getImcanoncompdv());
    entity.setImcanoncompeu(data.getImcanoncompeu());
    entity.setImcanoncontrdv(data.getImcanoncontrdv());
    entity.setImcanoncontreu(data.getImcanoncontreu());
    entity.setImcanonliqeu(data.getImcanonliqeu());
    entity.setImcombco(data.getImcombco());
    entity.setImcombrk(data.getImcombrk());
    entity.setImcomdvo(data.getImcomdvo());
    entity.setImcomisn(data.getImcomisn());
    entity.setImcomsvb(data.getImcomsvb());
    entity.setImcomisn(data.getImcomisn());
    entity.setImcomsvb(data.getImcomsvb());
    entity.setImderges(data.getImderges());
    entity.setImderliq(data.getImderliq());
    entity.setImfinsvb(data.getImfinsvb());
    entity.setImnetliq(data.getImnetliq());
    entity.setImnfiliq(data.getImnfiliq());
    entity.setImntbrok(data.getImntbrok());
    entity.setImtotbru(data.getImtotbru());
    entity.setImtotnet(data.getImtotnet());
    entity.setNbtitliq(data.getNbtitliq());
    entity.setNurefbrk(data.getNurefbrk());
    entity.setNutitliq(data.getNutitliq());
    entity.setNuversion(data.getNuversion());
    entity.setOurpar(data.getOurpar());
    entity.setPccombco(data.getPccombco());
    entity.setPccombrk(data.getPccombrk());
    entity.setPccomdev(data.getPccomdev());
    entity.setPccomisn(data.getPccomisn());
    entity.setPkentity(data.getPkentity());
    entity.setRftitaud(data.getRftitaud());
    entity.setTheirpar(data.getTheirpar());
    entity.setTpdsaldo(data.getTpdsaldo());
    if (data.getAloId() != null) {
      LOG.debug(TAG + " :: entityToData insertando id del Alo en el AlcData ....");
      if (entity.getTmct0alo() == null) {
        entity.setTmct0alo(new Tmct0alo());
      }
      entity.getTmct0alo().setId(data.getAloId());
    }
  }

  public static List<Tmct0AlcData> copyDataList(List<Tmct0AlcData> tmct0AlcDataList) {
    List<Tmct0AlcData> lstAlcData = null;
    if (CollectionUtils.isNotEmpty(tmct0AlcDataList)) {
      lstAlcData = new ArrayList<Tmct0AlcData>();
      for (Tmct0AlcData data : tmct0AlcDataList) {
        lstAlcData.add(Tmct0AlcData.copy(data));

      }
    }

    return lstAlcData;
  }

  private static Tmct0AlcData copy(Tmct0AlcData data) {
    Tmct0AlcData copy = new Tmct0AlcData();
    copy.setAcctatcle(data.getAcctatcle());
    copy.setAsigorden(data.getAsigorden());
    copy.setBlock(data.getBlock());
    copy.setCaseecc(data.getCaseecc());
    copy.setCdcleari(data.getCdcleari());
    copy.setCdcomisn(data.getCdcomisn());
    copy.setCdcustod(data.getCdcustod());
    copy.setCddepcle(data.getCddepcle());
    copy.setCdentdep(data.getCdentdep());
    copy.setCdentliq(data.getCdentliq());
    copy.setCdenvliq(data.getCdenvliq());
    copy.setCdestadoasig(data.getCdestadoasig());
    copy.setCdenvmor(data.getCdenvmor());
    copy.setCdestliq(data.getCdestliq());
    copy.setCdmodord(data.getCdmodord());
    copy.setCdmoniso(data.getCdmoniso());
    copy.setCdniftit(data.getCdniftit());
    copy.setCdordtit(data.getCdordtit());
    copy.setCdrefban(data.getCdrefban());
    copy.setCdreftit(data.getCdreftit());
    copy.setCdsntprt(data.getCdsntprt());
    copy.setCdtyp(data.getCdtyp());
    copy.setCdusuaud(data.getCdusuaud());
    copy.setConcEfCliente(data.getConcEfCliente());
    copy.setConcEfMercado(data.getConcEfMercado());
    copy.setDesenliq(data.getDesenliq());
    copy.setEnviadoCuentaVirtual(data.getEnviadoCuentaVirtual());
    if (data.getEstadocont() != null) {
      copy.setEstadocont(Tmct0EstadoData.copy(data.getEstadocont()));
    }
    if (data.getEstadoentrec() != null) {
      copy.setEstadoentrec(Tmct0EstadoData.copy(data.getEstadoentrec()));
    }
    if (data.getEstadotit() != null) {
      copy.setEstadotit(Tmct0EstadoData.copy(data.getEstadotit()));
    }
    copy.setFeejeliq(data.getFeejeliq());
    copy.setFenvasignacion(data.getFenvasignacion());
    copy.setFenvtitular(data.getFenvtitular());
    copy.setFeopeliq(data.getFeopeliq());
    copy.setFhaudit(data.getFhaudit());
    copy.setFhenvios3(data.getFhenvios3());
    copy.setFlagliq(data.getFlagliq());
    copy.setId(data.getId());
    copy.setIdinfocomp(data.getIdinfocomp());
    copy.setImajusvb(data.getImajusvb());
    copy.setImcanoncompdv(data.getImcanoncompdv());
    copy.setImcanoncompeu(data.getImcanoncompeu());
    copy.setImcanoncontrdv(data.getImcanoncontrdv());
    copy.setImcanoncontreu(data.getImcanoncontreu());
    copy.setImcanonliqeu(data.getImcanonliqeu());
    copy.setImcombco(data.getImcombco());
    copy.setImcombrk(data.getImcombrk());
    copy.setImcomdvo(data.getImcomdvo());
    copy.setImcomisn(data.getImcomisn());
    copy.setImcomsvb(data.getImcomsvb());
    copy.setImcomisn(data.getImcomisn());
    copy.setImcomsvb(data.getImcomsvb());
    copy.setImderges(data.getImderges());
    copy.setImderliq(data.getImderliq());
    copy.setImfinsvb(data.getImfinsvb());
    copy.setImnetliq(data.getImnetliq());
    copy.setImnfiliq(data.getImnfiliq());
    copy.setImntbrok(data.getImntbrok());
    copy.setImtotbru(data.getImtotbru());
    copy.setImtotnet(data.getImtotnet());
    copy.setNbtitliq(data.getNbtitliq());
    copy.setNurefbrk(data.getNurefbrk());
    copy.setNutitliq(data.getNutitliq());
    copy.setNuversion(data.getNuversion());
    copy.setOurpar(data.getOurpar());
    copy.setPccombco(data.getPccombco());
    copy.setPccombrk(data.getPccombrk());
    copy.setPccomdev(data.getPccomdev());
    copy.setPccomisn(data.getPccomisn());
    copy.setPkentity(data.getPkentity());
    copy.setRftitaud(data.getRftitaud());
    copy.setTheirpar(data.getTheirpar());
    copy.setTpdsaldo(data.getTpdsaldo());
    if (data.getAloId() != null) {
      LOG.debug(TAG + " :: copyToData insertando id del Alo en el AlcData ....");
      copy.setAloId(new Tmct0aloId());
      copy.getAloId().setNbooking(data.getAloId().getNbooking());
      copy.getAloId().setNucnfclt(data.getAloId().getNucnfclt());
      copy.getAloId().setNuorden(data.getAloId().getNuorden());
    }
    return copy;
  }

}
