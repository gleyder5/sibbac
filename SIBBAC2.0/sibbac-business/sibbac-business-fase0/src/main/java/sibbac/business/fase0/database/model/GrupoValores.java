package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_GRUPOS_VALORES" )
public class GrupoValores extends MasterData< GrupoValores > {

	private static final long	serialVersionUID	= -8440949222010046955L;
}
