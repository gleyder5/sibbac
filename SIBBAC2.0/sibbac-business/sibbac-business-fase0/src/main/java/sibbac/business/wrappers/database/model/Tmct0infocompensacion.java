package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0INFOCOMPENSACION")
public class Tmct0infocompensacion implements Serializable {

  /**
   * Número de serie generado automáticamente
   */
  private static final long serialVersionUID = -8297613027726143837L;
  private Long idinfocomp;
  private Tmct0Regla tmct0Regla;
  private String cdnmotecnico;
  private String cdctacompensacion;
  private String cdmiembrocompensador;
  private String cdrefasignacion;
  private String errormsg;
  private Date auditFechaCambio;
  private String auditUser;

  public Tmct0infocompensacion() {
  }

  public Tmct0infocompensacion(Long idinfocomp) {
    this.idinfocomp = idinfocomp;
  }

  public Tmct0infocompensacion(Long idinfocomp, Tmct0Regla tmct0Regla, String cdnmotecnico, String cdctacompensacion,
      String cdmiembrocompensador, String cdrefasignacion, String errormsg, Date auditFechaCambio, String auditUser) {
    this.idinfocomp = idinfocomp;
    this.tmct0Regla = tmct0Regla;
    this.cdnmotecnico = cdnmotecnico;
    this.cdctacompensacion = cdctacompensacion;
    this.cdmiembrocompensador = cdmiembrocompensador;
    this.cdrefasignacion = cdrefasignacion;
    this.errormsg = errormsg;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDINFOCOMP", unique = true, nullable = false)
  public Long getIdinfocomp() {
    return this.idinfocomp;
  }

  public void setIdinfocomp(Long idinfocomp) {
    this.idinfocomp = idinfocomp;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_REGLA")
  public Tmct0Regla getTmct0Regla() {
    return this.tmct0Regla;
  }

  public void setTmct0Regla(Tmct0Regla tmct0Regla) {
    this.tmct0Regla = tmct0Regla;
  }

  @Column(name = "CDNMOTECNICO", length = 10)
  public String getCdnmotecnico() {
    return this.cdnmotecnico;
  }

  public void setCdnmotecnico(String cdnmotecnico) {
    this.cdnmotecnico = cdnmotecnico;
  }

  @Column(name = "CDCTACOMPENSACION", length = 3)
  public String getCdctacompensacion() {
    return this.cdctacompensacion;
  }

  public void setCdctacompensacion(String cdctacompensacion) {
    this.cdctacompensacion = cdctacompensacion;
  }

  @Column(name = "CDMIEMBROCOMPENSADOR", length = 4)
  public String getCdmiembrocompensador() {
    return this.cdmiembrocompensador;
  }

  public void setCdmiembrocompensador(String cdmiembrocompensador) {
    this.cdmiembrocompensador = cdmiembrocompensador;
  }

  @Column(name = "CDREFASIGNACION", length = 18)
  public String getCdrefasignacion() {
    return this.cdrefasignacion;
  }

  public void setCdrefasignacion(String cdrefasignacion) {
    this.cdrefasignacion = cdrefasignacion;
  }

  @Column(name = "ERRORMSG", length = 500)
  public String getErrormsg() {
    return this.errormsg;
  }

  public void setErrormsg(String errormsg) {
    this.errormsg = errormsg;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Transient
  public boolean isOk() {
    return getCdctacompensacion() != null && !getCdctacompensacion().trim().isEmpty()
        || getCdmiembrocompensador() != null && !getCdmiembrocompensador().trim().isEmpty()
        && getCdrefasignacion() != null && !getCdrefasignacion().trim().isEmpty();
  }

}
