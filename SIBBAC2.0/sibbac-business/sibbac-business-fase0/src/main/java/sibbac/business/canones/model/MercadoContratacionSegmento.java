package sibbac.business.canones.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;

/**
 * Segmento de un mercado de contratación
 */
@Entity
@Table(name = "TMCT0_MERCADO_CONTRATACION_SEGMENTO")
public class MercadoContratacionSegmento extends DescriptedCanonEntity<MercadoContratacionSegmentoId> {

  /**
   * número de serie
   */
  private static final long serialVersionUID = 7926296767684884739L;
  
  @EmbeddedId
  public MercadoContratacionSegmentoId id;
  
  @Override
  public MercadoContratacionSegmentoId getId() {
    return id;
  }
  
  @Override
  public void setId(MercadoContratacionSegmentoId id) {
    this.id = id;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    MercadoContratacionSegmento other = (MercadoContratacionSegmento) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return toString("Segmento de mercado contratación");
  }

}
