package sibbac.business.mantenimientodatoscliente.database.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Embeddable
public class Tmct0fisId implements java.io.Serializable {
	
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0fisId.class);

	private static final long serialVersionUID = 1L;

	@Column(name = "CDDCLAVE", nullable = false, length = 20)
	private String cddclave = "";

	@Column(name = "NUMSEC", nullable = false, precision = 20)
	private BigDecimal numsec;

	public String getCddclave() {
		return cddclave;
	}

	public void setCddclave(String cddclave) {
		this.cddclave = cddclave;
	}

	public BigDecimal getNumsec() {
		return numsec;
	}

	public void setNumsec(BigDecimal numsec) {
		this.numsec = numsec;
	}

	public Tmct0fisId() {
		LOG.info("Contruido Tmct0fisId");
	}

	public Tmct0fisId(String cddclave, BigDecimal numsec) {

		this.cddclave = cddclave;
		this.numsec = numsec;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cddclave == null) ? 0 : cddclave.hashCode());
		result = prime * result + ((numsec == null) ? 0 : numsec.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tmct0fisId other = (Tmct0fisId) obj;
		if (cddclave == null) {
			if (other.cddclave != null)
				return false;
		} else if (!cddclave.equals(other.cddclave))
			return false;
		if (numsec == null) {
			if (other.numsec != null)
				return false;
		} else if (!numsec.equals(other.numsec))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[Tmct0FIS: " + this.cddclave + "/" + this.numsec + "]";
	}

}
