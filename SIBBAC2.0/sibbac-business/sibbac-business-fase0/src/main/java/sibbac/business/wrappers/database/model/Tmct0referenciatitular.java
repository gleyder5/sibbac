package sibbac.business.wrappers.database.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0REFERENCIATITULAR")
public class Tmct0referenciatitular implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 5078129811459928343L;
  private Long idreferencia;
  private String cdreftitularpti;
  private String referenciaAdicional;
  private Date auditFechaCambio = new Date();
  private String auditUser;
  private List<Tmct0enviotitularidad> tmct0enviotitularidads = new ArrayList<Tmct0enviotitularidad>();
  private List<Tmct0referenciatitularFis> tmct0referenciatitularfis = new ArrayList<Tmct0referenciatitularFis>();

  private Long seqVal;

  // private List<Tmct0referenciatitularFis> tmct0referenciatitularFises = new
  // HashList<Tmct0referenciatitularFis>(0);

  public Tmct0referenciatitular() {
  }

  public Tmct0referenciatitular(Long idreferencia, String cdreftitularpti) {
    this.idreferencia = idreferencia;
    this.cdreftitularpti = cdreftitularpti;
  }

  public Tmct0referenciatitular(Long idreferencia, String cdreftitularpti, Date auditFechaCambio, String auditUser,
      List<Tmct0enviotitularidad> tmct0enviotitularidads/*
                                                         * , List<
                                                         * Tmct0referenciatitularFis
                                                         * >
                                                         * tmct0referenciatitularFises
                                                         */) {
    this.idreferencia = idreferencia;
    this.cdreftitularpti = cdreftitularpti;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.tmct0enviotitularidads = tmct0enviotitularidads;
    // this.tmct0referenciatitularFises = tmct0referenciatitularFises;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDREFERENCIA", unique = true, nullable = false)
  public Long getIdreferencia() {
    return this.idreferencia;
  }

  public void setIdreferencia(Long idreferencia) {
    this.idreferencia = idreferencia;
  }

  @Column(name = "CDREFTITULARPTI", unique = true, nullable = false, length = 20)
  public String getCdreftitularpti() {
    return this.cdreftitularpti;
  }

  public void setCdreftitularpti(String cdreftitularpti) {
    this.cdreftitularpti = cdreftitularpti;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0referenciatitular")
  public List<Tmct0enviotitularidad> getTmct0enviotitularidads() {
    return this.tmct0enviotitularidads;
  }

  public void setTmct0enviotitularidads(List<Tmct0enviotitularidad> tmct0enviotitularidads) {
    this.tmct0enviotitularidads = tmct0enviotitularidads;
  }

  @Column(name = "REFERENCIA_ADICIONAL", length = 20, nullable = false)
  public String getReferenciaAdicional() {
    return referenciaAdicional;
  }

  public void setReferenciaAdicional(String referenciaAdicional) {
    this.referenciaAdicional = referenciaAdicional;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "referenciaTitular", cascade = CascadeType.PERSIST)
  public List<Tmct0referenciatitularFis> getTmct0referenciatitularfis() {
    return tmct0referenciatitularfis;
  }

  public void setTmct0referenciatitularfis(List<Tmct0referenciatitularFis> tmct0referenciatitularfis) {
    this.tmct0referenciatitularfis = tmct0referenciatitularfis;
  }

  @Override
  public String toString() {
    return "Tmct0referenciatitular [idreferencia=" + idreferencia + ", cdreftitularpti=" + cdreftitularpti
        + ", referenciaAdicional=" + referenciaAdicional + "]";
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0referenciatitular")
  // public List<Tmct0referenciatitularFis> getTmct0referenciatitularFises() {
  // return this.tmct0referenciatitularFises;
  // }
  //
  // public void setTmct0referenciatitularFises(
  // List<Tmct0referenciatitularFis> tmct0referenciatitularFises) {
  // this.tmct0referenciatitularFises = tmct0referenciatitularFises;
  // }
  // @GeneratedValue(strategy = GenerationType.AUTO, generator =
  // "REFERENCIA_TITULAR_SEQ_GEN")
  // @SequenceGenerator(name = "REFERENCIA_TITULAR_SEQ_GEN", sequenceName =
  // "REFERENCIA_TITULAR_SEQ")
  // public Long getNewSequenceForReferenciaTitular() {
  // return seqVal;
  // }

}
