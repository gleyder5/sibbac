package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;

import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.AliasRecordatorio }. Entity:
 * "AliasRecordatorio".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = WRAPPERS.ALIAS_RECORDATORIO )
@Entity
@Component
@Audited
public class AliasRecordatorio extends ATableAudited< AliasRecordatorio > implements java.io.Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 *
	 */
	private static final long	serialVersionUID	= 2149656764136100645L;

	@OneToOne( fetch = FetchType.LAZY, optional = false )
	@JoinColumn( name = "ID_ALIAS", referencedColumnName = "ID", nullable = false )
	private Alias				alias;

	@Column( name = "IMMINIMO", scale = 6, precision = 19 )
	private BigDecimal			importeMinimo;

	@Column( name = "FRECUENCIA", columnDefinition = "int default 30" )
	@XmlAttribute
	private Integer				frecuencia;

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHFECHA_ULTIMO" )
	@XmlAttribute
	private Date				fechaUltimoRecordatorio;

	public AliasRecordatorio() {
	}

	/**
	 * @return the alias
	 */
	public Alias getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *            the alias to set
	 */
	public void setAlias( Alias alias ) {
		this.alias = alias;
	}

	/**
	 * @return the importeMinimo
	 */
	public BigDecimal getImporteMinimo() {
		return importeMinimo;
	}

	/**
	 * @param importeMinimo
	 *            the importeMinimo to set
	 */
	public void setImporteMinimo( BigDecimal importeMinimo ) {
		this.importeMinimo = importeMinimo;
	}

	public Integer getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia( Integer frecuencia ) {
		this.frecuencia = frecuencia;
	}

	public Date getFechaUltimoRecordatorio() {
		return this.fechaUltimoRecordatorio;
	}

	public void setFechaUltimoRecordatorio( Date fechaUltimoRecordatorio ) {
		this.fechaUltimoRecordatorio = fechaUltimoRecordatorio;
	}

}
