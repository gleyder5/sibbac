package sibbac.business.fase0.database.model;


import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_TIPOS_PRODUCTO" )
public class TiposProducto extends MasterData< TiposProducto > {

	private static final long	serialVersionUID	= 2016475237521523355L;

}
