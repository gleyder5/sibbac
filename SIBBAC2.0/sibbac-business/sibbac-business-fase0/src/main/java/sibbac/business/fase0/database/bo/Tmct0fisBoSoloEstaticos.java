package sibbac.business.fase0.database.bo;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0fisDaoSoloEstaticos;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fisId;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0fisBoSoloEstaticos extends AbstractBo<Tmct0fis, Tmct0fisId, Tmct0fisDaoSoloEstaticos> {

  private final String usuario = "MIDDLE";
  private final String fechaFin = "99991231";

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0fisBoSoloEstaticos.class);

  public Tmct0fis getFisById(String codclave, BigDecimal numsec) {
    return dao.getFisById(codclave, numsec);
  }

  public List<Tmct0fis> findAllByCddclaveOrderNumsecdesc(String codclave) {
    return dao.findAllByCddclaveOrderNumsecdesc(codclave);
  }

} // Tmct0fisBo

