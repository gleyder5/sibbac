package sibbac.business.fase0.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0cfgId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3308148172199999678L;
	private String				application;
	private String				process;
	private String				keyname;

	public Tmct0cfgId() {
	}

	public Tmct0cfgId( String application, String process, String keyname ) {
		this.application = application;
		this.process = process;
		this.keyname = keyname;
	}

	@Column( name = "APPLICATION", nullable = false, length = 50 )
	public String getApplication() {
		return this.application;
	}

	public void setApplication( String application ) {
		this.application = application;
	}

	@Column( name = "PROCESS", nullable = false, length = 50 )
	public String getProcess() {
		return this.process;
	}

	public void setProcess( String process ) {
		this.process = process;
	}

	@Column( name = "KEYNAME", nullable = false, length = 100 )
	public String getKeyname() {
		return this.keyname;
	}

	public void setKeyname( String keyname ) {
		this.keyname = keyname;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0cfgId ) )
			return false;
		Tmct0cfgId castOther = ( Tmct0cfgId ) other;

		return ( ( this.getApplication() == castOther.getApplication() ) || ( this.getApplication() != null
				&& castOther.getApplication() != null && this.getApplication().equals( castOther.getApplication() ) ) )
				&& ( ( this.getProcess() == castOther.getProcess() ) || ( this.getProcess() != null && castOther.getProcess() != null && this
						.getProcess().equals( castOther.getProcess() ) ) )
				&& ( ( this.getKeyname() == castOther.getKeyname() ) || ( this.getKeyname() != null && castOther.getKeyname() != null && this
						.getKeyname().equals( castOther.getKeyname() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getApplication() == null ? 0 : this.getApplication().hashCode() );
		result = 37 * result + ( getProcess() == null ? 0 : this.getProcess().hashCode() );
		result = 37 * result + ( getKeyname() == null ? 0 : this.getKeyname().hashCode() );
		return result;
	}

}
