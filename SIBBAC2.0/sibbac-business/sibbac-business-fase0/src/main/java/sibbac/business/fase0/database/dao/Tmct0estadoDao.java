package sibbac.business.fase0.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0estado;

@Repository
public interface Tmct0estadoDao extends JpaRepository<Tmct0estado, Integer> {

  public List<Tmct0estado> findByIdtipoestado(Integer idtipoestado);

  public List<Tmct0estado> findByIdtipoestadoOrderByNombreAsc(Integer idtipoestado);

  public List<Tmct0estado> findByIdtipoestadoOrderByIdestadoAsc(Integer idtipoestado);

  @Query("SELECT est FROM Tmct0estado est WHERE est.nombre = :nombre AND est.idtipoestado = :idTipoEstado")
  public Tmct0estado findByNombreAndIdTipoEstado(@Param("nombre") String nombre,
      @Param("idTipoEstado") Integer idTipoEstado);

  @Query("SELECT est FROM Tmct0estado est WHERE est.nombre = :nombre")
  public Tmct0estado findByNombre(@Param("nombre") String nombre);

  @Query(value = "SELECT ID_ESTADO1, ID_ESTADO2, ID_ESTADO_EQUIVALENTE "
      + "FROM TMCT0ESTADO_EQUIVALENTE ORDER BY ID_ESTADO1 ASC, ID_ESTADO2 ASC, ID_ESTADO_EQUIVALENTE ASC", nativeQuery = true)
  public List<Object[]> findAllEstadosEquivalentes();
}
