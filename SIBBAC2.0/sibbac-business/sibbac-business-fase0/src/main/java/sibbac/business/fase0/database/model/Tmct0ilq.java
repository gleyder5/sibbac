package sibbac.business.fase0.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0ILQ. INSTRUCCIONES LIQUIDACION
 * Documentación de la tablaTMCT0ILQ <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 * 

 **/
@Entity
@Table(name = "TMCT0ILQ")
public class Tmct0ilq {

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "cdaliass", column = @Column(name = "CDALIASS", nullable = false, length = 20)),
      @AttributeOverride(name = "cdsubcta", column = @Column(name = "CDSUBCTA", nullable = false, length = 20)),
      @AttributeOverride(name = "cdcleare", column = @Column(name = "CDCLEARE", nullable = false, length = 25)),
      @AttributeOverride(name = "centro", column = @Column(name = "CENTRO", nullable = false, length = 30)),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)),
      @AttributeOverride(name = "numsec", column = @Column(name = "NUMSEC", nullable = false, precision = 20, scale = 0)) })
  private Tmct0ilqId id;

  /**
   * Table Field ACCTATCLE. ACCOUNT TO CLEARER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ACCTATCLE", nullable = false, length = 34)
  private java.lang.String acctatcle;

  /**
   * Table Field CDREFBAN. REFERENCIA BANCARIA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDREFBAN", nullable = false, length = 32)
  private java.lang.String cdrefban;

  /**
   * Table Field CDCUSTOD. ENTIDAD LIQUIDADORA CUSTODIO FINAL Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDCUSTOD", nullable = false, length = 25)
  private java.lang.String cdcustod;

  /**
   * Table Field PCCOMBCO. % COMISION BANCO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PCCOMBCO", nullable = false, precision = 7, scale = 4)
  private java.math.BigDecimal pccombco;

  /**
   * Table Field CDCOMISN. TIPO GESTION COMISION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDCOMISN", nullable = false, length = 3)
  private java.lang.String cdcomisn;

  /**
   * Table Field CDMODORD. MODO ORDEN SCLV Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  @Column(name = "CDMODORD", nullable = false, length = 3)
  private java.lang.String cdmodord;

  /**
   * Table Field DESGLOSE. TIPO DESGLOSE LIQUIDADORA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "DESGLOSE", nullable = false, length = 1)
  private Character desglose;

  /**
   * Table Field TCLIENTE. TIPO CLIENTE Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "TCLIENTE", nullable = false, length = 1)
  private java.lang.String tcliente;

  /**
   * Table Field CDUSUAUD. USUARIO AUDITORIA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", nullable = false, length = 10)
  private java.lang.String cdusuaud;

  /**
   * Table Field FHAUDIT. FECHA AUDITORIA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", nullable = false)
  private Date fhaudit;

  /**
   * Table Field FHINICIO. FECHA ALTA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "FHINICIO", nullable = false, precision = 8, scale = 0)
  private java.math.BigDecimal fhinicio;

  /**
   * Table Field FHFINAL. FECHA BAJA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "FHFINAL", nullable = false, precision = 8, scale = 0)
  private java.math.BigDecimal fhfinal;

  /**
   * Table Field OURPAR. Columna importada OURPAR Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "OURPAR", nullable = false, length = 50)
  private java.lang.String ourpar;

  /**
   * Table Field THEIRPAR. Columna importada THEIRPAR Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "THEIRPAR", nullable = false, length = 50)
  private java.lang.String theirpar;

  /**
   * Table Field PCCOMBRK. % COMISION BROKER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PCCOMBRK", nullable = false, precision = 7, scale = 4)
  private java.math.BigDecimal pccombrk;

  /**
   * execution capacity (propios/terceros)
   * */
  @Column(name = "TPDSALDO", nullable = false, length = 1)
  private Character tpdsaldo;

  @Column(name = "RFTITAUD", nullable = false, length = 16)
  private java.lang.String rftitaud;

  /** NUEVAS COLUMNAS LIQUIDACION EXTRANJERO **/

  /**
   * Table Field CDBROCLI. CODIGO CLIENTE Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBROCLI", nullable = true, length = 20)
  private java.lang.String cdbrocli;

  /**
   * Table Field TPSETCLE. TIPO LIQUIDACION CLEARER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "TPSETCLE", nullable = true, length = 25)
  private java.lang.String tpsetcle;

  /**
   * Table Field TPSETCUS. TIPO LIQUIDACION CUSTODIAN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "TPSETCUS", nullable = true, length = 25)
  private java.lang.String tpsetcus;

  /**
   * Table Field ACCTATCUS. ACCOUNT TO CUSTODIAN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ACCTATCUS", nullable = true, length = 34)
  private java.lang.String acctatcus;

  /**
   * Table Field DESCRALI. DESCRIPCION ALIAS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "DESCRALI", nullable = true, length = 130)
  private java.lang.String descrali;

  /**
   * Table Field ROUTING. ROUTING S/N Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "ROUTING", nullable = true, length = 1)
  private Character routing;

  /**
   * Table Field FIDESSA. FIDESSA STATUS I/D/U Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "FIDESSA", nullable = true, length = 1)
  private Character fidessa;

  /**
   * Table Field FHSENDFI. DATE SEND FIDESSA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "FHSENDFI", nullable = true, precision = 8, scale = 0)
  private java.math.BigDecimal fhsendfi;

  @Column(name = "ID_REGLA", nullable = true)
  private Long idRegla;
  @Column(name = "ID_COMPENSADOR", nullable = true)
  private Long idCompensador;
  @Column(name = "CD_REFERENCIA_ASIGNACION", nullable = true, length = 64)
  private String cdReferenciaAsignacion;
  @Column(name = "ID_CUENTA_COMPENSACION", nullable = true)
  private Long idCuentaCompensacion;
  @Column(name = "ID_NEMOTECNICO", nullable = true)
  private Long idNemotecnico;
  @Column(name = "CDORDCARTERA", nullable = true, length = 8)
  private String cdordcartera;
  @Column(name = "CDSUBORDCARTERA", nullable = true, length = 8)
  private String cdsubordcartera;
  @Column(name = "PCCOMDEV", nullable = false, precision = 7, scale = 4)
  private BigDecimal pccomdev;
  @Column(name = "IMCOMBRK", nullable = false, precision = 18, scale = 4)
  private BigDecimal imcombrk;
  @Column(name = "IMCOMDEV", nullable = false, precision = 18, scale = 4)
  private BigDecimal imcomdev;

  public Tmct0ilqId getId() {
    return id;
  }

  public java.lang.String getAcctatcle() {
    return acctatcle;
  }

  public java.lang.String getCdrefban() {
    return cdrefban;
  }

  public java.lang.String getCdcustod() {
    return cdcustod;
  }

  public java.math.BigDecimal getPccombco() {
    return pccombco;
  }

  public java.lang.String getCdcomisn() {
    return cdcomisn;
  }

  public java.lang.String getCdmodord() {
    return cdmodord;
  }

  public Character getDesglose() {
    return desglose;
  }

  public java.lang.String getTcliente() {
    return tcliente;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }

  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }

  public java.lang.String getOurpar() {
    return ourpar;
  }

  public java.lang.String getTheirpar() {
    return theirpar;
  }

  public java.math.BigDecimal getPccombrk() {
    return pccombrk;
  }

  public Character getTpdsaldo() {
    return tpdsaldo;
  }

  public java.lang.String getRftitaud() {
    return rftitaud;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getTpsetcle() {
    return tpsetcle;
  }

  public java.lang.String getTpsetcus() {
    return tpsetcus;
  }

  public java.lang.String getAcctatcus() {
    return acctatcus;
  }

  public java.lang.String getDescrali() {
    return descrali;
  }

  public Character getRouting() {
    return routing;
  }

  public Character getFidessa() {
    return fidessa;
  }

  public java.math.BigDecimal getFhsendfi() {
    return fhsendfi;
  }

  public Long getIdRegla() {
    return idRegla;
  }

  public Long getIdCompensador() {
    return idCompensador;
  }

  public String getCdReferenciaAsignacion() {
    return cdReferenciaAsignacion;
  }

  public Long getIdCuentaCompensacion() {
    return idCuentaCompensacion;
  }

  public Long getIdNemotecnico() {
    return idNemotecnico;
  }

  public String getCdordcartera() {
    return cdordcartera;
  }

  public String getCdsubordcartera() {
    return cdsubordcartera;
  }

  public BigDecimal getPccomdev() {
    return pccomdev;
  }

  public BigDecimal getImcombrk() {
    return imcombrk;
  }

  public BigDecimal getImcomdev() {
    return imcomdev;
  }

  public void setId(Tmct0ilqId id) {
    this.id = id;
  }

  public void setAcctatcle(java.lang.String acctatcle) {
    this.acctatcle = acctatcle;
  }

  public void setCdrefban(java.lang.String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public void setCdcustod(java.lang.String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public void setPccombco(java.math.BigDecimal pccombco) {
    this.pccombco = pccombco;
  }

  public void setCdcomisn(java.lang.String cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  public void setCdmodord(java.lang.String cdmodord) {
    this.cdmodord = cdmodord;
  }

  public void setDesglose(Character desglose) {
    this.desglose = desglose;
  }

  public void setTcliente(java.lang.String tcliente) {
    this.tcliente = tcliente;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setFhinicio(java.math.BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  public void setFhfinal(java.math.BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  public void setOurpar(java.lang.String ourpar) {
    this.ourpar = ourpar;
  }

  public void setTheirpar(java.lang.String theirpar) {
    this.theirpar = theirpar;
  }

  public void setPccombrk(java.math.BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  public void setTpdsaldo(Character tpdsaldo) {
    this.tpdsaldo = tpdsaldo;
  }

  public void setRftitaud(java.lang.String rftitaud) {
    this.rftitaud = rftitaud;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setTpsetcle(java.lang.String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  public void setTpsetcus(java.lang.String tpsetcus) {
    this.tpsetcus = tpsetcus;
  }

  public void setAcctatcus(java.lang.String acctatcus) {
    this.acctatcus = acctatcus;
  }

  public void setDescrali(java.lang.String descrali) {
    this.descrali = descrali;
  }

  public void setRouting(Character routing) {
    this.routing = routing;
  }

  public void setFidessa(Character fidessa) {
    this.fidessa = fidessa;
  }

  public void setFhsendfi(java.math.BigDecimal fhsendfi) {
    this.fhsendfi = fhsendfi;
  }

  public void setIdRegla(Long idRegla) {
    this.idRegla = idRegla;
  }

  public void setIdCompensador(Long idCompensador) {
    this.idCompensador = idCompensador;
  }

  public void setCdReferenciaAsignacion(String cdReferenciaAsignacion) {
    this.cdReferenciaAsignacion = cdReferenciaAsignacion;
  }

  public void setIdCuentaCompensacion(Long idCuentaCompensacion) {
    this.idCuentaCompensacion = idCuentaCompensacion;
  }

  public void setIdNemotecnico(Long idNemotecnico) {
    this.idNemotecnico = idNemotecnico;
  }

  public void setCdordcartera(String cdordcartera) {
    this.cdordcartera = cdordcartera;
  }

  public void setCdsubordcartera(String cdsubordcartera) {
    this.cdsubordcartera = cdsubordcartera;
  }

  public void setPccomdev(BigDecimal pccomdev) {
    this.pccomdev = pccomdev;
  }

  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  @Override
  public String toString() {
    return "Tmct0ilq [id=" + id + ", cdrefban=" + cdrefban + ", desglose=" + desglose + ", idRegla=" + idRegla
        + ", idCompensador=" + idCompensador + ", cdReferenciaAsignacion=" + cdReferenciaAsignacion
        + ", idCuentaCompensacion=" + idCuentaCompensacion + ", idNemotecnico=" + idNemotecnico + "]";
  }

}
