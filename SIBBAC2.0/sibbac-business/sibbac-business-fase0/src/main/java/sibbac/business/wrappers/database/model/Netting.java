package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Netting}.
 */

@Entity
@Table( name = DBConstants.CLEARING.NETTING )
@Audited
public class Netting extends ATable< Netting > implements Serializable {

	@Basic( optional = false )
	@Column( name = "CONTABILIZADO" )
	private Character					contabilizado;
	@Column( name = "LIQUIDADO" )
	private Character					liquidado;
	
	@OneToMany( mappedBy = "idnetting", fetch = FetchType.LAZY )
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private List< S3Liquidacion >	tmct0S3liquidacionList;

	private static final long			serialVersionUID	= -6686818607244338075L;

	@Column( name = "CAMARACOMPENSACION" )
	private String						camaraCompensacion;

	@Column( name = "CDALIAS", nullable = false, length = 20 )
	private String						cdalias;

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHAOPERACION", nullable = false, length = 4 )
	private Date						fechaOperacion;

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHACONTRATACION", nullable = false, length = 4 )
	private Date						fechaContratacion;

	@Column( name = "CDISIN", nullable = false, length = 12 )
	private String						cdisin;

	@Column( name = "NUTIULOCOMPRA", nullable = false, precision = 16, scale = 5 )
	private BigDecimal					nutituloCompra		= BigDecimal.ZERO;

	@Column( name = "EFECTIVOCOMPRA", nullable = false, precision = 18, scale = 4 )
	private BigDecimal					efectivoCompra		= BigDecimal.ZERO;

	@Column( name = "CORRETAJECOMPRA", nullable = false, precision = 18, scale = 4 )
	private BigDecimal					corretajeCompra		= BigDecimal.ZERO;

	@Column( name = "NUTIULOVENTA", nullable = false, precision = 16, scale = 5 )
	private BigDecimal					nutituloVenta		= BigDecimal.ZERO;

	@Column( name = "EFECTIVOVENTA", nullable = false, precision = 18, scale = 4 )
	private BigDecimal					efectivoVenta		= BigDecimal.ZERO;

	@Column( name = "CORRETAJEVENTA", nullable = false, precision = 18, scale = 4 )
	private BigDecimal					corretajeVenta		= BigDecimal.ZERO;

	@Column( name = "NUTIULONETEADO", nullable = false, precision = 16, scale = 5 )
	private BigDecimal					nutituloNeteado		= BigDecimal.ZERO;

	@Column( name = "EFECTIVONETEADO", nullable = false, precision = 18, scale = 4 )
	private BigDecimal					efectivoNeteado		= BigDecimal.ZERO;

	@Column( name = "CORRETAJENETEADO", nullable = false, precision = 18, scale = 4 )
	private BigDecimal					corretajeNeteado	= BigDecimal.ZERO;

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHACONTABILIDAD", length = 4 )
	private Date						fechaContabilidad;

	@Column( name = "CDCUENTACOMPENSACION", length = 35 )
	private String						cdcuentacompensacion;

	@Column( name = "NBVALORS", length = 35 )
	private String						nbvalors;

	@Column( name = "GASTOS", precision = 18, scale = 4 )
	private BigDecimal					gastos;
	
	@Column( name = "IMCOBRADO", precision = 18, scale = 4 )
	private BigDecimal imcobrado;

  

	public Netting() {

	}

	public Netting( String cdaliass, String cdisin, Date fechaOperacion, Date fechaContratacion ) {
		super();
		this.cdalias = cdaliass;
		this.cdisin = cdisin;
		this.fechaOperacion = fechaOperacion;
		this.fechaContratacion = fechaContratacion;
		this.contabilizado = 'N';
	}

	public String getCamaraCompensacion() {
		return this.camaraCompensacion;
	}

	public void setCamaraCompensacion( String camaraCompensacion ) {
		this.camaraCompensacion = camaraCompensacion;
	}

	public String getCdisin() {
		return this.cdisin;
	}

	public void setCdisin( String cdisin ) {
		this.cdisin = cdisin;
	}

	public Date getFechaOperacion() {
		return this.fechaOperacion;
	}

	public void setFechaOperacion( Date fechaOperacion ) {
		this.fechaOperacion = fechaOperacion;
	}

	public Date getFechaContratacion() {
		return this.fechaContratacion;
	}

	public void setFechaContratacion( Date fechaContratacion ) {
		this.fechaContratacion = fechaContratacion;
	}

	public String getCdalias() {
		return this.cdalias;
	}

	public void setCdalias( String cdalias ) {
		this.cdalias = cdalias;
	}

	public BigDecimal getNutituloCompra() {
		return this.nutituloCompra;
	}

	public void setNutituloCompra( BigDecimal nutituloCompra ) {
		this.nutituloCompra = nutituloCompra;
	}

	public BigDecimal getEfectivoCompra() {
		return this.efectivoCompra;
	}

	public void setEfectivoCompra( BigDecimal efectivoCompra ) {
		this.efectivoCompra = efectivoCompra;
	}

	public BigDecimal getCorretajeCompra() {
		return this.corretajeCompra;
	}

	public void setCorretajeCompra( BigDecimal corretajeCompra ) {
		this.corretajeCompra = corretajeCompra;
	}

	public BigDecimal getNutituloVenta() {
		return this.nutituloVenta;
	}

	public void setNutituloVenta( BigDecimal nutituloVenta ) {
		this.nutituloVenta = nutituloVenta;
	}

	public BigDecimal getEfectivoVenta() {
		return this.efectivoVenta;
	}

	public void setEfectivoVenta( BigDecimal efectivoVenta ) {
		this.efectivoVenta = efectivoVenta;
	}

	public BigDecimal getCorretajeVenta() {
		return this.corretajeVenta;
	}

	public void setCorretajeVenta( BigDecimal corretajeVenta ) {
		this.corretajeVenta = corretajeVenta;
	}

	public BigDecimal getNutituloNeteado() {
		return this.nutituloNeteado;
	}

	public void setNutituloNeteado( BigDecimal nutituloNeteado ) {
		this.nutituloNeteado = nutituloNeteado;
	}

	public BigDecimal getEfectivoNeteado() {
		return this.efectivoNeteado;
	}

	public void setEfectivoNeteado( BigDecimal efectivoNeteado ) {
		this.efectivoNeteado = efectivoNeteado;
	}

	public BigDecimal getCorretajeNeteado() {
		return this.corretajeNeteado;
	}

	public void setCorretajeNeteado( BigDecimal corretajeNeteado ) {
		this.corretajeNeteado = corretajeNeteado;
	}

	public Date getFechaContabilidad() {
		return this.fechaContabilidad;
	}

	public void setFechaContabilidad( Date fechaContabilidad ) {
		this.fechaContabilidad = fechaContabilidad;
	}

	public String getCdcuentacompensacion() {
		return cdcuentacompensacion;
	}

	public void setCdcuentacompensacion( String cdcuentacompensacion ) {
		this.cdcuentacompensacion = cdcuentacompensacion;
	}

	public String getNbvalors() {
		return nbvalors;
	}

	public void setNbvalors( String nbvalors ) {
		this.nbvalors = nbvalors;
	}

	public BigDecimal getGastos() {
		return gastos;
	}

	public void setGastos( BigDecimal gastos ) {
		this.gastos = gastos;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != null ? id.hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if ( !( object instanceof Netting ) ) {
			return false;
		}
		Netting other = ( Netting ) object;
		if ( ( this.id == null && other.id != null ) || ( this.id != null && !this.id.equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	public Character getContabilizado() {
		return contabilizado;
	}

	public void setContabilizado( Character contabilizado ) {
		this.contabilizado = contabilizado;
	}

	public Character getLiquidado() {
		return liquidado;
	}

	public void setLiquidado( Character liquidado ) {
		this.liquidado = liquidado;
	}

	@XmlTransient
	public List< S3Liquidacion > getTmct0S3liquidacionList() {
		return tmct0S3liquidacionList;
	}

	public void setTmct0S3liquidacionList( List< S3Liquidacion > tmct0S3liquidacionList ) {
		this.tmct0S3liquidacionList = tmct0S3liquidacionList;
	}
	
	/**
   * @return the imcobrado
   */
  public BigDecimal getImcobrado() {
    return imcobrado;
  }

  /**
   * @param imcobrado the imcobrado to set
   */
  public void setImcobrado(BigDecimal imcobrado) {
    this.imcobrado = imcobrado;
  }

	@Override
	public String toString() {
		return "sibbac.business.wrappers.database.model.Netting[ id=" + id + " ]";
	}

}
