package sibbac.business.fase0.database.dto;


import org.apache.commons.lang3.StringUtils;

import sibbac.business.fase0.database.model.Tmct0cleInternacional;
import sibbac.business.fase0.database.model.Tmct0cleNacional;


public class Tmct0cleDTO {

	private String	nombre;
	private String	biccle;

	public Tmct0cleDTO( final Tmct0cleInternacional tmct0cle ) {
		this.nombre = tmct0cle.formarBiccleDesc();
		this.biccle = tmct0cle.getBiccle();
	}
	
	public Tmct0cleDTO( final Tmct0cleNacional tmct0cle ) {
    this.nombre = tmct0cle.formarBiccleDesc();
    this.biccle = tmct0cle.getBiccle();
  }

	public Tmct0cleDTO( final String biccle, final String nombre ) {
		super();
		this.nombre = Tmct0cleInternacional.formarBiccleDesc( biccle, nombre );
		this.biccle = StringUtils.trimToNull( biccle );
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String alias ) {
		this.nombre = alias;
	}

	public String getBiccle() {
		return biccle;
	}

	public void setBiccle( String descripcion ) {
		this.biccle = descripcion;
	}

}
