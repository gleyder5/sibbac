package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0DESGLOSECLITIT")
public class Tmct0desgloseclitit implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 8721882628649563988L;
  private Long nudesglose;
  private Tmct0desglosecamara tmct0desglosecamara;
  private long idgrupoeje;
  private String nuejecuc;
  private String cdoperacion;
  private String nurefexemkt;
  private String cdcamara;
  private String cdsegmento;
  private char cdsentido;
  private BigDecimal imtitulos;
  private BigDecimal imprecio;
  private BigDecimal imefectivo;
  private Character cdindcotizacion;
  private String cddivisa;
  private String cdisin;
  private Date hoordenmkt;
  private Date feordenmkt;
  private String nuordenmkt;
  private Date auditFechaCambio;
  private String auditUser;
  private String cdPlataformaNegociacion;
  private BigDecimal imtotbru;
  private BigDecimal imcanoncompdv;
  private BigDecimal imcanoncompeu;
  private BigDecimal imcanoncontrdv;
  private BigDecimal imcanoncontreu;
  private BigDecimal imcanonliqdv;
  private BigDecimal imcanonliqeu;
  private String cdmiembromkt;
  private String tpopebol;
  private String numeroOperacionDCV = "";
  private String miembroCompensadorDestino;
  private String cuentaCompensacionDestino;
  private String codigoReferenciaAsignacion;

  protected java.lang.String cdoperacionecc = "";
  protected java.lang.String nuoperacionprev = "";

  protected java.lang.String nuoperacioninic = "";

  protected BigDecimal imcomisn;

  /**
   * Numero de titulos fallidos si el sentido de la operacion es 'venta' y
   * numero de titulos afectados si el sentido de la operacion es 'compra'.
   */
  private BigDecimal nutitfallidos;

  /** Fecha de ejecucion. */
  private Date feejecuc;

  /** Hora de ejecucion. */
  private Date hoejecuc;

  private BigDecimal imfinsvb;

  private BigDecimal imcomsvb = BigDecimal.ZERO;
  private BigDecimal imcombco = BigDecimal.ZERO;
  private BigDecimal imcomdvo = BigDecimal.ZERO;
  private BigDecimal imntbrok = BigDecimal.ZERO;
  private BigDecimal imnetliq = BigDecimal.ZERO;
  private BigDecimal imnfiliq = BigDecimal.ZERO;
  private BigDecimal imcombrk = BigDecimal.ZERO;
  private BigDecimal imajusvb = BigDecimal.ZERO;
  private BigDecimal imtotnet = BigDecimal.ZERO;
  private BigDecimal imcomsis = BigDecimal.ZERO;
  private BigDecimal imbansis = BigDecimal.ZERO;

  private BigDecimal imcobrado = BigDecimal.ZERO;
  // private BigDecimal imconciliado = BigDecimal.ZERO;
  private BigDecimal imefeMercadoCobrado = BigDecimal.ZERO;
  private BigDecimal imefeClienteCobrado = BigDecimal.ZERO;
  private BigDecimal imcanonCobrado = BigDecimal.ZERO;
  private BigDecimal imcomCobrado = BigDecimal.ZERO;
  private BigDecimal imbrkPagado = BigDecimal.ZERO;
  private BigDecimal imdvoPagado = BigDecimal.ZERO;

  protected Character isEjecucionPartida = 'N';

  private BigDecimal imCanonBasico = BigDecimal.ZERO;
  private BigDecimal imCanonEspecial = BigDecimal.ZERO;

  private boolean isMtf;

  private Tmct0anotacionecc anotacion;

  private Tmct0ejeFeeSchema tmct0ejeFeeSchema;
  
  private String euroccpClientNumber;

  public Tmct0desgloseclitit() {
  }

  public Tmct0desgloseclitit(long nudesglose, Tmct0desglosecamara tmct0desglosecamara, long idgrupoeje,
      String nuejecuc, String nurefexemkt, String cdcamara, char cdsentido, BigDecimal imtitulos, BigDecimal imprecio,
      BigDecimal imefectivo, String cddivisa, String cdisin, String nuordenmkt) {
    this.nudesglose = nudesglose;
    this.tmct0desglosecamara = tmct0desglosecamara;
    this.idgrupoeje = idgrupoeje;
    this.nuejecuc = nuejecuc;
    this.nurefexemkt = nurefexemkt;
    this.cdcamara = cdcamara;
    this.cdsentido = cdsentido;
    this.imtitulos = imtitulos;
    this.imprecio = imprecio;
    this.imefectivo = imefectivo;
    this.cddivisa = cddivisa;
    this.cdisin = cdisin;
    this.nuordenmkt = nuordenmkt;
  }

  public Tmct0desgloseclitit(long nudesglose, Tmct0desglosecamara tmct0desglosecamara, long idgrupoeje,
      String nuejecuc, String cdoperacion, String nurefexemkt, String cdcamara, String cdsegmento, char cdsentido,
      BigDecimal imtitulos, BigDecimal imprecio, BigDecimal imefectivo, Character cdindcotizacion, String cddivisa,
      String cdisin, Date hoordenmkt, Date feordenmkt, String nuordenmkt, Date auditFechaCambio, String auditUser,
      String cdPlataformaNegociacion, BigDecimal imtotbru, BigDecimal imcanoncompdv, BigDecimal imcanoncompeu,
      BigDecimal imcanoncontrdv, BigDecimal imcanoncontreu, BigDecimal imcanonliqdv, BigDecimal imcanonliqeu) {
    this.nudesglose = nudesglose;
    this.tmct0desglosecamara = tmct0desglosecamara;
    this.idgrupoeje = idgrupoeje;
    this.nuejecuc = nuejecuc;
    this.cdoperacion = cdoperacion;
    this.nurefexemkt = nurefexemkt;
    this.cdcamara = cdcamara;
    this.cdsegmento = cdsegmento;
    this.cdsentido = cdsentido;
    this.imtitulos = imtitulos;
    this.imprecio = imprecio;
    this.imefectivo = imefectivo;
    this.cdindcotizacion = cdindcotizacion;
    this.cddivisa = cddivisa;
    this.cdisin = cdisin;
    this.hoordenmkt = hoordenmkt;
    this.feordenmkt = feordenmkt;
    this.nuordenmkt = nuordenmkt;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.imtotbru = imtotbru;
    this.imcanoncompdv = imcanoncompdv;
    this.imcanoncompeu = imcanoncompeu;
    this.imcanoncontrdv = imcanoncontrdv;
    this.imcanoncontreu = imcanoncontreu;
    this.imcanonliqdv = imcanonliqdv;
    this.imcanonliqeu = imcanonliqeu;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "NUDESGLOSE", unique = true, nullable = false)
  public Long getNudesglose() {
    return this.nudesglose;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDDESGLOSECAMARA", nullable = false)
  public Tmct0desglosecamara getTmct0desglosecamara() {
    return this.tmct0desglosecamara;
  }

  public void setTmct0desglosecamara(Tmct0desglosecamara tmct0desglosecamara) {
    this.tmct0desglosecamara = tmct0desglosecamara;
  }

  @Column(name = "IDGRUPOEJE", nullable = false)
  public long getIdgrupoeje() {
    return this.idgrupoeje;
  }

  public void setIdgrupoeje(long idgrupoeje) {
    this.idgrupoeje = idgrupoeje;
  }

  @Column(name = "NUEJECUC", nullable = false, length = 20)
  public String getNuejecuc() {
    return this.nuejecuc;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  @Column(name = "CDOPERACION", length = 16)
  public String getCdoperacion() {
    return this.cdoperacion;
  }

  public void setCdoperacion(String cdoperacion) {
    this.cdoperacion = cdoperacion;
  }

  @Column(name = "CDCAMARA", nullable = false, length = 11)
  public String getCdcamara() {
    return this.cdcamara;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  @Column(name = "CDSEGMENTO", length = 20)
  public String getCdsegmento() {
    return this.cdsegmento;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  @Column(name = "CDSENTIDO", nullable = false, length = 1)
  public char getCdsentido() {
    return this.cdsentido;
  }

  public void setCdsentido(char cdsentido) {
    this.cdsentido = cdsentido;
  }

  @Column(name = "IMTITULOS", nullable = false, precision = 18, scale = 6)
  public BigDecimal getImtitulos() {
    return this.imtitulos;
  }

  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  @Column(name = "IMPRECIO", nullable = false, precision = 13, scale = 6)
  public BigDecimal getImprecio() {
    return this.imprecio;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  @Column(name = "IMEFECTIVO", nullable = false, precision = 15, scale = 2)
  public BigDecimal getImefectivo() {
    return this.imefectivo;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  @Column(name = "CDINDCOTIZACION", length = 1)
  public Character getCdindcotizacion() {
    return this.cdindcotizacion;
  }

  public void setCdindcotizacion(Character cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  @Column(name = "CDDIVISA", nullable = false, length = 3)
  public String getCddivisa() {
    return this.cddivisa;
  }

  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  @Column(name = "CDISIN", nullable = false, length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOORDENMKT", length = 8)
  public Date getHoordenmkt() {
    return this.hoordenmkt;
  }

  public void setHoordenmkt(Date hoordenmkt) {
    this.hoordenmkt = hoordenmkt;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEORDENMKT", length = 10)
  public Date getFeordenmkt() {
    return this.feordenmkt;
  }

  public void setFeordenmkt(Date feordenmkt) {
    this.feordenmkt = feordenmkt;
  }

  @Column(name = "NUORDENMKT", nullable = false, length = 32)
  public String getNuordenmkt() {
    return this.nuordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  @Column(name = "NUREFEXEMKT", nullable = false, length = 32)
  public String getNurefexemkt() {
    return this.nurefexemkt;
  }

  public void setNurefexemkt(String nurefexemkt) {
    this.nurefexemkt = nurefexemkt;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Column(name = "CD_PLATAFORMA_NEGOCIACION", length = 4)
  public String getCdPlataformaNegociacion() {
    return this.cdPlataformaNegociacion;
  }

  @Column(name = "IMTOTBRU", precision = 18, scale = 8)
  public BigDecimal getImtotbru() {
    return this.imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  @Column(name = "IMCANONCOMPDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncompdv() {
    return this.imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  @Column(name = "IMCANONCOMPEU", precision = 18, scale = 8)
  public BigDecimal getImcanoncompeu() {
    return this.imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  @Column(name = "IMCANONCONTRDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncontrdv() {
    return this.imcanoncontrdv;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  @Column(name = "IMCANONCONTREU", precision = 18, scale = 8)
  public BigDecimal getImcanoncontreu() {
    return this.imcanoncontreu;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  @Column(name = "IMCANONLIQDV", precision = 18, scale = 8)
  public BigDecimal getImcanonliqdv() {
    return this.imcanonliqdv;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  @Column(name = "IMCANONLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcanonliqeu() {
    return this.imcanonliqeu;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  @Column(name = "CDMIEMBROMKT", length = 11, nullable = true)
  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  @Column(name = "TPOPEBOL", length = 2)
  public String getTpopebol() {
    return tpopebol;
  }

  public void setTpopebol(String tpopebol) {
    this.tpopebol = tpopebol;
  }

  @Column(name = "NUTITFALLIDOS", precision = 18, scale = 6)
  public BigDecimal getNutitfallidos() {
    return this.nutitfallidos;
  }

  public void setNutitfallidos(BigDecimal nutitfallidos) {
    this.nutitfallidos = nutitfallidos;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC", length = 10)
  public Date getFeejecuc() {
    return this.feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOEJECUC", length = 8)
  public Date getHoejecuc() {
    return this.hoejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  /**
   * @return the numeroOperacionDCV
   */
  @Column(name = "NUMERO_OPERACION_DCV", length = 35)
  public String getNumeroOperacionDCV() {
    return numeroOperacionDCV;
  }

  /**
   * @param numeroOperacionDCV the numeroOperacionDCV to set
   */
  public void setNumeroOperacionDCV(String numeroOperacionDCV) {
    this.numeroOperacionDCV = numeroOperacionDCV;
  }

  @Column(name = "CDOPERACIONECC", length = 16, nullable = true)
  public java.lang.String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public void setCdoperacionecc(java.lang.String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  @Column(name = "NUOPERACIONPREV", length = 16, nullable = true)
  public java.lang.String getNuoperacionprev() {
    return nuoperacionprev;
  }

  public void setNuoperacionprev(java.lang.String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  @Column(name = "NUOPERACIONINIC", length = 16, nullable = true)
  public java.lang.String getNuoperacioninic() {
    return nuoperacioninic;
  }

  public void setNuoperacioninic(java.lang.String nuoperacioninic) {
    this.nuoperacioninic = nuoperacioninic;
  }

  @Column(name = "IMCOMISN", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImcomisn() {
    return this.imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  @Column(name = "IMFINSVB", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImfinsvb() {
    return imfinsvb;
  }

  public void setImfinsvb(BigDecimal imfinsvb) {
    this.imfinsvb = imfinsvb;
  }

  @Column(name = "IMNETLIQ", precision = 18, scale = 4)
  public BigDecimal getImnetliq() {
    return this.imnetliq;
  }

  public void setImnetliq(BigDecimal imnetliq) {
    this.imnetliq = imnetliq;
  }

  @Column(name = "IMNFILIQ", precision = 18, scale = 4)
  public BigDecimal getImnfiliq() {
    return this.imnfiliq;
  }

  public void setImnfiliq(BigDecimal imnfiliq) {
    this.imnfiliq = imnfiliq;
  }

  @Column(name = "IMCOMBRK", precision = 18, scale = 4)
  public BigDecimal getImcombrk() {
    return this.imcombrk;
  }

  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  @Column(name = "IMAJUSVB", precision = 18, scale = 4)
  public BigDecimal getImajusvb() {
    return this.imajusvb;
  }

  public void setImajusvb(BigDecimal imajusvb) {
    this.imajusvb = imajusvb;
  }

  @Column(name = "IMTOTNET", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImtotnet() {
    return this.imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public BigDecimal getImcomsis() {
    return imcomsis;
  }

  public BigDecimal getImbansis() {
    return imbansis;
  }

  @Column(name = "IMCOMSIS", precision = 18, scale = 4, nullable = true)
  public void setImcomsis(BigDecimal imcomsis) {
    this.imcomsis = imcomsis;
  }

  @Column(name = "IMBANSIS", precision = 18, scale = 4, nullable = true)
  public void setImbansis(BigDecimal imbansis) {
    this.imbansis = imbansis;
  }

  @Column(name = "IMNTBROK", precision = 18, scale = 4)
  public BigDecimal getImntbrok() {
    return this.imntbrok;
  }

  public void setImntbrok(BigDecimal imntbrok) {
    this.imntbrok = imntbrok;
  }

  @Column(name = "IMCOMDVO", precision = 18, scale = 4)
  public BigDecimal getImcomdvo() {
    return this.imcomdvo;
  }

  public void setImcomdvo(BigDecimal imcomdvo) {
    this.imcomdvo = imcomdvo;
  }

  @Column(name = "IMCOMBCO", precision = 18, scale = 4)
  public BigDecimal getImcombco() {
    return this.imcombco;
  }

  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  @Column(name = "IMCOMSVB", precision = 18, scale = 4)
  public BigDecimal getImcomsvb() {
    return this.imcomsvb;
  }

  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  @Column(name = "IS_EJECUCION_PARTIDA", length = 1, nullable = true)
  public Character getIsEjecucionPartida() {
    return isEjecucionPartida;
  }

  public void setIsEjecucionPartida(Character isEjecucionPartida) {
    this.isEjecucionPartida = isEjecucionPartida;
  }

  @Column(name = "MIEMBRO_COMPENSADOR_DESTINO", length = 4, nullable = true)
  public String getMiembroCompensadorDestino() {
    return miembroCompensadorDestino;
  }

  public void setMiembroCompensadorDestino(String miembroCompensadorDestino) {
    this.miembroCompensadorDestino = miembroCompensadorDestino;
  }

  @Column(name = "CD_REF_ASIGNACION", length = 20, nullable = true)
  public String getCodigoReferenciaAsignacion() {
    return codigoReferenciaAsignacion;
  }

  public void setCodigoReferenciaAsignacion(String codigoReferenciaAsignacion) {
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
  }

  @Column(name = "CUENTA_COMPENSACION_DESTINO", length = 3, nullable = true)
  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

  /**
   * @return the imcobrado
   */
  @Column(name = "IMCOBRADO", precision = 18, scale = 4)
  public BigDecimal getImcobrado() {
    return imcobrado;
  }

  // @Column(name = "IMCONCILIADO", precision = 18, scale = 4)
  // public BigDecimal getImconciliado() {
  // return imconciliado;
  // }

  @Column(name = "IMEFE_MERCADO_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImefeMercadoCobrado() {
    return imefeMercadoCobrado;
  }

  @Column(name = "IMEFE_CLIENTE_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImefeClienteCobrado() {
    return imefeClienteCobrado;
  }

  @Column(name = "IMCANON_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImcanonCobrado() {
    return imcanonCobrado;
  }

  @Column(name = "IMCOM_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImcomCobrado() {
    return imcomCobrado;
  }

  @Column(name = "IMBRK_PAGADO", precision = 18, scale = 4)
  public BigDecimal getImbrkPagado() {
    return imbrkPagado;
  }

  @Column(name = "IMDVO_PAGADO", precision = 18, scale = 4)
  public BigDecimal getImdvoPagado() {
    return imdvoPagado;
  }

  /**
   * @param imcobrado the imcobrado to set
   */
  public void setImcobrado(BigDecimal imcobrado) {
    this.imcobrado = imcobrado;
  }

  // public void setImconciliado(BigDecimal imconciliado) {
  // this.imconciliado = imconciliado;
  // }

  public void setImefeMercadoCobrado(BigDecimal imefeMercadoCobrado) {
    this.imefeMercadoCobrado = imefeMercadoCobrado;
  }

  public void setImefeClienteCobrado(BigDecimal imefeClienteCobrado) {
    this.imefeClienteCobrado = imefeClienteCobrado;
  }

  public void setImcanonCobrado(BigDecimal imcanonCobrado) {
    this.imcanonCobrado = imcanonCobrado;
  }

  public void setImcomCobrado(BigDecimal imcomCobrado) {
    this.imcomCobrado = imcomCobrado;
  }

  public void setImbrkPagado(BigDecimal imbrkPagado) {
    this.imbrkPagado = imbrkPagado;
  }

  public void setImdvoPagado(BigDecimal imdvoPagado) {
    this.imdvoPagado = imdvoPagado;
  }

  @Transient
  public boolean isMtf() {
    return isMtf;
  }

  public void setMtf(boolean isMtf) {
    this.isMtf = isMtf;
  }

  @Transient
  public Tmct0anotacionecc getAnotacion() {
    return anotacion;
  }

  public void setAnotacion(Tmct0anotacionecc anotacion) {
    this.anotacion = anotacion;
  }

  @Column(name = "IMCANON_ESPECIAL", precision = 18, scale = 4)
  public BigDecimal getImCanonEspecial() {
    return imCanonEspecial;
  }

  public void setImCanonEspecial(BigDecimal imCanonEspecial) {
    this.imCanonEspecial = imCanonEspecial;
  }

  @Column(name = "IMCANON_BASICO", precision = 18, scale = 4)
  public BigDecimal getImCanonBasico() {
    return imCanonBasico;
  }

  public void setImCanonBasico(BigDecimal imCanonBasico) {
    this.imCanonBasico = imCanonBasico;
  }

  @Transient
  public Tmct0ejeFeeSchema getTmct0ejeFeeSchema() {
    return tmct0ejeFeeSchema;
  }

  public void setTmct0ejeFeeSchema(Tmct0ejeFeeSchema tmct0ejeFeeSchema) {
    this.tmct0ejeFeeSchema = tmct0ejeFeeSchema;
  }

  @Transient
  public String getEuroccpClientNumber() {
    return euroccpClientNumber;
  }

  public void setEuroccpClientNumber(String euroccpClientNumber) {
    this.euroccpClientNumber = euroccpClientNumber;
  }

  @Override
  public String toString() {
    return "Tmct0desgloseclitit [nudesglose=" + nudesglose + ", idgrupoeje=" + idgrupoeje + ", nuejecuc=" + nuejecuc
        + ", cdoperacion=" + cdoperacion + ", nurefexemkt=" + nurefexemkt + ", cdcamara=" + cdcamara + ", cdsegmento="
        + cdsegmento + ", cdsentido=" + cdsentido + ", imtitulos=" + imtitulos + ", imprecio=" + imprecio
        + ", cddivisa=" + cddivisa + ", cdisin=" + cdisin + ", cdPlataformaNegociacion=" + cdPlataformaNegociacion
        + ", tpopebol=" + tpopebol + ", numeroOperacionDCV=" + numeroOperacionDCV + ", cdoperacionecc="
        + cdoperacionecc + ", nuoperacionprev=" + nuoperacionprev + ", nuoperacioninic=" + nuoperacioninic
        + ", isEjecucionPartida=" + isEjecucionPartida + "]";
  }

}
