package sibbac.business.wrappers.database.data;

import java.io.Serializable;

import sibbac.business.wrappers.database.model.Tmct0alo;

public class AloDesgloseData implements Serializable {

  /**
   * Número de serie automático
   */
  private static final long serialVersionUID = -7754516767630469836L;

  private final String alias;
 
  private final String estadoAsign;
  
  private final String estadoCont;
  
  public AloDesgloseData(Tmct0alo alo) {
    alias = alo.getCdalias();
    estadoAsign = alo.getTmct0estadoByCdestadoasig().getNombre();
    estadoCont = alo.getTmct0estadoByCdestadocont().getNombre();
  }
  
  public String getAlias() {
    return alias;
  }
  
  public String getEstadoAsign() {
    return estadoAsign;
  }
  
  public String getEstadoCont() {
    return estadoCont;
  }

}
