package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Identificadores }.
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@SuppressWarnings( "serial" )
@Table( name = WRAPPERS.IDENTIFICADOR )
@Entity
@XmlRootElement
@Audited
public class Identificadores extends ATableAudited< Identificadores > implements java.io.Serializable {

	/** The codigo. */

	@Column( name = "ID_IDENTIFICADORES", nullable = false )
	@XmlAttribute
	private Long	indentificadores;

	/**
	 * Instantiates a new identificadores.
	 */
	public Identificadores() {

	}

	public Long getIndentificadores() {
		return indentificadores;
	}

	public void setIndentificadores( Long indentificadores ) {
		this.indentificadores = indentificadores;
	}

}
