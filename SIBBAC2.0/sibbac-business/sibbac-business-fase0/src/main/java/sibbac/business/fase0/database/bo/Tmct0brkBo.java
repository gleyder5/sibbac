package sibbac.business.fase0.database.bo;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0brkDao;
import sibbac.business.fase0.database.model.Tmct0brk;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0brkBo extends AbstractBo<Tmct0brk, Long, Tmct0brkDao> {

  public Tmct0brk findByCdbroker(final String cdbroker) {
    return this.dao.findByCdbroker(cdbroker);
  }

  public Tmct0brk findByCdbrokerAndFechaContratacion(final String cdbroker, final Integer fecha) {
    return dao.findByCdbrokerAndFechaContratacion(cdbroker, fecha);
  }

  public boolean isCdbrokerMTF(final String cdbroker) {
    boolean isMTF = false;
    Tmct0brk obj = findByCdbroker(cdbroker);
    if (obj != null) {
      isMTF = StringUtils.isNotBlank(obj.getCocenliq());
    }

    return isMTF;
  }

  public boolean isCdbrokerMtf(final Map<String, Boolean> cachedValues, String cdbroker) {
    cdbroker = StringUtils.trim(cdbroker);
    Boolean value = cachedValues.get(cdbroker);
    if (value == null) {
      synchronized (cachedValues) {
        value = cachedValues.get(cdbroker);
        if (value == null) {
          value = isCdbrokerMTF(cdbroker);
          if (value == null) {
            value = false;
          }
          cachedValues.put(cdbroker, value);
        }
      }

    }
    return value;

  }

  public String findCocenliqByCdbroker(final String cdbroker) {
    return dao.findCocenliqByCdbroker(cdbroker);
  }

}
