package sibbac.business.fase0.database.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "TPTI_IN")
public class TptiIn implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -2828072703074208307L;

  /**
   * Constructor I. Constructor por defecto.
   */
  public TptiIn() {
  } // TptiIn

  /** Identificador del mensaje. */
  @Id
  @Column(name = "ID", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @XmlAttribute
  private Long id;

  @Column(name = "MEMBER", length = 11)
  @XmlAttribute
  private String member;

  @XmlAttribute
  @Column(name = "TRADEDATE")
  private Date tradedate;

  @XmlAttribute
  @Column(name = "MESSAGE_DATE", nullable = false)
  private Date messageDate;

  @XmlAttribute
  @Temporal(TemporalType.TIME)
  @Column(name = "MESSAGE_HOUR", nullable = false)
  private Date messageHour;

  @Column(name = "MESSAGETYPE", length = 4, nullable = false)
  @XmlAttribute
  private String messageType;

  @Column(name = "MESSAGELENGTH", length = 4, nullable = false)
  @XmlAttribute
  private Integer messageLength;

  @Column(name = "SCRIPTDIVIDEND", length = 1, nullable = false)
  @XmlAttribute
  private Character scriptDividend;

  @Column(name = "IDBATCH", length = 255)
  @XmlAttribute
  private String idBatch;

  @Column(name = "BATCH_ORDER", length = 4)
  @XmlAttribute
  private Integer batchOrder;

  @Column(name = "IS_LAST", length = 1)
  @XmlAttribute
  private Character isLast;

  @Column(name = "IDMESSAGE", length = 255)
  @XmlAttribute
  private String idMessage;

  @Column(name = "GROUP_DATA", length = 255)
  @XmlAttribute
  private String groupData;

  @Column(name = "PROCESSED", length = 1, nullable = false)
  @XmlAttribute
  private Character processed;

  @XmlAttribute
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO")
  private Date audtFechaCambio;

  @Column(name = "AUDIT_USER", length = 20)
  @XmlAttribute
  private String audtUser;

  @Column(name = "MESSAGE", length = 32768, nullable = false)
  @XmlAttribute
  private String message;

  @Column(name = "WITHOUT_ERROR", length = 1)
  @XmlAttribute
  private Character withoutError;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tptiIn", cascade = { CascadeType.PERSIST })
  private List<TptiInError> errors = new ArrayList<TptiInError>(0);

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
    tsb.append("ID", id);
    tsb.append("MEMBER", member);
    tsb.append("TRADEDATE", tradedate);
    tsb.append("MESSAGE_DATE", messageDate);
    tsb.append("MESSAGE_HOUR", messageHour);
    tsb.append("MESSAGETYPE", messageType);
    tsb.append("MESSAGELENGTH", messageLength);
    tsb.append("SCRIPTDIVIDEND", scriptDividend);
    tsb.append("IDBATCH", idBatch);
    tsb.append("BATCH_ORDER", batchOrder);
    tsb.append("IS_LAST", isLast);
    tsb.append("IDMESSAGE", idMessage);
    tsb.append("GROUP_DATA", groupData);
    tsb.append("PROCESSED", processed);
    tsb.append("AUDIT_FECHA_CAMBIO", audtFechaCambio);
    tsb.append("AUDIT_USER", audtUser);
    tsb.append("MESSAGE", message);
    tsb.append("WITHOUT_ERROR", withoutError);

    return tsb.toString();
  }// public String toString() {

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMember() {
    return member;
  }

  public void setMember(String member) {
    this.member = member;
  }

  public Date getTradedate() {
    return tradedate;
  }

  public void setTradedate(Date tradedate) {
    this.tradedate = tradedate;
  }

  public Date getMessageDate() {
    return messageDate;
  }

  public void setMessageDate(Date messageDate) {
    this.messageDate = messageDate;
  }

  public Date getMessageHour() {
    return messageHour;
  }

  public void setMessageHour(Date messageHour) {
    this.messageHour = messageHour;
  }

  public String getMessageType() {
    return messageType;
  }

  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }

  public Integer getMessageLength() {
    return messageLength;
  }

  public void setMessageLength(Integer messageLength) {
    this.messageLength = messageLength;
  }

  public Character getScriptDividend() {
    return scriptDividend;
  }

  public void setScriptDividend(Character scriptDividend) {
    this.scriptDividend = scriptDividend;
  }

  public String getIdBatch() {
    return idBatch;
  }

  public void setIdBatch(String idBatch) {
    this.idBatch = idBatch;
  }

  public Integer getBatchOrder() {
    return batchOrder;
  }

  public void setBatchOrder(Integer batchOrder) {
    this.batchOrder = batchOrder;
  }

  public Character getIsLast() {
    return isLast;
  }

  public void setIsLast(Character isLast) {
    this.isLast = isLast;
  }

  public String getIdMessage() {
    return idMessage;
  }

  public void setIdMessage(String idMessage) {
    this.idMessage = idMessage;
  }

  public String getGroupData() {
    return groupData;
  }

  public void setGroupData(String groupData) {
    this.groupData = groupData;
  }

  public Character getProcessed() {
    return processed;
  }

  public void setProcessed(Character processed) {
    this.processed = processed;
  }

  public Date getAudtFechaCambio() {
    return audtFechaCambio;
  }

  public void setAudtFechaCambio(Date audtFechaCambio) {
    this.audtFechaCambio = audtFechaCambio;
  }

  public String getAudtUser() {
    return audtUser;
  }

  public void setAudtUser(String audtUser) {
    this.audtUser = audtUser;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Character getWithoutError() {
    return withoutError;
  }

  public void setWithoutError(Character withoutError) {
    this.withoutError = withoutError;
  }

  public List<TptiInError> getErrors() {
    return errors;
  }

  public void setErrors(List<TptiInError> errors) {
    this.errors = errors;
  }
}// public class TptiIn implements java.io.Serializable {
