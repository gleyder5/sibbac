package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


@Entity
@Table( name = DBConstants.CONCILIACION.TIPO_MOVIMIENTO_CONCILIACION )
@Audited
public class Tmct0TipoMovimientoConciliacion extends ATableAudited< Tmct0TipoMovimientoConciliacion > implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	@Column( name = "TIPO_OPERACION", length = 3, nullable = false )
	private String				tipoOperacion;
	@Column( name = "DESCRIPCION", length = 255 )
	private String				descripcion;
	@Column( name = "AFECTA_TITULO_CONTADO", length = 1 )
	private Character			afectaTituloContado;
	@Column( name = "AFECTA_SALDO_EFECTIVO", length = 1 )
	private Character			afectaSaldoEfectivo;

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion( String tipoOperacion ) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public Character getAfectaTituloContado() {
		return afectaTituloContado;
	}

	public void setAfectaTituloContado( Character afectaTituloContado ) {
		this.afectaTituloContado = afectaTituloContado;
	}

	public Character getAfectaSaldoEfectivo() {
		return afectaSaldoEfectivo;
	}

	public void setAfectaSaldoEfectivo( Character afectaSaldoEfectivo ) {
		this.afectaSaldoEfectivo = afectaSaldoEfectivo;
	}

}
