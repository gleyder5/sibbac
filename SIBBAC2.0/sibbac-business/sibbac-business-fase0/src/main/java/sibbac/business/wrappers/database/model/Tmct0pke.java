package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0PKE. Confirmation sent to broker
 * Allocations to clearer Documentación para TMCT0PKE <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0PKE")
public class Tmct0pke implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 6941388731786291314L;
  @AttributeOverrides({
      @AttributeOverride(name = "pkbroker", column = @Column(name = "PKBROKER", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER", nullable = false, length = 20)),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
      @AttributeOverride(name = "nualose1", column = @Column(name = "NUALOSE1", nullable = false, length = 5, scale = 0)) })
  @EmbeddedId
  private Tmct0pkeId id;

  /**
   * Relation 1..1 with table TMCT0PKD. Constraint de relación entre TMCT0PKE y
   * TMCT0PKD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "PKBROKER", referencedColumnName = "PKBROKER", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDBROKER", referencedColumnName = "CDBROKER", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDMERCAD", referencedColumnName = "CDMERCAD", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false) })
  protected Tmct0pkd tmct0pkd;

  /**
   * Table Field DSCRIPT. Description Documentación para DSCRIPT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "DSCRIPT", length = 40)
  protected java.lang.String dscript;
  /**
   * Table Field LINMSD. Numero de Linea Cliente Documentación para LINMSD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "LINMSD", length = 5, scale = 0)
  protected java.math.BigDecimal linmsd;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;

  public Tmct0pkeId getId() {
    return id;
  }

  public Tmct0pkd getTmct0pkd() {
    return tmct0pkd;
  }

  public java.lang.String getDscript() {
    return dscript;
  }

  public java.math.BigDecimal getLinmsd() {
    return linmsd;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public void setId(Tmct0pkeId id) {
    this.id = id;
  }

  public void setTmct0pkd(Tmct0pkd tmct0pkd) {
    this.tmct0pkd = tmct0pkd;
  }

  public void setDscript(java.lang.String dscript) {
    this.dscript = dscript;
  }

  public void setLinmsd(java.math.BigDecimal linmsd) {
    this.linmsd = linmsd;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Override
  public String toString() {
    return String.format("Tmct0pke [id=%s]", this.id);
  }

}
