package sibbac.business.canones.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import static javax.persistence.TemporalType.TIMESTAMP;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad abstracta con campos comunes a las entidades del cálculo de cánon
 * @author XI326526
 */
@MappedSuperclass
public abstract class CanonEntity implements Serializable {

  /**
   * Default Serial version
   */
  private static final long serialVersionUID = 1L;

  @Version
  @Column(name = "VERSION")
  private int version;

  @Column(name = "FHAUDIT")
  @Temporal(TIMESTAMP)
  private Date fhaudit;

  @Column(name = "CDUSUAUD")
  private String cdusuaud;

  /**
   * @return version de la entidad
   */
  public int getVersion() {
    return version;
  }

  /**
   * @param version asigna un valor consecutivo
   */
  public void setVersion(int version) {
    this.version = version;
  }

  /**
   * @return fecha de ultima actualzación
   */
  @JsonIgnore
  public Date getFhaudit() {
    return fhaudit;
  }

  /**
   * @param fhaudit asigna un valor de actualización
   */
  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * @return usuario que ha modificado
   */
  @JsonIgnore
  public String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * @param cdusuaud asigna el usuario que ha realizado la últimam modificación
   */
  public void setCdUsuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdusuaud == null) ? 0 : cdusuaud.hashCode());
    result = prime * result + ((fhaudit == null) ? 0 : fhaudit.hashCode());
    result = prime * result + version;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CanonEntity other = (CanonEntity) obj;
    if (cdusuaud == null) {
      if (other.cdusuaud != null)
        return false;
    }
    else if (!cdusuaud.equals(other.cdusuaud))
      return false;
    if (fhaudit == null) {
      if (other.fhaudit != null)
        return false;
    }
    else if (!fhaudit.equals(other.fhaudit))
      return false;
    if (version != other.version)
      return false;
    return true;
  }

}
