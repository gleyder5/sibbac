package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_INDICADOR_DE_CAPACIDAD" )
public class Tmct0IndicadorDeCapacidad implements java.io.Serializable  {


	private static final long serialVersionUID = -5384407646454378016L;
	
	private Long id_indicador_capacidad;
	private Long valor;
	private String cd_codigo;
	private String descripcion;
	
	
	public Tmct0IndicadorDeCapacidad() {}


	public Tmct0IndicadorDeCapacidad(Long id_indicador_capacidad, Long valor,
			String cd_codigo, String descripcion) {
		super();
		this.id_indicador_capacidad = id_indicador_capacidad;
		this.valor = valor;
		this.cd_codigo = cd_codigo;
		this.descripcion = descripcion;
	}

	@Id
	@Column( name = "ID_INDICADOR_CAPACIDAD", unique = true, nullable = false, length = 8 )
	public Long getId_indicador_capacidad() {
		return id_indicador_capacidad;
	}


	public void setId_indicador_capacidad(Long id_indicador_capacidad) {
		this.id_indicador_capacidad = id_indicador_capacidad;
	}

	@Column( name = "VALOR" , length = 8)
	public Long getValor() {
		return valor;
	}


	public void setValor(Long valor) {
		this.valor = valor;
	}

	@Column( name = "CD_CODIGO", length = 1 )
	public String getCd_codigo() {
		return cd_codigo;
	}


	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}

	@Column( name = "DESCRIPCION", length = 255 )
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
	

	

	
	

}
