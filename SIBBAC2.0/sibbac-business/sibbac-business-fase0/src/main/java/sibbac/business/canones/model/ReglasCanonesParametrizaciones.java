package sibbac.business.canones.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_REGLAS_CANONES_PARAMETRIZACIONES")
public class ReglasCanonesParametrizaciones extends DescriptedCanonEntity<BigInteger> {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -3481827857923506514L;

  @Id
  @Column(name = ID_COLUMN_NAME)
  private BigInteger id;

  @Column(name = "CODIGO", length = 32, nullable = false)
  private String codigo;

  @Column(name = "SENTIDO")
  private Character sentido;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_COMPARATOR_SEGMENTO_MERCADO_CONTRATACION", referencedColumnName = ID_COLUMN_NAME)
  private ListaComparator segmentoMercadoContratacionComparator;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_SEGMENTO_MERCADO_CONTRATACION", referencedColumnName = ID_COLUMN_NAME)
  private Lista segmentoMercaContratacionLista;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_COMPARATOR_TIPO_OPERACION_BOLSA", referencedColumnName = ID_COLUMN_NAME)
  private ListaComparator tipoOperacionBolsaComparator;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_TIPO_OPERACION_BOLSA", referencedColumnName = ID_COLUMN_NAME)
  private Lista tipoOperacionBolsaLista;

  @OneToOne
  @JoinColumn(name = "ID_LISTA_CONDICIONES_ESPECIALES_COMPARATOR", referencedColumnName = ID_COLUMN_NAME)
  private ListaComparator condicionesEspecialesComparator;

  @Column(name = "IMPORTE_EFECTIVO_DESDE", length = 18, precision = 2)
  private BigDecimal importeEfectivoDesde;

  @Column(name = "IMPORTE_EFECTIVO_HASTA", length = 18, precision = 2)
  private BigDecimal importeEfectivoHasta;

  @Column(name = "IMPORTE_CANON_FIJO", length = 18, precision = 2)
  private BigDecimal importeCanonFijo;

  @Column(name = "PUNTO_BASICO_EFECTIVO", length = 18, precision = 2)
  private BigDecimal puntoBasicoEfectivo;

  @Column(name = "PUNTO_BASICO_MINIMO", length = 18, precision = 2)
  private BigDecimal puntoBasicoMinimo;

  @Column(name = "PUNTO_BASICO_MAXIMO", length = 18, precision = 2)
  private BigDecimal puntoBasicoMaximo;

  @Column(name = "IMPORTE_CANON_MINIMO", length = 18, precision = 2)
  private BigDecimal importeCanonMinimo;

  @Column(name = "IMPORTE_CANON_MAXIMO", length = 18, precision = 2)
  private BigDecimal importeCanonMaximo;

  @Column(name = "BONIFICACION_COMPARATOR", length = 32)
  private String bonificacionComparator;

  @Column(name = "IMPORTE_CANON_MINIMO_BONIFICACION", length = 18, precision = 2)
  private BigDecimal importeCanonMinimoBonificacion;
  
  @Column(name = "IMPORTE_CANON_MAXIMO_BONIFICACION", length = 18, precision = 2)
  private BigDecimal importeCanonMaximoBonificacion;

  @Column(name = "ACTIVO", length = 1, nullable = false)
  private char activo;

  @ManyToMany
  @JoinTable(name = "TMCT0_REGLAS_CANONES_PARAMETRIZACIONES_AGRUPACION", joinColumns = { @JoinColumn(name = "ID_REGLA_CANON_PARAMETRIZACION", referencedColumnName = ID_COLUMN_NAME) }, inverseJoinColumns = { @JoinColumn(name = "ID_CRITERIO_AGRUPACION", referencedColumnName = ID_COLUMN_NAME) })
  private List<ReglasCanonesCriterioAgrupacion> agrupaciones = new ArrayList<>();

  @ManyToMany
  @JoinTable(name = "TMCT0_REGLAS_CANONES_PARAMETRIZACIONES_CONDICIONES_ESPECIALES", joinColumns = { @JoinColumn(name = "ID_REGLA_CANON_PARAMETRIZACION", referencedColumnName = ID_COLUMN_NAME) }, inverseJoinColumns = { @JoinColumn(name = "ID_CONDICION_ESPECIAL", referencedColumnName = ID_COLUMN_NAME) })
  private List<ReglasCanonesCondicionesEspeciales> condicionesEspeciales = new ArrayList<>();

  public BigInteger getId() {
    return id;
  }

  public String getCodigo() {
    return codigo;
  }

  public Character getSentido() {
    return sentido;
  }

  public ListaComparator getSegmentoMercadoContratacionComparator() {
    return segmentoMercadoContratacionComparator;
  }

  public Lista getSegmentoMercaContratacionLista() {
    return segmentoMercaContratacionLista;
  }

  public ListaComparator getTipoOperacionBolsaComparator() {
    return tipoOperacionBolsaComparator;
  }

  public Lista getTipoOperacionBolsaLista() {
    return tipoOperacionBolsaLista;
  }

  public ListaComparator getCondicionesEspecialesComparator() {
    return condicionesEspecialesComparator;
  }

  public BigDecimal getImporteEfectivoDesde() {
    return importeEfectivoDesde;
  }

  public BigDecimal getImporteEfectivoHasta() {
    return importeEfectivoHasta;
  }

  public BigDecimal getImporteCanonFijo() {
    return importeCanonFijo;
  }

  public BigDecimal getPuntoBasicoEfectivo() {
    return puntoBasicoEfectivo;
  }

  public BigDecimal getPuntoBasicoMinimo() {
    return puntoBasicoMinimo;
  }

  public BigDecimal getPuntoBasicoMaximo() {
    return puntoBasicoMaximo;
  }

  public BigDecimal getImporteCanonMinimo() {
    return importeCanonMinimo;
  }

  public BigDecimal getImporteCanonMaximo() {
    return importeCanonMaximo;
  }

  public String getBonificacionComparator() {
    return bonificacionComparator;
  }

  public char getActivo() {
    return activo;
  }

  public List<ReglasCanonesCriterioAgrupacion> getAgrupaciones() {
    return agrupaciones;
  }

  public List<ReglasCanonesCondicionesEspeciales> getCondicionesEspeciales() {
    return condicionesEspeciales;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setSegmentoMercadoContratacionComparator(ListaComparator segmentoMercadoContratacionComparator) {
    this.segmentoMercadoContratacionComparator = segmentoMercadoContratacionComparator;
  }

  public void setSegmentoMercaContratacionLista(Lista segmentoMercaContratacionLista) {
    this.segmentoMercaContratacionLista = segmentoMercaContratacionLista;
  }

  public void setTipoOperacionBolsaComparator(ListaComparator tipoOperacionBolsaComparator) {
    this.tipoOperacionBolsaComparator = tipoOperacionBolsaComparator;
  }

  public void setTipoOperacionBolsaLista(Lista tipoOperacionBolsaLista) {
    this.tipoOperacionBolsaLista = tipoOperacionBolsaLista;
  }

  public void setCondicionesEspecialesComparator(ListaComparator condicionesEspecialesComparator) {
    this.condicionesEspecialesComparator = condicionesEspecialesComparator;
  }

  public void setImporteEfectivoDesde(BigDecimal importeEfectivoDesde) {
    this.importeEfectivoDesde = importeEfectivoDesde;
  }

  public void setImporteEfectivoHasta(BigDecimal importeEfectivoHasta) {
    this.importeEfectivoHasta = importeEfectivoHasta;
  }

  public void setImporteCanonFijo(BigDecimal importeCanonFijo) {
    this.importeCanonFijo = importeCanonFijo;
  }

  public void setPuntoBasicoEfectivo(BigDecimal puntoBasicoEfectivo) {
    this.puntoBasicoEfectivo = puntoBasicoEfectivo;
  }

  public void setPuntoBasicoMinimo(BigDecimal puntoBasicoMinimo) {
    this.puntoBasicoMinimo = puntoBasicoMinimo;
  }

  public void setPuntoBasicoMaximo(BigDecimal puntoBasicoMaximo) {
    this.puntoBasicoMaximo = puntoBasicoMaximo;
  }

  public void setImporteCanonMinimo(BigDecimal importeCanonMinimo) {
    this.importeCanonMinimo = importeCanonMinimo;
  }

  public void setImporteCanonMaximo(BigDecimal importeCanonMaximo) {
    this.importeCanonMaximo = importeCanonMaximo;
  }

  public void setBonificacionComparator(String bonificacionComparator) {
    this.bonificacionComparator = bonificacionComparator;
  }

  public BigDecimal getImporteCanonMinimoBonificacion() {
    return importeCanonMinimoBonificacion;
  }

  public BigDecimal getImporteCanonMaximoBonificacion() {
    return importeCanonMaximoBonificacion;
  }

  public void setImporteCanonMinimoBonificacion(BigDecimal importeCanonMinimoBonificacion) {
    this.importeCanonMinimoBonificacion = importeCanonMinimoBonificacion;
  }

  public void setImporteCanonMaximoBonificacion(BigDecimal importeCanonMaximoBonificacion) {
    this.importeCanonMaximoBonificacion = importeCanonMaximoBonificacion;
  }

  public void setActivo(char activo) {
    this.activo = activo;
  }

  public void setAgrupaciones(List<ReglasCanonesCriterioAgrupacion> agrupaciones) {
    this.agrupaciones = agrupaciones;
  }

  public void setCondicionesEspeciales(List<ReglasCanonesCondicionesEspeciales> condicionesEspeciales) {
    this.condicionesEspeciales = condicionesEspeciales;
  }

  public boolean isActivo() {
    return this.activo == '1';
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + activo;
    result = prime * result + ((agrupaciones == null) ? 0 : agrupaciones.hashCode());
    result = prime * result + ((bonificacionComparator == null) ? 0 : bonificacionComparator.hashCode());
    result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
    result = prime * result + ((condicionesEspeciales == null) ? 0 : condicionesEspeciales.hashCode());
    result = prime * result
        + ((condicionesEspecialesComparator == null) ? 0 : condicionesEspecialesComparator.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((importeCanonFijo == null) ? 0 : importeCanonFijo.hashCode());
    result = prime * result + ((importeCanonMaximo == null) ? 0 : importeCanonMaximo.hashCode());
    result = prime * result + ((importeCanonMinimo == null) ? 0 : importeCanonMinimo.hashCode());
    result = prime * result + ((importeEfectivoDesde == null) ? 0 : importeEfectivoDesde.hashCode());
    result = prime * result + ((importeEfectivoHasta == null) ? 0 : importeEfectivoHasta.hashCode());
    result = prime * result + ((puntoBasicoEfectivo == null) ? 0 : puntoBasicoEfectivo.hashCode());
    result = prime * result + ((puntoBasicoMaximo == null) ? 0 : puntoBasicoMaximo.hashCode());
    result = prime * result + ((puntoBasicoMinimo == null) ? 0 : puntoBasicoMinimo.hashCode());
    result = prime * result
        + ((segmentoMercaContratacionLista == null) ? 0 : segmentoMercaContratacionLista.hashCode());
    result = prime * result
        + ((segmentoMercadoContratacionComparator == null) ? 0 : segmentoMercadoContratacionComparator.hashCode());
    result = prime * result + ((sentido == null) ? 0 : sentido.hashCode());
    result = prime * result + ((tipoOperacionBolsaComparator == null) ? 0 : tipoOperacionBolsaComparator.hashCode());
    result = prime * result + ((tipoOperacionBolsaLista == null) ? 0 : tipoOperacionBolsaLista.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    ReglasCanonesParametrizaciones other = (ReglasCanonesParametrizaciones) obj;
    if (activo != other.activo)
      return false;
    if (agrupaciones == null) {
      if (other.agrupaciones != null)
        return false;
    }
    else if (!agrupaciones.equals(other.agrupaciones))
      return false;
    if (bonificacionComparator == null) {
      if (other.bonificacionComparator != null)
        return false;
    }
    else if (!bonificacionComparator.equals(other.bonificacionComparator))
      return false;
    if (codigo == null) {
      if (other.codigo != null)
        return false;
    }
    else if (!codigo.equals(other.codigo))
      return false;
    if (condicionesEspeciales == null) {
      if (other.condicionesEspeciales != null)
        return false;
    }
    else if (!condicionesEspeciales.equals(other.condicionesEspeciales))
      return false;
    if (condicionesEspecialesComparator == null) {
      if (other.condicionesEspecialesComparator != null)
        return false;
    }
    else if (!condicionesEspecialesComparator.equals(other.condicionesEspecialesComparator))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    }
    else if (!id.equals(other.id))
      return false;
    if (importeCanonFijo == null) {
      if (other.importeCanonFijo != null)
        return false;
    }
    else if (!importeCanonFijo.equals(other.importeCanonFijo))
      return false;
    if (importeCanonMaximo == null) {
      if (other.importeCanonMaximo != null)
        return false;
    }
    else if (!importeCanonMaximo.equals(other.importeCanonMaximo))
      return false;
    if (importeCanonMinimo == null) {
      if (other.importeCanonMinimo != null)
        return false;
    }
    else if (!importeCanonMinimo.equals(other.importeCanonMinimo))
      return false;
    if (importeEfectivoDesde == null) {
      if (other.importeEfectivoDesde != null)
        return false;
    }
    else if (!importeEfectivoDesde.equals(other.importeEfectivoDesde))
      return false;
    if (importeEfectivoHasta == null) {
      if (other.importeEfectivoHasta != null)
        return false;
    }
    else if (!importeEfectivoHasta.equals(other.importeEfectivoHasta))
      return false;
    if (puntoBasicoEfectivo == null) {
      if (other.puntoBasicoEfectivo != null)
        return false;
    }
    else if (!puntoBasicoEfectivo.equals(other.puntoBasicoEfectivo))
      return false;
    if (puntoBasicoMaximo == null) {
      if (other.puntoBasicoMaximo != null)
        return false;
    }
    else if (!puntoBasicoMaximo.equals(other.puntoBasicoMaximo))
      return false;
    if (puntoBasicoMinimo == null) {
      if (other.puntoBasicoMinimo != null)
        return false;
    }
    else if (!puntoBasicoMinimo.equals(other.puntoBasicoMinimo))
      return false;
    if (segmentoMercaContratacionLista == null) {
      if (other.segmentoMercaContratacionLista != null)
        return false;
    }
    else if (!segmentoMercaContratacionLista.equals(other.segmentoMercaContratacionLista))
      return false;
    if (segmentoMercadoContratacionComparator == null) {
      if (other.segmentoMercadoContratacionComparator != null)
        return false;
    }
    else if (!segmentoMercadoContratacionComparator.equals(other.segmentoMercadoContratacionComparator))
      return false;
    if (sentido == null) {
      if (other.sentido != null)
        return false;
    }
    else if (!sentido.equals(other.sentido))
      return false;
    if (tipoOperacionBolsaComparator == null) {
      if (other.tipoOperacionBolsaComparator != null)
        return false;
    }
    else if (!tipoOperacionBolsaComparator.equals(other.tipoOperacionBolsaComparator))
      return false;
    if (tipoOperacionBolsaLista == null) {
      if (other.tipoOperacionBolsaLista != null)
        return false;
    }
    else if (!tipoOperacionBolsaLista.equals(other.tipoOperacionBolsaLista))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return toString("Parametrizaciones");
  }

}
