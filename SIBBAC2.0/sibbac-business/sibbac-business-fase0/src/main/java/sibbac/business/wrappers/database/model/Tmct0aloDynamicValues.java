package sibbac.business.wrappers.database.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0ALO_DYNAMIC_VALUES")
public class Tmct0aloDynamicValues extends DynamicValuesBase {

  /**
   * 
   */
  private static final long serialVersionUID = -7746120540067947074L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
      @AttributeOverride(name = "nombre", column = @Column(name = "NOMBRE", nullable = false, length = 32)) })
  private Tmct0aloDynamicValuesId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false) })
  private Tmct0alo tmct0alo;

  public Tmct0aloDynamicValuesId getId() {
    return id;
  }

  public void setId(Tmct0aloDynamicValuesId id) {
    this.id = id;
  }

  public Tmct0alo getTmct0alo() {
    return tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

}
