package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.database.DBConstants;


@Entity
@Table( name = DBConstants.WRAPPERS.SWI )
public class Tmct0swi implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1748837168974655383L;
	@Id
	@Column( name = "SWIFT", length = 11, nullable = false )
	private String				swift;
	@Column( name = "DESCRIPTION", length = 176, nullable = false )
	private String				description;

	public Tmct0swi() {
	}

	public Tmct0swi(String swift, String description) {
		super();
		this.swift = swift;
		this.description = description;
	}

  public String getSwift() {
    return swift;
  }

  public String getDescription() {
    return description;
  }

	
}
