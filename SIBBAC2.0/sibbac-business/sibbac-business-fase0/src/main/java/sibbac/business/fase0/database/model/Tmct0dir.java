package sibbac.business.fase0.database.model;

/**
 * This class represent the table TMCT0DIR. ALIAS DIRECTIONS Documentación de la tablaTMCT0DIR <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 

 **/
public class Tmct0dir {

  /**
   * Table Field CDBROCLI. CLIENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdbrocli;

  /**
   * Table Field CDALIASS. CUENTA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdaliass;
  /**
   * Table Field CDSUBCTA. SUBCUENTA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdsubcta;
  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.math.BigDecimal numsec;
  /**
   * Table Field NAMEID. CONTACT NAME Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nameid;
  /**
   * Table Field EMPLOY. EMPLOYMENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String employ;
  /**
   * Table Field TPADDRES. ADDRESS TYPE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String tpaddres;
  /**
   * Table Field NBSTREET. STREET Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nbstreet;
  /**
   * Table Field NUSTREET. NUMBER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nustreet;
  /**
   * Table Field NBLOCAT. LOCATION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nblocat;
  /**
   * Table Field NBPROVIN. PROVINCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nbprovin;
  /**
   * Table Field CDZIPCOD. POSTAL CODE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdzipcod;
  /**
   * Table Field CDCOUNTR. COUNTRY CODE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdcountr;
  /**
   * Table Field NUPHONEE. PHONE NUMBER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nuphonee;
  /**
   * Table Field EMAILADD. EMAIL ADDRESS Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String emailadd;
  /**
   * Table Field MCONFIRM. CONFIRM METHOD -FIDESSA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String mconfirm;
  /**
   * Table Field NCONFIRM. CONFIRM LEVEL -FIDESSA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String nconfirm;
  /**
   * Table Field IDOASYS. ID ELECTRONIC CONFIRMATION (OASYS-FIDESSA) Documentación columna <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 

   **/
  protected java.lang.String idoasys;
  /**
   * Table Field CDUSUAUD. AUDIT USER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. AUDIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.sql.Timestamp fhaudit;
  /**
   * Table Field FHINICIO. INIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.math.BigDecimal fhinicio;
  /**
   * Table Field FHFINAL. FINAL DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 

   **/
  protected java.math.BigDecimal fhfinal;

  /**
   * Set value of Field CDBROCLI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdbrocli value of the field
   * @see #cdbrocli

   **/
  public void setCdbrocli(java.lang.String _cdbrocli) {
    cdbrocli = _cdbrocli;
  }

  /**
   * Get value of Field CDBROCLI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdbrocli

   **/
  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  /**
   * Set value of Field CDALIASS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdaliass value of the field
   * @see #cdaliass

   **/
  public void setCdaliass(java.lang.String _cdaliass) {
    cdaliass = _cdaliass;
  }

  /**
   * Get value of Field CDALIASS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdaliass

   **/
  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  /**
   * Set value of Field CDSUBCTA <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdsubcta value of the field
   * @see #cdsubcta

   **/
  public void setCdsubcta(java.lang.String _cdsubcta) {
    cdsubcta = _cdsubcta;
  }

  /**
   * Get value of Field CDSUBCTA <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdsubcta

   **/
  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  /**
   * Set value of Field NUMSEC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _numsec value of the field
   * @see #numsec

   **/
  public void setNumsec(java.math.BigDecimal _numsec) {
    numsec = _numsec;
  }

  /**
   * Get value of Field NUMSEC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #numsec

   **/
  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  /**
   * Set value of Field NAMEID <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nameid value of the field
   * @see #nameid

   **/
  public void setNameid(java.lang.String _nameid) {
    nameid = _nameid;
  }

  /**
   * Get value of Field NAMEID <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nameid

   **/
  public java.lang.String getNameid() {
    return nameid;
  }

  /**
   * Set value of Field EMPLOY <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _employ value of the field
   * @see #employ

   **/
  public void setEmploy(java.lang.String _employ) {
    employ = _employ;
  }

  /**
   * Get value of Field EMPLOY <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #employ

   **/
  public java.lang.String getEmploy() {
    return employ;
  }

  /**
   * Set value of Field TPADDRES <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _tpaddres value of the field
   * @see #tpaddres

   **/
  public void setTpaddres(java.lang.String _tpaddres) {
    tpaddres = _tpaddres;
  }

  /**
   * Get value of Field TPADDRES <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #tpaddres

   **/
  public java.lang.String getTpaddres() {
    return tpaddres;
  }

  /**
   * Set value of Field NBSTREET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nbstreet value of the field
   * @see #nbstreet

   **/
  public void setNbstreet(java.lang.String _nbstreet) {
    nbstreet = _nbstreet;
  }

  /**
   * Get value of Field NBSTREET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nbstreet

   **/
  public java.lang.String getNbstreet() {
    return nbstreet;
  }

  /**
   * Set value of Field NUSTREET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nustreet value of the field
   * @see #nustreet

   **/
  public void setNustreet(java.lang.String _nustreet) {
    nustreet = _nustreet;
  }

  /**
   * Get value of Field NUSTREET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nustreet

   **/
  public java.lang.String getNustreet() {
    return nustreet;
  }

  /**
   * Set value of Field NBLOCAT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nblocat value of the field
   * @see #nblocat

   **/
  public void setNblocat(java.lang.String _nblocat) {
    nblocat = _nblocat;
  }

  /**
   * Get value of Field NBLOCAT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nblocat

   **/
  public java.lang.String getNblocat() {
    return nblocat;
  }

  /**
   * Set value of Field NBPROVIN <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nbprovin value of the field
   * @see #nbprovin

   **/
  public void setNbprovin(java.lang.String _nbprovin) {
    nbprovin = _nbprovin;
  }

  /**
   * Get value of Field NBPROVIN <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nbprovin

   **/
  public java.lang.String getNbprovin() {
    return nbprovin;
  }

  /**
   * Set value of Field CDZIPCOD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdzipcod value of the field
   * @see #cdzipcod

   **/
  public void setCdzipcod(java.lang.String _cdzipcod) {
    cdzipcod = _cdzipcod;
  }

  /**
   * Get value of Field CDZIPCOD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdzipcod

   **/
  public java.lang.String getCdzipcod() {
    return cdzipcod;
  }

  /**
   * Set value of Field CDCOUNTR <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdcountr value of the field
   * @see #cdcountr

   **/
  public void setCdcountr(java.lang.String _cdcountr) {
    cdcountr = _cdcountr;
  }

  /**
   * Get value of Field CDCOUNTR <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdcountr

   **/
  public java.lang.String getCdcountr() {
    return cdcountr;
  }

  /**
   * Set value of Field NUPHONEE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nuphonee value of the field
   * @see #nuphonee

   **/
  public void setNuphonee(java.lang.String _nuphonee) {
    nuphonee = _nuphonee;
  }

  /**
   * Get value of Field NUPHONEE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nuphonee

   **/
  public java.lang.String getNuphonee() {
    return nuphonee;
  }

  /**
   * Set value of Field EMAILADD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _emailadd value of the field
   * @see #emailadd

   **/
  public void setEmailadd(java.lang.String _emailadd) {
    emailadd = _emailadd;
  }

  /**
   * Get value of Field EMAILADD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #emailadd

   **/
  public java.lang.String getEmailadd() {
    return emailadd;
  }

  /**
   * Set value of Field MCONFIRM <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _mconfirm value of the field
   * @see #mconfirm

   **/
  public void setMconfirm(java.lang.String _mconfirm) {
    mconfirm = _mconfirm;
  }

  /**
   * Get value of Field MCONFIRM <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #mconfirm

   **/
  public java.lang.String getMconfirm() {
    return mconfirm;
  }

  /**
   * Set value of Field NCONFIRM <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _nconfirm value of the field
   * @see #nconfirm

   **/
  public void setNconfirm(java.lang.String _nconfirm) {
    nconfirm = _nconfirm;
  }

  /**
   * Get value of Field NCONFIRM <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #nconfirm

   **/
  public java.lang.String getNconfirm() {
    return nconfirm;
  }

  /**
   * Set value of Field IDOASYS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _idoasys value of the field
   * @see #idoasys

   **/
  public void setIdoasys(java.lang.String _idoasys) {
    idoasys = _idoasys;
  }

  /**
   * Get value of Field IDOASYS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #idoasys

   **/
  public java.lang.String getIdoasys() {
    return idoasys;
  }

  /**
   * Set value of Field CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _cdusuaud value of the field
   * @see #cdusuaud

   **/
  public void setCdusuaud(java.lang.String _cdusuaud) {
    cdusuaud = _cdusuaud;
  }

  /**
   * Get value of Field CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #cdusuaud

   **/
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * Set value of Field FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _fhaudit value of the field
   * @see #fhaudit

   **/
  public void setFhaudit(java.sql.Timestamp _fhaudit) {
    fhaudit = _fhaudit;
  }

  /**
   * Get value of Field FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #fhaudit

   **/
  public java.sql.Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * Set value of Field FHINICIO <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _fhinicio value of the field
   * @see #fhinicio

   **/
  public void setFhinicio(java.math.BigDecimal _fhinicio) {
    fhinicio = _fhinicio;
  }

  /**
   * Get value of Field FHINICIO <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #fhinicio

   **/
  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }

  /**
   * Set value of Field FHFINAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @param _fhfinal value of the field
   * @see #fhfinal

   **/
  public void setFhfinal(java.math.BigDecimal _fhfinal) {
    fhfinal = _fhfinal;
  }

  /**
   * Get value of Field FHFINAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @return value of the field
   * @see #fhfinal

   **/
  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }

}
