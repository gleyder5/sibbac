package sibbac.business.fase0.database.bo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.dto.HoraEjeccucionDTO;
import sibbac.business.fase0.database.dto.PTIMessageVersionDateDTO;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0cfgId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.common.SIBBACCommons;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Todos los metodos deprecated han pasado a Tmct0xasAndTmct0cfgConfig
 * @author xIS16630
 *
 */
@Service
public class Tmct0cfgBo extends AbstractBo<Tmct0cfg, Tmct0cfgId, Tmct0cfgDao> {

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;
  public List<Tmct0cfg> findByAplicationAndProcess(String aplication,final String process){
    return this.dao.findByAplicationAndProcess(aplication,process);
  }
  public Tmct0cfg findByAplicationAndProcessAndKeyname(String aplication, final String process, final String keyname) {
    return this.dao.findByAplicationAndProcessAndKeyname(aplication, process, keyname);
  } // findByAplicationAndProcessAndKeyname

  private Tmct0cfg findByTmct0cfgConfig(String applName, Tmct0cfgConfig config) {
    return dao.findByAplicationAndProcessAndKeyname(applName, config.getProcess(), config.getKeyname());
  }

  private List<Tmct0cfg> findByLikeTmct0cfgConfig(String applName, Tmct0cfgConfig config) {
    return dao.findByAplicationAndProcessAndLikeKeyname(applName, config.getProcess(), "%" + config.getKeyname() + "%");
  }

  /**
   * En su lugar deberiamos usar @see
   * {@link sibbac.business.wrappers.database.bo.FestivoBo.Class#findAllFechaByCdmercad(cdmercad)}
   * */
  @Deprecated
  public List<Date> findHolidaysBolsa() {
    List<Date> holidays = new ArrayList<Date>();
    List<Tmct0cfg> cfgList = this.dao.findHolidaysBolsa();
    for (Tmct0cfg cfg : cfgList) {
      try {
        holidays.add(SIBBACCommons.convertStringToDate(cfg.getKeyvalue(), "yyyy-MM-dd"));
      }
      catch (Exception e) {
        LOG.error("Tmct0cfgBo :: processLine] Error convirtiendo a fecha application=" + cfg.getId().getApplication()
            + ", process=" + cfg.getId().getProcess() + ", process=" + cfg.getId().getKeyname());
      }
    }
    return holidays;
  } // findByAplicationAndProcessAndKeyname

  public List<Integer> getWorkDaysOfWeek() {
    List<Integer> workDaysOfWeekConfigList = new ArrayList<Integer>();
    for (String workDay : findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.CONFIG_WEEK_WORK_DAYS)
        .getKeyvalue().split(",")) {
      workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
    }
    return workDaysOfWeekConfigList;
  }

  /**
   * En su lugar deberiamos usar @see
   * {@link sibbac.business.wrappers.database.bo.FestivoBo.Class#findAllFechaByCdmercad(cdmercad)}
   * */
  @Deprecated
  public List<Date> getHolidays() throws ParseException {
    List<Date> holidaysConfigList = new ArrayList<Date>();
    DateFormat dF2 = new SimpleDateFormat("yyyy-MM-dd");
    for (Tmct0cfg tmct0cfg : findByLikeTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.CONFIG_HOLIDAYS_DAYS)) {
      holidaysConfigList.add(dF2.parse(tmct0cfg.getKeyvalue()));
    }
    return holidaysConfigList;
  }

  /**
   * @return true if is config to load internationa xml's
   * CONFIG_LOAD_XML_INTERNACIONAL("CONFIG", "xml.load.internacional"),
   */
  public boolean isLoadXmlInternacional() {
    boolean res = false;
    Tmct0cfg loadXmlInter = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.CONFIG_LOAD_XML_INTERNACIONAL);
    if (loadXmlInter != null && loadXmlInter.getKeyvalue() != null
        && "S".equalsIgnoreCase(loadXmlInter.getKeyvalue().trim())) {
      res = true;
    }
    return res;
  }

  public String getHoraInicioSessionPti() {
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.AUD_INIT_DAY_HOUR_BEGIN);
    if (cfg != null && cfg.getKeyvalue() != null) {
      return cfg.getKeyvalue().trim();
    }
    else {
      return null;
    }
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public String findHoraInicioImputacionesS3Pti() throws SIBBACBusinessException {
    String res = null;
    Tmct0cfg horaIniImputacionesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI);
    if (horaIniImputacionesCfg != null) {
      res = horaIniImputacionesCfg.getKeyvalue().trim();
      ;
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Tmct0cfg getHoraInicioImputacionesS3Pti() {
    return findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI);
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Tmct0cfg getHoraInicioImputacionesS3EuroCcp() {
    return findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI_EUROCCP);

  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public String findHoraInicioImputacionesS3EuroCcp() throws SIBBACBusinessException {
    String res = null;
    Tmct0cfg horaIniImputacionesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI_EUROCCP);
    if (horaIniImputacionesCfg != null) {
      res = horaIniImputacionesCfg.getKeyvalue().trim();
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * para segmento mab
   * @throws SIBBACBusinessException
   */
  public Tmct0cfg getHoraInicioImputacionesMabS3Pti() {
    return findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI_MAB);
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * para segmento mab
   * @throws SIBBACBusinessException
   */
  public String findHoraInicioImputacionesMabS3Pti() throws SIBBACBusinessException {
    String res = null;
    Tmct0cfg horaIniImputacionesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_HORAINI_MAB);
    if (horaIniImputacionesCfg != null) {
      res = horaIniImputacionesCfg.getKeyvalue().trim();
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Tmct0cfg getHoraFinalImputacionesS3Pti() throws SIBBACBusinessException {
    return findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND);

  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public String findHoraFinalImputacionesS3Pti() throws SIBBACBusinessException {
    String res = null;
    Tmct0cfg horaIniImputacionesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND);
    if (horaIniImputacionesCfg != null) {
      res = horaIniImputacionesCfg.getKeyvalue().trim();
    }
    else {
      LOG.warn("[findHoraFinalImputacionesS3] la hora final de las imputaciones s3 no esta configurada {}",
          Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND);
      res = "235959";
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Tmct0cfg getHoraFinalImputacionesS3EuroCcp() throws SIBBACBusinessException {
    return findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_EUROCCP);
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public String findHoraFinalImputacionesS3EuroCcp() throws SIBBACBusinessException {
    String res = null;
    Tmct0cfg horaIniImputacionesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_EUROCCP);
    if (horaIniImputacionesCfg != null) {
      res = horaIniImputacionesCfg.getKeyvalue().trim();
    }
    else {
      LOG.warn("[findHoraFinalImputacionesS3] la hora final de las imputaciones s3 no esta configurada {}",
          Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_EUROCCP);
      res = "235959";
    }
    return res;
  }

  public Tmct0cfg getHoraFinalImputacionesMabS3Pti() throws SIBBACBusinessException {
    return findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_MAB);

  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * para segmento mab
   * @throws SIBBACBusinessException
   */
  public String findHoraFinalImputacionesMabS3Pti() throws SIBBACBusinessException {
    String res = null;
    Tmct0cfg horaIniImputacionesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_MAB);
    if (horaIniImputacionesCfg != null) {
      res = horaIniImputacionesCfg.getKeyvalue().trim();
    }
    else {
      LOG.warn("[findHoraFinalImputacionesMabS3] la hora final de las imputaciones s3 no esta configurada {}",
          Tmct0cfgConfig.PTI_IMPUTACIONES_HORAEND_MAB);
      res = "235959";
    }
    return res;
  }

  /**
   * 
   * @return hora inicio en configuracion del proceso de imputacion s3/Botonazo
   * @throws SIBBACBusinessException
   */
  public Integer findDiasRevisionProcesoEnvioMovimientos() throws SIBBACBusinessException {
    Integer res = null;
    Tmct0cfg envioMosDaysPrevCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ENVIO_MO_SCAN_DAYS_PREV);
    if (envioMosDaysPrevCfg != null) {
      res = Integer.parseInt(envioMosDaysPrevCfg.getKeyvalue().trim());
    }
    return res;
  }

  /**
   * 
   * @return dias de activacion de procesos
   */
  public List<Integer> findDiasActivacionProcesos() {
    List<Integer> res = new ArrayList<Integer>();
    Tmct0cfg diasAcitvacionProcesos = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.CONFIG_WEEK_PROCESS_ACTIVATION_DAYS);
    if (diasAcitvacionProcesos != null) {
      String[] diasArray = diasAcitvacionProcesos.getKeyvalue().trim().split(",");
      for (String dia : diasArray) {
        res.add(Integer.parseInt(dia.trim()));
      }
    }
    return res;
  }

  // **********************************************CONFIG MO'S
  // *************************************************

  /**
   * @return Estados ecc Vivos mas los estados internos vivos 6, 9, 20 etc
   */
  public List<String> findEstadosEccActivos() {
    List<String> res = new ArrayList<String>();
    Tmct0cfg cdestadomovActivosCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESTADOS_ECC_ACTIVOS);

    if (cdestadomovActivosCfg != null) {
      res.addAll(Arrays.asList(cdestadomovActivosCfg.getKeyvalue().trim().split(",")));
    }
    return res;
  }

  /**
   * @return lista de tpopebol, cdmoperacionmkt que son sin ecc
   */
  public String getTpopebolsSinEcc() {
    String res = null;
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_CDOPERACIONMKT_SIN_ECC);
    if (cfg != null) {
      res = cfg.getKeyvalue();
    }
    return res;
  }

  public HoraEjeccucionDTO getIfIsHoraEjecucionFicheroSpb1819() {
    return getIfIsHoraEjecucion(Tmct0cfgConfig.SIBBAC20_SPB_HORAS_EJECUCION);
  }

  private List<HoraEjeccucionDTO> getHorasEjeccion(Tmct0cfgConfig config) {
    List<HoraEjeccucionDTO> res = new ArrayList<HoraEjeccucionDTO>();
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, config);
    String valorCfg = null;
    if (cfg != null) {
      valorCfg = cfg.getKeyvalue();
    }
    if (StringUtils.isNotBlank(valorCfg)) {
      String[] horasArray = valorCfg.split(",");
      String[] valoresHoraEjecucionArray;
      if (horasArray != null && horasArray.length > 0) {
        for (String horaEjecucionSt : horasArray) {
          Integer horaDesde = null;
          Integer horaHasta = null;
          Integer diaDesde = null;
          Integer diaHasta = null;
          valoresHoraEjecucionArray = horaEjecucionSt.split("-");
          Integer valorHoraDia;
          for (String valorHoraDiaSt : valoresHoraEjecucionArray) {
            valorHoraDia = Integer.parseInt(StringUtils.trim(valorHoraDiaSt.replaceAll("\\D+", "")));
            if (valorHoraDiaSt.contains("H")) {
              // es una hora
              if (horaDesde == null) {
                horaDesde = valorHoraDia;
              }
              else {
                horaHasta = valorHoraDia;
              }
            }
            else if (valorHoraDiaSt.contains("D")) {
              // es un dia
              if (diaDesde == null) {
                diaDesde = valorHoraDia;
              }
              else {
                diaHasta = valorHoraDia;
              }
            }
            else {
              LOG.warn("INCIDENCIA- configuracion fecha y hora de ejecucion del proceso.{} ", config);
            }

          }
          if (horaHasta == null) {
            horaHasta = horaDesde;
          }
          if (diaHasta == null) {
            diaHasta = diaDesde;
          }
          res.add(new HoraEjeccucionDTO(horaDesde, horaHasta, diaDesde, diaHasta));
        }
      }
      Collections.sort(res, new Comparator<HoraEjeccucionDTO>() {
        @Override
        public int compare(HoraEjeccucionDTO arg0, HoraEjeccucionDTO arg1) {
          return arg0.getHoraInicioEjecucion() >= arg1.getHoraInicioEjecucion() ? 1
              : arg0.getHoraFinEjecucion() >= arg1.getHoraFinEjecucion() ? 1 : 0;
        }
      });
    }
    return res;
  }

  /**
   * * EJEMPLO:12H-21D se ejecuta a las 12 y 21 dias atras 12H-20H-21D-1D </br>
   * se ejecuta de 12 a 20 y desde dia hace 21 dias hasta 1 dia Devuelve si es
   * hora de ejecucion dada una configuracion
   * 
   * @param config
   * @return
   */
  private HoraEjeccucionDTO getIfIsHoraEjecucion(Tmct0cfgConfig config) {
    int hourOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    // TODO dotar a este metodo de mas inteligencia para que podamos configurar
    // las fechas y dias de la semana
    // int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    // int dayOfMoth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    // int dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
    HoraEjeccucionDTO horaEjecucion = null;
    List<HoraEjeccucionDTO> horasEjecucion = getHorasEjeccion(config);
    LOG.debug("{}Horas planificadas: {}", config, horasEjecucion);
    for (HoraEjeccucionDTO horaEjeccucionDTO : horasEjecucion) {
      if (hourOfDay == horaEjeccucionDTO.getHoraInicioEjecucion()
          || hourOfDay > horaEjeccucionDTO.getHoraInicioEjecucion()
          && hourOfDay < horaEjeccucionDTO.getHoraFinEjecucion()) {
        horaEjecucion = horaEjeccucionDTO;
        break;
      }
    }

    return horaEjecucion;
  }

  public Tmct0cfg getPtiMessageVersion() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S.getProcess(),
        Tmct0cfgConfig.PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S.getKeyname());
    return cfg;
  }

  public Tmct0cfg getPtiAppIdentityDataEntidad() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_APPIDENTITYDATA_ENTIDAD.getProcess(),
        Tmct0cfgConfig.PTI_APPIDENTITYDATA_ENTIDAD.getKeyname());
    return cfg;
  }

  public Tmct0cfg getPtiImputacionesDailyDeactivate() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_IMPUTACIONES_DAILY_DEACTIVATE.getProcess(),
        Tmct0cfgConfig.PTI_IMPUTACIONES_DAILY_DEACTIVATE.getKeyname());
    return cfg;
  }

  public Tmct0cfg getPtiUsuarioOrigenImputacionPropia() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_USUARIO_ORIGEN_IMPUTACION_PROPIA.getProcess(),
        Tmct0cfgConfig.PTI_USUARIO_ORIGEN_IMPUTACION_PROPIA.getKeyname());
    return cfg;
  }

  public PTIMessageVersion getPtiMessageVersionFromTmct0cfg(String messageVersionCfg, Date date)
      throws SIBBACBusinessException {
    PTIMessageVersion version = PTIMessageVersion.VERSION_1;
    ObjectMapper mapper = new ObjectMapper();
    mapper.setDateFormat(new SimpleDateFormat(FormatDataUtils.DATE_FORMAT_1));
    List<PTIMessageVersionDateDTO> values;
    try {
      values = mapper.readValue(messageVersionCfg,
          TypeFactory.defaultInstance().constructCollectionType(List.class, PTIMessageVersionDateDTO.class));
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    if (CollectionUtils.isNotEmpty(values)) {
      for (PTIMessageVersionDateDTO ptiMessageVersionDate : values) {
        if (date.compareTo(ptiMessageVersionDate.getInit()) >= 0
            && (ptiMessageVersionDate.getEnd() == null || date.compareTo(ptiMessageVersionDate.getEnd()) <= 0)) {
          version = ptiMessageVersionDate.getVersion();
          break;
        }
      }
    }
    return version;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public PTIMessageVersion getPtiMessageVersionFromTmct0cfg(Date date) throws SIBBACBusinessException {
    try {
      Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
          Tmct0cfgConfig.PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S.getProcess(),
          Tmct0cfgConfig.PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S.getKeyname());
      return getPtiMessageVersionFromTmct0cfg(cfg.getKeyvalue(), date);
    }
    catch (RuntimeException e) {
      String message = MessageFormat.format(
          "Error inesperado al recuperar la versión de parse de mensaje PTI con fecha {0}", date);
      LOG.error("[getPtiMessageVersionFromTmct0cfg] {}", message, e);
    }
    return PTIMessageVersion.VERSION_1;
  }

  public String getIdEuroCcp() {
    String res = null;
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.EUROCCP_IDECC);
    if (cfg != null) {
      res = cfg.getKeyvalue();
    }
    return res;
  }

  public String getIdPti() {
    String res = null;
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_IDECC);
    if (cfg != null) {
      res = cfg.getKeyvalue();
    }
    return res;
  }

  public String getAliasScriptDividend() {
    String res = "";
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.SCRIPT_DIVIDEN_ALIAS_CONFIG);
    if (cfg != null) {
      res = cfg.getKeyvalue();
    }
    return res;
  }

  public List<String> getPtiMessageTypesInOrden() {
    Tmct0cfg typesCfg = findByTmct0cfgConfig(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_DATABASE_MESSAGE_PROCESS_MESSAGE_TYPES_IN_ORDER);
    return Arrays.asList(typesCfg.getKeyvalue().split(","));
  }

  public Long getMinutosEscaladoBokNoActivo() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESCALADOBOK_MINUTOS_NO_ACTIVOS.getProcess(),
        Tmct0cfgConfig.PTI_ESCALADOBOK_MINUTOS_NO_ACTIVOS.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return Long.parseLong(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public Long getMinutosEscaladoAloNoActivo() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESCALADOALO_MINUTOS_NO_ACTIVOS.getProcess(),
        Tmct0cfgConfig.PTI_ESCALADOALO_MINUTOS_NO_ACTIVOS.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return Long.parseLong(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public Long getMinutosEscaladoAlcNoActivo() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESCALADOALC_MINUTOS_NO_ACTIVOS.getProcess(),
        Tmct0cfgConfig.PTI_ESCALADOALC_MINUTOS_NO_ACTIVOS.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return Long.parseLong(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public Integer getCdestadoasigGtLockUserActivity() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESTADOS_ASIGNACION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO.getProcess(),
        Tmct0cfgConfig.PTI_ESTADOS_ASIGNACION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return Integer.parseInt(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public String getCdestadoasigLockUserActivity() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESTADOS_ASIGNACION_BLOQUEAN_ACTIVIDAD_USUARIO.getProcess(),
        Tmct0cfgConfig.PTI_ESTADOS_ASIGNACION_BLOQUEAN_ACTIVIDAD_USUARIO.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return datosConfiguracion.getKeyvalue().trim();
    }
    else {
      return null;
    }
  }

  public List<Integer> getListCdestadoasigLockUserActivity() {
    String estados = getCdestadoasigLockUserActivity();
    return convertirAListaEstados(estados);
  }

  private List<Integer> convertirAListaEstados(String estados) {
    List<Integer> listEstados = new ArrayList<Integer>();
    if (estados != null && !estados.trim().isEmpty()) {
      String[] estadosArr = estados.split(",");
      for (String st : estadosArr) {
        listEstados.add(Integer.parseInt(st.trim()));
      }
    }
    return listEstados;
  }

  public Integer getCdestadoentrecGtLockUserActivity() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.PTI_ESTADOS_ENTREGA_RECEPCION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO.getProcess(),
        Tmct0cfgConfig.PTI_ESTADOS_ENTREGA_RECEPCION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return Integer.parseInt(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public BigDecimal getEfectivoMinimoDerechos() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.ECONOMIC_CUPON_EFFECTIVE_LESS_THAN.getProcess(),
        Tmct0cfgConfig.ECONOMIC_CUPON_EFFECTIVE_LESS_THAN.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return new BigDecimal(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public BigDecimal getCanonEfectivoMinimoDerechos() {
    Tmct0cfg datosConfiguracion = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.ECONOMIC_CUPON_MANAGEMENT_RIGHTS.getProcess(),
        Tmct0cfgConfig.ECONOMIC_CUPON_MANAGEMENT_RIGHTS.getKeyname());
    if (datosConfiguracion != null && datosConfiguracion.getKeyvalue() != null
        && !datosConfiguracion.getKeyvalue().trim().isEmpty()) {
      return new BigDecimal(datosConfiguracion.getKeyvalue().trim());
    }
    else {
      return null;
    }
  }

  public Tmct0cfg getBanifLastModifiedDateFromProcessedFile() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.BANIF_LAST_MODIFIED_DATE_FROM_PROCESSED_FILES.getProcess(),
        Tmct0cfgConfig.BANIF_LAST_MODIFIED_DATE_FROM_PROCESSED_FILES.getKeyname());
    return cfg;
  }

  public Tmct0cfg getBanifLastLogDateFromProcessedFile() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.BANIF_LAST_LINE_LOG_DATE_FROM_PROCESSED_FILES.getProcess(),
        Tmct0cfgConfig.BANIF_LAST_LINE_LOG_DATE_FROM_PROCESSED_FILES.getKeyname());
    return cfg;
  }

  public Tmct0cfg getFirstDateRepartirCalculos() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.ECONOMIC_SHARE_OUT_FIRST_DATE.getProcess(),
        Tmct0cfgConfig.ECONOMIC_SHARE_OUT_FIRST_DATE.getKeyname());
    return cfg;
  }

  public Tmct0cfg getLastDateRepartirCalculos() {
    Tmct0cfg cfg = findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        Tmct0cfgConfig.ECONOMIC_SHARE_OUT_LAST_DATE.getProcess(),
        Tmct0cfgConfig.ECONOMIC_SHARE_OUT_LAST_DATE.getKeyname());
    return cfg;
  }

  public Integer getLimiteDiasEnvioTitularesEuroccp() throws SIBBACBusinessException {
    Integer res = null;
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.EUROCCP_FTL_DIAS_LIMITE_ENVIO_TITULARES);
    if (cfg != null) {
      try {
        res = Integer.valueOf(cfg.getKeyvalue().trim());
      }
      catch (NumberFormatException e) {
        throw new SIBBACBusinessException(String.format(
            "No se ha podido recuperar de la configuracion el valor: %s, err: %s",
            Tmct0cfgConfig.EUROCCP_FTL_DIAS_LIMITE_ENVIO_TITULARES, e.getMessage()), e);
      }
    }
    return res;
  }

  public Integer getLimiteDiasEnvioTitularesPti() throws SIBBACBusinessException {
    Integer res = null;
    Tmct0cfg cfg = findByTmct0cfgConfig(sibbac20ApplicationName, Tmct0cfgConfig.PTI_FTL_DIAS_LIMITE_ENVIO_TITULARES);
    if (cfg != null) {
      try {
        res = Integer.valueOf(cfg.getKeyvalue().trim());
      }
      catch (NumberFormatException e) {
        throw new SIBBACBusinessException(String.format(
            "No se ha podido recuperar de la configuracion el valor: %s, err: %s",
            Tmct0cfgConfig.EUROCCP_FTL_DIAS_LIMITE_ENVIO_TITULARES, e.getMessage()), e);
      }
    }
    return res;
  }

  // **********************************************CONFIG MO'S
  // *************************************************
} // Tmct0cfgBo
