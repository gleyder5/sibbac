package sibbac.business.canones.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_REGLAS_CANONES_PARAMETRIZACIONES_AGRUPACION")
public class ReglasCanonesParametrizacionesAgrupacion extends CanonEntity {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -3481827857923506514L;

  @EmbeddedId
  private ReglasCanonesParametrizacionesAgrupacionId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_REGLA_CANON_PARAMETRIZACION", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
  private ReglasCanonesParametrizaciones parametrizacion;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CRITERIO_AGRUPACION", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
  private ReglasCanonesCriterioAgrupacion criterioAgrupacion;

  public ReglasCanonesParametrizacionesAgrupacionId getId() {
    return id;
  }

  public ReglasCanonesParametrizaciones getParametrizacion() {
    return parametrizacion;
  }

  public ReglasCanonesCriterioAgrupacion getCriterioAgrupacion() {
    return criterioAgrupacion;
  }

  public void setId(ReglasCanonesParametrizacionesAgrupacionId id) {
    this.id = id;
  }

  public void setParametrizacion(ReglasCanonesParametrizaciones parametrizacion) {
    this.parametrizacion = parametrizacion;
  }

  public void setCriterioAgrupacion(ReglasCanonesCriterioAgrupacion criterioAgrupacion) {
    this.criterioAgrupacion = criterioAgrupacion;
  }

  @Override
  public String toString() {
    return String.format("ReglasCanonesParametrizacionesAgrupacion [id=%s, parametrizacion=%s, criterioAgrupacion=%s]",
        id, parametrizacion, criterioAgrupacion);
  }

}
