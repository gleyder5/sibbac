package sibbac.business.fase0.database.dto;

import java.math.BigInteger;

import sibbac.business.fase0.database.model.Tmct0act;


public class Tmct0actDTO {
	
	private String cdSubCta;
	private String cdBrocli;
	private BigInteger idRegla;
	private String cdCodigoRegla;
	private String cdAliass;
	private String desacrali;
	
	public Tmct0actDTO(){
		
	}
	
	public Tmct0actDTO(final Tmct0act act ){
		this(act.getId().getCdsubcta(),act.getDescrali());
		
	}

	public Tmct0actDTO(String cdSubCta, String cdBrocli){
		this.cdSubCta = cdSubCta;
		this.cdBrocli = cdBrocli;

	}

	public String getCdSubCta() {
		return cdSubCta;
	}

	public void setCdSubCta(String cdSubCta) {
		this.cdSubCta = cdSubCta;
	}

	public String getCdBrocli() {
		return cdBrocli;
	}

	public void setCdBrocli(String cdBrocli) {
		this.cdBrocli = cdBrocli;
	}

	public BigInteger getIdRegla() {
		return idRegla;
	}

	public void setIdRegla(BigInteger idRegla) {
		this.idRegla = idRegla;
	}

	public String getCdCodigoRegla() {
		return cdCodigoRegla;
	}

	public void setCdCodigoRegla(String cdCodigoRegla) {
		this.cdCodigoRegla = cdCodigoRegla;
	}

	public String getCdAliass() {
		return cdAliass;
	}

	public void setCdAliass(String cdAliass) {
		this.cdAliass = cdAliass;
	}
	
	public String getDesacrali() {
		return desacrali;
	}

	public void setDesacrali(String desacrali) {
		this.desacrali = desacrali;
	}

	public static Tmct0actDTO convertObjectToSubCtaDTO( Object[] valuesFromQuery){
		
		Tmct0actDTO subCta = new Tmct0actDTO();
		subCta.setIdRegla((BigInteger) valuesFromQuery[0]);
		subCta.setCdCodigoRegla((String) valuesFromQuery[1]);
		subCta.setCdAliass((String) valuesFromQuery[2]);
		subCta.setCdSubCta((String) valuesFromQuery[3]);
		subCta.setDesacrali((String) valuesFromQuery[4]);
		
		return(subCta);
		
		
	}	
	
	
}
