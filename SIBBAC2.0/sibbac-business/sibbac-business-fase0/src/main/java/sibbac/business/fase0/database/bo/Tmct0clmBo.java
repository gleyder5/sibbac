package sibbac.business.fase0.database.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0clmDao;
import sibbac.business.fase0.database.model.Tmct0clm;
import sibbac.business.fase0.database.model.Tmct0clmId;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0clmBo extends AbstractBo<Tmct0clm, Tmct0clmId, Tmct0clmDao> {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0clmBo.class);

  public Tmct0clmBo() {

  }

  public List<Tmct0clm> findAllByFechaAndCdcleare(final Integer fecha, final String cdcleare) {
    return dao.findAllByFechaAndCdcleare(fecha, cdcleare);
  }

}
