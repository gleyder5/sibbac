package sibbac.business.wrappers.database.model;

import java.io.Serializable;

/**
 * This class represent the table TMCT0CTT. Holders Documentación para TMCT0CTT <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 **/
public class Tmct0ctt implements Serializable {

  /**
   * Table Field NUHOLDER. Holder Sequential Documentación para NUHOLDER <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.math.BigDecimal nuholder;

  /**
   * Table Field CDDCLAVE. Fiscal information code Documentación para CDDCLAVE <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/
  protected java.lang.String cddclave;
  /**
   * Table Field NUDNICIF. DNI/CIF Documentación para NUDNICIF <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nudnicif;
  /**
   * Table Field NBCLIENT. Client name Documentación para NBCLIENT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nbclient;
  /**
   * Table Field NBCLIEN1. Client surname 1 Documentación para NBCLIEN1 <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nbclien1;
  /**
   * Table Field NBCLIEN2. Client surname 2 Documentación para NBCLIEN2 <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nbclien2;
  /**
   * Table Field TPDOMICI. Address type Documentación para TPDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String tpdomici;
  /**
   * Table Field NBDOMICI. Address Documentación para NBDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nbdomici;
  /**
   * Table Field NUDOMICI. Address number Documentación para NUDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nudomici;
  /**
   * Table Field NBCIUDAD. City Documentación para NBCIUDAD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nbciudad;
  /**
   * Table Field NBPROVIN. Province Documentación para NBPROVIN <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String nbprovin;
  /**
   * Table Field CDPOSTAL. Zip code Documentación para CDPOSTAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String cdpostal;
  /**
   * Table Field CDDEPAIS. Code country Documentación para CDDEPAIS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String cddepais;
  /**
   * Table Field NUSECNBR. Sequence address Documentación para NUSECNBR <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.math.BigDecimal nusecnbr;
  /**
   * Table Field NUSECTIT. Sequence holder Documentación para NUSECTIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.math.BigDecimal nusectit;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.sql.Timestamp fhaudit;

  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String cdusuaud;

  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/
  protected java.lang.Integer nuversion;

  /**
   * Relation 1..1 with table TMCT0CTD. Constraint de relación entre TMCT0CTT y TMCT0CTD <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  protected Tmct0ctd tmct0ctd;

  /**
   * Relation 1..1 with table TMCT0STA. Constraint de relación entre TMCT0CTT y TMCT0STA <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  protected Tmct0sta tmct0sta;
}
