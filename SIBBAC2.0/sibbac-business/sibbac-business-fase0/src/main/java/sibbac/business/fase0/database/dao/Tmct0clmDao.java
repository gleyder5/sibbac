package sibbac.business.fase0.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0clm;
import sibbac.business.fase0.database.model.Tmct0clmId;

@Repository
public interface Tmct0clmDao extends JpaRepository<Tmct0clm, Tmct0clmId> {

  @Query("SELECT C FROM Tmct0clm C WHERE  C.id.cdcleare = :cdcleare  and C.fhinicio <= :fecha AND C.fhfinal >= :fecha ")
  List<Tmct0clm> findAllByFechaAndCdcleare(@Param("fecha") final Integer fecha, @Param("cdcleare") final String cdcleare);

}
