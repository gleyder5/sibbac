package sibbac.business.wrappers.database.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link sibbac.business.contabilidad.database.model.contabilidad.ApunteContable}.
 */
@Table( name = DBConstants.TABLE_PREFIX + DBConstants.CONTABLES.ALC_ESTADOCONT)
@Entity
public class AlcEstadoCont extends ATable<AlcEstadoCont> {
	private static final long	serialVersionUID	= 2912670737174305393L;

	@ManyToOne(optional=false, cascade = CascadeType.PERSIST)
	@JoinColumns({
			@JoinColumn( name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false),
			@JoinColumn( name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false),
			@JoinColumn( name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false),
			@JoinColumn( name = "NUCNFLIQ", referencedColumnName = "NUCNFLIQ", nullable = false)
	} )
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Tmct0alc alc;

	/**
	 * Tipo de estado
	 * 10 = CORRETAJE Y CANON
	 * 30 = COMISION DEVOLUCION
	 * 40 = COMISION ORDENANTE
	 */
	@Column(name = "TIPOESTADO", nullable = false)
	protected TIPO_ESTADO tipoEstado;

	/**
	 * Estado contable
	 */
	@ManyToOne(optional=false)
	@JoinColumn( name = "ESTADOCONT", referencedColumnName = "IDESTADO", nullable = false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	protected Tmct0estado estadoCont;

	public AlcEstadoCont() {
	}

	public AlcEstadoCont(Tmct0alc alc, TIPO_ESTADO tipoEstado) {
		this.alc = alc;
		this.tipoEstado = tipoEstado;
	}

	public Tmct0alc getAlc() {
		return alc;
	}

	public void setAlc( Tmct0alc alc ) {
		this.alc = alc;
	}

	public TIPO_ESTADO getTipoEstado() {
		return tipoEstado;
	}

	public void setTipoEstado( TIPO_ESTADO tipoEstado ) {
		this.tipoEstado = tipoEstado;
	}

	public Tmct0estado getEstadoCont() {
		return estadoCont;
	}

	public void setEstadoCont( Tmct0estado estadoCont ) {
		this.estadoCont = estadoCont;
	}
}
