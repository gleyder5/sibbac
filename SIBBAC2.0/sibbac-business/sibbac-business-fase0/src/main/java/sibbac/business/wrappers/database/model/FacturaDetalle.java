package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * 
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table( name = WRAPPERS.FACTURA_DETALLE )
@Entity
@XmlRootElement
@Audited
public class FacturaDetalle extends ATableAudited< FacturaDetalle > implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long				serialVersionUID		= -1015805480684162971L;

	/** The factura sugerida. */
	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_FACTURASUGERIDA", referencedColumnName = "ID", nullable = false )
	@XmlAttribute
	private FacturaSugerida					facturaSugerida;

	/** The numero. */
	@Column( name = "NUMERO", nullable = false )
	@XmlAttribute
	private Integer							numero;

	/** The concepto. */
	@Column( name = "CONCEPTO", length = 200 )
	@XmlAttribute
	private String							concepto;

	@OneToMany( mappedBy = "facturaDetalle", cascade = {
			CascadeType.PERSIST, CascadeType.REMOVE
	} )
	private List< AlcOrdenDetalleFactura >	alcOrdenesDetalleFactura = new ArrayList<AlcOrdenDetalleFactura>();

	public final static Integer				NUMERO_LINEA_FACTURA	= 1;

	// ----------------------------------------------- Bean Constructors

	/**
	 * Instantiates a new linea factura sugerida.
	 */
	public FacturaDetalle() {
		super();
		this.numero = NUMERO_LINEA_FACTURA;

	}

	@Transactional public List< AlcOrdenes > getAlcOrdenes() {
		final List< AlcOrdenes > ordenes = new ArrayList< AlcOrdenes >();
		for ( AlcOrdenDetalleFactura alcOrdenDetalleFactura : alcOrdenesDetalleFactura ) {
			if (alcOrdenDetalleFactura.getAlcOrden()==null) {
				LOG.warn("El AlcOrdenDetalleFactura.id: "+alcOrdenDetalleFactura.getId()+" tiene su alcOrden a nulo");
			} else {
				ordenes.add( alcOrdenDetalleFactura.getAlcOrden() );
			}
		}
		return ordenes;
	}

	public List<Tmct0alcId> getAlcIds() {
		final List<Tmct0alcId> alcs = new ArrayList<>();
		if ( CollectionUtils.isNotEmpty( alcOrdenesDetalleFactura ) ) {
			for ( AlcOrdenDetalleFactura alcOrdenDetalleFactura : alcOrdenesDetalleFactura ) {
				alcs.add( alcOrdenDetalleFactura.getAlcOrden().createAlcIdFromFields());
			}
		}
		return alcs;
	}

	/**
	 * Cuando está sumando imfinsvb, en verdad lo que suma son
	 * imcomisn
	 * @return the imfinsvb
	 */
	public BigDecimal getImfinsvb() {
		BigDecimal imfinsvb = BigDecimal.ZERO;
		if ( CollectionUtils.isNotEmpty( alcOrdenesDetalleFactura ) ) {
			for ( final AlcOrdenDetalleFactura alcOrdenDetalleFactura : alcOrdenesDetalleFactura ) {
				BigDecimal imfinsvbOrden = alcOrdenDetalleFactura.getImfinsvb();
				if ( imfinsvbOrden != null ) {
					imfinsvb = imfinsvb.add( imfinsvbOrden );
				}
			}
		}
		return imfinsvb;
	}

	/**
	 * @param importe sacado de 
	 * @param alcOrden
	 * @return
	 */
	public boolean addAlcOrden( final AlcOrdenes alcOrden, BigDecimal importe ) {
		if ( CollectionUtils.isEmpty( alcOrdenesDetalleFactura ) ) {
			alcOrdenesDetalleFactura = new ArrayList< AlcOrdenDetalleFactura >();
		}
		final AlcOrdenDetalleFactura alcOrdenDetalleFactura = new AlcOrdenDetalleFactura();
		alcOrdenDetalleFactura.setAlcOrden( alcOrden );
		alcOrdenDetalleFactura.setFacturaDetalle( this );
		/*en este campo hay que meter el imcomisn*/
		alcOrdenDetalleFactura.setImfinsvb( importe.setScale(4, RoundingMode.HALF_UP) );
		//alcOrdenDetalleFactura.setImfinsvb( alcOrden.getImfinsvb() );
		alcOrden.addFacturaDetalle( this );
		return alcOrdenesDetalleFactura.add( alcOrdenDetalleFactura );
	}

	/**
	 * Gets the factura sugerida.
	 *
	 * @return the factura sugerida
	 */
	public FacturaSugerida getFacturaSugerida() {
		return facturaSugerida;
	}

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	public Integer getNumero() {
		return numero;
	}

	/**
	 * Gets the concepto.
	 *
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * Sets the factura sugerida.
	 *
	 * @param facturaSugerida
	 *            the new factura sugerida
	 */
	public void setFacturaSugerida( FacturaSugerida facturaSugerida ) {
		this.facturaSugerida = facturaSugerida;
	}

	/**
	 * Sets the numero.
	 *
	 * @param numero
	 *            the new numero
	 */
	public void setNumero( Integer numero ) {
		this.numero = numero;
	}

	/**
	 * Sets the concepto.
	 *
	 * @param concepto
	 *            the new concepto
	 */
	public void setConcepto( String concepto ) {
		this.concepto = concepto;
	}

	/**
	 * @return the alcOrdenesDetalleFactura
	 */
	public List< AlcOrdenDetalleFactura > getAlcOrdenesDetalleFactura() {
		return alcOrdenesDetalleFactura;
	}

	/**
	 * @param alcOrdenesDetalleFactura
	 *            the alcOrdenesDetalleFactura to set
	 */
	public void setAlcOrdenesDetalleFactura( List< AlcOrdenDetalleFactura > alcOrdenesLineaDeFacturaSugerida ) {
		this.alcOrdenesDetalleFactura = alcOrdenesLineaDeFacturaSugerida;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( numero );
		return hcb.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		final FacturaDetalle ldfs = ( FacturaDetalle ) obj;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( this.getNumero(), ldfs.getNumero() );
		return eqb.isEquals();
	}

}
