package sibbac.business.fase0.database.dto;

import java.io.Serializable;
import java.util.Date;

import sibbac.pti.PTIMessageVersion;

public class PTIMessageVersionDateDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -5844173914635355757L;
  private PTIMessageVersion version;
  private Date init;
  private Date end;

  public PTIMessageVersionDateDTO() {
  }

  public PTIMessageVersion getVersion() {
    return version;
  }

  public void setVersion(PTIMessageVersion version) {
    this.version = version;
  }

  public Date getInit() {
    return init;
  }

  public void setInit(Date init) {
    this.init = init;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  @Override
  public String toString() {
    return "PTIMessageVersionDateDTO [version=" + version + ", init=" + init + ", end=" + end + "]";
  }

}
