package sibbac.business.canones.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.canones.common.CanonCalculadoEfectivoDTO;
import sibbac.business.canones.model.CanonAgrupadoRegla;

@Repository
public class CanonAgrupadoReglaDaoImpl {

  @Autowired
  private EntityManager em;

  public CanonAgrupadoRegla findByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta) {
    CanonAgrupadoRegla res = null;

    StringBuilder stringBuilderQuery = new StringBuilder(" select c from CanonAgrupadoRegla c ");

    Map<String, Object> mapParameters = new HashMap<String, Object>();
    List<String> listFilter = new ArrayList<String>();

    this.createFiltersAndParameters(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta, listFilter, mapParameters, true);

    stringBuilderQuery.append(this.createWhere(listFilter));

    Query queryHql = em.createQuery(stringBuilderQuery.toString(), CanonAgrupadoRegla.class);

    this.addParametersToQuery(mapParameters, queryHql);

    try {
      res = (CanonAgrupadoRegla) queryHql.getSingleResult();
    }
    catch (NoResultException e) {
      res = null;
    }
    return res;
  }

  public List<CanonAgrupadoRegla> findAllByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta) {
    List<CanonAgrupadoRegla> res = new ArrayList<CanonAgrupadoRegla>();

    StringBuilder stringBuilderQuery = new StringBuilder(" select c from CanonAgrupadoRegla c ");

    Map<String, Object> mapParameters = new HashMap<String, Object>();
    List<String> listFilter = new ArrayList<String>();

    this.createFiltersAndParameters(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta, listFilter, mapParameters, true);

    stringBuilderQuery.append(this.createWhere(listFilter));

    Query queryHql = em.createQuery(stringBuilderQuery.toString(), CanonAgrupadoRegla.class);

    this.addParametersToQuery(mapParameters, queryHql);
    try {
      List<?> result = (List<?>) queryHql.getResultList();
      for (Object object : result) {
        res.add((CanonAgrupadoRegla) object);
      }
    }
    catch (NoResultException e) {
    }
    return res;
  }

  public CanonCalculadoEfectivoDTO sumCanonAndEfectivoByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta) {
    CanonCalculadoEfectivoDTO res = new CanonCalculadoEfectivoDTO();
    res.setImcanon(BigDecimal.ZERO);
    res.setImefectivo(BigDecimal.ZERO);

    StringBuilder stringBuilderQuery = new StringBuilder(
        " select sum(c.importeEfectivo), sum(c.importeCanon) from CanonAgrupadoRegla c ");

    Map<String, Object> mapParameters = new HashMap<String, Object>();
    List<String> listFilter = new ArrayList<String>();

    this.createFiltersAndParameters(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta, listFilter, mapParameters, false);

    stringBuilderQuery.append(this.createWhere(listFilter));

    Query queryHql = em.createQuery(stringBuilderQuery.toString());

    this.addParametersToQuery(mapParameters, queryHql);
    try {
      Object[] resAux = (Object[]) queryHql.getSingleResult();
      if (resAux != null) {
        res.setImefectivo((BigDecimal) resAux[0]);
        res.setImcanon((BigDecimal) resAux[1]);
      }
    }
    catch (NoResultException e) {
    }
    return res;
  }

  public BigDecimal sumEfectivoByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta) {
    BigDecimal res = BigDecimal.ZERO;

    StringBuilder stringBuilderQuery = new StringBuilder(" select sum(c.importeEfectivo) from CanonAgrupadoRegla c ");

    Map<String, Object> mapParameters = new HashMap<String, Object>();
    List<String> listFilter = new ArrayList<String>();

    this.createFiltersAndParameters(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta, listFilter, mapParameters, false);

    stringBuilderQuery.append(this.createWhere(listFilter));

    Query queryHql = em.createQuery(stringBuilderQuery.toString());

    this.addParametersToQuery(mapParameters, queryHql);
    try {
      BigDecimal resAux = (BigDecimal) queryHql.getSingleResult();
      if (resAux != null) {
        res = resAux;
      }
    }
    catch (NoResultException e) {
      res = null;
    }
    return res;
  }

  public BigDecimal sumCanonByAllData(String tipoCanon, String tipoCalculo, Date fecha, String idMercadoContratacion,
      String titular, String isin, String tipoOperacionBolsa, Character sentido, BigDecimal precio,
      String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta) {
    BigDecimal res = BigDecimal.ZERO;

    StringBuilder stringBuilderQuery = new StringBuilder(" select sum(c.importeCanon) from CanonAgrupadoRegla c ");

    Map<String, Object> mapParameters = new HashMap<String, Object>();
    List<String> listFilter = new ArrayList<String>();

    this.createFiltersAndParameters(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta, listFilter, mapParameters, false);

    stringBuilderQuery.append(this.createWhere(listFilter));

    Query queryHql = em.createQuery(stringBuilderQuery.toString());

    this.addParametersToQuery(mapParameters, queryHql);
    try {
      BigDecimal resAux = (BigDecimal) queryHql.getSingleResult();
      if (resAux != null) {
        res = resAux;
      }
    }
    catch (NoResultException e) {
      res = null;
    }
    return res;
  }

  private String createWhere(List<String> listFilters) {
    StringBuilder sb = new StringBuilder(" where ");
    for (int i = 0; i < listFilters.size(); i++) {
      if (i == 0) {
        sb.append(" ").append(listFilters.get(i)).append(" ");
      }
      else {
        sb.append(" and ").append(listFilters.get(i)).append(" ");
      }
    }
    return sb.toString();

  }

  private void addParametersToQuery(Map<String, Object> parameters, Query query) {
    for (String param : parameters.keySet()) {
      if (parameters.get(param) != null)
        query.setParameter(param, parameters.get(param));
    }
  }

  private void createFiltersAndParameters(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta, List<String> listFilters,
      Map<String, Object> mapParameters, boolean addNullsFilter) {

    if (fecha == null) {
      if (addNullsFilter)
        listFilters.add("c.fechaContratacion is null");
    }
    else {
      listFilters.add("c.fechaContratacion = :fechaContratacion");
      mapParameters.put("fechaContratacion", fecha);
    }

    if (idMercadoContratacion == null) {
      if (addNullsFilter)
        listFilters.add("c.mercadoContratacion is null");
    }
    else {
      listFilters.add("c.mercadoContratacion = :mercadoContratacion");
      mapParameters.put("mercadoContratacion", idMercadoContratacion);
    }
    
    if (miembroMercado == null) {
      if (addNullsFilter)
        listFilters.add("c.miembroMercado is null");
    }
    else {
      listFilters.add("c.miembroMercado = :miembroMercado");
      mapParameters.put("miembroMercado", miembroMercado);
    }

    if (titular == null) {
      if (addNullsFilter)
        listFilters.add("c.titular is null");
    }
    else {
      listFilters.add("c.titular = :titular");
      mapParameters.put("titular", titular);
    }

    if (isin == null) {
      if (addNullsFilter)
        listFilters.add("c.isin is null");
    }
    else {
      listFilters.add("c.isin = :isin");
      mapParameters.put("isin", isin);
    }

    if (tipoOperacionBolsa == null) {
      if (addNullsFilter)
        listFilters.add("c.tipoOperacionBolsa is null");
    }
    else {
      listFilters.add("c.tipoOperacionBolsa = :tipoOperacionBolsa");
      mapParameters.put("tipoOperacionBolsa", tipoOperacionBolsa);
    }

    if (sentido == null) {
      if (addNullsFilter)
        listFilters.add("c.sentido is null");
    }
    else {
      listFilters.add("c.sentido = :sentido");
      mapParameters.put("sentido", sentido);
    }

    if (precio == null) {
      if (addNullsFilter)
        listFilters.add("c.precio is null");
    }
    else {
      listFilters.add("c.precio = :precio");
      mapParameters.put("precio", precio);
    }

    

    if (ordenMercado == null) {
      if (addNullsFilter)
        listFilters.add("c.ordenMercado is null");
    }
    else {
      listFilters.add("c.ordenMercado = :ordenMercado");
      mapParameters.put("ordenMercado", ordenMercado);
    }

    if (ejecucionMercado == null) {
      if (addNullsFilter)
        listFilters.add("c.ejecucionMercado is null");
    }
    else {
      listFilters.add("c.ejecucionMercado = :ejecucionMercado");
      mapParameters.put("ejecucionMercado", ejecucionMercado);
    }

    if (tipoSubasta == null) {
      if (addNullsFilter)
        listFilters.add("c.tipoSubasta is null");
    }
    else {
      listFilters.add("c.tipoSubasta = :tipoSubasta");
      mapParameters.put("tipoSubasta", tipoSubasta);
    }

    if (idSegmentoMercadoContratacion == null) {
      if (addNullsFilter)
        listFilters.add("c.segmentoMercadoContratacion is null");
    }
    else {
      listFilters.add("c.segmentoMercadoContratacion = :segmentoMercadoContratacion");
      mapParameters.put("segmentoMercadoContratacion", idSegmentoMercadoContratacion);
    }

    listFilters.add("c.ordenPuntoMedio = :ordenPuntoMedio");
    mapParameters.put("ordenPuntoMedio", ordenPuntoMedio);

    listFilters.add("c.ordenOculta = :ordenOculta");
    mapParameters.put("ordenOculta", ordenOculta);

    listFilters.add("c.ordenBloqueCombinado = :ordenBloqueCombinado");
    mapParameters.put("ordenBloqueCombinado", ordenBloqueCombinado);

    listFilters.add("c.ordenVolumenOculto = :ordenVolumenOculto");
    mapParameters.put("ordenVolumenOculto", ordenVolumenOculto);

    listFilters.add("c.restriccionOrden = :restriccionOrden");
    mapParameters.put("restriccionOrden", restriccionOrden);

    listFilters.add("c.ordenEjecutadaSubasta = :ordenEjecutadaSubasta");
    mapParameters.put("ordenEjecutadaSubasta", ordenEjecutadaSubasta);

    if (tipoCanon != null) {
      listFilters.add("c.tipoCanon = :tipoCanon");
      mapParameters.put("tipoCanon", tipoCanon);
    }
    if (tipoCalculo != null) {
      listFilters.add("c.tipoCalculo = :tipoCalculo");
      mapParameters.put("tipoCalculo", tipoCalculo);
    }
  }
}
