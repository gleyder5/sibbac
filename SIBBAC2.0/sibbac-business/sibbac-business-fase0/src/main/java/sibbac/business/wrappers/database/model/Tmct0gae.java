package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0GAE. EGA Executions Documentación para TMCT0GAE <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0GAE")
public class Tmct0gae implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -8790758497861883654L;
//@formatter:off
   @EmbeddedId
   @AttributeOverrides({ @AttributeOverride(name = "nuorden",  column = @Column(name = "NUORDEN", nullable = false, length = 20)), 
                         @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)), 
                         @AttributeOverride(name = "nuagreje", column = @Column(name = "NUAGREJE", nullable = false, length = 20)), 
                         @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
                         @AttributeOverride(name = "nuejecuc", column = @Column(name = "NUEJECUC", nullable = false, length = 20))
   })
  private Tmct0gaeId id;
    /**
    * Relation 1..1 with table TMCT0GAL. Constraint de relación entre TMCT0GAE y TMCT0GAL <!-- begin-user-doc --> <!--
    * end-user-doc -->
    * 
    **/
   @ManyToOne(optional = false, fetch = FetchType.LAZY)
   @JoinColumns(value = { @JoinColumn(name = "NUORDEN",  referencedColumnName = "NUORDEN",  nullable = false, insertable = false, updatable = false), 
                        @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
                        @JoinColumn(name = "NUAGREJE", referencedColumnName = "NUAGREJE", nullable = false, insertable = false, updatable = false),
                        @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false)
   })
  protected Tmct0gal tmct0gal;
 //@formatter:on
  /**
   * Table Field NUTITASIG. Assigned volume Documentación para NUTITASIG <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "", length = 16, scale = 5)
  protected java.math.BigDecimal nutitasig = BigDecimal.ZERO;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;

  public Tmct0gaeId getId() {
    return id;
  }

  public Tmct0gal getTmct0gal() {
    return tmct0gal;
  }

  public java.math.BigDecimal getNutitasig() {
    return nutitasig;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public void setId(Tmct0gaeId id) {
    this.id = id;
  }

  public void setTmct0gal(Tmct0gal tmct0gal) {
    this.tmct0gal = tmct0gal;
  }

  public void setNutitasig(java.math.BigDecimal nutitasig) {
    this.nutitasig = nutitasig;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Override
  public String toString() {
    return "Tmct0gae [id=" + id + ", nutitasig=" + nutitasig + "]";
  }

}
