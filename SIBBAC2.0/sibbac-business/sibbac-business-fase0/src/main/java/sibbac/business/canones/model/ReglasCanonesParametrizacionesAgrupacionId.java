package sibbac.business.canones.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReglasCanonesParametrizacionesAgrupacionId implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 8347581135846818938L;
  @Column(name = "ID_REGLA_CANON_PARAMETRIZACION", nullable = false)
  private BigInteger idReglaCanonParametrizacion;
  @Column(name = "ID_CRITERIO_AGRUPACION", nullable = false)
  private String idCriterioAgrupacion;

  public BigInteger getIdReglaCanonParametrizacion() {
    return idReglaCanonParametrizacion;
  }

  public String getIdCriterioAgrupacion() {
    return idCriterioAgrupacion;
  }

  public void setIdReglaCanonParametrizacion(BigInteger idReglaCanonParametrizacion) {
    this.idReglaCanonParametrizacion = idReglaCanonParametrizacion;
  }

  public void setIdCriterioAgrupacion(String idCriterioAgrupacion) {
    this.idCriterioAgrupacion = idCriterioAgrupacion;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((idCriterioAgrupacion == null) ? 0 : idCriterioAgrupacion.hashCode());
    result = prime * result + ((idReglaCanonParametrizacion == null) ? 0 : idReglaCanonParametrizacion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ReglasCanonesParametrizacionesAgrupacionId other = (ReglasCanonesParametrizacionesAgrupacionId) obj;
    if (idCriterioAgrupacion == null) {
      if (other.idCriterioAgrupacion != null)
        return false;
    }
    else if (!idCriterioAgrupacion.equals(other.idCriterioAgrupacion))
      return false;
    if (idReglaCanonParametrizacion == null) {
      if (other.idReglaCanonParametrizacion != null)
        return false;
    }
    else if (!idReglaCanonParametrizacion.equals(other.idReglaCanonParametrizacion))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format(
        "ReglasCanonesParametrizacionesAgrupacionId [idReglaCanonParametrizacion=%s, idCriterioAgrupacion=%s]",
        idReglaCanonParametrizacion, idCriterioAgrupacion);
  }

}
