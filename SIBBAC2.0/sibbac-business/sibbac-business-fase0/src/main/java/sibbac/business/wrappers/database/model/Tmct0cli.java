package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "TMCT0CLI")
public class Tmct0cli implements java.io.Serializable {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = 7746121657244783408L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDCLIENT", length = 8, nullable = false)
  private Long idClient;
  private String descli;
  private Character tpidenti;
  private String cif;
  private String cdbdp;
  private String tpreside;
  private String cdrefbnk;
  private Character tpclienn;
  private String cdkgr;
  private String cdcat;
  private Character fidessa;
  private Integer fhsendfi;
  private String cdusuaud;
  private Date fhaudit;
  private Integer fhinicio;
  private Integer fhfinal;
  private String cdotheid;
  private String cdbrocli;
  private BigDecimal numsec;

  private Character tpfisjur;
  private Character cdestado;
  private Character tpNacTit;
  private String cdPais;
  private String cdNacTit;
  private String motiPrev;
  private Character cddeprev;
  private String tpManCat;
  private String ctFiscal;

  @Column(name = "RIESGO_PBC", nullable = true, length = 25)
  private String riesgoPbc;

  @XmlAttribute
  @Temporal(TemporalType.DATE)
  private Date fhNacimi;

  @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<Contacto> contactos = new ArrayList<Contacto>();

  @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<Tmct0CliMifid> mifid;


  @OneToOne(mappedBy = "cliente", cascade = CascadeType.ALL)
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0Address address;

  @XmlAttribute
  @Temporal(TemporalType.DATE)
  @Column(name = "FH_REV_RIESGO_PBC", nullable = true, precision = 8)
  private Date fhRevRiesgoPbc;

  @XmlAttribute
  @Temporal(TemporalType.DATE)
  @Column(name = "FH_REV_PREV", nullable = true, length = 10)
  private Date fhRevPrev;

  public String getDireccionPrincipal() {
    if (address != null) {
      return address.getDescripcion();
    }
    return null;
  }

  public Tmct0cli() {
  }

  public Tmct0cli(Long idClient, String descli, Character tpidenti, String cif, String cdbdp, String tpreside,
      String cdrefbnk, Character tpclienn, String cdkgr, String cdcat, Character fidessa, Integer fhsendfi,
      String cdusuaud, Date fhaudit, Integer fhinicio, Integer fhfinal, String cdotheid, String cdbrocli,
      BigDecimal numsec, Character tpfisjur, Character cdestado, Character tpNacTit, String cdPais, String cdNacTit,
      String motiPrev, Character cddeprev, String tpManCat, String ctFiscal, Date fhNacimi, String riesgoPbc,
      Date fhRevPrev) {
    this.idClient = idClient;
    this.descli = descli;
    this.tpidenti = tpidenti;
    this.cif = cif;
    this.cdbdp = cdbdp;
    this.tpreside = tpreside;
    this.cdrefbnk = cdrefbnk;
    this.tpclienn = tpclienn;
    this.cdkgr = cdkgr;
    this.cdcat = cdcat;
    this.fidessa = fidessa;
    this.fhsendfi = fhsendfi;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.fhinicio = fhinicio;
    this.fhfinal = fhfinal;
    this.cdotheid = cdotheid;
    this.cdbrocli = cdbrocli;
    this.numsec = numsec;
    this.tpfisjur = tpfisjur;
    this.cdestado = cdestado;
    this.tpNacTit = tpNacTit;
    this.cdPais = cdPais;
    this.cdNacTit = cdNacTit;
    this.motiPrev = motiPrev;
    this.cddeprev = cddeprev;
    this.tpManCat = tpManCat;
    this.ctFiscal = ctFiscal;
    this.fhNacimi = fhNacimi;
    this.riesgoPbc = riesgoPbc;
    this.fhRevPrev = fhRevPrev;
  }

  public Date getFhRevRiesgoPbc() {
    return fhRevRiesgoPbc;
  }

  public void setFhRevRiesgoPbc(Date fhRevRiesgoPbc) {
    this.fhRevRiesgoPbc = fhRevRiesgoPbc;
  }

  public Set<Tmct0Address> getListadoDireccionesRelacionadas() {
    Set<Tmct0Address> direcciones = new HashSet<Tmct0Address>();
    if (getAddress() != null) {
      direcciones.add(getAddress());
    }
    if (contactos != null && !contactos.isEmpty()) {
      for (Contacto contacto : contactos) {
        if (contacto.getAddress() != null) {
          direcciones.add(contacto.getAddress());
        }
      }
    }
    return direcciones;
  }

  public Long getId() {
    return idClient;
  }

  public Long getIdCliente() {
    return idClient;
  }

  @Column(name = "CDBROCLI", nullable = false, length = 20)
  public String getCdbrocli() {
    if (cdbrocli != null)
      cdbrocli = cdbrocli.trim();
    return this.cdbrocli;
  }

  public void setCdbrocli(String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  @Column(name = "NUMSEC", nullable = false, precision = 20)
  public BigDecimal getNumsec() {
    return this.numsec;
  }

  public void setNumsec(BigDecimal numsec) {
    this.numsec = numsec;
  }

  @Column(name = "DESCLI", nullable = false, length = 130)
  public String getDescli() {
    if (descli != null)
      descli = descli.trim();
    return this.descli;
  }

  public void setDescli(String descli) {
    this.descli = descli;
  }

  @Column(name = "TPIDENTI", nullable = false, length = 1)
  public Character getTpidenti() {
    return this.tpidenti;
  }

  public void setTpidenti(Character tpidenti) {
    this.tpidenti = tpidenti;
  }

  @Column(name = "CIF", nullable = false, length = 11)
  public String getCif() {
    if (cif != null)
      cif = cif.trim();
    return this.cif;
  }

  public void setCif(String cif) {
    this.cif = cif;
  }

  @Column(name = "CDBDP", nullable = false, length = 130)
  public String getCdbdp() {
    if (cdbdp != null)
      cdbdp = cdbdp.trim();
    return this.cdbdp;
  }

  public void setCdbdp(String cdbdp) {
    this.cdbdp = cdbdp;
  }

  @Column(name = "TPRESIDE", nullable = false, length = 2)
  public String getTpreside() {
    if (StringUtils.isNotEmpty(this.tpreside)) {
      return this.tpreside.trim();
    }
    return this.tpreside;
  }

  public void setTpreside(String tpreside) {
    this.tpreside = tpreside;
  }

  @Column(name = "CDREFBNK", nullable = false, length = 130)
  public String getCdrefbnk() {
    if (cdrefbnk != null)
      cdrefbnk = cdrefbnk.trim();
    return this.cdrefbnk;
  }

  public void setCdrefbnk(String cdrefbnk) {
    this.cdrefbnk = cdrefbnk;
  }

  @Column(name = "TPCLIENN", nullable = false, length = 1)
  public Character getTpclienn() {
    return this.tpclienn;
  }

  public void setTpclienn(Character tpclienn) {
    this.tpclienn = tpclienn;
  }

  @Column(name = "CDKGR", nullable = false, length = 25)
  public String getCdkgr() {
    if (cdkgr != null)
      cdkgr = cdkgr.trim();
    return this.cdkgr;
  }

  public void setCdkgr(String cdkgr) {
    this.cdkgr = cdkgr;
  }

  @Column(name = "CDCAT", nullable = false, length = 10)
  public String getCdcat() {
    if (cdcat != null)
      cdcat = cdcat.trim();
    return this.cdcat;
  }

  public void setCdcat(String cdcat) {
    this.cdcat = cdcat;
  }

  @Column(name = "FIDESSA", nullable = false, length = 1)
  public Character getFidessa() {
    return this.fidessa;
  }

  public void setFidessa(Character fidessa) {
    this.fidessa = fidessa;
  }

  @Column(name = "FHSENDFI", nullable = false, precision = 8)
  public Integer getFhsendfi() {
    return this.fhsendfi;
  }

  public void setFhsendfi(Integer fhsendfi) {
    this.fhsendfi = fhsendfi;
  }

  @Column(name = "CDUSUAUD", nullable = false, length = 10)
  public String getCdusuaud() {
    if (cdusuaud != null)
      cdusuaud = cdusuaud.trim();
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", nullable = false, length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "FHINICIO", nullable = false, precision = 8)
  public Integer getFhinicio() {
    return this.fhinicio;
  }

  public void setFhinicio(Integer fhinicio) {
    this.fhinicio = fhinicio;
  }

  @Column(name = "FHFINAL", precision = 8)
  public Integer getFhfinal() {
    return this.fhfinal;
  }

  public void setFhfinal(Integer fhfinal) {
    this.fhfinal = fhfinal;
  }

  @Column(name = "CDOTHEID", nullable = false, length = 20)
  public String getCdotheid() {
    if (cdotheid != null)
      cdotheid = cdotheid.trim();
    return this.cdotheid;
  }

  public void setCdotheid(String cdotheid) {
    this.cdotheid = cdotheid;
  }

  @Column(name = "TPMANCAT", nullable = false, length = 50)
  public String getTpManCat() {
    if (tpManCat != null)
      tpManCat = tpManCat.trim();
    return this.tpManCat;
  }

  public void setTpManCat(String tpManCat) {
    this.tpManCat = tpManCat;
  }

  @Column(name = "CTFISCAL", nullable = false, length = 50)
  public String getCtFiscal() {
    if (ctFiscal != null)
      ctFiscal = ctFiscal.trim();
    return this.ctFiscal;
  }

  public void setCtFiscal(String ctFiscal) {
    this.ctFiscal = ctFiscal;
  }

  @Column(name = "FHNACIMI", nullable = true, precision = 8)
  public Date getFhNacimi() {
    return this.fhNacimi;
  }

  public void setFhNacimi(Date fhNacimi) {
    this.fhNacimi = fhNacimi;
  }

  /**
   * @return the mifid
   */

  public List<Tmct0CliMifid> getMifid() {
    return mifid;
  }

  /**
   * @param mifid the mifid to set
   */
  public void setMifid(List<Tmct0CliMifid> mifid) {
    this.mifid = mifid;
  }

  public List<Contacto> getContactos() {
    return contactos;
  }

  public void setContactos(List<Contacto> contactos) {
    this.contactos = contactos;
  }

  /**
   * @return the address
   */

  public Tmct0Address getAddress() {
    return address;
  }

  /**
   * @param address the address to set
   */
  public void setAddress(Tmct0Address address) {
    this.address = address;
  }

  /**
   * @return the idClient
   */
  public Long getIdClient() {
    return idClient;
  }

  /**
   * @param idClient the idClient to set
   */
  public void setIdClient(Long idClient) {
    this.idClient = idClient;
  }

  /**
   * @return the tpfisjur
   */
  @Column(name = "TPFISJUR", nullable = false, length = 1)
  public Character getTpfisjur() {
    return tpfisjur;
  }

  /**
   * @param tpfisjur the tpfisjur to set
   */
  public void setTpfisjur(Character tpfisjur) {
    this.tpfisjur = tpfisjur;
  }

  /**
   * @return the cdestado
   */
  @Column(name = "CDESTADO", nullable = false, length = 1)
  public Character getCdestado() {
    return cdestado;
  }

  /**
   * @param cdestado the cdestado to set
   */
  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  /**
   * @return the tpNacTit
   */
  @Column(name = "TPNACTIT", nullable = false, length = 1)
  public Character getTpNacTit() {
    return tpNacTit;
  }

  /**
   * @param tpNacTit the tpNacTit to set
   */
  public void setTpNacTit(Character tpNacTit) {
    this.tpNacTit = tpNacTit;
  }

  /**
   * @return the cdPais
   */
  @Column(name = "CDPAIS", nullable = false, length = 2)
  public String getCdPais() {
    if (cdPais != null)
      cdPais = cdPais.trim();
    return cdPais;
  }

  /**
   * @param cdPais the cdPais to set
   */
  public void setCdPais(String cdPais) {
    this.cdPais = cdPais;
  }

  /**
   * @return the cdNacTit
   */
  @Column(name = "CDNACTIT", nullable = false, length = 3)
  public String getCdNacTit() {
    if (cdNacTit != null)
      cdNacTit = cdNacTit.trim();
    return cdNacTit;
  }

  /**
   * @param cdNacTit the cdNacTit to set
   */
  public void setCdNacTit(String cdNacTit) {
    this.cdNacTit = cdNacTit;
  }

  /**
   * @return the motiPrev
   */
  @Column(name = "MOTIPREV", nullable = false, length = 10)
  public String getMotiPrev() {
    if (motiPrev != null)
      motiPrev = motiPrev.trim();
    return motiPrev;
  }

  /**
   * @param motiPrev the motiPrev to set
   */

  public void setMotiPrev(String motiPrev) {
    this.motiPrev = motiPrev;
  }

  /**
   * @return the controlPrev
   */
  @Column(name = "CDDEPREV", nullable = false, length = 1)
  public Character getCddeprev() {
    return cddeprev;
  }

  /**
   * @param controlPrev the controlPrev to set
   */
  public void setCddeprev(Character controlPrev) {
    this.cddeprev = controlPrev;
  }

  public String getRiesgoPbc() {
    return riesgoPbc;
  }

  public void setRiesgoPbc(String riesgoPbc) {
    this.riesgoPbc = riesgoPbc;
  }

  @Transient
  public boolean isEspanol() {
    return 'N' == tpNacTit;
  }

  @Column(name = "FH_REV_PREV", nullable = true, precision = 8)
  public Date getFhRevPrev() {
    return this.fhRevPrev;
  }

  public void setFhRevPrev(Date fhRevPrev) {
    this.fhRevPrev = fhRevPrev;
  }
}