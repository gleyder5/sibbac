package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;


@Entity
@Table( name = DBConstants.WRAPPERS.TIPO_CUENTA_CONCILIACION )
@XmlRootElement
public class TipoCuentaConciliacion implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	@Column( name = "ID" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private long				id;
	@Column( name = "NAME" )
	private String				name;

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

}
