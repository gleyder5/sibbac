package sibbac.business.fase0.database.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0_MENSAJES")
public class Tmct0Mensajes implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -5162094326961962962L;
  private Long idMensaje;
  private Date horaRecepcion;
  private List<Tmct0MensajesR00> tmct0MensajesR00s = new ArrayList<Tmct0MensajesR00>(0);

  public Tmct0Mensajes() {
  }

  public Tmct0Mensajes(Long idMensaje, Date horaRecepcion) {
    this.idMensaje = idMensaje;
    this.horaRecepcion = horaRecepcion;
  }

  public Tmct0Mensajes(Long idMensaje, Date horaRecepcion, List<Tmct0MensajesR00> tmct0MensajesR00s) {
    this.idMensaje = idMensaje;
    this.horaRecepcion = horaRecepcion;
    this.tmct0MensajesR00s = tmct0MensajesR00s;
  }

  @Id
  @Column(name = "ID_MENSAJE", unique = true, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getIdMensaje() {
    return this.idMensaje;
  }

  public void setIdMensaje(Long idMensaje) {
    this.idMensaje = idMensaje;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "HORA_RECEPCION", nullable = false, length = 26)
  public Date getHoraRecepcion() {
    return this.horaRecepcion;
  }

  public void setHoraRecepcion(Date horaRecepcion) {
    this.horaRecepcion = horaRecepcion;
  }

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "tmct0Mensajes", cascade = CascadeType.ALL)
  public List<Tmct0MensajesR00> getTmct0MensajesR00s() {
    return this.tmct0MensajesR00s;
  }

  public void setTmct0MensajesR00s(List<Tmct0MensajesR00> tmct0MensajesR00s) {
    this.tmct0MensajesR00s = tmct0MensajesR00s;
  }

  @Transient
  public String getFullText() {
    if (this.tmct0MensajesR00s != null && !this.tmct0MensajesR00s.isEmpty()) {
      StringBuffer sb = new StringBuffer();
      String texto = null;
      for (Tmct0MensajesR00 r00 : this.tmct0MensajesR00s) {
        texto = r00.getTexto();
        if (texto != null && texto.trim().length() > 0) {
          sb.append(texto.trim() + " ");
        }
      }
      return sb.toString().trim();
    }
    else {
      return null;
    }
  }

  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer("Message:");
    sb.append(" [id==" + this.idMensaje + "]");
    sb.append(" [hora==" + this.horaRecepcion + "]");
    if (this.tmct0MensajesR00s != null && !this.tmct0MensajesR00s.isEmpty()) {
      for (Tmct0MensajesR00 r00 : this.tmct0MensajesR00s) {
        sb.append(" [" + r00.getTexto() + "]");
      }
    }
    return sb.toString();
  }

}
