package sibbac.business.fase0.database.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0EST")
public class Tmct0est implements java.io.Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private Tmct0estId id;
  private String cdsaleid;
  private String cdmanage;
  private String stgroup;
  private String stlevel0;
  private String stlevel1;
  private String stlevel2;
  private String stlevel3;
  private String stlevel4;
  private String cdsalelm;
  private String cdmanalm;
  private String stgroulm;
  private String stlev0lm;
  private String stlev1lm;
  private String stlev2lm;
  private String stlev3lm;
  private String stlev4lm;
  private String cdotheid;
  private String cdkgr;
  private String idaux;
  private String cdusuaud;
  private Timestamp fhaudit;
  private BigDecimal fhinicio;
  private BigDecimal fhfinal;

  public Tmct0est() {
  }

  public Tmct0est(Tmct0estId id, String cdsaleid, String cdmanage, String stgroup, String stlevel0, String stlevel1,
      String stlevel2, String stlevel3, String stlevel4, String cdsalelm, String cdmanalm, String stgroulm,
      String stlev0lm, String stlev1lm, String stlev2lm, String stlev3lm, String stlev4lm, String cdotheid,
      String cdkgr, String idaux, String cdusuaud, Timestamp fhaudit, BigDecimal fhinicio, BigDecimal fhfinal) {
    super();
    this.id = id;
    this.cdsaleid = cdsaleid;
    this.cdmanage = cdmanage;
    this.stgroup = stgroup;
    this.stlevel0 = stlevel0;
    this.stlevel1 = stlevel1;
    this.stlevel2 = stlevel2;
    this.stlevel3 = stlevel3;
    this.stlevel4 = stlevel4;
    this.cdsalelm = cdsalelm;
    this.cdmanalm = cdmanalm;
    this.stgroulm = stgroulm;
    this.stlev0lm = stlev0lm;
    this.stlev1lm = stlev1lm;
    this.stlev2lm = stlev2lm;
    this.stlev3lm = stlev3lm;
    this.stlev4lm = stlev4lm;
    this.cdotheid = cdotheid;
    this.cdkgr = cdkgr;
    this.idaux = idaux;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.fhinicio = fhinicio;
    this.fhfinal = fhfinal;
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "cdbrocli", column = @Column(name = "CDBROCLI", nullable = false, length = 20)),
      @AttributeOverride(name = "cdaliass", column = @Column(name = "CDALIASS", nullable = false, length = 20)),
      @AttributeOverride(name = "numsec", column = @Column(name = "NUMSEC", nullable = false, precision = 20)),
      @AttributeOverride(name = "cdsubcta", column = @Column(name = "CDSUBCTA", nullable = false, length = 20)) })
  public Tmct0estId getId() {
    return id;
  }

  public void setId(Tmct0estId id) {
    this.id = id;
  }

  @Transient
  public String getCdbrocli() {
    String cdbrocli = null;
    final Tmct0estId id = this.getId();
    if (id != null) {
      cdbrocli = id.getCdbrocli();
    }
    return cdbrocli;
  }

  @Transient
  public String getCdaliass() {
    String cdaliass = null;
    final Tmct0estId id = this.getId();
    if (id != null) {
      cdaliass = id.getCdaliass();
    }
    return cdaliass;
  }

  @Transient
  public BigDecimal getNumsec() {
    BigDecimal numsec = null;
    final Tmct0estId id = this.getId();
    if (id != null) {
      numsec = id.getNumsec();
    }
    return numsec;
  }

  @Transient
  public String getCdsubcta() {
    String cdsubcta = null;
    final Tmct0estId id = this.getId();
    if (id != null) {
      cdsubcta = id.getCdaliass();
    }
    return cdsubcta;
  }

  @Column(name = "CDSALEID", nullable = false, length = 8)
  public String getCdsaleid() {
    return cdsaleid;
  }

  public void setCdsaleid(String cdsaleid) {
    this.cdsaleid = cdsaleid;
  }

  @Column(name = "CDMANAGE", nullable = false, length = 8)
  public String getCdmanage() {
    return cdmanage;
  }

  public void setCdmanage(String cdmanage) {
    this.cdmanage = cdmanage;
  }

  @Column(name = "STGROUP", nullable = false, length = 3)
  public String getStgroup() {
    return stgroup;
  }

  public void setStgroup(String stgroup) {
    this.stgroup = stgroup;
  }

  @Column(name = "STLEVEL0", nullable = false, length = 5)
  public String getStlevel0() {
    return stlevel0;
  }

  public void setStlevel0(String stlevel0) {
    this.stlevel0 = stlevel0;
  }

  @Column(name = "STLEVEL1", nullable = false, length = 5)
  public String getStlevel1() {
    return stlevel1;
  }

  public void setStlevel1(String stlevel1) {
    this.stlevel1 = stlevel1;
  }

  @Column(name = "STLEVEL2", nullable = false, length = 5)
  public String getStlevel2() {
    return stlevel2;
  }

  public void setStlevel2(String stlevel2) {
    this.stlevel2 = stlevel2;
  }

  @Column(name = "STLEVEL3", nullable = false, length = 5)
  public String getStlevel3() {
    return stlevel3;
  }

  public void setStlevel3(String stlevel3) {
    this.stlevel3 = stlevel3;
  }

  @Column(name = "STLEVEL4", nullable = false, length = 5)
  public String getStlevel4() {
    return stlevel4;
  }

  public void setStlevel4(String stlevel4) {
    this.stlevel4 = stlevel4;
  }

  @Column(name = "CDSALELM", nullable = false, length = 8)
  public String getCdsalelm() {
    return cdsalelm;
  }

  public void setCdsalelm(String cdsalelm) {
    this.cdsalelm = cdsalelm;
  }

  @Column(name = "CDMANALM", nullable = false, length = 8)
  public String getCdmanalm() {
    return cdmanalm;
  }

  public void setCdmanalm(String cdmanalm) {
    this.cdmanalm = cdmanalm;
  }

  @Column(name = "STGROULM", nullable = false, length = 3)
  public String getStgroulm() {
    return stgroulm;
  }

  public void setStgroulm(String stgroulm) {
    this.stgroulm = stgroulm;
  }

  @Column(name = "STLEV0LM", nullable = false, length = 5)
  public String getStlev0lm() {
    return stlev0lm;
  }

  public void setStlev0lm(String stlev0lm) {
    this.stlev0lm = stlev0lm;
  }

  @Column(name = "STLEV1LM", nullable = false, length = 5)
  public String getStlev1lm() {
    return stlev1lm;
  }

  public void setStlev1lm(String stlev1lm) {
    this.stlev1lm = stlev1lm;
  }

  @Column(name = "STLEV2LM", nullable = false, length = 5)
  public String getStlev2lm() {
    return stlev2lm;
  }

  public void setStlev2lm(String stlev2lm) {
    this.stlev2lm = stlev2lm;
  }

  @Column(name = "STLEV3LM", nullable = false, length = 5)
  public String getStlev3lm() {
    return stlev3lm;
  }

  public void setStlev3lm(String stlev3lm) {
    this.stlev3lm = stlev3lm;
  }

  @Column(name = "STLEV4LM", nullable = false, length = 5)
  public String getStlev4lm() {
    return stlev4lm;
  }

  public void setStlev4lm(String stlev4lm) {
    this.stlev4lm = stlev4lm;
  }

  @Column(name = "CDOTHEID", nullable = false, length = 20)
  public String getCdotheid() {
    return cdotheid;
  }

  public void setCdotheid(String cdotheid) {
    this.cdotheid = cdotheid;
  }

  @Column(name = "CDKGR", nullable = false, length = 25)
  public String getCdkgr() {
    return cdkgr;
  }

  public void setCdkgr(String cdkgr) {
    this.cdkgr = cdkgr;
  }

  @Column(name = "IDAUX", nullable = false, length = 25)
  public String getIdaux() {
    return idaux;
  }

  public void setIdaux(String idaux) {
    this.idaux = idaux;
  }

  @Column(name = "CDUSUAUD", nullable = false, length = 10)
  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Column(name = "FHAUDIT", nullable = false, length = 26)
  public Timestamp getFhaudit() {
    return fhaudit;
  }

  public void setFhaudit(Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "FHINICIO", nullable = false, precision = 8)
  public BigDecimal getFhinicio() {
    return fhinicio;
  }

  public void setFhinicio(BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  @Column(name = "FHFINAL", nullable = false, precision = 8)
  public BigDecimal getFhfinal() {
    return fhfinal;
  }

  public void setFhfinal(BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  @Override
  public String toString() {
    return "Tmct0est [id=" + id + ", cdsaleid=" + cdsaleid + ", cdmanage=" + cdmanage + ", stgroup=" + stgroup
        + ", stlevel0=" + stlevel0 + ", stlevel1=" + stlevel1 + ", stlevel2=" + stlevel2 + ", stlevel3=" + stlevel3
        + ", stlevel4=" + stlevel4 + ", cdsalelm=" + cdsalelm + ", cdmanalm=" + cdmanalm + ", stgroulm=" + stgroulm
        + ", stlev0lm=" + stlev0lm + ", stlev1lm=" + stlev1lm + ", stlev2lm=" + stlev2lm + ", stlev3lm=" + stlev3lm
        + ", stlev4lm=" + stlev4lm + ", cdotheid=" + cdotheid + ", cdkgr=" + cdkgr + ", idaux=" + idaux + ", cdusuaud="
        + cdusuaud + ", fhaudit=" + fhaudit + ", fhinicio=" + fhinicio + ", fhfinal=" + fhfinal + "]";
  }

}
