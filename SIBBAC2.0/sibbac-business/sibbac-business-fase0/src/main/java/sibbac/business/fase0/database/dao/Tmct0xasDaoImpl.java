package sibbac.business.fase0.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.common.DTOUtils;
import sibbac.business.fase0.database.dto.XasDTO;

@Repository
public class Tmct0xasDaoImpl {
	
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0xasDaoImpl.class);
	
	@PersistenceContext
	private EntityManager	em;
	
	private void setParametersWithVectors( Query query, String sBolsaMercado, String fechaBolsaMercadoParamName ) {
		if ( sBolsaMercado != null ) {
			query.setParameter( fechaBolsaMercadoParamName, sBolsaMercado );
		}
	}
	
	private List< XasDTO > resultadosConsulta (Query query)
	{
		List<Object> resultList = null;
		try{
		    resultList = query.getResultList();
		}catch(Exception ex){
			LOG.error("Error resultados consulta: ", ex.getClass().getName(), ex.getMessage());
		}

		final List< XasDTO > bolsaMercadoList = new ArrayList< XasDTO >();
		if ( CollectionUtils.isNotEmpty( resultList ) ) {
			for ( Object obj : resultList ) {
				bolsaMercadoList.add(DTOUtils.convertObjectToXasDTO( ( Object[] ) obj ));
			}//for ( Object obj : resultList ) {
		}//if ( CollectionUtils.isNotEmpty( resultList ) ) {
		return bolsaMercadoList;
	}//private List< XasDTO > resultadosConsulta (Query query)
	
	private Query consultaBolsasMercado (String sBolsaMercado)
	{
		
		final StringBuilder select = new StringBuilder("select T.INFORXML, SUB.INFORAS4 " );

		final StringBuilder from = new StringBuilder( " from TMCT0XAS T, TMCT0XAS SUB " );
		final StringBuilder where = new StringBuilder( " " );
		boolean whereEmpty = true;

		final String bolsaMercadoParamName = "bolsaMercado";
		
		where.append(" where T.NBCAMAS4 = 'CDBOLSAS' AND T.INFORAS4 = SUB.INFORXML AND SUB.NBCAMAS4='CODORGEJ' AND T.NUVERSION = '1' ");
		if ( sBolsaMercado != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "SUB.INFORAS4 = :" + bolsaMercadoParamName );
		}
		else
		{
			where.append( "AND (SUB.INFORAS4 = '001' OR SUB.INFORAS4 = '002' OR SUB.INFORAS4 = '003' OR SUB.INFORAS4 = '004') ");
		}
			
		final Query query = em.createNativeQuery( select.append( from.append( where ) )
				.toString() );

		setParametersWithVectors( query, sBolsaMercado, bolsaMercadoParamName);
		
		return query;
	}
	
	public List< XasDTO > findDTOForService( String sBolsaMercado ) {
		
		Query query = consultaBolsasMercado (sBolsaMercado);		
		return this.resultadosConsulta(query);
		
	}//public List< XasDTO > findDTOForService( String sBolsaMercado ) {

}//public class Tmct0xasDaoImpl {
