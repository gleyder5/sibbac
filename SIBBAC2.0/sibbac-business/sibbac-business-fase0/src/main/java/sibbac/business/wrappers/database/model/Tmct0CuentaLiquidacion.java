package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;


/**
 * The Class Tmct0CuentaLiquidacion.
 */
@Table( name = DBConstants.CONCILIACION.TMCT0_CUENTA_LIQUIDACION )
@Entity
@XmlRootElement
public class Tmct0CuentaLiquidacion implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long					serialVersionUID	= 1L;

	/** The id. */
	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private long								id;

	/** The entidad registro. */
	@ManyToOne
	@JoinColumn( name = "ER", referencedColumnName = "ID" )
	private Tmct0EntidadRegistro				entidadRegistro;

	/** The id mercado contratacion. */
	@Column( name = "ID_MERCADO_CONTRATACION" )
	private long								idMercadoContratacion;

	/** The cd codigo. */
	@Column( name = "CD_CODIGO" )
	private String								cdCodigo;

	/** The num cuenta er. */
	@Column( name = "NUM_CUENTA_ER" )
	private long								numCuentaER;

	/** The es cuenta propia. */
	@Column( name = "ES_CUENTA_PROPIA" )
	private boolean								esCuentaPropia;

	/** The tipo neteo titulo. */
	@ManyToOne
	@JoinColumn( name = "TIPO_NETEO_TITULO", referencedColumnName = "ID" )
	private Tmct0TipoNeteo						tipoNeteoTitulo;

	/** The tipo neteo efectivo. */
	@ManyToOne
	@JoinColumn( name = "TIPO_NETEO_EFECTIVO", referencedColumnName = "ID" )
	private Tmct0TipoNeteo						tipoNeteoEfectivo;

	/** The frecuencia envio extracto. */
	@Column( name = "FRECUENCIA_ENVIO_EXTRACTO" )
	private long								frecuenciaEnvioExtracto;

	/** The tipo fichero conciliacion. */
	@Column( name = "TIPO_FICHERO_CONCILIACION" )
	private String								tipoFicheroConciliacion;

	// @ManyToOne( fetch = FetchType.LAZY )
	// @JoinColumn( name = "ID_CUENTA_COMPENSACION", referencedColumnName = "ID_CUENTA_COMPENSACION" )
	// private Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion;

	/** The cuenta norma43. */
	@Column( name = "CUENTA_NORMA_43", precision = 8 )
	private Long								cuentaNorma43;

	/** The cd cuenta liquidacion. */
	@Column( name = "CD_CUENTA_LIQUIDACION", length = 35 )
	private String								cdCuentaLiquidacion;

	/** The tipo cuenta liquidacion. */
	@ManyToOne
	@JoinColumn( name = "TIPO_CUENTA_LIQUIDACION", referencedColumnName = "ID" )
	private TipoCuentaLiquidacion				tipoCuentaLiquidacion;

	/** The tipo cuenta conciliacion. */
	@ManyToOne
	@JoinColumn( name = "TIPO_CUENTA_CONCILIACION", referencedColumnName = "ID" )
	private TipoCuentaConciliacion				tipoCuentaConciliacion;

	/** The relacion cuenta mercado list. */
	@OneToMany( fetch = FetchType.LAZY, targetEntity = RelacionCuentaMercado.class, mappedBy = "relacionCuentaMercadoPK.idCtaLiquidacion" )
	private List< RelacionCuentaMercado >		relacionCuentaMercadoList;
	/** Lista de cuentas de compensacion relacionadas */
	@OneToMany( fetch = FetchType.LAZY, targetEntity = Tmct0CuentasDeCompensacion.class, mappedBy = "tmct0CuentaLiquidacion" )
	private List< Tmct0CuentasDeCompensacion >	tmct0CuentasDeCompensacionList;

	/**
	 * True = titulos, False = efectivo
	 */
	@Column( name = "TITULOS" )
	private boolean								titulos;
	@ManyToOne
	@JoinColumn( name = "ID_SISTEMA_LIQUIDACION", referencedColumnName = "ID" )
	private SistemaLiquidacion					sistemaLiquidacion;

	// Hack: 2015.09.23: Added for S3 Matching against "MovimientoCuentaVirtualS3.subDepositario".
	@Column( name = "CD_CUENTA_IBERCLEAR", length = 35 )
	private String								cuentaIberclear;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId( long id ) {
		this.id = id;
	}

	/**
	 * Gets the entidad registro.
	 *
	 * @return the entidad registro
	 */
	public Tmct0EntidadRegistro getEntidadRegistro() {
		return entidadRegistro;
	}

	/**
	 * Sets the entidad registro.
	 *
	 * @param entidadRegistro the new entidad registro
	 */
	public void setEntidadRegistro( Tmct0EntidadRegistro entidadRegistro ) {
		this.entidadRegistro = entidadRegistro;
	}

	/**
	 * Gets the id mercado contratacion.
	 *
	 * @return the id mercado contratacion
	 */
	public long getIdMercadoContratacion() {
		return idMercadoContratacion;
	}

	/**
	 * Sets the id mercado contratacion.
	 *
	 * @param idMercadoContratacion the new id mercado contratacion
	 */
	public void setIdMercadoContratacion( long idMercadoContratacion ) {
		this.idMercadoContratacion = idMercadoContratacion;
	}

	/**
	 * Gets the cd codigo.
	 *
	 * @return the cd codigo
	 */
	public String getCdCodigo() {
		return cdCodigo;
	}

	/**
	 * Sets the cd codigo.
	 *
	 * @param cdCodigo the new cd codigo
	 */
	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	/**
	 * Gets the num cuenta er.
	 *
	 * @return the num cuenta er
	 */
	public long getNumCuentaER() {
		return numCuentaER;
	}

	/**
	 * Sets the num cuenta er.
	 *
	 * @param numCuentaER the new num cuenta er
	 */
	public void setNumCuentaER( long numCuentaER ) {
		this.numCuentaER = numCuentaER;
	}

	/**
	 * Checks if is es cuenta propia.
	 *
	 * @return true, if is es cuenta propia
	 */
	public boolean isEsCuentaPropia() {
		return esCuentaPropia;
	}

	/**
	 * Sets the es cuenta propia.
	 *
	 * @param esCuentaPropia the new es cuenta propia
	 */
	public void setEsCuentaPropia( boolean esCuentaPropia ) {
		this.esCuentaPropia = esCuentaPropia;
	}

	/**
	 * Gets the tipo neteo titulo.
	 *
	 * @return the tipo neteo titulo
	 */
	public Tmct0TipoNeteo getTipoNeteoTitulo() {
		return tipoNeteoTitulo;
	}

	/**
	 * Sets the tipo neteo titulo.
	 *
	 * @param tipoNeteoTitulo the new tipo neteo titulo
	 */
	public void setTipoNeteoTitulo( Tmct0TipoNeteo tipoNeteoTitulo ) {
		this.tipoNeteoTitulo = tipoNeteoTitulo;
	}

	/**
	 * Gets the tipo neteo efectivo.
	 *
	 * @return the tipo neteo efectivo
	 */
	public Tmct0TipoNeteo getTipoNeteoEfectivo() {
		return tipoNeteoEfectivo;
	}

	/**
	 * Sets the tipo neteo efectivo.
	 *
	 * @param tipoNeteoEfectivo the new tipo neteo efectivo
	 */
	public void setTipoNeteoEfectivo( Tmct0TipoNeteo tipoNeteoEfectivo ) {
		this.tipoNeteoEfectivo = tipoNeteoEfectivo;
	}

	/**
	 * Gets the frecuencia envio extracto.
	 *
	 * @return the frecuencia envio extracto
	 */
	public long getFrecuenciaEnvioExtracto() {
		return frecuenciaEnvioExtracto;
	}

	/**
	 * Sets the frecuencia envio extracto.
	 *
	 * @param frecuenciaEnvioExtracto the new frecuencia envio extracto
	 */
	public void setFrecuenciaEnvioExtracto( long frecuenciaEnvioExtracto ) {
		this.frecuenciaEnvioExtracto = frecuenciaEnvioExtracto;
	}

	/**
	 * Gets the tipo fichero conciliacion.
	 *
	 * @return the tipo fichero conciliacion
	 */
	public String getTipoFicheroConciliacion() {
		return tipoFicheroConciliacion;
	}

	/**
	 * Sets the tipo fichero conciliacion.
	 *
	 * @param tipoFicheroConciliacion the new tipo fichero conciliacion
	 */
	public void setTipoFicheroConciliacion( String tipoFicheroConciliacion ) {
		this.tipoFicheroConciliacion = tipoFicheroConciliacion;
	}

	/**
	 * Gets the tmct0 cuentas de compensacion.
	 *
	 * @return the tmct0 cuentas de compensacion
	 */
	// public Tmct0CuentasDeCompensacion getTmct0CuentasDeCompensacion() {
	// return tmct0CuentasDeCompensacion;
	// }

	/**
	 * Sets the tmct0 cuentas de compensacion.
	 *
	 * @param tmct0CuentasDeCompensacion the new tmct0 cuentas de compensacion
	 */
	// public void setTmct0CuentasDeCompensacion( Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion ) {
	// this.tmct0CuentasDeCompensacion = tmct0CuentasDeCompensacion;
	// }

	/**
	 * Gets the cuenta norma43.
	 *
	 * @return the cuenta norma43
	 */
	public Long getCuentaNorma43() {
		return cuentaNorma43;
	}

	/**
	 * Sets the cuenta norma43.
	 *
	 * @param cuentaNorma43 the new cuenta norma43
	 */
	public void setCuentaNorma43( Long cuentaNorma43 ) {
		this.cuentaNorma43 = cuentaNorma43;
	}

	/**
	 * Gets the cd cuenta liquidacion.
	 *
	 * @return the cd cuenta liquidacion
	 */
	public String getCdCuentaLiquidacion() {
		return cdCuentaLiquidacion;
	}

	/**
	 * Sets the cd cuenta liquidacion.
	 *
	 * @param cdCuentaLiquidacion the new cd cuenta liquidacion
	 */
	public void setCdCuentaLiquidacion( String cdCuentaLiquidacion ) {
		this.cdCuentaLiquidacion = cdCuentaLiquidacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[" + this.getClass().getName().toUpperCase() + "==" + this.id + "]";
	}

	/**
	 * Gets the tipo cuenta liquidacion.
	 *
	 * @return the tipo cuenta liquidacion
	 */
	public TipoCuentaLiquidacion getTipoCuentaLiquidacion() {
		return tipoCuentaLiquidacion;
	}

	/**
	 * Sets the tipo cuenta liquidacion.
	 *
	 * @param tipoCuentaLiquidacion the new tipo cuenta liquidacion
	 */
	public void setTipoCuentaLiquidacion( TipoCuentaLiquidacion tipoCuentaLiquidacion ) {
		this.tipoCuentaLiquidacion = tipoCuentaLiquidacion;
	}

	/**
	 * Gets the tipo cuenta conciliacion.
	 *
	 * @return the tipo cuenta conciliacion
	 */
	public TipoCuentaConciliacion getTipoCuentaConciliacion() {
		return tipoCuentaConciliacion;
	}

	/**
	 * Sets the tipo cuenta conciliacion.
	 *
	 * @param tipoCuentaConciliacion the new tipo cuenta conciliacion
	 */
	public void setTipoCuentaConciliacion( TipoCuentaConciliacion tipoCuentaConciliacion ) {
		this.tipoCuentaConciliacion = tipoCuentaConciliacion;
	}

	/**
	 * Gets the relacion cuenta mercado list.
	 *
	 * @return the relacion cuenta mercado list
	 */
	public List< RelacionCuentaMercado > getRelacionCuentaMercadoList() {
		return relacionCuentaMercadoList;
	}

	/**
	 * Sets the relacion cuenta mercado list.
	 *
	 * @param relacionCuentaMercadoList the new relacion cuenta mercado list
	 */
	public void setRelacionCuentaMercadoList( List< RelacionCuentaMercado > relacionCuentaMercadoList ) {
		this.relacionCuentaMercadoList = relacionCuentaMercadoList;
	}

	public List< Tmct0CuentasDeCompensacion > getTmct0CuentasDeCompensacionList() {
		return tmct0CuentasDeCompensacionList;
	}

	public void setTmct0CuentasDeCompensacionList( List< Tmct0CuentasDeCompensacion > tmct0CuentasDeCompensacionList ) {
		this.tmct0CuentasDeCompensacionList = tmct0CuentasDeCompensacionList;
	}

	public boolean isTitulos() {
		return titulos;
	}

	public void setTitulos( boolean titulos ) {
		this.titulos = titulos;
	}

	public SistemaLiquidacion getSistemaLiquidacion() {
		return sistemaLiquidacion;
	}

	public void setSistemaLiquidacion( SistemaLiquidacion sistemaLiquidacion ) {
		this.sistemaLiquidacion = sistemaLiquidacion;
	}

	public String getCuentaIberclear() {
		return this.cuentaIberclear;
	}

	public void setCuentaIberclear( String cuentaIberclear ) {
		this.cuentaIberclear = cuentaIberclear;
	}

}
