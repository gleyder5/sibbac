package sibbac.business.wrappers.database.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity: "Contacto".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table(name = WRAPPERS.CONTACTO)
@Entity
@Component
@XmlRootElement
@Audited
public class Contacto extends ATableAudited<Contacto> implements java.io.Serializable {

  // ------------------------------------------ Static Bean properties

  /**
	 *
	 */
  private static final long serialVersionUID = -7400080410442424722L;

  public static final Character ACTIVO_S = 'S';
  public static final Character ACTIVO_N = 'N';

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "ID_ALIAS", referencedColumnName = "ID", nullable = true)
  private Alias alias;

  @JsonIgnore
  @ManyToOne(optional = true)
  @JoinColumn(name = "IDCLIENT", referencedColumnName = "IDCLIENT", nullable = true)
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0cli cliente;

  @OneToOne(fetch = FetchType.EAGER, optional = true, cascade = CascadeType.ALL)
  @JoinColumn(name = "IDADDRESS", referencedColumnName = "IDADDRESS", nullable = true)
  @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
  private Tmct0Address address;
  // ------------------------------------------------- Bean properties

  @Column(name = "NBNOMBRED", length = 60)
  @XmlAttribute
  private String descripcion;

  @Column(name = "NBTELEFONO1", length = 60)
  @XmlAttribute
  private String telefono1;

  @Column(name = "NBTELEFONO2", length = 60)
  @XmlAttribute
  private String telefono2;

  @Column(name = "NBMOVIL", length = 20)
  @XmlAttribute
  private String movil;

  @Column(name = "NBFAX", length = 60)
  @XmlAttribute
  private String fax;

  @Column(name = "NBEMAIL", nullable = false, length = 60)
  @XmlAttribute
  private String email;

  @Column(name = "NBCOMENTARIOS", length = 60)
  @XmlAttribute
  private String comentarios;

  @Column(name = "NBPOSICION", length = 60)
  @XmlAttribute
  private String posicionCargo;

  @Column(name = "ACTIVO", length = 1, nullable = false)
  @XmlAttribute
  private Character activo;

  @Column(name = "NBNOMBRE", nullable = false, length = 60)
  @XmlAttribute
  private String nombre;

  @Column(name = "NBPAPELLIDO", nullable = false, length = 60)
  @XmlAttribute
  private String apellido1;

  @Column(name = "NBSAPELLIDO", length = 60)
  @XmlAttribute
  private String apellido2;

  @Column(name = "DEFECTO", length = 1)
  @XmlAttribute
  private Boolean defecto;

  @Column(name = "NBGESTOR", length = 1)
  @XmlAttribute
  private Boolean gestor;
  
  @Column(name = "CDETRALI", length = 20)
  @XmlAttribute
  private String cdEtrali;

  // ----------------------------------------------- Bean Constructors

  public Contacto() {

  }

  public String getApellidos() {
    String ap = "";
    if (apellido1 != null) {
      ap = ap + apellido1;
    }
    if (apellido2 != null) {
      ap = ap + " " + apellido2;
    }
    return ap;
  }

  public Long getIdCliente() {
    if (cliente != null) {
      return cliente.getIdCliente();
    }
    return null;
  }

  public String getDireccionPrincipal() {
    if (address != null) {
      return address.getDescripcion();
    }
    return "";
  }

  public String getTelefonos() {
    String t = String.valueOf(telefono1);
    if (telefono2 != null) {
      t = t + " - " + telefono2;
    }
    return t;
  }
  
  public String getCdEtrali() {
    return cdEtrali;
  }

  public void setCdEtrali(String cdEtrali) {
    this.cdEtrali = cdEtrali;
  }

  /**
   * De una lista de contactos, devuelve los correos electronicos no repetidos
   * 
   * @param contactos
   * @return
   */
  public static List<String> getCorreosContactos(final List<Contacto> contactos) {
    final List<String> correosContactos = new ArrayList<String>();
    final Set<String> correosContactosSet = new HashSet<String>();
    if (CollectionUtils.isNotEmpty(contactos)) {
      for (final Contacto contacto : contactos) {
        final String[] correosContacto = StringUtils.split(contacto.getEmail(), ';');
        correosContactosSet.addAll(Arrays.asList(correosContacto));
      }
    }
    correosContactos.addAll(correosContactosSet);
    return correosContactos;
  }

  /**
   * @return the alias
   */
  public Alias getAlias() {
    return alias;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(Alias alias) {
    this.alias = alias;
  }

  /**
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * @return the telefono1
   */
  public String getTelefono1() {
    return telefono1;
  }

  /**
   * @param telefono1 the telefono1 to set
   */
  public void setTelefono1(String telefono1) {
    this.telefono1 = telefono1;
  }

  /**
   * @return the telefono2
   */
  public String getTelefono2() {
    return telefono2;
  }

  /**
   * @param telefono2 the telefono2 to set
   */
  public void setTelefono2(String telefono2) {
    this.telefono2 = telefono2;
  }

  /**
   * @return the movil
   */
  public String getMovil() {
    return movil;
  }

  /**
   * @param movil the movil to set
   */
  public void setMovil(String movil) {
    this.movil = movil;
  }

  /**
   * @return the fax
   */
  public String getFax() {
    return fax;
  }

  /**
   * @param fax the fax to set
   */
  public void setFax(String fax) {
    this.fax = fax;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  public Tmct0cli getCliente() {
    return cliente;
  }

  public void setCliente(Tmct0cli cliente) {
    this.cliente = cliente;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the comentarios
   */
  public String getComentarios() {
    return comentarios;
  }

  /**
   * @param comentarios the comentarios to set
   */
  public void setComentarios(String comentarios) {
    this.comentarios = comentarios;
  }

  /**
   * @return the posicionCargo
   */
  public String getPosicionCargo() {
    return posicionCargo;
  }

  /**
   * @param posicionCargo the posicionCargo to set
   */
  public void setPosicionCargo(String posicionCargo) {
    this.posicionCargo = posicionCargo;
  }

  /**
   * @return the activo
   */
  public Character getActivo() {
    return activo;
  }

  /**
   * @param activo the activo to set
   */
  public void setActivo(Character activo) {
    this.activo = activo;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the apellido1
   */
  public String getApellido1() {
    return apellido1;
  }

  /**
   * @param apellido1 the apellido1 to set
   */
  public void setApellido1(String apellido1) {
    this.apellido1 = apellido1;
  }

  /**
   * @return the apellido2
   */
  public String getApellido2() {
    return apellido2;
  }

  /**
   * @param apellido2 the apellido2 to set
   */
  public void setApellido2(String apellido2) {
    this.apellido2 = apellido2;
  }

  public Boolean getGestor() {
    return gestor;
  }

  public void setGestor(Boolean gestor) {
    this.gestor = gestor;
  }

  /**
   * @return the defecto
   */
  public Boolean getDefecto() {
    return defecto;
  }

  /**
   * @param defecto the defecto to set
   */
  public void setDefecto(Boolean defecto) {
    this.defecto = defecto;
  }

  @Override
  public String toString() {
    return "Contacto [alias=" + alias + ", descripcion=" + descripcion + ", telefono1=" + telefono1 + ", telefono2="
           + telefono2 + ", movil=" + movil + ", fax=" + fax + ", email=" + email + ", comentarios=" + comentarios
           + ", posicionCargo=" + posicionCargo + ", activo=" + activo + ", nombre=" + nombre + ", apellido1="
           + apellido1 + ", apellido2=" + apellido2 + ", defecto=" + defecto + ", gestor=" + gestor + ", id=" + id
           + "]";
  }

  public Tmct0Address getAddress() {
    return address;
  }

  public void setAddress(Tmct0Address address) {
    this.address = address;
  }

  public String getNameAndDescClient() {
    if (cliente == null)
      return "";
    String retorno = "";
    if (cliente.getCdbrocli() != null)
      retorno += cliente.getCdbrocli();
    if (cliente.getDescli() != null)
      retorno += " - " + cliente.getDescli();
    return retorno;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((activo == null) ? 0 : activo.hashCode());
    result = prime * result + ((address == null) ? 0 : address.hashCode());
    result = prime * result + ((alias == null) ? 0 : alias.hashCode());
    result = prime * result + ((apellido1 == null) ? 0 : apellido1.hashCode());
    result = prime * result + ((apellido2 == null) ? 0 : apellido2.hashCode());
    result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
    result = prime * result + ((comentarios == null) ? 0 : comentarios.hashCode());
    result = prime * result + ((defecto == null) ? 0 : defecto.hashCode());
    result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((fax == null) ? 0 : fax.hashCode());
    result = prime * result + ((gestor == null) ? 0 : gestor.hashCode());
    result = prime * result + ((movil == null) ? 0 : movil.hashCode());
    result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
    result = prime * result + ((posicionCargo == null) ? 0 : posicionCargo.hashCode());
    result = prime * result + ((telefono1 == null) ? 0 : telefono1.hashCode());
    result = prime * result + ((telefono2 == null) ? 0 : telefono2.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Contacto other = (Contacto) obj;
    if (activo == null) {
      if (other.activo != null)
        return false;
    } else if (!activo.equals(other.activo))
      return false;
    if (address == null) {
      if (other.address != null)
        return false;
    } else if (!address.equals(other.address))
      return false;
    if (alias == null) {
      if (other.alias != null)
        return false;
    } else if (!alias.equals(other.alias))
      return false;
    if (apellido1 == null) {
      if (other.apellido1 != null)
        return false;
    } else if (!apellido1.equals(other.apellido1))
      return false;
    if (apellido2 == null) {
      if (other.apellido2 != null)
        return false;
    } else if (!apellido2.equals(other.apellido2))
      return false;
    if (cliente == null) {
      if (other.cliente != null)
        return false;
    } else if (!cliente.equals(other.cliente))
      return false;
    if (comentarios == null) {
      if (other.comentarios != null)
        return false;
    } else if (!comentarios.equals(other.comentarios))
      return false;
    if (defecto == null) {
      if (other.defecto != null)
        return false;
    } else if (!defecto.equals(other.defecto))
      return false;
    if (descripcion == null) {
      if (other.descripcion != null)
        return false;
    } else if (!descripcion.equals(other.descripcion))
      return false;
    if (email == null) {
      if (other.email != null)
        return false;
    } else if (!email.equals(other.email))
      return false;
    if (fax == null) {
      if (other.fax != null)
        return false;
    } else if (!fax.equals(other.fax))
      return false;
    if (gestor == null) {
      if (other.gestor != null)
        return false;
    } else if (!gestor.equals(other.gestor))
      return false;
    if (movil == null) {
      if (other.movil != null)
        return false;
    } else if (!movil.equals(other.movil))
      return false;
    if (nombre == null) {
      if (other.nombre != null)
        return false;
    } else if (!nombre.equals(other.nombre))
      return false;
    if (posicionCargo == null) {
      if (other.posicionCargo != null)
        return false;
    } else if (!posicionCargo.equals(other.posicionCargo))
      return false;
    if (telefono1 == null) {
      if (other.telefono1 != null)
        return false;
    } else if (!telefono1.equals(other.telefono1))
      return false;
    if (telefono2 == null) {
      if (other.telefono2 != null)
        return false;
    } else if (!telefono2.equals(other.telefono2))
      return false;
    return true;
  }

}
