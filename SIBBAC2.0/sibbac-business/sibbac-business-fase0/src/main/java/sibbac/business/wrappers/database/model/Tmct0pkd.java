package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0PKD. Confirmation sent to broker
 * Allocations Documentación para TMCT0PKD <!-- begin-user-doc --> <!--
 * end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0PKD")
public class Tmct0pkd implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7336090661692999253L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "pkbroker", column = @Column(name = "PKBROKER", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER", nullable = false, length = 20)),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)) })
  private Tmct0pkdId id;

  /**
   * Relation 1..1 with table TMCT0PKC. Constraint de relación entre TMCT0PKD y
   * TMCT0PKC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "PKBROKER", referencedColumnName = "PKBROKER", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDBROKER", referencedColumnName = "CDBROKER", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDMERCAD", referencedColumnName = "CDMERCAD", nullable = false, insertable = false, updatable = false) })
  protected Tmct0pkc tmct0pkc;
  /**
   * Relation 1..n with table TMCT0PKE. Constraint de relación entre TMCT0PKE y
   * TMCT0PKD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0pkd")
  protected List<Tmct0pke> tmct0pkes = new ArrayList<Tmct0pke>(0);

  /**
   * Table Field DSCRIPT. Description Documentación para DSCRIPT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "DSCRIPT", length = 40)
  protected java.lang.String dscript;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;

  /**
   * Table Field PKENTITY. PK sent entity Documentación para PKENTITY <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PKENTITY", length = 16)
  protected java.lang.String pkentity;

  public Tmct0pkdId getId() {
    return id;
  }

  public Tmct0pkc getTmct0pkc() {
    return tmct0pkc;
  }

  public List<Tmct0pke> getTmct0pkes() {
    return tmct0pkes;
  }

  public java.lang.String getDscript() {
    return dscript;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public java.lang.String getPkentity() {
    return pkentity;
  }

  public void setId(Tmct0pkdId id) {
    this.id = id;
  }

  public void setTmct0pkc(Tmct0pkc tmct0pkc) {
    this.tmct0pkc = tmct0pkc;
  }

  public void setTmct0pkes(List<Tmct0pke> tmct0pkes) {
    this.tmct0pkes = tmct0pkes;
  }

  public void setDscript(java.lang.String dscript) {
    this.dscript = dscript;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  public void setPkentity(java.lang.String pkentity) {
    this.pkentity = pkentity;
  }

  @Override
  public String toString() {
    return String.format("Tmct0pkd [id=%s]", this.id);
  }

}
