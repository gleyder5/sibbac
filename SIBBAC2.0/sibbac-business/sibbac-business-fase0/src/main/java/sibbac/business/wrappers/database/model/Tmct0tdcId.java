package sibbac.business.wrappers.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class Tmct0tdcId implements java.io.Serializable, Comparable<Tmct0tdcId> {

  /**
   * 
   */
  private static final long serialVersionUID = -4744896965966824129L;

  /**
   * Table Field FHINIVAL. Fecha inicio validez Documentación para FHINIVAL <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/

  @Temporal(TemporalType.DATE)
  @Column(name = "FHINIVAL")
  protected Date fhinival;

  /**
   * Table Field FHFINVAL. Fecha fin validez Documentación para FHFINVAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FHFINVAL")
  protected Date fhfinval;
  /**
   * Table Field CDDBOLSA. Código de bolsa Documentación para CDDBOLSA <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDDBOLSA", length = 3)
  protected java.lang.String cddbolsa;
  /**
   * Table Field IMMAXEFE. Importe efectivo máximo Documentación para IMMAXEFE <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/
  @Column(name = "IMMAXEFE", precision = 18, scale = 4)
  protected java.math.BigDecimal immaxefe;
  /**
   * Table Field IMDERGES. Cánon de gestión Documentación para IMDERGES <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected Integer nuversion;

  @Override
  public String toString() {
    return "Tmct0tdcId [fhinival=" + fhinival + ", fhfinval=" + fhfinval + ", cddbolsa=" + cddbolsa + ", immaxefe="
           + immaxefe + ", nuversion=" + nuversion + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cddbolsa == null) ? 0 : cddbolsa.hashCode());
    result = prime * result + ((fhfinval == null) ? 0 : fhfinval.hashCode());
    result = prime * result + ((fhinival == null) ? 0 : fhinival.hashCode());
    result = prime * result + ((immaxefe == null) ? 0 : immaxefe.hashCode());
    result = prime * result + ((nuversion == null) ? 0 : nuversion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0tdcId other = (Tmct0tdcId) obj;
    if (cddbolsa == null) {
      if (other.cddbolsa != null)
        return false;
    } else if (!cddbolsa.equals(other.cddbolsa))
      return false;
    if (fhfinval == null) {
      if (other.fhfinval != null)
        return false;
    } else if (!fhfinval.equals(other.fhfinval))
      return false;
    if (fhinival == null) {
      if (other.fhinival != null)
        return false;
    } else if (!fhinival.equals(other.fhinival))
      return false;
    if (immaxefe == null) {
      if (other.immaxefe != null)
        return false;
    } else if (!immaxefe.equals(other.immaxefe))
      return false;
    if (nuversion == null) {
      if (other.nuversion != null)
        return false;
    } else if (!nuversion.equals(other.nuversion))
      return false;
    return true;
  }

  public Date getFhinival() {
    return fhinival;
  }

  public Date getFhfinval() {
    return fhfinval;
  }

  public java.lang.String getCddbolsa() {
    return cddbolsa;
  }

  public java.math.BigDecimal getImmaxefe() {
    return immaxefe;
  }

  public Integer getNuversion() {
    return nuversion;
  }

  public void setFhinival(Date fhinival) {
    this.fhinival = fhinival;
  }

  public void setFhfinval(Date fhfinval) {
    this.fhfinval = fhfinval;
  }

  public void setCddbolsa(java.lang.String cddbolsa) {
    this.cddbolsa = cddbolsa;
  }

  public void setImmaxefe(java.math.BigDecimal immaxefe) {
    this.immaxefe = immaxefe;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Override
  public int compareTo(Tmct0tdcId arg0) {
    if (arg0 == null) {
      return 0;
    } else {
      return this.compareTo(arg0);
    }
  }

}
