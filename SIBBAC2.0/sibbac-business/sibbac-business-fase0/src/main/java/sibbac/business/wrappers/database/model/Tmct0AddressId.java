package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;

@Embeddable
public class Tmct0AddressId implements java.io.Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column( name = "IDADDRESS", nullable = false, unique = true )
	@XmlAttribute
	private Long				idAddress;
	

	
	public Tmct0AddressId() {
		
	}

	

	public Tmct0AddressId(Long idAddress) {
		super();
		this.idAddress = idAddress;
		
	}



	public Long getIdAddress() {
		return idAddress;
	}



	public void setIdAddress(Long idAddress) {
		this.idAddress = idAddress;
	}



	

	
}
