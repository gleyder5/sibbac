package sibbac.business.wrappers.database.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;

/**
 * The Class Tmct0CuentasDeCompensacion.
 */
@XmlRootElement
@Entity
@Table(name = DBConstants.WRAPPERS.CUENTAS_DE_COMPENSACION)
public class Tmct0CuentasDeCompensacion implements java.io.Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6987157417811245283L;

  /** The audit fecha cambio. */
  @XmlAttribute
  private Date auditFechaCambio;

  /** The audit user. */
  @XmlAttribute
  private String auditUser;

  /** The cd codigo. */
  private String cdCodigo;

  /** The cd codigo S3. */
  private String cdCodigoS3;

  /** The cd cuenta compensacion. */
  private String cdCuentaCompensacion;

  /** The cuenta clearing. */
  private char cuentaClearing;

  /** The id cuenta compensacion. */
  private Long idCuentaCompensacion;

  /** The s3. */
  private Character s3;

  /** The tmct0anotacionecc. */
  private Set<Tmct0anotacionecc> tmct0anotacionecc;

  /** The tmct0 compensador. */
  private Tmct0Compensador tmct0Compensador;

  private Tmct0CuentaLiquidacion tmct0CuentaLiquidacion;

  private Tmct0EntidadRegistro entidadRegistro;

  /** The id mercado contratacion. */
  private long idMercadoContratacion;

  /** The num cuenta er. */
  private long numCuentaER;

  /** The es cuenta propia. */
  private boolean esCuentaPropia;

  /** The tipo neteo titulo. */
  private Tmct0TipoNeteo tipoNeteoTitulo;

  /** The tipo neteo efectivo. */
  private Tmct0TipoNeteo tipoNeteoEfectivo;

  /** The frecuencia envio extracto. */
  private long frecuenciaEnvioExtracto;

  /** The tipo fichero conciliacion. */
  private String tipoFicheroConciliacion;

  /** The cuenta norma43. */
  private Long cuentaNorma43;

  /** The tipo cuenta liquidacion. */
  private TipoCuentaLiquidacion tipoCuentaLiquidacion;

  /** The tipo cuenta conciliacion. */
  private TipoCuentaConciliacion tipoCuentaConciliacion;

  /** FIN */
  /** The tmct0 tipo cuenta de compensacion. */
  private Tmct0TipoCuentaDeCompensacion tmct0TipoCuentaDeCompensacion;
  /** The relacion cuenta mercado list. */

  private List<Tmct0Mercado> mercados;
  private String subcustodioDomestico;
  private String numeroCuentaSubcustodio;
  private SistemaLiquidacion sistemaLiquidacion;

  private String clientNumber;
  private String accountNumber;
  private String subaccountNumber;

  /**
   * Instantiates a new tmct0 cuentas de compensacion.
   */
  public Tmct0CuentasDeCompensacion() {
  }

  /**
   * Instantiates a new tmct0 cuentas de compensacion.
   *
   * @param idCuentaCompensacion the id cuenta compensacion
   * @param tmct0Compensador the tmct0 compensador
   * @param tmct0TipoCuentaDeCompensacion the tmct0 tipo cuenta de compensacion
   * @param cdCodigo the cd codigo
   * @param auditFechaCambio the audit fecha cambio
   * @param cuentaClearing the cuenta clearing
   */
  public Tmct0CuentasDeCompensacion(Long idCuentaCompensacion, Tmct0Compensador tmct0Compensador,
      Tmct0TipoCuentaDeCompensacion tmct0TipoCuentaDeCompensacion, String cdCodigo, Date auditFechaCambio,
      char cuentaClearing) {
    this.idCuentaCompensacion = idCuentaCompensacion;
    this.tmct0Compensador = tmct0Compensador;
    this.tmct0TipoCuentaDeCompensacion = tmct0TipoCuentaDeCompensacion;
    this.cdCodigo = cdCodigo;
    this.auditFechaCambio = auditFechaCambio;
    this.cuentaClearing = cuentaClearing;
  }

  /**
   * Instantiates a new tmct0 cuentas de compensacion.
   *
   * @param idCuentaCompensacion the id cuenta compensacion
   * @param tmct0Compensador the tmct0 compensador
   * @param tmct0TipoCuentaDeCompensacion the tmct0 tipo cuenta de compensacion
   * @param cdCodigo the cd codigo
   * @param auditFechaCambio the audit fecha cambio
   * @param s3 the s3
   * @param auditUser the audit user
   * @param cuentaClearing the cuenta clearing
   * @param cdCuentaCompensacion the cd cuenta compensacion
   * @param tmct0anotacionecc the tmct0anotacionecc
   */
  public Tmct0CuentasDeCompensacion(Long idCuentaCompensacion, Tmct0Compensador tmct0Compensador,
      Tmct0TipoCuentaDeCompensacion tmct0TipoCuentaDeCompensacion, String cdCodigo, Date auditFechaCambio,
      Character s3, String auditUser, char cuentaClearing, String cdCuentaCompensacion,
      Set<Tmct0anotacionecc> tmct0anotacionecc) {
    this.idCuentaCompensacion = idCuentaCompensacion;
    this.tmct0Compensador = tmct0Compensador;
    this.tmct0TipoCuentaDeCompensacion = tmct0TipoCuentaDeCompensacion;
    this.cdCodigo = cdCodigo;
    this.auditFechaCambio = auditFechaCambio;
    this.s3 = s3;
    this.auditUser = auditUser;
    this.cuentaClearing = cuentaClearing;
    this.cdCuentaCompensacion = cdCuentaCompensacion;
    this.tmct0anotacionecc = tmct0anotacionecc;
  }

  /**
   * Gets the audit fecha cambio.
   *
   * @return the audit fecha cambio
   */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", nullable = false, length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  /**
   * Gets the audit user.
   *
   * @return the audit user
   */
  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  /**
   * Gets the cd codigo.
   *
   * @return the cd codigo
   */
  @Column(name = "CD_CODIGO", unique = true, nullable = false, length = 16)
  public String getCdCodigo() {
    return this.cdCodigo;
  }

  /**
   * Gets the cd codigo S3.
   *
   * @return the cd codigo S3
   */
  @Column(name = "CD_CODIGO_S3", unique = false, nullable = true, length = 20)
  public String getCdCodigoS3() {
    return this.cdCodigoS3;
  }

  /**
   * Gets the cd cuenta compensacion.
   *
   * @return the cd cuenta compensacion
   */
  @Column(name = "CD_CUENTA_COMPENSACION", length = 35)
  public String getCdCuentaCompensacion() {
    return this.cdCuentaCompensacion;
  }

  /**
   * Gets the cuenta clearing.
   *
   * @return the cuenta clearing
   */
  @Column(name = "CUENTA_CLEARING", nullable = false, length = 1)
  public char getCuentaClearing() {
    return this.cuentaClearing;
  }

  /**
   * Gets the id cuenta compensacion.
   *
   * @return the id cuenta compensacion
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_CUENTA_COMPENSACION")
  public Long getIdCuentaCompensacion() {
    return this.idCuentaCompensacion;
  }

  /**
   * Gets the s3.
   *
   * @return the s3
   */
  @Column(name = "S3", length = 1)
  public Character getS3() {
    return this.s3;
  }

  /**
   * Gets the tmct0anotacionecc.
   *
   * @return the tmct0anotacionecc
   */
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0CuentasDeCompensacion")
  public Set<Tmct0anotacionecc> getTmct0anotacionecc() {
    return this.tmct0anotacionecc;
  }

  /**
   * Gets the tmct0 compensador.
   *
   * @return the tmct0 compensador
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_COMPENSADOR", nullable = false)
  public Tmct0Compensador getTmct0Compensador() {
    return this.tmct0Compensador;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CUENTA_LIQUIDACION", referencedColumnName = "ID")
  public Tmct0CuentaLiquidacion getTmct0CuentaLiquidacion() {
    return tmct0CuentaLiquidacion;
  }

  /**
   * Gets the tmct0 tipo cuenta de compensacion.
   *
   * @return the tmct0 tipo cuenta de compensacion
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_TIPO_CUENTA", nullable = false)
  public Tmct0TipoCuentaDeCompensacion getTmct0TipoCuentaDeCompensacion() {
    return this.tmct0TipoCuentaDeCompensacion;
  }

  /**
   * Sets the audit fecha cambio.
   *
   * @param auditFechaCambio the new audit fecha cambio
   */
  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  /**
   * Sets the audit user.
   *
   * @param auditUser the new audit user
   */
  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * Sets the cd codigo.
   *
   * @param cdCodigo the new cd codigo
   */
  public void setCdCodigo(String cdCodigo) {
    this.cdCodigo = cdCodigo;
  }

  /**
   * Sets the cd codigo S3.
   *
   * @param cdCodigoS3 the new cd codigo S3
   */
  public void setCdCodigoS3(String cdCodigoS3) {
    this.cdCodigoS3 = cdCodigoS3;
  }

  /**
   * Sets the cd cuenta compensacion.
   *
   * @param cdCuentaCompensacion the new cd cuenta compensacion
   */
  public void setCdCuentaCompensacion(String cdCuentaCompensacion) {
    this.cdCuentaCompensacion = cdCuentaCompensacion;
  }

  /**
   * Sets the cuenta clearing.
   *
   * @param cuentaClearing the new cuenta clearing
   */
  public void setCuentaClearing(char cuentaClearing) {
    this.cuentaClearing = cuentaClearing;
  }

  /**
   * Sets the s3.
   *
   * @param s3 the new s3
   */
  public void setS3(Character s3) {
    this.s3 = s3;
  }

  /**
   * Sets the tmct0anotacionecc.
   *
   * @param tmct0anotacionecc the new tmct0anotacionecc
   */
  public void setTmct0anotacionecc(Set<Tmct0anotacionecc> tmct0anotacionecc) {
    this.tmct0anotacionecc = tmct0anotacionecc;
  }

  /**
   * Sets the tmct0 compensador.
   *
   * @param tmct0Compensador the new tmct0 compensador
   */
  public void setTmct0Compensador(Tmct0Compensador tmct0Compensador) {
    this.tmct0Compensador = tmct0Compensador;
  }

  public void setTmct0CuentaLiquidacion(Tmct0CuentaLiquidacion tmct0CuentaLiquidacion) {
    this.tmct0CuentaLiquidacion = tmct0CuentaLiquidacion;
  }

  /**
   * Sets the tmct0 tipo cuenta de compensacion.
   *
   * @param tmct0TipoCuentaDeCompensacion the new tmct0 tipo cuenta de
   * compensacion
   */
  public void setTmct0TipoCuentaDeCompensacion(Tmct0TipoCuentaDeCompensacion tmct0TipoCuentaDeCompensacion) {
    this.tmct0TipoCuentaDeCompensacion = tmct0TipoCuentaDeCompensacion;
  }

  /** CAMPOS IGUALES A LA TABLE TMCT0_CUENTA_LIQUIDACION */
  /** The entidad registro. */
  @ManyToOne
  @JoinColumn(name = "ER", referencedColumnName = "ID")
  public Tmct0EntidadRegistro getEntidadRegistro() {
    return entidadRegistro;
  }

  public void setEntidadRegistro(Tmct0EntidadRegistro entidadRegistro) {
    this.entidadRegistro = entidadRegistro;
  }

  @Column(name = "ID_MERCADO_CONTRATACION")
  public long getIdMercadoContratacion() {
    return idMercadoContratacion;
  }

  public void setIdMercadoContratacion(long idMercadoContratacion) {
    this.idMercadoContratacion = idMercadoContratacion;
  }

  @Column(name = "NUM_CUENTA_ER")
  public long getNumCuentaER() {
    return numCuentaER;
  }

  public void setNumCuentaER(long numCuentaER) {
    this.numCuentaER = numCuentaER;
  }

  @Column(name = "ES_CUENTA_PROPIA")
  public boolean getEsCuentaPropia() {
    return esCuentaPropia;
  }

  public void setEsCuentaPropia(boolean esCuentaPropia) {
    this.esCuentaPropia = esCuentaPropia;
  }

  @ManyToOne
  @JoinColumn(name = "TIPO_NETEO_TITULO", referencedColumnName = "ID")
  public Tmct0TipoNeteo getTipoNeteoTitulo() {
    return tipoNeteoTitulo;
  }

  public void setTipoNeteoTitulo(Tmct0TipoNeteo tipoNeteoTitulo) {
    this.tipoNeteoTitulo = tipoNeteoTitulo;
  }

  @ManyToOne
  @JoinColumn(name = "TIPO_NETEO_EFECTIVO", referencedColumnName = "ID")
  public Tmct0TipoNeteo getTipoNeteoEfectivo() {
    return tipoNeteoEfectivo;
  }

  public void setTipoNeteoEfectivo(Tmct0TipoNeteo tipoNeteoEfectivo) {
    this.tipoNeteoEfectivo = tipoNeteoEfectivo;
  }

  @Column(name = "FRECUENCIA_ENVIO_EXTRACTO")
  public long getFrecuenciaEnvioExtracto() {
    return frecuenciaEnvioExtracto;
  }

  public void setFrecuenciaEnvioExtracto(long frecuenciaEnvioExtracto) {
    this.frecuenciaEnvioExtracto = frecuenciaEnvioExtracto;
  }

  @Column(name = "TIPO_FICHERO_CONCILIACION")
  public String getTipoFicheroConciliacion() {
    return tipoFicheroConciliacion;
  }

  public void setTipoFicheroConciliacion(String tipoFicheroConciliacion) {
    this.tipoFicheroConciliacion = tipoFicheroConciliacion;
  }

  @Column(name = "CUENTA_NORMA_43", precision = 8)
  public Long getCuentaNorma43() {
    return cuentaNorma43;
  }

  public void setCuentaNorma43(Long cuentaNorma43) {
    this.cuentaNorma43 = cuentaNorma43;
  }

  @ManyToOne
  @JoinColumn(name = "TIPO_CUENTA_LIQUIDACION", referencedColumnName = "ID", nullable = true)
  public TipoCuentaLiquidacion getTipoCuentaLiquidacion() {
    return tipoCuentaLiquidacion;
  }

  public void setTipoCuentaLiquidacion(TipoCuentaLiquidacion tipoCuentaLiquidacion) {
    this.tipoCuentaLiquidacion = tipoCuentaLiquidacion;
  }

  @ManyToOne
  @JoinColumn(name = "TIPO_CUENTA_CONCILIACION", referencedColumnName = "ID", nullable = true)
  public TipoCuentaConciliacion getTipoCuentaConciliacion() {
    return tipoCuentaConciliacion;
  }

  public void setTipoCuentaConciliacion(TipoCuentaConciliacion tipoCuentaConciliacion) {
    this.tipoCuentaConciliacion = tipoCuentaConciliacion;
  }

  @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
  @JoinTable(name = DBConstants.WRAPPERS.RELACION_CUENTACOMP_MERCADO, joinColumns = @JoinColumn(name = "ID_CUENTA_COMPENSACION", referencedColumnName = "ID_CUENTA_COMPENSACION"), inverseJoinColumns = @JoinColumn(name = "ID_MERCADO", referencedColumnName = "ID"))
  public List<Tmct0Mercado> getMercados() {
    return this.mercados;
  }

  public void setMercados(List<Tmct0Mercado> mercados) {
    this.mercados = mercados;
  }

  @PrePersist
  public void prePersist() {
    auditFechaCambio = new Date();
  }

  @Column(name = "SUBCUSTODIO_DOMESTICO", length = 255)
  public String getSubcustodioDomestico() {
    return subcustodioDomestico;
  }

  public void setSubcustodioDomestico(String subcustodioDomestico) {
    this.subcustodioDomestico = subcustodioDomestico;
  }

  @Column(name = "NUMERO_CUENTA_SUBCUSTODIO", length = 35)
  public String getNumeroCuentaSubcustodio() {
    return numeroCuentaSubcustodio;
  }

  public void setNumeroCuentaSubcustodio(String numeroCuentaSubcustodio) {
    this.numeroCuentaSubcustodio = numeroCuentaSubcustodio;
  }

  public void setIdCuentaCompensacion(Long idCuentaCompensacion) {
    this.idCuentaCompensacion = idCuentaCompensacion;
  }

  @ManyToOne
  @JoinColumn(name = "ID_SISTEMA_LIQUIDACION", referencedColumnName = "ID")
  public SistemaLiquidacion getSistemaLiquidacion() {
    return sistemaLiquidacion;
  }

  public void setSistemaLiquidacion(SistemaLiquidacion sistemaLiquidacion) {
    this.sistemaLiquidacion = sistemaLiquidacion;
  }

  @Column(name = "CLIENT_NUMBER", length = 10)
  public String getClientNumber() {
    return clientNumber;
  }

  @Column(name = "ACCOUNT_NUMBER", length = 10)
  public String getAccountNumber() {
    return accountNumber;
  }

  @Column(name = "SUBACCOUNT_NUMBER", length = 10)
  public String getSubaccountNumber() {
    return subaccountNumber;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setSubaccountNumber(String subaccountNumber) {
    this.subaccountNumber = subaccountNumber;
  }

}
