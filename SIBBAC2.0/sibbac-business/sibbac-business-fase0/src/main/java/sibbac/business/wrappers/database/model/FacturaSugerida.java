package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * The Class FacturaSugerida.
 */
@Table( name = WRAPPERS.FACTURA_SUGERIDA )
@Entity
@Audited
public class FacturaSugerida extends ATableAudited< FacturaSugerida > implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long		serialVersionUID	= 1L;

	/** The estado. */
	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ESTADO", nullable = false, referencedColumnName = "IDESTADO" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	private Tmct0estado				estado;

	/** The fecha factura sugerida. */
	@Column( name = "FHCREACION", nullable = false )
	@Temporal( TemporalType.TIMESTAMP )
	private Date					fechaCreacion;

	@Column( name = "FHINICIO", nullable = false )
	@Temporal( TemporalType.TIMESTAMP )
	private Date					fechaInicio;

	@Column( name = "FHFIN", nullable = false )
	@Temporal( TemporalType.TIMESTAMP )
	private Date					fechaFin;

	/** The alias Subcuenta. */
	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ALIAS", nullable = false, referencedColumnName = "ID" )
	private Alias					alias;

	/** The periodo. */
	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_PERIODO", nullable = false, referencedColumnName = "ID" )
	private Periodos				periodo;

	/** The periodo. */
	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_MONEDA", nullable = false, referencedColumnName = "ID" )
	private Monedas					moneda;

	/** The comentarios. */
	@Column( name = "NBCOMENTARIOS", length = 500 )
	private String					comentarios;

	@Column( name = "IMFINSVB", precision = 18, scale = 4 )
	private BigDecimal				imfinsvb;

	@OneToMany( mappedBy = "facturaSugerida", cascade = {
			CascadeType.PERSIST, CascadeType.REMOVE
	} )
	private List< FacturaDetalle >	detallesFactura;

	/**
	 * Instantiates a new factura sugerida.
	 */
	public FacturaSugerida() {

	}

	@Transactional public List< AlcOrdenes > getAlcOrdenes() {
		final List< AlcOrdenes > ordenes = new ArrayList< AlcOrdenes >();
		if ( CollectionUtils.isNotEmpty( detallesFactura ) ) {
			for ( FacturaDetalle facturaDetalle : detallesFactura ) {
				ordenes.addAll( facturaDetalle.getAlcOrdenes() );
			}
		}
		return ordenes;
	}

	public List< AlcOrdenDetalleFactura > getAlcOrdenFacturaDetalles() {
		final List< AlcOrdenDetalleFactura > alcOrdenFacturaDetalles = new ArrayList< AlcOrdenDetalleFactura >();
		if ( CollectionUtils.isNotEmpty( detallesFactura ) ) {
			for ( FacturaDetalle facturaDetalle : detallesFactura ) {
				alcOrdenFacturaDetalles.addAll( facturaDetalle.getAlcOrdenesDetalleFactura() );
			}
		}
		return alcOrdenFacturaDetalles;
	}
	
	public List< Tmct0alcId > getAlcIds() {
		final List< Tmct0alcId > alc = new ArrayList<>();
		if ( CollectionUtils.isNotEmpty( detallesFactura ) ) {
			for (FacturaDetalle facturaDetalle : detallesFactura ) {
				alc.addAll( facturaDetalle.getAlcIds() );
			}
		}
		return alc;
	}

	/**
	 * @return the imfinsvb
	 */
	public BigDecimal getImfinsvb() {
		return imfinsvb;
	}

	/**
	 * @param imfinsvb the imfinsvb to set
	 */
	public void setImfinsvb( BigDecimal imfinsvb ) {
		this.imfinsvb = imfinsvb;
	}

	/**
	 * Añade una linea a la lista de facturas existentes
	 * 
	 * @param facturaDetalle
	 * @return
	 */
	public boolean addDetalleFactura( final FacturaDetalle facturaDetalle ) {
		if ( CollectionUtils.isEmpty( detallesFactura ) ) {
			detallesFactura = new ArrayList< FacturaDetalle >();
		}
		facturaDetalle.setFacturaSugerida( this );

		return detallesFactura.add( facturaDetalle );

	}

	/**
	 * @return
	 */
	public FacturaDetalle getDetalleFactura( final Integer numero ) {
		FacturaDetalle detalleFacturaEncontrado = null;
		if ( CollectionUtils.isNotEmpty( detallesFactura ) ) {
			for ( final FacturaDetalle facturaDetalle : detallesFactura ) {
				if ( numero.equals( facturaDetalle.getNumero() ) ) {
					detalleFacturaEncontrado = facturaDetalle;
				}
			}
		}
		return detalleFacturaEncontrado;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public Tmct0estado getEstado() {
		return estado;
	}

	/**
	 * Gets the fecha factura sugerida.
	 *
	 * @return the fecha factura sugerida
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio( Date fechaInicio ) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the fechaFin
	 */
	public Date getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin( Date fechaFin ) {
		this.fechaFin = fechaFin;
	}

	/**
	 * Gets the alias subcuenta.
	 *
	 * @return the alias subcuenta
	 */
	public Alias getAlias() {
		return alias;
	}

	/**
	 * Gets the periodo.
	 *
	 * @return the periodo
	 */
	public Periodos getPeriodo() {
		return periodo;
	}

	/**
	 * Gets the comentarios.
	 *
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado
	 *            the new estado
	 */
	public void setEstado( Tmct0estado estado ) {
		this.estado = estado;
	}

	/**
	 * Sets the fecha factura sugerida.
	 *
	 * @param fechaCreacion
	 *            the new fecha factura sugerida
	 */
	public void setFechaCreacion( Date fechaCreacion ) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * Sets the alias subcuenta.
	 *
	 * @param aliasSubcuenta
	 *            the new alias
	 */
	public void setAlias( Alias alias ) {
		this.alias = alias;
	}

	/**
	 * Sets the periodo.
	 *
	 * @param periodo
	 *            the new periodo
	 */
	public void setPeriodo( Periodos periodo ) {
		this.periodo = periodo;
	}

	/**
	 * Sets the comentarios.
	 *
	 * @param comentarios
	 *            the new comentarios
	 */
	public void setComentarios( String comentarios ) {
		this.comentarios = comentarios;
	}

	/**
	 * @return the moneda
	 */
	public Monedas getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda( Monedas moneda ) {
		this.moneda = moneda;
	}

	/**
	 * @return the detallesFactura
	 */
	public List< FacturaDetalle > getDetallesFactura() {
		return detallesFactura;
	}

	/**
	 * @param detallesFactura
	 *            the detallesFactura to set
	 */
	public void setDetallesFactura( List< FacturaDetalle > lineasDeFacturaSugerida ) {
		this.detallesFactura = lineasDeFacturaSugerida;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( periodo );
		hcb.append( alias );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		final FacturaSugerida facturaSugerida = ( FacturaSugerida ) obj;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( this.getPeriodo(), facturaSugerida.getPeriodo() );
		eqb.append( this.getAlias(), facturaSugerida.getAlias() );
		return super.equals( obj );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public String toString() {
		final ToStringBuilder tsb = new ToStringBuilder( this, ToStringStyle.SHORT_PREFIX_STYLE );
		tsb.append( this.getPeriodo() );
		tsb.append( this.getAlias() );
		return tsb.toString();
	}
}
