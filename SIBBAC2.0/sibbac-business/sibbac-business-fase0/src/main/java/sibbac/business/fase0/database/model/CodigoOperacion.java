package sibbac.business.fase0.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_CODIGOS_OPERACION" )
public class CodigoOperacion extends MasterData< CodigoOperacion > {

	private static final long	serialVersionUID	= -8837274734594597352L;

	@Column( name = "CD_TRDSUBTYPE_829", length = 16 )
	private String				trdType;
	@Column( name = "CD_TRDTYPE_828", length = 16 )
	private String				trdSubType;

	public String getTrdType() {
		return trdType;
	}

	public String getTrdSubType() {
		return trdSubType;
	}

	public void setTrdType( String trdType ) {
		this.trdType = trdType;
	}

	public void setTrdSubType( String trdSubType ) {
		this.trdSubType = trdSubType;
	}

}
