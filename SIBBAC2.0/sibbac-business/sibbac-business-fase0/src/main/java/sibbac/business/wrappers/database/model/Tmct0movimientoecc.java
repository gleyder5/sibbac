package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0MOVIMIENTOECC")
public class Tmct0movimientoecc implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -4032260281486674825L;
  private Long idmovimiento;
  private Tmct0desglosecamara tmct0desglosecamara;
  private Tmct0movimientoeccFidessa tmct0movimientoeccFidessa;
  private String cdrefmovimiento;
  private BigDecimal imtitulos = BigDecimal.ZERO;
  private String cdrefinternaasig;
  private String cdrefmovimientoecc;
  private String cdrefnotificacion;
  private String cdrefnotificacionprev;
  private String cdtipomov;
  private String cdestadomov;
  private BigDecimal imefectivo = BigDecimal.ZERO;
  private char cdindcotizacion;
  private String nberror;
  private String cderror;
  private Long idmovimientopadre;
  private Date auditFechaCambio;
  private String auditUser;
  private Character imputacionPropia;
  // private List<Tmct0anotacionecc> tmct0anotacioneccs = new
  // ArrayList<Tmct0anotacionecc>(0);
  private List<Tmct0movimientooperacionnuevaecc> tmct0movimientooperacionnuevaeccs = new ArrayList<Tmct0movimientooperacionnuevaecc>();

  private String isin;

  private Character sentido;

  private Character enviadocuentavirtual;

  // private boolean enviadoCuentaAlias = false;

  private String cuentaCompensacionDestino;

  protected Tmct0alo tmct0alo;

  protected String mnemotecnico;

  protected String miembroCompensadorDestino;

  protected String codigoReferenciaAsignacion;

  protected Date fechaLiquidacion;

  protected Integer cdestadoasig;

  public Tmct0movimientoecc() {
  }

  public Tmct0movimientoecc(Long idmovimiento, BigDecimal imtitulos, String cdestadomov, BigDecimal imefectivo,
      char cdindcotizacion) {
    this.idmovimiento = idmovimiento;
    this.imtitulos = imtitulos;
    this.cdestadomov = cdestadomov;
    this.imefectivo = imefectivo;
    this.cdindcotizacion = cdindcotizacion;
  }

  public Tmct0movimientoecc(Long idmovimiento, Tmct0desglosecamara tmct0desglosecamara,
      Tmct0movimientoeccFidessa tmct0movimientoeccFidessa, String cdrefmovimiento, BigDecimal imtitulos,
      String cdrefinternaasig, String cdrefmovimientoecc, String cdrefnotificacion, String cdrefnotificacionprev,
      String cdtipomov, String cdestadomov, BigDecimal imefectivo, char cdindcotizacion, String nberror,
      String cderror, Long idmovimientopadre, Date auditFechaCambio, String auditUser,
      List<Tmct0anotacionecc> tmct0anotacioneccs/*
                                                 * , List<Tmct0cancelacionecc>
                                                 * tmct0cancelacioneccs, List<
                                                 * Tmct0histMovimientoecc>
                                                 * tmct0histMovimientoeccs
                                                 */) {
    this.idmovimiento = idmovimiento;
    this.tmct0desglosecamara = tmct0desglosecamara;
    this.tmct0movimientoeccFidessa = tmct0movimientoeccFidessa;
    this.cdrefmovimiento = cdrefmovimiento;
    this.imtitulos = imtitulos;
    this.cdrefinternaasig = cdrefinternaasig;
    this.cdrefmovimientoecc = cdrefmovimientoecc;
    this.cdrefnotificacion = cdrefnotificacion;
    this.cdrefnotificacionprev = cdrefnotificacionprev;
    this.cdtipomov = cdtipomov;
    this.cdestadomov = cdestadomov;
    this.imefectivo = imefectivo;
    this.cdindcotizacion = cdindcotizacion;
    this.nberror = nberror;
    this.cderror = cderror;
    this.idmovimientopadre = idmovimientopadre;
    this.auditFechaCambio = auditFechaCambio;
    this.auditUser = auditUser;
    // this.tmct0anotacioneccs = tmct0anotacioneccs;
    // this.tmct0cancelacioneccs = tmct0cancelacioneccs;
    // this.tmct0histMovimientoeccs = tmct0histMovimientoeccs;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IDMOVIMIENTO", unique = true, nullable = false)
  public Long getIdmovimiento() {
    return this.idmovimiento;
  }

  public void setIdmovimiento(Long idmovimiento) {
    this.idmovimiento = idmovimiento;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDDESGLOSECAMARA")
  public Tmct0desglosecamara getTmct0desglosecamara() {
    return this.tmct0desglosecamara;
  }

  public void setTmct0desglosecamara(Tmct0desglosecamara tmct0desglosecamara) {
    this.tmct0desglosecamara = tmct0desglosecamara;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_MOVIMIENTO_FIDESSA")
  public Tmct0movimientoeccFidessa getTmct0movimientoeccFidessa() {
    return this.tmct0movimientoeccFidessa;
  }

  public void setTmct0movimientoeccFidessa(Tmct0movimientoeccFidessa tmct0movimientoeccFidessa) {
    this.tmct0movimientoeccFidessa = tmct0movimientoeccFidessa;
  }

  @Column(name = "CDREFMOVIMIENTO", length = 30)
  public String getCdrefmovimiento() {
    return this.cdrefmovimiento;
  }

  public void setCdrefmovimiento(String cdrefmovimiento) {
    this.cdrefmovimiento = cdrefmovimiento;
  }

  @Column(name = "IMTITULOS", nullable = false, precision = 12, scale = 6)
  public BigDecimal getImtitulos() {
    return this.imtitulos;
  }

  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  @Column(name = "CDREFINTERNAASIG", length = 18)
  public String getCdrefinternaasig() {
    return this.cdrefinternaasig;
  }

  public void setCdrefinternaasig(String cdrefinternaasig) {
    this.cdrefinternaasig = cdrefinternaasig;
  }

  @Column(name = "CDREFMOVIMIENTOECC", length = 20)
  public String getCdrefmovimientoecc() {
    return this.cdrefmovimientoecc;
  }

  public void setCdrefmovimientoecc(String cdrefmovimientoecc) {
    this.cdrefmovimientoecc = cdrefmovimientoecc;
  }

  @Column(name = "CDREFNOTIFICACION", length = 20)
  public String getCdrefnotificacion() {
    return this.cdrefnotificacion;
  }

  public void setCdrefnotificacion(String cdrefnotificacion) {
    this.cdrefnotificacion = cdrefnotificacion;
  }

  @Column(name = "CDREFNOTIFICACIONPREV", length = 20)
  public String getCdrefnotificacionprev() {
    return this.cdrefnotificacionprev;
  }

  public void setCdrefnotificacionprev(String cdrefnotificacionprev) {
    this.cdrefnotificacionprev = cdrefnotificacionprev;
  }

  @Column(name = "CDTIPOMOV", length = 2)
  public String getCdtipomov() {
    return this.cdtipomov;
  }

  public void setCdtipomov(String cdtipomov) {
    this.cdtipomov = cdtipomov;
  }

  @Column(name = "CDESTADOMOV", nullable = false, length = 2)
  public String getCdestadomov() {
    return this.cdestadomov;
  }

  public void setCdestadomov(String cdestadomov) {
    this.cdestadomov = cdestadomov;
  }

  @Column(name = "IMEFECTIVO", nullable = false, precision = 13)
  public BigDecimal getImefectivo() {
    return this.imefectivo;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  @Column(name = "CDINDCOTIZACION", nullable = false, length = 1)
  public char getCdindcotizacion() {
    return this.cdindcotizacion;
  }

  public void setCdindcotizacion(char cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  @Column(name = "NBERROR", length = 40)
  public String getNberror() {
    return this.nberror;
  }

  public void setNberror(String nberror) {
    this.nberror = nberror;
  }

  @Column(name = "CDERROR", length = 3)
  public String getCderror() {
    return this.cderror;
  }

  public void setCderror(String cderror) {
    this.cderror = cderror;
  }

  @Column(name = "IDMOVIMIENTOPADRE")
  public Long getIdmovimientopadre() {
    return this.idmovimientopadre;
  }

  public void setIdmovimientopadre(Long idmovimientopadre) {
    this.idmovimientopadre = idmovimientopadre;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * @return the imputacionPropia
   */
  @Column(name = "IMPUTACION_PROPIA", length = 1)
  public Character getImputacionPropia() {
    return imputacionPropia;
  }

  /**
   * @param imputacionPropia the imputacionPropia to set
   */
  public void setImputacionPropia(Character imputacionPropia) {
    this.imputacionPropia = imputacionPropia;
  }

  // >@OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0movimientoecc")
  // public List<Tmct0anotacionecc> getTmct0anotacioneccs() {
  // return this.tmct0anotacioneccs;
  // }
  //
  // public void setTmct0anotacioneccs(List<Tmct0anotacionecc>
  // tmct0anotacioneccs) {
  // this.tmct0anotacioneccs = tmct0anotacioneccs;
  // }

  @Column(name = "ISIN", length = 12)
  public String getIsin() {
    return isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  @Column(name = "SENTIDO")
  public Character getSentido() {
    return sentido;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  @Column(name = "ENVIADOCUENTAVIRTUAL")
  public Character getEnviadocuentavirtual() {
    return enviadocuentavirtual;
  }

  public void setEnviadocuentavirtual(Character enviadocuentavirtual) {
    this.enviadocuentavirtual = enviadocuentavirtual;
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0movimientoecc")
  // public List<Tmct0cancelacionecc> getTmct0cancelacioneccs() {
  // return this.tmct0cancelacioneccs;
  // }
  //
  // public void setTmct0cancelacioneccs(
  // List<Tmct0cancelacionecc> tmct0cancelacioneccs) {
  // this.tmct0cancelacioneccs = tmct0cancelacioneccs;
  // }
  //
  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0movimientoecc")
  // public List<Tmct0histMovimientoecc> getTmct0histMovimientoeccs() {
  // return this.tmct0histMovimientoeccs;
  // }
  //
  // public void setTmct0histMovimientoeccs(
  // List<Tmct0histMovimientoecc> tmct0histMovimientoeccs) {
  // this.tmct0histMovimientoeccs = tmct0histMovimientoeccs;
  // }

  // @Column( name = "ENVIADOCUENTAALIAS" )
  // public boolean isEnviadoCuentaAlias() {
  // return enviadoCuentaAlias;
  // }
  //
  // public void setEnviadoCuentaAlias( boolean enviadoCuentaAlias ) {
  // this.enviadoCuentaAlias = enviadoCuentaAlias;
  // }

  @Column(name = "CUENTA_COMPENSACION_DESTINO", length = 3)
  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

  @Column(name = "MNEMOTECNICO", length = 10, nullable = true)
  public String getMnemotecnico() {
    return mnemotecnico;
  }

  public void setMnemotecnico(String mnemotecnico) {
    this.mnemotecnico = mnemotecnico;
  }

  @Column(name = "MIEMBRO_COMPENSADOR_DESTINO", length = 4, nullable = true)
  public String getMiembroCompensadorDestino() {
    return miembroCompensadorDestino;
  }

  public void setMiembroCompensadorDestino(String miembroCompensadorDestino) {
    this.miembroCompensadorDestino = miembroCompensadorDestino;
  }

  @Column(name = "CD_REF_ASIGNACION", length = 18, nullable = true)
  public String getCodigoReferenciaAsignacion() {
    return codigoReferenciaAsignacion;
  }

  public void setCodigoReferenciaAsignacion(String codigoReferenciaAsignacion) {
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
  }

  @ManyToOne(optional = true, fetch = FetchType.LAZY)
  @JoinColumns(value = { @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = true),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = true),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = true) })
  public Tmct0alo getTmct0alo() {
    return tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0movimientoecc", cascade = { CascadeType.PERSIST })
  public List<Tmct0movimientooperacionnuevaecc> getTmct0movimientooperacionnuevaeccs() {
    return tmct0movimientooperacionnuevaeccs;
  }

  public void setTmct0movimientooperacionnuevaeccs(
      List<Tmct0movimientooperacionnuevaecc> tmct0movimientooperacionnuevaeccs) {
    this.tmct0movimientooperacionnuevaeccs = tmct0movimientooperacionnuevaeccs;
  }

  @Transient
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  @Column(name = "CDESTADOASIG", nullable = true)
  public Integer getCdestadoasig() {
    return cdestadoasig;
  }

  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

}
