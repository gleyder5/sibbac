package sibbac.business.wrappers.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_CAMARA_COMPENSACION")
public class Tmct0CamaraCompensacion implements Serializable {

  /**
   * Número de serie generado automáticamente
   */
  private static final long serialVersionUID = 764436424892060805L;
  private Long idCamaraCompensacion;
  private String cdCodigo;
  private String nbDescripcion;
  private String codigoLei;

  public Tmct0CamaraCompensacion() {
  }

  public Tmct0CamaraCompensacion(Long idCamaraCompensacion, String cdCodigo) {
    this.idCamaraCompensacion = idCamaraCompensacion;
    this.cdCodigo = cdCodigo;
  }

  public Tmct0CamaraCompensacion(Long idCamaraCompensacion, String cdCodigo, String nbDescripcion) {
    this.idCamaraCompensacion = idCamaraCompensacion;
    this.cdCodigo = cdCodigo;
    this.nbDescripcion = nbDescripcion;
  }

  @Id
  @Column(name = "ID_CAMARA_COMPENSACION", unique = true, nullable = false, length = 8)
  public Long getIdCamaraCompensacion() {
    return this.idCamaraCompensacion;
  }

  public void setIdCamaraCompensacion(Long idCamaraCompensacion) {
    this.idCamaraCompensacion = idCamaraCompensacion;
  }

  @Column(name = "CD_CODIGO", unique = true, nullable = false, length = 11)
  public String getCdCodigo() {
    return this.cdCodigo;
  }

  public void setCdCodigo(String cdCodigo) {
    this.cdCodigo = cdCodigo;
  }

  @Column(name = "NB_DESCRIPCION")
  public String getNbDescripcion() {
    return this.nbDescripcion;
  }

  public void setNbDescripcion(String nbDescripcion) {
    this.nbDescripcion = nbDescripcion;
  }

  @Column(name = "CODIGO_LEI", length = 20)
  public String getCodigoLei() {
    return codigoLei;
  }

  public void setCodigoLei(String codigoLei) {
    this.codigoLei = codigoLei;
  }

  @Override
  public String toString() {
    return "Tmct0CamaraCompensacion [idCamaraCompensacion=" + idCamaraCompensacion + ", cdCodigo=" + cdCodigo
        + ", nbDescripcion=" + nbDescripcion + "]";
  }

}
