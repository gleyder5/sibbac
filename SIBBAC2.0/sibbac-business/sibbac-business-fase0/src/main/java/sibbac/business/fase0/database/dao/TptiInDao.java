package sibbac.business.fase0.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.TptiInDTO;
import sibbac.business.fase0.database.model.TptiIn;

@Repository
public interface TptiInDao extends JpaRepository<TptiIn, Long> {

  @Query("select new sibbac.business.fase0.database.dto.TptiInDTO(t.id, t.message) " + "from TptiIn t "
      + "where t.messageType = 'AN' and t.processed = 'S' " + "and (t.withoutError IS NULL or t.withoutError = 'N')")
  public List<TptiInDTO> findAllTpTiInSinProcesar();

  @Query("select new sibbac.business.fase0.database.dto.TptiInDTO(t.id, t.message) " + "from TptiIn t "
      + "where t.messageType = 'AN' and t.processed = 'S' and (t.withoutError IS NULL or t.withoutError = 'N')"
      + "and t.audtFechaCambio between :fechadesde and :fechahasta")
  public List<TptiInDTO> findAllTpTiInSinProcesar(@Param("fechadesde") Date fechadesde,
      @Param("fechahasta") Date fechahasta);

  @Query("select new sibbac.business.fase0.database.dto.TptiInDTO(t.messageType, t.messageDate) "
      + "from TptiIn t where t.processed = 'N' and t.messageType in (:lmessageTypes)  "
      + " and t.scriptDividend = :scriptDividend "
      + "group by t.messageType, t.messageDate order by t.messageDate asc, t.messageType asc ")
  List<TptiInDTO> findAllDistinctMessageTypeAndMessageDateByMessageTypesAndNotProcessedAndScriptDividend(
      @Param("lmessageTypes") List<String> messageTypes, @Param("scriptDividend") Character scriptDividend);

  @Query("select new sibbac.business.fase0.database.dto.TptiInDTO(t.messageType, t.messageDate) "
      + "from TptiIn t where t.processed = 'N' and t.messageType not in (:lmessageTypes) "
      + " and t.scriptDividend = :scriptDividend "
      + "group by t.messageType, t.messageDate order by t.messageDate asc, t.messageType asc ")
  List<TptiInDTO> findAllDistinctMessageTypeAndMessageDateByNotInMessageTypesAndNotProcessedAndScriptDividend(
      @Param("lmessageTypes") List<String> messageTypes, @Param("scriptDividend") Character scriptDividend);

  @Query("select t.id from TptiIn t where t.messageDate = :messageDate and t.messageType = :messageType and t.scriptDividend = :scriptDividend "
      + "and t.processed = 'N' order by t.id asc")
  List<Long> findAllIdsByMessageDateAndMessageTypeNotProcessedAndScriptDividend(@Param("messageDate") Date messageDate,
      @Param("messageType") String messageType, @Param("scriptDividend") Character scriptDividend);

  @Query("select t.id from TptiIn t where t.processed = 'S' and ( t.withoutError IS NULL or t.withoutError = 'S' ) and t.scriptDividend = :scriptDividend "
      + " order by t.id asc")
  List<Long> findAllIdsByProcessedAndWithoutError(@Param("scriptDividend") Character scriptDividend);

  @Query("select t.id from TptiIn t where t.messageDate <= :messageDate and t.processed = 'S' and t.withoutError = 'N'"
      + " and t.scriptDividend = :scriptDividend order by t.id asc")
  List<Long> findAllIdsByProcessedAndWithErrorAndSctiptDividend(@Param("messageDate") Date messageDate,
      @Param("scriptDividend") Character scriptDividend);

  @Query("select t.groupData from TptiIn t where t.messageDate = :messageDate and t.messageType = :messageType "
      + "and t.processed = 'N' group by t.groupData order by t.groupData asc ")
  List<String> findAllGroupDataByMessageDateAndMessageTypeNotProcessed(@Param("messageDate") Date messageDate,
      @Param("messageType") String messageType);

  @Query("select t.id from TptiIn t where t.groupData = :groupData and t.messageDate = :messageDate and t.processed = 'N' and t.messageType = :messageType "
      + " and t.scriptDividend = :scriptDividend  order by t.messageDate asc, t.messageHour asc, t.id asc ")
  List<Long> findAllIdsByGroupDataAndMessageDateAndMessageTypeAndNotProcessedAndScriptDividend(
      @Param("groupData") String groupData, @Param("messageDate") Date messageDate,
      @Param("messageType") String messageType, @Param("scriptDividend") Character scriptDividend);

  @Query("select t from TptiIn t where t.groupData = :groupData and t.processed = 'N' "
      + "order by t.messageDate asc, t.messageHour asc, t.id asc ")
  List<TptiIn> findAllByGroupDataAndNotProcessed(@Param("groupData") String groupData);

}// public interface TptiInDao extends JpaRepository< TptiIn , Long> {
