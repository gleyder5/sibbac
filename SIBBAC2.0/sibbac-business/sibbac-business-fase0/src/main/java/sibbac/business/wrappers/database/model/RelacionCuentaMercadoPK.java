package sibbac.business.wrappers.database.model;


import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Embeddable
public class RelacionCuentaMercadoPK implements Serializable {

	@ManyToOne
	@JoinColumn( name = "ID_MERCADO", referencedColumnName = "id", nullable=false )
	private Tmct0Mercado			idMercado;
	@ManyToOne
	@JoinColumn( name = "ID_CUENTA_LIQUIDACION", referencedColumnName = "ID", nullable=false )
	private Tmct0CuentaLiquidacion	idCtaLiquidacion;

	public RelacionCuentaMercadoPK() {
		//
	}

	public RelacionCuentaMercadoPK( Tmct0Mercado mercado, Tmct0CuentaLiquidacion cta ) {
		this.idMercado = mercado;
		this.idCtaLiquidacion = cta;
	}

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	@Override
	public boolean equals( Object other ) {
		if ( this == other )
			return true;
		if ( !( other instanceof RelacionCuentaMercadoPK ) )
			return false;
		RelacionCuentaMercadoPK castOther = ( RelacionCuentaMercadoPK ) other;
		return idCtaLiquidacion.getId() == ( castOther.idCtaLiquidacion.getId() ) && idMercado.getId() == castOther.idMercado.getId();
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idCtaLiquidacion.hashCode();
		hash = hash * prime + this.idMercado.hashCode();
		return hash;
	}

	public Tmct0Mercado getIdMercado() {
		return idMercado;
	}

	public void setIdMercado( Tmct0Mercado idMercado ) {
		this.idMercado = idMercado;
	}

	public Tmct0CuentaLiquidacion getIdCtaLiquidacion() {
		return idCtaLiquidacion;
	}

	public void setIdCtaLiquidacion( Tmct0CuentaLiquidacion idCtaLiquidacion ) {
		this.idCtaLiquidacion = idCtaLiquidacion;
	}

}
