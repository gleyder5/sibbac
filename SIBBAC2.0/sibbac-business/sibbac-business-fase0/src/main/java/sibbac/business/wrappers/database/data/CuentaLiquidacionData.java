package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.model.RelacionCuentaMercado;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;
import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;

public class CuentaLiquidacionData implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory
	    .getLogger(CuentaLiquidacionData.class.getName());

    private long id;
    private EntidadRegistroData entidadRegistro;
    private long idMercadoContratacion;
    private String cdCodigo;
    private long numCuentaER;
    private boolean esCuentaPropia;
    private Tmct0TipoEr tipoER;
    private Tmct0TipoNeteo tipoNeteoTitulo;
    private Tmct0TipoNeteo tipoNeteoEfectivo;
    private long frecuenciaEnvioExtracto;
    private String tipoFicheroConciliacion;
    private TipoCuentaConciliacionData tipoCuentaConciliacion;
    private List<RelacionCuentaMercadoData> relacionCuentaMercadoDataList = new LinkedList<RelacionCuentaMercadoData>();
    private List<CuentaDeCompensacionData> cuentaDeCompensacionList = new LinkedList<CuentaDeCompensacionData>();
    private SistemaLiquidacionData sistemaLiquidacion;
    private String cuentaIberclear;
    
    public CuentaLiquidacionData() {
	// default constructor;
    }

    public CuentaLiquidacionData(Tmct0CuentaLiquidacion cl) {

	this.id = cl.getId();
	this.entidadRegistro = new EntidadRegistroData(cl.getEntidadRegistro());
	this.tipoER = (entidadRegistro != null) ? entidadRegistro.getTipoer()
		: null;
	this.idMercadoContratacion = cl.getIdMercadoContratacion();
	this.cdCodigo = cl.getCdCodigo();
	this.numCuentaER = cl.getNumCuentaER();
	this.esCuentaPropia = cl.isEsCuentaPropia();
	this.tipoNeteoTitulo = cl.getTipoNeteoTitulo();
	this.tipoNeteoEfectivo = cl.getTipoNeteoEfectivo();
	this.frecuenciaEnvioExtracto = cl.getFrecuenciaEnvioExtracto();
	this.tipoFicheroConciliacion = cl.getTipoFicheroConciliacion();
	this.cuentaIberclear = cl.getCuentaIberclear();
	//
	if(cl.getSistemaLiquidacion() != null){
	    this.sistemaLiquidacion = new SistemaLiquidacionData(cl.getSistemaLiquidacion());
	}
	this.tipoCuentaConciliacion = new TipoCuentaConciliacionData();
	if (cl.getTipoCuentaConciliacion() != null) {
	    this.tipoCuentaConciliacion.setId(cl.getTipoCuentaConciliacion()
		    .getId());
	    this.tipoCuentaConciliacion.setName(cl.getTipoCuentaConciliacion()
		    .getName());
	}
	extractCuentasMercados(cl);
	extractCuentasDeCompensacion(cl);
	
    }

    private void extractCuentasDeCompensacion(Tmct0CuentaLiquidacion cl) {
	try {
	    if (cl.getTmct0CuentasDeCompensacionList() != null
		    && cl.getTmct0CuentasDeCompensacionList().size() > 0) {
		for (Tmct0CuentasDeCompensacion cc : cl
			.getTmct0CuentasDeCompensacionList()) {
		    CuentaDeCompensacionData ccData = new CuentaDeCompensacionData(
			    cc);
		    this.cuentaDeCompensacionList.add(ccData);
		}
	    }
	} catch (PersistenceException ex) {
	    LOG.error(ex.getMessage(), ex);
	}
    }

    private void extractCuentasMercados(Tmct0CuentaLiquidacion cl) {
	try {
	    if (cl.getRelacionCuentaMercadoList() != null
		    && cl.getRelacionCuentaMercadoList().size() > 0) {
		for (RelacionCuentaMercado rel : cl
			.getRelacionCuentaMercadoList()) {
		    RelacionCuentaMercadoData relData = new RelacionCuentaMercadoData(
			    rel);
		    this.relacionCuentaMercadoDataList.add(relData);
		}
	    }
	} catch (PersistenceException ex) {
	    LOG.error(ex.getMessage(), ex);
	}
    }

    public CuentaLiquidacionData entityToData(Tmct0CuentaLiquidacion cl) {

	this.id = cl.getId();
	this.entidadRegistro = new EntidadRegistroData(cl.getEntidadRegistro());
	this.tipoER = (entidadRegistro != null) ? entidadRegistro.getTipoer()
		: null;
	this.idMercadoContratacion = cl.getIdMercadoContratacion();
	this.cdCodigo = cl.getCdCodigo();
	this.numCuentaER = cl.getNumCuentaER();
	this.esCuentaPropia = cl.isEsCuentaPropia();
	this.tipoNeteoTitulo = cl.getTipoNeteoTitulo();
	this.tipoNeteoEfectivo = cl.getTipoNeteoEfectivo();
	this.frecuenciaEnvioExtracto = cl.getFrecuenciaEnvioExtracto();
	this.tipoFicheroConciliacion = cl.getTipoFicheroConciliacion();
	this.tipoCuentaConciliacion = new TipoCuentaConciliacionData();
	if (cl.getTipoCuentaConciliacion() != null) {
	    this.tipoCuentaConciliacion.setId(cl.getTipoCuentaConciliacion()
		    .getId());
	    this.tipoCuentaConciliacion.setName(cl.getTipoCuentaConciliacion()
		    .getName());
	}
	extractCuentasMercados(cl);
	return this;

    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public EntidadRegistroData getEntidadRegistro() {
	return entidadRegistro;
    }

    public void setEntidadRegistro(EntidadRegistroData entidadRegistro) {
	this.entidadRegistro = entidadRegistro;
    }

    public Tmct0TipoEr getTipoER() {
	return tipoER;
    }

    public void setTipoER(Tmct0TipoEr tipoER) {
	this.tipoER = tipoER;
    }

    public long getIdMercadoContratacion() {
	return idMercadoContratacion;
    }

    public void setIdMercadoContratacion(long idMercadoContratacion) {
	this.idMercadoContratacion = idMercadoContratacion;
    }

    public String getCdCodigo() {
	return cdCodigo;
    }

    public void setCdCodigo(String cdCodigo) {
	this.cdCodigo = cdCodigo;
    }

    public long getNumCuentaER() {
	return numCuentaER;
    }

    public void setNumCuentaER(long numCuentaER) {
	this.numCuentaER = numCuentaER;
    }

    public boolean isEsCuentaPropia() {
	return esCuentaPropia;
    }

    public void setEsCuentaPropia(boolean esCuentaPropia) {
	this.esCuentaPropia = esCuentaPropia;
    }

    public Tmct0TipoNeteo getTipoNeteoTitulo() {
	return tipoNeteoTitulo;
    }

    public void setTipoNeteoTitulo(Tmct0TipoNeteo tipoNeteoTitulo) {
	this.tipoNeteoTitulo = tipoNeteoTitulo;
    }

    public Tmct0TipoNeteo getTipoNeteoEfectivo() {
	return tipoNeteoEfectivo;
    }

    public void setTipoNeteoEfectivo(Tmct0TipoNeteo tipoNeteoEfectivo) {
	this.tipoNeteoEfectivo = tipoNeteoEfectivo;
    }

    public long getFrecuenciaEnvioExtracto() {
	return frecuenciaEnvioExtracto;
    }

    public void setFrecuenciaEnvioExtracto(long frecuenciaEnvioExtracto) {
	this.frecuenciaEnvioExtracto = frecuenciaEnvioExtracto;
    }

    public String getTipoFicheroConciliacion() {
	return tipoFicheroConciliacion;
    }

    public void setTipoFicheroConciliacion(String tipoFicheroConciliacion) {
	this.tipoFicheroConciliacion = tipoFicheroConciliacion;
    }

    public TipoCuentaConciliacionData getTipoCuentaConciliacion() {
	return tipoCuentaConciliacion;
    }

    public void setTipoCuentaConciliacion(
	    TipoCuentaConciliacionData tipoCuentaConciliacion) {
	this.tipoCuentaConciliacion = tipoCuentaConciliacion;
    }

    public List<RelacionCuentaMercadoData> getRelacionCuentaMercadoDataList() {
	return relacionCuentaMercadoDataList;
    }

    public void setRelacionCuentaMercadoDataList(
	    List<RelacionCuentaMercadoData> relacionCuentaMercadoDataList) {
	this.relacionCuentaMercadoDataList = relacionCuentaMercadoDataList;
    }

    public List<CuentaDeCompensacionData> getCuentaDeCompensacionList() {
	return cuentaDeCompensacionList;
    }

    public void setCuentaDeCompensacionList(
	    List<CuentaDeCompensacionData> cuentaDeCompensacionList) {
	this.cuentaDeCompensacionList = cuentaDeCompensacionList;
    }

    public SistemaLiquidacionData getSistemaLiquidacion() {
        return sistemaLiquidacion;
    }

    public void setSistemaLiquidacion(SistemaLiquidacionData sistemaLiquidacion) {
        this.sistemaLiquidacion = sistemaLiquidacion;
    }

    public String getCuentaIberclear() {
        return cuentaIberclear;
    }

    public void setCuentaIberclear(String cuentaIberclear) {
        this.cuentaIberclear = cuentaIberclear;
    }

}
