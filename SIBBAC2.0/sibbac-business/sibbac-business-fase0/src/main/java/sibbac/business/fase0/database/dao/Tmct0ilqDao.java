package sibbac.business.fase0.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0ilq;
import sibbac.business.fase0.database.model.Tmct0ilqId;

@Repository
public interface Tmct0ilqDao extends JpaRepository<Tmct0ilq, Tmct0ilqId> {

  @Query("SELECT i FROM Tmct0ilq i WHERE i.id.cdaliass = :cdaliass AND i.id.cdsubcta = :cdsubcta "
         + " AND i.id.centro = :centro AND i.id.cdmercad = :cdmercad AND i.fhinicio <= :fecha AND i.fhfinal > :fecha order by i.id.numsec DESC ")
  List<Tmct0ilq> findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(@Param("cdaliass") String cdaliass,
                                                                                                    @Param("cdsubcta") String cdsubcta,
                                                                                                    @Param("centro") String centro,
                                                                                                    @Param("cdmercad") String cdmercad,
                                                                                                    @Param("fecha") BigDecimal fecha);

  @Query("SELECT i FROM Tmct0ilq i WHERE i.id.cdaliass = :cdaliass AND i.id.cdsubcta = :cdsubcta"
         + " AND i.id.centro = :centro AND i.id.cdmercad = :cdmercad AND i.fhfinal = :fhfinal order by i.id.numsec DESC ")
  List<Tmct0ilq> findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndFhfinal(@Param("cdaliass") String cdaliass,
                                                                                    @Param("cdsubcta") String cdsubcta,
                                                                                    @Param("centro") String centro,
                                                                                    @Param("cdmercad") String cdmercad,
                                                                                    @Param("fhfinal") BigDecimal fhfinal);

}
