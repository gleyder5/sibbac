package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0AFI")
public class Tmct0afi implements java.io.Serializable {

  private static final long serialVersionUID = 7435988567955368916L;
  private Tmct0afiId id;
  private Tmct0alo tmct0alo;
  private String tpdomici;
  private String nbdomici;
  private String nudomici;
  private String nbciudad;
  private String nbprovin;
  private String cdpostal;
  private String cddepais;
  private String nudnicif;
  private String nbclient;
  private String nbclient1;
  private String nbclient2;
  private Character tptiprep;
  private String cdnactit;
  private Date fhdenvio;
  private String idproced;
  private Character cdinacti;
  private Date fhinacti;
  private String cdusuina;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuoprout;
  private Short nupareje;
  private Short nusecnbr;
  private Character cdenvaud;
  private Character tpfisjur;
  private Short cdbloque;
  private String cdddebic;
  private Character tpidenti;
  private Character tpsocied;
  private Character tpnactit;
  private String rftitaud;
  private String cdholder;
  private String cdreferenciatitular;
  private Character enviadopti;
  private Date fechaenvioti;
  private BigDecimal particip = BigDecimal.ZERO;
  private Character cdestado;
  private String ccv;

  private Date fechaNacimiento;

  public Tmct0afi() {
  }

  public Tmct0afi(Tmct0afiId id, Tmct0alo tmct0alo, String tpdomici, String nbdomici, String nudomici, String nbciudad,
      String nbprovin, String cdpostal, String cddepais, String nudnicif, String nbclient, String nbclient1,
      String nbclient2, Character tptiprep, String cdnactit, String idproced, Character cdinacti, String cdusuina,
      String cdusuaud, Date fhaudit, Integer nuoprout, Short nupareje, Short nusecnbr, Character cdenvaud,
      Character tpfisjur, Short cdbloque, String cdddebic, Character tpidenti, Character tpsocied, Character tpnactit) {
    this.id = id;
    this.tmct0alo = tmct0alo;
    this.tpdomici = tpdomici;
    this.nbdomici = nbdomici;
    this.nudomici = nudomici;
    this.nbciudad = nbciudad;
    this.nbprovin = nbprovin;
    this.cdpostal = cdpostal;
    this.cddepais = cddepais;
    this.nudnicif = nudnicif;
    this.nbclient = nbclient;
    this.nbclient1 = nbclient1;
    this.nbclient2 = nbclient2;
    this.tptiprep = tptiprep;
    this.cdnactit = cdnactit;
    this.idproced = idproced;
    this.cdinacti = cdinacti;
    this.cdusuina = cdusuina;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
    this.nusecnbr = nusecnbr;
    this.cdenvaud = cdenvaud;
    this.tpfisjur = tpfisjur;
    this.cdbloque = cdbloque;
    this.cdddebic = cdddebic;
    this.tpidenti = tpidenti;
    this.tpsocied = tpsocied;
    this.tpnactit = tpnactit;
    if (cdddebic == null) {
      this.cdddebic = nudnicif;
    }
    if (cdholder == null) {
      this.cdholder = nudnicif;
    }
    if (rftitaud == null) {
      this.rftitaud = nudnicif;
    }

  }

  public Tmct0afi(Tmct0afiId id, Tmct0alo tmct0alo, String tpdomici, String nbdomici, String nudomici, String nbciudad,
      String nbprovin, String cdpostal, String cddepais, String nudnicif, String nbclient, String nbclient1,
      String nbclient2, Character tptiprep, String cdnactit, Date fhdenvio, String idproced, Character cdinacti,
      Date fhinacti, String cdusuina, String cdusuaud, Date fhaudit, Integer nuoprout, Short nupareje, Short nusecnbr,
      Character cdenvaud, Character tpfisjur, Short cdbloque, String cdddebic, Character tpidenti, Character tpsocied,
      Character tpnactit, String rftitaud, String cdholder, String cdreferenciatitular, Character enviadopti,
      Date fechaenvioti, BigDecimal particip, Character cdestado, String ccv) {
    this.id = id;
    this.tmct0alo = tmct0alo;
    this.tpdomici = tpdomici;
    this.nbdomici = nbdomici;
    this.nudomici = nudomici;
    this.nbciudad = nbciudad;
    this.nbprovin = nbprovin;
    this.cdpostal = cdpostal;
    this.cddepais = cddepais;
    this.nudnicif = nudnicif;
    this.nbclient = nbclient;
    this.nbclient1 = nbclient1;
    this.nbclient2 = nbclient2;
    this.tptiprep = tptiprep;
    this.cdnactit = cdnactit;
    this.fhdenvio = fhdenvio;
    this.idproced = idproced;
    this.cdinacti = cdinacti;
    this.fhinacti = fhinacti;
    this.cdusuina = cdusuina;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
    this.nusecnbr = nusecnbr;
    this.cdenvaud = cdenvaud;
    this.tpfisjur = tpfisjur;
    this.cdbloque = cdbloque;
    this.cdddebic = cdddebic;
    this.tpidenti = tpidenti;
    this.tpsocied = tpsocied;
    this.tpnactit = tpnactit;
    this.rftitaud = rftitaud;
    this.cdholder = cdholder;
    this.cdreferenciatitular = cdreferenciatitular;
    this.enviadopti = enviadopti;
    this.fechaenvioti = fechaenvioti;
    this.particip = particip;
    this.cdestado = cdestado;
    this.ccv = ccv;
    if (cdddebic == null) {
      this.cdddebic = nudnicif;
    }
    if (cdholder == null) {
      this.cdholder = nudnicif;
    }
    if (rftitaud == null) {
      this.rftitaud = nudnicif;
    }
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfliq", column = @Column(name = "NUCNFLIQ", nullable = false, precision = 4, scale = 0)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "NUSECUEN", nullable = false, precision = 20, scale = 0)) })
  public Tmct0afiId getId() {
    return this.id;
  }

  public void setId(Tmct0afiId id) {
    this.id = id;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false) })
  public Tmct0alo getTmct0alo() {
    return this.tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  @Column(name = "TPDOMICI", nullable = false, length = 2)
  public String getTpdomici() {
    return this.tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  @Column(name = "NBDOMICI", nullable = false, length = 40)
  public String getNbdomici() {
    return this.nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  @Column(name = "NUDOMICI", nullable = false, length = 4)
  public String getNudomici() {
    return this.nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  @Column(name = "NBCIUDAD", nullable = false, length = 40)
  public String getNbciudad() {
    return this.nbciudad;
  }

  public void setNbciudad(String nbciudad) {
    this.nbciudad = nbciudad;
  }

  @Column(name = "NBPROVIN", nullable = false, length = 25)
  public String getNbprovin() {
    return this.nbprovin;
  }

  public void setNbprovin(String nbprovin) {
    this.nbprovin = nbprovin;
  }

  @Column(name = "CDPOSTAL", nullable = false, length = 5)
  public String getCdpostal() {
    return this.cdpostal;
  }

  public void setCdpostal(String cdpostal) {
    this.cdpostal = cdpostal;
  }

  @Column(name = "CDDEPAIS", nullable = false, length = 3)
  public String getCddepais() {
    return this.cddepais;
  }

  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  @Column(name = "NUDNICIF", nullable = false, length = 11)
  public String getNudnicif() {
    return this.nudnicif;
  }

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  @Column(name = "NBCLIENT", nullable = false, length = 30)
  public String getNbclient() {
    return this.nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  @Column(name = "NBCLIENT1", nullable = false, length = 50)
  public String getNbclient1() {
    return this.nbclient1;
  }

  public void setNbclient1(String nbclient1) {
    this.nbclient1 = nbclient1;
  }

  @Column(name = "NBCLIENT2", nullable = false, length = 30)
  public String getNbclient2() {
    return this.nbclient2;
  }

  public void setNbclient2(String nbclient2) {
    this.nbclient2 = nbclient2;
  }

  @Column(name = "TPTIPREP", nullable = false, length = 1)
  public Character getTptiprep() {
    return this.tptiprep;
  }

  public void setTptiprep(Character tptiprep) {
    this.tptiprep = tptiprep;
  }

  @Column(name = "CDNACTIT", nullable = false, length = 3)
  public String getCdnactit() {
    return this.cdnactit;
  }

  public void setCdnactit(String cdnactit) {
    this.cdnactit = cdnactit;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHDENVIO", nullable = true, length = 26)
  public Date getFhdenvio() {
    return this.fhdenvio;
  }

  public void setFhdenvio(Date fhdenvio) {
    this.fhdenvio = fhdenvio;
  }

  @Column(name = "IDPROCED", nullable = false, length = 20)
  public String getIdproced() {
    return this.idproced;
  }

  public void setIdproced(String idproced) {
    this.idproced = idproced;
  }

  @Column(name = "CDINACTI", nullable = false, length = 1)
  public Character getCdinacti() {
    return this.cdinacti;
  }

  public void setCdinacti(Character cdinacti) {
    this.cdinacti = cdinacti;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHINACTI", nullable = true, length = 26)
  public Date getFhinacti() {
    return this.fhinacti;
  }

  public void setFhinacti(Date fhinacti) {
    this.fhinacti = fhinacti;
  }

  @Column(name = "CDUSUINA", nullable = false, length = 10)
  public String getCdusuina() {
    return this.cdusuina;
  }

  public void setCdusuina(String cdusuina) {
    this.cdusuina = cdusuina;
  }

  @Column(name = "CDUSUAUD", nullable = false, length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", nullable = false, length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUOPROUT", nullable = false, precision = 9, scale = 0)
  public Integer getNuoprout() {
    return this.nuoprout;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  @Column(name = "NUPAREJE", nullable = false, precision = 4, scale = 0)
  public Short getNupareje() {
    return this.nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  @Column(name = "NUSECNBR", nullable = false, precision = 3, scale = 0)
  public Short getNusecnbr() {
    return this.nusecnbr;
  }

  public void setNusecnbr(Short nusecnbr) {
    this.nusecnbr = nusecnbr;
  }

  @Column(name = "CDENVAUD", nullable = false, length = 1)
  public Character getCdenvaud() {
    return this.cdenvaud;
  }

  public void setCdenvaud(Character cdenvaud) {
    this.cdenvaud = cdenvaud;
  }

  @Column(name = "TPFISJUR", nullable = false, length = 1)
  public Character getTpfisjur() {
    return this.tpfisjur;
  }

  public void setTpfisjur(Character tpfisjur) {
    this.tpfisjur = tpfisjur;
  }

  @Column(name = "CDBLOQUE", nullable = false, precision = 3, scale = 0)
  public Short getCdbloque() {
    return this.cdbloque;
  }

  public void setCdbloque(Short cdbloque) {
    this.cdbloque = cdbloque;
  }

  @Column(name = "CDDDEBIC", nullable = false, length = 11)
  public String getCdddebic() {
    return this.cdddebic;
  }

  public void setCdddebic(String cdddebic) {
    this.cdddebic = cdddebic;
  }

  @Column(name = "TPIDENTI", nullable = false, length = 1)
  public Character getTpidenti() {
    return this.tpidenti;
  }

  public void setTpidenti(Character tpidenti) {
    this.tpidenti = tpidenti;
  }

  @Column(name = "TPSOCIED", nullable = false, length = 1)
  public Character getTpsocied() {
    return this.tpsocied;
  }

  public void setTpsocied(Character tpsocied) {
    this.tpsocied = tpsocied;
  }

  @Column(name = "TPNACTIT", nullable = false, length = 1)
  public Character getTpnactit() {
    return this.tpnactit;
  }

  public void setTpnactit(Character tpnactit) {
    this.tpnactit = tpnactit;
  }

  @Column(name = "RFTITAUD", nullable = false, length = 16)
  public String getRftitaud() {
    return this.rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  @Column(name = "CDHOLDER", nullable = false, length = 40)
  public String getCdholder() {
    return this.cdholder;
  }

  public void setCdholder(String cdholder) {
    this.cdholder = cdholder;
  }

  @Column(name = "CDREFERENCIATITULAR", nullable = true, length = 20)
  public String getCdreferenciatitular() {
    return this.cdreferenciatitular;
  }

  public void setCdreferenciatitular(String cdreferenciatitular) {
    this.cdreferenciatitular = cdreferenciatitular;
  }

  @Column(name = "ENVIADOPTI", nullable = true, length = 1)
  public Character getEnviadopti() {
    return this.enviadopti;
  }

  public void setEnviadopti(Character enviadopti) {
    this.enviadopti = enviadopti;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHAENVIOTI", nullable = true, length = 10)
  public Date getFechaenvioti() {
    return this.fechaenvioti;
  }

  public void setFechaenvioti(Date fechaenvioti) {
    this.fechaenvioti = fechaenvioti;
  }

  @Column(name = "PARTICIP", nullable = true, precision = 5, scale = 2)
  public BigDecimal getParticip() {
    return this.particip;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  @Column(name = "CDESTADO", nullable = true, length = 1)
  public Character getCdestado() {
    return this.cdestado;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  @Column(name = "CCV", nullable = true, length = 20)
  public String getCcv() {
    return this.ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_NACIMIENTO", nullable = true)
  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

}



