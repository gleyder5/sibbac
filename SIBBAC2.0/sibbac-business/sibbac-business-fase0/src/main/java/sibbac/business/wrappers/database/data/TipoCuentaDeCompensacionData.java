package sibbac.business.wrappers.database.data;

import java.io.Serializable;

import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;

public class TipoCuentaDeCompensacionData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private String descripcion;
    private String codigo;

    public TipoCuentaDeCompensacionData(Tmct0TipoCuentaDeCompensacion tc){
	this.id = tc.getIdTipoCuenta();
	this.codigo = tc.getCdCodigo();
	this.descripcion = tc.getNbDescripcion();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
}
