package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.AlcOrdenes } . Entity:
 * "AlcOrdenes".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = WRAPPERS.ALC_ORDEN_FACTURA_DETALLE )
@Entity
@Component
@XmlRootElement
@Audited
public class AlcOrdenDetalleFactura extends ATable< AlcOrdenDetalleFactura > implements java.io.Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 9081239442995540410L;

	@ManyToOne( cascade = CascadeType.PERSIST )
	@JoinColumn( name = "ID_ALC_ORDEN", referencedColumnName = "ID" )
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private AlcOrdenes			alcOrden;

	@ManyToOne
	@JoinColumn( name = "ID_FACTURA_DETALLE", referencedColumnName = "ID" )
	private FacturaDetalle		facturaDetalle;

	@Column( name = "IMFINSVB", precision = 18, scale = 4 )
	private BigDecimal			imfinsvb;

	// ----------------------------------------------- Bean
	// Constructors---------------------

	public AlcOrdenDetalleFactura() {
	}

	/**
	 * @return the alcOrden
	 */
	public AlcOrdenes getAlcOrden() {
		return alcOrden;
	}

	/**
	 * @param alcOrden
	 *            the alcOrden to set
	 */
	public void setAlcOrden( AlcOrdenes alcOrden ) {
		this.alcOrden = alcOrden;
	}

	/**
	 * @return the facturaDetalle
	 */
	public FacturaDetalle getFacturaDetalle() {
		return facturaDetalle;
	}

	/**
	 * @param facturaDetalle
	 *            the facturaDetalle to set
	 */
	public void setFacturaDetalle( FacturaDetalle facturaDetalle ) {
		this.facturaDetalle = facturaDetalle;
	}

	/**
	 * @return the imfinsvb
	 */
	public BigDecimal getImfinsvb() {
		return imfinsvb;
	}

	/**
	 * @param imfinsvb
	 *            the imfinsvb to set
	 */
	public void setImfinsvb( BigDecimal imfinsvb ) {
		this.imfinsvb = imfinsvb;
	}

//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see java.lang.Object#hashCode()
//	 */
//	@Override
//	public int hashCode() {
//		final HashCodeBuilder hcb = new HashCodeBuilder();
//		hcb.append( alcOrden );
//		hcb.append( ( facturaDetalle != null ) ? facturaDetalle.getFacturaSugerida() : -1 );
//		return hcb.toHashCode();
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 */
//	@Override
//	public boolean equals( Object obj ) {
//		final AlcOrdenDetalleFactura alcOrdenDetalleFactura = ( AlcOrdenDetalleFactura ) obj;
//		final EqualsBuilder eqb = new EqualsBuilder();
//		eqb.append( this.getAlcOrden(), alcOrdenDetalleFactura.getAlcOrden() );
//		eqb.append( this.getFacturaDetalle().getFacturaSugerida(), alcOrdenDetalleFactura.getFacturaDetalle().getFacturaSugerida() );
//		return super.equals( obj );
//	}
}
