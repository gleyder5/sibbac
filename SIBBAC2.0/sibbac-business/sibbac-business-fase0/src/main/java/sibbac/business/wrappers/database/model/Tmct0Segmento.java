package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "TMCT0_SEGMENTO" )
public class Tmct0Segmento implements java.io.Serializable {
	

	private static final long serialVersionUID = -4818371378107336679L;
	private int	id_segmento;	
	private String nbDescripcion;
	private String cd_codigo;

	
	public Tmct0Segmento() {}

	
	public Tmct0Segmento(int id_segmento, String nb_descripcion,
			String cd_codigo) {
		super();
		this.id_segmento = id_segmento;
		this.nbDescripcion = nbDescripcion;
		this.cd_codigo = cd_codigo;
	}

	@Id
	@Column( name = "ID_SEGMENTO", unique = true, nullable = false, length = 8 )
	public int getId_segmento() {
		return id_segmento;
	}


	public void setId_segmento(int id_segmento) {
		this.id_segmento = id_segmento;
	}

	@Column( name = "NB_DESCRIPCION", length = 255 )
	public String getNbDescripcion() {
		return nbDescripcion;
	}


	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}

	@Column( name = "CD_CODIGO", nullable = false, length = 4 )
	public String getCd_codigo() {
		return cd_codigo;
	}


	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
