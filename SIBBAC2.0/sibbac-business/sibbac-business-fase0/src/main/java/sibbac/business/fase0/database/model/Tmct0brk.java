package sibbac.business.fase0.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0BRK")
public class Tmct0brk implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -6074001949443029012L;
  private Tmct0brkId id;
  private String nbbroker;
  private Character inforcon;
  private Character insenmod;
  private Character insenfla;
  private String cdusufic;
  private String cdpwdfic;
  private String cdpatfic;
  private Character cdopefic;
  private BigDecimal immargen;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuversion;
  private String cocenliq;
  private String cdidenti;
  private String cdbicid;
  private String nameid;
  private char fidessa;
  private int fhsendfi;
  private Integer cdestadoasig;
  private Integer fhinicio;
  private Integer fhfinal;
  private String auxiliarcontable;
  
  private String codigoLei;
  private char reporteMifid;

  // private Set<Tmct0bri> tmct0bris = new HashSet<Tmct0bri>(0);

  public Tmct0brk() {
  }

  public Tmct0brk(Tmct0brkId id, String cdidenti, String cdbicid, String nameid, char fidessa, int fhsendfi) {
    this.id = id;
    this.cdidenti = cdidenti;
    this.cdbicid = cdbicid;
    this.nameid = nameid;
    this.fidessa = fidessa;
    this.fhsendfi = fhsendfi;
  }

  public Tmct0brk(Tmct0brkId id,
                  String nbbroker,
                  Character inforcon,
                  Character insenmod,
                  Character insenfla,
                  String cdusufic,
                  String cdpwdfic,
                  String cdpatfic,
                  Character cdopefic,
                  BigDecimal immargen,
                  String cdusuaud,
                  Date fhaudit,
                  Integer nuversion,
                  String cocenliq,
                  String cdidenti,
                  String cdbicid,
                  String nameid,
                  char fidessa,
                  int fhsendfi,
                  Integer cdestadoasig,
                  Integer fhinicio,
                  Integer fhfinal,
                  String auxiliarcontable/* , Set<Tmct0bri> tmct0bris */) {
    this.id = id;
    this.nbbroker = nbbroker;
    this.inforcon = inforcon;
    this.insenmod = insenmod;
    this.insenfla = insenfla;
    this.cdusufic = cdusufic;
    this.cdpwdfic = cdpwdfic;
    this.cdpatfic = cdpatfic;
    this.cdopefic = cdopefic;
    this.immargen = immargen;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuversion = nuversion;
    this.cocenliq = cocenliq;
    this.cdidenti = cdidenti;
    this.cdbicid = cdbicid;
    this.nameid = nameid;
    this.fidessa = fidessa;
    this.fhsendfi = fhsendfi;
    this.cdestadoasig = cdestadoasig;
    this.fhinicio = fhinicio;
    this.fhfinal = fhfinal;
    this.auxiliarcontable = auxiliarcontable;
    // this.tmct0bris = tmct0bris;
  }

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER",
                                                                               nullable = false,
                                                                               length = 20)), @AttributeOverride(name = "numsec",
                                                                                                                 column = @Column(name = "NUMSEC",
                                                                                                                                  nullable = false,
                                                                                                                                  precision = 20))
  })
  public Tmct0brkId getId() {
    return this.id;
  }

  public void setId(Tmct0brkId id) {
    this.id = id;
  }

  @Column(name = "NBBROKER", length = 50)
  public String getNbbroker() {
    return this.nbbroker;
  }

  public void setNbbroker(String nbbroker) {
    this.nbbroker = nbbroker;
  }

  @Column(name = "INFORCON", length = 1)
  public Character getInforcon() {
    return this.inforcon;
  }

  public void setInforcon(Character inforcon) {
    this.inforcon = inforcon;
  }

  @Column(name = "INSENMOD", length = 1)
  public Character getInsenmod() {
    return this.insenmod;
  }

  public void setInsenmod(Character insenmod) {
    this.insenmod = insenmod;
  }

  @Column(name = "INSENFLA", length = 1)
  public Character getInsenfla() {
    return this.insenfla;
  }

  public void setInsenfla(Character insenfla) {
    this.insenfla = insenfla;
  }

  @Column(name = "CDUSUFIC", length = 10)
  public String getCdusufic() {
    return this.cdusufic;
  }

  public void setCdusufic(String cdusufic) {
    this.cdusufic = cdusufic;
  }

  @Column(name = "CDPWDFIC", length = 10)
  public String getCdpwdfic() {
    return this.cdpwdfic;
  }

  public void setCdpwdfic(String cdpwdfic) {
    this.cdpwdfic = cdpwdfic;
  }

  @Column(name = "CDPATFIC", length = 80)
  public String getCdpatfic() {
    return this.cdpatfic;
  }

  public void setCdpatfic(String cdpatfic) {
    this.cdpatfic = cdpatfic;
  }

  @Column(name = "CDOPEFIC", length = 1)
  public Character getCdopefic() {
    return this.cdopefic;
  }

  public void setCdopefic(Character cdopefic) {
    this.cdopefic = cdopefic;
  }

  @Column(name = "IMMARGEN", precision = 18, scale = 8)
  public BigDecimal getImmargen() {
    return this.immargen;
  }

  public void setImmargen(BigDecimal immargen) {
    this.immargen = immargen;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION", precision = 4)
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "COCENLIQ", length = 20)
  public String getCocenliq() {
    return this.cocenliq;
  }

  public void setCocenliq(String cocenliq) {
    this.cocenliq = cocenliq;
  }

  @Column(name = "CDIDENTI", nullable = false, length = 10)
  public String getCdidenti() {
    return this.cdidenti;
  }

  public void setCdidenti(String cdidenti) {
    this.cdidenti = cdidenti;
  }

  @Column(name = "CDBICID", nullable = false, length = 11)
  public String getCdbicid() {
    return this.cdbicid;
  }

  public void setCdbicid(String cdbicid) {
    this.cdbicid = cdbicid;
  }

  @Column(name = "NAMEID", nullable = false, length = 146)
  public String getNameid() {
    return this.nameid;
  }

  public void setNameid(String nameid) {
    this.nameid = nameid;
  }

  @Column(name = "FIDESSA", nullable = false, length = 1)
  public char getFidessa() {
    return this.fidessa;
  }

  public void setFidessa(char fidessa) {
    this.fidessa = fidessa;
  }

  @Column(name = "FHSENDFI", nullable = false, precision = 8)
  public int getFhsendfi() {
    return this.fhsendfi;
  }

  public void setFhsendfi(int fhsendfi) {
    this.fhsendfi = fhsendfi;
  }

  @Column(name = "CDESTADOASIG", precision = 4)
  public Integer getCdestadoasig() {
    return this.cdestadoasig;
  }

  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  @Column(name = "FHINICIO", precision = 8)
  public Integer getFhinicio() {
    return this.fhinicio;
  }

  public void setFhinicio(Integer fhinicio) {
    this.fhinicio = fhinicio;
  }

  @Column(name = "FHFINAL", precision = 8)
  public Integer getFhfinal() {
    return this.fhfinal;
  }

  public void setFhfinal(Integer fhfinal) {
    this.fhfinal = fhfinal;
  }

  @Column(name = "AUXILIARCONTABLE", length = 45)
  public String getAuxiliarcontable() {
    return this.auxiliarcontable;
  }

  public void setAuxiliarcontable(String auxiliarcontable) {
    this.auxiliarcontable = auxiliarcontable;
  }
  
  @Column(name = "CODIGO_LEI", length = 20)
  public String getCodigoLei() {
    return codigoLei;
  }

  public void setCodigoLei(String codigoLei) {
    this.codigoLei = codigoLei;
  }

  @Column(name = "REPORTE_MIFID")
  public char getReporteMifid() {
    return reporteMifid;
  }

  public void setReporteMifid(char reporteMifid) {
    this.reporteMifid = reporteMifid;
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0brk")
  // public Set<Tmct0bri> getTmct0bris() {
  // return this.tmct0bris;
  // }
  //
  // public void setTmct0bris(Set<Tmct0bri> tmct0bris) {
  // this.tmct0bris = tmct0bris;
  // }

}
