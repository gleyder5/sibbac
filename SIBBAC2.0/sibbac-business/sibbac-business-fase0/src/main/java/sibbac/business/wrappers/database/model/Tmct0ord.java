package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import sibbac.business.fase0.database.model.Tmct0estado;

@Entity
@Table(name = "TMCT0ORD")
public class Tmct0ord implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -7698165597554696386L;
  private String nuorden = "";
  private Tmct0estado tmct0estado;
  private Tmct0sta tmct0sta;
  private Character cdentdd = ' ';
  private String cdcanal = "";
  private Character cdcaptur = ' ';
  private String cdclente = "";
  private String cdcuenta = "";
  private String cdgesord = "";
  private String cdisin = "";
  private String nbvalors = "";
  private String cdorigen = "";
  private String tporigen = "";
  private Character cdtpoper = ' ';
  private Character cdtyp = ' ';
  private String cdvia = "";
  private String cdtipord = "";
  private Character cdclsmdo = ' ';
  private String dsobserv = "";
  private Character cdtpcamb = ' ';
  private BigDecimal imtpcamb = new BigDecimal(0.0);
  private String cdmoniso = "";
  private String cdtpmerc = "";
  private BigDecimal nutitord = new BigDecimal(0.0);
  private BigDecimal nutiteje = new BigDecimal(0.0);
  private BigDecimal imcbclie = new BigDecimal(0.0);
  private BigDecimal imcbmerc = new BigDecimal(0.0);
  private BigDecimal imlimite = new BigDecimal(0.0);
  private Date fealta = new Date();
  private Date hoalta = new Date();
  private Date fecontra = new Date();
  private Date feejecuc = new Date();
  private Date hoejecuc = new Date();
  private Date fevalide = new Date();
  private Date fevalor = new Date();
  private BigDecimal imcbntcl = new BigDecimal(0.0);
  private BigDecimal imnetobr = new BigDecimal(0.0);
  private BigDecimal imnetocl = new BigDecimal(0.0);
  private BigDecimal imntbreu = new BigDecimal(0.0);
  private BigDecimal imntcleu = new BigDecimal(0.0);
  private BigDecimal podevolu = new BigDecimal(0.0);
  private BigDecimal pofpacli = new BigDecimal(0.0);
  private BigDecimal imdevcdv = new BigDecimal(0.0);
  private BigDecimal imdevcli = new BigDecimal(0.0);
  private BigDecimal imfibrdv = new BigDecimal(0.0);
  private BigDecimal imfibreu = new BigDecimal(0.0);
  private BigDecimal imsvb = new BigDecimal(0.0);
  private BigDecimal imsvbeu = new BigDecimal(0.0);
  private BigDecimal imfpacdv = new BigDecimal(0.0);
  private BigDecimal imbensvb = new BigDecimal(0.0);
  private Character cdindgas = ' ';
  private Character cdindimp = ' ';
  private Character cdreside = ' ';
  private BigDecimal imbrmerc = new BigDecimal(0.0);
  private BigDecimal imbrmreu = new BigDecimal(0.0);
  private BigDecimal imgastos = new BigDecimal(0.0);
  private BigDecimal imgatdvs = new BigDecimal(0.0);
  private BigDecimal imimpdvs = new BigDecimal(0.0);
  private BigDecimal imimpeur = new BigDecimal(0.0);
  private Character xtdcm = ' ';
  private Character xtderech;
  private BigDecimal imcliqeu = new BigDecimal(0.0);
  private BigDecimal imcconeu = new BigDecimal(0.0);
  private BigDecimal imdereeu = new BigDecimal(0.0);
  private BigDecimal imnomval = new BigDecimal(0.0);
  private String cdusuari = "";
  private Integer nuoprout = 0;
  private String nureford = "";
  private Character cdwareho;
  private Integer nuejecnt = 0;
  private String rfparten = "";
  private String cdbroker = "";
  private String cdmercad = "";
  private String cdsucliq = "";
  private String nuctacli = "";
  private String cdusuaud = "";
  private Date fhaudit = new Date();
  private Integer nuversion = 0;
  private BigDecimal imgasliqdv = new BigDecimal(0.0);
  private BigDecimal imgasliqeu = new BigDecimal(0.0);
  private BigDecimal imgasgesdv = new BigDecimal(0.0);
  private BigDecimal imgasgeseu = new BigDecimal(0.0);
  private BigDecimal imgastotdv = new BigDecimal(0.0);
  private BigDecimal imgastoteu = new BigDecimal(0.0);
  private String cdexoner;
  private char isscrdiv = 'N';
  private String cdcleare;
  private String nif;
  private String contrprr;
  private String codprod;
  private String segmentclte;
  private String cdmodnego;
  private String ccv;
  private Integer cdestadotit = 0;
  private String reftitular;
  private String refadicional;

  private List<Tmct0cto> tmct0ctos = new ArrayList<Tmct0cto>();
  private List<Tmct0log> tmct0logs = new ArrayList<Tmct0log>();
  private List<Tmct0bok> tmct0boks = new ArrayList<Tmct0bok>();
  private List<Tmct0ordDynamicValues> dynamicValues = new ArrayList<Tmct0ordDynamicValues>();

  public Tmct0ord() {
  }

  public Tmct0ord(String nuorden, Tmct0sta tmct0sta, char isscrdiv) {
    this.nuorden = nuorden;
    this.tmct0sta = tmct0sta;
    this.isscrdiv = isscrdiv;
  }

  public Tmct0ord(String nuorden, Tmct0estado tmct0estado, Tmct0sta tmct0sta, Character cdentdd, String cdcanal,
      Character cdcaptur, String cdclente, String cdcuenta, String cdgesord, String cdisin, String nbvalors,
      String cdorigen, String tporigen, Character cdtpoper, Character cdtyp, String cdvia, String cdtipord,
      Character cdclsmdo, String dsobserv, Character cdtpcamb, BigDecimal imtpcamb, String cdmoniso, String cdtpmerc,
      BigDecimal nutitord, BigDecimal nutiteje, BigDecimal imcbclie, BigDecimal imcbmerc, BigDecimal imlimite,
      Date fealta, Date hoalta, Date fecontra, Date feejecuc, Date hoejecuc, Date fevalide, Date fevalor,
      BigDecimal imcbntcl, BigDecimal imnetobr, BigDecimal imnetocl, BigDecimal imntbreu, BigDecimal imntcleu,
      BigDecimal podevolu, BigDecimal pofpacli, BigDecimal imdevcdv, BigDecimal imdevcli, BigDecimal imfibrdv,
      BigDecimal imfibreu, BigDecimal imsvb, BigDecimal imsvbeu, BigDecimal imfpacdv, BigDecimal imbensvb,
      Character cdindgas, Character cdindimp, Character cdreside, BigDecimal imbrmerc, BigDecimal imbrmreu,
      BigDecimal imgastos, BigDecimal imgatdvs, BigDecimal imimpdvs, BigDecimal imimpeur, Character xtdcm,
      Character xtderech, BigDecimal imcliqeu, BigDecimal imcconeu, BigDecimal imdereeu, BigDecimal imnomval,
      String cdusuari, Integer nuoprout, String nureford, Character cdwareho, Integer nuejecnt, String rfparten,
      String cdbroker, String cdmercad, String cdsucliq, String nuctacli, String cdusuaud, Date fhaudit,
      Integer nuversion, BigDecimal imgasliqdv, BigDecimal imgasliqeu, BigDecimal imgasgesdv, BigDecimal imgasgeseu,
      BigDecimal imgastotdv, BigDecimal imgastoteu, String cdexoner, char isscrdiv, String cdcleare, String nif,
      String contrprr, String codprod, String segmentclte, String cdmodnego, String ccv, Integer cdestadotit,
      String reftitular, String refadicional, List<Tmct0cto> tmct0ctos, List<Tmct0log> tmct0logs,
      List<Tmct0bok> tmct0boks) {
    this.nuorden = nuorden;
    this.tmct0estado = tmct0estado;
    this.tmct0sta = tmct0sta;
    this.cdentdd = cdentdd;
    this.cdcanal = cdcanal;
    this.cdcaptur = cdcaptur;
    this.cdclente = cdclente;
    this.cdcuenta = cdcuenta;
    this.cdgesord = cdgesord;
    this.cdisin = cdisin;
    this.nbvalors = nbvalors;
    this.cdorigen = cdorigen;
    this.tporigen = tporigen;
    this.cdtpoper = cdtpoper;
    this.cdtyp = cdtyp;
    this.cdvia = cdvia;
    this.cdtipord = cdtipord;
    this.cdclsmdo = cdclsmdo;
    this.dsobserv = dsobserv;
    this.cdtpcamb = cdtpcamb;
    this.imtpcamb = imtpcamb;
    this.cdmoniso = cdmoniso;
    this.cdtpmerc = cdtpmerc;
    this.nutitord = nutitord;
    this.nutiteje = nutiteje;
    this.imcbclie = imcbclie;
    this.imcbmerc = imcbmerc;
    this.imlimite = imlimite;
    this.fealta = fealta;
    this.hoalta = hoalta;
    this.fecontra = fecontra;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.fevalide = fevalide;
    this.fevalor = fevalor;
    this.imcbntcl = imcbntcl;
    this.imnetobr = imnetobr;
    this.imnetocl = imnetocl;
    this.imntbreu = imntbreu;
    this.imntcleu = imntcleu;
    this.podevolu = podevolu;
    this.pofpacli = pofpacli;
    this.imdevcdv = imdevcdv;
    this.imdevcli = imdevcli;
    this.imfibrdv = imfibrdv;
    this.imfibreu = imfibreu;
    this.imsvb = imsvb;
    this.imsvbeu = imsvbeu;
    this.imfpacdv = imfpacdv;
    this.imbensvb = imbensvb;
    this.cdindgas = cdindgas;
    this.cdindimp = cdindimp;
    this.cdreside = cdreside;
    this.imbrmerc = imbrmerc;
    this.imbrmreu = imbrmreu;
    this.imgastos = imgastos;
    this.imgatdvs = imgatdvs;
    this.imimpdvs = imimpdvs;
    this.imimpeur = imimpeur;
    this.xtdcm = xtdcm;
    this.xtderech = xtderech;
    this.imcliqeu = imcliqeu;
    this.imcconeu = imcconeu;
    this.imdereeu = imdereeu;
    this.imnomval = imnomval;
    this.cdusuari = cdusuari;
    this.nuoprout = nuoprout;
    this.nureford = nureford;
    this.cdwareho = cdwareho;
    this.nuejecnt = nuejecnt;
    this.rfparten = rfparten;
    this.cdbroker = cdbroker;
    this.cdmercad = cdmercad;
    this.cdsucliq = cdsucliq;
    this.nuctacli = nuctacli;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuversion = nuversion;
    this.imgasliqdv = imgasliqdv;
    this.imgasliqeu = imgasliqeu;
    this.imgasgesdv = imgasgesdv;
    this.imgasgeseu = imgasgeseu;
    this.imgastotdv = imgastotdv;
    this.imgastoteu = imgastoteu;
    this.cdexoner = cdexoner;
    this.isscrdiv = isscrdiv;
    this.cdcleare = cdcleare;
    this.nif = nif;
    this.contrprr = contrprr;
    this.codprod = codprod;
    this.segmentclte = segmentclte;
    this.cdmodnego = cdmodnego;
    this.ccv = ccv;
    this.cdestadotit = cdestadotit;
    this.reftitular = reftitular;
    this.refadicional = refadicional;
    this.tmct0ctos = tmct0ctos;
    this.tmct0logs = tmct0logs;
    this.tmct0boks = tmct0boks;
  }

  @Transient
  public List<Tmct0alc> getTmct0alcs() {
    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
    List<Tmct0bok> boks = this.getTmct0boks();
    if (boks != null) {
      for (Tmct0bok bok : boks) {
        alcs.addAll(bok.getTmct0alcs());
      }
    }
    return alcs;
  }

  @Transient
  public List<Tmct0alo> getTmct0alos() {
    List<Tmct0alo> alos = new ArrayList<Tmct0alo>();
    List<Tmct0bok> boks = this.getTmct0boks();
    if (boks != null) {
      for (Tmct0bok bok : boks) {
        alos.addAll(bok.getTmct0alos());
      }
    }
    return alos;
  }

  @Id
  @Column(name = "NUORDEN", nullable = false, length = 20)
  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOASIG")
  public Tmct0estado getTmct0estado() {
    return this.tmct0estado;
  }

  public void setTmct0estado(Tmct0estado tmct0estado) {
    this.tmct0estado = tmct0estado;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  public Tmct0sta getTmct0sta() {
    return this.tmct0sta;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Column(name = "CDENTDD", length = 1)
  public Character getCdentdd() {
    return this.cdentdd;
  }

  public void setCdentdd(Character cdentdd) {
    this.cdentdd = cdentdd;
  }

  @Column(name = "CDCANAL", length = 3)
  public String getCdcanal() {
    return this.cdcanal;
  }

  public void setCdcanal(String cdcanal) {
    this.cdcanal = cdcanal;
  }

  @Column(name = "CDCAPTUR", length = 1)
  public Character getCdcaptur() {
    return this.cdcaptur;
  }

  public void setCdcaptur(Character cdcaptur) {
    this.cdcaptur = cdcaptur;
  }

  @Column(name = "CDCLENTE", length = 20)
  public String getCdclente() {
    return this.cdclente;
  }

  public void setCdclente(String cdclente) {
    this.cdclente = cdclente;
  }

  @Column(name = "CDCUENTA", length = 20)
  public String getCdcuenta() {
    return this.cdcuenta;
  }

  public void setCdcuenta(String cdcuenta) {
    this.cdcuenta = cdcuenta;
  }

  @Column(name = "CDGESORD", length = 3)
  public String getCdgesord() {
    return this.cdgesord;
  }

  public void setCdgesord(String cdgesord) {
    this.cdgesord = cdgesord;
  }

  @Column(name = "CDISIN", length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "NBVALORS", length = 40)
  public String getNbvalors() {
    return this.nbvalors;
  }

  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  @Column(name = "CDORIGEN", length = 7)
  public String getCdorigen() {
    return this.cdorigen;
  }

  public void setCdorigen(String cdorigen) {
    this.cdorigen = cdorigen;
  }

  @Column(name = "TPORIGEN", length = 3)
  public String getTporigen() {
    return this.tporigen;
  }

  public void setTporigen(String tporigen) {
    this.tporigen = tporigen;
  }

  @Column(name = "CDTPOPER", length = 1)
  public Character getCdtpoper() {
    return this.cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  @Column(name = "CDTYP", length = 1)
  public Character getCdtyp() {
    return this.cdtyp;
  }

  public void setCdtyp(Character cdtyp) {
    this.cdtyp = cdtyp;
  }

  @Column(name = "CDVIA", length = 4)
  public String getCdvia() {
    return this.cdvia;
  }

  public void setCdvia(String cdvia) {
    this.cdvia = cdvia;
  }

  @Column(name = "CDTIPORD", length = 3)
  public String getCdtipord() {
    return this.cdtipord;
  }

  public void setCdtipord(String cdtipord) {
    this.cdtipord = cdtipord;
  }

  @Column(name = "CDCLSMDO", length = 1)
  public Character getCdclsmdo() {
    return this.cdclsmdo;
  }

  public void setCdclsmdo(Character cdclsmdo) {
    this.cdclsmdo = cdclsmdo;
  }

  @Column(name = "DSOBSERV", length = 200)
  public String getDsobserv() {
    return this.dsobserv;
  }

  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }

  @Column(name = "CDTPCAMB", length = 1)
  public Character getCdtpcamb() {
    return this.cdtpcamb;
  }

  public void setCdtpcamb(Character cdtpcamb) {
    this.cdtpcamb = cdtpcamb;
  }

  @Column(name = "IMTPCAMB", precision = 18, scale = 8)
  public BigDecimal getImtpcamb() {
    return this.imtpcamb;
  }

  public void setImtpcamb(BigDecimal imtpcamb) {
    this.imtpcamb = imtpcamb;
  }

  @Column(name = "CDMONISO", length = 3)
  public String getCdmoniso() {
    return this.cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  @Column(name = "CDTPMERC", length = 4)
  public String getCdtpmerc() {
    return this.cdtpmerc;
  }

  public void setCdtpmerc(String cdtpmerc) {
    this.cdtpmerc = cdtpmerc;
  }

  @Column(name = "NUTITORD", precision = 16, scale = 5)
  public BigDecimal getNutitord() {
    return this.nutitord;
  }

  public void setNutitord(BigDecimal nutitord) {
    this.nutitord = nutitord;
  }

  @Column(name = "NUTITEJE", precision = 16, scale = 5)
  public BigDecimal getNutiteje() {
    return this.nutiteje;
  }

  public void setNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje;
  }

  @Column(name = "IMCBCLIE", precision = 18, scale = 8)
  public BigDecimal getImcbclie() {
    return this.imcbclie;
  }

  public void setImcbclie(BigDecimal imcbclie) {
    this.imcbclie = imcbclie;
  }

  @Column(name = "IMCBMERC", precision = 18, scale = 8)
  public BigDecimal getImcbmerc() {
    return this.imcbmerc;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  @Column(name = "IMLIMITE", precision = 18, scale = 8)
  public BigDecimal getImlimite() {
    return this.imlimite;
  }

  public void setImlimite(BigDecimal imlimite) {
    this.imlimite = imlimite;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEALTA", length = 10)
  public Date getFealta() {
    return this.fealta;
  }

  public void setFealta(Date fealta) {
    this.fealta = fealta;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOALTA", length = 8)
  public Date getHoalta() {
    return this.hoalta;
  }

  public void setHoalta(Date hoalta) {
    this.hoalta = hoalta;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECONTRA", length = 10)
  public Date getFecontra() {
    return this.fecontra;
  }

  public void setFecontra(Date fecontra) {
    this.fecontra = fecontra;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC", length = 10)
  public Date getFeejecuc() {
    return this.feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOEJECUC", length = 8)
  public Date getHoejecuc() {
    return this.hoejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALIDE", length = 10)
  public Date getFevalide() {
    return this.fevalide;
  }

  public void setFevalide(Date fevalide) {
    this.fevalide = fevalide;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 10)
  public Date getFevalor() {
    return this.fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  @Column(name = "IMCBNTCL", precision = 18, scale = 8)
  public BigDecimal getImcbntcl() {
    return this.imcbntcl;
  }

  public void setImcbntcl(BigDecimal imcbntcl) {
    this.imcbntcl = imcbntcl;
  }

  @Column(name = "IMNETOBR", precision = 18, scale = 8)
  public BigDecimal getImnetobr() {
    return this.imnetobr;
  }

  public void setImnetobr(BigDecimal imnetobr) {
    this.imnetobr = imnetobr;
  }

  @Column(name = "IMNETOCL", precision = 18, scale = 8)
  public BigDecimal getImnetocl() {
    return this.imnetocl;
  }

  public void setImnetocl(BigDecimal imnetocl) {
    this.imnetocl = imnetocl;
  }

  @Column(name = "IMNTBREU", precision = 18, scale = 8)
  public BigDecimal getImntbreu() {
    return this.imntbreu;
  }

  public void setImntbreu(BigDecimal imntbreu) {
    this.imntbreu = imntbreu;
  }

  @Column(name = "IMNTCLEU", precision = 18, scale = 8)
  public BigDecimal getImntcleu() {
    return this.imntcleu;
  }

  public void setImntcleu(BigDecimal imntcleu) {
    this.imntcleu = imntcleu;
  }

  @Column(name = "PODEVOLU", precision = 16, scale = 6)
  public BigDecimal getPodevolu() {
    return this.podevolu;
  }

  public void setPodevolu(BigDecimal podevolu) {
    this.podevolu = podevolu;
  }

  @Column(name = "POFPACLI", precision = 16, scale = 6)
  public BigDecimal getPofpacli() {
    return this.pofpacli;
  }

  public void setPofpacli(BigDecimal pofpacli) {
    this.pofpacli = pofpacli;
  }

  @Column(name = "IMDEVCDV", precision = 18, scale = 8)
  public BigDecimal getImdevcdv() {
    return this.imdevcdv;
  }

  public void setImdevcdv(BigDecimal imdevcdv) {
    this.imdevcdv = imdevcdv;
  }

  @Column(name = "IMDEVCLI", precision = 18, scale = 8)
  public BigDecimal getImdevcli() {
    return this.imdevcli;
  }

  public void setImdevcli(BigDecimal imdevcli) {
    this.imdevcli = imdevcli;
  }

  @Column(name = "IMFIBRDV", precision = 18, scale = 8)
  public BigDecimal getImfibrdv() {
    return this.imfibrdv;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  @Column(name = "IMFIBREU", precision = 18, scale = 8)
  public BigDecimal getImfibreu() {
    return this.imfibreu;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  @Column(name = "IMSVB", precision = 18, scale = 8)
  public BigDecimal getImsvb() {
    return this.imsvb;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  @Column(name = "IMSVBEU", precision = 18, scale = 8)
  public BigDecimal getImsvbeu() {
    return this.imsvbeu;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  @Column(name = "IMFPACDV", precision = 18, scale = 8)
  public BigDecimal getImfpacdv() {
    return this.imfpacdv;
  }

  public void setImfpacdv(BigDecimal imfpacdv) {
    this.imfpacdv = imfpacdv;
  }

  @Column(name = "IMBENSVB", precision = 18, scale = 8)
  public BigDecimal getImbensvb() {
    return this.imbensvb;
  }

  public void setImbensvb(BigDecimal imbensvb) {
    this.imbensvb = imbensvb;
  }

  @Column(name = "CDINDGAS", length = 1)
  public Character getCdindgas() {
    return this.cdindgas;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }

  @Column(name = "CDINDIMP", length = 1)
  public Character getCdindimp() {
    return this.cdindimp;
  }

  public void setCdindimp(Character cdindimp) {
    this.cdindimp = cdindimp;
  }

  @Column(name = "CDRESIDE", length = 1)
  public Character getCdreside() {
    return this.cdreside;
  }

  public void setCdreside(Character cdreside) {
    this.cdreside = cdreside;
  }

  @Column(name = "IMBRMERC", precision = 18, scale = 8)
  public BigDecimal getImbrmerc() {
    return this.imbrmerc;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  @Column(name = "IMBRMREU", precision = 18, scale = 8)
  public BigDecimal getImbrmreu() {
    return this.imbrmreu;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  @Column(name = "IMGASTOS", precision = 18, scale = 8)
  public BigDecimal getImgastos() {
    return this.imgastos;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  @Column(name = "IMGATDVS", precision = 18, scale = 8)
  public BigDecimal getImgatdvs() {
    return this.imgatdvs;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  @Column(name = "IMIMPDVS", precision = 18, scale = 8)
  public BigDecimal getImimpdvs() {
    return this.imimpdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  @Column(name = "IMIMPEUR", precision = 18, scale = 8)
  public BigDecimal getImimpeur() {
    return this.imimpeur;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  @Column(name = "XTDCM", length = 1)
  public Character getXtdcm() {
    return this.xtdcm;
  }

  public void setXtdcm(Character xtdcm) {
    this.xtdcm = xtdcm;
  }

  @Column(name = "XTDERECH", length = 1)
  public Character getXtderech() {
    return this.xtderech;
  }

  public void setXtderech(Character xtderech) {
    this.xtderech = xtderech;
  }

  @Column(name = "IMCLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcliqeu() {
    return this.imcliqeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  @Column(name = "IMCCONEU", precision = 18, scale = 8)
  public BigDecimal getImcconeu() {
    return this.imcconeu;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  @Column(name = "IMDEREEU", precision = 18, scale = 8)
  public BigDecimal getImdereeu() {
    return this.imdereeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  @Column(name = "IMNOMVAL", precision = 18, scale = 8)
  public BigDecimal getImnomval() {
    return this.imnomval;
  }

  public void setImnomval(BigDecimal imnomval) {
    this.imnomval = imnomval;
  }

  @Column(name = "CDUSUARI", length = 8)
  public String getCdusuari() {
    return this.cdusuari;
  }

  public void setCdusuari(String cdusuari) {
    this.cdusuari = cdusuari;
  }

  @Column(name = "NUOPROUT", precision = 9)
  public Integer getNuoprout() {
    return this.nuoprout;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  @Column(name = "NUREFORD", length = 32)
  public String getNureford() {
    return this.nureford;
  }

  public void setNureford(String nureford) {
    this.nureford = nureford;
  }

  @Column(name = "CDWAREHO", length = 1)
  public Character getCdwareho() {
    return this.cdwareho;
  }

  public void setCdwareho(Character cdwareho) {
    this.cdwareho = cdwareho;
  }

  @Column(name = "NUEJECNT", precision = 5)
  public Integer getNuejecnt() {
    return this.nuejecnt;
  }

  public void setNuejecnt(Integer nuejecnt) {
    this.nuejecnt = nuejecnt;
  }

  @Column(name = "RFPARTEN", length = 16)
  public String getRfparten() {
    return this.rfparten;
  }

  public void setRfparten(String rfparten) {
    this.rfparten = rfparten;
  }

  @Column(name = "CDBROKER", length = 20)
  public String getCdbroker() {
    return this.cdbroker;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  @Column(name = "CDMERCAD", length = 20)
  public String getCdmercad() {
    return this.cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  @Column(name = "CDSUCLIQ", length = 4)
  public String getCdsucliq() {
    return this.cdsucliq;
  }

  public void setCdsucliq(String cdsucliq) {
    this.cdsucliq = cdsucliq;
  }

  @Column(name = "NUCTACLI", length = 20)
  public String getNuctacli() {
    return this.nuctacli;
  }

  public void setNuctacli(String nuctacli) {
    this.nuctacli = nuctacli;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION")
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "IMGASLIQDV", precision = 18, scale = 8)
  public BigDecimal getImgasliqdv() {
    return this.imgasliqdv;
  }

  public void setImgasliqdv(BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  @Column(name = "IMGASLIQEU", precision = 18, scale = 8)
  public BigDecimal getImgasliqeu() {
    return this.imgasliqeu;
  }

  public void setImgasliqeu(BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  @Column(name = "IMGASGESDV", precision = 18, scale = 8)
  public BigDecimal getImgasgesdv() {
    return this.imgasgesdv;
  }

  public void setImgasgesdv(BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  @Column(name = "IMGASGESEU", precision = 18, scale = 8)
  public BigDecimal getImgasgeseu() {
    return this.imgasgeseu;
  }

  public void setImgasgeseu(BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  @Column(name = "IMGASTOTDV", precision = 18, scale = 8)
  public BigDecimal getImgastotdv() {
    return this.imgastotdv;
  }

  public void setImgastotdv(BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  @Column(name = "IMGASTOTEU", precision = 18, scale = 8)
  public BigDecimal getImgastoteu() {
    return this.imgastoteu;
  }

  public void setImgastoteu(BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  @Column(name = "CDEXONER", length = 20)
  public String getCdexoner() {
    return this.cdexoner;
  }

  public void setCdexoner(String cdexoner) {
    this.cdexoner = cdexoner;
  }

  @Column(name = "ISSCRDIV", nullable = false, length = 1)
  public char getIsscrdiv() {
    return this.isscrdiv;
  }

  public void setIsscrdiv(char isscrdiv) {
    this.isscrdiv = isscrdiv;
  }

  @Column(name = "CDCLEARE", length = 25)
  public String getCdcleare() {
    return this.cdcleare;
  }

  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  @Column(name = "NIF", length = 11)
  public String getNif() {
    return this.nif;
  }

  public void setNif(String nif) {
    this.nif = nif;
  }

  @Column(name = "CONTRPRR", length = 10)
  public String getContrprr() {
    return this.contrprr;
  }

  public void setContrprr(String contrprr) {
    this.contrprr = contrprr;
  }

  @Column(name = "CODPROD", length = 2)
  public String getCodprod() {
    return this.codprod;
  }

  public void setCodprod(String codprod) {
    this.codprod = codprod;
  }

  @Column(name = "SEGMENTCLTE", length = 3)
  public String getSegmentclte() {
    return this.segmentclte;
  }

  public void setSegmentclte(String segmentclte) {
    this.segmentclte = segmentclte;
  }

  @Column(name = "CDMODNEGO", length = 35)
  public String getCdmodnego() {
    return this.cdmodnego;
  }

  public void setCdmodnego(String cdmodnego) {
    this.cdmodnego = cdmodnego;
  }

  @Column(name = "CCV", length = 20)
  public String getCcv() {
    return this.ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  @Column(name = "CDESTADOTIT")
  public Integer getCdestadotit() {
    return this.cdestadotit;
  }

  public void setCdestadotit(Integer cdestadotit) {
    this.cdestadotit = cdestadotit;
  }

  @Column(name = "REFTITULAR", length = 20)
  public String getReftitular() {
    return this.reftitular;
  }

  public void setReftitular(String reftitular) {
    this.reftitular = reftitular;
  }

  @Column(name = "REFADICIONAL", length = 20)
  public String getRefadicional() {
    return this.refadicional;
  }

  public void setRefadicional(String refadicional) {
    this.refadicional = refadicional;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0ord")
  public List<Tmct0cto> getTmct0ctos() {
    return this.tmct0ctos;
  }

  public void setTmct0ctos(List<Tmct0cto> tmct0ctos) {
    this.tmct0ctos = tmct0ctos;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0ord")
  public List<Tmct0log> getTmct0logs() {
    return this.tmct0logs;
  }

  public void setTmct0logs(List<Tmct0log> tmct0logs) {
    this.tmct0logs = tmct0logs;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0ord")
  public List<Tmct0bok> getTmct0boks() {
    return this.tmct0boks;
  }

  public void setTmct0boks(List<Tmct0bok> tmct0boks) {
    this.tmct0boks = tmct0boks;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0ord", cascade = CascadeType.ALL)
  public List<Tmct0ordDynamicValues> getDynamicValues() {
    return dynamicValues;
  }

  public void setDynamicValues(List<Tmct0ordDynamicValues> dynamicValues) {
    this.dynamicValues = dynamicValues;
  }

}// public class Tmct0ord implements java.io.Serializable {
