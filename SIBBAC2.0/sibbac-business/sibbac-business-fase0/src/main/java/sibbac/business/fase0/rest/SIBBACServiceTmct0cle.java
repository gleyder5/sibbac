package sibbac.business.fase0.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0cleNacionalBo;
import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.fase0.database.dto.Tmct0cleDTO;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class SIBBACServiceTmct0cle
 */
@SIBBACService
public class SIBBACServiceTmct0cle implements SIBBACServiceBean {

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServiceTmct0cle.class );

	@Autowired
	private Tmct0cleNacionalBo			tmct0cleNacionalBo;

	public SIBBACServiceTmct0cle() {
		LOG.trace( "[SIBBACServiceTmct0cle::<constructor>] Initiating SIBBAC Service for \"Texto\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.trace( "[ServiceTextos::process] Processing a web request..." );
		final WebResponse webResponse = new WebResponse();
		final Map< String, Object > resultados = new HashMap< String, Object >();
		// extrae parametros del httpRequest
		final SIBBACServiceTmct0cleCommand action = getCommand( webRequest.getAction() );
		LOG.trace( "[ServiceTextos::process] Command.: [{}]", action.getCommand() );
		switch ( action ) {
			case CMD_GET_LISTA:
				LOG.trace( " Llamada el WebResponse con la lista de TEXTOS " );
				resultados.put( "listado", getLista() );
				break;
			default:
				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;
		}
		resultados.put( "status", "OK" );
		webResponse.setResultados( resultados );
		return webResponse;
	}

	private List< LabelValueObject > getLista() {

		final Set< Tmct0cleDTO > tmct0cles = tmct0cleNacionalBo.findDistinctDscleareAndBiccleByFhfinal();
		final List< LabelValueObject > listado = new ArrayList< LabelValueObject >();

		if ( CollectionUtils.isNotEmpty( tmct0cles ) ) {
			for ( Tmct0cleDTO tmct0cleDTO : tmct0cles ) {
				final String biccle = tmct0cleDTO.getBiccle();
				final String nombre = tmct0cleDTO.getNombre();
				if ( biccle != null && nombre != null ) {
					listado.add( new LabelValueObject( tmct0cleDTO.getNombre(), tmct0cleDTO.getBiccle() ) );
				}
			}
		}
		return listado;
	}

	private enum SIBBACServiceTmct0cleCommand {
		CMD_GET_LISTA( "getLista" ), CMD_MODIFICAR_TEXTO( "modificarTexto" ), CMD_NO_COMMAND( "" );

		private String	command;

		/**
		 * Instantiates a new SIBBACServiceTextoCommand
		 *
		 * @param command
		 *            the command
		 */
		private SIBBACServiceTmct0cleCommand( String command ) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * Gets the command.
	 *
	 * @param command
	 *            the command
	 * @return the command
	 */
	private SIBBACServiceTmct0cleCommand getCommand( final String command ) {
		final SIBBACServiceTmct0cleCommand[] commands = SIBBACServiceTmct0cleCommand.values();
		SIBBACServiceTmct0cleCommand commandRequested = null;
		for ( int i = 0; i < commands.length; i++ ) {
			if ( commands[ i ].getCommand().equalsIgnoreCase( command ) ) {
				commandRequested = commands[ i ];
			}
		}
		if ( commandRequested == null ) {
			commandRequested = SIBBACServiceTmct0cleCommand.CMD_NO_COMMAND;
			LOG.debug( "No se reconoce el comando [{}]", command );
		} else {
			LOG.debug( "Se selecciona el comando [{}]", command );
		}
		return commandRequested;
	}

	/**
	 * Gets the available commands.
	 *
	 * @return the available commands
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > result = new ArrayList< String >();
		final SIBBACServiceTmct0cleCommand[] commands = SIBBACServiceTmct0cleCommand.values();
		for ( int i = 0; i < commands.length; i++ ) {
			result.add( commands[ i ].getCommand() );
		}
		return result;
	}

	@Override
	public List< String > getFields() {
		return null;
	}
}
