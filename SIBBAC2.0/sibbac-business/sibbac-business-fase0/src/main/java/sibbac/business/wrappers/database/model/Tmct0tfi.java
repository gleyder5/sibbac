package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants;

@Entity
@Table( name = DBConstants.WRAPPERS.TFI )
public class Tmct0tfi implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
 private Tmct0tfiId id;
  
  @Column(name = "TIPOTIT")
  private String   tipotit;
  
  @Column(name = "CDUSUAUD")
  private String  cdusuaud;
  
  @Temporal(TemporalType.DATE)
  @Column(name = "FHAUDIT", length = 10)
  private Date fhaudit;

  @Column(name = "FHINICIO", length = 8)
  private BigDecimal fhinicio = BigDecimal.ZERO;
  
  @Column(name = "FHFINAL", length = 8)
  private BigDecimal fhfinal = BigDecimal.ZERO;
  
  @Column(name = "TPTIPREP")
  private String tptiprep;
  
  public Tmct0tfi() {
  }

 

  
  public Tmct0tfi(Tmct0tfiId id,
                  String tipotit,
                  String cdusuaud,
                  Date fhaudit,
                  BigDecimal fhinicio,
                  BigDecimal fhfinal,
                  String tptiprep) {
    this.id = id;
    this.tipotit = tipotit;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.fhinicio = fhinicio;
    this.fhfinal = fhfinal;
    this.tptiprep = tptiprep;
  }




  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "cdaliass", column = @Column(name = "CDALIASS")),
      @AttributeOverride(name = "cdsubcta", column = @Column(name = "CDSUBCTA")),
      @AttributeOverride(name = "centro", column = @Column(name = "CENTRO")),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD")),
      @AttributeOverride(name = "cddclave", column = @Column(name = "CDDCLAVE")),
      @AttributeOverride(name = "numsec", column = @Column(name = "NUMSEC"))

  })
  public Tmct0tfiId getId() {
    return id;
  }

  public void setId(Tmct0tfiId id) {
    this.id = id;
  }

  public String getTipotit() {
    return tipotit;
  }

  public void setTipotit(String tipotit) {
    this.tipotit = tipotit;
  }



  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public BigDecimal getFhinicio() {
    return fhinicio;
  }

  public void setFhinicio(BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  public BigDecimal getFhfinal() {
    return fhfinal;
  }

  public void setFhfinal(BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  public String getTptiprep() {
    return tptiprep;
  }

  public void setTptiprep(String tptiprep) {
    this.tptiprep = tptiprep;
  }
  
  
  
  

}
