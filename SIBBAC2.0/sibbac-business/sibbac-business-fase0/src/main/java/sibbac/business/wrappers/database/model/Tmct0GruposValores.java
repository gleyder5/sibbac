package sibbac.business.wrappers.database.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_GRUPOS_VALORES")
public class Tmct0GruposValores implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -1584155948899532660L;
  private Integer id;
  private String cdCodigo;
  private String nbDescripcion;
  private List<Tmct0Valores> tmct0Valoreses = new ArrayList<Tmct0Valores>(0);

  public Tmct0GruposValores() {
  }

  public Tmct0GruposValores(Integer id, String cdCodigo) {
    this.id = id;
    this.cdCodigo = cdCodigo;
  }

  public Tmct0GruposValores(Integer id, String cdCodigo, String nbDescripcion, List<Tmct0Valores> tmct0Valoreses) {
    this.id = id;
    this.cdCodigo = cdCodigo;
    this.nbDescripcion = nbDescripcion;
    this.tmct0Valoreses = tmct0Valoreses;
  }

  @Id
  @Column(name = "ID", unique = true, nullable = false)
  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "CD_CODIGO", unique = true, nullable = false, length = 4)
  public String getCdCodigo() {
    return this.cdCodigo;
  }

  public void setCdCodigo(String cdCodigo) {
    this.cdCodigo = cdCodigo;
  }

  @Column(name = "NB_DESCRIPCION")
  public String getNbDescripcion() {
    return this.nbDescripcion;
  }

  public void setNbDescripcion(String nbDescripcion) {
    this.nbDescripcion = nbDescripcion;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0GruposValores")
  public List<Tmct0Valores> getTmct0Valoreses() {
    return this.tmct0Valoreses;
  }

  public void setTmct0Valoreses(List<Tmct0Valores> tmct0Valoreses) {
    this.tmct0Valoreses = tmct0Valoreses;
  }

}
