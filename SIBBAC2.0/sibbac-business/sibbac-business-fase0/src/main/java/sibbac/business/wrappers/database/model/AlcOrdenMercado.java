package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sibbac.database.DBConstants.WRAPPERS;

/**
 *
 * Entidad para la gestion de TMCT0_ALC_ORDEN_MERCADO.
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = WRAPPERS.ALC_ORDEN_MERCADO)
public class AlcOrdenMercado implements java.io.Serializable {

	private static final long serialVersionUID = -2775316069454623564L;

	/** Campos de la entidad */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "IDALC_ORDEN", nullable = false)
	private AlcOrdenes alcOrdenes;
	
	@Column(name = "NUORDEN", length = 20, nullable = false)
	private String nuOrden;

	@Column(name = "NBOOKING", length = 20, nullable = false)
	private String nBooking;

	@Column(name = "NUCNFCLT", length = 20, nullable = false)
	private String nuCnfClt;

	@Column(name = "NUCNFLIQ", length = 2, nullable = false)
	private Short nuCnfLiq;

	@ManyToOne
    @JoinColumn(name = "IDDESGLOSECAMARA", nullable = false)
	private Tmct0desglosecamara desgloseCamara;

	@Column(name = "ESTADO", length = 1, nullable = false)
	private char estado;
	
	@Column(name = "TITULOS", length = 18, nullable = false)
	private BigDecimal titulos;

	@Column(name = "AUDIT_DATE", nullable = false)
	private Date auditDate;
	
	@Column(name = "AUDIT_USER", length = 20, nullable = false)
	private String auditUser;

	public AlcOrdenMercado() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AlcOrdenes getAlcOrdenes() {
		return alcOrdenes;
	}

	public void setAlcOrdenes(AlcOrdenes alcOrdenes) {
		this.alcOrdenes = alcOrdenes;
	}

	public String getNuOrden() {
		return nuOrden;
	}

	public void setNuOrden(String nuOrden) {
		this.nuOrden = nuOrden;
	}

	public String getnBooking() {
		return nBooking;
	}

	public void setnBooking(String nBooking) {
		this.nBooking = nBooking;
	}

	public String getNuCnfClt() {
		return nuCnfClt;
	}

	public void setNuCnfClt(String nuCnfClt) {
		this.nuCnfClt = nuCnfClt;
	}

	public Short getNuCnfLiq() {
		return nuCnfLiq;
	}

	public void setNuCnfLiq(Short nuCnfLiq) {
		this.nuCnfLiq = nuCnfLiq;
	}

	public Tmct0desglosecamara getDesgloseCamara() {
		return desgloseCamara;
	}

	public void setDesgloseCamara(Tmct0desglosecamara desgloseCamara) {
		this.desgloseCamara = desgloseCamara;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos(BigDecimal titulos) {
		this.titulos = titulos;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}
	
	//Métodos de dominio

    public boolean isEstadoBaja() {
        return 'B' == estado;
    }

    public boolean isEstadoAlta() {
        return 'A' == estado;
    }

    public void registraBaja() {
        setEstado('B');
        setAuditDate(new Date());
        setAuditUser("SIBBAC");
    }


}