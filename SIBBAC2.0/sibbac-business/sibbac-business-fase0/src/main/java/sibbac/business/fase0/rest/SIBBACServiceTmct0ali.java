package sibbac.business.fase0.rest;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceTmct0ali implements SIBBACServiceBean {
	private static final Logger LOG	= LoggerFactory.getLogger( SIBBACServiceTmct0ali.class );

	// COMMANDS
	private static final String	CMD_GET_ALIAS	= "getAlias";
	private static final String	CMD_GET_ALIAS2	= "getAlias2";
	// RESULTS
	private static final String	RESULT_ALIAS	= "result_alias";
	@Autowired
	private Tmct0aliDao			aliDao;

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		final String command = webRequest.getAction();
		LOG.debug( "Entra con command: " + command );
		WebResponse response = new WebResponse();
		Map< String, Object > result = new HashMap< String, Object >();
		List< Tmct0aliDTO > dataList;
		switch ( command ) {
			case CMD_GET_ALIAS:
				dataList = getAlias();
				result.put( RESULT_ALIAS, dataList );
				response.setResultados( result );
				break;
			case CMD_GET_ALIAS2:
				response.setResultados( getAlias( webRequest.getFilters() ) );
				break;
			default:
				break;
		}
		return response;
	}

	private List< Tmct0aliDTO > getAlias() {
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );
		Calendar cal = Calendar.getInstance();
		Integer fechaActivo = Integer.parseInt( sdf.format( cal.getTime() ) );
		List< Tmct0aliDTO > dataList = aliDao.findAllAli( fechaActivo );
		return dataList;
	}

	private Map< String, Object > getAlias(Map<String, String> filters) {
		Map< String, Object > mDato;
		Map< String, Object > mSalida = new HashMap< String, Object >();
		List< String > lAlias = new ArrayList< String >();
		List< Map< String, Object >> lAli = new ArrayList< Map< String, Object >>();
		LOG.debug( "text: " + filters.get( "text" ) + ". aliasSelected: " +  filters.get( "aliasSelected" ) );
		String[] split = filters.get( "aliasSelected" ).split( ",\\s*" );
		for (int i=0; i<split.length; i++) {
			lAlias.add( split[i] );
		}
		for ( Object[] objects : aliDao.findByText(filters.get( "text" ), lAlias)) {
			lAli.add( mDato = new HashMap< String, Object >() );
			mDato.put( "cdalias", ((String) objects[ 0 ]).trim() );
			mDato.put( "descrali", ((String) objects[ 1 ]).trim() );
		}
		mSalida.put( "alias", lAli );
		return mSalida;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new LinkedList< String >();
		commands.add( CMD_GET_ALIAS );
		return commands;
	}

	@Override
	public List< String > getFields() {
		// TODO Auto-generated method stub
		return null;
	}

}
