package sibbac.business.fase0.database.dto;


import sibbac.business.fase0.database.model.Tmct0estado;


public class Tmct0estadoDTO {

	private Integer	idestado;
	private String	nombre;
	private Integer	idtipoestado;

	public Tmct0estadoDTO() {

	}

	public Tmct0estadoDTO( final Tmct0estado estado ) {
		this.idestado = estado.getIdestado();
		this.nombre = estado.getNombre();
		this.idtipoestado = estado.getIdtipoestado();
	}

	public Integer getIdestado() {
		return idestado;
	}

	public void setIdestado( Integer idestado ) {
		this.idestado = idestado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public Integer getIdtipoestado() {
		return idtipoestado;
	}

	public void setIdtipoestado( Integer idtipoestado ) {
		this.idtipoestado = idtipoestado;
	}

}
