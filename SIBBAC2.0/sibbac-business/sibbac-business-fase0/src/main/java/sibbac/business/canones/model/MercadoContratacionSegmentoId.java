package sibbac.business.canones.model;

import javax.persistence.Embeddable;
import javax.persistence.Column;

/**
 * Llave primaria de un Segmento de un Mercado de Contratación.
 */
@Embeddable
public class MercadoContratacionSegmentoId extends MercadoContratacionRefId {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -4373132084697129858L;

  @Column(name = MercadoContratacion.ID_COLUMN_NAME, length = 2)
  private String idField;
  
  @Override
  public String getIdField() {
    return idField;
  }
  
  @Override
  public void setIdField(String idField) {
    this.idField = idField;
  }
  
}
