package sibbac.business.fase0.database.bean;


import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import sibbac.common.SIBBACBusinessException;


/**
 * The Class ServiceTextoBean.
 */
public class ServiceTmct0cleBean {

	/**
	 * @param filters
	 */
	public ServiceTmct0cleBean( Map< String, String > filters ) throws SIBBACBusinessException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		final ToStringBuilder tsb = new ToStringBuilder( ToStringStyle.SHORT_PREFIX_STYLE );
		return tsb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object object ) {
		//final ServiceTmct0cleBean serviceTextoBean = ( ServiceTmct0cleBean ) object;
		final EqualsBuilder eqb = new EqualsBuilder();
		return eqb.isEquals();
	}

}
