package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0pkcId implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -126834263383563575L;
  @Column(name = "PKBROKER", length = 9, scale = 0, nullable = false)
  private Integer pkbroker;
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden;
  @Column(name = "NBOOKING", length = 20, nullable = false)
  private java.lang.String nbooking;
  @Column(name = "CDBROKER", length = 20, nullable = false)
  private java.lang.String cdbroker;
  @Column(name = "CDMERCAD", length = 20, nullable = false)
  private java.lang.String cdmercad;

  public Tmct0pkcId() {
  }

  public Tmct0pkcId(Integer pkbroker, String nuorden, String nbooking, String cdbroker, String cdmercad) {
    super();
    this.pkbroker = pkbroker;
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.cdbroker = cdbroker;
    this.cdmercad = cdmercad;
  }

  public Integer getPkbroker() {
    return pkbroker;
  }

  public String getNuorden() {
    return nuorden;
  }

  public java.lang.String getNbooking() {
    return nbooking;
  }

  public java.lang.String getCdbroker() {
    return cdbroker;
  }

  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  public void setPkbroker(Integer pkbroker) {
    this.pkbroker = pkbroker;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(java.lang.String nbooking) {
    this.nbooking = nbooking;
  }

  public void setCdbroker(java.lang.String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbroker == null) ? 0 : cdbroker.hashCode());
    result = prime * result + ((cdmercad == null) ? 0 : cdmercad.hashCode());
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    result = prime * result + ((pkbroker == null) ? 0 : pkbroker.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0pkcId other = (Tmct0pkcId) obj;
    if (cdbroker == null) {
      if (other.cdbroker != null)
        return false;
    }
    else if (!cdbroker.equals(other.cdbroker))
      return false;
    if (cdmercad == null) {
      if (other.cdmercad != null)
        return false;
    }
    else if (!cdmercad.equals(other.cdmercad))
      return false;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    }
    else if (!nbooking.equals(other.nbooking))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    }
    else if (!nuorden.equals(other.nuorden))
      return false;
    if (pkbroker == null) {
      if (other.pkbroker != null)
        return false;
    }
    else if (!pkbroker.equals(other.pkbroker))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tmct0pkcId %d##%s##%s##%s##%s", this.pkbroker, this.nuorden, this.nbooking, this.cdbroker,
        this.cdmercad);
  }

}
