package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0_DESGLOSECAMARA_ENVIO_RT")
public class Tmct0DesgloseCamaraEnvioRt implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -690171960201483416L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDDESGLOSECAMARAENVIORT", unique = true, nullable = false)
  private Long idDesgloseCamaraEnvioRt;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDDESGLOSECAMARA", nullable = false)
  private Tmct0desglosecamara desgloseCamara;

  @Transient
  private Tmct0desgloseclitit tmct0desgloseclitit;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDENVIOTITULARIDAD", nullable = false)
  private Tmct0enviotitularidad envioTitularidad;

  @Column(name = "CDMIEMBROMKT", length = 10, nullable = false)
  protected String cdmiembromkt;

  @Column(name = "CDREFTITULARPTI", length = 20, nullable = false)
  private String referenciaTitular;

  @Column(name = "ESTADO", length = 1, nullable = false)
  private Character estado;

  @Column(name = "AUDIT_USER", length = 20)
  private String audit_user;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO")
  private Date audit_fecha_cambio;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECONTRATACION", nullable = false)
  private Date fecontratacion;

  @Column(name = "CDOPERACIONECC", length = 16, nullable = true)
  protected String cdoperacionecc;

  @Column(length = 9, nullable = true)
  protected String nuordenmkt;

  @Column(name = "CRITERIO_COMUNICACION_TITULAR", length = 35, nullable = false)
  protected String criterioComunicacionTitular;

  @Column(name = "IDENTIFICACION_CRITERIO_COMUNICACION_TITULAR", length = 1, nullable = false)
  protected Character identificacionCriterioComunicacionTitular;

  @Column(name = "IMTITULOS", length = 18, scale = 6, nullable = true)
  protected java.math.BigDecimal imtitulos;

  @Column(name = "NUDESGLOSE", nullable = true)
  protected Long nudesglose;

  @Column(name = "CD_PLATAFORMA_NEGOCIACION", length = 4, nullable = true)
  protected String cdPlataformaNegociacion;

  public Long getIdDesgloseCamaraEnvioRt() {
    return idDesgloseCamaraEnvioRt;
  }

  public Tmct0desglosecamara getDesgloseCamara() {
    return desgloseCamara;
  }

  public Tmct0enviotitularidad getEnvioTitularidad() {
    return envioTitularidad;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public String getReferenciaTitular() {
    return referenciaTitular;
  }

  public Character getEstado() {
    return estado;
  }

  public String getAudit_user() {
    return audit_user;
  }

  public Date getAudit_fecha_cambio() {
    return audit_fecha_cambio;
  }

  public Date getFecontratacion() {
    return fecontratacion;
  }

  public String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getCriterioComunicacionTitular() {
    return criterioComunicacionTitular;
  }

  public Character getIdentificacionCriterioComunicacionTitular() {
    return identificacionCriterioComunicacionTitular;
  }

  public java.math.BigDecimal getImtitulos() {
    return imtitulos;
  }

  public Long getNudesglose() {
    return nudesglose;
  }

  public void setIdDesgloseCamaraEnvioRt(Long idDesgloseCamaraEnvioRt) {
    this.idDesgloseCamaraEnvioRt = idDesgloseCamaraEnvioRt;
  }

  public void setDesgloseCamara(Tmct0desglosecamara desgloseCamara) {
    this.desgloseCamara = desgloseCamara;
  }

  public void setEnvioTitularidad(Tmct0enviotitularidad envioTitularidad) {
    this.envioTitularidad = envioTitularidad;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  public void setReferenciaTitular(String referenciaTitular) {
    this.referenciaTitular = referenciaTitular;
  }

  public void setEstado(Character estado) {
    this.estado = estado;
  }

  public void setAudit_user(String audit_user) {
    this.audit_user = audit_user;
  }

  public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
    this.audit_fecha_cambio = audit_fecha_cambio;
  }

  public void setFecontratacion(Date fecontratacion) {
    this.fecontratacion = fecontratacion;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public void setCriterioComunicacionTitular(String criterioComunicacionTitular) {
    this.criterioComunicacionTitular = criterioComunicacionTitular;
  }

  public void setIdentificacionCriterioComunicacionTitular(Character identificacionCriterioComunicacionTitular) {
    this.identificacionCriterioComunicacionTitular = identificacionCriterioComunicacionTitular;
  }

  public void setImtitulos(java.math.BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  public Tmct0desgloseclitit getTmct0desgloseclitit() {
    return tmct0desgloseclitit;
  }

  public void setTmct0desgloseclitit(Tmct0desgloseclitit tmct0desgloseclitit) {
    this.tmct0desgloseclitit = tmct0desgloseclitit;
  }

  public String getCdPlataformaNegociacion() {
    return cdPlataformaNegociacion;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

}
