package sibbac.business.fase0.database.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.dto.EstadosEquivalentesDTO;
import sibbac.business.fase0.database.dto.Tmct0estadoDTO;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0estadoBo extends AbstractBo<Tmct0estado, Integer, Tmct0estadoDao> {

  private static final String STRING_FORMAT_KEY_ESTADOS_EQUIVALENTES = "%s######%s";

  public Tmct0estado findByIdFacturaPendienteRecordatorio(EstadosEnumerados.FACTURA_PENDIENTE_RECORDATORIO estado) {
    return dao.findOne(estado.getId());
  }

  public Tmct0estado findByIdFacturaSugerida(EstadosEnumerados.FACTURA_SUGERIDA estado) {
    return dao.findOne(estado.getId());
  }

  public Tmct0estado findByIdAlcOrden(EstadosEnumerados.ALC_ORDENES estado) {
    return dao.findOne(estado.getId());
  }

  public Tmct0estado findByIdFactura(EstadosEnumerados.FACTURA estado) {
    return dao.findOne(estado.getId());
  }

  public Tmct0estado findByIdContabilidad(EstadosEnumerados.CONTABILIDAD estado) {
    return dao.findOne(estado.getId());
  }

  public Tmct0estado findByIdFacturaRectificativa(EstadosEnumerados.FACTURA_RECTIFICATIVA estado) {
    return dao.findOne(estado.getId());
  }

  public List<Tmct0estado> findByIdtipoestado(Tmct0estado estado) {
    return dao.findByIdtipoestado(estado.getIdtipoestado());
  }

  public List<Tmct0estadoDTO> findByIdtipoestadoOrderByNombreAsc(Integer idtipoestado) {
    final List<Tmct0estado> estados = dao.findByIdtipoestadoOrderByNombreAsc(idtipoestado);
    return getEstadosDTO(estados);
  }

  public List<Tmct0estadoDTO> findByIdtipoestadoOrderByIdestadoAsc(Integer idtipoestado) {
    final List<Tmct0estado> estados = dao.findByIdtipoestadoOrderByIdestadoAsc(idtipoestado);
    return getEstadosDTO(estados);
  }

  public List<Tmct0estadoDTO> findEstadosSugueridasVisibles() {
    final List<Integer> estados = new ArrayList<Integer>();
    estados.add(EstadosEnumerados.FACTURA_SUGERIDA.SUGERIDA.getId());
    estados.add(EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA.getId());
    return getEstadosDTO(dao.findAll(estados));
  }

  /**
   * @param estados
   * @param estadosDTO
   */
  private List<Tmct0estadoDTO> getEstadosDTO(final List<Tmct0estado> estados) {
    final List<Tmct0estadoDTO> estadosDTO = new ArrayList<Tmct0estadoDTO>();
    if (CollectionUtils.isNotEmpty(estados)) {
      for (Tmct0estado estado : estados) {
        estadosDTO.add(new Tmct0estadoDTO(estado));
      }
    }
    return estadosDTO;
  }

  private List<EstadosEquivalentesDTO> findAllEstadosEquivalentes() {
    List<EstadosEquivalentesDTO> estadosEquivalentes = new ArrayList<EstadosEquivalentesDTO>();
    List<Object[]> estadosEquivalentesArray = dao.findAllEstadosEquivalentes();
    if (CollectionUtils.isNotEmpty(estadosEquivalentesArray)) {
      for (Object[] o : estadosEquivalentesArray) {
        estadosEquivalentes.add(new EstadosEquivalentesDTO((Integer) o[0], (Integer) o[1], (Integer) o[2]));
      }
    }
    return estadosEquivalentes;
  }

  public Map<String, Integer> getMapEstadosEquivalentes() {
    Map<String, Integer> map = new HashMap<String, Integer>();
    List<EstadosEquivalentesDTO> estadosEquivalentes = this.findAllEstadosEquivalentes();
    String key;
    for (EstadosEquivalentesDTO estadosEquivalentesDTO : estadosEquivalentes) {
      key = String.format(STRING_FORMAT_KEY_ESTADOS_EQUIVALENTES, estadosEquivalentesDTO.getId_estado1(),
          estadosEquivalentesDTO.getId_estado2());
      map.put(key, estadosEquivalentesDTO.getId_estado_equivalente());
    }
    return map;
  }

  public Integer getEstadoEquivalente(Map<String, Integer> cachedValues, int estado1, int estado2) {
    return cachedValues.get(String.format(STRING_FORMAT_KEY_ESTADOS_EQUIVALENTES, estado1, estado2));
  }
}
