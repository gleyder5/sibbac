package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Texto }.
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
@Table( name = WRAPPERS.TEXTO_IDIOMA )
@Entity
@Audited
public class TextosIdiomas extends ATableAudited< TextosIdiomas > implements java.io.Serializable {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 1303703050839761566L;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_IDIOMA", referencedColumnName = "ID", nullable = false )
	private Idioma				idioma;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_TEXTO", referencedColumnName = "ID", nullable = false )
	private Texto				texto;

	@Column( name = "NBDESCRIPCIONES", length = 200 )
	private String				descripcion;

	public TextosIdiomas() {
	}

	/**
	 * @return the idioma
	 */
	public Idioma getIdioma() {
		return idioma;
	}

	/**
	 * @param idioma
	 *            the idioma to set
	 */
	public void setIdioma( Idioma idioma ) {
		this.idioma = idioma;
	}

	/**
	 * @return the texto
	 */
	public Texto getTexto() {
		return texto;
	}

	/**
	 * @param texto
	 *            the texto to set
	 */
	public void setTexto( Texto texto ) {
		this.texto = texto;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {

		final ToStringBuilder tsb = new ToStringBuilder( ToStringStyle.SHORT_PREFIX_STYLE );
		tsb.append( getTexto() );
		tsb.append( getIdioma() );
		return tsb.toString();

	}

}
