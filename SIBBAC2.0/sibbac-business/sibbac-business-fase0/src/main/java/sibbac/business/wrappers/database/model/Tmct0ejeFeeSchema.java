package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
@Table(name = "TMCT0EJE_FEE_SCHEMA")
public class Tmct0ejeFeeSchema implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3183150325080195679L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "NUORDEN", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "NBOOKING", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "NUEJECUC", column = @Column(name = "NUEJECUC", nullable = false, length = 20)) })
  private Tmct0ejeFeeSchemaId id;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUEJECUC", referencedColumnName = "NUEJECUC", nullable = false, insertable = false, updatable = false) })
  private Tmct0eje tmct0eje;

  @Column(name = "TIPO_TARIFA", nullable = false)
  private char tipoTarifa;

  @Column(name = "SUBASTA_APERTURA", nullable = false)
  private char subastaApertura;

  @Column(name = "SUBASTA_VOLATILIDAD", nullable = false)
  private char subastaVolatilidad;

  @Column(name = "SUBASTA_CIERRE", nullable = false)
  private char subastaCierre;

  @Column(name = "RESTRICCION_ORDEN", nullable = false)
  private char restriccionOrden;

  @Column(name = "ORDEN_VOLUMEN_OCULTO", nullable = false)
  private char ordenVolumenOculto;

  @Column(name = "ORDEN_PUNTO_MEDIO", nullable = false)
  private char ordenPuntoMedio;

  @Column(name = "ORDEN_OCULTA", nullable = false)
  private char ordenOculta;

  @Column(name = "ORDEN_BLOQUE_COMBINADO", nullable = false)
  private char ordenBloqueCombinado;

  @Version
  @Column(name = "VERSION", nullable = true)
  private Integer version;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", nullable = true)
  private Date fhaudit;

  @Column(name = "CDUSUAUD", nullable = true)
  private String cdusuaud;

  public Tmct0eje getTmct0eje() {
    return tmct0eje;
  }

  public Tmct0ejeFeeSchemaId getId() {
    return id;
  }

  public void setId(Tmct0ejeFeeSchemaId id) {
    this.id = id;
  }

  public char getTipoTarifa() {
    return tipoTarifa;
  }

  public char getSubastaApertura() {
    return subastaApertura;
  }

  public char getSubastaVolatilidad() {
    return subastaVolatilidad;
  }

  public char getSubastaCierre() {
    return subastaCierre;
  }

  public char getRestriccionOrden() {
    return restriccionOrden;
  }

  public char getOrdenVolumenOculto() {
    return ordenVolumenOculto;
  }

  public char getOrdenPuntoMedio() {
    return ordenPuntoMedio;
  }

  public char getOrdenOculta() {
    return ordenOculta;
  }

  public char getOrdenBloqueCombinado() {
    return ordenBloqueCombinado;
  }

  public Integer getVersion() {
    return version;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setTmct0eje(Tmct0eje tmct0eje) {
    this.tmct0eje = tmct0eje;
  }

  public void setTipoTarifa(char tipoTarifa) {
    this.tipoTarifa = tipoTarifa;
  }

  public void setSubastaApertura(char subastaApertura) {
    this.subastaApertura = subastaApertura;
  }

  public void setSubastaVolatilidad(char subastaVolatilidad) {
    this.subastaVolatilidad = subastaVolatilidad;
  }

  public void setSubastaCierre(char subastaCierre) {
    this.subastaCierre = subastaCierre;
  }

  public void setRestriccionOrden(char restriccionOrden) {
    this.restriccionOrden = restriccionOrden;
  }

  public void setOrdenVolumenOculto(char ordenVolumenOculto) {
    this.ordenVolumenOculto = ordenVolumenOculto;
  }

  public void setOrdenPuntoMedio(char ordenPuntoMedio) {
    this.ordenPuntoMedio = ordenPuntoMedio;
  }

  public void setOrdenOculta(char ordenOculta) {
    this.ordenOculta = ordenOculta;
  }

  public void setOrdenBloqueCombinado(char ordenBloqueCombinado) {
    this.ordenBloqueCombinado = ordenBloqueCombinado;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

}
