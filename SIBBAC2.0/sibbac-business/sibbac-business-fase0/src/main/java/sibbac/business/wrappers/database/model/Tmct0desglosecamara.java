package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0estado;

@Entity
@Table(name = "TMCT0DESGLOSECAMARA")
public class Tmct0desglosecamara implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -3056574443577009414L;
  private Long iddesglosecamara;
  private Tmct0CamaraCompensacion tmct0CamaraCompensacion;
  private Tmct0alc tmct0alc;
  private Tmct0enviotitularidad tmct0enviotitularidad;
  private Tmct0estado tmct0estadoByCdestadocont;
  private Tmct0estado tmct0estadoByCdestadoasig;
  private Tmct0estado tmct0estadoByCdestadotit;
  private Tmct0estado tmct0estadoByCdestadoentrec;
  private Tmct0infocompensacion tmct0infocompensacion;
  private BigDecimal nutitulos = new BigDecimal(0);
  private char asigorden = 'N';
  private char casepti = 'N';
  private String auditUser;
  private Date auditFechaCambio;
  private Date feliquidacion = new Date();
  private Date fecontratacion = new Date();
  private Character envios3;
  private Date fhenvios3;
  private Character imputacionpropia;
  private Character sentido;
  private String isin;
  private String estadoCNMV;
  
  
  /**
   * Número de títulos fallidos si el sentido de la operación es 'venta' y número de títulos afectados si el sentido de
   * la operación es 'compra'.
   */
  private BigDecimal nutitfallidos;

  private List<Tmct0movimientoecc> tmct0movimientos = new ArrayList<Tmct0movimientoecc>();

  private List<Tmct0desgloseclitit> tmct0desgloseclitits = new ArrayList<Tmct0desgloseclitit>();

  private List<Tmct0DesgloseCamaraEnvioRt> tmct0DesgloseCamaraEnvioRt = new ArrayList<Tmct0DesgloseCamaraEnvioRt>();

  public Tmct0desglosecamara() {
  }

  public Tmct0desglosecamara(long iddesglosecamara,
                             Tmct0CamaraCompensacion tmct0CamaraCompensacion,
                             Tmct0alc tmct0alc,
                             Tmct0estado tmct0estadoByCdestadoasig,
                             Tmct0estado tmct0estadoByCdestadotit,
                             Tmct0infocompensacion tmct0infocompensacion,
                             BigDecimal nutitulos,
                             char asigorden,
                             char casepti,
                             Date feliquidacion,
                             Date fecontratacion) {
    this.iddesglosecamara = iddesglosecamara;
    this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
    this.tmct0alc = tmct0alc;
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
    this.tmct0estadoByCdestadotit = tmct0estadoByCdestadotit;
    this.tmct0infocompensacion = tmct0infocompensacion;
    this.nutitulos = nutitulos;
    this.asigorden = asigorden;
    this.casepti = casepti;
    this.feliquidacion = feliquidacion;
    this.fecontratacion = fecontratacion;
  }

  public Tmct0desglosecamara(long iddesglosecamara,
                             Tmct0CamaraCompensacion tmct0CamaraCompensacion,
                             Tmct0alc tmct0alc,
                             Tmct0enviotitularidad tmct0enviotitularidad,
                             Tmct0estado tmct0estadoByCdestadocont,
                             Tmct0estado tmct0estadoByCdestadoasig,
                             Tmct0estado tmct0estadoByCdestadotit,
                             Tmct0estado tmct0estadoByCdestadoentrec,
                             Tmct0infocompensacion tmct0infocompensacion,
                             BigDecimal nutitulos,
                             char asigorden,
                             char casepti,
                             String auditUser,
                             Date auditFechaCambio,
                             Date feliquidacion,
                             Date fecontratacion,
                             Character envios3,
                             Character imputacionpropia,
                             Date fhenvios3,
                             List<Tmct0desgloseclitit> tmct0desgloseclitits) {
    this.iddesglosecamara = iddesglosecamara;
    this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
    this.tmct0alc = tmct0alc;
    this.tmct0enviotitularidad = tmct0enviotitularidad;
    this.tmct0estadoByCdestadocont = tmct0estadoByCdestadocont;
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
    this.tmct0estadoByCdestadotit = tmct0estadoByCdestadotit;
    this.tmct0estadoByCdestadoentrec = tmct0estadoByCdestadoentrec;
    this.tmct0infocompensacion = tmct0infocompensacion;
    this.nutitulos = nutitulos;
    this.asigorden = asigorden;
    this.casepti = casepti;
    this.auditUser = auditUser;
    this.auditFechaCambio = auditFechaCambio;
    this.feliquidacion = feliquidacion;
    this.fecontratacion = fecontratacion;
    this.envios3 = envios3;
    this.imputacionpropia = imputacionpropia;
    this.fhenvios3 = fhenvios3;
    this.tmct0desgloseclitits = tmct0desgloseclitits;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IDDESGLOSECAMARA", unique = true, nullable = false)
  public Long getIddesglosecamara() {
    return this.iddesglosecamara;
  }

  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDCAMARA", nullable = false)
  public Tmct0CamaraCompensacion getTmct0CamaraCompensacion() {
    return this.tmct0CamaraCompensacion;
  }

  public void setTmct0CamaraCompensacion(Tmct0CamaraCompensacion tmct0CamaraCompensacion) {
    this.tmct0CamaraCompensacion = tmct0CamaraCompensacion;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false), @JoinColumn(name = "NUORDEN",
                                                                                                                  referencedColumnName = "NUORDEN",
                                                                                                                  nullable = false), @JoinColumn(name = "NUCNFLIQ",
                                                                                                                                                 referencedColumnName = "NUCNFLIQ",
                                                                                                                                                 nullable = false), @JoinColumn(name = "NUCNFCLT",
                                                                                                                                                                                referencedColumnName = "NUCNFCLT",
                                                                                                                                                                                nullable = false)
  })
  public Tmct0alc getTmct0alc() {
    return this.tmct0alc;
  }

  public void setTmct0alc(Tmct0alc tmct0alc) {
    this.tmct0alc = tmct0alc;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDENVIO")
  public Tmct0enviotitularidad getTmct0enviotitularidad() {
    return this.tmct0enviotitularidad;
  }

  public void setTmct0enviotitularidad(Tmct0enviotitularidad tmct0enviotitularidad) {
    this.tmct0enviotitularidad = tmct0enviotitularidad;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT")
  public Tmct0estado getTmct0estadoByCdestadocont() {
    return this.tmct0estadoByCdestadocont;
  }

  public void setTmct0estadoByCdestadocont(Tmct0estado tmct0estadoByCdestadocont) {
    this.tmct0estadoByCdestadocont = tmct0estadoByCdestadocont;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOASIG", nullable = false)
  public Tmct0estado getTmct0estadoByCdestadoasig() {
    return this.tmct0estadoByCdestadoasig;
  }

  public void setTmct0estadoByCdestadoasig(Tmct0estado tmct0estadoByCdestadoasig) {
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOTIT", nullable = false)
  public Tmct0estado getTmct0estadoByCdestadotit() {
    return this.tmct0estadoByCdestadotit;
  }

  public void setTmct0estadoByCdestadotit(Tmct0estado tmct0estadoByCdestadotit) {
    this.tmct0estadoByCdestadotit = tmct0estadoByCdestadotit;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOENTREC")
  public Tmct0estado getTmct0estadoByCdestadoentrec() {
    return this.tmct0estadoByCdestadoentrec;
  }

  public void setTmct0estadoByCdestadoentrec(Tmct0estado tmct0estadoByCdestadoentrec) {
    this.tmct0estadoByCdestadoentrec = tmct0estadoByCdestadoentrec;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDINFOCOMPENSACION", nullable = true)
  public Tmct0infocompensacion getTmct0infocompensacion() {
    return this.tmct0infocompensacion;
  }

  public void setTmct0infocompensacion(Tmct0infocompensacion tmct0infocompensacion) {
    this.tmct0infocompensacion = tmct0infocompensacion;
  }

  @Column(name = "NUTITULOS", nullable = false, precision = 12, scale = 6)
  public BigDecimal getNutitulos() {
    return this.nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  @Column(name = "ASIGORDEN", nullable = false, length = 1)
  public char getAsigorden() {
    return this.asigorden;
  }

  public void setAsigorden(char asigorden) {
    this.asigorden = asigorden;
  }

  @Column(name = "CASEPTI", nullable = false, length = 1)
  public char getCasepti() {
    return this.casepti;
  }

  public void setCasepti(char casepti) {
    this.casepti = casepti;
  }

  @Column(name = "AUDIT_USER", length = 20)
  public String getAuditUser() {
    return this.auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "AUDIT_FECHA_CAMBIO", length = 26)
  public Date getAuditFechaCambio() {
    return this.auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FELIQUIDACION", nullable = false, length = 10)
  public Date getFeliquidacion() {
    return this.feliquidacion;
  }

  public void setFeliquidacion(Date feliquidacion) {
    this.feliquidacion = feliquidacion;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECONTRATACION", nullable = false, length = 10)
  public Date getFecontratacion() {
    return this.fecontratacion;
  }

  public void setFecontratacion(Date fecontratacion) {
    this.fecontratacion = fecontratacion;
  }

  @Column(name = "ENVIOS3", length = 1)
  public Character getEnvios3() {
    return this.envios3;
  }

  public void setEnvios3(Character envios3) {
    this.envios3 = envios3;
  }

  /**
   * @return the imputacionPropia
   */
  @Column(name = "IMPUTACIONPROPIA", length = 1)
  public Character getImputacionpropia() {
    return imputacionpropia;
  }

  /**
   * @param imputacionpropia the imputacionpropia to set
   */
  public void setImputacionpropia(Character imputacionpropia) {
    this.imputacionpropia = imputacionpropia;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHENVIOS3", length = 26)
  public Date getFhenvios3() {
    return this.fhenvios3;
  }

  public void setFhenvios3(Date fhenvios3) {
    this.fhenvios3 = fhenvios3;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0desglosecamara")
  public List<Tmct0movimientoecc> getTmct0movimientos() {
    return tmct0movimientos;
  }

  public void setTmct0movimientos(List<Tmct0movimientoecc> tmct0movimientos) {
    this.tmct0movimientos = tmct0movimientos;
  }

  // @OneToMany( cascade = { CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "tmct0desglosecamara" )
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0desglosecamara")
  public List<Tmct0desgloseclitit> getTmct0desgloseclitits() {
    return this.tmct0desgloseclitits;
  }

  public void setTmct0desgloseclitits(List<Tmct0desgloseclitit> tmct0desgloseclitits) {
    this.tmct0desgloseclitits = tmct0desgloseclitits;
  }

  @Transactional
  public boolean addTmct0desgloseclitits(final Tmct0desgloseclitit dct) {
    if (CollectionUtils.isEmpty(tmct0desgloseclitits)) {
      this.tmct0desgloseclitits = new ArrayList<Tmct0desgloseclitit>();
    }
    dct.setTmct0desglosecamara(this);

    return tmct0desgloseclitits.add(dct);

  }

  @Column(name = "NUTITFALLIDOS", precision = 18, scale = 6)
  public BigDecimal getNutitfallidos() {
    return this.nutitfallidos;
  }

  public void setNutitfallidos(BigDecimal nutitfallidos) {
    this.nutitfallidos = nutitfallidos;
  }

  @Column(name = "SENTIDO", length = 1)
  public Character getSentido() {
    return sentido;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  @Column(name = "ISIN", length = 12)
  public String getIsin() {
    return isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  
  @Column(name = "ESTADOCNMV", length = 3)
  public String getEstadoCNMV() {
    return estadoCNMV;
  }

  public void setEstadoCNMV(String estadoCNMV) {
    this.estadoCNMV = estadoCNMV;
  }  

  
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "desgloseCamara")
  public List<Tmct0DesgloseCamaraEnvioRt> getTmct0DesgloseCamaraEnvioRt() {
    return tmct0DesgloseCamaraEnvioRt;
  }



  public void setTmct0DesgloseCamaraEnvioRt(List<Tmct0DesgloseCamaraEnvioRt> tmct0DesgloseCamaraEnvioRt) {
    this.tmct0DesgloseCamaraEnvioRt = tmct0DesgloseCamaraEnvioRt;
  }
  
  /**
   * Determina la cuenta de compensacion asociada a este desglose camara.
   * @return el campo cuentaCompensacionDestino del primer movimiento ecc
   * que esta de alta (estado 9). Nulo en caso de haber una cuenta de 
   * compensacion
   */
  @Transient
  public String getCuentaCompensacion() {
    String cdEstadoMov;
    
    if (tmct0movimientos != null) {
      for (Tmct0movimientoecc mov : tmct0movimientos) {
        cdEstadoMov = mov.getCdestadomov();
        if (cdEstadoMov != null && cdEstadoMov.trim().equals("9")) {
          return mov.getCuentaCompensacionDestino();
        }
      }
    }
    return null;
  }

}

