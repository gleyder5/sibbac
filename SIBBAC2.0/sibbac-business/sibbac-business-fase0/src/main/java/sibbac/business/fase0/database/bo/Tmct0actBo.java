package sibbac.business.fase0.database.bo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0actDao;
import sibbac.business.fase0.database.dao.Tmct0actDaoImp;
import sibbac.business.fase0.database.dto.Tmct0actDTO;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0actId;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0actBo extends AbstractBo<Tmct0act, Tmct0actId, Tmct0actDao> {

  @Autowired
  private Tmct0actDao tmct0actDao;

  @Autowired
  private Tmct0actDaoImp tmct0actDaoimp;

  /**
   * Recupera la lista de alias activos
   */
  public List<Tmct0act> findAliasSubuentasActivos() {

    return this.dao.findAliasSubuentasActivos(Tmct0ali.fhfinActivos);

  }

  public List<Tmct0act> findByCdaliassAndCdsubctaAndFecha(final String alias, final String subCta, final Integer fecha) {
    return dao.findByCdaliassAndCdsubctaAndFecha(alias, subCta, fecha);
  }

  public List<Tmct0actId> extractIds(final List<Tmct0act> tmct0acts) {
    final List<Tmct0actId> tmct0aliIds = new ArrayList<Tmct0actId>();
    if (CollectionUtils.isNotEmpty(tmct0acts)) {
      for (final Tmct0act tmct0act : tmct0acts) {
        tmct0aliIds.add(tmct0act.getId());
      }
    }
    return tmct0aliIds;
  }

  // Devuelve una lista de las subcuentas
  @Transactional
  public List<Tmct0act> getListaSubCta(String alias) {
    return tmct0actDaoimp.findSubCtasUnicas(alias);
  }

  @Transactional
  public List<Tmct0act> getSubCtaBro(String alias, String subCta, Integer fhfinal) {
    // List< Tmct0act > retorno = new ArrayList< Tmct0act >();
    return tmct0actDao.findByCdaliassAndCdsubcta(alias, subCta, fhfinal);
    // return retorno;
  }

  public List<Tmct0act> findSubctaById_regla(Long Id_regla) {
    return dao.findSubctaById_regla(Id_regla);
  }

  public List<Tmct0actDTO> findSubctaByManyReglas(List<Long> Id_regla) {
    return tmct0actDaoimp.findSubCtasBymanyReglas(Id_regla);
  }

  @Transactional
  public Tmct0act getAliasSubCta(String alias, String subCta) {
    // List< Tmct0act > retorno = new ArrayList< Tmct0act >();
    return tmct0actDao.findByCdaliassAndCdsubcta2(alias, subCta);
    // return retorno;
  }

}
