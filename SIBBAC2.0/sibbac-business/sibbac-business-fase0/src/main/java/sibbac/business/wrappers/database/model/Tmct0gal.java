package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0GAL. EG Allocations Documentación para
 * TMCT0GAL <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0GAL")
public class Tmct0gal implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4237309535400568498L;
  // @formatter:off
  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nuagreje", column = @Column(name = "NUAGREJE", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)) })
  private Tmct0galId id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUAGREJE", referencedColumnName = "NUAGREJE", nullable = false, insertable = false, updatable = false) })
  private Tmct0gre tmct0gre;

  // @formatter:on
  /**
   * Table Field IMTPCAMB. Exchange rate with EUR Documentación para IMTPCAMB
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMTPCAMB", length = 18, scale = 8)
  protected BigDecimal imtpcamb;
  /**
   * Table Field CDMONISO. ISO currency code Documentación para CDMONISO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDMONISO", length = 3)
  protected java.lang.String cdmoniso;
  /**
   * Table Field CDINDGAS. Flag showing if the client pays market charges
   * Documentación para CDINDGAS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDINDGAS", length = 1)
  protected Character cdindgas;
  /**
   * Table Field CDINDIMP. Flag showing if the client pays market taxes
   * Documentación para CDINDIMP <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDINDIMP", length = 1)
  protected Character cdindimp;
  /**
   * Table Field CDRESIDE. Is the client resident on the market we traded on?
   * (Yes:"S" or No:"N") Documentación para CDRESIDE <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDRESIDE", length = 1)
  protected Character cdreside;
  /**
   * Table Field IMBRMERC. Gross market consideration in order currency
   * Documentación para IMBRMERC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMBRMERC", length = 18, scale = 8)
  protected BigDecimal imbrmerc;
  /**
   * Table Field IMBRMREU. Gross market consideration in EUR Documentación para
   * IMBRMREU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMBRMREU", length = 18, scale = 8)
  protected BigDecimal imbrmreu;
  /**
   * Table Field IMGASTOS. Market Charges in EUR Documentación para IMGASTOS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASTOS", length = 18, scale = 8)
  protected BigDecimal imgastos;
  /**
   * Table Field IMGATDVS. Market Charges in order currency Documentación para
   * IMGATDVS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGATDVS", length = 18, scale = 8)
  protected BigDecimal imgatdvs;
  /**
   * Table Field IMIMPDVS. Market Taxes in order currency Documentación para
   * IMIMPDVS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMIMPDVS", length = 18, scale = 8)
  protected BigDecimal imimpdvs;
  /**
   * Table Field IMIMPEUR. Market Taxes in EUR Documentación para IMIMPEUR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMIMPEUR", length = 18, scale = 8)
  protected BigDecimal imimpeur;
  /**
   * Table Field IMCCONEU. Spanish trading market charges in EUR Documentación
   * para IMCCONEU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCCONEU", length = 18, scale = 8)
  protected BigDecimal imcconeu;
  /**
   * Table Field IMCLIQEU. Spanish clearing market charges in EUR Documentación
   * para IMCLIQEU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCLIQEU", length = 18, scale = 8)
  protected BigDecimal imcliqeu;
  /**
   * Table Field IMDEREEU. Rights amount in EUR Documentación para IMDEREEU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMDEREEU", length = 18, scale = 8)
  protected BigDecimal imdereeu;
  /**
   * Table Field XTDCM. Rights Calculation method Documentación para XTDCM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "XTDCM", length = 1)
  protected Character xtdcm;
  /**
   * Table Field XTDERECH. Is the client paying for the Rights? S/N
   * Documentación para XTDERECH <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "XTDERECH", length = 1)
  protected Character xtderech;
  /**
   * Table Field IMCAMBIO. Average price Documentación para IMCAMBIO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCAMBIO", length = 18, scale = 8)
  protected BigDecimal imcambio;
  /**
   * Table Field NUTITAGR. Grouped volume Documentación para NUTITAGR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUTITAGR", length = 16, scale = 5)
  protected BigDecimal nutitagr;
  /**
   * Table Field FEEJECUC. Date-time of last execution Documentación para
   * FEEJECUC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC")
  protected java.util.Date feejecuc;
  /**
   * Table Field HOEJECUC. Time of last execution Documentación para HOEJECUC
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIME)
  @Column(name = "HOEJECUC")
  protected java.util.Date hoejecuc;
  /**
   * Table Field IMSVB. Commission SVB in order currency Documentación para
   * IMSVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMSVB", length = 18, scale = 8)
  protected BigDecimal imsvb;
  /**
   * Table Field IMSVBEU. Commission SVB in EUR Documentación para IMSVBEU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMSVBEU", length = 18, scale = 8)
  protected BigDecimal imsvbeu;
  /**
   * Table Field PCCOMSVB. Percentage commission SVB Documentación para PCCOMSVB
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PCCOMSVB", length = 16, scale = 6)
  protected BigDecimal pccomsvb;
  /**
   * Table Field IMCOMDEV. Rebate amount in order ccy Documentación para
   * IMCOMDEV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCOMDEV", length = 18, scale = 8)
  protected BigDecimal imcomdev;
  /**
   * Table Field EUCOMDEV. Rebate amount in EUR Documentación para EUCOMDEV <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "EUCOMDEV", length = 18, scale = 8)
  protected BigDecimal eucomdev;
  /**
   * Table Field IMTOTNET. Net consideration in order currency Documentación
   * para IMTOTNET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMTOTNET", length = 18, scale = 8)
  protected BigDecimal imtotnet;
  /**
   * Table Field EUTOTNET. Net consideration in EUR Documentación para EUTOTNET
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "EUTOTNET", length = 18, scale = 8)
  protected BigDecimal eutotnet;
  /**
   * Table Field IMCOMISN. Commission amount in order currency Documentación
   * para IMCOMISN <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCOMISN", length = 18, scale = 8)
  protected BigDecimal imcomisn;
  /**
   * Table Field IMCOMIEU. Commission amount in EUR Documentación para IMCOMIEU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMCOMIEU", length = 18, scale = 8)
  protected BigDecimal imcomieu;
  /**
   * Table Field IMFIBRDV. Broker commission in order currency Documentación
   * para IMFIBRDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMFIBRDV", length = 18, scale = 8)
  protected BigDecimal imfibrdv;
  /**
   * Table Field IMFIBREU. Broker commission in EUR Documentación para IMFIBREU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMFIBREU", length = 18, scale = 8)
  protected BigDecimal imfibreu;
  /**
   * Table Field PCCOMBRK. % Commission broker Documentación para PCCOMBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PCCOMBRK", length = 16, scale = 6)
  protected BigDecimal pccombrk;
  /**
   * Table Field IMBRMREUB. Gross market broker consideration in EUR
   * Documentación para IMBRMREUB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMBRMREUB", length = 18, scale = 8)
  protected BigDecimal imbrmreub;
  /**
   * Table Field IMGASTOSB. Market Charges Broker in EUR Documentación para
   * IMGASTOSB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASTOSB", length = 18, scale = 8)
  protected BigDecimal imgastosb;
  /**
   * Table Field IMBRMERCB. Gross market broker consideration in order currency
   * Documentación para IMBRMERCB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMBRMERCB", length = 18, scale = 8)
  protected BigDecimal imbrmercb;
  /**
   * Table Field IMGATDVSB. Market Charges Broker in order currency
   * Documentación para IMGATDVSB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGATDVSB", length = 18, scale = 8)
  protected BigDecimal imgatdvsb;
  /**
   * Table Field IMIMPDVSB. Market Taxes Broker in order currency Documentación
   * para IMIMPDVSB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMIMPDVSB", length = 18, scale = 8)
  protected BigDecimal imimpdvsb;
  /**
   * Table Field IMIMPEURB. Market Taxes Broker in EUR Documentación para
   * IMIMPEURB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMIMPEURB", length = 18, scale = 8)
  protected BigDecimal imimpeurb;
  /**
   * Table Field CDINDGASB. Flag showing if the broker pays market charges
   * Documentación para CDINDGASB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDINDGASB", length = 1)
  protected Character cdindgasb;
  /**
   * Table Field CDINDIMPB. Flag showing if the broker pays market taxes
   * Documentación para CDINDIMPB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDINDIMPB", length = 1)
  protected Character cdindimpb;
  /**
   * Table Field CDRESIDEB. Is the broker resident on the market we traded on?
   * (Yes:"S" or No:"N") Documentación para CDRESIDEB <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDRESIDEB", length = 1)
  protected Character cdresideb;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected java.util.Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NUVERSION")
  protected java.lang.Integer nuversion;

  /**
   * Table Field IMGASLIQDV. Gastos Liquidacion en Divisa Documentación para
   * IMGASLIQDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASLIQDV", length = 18, scale = 8)
  protected BigDecimal imgasliqdv;
  /**
   * Table Field IMGASLIQEU. Gastos Liquidacion en EUR Documentación para
   * IMGASLIQEU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASLIQEU", length = 18, scale = 8)
  protected BigDecimal imgasliqeu;
  /**
   * Table Field IMGASGESDV. Gastos Gestion en Divisa Documentación para
   * IMGASGESDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASGESDV", length = 18, scale = 8)
  protected BigDecimal imgasgesdv;
  /**
   * Table Field IMGASGESEU. Gastos Gestion en EUR Documentación para IMGASGESEU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASGESEU", length = 18, scale = 8)
  protected BigDecimal imgasgeseu;
  /**
   * Table Field IMGASTOTDV. Gastos Totales en Divisa Documentación para
   * IMGASTOTDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASTOTDV", length = 18, scale = 8)
  protected BigDecimal imgastotdv;
  /**
   * Table Field IMGASTOTEU. Gastos Totales en EUR Documentación para IMGASTOTEU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASTOTEU", length = 18, scale = 8)
  protected BigDecimal imgastoteu;
  /**
   * Table Field IMGASLIQDVB. Gastos Liquidacion en Divisa Documentación para
   * IMGASLIQDVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASLIQDVB", length = 18, scale = 8)
  protected BigDecimal imgasliqdvb;
  /**
   * Table Field IMGASLIQEUB. Gastos Liquidacion en EUR Documentación para
   * IMGASLIQEUB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASLIQEUB", length = 18, scale = 8)
  protected BigDecimal imgasliqeub;
  /**
   * Table Field IMGASGESQDVB. Gastos Gestion en Divisa Documentación para
   * IMGASGESDVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASGESDVB", length = 18, scale = 8)
  protected BigDecimal imgasgesdvb;
  /**
   * Table Field IMGASGESEUB. Gastos Gestion en EUR Documentación para
   * IMGASGESEUB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASGESEUB", length = 18, scale = 8)
  protected BigDecimal imgasgeseub;
  /**
   * Table Field IMGASTOTDVB. Gastos Totales en Divisa Documentación para
   * IMGASTOTDVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASTOTDVB", length = 18, scale = 8)
  protected BigDecimal imgastotdvb;
  /**
   * Table Field IMGASTOTEUB. Gastos Totales en EUR Documentación para
   * IMGASTOTEUB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "IMGASTOTEUB", length = 18, scale = 8)
  protected BigDecimal imgastoteub;

  @Column(name = "CANONCONTRPAGOCLTE")
  private Boolean canonContrPagoClte;

  @Column(name = "CANONCONTRPAGOMKT")
  private Boolean canonContrPagoMkt;

  @Column(name = "CANONLIQPAGOCLTE")
  private Boolean canonLiqPagoClte;

  @Column(name = "CANONLIQPAGOMKT")
  private Boolean canonLiqPagoMkt;

  @Column(name = "CANONCOMPPAGOCLTE")
  private Boolean canonCompPagoClte;

  @Column(name = "CANONCOMPPAGOMKT")
  private Boolean canonCompPagoMkt;

  @Column(name = "IMCANONCOMPDV", length = 18, scale = 8)
  private BigDecimal imCanonCompDv;

  @Column(name = "IMCANONCOMPEU", length = 18, scale = 8)
  private BigDecimal imCanonCompEu;

  /**
   * Relation 1..n with table TMCT0GAE. Constraint de relación entre TMCT0GAE y
   * TMCT0GAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0gal")
  protected List<Tmct0gae> tmct0gaes = new ArrayList<Tmct0gae>(0);

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0gal", cascade = { CascadeType.PERSIST })
  protected List<Impuesto> impuestos = new ArrayList<Impuesto>(0);

  public Tmct0galId getId() {
    return id;
  }

  public Tmct0gre getTmct0gre() {
    return tmct0gre;
  }

  public void setTmct0gre(Tmct0gre tmct0gre) {
    this.tmct0gre = tmct0gre;
  }

  public BigDecimal getImtpcamb() {
    return imtpcamb;
  }

  public java.lang.String getCdmoniso() {
    return cdmoniso;
  }

  public Character getCdindgas() {
    return cdindgas;
  }

  public Character getCdindimp() {
    return cdindimp;
  }

  public Character getCdreside() {
    return cdreside;
  }

  public BigDecimal getImbrmerc() {
    return imbrmerc;
  }

  public BigDecimal getImbrmreu() {
    return imbrmreu;
  }

  public BigDecimal getImgastos() {
    return imgastos;
  }

  public BigDecimal getImgatdvs() {
    return imgatdvs;
  }

  public BigDecimal getImimpdvs() {
    return imimpdvs;
  }

  public BigDecimal getImimpeur() {
    return imimpeur;
  }

  public BigDecimal getImcconeu() {
    return imcconeu;
  }

  public BigDecimal getImcliqeu() {
    return imcliqeu;
  }

  public BigDecimal getImdereeu() {
    return imdereeu;
  }

  public Character getXtdcm() {
    return xtdcm;
  }

  public Character getXtderech() {
    return xtderech;
  }

  public BigDecimal getImcambio() {
    return imcambio;
  }

  public BigDecimal getNutitagr() {
    return nutitagr;
  }

  public java.util.Date getFeejecuc() {
    return feejecuc;
  }

  public java.util.Date getHoejecuc() {
    return hoejecuc;
  }

  public BigDecimal getImsvb() {
    return imsvb;
  }

  public BigDecimal getImsvbeu() {
    return imsvbeu;
  }

  public BigDecimal getPccomsvb() {
    return pccomsvb;
  }

  public BigDecimal getImcomdev() {
    return imcomdev;
  }

  public BigDecimal getEucomdev() {
    return eucomdev;
  }

  public BigDecimal getImtotnet() {
    return imtotnet;
  }

  public BigDecimal getEutotnet() {
    return eutotnet;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public BigDecimal getImcomieu() {
    return imcomieu;
  }

  public BigDecimal getImfibrdv() {
    return imfibrdv;
  }

  public BigDecimal getImfibreu() {
    return imfibreu;
  }

  public BigDecimal getPccombrk() {
    return pccombrk;
  }

  public BigDecimal getImbrmreub() {
    return imbrmreub;
  }

  public BigDecimal getImgastosb() {
    return imgastosb;
  }

  public BigDecimal getImbrmercb() {
    return imbrmercb;
  }

  public BigDecimal getImgatdvsb() {
    return imgatdvsb;
  }

  public BigDecimal getImimpdvsb() {
    return imimpdvsb;
  }

  public BigDecimal getImimpeurb() {
    return imimpeurb;
  }

  public Character getCdindgasb() {
    return cdindgasb;
  }

  public Character getCdindimpb() {
    return cdindimpb;
  }

  public Character getCdresideb() {
    return cdresideb;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public java.util.Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public BigDecimal getImgasliqdv() {
    return imgasliqdv;
  }

  public BigDecimal getImgasliqeu() {
    return imgasliqeu;
  }

  public BigDecimal getImgasgesdv() {
    return imgasgesdv;
  }

  public BigDecimal getImgasgeseu() {
    return imgasgeseu;
  }

  public BigDecimal getImgastotdv() {
    return imgastotdv;
  }

  public BigDecimal getImgastoteu() {
    return imgastoteu;
  }

  public BigDecimal getImgasliqdvb() {
    return imgasliqdvb;
  }

  public BigDecimal getImgasliqeub() {
    return imgasliqeub;
  }

  public BigDecimal getImgasgesdvb() {
    return imgasgesdvb;
  }

  public BigDecimal getImgasgeseub() {
    return imgasgeseub;
  }

  public BigDecimal getImgastotdvb() {
    return imgastotdvb;
  }

  public BigDecimal getImgastoteub() {
    return imgastoteub;
  }

  public Boolean getCanonContrPagoClte() {
    return canonContrPagoClte;
  }

  public Boolean getCanonContrPagoMkt() {
    return canonContrPagoMkt;
  }

  public Boolean getCanonLiqPagoClte() {
    return canonLiqPagoClte;
  }

  public Boolean getCanonLiqPagoMkt() {
    return canonLiqPagoMkt;
  }

  public Boolean getCanonCompPagoClte() {
    return canonCompPagoClte;
  }

  public Boolean getCanonCompPagoMkt() {
    return canonCompPagoMkt;
  }

  public BigDecimal getImCanonCompDv() {
    return imCanonCompDv;
  }

  public BigDecimal getImCanonCompEu() {
    return imCanonCompEu;
  }

  public List<Tmct0gae> getTmct0gaes() {
    return tmct0gaes;
  }

  public List<Impuesto> getImpuestos() {
    return impuestos;
  }

  public void setId(Tmct0galId id) {
    this.id = id;
  }

  public void setImtpcamb(BigDecimal imtpcamb) {
    this.imtpcamb = imtpcamb;
  }

  public void setCdmoniso(java.lang.String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }

  public void setCdindimp(Character cdindimp) {
    this.cdindimp = cdindimp;
  }

  public void setCdreside(Character cdreside) {
    this.cdreside = cdreside;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  public void setXtdcm(Character xtdcm) {
    this.xtdcm = xtdcm;
  }

  public void setXtderech(Character xtderech) {
    this.xtderech = xtderech;
  }

  public void setImcambio(BigDecimal imcambio) {
    this.imcambio = imcambio;
  }

  public void setNutitagr(BigDecimal nutitagr) {
    this.nutitagr = nutitagr;
  }

  public void setFeejecuc(java.util.Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public void setHoejecuc(java.util.Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  public void setPccomsvb(BigDecimal pccomsvb) {
    this.pccomsvb = pccomsvb;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  public void setEucomdev(BigDecimal eucomdev) {
    this.eucomdev = eucomdev;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public void setEutotnet(BigDecimal eutotnet) {
    this.eutotnet = eutotnet;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public void setImcomieu(BigDecimal imcomieu) {
    this.imcomieu = imcomieu;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  public void setPccombrk(BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  public void setImbrmreub(BigDecimal imbrmreub) {
    this.imbrmreub = imbrmreub;
  }

  public void setImgastosb(BigDecimal imgastosb) {
    this.imgastosb = imgastosb;
  }

  public void setImbrmercb(BigDecimal imbrmercb) {
    this.imbrmercb = imbrmercb;
  }

  public void setImgatdvsb(BigDecimal imgatdvsb) {
    this.imgatdvsb = imgatdvsb;
  }

  public void setImimpdvsb(BigDecimal imimpdvsb) {
    this.imimpdvsb = imimpdvsb;
  }

  public void setImimpeurb(BigDecimal imimpeurb) {
    this.imimpeurb = imimpeurb;
  }

  public void setCdindgasb(Character cdindgasb) {
    this.cdindgasb = cdindgasb;
  }

  public void setCdindimpb(Character cdindimpb) {
    this.cdindimpb = cdindimpb;
  }

  public void setCdresideb(Character cdresideb) {
    this.cdresideb = cdresideb;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(java.util.Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  public void setImgasliqdv(BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  public void setImgasliqeu(BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  public void setImgasgesdv(BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  public void setImgasgeseu(BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  public void setImgastotdv(BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  public void setImgastoteu(BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  public void setImgasliqdvb(BigDecimal imgasliqdvb) {
    this.imgasliqdvb = imgasliqdvb;
  }

  public void setImgasliqeub(BigDecimal imgasliqeub) {
    this.imgasliqeub = imgasliqeub;
  }

  public void setImgasgesdvb(BigDecimal imgasgesdvb) {
    this.imgasgesdvb = imgasgesdvb;
  }

  public void setImgasgeseub(BigDecimal imgasgeseub) {
    this.imgasgeseub = imgasgeseub;
  }

  public void setImgastotdvb(BigDecimal imgastotdvb) {
    this.imgastotdvb = imgastotdvb;
  }

  public void setImgastoteub(BigDecimal imgastoteub) {
    this.imgastoteub = imgastoteub;
  }

  public void setCanonContrPagoClte(Boolean canonContrPagoClte) {
    this.canonContrPagoClte = canonContrPagoClte;
  }

  public void setCanonContrPagoMkt(Boolean canonContrPagoMkt) {
    this.canonContrPagoMkt = canonContrPagoMkt;
  }

  public void setCanonLiqPagoClte(Boolean canonLiqPagoClte) {
    this.canonLiqPagoClte = canonLiqPagoClte;
  }

  public void setCanonLiqPagoMkt(Boolean canonLiqPagoMkt) {
    this.canonLiqPagoMkt = canonLiqPagoMkt;
  }

  public void setCanonCompPagoClte(Boolean canonCompPagoClte) {
    this.canonCompPagoClte = canonCompPagoClte;
  }

  public void setCanonCompPagoMkt(Boolean canonCompPagoMkt) {
    this.canonCompPagoMkt = canonCompPagoMkt;
  }

  public void setImCanonCompDv(BigDecimal imCanonCompDv) {
    this.imCanonCompDv = imCanonCompDv;
  }

  public void setImCanonCompEu(BigDecimal imCanonCompEu) {
    this.imCanonCompEu = imCanonCompEu;
  }

  public void setTmct0gaes(List<Tmct0gae> tmct0gaes) {
    this.tmct0gaes = tmct0gaes;
  }

  public void setImpuestos(List<Impuesto> impuestos) {
    this.impuestos = impuestos;
  }

  @Override
  public String toString() {
    return "Tmct0gal [id=" + id + "]";
  }

}
