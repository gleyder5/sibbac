package sibbac.business.fase0.database.model;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0CFG" )
public class Tmct0cfg implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4704457301145821316L;
	private Tmct0cfgId			id;
	private String				keyvalue;
	private String				cdauxili;

	public Tmct0cfg() {
	}

	public Tmct0cfg( Tmct0cfgId id, String keyvalue, String cdauxili ) {
		this.id = id;
		this.keyvalue = keyvalue;
		this.cdauxili = cdauxili;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "application", column = @Column( name = "APPLICATION", nullable = false, length = 50 ) ),
			@AttributeOverride( name = "process", column = @Column( name = "PROCESS", nullable = false, length = 50 ) ),
			@AttributeOverride( name = "keyname", column = @Column( name = "KEYNAME", nullable = false, length = 100 ) )
	} )
	public Tmct0cfgId getId() {
		return this.id;
	}

	public void setId( Tmct0cfgId id ) {
		this.id = id;
	}

	@Column( name = "KEYVALUE", nullable = false, length = 500 )
	public String getKeyvalue() {
		return this.keyvalue;
	}

	public void setKeyvalue( String keyvalue ) {
		this.keyvalue = keyvalue;
	}

	@Column( name = "CDAUXILI", nullable = false, length = 25 )
	public String getCdauxili() {
		return this.cdauxili;
	}

	public void setCdauxili( String cdauxili ) {
		this.cdauxili = cdauxili;
	}

}
