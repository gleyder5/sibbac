package sibbac.business.fase0.database.bo;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.CodigoOperacionDao;
import sibbac.business.fase0.database.dao.ConceptosMovimientoEfectivoDao;
import sibbac.business.fase0.database.dao.DepositariosValoresDao;
import sibbac.business.fase0.database.dao.GrupoValoresDao;
import sibbac.business.fase0.database.dao.ModosMatGarantiaDao;
import sibbac.business.fase0.database.dao.TiposActivoDao;
import sibbac.business.fase0.database.dao.TiposGarantiaExigidaDao;
import sibbac.business.fase0.database.dao.TiposPersonaDao;
import sibbac.business.fase0.database.dao.TiposProductoDao;
import sibbac.business.fase0.database.model.CodigoOperacion;
import sibbac.business.fase0.database.model.ConceptosMovimientoEfectivo;
import sibbac.business.fase0.database.model.DepositariosValores;
import sibbac.business.fase0.database.model.GrupoValores;
import sibbac.business.fase0.database.model.ModosMatGarantia;
import sibbac.business.fase0.database.model.TiposActivo;
import sibbac.business.fase0.database.model.TiposGarantiaExigida;
import sibbac.business.fase0.database.model.TiposPersona;
import sibbac.business.fase0.database.model.TiposProducto;


@Service
public class DatosMaestrosService {

	@Autowired
	CodigoOperacionDao				codigoOperacionDao;

	@Autowired
	GrupoValoresDao					grupoValoreslDao;

	@Autowired
	TiposProductoDao				tiposProductoDao;

	@Autowired
	TiposPersonaDao					tiposPersonaDao;

	@Autowired
	TiposGarantiaExigidaDao			tiposGarantiaExigidaDao;

	@Autowired
	ConceptosMovimientoEfectivoDao	conceptosMovimientoEfectivoDao;

	@Autowired
	ModosMatGarantiaDao				modosMatGarantiaDao;

	@Autowired
	DepositariosValoresDao			depositariosValoresDao;

	@Autowired
	TiposActivoDao					tiposActivoDao;

	public List< CodigoOperacion > getCodigosOperacion() {
		return codigoOperacionDao.findAll();
	}

	public List< GrupoValores > getGrupoValores() {
		return grupoValoreslDao.findAll();
	}

	public List< TiposProducto > getTiposProducto() {
		return tiposProductoDao.findAll();
	}

	public List< TiposPersona > getTiposPersona() {
		return tiposPersonaDao.findAll();
	}

	public List< TiposGarantiaExigida > getTiposGarantiasExigidas() {
		return tiposGarantiaExigidaDao.findAll();
	}

	public List< ConceptosMovimientoEfectivo > getConceptosMovimientoEfectivos() {
		return conceptosMovimientoEfectivoDao.findAll();
	}

	public List< ModosMatGarantia > getModosMatGarantias() {
		return modosMatGarantiaDao.findAll();
	}

	public List< DepositariosValores > getDepositariosValores() {
		return depositariosValoresDao.findAll();
	}

	public List< TiposActivo > getTiposActivos() {
		return tiposActivoDao.findAll();
	}
}
