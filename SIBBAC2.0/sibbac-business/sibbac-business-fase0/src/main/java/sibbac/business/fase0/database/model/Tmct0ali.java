package sibbac.business.fase0.database.model;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


@Entity
@Table( name = "TMCT0ALI" )
public class Tmct0ali implements java.io.Serializable {

	public final static int		fhfinActivos		= 99991231;

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1521816108058111085L;
	private Tmct0aliId			id;
	private String				descrali;
	private Character			tpdsaldo;
	private Character			routing;
	private BigDecimal			pccombco;
	private String				cdcomisn;
	private String				cdmodord;
	private BigDecimal			pccombrk;
	private Character			desglose;
	private Character			mktfeed;
	private Character			fidessa;
	private Integer				fhsendfi;
	private String				cdusuaud;
	private Date				fhaudit;
	private Integer				fhinicio;
	private Integer				fhfinal;
	private Long				idRegla;
	private Long				auxiliar;
	private Character			facturar;
	private Character			indNeting;
	private String				cdtype;
	private Character			retailSpecialOp;
	private Character			retrocession;
	private Character			ownAccount;
	private Character			ifinanciero;
	private Boolean isGrupo = Boolean.FALSE;
	private String auxiliarContable;
	private Long auxiliarCarteras;
	private Character disociar;
	private Character bonificado;
	private Character corretajePti;

	/**
   * @return the isGrupo
   */
	@Column(name = "ISGRUPO", nullable = true)
  public Boolean getIsGrupo() {
    return isGrupo;
  }

  /**
   * @param isGrupo the isGrupo to set
   */
  public void setIsGrupo(Boolean isGrupo) {
    this.isGrupo = isGrupo;
  }

  public Tmct0ali() {
	}

	public Tmct0ali( Tmct0aliId id, String descrali, Character tpdsaldo, Character routing, BigDecimal pccombco, String cdcomisn,
			String cdmodord, BigDecimal pccombrk, Character desglose, Character mktfeed, Character fidessa, Integer fhsendfi,
			String cdusuaud, Date fhaudit, Integer fhinicio, Integer fhfinal, Character facturar, String cdtype ) {
		this.id = id;
		this.descrali = descrali;
		this.tpdsaldo = tpdsaldo;
		this.routing = routing;
		this.pccombco = pccombco;
		this.cdcomisn = cdcomisn;
		this.cdmodord = cdmodord;
		this.pccombrk = pccombrk;
		this.desglose = desglose;
		this.mktfeed = mktfeed;
		this.fidessa = fidessa;
		this.fhsendfi = fhsendfi;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.fhinicio = fhinicio;
		this.fhfinal = fhfinal;
		this.facturar = facturar;
		this.cdtype = cdtype;
	}

	public Tmct0ali( Tmct0aliId id, String descrali, Character tpdsaldo, Character routing, BigDecimal pccombco, String cdcomisn,
			String cdmodord, BigDecimal pccombrk, Character desglose, Character mktfeed, Character fidessa, Integer fhsendfi,
			String cdusuaud, Date fhaudit, Integer fhinicio, Integer fhfinal, Long idRegla, Long auxiliar, Character facturar,
			Character indNeting, String cdtype, Character retailSpecialOp, Character retrocession, Character ownAccount ) {
		this.id = id;
		this.descrali = descrali;
		this.tpdsaldo = tpdsaldo;
		this.routing = routing;
		this.pccombco = pccombco;
		this.cdcomisn = cdcomisn;
		this.cdmodord = cdmodord;
		this.pccombrk = pccombrk;
		this.desglose = desglose;
		this.mktfeed = mktfeed;
		this.fidessa = fidessa;
		this.fhsendfi = fhsendfi;
		this.cdusuaud = cdusuaud;
		this.fhaudit = fhaudit;
		this.fhinicio = fhinicio;
		this.fhfinal = fhfinal;
		this.idRegla = idRegla;
		this.auxiliar = auxiliar;
		this.facturar = facturar;
		this.indNeting = indNeting;
		this.cdtype = cdtype;
		this.retailSpecialOp = retailSpecialOp;
		this.retrocession = retrocession;
		this.ownAccount = ownAccount;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "cdbrocli", column = @Column( name = "CDBROCLI", nullable = false, length = 20 ) ),
			@AttributeOverride( name = "cdaliass", column = @Column( name = "CDALIASS", nullable = false, length = 20 ) ),
			@AttributeOverride( name = "numsec", column = @Column( name = "NUMSEC", nullable = false, precision = 20 ) )
	} )
	public Tmct0aliId getId() {
		return this.id;
	}

	public void setId( Tmct0aliId id ) {
		this.id = id;
	}

	@Transient
	public String getCdbrocli() {
		String cdbrocli = null;
		final Tmct0aliId id = this.getId();
		if ( id != null ) {
			cdbrocli = id.getCdbrocli();
		}
		return cdbrocli;
	}

	@Transient
	public String getCdaliass() {
		String cdaliass = null;
		final Tmct0aliId id = this.getId();
		if ( id != null ) {
			cdaliass = id.getCdaliass();
		}
		return cdaliass;
	}

	@Transient
	public BigDecimal getNumsec() {
		BigDecimal numsec = null;
		final Tmct0aliId id = this.getId();
		if ( id != null ) {
			numsec = id.getNumsec();
		}
		return numsec;
	}

	@Column( name = "DESCRALI", nullable = false, length = 130 )
	public String getDescrali() {
		return this.descrali;
	}

	public void setDescrali( String descrali ) {
		this.descrali = descrali;
	}

	@Column( name = "TPDSALDO", nullable = false, length = 1 )
	public Character getTpdsaldo() {
		return this.tpdsaldo;
	}

	public void setTpdsaldo( Character tpdsaldo ) {
		this.tpdsaldo = tpdsaldo;
	}

	@Column( name = "ROUTING", nullable = false, length = 1 )
	public Character getRouting() {
		return this.routing;
	}

	public void setRouting( Character routing ) {
		this.routing = routing;
	}

	@Column( name = "PCCOMBCO", nullable = false, precision = 7, scale = 4 )
	public BigDecimal getPccombco() {
		return this.pccombco;
	}

	public void setPccombco( BigDecimal pccombco ) {
		this.pccombco = pccombco;
	}

	@Column( name = "CDCOMISN", nullable = false, length = 3 )
	public String getCdcomisn() {
		return this.cdcomisn;
	}

	public void setCdcomisn( String cdcomisn ) {
		this.cdcomisn = cdcomisn;
	}

	@Column( name = "CDMODORD", nullable = false, length = 3 )
	public String getCdmodord() {
		return this.cdmodord;
	}

	public void setCdmodord( String cdmodord ) {
		this.cdmodord = cdmodord;
	}

	@Column( name = "PCCOMBRK", nullable = false, precision = 7, scale = 4 )
	public BigDecimal getPccombrk() {
		return this.pccombrk;
	}

	public void setPccombrk( BigDecimal pccombrk ) {
		this.pccombrk = pccombrk;
	}

	@Column( name = "DESGLOSE", nullable = false, length = 1 )
	public Character getDesglose() {
		return this.desglose;
	}

	public void setDesglose( Character desglose ) {
		this.desglose = desglose;
	}

	@Column( name = "MKTFEED", nullable = false, length = 1 )
	public Character getMktfeed() {
		return this.mktfeed;
	}

	public void setMktfeed( Character mktfeed ) {
		this.mktfeed = mktfeed;
	}

	@Column( name = "FIDESSA", nullable = false, length = 1 )
	public Character getFidessa() {
		return this.fidessa;
	}

	public void setFidessa( Character fidessa ) {
		this.fidessa = fidessa;
	}

	@Column( name = "FHSENDFI", nullable = false, precision = 8 )
	public Integer getFhsendfi() {
		return this.fhsendfi;
	}

	public void setFhsendfi( Integer fhsendfi ) {
		this.fhsendfi = fhsendfi;
	}

	@Column( name = "CDUSUAUD", nullable = false, length = 10 )
	public String getCdusuaud() {
		return this.cdusuaud;
	}

	public void setCdusuaud( String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHAUDIT", nullable = false, length = 26 )
	public Date getFhaudit() {
		return this.fhaudit;
	}

	public void setFhaudit( Date fhaudit ) {
		this.fhaudit = fhaudit;
	}

	@Column( name = "FHINICIO", nullable = false, precision = 8 )
	public Integer getFhinicio() {
		return this.fhinicio;
	}

	public void setFhinicio( Integer fhinicio ) {
		this.fhinicio = fhinicio;
	}

	@Column( name = "FHFINAL", nullable = false, precision = 8 )
	public Integer getFhfinal() {
		return this.fhfinal;
	}

	public void setFhfinal( Integer fhfinal ) {
		this.fhfinal = fhfinal;
	}

	@Column( name = "ID_REGLA", precision = 8 )
	public Long getIdRegla() {
		return this.idRegla;
	}

	public void setIdRegla( Long idRegla ) {
		this.idRegla = idRegla;
	}

	@Column( name = "AUXILIAR" )
	public Long getAuxiliar() {
		return this.auxiliar;
	}

	public void setAuxiliar( Long auxiliar ) {
		this.auxiliar = auxiliar;
	}

	@Column( name = "FACTURAR", nullable = false, length = 1 )
	public Character getFacturar() {
		return this.facturar;
	}

	public void setFacturar( Character facturar ) {
		this.facturar = facturar;
	}

	@Column( name = "IND_NETING", length = 1 )
	public Character getIndNeting() {
		return this.indNeting;
	}

	public void setIndNeting( Character indNeting ) {
		this.indNeting = indNeting;
	}

	@Column( name = "CDTYPE", nullable = false, length = 2 )
	public String getCdtype() {
		return this.cdtype;
	}

	public void setCdtype( String cdtype ) {
		this.cdtype = cdtype;
	}

	@Column( name = "RETAIL_SPECIAL_OP", length = 1 )
	public Character getRetailSpecialOp() {
		return this.retailSpecialOp;
	}

	public void setRetailSpecialOp( Character retailSpecialOp ) {
		this.retailSpecialOp = retailSpecialOp;
	}

	@Column( name = "RETROCESSION", length = 1 )
	public Character getRetrocession() {
		return this.retrocession;
	}

	public void setRetrocession( Character retrocession ) {
		this.retrocession = retrocession;
	}

	@Column( name = "OWN_ACCOUNT", length = 1 )
	public Character getOwnAccount() {
		return this.ownAccount;
	}

	public void setOwnAccount( Character ownAccount ) {
		this.ownAccount = ownAccount;
	}

	@Column( name = "IFINANCIERO", length = 1 )
	public Character getIfinanciero() {
		return ifinanciero;
	}

	public void setIfinanciero( Character ifinanciero ) {
		this.ifinanciero = ifinanciero;
	}

	@Column( name = "AUXILIARCONTABLE", length = 45 )
	public String getAuxiliarContable() {
		return auxiliarContable;
	}

	public void setAuxiliarContable(String auxiliarContable) {
		this.auxiliarContable = auxiliarContable;
	}

	@Column( name = "AUXILIARCARTERAS" )
	public Long getAuxiliarCarteras() {
		return auxiliarCarteras;
	}

	public void setAuxiliarCarteras(Long auxiliarCarteras) {
		this.auxiliarCarteras = auxiliarCarteras;
	}

	@Column( name = "DISOCIAR", length = 1 )
	public Character getDisociar() {
		return disociar;
	}

	public void setDisociar(Character disociar) {
		this.disociar = disociar;
	}

	@Column( name = "BONIFICADO", length = 1 )
	public Character getBonificado() {
		return bonificado;
	}

	public void setBonificado(Character bonificado) {
		this.bonificado = bonificado;
	}

	@Column( name = "CORRETAJE_PTI", length = 1 )
	public Character getCorretajePti() {
		return corretajePti;
	}

	public void setCorretajePti(Character corretajePti) {
		this.corretajePti = corretajePti;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( id );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		final Tmct0ali tmct0ali = ( Tmct0ali ) obj;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( this.getId(), tmct0ali.getId() );
		return super.equals( obj );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		ToStringBuilder tsb = new ToStringBuilder( this, ToStringStyle.SHORT_PREFIX_STYLE );
		tsb.append( "DESCRALI", descrali );
		tsb.append( "TPSALDO", tpdsaldo );
		tsb.append( "ROUTING", routing );
		tsb.append( "PCCOMBO", pccombco );
		tsb.append( "CDCOMISN", cdcomisn );
		tsb.append( "CDMODORD", cdmodord );
		tsb.append( "PCCOMBRK", pccombrk );
		tsb.append( "DESGLOSE", desglose );
		tsb.append( "MKTFEED", mktfeed );
		tsb.append( "FIDESSA", fidessa );
		tsb.append( "FHSENDFI", fhsendfi );
		tsb.append( "CDUSUAUD", cdusuaud );
		tsb.append( "FHAUDID", fhaudit );
		tsb.append( "FHINICIO", fhinicio );
		tsb.append( "FHFINAL", fhfinal );
		tsb.append( "IDREGLA", idRegla );
		tsb.append( "FACTURAR", facturar );
		tsb.append( "INDNETING", indNeting );
		tsb.append( "CDTYPE", cdtype );
		tsb.append( "RETAILSPECIALOP", retailSpecialOp );
		tsb.append( "RETROCESSION", retrocession );
		tsb.append( "OWNACCOUNT", ownAccount );

		return tsb.toString();
	}

}
