package sibbac.business.fase0.database.model;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0actId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4773925033915830317L;
	private String				cdbrocli;
	private String				cdaliass;
	private String				cdsubcta;
	private BigDecimal			numsec;

	public Tmct0actId() {
	}

	public Tmct0actId( String cdbrocli, String cdaliass, String cdsubcta, BigDecimal numsec ) {
		this.cdbrocli = cdbrocli;
		this.cdaliass = cdaliass;
		this.cdsubcta = cdsubcta;
		this.numsec = numsec;
	}

	@Column( name = "CDBROCLI", nullable = false, length = 20 )
	public String getCdbrocli() {
		return this.cdbrocli;
	}

	public void setCdbrocli( String cdbrocli ) {
		this.cdbrocli = cdbrocli;
	}

	@Column( name = "CDALIASS", nullable = false, length = 20 )
	public String getCdaliass() {
		return this.cdaliass;
	}

	public void setCdaliass( String cdaliass ) {
		this.cdaliass = cdaliass;
	}

	@Column( name = "CDSUBCTA", nullable = false, length = 20 )
	public String getCdsubcta() {
		return this.cdsubcta;
	}

	public void setCdsubcta( String cdsubcta ) {
		this.cdsubcta = cdsubcta;
	}

	@Column( name = "NUMSEC", nullable = false, precision = 20 )
	public BigDecimal getNumsec() {
		return this.numsec;
	}

	public void setNumsec( BigDecimal numsec ) {
		this.numsec = numsec;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0actId ) )
			return false;
		Tmct0actId castOther = ( Tmct0actId ) other;

		return ( ( this.getCdbrocli() == castOther.getCdbrocli() ) || ( this.getCdbrocli() != null && castOther.getCdbrocli() != null && this
				.getCdbrocli().equals( castOther.getCdbrocli() ) ) )
				&& ( ( this.getCdaliass() == castOther.getCdaliass() ) || ( this.getCdaliass() != null && castOther.getCdaliass() != null && this
						.getCdaliass().equals( castOther.getCdaliass() ) ) )
				&& ( ( this.getCdsubcta() == castOther.getCdsubcta() ) || ( this.getCdsubcta() != null && castOther.getCdsubcta() != null && this
						.getCdsubcta().equals( castOther.getCdsubcta() ) ) )
				&& ( ( this.getNumsec() == castOther.getNumsec() ) || ( this.getNumsec() != null && castOther.getNumsec() != null && this
						.getNumsec().equals( castOther.getNumsec() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getCdbrocli() == null ? 0 : this.getCdbrocli().hashCode() );
		result = 37 * result + ( getCdaliass() == null ? 0 : this.getCdaliass().hashCode() );
		result = 37 * result + ( getCdsubcta() == null ? 0 : this.getCdsubcta().hashCode() );
		result = 37 * result + ( getNumsec() == null ? 0 : this.getNumsec().hashCode() );
		return result;
	}

}
