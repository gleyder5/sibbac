package sibbac.business.wrappers.database.data;

import sibbac.business.wrappers.database.model.SistemaLiquidacion;

public class SistemaLiquidacionData {
    private long id;
    private String codigo;
    private String descripcion;
    public SistemaLiquidacionData(){
	//
    }
    public SistemaLiquidacionData(SistemaLiquidacion sl){
	this.id = sl.getId();
	this.codigo = sl.getCodigo();
	this.descripcion = sl.getDescripcion();
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
