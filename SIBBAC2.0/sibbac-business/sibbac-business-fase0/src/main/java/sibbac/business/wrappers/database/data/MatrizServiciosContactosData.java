package sibbac.business.wrappers.database.data;


import java.util.ArrayList;
import java.util.List;


/**
 * The Class MatrizServiciosContactosData.
 */
public class MatrizServiciosContactosData {

	/** The servicios contactos datas. */
	private List< CombinacionData >	combinacionDatas;

	/** The servicios. */
	private List< ServicioData >	servicios;

	/** The contactos. */
	private List< ContactosData >	contactos;

	/**
	 * Adds the.
	 *
	 * @param combinacionData the combinacion data
	 * @return true, if successful
	 */
	public boolean add( CombinacionData combinacionData ) {
		return getCombinacionDatas().add( combinacionData );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		MatrizServiciosContactosData other = ( MatrizServiciosContactosData ) obj;
		if ( combinacionDatas == null ) {
			if ( other.combinacionDatas != null )
				return false;
		} else if ( !combinacionDatas.equals( other.combinacionDatas ) )
			return false;
		if ( contactos == null ) {
			if ( other.contactos != null )
				return false;
		} else if ( !contactos.equals( other.contactos ) )
			return false;
		if ( servicios == null ) {
			if ( other.servicios != null )
				return false;
		} else if ( !servicios.equals( other.servicios ) )
			return false;
		return true;
	}

	/**
	 * Gets the combinacion datas.
	 *
	 * @return the combinacion datas
	 */
	public final List< CombinacionData > getCombinacionDatas() {
		if ( combinacionDatas == null ) {
			combinacionDatas = new ArrayList< CombinacionData >();
		}
		return combinacionDatas;
	}

	/**
	 * Gets the contactos.
	 *
	 * @return the contactos
	 */
	public final List< ContactosData > getContactos() {
		if ( contactos == null ) {
			contactos = new ArrayList< ContactosData >();
		}
		return contactos;
	}

	/**
	 * Gets the servicios.
	 *
	 * @return the servicios
	 */
	public final List< ServicioData > getServicios() {
		if ( servicios == null ) {
			servicios = new ArrayList< ServicioData >();
		}
		return servicios;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( combinacionDatas == null ) ? 0 : combinacionDatas.hashCode() );
		result = prime * result + ( ( contactos == null ) ? 0 : contactos.hashCode() );
		result = prime * result + ( ( servicios == null ) ? 0 : servicios.hashCode() );
		return result;
	}

	/**
	 * Sets the combinacion datas.
	 *
	 * @param combinacionDatas the new combinacion datas
	 */
	public final void setCombinacionDatas( List< CombinacionData > combinacionDatas ) {
		this.combinacionDatas = combinacionDatas;
	}

	/**
	 * Sets the contactos.
	 *
	 * @param contactos the new contactos
	 */
	public final void setContactos( List< ContactosData > contactos ) {
		this.contactos = contactos;
	}

	/**
	 * Sets the servicios.
	 *
	 * @param servicios the new servicios
	 */
	public final void setServicios( List< ServicioData > servicios ) {
		this.servicios = servicios;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MatrizServiciosContactosData [combinacionDatas=" + combinacionDatas + ", servicios=" + servicios + ", contactos="
				+ contactos + "]";
	}

}
