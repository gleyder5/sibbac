package sibbac.business.wrappers.database.model;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Entity
@Table( name = DBConstants.WRAPPERS.MERCADO )
@XmlRootElement
public class Tmct0Mercado extends ATable< Tmct0Mercado > {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	@Column( name = "CODIGO", length = 12 )
	private String				codigo;
	@Column( name = "DESCRIPCION", length = 50 )
	private String				descripcion;
	@ManyToMany(mappedBy="mercados")
	private List<Tmct0CuentasDeCompensacion>cuentasDeCompensacion;
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
