package sibbac.business.wrappers.database.data;


import java.io.Serializable;

import sibbac.business.wrappers.database.model.RelacionCuentaMercado;
import sibbac.business.wrappers.database.model.RelacionCuentaMercadoPK;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Mercado;


public class RelacionCuentaMercadoData implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private long				idCta;
	private long				idMkt;
	private String				codCta;
	private String				codMkt;

	public RelacionCuentaMercadoData( RelacionCuentaMercado rmkt ) {
			RelacionCuentaMercadoPK pk = rmkt.getRelacionCuentaMercadoPK();
			this.idCta = pk.getIdCtaLiquidacion().getId();
			this.idMkt = pk.getIdMercado().getId();
			this.codCta = pk.getIdCtaLiquidacion().getCdCodigo();
			this.codMkt = pk.getIdMercado().getCodigo();
	}

	public RelacionCuentaMercadoData( Tmct0Mercado mkt, Tmct0CuentasDeCompensacion cc ) {
		this.idCta = cc.getIdCuentaCompensacion();
		this.idMkt = mkt.getId();
		this.codCta = cc.getCdCodigo();
		this.codMkt = mkt.getCodigo();
	}

	public long getIdCta() {
		return idCta;
	}

	public void setIdCta( long idCta ) {
		this.idCta = idCta;
	}

	public long getIdMkt() {
		return idMkt;
	}

	public void setIdMkt( long idMkt ) {
		this.idMkt = idMkt;
	}

	public String getCodCta() {
		return codCta;
	}

	public void setCodCta( String codCta ) {
		this.codCta = codCta;
	}

	public String getCodMkt() {
		return codMkt;
	}

	public void setCodMkt( String codMkt ) {
		this.codMkt = codMkt;
	}

}
