package sibbac.business.canones.model;

import java.util.Date;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import static javax.persistence.TemporalType.DATE;

/**
 * Registro de la solicitudes para recalcular los canones
 */
@Entity
@Table(name = "TMCT0_CANON_SOLICITA_RECALCULO", 
    uniqueConstraints = {@UniqueConstraint(columnNames = {"REF_TITULAR", "FECHA"})})
public class CanonSolicitaRecalculo extends CanonEntity {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = 5513630005853437957L;
  
  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private BigInteger id;
  
  @Column(name = "REF_TITULAR", nullable = false)
  private String refTitular;
  
  @Column(name = "FECHA", nullable = false)
  @Temporal(DATE)
  private Date fecha;

  /**
   * @return identificador de la solicitud
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * @return referencia del titular
   */
  public String getRefTitular() {
    return refTitular;
  }

  /**
   * @param refTitular determina la referencia del titular, no puede ser nulo.
   */
  public void setRefTitular(String refTitular) {
    this.refTitular = refTitular;
  }

  /**
   * @return fecha para la que se solicita el recalculo.
   */
  public Date getFecha() {
    return fecha;
  }

  /**
   * @param fecha determina la fecha de solitud del recalculo
   */
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((refTitular == null) ? 0 : refTitular.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    CanonSolicitaRecalculo other = (CanonSolicitaRecalculo) obj;
    if (fecha == null) {
      if (other.fecha != null)
        return false;
    } else if (!fecha.equals(other.fecha))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (refTitular == null) {
      if (other.refTitular != null)
        return false;
    } else if (!refTitular.equals(other.refTitular))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return String.format("%1$tF - %2$s", fecha, refTitular);
  }

}
