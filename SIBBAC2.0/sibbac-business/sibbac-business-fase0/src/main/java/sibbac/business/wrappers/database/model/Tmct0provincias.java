package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;


@Entity
@Table( name = DBConstants.WRAPPERS.PROVINCIAS )
@Audited
public class Tmct0provincias  implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8970673967653451946L;
	

	@Id
	@Column( name = "CDCODIGO", length = 6, nullable = false )
	private String				cdcodigo;
	
	@Column( name = "NBPROVIN", length = 100, nullable = false )
	private String				nbprovin;
	
	@Column( name = "NBPROVIN2", length = 100 )
	private String				nbprovin2;
	
	
	@Column( name = "CDESTADO", length = 1 )
	private String				cdestado;
	

	public Tmct0provincias() {
	
	}


	public Tmct0provincias(String cdcodigo, String nbprovin, String nbprovin2,
			String cdestado) {
		super();
		this.cdcodigo = cdcodigo;
		this.nbprovin = nbprovin;
		this.nbprovin2 = nbprovin2;
		this.cdestado = cdestado;
	}


	public String getCdcodigo() {
		return cdcodigo;
	}


	public void setCdcodigo(String cdcodigo) {
		this.cdcodigo = cdcodigo;
	}


	public String getNbprovin() {
		return nbprovin;
	}


	public void setNbprovin(String nbprovin) {
		this.nbprovin = nbprovin;
	}


	public String getNbprovin2() {
		return nbprovin2;
	}


	public void setNbprovin2(String nbprovin2) {
		this.nbprovin2 = nbprovin2;
	}


	public String getCdestado() {
		return cdestado;
	}


	public void setCdestado(String cdestado) {
		this.cdestado = cdestado;
	}
	
	

	

	


	
	
	
	
	
	
	
	

}
