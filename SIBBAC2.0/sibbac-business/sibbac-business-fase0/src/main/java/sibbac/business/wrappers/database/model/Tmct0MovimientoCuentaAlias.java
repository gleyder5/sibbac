/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Entity
@Table( name = DBConstants.CONCILIACION.TMCT0_MOVIMIENTO_CUENTA_ALIAS )
@XmlRootElement
public class Tmct0MovimientoCuentaAlias extends ATable< Tmct0MovimientoCuentaAlias > implements Serializable {

	private static final long	serialVersionUID	= -2053999621531701526L;

	@XmlAttribute
	@Column( name = "TRADEDATE" )
	protected Date				tradedate;

	@XmlAttribute
	@Column( name = "SETTLEMENTDATE" )
	protected Date				settlementdate;

	@XmlAttribute
	@Column( name = "CDOPERACION", length = 4 )
	protected String			cdoperacion;

	@XmlAttribute
	@Column( name = "ISIN", nullable = false, length = 12 )
	protected String			isin;

	@XmlAttribute
	@Column( name = "MERCADO", length = 12 )
	protected String			mercado;

	@XmlAttribute
	@Column( name = "TITULOS", precision = 18, scale = 6 )
	protected BigDecimal		titulos;

	@XmlAttribute
	@Column( name = "IMEFECTIVO", precision = 17, scale = 2 )
	protected BigDecimal		imefectivo;

	@XmlAttribute
	@Column( name = "SIGNO_ANOTACION", length = 1 )
	protected char				signoanotacion;

	@XmlAttribute
	@Column( name = "CDALIASS", length = 20 )
	protected String			cdaliass;

	@XmlAttribute
	@Column( name = "CD_CODIGO_CUENTA_LIQ" )
	private String				cdcodigocuentaliq;

	@XmlAttribute
	@Column( name = "REFMOVIMIENTO", length = 16 )
	private String				refmovimiento;

	@XmlAttribute
	@Column( name = "CORRETAJE", precision = 18, scale = 8 )
	protected BigDecimal		corretaje;

	@XmlAttribute
	@Column( name = "GUARDADO_SALDO", nullable = true )
	protected Boolean			guardadosaldo = Boolean.FALSE;
	@Column(name="SISTEMA_LIQUIDACION", length=35)
	private String sistemaLiquidacion;
	
	public Tmct0MovimientoCuentaAlias() {

	}

	public Tmct0MovimientoCuentaAlias( Date tradedate, Date settlementdate, String cdoperacion, String isin, String mercado,
			BigDecimal titulos, BigDecimal imefectivo, char signoanotacion, String cdaliass, String refmovimiento ) {
		this.tradedate = tradedate;
		this.settlementdate = settlementdate;
		this.cdoperacion = cdoperacion;
		this.isin = isin;
		this.mercado = mercado;
		this.titulos = titulos;
		this.imefectivo = imefectivo;
		this.signoanotacion = signoanotacion;
		this.cdaliass = cdaliass;
		this.refmovimiento = refmovimiento;
	}

	public Tmct0MovimientoCuentaAlias( Date tradedate, Date settlementdate, String cdoperacion, String isin, String mercado,
			BigDecimal titulos, BigDecimal imefectivo, char signoanotacion, String cdaliass, String refmovimiento, String cdcodigocuentaliq ) {
		this.tradedate = tradedate;
		this.settlementdate = settlementdate;
		this.cdoperacion = cdoperacion;
		this.isin = isin;
		this.mercado = mercado;
		this.titulos = titulos;
		this.imefectivo = imefectivo;
		this.signoanotacion = signoanotacion;
		this.cdaliass = cdaliass;
		this.refmovimiento = refmovimiento;
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

	public Tmct0MovimientoCuentaAlias( Date tradedate, Date settlementdate, String cdoperacion, String isin, String mercado,
			BigDecimal titulos, BigDecimal imefectivo, char signoanotacion, String cdaliass, String refmovimiento,
			String cdcodigocuentaliq, Boolean guardadosaldo ) {
		this.tradedate = tradedate;
		this.settlementdate = settlementdate;
		this.cdoperacion = cdoperacion;
		this.isin = isin;
		this.mercado = mercado;
		this.titulos = titulos;
		this.imefectivo = imefectivo;
		this.signoanotacion = signoanotacion;
		this.cdaliass = cdaliass;
		this.refmovimiento = refmovimiento;
		this.cdcodigocuentaliq = cdcodigocuentaliq;
		this.guardadosaldo = guardadosaldo;
	}

	public Date getTradedate() {
		return tradedate;
	}

	public void setTradedate( Date tradedate ) {
		this.tradedate = tradedate;
	}

	public Date getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate( Date settlementdate ) {
		this.settlementdate = settlementdate;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getMercado() {
		return mercado;
	}

	public void setMercado( String mercado ) {
		this.mercado = mercado;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getImefectivo() {
		return imefectivo;
	}

	public void setImefectivo( BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	public String getCdaliass() {
		return cdaliass;
	}

	public void setCdaliass( String cdaliass ) {
		this.cdaliass = cdaliass;
	}

	public String getCdoperacion() {
		return cdoperacion;
	}

	public void setCdoperacion( String cdoperacion ) {
		this.cdoperacion = cdoperacion;
	}

	public char getSignoanotacion() {
		return signoanotacion;
	}

	public void setSignoanotacion( char signoanotacion ) {
		this.signoanotacion = signoanotacion;
	}

	public String getCdcodigocuentaliq() {
		return cdcodigocuentaliq;
	}

	public void setCdcodigocuentaliq( String cdcodigocuentaliq ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

	public String getRefmovimiento() {
		return refmovimiento;
	}

	public void setRefmovimiento( String refmovimiento ) {
		this.refmovimiento = refmovimiento;
	}

	public BigDecimal getCorretaje() {
		return corretaje;
	}

	public void setCorretaje( BigDecimal corretaje ) {
		this.corretaje = corretaje;
	}

	public Boolean getGuardadosaldo() {
		return guardadosaldo;
	}

	public void setGuardadosaldo( Boolean guardadosaldo ) {
		this.guardadosaldo = guardadosaldo;
	}
	

	public String getSistemaLiquidacion() {
	    return sistemaLiquidacion;
	}

	public void setSistemaLiquidacion(String sistemaLiquidacion) {
	    this.sistemaLiquidacion = sistemaLiquidacion;
	}

	@Override
	public String toString() {
		return "sibbac.business.conciliacion.database.model.Tmct0MovimientoCuentaAlias[ id=" + id + " ]";
	}

}
