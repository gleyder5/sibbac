package sibbac.business.wrappers.database.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Entity: "GruposImpositivos".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table( name = WRAPPERS.GRUPO_IMPOSITIVO )
@Entity
@XmlRootElement
@Audited
public class GruposImpositivos extends ATableAudited< GruposImpositivos > implements java.io.Serializable {

	public static final String	GRUPO_IMPOSITIVO_CANCELADO_S			= "S";
	public static final String	GRUPO_IMPOSITIVO_CANCELADO_N			= "N";

	public static final String	GRUPO_IMPOSITIVO_CATEGORIA_REPERCUTIDA	= "R";
	public static final String	GRUPO_IMPOSITIVO_CATEGORIA_SOPORTADA	= "S";
	/**
	 *
	 */
	private static final long	serialVersionUID						= 1131970605360103919L;

	@Column( name = "PCPORCENTAJE", nullable = false )
	@XmlAttribute
	private Double				porcentaje;

	@Column( name = "FHVALIDODESDE", nullable = false )
	@XmlAttribute
	@Temporal( TemporalType.DATE )
	private Date				validoDesde;

	@Column( name = "CATEGORIA", nullable = false, length = 1 )
	@XmlAttribute
	private String				categoria;

	@Column( name = "NBDESCRIPCION", length = 50 )
	@XmlAttribute
	private String				descripcion;

	@Column( name = "CANCELADO", length = 1 )
	@XmlAttribute
	private String				cancelado;

	// ----------------------------------------------- Bean Constructors

	public GruposImpositivos() {

	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for pcPorcentaje
	 *
	 * @return pcPorcentaje
	 */
	public Double getPorcentaje() {
		return this.porcentaje;
	}

	/**
	 * Getter for validoDesde
	 *
	 * @return validoDesde
	 */
	public Date getValidoDesde() {
		return this.validoDesde;
	}

	/**
	 * Getter for categoria
	 *
	 * @return categoria
	 */
	public String getCategoria() {
		return this.categoria;
	}

	/**
	 * Getter for descripcion
	 *
	 * @return descripcion
	 */
	public String getDescripcion() {
		return this.descripcion;
	}

	/**
	 * Getter for cancelado
	 *
	 * @return cancelado
	 */
	public String getCancelado() {
		return this.cancelado;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for porcentaje
	 *
	 * @param porcentaje
	 */
	public void setPorcentaje( Double porcentaje ) {
		this.porcentaje = porcentaje;
	}

	/**
	 * Setter for validoDesde
	 *
	 * @param validoDesde
	 */
	public void setValidoDesde( Date fhValidoDesde ) {
		this.validoDesde = fhValidoDesde;
	}

	/**
	 * Setter for categoria
	 *
	 * @param categoria
	 */
	public void setCategoria( String categoria ) {
		this.categoria = categoria;
	}

	/**
	 * Setter for descripcion
	 *
	 * @param descripcion
	 */
	public void setDescripcion( String nbDescripcion ) {
		this.descripcion = nbDescripcion;
	}

	/**
	 * Setter for cancelado
	 *
	 * @param cancelado
	 */
	public void setCancelado( String cancelado ) {
		this.cancelado = cancelado;
	}

}
