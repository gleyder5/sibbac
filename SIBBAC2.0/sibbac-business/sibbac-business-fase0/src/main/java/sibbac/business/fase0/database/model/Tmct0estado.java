package sibbac.business.fase0.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0ESTADO" )
public class Tmct0estado implements java.io.Serializable {

	/**
	 * 
	 */

	private static final long	serialVersionUID		= -7411848278049078885L;
	private Integer				idestado;
	private String				nombre;
	private Integer				idtipoestado;

	public static final Integer	FACTURA_SUGERIDA		=  5;
	public static final Integer	ALC_ORDENES				=  6;
	public static final Integer	PER_ESTADOS				=  8;
	public static final Integer	FACTURA					=  9;
	public static final Integer	FACTURA_RECTIFICATIVA	=  10;
	public static final Integer	OPERACION_ESPECIAL_ASIG =  20;
	public static final Integer	OPERACION_ESPECIAL_CONT = 310;

	public Tmct0estado() {
		super();
	}

	public Tmct0estado( Integer idestado ) {
		this();
		this.idestado = idestado;

	}

	public Tmct0estado( Integer idestado, String nombre, int idtipoestado ) {
		this( idestado );
		this.nombre = nombre;
		this.idtipoestado = idtipoestado;
	}

	@Id
	@Column( name = "IDESTADO", unique = true, nullable = false )
	public Integer getIdestado() {
		return this.idestado;
	}

	public void setIdestado( Integer idestado ) {
		this.idestado = idestado;
	}

	@Column( name = "NOMBRE", nullable = false, length = 40 )
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	@Column( name = "IDTIPOESTADO", nullable = false )
	public Integer getIdtipoestado() {
		return this.idtipoestado;
	}

	public void setIdtipoestado( Integer idtipoestado ) {
		this.idtipoestado = idtipoestado;
	}
	
	@Override
	public boolean equals(Object obj) {
	    Tmct0estado other;
	    
	    if(obj == null)
	        return false;
	    if(obj == this) 
	        return true;
	    if(obj instanceof Tmct0estado) {
	        other = (Tmct0estado) obj;
	        return idestado.equals(other.idestado);
	    }
	    return false;
	}

}
