package sibbac.business.fase0.database.bo;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.Tmct0blqDao;
import sibbac.business.fase0.database.model.Tmct0blq;
import sibbac.business.fase0.database.model.Tmct0blqId;
import sibbac.database.bo.AbstractBo;

@Component
public class Tmct0blqBo extends AbstractBo<Tmct0blq, Tmct0blqId, Tmct0blqDao> {
  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0blqBo.class);

  public Tmct0blqBo() {
  }

  public List<Tmct0blq> findAllTmct0blqByCdbrokerAndCdmercadAndTpsettleAndFecha(String cdbroker,
                                                                                String cdmercad,
                                                                                String tpsettle,
                                                                                BigDecimal fecha) {
    return dao.findAllTmct0blqByCdbrokerAndCdmercadAndTpsettleAndFecha(cdbroker, cdmercad, tpsettle, fecha);
  }

  public List<Tmct0blq> findAllTmct0blqByCdbrokerAndCentroAndCdmercadAndTpsettleAndFecha(String cdbroker,
                                                                                         String centro,
                                                                                         String cdmercad,
                                                                                         String tpsettle,
                                                                                         BigDecimal fecha) {
    return dao.findAllTmct0blqByCdbrokerAndCentroAndCdmercadAndTpsettleAndFecha(cdbroker, centro, cdmercad, tpsettle,
                                                                                fecha);
  }

}
