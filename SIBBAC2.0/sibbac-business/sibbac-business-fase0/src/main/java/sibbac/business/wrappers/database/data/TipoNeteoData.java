package sibbac.business.wrappers.database.data;


import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;


public class TipoNeteoData {

	private long	id;
	private String	name;

	public TipoNeteoData( Tmct0TipoNeteo tpNeteo ) {
		this.id = tpNeteo.getId();
		this.name = tpNeteo.getNombre();
	}

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

}
