package sibbac.business.wrappers.database.data;

import java.io.Serializable;

import sibbac.business.wrappers.database.model.Tmct0alcId;

public class Tmct0AlcIdData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String nbooking = "";
    private String nuorden = "";
    private short nucnfliq = 0;
    private String nucnfclt = "";
    
    public Tmct0AlcIdData(){
	//
    }
    public Tmct0AlcIdData(Tmct0alcId tmct0alcId){
	entityToData(tmct0alcId, this);
    }
    
    public static Tmct0AlcIdData entityToData(Tmct0alcId entity){
	Tmct0AlcIdData data = new Tmct0AlcIdData();
	entityToData(entity, data);
	return data;
    }
    public static void entityToData(Tmct0alcId entity, Tmct0AlcIdData data) {
	data.setNbooking(entity.getNbooking());
	data.setNucnfclt(entity.getNucnfclt());
	data.setNucnfliq(entity.getNucnfliq());
	data.setNuorden(entity.getNuorden());
    }
    
    public String getNbooking() {
        return nbooking;
    }
    public void setNbooking(String nbooking) {
        this.nbooking = nbooking;
    }
    public String getNuorden() {
        return nuorden;
    }
    public void setNuorden(String nuorden) {
        this.nuorden = nuorden;
    }
    public short getNucnfliq() {
        return nucnfliq;
    }
    public void setNucnfliq(short nucnfliq) {
        this.nucnfliq = nucnfliq;
    }
    public String getNucnfclt() {
        return nucnfclt;
    }
    public void setNucnfclt(String nucnfclt) {
        this.nucnfclt = nucnfclt;
    }
    
}
