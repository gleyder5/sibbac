package sibbac.business.wrappers.database.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0ORD_DYNAMIC_VALUES")
public class Tmct0ordDynamicValues extends DynamicValuesBase {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nombre", column = @Column(name = "NOMBRE", nullable = false, length = 32)) })
  private Tmct0ordDynamicValuesId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "NUORDEN", nullable = false, insertable = false, updatable = false)
  private Tmct0ord tmct0ord;

  public Tmct0ordDynamicValuesId getId() {
    return id;
  }

  public void setId(Tmct0ordDynamicValuesId id) {
    this.id = id;
  }

  public Tmct0ord getTmct0ord() {
    return tmct0ord;
  }

  public void setTmct0ord(Tmct0ord tmct0ord) {
    this.tmct0ord = tmct0ord;
  }

}
