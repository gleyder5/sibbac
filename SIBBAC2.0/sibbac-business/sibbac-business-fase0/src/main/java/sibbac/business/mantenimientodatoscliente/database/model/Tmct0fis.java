package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "TMCT0FIS", uniqueConstraints = @UniqueConstraint(columnNames = "RFTITAUD"))
public class Tmct0fis implements Serializable {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0fis.class);

  private static final long serialVersionUID = 1L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "cddclave", column = @Column(name = "CDDCLAVE", nullable = false, length = 20)),
      @AttributeOverride(name = "numsec", column = @Column(name = "NUMSEC", nullable = false, precision = 20))

  })
  private Tmct0fisId id;

  @Column(name = "TPDOMICI", nullable = false, length = 2)
  private String tpdomici;

  @Column(name = "NBDOMICI", nullable = false, length = 40)
  private String nbdomici;

  @Column(name = "NUDOMICI", nullable = false, length = 4)
  private String nudomici;

  @Column(name = "NBCIUDAD", nullable = false, length = 40)
  private String nbciudad;

  @Column(name = "NBPROVIN", nullable = false, length = 25)
  private String nbprovin;

  @Column(name = "CDPOSTAL", nullable = false, length = 5)
  private String cdpostal;

  @Column(name = "CDDEPAIS", nullable = false, length = 3)
  private String cddepais;

  @Column(name = "NUDNICIF", nullable = false, length = 10)
  private String nudnicif;

  @Column(name = "NBCLIENT", nullable = false, length = 30)
  private String nbclient;

  @Column(name = "NBCLIEN1", nullable = false, length = 50)
  private String nbclien1;

  @Column(name = "NBCLIEN2", nullable = false, length = 30)
  private String nbclien2;

  @Column(name = "TPTIPREP", nullable = false, length = 1)
  private String tptiprep;

  @Column(name = "CDNACTIT", nullable = false, length = 3)
  private String cdnactit;

  @Column(name = "FHMODIFI", nullable = false, precision = 8)
  private BigDecimal fhmodifi;

  @Column(name = "HRMODIFI", nullable = false, length = 6)
  private BigDecimal hrmodifi;

  @Column(name = "CDUSUARI", nullable = false, length = 10)
  private String cdusuari;

  @Column(name = "CDORDTIT", nullable = false, length = 25)
  private String cdordtit;

  @Column(name = "RFTITAUD", nullable = false, length = 16)
  private String rftitaud;

  @Column(name = "CDDDEBIC", nullable = false, length = 11)
  private String cdddebic;

  @Column(name = "TPIDENTI", length = 1)
  private String tpidenti;

  @Column(name = "TPSOCIED", length = 1)
  private String tpsocied;

  @Column(name = "TPNACTIT", length = 1)
  private String tpnactit;

  @Column(name = "CDUSUAUD", length = 10)
  private String cdusuaud;

  @Temporal(TemporalType.DATE)
  @Column(name = "FHAUDIT", length = 10)
  private Date fhaudit;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_NACIMIENTO", length = 10)
  private Date fechaNacimiento;

  @Column(name = "FHINICIO", length = 8)
  private BigDecimal fhinicio = BigDecimal.ZERO;
  @Column(name = "FHFINAL", length = 8)
  private BigDecimal fhfinal = BigDecimal.ZERO;

  @Column(name = "CDHOLDER", length = 40)
  private String cdholder;

  @Column(name = "CATEGORY", length = 10)
  private String category;

  @Column(name = "CDREFBAN", length = 32)
  private String cdrefban;

  @Column(name = "TIPO_ID_MIFID", length = 1, nullable = false)
  private char tipoIdMifid = ' ';

  @Column(name = "ID_MIFID", length = 20, nullable = false)
  private String idMifid = "";

  @Column(name = "IND_CLIENTE_SAN", length = 1, nullable = false)
  private char indClienteSan = 'N';

  @Column(name = "NUM_PERSONA_SAN", length = 9, nullable = false)
  private String numPersonaSan = "";

  public Tmct0fis() {
    LOG.info("Contruido Tmct0fis");
  }

  public Tmct0fis(Tmct0fisId id, String tpdomici, String nbdomici, String nudomici, String nbciudad, String nbprovin,
      String cdpostal, String cddepais, String nudnicif, String nbclient, String nbclien1, String nbclien2,
      String tptiprep, String cdnactit, BigDecimal fhmodifi, BigDecimal hrmodifi, String cdusuari, String cdordtit,
      String rftitaud, String cdddebic, String tpidenti, String tpsocied, String tpnactit, String cdusuaud,
      Date fhaudit, BigDecimal fhinicio, BigDecimal fhfinal, String cdholder, String category, String cdrefban) {

    this.id = id;
    this.tpdomici = tpdomici;
    this.nbdomici = nbdomici;
    this.nudomici = nudomici;
    this.nbciudad = nbciudad;
    this.nbprovin = nbprovin;
    this.cdpostal = cdpostal;
    this.cddepais = cddepais;
    this.nudnicif = nudnicif;
    this.nbclient = nbclient;
    this.nbclien1 = nbclien1;
    this.nbclien2 = nbclien2;
    this.tptiprep = tptiprep;
    this.cdnactit = cdnactit;
    this.fhmodifi = fhmodifi;
    this.hrmodifi = hrmodifi;
    this.cdusuari = cdusuari;
    this.cdordtit = cdordtit;
    this.rftitaud = rftitaud;
    this.cdddebic = cdddebic;
    this.tpidenti = tpidenti;
    this.tpsocied = tpsocied;
    this.tpnactit = tpnactit;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.fhinicio = fhinicio;
    this.fhfinal = fhfinal;
    this.cdholder = cdholder;
    this.category = category;
    this.cdrefban = cdrefban;
  }

  public Tmct0fisId getId() {
    return this.id;
  }

  public void setId(Tmct0fisId id) {
    this.id = id;
  }

  public String getTpdomici() {
    return tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  public String getNbdomici() {
    return nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  public String getNudomici() {
    return nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  public String getNbciudad() {
    return nbciudad;
  }

  public void setNbciudad(String nbciudad) {
    this.nbciudad = nbciudad;
  }

  public String getNbprovin() {
    return nbprovin;
  }

  public void setNbprovin(String nbprovin) {
    this.nbprovin = nbprovin;
  }

  public String getCdpostal() {
    return cdpostal;
  }

  public void setCdpostal(String cdpostal) {
    this.cdpostal = cdpostal;
  }

  public String getCddepais() {
    return cddepais;
  }

  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  public String getNudnicif() {
    return nudnicif;
  }

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  public String getNbclient() {
    return nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public String getNbclien1() {
    return nbclien1;
  }

  public void setNbclien1(String nbclien1) {
    this.nbclien1 = nbclien1;
  }

  public String getNbclien2() {
    return nbclien2;
  }

  public void setNbclien2(String nbclien2) {
    this.nbclien2 = nbclien2;
  }

  public String getTptiprep() {
    return tptiprep;
  }

  public void setTptiprep(String tptiprep) {
    this.tptiprep = tptiprep;
  }

  public String getCdnactit() {
    return cdnactit;
  }

  public void setCdnactit(String cdnactit) {
    this.cdnactit = cdnactit;
  }

  public BigDecimal getFhmodifi() {
    return fhmodifi;
  }

  public void setFhmodifi(BigDecimal fhmodifi) {
    this.fhmodifi = fhmodifi;
  }

  public BigDecimal getHrmodifi() {
    return hrmodifi;
  }

  public void setHrmodifi(BigDecimal hrmodifi) {
    this.hrmodifi = hrmodifi;
  }

  public String getCdusuari() {
    return cdusuari;
  }

  public void setCdusuari(String cdusuari) {
    this.cdusuari = cdusuari;
  }

  public String getCdordtit() {
    return cdordtit;
  }

  public void setCdordtit(String cdordtit) {
    this.cdordtit = cdordtit;
  }

  public String getRftitaud() {
    return rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  public String getCdddebic() {
    return cdddebic;
  }

  public void setCdddebic(String cdddebic) {
    this.cdddebic = cdddebic;
  }

  public String getTpidenti() {
    return tpidenti;
  }

  public void setTpidenti(String tpidenti) {
    this.tpidenti = tpidenti;
  }

  public String getTpsocied() {
    return tpsocied;
  }

  public void setTpsocied(String tpsocied) {
    this.tpsocied = tpsocied;
  }

  public String getTpnactit() {
    return tpnactit;
  }

  public void setTpnactit(String tpnactit) {
    this.tpnactit = tpnactit;
  }

  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(Date fecha_nacimiento) {
    this.fechaNacimiento = fecha_nacimiento;
  }

  public BigDecimal getFhinicio() {
    return fhinicio;
  }

  public void setFhinicio(BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  public BigDecimal getFhfinal() {
    return fhfinal;
  }

  public void setFhfinal(BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  public String getCdholder() {
    return cdholder;
  }

  public void setCdholder(String cdholder) {
    this.cdholder = cdholder;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getCdrefban() {
    return cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public char getTipoIdMifid() {
    return tipoIdMifid;
  }

  public String getIdMifid() {
    return idMifid;
  }

  public char getIndClienteSan() {
    return indClienteSan;
  }

  public String getNumPersonaSan() {
    return numPersonaSan;
  }

  public void setTipoIdMifid(char tipoIdMifid) {
    this.tipoIdMifid = tipoIdMifid;
  }

  public void setIdMifid(String idMifid) {
    this.idMifid = idMifid;
  }

  public void setIndClienteSan(char indClienteSan) {
    this.indClienteSan = indClienteSan;
  }

  public void setNumPersonaSan(String numPersonaSan) {
    this.numPersonaSan = numPersonaSan;
  }

  @Override
  public String toString() {
    return "Tmct0fis [id=" + id + ", tpdomici=" + tpdomici + ", nbdomici=" + nbdomici + ", nudomici=" + nudomici
        + ", nbciudad=" + nbciudad + ", nbprovin=" + nbprovin + ", cdpostal=" + cdpostal + ", cddepais=" + cddepais
        + ", nudnicif=" + nudnicif + ", nbclient=" + nbclient + ", nbclien1=" + nbclien1 + ", nbclien2=" + nbclien2
        + ", tptiprep=" + tptiprep + ", cdnactit=" + cdnactit + ", fhmodifi=" + fhmodifi + ", hrmodifi=" + hrmodifi
        + ", cdusuari=" + cdusuari + ", cdordtit=" + cdordtit + ", rftitaud=" + rftitaud + ", cdddebic=" + cdddebic
        + ", tpidenti=" + tpidenti + ", tpsocied=" + tpsocied + ", tpnactit=" + tpnactit + ", cdusuaud=" + cdusuaud
        + ", fhaudit=" + fhaudit + ", fechaNacimiento=" + fechaNacimiento + ", fhinicio=" + fhinicio + ", fhfinal="
        + fhfinal + ", cdholder=" + cdholder + ", category=" + category + ", cdrefban=" + cdrefban + ", tipoIdMifid="
        + tipoIdMifid + ", idMifid=" + idMifid + ", indClienteSan=" + indClienteSan + ", numPersonaSan="
        + numPersonaSan + "]";
  }
}
