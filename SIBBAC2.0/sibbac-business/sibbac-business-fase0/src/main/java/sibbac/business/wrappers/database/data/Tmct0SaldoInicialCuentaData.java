/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sibbac.business.wrappers.database.data;


import java.math.BigDecimal;
import java.util.Date;


public class Tmct0SaldoInicialCuentaData {

	protected Date			fecha;
	protected String		isin;
	protected BigDecimal	titulos;
	protected BigDecimal	imefectivo;
	protected String		cdaliass;
	private String			cdcodigocuentaliq;

	public Tmct0SaldoInicialCuentaData() {

	}

	public Tmct0SaldoInicialCuentaData( Date fecha, String isin, BigDecimal titulos, BigDecimal imefectivo, String cdaliass,
			String cdcodigocuentaliq ) {
		this.fecha = fecha;
		this.isin = isin;
		this.titulos = titulos;
		this.imefectivo = imefectivo;
		this.cdaliass = cdaliass;
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha( Date fecha ) {
		this.fecha = fecha;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getImefectivo() {
		return imefectivo;
	}

	public void setImefectivo( BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	public String getCdaliass() {
		return cdaliass;
	}

	public void setCdaliass( String cdaliass ) {
		this.cdaliass = cdaliass;
	}

	public String getCdcodigocuentaliq() {
		return cdcodigocuentaliq;
	}

	public void setCdcodigocuentaliq( String cdcodigocuentaliq ) {
		this.cdcodigocuentaliq = cdcodigocuentaliq;
	}

}
