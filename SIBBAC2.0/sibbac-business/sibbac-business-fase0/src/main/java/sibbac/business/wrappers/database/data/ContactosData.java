package sibbac.business.wrappers.database.data;


/**
 * The Class ContactosData.
 */
public class ContactosData {

	/** The id. */
	private final Long		idContacto;
	/** The nombre. */
	private final String	nombreContacto;

	/** The primer apellido. */
	private final String	pApellidoContacto;

	/** The segundo apellido. */
	private final String	sApellidoContacto;

	/** The telefono a contacto. */
	private final String	telefonoAContacto;

	/** The email contacto. */
	private final String	emailContacto;

	/**
	 * Instantiates a new contactos data.
	 *
	 * @param idContacto the id contacto
	 * @param nombreContacto the nombre contacto
	 * @param pApellidoContacto the apellido contacto
	 * @param sApellidoContacto the s apellido contacto
	 * @param telefonoAContacto the telefono a contacto
	 * @param emailContacto the email contacto
	 */
	public ContactosData( Long idContacto, String nombreContacto, String pApellidoContacto, String sApellidoContacto,
			String telefonoAContacto, String emailContacto ) {
		super();
		this.idContacto = idContacto;
		this.nombreContacto = nombreContacto;
		this.pApellidoContacto = pApellidoContacto;
		this.sApellidoContacto = sApellidoContacto;
		this.telefonoAContacto = telefonoAContacto;
		this.emailContacto = emailContacto;
	}

	/**
	 * Instantiates a new contactos data.
	 *
	 * @param contactosData the contactos data
	 */
	public ContactosData( ContactosData contactosData ) {
		super();
		this.idContacto = contactosData.getIdContacto();
		this.nombreContacto = contactosData.getNombreContacto();
		this.pApellidoContacto = contactosData.getpApellidoContacto();
		this.sApellidoContacto = contactosData.getsApellidoContacto();
		this.telefonoAContacto = contactosData.getTelefonoAContacto();
		this.emailContacto = contactosData.getEmailContacto();
	}

	/**
	 * Gets the id contacto.
	 *
	 * @return the id contacto
	 */
	public final Long getIdContacto() {
		return idContacto;
	}

	/**
	 * Gets the nombre contacto.
	 *
	 * @return the nombre contacto
	 */
	public final String getNombreContacto() {
		return nombreContacto;
	}

	/**
	 * Gets the p apellido contacto.
	 *
	 * @return the p apellido contacto
	 */
	public final String getpApellidoContacto() {
		return pApellidoContacto;
	}

	/**
	 * Gets the s apellido contacto.
	 *
	 * @return the s apellido contacto
	 */
	public final String getsApellidoContacto() {
		return sApellidoContacto;
	}

	/**
	 * Gets the telefono a contacto.
	 *
	 * @return the telefono a contacto
	 */
	public final String getTelefonoAContacto() {
		return telefonoAContacto;
	}

	/**
	 * Gets the email contacto.
	 *
	 * @return the email contacto
	 */
	public final String getEmailContacto() {
		return emailContacto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( emailContacto == null ) ? 0 : emailContacto.hashCode() );
		result = prime * result + ( ( idContacto == null ) ? 0 : idContacto.hashCode() );
		result = prime * result + ( ( nombreContacto == null ) ? 0 : nombreContacto.hashCode() );
		result = prime * result + ( ( pApellidoContacto == null ) ? 0 : pApellidoContacto.hashCode() );
		result = prime * result + ( ( sApellidoContacto == null ) ? 0 : sApellidoContacto.hashCode() );
		result = prime * result + ( ( telefonoAContacto == null ) ? 0 : telefonoAContacto.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		ContactosData other = ( ContactosData ) obj;
		if ( emailContacto == null ) {
			if ( other.emailContacto != null )
				return false;
		} else if ( !emailContacto.equals( other.emailContacto ) )
			return false;
		if ( idContacto == null ) {
			if ( other.idContacto != null )
				return false;
		} else if ( !idContacto.equals( other.idContacto ) )
			return false;
		if ( nombreContacto == null ) {
			if ( other.nombreContacto != null )
				return false;
		} else if ( !nombreContacto.equals( other.nombreContacto ) )
			return false;
		if ( pApellidoContacto == null ) {
			if ( other.pApellidoContacto != null )
				return false;
		} else if ( !pApellidoContacto.equals( other.pApellidoContacto ) )
			return false;
		if ( sApellidoContacto == null ) {
			if ( other.sApellidoContacto != null )
				return false;
		} else if ( !sApellidoContacto.equals( other.sApellidoContacto ) )
			return false;
		if ( telefonoAContacto == null ) {
			if ( other.telefonoAContacto != null )
				return false;
		} else if ( !telefonoAContacto.equals( other.telefonoAContacto ) )
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContactosData [idContacto=" + idContacto + ", nombreContacto=" + nombreContacto + ", pApellidoContacto="
				+ pApellidoContacto + ", sApellidoContacto=" + sApellidoContacto + ", telefonoAContacto=" + telefonoAContacto
				+ ", emailContacto=" + emailContacto + "]";
	}

}
