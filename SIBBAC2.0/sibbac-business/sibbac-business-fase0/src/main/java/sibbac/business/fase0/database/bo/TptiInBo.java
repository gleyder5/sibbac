package sibbac.business.fase0.database.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.TptiInDao;
import sibbac.business.fase0.database.dao.TptiInErrorDao;
import sibbac.business.fase0.database.dto.TptiInDTO;
import sibbac.business.fase0.database.model.TptiIn;
import sibbac.business.fase0.database.model.TptiInError;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.Header;

@Service
public class TptiInBo extends AbstractBo<TptiIn, Long, TptiInDao> {

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private TptiInErrorDao ptiInErrorDao;

  /***************************************** QUERYS ******************************************/

  public List<TptiInDTO> findAllTpTiInSinProcesar(Date fechaDesde, Date fechaHasta) {

    return dao.findAllTpTiInSinProcesar(fechaDesde, fechaHasta);
  }

  public List<Long> findAllIdsByProcessedAndWithoutError(char sd) {
    return dao.findAllIdsByProcessedAndWithoutError(sd);
  }

  public List<Long> findAllIdsByProcessedAndWithErrorAndSctiptDividend(Date beforeDate, Character scriptdividend) {
    return dao.findAllIdsByProcessedAndWithErrorAndSctiptDividend(beforeDate, scriptdividend);
  }

  public List<TptiInDTO> findAllDistinctMessageTypeAndMessageDateByNotInMessageTypesAndNotProcessedAndScriptDividend(
      List<String> messageTypes, Character scriptDividend) {
    return dao.findAllDistinctMessageTypeAndMessageDateByNotInMessageTypesAndNotProcessedAndScriptDividend(
        messageTypes, scriptDividend);
  }

  public List<TptiInDTO> findAllDistinctMessageTypeAndMessageDateByMessageTypesAndNotProcessedAndScriptDividend(
      List<String> messageTypes, Character scriptdividend) {
    return dao.findAllDistinctMessageTypeAndMessageDateByMessageTypesAndNotProcessedAndScriptDividend(messageTypes,
        scriptdividend);
  }

  public List<Long> findAllIdsByMessageDateAndMessageTypeNotProcessedAndScriptDividend(Date messageDate,
      String messageType, Character scriptDividend) {
    return dao.findAllIdsByMessageDateAndMessageTypeNotProcessedAndScriptDividend(messageDate, messageType,
        scriptDividend);
  }

  public List<String> findAllGroupDataByMessageDateAndMessageTypeNotProcessed(Date messageDate, String messageType) {
    return dao.findAllGroupDataByMessageDateAndMessageTypeNotProcessed(messageDate, messageType);
  }

  public List<Long> findAllIdsByGroupDataAndMessageDateAndMessageTypeAndNotProcessedAndScriptDividend(String groupData,
      Date messageDate, String messageType, Character scriptDividend) {
    return dao.findAllIdsByGroupDataAndMessageDateAndMessageTypeAndNotProcessedAndScriptDividend(groupData,
        messageDate, messageType, scriptDividend);
  }

  public List<TptiIn> findAllByGroupDataAndNotProcessed(String groupData) {
    return dao.findAllByGroupDataAndNotProcessed(groupData);
  }

  /***************************************** QUERYS ******************************************/
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public TptiIn savePtiMessageNewTransaction(String ptiMessageSt, String messageId, String groupId, Integer groupSeq,
      boolean isLast, Long jmsTimestamp, String cdusuaud, boolean isScriptDividend, String cfgVersion) {
    return this.savePtiMessage(ptiMessageSt, messageId, groupId, groupSeq, isLast, jmsTimestamp, cdusuaud,
        isScriptDividend, cfgVersion);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public TptiIn savePtiMessage(String ptiMessageSt, String messageId, String groupId, Integer groupSeq, boolean isLast,
      Long jmsTimestamp, String cdusuaud, boolean isScriptDividend, String cfgVersion) {
    LOG.trace("[savePtiMessage] inicio");
    TptiIn in = new TptiIn();
    in.setMessage(ptiMessageSt);
    in.setMessageLength(StringUtils.length(ptiMessageSt));

    in.setScriptDividend(isScriptDividend ? 'S' : 'N');
    in.setIdMessage(messageId);
    in.setIdBatch(groupId);
    in.setBatchOrder(groupSeq);
    in.setIsLast(isLast ? 'S' : 'N');
    in.setAudtFechaCambio(new Date());
    in.setAudtUser(cdusuaud);
    in.setProcessed('N');
    // in.setWithoutError('S');
    try {
      Header head = (Header) PTIMessageFactory.parseHeader(ptiMessageSt);
      PTIMessageVersion version = cfgBo.getPtiMessageVersionFromTmct0cfg(cfgVersion, head.getFechaDeEnvio());
      PTIMessage ptiMessage;
      try {
        ptiMessage = PTIMessageFactory.parse(version, ptiMessageSt);
      }
      catch (Exception e) {
        if (version == PTIMessageVersion.VERSION_1) {
          ptiMessage = PTIMessageFactory.parse(PTIMessageVersion.VERSION_T2S, ptiMessageSt);
          version = PTIMessageVersion.VERSION_T2S;
        }
        else if (version == PTIMessageVersion.VERSION_T2S) {
          ptiMessage = PTIMessageFactory.parse(PTIMessageVersion.VERSION_1, ptiMessageSt);
          version = PTIMessageVersion.VERSION_1;
        }
        else {
          throw e;
        }
      }
      in.setMessageType(head.getTipoDeRegistro());
      in.setMember(head.getMiembro());
      Date fechaEnvio = head.getFecha();
      if (fechaEnvio == null && jmsTimestamp != null && jmsTimestamp > 0) {
        fechaEnvio = new Date(jmsTimestamp);
      }
      else if (fechaEnvio == null) {
        fechaEnvio = new Date();
      }

      in.setMessageDate(fechaEnvio);
      in.setMessageHour(fechaEnvio);
      in.setGroupData(getGroupData(ptiMessage));
      in.setTradedate(fechaEnvio);

    }
    catch (Exception e) {
      LOG.warn("INCIDENCIA-Parseando MessagePTI: {} {}", ptiMessageSt, e);
      in.setWithoutError('N');
      in.setProcessed('S');
      in.setMessageDate(new Date());
      in.setMessageHour(new Date());
      in.setTradedate(new Date());
      in.setMessageType(StringUtils.substring(ptiMessageSt, 0, 4));
      in.setMember(StringUtils.substring(ptiMessageSt, 25, 30));
      crearError(e, in, cdusuaud);

    }

    in = save(in);
    LOG.debug(
        "[savePtiMessage] Grabando MessagePTI: ID:{} ## MessageId: {} ## GroupId: {} ## GroupSeq: {} ## isLast: {} ## Type: {} ## GroupData: {} ## Member: {}",
        in.getId(), in.getIdMessage(), in.getIdBatch(), in.getBatchOrder(), in.getIsLast(), in.getMessageType(),
        in.getGroupData(), in.getMember());
    LOG.trace("[savePtiMessage] fin");
    return in;
  }

  @SuppressWarnings("unused")
  private static String getGroupData(PTIMessage ptiMessage) {
    String groupData = "";
    try {
      PTIMessageRecord r00 = null;
      PTIMessageRecord r01 = null;
      PTIMessageRecord r02 = null;
      PTIMessageRecord r03 = null;
      PTIMessageRecord r04 = null;
      PTIMessageRecord r05 = null;
      PTIMessageRecord r06 = null;
      PTIMessageRecord r07 = null;

      String messageType = ptiMessage.getTipo().name();
      if (ptiMessage.hasRecords(PTIMessageRecordType.R00)) {
        r00 = ptiMessage.getAll(PTIMessageRecordType.R00).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R01)) {
        r01 = ptiMessage.getAll(PTIMessageRecordType.R01).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R02)) {
        r02 = ptiMessage.getAll(PTIMessageRecordType.R02).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R03)) {
        r03 = ptiMessage.getAll(PTIMessageRecordType.R03).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R04)) {
        r04 = ptiMessage.getAll(PTIMessageRecordType.R04).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R05)) {
        r05 = ptiMessage.getAll(PTIMessageRecordType.R05).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R06)) {
        r06 = ptiMessage.getAll(PTIMessageRecordType.R06).get(0);
      }
      if (ptiMessage.hasRecords(PTIMessageRecordType.R07)) {
        r07 = ptiMessage.getAll(PTIMessageRecordType.R07).get(0);
      }

      if (PTIMessageType.AC.name().equals(messageType.trim())) {
        if (r00 != null) {
          groupData = ((sibbac.pti.messages.ac.R00) r00).getReferenciaDeMovimientoECC();
        }
      }
      else if (PTIMessageType.AN.name().equals(messageType.trim())) {
        if (r01 != null) {
          groupData = ((sibbac.pti.messages.an.R01) r01).getNumeroOperacionInicial();
        }
      }
      else if (PTIMessageType.CE.name().equals(messageType.trim())) {
        if (r00 != null) {
          if (((sibbac.pti.messages.ce.R00) r00).getFechaContratacion() != null) {
            groupData = String.format("%1$tY%1$tm%td%s", ((sibbac.pti.messages.ce.R00) r00).getFechaContratacion(),
                StringUtils.leftPad(((sibbac.pti.messages.ce.R00) r00).getNumeroEjecucion().toString(), 9, '0'));
          }
          else {
            groupData = StringUtils.leftPad(((sibbac.pti.messages.ce.R00) r00).getNumeroEjecucion().toString(), 9, '0');
          }
        }
      }
      else if (PTIMessageType.RT.name().equals(messageType.trim())) {
        if (ptiMessage.hasRecords(PTIMessageRecordType.R00)) {
          r00 = ptiMessage.getAll(PTIMessageRecordType.R00).get(0);
          groupData = ((sibbac.pti.messages.rt.R00) r00).getCriterioComunicacionTitularidad();
        }
      }
      else if (PTIMessageType.TI.name().equals(messageType.trim())) {
        if (r00 != null) {
          groupData = String.format("%s%s", ((sibbac.pti.messages.ti.R00) r00).getReferenciaTitular(),
              ((sibbac.pti.messages.ti.R00) r00).getReferenciaAdicional());
        }
      }
      else if (PTIMessageType.MO.name().equals(messageType.trim())) {
        if (r00 != null) {
          groupData = ((sibbac.pti.messages.mo.R00) r00).getReferenciaDeMovimientoECC();
        }
      }
      else if (PTIMessageType.VA.name().equals(messageType.trim())) {
        if (r00 != null) {
          groupData = ((sibbac.pti.messages.va.R00) r00).getCodigoDeValor();
        }
      }
      else if (PTIMessageType.PV.name().equals(messageType.trim())) {
        if (r00 != null) {
          groupData = ((sibbac.pti.messages.pv.R00) r00).getCodigoDeValor();
        }
      }
      else {
        groupData = "";
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }
    return groupData;
  }

  public TptiInError crearError(Exception e, TptiIn tptiIn, String cdusud) {
    TptiInError error = new TptiInError();
    error.setErrorClass(e.getClass().getCanonicalName());
    error.setTraceError(e.toString());
    error.setTptiIn(tptiIn);
    error.setAudit_fecha_cambio(new Date());
    error.setAudit_user(cdusud);
    tptiIn.getErrors().add(error);
    return error;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void deletePtiMesages(List<Long> ids) {
    TptiIn ptiIn;
    List<TptiInError> errores;
    for (Long id : ids) {
      LOG.trace("Delete id: {}", id);
      ptiIn = findById(id);
      if (ptiIn != null) {
        LOG.trace(
            "[deletePtiMesages] borrando id: {} ## MsgType: {} ## MessageDate: {} ## Processed: {} ## WithOutErr: {}.",
            ptiIn.getId(), ptiIn.getMessageType(), ptiIn.getMessageDate(), ptiIn.getProcessed(),
            ptiIn.getWithoutError());
        errores = ptiInErrorDao.findAllByIdTptiIn(ptiIn.getId());
        if (CollectionUtils.isNotEmpty(errores)) {
          ptiInErrorDao.delete(errores);
        }
        delete(ptiIn);
      }
    }
    LOG.trace("Deleted ids:{}", ids);
  }
}// public class TptiInBo extends AbstractBo< TptiIn, Long, TptiInDao >{
