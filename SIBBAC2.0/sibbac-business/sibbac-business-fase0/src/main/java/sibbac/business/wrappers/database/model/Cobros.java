package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.envers.Audited;
import org.springframework.stereotype.Component;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Cobros }.
 *
 * @author Vector ITC Group
 * @version 0.1
 * @since 0.1
 */

@Table( name = DBConstants.WRAPPERS.COBROS )
@Entity
@Component
@XmlRootElement
@Audited
public class Cobros extends ATableAudited< Cobros > implements java.io.Serializable {

	private static final long		serialVersionUID	= 5984772035783503451L;

	@ManyToOne( fetch = FetchType.LAZY, optional = false )
	@JoinColumn( name = "ID_ALIAS", referencedColumnName = "ID", nullable = false )
	@XmlAttribute
	private Alias					alias;

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHFECHA" )
	@XmlAttribute
	private Date					fecha;

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "FHFECHA_ENVIO" )
	@XmlAttribute
	private Date					fechaEnvioMail;

	@Column( name = "IMIMPORTE", scale = 6, precision = 13 )
	@XmlAttribute
	private BigDecimal				importe;

	@Column( name = "IMCOMISIONES", scale = 6, precision = 13 )
	@XmlAttribute
	private BigDecimal				comisiones;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_TIPODOCUMENTO", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private TipoDeDocumento			tipoDeDocumento;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_MONEDA", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private Monedas					moneda;

	@Column( name = "NBCOBRO", nullable = false )
	@XmlAttribute
	private Long					cobro;

	@OneToMany( mappedBy = "cobro", cascade = CascadeType.PERSIST )
	private List< LineaDeCobro >	lineasDeCobro;

	@Column( name = "CONTABILIZADO" )
	@XmlAttribute
	protected Boolean				contabilizado;

	/**
	 * Instantiates a new cobros.
	 */
	public Cobros() {
		super();
	}

	/**
	 * Añade una linea a la lista de facturas existentes
	 * 
	 * @param lineaDeCobro
	 * @return
	 */
	public boolean addLineaCobro( final LineaDeCobro lineaDeCobro ) {
		if ( CollectionUtils.isEmpty( lineasDeCobro ) ) {
			lineasDeCobro = new ArrayList< LineaDeCobro >();
		}
		lineaDeCobro.setCobro( this );

		return lineasDeCobro.add( lineaDeCobro );

	}

	public List< Factura > getFacturas() {
		final List< Factura > facturas = new ArrayList< Factura >();
		if ( CollectionUtils.isNotEmpty( lineasDeCobro ) ) {
			for ( LineaDeCobro lineaDeCobro : lineasDeCobro ) {
				facturas.add( lineaDeCobro.getFactura() );
			}
		}
		return facturas;
	}

	public Contacto getContactoPorDefecto() {
		Contacto contacto = new Contacto();
		if ( alias != null ) {
			contacto = alias.getContactoDefecto();
		}
		return contacto;
	}

	/**
	 * @return the alias
	 */
	public Alias getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *            the alias to set
	 */
	public void setAlias( Alias alias ) {
		this.alias = alias;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha( Date fecha ) {
		this.fecha = fecha;
	}

	public Date getFechaEnvioMail() {
		return this.fechaEnvioMail;
	}

	public void setFechaEnvioMail( Date fechaEnvioMail ) {
		this.fechaEnvioMail = fechaEnvioMail;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	public Boolean getContabilizado() {
		return contabilizado;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	/**
	 * @return the comisiones
	 */
	public BigDecimal getComisiones() {
		return comisiones;
	}

	/**
	 * @param comisiones
	 *            the comisiones to set
	 */
	public void setComisiones( BigDecimal comisiones ) {
		this.comisiones = comisiones;
	}

	/**
	 * @return the tipoDeDocumento
	 */
	public TipoDeDocumento getTipoDeDocumento() {
		return tipoDeDocumento;
	}

	/**
	 * @param tipoDeDocumento
	 *            the tipoDeDocumento to set
	 */
	public void setTipoDeDocumento( TipoDeDocumento tipoDeDocumento ) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	/**
	 * @return the moneda
	 */
	public Monedas getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda( Monedas moneda ) {
		this.moneda = moneda;
	}

	public List< LineaDeCobro > getLineasDeCobro() {
		return lineasDeCobro;
	}

	public void setLineasDeCobro( List< LineaDeCobro > lineasDeCobro ) {
		this.lineasDeCobro = lineasDeCobro;
	}

	public Long getCobro() {
		return cobro;
	}

	public void setCobro( Long cobro ) {
		this.cobro = cobro;
	}

	public void setContabilizado( Boolean contabilizado ) {
		this.contabilizado = contabilizado;
	}
}
