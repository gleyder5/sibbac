package sibbac.business.canones.dao;

import java.util.Date;
import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.canones.model.CanonSolicitaRecalculo;

@Repository
public interface CanonSolicitaRecalculoDao extends JpaRepository<CanonSolicitaRecalculo, BigInteger> {
  
  @Query("SELECT COUNT(s) FROM CanonSolicitaRecalculo s WHERE s.refTitular = :titular AND s.fecha = :fecha")
  public int conteoTitularFecha(@Param("titular") String titular, @Param("fecha") Date fecha);

}
