package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class represent the table TMCT0CTD. Centers Bookings Allocations Client
 * Clearer Documentación para TMCT0CTD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 * 
 **/
@Entity
@Table(name = "TMCT0CTD")
public class Tmct0ctd implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5718318568937572070L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER", nullable = false, length = 20)),
      @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)),
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)),
      @AttributeOverride(name = "nualosec", column = @Column(name = "NUALOSEC", nullable = false, length = 5, scale = 0)) })
  protected Tmct0ctdId id;

  /**
   * Relation 1..1 with table TMCT0CTA. Constraint de relación entre TMCT0CTD y
   * TMCT0CTA <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  // private String nucnfclt;
  // private String cdmercad;
  // private String nuorden;
  // private String cdbroker;
  // private String nbooking;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDBROKER", referencedColumnName = "CDBROKER", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "CDMERCAD", referencedColumnName = "CDMERCAD", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false)

  })
  protected Tmct0cta tmct0cta;

  /**
   * Table Field CDALIAS. Subaccount mnemonic Documentación para CDALIAS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdalias;
  /**
   * Table Field DESCRALI. Alias description Documentación para DESCRALI <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String descrali;
  /**
   * Table Field IMCAMMED. Gross average price in order currency Documentación
   * para IMCAMMED <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcammed;
  /**
   * Table Field IMCAMNET. Net average price in order currency Documentación
   * para IMCAMNET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcamnet;
  /**
   * Table Field IMTOTBRU. Gross consideration given to the client in order
   * currency Documentación para IMTOTBRU <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imtotbru;
  /**
   * Table Field IMTOTNET. Net consideration in order currency Documentación
   * para IMTOTNET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imtotnet;
  /**
   * Table Field EUTOTBRU. Gross consideration given to the client in EUR
   * Documentación para EUTOTBRU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal eutotbru;
  /**
   * Table Field EUTOTNET. Net consideration in EUR Documentación para EUTOTNET
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal eutotnet;
  /**
   * Table Field IMNETBRK. Net local consideration in order ccy Documentación
   * para IMNETBRK <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imnetbrk;
  /**
   * Table Field EUNETBRK. Net local consideration in EUR Documentación para
   * EUNETBRK <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal eunetbrk;
  /**
   * Table Field IMCOMISN. Commission amount in order currency Documentación
   * para IMCOMISN <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcomisn;
  /**
   * Table Field IMCOMIEU. Commission amount in EUR Documentación para IMCOMIEU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcomieu;
  /**
   * Table Field IMCOMDEV. Rebate amount in order ccy Documentación para
   * IMCOMDEV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcomdev;
  /**
   * Table Field EUCOMDEV. Rebate amount in EUR Documentación para EUCOMDEV <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal eucomdev;
  /**
   * Table Field IMFIBRDV. Broker commission in order currency Documentación
   * para IMFIBRDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imfibrdv;
  /**
   * Table Field IMFIBREU. Broker commission in EUR Documentación para IMFIBREU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.math.BigDecimal imfibreu;
  /**
   * Table Field IMSVB. Commission SVB in order currency Documentación para
   * IMSVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imsvb;
  /**
   * Table Field IMSVBEU. Commission SVB in EUR Documentación para IMSVBEU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imsvbeu;
  /**
   * Table Field IMBRMERC. Gross market consideration in order currency
   * Documentación para IMBRMERC <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imbrmerc;
  /**
   * Table Field IMBRMREU. Gross market consideration in EUR Documentación para
   * IMBRMREU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imbrmreu;
  /**
   * Table Field IMGASTOS. Market Charges in EUR Documentación para IMGASTOS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgastos;
  /**
   * Table Field IMGATDVS. Market Charges in order currency Documentación para
   * IMGATDVS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgatdvs;
  /**
   * Table Field IMIMPDVS. Market Taxes in order currency Documentación para
   * IMIMPDVS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imimpdvs;
  /**
   * Table Field IMIMPEUR. Market Taxes in EUR Documentación para IMIMPEUR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imimpeur;
  /**
   * Table Field IMCCONEU. Spanish trading market charges in EUR Documentación
   * para IMCCONEU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcconeu;
  /**
   * Table Field IMCLIQEU. Spanish clearing market charges in EUR Documentación
   * para IMCLIQEU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcliqeu;
  /**
   * Table Field IMDEREEU. Rights amount in EUR Documentación para IMDEREEU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imdereeu;
  /**
   * Table Field NUTITAGR. Grouped volume Documentación para NUTITAGR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal nutitagr;
  /**
   * Table Field IMNETOBR. Net broker consideration in order currency
   * Documentación para IMNETOBR <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imnetobr;
  /**
   * Table Field PODEVOLU. Rebate percentage Documentación para PODEVOLU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal podevolu;
  /**
   * Table Field POTAXBRK. % Market tax broker Documentación para POTAXBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal potaxbrk;
  /**
   * Table Field PCCOMBRK. % Commission broker Documentación para PCCOMBRK <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal pccombrk;
  /**
   * Table Field CDCLEARE. Clearer Documentación para CDCLEARE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdcleare;
  /**
   * Table Field ACCTATCLE. Account to clearer Documentación para ACCTATCLE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String acctatcle;
  /**
   * Table Field CDBENCLE. Beneficiary code of the clearer SettlementChain
   * Documentación para CDBENCLE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdbencle;
  /**
   * Table Field ACBENCLE. Beneficiary Account of the clearer SettlementChain
   * Documentación para ACBENCLE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String acbencle;
  /**
   * Table Field IDBENCLE. Beneficiary ID of the clearer SettlementChain
   * Documentación para IDBENCLE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String idbencle;
  /**
   * Table Field GCLECODE. Global Custodian code of the clearer SettlementChain
   * Documentación para GCLECODE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String gclecode;
  /**
   * Table Field GCLEACCT. Global Custodian Account of the clearer
   * SettlementChain Documentación para GCLEACCT <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String gcleacct;
  /**
   * Table Field GCLEID. Global Custodian ID of the clearer SettlementChain
   * Documentación para GCLEID <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String gcleid;
  /**
   * Table Field LCLECODE. Local Custodian code of the clearer SettlementChain
   * Documentación para LCLECODE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String lclecode;
  /**
   * Table Field LCLEACCT. Local Custodian Account of the clearer
   * SettlementChain Documentación para LCLEACCT <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String lcleacct;
  /**
   * Table Field LCLEID. Local Custodian ID of the clearer SettlementChain
   * Documentación para LCLEID <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String lcleid;
  /**
   * Table Field PCCLESET. Place of Settlement of the clearer SettlementChain
   * Documentación para PCCLESET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String pccleset;
  /**
   * Table Field SENDBECLE. Send to beneficiary of the clearer SettlementChain
   * Documentación para SENDBECLE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Character sendbecle;
  /**
   * Table Field TPSETCLE. Settlement Type of the clearer SettlementChain
   * Documentación para TPSETCLE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String tpsetcle;
  /**
   * Table Field TYSETCLE. Settlement Type Clearer Documentación para TYSETCLE
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String tysetcle;
  /**
   * Table Field CDCUSTOD. Custodian Documentación para CDCUSTOD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdcustod;
  /**
   * Table Field ACCTATCUS. Account to custodian Documentación para ACCTATCUS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String acctatcus;
  /**
   * Table Field CDBENCUS. Beneficiary code of the custodian SettlementChain
   * Documentación para CDBENCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdbencus;
  /**
   * Table Field ACBENCUS. Beneficiary Account of the custodian SettlementChain
   * Documentación para ACBENCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String acbencus;
  /**
   * Table Field IDBENCUS. Beneficiary ID of the custodian SettlementChain
   * Documentación para IDBENCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String idbencus;
  /**
   * Table Field GCUSCODE. Global Custodian code of the custodian
   * SettlementChain Documentación para GCUSCODE <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String gcuscode;
  /**
   * Table Field GCUSACCT. Global Custodian Account of the custotian
   * SettlementChain Documentación para GCUSACCT <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String gcusacct;
  /**
   * Table Field GCUSID. Global Custodian ID of the custodian SettlementChain
   * Documentación para GCUSID <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String gcusid;
  /**
   * Table Field LCUSCODE. Local Custodian code of the custodian SettlementChain
   * Documentación para LCUSCODE <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String lcuscode;
  /**
   * Table Field LCUSACCT. Local Custodian Account of the custotian
   * SettlementChain Documentación para LCUSACCT <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String lcusacct;
  /**
   * Table Field LCUSID. Local Custodian ID of the custodian SettlementChain
   * Documentación para LCUSID <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String lcusid;
  /**
   * Table Field PCCUSSET. Place of Settlement of the custodian SettlementChain
   * Documentación para PCCUSSET <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String pccusset;
  /**
   * Table Field SENDBECUS. Send to beneficiary of the cusotdian SettlementChain
   * Documentación para SENDBECUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Character sendbecus;
  /**
   * Table Field TPSETCUS. Settlement Type of the custodian SettlementChain
   * Documentación para TPSETCUS <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String tpsetcus;
  /**
   * Table Field TYSETCUS. Settlement Type Custodian Documentación para TYSETCUS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String tysetcus;
  /**
   * Table Field CDENTLIQ. Entity clearer Documentación para CDENTLIQ <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  protected java.lang.String cdentliq;
  /**
   * Table Field CDREFBAN. Bank reference Documentación para CDREFBAN <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdrefban;
  /**
   * Table Field NBTITULA. Titular name Documentación para NBTITULA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String nbtitula;
  /**
   * Table Field FEVALOR. Settlement Date Documentación para FEVALOR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.util.Date fevalor;
  /**
   * Table Field FEVALORR. Settlement Date real Documentación para FEVALORR <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.util.Date fevalorr;
  /**
   * Table Field FETRADE. Trade Date Documentación para FETRADE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.util.Date fetrade;
  /**
   * Table Field CDSTCEXC. Stock Exchange Documentación para CDSTCEXC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdstcexc;
  /**
   * Table Field CDMONISO. Currency Documentación para CDMONISO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdmoniso;
  /**
   * Table Field IMCAMALO. Client average price allocation Documentación para
   * IMCAMALO <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imcamalo;
  /**
   * Table Field IMMARPRI. Market Average Price Documentación para IMMARPRI <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal immarpri;
  /**
   * Table Field IMCLECHA. Clearer charge Documentación para IMCLECHA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imclecha;
  /**
   * Table Field PKBROKER1. PK sent Broker Documentación para PKBROKER1 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Integer pkbroker1;
  /**
   * Table Field PKENTITY. PK sent entity Documentación para PKENTITY <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String pkentity;
  /**
   * Table Field CDTPOPER. Operation Type Documentación para CDTPOPER <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Character cdtpoper;
  /**
   * Table Field CDISIN. ISIN code Documentación para CDISIN <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdisin;
  /**
   * Table Field NBVALORS. ISIN name Documentación para NBVALORS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String nbvalors;
  /**
   * Table Field NUOPROUT. Routing Operation Number Documentación para NUOPROUT
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Integer nuoprout;
  /**
   * Table Field NUPAREJE. Execution Partial Number Documentación para NUPAREJE
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Short nupareje;
  /**
   * Table Field NUOPEAUD. AUD operation number Documentación para NUOPEAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String nuopeaud;
  /**
   * Table Field PCEFECUP. % Efecto cupón Documentación para PCEFECUP <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal pcefecup;
  /**
   * Table Field CDMODORD. Order mode Documentación para CDMODORD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdmodord;
  /**
   * Table Field CDORDENA. Ordena Documentación para CDORDENA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdordena;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Date fhaudit;
  /**
   * Table Field NUVERSION. Audit version number Documentación para NUVERSION
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.Integer nuversion;

  /**
   * Table Field IMGASLIQDV. Gastos Liquidacion en Divisa Documentación para
   * IMGASLIQDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgasliqdv;

  /**
   * Table Field IMGASLIQEU. Gastos Liquidacion en EUR Documentación para
   * IMGASLIQEU <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgasliqeu;

  /**
   * Table Field IMGASGESQDV. Gastos Gestion en Divisa Documentación para
   * IMGASGESDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgasgesdv;

  /**
   * Table Field IMGASGESEU. Gastos Gestion en EUR Documentación para IMGASGESEU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgasgeseu;

  /**
   * Table Field IMGASTOTDV. Gastos Totales en Divisa Documentación para
   * IMGASTOTDV <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgastotdv;

  /**
   * Table Field IMGASTOTEU. Gastos Totales en EUR Documentación para IMGASTOTEU
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.math.BigDecimal imgastoteu;

  /**
   * Table Field CDINDGAS. Indicador Gastos Documentación para CDINDGAS <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected Character cdindgas;

  /**
   * Table Field CDTYP. Propio (P). Tercero (T) DocumentaciÃ³n para CDTYP <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column
  protected java.lang.String cdtyp;

  /**
   * Relation 1..1 with table TMCT0STA. Constraint de relación entre TMCT0CTD y
   * TMCT0STA <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  protected Tmct0sta tmct0sta;

  /**
   * Relation 1..n with table TMCT0CTT. Constraint de relación entre TMCT0CTT y
   * TMCT0CTD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  // @ManyToOne(fetch = FetchType.LAZY)
  // @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName =
  // "CDESTADO", nullable = false), @JoinColumn(name = "CDTIPEST",
  // referencedColumnName = "CDTIPEST",
  // nullable = false)
  // })
  // protected List<Tmct0ctt> tmct0ctts;

  public Tmct0ctdId getId() {
    return id;
  }

  public Tmct0cta getTmct0cta() {
    return tmct0cta;
  }

  public java.lang.String getCdalias() {
    return cdalias;
  }

  public java.lang.String getDescrali() {
    return descrali;
  }

  public java.math.BigDecimal getImcammed() {
    return imcammed;
  }

  public java.math.BigDecimal getImcamnet() {
    return imcamnet;
  }

  public java.math.BigDecimal getImtotbru() {
    return imtotbru;
  }

  public java.math.BigDecimal getImtotnet() {
    return imtotnet;
  }

  public java.math.BigDecimal getEutotbru() {
    return eutotbru;
  }

  public java.math.BigDecimal getEutotnet() {
    return eutotnet;
  }

  public java.math.BigDecimal getImnetbrk() {
    return imnetbrk;
  }

  public java.math.BigDecimal getEunetbrk() {
    return eunetbrk;
  }

  public java.math.BigDecimal getImcomisn() {
    return imcomisn;
  }

  public java.math.BigDecimal getImcomieu() {
    return imcomieu;
  }

  public java.math.BigDecimal getImcomdev() {
    return imcomdev;
  }

  public java.math.BigDecimal getEucomdev() {
    return eucomdev;
  }

  public java.math.BigDecimal getImfibrdv() {
    return imfibrdv;
  }

  public java.math.BigDecimal getImfibreu() {
    return imfibreu;
  }

  public java.math.BigDecimal getImsvb() {
    return imsvb;
  }

  public java.math.BigDecimal getImsvbeu() {
    return imsvbeu;
  }

  public java.math.BigDecimal getImbrmerc() {
    return imbrmerc;
  }

  public java.math.BigDecimal getImbrmreu() {
    return imbrmreu;
  }

  public java.math.BigDecimal getImgastos() {
    return imgastos;
  }

  public java.math.BigDecimal getImgatdvs() {
    return imgatdvs;
  }

  public java.math.BigDecimal getImimpdvs() {
    return imimpdvs;
  }

  public java.math.BigDecimal getImimpeur() {
    return imimpeur;
  }

  public java.math.BigDecimal getImcconeu() {
    return imcconeu;
  }

  public java.math.BigDecimal getImcliqeu() {
    return imcliqeu;
  }

  public java.math.BigDecimal getImdereeu() {
    return imdereeu;
  }

  public java.math.BigDecimal getNutitagr() {
    return nutitagr;
  }

  public java.math.BigDecimal getImnetobr() {
    return imnetobr;
  }

  public java.math.BigDecimal getPodevolu() {
    return podevolu;
  }

  public java.math.BigDecimal getPotaxbrk() {
    return potaxbrk;
  }

  public java.math.BigDecimal getPccombrk() {
    return pccombrk;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public java.lang.String getAcctatcle() {
    return acctatcle;
  }

  public java.lang.String getCdbencle() {
    return cdbencle;
  }

  public java.lang.String getAcbencle() {
    return acbencle;
  }

  public java.lang.String getIdbencle() {
    return idbencle;
  }

  public java.lang.String getGclecode() {
    return gclecode;
  }

  public java.lang.String getGcleacct() {
    return gcleacct;
  }

  public java.lang.String getGcleid() {
    return gcleid;
  }

  public java.lang.String getLclecode() {
    return lclecode;
  }

  public java.lang.String getLcleacct() {
    return lcleacct;
  }

  public java.lang.String getLcleid() {
    return lcleid;
  }

  public java.lang.String getPccleset() {
    return pccleset;
  }

  public Character getSendbecle() {
    return sendbecle;
  }

  public java.lang.String getTpsetcle() {
    return tpsetcle;
  }

  public java.lang.String getTysetcle() {
    return tysetcle;
  }

  public java.lang.String getCdcustod() {
    return cdcustod;
  }

  public java.lang.String getAcctatcus() {
    return acctatcus;
  }

  public java.lang.String getCdbencus() {
    return cdbencus;
  }

  public java.lang.String getAcbencus() {
    return acbencus;
  }

  public java.lang.String getIdbencus() {
    return idbencus;
  }

  public java.lang.String getGcuscode() {
    return gcuscode;
  }

  public java.lang.String getGcusacct() {
    return gcusacct;
  }

  public java.lang.String getGcusid() {
    return gcusid;
  }

  public java.lang.String getLcuscode() {
    return lcuscode;
  }

  public java.lang.String getLcusacct() {
    return lcusacct;
  }

  public java.lang.String getLcusid() {
    return lcusid;
  }

  public java.lang.String getPccusset() {
    return pccusset;
  }

  public Character getSendbecus() {
    return sendbecus;
  }

  public java.lang.String getTpsetcus() {
    return tpsetcus;
  }

  public java.lang.String getTysetcus() {
    return tysetcus;
  }

  public java.lang.String getCdentliq() {
    return cdentliq;
  }

  public java.lang.String getCdrefban() {
    return cdrefban;
  }

  public java.lang.String getNbtitula() {
    return nbtitula;
  }

  public java.util.Date getFevalor() {
    return fevalor;
  }

  public java.util.Date getFevalorr() {
    return fevalorr;
  }

  public java.util.Date getFetrade() {
    return fetrade;
  }

  public java.lang.String getCdstcexc() {
    return cdstcexc;
  }

  public java.lang.String getCdmoniso() {
    return cdmoniso;
  }

  public java.math.BigDecimal getImcamalo() {
    return imcamalo;
  }

  public java.math.BigDecimal getImmarpri() {
    return immarpri;
  }

  public java.math.BigDecimal getImclecha() {
    return imclecha;
  }

  public Integer getPkbroker1() {
    return pkbroker1;
  }

  public java.lang.String getPkentity() {
    return pkentity;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public java.lang.String getCdisin() {
    return cdisin;
  }

  public java.lang.String getNbvalors() {
    return nbvalors;
  }

  public Integer getNuoprout() {
    return nuoprout;
  }

  public Short getNupareje() {
    return nupareje;
  }

  public java.lang.String getNuopeaud() {
    return nuopeaud;
  }

  public java.math.BigDecimal getPcefecup() {
    return pcefecup;
  }

  public java.lang.String getCdmodord() {
    return cdmodord;
  }

  public java.lang.String getCdordena() {
    return cdordena;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public java.lang.Integer getNuversion() {
    return nuversion;
  }

  public java.math.BigDecimal getImgasliqdv() {
    return imgasliqdv;
  }

  public java.math.BigDecimal getImgasliqeu() {
    return imgasliqeu;
  }

  public java.math.BigDecimal getImgasgesdv() {
    return imgasgesdv;
  }

  public java.math.BigDecimal getImgasgeseu() {
    return imgasgeseu;
  }

  public java.math.BigDecimal getImgastotdv() {
    return imgastotdv;
  }

  public java.math.BigDecimal getImgastoteu() {
    return imgastoteu;
  }

  public Character getCdindgas() {
    return cdindgas;
  }

  public java.lang.String getCdtyp() {
    return cdtyp;
  }

  public Tmct0sta getTmct0sta() {
    return tmct0sta;
  }

  public void setId(Tmct0ctdId id) {
    this.id = id;
  }

  public void setTmct0cta(Tmct0cta tmct0cta) {
    this.tmct0cta = tmct0cta;
  }

  public void setCdalias(java.lang.String cdalias) {
    this.cdalias = cdalias;
  }

  public void setDescrali(java.lang.String descrali) {
    this.descrali = descrali;
  }

  public void setImcammed(java.math.BigDecimal imcammed) {
    this.imcammed = imcammed;
  }

  public void setImcamnet(java.math.BigDecimal imcamnet) {
    this.imcamnet = imcamnet;
  }

  public void setImtotbru(java.math.BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public void setImtotnet(java.math.BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public void setEutotbru(java.math.BigDecimal eutotbru) {
    this.eutotbru = eutotbru;
  }

  public void setEutotnet(java.math.BigDecimal eutotnet) {
    this.eutotnet = eutotnet;
  }

  public void setImnetbrk(java.math.BigDecimal imnetbrk) {
    this.imnetbrk = imnetbrk;
  }

  public void setEunetbrk(java.math.BigDecimal eunetbrk) {
    this.eunetbrk = eunetbrk;
  }

  public void setImcomisn(java.math.BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public void setImcomieu(java.math.BigDecimal imcomieu) {
    this.imcomieu = imcomieu;
  }

  public void setImcomdev(java.math.BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  public void setEucomdev(java.math.BigDecimal eucomdev) {
    this.eucomdev = eucomdev;
  }

  public void setImfibrdv(java.math.BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  public void setImfibreu(java.math.BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  public void setImsvb(java.math.BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  public void setImsvbeu(java.math.BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  public void setImbrmerc(java.math.BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  public void setImbrmreu(java.math.BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  public void setImgastos(java.math.BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  public void setImgatdvs(java.math.BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  public void setImimpdvs(java.math.BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  public void setImimpeur(java.math.BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  public void setImcconeu(java.math.BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  public void setImcliqeu(java.math.BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  public void setImdereeu(java.math.BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  public void setNutitagr(java.math.BigDecimal nutitagr) {
    this.nutitagr = nutitagr;
  }

  public void setImnetobr(java.math.BigDecimal imnetobr) {
    this.imnetobr = imnetobr;
  }

  public void setPodevolu(java.math.BigDecimal podevolu) {
    this.podevolu = podevolu;
  }

  public void setPotaxbrk(java.math.BigDecimal potaxbrk) {
    this.potaxbrk = potaxbrk;
  }

  public void setPccombrk(java.math.BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public void setAcctatcle(java.lang.String acctatcle) {
    this.acctatcle = acctatcle;
  }

  public void setCdbencle(java.lang.String cdbencle) {
    this.cdbencle = cdbencle;
  }

  public void setAcbencle(java.lang.String acbencle) {
    this.acbencle = acbencle;
  }

  public void setIdbencle(java.lang.String idbencle) {
    this.idbencle = idbencle;
  }

  public void setGclecode(java.lang.String gclecode) {
    this.gclecode = gclecode;
  }

  public void setGcleacct(java.lang.String gcleacct) {
    this.gcleacct = gcleacct;
  }

  public void setGcleid(java.lang.String gcleid) {
    this.gcleid = gcleid;
  }

  public void setLclecode(java.lang.String lclecode) {
    this.lclecode = lclecode;
  }

  public void setLcleacct(java.lang.String lcleacct) {
    this.lcleacct = lcleacct;
  }

  public void setLcleid(java.lang.String lcleid) {
    this.lcleid = lcleid;
  }

  public void setPccleset(java.lang.String pccleset) {
    this.pccleset = pccleset;
  }

  public void setSendbecle(Character sendbecle) {
    this.sendbecle = sendbecle;
  }

  public void setTpsetcle(java.lang.String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  public void setTysetcle(java.lang.String tysetcle) {
    this.tysetcle = tysetcle;
  }

  public void setCdcustod(java.lang.String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public void setAcctatcus(java.lang.String acctatcus) {
    this.acctatcus = acctatcus;
  }

  public void setCdbencus(java.lang.String cdbencus) {
    this.cdbencus = cdbencus;
  }

  public void setAcbencus(java.lang.String acbencus) {
    this.acbencus = acbencus;
  }

  public void setIdbencus(java.lang.String idbencus) {
    this.idbencus = idbencus;
  }

  public void setGcuscode(java.lang.String gcuscode) {
    this.gcuscode = gcuscode;
  }

  public void setGcusacct(java.lang.String gcusacct) {
    this.gcusacct = gcusacct;
  }

  public void setGcusid(java.lang.String gcusid) {
    this.gcusid = gcusid;
  }

  public void setLcuscode(java.lang.String lcuscode) {
    this.lcuscode = lcuscode;
  }

  public void setLcusacct(java.lang.String lcusacct) {
    this.lcusacct = lcusacct;
  }

  public void setLcusid(java.lang.String lcusid) {
    this.lcusid = lcusid;
  }

  public void setPccusset(java.lang.String pccusset) {
    this.pccusset = pccusset;
  }

  public void setSendbecus(Character sendbecus) {
    this.sendbecus = sendbecus;
  }

  public void setTpsetcus(java.lang.String tpsetcus) {
    this.tpsetcus = tpsetcus;
  }

  public void setTysetcus(java.lang.String tysetcus) {
    this.tysetcus = tysetcus;
  }

  public void setCdentliq(java.lang.String cdentliq) {
    this.cdentliq = cdentliq;
  }

  public void setCdrefban(java.lang.String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public void setNbtitula(java.lang.String nbtitula) {
    this.nbtitula = nbtitula;
  }

  public void setFevalor(java.util.Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setFevalorr(java.util.Date fevalorr) {
    this.fevalorr = fevalorr;
  }

  public void setFetrade(java.util.Date fetrade) {
    this.fetrade = fetrade;
  }

  public void setCdstcexc(java.lang.String cdstcexc) {
    this.cdstcexc = cdstcexc;
  }

  public void setCdmoniso(java.lang.String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setImcamalo(java.math.BigDecimal imcamalo) {
    this.imcamalo = imcamalo;
  }

  public void setImmarpri(java.math.BigDecimal immarpri) {
    this.immarpri = immarpri;
  }

  public void setImclecha(java.math.BigDecimal imclecha) {
    this.imclecha = imclecha;
  }

  public void setPkbroker1(Integer pkbroker1) {
    this.pkbroker1 = pkbroker1;
  }

  public void setPkentity(java.lang.String pkentity) {
    this.pkentity = pkentity;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public void setCdisin(java.lang.String cdisin) {
    this.cdisin = cdisin;
  }

  public void setNbvalors(java.lang.String nbvalors) {
    this.nbvalors = nbvalors;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  public void setNuopeaud(java.lang.String nuopeaud) {
    this.nuopeaud = nuopeaud;
  }

  public void setPcefecup(java.math.BigDecimal pcefecup) {
    this.pcefecup = pcefecup;
  }

  public void setCdmodord(java.lang.String cdmodord) {
    this.cdmodord = cdmodord;
  }

  public void setCdordena(java.lang.String cdordena) {
    this.cdordena = cdordena;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public void setNuversion(java.lang.Integer nuversion) {
    this.nuversion = nuversion;
  }

  public void setImgasliqdv(java.math.BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  public void setImgasliqeu(java.math.BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  public void setImgasgesdv(java.math.BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  public void setImgasgeseu(java.math.BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  public void setImgastotdv(java.math.BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  public void setImgastoteu(java.math.BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }

  public void setCdtyp(java.lang.String cdtyp) {
    this.cdtyp = cdtyp;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Override
  public String toString() {
    return "Tmct0ctd [id=" + id + "]";
  }

  public static Tmct0ctd mapFromObject(Object[] obj) {

    Tmct0ctd tmct0ctd = new Tmct0ctd();

    Tmct0ctdId tmct0ctdId = new Tmct0ctdId();
    tmct0ctdId.setNucnfclt((String) obj[6]);
    tmct0ctdId.setCdmercad((String) obj[7]);
    tmct0ctdId.setNuorden((String) obj[8]);
    tmct0ctdId.setCdbroker((String) obj[9]);
    tmct0ctdId.setNbooking((String) obj[10]);
    tmct0ctd.setId(tmct0ctdId);

    tmct0ctd.setCdalias((String) obj[0]);
    tmct0ctd.setFetrade((Date) obj[1]);
    tmct0ctd.setNuoprout(((BigDecimal) obj[2]).intValue());
    tmct0ctd.setNupareje(((BigDecimal) obj[3]).shortValue());
    tmct0ctd.setCdcleare((String) obj[4]);
    tmct0ctd.setCdisin((String) obj[5]);

    tmct0ctd.setCdtpoper((Character) obj[11]);
    tmct0ctd.setEutotbru((BigDecimal) obj[12]);
    tmct0ctd.setNutitagr((BigDecimal) obj[13]);
    tmct0ctd.setEucomdev((BigDecimal) obj[14]);
    tmct0ctd.setEutotnet((BigDecimal) obj[15]);
    tmct0ctd.setImfibreu((BigDecimal) obj[16]);
    tmct0ctd.setFhaudit(((Timestamp) obj[17]));
    tmct0ctd.setImsvbeu(((BigDecimal) obj[18]));

    return tmct0ctd;
  }

}
