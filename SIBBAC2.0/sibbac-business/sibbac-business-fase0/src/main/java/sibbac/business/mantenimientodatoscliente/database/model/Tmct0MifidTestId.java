package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class Tmct0MifidTestId implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "IDEMPR", nullable = false, length = 4)
  private String idempr;

  @Column(name = "TIPOPERS", nullable = false, length = 1)
  private String tipopers;

  @Column(name = "CODPERS", nullable = false, length = 8)
  private Long codpers;

  @Column(name = "IDTEST", nullable = false, length = 8)
  private Long idtest;

  @Column(name = "FECFIN", nullable = false, length = 4)
  private Date fecfin;

  public Tmct0MifidTestId() {
  }

  public Tmct0MifidTestId(String idempr, String tipopers, Long codpers, long idtest) {
    super();
    this.idempr = idempr;
    this.tipopers = tipopers;
    this.codpers = codpers;
    this.idtest = idtest;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codpers == null) ? 0 : codpers.hashCode());
    result = prime * result + ((fecfin == null) ? 0 : fecfin.hashCode());
    result = prime * result + ((idempr == null) ? 0 : idempr.hashCode());
    result = prime * result + ((idtest == null) ? 0 : idtest.hashCode());
    result = prime * result + ((tipopers == null) ? 0 : tipopers.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0MifidTestId other = (Tmct0MifidTestId) obj;
    if (codpers == null) {
      if (other.codpers != null)
        return false;
    }
    else if (!codpers.equals(other.codpers))
      return false;
    if (fecfin == null) {
      if (other.fecfin != null)
        return false;
    }
    else if (!fecfin.equals(other.fecfin))
      return false;
    if (idempr == null) {
      if (other.idempr != null)
        return false;
    }
    else if (!idempr.equals(other.idempr))
      return false;
    if (idtest == null) {
      if (other.idtest != null)
        return false;
    }
    else if (!idtest.equals(other.idtest))
      return false;
    if (tipopers == null) {
      if (other.tipopers != null)
        return false;
    }
    else if (!tipopers.equals(other.tipopers))
      return false;
    return true;
  }

  public String getIdempr() {
    return idempr;
  }

  public void setIdempr(String idempr) {
    this.idempr = idempr;
  }

  public String getTipopers() {
    return tipopers;
  }

  public void setTipopers(String tipopers) {
    this.tipopers = tipopers;
  }

  public Long getCodpers() {
    return codpers;
  }

  public void setCodpers(Long codpers) {
    this.codpers = codpers;
  }

  public Long getIdtest() {
    return idtest;
  }

  public void setIdtest(Long idtest) {
    this.idtest = idtest;
  }

  public Date getFecfin() {
    return fecfin;
  }

  public void setFecfin(Date fecfin) {
    this.fecfin = fecfin;
  }

  /**
   * @return El código de persona con el formato esperado por el código de base de datos de Partenón.
   */
  @Transient
  public String getCDBDP() {
    return String.format("%09d", codpers);
  }

  @Override
  public String toString() {
    return "Tmct0MifidTestId [idempr=" + idempr + ", tipopers=" + tipopers + ", codpers=" + codpers + ", idtest="
        + idtest + ", fecfin=" + fecfin + "]";
  }

}
