package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Tmct0ejeFeeSchemaId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6960112326460445968L;
	private String				nuorden;
	private String				nbooking;
	private String				nuejecuc;

	public Tmct0ejeFeeSchemaId() {
	}

	public Tmct0ejeFeeSchemaId( String nuorden, String nbooking, String nuejecuc ) {
		this.nuorden = nuorden;
		this.nbooking = nbooking;
		this.nuejecuc = nuejecuc;
	}

	@Column( name = "NUORDEN", nullable = false, length = 20 )
	public String getNuorden() {
		return this.nuorden;
	}

	public void setNuorden( String nuorden ) {
		this.nuorden = nuorden;
	}

	@Column( name = "NBOOKING", nullable = false, length = 20 )
	public String getNbooking() {
		return this.nbooking;
	}

	public void setNbooking( String nbooking ) {
		this.nbooking = nbooking;
	}

	@Column( name = "NUEJECUC", nullable = false, length = 20 )
	public String getNuejecuc() {
		return this.nuejecuc;
	}

	public void setNuejecuc( String nuejecuc ) {
		this.nuejecuc = nuejecuc;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0ejeFeeSchemaId ) )
			return false;
		Tmct0ejeFeeSchemaId castOther = ( Tmct0ejeFeeSchemaId ) other;

		return ( ( this.getNuorden() == castOther.getNuorden() ) || ( this.getNuorden() != null && castOther.getNuorden() != null && this
				.getNuorden().equals( castOther.getNuorden() ) ) )
				&& ( ( this.getNbooking() == castOther.getNbooking() ) || ( this.getNbooking() != null && castOther.getNbooking() != null && this
						.getNbooking().equals( castOther.getNbooking() ) ) )
				&& ( ( this.getNuejecuc() == castOther.getNuejecuc() ) || ( this.getNuejecuc() != null && castOther.getNuejecuc() != null && this
						.getNuejecuc().equals( castOther.getNuejecuc() ) ) );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getNuorden() == null ? 0 : this.getNuorden().hashCode() );
		result = 37 * result + ( getNbooking() == null ? 0 : this.getNbooking().hashCode() );
		result = 37 * result + ( getNuejecuc() == null ? 0 : this.getNuejecuc().hashCode() );
		return result;
	}

  @Override
  public String toString() {
    return "Tmct0ejeFeeSchemaId [nuorden=" + nuorden + ", nbooking=" + nbooking + ", nuejecuc=" + nuejecuc + "]";
  }

	
	
	
}
