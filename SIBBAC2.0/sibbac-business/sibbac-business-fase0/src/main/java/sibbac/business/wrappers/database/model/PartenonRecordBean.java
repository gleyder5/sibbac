package sibbac.business.wrappers.database.model;

import static org.apache.commons.lang3.StringUtils.left;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import sibbac.business.wrappers.database.data.Tmct0AfiErrorData;
import sibbac.database.DBConstants.WRAPPERS;

@Table(name = WRAPPERS.PARTENON_RECORD_BEAN)
@Entity
public class PartenonRecordBean extends OperacionEspecialRecordBean {

  /**
	 * 
	 */
  private static final long serialVersionUID = -3013038522700417895L;
  

  @Column(name = "CODEMPR", length = 4)
  private String codeMpr;
  @Column(name = "NUMORDEN", length = 10)
  private String numOrden;
  @Column(name = "IDSENC", precision = 2)
  private BigDecimal idSenc;
  @Column(name = "NIF_BIC", length = 11)
  private String nifbic;
  @Column(name = "NOMAPELL", length = 40)
  private String nomapell;
  @Transient
  private String nbclient;
  @Transient
  private String nbclient1;
  @Transient
  private String nbclient2;
  @Transient
  private String tpdomici;
  @Transient
  private String nbdomici;
  @Transient
  private String nudomici;
  @Column(name = "DOMICILI", length = 40)
  private String domicili;
  @Column(name = "DISTRITO", length = 5)
  private String distrito;
  @Column(name = "POBLACIO", length = 20)
  private String poblacion;
  @Column(name = "PROVINCIA", length = 20)
  private String provincia;
  @Column(name = "PAIS", length = 20)
  private String pais;
  @Column(name = "NACLIDAD", length = 3)
  private String nacionalidad;
  @Column(name = "TIPTITU", length = 1)
  private Character tiptitu;
  @Column(name = "PARTICIP", precision = 5, scale = 2)
  private BigDecimal particip;
  @Column(name = "BOLSA", length = 2)
  private String bolsa;
  @Column(name = "PAISRE", length = 3)
  private String paisResidencia;

  @Column(name = "TIPODOC", length = 1)
  private Character tipoDocumento;
  @Column(name = "TIPPER", length = 1)
  private Character tipoPersona;
  @Column(name = "INDNAC", length = 1)
  private Character indNac;
  @Column(name = "CCV", length = 30)
  private String ccv;
  @Transient
  private String filler;

  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "ID_DESGLOSE_RECORD_BEAN", referencedColumnName = "ID")
  private DesgloseRecordBean desgloseRecordBean;
  @Transient
  private Set<CodigoError> errorSet = new HashSet<CodigoError>();
  @Transient
  private Tmct0alo tmct0alo;
  @Transient
  private Short nucnfliq;
  @Transient
  private Date fechaContratacion;

  @Transient
  private Tmct0AfiError afiError;

  @Transient
  private List<Tmct0afiId> afiIdsModificados = new ArrayList<Tmct0afiId>();

  public PartenonRecordBean() {
    super();
  }

  public PartenonRecordBean(Tmct0AfiError tmct0afierror) {
    setAfiError(tmct0afierror);
    try {
      this.setId(tmct0afierror.getId());
      setTmct0alo(tmct0afierror.getTmct0alo());
      setNucnfliq(tmct0afierror.getNucnfliq());

      this.setNifbic(tmct0afierror.getNudnicif());
      this.setNbclient(tmct0afierror.getNbclient());
      this.setNbclient1(tmct0afierror.getNbclient1());
      this.setNbclient2(tmct0afierror.getNbclient2());
      this.setTpdomici(tmct0afierror.getTpdomici());
      this.setNbdomici(tmct0afierror.getNbdomici());
      this.setNudomici(tmct0afierror.getNudomici());
      this.setDistrito(tmct0afierror.getCdpostal());
      this.setPoblacion(tmct0afierror.getNbciudad());
      this.setProvincia(tmct0afierror.getNbprovin());
      this.setNacionalidad(tmct0afierror.getCdnactit());
      if (tmct0afierror.getTptiprep() != null) {
        this.setTiptitu(tmct0afierror.getTptiprep());
      }
      this.setPaisResidencia(tmct0afierror.getCddepais());
      if (tmct0afierror.getTpidenti() != null) {
        this.setTipoDocumento(tmct0afierror.getTpidenti());
      }
      if (tmct0afierror.getTpsocied() != null) {
        this.setTipoPersona(tmct0afierror.getTpsocied());
      }
      if (tmct0afierror.getTpnactit() != null) {
        this.setIndNac(tmct0afierror.getTpnactit());
      }
      this.setCcv(tmct0afierror.getCcv());
      this.setParticip(tmct0afierror.getParticip());
      this.setNumOrden(tmct0afierror.getNureford());
      this.setTipoRegistro(tmct0afierror.getIdmensaje());
      this.setTmct0alo(tmct0afierror.getTmct0alo());
      this.setNucnfliq(tmct0afierror.getNucnfliq());
    }
    catch (Exception e) {
      e.getMessage();
    }

  }

  public PartenonRecordBean(Tmct0afi tmct0afi) {
    try {

      this.setId(tmct0afi.getId().getNusecuen().longValue());
      this.setTmct0alo(tmct0afi.getTmct0alo());
      this.setNucnfliq(tmct0afi.getId().getNucnfliq());

      this.setNifbic(tmct0afi.getCdholder());
      this.setNbclient(tmct0afi.getNbclient());
      this.setNbclient1(tmct0afi.getNbclient1());
      this.setNbclient2(tmct0afi.getNbclient2());
      this.setDomicili(tmct0afi.getTpdomici() + " " + tmct0afi.getNbdomici());
      this.setTpdomici(tmct0afi.getTpdomici());
      this.setNbdomici(tmct0afi.getNbdomici());
      this.setNudomici(tmct0afi.getNudomici());
      this.setDistrito(tmct0afi.getCdpostal());
      this.setPoblacion(tmct0afi.getNbciudad());
      this.setProvincia(tmct0afi.getNbprovin());
      this.setNacionalidad(tmct0afi.getCdnactit());
      if (tmct0afi.getTptiprep() != null) {
        this.setTiptitu(tmct0afi.getTptiprep());
      }
      this.setPaisResidencia(tmct0afi.getCddepais());

      if (tmct0afi.getTpidenti() != null) {
        this.setTipoDocumento(tmct0afi.getTpidenti());
      }
      if (tmct0afi.getTpsocied() != null) {
        this.setTipoPersona(tmct0afi.getTpsocied());
      }
      if (tmct0afi.getTpnactit() != null) {
        this.setIndNac(tmct0afi.getTpnactit());
      }
      this.setCcv(tmct0afi.getCcv());
      this.setParticip(tmct0afi.getParticip());
      if (tmct0afi.getTptiprep() == null && tmct0afi.getTptiprep() != TipoTitular.REPRESENTANTE.getTipoTitular()) {
        this.setParticip(new BigDecimal("100"));
      }
    }
    catch (Exception e) {
      if (tmct0afi.getTmct0alo() != null) {
        LOG.debug("[PartenonRecordBean - Constructor] Error pasando tmct0afi a bean: " + tmct0afi.getTmct0alo().getId());
      }
      else {
        LOG.debug("[PartenonRecordBean - Constructor] Error pasando tmct0afi a bean: (nudnicif)"
            + tmct0afi.getNudnicif());
      }
    }

  }

  public PartenonRecordBean(String nbclient, String nbclient1, String nbclient2, String nudnicif, Character tpnactit,
      String nacionalidad, Character tipodocumento, String paisres, String provincia, String ciudad, String codpostal,
      Character tptiprep, Character tpsocied, BigDecimal particip, String ccv, String nureford, String tpdomici,
      String nbdomici, String nudomici) {
    this.nbclient = nbclient;
    this.nbclient1 = nbclient1;
    this.nbclient2 = nbclient2;
    this.nifbic = nudnicif;
    this.indNac = tpnactit;
    this.nacionalidad = nacionalidad;
    this.tipoDocumento = tipodocumento;
    this.paisResidencia = paisres;
    this.provincia = provincia;
    this.poblacion = ciudad;
    this.distrito = codpostal;
    this.tiptitu = tptiprep;
    this.tipoPersona = tpsocied;
    this.particip = particip;
    this.ccv = ccv;
    this.numOrden = nureford;
    if (tpdomici == null) {
      this.tpdomici = "";
    }
    else {
      this.tpdomici = tpdomici;
    }
    if (nbdomici == null) {
      this.nbdomici = "";
    }
    else {
      this.nbdomici = nbdomici;
    }
    if (nudomici == null) {
      this.nudomici = "";
    }
    else {
      this.nudomici = nudomici;
    }

    // Se mete a null para que se use el nudomici
    this.domicili = null;

  }

  public PartenonRecordBean(PartenonRecordBean bean) {
    this.nbclient = bean.nbclient;
    this.nbclient1 = bean.nbclient1;
    this.nbclient2 = bean.nbclient2;
    this.nifbic = bean.nifbic;
    this.indNac = bean.indNac;
    this.nacionalidad = bean.nacionalidad;
    this.tipoDocumento = bean.tipoDocumento;
    this.paisResidencia = bean.paisResidencia;
    this.provincia = bean.provincia;
    this.poblacion = bean.poblacion;
    this.distrito = bean.distrito;
    this.tiptitu = bean.tiptitu;
    this.tipoPersona = bean.tipoPersona;
    this.particip = bean.particip;
    this.ccv = bean.ccv;
    this.numOrden = bean.numOrden;
    this.tpdomici = bean.tpdomici;
    this.nbdomici = bean.nbdomici;
    this.nudomici = bean.nudomici;
    this.domicili = bean.domicili;
    this.nomapell = bean.nomapell;
  }

  public String getCodeMpr() {
    return codeMpr;
  }

  public void setCodeMpr(String codeMpr) {
    this.codeMpr = codeMpr;
  }

  public String getNumOrden() {
    return numOrden;
  }

  public void setNumOrden(String numOrden) {
    this.numOrden = numOrden;
  }

  public BigDecimal getIdSenc() {
    return idSenc;
  }

  public void setIdSenc(BigDecimal idSenc) {
    this.idSenc = idSenc;
  }

  public String getNifbic() {
    return nifbic;
  }

  public void setNifbic(String nifbic) {
    this.nifbic = nifbic;
  }

  public String getDomicili() {
    return domicili;
  }

  public void setDomicili(String domicili) {
    this.domicili = domicili;
  }

  public String getTpdomici() {
    return tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  public String getNbdomici() {
    return nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  public String getNudomici() {
    return nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  public String getDistrito() {
    return distrito;
  }

  public void setDistrito(String distrito) {
    this.distrito = distrito;
  }

  public String getPoblacion() {
    return poblacion;
  }

  public void setPoblacion(String poblacion) {
    this.poblacion = poblacion;
  }

  public String getProvincia() {
    return provincia;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public String getPais() {
    return pais;
  }

  public void setPais(String pais) {
    this.pais = pais;
  }

  public String getNacionalidad() {
    return nacionalidad;
  }

  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  public Character getTiptitu() {
    return tiptitu;
  }

  public void setTiptitu(Character tiptitu) {
    this.tiptitu = tiptitu;
  }

  public BigDecimal getParticip() {
    return particip;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  public String getBolsa() {
    return bolsa;
  }

  public void setBolsa(String bolsa) {
    this.bolsa = bolsa;
  }

  public String getPaisResidencia() {
    return paisResidencia;
  }

  public void setPaisResidencia(String paisResidencia) {
    this.paisResidencia = paisResidencia;
  }

  public Character getTipoDocumento() {
    return tipoDocumento;
  }

  public void setTipoDocumento(Character tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  public Character getTipoPersona() {
    return tipoPersona;
  }

  public void setTipoPersona(Character tipoPersona) {
    this.tipoPersona = tipoPersona;
  }

  public Character getIndNac() {
    return indNac;
  }

  public void setIndNac(Character indNac) {
    this.indNac = indNac;
  }

  public String getCcv() {
    return ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  public String getNomapell() {
    return nomapell;
  }

  public void setNomapell(String nomapell) {
    this.nomapell = nomapell;
  }

  public String getNbclient() {
    return nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public String getNbclient1() {
    return nbclient1;
  }

  public void setNbclient1(String nbclient1) {
    this.nbclient1 = nbclient1;
  }

  public String getNbclient2() {
    return nbclient2;
  }

  public void setNbclient2(String nbclient2) {
    this.nbclient2 = nbclient2;
  }

  public String getFiller() {
    return filler;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

  /**
   * @return the desgloseRecordBean
   */
  public DesgloseRecordBean getDesgloseRecordBean() {
    return desgloseRecordBean;
  }

  /**
   * @param desgloseRecordBean the desgloseRecordBean to set
   */
  public void setDesgloseRecordBean(DesgloseRecordBean desgloseRecordBean) {
    this.desgloseRecordBean = desgloseRecordBean;
  }

  public Set<CodigoError> getErrorSet() {
    return errorSet;
  }

  public void setErrorSet(Set<CodigoError> errorSet) {
    this.errorSet = errorSet;
  }

  public Tmct0afi toTmct0afi(short cdbloque) {
    Tmct0afi tmct0afi = new Tmct0afi();

    tmct0afi.setNudnicif(getNifbic());
    if (getNbclient() != null) {
      tmct0afi.setNbclient(left(getNbclient(), 30));
    }
    else {
      tmct0afi.setNbclient("");
    }
    if (getNbclient1() != null) {
      tmct0afi.setNbclient1(left(getNbclient1(), 50));
    }
    else {
      tmct0afi.setNbclient1("");
    }
    if (getNbclient2() != null) {
      tmct0afi.setNbclient2(left(getNbclient2(), 30));
    }
    else {
      tmct0afi.setNbclient2("");
    }
    // Aqui hay una calle y numero en afi
    if (getDomicili() != null && getDomicili().trim().length() == 2) {
      tmct0afi.setTpdomici(getDomicili().substring(0, 2));
      tmct0afi.setNbdomici("");
      tmct0afi.setNudomici("");
    }
    else if (getDomicili() != null && getDomicili().length() > 3) {
      tmct0afi.setTpdomici(getDomicili().substring(0, 2));
      tmct0afi.setNbdomici(getDomicili().substring(3));
      tmct0afi.setNudomici("");
    }
    else {
      if (getTpdomici() != null) {
        tmct0afi.setTpdomici(getTpdomici());
      }
      else {
        tmct0afi.setTpdomici("");
      }
      if (getNbdomici() != null) {
        tmct0afi.setNbdomici(getNbdomici());
      }
      else {
        tmct0afi.setNbdomici("");
      }
      if (getNudomici() != null) {
        tmct0afi.setNudomici(getNudomici());
      }
      else {
        tmct0afi.setNudomici("");
      }
    }

    if (getDistrito() != null) {
      tmct0afi.setCdpostal(getDistrito().toString());
    }
    else {
      tmct0afi.setCdpostal("");
    }
    if (getPoblacion() != null) {
      tmct0afi.setNbciudad(getPoblacion());
    }
    else {
      tmct0afi.setNbciudad("");
    }
    if (getProvincia() != null) {
      tmct0afi.setNbprovin(getProvincia());
    }
    else {
      tmct0afi.setNbprovin("");
    }
    tmct0afi.setCdnactit(getNacionalidad().toString());
    tmct0afi.setTptiprep(getTiptitu().toString().charAt(0));
    if (getPaisResidencia() != null) {
      tmct0afi.setCddepais(getPaisResidencia());
    }
    else {
      tmct0afi.setCddepais("");
    }

    tmct0afi.setTpidenti(getTipoDocumento().toString().charAt(0));
    tmct0afi.setTpsocied(getTipoPersona());
    tmct0afi.setTpnactit(getIndNac());
    tmct0afi.setCcv(getCcv());
    tmct0afi.setParticip(getParticip());
    tmct0afi.setCdusuaud("ROUTING");

    // Rellenos a mano para poder guardar en BBDD
    tmct0afi.setCdbloque(cdbloque);
    tmct0afi.setCdenvaud(' ');
    tmct0afi.setCdinacti('A');
    tmct0afi.setCdusuina("");
    tmct0afi.setFhaudit(new Date());
    tmct0afi.setIdproced("ROUTING");
    tmct0afi.setCdholder(getNifbic());
    if (TipoDocumento.BIC.getTipoDocumento().equals(getTipoDocumento())) {
      tmct0afi.setNudnicif("");
      tmct0afi.setCdddebic(getNifbic());
    }
    else {
      tmct0afi.setNudnicif(getNifbic());
      tmct0afi.setCdddebic("");
    }
    tmct0afi.setTpfisjur(getTipoPersona().toString().charAt(0));
    tmct0afi.setRftitaud(getNifbic());

    // Sin uso
    tmct0afi.setNuoprout(0);
    tmct0afi.setNupareje((short) 0);
    tmct0afi.setNusecnbr((short) 0);

    return tmct0afi;
  }

  public void loadDireccion() {
    if ((StringUtils.isBlank(getTpdomici()) || StringUtils.isBlank(getNbdomici()))
        && StringUtils.isNotBlank(getDomicili())) {
      String[] tokens = StringUtils.trim(getDomicili()).split(" ");
      if (tokens.length > 1) {
        if (tokens[0].length() == 2) {
          setTpdomici(StringUtils.trim(tokens[0]));
          setNbdomici(StringUtils.trim(getDomicili()).substring(getTpdomici().length()));
        }
      }
      else {
        setNbdomici(StringUtils.trim(getDomicili()));
      }
    }
  }

  public void loadNombreAppelidos() {

    if ((StringUtils.isBlank(getNbclient()) || StringUtils.isBlank(getNbclient1()) || StringUtils
        .isBlank(getNbclient2())) && StringUtils.isNotBlank(getNomapell())) {
      boolean nomappelContainsSeparador = getNomapell().contains("*");

      String[] nombres = new String[] { getNomapell() };
      if (nomappelContainsSeparador) {
        nombres = getNomapell().split("\\*");
      }

      if (TipoPersona.FISICA.getTipoPersona().equals(getTipoPersona())) {
        // Para persona fisica nacional es obligatorio informar el nombre y
        // dos apellidos
        if (StringUtils.isEmpty(getNbclient()) || StringUtils.isEmpty(getNbclient1())
            || StringUtils.isEmpty(getNbclient2())) {
          if (nombres.length > 0) {
            setNbclient(StringUtils.trim(nombres[0]));
          }
          if (nombres.length > 1) {
            setNbclient1(StringUtils.trim(nombres[1]));
          }
          if (nombres.length > 2) {
            setNbclient2(StringUtils.trim(nombres[2]));
          }
        }
      }
      else if (TipoPersona.JURIDICA.getTipoPersona().equals(getTipoPersona())) {
        // Para persona fisica no es obligatorio informar el nombre y un
        // apellido

        setNbclient("");
        setNbclient1(StringUtils.trim(getNomapell()));
        setNbclient2("");
      }

    }

  }

  public Tmct0AfiError toTmct0afierror(String idMensaje) {
    Tmct0AfiError afiErro = toTmct0afierror();
    afiErro.setIdmensaje(idMensaje);
    return afiErro;
  }

  public Tmct0AfiError toTmct0afierror() {
    Tmct0AfiError tmct0afierror = new Tmct0AfiError();

    try {
      tmct0afierror.setId(null);
      tmct0afierror.setIdmensaje(left(getIdMensajeConcatenado(), 30));
      tmct0afierror.setNudnicif(getNifbic());

      boolean nomappelContainsSeparador = false;
      if (getNomapell() != null) {
        nomappelContainsSeparador = getNomapell().contains("\\*");
      }

      String[] nombres = new String[] { getNomapell() };
      if (nomappelContainsSeparador) {
        nombres = getNomapell().split("\\*");
      }
      String nbclient = getNbclient();
      String nbclien1 = getNbclient1();
      String nbclien2 = getNbclient2();
      if (StringUtils.isBlank(nbclient) && nombres.length == 3) {
        nbclient = nombres[0];
      }
      if (StringUtils.isBlank(nbclien1) && nombres.length == 3) {
        nbclient = nombres[1];
      }
      else if (StringUtils.isBlank(nbclien1)) {
        nbclien1 = nombres[0];
      }
      if (StringUtils.isBlank(nbclien2) && nombres.length == 3) {
        nbclient = nombres[2];
      }

      tmct0afierror.setNbclient(left(nbclient, 30));
      tmct0afierror.setNbclient1(left(nbclien1, 50));
      tmct0afierror.setNbclient2(left(nbclien2, 30));
      tmct0afierror.setNureford(getNumOrden());
      // Aqui hay una calle y numero en afi
      if (getDomicili() != null && getDomicili().trim().length() == 2) {
        tmct0afierror.setTpdomici(getDomicili().substring(0, 2));
      }
      else if (getDomicili() != null && getDomicili().length() > 3) {
        tmct0afierror.setTpdomici(getDomicili().substring(0, 2));
        tmct0afierror.setNbdomici(getDomicili().substring(3));
      }
      else {
        tmct0afierror.setTpdomici(getTpdomici());
        tmct0afierror.setNbdomici(getNbdomici());
        tmct0afierror.setNudomici(getNudomici());
      }
      if (getDistrito() != null) {
        tmct0afierror.setCdpostal(getDistrito().toString());
      }
      if (getPoblacion() != null) {
        tmct0afierror.setNbciudad(getPoblacion());
      }

      if (getProvincia() != null) {
        tmct0afierror.setNbprovin(getProvincia());
      }

      if (getNacionalidad() != null) {
        tmct0afierror.setCdnactit(getNacionalidad().toString());
      }
      if (getTiptitu() != null) {
        tmct0afierror.setTptiprep(getTiptitu().toString().charAt(0));
      }
      tmct0afierror.setCddepais(getPaisResidencia());

      if (getTipoDocumento() != null) {
        tmct0afierror.setTpidenti(getTipoDocumento().toString().charAt(0));
      }
      if (getTipoPersona() != null) {
        tmct0afierror.setTpsocied(getTipoPersona().toString().charAt(0));
      }
      if (getIndNac() != null) {
        tmct0afierror.setTpnactit(getIndNac().toString().charAt(0));
      }
      tmct0afierror.setCcv(getCcv());
      tmct0afierror.setErrors(getErrorSet());
      tmct0afierror.setParticip(getParticip());
      tmct0afierror.setRftitaud(getNifbic());
      tmct0afierror.setTmct0alo(getTmct0alo());
      tmct0afierror.setNucnfliq(getNucnfliq());
    }
    catch (Exception e) {
      LOG.error(e.getMessage(), e);
    }
    return tmct0afierror;
  }

  public Tmct0AfiErrorData toTmct0afierrorData() {
    Tmct0AfiErrorData tmct0afierrorData = new Tmct0AfiErrorData();

    try {
      Tmct0AfiErrorData.entityToData(this, tmct0afierrorData);
    }
    catch (Exception e) {
      LOG.error(e.getMessage(), e);
    }
    return tmct0afierrorData;
  }

  public void updateTmct0afierror(Tmct0AfiError tmct0afierror) {

    if (getNifbic() != null) {
      tmct0afierror.setNudnicif(getNifbic());
    }
    if (getNbclient() != null) {
      tmct0afierror.setNbclient(getNbclient());
    }
    if (getNbclient1() != null) {
      tmct0afierror.setNbclient1(getNbclient1());
    }
    if (getNbclient2() != null) {
      tmct0afierror.setNbclient2(getNbclient2());
    }
    if (getTpdomici() != null) {
      tmct0afierror.setTpdomici(getTpdomici());
    }
    if (getNbdomici() != null) {
      tmct0afierror.setNbdomici(getNbdomici());
    }
    if (getNudomici() != null) {
      tmct0afierror.setNudomici(getNudomici());
    }

    if (getDistrito() != null) {
      tmct0afierror.setCdpostal(getDistrito());
    }
    if (getPoblacion() != null) {
      tmct0afierror.setNbciudad(getPoblacion());
    }
    if (getProvincia() != null) {
      tmct0afierror.setNbprovin(getProvincia());
    }
    if (getNacionalidad() != null) {
      tmct0afierror.setCdnactit(getNacionalidad().toString());
    }
    if (getTiptitu() != null) {
      tmct0afierror.setTptiprep(getTiptitu().toString().charAt(0));
    }
    if (getPaisResidencia() != null) {
      tmct0afierror.setCddepais(getPaisResidencia());
    }
    if (getTipoDocumento() != null) {
      tmct0afierror.setTpidenti(getTipoDocumento().toString().charAt(0));
    }
    if (getTipoPersona() != null) {
      tmct0afierror.setTpsocied(getTipoPersona().toString().charAt(0));
    }
    if (getIndNac() != null) {
      tmct0afierror.setTpnactit(getIndNac().toString().charAt(0));
    }
    if (getCcv() != null) {
      tmct0afierror.setCcv(getCcv());
    }

    if (getParticip() != null) {
      tmct0afierror.setParticip(getParticip());
    }
    if (getNumOrden() != null) {
      tmct0afierror.setNureford(getNumOrden());
    }

  }

  public void updateTmct0afi(Tmct0afi tmct0afi) {

    if (getNbclient() != null) {
      tmct0afi.setNbclient(getNbclient());
    }
    if (getNbclient1() != null) {
      tmct0afi.setNbclient1(getNbclient1());
    }
    if (getNbclient2() != null) {
      tmct0afi.setNbclient2(getNbclient2());
    }

    if (getTpdomici() != null) {
      tmct0afi.setTpdomici(getTpdomici());
    }
    if (getNbdomici() != null) {
      tmct0afi.setNbdomici(getNbdomici());
    }
    if (getNudomici() != null) {
      tmct0afi.setNudomici(getNudomici());
    }

    if (getDistrito() != null) {
      tmct0afi.setCdpostal(getDistrito().toString());
    }
    if (getPoblacion() != null) {
      tmct0afi.setNbciudad(getPoblacion());
    }
    if (getProvincia() != null) {
      tmct0afi.setNbprovin(getProvincia());
    }
    if (getNacionalidad() != null) {
      tmct0afi.setCdnactit(getNacionalidad().toString());
    }
    if (getTiptitu() != null) {
      tmct0afi.setTptiprep(getTiptitu().toString().charAt(0));
    }
    if (getPaisResidencia() != null) {
      tmct0afi.setCddepais(getPaisResidencia());
    }
    if (getNifbic() != null) {
      tmct0afi.setCdholder(getNifbic());

      if (TipoDocumento.BIC.getTipoDocumento().equals(getTipoDocumento())) {
        tmct0afi.setNudnicif("");
        tmct0afi.setCdddebic(getNifbic());
      }
      else {
        tmct0afi.setNudnicif(getNifbic());
        tmct0afi.setCdddebic("");
      }
    }

    if (getTipoDocumento() != null) {
      tmct0afi.setTpidenti(getTipoDocumento().toString().charAt(0));
    }
    if (getTipoPersona() != null) {
      tmct0afi.setTpsocied(getTipoPersona().toString().charAt(0));
    }
    if (getIndNac() != null) {
      tmct0afi.setTpnactit(getIndNac().toString().charAt(0));
    }
    if (getCcv() != null) {
      tmct0afi.setCcv(getCcv());
    }
    if (getParticip() != null) {
      tmct0afi.setParticip(getParticip());
    }

  }

  public void sustituirCodigoPostalExtranjero() {
    if (StringUtils.isNotEmpty(getPaisResidencia()) && !getPaisResidencia().equals("011")
        && !getPaisResidencia().equals("042")) {
      setDistrito("99" + getPaisResidencia());
    }
  }

  public Tmct0alo getTmct0alo() {
    return tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  public Short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  

  public String getIdMensajeConcatenado() {
    try {
      return new StringBuilder().append(getTipoRegistro()).append(getCodeMpr()).append(getNumOrden())
          .append(getIdSenc()).toString();
    }
    catch (Exception e) {
      return getTipoRegistro();
    }
  }

  /**
   * Updates the fields when there are two lines for one register
   * 
   * @param bean
   */
  public void updateData(PartenonRecordBean bean) {
    LOG.trace("[PartenonRecordBean :: updateData] Init (Es un registro partido en varias lineas)");

    if (!this.getNomapell().equals(bean.getNomapell())) {
      this.setNomapell(this.getNomapell() + bean.getNomapell());
      String nomApell = this.getNomapell().trim();
      if (nomApell.indexOf("*") == -1) {
        int indicePrimerSubstring = 50;
        if (nomApell.length() < 50) {
          indicePrimerSubstring = nomApell.length();
        }
        this.setNbclient(" ");
        this.setNbclient1(nomApell.substring(0, indicePrimerSubstring));
        if (nomApell.length() > 50) {
          int indiceSegundoSubstring = 30;
          if (nomApell.length() < 80) {
            indiceSegundoSubstring = nomApell.length() - 50;
          }
          this.setNbclient2(nomApell.substring(50, 50 + indiceSegundoSubstring));
        }
        else {
          this.setNbclient2(" ");
        }
      }
      else {
        String[] names = nomApell.split("\\*");
        this.setNomapell(nomApell);
        if (names.length >= 1) {
          this.setNbclient(names[0].trim());
        }
        if (names.length >= 2) {
          this.setNbclient1(names[1].trim());
        }
        if (names.length >= 3) {
          this.setNbclient2(names[2].trim());
        }

      }

    }

    if (this.getDomicili() != null) {
      if (!this.getDomicili().equals(bean.getDomicili())) {
        this.setDomicili((this.getDomicili() + bean.getDomicili()).trim());
      }
      if (this.getDomicili().length() > 40) {
        this.setDomicili(this.getDomicili().substring(0, 40));
      }
    }

    if (this.getPoblacion() != null) {
      if (!this.getPoblacion().equals(bean.getPoblacion())) {
        this.setPoblacion((this.getPoblacion() + bean.getPoblacion()).trim());
      }
      if (this.getPoblacion().length() > 40) {
        this.setDomicili(this.getDomicili().substring(0, 40));
      }
    }

    if (this.getProvincia() != null) {
      if (!this.getProvincia().equals(bean.getProvincia())) {
        this.setProvincia((this.getProvincia() + bean.getProvincia()).trim());
      }
      if (this.getProvincia().length() > 25) {
        this.setProvincia(this.getProvincia().substring(0, 25));
      }
    }

    // Cuidando de las longitudes
    if (this.getNbclient() != null) {
      if (this.getNbclient().length() > 30) {
        this.setNbclient(this.getNbclient().substring(0, 30));
      }
    }

    if (this.getNbclient1() != null) {
      if (this.getNbclient1().length() > 50) {
        this.setNbclient1(this.getNbclient1().substring(0, 50));
      }
    }

    if (this.getNbclient2() != null) {
      if (this.getNbclient2().length() > 30) {
        this.setNbclient2(this.getNbclient2().substring(0, 30));
      }
    }
    LOG.trace("[PartenonRecordBean :: updateData] Fin (Es un registro partido en varias lineas)");

  }

  /**
   * Separa el nomapell en nbclient, nbclient1 y nbclient2
   */
  public void separarNombreTitular() {
    try {
      if (getNomapell().indexOf("*") == -1) {
        int indicePrimerSubstring = 50;
        if (getNomapell().length() < 50) {
          indicePrimerSubstring = getNomapell().length();
        }
        setNbclient(" ");
        setNbclient1(getNomapell().substring(0, indicePrimerSubstring));
        if (getNomapell().length() > 50) {
          int indiceSegundoSubstring = 30;
          if (getNomapell().length() < 80) {
            indiceSegundoSubstring = getNomapell().length() - 50;
          }
          setNbclient2(getNomapell().substring(50, 50 + indiceSegundoSubstring));
        }
        else {
          setNbclient2(" ");
        }
      }
      else {
        String[] names = getNomapell().split("\\*");
        setNbclient(names[0].trim());
        setNbclient1(names[1].trim());
        if (names.length > 2) {
          setNbclient2(names[2].trim());
        }
        else {
          setNbclient2("");
        }
      }
    }
    catch (Exception e) {
      setNomapell(getNomapell());
    }

  }

  public Date getFechaContratacion() {
    return fechaContratacion;
  }

  public void setFechaContratacion(Date fechaContratacion) {
    this.fechaContratacion = fechaContratacion;
  }

  public Tmct0AfiError getAfiError() {
    return afiError;
  }

  public void setAfiError(Tmct0AfiError afiError) {
    this.afiError = afiError;
  }

  public List<Tmct0afiId> getAfiIdsModificados() {
    return afiIdsModificados;
  }

  public void setAfiIdsModificados(List<Tmct0afiId> afiIdsModificados) {
    this.afiIdsModificados = afiIdsModificados;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public String toString() {
    final ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
    tsb.append(this.getTipoRegistro());
    tsb.append(this.getCodeMpr());
    tsb.append(this.getNumOrden());
    tsb.append(this.getIdSenc());
    return tsb.toString();
  }

  @Override
  public int hashCode() {
    final HashCodeBuilder hcb = new HashCodeBuilder();
    hcb.append(getTipoRegistro());
    hcb.append(codeMpr);
    hcb.append(numOrden);
    hcb.append(idSenc);
    return hcb.toHashCode();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    final PartenonRecordBean partenonBean = (PartenonRecordBean) obj;
    final EqualsBuilder eqb = new EqualsBuilder();
    eqb.append(this.getTipoRegistro(), partenonBean.getTipoRegistro());
    eqb.append(this.getCodeMpr(), partenonBean.getCodeMpr());
    eqb.append(this.getNumOrden(), partenonBean.getNumOrden());
    eqb.append(this.getIdSenc(), partenonBean.getIdSenc());
    return eqb.isEquals();
  }

}
