package sibbac.business.wrappers.database.data;

import sibbac.business.wrappers.database.model.Tmct0Compensador;

public class CompensadorData {
    private long idCompensador;
    private String nombre;
    private String descripcion;
    
    public CompensadorData(Tmct0Compensador comp){
	this.idCompensador = comp.getIdCompensador();
	this.nombre = comp.getNbNombre();
	this.descripcion = comp.getNbDescripcion();
    }

    public long getIdCompensador() {
        return idCompensador;
    }

    public void setIdCompensador(long idCompensador) {
        this.idCompensador = idCompensador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
