package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Monedas }. Entity: "Monedas".
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = WRAPPERS.MONEDA )
@Entity
@XmlRootElement
@Audited
public class Monedas extends ATableAudited< Monedas > implements java.io.Serializable {

	@Column( name = "NBCODIGO", nullable = false, length = 3 )
	@XmlAttribute
	private String				codigo;
	/**
	 *
	 */
	private static final long	serialVersionUID	= 1075968899412373267L;
	@Column( name = "NBDESCRIPCION", nullable = false, length = 50 )
	@XmlAttribute
	private String				descripcion;
	public static final String	COD_MONEDA_EUR		= "EUR";

	public Monedas() {
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	/**
	 * Getter for descripcion
	 *
	 * @return descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Setter for descripcion
	 *
	 * @param descripcion
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
