package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0paisesId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Column( name = "CDISOALF2", length = 2 )
	private String				cdisoalf2;
	
	@Column( name = "CDISOALF3", length = 3)
	private String				cdisoalf3;
	
	@Column( name = "CDISONUM", length = 6 )
	private String				cdisonum;
	
	@Column( name = "CDESTADO", length = 1 )
	private String				cdestado;
	
	public Tmct0paisesId() {
	}

	public Tmct0paisesId(String cdisoalf2, String cdisoalf3, String cdisonum,
			String cdestado) {
		super();
		this.cdisoalf2 = cdisoalf2;
		this.cdisoalf3 = cdisoalf3;
		this.cdisonum = cdisonum;
		this.cdestado = cdestado;
	}

	public String getCdisoalf2() {
		return cdisoalf2;
	}

	public void setCdisoalf2(String cdisoalf2) {
		this.cdisoalf2 = cdisoalf2;
	}

	public String getCdisoalf3() {
		return cdisoalf3;
	}

	public void setCdisoalf3(String cdisoalf3) {
		this.cdisoalf3 = cdisoalf3;
	}

	public String getCdisonum() {
		return cdisonum;
	}

	public void setCdisonum(String cdisonum) {
		this.cdisonum = cdisonum;
	}

	public String getCdestado() {
		return cdestado;
	}

	public void setCdestado(String cdestado) {
		this.cdestado = cdestado;
	}
	
}
