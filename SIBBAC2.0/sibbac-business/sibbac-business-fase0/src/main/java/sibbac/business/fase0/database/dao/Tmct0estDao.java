package sibbac.business.fase0.database.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0est;
import sibbac.business.fase0.database.model.Tmct0estId;

@Repository
public interface Tmct0estDao extends JpaRepository<Tmct0est, Tmct0estId> {

  @Query("select a from Tmct0est a where a.id.cdaliass = :cdaliass AND a.fhfinal >= :fecha AND a.fhinicio <= :fecha")
  public Tmct0est findByCdaliassFechaActual(@Param("cdaliass") String cdaliass, @Param("fecha") BigDecimal fecha);

}
