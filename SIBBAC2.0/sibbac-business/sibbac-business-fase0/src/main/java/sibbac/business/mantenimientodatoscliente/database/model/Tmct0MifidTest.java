package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TMCT0_MIFIDTEST")
public class Tmct0MifidTest implements Serializable {
	
  private static final long serialVersionUID = 5323808253289187655L;

  @EmbeddedId
	private Tmct0MifidTestId id;

	@Column(name = "FECINI", nullable = true, length = 4)
	private Date fecini;

	@Temporal( TemporalType.TIME )
	@Column(name = "HORAACTU", nullable = true, length = 3)
	private Date horaactu;

	@Column(name = "ESTCUEST", nullable = true, length = 2)
	private String estcuest;

	@Column(name = "IDCUEST", nullable = true, length = 5)
	private String idcuest;

	@Column(name = "TIPOPER2", nullable = true, length = 1)
	private String tipoper2;

	@Column(name = "CODPERS2", nullable = true, length = 8)
  private long codpers2;

	@Column(name = "PUNTUAC", nullable = true, length = 8)
	private long puntuac;

	@Column(name = "PORCENTA", nullable = true, precision = 7, scale =5)
	private BigDecimal porcenta;

	@Column(name = "CDPERFIL", nullable = true, length = 2)
	private String cdperfil;

	@Column(name = "CDCENTRO", nullable = true, length = 4)
	private String cdcentro;

	@Column(name = "FECALTA", nullable = true, length = 4)
	private Date fecalta;

	@Column(name = "USUALTA", nullable = true, length = 8)
	private String usualta;

	@Column(name = "FECMODI", nullable = true, length = 4)
	private Date fecmodi;

	@Column(name = "USUMODI", nullable = true, length = 8)
	private String usumodi;

	public Tmct0MifidTest() {
	}

	public Tmct0MifidTest(Tmct0MifidTestId id, Date fecini, Date horaactu,
			String estcuest, String idcuest, String tipoper2, long codpers2,
			long puntuac, BigDecimal porcenta, String cdperfil,
			String cdcentro, Date fecfin, Date fecalta, String usualta,
			Date fecmodi, String usumodi) {
		super();
		this.id = id;
		this.fecini = fecini;
		this.horaactu = horaactu;
		this.estcuest = estcuest;
		this.idcuest = idcuest;
		this.tipoper2 = tipoper2;
		this.codpers2 = codpers2;
		this.puntuac = puntuac;
		this.porcenta = porcenta;
		this.cdperfil = cdperfil;
		this.cdcentro = cdcentro;
		this.fecalta = fecalta;
		this.usualta = usualta;
		this.fecmodi = fecmodi;
		this.usumodi = usumodi;
	}
	
  public Tmct0MifidTestId getId() {
		return id;
	}

	public void setId(Tmct0MifidTestId id) {
		this.id = id;
	}

	public Date getFecini() {
		return fecini;
	}

	public void setFecini(Date fecini) {
		this.fecini = fecini;
	}

	public Date getHoraactu() {
		return horaactu;
	}

	public void setHoraactu(Date horaactu) {
		this.horaactu = horaactu;
	}

	public String getEstcuest() {
		return estcuest;
	}

	public void setEstcuest(String estcuest) {
		this.estcuest = estcuest;
	}

	public String getIdcuest() {
		return idcuest;
	}

	public void setIdcuest(String idcuest) {
		this.idcuest = idcuest;
	}

	public String getTipoper2() {
		return tipoper2;
	}

	public void setTipoper2(String tipoper2) {
		this.tipoper2 = tipoper2;
	}

	public long getCodpers2() {
		return codpers2;
	}

	public void setCodpers2(long codpers2) {
		this.codpers2 = codpers2;
	}

	public long getPuntuac() {
		return puntuac;
	}

	public void setPuntuac(long puntuac) {
		this.puntuac = puntuac;
	}

	public BigDecimal getPorcenta() {
		return porcenta;
	}

	public void setPorcenta(BigDecimal porcenta) {
		this.porcenta = porcenta;
	}

	public String getCdperfil() {
		return cdperfil;
	}

	public void setCdperfil(String cdperfil) {
		this.cdperfil = cdperfil;
	}

	public String getCdcentro() {
		return cdcentro;
	}

	public void setCdcentro(String cdcentro) {
		this.cdcentro = cdcentro;
	}

	public Date getFecalta() {
		return fecalta;
	}

	public void setFecalta(Date fecalta) {
		this.fecalta = fecalta;
	}

	public String getUsualta() {
		return usualta;
	}

	public void setUsualta(String usualta) {
		this.usualta = usualta;
	}

	public Date getFecmodi() {
		return fecmodi;
	}

	public void setFecmodi(Date fecmodi) {
		this.fecmodi = fecmodi;
	}

	public String getUsumodi() {
		return usumodi;
	}

	public void setUsumodi(String usumodi) {
		this.usumodi = usumodi;
	}
	
  /**
   * @return El código de persona con el formato esperado por el código de base de datos de Partenón o nulo en caso
   * de que la clave esté vacía.
   */
  @Transient
  public String getCDBDP() {
    if(id == null) {
      return null;
    }
    return id.getCDBDP();
  }

  @Override
  public String toString() {
    return "Tmct0MifidTest [id=" + id + ", fecini=" + fecini + ", horaactu=" + horaactu + ", estcuest=" + estcuest
        + ", idcuest=" + idcuest + ", tipoper2=" + tipoper2 + ", codpers2=" + codpers2 + ", puntuac=" + puntuac
        + ", porcenta=" + porcenta + ", cdperfil=" + cdperfil + ", cdcentro=" + cdcentro + ", fecalta=" + fecalta
        + ", usualta=" + usualta + ", fecmodi=" + fecmodi + ", usumodi=" + usumodi + "]";
  }
}
