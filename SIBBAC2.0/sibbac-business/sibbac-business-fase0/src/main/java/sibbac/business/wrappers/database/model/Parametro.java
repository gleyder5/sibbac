package sibbac.business.wrappers.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.Parametro }. Entity:
 * "Parametro".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = DBConstants.WRAPPERS.PARAMETRO )
@Entity
@XmlRootElement
@Audited
public class Parametro extends ATableAudited< Parametro > implements java.io.Serializable {

	private static final long	serialVersionUID	= 6220270947704636336L;

	@Column( name = "NBSOCIEDAD", length = 200 )
	@XmlAttribute
	private String				sociedad;

	@Column( name = "NBREGISTROMERCANTIL", length = 200 )
	@XmlAttribute
	private String				registroMercantil;

	@Column( name = "NBTEXTO1", length = 200 )
	@XmlAttribute
	private String				texto1;

	@Column( name = "NBTEXTO2", length = 200 )
	@XmlAttribute
	private String				texto2;

	@Column( name = "NBENTIDADCUENTA", length = 4 )
	@XmlAttribute
	private String				entidadCuenta;

	@Column( name = "NBSUCURSALCUENTA", length = 4 )
	@XmlAttribute
	private String				sucursalCuenta;

	@Column( name = "NBDIGITOCONTROL", length = 2 )
	@XmlAttribute
	private String				digitoControl;

	@Column( name = "NBCUENTA", length = 10 )
	@XmlAttribute
	private String				cuenta;

	@Column( name = "NBBANCO", length = 100 )
	@XmlAttribute
	private String				banco;

	@Column( name = "NBBIC", length = 8 )
	@XmlAttribute
	private String				BIC;

	@Column( name = "IBAN", length = 4 )
	@XmlAttribute
	private String				IBAN;

	public Parametro() {
		super();
	}

	/**
	 * @return the sociedad
	 */
	public String getSociedad() {
		return sociedad;
	}

	/**
	 * @param sociedad
	 *            the sociedad to set
	 */
	public void setSociedad( String sociedad ) {
		this.sociedad = sociedad;
	}

	/**
	 * @return the registroMercantil
	 */
	public String getRegistroMercantil() {
		return registroMercantil;
	}

	/**
	 * @param registroMercantil
	 *            the registroMercantil to set
	 */
	public void setRegistroMercantil( String registroMercantil ) {
		this.registroMercantil = registroMercantil;
	}

	/**
	 * @return the texto1
	 */
	public String getTexto1() {
		return texto1;
	}

	/**
	 * @param texto1
	 *            the texto1 to set
	 */
	public void setTexto1( String texto1 ) {
		this.texto1 = texto1;
	}

	/**
	 * @return the texto2
	 */
	public String getTexto2() {
		return texto2;
	}

	/**
	 * @param texto2
	 *            the texto2 to set
	 */
	public void setTexto2( String texto2 ) {
		this.texto2 = texto2;
	}

	/**
	 * @return the entidadCuenta
	 */
	public String getEntidadCuenta() {
		return entidadCuenta;
	}

	/**
	 * @param entidadCuenta
	 *            the entidadCuenta to set
	 */
	public void setEntidadCuenta( String entidadCuenta ) {
		this.entidadCuenta = entidadCuenta;
	}

	/**
	 * @return the sucursalCuenta
	 */
	public String getSucursalCuenta() {
		return sucursalCuenta;
	}

	/**
	 * @param sucursalCuenta
	 *            the sucursalCuenta to set
	 */
	public void setSucursalCuenta( String sucursalCuenta ) {
		this.sucursalCuenta = sucursalCuenta;
	}

	/**
	 * @return the digitoControl
	 */
	public String getDigitoControl() {
		return digitoControl;
	}

	/**
	 * @param digitoControl
	 *            the digitoControl to set
	 */
	public void setDigitoControl( String digitoControl ) {
		this.digitoControl = digitoControl;
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta
	 *            the cuenta to set
	 */
	public void setCuenta( String cuenta ) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * @param banco
	 *            the banco to set
	 */
	public void setBanco( String banco ) {
		this.banco = banco;
	}

	/**
	 * @return the bIC
	 */
	public String getBIC() {
		return BIC;
	}

	/**
	 * @param BIC
	 *            the bIC to set
	 */
	public void setBIC( String BIC ) {
		this.BIC = BIC;
	}

	/**
	 * @return the iBAN
	 */
	public String getIBAN() {
		return IBAN;
	}

	/**
	 * @param iBAN the iBAN to set
	 */
	public void setIBAN( String iBAN ) {
		IBAN = iBAN;
	}

	public String CuentaBancariaToString() {
		return this.banco + " " + this.sucursalCuenta + " " + this.digitoControl + " " + this.cuenta;
	}

	public String IbanToString() {
		return this.IBAN + " " + this.banco + " " + this.sucursalCuenta + " " + this.digitoControl + " " + this.cuenta;
	}

}
