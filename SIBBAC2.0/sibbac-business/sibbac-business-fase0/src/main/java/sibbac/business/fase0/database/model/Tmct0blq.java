package sibbac.business.fase0.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0BLQ. BROKER INSTRUCTIONS Documentación de la tablaTMCT0BLQ <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * 
 **/
@Entity
@Table(name = "TMCT0BLQ")
public class Tmct0blq implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -7122156088495067108L;
  //@formatter:off
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdbroker", column = @Column(name = "CDBROKER", nullable = false, length = 20)), 
                        @AttributeOverride(name = "centro", column = @Column(name = "CENTRO", nullable = false, length = 30)),
                        @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD", nullable = false, length = 20)),
                        @AttributeOverride(name = "tpsettle", column = @Column(name = "TPSETTLE", nullable = false, length = 25)),
                        @AttributeOverride(name = "numsec", column = @Column(name = "NUMSEC", nullable = false, length = 20, scale = 0))
  })
  private Tmct0blqId id;
  //@formatter:on
  /**
   * Table Field CDBICID. BIC Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBICID", length = 11)
  protected java.lang.String cdbicid;
  /**
   * Table Field NAMEID. NAME Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "NAMEID", length = 146)
  protected java.lang.String nameid;
  /**
   * Table Field BLCUSBIC. LOCAL CUSTODIAN BIC Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BLCUSBIC", length = 11)
  protected java.lang.String blcusbic;
  /**
   * Table Field BCDLOCCUS. LOCAL CUSTODIAN NAME Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDLOCCUS", length = 146)
  protected java.lang.String bcdloccus;
  /**
   * Table Field BACLOCCUS. LOCAL CUSTODIAN ACCOUNT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BACLOCCUS", length = 34)
  protected java.lang.String bacloccus;
  /**
   * Table Field BIDLOCCUS. LOCAL CUSTODIAN ID Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BIDLOCCUS", length = 43)
  protected java.lang.String bidloccus;
  /**
   * Table Field BGCUSBIC. GLOBAL CUSTODIAN BIC Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BGCUSBIC", length = 11)
  protected java.lang.String bgcusbic;
  /**
   * Table Field BCDGLOCUS. GLOBAL CUSTODIAN NAME Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDGLOCUS", length = 146)
  protected java.lang.String bcdglocus;
  /**
   * Table Field BACGLOCUS. GLOBAL CUSTODIAN ACCOUNT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BACGLOCUS", length = 34)
  protected java.lang.String bacglocus;
  /**
   * Table Field BIDGLOCUS. GLOBAL CUSTODIAN ID Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BIDGLOCUS", length = 43)
  protected java.lang.String bidglocus;
  /**
   * Table Field BCDBENBIC. BENEFICIARIO BIC Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDBENBIC", length = 11)
  protected java.lang.String bcdbenbic;
  /**
   * Table Field BCDBENEFI. BENEFICIARIO NAME Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BCDBENEFI", length = 146)
  protected java.lang.String bcdbenefi;
  /**
   * Table Field BACBENEFI. BENEFICIARIO ACCOUNT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BACBENEFI", length = 34)
  protected java.lang.String bacbenefi;
  /**
   * Table Field BIDBENEFI. BENEFICIARIO ID Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "BIDBENEFI", length = 43)
  protected java.lang.String bidbenefi;
  /**
   * Table Field CDBICMAR. PLACE OF SETTLEMENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDBICMAR", length = 43)
  protected java.lang.String cdbicmar;
  /**
   * Table Field COCENLIQ. CENTRAL COUNTERPARTY Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "COCENLIQ", length = 20)
  protected java.lang.String cocenliq;
  /**
   * Table Field CDUSUAUD. AUDIT USER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. AUDIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;
  /**
   * Table Field FHINICIO. INIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "FHINICIO", length = 8, scale = 0)
  protected java.math.BigDecimal fhinicio;
  /**
   * Table Field FHFINAL. FINAL DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "FHFINAL", length = 8, scale = 0)
  protected java.math.BigDecimal fhfinal;
  public Tmct0blqId getId() {
    return id;
  }
  public java.lang.String getCdbicid() {
    return cdbicid;
  }
  public java.lang.String getNameid() {
    return nameid;
  }
  public java.lang.String getBlcusbic() {
    return blcusbic;
  }
  public java.lang.String getBcdloccus() {
    return bcdloccus;
  }
  public java.lang.String getBacloccus() {
    return bacloccus;
  }
  public java.lang.String getBidloccus() {
    return bidloccus;
  }
  public java.lang.String getBgcusbic() {
    return bgcusbic;
  }
  public java.lang.String getBcdglocus() {
    return bcdglocus;
  }
  public java.lang.String getBacglocus() {
    return bacglocus;
  }
  public java.lang.String getBidglocus() {
    return bidglocus;
  }
  public java.lang.String getBcdbenbic() {
    return bcdbenbic;
  }
  public java.lang.String getBcdbenefi() {
    return bcdbenefi;
  }
  public java.lang.String getBacbenefi() {
    return bacbenefi;
  }
  public java.lang.String getBidbenefi() {
    return bidbenefi;
  }
  public java.lang.String getCdbicmar() {
    return cdbicmar;
  }
  public java.lang.String getCocenliq() {
    return cocenliq;
  }
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }
  public Date getFhaudit() {
    return fhaudit;
  }
  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }
  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }
  public void setId(Tmct0blqId id) {
    this.id = id;
  }
  public void setCdbicid(java.lang.String cdbicid) {
    this.cdbicid = cdbicid;
  }
  public void setNameid(java.lang.String nameid) {
    this.nameid = nameid;
  }
  public void setBlcusbic(java.lang.String blcusbic) {
    this.blcusbic = blcusbic;
  }
  public void setBcdloccus(java.lang.String bcdloccus) {
    this.bcdloccus = bcdloccus;
  }
  public void setBacloccus(java.lang.String bacloccus) {
    this.bacloccus = bacloccus;
  }
  public void setBidloccus(java.lang.String bidloccus) {
    this.bidloccus = bidloccus;
  }
  public void setBgcusbic(java.lang.String bgcusbic) {
    this.bgcusbic = bgcusbic;
  }
  public void setBcdglocus(java.lang.String bcdglocus) {
    this.bcdglocus = bcdglocus;
  }
  public void setBacglocus(java.lang.String bacglocus) {
    this.bacglocus = bacglocus;
  }
  public void setBidglocus(java.lang.String bidglocus) {
    this.bidglocus = bidglocus;
  }
  public void setBcdbenbic(java.lang.String bcdbenbic) {
    this.bcdbenbic = bcdbenbic;
  }
  public void setBcdbenefi(java.lang.String bcdbenefi) {
    this.bcdbenefi = bcdbenefi;
  }
  public void setBacbenefi(java.lang.String bacbenefi) {
    this.bacbenefi = bacbenefi;
  }
  public void setBidbenefi(java.lang.String bidbenefi) {
    this.bidbenefi = bidbenefi;
  }
  public void setCdbicmar(java.lang.String cdbicmar) {
    this.cdbicmar = cdbicmar;
  }
  public void setCocenliq(java.lang.String cocenliq) {
    this.cocenliq = cocenliq;
  }
  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }
  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }
  public void setFhinicio(java.math.BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }
  public void setFhfinal(java.math.BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }
  @Override
  public String toString() {
    return "Tmct0blq [id=" + id + "]";
  }
  
  
  
  
}
