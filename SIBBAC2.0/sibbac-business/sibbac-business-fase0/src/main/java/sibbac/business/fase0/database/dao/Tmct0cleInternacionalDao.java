package sibbac.business.fase0.database.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.Tmct0cleDTO;
import sibbac.business.fase0.database.model.Tmct0cleIdInternacional;
import sibbac.business.fase0.database.model.Tmct0cleInternacional;

@Repository
public interface Tmct0cleInternacionalDao extends JpaRepository<Tmct0cleInternacional, Tmct0cleIdInternacional> {

  @Query("SELECT C FROM Tmct0cleInternacional C WHERE C.fhinicio <= :fecha AND C.fhfinal >= :fecha AND C.id.cdcleare = :cdcleare  ")
  List<Tmct0cleInternacional> findByFechaAndCdcleare(@Param("fecha") final Integer fecha,
      @Param("cdcleare") final String cdcleare);

  @Query("SELECT cle FROM Tmct0cleInternacional cle WHERE cle.id.cdcleare=:cdcleare AND cle.fhfinal=99991231")
  List<Tmct0cleInternacional> findActiveByCdcleare(@Param("cdcleare") final String cdcleare);

  @Query("select distinct new sibbac.business.fase0.database.dto.Tmct0cleDTO (  biccle, dscleare) from Tmct0cleInternacional where fhfinal >= :fhFinal and centro != 'MMEE'")
  Set<Tmct0cleDTO> findDistinctDscleareAndBiccleByFhfinal(@Param("fhFinal") final Integer fhFinal);

  @Query("select c from Tmct0cleInternacional c where c.id.cdcleare = :pcdcleare and c.id.centro = :pcentro and c.id.cdmercad = :pcdmercad"
      + " and c.id.cdaliass = :pcdaliass and c.id.cdsubcta = :pcdsubcta and c.id.tpsettle = :ptpsettle "
      + " and c.fhfinal >= :pfecha and c.fhinicio <= :pfecha "
      + " order by c.id.cdcleare asc, c.id.centro asc, c.id.cdmercad asc, c.id.tpsettle asc, c.id.numsec desc")
  List<Tmct0cleInternacional> findAllTmct0cleByCdcleareAndCentroAndCdmercadAndCdaliassAndCdsubctaAndTpsettleAndFecha(
      @Param("pcdcleare") final String cdcleare, @Param("pcentro") final String centro,
      @Param("pcdmercad") final String cdmercad, @Param("pcdaliass") final String cdaliass,
      @Param("pcdsubcta") final String cdsubcta, @Param("ptpsettle") final String tpsettle,
      @Param("pfecha") final Integer fecha);

}
