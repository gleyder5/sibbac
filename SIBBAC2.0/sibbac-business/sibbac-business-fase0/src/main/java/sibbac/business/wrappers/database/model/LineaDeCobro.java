package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.business.wrappers.database.model.LineaDeCobro } .
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */

@Table( name = DBConstants.WRAPPERS.LINEA_DE_COBRO )
@Entity
@XmlRootElement
@Audited
public class LineaDeCobro extends ATableAudited< LineaDeCobro > implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -4518556736282215435L;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_COBRO", referencedColumnName = "ID", nullable = false )
	@XmlAttribute
	private Cobros				cobro;

	@ManyToOne( fetch = FetchType.LAZY, optional = false, cascade = CascadeType.PERSIST )
	@JoinColumn( name = "ID_FACTURA", referencedColumnName = "ID", nullable = false )
	@XmlAttribute
	private Factura				factura;

	@Column( name = "IMAPLICADO", nullable = false, scale = 6, precision = 19 )
	@XmlAttribute
	private BigDecimal			importeAplicado;

	@Column( name = "IMIMPORTE", nullable = false, scale = 6, precision = 19 )
	@XmlAttribute
	private BigDecimal			importe;

	public LineaDeCobro() {
		importe = BigDecimal.ZERO;
	}

	/**
	 * @return the cobro
	 */
	public Cobros getCobro() {
		return cobro;
	}

	/**
	 * @param cobro
	 *            the cobro to set
	 */
	public void setCobro( Cobros cobro ) {
		this.cobro = cobro;
	}

	/**
	 * @return the factura
	 */
	public Factura getFactura() {
		return factura;
	}

	/**
	 * @param factura
	 *            the factura to set
	 */
	public void setFactura( Factura factura ) {
		this.factura = factura;
	}

	/**
	 * @return the importeAplicado
	 */
	public BigDecimal getImporteAplicado() {
		return importeAplicado;
	}

	/**
	 * @param importeAplicado
	 *            the importeAplicado to set
	 */
	public void setImporteAplicado( BigDecimal importeAplicado ) {
		this.importeAplicado = importeAplicado;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	@Override
	public String toString() {
		return "CobrosLin [cobro=" + cobro + ", facturas=" + factura + ", importeAplicado=" + importeAplicado + ", importeAplicado="
				+ importeAplicado + "]";
	}

}
