package sibbac.business.mantenimientodatoscliente.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0MifidCategoriaId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "IDEMPR", nullable = false, length = 4)
	private String idempr;

	@Column(name = "TIPOPERS", nullable = false, length = 1)
	private String tipopers;

	@Column(name = "CODPERS", nullable = false, length = 8)
	private long codpers;

	public Tmct0MifidCategoriaId() {
	}

	public Tmct0MifidCategoriaId(String idempr, String tipopers, long codpers) {
		super();
		this.idempr = idempr;
		this.tipopers = tipopers;
		this.codpers = codpers;
	}

	public String getIdempr() {
		return idempr;
	}

	public void setIdempr(String idempr) {
		this.idempr = idempr;
	}

	public String getTipopers() {
		return tipopers;
	}

	public void setTipopers(String tipopers) {
		this.tipopers = tipopers;
	}

	public long getCodpers() {
		return codpers;
	}

	public void setCodpers(long codpers) {
		this.codpers = codpers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (codpers ^ (codpers >>> 32));
		result = prime * result + ((idempr == null) ? 0 : idempr.hashCode());
		result = prime * result
				+ ((tipopers == null) ? 0 : tipopers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tmct0MifidCategoriaId other = (Tmct0MifidCategoriaId) obj;
		if (codpers != other.codpers)
			return false;
		if (idempr == null) {
			if (other.idempr != null)
				return false;
		} else if (!idempr.equals(other.idempr))
			return false;
		if (tipopers == null) {
			if (other.tipopers != null)
				return false;
		} else if (!tipopers.equals(other.tipopers))
			return false;
		return true;
	}

}
