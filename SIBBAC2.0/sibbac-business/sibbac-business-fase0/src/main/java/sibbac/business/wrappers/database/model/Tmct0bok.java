package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import sibbac.business.fase0.database.model.Tmct0estado;

@Entity
@Table(name = "TMCT0BOK")
public class Tmct0bok implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 8221757978195019510L;
  private Tmct0bokId id;
  private Tmct0estado tmct0estadoByCdestadocont;
  private Tmct0estado tmct0estadoByCdestadoasig;
  private Tmct0ord tmct0ord;
  private Tmct0sta tmct0sta;
  private String dsobserv = " ";
  private BigDecimal imtpcamb = new BigDecimal(0.0);
  private String cdmoniso = " ";
  private BigDecimal nutitord = new BigDecimal(0.0);
  private BigDecimal nutiteje = new BigDecimal(0.0);
  private String cdisin = " ";
  private String nbvalors = " ";
  private Character cdtpoper = ' ';
  private BigDecimal imlimite = new BigDecimal(0.0);
  private BigDecimal imcbmerc = new BigDecimal(0.0);
  private Date feconsol = new Date();
  private Date fealta = new Date();
  private Date hoalta = new Date();
  private Date fecontra = new Date();
  private Date feejecuc = new Date();
  private Date hoejecuc = new Date();
  private Date fevalide = new Date();
  private Date fevalor = new Date();
  private Date fevalorr = new Date();
  private Date fetrade = new Date();
  private BigDecimal imntbreu = new BigDecimal(0.0);
  private BigDecimal imnetobr = new BigDecimal(0.0);
  private BigDecimal imntcleu = new BigDecimal(0.0);
  private BigDecimal imnetocl = new BigDecimal(0.0);
  private BigDecimal imcbntcl = new BigDecimal(0.0);
  private BigDecimal podevolu = new BigDecimal(0.0);
  private BigDecimal pofpacli = new BigDecimal(0.0);
  private BigDecimal imfpacdv = new BigDecimal(0.0);
  private BigDecimal imbensvb = new BigDecimal(0.0);
  private BigDecimal imdevcdv = new BigDecimal(0.0);
  private BigDecimal imdevcli = new BigDecimal(0.0);
  private BigDecimal imfibrdv = new BigDecimal(0.0);
  private BigDecimal imfibreu = new BigDecimal(0.0);
  private BigDecimal imsvb = new BigDecimal(0.0);
  private BigDecimal imsvbeu = new BigDecimal(0.0);
  private Character cdindgas = ' ';
  private Character cdindimp = ' ';
  private Character cdreside = ' ';
  private BigDecimal imbrmerc = new BigDecimal(0.0);
  private BigDecimal imbrmreu = new BigDecimal(0.0);
  private BigDecimal imgatdvs = new BigDecimal(0.0);
  private BigDecimal imgastos = new BigDecimal(0.0);
  private BigDecimal imimpdvs = new BigDecimal(0.0);
  private BigDecimal imimpeur = new BigDecimal(0.0);
  private Character xtdcm = ' ';
  private Character xtderech = ' ';
  private BigDecimal imcconeu = new BigDecimal(0.0);
  private BigDecimal imcliqeu = new BigDecimal(0.0);
  private BigDecimal imdereeu = new BigDecimal(0.0);
  private String cdusuari = " ";
  private String cdalias = " ";
  private String cdproduc = " ";
  private Integer nuctapro = 0;
  private Character cdbloque = ' ';
  private Character cddatliq = ' ';
  private Character inbokmul = ' ';
  private Character indatfis = ' ';
  private String erdatfis = "";
  private String cdusublo = " ";
  private String cdusuaud = " ";
  private Date fhaudit = new Date();
  private Integer nuversion = 0;
  private String descrali = " ";
  private String cdauxili = " ";
  private Integer nuagrpkb = 0;
  private String ccv;
  private String cdrefban;
  private Character tipexp;
  private BigDecimal imgasliqdv;
  private BigDecimal imgasliqeu;
  private BigDecimal imgasgesdv;
  private BigDecimal imgasgeseu;
  private BigDecimal imgastotdv;
  private BigDecimal imgastoteu;
  private char cuadreau;
  private Character isnacmul = 'N';
  private Character casepti = 'N';
  private Integer cdestadotit = 0;
  private Integer canoncontrpagoclte = 0;
  private Integer canoncontrpagomkt = 0;
  private Integer canonliqpagoclte = 0;
  private Integer canonliqpagocmkt = 0;
  private Integer canoncomppagoclte = 0;
  private Integer canoncomppagomkt = 0;
  private BigDecimal imcanoncompdv = new BigDecimal(0.0);
  private BigDecimal imcanoncompeu = new BigDecimal(0.0);
  private BigDecimal imcorretajesvfinaldv = new BigDecimal(0.0);
  private BigDecimal imcomclteeu = new BigDecimal(0.0);
  private List<Tmct0alo> tmct0alos = new ArrayList<Tmct0alo>();
  private List<Tmct0eje> tmct0ejes = new ArrayList<Tmct0eje>();

  private List<Tmct0gre> tmct0gres = new ArrayList<Tmct0gre>();
  private List<Tmct0bokDynamicValues> dynamicValues = new ArrayList<Tmct0bokDynamicValues>();

  private boolean isGrupo = false;

  public Tmct0bok() {
  }

  public Tmct0bok(Tmct0bokId id, Tmct0ord tmct0ord, Tmct0sta tmct0sta, String erdatfis, char cuadreau) {
    this.id = id;
    this.tmct0ord = tmct0ord;
    this.tmct0sta = tmct0sta;
    this.erdatfis = erdatfis;
    this.cuadreau = cuadreau;
  }

  public Tmct0bok(Tmct0bokId id, Tmct0estado tmct0estadoByCdestadocont, Tmct0estado tmct0estadoByCdestadoasig,
      Tmct0ord tmct0ord, Tmct0sta tmct0sta, String dsobserv, BigDecimal imtpcamb, String cdmoniso, BigDecimal nutitord,
      BigDecimal nutiteje, String cdisin, String nbvalors, Character cdtpoper, BigDecimal imlimite,
      BigDecimal imcbmerc, Date feconsol, Date fealta, Date hoalta, Date fecontra, Date feejecuc, Date hoejecuc,
      Date fevalide, Date fevalor, Date fevalorr, Date fetrade, BigDecimal imntbreu, BigDecimal imnetobr,
      BigDecimal imntcleu, BigDecimal imnetocl, BigDecimal imcbntcl, BigDecimal podevolu, BigDecimal pofpacli,
      BigDecimal imfpacdv, BigDecimal imbensvb, BigDecimal imdevcdv, BigDecimal imdevcli, BigDecimal imfibrdv,
      BigDecimal imfibreu, BigDecimal imsvb, BigDecimal imsvbeu, Character cdindgas, Character cdindimp,
      Character cdreside, BigDecimal imbrmerc, BigDecimal imbrmreu, BigDecimal imgatdvs, BigDecimal imgastos,
      BigDecimal imimpdvs, BigDecimal imimpeur, Character xtdcm, Character xtderech, BigDecimal imcconeu,
      BigDecimal imcliqeu, BigDecimal imdereeu, String cdusuari, String cdalias, String cdproduc, Integer nuctapro,
      Character cdbloque, Character cddatliq, Character inbokmul, Character indatfis, String erdatfis, String cdusublo,
      String cdusuaud, Date fhaudit, Integer nuversion, String descrali, String cdauxili, Integer nuagrpkb, String ccv,
      String cdrefban, Character tipexp, BigDecimal imgasliqdv, BigDecimal imgasliqeu, BigDecimal imgasgesdv,
      BigDecimal imgasgeseu, BigDecimal imgastotdv, BigDecimal imgastoteu, char cuadreau, Character isnacmul,
      Character casepti, Integer cdestadotit, Integer canoncontrpagoclte, Integer canoncontrpagomkt,
      Integer canonliqpagoclte, Integer canonliqpagocmkt, Integer canoncomppagoclte, Integer canoncomppagomkt,
      BigDecimal imcanoncompdv, BigDecimal imcanoncompeu, BigDecimal imcorretajesvfinaldv, BigDecimal imcomclteeu,
      List<Tmct0alo> tmct0alos, List<Tmct0eje> tmct0ejes /*
                                                          * , List<Tmct0gre >
                                                          * tmct0gres
                                                          */
  ) {
    this.id = id;
    this.tmct0estadoByCdestadocont = tmct0estadoByCdestadocont;
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
    this.tmct0ord = tmct0ord;
    this.tmct0sta = tmct0sta;
    this.dsobserv = dsobserv;
    this.imtpcamb = imtpcamb;
    this.cdmoniso = cdmoniso;
    this.nutitord = nutitord;
    this.nutiteje = nutiteje;
    this.cdisin = cdisin;
    this.nbvalors = nbvalors;
    this.cdtpoper = cdtpoper;
    this.imlimite = imlimite;
    this.imcbmerc = imcbmerc;
    this.feconsol = feconsol;
    this.fealta = fealta;
    this.hoalta = hoalta;
    this.fecontra = fecontra;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.fevalide = fevalide;
    this.fevalor = fevalor;
    this.fevalorr = fevalorr;
    this.fetrade = fetrade;
    this.imntbreu = imntbreu;
    this.imnetobr = imnetobr;
    this.imntcleu = imntcleu;
    this.imnetocl = imnetocl;
    this.imcbntcl = imcbntcl;
    this.podevolu = podevolu;
    this.pofpacli = pofpacli;
    this.imfpacdv = imfpacdv;
    this.imbensvb = imbensvb;
    this.imdevcdv = imdevcdv;
    this.imdevcli = imdevcli;
    this.imfibrdv = imfibrdv;
    this.imfibreu = imfibreu;
    this.imsvb = imsvb;
    this.imsvbeu = imsvbeu;
    this.cdindgas = cdindgas;
    this.cdindimp = cdindimp;
    this.cdreside = cdreside;
    this.imbrmerc = imbrmerc;
    this.imbrmreu = imbrmreu;
    this.imgatdvs = imgatdvs;
    this.imgastos = imgastos;
    this.imimpdvs = imimpdvs;
    this.imimpeur = imimpeur;
    this.xtdcm = xtdcm;
    this.xtderech = xtderech;
    this.imcconeu = imcconeu;
    this.imcliqeu = imcliqeu;
    this.imdereeu = imdereeu;
    this.cdusuari = cdusuari;
    this.cdalias = cdalias;
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
    this.cdbloque = cdbloque;
    this.cddatliq = cddatliq;
    this.inbokmul = inbokmul;
    this.indatfis = indatfis;
    this.erdatfis = erdatfis;
    this.cdusublo = cdusublo;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuversion = nuversion;
    this.descrali = descrali;
    this.cdauxili = cdauxili;
    this.nuagrpkb = nuagrpkb;
    this.ccv = ccv;
    this.cdrefban = cdrefban;
    this.tipexp = tipexp;
    this.imgasliqdv = imgasliqdv;
    this.imgasliqeu = imgasliqeu;
    this.imgasgesdv = imgasgesdv;
    this.imgasgeseu = imgasgeseu;
    this.imgastotdv = imgastotdv;
    this.imgastoteu = imgastoteu;
    this.cuadreau = cuadreau;
    this.isnacmul = isnacmul;
    this.casepti = casepti;
    this.cdestadotit = cdestadotit;
    this.canoncontrpagoclte = canoncontrpagoclte;
    this.canoncontrpagomkt = canoncontrpagomkt;
    this.canonliqpagoclte = canonliqpagoclte;
    this.canonliqpagocmkt = canonliqpagocmkt;
    this.canoncomppagoclte = canoncomppagoclte;
    this.canoncomppagomkt = canoncomppagomkt;
    this.imcanoncompdv = imcanoncompdv;
    this.imcanoncompeu = imcanoncompeu;
    this.imcorretajesvfinaldv = imcorretajesvfinaldv;
    this.imcomclteeu = imcomclteeu;
    this.tmct0alos = tmct0alos;
    this.tmct0ejes = tmct0ejes;
    // this.tmct0gres = tmct0gres;
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)) })
  public Tmct0bokId getId() {
    return this.id;
  }

  public void setId(Tmct0bokId id) {
    this.id = id;
  }

  @Transient
  List<Tmct0alc> getTmct0alcs() {
    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
    List<Tmct0alo> alos = this.getTmct0alos();
    if (alos != null) {
      for (Tmct0alo alo : alos) {
        alcs.addAll(alo.getTmct0alcs());
      }
    }
    return alcs;
  }

  @Transient
  public String getCdmercad() {
    String cdmercad = null;
    final Tmct0ord tmct0ord = this.getTmct0ord();
    if (tmct0ord != null) {
      cdmercad = tmct0ord.getCdmercad();
    }
    return cdmercad;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT")
  public Tmct0estado getTmct0estadoByCdestadocont() {
    return this.tmct0estadoByCdestadocont;
  }

  public void setTmct0estadoByCdestadocont(Tmct0estado tmct0estadoByCdestadocont) {
    this.tmct0estadoByCdestadocont = tmct0estadoByCdestadocont;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOASIG")
  public Tmct0estado getTmct0estadoByCdestadoasig() {
    return this.tmct0estadoByCdestadoasig;
  }

  public void setTmct0estadoByCdestadoasig(Tmct0estado tmct0estadoByCdestadoasig) {
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
  }

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "NUORDEN", nullable = false, insertable = false, updatable = false)
  public Tmct0ord getTmct0ord() {
    return this.tmct0ord;
  }

  public void setTmct0ord(Tmct0ord tmct0ord) {
    this.tmct0ord = tmct0ord;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  public Tmct0sta getTmct0sta() {
    return this.tmct0sta;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Column(name = "DSOBSERV", length = 200)
  public String getDsobserv() {
    return this.dsobserv;
  }

  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }

  @Column(name = "IMTPCAMB", precision = 18, scale = 8)
  public BigDecimal getImtpcamb() {
    return this.imtpcamb;
  }

  public void setImtpcamb(BigDecimal imtpcamb) {
    this.imtpcamb = imtpcamb;
  }

  @Column(name = "CDMONISO", length = 3)
  public String getCdmoniso() {
    return this.cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  @Column(name = "NUTITORD", precision = 16, scale = 5)
  public BigDecimal getNutitord() {
    return this.nutitord;
  }

  public void setNutitord(BigDecimal nutitord) {
    this.nutitord = nutitord;
  }

  @Column(name = "NUTITEJE", precision = 16, scale = 5)
  public BigDecimal getNutiteje() {
    return this.nutiteje;
  }

  public void setNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje;
  }

  @Column(name = "CDISIN", length = 12)
  public String getCdisin() {
    return this.cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  @Column(name = "NBVALORS", length = 40)
  public String getNbvalors() {
    return this.nbvalors;
  }

  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  @Column(name = "CDTPOPER", length = 1)
  public Character getCdtpoper() {
    return this.cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  @Column(name = "IMLIMITE", precision = 18, scale = 8)
  public BigDecimal getImlimite() {
    return this.imlimite;
  }

  public void setImlimite(BigDecimal imlimite) {
    this.imlimite = imlimite;
  }

  @Column(name = "IMCBMERC", precision = 18, scale = 8)
  public BigDecimal getImcbmerc() {
    return this.imcbmerc;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECONSOL", length = 4)
  public Date getFeconsol() {
    return this.feconsol;
  }

  public void setFeconsol(Date feconsol) {
    this.feconsol = feconsol;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEALTA", length = 4)
  public Date getFealta() {
    return this.fealta;
  }

  public void setFealta(Date fealta) {
    this.fealta = fealta;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOALTA", length = 3)
  public Date getHoalta() {
    return this.hoalta;
  }

  public void setHoalta(Date hoalta) {
    this.hoalta = hoalta;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FECONTRA", length = 4)
  public Date getFecontra() {
    return this.fecontra;
  }

  public void setFecontra(Date fecontra) {
    this.fecontra = fecontra;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJECUC", length = 4)
  public Date getFeejecuc() {
    return this.feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  @Temporal(TemporalType.TIME)
  @Column(name = "HOEJECUC", length = 3)
  public Date getHoejecuc() {
    return this.hoejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALIDE", length = 4)
  public Date getFevalide() {
    return this.fevalide;
  }

  public void setFevalide(Date fevalide) {
    this.fevalide = fevalide;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 4)
  public Date getFevalor() {
    return this.fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALORR", length = 4)
  public Date getFevalorr() {
    return this.fevalorr;
  }

  public void setFevalorr(Date fevalorr) {
    this.fevalorr = fevalorr;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FETRADE", length = 4)
  public Date getFetrade() {
    return this.fetrade;
  }

  public void setFetrade(Date fetrade) {
    this.fetrade = fetrade;
  }

  @Column(name = "IMNTBREU", precision = 18, scale = 8)
  public BigDecimal getImntbreu() {
    return this.imntbreu;
  }

  public void setImntbreu(BigDecimal imntbreu) {
    this.imntbreu = imntbreu;
  }

  @Column(name = "IMNETOBR", precision = 18, scale = 8)
  public BigDecimal getImnetobr() {
    return this.imnetobr;
  }

  public void setImnetobr(BigDecimal imnetobr) {
    this.imnetobr = imnetobr;
  }

  @Column(name = "IMNTCLEU", precision = 18, scale = 8)
  public BigDecimal getImntcleu() {
    return this.imntcleu;
  }

  public void setImntcleu(BigDecimal imntcleu) {
    this.imntcleu = imntcleu;
  }

  @Column(name = "IMNETOCL", precision = 18, scale = 8)
  public BigDecimal getImnetocl() {
    return this.imnetocl;
  }

  public void setImnetocl(BigDecimal imnetocl) {
    this.imnetocl = imnetocl;
  }

  @Column(name = "IMCBNTCL", precision = 18, scale = 8)
  public BigDecimal getImcbntcl() {
    return this.imcbntcl;
  }

  public void setImcbntcl(BigDecimal imcbntcl) {
    this.imcbntcl = imcbntcl;
  }

  @Column(name = "PODEVOLU", precision = 16, scale = 6)
  public BigDecimal getPodevolu() {
    return this.podevolu;
  }

  public void setPodevolu(BigDecimal podevolu) {
    this.podevolu = podevolu;
  }

  @Column(name = "POFPACLI", precision = 16, scale = 6)
  public BigDecimal getPofpacli() {
    return this.pofpacli;
  }

  public void setPofpacli(BigDecimal pofpacli) {
    this.pofpacli = pofpacli;
  }

  @Column(name = "IMFPACDV", precision = 18, scale = 8)
  public BigDecimal getImfpacdv() {
    return this.imfpacdv;
  }

  public void setImfpacdv(BigDecimal imfpacdv) {
    this.imfpacdv = imfpacdv;
  }

  @Column(name = "IMBENSVB", precision = 18, scale = 8)
  public BigDecimal getImbensvb() {
    return this.imbensvb;
  }

  public void setImbensvb(BigDecimal imbensvb) {
    this.imbensvb = imbensvb;
  }

  @Column(name = "IMDEVCDV", precision = 18, scale = 8)
  public BigDecimal getImdevcdv() {
    return this.imdevcdv;
  }

  public void setImdevcdv(BigDecimal imdevcdv) {
    this.imdevcdv = imdevcdv;
  }

  @Column(name = "IMDEVCLI", precision = 18, scale = 8)
  public BigDecimal getImdevcli() {
    return this.imdevcli;
  }

  public void setImdevcli(BigDecimal imdevcli) {
    this.imdevcli = imdevcli;
  }

  @Column(name = "IMFIBRDV", precision = 18, scale = 8)
  public BigDecimal getImfibrdv() {
    return this.imfibrdv;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  @Column(name = "IMFIBREU", precision = 18, scale = 8)
  public BigDecimal getImfibreu() {
    return this.imfibreu;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  @Column(name = "IMSVB", precision = 18, scale = 8)
  public BigDecimal getImsvb() {
    return this.imsvb;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  @Column(name = "IMSVBEU", precision = 18, scale = 8)
  public BigDecimal getImsvbeu() {
    return this.imsvbeu;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  @Column(name = "CDINDGAS", length = 1)
  public Character getCdindgas() {
    return this.cdindgas;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }

  @Column(name = "CDINDIMP", length = 1)
  public Character getCdindimp() {
    return this.cdindimp;
  }

  public void setCdindimp(Character cdindimp) {
    this.cdindimp = cdindimp;
  }

  @Column(name = "CDRESIDE", length = 1)
  public Character getCdreside() {
    return this.cdreside;
  }

  public void setCdreside(Character cdreside) {
    this.cdreside = cdreside;
  }

  @Column(name = "IMBRMERC", precision = 18, scale = 8)
  public BigDecimal getImbrmerc() {
    return this.imbrmerc;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  @Column(name = "IMBRMREU", precision = 18, scale = 8)
  public BigDecimal getImbrmreu() {
    return this.imbrmreu;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  @Column(name = "IMGATDVS", precision = 18, scale = 8)
  public BigDecimal getImgatdvs() {
    return this.imgatdvs;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  @Column(name = "IMGASTOS", precision = 18, scale = 8)
  public BigDecimal getImgastos() {
    return this.imgastos;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  @Column(name = "IMIMPDVS", precision = 18, scale = 8)
  public BigDecimal getImimpdvs() {
    return this.imimpdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  @Column(name = "IMIMPEUR", precision = 18, scale = 8)
  public BigDecimal getImimpeur() {
    return this.imimpeur;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  @Column(name = "XTDCM", length = 1)
  public Character getXtdcm() {
    return this.xtdcm;
  }

  public void setXtdcm(Character xtdcm) {
    this.xtdcm = xtdcm;
  }

  @Column(name = "XTDERECH", length = 1)
  public Character getXtderech() {
    return this.xtderech;
  }

  public void setXtderech(Character xtderech) {
    this.xtderech = xtderech;
  }

  @Column(name = "IMCCONEU", precision = 18, scale = 8)
  public BigDecimal getImcconeu() {
    return this.imcconeu;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  @Column(name = "IMCLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcliqeu() {
    return this.imcliqeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  @Column(name = "IMDEREEU", precision = 18, scale = 8)
  public BigDecimal getImdereeu() {
    return this.imdereeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  @Column(name = "CDUSUARI", length = 8)
  public String getCdusuari() {
    return this.cdusuari;
  }

  public void setCdusuari(String cdusuari) {
    this.cdusuari = cdusuari;
  }

  @Column(name = "CDALIAS", length = 20)
  public String getCdalias() {
    return this.cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  @Column(name = "CDPRODUC", length = 3)
  public String getCdproduc() {
    return this.cdproduc;
  }

  public void setCdproduc(String cdproduc) {
    this.cdproduc = cdproduc;
  }

  @Column(name = "NUCTAPRO", precision = 5)
  public Integer getNuctapro() {
    return this.nuctapro;
  }

  public void setNuctapro(Integer nuctapro) {
    this.nuctapro = nuctapro;
  }

  @Column(name = "CDBLOQUE", length = 1)
  public Character getCdbloque() {
    return this.cdbloque;
  }

  public void setCdbloque(Character cdbloque) {
    this.cdbloque = cdbloque;
  }

  @Column(name = "CDDATLIQ", length = 1)
  public Character getCddatliq() {
    return this.cddatliq;
  }

  public void setCddatliq(Character cddatliq) {
    this.cddatliq = cddatliq;
  }

  @Column(name = "INBOKMUL", length = 1)
  public Character getInbokmul() {
    return this.inbokmul;
  }

  public void setInbokmul(Character inbokmul) {
    this.inbokmul = inbokmul;
  }

  @Column(name = "INDATFIS", length = 1)
  public Character getIndatfis() {
    return this.indatfis;
  }

  public void setIndatfis(Character indatfis) {
    this.indatfis = indatfis;
  }

  @Column(name = "ERDATFIS", nullable = false, length = 40)
  public String getErdatfis() {
    return this.erdatfis;
  }

  public void setErdatfis(String erdatfis) {
    this.erdatfis = erdatfis;
  }

  @Column(name = "CDUSUBLO", length = 10)
  public String getCdusublo() {
    return this.cdusublo;
  }

  public void setCdusublo(String cdusublo) {
    this.cdusublo = cdusublo;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION", precision = 4)
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "DESCRALI", length = 130)
  public String getDescrali() {
    return this.descrali;
  }

  public void setDescrali(String descrali) {
    this.descrali = descrali;
  }

  @Column(name = "CDAUXILI", length = 20)
  public String getCdauxili() {
    return this.cdauxili;
  }

  public void setCdauxili(String cdauxili) {
    this.cdauxili = cdauxili;
  }

  @Column(name = "NUAGRPKB", precision = 9)
  public Integer getNuagrpkb() {
    return this.nuagrpkb;
  }

  public void setNuagrpkb(Integer nuagrpkb) {
    this.nuagrpkb = nuagrpkb;
  }

  @Column(name = "CCV", length = 40)
  public String getCcv() {
    return this.ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  @Column(name = "CDREFBAN", length = 32)
  public String getCdrefban() {
    return this.cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  @Column(name = "TIPEXP", length = 1)
  public Character getTipexp() {
    return this.tipexp;
  }

  public void setTipexp(Character tipexp) {
    this.tipexp = tipexp;
  }

  @Column(name = "IMGASLIQDV", precision = 18, scale = 8)
  public BigDecimal getImgasliqdv() {
    return this.imgasliqdv;
  }

  public void setImgasliqdv(BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  @Column(name = "IMGASLIQEU", precision = 18, scale = 8)
  public BigDecimal getImgasliqeu() {
    return this.imgasliqeu;
  }

  public void setImgasliqeu(BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  @Column(name = "IMGASGESDV", precision = 18, scale = 8)
  public BigDecimal getImgasgesdv() {
    return this.imgasgesdv;
  }

  public void setImgasgesdv(BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  @Column(name = "IMGASGESEU", precision = 18, scale = 8)
  public BigDecimal getImgasgeseu() {
    return this.imgasgeseu;
  }

  public void setImgasgeseu(BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  @Column(name = "IMGASTOTDV", precision = 18, scale = 8)
  public BigDecimal getImgastotdv() {
    return this.imgastotdv;
  }

  public void setImgastotdv(BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  @Column(name = "IMGASTOTEU", precision = 18, scale = 8)
  public BigDecimal getImgastoteu() {
    return this.imgastoteu;
  }

  public void setImgastoteu(BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  @Column(name = "CUADREAU", nullable = false, length = 1)
  public char getCuadreau() {
    return this.cuadreau;
  }

  public void setCuadreau(char cuadreau) {
    this.cuadreau = cuadreau;
  }

  @Column(name = "ISNACMUL", length = 1)
  public Character getIsnacmul() {
    return this.isnacmul;
  }

  public void setIsnacmul(Character isnacmul) {
    this.isnacmul = isnacmul;
  }

  @Column(name = "CASEPTI", length = 1)
  public Character getCasepti() {
    return this.casepti;
  }

  public void setCasepti(Character casepti) {
    this.casepti = casepti;
  }

  @Column(name = "CDESTADOTIT")
  public Integer getCdestadotit() {
    return this.cdestadotit;
  }

  public void setCdestadotit(Integer cdestadotit) {
    this.cdestadotit = cdestadotit;
  }

  @Column(name = "CANONCONTRPAGOCLTE", precision = 4)
  public Integer getCanoncontrpagoclte() {
    return this.canoncontrpagoclte;
  }

  public void setCanoncontrpagoclte(Integer canoncontrpagoclte) {
    this.canoncontrpagoclte = canoncontrpagoclte;
  }

  @Column(name = "CANONCONTRPAGOMKT", precision = 4)
  public Integer getCanoncontrpagomkt() {
    return this.canoncontrpagomkt;
  }

  public void setCanoncontrpagomkt(Integer canoncontrpagomkt) {
    this.canoncontrpagomkt = canoncontrpagomkt;
  }

  @Column(name = "CANONLIQPAGOCLTE", precision = 4)
  public Integer getCanonliqpagoclte() {
    return this.canonliqpagoclte;
  }

  public void setCanonliqpagoclte(Integer canonliqpagoclte) {
    this.canonliqpagoclte = canonliqpagoclte;
  }

  @Column(name = "CANONLIQPAGOCMKT", precision = 4)
  public Integer getCanonliqpagocmkt() {
    return this.canonliqpagocmkt;
  }

  public void setCanonliqpagocmkt(Integer canonliqpagocmkt) {
    this.canonliqpagocmkt = canonliqpagocmkt;
  }

  @Column(name = "CANONCOMPPAGOCLTE", precision = 4)
  public Integer getCanoncomppagoclte() {
    return this.canoncomppagoclte;
  }

  public void setCanoncomppagoclte(Integer canoncomppagoclte) {
    this.canoncomppagoclte = canoncomppagoclte;
  }

  @Column(name = "CANONCOMPPAGOMKT", precision = 4)
  public Integer getCanoncomppagomkt() {
    return this.canoncomppagomkt;
  }

  public void setCanoncomppagomkt(Integer canoncomppagomkt) {
    this.canoncomppagomkt = canoncomppagomkt;
  }

  @Column(name = "IMCANONCOMPDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncompdv() {
    return this.imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  @Column(name = "IMCANONCOMPEU", precision = 18, scale = 8)
  public BigDecimal getImcanoncompeu() {
    return this.imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  @Column(name = "IMCORRETAJESVFINALDV", precision = 18, scale = 8)
  public BigDecimal getImcorretajesvfinaldv() {
    return this.imcorretajesvfinaldv;
  }

  public void setImcorretajesvfinaldv(BigDecimal imcorretajesvfinaldv) {
    this.imcorretajesvfinaldv = imcorretajesvfinaldv;
  }

  @Column(name = "IMCOMCLTEEU", precision = 18, scale = 8)
  public BigDecimal getImcomclteeu() {
    return this.imcomclteeu;
  }

  public void setImcomclteeu(BigDecimal imcomclteeu) {
    this.imcomclteeu = imcomclteeu;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0bok")
  public List<Tmct0alo> getTmct0alos() {
    return this.tmct0alos;
  }

  public void setTmct0alos(List<Tmct0alo> tmct0alos) {
    this.tmct0alos = tmct0alos;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0bok")
  public List<Tmct0eje> getTmct0ejes() {
    return this.tmct0ejes;
  }

  public void setTmct0ejes(List<Tmct0eje> tmct0ejes) {
    this.tmct0ejes = tmct0ejes;
  }

  @Transient
  public boolean isGrupo() {
    return isGrupo;
  }

  public void setGrupo(boolean isGrupo) {
    this.isGrupo = isGrupo;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0bok")
  public List<Tmct0gre> getTmct0gres() {
    return tmct0gres;
  }

  public void setTmct0gres(List<Tmct0gre> tmct0gres) {
    this.tmct0gres = tmct0gres;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0bok", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
  public List<Tmct0bokDynamicValues> getDynamicValues() {
    return dynamicValues;
  }

  public void setDynamicValues(List<Tmct0bokDynamicValues> dynamicValues) {
    this.dynamicValues = dynamicValues;
  }

}
