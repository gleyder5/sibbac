package sibbac.business.minijobs.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


@Table( name = DBConstants.SINECC.NUMERO_OPERACION_DCV )
@Entity
@XmlRootElement
public class NumeroOperacionDcv extends ATable< NumeroOperacionDcv > implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 3671296140002836054L;

	@XmlAttribute
	@Column( name = "CODIGO_DCV", length = 11, nullable = true )
	protected java.lang.String		codigoDcv;

	@XmlAttribute
	@Column( name = "NUMERO_OPERACION_DCV", length = 35, nullable = true )
	protected java.lang.String		numeroOperacionDCV;

	@XmlAttribute
	@Column( name = "CDMIEMBROMKT", length = 4, nullable = true )
	protected java.lang.String		cdmiembromkt;

	@XmlAttribute
	@Column( name = "NUORDENMKT", length = 9, nullable = true )
	protected java.lang.String		nuordenmkt;

	@XmlAttribute
	@Column( name = "NUEXEMKT", length = 9, nullable = true )
	protected java.lang.String		nuexemkt;

	@XmlAttribute
	@Column( name = "CDISIN", length = 12, nullable = true )
	protected java.lang.String		cdisin;

	@XmlAttribute
	@Column( name = "CDSENTIDO", length = 1, nullable = true )
	protected java.lang.String		cdsentido;

	@XmlAttribute
	@Column( name = "CDSEGMENTO", length = 2, nullable = true )
	protected java.lang.String		cdsegmento;

	@XmlAttribute
	@Column( name = "CDOPERACIONMKT", length = 2, nullable = true )
	protected java.lang.String		cdoperacionmkt;

	@XmlAttribute
	@Column( name = "CD_PLATAFORMA_NEGOCIACION", length = 4, nullable = true )
	protected java.lang.String		cdPlataformaNegociacion;

	@XmlAttribute
	@Column( name = "CDINDCOTIZACION", length = 1, nullable = true )
	protected java.lang.String		cdindcotizacion;

	@XmlAttribute
	@Temporal( TemporalType.DATE )
	@Column( name = "FORDENMKT", nullable = true )
	protected java.util.Date		fordenmkt;

	@XmlAttribute
	@Temporal( TemporalType.TIME )
	@Column( name = "HORDENMKT", nullable = true )
	protected java.util.Date		hordenmkt;

	@XmlAttribute
	@Temporal( TemporalType.DATE )
	@Column( name = "FLIQTEORICA", nullable = true )
	protected java.util.Date		fliqteorica;

	@XmlAttribute
	@Temporal( TemporalType.DATE )
	@Column( name = "FNEGOCIACION", nullable = true )
	protected java.util.Date		fnegociacion;

	@XmlAttribute
	@Temporal( TemporalType.TIME )
	@Column( name = "HNEGOCIACION", nullable = true )
	protected java.util.Date		hnegociacion;

	@XmlAttribute
	@Column( name = "IMTITULOS", precision = 18, scale = 6, nullable = true )
	protected java.math.BigDecimal	imtitulos;

	@XmlAttribute
	@Column( name = "IMPRECIO", precision = 13, scale = 6, nullable = true )
	protected java.math.BigDecimal	imprecio;

	@XmlAttribute
	@Column( name = "IMEFECTIVO", precision = 15, scale = 2, nullable = true )
	protected java.math.BigDecimal	imefectivo;

	@XmlAttribute
	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "AUDIT_FECHA_CAMBIO", nullable = true )
	protected java.util.Date		audit_fecha_cambio;

	@XmlAttribute
	@Column( name = "AUDIT_USER_CAMBIO", length = 20, nullable = true )
	protected java.lang.String		audit_user_cambio;

	@XmlAttribute
	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "AUDIT_FECHA_ALTA", nullable = true )
	protected java.util.Date		audit_fecha_alta;

	@XmlAttribute
	@Column( name = "AUDIT_USER_ALTA", length = 20, nullable = true )
	protected java.lang.String		audit_user_alta;

	public NumeroOperacionDcv() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	/**
	 * @return the codigoDcv
	 */
	public java.lang.String getCodigoDcv() {
		return codigoDcv;
	}

	/**
	 * @param codigoDcv the codigoDcv to set
	 */
	public void setCodigoDcv( java.lang.String codigoDcv ) {
		this.codigoDcv = codigoDcv;
	}

	/**
	 * @return the numeroOperacionDCV
	 */
	public java.lang.String getNumeroOperacionDCV() {
		return numeroOperacionDCV;
	}

	/**
	 * @param numeroOperacionDCV the numeroOperacionDCV to set
	 */
	public void setNumeroOperacionDCV( java.lang.String numeroOperacionDCV ) {
		this.numeroOperacionDCV = numeroOperacionDCV;
	}

	/**
	 * @return the cdmiembromkt
	 */
	public java.lang.String getCdmiembromkt() {
		return cdmiembromkt;
	}

	/**
	 * @param cdmiembromkt the cdmiembromkt to set
	 */
	public void setCdmiembromkt( java.lang.String cdmiembromkt ) {
		this.cdmiembromkt = cdmiembromkt;
	}

	/**
	 * @return the nuordenmkt
	 */
	public java.lang.String getNuordenmkt() {
		return nuordenmkt;
	}

	/**
	 * @param nuordenmkt the nuordenmkt to set
	 */
	public void setNuordenmkt( java.lang.String nuordenmkt ) {
		this.nuordenmkt = nuordenmkt;
	}

	/**
	 * @return the nuexemkt
	 */
	public java.lang.String getNuexemkt() {
		return nuexemkt;
	}

	/**
	 * @param nuexemkt the nuexemkt to set
	 */
	public void setNuexemkt( java.lang.String nuexemkt ) {
		this.nuexemkt = nuexemkt;
	}

	/**
	 * @return the cdisin
	 */
	public java.lang.String getCdisin() {
		return cdisin;
	}

	/**
	 * @param cdisin the cdisin to set
	 */
	public void setCdisin( java.lang.String cdisin ) {
		this.cdisin = cdisin;
	}

	/**
	 * @return the cdsentido
	 */
	public java.lang.String getCdsentido() {
		return cdsentido;
	}

	/**
	 * @param cdsentido the cdsentido to set
	 */
	public void setCdsentido( java.lang.String cdsentido ) {
		this.cdsentido = cdsentido;
	}

	/**
	 * @return the cdsegmento
	 */
	public java.lang.String getCdsegmento() {
		return cdsegmento;
	}

	/**
	 * @param cdsegmento the cdsegmento to set
	 */
	public void setCdsegmento( java.lang.String cdsegmento ) {
		this.cdsegmento = cdsegmento;
	}

	/**
	 * @return the cdoperacionmkt
	 */
	public java.lang.String getCdoperacionmkt() {
		return cdoperacionmkt;
	}

	/**
	 * @param cdoperacionmkt the cdoperacionmkt to set
	 */
	public void setCdoperacionmkt( java.lang.String cdoperacionmkt ) {
		this.cdoperacionmkt = cdoperacionmkt;
	}

	/**
	 * @return the cdPlataformaNegociacion
	 */
	public java.lang.String getCdPlataformaNegociacion() {
		return cdPlataformaNegociacion;
	}

	/**
	 * @param cdPlataformaNegociacion the cdPlataformaNegociacion to set
	 */
	public void setCdPlataformaNegociacion( java.lang.String cdPlataformaNegociacion ) {
		this.cdPlataformaNegociacion = cdPlataformaNegociacion;
	}

	/**
	 * @return the cdindcotizacion
	 */
	public java.lang.String getCdindcotizacion() {
		return cdindcotizacion;
	}

	/**
	 * @param cdindcotizacion the cdindcotizacion to set
	 */
	public void setCdindcotizacion( java.lang.String cdindcotizacion ) {
		this.cdindcotizacion = cdindcotizacion;
	}

	/**
	 * @return the fordenmkt
	 */
	public java.util.Date getFordenmkt() {
		return fordenmkt;
	}

	/**
	 * @param fordenmkt the fordenmkt to set
	 */
	public void setFordenmkt( java.util.Date fordenmkt ) {
		this.fordenmkt = fordenmkt;
	}

	/**
	 * @return the hordenmkt
	 */
	public java.util.Date getHordenmkt() {
		return hordenmkt;
	}

	/**
	 * @param hordenmkt the hordenmkt to set
	 */
	public void setHordenmkt( java.util.Date hordenmkt ) {
		this.hordenmkt = hordenmkt;
	}

	/**
	 * @return the fliqteorica
	 */
	public java.util.Date getFliqteorica() {
		return fliqteorica;
	}

	/**
	 * @param fliqteorica the fliqteorica to set
	 */
	public void setFliqteorica( java.util.Date fliqteorica ) {
		this.fliqteorica = fliqteorica;
	}

	/**
	 * @return the fnegociacion
	 */
	public java.util.Date getFnegociacion() {
		return fnegociacion;
	}

	/**
	 * @param fnegociacion the fnegociacion to set
	 */
	public void setFnegociacion( java.util.Date fnegociacion ) {
		this.fnegociacion = fnegociacion;
	}

	/**
	 * @return the hnegociacion
	 */
	public java.util.Date getHnegociacion() {
		return hnegociacion;
	}

	/**
	 * @param hnegociacion the hnegociacion to set
	 */
	public void setHnegociacion( java.util.Date hnegociacion ) {
		this.hnegociacion = hnegociacion;
	}

	/**
	 * @return the imtitulos
	 */
	public java.math.BigDecimal getImtitulos() {
		return imtitulos;
	}

	/**
	 * @param imtitulos the imtitulos to set
	 */
	public void setImtitulos( java.math.BigDecimal imtitulos ) {
		this.imtitulos = imtitulos;
	}

	/**
	 * @return the imprecio
	 */
	public java.math.BigDecimal getImprecio() {
		return imprecio;
	}

	/**
	 * @param imprecio the imprecio to set
	 */
	public void setImprecio( java.math.BigDecimal imprecio ) {
		this.imprecio = imprecio;
	}

	/**
	 * @return the imefectivo
	 */
	public java.math.BigDecimal getImefectivo() {
		return imefectivo;
	}

	/**
	 * @param imefectivo the imefectivo to set
	 */
	public void setImefectivo( java.math.BigDecimal imefectivo ) {
		this.imefectivo = imefectivo;
	}

	/**
	 * @return the audit_fecha_cambio
	 */
	public java.util.Date getAudit_fecha_cambio() {
		return audit_fecha_cambio;
	}

	/**
	 * @param audit_fecha_cambio the audit_fecha_cambio to set
	 */
	public void setAudit_fecha_cambio( java.util.Date audit_fecha_cambio ) {
		this.audit_fecha_cambio = audit_fecha_cambio;
	}

	/**
	 * @return the audit_user_cambio
	 */
	public java.lang.String getAudit_user_cambio() {
		return audit_user_cambio;
	}

	/**
	 * @param audit_user_cambio the audit_user_cambio to set
	 */
	public void setAudit_user_cambio( java.lang.String audit_user_cambio ) {
		this.audit_user_cambio = audit_user_cambio;
	}

	/**
	 * @return the audit_fecha_alta
	 */
	public java.util.Date getAudit_fecha_alta() {
		return audit_fecha_alta;
	}

	/**
	 * @param audit_fecha_alta the audit_fecha_alta to set
	 */
	public void setAudit_fecha_alta( java.util.Date audit_fecha_alta ) {
		this.audit_fecha_alta = audit_fecha_alta;
	}

	/**
	 * @return the audit_user_alta
	 */
	public java.lang.String getAudit_user_alta() {
		return audit_user_alta;
	}

	/**
	 * @param audit_user_alta the audit_user_alta to set
	 */
	public void setAudit_user_alta( java.lang.String audit_user_alta ) {
		this.audit_user_alta = audit_user_alta;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NumeroOperacionDcv [id=" + id + ", codigoDcv=" + codigoDcv + ", numeroOperacionDCV=" + numeroOperacionDCV
				+ ", cdmiembromkt=" + cdmiembromkt + ", nuordenmkt=" + nuordenmkt + ", nuexemkt=" + nuexemkt + ", cdisin=" + cdisin
				+ ", cdsentido=" + cdsentido + ", cdsegmento=" + cdsegmento + ", cdoperacionmkt=" + cdoperacionmkt
				+ ", cdPlataformaNegociacion=" + cdPlataformaNegociacion + ", cdindcotizacion=" + cdindcotizacion + ", fordenmkt="
				+ fordenmkt + ", hordenmkt=" + hordenmkt + ", fliqteorica=" + fliqteorica + ", fnegociacion=" + fnegociacion
				+ ", hnegociacion=" + hnegociacion + ", imtitulos=" + imtitulos + ", imprecio=" + imprecio + ", imefectivo=" + imefectivo
				+ ", audit_fecha_cambio=" + audit_fecha_cambio + ", audit_user_cambio=" + audit_user_cambio + ", audit_fecha_alta="
				+ audit_fecha_alta + ", audit_user_alta=" + audit_user_alta + "]";
	}

}
