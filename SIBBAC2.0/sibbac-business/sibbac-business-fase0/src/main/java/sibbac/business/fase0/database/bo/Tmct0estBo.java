package sibbac.business.fase0.database.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0estDao;
import sibbac.business.fase0.database.model.Tmct0est;
import sibbac.business.fase0.database.model.Tmct0estId;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0estBo extends AbstractBo<Tmct0est, Tmct0estId, Tmct0estDao> {

  /**
   * Recupera la lista de alias activos
   */

  public Tmct0est findByCdaliassFechaActual(String cdaliass, BigDecimal fhactual) {
    return dao.findByCdaliassFechaActual(cdaliass, fhactual);
  }
}
