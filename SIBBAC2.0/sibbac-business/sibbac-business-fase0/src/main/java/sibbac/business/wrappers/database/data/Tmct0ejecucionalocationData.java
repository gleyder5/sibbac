package sibbac.business.wrappers.database.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;

public class Tmct0ejecucionalocationData implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  private Long idejecucionalocation;
  private Tmct0ejeData tmct0eje;
  private Tmct0grupoejecucionData tmct0grupoejecucion;
  private Long idGrupo;
  private BigDecimal nutitulos;
  private BigDecimal numTitulosRestantes;
  private Date auditFechaCambio;
  private String auditUser;
  private BigDecimal nutitulosPendientesDesglose;
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ejecucionalocationData.class);
  private static final String TAG = Tmct0ejecucionalocationData.class.getName();

  public Tmct0ejecucionalocationData() {

  }

  public Tmct0ejecucionalocationData(Tmct0ejecucionalocation entity) {
    entityToData(entity, this);
  }

  public Tmct0ejecucionalocationData(Tmct0ejecucionalocationData original,
                                     HashMap<Long, Tmct0grupoejecucionData> hmGruposEjecucion) {
    this.setIdejecucionalocation(null);
    this.setTmct0eje(original.getTmct0eje());
    this.setNutitulos(original.getNutitulos());
    this.setNutitulosPendientesDesglose(original.getNutitulosPendientesDesglose());
    this.setAuditFechaCambio(new Date());
    this.setAuditUser(original.getAuditUser());

    Long idgrupoeje = original.getTmct0grupoejecucion().getIdgrupoeje();
    Tmct0grupoejecucionData tmct0grupoejecucion = hmGruposEjecucion.get(idgrupoeje);

    this.setTmct0grupoejecucion(tmct0grupoejecucion);
    this.setIdGrupo(idgrupoeje);
    if (tmct0grupoejecucion == null) {
      this.tmct0grupoejecucion = new Tmct0grupoejecucionData(original.getTmct0grupoejecucion());
    }
    hmGruposEjecucion.put(idgrupoeje, this.tmct0grupoejecucion);

  }

  public static Tmct0ejecucionalocationData entityToData(Tmct0ejecucionalocation entity) {
    Tmct0ejecucionalocationData data = new Tmct0ejecucionalocationData();
    entityToData(entity, data);
    return data;
  }// public static Tmct0ejecucionalocationData
   // entityToData(Tmct0ejecucionalocation entity){

  public static void entityToData(Tmct0ejecucionalocation entity, Tmct0ejecucionalocationData data) {
    data.setAuditFechaCambio(entity.getAuditFechaCambio());
    data.setAuditUser(entity.getAuditUser());
    data.setIdejecucionalocation(entity.getIdejecucionalocation());
    data.setNutitulos(entity.getNutitulos());
    data.setNumTitulosRestantes(entity.getNutitulos());
    data.setNutitulosPendientesDesglose(entity.getNutitulosPendientesDesglose());
    data.setTmct0eje(Tmct0ejeData.entityToData(entity.getTmct0eje()));
    data.setTmct0grupoejecucion(Tmct0grupoejecucionData.entityToData(entity.getTmct0grupoejecucion()));
  }// public static void entityToData(Tmct0ejecucionalocation entity,
   // Tmct0ejecucionalocationData data) {

  public static void dataToEntity(Tmct0ejecucionalocationData data, Tmct0ejecucionalocation entity) {
    dataToEntity(data, entity, true);
  }// public static Tmct0ejecucionalocation
   // dataToEntity(Tmct0ejecucionalocationData data) {

  public static void dataToEntity(Tmct0ejecucionalocationData data,
                                  Tmct0ejecucionalocation entity,
                                  boolean entityGrupoejecucion) {
    entity.setAuditFechaCambio(data.getAuditFechaCambio());
    entity.setAuditUser(data.getAuditUser());
    entity.setIdejecucionalocation(data.getIdejecucionalocation());
    entity.setNutitulos(data.getNutitulos());
    entity.setNutitulosPendientesDesglose(data.getNutitulosPendientesDesglose());
    entity.setTmct0eje(Tmct0ejeData.dataToEntity(data.getTmct0eje()));
    if (entityGrupoejecucion) {
      entity.setTmct0grupoejecucion(Tmct0grupoejecucionData.dataToEntity(data.getTmct0grupoejecucion()));
    }
  }// public static Tmct0ejecucionalocation
   // dataToEntity(Tmct0ejecucionalocationData data) {

  public static Tmct0ejecucionalocation dataToEntity(Tmct0ejecucionalocationData data, boolean entityGrupoejecucion) {
    Tmct0ejecucionalocation entity = new Tmct0ejecucionalocation();
    dataToEntity(data, entity, entityGrupoejecucion);
    return entity;
  }// public static Tmct0ejecucionalocation
   // dataToEntity(Tmct0ejecucionalocationData data, boolean
   // entityGrupoejecucion) {

  public static Tmct0ejecucionalocation dataToEntity(Tmct0ejecucionalocationData data) {
    Tmct0ejecucionalocation entity = new Tmct0ejecucionalocation();
    dataToEntity(data, entity, true);
    return entity;
  }// public static Tmct0ejecucionalocation
   // dataToEntity(Tmct0ejecucionalocationData data) {

  public Long getIdejecucionalocation() {
    return idejecucionalocation;
  }

  public void setIdejecucionalocation(Long idejecucionalocation) {
    this.idejecucionalocation = idejecucionalocation;
  }

  public Tmct0ejeData getTmct0eje() {
    return tmct0eje;
  }

  public void setTmct0eje(Tmct0ejeData tmct0eje) {
    this.tmct0eje = tmct0eje;
  }

  public Tmct0grupoejecucionData getTmct0grupoejecucion() {
    return tmct0grupoejecucion;
  }

  public void setTmct0grupoejecucion(Tmct0grupoejecucionData tmct0grupoejecucion) {
    this.tmct0grupoejecucion = tmct0grupoejecucion;
  }

  public BigDecimal getNutitulos() {
    return nutitulos;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  public BigDecimal getNumTitulosRestantes() {
    return numTitulosRestantes;
  }

  public void setNumTitulosRestantes(BigDecimal numTitulosRestantes) {
    this.numTitulosRestantes = numTitulosRestantes;
  }

  public Date getAuditFechaCambio() {
    return auditFechaCambio;
  }

  public void setAuditFechaCambio(Date auditFechaCambio) {
    this.auditFechaCambio = auditFechaCambio;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public BigDecimal getNutitulosPendientesDesglose() {
    return nutitulosPendientesDesglose;
  }

  public void setNutitulosPendientesDesglose(BigDecimal nutitulosPendientesDesglose) {
    this.nutitulosPendientesDesglose = nutitulosPendientesDesglose;
  }

  public static List<Tmct0ejecucionalocation> dataListToEntityList(List<Tmct0ejecucionalocationData> tmct0ejecucionalocations) {
    List<Tmct0ejecucionalocation> entityList = new ArrayList<Tmct0ejecucionalocation>();
    if (CollectionUtils.isNotEmpty(tmct0ejecucionalocations)) {
      for (Tmct0ejecucionalocationData data : tmct0ejecucionalocations) {
        entityList.add(Tmct0ejecucionalocationData.dataToEntity(data, false));
      }
    } else {
      return new ArrayList<Tmct0ejecucionalocation>(0);
    }
    return entityList;
  }

  public Long getIdGrupo() {
    return idGrupo;
  }

  public void setIdGrupo(Long idGrupo) {
    this.idGrupo = idGrupo;
  }

  @Override
  public String toString() {
    return "Tmct0ejecucionalocationData [idejecucionalocation=" + idejecucionalocation + ", tmct0eje=" + tmct0eje
           + ", tmct0grupoejecucion=" + tmct0grupoejecucion + ", idGrupo=" + idGrupo + ", nutitulos=" + nutitulos
           + ", numTitulosRestantes=" + numTitulosRestantes + ", auditFechaCambio=" + auditFechaCambio + ", auditUser="
           + auditUser + ", nutitulosPendientesDesglose=" + nutitulosPendientesDesglose + "]";
  }

}// public class Tmct0ejecucionalocationData implements java.io.Serializable {
