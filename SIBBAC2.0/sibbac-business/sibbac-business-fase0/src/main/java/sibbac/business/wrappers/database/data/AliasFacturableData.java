package sibbac.business.wrappers.database.data;


import org.apache.commons.lang3.StringUtils;

import sibbac.business.wrappers.database.model.Alias;


public class AliasFacturableData {

	private Alias	alias;
	private String	cdsubctaFacturada;
	private String	cdaliasFacturado;

	public AliasFacturableData( final Alias alias, final String cdaliasFacturado ) {
		this.alias = alias;
		this.cdaliasFacturado = cdaliasFacturado;
	}

	public AliasFacturableData( final Alias alias, final String cdaliasFacturado, final String cdsubctaFacturada ) {
		this.alias = alias;
		this.cdaliasFacturado = cdaliasFacturado;
		this.cdsubctaFacturada = cdsubctaFacturada;
	}

	public String getCdAlias() {
		String cdAlias = null;
		if ( StringUtils.isNotBlank( cdsubctaFacturada ) ) {
			cdAlias = StringUtils.rightPad( cdsubctaFacturada.trim() + "|" + cdaliasFacturado.trim(), 20 );
		} else {
			cdAlias = cdaliasFacturado.trim();
		}
		return cdAlias;
	}

	/**
	 * @return the alias
	 */
	public Alias getAlias() {
		return alias;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias( Alias alias ) {
		this.alias = alias;
	}

	/**
	 * @return the cdsubctaFacturada
	 */
	public String getCdsubctaFacturada() {
		return cdsubctaFacturada;
	}

	/**
	 * @param cdsubctaFacturada the cdsubctaFacturada to set
	 */
	public void setCdsubctaFacturada( String cdsubctaFacturada ) {
		this.cdsubctaFacturada = cdsubctaFacturada;
	}

	/**
	 * @return the cdaliasFacturado
	 */
	public String getCdaliasFacturado() {
		return cdaliasFacturado;
	}

	/**
	 * @param cdaliasFacturado the cdaliasFacturado to set
	 */
	public void setCdaliasFacturado( String cdaliasFacturado ) {
		this.cdaliasFacturado = cdaliasFacturado;
	}

}
