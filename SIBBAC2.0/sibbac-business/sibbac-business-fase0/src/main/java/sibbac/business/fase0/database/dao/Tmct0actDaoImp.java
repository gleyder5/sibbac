package sibbac.business.fase0.database.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.Tmct0actDTO;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0actId;


@Repository
public class Tmct0actDaoImp {
	
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0actDaoImp.class);
	
	@PersistenceContext
	private EntityManager	em;
	
	@SuppressWarnings("unchecked")
	public List<Tmct0act> findSubCtasUnicas(String alias){
		
		
		Calendar c1 = Calendar.getInstance();
		String fechaTxt = Integer.toString(c1.get(Calendar.YEAR))+Integer.toString(c1.get(Calendar.MONTH))+Integer.toString(c1.get(Calendar.DATE));
		
		Integer fechaActual = Integer.parseInt(fechaTxt);
		
		StringBuilder select = new StringBuilder("select CDSUBCTA, CDBROCLI, DESCRALI ");
		StringBuilder from = new StringBuilder ("from Tmct0act ");
		StringBuilder order = new StringBuilder("where CDALIASS = :alias and FHFINAL >= :fechaActual order by CDSUBCTA");
		
		Query query = null;
		
		query = em.createNativeQuery( select.append( from.append( order ) ).toString() );
		
		setParameters( query,  alias, fechaActual, "alias", "fechaActual");
		
		Tmct0act act = null;
		Tmct0actId id = null;
		List< Tmct0act > listaAct = new ArrayList< Tmct0act >();
		List< Object > resultList = null;
		

		resultList = query.getResultList();
		
		if ( CollectionUtils.isNotEmpty( resultList ) ) {
			for ( Object obj : resultList ) {
				Object[] obj2 = (Object[]) obj;
				act = new Tmct0act();
				id = new Tmct0actId();
				id.setCdsubcta((String) obj2[0]);
				act.setId(id);
				act.setCdbroclifacturacion((String) obj2[1]); 
				act.setDescrali((String) obj2[2]);
				listaAct.add(act);
			}
		}
		
		return listaAct;
		
	}
	private void setParameters( Query query, String alias, Integer fechaActual, String aliasParam, String fechaActualParam	){
		
		if ( alias != null  ) {
			query.setParameter( aliasParam, alias );
		}
		
		if ( fechaActual != null  ) {
			query.setParameter( fechaActualParam, fechaActual );
		}


	}
	
	public List<Tmct0actDTO> findSubCtasBymanyReglas(List<Long> idReglas){
		
		
		Calendar c1 = Calendar.getInstance();
		String fechaTxt = Integer.toString(c1.get(Calendar.YEAR))+Integer.toString(c1.get(Calendar.MONTH))+Integer.toString(c1.get(Calendar.DATE));
		
		Integer fechaActual = Integer.parseInt(fechaTxt);
		
		StringBuilder select = new StringBuilder("select regla.id_regla, regla.cd_codigo, act.cdaliass, act.CDSUBCTA, act.descrali ");
		StringBuilder from = new StringBuilder ("from Tmct0act act left join tmct0_regla regla on regla.ID_REGLA = act.id_regla ");
		StringBuilder order = new StringBuilder("WHERE act.ID_REGLA in ( :idReglas ) and act.FHFINAL >= :fechaActual order by regla.activo DESC, regla.id_regla");
		
		Query query = null;
		
		query = em.createNativeQuery( select.append( from.append( order ) ).toString() );
		
		setParametersWithVectors( query,  idReglas, fechaActual, "idReglas", "fechaActual");
		
		Tmct0actDTO act = null;
		List< Tmct0actDTO > listaAct = new ArrayList< Tmct0actDTO >();
		List< Object > resultList = null;
		
		try{
			resultList = query.getResultList();
			
			if ( CollectionUtils.isNotEmpty( resultList ) ) {
				for ( Object obj : resultList ) {

					act =  Tmct0actDTO.convertObjectToSubCtaDTO( ( Object[] ) obj );
					listaAct.add(act);
				}
			}
		}catch(Exception ex){
			LOG.error("Error al consultar TMCT0ACT por reglas: ", ex.getClass().getName(), ex.getMessage());
		}
		
		return listaAct;

	}
	
	private void setParametersWithVectors(Query query, List<Long> idReglas, Integer fechaActual, String idReglasParamName, String fechaActualParamName ){

		if ( idReglas != null && idReglas.size() > 0  ) {
			query.setParameter( idReglasParamName, idReglas );
		}
		
		if ( fechaActual != null  ) {
			query.setParameter( fechaActualParamName, fechaActual );
		}			
		
	}

}
