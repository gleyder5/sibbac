package sibbac.business.fase0.database.dto;

import java.util.Date;

import sibbac.common.utils.FormatDataUtils;

public class SessionStatus {

  public static enum STATUS {
    UNKNOWN("No disponible"),
    WEEKEND("Fin de semana"),
    SCRIPT_DIVIDEND("Script Dividend"),
    SESSION_BEFORE("La sesión aún no ha comenzado"),
    SESSION_OPENED("La sesión está abierta. Cerrará a las: "),
    SESSION_CLOSED("La sesión está cerrada"),
    MANUAL_SESSION_BEFORE("La sesión está cerrada. Hay prevista una sesión adicional que comenzará a las "),
    MANUAL_SESSION_AFTER("La sesión adicional está abierta."),
    USER_CLOSED("El usuario ha cerrado la sesión");
    private String description;

    private STATUS(final String description) {
      this.description = description;
    }

    public String getDescription() {
      return description;
    }
  };

  public static enum ERROR {
    NO_ERROR("Sin errores"),
    DATA_ACCESS("Error en el acceso a los datos"),
    NO_INI_TIME("No hay hora de inicio de sesión"),
    NO_END_TIME("No hay hora de fin de sesión");
    private String description;

    private ERROR(final String description) {
      this.description = description;
    }

    public String getDescription() {
      return description;
    }
  };

  private Date when = null;
  private boolean weekEnd = false;
  private boolean scriptDividend = false;
  private boolean generalClosed = false;

  private boolean beforeStart = false;
  private boolean userSessionStart = false;
  private boolean userClosedTheSession = false;
  private boolean opened = false;
  private boolean userWindowAvailable = false;

  private Date inicioSesion = null;
  private String strInicioSesion = null;
  private Date finSesion = null;
  private String strFinSesion = null;
  private Date inicioUserSesion = null;
  private String strInicioUserSesion = null;

  private STATUS status = STATUS.UNKNOWN;
  private ERROR error = ERROR.NO_ERROR;

  /**
   * 
   */
  public SessionStatus() {
    this(new Date());
  }

  /**
   * 
   */
  public SessionStatus(Date when) {
    this.when = when;
  }

  /**
   * Getter for when
   *
   * @return the when
   */
  public Date getWhen() {
    return when;
  }

  /**
   * Setter for when
   *
   * @param when the when to set
   */
  public void setWhen(Date when) {
    this.when = when;
  }

  public boolean canManuallyOpenOrCloseTheSession() {
    return (this.error.compareTo(ERROR.NO_ERROR) == 0 && (this.status.compareTo(STATUS.UNKNOWN) != 0 && this.status.compareTo(STATUS.WEEKEND) != 0));
  }

  public String getInfo() {
    if (this.error.compareTo(ERROR.NO_ERROR) == 0) {
      String info = this.status.getDescription();
      switch (this.status) {
        case SESSION_OPENED:
          info += this.getStrFinSesion();
          break;
        case MANUAL_SESSION_BEFORE:
          info += this.getStrInicioUserSesion();
          break;
        default:
          break;
      }
      return info;
    } else {
      return this.error.getDescription();
    }
  }

  /**
   * Getter for status
   *
   * @return the status
   */
  public STATUS getStatus() {
    return status;
  }

  /**
   * Setter for status
   *
   * @param status the status to set
   */
  public void setStatus(STATUS status) {
    this.status = status;
  }

  /**
   * Getter for error
   *
   * @return the error
   */
  public ERROR getError() {
    return error;
  }

  /**
   * Setter for error
   *
   * @param error the error to set
   */
  public void setError(ERROR error) {
    this.error = error;
  }

  /**
   * Getter for weekEnd
   *
   * @return the weekEnd
   */
  public boolean isWeekEnd() {
    return weekEnd;
  }

  /**
   * Setter for weekEnd
   *
   * @param weekEnd the weekEnd to set
   */
  public void setWeekEnd(boolean weekEnd) {
    this.weekEnd = weekEnd;
  }

  /**
   * Getter for scriptDividend
   *
   * @return the scriptDividend
   */
  public boolean isScriptDividend() {
    return scriptDividend;
  }

  /**
   * Setter for scriptDividend
   *
   * @param scriptDividend the scriptDividend to set
   */
  public void setScriptDividend(boolean scriptDividend) {
    this.scriptDividend = scriptDividend;
  }

  /**
   * Getter for generalClosed
   *
   * @return the generalClosed
   */
  public boolean isGeneralClosed() {
    return generalClosed;
  }

  /**
   * Setter for generalClosed
   *
   * @param generalClosed the generalClosed to set
   */
  public void setGeneralClosed(boolean generalClosed) {
    this.generalClosed = generalClosed;
  }

  /**
   * Getter for inicioSesion
   *
   * @return the inicioSesion
   */
  public Date getInicioSesion() {
    return inicioSesion;
  }

  /**
   * Setter for inicioSesion
   *
   * @param inicioSesion the inicioSesion to set
   */
  public void setInicioSesion(Date inicioSesion) {
    this.inicioSesion = inicioSesion;
    this.strInicioSesion = FormatDataUtils.convertDateToString(inicioSesion, FormatDataUtils.TIME_FORMAT);
  }

  /**
   * Getter for strInicioSesion
   *
   * @return the strInicioSesion
   */
  public String getStrInicioSesion() {
    return strInicioSesion;
  }

  /**
   * Setter for strInicioSesion
   *
   * @param strInicioSesion the strInicioSesion to set
   */
  public void setStrInicioSesion(String strInicioSesion) {
    this.strInicioSesion = strInicioSesion;
  }

  /**
   * Getter for beforeStart
   *
   * @return the beforeStart
   */
  public boolean isBeforeStart() {
    return beforeStart;
  }

  /**
   * Setter for beforeStart
   *
   * @param beforeStart the beforeStart to set
   */
  public void setBeforeStart(boolean beforeStart) {
    this.beforeStart = beforeStart;
  }

  /**
   * Getter for finSesion
   *
   * @return the finSesion
   */
  public Date getFinSesion() {
    return finSesion;
  }

  /**
   * Setter for finSesion
   *
   * @param finSesion the finSesion to set
   */
  public void setFinSesion(Date finSesion) {
    this.finSesion = finSesion;
    this.strFinSesion = FormatDataUtils.convertDateToString(this.finSesion);
  }

  /**
   * Getter for strFinSesion
   *
   * @return the strFinSesion
   */
  public String getStrFinSesion() {
    return strFinSesion;
  }

  /**
   * Setter for strFinSesion
   *
   * @param strFinSesion the strFinSesion to set
   */
  public void setStrFinSesion(String strFinSesion) {
    this.strFinSesion = strFinSesion;
  }

  /**
   * Getter for userSessionStart
   *
   * @return the userSessionStart
   */
  public boolean isUserSessionStart() {
    return userSessionStart;
  }

  /**
   * Setter for userSessionStart
   *
   * @param userSessionStart the userSessionStart to set
   */
  public void setUserSessionStart(boolean userSessionStart) {
    this.userSessionStart = userSessionStart;
  }

  /**
   * Getter for inicioUserSesion
   *
   * @return the inicioUserSesion
   */
  public Date getInicioUserSesion() {
    return inicioUserSesion;
  }

  /**
   * Setter for inicioUserSesion
   *
   * @param inicioUserSesion the inicioUserSesion to set
   */
  public void setInicioUserSesion(Date inicioUserSesion) {
    this.inicioUserSesion = inicioUserSesion;
    if (this.inicioUserSesion != null) {
      this.strInicioUserSesion = FormatDataUtils.convertDateToString(inicioUserSesion, FormatDataUtils.TIME_FORMAT);
    } else {
      this.strInicioUserSesion = "";
    }
  }

  /**
   * Getter for strInicioUserSesion
   *
   * @return the strInicioUserSesion
   */
  public String getStrInicioUserSesion() {
    return strInicioUserSesion;
  }

  /**
   * Setter for strInicioUserSesion
   *
   * @param strInicioUserSesion the strInicioUserSesion to set
   */
  public void setStrInicioUserSesion(String strInicioUserSesion) {
    this.strInicioUserSesion = strInicioUserSesion;
  }

  /**
   * Getter for userClosedTheSession
   *
   * @return the userClosedTheSession
   */
  public boolean isUserClosedTheSession() {
    return userClosedTheSession;
  }

  /**
   * Setter for userClosedTheSession
   *
   * @param userClosedTheSession the userClosedTheSession to set
   */
  public void setUserClosedTheSession(boolean userClosedTheSession) {
    this.userClosedTheSession = userClosedTheSession;
  }

  /**
   * Getter for opened
   *
   * @return the opened
   */
  public boolean isOpened() {
    return opened;
  }

  /**
   * Setter for opened
   *
   * @param opened the opened to set
   */
  public void setOpened(boolean opened) {
    this.opened = opened;
  }

  /**
   * Getter for userWindowAvailable
   *
   * @return the userWindowAvailable
   */
  public boolean isUserWindowAvailable() {
    return userWindowAvailable;
  }

  /**
   * Setter for userWindowAvailable
   *
   * @param userWindowAvailable the userWindowAvailable to set
   */
  public void setUserWindowAvailable(boolean userWindowAvailable) {
    this.userWindowAvailable = userWindowAvailable;
  }

  @Override
  public String toString() {
    return "SessionStatus [when=" + when + ", weekEnd=" + weekEnd + ", scriptDividend=" + scriptDividend
           + ", generalClosed=" + generalClosed + ", beforeStart=" + beforeStart + ", userSessionStart="
           + userSessionStart + ", userClosedTheSession=" + userClosedTheSession + ", opened=" + opened
           + ", userWindowAvailable=" + userWindowAvailable + ", inicioSesion=" + inicioSesion + ", strInicioSesion="
           + strInicioSesion + ", finSesion=" + finSesion + ", strFinSesion=" + strFinSesion + ", inicioUserSesion="
           + inicioUserSesion + ", strInicioUserSesion=" + strInicioUserSesion + ", status=" + status + ", error="
           + error + "]";
  }

}
