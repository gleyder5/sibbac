package sibbac.business.fase0.database.dto;

import java.math.BigDecimal;
import java.sql.Date;

public class Tmct0cliDTO {
	
	private String cliente;
	private String alias;
	private Date fechaContratacion;
	private String referencia;
	private String isin;
	private String sentido;
	private BigDecimal precio;
	private BigDecimal titulos;
	
	public Tmct0cliDTO(){
		
	}
	
	public Tmct0cliDTO(String cliente, String alias, Date fechaContratacion, String referencia, String isin, String sentido,
					   BigDecimal precio, BigDecimal titulos){
		this.cliente = cliente;
		this.alias = alias;
		this.fechaContratacion = fechaContratacion;
		this.referencia = referencia;
		this.isin = isin;
		this.sentido = sentido;
		this.precio = precio;
		this.titulos = titulos;

	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(Date fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getSentido() {
		return sentido;
	}

	public void setSentido(String sentido) {
		this.sentido = sentido;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos(BigDecimal titulos) {
		this.titulos = titulos;
	}
	
	public static Tmct0cliDTO convertObjectToClienteDTO( Object[] valuesFromQuery){
		
		Tmct0cliDTO dto = new Tmct0cliDTO();
		dto.setCliente((String) valuesFromQuery[0]);
		dto.setAlias((String) valuesFromQuery[1]);
		dto.setFechaContratacion((Date) valuesFromQuery[2]);
		dto.setReferencia((String) valuesFromQuery[3]);
		dto.setIsin((String) valuesFromQuery[4]);
		dto.setSentido(Character.toString((char) valuesFromQuery[5]));
		dto.setPrecio((BigDecimal) valuesFromQuery[6]);
		dto.setTitulos((BigDecimal) valuesFromQuery[7]);
		
		return dto;
	}

}
