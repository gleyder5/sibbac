package sibbac.business.fase0.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.business.fase0.database.model.Tmct0xasId;


@Repository
public interface Tmct0xasDao extends JpaRepository< Tmct0xas, Tmct0xasId > {

	@Query( "select t from Tmct0xas t where nbcamxml = ?1 and inforxml = ?2" )
	List< Tmct0xas > findByNameAndInfor( final String nbcamxml, final String inforxml );
	
	@Query( "select t from Tmct0xas t where nbcamxml = ?1 and nbcamas4 = ?2" )
  List< Tmct0xas > findByNbvamxmlAndNbcamas4( final String nbcamxml, final String nbcamas4 );

	@Query( "select t from Tmct0xas t where nbcamxml = ?1 and inforxml = ?2 and nbcamas4 = ?3 " )
	Tmct0xas findByNameAndInforAndNbcamas( final String nbcamxml, final String inforxml, final String nbcamas4 );

}
