package sibbac.business.wrappers.database.model;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.formula.functions.T;
// import org.apache.poi.ss.formula.functions.T;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.database.DBConstants.WRAPPERS;
import sibbac.database.model.ATableAudited;


/**
 * Entity: "FacturaRectificativa".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table( name = WRAPPERS.FACTURA_RECTIFICATIVA )
@Entity
@XmlRootElement
@Audited
public class FacturaRectificativa extends ATableAudited< FacturaRectificativa > implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1690750624643349753L;

	@OneToOne( fetch = FetchType.LAZY, optional = false )
	@JoinColumn( name = "ID_FACTURA", nullable = false, referencedColumnName = "ID" )
	private Factura				factura;

	@Column( name = "FHFECHAFAC", nullable = false )
	@XmlAttribute
	@Temporal( TemporalType.TIMESTAMP )
	private Date				fechaFactura;

	@Column( name = "NBDOCNUMERO", nullable = false, unique = true )
	@XmlAttribute
	private Long				numeroFactura;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_TIPODOCUMENTO", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private TipoDeDocumento		tipoDeDocumento;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_ESTADOFACTURA", nullable = false, referencedColumnName = "IDESTADO" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Tmct0estado			estado;

	@Column( name = "FHFECHACRE", nullable = false )
	@XmlAttribute
	@Temporal( TemporalType.DATE )
	private Date				fechaCreacion;

	@ManyToOne( optional = false )
	@JoinColumn( name = "ID_MONEDA", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	private Monedas				moneda;
	
	@Column( name = "ENVIADO" )
	@XmlAttribute
	protected Boolean enviado;

	public FacturaRectificativa() {
		super();

		this.fechaCreacion = new Date();

	}

	public Date getFechaInicio() {
		Date fechaInicio = null;
		final Factura factura = getFactura();
		if ( factura != null ) {
			fechaInicio = factura.getFechaInicio();
		}
		return fechaInicio;
	}

	public Date getFechaFin() {
		Date fechaFin = null;
		final Factura factura = getFactura();
		if ( factura != null ) {
			fechaFin = factura.getFechaFin();
		}
		return fechaFin;
	}

	/**
	 * Calcula la base imponible
	 * 
	 * @return
	 */
	public Alias getAlias() {
		Alias alias = null;
		final Factura factura = getFactura();
		if ( factura != null ) {
			alias = factura.getAlias();
		}
		return alias;
	}

	/**
	 * Calcula la base imponible
	 * 
	 * @return
	 */
	public String getComentarios() {
		String comentarios = null;
		final Factura factura = getFactura();
		if ( factura != null ) {
			comentarios = factura.getComentarios();
		}
		return comentarios;
	}

	/**
	 * Calcula la base imponible
	 * 
	 * @return
	 */
	public BigDecimal getImfinsvb() {
		BigDecimal imfinsvb = NumberUtils.createBigDecimal( "0" );
		final Factura factura = getFactura();
		if ( factura != null ) {
			imfinsvb = factura.getImfinsvb();
		}
		return imfinsvb;
	}

	/**
	 * Calcula el importe total de los impuestos
	 * 
	 * @return
	 */
	public BigDecimal getImporteImpuestos() {
		BigDecimal importeImpuestos = NumberUtils.createBigDecimal( "0" );
		final Factura factura = getFactura();
		if ( factura != null ) {
			importeImpuestos = factura.getImporteImpuestos();
		}
		return importeImpuestos;
	}

	/**
	 * Calcula el importe total de la factura (con impuestos)
	 * 
	 * @return
	 */
	public BigDecimal getImporteTotal() {
		final BigDecimal importeTotal = getImfinsvb().add( getImporteImpuestos() );

		return importeTotal;
	}

	// ------------------------------------------- Bean methods: Getters

	public Date getFechaFactura() {
		return this.fechaFactura;
	}

	/**
	 * @return the factura
	 */
	public Factura getFactura() {
		return factura;
	}

	public Long getNumeroFactura() {
		return this.numeroFactura;
	}

	public TipoDeDocumento getTipoDeDocumento() {
		return this.tipoDeDocumento;
	}

	public Tmct0estado getEstado() {
		return this.estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public Monedas getMoneda() {
		return this.moneda;
	}

	public Periodos getPeriodo() {
		Periodos periodo = null;
		if ( factura != null ) {
			periodo = factura.getPeriodo();
		}
		return periodo;
	}

	// ------------------------------------------- Bean methods: Setters

	public void setFechaFactura( Date fechaFactura ) {
		this.fechaFactura = fechaFactura;
	}

	/**
	 * @param factura
	 *            the factura to set
	 */
	public void setFactura( Factura factura ) {
		this.factura = factura;
	}

	public void setNumeroFactura( Long numeroFactura ) {
		this.numeroFactura = numeroFactura;
	}

	public void setTipoDeDocumento( TipoDeDocumento tipoDeDocumento ) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	public void setEstado( Tmct0estado estado ) {
		this.estado = estado;
	}

	public void setFechaCreacion( Date fechaCreacion ) {
		this.fechaCreacion = fechaCreacion;
	}

	public void setMoneda( Monedas moneda ) {
		this.moneda = moneda;
	}
	
	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}

	// ------------------------------------------------ Business Methods

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	@Override
	public int compareTo( final FacturaRectificativa other ) {
		// Si el "padre" ya es distinto...
		int father = super.compareTo( other );
		if ( father != 0 ) {
			return father;
		}

		// Entity concrete fields.
		int internal = 0;
		return ( internal == 0 ) ? 0 : -3;
	}

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[FACTURA RECTIFICATIVA==" + this.id + "]";
	}

}
