package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;
@Entity
@Table(name=DBConstants.WRAPPERS.SISTEMA_LIQUIDACION)
public class SistemaLiquidacion extends ATable<SistemaLiquidacion> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Column(name="CODIGO")
    private String codigo;
    @Column(name="DESCRIPCION")
    private String descripcion;
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    

}
