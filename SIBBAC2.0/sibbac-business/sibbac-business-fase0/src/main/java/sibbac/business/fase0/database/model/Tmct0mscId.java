package sibbac.business.fase0.database.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Embeddable
public class Tmct0mscId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 2007069073628210771L;
	private String				cdmensa				= "";
	private Date				femensa				= new Date();
	private int					nuenvio				= 0;

	public Tmct0mscId() {
	}

	public Tmct0mscId( String cdmensa, Date femensa, int nuenvio ) {
		this.cdmensa = cdmensa;
		this.femensa = femensa;
		this.nuenvio = nuenvio;
	}

	@Column( name = "CDMENSA", nullable = false, length = 3 )
	public String getCdmensa() {
		return this.cdmensa;
	}

	public void setCdmensa( String cdmensa ) {
		this.cdmensa = cdmensa;
	}

	@Temporal( TemporalType.DATE )
	@Column( name = "FEMENSA", nullable = false )
	public Date getFemensa() {
		return this.femensa;
	}

	public void setFemensa( Date femensa ) {
		this.femensa = femensa;
	}

	@Column( name = "NUENVIO", nullable = false, precision = 5 )
	public int getNuenvio() {
		return this.nuenvio;
	}

	public void setNuenvio( int nuenvio ) {
		this.nuenvio = nuenvio;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tmct0mscId ) )
			return false;
		Tmct0mscId castOther = ( Tmct0mscId ) other;

		return ( ( this.getCdmensa() == castOther.getCdmensa() ) || ( this.getCdmensa() != null && castOther.getCdmensa() != null && this
				.getCdmensa().equals( castOther.getCdmensa() ) ) )
				&& ( ( this.getFemensa() == castOther.getFemensa() ) || ( this.getFemensa() != null && castOther.getFemensa() != null && this
						.getFemensa().equals( castOther.getFemensa() ) ) ) && ( this.getNuenvio() == castOther.getNuenvio() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getCdmensa() == null ? 0 : this.getCdmensa().hashCode() );
		result = 37 * result + ( getFemensa() == null ? 0 : this.getFemensa().hashCode() );
		result = 37 * result + this.getNuenvio();
		return result;
	}

}
