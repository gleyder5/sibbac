package sibbac.business.canones.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MercadoContratacionTipoOperacionBolsaId extends MercadoContratacionRefId {

  /**
   * Número de serie
   */
  private static final long serialVersionUID = -3013106344691480955L;

  @Column(name = MercadoContratacion.ID_COLUMN_NAME, length = 4, nullable = false)
  public String idField;
  
  @Override
  public String getIdField() {
    return idField;
  }
  
  @Override
  public void setIdField(String idField) {
    this.idField = idField;
  }

}
