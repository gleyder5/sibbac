package sibbac.business.fase0.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0dbs;

@Repository
public interface Tmct0dbsDao extends JpaRepository<Tmct0dbs, Long> {

  @Query(value = "select d from Tmct0dbs d where d.ntabla = :ntabla and d.fecha = :fecha "
      + " and d.nuoprout = :nuoprout and d.nupareje = :nupareje " + " and d.nsec1 = :nusec1 and d.nsec2 = :nusec2 "
      + " order by d.iddbs desc ")
  List<Tmct0dbs> findAllByNtablaAndFechaAndNuoproutAndNuparejeAndNsec1AndNsec2(@Param("ntabla") String tabla,
      @Param("fecha") Date fecha, @Param("nuoprout") int nuoprout, @Param("nupareje") short nupareje,
      @Param("nusec1") int nusec1, @Param("nusec2") int nusec2);

  @Query(value = "select d from Tmct0dbs d where d.ntabla = :ntabla and d.fecha = :fecha "
      + " and d.auxiliar = :auxiliar order by d.iddbs desc ")
  List<Tmct0dbs> findAllByNtablaAndFechaAndAuxiliar(@Param("ntabla") String tabla, @Param("fecha") Date fecha,
      @Param("auxiliar") String auxiliar);

}
