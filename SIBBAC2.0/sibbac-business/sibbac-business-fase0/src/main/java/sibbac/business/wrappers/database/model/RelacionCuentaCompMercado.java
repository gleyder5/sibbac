package sibbac.business.wrappers.database.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;

@Table(name=DBConstants.WRAPPERS.RELACION_CUENTACOMP_MERCADO)
@Entity
public class RelacionCuentaCompMercado implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private RelacionCuentaCompMercadoPK relacionCuentaCompMercadoPK;
}
