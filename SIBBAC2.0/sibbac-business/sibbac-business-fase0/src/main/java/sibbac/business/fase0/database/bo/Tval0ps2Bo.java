package sibbac.business.fase0.database.bo;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnvasql.dao.Tval0ps2Dao;
import sibbac.database.bsnvasql.model.Tval0ps1;
import sibbac.database.bsnvasql.model.Tval0ps2;


@Service
public class Tval0ps2Bo extends AbstractBo< Tval0ps2, String, Tval0ps2Dao > {

	public Tval0ps2 findByCdispais( String cdispais ) {
		Tval0ps2 pais;
		try {
			pais = dao.findOne( cdispais );
		} catch ( Exception e ) {
			pais = null;
		}
		return pais;
	}

	public List< Tval0ps2 > findAllActiveOrderByCdiso2po() {
		return dao.findAllActiveOrderByCdiso2po();
	}

	public Tval0ps2 findActiveByCdiso2po( String cdiso2po ) {
		return dao.findActiveByCdiso2po( cdiso2po );
	}

	/**
	 * Obtiene un pais en una entidad Tval0ps2 recibiendo una Tval0ps1 por medio del codigo de 2 posiciones
	 * @param tval0ps1
	 * @return Devuelve el pais en Tval0ps2 si puede encontrarlo o null si no puede
	 */
	public Tval0ps2 getTval0ps2FromTval0ps1( Tval0ps1 tval0ps1 ) {
		try {
			return dao.findActiveByCdiso2po( tval0ps1.getCdiso2po() );
		} catch ( Exception e ) {
			return null;
		}
	}

	public List< String > findAllCodigosActivos() {
		List<String> codigos = new ArrayList< String >();
		try{
			List<Object> resultados =  this.dao.findAllCodigosActivos();
			for(Object resultado:resultados){
				codigos.add( (String) resultado );
			}
		}catch(Exception e){
			LOG.error( "Tval0ps2Bo -- Error creando lista de códigos de países "+e.getMessage() );
		}
		return codigos;
	}
	/**
	 * Obtiene el código de un pais dado en estandar ISO, a codigo pais estandar Banco de España
	 * @param codIsoPais
	 * @return String
	 */
	public String castCodigoPaisFromISOToSpanishBank(String codIsoPais){
	    String codPais = "";
	    Object resultCod = dao.castCodigoPaisFromISOToSpanishBank(codIsoPais);
	    if(resultCod != null){
		codPais = String.valueOf(resultCod);
	    }
	    return codPais;
	}

}
