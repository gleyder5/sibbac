package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0DesgloseTitular;

public class DesgloseTitularData implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private Date auditDate;
    private String auditUser;
    private Character cdestado;
    private String idholder;
    private Character tptiprep;
    private Character tpidenti;
    private String nbidenti;
    private String nbclient;
    private String nbclient1;
    private String nbclient2;
    private String cdnactit;
    private Character tpsocied;
    private Character tpnactit;
    private BigDecimal particip;
    private String tpdomici;
    private String nbdomici;
    private String nudomici;
    private String nbciudad;
    private String nbprovin;
    private String cdpostal;
    private String cddepais;
    private Long idDesglose;
    
    public DesgloseTitularData(){
	//
    }
    
    public DesgloseTitularData(Long id, Date auditDate, String auditUser,
	    Character cdestado, String idholder, Character tptiprep,
	    Character tpidenti, String nbidenti, String nbclient,
	    String nbclient1, String nbclient2, String cdnactit,
	    Character tpsocied, Character tpnactit, BigDecimal particip,
	    String tpdomici, String nbdomici, String nudomici, String nbciudad,
	    String nbprovin, String cdpostal, String cddepais, Long idDesglose) {
	super();
	this.id = id;
	this.auditDate = auditDate;
	this.auditUser = auditUser;
	this.cdestado = cdestado;
	this.idholder = idholder;
	this.tptiprep = tptiprep;
	this.tpidenti = tpidenti;
	this.nbidenti = nbidenti;
	this.nbclient = nbclient;
	this.nbclient1 = nbclient1;
	this.nbclient2 = nbclient2;
	this.cdnactit = cdnactit;
	this.tpsocied = tpsocied;
	this.tpnactit = tpnactit;
	this.particip = particip;
	this.tpdomici = tpdomici;
	this.nbdomici = nbdomici;
	this.nudomici = nudomici;
	this.nbciudad = nbciudad;
	this.nbprovin = nbprovin;
	this.cdpostal = cdpostal;
	this.cddepais = cddepais;
	this.idDesglose = idDesglose;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Date getAuditDate() {
        return auditDate;
    }
    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }
    public String getAuditUser() {
        return auditUser;
    }
    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }
    public Character getCdestado() {
        return cdestado;
    }
    public void setCdestado(Character cdestado) {
        this.cdestado = cdestado;
    }
    public String getIdholder() {
        return idholder;
    }
    public void setIdholder(String idholder) {
        this.idholder = idholder;
    }
    public Character getTptiprep() {
        return tptiprep;
    }
    public void setTptiprep(Character tptiprep) {
        this.tptiprep = tptiprep;
    }
    public Character getTpidenti() {
        return tpidenti;
    }
    public void setTpidenti(Character tpidenti) {
        this.tpidenti = tpidenti;
    }
    public String getNbidenti() {
        return nbidenti;
    }
    public void setNbidenti(String nbidenti) {
        this.nbidenti = nbidenti;
    }
    public String getNbclient() {
        return nbclient;
    }
    public void setNbclient(String nbclient) {
        this.nbclient = nbclient;
    }
    public String getNbclient1() {
        return nbclient1;
    }
    public void setNbclient1(String nbclient1) {
        this.nbclient1 = nbclient1;
    }
    public String getNbclient2() {
        return nbclient2;
    }
    public void setNbclient2(String nbclient2) {
        this.nbclient2 = nbclient2;
    }
    public String getCdnactit() {
        return cdnactit;
    }
    public void setCdnactit(String cdnactit) {
        this.cdnactit = cdnactit;
    }
    public Character getTpsocied() {
        return tpsocied;
    }
    public void setTpsocied(Character tpsocied) {
        this.tpsocied = tpsocied;
    }
    public Character getTpnactit() {
        return tpnactit;
    }
    public void setTpnactit(Character tpnactit) {
        this.tpnactit = tpnactit;
    }
    public BigDecimal getParticip() {
        return particip;
    }
    public void setParticip(BigDecimal particip) {
        this.particip = particip;
    }
    public String getTpdomici() {
        return tpdomici;
    }
    public void setTpdomici(String tpdomici) {
        this.tpdomici = tpdomici;
    }
    public String getNbdomici() {
        return nbdomici;
    }
    public void setNbdomici(String nbdomici) {
        this.nbdomici = nbdomici;
    }
    public String getNudomici() {
        return nudomici;
    }
    public void setNudomici(String nudomici) {
        this.nudomici = nudomici;
    }
    public String getNbciudad() {
        return nbciudad;
    }
    public void setNbciudad(String nbciudad) {
        this.nbciudad = nbciudad;
    }
    public String getNbprovin() {
        return nbprovin;
    }
    public void setNbprovin(String nbprovin) {
        this.nbprovin = nbprovin;
    }
    public String getCdpostal() {
        return cdpostal;
    }
    public void setCdpostal(String cdpostal) {
        this.cdpostal = cdpostal;
    }
    public String getCddepais() {
        return cddepais;
    }
    public void setCddepais(String cddepais) {
        this.cddepais = cddepais;
    }
    public Long getIdDesglose() {
        return idDesglose;
    }
    public void setIdDesglose(Long idDesglose) {
        this.idDesglose = idDesglose;
    }
    
    public static DesgloseTitularData entityToData(Tmct0DesgloseTitular entity){
	DesgloseTitularData data = new DesgloseTitularData();
	data.setCddepais(entity.getCddepais());
	data.setCdestado(entity.getCdestado());
	data.setCdnactit(entity.getCdnactit());
	data.setCdpostal(entity.getCdpostal());
	data.setId(entity.getId());
	data.setIdDesglose(entity.getId());
	data.setIdholder(entity.getIdholder());
	data.setNbciudad(entity.getNbciudad());
	data.setNbclient(entity.getNbclient());
	data.setNbclient1(entity.getNbclient1());
	data.setNbclient2(entity.getNbclient2());
	data.setNbdomici(entity.getNbdomici());
	data.setNbidenti(entity.getNbidenti());
	data.setNbprovin(entity.getNbprovin());
	data.setNudomici(entity.getNudomici());
	data.setParticip(entity.getParticip());
	data.setTpdomici(entity.getTpdomici());
	data.setTpidenti(entity.getTpidenti());
	data.setTpnactit(entity.getTpnactit());
	data.setTpsocied(entity.getTpsocied());
	data.setTptiprep(entity.getTptiprep());
	
	return data;
    }
    
    public static List<DesgloseTitularData> entityListToDataList(
	    List<Tmct0DesgloseTitular> tmct0DesgloseTitularList) {
	List<DesgloseTitularData>dataList = new LinkedList<DesgloseTitularData>();
	for(Tmct0DesgloseTitular entity : tmct0DesgloseTitularList){
	    DesgloseTitularData data = entityToData(entity);
	    dataList.add(data);
	}
	return dataList;
    }
    
    
}
