package sibbac.business.fase0.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0Mensajes;


@Repository
public interface Tmct0MensajesDao extends JpaRepository< Tmct0Mensajes, Long > {
}
