package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;

public class Tmct0AloData implements Serializable {
  /**
     * 
     */
  private static final long serialVersionUID = 1L;
  private Tmct0aloId id;
  private Tmct0bokId tmct0bokId;
  private Tmct0EstadoData tmct0estadoByCdestadoasig;
  private Tmct0EstadoData tmct0estadoByCdestadocont;
  private String cdalias = " ";
  private String descrali = " ";
  private String cdusuges = " ";
  private Date fevalor = new Date();
  private Date feoperac = new Date();
  private Date hooperac = new Date();
  private Character cdtpoper = ' ';
  private String nbtitulr = " ";
  private BigDecimal nutitcli = new BigDecimal(0.0);
  private BigDecimal nutitpen = new BigDecimal(0.0);
  private String cdisin = " ";
  private String nbvalors = " ";
  private Character cdenvcnf = ' ';
  private Character cdenvoas = ' ';
  private Character cdenvseq = ' ';
  private Character cdestoas = ' ';
  private String cdrefoas = " ";
  private Date fhenvoas = new Date();
  private Date hoenvoas = new Date();
  private Date fhenvseq = new Date();
  private Date hoenvseq = new Date();
  private String cdcustod = " ";
  private String acctatcus = " ";
  private String cdbencus = " ";
  private String acbencus = " ";
  private String idbencus = " ";
  private String gcuscode = " ";
  private String gcusacct = " ";
  private String gcusid = " ";
  private String lcuscode = " ";
  private String lcusacct = " ";
  private String lcusid = " ";
  private String pccusset = " ";
  private Character sendbecus = ' ';
  private String tpsetcus = " ";
  private String tysetcus = " ";
  private String cdcleare = " ";
  private String acctatcle = " ";
  private String cdbencle = " ";
  private String acbencle = " ";
  private String idbencle = " ";
  private String gclecode = " ";
  private String gcleacct = " ";
  private String gcleid = " ";
  private String lclecode = " ";
  private String lcleacct = " ";
  private String lcleid = " ";
  private String pccleset = " ";
  private Character sendbecle = ' ';
  private String tpsetcle = " ";
  private String tysetcle = " ";
  private BigDecimal imcammed = new BigDecimal(0.0);
  private BigDecimal imcamnet = new BigDecimal(0.0);
  private BigDecimal imtotbru = new BigDecimal(0.0);
  private BigDecimal imtotnet = new BigDecimal(0.0);
  private BigDecimal eutotbru = new BigDecimal(0.0);
  private BigDecimal eutotnet = new BigDecimal(0.0);
  private BigDecimal imnetbrk = new BigDecimal(0.0);
  private BigDecimal eunetbrk = new BigDecimal(0.0);
  private BigDecimal imcomisn = new BigDecimal(0.0);
  private BigDecimal imcomieu = new BigDecimal(0.0);
  private BigDecimal pccomisn = new BigDecimal(0.0);
  private BigDecimal imcomdev = new BigDecimal(0.0);
  private BigDecimal eucomdev = new BigDecimal(0.0);
  private BigDecimal imfibrdv = new BigDecimal(0.0);
  private BigDecimal imfibreu = new BigDecimal(0.0);
  private BigDecimal imsvb = new BigDecimal(0.0);
  private BigDecimal imsvbeu = new BigDecimal(0.0);
  private BigDecimal imtpcamb = new BigDecimal(0.0);
  private String cdmoniso = " ";
  private Character cdindgas = ' ';
  private Character cdindimp = ' ';
  private Character cdreside = ' ';
  private BigDecimal imbrmerc = new BigDecimal(0.0);
  private BigDecimal imbrmreu = new BigDecimal(0.0);
  private BigDecimal imgastos = new BigDecimal(0.0);
  private BigDecimal imgatdvs = new BigDecimal(0.0);
  private BigDecimal imimpdvs = new BigDecimal(0.0);
  private BigDecimal imimpeur = new BigDecimal(0.0);
  private BigDecimal imcconeu = new BigDecimal(0.0);
  private BigDecimal imcliqeu = new BigDecimal(0.0);
  private BigDecimal imdereeu = new BigDecimal(0.0);
  private Character xtdcm = ' ';
  private Character xtderech = ' ';
  private String rfparten = " ";
  private Character tpcalcul = ' ';
  private Character indatfis = ' ';
  private String erdatfis = " ";
  private Character cddatliq = ' ';
  private Character cdbloque = ' ';
  private Integer nuoprout = 0;
  private Short nupareje = 0;
  private String dsobserv = " ";
  private String cdusublo = " ";
  private String cdusuaud = " ";
  private Date fhaudit = new Date();
  private Integer nuversion = 0;
  private Integer nuagrpkb = 0;
  private String pkentity = " ";
  private String biccus = " ";
  private String namecus = " ";
  private String lcusbic = " ";
  private String cdloccus = " ";
  private String gcusbic = " ";
  private String cdglocus = " ";
  private String bcusbic = " ";
  private String cdnbcus = " ";
  private String biccle = " ";
  private String namecle = " ";
  private String lclebic = " ";
  private String cdloccle = " ";
  private String gclebic = " ";
  private String cdglocle = " ";
  private String bclebic = " ";
  private String cdnbcle = " ";
  private Date fevalorr = new Date();
  private BigDecimal imliq = new BigDecimal(0.0);
  private BigDecimal imeurliq = new BigDecimal(0.0);
  private BigDecimal imajliq = new BigDecimal(0.0);
  private BigDecimal imajeliq = new BigDecimal(0.0);
  private BigDecimal imgasliqdv;
  private BigDecimal imgasliqeu;
  private BigDecimal imgasgesdv;
  private BigDecimal imgasgeseu;
  private BigDecimal imgastotdv;
  private BigDecimal imgastoteu;
  private String refbanif;
  private String refcltdes;
  private String cdholder;
  private Integer cdestadotit;
  private String reftitular;
  private BigDecimal imcorretajesvfinaldv = new BigDecimal(0.0);
  private BigDecimal imcomclteeu = new BigDecimal(0.0);
  private Integer canoncontrpagoclte;
  private Integer canoncontrpagomkt;
  private Integer canonliqpagoclte;
  private Integer canonliqpagomkt;
  private Integer canoncomppagoclte;
  private Integer canoncomppagomkt;
  private BigDecimal imcanoncompdv;
  private BigDecimal imcanoncompeu;
  private BigDecimal imcanoncontrdv;
  private BigDecimal imcanoncontreu;
  private BigDecimal imcanonliqdv;
  private BigDecimal imcanonliqeu;
  private List<Tmct0AlcData> tmct0AlcDataList;
  private Tmct0staData tmct0sta;
  private String refCliente;
  
  /* Constants */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0AloData.class);

  /**/
  private static boolean initializedByConstructor = false;

  public Tmct0AloData() {
    LOG.debug("Tmct0AloData cONSTRUCTOR POR DEFECTO");
  }

  public Tmct0AloData(Tmct0alo entity) {
    LOG.debug("[Tmct0AloData (Tmct0alo entity)] Inicio ...");
    /* insertar alcs */
    try {
      insertAlcs(entity.getTmct0alcs(), this);
    } catch (PersistenceException ex) {
      LOG.error("[Tmct0AloData Mensaje]" + ex.getMessage() + " cause: " + ex.getCause() + " Id: " + entity.getId(), ex);
    } catch (Exception e) {
      LOG.error("[Tmct0AloData Excepcion generica]" + e.getMessage() + " cause: " + e.getCause() + " Id: "
                    + entity.getId(), e);
    }
    initializedByConstructor = true;
    entityToData(entity, this);
    LOG.debug("[Tmct0AloData (Tmct0alo entity)] Fin ...");
  }

  public Tmct0AloData(Tmct0alo entity, boolean onlyLoadAloData) {
    LOG.debug("[Tmct0AloData (Tmct0alo entity)] Inicio ...");
    if (!onlyLoadAloData) {
      /* insertar alcs */
      try {

        insertAlcs(entity.getTmct0alcs(), this);
      } catch (PersistenceException ex) {
        LOG.error("[Tmct0AloData Mensaje]" + ex.getMessage() + " cause: " + ex.getCause() + " Id: " + entity.getId(),
                  ex);
      } catch (Exception e) {
        LOG.error("[Tmct0AloData Excepcion generica]" + e.getMessage() + " cause: " + e.getCause() + " Id: "
                      + entity.getId(), e);
      }
    }
    initializedByConstructor = true;
    entityToData(entity, this, onlyLoadAloData);
    LOG.debug("[Tmct0AloData (Tmct0alo entity)] Fin ...");
  }

  public Tmct0AloData(Tmct0alo entity, Collection<Tmct0alc> alcsCollection) {
    insertAlcsAndParseEntityToData(entity, alcsCollection);

  }

  public Tmct0AloData(Tmct0alo entity,
                      Collection<Tmct0alc> alcsCollection,
                      Tmct0estado tmct0estadoByCdestadoasig,
                      Tmct0estado tmct0estadoByCdestadocont) {
    if (tmct0estadoByCdestadoasig != null) {
      setTmct0estadoByCdestadoasig(Tmct0EstadoData.entityToData(tmct0estadoByCdestadoasig));
    }
    if (tmct0estadoByCdestadocont != null) {
      setTmct0estadoByCdestadocont(Tmct0EstadoData.entityToData(tmct0estadoByCdestadocont));
    }
    initializedByConstructor = true;
    insertAlcsAndParseEntityToData(entity, alcsCollection);
  }

  private void insertAlcsAndParseEntityToData(Tmct0alo entity, Collection<Tmct0alc> alcsCollection) {
    if (alcsCollection != null) {
      List<Tmct0alc> alcs = new LinkedList<Tmct0alc>(alcsCollection);
      entityToData(entity, alcs, this);
    } else {
      entityToData(entity, this);
    }
  }

  public static Tmct0AloData entityToData(Tmct0alo entity) {
    Tmct0AloData data = new Tmct0AloData();
    entityToData(entity, data);
    return data;
  }

  public static void entityToData(Tmct0alo entity, Tmct0AloData data, boolean loadOnlyAloData) {
    LOG.debug("[entityToData(Tmct0alo entity, Tmct0AloData data)] Inicio ...");
    if (data == null) {
      data = new Tmct0AloData();
    }
    if (entity != null) {
      data.setAcbencle(entity.getAcbencle());
      data.setAcbencus(entity.getAcbencus());
      data.setAcctatcle(entity.getAcctatcle());
      data.setAcctatcus(entity.getAcctatcus());
      data.setBclebic(entity.getBclebic());
      data.setBcusbic(entity.getBcusbic());
      data.setBiccle(entity.getBiccle());
      data.setBiccus(entity.getBiccus());
      data.setCanoncomppagoclte(entity.getCanoncomppagoclte());
      data.setCanoncomppagomkt(entity.getCanoncomppagomkt());
      data.setCanoncontrpagoclte(entity.getCanoncontrpagoclte());
      data.setCanoncontrpagomkt(entity.getCanoncontrpagomkt());
      data.setCanonliqpagoclte(entity.getCanonliqpagoclte());
      data.setCanonliqpagomkt(entity.getCanonliqpagomkt());
      data.setCdalias(entity.getCdalias());
      data.setCdbencle(entity.getCdbencle());
      data.setCdbencus(entity.getCdbencus());
      data.setCdbloque(entity.getCdbloque());
      data.setCdcleare(entity.getCdcleare());
      data.setCdcustod(entity.getCdcustod());
      data.setCddatliq(entity.getCddatliq());
      data.setCdenvcnf(entity.getCdenvcnf());
      data.setCdenvoas(entity.getCdenvoas());
      data.setCdenvoas(entity.getCdenvoas());
      data.setCdenvseq(entity.getCdenvseq());
      data.setCdestadotit(entity.getCdestadotit());
      data.setCdestoas(entity.getCdestoas());
      data.setCdglocle(entity.getCdglocle());
      data.setCdglocus(entity.getCdglocle());
      data.setCdholder(entity.getCdholder());
      data.setCdindgas(entity.getCdindgas());
      data.setCdindimp(entity.getCdindimp());
      data.setCdisin(entity.getCdisin());
      data.setCdloccle(entity.getCdloccle());
      data.setCdloccus(entity.getCdloccus());
      data.setCdmoniso(entity.getCdmoniso());
      data.setCdnbcle(entity.getCdnbcle());
      data.setCdnbcus(entity.getCdnbcus());
      data.setCdrefoas(entity.getCdrefoas());
      data.setCdreside(entity.getCdreside());
      data.setCdtpoper(entity.getCdtpoper());
      data.setCdusuaud(entity.getCdusuaud());
      data.setCdusublo(entity.getCdusublo());
      data.setCdusuges(entity.getCdusuges());
      data.setDescrali(entity.getDescrali());
      data.setDsobserv(entity.getDsobserv());
      data.setErdatfis(entity.getErdatfis());
      data.setEucomdev(entity.getEucomdev());
      data.setEunetbrk(entity.getEunetbrk());
      data.setEutotbru(entity.getEutotbru());
      data.setEutotnet(entity.getEutotnet());
      data.setFeoperac(entity.getFeoperac());
      data.setFevalor(entity.getFevalor());
      data.setFevalorr(entity.getFevalorr());
      data.setFevalor(entity.getFevalor());
      data.setFhenvoas(entity.getFhenvoas());
      data.setFhenvseq(entity.getFhenvseq());
      data.setGcleacct(entity.getGcleacct());
      data.setGclebic(entity.getGclebic());
      data.setGclecode(entity.getGclecode());
      data.setGcleid(entity.getGcleid());
      data.setGcusacct(entity.getGcusacct());
      data.setGcusbic(entity.getGcusbic());
      data.setGcuscode(entity.getGcuscode());
      data.setGcusid(entity.getGcusid());
      data.setHoenvoas(entity.getHoenvoas());
      data.setHoenvseq(entity.getHoenvseq());
      data.setHooperac(entity.getHooperac());
      data.setId(entity.getId());
      data.setIdbencle(entity.getIdbencle());
      data.setIdbencus(entity.getIdbencus());
      /* Im */
      data.setImajeliq(entity.getImajeliq());
      data.setImajliq(entity.getImajliq());
      data.setImbrmerc(entity.getImbrmerc());
      data.setImbrmreu(entity.getImbrmreu());
      data.setImcammed(entity.getImcammed());
      data.setImcamnet(entity.getImcamnet());
      data.setImcanoncompdv(entity.getImcanoncompdv());
      data.setImcanoncompeu(entity.getImcanoncompeu());
      data.setImcanoncontrdv(entity.getImcanoncontrdv());
      data.setImcanoncontreu(entity.getImcanoncontreu());
      data.setImcanonliqdv(entity.getImcanonliqdv());
      data.setImcanonliqeu(entity.getImcanonliqeu());
      data.setImcconeu(entity.getImcconeu());
      data.setImcliqeu(entity.getImcliqeu());
      data.setImcomclteeu(entity.getImcomclteeu());
      data.setImcomdev(entity.getImcomdev());
      data.setImcomieu(entity.getImcomieu());
      data.setImcomisn(entity.getImcomisn());
      data.setImcorretajesvfinaldv(entity.getImcorretajesvfinaldv());
      data.setImdereeu(entity.getImdereeu());
      data.setImeurliq(entity.getImeurliq());
      data.setImfibrdv(entity.getImfibrdv());
      data.setImfibreu(entity.getImfibreu());
      /**/
      data.setImgasgesdv(entity.getImgasgesdv());
      data.setImgasgeseu(entity.getImgasgeseu());
      data.setImgasliqdv(entity.getImgasliqdv());
      data.setImgasliqeu(entity.getImgasliqeu());
      data.setImgastos(entity.getImgastos());
      data.setImgastotdv(entity.getImgastotdv());
      data.setImgastoteu(entity.getImgastoteu());
      data.setImgatdvs(entity.getImgatdvs());
      //
      data.setImimpdvs(entity.getImimpdvs());
      data.setImimpeur(entity.getImimpeur());
      data.setImliq(entity.getImliq());
      data.setImnetbrk(entity.getImnetbrk());
      data.setImsvb(entity.getImsvb());
      data.setImsvbeu(entity.getImsvbeu());
      data.setImtotbru(entity.getImtotbru());
      data.setImtotnet(entity.getImtotnet());
      data.setImtpcamb(entity.getImtpcamb());
      /**/
      data.setLcleacct(entity.getLcleacct());
      data.setLclebic(entity.getLclebic());
      data.setLclecode(entity.getLclecode());
      data.setLcleid(entity.getLcleid());
      data.setLcusacct(entity.getLcusacct());
      data.setLcusbic(entity.getLcusbic());
      data.setLcuscode(entity.getLcuscode());
      data.setLcusid(entity.getLcusid());
      /**/
      data.setNamecle(entity.getNamecle());
      data.setNamecus(entity.getNamecus());
      data.setNbtitulr(entity.getNbtitulr());
      data.setNbvalors(entity.getNbvalors());
      data.setNuagrpkb(entity.getNuagrpkb());
      data.setNuoprout(entity.getNuoprout());
      data.setNupareje(entity.getNupareje());
      data.setNutitcli(entity.getNutitcli());
      data.setNutitpen(entity.getNutitpen());
      data.setNuversion(entity.getNuversion());
      if (!loadOnlyAloData) {

        if (data.getTmct0estadoByCdestadoasig() == null) {
          try {
            data.setTmct0estadoByCdestadoasig(Tmct0EstadoData.entityToData(entity.getTmct0estadoByCdestadoasig()));
          } catch (Exception ex) {
            LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
          }
        }
        if (data.getTmct0estadoByCdestadocont() == null) {
          try {
            data.setTmct0estadoByCdestadocont(Tmct0EstadoData.entityToData(entity.getTmct0estadoByCdestadocont()));
          } catch (Exception ex) {
            LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
          }
        }

        if (entity.getTmct0sta() != null) {
          try {
            data.setTmct0sta(Tmct0staData.entityToData(entity.getTmct0sta()));
          } catch (Exception ex) {
            LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
          }
        }
        /*
         * si los estados no han sido seteados en el constructor eso es que se usa este método desde otra clase
         */
        if (!initializedByConstructor) {
          try {
            insertAlcs(entity.getTmct0alcs(), data);
          } catch (Exception ex) {
            LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
          }
        }
      }
      initializedByConstructor = false;
    }

    /**/
    try {
      data.setTmct0bokId(entity.getTmct0bok().getId());
    } catch (Exception ex) {
      LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
    }
    data.setTpcalcul(entity.getTpcalcul());
    data.setTpsetcus(entity.getTpsetcus());
    data.setTpsetcle(entity.getTpsetcle());
    data.setTysetcle(entity.getTysetcle());
    data.setTysetcus(entity.getTysetcus());
    data.setXtdcm(entity.getXtdcm());
    data.setXtderech(entity.getXtderech());
    data.setRefCliente(entity.getRefCliente());

  }

  public static void entityToData(Tmct0alo entity, Tmct0AloData data) {
    LOG.debug("[entityToData(Tmct0alo entity, Tmct0AloData data)] Inicio ...");
    if (data == null) {
      data = new Tmct0AloData();
    }
    if (entity != null) {
      data.setAcbencle(entity.getAcbencle());
      data.setAcbencus(entity.getAcbencus());
      data.setAcctatcle(entity.getAcctatcle());
      data.setAcctatcus(entity.getAcctatcus());
      data.setBclebic(entity.getBclebic());
      data.setBcusbic(entity.getBcusbic());
      data.setBiccle(entity.getBiccle());
      data.setBiccus(entity.getBiccus());
      data.setCanoncomppagoclte(entity.getCanoncomppagoclte());
      data.setCanoncomppagomkt(entity.getCanoncomppagomkt());
      data.setCanoncontrpagoclte(entity.getCanoncontrpagoclte());
      data.setCanoncontrpagomkt(entity.getCanoncontrpagomkt());
      data.setCanonliqpagoclte(entity.getCanonliqpagoclte());
      data.setCanonliqpagomkt(entity.getCanonliqpagomkt());
      data.setCdalias(entity.getCdalias());
      data.setCdbencle(entity.getCdbencle());
      data.setCdbencus(entity.getCdbencus());
      data.setCdbloque(entity.getCdbloque());
      data.setCdcleare(entity.getCdcleare());
      data.setCdcustod(entity.getCdcustod());
      data.setCddatliq(entity.getCddatliq());
      data.setCdenvcnf(entity.getCdenvcnf());
      data.setCdenvoas(entity.getCdenvoas());
      data.setCdenvoas(entity.getCdenvoas());
      data.setCdenvseq(entity.getCdenvseq());
      data.setCdestadotit(entity.getCdestadotit());
      data.setCdestoas(entity.getCdestoas());
      data.setCdglocle(entity.getCdglocle());
      data.setCdglocus(entity.getCdglocle());
      data.setCdholder(entity.getCdholder());
      data.setCdindgas(entity.getCdindgas());
      data.setCdindimp(entity.getCdindimp());
      data.setCdisin(entity.getCdisin());
      data.setCdloccle(entity.getCdloccle());
      data.setCdloccus(entity.getCdloccus());
      data.setCdmoniso(entity.getCdmoniso());
      data.setCdnbcle(entity.getCdnbcle());
      data.setCdnbcus(entity.getCdnbcus());
      data.setCdrefoas(entity.getCdrefoas());
      data.setCdreside(entity.getCdreside());
      data.setCdtpoper(entity.getCdtpoper());
      data.setCdusuaud(entity.getCdusuaud());
      data.setCdusublo(entity.getCdusublo());
      data.setCdusuges(entity.getCdusuges());
      data.setDescrali(entity.getDescrali());
      data.setDsobserv(entity.getDsobserv());
      data.setErdatfis(entity.getErdatfis());
      data.setEucomdev(entity.getEucomdev());
      data.setEunetbrk(entity.getEunetbrk());
      data.setEutotbru(entity.getEutotbru());
      data.setEutotnet(entity.getEutotnet());
      data.setFeoperac(entity.getFeoperac());
      data.setFevalor(entity.getFevalor());
      data.setFevalorr(entity.getFevalorr());
      data.setFevalor(entity.getFevalor());
      data.setFhenvoas(entity.getFhenvoas());
      data.setFhenvseq(entity.getFhenvseq());
      data.setGcleacct(entity.getGcleacct());
      data.setGclebic(entity.getGclebic());
      data.setGclecode(entity.getGclecode());
      data.setGcleid(entity.getGcleid());
      data.setGcusacct(entity.getGcusacct());
      data.setGcusbic(entity.getGcusbic());
      data.setGcuscode(entity.getGcuscode());
      data.setGcusid(entity.getGcusid());
      data.setHoenvoas(entity.getHoenvoas());
      data.setHoenvseq(entity.getHoenvseq());
      data.setHooperac(entity.getHooperac());
      data.setId(entity.getId());
      data.setIdbencle(entity.getIdbencle());
      data.setIdbencus(entity.getIdbencus());
      /* Im */
      data.setImajeliq(entity.getImajeliq());
      data.setImajliq(entity.getImajliq());
      data.setImbrmerc(entity.getImbrmerc());
      data.setImbrmreu(entity.getImbrmreu());
      data.setImcammed(entity.getImcammed());
      data.setImcamnet(entity.getImcamnet());
      data.setImcanoncompdv(entity.getImcanoncompdv());
      data.setImcanoncompeu(entity.getImcanoncompeu());
      data.setImcanoncontrdv(entity.getImcanoncontrdv());
      data.setImcanoncontreu(entity.getImcanoncontreu());
      data.setImcanonliqdv(entity.getImcanonliqdv());
      data.setImcanonliqeu(entity.getImcanonliqeu());
      data.setImcconeu(entity.getImcconeu());
      data.setImcliqeu(entity.getImcliqeu());
      data.setImcomclteeu(entity.getImcomclteeu());
      data.setImcomdev(entity.getImcomdev());
      data.setImcomieu(entity.getImcomieu());
      data.setImcomisn(entity.getImcomisn());
      data.setImcorretajesvfinaldv(entity.getImcorretajesvfinaldv());
      data.setImdereeu(entity.getImdereeu());
      data.setImeurliq(entity.getImeurliq());
      data.setImfibrdv(entity.getImfibrdv());
      data.setImfibreu(entity.getImfibreu());
      /**/
      data.setImgasgesdv(entity.getImgasgesdv());
      data.setImgasgeseu(entity.getImgasgeseu());
      data.setImgasliqdv(entity.getImgasliqdv());
      data.setImgasliqeu(entity.getImgasliqeu());
      data.setImgastos(entity.getImgastos());
      data.setImgastotdv(entity.getImgastotdv());
      data.setImgastoteu(entity.getImgastoteu());
      data.setImgatdvs(entity.getImgatdvs());
      //
      data.setImimpdvs(entity.getImimpdvs());
      data.setImimpeur(entity.getImimpeur());
      data.setImliq(entity.getImliq());
      data.setImnetbrk(entity.getImnetbrk());
      data.setImsvb(entity.getImsvb());
      data.setImsvbeu(entity.getImsvbeu());
      data.setImtotbru(entity.getImtotbru());
      data.setImtotnet(entity.getImtotnet());
      data.setImtpcamb(entity.getImtpcamb());
      /**/
      data.setLcleacct(entity.getLcleacct());
      data.setLclebic(entity.getLclebic());
      data.setLclecode(entity.getLclecode());
      data.setLcleid(entity.getLcleid());
      data.setLcusacct(entity.getLcusacct());
      data.setLcusbic(entity.getLcusbic());
      data.setLcuscode(entity.getLcuscode());
      data.setLcusid(entity.getLcusid());
      /**/
      data.setNamecle(entity.getNamecle());
      data.setNamecus(entity.getNamecus());
      data.setNbtitulr(entity.getNbtitulr());
      data.setNbvalors(entity.getNbvalors());
      data.setNuagrpkb(entity.getNuagrpkb());
      data.setNuoprout(entity.getNuoprout());
      data.setNupareje(entity.getNupareje());
      data.setNutitcli(entity.getNutitcli());
      data.setNutitpen(entity.getNutitpen());
      data.setNuversion(entity.getNuversion());

      if (data.getTmct0estadoByCdestadoasig() == null) {
        try {
          data.setTmct0estadoByCdestadoasig(Tmct0EstadoData.entityToData(entity.getTmct0estadoByCdestadoasig()));
        } catch (Exception ex) {
          LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
        }
      }
      if (data.getTmct0estadoByCdestadocont() == null) {
        try {
          data.setTmct0estadoByCdestadocont(Tmct0EstadoData.entityToData(entity.getTmct0estadoByCdestadocont()));
        } catch (Exception ex) {
          LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
        }
      }

      if (entity.getTmct0sta() != null) {
        try {
          data.setTmct0sta(Tmct0staData.entityToData(entity.getTmct0sta()));
        } catch (Exception ex) {
          LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
        }
      }
      /*
       * si los estados no han sido seteados en el constructor eso es que se usa este método desde otra clase
       */
      if (!initializedByConstructor) {
        try {
          insertAlcs(entity.getTmct0alcs(), data);
        } catch (Exception ex) {
          LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
        }
      }
      initializedByConstructor = false;
      /**/
      try {
        data.setTmct0bokId(entity.getTmct0bok().getId());
      } catch (Exception ex) {
        LOG.error(ex.getMessage() + " cause: " + ex.getCause() + " id: " + entity.getId(), ex);
      }
      data.setTpcalcul(entity.getTpcalcul());
      data.setTpsetcus(entity.getTpsetcus());
      data.setTpsetcle(entity.getTpsetcle());
      data.setTysetcle(entity.getTysetcle());
      data.setTysetcus(entity.getTysetcus());
      data.setXtdcm(entity.getXtdcm());
      data.setXtderech(entity.getXtderech());
      data.setRefCliente(entity.getRefCliente());
    }

    LOG.debug("[entityToData(Tmct0alo entity, Tmct0AloData data)] Fin ...");
  } // entityToData

  private static void insertAlcs(List<Tmct0alc> alcs, Tmct0AloData data) {
    /* ALCS */
    LOG.debug("[insertAlcs(List<Tmct0alc> alcs, Tmct0AloData data)] Inicio");
    if (CollectionUtils.isNotEmpty(alcs)) {
      LOG.debug("[insertAlcs(List<Tmct0alc> alcs, Tmct0AloData data)] La coleccion no esta vacia");
      data.setTmct0AlcDataList(Tmct0AlcData.entityListToDataList(alcs));
      LOG.debug("[insertAlcs(List<Tmct0alc> alcs, Tmct0AloData data)] Coleccion añadida ...");
    } // if
  } // insertAlcs

  public static void entityToData(Tmct0alo entity, List<Tmct0alc> alcs, Tmct0AloData data) {
    /* Crea el data */
    entityToData(entity, data);
    /* le inserta los alcs */
    insertAlcs(alcs, data);
  }

  public static Tmct0alo dataToEntity(Tmct0AloData data) {
    Tmct0alo entity = new Tmct0alo();
    Tmct0AloData.dataToEntity(data, entity);
    return entity;
  }

  public static void dataToEntity(Tmct0AloData data, Tmct0alo entity) {
    entity.setAcbencle(data.getAcbencle());
    entity.setAcbencus(data.getAcbencus());
    entity.setAcctatcle(data.getAcctatcle());
    entity.setAcctatcus(data.getAcctatcus());
    entity.setBclebic(data.getBclebic());
    entity.setBcusbic(data.getBcusbic());
    entity.setBiccle(data.getBiccle());
    entity.setBiccus(data.getBiccus());
    entity.setCanoncomppagoclte(data.getCanoncomppagoclte());
    entity.setCanoncomppagomkt(data.getCanoncomppagomkt());
    entity.setCanoncontrpagoclte(data.getCanoncontrpagoclte());
    entity.setCanoncontrpagomkt(data.getCanoncontrpagomkt());
    entity.setCanonliqpagoclte(data.getCanonliqpagoclte());
    entity.setCanonliqpagomkt(data.getCanonliqpagomkt());
    entity.setCdalias(data.getCdalias());
    entity.setCdbencle(data.getCdbencle());
    entity.setCdbencus(data.getCdbencus());
    entity.setCdbloque(data.getCdbloque());
    entity.setCdcleare(data.getCdcleare());
    entity.setCdcustod(data.getCdcustod());
    entity.setCddatliq(data.getCddatliq());
    entity.setCdenvcnf(data.getCdenvcnf());
    entity.setCdenvoas(data.getCdenvoas());
    entity.setCdenvoas(data.getCdenvoas());
    entity.setCdenvseq(data.getCdenvseq());
    entity.setCdestadotit(data.getCdestadotit());
    entity.setCdestoas(data.getCdestoas());
    entity.setCdglocle(data.getCdglocle());
    entity.setCdglocus(data.getCdglocle());
    entity.setCdholder(data.getCdholder());
    entity.setCdindgas(data.getCdindgas());
    entity.setCdindimp(data.getCdindimp());
    entity.setCdisin(data.getCdisin());
    entity.setCdloccle(data.getCdloccle());
    entity.setCdloccus(data.getCdloccus());
    entity.setCdmoniso(data.getCdmoniso());
    entity.setCdnbcle(data.getCdnbcle());
    entity.setCdnbcus(data.getCdnbcus());
    entity.setCdrefoas(data.getCdrefoas());
    entity.setCdreside(data.getCdreside());
    entity.setCdtpoper(data.getCdtpoper());
    entity.setCdusuaud(data.getCdusuaud());
    entity.setCdusublo(data.getCdusublo());
    entity.setCdusuges(data.getCdusuges());
    entity.setDescrali(data.getDescrali());
    entity.setDsobserv(data.getDsobserv());
    entity.setErdatfis(data.getErdatfis());
    entity.setEucomdev(data.getEucomdev());
    entity.setEunetbrk(data.getEunetbrk());
    entity.setEutotbru(data.getEutotbru());
    entity.setEutotnet(data.getEutotnet());
    entity.setFeoperac(data.getFeoperac());
    entity.setFevalor(data.getFevalor());
    entity.setFevalorr(data.getFevalorr());
    entity.setFevalor(data.getFevalor());
    entity.setFhenvoas(data.getFhenvoas());
    entity.setFhenvseq(data.getFhenvseq());
    entity.setGcleacct(data.getGcleacct());
    entity.setGclebic(data.getGclebic());
    entity.setGclecode(data.getGclecode());
    entity.setGcleid(data.getGcleid());
    entity.setGcusacct(data.getGcusacct());
    entity.setGcusbic(data.getGcusbic());
    entity.setGcuscode(data.getGcuscode());
    entity.setGcusid(data.getGcusid());
    entity.setHoenvoas(data.getHoenvoas());
    entity.setHoenvseq(data.getHoenvseq());
    entity.setHooperac(data.getHooperac());
    entity.setId(data.getId());
    entity.setIdbencle(data.getIdbencle());
    entity.setIdbencus(data.getIdbencus());
    /* Im */
    entity.setImajeliq(data.getImajeliq());
    entity.setImajliq(data.getImajliq());
    entity.setImbrmerc(data.getImbrmerc());
    entity.setImbrmreu(data.getImbrmreu());
    entity.setImcammed(data.getImcammed());
    entity.setImcamnet(data.getImcamnet());
    entity.setImcanoncompdv(data.getImcanoncompdv());
    entity.setImcanoncompeu(data.getImcanoncompeu());
    entity.setImcanoncontrdv(data.getImcanoncontrdv());
    entity.setImcanoncontreu(data.getImcanoncontreu());
    entity.setImcanonliqdv(data.getImcanonliqdv());
    entity.setImcanonliqeu(data.getImcanonliqeu());
    entity.setImcconeu(data.getImcconeu());
    entity.setImcliqeu(data.getImcliqeu());
    entity.setImcomclteeu(data.getImcomclteeu());
    entity.setImcomdev(data.getImcomdev());
    entity.setImcomieu(data.getImcomieu());
    entity.setImcomisn(data.getImcomisn());
    entity.setImcorretajesvfinaldv(data.getImcorretajesvfinaldv());
    entity.setImdereeu(data.getImdereeu());
    entity.setImeurliq(data.getImeurliq());
    entity.setImfibrdv(data.getImfibrdv());
    entity.setImfibreu(data.getImfibreu());
    /**/
    entity.setImgasgesdv(data.getImgasgesdv());
    entity.setImgasgeseu(data.getImgasgeseu());
    entity.setImgasliqdv(data.getImgasliqdv());
    entity.setImgasliqeu(data.getImgasliqeu());
    entity.setImgastos(data.getImgastos());
    entity.setImgastotdv(data.getImgastotdv());
    entity.setImgastoteu(data.getImgastoteu());
    entity.setImgatdvs(data.getImgatdvs());
    //
    entity.setImimpdvs(data.getImimpdvs());
    entity.setImimpeur(data.getImimpeur());
    entity.setImliq(data.getImliq());
    entity.setImnetbrk(data.getImnetbrk());
    entity.setImsvb(data.getImsvb());
    entity.setImsvbeu(data.getImsvbeu());
    entity.setImtotbru(data.getImtotbru());
    entity.setImtotnet(data.getImtotnet());
    entity.setImtpcamb(data.getImtpcamb());
    /* ALCS */
    if (CollectionUtils.isNotEmpty(data.getTmct0AlcDataList())) {
      entity.setTmct0alcs(Tmct0AlcData.dataListToEntityList(data.getTmct0AlcDataList()));
    }
    /**/
    entity.setLcleacct(data.getLcleacct());
    entity.setLclebic(data.getLclebic());
    entity.setLclecode(data.getLclecode());
    entity.setLcleid(data.getLcleid());
    entity.setLcusacct(data.getLcusacct());
    entity.setLcusbic(data.getLcusbic());
    entity.setLcuscode(data.getLcuscode());
    entity.setLcusid(data.getLcusid());
    /**/
    entity.setNamecle(data.getNamecle());
    entity.setNamecus(data.getNamecus());
    entity.setNbtitulr(data.getNbtitulr());
    entity.setNbvalors(data.getNbvalors());
    entity.setNuagrpkb(data.getNuagrpkb());
    entity.setNuoprout(data.getNuoprout());
    entity.setNupareje(data.getNupareje());
    entity.setNutitcli(data.getNutitcli());
    entity.setNutitpen(data.getNutitpen());
    entity.setNuversion(data.getNuversion());
    /**/
    entity.setTmct0estadoByCdestadoasig(Tmct0EstadoData.dataToEntity(data.getTmct0estadoByCdestadoasig()));
    entity.setTmct0estadoByCdestadocont(Tmct0EstadoData.dataToEntity(data.getTmct0estadoByCdestadocont()));
    /**/
    if (data.getTmct0bokId() != null) {
      if (entity.getTmct0bok() == null) {
        entity.setTmct0bok(new Tmct0bok());
      }
      entity.getTmct0bok().setId(data.getTmct0bokId());
    }
    entity.setTpcalcul(data.getTpcalcul());
    entity.setTpsetcus(data.getTpsetcus());
    entity.setTpsetcle(data.getTpsetcle());
    entity.setTysetcle(data.getTysetcle());
    entity.setTysetcus(data.getTysetcus());
    entity.setXtdcm(data.getXtdcm());
    entity.setXtderech(data.getXtderech());
    entity.setRefCliente(data.getRefCliente());
    if (data.getTmct0sta() != null) {
      entity.setTmct0sta(Tmct0staData.dataToEntity(data.getTmct0sta()));
    }
  }

  public static List<Tmct0AloData> entityListToDataList(List<Tmct0alo> tmct0alos) {
    List<Tmct0AloData> dataList = new ArrayList<Tmct0AloData>();
    Iterator<Tmct0alo> it = tmct0alos.iterator();
    while (it.hasNext()) {
      dataList.add(entityToData(it.next()));
    }
    return dataList;
  }

  public List<Tmct0AlcData> getTmct0AlcDataList() {
    return tmct0AlcDataList;
  }

  public void setTmct0AlcDataList(List<Tmct0AlcData> tmct0AlcDataList) {
    this.tmct0AlcDataList = tmct0AlcDataList;
  }

  public Tmct0EstadoData getTmct0estadoByCdestadoasig() {
    return tmct0estadoByCdestadoasig;
  }

  public Tmct0aloId getId() {
    return this.id;
  }

  public void setId(Tmct0aloId id) {
    this.id = id;
  }

  public Tmct0bokId getTmct0bokId() {
    return tmct0bokId;
  }

  public void setTmct0bokId(Tmct0bokId tmct0bokId) {
    this.tmct0bokId = tmct0bokId;
  }

  public void setTmct0estadoByCdestadoasig(Tmct0EstadoData tmct0estadoByCdestadoasig) {
    this.tmct0estadoByCdestadoasig = tmct0estadoByCdestadoasig;
  }

  public Tmct0EstadoData getTmct0estadoByCdestadocont() {
    return tmct0estadoByCdestadocont;
  }

  public void setTmct0estadoByCdestadocont(Tmct0EstadoData tmct0estadoByCdestadocont) {
    this.tmct0estadoByCdestadocont = tmct0estadoByCdestadocont;
  }

  public String getCdalias() {
    return cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  public String getDescrali() {
    return descrali;
  }

  public void setDescrali(String descrali) {
    this.descrali = descrali;
  }

  public String getCdusuges() {
    return cdusuges;
  }

  public void setCdusuges(String cdusuges) {
    this.cdusuges = cdusuges;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public Date getFeoperac() {
    return feoperac;
  }

  public void setFeoperac(Date feoperac) {
    this.feoperac = feoperac;
  }

  public Date getHooperac() {
    return hooperac;
  }

  public void setHooperac(Date hooperac) {
    this.hooperac = hooperac;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public String getNbtitulr() {
    return nbtitulr;
  }

  public void setNbtitulr(String nbtitulr) {
    this.nbtitulr = nbtitulr;
  }

  public BigDecimal getNutitcli() {
    return nutitcli;
  }

  public void setNutitcli(BigDecimal nutitcli) {
    this.nutitcli = nutitcli;
  }

  public BigDecimal getNutitpen() {
    return nutitpen;
  }

  public void setNutitpen(BigDecimal nutitpen) {
    this.nutitpen = nutitpen;
  }

  public String getCdisin() {
    return cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public String getNbvalors() {
    return nbvalors;
  }

  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  public Character getCdenvcnf() {
    return cdenvcnf;
  }

  public void setCdenvcnf(Character cdenvcnf) {
    this.cdenvcnf = cdenvcnf;
  }

  public Character getCdenvoas() {
    return cdenvoas;
  }

  public void setCdenvoas(Character cdenvoas) {
    this.cdenvoas = cdenvoas;
  }

  public Character getCdenvseq() {
    return cdenvseq;
  }

  public void setCdenvseq(Character cdenvseq) {
    this.cdenvseq = cdenvseq;
  }

  public Character getCdestoas() {
    return cdestoas;
  }

  public void setCdestoas(Character cdestoas) {
    this.cdestoas = cdestoas;
  }

  public String getCdrefoas() {
    return cdrefoas;
  }

  public void setCdrefoas(String cdrefoas) {
    this.cdrefoas = cdrefoas;
  }

  public Date getFhenvoas() {
    return fhenvoas;
  }

  public void setFhenvoas(Date fhenvoas) {
    this.fhenvoas = fhenvoas;
  }

  public Date getHoenvoas() {
    return hoenvoas;
  }

  public void setHoenvoas(Date hoenvoas) {
    this.hoenvoas = hoenvoas;
  }

  public Date getFhenvseq() {
    return fhenvseq;
  }

  public void setFhenvseq(Date fhenvseq) {
    this.fhenvseq = fhenvseq;
  }

  public Date getHoenvseq() {
    return hoenvseq;
  }

  public void setHoenvseq(Date hoenvseq) {
    this.hoenvseq = hoenvseq;
  }

  public String getCdcustod() {
    return cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public String getAcctatcus() {
    return acctatcus;
  }

  public void setAcctatcus(String acctatcus) {
    this.acctatcus = acctatcus;
  }

  public String getCdbencus() {
    return cdbencus;
  }

  public void setCdbencus(String cdbencus) {
    this.cdbencus = cdbencus;
  }

  public String getAcbencus() {
    return acbencus;
  }

  public void setAcbencus(String acbencus) {
    this.acbencus = acbencus;
  }

  public String getIdbencus() {
    return idbencus;
  }

  public void setIdbencus(String idbencus) {
    this.idbencus = idbencus;
  }

  public String getGcuscode() {
    return gcuscode;
  }

  public void setGcuscode(String gcuscode) {
    this.gcuscode = gcuscode;
  }

  public String getGcusacct() {
    return gcusacct;
  }

  public void setGcusacct(String gcusacct) {
    this.gcusacct = gcusacct;
  }

  public String getGcusid() {
    return gcusid;
  }

  public void setGcusid(String gcusid) {
    this.gcusid = gcusid;
  }

  public String getLcuscode() {
    return lcuscode;
  }

  public void setLcuscode(String lcuscode) {
    this.lcuscode = lcuscode;
  }

  public String getLcusacct() {
    return lcusacct;
  }

  public void setLcusacct(String lcusacct) {
    this.lcusacct = lcusacct;
  }

  public String getLcusid() {
    return lcusid;
  }

  public void setLcusid(String lcusid) {
    this.lcusid = lcusid;
  }

  public String getPccusset() {
    return pccusset;
  }

  public void setPccusset(String pccusset) {
    this.pccusset = pccusset;
  }

  public Character getSendbecus() {
    return sendbecus;
  }

  public void setSendbecus(Character sendbecus) {
    this.sendbecus = sendbecus;
  }

  public String getTpsetcus() {
    return tpsetcus;
  }

  public void setTpsetcus(String tpsetcus) {
    this.tpsetcus = tpsetcus;
  }

  public String getTysetcus() {
    return tysetcus;
  }

  public void setTysetcus(String tysetcus) {
    this.tysetcus = tysetcus;
  }

  public String getCdcleare() {
    return cdcleare;
  }

  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public String getAcctatcle() {
    return acctatcle;
  }

  public void setAcctatcle(String acctatcle) {
    this.acctatcle = acctatcle;
  }

  public String getCdbencle() {
    return cdbencle;
  }

  public void setCdbencle(String cdbencle) {
    this.cdbencle = cdbencle;
  }

  public String getAcbencle() {
    return acbencle;
  }

  public void setAcbencle(String acbencle) {
    this.acbencle = acbencle;
  }

  public String getIdbencle() {
    return idbencle;
  }

  public void setIdbencle(String idbencle) {
    this.idbencle = idbencle;
  }

  public String getGclecode() {
    return gclecode;
  }

  public void setGclecode(String gclecode) {
    this.gclecode = gclecode;
  }

  public String getGcleacct() {
    return gcleacct;
  }

  public void setGcleacct(String gcleacct) {
    this.gcleacct = gcleacct;
  }

  public String getGcleid() {
    return gcleid;
  }

  public void setGcleid(String gcleid) {
    this.gcleid = gcleid;
  }

  public String getLclecode() {
    return lclecode;
  }

  public void setLclecode(String lclecode) {
    this.lclecode = lclecode;
  }

  public String getLcleacct() {
    return lcleacct;
  }

  public void setLcleacct(String lcleacct) {
    this.lcleacct = lcleacct;
  }

  public String getLcleid() {
    return lcleid;
  }

  public void setLcleid(String lcleid) {
    this.lcleid = lcleid;
  }

  public String getPccleset() {
    return pccleset;
  }

  public void setPccleset(String pccleset) {
    this.pccleset = pccleset;
  }

  public Character getSendbecle() {
    return sendbecle;
  }

  public void setSendbecle(Character sendbecle) {
    this.sendbecle = sendbecle;
  }

  public String getTpsetcle() {
    return tpsetcle;
  }

  public void setTpsetcle(String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  public String getTysetcle() {
    return tysetcle;
  }

  public void setTysetcle(String tysetcle) {
    this.tysetcle = tysetcle;
  }

  public BigDecimal getImcammed() {
    return imcammed;
  }

  public void setImcammed(BigDecimal imcammed) {
    this.imcammed = imcammed;
  }

  public BigDecimal getImcamnet() {
    return imcamnet;
  }

  public void setImcamnet(BigDecimal imcamnet) {
    this.imcamnet = imcamnet;
  }

  public BigDecimal getImtotbru() {
    return imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public BigDecimal getImtotnet() {
    return imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public BigDecimal getEutotbru() {
    return eutotbru;
  }

  public void setEutotbru(BigDecimal eutotbru) {
    this.eutotbru = eutotbru;
  }

  public BigDecimal getEutotnet() {
    return eutotnet;
  }

  public void setEutotnet(BigDecimal eutotnet) {
    this.eutotnet = eutotnet;
  }

  public BigDecimal getImnetbrk() {
    return imnetbrk;
  }

  public void setImnetbrk(BigDecimal imnetbrk) {
    this.imnetbrk = imnetbrk;
  }

  public BigDecimal getEunetbrk() {
    return eunetbrk;
  }

  public void setEunetbrk(BigDecimal eunetbrk) {
    this.eunetbrk = eunetbrk;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public BigDecimal getImcomieu() {
    return imcomieu;
  }

  public void setImcomieu(BigDecimal imcomieu) {
    this.imcomieu = imcomieu;
  }

  public BigDecimal getPccomisn() {
    return pccomisn;
  }

  public void setPccomisn(BigDecimal pccomisn) {
    this.pccomisn = pccomisn;
  }

  public BigDecimal getImcomdev() {
    return imcomdev;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  public BigDecimal getEucomdev() {
    return eucomdev;
  }

  public void setEucomdev(BigDecimal eucomdev) {
    this.eucomdev = eucomdev;
  }

  public BigDecimal getImfibrdv() {
    return imfibrdv;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  public BigDecimal getImfibreu() {
    return imfibreu;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  public BigDecimal getImsvb() {
    return imsvb;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  public BigDecimal getImsvbeu() {
    return imsvbeu;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  public BigDecimal getImtpcamb() {
    return imtpcamb;
  }

  public void setImtpcamb(BigDecimal imtpcamb) {
    this.imtpcamb = imtpcamb;
  }

  public String getCdmoniso() {
    return cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public Character getCdindgas() {
    return cdindgas;
  }

  public void setCdindgas(Character cdindgas) {
    this.cdindgas = cdindgas;
  }

  public Character getCdindimp() {
    return cdindimp;
  }

  public void setCdindimp(Character cdindimp) {
    this.cdindimp = cdindimp;
  }

  public Character getCdreside() {
    return cdreside;
  }

  public void setCdreside(Character cdreside) {
    this.cdreside = cdreside;
  }

  public BigDecimal getImbrmerc() {
    return imbrmerc;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  public BigDecimal getImbrmreu() {
    return imbrmreu;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  public BigDecimal getImgastos() {
    return imgastos;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  public BigDecimal getImgatdvs() {
    return imgatdvs;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  public BigDecimal getImimpdvs() {
    return imimpdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  public BigDecimal getImimpeur() {
    return imimpeur;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  public BigDecimal getImcconeu() {
    return imcconeu;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  public BigDecimal getImcliqeu() {
    return imcliqeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  public BigDecimal getImdereeu() {
    return imdereeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  public Character getXtdcm() {
    return xtdcm;
  }

  public void setXtdcm(Character xtdcm) {
    this.xtdcm = xtdcm;
  }

  public Character getXtderech() {
    return xtderech;
  }

  public void setXtderech(Character xtderech) {
    this.xtderech = xtderech;
  }

  public String getRfparten() {
    return rfparten;
  }

  public void setRfparten(String rfparten) {
    this.rfparten = rfparten;
  }

  public Character getTpcalcul() {
    return tpcalcul;
  }

  public void setTpcalcul(Character tpcalcul) {
    this.tpcalcul = tpcalcul;
  }

  public Character getIndatfis() {
    return indatfis;
  }

  public void setIndatfis(Character indatfis) {
    this.indatfis = indatfis;
  }

  public String getErdatfis() {
    return erdatfis;
  }

  public void setErdatfis(String erdatfis) {
    this.erdatfis = erdatfis;
  }

  public Character getCddatliq() {
    return cddatliq;
  }

  public void setCddatliq(Character cddatliq) {
    this.cddatliq = cddatliq;
  }

  public Character getCdbloque() {
    return cdbloque;
  }

  public void setCdbloque(Character cdbloque) {
    this.cdbloque = cdbloque;
  }

  public Integer getNuoprout() {
    return nuoprout;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  public Short getNupareje() {
    return nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  public String getDsobserv() {
    return dsobserv;
  }

  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }

  public String getCdusublo() {
    return cdusublo;
  }

  public void setCdusublo(String cdusublo) {
    this.cdusublo = cdusublo;
  }

  public String getCdusuaud() {
    return cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  public Integer getNuversion() {
    return nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  public Integer getNuagrpkb() {
    return nuagrpkb;
  }

  public void setNuagrpkb(Integer nuagrpkb) {
    this.nuagrpkb = nuagrpkb;
  }

  public String getPkentity() {
    return pkentity;
  }

  public void setPkentity(String pkentity) {
    this.pkentity = pkentity;
  }

  public String getBiccus() {
    return biccus;
  }

  public void setBiccus(String biccus) {
    this.biccus = biccus;
  }

  public String getNamecus() {
    return namecus;
  }

  public void setNamecus(String namecus) {
    this.namecus = namecus;
  }

  public String getLcusbic() {
    return lcusbic;
  }

  public void setLcusbic(String lcusbic) {
    this.lcusbic = lcusbic;
  }

  public String getCdloccus() {
    return cdloccus;
  }

  public void setCdloccus(String cdloccus) {
    this.cdloccus = cdloccus;
  }

  public String getGcusbic() {
    return gcusbic;
  }

  public void setGcusbic(String gcusbic) {
    this.gcusbic = gcusbic;
  }

  public String getCdglocus() {
    return cdglocus;
  }

  public void setCdglocus(String cdglocus) {
    this.cdglocus = cdglocus;
  }

  public String getBcusbic() {
    return bcusbic;
  }

  public void setBcusbic(String bcusbic) {
    this.bcusbic = bcusbic;
  }

  public String getCdnbcus() {
    return cdnbcus;
  }

  public void setCdnbcus(String cdnbcus) {
    this.cdnbcus = cdnbcus;
  }

  public String getBiccle() {
    return biccle;
  }

  public void setBiccle(String biccle) {
    this.biccle = biccle;
  }

  public String getNamecle() {
    return namecle;
  }

  public void setNamecle(String namecle) {
    this.namecle = namecle;
  }

  public String getLclebic() {
    return lclebic;
  }

  public void setLclebic(String lclebic) {
    this.lclebic = lclebic;
  }

  public String getCdloccle() {
    return cdloccle;
  }

  public void setCdloccle(String cdloccle) {
    this.cdloccle = cdloccle;
  }

  public String getGclebic() {
    return gclebic;
  }

  public void setGclebic(String gclebic) {
    this.gclebic = gclebic;
  }

  public String getCdglocle() {
    return cdglocle;
  }

  public void setCdglocle(String cdglocle) {
    this.cdglocle = cdglocle;
  }

  public String getBclebic() {
    return bclebic;
  }

  public void setBclebic(String bclebic) {
    this.bclebic = bclebic;
  }

  public String getCdnbcle() {
    return cdnbcle;
  }

  public void setCdnbcle(String cdnbcle) {
    this.cdnbcle = cdnbcle;
  }

  public Date getFevalorr() {
    return fevalorr;
  }

  public void setFevalorr(Date fevalorr) {
    this.fevalorr = fevalorr;
  }

  public BigDecimal getImliq() {
    return imliq;
  }

  public void setImliq(BigDecimal imliq) {
    this.imliq = imliq;
  }

  public BigDecimal getImeurliq() {
    return imeurliq;
  }

  public void setImeurliq(BigDecimal imeurliq) {
    this.imeurliq = imeurliq;
  }

  public BigDecimal getImajliq() {
    return imajliq;
  }

  public void setImajliq(BigDecimal imajliq) {
    this.imajliq = imajliq;
  }

  public BigDecimal getImajeliq() {
    return imajeliq;
  }

  public void setImajeliq(BigDecimal imajeliq) {
    this.imajeliq = imajeliq;
  }

  public BigDecimal getImgasliqdv() {
    return imgasliqdv;
  }

  public void setImgasliqdv(BigDecimal imgasliqdv) {
    this.imgasliqdv = imgasliqdv;
  }

  public BigDecimal getImgasliqeu() {
    return imgasliqeu;
  }

  public void setImgasliqeu(BigDecimal imgasliqeu) {
    this.imgasliqeu = imgasliqeu;
  }

  public BigDecimal getImgasgesdv() {
    return imgasgesdv;
  }

  public void setImgasgesdv(BigDecimal imgasgesdv) {
    this.imgasgesdv = imgasgesdv;
  }

  public BigDecimal getImgasgeseu() {
    return imgasgeseu;
  }

  public void setImgasgeseu(BigDecimal imgasgeseu) {
    this.imgasgeseu = imgasgeseu;
  }

  public BigDecimal getImgastotdv() {
    return imgastotdv;
  }

  public void setImgastotdv(BigDecimal imgastotdv) {
    this.imgastotdv = imgastotdv;
  }

  public BigDecimal getImgastoteu() {
    return imgastoteu;
  }

  public void setImgastoteu(BigDecimal imgastoteu) {
    this.imgastoteu = imgastoteu;
  }

  public String getRefbanif() {
    return refbanif;
  }

  public void setRefbanif(String refbanif) {
    this.refbanif = refbanif;
  }

  public String getRefcltdes() {
    return refcltdes;
  }

  public void setRefcltdes(String refcltdes) {
    this.refcltdes = refcltdes;
  }

  public String getCdholder() {
    return cdholder;
  }

  public void setCdholder(String cdholder) {
    this.cdholder = cdholder;
  }

  public Integer getCdestadotit() {
    return cdestadotit;
  }

  public void setCdestadotit(Integer cdestadotit) {
    this.cdestadotit = cdestadotit;
  }

  public String getReftitular() {
    return reftitular;
  }

  public void setReftitular(String reftitular) {
    this.reftitular = reftitular;
  }

  public BigDecimal getImcorretajesvfinaldv() {
    return imcorretajesvfinaldv;
  }

  public void setImcorretajesvfinaldv(BigDecimal imcorretajesvfinaldv) {
    this.imcorretajesvfinaldv = imcorretajesvfinaldv;
  }

  public BigDecimal getImcomclteeu() {
    return imcomclteeu;
  }

  public void setImcomclteeu(BigDecimal imcomclteeu) {
    this.imcomclteeu = imcomclteeu;
  }

  public Integer getCanoncontrpagoclte() {
    return canoncontrpagoclte;
  }

  public void setCanoncontrpagoclte(Integer canoncontrpagoclte) {
    this.canoncontrpagoclte = canoncontrpagoclte;
  }

  public Integer getCanoncontrpagomkt() {
    return canoncontrpagomkt;
  }

  public void setCanoncontrpagomkt(Integer canoncontrpagomkt) {
    this.canoncontrpagomkt = canoncontrpagomkt;
  }

  public Integer getCanonliqpagoclte() {
    return canonliqpagoclte;
  }

  public void setCanonliqpagoclte(Integer canonliqpagoclte) {
    this.canonliqpagoclte = canonliqpagoclte;
  }

  public Integer getCanonliqpagomkt() {
    return canonliqpagomkt;
  }

  public void setCanonliqpagomkt(Integer canonliqpagomkt) {
    this.canonliqpagomkt = canonliqpagomkt;
  }

  public Integer getCanoncomppagoclte() {
    return canoncomppagoclte;
  }

  public void setCanoncomppagoclte(Integer canoncomppagoclte) {
    this.canoncomppagoclte = canoncomppagoclte;
  }

  public Integer getCanoncomppagomkt() {
    return canoncomppagomkt;
  }

  public void setCanoncomppagomkt(Integer canoncomppagomkt) {
    this.canoncomppagomkt = canoncomppagomkt;
  }

  public BigDecimal getImcanoncompdv() {
    return imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  public BigDecimal getImcanoncompeu() {
    return imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  public BigDecimal getImcanoncontrdv() {
    return imcanoncontrdv;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  public BigDecimal getImcanoncontreu() {
    return imcanoncontreu;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  public BigDecimal getImcanonliqdv() {
    return imcanonliqdv;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  public BigDecimal getImcanonliqeu() {
    return imcanonliqeu;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  public Tmct0staData getTmct0sta() {
    return tmct0sta;
  }

  public void setTmct0sta(Tmct0staData tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  public String getRefCliente() {
    return refCliente;
  }

  public void setRefCliente(String refCliente) {
    this.refCliente = refCliente;
  }

  public static Tmct0AloData copy(Tmct0AloData data) {
    Tmct0AloData copy = new Tmct0AloData();
    if (CollectionUtils.isNotEmpty(data.getTmct0AlcDataList())) {
      copy.setTmct0AlcDataList(Tmct0AlcData.copyDataList(data.getTmct0AlcDataList()));
    }
    copy.setAcbencle(data.getAcbencle());
    copy.setAcbencus(data.getAcbencus());
    copy.setAcctatcle(data.getAcctatcle());
    copy.setAcctatcus(data.getAcctatcus());
    copy.setBclebic(data.getBclebic());
    copy.setBcusbic(data.getBcusbic());
    copy.setBiccle(data.getBiccle());
    copy.setBiccus(data.getBiccus());
    copy.setCanoncomppagoclte(data.getCanoncomppagoclte());
    copy.setCanoncomppagomkt(data.getCanoncomppagomkt());
    copy.setCanoncontrpagoclte(data.getCanoncontrpagoclte());
    copy.setCanoncontrpagomkt(data.getCanoncontrpagomkt());
    copy.setCanonliqpagoclte(data.getCanonliqpagoclte());
    copy.setCanonliqpagomkt(data.getCanonliqpagomkt());
    copy.setCdalias(data.getCdalias());
    copy.setCdbencle(data.getCdbencle());
    copy.setCdbencus(data.getCdbencus());
    copy.setCdbloque(data.getCdbloque());
    copy.setCdcleare(data.getCdcleare());
    copy.setCdcustod(data.getCdcustod());
    copy.setCddatliq(data.getCddatliq());
    copy.setCdenvcnf(data.getCdenvcnf());
    copy.setCdenvoas(data.getCdenvoas());
    copy.setCdenvoas(data.getCdenvoas());
    copy.setCdenvseq(data.getCdenvseq());
    copy.setCdestadotit(data.getCdestadotit());
    copy.setCdestoas(data.getCdestoas());
    copy.setCdglocle(data.getCdglocle());
    copy.setCdglocus(data.getCdglocle());
    copy.setCdholder(data.getCdholder());
    copy.setCdindgas(data.getCdindgas());
    copy.setCdindimp(data.getCdindimp());
    copy.setCdisin(data.getCdisin());
    copy.setCdloccle(data.getCdloccle());
    copy.setCdloccus(data.getCdloccus());
    copy.setCdmoniso(data.getCdmoniso());
    copy.setCdnbcle(data.getCdnbcle());
    copy.setCdnbcus(data.getCdnbcus());
    copy.setCdrefoas(data.getCdrefoas());
    copy.setCdreside(data.getCdreside());
    copy.setCdtpoper(data.getCdtpoper());
    copy.setCdusuaud(data.getCdusuaud());
    copy.setCdusublo(data.getCdusublo());
    copy.setCdusuges(data.getCdusuges());
    copy.setDescrali(data.getDescrali());
    copy.setDsobserv(data.getDsobserv());
    copy.setErdatfis(data.getErdatfis());
    copy.setEucomdev(data.getEucomdev());
    copy.setEunetbrk(data.getEunetbrk());
    copy.setEutotbru(data.getEutotbru());
    copy.setEutotnet(data.getEutotnet());
    copy.setFeoperac(data.getFeoperac());
    copy.setFevalor(data.getFevalor());
    copy.setFevalorr(data.getFevalorr());
    copy.setFevalor(data.getFevalor());
    copy.setFhenvoas(data.getFhenvoas());
    copy.setFhenvseq(data.getFhenvseq());
    copy.setGcleacct(data.getGcleacct());
    copy.setGclebic(data.getGclebic());
    copy.setGclecode(data.getGclecode());
    copy.setGcleid(data.getGcleid());
    copy.setGcusacct(data.getGcusacct());
    copy.setGcusbic(data.getGcusbic());
    copy.setGcuscode(data.getGcuscode());
    copy.setGcusid(data.getGcusid());
    copy.setHoenvoas(data.getHoenvoas());
    copy.setHoenvseq(data.getHoenvseq());
    copy.setHooperac(data.getHooperac());
    copy.setId(data.getId());
    copy.setIdbencle(data.getIdbencle());
    copy.setIdbencus(data.getIdbencus());
    /* Im */
    copy.setImajeliq(data.getImajeliq());
    copy.setImajliq(data.getImajliq());
    copy.setImbrmerc(data.getImbrmerc());
    copy.setImbrmreu(data.getImbrmreu());
    copy.setImcammed(data.getImcammed());
    copy.setImcamnet(data.getImcamnet());
    copy.setImcanoncompdv(data.getImcanoncompdv());
    copy.setImcanoncompeu(data.getImcanoncompeu());
    copy.setImcanoncontrdv(data.getImcanoncontrdv());
    copy.setImcanoncontreu(data.getImcanoncontreu());
    copy.setImcanonliqdv(data.getImcanonliqdv());
    copy.setImcanonliqeu(data.getImcanonliqeu());
    copy.setImcconeu(data.getImcconeu());
    copy.setImcliqeu(data.getImcliqeu());
    copy.setImcomclteeu(data.getImcomclteeu());
    copy.setImcomdev(data.getImcomdev());
    copy.setImcomieu(data.getImcomieu());
    copy.setImcomisn(data.getImcomisn());
    copy.setImcorretajesvfinaldv(data.getImcorretajesvfinaldv());
    copy.setImdereeu(data.getImdereeu());
    copy.setImeurliq(data.getImeurliq());
    copy.setImfibrdv(data.getImfibrdv());
    copy.setImfibreu(data.getImfibreu());
    /**/
    copy.setImgasgesdv(data.getImgasgesdv());
    copy.setImgasgeseu(data.getImgasgeseu());
    copy.setImgasliqdv(data.getImgasliqdv());
    copy.setImgasliqeu(data.getImgasliqeu());
    copy.setImgastos(data.getImgastos());
    copy.setImgastotdv(data.getImgastotdv());
    copy.setImgastoteu(data.getImgastoteu());
    copy.setImgatdvs(data.getImgatdvs());
    //
    copy.setImimpdvs(data.getImimpdvs());
    copy.setImimpeur(data.getImimpeur());
    copy.setImliq(data.getImliq());
    copy.setImnetbrk(data.getImnetbrk());
    copy.setImsvb(data.getImsvb());
    copy.setImsvbeu(data.getImsvbeu());
    copy.setImtotbru(data.getImtotbru());
    copy.setImtotnet(data.getImtotnet());
    copy.setImtpcamb(data.getImtpcamb());

    /**/
    copy.setLcleacct(data.getLcleacct());
    copy.setLclebic(data.getLclebic());
    copy.setLclecode(data.getLclecode());
    copy.setLcleid(data.getLcleid());
    copy.setLcusacct(data.getLcusacct());
    copy.setLcusbic(data.getLcusbic());
    copy.setLcuscode(data.getLcuscode());
    copy.setLcusid(data.getLcusid());
    /**/
    copy.setNamecle(data.getNamecle());
    copy.setNamecus(data.getNamecus());
    copy.setNbtitulr(data.getNbtitulr());
    copy.setNbvalors(data.getNbvalors());
    copy.setNuagrpkb(data.getNuagrpkb());
    copy.setNuoprout(data.getNuoprout());
    copy.setNupareje(data.getNupareje());
    copy.setNutitcli(data.getNutitcli());
    copy.setNutitpen(data.getNutitpen());
    copy.setNuversion(data.getNuversion());
    /**/
    copy.setTmct0estadoByCdestadoasig(Tmct0EstadoData.copy(data.getTmct0estadoByCdestadoasig()));
    copy.setTmct0estadoByCdestadocont(Tmct0EstadoData.copy(data.getTmct0estadoByCdestadocont()));
    /**/
    if (data.getTmct0bokId() != null) {
      copy.setTmct0bokId(new Tmct0bokId());
      copy.getTmct0bokId().setNbooking(data.getTmct0bokId().getNbooking());
      copy.getTmct0bokId().setNuorden(data.getTmct0bokId().getNuorden());
    }
    copy.setTpcalcul(data.getTpcalcul());
    copy.setTpsetcus(data.getTpsetcus());
    copy.setTpsetcle(data.getTpsetcle());
    copy.setTysetcle(data.getTysetcle());
    copy.setTysetcus(data.getTysetcus());
    copy.setXtdcm(data.getXtdcm());
    copy.setXtderech(data.getXtderech());
    copy.setRefCliente(data.getRefCliente());
    if (data.getTmct0sta() != null) {
      copy.setTmct0sta(Tmct0staData.copy(data.getTmct0sta()));
    }
    return copy;
  }

}
