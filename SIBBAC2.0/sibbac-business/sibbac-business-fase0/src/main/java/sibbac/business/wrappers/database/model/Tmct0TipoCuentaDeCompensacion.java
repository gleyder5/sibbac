package sibbac.business.wrappers.database.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table( name = "TMCT0_TIPO_CUENTA_DE_COMPENSACION" )
public class Tmct0TipoCuentaDeCompensacion implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long					serialVersionUID			= -7706984127877143346L;
	private Long								idTipoCuenta;
	private String								cdCodigo;
	private String								nbDescripcion;
	private char								ifinanciero;
	private List< Tmct0CuentasDeCompensacion >	tmct0CuentasDeCompensacions	= new ArrayList< Tmct0CuentasDeCompensacion >( 0 );

	public Tmct0TipoCuentaDeCompensacion() {
	}

	public Tmct0TipoCuentaDeCompensacion( Long idTipoCuenta, String cdCodigo ) {
		this.idTipoCuenta = idTipoCuenta;
		this.cdCodigo = cdCodigo;
	}

	public Tmct0TipoCuentaDeCompensacion( Long idTipoCuenta, String cdCodigo, String nbDescripcion,
			List< Tmct0CuentasDeCompensacion > tmct0CuentasDeCompensacions ) {
		this.idTipoCuenta = idTipoCuenta;
		this.cdCodigo = cdCodigo;
		this.nbDescripcion = nbDescripcion;
		this.tmct0CuentasDeCompensacions = tmct0CuentasDeCompensacions;
	}

	@Id
	@Column( name = "ID_TIPO_CUENTA", unique = true, nullable = false )
	public Long getIdTipoCuenta() {
		return this.idTipoCuenta;
	}

	public void setIdTipoCuenta( Long idTipoCuenta ) {
		this.idTipoCuenta = idTipoCuenta;
	}

	@Column( name = "CD_CODIGO", unique = true, nullable = false, length = 4 )
	public String getCdCodigo() {
		return this.cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	@Column( name = "NB_DESCRIPCION" )
	public String getNbDescripcion() {
		return this.nbDescripcion;
	}

	public void setNbDescripcion( String nbDescripcion ) {
		this.nbDescripcion = nbDescripcion;
	}

	@Column( name = "IFINANCIERO", length = 1 )
	public char getIfinanciero() {
		return ifinanciero;
	}

	public void setIfinanciero( char ifinanciero ) {
		this.ifinanciero = ifinanciero;
	}

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "tmct0TipoCuentaDeCompensacion" )
	public List< Tmct0CuentasDeCompensacion > getTmct0CuentasDeCompensacions() {
		return this.tmct0CuentasDeCompensacions;
	}

	public void setTmct0CuentasDeCompensacions( List< Tmct0CuentasDeCompensacion > tmct0CuentasDeCompensacions ) {
		this.tmct0CuentasDeCompensacions = tmct0CuentasDeCompensacions;
	}


}
