package sibbac.business.fase0.rest;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.DatosMaestrosService;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceDatosMaestros implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger		LOG												= LoggerFactory
																							.getLogger( SIBBACServiceDatosMaestros.class );

	private static final String		CMD_GET_CODIGOS_OPERACION						= "getCodigosOperacion";
	private static final String		CMD_GET_GRUPOS_VALORES							= "getGruposValores";
	private static final String		CMD_GET_TIPOS_PRODUCTO							= "getTiposProducto";
	private static final String		CMD_GET_TIPOS_PERSONA							= "getTiposPersona";
	private static final String		CMD_GET_TIPOS_GARANTIAS							= "getTiposGarantiaExigida";
	private static final String		CMD_GET_TIPOS_CONCEPTOS_MOVIMIENTOS_EFECTIVO	= "getConceptosMovimientoEfectivo";
	private static final String		CMD_GET_MODOS_MAT_GARANTIAS						= "getModosMatGarantias";
	private static final String		CMD_GET_DEPOSITARIOS_VALORES					= "getDepositariosValores";
	private static final String		CMD_GET_TIPOS_ACTIVOS							= "getTiposActivo";

	@Autowired
	private DatosMaestrosService	service;

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		String prefix = "[SIBBACServiceTipoMovimiento::process] ";
		LOG.debug( prefix + "Processing a web request..." );
		WebResponse webResponse = new WebResponse();

		String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );
		Map< String, Object > response = new HashMap< String, Object >();
		switch ( command ) {
			case CMD_GET_CODIGOS_OPERACION:
				response.put( "datosMaestros", service.getCodigosOperacion() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_GRUPOS_VALORES:
				response.put( "datosMaestros", service.getGrupoValores() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_TIPOS_PRODUCTO:
				response.put( "datosMaestros", service.getTiposProducto() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_TIPOS_PERSONA:
				response.put( "datosMaestros", service.getTiposPersona() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_TIPOS_GARANTIAS:
				response.put( "datosMaestros", service.getTiposGarantiasExigidas() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_TIPOS_CONCEPTOS_MOVIMIENTOS_EFECTIVO:
				response.put( "datosMaestros", service.getConceptosMovimientoEfectivos() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_MODOS_MAT_GARANTIAS:
				response.put( "datosMaestros", service.getModosMatGarantias() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_DEPOSITARIOS_VALORES:
				response.put( "datosMaestros", service.getDepositariosValores() );
				webResponse.setResultados( response );
				break;
			case CMD_GET_TIPOS_ACTIVOS:
				response.put( "datosMaestros", service.getTiposActivos() );
				webResponse.setResultados( response );
				break;

		}
		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {
		return Arrays.asList( new String[] {
				CMD_GET_CODIGOS_OPERACION, CMD_GET_GRUPOS_VALORES, CMD_GET_TIPOS_PRODUCTO, CMD_GET_TIPOS_PERSONA, CMD_GET_TIPOS_GARANTIAS
		} );
	}

	@Override
	public List< String > getFields() {
		return Arrays.asList( new String[] {} );
	}

}
