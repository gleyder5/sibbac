package sibbac.business.wrappers.database.data;


// TODO: Auto-generated Javadoc
/**
 * The Class ServiciosContactosData.
 */
public class ServiciosContactosData {

	/** The servicio. */
	private final ServicioData	servicio;

	/** The contacto. */
	private final ContactosData	contacto;

	/** The is active. */
	private Boolean				isActive;

	/**
	 * Instantiates a new servicios contactos data.
	 *
	 * @param serviciosContactosData
	 *            the servicios contactos data
	 * @param isActive
	 *            the is active
	 */
	public ServiciosContactosData( ServiciosContactosData serviciosContactosData, Boolean isActive ) {
		super();
		this.servicio = new ServicioData( serviciosContactosData.getServicio() );
		this.contacto = new ContactosData( serviciosContactosData.getContacto() );
		this.isActive = isActive;
	}

	/**
	 * Instantiates a new servicios contactos data.
	 *
	 * @param servicio the servicio
	 * @param contacto the contacto
	 * @param isActive the is active
	 */
	public ServiciosContactosData( ServicioData servicio, ContactosData contacto, Boolean isActive ) {
		super();
		this.servicio = servicio;
		this.contacto = contacto;
		this.isActive = isActive;
	}

	/**
	 * Instantiates a new servicios contactos data.
	 *
	 * @param idServicio the id servicio
	 * @param nombreServicio the nombre servicio
	 * @param idContacto the id contacto
	 * @param nombreContacto the nombre contacto
	 * @param pApellidoContacto the apellido contacto
	 * @param sApellidoContacto the s apellido contacto
	 * @param telefonoAContacto the telefono a contacto
	 * @param emailContacto the email contacto
	 */
	public ServiciosContactosData( Long idServicio, String nombreServicio, String descripcionServicio, Long idContacto,
			String nombreContacto, String pApellidoContacto, String sApellidoContacto, String telefonoAContacto, String emailContacto ) {
		super();
		this.servicio = new ServicioData( idServicio, nombreServicio, descripcionServicio );
		this.contacto = new ContactosData( idContacto, nombreContacto, pApellidoContacto, sApellidoContacto, telefonoAContacto,
				emailContacto );
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the checks if is active
	 */
	public final Boolean getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive
	 *            the new checks if is active
	 */
	public final void setIsActive( Boolean isActive ) {
		this.isActive = isActive;
	}

	/**
	 * Gets the contacto.
	 *
	 * @return the contacto
	 */
	public final ContactosData getContacto() {
		return contacto;
	}

	/**
	 * Gets the servicio.
	 *
	 * @return the servicio
	 */
	public final ServicioData getServicio() {
		return servicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiciosContactosData [servicio=" + servicio + ", contacto=" + contacto + ", isActive=" + isActive + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( contacto == null ) ? 0 : contacto.hashCode() );
		result = prime * result + ( ( isActive == null ) ? 0 : isActive.hashCode() );
		result = prime * result + ( ( servicio == null ) ? 0 : servicio.hashCode() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		ServiciosContactosData other = ( ServiciosContactosData ) obj;
		if ( contacto == null ) {
			if ( other.contacto != null )
				return false;
		} else if ( !contacto.equals( other.contacto ) )
			return false;
		if ( isActive == null ) {
			if ( other.isActive != null )
				return false;
		} else if ( !isActive.equals( other.isActive ) )
			return false;
		if ( servicio == null ) {
			if ( other.servicio != null )
				return false;
		} else if ( !servicio.equals( other.servicio ) )
			return false;
		return true;
	}

}
