package sibbac.business.wrappers.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TMCT0CTO")
public class Tmct0cto implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -2421595559942311041L;
  private Tmct0ctoId id;
  private Tmct0ord tmct0ord;
  private Tmct0sta tmct0sta;
  private BigDecimal imcammed;
  private BigDecimal imcamnet;
  private BigDecimal imtotbru;
  private BigDecimal imtotnet;
  private BigDecimal eutotbru;
  private BigDecimal eutotnet;
  private BigDecimal imnetbrk;
  private BigDecimal eunetbrk;
  private BigDecimal imcomisn;
  private BigDecimal imcomieu;
  private BigDecimal imcomdev;
  private BigDecimal eucomdev;
  private BigDecimal imfibrdv;
  private BigDecimal imfibreu;
  private BigDecimal imsvb;
  private BigDecimal imsvbeu;
  private BigDecimal imbrmerc;
  private BigDecimal imbrmreu;
  private BigDecimal imgastos;
  private BigDecimal imgatdvs;
  private BigDecimal imimpdvs;
  private BigDecimal imimpeur;
  private BigDecimal imcconeu;
  private BigDecimal imcliqeu;
  private BigDecimal imdereeu;
  private BigDecimal nutitagr = BigDecimal.ZERO;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuversion;
  private List<Tmct0ctg> tmct0ctgs = new ArrayList<Tmct0ctg>(0);

  public Tmct0cto() {
  }

  public Tmct0cto(Tmct0ctoId id, Tmct0ord tmct0ord, Tmct0sta tmct0sta) {
    this.id = id;
    this.tmct0ord = tmct0ord;
    this.tmct0sta = tmct0sta;
  }

  public Tmct0cto(Tmct0ctoId id,
                  Tmct0ord tmct0ord,
                  Tmct0sta tmct0sta,
                  BigDecimal imcammed,
                  BigDecimal imcamnet,
                  BigDecimal imtotbru,
                  BigDecimal imtotnet,
                  BigDecimal eutotbru,
                  BigDecimal eutotnet,
                  BigDecimal imnetbrk,
                  BigDecimal eunetbrk,
                  BigDecimal imcomisn,
                  BigDecimal imcomieu,
                  BigDecimal imcomdev,
                  BigDecimal eucomdev,
                  BigDecimal imfibrdv,
                  BigDecimal imfibreu,
                  BigDecimal imsvb,
                  BigDecimal imsvbeu,
                  BigDecimal imbrmerc,
                  BigDecimal imbrmreu,
                  BigDecimal imgastos,
                  BigDecimal imgatdvs,
                  BigDecimal imimpdvs,
                  BigDecimal imimpeur,
                  BigDecimal imcconeu,
                  BigDecimal imcliqeu,
                  BigDecimal imdereeu,
                  BigDecimal nutitagr,
                  String cdusuaud,
                  Date fhaudit,
                  Integer nuversion,
                  List<Tmct0ctg> tmct0ctgs) {
    this.id = id;
    this.tmct0ord = tmct0ord;
    this.tmct0sta = tmct0sta;
    this.imcammed = imcammed;
    this.imcamnet = imcamnet;
    this.imtotbru = imtotbru;
    this.imtotnet = imtotnet;
    this.eutotbru = eutotbru;
    this.eutotnet = eutotnet;
    this.imnetbrk = imnetbrk;
    this.eunetbrk = eunetbrk;
    this.imcomisn = imcomisn;
    this.imcomieu = imcomieu;
    this.imcomdev = imcomdev;
    this.eucomdev = eucomdev;
    this.imfibrdv = imfibrdv;
    this.imfibreu = imfibreu;
    this.imsvb = imsvb;
    this.imsvbeu = imsvbeu;
    this.imbrmerc = imbrmerc;
    this.imbrmreu = imbrmreu;
    this.imgastos = imgastos;
    this.imgatdvs = imgatdvs;
    this.imimpdvs = imimpdvs;
    this.imimpeur = imimpeur;
    this.imcconeu = imcconeu;
    this.imcliqeu = imcliqeu;
    this.imdereeu = imdereeu;
    this.nutitagr = nutitagr;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuversion = nuversion;
    this.tmct0ctgs = tmct0ctgs;
  }

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdmercad", column = @Column(name = "CDMERCAD",
                                                                               nullable = false,
                                                                               length = 20)), @AttributeOverride(name = "nuorden",
                                                                                                                 column = @Column(name = "NUORDEN",
                                                                                                                                  nullable = false,
                                                                                                                                  length = 20)), @AttributeOverride(name = "cdbroker",
                                                                                                                                                                    column = @Column(name = "CDBROKER",
                                                                                                                                                                                     nullable = false,
                                                                                                                                                                                     length = 20))
  })
  public Tmct0ctoId getId() {
    return this.id;
  }

  public void setId(Tmct0ctoId id) {
    this.id = id;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "NUORDEN", nullable = false, insertable = false, updatable = false)
  public Tmct0ord getTmct0ord() {
    return this.tmct0ord;
  }

  public void setTmct0ord(Tmct0ord tmct0ord) {
    this.tmct0ord = tmct0ord;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false), @JoinColumn(name = "CDTIPEST",
                                                                                                                  referencedColumnName = "CDTIPEST",
                                                                                                                  nullable = false)
  })
  public Tmct0sta getTmct0sta() {
    return this.tmct0sta;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Column(name = "IMCAMMED", precision = 18, scale = 8)
  public BigDecimal getImcammed() {
    return this.imcammed;
  }

  public void setImcammed(BigDecimal imcammed) {
    this.imcammed = imcammed;
  }

  @Column(name = "IMCAMNET", precision = 18, scale = 8)
  public BigDecimal getImcamnet() {
    return this.imcamnet;
  }

  public void setImcamnet(BigDecimal imcamnet) {
    this.imcamnet = imcamnet;
  }

  @Column(name = "IMTOTBRU", precision = 18, scale = 8)
  public BigDecimal getImtotbru() {
    return this.imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  @Column(name = "IMTOTNET", precision = 18, scale = 8)
  public BigDecimal getImtotnet() {
    return this.imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  @Column(name = "EUTOTBRU", precision = 18, scale = 8)
  public BigDecimal getEutotbru() {
    return this.eutotbru;
  }

  public void setEutotbru(BigDecimal eutotbru) {
    this.eutotbru = eutotbru;
  }

  @Column(name = "EUTOTNET", precision = 18, scale = 8)
  public BigDecimal getEutotnet() {
    return this.eutotnet;
  }

  public void setEutotnet(BigDecimal eutotnet) {
    this.eutotnet = eutotnet;
  }

  @Column(name = "IMNETBRK", precision = 18, scale = 8)
  public BigDecimal getImnetbrk() {
    return this.imnetbrk;
  }

  public void setImnetbrk(BigDecimal imnetbrk) {
    this.imnetbrk = imnetbrk;
  }

  @Column(name = "EUNETBRK", precision = 18, scale = 8)
  public BigDecimal getEunetbrk() {
    return this.eunetbrk;
  }

  public void setEunetbrk(BigDecimal eunetbrk) {
    this.eunetbrk = eunetbrk;
  }

  @Column(name = "IMCOMISN", precision = 18, scale = 8)
  public BigDecimal getImcomisn() {
    return this.imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  @Column(name = "IMCOMIEU", precision = 18, scale = 8)
  public BigDecimal getImcomieu() {
    return this.imcomieu;
  }

  public void setImcomieu(BigDecimal imcomieu) {
    this.imcomieu = imcomieu;
  }

  @Column(name = "IMCOMDEV", precision = 18, scale = 8)
  public BigDecimal getImcomdev() {
    return this.imcomdev;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  @Column(name = "EUCOMDEV", precision = 18, scale = 8)
  public BigDecimal getEucomdev() {
    return this.eucomdev;
  }

  public void setEucomdev(BigDecimal eucomdev) {
    this.eucomdev = eucomdev;
  }

  @Column(name = "IMFIBRDV", precision = 18, scale = 8)
  public BigDecimal getImfibrdv() {
    return this.imfibrdv;
  }

  public void setImfibrdv(BigDecimal imfibrdv) {
    this.imfibrdv = imfibrdv;
  }

  @Column(name = "IMFIBREU", precision = 18, scale = 8)
  public BigDecimal getImfibreu() {
    return this.imfibreu;
  }

  public void setImfibreu(BigDecimal imfibreu) {
    this.imfibreu = imfibreu;
  }

  @Column(name = "IMSVB", precision = 18, scale = 8)
  public BigDecimal getImsvb() {
    return this.imsvb;
  }

  public void setImsvb(BigDecimal imsvb) {
    this.imsvb = imsvb;
  }

  @Column(name = "IMSVBEU", precision = 18, scale = 8)
  public BigDecimal getImsvbeu() {
    return this.imsvbeu;
  }

  public void setImsvbeu(BigDecimal imsvbeu) {
    this.imsvbeu = imsvbeu;
  }

  @Column(name = "IMBRMERC", precision = 18, scale = 8)
  public BigDecimal getImbrmerc() {
    return this.imbrmerc;
  }

  public void setImbrmerc(BigDecimal imbrmerc) {
    this.imbrmerc = imbrmerc;
  }

  @Column(name = "IMBRMREU", precision = 18, scale = 8)
  public BigDecimal getImbrmreu() {
    return this.imbrmreu;
  }

  public void setImbrmreu(BigDecimal imbrmreu) {
    this.imbrmreu = imbrmreu;
  }

  @Column(name = "IMGASTOS", precision = 18, scale = 8)
  public BigDecimal getImgastos() {
    return this.imgastos;
  }

  public void setImgastos(BigDecimal imgastos) {
    this.imgastos = imgastos;
  }

  @Column(name = "IMGATDVS", precision = 18, scale = 8)
  public BigDecimal getImgatdvs() {
    return this.imgatdvs;
  }

  public void setImgatdvs(BigDecimal imgatdvs) {
    this.imgatdvs = imgatdvs;
  }

  @Column(name = "IMIMPDVS", precision = 18, scale = 8)
  public BigDecimal getImimpdvs() {
    return this.imimpdvs;
  }

  public void setImimpdvs(BigDecimal imimpdvs) {
    this.imimpdvs = imimpdvs;
  }

  @Column(name = "IMIMPEUR", precision = 18, scale = 8)
  public BigDecimal getImimpeur() {
    return this.imimpeur;
  }

  public void setImimpeur(BigDecimal imimpeur) {
    this.imimpeur = imimpeur;
  }

  @Column(name = "IMCCONEU", precision = 18, scale = 8)
  public BigDecimal getImcconeu() {
    return this.imcconeu;
  }

  public void setImcconeu(BigDecimal imcconeu) {
    this.imcconeu = imcconeu;
  }

  @Column(name = "IMCLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcliqeu() {
    return this.imcliqeu;
  }

  public void setImcliqeu(BigDecimal imcliqeu) {
    this.imcliqeu = imcliqeu;
  }

  @Column(name = "IMDEREEU", precision = 18, scale = 8)
  public BigDecimal getImdereeu() {
    return this.imdereeu;
  }

  public void setImdereeu(BigDecimal imdereeu) {
    this.imdereeu = imdereeu;
  }

  @Column(name = "NUTITAGR", precision = 16, scale = 5)
  public BigDecimal getNutitagr() {
    return this.nutitagr;
  }

  public void setNutitagr(BigDecimal nutitagr) {
    this.nutitagr = nutitagr;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION")
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0cto")
  public List<Tmct0ctg> getTmct0ctgs() {
    return this.tmct0ctgs;
  }

  public void setTmct0ctgs(List<Tmct0ctg> tmct0ctgs) {
    this.tmct0ctgs = tmct0ctgs;
  }

}
