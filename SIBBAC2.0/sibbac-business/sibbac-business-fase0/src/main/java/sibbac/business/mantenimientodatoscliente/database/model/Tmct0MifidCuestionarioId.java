package sibbac.business.mantenimientodatoscliente.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0MifidCuestionarioId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "IDEMPR", nullable = false, length = 4)
	private String idempr;
	
	@Column(name = "IDCUEST", nullable = false, length = 5)
	private String idcuest;

	@Column(name = "CODSEG", nullable = false, length = 2)
	private String codseg;

	public Tmct0MifidCuestionarioId() {
	}

	public Tmct0MifidCuestionarioId(String idempr, String idcuest, String codseg) {
		super();
		this.idempr = idempr;
		this.idcuest = idcuest;
		this.codseg = codseg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codseg == null) ? 0 : codseg.hashCode());
		result = prime * result + ((idcuest == null) ? 0 : idcuest.hashCode());
		result = prime * result + ((idempr == null) ? 0 : idempr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tmct0MifidCuestionarioId other = (Tmct0MifidCuestionarioId) obj;
		if (codseg == null) {
			if (other.codseg != null)
				return false;
		} else if (!codseg.equals(other.codseg))
			return false;
		if (idcuest == null) {
			if (other.idcuest != null)
				return false;
		} else if (!idcuest.equals(other.idcuest))
			return false;
		if (idempr == null) {
			if (other.idempr != null)
				return false;
		} else if (!idempr.equals(other.idempr))
			return false;
		return true;
	}

	public String getIdempr() {
		return idempr;
	}

	public void setIdempr(String idempr) {
		this.idempr = idempr;
	}

	public String getIdcuest() {
		return idcuest;
	}

	public void setIdcuest(String idcuest) {
		this.idcuest = idcuest;
	}

	public String getCodseg() {
		return codseg;
	}

	public void setCodseg(String codseg) {
		this.codseg = codseg;
	}
	
	
	

}
