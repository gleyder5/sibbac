package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0ctdId implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -2173661790504569525L;
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden;
  @Column(name = "CDBROKER", nullable = false, length = 20)
  private String cdbroker;
  @Column(name = "CDMERCAD", nullable = false, length = 20)
  private String cdmercad;
  @Column(name = "NBOOKING", nullable = false, length = 20)
  private String nbooking;
  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  private String nucnfclt;
  @Column(name = "NUALOSEC", nullable = false, length = 5, scale = 0)
  private Integer nualosec;

  public Tmct0ctdId() {
  }

  public Tmct0ctdId(String nuorden, String cdbroker, String cdmercad, String nbooking, String nucnfclt, Integer nualosec) {
    super();
    this.nuorden = nuorden;
    this.cdbroker = cdbroker;
    this.cdmercad = cdmercad;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nualosec = nualosec;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public String getCdmercad() {
    return cdmercad;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public Integer getNualosec() {
    return nualosec;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNualosec(Integer nualosec) {
    this.nualosec = nualosec;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbroker == null) ? 0 : cdbroker.hashCode());
    result = prime * result + ((cdmercad == null) ? 0 : cdmercad.hashCode());
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nualosec == null) ? 0 : nualosec.hashCode());
    result = prime * result + ((nucnfclt == null) ? 0 : nucnfclt.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0ctdId other = (Tmct0ctdId) obj;
    if (cdbroker == null) {
      if (other.cdbroker != null)
        return false;
    } else if (!cdbroker.equals(other.cdbroker))
      return false;
    if (cdmercad == null) {
      if (other.cdmercad != null)
        return false;
    } else if (!cdmercad.equals(other.cdmercad))
      return false;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    } else if (!nbooking.equals(other.nbooking))
      return false;
    if (nualosec == null) {
      if (other.nualosec != null)
        return false;
    } else if (!nualosec.equals(other.nualosec))
      return false;
    if (nucnfclt == null) {
      if (other.nucnfclt != null)
        return false;
    } else if (!nucnfclt.equals(other.nucnfclt))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    } else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tmct0ctdId %s##%s##%s##%s##%s##%s", nuorden, cdbroker, cdmercad, nbooking, nucnfclt, nualosec);
  }

}
