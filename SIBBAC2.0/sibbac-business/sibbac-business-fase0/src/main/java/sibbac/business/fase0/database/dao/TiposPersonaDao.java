package sibbac.business.fase0.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.TiposPersona;

@Repository
public interface TiposPersonaDao extends JpaRepository< TiposPersona, Long > {

}
