package sibbac.business.fase0.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.SalesTrader;

@Repository
public interface SalesTraderDao extends JpaRepository<SalesTrader, String> {

  @Query("select s from SalesTrader s where s.codigoCorto = :codigoCorto and s.fhfinal >= :fecha and s.fhinicio <= :fecha")
  List<SalesTrader> findAllByCodigoCortoAndFecha(@Param("codigoCorto") int codigoCorto, @Param("fecha") Date fecha);

}
