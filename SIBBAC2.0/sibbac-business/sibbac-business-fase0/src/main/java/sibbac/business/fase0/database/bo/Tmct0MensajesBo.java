package sibbac.business.fase0.database.bo;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0MensajesDao;
import sibbac.business.fase0.database.model.Tmct0Mensajes;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0MensajesBo extends AbstractBo< Tmct0Mensajes, Long, Tmct0MensajesDao > {

	public Page< Tmct0Mensajes > getLast10Messages() {
		return this.dao.findAll( new PageRequest( 0, 10, Direction.DESC, "horaRecepcion" ) );
		// return this.dao.findAll( new PageRequest( 0, 10, this.getBasicSort() ) );
		// return this.dao.findAll( new PageRequest( 0, 10, this.getComplexSort() ) );
	}
	/*
	 * private Sort getBasicSort() {
	 * return new Sort( Direction.DESC, "horaRecepcion" );
	 * }
	 * 
	 * private Sort getComplexSort() {
	 * return new Sort(
	 * new Order(Direction.DESC, "horaRecepcion"),
	 * new Order(Direction.ASC, "tmct0MensajesR00s.idMensajeR00")
	 * );
	 * }
	 */
}
