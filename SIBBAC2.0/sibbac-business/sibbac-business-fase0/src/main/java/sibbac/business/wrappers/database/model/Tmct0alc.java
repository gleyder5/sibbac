package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.model.Tmct0estado;

@Entity
@Table(name = "TMCT0ALC")
public class Tmct0alc implements Serializable {

  /**
   * Número de serie asignado automáticamente
   */
  private static final long serialVersionUID = -9134573668688368949L;

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0alc.class);

  private Tmct0alcId id;
  private Tmct0alo tmct0alo;
  private Tmct0estado estadoentrec;
  private Tmct0estado estadocont;
  private Tmct0estado estadocontMercado;
  private Tmct0estado estadocontCanon;
  private Tmct0estado estadocontDevolucion;
  private Tmct0estado estadotit;
  private Tmct0sta tmct0sta;
  private BigDecimal nutitliq;
  private String cdentliq;
  private BigDecimal imcombco;
  private BigDecimal imcomdvo;
  private BigDecimal imcomsvb;
  private Date feejeliq;
  private Date feopeliq;
  private String cdestliq;
  private String cdrefban;
  private String cdmoniso;
  private BigDecimal imntbrok;
  private String cdmodord;
  private String cdenvmor;
  private String nbtitliq;
  private String cdniftit;
  private BigDecimal pccombco;
  private BigDecimal pccomdev;
  private BigDecimal pccombrk;
  private String cdcomisn;
  private String tpdsaldo;
  private BigDecimal imnetliq;
  private BigDecimal imnfiliq;
  private String cdenvliq;
  private String nurefbrk;
  private String cdentdep;
  private String cddepcle;
  private Character cdcleari;
  private String cdreftit;
  private String cdusuaud;
  private Date fhaudit;
  private Integer nuversion;
  private String desenliq;
  private String cdordtit;
  private String cdcustod;
  private String rftitaud;
  private String acctatcle;
  private String pkentity;
  private Character flagliq;
  private String ourpar;
  private String theirpar;
  private BigDecimal imefeagr;
  private BigDecimal imcombrk;
  private BigDecimal imfinsvb;
  private BigDecimal imajusvb;
  private BigDecimal imtotnet;
  private BigDecimal imtotbru;
  private BigDecimal pccomisn;
  private BigDecimal imcomisn;
  private BigDecimal imderliq;
  private BigDecimal imderges;
  private char cdtyp;
  private Character cdsntprt;
  private Long idinfocomp;
  private Integer cdestadoasig;
  private Character caseecc;
  private Character asigorden;
  private Character block;
  private Date fenvasignacion;
  private Date fenvtitular;
  private BigDecimal imcanoncompdv;
  private BigDecimal imcanoncompeu;
  private BigDecimal imcanoncontrdv;
  private BigDecimal imcanoncontreu;
  private BigDecimal imcanonliqdv;
  private BigDecimal imcanonliqeu;
  private Date fhenvios3;
  private Character enviadoCuentaVirtual;
  private Date concEfMercado;
  private Date concEfCliente;
  private List<Tmct0desglosecamara> tmct0desglosecamaras = new ArrayList<Tmct0desglosecamara>();
  private List<AlcEstadoCont> estadosContables = new ArrayList<AlcEstadoCont>();

  private Character liquidadocliente;

  /**
   * Número de títulos fallidos si el sentido de la operación es 'venta' y
   * número de títulos afectados si el sentido de la operación es 'compra'.
   */
  private BigDecimal nutitfallidos;

  private Boolean clearing;
  private Boolean facturar;
  private Long auxiliar;

  private Character corretajePti;

  private BigDecimal imcomsis = BigDecimal.ZERO;
  private BigDecimal imbansis = BigDecimal.ZERO;

  private Tmct0referenciatitular referenciaTitular;

  private Tmct0infocompensacion tmct0infocompensacion;

  private BigDecimal imcobrado = BigDecimal.ZERO;
  private BigDecimal imconciliado = BigDecimal.ZERO;
  private BigDecimal imefeMercadoCobrado = BigDecimal.ZERO;
  private BigDecimal imefeClienteCobrado = BigDecimal.ZERO;
  private BigDecimal imcanonCobrado = BigDecimal.ZERO;
  private BigDecimal imcomCobrado = BigDecimal.ZERO;
  private BigDecimal imbrkPagado = BigDecimal.ZERO;
  private BigDecimal imdvoPagado = BigDecimal.ZERO;

  private BigDecimal imCanonBasico = BigDecimal.ZERO;
  private BigDecimal imCanonEspecial = BigDecimal.ZERO;
  private Character canonCalculado = 'N';

  public Tmct0alc() {
  }

  public Tmct0alc(Tmct0alcId id, Tmct0alo tmct0alo, Tmct0sta tmct0sta, String cdordtit, String cdcustod,
      String rftitaud, String acctatcle, String pkentity, BigDecimal imtotnet, BigDecimal imtotbru,
      BigDecimal pccomisn, BigDecimal imcomisn, BigDecimal imderliq, BigDecimal imderges, char cdtyp) {
    this.id = id;
    this.tmct0alo = tmct0alo;
    this.tmct0sta = tmct0sta;
    this.cdordtit = cdordtit;
    this.cdcustod = cdcustod;
    this.rftitaud = rftitaud;
    this.acctatcle = acctatcle;
    this.pkentity = pkentity;
    this.imtotnet = imtotnet;
    this.imtotbru = imtotbru;
    this.pccomisn = pccomisn;
    this.imcomisn = imcomisn;
    this.imderliq = imderliq;
    this.imderges = imderges;
    this.cdtyp = cdtyp;
  }

  public Tmct0alc(Tmct0alcId id, Tmct0alo tmct0alo, Tmct0estado estadoentrec, Tmct0estado estadocont,
      Tmct0estado estadotit, Tmct0sta tmct0sta, BigDecimal nutitliq, String cdentliq, BigDecimal imcombco,
      BigDecimal imcomdvo, BigDecimal imcomsvb, Date feejeliq, Date feopeliq, String cdestliq, String cdrefban,
      String cdmoniso, BigDecimal imntbrok, String cdmodord, String cdenvmor, String nbtitliq, String cdniftit,
      BigDecimal pccombco, BigDecimal pccomdev, BigDecimal pccombrk, String cdcomisn, String tpdsaldo,
      BigDecimal imnetliq, BigDecimal imnfiliq, String cdenvliq, String nurefbrk, String cdentdep, String cddepcle,
      Character cdcleari, String cdreftit, String cdusuaud, Date fhaudit, Integer nuversion, String desenliq,
      String cdordtit, String cdcustod, String rftitaud, String acctatcle, String pkentity, Character flagliq,
      String ourpar, String theirpar, BigDecimal imefeagr, BigDecimal imcombrk, BigDecimal imfinsvb,
      BigDecimal imajusvb, BigDecimal imtotnet, BigDecimal imtotbru, BigDecimal pccomisn, BigDecimal imcomisn,
      BigDecimal imderliq, BigDecimal imderges, char cdtyp, Character cdsntprt, Long idinfocomp, Integer cdestadoasig,
      Character caseecc, Character asigorden, Character block, Date fenvasignacion, Date fenvtitular,
      BigDecimal imcanoncompdv, BigDecimal imcanoncompeu, BigDecimal imcanoncontrdv, BigDecimal imcanoncontreu,
      BigDecimal imcanonliqdv, BigDecimal imcanonliqeu, Date fhenvios3, List<Tmct0desglosecamara> tmct0desglosecamaras/*
                                                                                                                       * ,
                                                                                                                       * Set
                                                                                                                       * <
                                                                                                                       * Tmct0ald
                                                                                                                       * >
                                                                                                                       * tmct0alds
                                                                                                                       */) {
    this.id = id;
    this.tmct0alo = tmct0alo;
    this.estadoentrec = estadoentrec;
    this.estadocont = estadocont;
    this.estadotit = estadotit;
    this.tmct0sta = tmct0sta;
    this.nutitliq = nutitliq;
    this.cdentliq = cdentliq;
    this.imcombco = imcombco;
    this.imcomdvo = imcomdvo;
    this.imcomsvb = imcomsvb;
    this.feejeliq = feejeliq;
    this.feopeliq = feopeliq;
    this.cdestliq = cdestliq;
    this.cdrefban = cdrefban;
    this.cdmoniso = cdmoniso;
    this.imntbrok = imntbrok;
    this.cdmodord = cdmodord;
    this.cdenvmor = cdenvmor;
    this.nbtitliq = nbtitliq;
    this.cdniftit = cdniftit;
    this.pccombco = pccombco;
    this.pccomdev = pccomdev;
    this.pccombrk = pccombrk;
    this.cdcomisn = cdcomisn;
    this.tpdsaldo = tpdsaldo;
    this.imnetliq = imnetliq;
    this.imnfiliq = imnfiliq;
    this.cdenvliq = cdenvliq;
    this.nurefbrk = nurefbrk;
    this.cdentdep = cdentdep;
    this.cddepcle = cddepcle;
    this.cdcleari = cdcleari;
    this.cdreftit = cdreftit;
    this.cdusuaud = cdusuaud;
    this.fhaudit = fhaudit;
    this.nuversion = nuversion;
    this.desenliq = desenliq;
    this.cdordtit = cdordtit;
    this.cdcustod = cdcustod;
    this.rftitaud = rftitaud;
    this.acctatcle = acctatcle;
    this.pkentity = pkentity;
    this.flagliq = flagliq;
    this.ourpar = ourpar;
    this.theirpar = theirpar;
    this.imefeagr = imefeagr;
    this.imcombrk = imcombrk;
    this.imfinsvb = imfinsvb;
    this.imajusvb = imajusvb;
    this.imtotnet = imtotnet;
    this.imtotbru = imtotbru;
    this.pccomisn = pccomisn;
    this.imcomisn = imcomisn;
    this.imderliq = imderliq;
    this.imderges = imderges;
    this.cdtyp = cdtyp;
    this.cdsntprt = cdsntprt;
    this.idinfocomp = idinfocomp;
    this.cdestadoasig = cdestadoasig;
    this.caseecc = caseecc;
    this.asigorden = asigorden;
    this.block = block;
    this.fenvasignacion = fenvasignacion;
    this.fenvtitular = fenvtitular;
    this.imcanoncompdv = imcanoncompdv;
    this.imcanoncompeu = imcanoncompeu;
    this.imcanoncontrdv = imcanoncontrdv;
    this.imcanoncontreu = imcanoncontreu;
    this.imcanonliqdv = imcanonliqdv;
    this.imcanonliqeu = imcanonliqeu;
    this.fhenvios3 = fhenvios3;
    this.tmct0desglosecamaras = tmct0desglosecamaras;
    // this.tmct0alds = tmct0alds;
  }

  public Tmct0alc(Tmct0alcId id) {
    super();
    this.id = id;
  }

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "nbooking", column = @Column(name = "NBOOKING", nullable = false, length = 20)),
      @AttributeOverride(name = "nuorden", column = @Column(name = "NUORDEN", nullable = false, length = 20)),
      @AttributeOverride(name = "nucnfliq", column = @Column(name = "NUCNFLIQ", nullable = false, precision = 4)),
      @AttributeOverride(name = "nucnfclt", column = @Column(name = "NUCNFCLT", nullable = false, length = 20)) })
  public Tmct0alcId getId() {
    return this.id;
  }

  public void setId(Tmct0alcId id) {
    this.id = id;
  }

  @Transient
  public String getNuorden() {
    String nuorden = null;
    final Tmct0alcId id = this.getId();
    if (id != null) {
      nuorden = id.getNuorden();
    }
    return nuorden;
  }

  @Transient
  public String getNbooking() {
    String nbooking = null;
    final Tmct0alcId id = this.getId();
    if (id != null) {
      nbooking = id.getNbooking();
    }
    return nbooking;
  }

  @Transient
  public String getNucnfclt() {
    String nucnfclt = null;
    final Tmct0alcId id = this.getId();
    if (id != null) {
      nucnfclt = id.getNucnfclt();
    }
    return nucnfclt;
  }

  @Transient
  public Short getNucnfliq() {
    Short nucnfliq = null;
    final Tmct0alcId id = this.getId();
    if (id != null) {
      nucnfliq = id.getNucnfliq();
    }
    return nucnfliq;
  }

  @Transient
  public Character getCdtpoper() {
    Character cdtpoper = null;
    final Tmct0alo tmct0alo = this.getTmct0alo();
    if (tmct0alo != null) {
      cdtpoper = tmct0alo.getCdtpoper();
    }
    return cdtpoper;
  }

  @Transient
  public String getCdisin() {
    String cdisin = null;
    final Tmct0alo tmct0alo = this.getTmct0alo();
    if (tmct0alo != null) {
      cdisin = tmct0alo.getCdisin();
    }
    return cdisin;
  }

  @Transient
  public String getNbvalors() {
    String nbvalors = null;
    final Tmct0alo tmct0alo = this.getTmct0alo();
    if (tmct0alo != null) {
      nbvalors = tmct0alo.getNbvalors();
    }
    return nbvalors;
  }

  @Transient
  public String getCdmercad() {
    String cdmercad = null;
    final Tmct0alo tmct0alo = this.getTmct0alo();
    if (tmct0alo != null) {
      cdmercad = tmct0alo.getCdmercad();
    }
    return cdmercad;
  }

  @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "NUCNFCLT", referencedColumnName = "NUCNFCLT", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NBOOKING", referencedColumnName = "NBOOKING", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUORDEN", referencedColumnName = "NUORDEN", nullable = false, insertable = false, updatable = false) })
  public Tmct0alo getTmct0alo() {
    return this.tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOENTREC")
  public Tmct0estado getEstadoentrec() {
    return this.estadoentrec;
  }

  public void setEstadoentrec(Tmct0estado estadoentrec) {
    this.estadoentrec = estadoentrec;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT")
  public Tmct0estado getEstadocont() {
    return this.estadocont;
  }

  public void setEstadocont(Tmct0estado estadocont) {
    this.estadocont = estadocont;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT_MERCADO")
  public Tmct0estado getEstadocontMercado() {
    return estadocontMercado;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT_CANON")
  public Tmct0estado getEstadocontCanon() {
    return estadocontCanon;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT_DEVOL")
  public Tmct0estado getEstadocontDevolucion() {
    return estadocontDevolucion;
  }

  public void setEstadocontMercado(Tmct0estado estadocontMercado) {
    this.estadocontMercado = estadocontMercado;
  }

  public void setEstadocontCanon(Tmct0estado estadocontCanon) {
    this.estadocontCanon = estadocontCanon;
  }

  public void setEstadocontDevolucion(Tmct0estado estadocontDevolucion) {
    this.estadocontDevolucion = estadocontDevolucion;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOTIT")
  public Tmct0estado getEstadotit() {
    return this.estadotit;
  }

  public void setEstadotit(Tmct0estado estadotit) {
    this.estadotit = estadotit;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({ @JoinColumn(name = "CDESTADO", referencedColumnName = "CDESTADO", nullable = false),
      @JoinColumn(name = "CDTIPEST", referencedColumnName = "CDTIPEST", nullable = false) })
  public Tmct0sta getTmct0sta() {
    return this.tmct0sta;
  }

  public void setTmct0sta(Tmct0sta tmct0sta) {
    this.tmct0sta = tmct0sta;
  }

  @Column(name = "NUTITLIQ", precision = 16, scale = 5)
  public BigDecimal getNutitliq() {
    return this.nutitliq;
  }

  public void setNutitliq(BigDecimal nutitliq) {
    this.nutitliq = nutitliq;
  }

  @Column(name = "CDENTLIQ", length = 4)
  public String getCdentliq() {
    return this.cdentliq;
  }

  public void setCdentliq(String cdentliq) {
    this.cdentliq = cdentliq;
  }

  @Column(name = "IMCOMBCO", precision = 18, scale = 4)
  public BigDecimal getImcombco() {
    return this.imcombco;
  }

  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  @Column(name = "IMCOMDVO", precision = 18, scale = 4)
  public BigDecimal getImcomdvo() {
    return this.imcomdvo;
  }

  public void setImcomdvo(BigDecimal imcomdvo) {
    this.imcomdvo = imcomdvo;
  }

  @Column(name = "IMCOMSVB", precision = 18, scale = 4)
  public BigDecimal getImcomsvb() {
    return this.imcomsvb;
  }

  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJELIQ", length = 10)
  public Date getFeejeliq() {
    return this.feejeliq;
  }

  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  @Temporal(TemporalType.DATE)
  @Column(name = "FEOPELIQ", length = 10)
  public Date getFeopeliq() {
    return this.feopeliq;
  }

  public void setFeopeliq(Date feopeliq) {
    this.feopeliq = feopeliq;
  }

  @Column(name = "CDESTLIQ", length = 3)
  public String getCdestliq() {
    return this.cdestliq;
  }

  public void setCdestliq(String cdestliq) {
    this.cdestliq = cdestliq;
  }

  @Column(name = "CDREFBAN", length = 32)
  public String getCdrefban() {
    return this.cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  @Column(name = "CDMONISO", length = 3)
  public String getCdmoniso() {
    return this.cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  @Column(name = "IMNTBROK", precision = 18, scale = 4)
  public BigDecimal getImntbrok() {
    return this.imntbrok;
  }

  public void setImntbrok(BigDecimal imntbrok) {
    this.imntbrok = imntbrok;
  }

  @Column(name = "CDMODORD", length = 3)
  public String getCdmodord() {
    return this.cdmodord;
  }

  public void setCdmodord(String cdmodord) {
    this.cdmodord = cdmodord;
  }

  @Column(name = "CDENVMOR", length = 3)
  public String getCdenvmor() {
    return this.cdenvmor;
  }

  public void setCdenvmor(String cdenvmor) {
    this.cdenvmor = cdenvmor;
  }

  @Column(name = "NBTITLIQ", length = 60)
  public String getNbtitliq() {
    return this.nbtitliq;
  }

  public void setNbtitliq(String nbtitliq) {
    this.nbtitliq = nbtitliq;
  }

  @Column(name = "CDNIFTIT", length = 11)
  public String getCdniftit() {
    return this.cdniftit;
  }

  public void setCdniftit(String cdniftit) {
    this.cdniftit = cdniftit;
  }

  @Column(name = "PCCOMBCO", precision = 7, scale = 4)
  public BigDecimal getPccombco() {
    return this.pccombco;
  }

  public void setPccombco(BigDecimal pccombco) {
    this.pccombco = pccombco;
  }

  @Column(name = "PCCOMDEV", precision = 7, scale = 4)
  public BigDecimal getPccomdev() {
    return this.pccomdev;
  }

  public void setPccomdev(BigDecimal pccomdev) {
    this.pccomdev = pccomdev;
  }

  @Column(name = "PCCOMBRK", precision = 16, scale = 6)
  public BigDecimal getPccombrk() {
    return this.pccombrk;
  }

  public void setPccombrk(BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  @Column(name = "CDCOMISN", length = 3)
  public String getCdcomisn() {
    return this.cdcomisn;
  }

  public void setCdcomisn(String cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  @Column(name = "TPDSALDO", length = 3)
  public String getTpdsaldo() {
    return this.tpdsaldo;
  }

  public void setTpdsaldo(String tpdsaldo) {
    this.tpdsaldo = tpdsaldo;
  }

  @Column(name = "IMNETLIQ", precision = 18, scale = 4)
  public BigDecimal getImnetliq() {
    return this.imnetliq;
  }

  public void setImnetliq(BigDecimal imnetliq) {
    this.imnetliq = imnetliq;
  }

  @Column(name = "IMNFILIQ", precision = 18, scale = 4)
  public BigDecimal getImnfiliq() {
    return this.imnfiliq;
  }

  public void setImnfiliq(BigDecimal imnfiliq) {
    this.imnfiliq = imnfiliq;
  }

  @Column(name = "CDENVLIQ", length = 3)
  public String getCdenvliq() {
    return this.cdenvliq;
  }

  public void setCdenvliq(String cdenvliq) {
    this.cdenvliq = cdenvliq;
  }

  @Column(name = "NUREFBRK", length = 15)
  public String getNurefbrk() {
    return this.nurefbrk;
  }

  public void setNurefbrk(String nurefbrk) {
    this.nurefbrk = nurefbrk;
  }

  @Column(name = "CDENTDEP", length = 4)
  public String getCdentdep() {
    return this.cdentdep;
  }

  public void setCdentdep(String cdentdep) {
    this.cdentdep = cdentdep;
  }

  @Column(name = "CDDEPCLE", length = 3)
  public String getCddepcle() {
    return this.cddepcle;
  }

  public void setCddepcle(String cddepcle) {
    this.cddepcle = cddepcle;
  }

  @Column(name = "CDCLEARI", length = 1)
  public Character getCdcleari() {
    return this.cdcleari;
  }

  public void setCdcleari(Character cdcleari) {
    this.cdcleari = cdcleari;
  }

  @Column(name = "CDREFTIT", length = 20)
  public String getCdreftit() {
    return this.cdreftit;
  }

  public void setCdreftit(String cdreftit) {
    this.cdreftit = cdreftit;
  }

  @Column(name = "CDUSUAUD", length = 10)
  public String getCdusuaud() {
    return this.cdusuaud;
  }

  public void setCdusuaud(String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHAUDIT", length = 26)
  public Date getFhaudit() {
    return this.fhaudit;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

  @Column(name = "NUVERSION")
  public Integer getNuversion() {
    return this.nuversion;
  }

  public void setNuversion(Integer nuversion) {
    this.nuversion = nuversion;
  }

  @Column(name = "DESENLIQ", length = 150)
  public String getDesenliq() {
    return this.desenliq;
  }

  public void setDesenliq(String desenliq) {
    this.desenliq = desenliq;
  }

  @Column(name = "CDORDTIT", nullable = false, length = 25)
  public String getCdordtit() {
    return this.cdordtit;
  }

  public void setCdordtit(String cdordtit) {
    this.cdordtit = cdordtit;
  }

  @Column(name = "CDCUSTOD", nullable = false, length = 25)
  public String getCdcustod() {
    return this.cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  @Column(name = "RFTITAUD", nullable = false, length = 20)
  public String getRftitaud() {
    return this.rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  @Column(name = "ACCTATCLE", nullable = false, length = 34)
  public String getAcctatcle() {
    return this.acctatcle;
  }

  public void setAcctatcle(String acctatcle) {
    this.acctatcle = acctatcle;
  }

  @Column(name = "PKENTITY", nullable = false, length = 16)
  public String getPkentity() {
    return this.pkentity;
  }

  public void setPkentity(String pkentity) {
    this.pkentity = pkentity;
  }

  @Column(name = "FLAGLIQ", length = 1)
  public Character getFlagliq() {
    return this.flagliq;
  }

  public void setFlagliq(Character flagliq) {
    this.flagliq = flagliq;
  }

  @Column(name = "OURPAR", length = 50)
  public String getOurpar() {
    return this.ourpar;
  }

  public void setOurpar(String ourpar) {
    this.ourpar = ourpar;
  }

  @Column(name = "THEIRPAR", length = 50)
  public String getTheirpar() {
    return this.theirpar;
  }

  public void setTheirpar(String theirpar) {
    this.theirpar = theirpar;
  }

  @Column(name = "IMEFEAGR", precision = 18, scale = 4)
  public BigDecimal getImefeagr() {
    return this.imefeagr;
  }

  public void setImefeagr(BigDecimal imefeagr) {
    this.imefeagr = imefeagr;
  }

  @Column(name = "IMCOMBRK", precision = 18, scale = 4)
  public BigDecimal getImcombrk() {
    return this.imcombrk;
  }

  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  @Column(name = "IMFINSVB", precision = 18, scale = 4)
  public BigDecimal getImfinsvb() {
    return this.imfinsvb;
  }

  public void setImfinsvb(BigDecimal imfinsvb) {
    this.imfinsvb = imfinsvb;
  }

  @Column(name = "IMAJUSVB", precision = 18, scale = 4)
  public BigDecimal getImajusvb() {
    return this.imajusvb;
  }

  public void setImajusvb(BigDecimal imajusvb) {
    this.imajusvb = imajusvb;
  }

  @Column(name = "IMTOTNET", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImtotnet() {
    return this.imtotnet;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  @Column(name = "IMTOTBRU", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImtotbru() {
    return this.imtotbru;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  @Column(name = "PCCOMISN", nullable = false, precision = 16, scale = 6)
  public BigDecimal getPccomisn() {
    return this.pccomisn;
  }

  public void setPccomisn(BigDecimal pccomisn) {
    this.pccomisn = pccomisn;
  }

  @Column(name = "IMCOMISN", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImcomisn() {
    return this.imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  @Column(name = "IMDERLIQ", nullable = false, precision = 18, scale = 4)
  public BigDecimal getImderliq() {
    return this.imderliq;
  }

  public void setImderliq(BigDecimal imderliq) {
    this.imderliq = imderliq;
  }

  @Column(name = "IMDERGES", nullable = false, precision = 18, scale = 4)
  public BigDecimal getImderges() {
    return this.imderges;
  }

  public void setImderges(BigDecimal imderges) {
    this.imderges = imderges;
  }

  @Column(name = "CDTYP", nullable = false, length = 1)
  public char getCdtyp() {
    return this.cdtyp;
  }

  public void setCdtyp(char cdtyp) {
    this.cdtyp = cdtyp;
  }

  @Column(name = "CDSNTPRT", length = 1)
  public Character getCdsntprt() {
    return this.cdsntprt;
  }

  public void setCdsntprt(Character cdsntprt) {
    this.cdsntprt = cdsntprt;
  }

  @Column(name = "IDINFOCOMP")
  public Long getIdinfocomp() {
    return this.idinfocomp;
  }

  public void setIdinfocomp(Long idinfocomp) {
    this.idinfocomp = idinfocomp;
  }

  @Column(name = "CDESTADOASIG", precision = 4)
  public Integer getCdestadoasig() {
    return this.cdestadoasig;
  }

  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  @Column(name = "CASEECC", length = 1)
  public Character getCaseecc() {
    return this.caseecc;
  }

  public void setCaseecc(Character caseecc) {
    this.caseecc = caseecc;
  }

  @Column(name = "ASIGORDEN", length = 1)
  public Character getAsigorden() {
    return this.asigorden;
  }

  public void setAsigorden(Character asigorden) {
    this.asigorden = asigorden;
  }

  @Column(name = "BLOCK", length = 1)
  public Character getBlock() {
    return this.block;
  }

  public void setBlock(Character block) {
    this.block = block;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FENVASIGNACION", length = 26)
  public Date getFenvasignacion() {
    return this.fenvasignacion;
  }

  public void setFenvasignacion(Date fenvasignacion) {
    this.fenvasignacion = fenvasignacion;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FENVTITULAR", length = 26)
  public Date getFenvtitular() {
    return this.fenvtitular;
  }

  public void setFenvtitular(Date fenvtitular) {
    this.fenvtitular = fenvtitular;
  }

  @Column(name = "IMCANONCOMPDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncompdv() {
    return this.imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  @Column(name = "IMCANONCOMPEU", precision = 18, scale = 8)
  public BigDecimal getImcanoncompeu() {
    return this.imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  @Column(name = "IMCANONCONTRDV", precision = 18, scale = 8)
  public BigDecimal getImcanoncontrdv() {
    return this.imcanoncontrdv;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  @Column(name = "IMCANONCONTREU", precision = 18, scale = 8)
  public BigDecimal getImcanoncontreu() {
    return this.imcanoncontreu;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  @Column(name = "IMCANONLIQDV", precision = 18, scale = 8)
  public BigDecimal getImcanonliqdv() {
    return this.imcanonliqdv;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  @Column(name = "IMCANONLIQEU", precision = 18, scale = 8)
  public BigDecimal getImcanonliqeu() {
    return this.imcanonliqeu;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "FHENVIOS3", length = 26)
  public Date getFhenvios3() {
    return this.fhenvios3;
  }

  public void setFhenvios3(Date fhenvios3) {
    this.fhenvios3 = fhenvios3;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0alc", cascade = CascadeType.PERSIST)
  public List<Tmct0desglosecamara> getTmct0desglosecamaras() {
    return this.tmct0desglosecamaras;
  }

  public void setTmct0desglosecamaras(List<Tmct0desglosecamara> tmct0desglosecamaras) {
    this.tmct0desglosecamaras = tmct0desglosecamaras;
  }

  @Transactional
  public boolean addTmct0desglosecamara(final Tmct0desglosecamara dc) {
    if (CollectionUtils.isEmpty(tmct0desglosecamaras)) {
      this.tmct0desglosecamaras = new ArrayList<Tmct0desglosecamara>();
    }
    dc.setTmct0alc(this);

    return tmct0desglosecamaras.add(dc);

  }

  // @OneToMany( fetch = FetchType.LAZY, cascade=CascadeType.PERSIST )
  // @OneToMany( fetch = FetchType.LAZY, mappedBy = "alc",
  // cascade=CascadeType.PERSIST )
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "alc")
  // @OneToMany
  // @JoinColumns({
  // @JoinColumn( name = "NUORDEN", referencedColumnName = "NUORDEN"),
  // @JoinColumn( name = "NBOOKING", referencedColumnName = "NBOOKING"),
  // @JoinColumn( name = "NUCNFCLT", referencedColumnName = "NUCNFCLT"),
  // @JoinColumn( name = "NUCNFLIQ", referencedColumnName = "NUCNFLIQ")
  // } )
  public List<AlcEstadoCont> getEstadosContables() {
    return estadosContables;
  }

  public void setEstadosContables(List<AlcEstadoCont> estadosContables) {
    this.estadosContables = estadosContables;
  }

  @Column(name = "LIQUIDADOCLIENTE", length = 1)
  public Character getLiquidadocliente() {
    return liquidadocliente;
  }

  public void setLiquidadocliente(Character liquidadocliente) {
    this.liquidadocliente = liquidadocliente;
  }

  // @OneToMany(fetch = FetchType.LAZY, mappedBy = "tmct0alc")
  // public Set<Tmct0ald> getTmct0alds() {
  // return this.tmct0alds;
  // }
  //
  // public void setTmct0alds(Set<Tmct0ald> tmct0alds) {
  // this.tmct0alds = tmct0alds;
  // }

  @Column(name = "NUTITFALLIDOS", precision = 18, scale = 6)
  public BigDecimal getNutitfallidos() {
    return this.nutitfallidos;
  }

  public void setNutitfallidos(BigDecimal nutitfallidos) {
    this.nutitfallidos = nutitfallidos;
  }

  public Character getEnviadoCuentaVirtual() {
    return enviadoCuentaVirtual;
  }

  public void setEnviadoCuentaVirtual(Character enviadoCuentaVirtual) {
    this.enviadoCuentaVirtual = enviadoCuentaVirtual;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CONC_EF_MERCADO")
  public Date getConcEfMercado() {
    return concEfMercado;
  }

  public void setConcEfMercado(Date concEfMercado) {
    this.concEfMercado = concEfMercado;
  }

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CONC_EF_CLIENTE")
  public Date getConcEfCliente() {
    return concEfCliente;
  }

  public void setConcEfCliente(Date concEfCliente) {
    this.concEfCliente = concEfCliente;
  }

  @Transient
  public AlcEstadoCont addEstadosContables(TIPO_ESTADO tipoEstado, Tmct0estado estadoCont2) {
    if (estadosContables == null) {
      estadosContables = new ArrayList<AlcEstadoCont>();
    }
    AlcEstadoCont estadoCont = new AlcEstadoCont();
    estadoCont.setAlc(this);
    estadoCont.setTipoEstado(tipoEstado);
    estadoCont.setEstadoCont(estadoCont2);
    estadosContables.add(estadoCont);
    return estadoCont;
  }

  @Transient
  public Boolean isClearing() {
    return clearing;
  }

  public void setClearing(Boolean clearing) {
    this.clearing = clearing;
  }

  @Transient
  public Boolean isFacturar() {
    return facturar;
  }

  public void setFacturarBoolean(Boolean facturar) {
    this.facturar = facturar;
  }

  public void setFacturar(Character facturar) {
    setFacturarBoolean(facturar != null && 'S' == facturar);
  }

  @Transient
  public Long getAuxiliar() {
    return auxiliar;
  }

  public void setAuxiliar(Long auxiliar) {
    this.auxiliar = auxiliar;
  }

  @Column(name = "CORRETAJE_PTI", length = 1, nullable = true)
  public Character getCorretajePti() {
    return corretajePti;
  }

  public void setCorretajePti(Character corretajePti) {
    this.corretajePti = corretajePti;
  }

  @Column(name = "IMCOMSIS", precision = 18, scale = 4, nullable = true)
  public BigDecimal getImcomsis() {
    return imcomsis;
  }

  @Column(name = "IMBANSIS", precision = 18, scale = 4, nullable = true)
  public BigDecimal getImbansis() {
    return imbansis;
  }

  public void setImcomsis(BigDecimal imcomsis) {
    this.imcomsis = imcomsis;
  }

  public void setImbansis(BigDecimal imbansis) {
    this.imbansis = imbansis;
  }

  /**
   * @return the imcobrado
   */
  @Column(name = "IMCOBRADO", precision = 18, scale = 4)
  public BigDecimal getImcobrado() {
    return imcobrado;
  }

  @Column(name = "IMCONCILIADO", precision = 18, scale = 4)
  public BigDecimal getImconciliado() {
    return imconciliado;
  }

  @Column(name = "IMEFE_MERCADO_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImefeMercadoCobrado() {
    return imefeMercadoCobrado;
  }

  @Column(name = "IMEFE_CLIENTE_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImefeClienteCobrado() {
    return imefeClienteCobrado;
  }

  @Column(name = "IMCANON_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImcanonCobrado() {
    return imcanonCobrado;
  }

  @Column(name = "IMCOM_COBRADO", precision = 18, scale = 4)
  public BigDecimal getImcomCobrado() {
    return imcomCobrado;
  }

  @Column(name = "IMBRK_PAGADO", precision = 18, scale = 4)
  public BigDecimal getImbrkPagado() {
    return imbrkPagado;
  }

  @Column(name = "IMDVO_PAGADO", precision = 18, scale = 4)
  public BigDecimal getImdvoPagado() {
    return imdvoPagado;
  }

  /**
   * @param imcobrado the imcobrado to set
   */
  public void setImcobrado(BigDecimal imcobrado) {
    this.imcobrado = imcobrado;
  }

  public void setImconciliado(BigDecimal imconciliado) {
    this.imconciliado = imconciliado;
  }

  public void setImefeMercadoCobrado(BigDecimal imefeMercadoCobrado) {
    this.imefeMercadoCobrado = imefeMercadoCobrado;
  }

  public void setImefeClienteCobrado(BigDecimal imefeClienteCobrado) {
    this.imefeClienteCobrado = imefeClienteCobrado;
  }

  public void setImcanonCobrado(BigDecimal imcanonCobrado) {
    this.imcanonCobrado = imcanonCobrado;
  }

  public void setImcomCobrado(BigDecimal imcomCobrado) {
    this.imcomCobrado = imcomCobrado;
  }

  public void setImbrkPagado(BigDecimal imbrkPagado) {
    this.imbrkPagado = imbrkPagado;
  }

  public void setImdvoPagado(BigDecimal imdvoPagado) {
    this.imdvoPagado = imdvoPagado;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_REFERENCIA_TITULAR", nullable = true)
  public Tmct0referenciatitular getReferenciaTitular() {
    return referenciaTitular;
  }

  public void setReferenciaTitular(Tmct0referenciatitular referenciaTitular) {
    this.referenciaTitular = referenciaTitular;
  }

  @Transient
  public Tmct0infocompensacion getTmct0infocompensacion() {
    return tmct0infocompensacion;
  }

  public void setTmct0infocompensacion(Tmct0infocompensacion tmct0infocompensacion) {
    this.tmct0infocompensacion = tmct0infocompensacion;
  }

  /**
   * Determina si los estados de los desgloses camaras dependientes de este Alc
   * son consistentes
   * @return nulo en caso de que los estados asig no sean consistentes, no sean
   * mayores o iguales a ASIGNACIONES.PDTE_ENVIAR o los estados entrec no sean
   * consistentes. O un valor entero que representa el estado entrec compartido
   * entre todos los desgloses camara.
   */
  @Transient
  public Integer estadoConsistenteEnDesgloses() {
    Integer idEstadoAsigComun = null, idEstadoEntrecComun = null;
    Tmct0estado estado;

    for (Tmct0desglosecamara desglose : tmct0desglosecamaras) {
      // Estado asig
      estado = desglose.getTmct0estadoByCdestadoasig();
      if (estado == null || estado.getIdestado() == null) {
        LOG.warn("Encontrado desglose camara con estado asignado nulo. ID: {}", desglose.getIddesglosecamara());
        return null;
      }
      if (estado.getIdestado().intValue() == ASIGNACIONES.RECHAZADA.getId())
        continue;
      if (idEstadoAsigComun == null) {
        idEstadoAsigComun = estado.getIdestado();
        if (idEstadoAsigComun < ASIGNACIONES.PDTE_ENVIAR.getId())
          return null;
      }
      else if (!idEstadoAsigComun.equals(estado.getIdestado())) {
        LOG.warn("Encontrado desglose con estado asig incosistente {} != {} en alc {}", idEstadoAsigComun,
            estado.getIdestado(), getId());
        return null;
      }
      // Estado entrec
      estado = desglose.getTmct0estadoByCdestadoentrec();
      if (estado == null || estado.getIdestado() == null) {
        LOG.warn("Encontrado desglose camara con estado S3 nulo. ID: {}", desglose.getIddesglosecamara());
        return null;
      }
      if (idEstadoEntrecComun == null)
        idEstadoEntrecComun = estado.getIdestado();
      else if (!idEstadoEntrecComun.equals(estado.getIdestado()))
        return null; // Estados entrec diferentes
    }
    return idEstadoEntrecComun;
  }

  @Column(name = "IMCANON_ESPECIAL", precision = 18, scale = 4)
  public BigDecimal getImCanonEspecial() {
    return imCanonEspecial;
  }

  @Column(name = "CANON_CALCULADO", length = 1)
  public Character getCanonCalculado() {
    return canonCalculado;
  }
  
  @Column(name = "IMCANON_BASICO", precision = 18, scale = 4)
  public BigDecimal getImCanonBasico() {
    return imCanonBasico;
  }

  public void setImCanonBasico(BigDecimal imCanonBasico) {
    this.imCanonBasico = imCanonBasico;
  }

  public void setImCanonEspecial(BigDecimal imCanonEspecial) {
    this.imCanonEspecial = imCanonEspecial;
  }

  public void setCanonCalculado(Character canonCalculado) {
    this.canonCalculado = canonCalculado;
  }

  @Transient
  public boolean isCanonCalculoPendiente() {
    if(canonCalculado == null) {
      return true;
    }
    return canonCalculado.charValue() == 'N';
  }

}
