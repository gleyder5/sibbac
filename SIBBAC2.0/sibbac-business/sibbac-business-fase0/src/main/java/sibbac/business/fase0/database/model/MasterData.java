package sibbac.business.fase0.database.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

import sibbac.database.model.ATable;


@MappedSuperclass
@Embeddable
public abstract class MasterData< T extends MasterData< T >> extends ATable< MasterData< T >> {

	private static final long	serialVersionUID	= -2129895426619823828L;

	@Column( name = "NB_DESCRIPCION", length = 255 )
	private String				descripcion;
	@Column( name = "CD_CODIGO", length = 4 )
	private String				codigo;

	public String getDescripcion() {
		return descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

}
