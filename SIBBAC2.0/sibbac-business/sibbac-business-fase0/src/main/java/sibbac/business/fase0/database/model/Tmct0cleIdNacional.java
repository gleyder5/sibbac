package sibbac.business.fase0.database.model;

// Generated 14-may-2015 13:39:24 by Hibernate Tools 4.3.1

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Tmct0cleIdInternacional generated by hbm2java
 */
@Embeddable
public class Tmct0cleIdNacional implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1699388886369118080L;
  @Column(name = "CDCLEARE", nullable = false, length = 25)
  private String cdcleare;
  @Column(name = "CENTRO", nullable = false, length = 30)
  private String centro;
  @Column(name = "CDMERCAD", nullable = false, length = 20)
  private String cdmercad;
  @Column(name = "NUMSEC", nullable = false, precision = 20, scale = 0)
  private BigDecimal numsec;

  public Tmct0cleIdNacional() {
  }

  public Tmct0cleIdNacional(String cdcleare, String centro, String cdmercad, BigDecimal numsec) {
    this.cdcleare = cdcleare;
    this.centro = centro;
    this.cdmercad = cdmercad;
    this.numsec = numsec;
  }

  public String getCdcleare() {
    return this.cdcleare;
  }

  public void setCdcleare(String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public String getCentro() {
    return this.centro;
  }

  public void setCentro(String centro) {
    this.centro = centro;
  }

  public String getCdmercad() {
    return this.cdmercad;
  }

  public void setCdmercad(String cdmercad) {
    this.cdmercad = cdmercad;
  }

  public BigDecimal getNumsec() {
    return this.numsec;
  }

  public void setNumsec(BigDecimal numsec) {
    this.numsec = numsec;
  }

  public boolean equals(Object other) {
    if ((this == other))
      return true;
    if ((other == null))
      return false;
    if (!(other instanceof Tmct0cleIdNacional))
      return false;
    Tmct0cleIdNacional castOther = (Tmct0cleIdNacional) other;

    return ((this.getCdcleare() == castOther.getCdcleare()) || (this.getCdcleare() != null
        && castOther.getCdcleare() != null && this.getCdcleare().equals(castOther.getCdcleare())))
        && ((this.getCentro() == castOther.getCentro()) || (this.getCentro() != null && castOther.getCentro() != null && this
            .getCentro().equals(castOther.getCentro())))
        && ((this.getCdmercad() == castOther.getCdmercad()) || (this.getCdmercad() != null
            && castOther.getCdmercad() != null && this.getCdmercad().equals(castOther.getCdmercad())))
        && ((this.getNumsec() == castOther.getNumsec()) || (this.getNumsec() != null && castOther.getNumsec() != null && this
            .getNumsec().equals(castOther.getNumsec())));
  }

  public int hashCode() {
    int result = 17;

    result = 37 * result + (getCdcleare() == null ? 0 : this.getCdcleare().hashCode());
    result = 37 * result + (getCentro() == null ? 0 : this.getCentro().hashCode());
    result = 37 * result + (getCdmercad() == null ? 0 : this.getCdmercad().hashCode());
    result = 37 * result + (getNumsec() == null ? 0 : this.getNumsec().hashCode());
    return result;
  }

}
