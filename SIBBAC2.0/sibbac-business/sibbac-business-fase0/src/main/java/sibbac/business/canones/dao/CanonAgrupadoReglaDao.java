package sibbac.business.canones.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.canones.model.CanonAgrupadoRegla;

@Component
public interface CanonAgrupadoReglaDao extends JpaRepository<CanonAgrupadoRegla, Long> {

  /**
   * Actualiza a cero el importe efectivo y el importe canon a cero para todas
   * las ocurrencias que cumpla con la fecha de contratacion y el titular
   * actualizando datos de auditoría.
   * @param fechaContratacion fecha de contratación a buscar.
   * @param titular titular a buscar.
   * @param fhaudit fecha para auditoría
   * @param cdusuaud usuario para auditoría
   * @return número de modificaciones realizadas.
   */
  @Modifying
  @Query("UPDATE CanonAgrupadoRegla r SET r.importeEfectivo = 0, r.importeCanon = 0, "
      + " r.fhaudit = :fhaudit, r.cdusuaud = :cdusuaud "
      + " WHERE r.titular = :titular AND r.fechaContratacion = :fechaContratacion ")
  int updateImportesaCero(@Param("fechaContratacion") Date fechaContratacion, @Param("titular") String titular,
      @Param("fhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);

}
