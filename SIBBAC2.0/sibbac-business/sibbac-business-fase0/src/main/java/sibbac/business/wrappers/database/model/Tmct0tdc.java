package sibbac.business.wrappers.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represent the table TMCT0TDC. Derechos de liquidación y cánones de gestión Documentación para TMCT0TDC
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 **/

@Entity
@Table(name = "TMCT0TDC")
public class Tmct0tdc implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1983054577374139540L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "fhinival", column = @Column(name = "FHINIVAL", nullable = false)), 
                        @AttributeOverride(name = "fhfinval", column = @Column(name = "FHFINVAL", nullable = false)), 
                        @AttributeOverride(name = "cddbolsa", column = @Column(name = "CDDBOLSA", nullable = false, length=3)), 
                        @AttributeOverride(name = "immaxefe", column = @Column(name = "IMMAXEFE", nullable = false,precision = 18, scale = 4)),
                        @AttributeOverride(name = "nuversion", column = @Column(name = "NUVERSION", nullable = false))
  })
  protected Tmct0tdcId id;

  @Column(name = "IMDERGES", precision = 18, scale = 4)
  protected java.math.BigDecimal imderges;
  /**
   * Table Field IMDERLIQ. Derechos de liquidación Documentación para IMDERLIQ <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   * 
   **/
  @Column(name = "IMDERLIQ", precision = 18, scale = 4)
  protected java.math.BigDecimal imderliq;
  /**
   * Table Field PCFACGES. % Canón gestión en puntos básicos Documentación para PCFACGES <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   **/
  @Column(name = "PCFACGES", precision = 7, scale = 4)
  protected java.math.BigDecimal pcfacges;
  /**
   * Table Field PCFACLIQ. % Derechos liquidación en puntos básicos Documentación para PCFACLIQ <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * 
   **/
  @Column(name = "PCFACLIQ", precision = 7, scale = 4)
  protected java.math.BigDecimal pcfacliq;
  /**
   * Table Field CDUSUAUD. Audit user Documentación para CDUSUAUD <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Column(name = "CDUSUAUD", length = 10)
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. Audit datetime Documentación para FHAUDIT <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   **/
  @Temporal(TemporalType.DATE)
  @Column(name = "FHAUDIT")
  protected Date fhaudit;

 

  public java.math.BigDecimal getImderges() {
    return imderges;
  }

  public Tmct0tdcId getId() {
    return id;
  }

  public void setId(Tmct0tdcId id) {
    this.id = id;
  }

  public java.math.BigDecimal getImderliq() {
    return imderliq;
  }

  public java.math.BigDecimal getPcfacges() {
    return pcfacges;
  }

  public java.math.BigDecimal getPcfacliq() {
    return pcfacliq;
  }

  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  public Date getFhaudit() {
    return fhaudit;
  }


  public void setImderges(java.math.BigDecimal imderges) {
    this.imderges = imderges;
  }

  public void setImderliq(java.math.BigDecimal imderliq) {
    this.imderliq = imderliq;
  }

  public void setPcfacges(java.math.BigDecimal pcfacges) {
    this.pcfacges = pcfacges;
  }

  public void setPcfacliq(java.math.BigDecimal pcfacliq) {
    this.pcfacliq = pcfacliq;
  }

  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  public void setFhaudit(Date fhaudit) {
    this.fhaudit = fhaudit;
  }

}
