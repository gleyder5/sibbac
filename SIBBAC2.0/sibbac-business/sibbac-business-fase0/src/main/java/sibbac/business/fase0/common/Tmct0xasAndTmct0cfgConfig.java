package sibbac.business.fase0.common;

import java.io.Closeable;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import sibbac.business.fase0.database.dto.HoraEjeccucionDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessageVersion;

public interface Tmct0xasAndTmct0cfgConfig extends Closeable, Serializable {

  public List<Integer> getWorkDaysOfWeek();

  public List<Date> getHolidays() throws ParseException;

  public boolean isLoadXmlInternacional();

  public String getHoraInicioSessionPti();

  public Date findHoraInicioImputacionesS3() throws SIBBACBusinessException;

  public Date findHoraInicioImputacionesMabS3() throws SIBBACBusinessException;

  public Date findHoraFinalImputacionesS3() throws SIBBACBusinessException;

  public Date findHoraFinalImputacionesMabS3() throws SIBBACBusinessException;

  public Integer findDiasRevisionProcesoEnvioMovimientos() throws SIBBACBusinessException;

  public List<Integer> findDiasActivacionProcesos();

  public List<String> findEstadosEccActivos();

  public String getTpopebolsSinEcc();

  public HoraEjeccucionDTO getIfIsHoraEjecucionFicheroSpb1819();

  public String getPtiMessageVersion();

  public String getPtiAppIdentityDataEntidad();

  public String getPtiImputacionesDailyDeactivate();

  public String getPtiUsuarioOrigenImputacionPropia();

  public PTIMessageVersion getPtiMessageVersionFromTmct0cfg(String messageVersionCfg, Date date)
      throws SIBBACBusinessException;

  public PTIMessageVersion getPtiMessageVersionFromTmct0cfg(Date date) throws SIBBACBusinessException;

  public String getIdEuroCcp();

  public String getIdPti();

  public String getCifSvb();

  public String getAliasScriptDividend();

  public List<String> getPtiMessageTypesInOrden();

  public Long getMinutosEscaladoBokNoActivo();

  public Long getMinutosEscaladoAloNoActivo();

  public Long getMinutosEscaladoAlcNoActivo();

  public Integer getCdestadoasigGtLockUserActivity();

  public String getCdestadoasigLockUserActivity();

  public List<Integer> getListCdestadoasigLockUserActivity();

  public Integer getCdestadoentrecGtLockUserActivity();

  public BigDecimal getEfectivoMinimoDerechos();

  public BigDecimal getCanonEfectivoMinimoDerechos();

  public Integer getDaysEnvioCe();

  /**************************************************************************** FIN*CONFIGURACION *********************************************************************/

  /******************************************************************************** CONVERSIONES **********************************************************************/

  public String getClearingPlatformClearingPlatform(String clearingplatform);

  public String getSentidoSibbacSentidoPti(String sentidoSibbac);

  public String getSentidoSibbacSentidoEuroCcp(String sentidoSibbac);

  public String getSentidoEuroCcpSentidoSibbac(String sentidoSibbac);

  public Character getSentidoPtiSentidoSibbac(String sentidoPti);

  public Character getIndicadorCapacidadPtiIndicadorCapacidad(String indCapacidadPti);

  public Character getIndicadorCapacidadIndicadorCapacidadPti(String indCapacidad);

  public String getSettlementExchangeCdmercad(String settlementExchange);

  public String getSettlementExchangeCdclsmdo(String settlementExchange);

  public String getCdcleareClearer(String cdcleare);

  public String getCdbrokerCdbolsas(String cdbroker);

  public String getCdbrokerCdmiembro(String cdbroker);

  public String getCdbrokerMtf(String cdbroker);

  public String getCdmercadMtf(String cdmercad);

  public String getCdmercadTpsubmer(String cdmercad);

  public String getCdcanalCdcanent(String cdcanal);

  public String getGenpkbDesglose(String clave);

  public String getCdtipordTpordens(String cdtipord);

  public String getCdcuentaCdorigen(String cdcuenta);

  public String getCdclenteCdorigen(String cdclente);

  public String getCdorigenCdorigen(String cdorigen);

  public Character getCdmercadTpmercad(String cdmercad);

  public Character getCdtpcambCdtpcambio(Character cdtpcamb);

  public Character getCdtpcambTpcontra(Character cdtpcamb);

}
