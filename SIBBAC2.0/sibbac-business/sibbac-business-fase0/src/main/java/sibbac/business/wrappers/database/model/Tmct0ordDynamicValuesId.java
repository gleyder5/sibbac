package sibbac.business.wrappers.database.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tmct0ordDynamicValuesId implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -126834263383563575L;
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden = "";
  @Column(name = "NOMBRE", nullable = false, length = 32)
  private String nombre = "";

  public Tmct0ordDynamicValuesId() {
  }

  public Tmct0ordDynamicValuesId(String nuorden, String nombre) {
    this.nuorden = nuorden;
    this.nombre = nombre;
  }

  public String getNuorden() {
    return this.nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public String geNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0ordDynamicValuesId other = (Tmct0ordDynamicValuesId) obj;
    if (nombre == null) {
      if (other.nombre != null)
        return false;
    }
    else if (!nombre.equals(other.nombre))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    }
    else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public String toString() {
    return String.format("Tmct0ordDynamicValuesId %s##%s", nuorden, nombre);
  }

}
