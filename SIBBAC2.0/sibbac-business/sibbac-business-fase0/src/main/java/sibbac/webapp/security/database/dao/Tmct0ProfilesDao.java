package sibbac.webapp.security.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0Profiles;
import sibbac.webapp.security.database.model.Tmct0Users;


@Repository
public interface Tmct0ProfilesDao extends JpaRepository<Tmct0Profiles, Long> {

 
}
