package sibbac.webapp.security.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_USERS_PASSWORD Roles que gestiona el sistema
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_USERS_PASSWORD")
public class Tmct0Passwords implements java.io.Serializable {

  /** Campos de la entidad */
  private static final long serialVersionUID = 1L;


  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_USER_PASSWORD", length = 8, nullable = false)
  private Long idUserPassword;

  @Column(name = "PASSWORD", length = 200, nullable = false)
  private String password;

  @Temporal(TemporalType.DATE)
  @Column(name = "FPASSWORD", length = 10, nullable = false)
  private Date fPassword;

  @Column(name = "ID_USER", length = 8, nullable = false)
  private Long idUser;

  public Long getIdUserPassword() {
    return idUserPassword;
  }

  public void setIdUserPassword(Long idUserPassword) {
    this.idUserPassword = idUserPassword;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getfPassword() {
    return fPassword;
  }

  public void setfPassword(Date fPassword) {
    this.fPassword = fPassword;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

}
