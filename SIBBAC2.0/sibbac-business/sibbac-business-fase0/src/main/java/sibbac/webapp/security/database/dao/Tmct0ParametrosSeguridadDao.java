package sibbac.webapp.security.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0ParametrosSeguridad;

@Repository
public interface Tmct0ParametrosSeguridadDao extends JpaRepository<Tmct0ParametrosSeguridad, String> {

}
