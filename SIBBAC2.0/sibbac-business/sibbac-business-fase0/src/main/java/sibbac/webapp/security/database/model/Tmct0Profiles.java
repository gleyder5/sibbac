package sibbac.webapp.security.database.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_ROLES Roles que gestiona el sistema
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_PROFILES")
public class Tmct0Profiles implements java.io.Serializable {

  /** Campos de la entidad */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_PROFILE", length = 8, nullable = false)
  private Long idProfile;

  @Column(name = "NAME", length = 50, nullable = false)
  private String name;

  @Column(name = "DESCRIPTION", length = 200)
  private String description;

  @ManyToMany
  @JoinTable(name = "TMCT0_USERS_PROFILES",
             joinColumns = @JoinColumn(name = "ID_PROFILE", referencedColumnName = "ID_PROFILE"),
             inverseJoinColumns = @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER"))
  private List<Tmct0Users> users;

  @ManyToMany
  @JoinTable(name = "TMCT0_PROFILE_ACTION_PAGE",
             joinColumns = @JoinColumn(name = "ID_PROFILE", referencedColumnName = "ID_PROFILE"),
             inverseJoinColumns = @JoinColumn(name = "ID_ACTION_PAGE", referencedColumnName = "ID_ACTION_PAGE"))
   private List<Tmct0ActionPage> actionPages;
    

  public Tmct0Profiles() {
  };

  public Tmct0Profiles(Long idProfile, String name, String description) {
    this.idProfile = idProfile;
    this.name = name;
    this.description = description;
  }

   public Tmct0Profiles(Long idProfile,
                       String name,
                       String description,
                       List<Tmct0Users> users,
                       List<Tmct0ActionPage> actionPages) {
    super();
    this.idProfile = idProfile;
    this.name = name;
    this.description = description;
    this.users = users;
    this.actionPages = actionPages;
  }

  public Long getIdProfile() {
    return idProfile;
  }

  public void setIdProfile(Long idProfile) {
    this.idProfile = idProfile;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Tmct0Users> getUsers() {
    return users;
  }

  public void setUsers(List<Tmct0Users> users) {
    this.users = users;
  }

  public List<Tmct0ActionPage> getActionPages() {
    return actionPages;
  }

  public void setActionPages(List<Tmct0ActionPage> actionPages) {
    this.actionPages = actionPages;
  }
  
}
