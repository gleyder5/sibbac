package sibbac.webapp.security.database.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_USERS Usuarios que van a tener acceso al sistema
 * 
 * @author Neoris
 *
 */

@Entity
@Table(name = "TMCT0_USERS")
public class Tmct0Users implements java.io.Serializable {

  public List<Tmct0Passwords> getTmct0PasswordsList() {
    return tmct0PasswordsList;
  }

  public void setTmct0PasswordsList(List<Tmct0Passwords> tmct0PasswordsList) {
    this.tmct0PasswordsList = tmct0PasswordsList;
  }

  /** Campos de la entidad */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_USER", length = 8, nullable = false)
  private Long idUser;

  @Column(name = "USERNAME", length = 20, nullable = false)
  private String username;

  @Column(name = "PASSWORD", length = 200, nullable = false)
  private String password;

  @Column(name = "NAME", length = 200, nullable = false)
  private String name;

  @Column(name = "LASTNAME", length = 200, nullable = false)
  private String lastname;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "LASTLOGIN", length = 26, nullable = true)
  private Date lastlogin;

  @Type(type = "yes_no")
  @Column(name = "RESET")
  private boolean reset;

  @Column(name = "EMAIL", length = 200, nullable = true)
  private String email;

  @Column(name = "NUM_INTENTOS", length = 4, nullable = false)
  private Integer num_intentos;

  @Temporal(TemporalType.DATE)
  @Column(name = "FPASSWORD", length = 10, nullable = false)
  private Date fPassword;

  @ManyToMany
  @JoinTable(name = "TMCT0_USERS_PROFILES",
             joinColumns = @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER"),
             inverseJoinColumns = @JoinColumn(name = "ID_PROFILE", referencedColumnName = "ID_PROFILE"))
  private List<Tmct0Profiles> profiles;

  @OneToMany(targetEntity = Tmct0Passwords.class, mappedBy = "idUser", cascade = CascadeType.ALL)
  @OrderBy("fPassword DESC")
  private List<Tmct0Passwords> tmct0PasswordsList;

  public Tmct0Users() {
  };

  public boolean isReset() {
    return reset;
  }

  public void setReset(boolean reset) {
    this.reset = reset;
  }

  public Tmct0Users(Long idUser, String username, String password, String name, String lastname) {
    this.idUser = idUser;
    this.username = username;
    this.setPassword(password);
    this.name = name;
    this.lastname = lastname;
  }

  public Tmct0Users(Long idUser, String username, String password, String name, String lastname, Date lastlogin) {
    this.idUser = idUser;
    this.username = username;
    this.setPassword(password);
    this.name = name;
    this.lastname = lastname;
    this.lastlogin = lastlogin;
  }

  public Tmct0Users(Long idUser, String username, String password, String name, String lastname, String email) {
    this.idUser = idUser;
    this.username = username;
    this.setPassword(password);
    this.name = name;
    this.lastname = lastname;
    this.email = email;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long id_user) {
    this.idUser = id_user;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  /*
   * El método desencripta la password antes de recuperarla
   */
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public Date getLastlogin() {
    return lastlogin;
  }

  public void setLastlogin(Date lastlogin) {
    this.lastlogin = lastlogin;
  }

  public List<Tmct0Profiles> getProfiles() {
    return profiles;
  }

  public void setProfiles(List<Tmct0Profiles> profiles) {
    this.profiles = profiles;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getfPassword() {
    return fPassword;
  }

  public void setfPassword(Date fPassword) {
    this.fPassword = fPassword;
  }

  public Integer getNum_intentos() {
    return num_intentos;
  }

  public void setNum_intentos(Integer num_intentos) {
    this.num_intentos = num_intentos;
  }

}
