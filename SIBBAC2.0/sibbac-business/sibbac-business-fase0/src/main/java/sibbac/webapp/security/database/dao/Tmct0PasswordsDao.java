package sibbac.webapp.security.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0Passwords;

@Repository
public interface Tmct0PasswordsDao extends JpaRepository<Tmct0Passwords, Long> {

}
