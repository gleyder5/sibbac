package sibbac.webapp.security.database.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_PAGES
 * Funcionalidades o páginas a las que van a tener acceso los usuarios en función del rol.
 * @author Neoris
 *
 */

@Entity
@Table( name = "TMCT0_PAGES" )
public class Tmct0Pages implements java.io.Serializable {

  private static final long serialVersionUID  = 1L;
  
  /** Campos de la entidad */
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column( name = "ID_PAGE", length = 8, nullable = false )
  private Long  idPage;
  
  @Column( name = "NAME", length = 100, nullable = false )
  private String       name;
  
  @Column( name = "DESCRIPTION", length = 200 )
  private String       description;
  
  @Column( name = "URL", length = 200 )
  private String       url;
  
  @Column( name = "ID_PARENT", length = 4 )
  private Integer  idParent;
  
  @Column( name = "ORDERPAGE", length = 4, nullable = false )
  private Integer  orderPage;
  
  @ManyToMany
  @JoinTable(name = "TMCT0_ACTIONS_PAGES",
             joinColumns = @JoinColumn(name = "ID_PAGE", referencedColumnName = "ID_PAGE"),
             inverseJoinColumns = @JoinColumn(name = "ID_ACTION", referencedColumnName = "ID_ACTION"))
  private List<Tmct0Actions> acciones;
  
   
  public Tmct0Pages() {
   }



  public Tmct0Pages(Long idPage,
                    String name,
                    String description,
                    String url,
                    Integer idParent,
                    Integer orderPage) {
    this.idPage = idPage;
    this.name = name;
    this.description = description;
    this.url = url;
    this.idParent = idParent;
    this.orderPage = orderPage;
  }



  public Long getIdPage() {
    return idPage;
  }

  public void setIdPage(Long idPage) {
    this.idPage = idPage;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getIdParent() {
    return idParent;
  }

  public void setIdParent(Integer idparent) {
    this.idParent = idparent;
  }
  
  public Integer getOrderPage() {
    return orderPage;
  }

  public void setOrderPage(Integer orderPage) {
    this.orderPage = orderPage;
  }

  
  
  
}
