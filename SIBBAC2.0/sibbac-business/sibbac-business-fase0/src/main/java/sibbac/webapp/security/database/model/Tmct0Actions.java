package sibbac.webapp.security.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_ACTIONS Acciones a las que van a tener acceso los usuarios dentro de una pagina.
 * 
 * @author Neoris
 *
 */

@Entity
@Table(name = "TMCT0_ACTIONS")
public class Tmct0Actions {

  @Id
  @Column(name = "ID_ACTION", length = 8, nullable = false)
  private Long idAction;

  @Column(name = "NAME", length = 50, nullable = false)
  private String name;

  @Column(name = "DESCRIPTION", length = 200)
  private String description;

  @Column(name = "ORDERACTION", length = 4, nullable = false)
  private Integer orderAction;

  public Tmct0Actions() {

  }

  public Tmct0Actions(Long idAction, String name, String description) {
    this.idAction = idAction;
    this.name = name;
    this.description = description;
  }

  public Long getIdAction() {
    return idAction;
  }

  public void setIdAction(Long idAction) {
    this.idAction = idAction;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getOrderAction() {
    return orderAction;
  }

  public void setOrderAction(Integer orderAction) {
    this.orderAction = orderAction;
  }

}
