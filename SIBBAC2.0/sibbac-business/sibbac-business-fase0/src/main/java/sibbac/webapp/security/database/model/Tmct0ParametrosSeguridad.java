package sibbac.webapp.security.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_PARAMETROS_SEGURIDAD")
public class Tmct0ParametrosSeguridad {

  @Id
  @Column(name = "CLAVE", length = 50, nullable = false)
  private String clave;

  @Column(name = "VALOR", length = 8, nullable = false)
  private Integer valor;

  public Tmct0ParametrosSeguridad() {
  }

  public Tmct0ParametrosSeguridad(String clave, Integer valor) {
    this.clave = clave;
    this.valor = valor;
  }

  public String getClave() {
    return clave;
  }

  public void setClave(String clave) {
    this.clave = clave;
  }

  public Integer getValor() {
    return valor;
  }

  public void setValor(Integer valor) {
    this.valor = valor;
  }

}
