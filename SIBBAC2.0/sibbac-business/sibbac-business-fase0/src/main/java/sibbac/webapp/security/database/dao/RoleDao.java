package sibbac.webapp.security.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {

  @Query("select role from Role role where role.username = :userId")
  public List<Role> getRoleByUserId(@Param("userId") final String userId);

  @Query("select count(role) from Role role where role.username = :userId")
  public Long isUserInSibbac(@Param("userId") final String userId);
}
