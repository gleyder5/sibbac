package sibbac.webapp.security.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0Pages;

@Repository
public interface Tmct0PagesDao extends JpaRepository<Tmct0Pages, Long> {

  // Obtiene todas las paginas que tienen acceso a funcionalidad
  @Query("SELECT u FROM Tmct0Pages u WHERE u.url is not null  order by u.description ")
  public List<Tmct0Pages> getPagesWithUrl();

  // Obtiene todas las opciones principales
  @Query("SELECT u FROM Tmct0Pages u where u.idParent is null order by u.orderPage ")
  public List<Tmct0Pages> getPagesWithoutUrl();

  // Obtiene todas las opciones principales
  @Query("SELECT u FROM Tmct0Pages u where u.idParent = :idParent order by u.orderPage ")
  public List<Tmct0Pages> getPagesByIdParent(@Param("idParent") Integer idParent);

  // Obtiene todas las opciones ordenadas
  @Query("SELECT u FROM Tmct0Pages u order by u.orderPage ")
  public List<Tmct0Pages> getPages();

}
