package sibbac.webapp.security.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0Users;

@Repository
public interface Tmct0UsersDao extends JpaRepository<Tmct0Users, Long> {

  @Query("SELECT u FROM Tmct0Users u where u.username = :username and u.password = :password and reset != null")
  public Tmct0Users getByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

  @Query("SELECT u FROM Tmct0Users u where u.username = :username")
  public Tmct0Users getByUsername(@Param("username") String username);

  @Query("SELECT u FROM Tmct0Users u where u.username = :username and u.idUser <> :idUser")
  public Tmct0Users getByUsernameNotUserId(@Param("username") String username, @Param("idUser") Long idUser);

  @Query("SELECT u FROM Tmct0Users u order by u.username asc")
  public List<Tmct0Users> findUsersOrderByUsername();
  
  @Query(value = "SELECT CONCAT(CONCAT(USERS.NAME, ' '), USERS.LASTNAME) FROM BSNBPSQL.TMCT0_USERS USERS WHERE USERS.USERNAME = :userName", nativeQuery = true)
  public String findUserNameLastNameConcatByUserName(@Param("userName") String userName);

}
