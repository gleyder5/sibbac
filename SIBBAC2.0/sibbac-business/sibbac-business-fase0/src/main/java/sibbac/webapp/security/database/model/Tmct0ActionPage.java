package sibbac.webapp.security.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_ACTIONS_PAGES
 * Acciones que se pueden hacer en cada pagina.
 * @author Neoris
 *
 */

@Entity
@Table( name = "TMCT0_ACTIONS_PAGES" )
public class Tmct0ActionPage {

  @Id
  @Column(name = "ID_ACTION_PAGE", length = 8, nullable = false)
  private Long idActionPage;
  
  @ManyToOne
  @JoinColumn( name = "ID_ACTION", referencedColumnName = "ID_ACTION", nullable=false )
  private Tmct0Actions action;
  
 
  @ManyToOne
  @JoinColumn( name = "ID_PAGE", referencedColumnName = "ID_PAGE", nullable=false )
  private Tmct0Pages page;


  public Tmct0ActionPage () {
    
  }
  
  public Tmct0ActionPage(Long idActionPage, Tmct0Actions action, Tmct0Pages page) {
    this.idActionPage = idActionPage;
    this.action = action;
    this.page = page;
  }


  public Long getIdActionPage() {
    return idActionPage;
  }


  public void setIdActionPage(Long idActionPage) {
    this.idActionPage = idActionPage;
  }


  public Tmct0Actions getAction() {
    return action;
  }


  public void setAction(Tmct0Actions action) {
    this.action = action;
  }


  public Tmct0Pages getPage() {
    return page;
  }


  public void setPage(Tmct0Pages page) {
    this.page = page;
  }
  
  
}
