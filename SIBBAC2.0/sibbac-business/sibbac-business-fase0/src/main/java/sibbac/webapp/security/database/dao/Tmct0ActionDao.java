package sibbac.webapp.security.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0Actions;

@Repository
public interface Tmct0ActionDao extends JpaRepository<Tmct0Actions, Long> {

  @Query("SELECT a FROM Tmct0Actions a order by a.orderAction asc")
  public List<Tmct0Actions> findActionsOrderByOrderAction();

}
