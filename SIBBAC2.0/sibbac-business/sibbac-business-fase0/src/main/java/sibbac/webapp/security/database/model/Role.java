package sibbac.webapp.security.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.model.ATable;

@Entity
@Table( name = "ROLES" )
public class Role extends ATable<Role> implements Serializable{

	/**
	 * Serail id
	 */
	private static final long serialVersionUID = 4983078501253164938L;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="ROLNAME")
	private String rol;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

}
