package sibbac.webapp.security.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.model.Tmct0ActionPage;

@Repository
public interface Tmct0ActionPageDao extends JpaRepository<Tmct0ActionPage, Long> {

  @Query(value = "select NVL( ACP.ID_ACTION_PAGE , 0) ,  ACT.ID_ACTION  From tmct0_ACTIONS ACT LEFT JOIN  tmct0_actions_pages acp ON ACT.ID_ACTION = acp.ID_ACTION AND ACP.ID_PAGE = (:idPage) order by ACT.ID_ACTION ",
         nativeQuery = true)
  public List<Object[]> getActionsByPage(@Param("idPage") Long idPage);
}
