package sibbac.database.megbpdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0ctr;
import sibbac.database.megbpdta.model.Fmeg0ctrId;

@Repository
public interface Fmeg0ctrDao extends JpaRepository<Fmeg0ctr, Fmeg0ctrId> {
  // @Query("select r from Fmeg0ctr r where r.id.cdtpcomi = :cdtpcomi and r.id.cdcomisi = :cdcomisi and  r.id.cdtarifa = :cdtarifa")
  List<Fmeg0ctr> findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(String cdtpcomi, String cdcomisi, String cdtarifa);

}
