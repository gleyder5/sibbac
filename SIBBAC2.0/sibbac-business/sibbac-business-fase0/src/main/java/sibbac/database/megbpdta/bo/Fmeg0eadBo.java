package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0eadDao;
import sibbac.database.megbpdta.model.Fmeg0ead;
import sibbac.database.megbpdta.model.Fmeg0eadId;

@Service
public class Fmeg0eadBo extends AbstractFmegBo<Fmeg0ead, Fmeg0eadId, Fmeg0eadDao> {

  @Override
  @Transactional
  public Fmeg0ead save(Fmeg0ead entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdident1(entity.getId().getCdident1());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
