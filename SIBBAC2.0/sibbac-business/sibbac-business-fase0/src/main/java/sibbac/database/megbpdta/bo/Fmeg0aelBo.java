package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0aelDao;
import sibbac.database.megbpdta.model.Fmeg0ael;
import sibbac.database.megbpdta.model.Fmeg0aelId;

@Service
public class Fmeg0aelBo extends AbstractFmegBo<Fmeg0ael, Fmeg0aelId, Fmeg0aelDao> {

  public List<Fmeg0ael> findAllByIdCdaliass(String cdaliass) {
    return dao.findAllByIdCdaliass(cdaliass);
  }

  public List<Fmeg0ael> findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(String cdcleare, String tpsettle,
      String idenvfid) {
    return dao.findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(cdcleare, tpsettle, idenvfid);
  }

  @Override
  @Transactional
  public Fmeg0ael save(Fmeg0ael entity, boolean isNew) {
    if (entity.getId().getNusecael() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecaelByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecael(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
