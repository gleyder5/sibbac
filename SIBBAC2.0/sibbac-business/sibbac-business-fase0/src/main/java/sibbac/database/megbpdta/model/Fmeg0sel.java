package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0SEL. Subcuentas-Elegible Clearers
 * Documentación para FMEG0SEL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0SEL", schema = "MEGBPDTA")
public class Fmeg0sel extends Fmeg0sucChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -10727467497121288L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdsubcta", column = @Column(name = "cdsubcta", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  private Fmeg0selId id;

  /**
   * Table Field TPSETTLE. settlementType MEGARA: settlementType FIDESSA:
   * ELIGIBLE_CLEARER_SETT_TYPE. Se accede a los 2 ficheros de conversion para
   * transcodificar en AS400 y luego en FIDESSA. Los valores de MEGARA son los
   * mismos que se envian a FIDESSA (Si es Domestic/Cedel, cambiar a DC Si es
   * Domestic/Domestic, cambiar a DD Si es Domestic/Euroclear, cambiar a DE Si
   * es Euroclear/Cedel, cambiar a EC Si es Euroclear/Domestic, cambiar a ED Si
   * es Euroclear/Euroclear, cambiar a EE) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpsettle;
  /**
   * Table Field IDENTITM. mainEntityId MEGARA: mainEntityId, siempre con valor
   * 0036 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String identitm;
  /**
   * Table Field CDMARKET. market MEGARA: market FIDESSA: ELIGIBLE_CLEARER_MKT,
   * MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field TPCLEMOD. clearingMode MEGARA: clearingMode. Se accede al
   * fichero de conversion (Si su valor es SOCIEDAD, cambiar a S en el AS400, si
   * es RECTORA, cambiar a R en el AS400) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpclemod;
  /**
   * Table Field CDCLEARE. clearer MEGARA: clearer FIDESSA:
   * ELIGIBLE_CLEARER_ENTITY <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcleare;
  /**
   * Table Field ACCATCLE. accountAtClearer MEGARA: accountAtClearer FIDESSA:
   * ELIGIBLE_CLEARER_ACC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String accatcle;
  /**
   * Table Field IDENVFID. Enviado S/N Documentación para IDENVFID <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idenvfid;

  public Fmeg0selId getId() {
    return id;
  }

  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  public java.lang.String getIdentitm() {
    return identitm;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getTpclemod() {
    return tpclemod;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public java.lang.String getAccatcle() {
    return accatcle;
  }

  public java.lang.String getIdenvfid() {
    return idenvfid;
  }

  public void setId(Fmeg0selId id) {
    this.id = id;
  }

  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  public void setIdentitm(java.lang.String identitm) {
    this.identitm = identitm;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setTpclemod(java.lang.String tpclemod) {
    this.tpclemod = tpclemod;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public void setAccatcle(java.lang.String accatcle) {
    this.accatcle = accatcle;
  }

  public void setIdenvfid(java.lang.String idenvfid) {
    this.idenvfid = idenvfid;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s", id.getCdsubcta(), id.getNusecuen());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0SEL";
  }

  @Override
  public String toString() {
    return String.format(
        "Fmeg0sel [id=%s, tpsettle=%s, identitm=%s, cdmarket=%s, tpclemod=%s, cdcleare=%s, accatcle=%s, idenvfid=%s]",
        id, tpsettle, identitm, cdmarket, tpclemod, cdcleare, accatcle, idenvfid);
  }
  
  

}