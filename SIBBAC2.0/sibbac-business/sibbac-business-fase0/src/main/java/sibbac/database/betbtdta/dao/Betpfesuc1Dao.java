package sibbac.database.betbtdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.betbtdta.model.Betpfesuc1;
import sibbac.database.betbtdta.model.Betpfesuc1Id;

@Repository
public interface Betpfesuc1Dao extends JpaRepository<Betpfesuc1, Betpfesuc1Id> {

  @Query(value = "select b.esubcta from Betpfesuc b WHERE b.id.ectoms = :subAccountBroclient  and b.id.ectcgl = :alias and b.id.esubcgl = :cdsubcta"
      + " and b.ectfba = 0 ORDER BY b.ectfal DESC")
  List<BigDecimal> findAllNuctaproByIdBroClientAndIdCdaliassAndIdCdsubcta(
      @Param("subAccountBroclient") String subAccountBroclient, @Param("alias") BigDecimal alias,
      @Param("cdsubcta") BigDecimal cdsubcta);
}
