package sibbac.database.bsnvasql.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnvasql.model.Tval0vpb;

@Repository
public interface Tval0vpbDao extends JpaRepository< Tval0vpb, String > {

}
