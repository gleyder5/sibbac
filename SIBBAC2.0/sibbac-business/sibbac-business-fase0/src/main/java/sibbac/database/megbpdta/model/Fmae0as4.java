package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMAE0AS4. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AS4 <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Entity
@Table(name = "FMAE0AS4", schema = "MEGBPDTA")
public class Fmae0as4 implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  /**
   * Table Field IDINFMAE. Identificador informacion maestra Documentación para
   * IDINFMAE <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "idinfmae", column = @Column(name = "idinfmae", nullable = false)),
      @AttributeOverride(name = "idclient", column = @Column(name = "idclient", nullable = false)),
      @AttributeOverride(name = "idprocta", column = @Column(name = "idprocta", nullable = false)),
      @AttributeOverride(name = "nudirecc", column = @Column(name = "nudirecc", nullable = false)),
      @AttributeOverride(name = "tpmodeid", column = @Column(name = "tpmodeid", nullable = false)),
      @AttributeOverride(name = "fhproces", column = @Column(name = "fhproces", nullable = false)) })
  protected Fmae0as4Id id;

  /**
   * Table Field HRPROCES. Hora sistema Documentación para HRPROCES <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String hrproces;

  public Fmae0as4Id getId() {
    return id;
  }

  public java.lang.String getHrproces() {
    return hrproces;
  }

  public void setId(Fmae0as4Id id) {
    this.id = id;
  }

  public void setHrproces(java.lang.String hrproces) {
    this.hrproces = hrproces;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s|%s|%s|%s|%s", id.getIdinfmae(), id.getIdclient(), id.getIdprocta(), id.getNudirecc(),
        id.getTpmodeid(), id.getFhproces());
  }

  @Override
  public String getNombreTabla() {
    return "FMAE0AS4";
  }

  @Override
  public String toString() {
    return String.format("Fmae0as4 [id=%s, hrproces=%s]", id, hrproces);
  }

}