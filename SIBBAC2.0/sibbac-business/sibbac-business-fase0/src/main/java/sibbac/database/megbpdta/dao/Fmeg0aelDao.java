package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0ael;
import sibbac.database.megbpdta.model.Fmeg0aelId;

@Repository
public interface Fmeg0aelDao extends JpaRepository<Fmeg0ael, Fmeg0aelId> {

  @Query(value = "select max(a.id.nusecael) from Fmeg0ael a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecaelByIdCdaliass(@Param("cdaliass") String cdaliass);

  List<Fmeg0ael> findAllByIdCdaliass(String cdaliass);

  @Query("select a from Fmeg0ael a where a.cdcleare = :cdcleare and a.tpsettle = :tpsettle and a.idenvfid = :idenvfid and a.fmeg0ali.tpmodeid !='D' ")
  List<Fmeg0ael> findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(@Param("cdcleare") String cdcleare,
      @Param("tpsettle") String tpsettle, @Param("idenvfid") String idenvfid);

}
