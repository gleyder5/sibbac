package sibbac.database.megbpdta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0bse;
import sibbac.database.megbpdta.model.Fmeg0bseId;

@Repository
public interface Fmeg0bseDao extends JpaRepository<Fmeg0bse, Fmeg0bseId> {

}
