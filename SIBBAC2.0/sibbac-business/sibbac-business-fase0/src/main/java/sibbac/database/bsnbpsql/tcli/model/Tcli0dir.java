package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table TCLI0DIR. DIRECCIONES/CUENTA Documentación de
 * la tablaTCLI0DIR <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TCLI0DIR")
public class Tcli0dir implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -7392359266376634282L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdproduc", column = @Column(name = "cdproduc", nullable = false)),
      @AttributeOverride(name = "nuctapro", column = @Column(name = "nuctapro", nullable = false)),
      @AttributeOverride(name = "nudirecc", column = @Column(name = "nudirecc", nullable = false)) })
  protected Tcli0dirId id;

  /**
   * Table Field NBATENCI. Columna importada NBATENCI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbatenci;
  /**
   * Table Field TPDOMICI. Columna importada TPDOMICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdomici;
  /**
   * Table Field NBDOMICI. Columna importada NBDOMICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbdomici;
  /**
   * Table Field NUDOMICI. Columna importada NUDOMICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nudomici;
  /**
   * Table Field NBCIUDAD. Columna importada NBCIUDAD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbciudad;
  /**
   * Table Field CDPROVIN. Columna importada CDPROVIN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdprovin;
  /**
   * Table Field NBPROVIN. Columna importada NBPROVIN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbprovin;
  /**
   * Table Field CDPOSTAL. Columna importada CDPOSTAL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdpostal;
  /**
   * Table Field CDDEPAIS. Columna importada CDDEPAIS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddepais;
  /**
   * Table Field NUTELINT. Columna importada NUTELINT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelint;
  /**
   * Table Field NUTELPRF. Columna importada NUTELPRF Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelprf;
  /**
   * Table Field NUTELEFO. Columna importada NUTELEFO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelefo;
  /**
   * Table Field NUTELEXX. Columna importada NUTELEXX Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelexx;
  /**
   * Table Field NUTELFAX. Columna importada NUTELFAX Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelfax;
  /**
   * Table Field NBNOTAS1. Columna importada NBNOTAS1 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbnotas1;
  /**
   * Table Field FHMODIFI. Columna importada FHMODIFI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhmodifi;
  /**
   * Table Field CDUSRMOD. Columna importada CDUSRMOD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdusrmod;
  /**
   * Table Field NUDIRSWI. Columna importada NUDIRSWI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nudirswi;
  /**
   * Table Field CDSISREC. Columna importada CDSISREC Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdsisrec;
  /**
   * Table Field CDCOMENT. Columna importada CDCOMENT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcoment;
  /**
   * Table Field TPCONFIR. Columna importada TPCONFIR Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpconfir;
  /**
   * Table Field CDIDIOMA. Columna importada CDIDIOMA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdidioma;
  /**
   * Table Field TPCONGES. Columna importada TPCONGES Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpconges;
  /**
   * Table Field TPMODCON. Columna importada TPMODCON Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodcon;
  /**
   * Table Field NUDIRSEQ. Columna importada NUDIRSEQ Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nudirseq;
  /**
   * Table Field DIREMAIL. Columna importada DIREMAIL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String diremail;
  /**
   * Table Field CDCLIOAS. Columna importada CDCLIOAS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdclioas;
  /**
   * Table Field CDDALERT. Columna importada CDDALERT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddalert;
}