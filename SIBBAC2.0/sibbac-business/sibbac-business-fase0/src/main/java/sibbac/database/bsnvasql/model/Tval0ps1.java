package sibbac.database.bsnvasql.model;


// Generated 30-jul-2015 12:08:15 by Hibernate Tools 4.3.1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table( schema = "BSNVASQL", name = "TVAL0PS1", uniqueConstraints = @UniqueConstraint( columnNames = "CDHPPAIS" ) )
public class Tval0ps1 implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7215108988641850139L;
	private Tval0ps1Id			id;
	private String				nbhppais;
	private Integer				fhdealta;
	private Integer				fhmodifi;
	private String				cdusralt;
	private String				cdusrmod;
	private String				cdiso2po;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride( name = "cdhppais", column = @Column( name = "CDHPPAIS", unique = true, nullable = false, length = 3 ) ),
			@AttributeOverride( name = "fhdebaja", column = @Column( name = "FHDEBAJA", nullable = false, precision = 8, scale = 0 ) )
	} )
	public Tval0ps1Id getId() {
		return this.id;
	}

	public void setId( Tval0ps1Id id ) {
		this.id = id;
	}

	@Column( name = "NBHPPAIS", nullable = false, length = 40 )
	public String getNbhppais() {
		return this.nbhppais;
	}

	public void setNbhppais( String nbhppais ) {
		this.nbhppais = nbhppais;
	}

	@Column( name = "FHDEALTA", nullable = false, precision = 8, scale = 0 )
	public Integer getFhdealta() {
		return this.fhdealta;
	}

	public void setFhdealta( Integer fhdealta ) {
		this.fhdealta = fhdealta;
	}

	@Column( name = "FHMODIFI", nullable = false, precision = 8, scale = 0 )
	public Integer getFhmodifi() {
		return this.fhmodifi;
	}

	public void setFhmodifi( Integer fhmodifi ) {
		this.fhmodifi = fhmodifi;
	}

	@Column( name = "CDUSRALT", nullable = false, length = 10 )
	public String getCdusralt() {
		return this.cdusralt;
	}

	public void setCdusralt( String cdusralt ) {
		this.cdusralt = cdusralt;
	}

	@Column( name = "CDUSRMOD", nullable = false, length = 10 )
	public String getCdusrmod() {
		return this.cdusrmod;
	}

	public void setCdusrmod( String cdusrmod ) {
		this.cdusrmod = cdusrmod;
	}

	@Column( name = "CDISO2PO", nullable = false, length = 2 )
	public String getCdiso2po() {
		return this.cdiso2po;
	}

	public void setCdiso2po( String cdiso2po ) {
		this.cdiso2po = cdiso2po;
	}

}
