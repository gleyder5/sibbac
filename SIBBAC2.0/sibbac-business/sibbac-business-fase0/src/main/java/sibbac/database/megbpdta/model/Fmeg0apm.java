package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0APM. Alias-PayMarketException
 * Documentación para FMEG0APM <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0APM", schema = "MEGBPDTA")
public class Fmeg0apm extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 2132476331849812453L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0apmId id;

  /**
   * Table Field IDSTEXCH. stockExchange MEGARA: stockExchange <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idstexch;
  
  

  public Fmeg0apmId getId() {
    return id;
  }

  public java.lang.String getIdstexch() {
    return idstexch;
  }

  public void setId(Fmeg0apmId id) {
    this.id = id;
  }

  public void setIdstexch(java.lang.String idstexch) {
    this.idstexch = idstexch;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdaliass(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0APM";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0apm [id=%s, idstexch=%s]", id, idstexch);
  }
  
  
}