package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0sot;
import sibbac.database.megbpdta.model.Fmeg0sotId;

@Repository
public interface Fmeg0sotDao extends JpaRepository<Fmeg0sot, Fmeg0sotId> {

  @Query(value = "select max(s.id.nusecuen) from Fmeg0sot s where s.id.cdsubcta = :cdsubcta")
  BigDecimal maxIdNusecuenByIdCdsubcta(@Param("cdsubcta") String cdsubcta);
  
  
  @Query(value = "select s from Fmeg0sot s where s.id.cdsubcta = :cdsubcta order by s.id.nusecuen ASC")
  List<Fmeg0sot> findAllByIdCdsubcta(@Param("cdsubcta") String cdsubcta);

}
