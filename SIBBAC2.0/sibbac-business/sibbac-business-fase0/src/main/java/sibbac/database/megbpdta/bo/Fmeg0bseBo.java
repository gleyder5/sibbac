package sibbac.database.megbpdta.bo;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0bseDao;
import sibbac.database.megbpdta.model.Fmeg0bse;
import sibbac.database.megbpdta.model.Fmeg0bseId;

@Service
public class Fmeg0bseBo extends AbstractFmegBo<Fmeg0bse, Fmeg0bseId, Fmeg0bseDao> {

}
