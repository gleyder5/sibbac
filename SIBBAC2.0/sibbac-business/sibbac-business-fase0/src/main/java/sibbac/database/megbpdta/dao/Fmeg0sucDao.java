package sibbac.database.megbpdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0suc;

@Repository
public interface Fmeg0sucDao extends JpaRepository<Fmeg0suc, String> {

  @Query(value = "select a from Fmeg0suc a where a.cdsubcta = :cdsubcta and a.tpmodeid != 'D' ")
  List<Fmeg0suc> findAllByCdsubctaAndTpmodeidDistinctD(@Param("cdsubcta") String cdsubcta);

  @Query(value = "select count(*) from Fmeg0suc a where a.cdsubcta = :cdsubcta and a.tpmodeid != 'D' ")
  Integer countByCdsubctaAndTpmodeidDistinctD(@Param("cdsubcta") String cdsubcta);

}
