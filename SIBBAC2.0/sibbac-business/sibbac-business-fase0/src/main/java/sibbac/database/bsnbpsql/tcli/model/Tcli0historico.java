package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Tcli0historico implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5719842679082105408L;
  @Column(nullable = false)
  protected java.math.BigDecimal fhenthis;

  @Column(nullable = false)
  protected java.math.BigDecimal hoenthis;

  @Column(nullable = false)
  protected String cdurshis;

  @Column(nullable = false)
  protected String cdaccion;

  public Tcli0historico() {

  }

  public Tcli0historico(BigDecimal fhenthis, BigDecimal hoenthis, String cdurshis, String cdaccion) {
    super();
    this.fhenthis = fhenthis;
    this.hoenthis = hoenthis;
    this.cdurshis = cdurshis;
    this.cdaccion = cdaccion;
  }

  public java.math.BigDecimal getFhenthis() {
    return fhenthis;
  }

  public java.math.BigDecimal getHoenthis() {
    return hoenthis;
  }

  public String getCdurshis() {
    return cdurshis;
  }

  public String getCdaccion() {
    return cdaccion;
  }

  public void setFhenthis(java.math.BigDecimal fhenthis) {
    this.fhenthis = fhenthis;
  }

  public void setHoenthis(java.math.BigDecimal hoenthis) {
    this.hoenthis = hoenthis;
  }

  public void setCdurshis(String cdurshis) {
    this.cdurshis = cdurshis;
  }

  public void setCdaccion(String cdaccion) {
    this.cdaccion = cdaccion;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdaccion == null) ? 0 : cdaccion.hashCode());
    result = prime * result + ((cdurshis == null) ? 0 : cdurshis.hashCode());
    result = prime * result + ((fhenthis == null) ? 0 : fhenthis.hashCode());
    result = prime * result + ((hoenthis == null) ? 0 : hoenthis.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tcli0historico other = (Tcli0historico) obj;
    if (cdaccion == null) {
      if (other.cdaccion != null)
        return false;
    }
    else if (!cdaccion.equals(other.cdaccion))
      return false;
    if (cdurshis == null) {
      if (other.cdurshis != null)
        return false;
    }
    else if (!cdurshis.equals(other.cdurshis))
      return false;
    if (fhenthis == null) {
      if (other.fhenthis != null)
        return false;
    }
    else if (!fhenthis.equals(other.fhenthis))
      return false;
    if (hoenthis == null) {
      if (other.hoenthis != null)
        return false;
    }
    else if (!hoenthis.equals(other.hoenthis))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tcli0historico [fhenthis=%s, hoenthis=%s, cdurshis=%s, cdaccion=%s]", fhenthis, hoenthis,
        cdurshis, cdaccion);
  }
  
  

}
