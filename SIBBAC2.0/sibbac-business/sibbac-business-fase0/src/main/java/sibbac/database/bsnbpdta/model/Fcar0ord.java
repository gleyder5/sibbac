package sibbac.database.bsnbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FCAR0ORD. Clientes cartera Documentación de la
 * tablaFCAR0ORD <!-- begin-user-doc --> <!-- end-user-doc -->
 **/

@Entity
@Table(name = "Fcar0ord", schema = "BSNBPDTA")
public class Fcar0ord implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 2154578447635715922L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "fhiniord", column = @Column(name = "fhiniord", nullable = false)),
      @AttributeOverride(name = "fhbajord", column = @Column(name = "fhbajord", nullable = false)),
      @AttributeOverride(name = "cdclient", column = @Column(name = "cdclient", nullable = false)),
      @AttributeOverride(name = "cdclient", column = @Column(name = "cdclient", nullable = false)),
      @AttributeOverride(name = "cdbolsas", column = @Column(name = "cdbolsas", nullable = false)) })
  protected Fcar0ordId id;

  /**
   * Table Field CDPROTER. (P)PROPIO/(T)TERCER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.lang.String cdproter;
  /**
   * Table Field CDINDCAR. (C)CASA/(P)PROPIA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.lang.String cdindcar;
  /**
   * Table Field CDORDENA. ORDENANTE CARTERA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.lang.String cdordena;
  /**
   * Table Field CDSUBORD. SUBORDENANTE CARTERA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.lang.String cdsubord;
  /**
   * Table Field FHMODIFI. FECHA ULT.MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.math.BigDecimal fhmodifi;
  /**
   * Table Field HRMODIFI. HORA ULT.MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.math.BigDecimal hrmodifi;
  /**
   * Table Field CDUSRMOD. USUARIO ULT.MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  protected java.lang.String cdusrmod;
  public Fcar0ordId getId() {
    return id;
  }
  public java.lang.String getCdproter() {
    return cdproter;
  }
  public java.lang.String getCdindcar() {
    return cdindcar;
  }
  public java.lang.String getCdordena() {
    return cdordena;
  }
  public java.lang.String getCdsubord() {
    return cdsubord;
  }
  public java.math.BigDecimal getFhmodifi() {
    return fhmodifi;
  }
  public java.math.BigDecimal getHrmodifi() {
    return hrmodifi;
  }
  public java.lang.String getCdusrmod() {
    return cdusrmod;
  }
  public void setId(Fcar0ordId id) {
    this.id = id;
  }
  public void setCdproter(java.lang.String cdproter) {
    this.cdproter = cdproter;
  }
  public void setCdindcar(java.lang.String cdindcar) {
    this.cdindcar = cdindcar;
  }
  public void setCdordena(java.lang.String cdordena) {
    this.cdordena = cdordena;
  }
  public void setCdsubord(java.lang.String cdsubord) {
    this.cdsubord = cdsubord;
  }
  public void setFhmodifi(java.math.BigDecimal fhmodifi) {
    this.fhmodifi = fhmodifi;
  }
  public void setHrmodifi(java.math.BigDecimal hrmodifi) {
    this.hrmodifi = hrmodifi;
  }
  public void setCdusrmod(java.lang.String cdusrmod) {
    this.cdusrmod = cdusrmod;
  }
  @Override
  public String toString() {
    return String.format(
        "Fcar0ord [id=%s, cdproter=%s, cdindcar=%s, cdordena=%s, cdsubord=%s, fhmodifi=%s, hrmodifi=%s, cdusrmod=%s]",
        id, cdproter, cdindcar, cdordena, cdsubord, fhmodifi, hrmodifi, cdusrmod);
  }
  
  
}