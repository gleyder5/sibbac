package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table TGRL0ORD. Tabla Ordenes Documentación de la
 * tablaTGRL0ORD <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TGRL0ORD")
public class Tgrl0ord implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -2039686472080154492L;

  /**
   * Table Field NUOPROUT. Nº Routing Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Id
  protected Integer nuoprout;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tgrl0ord")
  protected List<Tgrl0eje> tgrl0ejes = new ArrayList<Tgrl0eje>(0);

  /**
   * Table Field NUCONTRA. Nº Contrato Crédito Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 10, scale = 0)
  protected java.math.BigDecimal nucontra;
  /**
   * Table Field CDREFCRE. Referencia Crédito Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdrefcre;
  /**
   * Table Field CDUSUARI. Usuario Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdusuari;
  /**
   * Table Field CDCLIENT. Cliente (producto+cuenta) Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdclient;
  /**
   * Table Field CDISINVV. Valor ISIN Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 12)
  protected java.lang.String cdisinvv;
  /**
   * Table Field NUSERIEV. Serie Valor Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String nuseriev;
  /**
   * Table Field CDBOLSAS. Código bolsa Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String cdbolsas;
  /**
   * Table Field TPOPERAC. Tipo de operación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpoperac;
  /**
   * Table Field TPORDENS. Tipo de orden Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String tpordens;
  /**
   * Table Field TPMERCAD. Mercado Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpmercad;
  /**
   * Table Field CDLOTPIC. Código Lotes/Picos Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdlotpic;
  /**
   * Table Field FHRECORD. F. recepción orden Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhrecord;
  /**
   * Table Field HORECORD. H. recepción orden Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected java.math.BigDecimal horecord;
  /**
   * Table Field FHCONTRA. F. Contratación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhcontra;
  /**
   * Table Field FHCANCEL. F. Cancelación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhcancel;
  /**
   * Table Field HOCANCEL. H. Cancelación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected Integer hocancel;
  /**
   * Table Field FHPLAVAL. F. Plazo validez Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhplaval;
  /**
   * Table Field TPCONTRA. Tipo contratación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpcontra;
  /**
   * Table Field NUCANORD. Cantidad ordenada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal nucanord;
  /**
   * Table Field NUCANPDT. Cantidad pendiente Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal nucanpdt;
  /**
   * Table Field NUCANCAN. Cantidad cancelada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal nucancan;
  /**
   * Table Field NUCANOCU. Cantidad oculta Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal nucanocu;
  /**
   * Table Field NUCANMIN. Cantidad mínima Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal nucanmin;
  /**
   * Table Field TPCAMBIO. Tipo cambio Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpcambio;
  /**
   * Table Field IMLIMCAM. Límite cambio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 12, scale = 4)
  protected java.math.BigDecimal imlimcam;
  /**
   * Table Field IMCABSTO. Cambio ON STOP Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 12, scale = 4)
  protected java.math.BigDecimal imcabsto;
  /**
   * Table Field NUEJECUC. Nº Ejecuciones Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4)
  protected Short nuejecuc;

  /**
   * Table Field TOCANEJE. Cantidad ejecutada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal tocaneje;
  /**
   * Table Field CDESTADO. Estado orden Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdestado;
  /**
   * Table Field CDOPEMER. Operador mercado Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdopemer;
  /**
   * Table Field NUCEDENT. Cedente crédito Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 10, scale = 0)
  protected java.math.BigDecimal nucedent;
  /**
   * Table Field NUPARCAN. Parcial cancelación crédito Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected Short nuparcan;
  /**
   * Table Field CDGESTOR. Código gestor Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdgestor;
  /**
   * Table Field CDSISLIV. Sistema de liquidación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdsisliv;
  /**
   * Table Field CDCLASEV. Clase valor Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String cdclasev;
  /**
   * Table Field IMNOMVAL. Nominal valor Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 17, scale = 4)
  protected java.math.BigDecimal imnomval;
  /**
   * Table Field PCCOMISN. Porcentaje comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6, scale = 3)
  protected java.math.BigDecimal pccomisn;
  /**
   * Table Field CDESTORD. Situación orden Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdestord;
  /**
   * Table Field IMLCINTR. Límite cambio int. en mercado Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 11, scale = 2)
  protected java.math.BigDecimal imlcintr;
  /**
   * Table Field CDORIGEN. Código de origen Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String cdorigen;
  /**
   * Table Field CDDERECH. Código de derechos Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdderech;
  /**
   * Table Field CDCOMISN. Código de comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdcomisn;
  /**
   * Table Field TOCANCOM. Cantidad Títulos comunicada Stratus Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal tocancom;
  /**
   * Table Field TPSUBMER. Tipo Submercado Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String tpsubmer;
  /**
   * Table Field NUOPCLTE. Nº Operación Cliente Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 16)
  protected java.lang.String nuopclte;
  /**
   * Table Field CDDIVISA. Código de Divisa Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String cddivisa;
  /**
   * Table Field CDWAREHO. Código de WareHouse Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdwareho;
  /**
   * Table Field FHSOLCAN. F. Solicitud Cancelación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhsolcan;
  /**
   * Table Field HOSOLCAN. H. Solicitud Cancelación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected Integer hosolcan;
  /**
   * Table Field TPORIGEN. Tipo Orígen Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String tporigen;
  /**
   * Table Field CDCAMMED. Código Cambio Medio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdcammed;
  /**
   * Table Field CDCANENT. Código Canal de entrada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String cdcanent;
  /**
   * Table Field TPRESNEG. Tipo Restricción Negociación Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String tpresneg;
  /**
   * Table Field TPEJECUC. Tipo Restricción Ejecución Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String tpejecuc;

  public Integer getNuoprout() {
    return nuoprout;
  }

  public List<Tgrl0eje> getTgrl0ejes() {
    return tgrl0ejes;
  }

  public java.math.BigDecimal getNucontra() {
    return nucontra;
  }

  public Character getCdrefcre() {
    return cdrefcre;
  }

  public java.lang.String getCdusuari() {
    return cdusuari;
  }

  public java.lang.String getCdclient() {
    return cdclient;
  }

  public java.lang.String getCdisinvv() {
    return cdisinvv;
  }

  public java.lang.String getNuseriev() {
    return nuseriev;
  }

  public java.lang.String getCdbolsas() {
    return cdbolsas;
  }

  public Character getTpoperac() {
    return tpoperac;
  }

  public java.lang.String getTpordens() {
    return tpordens;
  }

  public Character getTpmercad() {
    return tpmercad;
  }

  public Character getCdlotpic() {
    return cdlotpic;
  }

  public Integer getFhrecord() {
    return fhrecord;
  }

  public java.math.BigDecimal getHorecord() {
    return horecord;
  }

  public Integer getFhcontra() {
    return fhcontra;
  }

  public Integer getFhcancel() {
    return fhcancel;
  }

  public Integer getHocancel() {
    return hocancel;
  }

  public Integer getFhplaval() {
    return fhplaval;
  }

  public Character getTpcontra() {
    return tpcontra;
  }

  public java.math.BigDecimal getNucanord() {
    return nucanord;
  }

  public java.math.BigDecimal getNucanpdt() {
    return nucanpdt;
  }

  public java.math.BigDecimal getNucancan() {
    return nucancan;
  }

  public java.math.BigDecimal getNucanocu() {
    return nucanocu;
  }

  public java.math.BigDecimal getNucanmin() {
    return nucanmin;
  }

  public Character getTpcambio() {
    return tpcambio;
  }

  public java.math.BigDecimal getImlimcam() {
    return imlimcam;
  }

  public java.math.BigDecimal getImcabsto() {
    return imcabsto;
  }

  public Short getNuejecuc() {
    return nuejecuc;
  }

  public java.math.BigDecimal getTocaneje() {
    return tocaneje;
  }

  public Character getCdestado() {
    return cdestado;
  }

  public java.lang.String getCdopemer() {
    return cdopemer;
  }

  public java.math.BigDecimal getNucedent() {
    return nucedent;
  }

  public Short getNuparcan() {
    return nuparcan;
  }

  public java.lang.String getCdgestor() {
    return cdgestor;
  }

  public Character getCdsisliv() {
    return cdsisliv;
  }

  public java.lang.String getCdclasev() {
    return cdclasev;
  }

  public java.math.BigDecimal getImnomval() {
    return imnomval;
  }

  public java.math.BigDecimal getPccomisn() {
    return pccomisn;
  }

  public Character getCdestord() {
    return cdestord;
  }

  public java.math.BigDecimal getImlcintr() {
    return imlcintr;
  }

  public java.lang.String getCdorigen() {
    return cdorigen;
  }

  public Character getCdderech() {
    return cdderech;
  }

  public Character getCdcomisn() {
    return cdcomisn;
  }

  public java.math.BigDecimal getTocancom() {
    return tocancom;
  }

  public java.lang.String getTpsubmer() {
    return tpsubmer;
  }

  public java.lang.String getNuopclte() {
    return nuopclte;
  }

  public java.lang.String getCddivisa() {
    return cddivisa;
  }

  public Character getCdwareho() {
    return cdwareho;
  }

  public Integer getFhsolcan() {
    return fhsolcan;
  }

  public Integer getHosolcan() {
    return hosolcan;
  }

  public java.lang.String getTporigen() {
    return tporigen;
  }

  public Character getCdcammed() {
    return cdcammed;
  }

  public java.lang.String getCdcanent() {
    return cdcanent;
  }

  public java.lang.String getTpresneg() {
    return tpresneg;
  }

  public java.lang.String getTpejecuc() {
    return tpejecuc;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  public void setTgrl0ejes(List<Tgrl0eje> tgrl0ejes) {
    this.tgrl0ejes = tgrl0ejes;
  }

  public void setNucontra(java.math.BigDecimal nucontra) {
    this.nucontra = nucontra;
  }

  public void setCdrefcre(Character cdrefcre) {
    this.cdrefcre = cdrefcre;
  }

  public void setCdusuari(java.lang.String cdusuari) {
    this.cdusuari = cdusuari;
  }

  public void setCdclient(java.lang.String cdclient) {
    this.cdclient = cdclient;
  }

  public void setCdisinvv(java.lang.String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  public void setNuseriev(java.lang.String nuseriev) {
    this.nuseriev = nuseriev;
  }

  public void setCdbolsas(java.lang.String cdbolsas) {
    this.cdbolsas = cdbolsas;
  }

  public void setTpoperac(Character tpoperac) {
    this.tpoperac = tpoperac;
  }

  public void setTpordens(java.lang.String tpordens) {
    this.tpordens = tpordens;
  }

  public void setTpmercad(Character tpmercad) {
    this.tpmercad = tpmercad;
  }

  public void setCdlotpic(Character cdlotpic) {
    this.cdlotpic = cdlotpic;
  }

  public void setFhrecord(Integer fhrecord) {
    this.fhrecord = fhrecord;
  }

  public void setHorecord(java.math.BigDecimal horecord) {
    this.horecord = horecord;
  }

  public void setFhcontra(Integer fhcontra) {
    this.fhcontra = fhcontra;
  }

  public void setFhcancel(Integer fhcancel) {
    this.fhcancel = fhcancel;
  }

  public void setHocancel(Integer hocancel) {
    this.hocancel = hocancel;
  }

  public void setFhplaval(Integer fhplaval) {
    this.fhplaval = fhplaval;
  }

  public void setTpcontra(Character tpcontra) {
    this.tpcontra = tpcontra;
  }

  public void setNucanord(java.math.BigDecimal nucanord) {
    this.nucanord = nucanord;
  }

  public void setNucanpdt(java.math.BigDecimal nucanpdt) {
    this.nucanpdt = nucanpdt;
  }

  public void setNucancan(java.math.BigDecimal nucancan) {
    this.nucancan = nucancan;
  }

  public void setNucanocu(java.math.BigDecimal nucanocu) {
    this.nucanocu = nucanocu;
  }

  public void setNucanmin(java.math.BigDecimal nucanmin) {
    this.nucanmin = nucanmin;
  }

  public void setTpcambio(Character tpcambio) {
    this.tpcambio = tpcambio;
  }

  public void setImlimcam(java.math.BigDecimal imlimcam) {
    this.imlimcam = imlimcam;
  }

  public void setImcabsto(java.math.BigDecimal imcabsto) {
    this.imcabsto = imcabsto;
  }

  public void setNuejecuc(Short nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public void setTocaneje(java.math.BigDecimal tocaneje) {
    this.tocaneje = tocaneje;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  public void setCdopemer(java.lang.String cdopemer) {
    this.cdopemer = cdopemer;
  }

  public void setNucedent(java.math.BigDecimal nucedent) {
    this.nucedent = nucedent;
  }

  public void setNuparcan(Short nuparcan) {
    this.nuparcan = nuparcan;
  }

  public void setCdgestor(java.lang.String cdgestor) {
    this.cdgestor = cdgestor;
  }

  public void setCdsisliv(Character cdsisliv) {
    this.cdsisliv = cdsisliv;
  }

  public void setCdclasev(java.lang.String cdclasev) {
    this.cdclasev = cdclasev;
  }

  public void setImnomval(java.math.BigDecimal imnomval) {
    this.imnomval = imnomval;
  }

  public void setPccomisn(java.math.BigDecimal pccomisn) {
    this.pccomisn = pccomisn;
  }

  public void setCdestord(Character cdestord) {
    this.cdestord = cdestord;
  }

  public void setImlcintr(java.math.BigDecimal imlcintr) {
    this.imlcintr = imlcintr;
  }

  public void setCdorigen(java.lang.String cdorigen) {
    this.cdorigen = cdorigen;
  }

  public void setCdderech(Character cdderech) {
    this.cdderech = cdderech;
  }

  public void setCdcomisn(Character cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  public void setTocancom(java.math.BigDecimal tocancom) {
    this.tocancom = tocancom;
  }

  public void setTpsubmer(java.lang.String tpsubmer) {
    this.tpsubmer = tpsubmer;
  }

  public void setNuopclte(java.lang.String nuopclte) {
    this.nuopclte = nuopclte;
  }

  public void setCddivisa(java.lang.String cddivisa) {
    this.cddivisa = cddivisa;
  }

  public void setCdwareho(Character cdwareho) {
    this.cdwareho = cdwareho;
  }

  public void setFhsolcan(Integer fhsolcan) {
    this.fhsolcan = fhsolcan;
  }

  public void setHosolcan(Integer hosolcan) {
    this.hosolcan = hosolcan;
  }

  public void setTporigen(java.lang.String tporigen) {
    this.tporigen = tporigen;
  }

  public void setCdcammed(Character cdcammed) {
    this.cdcammed = cdcammed;
  }

  public void setCdcanent(java.lang.String cdcanent) {
    this.cdcanent = cdcanent;
  }

  public void setTpresneg(java.lang.String tpresneg) {
    this.tpresneg = tpresneg;
  }

  public void setTpejecuc(java.lang.String tpejecuc) {
    this.tpejecuc = tpejecuc;
  }

  @Override
  public String toString() {
    return String.format("Tgrl0ord [nuoprout=%d]", nuoprout);
  }

}