package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0asd;
import sibbac.database.megbpdta.model.Fmeg0asdId;

@Repository
public interface Fmeg0asdDao extends JpaRepository<Fmeg0asd, Fmeg0asdId> {

  @Query(value = "select max(a.id.nusecuen) from Fmeg0asd a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

}
