package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0FIE. Ficheros Tratados MEGARA
 * Documentación para FMEG0FIE <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0FIE", schema = "MEGBPDTA")
public class Fmeg0fie implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 107319479372105301L;
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "nomxml", column = @Column(name = "nomxml", nullable = false)),
      @AttributeOverride(name = "fecproc", column = @Column(name = "fecproc", nullable = false)),
      @AttributeOverride(name = "horproc", column = @Column(name = "horproc", nullable = false)) })
  private Fmeg0fieId id;

  /**
   * Table Field NOMINTER. Nombre de Interface Nombre de Interface <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nominter;
  /**
   * Table Field CDINDETI. Codigo identificador Código identificador <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdindeti;
  /**
   * Table Field NOMENSA. Mensaje Mensaje <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nomensa;
  /**
   * Table Field ESTAFICH. Estado del fichero Estado del fichero <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String estafich;
  /**
   * Table Field NOMPGM. Nombre programa ejecutado Nombre programa ejecutado
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nompgm;
  /**
   * Table Field TPFICHER. Tipo fichero Tipo fichero <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpficher;
  /**
   * Table Field NOMUSUA. Usuario Documentación para NOMUSUA <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nomusua;
  /**
   * Table Field FECACTU. Fecha actualizacion Documentación para FECACTU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fecactu;
  /**
   * Table Field HORACTU. Hora actualizacion Documentación para HORACTU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal horactu;
  
  

  public Fmeg0fieId getId() {
    return id;
  }

  public java.lang.String getNominter() {
    return nominter;
  }

  public java.lang.String getCdindeti() {
    return cdindeti;
  }

  public java.lang.String getNomensa() {
    return nomensa;
  }

  public java.lang.String getEstafich() {
    return estafich;
  }

  public java.lang.String getNompgm() {
    return nompgm;
  }

  public java.lang.String getTpficher() {
    return tpficher;
  }

  public java.lang.String getNomusua() {
    return nomusua;
  }

  public java.math.BigDecimal getFecactu() {
    return fecactu;
  }

  public java.math.BigDecimal getHoractu() {
    return horactu;
  }

  public void setId(Fmeg0fieId id) {
    this.id = id;
  }

  public void setNominter(java.lang.String nominter) {
    this.nominter = nominter;
  }

  public void setCdindeti(java.lang.String cdindeti) {
    this.cdindeti = cdindeti;
  }

  public void setNomensa(java.lang.String nomensa) {
    this.nomensa = nomensa;
  }

  public void setEstafich(java.lang.String estafich) {
    this.estafich = estafich;
  }

  public void setNompgm(java.lang.String nompgm) {
    this.nompgm = nompgm;
  }

  public void setTpficher(java.lang.String tpficher) {
    this.tpficher = tpficher;
  }

  public void setNomusua(java.lang.String nomusua) {
    this.nomusua = nomusua;
  }

  public void setFecactu(java.math.BigDecimal fecactu) {
    this.fecactu = fecactu;
  }

  public void setHoractu(java.math.BigDecimal horactu) {
    this.horactu = horactu;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s|%s", id.getNomxml(), id.getFecproc(), id.getHorproc());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0FIE";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0fie [id=%s, nominter=%s, cdindeti=%s, nomensa=%s, estafich=%s, nompgm=%s, tpficher=%s, nomusua=%s, fecactu=%s, horactu=%s]",
            id, nominter, cdindeti, nomensa, estafich, nompgm, tpficher, nomusua, fecactu, horactu);
  }
  
  
}