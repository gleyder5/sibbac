package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table TCLI0hpc . PRODUCTOS/CLIENTE historico
 * Documentación de la tablaTCLI0PCL <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Entity
@Table(name = "TCLI0PCL")
public class Tcli0hpc implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -141924327236731943L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdproduc", column = @Column(name = "cdproduc", nullable = false)),
      @AttributeOverride(name = "nuctapro", column = @Column(name = "nuctapro", nullable = false)),
      @AttributeOverride(name = "nuclient", column = @Column(name = "nuclient", nullable = false)),
      @AttributeOverride(name = "fhenthis", column = @Column(name = "fhenthis", nullable = false)),
      @AttributeOverride(name = "hoenthis", column = @Column(name = "hoenthis", nullable = false)),
      @AttributeOverride(name = "cdurshis", column = @Column(name = "cdurshis", nullable = false)),
      @AttributeOverride(name = "cdaccion", column = @Column(name = "cdaccion", nullable = false)) })
  protected Tcli0hpcId id;

  @Column(nullable = false)
  protected java.lang.String nutituls;

  @Column(nullable = false)
  protected java.lang.String nuposici;

  @Column(nullable = false)
  protected java.lang.String cdprogrl;

  @Column(nullable = false)
  protected java.math.BigDecimal nuctagrl;

  @Column(nullable = false)
  protected java.lang.String tptiprep;
  
  
  

  public Tcli0hpcId getId() {
    return id;
  }

  public void setId(Tcli0hpcId id) {
    this.id = id;
  }

  public java.lang.String getNutituls() {
    return nutituls;
  }

  public java.lang.String getNuposici() {
    return nuposici;
  }

  public java.lang.String getCdprogrl() {
    return cdprogrl;
  }

  public java.math.BigDecimal getNuctagrl() {
    return nuctagrl;
  }

  public java.lang.String getTptiprep() {
    return tptiprep;
  }

  public void setNutituls(java.lang.String nutituls) {
    this.nutituls = nutituls;
  }

  public void setNuposici(java.lang.String nuposici) {
    this.nuposici = nuposici;
  }

  public void setCdprogrl(java.lang.String cdprogrl) {
    this.cdprogrl = cdprogrl;
  }

  public void setNuctagrl(java.math.BigDecimal nuctagrl) {
    this.nuctagrl = nuctagrl;
  }

  public void setTptiprep(java.lang.String tptiprep) {
    this.tptiprep = tptiprep;
  }

  @Override
  public String toString() {
    return String.format("Tcli0hpc [id=%s,  nutituls=%s, nuposici=%s, cdprogrl=%s, nuctagrl=%s, tptiprep=%s]", id,
        nutituls, nuposici, cdprogrl, nuctagrl, tptiprep);
  }

}