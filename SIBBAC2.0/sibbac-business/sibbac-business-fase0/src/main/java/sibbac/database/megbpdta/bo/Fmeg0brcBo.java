package sibbac.database.megbpdta.bo;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0brcDao;
import sibbac.database.megbpdta.model.Fmeg0brc;

@Service
public class Fmeg0brcBo extends AbstractFmegBo<Fmeg0brc, String, Fmeg0brcDao> {

}
