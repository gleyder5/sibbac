package sibbac.database.bsnbpsql.tcli.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tcli.model.Tcli0hpc;
import sibbac.database.bsnbpsql.tcli.model.Tcli0hpcId;

@Repository
public interface Tcli0hpcDao extends JpaRepository<Tcli0hpc, Tcli0hpcId> {

}
