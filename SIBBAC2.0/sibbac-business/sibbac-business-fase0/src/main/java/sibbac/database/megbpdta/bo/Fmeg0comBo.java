package sibbac.database.megbpdta.bo;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0comDao;
import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0comId;

@Service
public class Fmeg0comBo extends AbstractFmegBo<Fmeg0com, Fmeg0comId, Fmeg0comDao> {

}
