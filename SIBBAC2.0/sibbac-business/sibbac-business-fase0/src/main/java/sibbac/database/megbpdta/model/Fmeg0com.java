package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0COM. Comisiones Documentación para
 * FMEG0COM <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0COM", schema = "MEGBPDTA")
public class Fmeg0com implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 3802463797039336572L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdtpcomi", column = @Column(name = "cdtpcomi", nullable = false)),
      @AttributeOverride(name = "cdcomisi", column = @Column(name = "cdcomisi", nullable = false)),
      @AttributeOverride(name = "cdtarifa", column = @Column(name = "cdtarifa", nullable = false)) })
  protected Fmeg0comId id;

  /**
   * Table Field IDNUMREG. Numero registro MEGATRADE: numreg <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal idnumreg;
  /**
   * Table Field TPMODOOP. modo 1 ó 2 MEGATRADE: modo 1 ó 2 <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodoop;
  /**
   * Table Field CDBROCLI. broClient MEGARA: identifier FIDESSA:
   * CLIENTGROUP_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbrocli;
  /**
   * Table Field CDALIASS. Alias MEGARA: Alias FIDESSA: CLIENT_MNEMONIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdaliass;
  /**
   * Table Field CDSUBCTA. identifier MEGARA: identifier FIDESSA:
   * ACCOUNT_MNEMONIC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdsubcta;
  /**
   * Table Field CDBROKER. broker MEGARA: broker <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbroker;
  /**
   * Table Field CDTRATOO. cdtrato 1 ó 2 MEGATRADE: cdtraro 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtratoo;
  /**
   * Table Field TPCDVIAA. cdvia 1ó 2 MEGATRADE: cdvia 1 ó 2 <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpcdviaa;
  /**
   * Table Field DSGTOMDO. dsgtomdo 1 ó 2 MEGATRADE: dsgtomdo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String dsgtomdo;
  /**
   * Table Field CDRESIDE. residence MEGARA: residence FIDESSA:
   * COUNTRY_OF_DOMICILE <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdreside;
  /**
   * Table Field CDXTBROP. xtbroper 1 ó 2 MEGATRADE: xtbroper 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdxtbrop;
  /**
   * Table Field CDINDICE. cdindice 1 ó 2 MEGATRADE: cdindice 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdindice;
  /**
   * Table Field CDCLSMDO. cdclsmdo 1 ó 2 MEGATRADE : cdclsmdo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdclsmdo;
  /**
   * Table Field CDCANALL. cdcanal 1 ó 2 MEGATRADE : cdcanal 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcanall;
  /**
   * Table Field CDMARKET. market MEGARA: market FIDESSA: ELIGIBLE_CLEARER_MKT,
   * MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field CDTPMERC. cdtpmerc 1 ó 2 MEGATRADE: cdtpmerc 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtpmerc;
  /**
   * Table Field CDORIGEN. cdorigen 1 ó 2 MEGATRADE: cdorigen 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdorigen;
  /**
   * Table Field CDTPOPER. cdtpoper 1 ó 2 MEGATRADE: cdtpoper 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtpoper;
  /**
   * Table Field CDTPOPTV. cdtpoptv 1 ó 2 MEGATRADE: cdtpoptv 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtpoptv;
  /**
   * Table Field CDACTIVO. cdactivo 1 ó 2 MEGATRADE: cdactivo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdactivo;
  /**
   * Table Field CDNIFEMI. cdnifemi 1 ó 2 MEGATRADE: cdnifemi 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnifemi;
  /**
   * Table Field CDISINVV. cdisin 1 ó 2 MEGATRADE: cdisin 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdisinvv;
  /**
   * Table Field CDPERIOD. cdperio 1 ó 2 MEGATRADE: cdperio 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdperiod;
  /**
   * Table Field FHINIVIG. feinvig 1 ó 2 MEGATRADE: feinvig 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhinivig;
  /**
   * Table Field FHFINVIG. fefinvig 1 ó 2 MEGATRADE: fefinvig 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhfinvig;
  /**
   * Table Field CDTPTARI. cdtptari 1 ó 2 MEGATRADE: cdtptari 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtptari;
  /**
   * Table Field IMCOMFIJ. imfijo 1 ó 2 MEGATRADE: imfijo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal imcomfij;
  /**
   * Table Field PCTARIFA. potarifa 1 ó 2 MEGATRADE: potarifa 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal pctarifa;
  /**
   * Table Field CDUNIDAD. cdunidad 1 ó 2 MEGATRADE: cdunidad 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdunidad;
  /**
   * Table Field IMMINIMO. imminimo 1 ó 2 MEGATRADE: imminimo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal imminimo;
  /**
   * Table Field IMMAXIMO. immaximo 1 ó 2 MEGATRADE: immaximo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal immaximo;
  /**
   * Table Field IMDESCUE. imdescue 1 ó 2 MEGATRADE: imdescue 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal imdescue;
  /**
   * Table Field CDMONISO. cdmoniso 1 ó 2 MEGATRADE: cdmoniso 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmoniso;
  /**
   * Table Field CDTPDEVO. cdtpdevo 1 ó 2 MEGATRADE: cdtpdevo 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtpdevo;
  /**
   * Table Field CDTRAMOS. xttramos 1 ó 2 MEGATRADE: xttramos 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtramos;
  /**
   * Table Field CDTPCALC. cdtpcalc 1 ó 2 MEGATRADE: cdtpcalc 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtpcalc;
  /**
   * Table Field CDTPTRAM. cdtptram 1 ó 2 MEGATRADE: cdtptram 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtptram;
  /**
   * Table Field CDEXISTE. xtexiste 1 ó 2 MEGATRADE: xtexiste 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdexiste;
  /**
   * Table Field FHESTADO. feestado 1 ó 2 MEGATRADE: feestado 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhestado;
  /**
   * Table Field CDEXCEPC. xtexcepc 1 ö 2 MEGATRADE: xtexcepc 1 ö 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdexcepc;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0com")
  private List<Fmeg0ctr> fmeg0crts = new ArrayList<Fmeg0ctr>();

  public Fmeg0comId getId() {
    return id;
  }

  public java.math.BigDecimal getIdnumreg() {
    return idnumreg;
  }

  public java.lang.String getTpmodoop() {
    return tpmodoop;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  public java.lang.String getCdbroker() {
    return cdbroker;
  }

  public java.lang.String getCdtratoo() {
    return cdtratoo;
  }

  public java.lang.String getTpcdviaa() {
    return tpcdviaa;
  }

  public java.lang.String getDsgtomdo() {
    return dsgtomdo;
  }

  public java.lang.String getCdreside() {
    return cdreside;
  }

  public java.lang.String getCdxtbrop() {
    return cdxtbrop;
  }

  public java.lang.String getCdindice() {
    return cdindice;
  }

  public java.lang.String getCdclsmdo() {
    return cdclsmdo;
  }

  public java.lang.String getCdcanall() {
    return cdcanall;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getCdtpmerc() {
    return cdtpmerc;
  }

  public java.lang.String getCdorigen() {
    return cdorigen;
  }

  public java.lang.String getCdtpoper() {
    return cdtpoper;
  }

  public java.lang.String getCdtpoptv() {
    return cdtpoptv;
  }

  public java.lang.String getCdactivo() {
    return cdactivo;
  }

  public java.lang.String getCdnifemi() {
    return cdnifemi;
  }

  public java.lang.String getCdisinvv() {
    return cdisinvv;
  }

  public java.lang.String getCdperiod() {
    return cdperiod;
  }

  public java.lang.String getFhinivig() {
    return fhinivig;
  }

  public java.lang.String getFhfinvig() {
    return fhfinvig;
  }

  public java.lang.String getCdtptari() {
    return cdtptari;
  }

  public java.math.BigDecimal getImcomfij() {
    return imcomfij;
  }

  public java.math.BigDecimal getPctarifa() {
    return pctarifa;
  }

  public java.lang.String getCdunidad() {
    return cdunidad;
  }

  public java.math.BigDecimal getImminimo() {
    return imminimo;
  }

  public java.math.BigDecimal getImmaximo() {
    return immaximo;
  }

  public java.math.BigDecimal getImdescue() {
    return imdescue;
  }

  public java.lang.String getCdmoniso() {
    return cdmoniso;
  }

  public java.lang.String getCdtpdevo() {
    return cdtpdevo;
  }

  public java.lang.String getCdtramos() {
    return cdtramos;
  }

  public java.lang.String getCdtpcalc() {
    return cdtpcalc;
  }

  public java.lang.String getCdtptram() {
    return cdtptram;
  }

  public java.lang.String getCdexiste() {
    return cdexiste;
  }

  public java.lang.String getFhestado() {
    return fhestado;
  }

  public java.lang.String getCdexcepc() {
    return cdexcepc;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public void setId(Fmeg0comId id) {
    this.id = id;
  }

  public void setIdnumreg(java.math.BigDecimal idnumreg) {
    this.idnumreg = idnumreg;
  }

  public void setTpmodoop(java.lang.String tpmodoop) {
    this.tpmodoop = tpmodoop;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  public void setCdbroker(java.lang.String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdtratoo(java.lang.String cdtratoo) {
    this.cdtratoo = cdtratoo;
  }

  public void setTpcdviaa(java.lang.String tpcdviaa) {
    this.tpcdviaa = tpcdviaa;
  }

  public void setDsgtomdo(java.lang.String dsgtomdo) {
    this.dsgtomdo = dsgtomdo;
  }

  public void setCdreside(java.lang.String cdreside) {
    this.cdreside = cdreside;
  }

  public void setCdxtbrop(java.lang.String cdxtbrop) {
    this.cdxtbrop = cdxtbrop;
  }

  public void setCdindice(java.lang.String cdindice) {
    this.cdindice = cdindice;
  }

  public void setCdclsmdo(java.lang.String cdclsmdo) {
    this.cdclsmdo = cdclsmdo;
  }

  public void setCdcanall(java.lang.String cdcanall) {
    this.cdcanall = cdcanall;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setCdtpmerc(java.lang.String cdtpmerc) {
    this.cdtpmerc = cdtpmerc;
  }

  public void setCdorigen(java.lang.String cdorigen) {
    this.cdorigen = cdorigen;
  }

  public void setCdtpoper(java.lang.String cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public void setCdtpoptv(java.lang.String cdtpoptv) {
    this.cdtpoptv = cdtpoptv;
  }

  public void setCdactivo(java.lang.String cdactivo) {
    this.cdactivo = cdactivo;
  }

  public void setCdnifemi(java.lang.String cdnifemi) {
    this.cdnifemi = cdnifemi;
  }

  public void setCdisinvv(java.lang.String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  public void setCdperiod(java.lang.String cdperiod) {
    this.cdperiod = cdperiod;
  }

  public void setFhinivig(java.lang.String fhinivig) {
    this.fhinivig = fhinivig;
  }

  public void setFhfinvig(java.lang.String fhfinvig) {
    this.fhfinvig = fhfinvig;
  }

  public void setCdtptari(java.lang.String cdtptari) {
    this.cdtptari = cdtptari;
  }

  public void setImcomfij(java.math.BigDecimal imcomfij) {
    this.imcomfij = imcomfij;
  }

  public void setPctarifa(java.math.BigDecimal pctarifa) {
    this.pctarifa = pctarifa;
  }

  public void setCdunidad(java.lang.String cdunidad) {
    this.cdunidad = cdunidad;
  }

  public void setImminimo(java.math.BigDecimal imminimo) {
    this.imminimo = imminimo;
  }

  public void setImmaximo(java.math.BigDecimal immaximo) {
    this.immaximo = immaximo;
  }

  public void setImdescue(java.math.BigDecimal imdescue) {
    this.imdescue = imdescue;
  }

  public void setCdmoniso(java.lang.String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setCdtpdevo(java.lang.String cdtpdevo) {
    this.cdtpdevo = cdtpdevo;
  }

  public void setCdtramos(java.lang.String cdtramos) {
    this.cdtramos = cdtramos;
  }

  public void setCdtpcalc(java.lang.String cdtpcalc) {
    this.cdtpcalc = cdtpcalc;
  }

  public void setCdtptram(java.lang.String cdtptram) {
    this.cdtptram = cdtptram;
  }

  public void setCdexiste(java.lang.String cdexiste) {
    this.cdexiste = cdexiste;
  }

  public void setFhestado(java.lang.String fhestado) {
    this.fhestado = fhestado;
  }

  public void setCdexcepc(java.lang.String cdexcepc) {
    this.cdexcepc = cdexcepc;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public List<Fmeg0ctr> getFmeg0crts() {
    return fmeg0crts;
  }

  public void setFmeg0crts(List<Fmeg0ctr> fmeg0crts) {
    this.fmeg0crts = fmeg0crts;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s|%s]", id.getCdtpcomi(), id.getCdcomisi(), id.getCdtarifa());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0COM";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0com [id=%s, idnumreg=%s, tpmodoop=%s, cdbrocli=%s, cdaliass=%s, cdsubcta=%s, cdbroker=%s, cdtratoo=%s, tpcdviaa=%s, dsgtomdo=%s, cdreside=%s, cdxtbrop=%s, cdindice=%s, cdclsmdo=%s, cdcanall=%s, cdmarket=%s, cdtpmerc=%s, cdorigen=%s, cdtpoper=%s, cdtpoptv=%s, cdactivo=%s, cdnifemi=%s, cdisinvv=%s, cdperiod=%s, fhinivig=%s, fhfinvig=%s, cdtptari=%s, imcomfij=%s, pctarifa=%s, cdunidad=%s, imminimo=%s, immaximo=%s, imdescue=%s, cdmoniso=%s, cdtpdevo=%s, cdtramos=%s, cdtpcalc=%s, cdtptram=%s, cdexiste=%s, fhestado=%s, cdexcepc=%s, fhdebaja=%s, tpmodeid=%s]",
            id, idnumreg, tpmodoop, cdbrocli, cdaliass, cdsubcta, cdbroker, cdtratoo, tpcdviaa, dsgtomdo, cdreside,
            cdxtbrop, cdindice, cdclsmdo, cdcanall, cdmarket, cdtpmerc, cdorigen, cdtpoper, cdtpoptv, cdactivo,
            cdnifemi, cdisinvv, cdperiod, fhinivig, fhfinvig, cdtptari, imcomfij, pctarifa, cdunidad, imminimo,
            immaximo, imdescue, cdmoniso, cdtpdevo, cdtramos, cdtpcalc, cdtptram, cdexiste, fhestado, cdexcepc,
            fhdebaja, tpmodeid);
  }

}