package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Fmeg0cfiId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4937610674209731036L;

  /**
   * Table Field IDCAMAS4. Campo del AS/400 Documentación para IDCAMAS4 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idcamas4;

  /**
   * Table Field DSAAS400. Informacion almadenada en AS/400 Documentación para
   * DSAAS400 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String dsaas400;
  
  public Fmeg0cfiId() {
  }

  public Fmeg0cfiId(String idcamas4, String dsaas400) {
    super();
    this.idcamas4 = idcamas4;
    this.dsaas400 = dsaas400;
  }

  public java.lang.String getIdcamas4() {
    return idcamas4;
  }

  public java.lang.String getDsaas400() {
    return dsaas400;
  }

  public void setIdcamas4(java.lang.String idcamas4) {
    this.idcamas4 = idcamas4;
  }

  public void setDsaas400(java.lang.String dsaas400) {
    this.dsaas400 = dsaas400;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dsaas400 == null) ? 0 : dsaas400.hashCode());
    result = prime * result + ((idcamas4 == null) ? 0 : idcamas4.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0cfiId other = (Fmeg0cfiId) obj;
    if (dsaas400 == null) {
      if (other.dsaas400 != null)
        return false;
    }
    else if (!dsaas400.equals(other.dsaas400))
      return false;
    if (idcamas4 == null) {
      if (other.idcamas4 != null)
        return false;
    }
    else if (!idcamas4.equals(other.idcamas4))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0cfiId [idcamas4=%s, dsaas400=%s]", idcamas4, dsaas400);
  }
  
  
  

}
