package sibbac.database.bsnvasql.model;


// Generated 30-jul-2015 12:08:15 by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * Tval0ps1Id generated by hbm2java
 */
@Embeddable
public class Tval0ps1Id implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2439503540573905387L;
	private String				cdhppais;
	private Integer					fhdebaja;

	public Tval0ps1Id() {
	}

	public Tval0ps1Id( String cdhppais, Integer fhdebaja ) {
		this.cdhppais = cdhppais;
		this.fhdebaja = fhdebaja;
	}

	@Column( name = "CDHPPAIS", unique = true, nullable = false, length = 3 )
	public String getCdhppais() {
		return this.cdhppais;
	}

	public void setCdhppais( String cdhppais ) {
		this.cdhppais = cdhppais;
	}

	@Column( name = "FHDEBAJA", nullable = false, precision = 8, scale = 0 )
	public Integer getFhdebaja() {
		return this.fhdebaja;
	}

	public void setFhdebaja( Integer fhdebaja ) {
		this.fhdebaja = fhdebaja;
	}

	public boolean equals( Object other ) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof Tval0ps1Id ) )
			return false;
		Tval0ps1Id castOther = ( Tval0ps1Id ) other;

		return ( ( this.getCdhppais() == castOther.getCdhppais() ) || ( this.getCdhppais() != null && castOther.getCdhppais() != null && this
				.getCdhppais().equals( castOther.getCdhppais() ) ) ) && ( this.getFhdebaja() == castOther.getFhdebaja() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getCdhppais() == null ? 0 : this.getCdhppais().hashCode() );
		result = 37 * result + this.getFhdebaja();
		return result;
	}

}
