package sibbac.database.betbtdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table BETPFECL. Equivalencias Documentación de la
 * tablaBETPFECL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "BETPFECT_1", schema = "betbtdta")
public class Betpfect1 implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3123716921495236183L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "ectoms", column = @Column(name = "ectoms", nullable = false)),
      @AttributeOverride(name = "ectcgl", column = @Column(name = "ectcgl", nullable = false)),
      @AttributeOverride(name = "ectpro", column = @Column(name = "ectpro", nullable = false)) })
  protected Betpfect1Id id;

  /**
   * Table Field ectFAL. FECHA ALTA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal ectcta;

  /**
   * Table Field ectFAL. FECHA ALTA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal ectfal;
  /**
   * Table Field ectFBA. FECHA BAJA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal ectfba;
  /**
   * Table Field ectHMO. HORA MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ecthmo;
  /**
   * Table Field ectUMO. USUARIO MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ectumo;

  public Betpfect1Id getId() {
    return id;
  }

  public java.math.BigDecimal getEctcta() {
    return ectcta;
  }

  public java.math.BigDecimal getEctfal() {
    return ectfal;
  }

  public java.math.BigDecimal getEctfba() {
    return ectfba;
  }

  public java.lang.String getEcthmo() {
    return ecthmo;
  }

  public java.lang.String getEctumo() {
    return ectumo;
  }

  public void setId(Betpfect1Id id) {
    this.id = id;
  }

  public void setEctcta(java.math.BigDecimal ectcta) {
    this.ectcta = ectcta;
  }

  public void setEctfal(java.math.BigDecimal ectfal) {
    this.ectfal = ectfal;
  }

  public void setEctfba(java.math.BigDecimal ectfba) {
    this.ectfba = ectfba;
  }

  public void setEcthmo(java.lang.String ecthmo) {
    this.ecthmo = ecthmo;
  }

  public void setEctumo(java.lang.String ectumo) {
    this.ectumo = ectumo;
  }

  @Override
  public String toString() {
    return String.format("Betpfect [id=%s, ectcta=%s, ectfal=%s, ectfba=%s, ecthmo=%s, ectumo=%s]", id, ectcta, ectfal,
        ectfba, ecthmo, ectumo);
  }
  
  

}