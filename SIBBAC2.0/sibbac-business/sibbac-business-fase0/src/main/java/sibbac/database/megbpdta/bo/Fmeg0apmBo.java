package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0apmDao;
import sibbac.database.megbpdta.model.Fmeg0apm;
import sibbac.database.megbpdta.model.Fmeg0apmId;

@Service
public class Fmeg0apmBo extends AbstractFmegBo<Fmeg0apm, Fmeg0apmId, Fmeg0apmDao> {

  @Override
  @Transactional
  public Fmeg0apm save(Fmeg0apm entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
