package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0aotDao;
import sibbac.database.megbpdta.model.Fmeg0aot;
import sibbac.database.megbpdta.model.Fmeg0aotId;

@Service
public class Fmeg0aotBo extends AbstractFmegBo<Fmeg0aot, Fmeg0aotId, Fmeg0aotDao> {
  
  public List<Fmeg0aot> findAllByCdaliassAndTpidtype(String cdaliass, String tpidtype) {
    return dao.findAllByCdaliassAndTpidtype(cdaliass, tpidtype);
  }

  public List<String> findAllCdotheidByCdaliassAndTpidtype(String cdaliass, String tpidtype) {
    return dao.findAllCdotheidByCdaliassAndTpidtype(cdaliass, tpidtype);
  }
  
  public String findDescrip1ByCdaliassAndTpidtypeAndCdotheid(String cdaliass, String tpidtype, String cdotheid)  {
    return dao.findDescrip1ByCdaliassAndTpidtypeAndCdotheid(cdaliass,tpidtype,cdotheid);
  }

  @Override
  @Transactional
  public Fmeg0aot save(Fmeg0aot entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
