package sibbac.database.megbpdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0sua;
import sibbac.database.megbpdta.model.Fmeg0suaId;

@Repository
public interface Fmeg0suaDao extends JpaRepository<Fmeg0sua, Fmeg0suaId> {

  @Query(value = "select count(a) from Fmeg0sua a where a.id.cdaliass = :cdaliass and a.tpmodeid != 'D' ")
  Integer countByCdaliassAndTpmodeidDistinctD(@Param("cdaliass") String cdaliass);

  @Query(value = "select count(a) from Fmeg0sua a where a.id.cdaliass = :cdaliass and a.id.cdsubcta = :cdsubcta and a.tpmodeid != 'D' ")
  Integer countByCdaliassAndCdsubctaAndTpmodeidDistinctD(@Param("cdaliass") String cdaliass,
      @Param("cdsubcta") String cdsubcta);

  @Query(value = "select a from Fmeg0sua a where a.id.cdsubcta = :cdsubcta and a.tpmodeid != 'D' ")
  List<Fmeg0sua> findAllByCdsubctaAndTpmodeidDistinctD(@Param("cdsubcta") String cdsubcta);

}
