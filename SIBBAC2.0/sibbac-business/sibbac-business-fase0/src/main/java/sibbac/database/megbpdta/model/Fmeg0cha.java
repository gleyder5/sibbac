package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0CHA. Instrucciones de Liquidacion
 * Documentación para FMEG0CHA <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0CHA", schema = "MEGBPDTA")
public class Fmeg0cha extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = 2568534169412929038L;

  /**
   * Table Field CDINSLIQ. identifier MEGARA: identifier. Campo concatenado de
   * Code-SebAccount-BroClient-Alias <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Id
  @Column(nullable = false)
  protected java.lang.String cdinsliq;

  /**
   * Table Field DESCRIPT. description MEGARA: description FIDESSA:
   * CLIENTGROUP_NAME
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descript;
  /**
   * Table Field FHDAFROM. dateFrom MEGARA: dateFrom <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdafrom;
  /**
   * Table Field FHDATETO. dateTo MEGARA: dateTo <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdateto;
  /**
   * Table Field IDCOUNTP. counterpartIdentifier MEGARA: counterpartIdentifier
   * FIDESSA: MNEMONIC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idcountp;
  /**
   * Table Field CDBENEFI. beneficiary MEGARA: beneficiary FIDESSA:
   * BENEFICIARY_MN <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbenefi;
  /**
   * Table Field ACBENEFI. beneficiaryAccount MEGARA: beneficiaryAccount
   * FIDESSA: BENEFICIARY_ACC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String acbenefi;
  /**
   * Table Field IDBENEFI. beneficiaryId MEGARA: beneficiaryId FIDESSA:
   * BENEFICIARY_BIC_ID <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idbenefi;
  /**
   * Table Field CDGLOCUS. globalCustodian MEGARA: globalCustodian FIDESSA:
   * GLOBAL_CUST_MN <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdglocus;
  /**
   * Table Field ACGLOCUS. globalCustodianAccount MEGARA: globalCustodianAccount
   * FIDESSA: GLOBAL_CUST_ACC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String acglocus;
  /**
   * Table Field IDGLOCUS. globalCustodianId MEGARA: globalCustodianId FIDESSA:
   * GLOBAL_CUST_BIC_ID <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idglocus;
  /**
   * Table Field CDLOCCUS. localCustodian MEGARA: localCustodian FIDESSA:
   * LOCAL_CUST_MN <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdloccus;
  /**
   * Table Field ACLOCCUS. localCustodianAccount MEGARA: localCustodianAccount
   * FIDESSA: LOCAL_CUST_ACC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String acloccus;
  /**
   * Table Field IDLOCCUS. localCustodianId MEGARA: localCustodianId FIDESSA:
   * LOCAL_CUST_BIC_ID <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idloccus;
  /**
   * Table Field ISSENDBE. sendBeneficiary MEGARA: sendBeneficiary. (Si su valor
   * es true, cambiar a 1, si es false o blancos, cambiar a 0). <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String issendbe;
  /**
   * Table Field TPSETTLE. settlementType MEGARA: settlementType FIDESSA:
   * ELIGIBLE_CLEARER_SETT_TYPE. Se accede a los 2 ficheros de conversion para
   * transcodificar en AS400 y luego en FIDESSA. Los valores de MEGARA son los
   * mismos que se envian a FIDESSA (Si es Domestic/Cedel, cambiar a DC Si es
   * Domestic/Domestic, cambiar a DD Si es Domestic/Euroclear, cambiar a DE Si
   * es Euroclear/Cedel, cambiar a EC Si es Euroclear/Domestic, cambiar a ED Si
   * es Euroclear/Euroclear, cambiar a EE) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpsettle;
  /**
   * Table Field CDBROCLI. identifier MEGARA: identifier FIDESSA:
   * CLIENTGROUP_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbrocli;
  /**
   * Table Field CDALIASS. Alias MEGARA: Alias FIDESSA: CLIENT_MNEMONIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdaliass;
  /**
   * Table Field CDSUBCTA. identifier MEGARA: identifier FIDESSA:
   * ACCOUNT_MNEMONIC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdsubcta;
  /**
   * Table Field CDMARKET. market MEGARA: market FIDESSA: ELIGIBLE_CLEARER_MKT,
   * MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field CDBICMAR. placeOfSettlement MEGARA: placeOfSettlement FIDESSA:
   * PLACE_OF_SETTLEMENT <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbicmar;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;

  public java.lang.String getCdinsliq() {
    return cdinsliq;
  }

  public java.lang.String getDescript() {
    return descript;
  }

  public java.lang.String getFhdafrom() {
    return fhdafrom;
  }

  public java.lang.String getFhdateto() {
    return fhdateto;
  }

  public java.lang.String getIdcountp() {
    return idcountp;
  }

  public java.lang.String getCdbenefi() {
    return cdbenefi;
  }

  public java.lang.String getAcbenefi() {
    return acbenefi;
  }

  public java.lang.String getIdbenefi() {
    return idbenefi;
  }

  public java.lang.String getCdglocus() {
    return cdglocus;
  }

  public java.lang.String getAcglocus() {
    return acglocus;
  }

  public java.lang.String getIdglocus() {
    return idglocus;
  }

  public java.lang.String getCdloccus() {
    return cdloccus;
  }

  public java.lang.String getAcloccus() {
    return acloccus;
  }

  public java.lang.String getIdloccus() {
    return idloccus;
  }

  public java.lang.String getIssendbe() {
    return issendbe;
  }

  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getCdbicmar() {
    return cdbicmar;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public void setCdinsliq(java.lang.String cdinsliq) {
    this.cdinsliq = cdinsliq;
  }

  public void setDescript(java.lang.String descript) {
    this.descript = descript;
  }

  public void setFhdafrom(java.lang.String fhdafrom) {
    this.fhdafrom = fhdafrom;
  }

  public void setFhdateto(java.lang.String fhdateto) {
    this.fhdateto = fhdateto;
  }

  public void setIdcountp(java.lang.String idcountp) {
    this.idcountp = idcountp;
  }

  public void setCdbenefi(java.lang.String cdbenefi) {
    this.cdbenefi = cdbenefi;
  }

  public void setAcbenefi(java.lang.String acbenefi) {
    this.acbenefi = acbenefi;
  }

  public void setIdbenefi(java.lang.String idbenefi) {
    this.idbenefi = idbenefi;
  }

  public void setCdglocus(java.lang.String cdglocus) {
    this.cdglocus = cdglocus;
  }

  public void setAcglocus(java.lang.String acglocus) {
    this.acglocus = acglocus;
  }

  public void setIdglocus(java.lang.String idglocus) {
    this.idglocus = idglocus;
  }

  public void setCdloccus(java.lang.String cdloccus) {
    this.cdloccus = cdloccus;
  }

  public void setAcloccus(java.lang.String acloccus) {
    this.acloccus = acloccus;
  }

  public void setIdloccus(java.lang.String idloccus) {
    this.idloccus = idloccus;
  }

  public void setIssendbe(java.lang.String issendbe) {
    this.issendbe = issendbe;
  }

  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setCdbicmar(java.lang.String cdbicmar) {
    this.cdbicmar = cdbicmar;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public String getSincronizeId() {
    return String.format("%s", cdinsliq);
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0CHA";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0cha [cdinsliq=%s, descript=%s, fhdafrom=%s, fhdateto=%s, idcountp=%s, cdbenefi=%s, acbenefi=%s, idbenefi=%s, cdglocus=%s, acglocus=%s, idglocus=%s, cdloccus=%s, acloccus=%s, idloccus=%s, issendbe=%s, tpsettle=%s, cdbrocli=%s, cdaliass=%s, cdsubcta=%s, cdmarket=%s, cdbicmar=%s, fhdebaja=%s, tpmodeid=%s]",
            cdinsliq, descript, fhdafrom, fhdateto, idcountp, cdbenefi, acbenefi, idbenefi, cdglocus, acglocus,
            idglocus, cdloccus, acloccus, idloccus, issendbe, tpsettle, cdbrocli, cdaliass, cdsubcta, cdmarket,
            cdbicmar, fhdebaja, tpmodeid);
  }

}