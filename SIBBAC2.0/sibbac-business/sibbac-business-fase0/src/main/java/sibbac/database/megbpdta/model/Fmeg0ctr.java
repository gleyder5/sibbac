package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0CTR. Comisiones-Tramos Documentación para
 * FMEG0CTR <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0CTR", schema = "MEGBPDTA")
public class Fmeg0ctr extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 9193910472435865815L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdtpcomi", column = @Column(name = "cdtpcomi", nullable = false)),
      @AttributeOverride(name = "cdcomisi", column = @Column(name = "cdcomisi", nullable = false)),
      @AttributeOverride(name = "cdtarifa", column = @Column(name = "cdtarifa", nullable = false)),
      @AttributeOverride(name = "cdtramoo", column = @Column(name = "cdtramoo", nullable = false)) })
  protected Fmeg0ctrId id;

  /**
   * Relation 1..1 with table FMEG0COM. Constraint de relación entre FMEG0CTR y
   * FMEG0COM <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "cdtpcomi", referencedColumnName = "cdtpcomi", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "cdcomisi", referencedColumnName = "cdcomisi", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "cdtarifa", referencedColumnName = "cdtarifa", nullable = false, insertable = false, updatable = false) })
  protected Fmeg0com fmeg0com;

  /**
   * Table Field CDCLTRAM. cdcltram MEGATRADE: cdcltram <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcltram;
  /**
   * Table Field IMLIMNIM. imlimnim MEGATRADE: imlimnim <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal imlimnim;
  /**
   * Table Field IMLIMMAX. imlimmax MEGATRADE: imlimmax <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal imlimmax;
  /**
   * Table Field IMFIJONU. imfijo MEGATRADE: imfijo <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal imfijonu;
  /**
   * Table Field CDESTADO. Estado Documentación para CDESTADO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdestado;
  /**
   * Table Field POTARIFA. potarifa MEGATRADE: potarifa <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal potarifa;
  /**
   * Table Field FHESTADO. feestado 1 ó 2 MEGATRADE: feestado 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhestado;

  public Fmeg0ctrId getId() {
    return id;
  }

  public Fmeg0com getFmeg0com() {
    return fmeg0com;
  }

  public java.lang.String getCdcltram() {
    return cdcltram;
  }

  public java.math.BigDecimal getImlimnim() {
    return imlimnim;
  }

  public java.math.BigDecimal getImlimmax() {
    return imlimmax;
  }

  public java.math.BigDecimal getImfijonu() {
    return imfijonu;
  }

  public java.lang.String getCdestado() {
    return cdestado;
  }

  public java.math.BigDecimal getPotarifa() {
    return potarifa;
  }

  public java.lang.String getFhestado() {
    return fhestado;
  }

  public void setId(Fmeg0ctrId id) {
    this.id = id;
  }

  public void setFmeg0com(Fmeg0com fmeg0com) {
    this.fmeg0com = fmeg0com;
  }

  public void setCdcltram(java.lang.String cdcltram) {
    this.cdcltram = cdcltram;
  }

  public void setImlimnim(java.math.BigDecimal imlimnim) {
    this.imlimnim = imlimnim;
  }

  public void setImlimmax(java.math.BigDecimal imlimmax) {
    this.imlimmax = imlimmax;
  }

  public void setImfijonu(java.math.BigDecimal imfijonu) {
    this.imfijonu = imfijonu;
  }

  public void setCdestado(java.lang.String cdestado) {
    this.cdestado = cdestado;
  }

  public void setPotarifa(java.math.BigDecimal potarifa) {
    this.potarifa = potarifa;
  }

  public void setFhestado(java.lang.String fhestado) {
    this.fhestado = fhestado;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s|%s|%s", id.getCdtpcomi(), id.getCdcomisi(), id.getCdtarifa(), id.getCdtramoo());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0CTR";
  }

  @Override
  public String toString() {
    return String.format(
        "Fmeg0ctr [id=%s, cdcltram=%s, imlimnim=%s, imlimmax=%s, imfijonu=%s, cdestado=%s, potarifa=%s, fhestado=%s]",
        id, cdcltram, imlimnim, imlimmax, imfijonu, cdestado, potarifa, fhestado);
  }

}