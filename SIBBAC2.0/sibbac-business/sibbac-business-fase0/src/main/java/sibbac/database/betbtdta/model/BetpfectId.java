package sibbac.database.betbtdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BetpfectId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3447731098368260642L;

  @Column(nullable = false)
  protected java.lang.String ectoms;

  @Column(nullable = false)
  protected java.math.BigDecimal ectcgl;

  @Column(nullable = false)
  protected java.lang.String ectpro;

  public BetpfectId() {

  }


  public BetpfectId(String ectoms, BigDecimal ectcgl, String ectpro) {
    super();
    this.ectoms = ectoms;
    this.ectcgl = ectcgl;
    this.ectpro = ectpro;
  }

  
  

  public java.lang.String getEctoms() {
    return ectoms;
  }


  public java.math.BigDecimal getEctcgl() {
    return ectcgl;
  }


  public java.lang.String getEctpro() {
    return ectpro;
  }


  public void setEctoms(java.lang.String ectoms) {
    this.ectoms = ectoms;
  }


  public void setEctcgl(java.math.BigDecimal ectcgl) {
    this.ectcgl = ectcgl;
  }


  public void setEctpro(java.lang.String ectpro) {
    this.ectpro = ectpro;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((ectcgl == null) ? 0 : ectcgl.hashCode());
    result = prime * result + ((ectoms == null) ? 0 : ectoms.hashCode());
    result = prime * result + ((ectpro == null) ? 0 : ectpro.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BetpfectId other = (BetpfectId) obj;
    if (ectcgl == null) {
      if (other.ectcgl != null)
        return false;
    }
    else if (!ectcgl.equals(other.ectcgl))
      return false;
    if (ectoms == null) {
      if (other.ectoms != null)
        return false;
    }
    else if (!ectoms.equals(other.ectoms))
      return false;
    if (ectpro == null) {
      if (other.ectpro != null)
        return false;
    }
    else if (!ectpro.equals(other.ectpro))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("BetpfectId [ectoms=%s, ectcgl=%s, ectpro=%s]", ectoms, ectcgl, ectpro);
  }

}
