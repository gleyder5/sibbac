package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0asdDao;
import sibbac.database.megbpdta.model.Fmeg0asd;
import sibbac.database.megbpdta.model.Fmeg0asdId;

@Service
public class Fmeg0asdBo extends AbstractFmegBo<Fmeg0asd, Fmeg0asdId, Fmeg0asdDao> {

  @Override
  @Transactional
  public Fmeg0asd save(Fmeg0asd entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
