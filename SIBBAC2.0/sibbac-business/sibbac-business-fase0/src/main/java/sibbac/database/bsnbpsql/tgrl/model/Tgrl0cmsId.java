package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tgrl0cmsId implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = -8822622058103626374L;

  /**
   * Table Field NUOPROUT. Nº Operación Routing Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 9, nullable = false)
  protected Integer nuoprout;

  /**
   * Table Field NUPAREJE. Nº Operación de ejecución Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4, nullable = false)
  protected Short nupareje;

  /**
   * Table Field NUREGCOM. Nº Registro comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3, nullable = false)
  protected Short nuregcom;

  public Tgrl0cmsId() {
  }
  
  
  

  public Tgrl0cmsId(Integer nuoprout, Short nupareje, Short nuregcom) {
    super();
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
    this.nuregcom = nuregcom;
  }




  public Integer getNuoprout() {
    return nuoprout;
  }




  public Short getNupareje() {
    return nupareje;
  }




  public Short getNuregcom() {
    return nuregcom;
  }




  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }




  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }




  public void setNuregcom(Short nuregcom) {
    this.nuregcom = nuregcom;
  }




  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nuoprout == null) ? 0 : nuoprout.hashCode());
    result = prime * result + ((nupareje == null) ? 0 : nupareje.hashCode());
    result = prime * result + ((nuregcom == null) ? 0 : nuregcom.hashCode());
    return result;
  }




  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tgrl0cmsId other = (Tgrl0cmsId) obj;
    if (nuoprout == null) {
      if (other.nuoprout != null)
        return false;
    }
    else if (!nuoprout.equals(other.nuoprout))
      return false;
    if (nupareje == null) {
      if (other.nupareje != null)
        return false;
    }
    else if (!nupareje.equals(other.nupareje))
      return false;
    if (nuregcom == null) {
      if (other.nuregcom != null)
        return false;
    }
    else if (!nuregcom.equals(other.nuregcom))
      return false;
    return true;
  }




  @Override
  public String toString() {
    return String.format("Tgrl0ejeId %d##%d##%d", nuoprout, nupareje, nuregcom);
  }

}
