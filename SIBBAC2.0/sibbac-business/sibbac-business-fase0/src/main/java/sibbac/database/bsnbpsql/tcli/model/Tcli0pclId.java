package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tcli0pclId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3313699788436576564L;
  /**
   * Table Field NUCLIENT. Columna importada NUCLIENT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuclient;
  /**
   * Table Field CDPRODUC. Columna importada CDPRODUC Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdproduc;

  /**
   * Table Field NUCTAPRO. Columna importada NUCTAPRO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuctapro;

  public Tcli0pclId() {

  }

  public Tcli0pclId(String cdproduc, BigDecimal nuctapro, BigDecimal nuclient) {
    super();
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
    this.nuclient = nuclient;
  }

  public java.math.BigDecimal getNuclient() {
    return nuclient;
  }

  public java.lang.String getCdproduc() {
    return cdproduc;
  }

  public java.math.BigDecimal getNuctapro() {
    return nuctapro;
  }

  public void setNuclient(java.math.BigDecimal nuclient) {
    this.nuclient = nuclient;
  }

  public void setCdproduc(java.lang.String cdproduc) {
    this.cdproduc = cdproduc;
  }

  public void setNuctapro(java.math.BigDecimal nuctapro) {
    this.nuctapro = nuctapro;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdproduc == null) ? 0 : cdproduc.hashCode());
    result = prime * result + ((nuclient == null) ? 0 : nuclient.hashCode());
    result = prime * result + ((nuctapro == null) ? 0 : nuctapro.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tcli0pclId other = (Tcli0pclId) obj;
    if (cdproduc == null) {
      if (other.cdproduc != null)
        return false;
    }
    else if (!cdproduc.equals(other.cdproduc))
      return false;
    if (nuclient == null) {
      if (other.nuclient != null)
        return false;
    }
    else if (!nuclient.equals(other.nuclient))
      return false;
    if (nuctapro == null) {
      if (other.nuctapro != null)
        return false;
    }
    else if (!nuctapro.equals(other.nuctapro))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tcli0pclId [nuclient=%s, cdproduc=%s, nuctapro=%s]", nuclient, cdproduc, nuctapro);
  }

}
