package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0EID. Entidades-IdBySes Documentación para
 * FMEG0EID <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0EID", schema = "MEGBPDTA")
public class Fmeg0eid extends Fmeg0entChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -5141279080200254781L;
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdident1", column = @Column(name = "cdident1", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  private Fmeg0eidId id;

  /**
   * Table Field CDIDBYSE. identifier MEGARA: identifier. Siempre tendra el
   * valor 6666 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdidbyse;
  /**
   * Table Field CDMARKET. stockExchange MEGARA: market FIDESSA:
   * ELIGIBLE_CLEARER_MKT, MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field DESCRIP1. description MEGARA: description <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descrip1;
  
  

  public Fmeg0eidId getId() {
    return id;
  }

  public java.lang.String getCdidbyse() {
    return cdidbyse;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getDescrip1() {
    return descrip1;
  }

  public void setId(Fmeg0eidId id) {
    this.id = id;
  }

  public void setCdidbyse(java.lang.String cdidbyse) {
    this.cdidbyse = cdidbyse;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setDescrip1(java.lang.String descrip1) {
    this.descrip1 = descrip1;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s", id.getCdident1(), id.getNusecuen());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0EID";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0eid [id=%s, cdidbyse=%s, cdmarket=%s, descrip1=%s]", id, cdidbyse, cdmarket, descrip1);
  }
  
  

}