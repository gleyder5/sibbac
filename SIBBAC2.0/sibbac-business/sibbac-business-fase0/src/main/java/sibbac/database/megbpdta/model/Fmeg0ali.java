package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * This class represent the table FMEG0ALI. Alias Documentación para FMEG0ALI
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 **/

@Entity
@Table(name = "FMEG0ALI", schema = "MEGBPDTA")
public class Fmeg0ali implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = 6946001868501034161L;

  /**
   * Table Field CDALIASS. Alias MEGARA: Alias FIDESSA: CLIENT_MNEMONIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Id
  @XmlAttribute
  @Column(length = 20, nullable = false)
  protected java.lang.String cdaliass;

  /**
   * Table Field CDBROCLI. broClient MEGARA: identifier FIDESSA:
   * CLIENTGROUP_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 20, nullable = false)
  protected java.lang.String cdbrocli;
  /**
   * Table Field TPALLOCA. allocationType MEGARA: allocationType. Se accede al
   * fichero de conversión (Allocate To SubAccount, en el AS400, una -S-),
   * (Allocate To Referenced Holder, en el AS400, una -H-) (Allocate To
   * Referenced Holder or SubAccount, en el AS400, una -B-)
   * 
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 2, nullable = false)
  protected java.lang.String tpalloca;
  /**
   * Table Field TPEXALLO. expectedAllocation MEGARA: expectedAllocation. Se
   * accede al foichero de conversion (Si el valor es igual a: Detailled
   * Allocation, cambiar a D en el AS400) (Si el valor es igual a: Global
   * Allocation, cambiar a G en el AS400)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 2, nullable = false)
  protected java.lang.String tpexallo;
  /**
   * Table Field TPMAALLO. marketAllocationRule MEGARA: marketAllocationRule. Se
   * accede al foichero de conversión (Si su valor es igual a: Min Allocations,
   * cambiar a M en el AS400) (Si su valor es igual a: Proportional Allocations,
   * cambiar a P en el AS400)
   * 
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 2, nullable = false)
  protected java.lang.String tpmaallo;
  /**
   * Table Field TPCONFIR. confirmationType MEGARA: confirmationType. Se accede
   * al fichero de conversión. (Si su valor en el AS400 es igual a Y, se envia a
   * Fidessa BLOCK) (Si su valor en el AS400 es igual a Z, se envia a Fidessa
   * ALLOC) (Si su valor en el AS400 es igual a F, se envia a Fidessa FAX) (Si
   * su valor en el AS400 es igual a E, se envia a Fidessa EMAIL)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 2, nullable = false)
  protected java.lang.String tpconfir;
  /**
   * Table Field TPCALCUU. calculationType MEGARA: calculationType <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 2, nullable = false)
  protected java.lang.String tpcalcuu;
  /**
   * Table Field TPCALCLM. calculationTypeLM MEGARA: calculationTypeLM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 2, nullable = false)
  protected java.lang.String tpcalclm;
  /**
   * Table Field ISCONCLE. confirmtoClearer MEGARA: confirmtoClearer (Se
   * convertira de true a 1 y de false o blancos a 0)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isconcle;
  /**
   * Table Field DESCRALI. description MEGARA: description FIDESSA: CLIENT_NAME
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 130, nullable = false)
  protected java.lang.String descrali;
  /**
   * Table Field ISOWNACC. forOwnAccount MEGARA: forOwnAccount FIDESSA:
   * FOR_OWN_ACCOUNT (Se convertirá de true a 1 y de false o blancos a 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isownacc;
  /**
   * Table Field ISDEFAUL. isDefault MEGARA: isDefault (Se convertira de true a
   * 1 y de false o blancos a 0) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isdefaul;
  /**
   * Table Field ISPAYMAR. payMarket MEGARA: payMarket FIDESSA: MARKET_TAX (Si
   * su valor es true convertir a 1, si fuese false o blancos cconvertir a 0)
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String ispaymar;
  /**
   * Table Field PORTFOLI. portfolio MEGARA: portfolio FIDESSA: PORTFOLIO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String portfoli;
  /**
   * Table Field ISRECALL. reconcileAllocation MEGARA: reconcileAllocation
   * FIDESSA: CONFIRMATION_LEVEL (Si su contenido es true, convertir a 1, si
   * fuese false o blancos convertir a 0) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isrecall;
  /**
   * Table Field ISRECALM. reconcileAllocationLM MEGARA: reconcileAllocationLM
   * (Si su contenido es true, convertir a 1, si fuese false o blancos,
   * convertir a 0) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isrecalm;
  /**
   * Table Field ISRECCON. reconcileConfirmation MEGARA: reconcileConfirmation
   * FIDESSA: CONFIRMATION_LEVEL (Si su contenido es true, convertir a 1, si
   * fuese false o blancos, convertir a o)c <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isreccon;
  /**
   * Table Field ISRECCLM. reconcileConfirmationLM MEGARA:
   * reconcileConfirmationLM (Si su contenido es true, convertir a 1, si fuese
   * false o blancos, convertir a o) <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isrecclm;
  /**
   * Table Field ISREQALO. requireAllocation MEGARA: requireAllocation (Si su
   * contenido es true, convertir a 1, si fuese false o blancos, convertir a 0)
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isreqalo;
  /**
   * Table Field ISREQALM. requireAllocationLM MEGARA: requireAllocationLM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isreqalm;
  /**
   * Table Field ISREQALC. requireAllocConfirmation MEGARA:
   * requireAllocConfirmation (Si su contenido fuese true, convertir a 1, si
   * fuese false o blancos, convertir a o) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isreqalc;
  /**
   * Table Field ISREQCON. requireConfirmation MEGARA: requireConfirmation (Si
   * su contenido fuese true, convertir a 1, si fuese false o blancos, convertir
   * a 0) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isreqcon;
  /**
   * Table Field ISSTPROC. stp MEGARA: stp (Si su contenido fuese true,
   * convertir a 1, si fuese false o blancos, convertir a 0) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isstproc;
  /**
   * Table Field ISEXONER. exonerated MEGARA: exonerated (Si su contenido fuese
   * true, convertir en 1, si fuese false o true, convertir a 0). <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isexoner;
  /**
   * Table Field ISMANRET. manualRetrocession MEGARA: manualRetrocession (Si su
   * contenido fuese true, convertir a 1, si fuese false o blancos, convertir en
   * 0) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/

  protected java.lang.String ismanret;
  /**
   * Table Field ISROUTIN. routing MEGARA: routing (Si su contenido fuese true,
   * convertir a 1, si fuese false o blancos, convertir a 0) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isroutin;
  /**
   * Table Field ISREQRET. requireRetrocession MEGARA: requireRetrocession (si
   * su contenido fuese true, convertir en 1, si fuese false o blancos,
   * convertir a 0) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String isreqret;
  /**
   * Table Field CDTRADER. trader MEGARA: trader <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String cdtrader;
  /**
   * Table Field CDSALEID. sale MEGARA: sale <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String cdsaleid;
  /**
   * Table Field CDMANAGE. manager MEGARA: manager <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String cdmanage;
  /**
   * Table Field STGROUP. statisticGroup MEGARA: statisticGroup <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 3, nullable = false)
  protected java.lang.String stgroup;
  /**
   * Table Field STLEVEL0. statisticLevel0 MEGARA: statisticLevel0 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlevel0;
  /**
   * Table Field STLEVEL1. statisticLevel1 MEGARA: statisticLevel1 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlevel1;
  /**
   * Table Field STLEVEL2. statisticLevel2 MEGARA: statisticLevel2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlevel2;
  /**
   * Table Field STLEVEL3. statisticLevel3 MEGARA: statisticLevel3 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlevel3;
  /**
   * Table Field STLEVEL4. statisticLevel4 MEGARA: statisticLevel4 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlevel4;
  /**
   * Table Field CDTRADLM. traderLM MEGARA: traderLM <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String cdtradlm;
  /**
   * Table Field CDSALELM. saleLM MEGARA: saleLM <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String cdsalelm;
  /**
   * Table Field CDMANALM. managerLM MEGARA: managerLM <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String cdmanalm;
  /**
   * Table Field STGROULM. statisticGroupLM MEGARA: statisticGroupLM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 3, nullable = false)
  protected java.lang.String stgroulm;
  /**
   * Table Field STLEV0LM. statisticLevel0LM MEGARAL: statisticLevel0LM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlev0lm;
  /**
   * Table Field STLEV1LM. statisticLevel1LM MEGARA: statisticLevel1LM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlev1lm;
  /**
   * Table Field STLEV2LM. statisticLevel2LM MEGARA: statisticLevel2LM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlev2lm;
  /**
   * Table Field STLEV3LM. statisticLevel3LM MEGARA: statisticLevel3LM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlev3lm;
  /**
   * Table Field STLEV4LM. statisticLevel4LM MEGARA: statisticLevel4LM <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 5, nullable = false)
  protected java.lang.String stlev4lm;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 8, nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @XmlAttribute
  @Column(length = 1, nullable = false)
  protected java.lang.String tpmodeid;
  
  

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getTpalloca() {
    return tpalloca;
  }

  public java.lang.String getTpexallo() {
    return tpexallo;
  }

  public java.lang.String getTpmaallo() {
    return tpmaallo;
  }

  public java.lang.String getTpconfir() {
    return tpconfir;
  }

  public java.lang.String getTpcalcuu() {
    return tpcalcuu;
  }

  public java.lang.String getTpcalclm() {
    return tpcalclm;
  }

  public java.lang.String getIsconcle() {
    return isconcle;
  }

  public java.lang.String getDescrali() {
    return descrali;
  }

  public java.lang.String getIsownacc() {
    return isownacc;
  }

  public java.lang.String getIsdefaul() {
    return isdefaul;
  }

  public java.lang.String getIspaymar() {
    return ispaymar;
  }

  public java.lang.String getPortfoli() {
    return portfoli;
  }

  public java.lang.String getIsrecall() {
    return isrecall;
  }

  public java.lang.String getIsrecalm() {
    return isrecalm;
  }

  public java.lang.String getIsreccon() {
    return isreccon;
  }

  public java.lang.String getIsrecclm() {
    return isrecclm;
  }

  public java.lang.String getIsreqalo() {
    return isreqalo;
  }

  public java.lang.String getIsreqalm() {
    return isreqalm;
  }

  public java.lang.String getIsreqalc() {
    return isreqalc;
  }

  public java.lang.String getIsreqcon() {
    return isreqcon;
  }

  public java.lang.String getIsstproc() {
    return isstproc;
  }

  public java.lang.String getIsexoner() {
    return isexoner;
  }

  public java.lang.String getIsmanret() {
    return ismanret;
  }

  public java.lang.String getIsroutin() {
    return isroutin;
  }

  public java.lang.String getIsreqret() {
    return isreqret;
  }

  public java.lang.String getCdtrader() {
    return cdtrader;
  }

  public java.lang.String getCdsaleid() {
    return cdsaleid;
  }

  public java.lang.String getCdmanage() {
    return cdmanage;
  }

  public java.lang.String getStgroup() {
    return stgroup;
  }

  public java.lang.String getStlevel0() {
    return stlevel0;
  }

  public java.lang.String getStlevel1() {
    return stlevel1;
  }

  public java.lang.String getStlevel2() {
    return stlevel2;
  }

  public java.lang.String getStlevel3() {
    return stlevel3;
  }

  public java.lang.String getStlevel4() {
    return stlevel4;
  }

  public java.lang.String getCdtradlm() {
    return cdtradlm;
  }

  public java.lang.String getCdsalelm() {
    return cdsalelm;
  }

  public java.lang.String getCdmanalm() {
    return cdmanalm;
  }

  public java.lang.String getStgroulm() {
    return stgroulm;
  }

  public java.lang.String getStlev0lm() {
    return stlev0lm;
  }

  public java.lang.String getStlev1lm() {
    return stlev1lm;
  }

  public java.lang.String getStlev2lm() {
    return stlev2lm;
  }

  public java.lang.String getStlev3lm() {
    return stlev3lm;
  }

  public java.lang.String getStlev4lm() {
    return stlev4lm;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setTpalloca(java.lang.String tpalloca) {
    this.tpalloca = tpalloca;
  }

  public void setTpexallo(java.lang.String tpexallo) {
    this.tpexallo = tpexallo;
  }

  public void setTpmaallo(java.lang.String tpmaallo) {
    this.tpmaallo = tpmaallo;
  }

  public void setTpconfir(java.lang.String tpconfir) {
    this.tpconfir = tpconfir;
  }

  public void setTpcalcuu(java.lang.String tpcalcuu) {
    this.tpcalcuu = tpcalcuu;
  }

  public void setTpcalclm(java.lang.String tpcalclm) {
    this.tpcalclm = tpcalclm;
  }

  public void setIsconcle(java.lang.String isconcle) {
    this.isconcle = isconcle;
  }

  public void setDescrali(java.lang.String descrali) {
    this.descrali = descrali;
  }

  public void setIsownacc(java.lang.String isownacc) {
    this.isownacc = isownacc;
  }

  public void setIsdefaul(java.lang.String isdefaul) {
    this.isdefaul = isdefaul;
  }

  public void setIspaymar(java.lang.String ispaymar) {
    this.ispaymar = ispaymar;
  }

  public void setPortfoli(java.lang.String portfoli) {
    this.portfoli = portfoli;
  }

  public void setIsrecall(java.lang.String isrecall) {
    this.isrecall = isrecall;
  }

  public void setIsrecalm(java.lang.String isrecalm) {
    this.isrecalm = isrecalm;
  }

  public void setIsreccon(java.lang.String isreccon) {
    this.isreccon = isreccon;
  }

  public void setIsrecclm(java.lang.String isrecclm) {
    this.isrecclm = isrecclm;
  }

  public void setIsreqalo(java.lang.String isreqalo) {
    this.isreqalo = isreqalo;
  }

  public void setIsreqalm(java.lang.String isreqalm) {
    this.isreqalm = isreqalm;
  }

  public void setIsreqalc(java.lang.String isreqalc) {
    this.isreqalc = isreqalc;
  }

  public void setIsreqcon(java.lang.String isreqcon) {
    this.isreqcon = isreqcon;
  }

  public void setIsstproc(java.lang.String isstproc) {
    this.isstproc = isstproc;
  }

  public void setIsexoner(java.lang.String isexoner) {
    this.isexoner = isexoner;
  }

  public void setIsmanret(java.lang.String ismanret) {
    this.ismanret = ismanret;
  }

  public void setIsroutin(java.lang.String isroutin) {
    this.isroutin = isroutin;
  }

  public void setIsreqret(java.lang.String isreqret) {
    this.isreqret = isreqret;
  }

  public void setCdtrader(java.lang.String cdtrader) {
    this.cdtrader = cdtrader;
  }

  public void setCdsaleid(java.lang.String cdsaleid) {
    this.cdsaleid = cdsaleid;
  }

  public void setCdmanage(java.lang.String cdmanage) {
    this.cdmanage = cdmanage;
  }

  public void setStgroup(java.lang.String stgroup) {
    this.stgroup = stgroup;
  }

  public void setStlevel0(java.lang.String stlevel0) {
    this.stlevel0 = stlevel0;
  }

  public void setStlevel1(java.lang.String stlevel1) {
    this.stlevel1 = stlevel1;
  }

  public void setStlevel2(java.lang.String stlevel2) {
    this.stlevel2 = stlevel2;
  }

  public void setStlevel3(java.lang.String stlevel3) {
    this.stlevel3 = stlevel3;
  }

  public void setStlevel4(java.lang.String stlevel4) {
    this.stlevel4 = stlevel4;
  }

  public void setCdtradlm(java.lang.String cdtradlm) {
    this.cdtradlm = cdtradlm;
  }

  public void setCdsalelm(java.lang.String cdsalelm) {
    this.cdsalelm = cdsalelm;
  }

  public void setCdmanalm(java.lang.String cdmanalm) {
    this.cdmanalm = cdmanalm;
  }

  public void setStgroulm(java.lang.String stgroulm) {
    this.stgroulm = stgroulm;
  }

  public void setStlev0lm(java.lang.String stlev0lm) {
    this.stlev0lm = stlev0lm;
  }

  public void setStlev1lm(java.lang.String stlev1lm) {
    this.stlev1lm = stlev1lm;
  }

  public void setStlev2lm(java.lang.String stlev2lm) {
    this.stlev2lm = stlev2lm;
  }

  public void setStlev3lm(java.lang.String stlev3lm) {
    this.stlev3lm = stlev3lm;
  }

  public void setStlev4lm(java.lang.String stlev4lm) {
    this.stlev4lm = stlev4lm;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public String getSincronizeId() {
    final String sincronizeId = cdaliass.trim();
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0ALI";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0ali [cdaliass=%s, cdbrocli=%s, tpalloca=%s, tpexallo=%s, tpmaallo=%s, tpconfir=%s, tpcalcuu=%s, tpcalclm=%s, isconcle=%s, descrali=%s, isownacc=%s, isdefaul=%s, ispaymar=%s, portfoli=%s, isrecall=%s, isrecalm=%s, isreccon=%s, isrecclm=%s, isreqalo=%s, isreqalm=%s, isreqalc=%s, isreqcon=%s, isstproc=%s, isexoner=%s, ismanret=%s, isroutin=%s, isreqret=%s, cdtrader=%s, cdsaleid=%s, cdmanage=%s, stgroup=%s, stlevel0=%s, stlevel1=%s, stlevel2=%s, stlevel3=%s, stlevel4=%s, cdtradlm=%s, cdsalelm=%s, cdmanalm=%s, stgroulm=%s, stlev0lm=%s, stlev1lm=%s, stlev2lm=%s, stlev3lm=%s, stlev4lm=%s, fhdebaja=%s, tpmodeid=%s]",
            cdaliass, cdbrocli, tpalloca, tpexallo, tpmaallo, tpconfir, tpcalcuu, tpcalclm, isconcle, descrali,
            isownacc, isdefaul, ispaymar, portfoli, isrecall, isrecalm, isreccon, isrecclm, isreqalo, isreqalm,
            isreqalc, isreqcon, isstproc, isexoner, ismanret, isroutin, isreqret, cdtrader, cdsaleid, cdmanage,
            stgroup, stlevel0, stlevel1, stlevel2, stlevel3, stlevel4, cdtradlm, cdsalelm, cdmanalm, stgroulm,
            stlev0lm, stlev1lm, stlev2lm, stlev3lm, stlev4lm, fhdebaja, tpmodeid);
  }
  
  
}