package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0sec;
import sibbac.database.megbpdta.model.Fmeg0secId;

@Repository
public interface Fmeg0secDao extends JpaRepository<Fmeg0sec, Fmeg0secId> {

  @Query(value = "select max(s.id.nusecuen) from Fmeg0sec s where s.id.cdsubcta = :cdsubcta")
  BigDecimal maxIdNusecuenByIdCdsubcta(@Param("cdsubcta") String cdsubcta);
  
  @Query(value = "select s from Fmeg0sec s where s.id.cdsubcta = :cdsubcta")
  List<Fmeg0sec> findAllByIdCdsubcta(@Param("cdsubcta") String cdsubcta);

  @Query("select a from Fmeg0sec a where a.id.cdsubcta = :cdsubcta and a.cdcustod = :cdcustod "
      + " and a.tpsettle = :tpsettle and a.idenvfid = :idenvfid and a.fmeg0suc.tpmodeid != 'D' ")
  List<Fmeg0sec> findAllByIdCdsubctaAndIdCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(
      @Param("cdsubcta") String cdsubcta, @Param("cdcustod") String cdcustod, @Param("tpsettle") String tpsettle,
      @Param("idenvfid") String idenvfid);

}
