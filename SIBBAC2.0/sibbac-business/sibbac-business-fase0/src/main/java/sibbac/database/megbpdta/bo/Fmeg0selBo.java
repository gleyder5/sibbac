package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0selDao;
import sibbac.database.megbpdta.model.Fmeg0sel;
import sibbac.database.megbpdta.model.Fmeg0selId;

@Service
public class Fmeg0selBo extends AbstractFmegBo<Fmeg0sel, Fmeg0selId, Fmeg0selDao> {

  public List<Fmeg0sel> findAllByIdCdsubcta(String cdsubcta) {
    return dao.findAllByIdCdsubcta(cdsubcta);
  }

  public List<Fmeg0sel> findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(String cdcleare, String tpsettle,
      String idenvfid) {
    return dao.findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(cdcleare, tpsettle, idenvfid);
  }

  @Override
  @Transactional
  public Fmeg0sel save(Fmeg0sel entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdsubcta(entity.getId().getCdsubcta());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
