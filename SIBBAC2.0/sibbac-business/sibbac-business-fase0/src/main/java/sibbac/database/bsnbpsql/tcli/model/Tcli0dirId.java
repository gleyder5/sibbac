package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Tcli0dirId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -7342601239033296370L;

  /**
   * Table Field CDPRODUC. Columna importada CDPRODUC Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdproduc;

  /**
   * Table Field NUCTAPRO. Columna importada NUCTAPRO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuctapro;
  /**
   * Table Field NUDIRECC. Columna importada NUDIRECC Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nudirecc;

  public Tcli0dirId() {

  }

  public Tcli0dirId(String cdproduc, BigDecimal nuctapro, BigDecimal nudirecc) {
    super();
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
    this.nudirecc = nudirecc;
  }

  public java.lang.String getCdproduc() {
    return cdproduc;
  }

  public java.math.BigDecimal getNuctapro() {
    return nuctapro;
  }

  public java.math.BigDecimal getNudirecc() {
    return nudirecc;
  }

  public void setCdproduc(java.lang.String cdproduc) {
    this.cdproduc = cdproduc;
  }

  public void setNuctapro(java.math.BigDecimal nuctapro) {
    this.nuctapro = nuctapro;
  }

  public void setNudirecc(java.math.BigDecimal nudirecc) {
    this.nudirecc = nudirecc;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdproduc == null) ? 0 : cdproduc.hashCode());
    result = prime * result + ((nuctapro == null) ? 0 : nuctapro.hashCode());
    result = prime * result + ((nudirecc == null) ? 0 : nudirecc.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tcli0dirId other = (Tcli0dirId) obj;
    if (cdproduc == null) {
      if (other.cdproduc != null)
        return false;
    }
    else if (!cdproduc.equals(other.cdproduc))
      return false;
    if (nuctapro == null) {
      if (other.nuctapro != null)
        return false;
    }
    else if (!nuctapro.equals(other.nuctapro))
      return false;
    if (nudirecc == null) {
      if (other.nudirecc != null)
        return false;
    }
    else if (!nudirecc.equals(other.nudirecc))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tcli0dirId [cdproduc=%s, nuctapro=%s, nudirecc=%s]", cdproduc, nuctapro, nudirecc);
  }
  
  

}
