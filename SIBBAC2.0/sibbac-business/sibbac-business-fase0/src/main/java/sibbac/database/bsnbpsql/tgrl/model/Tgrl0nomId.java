package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tgrl0nomId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -8822622058103626374L;

  /**
   * Table Field NUOPROUT. Nº Operación Routing Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 9, nullable = false)
  protected Integer nuoprout;

  /**
   * Table Field NUPAREJE. Nº Operación de ejecución Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4, nullable = false)
  protected Short nupareje;

  /**
   * Table Field nusecnbr. Nº Registro direccion Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3, nullable = false)
  protected Short nusecnbr;

  /**
   * Table Field nusecnbr. Nº Registro titular Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3, nullable = false)
  protected Short nusectit;

  public Tgrl0nomId() {
  }

  public Tgrl0nomId(Integer nuoprout, Short nupareje, Short nusecnbr, Short nusectit) {
    super();
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
    this.nusecnbr = nusecnbr;
    this.nusectit = nusectit;
  }

  public Integer getNuoprout() {
    return nuoprout;
  }

  public Short getNupareje() {
    return nupareje;
  }

  public Short getNusecnbr() {
    return nusecnbr;
  }
  
  

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  public void setNusecnbr(Short nusecnbr) {
    this.nusecnbr = nusecnbr;
  }
  
  
  

  public Short getNusectit() {
    return nusectit;
  }

  public void setNusectit(Short nusectit) {
    this.nusectit = nusectit;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nuoprout == null) ? 0 : nuoprout.hashCode());
    result = prime * result + ((nupareje == null) ? 0 : nupareje.hashCode());
    result = prime * result + ((nusecnbr == null) ? 0 : nusecnbr.hashCode());
    result = prime * result + ((nusectit == null) ? 0 : nusectit.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tgrl0nomId other = (Tgrl0nomId) obj;
    if (nuoprout == null) {
      if (other.nuoprout != null)
        return false;
    }
    else if (!nuoprout.equals(other.nuoprout))
      return false;
    if (nupareje == null) {
      if (other.nupareje != null)
        return false;
    }
    else if (!nupareje.equals(other.nupareje))
      return false;
    if (nusecnbr == null) {
      if (other.nusecnbr != null)
        return false;
    }
    else if (!nusecnbr.equals(other.nusecnbr))
      return false;

    if (nusectit == null) {
      if (other.nusectit != null)
        return false;
    }
    else if (!nusectit.equals(other.nusectit))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tgrl0dirId %d##%d##%d##%d", nuoprout, nupareje, nusecnbr, nusectit);
  }

}
