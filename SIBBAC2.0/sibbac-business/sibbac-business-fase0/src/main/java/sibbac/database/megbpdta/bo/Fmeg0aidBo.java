package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0aidDao;
import sibbac.database.megbpdta.model.Fmeg0aid;
import sibbac.database.megbpdta.model.Fmeg0aidId;

@Service
public class Fmeg0aidBo extends AbstractFmegBo<Fmeg0aid, Fmeg0aidId, Fmeg0aidDao> {

  @Override
  @Transactional
  public Fmeg0aid save(Fmeg0aid entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
