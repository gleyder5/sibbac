package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0CFI. Conversion AS400-FIDESSA
 * Documentación para FMEG0CFI <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0CFI", schema = "MEGBPDTA")
public class Fmeg0cfi implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -8732509065961334426L;
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "idcamas4", column = @Column(name = "idcamas4", nullable = false)),
      @AttributeOverride(name = "dsaas400", column = @Column(name = "dsaas400", nullable = false)) })
  protected Fmeg0cfiId id;
  /**
   * Table Field DSFIDESSA. Información enviada a FIDESSA Documentación para
   * DSFIDESSA <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String dsfidessa;

  public Fmeg0cfiId getId() {
    return id;
  }

  public java.lang.String getDsfidessa() {
    return dsfidessa;
  }

  public void setId(Fmeg0cfiId id) {
    this.id = id;
  }

  public void setDsfidessa(java.lang.String dsfidessa) {
    this.dsfidessa = dsfidessa;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getIdcamas4(), id.getDsaas400());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0CFI";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0cfi [id=%s, dsfidessa=%s]", id, dsfidessa);
  }

}