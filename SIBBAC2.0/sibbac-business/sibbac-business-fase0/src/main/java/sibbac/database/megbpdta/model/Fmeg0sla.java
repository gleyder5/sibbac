package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0SLA. SLA Client Reporting Documentación
 * para FMEG0SLA <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0SLA", schema = "MEGBPDTA")
public class Fmeg0sla implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -7210647940000874807L;

  /**
   * Table Field NUCONTAD. Contador registro Documentación para NUCONTAD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Id
  @Column(nullable = false)
  protected java.math.BigDecimal nucontad;

  /**
   * Table Field CODSLACL. SLA Documentación para CODSLACL <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String codslacl;
  /**
   * Table Field DESCRIP6. Descripcion Documentación para DESCRIP6 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descrip6;
  /**
   * Table Field FHFROMDA. Fecha desde Documentación para FHFROMDA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhfromda;
  /**
   * Table Field FHTODATE. Fecha hasta Documentación para FHTODATE <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhtodate;
  /**
   * Table Field CDALIASS. Alias MEGARA: Alias FIDESSA: CLIENT_MNEMONIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdaliass;
  /**
   * Table Field CDBROCLI. broClient MEGARA: identifier FIDESSA:
   * CLIENTGROUP_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbrocli;
  /**
   * Table Field CDELEGI1. Subaccount/Broclient/Alias Documentación para
   * CDELEGI1 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdelegi1;
  /**
   * Table Field CDELEGI2. Subaccount/Broclient/Alias Documentación para
   * CDELEGI2 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdelegi2;
  /**
   * Table Field TPCLIENN. clientNature MEGARA: clientNature. Se accede al
   * fichero de conversion FIDESSA: CLIENT_TYPE (Si en Megara viene instit,
   * cambiar a I en AS400 y cambiar a C para FOIDESSA) (Si en Megara viene
   * retail, camboiar a R en AS400 y cambiar a T para FIDESSA) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpclienn;
  /**
   * Table Field PROCESSS. Proceso Documentación para PROCESSS. (Si su valor es
   * Send Client Allocation, se cambia por CA en el AS400) (Si su valor es Send
   * Client Confirmation, se cambia por CC en el AS400) (Si su valor es Send
   * Client Retro Invoice, se cambia por CR en el AS400) (Si su valor es Send
   * Periodic Statement, se cambia por PS en el AS400) <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String processs;
  /**
   * Table Field LCFOREIG. Descripcion Documentación para LCFOREIG (Si su valor
   * es foreign, se cambia por FM en el AS400) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String lcforeig;
  /**
   * Table Field CDFONAME. Descripcion Documentación para CDFONAME <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdfoname;
  /**
   * Table Field DSFONAME. Descripcion Documentación para DSFONAME <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String dsfoname;
  /**
   * Table Field PURPOSEE. purpose MEGARA: purpose <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String purposee;
  /**
   * Table Field MODSENDI. Descripcion Documentación para MODSENDI (Si su valor
   * es Batch, se cambia por B en el AS400) (Si su valor es End of day, se
   * cambia por E en el AS400) (Si su valor es Grouped At Batch, se cambia por
   * GB en el AS400) (Si su valor es Groupe At End of Day, se cambia por GE en
   * el AS400) (Si su valor es Real Time, se cambia por RT en el AS400)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String modsendi;
  /**
   * Table Field TPDESPER. deactivationPeriodicity MEGARA:
   * deactivationPeriodicity. Se accede al fcihero de conversion. (Si su valor
   * es Monthly, cambiar a M en el AS400) (Si es Quarterly, cambiar a Q en el
   * AS400) (Si es Twice a Year, cambiar a TW en el AS400) (Si es Yearly,
   * cambiar a Y en el AS400) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdesper;
  /**
   * Table Field NUOFCOPY. Descripcion Documentación para NUOFCOPY <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuofcopy;
  /**
   * Table Field ISBEGROU. Descripcion Documentación para ISBEGROU <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isbegrou;
  /**
   * Table Field ISNOTSEN. Descripcion Documentación para ISNOTSEN <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isnotsen;
  /**
   * Table Field CDIDIOMA. Idioma Documentación para CDIDIOMA <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdidioma;
  /**
   * Table Field CDESTADO. Estado Documentación para CDESTADO <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdestado;
  
  

  public java.math.BigDecimal getNucontad() {
    return nucontad;
  }

  public java.lang.String getCodslacl() {
    return codslacl;
  }

  public java.lang.String getDescrip6() {
    return descrip6;
  }

  public java.lang.String getFhfromda() {
    return fhfromda;
  }

  public java.lang.String getFhtodate() {
    return fhtodate;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getCdelegi1() {
    return cdelegi1;
  }

  public java.lang.String getCdelegi2() {
    return cdelegi2;
  }

  public java.lang.String getTpclienn() {
    return tpclienn;
  }

  public java.lang.String getProcesss() {
    return processs;
  }

  public java.lang.String getLcforeig() {
    return lcforeig;
  }

  public java.lang.String getCdfoname() {
    return cdfoname;
  }

  public java.lang.String getDsfoname() {
    return dsfoname;
  }

  public java.lang.String getPurposee() {
    return purposee;
  }

  public java.lang.String getModsendi() {
    return modsendi;
  }

  public java.lang.String getTpdesper() {
    return tpdesper;
  }

  public java.math.BigDecimal getNuofcopy() {
    return nuofcopy;
  }

  public java.lang.String getIsbegrou() {
    return isbegrou;
  }

  public java.lang.String getIsnotsen() {
    return isnotsen;
  }

  public java.lang.String getCdidioma() {
    return cdidioma;
  }

  public java.lang.String getCdestado() {
    return cdestado;
  }

  public void setNucontad(java.math.BigDecimal nucontad) {
    this.nucontad = nucontad;
  }

  public void setCodslacl(java.lang.String codslacl) {
    this.codslacl = codslacl;
  }

  public void setDescrip6(java.lang.String descrip6) {
    this.descrip6 = descrip6;
  }

  public void setFhfromda(java.lang.String fhfromda) {
    this.fhfromda = fhfromda;
  }

  public void setFhtodate(java.lang.String fhtodate) {
    this.fhtodate = fhtodate;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setCdelegi1(java.lang.String cdelegi1) {
    this.cdelegi1 = cdelegi1;
  }

  public void setCdelegi2(java.lang.String cdelegi2) {
    this.cdelegi2 = cdelegi2;
  }

  public void setTpclienn(java.lang.String tpclienn) {
    this.tpclienn = tpclienn;
  }

  public void setProcesss(java.lang.String processs) {
    this.processs = processs;
  }

  public void setLcforeig(java.lang.String lcforeig) {
    this.lcforeig = lcforeig;
  }

  public void setCdfoname(java.lang.String cdfoname) {
    this.cdfoname = cdfoname;
  }

  public void setDsfoname(java.lang.String dsfoname) {
    this.dsfoname = dsfoname;
  }

  public void setPurposee(java.lang.String purposee) {
    this.purposee = purposee;
  }

  public void setModsendi(java.lang.String modsendi) {
    this.modsendi = modsendi;
  }

  public void setTpdesper(java.lang.String tpdesper) {
    this.tpdesper = tpdesper;
  }

  public void setNuofcopy(java.math.BigDecimal nuofcopy) {
    this.nuofcopy = nuofcopy;
  }

  public void setIsbegrou(java.lang.String isbegrou) {
    this.isbegrou = isbegrou;
  }

  public void setIsnotsen(java.lang.String isnotsen) {
    this.isnotsen = isnotsen;
  }

  public void setCdidioma(java.lang.String cdidioma) {
    this.cdidioma = cdidioma;
  }

  public void setCdestado(java.lang.String cdestado) {
    this.cdestado = cdestado;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s", nucontad);
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0SHL";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0sla [nucontad=%s, codslacl=%s, descrip6=%s, fhfromda=%s, fhtodate=%s, cdaliass=%s, cdbrocli=%s, cdelegi1=%s, cdelegi2=%s, tpclienn=%s, processs=%s, lcforeig=%s, cdfoname=%s, dsfoname=%s, purposee=%s, modsendi=%s, tpdesper=%s, nuofcopy=%s, isbegrou=%s, isnotsen=%s, cdidioma=%s, cdestado=%s]",
            nucontad, codslacl, descrip6, fhfromda, fhtodate, cdaliass, cdbrocli, cdelegi1, cdelegi2, tpclienn,
            processs, lcforeig, cdfoname, dsfoname, purposee, modsendi, tpdesper, nuofcopy, isbegrou, isnotsen,
            cdidioma, cdestado);
  }
  
  
}