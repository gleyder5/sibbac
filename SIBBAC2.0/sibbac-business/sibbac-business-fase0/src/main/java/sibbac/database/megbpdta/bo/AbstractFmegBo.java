package sibbac.database.megbpdta.bo;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.wrappers.database.model.Tmct0dbs;
import sibbac.database.bo.AbstractBo;
import sibbac.database.megbpdta.model.FmegEntityInterface;

public class AbstractFmegBo<TYPE extends FmegEntityInterface, PK extends Serializable, DAO extends JpaRepository<TYPE, PK>>
    extends AbstractBo<TYPE, PK, DAO> {

  protected static final Logger LOG = LoggerFactory.getLogger(AbstractFmegBo.class);

  @Autowired
  private Tmct0dbsBo dbsBo;

  @Transactional
  public TYPE save(TYPE entity, boolean isNew) {
    LOG.trace("[save] inicio {}", entity.toString());
    entity = dao.save(entity);
    LOG.trace("[save] insert dbs for {}", entity.toString());
    Tmct0dbs dbs = dbsBo.insertRegisterFmeg(entity, isNew);
    LOG.trace("[save] inserted dbs {}", dbs.toString());
    LOG.trace("[save] fin {}", entity.toString());
    return entity;
  }

}
