package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0bhlDao;
import sibbac.database.megbpdta.model.Fmeg0bhl;
import sibbac.database.megbpdta.model.Fmeg0bhlId;

@Service
public class Fmeg0bhlBo extends AbstractFmegBo<Fmeg0bhl, Fmeg0bhlId, Fmeg0bhlDao> {

  @Override
  @Transactional
  public Fmeg0bhl save(Fmeg0bhl entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdbrocli(entity.getId().getCdbrocli());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
