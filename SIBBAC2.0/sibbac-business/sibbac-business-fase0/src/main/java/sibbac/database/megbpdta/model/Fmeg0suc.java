package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0SUC. Subcuentas Documentación para
 * FMEG0SUC <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0SUC", schema = "MEGBPDTA")
public class Fmeg0suc implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -8641564333212114541L;

  /**
   * Table Field CDSUBCTA. identifier MEGARA: identifier FIDESSA:
   * ACCOUNT_MNEMONIC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Id
  @Column(nullable = false)
  protected java.lang.String cdsubcta;

  /**
   * Table Field NBNAMEID. name MAGARA: name FODESSA: COUNTERPARTY_NAME,
   * ENTITY_NAME, MARKET_NAME, LOCAL_CUST_NAME, GLOBAL_CUST_NAME,
   * BENEFICIARY_NAME, ACCOUNT_NAME
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbnameid;
  /**
   * Table Field CDBROCLI. identifier MEGARA: identifier FIDESSA:
   * CLIENTGROUP_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbrocli;
  /**
   * Table Field CDOTIDEN. otherIdentifier MEGARA: otherIdentifier <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdotiden;
  /**
   * Table Field DESCRIPT. description MEGARA: description FIDESSA:
   * CLIENTGROUP_NAME
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descript;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0suc")
  private List<Fmeg0sad> fmeg0sads = new ArrayList<Fmeg0sad>();
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0suc")
  private List<Fmeg0sec> fmeg0secs = new ArrayList<Fmeg0sec>();
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0suc")
  private List<Fmeg0sel> fmeg0sels = new ArrayList<Fmeg0sel>();
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0suc")
  private List<Fmeg0shl> fmeg0shls = new ArrayList<Fmeg0shl>();
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0suc")
  private List<Fmeg0sot> fmeg0sots = new ArrayList<Fmeg0sot>();

  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  public java.lang.String getNbnameid() {
    return nbnameid;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getCdotiden() {
    return cdotiden;
  }

  public java.lang.String getDescript() {
    return descript;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public List<Fmeg0sad> getFmeg0sads() {
    return fmeg0sads;
  }

  public List<Fmeg0sec> getFmeg0secs() {
    return fmeg0secs;
  }

  public List<Fmeg0sel> getFmeg0sels() {
    return fmeg0sels;
  }

  public List<Fmeg0shl> getFmeg0shls() {
    return fmeg0shls;
  }

  public List<Fmeg0sot> getFmeg0sots() {
    return fmeg0sots;
  }

  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  public void setNbnameid(java.lang.String nbnameid) {
    this.nbnameid = nbnameid;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setCdotiden(java.lang.String cdotiden) {
    this.cdotiden = cdotiden;
  }

  public void setDescript(java.lang.String descript) {
    this.descript = descript;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public void setFmeg0sads(List<Fmeg0sad> fmeg0sads) {
    this.fmeg0sads = fmeg0sads;
  }

  public void setFmeg0secs(List<Fmeg0sec> fmeg0secs) {
    fmeg0secs = fmeg0secs;
  }

  public void setFmeg0sels(List<Fmeg0sel> fmeg0sels) {
    fmeg0sels = fmeg0sels;
  }

  public void setFmeg0shls(List<Fmeg0shl> fmeg0shls) {
    fmeg0shls = fmeg0shls;
  }

  public void setFmeg0sots(List<Fmeg0sot> fmeg0sots) {
    fmeg0sots = fmeg0sots;
  }

  public String getSincronizeId() {
    final String sincronizeId = cdsubcta.trim();
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0SUC";
  }

  @Override
  public String toString() {
    return String.format(
        "Fmeg0suc [cdsubcta=%s, nbnameid=%s, cdbrocli=%s, cdotiden=%s, descript=%s, fhdebaja=%s, tpmodeid=%s]",
        cdsubcta, nbnameid, cdbrocli, cdotiden, descript, fhdebaja, tpmodeid);
  }
  
  
}