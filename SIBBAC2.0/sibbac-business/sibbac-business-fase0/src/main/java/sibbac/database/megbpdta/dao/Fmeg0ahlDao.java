package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0ahl;
import sibbac.database.megbpdta.model.Fmeg0ahlId;

@Repository
public interface Fmeg0ahlDao extends JpaRepository<Fmeg0ahl, Fmeg0ahlId> {

  @Query(value = "select max(a.id.nusecuen) from Fmeg0ahl a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

}
