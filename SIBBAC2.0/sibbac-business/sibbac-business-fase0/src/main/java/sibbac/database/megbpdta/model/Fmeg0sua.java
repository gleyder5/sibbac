package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0SUA. Subcuentas-Alias Documentación para
 * FMEG0SUA <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0SUA", schema = "MEGBPDTA")
public class Fmeg0sua extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -6279060719807919058L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "cdsubcta", column = @Column(name = "cdsubcta", nullable = false)) })
  private Fmeg0suaId id;

  /**
   * Table Field ISDEFAUL. isDefault MEGARA: isDefault (Se convertira de true a
   * 1 y de false o blancos a 0) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isdefaul;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;
  
  

  public Fmeg0suaId getId() {
    return id;
  }

  public java.lang.String getIsdefaul() {
    return isdefaul;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public void setId(Fmeg0suaId id) {
    this.id = id;
  }

  public void setIsdefaul(java.lang.String isdefaul) {
    this.isdefaul = isdefaul;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s", id.getCdaliass(), id.getCdsubcta());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0SUA";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0sua [id=%s, isdefaul=%s, fhdebaja=%s, tpmodeid=%s]", id, isdefaul, fhdebaja, tpmodeid);
  }
  
  
}