package sibbac.database.megbpdta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0cfi;
import sibbac.database.megbpdta.model.Fmeg0cfiId;

@Repository
public interface Fmeg0cfiDao extends JpaRepository<Fmeg0cfi, Fmeg0cfiId> {

}
