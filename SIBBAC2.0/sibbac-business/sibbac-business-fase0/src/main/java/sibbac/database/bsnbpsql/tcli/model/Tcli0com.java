package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table TCLI0COM. COMPLEMENTOS/CUENTA Documentación de
 * la tablaTCLI0COM <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TCLI0COM")
public class Tcli0com implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4655532929372288220L;

  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "cdproduc", column = @Column(name = "CDPRODUC", nullable = false, length = 3)),
      @AttributeOverride(name = "nuctapro", column = @Column(name = "NUCTAPRO", nullable = false, length = 5, scale = 0)) })
  private Tcli0comId id;

  @Column(length = 15, scale = 2)
  private BigDecimal imtramo1;
  @Column(length = 15, scale = 2)
  private BigDecimal imtramo2;
  @Column(length = 15, scale = 2)
  private BigDecimal imtramo3;
  @Column(length = 6, scale = 3)
  private BigDecimal pccomis1;
  @Column(length = 6, scale = 3)
  private BigDecimal pccomis2;
  @Column(length = 6, scale = 3)
  private BigDecimal pccomis3;
  @Column(length = 6, scale = 3)
  private BigDecimal pccomis4;
  @Column(length = 15, scale = 2)
  private BigDecimal imriesco;
  @Column(length = 15, scale = 2)
  private BigDecimal imriesut;
  @Column
  private char tpconfir;
  @Column
  private char cdderliq;
  @Column
  private char cdcomisn;
  @Column
  private char cdproter;
  @Column(length = 8, scale = 0)
  private BigDecimal fhmodifi;
  @Column(length = 8)
  private String cdusrmod;
  @Column(length = 6, scale = 3)
  private BigDecimal pcsvbsis;
  @Column(length = 6, scale = 3)
  private BigDecimal pccomsis;
  @Column(length = 6, scale = 3)
  private BigDecimal pcbansis;
  @Column(length = 7, scale = 2)
  private BigDecimal immincom;
  @Column(length = 6)
  private String cdglobal;
  @Column(length = 3)
  private String tpriesgo;
  @Column
  private char cdtipcal;
  @Column
  private char cdcammed;
  @Column
  private char cdivapar;
  @Column
  private char cdtaspar;
  @Column
  private char idejeliq;

  public Tcli0com() {
  }

  public Tcli0comId getId() {
    return id;
  }

  public BigDecimal getImtramo1() {
    return imtramo1;
  }

  public BigDecimal getImtramo2() {
    return imtramo2;
  }

  public BigDecimal getImtramo3() {
    return imtramo3;
  }

  public BigDecimal getPccomis1() {
    return pccomis1;
  }

  public BigDecimal getPccomis2() {
    return pccomis2;
  }

  public BigDecimal getPccomis3() {
    return pccomis3;
  }

  public BigDecimal getPccomis4() {
    return pccomis4;
  }

  public BigDecimal getImriesco() {
    return imriesco;
  }

  public BigDecimal getImriesut() {
    return imriesut;
  }

  public char getTpconfir() {
    return tpconfir;
  }

  public char getCdderliq() {
    return cdderliq;
  }

  public char getCdcomisn() {
    return cdcomisn;
  }

  public char getCdproter() {
    return cdproter;
  }

  public BigDecimal getFhmodifi() {
    return fhmodifi;
  }

  public String getCdusrmod() {
    return cdusrmod;
  }

  public BigDecimal getPcsvbsis() {
    return pcsvbsis;
  }

  public BigDecimal getPccomsis() {
    return pccomsis;
  }

  public BigDecimal getPcbansis() {
    return pcbansis;
  }

  public BigDecimal getImmincom() {
    return immincom;
  }

  public String getCdglobal() {
    return cdglobal;
  }

  public String getTpriesgo() {
    return tpriesgo;
  }

  public char getCdtipcal() {
    return cdtipcal;
  }

  public char getCdcammed() {
    return cdcammed;
  }

  public char getCdivapar() {
    return cdivapar;
  }

  public char getCdtaspar() {
    return cdtaspar;
  }

  public char getIdejeliq() {
    return idejeliq;
  }

  public void setId(Tcli0comId id) {
    this.id = id;
  }

  public void setImtramo1(BigDecimal imtramo1) {
    this.imtramo1 = imtramo1;
  }

  public void setImtramo2(BigDecimal imtramo2) {
    this.imtramo2 = imtramo2;
  }

  public void setImtramo3(BigDecimal imtramo3) {
    this.imtramo3 = imtramo3;
  }

  public void setPccomis1(BigDecimal pccomis1) {
    this.pccomis1 = pccomis1;
  }

  public void setPccomis2(BigDecimal pccomis2) {
    this.pccomis2 = pccomis2;
  }

  public void setPccomis3(BigDecimal pccomis3) {
    this.pccomis3 = pccomis3;
  }

  public void setPccomis4(BigDecimal pccomis4) {
    this.pccomis4 = pccomis4;
  }

  public void setImriesco(BigDecimal imriesco) {
    this.imriesco = imriesco;
  }

  public void setImriesut(BigDecimal imriesut) {
    this.imriesut = imriesut;
  }

  public void setTpconfir(char tpconfir) {
    this.tpconfir = tpconfir;
  }

  public void setCdderliq(char cdderliq) {
    this.cdderliq = cdderliq;
  }

  public void setCdcomisn(char cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  public void setCdproter(char cdproter) {
    this.cdproter = cdproter;
  }

  public void setFhmodifi(BigDecimal fhmodifi) {
    this.fhmodifi = fhmodifi;
  }

  public void setCdusrmod(String cdusrmod) {
    this.cdusrmod = cdusrmod;
  }

  public void setPcsvbsis(BigDecimal pcsvbsis) {
    this.pcsvbsis = pcsvbsis;
  }

  public void setPccomsis(BigDecimal pccomsis) {
    this.pccomsis = pccomsis;
  }

  public void setPcbansis(BigDecimal pcbansis) {
    this.pcbansis = pcbansis;
  }

  public void setImmincom(BigDecimal immincom) {
    this.immincom = immincom;
  }

  public void setCdglobal(String cdglobal) {
    this.cdglobal = cdglobal;
  }

  public void setTpriesgo(String tpriesgo) {
    this.tpriesgo = tpriesgo;
  }

  public void setCdtipcal(char cdtipcal) {
    this.cdtipcal = cdtipcal;
  }

  public void setCdcammed(char cdcammed) {
    this.cdcammed = cdcammed;
  }

  public void setCdivapar(char cdivapar) {
    this.cdivapar = cdivapar;
  }

  public void setCdtaspar(char cdtaspar) {
    this.cdtaspar = cdtaspar;
  }

  public void setIdejeliq(char idejeliq) {
    this.idejeliq = idejeliq;
  }

}
