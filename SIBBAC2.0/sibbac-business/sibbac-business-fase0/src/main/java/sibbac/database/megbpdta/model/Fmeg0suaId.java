package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Fmeg0suaId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1667561252976052400L;

  /**
   * Table Field CDALIASS. Alias MEGARA: Alias FIDESSA: CLIENT_MNEMONIC <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdaliass;

  /**
   * Table Field CDSUBCTA. identifier MEGARA: identifier FIDESSA:
   * ACCOUNT_MNEMONIC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdsubcta;

  public Fmeg0suaId() {
  }

  public Fmeg0suaId(String cdaliass, String cdsubcta) {
    super();
    this.cdaliass = cdaliass;
    this.cdsubcta = cdsubcta;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdaliass == null) ? 0 : cdaliass.hashCode());
    result = prime * result + ((cdsubcta == null) ? 0 : cdsubcta.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0suaId other = (Fmeg0suaId) obj;
    if (cdaliass == null) {
      if (other.cdaliass != null)
        return false;
    }
    else if (!cdaliass.equals(other.cdaliass))
      return false;
    if (cdsubcta == null) {
      if (other.cdsubcta != null)
        return false;
    }
    else if (!cdsubcta.equals(other.cdsubcta))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0suaId [cdaliass=%s, cdsubcta=%s]", cdaliass, cdsubcta);
  }

}
