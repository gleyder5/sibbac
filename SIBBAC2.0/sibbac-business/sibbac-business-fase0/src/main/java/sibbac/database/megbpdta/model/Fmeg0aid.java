package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0AID. Alias-IdBySes Documentación para
 * FMEG0AID <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0AID", schema = "MEGBPDTA")
public class Fmeg0aid extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 6416509242113092551L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0aidId id;

  /**
   * Table Field CDIDBYSE. identifier MEGARA: identifier. Siempre tendra el
   * valor 6666 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdidbyse;
  /**
   * Table Field IDSTEXCH. stockExchange MEGARA: stockExchange <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idstexch;
  /**
   * Table Field DESCRIP2. description MEGARA: description <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descrip2;
  
  

  public Fmeg0aidId getId() {
    return id;
  }

  public java.lang.String getCdidbyse() {
    return cdidbyse;
  }

  public java.lang.String getIdstexch() {
    return idstexch;
  }

  public java.lang.String getDescrip2() {
    return descrip2;
  }

  public void setId(Fmeg0aidId id) {
    this.id = id;
  }

  public void setCdidbyse(java.lang.String cdidbyse) {
    this.cdidbyse = cdidbyse;
  }

  public void setIdstexch(java.lang.String idstexch) {
    this.idstexch = idstexch;
  }

  public void setDescrip2(java.lang.String descrip2) {
    this.descrip2 = descrip2;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdaliass(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0AID";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0aid [id=%s, cdidbyse=%s, idstexch=%s, descrip2=%s]", id, cdidbyse, idstexch, descrip2);
  }
  
  
}