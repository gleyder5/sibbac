package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0aecDao;
import sibbac.database.megbpdta.model.Fmeg0aec;
import sibbac.database.megbpdta.model.Fmeg0aecId;

@Service
public class Fmeg0aecBo extends AbstractFmegBo<Fmeg0aec, Fmeg0aecId, Fmeg0aecDao> {

  public List<Fmeg0aec> findAllByIdCdaliass(String cdaliass) {
    return dao.findAllByIdCdaliass(cdaliass);
  }

  public List<Fmeg0aec> findAllByIdCdaliassCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(String cdaliass,
      String cdcustod, String tpsettle, String idenvfid) {
    return dao.findAllByIdCdaliassCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(cdaliass, cdcustod, tpsettle, idenvfid);
  }

  @Override
  @Transactional
  public Fmeg0aec save(Fmeg0aec entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
