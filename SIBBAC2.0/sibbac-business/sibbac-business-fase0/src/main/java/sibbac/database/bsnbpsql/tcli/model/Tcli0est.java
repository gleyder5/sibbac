package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table TCLI0EST. ESTADISTICAS/CUENTA Documentación de
 * la tablaTCLI0EST <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "Tcli0est")
public class Tcli0est implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 4834145762665128936L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdproduc", column = @Column(name = "cdproduc", nullable = false)),
      @AttributeOverride(name = "nuctapro", column = @Column(name = "nuctapro", nullable = false)) })
  protected Tcli0estId id;

  /**
   * Table Field CDNIVEL0. Columna importada CDNIVEL0 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnivel0;
  /**
   * Table Field CDNIVEL1. Columna importada CDNIVEL1 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnivel1;
  /**
   * Table Field CDNIVEL2. Columna importada CDNIVEL2 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnivel2;
  /**
   * Table Field CDNIVEL3. Columna importada CDNIVEL3 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnivel3;
  /**
   * Table Field CDNIVEL4. Columna importada CDNIVEL4 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnivel4;
  /**
   * Table Field CDGRUPOO. Columna importada CDGRUPOO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdgrupoo;
  /**
   * Table Field NBGRUPOO. Columna importada NBGRUPOO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbgrupoo;
  /**
   * Table Field FHMODIFI. Columna importada FHMODIFI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhmodifi;
  /**
   * Table Field CDUSRMOD. Columna importada CDUSRMOD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdusrmod;
  /**
   * Table Field CDHPPAIS. Columna importada CDHPPAIS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdhppais;
  /**
   * Table Field CDPROTRA. Columna importada CDPROTRA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdprotra;
  /**
   * Table Field NUCTATRA. Columna importada NUCTATRA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuctatra;
  /**
   * Table Field CDPROSAL. Columna importada CDPROSAL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdprosal;
  /**
   * Table Field NUCTASAL. Columna importada NUCTASAL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuctasal;
  /**
   * Table Field CDGRUPOA. Columna importada CDGRUPOA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdgrupoa;
  /**
   * Table Field NBGRUPOA. Columna importada NBGRUPOA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbgrupoa;
  /**
   * Table Field CDPROACM. Columna importada CDPROACM Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdproacm;
  /**
   * Table Field NUCTAACM. Columna importada NUCTAACM Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuctaacm;
  public Tcli0estId getId() {
    return id;
  }
  public java.lang.String getCdnivel0() {
    return cdnivel0;
  }
  public java.lang.String getCdnivel1() {
    return cdnivel1;
  }
  public java.lang.String getCdnivel2() {
    return cdnivel2;
  }
  public java.lang.String getCdnivel3() {
    return cdnivel3;
  }
  public java.lang.String getCdnivel4() {
    return cdnivel4;
  }
  public java.lang.String getCdgrupoo() {
    return cdgrupoo;
  }
  public java.lang.String getNbgrupoo() {
    return nbgrupoo;
  }
  public java.math.BigDecimal getFhmodifi() {
    return fhmodifi;
  }
  public java.lang.String getCdusrmod() {
    return cdusrmod;
  }
  public java.lang.String getCdhppais() {
    return cdhppais;
  }
  public java.lang.String getCdprotra() {
    return cdprotra;
  }
  public java.math.BigDecimal getNuctatra() {
    return nuctatra;
  }
  public java.lang.String getCdprosal() {
    return cdprosal;
  }
  public java.math.BigDecimal getNuctasal() {
    return nuctasal;
  }
  public java.lang.String getCdgrupoa() {
    return cdgrupoa;
  }
  public java.lang.String getNbgrupoa() {
    return nbgrupoa;
  }
  public java.lang.String getCdproacm() {
    return cdproacm;
  }
  public java.math.BigDecimal getNuctaacm() {
    return nuctaacm;
  }
  public void setId(Tcli0estId id) {
    this.id = id;
  }
  public void setCdnivel0(java.lang.String cdnivel0) {
    this.cdnivel0 = cdnivel0;
  }
  public void setCdnivel1(java.lang.String cdnivel1) {
    this.cdnivel1 = cdnivel1;
  }
  public void setCdnivel2(java.lang.String cdnivel2) {
    this.cdnivel2 = cdnivel2;
  }
  public void setCdnivel3(java.lang.String cdnivel3) {
    this.cdnivel3 = cdnivel3;
  }
  public void setCdnivel4(java.lang.String cdnivel4) {
    this.cdnivel4 = cdnivel4;
  }
  public void setCdgrupoo(java.lang.String cdgrupoo) {
    this.cdgrupoo = cdgrupoo;
  }
  public void setNbgrupoo(java.lang.String nbgrupoo) {
    this.nbgrupoo = nbgrupoo;
  }
  public void setFhmodifi(java.math.BigDecimal fhmodifi) {
    this.fhmodifi = fhmodifi;
  }
  public void setCdusrmod(java.lang.String cdusrmod) {
    this.cdusrmod = cdusrmod;
  }
  public void setCdhppais(java.lang.String cdhppais) {
    this.cdhppais = cdhppais;
  }
  public void setCdprotra(java.lang.String cdprotra) {
    this.cdprotra = cdprotra;
  }
  public void setNuctatra(java.math.BigDecimal nuctatra) {
    this.nuctatra = nuctatra;
  }
  public void setCdprosal(java.lang.String cdprosal) {
    this.cdprosal = cdprosal;
  }
  public void setNuctasal(java.math.BigDecimal nuctasal) {
    this.nuctasal = nuctasal;
  }
  public void setCdgrupoa(java.lang.String cdgrupoa) {
    this.cdgrupoa = cdgrupoa;
  }
  public void setNbgrupoa(java.lang.String nbgrupoa) {
    this.nbgrupoa = nbgrupoa;
  }
  public void setCdproacm(java.lang.String cdproacm) {
    this.cdproacm = cdproacm;
  }
  public void setNuctaacm(java.math.BigDecimal nuctaacm) {
    this.nuctaacm = nuctaacm;
  }
  @Override
  public String toString() {
    return String
        .format(
            "Tcli0est [id=%s, cdnivel0=%s, cdnivel1=%s, cdnivel2=%s, cdnivel3=%s, cdnivel4=%s, cdgrupoo=%s, nbgrupoo=%s, fhmodifi=%s, cdusrmod=%s, cdhppais=%s, cdprotra=%s, nuctatra=%s, cdprosal=%s, nuctasal=%s, cdgrupoa=%s, nbgrupoa=%s, cdproacm=%s, nuctaacm=%s]",
            id, cdnivel0, cdnivel1, cdnivel2, cdnivel3, cdnivel4, cdgrupoo, nbgrupoo, fhmodifi, cdusrmod, cdhppais,
            cdprotra, nuctatra, cdprosal, nuctasal, cdgrupoa, nbgrupoa, cdproacm, nuctaacm);
  }
  
  
}