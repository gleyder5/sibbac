package sibbac.database.megbpdta.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0suaDao;
import sibbac.database.megbpdta.model.Fmeg0sua;
import sibbac.database.megbpdta.model.Fmeg0suaId;

@Service
public class Fmeg0suaBo extends AbstractFmegBo<Fmeg0sua, Fmeg0suaId, Fmeg0suaDao> {

  public Integer countByCdaliassAndTpmodeidDistinctD(String cdaliass) {
    return dao.countByCdaliassAndTpmodeidDistinctD(cdaliass);
  }

  public Integer countByCdaliassAndCdsubctaAndTpmodeidDistinctD(String cdaliass, String cdsubcta) {
    return dao.countByCdaliassAndCdsubctaAndTpmodeidDistinctD(cdaliass, cdsubcta);
  }

  public List<Fmeg0sua> findAllByCdsubctaAndTpmodeidDistinctD(String cdsubcta) {
    return dao.findAllByCdsubctaAndTpmodeidDistinctD(cdsubcta);
  }
}
