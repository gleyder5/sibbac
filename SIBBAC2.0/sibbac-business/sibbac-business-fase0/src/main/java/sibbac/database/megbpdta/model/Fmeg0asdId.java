package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AAD. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AAD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmeg0asdId implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  @Column(nullable = false)
  protected java.lang.String cdaliass;

  @Column(nullable = false)
  protected java.math.BigDecimal nusecael;

  @Column(nullable = false)
  protected java.math.BigDecimal nusecuen;
  
  public Fmeg0asdId() {
  }

  public Fmeg0asdId(String cdaliass, BigDecimal nusecael, BigDecimal nusecuen) {
    super();
    this.cdaliass = cdaliass;
    this.nusecael = nusecael;
    this.nusecuen = nusecuen;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.math.BigDecimal getNusecael() {
    return nusecael;
  }

  public java.math.BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setNusecael(java.math.BigDecimal nusecael) {
    this.nusecael = nusecael;
  }

  public void setNusecuen(java.math.BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdaliass == null) ? 0 : cdaliass.hashCode());
    result = prime * result + ((nusecael == null) ? 0 : nusecael.hashCode());
    result = prime * result + ((nusecuen == null) ? 0 : nusecuen.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0asdId other = (Fmeg0asdId) obj;
    if (cdaliass == null) {
      if (other.cdaliass != null)
        return false;
    }
    else if (!cdaliass.equals(other.cdaliass))
      return false;
    if (nusecael == null) {
      if (other.nusecael != null)
        return false;
    }
    else if (!nusecael.equals(other.nusecael))
      return false;
    if (nusecuen == null) {
      if (other.nusecuen != null)
        return false;
    }
    else if (!nusecuen.equals(other.nusecuen))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0asdId [cdaliass=%s, nusecael=%s, nusecuen=%s]", cdaliass, nusecael, nusecuen);
  }
  
  
  

}