package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0aad;
import sibbac.database.megbpdta.model.Fmeg0aadId;

@Repository
public interface Fmeg0aadDao extends JpaRepository<Fmeg0aad, Fmeg0aadId> {

  @Query(value = "select max(a.id.nusecuen) from Fmeg0aad a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

  List<Fmeg0aad> findAllByIdCdaliass(String cdaliass);

}
