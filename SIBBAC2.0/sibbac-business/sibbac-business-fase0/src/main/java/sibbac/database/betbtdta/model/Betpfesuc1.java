package sibbac.database.betbtdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table BETPFECL. Equivalencias Documentación de la
 * tablaBETPFECL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "BETPFESUC1", schema = "betbtdta")
public class Betpfesuc1 implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3123716921495236183L;
  

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "ectoms", column = @Column(name = "ectoms", nullable = false)),
      @AttributeOverride(name = "ectcgl", column = @Column(name = "ectcgl", nullable = false)),
      @AttributeOverride(name = "essubcgl", column = @Column(name = "essubcgl", nullable = false)),
      @AttributeOverride(name = "ectpro", column = @Column(name = "ectpro", nullable = false)),
      @AttributeOverride(name = "esubpro", column = @Column(name = "esubpro", nullable = false)) })
  protected Betpfesuc1Id id;

  /**
   * Table Field ectFAL. FECHA ALTA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal ectcta;
  
  @Column(nullable = false)
  protected String esubcta;

  /**
   * Table Field ectFAL. FECHA ALTA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal ectfal;
  /**
   * Table Field ectFBA. FECHA BAJA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal ectfba;
  /**
   * Table Field ectHMO. HORA MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ecthmo;
  /**
   * Table Field ectUMO. USUARIO MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ectumo;
  public Betpfesuc1Id getId() {
    return id;
  }
  public java.math.BigDecimal getEctcta() {
    return ectcta;
  }
  public String getEsubcta() {
    return esubcta;
  }
  public java.math.BigDecimal getEctfal() {
    return ectfal;
  }
  public java.math.BigDecimal getEctfba() {
    return ectfba;
  }
  public java.lang.String getEcthmo() {
    return ecthmo;
  }
  public java.lang.String getEctumo() {
    return ectumo;
  }
  public void setId(Betpfesuc1Id id) {
    this.id = id;
  }
  public void setEctcta(java.math.BigDecimal ectcta) {
    this.ectcta = ectcta;
  }
  public void setEsubcta(String esubcta) {
    this.esubcta = esubcta;
  }
  public void setEctfal(java.math.BigDecimal ectfal) {
    this.ectfal = ectfal;
  }
  public void setEctfba(java.math.BigDecimal ectfba) {
    this.ectfba = ectfba;
  }
  public void setEcthmo(java.lang.String ecthmo) {
    this.ecthmo = ecthmo;
  }
  public void setEctumo(java.lang.String ectumo) {
    this.ectumo = ectumo;
  }
  @Override
  public String toString() {
    return String.format("Betpfectsuc1 [id=%s, ectcta=%s, esubcta=%s, ectfal=%s, ectfba=%s, ecthmo=%s, ectumo=%s]", id,
        ectcta, esubcta, ectfal, ectfba, ecthmo, ectumo);
  }

  
}