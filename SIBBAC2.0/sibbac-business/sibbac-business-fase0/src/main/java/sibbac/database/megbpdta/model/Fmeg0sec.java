package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0SEC. Subcuentas-Elegible Custodian
 * Documentación para FMEG0SEC <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0SEC", schema = "MEGBPDTA")
public class Fmeg0sec extends Fmeg0sucChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -2339422325837620812L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdsubcta", column = @Column(name = "cdsubcta", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  private Fmeg0secId id;

  /**
   * Table Field TPSETTLE. settlementType MEGARA: settlementType FIDESSA:
   * ELIGIBLE_CLEARER_SETT_TYPE. Se accede a los 2 ficheros de conversion para
   * transcodificar en AS400 y luego en FIDESSA. Los valores de MEGARA son los
   * mismos que se envian a FIDESSA (Si es Domestic/Cedel, cambiar a DC Si es
   * Domestic/Domestic, cambiar a DD Si es Domestic/Euroclear, cambiar a DE Si
   * es Euroclear/Cedel, cambiar a EC Si es Euroclear/Domestic, cambiar a ED Si
   * es Euroclear/Euroclear, cambiar a EE) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpsettle;
  /**
   * Table Field IDENTITM. mainEntityId MEGARA: mainEntityId, siempre con valor
   * 0036 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String identitm;
  /**
   * Table Field CDMARKET. market MEGARA: market FIDESSA: ELIGIBLE_CLEARER_MKT,
   * MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field CDCUSTOD. custodian MEGARA: custodian FIDESSA:
   * ELIGIBLE_CUST_ENTITY <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcustod;
  /**
   * Table Field IDENVFID. Enviado S/N Documentación para IDENVFID <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idenvfid;

  public Fmeg0secId getId() {
    return id;
  }

  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  public java.lang.String getIdentitm() {
    return identitm;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getCdcustod() {
    return cdcustod;
  }

  public java.lang.String getIdenvfid() {
    return idenvfid;
  }

  public void setId(Fmeg0secId id) {
    this.id = id;
  }

  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  public void setIdentitm(java.lang.String identitm) {
    this.identitm = identitm;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setCdcustod(java.lang.String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public void setIdenvfid(java.lang.String idenvfid) {
    this.idenvfid = idenvfid;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s", id.getCdsubcta(), id.getNusecuen());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0SEC";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0sec [id=%s, tpsettle=%s, identitm=%s, cdmarket=%s, cdcustod=%s, idenvfid=%s]", id,
        tpsettle, identitm, cdmarket, cdcustod, idenvfid);
  }
  
  

}