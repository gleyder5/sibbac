package sibbac.database.megbpdta.bo;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0entDao;
import sibbac.database.megbpdta.model.Fmeg0ent;

@Service
public class Fmeg0entBo extends AbstractFmegBo<Fmeg0ent, String, Fmeg0entDao> {

}
