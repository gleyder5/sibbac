package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Fmeg0cmgId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3867820615207221739L;

  /**
   * Table Field IDCAMAS4. Campo del AS/400 Documentación para IDCAMAS4 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(name = "IDCAMAS4", nullable = false)
  protected java.lang.String idcamas4;

  /**
   * Table Field DSMEGARA. Informacion recibida de MEGARA Documentación para
   * DSMEGARA <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(name = "DSMEGARA", nullable = false)
  protected java.lang.String dsmegara;

  public Fmeg0cmgId() {
  }

  public Fmeg0cmgId(String idcamas4, String dsmegara) {
    super();
    this.idcamas4 = idcamas4;
    this.dsmegara = dsmegara;
  }

  public java.lang.String getIdcamas4() {
    return idcamas4;
  }

  public java.lang.String getDsmegara() {
    return dsmegara;
  }

  public void setIdcamas4(java.lang.String idcamas4) {
    this.idcamas4 = idcamas4;
  }

  public void setDsmegara(java.lang.String dsmegara) {
    this.dsmegara = dsmegara;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dsmegara == null) ? 0 : dsmegara.hashCode());
    result = prime * result + ((idcamas4 == null) ? 0 : idcamas4.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0cmgId other = (Fmeg0cmgId) obj;
    if (dsmegara == null) {
      if (other.dsmegara != null)
        return false;
    }
    else if (!dsmegara.equals(other.dsmegara))
      return false;
    if (idcamas4 == null) {
      if (other.idcamas4 != null)
        return false;
    }
    else if (!idcamas4.equals(other.idcamas4))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0cmgId [idcamas4=%s, dsmegara=%s]", idcamas4, dsmegara);
  }

}
