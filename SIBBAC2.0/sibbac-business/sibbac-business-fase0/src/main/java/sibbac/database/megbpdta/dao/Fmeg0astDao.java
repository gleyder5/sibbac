package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0ast;
import sibbac.database.megbpdta.model.Fmeg0astId;

@Repository
public interface Fmeg0astDao extends JpaRepository<Fmeg0ast, Fmeg0astId> {

  @Query(value = "select max(a.id.nusecuen) from Fmeg0ast a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

}
