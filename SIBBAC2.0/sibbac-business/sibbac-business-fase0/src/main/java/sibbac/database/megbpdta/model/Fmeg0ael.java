package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0AEL. Alias-Elegible Clearers
 * Documentación para FMEG0AEL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0AEL", schema = "MEGBPDTA")
public class Fmeg0ael extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = 7607500913694195815L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecael", column = @Column(name = "nusecael", nullable = false)) })
  protected Fmeg0aelId id;

  /**
   * Table Field TPSETTLE. settlementType MEGARA: settlementType FIDESSA:
   * ELIGIBLE_CLEARER_SETT_TYPE. Se accede a los 2 ficheros de conversion para
   * transcodificar en AS400 y luego en FIDESSA. Los valores de MEGARA son los
   * mismos que se envian a FIDESSA (Si es Domestic/Cedel, cambiar a DC Si es
   * Domestic/Domestic, cambiar a DD Si es Domestic/Euroclear, cambiar a DE Si
   * es Euroclear/Cedel, cambiar a EC Si es Euroclear/Domestic, cambiar a ED Si
   * es Euroclear/Euroclear, cambiar a EE) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpsettle;
  /**
   * Table Field IDENTITM. mainEntityId MEGARA: mainEntityId, siempre con valor
   * 0036 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String identitm;
  /**
   * Table Field CDMARKET. market MEGARA: market FIDESSA: ELIGIBLE_CLEARER_MKT,
   * MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field CDCLEARE. clearer MEGARA: clearer FIDESSA:
   * ELIGIBLE_CLEARER_ENTITY <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcleare;
  /**
   * Table Field ACCATCLE. accountAtClearer MEGARA: accountAtClearer FIDESSA:
   * ELIGIBLE_CLEARER_ACC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String accatcle;
  /**
   * Table Field IDENVFID. Enviado S/N Documentación para IDENVFID <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idenvfid;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0ael", fetch = FetchType.LAZY)
  private List<Fmeg0asd> fmeg0asds = new ArrayList<Fmeg0asd>();

  public Fmeg0aelId getId() {
    return id;
  }

  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  public java.lang.String getIdentitm() {
    return identitm;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public java.lang.String getAccatcle() {
    return accatcle;
  }

  public java.lang.String getIdenvfid() {
    return idenvfid;
  }

  public List<Fmeg0asd> getFmeg0asds() {
    return fmeg0asds;
  }

  public void setId(Fmeg0aelId id) {
    this.id = id;
  }

  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  public void setIdentitm(java.lang.String identitm) {
    this.identitm = identitm;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public void setAccatcle(java.lang.String accatcle) {
    this.accatcle = accatcle;
  }

  public void setIdenvfid(java.lang.String idenvfid) {
    this.idenvfid = idenvfid;
  }

  public void setFmeg0asds(List<Fmeg0asd> fmeg0asds) {
    this.fmeg0asds = fmeg0asds;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdaliass(), id.getNusecael());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0AEL";
  }

  @Override
  public String toString() {
    return String.format(
        "Fmeg0ael [id=%s, tpsettle=%s, identitm=%s, cdmarket=%s, cdcleare=%s, accatcle=%s, idenvfid=%s]", id, tpsettle,
        identitm, cdmarket, cdcleare, accatcle, idenvfid);
  }

}