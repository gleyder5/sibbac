package sibbac.database.betbtdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.betbtdta.model.Betpfecl;
import sibbac.database.betbtdta.model.BetpfeclId;

@Repository
public interface BetpfeclDao extends JpaRepository<Betpfecl, BetpfeclId> {

  List<Betpfecl> findAllByIdEcloms(String ecloms);

}
