package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Fmeg0comId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4907462992671683116L;

  /**
   * Table Field CDTPCOMI. cdtpcomi 1 ó 2 MEGATRADE: cdtpcomi 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtpcomi;

  /**
   * Table Field CDCOMISI. cdcomisi MEGATRADE: cdcomisi 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcomisi;
  /**
   * Table Field CDTARIFA. cdtarifa 1 ó 2 MEGATRADE: cdtarifa 1 ó 2 <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdtarifa;
  
  public Fmeg0comId() {
  }

  public Fmeg0comId(String cdtpcomi, String cdcomisi, String cdtarifa) {
    super();
    this.cdtpcomi = cdtpcomi;
    this.cdcomisi = cdcomisi;
    this.cdtarifa = cdtarifa;
  }


  public java.lang.String getCdtpcomi() {
    return cdtpcomi;
  }

  public java.lang.String getCdcomisi() {
    return cdcomisi;
  }

  public java.lang.String getCdtarifa() {
    return cdtarifa;
  }

  public void setCdtpcomi(java.lang.String cdtpcomi) {
    this.cdtpcomi = cdtpcomi;
  }

  public void setCdcomisi(java.lang.String cdcomisi) {
    this.cdcomisi = cdcomisi;
  }

  public void setCdtarifa(java.lang.String cdtarifa) {
    this.cdtarifa = cdtarifa;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdcomisi == null) ? 0 : cdcomisi.hashCode());
    result = prime * result + ((cdtarifa == null) ? 0 : cdtarifa.hashCode());
    result = prime * result + ((cdtpcomi == null) ? 0 : cdtpcomi.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0comId other = (Fmeg0comId) obj;
    if (cdcomisi == null) {
      if (other.cdcomisi != null)
        return false;
    }
    else if (!cdcomisi.equals(other.cdcomisi))
      return false;
    if (cdtarifa == null) {
      if (other.cdtarifa != null)
        return false;
    }
    else if (!cdtarifa.equals(other.cdtarifa))
      return false;
    if (cdtpcomi == null) {
      if (other.cdtpcomi != null)
        return false;
    }
    else if (!cdtpcomi.equals(other.cdtpcomi))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0comId [cdtpcomi=%s, cdcomisi=%s, cdtarifa=%s]", cdtpcomi, cdcomisi, cdtarifa);
  }

}
