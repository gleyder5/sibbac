package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0eidDao;
import sibbac.database.megbpdta.model.Fmeg0eid;
import sibbac.database.megbpdta.model.Fmeg0eidId;

@Service
public class Fmeg0eidBo extends AbstractFmegBo<Fmeg0eid, Fmeg0eidId, Fmeg0eidDao> {

  @Override
  @Transactional
  public Fmeg0eid save(Fmeg0eid entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdident1(entity.getId().getCdident1());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
