package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Fmeg0eidId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -5644286533710071835L;

  /**
   * Table Field CDIDENT1. identifier MEGARA: identifier FIDESSA:
   * COUNTERPARTY_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdident1;

  @Column(nullable = false)
  protected BigDecimal nusecuen;

  public Fmeg0eidId() {
  }

  public Fmeg0eidId(String cdident1, BigDecimal nusecuen) {
    super();
    this.cdident1 = cdident1;
    this.nusecuen = nusecuen;
  }

  public java.lang.String getCdident1() {
    return cdident1;
  }

  public BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setCdident1(java.lang.String cdident1) {
    this.cdident1 = cdident1;
  }

  public void setNusecuen(BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdident1 == null) ? 0 : cdident1.hashCode());
    result = prime * result + ((nusecuen == null) ? 0 : nusecuen.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0eidId other = (Fmeg0eidId) obj;
    if (cdident1 == null) {
      if (other.cdident1 != null)
        return false;
    }
    else if (!cdident1.equals(other.cdident1))
      return false;
    if (nusecuen == null) {
      if (other.nusecuen != null)
        return false;
    }
    else if (!nusecuen.equals(other.nusecuen))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0eidId [cdident1=%s, nusecuen=%s]", cdident1, nusecuen);
  }

}
