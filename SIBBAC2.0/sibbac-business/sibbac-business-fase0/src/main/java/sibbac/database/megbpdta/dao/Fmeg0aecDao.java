package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0aec;
import sibbac.database.megbpdta.model.Fmeg0aecId;

@Repository
public interface Fmeg0aecDao extends JpaRepository<Fmeg0aec, Fmeg0aecId> {

  List<Fmeg0aec> findAllByIdCdaliass(String cdaliass);

  @Query(value = "select max(a.id.nusecuen) from Fmeg0aec a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

  @Query("select a from Fmeg0aec a where a.id.cdaliass = :cdaliass and  a.cdcustod = :cdcustod and a.tpsettle = :tpsettle "
      + " and a.idenvfid = :idenvfid and a.fmeg0ali.tpmodeid !='D' ")
  List<Fmeg0aec> findAllByIdCdaliassCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(@Param("cdaliass") String cdaliass,
      @Param("cdcustod") String cdcustod, @Param("tpsettle") String tpsettle, @Param("idenvfid") String idenvfid);

}
