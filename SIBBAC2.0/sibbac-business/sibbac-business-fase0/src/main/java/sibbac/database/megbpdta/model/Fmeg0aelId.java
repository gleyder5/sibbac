package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AAD. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AAD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmeg0aelId implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  @Column(nullable = false)
  protected java.lang.String cdaliass;

  @Column(nullable = false)
  protected java.math.BigDecimal nusecael;

  
  public Fmeg0aelId() {
  }
  
  
  public Fmeg0aelId(String cdaliass, BigDecimal nusecael) {
    super();
    this.cdaliass = cdaliass;
    this.nusecael = nusecael;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.math.BigDecimal getNusecael() {
    return nusecael;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setNusecael(java.math.BigDecimal nusecael) {
    this.nusecael = nusecael;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdaliass == null) ? 0 : cdaliass.hashCode());
    result = prime * result + ((nusecael == null) ? 0 : nusecael.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0aelId other = (Fmeg0aelId) obj;
    if (cdaliass == null) {
      if (other.cdaliass != null)
        return false;
    }
    else if (!cdaliass.equals(other.cdaliass))
      return false;
    if (nusecael == null) {
      if (other.nusecael != null)
        return false;
    }
    else if (!nusecael.equals(other.nusecael))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0aelId [cdaliass=%s, nusecael=%s]", cdaliass, nusecael);
  }

}