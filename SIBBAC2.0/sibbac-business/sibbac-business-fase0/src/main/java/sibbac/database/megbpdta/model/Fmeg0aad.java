package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0AAD. Alias-Addresses Documentación para
 * FMEG0AAD <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0AAD", schema = "MEGBPDTA")
public class Fmeg0aad extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -9184699316687486164L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0aadId id;
  /**
   * Table Field TPADDRES. adresseType MEGARA: adresseType FIDESSA:
   * CONFIRMATION_METHOD. Se accede al fichero de conversion. (Si es AEmail,
   * cambiar a EM, si es AFax, cambiar a FA, si es AFile, cambiar a FI, si es
   * AMail, cambiar a MA, si es AMobile, cambiar a MO, si es AOasys, cambiar a
   * OA, si es APhone, cambiar a PH, si es ASequal, cambiar a SQ, si es ASwift,
   * cambiar a SW, si es ATelex, cambiar a TE) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpaddres;
  /**
   * Table Field CONTFUNC. contactFunction MEGARA: contactFunction <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String contfunc;
  /**
   * Table Field CONTNAME. contactName MEGARA: contactName <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String contname;
  /**
   * Table Field PURPOSEE. purpose MEGARA: purpose <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String purposee;
  /**
   * Table Field MAILADDR. amailaddress MEGARA: amailaddress <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String mailaddr;
  /**
   * Table Field CDZIPCOD. zipCode MEGARA: zipCode <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdzipcod;
  /**
   * Table Field NBLOCAT1. locatiion MEGARA: locatiion <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nblocat1;
  /**
   * Table Field NBLOCAT2. location2 MEGARA: location2 <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nblocat2;
  /**
   * Table Field NBSTREET. street MEGARA: street <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbstreet;
  /**
   * Table Field NUSTREET. streetNumber MEGARA: streetNumber <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nustreet;
  /**
   * Table Field NUFLATTT. flat MEGARA: flat <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nuflattt;
  /**
   * Table Field NUFAXNUM. afaxnumber MEGARA: afaxnumber <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nufaxnum;
  /**
   * Table Field EMAILADD. aemailaddress MEGARA: aemailaddress <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String emailadd;
  /**
   * Table Field SWIFTADD. aswiftaddress MEGARA: aswiftaddress <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String swiftadd;
  /**
   * Table Field NUPHONEE. aphonenumber MEGARA: aphonenumber <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nuphonee;
  /**
   * Table Field IDOASYSA. aoasysaddress MEGARA: aoasysaddress FIDESSA: OASYS
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idoasysa;
  /**
   * Table Field IDSEQUAL. asequaladdress MEGARA: asequaladdress <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idsequal;
  /**
   * Table Field NUTELEXX. atelexNumber MEGARA: atelexNumber <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelexx;
  /**
   * Table Field NOFILEAD. afileaddress MEGARA: afileaddress <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nofilead;
  /**
   * Table Field CDINDADD. Tipo direccion Documentación para CDINDADD <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdindadd;

  public Fmeg0aadId getId() {
    return id;
  }

  public java.lang.String getTpaddres() {
    return tpaddres;
  }

  public java.lang.String getContfunc() {
    return contfunc;
  }

  public java.lang.String getContname() {
    return contname;
  }

  public java.lang.String getPurposee() {
    return purposee;
  }

  public java.lang.String getMailaddr() {
    return mailaddr;
  }

  public java.lang.String getCdzipcod() {
    return cdzipcod;
  }

  public java.lang.String getNblocat1() {
    return nblocat1;
  }

  public java.lang.String getNblocat2() {
    return nblocat2;
  }

  public java.lang.String getNbstreet() {
    return nbstreet;
  }

  public java.lang.String getNustreet() {
    return nustreet;
  }

  public java.lang.String getNuflattt() {
    return nuflattt;
  }

  public java.lang.String getNufaxnum() {
    return nufaxnum;
  }

  public java.lang.String getEmailadd() {
    return emailadd;
  }

  public java.lang.String getSwiftadd() {
    return swiftadd;
  }

  public java.lang.String getNuphonee() {
    return nuphonee;
  }

  public java.lang.String getIdoasysa() {
    return idoasysa;
  }

  public java.lang.String getIdsequal() {
    return idsequal;
  }

  public java.lang.String getNutelexx() {
    return nutelexx;
  }

  public java.lang.String getNofilead() {
    return nofilead;
  }

  public java.lang.String getCdindadd() {
    return cdindadd;
  }

  public void setId(Fmeg0aadId id) {
    this.id = id;
  }

  public void setTpaddres(java.lang.String tpaddres) {
    this.tpaddres = tpaddres;
  }

  public void setContfunc(java.lang.String contfunc) {
    this.contfunc = contfunc;
  }

  public void setContname(java.lang.String contname) {
    this.contname = contname;
  }

  public void setPurposee(java.lang.String purposee) {
    this.purposee = purposee;
  }

  public void setMailaddr(java.lang.String mailaddr) {
    this.mailaddr = mailaddr;
  }

  public void setCdzipcod(java.lang.String cdzipcod) {
    this.cdzipcod = cdzipcod;
  }

  public void setNblocat1(java.lang.String nblocat1) {
    this.nblocat1 = nblocat1;
  }

  public void setNblocat2(java.lang.String nblocat2) {
    this.nblocat2 = nblocat2;
  }

  public void setNbstreet(java.lang.String nbstreet) {
    this.nbstreet = nbstreet;
  }

  public void setNustreet(java.lang.String nustreet) {
    this.nustreet = nustreet;
  }

  public void setNuflattt(java.lang.String nuflattt) {
    this.nuflattt = nuflattt;
  }

  public void setNufaxnum(java.lang.String nufaxnum) {
    this.nufaxnum = nufaxnum;
  }

  public void setEmailadd(java.lang.String emailadd) {
    this.emailadd = emailadd;
  }

  public void setSwiftadd(java.lang.String swiftadd) {
    this.swiftadd = swiftadd;
  }

  public void setNuphonee(java.lang.String nuphonee) {
    this.nuphonee = nuphonee;
  }

  public void setIdoasysa(java.lang.String idoasysa) {
    this.idoasysa = idoasysa;
  }

  public void setIdsequal(java.lang.String idsequal) {
    this.idsequal = idsequal;
  }

  public void setNutelexx(java.lang.String nutelexx) {
    this.nutelexx = nutelexx;
  }

  public void setNofilead(java.lang.String nofilead) {
    this.nofilead = nofilead;
  }

  public void setCdindadd(java.lang.String cdindadd) {
    this.cdindadd = cdindadd;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdaliass(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0AAD";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0aad [id=%s, tpaddres=%s, contname=%s, mailaddr=%s]", id, tpaddres, contname, mailaddr);
  }

}