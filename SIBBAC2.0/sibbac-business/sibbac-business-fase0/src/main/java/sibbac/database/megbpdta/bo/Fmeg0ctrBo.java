package sibbac.database.megbpdta.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0ctrDao;
import sibbac.database.megbpdta.model.Fmeg0ctr;
import sibbac.database.megbpdta.model.Fmeg0ctrId;

@Service
public class Fmeg0ctrBo extends AbstractFmegBo<Fmeg0ctr, Fmeg0ctrId, Fmeg0ctrDao> {

  public List<Fmeg0ctr> findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(String cdtpcomi, String cdcomisi, String cdtarifa) {
    return dao.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(cdtpcomi, cdcomisi, cdtarifa);
  }

}
