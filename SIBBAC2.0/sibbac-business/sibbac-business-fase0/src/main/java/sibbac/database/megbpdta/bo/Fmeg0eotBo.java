package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0eotDao;
import sibbac.database.megbpdta.model.Fmeg0eot;
import sibbac.database.megbpdta.model.Fmeg0eotId;

@Service
public class Fmeg0eotBo extends AbstractFmegBo<Fmeg0eot, Fmeg0eotId, Fmeg0eotDao> {

  public List<Fmeg0eot> findAllByCdident1AndTpidtype(String cdident1, String tpidtype) {
    return dao.findAllByCdident1AndTpidtype(cdident1, tpidtype);
  }

  public List<String> findAllCdidentiByCdident1AndTpidtype(String cdident1, String tpidtype) {
    return dao.findAllCdidentiByCdident1AndTpidtype(cdident1, tpidtype);
  }

  @Override
  @Transactional
  public Fmeg0eot save(Fmeg0eot entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdident1(entity.getId().getCdident1());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
