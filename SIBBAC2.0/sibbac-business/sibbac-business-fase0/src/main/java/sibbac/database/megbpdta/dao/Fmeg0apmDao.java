package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0apm;
import sibbac.database.megbpdta.model.Fmeg0apmId;

@Repository
public interface Fmeg0apmDao extends JpaRepository<Fmeg0apm, Fmeg0apmId> {

  @Query(value = "select max(a.id.nusecuen) from Fmeg0apm a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

}
