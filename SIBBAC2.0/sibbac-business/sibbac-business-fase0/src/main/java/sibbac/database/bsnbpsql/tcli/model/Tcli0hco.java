package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * TCLI0HCO. DATOS DEL CLIENTE
 **/
@Entity
@Table(name = "Tcli0hco")
public class Tcli0hco implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7345497327298294047L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdproduc", column = @Column(name = "cdproduc", nullable = false)),
      @AttributeOverride(name = "nuctapro", column = @Column(name = "nuctapro", nullable = false)) })
  protected Tcli0hcoId id;

  @Column(nullable = false)
  protected java.math.BigDecimal imtramo1;

  @Column(nullable = false)
  protected java.math.BigDecimal imtramo2;

  @Column(nullable = false)
  protected java.math.BigDecimal imtramo3;

  @Column(nullable = false)
  protected java.math.BigDecimal pccomis1;

  @Column(nullable = false)
  protected java.math.BigDecimal pccomis2;

  @Column(nullable = false)
  protected java.math.BigDecimal pccomis3;

  @Column(nullable = false)
  protected java.math.BigDecimal pccomis4;

  @Column(nullable = false)
  protected java.math.BigDecimal imriesco;

  @Column(nullable = false)
  protected java.math.BigDecimal imriesut;

  @Column(nullable = false)
  protected java.lang.String tpconfir;

  @Column(nullable = false)
  protected java.lang.String cdderliq;

  @Column(nullable = false)
  protected java.lang.String cdcomisn;

  @Column(nullable = false)
  protected java.lang.String cdproter;

  @Column(nullable = false)
  protected java.math.BigDecimal pcsvbsis;

  @Column(nullable = false)
  protected java.math.BigDecimal pccomsis;

  @Column(nullable = false)
  protected java.math.BigDecimal pcbansis;

  @Column(nullable = false)
  protected java.math.BigDecimal immincom;

  @Column(nullable = false)
  protected java.lang.String cdglobal;

  @Column(nullable = false)
  protected java.lang.String tpriesgo;

  @Column(nullable = false)
  protected java.lang.String cdtipcal;

  @Column(nullable = false)
  protected java.lang.String cdcammed;

  @Column(nullable = false)
  protected java.math.BigDecimal fhenthis;

  @Column(nullable = false)
  protected java.math.BigDecimal hoenthis;

  @Column(nullable = false)
  protected java.lang.String cdusrhis;

  @Column(nullable = false)
  protected java.lang.String cdaccion;

  @Column(nullable = false)
  protected java.lang.String cdivapar;

  @Column(nullable = false)
  protected java.lang.String cdtaspar;

  public Tcli0hcoId getId() {
    return id;
  }

  public java.math.BigDecimal getImtramo1() {
    return imtramo1;
  }

  public java.math.BigDecimal getImtramo2() {
    return imtramo2;
  }

  public java.math.BigDecimal getImtramo3() {
    return imtramo3;
  }

  public java.math.BigDecimal getPccomis1() {
    return pccomis1;
  }

  public java.math.BigDecimal getPccomis2() {
    return pccomis2;
  }

  public java.math.BigDecimal getPccomis3() {
    return pccomis3;
  }

  public java.math.BigDecimal getPccomis4() {
    return pccomis4;
  }

  public java.math.BigDecimal getImriesco() {
    return imriesco;
  }

  public java.math.BigDecimal getImriesut() {
    return imriesut;
  }

  public java.lang.String getTpconfir() {
    return tpconfir;
  }

  public java.lang.String getCdderliq() {
    return cdderliq;
  }

  public java.lang.String getCdcomisn() {
    return cdcomisn;
  }

  public java.lang.String getCdproter() {
    return cdproter;
  }

  public java.math.BigDecimal getPcsvbsis() {
    return pcsvbsis;
  }

  public java.math.BigDecimal getPccomsis() {
    return pccomsis;
  }

  public java.math.BigDecimal getPcbansis() {
    return pcbansis;
  }

  public java.math.BigDecimal getImmincom() {
    return immincom;
  }

  public java.lang.String getCdglobal() {
    return cdglobal;
  }

  public java.lang.String getTpriesgo() {
    return tpriesgo;
  }

  public java.lang.String getCdtipcal() {
    return cdtipcal;
  }

  public java.lang.String getCdcammed() {
    return cdcammed;
  }

  public java.math.BigDecimal getFhenthis() {
    return fhenthis;
  }

  public java.math.BigDecimal getHoenthis() {
    return hoenthis;
  }

  public java.lang.String getCdusrhis() {
    return cdusrhis;
  }

  public java.lang.String getCdaccion() {
    return cdaccion;
  }

  public java.lang.String getCdivapar() {
    return cdivapar;
  }

  public java.lang.String getCdtaspar() {
    return cdtaspar;
  }

  public void setId(Tcli0hcoId id) {
    this.id = id;
  }

  public void setImtramo1(java.math.BigDecimal imtramo1) {
    this.imtramo1 = imtramo1;
  }

  public void setImtramo2(java.math.BigDecimal imtramo2) {
    this.imtramo2 = imtramo2;
  }

  public void setImtramo3(java.math.BigDecimal imtramo3) {
    this.imtramo3 = imtramo3;
  }

  public void setPccomis1(java.math.BigDecimal pccomis1) {
    this.pccomis1 = pccomis1;
  }

  public void setPccomis2(java.math.BigDecimal pccomis2) {
    this.pccomis2 = pccomis2;
  }

  public void setPccomis3(java.math.BigDecimal pccomis3) {
    this.pccomis3 = pccomis3;
  }

  public void setPccomis4(java.math.BigDecimal pccomis4) {
    this.pccomis4 = pccomis4;
  }

  public void setImriesco(java.math.BigDecimal imriesco) {
    this.imriesco = imriesco;
  }

  public void setImriesut(java.math.BigDecimal imriesut) {
    this.imriesut = imriesut;
  }

  public void setTpconfir(java.lang.String tpconfir) {
    this.tpconfir = tpconfir;
  }

  public void setCdderliq(java.lang.String cdderliq) {
    this.cdderliq = cdderliq;
  }

  public void setCdcomisn(java.lang.String cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  public void setCdproter(java.lang.String cdproter) {
    this.cdproter = cdproter;
  }

  public void setPcsvbsis(java.math.BigDecimal pcsvbsis) {
    this.pcsvbsis = pcsvbsis;
  }

  public void setPccomsis(java.math.BigDecimal pccomsis) {
    this.pccomsis = pccomsis;
  }

  public void setPcbansis(java.math.BigDecimal pcbansis) {
    this.pcbansis = pcbansis;
  }

  public void setImmincom(java.math.BigDecimal immincom) {
    this.immincom = immincom;
  }

  public void setCdglobal(java.lang.String cdglobal) {
    this.cdglobal = cdglobal;
  }

  public void setTpriesgo(java.lang.String tpriesgo) {
    this.tpriesgo = tpriesgo;
  }

  public void setCdtipcal(java.lang.String cdtipcal) {
    this.cdtipcal = cdtipcal;
  }

  public void setCdcammed(java.lang.String cdcammed) {
    this.cdcammed = cdcammed;
  }

  public void setFhenthis(java.math.BigDecimal fhenthis) {
    this.fhenthis = fhenthis;
  }

  public void setHoenthis(java.math.BigDecimal hoenthis) {
    this.hoenthis = hoenthis;
  }

  public void setCdusrhis(java.lang.String cdusrhis) {
    this.cdusrhis = cdusrhis;
  }

  public void setCdaccion(java.lang.String cdaccion) {
    this.cdaccion = cdaccion;
  }

  public void setCdivapar(java.lang.String cdivapar) {
    this.cdivapar = cdivapar;
  }

  public void setCdtaspar(java.lang.String cdtaspar) {
    this.cdtaspar = cdtaspar;
  }

}