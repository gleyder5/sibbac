package sibbac.database.bsnvasql.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnvasql.model.Tval0ps2;

@Repository
public interface Tval0ps2Dao extends JpaRepository<Tval0ps2, String> {

  @Query("SELECT pais FROM Tval0ps2 pais WHERE pais.id.fhdebaja = 0 ORDER BY pais.cdiso2po ASC ")
  List<Tval0ps2> findAllActiveOrderByCdiso2po();

  @Query("SELECT pais FROM Tval0ps2 pais WHERE pais.fhdebaja = 0 AND pais.cdiso2po = :cdiso2po")
  Tval0ps2 findActiveByCdiso2po(@Param("cdiso2po") String cdiso2po);

  @Query(value = "SELECT CDISPAIS FROM BSNVASQL.TVAL0PS2 WHERE FHDEBAJA=0", nativeQuery = true)
  List<Object> findAllCodigosActivos();

  @Query(value = "SELECT ESP.CDHPPAIS AS ISO_BANCO_ESPANIA FROM BSNVASQL.TVAL0PS2 ISO "
      + "INNER JOIN BSNVASQL.TVAL0PS1 ESP ON ISO.CDISO2PO=ESP.CDISO2PO "
      + "WHERE ISO.CDISPAIS =:codIsoPais AND ISO.FHDEBAJA=0 AND ESP.FHDEBAJA=0", nativeQuery = true)
  Object castCodigoPaisFromISOToSpanishBank(@Param("codIsoPais") String codIsoPais);
}
