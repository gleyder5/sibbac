package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0secDao;
import sibbac.database.megbpdta.model.Fmeg0sec;
import sibbac.database.megbpdta.model.Fmeg0secId;

@Service
public class Fmeg0secBo extends AbstractFmegBo<Fmeg0sec, Fmeg0secId, Fmeg0secDao> {

  public List<Fmeg0sec> findAllByIdCdsubcta(String cdsubcta) {
    return dao.findAllByIdCdsubcta(cdsubcta);
  }

  public List<Fmeg0sec> findAllByIdCdsubctaAndIdCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(String cdsubcta,
      String cdcustod, String tpsettle, String idenvfid) {
    return dao.findAllByIdCdsubctaAndIdCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(cdsubcta, cdcustod, tpsettle,
        idenvfid);
  }

  @Override
  @Transactional
  public Fmeg0sec save(Fmeg0sec entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdsubcta(entity.getId().getCdsubcta());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
