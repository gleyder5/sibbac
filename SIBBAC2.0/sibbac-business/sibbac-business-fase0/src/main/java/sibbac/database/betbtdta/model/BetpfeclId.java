package sibbac.database.betbtdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BetpfeclId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1207559406640020175L;

  /**
   * Table Field ECLCLI. CLIENTE AS400 Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal eclcli;

  /**
   * Table Field ECLOMS. CLIENTE GL Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ecloms;

  public BetpfeclId() {

  }

  public BetpfeclId(BigDecimal eclcli, String ecloms) {
    super();
    this.eclcli = eclcli;
    this.ecloms = ecloms;
  }

  public java.math.BigDecimal getEclcli() {
    return eclcli;
  }

  public java.lang.String getEcloms() {
    return ecloms;
  }

  public void setEclcli(java.math.BigDecimal eclcli) {
    this.eclcli = eclcli;
  }

  public void setEcloms(java.lang.String ecloms) {
    this.ecloms = ecloms;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((eclcli == null) ? 0 : eclcli.hashCode());
    result = prime * result + ((ecloms == null) ? 0 : ecloms.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BetpfeclId other = (BetpfeclId) obj;
    if (eclcli == null) {
      if (other.eclcli != null)
        return false;
    }
    else if (!eclcli.equals(other.eclcli))
      return false;
    if (ecloms == null) {
      if (other.ecloms != null)
        return false;
    }
    else if (!ecloms.equals(other.ecloms))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("BetpfeclId [eclcli=%s, ecloms=%s]", eclcli, ecloms);
  }

}
