package sibbac.database.megbpdta.model;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Fmeg0entChildren {

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "cdident1", referencedColumnName = "cdident1", nullable = false, insertable = false, updatable = false)
  protected Fmeg0ent fmeg0ent;

  public Fmeg0ent getFmeg0ent() {
    return fmeg0ent;
  }

  public void setFmeg0ent(Fmeg0ent fmeg0ent) {
    this.fmeg0ent = fmeg0ent;
  }
  
  
  

}
