package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0ENT. Entidades Documentación para
 * FMEG0ENT <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0ENT", schema = "MEGBPDTA")
public class Fmeg0ent implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = 3706579064874933945L;

  /**
   * Table Field CDIDENT1. identifier MEGARA: identifier FIDESSA:
   * COUNTERPARTY_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Id
  @Column(nullable = false)
  protected java.lang.String cdident1;

  /**
   * Table Field CDENTRO1. entityRole MEGARA: entityRole. Se accede al fichero
   * de conversion. (Si su valor es custodian, cambiar a C) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdentro1;
  /**
   * Table Field CDENTRO2. entityRole MEGARA: entityRole. Se accede al fichero
   * de conversion. (Si su valor es broker, cambiar a B) <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdentro2;
  /**
   * Table Field TPENTITY. entityType MEGARA: entityType. Se accede al fichero
   * de conversion. (Si su valor es PersonMoral, cambiar a PM en el AS400) (Si
   * su valor es PersonPhysical, cambiar a PF en el AS400) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpentity;
  /**
   * Table Field CDIDENT2. identifier2 MEGARA: identifier2 <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdident2;
  /**
   * Table Field NBNAMEID. name MAGARA: name FODESSA: COUNTERPARTY_NAME,
   * ENTITY_NAME, MARKET_NAME, LOCAL_CUST_NAME, GLOBAL_CUST_NAME,
   * BENEFICIARY_NAME, ACCOUNT_NAME
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbnameid;
  /**
   * Table Field CDNATION. nationality MEGARA: nationality <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnation;
  /**
   * Table Field CDRESIDE. residence MEGARA: residence FIDESSA:
   * COUNTRY_OF_DOMICILE <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdreside;
  /**
   * Table Field LEGALGRU. legalGroup MEGARA: legalGroup. Siempre estara vacio
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String legalgru;
  /**
   * Table Field FHINCORP. dateIncorporation MEGARA: dateIncorporation. Formato
   * dd/mm/yyyy <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhincorp;
  /**
   * Table Field NBMAINCT. mainContact MEGARA: mainContact <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbmainct;
  /**
   * Table Field NBOTHECT. otherContact MEGARA: otherContact <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbothect;
  /**
   * Table Field NBOTHENA. otherName MEGARA: otherName <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbothena;
  /**
   * Table Field CDBICCID. bic MEGARA: bic FIDESSA: ENTITY_BIC, LOCAL_CUST_BIC,
   * GLOBAL_CUST_BIC, BENEFICIARY_BIC <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbiccid;
  /**
   * Table Field FHBIRTHD. birthday MEGARA: birthday <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhbirthd;
  /**
   * Table Field TPCIVILI. civility MEGARA: civility. Se accede al fichero de
   * conversion.
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpcivili;
  /**
   * Table Field NBFAMIL1. familyName1 MEGARA: familyName1 <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbfamil1;
  /**
   * Table Field NBFAMIL2. familyName2 MEGARA: familyName2 <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbfamil2;
  /**
   * Table Field NBFIRSTT. firstName MEGARA: firstName <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbfirstt;
  /**
   * Table Field TPGENDAR. gendar MEGARA: gendar. Se accede al fcihero de
   * conversion. (Si su valor es Female, cambiar a F en el AS400) (Si su valor
   * es Male, cambiar a M en el AS400) <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpgendar;
  /**
   * Table Field NBSECOND. secondName MEGARA: secondName <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbsecond;
  /**
   * Table Field NBTHIRDD. thirdName MEGARA: thirdName <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbthirdd;
  /**
   * Table Field DESCRIP5. description MEGARA: description <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descrip5;
  /**
   * Table Field TPISSUER. issuerType MEGARA: issuerType <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpissuer;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0ent")
  private List<Fmeg0ead> fmeg0eads = new ArrayList<Fmeg0ead>();
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0ent")
  private List<Fmeg0eid> fmeg0eids = new ArrayList<Fmeg0eid>();
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0ent")
  private List<Fmeg0eot> fmeg0eots = new ArrayList<Fmeg0eot>();

  public java.lang.String getCdident1() {
    return cdident1;
  }

  public java.lang.String getCdentro1() {
    return cdentro1;
  }

  public java.lang.String getCdentro2() {
    return cdentro2;
  }

  public java.lang.String getTpentity() {
    return tpentity;
  }

  public java.lang.String getCdident2() {
    return cdident2;
  }

  public java.lang.String getNbnameid() {
    return nbnameid;
  }

  public java.lang.String getCdnation() {
    return cdnation;
  }

  public java.lang.String getCdreside() {
    return cdreside;
  }

  public java.lang.String getLegalgru() {
    return legalgru;
  }

  public java.lang.String getFhincorp() {
    return fhincorp;
  }

  public java.lang.String getNbmainct() {
    return nbmainct;
  }

  public java.lang.String getNbothect() {
    return nbothect;
  }

  public java.lang.String getNbothena() {
    return nbothena;
  }

  public java.lang.String getCdbiccid() {
    return cdbiccid;
  }

  public java.lang.String getFhbirthd() {
    return fhbirthd;
  }

  public java.lang.String getTpcivili() {
    return tpcivili;
  }

  public java.lang.String getNbfamil1() {
    return nbfamil1;
  }

  public java.lang.String getNbfamil2() {
    return nbfamil2;
  }

  public java.lang.String getNbfirstt() {
    return nbfirstt;
  }

  public java.lang.String getTpgendar() {
    return tpgendar;
  }

  public java.lang.String getNbsecond() {
    return nbsecond;
  }

  public java.lang.String getNbthirdd() {
    return nbthirdd;
  }

  public java.lang.String getDescrip5() {
    return descrip5;
  }

  public java.lang.String getTpissuer() {
    return tpissuer;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public List<Fmeg0ead> getFmeg0eads() {
    return fmeg0eads;
  }

  public List<Fmeg0eid> getFmeg0eids() {
    return fmeg0eids;
  }

  public List<Fmeg0eot> getFmeg0eots() {
    return fmeg0eots;
  }

  public void setCdident1(java.lang.String cdident1) {
    this.cdident1 = cdident1;
  }

  public void setCdentro1(java.lang.String cdentro1) {
    this.cdentro1 = cdentro1;
  }

  public void setCdentro2(java.lang.String cdentro2) {
    this.cdentro2 = cdentro2;
  }

  public void setTpentity(java.lang.String tpentity) {
    this.tpentity = tpentity;
  }

  public void setCdident2(java.lang.String cdident2) {
    this.cdident2 = cdident2;
  }

  public void setNbnameid(java.lang.String nbnameid) {
    this.nbnameid = nbnameid;
  }

  public void setCdnation(java.lang.String cdnation) {
    this.cdnation = cdnation;
  }

  public void setCdreside(java.lang.String cdreside) {
    this.cdreside = cdreside;
  }

  public void setLegalgru(java.lang.String legalgru) {
    this.legalgru = legalgru;
  }

  public void setFhincorp(java.lang.String fhincorp) {
    this.fhincorp = fhincorp;
  }

  public void setNbmainct(java.lang.String nbmainct) {
    this.nbmainct = nbmainct;
  }

  public void setNbothect(java.lang.String nbothect) {
    this.nbothect = nbothect;
  }

  public void setNbothena(java.lang.String nbothena) {
    this.nbothena = nbothena;
  }

  public void setCdbiccid(java.lang.String cdbiccid) {
    this.cdbiccid = cdbiccid;
  }

  public void setFhbirthd(java.lang.String fhbirthd) {
    this.fhbirthd = fhbirthd;
  }

  public void setTpcivili(java.lang.String tpcivili) {
    this.tpcivili = tpcivili;
  }

  public void setNbfamil1(java.lang.String nbfamil1) {
    this.nbfamil1 = nbfamil1;
  }

  public void setNbfamil2(java.lang.String nbfamil2) {
    this.nbfamil2 = nbfamil2;
  }

  public void setNbfirstt(java.lang.String nbfirstt) {
    this.nbfirstt = nbfirstt;
  }

  public void setTpgendar(java.lang.String tpgendar) {
    this.tpgendar = tpgendar;
  }

  public void setNbsecond(java.lang.String nbsecond) {
    this.nbsecond = nbsecond;
  }

  public void setNbthirdd(java.lang.String nbthirdd) {
    this.nbthirdd = nbthirdd;
  }

  public void setDescrip5(java.lang.String descrip5) {
    this.descrip5 = descrip5;
  }

  public void setTpissuer(java.lang.String tpissuer) {
    this.tpissuer = tpissuer;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public void setFmeg0eads(List<Fmeg0ead> fmeg0eads) {
    this.fmeg0eads = fmeg0eads;
  }

  public void setFmeg0eids(List<Fmeg0eid> fmeg0eids) {
    this.fmeg0eids = fmeg0eids;
  }

  public void setFmeg0eots(List<Fmeg0eot> fmeg0eots) {
    this.fmeg0eots = fmeg0eots;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s", cdident1);
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0ENT";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0ent [cdident1=%s, cdentro1=%s, cdentro2=%s, tpentity=%s, cdident2=%s, nbnameid=%s]",
        cdident1, cdentro1, cdentro2, tpentity, cdident2, nbnameid);
  }

}