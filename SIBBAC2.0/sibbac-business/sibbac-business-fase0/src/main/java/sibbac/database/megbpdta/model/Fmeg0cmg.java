package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0CMG. Conversion MEGARA-AS400
 * Documentación para FMEG0CMG <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0CMG", schema = "MEGBPDTA")
public class Fmeg0cmg implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = 8077980072315936404L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "idcamas4", column = @Column(name = "idcamas4", nullable = false)),
      @AttributeOverride(name = "dsmegara", column = @Column(name = "dsmegara", nullable = false)) })
  private Fmeg0cmgId id;
  /**
   * Table Field DSAAS400. Informacion almadenada en AS/400 Documentación para
   * DSAAS400 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(name = "DSAAS400", nullable = false)
  protected java.lang.String dsaas400;

  public Fmeg0cmgId getId() {
    return id;
  }

  public java.lang.String getDsaas400() {
    return dsaas400;
  }

  public void setId(Fmeg0cmgId id) {
    this.id = id;
  }

  public void setDsaas400(java.lang.String dsaas400) {
    this.dsaas400 = dsaas400;
  }

  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getIdcamas4(), id.getDsmegara());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0CMG";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0cmg [id=%s, dsaas400=%s]", id, dsaas400);
  }

}