package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0EOT. Entidades-OtherIds Documentación
 * para FMEG0EOT <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0EOT", schema = "MEGBPDTA")
public class Fmeg0eot extends Fmeg0entChildren implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = -8920892028804554191L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdident1", column = @Column(name = "cdident1", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0eotId id;

  @Column(nullable = true)
  protected String cdidenti;
  /**
   * Table Field TPIDTYPE. idType MEGARA: idType FIDESSA: CONFIRMATION_METHOD Se
   * accede al fichero de conversión para transcodificar los valores de Megara
   * al AS400 (Si es Accounting System, cambiar a -A-, Accountin System, cambiar
   * a -AL-, Alert System Code, cambiar a AC, Bloomber, cambiar a B, Cartera,
   * cambiar a C, Cartera LM, cambiar a CL, Data Ware House, cambiar a D, Data
   * Ware House LM, cambiar a DL, Fiscal, cambiar a F, Kredinet System, cambiar
   * a K, National, cambiar a N, Reuter RIC, cambiar a R) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpidtype;
  /**
   * Table Field DESCRIP1. description MEGARA: description <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descrip1;
  /**
   * Table Field TPDOCUME. documentType MEGARA: documentType. Se accede al
   * fichero de conversion (Si es Fiscal Id Code, cambiar a FC en el AS400) (Si
   * es National Id Card, cambiar a NC en el AS400) (Si es Passport, cambiar a P
   * en el AS400) (Si es Residence Card, cambiar a RC en el AS400) (Si es Social
   * Card, cambiar a SC en el AS400) <!-- begin-user-doc --> <!-- end-user-doc
   * -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdocume;

  public String getCdidenti() {
    return cdidenti;
  }

  public void setCdidenti(String cdidenti) {
    this.cdidenti = cdidenti;
  }

  public Fmeg0eotId getId() {
    return id;
  }

  public java.lang.String getTpidtype() {
    return tpidtype;
  }

  public java.lang.String getDescrip1() {
    return descrip1;
  }

  public java.lang.String getTpdocume() {
    return tpdocume;
  }

  public void setId(Fmeg0eotId id) {
    this.id = id;
  }

  public void setTpidtype(java.lang.String tpidtype) {
    this.tpidtype = tpidtype;
  }

  public void setDescrip1(java.lang.String descrip1) {
    this.descrip1 = descrip1;
  }

  public void setTpdocume(java.lang.String tpdocume) {
    this.tpdocume = tpdocume;
  }

  @Override
  public String getSincronizeId() {
    return String.format("%s|%s", id.getCdident1(), id.getNusecuen());
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0EOT";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0eot [id=%s, cdidenti=%s, tpidtype=%s, descrip1=%s, tpdocume=%s]", id, cdidenti,
        tpidtype, descrip1, tpdocume);
  }

}