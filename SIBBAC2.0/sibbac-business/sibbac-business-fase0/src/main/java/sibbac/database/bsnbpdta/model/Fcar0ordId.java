package sibbac.database.bsnbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Fcar0ordId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 6886700725551189454L;

  /**
   * Table Field FHINIORD. FECHA INICIO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhiniord;

  /**
   * Table Field FHBAJORD. FECHA BAJA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhbajord;
  /**
   * Table Field CDCLIENT. CLIENTE AS400 Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdclient;
  /**
   * Table Field CDBOLSAS. CODIGO BOLSA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdbolsas;

  public Fcar0ordId(BigDecimal fhiniord, BigDecimal fhbajord, String cdclient, String cdbolsas) {
    super();
    this.fhiniord = fhiniord;
    this.fhbajord = fhbajord;
    this.cdclient = cdclient;
    this.cdbolsas = cdbolsas;
  }

  public java.math.BigDecimal getFhiniord() {
    return fhiniord;
  }

  public java.math.BigDecimal getFhbajord() {
    return fhbajord;
  }

  public java.lang.String getCdclient() {
    return cdclient;
  }

  public java.lang.String getCdbolsas() {
    return cdbolsas;
  }

  public void setFhiniord(java.math.BigDecimal fhiniord) {
    this.fhiniord = fhiniord;
  }

  public void setFhbajord(java.math.BigDecimal fhbajord) {
    this.fhbajord = fhbajord;
  }

  public void setCdclient(java.lang.String cdclient) {
    this.cdclient = cdclient;
  }

  public void setCdbolsas(java.lang.String cdbolsas) {
    this.cdbolsas = cdbolsas;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbolsas == null) ? 0 : cdbolsas.hashCode());
    result = prime * result + ((cdclient == null) ? 0 : cdclient.hashCode());
    result = prime * result + ((fhbajord == null) ? 0 : fhbajord.hashCode());
    result = prime * result + ((fhiniord == null) ? 0 : fhiniord.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fcar0ordId other = (Fcar0ordId) obj;
    if (cdbolsas == null) {
      if (other.cdbolsas != null)
        return false;
    }
    else if (!cdbolsas.equals(other.cdbolsas))
      return false;
    if (cdclient == null) {
      if (other.cdclient != null)
        return false;
    }
    else if (!cdclient.equals(other.cdclient))
      return false;
    if (fhbajord == null) {
      if (other.fhbajord != null)
        return false;
    }
    else if (!fhbajord.equals(other.fhbajord))
      return false;
    if (fhiniord == null) {
      if (other.fhiniord != null)
        return false;
    }
    else if (!fhiniord.equals(other.fhiniord))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fcar0ordId [fhiniord=%s, fhbajord=%s, cdclient=%s, cdbolsas=%s]", fhiniord, fhbajord,
        cdclient, cdbolsas);
  }

}
