package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tcli0comId implements Serializable{
  

  /**
   * 
   */
  private static final long serialVersionUID = 4380592285284046612L;
  @Column(length = 3)
  private String cdproduc;
  @Column(length = 5, scale = 0)
  private int nuctapro;

  public Tcli0comId() {
  }

  public Tcli0comId(String cdproduc, short nuctapro) {
    super();
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
  }

  public String getCdproduc() {
    return cdproduc;
  }

  public int getNuctapro() {
    return nuctapro;
  }

  public void setCdproduc(String cdproduc) {
    this.cdproduc = cdproduc;
  }

  public void setNuctapro(int nuctapro) {
    this.nuctapro = nuctapro;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdproduc == null) ? 0 : cdproduc.hashCode());
    result = prime * result + nuctapro;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tcli0comId other = (Tcli0comId) obj;
    if (cdproduc == null) {
      if (other.cdproduc != null)
        return false;
    }
    else if (!cdproduc.equals(other.cdproduc))
      return false;
    if (nuctapro != other.nuctapro)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Tcli0comId [cdproduc=" + cdproduc + ", nuctapro=" + nuctapro + "]";
  }
  
  

}
