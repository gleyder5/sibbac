package sibbac.database.megbpdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmae0as4;
import sibbac.database.megbpdta.model.Fmae0as4Id;

@Repository
public interface Fmae0as4Dao extends JpaRepository<Fmae0as4, Fmae0as4Id> {

  @Query(value = "select a from Fmae0as4 a order by a.id.fhproces asc, a.hrproces asc")
  List<Fmae0as4> findAllOrderByIdFhprocesAndHrproces();

}
