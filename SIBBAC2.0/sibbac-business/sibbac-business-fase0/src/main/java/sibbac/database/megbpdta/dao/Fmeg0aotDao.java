package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0aot;
import sibbac.database.megbpdta.model.Fmeg0aotId;
import sibbac.database.megbpdta.model.Fmeg0eot;

@Repository
public interface Fmeg0aotDao extends JpaRepository<Fmeg0aot, Fmeg0aotId> {

  @Query(value = "select max(a.id.nusecuen) from Fmeg0aot a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);
  
  @Query("select a.cdotheid from Fmeg0aot a where a.id.cdaliass = :cdaliass and a.tpidtype = :tpidtype")
  List<String> findAllCdotheidByCdaliassAndTpidtype(@Param("cdaliass") String cdaliass, @Param("tpidtype") String tpidtype);

  @Query("select a from Fmeg0aot a where a.id.cdaliass = :cdaliass and a.tpidtype = :tpidtype")
  List<Fmeg0aot> findAllByCdaliassAndTpidtype(@Param("cdaliass") String cdaliass, @Param("tpidtype") String tpidtype);

  @Query(value = "select descrip1 from Fmeg0aot a where a.id.cdaliass = :cdaliass and a.tpidtype = :tpidtype and a.cdotheid = :cdotheid")
  String findDescrip1ByCdaliassAndTpidtypeAndCdotheid(@Param("cdaliass") String cdaliass, @Param("tpidtype") String tpidtype,  @Param("cdotheid") String cdotheid);



}
