package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0BSE. Broker by Stock Exchange
 * Documentación para FMEG0BSE <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0BSE", schema = "MEGBPDTA")
public class Fmeg0bse implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = -8657739597107630923L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdidenti", column = @Column(name = "cdidenti", nullable = false)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "cdbroker", nullable = false)) })
  protected Fmeg0bseId id;
  /**
   * Table Field CDLANGUA. language MEGARA: language. Se accede al fichero de
   * conversion. (Se movera el mismo valor que provenga de Megara) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdlangua;
  /**
   * Table Field BILLMARK. billMarketCharge MEGARA: billMarketCharge (Si su
   * valor es true, cambiar a 1, si es false o blancos, cambiar a 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String billmark;
  /**
   * Table Field IDBYSTOC. identifier MEGARA: identifier <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idbystoc;
  /**
   * Table Field ISACTIVE. IsActive MEGARA: IsActive (Si su valor es true,
   * cambiar a 1, si es false o blancos, cambiar a 0) <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isactive;
  /**
   * Table Field ISDEFBRO. isDefaultBroker MEGARA: isDefaultBroker (si su valor
   * es true, cambiar a 1, si es false o blancos, cambiar a 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isdefbro;
  /**
   * Table Field MAXVADAY. maxValidatyDay MEGARA: maxValidatyDay <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal maxvaday;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0bse")
  private List<Fmeg0bbm> fmeg0bbms = new ArrayList<Fmeg0bbm>();

  public Fmeg0bseId getId() {
    return id;
  }

  public java.lang.String getCdlangua() {
    return cdlangua;
  }

  public java.lang.String getBillmark() {
    return billmark;
  }

  public java.lang.String getIdbystoc() {
    return idbystoc;
  }

  public java.lang.String getIsactive() {
    return isactive;
  }

  public java.lang.String getIsdefbro() {
    return isdefbro;
  }

  public java.math.BigDecimal getMaxvaday() {
    return maxvaday;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public void setId(Fmeg0bseId id) {
    this.id = id;
  }

  public void setCdlangua(java.lang.String cdlangua) {
    this.cdlangua = cdlangua;
  }

  public void setBillmark(java.lang.String billmark) {
    this.billmark = billmark;
  }

  public void setIdbystoc(java.lang.String idbystoc) {
    this.idbystoc = idbystoc;
  }

  public void setIsactive(java.lang.String isactive) {
    this.isactive = isactive;
  }

  public void setIsdefbro(java.lang.String isdefbro) {
    this.isdefbro = isdefbro;
  }

  public void setMaxvaday(java.math.BigDecimal maxvaday) {
    this.maxvaday = maxvaday;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public List<Fmeg0bbm> getFmeg0bbms() {
    return fmeg0bbms;
  }

  public void setFmeg0bbms(List<Fmeg0bbm> fmeg0bbms) {
    this.fmeg0bbms = fmeg0bbms;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdstoexc(), id.getCdbroker());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0BSE";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0bse [id=%s, cdlangua=%s, billmark=%s, idbystoc=%s, isactive=%s, isdefbro=%s, maxvaday=%s, fhdebaja=%s, tpmodeid=%s]",
            id, cdlangua, billmark, idbystoc, isactive, isdefbro, maxvaday, fhdebaja, tpmodeid);
  }

}