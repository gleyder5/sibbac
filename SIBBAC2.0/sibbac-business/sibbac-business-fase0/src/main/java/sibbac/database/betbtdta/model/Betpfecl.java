package sibbac.database.betbtdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table BETPFECL. Equivalencias Documentación de la
 * tablaBETPFECL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "BETPFECL", schema = "betbtdta")
public class Betpfecl implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3123716921495236183L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "eclcli", column = @Column(name = "eclcli", nullable = false)),
      @AttributeOverride(name = "ecloms", column = @Column(name = "ecloms", nullable = false)) })
  protected BetpfeclId id;

  /**
   * Table Field ECLFAL. FECHA ALTA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal eclfal;
  /**
   * Table Field ECLFBA. FECHA BAJA Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal eclfba;
  /**
   * Table Field ECLHMO. HORA MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String eclhmo;
  /**
   * Table Field ECLUMO. USUARIO MODIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String eclumo;

  public BetpfeclId getId() {
    return id;
  }

  public java.math.BigDecimal getEclfal() {
    return eclfal;
  }

  public java.math.BigDecimal getEclfba() {
    return eclfba;
  }

  public java.lang.String getEclhmo() {
    return eclhmo;
  }

  public java.lang.String getEclumo() {
    return eclumo;
  }

  public void setId(BetpfeclId id) {
    this.id = id;
  }

  public void setEclfal(java.math.BigDecimal eclfal) {
    this.eclfal = eclfal;
  }

  public void setEclfba(java.math.BigDecimal eclfba) {
    this.eclfba = eclfba;
  }

  public void setEclhmo(java.lang.String eclhmo) {
    this.eclhmo = eclhmo;
  }

  public void setEclumo(java.lang.String eclumo) {
    this.eclumo = eclumo;
  }

}