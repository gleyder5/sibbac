package sibbac.database.bsnbpsql.tcli.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tcli.model.Tcli0pcl;
import sibbac.database.bsnbpsql.tcli.model.Tcli0pclId;

@Repository
public interface Tcli0pclDao extends JpaRepository<Tcli0pcl, Tcli0pclId> {

  @Query("select count(p) from Tcli0pcl p where p.id.cdproduc = '008' and p.id.nuctapro = :nuctapro")
  int existsNuctapro(@Param("nuctapro") BigDecimal nuctapro);

}
