package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0AHL. Alias-Holders Documentación para
 * FMEG0AHL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0AHL", schema = "MEGPBDTA")
public class Fmeg0ahl extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -2277403271354823829L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0ahlId id;

  /**
   * Table Field CDEXTREF. externalReference MEGARA: externalReference <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdextref;

  /**
   * Table Field ISMAINAL. IsMain MEGARA: IsMain (Si su contenido es true,
   * convertir en 1, si fuese false o blancos, convertir o 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ismainal;

  /**
   * Table Field PARTPERC. participationPercentage MEGARA:
   * participationPercentage <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String partperc;

  /**
   * Table Field CDHOLDER. holder MEGARA: holder <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdholder;

  /**
   * Table Field TPHOLDER. holderType MEGARA: holderType. Se accede al fichero
   * de conversion. (Si su valor es Holder, cambiar a H en el AS400) (Si su
   * valor Representative, cambiar a R en el AS400) (Si su valor es Ownership
   * Without Usufruct, cambiar a O en el AS400) (Si su valor esUsufructuary,
   * cambiar a U en el AS400)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpholder;
  
  

  public Fmeg0ahlId getId() {
    return id;
  }

  public java.lang.String getCdextref() {
    return cdextref;
  }

  public java.lang.String getIsmainal() {
    return ismainal;
  }

  public java.lang.String getPartperc() {
    return partperc;
  }

  public java.lang.String getCdholder() {
    return cdholder;
  }

  public java.lang.String getTpholder() {
    return tpholder;
  }

  public void setId(Fmeg0ahlId id) {
    this.id = id;
  }

  public void setCdextref(java.lang.String cdextref) {
    this.cdextref = cdextref;
  }

  public void setIsmainal(java.lang.String ismainal) {
    this.ismainal = ismainal;
  }

  public void setPartperc(java.lang.String partperc) {
    this.partperc = partperc;
  }

  public void setCdholder(java.lang.String cdholder) {
    this.cdholder = cdholder;
  }

  public void setTpholder(java.lang.String tpholder) {
    this.tpholder = tpholder;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdaliass(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0AHL";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0ahl [id=%s, cdextref=%s, ismainal=%s, partperc=%s, cdholder=%s, tpholder=%s]", id,
        cdextref, ismainal, partperc, cdholder, tpholder);
  }
  
  
}