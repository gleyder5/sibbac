package sibbac.database.megbpdta.model;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Fmeg0sucChildren {

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "cdsubcta", referencedColumnName = "cdsubcta", nullable = false, insertable = false, updatable = false)
  protected Fmeg0suc fmeg0suc;

  public Fmeg0suc getFmeg0suc() {
    return fmeg0suc;
  }

  public void setFmeg0suc(Fmeg0suc fmeg0suc) {
    this.fmeg0suc = fmeg0suc;
  }

}
