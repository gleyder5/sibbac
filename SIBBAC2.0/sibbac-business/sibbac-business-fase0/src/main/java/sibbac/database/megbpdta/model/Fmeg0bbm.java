package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0BBM. Broker by Stock
 * Exchange-BrokerMedias Documentación para FMEG0BBM <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0BBM", schema = "MEGBPDTA")
public class Fmeg0bbm implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = -7601937441176985527L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdstoexc", column = @Column(name = "cdstoexc", nullable = false)),
      @AttributeOverride(name = "cdbroker", column = @Column(name = "cdbroker", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0bbmId id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns({
      @JoinColumn(name = "cdstoexc", referencedColumnName = "cdstoexc", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "cdbroker", referencedColumnName = "cdbroker", nullable = false, insertable = false, updatable = false) })
  protected Fmeg0bse fmeg0bse;

  /**
   * Table Field ISACTIVE. IsActive MEGARA: IsActive (Si su valor es true,
   * cambiar a 1, si es false o blancos, cambiar a 0) <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isactive;
  /**
   * Table Field PREFEORD. preferenceOrder MEGARA: preferenceOrder <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String prefeord;
  /**
   * Table Field IDMEDIAA. media MEGARA: media <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idmediaa;

  public Fmeg0bbmId getId() {
    return id;
  }

  public Fmeg0bse getFmeg0bse() {
    return fmeg0bse;
  }

  public java.lang.String getIsactive() {
    return isactive;
  }

  public java.lang.String getPrefeord() {
    return prefeord;
  }

  public java.lang.String getIdmediaa() {
    return idmediaa;
  }

  public void setId(Fmeg0bbmId id) {
    this.id = id;
  }

  public void setFmeg0bse(Fmeg0bse fmeg0bse) {
    this.fmeg0bse = fmeg0bse;
  }

  public void setIsactive(java.lang.String isactive) {
    this.isactive = isactive;
  }

  public void setPrefeord(java.lang.String prefeord) {
    this.prefeord = prefeord;
  }

  public void setIdmediaa(java.lang.String idmediaa) {
    this.idmediaa = idmediaa;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s|%S", id.getCdstoexc(), id.getCdbroker(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0BBM";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0bbm [id=%s,  isactive=%s, prefeord=%s, idmediaa=%s]", id, isactive, prefeord, idmediaa);
  }

}