package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AS4. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AS4 <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmae0as4Id implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  /**
   * Table Field IDINFMAE. Identificador informacion maestra Documentación para
   * IDINFMAE <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idinfmae;

  /**
   * Table Field IDCLIENT. Identificador cliente Documentación para IDCLIENT
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idclient;
  /**
   * Table Field IDPROCTA. Identificador cuenta/subcuenta Documentación para
   * IDPROCTA <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idprocta;
  /**
   * Table Field NUDIRECC. Numero identificador del contacto Documentación para
   * NUDIRECC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nudirecc;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;
  /**
   * Table Field FHPROCES. Fecha sistema Documentación para FHPROCES <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhproces;
  
  public Fmae0as4Id() {
  }

  public Fmae0as4Id(String idinfmae, String idclient, String idprocta, BigDecimal nudirecc, String tpmodeid,
      String fhproces) {
    super();
    this.idinfmae = idinfmae;
    this.idclient = idclient;
    this.idprocta = idprocta;
    this.nudirecc = nudirecc;
    this.tpmodeid = tpmodeid;
    this.fhproces = fhproces;
  }

  /**
   * Set value of Field IDINFMAE <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _idinfmae value of the field
   * @see #idinfmae
   **/
  public void setIdinfmae(java.lang.String _idinfmae) {
    idinfmae = _idinfmae;
  }

  /**
   * Get value of Field IDINFMAE <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #idinfmae
   **/
  public java.lang.String getIdinfmae() {
    return idinfmae;
  }

  /**
   * Set value of Field IDCLIENT <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _idclient value of the field
   * @see #idclient
   **/
  public void setIdclient(java.lang.String _idclient) {
    idclient = _idclient;
  }

  /**
   * Get value of Field IDCLIENT <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #idclient
   **/
  public java.lang.String getIdclient() {
    return idclient;
  }

  /**
   * Set value of Field IDPROCTA <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _idprocta value of the field
   * @see #idprocta
   **/
  public void setIdprocta(java.lang.String _idprocta) {
    idprocta = _idprocta;
  }

  /**
   * Get value of Field IDPROCTA <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #idprocta
   **/
  public java.lang.String getIdprocta() {
    return idprocta;
  }

  /**
   * Set value of Field NUDIRECC <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _nudirecc value of the field
   * @see #nudirecc
   **/
  public void setNudirecc(java.math.BigDecimal _nudirecc) {
    nudirecc = _nudirecc;
  }

  /**
   * Get value of Field NUDIRECC <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #nudirecc
   **/
  public java.math.BigDecimal getNudirecc() {
    return nudirecc;
  }

  /**
   * Set value of Field TPMODEID <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _tpmodeid value of the field
   * @see #tpmodeid
   **/
  public void setTpmodeid(java.lang.String _tpmodeid) {
    tpmodeid = _tpmodeid;
  }

  /**
   * Get value of Field TPMODEID <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #tpmodeid
   **/
  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  /**
   * Set value of Field FHPROCES <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _fhproces value of the field
   * @see #fhproces
   **/
  public void setFhproces(java.lang.String _fhproces) {
    fhproces = _fhproces;
  }

  /**
   * Get value of Field FHPROCES <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #fhproces
   **/
  public java.lang.String getFhproces() {
    return fhproces;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fhproces == null) ? 0 : fhproces.hashCode());
    result = prime * result + ((idclient == null) ? 0 : idclient.hashCode());
    result = prime * result + ((idinfmae == null) ? 0 : idinfmae.hashCode());
    result = prime * result + ((idprocta == null) ? 0 : idprocta.hashCode());
    result = prime * result + ((nudirecc == null) ? 0 : nudirecc.hashCode());
    result = prime * result + ((tpmodeid == null) ? 0 : tpmodeid.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmae0as4Id other = (Fmae0as4Id) obj;
    if (fhproces == null) {
      if (other.fhproces != null)
        return false;
    }
    else if (!fhproces.equals(other.fhproces))
      return false;
    if (idclient == null) {
      if (other.idclient != null)
        return false;
    }
    else if (!idclient.equals(other.idclient))
      return false;
    if (idinfmae == null) {
      if (other.idinfmae != null)
        return false;
    }
    else if (!idinfmae.equals(other.idinfmae))
      return false;
    if (idprocta == null) {
      if (other.idprocta != null)
        return false;
    }
    else if (!idprocta.equals(other.idprocta))
      return false;
    if (nudirecc == null) {
      if (other.nudirecc != null)
        return false;
    }
    else if (!nudirecc.equals(other.nudirecc))
      return false;
    if (tpmodeid == null) {
      if (other.tpmodeid != null)
        return false;
    }
    else if (!tpmodeid.equals(other.tpmodeid))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmae0as4Id [idinfmae=%s, idclient=%s, idprocta=%s, nudirecc=%s, tpmodeid=%s, fhproces=%s]",
        idinfmae, idclient, idprocta, nudirecc, tpmodeid, fhproces);
  }

}