package sibbac.database.megbpdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0fie;
import sibbac.database.megbpdta.model.Fmeg0fieId;

@Repository
public interface Fmeg0fieDao extends JpaRepository<Fmeg0fie, Fmeg0fieId> {

  @Query("select f from Fmeg0fie f where f.id.nomxml = :nomxml order by f.id.fecproc desc, f.id.horproc desc")
  List<Fmeg0fie> findAllByIdNomxmlOrderByFecprocDescAndHorprocDesc(@Param("nomxml") String nomxml);

}
