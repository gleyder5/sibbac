package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0AEC. Alias-Elegible Custodian
 * Documentación para FMEG0AEC <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0AEC", schema = "MEGBPDTA")
public class Fmeg0aec extends Fmeg0aliChildren implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = -1576272772797731359L;

  /**
   * Table Field NUSECUEN. Contador Secuencial Documentación para NUSECUEN
   * (Contador de secuencia de registros) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0aecId id;

  /**
   * Table Field TPSETTLE. settlementType MEGARA: settlementType FIDESSA:
   * ELIGIBLE_CLEARER_SETT_TYPE. Se accede a los 2 ficheros de conversion para
   * transcodificar en AS400 y luego en FIDESSA. Los valores de MEGARA son los
   * mismos que se envian a FIDESSA (Si es Domestic/Cedel, cambiar a DC Si es
   * Domestic/Domestic, cambiar a DD Si es Domestic/Euroclear, cambiar a DE Si
   * es Euroclear/Cedel, cambiar a EC Si es Euroclear/Domestic, cambiar a ED Si
   * es Euroclear/Euroclear, cambiar a EE) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpsettle;
  /**
   * Table Field IDENTITM. mainEntityId MEGARA: mainEntityId, siempre con valor
   * 0036 <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String identitm;
  /**
   * Table Field CDMARKET. market MEGARA: market FIDESSA: ELIGIBLE_CLEARER_MKT,
   * MARKET <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdmarket;
  /**
   * Table Field TPCLEMOD. clearingMode MEGARA: clearingMode. Se accede al
   * fichero de conversion (Si su valor es SOCIEDAD, cambiar a S en el AS400, si
   * es RECTORA, cambiar a R en el AS400) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpclemod;
  /**
   * Table Field CDCUSTOD. custodian MEGARA: custodian FIDESSA:
   * ELIGIBLE_CUST_ENTITY <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcustod;
  /**
   * Table Field IDENVFID. Enviado S/N Documentación para IDENVFID <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String idenvfid;

  public Fmeg0aecId getId() {
    return id;
  }

  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  public java.lang.String getIdentitm() {
    return identitm;
  }

  public java.lang.String getCdmarket() {
    return cdmarket;
  }

  public java.lang.String getTpclemod() {
    return tpclemod;
  }

  public java.lang.String getCdcustod() {
    return cdcustod;
  }

  public java.lang.String getIdenvfid() {
    return idenvfid;
  }

  public void setId(Fmeg0aecId id) {
    this.id = id;
  }

  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  public void setIdentitm(java.lang.String identitm) {
    this.identitm = identitm;
  }

  public void setCdmarket(java.lang.String cdmarket) {
    this.cdmarket = cdmarket;
  }

  public void setTpclemod(java.lang.String tpclemod) {
    this.tpclemod = tpclemod;
  }

  public void setCdcustod(java.lang.String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public void setIdenvfid(java.lang.String idenvfid) {
    this.idenvfid = idenvfid;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdaliass(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0AEC";
  }

  @Override
  public String toString() {
    return String.format(
        "Fmeg0aec [id=%s, tpsettle=%s, identitm=%s, cdmarket=%s, tpclemod=%s, cdcustod=%s, idenvfid=%s]", id, tpsettle,
        identitm, cdmarket, tpclemod, cdcustod, idenvfid);
  }
  
  

}