package sibbac.database.megbpdta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0comId;

@Repository
public interface Fmeg0comDao extends JpaRepository<Fmeg0com, Fmeg0comId> {

}
