package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0shlDao;
import sibbac.database.megbpdta.model.Fmeg0shl;
import sibbac.database.megbpdta.model.Fmeg0shlId;

@Service
public class Fmeg0shlBo extends AbstractFmegBo<Fmeg0shl, Fmeg0shlId, Fmeg0shlDao> {

  public List<Fmeg0shl> findAllByIdCdsubcta(String cdsubcta) {
    return dao.findAllByIdCdsubcta(cdsubcta);
  }

  @Override
  @Transactional
  public Fmeg0shl save(Fmeg0shl entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdsubcta(entity.getId().getCdsubcta());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
