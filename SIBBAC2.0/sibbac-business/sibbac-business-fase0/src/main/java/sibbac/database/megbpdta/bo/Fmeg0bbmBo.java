package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0bbmDao;
import sibbac.database.megbpdta.model.Fmeg0bbm;
import sibbac.database.megbpdta.model.Fmeg0bbmId;

@Service
public class Fmeg0bbmBo extends AbstractFmegBo<Fmeg0bbm, Fmeg0bbmId, Fmeg0bbmDao> {

  @Override
  @Transactional
  public Fmeg0bbm save(Fmeg0bbm entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdstoexcAndIdCdbroker(entity.getFmeg0bse().getId().getCdstoexc(),
          entity.getFmeg0bse().getId().getCdbroker());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
