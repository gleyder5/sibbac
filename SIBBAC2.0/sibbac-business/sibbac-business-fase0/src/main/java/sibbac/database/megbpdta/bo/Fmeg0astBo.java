package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0astDao;
import sibbac.database.megbpdta.model.Fmeg0ast;
import sibbac.database.megbpdta.model.Fmeg0astId;

@Service
public class Fmeg0astBo extends AbstractFmegBo<Fmeg0ast, Fmeg0astId, Fmeg0astDao> {

  @Override
  @Transactional
  public Fmeg0ast save(Fmeg0ast entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
