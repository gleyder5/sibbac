package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AAD. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AAD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmeg0bhlId implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  @Column(nullable = false)
  protected java.lang.String cdbrocli;

  @Column(nullable = false)
  protected java.math.BigDecimal nusecuen;

  public Fmeg0bhlId() {
  }

  public Fmeg0bhlId(String cdbrocli, BigDecimal nusecuen) {
    super();
    this.cdbrocli = cdbrocli;
    this.nusecuen = nusecuen;
  }

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.math.BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setNusecuen(java.math.BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbrocli == null) ? 0 : cdbrocli.hashCode());
    result = prime * result + ((nusecuen == null) ? 0 : nusecuen.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0bhlId other = (Fmeg0bhlId) obj;
    if (cdbrocli == null) {
      if (other.cdbrocli != null)
        return false;
    }
    else if (!cdbrocli.equals(other.cdbrocli))
      return false;
    if (nusecuen == null) {
      if (other.nusecuen != null)
        return false;
    }
    else if (!nusecuen.equals(other.nusecuen))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0bhlId [cdbrocli=%s, nusecuen=%s]", cdbrocli, nusecuen);
  }

}