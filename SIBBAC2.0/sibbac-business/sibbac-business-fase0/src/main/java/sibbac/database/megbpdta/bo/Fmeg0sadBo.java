package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0sadDao;
import sibbac.database.megbpdta.model.Fmeg0sad;
import sibbac.database.megbpdta.model.Fmeg0sadId;

@Service
public class Fmeg0sadBo extends AbstractFmegBo<Fmeg0sad, Fmeg0sadId, Fmeg0sadDao> {

  @Override
  @Transactional
  public Fmeg0sad save(Fmeg0sad entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdsubcta(entity.getId().getCdsubcta());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
