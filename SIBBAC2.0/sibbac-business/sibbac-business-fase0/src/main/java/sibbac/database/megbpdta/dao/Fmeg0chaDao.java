package sibbac.database.megbpdta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0cha;

@Repository
public interface Fmeg0chaDao extends JpaRepository<Fmeg0cha, String> {


}
