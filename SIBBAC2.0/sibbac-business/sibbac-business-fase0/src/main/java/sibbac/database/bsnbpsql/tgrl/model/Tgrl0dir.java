package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TGRL0DIR")
public class Tgrl0dir implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -9143851940520524741L;

  @AttributeOverrides({
      @AttributeOverride(name = "nuoprout", column = @Column(name = "NUOPROUT", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nupareje", column = @Column(name = "NUPAREJE", nullable = false, length = 4, scale = 0)),
      @AttributeOverride(name = "nusecnbr", column = @Column(name = "NUSECNBR", nullable = false, length = 3, scale = 0)) })
  @EmbeddedId
  private Tgrl0dirId id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "NUOPROUT", referencedColumnName = "NUOPROUT", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUPAREJE", referencedColumnName = "NUPAREJE", nullable = false, insertable = false, updatable = false) })
  private Tgrl0eje tgrl0eje;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "direccion")
  private List<Tgrl0nom> titulares = new ArrayList<Tgrl0nom>();

  /**
   * Table Field TPDOMICI. Tipo de domicilio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 2, nullable = false)
  protected java.lang.String tpdomici;
  /**
   * Table Field NBDOMICI. Descripción domicilio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 40, nullable = false)
  protected java.lang.String nbdomici;
  /**
   * Table Field NUDOMICI. Número domicilio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4, nullable = false)
  protected java.lang.String nudomici;
  /**
   * Table Field NBCIUDAD. Ciudad Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 40, nullable = false)
  protected java.lang.String nbciudad;
  /**
   * Table Field NBPROVIN. Provincia Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 25, nullable = false)
  protected java.lang.String nbprovin;
  /**
   * Table Field CDPOSTAL. Cod. Postal Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 5, nullable = false)
  protected java.lang.String cdpostal;
  /**
   * Table Field CDDEPAIS. Cod. País Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 3, nullable = false)
  protected java.lang.String cddepais;
  /**
   * Table Field TPPROCED. Tipo procedencia Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1, nullable = false)
  protected Character tpproced;
  /**
   * Table Field FHENTSVB. F. entrada SVB Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8, scale = 0, nullable = false)
  protected java.math.BigDecimal fhentsvb;
  /**
   * Table Field FHENVBOL. F. envío bolsa Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8, scale = 0, nullable = false)
  protected java.math.BigDecimal fhenvbol;
  /**
   * Table Field FHBAJANO. F. baja Nombre Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8, scale = 0, nullable = false)
  protected java.math.BigDecimal fhbajano;

  public Tgrl0dir() {
  }

  /**
   * Set value of Field TPDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _tpdomici value of the field
   * @see #tpdomici
   * @generated
   **/
  public void setTpdomici(java.lang.String _tpdomici) {
    tpdomici = _tpdomici;
  }

  /**
   * Get value of Field TPDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #tpdomici
   * @generated
   **/
  public java.lang.String getTpdomici() {
    return tpdomici;
  }

  /**
   * Set value of Field NBDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _nbdomici value of the field
   * @see #nbdomici
   * @generated
   **/
  public void setNbdomici(java.lang.String _nbdomici) {
    nbdomici = _nbdomici;
  }

  /**
   * Get value of Field NBDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #nbdomici
   * @generated
   **/
  public java.lang.String getNbdomici() {
    return nbdomici;
  }

  /**
   * Set value of Field NUDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _nudomici value of the field
   * @see #nudomici
   * @generated
   **/
  public void setNudomici(java.lang.String _nudomici) {
    nudomici = _nudomici;
  }

  /**
   * Get value of Field NUDOMICI <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #nudomici
   * @generated
   **/
  public java.lang.String getNudomici() {
    return nudomici;
  }

  /**
   * Set value of Field NBCIUDAD <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _nbciudad value of the field
   * @see #nbciudad
   * @generated
   **/
  public void setNbciudad(java.lang.String _nbciudad) {
    nbciudad = _nbciudad;
  }

  /**
   * Get value of Field NBCIUDAD <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #nbciudad
   * @generated
   **/
  public java.lang.String getNbciudad() {
    return nbciudad;
  }

  /**
   * Set value of Field NBPROVIN <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _nbprovin value of the field
   * @see #nbprovin
   * @generated
   **/
  public void setNbprovin(java.lang.String _nbprovin) {
    nbprovin = _nbprovin;
  }

  /**
   * Get value of Field NBPROVIN <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #nbprovin
   * @generated
   **/
  public java.lang.String getNbprovin() {
    return nbprovin;
  }

  /**
   * Set value of Field CDPOSTAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _cdpostal value of the field
   * @see #cdpostal
   * @generated
   **/
  public void setCdpostal(java.lang.String _cdpostal) {
    cdpostal = _cdpostal;
  }

  /**
   * Get value of Field CDPOSTAL <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #cdpostal
   * @generated
   **/
  public java.lang.String getCdpostal() {
    return cdpostal;
  }

  /**
   * Set value of Field CDDEPAIS <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _cddepais value of the field
   * @see #cddepais
   * @generated
   **/
  public void setCddepais(java.lang.String _cddepais) {
    cddepais = _cddepais;
  }

  /**
   * Get value of Field CDDEPAIS <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #cddepais
   * @generated
   **/
  public java.lang.String getCddepais() {
    return cddepais;
  }

  /**
   * Set value of Field TPPROCED <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _tpproced value of the field
   * @see #tpproced
   * @generated
   **/
  public void setTpproced(Character _tpproced) {
    tpproced = _tpproced;
  }

  /**
   * Get value of Field TPPROCED <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #tpproced
   * @generated
   **/
  public Character getTpproced() {
    return tpproced;
  }

  /**
   * Set value of Field FHENTSVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _fhentsvb value of the field
   * @see #fhentsvb
   * @generated
   **/
  public void setFhentsvb(java.math.BigDecimal _fhentsvb) {
    fhentsvb = _fhentsvb;
  }

  /**
   * Get value of Field FHENTSVB <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #fhentsvb
   * @generated
   **/
  public java.math.BigDecimal getFhentsvb() {
    return fhentsvb;
  }

  /**
   * Set value of Field FHENVBOL <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _fhenvbol value of the field
   * @see #fhenvbol
   * @generated
   **/
  public void setFhenvbol(java.math.BigDecimal _fhenvbol) {
    fhenvbol = _fhenvbol;
  }

  /**
   * Get value of Field FHENVBOL <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #fhenvbol
   * @generated
   **/
  public java.math.BigDecimal getFhenvbol() {
    return fhenvbol;
  }

  /**
   * Set value of Field FHBAJANO <!-- begin-user-doc --> <!-- end-user-doc -->
   * @param _fhbajano value of the field
   * @see #fhbajano
   * @generated
   **/
  public void setFhbajano(java.math.BigDecimal _fhbajano) {
    fhbajano = _fhbajano;
  }

  /**
   * Get value of Field FHBAJANO <!-- begin-user-doc --> <!-- end-user-doc -->
   * @return value of the field
   * @see #fhbajano
   * @generated
   **/
  public java.math.BigDecimal getFhbajano() {
    return fhbajano;
  }

  public Tgrl0dirId getId() {
    return id;
  }

  public Tgrl0eje getTgrl0eje() {
    return tgrl0eje;
  }

  public List<Tgrl0nom> getTitulares() {
    return titulares;
  }

  public void setId(Tgrl0dirId id) {
    this.id = id;
  }

  public void setTgrl0eje(Tgrl0eje tgrl0eje) {
    this.tgrl0eje = tgrl0eje;
  }

  public void setTitulares(List<Tgrl0nom> titulares) {
    this.titulares = titulares;
  }

  @Override
  public String toString() {
    return "Tgrl0dir [id=" + id + ", tpdomici=" + tpdomici + ", nbdomici=" + nbdomici + ", nudomici=" + nudomici
        + ", nbciudad=" + nbciudad + ", nbprovin=" + nbprovin + ", cdpostal=" + cdpostal + ", cddepais=" + cddepais
        + ", tpproced=" + tpproced + ", fhentsvb=" + fhentsvb + ", fhenvbol=" + fhenvbol + ", fhbajano=" + fhbajano
        + "]";
  }
  
  
  
}
