package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tcli0hcoId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 4906447239778300150L;

  @Column(nullable = false)
  protected java.lang.String cdproduc;

  @Column(nullable = false)
  protected java.math.BigDecimal nuctapro;

  public Tcli0hcoId() {

  }

  public Tcli0hcoId(String cdproduc, BigDecimal nuctapro) {
    super();
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
  }

  public java.lang.String getCdproduc() {
    return cdproduc;
  }

  public java.math.BigDecimal getNuctapro() {
    return nuctapro;
  }

  public void setCdproduc(java.lang.String cdproduc) {
    this.cdproduc = cdproduc;
  }

  public void setNuctapro(java.math.BigDecimal nuctapro) {
    this.nuctapro = nuctapro;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdproduc == null) ? 0 : cdproduc.hashCode());
    result = prime * result + ((nuctapro == null) ? 0 : nuctapro.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tcli0hcoId other = (Tcli0hcoId) obj;
    if (cdproduc == null) {
      if (other.cdproduc != null)
        return false;
    }
    else if (!cdproduc.equals(other.cdproduc))
      return false;
    if (nuctapro == null) {
      if (other.nuctapro != null)
        return false;
    }
    else if (!nuctapro.equals(other.nuctapro))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tcli0hcoId [cdproduc=%s, nuctapro=%s]", cdproduc, nuctapro);
  }

}
