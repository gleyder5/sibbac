package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0eot;
import sibbac.database.megbpdta.model.Fmeg0eotId;

@Repository
public interface Fmeg0eotDao extends JpaRepository<Fmeg0eot, Fmeg0eotId> {

  @Query("select max(e.id.nusecuen) from Fmeg0eot e where e.id.cdident1 = :cdident1")
  BigDecimal maxIdNusecuenByIdCdident1(@Param("cdident1") String cdident1);
  
  @Query("select e.cdidenti from Fmeg0eot e where e.id.cdident1 = :cdident1 and e.tpidtype = :tpidtype")
  List<String> findAllCdidentiByCdident1AndTpidtype(@Param("cdident1") String cdident1, @Param("tpidtype") String tpidtype);

  @Query("select e from Fmeg0eot e where e.id.cdident1 = :cdident1 and e.tpidtype = :tpidtype")
  List<Fmeg0eot> findAllByCdident1AndTpidtype(@Param("cdident1") String cdident1, @Param("tpidtype") String tpidtype);

}
