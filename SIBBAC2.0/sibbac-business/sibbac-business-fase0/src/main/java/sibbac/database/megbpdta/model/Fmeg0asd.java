package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0ASD. Alias-Setllement Detail
 * Documentación para FMEG0ASD <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0ASD", schema = "MEGBPDTA")
public class Fmeg0asd implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = -3130130083867142356L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdaliass", column = @Column(name = "cdaliass", nullable = false)),
      @AttributeOverride(name = "nusecael", column = @Column(name = "nusecael", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0asdId id;

  /**
   * Table Field TPCLEMOD. clearingMode MEGARA: clearingMode. Se accede al
   * fichero de conversion (Si su valor es SOCIEDAD, cambiar a S en el AS400, si
   * es RECTORA, cambiar a R en el AS400) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpclemod;
  /**
   * Table Field CDCUSTOD. custodian MEGARA: custodian FIDESSA:
   * ELIGIBLE_CUST_ENTITY <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcustod;
  /**
   * Table Field ISSENDCH. sendComToCH MEGARA: sendComToCH (Si su valor es true,
   * cambiar a 1, si es false o blancos, cambiar a 0) <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String issendch;
  /**
   * Table Field ISMANCOM. chManageCom MEGARA: chManageCom (Si su valor es true,
   * cambiar a 1, si es false o blancos, cambiar a 0)c <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ismancom;
  /**
   * Relation 1..1 with table FMEG0AEL. Constraint de relación entre FMEG0ASD y
   * FMEG0AEL <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "cdaliass", referencedColumnName = "cdaliass", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "nusecael", referencedColumnName = "nusecael", nullable = false, insertable = false, updatable = false) })
  protected Fmeg0ael fmeg0ael;

  public Fmeg0asdId getId() {
    return id;
  }

  public java.lang.String getTpclemod() {
    return tpclemod;
  }

  public java.lang.String getCdcustod() {
    return cdcustod;
  }

  public java.lang.String getIssendch() {
    return issendch;
  }

  public java.lang.String getIsmancom() {
    return ismancom;
  }

  public Fmeg0ael getFmeg0ael() {
    return fmeg0ael;
  }

  public void setId(Fmeg0asdId id) {
    this.id = id;
  }

  public void setTpclemod(java.lang.String tpclemod) {
    this.tpclemod = tpclemod;
  }

  public void setCdcustod(java.lang.String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public void setIssendch(java.lang.String issendch) {
    this.issendch = issendch;
  }

  public void setIsmancom(java.lang.String ismancom) {
    this.ismancom = ismancom;
  }

  public void setFmeg0ael(Fmeg0ael fmeg0ael) {
    this.fmeg0ael = fmeg0ael;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s|%s", id.getCdaliass(), id.getNusecael(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0ASD";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0asd [id=%s, tpclemod=%s, cdcustod=%s, issendch=%s, ismancom=%s]", id, tpclemod,
        cdcustod, issendch, ismancom);
  }
  
  
}