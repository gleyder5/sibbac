package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table TGRL0EJE. Tabla Ejecuciones Documentación de
 * la tablaTGRL0EJE <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TGRL0EJE")
public class Tgrl0eje implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -1570527739648912890L;
  @AttributeOverrides({
      @AttributeOverride(name = "nuoprout", column = @Column(name = "NUOPROUT", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nupareje", column = @Column(name = "NUPAREJE", nullable = false, length = 4, scale = 0)) })
  @EmbeddedId
  private Tgrl0ejeId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "NUOPROUT", nullable = false, insertable = false, updatable = false)
  private Tgrl0ord tgrl0ord;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tgrl0eje")
  private List<Tgrl0cms> apuntesContables = new ArrayList<Tgrl0cms>();
  
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "tgrl0eje")
  private List<Tgrl0dir> direcionesTitulares = new ArrayList<Tgrl0dir>();

  /**
   * Table Field NUOPBOLS. Nº Operación Bolsa Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 15)
  protected java.lang.String nuopbols;
  /**
   * Table Field CDCLIENT. Código cliente Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdclient;
  /**
   * Table Field CDORDENA. Cod. Ordenante Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4)
  protected java.lang.String cdordena;
  /**
   * Table Field CDSUBORD. Cod. Subordenante Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4)
  protected java.lang.String cdsubord;
  /**
   * Table Field CDENTLIQ. Entidad liquidadora Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4)
  protected java.lang.String cdentliq;
  /**
   * Table Field CDISINVV. Valor ISIN Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 12)
  protected java.lang.String cdisinvv;
  /**
   * Table Field NUSERIEV. Serie Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String nuseriev;
  /**
   * Table Field CDBOLSAS. Bolsa Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 4)
  protected java.lang.String cdbolsas;
  /**
   * Table Field TPOPERAC. Tipo operación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpoperac;
  /**
   * Table Field TPMERCAD. Tipo mercado Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpmercad;
  /**
   * Table Field FHEJECUC. Fecha ejecución Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhejecuc;
  /**
   * Table Field HOEJECUC. Hora ejecución Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected Integer hoejecuc;
  /**
   * Table Field TPCONTRA. Tipo contratación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpcontra;
  /**
   * Table Field NUCANEJE. Cantidad ejecutada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 13, scale = 2)
  protected java.math.BigDecimal nucaneje;
  /**
   * Table Field TPCAMBIO. Tipo cambio Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpcambio;
  /**
   * Table Field IMCAMBIO. Importe cambio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 12, scale = 4)
  protected java.math.BigDecimal imcambio;
  /**
   * Table Field IMEFECUP. Importe efectivo cupón Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 11, scale = 4)
  protected java.math.BigDecimal imefecup;
  /**
   * Table Field IMEFECTI. Importe efectivo Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 15, scale = 2)
  protected java.math.BigDecimal imefecti;
  /**
   * Table Field PCCOMISN. % Comisión Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 6, scale = 3)
  protected java.math.BigDecimal pccomisn;
  /**
   * Table Field IMBONIFI. Bonificación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imbonifi;
  /**
   * Table Field IMDERLIQ. Derechos liquidación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imderliq;
  /**
   * Table Field IMDERCON. Derechos contratación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imdercon;
  /**
   * Table Field IMCANGES. Derechos gestión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imcanges;
  /**
   * Table Field IMTIMBRE. Timbre Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imtimbre;
  /**
   * Table Field HOCONFIR. Hora confirmación. Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected Integer hoconfir;
  /**
   * Table Field CDESTADO. Cod. Estado Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdestado;
  /**
   * Table Field CDSISLIV. Sistema liquidación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdsisliv;
  /**
   * Table Field CDCLASEV. Clase valor Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String cdclasev;
  /**
   * Table Field CDGRCOMV. Grupo comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdgrcomv;
  /**
   * Table Field NUCANTLI. Cantidad liquidada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 11, scale = 0)
  protected java.math.BigDecimal nucantli;
  /**
   * Table Field IMEFECLI. Efectivo liquidado Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 15, scale = 2)
  protected java.math.BigDecimal imefecli;
  /**
   * Table Field IMBONILI. Bonificación liquidada Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imbonili;
  /**
   * Table Field IMDLIQLI. Dere. Liquidación liquidados Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imdliqli;
  /**
   * Table Field IMDCONLI. contratación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imdconli;
  /**
   * Table Field IMDGEBLI. gestión Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imdgebli;
  /**
   * Table Field IMTIMBLI. timbre Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 2)
  protected java.math.BigDecimal imtimbli;
  /**
   * Table Field FHRECTIF. F. movimiento Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhrectif;
  /**
   * Table Field FHCONCON. F. Entrada contratación en Base de Datos.
   * Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected Integer fhconcon;
  /**
   * Table Field FHLIQBOL. Fecha Liquidación Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhliqbol;
  /**
   * Table Field NUULLIPA. Nº última liquidación. Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected Short nuullipa;
  /**
   * Table Field FHCOCLPA. F. confirmación cliente papel Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhcoclpa;
  /**
   * Table Field FHCOLIBO. F. Entrada liquidación en Base de Datos.
   * Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhcolibo;
  /**
   * Table Field CDLIQBOL. Cod. Liquidación bolsa Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdliqbol;
  /**
   * Table Field CDCOBEFE. Cod. Cobro efectivo. Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdcobefe;
  /**
   * Table Field CDTITULA. Estado titular Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdtitula;
  /**
   * Table Field CDSITDES. Situación desglose Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdsitdes;
  /**
   * Table Field CDINTERN. Código interno bolsa Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7)
  protected java.lang.String cdintern;
  /**
   * Table Field NUPRCLTE. Nº parcial ejecución cliente. Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String nuprclte;
  /**
   * Table Field NUOPCLTE. Nº Operación cliente Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 16)
  protected java.lang.String nuopclte;
  /**
   * Table Field NUOPCATS. Nº operación CATS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer nuopcats;
  /**
   * Table Field TPEJERCI. Tipo ejercicio. Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 2)
  protected java.lang.String tpejerci;
  /**
   * Table Field CDPROCED. Código procedencia Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdproced;
  /**
   * Table Field CDREFBAN. Referencia bancaria Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 16)
  protected java.lang.String cdrefban;
  /**
   * Table Field CDPROTER. P=Propios/T=Terceros Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdproter;
  /**
   * Table Field CDSITSCL. Situación SCL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdsitscl;
  /**
   * Table Field TPOPESTD. Tipo operación estadístico Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected java.lang.String tpopestd;
  /**
   * Table Field CDCOMISN. Quien gestiona la comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdcomisn;
  /**
   * Table Field PCLIMCAM. Porcentaje Límite Cambio Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 4)
  protected java.math.BigDecimal pclimcam;
  /**
   * Table Field PCEFECUP. Porcentaje Efecto Cupón Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 7, scale = 4)
  protected java.math.BigDecimal pcefecup;
  /**
   * Table Field CDMODORD. Código Modo Orden Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdmodord;
  /**
   * Table Field CDGESSCL. Código Gestión SCL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdgesscl;
  /**
   * Table Field CDCLEARE. Clearer Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 20)
  protected java.lang.String cdcleare;
  /**
   * Table Field CDMERCAD. Mercado Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 20)
  protected java.lang.String cdmercad;

  public Tgrl0ejeId getId() {
    return id;
  }

  public Tgrl0ord getTgrl0ord() {
    return tgrl0ord;
  }

  public List<Tgrl0cms> getApuntesContables() {
    return apuntesContables;
  }

  public java.lang.String getNuopbols() {
    return nuopbols;
  }

  public java.lang.String getCdclient() {
    return cdclient;
  }

  public java.lang.String getCdordena() {
    return cdordena;
  }

  public java.lang.String getCdsubord() {
    return cdsubord;
  }

  public java.lang.String getCdentliq() {
    return cdentliq;
  }

  public java.lang.String getCdisinvv() {
    return cdisinvv;
  }

  public java.lang.String getNuseriev() {
    return nuseriev;
  }

  public java.lang.String getCdbolsas() {
    return cdbolsas;
  }

  public Character getTpoperac() {
    return tpoperac;
  }

  public Character getTpmercad() {
    return tpmercad;
  }

  public Integer getFhejecuc() {
    return fhejecuc;
  }

  public Integer getHoejecuc() {
    return hoejecuc;
  }

  public Character getTpcontra() {
    return tpcontra;
  }

  public java.math.BigDecimal getNucaneje() {
    return nucaneje;
  }

  public Character getTpcambio() {
    return tpcambio;
  }

  public java.math.BigDecimal getImcambio() {
    return imcambio;
  }

  public java.math.BigDecimal getImefecup() {
    return imefecup;
  }

  public java.math.BigDecimal getImefecti() {
    return imefecti;
  }

  public java.math.BigDecimal getPccomisn() {
    return pccomisn;
  }

  public java.math.BigDecimal getImbonifi() {
    return imbonifi;
  }

  public java.math.BigDecimal getImderliq() {
    return imderliq;
  }

  public java.math.BigDecimal getImdercon() {
    return imdercon;
  }

  public java.math.BigDecimal getImcanges() {
    return imcanges;
  }

  public java.math.BigDecimal getImtimbre() {
    return imtimbre;
  }

  public Integer getHoconfir() {
    return hoconfir;
  }

  public Character getCdestado() {
    return cdestado;
  }

  public Character getCdsisliv() {
    return cdsisliv;
  }

  public java.lang.String getCdclasev() {
    return cdclasev;
  }

  public Character getCdgrcomv() {
    return cdgrcomv;
  }

  public java.math.BigDecimal getNucantli() {
    return nucantli;
  }

  public java.math.BigDecimal getImefecli() {
    return imefecli;
  }

  public java.math.BigDecimal getImbonili() {
    return imbonili;
  }

  public java.math.BigDecimal getImdliqli() {
    return imdliqli;
  }

  public java.math.BigDecimal getImdconli() {
    return imdconli;
  }

  public java.math.BigDecimal getImdgebli() {
    return imdgebli;
  }

  public java.math.BigDecimal getImtimbli() {
    return imtimbli;
  }

  public Integer getFhrectif() {
    return fhrectif;
  }

  public Integer getFhconcon() {
    return fhconcon;
  }

  public Integer getFhliqbol() {
    return fhliqbol;
  }

  public Short getNuullipa() {
    return nuullipa;
  }

  public Integer getFhcoclpa() {
    return fhcoclpa;
  }

  public Integer getFhcolibo() {
    return fhcolibo;
  }

  public Character getCdliqbol() {
    return cdliqbol;
  }

  public java.lang.String getCdcobefe() {
    return cdcobefe;
  }

  public Character getCdtitula() {
    return cdtitula;
  }

  public Character getCdsitdes() {
    return cdsitdes;
  }

  public java.lang.String getCdintern() {
    return cdintern;
  }

  public java.lang.String getNuprclte() {
    return nuprclte;
  }

  public java.lang.String getNuopclte() {
    return nuopclte;
  }

  public Integer getNuopcats() {
    return nuopcats;
  }

  public java.lang.String getTpejerci() {
    return tpejerci;
  }

  public Character getCdproced() {
    return cdproced;
  }

  public java.lang.String getCdrefban() {
    return cdrefban;
  }

  public Character getCdproter() {
    return cdproter;
  }

  public Character getCdsitscl() {
    return cdsitscl;
  }

  public java.lang.String getTpopestd() {
    return tpopestd;
  }

  public Character getCdcomisn() {
    return cdcomisn;
  }

  public java.math.BigDecimal getPclimcam() {
    return pclimcam;
  }

  public java.math.BigDecimal getPcefecup() {
    return pcefecup;
  }

  public Character getCdmodord() {
    return cdmodord;
  }

  public Character getCdgesscl() {
    return cdgesscl;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  public void setId(Tgrl0ejeId id) {
    this.id = id;
  }

  public void setTgrl0ord(Tgrl0ord tgrl0ord) {
    this.tgrl0ord = tgrl0ord;
  }

  public void setApuntesContables(List<Tgrl0cms> apuntesContables) {
    this.apuntesContables = apuntesContables;
  }

  public void setNuopbols(java.lang.String nuopbols) {
    this.nuopbols = nuopbols;
  }

  public void setCdclient(java.lang.String cdclient) {
    this.cdclient = cdclient;
  }

  public void setCdordena(java.lang.String cdordena) {
    this.cdordena = cdordena;
  }

  public void setCdsubord(java.lang.String cdsubord) {
    this.cdsubord = cdsubord;
  }

  public void setCdentliq(java.lang.String cdentliq) {
    this.cdentliq = cdentliq;
  }

  public void setCdisinvv(java.lang.String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  public void setNuseriev(java.lang.String nuseriev) {
    this.nuseriev = nuseriev;
  }

  public void setCdbolsas(java.lang.String cdbolsas) {
    this.cdbolsas = cdbolsas;
  }

  public void setTpoperac(Character tpoperac) {
    this.tpoperac = tpoperac;
  }

  public void setTpmercad(Character tpmercad) {
    this.tpmercad = tpmercad;
  }

  public void setFhejecuc(Integer fhejecuc) {
    this.fhejecuc = fhejecuc;
  }

  public void setHoejecuc(Integer hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public void setTpcontra(Character tpcontra) {
    this.tpcontra = tpcontra;
  }

  public void setNucaneje(java.math.BigDecimal nucaneje) {
    this.nucaneje = nucaneje;
  }

  public void setTpcambio(Character tpcambio) {
    this.tpcambio = tpcambio;
  }

  public void setImcambio(java.math.BigDecimal imcambio) {
    this.imcambio = imcambio;
  }

  public void setImefecup(java.math.BigDecimal imefecup) {
    this.imefecup = imefecup;
  }

  public void setImefecti(java.math.BigDecimal imefecti) {
    this.imefecti = imefecti;
  }

  public void setPccomisn(java.math.BigDecimal pccomisn) {
    this.pccomisn = pccomisn;
  }

  public void setImbonifi(java.math.BigDecimal imbonifi) {
    this.imbonifi = imbonifi;
  }

  public void setImderliq(java.math.BigDecimal imderliq) {
    this.imderliq = imderliq;
  }

  public void setImdercon(java.math.BigDecimal imdercon) {
    this.imdercon = imdercon;
  }

  public void setImcanges(java.math.BigDecimal imcanges) {
    this.imcanges = imcanges;
  }

  public void setImtimbre(java.math.BigDecimal imtimbre) {
    this.imtimbre = imtimbre;
  }

  public void setHoconfir(Integer hoconfir) {
    this.hoconfir = hoconfir;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  public void setCdsisliv(Character cdsisliv) {
    this.cdsisliv = cdsisliv;
  }

  public void setCdclasev(java.lang.String cdclasev) {
    this.cdclasev = cdclasev;
  }

  public void setCdgrcomv(Character cdgrcomv) {
    this.cdgrcomv = cdgrcomv;
  }

  public void setNucantli(java.math.BigDecimal nucantli) {
    this.nucantli = nucantli;
  }

  public void setImefecli(java.math.BigDecimal imefecli) {
    this.imefecli = imefecli;
  }

  public void setImbonili(java.math.BigDecimal imbonili) {
    this.imbonili = imbonili;
  }

  public void setImdliqli(java.math.BigDecimal imdliqli) {
    this.imdliqli = imdliqli;
  }

  public void setImdconli(java.math.BigDecimal imdconli) {
    this.imdconli = imdconli;
  }

  public void setImdgebli(java.math.BigDecimal imdgebli) {
    this.imdgebli = imdgebli;
  }

  public void setImtimbli(java.math.BigDecimal imtimbli) {
    this.imtimbli = imtimbli;
  }

  public void setFhrectif(Integer fhrectif) {
    this.fhrectif = fhrectif;
  }

  public void setFhconcon(Integer fhconcon) {
    this.fhconcon = fhconcon;
  }

  public void setFhliqbol(Integer fhliqbol) {
    this.fhliqbol = fhliqbol;
  }

  public void setNuullipa(Short nuullipa) {
    this.nuullipa = nuullipa;
  }

  public void setFhcoclpa(Integer fhcoclpa) {
    this.fhcoclpa = fhcoclpa;
  }

  public void setFhcolibo(Integer fhcolibo) {
    this.fhcolibo = fhcolibo;
  }

  public void setCdliqbol(Character cdliqbol) {
    this.cdliqbol = cdliqbol;
  }

  public void setCdcobefe(java.lang.String cdcobefe) {
    this.cdcobefe = cdcobefe;
  }

  public void setCdtitula(Character cdtitula) {
    this.cdtitula = cdtitula;
  }

  public void setCdsitdes(Character cdsitdes) {
    this.cdsitdes = cdsitdes;
  }

  public void setCdintern(java.lang.String cdintern) {
    this.cdintern = cdintern;
  }

  public void setNuprclte(java.lang.String nuprclte) {
    this.nuprclte = nuprclte;
  }

  public void setNuopclte(java.lang.String nuopclte) {
    this.nuopclte = nuopclte;
  }

  public void setNuopcats(Integer nuopcats) {
    this.nuopcats = nuopcats;
  }

  public void setTpejerci(java.lang.String tpejerci) {
    this.tpejerci = tpejerci;
  }

  public void setCdproced(Character cdproced) {
    this.cdproced = cdproced;
  }

  public void setCdrefban(java.lang.String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public void setCdproter(Character cdproter) {
    this.cdproter = cdproter;
  }

  public void setCdsitscl(Character cdsitscl) {
    this.cdsitscl = cdsitscl;
  }

  public void setTpopestd(java.lang.String tpopestd) {
    this.tpopestd = tpopestd;
  }

  public void setCdcomisn(Character cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  public void setPclimcam(java.math.BigDecimal pclimcam) {
    this.pclimcam = pclimcam;
  }

  public void setPcefecup(java.math.BigDecimal pcefecup) {
    this.pcefecup = pcefecup;
  }

  public void setCdmodord(Character cdmodord) {
    this.cdmodord = cdmodord;
  }

  public void setCdgesscl(Character cdgesscl) {
    this.cdgesscl = cdgesscl;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }
  
  

  public List<Tgrl0dir> getDirecionesTitulares() {
    return direcionesTitulares;
  }

  public void setDirecionesTitulares(List<Tgrl0dir> direcionesTitulares) {
    this.direcionesTitulares = direcionesTitulares;
  }

  @Override
  public String toString() {
    return String.format("Tgrl0eje [id=%s]", id);
  }
}