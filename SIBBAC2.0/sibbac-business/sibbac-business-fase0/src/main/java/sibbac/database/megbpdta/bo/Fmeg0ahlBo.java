package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0ahlDao;
import sibbac.database.megbpdta.model.Fmeg0ahl;
import sibbac.database.megbpdta.model.Fmeg0ahlId;

@Service
public class Fmeg0ahlBo extends AbstractFmegBo<Fmeg0ahl, Fmeg0ahlId, Fmeg0ahlDao> {

  @Override
  @Transactional
  public Fmeg0ahl save(Fmeg0ahl entity, boolean isNew) {
    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

}
