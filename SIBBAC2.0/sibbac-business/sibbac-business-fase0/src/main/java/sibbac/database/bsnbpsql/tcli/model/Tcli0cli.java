package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represent the table TCLI0CLI. DATOS DEL CLIENTE Documentación de
 * la tablaTCLI0CLI <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TCLI0CLI")
public class Tcli0cli implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -8026191415815384294L;

  /**
   * Table Field NUCLIENT. Columna importada NUCLIENT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Id
  @Column(nullable = false)
  protected java.math.BigDecimal nuclient;

  /**
   * Table Field NUDNICIF. Columna importada NUDNICIF Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nudnicif;
  /**
   * Table Field NBCLIENT. Columna importada NBCLIENT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbclient;
  /**
   * Table Field NB1APELL. Columna importada NB1APELL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nb1apell;
  /**
   * Table Field NB2APELL. Columna importada NB2APELL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nb2apell;
  /**
   * Table Field TPDOMICI. Columna importada TPDOMICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdomici;
  /**
   * Table Field NBDOMICI. Columna importada NBDOMICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbdomici;
  /**
   * Table Field NUDOMICI. Columna importada NUDOMICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nudomici;
  /**
   * Table Field NBCIUDAD. Columna importada NBCIUDAD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbciudad;
  /**
   * Table Field CDPROVIN. Columna importada CDPROVIN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdprovin;
  /**
   * Table Field NBPROVIN. Columna importada NBPROVIN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nbprovin;
  /**
   * Table Field CDPOSTAL. Columna importada CDPOSTAL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdpostal;
  /**
   * Table Field CDDEPAIS. Columna importada CDDEPAIS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddepais;
  /**
   * Table Field NUTELINT. Columna importada NUTELINT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelint;
  /**
   * Table Field NUTELPRF. Columna importada NUTELPRF Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelprf;
  /**
   * Table Field NUTELEFO. Columna importada NUTELEFO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelefo;
  /**
   * Table Field NUTELEXX. Columna importada NUTELEXX Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelexx;
  /**
   * Table Field NUTELFAX. Columna importada NUTELFAX Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutelfax;
  /**
   * Table Field CDDESEXO. Columna importada CDDESEXO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddesexo;
  /**
   * Table Field FHNACIMI. Columna importada FHNACIMI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhnacimi;
  /**
   * Table Field CDESTCIV. Columna importada CDESTCIV Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdestciv;
  /**
   * Table Field CDNACION. Columna importada CDNACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdnacion;
  /**
   * Table Field CDPAINAC. Columna importada CDPAINAC Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdpainac;
  /**
   * Table Field CDIDIOMA. Columna importada CDIDIOMA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdidioma;
  /**
   * Table Field CDACTIVI. Columna importada CDACTIVI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdactivi;
  /**
   * Table Field CDPROFES. Columna importada CDPROFES Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdprofes;
  /**
   * Table Field CDESTUDI. Columna importada CDESTUDI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdestudi;
  /**
   * Table Field TPSOCIED. Columna importada TPSOCIED Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpsocied;
  /**
   * Table Field CDSECTOR. Columna importada CDSECTOR Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdsector;
  /**
   * Table Field CDEMPLEA. Columna importada CDEMPLEA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdemplea;
  /**
   * Table Field FHALTCLI. Columna importada FHALTCLI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhaltcli;
  /**
   * Table Field FHMODIFI. Columna importada FHMODIFI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhmodifi;
  /**
   * Table Field CDUSRMOD. Columna importada CDUSRMOD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdusrmod;
  /**
   * Table Field CDDEBAJA. Columna importada CDDEBAJA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddebaja;
  /**
   * Table Field FHFINACT. Columna importada FHFINACT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhfinact;
  /**
   * Table Field TPCLIENT. Columna importada TPCLIENT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpclient;
  /**
   * Table Field CDDEPREV. Columna importada CDDEPREV Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddeprev;
  /**
   * Table Field CDCATEGO. Columna importada CDCATEGO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdcatego;
  /**
   * Table Field CDINDCLI. Columna importada CDINDCLI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdindcli;
  
  
  public java.math.BigDecimal getNuclient() {
    return nuclient;
  }
  public java.lang.String getNudnicif() {
    return nudnicif;
  }
  public java.lang.String getNbclient() {
    return nbclient;
  }
  public java.lang.String getNb1apell() {
    return nb1apell;
  }
  public java.lang.String getNb2apell() {
    return nb2apell;
  }
  public java.lang.String getTpdomici() {
    return tpdomici;
  }
  public java.lang.String getNbdomici() {
    return nbdomici;
  }
  public java.lang.String getNudomici() {
    return nudomici;
  }
  public java.lang.String getNbciudad() {
    return nbciudad;
  }
  public java.lang.String getCdprovin() {
    return cdprovin;
  }
  public java.lang.String getNbprovin() {
    return nbprovin;
  }
  public java.lang.String getCdpostal() {
    return cdpostal;
  }
  public java.lang.String getCddepais() {
    return cddepais;
  }
  public java.lang.String getNutelint() {
    return nutelint;
  }
  public java.lang.String getNutelprf() {
    return nutelprf;
  }
  public java.lang.String getNutelefo() {
    return nutelefo;
  }
  public java.lang.String getNutelexx() {
    return nutelexx;
  }
  public java.lang.String getNutelfax() {
    return nutelfax;
  }
  public java.lang.String getCddesexo() {
    return cddesexo;
  }
  public java.math.BigDecimal getFhnacimi() {
    return fhnacimi;
  }
  public java.lang.String getCdestciv() {
    return cdestciv;
  }
  public java.lang.String getCdnacion() {
    return cdnacion;
  }
  public java.lang.String getCdpainac() {
    return cdpainac;
  }
  public java.lang.String getCdidioma() {
    return cdidioma;
  }
  public java.lang.String getCdactivi() {
    return cdactivi;
  }
  public java.lang.String getCdprofes() {
    return cdprofes;
  }
  public java.lang.String getCdestudi() {
    return cdestudi;
  }
  public java.lang.String getTpsocied() {
    return tpsocied;
  }
  public java.lang.String getCdsector() {
    return cdsector;
  }
  public java.lang.String getCdemplea() {
    return cdemplea;
  }
  public java.math.BigDecimal getFhaltcli() {
    return fhaltcli;
  }
  public java.math.BigDecimal getFhmodifi() {
    return fhmodifi;
  }
  public java.lang.String getCdusrmod() {
    return cdusrmod;
  }
  public java.lang.String getCddebaja() {
    return cddebaja;
  }
  public java.math.BigDecimal getFhfinact() {
    return fhfinact;
  }
  public java.lang.String getTpclient() {
    return tpclient;
  }
  public java.lang.String getCddeprev() {
    return cddeprev;
  }
  public java.lang.String getCdcatego() {
    return cdcatego;
  }
  public java.lang.String getCdindcli() {
    return cdindcli;
  }
  public void setNuclient(java.math.BigDecimal nuclient) {
    this.nuclient = nuclient;
  }
  public void setNudnicif(java.lang.String nudnicif) {
    this.nudnicif = nudnicif;
  }
  public void setNbclient(java.lang.String nbclient) {
    this.nbclient = nbclient;
  }
  public void setNb1apell(java.lang.String nb1apell) {
    this.nb1apell = nb1apell;
  }
  public void setNb2apell(java.lang.String nb2apell) {
    this.nb2apell = nb2apell;
  }
  public void setTpdomici(java.lang.String tpdomici) {
    this.tpdomici = tpdomici;
  }
  public void setNbdomici(java.lang.String nbdomici) {
    this.nbdomici = nbdomici;
  }
  public void setNudomici(java.lang.String nudomici) {
    this.nudomici = nudomici;
  }
  public void setNbciudad(java.lang.String nbciudad) {
    this.nbciudad = nbciudad;
  }
  public void setCdprovin(java.lang.String cdprovin) {
    this.cdprovin = cdprovin;
  }
  public void setNbprovin(java.lang.String nbprovin) {
    this.nbprovin = nbprovin;
  }
  public void setCdpostal(java.lang.String cdpostal) {
    this.cdpostal = cdpostal;
  }
  public void setCddepais(java.lang.String cddepais) {
    this.cddepais = cddepais;
  }
  public void setNutelint(java.lang.String nutelint) {
    this.nutelint = nutelint;
  }
  public void setNutelprf(java.lang.String nutelprf) {
    this.nutelprf = nutelprf;
  }
  public void setNutelefo(java.lang.String nutelefo) {
    this.nutelefo = nutelefo;
  }
  public void setNutelexx(java.lang.String nutelexx) {
    this.nutelexx = nutelexx;
  }
  public void setNutelfax(java.lang.String nutelfax) {
    this.nutelfax = nutelfax;
  }
  public void setCddesexo(java.lang.String cddesexo) {
    this.cddesexo = cddesexo;
  }
  public void setFhnacimi(java.math.BigDecimal fhnacimi) {
    this.fhnacimi = fhnacimi;
  }
  public void setCdestciv(java.lang.String cdestciv) {
    this.cdestciv = cdestciv;
  }
  public void setCdnacion(java.lang.String cdnacion) {
    this.cdnacion = cdnacion;
  }
  public void setCdpainac(java.lang.String cdpainac) {
    this.cdpainac = cdpainac;
  }
  public void setCdidioma(java.lang.String cdidioma) {
    this.cdidioma = cdidioma;
  }
  public void setCdactivi(java.lang.String cdactivi) {
    this.cdactivi = cdactivi;
  }
  public void setCdprofes(java.lang.String cdprofes) {
    this.cdprofes = cdprofes;
  }
  public void setCdestudi(java.lang.String cdestudi) {
    this.cdestudi = cdestudi;
  }
  public void setTpsocied(java.lang.String tpsocied) {
    this.tpsocied = tpsocied;
  }
  public void setCdsector(java.lang.String cdsector) {
    this.cdsector = cdsector;
  }
  public void setCdemplea(java.lang.String cdemplea) {
    this.cdemplea = cdemplea;
  }
  public void setFhaltcli(java.math.BigDecimal fhaltcli) {
    this.fhaltcli = fhaltcli;
  }
  public void setFhmodifi(java.math.BigDecimal fhmodifi) {
    this.fhmodifi = fhmodifi;
  }
  public void setCdusrmod(java.lang.String cdusrmod) {
    this.cdusrmod = cdusrmod;
  }
  public void setCddebaja(java.lang.String cddebaja) {
    this.cddebaja = cddebaja;
  }
  public void setFhfinact(java.math.BigDecimal fhfinact) {
    this.fhfinact = fhfinact;
  }
  public void setTpclient(java.lang.String tpclient) {
    this.tpclient = tpclient;
  }
  public void setCddeprev(java.lang.String cddeprev) {
    this.cddeprev = cddeprev;
  }
  public void setCdcatego(java.lang.String cdcatego) {
    this.cdcatego = cdcatego;
  }
  public void setCdindcli(java.lang.String cdindcli) {
    this.cdindcli = cdindcli;
  }
  @Override
  public String toString() {
    return String
        .format(
            "Tcli0cli [nuclient=%s, nudnicif=%s, nbclient=%s, nb1apell=%s, nb2apell=%s, tpdomici=%s, nbdomici=%s, nudomici=%s, nbciudad=%s, cdprovin=%s, nbprovin=%s, cdpostal=%s, cddepais=%s, nutelint=%s, nutelprf=%s, nutelefo=%s, nutelexx=%s, nutelfax=%s, cddesexo=%s, fhnacimi=%s, cdestciv=%s, cdnacion=%s, cdpainac=%s, cdidioma=%s, cdactivi=%s, cdprofes=%s, cdestudi=%s, tpsocied=%s, cdsector=%s, cdemplea=%s, fhaltcli=%s, fhmodifi=%s, cdusrmod=%s, cddebaja=%s, fhfinact=%s, tpclient=%s, cddeprev=%s, cdcatego=%s, cdindcli=%s]",
            nuclient, nudnicif, nbclient, nb1apell, nb2apell, tpdomici, nbdomici, nudomici, nbciudad, cdprovin,
            nbprovin, cdpostal, cddepais, nutelint, nutelprf, nutelefo, nutelexx, nutelfax, cddesexo, fhnacimi,
            cdestciv, cdnacion, cdpainac, cdidioma, cdactivi, cdprofes, cdestudi, tpsocied, cdsector, cdemplea,
            fhaltcli, fhmodifi, cdusrmod, cddebaja, fhfinact, tpclient, cddeprev, cdcatego, cdindcli);
  }
  
  
}