package sibbac.database.megbpdta.bo;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0aadDao;
import sibbac.database.megbpdta.model.Fmeg0aad;
import sibbac.database.megbpdta.model.Fmeg0aadId;

@Service
public class Fmeg0aadBo extends AbstractFmegBo<Fmeg0aad, Fmeg0aadId, Fmeg0aadDao> {

  @Override
  @Transactional
  public Fmeg0aad save(Fmeg0aad entity, boolean isNew) {

    if (entity.getId().getNusecuen() == null) {
      BigDecimal maxNusecuen = dao.maxIdNusecuenByIdCdaliass(entity.getId().getCdaliass());
      if (maxNusecuen == null) {
        maxNusecuen = BigDecimal.ZERO;
      }
      maxNusecuen = maxNusecuen.add(BigDecimal.ONE);
      entity.getId().setNusecuen(maxNusecuen);
    }
    return super.save(entity, isNew);
  }

  public List<Fmeg0aad> findAllByIdCdaliass(String cdaliass) {
    return dao.findAllByIdCdaliass(cdaliass);
  }

}
