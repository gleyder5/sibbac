package sibbac.database.megbpdta.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0ali;

@Repository
public interface Fmeg0aliDao extends JpaRepository<Fmeg0ali, String> {

  @Query(value = "select a from Fmeg0ali a where a.cdbrocli = :cdbrocli and a.tpmodeid != 'D' ")
  List<Fmeg0ali> findAllByCdbrocliAndTpmodeidDistinctD(@Param("cdbrocli") String cdbrocli);

  @Query(value = "select count(*) from Fmeg0ali a where a.cdaliass = :cdaliass and a.tpmodeid != 'D' ")
  Integer countByCdaliassAndTpmodeidDistinctD(@Param("cdaliass") String cdaliass);
}
