package sibbac.database.megbpdta.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0sucDao;
import sibbac.database.megbpdta.model.Fmeg0suc;

@Service
public class Fmeg0sucBo extends AbstractFmegBo<Fmeg0suc, String, Fmeg0sucDao> {

  public List<Fmeg0suc> findAllByCdsubctaAndTpmodeidDistinctD(String cdsubcta) {
    return dao.findAllByCdsubctaAndTpmodeidDistinctD(cdsubcta);
  }

  public Integer countByCdsubctaAndTpmodeidDistinctD(String cdsubcta) {
    return dao.countByCdsubctaAndTpmodeidDistinctD(cdsubcta);
  }

}
