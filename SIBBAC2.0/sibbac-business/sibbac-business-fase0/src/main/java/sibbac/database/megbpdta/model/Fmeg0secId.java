package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class Fmeg0secId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5501133616889216226L;

  @Column(nullable = false)
  protected String cdsubcta;

  /**
   * Table Field NUSECUEN. Contador Secuencial Documentación para NUSECUEN
   * (Contador de secuencia de registros) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nusecuen;
  
  public Fmeg0secId() {
  }

  public Fmeg0secId(String cdsubcta, BigDecimal nusecuen) {
    super();
    this.cdsubcta = cdsubcta;
    this.nusecuen = nusecuen;
  }

  public String getCdsubcta() {
    return cdsubcta;
  }

  public java.math.BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setCdsubcta(String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  public void setNusecuen(java.math.BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdsubcta == null) ? 0 : cdsubcta.hashCode());
    result = prime * result + ((nusecuen == null) ? 0 : nusecuen.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0secId other = (Fmeg0secId) obj;
    if (cdsubcta == null) {
      if (other.cdsubcta != null)
        return false;
    }
    else if (!cdsubcta.equals(other.cdsubcta))
      return false;
    if (nusecuen == null) {
      if (other.nusecuen != null)
        return false;
    }
    else if (!nusecuen.equals(other.nusecuen))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0secId [cdsubcta=%s, nusecuen=%s]", cdsubcta, nusecuen);
  }
  
  
  
  

}
