package sibbac.database.betbtdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Betpfesuc1Id implements Serializable {

 
  /**
   * 
   */
  private static final long serialVersionUID = 5055279944317068251L;

  @Column(nullable = false)
  protected java.lang.String ectoms;

  @Column(nullable = false)
  protected java.math.BigDecimal ectcgl;

  @Column(nullable = false)
  protected java.math.BigDecimal esubcgl;

  @Column(nullable = false)
  protected java.lang.String ectpro;

  @Column(nullable = false)
  protected java.lang.String esubpro;

  public Betpfesuc1Id() {

  }

  public Betpfesuc1Id(String ectoms, BigDecimal ectcgl, BigDecimal esubcgl, String ectpro, String esubpro) {
    super();
    this.ectoms = ectoms;
    this.ectcgl = ectcgl;
    this.esubcgl = esubcgl;
    this.ectpro = ectpro;
    this.esubpro = esubpro;
  }

  public java.lang.String getEctoms() {
    return ectoms;
  }

  public java.math.BigDecimal getEctcgl() {
    return ectcgl;
  }

  public java.math.BigDecimal getEsubcgl() {
    return esubcgl;
  }

  public java.lang.String getEctpro() {
    return ectpro;
  }

  public java.lang.String getEsubpro() {
    return esubpro;
  }

  public void setEctoms(java.lang.String ectoms) {
    this.ectoms = ectoms;
  }

  public void setEctcgl(java.math.BigDecimal ectcgl) {
    this.ectcgl = ectcgl;
  }

  public void setEsubcgl(java.math.BigDecimal esubcgl) {
    this.esubcgl = esubcgl;
  }

  public void setEctpro(java.lang.String ectpro) {
    this.ectpro = ectpro;
  }

  public void setEsubpro(java.lang.String esubpro) {
    this.esubpro = esubpro;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((ectcgl == null) ? 0 : ectcgl.hashCode());
    result = prime * result + ((ectoms == null) ? 0 : ectoms.hashCode());
    result = prime * result + ((ectpro == null) ? 0 : ectpro.hashCode());
    result = prime * result + ((esubcgl == null) ? 0 : esubcgl.hashCode());
    result = prime * result + ((esubpro == null) ? 0 : esubpro.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Betpfesuc1Id other = (Betpfesuc1Id) obj;
    if (ectcgl == null) {
      if (other.ectcgl != null)
        return false;
    }
    else if (!ectcgl.equals(other.ectcgl))
      return false;
    if (ectoms == null) {
      if (other.ectoms != null)
        return false;
    }
    else if (!ectoms.equals(other.ectoms))
      return false;
    if (ectpro == null) {
      if (other.ectpro != null)
        return false;
    }
    else if (!ectpro.equals(other.ectpro))
      return false;
    if (esubcgl == null) {
      if (other.esubcgl != null)
        return false;
    }
    else if (!esubcgl.equals(other.esubcgl))
      return false;
    if (esubpro == null) {
      if (other.esubpro != null)
        return false;
    }
    else if (!esubpro.equals(other.esubpro))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Betpfesuc1Id [ectoms=%s, ectcgl=%s, esubcgl=%s, ectpro=%s, esubpro=%s]", ectoms, ectcgl,
        esubcgl, ectpro, esubpro);
  }

  
  
  
}
