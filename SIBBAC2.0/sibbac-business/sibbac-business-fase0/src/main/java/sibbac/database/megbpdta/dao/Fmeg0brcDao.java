package sibbac.database.megbpdta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0brc;

@Repository
public interface Fmeg0brcDao extends JpaRepository<Fmeg0brc, String> {

}
