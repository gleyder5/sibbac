package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0bhl;
import sibbac.database.megbpdta.model.Fmeg0bhlId;

@Repository
public interface Fmeg0bhlDao extends JpaRepository<Fmeg0bhl, Fmeg0bhlId> {

  @Query("select max(b.id.nusecuen) from Fmeg0bhl b where b.id.cdbrocli = :cdbrocli")
  BigDecimal maxIdNusecuenByIdCdbrocli(@Param("cdbrocli") String cdbrocli);

}
