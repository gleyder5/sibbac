package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TGRL0NOM")
public class Tgrl0nom implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1698790408785189365L;

  @AttributeOverrides({
      @AttributeOverride(name = "nuoprout", column = @Column(name = "NUOPROUT", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nupareje", column = @Column(name = "NUPAREJE", nullable = false, length = 4, scale = 0)),
      @AttributeOverride(name = "nusecnbr", column = @Column(name = "NUSECNBR", nullable = false, length = 3, scale = 0)) })
  @EmbeddedId
  private Tgrl0nomId id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "NUOPROUT", referencedColumnName = "NUOPROUT", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUPAREJE", referencedColumnName = "NUPAREJE", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUSECNBR", referencedColumnName = "NUSECNBR", nullable = false, insertable = false, updatable = false) })
  private Tgrl0dir direccion;

  /**
   * Table Field NBCLIENT. Nombre Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 30, nullable = false)
  protected java.lang.String nbclient;

  /**
   * Table Field NBCLIEN1. 1er apellido Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 50, nullable = false)
  protected java.lang.String nbclien1;

  /**
   * Table Field NBCLIEN2. 2º apellido Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 30, nullable = false)
  protected java.lang.String nbclien2;

  /**
   * Table Field NUDNICIF. DNI/CIF Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 10, nullable = false)
  protected java.lang.String nudnicif;

  /**
   * Table Field TPTIPREP. Titular/representante Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1, nullable = false)
  protected Character tptiprep;

  /**
   * Table Field CDNACTIT. Código Nacionalidad Titular (ISO) Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3, nullable = false)
  protected java.lang.String cdnactit;

  /**
   * Table Field CDDDEBIC. CODIGO BIC Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(length = 11, nullable = false)
  protected java.lang.String cdddebic;

  /**
   * Table Field TPIDENTI. TIPO DE IDENTIFICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1, nullable = false)
  protected Character tpidenti;

  /**
   * Table Field TPSOCIED. INDICADOR PERSONA FISICA JURIDICA Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1, nullable = false)
  protected Character tpsocied;

  /**
   * Table Field TPNACTIT. INDICADOR DE NACIONALIDAD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1, nullable = false)
  protected Character tpnactit;

  public Tgrl0nom() {
  }

  public Tgrl0nomId getId() {
    return id;
  }

  public Tgrl0dir getDireccion() {
    return direccion;
  }

  public java.lang.String getNbclient() {
    return nbclient;
  }

  public java.lang.String getNbclien1() {
    return nbclien1;
  }

  public java.lang.String getNbclien2() {
    return nbclien2;
  }

  public java.lang.String getNudnicif() {
    return nudnicif;
  }

  public Character getTptiprep() {
    return tptiprep;
  }

  public java.lang.String getCdnactit() {
    return cdnactit;
  }

  public java.lang.String getCdddebic() {
    return cdddebic;
  }

  public Character getTpidenti() {
    return tpidenti;
  }

  public Character getTpsocied() {
    return tpsocied;
  }

  public Character getTpnactit() {
    return tpnactit;
  }

  public void setId(Tgrl0nomId id) {
    this.id = id;
  }

  public void setDireccion(Tgrl0dir direccion) {
    this.direccion = direccion;
  }

  public void setNbclient(java.lang.String nbclient) {
    this.nbclient = nbclient;
  }

  public void setNbclien1(java.lang.String nbclien1) {
    this.nbclien1 = nbclien1;
  }

  public void setNbclien2(java.lang.String nbclien2) {
    this.nbclien2 = nbclien2;
  }

  public void setNudnicif(java.lang.String nudnicif) {
    this.nudnicif = nudnicif;
  }

  public void setTptiprep(Character tptiprep) {
    this.tptiprep = tptiprep;
  }

  public void setCdnactit(java.lang.String cdnactit) {
    this.cdnactit = cdnactit;
  }

  public void setCdddebic(java.lang.String cdddebic) {
    this.cdddebic = cdddebic;
  }

  public void setTpidenti(Character tpidenti) {
    this.tpidenti = tpidenti;
  }

  public void setTpsocied(Character tpsocied) {
    this.tpsocied = tpsocied;
  }

  public void setTpnactit(Character tpnactit) {
    this.tpnactit = tpnactit;
  }

  @Override
  public String toString() {
    return "Tgrl0nom [id=" + id + ", nbclient=" + nbclient + ", nbclien1=" + nbclien1 + ", nbclien2=" + nbclien2
        + ", nudnicif=" + nudnicif + ", tptiprep=" + tptiprep + ", cdnactit=" + cdnactit + ", cdddebic=" + cdddebic
        + ", tpidenti=" + tpidenti + ", tpsocied=" + tpsocied + ", tpnactit=" + tpnactit + "]";
  }
  
  

}
