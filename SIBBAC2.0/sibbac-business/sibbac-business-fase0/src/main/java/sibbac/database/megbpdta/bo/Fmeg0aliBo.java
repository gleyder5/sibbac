package sibbac.database.megbpdta.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0aliDao;
import sibbac.database.megbpdta.model.Fmeg0ali;

@Service
public class Fmeg0aliBo extends AbstractFmegBo<Fmeg0ali, String, Fmeg0aliDao> {

  public List<Fmeg0ali> findAllByCdbrocliAndTpmodeidDistinctD(String cdbrocli) {
    return dao.findAllByCdbrocliAndTpmodeidDistinctD(cdbrocli);
  }

  public Integer countByCdaliassAndTpmodeidDistinctD(String cdaliass) {
    return dao.countByCdaliassAndTpmodeidDistinctD(cdaliass);
  }

}
