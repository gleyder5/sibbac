package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0sad;
import sibbac.database.megbpdta.model.Fmeg0sadId;

@Repository
public interface Fmeg0sadDao extends JpaRepository<Fmeg0sad, Fmeg0sadId> {
  @Query(value = "select max(s.id.nusecuen) from Fmeg0sad s where s.id.cdsubcta = :cdsubcta")
  BigDecimal maxIdNusecuenByIdCdsubcta(@Param("cdsubcta") String cdsubcta);

}
