package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0bbm;
import sibbac.database.megbpdta.model.Fmeg0bbmId;

@Repository
public interface Fmeg0bbmDao extends JpaRepository<Fmeg0bbm, Fmeg0bbmId> {
  @Query(value = "select max(b.id.nusecuen) from Fmeg0bbm b where b.id.cdstoexc = :cdstoexc and b.id.cdbroker = :cdbroker")
  BigDecimal maxIdNusecuenByIdCdstoexcAndIdCdbroker(@Param("cdstoexc") String cdstoexc,
      @Param("cdbroker") String cdbroker);

}
