package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Fmeg0fieId implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6745035035713136683L;

  /**
   * Table Field NOMXML. Nombre archivo XML Nombre archivo XML <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nomxml;

  /**
   * Table Field FECPROC. Fecha de proceso Fecha de proceso <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fecproc;
  /**
   * Table Field HORPROC. Hora de proceso Hora de proceso <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal horproc;

  public Fmeg0fieId() {
  }

  public Fmeg0fieId(String nomxml, BigDecimal fecproc, BigDecimal horproc) {
    super();
    this.nomxml = nomxml;
    this.fecproc = fecproc;
    this.horproc = horproc;
  }

  public java.lang.String getNomxml() {
    return nomxml;
  }

  public java.math.BigDecimal getFecproc() {
    return fecproc;
  }

  public java.math.BigDecimal getHorproc() {
    return horproc;
  }

  public void setNomxml(java.lang.String nomxml) {
    this.nomxml = nomxml;
  }

  public void setFecproc(java.math.BigDecimal fecproc) {
    this.fecproc = fecproc;
  }

  public void setHorproc(java.math.BigDecimal horproc) {
    this.horproc = horproc;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fecproc == null) ? 0 : fecproc.hashCode());
    result = prime * result + ((horproc == null) ? 0 : horproc.hashCode());
    result = prime * result + ((nomxml == null) ? 0 : nomxml.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0fieId other = (Fmeg0fieId) obj;
    if (fecproc == null) {
      if (other.fecproc != null)
        return false;
    }
    else if (!fecproc.equals(other.fecproc))
      return false;
    if (horproc == null) {
      if (other.horproc != null)
        return false;
    }
    else if (!horproc.equals(other.horproc))
      return false;
    if (nomxml == null) {
      if (other.nomxml != null)
        return false;
    }
    else if (!nomxml.equals(other.nomxml))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0fieId [nomxml=%s, fecproc=%s, horproc=%s]", nomxml, fecproc, horproc);
  }

}
