package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AAD. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AAD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmeg0astId implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  @Column(nullable = false)
  protected java.lang.String cdaliass;

  @Column(nullable = false)
  protected java.math.BigDecimal nusecuen;

  
  public Fmeg0astId() {
  }
  public Fmeg0astId(String cdaliass, BigDecimal nusecuen) {
    super();
    this.cdaliass = cdaliass;
    this.nusecuen = nusecuen;
  }

  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  public java.math.BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  public void setNusecuen(java.math.BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdaliass == null) ? 0 : cdaliass.hashCode());
    result = prime * result + ((nusecuen == null) ? 0 : nusecuen.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0astId other = (Fmeg0astId) obj;
    if (cdaliass == null) {
      if (other.cdaliass != null)
        return false;
    }
    else if (!cdaliass.equals(other.cdaliass))
      return false;
    if (nusecuen == null) {
      if (other.nusecuen != null)
        return false;
    }
    else if (!nusecuen.equals(other.nusecuen))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0astId [cdaliass=%s, nusecael=%s]", cdaliass, nusecuen);
  }

}