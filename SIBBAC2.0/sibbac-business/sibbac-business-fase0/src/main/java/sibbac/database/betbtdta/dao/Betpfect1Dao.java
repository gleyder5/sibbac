package sibbac.database.betbtdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.betbtdta.model.Betpfect1;
import sibbac.database.betbtdta.model.Betpfect1Id;

@Repository
public interface Betpfect1Dao extends JpaRepository<Betpfect1, Betpfect1Id> {

  @Query(value = "select b.ectcta from Betpfect1 b WHERE b.id.ectcgl = :alias  AND b.ectfba = 0 ORDER BY b.ectfal DESC")
  List<BigDecimal> findAllNuctaproByAlias(@Param("alias") BigDecimal alias);

  @Query(value = "select b.id.ectcgl from Betpfect1 b WHERE b.id.ectpro = :cdproduct AND b.ectcta = :nuctapro AND b.ectfba = 0 ORDER BY b.ectfal DESC")
  List<BigDecimal> findAllAliasByCdproductAndNuctapro(@Param("cdproduct") String cdproduct, @Param("nuctapro") BigDecimal nucntapro);

}
