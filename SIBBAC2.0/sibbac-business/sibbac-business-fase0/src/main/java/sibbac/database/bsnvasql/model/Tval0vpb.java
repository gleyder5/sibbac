package sibbac.database.bsnvasql.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table( schema = "BSNVASQL", name = "TVAL0VPB" )
public class Tval0vpb implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -9126331411970113503L;
	/**
	 * 
	 */
	private String				cdviapub;
	private String				nbviapub;

	@Id
	@Column( name = "CDVIAPUB", nullable = false, length = 2 )
	public String getCdviapub() {
		return this.cdviapub;
	}

	public void setCdviapub( String cdviapub ) {
		this.cdviapub = cdviapub;
	}

	@Column( name = "NBVIAPUB", nullable = false, length = 15 )
	public String getNbviapub() {
		return this.nbviapub;
	}

	public void setNbviapub( String cdiso2po ) {
		this.nbviapub = cdiso2po;
	}

}
