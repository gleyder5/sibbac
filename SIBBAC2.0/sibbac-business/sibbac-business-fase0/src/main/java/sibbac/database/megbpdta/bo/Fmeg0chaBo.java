package sibbac.database.megbpdta.bo;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0chaDao;
import sibbac.database.megbpdta.model.Fmeg0cha;

@Service
public class Fmeg0chaBo extends AbstractFmegBo<Fmeg0cha, String, Fmeg0chaDao> {

}
