package sibbac.database.bsnbpsql.tcli.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tcli.model.Tcli0hco;
import sibbac.database.bsnbpsql.tcli.model.Tcli0hcoId;

@Repository
public interface Tcli0hcoDao extends JpaRepository<Tcli0hco, Tcli0hcoId> {

//  @Query("select count(p) > 0 from Tcli0pcl p where p.id.cdproduc = '008' and p.id.nuctapro = :nuctapro")
//  boolean existsNuctapro(@Param("nuctapro") BigDecimal nuctapro);

}
