package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0sel;
import sibbac.database.megbpdta.model.Fmeg0selId;

@Repository
public interface Fmeg0selDao extends JpaRepository<Fmeg0sel, Fmeg0selId> {

  @Query(value = "select max(s.id.nusecuen) from Fmeg0sel s where s.id.cdsubcta = :cdsubcta")
  BigDecimal maxIdNusecuenByIdCdsubcta(@Param("cdsubcta") String cdsubcta);

  @Query("select a from Fmeg0sel a where a.cdcleare = :cdcleare and a.tpsettle = :tpsettle and a.idenvfid = :idenvfid and a.fmeg0suc.tpmodeid != 'D' ")
  List<Fmeg0sel> findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(@Param("cdcleare") String cdcleare,
      @Param("tpsettle") String tpsettle, @Param("idenvfid") String idenvfid);

  @Query(value = "select s from Fmeg0sel s where s.id.cdsubcta = :cdsubcta")
  List<Fmeg0sel> findAllByIdCdsubcta(@Param("cdsubcta") String cdsubcta);

}
