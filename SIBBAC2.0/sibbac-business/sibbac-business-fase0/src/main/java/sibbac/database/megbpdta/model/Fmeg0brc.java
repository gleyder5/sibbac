package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0BRC. BroClient Documentación para
 * FMEG0BRC <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0BRC", schema = "MEGBPDTA")
public class Fmeg0brc implements Serializable, FmegEntityInterface {
  /**
   * 
   */
  private static final long serialVersionUID = 6728564775941447811L;

  /**
   * Table Field CDBROCLI. identifier MEGARA: identifier FIDESSA:
   * CLIENTGROUP_MNEMONIC
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Id
  @Column(nullable = false)
  protected java.lang.String cdbrocli;

  /**
   * Table Field ISACTCLI. activeClient MEGARA: activeClient (Si su valor es
   * true, cambiar a 1, si es false o blancos, cambiar a 0) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isactcli;
  /**
   * Table Field TPDESPER. deactivationPeriodicity MEGARA:
   * deactivationPeriodicity. Se accede al fcihero de conversion. (Si su valor
   * es Monthly, cambiar a M en el AS400) (Si es Quarterly, cambiar a Q en el
   * AS400) (Si es Twice a Year, cambiar a TW en el AS400) (Si es Yearly,
   * cambiar a Y en el AS400) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdesper;
  /**
   * Table Field DESCRIPT. description MEGARA: description FIDESSA:
   * CLIENTGROUP_NAME
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String descript;
  /**
   * Table Field ISAUTHOR. hasAuthorization MEGARA: hasAuthorization (Si su
   * valor es true, cambiar a 1, si es false o blancos, cambiar a 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isauthor;
  /**
   * Table Field ISRISKAU. hasRiskAuthorization MEGARA: hasRiskAuthorization (Si
   * su valor es true, cambiar a 1, si es false o blancos cambiar a 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String isriskau;
  /**
   * Table Field TPRESIDE. residenceType MEGARA: residenceType. Se accede al
   * fichero de conversion (Si su valor es Resident, cambiar a R en el AS400)
   * (Si su valor es Non Resident, cambiar a NR en el AS400) <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpreside;
  /**
   * Table Field TPMANCAT. managementCategory MEGARA: managementCategory. Se
   * accede al fichero de conversion, aunque los valores de Megara son los
   * mismos que los existen en el fichero de conversion. <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmancat;

  /**
   * Table Field MDLORIGI. mediaOfOrigin MEGARA: mediaOfOrigin <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String mdlorigi;
  /**
   * Table Field CDOFFICE. office MEGARA: office <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdoffice;
  /**
   * Table Field TPDESMET. deactivationMethod MEGARA: deactivationMethod.
   * Fichero de conversion. (Si su valor es I, mismo valor en el AS400) (Si su
   * valor es H, mismo valor en el AS400)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdesmet;
  /**
   * Table Field TPDESACT. deactivationType MEGARA: deactivationType. Se accede
   * al fcihero de conversion. (Si su valor es T, mismo valor en el AS400) (si
   * su valor es E, mismo valor en el AS400) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpdesact;
  /**
   * Table Field CTFISCAL. fiscalCategory MEGARA: fiscalCategory <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ctfiscal;
  /**
   * Table Field NUMAXGRO. maxGroupDepth MEGARA: maxGroupDepth <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal numaxgro;
  /**
   * Table Field ISSUBTAX. subjectToTax MEGARA: subjectToTax <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String issubtax;
  /**
   * Table Field CDREFBNK. bankReference MEGARA: bankReference <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdrefbnk;
  /**
   * Table Field CTGMIFID. miFIDCategory MEGARA: miFIDCategory FIDESSA:
   * MIFID_CLASSIFICATION (Si su valor es Countepatr, se cambia por C en el
   * AS400 y ECP para FIDESSA) (Si su valor es Professional, se cambia por P en
   * el AS400 y P para FODESSA) (Si su valor es Retail, se cambia por R en el
   * AS400 y R para FIDESSA) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ctgmifid;
  /**
   * Table Field TPCLIENN. clientNature MEGARA: clientNature. Se accede al
   * fichero de conversion FIDESSA: CLIENT_TYPE (Si en Megara viene instit,
   * cambiar a I en AS400 y cambiar a C para FOIDESSA) (Si en Megara viene
   * retail, camboiar a R en AS400 y cambiar a T para FIDESSA) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpclienn;
  /**
   * Table Field FHDEBAJA. Fecha Baja FIDESSA: STATUS_INDICATOR (Formato
   * aaaammdd) <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String fhdebaja;
  /**
   * Table Field TPMODEID. Modo Operacion Documentación para TPMODEID Valores
   * posibles: I(Input), U(Update), D (Delete) <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpmodeid;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "fmeg0brc")
  protected List<Fmeg0bhl> fmeg0bhls = new ArrayList<Fmeg0bhl>();

  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  public java.lang.String getIsactcli() {
    return isactcli;
  }

  public java.lang.String getTpdesper() {
    return tpdesper;
  }

  public java.lang.String getDescript() {
    return descript;
  }

  public java.lang.String getIsauthor() {
    return isauthor;
  }

  public java.lang.String getIsriskau() {
    return isriskau;
  }

  public java.lang.String getTpreside() {
    return tpreside;
  }

  public java.lang.String getTpmancat() {
    return tpmancat;
  }

  public java.lang.String getMdlorigi() {
    return mdlorigi;
  }

  public java.lang.String getCdoffice() {
    return cdoffice;
  }

  public java.lang.String getTpdesmet() {
    return tpdesmet;
  }

  public java.lang.String getTpdesact() {
    return tpdesact;
  }

  public java.lang.String getCtfiscal() {
    return ctfiscal;
  }

  public java.math.BigDecimal getNumaxgro() {
    return numaxgro;
  }

  public java.lang.String getIssubtax() {
    return issubtax;
  }

  public java.lang.String getCdrefbnk() {
    return cdrefbnk;
  }

  public java.lang.String getCtgmifid() {
    return ctgmifid;
  }

  public java.lang.String getTpclienn() {
    return tpclienn;
  }

  public java.lang.String getFhdebaja() {
    return fhdebaja;
  }

  public java.lang.String getTpmodeid() {
    return tpmodeid;
  }

  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  public void setIsactcli(java.lang.String isactcli) {
    this.isactcli = isactcli;
  }

  public void setTpdesper(java.lang.String tpdesper) {
    this.tpdesper = tpdesper;
  }

  public void setDescript(java.lang.String descript) {
    this.descript = descript;
  }

  public void setIsauthor(java.lang.String isauthor) {
    this.isauthor = isauthor;
  }

  public void setIsriskau(java.lang.String isriskau) {
    this.isriskau = isriskau;
  }

  public void setTpreside(java.lang.String tpreside) {
    this.tpreside = tpreside;
  }

  public void setTpmancat(java.lang.String tpmancat) {
    this.tpmancat = tpmancat;
  }

  public void setMdlorigi(java.lang.String mdlorigi) {
    this.mdlorigi = mdlorigi;
  }

  public void setCdoffice(java.lang.String cdoffice) {
    this.cdoffice = cdoffice;
  }

  public void setTpdesmet(java.lang.String tpdesmet) {
    this.tpdesmet = tpdesmet;
  }

  public void setTpdesact(java.lang.String tpdesact) {
    this.tpdesact = tpdesact;
  }

  public void setCtfiscal(java.lang.String ctfiscal) {
    this.ctfiscal = ctfiscal;
  }

  public void setNumaxgro(java.math.BigDecimal numaxgro) {
    this.numaxgro = numaxgro;
  }

  public void setIssubtax(java.lang.String issubtax) {
    this.issubtax = issubtax;
  }

  public void setCdrefbnk(java.lang.String cdrefbnk) {
    this.cdrefbnk = cdrefbnk;
  }

  public void setCtgmifid(java.lang.String ctgmifid) {
    this.ctgmifid = ctgmifid;
  }

  public void setTpclienn(java.lang.String tpclienn) {
    this.tpclienn = tpclienn;
  }

  public void setFhdebaja(java.lang.String fhdebaja) {
    this.fhdebaja = fhdebaja;
  }

  public void setTpmodeid(java.lang.String tpmodeid) {
    this.tpmodeid = tpmodeid;
  }

  public List<Fmeg0bhl> getFmeg0bhls() {
    return fmeg0bhls;
  }

  public void setFmeg0bhls(List<Fmeg0bhl> fmeg0bhls) {
    this.fmeg0bhls = fmeg0bhls;
  }

  public String getSincronizeId() {
    final String sincronizeId = cdbrocli.trim();
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0BRC";
  }

  @Override
  public String toString() {
    return String
        .format(
            "Fmeg0brc [cdbrocli=%s, isactcli=%s, tpdesper=%s, descript=%s, isauthor=%s, isriskau=%s, tpreside=%s, tpmancat=%s, mdlorigi=%s, cdoffice=%s, tpdesmet=%s, tpdesact=%s, ctfiscal=%s, numaxgro=%s, issubtax=%s, cdrefbnk=%s, ctgmifid=%s, tpclienn=%s, fhdebaja=%s, tpmodeid=%s]",
            cdbrocli, isactcli, tpdesper, descript, isauthor, isriskau, tpreside, tpmancat, mdlorigi, cdoffice,
            tpdesmet, tpdesact, ctfiscal, numaxgro, issubtax, cdrefbnk, ctgmifid, tpclienn, fhdebaja, tpmodeid);
  }
  
  
}