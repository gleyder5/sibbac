package sibbac.database.bsnbpsql.tcli.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represent the table TCLI0PCL. PRODUCTOS/CLIENTE Documentación de
 * la tablaTCLI0PCL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TCLI0PCL")
public class Tcli0pcl implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -141924327236731943L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdproduc", column = @Column(name = "cdproduc", nullable = false)),
      @AttributeOverride(name = "nuclient", column = @Column(name = "nuclient", nullable = false)),
      @AttributeOverride(name = "nuctapro", column = @Column(name = "nuctapro", nullable = false)) })
  protected Tcli0pclId id;

  /**
   * Table Field NUTITULS. Columna importada NUTITULS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nutituls;
  /**
   * Table Field NUPOSICI. Columna importada NUPOSICI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String nuposici;
  /**
   * Table Field FHMODIFI. Columna importada FHMODIFI Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal fhmodifi;
  /**
   * Table Field CDUSRMOD. Columna importada CDUSRMOD Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdusrmod;
  /**
   * Table Field CDDEBAJA. Columna importada CDDEBAJA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cddebaja;
  /**
   * Table Field CDPROGRL. Columna importada CDPROGRL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdprogrl;
  /**
   * Table Field NUCTAGRL. Columna importada NUCTAGRL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.math.BigDecimal nuctagrl;
  /**
   * Table Field TPTIPREP. Columna importada TPTIPREP Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tptiprep;

  public Tcli0pclId getId() {
    return id;
  }

  public java.lang.String getNutituls() {
    return nutituls;
  }

  public java.lang.String getNuposici() {
    return nuposici;
  }

  public java.math.BigDecimal getFhmodifi() {
    return fhmodifi;
  }

  public java.lang.String getCdusrmod() {
    return cdusrmod;
  }

  public java.lang.String getCddebaja() {
    return cddebaja;
  }

  public java.lang.String getCdprogrl() {
    return cdprogrl;
  }

  public java.math.BigDecimal getNuctagrl() {
    return nuctagrl;
  }

  public java.lang.String getTptiprep() {
    return tptiprep;
  }

  public void setId(Tcli0pclId id) {
    this.id = id;
  }

  public void setNutituls(java.lang.String nutituls) {
    this.nutituls = nutituls;
  }

  public void setNuposici(java.lang.String nuposici) {
    this.nuposici = nuposici;
  }

  public void setFhmodifi(java.math.BigDecimal fhmodifi) {
    this.fhmodifi = fhmodifi;
  }

  public void setCdusrmod(java.lang.String cdusrmod) {
    this.cdusrmod = cdusrmod;
  }

  public void setCddebaja(java.lang.String cddebaja) {
    this.cddebaja = cddebaja;
  }

  public void setCdprogrl(java.lang.String cdprogrl) {
    this.cdprogrl = cdprogrl;
  }

  public void setNuctagrl(java.math.BigDecimal nuctagrl) {
    this.nuctagrl = nuctagrl;
  }

  public void setTptiprep(java.lang.String tptiprep) {
    this.tptiprep = tptiprep;
  }

  @Override
  public String toString() {
    return String
        .format(
            "Tcli0pcl [id=%s, nutituls=%s, nuposici=%s, fhmodifi=%s, cdusrmod=%s, cddebaja=%s, cdprogrl=%s, nuctagrl=%s, tptiprep=%s]",
            id, nutituls, nuposici, fhmodifi, cdusrmod, cddebaja, cdprogrl, nuctagrl, tptiprep);
  }

}