package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0eid;
import sibbac.database.megbpdta.model.Fmeg0eidId;

@Repository
public interface Fmeg0eidDao extends JpaRepository<Fmeg0eid, Fmeg0eidId> {

  @Query("select max(e.id.nusecuen) from Fmeg0eid e where e.id.cdident1 = :cdident1")
  BigDecimal maxIdNusecuenByIdCdident1(@Param("cdident1") String cdident1);

}
