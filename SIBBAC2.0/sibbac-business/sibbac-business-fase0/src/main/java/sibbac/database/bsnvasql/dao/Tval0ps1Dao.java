package sibbac.database.bsnvasql.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnvasql.model.Tval0ps1;
import sibbac.database.bsnvasql.model.Tval0ps1Id;

@Repository
public interface Tval0ps1Dao extends JpaRepository<Tval0ps1, Tval0ps1Id> {

  @Query("SELECT pais FROM Tval0ps1 pais WHERE pais.id.fhdebaja = 0 ORDER BY pais.cdiso2po ASC ")
  List<Tval0ps1> findAllActiveOrderByCdiso2po();

  @Query("SELECT pais FROM Tval0ps1 pais WHERE pais.id.fhdebaja = 0 AND pais.cdiso2po = :cdiso2po")
  Tval0ps1 findActiveByCdiso2po(@Param("cdiso2po") String cdiso2po);

  @Query(value = "SELECT CDHPPAIS FROM BSNVASQL.TVAL0PS1 WHERE FHDEBAJA=0", nativeQuery = true)
  List<Object> findAllCodigosActivos();
}
