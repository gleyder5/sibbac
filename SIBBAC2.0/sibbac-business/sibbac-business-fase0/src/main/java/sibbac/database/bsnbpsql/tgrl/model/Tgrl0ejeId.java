package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Tgrl0ejeId implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = -1347891815129750253L;

  /**
   * Table Field NUOPROUT. Nº Operación Routing Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 9, nullable = false)
  protected Integer nuoprout;

  /**
   * Table Field NUPAREJE. Nº Operación de ejecución Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 4, nullable = false)
  protected Short nupareje;

  public Tgrl0ejeId() {
  }

  public Tgrl0ejeId(Integer nuoprout, Short nupareje) {
    super();
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
  }

  public Integer getNuoprout() {
    return nuoprout;
  }

  public Short getNupareje() {
    return nupareje;
  }

  public void setNuoprout(Integer nuoprout) {
    this.nuoprout = nuoprout;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nuoprout == null) ? 0 : nuoprout.hashCode());
    result = prime * result + ((nupareje == null) ? 0 : nupareje.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tgrl0ejeId other = (Tgrl0ejeId) obj;
    if (nuoprout == null) {
      if (other.nuoprout != null)
        return false;
    }
    else if (!nuoprout.equals(other.nuoprout))
      return false;
    if (nupareje == null) {
      if (other.nupareje != null)
        return false;
    }
    else if (!nupareje.equals(other.nupareje))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Tgrl0ejeId %d##%d", nuoprout, nupareje);
  }

}
