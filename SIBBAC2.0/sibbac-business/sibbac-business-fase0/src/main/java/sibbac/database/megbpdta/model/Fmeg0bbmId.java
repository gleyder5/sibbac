package sibbac.database.megbpdta.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AAD. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AAD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmeg0bbmId implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  @Column(nullable = false)
  protected java.lang.String cdstoexc;

  @Column(nullable = false)
  protected String cdbroker;

  @Column(nullable = false)
  protected BigDecimal nusecuen;
  
  public Fmeg0bbmId() {
  }

  public Fmeg0bbmId(String cdstoexc, String cdbroker, BigDecimal nusecuen) {
    super();
    this.cdstoexc = cdstoexc;
    this.cdbroker = cdbroker;
    this.nusecuen = nusecuen;
  }

  public java.lang.String getCdstoexc() {
    return cdstoexc;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setCdstoexc(java.lang.String cdstoexc) {
    this.cdstoexc = cdstoexc;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setNusecuen(BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbroker == null) ? 0 : cdbroker.hashCode());
    result = prime * result + ((cdstoexc == null) ? 0 : cdstoexc.hashCode());
    result = prime * result + ((nusecuen == null) ? 0 : nusecuen.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0bbmId other = (Fmeg0bbmId) obj;
    if (cdbroker == null) {
      if (other.cdbroker != null)
        return false;
    }
    else if (!cdbroker.equals(other.cdbroker))
      return false;
    if (cdstoexc == null) {
      if (other.cdstoexc != null)
        return false;
    }
    else if (!cdstoexc.equals(other.cdstoexc))
      return false;
    if (nusecuen == null) {
      if (other.nusecuen != null)
        return false;
    }
    else if (!nusecuen.equals(other.nusecuen))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0bbmId [cdstoexc=%s, cdbroker=%s, nusecuen=%s]", cdstoexc, cdbroker, nusecuen);
  }
  
  
  

}