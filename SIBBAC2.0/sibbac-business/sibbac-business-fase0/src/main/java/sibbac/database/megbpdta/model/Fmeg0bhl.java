package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class represent the table FMEG0BHL. Broclient-Holders Documentación para
 * FMEG0BHL <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "FMEG0BHL", schema = "MEGBPDTA")
public class Fmeg0bhl implements Serializable, FmegEntityInterface {

  /**
   * 
   */
  private static final long serialVersionUID = -2390483432908786087L;

  @EmbeddedId
  @AttributeOverrides({ @AttributeOverride(name = "cdbrocli", column = @Column(name = "cdbrocli", nullable = false)),
      @AttributeOverride(name = "nusecuen", column = @Column(name = "nusecuen", nullable = false)) })
  protected Fmeg0bhlId id;

  /**
   * Relation 1..1 with table FMEG0BRC. Constraint de relación entre FMEG0BHL y
   * FMEG0BRC <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "cdbrocli", referencedColumnName = "cdbrocli", nullable = false, insertable = false, updatable = false)
  protected Fmeg0brc fmeg0brc;

  /**
   * Table Field CDEXTREF. externalReference MEGARA: externalReference <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdextref;
  /**
   * Table Field ISMAINAL. IsMain MEGARA: IsMain (Si su contenido es true,
   * convertir en 1, si fuese false o blancos, convertir o 0) <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String ismainal;
  /**
   * Table Field PARTPERC. participationPercentage MEGARA:
   * participationPercentage <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String partperc;
  /**
   * Table Field CDHOLDER. holder MEGARA: holder <!-- begin-user-doc --> <!--
   * end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String cdholder;
  /**
   * Table Field TPHOLDER. holderType MEGARA: holderType. Se accede al fichero
   * de conversion. (Si su valor es Holder, cambiar a H en el AS400) (Si su
   * valor Representative, cambiar a R en el AS400) (Si su valor es Ownership
   * Without Usufruct, cambiar a O en el AS400) (Si su valor esUsufructuary,
   * cambiar a U en el AS400)
   * 
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(nullable = false)
  protected java.lang.String tpholder;

  public Fmeg0bhlId getId() {
    return id;
  }

  public Fmeg0brc getFmeg0brc() {
    return fmeg0brc;
  }

  public java.lang.String getCdextref() {
    return cdextref;
  }

  public java.lang.String getIsmainal() {
    return ismainal;
  }

  public java.lang.String getPartperc() {
    return partperc;
  }

  public java.lang.String getCdholder() {
    return cdholder;
  }

  public java.lang.String getTpholder() {
    return tpholder;
  }

  public void setId(Fmeg0bhlId id) {
    this.id = id;
  }

  public void setFmeg0brc(Fmeg0brc fmeg0brc) {
    this.fmeg0brc = fmeg0brc;
  }

  public void setCdextref(java.lang.String cdextref) {
    this.cdextref = cdextref;
  }

  public void setIsmainal(java.lang.String ismainal) {
    this.ismainal = ismainal;
  }

  public void setPartperc(java.lang.String partperc) {
    this.partperc = partperc;
  }

  public void setCdholder(java.lang.String cdholder) {
    this.cdholder = cdholder;
  }

  public void setTpholder(java.lang.String tpholder) {
    this.tpholder = tpholder;
  }

  @Override
  public String getSincronizeId() {
    final String sincronizeId = String.format("%s|%s", id.getCdbrocli(), id.getNusecuen());
    return sincronizeId;
  }

  @Override
  public String getNombreTabla() {
    return "FMEG0BHL";
  }

  @Override
  public String toString() {
    return String.format("Fmeg0bhl [id=%s, cdextref=%s, ismainal=%s, partperc=%s, cdholder=%s, tpholder=%s]", id,
        cdextref, ismainal, partperc, cdholder, tpholder);
  }
  
  
}