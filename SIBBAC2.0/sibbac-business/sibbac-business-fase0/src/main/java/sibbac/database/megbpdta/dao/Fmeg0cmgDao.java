package sibbac.database.megbpdta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0cmg;
import sibbac.database.megbpdta.model.Fmeg0cmgId;

@Repository
public interface Fmeg0cmgDao extends JpaRepository<Fmeg0cmg, Fmeg0cmgId> {

}
