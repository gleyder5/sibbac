package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0aid;
import sibbac.database.megbpdta.model.Fmeg0aidId;

@Repository
public interface Fmeg0aidDao extends JpaRepository<Fmeg0aid, Fmeg0aidId> {
  @Query(value = "select max(a.id.nusecuen) from Fmeg0aid a where a.id.cdaliass = :cdaliass")
  BigDecimal maxIdNusecuenByIdCdaliass(@Param("cdaliass") String cdaliass);

}
