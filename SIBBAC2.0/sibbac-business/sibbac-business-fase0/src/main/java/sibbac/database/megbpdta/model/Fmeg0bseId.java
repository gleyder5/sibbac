package sibbac.database.megbpdta.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class represent the table FMAE0AAD. Informacion maestra para enviar a
 * FIDESSA Documentación para FMAE0AAD <!-- begin-user-doc --> <!-- end-user-doc
 * -->
 **/
@Embeddable
public class Fmeg0bseId implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5774020312539486269L;

  @Column(nullable = false)
  protected java.lang.String cdstoexc;

  @Column(nullable = false)
  protected String cdbroker;
  
  public Fmeg0bseId() {
  }

  public Fmeg0bseId(String cdstoexc, String cdbroker) {
    super();
    this.cdstoexc = cdstoexc;
    this.cdbroker = cdbroker;
  }

  public java.lang.String getCdstoexc() {
    return cdstoexc;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public void setCdstoexc(java.lang.String cdstoexc) {
    this.cdstoexc = cdstoexc;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdbroker == null) ? 0 : cdbroker.hashCode());
    result = prime * result + ((cdstoexc == null) ? 0 : cdstoexc.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Fmeg0bseId other = (Fmeg0bseId) obj;
    if (cdbroker == null) {
      if (other.cdbroker != null)
        return false;
    }
    else if (!cdbroker.equals(other.cdbroker))
      return false;
    if (cdstoexc == null) {
      if (other.cdstoexc != null)
        return false;
    }
    else if (!cdstoexc.equals(other.cdstoexc))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Fmeg0bseId [cdstoexc=%s, cdbroker=%s]", cdstoexc, cdbroker);
  }

  
  
}