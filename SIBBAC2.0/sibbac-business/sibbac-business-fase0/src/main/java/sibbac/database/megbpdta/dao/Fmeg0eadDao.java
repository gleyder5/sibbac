package sibbac.database.megbpdta.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.megbpdta.model.Fmeg0ead;
import sibbac.database.megbpdta.model.Fmeg0eadId;

@Repository
public interface Fmeg0eadDao extends JpaRepository<Fmeg0ead, Fmeg0eadId> {

  @Query("select max(e.id.nusecuen) from Fmeg0ead e where e.id.cdident1 = :cdident1")
  BigDecimal maxIdNusecuenByIdCdident1(@Param("cdident1") String cdident1);

}
