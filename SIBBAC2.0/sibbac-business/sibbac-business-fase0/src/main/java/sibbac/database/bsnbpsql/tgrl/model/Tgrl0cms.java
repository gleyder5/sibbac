package sibbac.database.bsnbpsql.tgrl.model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class represent the table TGRL0CMS. Tabla Comisiones Documentación de la
 * tablaTGRL0CMS <!-- begin-user-doc --> <!-- end-user-doc -->
 **/
@Entity
@Table(name = "TGRL0CMS")
public class Tgrl0cms implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -9143851940520524741L;

  @AttributeOverrides({
      @AttributeOverride(name = "nuoprout", column = @Column(name = "NUOPROUT", nullable = false, length = 9, scale = 0)),
      @AttributeOverride(name = "nupareje", column = @Column(name = "NUPAREJE", nullable = false, length = 4, scale = 0)),
      @AttributeOverride(name = "nuregcom", column = @Column(name = "NUREGCOM", nullable = false, length = 3, scale = 0)) })
  @EmbeddedId
  private Tgrl0cmsId id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumns(value = {
      @JoinColumn(name = "NUOPROUT", referencedColumnName = "NUOPROUT", nullable = false, insertable = false, updatable = false),
      @JoinColumn(name = "NUPAREJE", referencedColumnName = "NUPAREJE", nullable = false, insertable = false, updatable = false) })
  private Tgrl0eje tgrl0eje;

  /**
   * Table Field CDSVBBRK. Indicativo comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdsvbbrk;
  /**
   * Table Field TPREGCOM. Tipo registro comisión Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character tpregcom;
  /**
   * Table Field IMREGCOM. Importe Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 11, scale = 2)
  protected java.math.BigDecimal imregcom;
  /**
   * Table Field FHMOVCOM. F. Movimiento, liquidaciones, procesos Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhmovcom;
  /**
   * Table Field FHCOLICM. F. entrada del registro en la B.Datos Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 6)
  protected Integer fhcolicm;
  /**
   * Table Field CDESTADO. Estado Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   **/
  @Column(length = 1)
  protected Character cdestado;
  /**
   * Table Field CDUSRCOM. Usuario que realiza el alta Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected java.lang.String cdusrcom;
  /**
   * Table Field FHENTCON. F. entrada en contabilidad Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 8)
  protected Integer fhentcon;
  /**
   * Table Field CDMONISO. Columna importada CDMONISO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 3)
  protected java.lang.String cdmoniso;
  /**
   * Table Field IMREGDIV. Columna importada IMREGDIV Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 11, scale = 2)
  protected java.math.BigDecimal imregdiv;
  /**
   * Table Field CDBROKER. Columna importada CDBROKER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 20)
  protected java.lang.String cdbroker;
  /**
   * Table Field CDCLEARE. Columna importada CDCLEARE Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   **/
  @Column(length = 15)
  protected java.lang.String cdcleare;

  public Tgrl0cmsId getId() {
    return id;
  }

  public Tgrl0eje getTgrl0eje() {
    return tgrl0eje;
  }

  public Character getCdsvbbrk() {
    return cdsvbbrk;
  }

  public Character getTpregcom() {
    return tpregcom;
  }

  public java.math.BigDecimal getImregcom() {
    return imregcom;
  }

  public Integer getFhmovcom() {
    return fhmovcom;
  }

  public Integer getFhcolicm() {
    return fhcolicm;
  }

  public Character getCdestado() {
    return cdestado;
  }

  public java.lang.String getCdusrcom() {
    return cdusrcom;
  }

  public Integer getFhentcon() {
    return fhentcon;
  }

  public java.lang.String getCdmoniso() {
    return cdmoniso;
  }

  public java.math.BigDecimal getImregdiv() {
    return imregdiv;
  }

  public java.lang.String getCdbroker() {
    return cdbroker;
  }

  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  public void setId(Tgrl0cmsId id) {
    this.id = id;
  }

  public void setTgrl0eje(Tgrl0eje tgrl0eje) {
    this.tgrl0eje = tgrl0eje;
  }

  public void setCdsvbbrk(Character cdsvbbrk) {
    this.cdsvbbrk = cdsvbbrk;
  }

  public void setTpregcom(Character tpregcom) {
    this.tpregcom = tpregcom;
  }

  public void setImregcom(java.math.BigDecimal imregcom) {
    this.imregcom = imregcom;
  }

  public void setFhmovcom(Integer fhmovcom) {
    this.fhmovcom = fhmovcom;
  }

  public void setFhcolicm(Integer fhcolicm) {
    this.fhcolicm = fhcolicm;
  }

  public void setCdestado(Character cdestado) {
    this.cdestado = cdestado;
  }

  public void setCdusrcom(java.lang.String cdusrcom) {
    this.cdusrcom = cdusrcom;
  }

  public void setFhentcon(Integer fhentcon) {
    this.fhentcon = fhentcon;
  }

  public void setCdmoniso(java.lang.String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setImregdiv(java.math.BigDecimal imregdiv) {
    this.imregdiv = imregdiv;
  }

  public void setCdbroker(java.lang.String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  @Override
  public String toString() {
    return String.format("Tgrl0cms [id=%s]", id);
  }

}