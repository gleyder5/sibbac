package sibbac.database.megbpdta.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmae0as4Dao;
import sibbac.database.megbpdta.model.Fmae0as4;
import sibbac.database.megbpdta.model.Fmae0as4Id;

@Service
public class Fmae0as4Bo extends AbstractFmegBo<Fmae0as4, Fmae0as4Id, Fmae0as4Dao> {

  public List<Fmae0as4> findAllOrderByIdFhprocesAndHrproces() {
    return dao.findAllOrderByIdFhprocesAndHrproces();
  }

}
