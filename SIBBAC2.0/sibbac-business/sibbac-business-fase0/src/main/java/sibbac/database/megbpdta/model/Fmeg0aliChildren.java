package sibbac.database.megbpdta.model;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Fmeg0aliChildren {

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "cdaliass", referencedColumnName = "cdaliass", nullable = false, insertable = false, updatable = false)
  protected Fmeg0ali fmeg0ali;

  public Fmeg0ali getFmeg0ali() {
    return fmeg0ali;
  }

  public void setFmeg0ali(Fmeg0ali fmeg0ali) {
    this.fmeg0ali = fmeg0ali;
  }

}
