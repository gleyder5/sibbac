package sibbac.business.fase0.database;


// Stati
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import sibbac.business.fase0.database.bo.Tmct0MensajesBo;
// import sibbac.business.fase0.database.bo.Tmct0MensajesBoR00;
import sibbac.business.fase0.database.model.Tmct0Mensajes;


// Internal

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@Sql( "/loadNotificaciones.sql" )
public class TestMensajes extends ADBTest {

	@Autowired
	private Tmct0MensajesBo	boMensajes;

	/*
	 * @Inject
	 * private Tmct0MensajesR00Bo boMensajesR00;
	 */

	@Test
	// @Transactional
			public
			void test10Mensajes() {

		LOG.debug( "Looking for the last 10 messages..." );
		Page< Tmct0Mensajes > messages = boMensajes.getLast10Messages();
		LOG.debug( "GOT [{}]", messages );
		assertNotNull( "NULL messages received!", messages );
		assertTrue( "EMPTY messages received!", messages != null );
		// assertFalse( "EMPTY messages received!", messages.isEmpty() );
		LOG.debug( "# Mensajes: {}/{}", messages.getTotalElements(), messages.getTotalPages() );

		for ( Tmct0Mensajes m : messages ) {
			LOG.debug( "+ Mensaje: {}: {}", m.getIdMensaje(), m );
		}

	}

}
