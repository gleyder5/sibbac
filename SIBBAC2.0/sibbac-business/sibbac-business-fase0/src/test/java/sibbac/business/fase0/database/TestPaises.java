package sibbac.business.fase0.database;


import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import sibbac.business.fase0.database.bo.Tval0ps1Bo;
import sibbac.business.fase0.database.bo.Tval0ps2Bo;
import sibbac.database.bsnvasql.model.Tval0ps1;
import sibbac.database.bsnvasql.model.Tval0ps2;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
@SuppressWarnings( "unused" )
public class TestPaises extends ADBTest {

	@Autowired
	private Tval0ps1Bo	tval0ps1bo;
	
	@Autowired
	private Tval0ps2Bo	tval0ps2bo;
	

	@Test
	@Ignore
	public void traerPaisPorCodigo() {
		Tval0ps1 pais = tval0ps1bo.findByCdhppais("334");
		pais.getCdiso2po();
	}
	

	@Test
	public void traducirPais() {
		Tval0ps1 pais = tval0ps1bo.findByCdhppais("XXX");
		Tval0ps2 pais2 = tval0ps2bo.getTval0ps2FromTval0ps1(pais);
		pais2.getCdiso2po();
	}
	

	@Test
	@Ignore
	public void traducirPaisInversa() {
		Tval0ps2 pais = tval0ps2bo.findByCdispais( "724" );
		Tval0ps1 pais1 = tval0ps1bo.getTval0ps1FromTval0ps2(pais);
		pais1.getCdiso2po();
	}
}
