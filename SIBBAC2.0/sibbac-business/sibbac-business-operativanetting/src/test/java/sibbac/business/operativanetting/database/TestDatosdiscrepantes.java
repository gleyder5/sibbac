package sibbac.business.operativanetting.database;


// Internal
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// Java
// JUnit
// Spring

import sibbac.business.operativanetting.database.bo.DatosdiscrepantesBo;
import sibbac.business.operativanetting.database.model.Datosdiscrepantes;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestDatosdiscrepantes extends ADBTest {

	@Autowired
	private DatosdiscrepantesBo	BoDatosdiscrepante	= null;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestRealizarDatosDiscrepante() {

		LOG.debug( "Starting..." );

		for ( Datosdiscrepantes datos_discrepante : BoDatosdiscrepante.findAll() ) {
			LOG.debug( datos_discrepante.getCodigo() + " " + datos_discrepante.getDescripcion_es() );
		}

		LOG.debug( "Se inserta un nuevo registro de S3Errores" );

	}

}
