package sibbac.business.operativanetting.tasks;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class TaskInteresesFinanciacionTest.
 */
public class TaskInteresesFinanciacionTest extends AbstractTest{
	
	/** The task intereses financiacion. */
	@Autowired
	private TaskInteresesFinanciacion taskInteresesFinanciacion;
	
	/**
	 * Execute.
	 */
	@Test
	public void execute(){
		taskInteresesFinanciacion.execute();
	}

}
