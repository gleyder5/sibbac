package sibbac.business.operativanetting.tasks;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class TaskRespuestaFicheroMegaraTest.
 */
public class TaskRespuestaFicheroMegaraTest extends AbstractTest {
	
	/** The task respuesta fichero megara. */
	@Autowired
	private TaskRespuestaFicheroMegara taskRespuestaFicheroMegara; 
	
	/**
	 * Execute.
	 */
	@Test
	public void execute(){
		taskRespuestaFicheroMegara.execute();
	}

	
}
