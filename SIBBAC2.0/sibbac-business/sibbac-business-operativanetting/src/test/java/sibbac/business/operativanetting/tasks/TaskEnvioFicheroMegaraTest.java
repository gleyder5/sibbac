package sibbac.business.operativanetting.tasks;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class TaskEnvioFicheroMegaraTest.
 */
public class TaskEnvioFicheroMegaraTest extends AbstractTest {
	
	/** The task envio fichero megara. */
	@Autowired
	private TaskEnvioFicheroMegara taskEnvioFicheroMegara;
	
	/**
	 * Execute.
	 */
	@Test
	public void execute(){
		taskEnvioFicheroMegara.execute();
	}

}
