package sibbac.business.operativanetting.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// Java
// JUnit
// Spring



import sibbac.business.wrappers.database.bo.S3LiquidacionBo;
import sibbac.business.wrappers.database.model.S3Liquidacion;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestS3Liquidacion extends ADBTest {

	@Autowired
	private S3LiquidacionBo	BoS3Liquidacion	= null;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestRealizarS3Liquidacion() {

		LOG.debug( "Starting..." );

		S3Liquidacion datos_liquidacion = new S3Liquidacion( "IDFICHERO", 'C', "NBOOKING001", "NUCNFCLT0001", ( short ) 2, 'A', "PDL",
				new Date(), new Date(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, new Date() );

		BoS3Liquidacion.save( datos_liquidacion );

		LOG.debug( "Se inserta un nuevo registro de S3Liquidacion" );

		assertNotNull( "Error al autogenerar la clave de Tmct0S3Liquidacion", datos_liquidacion.getId() );

		Long id = datos_liquidacion.getId();

		S3Liquidacion recuperar_datos = BoS3Liquidacion.findById( id );

		recuperar_datos.setFechaOperacion( new Date() );
		recuperar_datos.setContabilizado( 'S' );

		BoS3Liquidacion.save( recuperar_datos );

		LOG.debug( "Se actualiza los campos" );

		assertTrue( "Error al actualizar los campos", id.equals( recuperar_datos.getId() ) );

	}

}
