package sibbac.business.operativanetting.rest;


// Internal
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// Java
// JUnit
// Spring

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.wrappers.database.bo.S3ErroresBo;
import sibbac.business.wrappers.database.bo.S3LiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.business.wrappers.database.model.S3Liquidacion;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestService extends ADBTest {

	@Autowired
	private S3LiquidacionBo	BoS3Liquidacion	= null;

	@Autowired
	private S3ErroresBo		BoS3Errores		= null;

	@Autowired
	private Tmct0aliBo		BoTmct0ali		= null;

	@Autowired
	private Tmct0ValoresBo	BoTmct0Valores	= null;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestTitulosSinMovimientos() {

		// String alias = "";
		//
		//
		// List<Netting> lista_titulos_sin_movimiento = BoNetting.findByTitulosSinMovimiento(alias);
		//
		//
		// Map<String, Object> map = new HashMap<String,Object>();
		// List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		// Map<String, Object> bean;
		// map.put(Constantes.RESULT_LISTA, list);
		// for (Netting titulos_sin_movimiento : lista_titulos_sin_movimiento) {
		// list.add(bean = new HashMap<String, Object>());
		// bean.put("fechaoperacion", titulos_sin_movimiento.getFechaOperacion());
		// bean.put("fechacontratacion", titulos_sin_movimiento.getFechaContratacion());
		// bean.put("cdalias", titulos_sin_movimiento.getCdalias());
		// bean.put("camaracompensacion", titulos_sin_movimiento.getCamaraCompensacion());
		//
		// int resultado_precio = titulos_sin_movimiento.getEfectivoCompra().compareTo(titulos_sin_movimiento.getEfectivoVenta());
		// // el efectivo de la compra es mayor
		// if (resultado_precio == 1){
		// bean.put("sentido", Constantes.COMPRA);
		// }else{
		// bean.put("sentido", Constantes.VENTA);
		// }
		//
		// bean.put("cdisin", titulos_sin_movimiento.getCdisin());
		// bean.put("nutiuloneteado", titulos_sin_movimiento.getNutituloNeteado());
		// bean.put("efectivoneteado", titulos_sin_movimiento.getEfectivoNeteado());
		// bean.put("corretajeneteado", titulos_sin_movimiento.getCorretajeNeteado());
		// bean.put("contabilizado", titulos_sin_movimiento.getContabilizado());
		// bean.put("fechacontabilizado", titulos_sin_movimiento.getFechaContabilidad());
		//
		// SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-DD");
		// String fechaneteo = formatter.format(titulos_sin_movimiento.getAuditDate());
		// bean.put("fechaneteo", fechaneteo);
		// }
		//

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestErrores() {

		List< S3Errores > lista_errores = null;

		Long idLiquidacion = new Long( 29 );

		lista_errores = BoS3Errores.findByIdLiquidacion( idLiquidacion );

		Map< String, Object > map = new HashMap< String, Object >();
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > bean;
		map.put( Constantes.RESULT_LISTA, list );
		for ( S3Errores datos_errores_s3 : lista_errores ) {
			list.add( bean = new HashMap< String, Object >() );

			bean.put( "datoerrorsv", datos_errores_s3.getDatoErrorSV() );
			bean.put( "datoerrorcliente", datos_errores_s3.getDatoErrorCliente() );
			bean.put( "dserror", datos_errores_s3.getDserror() );

		}

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestLiquidacion() {

		List< S3Liquidacion > lista_liquidacion = null;

		String nbooking = "00000474027TRMA1";
		String nucnflct = "00000070831CAMA1";
		short ncnfliq = 1;

		lista_liquidacion = BoS3Liquidacion.findByNbookingAndNucnflictAndNcnfliq( nbooking, nucnflct, ncnfliq );

		Map< String, Object > map = new HashMap< String, Object >();
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > bean;
		map.put( Constantes.RESULT_LISTA, list );
		for ( S3Liquidacion datos_liquidacion_s3 : lista_liquidacion ) {
			list.add( bean = new HashMap< String, Object >() );
			bean.put( "idliquidacion", datos_liquidacion_s3.getId() );
			bean.put( "cdestados3", datos_liquidacion_s3.getCdestadoS3() );
			bean.put( "fechaoperacion", datos_liquidacion_s3.getFechaOperacion() );
			bean.put( "fechavalor", datos_liquidacion_s3.getFechaValor() );
			bean.put( "nutiuloliquidados", datos_liquidacion_s3.getNutituloLiquidados() );
			bean.put( "importenetoliquidado", datos_liquidacion_s3.getImporteNetoLiquidado() );
			bean.put( "contabilizado", datos_liquidacion_s3.getContabilizado() );
			bean.put( "fechacontabilidad", datos_liquidacion_s3.getFechaContabilidad() );
			bean.put( "importeefectivo", datos_liquidacion_s3.getImporteEfectivo() );
			bean.put( "importecorretaje", datos_liquidacion_s3.getImporteCorretaje() );
			SimpleDateFormat formatter = new SimpleDateFormat( "YYYY-MM-DD" );
			String fechaliquidacion = formatter.format( datos_liquidacion_s3.getAuditDate() );
			bean.put( "fechaliquidacion", fechaliquidacion );

		}

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestServiceAliasNetting() {

		List< String > lista_alias = BoTmct0ali.findAliasNetting();
		String id, descrali;

		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();

		for ( String alias : lista_alias ) {

			id = alias.substring( 0, alias.indexOf( "|" ) ).trim();
			descrali = alias.substring( alias.indexOf( "|" ) + 1 ).trim();

			Map< String, Object > bean = new HashMap< String, Object >();
			bean.put( "id", id );
			bean.put( "descripcion", descrali );
			list.add( bean );

		}

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestServiceIsin() {

		List< String > lista_isin = BoTmct0Valores.findIsin();
		String id, descripcion;

		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		for ( String isin : lista_isin ) {
			id = isin.substring( 0, isin.indexOf( "|" ) ).trim();
			descripcion = isin.substring( isin.indexOf( "|" ) + 1 ).trim();
			Map< String, Object > bean = new HashMap< String, Object >();
			bean.put( "id", id );
			bean.put( "descripcion", descripcion );
			list.add( bean );
		}

	}

}
