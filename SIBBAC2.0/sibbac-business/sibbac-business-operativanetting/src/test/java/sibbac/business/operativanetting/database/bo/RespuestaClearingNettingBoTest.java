package sibbac.business.operativanetting.database.bo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RespuestaClearingNettingBoTest {
    
    @Test
    public void referenciaSVAceptableMercadoTest() {
        assertTrue(RespuestaClearingNettingBo.referenciaSVAceptable("M000000012345678"));
    }
    
    @Test
    public void referenciaSVAceptableClienteTest() {
        assertTrue(RespuestaClearingNettingBo.referenciaSVAceptable("C000000012345678"));
    }

    @Test
    public void referenciaSVAceptableMercadoNuevoTest() {
        assertTrue(RespuestaClearingNettingBo.referenciaSVAceptable("MN00000001234567"));
    }
    
    @Test
    public void referenciaSVAceptableClienteNettingTest() {
        assertTrue(RespuestaClearingNettingBo.referenciaSVAceptable("N000000012345678"));
    }
    
    @Test
    public void referenciaSVAceptableRechazaCortaTest() {
        assertFalse(RespuestaClearingNettingBo.referenciaSVAceptable("C00000012345678"));
    }
    
    @Test
    public void referenciaSVAceptableRechazaLargaTest() {
        assertFalse(RespuestaClearingNettingBo.referenciaSVAceptable("C00000000012345678"));
    }
    
    @Test
    public void referenciaSVAceptableRechazaOtraInicialTest() {
        assertFalse(RespuestaClearingNettingBo.referenciaSVAceptable("A000000012345678"));
    }
    
    @Test
    public void referenciaSVAceptableRechazaMercadoOtraInicialTest() {
        assertFalse(RespuestaClearingNettingBo.referenciaSVAceptable("MA00000012345678"));
    }
    
    @Test
    public void referenciaSVAceptableRechazaClienteLetrasFinalTest() {
        assertFalse(RespuestaClearingNettingBo.referenciaSVAceptable("C00000001234567P"));
    }
}
