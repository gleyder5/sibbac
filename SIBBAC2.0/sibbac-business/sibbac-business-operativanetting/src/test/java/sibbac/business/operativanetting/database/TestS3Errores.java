package sibbac.business.operativanetting.database;


// Internal
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// Java
// JUnit
// Spring




import sibbac.business.wrappers.database.bo.S3ErroresBo;
import sibbac.business.wrappers.database.bo.S3LiquidacionBo;
import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.business.wrappers.database.model.S3Liquidacion;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestS3Errores extends ADBTest {

	@Autowired
	private S3LiquidacionBo	BoS3Liquidacion	= null;
	@Autowired
	private S3ErroresBo		BoS3Errores		= null;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void TestRealizarS3Errores() {

		LOG.debug( "Starting..." );

		S3Liquidacion datos_liquidacion = new S3Liquidacion( "IDFICHERO", 'C', "NBOOKING002", "NUCNFCLT0002", ( short ) 3, 'B', "PDL",
				new Date(), new Date(), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, new Date() );

		BoS3Liquidacion.save( datos_liquidacion );

		LOG.debug( "Se inserta un nuevo registro de S3Errores" );

		assertNotNull( "Error al autogenerar la clave", datos_liquidacion.getId() );

		S3Errores datos_errores = new S3Errores( datos_liquidacion, 'A' );

		BoS3Errores.save( datos_errores );

		assertNotNull( "Error al autogenerar la clave", datos_errores.getId() );

		datos_errores.setDserror( "DESCRIPCION" );

		BoS3Errores.save( datos_errores );
		LOG.debug( "Se actualizan datos" );

	}

}
