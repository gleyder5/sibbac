package sibbac.business.operativanetting.interes;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class CalculoInteresesTest.
 */
public class CalculoInteresesTest extends AbstractTest {

	/** The calculo interes. */
	@Autowired
	private ICalculoInteres calculoInteres;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CalculoInteresesTest.class);

	/**
	 * Calculate test.
	 */
	@Test
	public void calculateTest() {
		DateTime fechaDesde = new DateTime(2015, 1, 1, 0, 0);
		DateTime fechaHasta = new DateTime(2015, 12, 31, 0, 0);
		assertNotNull(calculoInteres);
		EntradaCalculoInteresBean entradaCalculoInteresBean = new EntradaCalculoInteresBean(
				fechaDesde.toDate(), fechaHasta.toDate(), "1522",
				new BigDecimal("1000000"));
		ResultadoCalculoInteresBean calculoInteresBean = calculoInteres
				.calculate(entradaCalculoInteresBean);
		
		LOG.debug(calculoInteresBean.toString());
	}

}
