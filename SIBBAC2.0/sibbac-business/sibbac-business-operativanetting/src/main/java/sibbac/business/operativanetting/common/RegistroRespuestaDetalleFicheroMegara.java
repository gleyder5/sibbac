package sibbac.business.operativanetting.common;


public class RegistroRespuestaDetalleFicheroMegara {

	// datos recibidos desde el fichero (bloque de detalle)
	private String	codigoDatoDiscrepante;
	private String	datoDiscrepanteSV;
	private String	datoDiscrepanteCliente;
	private String	textoDatoDiscrepante;

	public RegistroRespuestaDetalleFicheroMegara( String Linea ) {

		// Codigo dato discrepante CHAR(4)
		Integer aux_tam = 4;
		this.codigoDatoDiscrepante = Linea.substring( 0, aux_tam );
		Linea = Linea.substring( aux_tam );

		// Referencia de la operacion CHAR(60)
		aux_tam = 60;
		this.datoDiscrepanteSV = Linea.substring( 0, aux_tam );
		Linea = Linea.substring( aux_tam );

		// Dato discrepante cliente CHAR(60)
		aux_tam = 60;
		this.datoDiscrepanteCliente = Linea.substring( 0, aux_tam );
		Linea = Linea.substring( aux_tam );

		// Texto de dato discrepante CHAR(60)
		aux_tam = 60;
		this.textoDatoDiscrepante = Linea.substring( 0, aux_tam );
		Linea = Linea.substring( aux_tam );

	}

	public String getCodigoDatoDiscrepante() {
		return codigoDatoDiscrepante;
	}

	public void setCodigoDatoDiscrepante( String codigoDatoDiscrepante ) {
		this.codigoDatoDiscrepante = codigoDatoDiscrepante;
	}

	public String getDatoDiscrepanteSV() {
		return datoDiscrepanteSV;
	}

	public void setDatoDiscrepanteSV( String datoDiscrepanteSV ) {
		this.datoDiscrepanteSV = datoDiscrepanteSV;
	}

	public String getDatoDiscrepanteCliente() {
		return datoDiscrepanteCliente;
	}

	public void setDatoDiscrepanteCliente( String datoDiscrepanteCliente ) {
		this.datoDiscrepanteCliente = datoDiscrepanteCliente;
	}

	public String getTextoDatoDiscrepante() {
		return textoDatoDiscrepante;
	}

	public void setTextoDatoDiscrepante( String textoDatoDiscrepante ) {
		this.textoDatoDiscrepante = textoDatoDiscrepante;
	}

}
