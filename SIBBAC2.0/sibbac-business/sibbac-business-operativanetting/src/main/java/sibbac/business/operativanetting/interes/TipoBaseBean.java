package sibbac.business.operativanetting.interes;

import java.math.BigDecimal;

/**
 * The Class TipoBaseBean.
 */
public class TipoBaseBean {
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((interes == null) ? 0 : interes.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoBaseBean other = (TipoBaseBean) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (interes == null) {
			if (other.interes != null)
				return false;
		} else if (!interes.equals(other.interes))
			return false;
		return true;
	}

	/** The interes. */
	private BigDecimal interes;

	/** The base. */
	private Integer base;

	/**
	 * Instantiates a new tipo base bean.
	 *
	 * @param interes the interes
	 * @param base the base
	 */
	public TipoBaseBean(BigDecimal interes, Integer base) {
		super();
		this.interes = interes;
		this.base = base;
	}

	/**
	 * Gets the interes.
	 *
	 * @return the interes
	 */
	public BigDecimal getInteres() {
		return interes;
	}

	/**
	 * Sets the interes.
	 *
	 * @param interes the new interes
	 */
	public void setInteres(BigDecimal interes) {
		this.interes = interes;
	}

	/**
	 * Gets the base.
	 *
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/**
	 * Sets the base.
	 *
	 * @param base the new base
	 */
	public void setBase(Integer base) {
		this.base = base;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TipoBaseBean [interes=" + interes + ", base=" + base + "]";
	}
	
	

}
