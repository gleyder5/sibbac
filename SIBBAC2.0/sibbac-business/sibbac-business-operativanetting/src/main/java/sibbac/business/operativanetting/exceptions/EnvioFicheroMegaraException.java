/**
 * # EnvioFicheroMegaraException.java
 * # Fecha de creación: 05/10/2015
 */
package sibbac.business.operativanetting.exceptions;

/**
 * Se lanza para indicar que un un alc o un desglosecamara no cumplen las condiciones necesarias para ser enviadas a
 * Megara.
 * 
 * @author XI316153
 * @version 1.0
 * @since SIBBAC2.0
 */
public class EnvioFicheroMegaraException extends Exception {

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTANTES ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -431338040672973507L;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Constructor I. Genera una nueva excepción con el mensaje especificado.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   */
  public EnvioFicheroMegaraException(String mensaje) {
    super(mensaje);
  } // EnvioFicheroMegaraException

  /**
   * Constructor II. Genera una nueva excepción con el mensaje especificado y la excepción producida.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   * @param causa Causa por la que se ha lanzado la excepción.
   */
  public EnvioFicheroMegaraException(String mensaje, Throwable causa) {
    super(mensaje, causa);
  } // EnvioFicheroMegaraException

} // EnvioFicheroMegaraException
