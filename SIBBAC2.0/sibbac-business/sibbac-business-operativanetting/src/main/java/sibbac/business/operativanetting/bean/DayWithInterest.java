package sibbac.business.operativanetting.bean;

import java.math.BigDecimal;

import org.joda.time.DateTime;

/**
 * The Class IntervalWithInterest.
 */
public class DayWithInterest {

	/** The day. */
	private final DateTime day;

	/** The interest. */
	private final BigDecimal interest;
	
	/** The base. */
	private final Integer base;
	
	/** The is default. */
	private final Boolean isDefault;

	/**
	 * Instantiates a new day with interest.
	 *
	 * @param day the day
	 * @param interest the interest
	 * @param base the base
	 * @param isDefault the is default
	 */
	public DayWithInterest(DateTime day, BigDecimal interest, Integer base ,Boolean isDefault) {
		super();
		this.day = day;
		this.interest = interest;
		this.base = base;
		this.isDefault = isDefault;
	}
	

	/**
	 * Gets the day.
	 *
	 * @return the day
	 */
	public DateTime getDay() {
		return day;
	}

	/**
	 * Gets the interest.
	 *
	 * @return the interest
	 */
	public BigDecimal getInterest() {
		return interest;
	}

	/**
	 * Gets the base.
	 *
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/**
	 * Gets the checks if is default.
	 *
	 * @return the checks if is default
	 */
	public Boolean getIsDefault() {
		return isDefault;
	}
	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result
				+ ((interest == null) ? 0 : interest.hashCode());
		result = prime * result
				+ ((isDefault == null) ? 0 : isDefault.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DayWithInterest other = (DayWithInterest) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (interest == null) {
			if (other.interest != null)
				return false;
		} else if (!interest.equals(other.interest))
			return false;
		if (isDefault == null) {
			if (other.isDefault != null)
				return false;
		} else if (!isDefault.equals(other.isDefault))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DayWithInterest [day=" + day + ", interest=" + interest
				+ ", base=" + base + ", isDefault=" + isDefault + "]";
	}

	
	


}
