package sibbac.business.operativanetting.database.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.wrappers.database.model.Netting;
import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.business.operativanetting.database.model.S3Intereses}.
 */

@Entity
@Table( name = DBConstants.CLEARING.S3INTERESES )
public class S3Intereses extends ATable< S3Intereses > implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID		= 3197556519160864091L;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumns( value = {
		@JoinColumn( name = "IDNETTING", referencedColumnName = "ID" )
	} )
	private Netting				netting;

	@Column( name = "NBOOKING", length = 20 )
	private String				nbooking;

	@Column( name = "NUCNFCLT", length = 20 )
	private String				nucnfclt;

	@Column( name = "NUCNFLIQ", precision = 4, scale = 0 )
	private Short				nucnfliq;

	@Column( name = "CDESTADO", nullable = false, length = 1 )
	private Character			cdestado				= 'A';

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHAOPERACION", nullable = false, length = 4 )
	private Date				fechaOperacion;

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHACONTRATACION", nullable = false, length = 4 )
	private Date				fechaContratacion;

	@Column( name = "NUTITULO", nullable = false, precision = 16, scale = 5 )
	private BigDecimal			nutitulo				= BigDecimal.ZERO;

	@Column( name = "IMPORTEINTERES", nullable = false, precision = 18, scale = 4 )
	private BigDecimal			importeInteres			= BigDecimal.ZERO;

	@Column( name = "TIPOINTERES", nullable = false, precision = 18, scale = 4 )
	private BigDecimal			tipoInteres				= BigDecimal.ZERO;

	@Column( name = "CONTABILIZADO", nullable = false, length = 1 )
	private Character			contabilizado			= 'N';

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHACONTABILIDAD", length = 4 )
	private Date				fechaContabilidad;

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHALIQUIDACION", length = 4 )
	private Date				fechaLiquidacion;

	@Column( name = "IMPORTEEFECTIVO", nullable = false, precision = 18, scale = 4 )
	private BigDecimal			importeEfectivo			= BigDecimal.ZERO;

	@Column( name = "IMPORTENETOLIQUIDADO", nullable = false, precision = 18, scale = 4 )
	private BigDecimal			importeNetoLiquidado	= BigDecimal.ZERO;

	@Column( name = "CDALIAS", length = 20 )
	private String				cdalias;

	@Column( name = "TITULAR", length = 100 )
	private String				titular;

	@Temporal( TemporalType.DATE )
	@Column( name = "FECHAENVIO", length = 4 )
	private Date				fechaEnvio;

	@Column( name = "BASE" )
	private Integer				base;

	public S3Intereses() {

	}

	public BigDecimal getImporteNetoLiquidado() {
		return importeNetoLiquidado;
	}

	public void setImporteNetoLiquidado( BigDecimal importeNetoLiquidado ) {
		this.importeNetoLiquidado = importeNetoLiquidado;
	}

	public Netting getNetting() {
		return netting;
	}

	public void setNetting( Netting netting ) {
		this.netting = netting;
	}

	public String getNbooking() {
		return nbooking;
	}

	public void setNbooking( String nbooking ) {
		this.nbooking = nbooking;
	}

	public String getNucnfclt() {
		return nucnfclt;
	}

	public void setNucnfclt( String nucnfclt ) {
		this.nucnfclt = nucnfclt;
	}

	public short getNucnfliq() {
		return nucnfliq;
	}

	public void setNucnfliq( Short nucnfliq ) {
		this.nucnfliq = nucnfliq;
	}

	public char getCdestado() {
		return cdestado;
	}

	public void setCdestado( Character cdestado ) {
		this.cdestado = cdestado;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion( Date fechaOperacion ) {
		this.fechaOperacion = fechaOperacion;
	}

	public Date getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion( Date fechaContratacion ) {
		this.fechaContratacion = fechaContratacion;
	}

	public BigDecimal getNutitulo() {
		return nutitulo;
	}

	public void setNutitulo( BigDecimal nutitulo ) {
		this.nutitulo = nutitulo;
	}

	public BigDecimal getImporteInteres() {
		return importeInteres;
	}

	public void setImporteInteres( BigDecimal importeInteres ) {
		this.importeInteres = importeInteres;
	}

	public BigDecimal getTipoInteres() {
		return tipoInteres;
	}

	public void setTipoInteres( BigDecimal tipoInteres ) {
		this.tipoInteres = tipoInteres;
	}

	public char getContabilizado() {
		return contabilizado;
	}

	public void setContabilizado( Character contabilizado ) {
		this.contabilizado = contabilizado;
	}

	public Date getFechaContabilidad() {
		return fechaContabilidad;
	}

	public void setFechaContabilidad( Date fechaContabilidad ) {
		this.fechaContabilidad = fechaContabilidad;
	}

	public Date getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	public void setFechaLiquidacion( Date fechaLiquidacion ) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

	public BigDecimal getImporteEfectivo() {
		return importeEfectivo;
	}

	public void setImporteEfectivo( BigDecimal importeEfectivo ) {
		this.importeEfectivo = importeEfectivo;
	}

	public String getCdalias() {
		return cdalias;
	}

	public void setCdalias( String cdalias ) {
		this.cdalias = cdalias;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular( String titular ) {
		this.titular = titular;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio( Date fechaEnvio ) {
		this.fechaEnvio = fechaEnvio;
	}

	/**
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/**
	 * @param base the base to set
	 */
	public void setBase( Integer base ) {
		this.base = base;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != null ? id.hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if ( !( object instanceof S3Intereses ) ) {
			return false;
		}
		S3Intereses other = ( S3Intereses ) object;
		if ( ( this.id == null && other.id != null ) || ( this.id != null && !this.id.equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sibbac.business.operativanetting.database.model.S3Liquidacion[ id=" + id + " ]";
	}

}
