package sibbac.business.operativanetting.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import sibbac.business.operativanetting.common.Utilidad;

/**
 * The Class ServiceFinanciacionBean.
 */
public class ServiceFinanciacionBean {

	/** The intereses. */
	private BigDecimal intereses;

	/** The minimo importe. */
	private BigDecimal minimoImporte;

	/** The fecha inicio. */
	private Date fechaInicio;

	/** The fecha hasta. */
	private Date fechaHasta;

	/** The id alias. */
	private Long idAlias;
	
	/** The cdalias. */
	private String cdalias;
	
	/** The estado. */
	private String estado;
	
	/** The intereses desde. */
	private BigDecimal importeInteresesDesde;
	
	/** The intereses hasta. */
	private BigDecimal importeInteresesHasta;
	
	/** The id. */
	private Long id;
	
	/** The base. */
	private Integer base;

	/**
	 * Instantiates a new service financiacion bean.
	 *
	 * @param filters
	 *            the filters
	 */
	public ServiceFinanciacionBean(Map<String, String> filters) {
		if (filters.get("fechaInicio") != null
				&& !filters.get("fechaInicio").equals("")) {
			fechaInicio = Utilidad.getFechaFormateada(filters
					.get("fechaInicio"));
		}
		if (filters.get("fechaHasta") != null
				&& !filters.get("fechaHasta").equals("")) {
			fechaHasta = Utilidad.getFechaFormateada(filters
					.get("fechaHasta"));
		}
		if (filters.get("idAlias") != null
				&& !filters.get("idAlias").equals("")) {
			idAlias = Long.parseLong(filters.get("idAlias"));
		}
		if (filters.get("intereses") != null
				&& !filters.get("intereses").equals("")) {
			intereses = new BigDecimal(filters.get("intereses"));
		}
		if (filters.get("minimoImporte") != null
				&& !filters.get("minimoImporte").equals("")) {
			minimoImporte = new BigDecimal(filters.get("minimoImporte"));
		}
		if (filters.get("importeInteresDesde") != null
				&& !filters.get("importeInteresDesde").equals("")) {
			importeInteresesDesde = new BigDecimal(filters.get("importeInteresDesde"));
		}
		if (filters.get("importeInteresesHasta") != null
				&& !filters.get("importeInteresesHasta").equals("")) {
			importeInteresesHasta = new BigDecimal(filters.get("importeInteresesHasta"));
		}
		if (filters.get("cdalias") != null
				&& !filters.get("cdalias").equals("")) {
			cdalias = filters.get("cdalias");
		}
		if (filters.get("estado") != null
				&& !filters.get("estado").equals("")) {
			estado = filters.get("estado");
		}
		if (filters.get("id") != null
				&& !filters.get("id").equals("")) {
			id = Long.parseLong(filters.get("id"));
		}
		if (filters.get("base") != null
				&& !filters.get("base").equals("")) {
			base = Integer.parseInt(filters.get("base"));
		}
	}

	/**
	 * Gets the intereses.
	 *
	 * @return the intereses
	 */
	public BigDecimal getIntereses() {
		return intereses;
	}

	/**
	 * Gets the minimo importe.
	 *
	 * @return the minimo importe
	 */
	public BigDecimal getMinimoImporte() {
		return minimoImporte;
	}

	/**
	 * Gets the fecha inicio.
	 *
	 * @return the fecha inicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * Gets the fecha hasta.
	 *
	 * @return the fecha hasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * Gets the id alias.
	 *
	 * @return the id alias
	 */
	public Long getIdAlias() {
		return idAlias;
	}

	/**
	 * Gets the cdalias.
	 *
	 * @return the cdalias
	 */
	public String getCdalias() {
		return cdalias;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * Gets the importe intereses desde.
	 *
	 * @return the importe intereses desde
	 */
	public BigDecimal getImporteInteresesDesde() {
		return importeInteresesDesde;
	}

	/**
	 * Gets the importe intereses hasta.
	 *
	 * @return the importe intereses hasta
	 */
	public BigDecimal getImporteInteresesHasta() {
		return importeInteresesHasta;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the base.
	 *
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiceFinanciacionBean [intereses=" + intereses
				+ ", minimoImporte=" + minimoImporte + ", fechaInicio="
				+ fechaInicio + ", fechaHasta=" + fechaHasta + ", idAlias="
				+ idAlias + ", cdalias=" + cdalias + ", estado=" + estado
				+ ", importeInteresesDesde=" + importeInteresesDesde
				+ ", importeInteresesHasta=" + importeInteresesHasta + ", id="
				+ id + ", base=" + base + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((cdalias == null) ? 0 : cdalias.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result
				+ ((fechaHasta == null) ? 0 : fechaHasta.hashCode());
		result = prime * result
				+ ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idAlias == null) ? 0 : idAlias.hashCode());
		result = prime
				* result
				+ ((importeInteresesDesde == null) ? 0 : importeInteresesDesde
						.hashCode());
		result = prime
				* result
				+ ((importeInteresesHasta == null) ? 0 : importeInteresesHasta
						.hashCode());
		result = prime * result
				+ ((intereses == null) ? 0 : intereses.hashCode());
		result = prime * result
				+ ((minimoImporte == null) ? 0 : minimoImporte.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceFinanciacionBean other = (ServiceFinanciacionBean) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (cdalias == null) {
			if (other.cdalias != null)
				return false;
		} else if (!cdalias.equals(other.cdalias))
			return false;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (fechaHasta == null) {
			if (other.fechaHasta != null)
				return false;
		} else if (!fechaHasta.equals(other.fechaHasta))
			return false;
		if (fechaInicio == null) {
			if (other.fechaInicio != null)
				return false;
		} else if (!fechaInicio.equals(other.fechaInicio))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idAlias == null) {
			if (other.idAlias != null)
				return false;
		} else if (!idAlias.equals(other.idAlias))
			return false;
		if (importeInteresesDesde == null) {
			if (other.importeInteresesDesde != null)
				return false;
		} else if (!importeInteresesDesde.equals(other.importeInteresesDesde))
			return false;
		if (importeInteresesHasta == null) {
			if (other.importeInteresesHasta != null)
				return false;
		} else if (!importeInteresesHasta.equals(other.importeInteresesHasta))
			return false;
		if (intereses == null) {
			if (other.intereses != null)
				return false;
		} else if (!intereses.equals(other.intereses))
			return false;
		if (minimoImporte == null) {
			if (other.minimoImporte != null)
				return false;
		} else if (!minimoImporte.equals(other.minimoImporte))
			return false;
		return true;
	}
	
	
	
}

