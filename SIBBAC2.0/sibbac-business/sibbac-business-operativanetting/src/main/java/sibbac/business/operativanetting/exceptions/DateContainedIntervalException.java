package sibbac.business.operativanetting.exceptions;

import java.util.List;

import sibbac.business.operativanetting.database.model.FechaInicioFechaHasta;

/**
 * The Class DateContainedIntervalException.
 */
@SuppressWarnings("serial")
public class DateContainedIntervalException extends RuntimeException {

	/** The fecha inicio fecha hastas. */
	List<FechaInicioFechaHasta> fechaInicioFechaHastas;

	/** The Constant MESSAGE. */
	private final static String MESSAGE = "La fecha de Inicio o la fecha de hasta esta contenida en algun intervalo";

	/**
	 * Instantiates a new date contained interval exception.
	 *
	 * @param fechaInicioFechaHastas
	 *            the fecha inicio fecha hastas
	 */
	public DateContainedIntervalException(
			List<FechaInicioFechaHasta> fechaInicioFechaHastas) {
		super(MESSAGE);
		this.fechaInicioFechaHastas = fechaInicioFechaHastas;
	}

	/**
	 * Gets the fecha inicio fecha hastas.
	 *
	 * @return the fecha inicio fecha hastas
	 */
	public List<FechaInicioFechaHasta> getFechaInicioFechaHastas() {
		return fechaInicioFechaHastas;
	}

}
