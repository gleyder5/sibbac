package sibbac.business.operativanetting.common;

import static java.text.MessageFormat.format;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fallidos.database.dao.FixmlFallidoDao;
import sibbac.business.fallidos.database.model.FixmlFallido;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.operativanetting.database.bo.DatosdiscrepantesBo;
import sibbac.business.operativanetting.database.bo.S3InteresesBo;
import sibbac.business.operativanetting.database.model.Datosdiscrepantes;
import sibbac.business.operativanetting.database.model.S3Intereses;
import sibbac.business.operativanetting.exceptions.RecepcionFicheroMegaraException;
import sibbac.business.operativanetting.interes.EntradaCalculoInteresBean;
import sibbac.business.operativanetting.interes.ICalculoInteres;
import sibbac.business.operativanetting.interes.ResultadoCalculoInteresBean;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.NettingBo;
import sibbac.business.wrappers.database.bo.S3ErroresBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.dao.S3LiquidacionDao;
import sibbac.business.wrappers.database.dao.Tmct0MovimientoCuentaAliasDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.AlcOrdenMercado;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Idioma;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.business.wrappers.database.model.S3Liquidacion;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.database.bo.AbstractBo;

/**
 * Procesa los ficheros de respuesta de Megara.
 * 
 * @author XI316153
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RespuestaClearingNetting extends AbstractBo<Tmct0alc, Tmct0alcId, Tmct0alcDao> {

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

    /** Referencia para la inserción de logs. */
    private static final Logger LOG = LoggerFactory.getLogger(RespuestaClearingNetting.class);

    private static final String TIPO_OPERACION_COUI = "COUI";
    private static final String TIPO_OPERACION_CAUI = "CAUI";
    private static final String TIPO_OPERACION_COLLC = "COLLC";
    private static final String TIPO_OPERACION_COLLP = "COLLP";

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

    private final Map<String, List<String>> referenciasCanceladas;

    private final List<String> referenciasNoEncontradas;

    /**
     * Lista donde almacenar las operaciones no
     * encontradas para su envio posterior por mail.
     */
    private final List<RegistroRespuestaCabeceraFicheroMegara> colateralesNoencontrados;

    private final List<String> alcAlarmasList;
    
    /** lineas que no han sido reconocidas y se registran como rechazadas */
    private final List<String> lineasRechazadas;

    @Autowired
    private Tmct0alcBo tmct0alcBo;

    @Autowired
    private Tmct0desgloseCamaraBo tmct0desglosecamaraBo;

    @Autowired
    private AlcOrdenesBo alcOrdenesBo;

    @Autowired
    private S3LiquidacionDao s3LiquidacionDao;

    @Autowired
    private S3ErroresBo s3ErroresBo;

    @Autowired
    private DatosdiscrepantesBo datosdiscrepantesBo;

    @Autowired
    private NettingBo nettingBo;

    @Autowired
    private S3InteresesBo s3InteresesBo;

    @Autowired
    private AliasBo aliasBo;

    @Autowired
    private Tmct0CuentasDeCompensacionBo tmct0CuentasDeCompensacionBo;

    @Autowired
    private Tmct0MovimientoCuentaAliasBo tmct0MovimientoCuentaAliasBo;

    @Autowired
    private Tmct0MovimientoCuentaAliasDao tmct0MovimientoCuentaAliasDao;

    @Autowired
    private ICalculoInteres calculoInteres;

    @Autowired
    private FixmlFallidoDao fixmlFallidoDao;

    /**
     * Parámetros de configuración del proceso.
     */
    private Map<String, String> configParams;

    /**
     * Mapa de estados.
     */
    private Map<String, Tmct0estado> estadosMap;
    
    private RegistroRespuestaCabeceraFicheroMegara registro;
    
    private Date fechaFichero;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

    public RespuestaClearingNetting() {
        referenciasCanceladas = new HashMap<>();
        referenciasNoEncontradas = new ArrayList<>();
        colateralesNoencontrados = new ArrayList<>();
        alcAlarmasList = new ArrayList<>();
        lineasRechazadas = new ArrayList<>();
    }

    public Map<String, List<String>> getReferenciasCanceladas() {
        return referenciasCanceladas;
    }

    public List<String> getReferenciasNoEncontradas() {
        return referenciasNoEncontradas;
    }

    public List<RegistroRespuestaCabeceraFicheroMegara> getColateralesNoencontrados() {
        return colateralesNoencontrados;
    }

    public List<String> getAlcAlarmasList() {
        return alcAlarmasList;
    }

    /**
     * Valores de inicializacion para el proceso
     * 
     * @param configParams Parámetros de configuración del proceso.
     * @param estadosMap Mapa de estados.
     */
    public void init(Map<String, String> configParams, Map<String, Tmct0estado> estadosMap) {
        this.configParams = configParams;
        this.estadosMap = estadosMap;
    }

    /**
     * Procesa la lista de registros recibidos de Megara.
     * 
     * @param registrosList Lista de registros a tratar.
     * @param fechaFichero Fecha del fichero que contiene los registros a
     * tratar.
     * @throws RecepcionFicheroMegaraException Si ocurre algún error durante el
     * procesamiento de los registro.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { RecepcionFicheroMegaraException.class,
            Exception.class })
    public void procesarRespuestas(List<RegistroRespuestaCabeceraFicheroMegara> registrosList, Date fechaFichero)
            throws RecepcionFicheroMegaraException {
        LOG.debug("[procesarRespuestas] Inicio ... ");
        lineasRechazadas.clear();
        this.fechaFichero = fechaFichero;
        for (int i = 0; i < registrosList.size(); i++) {
            registro = registrosList.get(i);
            try {
                tratamientoRegistroRespuesta();
            } 
            catch (Exception e) {
                LOG.warn("[procesarRespuesta] Al procesar respuesta", e);
                lineasRechazadas.add(registro.getLineaOriginal());
            } // catch
        } // for
    } // procesarRespuestas
    
    public boolean hayLineasRechazadas() {
      return !lineasRechazadas.isEmpty();
    }
    
    public Iterator<String> getLineasRechazadas() {
      return lineasRechazadas.iterator();
    }

    /**
     * Procesa el registro recibido de Megara.
     * 
     * @throws RecepcionFicheroMegaraException Si ocurre algún error durante el
     * procesamiento del registro.
     */
    private void tratamientoRegistroRespuesta() throws RecepcionFicheroMegaraException {
        LOG.debug("[tratamientoRegistroRespuesta] Inicio ... Trantando '{}'-'{}'-'{}'", registro.getTipoOperacion(),
                registro.getReferenciaSV(), registro.getEstadoOperacion());
        switch(registro.getTipoOperacion()) {
          case TIPO_OPERACION_COUI:
            procesarCOUI(registro);
            break;
          case TIPO_OPERACION_CAUI:
            procesarCAUI(registro);
            break;
          case TIPO_OPERACION_COLLC:
          case TIPO_OPERACION_COLLP:
            procesarCOLL(registro);
            break;
          default:
            String tipo_clearing = registro.getReferenciaSV().substring(0, 1);
            // Pata cliente (tanto con netting como sin netting)
            if (tipo_clearing.equals(Utilidad.CLEARING_TIPO.CLIENTE.getTipo())
                    || tipo_clearing.equals(Utilidad.CLEARING_TIPO.CLIENTE_NETTING.getTipo())) {
                respuestaCliente();
            } else if (tipo_clearing.equals(Utilidad.CLEARING_TIPO.MERCADO.getTipo())) {
                respuestaMercado();
            } else {
                LOG.warn("[tratamientoRegistroRespuesta] El tipo de clearing recibido no concuerda"
                        + " con ninguno de los configurados [" + tipo_clearing + "]");
                registraNoProcesado();
            } 
        }
        LOG.debug("[tratamientoRegistroRespuesta] Fin ...");
    } // tratamientoRegistroRespuesta

    private void respuestaCliente() {
        String alias_final, tipo_clearing;
        Tmct0alc tmct0alc = null;
        Netting netting = null;
        // Lista de alc pertenecientes a un neteo o un cliente sin neteo
        List<Tmct0alc> alcList = new ArrayList<>();
        // Para los casos que la liquidacion real supere a la prevista
        S3Intereses nuevo_intereses = null;
        S3Liquidacion nueva_liquidacion;

        LOG.debug("[respuestaCliente] Inicio...");
        tipo_clearing = registro.getReferenciaSV().substring(0, 1);
        nueva_liquidacion = generaLiquidacionDesdeRegistro(Constantes.CHAR_CLIENTE);
        // Se actualiza cuando se recupere el valor
        nueva_liquidacion.setImporteEfectivo(BigDecimal.ZERO); 
        // Se distingue el tipo de cliente porque la referencia es diferente y
        // hay que buscar por diferentes datos
        if (tipo_clearing.equals(Utilidad.CLEARING_TIPO.CLIENTE.getTipo())) {
            LOG.debug("[respuestaCliente] Clearing tipo cliente");
            AlcOrdenes alcOrdenes = alcOrdenesBo.findByReferenciaS3(registro.getReferenciaSV());
            if (alcOrdenes == null) {
                if (referenciasNoEncontradas.contains(registro.getReferenciaSV())) {
                    LOG.debug("[respuestaCliente] Referencia no encontrada ya registrada");
                    return;
                } else {
                    LOG.debug("[respuestaCliente] Se registra referencia no encontrada y se registra alarma");
                    alcAlarmasList.add("Se ha recibido información de S3 con una referencia desconocida ("
                            + registro.getReferenciaSV() + " ).");
                    referenciasNoEncontradas.add(registro.getReferenciaSV());
                    return;
                } // else

            } else {

                if ('B' == alcOrdenes.getEstadoInstruccion()) {
                    LOG.debug("[respuestaCliente] Alarma por orden dada de baja");
                    alcAlarmasList.add("Se ha recibido información de S3 para un registro de baja ("
                            + registro.getReferenciaSV() + " ).");
                    return;
                }

            }
            Tmct0alcId alcId = alcOrdenes.createAlcIdFromFields();
            tmct0alc = tmct0alcBo.findById(alcId);
            if (tmct0alc == null) {
                LOG.warn("[tratamientoRegistroRespuesta] No se localiza un alc con id {}", alcId);
                return;
            }
            // La referencia recibida pertenece a un ALC que está cancelado
            if (referenciasCanceladas.containsKey(registro.getReferenciaSV())) {
                LOG.debug("[respuestaCliente] La referencia encontrada ha sido cancelada");
                return;
            } else {
                if (tmct0alc.getCdestadoasig().compareTo(ASIGNACIONES.RECHAZADA.getId()) == 0) {
                    LOG.debug("[respuestaCliente] Se registra una referencia cancelada y se genera alarma");
                    alcAlarmasList.add(new String(tmct0alc.getNbooking().trim().concat("#")
                            .concat(tmct0alc.getNutitliq().setScale(0, BigDecimal.ROUND_DOWN).toString()).concat("#")
                            .concat(tmct0alc.getCdordtit() != null ? tmct0alc.getCdordtit().trim() : "<null>")));
                    referenciasCanceladas.put(registro.getReferenciaSV(), alcAlarmasList);
                    return;
                } // if
            } // else
            LOG.debug("[respuestaCliente] inicia registro de nueva liquidacion");
            nueva_liquidacion.setNbooking(tmct0alc.getId().getNbooking());
            nueva_liquidacion.setNucnfliq(tmct0alc.getId().getNucnfliq());
            nueva_liquidacion.setNucnfclt(tmct0alc.getId().getNucnfclt());
            alcList.add(tmct0alc);

            Tmct0alo datos_alo = tmct0alc.getTmct0alo();
            alias_final = datos_alo.getTmct0bok().getCdalias().trim();

            // Para compras cuando la fecha de liquidacion real (actual) es
            // mayor a la fecha liquidacion prevista
            if (datos_alo.getCdtpoper().toString().equals(Constantes.COMPRA)
                    && tmct0alc.getFeopeliq().before(new Date())) {
                nuevo_intereses = new S3Intereses();
                nuevo_intereses.setNbooking(tmct0alc.getId().getNbooking());
                nuevo_intereses.setNucnfliq(tmct0alc.getId().getNucnfliq());
                nuevo_intereses.setNucnfclt(tmct0alc.getId().getNucnfclt());
                nuevo_intereses.setCdalias(alias_final);
                nuevo_intereses.setFechaContratacion(tmct0alc.getFeejeliq());
                nuevo_intereses.setFechaOperacion(tmct0alc.getFeopeliq());
                nuevo_intereses.setNutitulo(tmct0alc.getNutitliq());
                nuevo_intereses.setImporteEfectivo(tmct0alc.getImefeagr());
                nuevo_intereses.setTitular(tmct0alc.getNbtitliq());
            } // if
        } else {
            Long idNetting = null;
            try {
                idNetting = Long.valueOf(registro.getReferenciaSV().substring(1));
            } catch (NumberFormatException e) {
                LOG.error("[tratamientoRegistroRespuesta] Error convirtiendo idNetting ... " + e.getMessage());
                return;
            } // catch

            netting = nettingBo.findById(idNetting);
            if (netting == null) {
                if (referenciasNoEncontradas.contains(registro.getReferenciaSV())) {
                    return;
                } else {
                    referenciasNoEncontradas.add(registro.getReferenciaSV());
                    return;
                } // else
            } // if

            alias_final = netting.getCdalias().trim();
            nueva_liquidacion.setNetting(netting);

            // Para compras cuando se la fecha liquidacion real (actual) es
            // mayor a la fecha de liquidacion prevista
            if (netting.getNutituloNeteado().compareTo(BigDecimal.ZERO) > 0
                    && netting.getFechaContratacion().before(new Date())) {
                nuevo_intereses = new S3Intereses();
                nuevo_intereses.setNetting(netting);
                nuevo_intereses.setCdalias(alias_final);
                nuevo_intereses.setFechaOperacion(netting.getFechaOperacion());
                nuevo_intereses.setFechaContratacion(netting.getFechaContratacion());
                nuevo_intereses.setNutitulo(netting.getNutituloNeteado());
                nuevo_intereses.setImporteEfectivo(netting.getEfectivoNeteado());
                nuevo_intereses.setImporteNetoLiquidado(nueva_liquidacion.getImporteNetoLiquidado());

                List<AlcOrdenes> alcOrdenesList = alcOrdenesBo.findByNetting(netting);
                for (AlcOrdenes alcOrdenes : alcOrdenesList) {
                    Tmct0alcId alcId = alcOrdenes.createAlcIdFromFields();
                    tmct0alc = tmct0alcBo.findById(alcId);
                    if (tmct0alc == null) {
                        LOG.warn("[tratamientoRegistroRespuesta] No se localiza un Alc con id {}", alcId);
                        continue;
                    }
                    alcList.add(tmct0alc);
                    nuevo_intereses.setTitular(tmct0alc.getNbtitliq());
                } // for
            } // if

            // La referencia recibida pertenece a un ALC que está cancelado
            if (referenciasCanceladas.containsKey(registro.getReferenciaSV())) {
                return;
            } else {
                List<String> canceladasList = new ArrayList<>();
                for (Tmct0alc tmct0alcTemp : alcList) {
                    if (tmct0alcTemp.getCdestadoasig().compareTo(ASIGNACIONES.RECHAZADA.getId()) == 0) {
                        canceladasList.add(new String(tmct0alcTemp.getNbooking().trim().concat("#")
                                .concat(tmct0alcTemp.getNutitliq().setScale(0, BigDecimal.ROUND_DOWN).toString())
                                .concat("#").concat(tmct0alcTemp.getCdordtit() != null
                                        ? tmct0alcTemp.getCdordtit().trim() : "<null>")));
                    } // if
                } // for
                if (!canceladasList.isEmpty()) {
                    referenciasCanceladas.put(registro.getReferenciaSV(), alcAlarmasList);
                    return;
                }
            } // else

        } // else

        switch (registro.getEstadoOperacion()) {
        case Constantes.CDEST_CANC_ACEPTADA:
            for (Tmct0alc tmct0alcActualizar : alcList) {
                tmct0alcActualizar.setEstadoentrec(estadosMap.get("estadoClearingAceptadaAnulacion"));
            } // for
            tmct0alcBo.save(alcList);
            break;
        case Constantes.CDEST_CANC_RECHAZADA:
            nueva_liquidacion.setNutituloLiquidados(BigDecimal.ZERO);
            nueva_liquidacion.setImporteEfectivo(BigDecimal.ZERO);
            nueva_liquidacion.setImporteNetoLiquidado(BigDecimal.ZERO);
            nueva_liquidacion.setImporteCorretaje(BigDecimal.ZERO);

            for (Tmct0alc tmct0alcActualizar : alcList) {
                tmct0alcActualizar.setEstadoentrec(estadosMap.get("estadoClearingRechazadaAnulacion"));
            } // for
            tmct0alcBo.save(alcList);
            break;
        case Constantes.CDEST_PENDIENTE:
            for (Tmct0alc tmct0alcActualizar : alcList) {
                if (tmct0alcActualizar.getEstadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingPendienteCase").getIdestado()) < 0) {
                    tmct0alcActualizar.setEstadoentrec(estadosMap.get("estadoClearingPendienteCase"));
                } // if
                tmct0alcBo.save(alcList);
            } // else
            break;
        case Constantes.CDEST_UNMATCH:
            for (Tmct0alc tmct0alcActualizar : alcList) {
                if (tmct0alcActualizar.getEstadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingNoCasada").getIdestado()) < 0) {
                    tmct0alcActualizar.setEstadoentrec(estadosMap.get("estadoClearingNoCasada"));
                } // if
            } // for
            tmct0alcBo.save(alcList);
            break;
        case Constantes.CDEST_MATCHED:
            for (Tmct0alc tmct0alcActualizar : alcList) {
                if (tmct0alcActualizar.getEstadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingCasadaPdteLiq").getIdestado()) < 0) {
                    tmct0alcActualizar.setEstadoentrec(estadosMap.get("estadoClearingCasadaPdteLiq"));
                } // if
            } // for
            tmct0alcBo.save(alcList);
            break;
        case Constantes.CDEST_FALLIDA:
            if (registro.getBloquesDestalle() > 0) {
                LOG.debug("[tratamientoRegistroRespuesta] Procesando bloques de detalle de error ....");
                S3Errores datos_error = null;
                for (RegistroRespuestaDetalleFicheroMegara bloqueDetalle : registro.getBloquesDestalleList()) {
                    datos_error = new S3Errores();
                    datos_error.setS3liquidacion(nueva_liquidacion);
                    datos_error.setDatoErrorCliente(bloqueDetalle.getDatoDiscrepanteCliente());
                    datos_error.setDatoErrorSV(bloqueDetalle.getDatoDiscrepanteSV());

                    // Se busca la tradución del código en la tabla
                    Datosdiscrepantes datos_discrepante = datosdiscrepantesBo
                            .findByCodigo(bloqueDetalle.getCodigoDatoDiscrepante());
                    // si no viene en la tabla o es NARR(texto libre)
                    if (datos_discrepante == null
                            || bloqueDetalle.getCodigoDatoDiscrepante().equals(Constantes.CD_DATOS_DISCREPANTE_NARR)) {
                        datos_error.setDserror(bloqueDetalle.getTextoDatoDiscrepante());
                    } else {
                        datos_error.setDserror(recuperarDescripcionDatosDiscrepantes(alias_final, datos_discrepante));
                    } // else

                    s3ErroresBo.save(datos_error);
                } // for
            } // if (registro.getBloquesDestalle() > 0)

            for (Tmct0alc tmct0alcActualizar : alcList) {
                if (tmct0alcActualizar.getEstadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingLiqFallida").getIdestado()) < 0) {
                    tmct0alcActualizar.setEstadoentrec(estadosMap.get("estadoClearingLiqFallida"));
                } // if
            } // for
            tmct0alcBo.save(alcList);
            break;
        case Constantes.CDEST_LIQUIDADA:
            Tmct0estado estado = null;

            if (tipo_clearing.equals(Utilidad.CLEARING_TIPO.CLIENTE.getTipo())) {
                LOG.debug("[tratamientoRegistroRespuesta] Procesando clearing");

                try {
                    estado = calcularDatosCliente(nueva_liquidacion, tmct0alc.getImefeagr(), tmct0alc.getImfinsvb(),
                            tmct0alc.getNutitliq());
                } catch (RecepcionFicheroMegaraException e) {
                    LOG.warn("[tratamientoRegistroRespuesta] " + e.getMessage());
                    return;
                } // catch
                LOG.debug("[tratamientoRegistroRespuesta] Estado a establecer: " + estado.getNombre());

                // Por cada linea del fichero insertamos un movimiento en cuenta
                // alias
                Tmct0MovimientoCuentaAlias movimientoCuentaAlias = new Tmct0MovimientoCuentaAlias();

                // En caso de compras
                if (tmct0alc.getTmct0alo().getCdtpoper().toString().equals(Constantes.COMPRA)) {
                    movimientoCuentaAlias.setCdoperacion(configParams.get("compra_cliente_mov"));
                    movimientoCuentaAlias.setSignoanotacion('S');
                    movimientoCuentaAlias.setTitulos(nueva_liquidacion.getNutituloLiquidados().negate());
                    movimientoCuentaAlias.setImefectivo(nueva_liquidacion.getImporteNetoLiquidado());
                } else if (tmct0alc.getTmct0alo().getCdtpoper().toString().equals(Constantes.VENTA)) {
                    movimientoCuentaAlias.setCdoperacion(configParams.get("venta_cliente_mov"));
                    movimientoCuentaAlias.setSignoanotacion('E');
                    movimientoCuentaAlias.setTitulos(nueva_liquidacion.getNutituloLiquidados());
                    movimientoCuentaAlias.setImefectivo(nueva_liquidacion.getImporteNetoLiquidado());
                } // else if

                movimientoCuentaAlias.setCorretaje(tmct0alc.getImcomisn());
                movimientoCuentaAlias.setCdaliass(tmct0alc.getTmct0alo().getTmct0bok().getCdalias());
                movimientoCuentaAlias.setIsin(tmct0alc.getTmct0alo().getCdisin());
                List<Tmct0desglosecamara> dcList = tmct0alc.getTmct0desglosecamaras();
                if(dcList != null && !dcList.isEmpty()) {
                    String codigoCamaraCompensacion = dcList.get(0).getTmct0CamaraCompensacion().getCdCodigo();
                    movimientoCuentaAlias.setMercado(codigoCamaraCompensacion);
                }
                else
                    LOG.warn("[respuestaCliente] no se han localizado desglose camaara en el Alc {} y no se "
                            + "ha determinado el codigo de camara compensacion", tmct0alc.getId());
                movimientoCuentaAlias.setTradedate(tmct0alc.getFeejeliq());
                movimientoCuentaAlias.setSettlementdate(nueva_liquidacion.getFechaValor());
                movimientoCuentaAlias.setCdcodigocuentaliq(tmct0CuentasDeCompensacionBo.findByTmct0alc(tmct0alc).get(0)
                        .getTmct0CuentaLiquidacion().getCdCodigo());
                movimientoCuentaAlias.setRefmovimiento(nueva_liquidacion.getIdFichero());

                tmct0MovimientoCuentaAliasBo.save(movimientoCuentaAlias);
            } else {
                LOG.debug("[tratamientoRegistroRespuesta] Procesando netting");
                try {
                    estado = calcularDatosCliente(nueva_liquidacion, netting.getEfectivoNeteado(),
                            netting.getCorretajeNeteado(), netting.getNutituloNeteado());
                } catch (RecepcionFicheroMegaraException e) {
                    LOG.warn("[tratamientoRegistroRespuesta] " + e.getMessage());
                    return;
                } // catch
                LOG.debug("[tratamientoRegistroRespuesta] Estado a establecer: " + estado.getNombre());

                // Se modifica el estado de liquidación del netting
                if (estado.getIdestado() == CLEARING.LIQUIDADA.getId()) {
                    netting.setLiquidado('S');
                    nettingBo.getDao().save(netting);
                } // else

                // Por cada linea del fichero insertamos un movimiento en cuenta
                // alias
                Tmct0MovimientoCuentaAlias movimientoCuentaAlias = new Tmct0MovimientoCuentaAlias();

                // En caso de compras
                if (tmct0alc.getTmct0alo().getCdtpoper().toString().equals(Constantes.COMPRA)) {
                    movimientoCuentaAlias.setCdoperacion(configParams.get("compra_cliente_mov"));
                    movimientoCuentaAlias.setSignoanotacion('S');
                    movimientoCuentaAlias.setTitulos(netting.getNutituloNeteado().negate());
                    movimientoCuentaAlias.setImefectivo(netting.getEfectivoNeteado());
                } else if (tmct0alc.getTmct0alo().getCdtpoper().toString().equals(Constantes.VENTA)) {
                    movimientoCuentaAlias.setCdoperacion(configParams.get("venta_cliente_mov"));
                    movimientoCuentaAlias.setSignoanotacion('E');
                    movimientoCuentaAlias.setTitulos(netting.getNutituloNeteado());
                    movimientoCuentaAlias.setImefectivo(netting.getEfectivoNeteado());
                } // else if

                movimientoCuentaAlias.setCorretaje(netting.getCorretajeNeteado());
                movimientoCuentaAlias.setCdaliass(netting.getCdalias());
                movimientoCuentaAlias.setIsin(netting.getCdisin());
                movimientoCuentaAlias.setMercado(netting.getCamaraCompensacion());
                movimientoCuentaAlias.setTradedate(netting.getFechaContratacion());
                movimientoCuentaAlias.setSettlementdate(netting.getFechaOperacion());
                movimientoCuentaAlias.setCdcodigocuentaliq(netting.getCdcuentacompensacion());
                movimientoCuentaAlias.setRefmovimiento(nueva_liquidacion.getIdFichero());

                tmct0MovimientoCuentaAliasBo.save(movimientoCuentaAlias);
            } // else
            LOG.debug("[tratamientoRegistroRespuesta] Almacenando liquidacion despues de los cambios ...");

            // actualizar intereses en caso de que tengan porque es compra y se
            // ha pasado de la fecha
            if (nuevo_intereses != null) {
                LOG.debug("[tratamientoRegistroRespuesta] Almacenando intereses ...");
                nuevo_intereses.setFechaLiquidacion(new Date()); // fecha
                                                                 // liquidacion
                                                                 // real
                nuevo_intereses.setImporteNetoLiquidado(nueva_liquidacion.getImporteNetoLiquidado());
                // calculamos los intereses
                EntradaCalculoInteresBean entrada_calculo = new EntradaCalculoInteresBean(
                        nuevo_intereses.getFechaOperacion(), new Date(), nuevo_intereses.getCdalias(),
                        nuevo_intereses.getImporteNetoLiquidado());
                ResultadoCalculoInteresBean resultado_calculo = calculoInteres.calculate(entrada_calculo);
                nuevo_intereses.setImporteInteres(resultado_calculo.getImporteInteres());
                nuevo_intereses.setTipoInteres(resultado_calculo.getTipoBaseBean().getInteres());
                nuevo_intereses.setBase(resultado_calculo.getTipoBaseBean().getBase());
                s3InteresesBo.save(nuevo_intereses);
            } // if

            for (Tmct0alc tmct0alcActualizar : alcList) {
                tmct0alcActualizar.setEstadoentrec(estado);
            }
            tmct0alcBo.save(alcList);
            break;
        } // switch (registro.getEstadoOperacion())
        s3LiquidacionDao.save(nueva_liquidacion);
        LOG.debug("[respuestaCliente] Fin");
    }

    private void respuestaMercado() {
        final String referenciaS3;
        final AlcOrdenes alcOrdenes;
        final Tmct0alc tmct0alc;
        final String aliasFinal;
        final List<Tmct0MovimientoCuentaAlias> movCuentaAliasList;
        Tmct0desglosecamara desgloseCamara;
        S3Liquidacion nuevaLiquidacion;
        
        LOG.debug("[respuestaMercado] Inicia...");
        referenciaS3 = registro.getReferenciaSV();
        alcOrdenes = alcOrdenesBo.findByReferenciaS3(referenciaS3);
        if (alcOrdenes == null) {
            referenciasNoEncontradas.add(format("Pata mercado S3 con referencia {0} desconocida", referenciaS3));
            return;
        }
        if (alcOrdenes.isEstadoBaja() && !referenciasCanceladas.containsKey(referenciaS3)) {
            referenciasCanceladas.put(referenciaS3,
                    Collections.singletonList(format("NuOrden: {0}", alcOrdenes.getNuorden())));
            return;
        }
        if(alcOrdenes.getOrdenesMercado().isEmpty()) {
            alcAlarmasList.add(format("La orden con referencia {0} no tiene ordenes mercado", referenciaS3));
            return;
        }
        tmct0alc = alcOrdenes.getOrdenesMercado().get(0).getDesgloseCamara().getTmct0alc();
        aliasFinal = tmct0alc.getTmct0alo().getTmct0bok().getCdalias().trim();
        switch (registro.getEstadoOperacion()) {
        case Constantes.CDEST_CANC_ACEPTADA:
            LOG.debug("[respuestaMercado] Eliminando movimiento cuenta alias de: "
                    + registro.getReferenciaSV());
            movCuentaAliasList = tmct0MovimientoCuentaAliasDao.findByRefMovimiento(registro.getReferenciaSV());
            LOG.debug("[respuestaMercado] movCuentaAlias encontrados: " + movCuentaAliasList.size());
            // Sólo debería haber uno
            tmct0MovimientoCuentaAliasDao.delete(movCuentaAliasList.get(0));
            LOG.debug("[respuestaMercado] Eliminado el primero");
            for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) {
                desgloseCamara = om.getDesgloseCamara();
                desgloseCamara.setTmct0estadoByCdestadoentrec(estadosMap.get("estadoClearingAceptadaAnulacion"));
                tmct0desglosecamaraBo.save(desgloseCamara);
            }
            break;
        case Constantes.CDEST_CANC_RECHAZADA:
            LOG.debug("[respuestaMercado] Cancelacion rechazada");
            nuevaLiquidacion = generaLiquidacionDesdeRegistro(Constantes.CHAR_MERCADO);
            nuevaLiquidacion.setNutituloLiquidados(BigDecimal.ZERO);
            nuevaLiquidacion.setImporteEfectivo(BigDecimal.ZERO);
            nuevaLiquidacion.setImporteNetoLiquidado(BigDecimal.ZERO);
            nuevaLiquidacion.setImporteCorretaje(BigDecimal.ZERO);
            s3LiquidacionDao.save(nuevaLiquidacion);
            for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) {
                desgloseCamara = om.getDesgloseCamara();
                desgloseCamara.setTmct0estadoByCdestadoentrec(estadosMap.get("estadoClearingRechazadaAnulacion"));
                tmct0desglosecamaraBo.save(desgloseCamara);
            }
            break;
        case Constantes.CDEST_PENDIENTE:
            LOG.debug("[respuestaMercado] Pendiente");
            for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) {
                desgloseCamara = om.getDesgloseCamara();
                if (desgloseCamara.getTmct0estadoByCdestadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingPendienteCase").getIdestado()) < 0) {
                    desgloseCamara.setTmct0estadoByCdestadoentrec(estadosMap.get("estadoClearingPendienteCase"));
                    tmct0desglosecamaraBo.save(desgloseCamara);
                } // if
            }
            break;
        case Constantes.CDEST_UNMATCH:
            LOG.debug("[respuestaMercado] Unmatch");
            for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) {
                desgloseCamara = om.getDesgloseCamara();
                if (desgloseCamara.getTmct0estadoByCdestadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingNoCasada").getIdestado()) < 0) {
                    desgloseCamara.setTmct0estadoByCdestadoentrec(estadosMap.get("estadoClearingNoCasada"));
                    tmct0desglosecamaraBo.save(desgloseCamara);
                } // if
            }
            break;
        case Constantes.CDEST_MATCHED:
            LOG.debug("[respuestaMercador] Matched");
            for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) {
                desgloseCamara = om.getDesgloseCamara();
                if (desgloseCamara.getTmct0estadoByCdestadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingCasadaPdteLiq").getIdestado()) < 0) {
                    desgloseCamara.setTmct0estadoByCdestadoentrec(estadosMap.get("estadoClearingCasadaPdteLiq"));
                    tmct0desglosecamaraBo.save(desgloseCamara);
                } // if
            }
            break;
        case Constantes.CDEST_FALLIDA:
            LOG.debug("[respuestaMercado] Fallida");
            if (registro.getBloquesDestalle() > 0) {
                LOG.debug("[tratamientoRegistroRespuesta] Procesando bloques de detalle de error ....");
                S3Errores datos_error;

                nuevaLiquidacion = generaLiquidacionDesdeRegistro(Constantes.CHAR_MERCADO);
                nuevaLiquidacion.setCdestadoS3("ERR");
                s3LiquidacionDao.save(nuevaLiquidacion);
                for (RegistroRespuestaDetalleFicheroMegara bloqueDetalle : registro.getBloquesDestalleList()) {
                    datos_error = new S3Errores();
                    datos_error.setS3liquidacion(nuevaLiquidacion);
                    datos_error.setDatoErrorCliente(bloqueDetalle.getDatoDiscrepanteCliente());
                    datos_error.setDatoErrorSV(bloqueDetalle.getDatoDiscrepanteSV());

                    // Se busca la tradución del código en la tabla
                    Datosdiscrepantes datos_discrepante = datosdiscrepantesBo
                            .findByCodigo(bloqueDetalle.getCodigoDatoDiscrepante());
                    // si no viene en la tabla o es NARR(texto libre)
                    if (datos_discrepante == null
                            || bloqueDetalle.getCodigoDatoDiscrepante().equals(Constantes.CD_DATOS_DISCREPANTE_NARR)) {
                        datos_error.setDserror(bloqueDetalle.getTextoDatoDiscrepante());
                    } 
                    else {
                        datos_error.setDserror(recuperarDescripcionDatosDiscrepantes(aliasFinal, datos_discrepante));
                    } // else

                    s3ErroresBo.save(datos_error);
                } // for
            } // if (registro.getBloquesDestalle() > 0)
            for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) {
                desgloseCamara = om.getDesgloseCamara();
                if (desgloseCamara.getTmct0estadoByCdestadoentrec().getIdestado()
                        .compareTo(estadosMap.get("estadoClearingLiqFallida").getIdestado()) < 0) {
                    desgloseCamara.setTmct0estadoByCdestadoentrec(estadosMap.get("estadoClearingLiqFallida"));
                    tmct0desglosecamaraBo.save(desgloseCamara);
                } // if
            }
            break;
        case Constantes.CDEST_LIQUIDADA:
            pataMercadoLiquidada(alcOrdenes);
            break;
        } // switch (registro.getEstadoOperacion())        
        
    }
    
    private void generaMovimientoCuentasAliasMercado(S3Liquidacion nueva_liquidacion, final Tmct0estado estado, 
            Tmct0desglosecamara tmct0desglosecamara) {
        final List<Tmct0MovimientoCuentaAlias> movCuentaAliasList;
        final Tmct0MovimientoCuentaAlias movimientoCuentaAlias;
        final Tmct0alc tmct0alc;
        final String alias_final;
        
        tmct0alc = tmct0desglosecamara.getTmct0alc();
        alias_final = tmct0alc.getTmct0alo().getTmct0bok().getCdalias().trim();
        movimientoCuentaAlias = new Tmct0MovimientoCuentaAlias();
        // Crear el movimiento cuenta alias para la liquidación recibida
        if (tmct0alc.getTmct0alo().getCdtpoper().toString().equals(Constantes.COMPRA)) {
            movimientoCuentaAlias.setCdoperacion(configParams.get("compra_mercado_mov"));
            movimientoCuentaAlias.setSignoanotacion('E');
            movimientoCuentaAlias.setTitulos(nueva_liquidacion.getNutituloLiquidados());
            movimientoCuentaAlias.setImefectivo(nueva_liquidacion.getImporteNetoLiquidado().negate());
        } else {
            movimientoCuentaAlias.setCdoperacion(configParams.get("venta_mercado_mov"));
            movimientoCuentaAlias.setSignoanotacion('S');
            movimientoCuentaAlias.setTitulos(nueva_liquidacion.getNutituloLiquidados().negate());
            movimientoCuentaAlias.setImefectivo(nueva_liquidacion.getImporteNetoLiquidado());
        } // else
        movimientoCuentaAlias.setRefmovimiento(nueva_liquidacion.getIdFichero());
        movimientoCuentaAlias.setCdaliass(alias_final);
        movimientoCuentaAlias.setIsin(tmct0alc.getTmct0alo().getCdisin());
        movimientoCuentaAlias.setMercado(tmct0desglosecamara.getTmct0CamaraCompensacion().getCdCodigo());
        movimientoCuentaAlias.setTradedate(tmct0alc.getFeejeliq());
        movimientoCuentaAlias.setSettlementdate(nueva_liquidacion.getFechaValor());
        movimientoCuentaAlias.setCdcodigocuentaliq(tmct0CuentasDeCompensacionBo
                .findByTmct0desglosecamara(tmct0desglosecamara).getTmct0CuentaLiquidacion().getCdCodigo());
        movimientoCuentaAlias.setCorretaje(BigDecimal.ZERO);

        LOG.debug("[tratamientoRegistroRespuesta] Comprobando movimientos cuenta alias de: "
                + registro.getReferenciaSV());
        movCuentaAliasList = tmct0MovimientoCuentaAliasDao.findByRefMovimiento(registro.getReferenciaSV());
        LOG.debug("[tratamientoRegistroRespuesta] movCuentaAlias encontrados: " + movCuentaAliasList.size());
        for (Tmct0MovimientoCuentaAlias movCuentaAlias : movCuentaAliasList) {
            if (movCuentaAlias.getSettlementdate() == null) {
                // Se trata del movimiento generado en el env�o a Megara.
                // Hay que restarle t�tulos y efectivo si no queda
                // totalmente liquidada o eliminarlo si se liquida
                // totalmente
                if (estado.getIdestado() == ASIGNACIONES.LIQUIDADA.getId()
                        || estado.getIdestado() == ASIGNACIONES.LIQUIDADA_PROPIA.getId()) {
                    LOG.debug(
                            "[tratamientoRegistroRespuesta] Eliminando movimiento cuenta alias generado en envio");
                    tmct0MovimientoCuentaAliasBo.delete(movCuentaAlias);
                } else {
                    LOG.debug(
                            "[tratamientoRegistroRespuesta] Restando titulos y efectivo al movimiento cuenta alias generado en envio ");
                    if (movCuentaAlias.getImefectivo().compareTo(BigDecimal.ZERO) < 0) {
                        movCuentaAlias.setImefectivo(
                                movCuentaAlias.getImefectivo().add(nueva_liquidacion.getImporteNetoLiquidado()));
                        movCuentaAlias.setTitulos(
                                movCuentaAlias.getTitulos().subtract(nueva_liquidacion.getNutituloLiquidados()));
                    } else {
                        movCuentaAlias.setImefectivo(movCuentaAlias.getImefectivo()
                                .subtract(nueva_liquidacion.getImporteNetoLiquidado()));
                        movCuentaAlias.setTitulos(
                                movCuentaAlias.getTitulos().add(nueva_liquidacion.getNutituloLiquidados()));
                    }
                } // else
                break; // Ya se ha encontrado el registro. Se sale
            } // if
        } // for
        tmct0MovimientoCuentaAliasBo.save(movimientoCuentaAlias);
    }
    
    private void pataMercadoLiquidada(AlcOrdenes alcOrdenes) {
        final BigDecimal titulosAsignados, totalLiquidados;
        final S3Liquidacion liquidacion;
        final Tmct0estado estado;
        final int cmpTitulos;
        BigDecimal titulosYaLiquidados;

        LOG.debug("[pataMercadoLiquidada] Inicio...");
        titulosYaLiquidados = s3LiquidacionDao.sumTitulosLiquidadosPorReferencia(alcOrdenes.getReferenciaS3(), "LIQ");
        if(titulosYaLiquidados == null) {
            titulosYaLiquidados = BigDecimal.ZERO;
        }
        titulosAsignados = alcOrdenes.getTitulosAsignados();
        totalLiquidados = titulosYaLiquidados.add(registro.getTitulosLiquidados());
        cmpTitulos = totalLiquidados.compareTo(titulosAsignados);
        if(cmpTitulos > 0) {
            alcAlarmasList.add(format(
            "La liquidacion con referencia S3 {0} sería número mayor de titulos que los pendientes: {1} contra {2}",
            alcOrdenes.getReferenciaS3(), registro.getTitulosLiquidados().toString(), titulosAsignados.toString()));
            return;
        }
        estado = estadosMap.get(cmpTitulos == 0 ? "estadoClearingLiq" : "estadoClearingLiqParcial");
        liquidacion = generaLiquidacionDesdeRegistro(Constantes.CHAR_MERCADO);
        liquidacion.setImporteEfectivo(registro.getImporteNetoLiquidado());
        s3LiquidacionDao.save(liquidacion);
        generaMovimientoCuentasAliasMercado(liquidacion, estado, 
            alcOrdenes.getOrdenesMercado().get(0).getDesgloseCamara());
        for(AlcOrdenMercado om : alcOrdenes.getOrdenesMercado()) { 
          om.getDesgloseCamara().setTmct0estadoByCdestadoentrec(estado);
        }
        LOG.debug("[pataMercadoLiquidada] Fin");
    }
    
    /**
     * Recupera los datos del alias.
     * 
     * @param cdalias Identificador del alias.
     * @param datos_discrepante Datos discrepantes a actualizar.
     * @return <code>java.lang.String - </code>Datos del alias.
     */
    private String recuperarDescripcionDatosDiscrepantes(final String cdalias, Datosdiscrepantes datos_discrepante) {
        LOG.debug("[recuperarDescripcionDatosDiscrepantes] Inicio ...");
        String descripcionDatosDiscrepantes = null;
        if (StringUtils.isNotEmpty(cdalias)) {
            final Alias alias = aliasBo.findByCdaliass(cdalias);
            if (alias != null) {
                final Idioma idioma = alias.getIdioma();
                if (idioma != null) {
                    if (Idioma.COD_IDIOMA_ES.equals(idioma.getCodigo())) {
                        descripcionDatosDiscrepantes = datos_discrepante.getDescripcion_es();
                    } // if
                } // if
            } // if
        } // if
        if (StringUtils.isEmpty(descripcionDatosDiscrepantes)) {
            descripcionDatosDiscrepantes = datos_discrepante.getDescripcion_eng();
        } // if
        LOG.debug("[recuperarDescripcionDatosDiscrepantes] Fin ...");
        return descripcionDatosDiscrepantes;
    } // recuperarDescripcionDatosDiscrepantes

    /**
     * Calcula los datos restantes para la liquidación S3 de la pata cliente
     * recibida.
     * 
     * @param liquidacion_tratar Liquidación que se está tratando.
     * @param efectivo Efectivo del/de los ALC.
     * @param corretaje Corretaje del/de los ALC.
     * @param titulos Titulos del/de los ALC.
     * @return <code>Tmct0estado - </code>Estado a establecer al/a los ALC.
     * @throws RecepcionFicheroMegaraException Si el/los ALC ya se encontraba
     * totalmente liquidado.
     */
    private Tmct0estado calcularDatosCliente(S3Liquidacion liquidacion_tratar, BigDecimal efectivo,
            BigDecimal corretaje, BigDecimal titulos) throws RecepcionFicheroMegaraException {
        LOG.debug("[calcularDatosCliente] Inicio ...");

        BigDecimal nutitulosTotalLiq = liquidacion_tratar.getNutituloLiquidados();
        BigDecimal importeNetoTotalLiq = liquidacion_tratar.getImporteNetoLiquidado();
        BigDecimal efectivoTotalLiq = liquidacion_tratar.getImporteEfectivo();

        // buscamos todas las liquidaciones parciales para sumarselas (incluida
        // la que se esta tratando)
        List<S3Liquidacion> lista_liquidacion_parciales = s3LiquidacionDao
                .findByidFicheroAndCdestadoS3(liquidacion_tratar.getIdFichero(), liquidacion_tratar.getCdestadoS3());

        for (S3Liquidacion datos_liq_parcial : lista_liquidacion_parciales) {
            // sumamos todas las cantidades parciales
            nutitulosTotalLiq = nutitulosTotalLiq.add(datos_liq_parcial.getNutituloLiquidados());
            importeNetoTotalLiq = importeNetoTotalLiq.add(datos_liq_parcial.getImporteNetoLiquidado());
            efectivoTotalLiq = efectivoTotalLiq.add(datos_liq_parcial.getImporteEfectivo());
        } // for
        LOG.debug(
                "[calcularDatosCliente] Sumatorio de liquidaciones ya recibidas [nutitulosTotalLiq: '{}'; importeNetoTotalLiq: '{}'; efectivoTotalLiq: '{}']",
                nutitulosTotalLiq, importeNetoTotalLiq, efectivoTotalLiq);

        // Si ya se han liquidado previamente todos los títulos de la orden
        if (nutitulosTotalLiq.subtract(liquidacion_tratar.getNutituloLiquidados()).compareTo(titulos) == 0) {
            // s3LiquidacionBo.delete(liquidacion_tratar);
            throw new RecepcionFicheroMegaraException("La pata cliente ya se encuentra totalmente liquidada");
        } // if

        int resul_efectivo = importeNetoTotalLiq.compareTo(efectivo);
        if (resul_efectivo == -1) {
            // si IMPORTE_LIQUIDADO_TOTAL < EFECTIVO (entonces lo metemos en el
            // efectivo porque no lo hemos superado)
            liquidacion_tratar.setImporteEfectivo(liquidacion_tratar.getImporteNetoLiquidado());
            liquidacion_tratar.setImporteCorretaje(BigDecimal.ZERO);
        } else {
            // si IMPORTE_LIQUIDADO_TOTAL >= EFECTIVO (al pasarnos del efectivo
            // entonces metemos lo que falta para llegar al
            // efectivo y el resto al corretaje)

            // restamos el efectivo al que queremos llegar menos el efectivo
            // total liquidado (a falta de esta ultima
            // liquidacion)
            BigDecimal resto_efectivo = efectivo.subtract(efectivoTotalLiq);
            liquidacion_tratar.setImporteEfectivo(resto_efectivo);

            // por ultimo restamos el importe neto de esta liquidacion menos el
            // resto efectivo (en el caso de que le faltaba
            // de incluir algo)
            BigDecimal resto_corretaje = liquidacion_tratar.getImporteNetoLiquidado().subtract(resto_efectivo);
            liquidacion_tratar.setImporteCorretaje(resto_corretaje);
        } // else

        LOG.debug("[calcularDatosCliente] Fin ...");
        int resul_titulos = nutitulosTotalLiq.compareTo(titulos);
        if (resul_titulos == -1) {
            // si NUTITULOLIQUIDADOS < NUTITULO (ALC o NETEADOS)
            // return
            // tmct0estadoBo.findById(CLEARING.LIQUIDADA_PARCIAL.getId());
            return estadosMap.get("estadoClearingLiqParcial");
        } else {
            // return tmct0estadoBo.findById(CLEARING.LIQUIDADA.getId());
            return estadosMap.get("estadoClearingLiq");
        } // else
    } // calcularDatosCliente


    /**
     * Busca los FIXML de alta de colateral recibidos para la operación
     * especificada y los actualiza con el número de instrucción de liquidación
     * recibida.
     * 
     * @param registro Datos de la operación.
     */
    private void procesarCOUI(RegistroRespuestaCabeceraFicheroMegara registro) {
        LOG.debug("[procesarCOUI] Inicio ...");
        LOG.debug(
                "[procesarCOUI] Buscando: importeEfectivoInstruccion: {}; importeEfectivoVivoOperacion: {}; isin: {}; volumenOperacion: {};"
                        + " volumenVivoOperacion: {}; fileType: {}; fechaLiquidacion: {}",
                registro.getImporteNetoLiquidado(), registro.getImporteNetoLiquidado(), registro.getIsin(),
                registro.getTitulosLiquidados(), registro.getTitulosLiquidados(),
                sibbac.business.fallidos.utils.Constantes.CSECLENDING_FILENAME, registro.getFechaValor());
        FixmlFallido coui = fixmlFallidoDao.findFixmlForS3Colateral(registro.getImporteNetoLiquidado(),
                registro.getImporteNetoLiquidado(), registro.getIsin(), registro.getTitulosLiquidados(),
                registro.getTitulosLiquidados(), sibbac.business.fallidos.utils.Constantes.CSECLENDING_FILENAME,
                registro.getFechaValor());

        if (coui != null) {
            LOG.debug("[procesarCOUI] FixmlFallido encontrado. estableciendo ILIQS3: {}", registro.getReferenciaSV());
            coui.setIlqs3(registro.getReferenciaSV());
            fixmlFallidoDao.save(coui);
        } else {
            LOG.debug("[procesarCOUI] FixmlFallido no encontrado ...");
            colateralesNoencontrados.add(registro);
        }
        LOG.debug("[procesarCOUI] Fin ...");
    } // procesarCOUI

    /**
     * Busca los FIXML de baja de colateral recibidos para la operación
     * especificada y los actualiza con el número de instrucción de liquidación
     * recibida.
     * 
     * @param registro Datos de la operación.
     */
    private void procesarCAUI(RegistroRespuestaCabeceraFicheroMegara registro) {
        LOG.debug("[procesarCAUI] Inicio ...");
        LOG.debug(
                "[procesarCAUI] Buscando: importeEfectivoInstruccion: {}; importeEfectivoVivoOperacion: {}; isin: {}; volumenOperacion: {};"
                        + " volumenVivoOperacion: {}; fileType: {}; fechaLiquidacion: {}",
                registro.getImporteNetoLiquidado(), registro.getImporteNetoLiquidado(), registro.getIsin(),
                registro.getTitulosLiquidados(), registro.getTitulosLiquidados(),
                sibbac.business.fallidos.utils.Constantes.CSECLENDCANC_FILENAME, registro.getFechaValor());
        FixmlFallido caui = fixmlFallidoDao.findFixmlForS3Colateral(registro.getImporteNetoLiquidado(),
                registro.getImporteNetoLiquidado(), registro.getIsin(), registro.getTitulosLiquidados(),
                registro.getTitulosLiquidados(), sibbac.business.fallidos.utils.Constantes.CSECLENDCANC_FILENAME,
                registro.getFechaValor());

        if (caui != null) {
            LOG.debug("[procesarCAUI] FixmlFallido encontrado. estableciendo ILIQS3: {}", registro.getReferenciaSV());
            caui.setIlqs3(registro.getReferenciaSV());
            fixmlFallidoDao.save(caui);
        } else {
            LOG.debug("[procesarCAUI] FixmlFallido no encontrado ...");
            colateralesNoencontrados.add(registro);
        }
        LOG.debug("[procesarCAUI] Fin ...");
    } // procesarCAUI

    /**
     * Busca los FIXML de ajuste de colateral recibidos para la operación
     * especificada y los actualiza con el número de instrucción de liquidación
     * recibida.
     * 
     * @param registro Datos de la operación.
     */
    private void procesarCOLL(RegistroRespuestaCabeceraFicheroMegara registro) {
        LOG.debug("[procesarCOLL] Inicio ...");
        LOG.debug(
                "[procesarCOLL] Buscando: importeEfectivoInstruccion: {}; isin: {}; fileType: {}; fechaLiquidacion: {}",
                registro.getImporteNetoLiquidado(), registro.getIsin(),
                sibbac.business.fallidos.utils.Constantes.CSECLENDVALUE_FILENAME, registro.getFechaValor());
        FixmlFallido coll = fixmlFallidoDao.findFixmlForS3AjusteColateral(registro.getImporteNetoLiquidado(),
                registro.getIsin(), sibbac.business.fallidos.utils.Constantes.CSECLENDVALUE_FILENAME,
                registro.getFechaValor());

        if (coll != null) {
            LOG.debug("[procesarCOLL] FixmlFallido encontrado. estableciendo ILIQS3: {}",
                    (registro.getTipoOperacion().concat("#").concat(registro.getReferenciaSV())));
            coll.setIlqs3(registro.getTipoOperacion().concat("#").concat(registro.getReferenciaSV()));
            fixmlFallidoDao.save(coll);
        } else {
            LOG.debug("[procesarCOLL] FixmlFallido no encontrado ...");
            colateralesNoencontrados.add(registro);
        }
        LOG.debug("[procesarCOLL] Fin ...");
    } // procesarCOLL
    
    private S3Liquidacion generaLiquidacionDesdeRegistro(char origen) {
        S3Liquidacion nuevaLiquidacion;
        
        LOG.debug("[generaLiquidacionDesdeRegistro] Inicio...");
        nuevaLiquidacion = new S3Liquidacion();
        nuevaLiquidacion.setIdFichero(registro.getReferenciaSV());
        nuevaLiquidacion.setCdestadoS3(registro.getEstadoOperacion());
        nuevaLiquidacion.setFechaOperacion(registro.getFechaOperacion());
        nuevaLiquidacion.setFechaValor(registro.getFechaValor());
        nuevaLiquidacion.setNutituloLiquidados(registro.getTitulosLiquidados());
        nuevaLiquidacion.setImporteNetoLiquidado(registro.getImporteNetoLiquidado());
        nuevaLiquidacion.setFechaFichero(fechaFichero);
        nuevaLiquidacion.setHoraValor(registro.getHoraValor());
        nuevaLiquidacion.setTipoOperacion(registro.getTipoOperacion());
        nuevaLiquidacion.setInstruccionLiquidacion(registro.getInstruccionLiquidacion());
        nuevaLiquidacion.setPoolReference(registro.getPoolReference());
        nuevaLiquidacion.setIsin(registro.getIsin());
        nuevaLiquidacion.setHoraFichero(fechaFichero);
        nuevaLiquidacion.setCdorigen(origen);
        LOG.debug("[generaLiquidacionDesdeRegistro] Fin");
        return nuevaLiquidacion;
    }
    
    private void registraNoProcesado() {
      lineasRechazadas.add(registro.getLineaOriginal());
    }
    

} // RespuestaClearingNettingBo
