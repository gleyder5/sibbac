package sibbac.business.operativanetting.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.business.operativanetting.database.model.Datosdiscrepantes}.
 */

@Entity
@Table( name = DBConstants.CLEARING.DATOSDISCREPANTES )
public class Datosdiscrepantes extends ATable< Datosdiscrepantes > implements Serializable {

	private static final long	serialVersionUID	= -6412765016803115691L;

	@Column( name = "CODIGO", nullable = false, length = 10 )
	private String				codigo;

	@Column( name = "DESCRIPCION_ES", nullable = false, length = 100 )
	private String				descripcion_es;

	@Column( name = "DESCRIPCION_ENG", nullable = false, length = 100 )
	private String				descripcion_eng;

	public Datosdiscrepantes() {

	}

	public Datosdiscrepantes( String codigo, String descripcion_es, String descripcion_eng ) {
		super();
		this.codigo = codigo;
		this.descripcion_es = descripcion_es;
		this.descripcion_eng = descripcion_eng;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public String getDescripcion_es() {
		return descripcion_es;
	}

	public void setDescripcion_es( String descripcion_es ) {
		this.descripcion_es = descripcion_es;
	}

	public String getDescripcion_eng() {
		return descripcion_eng;
	}

	public void setDescripcion_eng( String descripcion_eng ) {
		this.descripcion_eng = descripcion_eng;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += ( id != null ? id.hashCode() : 0 );
		return hash;
	}

	@Override
	public boolean equals( Object object ) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if ( !( object instanceof Datosdiscrepantes ) ) {
			return false;
		}
		Datosdiscrepantes other = ( Datosdiscrepantes ) object;
		if ( ( this.id == null && other.id != null ) || ( this.id != null && !this.id.equals( other.id ) ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sibbac.business.operativanetting.database.model.S3Liquidacion[ id=" + id + " ]";
	}

}
