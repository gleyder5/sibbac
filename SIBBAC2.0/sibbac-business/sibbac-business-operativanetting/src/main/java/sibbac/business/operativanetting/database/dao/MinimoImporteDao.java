package sibbac.business.operativanetting.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sibbac.business.operativanetting.database.model.MinimoImporte;
import sibbac.business.wrappers.database.model.Alias;

/**
 * The Interface MinimoImporteDao.
 */
public interface MinimoImporteDao extends JpaRepository<MinimoImporte, Long> {

	/**
	 * Find by date and alias.
	 *
	 * @param alias the alias
	 * @return the list
	 */
	public List<MinimoImporte> findAllByAlias(final Alias alias);
	
	/**
	 * Find all by alias is null.
	 *
	 * @return the list
	 */
	public List<MinimoImporte> findAllByAliasIsNull();

}
