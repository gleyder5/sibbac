/**
 * # RecepcionFicheroMegaraException.java
 * # Fecha de creación: 04/11/2015
 */
package sibbac.business.operativanetting.exceptions;

/**
 * Se lanza para indicar que un un alc o un desglosecamara no cumplen las condiciones necesarias para ser enviadas a
 * Megara.
 * 
 * @author XI316153
 * @version 1.0
 * @since SIBBAC2.0
 */
public class RecepcionFicheroMegaraException extends Exception {

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTANTES ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 751007897670247813L;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Constructor I. Genera una nueva excepción con el mensaje especificado.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   */
  public RecepcionFicheroMegaraException(String mensaje) {
    super(mensaje);
  } // RecepcionFicheroMegaraException

  /**
   * Constructor II. Genera una nueva excepción con el mensaje especificado y la excepción producida.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   * @param causa Causa por la que se ha lanzado la excepción.
   */
  public RecepcionFicheroMegaraException(String mensaje, Throwable causa) {
    super(mensaje, causa);
  } // RecepcionFicheroMegaraException

} // RecepcionFicheroMegaraException
