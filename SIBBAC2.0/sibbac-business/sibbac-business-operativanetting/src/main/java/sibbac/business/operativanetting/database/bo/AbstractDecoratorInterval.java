package sibbac.business.operativanetting.database.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import sibbac.business.operativanetting.database.model.FechaInicioFechaHasta;
import sibbac.business.operativanetting.exceptions.DateContainedIntervalException;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.database.bo.AbstractBo;

/**
 * The Class AbstractDecoratorInterval.
 *
 * @param <TYPE>
 *            the generic type
 * @param <PK>
 *            the generic type
 * @param <DAO>
 *            the generic type
 */
public abstract class AbstractDecoratorInterval<TYPE extends FechaInicioFechaHasta, PK extends Serializable, DAO extends JpaRepository<TYPE, PK>>
		extends AbstractBo<TYPE, PK, DAO> {

	@Autowired
	private AliasBo aliasBo;

	/**
	 * Checks if is in interval.
	 *
	 * @param interesDemoras
	 *            the interes demoras
	 * @param fechaInicio
	 *            the fecha inicio
	 * @param fechaHasta
	 *            the fecha hasta
	 * @return the list
	 */
	protected List<TYPE> isInInterval(List<TYPE> interesDemoras,
			Date fechaInicio, Date fechaHasta) {
		List<TYPE> result = new ArrayList<TYPE>();
		Interval interval;
		DateTime startDate;
		DateTime endDate;
		for (TYPE interesesDemora : interesDemoras) {
			startDate = new DateTime(interesesDemora.getFechaInicio().getTime());
			endDate = new DateTime(interesesDemora.getFechaHasta().getTime()).plus(Days.ONE);
			interval = new Interval(startDate,endDate);
			if (interval.contains(fechaInicio.getTime())
					|| interval.contains(fechaHasta.getTime())) {
				result.add(interesesDemora);
			}
		}
		return result;
	}

	/**
	 * Checks if is in interval.
	 *
	 * @param interesDemoras
	 *            the interes demoras
	 * @param fecha
	 *            the fecha
	 * @return the list
	 */
	protected List<TYPE> isInInterval(List<TYPE> interesDemoras, Date fecha) {
		return isInInterval(interesDemoras, fecha, fecha);
	}

	@SuppressWarnings("unchecked")
	public TYPE save(TYPE type) {
		TYPE result;
		Alias alias = null;
		List<TYPE> fechaInicioFechaHastas;
		if (type != null && type.getAlias() != null) {
			alias = aliasBo.findById(type.getAlias().getId());
		}
		fechaInicioFechaHastas = findByAlias(alias);
		fechaInicioFechaHastas = isInInterval(fechaInicioFechaHastas,
				type.getFechaInicio(), type.getFechaHasta());
		if (fechaInicioFechaHastas.size() == 0) {
			result = super.save(type);
		} else {
			throw new DateContainedIntervalException(
					(List<FechaInicioFechaHasta>) fechaInicioFechaHastas);
		}
		return result;
	}

	public abstract List<TYPE> findByAlias(Alias alias);
}
