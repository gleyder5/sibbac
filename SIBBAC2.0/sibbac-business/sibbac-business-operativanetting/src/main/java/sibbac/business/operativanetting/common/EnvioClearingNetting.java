package sibbac.business.operativanetting.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ALC_ORDENES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;

/**
 * Procesa los ALC para generar el fichero a enviar a Megara.
 * 
 * @author XI316153
 */
abstract class EnvioClearingNetting {

  private static final int[] ESTADOS = { CLEARING.INCIDENCIA.getId(), CLEARING.ENVIADO_FICHERO.getId(),
      CLEARING.ENVIADO_FICHERO_CUENTA_PROPIA.getId(), CLEARING.RECHAZADO_FICHERO.getId(), CLEARING.INCIDENCIA.getId(),
      CLEARING.ACEPTADA_ANULACION.getId(), ALC_ORDENES.SIN_FACTURA.getId(), ASIGNACIONES.EN_CURSO.getId() };

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  final String lineSeparator;

  final Map<Integer, Tmct0estado> estadosMap;

  Map<String, String> configParams;

  List<String> cuentasTerceros;

  EnvioClearingNetting() {
    lineSeparator = System.getProperty("line.separator");
    estadosMap = new HashMap<>();
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS
  abstract public void initProcess(Map<String, String> configParams);

  /**
   * Llamada de inicialización previa a la ejecución del proceso.
   * 
   * @param configParams Parámetros de configuración del proceso.
   */
  void initProcess(Map<String, String> configParams, Tmct0CuentasDeCompensacionBo tmct0CuentasDeCompensacionBo,
      Tmct0estadoBo estadoBo) {
    this.configParams = configParams;
    cuentasTerceros = tmct0CuentasDeCompensacionBo.findCuentasTerceros();
    for (int idEstado : ESTADOS) {
      estadosMap.put(idEstado, estadoBo.findById(idEstado));
    }
  }

  boolean isCuentaValoresInvalida(final String codigoCuentaValores) {
    final String trimed;

    if (codigoCuentaValores == null)
      return true;
    trimed = codigoCuentaValores.trim();
    if (trimed.length() != 18)
      return true;
    for (char s : trimed.toCharArray())
      if (!Character.isDigit(s))
        return true;
    return false;
  }

} // EnvioClearingNetting
