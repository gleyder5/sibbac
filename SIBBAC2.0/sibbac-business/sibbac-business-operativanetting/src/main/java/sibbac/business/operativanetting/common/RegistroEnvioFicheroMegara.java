package sibbac.business.operativanetting.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.operativanetting.common.Utilidad.CLEARING_TIPO;

public class RegistroEnvioFicheroMegara {

    /** Referencia para la inserción de logs. */
    private static final Logger LOG = LoggerFactory.getLogger(RegistroEnvioFicheroMegara.class);
  
  // datos necesarios para el fichero
  private String cdemisor;
  private String cdtipreg;
  private String nureftraDato;
  private String nureftraTipo;
  private String mcdesglose;
  private String sliquidacion;
  private String cdtpoper;
  private String cdintpop;
  private String platcont;
  private String cdisinxx;
  private String txdescri;
  private Date fhcontra;
  private Date fhvalcon;
  private Date fhteoliq;
  private BigDecimal nutitulo;
  private String cddivisa;
  private BigDecimal imcpreci;
  private BigDecimal imcprecc;
  private BigDecimal imefecti;
  private BigDecimal impornet;
  private BigDecimal imcanon;
  private BigDecimal imcomban;
  private BigDecimal imcomsvb;
  private String cdcontra;
  private String cdentdep;
  private String cdbolsa;
  private Integer nubancod;
  private Integer nusucurd;
  private BigDecimal nuctadep;
  private String cdclient;
  private String nbclient;
  private BigDecimal impcliente;
  private Date fhvencim;
  private BigDecimal impnetvto;
  private String cdcliter;
  private String ourparticipe;
  private String theirpartici;
  private String cdalias;
  private Integer nuctacontra;
  private String cdcontracsd;
  private Integer nuctacltcontra;
  private String cdparticipante;
  private String cdecc;
  private String cdmiembrocom;
  private String nuctamiembrocom;
  private String nurefecc;
  private String poolreference;
  private String cancelaciones;

  // datos auxiliares para el netting
  private Long idnetting;
  private Long idmovimientoCuentaAlias;
  private BigDecimal imcomisn;
  private BigDecimal imefeagr;
  private String camaraCompensacion;
  private String cdCuentaCompensacion;
  private String cdCuentaLiquidacion;
  private String sentido;

  public RegistroEnvioFicheroMegara() {
    cdemisor = "SIB";
    nutitulo = BigDecimal.ZERO;
    imcpreci = BigDecimal.ZERO;
    imcprecc = BigDecimal.ZERO;
    imefecti = BigDecimal.ZERO;
    impornet = BigDecimal.ZERO;
    imcanon = BigDecimal.ZERO;
    imcomban = BigDecimal.ZERO;
    imcomsvb = BigDecimal.ZERO;
    impcliente = BigDecimal.ZERO;
    impnetvto = BigDecimal.ZERO;
    nubancod = 0;
    nusucurd = 0;
    nubancod = 0;
    mcdesglose = "N";
    sliquidacion = "SCLV";
    nuctacontra = 0;
    nuctacltcontra = 0;
    idnetting = new Long(-1);
  }

  public String getCdemisor() {
    return this.cdemisor;
  }

  public void setCdemisor(String cdemisor) {
    this.cdemisor = cdemisor;
  }

  public String getNureftraDato() {
    return this.nureftraDato;
  }

  public void setNureftraTipo(String nureftraTipo) {
    this.nureftraTipo = nureftraTipo;
  }

  public String getNureftraTipo() {
    return this.nureftraTipo;
  }

  public void setNureftraDato(String nureftraDato) {
    this.nureftraDato = nureftraDato;
  }

  public String getMcdesglose() {
    return this.mcdesglose;
  }

  public void setMcdesglose(String mcdesglose) {
    this.mcdesglose = mcdesglose;
  }

  public String getSliquidacion() {
    return this.sliquidacion;
  }

  public void setSliquidacion(String sliquidacion) {
    this.sliquidacion = sliquidacion;
  }

  public String getCdtpoper() {
    return this.cdtpoper;
  }

  public void setCdtpoper(String cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public String getCdintpop() {
    return this.cdintpop;
  }

  public void setCdintpop(String cdintpop) {
    this.cdintpop = cdintpop;
  }

  public String getPlatcont() {
    return this.platcont;
  }

  public void setPlatcont(String platcont) {
    this.platcont = platcont;
  }

  public String getCdisinxx() {
    return this.cdisinxx;
  }

  public void setCdisinxx(String cdisinxx) {
    this.cdisinxx = cdisinxx;
  }

  public String getTxdescri() {
    return this.txdescri;
  }

  public void setTxdescri(String txdescri) {
    this.txdescri = txdescri;
  }

  public Date getFhcontra() {
    return this.fhcontra;
  }

  public void setFhcontra(Date fhcontra) {
    this.fhcontra = fhcontra;
  }

  public Date getFhvalcon() {
    return this.fhvalcon;
  }

  public void setFhvalcon(Date fhvalcon) {
    this.fhvalcon = fhvalcon;
  }

  public Date getFhteoliq() {
    return this.fhteoliq;
  }

  public void setFhteoliq(Date fhteoliq) {
    this.fhteoliq = fhteoliq;
  }

  public BigDecimal getNutitulo() {
    return this.nutitulo;
  }

  public void setNutitulo(BigDecimal nutitulo) {
    this.nutitulo = nutitulo;
  }

  public String getCddivisa() {
    return this.cddivisa;
  }

  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  public BigDecimal getImcpreci() {
    return this.imcpreci;
  }

  public void setImcpreci(BigDecimal imcpreci) {
    this.imcpreci = imcpreci;
  }

  public BigDecimal getImcprecc() {
    return this.imcprecc;
  }

  public void setImcprecc(BigDecimal imcprecc) {
    this.imcprecc = imcprecc;
  }

  public BigDecimal getImefecti() {
    return this.imefecti;
  }

  public void setImefecti(BigDecimal imefecti) {
    this.imefecti = imefecti;
  }

  public BigDecimal getImpornet() {
    return this.impornet;
  }

  public void setImpornet(BigDecimal impornet) {
    this.impornet = impornet;
  }

  public BigDecimal getImcanon() {
    return this.imcanon;
  }

  public void setImcanon(BigDecimal imcanon) {
    this.imcanon = imcanon;
  }

  public BigDecimal getImcomban() {
    return this.imcomban;
  }

  public void setImcomban(BigDecimal imcomban) {
    this.imcomban = imcomban;
  }

  public BigDecimal getImcomsvb() {
    return this.imcomsvb;
  }

  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  public String getCdcontra() {
    return this.cdcontra;
  }

  public void setCdcontra(String cdcontra) {
    this.cdcontra = cdcontra;
  }

  public String getCdentdep() {
    return this.cdentdep;
  }

  public void setCdentdep(String cdentdep) {
    this.cdentdep = cdentdep;
  }

  public String getCdbolsa() {
    return this.cdbolsa;
  }

  public void setCdbolsa(String cdbolsa) {
    this.cdbolsa = cdbolsa;
  }

  public Integer getNubancod() {
    return this.nubancod;
  }

  public void setNubancod(Integer nubancod) {
    this.nubancod = nubancod;
  }

  public Integer getNusucurd() {
    return this.nusucurd;
  }

  public void setNusucurd(Integer nusucurd) {
    this.nusucurd = nusucurd;
  }

  public BigDecimal getNuctadep() {
    return this.nuctadep;
  }

  public void setNuctadep(BigDecimal nuctadep) {
    this.nuctadep = nuctadep;
  }

  public String getCdclient() {
    return this.cdclient;
  }

  public void setCdclient(String cdclient) {
    this.cdclient = cdclient;
  }

  public String getNbclient() {
    return this.nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public BigDecimal getImpcliente() {
    return this.impcliente;
  }

  public void setImpcliente(BigDecimal impcliente) {
    this.impcliente = impcliente;
  }

  public Date getFhvencim() {
    return this.fhvencim;
  }

  public void setFhvencim(Date fhvencim) {
    this.fhvencim = fhvencim;
  }

  public BigDecimal getImpnetvto() {
    return this.impnetvto;
  }

  public void setImpnetvto(BigDecimal impnetvto) {
    this.impnetvto = impnetvto;
  }

  public String getCdcliter() {
    return this.cdcliter;
  }

  public void setCdcliter(String cdcliter) {
    this.cdcliter = cdcliter;
  }

  public String getOurparticipe() {
    return this.ourparticipe;
  }

  public void setOurparticipe(String ourparticipe) {
    this.ourparticipe = ourparticipe;
  }

  public String getTheirpartici() {
    return this.theirpartici;
  }

  public void setTheirpartici(String theirpartici) {
    this.theirpartici = theirpartici;
  }

  public String getCdalias() {
    return this.cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  public Integer getNuctacontra() {
    return this.nuctacontra;
  }

  public void setNuctacontra(Integer nuctacontra) {
    this.nuctacontra = nuctacontra;
  }

  public Integer getNuctacltcontra() {
    return this.nuctacltcontra;
  }

  public void setNuctacltcontra(Integer nuctacltcontra) {
    this.nuctacltcontra = nuctacltcontra;
  }

  public String getCdparticipante() {
    return this.cdparticipante;
  }

  public void setCdparticipante(String cdparticipante) {
    this.cdparticipante = cdparticipante;
  }

  public String getCdecc() {
    return this.cdecc;
  }

  public void setCdecc(String cdecc) {
    this.cdecc = cdecc;
  }

  public String getCdmiembrocom() {
    return this.cdmiembrocom;
  }

  public void setCdmiembrocom(String cdmiembrocom) {
    this.cdmiembrocom = cdmiembrocom;
  }

  public String getNuctamiembrocom() {
    return this.nuctamiembrocom;
  }

  public void setNuctamiembrocom(String nuctamiembrocom) {
    this.nuctamiembrocom = nuctamiembrocom;
  }

  public String getNurefecc() {
    return this.nurefecc;
  }

  public void setNurefecc(String nurefecc) {
    this.nurefecc = nurefecc;
  }

  public String getPoolreference() {
    return this.poolreference;
  }

  public void setPoolreference(String poolreference) {
    this.poolreference = poolreference;
  }

  public String getCancelaciones() {
    return this.cancelaciones;
  }

  public void setCancelaciones(String cancelaciones) {
    this.cancelaciones = cancelaciones;
  }

  public Long getIdnetting() {
    return this.idnetting;
  }

  public void setIdnetting(Long idnetting) {
    this.idnetting = idnetting;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public BigDecimal getImefeagr() {
    return imefeagr;
  }

  public void setImefeagr(BigDecimal imefeagr) {
    this.imefeagr = imefeagr;
  }

  public String getCdtipreg() {
    return cdtipreg;
  }

  public void setCdtipreg(String cdtipreg) {
    this.cdtipreg = cdtipreg;
  }

  public String getCdcontracsd() {
    return cdcontracsd;
  }

  public void setCdcontracsd(String cdcontracsd) {
    this.cdcontracsd = cdcontracsd;
  }

  public String getCamaraCompensacion() {
    return camaraCompensacion;
  }

  public void setCamaraCompensacion(String camaraCompensacion) {
    this.camaraCompensacion = camaraCompensacion;
  }

  public String getCdCuentaCompensacion() {
    return cdCuentaCompensacion;
  }

  public void setCdCuentaCompensacion(String cdCuentaCompensacion) {
    this.cdCuentaCompensacion = cdCuentaCompensacion;
  }

  public Long getIdMovimientoCuentaAlias() {
    return idmovimientoCuentaAlias;
  }

  public void setIdMovimientoCuentaAlias(Long idMovimientoCuentaAlias) {
    this.idmovimientoCuentaAlias = idMovimientoCuentaAlias;
  }

  public String getCdCuentaLiquidacion() {
    return cdCuentaLiquidacion;
  }

  public void setCdCuentaLiquidacion(String cdCuentaLiquidacion) {
    this.cdCuentaLiquidacion = cdCuentaLiquidacion;
  }

  public String getSentido() {
    return sentido;
  }

  public void setSentido(String sentido) {
    this.sentido = sentido;
  }
  
  public void setCuentaS3(final String cuentaS3) {
      final String cuenta;
      
      if(cuentaS3 != null) {
          cuenta = cuentaS3.trim();
          LOG.debug("[envioOperacionesCliente] cuenta: '{}", cuenta );
          if (cuenta.length() == 18) {
              try {
                  setNubancod(Integer.valueOf(cuenta.substring(0, 4)));
                  setNusucurd(Integer.valueOf(cuenta.substring(4, 8)));
                  setNuctadep(new BigDecimal(cuenta.substring(8)));
                  return;
              } catch (NumberFormatException nfe) {
                  LOG.warn("[setCuentaS3] La cuenta no respeta el formato esperado: {}", cuentaS3);
              } 
          }
          else 
              LOG.warn("[setCuentaS3] La cuenta no respeta la longitud esperado: {}", cuentaS3);
      }
      else 
          LOG.warn("[setCuentaS3] Se ha recibido un valor nulo");
  }

  public List<RecordFile> FormatearLineaFicheroRegistro01() {

    List<RecordFile> linea_registros = new ArrayList<RecordFile>();

    cdtipreg = "01";

    linea_registros.add(new RecordFile(cdemisor, 25));
    linea_registros.add(new RecordFile(cdtipreg, 2));

    // Nureftra Clientes: C + Nucnfliq (11) + Nucnfliq (3) + C
    // Nureftra Cliente netting: N + ID_NETTING
    // Nureftra Mercado: M + ID DESGLOSE CAMARA
    if (nureftraTipo != null && nureftraTipo.contains((CLEARING_TIPO.MERCADO.getTipo() + CLEARING_TIPO.CLIENTE_NETTING.getTipo()))) {
    	linea_registros.add(new RecordFile(nureftraTipo, 2));
    	linea_registros.add(new RecordFile(nureftraDato, 14, '0'));// rellenar con 0 por la izquierda
    } else {
    	linea_registros.add(new RecordFile(nureftraTipo, 1));
    	linea_registros.add(new RecordFile(nureftraDato, 15, '0'));// rellenar con 0 por la izquierda
    }

    linea_registros.add(new RecordFile(mcdesglose, 1));
    linea_registros.add(new RecordFile(sliquidacion, 4));
    linea_registros.add(new RecordFile(cdtpoper, 4));
    linea_registros.add(new RecordFile(cdintpop, 4));
    linea_registros.add(new RecordFile(platcont, 4));
    linea_registros.add(new RecordFile(cdisinxx, 12));
    linea_registros.add(new RecordFile(txdescri, 40));
    linea_registros.add(new RecordFile(fhcontra, 8));
    linea_registros.add(new RecordFile(fhvalcon, 8));
    linea_registros.add(new RecordFile(fhteoliq, 8));
    linea_registros.add(new RecordFile(nutitulo, 16, 2, true));
    linea_registros.add(new RecordFile(cddivisa, 3));
    linea_registros.add(new RecordFile(imcpreci, 24, 10, true));
    linea_registros.add(new RecordFile(imcprecc, 24, 10, true));
    linea_registros.add(new RecordFile(imefecti, 16, 2, true));
    linea_registros.add(new RecordFile(impornet, 16, 2, true));
    linea_registros.add(new RecordFile(imcanon, 16, 2, true));
    linea_registros.add(new RecordFile(imcomban, 16, 2, true));
    linea_registros.add(new RecordFile(imcomsvb, 16, 2, true));
    linea_registros.add(new RecordFile(cdcontra, 25));
    linea_registros.add(new RecordFile(cdentdep, 25));
    linea_registros.add(new RecordFile(cdbolsa, 1));
    linea_registros.add(new RecordFile(nubancod, 4));
    linea_registros.add(new RecordFile(nusucurd, 4));
    linea_registros.add(new RecordFile(nuctadep, 10, 0, false));
    linea_registros.add(new RecordFile(cdclient, 25));
    linea_registros.add(new RecordFile(nbclient, 100));
    linea_registros.add(new RecordFile(impcliente, 16, 2, true));
    linea_registros.add(new RecordFile(fhvencim, 8));
    linea_registros.add(new RecordFile(impnetvto, 16, 2, true));
    linea_registros.add(new RecordFile(cdcliter, 25));
    linea_registros.add(new RecordFile(ourparticipe, 50));
    linea_registros.add(new RecordFile(theirpartici, 50));
    linea_registros.add(new RecordFile("", 35));// linea_registros.add(new RecordFile(String.valueOf(nuctacontra),35));
    linea_registros.add(new RecordFile(cdcontracsd, 34));
    linea_registros.add(new RecordFile("", 35));// linea_registros.add(new
                                                // RecordFile(String.valueOf(nuctacltcontra),35));
    linea_registros.add(new RecordFile(cdparticipante, 25));
    linea_registros.add(new RecordFile(cdecc, 34));
    linea_registros.add(new RecordFile(cdmiembrocom, 34));
    linea_registros.add(new RecordFile(nuctamiembrocom, 35));
    linea_registros.add(new RecordFile(nurefecc, 16));
    linea_registros.add(new RecordFile(poolreference, 16));
    linea_registros.add(new RecordFile(cancelaciones, 1));

    return linea_registros;

  }

}
