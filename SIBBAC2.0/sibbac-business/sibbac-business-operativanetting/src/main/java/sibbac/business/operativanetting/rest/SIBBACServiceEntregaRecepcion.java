package sibbac.business.operativanetting.rest;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.dto.Tmct0estadoDTO;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.common.ReferenciaPata;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.operativanetting.database.bo.OperacionClearingNettingException;
import sibbac.business.operativanetting.database.bo.OperacionesClearingNettingBo;
import sibbac.business.operativanetting.exceptions.LoadConfigurationException;
import sibbac.business.wrappers.database.bo.NettingBo;
import sibbac.business.wrappers.database.bo.S3ErroresBo;
import sibbac.business.wrappers.database.bo.S3LiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.dto.AlcEntregaRecepcionClienteDTO;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.business.wrappers.database.model.S3Liquidacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceEntregaRecepcion implements SIBBACServiceBean {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para escribir en el log. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceEntregaRecepcion.class);

  private static final String TXT_ERROR_ESTADO_LIQUIDADA = 
          "No es cancelable porque tiene estado camara de 'LIQUIDADA' o 'LIQUIDADA PROPIA'.";
  
  private static final String TXT_SIN_DATOS_ENVIO = "Sin datos de envío";
  
  private static final String TXT_ESTADO_BAJA = "En estado 'Baja'";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS
	
  @Autowired
  private Tmct0alcBo BoTmct0alc;

  @Autowired
  private Tmct0estadoBo BoTmct0estado;

  @Autowired
  private S3LiquidacionBo BoS3Liquidacion;

  @Autowired
  private S3ErroresBo BoS3Errores;

  @Autowired
  private NettingBo BoNetting;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private OperacionesClearingNettingBo operacionesBo; 

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /**
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest)
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, String> filters;
    final List<Map<String, String>> params;
    final WebResponse webResponse;
    final String command;
    int procesados;

    command = webRequest.getAction();
    LOG.trace("Command: " + command);
    webResponse = new WebResponse();
    switch (command) {
      case Constantes.CMD_CUENTAS_COMPENSACION:
		final List<String> listaCuentas;
		final Map<String,?> result;
		try {
		    listaCuentas = operacionesBo.findDistinctCuentaCompensacionDestino();
		    result = Collections.singletonMap("cuentasCompensacion", listaCuentas);
	        webResponse.setResultados(result);
		} catch ( PersistenceException | JpaSystemException ex ) {
			LOG.error( ex.getMessage(), ex );
			webResponse.setError( ex.getMessage() );
		}
        break;
      case Constantes.CMD_LIST_ENTREGA_RECEPCION:
        filters = webRequest.getFilters();
        webResponse.setResultados(getEntregaRecepcion(filters));
        break;
      case Constantes.CMD_LIST_LIQUIDACION_S3:
        filters = webRequest.getFilters();
        webResponse.setResultados(getLiquidacionS3(filters));
        break;
      case Constantes.CMD_LIST_ERRORES_S3:
        filters = webRequest.getFilters();
        webResponse.setResultados(getErroresS3(filters));
        break;
      case Constantes.CMD_ESTADOS_CLEARING:
        filters = webRequest.getFilters();
        webResponse.setResultados(getEstadosClearing());
        break;
      case Constantes.CMD_CANCELACION_CLIENTE:
        params = webRequest.getParams();
        try {
          procesados = operacionesBo.cancelaListaPataCliente(ReferenciaPata.buildReferencias(params));
          webResponse.setResultados(Collections.singletonMap("procesados", procesados));
        }
        catch(OperacionClearingNettingException opex) {
          webResponse.setError(opex.getMessage());
          webResponse.setResultados(Collections.singletonMap("messages",  opex.getOpMessages()));
        }
        break;
      case Constantes.CMD_CANCELACION_TOTAL:
        params = webRequest.getParams();
        try {
          procesados = operacionesBo.cancelaListaAmbasPatas(ReferenciaPata.buildReferencias(params));
          webResponse.setResultados(Collections.singletonMap("procesados", procesados));
        }
        catch(OperacionClearingNettingException opex) {
          webResponse.setError(opex.getMessage());
          webResponse.setResultados(Collections.singletonMap("messages",  opex.getOpMessages()));
        }
        break;
      case Constantes.CMD_CIF_SVB:
        params = webRequest.getParams();
        webResponse.setResultados(getCifSvb(params));
        break;
      case Constantes.CMD_REENVIO:
        params = webRequest.getParams();
        try {
          procesados = operacionesBo.reenvia(ReferenciaPata.buildReferencias(params));
          webResponse.setResultados(Collections.singletonMap("procesados", procesados));
        }
        catch(OperacionClearingNettingException opex) {
          webResponse.setError(opex.getMessage());
          webResponse.setResultados(Collections.singletonMap("messages",  opex.getOpMessages()));
        }
        break;
      default:
        webResponse.setError("An action must be set. Valid actions are: " + command);
    } // switch

    return webResponse;
  } // process

  /**
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(Constantes.CMD_LIST_ENTREGA_RECEPCION);
    commands.add(Constantes.CMD_LIST_LIQUIDACION_S3);
    commands.add(Constantes.CMD_LIST_ERRORES_S3);
    commands.add(Constantes.CMD_ESTADOS_CLEARING);
    return commands;
  } // getAvailableCommands

  /**
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  @Override
  public List<String> getFields() {
    List<String> fields = new LinkedList<String>();
    return fields;
  } // getFields

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS
  

  /**
   * Devuelve la lista de estado de CLEARING.
   * 
   * @return
   */
  private Map<String, Object> getEstadosClearing() {
    Map<String, Object> map = new HashMap<String, Object>();
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

    Map<String, Object> bean = null;

    List<Tmct0estadoDTO> estadosClearing = BoTmct0estado.findByIdtipoestadoOrderByIdestadoAsc(new Integer(4));

    if (CollectionUtils.isNotEmpty(estadosClearing)) {

      for (Tmct0estadoDTO estadoDTO : estadosClearing) {
        bean = new HashMap<String, Object>();
        bean.put("id", estadoDTO.getIdestado());
        bean.put("descripcion", estadoDTO.getNombre());
        list.add(bean);
      } // for
    } else {
      bean = new HashMap<String, Object>();
      bean.put("id", new Integer(0));
      bean.put("descripcion", "Error. Lista recibida vacía");
      list.add(bean);
    } // else

    map.put(Constantes.RESULT_LISTA, list);

    return map;
  }

  /**
   * 
   * @param params
   * @return
   * @throws SIBBACBusinessException
   * @throws LoadConfigurationException
   */
  private Map<String, Object> getEntregaRecepcion(Map<String, String> params) throws SIBBACBusinessException {

    Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac.business.operativanetting.common.Constantes.SBRMV,
                                                                                  sibbac.business.operativanetting.common.Utilidad.DatosConfig.SVB_cif.getProcess(),
                                                                                  sibbac.business.operativanetting.common.Utilidad.DatosConfig.SVB_cif.getKeyname());

    String cif_svb = datosConfiguracion.getKeyvalue().trim();
    int liquidada = CLEARING.LIQUIDADA.getId();
    int liquidadaPropia = CLEARING.LIQUIDADA_PROPIA.getId();

    String nbooking = "";
    final String strNbooking = StringUtils.trimToEmpty(params.get("nbooking"));
    if (StringUtils.isNotEmpty(strNbooking)) {
      nbooking = strNbooking;
    }
    String nif = "";
    final String strNIF = StringUtils.trimToEmpty(params.get("nif"));
    if (StringUtils.isNotEmpty(strNIF)) {
      nif = strNIF;
    }
    String sentido = "";
    final String strSentido = StringUtils.trimToEmpty(params.get("sentido"));
    if (StringUtils.isNotEmpty(strSentido)) {
      sentido = strSentido;
    }
    String alias = "";
    final String strAlias = StringUtils.trimToEmpty(params.get("alias"));
    if (StringUtils.isNotEmpty(strAlias)) {
      alias = strAlias;
    }
    String isin = "";
    final String strIsin = StringUtils.trimToEmpty(params.get("isin"));
    if (StringUtils.isNotEmpty(strIsin)) {
      isin = strIsin;
    }
    String nombretitular = "";
    final String strNombreTitular = StringUtils.trimToEmpty(params.get("nombretitular"));
    if (StringUtils.isNotEmpty(strNombreTitular)) {
      nombretitular = strNombreTitular.toUpperCase();
    }
    Date fcontratacionDe = null;
    final String strFcontratacionDe = StringUtils.trimToEmpty(params.get("fcontratacionDe"));
    if (StringUtils.isNotEmpty(strFcontratacionDe)) {
      fcontratacionDe = Utilidad.getFechaFormateada(params.get("fcontratacionDe"));
    }
    Date fcontratacionA = null;
    String strFcontratacionA = StringUtils.trimToEmpty(params.get("fcontratacionA"));
    if (StringUtils.isNotEmpty(strFcontratacionA)) {
      fcontratacionA = Utilidad.getFechaFormateada(strFcontratacionA);
    }
    Date fliquidacionDe = null;
    String strFliquidacionDe = StringUtils.trimToEmpty(params.get("fliquidacionDe"));
    if (StringUtils.isNotEmpty(strFliquidacionDe)) {
      fliquidacionDe = Utilidad.getFechaFormateada(strFliquidacionDe);
    }
    Date fliquidacionA = null;
    final String strFliquidacionA = StringUtils.trimToEmpty(params.get("fliquidacionA"));
    if (StringUtils.isNotEmpty(strFliquidacionA)) {
      fliquidacionA = Utilidad.getFechaFormateada(strFliquidacionA);
    }

    String biccle = "";
    final String strBiccle = StringUtils.trimToEmpty(params.get("biccle"));
    if (StringUtils.isNotEmpty(strBiccle)) {
      biccle = strBiccle;
    }

    String referencias3 = "";
    final String strReferencias3 = StringUtils.trimToEmpty(params.get("referencias3"));
    if (StringUtils.isNotEmpty(strReferencias3)) {
      referencias3 = strReferencias3;
    }
    String estadoInstruccion = "";
    final String strEstadoInstruccion = StringUtils.trimToEmpty(params.get("estadoInstruccion"));
    if (StringUtils.isNotEmpty(strEstadoInstruccion)) {
    	estadoInstruccion = strEstadoInstruccion;
    }
    String cuenta = "";
    final String strCuenta = StringUtils.trimToEmpty(params.get("cuenta"));
    if (StringUtils.isNotEmpty(strCuenta)) {
    	cuenta = strCuenta;
    }

    BigDecimal importeNetoDesde = BigDecimal.ZERO;
    BigDecimal importeNetoHasta = BigDecimal.ZERO;
    BigDecimal titulosDesde = BigDecimal.ZERO;
    BigDecimal titulosHasta = BigDecimal.ZERO;
    try {
      final String strImporteNetoDesde = StringUtils.trimToEmpty(params.get("importeNetoDesde"));
      if (StringUtils.isNotEmpty(strImporteNetoDesde)) {
        importeNetoDesde = FormatDataUtils.convertStringToBigDecimal(strImporteNetoDesde);
      }

      final String strImporteNetoHasta = StringUtils.trimToEmpty(params.get("importeNetoHasta"));
      if (StringUtils.isNotEmpty(strImporteNetoHasta)) {
        importeNetoHasta = FormatDataUtils.convertStringToBigDecimal(strImporteNetoHasta);
      }

      final String strTitulosDesde = StringUtils.trimToEmpty(params.get("titulosDesde"));
      if (StringUtils.isNotEmpty(strTitulosDesde)) {
        titulosDesde = FormatDataUtils.convertStringToBigDecimal(strTitulosDesde);
      }

      final String strTitulosHasta = StringUtils.trimToEmpty(params.get("titulosHasta"));
      if (StringUtils.isNotEmpty(strTitulosHasta)) {
        titulosHasta = FormatDataUtils.convertStringToBigDecimal(strTitulosHasta);
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Se ha producido un error leyendo los parámetros de entrada del servicio SIBBACServiceEntregaReccepcion-ListaEntregaRecepcion");
      throw e;
    }

    boolean clienteNetting = false;
    Long idnetting = null;
    String strIdnetting = StringUtils.trimToEmpty(params.get("idnetting"));
    if (StringUtils.isNotEmpty(strIdnetting)) {
      idnetting = new Long(strIdnetting);
      clienteNetting = true;
    }
    Map<String, Object> map = new HashMap<>();
    List<Map<String, Object>> list = new ArrayList<>();
    Map<String, Object> bean;

    if (clienteNetting) {
      List<Tmct0alc> lista_entrega_recepcion;

      final Netting datos_netting = BoNetting.findById(idnetting);
      if (datos_netting == null) {
        LOG.debug("[SIBBACServiceEntregaRecepcion::getEntregaRecepcion] No se ha encontrado el idNetting: ["
                  + String.valueOf(idnetting) + "]");
        return null;
      }
      lista_entrega_recepcion = BoTmct0alc.findByNetting(datos_netting);

      for (Tmct0alc datos_entrega_recepcion : lista_entrega_recepcion) {

        Tmct0alo datos_alo = datos_entrega_recepcion.getTmct0alo();

        // filtrar por el resto de parametros que hemos recibido
        if (!sentido.equals("") && !datos_alo.getCdtpoper().equals(sentido.charAt(0))) {
          continue;
        }
        if (!isin.equals("") && !isin.equals(datos_alo.getCdisin().trim())) {
          continue;
        }

        String alias_final = datos_alo.getCdalias().trim();
        if (datos_alo.getCdalias().contains("|")) {
          alias_final = datos_alo.getCdalias().substring(datos_alo.getCdalias().indexOf("|") + 1).trim();
        }

        if (!alias.equals("") && !alias.equals(alias_final)) {
          continue;
        }

        LOG.trace("[SIBBACServiceEntregaRecepcion::getEntregaRecepcion] Recuperado el cliente: ["
                  + datos_alo.getCdisin() + "]");

        bean = new HashMap<>();

        bean.put("estadoentrec", datos_entrega_recepcion.getEstadoentrec().getNombre());
        bean.put("nbooking", datos_entrega_recepcion.getId().getNbooking().trim());
        bean.put("nucnfclt", datos_entrega_recepcion.getId().getNucnfclt().trim());
        bean.put("nucnfliq", datos_entrega_recepcion.getId().getNucnfliq());
        bean.put("cdcustod", datos_entrega_recepcion.getCdcustod());
        if (datos_alo.getCdtpoper().equals('C')) {
          bean.put("cdtpoper", 'E');
        } else {
          bean.put("cdtpoper", 'R');
        }
        bean.put("cdisin", datos_alo.getCdisin().trim());
        bean.put("nbvalors", datos_alo.getNbvalors().trim());
        bean.put("nutitliq", datos_entrega_recepcion.getNutitliq());
        bean.put("imefeagr", datos_entrega_recepcion.getImefeagr());
        bean.put("imcomisin", datos_entrega_recepcion.getImcomisn());
        bean.put("imnfiliq", datos_entrega_recepcion.getImnfiliq());
        bean.put("cdniftit", datos_entrega_recepcion.getCdniftit().trim());
        bean.put("nbtitliq", datos_entrega_recepcion.getNbtitliq().trim());
        bean.put("cdrefban", datos_entrega_recepcion.getCdrefban().trim());
        bean.put("ourpar", datos_entrega_recepcion.getOurpar().trim());
        bean.put("theirpar", datos_entrega_recepcion.getTheirpar().trim());
        bean.put("fecontra", datos_entrega_recepcion.getFeejeliq());

        if (datos_entrega_recepcion.getImcanoncompeu() == null) {
          datos_entrega_recepcion.setImcanoncompeu(BigDecimal.ZERO);
        }
        bean.put("canon_compensacion", datos_entrega_recepcion.getImcanoncompeu());
        if (datos_entrega_recepcion.getImcanoncontreu() == null) {
          datos_entrega_recepcion.setImcanoncontreu(BigDecimal.ZERO);
        }
        bean.put("canon_contratacion", datos_entrega_recepcion.getImcanoncontreu());
        if (datos_entrega_recepcion.getImcanonliqeu() == null) {
          datos_entrega_recepcion.setImcanonliqeu(BigDecimal.ZERO);
        }
        bean.put("canon_liquidacion", datos_entrega_recepcion.getImcanonliqeu());

        bean.put("cdalias", alias_final);
        bean.put("descrali", datos_alo.getDescrali().trim()); // CFF 28/10/2015

        Date fechaLiq = BoTmct0alc.getMaxFechaValorS3LiqByAlc(datos_entrega_recepcion);
        bean.put("fechaLiq", fechaLiq);
        bean.put("FTL", datos_entrega_recepcion.getFeopeliq());
        list.add(bean);

      }

    } else {

      LOG.debug("[SIBBACServiceEntregaRecepcion::getEntregaRecepcion] Else del neetting");

      String estado = "";
      final String strEstado = StringUtils.trimToEmpty(params.get("estado"));
      if (StringUtils.isNotEmpty(strEstado)) {
        estado = strEstado;
      }

      List<AlcEntregaRecepcionClienteDTO> lista_entrega_recepcion = null;
      String sentidoBusqueda = "";
      Integer tipo = 3;
      boolean sentidoInv = false;
      if (sentido.indexOf("#") != -1) {
        sentidoInv = true;
        sentido = sentido.replace("#", "");
      }
      if (sentido.equals("C") || sentido.equals("V") || sentido.equals("CV")) {
        sentidoBusqueda = sentido;
        tipo = 1;
      } else if (sentido.equals("O") || sentido.equals("E")) {
        sentidoBusqueda = sentido.equals("O") ? "C" : "V";
        tipo = 2;
      } else if (sentido.equals("OE")) {
        sentidoBusqueda = "CV";
        tipo = 2;
      } else if (sentido.equals("OC")) {
        sentidoBusqueda = "CC";
      } else if (sentido.equals("EV")) {
        sentidoBusqueda = "VV";
      }
      if (sentidoInv)
        sentidoBusqueda = "#" + sentidoBusqueda;

      lista_entrega_recepcion = BoTmct0alc.findByEntregaRecepcionTodos(fcontratacionDe,
                                                                       fcontratacionA,
                                                                       fliquidacionDe,
                                                                       fliquidacionA,
                                                                       estado,
                                                                       nbooking,
                                                                       nif,
                                                                       nombretitular,
                                                                       biccle,
                                                                       importeNetoDesde,
                                                                       importeNetoHasta,
                                                                       titulosDesde,
                                                                       titulosHasta,
                                                                       Integer.valueOf(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()),
                                                                       isin, alias, referencias3, cif_svb,
                                                                       sentidoBusqueda, tipo, estadoInstruccion, cuenta);

      for (AlcEntregaRecepcionClienteDTO datos_entrega_recepcion : lista_entrega_recepcion) {
        LOG.trace("[getEntregaRecepcion] Recuperado el cliente: [{}]",
                  datos_entrega_recepcion.getCdisin());
        bean = new HashMap<>();
        bean.put("estadoentrec", datos_entrega_recepcion.getNombre());
        bean.put("nbooking", datos_entrega_recepcion.getNbooking().trim());
        bean.put("nucnfclt", datos_entrega_recepcion.getNucnfclt().trim());
        bean.put("nucnfliq", datos_entrega_recepcion.getNucnfliq());
        bean.put("cdcustod", datos_entrega_recepcion.getCdcustod());
        if (datos_entrega_recepcion.getPata().equals("C")) {
          if (datos_entrega_recepcion.getCdtpoper().equals('C')) {
            bean.put("cdtpoper", 'E');
          } else {
            bean.put("cdtpoper", 'R');
          }
        } else {
          bean.put("cdtpoper", datos_entrega_recepcion.getCdtpoper());
        }
        bean.put("cdisin", datos_entrega_recepcion.getCdisin().trim());
        bean.put("nbvalors", datos_entrega_recepcion.getNbvalors().trim());
        bean.put("nutitliq", datos_entrega_recepcion.getNutitliq());
        bean.put("imefeagr", datos_entrega_recepcion.getImefeagr());
        bean.put("imcomisin", datos_entrega_recepcion.getImcomisn());
        bean.put("imnfiliq", datos_entrega_recepcion.getImnfiliq());
        bean.put("cdniftit", datos_entrega_recepcion.getCdniftit().trim());
        bean.put("nbtitliq", datos_entrega_recepcion.getNbtitliq().trim());
        bean.put("cdrefban", datos_entrega_recepcion.getCdrefban().trim());
        bean.put("ourpar", datos_entrega_recepcion.getOurpar().trim());
        bean.put("theirpar", datos_entrega_recepcion.getTheirpar().trim());
        bean.put("fecontra", datos_entrega_recepcion.getFeejeliq());
        if (datos_entrega_recepcion.getImcanoncompeu() == null) {
          datos_entrega_recepcion.setImcanoncompeu(BigDecimal.ZERO);
        }
        bean.put("canon_compensacion", datos_entrega_recepcion.getImcanoncompeu());
        if (datos_entrega_recepcion.getImcanoncontreu() == null) {
          datos_entrega_recepcion.setImcanoncontreu(BigDecimal.ZERO);
        }
        bean.put("canon_contratacion", datos_entrega_recepcion.getImcanoncontreu());
        if (datos_entrega_recepcion.getImcanonliqeu() == null) {
          datos_entrega_recepcion.setImcanonliqeu(BigDecimal.ZERO);
        }
        bean.put("canon_liquidacion", datos_entrega_recepcion.getImcanonliqeu());
        bean.put("cdalias", datos_entrega_recepcion.getCdalias());
        if (datos_entrega_recepcion.getDescrali() != null) {
          bean.put("descrali", datos_entrega_recepcion.getDescrali().trim()); // CFF 28/10/2015
        } else {
          bean.put("descrali", "");
        }
        bean.put("referencias3", datos_entrega_recepcion.getReferencias3());
        bean.put("nuorden", datos_entrega_recepcion.getNuorden());
        bean.put("pata", datos_entrega_recepcion.getPata());
        bean.put("desglosecamara", datos_entrega_recepcion.getDesglosecamara());
        bean.put("estdesglose", datos_entrega_recepcion.getEstdesglose());
        int estadoComp = (Integer) datos_entrega_recepcion.getIdestado();
        Character estadoInstr = datos_entrega_recepcion.getEstadoInstruccion();
        if(estadoInstr == null || estadoInstr.equals('B')) {
            bean.put("editable", false);
            bean.put("mensaje", TXT_ESTADO_BAJA);
        }
        else if ((estadoComp == liquidada) || (estadoComp == liquidadaPropia)) {
          bean.put("editable", false);
          bean.put("mensaje", TXT_ERROR_ESTADO_LIQUIDADA);
        }
        else if(datos_entrega_recepcion.getAlcOrdenMercadoId() == null &&
                datos_entrega_recepcion.getAlcOrdenesId() == null) {
          bean.put("editable", false);
          bean.put("mensaje", TXT_SIN_DATOS_ENVIO);
        }
        else
          bean.put("editable", true);
        bean.put("fechaLiq", datos_entrega_recepcion.getFechaLiquidacion());
        bean.put("FTL", datos_entrega_recepcion.getFechaLiquidacionTeorica());
        bean.put("estadoInstruccion", datos_entrega_recepcion.getEstadoInstruccion());
        bean.put("cuentaCompensacionDestino", datos_entrega_recepcion.getCuentaCompensancionDestino());
        bean.put("alcOrdenesId", datos_entrega_recepcion.getAlcOrdenesId());
        bean.put("alcOrdenMercadoId", datos_entrega_recepcion.getAlcOrdenMercadoId());
        list.add(bean);
      }
    }
    map.put(Constantes.RESULT_LISTA, list);
    return map;
  }

  /**
   * 
   * @param params
   * @return
   */
  private Map<String, Object> getLiquidacionS3(Map<String, String> params) {
    final List<S3Liquidacion> lista_liquidacion;
    final String referenciaS3;
    
    
    if(params.containsKey("referenciaS3")) {
      referenciaS3 = params.get("referenciaS3");
      lista_liquidacion = BoS3Liquidacion.findByReferenciaS3(referenciaS3);
    }
    else if (params.get("idnetting") != null) {
      Long idnetting = new Long(params.get("idnetting"));
      Netting datos_netting = BoNetting.findById(idnetting);
      if (datos_netting == null) {
        LOG.debug("[SIBBACServiceEntregaRecepcion::getLiquidacionS3] No se ha encontrado el idNetting: ["
                  + String.valueOf(idnetting) + "]");
        return null;
        
      }
      lista_liquidacion = BoS3Liquidacion.findByNettingOrdAudit_date(datos_netting);
    } else if(params.containsKey("nbooking")){
    	short  nucnfliq =  Short.parseShort(params.get("nucnfliq"));
    	lista_liquidacion = BoS3Liquidacion.findByNbookingAndNucnflictAndNcnfliq(params.get("nbooking"), params.get("nucnfclt"), nucnfliq);
    }    
    else {
      // no hay suficientes datos para buscar el historial de liquidacion
      LOG.debug("[SIBBACServiceEntregaRecepcion::getLiquidacionS3] Sin parametros suficientes para recuperar la liquidacion");
      return null;
    }

    Map<String, Object> map = new HashMap<String, Object>();
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    Map<String, Object> bean;
    for (S3Liquidacion datos_liquidacion_s3 : lista_liquidacion) {
      bean = new HashMap<String, Object>();
      bean.put("idliquidacion", datos_liquidacion_s3.getId());
      bean.put("cdestados3", datos_liquidacion_s3.getCdestadoS3());
      bean.put("fechaoperacion", datos_liquidacion_s3.getFechaOperacion());
      bean.put("fechavalor", datos_liquidacion_s3.getFechaValor());
      bean.put("nutiuloliquidados", datos_liquidacion_s3.getNutituloLiquidados());
      bean.put("importenetoliquidado", datos_liquidacion_s3.getImporteNetoLiquidado());
      bean.put("contabilizado", datos_liquidacion_s3.getContabilizado());
      bean.put("fechacontabilidad", datos_liquidacion_s3.getFechaContabilidad());
      bean.put("importeefectivo", datos_liquidacion_s3.getImporteEfectivo());
      bean.put("importecorretaje", datos_liquidacion_s3.getImporteCorretaje());
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String fechaliquidacion = formatter.format(datos_liquidacion_s3.getAuditDate());
      bean.put("fechaliquidacion", fechaliquidacion);
      list.add(bean);

    }

    map.put(Constantes.RESULT_LISTA, list);
    return map;
  }

  /**
   * 
   * @param params
   * @return
   */
  private Map<String, Object> getErroresS3(Map<String, String> params) {

    List<S3Errores> lista_errores = null;

    if (params.get("idliquidacion") != null) {
      Long idLiquidacion = new Long(params.get("idliquidacion"));
      lista_errores = BoS3Errores.findByIdLiquidacion(idLiquidacion);
      if (lista_errores != null) {
        LOG.debug("[SIBBACServiceEntregaRecepcion::getErroresS3] Se han encontrado errores de liquidacion: idliquidacion=["
                  + String.valueOf(idLiquidacion) + "]");
      }

    } else {
      // no hay suficientes datos para buscar el los errores de la liquidacion
      LOG.debug("[SIBBACServiceEntregaRecepcion::getErroresS3] Sin parametros suficientes para recuperar los errores de la liquidacion");
      return null;
    }

    Map<String, Object> map = new HashMap<String, Object>();
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    Map<String, Object> bean;
    map.put(Constantes.RESULT_LISTA, list);
    for (S3Errores datos_errores_s3 : lista_errores) {
      list.add(bean = new HashMap<String, Object>());

      bean.put("datoerrorsv", datos_errores_s3.getDatoErrorSV());
      bean.put("datoerrorcliente", datos_errores_s3.getDatoErrorCliente());
      bean.put("dserror", datos_errores_s3.getDserror());

    }
    return map;
  }


  private Map<String, Object> getCifSvb(List<Map<String, String>> params) {
    LOG.debug("[SIBBACServiceEntregaRecepcion::getCifSvb] Entra en el metodo getCancelacionTotal");

    Map<String, Object> list = new HashMap<String, Object>();

    Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac.business.operativanetting.common.Constantes.SBRMV,
                                                                                  sibbac.business.operativanetting.common.Utilidad.DatosConfig.SVB_cif.getProcess(),
                                                                                  sibbac.business.operativanetting.common.Utilidad.DatosConfig.SVB_cif.getKeyname());

    String cif_svb = datosConfiguracion.getKeyvalue().trim();

    if (cif_svb != null) {
      list.put("cif", cif_svb);
    } else {
      list.put("error", "No se ha podido obtener el cif de la sociedad de valores.");
    }

    return list;

  }


}
