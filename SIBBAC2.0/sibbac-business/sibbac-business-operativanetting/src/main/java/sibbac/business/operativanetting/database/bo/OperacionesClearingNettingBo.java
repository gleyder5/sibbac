package sibbac.business.operativanetting.database.bo;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import java.math.BigDecimal;
import java.text.MessageFormat;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.common.SIBBACBusinessException;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.operativanetting.common.ReferenciaPata;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.AlcOrdenMercadoDao;
import sibbac.business.wrappers.database.dao.AlcOrdenesDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.AlcOrdenMercado;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;

/**
 * Objeto de negocio que gestiona las operaciones de clearing netting con
 * multiples instancias: cancelaciones y reenvíos.
 * @author Neoris
 * */
@Service
public class OperacionesClearingNettingBo {

  /**
   * Bitacora de la clase
   */
  private static final Logger LOG = LoggerFactory.getLogger(OperacionesClearingNettingBo.class);

  /**
   * Cache interno de estados
   */
  private final Map<Integer, Tmct0estado> cacheEstados;

  @Autowired
  private AlcOrdenesDao alcOrdenesDao;

  @Autowired
  private AlcOrdenMercadoDao alcOrdenMercadoDao;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private Tmct0alcDao alcDao;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;

  private static void throwBussinessException(String metodo, String format, Object... ids)
      throws SIBBACBusinessException {
    final String mensaje;

    mensaje = MessageFormat.format(format, ids);
    LOG.warn("[{}] {}", metodo, mensaje);
    throw new SIBBACBusinessException(mensaje);
  }

  public OperacionesClearingNettingBo() {
    cacheEstados = Collections.synchronizedMap(new HashMap<Integer, Tmct0estado>());
  }

  public List<String> findDistinctCuentaCompensacionDestino() {
    return alcOrdenesDao.findDistinctCuentaCompensacionDestino();
  }

  /**
   * Cancela una lista de referencia de Patas cliente.
   * @param cancelaciones lista de cancelaciones
   * @return número de cancelaciones realizadas
   * @throws OperacionClearingNettingException
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public int cancelaListaPataCliente(List<ReferenciaPata> cancelaciones) throws OperacionClearingNettingException,
      SIBBACBusinessException {
    final List<String> mensajes;
    int conteo = 0;

    LOG.debug("[cancelaListaPataCliente] Inicio...");
    try {
      mensajes = new ArrayList<>();
      for (ReferenciaPata referencia : cancelaciones) {
        try {
          if (referencia.isPataMercado()) {
            conteo += cancelaPataClienteRefMercado(referencia);
          }
          else if (referencia.isPataCliente()) {
            conteo += cancelaPataClienteRefCliente(referencia);
          }
          else {
            mensajes.add(MessageFormat.format("Referencia incorrecta {0}", referencia));
          }
        }
        catch (SIBBACBusinessException bex) {
          mensajes.add(bex.getMessage());
        }
      }
      if (!mensajes.isEmpty())
        throw new OperacionClearingNettingException("Cancelaciones de pata cliente", mensajes);
      LOG.debug("[cancelaListaPataCliente] Fin");
      return conteo;
    }
    catch (RuntimeException rex) {
      LOG.debug("Inesperada al cancelar pata cliente de una lista", rex);
      throw new SIBBACBusinessException("Al cancelar pata cliente de una lista", rex);
    }
  }

  /**
   * Cancela ambas patas desde una lista de referencias
   * @param cancelaciones lista de refencias para su cancelación
   * @return número de cancelaciones realizadas
   * @throws OperacionClearingNettingException En caso de un fallo de la
   * operativa propia de la cancelación
   * @throws SIBBACBusinessException Cualquier otro error durante la operación
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public int cancelaListaAmbasPatas(List<ReferenciaPata> cancelaciones) throws OperacionClearingNettingException,
      SIBBACBusinessException {
    final List<String> mensajes;
    int conteo = 0;

    LOG.debug("[cancelaListaAmbasPatas] Inicio...");
    try {
      mensajes = new ArrayList<>();
      for (ReferenciaPata referencia : cancelaciones) {
        try {
          if (referencia.isPataMercado()) {
            conteo += cancelaTotalRefMercado(referencia);
          }
          else if (referencia.isPataCliente()) {
            conteo += cancelaTotalRefCliente(referencia);
          }
          else
            mensajes.add(MessageFormat.format("Referencia incorrecta {0}", referencia));
        }
        catch (SIBBACBusinessException bex) {
          mensajes.add(bex.getMessage());
        }
      }
      if (!mensajes.isEmpty())
        throw new OperacionClearingNettingException("Cancelaciones ambas patas", mensajes);
      LOG.debug("[cancelaListaAmbasPatas] Fin");
      return conteo;
    }
    catch (RuntimeException rex) {
      LOG.debug("Inesperada al cancelar ambas patas de una lista", rex);
      throw new SIBBACBusinessException("Al cancelar ambas patas de una lista", rex);
    }
  }

  /**
   * Reenvía las patas referenciadas en la lista
   * @param reenvios Lista de referencias a reenviar
   * @return número de reenvíos ejecutados
   * @throws OperacionClearingNettingException Error en la operación propia del
   * reenvío.
   * @throws SIBBACBusinessException Cualquier otro error de la operativa
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public int reenvia(List<ReferenciaPata> reenvios) throws OperacionClearingNettingException, SIBBACBusinessException {
    final List<String> mensajes;
    int conteo = 0;

    LOG.debug("[reenvia] Inicio...");
    try {
      mensajes = new ArrayList<>();
      for (ReferenciaPata referencia : reenvios) {
        try {
          if (referencia.isPataMercado()) {
            conteo += reenviaRefMercado(referencia);
          }
          else if (referencia.isPataCliente()) {
            conteo += reenviaRefCliente(referencia);
          }
          else
            mensajes.add(MessageFormat.format("Referencia incorrecta {0}", referencia));
        }
        catch (SIBBACBusinessException bex) {
          mensajes.add(bex.getMessage());
        }
      }
      if (!mensajes.isEmpty()) {
        throw new OperacionClearingNettingException("Reenvio", mensajes);
      }
      LOG.debug("[reenvia] Fin");
      return conteo;
    }
    catch (RuntimeException rex) {
      LOG.debug("Inesperada reenviar patas de una lista", rex);
      throw new SIBBACBusinessException("Al reenviar patas de una lista", rex);
    }
  }

  @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = SIBBACBusinessException.class)
  private int cancelaPataClienteRefMercado(ReferenciaPata cancelacion) throws SIBBACBusinessException {
    final AlcOrdenMercado ordenMercado;
    final AlcOrdenes alcOrdenes;
    final BigDecimal restantes;
    final long alcOrdenMercadoId;

    LOG.debug("[cancelaPataClienteRefMercado] Inicio...");
    alcOrdenMercadoId = cancelacion.getAlcOrdenMercadoId();
    ordenMercado = alcOrdenMercadoDao.findOne(alcOrdenMercadoId);
    if (ordenMercado == null)
      throwBussinessException("cancelaPataClienteRefMercado", "No se encuentra orden mercado con Id {0}",
          alcOrdenMercadoId);
    if (ordenMercado.isEstadoAlta()) {
      alcOrdenes = ordenMercado.getAlcOrdenes();
      if (alcOrdenes == null) {
        throwBussinessException("cancelaPataClienteRefMercado",
            "No se encuentra alc orden asociado con orden mercado Id {}", ordenMercado.getId());
      }
      LOG.debug("Se procede a cancelar la orden mercado {}", ordenMercado.getId());
      restantes = alcOrdenes.getTitulosAsignados().subtract(ordenMercado.getTitulos());
      if (restantes.compareTo(BigDecimal.ZERO) < 0)
        throwBussinessException("cancelaPataClienteRefMercado",
            "Dar de baja la orden mercado {0} provocaría número negativo", ordenMercado.getId());
      ordenMercado.registraBaja();
      alcOrdenes.setTitulosAsignados(restantes);
      ordenMercado.getDesgloseCamara().setTmct0estadoByCdestadoentrec(getEstado(CLEARING.ACEPTADA_ANULACION.getId()));
      LOG.debug("[cancelaPataClienteRefMercado] Fin");
      return 1;
    }
    return 0;
  }

  @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = SIBBACBusinessException.class)
  private int cancelaPataClienteRefCliente(ReferenciaPata cancelacion) throws SIBBACBusinessException {
    final AlcOrdenes alcOrdenes;
    final long alcOrdenesId;
    final Tmct0alcId alcId;
    final Tmct0alc alc;

    LOG.debug("[cancelaPataClienteRefCliente] Inicio...");
    alcOrdenesId = cancelacion.getAlcOrdenesId();
    alcOrdenes = alcOrdenesDao.findOne(alcOrdenesId);
    if (alcOrdenes == null) {
      throwBussinessException("cancelaPataClienteRefCliente", "No se encuentra el Alc orden con Id: {0}", alcOrdenesId);
    }
    alcId = alcOrdenes.createAlcIdFromFields();
    alc = alcDao.findOne(alcId);
    if (alc == null)
      throwBussinessException("cancelaPataClienteRefCliente",
          "No se encuentra el Alc con Id: {0} asociado a al Alc orden con Id: {1}", alcId, alcOrdenesId);
    LOG.debug("[cancelaPataClienteRefCliente] Se cumplen todas las condiciones para cancelacion alcOrdenesId: {}",
        alcOrdenesId);
    alcOrdenes.setEstadoInstruccion('B');
    alc.setEstadoentrec(getEstado(CLEARING.ACEPTADA_ANULACION.getId()));
    LOG.debug("[cancelaPataClienteRefCliente] Fin");
    return 1;
  }

  @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = SIBBACBusinessException.class)
  private int cancelaTotalRefMercado(ReferenciaPata cancelacion) throws SIBBACBusinessException {
    final AlcOrdenes alcOrdenes;
    final long alcOrdenesId;

    LOG.debug("[cancelaTotalRefMercado] Inicio...");
    alcOrdenesId = cancelacion.getAlcOrdenesId();
    alcOrdenes = alcOrdenesDao.findOne(alcOrdenesId);
    if (alcOrdenes == null) {
      throwBussinessException("cancelaTotalRefMercado", "No se encuentra el Alc orde con Id: {0}", alcOrdenesId);
    }
    // ES CUENTA PROPIA
    List<AlcOrdenMercado> ordenMercado = alcOrdenes.getOrdenesMercado();

    if (CollectionUtils.isNotEmpty(ordenMercado)) {
      Tmct0desglosecamara dc;
      String cuentaCompensacionSt;
      Tmct0CuentasDeCompensacion cuentaCompensacion;
      for (AlcOrdenMercado alcOrdenMercado : ordenMercado) {
        dc = alcOrdenMercado.getDesgloseCamara();
        logBo.insertarRegistro(dc.getTmct0alc(), new Date(), new Date(), "", dc.getTmct0estadoByCdestadoentrec(),
            "Cancelada pata mercado.");
        cuentaCompensacionSt = alcOrdenes.getCuentaCompensacionDestino();
        if (StringUtils.isNotBlank(cuentaCompensacionSt)) {
          cuentaCompensacion = cuentaCompensacionBo.findByCdCodigo(cuentaCompensacionSt);
          if (cuentaCompensacion != null && cuentaCompensacion.getEsCuentaPropia()) {
            dc.getTmct0alc().setEstadoentrec(new Tmct0estado(CLEARING.ACEPTADA_ANULACION.getId()));
            dc.getTmct0alc().setFhaudit(new Date());
            dc.getTmct0alc().setCdusuaud("SIBBAC20");
          }
        }
      }
    }

    alcOrdenes.cancelaTotalmente(getEstado(CLEARING.ACEPTADA_ANULACION.getId()));
    LOG.debug("[cancelTotalRefMercado] Se ha cancelado {}", alcOrdenes);

    return 1;
  }

  @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = SIBBACBusinessException.class)
  private int cancelaTotalRefCliente(ReferenciaPata cancelacion) throws SIBBACBusinessException {
    final long alcOrdenesId;
    final AlcOrdenes alcOrdenes;
    final Tmct0alcId alcId;
    final Tmct0alc alc;

    LOG.debug("[cancelaTotalRefCliente] Inicio...");
    alcOrdenesId = cancelacion.getAlcOrdenesId();
    alcOrdenes = alcOrdenesDao.findOne(alcOrdenesId);
    if (alcOrdenes == null) {
      throwBussinessException("cancelaTotalRefCliente", "No se encuentra el Alc orden con Id: {0}", alcOrdenesId);
    }

    alcOrdenes.setEstadoInstruccion('B');
    alcId = alcOrdenes.createAlcIdFromFields();
    alc = alcDao.findOne(alcId);
    if (alc == null)
      throwBussinessException("cancelaTotalRefCliente",
          "No se encuentra Alc para alcId [{0}] cancelando Alc orden con Id [{1}]", alcId, alcOrdenesId);

    logBo.insertarRegistro(alc, new Date(), new Date(), "", alc.getEstadoentrec(), "Cancelada pata cliente.");
    alc.setEstadoentrec(getEstado(CLEARING.ACEPTADA_ANULACION.getId()));
    LOG.debug("[cancelaTotalRefCliente] Se ha cancelado {}", alcOrdenes);
    return 1;
  }

  @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = SIBBACBusinessException.class)
  private int reenviaRefMercado(ReferenciaPata reenvio) throws SIBBACBusinessException {
    final long alcOrdenMercadoId;
    final AlcOrdenMercado alcOrdenMercado;
    final Tmct0desglosecamara desgloseCamara;

    LOG.debug("[reenviaRefMercado] Inicio");
    alcOrdenMercadoId = reenvio.getAlcOrdenMercadoId();
    alcOrdenMercado = alcOrdenMercadoDao.findOne(alcOrdenMercadoId);
    if (alcOrdenMercado == null) {
      throwBussinessException("reenviaRefMercado", "No se encuentra el Alc orden mercado con Id: {0}",
          alcOrdenMercadoId);
    }
    desgloseCamara = alcOrdenMercado.getDesgloseCamara();
    if (desgloseCamara != null
        && desgloseCamara.getTmct0estadoByCdestadoentrec().equals(getEstado(CLEARING.ACEPTADA_ANULACION.getId()))) {
      desgloseCamara.setTmct0estadoByCdestadoentrec(getEstado(CLEARING.PDTE_FICHERO.getId()));
      LOG.debug("[reenviaRefMercado] Se ha solicitado reenvio de {}", desgloseCamara);
      return 1;
    }
    return 0;
  }

  @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = SIBBACBusinessException.class)
  private int reenviaRefCliente(ReferenciaPata reenvio) throws SIBBACBusinessException {
    final long alcOrdenesId;
    final AlcOrdenes alcOrdenes;
    final Tmct0alc alc;

    LOG.debug("[reenviaRefCliente] Inicio...");
    alcOrdenesId = reenvio.getAlcOrdenesId();
    alcOrdenes = alcOrdenesDao.findOne(alcOrdenesId);
    if (alcOrdenes != null) {
      alc = alcOrdenes.getAlcdeReferencia();
      if (alc != null && alc.getEstadoentrec().equals(getEstado(CLEARING.ACEPTADA_ANULACION.getId()))) {
        alc.setEstadoentrec(getEstado(CLEARING.PDTE_FICHERO.getId()));
        LOG.debug("[reenviaRefCliente] Se solicita reeenvio de {}", alc);
        return 1;
      }
    }
    return 0;
  }

  private Tmct0estado getEstado(final Integer codEstado) {
    final Tmct0estado estado;

    if (!cacheEstados.containsKey(codEstado)) {
      estado = estadoDao.findOne(codEstado);
      cacheEstados.put(codEstado, estado);
    }
    else
      estado = cacheEstados.get(codEstado);
    return estado;
  }

}