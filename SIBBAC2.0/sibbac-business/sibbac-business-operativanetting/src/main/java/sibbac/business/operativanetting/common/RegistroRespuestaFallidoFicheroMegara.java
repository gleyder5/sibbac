/**
 * # RegistroRespuestaFallidoFicheroMegara.java
 * # Fecha de creación: 20/11/2015
 */
package sibbac.business.operativanetting.common;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Registro con los datos de una operación fallida.
 * 
 * @author XI316153
 */
public class RegistroRespuestaFallidoFicheroMegara {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  protected static final Logger LOG = LoggerFactory.getLogger(RegistroRespuestaFallidoFicheroMegara.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Número de instrucción de liquidación de la ECC fallida. CHAR(35). */
  private String instruccionLiquidacion;

  /** Valor de la operación fallida. CHAR(12). */
  private String isin;

  /** Bic code de la contrapartida. CHAR(25). */
  private String contrapartida;

  /** Referencioa SV de la instrucción. CHAR(16). */
  private String referenciaSV;

  /** Cuenta de liquidación de iberclear. CHAR(35). */
  private String cuentaLiquidacion;

  /** Fecha de ejecución de la orden. YYYYMMDD. */
  private Date fechaEjecucion;

  /** Fecha teórica de liquidación de la orden. YYYYMMDD. */
  private Date fechaLiquidacion;

  /** Número de títulos pendientes de liquidar de la orden. NUMBER (14,2). */
  private BigDecimal nuTitFallidos;

  /** Importe pendiente de liquidar de la orden. NUMBER (14,2). */
  private BigDecimal imEfectivoFallido;

  /** Código de cliente en S3. CHAR(20). */
  private String ccv;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Contruye un objeto de la clase a partir de la cadena especificada.
   * 
   * @param registro Línea a procesar.
   */
  public RegistroRespuestaFallidoFicheroMegara(String registro) {
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] Inicio ...");
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] Registro: " + registro);

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    Integer aux_tam = 35;
    this.instruccionLiquidacion = registro.substring(0, aux_tam);
    registro = registro.substring(aux_tam);

    aux_tam = 12;
    this.isin = registro.substring(0, aux_tam);
    registro = registro.substring(aux_tam);

    aux_tam = 25;
    this.contrapartida = registro.substring(0, aux_tam);
    registro = registro.substring(aux_tam);

    aux_tam = 16;
    this.referenciaSV = registro.substring(0, aux_tam);
    registro = registro.substring(aux_tam);

    aux_tam = 35;
    this.cuentaLiquidacion = registro.substring(0, aux_tam);
    registro = registro.substring(aux_tam);

    aux_tam = 8;
    String aux_valor = registro.substring(0, aux_tam);
    fechaEjecucion = Utilidad.ParserDate(aux_valor, sdf);
    registro = registro.substring(aux_tam);

    aux_tam = 8;
    aux_valor = registro.substring(0, aux_tam);
    fechaLiquidacion = Utilidad.ParserDate(aux_valor, sdf);
    registro = registro.substring(aux_tam);

    // FIXME posiciones decimales
    aux_tam = 16;
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] Texto nuTitFallidos: "
              + registro.substring(0, aux_tam - 2));
    BigDecimal aux_bd = new BigDecimal(registro.substring(0, aux_tam - 2).replaceFirst("^0+(?!$)", ""));
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] BigDecimal nuTitFallidos: "
              + aux_bd);
    // this.nuTitFallidos = aux_bd.movePointLeft(2);
    this.nuTitFallidos = aux_bd;
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] BigDecimal nuTitFallidos con decimales: "
              + nuTitFallidos);
    registro = registro.substring(aux_tam);

    aux_tam = 16;
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] Texto imEfectivoFallido: "
              + registro.substring(0, aux_tam));
    aux_bd = new BigDecimal(registro.substring(0, aux_tam).replaceFirst("^0+(?!$)", ""));
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] BigDecimal imEfectivoFallido: "
              + aux_bd);
    this.imEfectivoFallido = aux_bd.movePointLeft(2);
    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] BigDecimal imEfectivoFallido con decimales: "
              + imEfectivoFallido);
    registro = registro.substring(aux_tam);

    aux_tam = 20;
    this.ccv = registro.substring(0, aux_tam);
    registro = registro.substring(aux_tam);

    LOG.debug("[RegistroRespuestaFallidoFicheroMegara :: RegistroRespuestaFallidoFicheroMegara] Fin ...");
  } // RegistroRespuestaFallidoFicheroMegara

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the instruccionLiquidacion
   */
  public String getInstruccionLiquidacion() {
    return instruccionLiquidacion;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the contrapartida
   */
  public String getContrapartida() {
    return contrapartida;
  }

  /**
   * @return the referenciaSV
   */
  public String getReferenciaSV() {
    return referenciaSV;
  }

  /**
   * @return the cuentaLiquidacion
   */
  public String getCuentaLiquidacion() {
    return cuentaLiquidacion;
  }

  /**
   * @return the fechaEjecucion
   */
  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  /**
   * @return the fechaLiquidacion
   */
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  /**
   * @return the nuTitFallidos
   */
  public BigDecimal getNuTitFallidos() {
    return nuTitFallidos;
  }

  /**
   * @return the imEfectivoFallido
   */
  public BigDecimal getImEfectivoFallido() {
    return imEfectivoFallido;
  }

  /**
   * @return the ccv
   */
  public String getCcv() {
    return ccv;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param instruccionLiquidacion the instruccionLiquidacion to set
   */
  public void setInstruccionLiquidacion(String instruccionLiquidacion) {
    this.instruccionLiquidacion = instruccionLiquidacion;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param contrapartida the contrapartida to set
   */
  public void setContrapartida(String contrapartida) {
    this.contrapartida = contrapartida;
  }

  /**
   * @param referenciaSV the referenciaSV to set
   */
  public void setReferenciaSV(String referenciaSV) {
    this.referenciaSV = referenciaSV;
  }

  /**
   * @param cuentaLiquidacion the cuentaLiquidacion to set
   */
  public void setCuentaLiquidacion(String cuentaLiquidacion) {
    this.cuentaLiquidacion = cuentaLiquidacion;
  }

  /**
   * @param fechaEjecucion the fechaEjecucion to set
   */
  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  /**
   * @param fechaLiquidacion the fechaLiquidacion to set
   */
  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  /**
   * @param nuTitFallidos the nuTitFallidos to set
   */
  public void setNuTitFallidos(BigDecimal nuTitFallidos) {
    this.nuTitFallidos = nuTitFallidos;
  }

  /**
   * @param imEfectivoFallido the imEfectivoFallido to set
   */
  public void setImEfectivoFallido(BigDecimal imEfectivoFallido) {
    this.imEfectivoFallido = imEfectivoFallido;
  }

  /**
   * @param ccv the ccv to set
   */
  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

} // RegistroRespuestaFallidoFicheroMegara
