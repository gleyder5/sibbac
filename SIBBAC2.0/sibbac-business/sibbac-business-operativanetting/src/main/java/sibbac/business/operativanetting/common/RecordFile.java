package sibbac.business.operativanetting.common;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Date;


public class RecordFile {

	private String	result;

	// se le pasa el valor string y la longitud que va a ocupar en el fichero
	public RecordFile( String _value, Integer _len ) {
		// Le añade espacios para completar la longitud establecida
		this.result = fillString( _value, _len );
	}

	// se le pasa el valor string y la longitud que va a ocupar en el fichero
	public RecordFile( String _value, Integer _len, char rellenar_izq ) {
		// Le añade espacios para completar la longitud establecida
		this.result = fillStringLeft( _value, _len, rellenar_izq );
	}

	// se le pasa el integer y la longitud que va a ocupar en el fichero
	public RecordFile( Integer _value, Integer _len ) {
		// Le añade espacios para completar la longitud establecida
		this.result = fillInteger( _value, _len );
	}

	// para tipos decimal se le pasa la longitud tota, el numero de decimales y el signo
	public RecordFile( BigDecimal _value, Integer _len, Integer _decimales, boolean _signo ) {
		// se formatea de acuerdo a los datos indicados
		this.result = formatNum( _value, _len, _decimales, _signo );

	}

	// para los datos de días
	public RecordFile( Date _value, Integer _len ) {
		// se le quita los guiones
		String valor_aux = "";
		if ( _value != null ) {
			valor_aux = _value.toString();
			valor_aux = valor_aux.replace( "-", "" );
		}
		this.result = fillString( valor_aux.toString().trim(), _len );

	}

	public String getResult() {
		return this.result;
	}

	void setResult( String _result ) {
		this.result = _result;
	}

	// Rellena con espacios hasata la longitud solitidada
	private String fillString( String valor, Integer longitud ) {

		if ( valor == null ) {
			valor = "";
		}

		// si la longitud es mayor a la pedida hay algo mal en los datos y los marcamos a X
		if ( valor.trim().length() > longitud ) {
			char[] error = new char[ longitud ];
			Arrays.fill( error, 'X' );
			valor = new String( error );
			return valor;
		}

		valor = valor.trim();
		char[] resto = new char[ longitud - valor.length() ];
		Arrays.fill( resto, ' ' );
		valor += new String( resto );

		return valor;

	}

	// Rellena con espacios hasata la longitud solitidada
	private String fillStringLeft( String valor, Integer longitud, char rellenar_izq ) {

		if ( valor == null ) {
			valor = "";
		}

		// si la longitud es mayor a la pedida hay algo mal en los datos y los marcamos a X
		if ( valor.trim().length() > longitud ) {
			char[] error = new char[ longitud ];
			Arrays.fill( error, 'X' );
			valor = new String( error );
			return valor;
		}

		valor = valor.trim();
		char[] resto = new char[ longitud - valor.length() ];
		Arrays.fill( resto, rellenar_izq );
		valor = new String( resto ) + valor;

		return valor;

	}

	// Rellena con espacios hasata la longitud solitidada
	private String fillInteger( Integer entero, Integer longitud ) {

		if ( entero == null ) {
			entero = 0;
		}

		String valor = String.valueOf( entero );

		// si la longitud es mayor a la pedida hay algo mal en los datos y los marcamos a X
		if ( valor.trim().length() > longitud ) {
			char[] error = new char[ longitud ];
			Arrays.fill( error, 'X' );
			valor = new String( error );
			return valor;
		}

		valor = valor.trim();
		char[] resto = new char[ longitud - valor.length() ];
		Arrays.fill( resto, '0' );
		// añadirmo los ceros por la izquierda
		valor = new String( resto ) + valor;

		return valor;

	}

	private String formatNum( BigDecimal valor, Integer longitud, Integer decimales, boolean signo ) {

		if ( signo )
			longitud--;// quitamos uno para el signo

		if ( valor == null ) {
			valor = new BigDecimal( 0 );
		}
		if ( !signo ) {
			if ( valor.compareTo( new BigDecimal( 0 ) ) == -1 ) {
				valor = valor.abs();
			}
		}

		// Enteros es la longitud total, asi que hay que quitarle los decimales
		longitud = longitud - decimales;
		String valorFmt = "";
		// Pattern
		String pattern = "";
		char[] ptnEnteros = new char[ longitud ];
		Arrays.fill( ptnEnteros, '0' );
		pattern = new String( ptnEnteros );
		if ( decimales != 0 ) {
			char[] ptnDecimales = new char[ decimales ];
			Arrays.fill( ptnDecimales, '0' );
			pattern = pattern + "." + new String( ptnDecimales );
		}
		// Formatear
		DecimalFormat df = new DecimalFormat( pattern );
		valorFmt = df.format( valor );
		valorFmt = removeChars( valorFmt, df.getDecimalFormatSymbols().getDecimalSeparator() );
		// Añadir signo positivo si procede
		if ( signo ) {
			if ( valor.signum() >= 0 ) {
				valorFmt = "+" + valorFmt;
			} // cuando es negativo ya viene nos viene el simbolo -
		}
		return valorFmt;
	}

	private String removeChars( String src, char chr ) {
		if ( src.indexOf( ( int ) chr ) < 0 ) {
			return src;
		}
		char[] dst = new char[ src.length() - 1 ];
		char[] src2 = src.toCharArray();
		int i = 0;
		for ( char c : src2 ) {
			if ( c == chr ) {
				continue;
			}
			dst[ i ] = c;
			i++;
		}

		return new String( dst );
	}

}
