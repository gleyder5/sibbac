package sibbac.business.operativanetting.database.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

/**
 * The Class MinimoImporte.
 */
@Entity
@Table(name = DBConstants.CLEARING.MINIMOIMPORTEDEMORA)
@Audited
public class MinimoImporte extends ATableAudited<MinimoImporte> implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5764166887810988465L;

	/** The alias. */
	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_ALIAS", referencedColumnName = "ID", nullable = true, unique = true)
	private Alias alias;

	/** The intereses. */
	@Column(name = "MINIMO", nullable = false, precision = 15, scale = 2)
	private BigDecimal minimo;

	/**
	 * Instantiates a new minimo importe.
	 */
	public MinimoImporte() {
		super();
	}

	/**
	 * Gets the alias.
	 *
	 * @return the alias
	 */
	public Alias getAlias() {
		return alias;
	}

	/**
	 * Sets the alias.
	 *
	 * @param alias the new alias
	 */
	public void setAlias(Alias alias) {
		this.alias = alias;
	}

	/**
	 * Gets the minimo.
	 *
	 * @return the minimo
	 */
	public BigDecimal getMinimo() {
		return minimo;
	}

	/**
	 * Sets the minimo.
	 *
	 * @param minimo the new minimo
	 */
	public void setMinimo(BigDecimal minimo) {
		this.minimo = minimo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((minimo == null) ? 0 : minimo.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinimoImporte other = (MinimoImporte) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (minimo == null) {
			if (other.minimo != null)
				return false;
		} else if (!minimo.equals(other.minimo))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see sibbac.database.model.ATable#toString()
	 */
	@Override
	public String toString() {
		return "MinimoImporte [alias=" + alias + ", minimo=" + minimo + "]";
	}


}
