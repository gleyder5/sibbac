package sibbac.business.operativanetting.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.operativanetting.database.model.Datosdiscrepantes;


@Repository
public interface DatosdiscrepantesDao extends JpaRepository< Datosdiscrepantes, Long > {

	Datosdiscrepantes findByCodigo( String codigo );

}
