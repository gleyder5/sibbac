package sibbac.business.operativanetting.common;

import java.util.Map;
import java.util.List;
import java.text.MessageFormat;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.SIBBACBusinessException;

public class ReferenciaPata {
    
    private static final Logger LOG = LoggerFactory.getLogger(ReferenciaPata.class);

    public static List<ReferenciaPata> buildReferencias(List<Map<String,String>> params) {
        final List<ReferenciaPata> lista;
        
        lista = new ArrayList<>();
        for(Map<String,String> item : params)
            lista.add(new ReferenciaPata(item));
        return lista;
    }
    
    private static Long mapToLong(String value) {
        if(value == null)
            return null;
        try {
            return Long.parseLong(value);
        }
        catch(NumberFormatException nfe) {
            LOG.debug("[mapToLong] se ha intentado parsear como long el valor {}", value);
            return null;
        }
    };
    
    private final String pata;
    
    private final Long alcOrdenesId;
    
    private final Long alcOrdenMercadoId;
    
    private ReferenciaPata(Map<String,String> paramItem) {
        pata = paramItem.get("pata");
        alcOrdenesId = mapToLong(paramItem.get("alcOrdenesId"));
        alcOrdenMercadoId = mapToLong(paramItem.get("alcOrdenMercadoId"));
    }
    
    public boolean isPataMercado() {
        if(pata != null) 
            return pata.equals("M");
        return false;
    }
    
    public boolean isPataCliente() {
        if(pata != null)
            return pata.equals("C");
        return false;
    }
    
    @Override
    public String toString() {
        return MessageFormat.format("pata: {0}; alcOrdenesId: {1}; alcOrdenMercadoId: {2}", pata, alcOrdenesId, 
                alcOrdenMercadoId);
    }
    
    public long getAlcOrdenesId() throws SIBBACBusinessException {
        if(alcOrdenesId == null)
            throwBussinessException("No se proporciona Id de Alc orden: {0}");
        return alcOrdenesId;
    }
    
    public long getAlcOrdenMercadoId() throws SIBBACBusinessException {
        if(alcOrdenMercadoId == null)
            throwBussinessException("No se proporciona Id de Alc orden mercado: {0}");
        return alcOrdenMercadoId;
    }

    private void throwBussinessException(String format) throws SIBBACBusinessException {
        throw new SIBBACBusinessException(MessageFormat.format(format, this));
    }

}
