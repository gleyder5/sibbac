package sibbac.business.operativanetting.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;


public interface Constantes {

	// Comando para los servicios REST
	public static final String	CMD_LIST_LIQUIDACION_S3							= "ListaLiquidacionS3";
	public static final String	CMD_LIST_ERRORES_S3								= "listaErroresS3";
	public static final String	CMD_CONSULTA_NETTING_SIN_MOV_TIT				= "listaNettingSinMovimientoTitulos";
	public static final String	CMD_CONSULTA_NETTING							= "listaNetting";
	public static final String	CMD_LIST_ENTREGA_RECEPCION						= "ListaEntregaRecepcion";
	public static final String	CMD_CUENTAS_COMPENSACION						= "CuentasCompensacion";
	public static final String	CMD_ESTADOS_CLEARING							= "EstadosClearing";
	public static final String	CMD_MARCAR_GASTOS								= "MarcarGastos";
	public static final String	CMD_ALIAS_NETTING								= "AliasNetting";
	public static final String	CMD_TODOS_ALIAS									= "TodosAlias";
	public static final String	CMD_TODOS_ISIN									= "TodosIsin";
	public static final String  CMD_CANCELACION_CLIENTE							= "CancelacionCliente";
	public static final String  CMD_CANCELACION_TOTAL							= "CancelacionTotal";
	public static final String  CMD_CIF_SVB							            = "CifSvb";
	public static final String  CMD_REENVIO                                     = "Reenvio";

	// Datos de respuesta
	public static final String	RESULT_LISTA									= "lista";

	// Campos para los datos de los servicios REST
	public static final String	SRV_NBOOKING									= "nbooking";
	public static final String	SRV_NUCNFCLT									= "nucnfclt";
	public static final String	SRV_NUCNFLIQ									= "nucnfliq";

	// Constanstes para la generacion del fichero S3
	public static final String	MEGARA_MMNN_MTF_NUCTADEP						= "NUCTADEP";
	public static final String	CDTPOPER										= "CDTPOPER";
	public static final String	CLIENTEN										= "CLIENTEN";
	public static final String	BMEECC											= "BMEECC";
	public static final String	MERMOVCONC										= "MERMOVCONC";
	public static final String	CLIMOVCONC										= "CLIMOVCONC";
	public static final String	ECC												= "ECC";
	public static final String	BSCHESMMXXX										= "BSCHESMMXXX";
	public static final String	MEGARA_FILE_PREFIX								= "SVBMN_";
	public static final char	CHAR_NO											= 'N';
	public static final char	CHAR_SI											= 'S';
	public static final String	STRING_NO										= "N";
	public static final String	SBRMV											= "SBRMV";
	public static final char	CHAR_CLIENTE									= 'C';
	public static final char	CHAR_MERCADO									= 'M';
	public static final String	TMCT0MSC										= "TMCT0MSC";
	public static final String	COMPRA											= "C";
	public static final String	VENTA											= "V";
	public static final Integer	SIZE_LINEA_RESPUESTA							= 74;
	public static final String	COIF											= "COIF";
	public static final String	VEIF											= "VEIF";

	// Codigo tipo de de cuentas de compensacion
	public static final String	CIEN											= "CIEN";
	public static final String	CIEB											= "CIEB";

	// Codigos de la tabla TMCT0MEN para identificar el semaforo del envio y la respuesta del fichero S3
	public static final String	CODIGO_MEGARA_ENVIO_INSTRUCCIONES_NACIONAL		= "003";
	public static final String	CODIGO_MEGARA_RECEPCION_INSTRUCCIONES_NACIONAL	= "005";

	// Estados para dejar la tabla TMCT0MEN
	public static final String	ESTADO_LISTO_GENERAR							= "010";
	public static final String	ESTADO_EN_EJECUCION								= "900";
	public static final String	ESTADO_EN_ERROR									= "000";

	// Codigo de estado devuelto en la respuest del fichero S3
	public static final String	CDEST_PENDIENTE									= "PDT";
	public static final String	CDEST_UNMATCH									= "UNM";
	public static final String	CDEST_MATCHED									= "MCH";
	public static final String	CDEST_FALLIDA									= "ERR";
	public static final String	CDEST_LIQUIDADA									= "LIQ";
	public static final String	CD_DATOS_DISCREPANTE_NARR						= "NARR";
	public static final String  CDEST_CANC_ACEPTADA                 = "CAC";
  public static final String  CDEST_CANC_RECHAZADA           = "CRE";
	
	
	public static final Object CD_CUENTA_COMPENSACION_00P 						= "00P";
	
  /** Sufijo que se le añade a los ficheros que están siendo procesados. */
	public static final String WORKING_FILE_SUFFIX = ".working";

  /** Sufijo que se le añade a los ficheros que han producido errores durante el procesado. */
	public static final String ERROR_FILE_SUFFIX = ".error";
	
	/** Formato de las fechas que se leen del fichero. */
	public static final DateFormat FILE_DATE_FORMAT = new SimpleDateFormat("ddMMyyyy");

  /** Formato de las fechas que se leen del fichero. */
	public static final DateFormat DEST_FILE_DATE_FORMAT = new SimpleDateFormat("ddMMyyyy_HHmm");

}
