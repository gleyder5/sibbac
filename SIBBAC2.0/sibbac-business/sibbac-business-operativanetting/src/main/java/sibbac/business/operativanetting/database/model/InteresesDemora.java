package sibbac.business.operativanetting.database.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;

/**
 * The Class InteresesDemora.
 */
@Entity
@Table(name = DBConstants.CLEARING.INTERESESDEMORA)
@Audited
public class InteresesDemora extends ATableAudited<InteresesDemora> implements
		FechaInicioFechaHasta {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -569316784927283322L;

	/** The alias. */
	@ManyToOne(optional = true)
	@JoinColumn(name = "ID_ALIAS", referencedColumnName = "ID", nullable = true)
	private Alias alias;

	/** The fecha inicio. */
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIO", nullable = false, length = 4)
	private Date fechaInicio;

	/** The fecha hasta. */
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAHASTA", nullable = false, length = 4)
	private Date fechaHasta;

	/** The intereses. */
	@Column(name = "INTERESES", nullable = false, precision = 2, scale = 2)
	private BigDecimal intereses;
	
	/** The base. */
	@Column(name = "BASE", nullable = false, length=3)
	private Integer base;

	/**
	 * Instantiates a new intereses demora.
	 */
	public InteresesDemora() {
		super();
	}

	/* (non-Javadoc)
	 * @see sibbac.business.operativanetting.database.model.FechaInicioFechaHasta#getAlias()
	 */
	@Override
	public Alias getAlias() {
		return alias;
	}

	/**
	 * Sets the alias.
	 *
	 * @param alias the new alias
	 */
	public void setAlias(Alias alias) {
		this.alias = alias;
	}

	/* (non-Javadoc)
	 * @see sibbac.business.operativanetting.database.model.FechaInicioFechaHasta#getFechaInicio()
	 */
	@Override
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * Sets the fecha inicio.
	 *
	 * @param fechaInicio the new fecha inicio
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/* (non-Javadoc)
	 * @see sibbac.business.operativanetting.database.model.FechaInicioFechaHasta#getFechaHasta()
	 */
	@Override
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * Sets the fecha hasta.
	 *
	 * @param fechaHasta the new fecha hasta
	 */
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * Gets the intereses.
	 *
	 * @return the intereses
	 */
	public BigDecimal getIntereses() {
		return intereses;
	}

	/**
	 * Sets the intereses.
	 *
	 * @param intereses the new intereses
	 */
	public void setIntereses(BigDecimal intereses) {
		this.intereses = intereses;
	}

	/**
	 * Gets the base.
	 *
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/**
	 * Sets the base.
	 *
	 * @param base the new base
	 */
	public void setBase(Integer base) {
		this.base = base;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result
				+ ((fechaHasta == null) ? 0 : fechaHasta.hashCode());
		result = prime * result
				+ ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result
				+ ((intereses == null) ? 0 : intereses.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InteresesDemora other = (InteresesDemora) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (fechaHasta == null) {
			if (other.fechaHasta != null)
				return false;
		} else if (!fechaHasta.equals(other.fechaHasta))
			return false;
		if (fechaInicio == null) {
			if (other.fechaInicio != null)
				return false;
		} else if (!fechaInicio.equals(other.fechaInicio))
			return false;
		if (intereses == null) {
			if (other.intereses != null)
				return false;
		} else if (!intereses.equals(other.intereses))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see sibbac.database.model.ATable#toString()
	 */
	@Override
	public String toString() {
		return "InteresesDemora [alias=" + alias + ", fechaInicio="
				+ fechaInicio + ", fechaHasta=" + fechaHasta + ", intereses="
				+ intereses + ", base=" + base + "]";
	}
	
	
}
