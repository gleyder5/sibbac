package sibbac.business.operativanetting.database.bo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.operativanetting.bean.MinimoImporteAlias;
import sibbac.business.operativanetting.database.dao.MinimoImporteDao;
import sibbac.business.operativanetting.database.model.MinimoImporte;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.database.bo.AbstractBo;

/**
 * The Class MinimoImporteBo.
 */
@Service
public class MinimoImporteBo extends
		AbstractBo<MinimoImporte, Long, MinimoImporteDao> {

	/**
	 * Find all with alias.
	 *
	 * @return the list
	 */
	public List<MinimoImporteAlias> findAllWithAlias() {
		List<MinimoImporteAlias> result = new ArrayList<MinimoImporteAlias>();
		List<MinimoImporte> importes = dao.findAll();
		for (MinimoImporte minimoImporte : importes) {
			if (minimoImporte.getAlias() == null) {
				result.add(new MinimoImporteAlias(minimoImporte.getId(),
						minimoImporte.getMinimo(), ""));
			} else {
				result.add(new MinimoImporteAlias(minimoImporte.getId(),
						minimoImporte.getMinimo(), minimoImporte.getAlias()
								.formarNombreAlias()));
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.business.operativanetting.database.bo.AbstractDecoratorInterval
	 * #findByAlias(sibbac.business.wrappers.database.model.Alias)
	 */
	/**
	 * Find by alias.
	 *
	 * @param alias the alias
	 * @return the list
	 */
	public List<MinimoImporte> findByAlias(Alias alias) {
		List<MinimoImporte> result;
		if (alias == null) {
			result = dao.findAllByAliasIsNull();
		} else {
			result = dao.findAllByAlias(alias);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see sibbac.database.bo.AbstractBo#save(java.lang.Object)
	 */
	public MinimoImporte save(MinimoImporte minimoImporte) {
		MinimoImporte importe;
		MinimoImporte result;
		List<MinimoImporte> importes = findByAlias(minimoImporte.getAlias());
		if (importes.size() >= 1) {
			importe = importes.get(0);
			importe.setMinimo(minimoImporte.getMinimo());
			result = super.save(importe);
		} else {
			result = super.save(minimoImporte);
		}
		return result;
	}

}
