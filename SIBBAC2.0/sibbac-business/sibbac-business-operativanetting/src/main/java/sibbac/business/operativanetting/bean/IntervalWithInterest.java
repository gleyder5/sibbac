package sibbac.business.operativanetting.bean;

import java.math.BigDecimal;

import org.joda.time.Interval;

// TODO: Auto-generated Javadoc
/**
 * The Class DaysWithInterest.
 */
public class IntervalWithInterest {

	/** The interest. */
	private final BigDecimal interest;

	/** The days. */
	private final Interval interval;

	/** The base. */
	private final Integer base;

	/**
	 * Instantiates a new days with interest.
	 *
	 * @param interval
	 *            the interval
	 * @param interest
	 *            the interest
	 * @param base
	 *            the base
	 */
	public IntervalWithInterest(Interval interval, BigDecimal interest,
			Integer base) {
		super();
		this.interest = interest;
		this.interval = interval;
		this.base = base;
	}

	/**
	 * Gets the interest.
	 *
	 * @return the interest
	 */
	public BigDecimal getInterest() {
		return interest;
	}

	/**
	 * Gets the interval.
	 *
	 * @return the interval
	 */
	public Interval getInterval() {
		return interval;
	}

	/**
	 * Gets the base.
	 *
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result
				+ ((interest == null) ? 0 : interest.hashCode());
		result = prime * result
				+ ((interval == null) ? 0 : interval.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntervalWithInterest other = (IntervalWithInterest) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (interest == null) {
			if (other.interest != null)
				return false;
		} else if (!interest.equals(other.interest))
			return false;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IntervalWithInterest [interest=" + interest + ", interval="
				+ interval + ", base=" + base + "]";
	}

}
