package sibbac.business.operativanetting.tasks;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.DateUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.operativanetting.database.bo.EnvioClearingNettingBo;
import sibbac.business.operativanetting.exceptions.EnvioFicheroMegaraException;
import sibbac.business.operativanetting.exceptions.LoadConfigurationException;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_CLEARING.NAME, name = Task.GROUP_CLEARING.JOB_ENVIO_FICHERO_MEGARA, interval = 30, intervalUnit = IntervalUnit.MINUTE)
public class TaskEnvioFicheroMegara extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskEnvioFicheroMegara.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0cfg</code>. */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  private ApplicationContext ctx;

  /** Inicio del intervalo horario durante el que se puede ejecutar el proceso. */
  private Integer cfgIntervalStartTime;

  /** Final del intervalo horario durante el que se puede ejecutar el proceso. */
  private Integer cfgIntervalEndTime;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /** Constructor I. Constructor por defecto. */
  public TaskEnvioFicheroMegara() {
  } // TaskEnvioFicheroMegara

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void execute() {
    // Se marca el momento de inicio de la tarea
    final Long startTime = System.currentTimeMillis();
    final EnvioClearingNettingBo envioClearingNettingBo;

    LOG.debug("[TaskEnvioFicheroMegara :: execute] Iniciando la tarea de envio del fichero de Megara ...");

    Tmct0men tmct0men;

    try {
      // Se bloquea el semaforo
      tmct0men = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_CLEARING_ENVIO_FICHERO_MEGARA, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);
      try {
        LOG.debug("[TaskEnvioFicheroMegara :: execute] Ejecutando el proceso de envio del fichero megara ...");

        String result = null;

        // Se carga la configuración
        if (loadConfiguration()) {
          // Se obtiene la hora actual
          Integer horaActual = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

          // Si se está en el intervalo de ejecución de la tarea se ejecuta la lógica de la misma
          if (cfgIntervalStartTime <= horaActual && horaActual <= cfgIntervalEndTime) {
            Integer nuEnvio = new Integer(1);
            if (DateUtils.isSameDay(new Date(), tmct0men.getFemensa())) {
              nuEnvio = tmct0men.getNuenvio() + 1;
            } // if
            LOG.debug("[TaskEnvioFicheroMegara :: execute] Numero de envio a realizar: " + nuEnvio);
            envioClearingNettingBo = ctx.getBean(EnvioClearingNettingBo.class);
            result = envioClearingNettingBo.ejecutarEnvioOperaciones(nuEnvio);
          } else {
            LOG.info("[TaskEnvioFicheroMegara :: execute] La tarea no se ejecuta por no encontrarnos en el intervalo horario para su ejecucion");
          } // else

          LOG.debug("[TaskEnvioFicheroMegara :: execute] Tarea terminada correctamente");
        } else {
          LOG.info("[TaskEnvioFicheroMegara :: execute] La configuracion no permite la ejecucion de la tarea");
        } // else

        // Se desbloquea el semaforo
        if (result != null) {
          if (!result.contains("##")) {
            LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.LISTO_GENERAR");
            tmct0men.setNfile(result);
            tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.LISTO_GENERAR);
          } else {
            LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.ERROR");
            String[] resultado = result.split("##");
            if (resultado[0].compareTo("<null>") == 0) {
              LOG.warn("[TaskEnvioFicheroMegara :: execute] NO se ha escrito fichero pero ha habido error fatal en otros registros ... "
                       + result.split("##")[1]);
              tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
            } else {
              LOG.warn("[TaskEnvioFicheroMegara :: execute] Se ha escrito fichero pero ha habido error fatal en otros registros ... "
                  + result.split("##")[1]);
              tmct0men.setNfile(resultado[0]);
              tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.EN_ERROR);
            }
          } // else
        } else {
          LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.LISTO_GENERAR");
          tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.LISTO_GENERAR);
        } // else

      } catch (LoadConfigurationException lce) {
        LOG.error("[TaskEnvioFicheroMegara :: execute] Error al ejecutar la tarea ... ", lce.getMessage());
        LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.EN_ERROR");
        // Se pone el semaforo en error
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } catch (EnvioFicheroMegaraException efme) {
        LOG.error("[TaskEnvioFicheroMegara :: execute] Error al ejecutar la logica de envio ... ", efme.getMessage());
        LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.EN_ERROR");
        // Se pone el semaforo en error
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } catch (Exception e) {
        LOG.error("[TaskEnvioFicheroMegara :: execute] Error al ejecutar la tarea ... ", e.getMessage());
        LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.EN_ERROR");
        // Se pone el semaforo en error
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } // catch
    } catch (SIBBACBusinessException e) {
      LOG.warn("[TaskEnvioFicheroMegara :: execute] Error intentando bloquear el semaforo ... " + e.getMessage());
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[TaskEnvioFicheroMegara :: execute] Finalizada la tarea de envio del fichero de Megara ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally
  } // execute

  /**
   * Carga los datos de configuración del proceso.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si los datos de configuración se cargaron correctamente; <code>false</code> en caso
   *         contrario.
   * @throws LoadConfigurationException Si ocurre algún error al cargar la configuración o no se encuentra alguna propiedad.
   */
  private Boolean loadConfiguration() throws LoadConfigurationException {
    LOG.debug("[TaskEnvioFicheroMegara :: loadConfiguration] Cargando configuración del proceso ...");

    Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                                  Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getProcess(),
                                                                                  Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuración para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getKeyname());
    } // if

    if (datosConfiguracion.getKeyvalue().equals("N") || datosConfiguracion.getKeyvalue().equals("")) {
      LOG.warn("[TaskEnvioFicheroMegara :: loadConfiguration] El flag del proceso de generación del fichero de Megara no está activo: "
               + Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getKeyname());
      return false;
    } // if

    // Se recupera el inicio del intervalo horario durante el que se puede ejecutar el proceso
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_begin_hour_megara_nacional.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_begin_hour_megara_nacional.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuración para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_NACIONAL_begin_hour_megara_nacional.getKeyname());
    } // if
    cfgIntervalStartTime = Integer.valueOf(datosConfiguracion.getKeyvalue());

    // Se recupera el fin del intervalo horario durante el que se puede ejecutar el proceso
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_end_hour_megara_nacional.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_end_hour_megara_nacional.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuración para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_NACIONAL_end_hour_megara_nacional.getKeyname());
    } // if
    cfgIntervalEndTime = Integer.valueOf(datosConfiguracion.getKeyvalue());

    return true;
  } // loadConfiguration

} // TaskEnvioFicheroMegara
