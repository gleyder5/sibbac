package sibbac.business.operativanetting.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class InteresesDemoraAlias.
 */
public class InteresesDemoraAlias {

	/** The alias. */
	private final String alias;

	/** The base. */
	private final Integer base;

	/** The fecha hasta. */
	private final Date fechaHasta;

	/** The fecha inicio. */
	private final Date fechaInicio;

	/** The id. */
	private final Long id;

	/** The intereses. */
	private final BigDecimal intereses;

	/**
	 * Instantiates a new intereses demora alias.
	 *
	 * @param id the id
	 * @param fechaInicio the fecha inicio
	 * @param fechaHasta the fecha hasta
	 * @param intereses the intereses
	 * @param alias the alias
	 * @param base the base
	 */
	public InteresesDemoraAlias(Long id, Date fechaInicio, Date fechaHasta,
			BigDecimal intereses, String alias, Integer base) {
		super();
		this.id = id;
		this.fechaInicio = fechaInicio;
		this.fechaHasta = fechaHasta;
		this.intereses = intereses;
		this.alias = alias;
		this.base = base;
	}



	/**
	 * Gets the alias.
	 *
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Gets the base.
	 *
	 * @return the base
	 */
	public Integer getBase() {
		return base;
	}

	/**
	 * Gets the fecha hasta.
	 *
	 * @return the fecha hasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * Gets the fecha inicio.
	 *
	 * @return the fecha inicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the intereses.
	 *
	 * @return the intereses
	 */
	public BigDecimal getIntereses() {
		return intereses;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InteresesDemoraAlias [id=" + id + ", fechaInicio="
				+ fechaInicio + ", fechaHasta=" + fechaHasta + ", intereses="
				+ intereses + ", alias=" + alias + ", base=" + base + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result
				+ ((fechaHasta == null) ? 0 : fechaHasta.hashCode());
		result = prime * result
				+ ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((intereses == null) ? 0 : intereses.hashCode());
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InteresesDemoraAlias other = (InteresesDemoraAlias) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (fechaHasta == null) {
			if (other.fechaHasta != null)
				return false;
		} else if (!fechaHasta.equals(other.fechaHasta))
			return false;
		if (fechaInicio == null) {
			if (other.fechaInicio != null)
				return false;
		} else if (!fechaInicio.equals(other.fechaInicio))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (intereses == null) {
			if (other.intereses != null)
				return false;
		} else if (!intereses.equals(other.intereses))
			return false;
		return true;
	}

}
