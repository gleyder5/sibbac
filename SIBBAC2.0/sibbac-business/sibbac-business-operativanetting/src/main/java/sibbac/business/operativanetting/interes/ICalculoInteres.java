package sibbac.business.operativanetting.interes;

/**
 * The Interface ICalculoInteres.
 */
public interface ICalculoInteres {

	/**
	 * Calculate.
	 *
	 * @param entradaCalculoInteresBean the entrada calculo interes bean
	 * @return the resultado calculo interes bean
	 */
	ResultadoCalculoInteresBean calculate(
			EntradaCalculoInteresBean entradaCalculoInteresBean);

}
