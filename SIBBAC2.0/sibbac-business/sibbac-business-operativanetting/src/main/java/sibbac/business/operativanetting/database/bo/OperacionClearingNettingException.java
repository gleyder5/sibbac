package sibbac.business.operativanetting.database.bo;

import java.util.List;

import sibbac.common.SIBBACBusinessException;

/**
 * Excepción generada para provocar un Rollback en las transacciones de 
 * OperacionesClearingNetting.
 */
public class OperacionClearingNettingException extends SIBBACBusinessException {

    /**
     * Número de serie generado 
     */
    private static final long serialVersionUID = 4756882729663531190L;
    
    private final List<String> messages;
    
    OperacionClearingNettingException(String cause, List<String> messages) {
        super(cause);
        this.messages = messages;
    }
    
    public List<String> getOpMessages() {
        return messages;
    }
    
}
