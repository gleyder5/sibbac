package sibbac.business.operativanetting.interes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.operativanetting.bean.DayWithInterest;
import sibbac.business.operativanetting.bean.IntervalWithInterest;
import sibbac.business.operativanetting.database.bo.InteresesDemoraBo;
import sibbac.business.operativanetting.database.model.InteresesDemora;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.model.Alias;

/**
 * The Class CalculoInteres.
 */
@Service
final class CalculoInteres implements ICalculoInteres {

	/** The interes demora bo. */
	@Autowired
	private InteresesDemoraBo interesDemoraBo;

	/** The alias bo. */
	@Autowired
	private AliasBo aliasBo;

	/** The Constant ONE_HUNDRED. */
	private final static BigDecimal ONE_HUNDRED = new BigDecimal("100");

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.business.operativanetting.interes.ICalculoInteres#calculate(sibbac
	 * .business.operativanetting.interes.EntradaCalculoInteresBean)
	 */
	@Override
	public ResultadoCalculoInteresBean calculate(
			EntradaCalculoInteresBean entradaCalculoInteresBean) {
		DateTime fechaInicio = new DateTime(entradaCalculoInteresBean
				.getFechaDesde().getTime()).minusDays(1);
		DateTime fechaHasta = new DateTime(entradaCalculoInteresBean
				.getFechaHasta().getTime());
		Alias alias = getAlias(entradaCalculoInteresBean.getAlias());
		List<InteresesDemora> interesesDemorasByAlias = interesDemoraBo
				.findByAlias(alias);
		List<InteresesDemora> interesesDemorasByDefault = interesDemoraBo
				.findByAlias(null);
		List<IntervalWithInterest> daysWithInterestsByAlias = getDaysWithCost(
				interesesDemorasByAlias, fechaInicio, fechaHasta);
		List<IntervalWithInterest> daysWithInterestsByDefault = getDaysWithCost(
				interesesDemorasByDefault, fechaInicio, fechaHasta);
		List<DayWithInterest> dayWithInterests = getDaysWithInterest(
				daysWithInterestsByAlias, daysWithInterestsByDefault,
				fechaInicio, fechaHasta);
		return getCalculo(dayWithInterests,
				entradaCalculoInteresBean.getImporte());
	}

	/**
	 * Gets the days with interest.
	 *
	 * @param daysWithInterestsByAlias
	 *            the days with interests by alias
	 * @param daysWithInterestsByDefault
	 *            the days with interests by default
	 * @param fechaInicio
	 *            the fecha inicio
	 * @param fechaHasta
	 *            the fecha hasta
	 * @return the days with interest
	 */
	private List<DayWithInterest> getDaysWithInterest(
			List<IntervalWithInterest> daysWithInterestsByAlias,
			List<IntervalWithInterest> daysWithInterestsByDefault,
			DateTime fechaInicio, DateTime fechaHasta) {
		List<DayWithInterest> result = new ArrayList<DayWithInterest>();
		Integer days = Days.daysBetween(fechaInicio, fechaHasta).getDays();
		DateTime fechaCalculo = fechaInicio;
		Boolean flag;
		for (int i = 1; i < days; i++) {
			fechaCalculo = fechaInicio.plusDays(i);
			flag = Boolean.TRUE;
			for (IntervalWithInterest daysWithInterest : daysWithInterestsByAlias) {
				if (daysWithInterest.getInterval().contains(fechaCalculo)) {
					result.add(new DayWithInterest(fechaCalculo,
							daysWithInterest.getInterest(), daysWithInterest
									.getBase(), Boolean.FALSE));
					flag = Boolean.FALSE;
				}
			}
			if (flag) {
				for (IntervalWithInterest dayWithInterest : daysWithInterestsByDefault) {
					if (dayWithInterest.getInterval().contains(fechaCalculo)) {
						result.add(new DayWithInterest(fechaCalculo,
								dayWithInterest.getInterest(), dayWithInterest
										.getBase(), Boolean.TRUE));
					}
				}
			}
		}
		return result;
	}

	/**
	 * Gets the calculo.
	 *
	 * @param daysWithInterests
	 *            the days with interests
	 * @param importe
	 *            the importe
	 * @return the calculo
	 */
	private ResultadoCalculoInteresBean getCalculo(
			final List<DayWithInterest> daysWithInterests,
			final BigDecimal importe) {
		List<TipoBaseBean> tipoBaseBeans = new ArrayList<TipoBaseBean>();
		Set<Integer> bases = new HashSet<Integer>();
		BigDecimal sumatorioIntereses = BigDecimal.ZERO;
		BigDecimal interesMedio = BigDecimal.ZERO;
		BigDecimal importeTotal = BigDecimal.ZERO;
		BigDecimal importeInteres = BigDecimal.ZERO;
		Boolean flag = Boolean.TRUE;
		TipoBaseBean primeraTipoBaseBean = null;
		Integer sumatorioBase = 0;
		Integer primerBase = 0;
		TipoBaseBean tipoBaseBean;
		ResultadoCalculoInteresBean resultadoCalculoInteresBean;
		if (daysWithInterests.size() != 0
				&& BigDecimal.ZERO.compareTo(importe) != 0) {
			for (DayWithInterest dayWithInterest : daysWithInterests) {
				sumatorioIntereses = sumatorioIntereses.add(dayWithInterest
						.getInterest());
				sumatorioBase += dayWithInterest.getBase();
				bases.add(dayWithInterest.getBase());
				if (flag) {
					primerBase = dayWithInterest.getBase();
					flag = Boolean.FALSE;
				}
			}
			interesMedio = sumatorioIntereses.divide(new BigDecimal(
					sumatorioBase), 20, BigDecimal.ROUND_HALF_UP);
			for (Integer base : bases) {
				tipoBaseBean = new TipoBaseBean(
						interesMedio.multiply(new BigDecimal(base)), base);
				tipoBaseBeans.add(tipoBaseBean);
				if (base.equals(primerBase)) {
					primeraTipoBaseBean = tipoBaseBean;
				}
			}
			importeInteres = interesMedio
					.multiply(new BigDecimal(daysWithInterests.size()))
					.multiply(importe).divide(ONE_HUNDRED);
			importeTotal = importe.add(importeInteres);

			resultadoCalculoInteresBean = new ResultadoCalculoInteresBean(
					interesMedio, importeTotal, importe, importeInteres,
					tipoBaseBeans, primeraTipoBaseBean);
		} else {
			resultadoCalculoInteresBean = new ResultadoCalculoInteresBean(
					interesMedio, importeTotal, importe, importeInteres,
					tipoBaseBeans, new TipoBaseBean(BigDecimal.ZERO, 0));
		}

		return resultadoCalculoInteresBean;

	}

	/**
	 * Gets the days with cost.
	 *
	 * @param interesesDemoras
	 *            the intereses demoras
	 * @param fechaInicio
	 *            the fecha inicio
	 * @param fechaHasta
	 *            the fecha hasta
	 * @return the days with cost
	 */
	private List<IntervalWithInterest> getDaysWithCost(
			List<InteresesDemora> interesesDemoras, DateTime fechaInicio,
			DateTime fechaHasta) {
		List<IntervalWithInterest> result = new ArrayList<IntervalWithInterest>();
		DateTime startDay;
		DateTime endDay;
		Interval intervalStartDayEndDay;
		Interval intervalFechaInicioFechaFin = new Interval(
				fechaInicio.minus(1), fechaHasta.plus(1));
		for (InteresesDemora interesesDemora : interesesDemoras) {
			startDay = new DateTime(interesesDemora.getFechaInicio().getTime());
			endDay = new DateTime(interesesDemora.getFechaHasta().getTime());
			intervalStartDayEndDay = new Interval(startDay, endDay);
			if (intervalFechaInicioFechaFin.contains(startDay)
					&& intervalFechaInicioFechaFin.contains(endDay)) {
				result.add(new IntervalWithInterest(new Interval(startDay
						.minus(1), endDay.plus(1)), interesesDemora
						.getIntereses(), interesesDemora.getBase()));
			} else if (intervalFechaInicioFechaFin.contains(startDay)
					&& !intervalFechaInicioFechaFin.contains(endDay)) {
				result.add(new IntervalWithInterest(new Interval(startDay
						.minus(1), fechaHasta.plus(1)), interesesDemora
						.getIntereses(), interesesDemora.getBase()));
			} else if (!intervalFechaInicioFechaFin.contains(startDay)
					&& intervalFechaInicioFechaFin.contains(endDay)) {
				result.add(new IntervalWithInterest(new Interval(fechaInicio
						.minus(1), endDay.plus(1)), interesesDemora
						.getIntereses(), interesesDemora.getBase()));
			} else if (intervalStartDayEndDay
					.contains(intervalFechaInicioFechaFin)) {
				result.add(new IntervalWithInterest(new Interval(startDay
						.minus(1), endDay.plus(1)), interesesDemora
						.getIntereses(), interesesDemora.getBase()));
			}
		}
		return result;
	}

	/**
	 * Gets the alias.
	 *
	 * @param cdAlias
	 *            the cd alias
	 * @return the alias
	 */
	private Alias getAlias(String cdAlias) {
		Alias result = aliasBo.findByCdaliass(cdAlias);
		return result;
	}

}
