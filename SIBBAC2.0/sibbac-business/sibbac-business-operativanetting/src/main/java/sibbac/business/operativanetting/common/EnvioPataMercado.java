package sibbac.business.operativanetting.common;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ALC_ORDENES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.operativanetting.common.Utilidad.CLEARING_TIPO;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0MovimientoCuentaAliasBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0desgloseclititDao;
import sibbac.business.wrappers.database.dao.Tmct0movimientoeccDao;
import sibbac.business.wrappers.database.model.AlcOrdenMercado;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EnvioPataMercado extends EnvioClearingNetting {

  private static final Logger LOG = LoggerFactory.getLogger(EnvioPataMercado.class);

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0desgloseCamaraBo tmct0desgloseCamaraBo;

  @Autowired
  private Tmct0MovimientoCuentaAliasBo tmct0MovimientoCuentaAliasBo;

  @Autowired
  private Tmct0CamaraCompensacionBo tmct0CamaraCompensacionBo;

  @Autowired
  private Tmct0desgloseclititDao tmct0desgloseclititDao;

  @Autowired
  private Tmct0alcBo tmct0alcBo;

  @Autowired
  private Tmct0logBo tmct0logBo;

  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  @Autowired
  private Tmct0movimientoeccDao tmct0movimientoeccDao;

  @Autowired
  private Tmct0CuentasDeCompensacionBo tmct0CuentasDeCompensacionBo;

  @Autowired
  private Tmct0estadoBo estadoBo;

  private final Map<String, String> cuentasS3PorCodigo;

  public EnvioPataMercado() {
    cuentasS3PorCodigo = new HashMap<>();
  }

  @Override
  public void initProcess(Map<String, String> configParams) {
    initProcess(configParams, tmct0CuentasDeCompensacionBo, estadoBo);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
  public int generaEnvio(Date fechaBusqueda, PrintWriter prtWriter) throws SIBBACBusinessException {
    final List<String> ordenes;
    
    ordenes = tmct0alcBo.findOrdenesParaEnvioS3(fechaBusqueda);
    LOG.info("Total de ordenes para enviar a S3:  {}", ordenes.size());
    return procesaOrdenesMercado(ordenes, prtWriter);
  }

  /**
   * Procesa ordenes de mercado para su envío a S3
   * 
   * @param ordenes Listado de números de orden (NURDEN) a procesar
   * @param prWriter un PrintWriter que escribe al fichero final
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRED)
  private int procesaOrdenesMercado(final List<String> ordenes, final PrintWriter prWriter)
      throws SIBBACBusinessException {
    List<RegistroEnvioFicheroMegara> registrosMegara;
    List<Tmct0alc> alcsByOrden;
    int lineasEnviadas = 0;

    for (final String nuorden : ordenes) {
      if (!bokBo.booksValidosEnvioS3(nuorden)) {
        LOG.debug("[procesaOrdenesMercado] Los bookings no son validos para la orden {}", nuorden);
        continue;
      }
      LOG.debug("[procesaOrdenesMercado] Inicia mercado nuorden {}", nuorden);
      alcsByOrden = tmct0alcBo.findConDesglosesPorEnviar(nuorden);
      if (alcsByOrden.isEmpty()) {
        LOG.warn("[procesaOrdenesMercado] nuorden {} no contiene Alcs", nuorden);
        continue;
      }
      registrosMegara = resuelvePorOrden(nuorden, alcsByOrden);
      for (RegistroEnvioFicheroMegara registroMercado : registrosMegara) {
        if (registroMercado != null) {
          prWriter.print(lineSeparator);
          for (RecordFile recordFile : registroMercado.FormatearLineaFicheroRegistro01())
            prWriter.printf("%s", recordFile.getResult());
          lineasEnviadas++;
        }
      }
      LOG.debug("[procesaOrdenesMercado] Fin mercado nuorden {}", nuorden);
    }
    return lineasEnviadas;
  }

  private List<RegistroEnvioFicheroMegara> resuelvePorOrden(String nuorden, Collection<Tmct0alc> alcsByOrden)
      throws SIBBACBusinessException {
    final Map<AgrupacionPataMercado, AlcOrdenes> alcOrdenesExistentes;
    final Map<AgrupacionPataMercado, List<Tmct0desglosecamara>> desglosesNuevos;
    Map<AgrupacionPataMercado, List<Tmct0desglosecamara>> desglosesAgrupados;
    List<Tmct0desglosecamara> enLista;
    ValidacionPataMercado message;

    LOG.debug("[resuelvePorOrden] Inicio...");
    desglosesNuevos = new HashMap<>();
    alcOrdenesExistentes = agrupaAlcOrdenesExistentes(nuorden);
    for (Tmct0alc alc : alcsByOrden) {
      message = getMessageValidacionCuentaCompensacionPataMercado(alc);
      if (message != null) {
        marcarIncidenciaPataMercado(alc, message.mensaje);
        if (message.bloqueante) {
          return Collections.emptyList();
        }
      }
      desglosesAgrupados = agrupaDesgloses(alc);
      if (desglosesAgrupados == null) {
        LOG.warn("[resuelvePorOrden] No se ha regresado desgloses agrupados para Alc con Id {}", alc.getId());
        continue;
      }
      for (AgrupacionPataMercado apm : desglosesAgrupados.keySet()) {
        if (alcOrdenesExistentes.containsKey(apm)) {
          actualizaAlcOrdenes(alcOrdenesExistentes.get(apm), desglosesAgrupados.get(apm));
        }
        else {
          enLista = desglosesNuevos.get(apm);
          if (enLista == null) {
            enLista = new ArrayList<>();
            desglosesNuevos.put(apm, enLista);
          }
          enLista.addAll(desglosesAgrupados.get(apm));
        }
      }
    }
    LOG.debug("[resuelvePorOrden] Termina");
    return resuelveNuevosDesgloses(nuorden, desglosesNuevos);
  }

  @Transactional(propagation = Propagation.MANDATORY)
  private Map<AgrupacionPataMercado, AlcOrdenes> agrupaAlcOrdenesExistentes(String nuorden)
      throws SIBBACBusinessException {
    final List<AlcOrdenes> alcOrdenes;
    final Map<AgrupacionPataMercado, AlcOrdenes> agrupados;
    AlcOrdenMercado ordenMercado;
    AgrupacionPataMercado agrupacion;
    String codigoS3, mensaje;
    Tmct0desglosecamara desglose;

    alcOrdenes = alcOrdenesBo.findPatasMercadoPorOrden(nuorden);
    if (alcOrdenes.isEmpty()) {
      return Collections.emptyMap();
    }
    agrupados = new HashMap<>();
    for (AlcOrdenes ao : alcOrdenes) {
      if (ao.getOrdenesMercado().isEmpty()) {
        mensaje = MessageFormat.format("El Alc Orden con id {0} no contiene ordenes mercado", ao.getId());
        throw new SIBBACBusinessException(mensaje);
      }
      ordenMercado = ao.getOrdenesMercado().get(0);
      desglose = ordenMercado.getDesgloseCamara();
      if (desglose == null) {
        mensaje = MessageFormat.format("La orden mercado {0} apunta a un desglose nulo", ordenMercado.getId());
        throw new SIBBACBusinessException(mensaje);
      }
      codigoS3 = infiereCodigoS3(desglose);
      if (codigoS3 == null) {
        mensaje = MessageFormat.format("No se puede inferir codigoS3 para el desglose {0}",
            desglose.getIddesglosecamara());
        throw new SIBBACBusinessException(mensaje);
      }
      agrupacion = new AgrupacionPataMercado(desglose, codigoS3);
      if (agrupados.containsKey(agrupacion)) {
        mensaje = MessageFormat.format("Más de un alc orden para la agrupacion {0}", agrupacion);
        throw new SIBBACBusinessException(mensaje);
      }
      agrupados.put(agrupacion, ao);
    }
    return agrupados;
  }

  @Transactional(propagation = Propagation.MANDATORY)
  private void actualizaAlcOrdenes(AlcOrdenes existente, List<Tmct0desglosecamara> desgloses)
      throws SIBBACBusinessException {
    final List<Tmct0desglosecamara> pendientesEnvio;
    int estadoEntrecId;

    LOG.debug("[actualizaAlcOrdenes] Inicio...");
    if (existente.conSolicitudAnulacionTotal()) {
      LOG.debug("[actualizaAlcOrdenes] Se solicita anulacion total de Alc orden con id {}.", existente.getId());
      existente.anulaTotalmente(estadosMap.get(CLEARING.ACEPTADA_ANULACION.getId()));
      alcOrdenesBo.save(existente);
    }
    else {
      LOG.debug("[actualizaAlcOrdenes] {} actualizacions al Alc orden con id {} ", desgloses.size(), existente.getId());
      pendientesEnvio = new ArrayList<>();
      for (Tmct0desglosecamara desglose : desgloses) {
        estadoEntrecId = desglose.getTmct0estadoByCdestadoentrec().getIdestado();
        if (estadoEntrecId == CLEARING.PDTE_ENVIAR_ANULACION_PARCIAL.getId()) {
          resuelveAnulacionParcial(existente, desglose);
        }
        else if (estadoEntrecId == CLEARING.PDTE_FICHERO.getId() || estadoEntrecId == CLEARING.RECHAZADO_FICHERO.getId()
            || estadoEntrecId == CLEARING.INCIDENCIA.getId()) {
          pendientesEnvio.add(desglose);
        }
      }
      if (!pendientesEnvio.isEmpty())
        resuelvePendientesEnviar(existente, pendientesEnvio);
    }
    LOG.debug("[actualizaAlcOrdenes] Fin.");
  }

  @Transactional(propagation = Propagation.MANDATORY)
  private void resuelveAnulacionParcial(AlcOrdenes alcOrdenes, Tmct0desglosecamara anulacion)
      throws SIBBACBusinessException {
    final List<Tmct0MovimientoCuentaAlias> movCuentaAlias;

    LOG.debug("[resuelveAnulacionParcial] Inicio...");
    try {
      if (alcOrdenes.correspondeAnulacion(anulacion)) {
        LOG.debug("[Se acepta la anulación del desglose camara {} en el alc orden {}]", anulacion.getIddesglosecamara(),
            alcOrdenes.getId());
        anulacion.setTmct0estadoByCdestadoentrec(estadosMap.get(CLEARING.ACEPTADA_ANULACION.getId()));
        tmct0desgloseCamaraBo.save(anulacion);
        movCuentaAlias = tmct0MovimientoCuentaAliasBo.findByRefMovimiento(alcOrdenes.getReferenciaS3());
        LOG.debug("[processAlcList] movCuentaAlias encontrados: " + movCuentaAlias.size());
        // Sólo debería haber uno
        if (!movCuentaAlias.isEmpty())
          tmct0MovimientoCuentaAliasBo.delete(movCuentaAlias.get(0));
      }
      else {
        registraIncidencia(anulacion, "Anulacion sin correspondencia con id: " + anulacion.getIddesglosecamara());
      }
      LOG.debug("[resuelveAnulacionParcial] Fin");
    }
    catch (AlcOrdenes.AnulacionSuperaAsignadosException ex) {
      LOG.warn("{} para desglose cámara con id {}", ex.getMessage(), anulacion.getIddesglosecamara());
      registraIncidencia(anulacion, ex.getMessage());
    }
  }

  @Transactional(propagation = Propagation.MANDATORY)
  private List<RegistroEnvioFicheroMegara> resuelvePendientesEnviar(AlcOrdenes alcOrdenes,
      List<Tmct0desglosecamara> pendientesEnvio) throws SIBBACBusinessException {
    final List<RegistroEnvioFicheroMegara> listaRegistroMegara;
    Map<String, OrdenesaReportar> ordenesaReportar;
    String codigoS3;

    LOG.debug("[resuelvePendientesEnviar] Inicio...");
    ordenesaReportar = new HashMap<>();
    for (Tmct0desglosecamara pendiente : pendientesEnvio) {
      if (pendiente.getTmct0estadoByCdestadoasig().getIdestado().equals(ASIGNACIONES.RECHAZADA)
          || pendiente.getTmct0movimientos().isEmpty())
        continue; // No se toma en cuenta.
      codigoS3 = infiereCodigoS3(pendiente);
      if (isCuentaValoresInvalida(codigoS3)) {
        registraIncidencia(pendiente,
            MessageFormat.format("No se ha encontrado cuenta de valores válida para la cuenta compensacion {0}",
                pendiente.getCuentaCompensacion()));
        continue;
      }
      if (alcOrdenes.yaEnviado(pendiente)) {
        registraIncidencia(pendiente,
            MessageFormat.format("El desglose cámara {0} ya había sido enviado", pendiente.getIddesglosecamara()));
        return Collections.emptyList();
      }
      try {
        if (alcOrdenes.aceptaPendienteEnvio(pendiente)) {
          pendiente.setTmct0estadoByCdestadoentrec(estadosMap.get(CLEARING.ENVIADO_FICHERO.getId()));
          tmct0desgloseCamaraBo.save(pendiente);
          alcOrdenesBo.save(alcOrdenes);
          if (!ordenesaReportar.containsKey(alcOrdenes.getNuorden()))
            ordenesaReportar.put(alcOrdenes.getNuorden(), new OrdenesaReportar(alcOrdenes, codigoS3));
          return Collections.emptyList();
        }
      }
      catch (AlcOrdenes.PendientesSuperaRestantesException ex) {
        LOG.warn("{} para desglose cámara con id {}", ex.getMessage(), pendiente.getIddesglosecamara());
        registraIncidencia(pendiente, ex.getMessage());
        return Collections.emptyList();
      }
      registraIncidencia(pendiente, "Pendiente sin correspondencia con id: " + pendiente.getIddesglosecamara());
    }
    listaRegistroMegara = new ArrayList<>();
    for (OrdenesaReportar ordenes : ordenesaReportar.values()) {
      listaRegistroMegara.add(registraEnvioDesdeDesglose(ordenes.alcOrdenes, ordenes.cuentaValores));
    }
    LOG.debug("[resuelvePendientesEnviar] Fin, retornando {} registros", listaRegistroMegara.size());
    return listaRegistroMegara;
  }

  /**
   * A partir de los Alcs que recibe crea tantos AlcOrdenes como combinaciones
   * de Cámara Compensación, Cuenta Compensación y Fecha de liquidación existan.
   * 
   * @param nuorden Número de Orden
   * @param alcLista Lista con los Alcs a procesar. Todos los Alcs pertenecen al
   * número de orden
   * @param estadoPendienteFactura Estado que se asignará a los nuevos
   * AlcOrdenes
   * @return Los registros megara a partir de los cuales se generará el fichero.
   */
  @Transactional(propagation = Propagation.MANDATORY)
  private List<RegistroEnvioFicheroMegara> resuelveNuevosDesgloses(String nuorden,
      final Map<AgrupacionPataMercado, List<Tmct0desglosecamara>> desglosesAgrupados) {
    final List<RegistroEnvioFicheroMegara> listaRegistrosEnvio;
    final Tmct0estado estadoSinFactura;
    final Set<String> bookingsEnviados;
    AlcOrdenes alcOrdenes;
    List<Tmct0desglosecamara> listaDesgloses;
    BigDecimal titulosEnviados;
    String codigoS3;

    LOG.debug("[resuelveNuevosDesgloses] Inicio...");
    estadoSinFactura = estadosMap.get(ALC_ORDENES.SIN_FACTURA.getId());
    listaRegistrosEnvio = new ArrayList<>();
    bookingsEnviados = new HashSet<>();
    for (AgrupacionPataMercado agrupacion : desglosesAgrupados.keySet()) {
      listaDesgloses = desglosesAgrupados.get(agrupacion);
      if (listaDesgloses.isEmpty())
        continue;
      codigoS3 = agrupacion.getCdCuentaS3();
      if (isCuentaValoresInvalida(codigoS3)) {
        String msg = MessageFormat
            .format("No se ha encontrado cuenta de valores válida para la cuenta compensacion {0}", agrupacion);
        LOG.warn("[resuelveNuevasAlcOrdenes] {}", msg);
        tmct0logBo.insertarRegistro(nuorden, "0", msg, null, "SIBBAC");
        continue;
      }
      titulosEnviados = BigDecimal.ZERO;
      for (Tmct0desglosecamara desglose : listaDesgloses) {
        titulosEnviados = titulosEnviados.add(desglose.getNutitulos());
        bookingsEnviados.add(desglose.getTmct0alc().getId().getNbooking());
      }
      alcOrdenes = new AlcOrdenes();
      alcOrdenes.setNuorden(nuorden);
      alcOrdenes.setNbooking("0");
      alcOrdenes.setNucnfliq((short) 0);
      alcOrdenes.setNucnfclt("0");
      alcOrdenes.setAuditDate(new Date());
      alcOrdenes.setAuditUser("SIBBAC");
      alcOrdenes.setEstado(estadoSinFactura);
      alcOrdenes.setNetting(null);
      alcOrdenes.setCuentaCompensacionDestino(agrupacion.getCdCtaCompensacion());
      alcOrdenes.setIdCamaraCompensacion(agrupacion.getIdCamaraCompensacion());
      alcOrdenes.setTitulosEnviados(titulosEnviados);
      alcOrdenes.setTitulosAsignados(titulosEnviados);
      alcOrdenes.setFeliquidacion(agrupacion.getFechaLiquidacion());
      alcOrdenes.setEstadoInstruccion('A');
      creaOrdenMercadoDesdeDesglose(alcOrdenes, listaDesgloses);
      alcOrdenesBo.save(alcOrdenes);
      listaRegistrosEnvio.add(registraEnvioDesdeDesglose(alcOrdenes, codigoS3));
    }
    alcOrdenesBo.flush();
    for (String nbooking : bookingsEnviados) {
      tmct0logBo.insertarRegistro(nuorden, nbooking, "Fichero S3 (Clearing). Enviada pata mercado", "", "");
    }
    LOG.debug("[resuelveNuevosDesgloses] Fin, devolviendo {} registros de envio", listaRegistrosEnvio.size());
    ;
    return listaRegistrosEnvio;
  }

  /**
   * Agrupa los desgloses de una alc por cuenta compensación, cuenta S3 y fecha
   * de liquidación.
   * @return un mapa con los desgloses agrupados según los criterios descritos.
   * Puede ser un mapa vacío. Nulo en caso de error.
   */
  @Transactional(propagation = Propagation.MANDATORY)
  private Map<AgrupacionPataMercado, List<Tmct0desglosecamara>> agrupaDesgloses(final Tmct0alc tmct0alc) {
    final Map<AgrupacionPataMercado, List<Tmct0desglosecamara>> desglosesPorCuenta;
    List<Tmct0desglosecamara> listaDesgloses;
    AgrupacionPataMercado agrupacion;
    String codigoS3;

    LOG.debug("[agrupaDesgloses] Inicio...");
    desglosesPorCuenta = new HashMap<>();
    for (Tmct0desglosecamara desglose : tmct0alc.getTmct0desglosecamaras()) {
      if (desglose.getTmct0estadoByCdestadoasig().equals(ASIGNACIONES.RECHAZADA)
          || desglose.getTmct0movimientos().isEmpty()) {
        continue; // No se toma en cuenta
      }
      codigoS3 = infiereCodigoS3(desglose);
      if (codigoS3 == null) {
        LOG.warn("[agrupaDesgloses] No hay un codigoS3 determinado para el desglose con Id {}",
            desglose.getIddesglosecamara());
        return null;
      }
      agrupacion = new AgrupacionPataMercado(desglose, codigoS3);
      if (desglosesPorCuenta.containsKey(agrupacion))
        listaDesgloses = desglosesPorCuenta.get(agrupacion);
      else {
        listaDesgloses = new ArrayList<>();
        desglosesPorCuenta.put(agrupacion, listaDesgloses);
      }
      listaDesgloses.add(desglose);
    }
    LOG.debug("[agrupaDesgloses] Se retornan {} agrupaciones para el Alc con Id [{}]", desglosesPorCuenta.size(),
        tmct0alc.getId());
    return desglosesPorCuenta;
  }

  private String infiereCodigoS3(Tmct0desglosecamara desglose) {
    final String codigoCuentaCompensacion;
    String codigoS3;

    codigoCuentaCompensacion = desglose.getCuentaCompensacion();
    codigoS3 = cuentasS3PorCodigo.get(codigoCuentaCompensacion);
    if (codigoS3 == null) {
      codigoS3 = tmct0CuentasDeCompensacionBo.findCodigoS3ByCodigo(codigoCuentaCompensacion);
      if (codigoS3 != null) {
        cuentasS3PorCodigo.put(codigoCuentaCompensacion, codigoS3);
      }
    }
    if (codigoS3 == null && cuentasTerceros.contains(codigoCuentaCompensacion)) {
      codigoS3 = desglose.getTmct0alc().getAcctatcle();
    }
    return codigoS3;
  }

  private ValidacionPataMercado getMessageValidacionCuentaCompensacionPataMercado(Tmct0alc tmct0alc) {
    final List<Long> idsDesglosecamara = new ArrayList<>();
    final List<String> cuentasDc;
    String cuentaCompensacion;

    for (Tmct0desglosecamara dc : tmct0alc.getTmct0desglosecamaras()) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()
          && dc.getTmct0estadoByCdestadoentrec().getIdestado() >= EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()
          && dc.getTmct0estadoByCdestadoentrec().getIdestado() <= EstadosEnumerados.CLEARING.INCIDENCIA.getId()) {
        idsDesglosecamara.add(dc.getIddesglosecamara());
      }
    }
    if (idsDesglosecamara.isEmpty()) {
      LOG.debug("[getMessageValidacionCuentaCompensacionPataMercado] El Alc {} no tiene desgloses a enviar",
          tmct0alc.getId());
      return null;
    }
    cuentasDc = tmct0movimientoeccDao.findDistinctCuentaCompensacionByIdDesglosesCamara(idsDesglosecamara);
    if (cuentasDc.size() == 0 || cuentasDc.size() == 1 && StringUtils.isBlank(cuentasDc.get(0))) {
      return new ValidacionPataMercado("INCIDENCIA-No se ha encontrado cuenta de compensacion", false);
    }
    if (cuentasDc.size() > 1) {
      return new ValidacionPataMercado("INCIDENCIA-Distintas cuentas de compensacion para el mismo alc.", true);
    }
    for (Tmct0desglosecamara dc : tmct0alc.getTmct0desglosecamaras()) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()
          && dc.getTmct0estadoByCdestadoentrec().getIdestado() >= EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()
          && dc.getTmct0estadoByCdestadoentrec().getIdestado() <= EstadosEnumerados.CLEARING.INCIDENCIA.getId()) {
        cuentaCompensacion = dc.getCuentaCompensacion();
        if (cuentaCompensacion != null && !cuentasDc.get(0).trim().equals(cuentaCompensacion.trim())) {
          return new ValidacionPataMercado(
              MessageFormat.format("INCIDENCIA-Distintas cuentas en movimientos: {0} e ic {1}.", cuentasDc.get(0),
                  cuentaCompensacion),
              true);
        }
      }
    }
    return null;
  }

  private void marcarIncidenciaPataMercado(Tmct0alc tmct0alc, String message) {
    LOG.warn("[resuelveNuevasAlcOrdenes] {}", message);
    tmct0alc.setFhaudit(new Date());
    tmct0alc.setCdusuaud("ENV_C_N");
    tmct0alcBo.save(tmct0alc);
    for (Tmct0desglosecamara dc : tmct0alc.getTmct0desglosecamaras()) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
        dc.setTmct0estadoByCdestadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.INCIDENCIA.getId()));
        dc.setAuditFechaCambio(new Date());
        dc.setAuditUser("ENV_C_N");
      }
    }
    tmct0desgloseCamaraBo.save(tmct0alc.getTmct0desglosecamaras());
    tmct0alcBo.save(tmct0alc);
    tmct0logBo.insertarRegistro(tmct0alc.getId().getNuorden(), "0", message, null, "SIBBAC");
  }

  @Transactional(propagation = Propagation.REQUIRED)
  private RegistroEnvioFicheroMegara registraEnvioDesdeDesglose(final AlcOrdenes ordenes, final String cuentaS3) {
    final RegistroEnvioFicheroMegara registroMegaraMercado;
    final Tmct0MovimientoCuentaAlias movimientoCuentaAlias;
    final Tmct0alc tmct0alc;
    final Tmct0alo tmct0alo;
    final BigDecimal efectivoTotalMercado, precioMercado;
    final String alias, camara;
    final boolean isCompra;

    LOG.debug("[registraEnvioDesdeDesglose] Inicio...");
    camara = tmct0CamaraCompensacionBo.findById(ordenes.getIdCamaraCompensacion()).getCdCodigo();
    tmct0alc = ordenes.getAlcdeReferencia(); // Paso muy dudoso en su validez
    tmct0alo = tmct0alc.getTmct0alo();
    movimientoCuentaAlias = new Tmct0MovimientoCuentaAlias();
    registroMegaraMercado = new RegistroEnvioFicheroMegara();
    registroMegaraMercado.setCuentaS3(cuentaS3);
    efectivoTotalMercado = tmct0desgloseclititDao.sumByAlcOrdenes(ordenes.getId());
    LOG.debug("[registroEnvioDesdeDesglose] Efectivo acumulado: " + efectivoTotalMercado);
    if (efectivoTotalMercado == null || efectivoTotalMercado.compareTo(BigDecimal.ZERO) <= 0)
      return null;
    alias = tmct0alo.getTmct0bok().getCdalias().trim();
    isCompra = tmct0alo.getCdtpoper().toString().equals(Constantes.COMPRA);
    if (isCompra) {
      registroMegaraMercado.setCdtpoper(configParams.get("compra_bmeecc"));
      movimientoCuentaAlias.setCdoperacion(configParams.get("compra_mercado_mov"));
      movimientoCuentaAlias.setSignoanotacion('E');
      movimientoCuentaAlias.setTitulos(ordenes.getTitulosAsignados());
      movimientoCuentaAlias.setImefectivo(efectivoTotalMercado.negate());
    }
    else {
      registroMegaraMercado.setCdtpoper(configParams.get("venta_bmeecc"));
      movimientoCuentaAlias.setCdoperacion(configParams.get("venta_mercado_mov"));
      movimientoCuentaAlias.setSignoanotacion('S');
      movimientoCuentaAlias.setTitulos(ordenes.getTitulosAsignados().negate());
      movimientoCuentaAlias.setImefectivo(efectivoTotalMercado);
    } // else
    registroMegaraMercado.setNureftraDato(String.format("%014d", ordenes.getId()));
    registroMegaraMercado.setNureftraTipo(CLEARING_TIPO.MERCADO.getTipo() + CLEARING_TIPO.CLIENTE_NETTING.getTipo());
    registroMegaraMercado.setCdisinxx(tmct0alo.getCdisin());
    registroMegaraMercado.setTxdescri(tmct0alo.getNbvalors());
    registroMegaraMercado.setFhcontra(tmct0alc.getFeejeliq());
    registroMegaraMercado.setFhvalcon(tmct0alc.getFeopeliq());
    registroMegaraMercado.setFhteoliq(tmct0alc.getFeopeliq());
    registroMegaraMercado.setNutitulo(ordenes.getTitulosAsignados());
    registroMegaraMercado.setCddivisa(tmct0alo.getCdmoniso());
    // Se calcula el precio EFECTIVO / TITULOS (redondeado a 10 decimales)
    LOG.debug("[envioOperacionesMercado] desgloseCamara.getNutitulos(): " + ordenes.getTitulosAsignados());
    precioMercado = efectivoTotalMercado.divide(ordenes.getTitulosAsignados(), 2, RoundingMode.HALF_UP);
    registroMegaraMercado.setImcpreci(precioMercado);
    registroMegaraMercado.setImefecti(efectivoTotalMercado);
    registroMegaraMercado.setImpornet(efectivoTotalMercado);
    registroMegaraMercado.setCdclient(tmct0alc.getCdreftit());
    registroMegaraMercado.setCdalias(alias);
    registroMegaraMercado.setCdcontra(camara);
    movimientoCuentaAlias
        .setRefmovimiento(registroMegaraMercado.getNureftraTipo().concat(registroMegaraMercado.getNureftraDato()));
    movimientoCuentaAlias.setCdaliass(alias);
    movimientoCuentaAlias.setIsin(tmct0alo.getCdisin());
    movimientoCuentaAlias.setMercado(camara);
    movimientoCuentaAlias.setTradedate(tmct0alc.getFeejeliq());
    movimientoCuentaAlias.setGuardadosaldo(false);
    movimientoCuentaAlias.setCdcodigocuentaliq(ordenes.getCuentaCompensacionDestino());
    movimientoCuentaAlias.setCorretaje(BigDecimal.ZERO);
    tmct0MovimientoCuentaAliasBo.save(movimientoCuentaAlias);
    LOG.debug("[registraEnvioDesdeDesglose] Fin");
    return registroMegaraMercado;
  }

  @Transactional(propagation = Propagation.REQUIRED)
  private void creaOrdenMercadoDesdeDesglose(AlcOrdenes ordenes, List<Tmct0desglosecamara> listaDesgloses) {
    final Tmct0estado estadoEnviadoFichero;
    Tmct0alcId alcId;
    AlcOrdenMercado ordenMercado;

    LOG.debug("[creaOrdenMercadoDesdeDesglose] Inicio...");
    estadoEnviadoFichero = estadosMap.get(CLEARING.ENVIADO_FICHERO.getId());
    for (Tmct0desglosecamara desglose : listaDesgloses) {
      alcId = desglose.getTmct0alc().getId();
      ordenMercado = new AlcOrdenMercado();
      ordenMercado.setAlcOrdenes(ordenes);
      ordenMercado.setNuOrden(alcId.getNuorden());
      ordenMercado.setnBooking(alcId.getNbooking());
      ordenMercado.setNuCnfClt(alcId.getNucnfclt());
      ordenMercado.setNuCnfLiq(alcId.getNucnfliq());
      ordenMercado.setDesgloseCamara(desglose);
      ordenMercado.setTitulos(desglose.getNutitulos());
      ordenMercado.setEstado('A');
      ordenMercado.setAuditDate(new Date());
      ordenMercado.setAuditUser("SIBBAC");
      ordenes.getOrdenesMercado().add(ordenMercado);
      desglose.setTmct0estadoByCdestadoentrec(estadoEnviadoFichero);
      tmct0desgloseCamaraBo.save(desglose);
    }
    LOG.debug("[creaOrdenMercadoDesdeDesglose] Fin");
  }

  @Transactional(propagation = Propagation.REQUIRED)
  private void registraIncidencia(Tmct0desglosecamara desgloseEnIncidencia, String mensaje)
      throws SIBBACBusinessException {
    final Tmct0estado estadoIncidencia;

    estadoIncidencia = estadosMap.get(CLEARING.INCIDENCIA.getId());
    desgloseEnIncidencia.setTmct0estadoByCdestadoentrec(estadoIncidencia);
    tmct0desgloseCamaraBo.save(desgloseEnIncidencia);
    tmct0logBo.insertarRegistro(desgloseEnIncidencia.getTmct0alc(), new Date(), new Date(), null, estadoIncidencia,
        mensaje);
  }

  private static class OrdenesaReportar {

    private final AlcOrdenes alcOrdenes;

    private final String cuentaValores;

    private OrdenesaReportar(AlcOrdenes alcOrdenes, String cuentaValores) {
      this.alcOrdenes = alcOrdenes;
      this.cuentaValores = cuentaValores;
    }

  }

  private class ValidacionPataMercado {

    final String mensaje;

    final boolean bloqueante;

    private ValidacionPataMercado(String mensaje, boolean bloqueante) {
      this.mensaje = mensaje;
      this.bloqueante = bloqueante;
    }

  }
}
