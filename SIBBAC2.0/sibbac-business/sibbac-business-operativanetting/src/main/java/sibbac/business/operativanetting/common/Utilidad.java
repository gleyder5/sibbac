package sibbac.business.operativanetting.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Utilidad {

  // para distingir el tipo de clearing
  public enum CLEARING_TIPO {
    /**/
    CLIENTE_NETTING("N"),
    /**/
    CLIENTE("C"),
    /**/
    MERCADO("M");

    private String valor;

    private CLEARING_TIPO(String valor) {
      this.valor = valor;

    }

    public String getTipo() {
      return valor;
    }

  }

  /**
   * Clase de Enumerados que contiene las claves necesarias para obtener registros de la tabla tmct0cfg.
   * */
  public static enum DatosConfig {

    MEGARA_PARTENON_FOLDER_partenon_output("MEGARA_PARTENON_FOLDER", "partenon_output"),
    MEGARA_PARTENON_FOLDER_partenon_input("MEGARA_PARTENON_FOLDER", "partenon_input"),
    MEGARA_PARTENON_FOLDER_megara_input("MEGARA_PARTENON_FOLDER", "megara_input"),
    MEGARA_PARTENON_FOLDER_megara_output("MEGARA_PARTENON_FOLDER", "megara_output"),
    MEGARA_PARTENON_FOLDER_megara_input_tratados("MEGARA_PARTENON_FOLDER", "megara_input_tratados"),
    MEGARA_PARTENON_FOLDER_megara_output_tratados("MEGARA_PARTENON_FOLDER", "megara_output_tratados"),
    MEGARA_PARTENON_FOLDER_partenon_input_tratados("MEGARA_PARTENON_FOLDER", "partenon_input_tratados"),
    MEGARA_PARTENON_FOLDER_partenon_output_tratados("MEGARA_PARTENON_FOLDER", "partenon_output_tratados"),
    MEGARA_PARTENON_FOLDER_IN_FISCAL_DATA("MEGARA_PARTENON_FOLDER", "partenon.fiscal.data.in"),
    // MEGARA_PREVIOUS_DAYS_SINCE_SENTS_IN_CURRENT_FILE("MEGARA_NACIONAL",
    // "megara.previous.days.since.sents.in.current.file"),
    MEGARA_SENT_DAYS("MEGARA_NACIONAL", "sent.days"),
    MEGARA_SENT_DAY_LIMIT_HOUR("MEGARA_NACIONAL", "sent.day.limit.hour"),
    MEGARA_AUTOUPDATE_FLAGLIQ("MEGARA_NACIONAL", "megara.autoupdate.flagliq"),
    MEGARA_NACIONAL_begin_hour_megara_nacional("MEGARA_NACIONAL", "begin_hour_megara_nacional"),
    MEGARA_NACIONAL_end_hour_megara_nacional("MEGARA_NACIONAL", "end_hour_megara_nacional"),
    PTI_pti_idecc("PTI", "pti.idecc"),
    SVB_cif("SVB", "svb.cif");

    private String process, keyname;

    private DatosConfig(String _process, String _keyname) {
      process = _process;
      keyname = _keyname;
    }

    public String getProcess() {
      return process.trim();
    }

    public String getKeyname() {
      return keyname.trim();
    }

    @Override
    public String toString() {
      return "[process==" + this.process + " keyname= " + this.keyname + "]";
    }

  }

  public static final Date getPreviousWorkDate(Date dateIni, int daysPrevious) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(dateIni);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    for (int i = 0; i < daysPrevious; i++) {
      cal.add(Calendar.DAY_OF_YEAR, -1);
      while (isWeekEnd(new Date(cal.getTimeInMillis()))) {
        cal.add(Calendar.DAY_OF_YEAR, -1);
      }
    }

    return cal.getTime();
  }

  public static final boolean isWeekEnd(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
      return true;
    } else {
      return false;
    }
  }

  public static final String fillDate(Date valor, String format) {
    if (valor == null) {
      char[] fecha = new char[format.length()];
      Arrays.fill(fecha, ' ');
      return new String(fecha);
    }
    try {
      String valorFmt = new SimpleDateFormat(format).format(valor);
      return valorFmt;
    } catch (Exception e) {
      System.err.println("Caught Exception: " + e.getMessage());
      return null;
    }

  }

  public static final Date ParserDate(String fecha, SimpleDateFormat formato) {

    try {

      Date date = formato.parse(fecha);
      return date;

    } catch (ParseException e) {
      System.err.println("Caught ParseException: " + e.getMessage());
      return null;
    }

  }

  public static Date getFechaFormateada(String fecha) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date fecha_result;
    try {
      fecha_result = (Date) formatter.parse(fecha);
      return fecha_result;
    } catch (ParseException e) {
      System.err.println("Caught ParseException: " + e.getMessage());
      return null;
    }
  }

}
