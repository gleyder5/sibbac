package sibbac.business.operativanetting.database.bo;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.common.RegistroRespuestaCabeceraFicheroMegara;
import sibbac.business.operativanetting.common.RespuestaClearingNetting;
import sibbac.business.operativanetting.common.Utilidad.CLEARING_TIPO;
import sibbac.business.operativanetting.exceptions.RecepcionFicheroMegaraException;
import sibbac.business.wrappers.database.dao.S3LiquidacionDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;

/**
 * Procesa los ficheros de respuesta de Megara.
 * 
 * @author XI316153
 */
@Service
public class RespuestaClearingNettingBo extends AbstractBo<Tmct0alc, Tmct0alcId, Tmct0alcDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(RespuestaClearingNettingBo.class);

  private static final String TIPO_OPERACION_COUI = "COUI";
  private static final String TIPO_OPERACION_CAUI = "CAUI";
  private static final String TIPO_OPERACION_COLLC = "COLLC";
  private static final String TIPO_OPERACION_COLLP = "COLLP";

  private static final String mensajeReferenciaCanceladaMercado = "Posible desglose duplicado en el fichero de S3 (Pata Mercado): Referencia fichero: '%s' / Booking: '%s' / Titulos: '%s' / Titular: '%s'.";
  private static final String mensajeReferenciaCanceladaCliente = "Posible desglose duplicado en el fichero de S3 (Pata Cliente): Referencia fichero: '%s' / Booking: '%s' / Titulos: '%s' / Titular: '%s'.";
  private static final String mensajeReferenciaColateralNoEncontrada = "Referencia de colateral recibida en el fichero de S3 no encontrada en SIBBAC: Tipo de operación: '%s'; Referencia fichero: '%s'; Isin: '%s'; Titulos: '%s'; Efectivo: '%s'";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private S3LiquidacionDao s3LiquidacionDao;

  @Autowired
  private SendMail sendMail;

  @Value("${aviso.emails}")
  private String emails;

  @Value("${aviso.emails.alarma}")
  private String emailsAlarma;

  @Value("${aviso.subject}")
  private String subject;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  static boolean referenciaSVAceptable(String referenciaSV) {
    if (referenciaSV.length() != 16) {
      return false;
    }
    if (referenciaSV.startsWith(CLEARING_TIPO.CLIENTE.getTipo())
        || referenciaSV.startsWith(CLEARING_TIPO.CLIENTE_NETTING.getTipo())) {
      return NumberUtils.isDigits(referenciaSV.substring(1));
    }
    if (referenciaSV.startsWith(CLEARING_TIPO.MERCADO.getTipo())) {
      if (referenciaSV.charAt(1) == 'N') {
        return NumberUtils.isDigits(referenciaSV.substring(2));
      }
      return NumberUtils.isDigits(referenciaSV.substring(1));
    }
    return false;
  }

  /**
   * 
   * @throws IOException
   */
  @Transactional
  public Calendar ejecutarRespuestaFichero(File file2Process, File fileRejected, HashMap<String, String> configParams,
      HashMap<String, Tmct0estado> estadosMap, List<String> tiposOperacion) throws RecepcionFicheroMegaraException {
    LOG.debug("[ejecutarRespuestaFichero] Inicio ...");
    RespuestaClearingNetting respuestaClearingNetting;
    StringBuffer body = null;
    String linea = null;
    String[] nombreFicheroSplit = null;
    String fechaFichero = null;
    String horaFichero = null;
    File workingFile = null;
    RegistroRespuestaCabeceraFicheroMegara registro = null;
    List<RegistroRespuestaCabeceraFicheroMegara> registrosList = null;
    List<RegistroRespuestaCabeceraFicheroMegara> registrosSubList = null;
    List<Long> idS3Liquidacion = null;
    List<String> registrosNoProcesables = new ArrayList<String>();
    Calendar calFichero = Calendar.getInstance();
    Date hoy = new Date();
    boolean hayError = false;

    respuestaClearingNetting = ctx.getBean(RespuestaClearingNetting.class);
    respuestaClearingNetting.init(configParams, estadosMap);
    try {
      if (file2Process.getName().contains("NEW")) {
        LOG.debug("[ejecutarRespuestaFichero] Tratando fichero ...");

        try {
          nombreFicheroSplit = file2Process.getName().split("_");
          fechaFichero = nombreFicheroSplit[4];
          // En principio el fichero vendrá sin extensión, pero para curarnos en
          // salud ...
          horaFichero = nombreFicheroSplit[5].split("\\.")[0];

          calFichero.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.ES_DATE_FORMAT));
          calFichero.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaFichero.substring(0, 2)));
          calFichero.set(Calendar.MINUTE, Integer.valueOf(horaFichero.substring(2, 4)));
          calFichero.set(Calendar.SECOND, Integer.valueOf(horaFichero.substring(4, 6)));
          calFichero.set(Calendar.MILLISECOND, Integer.valueOf("000"));

          LOG.debug("[ejecutarRespuestaFichero] Fecha de control: '{}'", calFichero.getTime());
        }
        catch (Exception e) {
          throw new RecepcionFicheroMegaraException(
              "Incidencia obteniendo fecha de control del fichero ... " + e.getMessage());
        } // catch

        registrosList = new ArrayList<>();
        idS3Liquidacion = new ArrayList<>();
        try {
          workingFile = renameWorkingFile(file2Process);
          try (FileReader fr = new FileReader(workingFile); BufferedReader br = new BufferedReader(fr)) {
            while ((linea = br.readLine()) != null) {
              LOG.debug("[ejecutarRespuestaFichero] Leyendo la linea '{}'", linea);
              try {
                registro = new RegistroRespuestaCabeceraFicheroMegara(linea);
                if (referenciaSVAceptable(registro.getReferenciaSV())) {
                  LOG.debug("[ejecutarRespuestaFichero] La referencia {} es aceptable.", registro.getReferenciaSV());
                  if (tiposOperacion.contains(registro.getTipoOperacion())) {
                    idS3Liquidacion = s3LiquidacionDao.findIdByFields(calFichero.getTime(), registro.getReferenciaSV(),
                        registro.getEstadoOperacion(), registro.getFechaOperacion(), registro.getFechaValor(),
                        registro.getTitulosLiquidados(), registro.getImporteNetoLiquidado());
                    if (idS3Liquidacion.isEmpty()) {
                      registrosList.add(registro);
                    }
                    else {
                      LOG.debug("[ejecutarRespuestaFichero] El registro ya ha sido procesado [{}] ... Se descarta ...",
                          idS3Liquidacion.size());
                    } // else
                  }
                  else {
                    registrosNoProcesables.add(linea);
                  } // else
                }
                else if (registro.getTipoOperacion().compareTo(TIPO_OPERACION_COUI) == 0
                    || registro.getTipoOperacion().compareTo(TIPO_OPERACION_CAUI) == 0
                    || registro.getTipoOperacion().compareTo(TIPO_OPERACION_COLLC) == 0
                    || registro.getTipoOperacion().compareTo(TIPO_OPERACION_COLLP) == 0) {
                  idS3Liquidacion = s3LiquidacionDao.findIdByFields(calFichero.getTime(), registro.getReferenciaSV(),
                      registro.getEstadoOperacion(), registro.getFechaOperacion(), registro.getFechaValor(),
                      registro.getTitulosLiquidados(), registro.getImporteNetoLiquidado());
                  if (idS3Liquidacion.isEmpty()) {
                    registrosList.add(registro);
                  }
                  else {
                    LOG.debug("[ejecutarRespuestaFichero] El registro ya ha sido procesado [{}] ... Se descarta ...",
                        idS3Liquidacion.size());
                  } // else
                }
                else {
                  LOG.warn("[ejecutarRespuestaFichero] Línea rechazada: {}", linea);
                  registrosNoProcesables.add(linea);
                }
              }
              catch (Exception e) {
                registrosNoProcesables.add(linea);
                LOG.warn("Incidencia leyendo la linea:" + linea, e);
              }
            } // while ((linea = br.readLine()) != null)
          }
          LOG.debug("[ejecutarRespuestaFichero] Fichero leido. Procesando lineas ...");
          registraLineasRechazadas(registrosNoProcesables.iterator(), fileRejected);

          int size = registrosList.size();
          for (int i = 0; i <= size; i += 100) {
            try {
              registrosSubList = registrosList.subList(i, Math.min(i + 100, size));
              respuestaClearingNetting.procesarRespuestas(registrosSubList, calFichero.getTime());
              if (respuestaClearingNetting.hayLineasRechazadas()) {
                registraLineasRechazadas(respuestaClearingNetting.getLineasRechazadas(), fileRejected);
              }
            }
            catch (Exception e) {
              LOG.warn("[ejecutarRespuestaFichero] Incidencia procesando registro ... ", e);
              if (registrosSubList.size() > 1) {
                hayError = executeAfterBlockException(respuestaClearingNetting, registrosSubList, calFichero.getTime());
              }
              else {
                // Por si acaso la última sublista es de un elemento y no ha
                // habido errores en las anteriores
                hayError = true;
              } // else
            } // catch
          } // for (int i = 0; true; i += 100)
        }
        catch (FileNotFoundException fnfe) {
          LOG.warn("[ejecutarRespuestaFichero] Fichero no encontrado: '{}'", file2Process);
        } // catch
      }
      else {
        LOG.debug(
            "[ejecutarRespuestaFichero] El fichero no contiene la cadena 'NEW'. No se trata. Moviendo a tratados ...");
        workingFile = file2Process;
      } // else

      try {
        if (respuestaClearingNetting.getReferenciasCanceladas().size() > 0
            || respuestaClearingNetting.getReferenciasNoEncontradas().size() > 0) {
          LOG.debug(
              "[ejecutarRespuestaFichero] Preparando email de registros con referencias canceladas y/o no encontradas ...");
          body = new StringBuffer();

          body.append(System.getProperty("line.separator"));
          body.append(System.getProperty("line.separator"));
          body.append("Fichero: " + file2Process.getName());
          body.append(System.getProperty("line.separator"));
          body.append("Hora: " + hoy);

          if (respuestaClearingNetting.getReferenciasCanceladas().size() > 0) {
            body.append(System.getProperty("line.separator"));
            body.append(System.getProperty("line.separator"));
            body.append("Se han recibido de S3 las siguientes referencias de ordenes canceladas en SIBBAC");
            body.append(System.getProperty("line.separator"));
            body.append(System.getProperty("line.separator"));

            for (Map.Entry<String, List<String>> referencia : respuestaClearingNetting.getReferenciasCanceladas()
                .entrySet()) {
              for (String alcTemp : referencia.getValue()) {
                if (referencia.getKey().charAt(0) == 'M') {
                  body.append(String.format(mensajeReferenciaCanceladaMercado, referencia.getKey(),
                      alcTemp.split("#")[0], alcTemp.split("#")[1], alcTemp.split("#")[2]));
                }
                else {
                  body.append(String.format(mensajeReferenciaCanceladaCliente, referencia.getKey(),
                      alcTemp.split("#")[0], alcTemp.split("#")[1], alcTemp.split("#")[2]));
                }
                body.append(System.getProperty("line.separator"));
              } // for
            } // for
          } // if (referenciasCanceladas.size() > 0)

          if (respuestaClearingNetting.getReferenciasNoEncontradas().size() > 0) {
            body.append(System.getProperty("line.separator"));
            body.append(System.getProperty("line.separator"));
            body.append("Se han recibido de S3 las siguientes referencias de ordenes no encontradas en SIBBAC");
            body.append(System.getProperty("line.separator"));
            body.append(System.getProperty("line.separator"));

            for (String referenciaNoEncontrada : respuestaClearingNetting.getReferenciasNoEncontradas()) {
              // body.append(String.format(mensajeReferenciaNoEncontrada,
              // referenciaNoEncontrada));
              body.append(referenciaNoEncontrada);
              body.append(System.getProperty("line.separator"));
            } // for
          } // if (referenciasNoEncontradas.size() > 0)

          LOG.debug("[ejecutarRespuestaFichero] Enviando email ...");
          LOG.debug("[ejecutarRespuestaFichero] Body: " + body.toString());
          sendMail.sendMail(Arrays.asList(emailsAlarma.split(";")), subject, body.toString());
          LOG.debug("[ejecutarRespuestaFichero] Email enviado ...");
        } // if (referenciasCanceladas.size() > 0 ||
          // referenciasNoEncontradas.size() > 0)
      }
      catch (Exception e) {
        LOG.warn("[ejecutarRespuestaFichero] Incidencia al enviar email de alarmas ... " + e.getMessage());
      } // catch

      try {
        if (respuestaClearingNetting.getColateralesNoencontrados().size() > 0) {
          LOG.debug("[ejecutarRespuestaFichero] Preparando email de registros de colateral no encontrados ...");
          body = new StringBuffer();
          body.append("Se han encontrado los siguientes registros de colateral no encontrados recibidos de Megara:");
          body.append(System.getProperty("line.separator"));
          body.append("Fichero: " + file2Process.getName());
          body.append(System.getProperty("line.separator"));
          body.append("Hora: " + hoy);
          body.append(System.getProperty("line.separator"));
          body.append(System.getProperty("line.separator"));
          for (RegistroRespuestaCabeceraFicheroMegara aviso : respuestaClearingNetting.getColateralesNoencontrados()) {
            body.append(
                String.format(mensajeReferenciaColateralNoEncontrada, aviso.getTipoOperacion(), aviso.getReferenciaSV(),
                    aviso.getIsin(), aviso.getTitulosLiquidados(), aviso.getImporteNetoLiquidado()));
            body.append(System.getProperty("line.separator"));
          } // for
          LOG.debug("[ejecutarRespuestaFichero] Enviando email ...");
          sendMail.sendMail(Arrays.asList(emails.split(";")), subject, body.toString());
          LOG.debug("[ejecutarRespuestaFichero] Email enviado ...");
        } // if (registrosNovalidos.size() > 0)
      }
      catch (Exception e) {
        LOG.warn("[ejecutarRespuestaFichero] Incidencia al enviar email ... " + e.getMessage());
      } // catch

      try {
        if (respuestaClearingNetting.getAlcAlarmasList().size() > 0) {
          LOG.debug("[ejecutarRespuestaFichero] Preparando email de alarmas ...");
          body = new StringBuffer();
          body.append("Se han generado las siguientes alarmas de Recepción Megara:");
          body.append(System.getProperty("line.separator"));
          body.append("Fichero: " + file2Process.getName());
          body.append(System.getProperty("line.separator"));
          body.append("Hora: " + hoy);
          body.append(System.getProperty("line.separator"));
          body.append(System.getProperty("line.separator"));
          for (String alarma : respuestaClearingNetting.getAlcAlarmasList()) {
            body.append(alarma);
            body.append(System.getProperty("line.separator"));
          } // for
          LOG.debug("[ejecutarRespuestaFichero] Enviando email ...");
          sendMail.sendMail(Arrays.asList(emails.split(";")), subject, body.toString());
          LOG.debug("[ejecutarRespuestaFichero] Email enviado ...");
        } // if (registrosNovalidos.size() > 0)
      }
      catch (Exception e) {
        LOG.warn("[ejecutarRespuestaFichero] Incidencia al enviar email ... " + e.getMessage());
      } // catch

      if (hayError) {
        throw new RecepcionFicheroMegaraException("Se han producido errores. Revise el log.");
      }
      else {
        moverFicheroTratado(workingFile, configParams);
        return calFichero;
      } // else
    }
    catch (Exception e) {
      LOG.warn("[ejecutarRespuestaFichero] Al procesar fichero de recepcion ", e);
      renameErrorFile(workingFile);
      throw new RecepcionFicheroMegaraException("[ejecutarRespuestaFichero] " + e.getMessage(), e);
    }
    finally {
      LOG.debug("[ejecutarRespuestaFichero] Fin ...");
    } // finally
  } // ejecutarRespuestaFichero

  private void registraLineasRechazadas(Iterator<String> lineasRechazadas, File fileRejected) {
    final boolean append;
    final File parent;

    append = fileRejected.exists();
    if (!append) {
      parent = fileRejected.getParentFile();
      if (!parent.exists() && !parent.mkdirs()) {
        LOG.warn(
            "[registraLineasRechazadas] El directorio de ficheros de lineas rechazadas {} ni existe ni se puede crear",
            parent.getPath());
        while (lineasRechazadas.hasNext()) {
          LOG.info(lineasRechazadas.next());
        }
      }
    }
    try (PrintWriter printer = new PrintWriter(new BufferedOutputStream(new FileOutputStream(fileRejected, append)))) {
      while (lineasRechazadas.hasNext()) {
        printer.println(lineasRechazadas.next());
      }
      return;
    }
    catch (FileNotFoundException e) {
      LOG.warn("[registraLineasRechazadas] El fichero {} se identifica como existente, pero se ha lanzado excepcion",
          fileRejected.getAbsolutePath(), e);
    }
    catch (RuntimeException rex) {
      LOG.error("[registraLineasRechazadas] Error inesperado al registrar lineas rechazadas", rex);
    }
    while (lineasRechazadas.hasNext()) {
      LOG.info(lineasRechazadas.next());
    }
  }

  /**
   * Metodo que ejecuta el case despues de haber ocurrido una excepcion en un
   * bloque, el nuevo bloque transaccional es de 1 en 1.
   */
  private boolean executeAfterBlockException(RespuestaClearingNetting respuestaClearingNetting,
      List<RegistroRespuestaCabeceraFicheroMegara> registrosList, Date fechaFichero) {
    LOG.debug("[executeAfterBlockException] Inicio ...");
    List<RegistroRespuestaCabeceraFicheroMegara> registrosSubListException = null;
    boolean hayError = false;

    for (int i = 0; i < registrosList.size(); i++) {
      try {
        registrosSubListException = registrosList.subList(i, i + 1);
        respuestaClearingNetting.procesarRespuestas(registrosSubListException, fechaFichero);
      }
      catch (Exception e) {
        hayError = true;
      } // catch
    } // for

    LOG.debug("[executeAfterBlockException] Fin ...");
    return hayError;
  } // executeAfterBlockException

  /**
   * Mueve el fichero especificado a la carpeta de ficheros tratados.
   * 
   * @param fichero Fichero a mover.
   * @throws IOException Si ocurre algún error al mover el fichero.
   */
  private void moverFicheroTratado(File fichero, HashMap<String, String> configParams) throws IOException {
    LOG.debug("[moverFicheroTratado] Inicio ...");
    Calendar now = Calendar.getInstance();

    String nuevoNombre = fichero.getName().split(Constantes.WORKING_FILE_SUFFIX)[0] + "_"
        + Constantes.DEST_FILE_DATE_FORMAT.format(now.getTime());
    File nuevoFichero = new File(configParams.get("inputFilePathTratado"), nuevoNombre);

    if (!fichero.renameTo(nuevoFichero)) {
      LOG.error("[moverFicheroTratado] Incidencia al mover el fichero renombrado a la ruta ["
          + configParams.get("inputFilePathTratado") + "]");
      throw new IOException("[moverFicheroTratado] Incidencia al mover el fichero renombrado a la ruta ["
          + configParams.get("inputFilePathTratado") + "]");
    } // if
    LOG.debug("[moverFicheroTratado] Fin ...");
  } // moverFicheroTratado

  /**
   * Renombra el fichero para indicar que se está trabajando con él.
   * 
   * @throws IOException Si ocurre algún error al renombrar el fichero.
   */
  private File renameWorkingFile(File fichero) throws IOException {
    LOG.debug("[renameWorkingFile] Inicio ...");
    if (!fichero.getName().contains(Constantes.WORKING_FILE_SUFFIX)) {
      File ficheroDeTrabajo = new File(fichero.getParent(), fichero.getName().concat(Constantes.WORKING_FILE_SUFFIX));
      if (!fichero.renameTo(ficheroDeTrabajo)) {
        throw new IOException(
            "Error al renombrar el fichero a " + fichero.getName().concat(Constantes.WORKING_FILE_SUFFIX));
      }
      else {
        return ficheroDeTrabajo;
      } // else
    }
    else {
      return fichero;
    } // else
  } // renameWorkingFile

  /**
   * Renombra el fichero para indicar que ha producido errores.
   */
  private void renameErrorFile(File fichero) {
    LOG.debug("[renameErrorFile] Inicio ...");
    if (fichero != null && !fichero.getName().contains(Constantes.ERROR_FILE_SUFFIX)) {
      LOG.debug("[renameErrorFile] Fin ... " + fichero.renameTo(new File(fichero.getParent(),
          fichero.getName().split(Constantes.WORKING_FILE_SUFFIX)[0].concat(Constantes.ERROR_FILE_SUFFIX))));
    }
    else {
      LOG.debug("[renameErrorFile] Fin ... ");
    } // else
  } // renameErrorFile

} // RespuestaClearingNettingBo
