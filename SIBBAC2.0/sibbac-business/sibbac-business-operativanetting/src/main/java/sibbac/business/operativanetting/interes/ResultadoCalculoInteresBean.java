package sibbac.business.operativanetting.interes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CalculoInteresBean.
 */
public class ResultadoCalculoInteresBean {
	
	/** The interes medio. */
	private final BigDecimal interesMedio;

	/** The importe total. */
	private final BigDecimal importeTotal;

	/** The importe. */
	private final BigDecimal importe;

	/** The importe interes. */
	private final BigDecimal importeInteres;

	/** The tipo base beans. */
	private List<TipoBaseBean> tipoBaseBeans;
	
	private final TipoBaseBean  tipoBaseBean;

	/**
	 * Instantiates a new resultado calculo interes bean.
	 *
	 * @param interesMedio the interes medio
	 * @param importeTotal the importe total
	 * @param importe the importe
	 * @param importeInteres the importe interes
	 * @param tipoBaseBeans the tipo base beans
	 */
	public ResultadoCalculoInteresBean(BigDecimal interesMedio,
			BigDecimal importeTotal, BigDecimal importe,
			BigDecimal importeInteres, List<TipoBaseBean> tipoBaseBeans,TipoBaseBean tipoBaseBean) {
		super();
		this.interesMedio = interesMedio;
		this.importeTotal = importeTotal;
		this.importe = importe;
		this.importeInteres = importeInteres;
		this.tipoBaseBeans = tipoBaseBeans;
		this.tipoBaseBean = tipoBaseBean;
	}
	
	
	
	public TipoBaseBean getTipoBaseBean() {
		return tipoBaseBean;
	}



	/**
	 * Adds the.
	 *
	 * @param tipobase the tipobase
	 * @return true, if successful
	 */
	public boolean add(TipoBaseBean tipobase){
		return getTipoBaseBeans().add(tipobase);
	}

	/**
	 * Gets the tipo base beans.
	 *
	 * @return the tipo base beans
	 */
	public List<TipoBaseBean> getTipoBaseBeans() {
		if( tipoBaseBeans == null ){
			tipoBaseBeans = new ArrayList<TipoBaseBean>();
		}
		return tipoBaseBeans;
	}

	/**
	 * Sets the tipo base beans.
	 *
	 * @param tipoBaseBeans the new tipo base beans
	 */
	public void setTipoBaseBeans(
			List<TipoBaseBean> tipoBaseBeans) {
		this.tipoBaseBeans = tipoBaseBeans;
	}

	/**
	 * Gets the interes medio.
	 *
	 * @return the interes medio
	 */
	public BigDecimal getInteresMedio() {
		return interesMedio;
	}

	/**
	 * Gets the importe total.
	 *
	 * @return the importe total
	 */
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	/**
	 * Gets the importe.
	 *
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * Gets the importe interes.
	 *
	 * @return the importe interes
	 */
	public BigDecimal getImporteInteres() {
		return importeInteres;
	}

}
