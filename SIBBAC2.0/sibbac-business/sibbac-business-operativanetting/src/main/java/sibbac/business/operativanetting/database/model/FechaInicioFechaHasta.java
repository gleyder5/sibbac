package sibbac.business.operativanetting.database.model;

import java.io.Serializable;
import java.util.Date;

import sibbac.business.wrappers.database.model.Alias;

// TODO: Auto-generated Javadoc
/**
 * The Interface FechaInicioFechaHasta.
 */
public interface FechaInicioFechaHasta extends Serializable {

	/**
	 * Gets the fecha inicio.
	 *
	 * @return the fecha inicio
	 */
	Date getFechaInicio();

	/**
	 * Gets the fecha hasta.
	 *
	 * @return the fecha hasta
	 */
	Date getFechaHasta();

	/**
	 * Gets the alias.
	 *
	 * @return the alias
	 */
	Alias getAlias();

}
