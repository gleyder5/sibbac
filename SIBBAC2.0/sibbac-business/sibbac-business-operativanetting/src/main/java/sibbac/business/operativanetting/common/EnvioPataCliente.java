package sibbac.business.operativanetting.common;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.AssertionFailure;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0cleNacionalDao;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ALC_ORDENES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0cleNacional;
import sibbac.business.operativanetting.common.Utilidad.CLEARING_TIPO;
import sibbac.business.operativanetting.exceptions.EnvioFicheroMegaraException;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.NettingBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0movimientoeccDao;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EnvioPataCliente extends EnvioClearingNetting {

  private static final Logger LOG = LoggerFactory.getLogger(EnvioPataCliente.class);

  @Autowired
  private NettingBo nettingBo;

  @Autowired
  private Tmct0cleNacionalDao tmct0cleDao;

  @Autowired
  private Tmct0afiDao tmct0afiDao;

  @Autowired
  private Tmct0alcBo tmct0alcBo;

  @Autowired
  private Tmct0logBo tmct0logBo;

  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  @Autowired
  private Tmct0movimientoeccDao tmct0movimientoeccDao;

  @Autowired
  private Tmct0CuentasDeCompensacionBo tmct0CuentasDeCompensacionBo;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private Tmct0estadoBo estadoBo;

  private final Map<String, List<String>> registrosAlarma;

  public EnvioPataCliente() {
    registrosAlarma = new HashMap<>();
  }

  @Override
  public void initProcess(Map<String, String> configParams) {
    initProcess(configParams, tmct0CuentasDeCompensacionBo, estadoBo);
  }

  public Map<String, List<String>> getRegistrosAlarma() {
    return registrosAlarma;
  }

  /**
   * Procesa la lista de registros recibidos de Megara.
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
  public int generaEnvio(Date fechaBusqueda, PrintWriter prtWriter) throws EnvioFicheroMegaraException {
    final List<Date> listaFechas;
    List<Tmct0alc> alcList;
    int conteoLineas = 0;

    LOG.debug("[generarEnvio] Inicio ... ");
    try {
      listaFechas = DateHelper.getListaFechas(fechaBusqueda, new Date());
    }
    catch(Exception ex) {
      throw new EnvioFicheroMegaraException(ex.getMessage());
    }
    for(Date f : listaFechas) {
      LOG.debug("[generarEnvio] consultado para fecha {}", f);
      alcList = tmct0alcBo.findByEnvioFicheroMegaraClienteTactico(f);
      if (!alcList.isEmpty()) {
        conteoLineas += processAlcList(alcList, prtWriter);
      }
    }
    return conteoLineas;
  } // generarEnvio

  private String getMessageValidacionCuentaCompensacionPataCliente(Tmct0alc tmct0alc) {
    final List<String> cuentasDc;

    cuentasDc = tmct0movimientoeccDao.findDistinctCuentaCompensacionByTmct0alcId(tmct0alc.getId());
    if (cuentasDc.size() == 0 || cuentasDc.size() == 1 && StringUtils.isBlank(cuentasDc.get(0))) {
      return "INCIDENCIA-Mo se ha encontrado cuenta de compensacion";
    }
    if (cuentasDc.size() > 1) {
      return "INCIDENCIA-Distintas cuentas de compensacion para el mismo alc.";
    }
    for (Tmct0desglosecamara dc : tmct0alc.getTmct0desglosecamaras()) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
        if (dc.getTmct0infocompensacion().getCdctacompensacion() != null
            && !cuentasDc.get(0).trim().equals(dc.getTmct0infocompensacion().getCdctacompensacion().trim())) {
          return MessageFormat.format("INCIDENCIA-Distintas cuentas en movimientos: {0} e ic {1}.", cuentasDc.get(0),
              dc.getTmct0infocompensacion().getCdctacompensacion());
        }
      }
    }
    return null;
  }

  /**
   * Procesa la lista de objetos Tmct0alc.
   * 
   * @param alcList Lista.
   * @return <code>java.lang.Integer - </code>Número de líneas escritas en el
   * fichero.
   */
  @Transactional
  private int processAlcList(List<Tmct0alc> alcList, PrintWriter prtWriter) {
    LOG.debug("[processAlcList] Inicio ...");
    Integer lineasFichero = new Integer(0);
    List<RegistroEnvioFicheroMegara> registrosMegaraClienteList = new ArrayList<RegistroEnvioFicheroMegara>();
    HashMap<Long, Netting> nettingsClienteMap = new HashMap<Long, Netting>();
    HashMap<Tmct0alcId, AlcOrdenes> alcOrdenesClienteMap = new HashMap<Tmct0alcId, AlcOrdenes>();
    String linea = null;
    Integer estadoEntrecTmct0alc = null;
    Tmct0bok tmct0bok;
    Tmct0alo tmct0alo;
    Tmct0ali tmct0ali;
    String alias;
    boolean isCompra = false;
    boolean isCuentaPropia;
    AlcOrdenes alcOrdenes;
    String referencia = null;
    List<String> tmpDcList;
    Date hoyDate = null;
    boolean isNetting;
    Character indNetting;

    for (Tmct0alc tmct0alc : alcList) {
      try {
        LOG.debug("[processAlcList] Procesando alc: " + tmct0alc.getId().toString());

        estadoEntrecTmct0alc = tmct0alc.getEstadoentrec().getIdestado();
        tmct0alo = tmct0alc.getTmct0alo();
        tmct0bok = tmct0alo.getTmct0bok();
        alias = tmct0bok.getCdalias().trim();
        tmct0ali = aliBo.findByCdaliass(alias, 99991231);
        if (tmct0ali == null) {
          LOG.debug("[processAlcList] no se ha encontrado alias con codigo {}. Se omite el alc {}", alias, tmct0alc);
          continue;
        }
        indNetting = tmct0ali.getIndNeting();
        if(indNetting == null) {
          LOG.warn("[processAlcList] El alc [{}] no se procesa porque el indNetting del bok es nulo", tmct0ali.getId());
          continue;
        }
        switch(indNetting.charValue()) {
          case 'S': isNetting = true; break;
          case 'N': isNetting = false; break;
          default:
            LOG.warn("[processAlcList] El alc [{}] no se procesa porque el indNetting del bok es [{}]", 
                tmct0ali.getId(), indNetting.charValue());
            continue;
        }
        if(tmct0alc.isCanonCalculoPendiente()) {
          tmct0logBo.insertarRegistro(tmct0alc.getNuorden(), tmct0alc.getNbooking(), tmct0alc.getNucnfclt(),
              "EL IMPORTE NETO NO ES EL DEFINITIVO, EL CANON TODAVÍA ESTÁ PENDIENTE DE CÁLCULO", 
              tmct0alc.getEstadoentrec().getNombre(), "SIBBAC20");
          continue;
        }
        isCompra = tmct0alo.getCdtpoper().toString().equals(Constantes.COMPRA);
        hoyDate = new Date();

        for (Tmct0alc tmct0alcTemp : tmct0alo.getTmct0alcs()) {

          LOG.trace("Se procesará el ALC: Nuorden: " + tmct0alc.getNuorden() + ", Nbooking: " + tmct0alc.getNbooking()
              + ", Nbtitliq: " + tmct0alc.getNbtitliq() + ", Nucnfclt: " + tmct0alc.getNucnfclt());

          LOG.debug("[processAlcList] BUSCANDO POSIBLE DUPLICADO, Revisando ALC: ({}) del ALO: ({} / {})",
              tmct0alcTemp.getNucnfliq(), tmct0alcTemp.getNbooking(), tmct0alcTemp.getNucnfclt());
          // Si alguno de los ALC está en estado de asignación
          // rechazado y ya se envió a Megara
          if (tmct0alcTemp.getCdestadoasig().compareTo(ASIGNACIONES.RECHAZADA.getId()) == 0
              && tmct0alcTemp.getEstadoentrec().getIdestado().compareTo(CLEARING.ENVIADO_FICHERO.getId()) >= 0) {
            // 1.- Se busca el ALC_ORDEN del ALC
            alcOrdenes = alcOrdenesBo.findByTmct0alc(tmct0alcTemp);
            if (alcOrdenes == null) {
              // 1.1.- No hay ALC_ORDEN. Podría ser de la cuenta
              // de la SV y sólo haberse enviado la pata mercado.
              // Habrá que buscar para cada movimiento cuenta
              // alias si refmovimiento tiene desgloses camara del
              // alc
              // que esten rechazadas y enviadas a Megara
              if (tmct0alcTemp.getCdniftit().trim().compareTo(configParams.get("cif_svb")) == 0) {
                referencia = "CARTERA PROPIA";
              }
              else {
                continue;
              }
            }
            else {
              if (alcOrdenes.getReferenciaS3() != null && !alcOrdenes.getReferenciaS3().isEmpty()) {
                referencia = alcOrdenes.getReferenciaS3();
              }
              else if (alcOrdenes.getNetting() != null) {
                referencia = Utilidad.CLEARING_TIPO.CLIENTE_NETTING.getTipo()
                    .concat(String.format("%015d", alcOrdenes.getNetting().getId()));
              } // else
            } // else

            if (registrosAlarma.containsKey(referencia)) {
              for (Tmct0desglosecamara dc : tmct0alcTemp.getTmct0desglosecamaras()) {
                if (dc.getTmct0estadoByCdestadoentrec().getIdestado()
                    .compareTo(CLEARING.ENVIADO_FICHERO.getId()) >= 0) {
                  registrosAlarma.get(referencia).add(String.format("%s%s%014d", CLEARING_TIPO.MERCADO.getTipo(),
                      CLEARING_TIPO.CLIENTE_NETTING.getTipo(), dc.getIddesglosecamara()));
                }
              }
            }
            else {
              tmpDcList = new ArrayList<String>();
              for (Tmct0desglosecamara dc : tmct0alcTemp.getTmct0desglosecamaras()) {
                if (dc.getTmct0estadoByCdestadoentrec().getIdestado()
                    .compareTo(CLEARING.ENVIADO_FICHERO.getId()) >= 0) {
                  tmpDcList.add(String.format("%s%s%014d", CLEARING_TIPO.MERCADO.getTipo(),
                      CLEARING_TIPO.CLIENTE_NETTING.getTipo(), alcOrdenes.getId()));
                } // if
              } // for
              registrosAlarma.put(String.format("#%s#%s#%s", tmct0alcTemp.getNbooking(),
                  tmct0alcTemp.getNutitliq().toString(), tmct0alcTemp.getCdordtit()), tmpDcList);
            } // else
          } // if
        } // for
        if (estadoEntrecTmct0alc.compareTo(CLEARING.ACEPTADA_ANULACION.getId()) != 0) {
          String msg = this.getMessageValidacionCuentaCompensacionPataCliente(tmct0alc);
          if (StringUtils.isNotBlank(msg)) {
            throw new SIBBACBusinessException(msg);
          }

          isCuentaPropia = envioOperacionesCliente(tmct0alc, registrosMegaraClienteList, nettingsClienteMap,
              alcOrdenesClienteMap, tmct0alo, alias, isCompra, estadoEntrecTmct0alc, isNetting);
          // 5.- Todo ha ido bien, se procede a salvar la info de la
          // pata cliente
          tmct0alc.setFhenvios3(hoyDate);
          if (isCuentaPropia) {
            LOG.debug("[processAlcList] Estableciendo estado CLEARING.ENVIADO_FICHERO_CUENTA_PROPIA al ALC");
            tmct0alc.setEstadoentrec(estadosMap.get(CLEARING.ENVIADO_FICHERO_CUENTA_PROPIA.getId()));
            tmct0alcBo.save(tmct0alc);
            try {
              tmct0logBo.insertarRegistro(tmct0alc, hoyDate, hoyDate, null,
                  CLEARING.ENVIADO_FICHERO_CUENTA_PROPIA.getId(),
                  "Fichero S3 (Clearing). No se envia la pata cliente porque el desglose es de cartera propia");
            }
            catch (Exception ex) {
              LOG.warn("[processAlcList] Error insertando pata cliente en Tmct0log ... ", ex.getMessage());
            } // catch
          }
          else {
            LOG.debug("[processAlcList] Estableciendo estado CLEARING.ENVIADO_FICHERO_CLIENTE al ALC");

            if (estadoEntrecTmct0alc.compareTo(CLEARING.PDTE_ENVIAR_ANULACION.getId()) == 0) {
              try {
                tmct0logBo.insertarRegistro(tmct0alc, hoyDate, hoyDate, null,
                    CLEARING.ENVIADO_FICHERO_ANULACION.getId(),
                    "Fichero S3 (Clearing). Envio de solicitud de baja a S3 de la pata cliente");
              }
              catch (Exception ex) {
                LOG.warn("[processAlcList] Error insertando pata cliente en Tmct0log ... ", ex.getMessage());
              } // catch

              tmct0alc.setEstadoentrec(estadosMap.get(CLEARING.ACEPTADA_ANULACION.getId()));
              try {
                tmct0logBo.insertarRegistro(tmct0alc, hoyDate, hoyDate, null, CLEARING.ACEPTADA_ANULACION.getId(),
                    "Fichero S3 (Clearing). Simulando aceptacion de baja de S3 de la pata cliente");
              }
              catch (Exception ex) {
                LOG.warn("[processAlcList] Error insertando pata cliente en Tmct0log ... ", ex.getMessage());
              } // catch

              AlcOrdenes alcOrdenesBaja;
              alcOrdenesBaja = alcOrdenesBo.findByTmct0alc(tmct0alc);

              Long idnetting = null;
              if (alcOrdenesBaja.getNetting() != null) {
                idnetting = alcOrdenesBaja.getNetting().getId();
              }

              if (alcOrdenesBaja != null) {
                LOG.info(
                    "[processAlcList] Marcando en estado baja la AlcOrdenes generada en el anterior envio ... '{}'",
                    alcOrdenesBaja.getReferenciaS3());
                alcOrdenesBaja.setEstadoInstruccion('B');
                alcOrdenesBo.save(alcOrdenesBaja);
              }

              if (idnetting != null) {
                LOG.info("[processAlcList] Eliminando Netting generado en el anterior envio ... '{}'", idnetting);
                nettingBo.delete(idnetting);
              } // if

            }
            else {
              tmct0alc.setEstadoentrec(estadosMap.get(CLEARING.ENVIADO_FICHERO.getId()));

              try {
                tmct0logBo.insertarRegistro(tmct0alc, hoyDate, hoyDate, null, CLEARING.ENVIADO_FICHERO.getId(),
                    "Fichero S3 (Clearing). Enviada pata cliente");
              }
              catch (Exception ex) {
                LOG.warn("[processAlcList] Error insertando en Tmct0log ... ", ex.getMessage());
              } // catch
            } // else
            tmct0alcBo.save(tmct0alc);

          } // else
        }
        else {
          LOG.warn("[processAlcList] El ALC se encuentra en estado CLEARING.ACEPTADA_ANULACION. No se procesa.");
          try {
            tmct0logBo.insertarRegistro(tmct0alc, hoyDate, hoyDate, null, CLEARING.ACEPTADA_ANULACION.getId(),
                "Fichero S3 (Clearing). No se envia por encontrarse en estado CLEARING.ACEPTADA_ANULACION");
          }
          catch (Exception ex) {
            LOG.warn("[processAlcList] Error insertando en Tmct0log ... ", ex.getMessage());
          } // catch
        } // else
      }
      catch (HibernateException | AssertionFailure he) {
        LOG.error("[processAlcList] Error fatal de hibernate ... " + he.getMessage());
        throw he;
      }
      catch (Exception e) {
        LOG.error("[processAlcList] " + e.getMessage(), e);
        try {
          tmct0logBo.insertarRegistro(tmct0alc, hoyDate, hoyDate, null, CLEARING.INCIDENCIA.getId(),
              "Fichero S3 (Clearing). " + e.getMessage());
        }
        catch (Exception ex) {
          LOG.warn("[processAlcList] Error insertando en Tmct0log ... ", ex.getMessage());
        } // catch
        tmct0alc.setEstadoentrec(estadosMap.get(CLEARING.INCIDENCIA.getId()));
        // Debe salvar también los desgloses camara
        tmct0alcBo.save(tmct0alc);
      }
    } // for (Tmct0alc tmct0alc : alcList)

    // Aqui hay que almacenar los registros de la pata cliente porque si
    // existe netting se han podido modificar después
    // de insertarlos en la lista, por eso no se pueden insertar en el
    // fichero en cada pasada del bucle for
    LOG.debug("[processAlcList] Escribiendo en fichero la pata cliente ...");

    // Tmct0MovimientoCuentaAlias nuevo_movimientoCuentaAlias = null;
    // String refOperacion = null;

    for (RegistroEnvioFicheroMegara registroCliente : registrosMegaraClienteList) {
      if (registroCliente.getCancelaciones() == null
          || (registroCliente.getCancelaciones() != null && "S".compareTo(registroCliente.getCancelaciones()) != 0)) {
        // Si los titulos han quedado a cero no hay que presentarlas
        if (!BigDecimal.ZERO.equals(registroCliente.getNutitulo())) {
          linea = "";
          for (RecordFile Registro : registroCliente.FormatearLineaFicheroRegistro01()) {
            linea += Registro.getResult();
          }
          prtWriter.print(lineSeparator);
          prtWriter.print(linea);
          lineasFichero++;
        } // if (!BigDecimal.ZERO.equals(registroCliente.getNutitulo()))
      }
    } // for (RegistroEnvioFicheroMegara registroCliente :
      // registrosMegaraClienteList)

    LOG.debug("[processAlcList] Almacenando nettings ...");
    for (Map.Entry<Long, Netting> netting : nettingsClienteMap.entrySet()) {
      nettingBo.save(netting.getValue());
    } // for

    LOG.debug("[processAlcList] Almacenando alcOrdenes ...");
    for (Map.Entry<Tmct0alcId, AlcOrdenes> alcOrden : alcOrdenesClienteMap.entrySet()) {
      alcOrdenesBo.save(alcOrden.getValue());
    } // for

    LOG.debug("[processAlcList] Numero de lineas escritas en el buffer: " + lineasFichero);

    return lineasFichero;
  } // processAlcList

  /**
   * Envia la operación a cliente.
   * 
   * @param lista_liquidacion_alc
   * @return verdadero si no es cuenta propia y se genera el registro para envio
   * megara. Falso en caso contrario
   * @throws Exception
   */
  @Transactional
  private boolean envioOperacionesCliente(Tmct0alc tmct0alc,
      List<RegistroEnvioFicheroMegara> registrosMegaraClienteList, HashMap<Long, Netting> nettingsClienteMap,
      HashMap<Tmct0alcId, AlcOrdenes> alcOrdenesClienteMap, Tmct0alo tmct0alo, String alias, boolean isCompra,
      Integer estadoEntrec, boolean isNetting) throws Exception {
    final Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion;
    final Tmct0TipoCuentaDeCompensacion tmct0TipoCuentaDeCompensacion;
    String cdCodigoCuentaS3;

    LOG.debug("[envioOperacionesCliente] Inicio ...");
    List<String> cuentasDc = tmct0movimientoeccDao.findDistinctCuentaCompensacionByTmct0alcId(tmct0alc.getId());

    tmct0CuentasDeCompensacion = Objects.requireNonNull(tmct0CuentasDeCompensacionBo.findByCdCodigo(cuentasDc.get(0)),
        "Cuenta de compensacion requerida");

    if (tmct0CuentasDeCompensacion.getEsCuentaPropia())
      return true;
    RegistroEnvioFicheroMegara registroMegaraCliente = new RegistroEnvioFicheroMegara();
    tmct0TipoCuentaDeCompensacion = tmct0CuentasDeCompensacion.getTmct0TipoCuentaDeCompensacion();
    cdCodigoCuentaS3 = tmct0CuentasDeCompensacion.getCdCodigoS3();
    if (cdCodigoCuentaS3 == null && cuentasTerceros.contains(tmct0CuentasDeCompensacion.getCdCodigo())) {
      cdCodigoCuentaS3 = tmct0alc.getAcctatcle();
    }
    if (isCuentaValoresInvalida(cdCodigoCuentaS3)) {
      throw new Exception(
          MessageFormat.format("No se ha encontrado cuenta de valores válida para la cuenta compensacion {0}",
              tmct0CuentasDeCompensacion.getCdCodigo()));
    }
    registroMegaraCliente.setCuentaS3(cdCodigoCuentaS3);
    registroMegaraCliente.setCdCuentaCompensacion(tmct0CuentasDeCompensacion.getCdCuentaCompensacion());
    registroMegaraCliente.setCdCuentaLiquidacion(cdCodigoCuentaS3);
    // Para el fichero en la pata cliente el sentido se pone lo contrario
    registroMegaraCliente.setSentido(tmct0alo.getCdtpoper().toString());
    if (isCompra) {
      // Si es una compra hay un caso especial para tipo de cuentas CIEN y
      // CIEB
      if (tmct0TipoCuentaDeCompensacion.getCdCodigo().equals(Constantes.CIEN)
          || tmct0TipoCuentaDeCompensacion.getCdCodigo().equals(Constantes.CIEB)) {
        registroMegaraCliente.setCdtpoper(Constantes.VEIF); // VEIF
      }
      else {
        // registroMegaraCliente.setCdtpoper(venta_clienten); // VENT
        registroMegaraCliente.setCdtpoper(configParams.get("venta_clienten")); // VENT
      } // else
    }
    else {
      // Si es una venta hay un caso especial para tipo de cuentas CIEN y
      // CIEB
      if (tmct0TipoCuentaDeCompensacion.getCdCodigo().equals(Constantes.CIEN)
          || tmct0TipoCuentaDeCompensacion.getCdCodigo().equals(Constantes.CIEB)) {
        registroMegaraCliente.setCdtpoper(Constantes.COIF); // COIF
      }
      else {
        // registroMegaraCliente.setCdtpoper(compra_clienten); // COMP
        registroMegaraCliente.setCdtpoper(configParams.get("compra_clienten")); // COMP
      } // else
    } // else
    registroMegaraCliente.setCdisinxx(tmct0alo.getCdisin());
    registroMegaraCliente.setTxdescri(tmct0alo.getNbvalors());
    registroMegaraCliente.setFhcontra(tmct0alc.getFeejeliq());
    registroMegaraCliente.setFhvalcon(tmct0alc.getFeopeliq());
    registroMegaraCliente.setFhteoliq(tmct0alc.getFeopeliq());
    registroMegaraCliente.setNutitulo(tmct0alc.getNutitliq());
    registroMegaraCliente.setCddivisa(tmct0alo.getCdmoniso());
    LOG.debug("[envioOperacionesCliente] tmct0alc.getNutitliq(): " + tmct0alc.getNutitliq());
    registroMegaraCliente.setImcpreci(tmct0alc.getImnfiliq().divide(tmct0alc.getNutitliq(), 2, RoundingMode.HALF_UP));
    registroMegaraCliente.setImefecti(tmct0alc.getImnfiliq());
    registroMegaraCliente.setImefeagr(tmct0alc.getImefeagr());
    registroMegaraCliente.setImpornet(tmct0alc.getImnfiliq());
    registroMegaraCliente.setNbclient(tmct0alc.getNbtitliq());
    registroMegaraCliente.setImpcliente(tmct0alc.getImnfiliq());
    registroMegaraCliente.setOurparticipe(tmct0alc.getOurpar());
    registroMegaraCliente.setTheirpartici(tmct0alc.getTheirpar());
    registroMegaraCliente.setImpnetvto(tmct0alc.getImnfiliq());
    if (tmct0alc.getCdniftit().compareToIgnoreCase("CEDELULLXXX") == 0) {
      registroMegaraCliente.setCdparticipante("CEDELULLXXX");
    }
    else if (tmct0alc.getCdniftit().compareToIgnoreCase("MGTCBEBEXXX") == 0) {
      registroMegaraCliente.setCdparticipante("MGTCBEBEXXX");
    }
    else {
      registroMegaraCliente.setCdparticipante(tmct0alc.getCdrefban());
    } // else
    String cdcliter = tmct0alc.getCdordtit() != null ? tmct0alc.getCdordtit().trim() : "";
    if ((cdcliter.matches("[0-9]*")) && (cdcliter.length() == 10 || cdcliter.length() == 11)) {
      LOG.debug("[envioOperacionesCliente] Cdordtit numerico y de '" + cdcliter.length()
          + "' posiciones. Comprobando TMCT0AFI ...");
      List<Tmct0afi> tmct0afiList = tmct0afiDao.findActivosByAlcData(tmct0alc.getId().getNuorden(),
          tmct0alc.getId().getNbooking(), tmct0alc.getId().getNucnfclt(), tmct0alc.getId().getNucnfliq());
      LOG.debug("[envioOperacionesCliente] TMCT0AFI encontrados: " + tmct0afiList.size());
      for (Tmct0afi tmct0afi : tmct0afiList) {
        LOG.debug("[envioOperacionesCliente] Procesando TMCT0AFI: " + tmct0afi.getId().getNusecuen());
        if (tmct0afi.getCdholder().substring(0, 11).compareTo(tmct0alc.getCdniftit()) == 0) {
          LOG.debug("[envioOperacionesCliente] TMCT0AFI con DNIF igual al del ALC");
          if ((tmct0afi.getTpidenti() == 'O') && (tmct0afi.getCdddebic().matches("[0-9]*"))
              && (tmct0afi.getCdddebic().length() == 11)) {
            LOG.debug("[envioOperacionesCliente] TMCT0AFI con TPIDENTI=O numerico y de 11 posiciones encontrado");
            cdcliter = "COD".concat(tmct0afi.getCdddebic().substring(3));
          }
          else {
            LOG.debug(
                "[envioOperacionesCliente] Resto de condiciones no superadas. AFI.Tpidenti: '{}'. AFI.Cdddebic: '{}'",
                tmct0afi.getTpidenti(), tmct0afi.getCdddebic());
          } // else
        }
        else {
          LOG.debug(
              "[envioOperacionesCliente] TMCT0AFI con DNIF NO igual al del ALC. AFI.Cdholder: '{}'. ALC.Cdniftit: '{}'",
              tmct0afi.getCdholder().substring(0, 11), tmct0alc.getCdniftit());
        } // else
      } // for (Tmct0afi tmct0afi : tmct0afiList)
    } // if ((cdcliter.matches("[0-9]*")) && (cdcliter.length() == 10 ||
    LOG.debug("[envioOperacionesCliente] Cdcliter: '" + cdcliter + "'");
    registroMegaraCliente.setCdcliter(cdcliter);
    if (!"".equals(StringUtils.trimToEmpty(tmct0alc.getCdcustod()))) {
      registroMegaraCliente.setCdcontra(tmct0alc.getCdcustod());
    }
    else if (!"".equals(StringUtils.trimToEmpty(tmct0alc.getCdentdep()))) {
      List<Tmct0cleNacional> lista_cle = tmct0cleDao.findActiveByCdcleare(tmct0alc.getCdentdep());
      if (lista_cle.size() > 0) {
        registroMegaraCliente.setCdcontra(lista_cle.get(0).getBiccle());
      }
      else if (!"".equals(StringUtils.trimToEmpty(tmct0alc.getCdentliq()))) {
        lista_cle = tmct0cleDao.findActiveByCdcleare(tmct0alc.getCdentliq());
        if (lista_cle.size() > 0) {
          registroMegaraCliente.setCdcontra(lista_cle.get(0).getBiccle());
        }
        else {
          registroMegaraCliente.setCdcontra(tmct0alc.getAcctatcle());
        } // else
      }
      else {
        registroMegaraCliente.setCdcontra(tmct0alc.getAcctatcle());
      } // else
    }
    else if (!"".equals(StringUtils.trimToEmpty(tmct0alc.getCdentliq()))) {
      List<Tmct0cleNacional> lista_cle = tmct0cleDao.findActiveByCdcleare(tmct0alc.getCdentliq());
      if (lista_cle.size() > 0) {
        registroMegaraCliente.setCdcontra(lista_cle.get(0).getBiccle());
      }
      else {
        registroMegaraCliente.setCdcontra(tmct0alc.getAcctatcle());
      } // else
    }
    else {
      LOG.warn("Cdcustod, Cdentdep y Cdentliq vacios o nulos");
    } // else
    LOG.debug("[envioOperacionesCliente] Contrapartida: '" + registroMegaraCliente.getCdcontra() + "'");
    registroMegaraCliente.setImcomisn(BigDecimal.ZERO);
    registroMegaraCliente.setCdalias(alias);
    String cuentaCompensacionDestino = "";
    List<Long> desglosesCamara = new ArrayList<Long>();
    List<Tmct0desglosecamara> dcList = tmct0alc.getTmct0desglosecamaras();
    if (dcList != null && !dcList.isEmpty()) {
      String codigoCamaraCompensacion = dcList.get(0).getTmct0CamaraCompensacion().getCdCodigo();
      registroMegaraCliente.setCamaraCompensacion(codigoCamaraCompensacion);
      for (Tmct0desglosecamara desglose : dcList) {
        desglosesCamara.add(desglose.getIddesglosecamara());
      }
    }
    else
      LOG.warn("[envioOperacionesCliente] No se han localizado desgloses cámara para el Alc {} y "
          + "no se ha podido determinar el código de camara compensacion", tmct0alc.getId());
    List<String> tmct0movimientoeccList = tmct0movimientoeccDao
        .findDistinctCuentaCompensacionByIdDesglosesCamara(desglosesCamara);
    if (tmct0movimientoeccList == null || tmct0movimientoeccList.isEmpty()) {
      tmct0logBo.insertarRegistro(tmct0alc, new Date(), new Date(), null, estadoEntrec,
          "No se ha encontrado valor de cuenta compensación destino para movimientos en CDESTADOMOV=9 para el alc con id = "
              + tmct0alc.getId());
    }
    else if (tmct0movimientoeccList.size() > 1) {
      tmct0logBo.insertarRegistro(tmct0alc, new Date(), new Date(), null, estadoEntrec,
          "Se ha encontrado más un valor diferente de cuenta compensación destino para movimientos en CDESTADOMOV=9 para el alc con id = "
              + tmct0alc.getId());
    }
    else {
      cuentaCompensacionDestino = tmct0movimientoeccList.get(0);
    }
    // Se comprueba si es cliente netting para realizar los cálculos
    if (isNetting) {
      LOG.debug("[envioOperacionesCliente] El clientes es netting");

      // Si la liquidación no ha realizado ningún netting entonces se
      // añade a la lista, sino ya se ha neteado y se
      // calcula sobre el registro ya guardado
      Netting netting = realizarNetting(registroMegaraCliente, registrosMegaraClienteList, nettingsClienteMap, tmct0alc,
          estadoEntrec);
      if (netting == null) {
        throw new Exception("Error al realizar el netting: nettingActualizar == null");
      } // if

      if (estadoEntrec.compareTo(CLEARING.PDTE_ENVIAR_ANULACION.getId()) != 0) {
        LOG.debug("[envioOperacionesCliente] Creando DB alcOrdenes ...");

        Tmct0alcId tmct0alcId = tmct0alc.getId();
        AlcOrdenes alcOrdenes = new AlcOrdenes();
        alcOrdenes.setNbooking(tmct0alcId.getNbooking());
        alcOrdenes.setNuorden(tmct0alcId.getNuorden());
        alcOrdenes.setNucnfliq(tmct0alcId.getNucnfliq());
        alcOrdenes.setNucnfclt(tmct0alcId.getNucnfclt());
        alcOrdenes.setEstado(estadosMap.get(ALC_ORDENES.SIN_FACTURA.getId()));
        alcOrdenes.setNetting(netting);
        alcOrdenes.setReferenciaS3(registroMegaraCliente.getPoolreference());
        alcOrdenes.setCuentaCompensacionDestino(cuentaCompensacionDestino);
        alcOrdenes.setTitulosEnviados(tmct0alc.getNutitliq());
        alcOrdenes.setTitulosAsignados(tmct0alc.getNutitliq());
        alcOrdenes.setEstadoInstruccion('A');
        alcOrdenesBo.save(alcOrdenes);
        alcOrdenesClienteMap.put(tmct0alc.getId(), alcOrdenes);

      } // if
        // (estadoEntrec.compareTo(CLEARING.PDTE_ENVIAR_ANULACION.getId())
        // != 0)
    }
    else {
      LOG.debug("[envioOperacionesCliente] El clientes NO es de netting. Comprobando alcOrdenes ...");
      AlcOrdenes alcOrdenes = null;

      if (estadoEntrec.compareTo(CLEARING.PDTE_ENVIAR_ANULACION.getId()) == 0) {
        LOG.debug("[envioOperacionesCliente] Anulacion. Obteniendo DB alcOrdenes ...");
        // Se envía cancelación
        registroMegaraCliente.setCancelaciones("S");
        alcOrdenes = alcOrdenesBo.findByTmct0alc(tmct0alc);

        registroMegaraCliente.setNureftraTipo(Utilidad.CLEARING_TIPO.CLIENTE.getTipo());
        registroMegaraCliente.setNureftraDato(String.format("%015d", alcOrdenes.getId()));
        registroMegaraCliente
            .setPoolreference(registroMegaraCliente.getNureftraTipo().concat(registroMegaraCliente.getNureftraDato()));
        registrosMegaraClienteList.add(registroMegaraCliente);
      }
      else {
        LOG.debug("[envioOperacionesCliente] Creando DB alcOrdenes ...");

        Tmct0alcId tmct0alcId = tmct0alc.getId();
        alcOrdenes = new AlcOrdenes();
        alcOrdenes.setNbooking(tmct0alcId.getNbooking());
        alcOrdenes.setNuorden(tmct0alcId.getNuorden());
        alcOrdenes.setNucnfliq(tmct0alcId.getNucnfliq());
        alcOrdenes.setNucnfclt(tmct0alcId.getNucnfclt());
        alcOrdenes.setEstado(estadosMap.get(ALC_ORDENES.SIN_FACTURA.getId()));
        alcOrdenes.setCuentaCompensacionDestino(cuentaCompensacionDestino);
        alcOrdenes.setTitulosEnviados(tmct0alc.getNutitliq());
        alcOrdenes.setTitulosAsignados(tmct0alc.getNutitliq());
        alcOrdenes.setEstadoInstruccion('A');
        alcOrdenesBo.save(alcOrdenes);
        registroMegaraCliente.setNureftraTipo(Utilidad.CLEARING_TIPO.CLIENTE.getTipo());
        registroMegaraCliente.setNureftraDato(String.format("%015d", alcOrdenes.getId()));
        registroMegaraCliente.setPoolreference(alcOrdenes.getReferenciaS3());
        registrosMegaraClienteList.add(registroMegaraCliente);
        alcOrdenesClienteMap.put(tmct0alc.getId(), alcOrdenes);
      } // else
    } // else netting
    return false;
  } // envioOperacionesCliente

  /**
   * Realiza el netting entre la operación que se está procesando actualmente y
   * las anteriormente neteadas si las hay.
   * 
   * @param registroMegaraCliente Operación que se está procesando.
   * @param registrosMegaraClienteList Lista de operaciones procesadas.
   * @param nettingsClienteMap Mapa de neteos realizados.
   * @return Nuevo neteo creado o neteo modificado.
   * @throws Exception SI ocurre algún error durante la operación de neteo.
   */
  private Netting realizarNetting(RegistroEnvioFicheroMegara registroMegaraCliente,
      List<RegistroEnvioFicheroMegara> registrosMegaraClienteList, HashMap<Long, Netting> nettingsClienteMap,
      Tmct0alc tmct0alc, Integer estadoEntrec) throws Exception {
    LOG.debug("[realizarNettingParaLiquidacion] Inicio ...");
    Long idNetting = new Long(-1);
    boolean isFirstNettingRegistry = true;
    BigDecimal acumulado = BigDecimal.ZERO;

    // Se recorre la lista de los registros ya creados para ver si existe el
    // alias y realizar neting si es oportuno
    for (RegistroEnvioFicheroMegara registroMegaraClienteYaProcesado : registrosMegaraClienteList) {
      if (registroMegaraCliente.getCdalias().equals(registroMegaraClienteYaProcesado.getCdalias())
          && registroMegaraCliente.getFhcontra().equals(registroMegaraClienteYaProcesado.getFhcontra())
          && registroMegaraCliente.getFhteoliq().equals(registroMegaraClienteYaProcesado.getFhteoliq())
          && registroMegaraCliente.getCdisinxx().equals(registroMegaraClienteYaProcesado.getCdisinxx())
          && registroMegaraCliente.getCamaraCompensacion()
              .equals(registroMegaraClienteYaProcesado.getCamaraCompensacion())
          && registroMegaraCliente.getCdCuentaCompensacion()
              .equals(registroMegaraClienteYaProcesado.getCdCuentaCompensacion())) {
        LOG.debug("[realizarNettingParaLiquidacion] Encontrado neteo anterior. Acumulando ...");
        isFirstNettingRegistry = false;
        acumulado = BigDecimal.ZERO;

        // La comision siempre se suma porque viene en + y en - (es para
        // insertar en movimiento cuenta alias)
        acumulado = registroMegaraClienteYaProcesado.getImcomisn().add(registroMegaraCliente.getImcomisn());
        registroMegaraClienteYaProcesado.setImcomisn(acumulado);

        // Si es la misma operación o las dos compras o las dos ventas
        // entonces se suman o tambien cuando la liquidacion
        // guardada es vacio porque se han igualado la compra con la
        // venta
        if (registroMegaraCliente.getSentido().equals(registroMegaraClienteYaProcesado.getSentido())
            || registroMegaraClienteYaProcesado.getSentido().equals("")) {

          // Se suman los titulos a los que ya guardados
          acumulado = registroMegaraClienteYaProcesado.getNutitulo().add(registroMegaraCliente.getNutitulo());
          registroMegaraClienteYaProcesado.setNutitulo(acumulado);

          // Se suma los netos de S3 (efectivo+corretaje) de la
          // operación
          acumulado = registroMegaraClienteYaProcesado.getImefecti().add(registroMegaraCliente.getImefecti());
          registroMegaraClienteYaProcesado.setImefecti(acumulado);

          // Se suman los netos de la operación
          acumulado = registroMegaraClienteYaProcesado.getImpornet().add(registroMegaraCliente.getImpornet());
          registroMegaraClienteYaProcesado.setImpornet(acumulado);

          // Se suman los efectivos de la operación
          acumulado = registroMegaraClienteYaProcesado.getImefeagr().add(registroMegaraCliente.getImefeagr());
          registroMegaraClienteYaProcesado.setImefeagr(acumulado);

          // Se actualiza la operación con la nueva liquidación por si
          // estaba neteada a vacio
          registroMegaraClienteYaProcesado.setCdtpoper(registroMegaraCliente.getCdtpoper());
          registroMegaraClienteYaProcesado.setSentido(registroMegaraCliente.getSentido());
        }
        else {
          // Si son diferentes operaciónes significa que uno es una
          // compra y el otro es una venta
          int comparacionTitulos = registroMegaraCliente.getNutitulo()
              .compareTo(registroMegaraClienteYaProcesado.getNutitulo());
          // Si los titulos son iguales
          if (comparacionTitulos == 0) {
            // Se ponen los titulos a 0 ya que se igualan
            registroMegaraClienteYaProcesado.setNutitulo(BigDecimal.ZERO);

            // Al ser los titulos iguales hay que mirar cual es el
            // importe efectivo mayor
            int comparacionEfectivo = registroMegaraCliente.getImefecti()
                .compareTo(registroMegaraClienteYaProcesado.getImefecti());

            if (comparacionEfectivo == 0) { // Si el efectivo es
                                            // igual
              // Operación es vacio porque se iguala los titulos y
              // el precio de la compra-venta
              registroMegaraClienteYaProcesado.setCdtpoper("");
              registroMegaraClienteYaProcesado.setSentido("");
              registroMegaraClienteYaProcesado.setImefecti(BigDecimal.ZERO);
              registroMegaraClienteYaProcesado.setImpornet(BigDecimal.ZERO);
              registroMegaraClienteYaProcesado.setImcpreci(BigDecimal.ZERO);
              registroMegaraClienteYaProcesado.setImefeagr(BigDecimal.ZERO);

            }
            else if (comparacionEfectivo == 1) { // Si el precio
                                                 // de liquidación
                                                 // nueva es mayor
                                                 // a la guardada
              // Se resta el neto de S3 (efectivo+corretaje) de la
              // operación nueva menos el de la guardada
              acumulado = registroMegaraCliente.getImefecti().subtract(registroMegaraClienteYaProcesado.getImefecti());
              registroMegaraClienteYaProcesado.setImefecti(acumulado);

              // Se resta el neto de la operación
              acumulado = registroMegaraCliente.getImpornet().subtract(registroMegaraClienteYaProcesado.getImpornet());
              registroMegaraClienteYaProcesado.setImpornet(acumulado);

              // Se resta el efectivo de la operación
              acumulado = registroMegaraCliente.getImefeagr().subtract(registroMegaraClienteYaProcesado.getImefeagr());
              registroMegaraClienteYaProcesado.setImefeagr(acumulado);

              // Se actualiza la operación con la nueva
              // liquidación al ser mayor los titulos
              registroMegaraClienteYaProcesado.setCdtpoper(registroMegaraCliente.getCdtpoper());
              registroMegaraClienteYaProcesado.setSentido(registroMegaraCliente.getSentido());

            }
            else if (comparacionEfectivo == -1) { // Si el precio
                                                  // de la
                                                  // liquidacion
                                                  // guardada es
                                                  // mayor que la
                                                  // nueva
              // Se resta el neto s3 (efectivo+corretaje) de la
              // operación guardada menos el de la nueva
              acumulado = registroMegaraClienteYaProcesado.getImefecti().subtract(registroMegaraCliente.getImefecti());
              registroMegaraClienteYaProcesado.setImefecti(acumulado);

              // Se resta el neto de la operación
              acumulado = registroMegaraClienteYaProcesado.getImpornet().subtract(registroMegaraCliente.getImpornet());
              registroMegaraClienteYaProcesado.setImpornet(acumulado);

              // Se resta el efectivo de la operación
              acumulado = registroMegaraClienteYaProcesado.getImefeagr().subtract(registroMegaraCliente.getImefeagr());
              registroMegaraClienteYaProcesado.setImefeagr(acumulado);

              // Se actualiza la operación con la guardada al ser
              // mayor el efectivo
              registroMegaraClienteYaProcesado.setCdtpoper(registroMegaraClienteYaProcesado.getCdtpoper());
              registroMegaraClienteYaProcesado.setSentido(registroMegaraClienteYaProcesado.getSentido());
            } // else if comparacionEfectivo
          }
          else if (comparacionTitulos == 1) { // Si el primer valor
                                              // es mayor (los
                                              // titulos de la nueva
                                              // liquidación)
            // Al ser los nuevos titulos mayor entonces sobre estos
            // se resta los que ya están guardados
            acumulado = registroMegaraCliente.getNutitulo().subtract(registroMegaraClienteYaProcesado.getNutitulo());
            registroMegaraClienteYaProcesado.setNutitulo(acumulado);

            // Se resta el neto s3 (efectivo+corretaje) de la
            // operación
            acumulado = registroMegaraCliente.getImefecti().subtract(registroMegaraClienteYaProcesado.getImefecti());
            registroMegaraClienteYaProcesado.setImefecti(acumulado);

            // Se resta el neto de la operación
            acumulado = registroMegaraCliente.getImpornet().subtract(registroMegaraClienteYaProcesado.getImpornet());
            registroMegaraClienteYaProcesado.setImpornet(acumulado);

            // Se resta el efecto de la operación
            acumulado = registroMegaraCliente.getImefeagr().subtract(registroMegaraClienteYaProcesado.getImefeagr());
            registroMegaraClienteYaProcesado.setImefeagr(acumulado);

            // Se actualiza la operación con la nueva liquidación al
            // ser mayor los titulos
            registroMegaraClienteYaProcesado.setCdtpoper(registroMegaraCliente.getCdtpoper());
            registroMegaraClienteYaProcesado.setSentido(registroMegaraCliente.getSentido());
          }
          else if (comparacionTitulos == -1) { // Si el segundo
                                               // valor es mayor
                                               // (los titulos
                                               // guardados en la
                                               // lista)
            // Al tener mas titulos el guardado entonces le se
            // restan los titulos del nuevo
            acumulado = registroMegaraClienteYaProcesado.getNutitulo().subtract(registroMegaraCliente.getNutitulo());
            registroMegaraClienteYaProcesado.setNutitulo(acumulado);

            // Se resta el efectivo de la operación
            acumulado = registroMegaraClienteYaProcesado.getImefecti().subtract(registroMegaraCliente.getImefecti());
            registroMegaraClienteYaProcesado.setImefecti(acumulado);

            // Se resta el neto de la operación
            acumulado = registroMegaraClienteYaProcesado.getImpornet().subtract(registroMegaraCliente.getImpornet());
            registroMegaraClienteYaProcesado.setImpornet(acumulado);

            // Se resta el efectivo de la operación
            acumulado = registroMegaraClienteYaProcesado.getImefeagr().subtract(registroMegaraCliente.getImefeagr());
            registroMegaraClienteYaProcesado.setImefeagr(acumulado);
          } // else if comparacionTitulos
        } // else tipo de operacion

        // Se calcula el precio NETO / TITULOS. Simpre que los titulos o
        // el importe no sean cero
        if (registroMegaraClienteYaProcesado.getImpornet() == BigDecimal.ZERO
            || registroMegaraClienteYaProcesado.getNutitulo() == BigDecimal.ZERO) {
          acumulado = BigDecimal.ZERO;
        }
        else {
          LOG.debug("[envioOperacionesCliente] registroMegaraClienteYaProcesado.getNutitulo(): "
              + registroMegaraClienteYaProcesado.getNutitulo());
          acumulado = registroMegaraClienteYaProcesado.getImpornet()
              .divide(registroMegaraClienteYaProcesado.getNutitulo(), 2, RoundingMode.HALF_UP);
        }
        registroMegaraClienteYaProcesado.setImcpreci(acumulado);

        // Se actualiza la tabla netting sumando los nuevos valores a
        // los que ya hay guardados
        idNetting = registroMegaraClienteYaProcesado.getIdnetting();
        Netting modNetting = nettingsClienteMap.get(idNetting);
        if (modNetting == null) {
          throw new Exception(
              "No se encuenta en la tabla Netting el identificador [" + String.valueOf(idNetting) + "]");
        } // if

        if (registroMegaraCliente.getSentido().equals(Constantes.COMPRA)) { // En
                                                                            // caso
                                                                            // de
                                                                            // compra
          // Titulo compra
          acumulado = modNetting.getNutituloCompra().add(registroMegaraCliente.getNutitulo());
          modNetting.setNutituloCompra(acumulado);
          // Efectivo compra
          acumulado = modNetting.getEfectivoCompra().add(registroMegaraCliente.getImefeagr());
          modNetting.setEfectivoCompra(acumulado);
          // Corretaje compra
          acumulado = modNetting.getCorretajeCompra().add(registroMegaraCliente.getImcomisn());
          modNetting.setCorretajeCompra(acumulado);
        }
        else { // En caso de venta
          // Titulo venta
          acumulado = modNetting.getNutituloVenta().add(registroMegaraCliente.getNutitulo());
          modNetting.setNutituloVenta(acumulado);
          // Efectivo venta
          acumulado = modNetting.getEfectivoVenta().add(registroMegaraCliente.getImefeagr());
          modNetting.setEfectivoVenta(acumulado);
          // Corretaje venta
          acumulado = modNetting.getCorretajeVenta().add(registroMegaraCliente.getImcomisn());
          modNetting.setCorretajeVenta(acumulado);
        } // else

        // Se calcula el neteo de cada campo
        // Titulo neteado
        acumulado = modNetting.getNutituloCompra().subtract(modNetting.getNutituloVenta());
        modNetting.setNutituloNeteado(acumulado);
        // Efectivo neteado
        acumulado = modNetting.getEfectivoCompra().subtract(modNetting.getEfectivoVenta());
        modNetting.setEfectivoNeteado(acumulado);
        // Corretaje neteado
        acumulado = modNetting.getCorretajeCompra().add(modNetting.getCorretajeVenta());
        modNetting.setCorretajeNeteado(acumulado);

        // Ya se ha encontrado y se ha hecho el neteo. Se termina
        // devolviendo el netting modificado
        return modNetting;
      } // if (cumple condiciones netting)
    } // for (RegistroEnvioFicheroMegara registroMegaraClienteYaProcesado :
      // registrosMegaraClienteList)

    // Si no se ha realizado neteo entonces no cumple con otra liquidacion
    // para netear y se añade a la lista
    if (isFirstNettingRegistry) {
      Netting nuevoNetting = null;

      if (estadoEntrec.compareTo(CLEARING.PDTE_ENVIAR_ANULACION.getId()) == 0) {
        LOG.debug("[realizarNettingParaLiquidacion] Anulacion. Obteniendo DB netting ...");
        // Se envía cancelación
        registroMegaraCliente.setCancelaciones("S");
        nuevoNetting = alcOrdenesBo.findByTmct0alc(tmct0alc).getNetting();
      }
      else {
        LOG.debug("[envioOperacionesCliente] Creando DB netting ...");
        nuevoNetting = new Netting(registroMegaraCliente.getCdalias(), registroMegaraCliente.getCdisinxx(),
            registroMegaraCliente.getFhteoliq(), registroMegaraCliente.getFhcontra());
        // Como es el primero el neteado es el mismo
        nuevoNetting.setNutituloNeteado(registroMegaraCliente.getNutitulo());
        nuevoNetting.setEfectivoNeteado(registroMegaraCliente.getImefeagr());
        nuevoNetting.setCorretajeNeteado(registroMegaraCliente.getImcomisn());
        nuevoNetting.setCamaraCompensacion(registroMegaraCliente.getCamaraCompensacion());
        nuevoNetting.setCdcuentacompensacion(registroMegaraCliente.getCdCuentaCompensacion());
        nuevoNetting.setNbvalors(registroMegaraCliente.getTxdescri());
        if (registroMegaraCliente.getSentido().equals(Constantes.COMPRA)) {
          nuevoNetting.setNutituloCompra(registroMegaraCliente.getNutitulo());
          nuevoNetting.setEfectivoCompra(registroMegaraCliente.getImefeagr());
          nuevoNetting.setCorretajeCompra(registroMegaraCliente.getImcomisn());
        }
        else {
          nuevoNetting.setNutituloVenta(registroMegaraCliente.getNutitulo());
          nuevoNetting.setEfectivoVenta(registroMegaraCliente.getImefeagr());
          nuevoNetting.setCorretajeVenta(registroMegaraCliente.getImcomisn());
        } // else
        nettingBo.save(nuevoNetting);
      } // else
      nettingsClienteMap.put(nuevoNetting.getId(), nuevoNetting);

      registroMegaraCliente.setIdnetting(nuevoNetting.getId());
      registroMegaraCliente.setNureftraTipo(Utilidad.CLEARING_TIPO.CLIENTE_NETTING.getTipo());
      registroMegaraCliente.setNureftraDato(String.format("%015d", nuevoNetting.getId()));
      registroMegaraCliente
          .setPoolreference(registroMegaraCliente.getNureftraTipo().concat(registroMegaraCliente.getNureftraDato()));
      registrosMegaraClienteList.add(registroMegaraCliente);

      return nuevoNetting;
    } // if (isFirstNettingRegistry)

    return null;
  } // realizarNetting

}
