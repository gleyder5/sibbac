package sibbac.business.operativanetting.database.bo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.hibernate.AssertionFailure;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.common.EnvioPataMercado;
import sibbac.business.operativanetting.common.EnvioPataCliente;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.operativanetting.exceptions.EnvioFicheroMegaraException;
import sibbac.business.operativanetting.exceptions.LoadConfigurationException;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;

/**
 * Genera el fichero para enviar a Megara con las ordenes de clearing y netting que cumplas las condiciones para su envío.
 * 
 * @author XI316153
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EnvioClearingNettingBo extends AbstractBo<Tmct0alc, Tmct0alcId, Tmct0alcDao> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(EnvioClearingNettingBo.class);

  private static final String MENSAJE_ALARMA_CLIENTE = 
          "Referencia fichero cliente: '%s' / Booking: '%s' / Titulos: '%s' / Titular: '%s'.";
  
  private static final String MENSAJE_ALARMA_MERCADO = "Referencia fichero mercado: '%s%015d'.";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private Tmct0cfgDao tmct0cfgDao;

  @Autowired
  private Tmct0xasBo tmct0xasBo;

  @Autowired
  private ApplicationContext ctx;

  /** Indica desde que día hay que buscar las ordenes a enviar. */
  private Date diaDesde;

  /** Indica la ruta donde dejar el fichero generado. */
  private String outputFilePath;

  /** Nombre del fichero a generar. */
  private String outputFileName;

  /** Fichero a generar. */
  private File outputFile = null;

  /** Referencia para escribir en el fichero. */
  private PrintWriter prtWriter = null;

  /** Lista de días configurados como festivos. */
  private List<Date> holidaysConfigList = null;

  /** Lista de dias configurados como laborables. */
  private List<Integer> workDaysOfWeekConfigList = null;

  /** Parámetros de configuración del proceso. */
  private HashMap<String, String> configParams = null;

  @Autowired
  private SendMail sendMail;

  @Value("${aviso.emails.alarma}")
  private String emailsAlarma;

  @Value("${aviso.subject}")
  private String subject;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Prepara
   * 
   * @throws Exception Si falla al cargar la configuración.
   */
    @Transactional()
    public String ejecutarEnvioOperaciones(Integer nuEnvio) throws EnvioFicheroMegaraException {
        final Map<String, List<String>> registrosAlarma;
        final StringBuffer body;
        final EnvioPataMercado envioPataMercado;
        final EnvioPataCliente envioPataCliente;
        String errorMsgMercado = null, errorMsgCliente = null, result = null;
        int lineasFichero = 0, lineasFicheroParcial = 0;

        LOG.debug("[ejecutarEnvioOperaciones] Inicio ...");
        configParams = new HashMap<String, String>();
        try {
            if (loadConfiguration()) {
                openOutputFile(nuEnvio);
                LOG.debug("[ejecutarEnvioOperaciones] diaDesde: " + diaDesde);
                envioPataMercado = ctx.getBean(EnvioPataMercado.class);
                envioPataMercado.initProcess(configParams);
                //PATA MERCADO
                try {
                    LOG.debug("[ejecutarEnvioOperaciones] Inicio envio parte mercado");
                    lineasFichero = envioPataMercado.generaEnvio(diaDesde, prtWriter);
                    errorMsgMercado = null;
                    prtWriter.flush();
                    LOG.debug("[ejecutarEnvioOperaciones] Fin de envio parte mercado, total lineas{}", lineasFichero);
                } catch (HibernateException | AssertionFailure he) {
                    LOG.error("[ejecutarEnvioOperaciones] Incidencia fatal de hibernate en pata mercado: {} ",
                            he.getMessage());
                    LOG.debug("[ejecutarEnvioOperaciones] Pila de ejecucion ... ", he);
                    errorMsgMercado = "[ejecutarEnvioOperaciones] Incidencia fatal de hibernate ... " + he.getMessage();
                } catch (PersistenceException pe) {
                    LOG.error("[ejecutarEnvioOperaciones] Incidencia fatal de base de datos en pata mercado; {}",
                            pe.getMessage());
                    LOG.debug("[ejecutarEnvioOperaciones] Pila de ejecucion ... ", pe);
                    errorMsgMercado = "[ejecutarEnvioOperaciones] Incidencia fatal de base de datos ... "
                            + pe.getMessage();
                } catch (Exception ex) {
                    LOG.error("[ejecutarEnvioOperaciones] Error inesperado en pata mercado", ex);
                    errorMsgMercado = "Error inesperado" + ex.getMessage();
                }
                //PATA CLIENTE
                envioPataCliente = ctx.getBean(EnvioPataCliente.class);
                envioPataCliente.initProcess(configParams);
                try {
                    lineasFicheroParcial = envioPataCliente.generaEnvio(
                            diaDesde, prtWriter);
                    LOG.debug("[ejecutarEnvioOperaciones] Lineas de pata cliente a escribir en el fichero: "
                            + lineasFicheroParcial);
                    lineasFichero += lineasFicheroParcial;
                    prtWriter.flush();
                } catch (HibernateException | AssertionFailure he) {
                    LOG.error("[ejecutarEnvioOperaciones] Incidencia fatal de hibernate en pata cliente: {} ",
                            he.getMessage());
                    LOG.debug("[ejecutarEnvioOperaciones] Pila de ejecucion en pata cliente... ", he);
                    errorMsgCliente = "[ejecutarEnvioOperaciones] Incidencia fatal de hibernate ... " + he.getMessage();
                } catch (PersistenceException pe) {
                    LOG.error("[ejecutarEnvioOperaciones] Incidencia fatal de base de datos en pata cliente ... "
                            + pe.getMessage());
                    LOG.debug("[ejecutarEnvioOperaciones] Pila de ejecucion ... ", pe);
                    errorMsgCliente = "[ejecutarEnvioOperaciones] Incidencia fatal de base de datos ... "
                            + pe.getMessage();
                } catch (Exception ex) {
                    LOG.error("[ejecutarEnvioOperaciones] Error inesperado en pata cliente", ex);
                    errorMsgCliente = "Error inesperado" + ex.getMessage();
                }

                // Se cierra el fichero y se inserta la línea de fin de fichero
                // si se han escrito registros, si no se han escrito lineas se
                // borra
                result = closeOutputFile(lineasFichero);
                registrosAlarma = envioPataCliente.getRegistrosAlarma();
                try {
                    if (registrosAlarma.size() > 0) {
                        body = new StringBuffer();
                        body.append(
                                "Se han encontrado las siguientes posibles desgloses duplicados en el envio a Megara");
                        LOG.debug(
                                "[ejecutarEnvioOperaciones] Se han encontrado las siguientes posibles desgloses duplicados en el envio a Megara");
                        body.append(System.getProperty("line.separator"));
                        body.append(System.getProperty("line.separator"));

                        for (Map.Entry<String, List<String>> referencia : registrosAlarma.entrySet()) {
                            body.append(String.format(MENSAJE_ALARMA_CLIENTE, referencia.getKey().split("#")[0],
                                    referencia.getKey().split("#")[1], referencia.getKey().split("#")[2],
                                    referencia.getKey().split("#")[3]));
                            body.append(System.getProperty("line.separator"));
                            for (String dc : referencia.getValue()) {
                                body.append(String.format(MENSAJE_ALARMA_MERCADO,
                                        Utilidad.CLEARING_TIPO.MERCADO.getTipo(),
                                        Integer.valueOf(dc)));
                                body.append(System.getProperty("line.separator"));
                            } // for
                            body.append(System.getProperty("line.separator"));
                        } // for
                        LOG.debug("[ejecutarEnvioOperaciones] Enviando mail alarmas ...");
                        sendMail.sendMail(Arrays.asList(emailsAlarma.split(";")), subject, body.toString());
                        LOG.debug("[ejecutarEnvioOperaciones] Mail enviado");
                    } // if (registrosAlarma.size() > 0)
                } catch (Exception e) {
                    LOG.warn("[ejecutarEnvioOperaciones] Error al enviar mail de alarmas ... " + e.getMessage());
                } // catch
            } else {
                LOG.warn("[ejecutarEnvioOperaciones] La configuracion no permite la ejecucion de la tarea");
            } // else

            // Si ha habido error y se han escrito registros quiero devolver
            // ambas cosas para poder dejar la tarea en 000 pero enviar lo que
            // se haya escrito
            if (result == null)
                result = "<null>";
            if (errorMsgMercado == null && errorMsgCliente == null)
                return result;
            return String.format("%s##%s|%s", result, errorMsgMercado, errorMsgCliente);
        } catch (Exception e) {
            LOG.warn("[ejecutarEnvioOperaciones] Incidencia generica ... " + e.getMessage());
            LOG.debug("Pila de ejecucion ... ", e);
            throw new EnvioFicheroMegaraException("[ejecutarEnvioOperaciones] " + e.getMessage(), e);
        } finally {
            LOG.debug("[ejecutarEnvioOperaciones] Fin ...");
        } // finally
    } // ejecutarEnvioOperaciones

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Carga los datos de configuración del proceso.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si los datos de configuración se cargaron correctamente; <code>false</code> en caso
   *         contrario.
   * @throws LoadConfigurationException Si ocurre algún error al cargar la configuración o no se encuentra alguna propiedad.
   */
  private boolean loadConfiguration() throws LoadConfigurationException {
    LOG.debug("[loadConfiguration] Inicio ...");

    Calendar ahora = Calendar.getInstance();
    Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                                  Utilidad.DatosConfig.MEGARA_SENT_DAYS.getProcess(),
                                                                                  Utilidad.DatosConfig.MEGARA_SENT_DAYS.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_SENT_DAYS.getKeyname());
    } // if
    Date diaBuscar = DateHelper.getPreviousWorkDate(ahora.getTime(), new Integer(datosConfiguracion.getKeyvalue()), getWorkDaysOfWeek(),
                                                    getHolidays());
    LOG.debug("[loadConfiguration] diaBuscar: " + diaBuscar);

    // Se recupera la hora hasta la que envíar las ordenes del 'diaBuscar'
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_SENT_DAY_LIMIT_HOUR.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_SENT_DAY_LIMIT_HOUR.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_SENT_DAY_LIMIT_HOUR.getKeyname());
    } // if
    String[] horaConfigurada = datosConfiguracion.getKeyvalue().split(":");
    LOG.debug("[loadConfiguration] horaBuscar: " + datosConfiguracion.getKeyvalue());

    // Día actual con la hora configurada
    Calendar calConfigurado = Calendar.getInstance();
    calConfigurado.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaConfigurada[0]));
    calConfigurado.set(Calendar.MINUTE, Integer.parseInt(horaConfigurada[1]));
    calConfigurado.set(Calendar.SECOND, Integer.parseInt(horaConfigurada[2]));
    LOG.debug("[loadConfiguration] calConfigurado: " + calConfigurado.toString());
    LOG.debug("[loadConfiguration] ahora: " + ahora.toString());

    if (ahora.before(calConfigurado)) {
      diaDesde = diaBuscar;
    } else {
      Calendar restar = Calendar.getInstance();
      restar.setTime(diaBuscar);
      restar.add(Calendar.DAY_OF_YEAR, 1);
      diaDesde = restar.getTime();
    } // else
    LOG.debug("[loadConfiguration] diaDesde: " + diaDesde);

    // Se recupera la contrapartida configurada
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV, Utilidad.DatosConfig.PTI_pti_idecc.getProcess(),
                                                                         Utilidad.DatosConfig.PTI_pti_idecc.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.PTI_pti_idecc.getKeyname());
    } // if
    // cfgCdcontra = datosConfiguracion.getKeyvalue();
    configParams.put("cfgCdcontra", datosConfiguracion.getKeyvalue().trim());

    // Se recupera la ruta donde dejar el fichero generado
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_output.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_output.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_output.getKeyname());
    } // if
    outputFilePath = datosConfiguracion.getKeyvalue().trim();

    // Se recupera el CIF de la SVB
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV, Utilidad.DatosConfig.SVB_cif.getProcess(),
                                                                         Utilidad.DatosConfig.SVB_cif.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuración para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.SVB_cif.getKeyname());
    } // if
    configParams.put("cif_svb", datosConfiguracion.getKeyvalue().trim());

    Tmct0xas datosTmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.COMPRA, Constantes.CLIENTEN);
    if (datosTmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de traducion para: [" + Constantes.CDTPOPER + "] [" + Constantes.COMPRA + "] ["
                                           + Constantes.CLIENTEN + "]");
    } // if
    // compra_clienten = datosTmct0xas.getInforas4();
    configParams.put("compra_clienten", datosTmct0xas.getInforas4().trim());

    datosTmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.VENTA, Constantes.CLIENTEN);
    if (datosTmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de traducion para: [" + Constantes.CDTPOPER + "] [" + Constantes.VENTA + "] ["
                                           + Constantes.CLIENTEN + "]");
    } // if
    // venta_clienten = datosTmct0xas.getInforas4();
    configParams.put("venta_clienten", datosTmct0xas.getInforas4().trim());

    // Tradución de la operación
    datosTmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.COMPRA, Constantes.BMEECC);
    if (datosTmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de traducion para: [" + Constantes.CDTPOPER + "] [" + Constantes.COMPRA + "] ["
                                           + Constantes.BMEECC + "]");
    } // if
    // compra_bmeecc = datosTmct0xas.getInforas4();
    configParams.put("compra_bmeecc", datosTmct0xas.getInforas4().trim());

    datosTmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.VENTA, Constantes.BMEECC);
    if (datosTmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de traducion para: [" + Constantes.CDTPOPER + "] [" + Constantes.VENTA + "] ["
                                           + Constantes.BMEECC + "]");
    } // if
    // venta_bmeecc = datosTmct0xas.getInforas4();
    configParams.put("venta_bmeecc", datosTmct0xas.getInforas4().trim());
    // tradución de la operación para los tipos de operacion de movimiento conciliacion
    datosTmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.COMPRA, Constantes.MERMOVCONC);
    if (datosTmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de traducion para: [" + Constantes.CDTPOPER + "] [" + Constantes.COMPRA + "] ["
                                           + Constantes.MERMOVCONC + "]");
    } // if
    // compra_mercado_mov = datosTmct0xas.getInforas4();
    configParams.put("compra_mercado_mov", datosTmct0xas.getInforas4().trim());
    datosTmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.VENTA, Constantes.MERMOVCONC);
    if (datosTmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de traducion para: [" + Constantes.CDTPOPER + "] [" + Constantes.VENTA + "] ["
                                           + Constantes.MERMOVCONC + "]");
    } // if
    // venta_mercado_mov = datosTmct0xas.getInforas4();
    configParams.put("venta_mercado_mov", datosTmct0xas.getInforas4().trim());
    LOG.debug("[loadConfiguration] Fin ...");
    return true;
  } // loadConfiguration

  /**
   * Abre el fichero para insertar los datos a enviar a Megara.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el fichero se ha abierto correctamente; <code>false</code> en caso contrario.
   * @throws FileNotFoundException Si no se encuentra el fichero donde escribir.
   * @throws UnsupportedEncodingException 
   */
  private void openOutputFile(Integer nuEnvio) throws FileNotFoundException, IOException, UnsupportedEncodingException {
    LOG.debug("[openOutputFile] Inicio ...");
    // Se comprueba si es nuevo dia para empezar con otro numero de envio
    Date dia_actual = new Date();
    Calendar cal_dia_actual = Calendar.getInstance();
    cal_dia_actual.setTime(dia_actual);

    outputFileName = Constantes.MEGARA_FILE_PREFIX + Utilidad.fillDate(dia_actual, "ddMMyyyy").trim() + "_" + String.valueOf(nuEnvio) + ".tmp";
    outputFile = new File(outputFilePath, outputFileName);
    if(outputFile.exists())
        throw new IOException(String.format("Fichero %s ya existe", outputFileName));
    prtWriter = new PrintWriter(outputFile, StandardCharsets.ISO_8859_1.name());

    LOG.debug("[openOutputFile] Fichero temporal '" + outputFilePath + "/" + outputFileName + "' creado");

    String hora_minuto = String.format("%02d:%02d", cal_dia_actual.get(Calendar.HOUR_OF_DAY), cal_dia_actual.get(Calendar.MINUTE));
    String cabecera = "00" + Utilidad.fillDate(dia_actual, "yyyyMMdd").trim() + hora_minuto;
    LOG.debug("[openOutputFile] Insertando cabecera: " + cabecera);

    prtWriter.print(cabecera);
    LOG.debug("[openOutputFile] Fin ...");
  } // openOutputFile

  /**
   * Cierra el fichero.
   * 
   * @param size_registros Número de registros a escribir.
   * @return
   * @throws IOException Si ocurre algún error al cerrar/eliminar el fichero.
   */
  private String closeOutputFile(Integer size_registros) throws IOException {
    LOG.debug("[closeOutputFile] Inicio ...");
    String result = null;
    // Si no hay ningún registro se borra el fichero
    if (size_registros == 0) {
      LOG.debug("[closeOutputFile] Eliminando fichero temporal: [" + outputFileName + "] por no haber registros a enviar");
      prtWriter.flush();
      prtWriter.close();
      if (!outputFile.delete()) {
        throw new IOException("Error al eliminar el fichero temporal: [" + outputFileName + "]");
      } // if
    } else {
      // Se inserta la línea de fin de fichero
      prtWriter.println();
      prtWriter.println("FIN99");
      prtWriter.flush();
      prtWriter.close();

      // Se renombra el fichero
      outputFileName = outputFileName.replace("tmp", "dat");
      File newOutputFile = new File(outputFilePath, outputFileName);
      if (!outputFile.renameTo(newOutputFile)) {
        throw new IOException("Error al renombrar el fichero: [" + outputFileName
                              + "]. Posiblemente el fichero ya exista. Renombre el existente y vuelva a probar");
      } // if
      result = outputFileName;
    } // else

    LOG.debug("[closeOutputFile] Fin ...");
    return result;
  } // closeOutputFile

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS DE UTILIDAD

  /**
   * Obtiene una lista con los días de vacaciones configurados en la tabla tmct0cfg.
   * 
   * @return <code>List<Date> - </code>Lista de días de vacaciones.
   */
  private List<Date> getHolidays() {
    if (holidaysConfigList == null) {
      // Días de vacaciones configurados
      List<Tmct0cfg> configList = tmct0cfgDao.findByAplicationAndProcessAndLikeKeyname("SBRMV", "CONFIG", "holydays.anual.days%");
      if (configList != null && !configList.isEmpty()) {
        holidaysConfigList = new ArrayList<Date>(configList.size());

        for (Tmct0cfg tmct0cfg : configList) {
          holidaysConfigList.add(FormatDataUtils.convertStringToDate(tmct0cfg.getKeyvalue(), "yyyy-MM-dd"));
        } // for
      } else {
        holidaysConfigList = new ArrayList<Date>();
      } // else
    } // if
    return holidaysConfigList;
  } // getHolidays

  /**
   * Obtiene una lista con los días de laborables configurados en la tabla tmct0cfg.
   * 
   * @return <code>List<Integer> - </code>Lista de días de la semana laborables.
   */
  private List<Integer> getWorkDaysOfWeek() {
    if (workDaysOfWeekConfigList == null) {
      // Días de la semana laborables configurados
      Tmct0cfg tmct0cfg = tmct0cfgDao.findByAplicationAndProcessAndKeyname("SBRMV", "CONFIG", "week.work.days");

      if (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && !tmct0cfg.getKeyvalue().isEmpty()) {
        String[] workDays = tmct0cfg.getKeyvalue().split(",");
        workDaysOfWeekConfigList = new ArrayList<Integer>(workDays.length);

        for (String workDay : workDays) {
          workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
        } // for
      } else {
        workDaysOfWeekConfigList = new ArrayList<Integer>();
      } // else
    } // if

    return workDaysOfWeekConfigList;
  } // getWorkDaysOfWeek

} // EnvioClearingNettingBo
