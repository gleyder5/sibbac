package sibbac.business.operativanetting.bean;

import java.math.BigDecimal;

/**
 * The Class MinimoImporteAlias.
 */
public class MinimoImporteAlias {
	
	/** The id. */
	private final Long id;

	/** The minimo. */
	private final BigDecimal minimo;

	/** The alias. */
	private final String alias;

	/**
	 * Instantiates a new minimo importe alias.
	 *
	 * @param id the id
	 * @param minimo the minimo
	 * @param alias the alias
	 */
	public MinimoImporteAlias(Long id, BigDecimal minimo, String alias) {
		super();
		this.id = id;
		this.minimo = minimo;
		this.alias = alias;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the minimo.
	 *
	 * @return the minimo
	 */
	public BigDecimal getMinimo() {
		return minimo;
	}

	/**
	 * Gets the alias.
	 *
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((minimo == null) ? 0 : minimo.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinimoImporteAlias other = (MinimoImporteAlias) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (minimo == null) {
			if (other.minimo != null)
				return false;
		} else if (!minimo.equals(other.minimo))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MinimoImporteAlias [id=" + id + ", minimo=" + minimo
				+ ", alias=" + alias + "]";
	}
	
	

}
