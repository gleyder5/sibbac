package sibbac.business.operativanetting.interes;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class EntradaCalculoInteresBean.
 */
public class EntradaCalculoInteresBean {
	
	/** The fecha desde. */
	private final Date fechaDesde;
	
	/** The fecha hasta. */
	private final Date fechaHasta;
	
	/** The alias. */
	private final String alias;
	
	/** The importe. */
	private final BigDecimal importe;

	/**
	 * Instantiates a new entrada calculo interes bean.
	 *
	 * @param fechaDesde the fecha desde
	 * @param fechaHasta the fecha hasta
	 * @param alias the alias
	 * @param importe the importe
	 */
	public EntradaCalculoInteresBean(Date fechaDesde, Date fechaHasta,
			String alias, BigDecimal importe) {
		super();
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.alias = alias;
		this.importe = importe;
	}

	/**
	 * Gets the fecha desde.
	 *
	 * @return the fecha desde
	 */
	public final Date getFechaDesde() {
		return fechaDesde;
	}

	/**
	 * Gets the fecha hasta.
	 *
	 * @return the fecha hasta
	 */
	public final Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * Gets the alias.
	 *
	 * @return the alias
	 */
	public final String getAlias() {
		return alias;
	}

	/**
	 * Gets the importe.
	 *
	 * @return the importe
	 */
	public final BigDecimal getImporte() {
		return importe;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result
				+ ((fechaDesde == null) ? 0 : fechaDesde.hashCode());
		result = prime * result
				+ ((fechaHasta == null) ? 0 : fechaHasta.hashCode());
		result = prime * result + ((importe == null) ? 0 : importe.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntradaCalculoInteresBean other = (EntradaCalculoInteresBean) obj;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (fechaDesde == null) {
			if (other.fechaDesde != null)
				return false;
		} else if (!fechaDesde.equals(other.fechaDesde))
			return false;
		if (fechaHasta == null) {
			if (other.fechaHasta != null)
				return false;
		} else if (!fechaHasta.equals(other.fechaHasta))
			return false;
		if (importe == null) {
			if (other.importe != null)
				return false;
		} else if (!importe.equals(other.importe))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EntradaCalculoInteresBean [fechaDesde=" + fechaDesde
				+ ", fechaHasta=" + fechaHasta + ", alias=" + alias
				+ ", importe=" + importe + "]";
	}
	
	

}
