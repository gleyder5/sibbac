package sibbac.business.operativanetting.database.dao;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.operativanetting.database.model.S3Intereses;



@Repository
public interface S3InteresesDao extends JpaRepository< S3Intereses, Long > {

		
	@Query ("SELECT I FROM S3Intereses I WHERE  I.cdalias = :cdalias and I.fechaLiquidacion <= :hasta AND I.netting is null AND I.fechaEnvio = NULL ORDER BY I.nbooking  ")
	public List< S3Intereses > findByFechaAlc(@Param( "cdalias") String cdalias, @Param( "hasta") Date hasta);
	
	@Query ("SELECT I FROM S3Intereses I WHERE  I.cdalias = :cdalias and I.fechaLiquidacion <= :hasta AND I.netting is not null AND I.fechaEnvio = NULL ")
	public List< S3Intereses > findByFechaNetting(@Param( "cdalias") String cdalias, @Param( "hasta") Date hasta);

	
	@Query("SELECT I FROM S3Intereses I "
			+ "WHERE ('N'=:filtroalias or I.cdalias=:cdalias) "
				+ "AND ('N'=:filtrofcontratacionDesde or I.fechaContratacion >= :fcontratacionDesde) "
				+ "AND ('N'=:filtrofcontratacionHasta or I.fechaContratacion <= :fcontratacionHasta) "
				+ "AND ('N'=:filtroimporteInteresDesde or I.importeInteres >= :importeInteresDesde) "
				+ "AND ('N'=:filtroimporteInteresHasta or I.importeInteres <= :importeInteresHasta) ")	
	public List<S3Intereses> findByConsulta(@Param( "cdalias") String cdalias, @Param( "filtroalias") String filtroAlias,
			@Param( "fcontratacionDesde") Date fcontratacionDe, @Param( "filtrofcontratacionDesde") String filtrofcontratacionDe,
			@Param( "fcontratacionHasta") Date fcontratacionHasta, @Param( "filtrofcontratacionHasta") String filtrofcontratacionA,
			@Param( "importeInteresDesde") BigDecimal importeInteresDesde,  @Param( "filtroimporteInteresDesde") String filtroimporteInteresDe,
			@Param( "importeInteresHasta") BigDecimal importeInteresHasta, @Param( "filtroimporteInteresHasta") String filtroimporteInteresHasta);
	
	@Query("SELECT I FROM S3Intereses I "
			+ "WHERE ('N'=:filtroalias or I.cdalias<>:cdalias) "
				+ "AND ('N'=:filtrofcontratacionDesde or I.fechaContratacion >= :fcontratacionDesde) "
				+ "AND ('N'=:filtrofcontratacionHasta or I.fechaContratacion <= :fcontratacionHasta) "
				+ "AND ('N'=:filtroimporteInteresDesde or I.importeInteres >= :importeInteresDesde) "
				+ "AND ('N'=:filtroimporteInteresHasta or I.importeInteres <= :importeInteresHasta) ")	
	public List<S3Intereses> findByConsultaNotAlias(@Param( "cdalias") String cdalias, @Param( "filtroalias") String filtroAlias,
			@Param( "fcontratacionDesde") Date fcontratacionDe, @Param( "filtrofcontratacionDesde") String filtrofcontratacionDe,
			@Param( "fcontratacionHasta") Date fcontratacionHasta, @Param( "filtrofcontratacionHasta") String filtrofcontratacionA,
			@Param( "importeInteresDesde") BigDecimal importeInteresDesde,  @Param( "filtroimporteInteresDesde") String filtroimporteInteresDe,
			@Param( "importeInteresHasta") BigDecimal importeInteresHasta, @Param( "filtroimporteInteresHasta") String filtroimporteInteresHasta);
	
	
}
