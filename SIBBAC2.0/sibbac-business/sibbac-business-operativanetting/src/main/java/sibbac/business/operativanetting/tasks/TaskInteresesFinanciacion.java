package sibbac.business.operativanetting.tasks;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.operativanetting.database.bo.S3InteresesBo;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Tarea que genera el informe de intereses de financiacion.
 * 
 * @see SIBBACTask
 * @author XI316153
 */
@SIBBACJob(group = Task.GROUP_CLEARING.NAME,
           name = Task.GROUP_CLEARING.JOB_INTERESES_FINANCIACION,
           interval = 1,
           intervalUnit = IntervalUnit.DAY,
           startTime = "21:00:00")
public class TaskInteresesFinanciacion extends OperativanettingTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskInteresesFinanciacion.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>S3Intereses</code>. */
  @Autowired
  private S3InteresesBo s3InteresesBo;

  @Autowired
  private GestorServicios gestorServicios;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.business.operativanetting.tasks.OperativanettingTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_CLEARING_INTERESES_FINANCIACION;
  } // determinarTipoApunte

  /*
   * @see sibbac.business.operativanetting.tasks.OperativanettingTaskConcurrencyPrevent#executeTask()
   */
  @Override
  public void executeTask() {
    final Long startTime = System.currentTimeMillis();
    LOG.debug("[execute] Iniciando la tarea de generacion del informe de intereses de financiacion ...");
    try {
      final Date fechaHasta = new Date();
      final List<Alias> listaAlias = gestorServicios.isActive(this.jobPk);
      if (CollectionUtils.isNotEmpty(listaAlias)) {
        for (final Alias alias : listaAlias) {
          s3InteresesBo.generarInformeIntereses(this.jobPk, alias, fechaHasta);
        } // for
      } // if
    } catch (Exception ex) {
      LOG.warn("[execute] Incidencia al ejecutar la tarea ... ", ex.getMessage());
      throw ex;
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[execute] Finalizada la tarea de generacion del informe de intereses de financiacion ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
               + "]");
    } // finally
  } // executeTask
} // TaskInteresesFinanciacion
