package sibbac.business.operativanetting.rest;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.operativanetting.bean.InteresesDemoraAlias;
import sibbac.business.operativanetting.bean.MinimoImporteAlias;
import sibbac.business.operativanetting.bean.ServiceFinanciacionBean;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.database.bo.InteresesDemoraBo;
import sibbac.business.operativanetting.database.bo.MinimoImporteBo;
import sibbac.business.operativanetting.database.bo.S3InteresesBo;
import sibbac.business.operativanetting.database.model.InteresesDemora;
import sibbac.business.operativanetting.database.model.MinimoImporte;
import sibbac.business.operativanetting.database.model.S3Intereses;
import sibbac.business.operativanetting.exceptions.DateContainedIntervalException;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.database.JobPK;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class SIBBACServiceFinanciacionIntereses.
 */
@SIBBACService
final class SIBBACServiceFinanciacionIntereses implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger	LOG				= LoggerFactory.getLogger( SIBBACServiceFinanciacionIntereses.class );

	/** The minimo importe bo. */
	@Autowired
	private MinimoImporteBo		minimoImporteBo;

	/** The intereses demora bo. */
	@Autowired
	private InteresesDemoraBo	interesesDemoraBo;

	/** The alias bo. */
	@Autowired
	private AliasBo				aliasBo;

	/** The S3intereses bo. */
	@Autowired
	private S3InteresesBo		interesesBo;

	/** The alc ordenes bo. */
	@Autowired
	private AlcOrdenesBo		alcOrdenesBo;

	/** The tmct0alc bo. */
	@Autowired
	private Tmct0alcBo			tmct0alcBo;

	/** The tmct0ord bo. */
	@Autowired
	private Tmct0ordBo			tmct0ordBo;

	/** The Constant MESSAGE_ERROR. */
	private final static String	MESSAGE_ERROR	= "No es posible guardar el intervalo. El intervalo que se intenta guardar no es compatible con los intervalos guardados en BBDD";

	/**
	 * The Enum ComandosFinanciacionIntereses.
	 */
	private enum ComandosFinanciacionIntereses {

		/** The get lista de intereses. */
		GET_LISTA_INTERESES( "getListaIntereses" ),

		/** The baja de intereses. */
		MARCAR_BAJA_INTERESES( "MarcarBajaIntereses" ),

		/** The get interes por defecto. */
		GET_INTERES_POR_DEFECTO( "getInteresPorDefecto" ), /**
		 * The get minimo por
		 * defecto.
		 */
		GET_MINIMO_POR_DEFECTO( "getMinimoPorDefecto" ),
		/** The save intereses. */
		SAVE_INTERESES( "saveIntereses" ),
		/** The save minimo. */
		SAVE_MINIMO( "saveMinimo" ),
		/** The get all intereses. */
		GET_ALL_INTERESES( "getAllIntereses" ),
		/** The get all minimos. */
		GET_ALL_MINIMOS( "getAllMinimos" ),
		/** The delete interes. */
		DELETE_INTERES( "deleteIntereses" ),

		/** The delete minimo. */
		DELETE_MINIMO( "deleteMinimo" ),
		/** The delete interes. */
		ENVIAR_CALCULO_INTERESES( "enviarCalculoIntereses" ),
		/** The sin comando. */
		SIN_COMANDO( "" );

		/** The command. */
		private String	command;

		/**
		 * Instantiates a new comandos financiacion intereses.
		 *
		 * @param command
		 *            the command
		 */
		private ComandosFinanciacionIntereses( String command ) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * The Enum FieldsFinanciacionIntereses.
	 */
	private enum FieldsFinanciacionIntereses {

		/** The fecha inicio. */
		FECHA_INICIO( "fechaInicio" ),
		/** The fecha hasta. */
		FECHA_HASTA( "fechaHasta" ),
		/** The intereses. */
		INTERESES( "intereses" ),
		/** The minimo importe. */
		MINIMO_IMPORTE( "minimoImporte" ),
		/** The alias. */
		ALIAS( "idAlias" ),
		/** The referencia. */
		REFERENCIA( "referencia" ),
		/** The sentido. */
		SENTIDO( "sentido" ),
		/** The isin. */
		ISIN( "isin" ),
		/** The nombreValor. */
		NOMBREVALOR( "nombreValor" ),
		/** The mercado. */
		MERCADO( "mercado" ),
		/** The titulos. */
		TITULOS( "titulos" ),
		/** The importeNeto. */
		IMPORTENETO( "importeNeto" ),
		/** The titular. */
		TITULAR( "titular" ),
		/** The fechaContratacion. */
		FECHACONTRATACION( "fechaContratacion" ),
		/** The fechaOperacion. */
		FECHAOPERACION( "fechaOperacion" ),
		/** The fechaLiquidacion. */
		FECHALIQUIDACION( "fechaLiquidacion" ),
		/** The importeliquidado. */
		IMPORTELIQUIDADO( "importeliquidado" ),
		/** The estado. */
		ESTADO( "estado" ),
		/** The cdalias. */
		CDALIAS( "cdalias" ),
		/** The idInteres. */
		IDINTERES( "idInteres" ),
		/** The tipoInteres. */
		TIPOINTERES( "tipoInteres" ),

		/** The base. */
		BASE( "base" );

		/** The field. */
		private String	field;

		/**
		 * Instantiates a new fields financiacion intereses.
		 *
		 * @param field
		 *            the field
		 */
		private FieldsFinanciacionIntereses( String field ) {
			this.field = field;
		}

		/**
		 * Gets the field.
		 *
		 * @return the field
		 */
		public String getField() {
			return field;
		}
	}

	/**
	 * Gets the command.
	 *
	 * @param command
	 *            the command
	 * @return the command
	 */
	private ComandosFinanciacionIntereses getCommand( String command ) {
		ComandosFinanciacionIntereses[] commands = ComandosFinanciacionIntereses.values();
		ComandosFinanciacionIntereses result = null;
		for ( int i = 0; i < commands.length; i++ ) {
			if ( commands[ i ].getCommand().equalsIgnoreCase( command ) ) {
				result = commands[ i ];
			}
		}
		if ( result == null ) {
			result = ComandosFinanciacionIntereses.SIN_COMANDO;
		}
		LOG.debug( new StringBuffer( "Se selecciona la accion: " ).append( result.getCommand() ).toString() );
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.
	 * WebRequest)
	 */
	/**
	 * Process.
	 *
	 * @param webRequest
	 *            the web request
	 * @return the web response
	 * @throws SIBBACBusinessException
	 *             the SIBBAC business exception
	 */
	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		Map< String, String > filters = webRequest.getFilters();
		List< Map< String, String >> parametros = webRequest.getParams();
		WebResponse result = new WebResponse();
		ComandosFinanciacionIntereses comandosFinanciacionIntereses = getCommand( webRequest.getAction() );
		Map< String, Object > resultados = new HashMap< String, Object >();
		try {
			switch ( comandosFinanciacionIntereses ) {
				case GET_LISTA_INTERESES:
					resultados = getListaIntereses( new ServiceFinanciacionBean( filters ) );
					break;
				case MARCAR_BAJA_INTERESES:
					resultados = MarcarBajaIntereses( parametros );
					break;
				case GET_INTERES_POR_DEFECTO:
					resultados = getInteresPorDefecto();
					break;
				case GET_MINIMO_POR_DEFECTO:
					resultados = getMinimoPorDefecto();
					break;
				case SAVE_INTERESES:
					resultados = saveIntereses( new ServiceFinanciacionBean( filters ) );
					break;
				case SAVE_MINIMO:
					resultados = saveMinimo( new ServiceFinanciacionBean( filters ) );
					break;
				case GET_ALL_INTERESES:
					resultados = getAllIntereses();
					break;
				case GET_ALL_MINIMOS:
					resultados = getAllMinimos();
					break;
				case DELETE_INTERES:
					resultados = deleteIntereses( new ServiceFinanciacionBean( filters ) );
					break;
				case DELETE_MINIMO:
					resultados = deleteMinimo( new ServiceFinanciacionBean( filters ) );
					break;
				case SIN_COMANDO:
					break;
				case ENVIAR_CALCULO_INTERESES:
					resultados = enviarCalculoIntereses( new ServiceFinanciacionBean( filters ) );
					break;
			}
			resultados.put( "status", "OK" );
		} catch ( DateContainedIntervalException e ) {
			resultados.put( "status", "KO" );
			result.setError( MESSAGE_ERROR );
		}
		result.setResultados( resultados );
		return result;
	}

	/**
	 * Delete minimo.
	 *
	 * @param serviceFinanciacionBean the service financiacion bean
	 * @return the map
	 */
	private Map< String, Object > deleteMinimo( ServiceFinanciacionBean serviceFinanciacionBean ) {
		Map< String, Object > result = new HashMap< String, Object >();
		minimoImporteBo.delete( serviceFinanciacionBean.getId() );
		return result;
	}

	/**
	 * Delete minimo.
	 *
	 * @param serviceFinanciacionBean the service financiacion bean
	 * @return the map
	 */
	private Map< String, Object > enviarCalculoIntereses( ServiceFinanciacionBean serviceFinanciacionBean ) {
		Map< String, Object > result = new HashMap< String, Object >();
		final String cdalias = serviceFinanciacionBean.getCdalias();
		final Alias alias = aliasBo.findByCdaliass( cdalias );
		final JobPK jobPk = new JobPK( Task.GROUP_CLEARING.NAME, Task.GROUP_CLEARING.JOB_INTERESES_FINANCIACION );
		interesesBo.generarInformeIntereses( jobPk, alias, new Date() );
		return result;
	}

	/**
	 * Delete intereses.
	 *
	 * @param serviceFinanciacionBean
	 *            the service financiacion bean
	 * @return the map
	 */
	private Map< String, Object > deleteIntereses( ServiceFinanciacionBean serviceFinanciacionBean ) {
		Map< String, Object > result = new HashMap< String, Object >();
		interesesDemoraBo.delete( serviceFinanciacionBean.getId() );
		return result;
	}

	/**
	 * Marcar baja intereses.
	 *
	 * @param params the params
	 * @return the map
	 */
	private Map< String, Object > MarcarBajaIntereses( List< Map< String, String >> params ) {

		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > map = new HashMap< String, Object >();
		Map< String, Object > bean;

		for ( Map< String, String > parametro : params ) {

			bean = new HashMap< String, Object >();
			// buscamos el id interes y lo marcamos como estado de Baja
			Long idInteres = new Long( parametro.get( FieldsFinanciacionIntereses.IDINTERES.getField() ) );

			S3Intereses datos_modif_intereses = interesesBo.findById( idInteres );
			if ( datos_modif_intereses == null ) {
				continue;
			}
			datos_modif_intereses.setCdestado( 'B' );
			datos_modif_intereses.setContabilizado( 'N' );
			bean.put( FieldsFinanciacionIntereses.IDINTERES.getField(), datos_modif_intereses.getId() );
			list.add( bean );

		}

		map.put( Constantes.RESULT_LISTA, list );

		return map;

	}

	/**
	 * Gets the lista intereses.
	 *
	 * @param serviceFinanciacionBean the service financiacion bean
	 * @return the lista intereses
	 */
	private Map< String, Object > getListaIntereses( ServiceFinanciacionBean serviceFinanciacionBean ) {

		Map< String, Object > result = new HashMap< String, Object >();
		
		Integer tipo = 1;
		String alias = serviceFinanciacionBean.getCdalias();
		if(alias != null && alias.indexOf("#") != -1){
			alias = alias.replace("#", "");
			tipo = 2;
		}
		Integer tipoEstado = 1;
		String estado = serviceFinanciacionBean.getEstado();
		if(estado != null && estado.indexOf("#") != -1){
			estado = estado.replace("#", "");
			tipoEstado = 2;
		}

		List< S3Intereses > lista_intereses = interesesBo.findByConsulta( serviceFinanciacionBean.getFechaInicio(),
				serviceFinanciacionBean.getFechaHasta(), serviceFinanciacionBean.getImporteInteresesDesde(),
				serviceFinanciacionBean.getImporteInteresesHasta(), alias, tipo );
			
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > bean;

		for ( S3Intereses dato_intereses : lista_intereses ) {

			// filtramos por la variable estado
			if ( serviceFinanciacionBean.getEstado() != null && !serviceFinanciacionBean.getEstado().trim().equals( "" )){
				if(tipoEstado == 1 &&  dato_intereses.getCdestado() != estado.charAt( 0 ))
					continue;

				if(tipoEstado == 2 && dato_intereses.getCdestado() == estado.charAt( 0 ))
					continue;
			}

			Tmct0alc datos_alc;

			bean = new HashMap< String, Object >();

			// Miramos si va por netting, en tal caso hay diferencia para
			// obtener algunos datos
			if ( dato_intereses.getNetting() != null ) {

				Netting datos_netting = dato_intereses.getNetting();
				// sacamos el primer alc del neteo porque los datos deberian ser
				// iguales
				List< AlcOrdenes > lista_alc_ordenes = alcOrdenesBo.findByNetting( datos_netting );
				if ( lista_alc_ordenes.size() > 0 ) {
				    Tmct0alcId alcId = lista_alc_ordenes.get( 0 ).createAlcIdFromFields();
					datos_alc = tmct0alcBo.findById(alcId);
					if(datos_alc == null) {
					    LOG.warn("[getListaIntereses] Se ha intentado referir a una alc no existente: {}", alcId);
					    continue;
					}
				} else {
					continue;
				}
				bean.put( FieldsFinanciacionIntereses.REFERENCIA.getField(), datos_netting.getId() );

			} else {

				datos_alc = tmct0alcBo.findByNbookingAndNucnfliqAndNucnfclt( dato_intereses.getNbooking(), dato_intereses.getNucnfliq(),
						dato_intereses.getNucnfclt() );
				bean.put( FieldsFinanciacionIntereses.REFERENCIA.getField(),
						dato_intereses.getNbooking() + "/" + dato_intereses.getNucnfclt() + "/" + dato_intereses.getNucnfliq() );
			}

			bean.put( FieldsFinanciacionIntereses.IDINTERES.getField(), dato_intereses.getId() );
			bean.put( FieldsFinanciacionIntereses.SENTIDO.getField(), "COMPRA" );
			bean.put( FieldsFinanciacionIntereses.ISIN.getField(), datos_alc.getTmct0alo().getCdisin() );
			bean.put( FieldsFinanciacionIntereses.TITULOS.getField(), dato_intereses.getNutitulo() );
			bean.put( FieldsFinanciacionIntereses.TITULAR.getField(), dato_intereses.getTitular() );
			bean.put( FieldsFinanciacionIntereses.FECHACONTRATACION.getField(), dato_intereses.getFechaContratacion() );
			bean.put( FieldsFinanciacionIntereses.FECHAOPERACION.getField(), dato_intereses.getFechaOperacion() );
			bean.put( FieldsFinanciacionIntereses.FECHALIQUIDACION.getField(), dato_intereses.getFechaLiquidacion() );
			bean.put( FieldsFinanciacionIntereses.TIPOINTERES.getField(), dato_intereses.getTipoInteres() );
			bean.put( FieldsFinanciacionIntereses.INTERESES.getField(), dato_intereses.getImporteInteres() );
			bean.put( FieldsFinanciacionIntereses.IMPORTELIQUIDADO.getField(), dato_intereses.getImporteNetoLiquidado() );
			bean.put( FieldsFinanciacionIntereses.ESTADO.getField(), dato_intereses.getCdestado() );
			bean.put( FieldsFinanciacionIntereses.CDALIAS.getField(), dato_intereses.getCdalias() );
			bean.put( FieldsFinanciacionIntereses.BASE.getField(), dato_intereses.getBase() );
			Tmct0ord datos_ord = tmct0ordBo.findByNuorden( datos_alc.getId().getNuorden() );
			bean.put( FieldsFinanciacionIntereses.MERCADO.getField(), datos_ord.getCdmercad() );

			list.add( bean );

		}

		result.put( Constantes.RESULT_LISTA, list );
		return result;
	}

	/**
	 * Gets the minimo por defecto.
	 *
	 * @return the minimo por defecto
	 */
	private Map< String, Object > getMinimoPorDefecto() {
		Map< String, Object > result = new HashMap< String, Object >();
		List< MinimoImporte > minimoImportes = minimoImporteBo.findByAlias( null );
		MinimoImporte minimoImporte;
		if ( minimoImportes.size() == 1 ) {
			minimoImporte = minimoImportes.get( 0 );
			result.put( FieldsFinanciacionIntereses.MINIMO_IMPORTE.getField(), minimoImporte.getMinimo() );
		}
		return result;
	}

	/**
	 * Gets the all intereses.
	 *
	 * @return the all intereses
	 */
	private Map< String, Object > getAllIntereses() {
		Map< String, Object > result = new HashMap< String, Object >();
		List< InteresesDemoraAlias > interesesDemoraAlias = interesesDemoraBo.findAllWithAlias();
		result.put( "intereses", interesesDemoraAlias );
		return result;
	}

	/**
	 * Save minimo.
	 *
	 * @param serviceFinanciacionBean
	 *            the service financiacion bean
	 * @return the map
	 */
	private Map< String, Object > saveMinimo( ServiceFinanciacionBean serviceFinanciacionBean ) {
		Map< String, Object > result = new HashMap< String, Object >();
		MinimoImporte minimoImporte = new MinimoImporte();
		Alias alias;
		if ( serviceFinanciacionBean.getIdAlias() != null ) {
			alias = aliasBo.findById( serviceFinanciacionBean.getIdAlias() );
			minimoImporte.setAlias( alias );
		} else {
			minimoImporte.setAlias( null );
		}
		minimoImporte.setMinimo( serviceFinanciacionBean.getMinimoImporte() );
		minimoImporte = minimoImporteBo.save( minimoImporte );

		return result;
	}

	/**
	 * Save intereses.
	 *
	 * @param serviceFinanciacionBean
	 *            the service financiacion bean
	 * @return the map
	 */
	private Map< String, Object > saveIntereses( ServiceFinanciacionBean serviceFinanciacionBean ) {
		Map< String, Object > result = new HashMap< String, Object >();
		InteresesDemora interesesDemora;

		interesesDemora = new InteresesDemora();
		Alias alias;
		if ( serviceFinanciacionBean.getIdAlias() != null ) {
			alias = aliasBo.findById( serviceFinanciacionBean.getIdAlias() );
			interesesDemora.setAlias( alias );
		} else {
			interesesDemora.setAlias( null );
		}
		interesesDemora.setIntereses( serviceFinanciacionBean.getIntereses() );
		interesesDemora.setFechaHasta( serviceFinanciacionBean.getFechaHasta() );
		interesesDemora.setFechaInicio( serviceFinanciacionBean.getFechaInicio() );
		interesesDemora.setBase( serviceFinanciacionBean.getBase() );
		interesesDemora = interesesDemoraBo.save( interesesDemora );
		return result;
	}

	/**
	 * Gets the minimo por defecto.
	 *
	 * @return the minimo por defecto
	 */
	private Map< String, Object > getAllMinimos() {
		Map< String, Object > result = new HashMap< String, Object >();
		List< MinimoImporteAlias > minimoImportes = minimoImporteBo.findAllWithAlias();
		result.put( "minimos", minimoImportes );
		return result;
	}

	/**
	 * Gets the interes por defecto.
	 *
	 * @return the interes por defecto
	 */
	private Map< String, Object > getInteresPorDefecto() {
		Map< String, Object > result = new HashMap< String, Object >();
		InteresesDemora interesesDemora;
		List< InteresesDemora > interesesDemoras = interesesDemoraBo.findByNowAndAlias( null, new Date() );
		if ( interesesDemoras.size() == 1 ) {
			interesesDemora = interesesDemoras.get( 0 );
			result.put( FieldsFinanciacionIntereses.FECHA_HASTA.getField(), interesesDemora.getFechaHasta() );
			result.put( FieldsFinanciacionIntereses.FECHA_INICIO.getField(), interesesDemora.getFechaInicio() );
			result.put( FieldsFinanciacionIntereses.INTERESES.getField(), interesesDemora.getIntereses() );
			result.put( FieldsFinanciacionIntereses.BASE.getField(), interesesDemora.getBase() );
		} else {
			result.put( FieldsFinanciacionIntereses.FECHA_HASTA.getField(), "" );
			result.put( FieldsFinanciacionIntereses.FECHA_INICIO.getField(), "" );
			result.put( FieldsFinanciacionIntereses.INTERESES.getField(), "" );
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
	 */
	/**
	 * Gets the available commands.
	 *
	 * @return the available commands
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > result = new ArrayList< String >();
		ComandosFinanciacionIntereses[] commands = ComandosFinanciacionIntereses.values();
		for ( int i = 0; i < commands.length; i++ ) {
			result.add( commands[ i ].getCommand() );
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	/**
	 * Gets the fields.
	 *
	 * @return the fields
	 */
	@Override
	public List< String > getFields() {
		List< String > result = new ArrayList< String >();
		FieldsFinanciacionIntereses[] fields = FieldsFinanciacionIntereses.values();
		for ( int i = 0; i < fields.length; i++ ) {
			result.add( fields[ i ].getField() );
		}
		return result;
	}

}
