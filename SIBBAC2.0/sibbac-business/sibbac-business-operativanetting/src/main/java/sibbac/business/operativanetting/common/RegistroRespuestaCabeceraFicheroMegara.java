package sibbac.business.operativanetting.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.utils.FormatDataUtils;

/**
 * @author XI316153
 */
public class RegistroRespuestaCabeceraFicheroMegara {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  protected static final Logger LOG = LoggerFactory.getLogger(RegistroRespuestaCabeceraFicheroMegara.class);

  /** Formato de las fechas que se leen del fichero. */
  private static final String FILE_DATE_FORMAT = "yyyyMMdd";

  /** Formato de la hora de liquidación real. */
  private static final String TS_FORMAT = "yyyyMMddHHmmss";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Tipo de operación. CHAR(6). */
  private String tipoOperacion;

  /** Número de instrucción de liquidación de S3. CHAR(35). */
  private String instruccionLiquidacion;

  /** Pool reference enviada a S3. CHAR(16). */
  private String poolReference;

  /** Código Isin. CHAR(12). */
  private String isin;

  /** Estado de la operación. CHAR(3). */
  private String estadoOperacion;

  /** Referencia de la operación enviada a S3. CHAR(20). */
  private String referenciaSV;

  /** Fecha de contratación. YYYYMMDD. */
  private Date fechaOperacion;

  /** Fecha real de liquidación. YYYYMMDD. */
  private Date fechaValor;

  /** Número de títulos liquidados. NUMBER(12, 0). */
  private BigDecimal titulosLiquidados;

  /** Importe neto liquidado. NUMBER(15, 2). */
  private BigDecimal importeNetoLiquidado;

  /** Número de bloques de detalle. NUMBER(6). */
  private Integer bloquesDestalle;

  /** Lista de bloques de detalle. */
  private List<RegistroRespuestaDetalleFicheroMegara> bloquesDestalleList;

  /** Datos de una operación fallida. */
  private RegistroRespuestaFallidoFicheroMegara operacionFallida;

  /** Hora real de liquidación. YYYYMMDDHHMMSS. */
  private Date horaValor;
  
  /** Línea original que ha sido parseada */
  private final String lineaOriginal;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Contruye un objeto de esta clase a partir de la cadena especificada.
   * 
   * @param linea Línea a procesar.
   */
  public RegistroRespuestaCabeceraFicheroMegara(String linea) {
    lineaOriginal = linea;
    tipoOperacion = linea.substring(0, 6).trim();
    instruccionLiquidacion = linea.substring(6, 41).trim();
    poolReference = linea.substring(41, 57).trim();
    isin = linea.substring(57, 69).trim();
    estadoOperacion = linea.substring(69, 72);
    referenciaSV = linea.substring(72, 92).trim();
    fechaOperacion = FormatDataUtils.convertStringToDate(linea.substring(92, 100), FILE_DATE_FORMAT);
    fechaValor = FormatDataUtils.convertStringToDate(linea.substring(100, 108), FILE_DATE_FORMAT);
    titulosLiquidados = new BigDecimal(linea.substring(108, 120));
    importeNetoLiquidado = new BigDecimal(linea.substring(120, 137).replaceFirst("^0+(?!$)", "")).movePointLeft(2);
    bloquesDestalle = Integer.valueOf(linea.substring(137, 143));

    if (bloquesDestalle > 0) {
      bloquesDestalleList = new ArrayList<RegistroRespuestaDetalleFicheroMegara>(bloquesDestalle);

      for (Integer i = 0; i < bloquesDestalle; i++) {
        RegistroRespuestaDetalleFicheroMegara registro_detalle = new RegistroRespuestaDetalleFicheroMegara(
                                                                                                           linea.substring(143,
                                                                                                                           327));
        bloquesDestalleList.add(registro_detalle);
      } // for (Integer i = 0; i < bloquesDestalle; i++)

      if (Constantes.CDEST_FALLIDA.compareTo(this.estadoOperacion) == 0) {
        operacionFallida = new RegistroRespuestaFallidoFicheroMegara(linea.substring(327, 518));
        horaValor = linea.substring(518, 532).trim().isEmpty() ? null
                                                              : FormatDataUtils.convertStringToDate(linea.substring(518,
                                                                                                                    532),
                                                                                                    TS_FORMAT);
      } else {
        horaValor = linea.substring(327, 341).trim().isEmpty() ? null
                                                              : FormatDataUtils.convertStringToDate(linea.substring(327,
                                                                                                                    341),
                                                                                                    TS_FORMAT);
      }
    } else {
      if (Constantes.CDEST_FALLIDA.compareTo(this.estadoOperacion) == 0) {
        operacionFallida = new RegistroRespuestaFallidoFicheroMegara(linea.substring(143, 334));
        horaValor = linea.substring(334, 348).trim().isEmpty() ? null
                                                              : FormatDataUtils.convertStringToDate(linea.substring(334,
                                                                                                                    348),
                                                                                                    TS_FORMAT);
      } else {
        horaValor = linea.substring(143, 157).trim().isEmpty() ? null
                                                              : FormatDataUtils.convertStringToDate(linea.substring(143,
                                                                                                                    157),
                                                                                                    TS_FORMAT);
      }
    } // else
  } // RegistroRespuestaCabeceraFicheroMegara

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "RegistroRespuestaCabeceraFicheroMegara [tipoOperacion=" + tipoOperacion + ", instruccionLiquidacion="
           + instruccionLiquidacion + ", poolReference=" + poolReference + ", isin=" + isin + ", estadoOperacion="
           + estadoOperacion + ", referenciaSV=" + referenciaSV + ", fechaOperacion=" + fechaOperacion
           + ", fechaValor=" + fechaValor + ", horaValor=" + horaValor + ", titulosLiquidados=" + titulosLiquidados
           + ", importeNetoLiquidado=" + importeNetoLiquidado + ", bloquesDestalle=" + bloquesDestalle + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the tipoOperacion
   */
  public String getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * @return the instruccionLiquidacion
   */
  public String getInstruccionLiquidacion() {
    return instruccionLiquidacion;
  }

  /**
   * @return the poolReference
   */
  public String getPoolReference() {
    return poolReference;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the estadoOperacion
   */
  public String getEstadoOperacion() {
    return estadoOperacion;
  }

  /**
   * @return the referenciaSV
   */
  public String getReferenciaSV() {
    return referenciaSV;
  }

  /**
   * @return the fechaOperacion
   */
  public Date getFechaOperacion() {
    return fechaOperacion;
  }

  /**
   * @return the fechaValor
   */
  public Date getFechaValor() {
    return fechaValor;
  }

  /**
   * @return the titulosLiquidados
   */
  public BigDecimal getTitulosLiquidados() {
    return titulosLiquidados;
  }

  /**
   * @return the importeNetoLiquidado
   */
  public BigDecimal getImporteNetoLiquidado() {
    return importeNetoLiquidado;
  }

  /**
   * @return the bloquesDestalle
   */
  public Integer getBloquesDestalle() {
    return bloquesDestalle;
  }

  /**
   * @return the bloquesDestalleList
   */
  public List<RegistroRespuestaDetalleFicheroMegara> getBloquesDestalleList() {
    return bloquesDestalleList;
  }

  /**
   * @return the operacionFallida
   */
  public RegistroRespuestaFallidoFicheroMegara getOperacionFallida() {
    return operacionFallida;
  }

  /**
   * @return the horaValor
   */
  public Date getHoraValor() {
    return horaValor;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @param tipoOperacion the tipoOperacion to set
   */
  public void setTipoOperacion(String tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * @param instruccionLiquidacion the instruccionLiquidacion to set
   */
  public void setInstruccionLiquidacion(String instruccionLiquidacion) {
    this.instruccionLiquidacion = instruccionLiquidacion;
  }

  /**
   * @param poolReference the poolReference to set
   */
  public void setPoolReference(String poolReference) {
    this.poolReference = poolReference;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param estadoOperacion the estadoOperacion to set
   */
  public void setEstadoOperacion(String estadoOperacion) {
    this.estadoOperacion = estadoOperacion;
  }

  /**
   * @param referenciaSV the referenciaSV to set
   */
  public void setReferenciaSV(String referenciaSV) {
    this.referenciaSV = referenciaSV;
  }

  /**
   * @param fechaOperacion the fechaOperacion to set
   */
  public void setFechaOperacion(Date fechaOperacion) {
    this.fechaOperacion = fechaOperacion;
  }

  /**
   * @param fechaValor the fechaValor to set
   */
  public void setFechaValor(Date fechaValor) {
    this.fechaValor = fechaValor;
  }

  /**
   * @param titulosLiquidados the titulosLiquidados to set
   */
  public void setTitulosLiquidados(BigDecimal titulosLiquidados) {
    this.titulosLiquidados = titulosLiquidados;
  }

  /**
   * @param importeNetoLiquidado the importeNetoLiquidado to set
   */
  public void setImporteNetoLiquidado(BigDecimal importeNetoLiquidado) {
    this.importeNetoLiquidado = importeNetoLiquidado;
  }

  /**
   * @param bloquesDestalle the bloquesDestalle to set
   */
  public void setBloquesDestalle(Integer bloquesDestalle) {
    this.bloquesDestalle = bloquesDestalle;
  }

  /**
   * @param bloquesDestalleList the bloquesDestalleList to set
   */
  public void setBloquesDestalleList(List<RegistroRespuestaDetalleFicheroMegara> bloquesDestalleList) {
    this.bloquesDestalleList = bloquesDestalleList;
  }

  /**
   * @param operacionFallida the operacionFallida to set
   */
  public void setOperacionFallida(RegistroRespuestaFallidoFicheroMegara operacionFallida) {
    this.operacionFallida = operacionFallida;
  }

  /**
   * @param horaValor the horaValor to set
   */
  public void setHoraValor(Date horaValor) {
    this.horaValor = horaValor;
  }
  
  /**
   * Recupera la línea original que se leyó desde el fichero
   * @return una cadena con la línea original.
   */
  public String getLineaOriginal() {
    return lineaOriginal;
  }

} // RegistroRespuestaCabeceraFicheroMegara
