package sibbac.business.operativanetting.database.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.operativanetting.bean.InteresesDemoraAlias;
import sibbac.business.operativanetting.database.dao.InteresesDemoraDao;
import sibbac.business.operativanetting.database.model.InteresesDemora;
import sibbac.business.operativanetting.exceptions.DateContainedIntervalException;
import sibbac.business.wrappers.database.model.Alias;

/**
 * The Class InteresesDemoraBo.
 */
@Service
public class InteresesDemoraBo extends
		AbstractDecoratorInterval<InteresesDemora, Long, InteresesDemoraDao> {

	/**
	 * Find by date and alias.
	 *
	 * @param alias the alias
	 * @param dateStart the date start
	 * @param dateEnd the date end
	 * @return the list
	 * @throws DateContainedIntervalException the date contained interval exception
	 */
	public List<InteresesDemora> findByDateStartAndDateEndAndAlias(Alias alias,
			Date dateStart, Date dateEnd) throws DateContainedIntervalException {
		List<InteresesDemora> result;
		List<InteresesDemora> interesesDemoras = findByAlias(alias);
		result = isInInterval(interesesDemoras, dateStart, dateEnd);
		return result;
	}

	/**
	 * Find all with alias.
	 *
	 * @return the list
	 */
	public List<InteresesDemoraAlias> findAllWithAlias() {
		List<InteresesDemoraAlias> result = new ArrayList<InteresesDemoraAlias>();
		List<InteresesDemora> interesesDemoras = dao.findAll();
		for (InteresesDemora interesesDemora : interesesDemoras) {
			if (interesesDemora.getAlias() == null) {
				result.add(new InteresesDemoraAlias(interesesDemora.getId(),interesesDemora
						.getFechaInicio(), interesesDemora.getFechaHasta(),
						interesesDemora.getIntereses(), "", interesesDemora.getBase()));
			} else {
				result.add(new InteresesDemoraAlias(interesesDemora.getId(),interesesDemora
						.getFechaInicio(), interesesDemora.getFechaHasta(),
						interesesDemora.getIntereses(), interesesDemora
								.getAlias().formarNombreAlias(), interesesDemora.getBase()));
			}
		}
		return result;
	}

	/**
	 * Find by now and alias.
	 *
	 * @param alias the alias
	 * @param now the now
	 * @return the list
	 * @throws DateContainedIntervalException the date contained interval exception
	 */
	public List<InteresesDemora> findByNowAndAlias(Alias alias, Date now)
			throws DateContainedIntervalException {
		List<InteresesDemora> result;
		List<InteresesDemora> interesesDemoras = findByAlias(alias);
		result = isInInterval(interesesDemoras, now);
		return result;
	}

	/* (non-Javadoc)
	 * @see sibbac.business.operativanetting.database.bo.AbstractDecoratorInterval#findByAlias(sibbac.business.wrappers.database.model.Alias)
	 */
	@Override
	public List<InteresesDemora> findByAlias(Alias alias) {
		List<InteresesDemora> result;
		if (alias == null) {
			result = dao.findAllByAliasIsNull();
		} else {
			result = dao.findAllByAlias(alias);
		}
		return result;
	}

}
