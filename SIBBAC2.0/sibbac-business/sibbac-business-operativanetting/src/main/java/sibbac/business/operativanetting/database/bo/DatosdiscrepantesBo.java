package sibbac.business.operativanetting.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.operativanetting.database.dao.DatosdiscrepantesDao;
import sibbac.business.operativanetting.database.model.Datosdiscrepantes;
import sibbac.database.bo.AbstractBo;


@Service
public class DatosdiscrepantesBo extends AbstractBo< Datosdiscrepantes, Long, DatosdiscrepantesDao > {

	public Datosdiscrepantes findByCodigo( String codigo ) {
		return dao.findByCodigo( codigo );
	}

}
