package sibbac.business.operativanetting.common;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import sibbac.business.wrappers.database.model.Tmct0desglosecamara;

final class AgrupacionPataMercado implements Serializable {

  /**
   * Número de serie generado automáticamente
   */
  private static final long serialVersionUID = -5745486146089188726L;

  private final Long idCamaraCompensacion;

  private final String cdCtaCompensacion;
  
  private final String cdCuentaS3;
  
  private final Date fechaLiquidacion;

  AgrupacionPataMercado(Tmct0desglosecamara desglose, String cdCuentaS3) {
      Objects.requireNonNull(desglose, "Se ha proporcionado un desglose camara nulo"); 
      this.idCamaraCompensacion = Objects.requireNonNull(desglose.getTmct0CamaraCompensacion(), 
              "Se ha proporcionado un desglose cámara sin camara de compensación").getIdCamaraCompensacion();
      this.cdCtaCompensacion = desglose.getCuentaCompensacion();
      this.cdCuentaS3 = Objects.requireNonNull(cdCuentaS3, "Se ha proporcionado una cuenta S3 nula");
      this.fechaLiquidacion = Objects.requireNonNull(desglose.getFeliquidacion(), 
          "Se ha proporcinado una fecha liquidación nula");
  }
  
  @Override
  public boolean equals(final Object o) {
    final AgrupacionPataMercado other;

    if (o == this)
      return true;
    if (o == null)
      return false;
    if (getClass() != o.getClass())
      return false;
    other = (AgrupacionPataMercado) o;
    return Objects.equals(idCamaraCompensacion, other.idCamaraCompensacion)
           && Objects.equals(cdCtaCompensacion, other.cdCtaCompensacion)
           && Objects.equals(cdCuentaS3, other.cdCuentaS3) 
           && Objects.equals(fechaLiquidacion, other.fechaLiquidacion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idCamaraCompensacion, cdCtaCompensacion, cdCuentaS3, fechaLiquidacion);
  }

  @Override
  public String toString() {
    return String.format("%d#%s#%s#%s", idCamaraCompensacion, cdCtaCompensacion, cdCuentaS3, 
        fechaLiquidacion.toString());
  }

  Long getIdCamaraCompensacion() {
    return idCamaraCompensacion;
  }

  String getCdCtaCompensacion() {
      return cdCtaCompensacion;
  }
  
  String getCdCuentaS3() {
      return cdCuentaS3;
  }
  
  Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

}
