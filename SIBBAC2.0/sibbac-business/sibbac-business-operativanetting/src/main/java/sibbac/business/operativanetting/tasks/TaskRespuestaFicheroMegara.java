package sibbac.business.operativanetting.tasks;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.operativanetting.database.bo.RespuestaClearingNettingBo;
import sibbac.business.operativanetting.exceptions.LoadConfigurationException;
import sibbac.business.operativanetting.exceptions.RecepcionFicheroMegaraException;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_CLEARING.NAME,
           name = Task.GROUP_CLEARING.JOB_FICHERO_RESPUESTA_MEGARA,
           interval = 30,
           intervalUnit = IntervalUnit.MINUTE)
public class TaskRespuestaFicheroMegara extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskRespuestaFicheroMegara.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0cfg</code>. */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  /** Business object para la tabla <code>Tmct0xas</code>. */
  @Autowired
  private Tmct0xasBo tmct0xasBo;

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0estadoBo tmct0estadoBo;

  /** Business object del proceso. */
  @Autowired
  private RespuestaClearingNettingBo respuestaClearingNettingBo;

  /** Inicio del intervalo horario durante el que se puede ejecutar el proceso. */
  private Integer cfgIntervalStartTime;

  /** Final del intervalo horario durante el que se puede ejecutar el proceso. */
  private Integer cfgIntervalEndTime;

  /** Parámetros de configuración del proceso. */
  private HashMap<String, String> configParams = null;

  /** Estados que se utilizan durante el proceso. */
  private HashMap<String, Tmct0estado> estadosMap = null;

  /** Lista de tipos de ficheros que se deben procesar. */
  private List<String> tiposOperacion;

  /** Ruta de los ficheros pendientes de procesar. */
  private String inputFilePath;
  
  /** Ruta de los ficheros de lineas rechazadas */
  private String rejectedFilePath;

  /** Calendar con la hora limite para empezar a actualizar el campo Nuenvio. */
  private Calendar calLimiteLiq;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void execute() {
    final Long startTime = System.currentTimeMillis();
    Tmct0men tmct0men;
    Calendar calFichero;
    File rejectedFile;

    LOG.debug("[execute] Iniciando la tarea de procesado de los ficheros recibidos de Megara ...");
    configParams = new HashMap<String, String>();
    estadosMap = new HashMap<String, Tmct0estado>();
    tiposOperacion = new ArrayList<String>();
    try {
      tmct0men = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_CLEARING_RESPUESTA_FICHERO_MEGARA, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);
      try {
        if (loadConfiguration()) {
          Integer horaActual = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

          // Si se está en el intervalo de ejecución de la tarea se ejecuta la lógica de la misma
          if (cfgIntervalStartTime <= horaActual && horaActual <= cfgIntervalEndTime) {

            File[] listaFicherosPendientes = buscarFicheros(tmct0men.getCdauxili().trim());
            if (listaFicherosPendientes != null && listaFicherosPendientes.length > 0) {
              for (File fichero : listaFicherosPendientes) {
                LOG.debug("[execute] Procesando fichero: " + fichero.getName());
                try {
                  if (fichero.isFile()) {
                    rejectedFile = new File(rejectedFilePath, fichero.getName());
                    calFichero = respuestaClearingNettingBo.ejecutarRespuestaFichero(fichero, rejectedFile, 
                        configParams, estadosMap, tiposOperacion);
                    if (fichero.getName().contains("NEW")) {
                      if (calLimiteLiq.before(calFichero)) {
                        LOG.debug("[execute] Fichero con hora posterior a las 15:00:00 de hoy. Se incrementa Nuenvio ...");
                        tmct0menBo.updateFhauditAndFemensaAndHrmensaAndIncrementNuenvio(tmct0men, calFichero);
                      } else {
                        LOG.debug("[execute] Fichero con hora posterior a las 15:00:00 de hoy. No se incrementa Nuenvio ...");
                        tmct0menBo.updateFhauditAndFemensaAndHrmensaAndResetNuenvio(tmct0men, calFichero);
                      } // else
                    } else {
                      tmct0menBo.updateFhauditMEN(TIPO_TAREA.TASK_CLEARING_RESPUESTA_FICHERO_MEGARA);
                    }
                  } else {
                    LOG.debug("[execute] El elemento no es un fichero. Se omite su procesamiento");
                    tmct0menBo.updateFhauditMEN(TIPO_TAREA.TASK_CLEARING_RESPUESTA_FICHERO_MEGARA);
                  } // else
                } catch (Exception e) {
                  throw e;
                } // catch
              } // for (File fichero : listaFicherosPendientes)
            } // if (listaFicherosPendientes != null && listaFicherosPendientes.length > 0)
          } else {
            LOG.info("[execute] La tarea no se ejecuta por no encontrarnos en el intervalo horario para su ejecucion");
          } // else
          LOG.info("[execute] Tarea terminada correctamente");
        } else {
          LOG.info("[execute] La configuracion no permite la ejecucion de la tarea");
        } // else

        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.LISTO_GENERAR);

      } catch (LoadConfigurationException lce) {
        LOG.warn("[execute] Incidencia al ejecutar la tarea ... ", lce.getMessage());
        LOG.debug("[execute] Pila de la incidencia: ", lce);
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } catch (RecepcionFicheroMegaraException rfme) {
        LOG.warn("[execute] Incidencia al ejecutar la logica de recepcion ... ", rfme.getMessage());
        LOG.debug("[execute] Pila de la incidencia: ", rfme);
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } catch (Exception e) {
        LOG.warn("[execute] Incidencia al ejecutar la tarea ... ", e.getMessage());
        LOG.debug("[execute] Pila de la incidencia: ", e);
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } // catch
    } catch (SIBBACBusinessException e) {
      LOG.warn("[execute] Error intentando bloquear el semaforo ... " + e.getMessage());
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[execute] Finalizada la tarea de recepción del fichero de Megara ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally
  } // execute

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Carga los datos de configuración del proceso.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si los datos de configuración se cargaron correctamente; <code>false</code> en caso
   *         contrario.
   * @throws LoadConfigurationException Si ocurre algún error al cargar la configuración o no se encuentra alguna propiedad.
   */
  private Boolean loadConfiguration() throws LoadConfigurationException {
    LOG.debug("[loadConfiguration] Cargando configuracion del proceso ...");

    Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                                  Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getProcess(),
                                                                                  Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag: "
                                           + Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getKeyname());
    } // if

    if (datosConfiguracion.getKeyvalue().equals(SibbacEnums.SI_NO.NO) || datosConfiguracion.getKeyvalue().equals("")) {
      LOG.warn("[loadConfiguration] El flag del proceso de generacion del fichero de Megara no esta activo: "
               + Utilidad.DatosConfig.MEGARA_AUTOUPDATE_FLAGLIQ.getKeyname());
      return Boolean.FALSE;
    } // if

    // Se recupera el inicio del intervalo horario durante el que se puede ejecutar el proceso
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_begin_hour_megara_nacional.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_begin_hour_megara_nacional.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag: "
                                           + Utilidad.DatosConfig.MEGARA_NACIONAL_begin_hour_megara_nacional.getKeyname());
    } // if
    cfgIntervalStartTime = Integer.valueOf(datosConfiguracion.getKeyvalue());

    // Se recupera el fin del intervalo horario durante el que se puede ejecutar el proceso
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_end_hour_megara_nacional.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_NACIONAL_end_hour_megara_nacional.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag: "
                                           + Utilidad.DatosConfig.MEGARA_NACIONAL_end_hour_megara_nacional.getKeyname());
    } // if
    cfgIntervalEndTime = Integer.valueOf(datosConfiguracion.getKeyvalue());

    // Se recupera la ruta de los ficheros a procesar
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_input.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_input.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuracion para el flag: "
                                           + Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_input.getKeyname());
    } // if
    inputFilePath = datosConfiguracion.getKeyvalue().trim();
    rejectedFilePath = new StringBuilder(inputFilePath).append(System.getProperty("file.separator")).append("erroneos")
        .toString();
    // Se carga el calendario con la hora limite para actualizar el campo Nuenvio
    calLimiteLiq = Calendar.getInstance();
    calLimiteLiq.set(Calendar.HOUR_OF_DAY, Integer.parseInt("15"));
    calLimiteLiq.set(Calendar.MINUTE, Integer.parseInt("00"));
    calLimiteLiq.set(Calendar.SECOND, Integer.parseInt("00"));
    calLimiteLiq.set(Calendar.MILLISECOND, Integer.parseInt("000"));
    LOG.debug("[loadConfiguration] Dia/hora limite liquidacion: " + calLimiteLiq.getTime());

    // Se recupera la ruta del fichero donde se movera como ya tratado
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV,
                                                                         Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getProcess(),
                                                                         Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuración para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getKeyname());
    } // if
    // inputFilePathTratado = datosConfiguracion.getKeyvalue().trim();
    configParams.put("inputFilePathTratado", datosConfiguracion.getKeyvalue().trim());

    // Se recupera el CIF de la SVB
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(Constantes.SBRMV, Utilidad.DatosConfig.SVB_cif.getProcess(),
                                                                         Utilidad.DatosConfig.SVB_cif.getKeyname());
    if (datosConfiguracion == null) {
      throw new LoadConfigurationException("No hay datos de configuración para el flag del proceso Megara: "
                                           + Utilidad.DatosConfig.SVB_cif.getKeyname());
    } // if
    configParams.put("cif_svb", datosConfiguracion.getKeyvalue().trim());

    Tmct0xas tmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.COMPRA, Constantes.CLIMOVCONC);
    if (tmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de tradución para: [" + Constantes.CDTPOPER + "] [" + Constantes.COMPRA + "] ["
                                           + Constantes.CLIMOVCONC + "]");
    }
    configParams.put("compra_cliente_mov", tmct0xas.getInforas4().trim());

    tmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.VENTA, Constantes.CLIMOVCONC);
    if (tmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de tradución para: [" + Constantes.CDTPOPER + "] [" + Constantes.VENTA + "] ["
                                           + Constantes.CLIMOVCONC + "]");
    }
    configParams.put("venta_cliente_mov", tmct0xas.getInforas4().trim());

    // Tradución de la operación para los tipos de operacion de movimiento conciliacion
    tmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.COMPRA, Constantes.MERMOVCONC);
    if (tmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de tradución para: [" + Constantes.CDTPOPER + "] [" + Constantes.COMPRA + "] ["
                                           + Constantes.MERMOVCONC + "]");
    } // if
    configParams.put("compra_mercado_mov", tmct0xas.getInforas4().trim());

    tmct0xas = tmct0xasBo.findByNameAndInforAndNbcamas(Constantes.CDTPOPER, Constantes.VENTA, Constantes.MERMOVCONC);
    if (tmct0xas == null) {
      throw new LoadConfigurationException("No hay datos de tradución para: [" + Constantes.CDTPOPER + "] [" + Constantes.VENTA + "] ["
                                           + Constantes.MERMOVCONC + "]");
    } // if
    configParams.put("venta_mercado_mov", tmct0xas.getInforas4().trim());

    // Estados a utilizar durante el proceso
    try {
      estadosMap.put("estadoClearingAceptadaAnulacion", tmct0estadoBo.findById(CLEARING.ACEPTADA_ANULACION.getId()));
      estadosMap.put("estadoClearingRechazadaAnulacion", tmct0estadoBo.findById(CLEARING.RECHAZADA_ANULACION.getId()));
      estadosMap.put("estadoClearingPendienteCase", tmct0estadoBo.findById(CLEARING.PDTE_CASE.getId()));
      estadosMap.put("estadoClearingNoCasada", tmct0estadoBo.findById(CLEARING.NO_CASADA_FICHERO.getId()));
      estadosMap.put("estadoClearingCasadaPdteLiq", tmct0estadoBo.findById(CLEARING.CASADA_PDTE_LIQUIDACION.getId()));
      estadosMap.put("estadoClearingLiqFallida", tmct0estadoBo.findById(CLEARING.LIQUIDACION_FALLIDA.getId()));
      estadosMap.put("estadoClearingLiqParcial", tmct0estadoBo.findById(CLEARING.LIQUIDADA_PARCIAL.getId()));
      estadosMap.put("estadoClearingLiq", tmct0estadoBo.findById(CLEARING.LIQUIDADA.getId()));
      estadosMap.put("estadoClearingLiqPropia", tmct0estadoBo.findById(CLEARING.LIQUIDADA_PROPIA.getId()));
      estadosMap.put("estadoAsigCancelada", tmct0estadoBo.findById(ASIGNACIONES.RECHAZADA.getId()));
      estadosMap.put("estadoAsigEnCurso", tmct0estadoBo.findById(ASIGNACIONES.EN_CURSO.getId()));
      estadosMap.put("estadoAsigPdteLiq", tmct0estadoBo.findById(ASIGNACIONES.PDTE_LIQUIDAR.getId()));
      estadosMap.put("estadoAsigLiqPropia", tmct0estadoBo.findById(ASIGNACIONES.LIQUIDADA_PROPIA.getId()));
      estadosMap.put("estadoAsigLiq", tmct0estadoBo.findById(ASIGNACIONES.LIQUIDADA.getId()));
    } catch (Exception e) {
      throw new LoadConfigurationException("Error consultado estados a utilizar durante el proceso en base de datos " + e.getMessage());
    } // catch

    tiposOperacion.add("AJEFE");
    tiposOperacion.add("AJEFR");
    tiposOperacion.add("AJTIE");
    tiposOperacion.add("AJTIR");
    tiposOperacion.add("APLIC");
    tiposOperacion.add("CANPE");
    tiposOperacion.add("CANPSE");
    tiposOperacion.add("CAPL");
    tiposOperacion.add("CAUI");
    tiposOperacion.add("COLLC");
    tiposOperacion.add("COLLP");
    tiposOperacion.add("COMDER");
    tiposOperacion.add("COMP");
    tiposOperacion.add("COPV");
    tiposOperacion.add("COUI");
    tiposOperacion.add("CTITE");
    tiposOperacion.add("CTITR");
    tiposOperacion.add("ECLAUX");
    tiposOperacion.add("EDRIP");
    tiposOperacion.add("EFISIC");
    tiposOperacion.add("EIFAUX");
    tiposOperacion.add("ETIT");
    tiposOperacion.add("ETITU");
    tiposOperacion.add("INIPE");
    tiposOperacion.add("MTFE");
    tiposOperacion.add("MTFR");
    tiposOperacion.add("NCOMP");
    tiposOperacion.add("NVENT");
    tiposOperacion.add("OTCE");
    tiposOperacion.add("OTCR");
    tiposOperacion.add("RCLAUX");
    tiposOperacion.add("RIFAUX");
    tiposOperacion.add("RFISIC");
    tiposOperacion.add("RTIT");
    tiposOperacion.add("RTITU");
    tiposOperacion.add("VAPL");
    tiposOperacion.add("VDER");
    tiposOperacion.add("VENT");
    tiposOperacion.add("VRET");
    tiposOperacion.add("VOPA");

    return Boolean.TRUE;
  } // loadConfiguration

  /**
   * Busca los ficheros a tratar.
   * 
   * @param cdauxili Comienzo del nombre de los ficheros a tratar.
   * @return Array de ficheros a tratar.
   * @throws RecepcionFicheroMegaraException Si hay ficheros con error en la carpeta de ficheros a tratar.
   */
  private File[] buscarFicheros(final String cdauxili) throws RecepcionFicheroMegaraException {
    LOG.debug("[buscarFicheros] Inicio ...");

    File[] listaFicherosEncontrados = null;
    File searchFolder = new File(inputFilePath);

    if (searchFolder.isDirectory()) {
      LOG.debug("[buscarFicheros] Buscando ficheros que comiencen por: " + cdauxili);

      if (searchFolder.listFiles(new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
          return name.startsWith(cdauxili) && name.contains(Constantes.ERROR_FILE_SUFFIX);
        } // accept
      }).length == 0) {

        listaFicherosEncontrados = searchFolder.listFiles(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
            return name.startsWith(cdauxili);
          } // accept
        });

        Arrays.sort(listaFicherosEncontrados, new MegaraFilenameComparator());

        LOG.debug("[buscarFicheros] Ficheros a procesar ...");
        for (int i = 0; i < listaFicherosEncontrados.length; i++) {
          LOG.debug("[buscarFicheros] Posicion '{}' -> '{}'", (i + 1), listaFicherosEncontrados[i]);
        } // for
      } else {
        throw new RecepcionFicheroMegaraException("La carpeta de ficheros recibidos contiene ficheros con errores");
      } // else
    } else {
      LOG.error("[buscarFicheros] La ruta no es un directorio: {}", inputFilePath);
    } // else

    LOG.debug("[buscarFicheros] Fin ...");
    return listaFicherosEncontrados;
  } // buscarFicheros

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INNER CLASSES

  /**
   * Comparador para ordenar por la fecha y hora que traen en el nombre los ficheros recibidos de Megara.<br>
   * OPELIQ_SVBMN_NEW_0038_29032016_135404.
   * 
   * @author XI316153
   */
  private class MegaraFilenameComparator implements Comparator<File> {
    String[] nombreFicheroSplit = null;
    String fechaFichero = null;
    String horaFichero = null;
    Calendar calFile1 = Calendar.getInstance();
    Calendar calFile2 = Calendar.getInstance();

    /*
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(File file1, File file2) {

      // File 1
      nombreFicheroSplit = file1.getName().split("_");
      fechaFichero = nombreFicheroSplit[nombreFicheroSplit.length - 2];
      horaFichero = nombreFicheroSplit[nombreFicheroSplit.length - 1].split("\\.")[0];

      if (file1.getName().contains("NEW")) {
        calFile1.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.ES_DATE_FORMAT));
      } else if (file1.getName().contains("FINAL")) {
        calFile1.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.DATE_FORMAT));
      } else {
        fechaFichero = horaFichero;
        calFile1.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.ES_DATE_FORMAT));
        horaFichero = "000000";
      }
      calFile1.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaFichero.substring(0, 2)));
      calFile1.set(Calendar.MINUTE, Integer.valueOf(horaFichero.substring(2, 4)));
      calFile1.set(Calendar.SECOND, Integer.valueOf(horaFichero.substring(4, 6)));

      // File 2
      nombreFicheroSplit = file2.getName().split("_");
      fechaFichero = nombreFicheroSplit[nombreFicheroSplit.length - 2];
      horaFichero = nombreFicheroSplit[nombreFicheroSplit.length - 1].split("\\.")[0];

      if (file2.getName().contains("NEW")) {
        calFile2.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.ES_DATE_FORMAT));
      } else if (file2.getName().contains("FINAL")) {
        calFile2.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.DATE_FORMAT));
      } else {
        fechaFichero = horaFichero;
        calFile2.setTime(FormatDataUtils.convertStringToDate(fechaFichero, FormatDataUtils.ES_DATE_FORMAT));
        horaFichero = "000000";
      }
      calFile2.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaFichero.substring(0, 2)));
      calFile2.set(Calendar.MINUTE, Integer.valueOf(horaFichero.substring(2, 4)));
      calFile2.set(Calendar.SECOND, Integer.valueOf(horaFichero.substring(4, 6)));

      return calFile1.compareTo(calFile2);
    } // compare
  } // MegaraFilenameComparator

} // TaskRespuestaFicheroMegara
