package sibbac.business.operativanetting.database.bo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.operativanetting.database.dao.S3InteresesDao;
import sibbac.business.operativanetting.database.model.MinimoImporte;
import sibbac.business.operativanetting.database.model.S3Intereses;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.bo.TextosIdiomasBo;
import sibbac.business.wrappers.database.dao.AlcOrdenesDao;
import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0ordDao;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.XLSInteresesS3;
import sibbac.common.export.excel.ExcelBuilder;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.JobPK;

@Service
public class S3InteresesBo extends AbstractBo< S3Intereses, Long, S3InteresesDao > {

	private static final String	TOTAL_INTERES_BOOKING	= "Total importe de intéreses por operación";
	private static final String	TOTAL_INTERES_ALIAS		= "Total importe de intéreses";
	@Autowired
	private Tmct0alcDao			DaoTmct0alc;
	@Autowired
	private Tmct0ordDao			DaoTmct0ord;
	@Autowired
	private AlcOrdenesDao		DaoAlcOrdenes;
	@Autowired
	private MinimoImporteBo		BoMinimoImporte;
	@Autowired
	private TextosIdiomasBo		textoIdiomaBo;
	@Autowired
	private ExcelBuilder		excelBuilder;
	@Autowired
	private GestorServicios		gestorServicios;
	@Autowired
	private SendMail			sendMail;
	@Autowired
	private EtiquetasDao		etiquetasDao;

	public List< S3Intereses > findByConsulta( Date fcontratacionDe, Date fcontratacionHasta, BigDecimal importeInteresDesde,
			BigDecimal importeInteresHasta, String cdalias, Integer tipo ) {

		String filtroAlias = "S";
		if ( cdalias == null || cdalias.equals( "" ) ) {
			filtroAlias = "N";
		}

		String filtrofcontratacionDe = "S";
		if ( fcontratacionDe == null ) {
			filtrofcontratacionDe = "N";
		}
		String filtrofcontratacionA = "S";
		if ( fcontratacionHasta == null ) {
			filtrofcontratacionA = "N";
		}
		String filtroimporteInteresDe = "S";
		if ( importeInteresDesde == null ) {
			filtroimporteInteresDe = "N";
		}
		String filtroimporteInteresHasta = "S";
		if ( importeInteresHasta == null ) {
			filtroimporteInteresHasta = "N";
		}

		if(tipo==1){
			return dao.findByConsulta( cdalias, filtroAlias, fcontratacionDe, filtrofcontratacionDe, fcontratacionHasta, filtrofcontratacionA,
					importeInteresDesde, filtroimporteInteresDe, importeInteresHasta, filtroimporteInteresHasta );
		}else{
			return dao.findByConsultaNotAlias( cdalias, filtroAlias, fcontratacionDe, filtrofcontratacionDe, fcontratacionHasta, filtrofcontratacionA,
					importeInteresDesde, filtroimporteInteresDe, importeInteresHasta, filtroimporteInteresHasta );			
		}
	}

	/**
	 * @param fechaHasta
	 * @param alias
	 */
	public void generarInformeIntereses( final JobPK jobPk, final Alias alias, final Date fechaHasta ) {
		GenerarInformeInteresesALC( jobPk, alias, fechaHasta );
		GenerarInformeInteresesNetting( jobPk, alias, fechaHasta );
	}

	@Transactional
	public void GenerarInformeInteresesALC( JobPK jobPk, Alias alias, Date hasta ) {

		final List< XLSInteresesS3 > lineas_excel = new ArrayList< XLSInteresesS3 >();
		String nbooking_tratando = "";
		BigDecimal intereses_totales = BigDecimal.ZERO;
		BigDecimal intereses_total_booking = BigDecimal.ZERO;

		LOG.info( "[S3InteresesBo::GenerarInformeInteresesFacturacionALC] Buscando intereses de financiación para el Alias: "
				+ alias.formarNombreAlias() );
		List< S3Intereses > lista_intereses = dao.findByFechaAlc( alias.getCdaliass().trim(), hasta );

		// Inicializamos los campos con el primer valor
		if ( lista_intereses.size() > 0 ) {
			nbooking_tratando = lista_intereses.get( 0 ).getNbooking().trim();
		}

		for ( S3Intereses dato_intereses : lista_intereses ) {

			XLSInteresesS3 linea = new XLSInteresesS3();

			Tmct0alc datos_alc = DaoTmct0alc.findByNbookingAndNucnfliqAndNucnfclt( dato_intereses.getNbooking(),
					dato_intereses.getNucnfliq(), dato_intereses.getNucnfclt() );
			Tmct0ord datos_ord = DaoTmct0ord.findByNuorden( datos_alc.getId().getNuorden() );

			linea.setNuestraReferencia( dato_intereses.getNbooking() + "/" + dato_intereses.getNucnfclt() + "/"
					+ dato_intereses.getNucnfliq() );

			LOG.trace( "[S3InteresesBo::GenerarInformeInteresesFacturacionALC] Encontrado intereses con la referencia: "
					+ linea.getNuestraReferencia() );

			IncluirLineaDatosComunes( linea, dato_intereses, datos_alc, datos_ord );

			intereses_totales = intereses_totales.add( dato_intereses.getImporteInteres() );
			intereses_total_booking = intereses_total_booking.add( dato_intereses.getImporteInteres() );

			lineas_excel.add( linea );

			// Como va ordenado por alias y luego por nbooking cuando cambia el nbooking tenemos que añadir una fila
			if ( !nbooking_tratando.equals( dato_intereses.getNbooking() ) ) {
				linea = new XLSInteresesS3();
				linea.setNuestraReferencia( TOTAL_INTERES_BOOKING );
				linea.setInteresAplicado( intereses_total_booking.doubleValue() );
				lineas_excel.add( linea );
				intereses_total_booking = BigDecimal.ZERO;
			}

		}

		if ( lista_intereses.size() > 0 ) {
			// comprobamos si supera el importe minimo del alias y se genera el excel para mandar el email
			if ( ValidarImporteMinimo( alias, intereses_totales, lineas_excel, lista_intereses ) ) {
				enviarCorreo( jobPk, alias, lineas_excel );
			}
		} else {
			LOG.info( "[S3InteresesBo::GenerarInformeInteresesNetting] No se han encontrado ningun interes para el alias: "
					+ alias.formarNombreAlias() );
		}

	}

	@Transactional
	public void GenerarInformeInteresesNetting( JobPK jobPk, Alias alias, Date hasta ) {

		final List< XLSInteresesS3 > lineas_excel = new ArrayList< XLSInteresesS3 >();
		BigDecimal intereses_totales = BigDecimal.ZERO;

		LOG.info( "[S3InteresesBo::GenerarInformeInteresesNetting] Buscando intereses de financiación para el Alias: "
				+ alias.formarNombreAlias() );
		List< S3Intereses > lista_intereses = dao.findByFechaNetting( alias.getCdaliass().trim(), hasta );

		for ( S3Intereses dato_intereses : lista_intereses ) {

			Netting datos_netting = dato_intereses.getNetting();

			// sacamos el primer alc del neteo porque los datos deberian ser igualies
			List< AlcOrdenes > lista_alc_ordenes = DaoAlcOrdenes.findByNetting( datos_netting );
			if ( lista_alc_ordenes.size() > 0 ) {

				XLSInteresesS3 linea = new XLSInteresesS3();

				Tmct0alcId alcId = lista_alc_ordenes.get( 0 ).createAlcIdFromFields();
				Tmct0alc datos_alc = DaoTmct0alc.findOne(alcId);
				if(datos_alc == null) {
				    LOG.warn("[GenerarInformeInteresesNetting] Se ha solicitado un alc por clave inexistente: {}", 
				            alcId);
				    continue;
				}
				Tmct0ord datos_ord = DaoTmct0ord.findByNuorden( datos_alc.getId().getNuorden() );

				linea.setNuestraReferencia( String.valueOf( dato_intereses.getNetting().getId() ) );
				LOG.trace( "[S3InteresesBo::GenerarInformeInteresesNetting] Encontrado intereses con la referencia: "
						+ linea.getNuestraReferencia() );

				IncluirLineaDatosComunes( linea, dato_intereses, datos_alc, datos_ord );

				lineas_excel.add( linea );
			}
		}

		if ( lista_intereses.size() > 0 ) {
			// comprobamos si supera el importe minimo del alias y se genera el excel para mandar el email
			if ( ValidarImporteMinimo( alias, intereses_totales, lineas_excel, lista_intereses ) ) {
				enviarCorreo( jobPk, alias, lineas_excel );
			}
		} else {
			LOG.info( "[S3InteresesBo::GenerarInformeInteresesNetting] No se han encontrado ningun interes para el alias: "
					+ alias.formarNombreAlias() );
		}

	}

	private void IncluirLineaDatosComunes( XLSInteresesS3 linea, S3Intereses dato_intereses, Tmct0alc datos_alc, Tmct0ord datos_ord ) {

		linea.setFechaFiquidacion( FormatDataUtils.convertDateToString( dato_intereses.getFechaLiquidacion(),
				FormatDataUtils.DATE_FORMAT_SEPARATOR ) );
		linea.setfechaContratacion( FormatDataUtils.convertDateToString( dato_intereses.getFechaContratacion(),
				FormatDataUtils.DATE_FORMAT_SEPARATOR ) );
		linea.setFechaOperacion( FormatDataUtils.convertDateToString( dato_intereses.getFechaOperacion(),
				FormatDataUtils.DATE_FORMAT_SEPARATOR ) );
		linea.setImporteNetoLiquidado( dato_intereses.getImporteNetoLiquidado().doubleValue() );
		linea.setInteresAplicado( dato_intereses.getImporteInteres().doubleValue() );
		linea.setIsin( datos_alc.getTmct0alo().getCdisin() );
		linea.setMercado( datos_ord.getCdmercad() );
		linea.setPrecioMedioOrden( datos_alc.getTmct0alo().getImcamnet().doubleValue() );
		linea.setTipoInteres( dato_intereses.getTipoInteres().doubleValue() );
		linea.setTitular( dato_intereses.getTitular() );
		linea.setTotalTitulos( dato_intereses.getNutitulo().doubleValue() );
		linea.setNombreValor( datos_alc.getTmct0alo().getNbvalors() );
		linea.setDifereciaDias( ( int ) ( dato_intereses.getFechaLiquidacion().getTime() - dato_intereses.getFechaOperacion().getTime() )
				/ ( 1000 * 60 * 60 * 24 ) );

		// actualizamos la fecha de envio para no volver a enviar los intereses
		dato_intereses.setFechaEnvio( new Date() );
		dao.save( dato_intereses );

	}

	private boolean ValidarImporteMinimo( Alias alias, BigDecimal intereses_totales, List< XLSInteresesS3 > lineas_excel,
			List< S3Intereses > lista_intereses ) {

		// añadimos una ultima linea con el total
		XLSInteresesS3 linea = new XLSInteresesS3();
		linea.setNuestraReferencia( TOTAL_INTERES_ALIAS );
		linea.setInteresAplicado( intereses_totales.doubleValue() );
		lineas_excel.add( linea );

		// Comprobamos si supera el minimo de intereses
		List< MinimoImporte > lista_minimo_importe = BoMinimoImporte.findByAlias( alias );
		BigDecimal minimo_importe = BigDecimal.ZERO;
		if ( lista_minimo_importe.size() > 0 ) {
			minimo_importe = lista_minimo_importe.get( 0 ).getMinimo();
			LOG.debug( "[S3InteresesBo::ValidarImporteMinimo] Encontrado minimo para el alias: [" + String.valueOf( minimo_importe ) + "]" );
		} else {
			// al no tener un importe especifico por alias se busca el generico
			lista_minimo_importe = BoMinimoImporte.findByAlias( null );
			if ( lista_minimo_importe.size() > 0 ) {
				minimo_importe = lista_minimo_importe.get( 0 ).getMinimo();
				LOG.debug( "[S3InteresesBo::ValidarImporteMinimo] Encontrado minimo por defecto: [" + String.valueOf( minimo_importe )
						+ "]" );

			} else {
				LOG.warn( "[S3InteresesBo::ValidarImporteMinimo] No se han encontrado intereses minimos por defecto" );
			}
		}
		int resultado = minimo_importe.compareTo( intereses_totales );
		// si los intereses son superiores al minimo enctonces actualizamos la fecha de envio y posteriormente mandamos el email
		if ( resultado == -1 ) {
			LOG.debug( "[S3InteresesBo::ValidarImporteMinimo] Los intereses totales '" + intereses_totales.toString()
					+ "' + supera los intereses minimos: [" + String.valueOf( minimo_importe ) + "]" );
			return true;
		} else {
			LOG.debug( "[S3InteresesBo::ValidarImporteMinimo] Los intereses totales '" + intereses_totales.toString()
					+ "' + no se supera los intereses minimos: [" + String.valueOf( minimo_importe ) + "]" );
			// Entonces hay que actualizar el estado a D (descartado) y el contabalizado a N
			for ( S3Intereses dato_intereses_modif : lista_intereses ) {
				dato_intereses_modif.setCdestado( 'D' );
				dato_intereses_modif.setContabilizado( 'N' );
			}
			return false;
		}

	}

	public void enviarCorreo( final JobPK jobPk, Alias alias, final List< XLSInteresesS3 > filas_intereses ) {

		// Generamos el excel
		InputStream inputExcel = crearExcel( filas_intereses, alias );

		final List< Contacto > contactos = gestorServicios.getContactos( alias, jobPk );

		final List< String > destinatarios = new ArrayList< String >();

		for ( Contacto destinatario : contactos ) {
			String[] mail = destinatario.getEmail().split( ";" );
			for ( int i = 0; i < mail.length; i++ ) {
				destinatarios.add( mail[ i ].trim() );
			}
		}

		// Nombre del fichero destino (<Nombre>.<extension>)
		final List< String > nameDest = new ArrayList< String >();
		nameDest.add( "FinanciacionIntereses.xls" );

		// InputStream con datos de entrada
		final List< InputStream > inputstream = new ArrayList< InputStream >();
		inputstream.add( inputExcel );

		// Montamos el mapping con los datos que se van a sustituir en el cuerpo
		// del Mail

		Map< String, String > mapping = createMappingCuerpoMail( alias );

		// Recuperamos el body de base de datos
		String body = textoIdiomaBo.recuperarTextoIdioma( "CUERPOINTERESES", alias.getIdioma() );
		body = reemplazarKeys( body, mapping );

		String asunto = textoIdiomaBo.recuperarTextoIdioma( "ASUNTOINTERESES", alias.getIdioma() );
		asunto = reemplazarKeys( asunto, mapping );

		try {
			LOG.trace( "[S3InteresesBo::enviarCorreo] Enviando el email para el alias: " + alias.getCdaliass().trim() );
			sendMail.sendMail( destinatarios, asunto, body, inputstream, nameDest, null );
		} catch ( MailSendException | IllegalArgumentException | MessagingException | IOException e ) {
			LOG.error( "Error enviando el correo de facturas", e.getCause() );
		}
	}

	private InputStream crearExcel( List< XLSInteresesS3 > lineas_excel, final Alias alias ) {

		LOG.trace( "[S3InteresesBo::crearExcel] Generando el excel para el alias: " + alias.getCdaliass().trim() );
		InputStream inputExcel = null;

		final Map< String, String > lineasCabeceras = textoIdiomaBo.recuperarCabecerasExcelIntereses( alias.getCodigoIdioma() );
		try {
			final ByteArrayOutputStream out = excelBuilder.generaInformeIntereses( Utilidad.fillDate( new Date(), "ddMMyyyy" ).trim() + "_"
					+ alias.getCdaliass().trim(), lineas_excel, lineasCabeceras );
			inputExcel = new ByteArrayInputStream( out.toByteArray() );

		} catch ( Exception e ) {
			LOG.error( "Error generando un EXCEL" + e );
		}
		return inputExcel;

	}

	public Map< String, String > createMappingCuerpoMail( Alias alias ) {
		String key = null;
		String value = null;
		Map< String, String > mapping = new HashMap< String, String >();
		try {
			List< Etiquetas > etiquetas = ( List< Etiquetas > ) etiquetasDao.findAllByTabla( "Intereses" );
			for ( Etiquetas etiqueta : etiquetas ) {
				key = etiqueta.getEtiqueta();
				String campo = etiqueta.getCampo();
				switch ( campo ) {
					case "NombreAlias":
						value = alias.formarNombreAlias();
						break;
					default:
						value = "<No data>";
				}
				mapping.put( key, value );
			}
		} catch ( Exception e ) {
			// Nothing
		}
		return mapping;
	}

	public String reemplazarKeys( String body, Map< String, String > mapping ) {
		Set< String > keys = mapping.keySet();
		String value = null;
		for ( String key : keys ) {
			value = mapping.get( key ).trim();
			body = body.replace( key, value );
		}
		return body;
	}

}
