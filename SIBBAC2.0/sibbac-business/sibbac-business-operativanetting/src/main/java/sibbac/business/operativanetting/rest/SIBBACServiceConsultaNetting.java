package sibbac.business.operativanetting.rest;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.operativanetting.common.Constantes;
import sibbac.business.operativanetting.common.Utilidad;
import sibbac.business.wrappers.database.bo.NettingBo;
import sibbac.business.wrappers.database.bo.S3LiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceConsultaNetting implements SIBBACServiceBean {

	private static final Logger	LOG				= LoggerFactory.getLogger( SIBBACServiceConsultaNetting.class );

	@Autowired
	private NettingBo			BoNetting		= null;

	@Autowired
	private Tmct0alcBo			BoTmct0alc		= null;

	@Autowired
	private Tmct0estadoBo		BoTmct0estado	= null;

	@Autowired
	private Tmct0aliBo			BoTmct0ali		= null;

	@Autowired
	private Tmct0ValoresBo		BoTmct0Valores	= null;

	@Autowired
	private S3LiquidacionBo		BoS3Liquidacion	= null;

	@Override
	public WebResponse process( WebRequest webRequest ) {

		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
		LOG.trace( "Command: " + command );
		Map< String, String > filters = webRequest.getFilters();
		List< Map< String, String >> parametros = webRequest.getParams();

		switch ( command ) {

			case Constantes.CMD_MARCAR_GASTOS:

				webResponse.setResultados( MarcarGastos( filters, parametros ) );
				break;

			case Constantes.CMD_CONSULTA_NETTING_SIN_MOV_TIT:

				webResponse.setResultados( getConsultaNetting( filters, BigDecimal.ZERO ) );
				break;

			case Constantes.CMD_CONSULTA_NETTING:

				webResponse.setResultados( getConsultaNetting( filters, null ) );
				break;

			case Constantes.CMD_ALIAS_NETTING:

				webResponse.setResultados( getAlias( true ) );
				break;

			case Constantes.CMD_TODOS_ALIAS:

				webResponse.setResultados( getAlias( false ) );
				break;

			case Constantes.CMD_TODOS_ISIN:

				webResponse.setResultados( getIsin() );
				break;

			default:
				webResponse.setError( "An action must be set. Valid actions are: " + command );

		}

		return webResponse;
	}

	private Map< String, Object > getAlias( Boolean ClienteNetting ) {

		List< String > lista_alias;
		String id, descripcion;

		if ( ClienteNetting ) {
			lista_alias = BoTmct0ali.findAliasNetting();
		} else {
			lista_alias = BoTmct0ali.findAlias();
		}

		Map< String, Object > map = new HashMap< String, Object >();
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();

		for ( String alias : lista_alias ) {
			id = alias.substring( 0, alias.indexOf( "|" ) ).trim();
			descripcion = alias.substring( alias.indexOf( "|" ) + 1 ).trim();
			Map< String, Object > bean = new HashMap< String, Object >();
			bean.put( "id", id );
			bean.put( "descripcion", descripcion );
			list.add( bean );
		}

		map.put( Constantes.RESULT_LISTA, list );
		return map;
	}

	private Map< String, Object > getIsin() {

		List< String > lista_isin = BoTmct0Valores.findIsin();
		String id, descripcion;

		Map< String, Object > map = new HashMap< String, Object >();
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();

		for ( String isin : lista_isin ) {
			id = isin.substring( 0, isin.indexOf( "|" ) ).trim();
			descripcion = isin.substring( isin.indexOf( "|" ) + 1 ).trim();
			Map< String, Object > bean = new HashMap< String, Object >();
			bean.put( "id", id );
			bean.put( "descripcion", descripcion );
			list.add( bean );
		}

		map.put( Constantes.RESULT_LISTA, list );
		return map;
	}

	private Map< String, Object > MarcarGastos( Map< String, String > filters, List< Map< String, String >> params ) {

		BigDecimal gastos = BigDecimal.ZERO;
		if ( filters.get( "gastos" ) != null ) {
			gastos = new BigDecimal( filters.get( "gastos" ) );
		} else {
			return null;
		}
		// TODO REVISAR ALBERTO.
		Tmct0estado estado_liquidada_cliente = BoTmct0estado.findById( CLEARING.LIQUIDADA.getId() );

		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > map = new HashMap< String, Object >();
		Map< String, Object > bean;

		for ( Map< String, String > parametro : params ) {

			bean = new HashMap< String, Object >();
			// buscamos el netting y lo marcamos como contabilizado
			Long idNetting = new Long( parametro.get( "idNetting" ) );
			Netting datos_modif_netting = BoNetting.findById( idNetting );
			if ( datos_modif_netting == null ) {
				continue;
			}

			datos_modif_netting.setLiquidado( 'S' );
			datos_modif_netting.setGastos( gastos );

			// buscamos los alc asociados al netting para actualizar el estado
			List< Tmct0alc > lista_datos_alc = BoTmct0alc.findByNetting( datos_modif_netting );
			for ( Tmct0alc datos_alc : lista_datos_alc ) {
				datos_alc.setEstadoentrec( estado_liquidada_cliente );
				BoTmct0alc.save( datos_alc );
			}

			bean.put( "idnetting", datos_modif_netting.getId() );

			list.add( bean );

		}

		map.put( Constantes.RESULT_LISTA, list );

		return map;
	}

	private Map< String, Object > getConsultaNetting( Map< String, String > params, BigDecimal titulos ) {

		String alias = "";
		if ( params.get( "alias" ) != null ) {
			alias = params.get( "alias" );
		}
		String isin = "";
		if ( params.get( "isin" ) != null ) {
			isin = params.get( "isin" );
		}
		
		Integer tipo;
		if(alias.indexOf("#") != -1 && isin.indexOf("#") != -1){
			alias = alias.replace("#", "");
			isin = isin.replace("#", "");
			tipo = 4;
		}else if(alias.indexOf("#") != -1){
			alias = alias.replace("#", "");
			tipo = 3;
		}else if(isin.indexOf("#") != -1){
			isin = isin.replace("#", "");
			tipo = 2;
		}else{
			tipo = 1;
		}
		
		
		String liquidado = "";
		if ( params.get( "liquidado" ) != null ) {
			liquidado = params.get( "liquidado" );
		}
		Date fcontratacionDe = null;
		if ( params.get( "fcontratacionDe" ) != null && !params.get( "fcontratacionDe" ).equals( "" ) ) {
			fcontratacionDe = Utilidad.getFechaFormateada( params.get( "fcontratacionDe" ) );
		}
		Date fcontratacionA = null;
		if ( params.get( "fcontratacionA" ) != null && !params.get( "fcontratacionA" ).equals( "" ) ) {
			fcontratacionA = Utilidad.getFechaFormateada( params.get( "fcontratacionA" ) );
		}
		Date fliquidacionDe = null;
		if ( params.get( "fliquidacionDe" ) != null && !params.get( "fliquidacionDe" ).equals( "" ) ) {
			fliquidacionDe = Utilidad.getFechaFormateada( params.get( "fliquidacionDe" ) );
		}
		Date fliquidacionA = null;
		if ( params.get( "fliquidacionA" ) != null && !params.get( "fliquidacionA" ).equals( "" ) ) {
			fliquidacionA = Utilidad.getFechaFormateada( params.get( "fliquidacionA" ) );
		}

		List< Netting > lista_netting = BoNetting.findByConsulta( titulos, fcontratacionDe, fcontratacionA, fliquidacionDe, fliquidacionA, alias, isin, tipo );

		
		Map< String, Object > map = new HashMap< String, Object >();
		List< Map< String, Object >> list = new ArrayList< Map< String, Object >>();
		Map< String, Object > bean;

		for ( Netting netting : lista_netting ) {

			// filtramos por la variable liquidacion
			if ( !liquidado.equals( "" ) && netting.getLiquidado() != ( liquidado.charAt( 0 ) ) ) {
				continue;
			}

			String descrali = "";
			Tmct0ali datos_alias = BoTmct0ali.findByFirstAliasOrder( netting.getCdalias().trim() );
			if ( datos_alias != null ) {
				descrali = datos_alias.getDescrali().trim();
			}

			bean = new HashMap< String, Object >();
			bean.put( "idnetting", netting.getId() );
			bean.put( "fechaoperacion", netting.getFechaOperacion() );
			bean.put( "fechacontratacion", netting.getFechaContratacion() );
			bean.put( "cdalias", netting.getCdalias() );
			bean.put( "descrali", descrali );
			bean.put( "camaracompensacion", netting.getCamaraCompensacion() );
			bean.put( "nbvalors", netting.getNbvalors() );
			bean.put( "cdisin", netting.getCdisin() );

			if ( titulos != null && titulos == BigDecimal.ZERO ) {
				bean.put( "nutiulototales", netting.getNutituloCompra() );// sacamos la compra pero la venta es el mismo valor (hemos
																			// buscado los titulos neteados a 0)
			} else {
				bean.put( "nutiulototales", netting.getNutituloNeteado() );
			}

			bean.put( "efectivoneteado", netting.getEfectivoNeteado() );
			bean.put( "corretajeneteado", netting.getCorretajeNeteado() );
			bean.put( "contabilizado", netting.getContabilizado() );
			bean.put( "liquidado", netting.getLiquidado() );
			bean.put( "fechacontabilizado", netting.getFechaContabilidad() );

			Integer n_liq = BoS3Liquidacion.findByNetting( netting ).size();
			bean.put( "n_liquidaciones", n_liq );

			list.add( bean );

		}

		map.put( Constantes.RESULT_LISTA, list );
		return map;
	}

	@Override
	public List< String > getAvailableCommands() {

		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_CONSULTA_NETTING_SIN_MOV_TIT );
		commands.add( Constantes.CMD_CONSULTA_NETTING );
		commands.add( Constantes.CMD_MARCAR_GASTOS );
		commands.add( Constantes.CMD_ALIAS_NETTING );
		commands.add( Constantes.CMD_TODOS_ALIAS );
		return commands;

	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();

		return fields;
	}

}
