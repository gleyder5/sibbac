package sibbac.business.interfacesemitidas.dto;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Registro que contiene los bloques para cada factura emitida
 * 
 * @author adrian.prause
 *
 */
public class SecuenciaFacturaEmitidaDTO {

	private DatosFacturaEmitidaDTO bloque01;
	private ClaveRegimenDTO bloque02;
	private FacturaRectificadaDTO bloque04;
	private DeslgoseFacturasSujetasExentasDTO bloque06;
	private DeslgoseFacturasSujetasNoExentasDTO bloque07;
	private DivisionGeograficaDTO bloque09;

	public SecuenciaFacturaEmitidaDTO(DatosFacturaEmitidaDTO bloque01, ClaveRegimenDTO bloque02,
			FacturaRectificadaDTO bloque04, DeslgoseFacturasSujetasExentasDTO bloque06,
			DeslgoseFacturasSujetasNoExentasDTO bloque07, DivisionGeograficaDTO bloque09) {
		super();
		this.bloque01 = bloque01;
		this.bloque02 = bloque02;
		this.bloque04 = bloque04;
		this.bloque06 = bloque06;
		this.bloque07 = bloque07;
		this.bloque09 = bloque09;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(bloque01).append(StringHelper.SALTO_DE_LINEA);
		s.append(bloque02).append(StringHelper.SALTO_DE_LINEA);
		if (bloque04 != null) {
			s.append(bloque04).append(StringHelper.SALTO_DE_LINEA);
		}
		if (bloque06 != null) {
			s.append(bloque06).append(StringHelper.SALTO_DE_LINEA);
		}
		if (bloque07 != null) {
			s.append(bloque07).append(StringHelper.SALTO_DE_LINEA);
		}
		s.append(bloque09);
		return s.toString();
	}

	public DatosFacturaEmitidaDTO getBloque01() {
		return bloque01;
	}

	public void setBloque01(DatosFacturaEmitidaDTO bloque01) {
		this.bloque01 = bloque01;
	}

	public ClaveRegimenDTO getBloque02() {
		return bloque02;
	}

	public void setBloque02(ClaveRegimenDTO bloque02) {
		this.bloque02 = bloque02;
	}

	public DeslgoseFacturasSujetasExentasDTO getBloque06() {
		return bloque06;
	}

	public void setBloque06(DeslgoseFacturasSujetasExentasDTO bloque06) {
		this.bloque06 = bloque06;
	}

	public DivisionGeograficaDTO getBloque09() {
		return bloque09;
	}

	public void setBloque09(DivisionGeograficaDTO bloque09) {
		this.bloque09 = bloque09;
	}

	public FacturaRectificadaDTO getBloque04() {
		return bloque04;
	}

	public void setBloque04(FacturaRectificadaDTO bloque04) {
		this.bloque04 = bloque04;
	}

	public DeslgoseFacturasSujetasNoExentasDTO getBloque07() {
		return bloque07;
	}

	public void setBloque07(DeslgoseFacturasSujetasNoExentasDTO bloque07) {
		this.bloque07 = bloque07;
	}

}
