package sibbac.business.interfacesemitidas.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.dto.pantallas.BusquedaRespuestaErrGCDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.RespuestaErroresExpedidasFilterDTO;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;

/**
 * Data access object Impl de Tmct0ExpedidasFiles.
 */
@Repository
public class Tmct0ExpedidasFilesDaoImp {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ExpedidasFilesDaoImp.class);
	
	@PersistenceContext
	private EntityManager	em;
	
	/**
	 * 	Utilizado para la busqueda de Respuesta de errores GC.
	 * 	@param rErrExpFilterDTO rErrExpFilterDTO
	 * 	@return List<BusquedaRespuestaErrGCDTO> List<BusquedaRespuestaErrGCDTO>
	 * 	@throws SIBBACBusinessException SIBBACBusinessException 
	 */
	public List<BusquedaRespuestaErrGCDTO> findRespuestaErroresGC(
		RespuestaErroresExpedidasFilterDTO rErrExpFilterDTO) throws SIBBACBusinessException {
		
		List<BusquedaRespuestaErrGCDTO> respuestaficheros = new ArrayList<BusquedaRespuestaErrGCDTO>();
		Query query;
		StringBuffer qlString = new StringBuffer( "SELECT " );
		qlString.append( 
				" fil.id, fil.nbFichero, fil.tpFichero, fil.fhEnviRecep, "
				+ " fil.estado, fil.asociacion" );
		qlString.append( " FROM bsnbpsql.TMCT0_EXPEDIDAS_FILES fil" );
		qlString.append( " WHERE 1 = 1 " );
		
		if (StringUtils.isNotBlank(rErrExpFilterDTO.getNombreFichero())) {
			qlString.append( " AND fil.nbFichero = :nbFichero " );
		}
		
		if (StringUtils.isNotBlank(rErrExpFilterDTO.getNombreFicheroNot())) {
			qlString.append( " AND fil.nbFichero <> :nbFicheroNot " );
		}
		
		if (StringUtils.isNotBlank(rErrExpFilterDTO.getEstado())) {
			qlString.append( " AND fil.estado = :estado " );
		}
		
		if (StringUtils.isNotBlank(rErrExpFilterDTO.getEstadoNot())) {
			qlString.append( " AND fil.estado <> :estadoNot " );
		}
		
		if (StringUtils.isNotBlank(rErrExpFilterDTO.getTipoFichero())) {
			qlString.append( " AND fil.tpFichero = :tpFichero " );
		}
		
		if (StringUtils.isNotBlank(rErrExpFilterDTO.getTipoFicheroNot())) {
			qlString.append( " AND fil.tpFichero <> :tpFicheroNot " );
		}
		
		if (rErrExpFilterDTO.getFechaDesde() != null) {
			qlString.append( " AND fil.fhEnviRecep >= :fechaDesde " );
		}
		
		if (rErrExpFilterDTO.getFechaHasta() != null) {
			qlString.append( " AND fil.fhEnviRecep <= :fechaHasta " );
		}
		
		try {

			query = em.createNativeQuery(qlString.toString());
		
			if (StringUtils.isNotBlank(rErrExpFilterDTO.getNombreFichero())) {
				query.setParameter("nbFichero", rErrExpFilterDTO.getNombreFichero());
			}
			
			if (StringUtils.isNotBlank(rErrExpFilterDTO.getNombreFicheroNot())) {
				query.setParameter("nbFicheroNot", rErrExpFilterDTO.getNombreFicheroNot());
			}
			
			if (StringUtils.isNotBlank(rErrExpFilterDTO.getEstado())) {
				query.setParameter("estado", rErrExpFilterDTO.getEstado());
			}
			
			if (StringUtils.isNotBlank(rErrExpFilterDTO.getEstadoNot())) {
				query.setParameter("estadoNot", rErrExpFilterDTO.getEstadoNot());
			}
			
			if (StringUtils.isNotBlank(rErrExpFilterDTO.getTipoFichero())) {
				query.setParameter("tpFichero", rErrExpFilterDTO.getTipoFichero());
			}
			
			if (StringUtils.isNotBlank(rErrExpFilterDTO.getTipoFicheroNot())) {
				query.setParameter("tpFicheroNot", rErrExpFilterDTO.getTipoFicheroNot());
			}
			
			if (rErrExpFilterDTO.getFechaDesde() != null) {
				query.setParameter("fechaDesde", rErrExpFilterDTO.getFechaDesde());
			}
			
			if (rErrExpFilterDTO.getFechaHasta() != null) {
				query.setParameter("fechaHasta", rErrExpFilterDTO.getFechaHasta());
			}
			
			List<?> resultList = query.getResultList();

			if ( CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					BusquedaRespuestaErrGCDTO dto =  BusquedaRespuestaErrGCDTO.convertObjectToClienteDTO(
						(Object[]) obj);
					respuestaficheros.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return respuestaficheros;
	}

	/**
	 * Retorna el catalogo de estados
	 * @return
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoEstados() throws SIBBACBusinessException {
		List<SelectDTO> estados = new ArrayList<SelectDTO>();
		Query query;
		String qlString =
				"SELECT DISTINCT fil.estado as estado FROM bsnbpsql.TMCT0_EXPEDIDAS_FILES fil";
		try {
			query = em.createNativeQuery(qlString.toString());
			List<?> resultList = query.getResultList();
			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					SelectDTO dto = new SelectDTO(String.valueOf(((Object[]) obj)[0]),
							String.valueOf(((Object[]) obj)[0]));
					estados.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return estados;
		
	}
	
	/**
	 * Retorna el catalogo de tipos de fichero
	 * @return
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoTipoFicheros() throws SIBBACBusinessException {

		List<SelectDTO> tipos = new ArrayList<SelectDTO>();
		Query query;
		String qlString = 
				"SELECT DISTINCT fil.tpFichero as tpFichero FROM bsnbpsql.TMCT0_EXPEDIDAS_FILES fil";

		try {

			query = em.createNativeQuery(qlString.toString());

			List<?> resultList = query.getResultList();

			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					SelectDTO dto = new SelectDTO(String.valueOf(((Object[]) obj)[0]),
							String.valueOf(((Object[]) obj)[0]));
					tipos.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return tipos;
		
	}
	
	/**
	 * Retorna el catalogo de Nombres de fichero
	 * @return
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoNombresFicheros() throws SIBBACBusinessException {
		List<SelectDTO> tipos = new ArrayList<SelectDTO>();
		Query query;
		String qlString = "SELECT DISTINCT fil.nbFichero as nbFichero FROM bsnbpsql.TMCT0_EXPEDIDAS_FILES fil";
		try {
			query = em.createNativeQuery(qlString.toString());
			List<?> resultList = query.getResultList();
			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					SelectDTO dto = new SelectDTO((String)obj, (String)obj);
					tipos.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return tipos;
	}
	
}
