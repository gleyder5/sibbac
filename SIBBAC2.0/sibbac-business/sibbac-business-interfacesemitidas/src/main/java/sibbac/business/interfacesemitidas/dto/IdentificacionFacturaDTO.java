package sibbac.business.interfacesemitidas.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;

/**
 * Forma parte del bloque 01
 * 
 * @author adrian.prause
 *
 */
public class IdentificacionFacturaDTO {

	private Long numeroEmisorFactura;
	private String numeroFacturaEmisorResumenFin;
	private Date fechaExpedicionFacturaEmision;

	@Override
	public String toString() {
		SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO_GUIONES);
		try {
			String sfechaExpedicionFacturaEmision = "";
			if (fechaExpedicionFacturaEmision != null) {
				sfechaExpedicionFacturaEmision = format.format(fechaExpedicionFacturaEmision);
			}
			return StringHelper.padRight(String.valueOf(numeroEmisorFactura), 60)
					+ StringHelper.padRight(numeroFacturaEmisorResumenFin, 60)
					+ StringHelper.padRight(sfechaExpedicionFacturaEmision, 10);
		} catch (Exception e) {
			return "";
		}
	}

	public Long getNumeroEmisorFactura() {
		return numeroEmisorFactura;
	}

	public void setNumeroEmisorFactura(Long numeroEmisorFactura) {
		this.numeroEmisorFactura = numeroEmisorFactura;
	}

	public String getNumeroFacturaEmisorResumenFin() {
		return numeroFacturaEmisorResumenFin;
	}

	public void setNumeroFacturaEmisorResumenFin(String numeroFacturaEmisorResumenFin) {
		this.numeroFacturaEmisorResumenFin = numeroFacturaEmisorResumenFin;
	}

	public Date getFechaExpedicionFacturaEmision() {
		return fechaExpedicionFacturaEmision;
	}

	public void setFechaExpedicionFacturaEmision(Date fechaExpedicionFacturaEmision) {
		this.fechaExpedicionFacturaEmision = fechaExpedicionFacturaEmision;
	}

}
