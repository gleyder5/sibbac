package sibbac.business.interfacesemitidas.dto;

import java.util.ArrayList;
import java.util.List;

import sibbac.business.wrappers.common.StringHelper;

/**
 * DTO que contiene las facturas emitidas para fichero.
 * 
 * @author adrian.prause
 *
 */
public class FicheroFacturaEmitidaDTO {

	private String carpeta;
	private String entorno;
	private String version;
	private int secuencial;
	private CabeceraDTO bloque00;
	private List<SecuenciaFacturaEmitidaDTO> secuencias = new ArrayList<SecuenciaFacturaEmitidaDTO>();

	public FicheroFacturaEmitidaDTO(String entorno, String version, int secuencial, String carpeta) {
		super();
		this.entorno = entorno;
		this.version = version;
		this.secuencial = secuencial;
		this.carpeta = carpeta;
	}

	public String getRuta() {
		return carpeta + getNombre();
	}

	public String getCarpeta() {
		return carpeta;
	}

	public void setCarpeta(String carpeta) {
		this.carpeta = carpeta;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getEntorno() {
		return entorno;
	}

	public void setEntorno(String entorno) {
		this.entorno = entorno;
	}

	public int getSecuencial() {
		return secuencial;
	}

	public void setSecuencial(int secuencial) {
		this.secuencial = secuencial;
	}

	public String getIdentificacionInterface() {
		return bloque00.getInterfaz();
	}

	public String getCodigoEmpresa() {
		return bloque00.getEmpresaDeclarante();
	}

	public String getFecha() {
		return bloque00.getFechaFormatoFechaFichero();
	}

	public String getNombre() {
		String nombre = new String();
		nombre = entorno + ".E" + getCodigoEmpresa() + ".F" + getFecha() + "." + getIdentificacionInterface() + "." + version
				+ StringHelper.padCerosLeft(secuencial, 3);
		return nombre;
	}

	public CabeceraDTO getBloque00() {
		return bloque00;
	}

	public void setBloque00(CabeceraDTO bloque00) {
		this.bloque00 = bloque00;
	}

	public List<SecuenciaFacturaEmitidaDTO> getSecuencias() {
		return secuencias;
	}

	public void setSecuencias(List<SecuenciaFacturaEmitidaDTO> secuencias) {
		this.secuencias = secuencias;
	}

	public String getContenido() {
		StringBuilder s = new StringBuilder();
		s.append(this.getBloque00());
		for (SecuenciaFacturaEmitidaDTO secuencia : this.getSecuencias()) {
			s.append(StringHelper.SALTO_DE_LINEA);
			s.append(secuencia);
		}
		return s.toString();
	}

	public String getLogInfo() {
		String log = this.getNombre() + " - tamaño fichero generado - bloque 00: "
				+ this.getBloque00().toString().length();
		for (SecuenciaFacturaEmitidaDTO secuencia : this.getSecuencias()) {
			log = log + ", secuencia " + secuencia.getBloque01().getSecuencialRegistro();
			log = log + ": bloque01: " + secuencia.getBloque01().toString().length() + " (Datos factura: "
					+ secuencia.getBloque01().getDatosFactura().toString().length() + " ( Datos contraparte: + "
					+ secuencia.getBloque01().getDatosFactura().getDatosContraparte().toString().length()
					+ ", Desglose por factura: "
					+ secuencia.getBloque01().getDatosFactura().getDesglosePorFactura().toString().length() + ") ) ";
			log = log + ", bloque 02: " + secuencia.getBloque02().toString().length();
			if (null != secuencia.getBloque06()) {
				log = log + ", bloque 06: " + secuencia.getBloque06().toString().length();
			}
			if (null != secuencia.getBloque07()) {
				log = log + ", bloque 07: " + secuencia.getBloque07().toString().length();
			}
			log = log + ", bloque 09: " + secuencia.getBloque09().toString().length();
		}
		return log;
	}

}
