package sibbac.business.interfacesemitidas.database.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_EXPEDIDAS_FILES.
 */
@Entity
@Table(name = "TMCT0_EXPEDIDAS_FILES")
public class Tmct0ExpedidasFiles implements Serializable {
	
	/**
	 * Version serial generada automáticamente
	 */
	private static final long serialVersionUID = -7656884859942745052L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false, length = 8)
	private Long id;
	
	@Column(name = "NBFICHERO", nullable = false, length = 200)
	private String nbFichero;
	
	@Column(name = "TPFICHERO", nullable = false, length = 200)
	private String tpFichero = "";
	
	@Column(name = "FHENVIRECEP", nullable = false)
	private Date fhEnviRecep;
	
	@Column(name = "ESTADO", nullable = true, length = 2)
	private String estado;
	
	@Column(name = "ASOCIACION", nullable = true, length = 200)
	private String asociacion;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0ExpedidasFiles() {
		super();
	}
	
	public boolean isRespuesta(String respuesta, String sufijo) {
		final String[] nameParts;
		final int l;
		
		Objects.requireNonNull(nbFichero, "El nombre del fichero debe ser conocido");
		nameParts = nbFichero.split("\\.");
		l = nameParts.length;
		if(l <= 0) {
			return false;
		}
		if(nameParts[l - 1].equals(sufijo)) {
			return true;
		}
		for(int i = 0; i < l; i++) {
			if(nameParts[i].equals(respuesta)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isEmision(String respuesta, String sufijo) {
		return !isRespuesta(respuesta, sufijo);
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNbFichero() {
		return this.nbFichero;
	}

	public void setNbFichero(String nbFichero) {
		this.nbFichero = nbFichero;
	}

	public String getTpFichero() {
		return this.tpFichero;
	}

	public void setTpFichero(String tpFichero) {
		this.tpFichero = tpFichero != null ? tpFichero : "";
	}

	public Date getFhEnviRecep() {
		return this.fhEnviRecep;
	}

	public void setFhEnviRecep(Date fhEnviRecep) {
		this.fhEnviRecep = fhEnviRecep;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getAsociacion() {
		return this.asociacion;
	}

	public void setAsociacion(String asociacion) {
		this.asociacion = asociacion;
	}

}
