package sibbac.business.interfacesemitidas.dto.assembler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.interfacesemitidas.dto.CabeceraDTO;
import sibbac.business.interfacesemitidas.dto.ErroresDTO;
import sibbac.business.interfacesemitidas.dto.EstructuraRespuestaDTO;
import sibbac.business.interfacesemitidas.dto.IdentificacionFacturaRespuestaDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.RespuestaErroresExpedidasFilterDTO;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.CodigosRetornoBloque01DatosFacturaEnum;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.TipoRegistroEnum;
import sibbac.business.interfacesemitidas.helper.InterfacesEmitidasHelper;

/**
 * Clase que realiza la transformacion entre el DTO de respuesta y la info del
 * fichero de respuesta procesado.
 */
@Service
public class RespFacturasEmitidasAssembler {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(RespFacturasEmitidasAssembler.class);

  private static DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");

  /**
   * Se obtiene DTO por linea procesada.
   * @param line
   * @param estructuraRespuestaDTO
   * @return EstructuraRespuestaDTO
   * @throws ParseException
   */
  public EstructuraRespuestaDTO getDTOFromRow(String line, EstructuraRespuestaDTO estructuraRespuestaDTO)
      throws ParseException {
    if (InterfacesEmitidasHelper.isBloqueByTipoRegistro(line, TipoRegistroEnum.CABECERA.getId())) {
      CabeceraDTO cabeceraDTO = new CabeceraDTO();
      cabeceraDTO.popularCabeceraDTO(line);
      estructuraRespuestaDTO.setCabeceraDTO(cabeceraDTO);
    }
    else if (InterfacesEmitidasHelper.isBloqueByTipoRegistro(line, TipoRegistroEnum.BLOQUE_01.getId())) {
      IdentificacionFacturaRespuestaDTO identifFactRespDTO = new IdentificacionFacturaRespuestaDTO();
      identifFactRespDTO.popularIdentifFacturaRespuestaDTO(line);
      estructuraRespuestaDTO.setIdentificacionFacturaRespuestaDTO(identifFactRespDTO);
      LOG.trace("[getDTOFromRow] es estado de operación por código retorno {} ",
          InterfacesEmitidasHelper.isEstadoOperacionByCodigoRetorno(line,
          CodigosRetornoBloque01DatosFacturaEnum.OPERACION_ACEPTADA_CON_ERRORES.getId()));
    }
    else if (InterfacesEmitidasHelper.isBloqueByTipoRegistro(line, TipoRegistroEnum.BLOQUE_02.getId())) {
      ErroresDTO erroresDTO = new ErroresDTO();
      erroresDTO.popularErroresDTO(line);
      estructuraRespuestaDTO.setErroresDTO(erroresDTO);
    }
    return estructuraRespuestaDTO;
  }

  /**
   * Realiza la conversion entre los valores recogidos de la pantalla y el DTO
   * @param paramsObjects -> Valores que recibe el service de la pantalla y que
   * se quieren transformar en DTO.
   * @return Entidad con los datos del DTO tratados
   */
  public RespuestaErroresExpedidasFilterDTO getRespErroresExpFilterDto(Map<String, String> paramsObjects) {

    RespuestaErroresExpedidasFilterDTO rErrExpFilterDTO = new RespuestaErroresExpedidasFilterDTO();

    if (paramsObjects.get("nombreFichero") != null) {
      rErrExpFilterDTO.setNombreFichero(String.valueOf(paramsObjects.get("nombreFichero")));
    }

    if (paramsObjects.get("nombreFicheroNot") != null) {
      rErrExpFilterDTO.setNombreFicheroNot(String.valueOf(paramsObjects.get("nombreFicheroNot")));
    }

    if (paramsObjects.get("estadoFichero") != null) {
      rErrExpFilterDTO.setEstado(String.valueOf(paramsObjects.get("estadoFichero")));
    }

    if (paramsObjects.get("estadoFicheroNot") != null) {
      rErrExpFilterDTO.setEstadoNot(String.valueOf(paramsObjects.get("estadoFicheroNot")));
    }

    if (paramsObjects.get("tipoFichero") != null) {
      rErrExpFilterDTO.setTipoFichero(paramsObjects.get("tipoFichero"));
    }

    if (paramsObjects.get("tipoFicheroNot") != null) {
      rErrExpFilterDTO.setTipoFicheroNot(paramsObjects.get("tipoFicheroNot"));
    }

    if (paramsObjects.get("fechaDesde") != null) {
      try {
        rErrExpFilterDTO.setFechaDesde(dF.parse(paramsObjects.get("fechaDesde")));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }
    }
    if (paramsObjects.get("fechaHasta") != null) {
      try {
        rErrExpFilterDTO.setFechaHasta(dF.parse(paramsObjects.get("fechaHasta")));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }
    }

    return rErrExpFilterDTO;

  }

}
