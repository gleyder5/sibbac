package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0InterfacesEmitidasConst;

/**
 * Data access object de Tmct0InterfacesEmitidasConstDao.
 */
@Repository
public interface Tmct0InterfacesEmitidasConstDao extends JpaRepository<Tmct0InterfacesEmitidasConst, Long> {

  @Query("SELECT tConst FROM Tmct0InterfacesEmitidasConst tConst WHERE tConst.grupo = :grupo")
  public List<Tmct0InterfacesEmitidasConst> findByGrupo(@Param("grupo") String grupo);

  @Query("SELECT tConst FROM Tmct0InterfacesEmitidasConst tConst WHERE tConst.clave = :clave")
  public Tmct0InterfacesEmitidasConst findByClave(@Param("clave") String clave);

}
