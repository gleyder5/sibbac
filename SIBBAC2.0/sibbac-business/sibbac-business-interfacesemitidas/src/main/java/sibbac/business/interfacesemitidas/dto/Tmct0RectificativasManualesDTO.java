package sibbac.business.interfacesemitidas.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * DTO para la gestion de TMCT0_FACTURA_MANUAL
 * @author Neoris
 *
 */
public class Tmct0RectificativasManualesDTO {

	/**
	 * Identificador del registro en la tabla
	 */
	private BigInteger id;

	/**
	 * Fecha en la que actual de la creación o última modificación
	 */
	private Date auditDate;

	/**
	 * identificador del usuario que creó o modificó por última vez la factura
	 */
	private String auditUser;

	/**
	 * 
	 */
	private String nbComentarios;

	/**
	 * fecha de creación de la factura
	 */
	private Date fhFechaCre;

	/**
	 * fecha de contabilización factura
	 */
	private Date fhFechaFac;

	/**
	 * número de la factura (autogenerado)
	 */
	private BigInteger nbDocNumero;

	/**
	 * identificador del alias asociado al cliente
	 */
	private BigInteger idAlias;

	/**
	 * identificador del de la tabla TMCT0ESTADO
	 */
	private String idEstado;

	/**
	 * identificador de la moneda asociada a la factura extraído de la tabla TMCT0_MONEDA
	 */
	private String idMoneda;

	/**
	 * Se genera en función de la fecha actual (calcular igual que en la query orden de contabilidad)
	 */
	private Integer periodo;

	/**
	 * fecha de creación de la factura
	 */
	private Date fhInicio;

	/**
	 * identificador del tipo de factura asociado a la factura extraído de la tabla TMCT0_TIPO_FACTURA
	 */
	private String idTipoFactura;

	/**
	 * identificador de la causa exención asociada a la factura extraída de la tabla TMCT0_CAUSA_EXENCION
	 */
	private String idCausaExencion;

	/**
	 * identificador de la clave régimen asociada a la factura extraída de la tabla TMCT0_CLAVE_REGIMEN
	 */
	private String idClaveRegimen;

	/**
	 * Importe correspondiente a la parte Sujeta / Exenta o No. importe de total de la factura
	 */
	private BigDecimal impBaseImponible;

	/**
	 * según impuesto la cuota que se añade a la base imponible que sumada a ésta da el total de la factura. En caso de exenta sería 0, en caso de no exenta según impuesto
	 */
	private BigDecimal impImpuesto;

	/**
	 * identificador del impuesto asociado a la factura extraída de la tabla TMCT0_CODIGO_IMPUESTO
	 */
	private String idCodImpuesto;

	/**
	 * contendrá el valor 0 si es emisión por la prestación de un servicio y  1 si es entrega de un bien
	 */
	private String idEntregaBien;

	/**
	 * este campo será obligatoria si en el campo Clave Regimen Especial o Trascendencia se ha rellenado con el código 07 Régimen especial grupo de entidades en IVA (nivel avanzado),
	 *  podrá ir a cero si el usuario no introduce nada. Es un porcentaje
	 */
	private BigDecimal coefImpCostes;

	/**
	 * este campo se informara igual que el anterior
	 */
	private BigDecimal baseImponCostes;

	/**
	 * es la cuota resultante de aplicar un tipo impositivo a la base imponible
	 */
	private BigDecimal cuotaRepercut;
	
	/**
	 * es el importe total de la factura
	 */
	private BigDecimal imFinSvb;
	
	/**
	 * es el importe en divisa
	 */
	private BigDecimal imFinDiv;
	
	/**
	 *	id Exenta 
	 */
	private String idExenta;
	
	/**
	 *	id Sujeta 
	 */
	private String idSujeta;
	
	/**
	
	/**
	 * objeto cliente DTO
	 */
	private ClienteDTO clienteDTO;
	
	/**
	 * Listado de facturas rectificadas
	 */
	private List<Object> listaFacturasRectificadas;
	
	/**
	 * Importe correspondiente a la base imponible anterior de una factura rectificativa GRUPO IVA
	 */
	private BigDecimal baseImponibleAnterior;

	/**
	 * Importe correspondiente a la base imponible actual de una factura rectificativa GRUPO IVA
	 */
	private BigDecimal baseImponibleActual;

	public Tmct0RectificativasManualesDTO () {

	}

	/**
	 * Constructor con parámetros
	 */
	public Tmct0RectificativasManualesDTO(BigInteger id,
			Date auditDate,
			String auditUser,
			String nbComentarios,
			Date fhFechaCre,
			Date fhFechaFac,
			BigInteger nbDocNumero,
			BigInteger idAlias,
			String idEstado,
			String idMoneda,
			Integer periodo,
			Date fhInicio,
			String idTipoFactura,
			String idCausaExencion,
			String idClaveRegimen,
			BigDecimal impBaseImponible,
			BigDecimal impImpuesto,
			String idCodImpuesto,
			String idEntregaBien,
			BigDecimal coefImpCostes,
			BigDecimal baseImponCostes,
			BigDecimal cuotaRepercut,
			Date fechaDesde,
			Date fechaHasta,
			BigDecimal imFinSvb,
			BigDecimal imFinDiv,
			ClienteDTO clienteDTO,
			List<Object> listaFacturasRectificadas,
			String idExenta,
			String idSujeta,
            BigDecimal baseImponibleAnterior,
            BigDecimal baseImponibleActual) {

		this.id = id;
		this.auditDate = auditDate;
		this.auditUser = auditUser;
		this.nbComentarios = nbComentarios;
		this.fhFechaCre = fhFechaCre;
		this.fhFechaFac = fhFechaFac;
		this.nbDocNumero = nbDocNumero;
		this.idAlias = idAlias;
		this.idEstado = idEstado;
		this.idMoneda = idMoneda;
		this.periodo = periodo;
		this.fhInicio = fhInicio;
		this.idTipoFactura = idTipoFactura;
		this.idCausaExencion = idCausaExencion;
		this.idClaveRegimen = idClaveRegimen;
		this.impBaseImponible = impBaseImponible;
		this.impImpuesto = impImpuesto;
		this.idCodImpuesto = idCodImpuesto;
		this.idEntregaBien = idEntregaBien;
		this.coefImpCostes = coefImpCostes;
		this.baseImponCostes = baseImponCostes;
		this.cuotaRepercut = cuotaRepercut;
		this.imFinSvb = imFinSvb;
		this.imFinDiv = imFinDiv;
		this.clienteDTO = clienteDTO;
		this.listaFacturasRectificadas = listaFacturasRectificadas;
		this.idExenta = idExenta;
		this.idSujeta = idSujeta;
		this.baseImponibleAnterior=baseImponibleAnterior;
		this.baseImponibleActual=baseImponibleActual;
	}

	/**
	 * @return the baseImponibleAnterior
	 */
	public BigDecimal getBaseImponibleAnterior() {
		return baseImponibleAnterior;
	}

	/**
	 * @param baseImponibleAnterior the baseImponibleAnterior to set
	 */
	public void setBaseImponibleAnterior(BigDecimal baseImponibleAnterior) {
		this.baseImponibleAnterior = baseImponibleAnterior;
	}

	/**
	 * @return the baseImponibleActual
	 */
	public BigDecimal getBaseImponibleActual() {
		return baseImponibleActual;
	}

	/**
	 * @param baseImponibleActual the baseImponibleActual to set
	 */
	public void setBaseImponibleActual(BigDecimal baseImponibleActual) {
		this.baseImponibleActual = baseImponibleActual;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * @return the nbComentarios
	 */
	public String getNbComentarios() {
		return nbComentarios;
	}

	/**
	 * @param nbComentarios the nbComentarios to set
	 */
	public void setNbComentarios(String nbComentarios) {
		this.nbComentarios = nbComentarios;
	}

	/**
	 * @return the fhFechaCre
	 */
	public Date getFhFechaCre() {
		return fhFechaCre;
	}

	/**
	 * @param fhFechaCre the fhFechaCre to set
	 */
	public void setFhFechaCre(Date fhFechaCre) {
		this.fhFechaCre = fhFechaCre;
	}

	/**
	 * @return the fhFechaFac
	 */
	public Date getFhFechaFac() {
		return fhFechaFac;
	}

	/**
	 * @param fhFechaFac the fhFechaFac to set
	 */
	public void setFhFechaFac(Date fhFechaFac) {
		this.fhFechaFac = fhFechaFac;
	}

	/**
	 * @return the nbDocNumero
	 */
	public BigInteger getNbDocNumero() {
		return nbDocNumero;
	}

	/**
	 * @param nbDocNumero the nbDocNumero to set
	 */
	public void setNbDocNumero(BigInteger nbDocNumero) {
		this.nbDocNumero = nbDocNumero;
	}

	/**
	 * @return the idAlias
	 */
	public BigInteger getIdAlias() {
		return idAlias;
	}

	/**
	 * @param idAlias the idAlias to set
	 */
	public void setIdAlias(BigInteger idAlias) {
		this.idAlias = idAlias;
	}

	/**
	 * @return the idEstado
	 */
	public String getIdEstado() {
		return idEstado;
	}

	/**
	 * @param idEstado the idEstado to set
	 */
	public void setIdEstado(Integer idEstado) {
		this.idEstado = String.valueOf(idEstado);
	}

	/**
	 * @return the idMoneda
	 */
	public String getIdMoneda() {
		return idMoneda;
	}

	/**
	 * @param idMoneda the idMoneda to set
	 */
	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = String.valueOf(idMoneda);
	}

	/**
	 * @return the periodo
	 */
	public Integer getPeriodo() {
		return periodo;
	}

	/**
	 * @param periodo the periodo to set
	 */
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	/**
	 * @return the fhInicio
	 */
	public Date getFhInicio() {
		return fhInicio;
	}

	/**
	 * @param fhInicio the fhInicio to set
	 */
	public void setFhInicio(Date fhInicio) {
		this.fhInicio = fhInicio;
	}

	/**
	 * @return the idTipoFactura
	 */
	public String getIdTipoFactura() {
		return idTipoFactura;
	}

	/**
	 * @param idTipoFactura the idTipoFactura to set
	 */
	public void setIdTipoFactura(Integer idTipoFactura) {
		this.idTipoFactura = String.valueOf(idTipoFactura);
	}

	/**
	 * @return the idCausaExencion
	 */
	public String getIdCausaExencion() {
		return idCausaExencion;
	}

	/**
	 * @param idCausaExencion the idCausaExencion to set
	 */
	public void setIdCausaExencion(Integer idCausaExencion) {
		this.idCausaExencion = String.valueOf(idCausaExencion);
	}

	/**
	 * @return the idClaveRegimen
	 */
	public String getIdClaveRegimen() {
		return idClaveRegimen;
	}

	/**
	 * @param idClaveRegimen the idClaveRegimen to set
	 */
	public void setIdClaveRegimen(Integer idClaveRegimen) {
		this.idClaveRegimen = String.valueOf(idClaveRegimen);
	}

	/**
	 * @return the impBaseImponible
	 */
	public BigDecimal getImpBaseImponible() {
		return impBaseImponible;
	}

	/**
	 * @param impBaseImponible the impBaseImponible to set
	 */
	public void setImpBaseImponible(BigDecimal impBaseImponible) {
		this.impBaseImponible = impBaseImponible;
	}

	/**
	 * @return the impImpuesto
	 */
	public BigDecimal getImpImpuesto() {
		return impImpuesto;
	}

	/**
	 * @param impImpuesto the impImpuesto to set
	 */
	public void setImpImpuesto(BigDecimal impImpuesto) {
		this.impImpuesto = impImpuesto;
	}

	/**
	 * @return the idCodImpuesto
	 */
	public String getIdCodImpuesto() {
		return idCodImpuesto;
	}

	/**
	 * @param idCodImpuesto the idCodImpuesto to set
	 */
	public void setIdCodImpuesto(Integer idCodImpuesto) {
		this.idCodImpuesto = String.valueOf(idCodImpuesto);
	}

	/**
	 * @return the idEntregaBien
	 */
	public String getIdEntregaBien() {
		return idEntregaBien;
	}

	/**
	 * @param idEntregaBien the idEntregaBien to set
	 */
	public void setIdEntregaBien(Short idEntregaBien) {
		this.idEntregaBien = String.valueOf(idEntregaBien);
	}

	/**
	 * @return the coefImpCostes
	 */
	public BigDecimal getCoefImpCostes() {
		return coefImpCostes;
	}

	/**
	 * @param coefImpCostes the coefImpCostes to set
	 */
	public void setCoefImpCostes(BigDecimal coefImpCostes) {
		this.coefImpCostes = coefImpCostes;
	}

	/**
	 * @return the baseImponCostes
	 */
	public BigDecimal getBaseImponCostes() {
		return baseImponCostes;
	}

	/**
	 * @param baseImponCostes the baseImponCostes to set
	 */
	public void setBaseImponCostes(BigDecimal baseImponCostes) {
		this.baseImponCostes = baseImponCostes;
	}

	/**
	 * @return the cuotaRepercut
	 */
	public BigDecimal getCuotaRepercut() {
		return cuotaRepercut;
	}

	/**
	 * @param cuotaRepercut the cuotaRepercut to set
	 */
	public void setCuotaRepercut(BigDecimal cuotaRepercut) {
		this.cuotaRepercut = cuotaRepercut;
	}

	/**
	 * @return the imFinSvb
	 */
	public BigDecimal getImFinSvb() {
		return imFinSvb;
	}

	/**
	 * @param imFinSvb the imFinSvb to set
	 */
	public void setImFinSvb(BigDecimal imFinSvb) {
		this.imFinSvb = imFinSvb;
	}

	/**
	 * @return the imFinDiv
	 */
	public BigDecimal getImFinDiv() {
		return imFinDiv;
	}

	/**
	 * @param imFinDiv the imFinDiv to set
	 */
	public void setImFinDiv(BigDecimal imFinDiv) {
		this.imFinDiv = imFinDiv;
	}
	
	/**
	 * @return the idExenta
	 */
	public String getIdExenta() {
		return this.idExenta;
	}

	/**
	 * @param idExenta the idExenta to set
	 */
	public void setIdExenta(String idExenta) {
		this.idExenta = idExenta;
	}

	/**
	 * @return the idSujeta
	 */
	public String getIdSujeta() {
		return this.idSujeta;
	}

	/**
	 * @param idSujeta the idSujeta to set
	 */
	public void setIdSujeta(String idSujeta) {
		this.idSujeta = idSujeta;
	}

	/**
	 * @return the clienteDTO
	 */
	public ClienteDTO getClienteDTO() {
		return clienteDTO;
	}

	/**
	 * @param clienteDTO the clienteDTO to set
	 */
	public void setClienteDTO(ClienteDTO clienteDTO) {
		this.clienteDTO = clienteDTO;
	}
	
	/**
	 * @return the listaFacturasRectificadas
	 */
	public List<Object> getListaFacturasRectificadas() {
		return listaFacturasRectificadas;
	}

	/**
	 * @param listaFacturasRectificadas the listaFacturasRectificadas to set
	 */
	public void setListaFacturasRectificadas(List<Object> listaFacturasRectificadas) {
		this.listaFacturasRectificadas = listaFacturasRectificadas;
	}
}
