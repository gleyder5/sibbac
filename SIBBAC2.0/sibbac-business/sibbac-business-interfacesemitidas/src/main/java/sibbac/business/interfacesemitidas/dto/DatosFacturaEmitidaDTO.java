package sibbac.business.interfacesemitidas.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 01
 * 
 * @author adrian.prause
 *
 */
public class DatosFacturaEmitidaDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private String tipoComunicacion;
	private String claveUnica;

	private IdentificacionFacturaDTO identificacion;
	private DatosFacturaDTO datosFactura;

	private String claveUnicaIdentificacionOperacionOrigenRetrocedida;
	private Date fechaContabilizacion;
	private String indicadorExcepcion;
	private String origenOperacion;
	private String identificacionDatosContables;
	private String origenContable;
	private String otrosDatosAdicionales;

	@Override
	public String toString() {
		SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
		String sfechaContabilizacion = "";
		String datosFacturaEmitida = "";
		if (fechaContabilizacion != null) {
			sfechaContabilizacion = format.format(fechaContabilizacion);
		}
		try {
			datosFacturaEmitida = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padRight(tipoComunicacion, 2) + StringHelper.padRight(claveUnica, 100)
					+ identificacion + datosFactura
					+ StringHelper.padRight(claveUnicaIdentificacionOperacionOrigenRetrocedida, 100)
					+ StringHelper.padRight(sfechaContabilizacion, 10) + StringHelper.padRight(indicadorExcepcion, 1)
					+ StringHelper.padRight(origenOperacion, 3) + identificacionDatosContables
					+ StringHelper.padRight(origenContable, 500) + StringHelper.padRight(otrosDatosAdicionales, 500);
			return StringHelper.padRight(datosFacturaEmitida, 2449);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public String getTipoComunicacion() {
		return tipoComunicacion;
	}

	public void setTipoComunicacion(String tipoComunicacion) {
		this.tipoComunicacion = tipoComunicacion;
	}

	public String getClaveUnica() {
		return claveUnica;
	}

	public void setClaveUnica(String claveUnica) {
		this.claveUnica = claveUnica;
	}

	public IdentificacionFacturaDTO getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(IdentificacionFacturaDTO identificacion) {
		this.identificacion = identificacion;
	}

	public DatosFacturaDTO getDatosFactura() {
		return datosFactura;
	}

	public void setDatosFactura(DatosFacturaDTO datosFactura) {
		this.datosFactura = datosFactura;
	}

	public String getClaveUnicaIdentificacionOperacionOrigenRetrocedida() {
		return claveUnicaIdentificacionOperacionOrigenRetrocedida;
	}

	public void setClaveUnicaIdentificacionOperacionOrigenRetrocedida(
			String claveUnicaIdentificacionOperacionOrigenRetrocedida) {
		this.claveUnicaIdentificacionOperacionOrigenRetrocedida = claveUnicaIdentificacionOperacionOrigenRetrocedida;
	}

	public Date getFechaContabilizacion() {
		return fechaContabilizacion;
	}

	public void setFechaContabilizacion(Date fechaContabilizacion) {
		this.fechaContabilizacion = fechaContabilizacion;
	}

	public String getIndicadorExcepcion() {
		return indicadorExcepcion;
	}

	public void setIndicadorExcepcion(String indicadorExcepcion) {
		this.indicadorExcepcion = indicadorExcepcion;
	}

	public String getOrigenOperacion() {
		return origenOperacion;
	}

	public void setOrigenOperacion(String origenOperacion) {
		this.origenOperacion = origenOperacion;
	}

	public String getIdentificacionDatosContables() {
		return identificacionDatosContables;
	}

	public void setIdentificacionDatosContables(String identificacionDatosContables) {
		this.identificacionDatosContables = identificacionDatosContables;
	}

	public String getOrigenContable() {
		return origenContable;
	}

	public void setOrigenContable(String origenContable) {
		this.origenContable = origenContable;
	}

	public String getOtrosDatosAdicionales() {
		return otrosDatosAdicionales;
	}

	public void setOtrosDatosAdicionales(String otrosDatosAdicionales) {
		this.otrosDatosAdicionales = otrosDatosAdicionales;
	}

}
