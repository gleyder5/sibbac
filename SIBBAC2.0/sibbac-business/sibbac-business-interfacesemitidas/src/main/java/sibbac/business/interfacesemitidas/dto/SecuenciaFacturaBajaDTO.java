package sibbac.business.interfacesemitidas.dto;

/**
 * Registro que contiene los bloques para cada factura baja
 * 
 * @author adrian.prause
 *
 */
public class SecuenciaFacturaBajaDTO {

	private DatosFacturaBajaDTO bloque01;

	public SecuenciaFacturaBajaDTO(DatosFacturaBajaDTO bloque01) {
		super();
		this.bloque01 = bloque01;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(bloque01);
		return s.toString();
	}

	public DatosFacturaBajaDTO getBloque01() {
		return bloque01;
	}

	public void setBloque01(DatosFacturaBajaDTO bloque01) {
		this.bloque01 = bloque01;
	}

}
