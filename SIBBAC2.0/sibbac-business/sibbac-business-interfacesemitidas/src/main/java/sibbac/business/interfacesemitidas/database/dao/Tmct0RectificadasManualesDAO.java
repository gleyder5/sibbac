package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0RectificadasManualesDAO extends JpaRepository<Tmct0RectificativaManual, BigInteger> {

	@Query("SELECT f FROM Tmct0RectificativaManual f WHERE (f.enviado is null or f.enviado = false) and f.estado.idestado = :estado")
	public List<Tmct0RectificativaManual> findNoEnviadasIdByEstado(@Param("estado") Integer estado);

	@Query("SELECT f FROM Tmct0RectificativaManual f WHERE f.estado.idestado not in (:estado) and (f.enviado is null or f.enviado = false) and f.fhFechaCre >= :fechaDesde and f.fhFechaCre <= :fechaHasta")
	public List<Tmct0RectificativaManual> findNoEnviadasByIdEstadoDistinctBetweenFechas(@Param("estado") List<Integer> estado,
			@Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);
	
	@Modifying @Query("UPDATE Tmct0RectificativaManual SET enviado = true WHERE id = :id")
	public int marcaEnviado(@Param("id") BigInteger id);
	
	@Query("SELECT f FROM Tmct0RectificativaManual f WHERE nbDocNumero = :nbDocNumero")
	public Tmct0RectificativaManual findRectificativaManual(@Param("nbDocNumero") BigInteger nbDocNumero);

}
