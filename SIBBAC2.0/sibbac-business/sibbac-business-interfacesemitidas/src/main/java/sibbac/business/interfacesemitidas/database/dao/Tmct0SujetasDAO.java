package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0Sujetas;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0SujetasDAO extends JpaRepository<Tmct0Sujetas,Integer>{
	
	@Query("SELECT suj FROM Tmct0Sujetas suj ORDER BY suj.id ASC")
	public List<Tmct0Sujetas> findAllOrderByClave();

}
