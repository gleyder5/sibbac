package sibbac.business.interfacesemitidas.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_FACTURAS_SUJETAS
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_FACTURAS_SUJETAS")
public class Tmct0Sujetas {

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0Sujetas.class);

	/** Campos de la entidad */

	/**
	 * Identificador del registro en la tabla (autogenerado)
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 4, nullable = false)
	private Integer id;
	
	/**
	 * Indentificará la causa exención
	 */

	@Column(name = "CLAVE", length = 2, nullable = false)
	private String clave;
	
	/**
	 * contendrá una breve descripción del tipo de factura
	 */

	@Column(name = "VALOR", length = 250, nullable = false)
	private String valor;

	/**
	 * Constructor
	 */
	public Tmct0Sujetas() {
		LOG.info("Construido Tmct0CausaExencion");
	}

	/**
	 * Constructor con parámetros
	 */
	public Tmct0Sujetas(Integer id,
			String clave,
			String valor) {

		this.id = id;
		this.clave = clave;
		this.valor = valor;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
