package sibbac.business.interfacesemitidas.dto;

import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.CodigosRetornoBloque01DatosFacturaEnum;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.TipoRegistroEnum;

/**
 *	DTO que representa estructura de la respuesta.
 */
public class EstructuraRespuestaDTO {

	private String nombreFichero;
	private CabeceraDTO cabeceraDTO;
	private IdentificacionFacturaRespuestaDTO identificacionFacturaRespuestaDTO;
	private ErroresDTO erroresDTO;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public EstructuraRespuestaDTO() {
		super();
	}

	public String getNombreFichero() {
		return this.nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public CabeceraDTO getCabeceraDTO() {
		return this.cabeceraDTO;
	}

	public void setCabeceraDTO(CabeceraDTO cabeceraDTO) {
		this.cabeceraDTO = cabeceraDTO;
	}

	public IdentificacionFacturaRespuestaDTO getIdentificacionFacturaRespuestaDTO() {
		return this.identificacionFacturaRespuestaDTO;
	}

	public void setIdentificacionFacturaRespuestaDTO(
			IdentificacionFacturaRespuestaDTO identificacionFacturaRespuestaDTO) {
		this.identificacionFacturaRespuestaDTO = identificacionFacturaRespuestaDTO;
	}

	public ErroresDTO getErroresDTO() {
		return this.erroresDTO;
	}

	public void setErroresDTO(ErroresDTO erroresDTO) {
		this.erroresDTO = erroresDTO;
	}
	
	/**
	 *	Devuelve true si es un bloque de identificacion Factura Respuesta - BLOQUE 01.
	 *	@return boolean
	 */
	public boolean isBloqueIdentificacionFacturaRespuesta() {
		return this.identificacionFacturaRespuestaDTO != null 
			&& this.identificacionFacturaRespuestaDTO.getTipoRegistro().equals(
				TipoRegistroEnum.BLOQUE_01.getId());
	}
	
	/**
	 *	Devuelve true si la operacion fue exitosa.
	 *	@return boolean 
	 */
	public boolean isOperacionExitosa() {
		return this.identificacionFacturaRespuestaDTO != null 
			&& this.identificacionFacturaRespuestaDTO.getCodigoRetorno().equals(
				CodigosRetornoBloque01DatosFacturaEnum.OPERACION_ACEPTADA.getId());
	}
	
}
