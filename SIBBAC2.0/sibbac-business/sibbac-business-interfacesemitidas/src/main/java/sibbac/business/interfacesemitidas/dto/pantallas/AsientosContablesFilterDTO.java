package sibbac.business.interfacesemitidas.dto.pantallas;

import java.util.Date;

/**
 *	DTO que transfiere los filtros de la busqueda de asientos contables.
 */
public class AsientosContablesFilterDTO {

	private String compania;
	private String companiaNot;
	private Date fecha;
	private Integer idTipoFactura;
	private Integer idTipoFacturaNot;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public AsientosContablesFilterDTO() {
		super();
	}

	public String getCompania() {
		return this.compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getCompaniaNot() {
		return this.companiaNot;
	}

	public void setCompaniaNot(String companiaNot) {
		this.companiaNot = companiaNot;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getIdTipoFactura() {
		return this.idTipoFactura;
	}

	public void setIdTipoFactura(Integer idTipoFactura) {
		this.idTipoFactura = idTipoFactura;
	}

	public Integer getIdTipoFacturaNot() {
		return this.idTipoFacturaNot;
	}

	public void setIdTipoFacturaNot(Integer idTipoFacturaNot) {
		this.idTipoFacturaNot = idTipoFacturaNot;
	}

	
	
}