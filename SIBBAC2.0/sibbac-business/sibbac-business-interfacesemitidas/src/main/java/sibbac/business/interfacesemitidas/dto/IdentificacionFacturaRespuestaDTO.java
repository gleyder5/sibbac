package sibbac.business.interfacesemitidas.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 01: Identificacion factura - Para la respuesta
 */
public class IdentificacionFacturaRespuestaDTO extends IdentificacionFacturaDTO {

  private String tipoRegistro;
  private Integer secuencialRegistro;
  private String claveUnicaIdentificacionOperacion;
  private String codigoRetorno;
  private String datosAdicionales;

  @Override
  public String toString() {
    SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
    try {
      return StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
             + StringHelper.padRight(claveUnicaIdentificacionOperacion, 100)
             + StringHelper.padRight(String.valueOf(this.getNumeroEmisorFactura()), 60)
             + StringHelper.padRight(this.getNumeroFacturaEmisorResumenFin(), 60)
             + format.format(this.getFechaExpedicionFacturaEmision()) + StringHelper.padRight(codigoRetorno, 2)
             + StringHelper.padRight(datosAdicionales, 500);
    } catch (Exception e) {
      return "";
    }
  }

  public String getTipoRegistro() {
    return this.tipoRegistro;
  }

  public void setTipoRegistro(String tipoRegistro) {
    this.tipoRegistro = tipoRegistro;
  }

  public Integer getSecuencialRegistro() {
    return this.secuencialRegistro;
  }

  public void setSecuencialRegistro(Integer secuencialRegistro) {
    this.secuencialRegistro = secuencialRegistro;
  }

  public String getClaveUnicaIdentificacionOperacion() {
    return this.claveUnicaIdentificacionOperacion;
  }

  public void setClaveUnicaIdentificacionOperacion(String claveUnicaIdentificacionOperacion) {
    this.claveUnicaIdentificacionOperacion = claveUnicaIdentificacionOperacion;
  }

  public String getCodigoRetorno() {
    return this.codigoRetorno;
  }

  public void setCodigoRetorno(String codigoRetorno) {
    this.codigoRetorno = codigoRetorno;
  }

  public String getDatosAdicionales() {
    return datosAdicionales;
  }

  public void setDatosAdicionales(String datosAdicionales) {
    this.datosAdicionales = datosAdicionales;
  }

  /**
   * Popula DTO IdentificacionFacturaRespuesta segun la fila con datos.
   *
   * @param line
   * @throws ParseException
   */
  public void popularIdentifFacturaRespuestaDTO(String line) throws ParseException {
    SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
    this.tipoRegistro = line.substring(0, 2);
    this.secuencialRegistro = Integer.valueOf(line.substring(2, 9).trim());
    this.claveUnicaIdentificacionOperacion = line.substring(9, 109);
    this.setNumeroEmisorFactura(Long.parseLong(line.substring(109, 169).trim()));
    this.setNumeroFacturaEmisorResumenFin(line.substring(169, 229));
    this.setFechaExpedicionFacturaEmision(format.parse(line.substring(229, 239)));
    this.codigoRetorno = line.substring(239, 241);
    this.datosAdicionales = line.substring(241, 741);
  }

}
