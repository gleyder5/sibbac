package sibbac.business.interfacesemitidas.database.model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_CODIGO_IMPUESTO
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CODIGO_IMPUESTO")
public class Tmct0CodigoImpuesto {

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0CodigoImpuesto.class);

	/** Campos de la entidad */

	/**
	 * Identificador del registro en la tabla
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 8, nullable = false)
	private BigInteger id;
	
	/**
	 * Identificador del impuesto
	 */
	@Column(name = "IDIMPUESTO", length = 4, nullable = false)
	private Integer idImpuesto;
	
	/**
	 * descripción corta del impuesto
	 */
	@Column(name = "DESCCORTAIMP", length = 100, nullable = false)
	private String descCortaImp;
	
	/**
	 * Descripción larga del impuesto
	 */
	@Column(name = "DESCIMP", length = 250, nullable = false)
	private String descImp;
	
	/**
	 * Índice soportado deducible
	 */
	@Column(name = "INDSOPORTDED", length = 1, nullable = true)
	private Integer indSoportDed;
	
	/**
	 * Sub-impuesto relacionado
	 */
	@Column(name = "SUBIMPUESTO", length = 4, nullable = true)
	private Integer subImpuesto;
	
	/**
	 * Descripción corta del sub-impuesto
	 */
	@Column(name = "DESCCORTASUB", length = 100, nullable = false)
	private String descCortaSub;
	
	/**
	 * Descripción larga del sub-impuesto
	 */
	@Column(name = "DESCSUB", length = 250, nullable = false)
	private String descSub;
	
	/**
	 * indicador que indica que  requiere porcentaje
	 */
	@Column(name = "INDRECPORC", length = 1, nullable = true)
	private Integer indRecPorc;
	
	/**
	 * porcentaje del impuesto
	 */
	@Column(name = "PORCENTAJE", length = 3, nullable = true)
	private Integer porcentaje;
	
	/**
	 * porcentaje estándar del impuesto
	 */
	@Column(name = "PORCENTAJESTD", length = 1, nullable = true)
	private Character porcentajEst;


	/**
	 * Constructor
	 */
	public Tmct0CodigoImpuesto() {
		LOG.info("Construido Tmct0CodigoImpuesto");
	}

	/**
	 * Constructor con parámetros
	 */
	public Tmct0CodigoImpuesto(BigInteger id,
			Integer idImpuesto,
			String descCortaImp,
			String descImp,
			Integer indSoportDed,
			Integer subImpuesto,
			String descCortaSub,
			String descSub,
			Integer indRecPorc,
			Integer porcentaje,
			Character porcentajEst) {

		this.id = id;
		this.idImpuesto = idImpuesto;
		this.descCortaImp = descCortaImp;
		this.descImp = descImp;
		this.indSoportDed = indSoportDed;
		this.subImpuesto = subImpuesto;
		this.descCortaSub = descCortaSub;
		this.descSub = descSub;
		this.indRecPorc = indRecPorc;
		this.porcentaje = porcentaje;
		this.porcentajEst = porcentajEst;
	}

	/**
	 * @return the id
	 */
	public BigInteger getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigInteger id) {
		this.id = id;
	}

	/**
	 * @return the idImpuesto
	 */
	public Integer getIdImpuesto() {
		return idImpuesto;
	}

	/**
	 * @param idImpuesto the idImpuesto to set
	 */
	public void setIdImpuesto(Integer idImpuesto) {
		this.idImpuesto = idImpuesto;
	}

	/**
	 * @return the descCortaImp
	 */
	public String getDescCortaImp() {
		return descCortaImp;
	}

	/**
	 * @param descCortaImp the descCortaImp to set
	 */
	public void setDescCortaImp(String descCortaImp) {
		this.descCortaImp = descCortaImp;
	}

	/**
	 * @return the descImp
	 */
	public String getDescImp() {
		return descImp;
	}

	/**
	 * @param descImp the descImp to set
	 */
	public void setDescImp(String descImp) {
		this.descImp = descImp;
	}

	/**
	 * @return the indSoportDed
	 */
	public Integer getIndSoportDed() {
		return indSoportDed;
	}

	/**
	 * @param indSoportDed the indSoportDed to set
	 */
	public void setIndSoportDed(Integer indSoportDed) {
		this.indSoportDed = indSoportDed;
	}

	/**
	 * @return the subImpuesto
	 */
	public Integer getSubImpuesto() {
		return subImpuesto;
	}

	/**
	 * @param subImpuesto the subImpuesto to set
	 */
	public void setSubImpuesto(Integer subImpuesto) {
		this.subImpuesto = subImpuesto;
	}

	/**
	 * @return the descCortaSub
	 */
	public String getDescCortaSub() {
		return descCortaSub;
	}

	/**
	 * @param descCortaSub the descCortaSub to set
	 */
	public void setDescCortaSub(String descCortaSub) {
		this.descCortaSub = descCortaSub;
	}

	/**
	 * @return the descSub
	 */
	public String getDescSub() {
		return descSub;
	}

	/**
	 * @param descSub the descSub to set
	 */
	public void setDescSub(String descSub) {
		this.descSub = descSub;
	}

	/**
	 * @return the indRecPorc
	 */
	public Integer getIndRecPorc() {
		return indRecPorc;
	}

	/**
	 * @param indRecPorc the indRecPorc to set
	 */
	public void setIndRecPorc(Integer indRecPorc) {
		this.indRecPorc = indRecPorc;
	}

	/**
	 * @return the porcentaje
	 */
	public Integer getPorcentaje() {
		return porcentaje;
	}

	/**
	 * @param porcentaje the porcentaje to set
	 */
	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}

	/**
	 * @return the porcentajEst
	 */
	public Character getPorcentajEst() {
		return porcentajEst;
	}

	/**
	 * @param porcentajEst the porcentajEst to set
	 */
	public void setPorcentajEst(Character porcentajEst) {
		this.porcentajEst = porcentajEst;
	}

}
