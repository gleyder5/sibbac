package sibbac.business.interfacesemitidas.rest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.interfacesemitidas.bo.Tmct0ExpedidasFilesBo;
import sibbac.business.interfacesemitidas.database.model.Tmct0ExpedidasFiles;
import sibbac.business.interfacesemitidas.dto.ErroresFicheroModalDTO;
import sibbac.business.interfacesemitidas.dto.assembler.RespFacturasEmitidasAssembler;
import sibbac.business.interfacesemitidas.dto.pantallas.BusquedaRespuestaErrGCDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.RespuestaErroresExpedidasFilterDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceConsultarRespuestaErroresGC implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceConsultarRespuestaErroresGC.class);
	
	@Value("${interfacesemitidas.folder.in.tratados}")
	private String folderInTratados;
	
	@Value("${interfacesemitidas.folder.out.tratados}")
	private String folderOutTratados;
	
	@Value("${interfacesemitidas.respuesta:RESPSR}")
	private String respuesta;
	
	@Value("${interfacesemitidas.respuesta.sufijo:RESP}")
	private String respuestaSufijo;
	
	@Autowired
	private RespFacturasEmitidasAssembler respFacturasEmitidasAssembler;
	
	@Autowired
	private Tmct0ExpedidasFilesBo tmct0ExpedidasFilesBo;
	
  /**
   * The Enum ComandosRespuestaErroresGC.
   */
	private enum ComandosRespuestaErroresGC {

		/** Inicializa los datos del filtro de pantalla */
		CARGA_SELECTS("CargaSelects"),
		BUSCAR_RESPUESTA_ERRORES_GC("BuscarRespuestaErroresGC"),
		VER_ERRORES_FICHERO("VerErroresDelFichero"),
		DESCARGAR_FICHERO("DescargarFichero"),
		SIN_COMANDO("");

		/** The command. */
		private String command;

		/**
		 * Instantiates a new comandos Respuesta Errores GC.
		 * @param command the command
		 */
		private ComandosRespuestaErroresGC(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}
	
	@Override
	public WebResponse process(WebRequest webRequest)
			throws SIBBACBusinessException {
		Map<String, String> paramsFilters = webRequest.getFilters();
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandosRespuestaErroresGC command = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceConsultarRespuestaErroresGC");
			switch (command) {
			case CARGA_SELECTS:
				result.setResultados(this.cargarSelectsFiltro());
				break;
			case BUSCAR_RESPUESTA_ERRORES_GC:
				result.setResultados(this.buscarRespuestaErrGC(paramsFilters));
				break;
			case VER_ERRORES_FICHERO:
				result.setResultados(this.verErroresFichero(paramsObjects));
				break;
			case DESCARGAR_FICHERO:
				result.setResultados(this.descargarFichero(paramsObjects));
				break;
			default:
				result.setError("An action must be set. Valid actions are: " + command);
			}
		} catch (SIBBACBusinessException e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError("ERROR");
		}

		LOG.debug("Salgo del servicio SIBBACServiceConsultarRespuestaErroresGC");
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

	/**
	   * Carga inicial de los datos de selects de la pantalla.
	   * 
	   * @return
	   * @throws SIBBACBusinessException
	   */
	private Map<String, Object> cargarSelectsFiltro() throws SIBBACBusinessException {
		final Map<String, Object> catalogos;

		LOG.trace("[cargarSelectsFiltro] Iniciando datos para el filtro de Estados Fichero - cargarEstadosFichero");
		catalogos = new HashMap<>();
		try {
			catalogos.put("listaNombreFicheros", tmct0ExpedidasFilesBo.getCatalogoNombresFicheros());
			catalogos.put("listaTiposFichero", tmct0ExpedidasFilesBo.getCatalogoTipoFicheros());
			catalogos.put("listaEstadosFichero", tmct0ExpedidasFilesBo.getCatalogoEstados());
			return Collections.<String,Object>singletonMap("catalogos", catalogos);
		} 
		catch (Exception e) {
			LOG.error("[cargarSelectsFiltro] Error metodo init - Estados Fichero", e);
			throw new SIBBACBusinessException(
					"Error en la carga de datos iniciales de las opciones de busqueda - cargarEstadosFichero");
		}
	}
	
	/**
	 * Se recobra lista generada en la busqueda de respuestas con error del GC.
	 * @param 	paramsFilters
	 * @return	Map<String, Object>
	 * @throws 	SIBBACBusinessException
	 */
	private Map<String, Object> buscarRespuestaErrGC(Map<String, String> paramsFilters) 
			throws SIBBACBusinessException {
		LOG.debug("[buscarRespuestaErrGC] Inicio");
		RespuestaErroresExpedidasFilterDTO rErrExpFilterDTO = 
				this.respFacturasEmitidasAssembler.getRespErroresExpFilterDto(paramsFilters);
		List<BusquedaRespuestaErrGCDTO> listaBusquedaRespuestaErrGCDTO = 
			this.tmct0ExpedidasFilesBo.findRespuestaErroresGC(rErrExpFilterDTO);
		LOG.debug("[buscarRespuestaErrGC] Fin");
		return Collections.<String,Object>singletonMap("listaBusquedaRespuestaErrGCDTO", listaBusquedaRespuestaErrGCDTO);
	}
	
	/**
	 * 	Ver errores del fichero.
	 *	@param paramsObject 
	 * 	@return Map<String, Object>
	 * 	@throws SIBBACBusinessException
	 */
	 private Map<String, Object> verErroresFichero(
			 Map<String, Object> paramsObject) throws SIBBACBusinessException {
		 LOG.debug("[verErroresFichero] Inicio");
		 List<ErroresFicheroModalDTO> listaErroresFicheroModalDTO = 
				 this.tmct0ExpedidasFilesBo.getListadoErroresModalByNombreFichero(
						 (String) paramsObject.get("nombreFichero"));
		 LOG.debug("[verErroresFichero] Fin");
		 return Collections.<String,Object>singletonMap("listaErroresFicheroModalDTO", listaErroresFicheroModalDTO);
	 }
		
	/**
	 * Descargar fichero.
	 * 
	 * @param paramsObject
	 * @return Map<String, Object>
	 * @throws SIBBACBusinessException
	 * @throws IOException 
	 */
	private Map<String, Object> descargarFichero(Map<String, Object> paramsObject) throws SIBBACBusinessException, IOException {
		LOG.debug("[descargarFichero] Inicio");
		Map<String, Object> resultado = new HashMap<>();
		Tmct0ExpedidasFiles file = this.tmct0ExpedidasFilesBo.findById(new Long((Integer) paramsObject.get("id")));
		Path path = null;
		if (file.isEmision(respuesta, respuestaSufijo)) {
			path = Paths.get(folderOutTratados, file.getNbFichero());
		} else {
			path = Paths.get(folderInTratados, file.getNbFichero());
		}
		byte[] data = Files.readAllBytes(path);
		resultado.put("fileName", file.getNbFichero());
		resultado.put("fileContent", data);
		LOG.debug("[descargarFichero] Fin");
		return resultado;
	}
	
	
	/**
	 * Gets the command.
	 *
	 * @param command the command
	 * @return the command
	 */
	private ComandosRespuestaErroresGC getCommand(String command) {
		ComandosRespuestaErroresGC[] commands = ComandosRespuestaErroresGC.values();
		ComandosRespuestaErroresGC result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandosRespuestaErroresGC.SIN_COMANDO;
		}
		LOG.debug("[getCommand] Se selecciona la accion: {}", result.getCommand());
		return result;
	}
	
}
