package sibbac.business.interfacesemitidas.database.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_MONEDA
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_MONEDA")
public class Tmct0Monedas {

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0Monedas.class);

	/** Campos de la entidad */

	/**
	 * Identificador del registro en la tabla (autogenerado)
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 4, nullable = false)
	private Integer id;
	
	/**
	 * Fecha en la que actual de la creación o última modificación
	 */
	@Column(name = "AUDIT_DATE", length = 10, nullable = false)
	private Date auditDate;

	/**
	 * identificador del usuario que creó o modificó por última vez la factura
	 */
	@Column(name = "AUDIT_USER", length = 255, nullable = false)
	private String auditUser;
	
	/**
	 * versión
	 */
	@Column(name = "VERSION", length = 8, nullable = false)
	private BigInteger version;
	
	/**
	 * Descripción de la moneda
	 */
	@Column(name = "NBDESCRIPCION", length = 50, nullable = false)
	private String nbDescripcion;
	
	/**
	 * Código de la moneda
	 */
	@Column(name = "NBCODIGO", length = 3, nullable = false)
	private String nbCodigo;

	/**
	 * Constructor
	 */
	public Tmct0Monedas() {
		LOG.info("Construido Tmct0Monedas");
	}

	/**
	 * Constructor con parámetros
	 */
	public Tmct0Monedas(Integer id,
			Date auditDate,
			String auditUser,
			BigInteger version,
			String nbDescripcion,
			String nbCodigo) {

		this.id = id;
		this.auditDate = auditDate;
		this.auditUser = auditUser;
		this.version = version;
		this.nbDescripcion = nbDescripcion;
		this.nbCodigo = nbCodigo;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the auditDate
	 */
	public Date getAuditDate() {
		return auditDate;
	}

	/**
	 * @param auditDate the auditDate to set
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @return the auditUser
	 */
	public String getAuditUser() {
		return auditUser;
	}

	/**
	 * @param auditUser the auditUser to set
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * @return the version
	 */
	public BigInteger getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(BigInteger version) {
		this.version = version;
	}

	/**
	 * @return the nbDescripcion
	 */
	public String getNbDescripcion() {
		return nbDescripcion;
	}

	/**
	 * @param nbDescripcion the nbDescripcion to set
	 */
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}

	/**
	 * @return the nbCodigo
	 */
	public String getNbCodigo() {
		return nbCodigo;
	}

	/**
	 * @param nbCodigo the nbCodigo to set
	 */
	public void setNbCodigo(String nbCodigo) {
		this.nbCodigo = nbCodigo;
	}

	
}
