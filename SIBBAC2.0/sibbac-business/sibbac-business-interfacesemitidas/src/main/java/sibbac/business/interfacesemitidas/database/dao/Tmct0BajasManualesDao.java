package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0BajasManualesDao extends JpaRepository<Tmct0BajasManuales, BigInteger> {

  @Query("SELECT f FROM Tmct0BajasManuales f WHERE (f.enviado is null or f.enviado = false)")
  public List<Tmct0BajasManuales> findNoEnviadas();

  @Query("SELECT f FROM Tmct0BajasManuales f WHERE f.estado.idestado <> :estado and (f.enviado is null or f.enviado = false) and f.fhFechaCre >= :fechaDesde and f.fhFechaCre <= :fechaHasta")
  public List<Tmct0BajasManuales> findNoEnviadasByIdEstadoDistinctBetweenFechas(@Param("estado") Integer estado,
                                                                                @Param("fechaDesde") Date fechaDesde,
                                                                                @Param("fechaHasta") Date fechaHasta);

  @Query("SELECT f FROM Tmct0BajasManuales f WHERE f.nbDocNumero = :nbDocNumero")
  public Tmct0BajasManuales findBynbDocNumero(@Param("nbDocNumero") BigInteger nbDocNumero);
  
  @Modifying @Query("UPDATE Tmct0BajasManuales SET enviado = true WHERE id = :id")
  public int marcaEnviado(@Param("id") BigInteger id);


}
