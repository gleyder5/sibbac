  package sibbac.business.interfacesemitidas.tasks;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.interfacesemitidas.bo.EjecucionFacturasEmitidas;
import sibbac.business.interfacesemitidas.bo.EjecucionRespuestaFacturas;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_INTEFACES_EMITIDAS.NAME, name = Task.GROUP_INTEFACES_EMITIDAS.JOB_INTERFACES_EMITIDAS, interval = 1, intervalUnit = IntervalUnit.DAY, startTime = "04:00:00")
public class TaskInterfacesFacturasEmitidas extends SIBBACTask {

  private static final Logger LOG = LoggerFactory.getLogger(TaskInterfacesFacturasEmitidas.class);

  @Autowired
  private EjecucionRespuestaFacturas ejecucionRespuestaFacturas;

  @Autowired
  private EjecucionFacturasEmitidas ejecucionFacturasEmitidas;

  @Autowired
  private Tmct0menBo tmct0menBo;

  @Value("${interfacesemitidas.folder.out}")
  private String folderOut;

  private static NumberFormat nF1 = new DecimalFormat("000");

  @Override
  public void execute() {
    DateHelper dateHelper = new DateHelper();

    Tmct0men tmct0men = null;

    LOG.debug("[execute] Inicio");
    try {
      tmct0men = tmct0menBo.findByCdmensa(nF1.format(TIPO_APUNTE.TASK_EMISION_FACTURAS.getTipo()));

      if (!dateHelper.isDateSameToCurrent(tmct0men.getFhaudit())) {
        tmct0men = tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_EMISION_FACTURAS, TMCT0MSC.LISTO_GENERAR,
            TMCT0MSC.EN_EJECUCION);

        LOG.info("Carpeta Salida interfaces emitidas: {}", folderOut);
        ejecucionFacturasEmitidas.ejecutarEmisionFicheros();
        // Se permite ejecutar la respuesta independiente del resultado baja - emision.
        ejecucionRespuestaFacturas.ejecutarRespuestaFicheros();
        tmct0menBo.putEstadoMEN(TIPO_APUNTE.TASK_EMISION_FACTURAS, TMCT0MSC.EN_EJECUCION, TMCT0MSC.LISTO_GENERAR);
        LOG.debug("[execute] Fin");
      }
      else {
        LOG.debug("[La tarea ya ha sido ejecutada anteriormente]");
      }

    }
    catch (SIBBACBusinessException | RuntimeException e) {
      LOG.error("Error de proceso al procesar interfaces de factura", e);
      if (tmct0men != null) {
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      }
    }
  }

}