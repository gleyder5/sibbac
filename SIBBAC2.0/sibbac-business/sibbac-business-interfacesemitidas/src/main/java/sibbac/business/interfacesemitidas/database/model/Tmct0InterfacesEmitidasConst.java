package sibbac.business.interfacesemitidas.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_INTERFACES_EMITIDAS_CONST.
 */
@Entity
@Table(name = "TMCT0_INTERFACES_EMITIDAS_CONST")
public class Tmct0InterfacesEmitidasConst implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2273798098778270842L;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0InterfacesEmitidasConst() {
		super();
	}
	
	@Id
	@Column(name = "ID", nullable = false, length = 8)
	private Long id;
	
	@Column(name = "GRUPO", nullable = false, length = 130)
	private String grupo;
	
	@Column(name = "CLAVE", nullable = false, length = 130)
	private String clave;
	
	@Column(name = "VALOR", nullable = false, length = 250)
	private String valor;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
