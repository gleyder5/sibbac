package sibbac.business.interfacesemitidas.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entidad para la gestion de TMCT0_CLAVE_REGIMEN
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_CLAVE_REGIMEN")
public class Tmct0ClaveRegimen {

	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ClaveRegimen.class);

	/** Campos de la entidad */

	/**
	 * Identificador del registro en la tabla (autogenerado)
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 4, nullable = false)
	private Integer id;
	
	/**
	 * contendrá una breve descripción de la clave régimen
	 */
	@Column(name = "VALOR", length = 500, nullable = false)
	private String valor;

	/**
	 * Constructor
	 */
	public Tmct0ClaveRegimen() {
		LOG.info("Construido Tmct0ClaveRegimen");
	}

	/**
	 * Constructor con parámetros
	 */
	public Tmct0ClaveRegimen(Integer id,
			String valor) {

		this.id = id;
		this.valor = valor;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
