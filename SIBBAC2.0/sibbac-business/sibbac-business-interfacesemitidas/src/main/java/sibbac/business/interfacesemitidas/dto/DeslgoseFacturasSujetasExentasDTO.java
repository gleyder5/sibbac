package sibbac.business.interfacesemitidas.dto;

import java.math.BigDecimal;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 06
 * 
 * @author adrian.prause
 *
 */
public class DeslgoseFacturasSujetasExentasDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private String indicadorEntregaBien;
	private BigDecimal baseImponibleExenta;
	private String codigoImpuesto;
	private String codigoSubtipoImpuesto;

	@Override
	public String toString() {
		String desgloseFacturasSujetasExentas = "";
		try {
			desgloseFacturasSujetasExentas = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padRight(indicadorEntregaBien, 1);
					if (null!=baseImponibleExenta && baseImponibleExenta.signum() < 0) {
						desgloseFacturasSujetasExentas = desgloseFacturasSujetasExentas + StringHelper.formatBigDecimalWithNSign(baseImponibleExenta, 16, 2);
					} else {
						desgloseFacturasSujetasExentas = desgloseFacturasSujetasExentas + " " + StringHelper.formatBigDecimalWithNSign(baseImponibleExenta, 15, 2);
					}
					
					desgloseFacturasSujetasExentas = desgloseFacturasSujetasExentas + StringHelper.padRight(codigoImpuesto, 2) + StringHelper.padRight(codigoSubtipoImpuesto, 2);
					
			return StringHelper.padRight(desgloseFacturasSujetasExentas, 2449);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public String getIndicadorEntregaBien() {
		return indicadorEntregaBien;
	}

	public void setIndicadorEntregaBien(String indicadorEntregaBien) {
		this.indicadorEntregaBien = indicadorEntregaBien;
	}

	public BigDecimal getBaseImponibleExenta() {
		return baseImponibleExenta;
	}

	public void setBaseImponibleExenta(BigDecimal baseImponibleExenta) {
		this.baseImponibleExenta = baseImponibleExenta;
	}

	public String getCodigoImpuesto() {
		return codigoImpuesto;
	}

	public void setCodigoImpuesto(String codigoImpuesto) {
		this.codigoImpuesto = codigoImpuesto;
	}

	public String getCodigoSubtipoImpuesto() {
		return codigoSubtipoImpuesto;
	}

	public void setCodigoSubtipoImpuesto(String codigoSubtipoImpuesto) {
		this.codigoSubtipoImpuesto = codigoSubtipoImpuesto;
	}

}
