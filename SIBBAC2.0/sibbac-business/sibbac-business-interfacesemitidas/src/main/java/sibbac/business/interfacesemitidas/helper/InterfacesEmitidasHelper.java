package sibbac.business.interfacesemitidas.helper;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.CodigosRetornoBloque01DatosFacturaEnum;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.InterfazEnum;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.common.utils.SibbacEnums.EstadosEnum;

/**
 * Contiene metodos de utilidad para la funcionalidad de Interfaces emitidas.
 */
public class InterfacesEmitidasHelper {

	/**
	 *	Devuelve true si el tipo de registro es el indicado en el argumento.
	 *
	 *	@param row: es una fila de un fichero
	 *	@param String: Tipo de registro de la enum
	 *	@return boolean
	 */
	public static boolean isBloqueByTipoRegistro(String row, String tipoRegistro) {
		return (StringHelper.safeNull(row)).substring(0, 2).equals(tipoRegistro);
	}
	
	/**
	 * 	Devuelve true si el codigo de retorno es el indicado en el argumento.
	 * 	Se utiliza para el formato del BLOQUE 01 de respuesta.
	 * 
	 *	@param row: es una fila de un fichero
	 *	@param String: Codigo de retorno de la enum
	 *	@return boolean
	 */
	public static boolean isEstadoOperacionByCodigoRetorno(String row, String codigoRetorno) {
		return (StringHelper.safeNull(row)).substring(239, 241).equals(codigoRetorno);
	}
	
	/**
	 * 	Devuelve true si el nombre del fichero procesado es respuesta GI.
	 * 	Ejemplo de fichero respuesta GI: DESA.E0402.F170301.EXP.V0001.RESP
	 * 	@param fileName
	 * 	@return boolean
	 */
	public static boolean isFicheroRespuestaGI(String fileName) {
		String[] fragmentosFichero  = StringHelper.safeNull(fileName).split("\\.");
		if (fragmentosFichero != null && fragmentosFichero.length >= 6) {
			return StringHelper.safeNull(fragmentosFichero[5]).equalsIgnoreCase("RESP");
		}
		return false;
	}
	
	/**
	 * 	Devuelve true si el nombre del fichero procesado es respuesta SII.
	 * 	Ejemplo de fichero respuesta SII: DESA.E0402.RESPR.F170301.EXP.V0001  
	 * 	@param fileName
	 * 	@return boolean
	 */
	public static boolean isFicheroRespuestaSII(String fileName, String respuesta) {
		String[] fragmentosFichero  = StringHelper.safeNull(fileName).split("\\.");
		if (fragmentosFichero != null && fragmentosFichero.length >= 6) {
			return StringHelper.safeNull(fragmentosFichero[2]).equalsIgnoreCase(respuesta);
		}
		return false;
	}
	
	/**
	 * 	Devuelve true si el nombre del fichero procesado es de tipo alta/modificacion.
	 * 	Ejemplo de fichero respuesta SII tipo A/M: 	DESA.E0402.RESPR.F170301.EXP.V0001  
	 * 	Ejemplo de fichero respuesta GI tipo A/M: 	DESA.E0402.F170301.EXP.V0001.RESP 
	 * 	@param fileName
	 * 	@return boolean
	 */
	public static boolean isFicheroAltaModif(String fileName, String respuesta) {
		String[] fragmentosFichero  = StringHelper.safeNull(fileName).split("\\.");
		if (fragmentosFichero != null && fragmentosFichero.length >= 6) {
			if (isFicheroRespuestaSII(fileName, respuesta)) {
				return StringHelper.safeNull(fragmentosFichero[4]).equalsIgnoreCase("EXP");
			} else if (isFicheroRespuestaGI(fileName)) {
				return StringHelper.safeNull(fragmentosFichero[3]).equalsIgnoreCase("EXP");
			}
		}
		return false;
	}
	
	/**
	 * 	Devuelve true si el nombre del fichero procesado es de tipo baja.
	 * 	Ejemplo de fichero respuesta SII tipo baja: DESA.E0402.RESPR.F170301.EXB.V0001 
	 * 	Ejemplo de fichero respuesta GI tipo baja: 	DESA.E0402.F170301.EXB.V0001.RESP 
	 * 	@param fileName
	 * 	@return boolean
	 */
	public static boolean isFicheroBaja(String fileName, String respuesta) {
		String[] fragmentosFichero  = StringHelper.safeNull(fileName).split("\\.");
		if (fragmentosFichero != null && fragmentosFichero.length >= 6) {
			if (isFicheroRespuestaSII(fileName, respuesta)) {
				return StringHelper.safeNull(fragmentosFichero[4]).equalsIgnoreCase("EXB");
			} else if (isFicheroRespuestaGI(fileName)) {
				return StringHelper.safeNull(fragmentosFichero[3]).equalsIgnoreCase("EXB");
			}
		}
		return false;
	}
	
	/**
	 *	Devuelve el fichero asociado de envio en base al nomrbe del fichero de respuesta.
	 *	@param 	fileName
	 *	@return String 
	 */
	public static String getAsociacionRespByNombreFichero(String fileName, String respuesta) {
		if (StringUtils.isNotBlank(fileName)) {
			if (isFicheroRespuestaSII(fileName, respuesta)) {
				return StringUtils.replace(fileName, "." + respuesta, "");
			} else if (isFicheroRespuestaGI(fileName)) {
				return StringUtils.replace(fileName, ".RESP", "");
			}
		}
		return null;
	}
	
	
	/**
	 *	Devuelve el tipo de fichero segun el nombre del fichero.
	 *	@param fileName
	 *	@return String  
	 */
	public static String getTipoFicheroByNombreFichero(String fileName, String respuesta) {
		if (StringUtils.isNotBlank(fileName)) {
			if (isFicheroRespuestaSII(fileName, respuesta)) {
				return InterfazEnum.SII_EXPEDIDAS.getId();
			} else if (isFicheroRespuestaGI(fileName)) {
				return InterfazEnum.GC_EXPEDIDAS.getId();
			}
		}
		return null;
	}
	
	/**
	 * 	Devuelve el estado segun el codigo de retorno de datos de la respuesta.
	 * 	@param codigoRetorno
	 * 	@return String
	 */
	public static String getEstadoByCodigoRetorno(String codigoRetorno) {
		if (codigoRetorno.equalsIgnoreCase(
				CodigosRetornoBloque01DatosFacturaEnum.OPERACION_ACEPTADA.getId())) {
			return EstadosEnum.OK.getTexto();
		}
		return EstadosEnum.KO.getTexto();
	}
	
	/**
	 * 	Devuelve el periodo.
	 * 	@return periodo
	 */
	public static int getPeriodo() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		//Calculamos el periodo
		//((1 mes menos del actual * 6) + (1 dia menos del actual / 5) + 1)
		int mesAnterior = calendar.get(Calendar.MONTH);
		int diaAnterior = calendar.get(Calendar.DATE) - 1;
		int periodo = ((mesAnterior * 6) + (diaAnterior / 5)) + 1;
		return periodo;
	}
	
	/**
	 *	Devuelve el numero de factura formateado.
	 *	El formato a devolver: 
	 *	identif (8 para FM y 7 para RM) + ultimas 2 cifras del anio + nro completado por 0s a la izquierda	
	 *	@param identif identif
	 *	@param nbDocNumeroOrdenado nbDocNumeroOrdenado
	 *	@return BigInteger BigInteger
	 */
	public static BigInteger getNumeroFacturaFormateado(String identif, BigInteger nbDocNumeroOrdenado) {
		String nbFormatted = 
				identif 
				+ String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2, 4) 
				+ StringUtils.leftPad(String.valueOf(nbDocNumeroOrdenado.longValue()), 5, "0");
		return new BigInteger(nbFormatted);
	}
	
	public static BigInteger getNumeroFacturaFormateadoBaja(String identif, BigInteger nbDocNumeroOrdenado) {
		String nbFormatted = 
				"9"
				+ identif 
				+ String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2, 4) 
				+ StringUtils.leftPad(String.valueOf(nbDocNumeroOrdenado.longValue()), 4, "0");
		return new BigInteger(nbFormatted);
	}
	
	/**
	 *	Devuelve el numero de factura que sera modificado para la baja. 
	 *	El formato a devolver: 
	 *	9 + identif (8 para FM y 7 para RM) + ultimas 2 cifras del anio + 4 nros completado por 0s a la izquierda 
	 */
	public static BigInteger getNumeroFacturaBaja(BigInteger nbDocNumero) {
		String nbFormattedBaja = 
			"9" + String.valueOf(nbDocNumero).substring(0, 3) + String.valueOf(nbDocNumero).substring(4, 8);
		return new BigInteger(nbFormattedBaja);
	}
}