package sibbac.business.interfacesemitidas.utils;

/**
 * The Class Constantes.
 */
public class Constantes {

	/** Prefijo para Entidad tipo Facturas Manuales. */
	public static final String PREFJIJO_FACTURA_MANUAL = "M"; 
  
	/** Prefijo para Entidad tipo Facturas Rectificativas. */
	public static final String PREFJIJO_FACTURA_RECTIF = "R";
	
	/** Prefijo para Entidad tipo Facturas Manuales Baja. */
	public static final String PREFJIJO_FACTURA_MANUAL_BAJA = "MB"; 
  
	/** Prefijo para Entidad tipo Facturas Rectificativas Baja. */
	public static final String PREFJIJO_FACTURA_RECTIF_BAJA = "RB";
	
	/** Identificador factura manual para nro de factura. */
	public static final String IDENTIFICADOR_FACTURA_MANUAL = "8"; 
	
	/** Identificador factura rectificativa manual para nro de factura. */
	public static final String IDENTIFICADOR_FACTURA_RECTIF = "7"; 
	
	/** Identificador factura GRUPO IVA. */
	public static final String IDENTIFICADOR_FACTURA_GRUPO_IVA = "6"; 

	/** Identificador factura rectificativa GRUPO IVA. */
	public static final String IDENTIFICADOR_FACTURA_RECTIFICATIVA_GRUPO_IVA = "5"; 
}
