package sibbac.business.interfacesemitidas.dto;

import java.math.BigInteger;
import java.util.Date;


/**
 * DTO para la gestion de TMCT0_FACTURA_MANUAL
 * @author Neoris
 *
 */
public class Tmct0FiltroFacturasManualesDTO {

	/**
	 * fecha desde
	 */
	private Date fechaDesde;
	
	/**
	 * fecha hasta
	 */
	private Date fechaHasta;
	
	/**
	 * número de la factura (autogenerado)
	 */
	private BigInteger nbDocNumero;

	/**
	 * identificador del alias asociado al cliente
	 */
	private BigInteger idAlias;

	/**
	 * identificador del de la tabla TMCT0ESTADO
	 */
	private Integer idEstado;

	
	public Tmct0FiltroFacturasManualesDTO () {

	}

	/**
	 * Constructor con parámetros
	 */
	public Tmct0FiltroFacturasManualesDTO(Date fechaDesde,
			Date fechaHasta,
			BigInteger nbDocNumero,
			BigInteger idAlias,
			Integer idEstado) {

		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.nbDocNumero = nbDocNumero;
		this.idAlias = idAlias;
		this.idEstado = idEstado;
	}

	/**
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return fechaDesde;
	}

	/**
	 * @param fechaDesde the fechaDesde to set
	 */
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * @param fechaHasta the fechaHasta to set
	 */
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	/**
	 * @return the nbDocNumero
	 */
	public BigInteger getNbDocNumero() {
		return nbDocNumero;
	}

	/**
	 * @param nbDocNumero the nbDocNumero to set
	 */
	public void setNbDocNumero(BigInteger nbDocNumero) {
		this.nbDocNumero = nbDocNumero;
	}

	/**
	 * @return the idAlias
	 */
	public BigInteger getIdAlias() {
		return idAlias;
	}

	/**
	 * @param idAlias the idAlias to set
	 */
	public void setIdAlias(BigInteger idAlias) {
		this.idAlias = idAlias;
	}

	/**
	 * @return the idEstado
	 */
	public Integer getIdEstado() {
		return idEstado;
	}

	/**
	 * @param idEstado the idEstado to set
	 */
	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

}
