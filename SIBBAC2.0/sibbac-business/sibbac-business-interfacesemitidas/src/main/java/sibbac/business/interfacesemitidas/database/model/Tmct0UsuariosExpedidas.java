package sibbac.business.interfacesemitidas.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la gestion de TMCT0_USUARIOS_EXPEDIDAS.
 */
@Entity
@Table(name = "TMCT0_USUARIOS_EXPEDIDAS")
public class Tmct0UsuariosExpedidas implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6997636262064381203L;

	/**
	 *	Constructor no-arg. 
	 */
	public Tmct0UsuariosExpedidas() {
		super();
	}
	
	@Id
	@Column(name = "ID", nullable = false, length = 8)
	private Long id;
	
	@Column(name = "NOMBRE_USUARIO", nullable = false, length = 200)
	private String nombreUsuario;
	
	@Column(name = "EMAIL", nullable = false, length = 200)
	private String email;
	
	@Column(name = "DEPARTAMENTO", nullable = true, length = 200)
	private String departamento;

	@Column(name = "FHALTA", nullable = true)
	private Date fhAlta;
	
	@Column(name = "FHBAJA", nullable = true)
	private Date fhBaja;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public Date getFhAlta() {
		return this.fhAlta;
	}

	public void setFhAlta(Date fhAlta) {
		this.fhAlta = fhAlta;
	}

	public Date getFhBaja() {
		return this.fhBaja;
	}

	public void setFhBaja(Date fhBaja) {
		this.fhBaja = fhBaja;
	}
	
}
