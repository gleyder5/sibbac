package sibbac.business.interfacesemitidas.rest;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.interfacesemitidas.bo.Tmct0FacturasManualesBo;
import sibbac.business.interfacesemitidas.database.dao.Tmct0InterfacesEmitidasConstDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativaBajasManualesDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativasManualesDAOImpl;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;
import sibbac.business.interfacesemitidas.dto.ClienteDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0RectificativasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0RectificativasManualesTablaDTO;
import sibbac.business.interfacesemitidas.dto.assembler.Tmct0FacturasManualesAssembler;
import sibbac.business.interfacesemitidas.dto.assembler.Tmct0RectificativasManualesAssembler;
import sibbac.business.interfacesemitidas.helper.GeneradorPdf;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceRectificativasManuales implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceFacturasManuales.class);

	private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

	@Autowired
	Tmct0FacturasManualesAssembler tmct0FacturasManualesAssembler;

	@Autowired
	Tmct0RectificativasManualesAssembler tmct0RectificativasManualesAssembler;

	@Autowired
	Tmct0RectificativaBajasManualesDao tmct0RectificativaBajasManualesDao;

	@Autowired
	Tmct0RectificativasManualesDAO tmct0RectificativasManualesDao;

	@Autowired
	Tmct0RectificativasManualesDAOImpl tmct0RectificativasManualesDaoImpl;

	@Autowired
	private AliasDao aliasdao;

	@Autowired
	private Tmct0aliDao tmct0AliDao;

	@Autowired
	private Tmct0FacturasManualesBo tmct0FacturasManualesBo;

	@Autowired
	private Tmct0estadoDao estadoDao;

	@Autowired
	private Tmct0InterfacesEmitidasConstDao interfacesEmitidasConstDao;

	@Autowired
	private GeneradorPdf generadorPdf;

	/**
	 * The Enum ComandoFacturasManuales.
	 */
	private enum ComandoFacturasRectificativasManuales {

		/** Crear un factura manual con los datos Recibidos */
		CREAR_RECTIFICATIVA_MANUAL("crearRectificativaManual"),

		/** Modificar un factura manual con los datos Recibidos */
		MODIFICAR_RECTIFICATIVA_MANUAL("modificarRectificativaManual"),

		/** Borrar un factura manual con los datos Recibidos */
		BORRAR_RECTIFICATIVAS_MANUALES("borrarRectificativasManuales"),

		/** Consultar un factura manual con los datos Recibidos */
		CONSULTAR_RECTIFICATIVAS_MANUALES("consultarRectificativasManuales"),

		/** Consulta el detalle de una factura manual con los datos Recibidos */
		DETALLE_RECTIFICATIVA_MANUAL("detalleRectificativaManual"),

		/** Consulta del cliente */
		CONSULTA_CLIENTE("consultarCliente"),

		/** crear y contabilizar */
		CONTABILIZAR_CREAR("crearContabilizar"),

		/** modificar y contabilizar */
		CONTABILIZAR_MODIFICAR("modificarContabilizar"),

		/** Contabilizar */
		CONTABILIZAR("contabilizar"),

		/** Consulta el próximo número de factura que se asignará */
		CONSULTAR_PROX_NUMFACT("consultarProximoNumeroRectificativa"),
		
	    /** Consulta el próximo número de factura grupo iva que se asignará */
	    CONSULTAR_PROX_NUMFACT_GRUPO_IVA("consultarProximoNumeroFacturaRectificativaGrupoIva"),

		/** Anular bajas manuales */
		ANULAR_RECTIFICATIVAS_MANUALES("anularRectificativasManuales"),

		/** Exportar la factura a PDF */
		EXPORTAR_PDF("exportarPdf"),

		SIN_COMANDO("");

		/** The command. */
		private String command;

		/**
		 * Instantiates a new comandos Plantillas.
		 *
		 * @param command the command
		 */
		private ComandoFacturasRectificativasManuales(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
	 */
	/**
	 * Process.
	 *
	 * @param webRequest the web request
	 * @return the web response
	 * @throws SIBBACBusinessException the SIBBAC business exception
	 */
	@Override
	public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandoFacturasRectificativasManuales command = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceFacturasManuales");

			switch (command) {
			case CONSULTAR_RECTIFICATIVAS_MANUALES:
				result.setResultados(consultarRectificativasManuales(paramsObjects));
				break;
			case CREAR_RECTIFICATIVA_MANUAL:
				result.setResultados(crearRectificativaManual(paramsObjects));
				break;
			case MODIFICAR_RECTIFICATIVA_MANUAL:
				result.setResultados(modificarRectificativaManual(paramsObjects));
				break;
			case DETALLE_RECTIFICATIVA_MANUAL:
				result.setResultados(detalleRectificativaManual(paramsObjects));
				break;
			case BORRAR_RECTIFICATIVAS_MANUALES:
				result.setResultados(eliminarRectificativasManuales(paramsObjects));
				break;
			case CONSULTA_CLIENTE:
				result.setResultados(consultarCliente(paramsObjects));
				break;
			case CONTABILIZAR_CREAR:
				result.setResultados(crearYContabilizar(paramsObjects));
				break;
			case CONTABILIZAR_MODIFICAR:
				result.setResultados(modificarYContabilizar(paramsObjects));
				break;
			case CONTABILIZAR:
				result.setResultados(contabilizar(paramsObjects, true));
				break;
			case CONSULTAR_PROX_NUMFACT:
				result.setResultados(consultarProxNumFact(false));
				break;
			case CONSULTAR_PROX_NUMFACT_GRUPO_IVA:
				result.setResultados(consultarProxNumFact(true));
				break;
			case ANULAR_RECTIFICATIVAS_MANUALES:
				result.setResultados(anularRectificativasManuales(paramsObjects));
				break;
			case EXPORTAR_PDF:
				result.setResultados(exportarPdf(paramsObjects));
				break;

			default:
				result.setError("An action must be set. Valid actions are: " + command);
			} // switch
		} catch (SIBBACBusinessException e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(MESSAGE_ERROR);
		}

		LOG.debug("Salgo del servicio SIBBACServiceFacturasManuales");
		return result;
	}

	/**
	 * Consulta de las facturas rectificativas manuales a raiz de los datos insertados en pantalla
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarRectificativasManuales(Map<String, Object> paramObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - consultarRectificativasManuales");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> listaRectificativasManualesTablaDTO = new ArrayList<Object>();
		List<String> listCdAliass = new ArrayList<String>();
		List<Tmct0RectificativaManual> listaRectificativasManuales = new ArrayList<Tmct0RectificativaManual>();
		List<Tmct0RectificativaBajasManuales> listaRectificativasManualesBaja = new ArrayList<Tmct0RectificativaBajasManuales>();

		try {
			// Transformamos los parámetros de filtrado en un objeto
			Tmct0FiltroFacturasManualesDTO filtroRectificativasManuales = tmct0RectificativasManualesAssembler.getTmct0FiltroFacturasManualesDTO(paramObjects);

			// Recuperamos el ID ESTADO que pertenece al literal FACTURA BAJA
			Tmct0estado estadoFAct = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_BAJA)
					.getValor());

			// Si en el filtro se ha seleccionado el estado FACTURA BAJA se recuperará el listado de la tabla
			// FACTURA_MANUAL_RECTIFICATIVA_BAJA
			if (null != filtroRectificativasManuales && null != filtroRectificativasManuales.getIdEstado()
					&& filtroRectificativasManuales.getIdEstado().equals(estadoFAct.getIdestado())) {
				listaRectificativasManualesBaja = tmct0RectificativasManualesDaoImpl.consultarRectificativasManualesBaja(filtroRectificativasManuales);

				// Se guardan en un array todos los idAlias (cdAliass) para recuperar las descripciones asociadas a ellos
				for (Tmct0RectificativaBajasManuales tmct0RectificativasManualesBaja : listaRectificativasManualesBaja) {
					listCdAliass.add(tmct0RectificativasManualesBaja.getIdAlias().toString());
				}

				// Se transforman a lista de tablaDTO
				for (int i = 0; i < listaRectificativasManualesBaja.size(); i++) {
					try {
						String descrAli = aliasdao.findDescraliByCdaliass(listCdAliass.get(i));
						Tmct0RectificativasManualesTablaDTO rectificativasManualesTasblaDTO = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesTablaDTO(listaRectificativasManualesBaja.get(i),
								descrAli);
						listaRectificativasManualesTablaDTO.add(rectificativasManualesTasblaDTO);
					} catch (Exception e) {
						LOG.error("Error metodo consultarRectificativasManuales - " + e.getMessage(), e);
						throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
					}
				}
			} else {
				// Realizamos la llamada a la consulta de facturas manuales
				listaRectificativasManuales = tmct0RectificativasManualesDaoImpl.consultarRectificativasManuales(filtroRectificativasManuales);

				// Se guardan en un array todos los idAlias (cdAliass) para recuperar las descripciones asociadas a ellos
				for (Tmct0RectificativaManual tmct0RectificativasManuales : listaRectificativasManuales) {
					listCdAliass.add(tmct0RectificativasManuales.getIdAlias().toString());
				}

				// Se transforman a lista de tablaDTO
				for (int i = 0; i < listaRectificativasManuales.size(); i++) {
					try {
						String descrAli = aliasdao.findDescraliByCdaliass(listCdAliass.get(i));
						Tmct0RectificativasManualesTablaDTO rectificativasManualesTasblaDTO = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesTablaDTO(listaRectificativasManuales.get(i),
								descrAli);
						listaRectificativasManualesTablaDTO.add(rectificativasManualesTasblaDTO);
					} catch (Exception e) {
						LOG.error("Error metodo consultarRectificativasManuales - " + e.getMessage(), e);
						throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
					}
				}
			}

		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo consultarRectificativasManuales -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("listaRectificativasManuales", listaRectificativasManualesTablaDTO);

		LOG.info("FIN - SERVICIO - consultarRectificativasManuales");

		return result;
	}

	/**
	 * Crea una factura manual a raiz de los datos insertados en pantalla
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> crearRectificativaManual(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - crearRectificativaManual");

		Map<String, Object> result = new HashMap<String, Object>();

		try {
			// Transformamos los parámetros de la visual en Entidad
			Tmct0RectificativaManual filtroRectificativaManual = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesEntidad(paramsObjects,
					false);

			// Comprobamos si el alias existe
			List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroRectificativaManual.getIdAlias()));

			// Si no existe el alias se informa al usuario
			if (null == listaAlias || listaAlias.size() == 0) {
				LOG.error("Se esta intentanto crear una factura manual con con una empresa contraparte que no existe -  "
						+ filtroRectificativaManual.getNbDocNumero());
				result.put("existeAlias", "NO");
				return result;
			}

			// Comprobamos si el número de factura ya existe
			List<Tmct0RectificativaManual> listaRectificativasManuales = tmct0RectificativasManualesDao.findBynbDocNumero(filtroRectificativaManual.getNbDocNumero());

			// Si ya hay una factura con ese número se informa
			if (listaRectificativasManuales.size() > 0) {
				LOG.error("Se esta intentanto crear una factura manual rectificativa con el mismo número de factura que una ya existente -  "
						+ filtroRectificativaManual.getNbDocNumero());
				throw new SIBBACBusinessException(
						"Se esta intentanto crear una factura manual rectificativa con el mismo número de factura que una ya existente "
								+ filtroRectificativaManual.getNbDocNumero());
			}

			Tmct0estado estadoFActNew = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_DEFECTO_CREACION)
					.getValor());

			if (estadoFActNew != null) {
				filtroRectificativaManual.setEstado(estadoFActNew);
			} else {
				throw new SIBBACBusinessException(
						"No se ha podido determinar el estado inicial de la factura, consulte con el administrador ");
			}
			// Realizamos la llamada a la creación de factura manual
			tmct0RectificativasManualesDao.save(filtroRectificativaManual);// crearFacturaManual(filtroFacturaManual);

			// Indicamos que el resultado de la creación en la BBDD ha sido correcto
			result.put("status", "OK");

		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo crearRectificativaManual -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - crearRectificativaManual");

		return result;
	}

	/**
	 * Modifica una factura manual a raiz de los datos insertados en pantalla
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> modificarRectificativaManual(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - modificarRectificativaManual");

		Map<String, Object> result = new HashMap<String, Object>();

		try {
			// Transformamos los parámetros de la visual en Entidad
			Tmct0RectificativaManual filtroRectificativaManual = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesEntidad(paramsObjects,
					true);

			// Comprobamos si el alias existe
			List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroRectificativaManual.getIdAlias()));

			// Si no existe el alias se informa al usuario
			if (null == listaAlias || listaAlias.size() == 0) {
				LOG.error("Se esta intentanto modificar una factura manual rectificativa con con una empresa contraparte que no existe -  "
						+ filtroRectificativaManual.getNbDocNumero());
				result.put("existeAlias", "NO");
				return result;
			}

			// Realizamos la llamada a la creación de factura manual
			tmct0RectificativasManualesDao.save(filtroRectificativaManual);

			// Indicamos que el resultado de la creación en la BBDD ha sido correcto
			result.put("status", "OK");

		} catch (Exception e) {
			LOG.error("Error metodo modificarRectificativaManual -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - modificarRectificativaManual");

		return result;
	}

	/**
	 * Consulta el detalle de una factura manual
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> detalleRectificativaManual(Map<String, Object> paramObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - detalleRectificativaManual");

		Map<String, Object> result = new HashMap<String, Object>();
		Object objRectificativaManualDTO = new Object();

		try {
			// MFG Si el numero empieza por 9 se obtiene de las de bajas y sino de las normales
			String sNbDocumento = String.valueOf(paramObjects.get("nbDocNumero"));
			if (sNbDocumento.startsWith("9")) { // Factura de baja
				try {
					// Se obtiene la entidad por el id de la tabla de bajas
					Tmct0RectificativaBajasManuales tmct0FacturasRectManualBaja = tmct0RectificativaBajasManualesDao.findOne(new BigInteger(
							String.valueOf(paramObjects.get("id"))));

					// Convertimos la entidad Tmct0FacturasManualesDTO
					Tmct0RectificativasManualesDTO facturasRectificiativaBajaDTO = tmct0RectificativasManualesAssembler.getTmct0RectificativasBajaDetalleDTO(tmct0FacturasRectManualBaja);

					objRectificativaManualDTO = facturasRectificiativaBajaDTO;
				} catch (Exception e) {
					LOG.error("Error metodo detalleFacturaManual - " + e.getMessage(), e);
					throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales baja a DTO");
				}

			} else {
				// Se obtiene la entidad por el id de la tabla
				Tmct0RectificativaManual facturaRectificativaManual = tmct0RectificativasManualesDao.findOne(new BigInteger(
						String.valueOf(paramObjects.get("id"))));
				try {
					// Convertimos la entidad y el clientesDTO recuperados a Tmct0FacturasManualesDTO
					Tmct0RectificativasManualesDTO facturasRectificativaDTO = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesDetalleDTO(facturaRectificativaManual);

					objRectificativaManualDTO = facturasRectificativaDTO;
				} catch (Exception e) {
					LOG.error("Error metodo detalleFacturaManual - " + e.getMessage(), e);
					throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
				}
			}

			//      List<Tmct0RectificativaManual> listaRectificativasManuales = tmct0RectificativasManualesDao.findBynbDocNumero(new BigInteger(
			//                                                                                                                                   String.valueOf(paramObjects.get("nbDocNumero"))));

			// Se transforman a lista de tablaDTO
			//      for (Tmct0RectificativaManual tmct0RectificativasManuales : listaRectificativasManuales) {
			//        try {
			//          // Convertimos la entidad y el clientesDTO recuperados a Tmct0FacturasRectificativasDTO
			//          Tmct0RectificativasManualesDTO rectificativasManualesDTO = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesDetalleDTO(tmct0RectificativasManuales);
			//
			//          objRectificativaManualDTO = rectificativasManualesDTO;
			//        } catch (Exception e) {
			//          LOG.error("Error metodo detalleRectificativaManual - " + e.getMessage(), e);
			//          throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
			//        }
			//      }

		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo detalleRectificativaManual -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("detalleRectificativaManual", objRectificativaManualDTO);

		LOG.info("FIN - SERVICIO - detalleRectificativaManual");

		return result;
	}

	/**
	 * Consulta el cliente a raiz de un idAlias
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarCliente(Map<String, Object> paramObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - consultarCliente");

		Map<String, Object> result = new HashMap<String, Object>();
		Object clienteDTO = new Object();

		try {
			// Hacemos la llamada a BBDD para recuperar los datos de cliente a raiz del idAlias de la entidad
			ClienteDTO cliente = tmct0RectificativasManualesDaoImpl.consultarClienteByAlias(new BigInteger(
					String.valueOf(paramObjects.get("idAlias"))));

			clienteDTO = cliente;
		} catch (Exception e) {
			LOG.error("Error metodo consultarCliente - " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("cliente", clienteDTO);

		LOG.info("FIN - SERVICIO - consultarCliente");

		return result;
	}

	/**
	 * Elimina las facturas manuales seleccionadas.
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> eliminarRectificativasManuales(Map<String, Object> paramsObject) throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - eliminarRectificativasManuales");

		Map<String, Object> result = new HashMap<String, Object>();

		try {

			List<LinkedHashMap<?,?>> listaRMsBorrar = (List<LinkedHashMap<?,?>>) paramsObject.get("listaRectificativasManualesBorrar");
			List<BigInteger> listIdsRectificativasManualesBorrar = new ArrayList<BigInteger>();

			if (CollectionUtils.isNotEmpty(listaRMsBorrar)) {
				for (int x = 0; x < listaRMsBorrar.size(); x++) {
					LinkedHashMap<?,?> map = listaRMsBorrar.get(x);

					// Marcado modificable.
					Boolean isModificable = map != null ? (Boolean) map.get("esModificable") : null;
					// Id de rectificativa manual a borrar.
					Integer id = map.get("id") != null ? (Integer) map.get("id") : null;

					// Solo se pueden borrar las facturas marcadas como modificables.
					if (BooleanUtils.isTrue(isModificable) && id != null) {
						listIdsRectificativasManualesBorrar.add(new BigInteger(String.valueOf(id)));
					} else {
						throw new SIBBACBusinessException("Error: El registro a borrar está en un estado no modificable");
					}
				}
				this.tmct0RectificativasManualesDao.deleteFromId(listIdsRectificativasManualesBorrar);
			} else {
				throw new SIBBACBusinessException("Error: No hay registros a borrar.");
			}
		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo eliminarRectificativasManuales -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("status", "OK");

		LOG.info("FIN - SERVICIO - eliminarRectificativasManuales");

		return result;
	}

	/**
	 * Crea una factura manual a raiz de los datos insertados en pantalla
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> crearYContabilizar(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - crearYContabilizar");

		Map<String, Object> result = new HashMap<String, Object>();

		try {
			// Transformamos los parámetros de la visual en Entidad
			Tmct0RectificativaManual filtroRectificativaManual = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesEntidad(paramsObjects,
					false);

			// Comprobamos si el alias existe
			List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroRectificativaManual.getIdAlias()));

			// Si no existe el alias se informa al usuario
			if (null == listaAlias || listaAlias.size() == 0) {
				LOG.error("Se esta intentanto crear una factura manual rectificativa con con una empresa contraparte que no existe -  "
						+ filtroRectificativaManual.getNbDocNumero());
				result.put("existeAlias", "NO");
				return result;
			}

			// Comprobamos si el número de factura ya existe
			List<Tmct0RectificativaManual> listaRectificativasManuales = tmct0RectificativasManualesDao.findBynbDocNumero(filtroRectificativaManual.getNbDocNumero());

			// Si ya hay una factura con ese número se informa
			if (listaRectificativasManuales.size() > 0) {
				LOG.error("Se esta intentanto crear una factura manual rectificativa con el mismo número de factura que una ya existente -  "
						+ filtroRectificativaManual.getNbDocNumero());
				throw new SIBBACBusinessException(
						"Se esta intentanto crear una factura manual rectificativa con el mismo número de factura que una ya existente "
								+ filtroRectificativaManual.getNbDocNumero());
			}

			// Se asigna a la factura el estado Pte de contabilizar
			Tmct0estado estadoFActCont = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_PDTE_CONTABILIZAR)
					.getValor());

			if (estadoFActCont != null) {
				filtroRectificativaManual.setEstado(estadoFActCont);
			} else {
				throw new SIBBACBusinessException(
						"No se ha podido determinar el estado  PTE de contabilizar.  Consulte con el administrador  ");
			}

			// Realizamos la llamada a la creación de factura manual rectificativa
			tmct0RectificativasManualesDao.save(filtroRectificativaManual);

			// Indicamos que el resultado de la creación en la BBDD ha sido correcto
			result.put("status", "OK");

		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo crearYContabilizar -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - crearYContabilizar");

		return result;
	}

	/**
	 * Modifica una factura manual a raiz de los datos insertados en pantalla
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> modificarYContabilizar(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - modificarYContabilizar");

		Map<String, Object> result = new HashMap<String, Object>();

		try {
			// Transformamos los parámetros de la visual en Entidad
			Tmct0RectificativaManual filtroRectificativaManual = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesEntidad(paramsObjects,
					true);

			// Comprobamos si el alias existe
			List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroRectificativaManual.getIdAlias()));

			// Si no existe el alias se informa al usuario
			if (null == listaAlias || listaAlias.size() == 0) {
				LOG.error("Se esta intentanto crear una factura manual rectificativa con con una empresa contraparte que no existe -  "
						+ filtroRectificativaManual.getNbDocNumero());
				result.put("existeAlias", "NO");
				return result;
			}

			// Se asigna a la factura el estado Pte de contabilizar
			Tmct0estado estadoFActCont = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_PDTE_CONTABILIZAR)
					.getValor());

			if (estadoFActCont != null) {
				filtroRectificativaManual.setEstado(estadoFActCont);
			} else {
				throw new SIBBACBusinessException(
						"No se ha podido determinar el estado  PTE de contabilizar.  Consulte con el administrador  ");
			}

			// Realizamos la llamada a la creación de factura manual
			tmct0RectificativasManualesDao.save(filtroRectificativaManual);

			// Indicamos que el resultado de la creación en la BBDD ha sido correcto
			result.put("status", "OK");

		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo modificarYContabilizar -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - modificarYContabilizar");

		return result;
	}

	/**
	 * Contabilizar
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> contabilizar(Map<String, Object> paramsObjects, boolean esModificacion) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - contabilizar");

		Map<String, Object> result = new HashMap<String, Object>();

		try {
			// Transformamos los parámetros de la visual en Entidad
			Tmct0RectificativaManual filtroRectificativaManual = tmct0RectificativasManualesAssembler.getTmct0RectificativasManualesEntidad(paramsObjects,
					esModificacion);

			// Se asigna a la factura el estado Pte de contabilizar
			Tmct0estado estadoFActCont = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_PDTE_CONTABILIZAR)
					.getValor());

			if (estadoFActCont != null) {
				filtroRectificativaManual.setEstado(estadoFActCont);
			} else {
				throw new SIBBACBusinessException(
						"No se ha podido determinar el estado  PTE de contabilizar.  Consulte con el administrador  ");
			}

			// Realizamos la modificación del estado
			tmct0RectificativasManualesDao.save(filtroRectificativaManual);

			// Indicamos que el resultado de la creación en la BBDD ha sido correcto
			result.put("status", "OK");

		} catch (SIBBACBusinessException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error metodo contabilizar -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - contabilizar");

		return result;
	}

	/**
	 * Contabilizar
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> consultarProxNumFact(boolean esGrupoIva) throws SIBBACBusinessException, ParseException {

		LOG.info("INICIO - SERVICIO - consultarProxNumFact");

		Map<String, Object> result = new HashMap<String, Object>();
		BigInteger proxNumFact;

		try {
			// Hacemos la llamada a BBDD para recuperar los datos de cliente a raiz del idAlias de la entidad
			proxNumFact = tmct0RectificativasManualesDaoImpl.getNbDocNumeroRectificativa(esGrupoIva, false);
		} catch (Exception e) {
			LOG.error("Error metodo consultarProxNumFact - " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		result.put("proximoNumeroRectificativa", proxNumFact);

		LOG.info("FIN - SERVICIO - consultarProxNumFact");

		return result;
	}

	/**
	 * Elimina las facturas manuales seleccionadas.
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> anularRectificativasManuales(Map<String, Object> paramsObject) throws SIBBACBusinessException {

		LOG.info("INICIO - SERVICIO - anularRectificativasManuales");

		Map<String, Object> result = new HashMap<String, Object>();
		List<Integer> listaFMAnular = (List<Integer>) paramsObject.get("listaRectificativasManualesAnular");

		for (Integer idFatura : listaFMAnular) {
			try {

				BigInteger biFactura = BigInteger.valueOf(idFatura.intValue());
				this.tmct0FacturasManualesBo.anularFacturaManualRectif(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF,
						biFactura);
			} catch (Exception e) {
				LOG.error("Error metodo anularRectificativasManuales - Anulando Factura con id  " + idFatura + " Exception"
						+ e.getMessage(), e);
				result.put("status", "KO");
				return result;
			}
		}

		result.put("status", "OK");

		LOG.info("FIN - SERVICIO - anularRectificativasManuales");

		return result;
	}

	/**
	 * Exporta a pdf la factura seleccionada
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 * @throws ParseException
	 */
	private Map<String, Object> exportarPdf(Map<String, Object> paramObjects) throws SIBBACBusinessException,
	ParseException {

		LOG.info("INICIO - SERVICIO - exportarPdf");

		Map<String, Object> result = new HashMap<String, Object>();
		Object[] objPdf = new Object[1];
		boolean esBaja = false;

		try {

			BigInteger sNbDocumento = new BigInteger(String.valueOf(paramObjects.get("nbDocNumero")));

			if (String.valueOf(paramObjects.get("nbDocNumero")).startsWith("9")) { // Factura de baja
				objPdf = tmct0RectificativasManualesDaoImpl.consultarDatosPdf(sNbDocumento,true);
				esBaja = true;
			} else{
				objPdf = tmct0RectificativasManualesDaoImpl.consultarDatosPdf(sNbDocumento,false);
			}

			Map<String, String> mapConstantes = tmct0RectificativasManualesDaoImpl.consultarConstantes(Constantes.GRUPO_GENERADORPDF);

			if (null!=objPdf && objPdf.length>0 && null!=objPdf[0] && null!=mapConstantes && mapConstantes.size()>0) {
				if (String.valueOf(paramObjects.get("claveRegimen")).startsWith("6")) {
	        		result = generadorPdf.generarPdfFacturasRectificativasGrupoIVA(objPdf,mapConstantes);
	        	} else {
	        		result = generadorPdf.generarPdfFacturas(objPdf,mapConstantes,true,esBaja);
	        	}
			} else {
				LOG.error("Error metodo exportarPdf -  Alguno de los datos del cliente necesarios para la generación del PDF no está en BBDD");
				result.put("status", "KO");
				return result;
			}

		} catch (Exception e) {
			LOG.error("Error metodo exportarPdf -  " + e.getMessage(), e);
			result.put("status", "KO");
			return result;
		}

		LOG.info("FIN - SERVICIO - exportarPdf");

		return result;
	}

	/**
	 * Gets the command.
	 *
	 * @param command the command
	 * @return the command
	 */
	private ComandoFacturasRectificativasManuales getCommand(String command) {
		ComandoFacturasRectificativasManuales[] commands = ComandoFacturasRectificativasManuales.values();
		ComandoFacturasRectificativasManuales result = null;
		for (int i = 0; i < commands.length; i++) {
			if (commands[i].getCommand().equalsIgnoreCase(command)) {
				result = commands[i];
			}
		}
		if (result == null) {
			result = ComandoFacturasRectificativasManuales.SIN_COMANDO;
		}
		LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

}
