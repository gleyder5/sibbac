package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0CodigoImpuesto;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0CodigosImpuestosDAO extends JpaRepository<Tmct0CodigoImpuesto,BigInteger>{
	
	@Query("SELECT codImp FROM Tmct0CodigoImpuesto codImp ORDER BY codImp.idImpuesto ASC")
	public List<Tmct0CodigoImpuesto> findAllOrderByIdImpuesto();
	
	@Query("SELECT codImp FROM Tmct0CodigoImpuesto codImp where id = :id")
	public Tmct0CodigoImpuesto getById(@Param("id") BigInteger id);

}
