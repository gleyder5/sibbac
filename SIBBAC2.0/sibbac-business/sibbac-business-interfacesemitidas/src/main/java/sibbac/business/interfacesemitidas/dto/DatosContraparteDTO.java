package sibbac.business.interfacesemitidas.dto;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Datos de contraparte. Forma parte de los datos de factura, bloque 01.
 * 
 * @author adrian.prause
 *
 */
public class DatosContraparteDTO {

	private String apellidosNombresRazonSocial;
	private String tipoIdentificacionRepresentanteLegal;
	private String identificacionRepresentanteLegal;
	private String tipoIdentificacionContraparte;
	private String identificacionContraparte;
	private String idOtroCodigoPais;
	private String idOtroIdType;
	private String idOtroId;

	@Override
	public String toString() {
		try {
			return StringHelper.padRight(apellidosNombresRazonSocial, 40)
					+ StringHelper.padRight(tipoIdentificacionRepresentanteLegal, 2)
					+ StringHelper.padRight(identificacionRepresentanteLegal, 150)
					+ StringHelper.padRight(tipoIdentificacionContraparte, 2)
					+ StringHelper.padRight(identificacionContraparte, 150) + StringHelper.padRight(idOtroCodigoPais, 2)
					+ StringHelper.padRight(idOtroIdType, 2) + StringHelper.padRight(idOtroId, 150);
		} catch (Exception e) {
			return "";
		}
	}

	public String getApellidosNombresRazonSocial() {
		return apellidosNombresRazonSocial;
	}

	public void setApellidosNombresRazonSocial(String apellidosNombresRazonSocial) {
		this.apellidosNombresRazonSocial = apellidosNombresRazonSocial;
	}

	public String getTipoIdentificacionRepresentanteLegal() {
		return tipoIdentificacionRepresentanteLegal;
	}

	public void setTipoIdentificacionRepresentanteLegal(String tipoIdentificacionRepresentanteLegal) {
		this.tipoIdentificacionRepresentanteLegal = tipoIdentificacionRepresentanteLegal;
	}

	public String getIdentificacionRepresentanteLegal() {
		return identificacionRepresentanteLegal;
	}

	public void setIdentificacionRepresentanteLegal(String identificacionRepresentanteLegal) {
		this.identificacionRepresentanteLegal = identificacionRepresentanteLegal;
	}

	public String getTipoIdentificacionContraparte() {
		return tipoIdentificacionContraparte;
	}

	public void setTipoIdentificacionContraparte(String tipoIdentificacionContraparte) {
		this.tipoIdentificacionContraparte = tipoIdentificacionContraparte;
	}

	public String getIdentificacionContraparte() {
		return identificacionContraparte;
	}

	public void setIdentificacionContraparte(String identificacionContraparte) {
		this.identificacionContraparte = identificacionContraparte;
	}

	public String getIdOtroCodigoPais() {
		return idOtroCodigoPais;
	}

	public void setIdOtroCodigoPais(String idOtroCodigoPais) {
		this.idOtroCodigoPais = idOtroCodigoPais;
	}

	public String getIdOtroIdType() {
		return idOtroIdType;
	}

	public void setIdOtroIdType(String idOtroIdType) {
		this.idOtroIdType = idOtroIdType;
	}

	public String getIdOtroId() {
		return idOtroId;
	}

	public void setIdOtroId(String idOtroId) {
		this.idOtroId = idOtroId;
	}

}
