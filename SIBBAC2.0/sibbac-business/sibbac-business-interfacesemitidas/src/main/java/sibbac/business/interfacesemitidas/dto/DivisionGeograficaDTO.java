package sibbac.business.interfacesemitidas.dto;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 09
 * 
 * @author adrian.prause
 *
 */
public class DivisionGeograficaDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private String domicilioContraparte;
	private String domicilioOficinaOperacion;
	private String domicilioBien;

	@Override
	public String toString() {
		String divisionGeografica = "";
		try {
			divisionGeografica = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padRight(domicilioContraparte, 4)
					+ StringHelper.padRight(domicilioOficinaOperacion, 2) + StringHelper.padRight(domicilioBien, 4);
			return StringHelper.padRight(divisionGeografica, 2449);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public String getDomicilioContraparte() {
		return domicilioContraparte;
	}

	public void setDomicilioContraparte(String domicilioContraparte) {
		this.domicilioContraparte = domicilioContraparte;
	}

	public String getDomicilioOficinaOperacion() {
		return domicilioOficinaOperacion;
	}

	public void setDomicilioOficinaOperacion(String domicilioOficinaOperacion) {
		this.domicilioOficinaOperacion = domicilioOficinaOperacion;
	}

	public String getDomicilioBien() {
		return domicilioBien;
	}

	public void setDomicilioBien(String domicilioBien) {
		this.domicilioBien = domicilioBien;
	}

}
