package sibbac.business.interfacesemitidas.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;

/**
 * Datos de contraparte. Forma parte de los datos de factura, dentro del bloque
 * 01.
 * 
 * @author adrian.prause
 *
 */
public class DatosFacturaDTO {

	private String tipoFactura;
	private String tipoFacturaRectificativa;
	private String monedaImportesDatosFactura;
	private BigDecimal baseSustituidaRectificada;
	private BigDecimal cuotaSustituidaRectificada;
	private Date fechaOperacion;
	private BigDecimal importeTotalFactura;
	private String descripcionDeOperacion;
	private BigDecimal importePercibido;
	private String facturaEmitidaPorTerceros;

	private DatosContraparteDTO datosContraparte;
	private DesglosePorFacturaDTO desglosePorFactura;

	@Override
	public String toString() {
		try {
			SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
			String sfechaOperacion = "";
			if (fechaOperacion != null) {
				sfechaOperacion = format.format(fechaOperacion);
			}
			
			String cadena = StringHelper.padRight(tipoFactura, 2) + StringHelper.padRight(tipoFacturaRectificativa, 1)
					+ StringHelper.padRight(monedaImportesDatosFactura, 3);
			if (null!=baseSustituidaRectificada && baseSustituidaRectificada.signum() < 0) {
				cadena = cadena + StringHelper.formatBigDecimalWithNSign(baseSustituidaRectificada, 16, 2);
			} else {
				cadena = cadena + " " + StringHelper.formatBigDecimalWithNSign(baseSustituidaRectificada, 15, 2);
			}
			
			if (null!=cuotaSustituidaRectificada && cuotaSustituidaRectificada.signum() < 0) {
				cadena = cadena + StringHelper.formatBigDecimalWithNSign(cuotaSustituidaRectificada, 16, 2);
			} else {
				cadena = cadena + " " + StringHelper.formatBigDecimalWithNSign(cuotaSustituidaRectificada, 15, 2);
			}
			
			cadena = cadena + StringHelper.padRight(sfechaOperacion, 10);
			if (null!=importeTotalFactura && importeTotalFactura.signum() < 0) {
				cadena = cadena + StringHelper.formatBigDecimalWithNSign(importeTotalFactura, 16, 2);
			} else {
				cadena = cadena + " " + StringHelper.formatBigDecimalWithNSign(importeTotalFactura, 15, 2);
			}
			
			cadena = cadena + StringHelper.padRight(descripcionDeOperacion, 500)
			+ StringHelper.formatBigDecimalWithNSign(importePercibido, 16, 2)
			+ StringHelper.padRight(facturaEmitidaPorTerceros, 1) + datosContraparte + desglosePorFactura;
			
			return cadena;
					
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoFactura() {
		return tipoFactura;
	}

	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}

	public String getTipoFacturaRectificativa() {
		return tipoFacturaRectificativa;
	}

	public void setTipoFacturaRectificativa(String tipoFacturaRectificativa) {
		this.tipoFacturaRectificativa = tipoFacturaRectificativa;
	}

	public String getMonedaImportesDatosFactura() {
		return monedaImportesDatosFactura;
	}

	public void setMonedaImportesDatosFactura(String monedaImportesDatosFactura) {
		this.monedaImportesDatosFactura = monedaImportesDatosFactura;
	}

	public BigDecimal getBaseSustituidaRectificada() {
		return baseSustituidaRectificada;
	}

	public void setBaseSustituidaRectificada(BigDecimal baseSustituidaRectificada) {
		this.baseSustituidaRectificada = baseSustituidaRectificada;
	}

	public BigDecimal getCuotaSustituidaRectificada() {
		return cuotaSustituidaRectificada;
	}

	public void setCuotaSustituidaRectificada(BigDecimal cuotaSustituidaRectificada) {
		this.cuotaSustituidaRectificada = cuotaSustituidaRectificada;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public BigDecimal getImporteTotalFactura() {
		return importeTotalFactura;
	}

	public void setImporteTotalFactura(BigDecimal importeTotalFactura) {
		this.importeTotalFactura = importeTotalFactura;
	}

	public String getDescripcionDeOperacion() {
		return descripcionDeOperacion;
	}

	public void setDescripcionDeOperacion(String descripcionDeOperacion) {
		this.descripcionDeOperacion = descripcionDeOperacion;
	}

	public BigDecimal getImportePercibido() {
		return importePercibido;
	}

	public void setImportePercibido(BigDecimal importePercibido) {
		this.importePercibido = importePercibido;
	}

	public String getFacturaEmitidaPorTerceros() {
		return facturaEmitidaPorTerceros;
	}

	public void setFacturaEmitidaPorTerceros(String facturaEmitidaPorTerceros) {
		this.facturaEmitidaPorTerceros = facturaEmitidaPorTerceros;
	}

	public DatosContraparteDTO getDatosContraparte() {
		return datosContraparte;
	}

	public void setDatosContraparte(DatosContraparteDTO datosContraparte) {
		this.datosContraparte = datosContraparte;
	}

	public DesglosePorFacturaDTO getDesglosePorFactura() {
		return desglosePorFactura;
	}

	public void setDesglosePorFactura(DesglosePorFacturaDTO desglosePorFactura) {
		this.desglosePorFactura = desglosePorFactura;
	}

}
