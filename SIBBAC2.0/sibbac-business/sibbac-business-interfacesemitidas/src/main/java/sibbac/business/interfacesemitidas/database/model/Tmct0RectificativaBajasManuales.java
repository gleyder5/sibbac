package sibbac.business.interfacesemitidas.database.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.model.Tmct0estado;

/**
 * Entidad para la gestion de TMCT0_FACTURA_MANUAL_RECTIFICATIVA_BAJA
 * 
 * @author Neoris
 *
 */
@Entity
@Table(name = "TMCT0_FACTURA_MANUAL_RECTIFICATIVA_BAJA")
public class Tmct0RectificativaBajasManuales {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0RectificativaBajasManuales.class);

  /** Campos de la entidad */

  /**
   * Identificador del registro en la tabla
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", length = 8, nullable = false)
  private BigInteger id;

  /**
   * Fecha en la que actual de la creación o última modificación
   */
  @Column(name = "AUDIT_DATE", length = 10, nullable = false)
  private Date auditDate;

  /**
   * identificador del usuario que creó o modificó por última vez la factura
   */
  @Column(name = "AUDIT_USER", length = 255, nullable = false)
  private String auditUser;

  /**
	 * 
	 */
  @Column(name = "NBCOMENTARIOS", length = 1000, nullable = false)
  private String nbComentarios;

  /**
   * fecha de creación de la factura
   */
  @Column(name = "FHFECHACRE", length = 10, nullable = false)
  private Date fhFechaCre;

  /**
   * fecha de contabilización factura
   */
  @Column(name = "FHFECHAFAC", length = 10, nullable = false)
  private Date fhFechaFac;

  /**
   * número de la factura (autogenerado)
   */
  @Column(name = "NBDOCNUMERO", length = 8, nullable = false)
  private BigInteger nbDocNumero;
  
  /**
   * número de la factura (autogenerado)
   */
  @Column(name = "NBDOCNUMERO_ORIGINAL", length = 8, nullable = false)
  private BigInteger nbDocNumeroOriginal;

  /**
   * número de la factura 0
   */
  @Column(name = "NBDOCNUMERO0", length = 8, nullable = true)
  private BigInteger nbDocNumero0;

  /**
   * número de la factura 1
   */
  @Column(name = "NBDOCNUMERO1", length = 8, nullable = true)
  private BigInteger nbDocNumero1;

  /**
   * número de la factura 2
   */
  @Column(name = "NBDOCNUMERO2", length = 8, nullable = true)
  private BigInteger nbDocNumero2;

  /**
   * número de la factura 3
   */
  @Column(name = "NBDOCNUMERO3", length = 8, nullable = true)
  private BigInteger nbDocNumero3;

  /**
   * número de la factura 4
   */
  @Column(name = "NBDOCNUMERO4", length = 8, nullable = true)
  private BigInteger nbDocNumero4;

  /**
   * número de la factura 5
   */
  @Column(name = "NBDOCNUMERO5", length = 8, nullable = true)
  private BigInteger nbDocNumero5;

  /**
   * número de la factura 6
   */
  @Column(name = "NBDOCNUMERO6", length = 8, nullable = true)
  private BigInteger nbDocNumero6;

  /**
   * número de la factura 7
   */
  @Column(name = "NBDOCNUMERO7", length = 8, nullable = true)
  private BigInteger nbDocNumero7;

  /**
   * número de la factura 8
   */
  @Column(name = "NBDOCNUMERO8", length = 8, nullable = true)
  private BigInteger nbDocNumero8;

  /**
   * número de la factura 9
   */
  @Column(name = "NBDOCNUMERO9", length = 8, nullable = true)
  private BigInteger nbDocNumero9;

  /**
   * identificador del alias asociado al cliente
   */
  @Column(name = "ID_ALIAS", length = 8, nullable = false)
  private BigInteger idAlias;

  /**
   * identificador del de la tabla TMCT0ESTADO
   */
  @ManyToOne()
  @JoinColumn(name = "ID_ESTADO")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0estado estado;

  /**
   * identificador de la moneda asociada a la factura extraído de la tabla TMCT0_MONEDA
   */
  @ManyToOne()
  @JoinColumn(name = "ID_MONEDA")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0Monedas moneda;

  /**
   * Se genera en función de la fecha actual (calcular igual que en la query orden de contabilidad)
   */
  @Column(name = "PERIODO", length = 10, nullable = true)
  private Integer periodo;

  /**
   * fecha de creación de la factura
   */
  @Column(name = "FHINICIO", length = 10, nullable = false)
  private Date fhInicio;

  /**
   * identificador del tipo de factura asociado a la factura extraído de la tabla TMCT0_TIPO_FACTURA
   */
  @ManyToOne()
  @JoinColumn(name = "ID_TIPO_FACTURA")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0TipoFactura tipoFactura;

  /**
   * identificador de la causa exención asociada a la factura extraída de la tabla TMCT0_CAUSA_EXENCION
   */
  @ManyToOne()
  @JoinColumn(name = "ID_CAUSA_EXENCION")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0CausaExencion causaExencion;

  /**
   * identificador de la clave régimen asociada a la factura extraída de la tabla TMCT0_CLAVE_REGIMEN
   */
  @ManyToOne()
  @JoinColumn(name = "ID_CLAVEREGIMEN")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0ClaveRegimen claveRegimen;

  /**
   * Importe correspondiente a la parte Sujeta / Exenta o No. importe de total de la factura
   */
  @Column(name = "IMP_BASE_IMPONIBLE", length = 18, nullable = true)
  private BigDecimal impBaseImponible;

  /**
   * según impuesto la cuota que se añade a la base imponible que sumada a ésta da el total de la factura. En caso de
   * exenta sería 0, en caso de no exenta según impuesto
   */
  @Column(name = "IMP_IMPUESTO", length = 18, nullable = true)
  private BigDecimal impImpuesto;

  /**
   * identificador del impuesto asociado a la factura extraída de la tabla TMCT0_CODIGO_IMPUESTO
   */
  @ManyToOne()
  @JoinColumn(name = "ID_COD_IMPUESTO")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0CodigoImpuesto codImpuesto;

  /**
   * contendrá el valor 0 si es emisión por la prestación de un servicio y 1 si es entrega de un bien
   */
  @Column(name = "ID_ENTREGA_BIEN", length = 1, nullable = true)
  private Short idEntregaBien;

  /**
   * este campo será obligatoria si en el campo Clave Regimen Especial o Trascendencia se ha rellenado con el código 07
   * Régimen especial grupo de entidades en IVA (nivel avanzado), podrá ir a cero si el usuario no introduce nada. Es un
   * porcentaje
   */
  @Column(name = "COEF_IMP_COSTES", length = 10, nullable = true)
  private BigDecimal coefImpCostes;

  /**
   * este campo se informara igual que el anterior
   */
  @Column(name = "BASE_IMPON_COSTES", length = 18, nullable = true)
  private BigDecimal baseImponCostes;

  /**
   * es la cuota resultante de aplicar un tipo impositivo a la base imponible
   */
  @Column(name = "CUOTA_REPERCUT", length = 18, nullable = true)
  private BigDecimal cuotaRepercut;

  /**
   * es el importe total de la factura
   */
  @Column(name = "IMFINSVB", length = 18, nullable = true)
  private BigDecimal imFinSvb;

  /**
   * es el importe en divisa
   */
  @Column(name = "IMFINDIV", length = 18, nullable = true)
  private BigDecimal imFinDiv;

  @Column(name = "ENVIADO")
  @XmlAttribute
  protected Boolean enviado;

  /**
   * identificador de la causa exención asociada a la factura extraída de la tabla TMCT0_FACTURAS_SUJETAS
   */
  @ManyToOne()
  @JoinColumn(name = "ID_FACTURAS_SUJETAS", nullable = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0Sujetas tmct0Sujetas;

  /**
   * identificador de la causa exención asociada a la factura extraída de la tabla TMCT0_FACTURAS_EXENTAS
   */
  @ManyToOne()
  @JoinColumn(name = "ID_FACTURAS_EXENTAS")
  @LazyCollection(LazyCollectionOption.FALSE)
  private Tmct0Exentas tmct0Exentas;
  
	/**
	 * Importe correspondiente a la base imponible anterior de una factura rectificativa GRUPO IVA
	 */
	@Column(name = "BASE_IMPONIBLE_ANTERIOR", length = 18, nullable = true)
	private BigDecimal baseImponibleAnterior;
	
	/**
	 * Importe correspondiente a la base imponible actual de una factura rectificativa GRUPO IVA
	 */
	@Column(name = "BASE_IMPONIBLE_ACTUAL", length = 18, nullable = true)
	private BigDecimal baseImponibleActual;

  /**
   * Constructor
   */
  public Tmct0RectificativaBajasManuales() {
    LOG.info("Construido Tmct0FacturasManuales");
  }

  /**
   * Constructor con parámetros
   */
  public Tmct0RectificativaBajasManuales(BigInteger id,
                                         Date auditDate,
                                         String auditUser,
                                         String nbComentarios,
                                         Date fhFechaCre,
                                         Date fhFechaFac,
                                         BigInteger nbDocNumero,
                                         BigInteger nbDocNumeroOriginal,
                                         BigInteger nbDocNumero0,
                                         BigInteger nbDocNumero1,
                                         BigInteger nbDocNumero2,
                                         BigInteger nbDocNumero3,
                                         BigInteger nbDocNumero4,
                                         BigInteger nbDocNumero5,
                                         BigInteger nbDocNumero6,
                                         BigInteger nbDocNumero7,
                                         BigInteger nbDocNumero8,
                                         BigInteger nbDocNumero9,
                                         BigInteger idAlias,
                                         Tmct0estado estado,
                                         Tmct0Monedas moneda,
                                         Integer periodo,
                                         Date fhInicio,
                                         Tmct0TipoFactura tipoFactura,
                                         Tmct0CausaExencion causaExencion,
                                         Tmct0ClaveRegimen claveRegimen,
                                         BigDecimal impBaseImponible,
                                         BigDecimal impImpuesto,
                                         Tmct0CodigoImpuesto codImpuesto,
                                         Short idEntregaBien,
                                         BigDecimal coefImpCostes,
                                         BigDecimal baseImponCostes,
                                         BigDecimal cuotaRepercut,
                                         BigDecimal imFinSvb,
                                         BigDecimal imFinDiv,
                                         Boolean enviado,
                                         Tmct0Sujetas tmct0Sujetas,
                                         Tmct0Exentas tmct0Exentas,
                                         BigDecimal baseImponibleAnterior,
                             			 BigDecimal baseImponibleActual) {
    this.id = id;
    this.auditDate = auditDate;
    this.auditUser = auditUser;
    this.nbComentarios = nbComentarios;
    this.fhFechaCre = fhFechaCre;
    this.fhFechaFac = fhFechaFac;
    this.nbDocNumero = nbDocNumero;
    this.nbDocNumeroOriginal=nbDocNumeroOriginal;
    this.nbDocNumero0 = nbDocNumero0;
    this.nbDocNumero1 = nbDocNumero1;
    this.nbDocNumero2 = nbDocNumero2;
    this.nbDocNumero3 = nbDocNumero3;
    this.nbDocNumero4 = nbDocNumero4;
    this.nbDocNumero5 = nbDocNumero5;
    this.nbDocNumero6 = nbDocNumero6;
    this.nbDocNumero7 = nbDocNumero7;
    this.nbDocNumero8 = nbDocNumero8;
    this.nbDocNumero9 = nbDocNumero9;
    this.idAlias = idAlias;
    this.estado = estado;
    this.moneda = moneda;
    this.periodo = periodo;
    this.fhInicio = fhInicio;
    this.tipoFactura = tipoFactura;
    this.causaExencion = causaExencion;
    this.claveRegimen = claveRegimen;
    this.impBaseImponible = impBaseImponible;
    this.impImpuesto = impImpuesto;
    this.codImpuesto = codImpuesto;
    this.idEntregaBien = idEntregaBien;
    this.coefImpCostes = coefImpCostes;
    this.baseImponCostes = baseImponCostes;
    this.cuotaRepercut = cuotaRepercut;
    this.imFinSvb = imFinSvb;
    this.imFinDiv = imFinDiv;
    this.enviado = enviado;
    this.tmct0Sujetas = tmct0Sujetas;
    this.tmct0Exentas = tmct0Exentas;
	this.baseImponibleAnterior = baseImponibleAnterior;
	this.baseImponibleActual = baseImponibleActual;
  }

  public BigInteger getNbDocNumeroOriginal() {
	  return nbDocNumeroOriginal;
  }

  public void setNbDocNumeroOriginal(BigInteger nbDocNumeroOriginal) {
	  this.nbDocNumeroOriginal = nbDocNumeroOriginal;
  }

/**
 * @return the baseImponibleAnterior
 */
public BigDecimal getBaseImponibleAnterior() {
	return baseImponibleAnterior;
}

/**
 * @param baseImponibleAnterior the baseImponibleAnterior to set
 */
public void setBaseImponibleAnterior(BigDecimal baseImponibleAnterior) {
	this.baseImponibleAnterior = baseImponibleAnterior;
}

/**
 * @return the baseImponibleActual
 */
public BigDecimal getBaseImponibleActual() {
	return baseImponibleActual;
}

/**
 * @param baseImponibleActual the baseImponibleActual to set
 */
public void setBaseImponibleActual(BigDecimal baseImponibleActual) {
	this.baseImponibleActual = baseImponibleActual;
}

/**
   * Calcula el importe total de la factura (con impuestos)
   * 
   * @return
   */
  public BigDecimal getImporteTotal() {
    final BigDecimal importeTotal = getImFinSvb().add(getImporteImpuestos());
    return importeTotal;
  }

  /**
   * Calcula el importe total de los impuestos
   * 
   * @return
   */
  public BigDecimal getImporteImpuestos() {
    return impImpuesto;
  }

  /**
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * @return the auditDate
   */
  public Date getAuditDate() {
    return auditDate;
  }

  /**
   * @param auditDate the auditDate to set
   */
  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  /**
   * @return the auditUser
   */
  public String getAuditUser() {
    return auditUser;
  }

  /**
   * @param auditUser the auditUser to set
   */
  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  /**
   * @return the nbComentarios
   */
  public String getNbComentarios() {
    return nbComentarios;
  }

  /**
   * @param nbComentarios the nbComentarios to set
   */
  public void setNbComentarios(String nbComentarios) {
    this.nbComentarios = nbComentarios;
  }

  /**
   * @return the fhFechaCre
   */
  public Date getFhFechaCre() {
    return fhFechaCre;
  }

  /**
   * @param fhFechaCre the fhFechaCre to set
   */
  public void setFhFechaCre(Date fhFechaCre) {
    this.fhFechaCre = fhFechaCre;
  }

  /**
   * @return the fhFechaFac
   */
  public Date getFhFechaFac() {
    return fhFechaFac;
  }

  /**
   * @param fhFechaFac the fhFechaFac to set
   */
  public void setFhFechaFac(Date fhFechaFac) {
    this.fhFechaFac = fhFechaFac;
  }

  /**
   * @return the nbDocNumero
   */
  public BigInteger getNbDocNumero() {
    return nbDocNumero;
  }

  /**
   * @param nbDocNumero the nbDocNumero to set
   */
  public void setNbDocNumero(BigInteger nbDocNumero) {
    this.nbDocNumero = nbDocNumero;
  }

  /**
   * @return the nbDocNumero0
   */
  public BigInteger getNbDocNumero0() {
    return nbDocNumero0;
  }

  /**
   * @param nbDocNumero0 the nbDocNumero0 to set
   */
  public void setNbDocNumero0(BigInteger nbDocNumero0) {
    this.nbDocNumero0 = nbDocNumero0;
  }

  /**
   * @return the nbDocNumero1
   */
  public BigInteger getNbDocNumero1() {
    return nbDocNumero1;
  }

  /**
   * @param nbDocNumero1 the nbDocNumero1 to set
   */
  public void setNbDocNumero1(BigInteger nbDocNumero1) {
    this.nbDocNumero1 = nbDocNumero1;
  }

  /**
   * @return the nbDocNumero2
   */
  public BigInteger getNbDocNumero2() {
    return nbDocNumero2;
  }

  /**
   * @param nbDocNumero2 the nbDocNumero2 to set
   */
  public void setNbDocNumero2(BigInteger nbDocNumero2) {
    this.nbDocNumero2 = nbDocNumero2;
  }

  /**
   * @return the nbDocNumero3
   */
  public BigInteger getNbDocNumero3() {
    return nbDocNumero3;
  }

  /**
   * @param nbDocNumero3 the nbDocNumero3 to set
   */
  public void setNbDocNumero3(BigInteger nbDocNumero3) {
    this.nbDocNumero3 = nbDocNumero3;
  }

  /**
   * @return the nbDocNumero4
   */
  public BigInteger getNbDocNumero4() {
    return nbDocNumero4;
  }

  /**
   * @param nbDocNumero4 the nbDocNumero4 to set
   */
  public void setNbDocNumero4(BigInteger nbDocNumero4) {
    this.nbDocNumero4 = nbDocNumero4;
  }

  /**
   * @return the nbDocNumero5
   */
  public BigInteger getNbDocNumero5() {
    return nbDocNumero5;
  }

  /**
   * @param nbDocNumero5 the nbDocNumero5 to set
   */
  public void setNbDocNumero5(BigInteger nbDocNumero5) {
    this.nbDocNumero5 = nbDocNumero5;
  }

  /**
   * @return the nbDocNumero6
   */
  public BigInteger getNbDocNumero6() {
    return nbDocNumero6;
  }

  /**
   * @param nbDocNumero6 the nbDocNumero6 to set
   */
  public void setNbDocNumero6(BigInteger nbDocNumero6) {
    this.nbDocNumero6 = nbDocNumero6;
  }

  /**
   * @return the nbDocNumero7
   */
  public BigInteger getNbDocNumero7() {
    return nbDocNumero7;
  }

  /**
   * @param nbDocNumero7 the nbDocNumero7 to set
   */
  public void setNbDocNumero7(BigInteger nbDocNumero7) {
    this.nbDocNumero7 = nbDocNumero7;
  }

  /**
   * @return the nbDocNumero8
   */
  public BigInteger getNbDocNumero8() {
    return nbDocNumero8;
  }

  /**
   * @param nbDocNumero8 the nbDocNumero8 to set
   */
  public void setNbDocNumero8(BigInteger nbDocNumero8) {
    this.nbDocNumero8 = nbDocNumero8;
  }

  /**
   * @return the nbDocNumero9
   */
  public BigInteger getNbDocNumero9() {
    return nbDocNumero9;
  }

  /**
   * @param nbDocNumero9 the nbDocNumero9 to set
   */
  public void setNbDocNumero9(BigInteger nbDocNumero9) {
    this.nbDocNumero9 = nbDocNumero9;
  }

  /**
   * @return the idAlias
   */
  public BigInteger getIdAlias() {
    return idAlias;
  }

  /**
   * @param idAlias the idAlias to set
   */
  public void setIdAlias(BigInteger idAlias) {
    this.idAlias = idAlias;
  }

  /**
   * @return the idEstado
   */
  public Tmct0estado getEstado() {
    return estado;
  }

  /**
   * @param idEstado the idEstado to set
   */
  public void setEstado(Tmct0estado estado) {
    this.estado = estado;
  }

  /**
   * @return the moneda
   */
  public Tmct0Monedas getMoneda() {
    return moneda;
  }

  /**
   * @param idMoneda the moneda to set
   */
  public void setMoneda(Tmct0Monedas moneda) {
    this.moneda = moneda;
  }

  /**
   * @return the periodo
   */
  public Integer getPeriodo() {
    return periodo;
  }

  /**
   * @param periodo the periodo to set
   */
  public void setPeriodo(Integer periodo) {
    this.periodo = periodo;
  }

  /**
   * @return the fhInicio
   */
  public Date getFhInicio() {
    return fhInicio;
  }

  /**
   * @param fhInicio the fhInicio to set
   */
  public void setFhInicio(Date fhInicio) {
    this.fhInicio = fhInicio;
  }

  /**
   * @return the idTipoFactura
   */
  public Tmct0TipoFactura getTipoFactura() {
    return tipoFactura;
  }

  /**
   * @param idTipoFactura the idTipoFactura to set
   */
  public void setTipoFactura(Tmct0TipoFactura tipoFactura) {
    this.tipoFactura = tipoFactura;
  }

  /**
   * @return the idCausaExencion
   */
  public Tmct0CausaExencion getCausaExencion() {
    return causaExencion;
  }

  /**
   * @param idCausaExencion the idCausaExencion to set
   */
  public void setCausaExencion(Tmct0CausaExencion causaExencion) {
    this.causaExencion = causaExencion;
  }

  /**
   * @return the idClaveRegimen
   */
  public Tmct0ClaveRegimen getClaveRegimen() {
    return claveRegimen;
  }

  /**
   * @param idClaveRegimen the idClaveRegimen to set
   */
  public void setClaveRegimen(Tmct0ClaveRegimen claveRegimen) {
    this.claveRegimen = claveRegimen;
  }

  /**
   * @return the impBaseImponible
   */
  public BigDecimal getImpBaseImponible() {
    return impBaseImponible;
  }

  /**
   * @param impBaseImponible the impBaseImponible to set
   */
  public void setImpBaseImponible(BigDecimal impBaseImponible) {
    this.impBaseImponible = impBaseImponible;
  }

  /**
   * @return the impImpuesto
   */
  public BigDecimal getImpImpuesto() {
    return impImpuesto;
  }

  /**
   * @param impImpuesto the impImpuesto to set
   */
  public void setImpImpuesto(BigDecimal impImpuesto) {
    this.impImpuesto = impImpuesto;
  }

  /**
   * @return the idCodImpuesto
   */
  public Tmct0CodigoImpuesto getCodImpuesto() {
    return codImpuesto;
  }

  /**
   * @param idCodImpuesto the idCodImpuesto to set
   */
  public void setCodImpuesto(Tmct0CodigoImpuesto codImpuesto) {
    this.codImpuesto = codImpuesto;
  }

  /**
   * @return the idEntregaBien
   */
  public Short getIdEntregaBien() {
    return idEntregaBien;
  }

  /**
   * @param idEntregaBien the idEntregaBien to set
   */
  public void setIdEntregaBien(Short idEntregaBien) {
    this.idEntregaBien = idEntregaBien;
  }

  /**
   * @return the coefImpCostes
   */
  public BigDecimal getCoefImpCostes() {
    return coefImpCostes;
  }

  /**
   * @param coefImpCostes the coefImpCostes to set
   */
  public void setCoefImpCostes(BigDecimal coefImpCostes) {
    this.coefImpCostes = coefImpCostes;
  }

  /**
   * @return the baseImponCostes
   */
  public BigDecimal getBaseImponCostes() {
    return baseImponCostes;
  }

  /**
   * @param baseImponCostes the baseImponCostes to set
   */
  public void setBaseImponCostes(BigDecimal baseImponCostes) {
    this.baseImponCostes = baseImponCostes;
  }

  /**
   * @return the cuotaRepercut
   */
  public BigDecimal getCuotaRepercut() {
    return cuotaRepercut;
  }

  /**
   * @param cuotaRepercut the cuotaRepercut to set
   */
  public void setCuotaRepercut(BigDecimal cuotaRepercut) {
    this.cuotaRepercut = cuotaRepercut;
  }

  /**
   * @return the imFinSvb
   */
  public BigDecimal getImFinSvb() {
    return imFinSvb;
  }

  /**
   * @param imFinSvb the imFinSvb to set
   */
  public void setImFinSvb(BigDecimal imFinSvb) {
    this.imFinSvb = imFinSvb;
  }

  /**
   * @return the imFinDiv
   */
  public BigDecimal getImFinDiv() {
    return imFinDiv;
  }

  /**
   * @param imFinDiv the imFinDiv to set
   */
  public void setImFinDiv(BigDecimal imFinDiv) {
    this.imFinDiv = imFinDiv;
  }

  public Boolean isEnviado() {
    return enviado;
  }

  public void setEnviado(Boolean enviado) {
    this.enviado = enviado;
  }

  /**
   * @return tmct0Sujetas
   */
  public Tmct0Sujetas getTmct0Sujetas() {
    return this.tmct0Sujetas;
  }

  /**
   * @param tmct0Sujetas the tmct0Sujetas to set
   */
  public void setTmct0Sujetas(Tmct0Sujetas tmct0Sujetas) {
    this.tmct0Sujetas = tmct0Sujetas;
  }

  /**
   * @return tmct0Exentas
   */
  public Tmct0Exentas getTmct0Exentas() {
    return this.tmct0Exentas;
  }

  /**
   * @param tmct0Exentas the tmct0Exentas to set
   */
  public void setTmct0Exentas(Tmct0Exentas tmct0Exentas) {
    this.tmct0Exentas = tmct0Exentas;
  }

  public BigDecimal getPorcentajeImpuesto() {
    if (codImpuesto != null && codImpuesto.getPorcentaje() != null) {
      return new BigDecimal(codImpuesto.getPorcentaje());
    }
    return null;
  }
}
