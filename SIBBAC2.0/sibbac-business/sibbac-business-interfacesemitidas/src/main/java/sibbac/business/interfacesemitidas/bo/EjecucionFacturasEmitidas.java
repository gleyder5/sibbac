package sibbac.business.interfacesemitidas.bo;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.io.File;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.interfacesemitidas.database.dao.Tmct0CodigosImpuestosDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificadasManualesDAO;
import sibbac.business.interfacesemitidas.database.model.Tmct0CodigoImpuesto;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;
import sibbac.business.interfacesemitidas.dto.CabeceraDTO;
import sibbac.business.interfacesemitidas.dto.ClaveRegimenDTO;
import sibbac.business.interfacesemitidas.dto.DatosContraparteDTO;
import sibbac.business.interfacesemitidas.dto.DatosFacturaDTO;
import sibbac.business.interfacesemitidas.dto.DatosFacturaEmitidaDTO;
import sibbac.business.interfacesemitidas.dto.DesglosePorFacturaDTO;
import sibbac.business.interfacesemitidas.dto.DeslgoseFacturasSujetasExentasDTO;
import sibbac.business.interfacesemitidas.dto.DeslgoseFacturasSujetasNoExentasDTO;
import sibbac.business.interfacesemitidas.dto.DivisionGeograficaDTO;
import sibbac.business.interfacesemitidas.dto.FacturaDTO;
import sibbac.business.interfacesemitidas.dto.FacturaRectificadaDTO;
import sibbac.business.interfacesemitidas.dto.FicheroFacturaEmitidaDTO;
import sibbac.business.interfacesemitidas.dto.IdentificacionFacturaDTO;
import sibbac.business.interfacesemitidas.dto.SecuenciaFacturaEmitidaDTO;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.ClaveRegimenEspecialEnum;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.InterfazEnum;
import sibbac.business.utilities.database.dao.Tmct0SibbacConstantesDao;
import sibbac.business.utilities.database.model.Tmct0SibbacConstantes;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FileTextPlainUtil;

/**
 * Clase de negocios para ejecucion del procesamiento de creación para ficheros
 * de facturas emitidas o expedidas.
 */
@Service
public class EjecucionFacturasEmitidas {

	private static final Logger LOG = LoggerFactory.getLogger(EjecucionFacturasEmitidas.class);

	@Autowired
	private Tmct0FacturaBo facturaBo;

	@Autowired
	private Tmct0CodigosImpuestosDAO tmct0CodigoImpuestoDAO;

	@Autowired
	private AliasBo aliasBo;
	
	@Autowired
	private Tmct0aliBo aliBo;

	@Autowired
	private Tmct0cliBo clienteBo;
	
	@Autowired
	private Tmct0SibbacConstantesDao tmct0SibbacConstantesDao;

	@Autowired
	private Tmct0ExpedidasFilesBo tmct0ExpedidasFilesBo;

	@Value("${interfacesemitidas.enviroment}")
	private String environment;
	
	@Value("${interfacesemitidas.codigoempresa}")
	private String codEmpresa;
	
	@Value("${interfacesemitidas.version}")
	private String version;

	@Value("${interfacesemitidas.folder.out}")
	private String folderInterfacesEmitidasOut;

	@Autowired
	private FileTextPlainUtil fileUtil;
	
	@Autowired
	private Tmct0paisesDao tmct0PaisesDao;
	
	@Autowired
	private Tmct0RectificadasManualesDAO tmct0RectificadasManualesDAO;
	
	@Autowired
	private Tmct0FacturasManualesDAO tmct0FacturasManualesDAO;

	/**
	 * Ejecucion del procesamiento de respuesta para ficheros de SII y GI.
	 * 
	 * @throws GlobalException
	 */
	public void ejecutarEmisionFicheros() throws SIBBACBusinessException {
		boolean ficheroGenerado;
		
		LOG.info("[ejecucionFacturasEmitidas] Carpeta Salida interfaces emitidas: {}", folderInterfacesEmitidasOut);
		// Obtenemos las facturas a emitir
		File out = new File(folderInterfacesEmitidasOut);
		if(!out.exists()) {
			if(!out.mkdirs()) {
				throw new SIBBACBusinessException(MessageFormat.format("No se puede crear la ruta %s", 
						folderInterfacesEmitidasOut));
			}
		}
		else if(out.isFile()) {
			throw new SIBBACBusinessException(MessageFormat.format("%s existe como fichero, cuando debería ser una carpeta", 
					folderInterfacesEmitidasOut));
		}
		List<FacturaDTO> facturas = facturaBo.getListadoFacturasEmitir();
		FicheroFacturaEmitidaDTO fichero = new FicheroFacturaEmitidaDTO(environment, version, 1,
		    folderInterfacesEmitidasOut);
		if (facturas != null && !facturas.isEmpty()) {

			// Populamos el bloque 00
			CabeceraDTO cabecera = this.popularCabecera();

			// Creamos el fichero con nro de secuencia 1

			// Asignamos la cabecera al fichero
			fichero.setBloque00(cabecera);

			// Secuencial de factura
			Integer secuencialRegistro = 0;
			for (FacturaDTO factura : facturas) {

				// Incrementamos secuencial de factura
				secuencialRegistro++;

				// Populamos bloque 01
				DatosFacturaEmitidaDTO datosFactura = this.popularDatosFacturaEmitida(factura, secuencialRegistro);

				// Populamos bloque 02
				ClaveRegimenDTO claveRegimen = this.popularClaveRegimen(factura, secuencialRegistro);

				// Populamos bloque 04
				FacturaRectificadaDTO facturaRectificada = null;
				if (factura.isRectificadaManual()) {
					facturaRectificada = this.popularFacturaRectificada(factura, secuencialRegistro);
				}

				// Populamos bloque 06
				DeslgoseFacturasSujetasExentasDTO desglosePorFactura = null;
				if ((factura.isRegular() || !factura.getCausaExencion().isEmpty()) && factura.getCausaExencion().equals("E")) {
					desglosePorFactura = this.popularDesglosePorFacturaSujetasExentas(factura, secuencialRegistro);
				}

				// Populamos bloque 07
				DeslgoseFacturasSujetasNoExentasDTO desglosePorFacturaNoExenta = null;
				if ((factura.isManual() || factura.isRectificadaManual()) && !factura.getCausaExencion().isEmpty()
				    && factura.getCausaExencion().equals("NE")) {
					desglosePorFacturaNoExenta = this.popularDesglosePorFacturaSujetasNoExentas(factura, secuencialRegistro);
					// Validaciones
					if (!claveRegimen.getClaveRegimenEspecialOTrascendencia().equals(ClaveRegimenEspecialEnum.CLAVE_06)) {
						if (desglosePorFacturaNoExenta.getCoeficienteImputacion() == null)
							throw new SIBBACBusinessException(
							    "No se ha informado el coeficiente de imputación de costes de desglose por factura sujetas no exentas");
						if (desglosePorFacturaNoExenta.getBaseImponibleCoste() == null)
							throw new SIBBACBusinessException(
							    "No se ha informado la base imponible costes de desglose por factura sujetas no exentas");
					}
					if (desglosePorFacturaNoExenta.getBaseImpositivo() == null)
						throw new SIBBACBusinessException(
						    "No se ha informado la base impositivo de desglose por factura sujetas no exentas");
					if (factura.getIdCodImpuesto() == null)
						throw new SIBBACBusinessException(
						    "No se ha informado el impuesto de desglose por factura sujetas no exentas");
				}

				// Populamos bloque 09
				DivisionGeograficaDTO divisionGeografica = this.popularDivisionGeografica(factura, secuencialRegistro);

				// Asignamos los bloques al fichero
				fichero.getSecuencias().add(new SecuenciaFacturaEmitidaDTO(datosFactura, claveRegimen, facturaRectificada,
				    desglosePorFactura, desglosePorFacturaNoExenta, divisionGeografica));

			}

			// Nos fijamos si ya existe el fichero para ese secuencial y
			// creamos el siguiente
			while (fileUtil.exists(fichero.getRuta())) {
				fichero.setSecuencial(fichero.getSecuencial() + 1);
			}
			// Exportamos el contenido del fichero
			LOG.info(fichero.getLogInfo());
			ficheroGenerado = fileUtil.generateTextPlain(fichero.getRuta(), fichero.getContenido());
			if(ficheroGenerado) {
				// Creamos el histórico
				tmct0ExpedidasFilesBo.createHistorico(fichero.getNombre(), InterfazEnum.EXPEDIDA);
				// Marcamos las facturas como enviadas
				facturaBo.updateFacturasEnviadas(facturas);
			}
		} 
		else {
			SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
			String nombre = environment + ".E" + codEmpresa + ".F" + format.format(new Date()) + "."
			    + InterfacesEmitidasEnums.InterfazEnum.EXPEDIDA.getId() + "." + version + StringHelper.padCerosLeft(1, 3);
			String ruta = folderInterfacesEmitidasOut + nombre;
			fileUtil.generateTextPlain(ruta, "");
		}

	}

	private CabeceraDTO popularCabecera() {
		CabeceraDTO cabecera = new CabeceraDTO();
		cabecera.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.CABECERA.getId());
		cabecera.setInterfaz(InterfacesEmitidasEnums.InterfazEnum.EXPEDIDA.getId());
		cabecera.setFechaGeneracionFichero(new Date());
		cabecera.setEmpresaDeclarante(InterfacesEmitidasEnums.EmpresaDeclaranteEnum.CARGABAL.getId());
		return cabecera;
	}

	private DatosFacturaEmitidaDTO popularDatosFacturaEmitida(FacturaDTO factura, Integer secuencialRegistro) throws SIBBACBusinessException {
		DatosFacturaEmitidaDTO datosFactura = new DatosFacturaEmitidaDTO();
		datosFactura.setSecuencialRegistro(secuencialRegistro);
		datosFactura.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.BLOQUE_01.getId());
		if (factura.isAceptadaConErrores()) {
			datosFactura.setTipoComunicacion(InterfacesEmitidasEnums.TipoComunicacionEnum.A1.getId());
		} else {
			datosFactura.setTipoComunicacion(InterfacesEmitidasEnums.TipoComunicacionEnum.A0.getId());
		}
		datosFactura.setClaveUnica(null);
		datosFactura.setDatosFactura(this.popularDatosFactura(factura));
		datosFactura.setIdentificacion(this.popularDatosIdentificacion(factura));
		datosFactura.setClaveUnicaIdentificacionOperacionOrigenRetrocedida(null);
		datosFactura.setFechaContabilizacion(null);
		datosFactura.setIndicadorExcepcion(InterfacesEmitidasEnums.IndicadorExcepcionEnum.EXCEPCIONADA.getId());
		datosFactura.setOrigenOperacion("999");
		datosFactura.setIdentificacionDatosContables(
				InterfacesEmitidasEnums.IndicadorDatosContableEnum.NO_SE_ENVIA_OPERACION.getId());
		datosFactura.setOrigenContable(null);
		datosFactura.setOtrosDatosAdicionales(null);
		return datosFactura;
	}

	private DatosFacturaDTO popularDatosFactura(FacturaDTO factura) throws SIBBACBusinessException {
		String comentarios = "";
		DatosFacturaDTO datosFactura = new DatosFacturaDTO();
		datosFactura.setTipoFactura(factura.getTipoFactura());
		if (factura.isRectificadaManual()) {
			if (factura.isRegular() || factura.isRegularBaja()) {
				datosFactura.setTipoFacturaRectificativa("S");
			} else {
				//Si es GRUPO IVA es I sino es S
				if (factura.getClaveRegimen()==6) {
					datosFactura.setTipoFacturaRectificativa("I");
				} else {
					datosFactura.setTipoFacturaRectificativa("S");
				}
			}
		} 
		datosFactura.setMonedaImportesDatosFactura(InterfacesEmitidasEnums.MonedaEnum.EUR.getId());
		if ((factura.isRectificadaManual() && factura.isRegular()) || factura.isRegularBaja()) {
			datosFactura.setBaseSustituidaRectificada(factura.getImpBaseImponible());
		} else {
			datosFactura.setBaseSustituidaRectificada(null);
		}
		if ((factura.isRectificadaManual() && !factura.isRegular()) || factura.isRegularBaja()) {
			datosFactura.setCuotaSustituidaRectificada(factura.getImpImpuesto());
		} else {
			datosFactura.setCuotaSustituidaRectificada(null);
		}
		datosFactura.setFechaOperacion(null);
		datosFactura.setImporteTotalFactura(factura.getImporteTotal());
		if (null!=factura.getComentarios() && !"".equals(factura.getComentarios())) {
			comentarios = factura.getComentarios().replace("\n", " ");
			int longitud = comentarios.length()<500?factura.getComentarios().length():500;
			datosFactura.setDescripcionDeOperacion(comentarios.substring(0, longitud));
		} else {
			Tmct0SibbacConstantes constanteComentarioDefecto = null;
			List<Tmct0SibbacConstantes> listaConstantes = this.tmct0SibbacConstantesDao.findByClave(
				Constantes.CLAVE_COMENTARIO_POR_DEFECTO);
			if (CollectionUtils.isNotEmpty(listaConstantes)) {
				constanteComentarioDefecto = listaConstantes.get(0);
				datosFactura.setDescripcionDeOperacion(constanteComentarioDefecto.getValor());
			}
		}
		datosFactura.setImportePercibido(null);
		datosFactura.setFacturaEmitidaPorTerceros(null);

		datosFactura.setDesglosePorFactura(this.popularDesglosePorFactura(factura));
		//Seteamos el nombre del alias sacándolo de la TMCT0ALI solo para facturas manuales
		if (!factura.isRegular() || factura.isRegularBaja()) {
		factura.setNombreAlias(getNombreAlias(factura));
		}
		datosFactura.setDatosContraparte(this.popularDatosContraparte(factura));
		return datosFactura;
	}

	private IdentificacionFacturaDTO popularDatosIdentificacion(FacturaDTO factura) {
		IdentificacionFacturaDTO identificacion = new IdentificacionFacturaDTO();
		identificacion.setNumeroEmisorFactura(factura.getNumeroFactura());
		identificacion.setNumeroFacturaEmisorResumenFin(null);
		identificacion.setFechaExpedicionFacturaEmision(factura.getFecha());
		return identificacion;
	}

	private DesglosePorFacturaDTO popularDesglosePorFactura(FacturaDTO factura) {
		DesglosePorFacturaDTO desglose = new DesglosePorFacturaDTO();
		if (!factura.getExenta().equals("NE")) {
		desglose.setSujetasExentasCausa(InterfacesEmitidasEnums.CausaDesgloseFacturaEnum.E1.getId());
		}
		if (factura.isRectificadaManual() && !factura.isRegular() && factura.getExenta().equals("NE")) {
			desglose.setSujetasNoExentasTipoNoExenta(factura.getSujetaNoExenta());
		}
		if (factura.isManual()) {
			desglose.setSujetasNoExentasTipoNoExenta(factura.getSujetaNoExenta());
		}
		return desglose;
	}

	private DatosContraparteDTO popularDatosContraparte(FacturaDTO factura) throws SIBBACBusinessException {
		DatosContraparteDTO contraparte = new DatosContraparteDTO();
		if (factura.getIdAlias() != null) {
			Tmct0cli cliente = clienteBo.buscarClienteActivo(this.getCdBrocli(factura));
			if (cliente != null) {
				//Si TIPIDENTI es 06 (BIC u Otros) no se rellenan tipoIdentificacionContraparte ni identificacionContraparte
				if (InterfacesEmitidasEnums.TipoCodigoIdentificacion.fromValue(cliente.getTpidenti()).getEquivalencia().equals("06")) {
					contraparte.setIdentificacionContraparte(null);
					contraparte.setTipoIdentificacionContraparte(null);
					//Código de pais asociado. Si es BIC, el país está comprendido en su nomenclatura sería desde la posición 5:2
					//Si es otros se recoge del campo CDPAIS
					if (InterfacesEmitidasEnums.TipoCodigoIdentificacion.fromValue(cliente.getTpidenti()).getCodigo().equals('B')) {
						if (StringUtils.isNotBlank(cliente.getCif())) {
						contraparte.setIdOtroCodigoPais(cliente.getCif().trim().substring(4,6));
						} else {
							contraparte.setIdOtroCodigoPais(null);
						}
						contraparte.setIdOtroIdType("06");
					} else if (InterfacesEmitidasEnums.TipoCodigoIdentificacion.fromValue(cliente.getTpidenti()).getCodigo().equals('O')) {
						Tmct0paises paisBIC = null;
						contraparte.setIdOtroCodigoPais(cliente.getCdPais());
						contraparte.setIdOtroIdType("06");
						//Si es un NIF intracomunitario, se moverá 02 (esto es cuando la estructura del campo sea sus dos primeras posiciones el código alfabético de dos posiciones de país 
						//seguido del identificador nacional. Para saber si las dos primeras posiciones son de un país nos iríamos a la tabla de países a validar que existe.
						//Es decir, cuando tengamos un TPIDENTI = O, cogemos las 2 primeras posiciones y si se trata de un país que esté en la tabla de paises
						//y de las posiciones desde la 3 a las 11 sean números , entonces movemos 02. En caso contrario movemos 06.
						if (StringUtils.isNotBlank(cliente.getCif()) && cliente.getCif().length() == 11) {
							paisBIC = tmct0PaisesDao.findBycdisoalf2(cliente.getCif().trim().substring(0, 2));
							if (null!=paisBIC && StringUtils.isNumeric(cliente.getCif().trim().substring(2, 11))) {
								contraparte.setIdOtroIdType("02");
							}
						}
					}
					//Se añade en la posicion 1179 el equivalente a la posicion 1025 de las que no son tipo 06
					contraparte.setIdOtroId(cliente.getCif());
				} else {
					if (StringUtils.isNotBlank(cliente.getCif())) {
						contraparte.setIdentificacionContraparte(cliente.getCif());
					} else {
						contraparte.setIdentificacionContraparte(null);
					}
					contraparte.setTipoIdentificacionContraparte(InterfacesEmitidasEnums.TipoCodigoIdentificacion.fromValue(cliente.getTpidenti()).getEquivalencia());
				}
			}
			contraparte.setApellidosNombresRazonSocial(factura.getNombreAlias());
			contraparte.setTipoIdentificacionRepresentanteLegal(null);
			contraparte.setIdentificacionRepresentanteLegal(null);
		}
		return contraparte;
	}

	private ClaveRegimenDTO popularClaveRegimen(FacturaDTO factura, Integer secuencialRegistro) {
		ClaveRegimenDTO claveRegimen = new ClaveRegimenDTO();
		claveRegimen.setSecuencialRegistro(secuencialRegistro);
		claveRegimen.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.BLOQUE_02.getId());
		if (factura.getClaveRegimen() != null) {
			claveRegimen.setClaveRegimenEspecialOTrascendencia(factura.getClaveRegimen());
		} else {
			claveRegimen.setClaveRegimenEspecialOTrascendencia(InterfacesEmitidasEnums.ClaveRegimenEspecialEnum.CLAVE_01.getId());
		}
		return claveRegimen;
	}

	private FacturaRectificadaDTO popularFacturaRectificada(FacturaDTO factura, Integer secuencialRegistro) {
		FacturaRectificadaDTO fr = new FacturaRectificadaDTO();
		//Si es una factura de baja obtenemos los datos de la factura original y seteamos su fecha y número de factura
		if (factura.isRectificadaBaja()) {
			Tmct0RectificativaManual rectificativaManual = tmct0RectificadasManualesDAO.findRectificativaManual(factura.getNumeroFacturaOriginal());
			if (null!=rectificativaManual) {
				factura.setFecha(rectificativaManual.getFhFechaCre());
				factura.setIdsFacturasARectificar(""+rectificativaManual.getNbDocNumero());
			}
		} else if (factura.isManualBaja()) {
			Tmct0FacturasManuales facturaManual = tmct0FacturasManualesDAO.findFacturaManual(factura.getNumeroFacturaOriginal());
			if (null!=facturaManual) {
				factura.setFecha(facturaManual.getFhFechaCre());
				factura.setIdsFacturasARectificar(""+facturaManual.getNbDocNumero());
			}
		}
		fr.setSecuencialRegistro(secuencialRegistro);
		fr.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.BLOQUE_04.getId());
		fr.setClaveRegimenEspecial(InterfacesEmitidasEnums.ClaveRegimenEspecialEnum.CLAVE_01.getId());
		fr.setFechaExpedicion(factura.getFecha());
		if ((factura.isRectificadaManual() && !factura.isRegular()) || (factura.isRegularBaja() && factura.isRectificadaManual())) {
			fr.setNumeroFacturaRectificada(factura.getIdsFacturasARectificar());
		} else {
			fr.setNumeroFacturaRectificada(factura.getIdFacturaARectificar());
		}
		
		return fr;
	}

	private DeslgoseFacturasSujetasExentasDTO popularDesglosePorFacturaSujetasExentas(FacturaDTO factura,
			Integer secuencialRegistro) {
		DeslgoseFacturasSujetasExentasDTO desglosePorFactura = new DeslgoseFacturasSujetasExentasDTO();
		desglosePorFactura.setSecuencialRegistro(secuencialRegistro);
		desglosePorFactura.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.BLOQUE_06.getId());
		desglosePorFactura.setIndicadorEntregaBien(InterfacesEmitidasEnums.IndicadorEntregaEnum.BIEN.getId());
		desglosePorFactura.setBaseImponibleExenta(factura.getBaseImponible());
		desglosePorFactura.setCodigoImpuesto(InterfacesEmitidasEnums.CodigoImpuestoEnum.CODIGO_01.getId());
		desglosePorFactura.setCodigoSubtipoImpuesto(InterfacesEmitidasEnums.SubtipoImpuestoEnum.SUBTIPO_01.getId());
		return desglosePorFactura;
	}

	private DeslgoseFacturasSujetasNoExentasDTO popularDesglosePorFacturaSujetasNoExentas(FacturaDTO factura,
			Integer secuencialRegistro) throws SIBBACBusinessException {
		DeslgoseFacturasSujetasNoExentasDTO desglosePorFactura = new DeslgoseFacturasSujetasNoExentasDTO();
		desglosePorFactura.setSecuencialRegistro(secuencialRegistro);
		desglosePorFactura.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.BLOQUE_07.getId());
		desglosePorFactura.setIndicadorEntregaBien(InterfacesEmitidasEnums.IndicadorEntregaEnum.BIEN.getId());
		desglosePorFactura.setImporteFactura(factura.getImporteFactura());
		desglosePorFactura.setCoeficienteImputacion(factura.getCoefImpCostes());
		desglosePorFactura.setBaseImponibleCoste(factura.getBaseImponCostes());
		desglosePorFactura.setBaseImpositivo(factura.getImpBaseImponible());
		if (factura.getIdCodImpuesto() != null) {
			Tmct0CodigoImpuesto impuesto = tmct0CodigoImpuestoDAO.getById(factura.getIdCodImpuesto());
			if (impuesto.getPorcentaje() != null) {
				desglosePorFactura.setTipoImpositivo(new BigDecimal(impuesto.getPorcentaje()));
			}
			desglosePorFactura.setCodigoImpuesto(String.valueOf(impuesto.getIdImpuesto()));
			desglosePorFactura.setCodigoSubtipoImpuesto(String.valueOf(impuesto.getSubImpuesto()));
		} else {
			desglosePorFactura.setCodigoImpuesto("09");
			desglosePorFactura.setCodigoSubtipoImpuesto("01");
		}
		desglosePorFactura.setCuotaImpuesto(factura.getImpImpuesto());
		return desglosePorFactura;
	}

	private DivisionGeograficaDTO popularDivisionGeografica(FacturaDTO factura, Integer secuencialRegistro)
			throws SIBBACBusinessException {
		Tmct0cli cliente = clienteBo.buscarClienteActivo(this.getCdBrocli(factura));
		DivisionGeograficaDTO divisionGeografica = new DivisionGeograficaDTO();
		divisionGeografica.setSecuencialRegistro(secuencialRegistro);
		divisionGeografica.setTipoRegistro(InterfacesEmitidasEnums.TipoRegistroEnum.BLOQUE_09.getId());
		if (cliente != null && cliente.getAddress() != null && cliente.getAddress().getCdPostal() != null) {
			String codPostal = cliente.getAddress().getCdPostal().substring(0, 2);
			if (!codPostal.equals(InterfacesEmitidasEnums.CodigoPostalEnum.OTROS.getId())) {
				divisionGeografica.setDomicilioContraparte(codPostal);
			}
		}
		divisionGeografica.setDomicilioOficinaOperacion(InterfacesEmitidasEnums.CodigoPostalEnum.MADRID.getId());
		return divisionGeografica;
	}

	private String getCdBrocli(FacturaDTO factura) throws SIBBACBusinessException {
		if (factura.isRegular() && !factura.isRegularBaja()) {
			Alias alias = aliasBo.findById(factura.getIdAlias());
			if (alias == null)
				throw new SIBBACBusinessException("No se pudo recuperar el alias para el id: " + factura.getIdAlias());
			return alias.getCdbrocli();
		} else {
			List<Tmct0ali> alias = aliBo.findByCdAlias(String.valueOf(factura.getIdAlias()));
			if (alias == null || alias.isEmpty())
				throw new SIBBACBusinessException("No se pudo recuperar el alias para el id: " + factura.getIdAlias());
			return alias.get(0).getCdbrocli();
		}
		
	}
	
	private String getNombreAlias(FacturaDTO factura) throws SIBBACBusinessException {
		List<Tmct0ali> alias = aliBo.findByCdAlias(String.valueOf(factura.getIdAlias()));
		if (alias == null || alias.isEmpty())
			throw new SIBBACBusinessException("No se pudo recuperar el alias para el id: " + factura.getIdAlias());
		return alias.get(0).getDescrali();
		
	}

}
