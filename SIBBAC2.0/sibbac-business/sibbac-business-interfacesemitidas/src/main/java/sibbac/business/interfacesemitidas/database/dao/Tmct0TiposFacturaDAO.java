package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0TiposFactura;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0TiposFacturaDAO extends JpaRepository<Tmct0TiposFactura,Integer>{
	
	@Query("SELECT tipFact FROM Tmct0TiposFactura tipFact ORDER BY tipFact.clave ASC")
	public List<Tmct0TiposFactura> findAllOrderByClave();

}
