package sibbac.business.interfacesemitidas.dto;

import java.math.BigDecimal;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 07
 * 
 * @author adrian.prause
 *
 */
public class DeslgoseFacturasSujetasNoExentasDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private String indicadorEntregaBien;
	private BigDecimal importeFactura;
	private BigDecimal coeficienteImputacion;
	private BigDecimal baseImponibleCoste;
	private BigDecimal tipoImpositivo;
	private BigDecimal baseImpositivo;
	private BigDecimal cuotaImpuesto;
	private String codigoImpuesto;
	private String codigoSubtipoImpuesto;

	@Override
	public String toString() {
		String desgloseFacturasSujetasNoExentas ="";
		try {
			desgloseFacturasSujetasNoExentas = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padRight(indicadorEntregaBien, 1);
					if (null!=importeFactura && importeFactura.signum() < 0) {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.formatBigDecimalWithNSign(importeFactura, 16, 2);
					} else {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + " " + StringHelper.formatBigDecimalWithNSign(importeFactura, 15, 2);
					}
					
					if (null!=coeficienteImputacion && coeficienteImputacion.signum() < 0) {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.formatBigDecimalWithNSign(coeficienteImputacion, 4, 6);
					} else {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + " " + StringHelper.formatBigDecimalWithNSign(coeficienteImputacion, 3, 6);
					}
					
					if (null!=baseImponibleCoste && baseImponibleCoste.signum() < 0) {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.formatBigDecimalWithNSign(baseImponibleCoste, 16, 2);
					} else {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + " " + StringHelper.formatBigDecimalWithNSign(baseImponibleCoste, 15, 2);
					}
					
					if (null!=tipoImpositivo && tipoImpositivo.signum() < 0) {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.formatBigDecimalWithNSign(tipoImpositivo, 4, 6);
					} else {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + " " + StringHelper.formatBigDecimalWithNSign(tipoImpositivo, 3, 6);
					}
					
					if (null!=baseImpositivo && baseImpositivo.signum() < 0) {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.formatBigDecimalWithNSign(baseImpositivo, 16, 2);
					} else {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + " " + StringHelper.formatBigDecimalWithNSign(baseImpositivo, 15, 2);
					}
					
					if (null!=cuotaImpuesto && cuotaImpuesto.signum() < 0) {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.formatBigDecimalWithNSign(cuotaImpuesto, 16, 2);
					} else {
						desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + " " + StringHelper.formatBigDecimalWithNSign(cuotaImpuesto, 15, 2);
					}
					
					desgloseFacturasSujetasNoExentas = desgloseFacturasSujetasNoExentas + StringHelper.padCerosLeft(codigoImpuesto, 2) + StringHelper.padCerosLeft(codigoSubtipoImpuesto, 2);
					
			return StringHelper.padRight(desgloseFacturasSujetasNoExentas, 2449);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public String getIndicadorEntregaBien() {
		return indicadorEntregaBien;
	}

	public void setIndicadorEntregaBien(String indicadorEntregaBien) {
		this.indicadorEntregaBien = indicadorEntregaBien;
	}

	public BigDecimal getImporteFactura() {
		return importeFactura;
	}

	public void setImporteFactura(BigDecimal importeFactura) {
		this.importeFactura = importeFactura;
	}

	public BigDecimal getCoeficienteImputacion() {
		return coeficienteImputacion;
	}

	public void setCoeficienteImputacion(BigDecimal coeficienteImputacion) {
		this.coeficienteImputacion = coeficienteImputacion;
	}

	public BigDecimal getBaseImponibleCoste() {
		return baseImponibleCoste;
	}

	public void setBaseImponibleCoste(BigDecimal baseImponibleCoste) {
		this.baseImponibleCoste = baseImponibleCoste;
	}

	public BigDecimal getTipoImpositivo() {
		return tipoImpositivo;
	}

	public void setTipoImpositivo(BigDecimal tipoImpositivo) {
		this.tipoImpositivo = tipoImpositivo;
	}

	public BigDecimal getBaseImpositivo() {
		return baseImpositivo;
	}

	public void setBaseImpositivo(BigDecimal baseImpositivo) {
		this.baseImpositivo = baseImpositivo;
	}

	public BigDecimal getCuotaImpuesto() {
		return cuotaImpuesto;
	}

	public void setCuotaImpuesto(BigDecimal cuotaImpuesto) {
		this.cuotaImpuesto = cuotaImpuesto;
	}

	public String getCodigoImpuesto() {
		return codigoImpuesto;
	}

	public void setCodigoImpuesto(String codigoImpuesto) {
		this.codigoImpuesto = codigoImpuesto;
	}

	public String getCodigoSubtipoImpuesto() {
		return codigoSubtipoImpuesto;
	}

	public void setCodigoSubtipoImpuesto(String codigoSubtipoImpuesto) {
		this.codigoSubtipoImpuesto = codigoSubtipoImpuesto;
	}

}
