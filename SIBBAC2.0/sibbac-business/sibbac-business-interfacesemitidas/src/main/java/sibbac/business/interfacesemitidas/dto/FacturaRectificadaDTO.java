package sibbac.business.interfacesemitidas.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 04
 * 
 * @author adrian.prause
 *
 */
public class FacturaRectificadaDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private Integer claveRegimenEspecial;
	private String numeroFacturaRectificada;
	private Date fechaExpedicion;

	@Override
	public String toString() {
		String facturaRectificada = "";
		try {
			SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO_GUIONES);
			String sfechaExpedicion = "";
			if (fechaExpedicion != null) {
				sfechaExpedicion = format.format(fechaExpedicion);
			}
			facturaRectificada = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
//					+ StringHelper.padCerosLeft(claveRegimenEspecial, 2)
					+ StringHelper.padRight("",100)
					+ StringHelper.padRight(numeroFacturaRectificada, 60) + sfechaExpedicion;
			return StringHelper.padRight(facturaRectificada, 2449);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public Integer getClaveRegimenEspecial() {
		return claveRegimenEspecial;
	}

	public void setClaveRegimenEspecial(Integer claveRegimenEspecial) {
		this.claveRegimenEspecial = claveRegimenEspecial;
	}

	public String getNumeroFacturaRectificada() {
		return numeroFacturaRectificada;
	}

	public void setNumeroFacturaRectificada(String numeroFacturaRectificada) {
		this.numeroFacturaRectificada = numeroFacturaRectificada;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

}
