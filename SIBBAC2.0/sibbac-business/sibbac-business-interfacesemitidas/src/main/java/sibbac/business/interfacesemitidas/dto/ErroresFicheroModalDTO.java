package sibbac.business.interfacesemitidas.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 *	DTO para modal de Errores de fichero.
 */
public class ErroresFicheroModalDTO {

	private String numeroFacturaEmisor;
	private Date fechaExpedicionFactura;
	private String codigoRetorno;
	private String tipoError;
	private String claseError;
	private String descripcion;
	
	public String getNumeroFacturaEmisor() {
		return this.numeroFacturaEmisor;
	}
	
	public void setNumeroFacturaEmisor(String numeroFacturaEmisor) {
		this.numeroFacturaEmisor = numeroFacturaEmisor;
	}
	
	public Date getFechaExpedicionFactura() {
		return this.fechaExpedicionFactura;
	}
	
	public void setFechaExpedicionFactura(Date fechaExpedicionFactura) {
		this.fechaExpedicionFactura = fechaExpedicionFactura;
	}
	
	public String getCodigoRetorno() {
		return this.codigoRetorno;
	}
	
	public void setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}
	
	public String getTipoError() {
		return this.tipoError;
	}
	
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	
	public String getClaseError() {
		return this.claseError;
	}
	
	public void setClaseError(String claseError) {
		this.claseError = claseError;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 *	Convierte una lista de DTO's del fichero a una con los datos para un modal front.
	 *	@param listaEstrRespDTO
	 *	@return List<ErroresFicheroModalDTO>  
	 */
	public static List<ErroresFicheroModalDTO> convertFromDTO(List<EstructuraRespuestaDTO> listaEstrRespDTO) {
		List<ErroresFicheroModalDTO> listaErroresFicheroModal = new ArrayList<ErroresFicheroModalDTO>();
		if (CollectionUtils.isNotEmpty(listaEstrRespDTO)) {
			for (EstructuraRespuestaDTO estructRespDTO : listaEstrRespDTO) {
				ErroresFicheroModalDTO eFModalDTO = new ErroresFicheroModalDTO();
				if (estructRespDTO.getErroresDTO() != null) {
					eFModalDTO.setClaseError(estructRespDTO.getErroresDTO().getClaseError());
					eFModalDTO.setDescripcion(estructRespDTO.getErroresDTO().getDescripcionError());
					eFModalDTO.setTipoError(estructRespDTO.getErroresDTO().getTipoError());
				}
				if (estructRespDTO.getIdentificacionFacturaRespuestaDTO() != null) {
					eFModalDTO.setCodigoRetorno(estructRespDTO.getIdentificacionFacturaRespuestaDTO().getCodigoRetorno());
					eFModalDTO.setFechaExpedicionFactura(
						estructRespDTO.getIdentificacionFacturaRespuestaDTO().getFechaExpedicionFacturaEmision());
					eFModalDTO.setNumeroFacturaEmisor(
						String.valueOf(estructRespDTO.getIdentificacionFacturaRespuestaDTO().getNumeroEmisorFactura()));
				}
				listaErroresFicheroModal.add(eFModalDTO);
			}
		}
		return listaErroresFicheroModal;
	}
	
}
