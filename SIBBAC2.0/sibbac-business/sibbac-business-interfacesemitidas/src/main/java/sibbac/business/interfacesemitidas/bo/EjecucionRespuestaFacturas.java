package sibbac.business.interfacesemitidas.bo;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.FileHelper;
import sibbac.common.SIBBACBusinessException;

/**
 * 	Clase de negocios para ejecucion del procesamiento 
 * 	de respuesta para ficheros de SII y GI.
 */
@Service
public class EjecucionRespuestaFacturas {

	 private static final Logger LOG = LoggerFactory.getLogger(EjecucionRespuestaFacturas.class);
	
	 @Value("${interfacesemitidas.folder.in}")
	 private String folderInterfacesEmitidasIn;
	 
	 @Value("${interfacesemitidas.respuesta}")
	 private String respuesta;
	 
	 @Autowired
	 private Tmct0ExpedidasFilesBo tmct0ExpedidasFilesBo;
	 
	 /**
   	  * Ejecucion del procesamiento de respuesta para ficheros de SII y GI.
   	  * @throws SIBBACBusinessException
   	  */
	 public void ejecutarRespuestaFicheros() throws SIBBACBusinessException {
		 final List<File> archivosRespuesta;
		 
		LOG.info("[ejecutarRespuestaFicheros] Inicio. Carpeta Entrada interfaces emitidas: {}", folderInterfacesEmitidasIn);
		try {
			// Se buscan ficheros de texto plano.
			archivosRespuesta = FileHelper.getListaArchivosEnDirectorioByExtension(folderInterfacesEmitidasIn, "");
	    // Se almacena en la DB cada uno de los Bloques de datos procesados de los ficheros de respuesta.
			tmct0ExpedidasFilesBo.saveAndProcessRespuestaExpedidasFromFiles(archivosRespuesta, respuesta);
		} 
		catch (SIBBACBusinessException e) {
			LOG.error("[ejecutarRespuestaFicheros] Error de negocio al ejecutar la respuesta de ficheros de interfaz", e);
			throw e;
		}
		LOG.debug("[ejecutarRespuestaFicheros] Fin");
	 }
	 
}
