package sibbac.business.interfacesemitidas.bo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.interfacesemitidas.database.dao.Tmct0BajasManualesDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificadasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativaBajasManualesDao;
import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;
import sibbac.business.interfacesemitidas.dto.FacturaDTO;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.FacturaRectificativaDao;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaRectificativa;

/**
 * Business object de Tmct0FacturaBo.
 */
@Service
public class Tmct0FacturaBo {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0FacturaBo.class);

	@Autowired
	private FacturaDao facturaDao;

	@Autowired
	private Tmct0FacturasManualesDAO facturaManualDao;

	@Autowired
	private Tmct0RectificadasManualesDAO facturaRectificadasManualDao;

	@Autowired
	private Tmct0RectificativaBajasManualesDao facturaRectificadasBajaManualDao;
	
	@Autowired
	private Tmct0BajasManualesDao facturaBajaManualDao;
	
	@Autowired
	FacturaRectificativaDao facturaRectificativaDao;

	/**
	 * @return
	 */
	@Transactional
	public List<FacturaDTO> getListadoFacturasEmitir() {

		List<FacturaDTO> facturas = new ArrayList<FacturaDTO>();
		facturas.addAll(this.getFacturasNoEnviadas());
		facturas.addAll(this.getFacturasRectificativasNoEnviadas());
		facturas.addAll(this.getFacturasManualesNoEnviadas());
		facturas.addAll(this.getListadoFacturasManualesBajasNoEnviadas());
		facturas.addAll(this.getListadoFacturasManualesRectificativasBajasNoEnviadas());
		return facturas;

	}

	/**
	 * @return
	 */
	@Transactional
	public void updateFacturasEnviadas(List<FacturaDTO> dtos) {
		int enviados;
		
		for (FacturaDTO dto : dtos) {
			enviados = 0;
			if (dto.isRegular()) {
				if (dto.isBaja() || dto.isRectificadaManual()) {
					enviados = facturaRectificativaDao.marcaEnviado(dto.getIdFactura());
				}
				else{
					enviados = facturaDao.marcaEnviado(dto.getIdFactura());
				}
			} else if (dto.isManual()) {
				if (dto.isBaja()) {
					enviados = facturaBajaManualDao.marcaEnviado(dto.getId());
				} else {
					enviados = facturaManualDao.marcaEnviado(dto.getId());
				}
			} else if (dto.isRectificadaManual()) {
				if (dto.isBaja()) {
					enviados = facturaRectificadasBajaManualDao.marcaEnviado(dto.getId());
				} else {
					enviados = facturaRectificadasManualDao.marcaEnviado(dto.getId());
				}
			}
			if(enviados != 1) {
				LOG.error("[updateFacturasEnviadas] Se han actualizado {} entidades, se esperaba 1 para idFactura {}, o Id {}",
						enviados, dto.getIdFactura(), dto.getId());
			}
		}
	}

	private List<FacturaDTO> getFacturasNoEnviadas() {
		List<Integer> estados = new ArrayList<Integer>();
		estados.add(EstadosEnumerados.FACTURA.RECTIFICADA.getId());
		List<Factura> facturas = facturaDao.findNoEnviadasByIdEstadoDistinctBetweenFechas(estados, this.getFechaDesde(), this.getFechaHasta());
		List<FacturaDTO> dtos = new ArrayList<FacturaDTO>();
		if (facturas != null && !facturas.isEmpty()) {
			for (Factura factura : facturas) {
				FacturaDTO dto = new FacturaDTO(factura);
				dtos.add(dto);
			}
		}
		return dtos;
	}

	private List<FacturaDTO> getFacturasRectificativasNoEnviadas() {
		List<Integer> estados = new ArrayList<Integer>();
		estados.add(EstadosEnumerados.FACTURA.RECTIFICADA.getId());
		List<FacturaRectificativa> facturas = facturaRectificativaDao.findRectificativasNoEnviadasByIdEstadoDistinctBetweenFechas(estados, this.getFechaDesde(), this.getFechaHasta());
		List<FacturaDTO> dtos = new ArrayList<FacturaDTO>();
		if (facturas != null && !facturas.isEmpty()) {
			for (FacturaRectificativa factura : facturas) {
				FacturaDTO dto = new FacturaDTO(factura);
				dtos.add(dto);
			}
		}
		return dtos;
	}
	
	private List<FacturaDTO> getFacturasManualesNoEnviadas() {
		List<Integer> estados = new ArrayList<Integer>();
		estados.add(EstadosEnumerados.FACTURA.RECTIFICADA.getId());
		estados.add(EstadosEnumerados.FACTURA.FACTURA_GUARDADA.getId());
		List<Tmct0FacturasManuales> facturas = facturaManualDao.findNoEnviadasByIdEstadoDistinctBetweenFechas(estados, this.getFechaDesde(), this.getFechaHasta());
		List<FacturaDTO> dtos = new ArrayList<FacturaDTO>();
		if (facturas != null && !facturas.isEmpty()) {
			for (Tmct0FacturasManuales factura : facturas) {
				FacturaDTO dto = new FacturaDTO(factura);
				dtos.add(dto);
			}
		}
		return dtos;
	}

	private Date getFechaHasta() {
		Calendar hasta = Calendar.getInstance();
		hasta.add(Calendar.DATE, 1);
		hasta.set(Calendar.HOUR_OF_DAY, 0);
		return hasta.getTime();
	}

	private Date getFechaDesde() {

		Calendar desde = Calendar.getInstance();
		desde.add(Calendar.DATE, -3);
		desde.set(Calendar.HOUR_OF_DAY, 0);
		return desde.getTime();

	}

	/**
	 * @return
	 */
	@Transactional
	private List<FacturaDTO> getListadoFacturasManualesBajasNoEnviadas() {
		List<FacturaDTO> dtos = new ArrayList<FacturaDTO>();
		final List<Tmct0BajasManuales> facturas = facturaBajaManualDao
				.findNoEnviadas();
		if (facturas != null && !facturas.isEmpty()) {
			for (Tmct0BajasManuales factura : facturas) {
				FacturaDTO dto = new FacturaDTO(factura);
				dtos.add(dto);
			}
		}
		return dtos;
	}
	
	/**
	 * @return
	 */
	@Transactional
	private List<FacturaDTO> getListadoFacturasManualesRectificativasBajasNoEnviadas() {
		List<FacturaDTO> dtos = new ArrayList<FacturaDTO>();
		final List<Tmct0RectificativaBajasManuales> facturas = facturaRectificadasBajaManualDao.findNoEnviadas();
		if (facturas != null && !facturas.isEmpty()) {
			for (Tmct0RectificativaBajasManuales factura : facturas) {
				FacturaDTO dto = new FacturaDTO(factura);
				dtos.add(dto);
			}
		}
		return dtos;
	}
}
