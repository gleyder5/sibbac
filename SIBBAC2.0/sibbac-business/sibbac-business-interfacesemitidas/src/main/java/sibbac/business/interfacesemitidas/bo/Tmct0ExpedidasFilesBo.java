package sibbac.business.interfacesemitidas.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.interfacesemitidas.database.dao.Tmct0ExpedidasFilesDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0ExpedidasFilesDaoImp;
import sibbac.business.interfacesemitidas.database.dao.Tmct0InterfacesEmitidasConstDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0UsuariosExpedidasDao;
import sibbac.business.interfacesemitidas.database.model.Tmct0ExpedidasFiles;
import sibbac.business.interfacesemitidas.database.model.Tmct0InterfacesEmitidasConst;
import sibbac.business.interfacesemitidas.dto.ErroresFicheroModalDTO;
import sibbac.business.interfacesemitidas.dto.EstructuraRespuestaDTO;
import sibbac.business.interfacesemitidas.dto.IdentificacionFacturaRespuestaDTO;
import sibbac.business.interfacesemitidas.dto.assembler.RespFacturasEmitidasAssembler;
import sibbac.business.interfacesemitidas.dto.pantallas.BusquedaRespuestaErrGCDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.RespuestaErroresExpedidasFilterDTO;
import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums.InterfazEnum;
import sibbac.business.interfacesemitidas.helper.InterfacesEmitidasHelper;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.FileHelper;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;

import com.google.common.io.Files;

/**
 * Business object de Tmct0ExpedidasFilesBo.
 */
@Service
public class Tmct0ExpedidasFilesBo extends AbstractBo<Tmct0ExpedidasFiles, Long, Tmct0ExpedidasFilesDao> {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ExpedidasFilesBo.class);

	@Autowired
	Tmct0InterfacesEmitidasConstDao tmct0InterfacesEmitidasConstDao;

	@Autowired
	Tmct0UsuariosExpedidasDao tmct0UsuariosExpedidasDao;

	@Autowired
	private Tmct0ExpedidasFilesDaoImp tmct0ExpedidasFilesDaoImp;

	@Autowired
	private RespFacturasEmitidasAssembler respFactsEmitAssembler;

	@Autowired
	private SendMail sendMail;

	@Value("${interfacesemitidas.folder.in}")
	private String folderInterfacesEmitidasIn;

	@Value("${interfacesemitidas.folder.in.tratados}")
	private String folderInterfacesEmitidasInTratados;

	@Value("${sibbac.interfacesemitidas.mail.errores.body}")
	private String bodyMail;

	@Value("${sibbac.interfacesemitidas.mail.errores.subject}")
	private String subjectMail;

	/**
	 * Utilizado para la busqueda de Respuesta de errores GC.
	 * 
	 * @param rErrExpFilterDTO
	 *          rErrExpFilterDTO
	 * @return List<BusquedaRespuestaErrGCDTO> List<BusquedaRespuestaErrGCDTO>
	 * @throws SIBBACBusinessException
	 *           SIBBACBusinessException
	 */
	public List<BusquedaRespuestaErrGCDTO> findRespuestaErroresGC(RespuestaErroresExpedidasFilterDTO rErrExpFilterDTO)
	    throws SIBBACBusinessException {
		return this.tmct0ExpedidasFilesDaoImp.findRespuestaErroresGC(rErrExpFilterDTO);
	}

	@Transactional
	public Tmct0ExpedidasFiles createHistorico(String nbFichero, InterfazEnum tipo) throws SIBBACBusinessException {
		LOG.info("Creando nuevo registro de {} con fichero {}", tipo.getDescription(), nbFichero);
		Tmct0ExpedidasFiles file = new Tmct0ExpedidasFiles();
		file.setNbFichero(nbFichero);
		file.setFhEnviRecep(new Date());
		file.setTpFichero(tipo.getDescription());
		save(file);
		return file;
	}

	/**
	 * Se almacenan bloques de datos (BLOQUE 01) de los ficheros de respuesta.
	 * 
	 * @param files:
	 *          son los ficheros de respuesta procesados
	 * @throws SIBBACBusinessException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void saveAndProcessRespuestaExpedidasFromFiles(List<File> files, String respuesta)
	    throws SIBBACBusinessException {
		final List<String> ficherosConErrores;
		final Date fechaEnvRecep;
		String message, line;

		LOG.debug("[saveAndProcessRespuestaExpedidasFromFiles] Inicio");
		if (CollectionUtils.isEmpty(files)) {
			LOG.info("[saveAndProcessRespuestaExpedidasFromFiles] No se encontraron ficheros de respuesta a procesar.");
			return;
		}
		ficherosConErrores = new ArrayList<>();
		fechaEnvRecep = new Date();
		for (File file : files) {
			line = null;
			EstructuraRespuestaDTO estrRespDTO = new EstructuraRespuestaDTO();
			estrRespDTO.setNombreFichero(file.getName());
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				while ((line = br.readLine()) != null) {
					estrRespDTO = this.respFactsEmitAssembler.getDTOFromRow(line, estrRespDTO);
					// Si es un bloque con datos de factura y la operacion no fue
					// exitosa
					// se asume que hubo errores.
					if (estrRespDTO != null && estrRespDTO.isBloqueIdentificacionFacturaRespuesta()
					    && !estrRespDTO.isOperacionExitosa()) {
						ficherosConErrores.add(file.getName());
					}
				}
				// Se almacena el historico si existe el directorio de tratados.
				File dirTratados = new File(folderInterfacesEmitidasInTratados);
				if (dirTratados != null && dirTratados.exists()) {
					this.saveHistoricoAndUpdateFicheroEnviado(estrRespDTO, fechaEnvRecep, respuesta);
				}
				br.close();

				// Se mueve el fichero a la carpeta tratados.
				Files.move(file, new File(folderInterfacesEmitidasInTratados, file.getName()));

			} catch (ParseException pex) {
				message = MessageFormat.format("Error en el parseo del fichero {0}: linea {1}", file.getName(), line);
				LOG.error("saveAndProcessRespuestaExpedidasFromFiles] {}", message);
				throw new SIBBACBusinessException(message, pex);
			} catch (IOException ioex) {
				message = MessageFormat.format("Error de proceso de fichero: {0}", file.getName());
				LOG.error("[saveAndProcessRespuestaExpedidasFromFiles] {}", message);
				throw new SIBBACBusinessException(message, ioex);
			} catch (RuntimeException ex) {
				LOG.error("[saveAndProcessRespuestaExpedidasFromFiles] Error inesperado", ex);
				throw new SIBBACBusinessException("Error no controlado", ex);
			}
		}

		if (CollectionUtils.isNotEmpty(ficherosConErrores)) {
			Set<String> ficherosConErroresSet = new HashSet<String>(ficherosConErrores);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			// El texto que contendrá el email deberá llevar la siguiente
			// información:
			// - Nombre del fichero que contiene los errores.
			// - Fecha de recepción del fichero.
			// - Texto que le recuerde que existen errores en el fichero y
			// además la obligatoriedad de corregirlos en los siguientes tres días
			// debido a la caducidad de la factura.
			List<String> listaEmails = this.tmct0UsuariosExpedidasDao.findAllEmailActivosOrderByEmail();
			if (CollectionUtils.isNotEmpty(listaEmails)) {
				try {
					this.sendMail.sendMail(listaEmails, subjectMail,
					    bodyMail + "\n" + sdf.format(fechaEnvRecep) + ". \n" + StringUtils.join(ficherosConErroresSet, ","));
				} catch (IOException | MessagingException e) {
					LOG.error("[saveAndProcessRespuestaExpedidasFromFiles] {}",
					    "Error en el envío de mail de notificación de errores en interfaces emitidas");
					throw new SIBBACBusinessException(e);
				}
			} else {
				// En caso de que nadie pueda atender los errores debe informarse
				// sobre esto.
				LOG.error("[saveAndProcessRespuestaExpedidasFromFiles] No hay usuarios disponibles para atender los errores");
				throw new SIBBACBusinessException("No hay usuarios disponibles para atender los errores");
			}
		}
		LOG.debug("[saveAndProcessRespuestaExpedidasFromFiles] Fin");
	}

	/**
	 * Se obtienen datos del fichero almacenado en fileSystem.
	 * 
	 * @param fileName:
	 *          nombre del fichero seleccionado
	 * @throws SIBBACBusinessException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public List<ErroresFicheroModalDTO> getListadoErroresModalByNombreFichero(String fileName)
	    throws SIBBACBusinessException {
		final List<File> ficheros;
		List<EstructuraRespuestaDTO> listaEstrRespDTO;
		String message;

		LOG.debug("[getListadoErroresModalByNombreFichero] Inicio");
		ficheros = FileHelper.getListaArchivosEnDirectorioByName(folderInterfacesEmitidasInTratados, fileName);
		if (CollectionUtils.isEmpty(ficheros)) {
			LOG.info("[getListadoErroresModalByNombreFichero] lista de ficheros vacía -> Retorno sin errores");
			return Collections.emptyList();
		}
		listaEstrRespDTO = new ArrayList<>();
		for (File file : ficheros) {
			EstructuraRespuestaDTO estrRespDTO = new EstructuraRespuestaDTO();
			String line = null;
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				while ((line = br.readLine()) != null) {
					estrRespDTO = this.respFactsEmitAssembler.getDTOFromRow(line, estrRespDTO);
				}
				listaEstrRespDTO.add(estrRespDTO);
				br.close();
			} catch (ParseException | IOException ex) {
				message = MessageFormat.format("Error al parsear fichero {}, linea {}", file.getName(), line);
				LOG.error("[getListadoErroresModalByNombreFichero] {}", message);
				throw new SIBBACBusinessException(message, ex);
			}
		}
		if (CollectionUtils.isNotEmpty(listaEstrRespDTO)) {
			LOG.info("[getListadoErroresModalByNombreFichero] La lista no es vacia, se convierte a errores y retorna");
			return ErroresFicheroModalDTO.convertFromDTO(listaEstrRespDTO);
		}
		LOG.info("[getListadoErroresModalByNombreFichero] Retorno sin errores");
		return Collections.emptyList();
	}

	/**
	 * Retorna el catálogo de tipos de fichero
	 * 
	 * @return List<SelectDTO>
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoTipoFicheros() throws SIBBACBusinessException {
		List<SelectDTO> l = new ArrayList<SelectDTO>();
		List<Tmct0InterfacesEmitidasConst> listInterfEmitConst = this.tmct0InterfacesEmitidasConstDao
		    .findByGrupo(Constantes.GRUPO_INTERFACES);
		if (CollectionUtils.isNotEmpty(listInterfEmitConst)) {
			for (Tmct0InterfacesEmitidasConst interfEmitConst : listInterfEmitConst) {
				l.add(new SelectDTO(interfEmitConst.getClave(), interfEmitConst.getValor()));
			}
		}
		return l;
	}

	/**
	 * Retorna el catálogo de estados
	 * 
	 * @return List<SelectDTO>
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoEstados() throws SIBBACBusinessException {
		List<SelectDTO> l = new ArrayList<SelectDTO>();
		List<Tmct0InterfacesEmitidasConst> listInterfEmitConst = this.tmct0InterfacesEmitidasConstDao
		    .findByGrupo(Constantes.GRUPO_ESTADOS);
		if (CollectionUtils.isNotEmpty(listInterfEmitConst)) {
			for (Tmct0InterfacesEmitidasConst interfEmitConst : listInterfEmitConst) {
				l.add(new SelectDTO(interfEmitConst.getClave(), interfEmitConst.getValor()));
			}
		}
		return l;
	}

	/**
	 * Retorna el catálogo de nombres
	 * 
	 * @return
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoNombresFicheros() throws SIBBACBusinessException {
		return this.tmct0ExpedidasFilesDaoImp.getCatalogoNombresFicheros();
	}

	/**
	 * Se guarda el historico para el caso de la respuesta y actualiza fichero
	 * enviado.
	 * 
	 * @param estrRespDTO
	 * @throws SIBBACBusinessException
	 */
	private void saveHistoricoAndUpdateFicheroEnviado(EstructuraRespuestaDTO estrRespDTO, Date fechaEnvRecep,
	    String respuesta) throws SIBBACBusinessException {
		final Tmct0ExpedidasFiles tmct0ExpedidasFiles;
		final IdentificacionFacturaRespuestaDTO identificacionFactura;
		
		if (estrRespDTO == null) {
			LOG.debug("[saveHistoricoAndUpdateFicheroEnviado] Parametro estrRespDTO nulo, devuelve sin ejecución");
			return;
		}
		LOG.debug("[saveHistoricoAndUpdateFicheroEnviado] Inicio...");
		tmct0ExpedidasFiles = new Tmct0ExpedidasFiles();
		tmct0ExpedidasFiles.setNbFichero(estrRespDTO.getNombreFichero());
		tmct0ExpedidasFiles.setTpFichero(
		    InterfacesEmitidasHelper.getTipoFicheroByNombreFichero(estrRespDTO.getNombreFichero(), respuesta));
		identificacionFactura = estrRespDTO.getIdentificacionFacturaRespuestaDTO();
		if(identificacionFactura != null) {
			tmct0ExpedidasFiles.setEstado(InterfacesEmitidasHelper.getEstadoByCodigoRetorno(
					identificacionFactura.getCodigoRetorno()));
		}
		else {
			LOG.warn("[saveHistoricoAndUpdateFicheroEnviado] El identificador de factura de la respuesta {} recibida es nulo.",
					estrRespDTO.getNombreFichero());
		}
		tmct0ExpedidasFiles.setFhEnviRecep(fechaEnvRecep);
		tmct0ExpedidasFiles.setAsociacion(
		    InterfacesEmitidasHelper.getAsociacionRespByNombreFichero(estrRespDTO.getNombreFichero(), respuesta));
		this.save(tmct0ExpedidasFiles);
		LOG.debug("[saveHistoricoAndUpdateFicheroEnviado] Se ha almacenado la factura expedida {} con nombre {}", 
				tmct0ExpedidasFiles.getId(), tmct0ExpedidasFiles.getNbFichero());
		// Generado el historico del fichero se actualiza el fichero enviado
		// asociado.
		if (tmct0ExpedidasFiles.getAsociacion() != null) {
			List<Tmct0ExpedidasFiles> listaTmct0ExpedidasFilesEnvio = this.dao
			    .findByNombreFichero(tmct0ExpedidasFiles.getAsociacion());
			if (CollectionUtils.isNotEmpty(listaTmct0ExpedidasFilesEnvio)) {
				for (Tmct0ExpedidasFiles tExpFiles : listaTmct0ExpedidasFilesEnvio) {
					tExpFiles.setAsociacion(tmct0ExpedidasFiles.getNbFichero());
					this.save(tExpFiles);
				}
				LOG.debug("[saveHistoricoAndUpdateFicheroEnviado] Se han almacenado {} relaciones para el fichero {}", 
						listaTmct0ExpedidasFilesEnvio.size(), tmct0ExpedidasFiles.getNbFichero());
			}
		}
		LOG.debug("[saveHistoricoAndUpdateFicheroEnviado] Fin");
	}

}
