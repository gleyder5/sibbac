package sibbac.business.interfacesemitidas.dto.assembler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativasManualesDAO;
import sibbac.business.interfacesemitidas.database.model.Tmct0CausaExencion;
import sibbac.business.interfacesemitidas.database.model.Tmct0ClaveRegimen;
import sibbac.business.interfacesemitidas.database.model.Tmct0CodigoImpuesto;
import sibbac.business.interfacesemitidas.database.model.Tmct0Exentas;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0Monedas;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;
import sibbac.business.interfacesemitidas.database.model.Tmct0Sujetas;
import sibbac.business.interfacesemitidas.database.model.Tmct0TipoFactura;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0RectificativasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0RectificativasManualesTablaDTO;
import sibbac.business.interfacesemitidas.helper.InterfacesEmitidasHelper;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_FACTURA_MANUAL
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0RectificativasManualesAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0RectificativasManualesAssembler.class);

	@Autowired
	private Tmct0RectificativasManualesDAO tmct0RectificativasManualesDAO;
	
	@Autowired
	private Tmct0FacturasManualesDAO tmct0FacturasManualesDAO;
	
	public Tmct0RectificativasManualesAssembler() {
		super();
	}

	/**
	 * Realiza la conversion entre los parámetros de la visual y la entidad
	 * 
	 * @param paramsObjects -> parámetros que manda la visual
	 * @return Entidad con los datos de la visual tratados
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public Tmct0RectificativaManual getTmct0RectificativasManualesEntidad(Map<String, Object> paramsObjects,
			boolean esModificacion) throws ParseException {

		LOG.info("INICIO - ASSEMBLER - getTmct0RectificativasManualesEntidad");

		Tmct0RectificativaManual entidadRectificativasManuales = new Tmct0RectificativaManual();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();

		try {

			// Si es una modificación seteamos el id
			if (esModificacion) {
				entidadRectificativasManuales = tmct0RectificativasManualesDAO.findOne(new BigInteger(
						String.valueOf(paramsObjects.get("id"))));
			}
			// Si es un alta seteamos las fechas
			if (!esModificacion) {
				entidadRectificativasManuales.setFhFechaCre(dateFormat.parse(dateFormat.format(date)));
				entidadRectificativasManuales.setFhFechaFac(dateFormat.parse(dateFormat.format(date)));
				entidadRectificativasManuales.setFhInicio(dateFormat.parse(dateFormat.format(date)));
				entidadRectificativasManuales.setEnviado(false);
			}

			entidadRectificativasManuales.setAuditDate(dateFormat.parse(dateFormat.format(date)));
			entidadRectificativasManuales.setAuditUser(String.valueOf(paramsObjects.get("auditUser")));
			entidadRectificativasManuales.setIdAlias(new BigInteger(String.valueOf(paramsObjects.get("idAlias"))));
			//MFG la causa de exencion ha dejado de ser obligatoria  
			if ((paramsObjects.get("idCausaExencion") != null) && (StringUtils.isNotEmpty(String.valueOf(paramsObjects.get("idCausaExencion"))))){
				entidadRectificativasManuales.setCausaExencion(new Tmct0CausaExencion());
				entidadRectificativasManuales.getCausaExencion()
				.setId(Integer.parseInt(String.valueOf(paramsObjects.get("idCausaExencion"))));
			}    
			entidadRectificativasManuales.setClaveRegimen(new Tmct0ClaveRegimen());
			entidadRectificativasManuales.getClaveRegimen()
			.setId(Integer.parseInt(String.valueOf(paramsObjects.get("idClaveRegimen"))));
			entidadRectificativasManuales.setCodImpuesto(new Tmct0CodigoImpuesto());
			entidadRectificativasManuales.getCodImpuesto()
			.setId(new BigInteger(String.valueOf(paramsObjects.get("idCodImpuesto"))));
			entidadRectificativasManuales.setIdEntregaBien(new Short(String.valueOf(paramsObjects.get("idEntregaBien"))));
			entidadRectificativasManuales.setMoneda(new Tmct0Monedas());
			entidadRectificativasManuales.getMoneda().setId(new Integer(String.valueOf(paramsObjects.get("idMoneda"))));
			entidadRectificativasManuales.setTipoFactura(new Tmct0TipoFactura());
			entidadRectificativasManuales.getTipoFactura()
			.setId(Integer.parseInt(String.valueOf(paramsObjects.get("idTipoFactura"))));
			entidadRectificativasManuales.setImpBaseImponible(new BigDecimal(
					String.valueOf(paramsObjects.get("impBaseImponible"))));
			entidadRectificativasManuales.setImpImpuesto(new BigDecimal(String.valueOf(paramsObjects.get("impImpuesto"))));
			entidadRectificativasManuales.setNbComentarios(String.valueOf(paramsObjects.get("nbComentarios")));
			entidadRectificativasManuales.setNbDocNumero(new BigInteger(String.valueOf(paramsObjects.get("nbDocNumero"))));
			entidadRectificativasManuales.setPeriodo(InterfacesEmitidasHelper.getPeriodo());
			entidadRectificativasManuales.setImFinDiv(new BigDecimal(String.valueOf(paramsObjects.get("imFinDiv"))));
			entidadRectificativasManuales.setImFinSvb(new BigDecimal(String.valueOf(paramsObjects.get("imFinSvb"))));
			entidadRectificativasManuales.setBaseImponibleActual(new BigDecimal(String.valueOf(paramsObjects.get("baseImponibleActual"))));
			entidadRectificativasManuales.setBaseImponibleAnterior(new BigDecimal(String.valueOf(paramsObjects.get("baseImponibleAnterior"))));

			/** Inicio: Campos nullables solo rellenables para T. Factura Sujeta: NO EXENTA. */
			String baseImponCostes = paramsObjects.get("baseImponCostes") != null ? String.valueOf(paramsObjects.get("baseImponCostes"))
					: null;
			if (StringUtils.isNotBlank(baseImponCostes)) {
				entidadRectificativasManuales.setBaseImponCostes(new BigDecimal(baseImponCostes));
			} else {
				entidadRectificativasManuales.setBaseImponCostes(null);
			}

			String coefImpCostes = paramsObjects.get("coefImpCostes") != null ? String.valueOf(paramsObjects.get("coefImpCostes"))
					: null;
			if (StringUtils.isNotBlank(coefImpCostes)) {
				entidadRectificativasManuales.setCoefImpCostes(new BigDecimal(coefImpCostes));
			} else {
				entidadRectificativasManuales.setCoefImpCostes(null);
			}

			String cuotaRepercut = paramsObjects.get("cuotaRepercut") != null ? String.valueOf(paramsObjects.get("cuotaRepercut"))
					: null;
			if (StringUtils.isNotBlank(cuotaRepercut)) {
				entidadRectificativasManuales.setCuotaRepercut(new BigDecimal(cuotaRepercut));
			} else {
				entidadRectificativasManuales.setCuotaRepercut(null);
			}
			/** Fin: Campos nullables solo rellenables para T. Factura Sujeta: NO EXENTA. */

			String idExenta = paramsObjects.get("idExenta") != null ? String.valueOf(paramsObjects.get("idExenta")) : null;
			if (StringUtils.isNotBlank(idExenta)) {
				entidadRectificativasManuales.setTmct0Exentas(new Tmct0Exentas());
				entidadRectificativasManuales.getTmct0Exentas().setId(Integer.parseInt(String.valueOf(idExenta)));
			}

			String idSujeta = paramsObjects.get("idSujeta") != null ? String.valueOf(paramsObjects.get("idSujeta")) : null;
			if (StringUtils.isNotBlank(idSujeta)) {
				entidadRectificativasManuales.setTmct0Sujetas(new Tmct0Sujetas());
				entidadRectificativasManuales.getTmct0Sujetas().setId(Integer.parseInt(String.valueOf(idSujeta)));
			} else {
				entidadRectificativasManuales.setTmct0Sujetas(null);
			}

			if (entidadRectificativasManuales.getNbDocNumero().toString().startsWith("6") && StringUtils.isNotBlank(cuotaRepercut)) {
				entidadRectificativasManuales.setImpImpuesto(entidadRectificativasManuales.getCuotaRepercut());
			}

			List<Map<String, String>> listaRectificadas = (List<Map<String, String>>) paramsObjects.get("listaFacturasRectificadas");

			for (int i = 0; i < listaRectificadas.size(); i++) {
				switch (i) {
				case 0:
					entidadRectificativasManuales.setNbDocNumero0(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 1:
					entidadRectificativasManuales.setNbDocNumero1(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 2:
					entidadRectificativasManuales.setNbDocNumero2(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 3:
					entidadRectificativasManuales.setNbDocNumero3(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 4:
					entidadRectificativasManuales.setNbDocNumero4(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 5:
					entidadRectificativasManuales.setNbDocNumero5(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 6:
					entidadRectificativasManuales.setNbDocNumero6(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 7:
					entidadRectificativasManuales.setNbDocNumero7(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 8:
					entidadRectificativasManuales.setNbDocNumero8(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				case 9:
					entidadRectificativasManuales.setNbDocNumero9(new BigInteger(
							String.valueOf((listaRectificadas.get(i).get("value")))));
					break;
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0RectificativasManualesEntidad -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0RectificativasManualesEntidad");

		return entidadRectificativasManuales;

	}

	/**
	 * Realiza la conversion entre la entidad y el dto
	 * 
	 * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0RectificativasManualesTablaDTO getTmct0RectificativasManualesTablaDTO(Tmct0RectificativaManual datosRectificativasManuales,
			String descrAli) {

		LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

		Tmct0RectificativasManualesTablaDTO rectificativasManualesTablaDTO = new Tmct0RectificativasManualesTablaDTO();

		try {
			rectificativasManualesTablaDTO.setId(datosRectificativasManuales.getId());
			rectificativasManualesTablaDTO.setFhFechaCre(datosRectificativasManuales.getFhFechaCre());
			rectificativasManualesTablaDTO.setFhFechaFac(datosRectificativasManuales.getFhFechaFac());
			rectificativasManualesTablaDTO.setEmpContraparte(descrAli);
			rectificativasManualesTablaDTO.setEstado(datosRectificativasManuales.getEstado().getNombre());
			rectificativasManualesTablaDTO.setImpBaseImponible(datosRectificativasManuales.getImpBaseImponible());
			rectificativasManualesTablaDTO.setImpImpuesto(datosRectificativasManuales.getImpImpuesto());
			rectificativasManualesTablaDTO.setNbDocNumero(datosRectificativasManuales.getNbDocNumero());
			rectificativasManualesTablaDTO.setCoefImpCostes(datosRectificativasManuales.getCoefImpCostes());
			rectificativasManualesTablaDTO.setBaseImponCostes(datosRectificativasManuales.getBaseImponCostes());
			rectificativasManualesTablaDTO.setCuotaRepercut(datosRectificativasManuales.getCuotaRepercut());
			rectificativasManualesTablaDTO.setImFinSvb(datosRectificativasManuales.getImFinSvb());
			rectificativasManualesTablaDTO.setImFinDiv(datosRectificativasManuales.getImFinDiv());
			rectificativasManualesTablaDTO.setNbComentarios(datosRectificativasManuales.getNbComentarios());
			rectificativasManualesTablaDTO.setClaveRegimen(datosRectificativasManuales.getClaveRegimen().getId());
			if (datosRectificativasManuales.getEstado().getNombre().equals(Constantes.ESTADO_ANULADA)
					|| datosRectificativasManuales.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)
					|| datosRectificativasManuales.getEstado().getNombre().equals(Constantes.ESTADO_PDTE_CONTABILIZAR)) {
				rectificativasManualesTablaDTO.setEsModificable(false);
			} else {
				rectificativasManualesTablaDTO.setEsModificable(true);
			}

			if (datosRectificativasManuales.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)) {
				rectificativasManualesTablaDTO.setEsAnulable(true);
			} else {
				rectificativasManualesTablaDTO.setEsAnulable(false);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FacturasManualesTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

		return rectificativasManualesTablaDTO;

	}

	/**
	 * Realiza la conversion entre la entidad y el dto
	 * 
	 * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0RectificativasManualesTablaDTO getTmct0RectificativasManualesTablaDTO(Tmct0RectificativaBajasManuales datosRectificativasManualesBaja,
			String descrAli) {

		LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

		Tmct0RectificativasManualesTablaDTO rectificativasManualesTablaDTO = new Tmct0RectificativasManualesTablaDTO();

		try {
			rectificativasManualesTablaDTO.setId(datosRectificativasManualesBaja.getId());
			rectificativasManualesTablaDTO.setFhFechaCre(datosRectificativasManualesBaja.getFhFechaCre());
			rectificativasManualesTablaDTO.setFhFechaFac(datosRectificativasManualesBaja.getFhFechaFac());
			rectificativasManualesTablaDTO.setEmpContraparte(descrAli);
			rectificativasManualesTablaDTO.setEstado(datosRectificativasManualesBaja.getEstado().getNombre());
			rectificativasManualesTablaDTO.setImpBaseImponible(datosRectificativasManualesBaja.getImpBaseImponible());
			rectificativasManualesTablaDTO.setImpImpuesto(datosRectificativasManualesBaja.getImpImpuesto());
			rectificativasManualesTablaDTO.setNbDocNumero(datosRectificativasManualesBaja.getNbDocNumero());
			rectificativasManualesTablaDTO.setCoefImpCostes(datosRectificativasManualesBaja.getCoefImpCostes());
			rectificativasManualesTablaDTO.setBaseImponCostes(datosRectificativasManualesBaja.getBaseImponCostes());
			rectificativasManualesTablaDTO.setCuotaRepercut(datosRectificativasManualesBaja.getCuotaRepercut());
			rectificativasManualesTablaDTO.setImFinSvb(datosRectificativasManualesBaja.getImFinSvb());
			rectificativasManualesTablaDTO.setImFinDiv(datosRectificativasManualesBaja.getImFinDiv());
			rectificativasManualesTablaDTO.setNbComentarios(datosRectificativasManualesBaja.getNbComentarios());
			rectificativasManualesTablaDTO.setBaseImponibleActual(datosRectificativasManualesBaja.getBaseImponibleActual());
			rectificativasManualesTablaDTO.setBaseImponibleAnterior(datosRectificativasManualesBaja.getBaseImponibleAnterior());
			rectificativasManualesTablaDTO.setClaveRegimen(datosRectificativasManualesBaja.getClaveRegimen().getId());
			if (datosRectificativasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_ANULADA)
					|| datosRectificativasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)
					|| datosRectificativasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_PDTE_CONTABILIZAR)) {
				rectificativasManualesTablaDTO.setEsModificable(false);
			} else {
				rectificativasManualesTablaDTO.setEsModificable(true);
			}

			String nbDocNumeroString = String.valueOf(datosRectificativasManualesBaja.getNbDocNumero().longValue());
			if (datosRectificativasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA) && !nbDocNumeroString.startsWith("9")) {
				rectificativasManualesTablaDTO.setEsAnulable(true);
			} else {
				rectificativasManualesTablaDTO.setEsAnulable(false);
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FacturasManualesTablaDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

		return rectificativasManualesTablaDTO;

	}

	/**
	 * Realiza la conversion entre la entidad y el dto Tmct0FacturasManualesDTO
	 * 
	 * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0FacturasManualesDTO getTmct0FacturasManualesDTO(Tmct0FacturasManuales datosFacturasManuales) {

		LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesDTO");

		Tmct0FacturasManualesDTO facturasManualesDTO = new Tmct0FacturasManualesDTO();

		try {

			facturasManualesDTO.setFhFechaCre(datosFacturasManuales.getFhFechaCre());
			facturasManualesDTO.setFhFechaFac(datosFacturasManuales.getFhFechaFac());
			facturasManualesDTO.setIdAlias(datosFacturasManuales.getIdAlias());
			facturasManualesDTO.setIdEstado(datosFacturasManuales.getEstado().getIdestado());
			facturasManualesDTO.setImpBaseImponible(datosFacturasManuales.getImpBaseImponible());
			facturasManualesDTO.setImpImpuesto(datosFacturasManuales.getImpImpuesto());
			facturasManualesDTO.setNbDocNumero(datosFacturasManuales.getNbDocNumero());
			facturasManualesDTO.setCoefImpCostes(datosFacturasManuales.getCoefImpCostes());
			facturasManualesDTO.setBaseImponCostes(datosFacturasManuales.getBaseImponCostes());
			facturasManualesDTO.setCuotaRepercut(datosFacturasManuales.getCuotaRepercut());

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FacturasManualesDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesDTO");

		return facturasManualesDTO;

	}

	/**
	 * Realiza la conversion entre la entidad y el ClientesDTO a Tmct0FacturasManualesDTO
	 * 
	 * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
	 * @param clienteDTO -> DTO para añadir al objeto cliente del Tmct0FacturasManualesDTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0RectificativasManualesDTO getTmct0RectificativasManualesDetalleDTO(Tmct0RectificativaManual datosRectificativasManuales) {

		LOG.info("INICIO - ASSEMBLER - getTmct0RectificativasManualesDetalleDTO");

		List<Object> listaFacturasRectificadas = new ArrayList<Object>();
		Tmct0RectificativasManualesDTO rectificativasManualesDTO = new Tmct0RectificativasManualesDTO();

		try {

			rectificativasManualesDTO.setAuditDate(datosRectificativasManuales.getAuditDate());
			rectificativasManualesDTO.setAuditUser(datosRectificativasManuales.getAuditUser());
			rectificativasManualesDTO.setBaseImponCostes(datosRectificativasManuales.getBaseImponCostes());
			rectificativasManualesDTO.setCoefImpCostes(datosRectificativasManuales.getCoefImpCostes());
			rectificativasManualesDTO.setCuotaRepercut(datosRectificativasManuales.getCuotaRepercut());
			rectificativasManualesDTO.setFhFechaCre(datosRectificativasManuales.getFhFechaCre());
			rectificativasManualesDTO.setFhFechaFac(datosRectificativasManuales.getFhFechaFac());
			rectificativasManualesDTO.setFhInicio(datosRectificativasManuales.getFhInicio());
			rectificativasManualesDTO.setId(datosRectificativasManuales.getId());
			rectificativasManualesDTO.setIdAlias(datosRectificativasManuales.getIdAlias());
			if (datosRectificativasManuales.getCausaExencion() != null) {
				rectificativasManualesDTO.setIdCausaExencion(datosRectificativasManuales.getCausaExencion().getId());
			}

			rectificativasManualesDTO.setIdClaveRegimen(datosRectificativasManuales.getClaveRegimen().getId());
			rectificativasManualesDTO.setIdCodImpuesto(new Integer(
					String.valueOf(datosRectificativasManuales.getCodImpuesto()
							.getId())));
			rectificativasManualesDTO.setIdEntregaBien(datosRectificativasManuales.getIdEntregaBien());
			rectificativasManualesDTO.setIdEstado(datosRectificativasManuales.getEstado().getIdestado());
			rectificativasManualesDTO.setIdMoneda(datosRectificativasManuales.getMoneda().getId());
			rectificativasManualesDTO.setIdTipoFactura(datosRectificativasManuales.getTipoFactura().getId());
			rectificativasManualesDTO.setImFinDiv(datosRectificativasManuales.getImFinDiv());
			rectificativasManualesDTO.setImFinSvb(datosRectificativasManuales.getImFinSvb());
			rectificativasManualesDTO.setImpBaseImponible(datosRectificativasManuales.getImpBaseImponible());
			rectificativasManualesDTO.setImpImpuesto(datosRectificativasManuales.getImpImpuesto());
			rectificativasManualesDTO.setNbComentarios(datosRectificativasManuales.getNbComentarios());
			rectificativasManualesDTO.setNbDocNumero(datosRectificativasManuales.getNbDocNumero());
			rectificativasManualesDTO.setPeriodo(datosRectificativasManuales.getPeriodo());
			rectificativasManualesDTO.setBaseImponibleActual(datosRectificativasManuales.getBaseImponibleActual());
			rectificativasManualesDTO.setBaseImponibleAnterior(datosRectificativasManuales.getBaseImponibleAnterior());

			List<Tmct0FacturasManuales> facturaARectificar = new ArrayList<Tmct0FacturasManuales>();
			if (null != datosRectificativasManuales.getNbDocNumero0() && !datosRectificativasManuales.getNbDocNumero0().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero0());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero0().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero1() && !datosRectificativasManuales.getNbDocNumero1().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero1());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero1().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero2() && !datosRectificativasManuales.getNbDocNumero2().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero2());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero2().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero3() && !datosRectificativasManuales.getNbDocNumero3().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero3());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero3().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero4() && !datosRectificativasManuales.getNbDocNumero4().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero4());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero4().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero5() && !datosRectificativasManuales.getNbDocNumero5().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero5());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero5().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero6() && !datosRectificativasManuales.getNbDocNumero6().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero6());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero6().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero7() && !datosRectificativasManuales.getNbDocNumero7().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero7());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero7().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero8() && !datosRectificativasManuales.getNbDocNumero8().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero8());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero8().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero9() && !datosRectificativasManuales.getNbDocNumero9().equals("")) {
				facturaARectificar = tmct0FacturasManualesDAO.findBynbDocNumero(datosRectificativasManuales.getNbDocNumero9());
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(), datosRectificativasManuales.getNbDocNumero9().toString(), facturaARectificar.get(0).getImpBaseImponible().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			rectificativasManualesDTO.setListaFacturasRectificadas(listaFacturasRectificadas);

			if (datosRectificativasManuales.getTmct0Exentas() != null) {
				rectificativasManualesDTO.setIdExenta(String.valueOf(datosRectificativasManuales.getTmct0Exentas().getId()));
			}
			if (datosRectificativasManuales.getTmct0Sujetas() != null) {
				rectificativasManualesDTO.setIdSujeta(String.valueOf(datosRectificativasManuales.getTmct0Sujetas().getId()));
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0RectificativasManualesDetalleDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0RectificativasManualesDetalleDTO");

		return rectificativasManualesDTO;

	}

	/**
	 * Realiza la conversion entre la entidad Tmct0RectificativaBajasManuales a Tmct0RectificativasManualesDTO
	 * 
	 * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
	 * @param clienteDTO -> DTO para añadir al objeto cliente del Tmct0FacturasManualesDTO
	 * @return DTO con los datos de la entidad tratados
	 */
	public Tmct0RectificativasManualesDTO getTmct0RectificativasBajaDetalleDTO(Tmct0RectificativaBajasManuales datosRectificativasManuales) {

		LOG.info("INICIO - ASSEMBLER - getTmct0RectificativasManualesDetalleDTO");

		List<Object> listaFacturasRectificadas = new ArrayList<Object>();
		Tmct0RectificativasManualesDTO rectificativasManualesDTO = new Tmct0RectificativasManualesDTO();

		try {

			rectificativasManualesDTO.setAuditDate(datosRectificativasManuales.getAuditDate());
			rectificativasManualesDTO.setAuditUser(datosRectificativasManuales.getAuditUser());
			rectificativasManualesDTO.setBaseImponCostes(datosRectificativasManuales.getBaseImponCostes());
			rectificativasManualesDTO.setCoefImpCostes(datosRectificativasManuales.getCoefImpCostes());
			rectificativasManualesDTO.setCuotaRepercut(datosRectificativasManuales.getCuotaRepercut());
			rectificativasManualesDTO.setFhFechaCre(datosRectificativasManuales.getFhFechaCre());
			rectificativasManualesDTO.setFhFechaFac(datosRectificativasManuales.getFhFechaFac());
			rectificativasManualesDTO.setFhInicio(datosRectificativasManuales.getFhInicio());
			rectificativasManualesDTO.setId(datosRectificativasManuales.getId());
			rectificativasManualesDTO.setIdAlias(datosRectificativasManuales.getIdAlias());

			if( datosRectificativasManuales.getCausaExencion() != null){
				rectificativasManualesDTO.setIdCausaExencion(datosRectificativasManuales.getCausaExencion().getId());  
			}

			rectificativasManualesDTO.setIdClaveRegimen(datosRectificativasManuales.getClaveRegimen().getId());
			rectificativasManualesDTO.setIdCodImpuesto(new Integer(
					String.valueOf(datosRectificativasManuales.getCodImpuesto()
							.getId())));
			rectificativasManualesDTO.setIdEntregaBien(datosRectificativasManuales.getIdEntregaBien());
			rectificativasManualesDTO.setIdEstado(datosRectificativasManuales.getEstado().getIdestado());
			rectificativasManualesDTO.setIdMoneda(datosRectificativasManuales.getMoneda().getId());
			rectificativasManualesDTO.setIdTipoFactura(datosRectificativasManuales.getTipoFactura().getId());
			rectificativasManualesDTO.setImFinDiv(datosRectificativasManuales.getImFinDiv());
			rectificativasManualesDTO.setImFinSvb(datosRectificativasManuales.getImFinSvb());
			rectificativasManualesDTO.setImpBaseImponible(datosRectificativasManuales.getImpBaseImponible());
			rectificativasManualesDTO.setImpImpuesto(datosRectificativasManuales.getImpImpuesto());
			rectificativasManualesDTO.setNbComentarios(datosRectificativasManuales.getNbComentarios());
			rectificativasManualesDTO.setNbDocNumero(datosRectificativasManuales.getNbDocNumero());
			rectificativasManualesDTO.setPeriodo(datosRectificativasManuales.getPeriodo());

			if (null != datosRectificativasManuales.getNbDocNumero0()
					&& !datosRectificativasManuales.getNbDocNumero0().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero0().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero1()
					&& !datosRectificativasManuales.getNbDocNumero1().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero1().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero2()
					&& !datosRectificativasManuales.getNbDocNumero2().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero2().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero3()
					&& !datosRectificativasManuales.getNbDocNumero3().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero3().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero4()
					&& !datosRectificativasManuales.getNbDocNumero4().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero4().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero5()
					&& !datosRectificativasManuales.getNbDocNumero5().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero5().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero6()
					&& !datosRectificativasManuales.getNbDocNumero6().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero6().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero7()
					&& !datosRectificativasManuales.getNbDocNumero7().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero7().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero8()
					&& !datosRectificativasManuales.getNbDocNumero8().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero8().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			if (null != datosRectificativasManuales.getNbDocNumero9()
					&& !datosRectificativasManuales.getNbDocNumero9().equals("")) {
				SelectDTO facturasRectificadasDTO = new SelectDTO(datosRectificativasManuales.getId().toString(),
						datosRectificativasManuales.getNbDocNumero9().toString());
				listaFacturasRectificadas.add(facturasRectificadasDTO);
			}
			rectificativasManualesDTO.setListaFacturasRectificadas(listaFacturasRectificadas);

			if (datosRectificativasManuales.getTmct0Exentas() != null) {
				rectificativasManualesDTO.setIdExenta(String.valueOf(datosRectificativasManuales.getTmct0Exentas().getId()));
			}
			if (datosRectificativasManuales.getTmct0Sujetas() != null) {
				rectificativasManualesDTO.setIdSujeta(String.valueOf(datosRectificativasManuales.getTmct0Sujetas().getId()));
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0RectificativasManualesDetalleDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0RectificativasManualesDetalleDTO");

		return rectificativasManualesDTO;

	}

	/**
	 * Realiza la conversion entre los parámetros de la visual y un objeto filtroDTO
	 * 
	 * @param paramsObjects -> parámetros que manda la visual
	 * @return Entidad con los datos de la visual tratados
	 * @throws ParseException
	 */
	public Tmct0FiltroFacturasManualesDTO getTmct0FiltroFacturasManualesDTO(Map<String, Object> paramsObjects) throws ParseException {

		LOG.info("INICIO - ASSEMBLER - getTmct0FiltroFacturasManualesDTO");

		Tmct0FiltroFacturasManualesDTO filtroFacturasManuales = new Tmct0FiltroFacturasManualesDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		try {

			if ((paramsObjects.get("fechaDesde") != null) && (!paramsObjects.get("fechaDesde").equals(""))) {
				filtroFacturasManuales.setFechaDesde(formatter.parse(String.valueOf(paramsObjects.get("fechaDesde"))));
			}
			if ((paramsObjects.get("fechaHasta") != null) && (!paramsObjects.get("fechaHasta").equals(""))) {
				filtroFacturasManuales.setFechaHasta(formatter.parse(String.valueOf(paramsObjects.get("fechaHasta"))));
			}
			if ((paramsObjects.get("idAlias") != null) && (!paramsObjects.get("idAlias").equals(""))) {
				filtroFacturasManuales.setIdAlias(new BigInteger(String.valueOf(paramsObjects.get("idAlias"))));
			}
			if ((paramsObjects.get("idEstado") != null) && (!paramsObjects.get("idEstado").equals(""))) {
				filtroFacturasManuales.setIdEstado(Integer.parseInt(String.valueOf(paramsObjects.get("idEstado"))));
			}
			if ((paramsObjects.get("nbDocNumero") != null) && (!paramsObjects.get("nbDocNumero").equals(""))) {
				filtroFacturasManuales.setNbDocNumero(new BigInteger(String.valueOf(paramsObjects.get("nbDocNumero"))));
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FiltroFacturasManualesDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0FiltroFacturasManualesDTO");

		return filtroFacturasManuales;

	}

	/**
	 * Realiza la conversion entre los parámetros de la visual y un objeto filtroDTO
	 * 
	 * @param paramsObjects -> parámetros que manda la visual
	 * @return Entidad con los datos de la visual tratados
	 * @throws ParseException
	 */
	public Tmct0FiltroFacturasManualesDTO getTmct0FiltroBajasManualesDTO(Map<String, Object> paramsObjects) throws ParseException {

		LOG.info("INICIO - ASSEMBLER - getTmct0FiltroBajasManualesDTO");

		Tmct0FiltroFacturasManualesDTO filtroFacturasManuales = new Tmct0FiltroFacturasManualesDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {

			if (!paramsObjects.get("fechaDesde").equals("")) {
				filtroFacturasManuales.setFechaDesde(formatter.parse(String.valueOf(paramsObjects.get("fechaDesde"))));
			}
			if (!paramsObjects.get("fechaHasta").equals("")) {
				filtroFacturasManuales.setFechaHasta(formatter.parse(String.valueOf(paramsObjects.get("fechaHasta"))));
			}
			if (!paramsObjects.get("idAlias").equals("")) {
				filtroFacturasManuales.setIdAlias(new BigInteger(String.valueOf(paramsObjects.get("idAlias"))));
			}
			if (!paramsObjects.get("nbDocNumero").equals("")) {
				filtroFacturasManuales.setNbDocNumero(new BigInteger(String.valueOf(paramsObjects.get("nbDocNumero"))));
			}

		} catch (Exception e) {
			LOG.error("Error metodo getTmct0FiltroBajasManualesDTO -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - ASSEMBLER - getTmct0FiltroBajasManualesDTO");

		return filtroFacturasManuales;

	}
}
