package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0RectificativasManualesDAO extends JpaRepository<Tmct0RectificativaManual, BigInteger> {

	@Query("SELECT f FROM Tmct0RectificativaManual f WHERE (f.enviado is null or f.enviado = false) and f.estado.idestado = :estado")
	public List<Tmct0RectificativaManual> findNoEnviadasIdByEstado(@Param("estado") Integer estado);

	@Query("SELECT f FROM Tmct0RectificativaManual f WHERE f.estado.idestado <> :estado and (f.enviado is null or f.enviado = false) and f.fhFechaCre >= :fechaDesde and f.fhFechaCre <= :fechaHasta")
	public List<Tmct0RectificativaManual> findNoEnviadasByIdEstadoDistinctBetweenFechas(@Param("estado") Integer estado,
			@Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);

	@Query( "SELECT factManu FROM Tmct0RectificativaManual factManu WHERE factManu.nbDocNumero = :nbDocNumero" )
	public List<Tmct0RectificativaManual> findBynbDocNumero(@Param( "nbDocNumero" ) BigInteger nbDocNumero);
	
	@Query( "SELECT factManu FROM Tmct0RectificativaManual factManu WHERE factManu.id in :listIds" )
	public List<Tmct0RectificativaManual> findById(@Param( "listIds" ) List<BigInteger> listIds);
	
	@Modifying @Query(value="DELETE FROM TMCT0_FACTURA_MANUAL_RECTIFICATIVA WHERE id IN :listId",nativeQuery=true)
	public void deleteFromId(@Param( "listId" ) List<BigInteger> listId);

}
