package sibbac.business.interfacesemitidas.rest;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.interfacesemitidas.bo.Tmct0FacturasManualesBo;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAOImpl;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesTablaDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.assembler.Tmct0FacturasManualesAssembler;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceBajasManuales implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceFacturasManuales.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0FacturasManualesAssembler tmct0FacturasManualesAssembler;

  @Autowired
  Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

  @Autowired
  Tmct0FacturasManualesDAOImpl tmct0FacturasManualesDaoImpl;

  @Autowired
  private AliasDao aliasdao;

  @Autowired
  private Tmct0FacturasManualesBo tmct0FacturasManualesBo;

  /**
   * The Enum ComandoBajasManuales.
   */
  private enum ComandoBajasManuales {

    /** Consultar facturas contabilizadas */
    CONSULTAR_FACTURAS_CONTABILIZADAS("consultarFacturasContabilizadas"),

    /** Consultar detalle baja manual */
    CONSULTAR_DETALLE("detalleBajaManual"),

    /** Anular bajas manuales */
    ANULAR_BAJAS_MANUALES("anularBajasManuales"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandoBajasManuales(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandoBajasManuales command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceFacturasManuales");

      switch (command) {
        case CONSULTAR_FACTURAS_CONTABILIZADAS:
          result.setResultados(consultarFacturasContabilizadas(paramsObjects));
          break;
        case CONSULTAR_DETALLE:
          result.setResultados(detalleBajaManual(paramsObjects));
          break;
        case ANULAR_BAJAS_MANUALES:
          result.setResultados(anularBajasManuales(paramsObjects));
          break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceFacturasManuales");
    return result;
  }

  /**
   * Consulta de las facturas manuales a raiz de los datos insertados en pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarFacturasContabilizadas(Map<String, Object> paramObjects) throws SIBBACBusinessException,
                                                                                               ParseException {

    LOG.info("INICIO - SERVICIO - consultarFacturasContabilizadas");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaFacturasManualesTablaDTO = new ArrayList<Object>();
    List<String> listCdAliass = new ArrayList<String>();

    try {
      // Transformamos los parámetros de filtrado en un objeto
      Tmct0FiltroFacturasManualesDTO filtroFacturasManuales = tmct0FacturasManualesAssembler.getTmct0FiltroBajasManualesDTO(paramObjects);

      // Realizamos la llamada a la consulta de facturas manuales
      List<Tmct0FacturasManuales> listaFacturasManuales = tmct0FacturasManualesDaoImpl.consultarFacturasContabilizadas(filtroFacturasManuales);

      // Se guardan en un array todos los idAlias (cdAliass) para recuperar las descripciones asociadas a ellos
      for (Tmct0FacturasManuales tmct0FacturasManuales : listaFacturasManuales) {
        listCdAliass.add(tmct0FacturasManuales.getIdAlias().toString());
      }

      // Se transforman a lista de tablaDTO
      for (int i = 0; i < listaFacturasManuales.size(); i++) {
        try {
          String descrAli = aliasdao.findDescraliByCdaliass(listCdAliass.get(i));
          Tmct0FacturasManualesTablaDTO facturasManualesTasblaDTO = tmct0FacturasManualesAssembler.getTmct0FacturasManualesTablaDTO(listaFacturasManuales.get(i),
                                                                                                                                    descrAli);
          listaFacturasManualesTablaDTO.add(facturasManualesTasblaDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarFacturasManuales - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarFacturasContabilizadas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarFacturasContabilizadas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaFacturasContabilizadas", listaFacturasManualesTablaDTO);

    LOG.info("FIN - SERVICIO - consultarFacturasContabilizadas");

    return result;
  }

  /**
   * Consulta el detalle de una baja manual
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> detalleBajaManual(Map<String, Object> paramObjects) throws SIBBACBusinessException,
                                                                                 ParseException {

    LOG.info("INICIO - SERVICIO - detalleBajaManual");

    Map<String, Object> result = new HashMap<String, Object>();
    Object objFacturaManualDTO = new Object();

    try {
      // Realizamos la llamada a la consulta de facturas manuales a raiz del número de factura
      List<Tmct0FacturasManuales> listaFacturasManuales = tmct0FacturasManualesDao.findBynbDocNumero(new BigInteger(
                                                                                                                    String.valueOf(paramObjects.get("nbDocNumero"))));

      // Se transforman a lista de tablaDTO
      for (Tmct0FacturasManuales tmct0FacturasManuales : listaFacturasManuales) {
        try {
          // Convertimos la entidad y el clientesDTO recuperados a Tmct0FacturasManualesDTO
          Tmct0FacturasManualesDTO facturasManualesDTO = tmct0FacturasManualesAssembler.getTmct0FacturasManualesDetalleDTO(tmct0FacturasManuales);

          objFacturaManualDTO = facturasManualesDTO;
        } catch (Exception e) {
          LOG.error("Error metodo detalleBajaManual - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo detalleBajaManual -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo detalleBajaManual -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("detalleFacturaContabilizada", objFacturaManualDTO);

    LOG.info("FIN - SERVICIO - detalleBajaManual");

    return result;
  }

  /**
   * Elimina las facturas manuales seleccionadas.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> anularBajasManuales(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - anularBajasManuales");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Integer> listaFMAnular = (List<Integer>) paramsObject.get("listaFacturasManualesAnular");

    for (Integer idFatura : listaFMAnular) {
      try {

        BigInteger biFactura = BigInteger.valueOf(idFatura.intValue());
        this.tmct0FacturasManualesBo.anularFacturaManualRectif(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL,
                                                               biFactura);
      } catch (Exception e) {
        LOG.error("Error metodo anularBajasManuales - Anulando Factura con id  " + idFatura + " Exception"
                      + e.getMessage(), e);
        result.put("status", "KO");
        return result;
      }
    }

    result.put("status", "OK");

    LOG.info("FIN - SERVICIO - anularBajasManuales");

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandoBajasManuales getCommand(String command) {
    ComandoBajasManuales[] commands = ComandoBajasManuales.values();
    ComandoBajasManuales result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandoBajasManuales.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    return null;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

}
