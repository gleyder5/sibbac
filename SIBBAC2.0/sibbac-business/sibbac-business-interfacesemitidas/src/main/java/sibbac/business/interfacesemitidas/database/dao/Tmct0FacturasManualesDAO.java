package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0FacturasManualesDAO extends JpaRepository<Tmct0FacturasManuales, BigInteger> {

	@Query("SELECT f FROM Tmct0FacturasManuales f WHERE (f.enviado is null or f.enviado = false) and f.estado.idestado = :estado")
	public List<Tmct0FacturasManuales> findNoEnviadasIdByEstado(@Param("estado") Integer estado);

	@Query("SELECT f FROM Tmct0FacturasManuales f WHERE f.estado.idestado not in (:estado) and (f.enviado is null or f.enviado = false) and f.fhFechaCre >= :fechaDesde and f.fhFechaCre <= :fechaHasta")
	public List<Tmct0FacturasManuales> findNoEnviadasByIdEstadoDistinctBetweenFechas(@Param("estado") List<Integer> estado,
			@Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);

	@Query( "SELECT factManu FROM Tmct0FacturasManuales factManu WHERE factManu.nbDocNumero = :nbDocNumero" )
	public List<Tmct0FacturasManuales> findBynbDocNumero(@Param( "nbDocNumero" ) BigInteger nbDocNumero);
	
	@Query( "SELECT factManu FROM Tmct0FacturasManuales factManu WHERE factManu.id in :listIds" )
	public List<Tmct0FacturasManuales> findById(@Param( "listIds" ) List<BigInteger> listIds);
	
	@Modifying @Query(value="DELETE FROM TMCT0_FACTURA_MANUAL WHERE id IN :listId",nativeQuery=true)
	public void deleteFromId(@Param( "listId" ) List<BigInteger> listId);
	
	@Query( "SELECT factManu FROM Tmct0FacturasManuales factManu WHERE factManu.estado.idestado = :idEstado ORDER BY factManu.id ASC")
	public List<Tmct0FacturasManuales> findAllOrderById(@Param("idEstado") Integer idEstado);
	
	@Modifying @Query("UPDATE Tmct0FacturasManuales SET enviado = true WHERE id = :id")
	public int marcaEnviado(@Param("id") BigInteger id);
	
	@Query( "SELECT factManu FROM Tmct0FacturasManuales factManu WHERE factManu.nbDocNumero = :nbDocNumero" )
	public Tmct0FacturasManuales findFacturaManual(@Param( "nbDocNumero" ) BigInteger nbDocNumero);

}
