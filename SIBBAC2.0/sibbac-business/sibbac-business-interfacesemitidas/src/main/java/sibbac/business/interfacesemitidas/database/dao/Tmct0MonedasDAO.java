package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0Monedas;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0MonedasDAO extends JpaRepository<Tmct0Monedas,Integer>{
	
	@Query("SELECT mon FROM Tmct0Monedas mon ORDER BY mon.nbCodigo ASC")
	public List<Tmct0Monedas> findAllOrderByNbCodigo();

}
