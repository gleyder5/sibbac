package sibbac.business.interfacesemitidas.dto;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 02
 * 
 * @author adrian.prause
 *
 */
public class ClaveRegimenDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private Integer claveRegimenEspecialOTrascendencia;

	@Override
	public String toString() {
		String claveRegimen = "";
		try {
			claveRegimen = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padCerosLeft(claveRegimenEspecialOTrascendencia, 2);
			return StringHelper.padRight(claveRegimen, 2449);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public Integer getClaveRegimenEspecialOTrascendencia() {
		return claveRegimenEspecialOTrascendencia;
	}

	public void setClaveRegimenEspecialOTrascendencia(Integer claveRegimenEspecialOTrascendencia) {
		this.claveRegimenEspecialOTrascendencia = claveRegimenEspecialOTrascendencia;
	}

}
