package sibbac.business.interfacesemitidas.dto.assembler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0CausaExencion;
import sibbac.business.interfacesemitidas.database.model.Tmct0ClaveRegimen;
import sibbac.business.interfacesemitidas.database.model.Tmct0CodigoImpuesto;
import sibbac.business.interfacesemitidas.database.model.Tmct0Exentas;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0Monedas;
import sibbac.business.interfacesemitidas.database.model.Tmct0Sujetas;
import sibbac.business.interfacesemitidas.database.model.Tmct0TipoFactura;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesTablaDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.helper.InterfacesEmitidasHelper;
import sibbac.business.wrappers.common.Constantes;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_FACTURA_MANUAL
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0FacturasManualesAssembler {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0FacturasManualesAssembler.class);

  @Autowired
  Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

  public Tmct0FacturasManualesAssembler() {
    super();
  }

  /**
   * Realiza la conversion entre los parámetros de la visual y la entidad
   * 
   * @param paramsObjects -> parámetros que manda la visual
   * @return Entidad con los datos de la visual tratados
   * @throws ParseException
   */
  public Tmct0FacturasManuales getTmct0FacturasManualesEntidad(Map<String, Object> paramsObjects, boolean esModificacion) throws ParseException {

    LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesEntidad");

    Tmct0FacturasManuales entidadFacturasManuales = new Tmct0FacturasManuales();
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Date date = new Date();

    try {

      // Si es una modificación seteamos el id
      if (esModificacion) {
        // entidadFacturasManuales.setId(new BigInteger(String.valueOf(paramsObjects.get("id"))));
        entidadFacturasManuales = tmct0FacturasManualesDao.findOne(new BigInteger(
                                                                                  String.valueOf(paramsObjects.get("id"))));
      }
      // Si es un alta seteamos las fechas
      if (!esModificacion) {
        entidadFacturasManuales.setFhFechaCre(dateFormat.parse(dateFormat.format(date)));
        entidadFacturasManuales.setFhFechaFac(dateFormat.parse(dateFormat.format(date)));
        entidadFacturasManuales.setFhInicio(dateFormat.parse(dateFormat.format(date)));
        entidadFacturasManuales.setEnviado(false);
      }
      entidadFacturasManuales.setAuditDate(dateFormat.parse(dateFormat.format(date)));
      entidadFacturasManuales.setAuditUser(String.valueOf(paramsObjects.get("auditUser")));
      entidadFacturasManuales.setIdAlias(new BigInteger(String.valueOf(paramsObjects.get("idAlias"))));
      //MFG la causa de exencion ha dejado de ser obligatoria  
      if ((paramsObjects.get("idCausaExencion") != null) && (StringUtils.isNotEmpty(String.valueOf(paramsObjects.get("idCausaExencion"))))){
          entidadFacturasManuales.setCausaExencion(new Tmct0CausaExencion());
          entidadFacturasManuales.getCausaExencion()
                                 .setId(Integer.parseInt(String.valueOf(paramsObjects.get("idCausaExencion"))));
    	  
      }
      
      entidadFacturasManuales.setClaveRegimen(new Tmct0ClaveRegimen());
      entidadFacturasManuales.getClaveRegimen()
                             .setId(Integer.parseInt(String.valueOf(paramsObjects.get("idClaveRegimen"))));
      entidadFacturasManuales.setCodImpuesto(new Tmct0CodigoImpuesto());
      entidadFacturasManuales.getCodImpuesto()
                             .setId(new BigInteger(String.valueOf(paramsObjects.get("idCodImpuesto"))));
      entidadFacturasManuales.setIdEntregaBien(new Short(String.valueOf(paramsObjects.get("idEntregaBien"))));
      entidadFacturasManuales.setMoneda(new Tmct0Monedas());
      entidadFacturasManuales.getMoneda().setId(new Integer(String.valueOf(paramsObjects.get("idMoneda"))));
      entidadFacturasManuales.setTipoFactura(new Tmct0TipoFactura());
      entidadFacturasManuales.getTipoFactura()
                             .setId(Integer.parseInt(String.valueOf(paramsObjects.get("idTipoFactura"))));
      entidadFacturasManuales.setImpBaseImponible(new BigDecimal(String.valueOf(paramsObjects.get("impBaseImponible"))));
      entidadFacturasManuales.setImpImpuesto(new BigDecimal(String.valueOf(paramsObjects.get("impImpuesto"))));
      if (paramsObjects.get("nbComentarios") != null) {
        entidadFacturasManuales.setNbComentarios(String.valueOf(paramsObjects.get("nbComentarios")));
      } else {
        entidadFacturasManuales.setNbComentarios(null);
      }
      entidadFacturasManuales.setNbDocNumero(new BigInteger(String.valueOf(paramsObjects.get("nbDocNumero"))));
      entidadFacturasManuales.setPeriodo(InterfacesEmitidasHelper.getPeriodo());
      entidadFacturasManuales.setImFinDiv(new BigDecimal(String.valueOf(paramsObjects.get("imFinDiv"))));
      entidadFacturasManuales.setImFinSvb(new BigDecimal(String.valueOf(paramsObjects.get("imFinSvb"))));

      String idExenta = paramsObjects.get("idExenta") != null ? String.valueOf(paramsObjects.get("idExenta")) : null;
      if (StringUtils.isNotBlank(idExenta)) {
        entidadFacturasManuales.setTmct0Exentas(new Tmct0Exentas());
        entidadFacturasManuales.getTmct0Exentas().setId(Integer.parseInt(String.valueOf(idExenta)));
      }

      String idSujeta = paramsObjects.get("idSujeta") != null ? String.valueOf(paramsObjects.get("idSujeta")) : null;
      if (StringUtils.isNotBlank(idSujeta)) {
        entidadFacturasManuales.setTmct0Sujetas(new Tmct0Sujetas());
        entidadFacturasManuales.getTmct0Sujetas().setId(Integer.parseInt(String.valueOf(idSujeta)));
      } else {
        entidadFacturasManuales.setTmct0Sujetas(null);
      }

      /** Inicio: Campos nullables solo rellenables para T. Factura Sujeta: NO EXENTA. */
      String baseImponCostes = paramsObjects.get("baseImponCostes") != null ? String.valueOf(paramsObjects.get("baseImponCostes"))
                                                                           : null;
      if (StringUtils.isNotBlank(baseImponCostes)) {
        entidadFacturasManuales.setBaseImponCostes(new BigDecimal(baseImponCostes));
      } else {
        entidadFacturasManuales.setBaseImponCostes(null);
      }

      String coefImpCostes = paramsObjects.get("coefImpCostes") != null ? String.valueOf(paramsObjects.get("coefImpCostes"))
                                                                       : null;
      if (StringUtils.isNotBlank(coefImpCostes)) {
        entidadFacturasManuales.setCoefImpCostes(new BigDecimal(coefImpCostes));
      } else {
        entidadFacturasManuales.setCoefImpCostes(null);
      }

      String cuotaRepercut = paramsObjects.get("cuotaRepercut") != null ? String.valueOf(paramsObjects.get("cuotaRepercut"))
                                                                       : null;
      if (StringUtils.isNotBlank(cuotaRepercut)) {
        entidadFacturasManuales.setCuotaRepercut(new BigDecimal(cuotaRepercut));
      } else {
        entidadFacturasManuales.setCuotaRepercut(null);
      }
      if (entidadFacturasManuales.getNbDocNumero().toString().startsWith("6") && StringUtils.isNotBlank(cuotaRepercut)) {
    	  entidadFacturasManuales.setImpImpuesto(entidadFacturasManuales.getCuotaRepercut());
      }
      /** Fin: Campos nullables solo rellenables para T. Factura Sujeta: NO EXENTA. */
    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FacturasManualesEntidad -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesEntidad");

    return entidadFacturasManuales;

  }

  /**
   * Realiza la conversion entre la entidad y el dto
   * 
   * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0FacturasManualesTablaDTO getTmct0FacturasManualesTablaDTO(Tmct0FacturasManuales datosFacturasManuales,
                                                                        String descrAli) {

    LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

    Tmct0FacturasManualesTablaDTO facturasManualesTablaDTO = new Tmct0FacturasManualesTablaDTO();

    try {

      facturasManualesTablaDTO.setId(datosFacturasManuales.getId());
      facturasManualesTablaDTO.setFhFechaCre(datosFacturasManuales.getFhFechaCre());
      facturasManualesTablaDTO.setFhFechaFac(datosFacturasManuales.getFhFechaFac());
      facturasManualesTablaDTO.setEmpContraparte(descrAli);
      facturasManualesTablaDTO.setEstado(datosFacturasManuales.getEstado().getNombre());
      facturasManualesTablaDTO.setImpBaseImponible(datosFacturasManuales.getImpBaseImponible());
      facturasManualesTablaDTO.setImpImpuesto(datosFacturasManuales.getImpImpuesto());
      facturasManualesTablaDTO.setNbDocNumero(datosFacturasManuales.getNbDocNumero());
      facturasManualesTablaDTO.setCoefImpCostes(datosFacturasManuales.getCoefImpCostes());
      facturasManualesTablaDTO.setBaseImponCostes(datosFacturasManuales.getBaseImponCostes());
      facturasManualesTablaDTO.setCuotaRepercut(datosFacturasManuales.getCuotaRepercut());
      facturasManualesTablaDTO.setImFinSvb(datosFacturasManuales.getImFinSvb());
      facturasManualesTablaDTO.setImFinDiv(datosFacturasManuales.getImFinDiv());
      facturasManualesTablaDTO.setNbComentarios(datosFacturasManuales.getNbComentarios());
      if (datosFacturasManuales.getEstado().getNombre().equals(Constantes.ESTADO_ANULADA)
          || datosFacturasManuales.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)
          || datosFacturasManuales.getEstado().getNombre().equals(Constantes.ESTADO_PDTE_CONTABILIZAR)) {
        facturasManualesTablaDTO.setEsModificable(false);
      } else {
        facturasManualesTablaDTO.setEsModificable(true);
      }

      if (datosFacturasManuales.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)) {
        facturasManualesTablaDTO.setEsAnulable(true);
      } else {
        facturasManualesTablaDTO.setEsAnulable(false);
      }

    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FacturasManualesTablaDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

    return facturasManualesTablaDTO;

  }

  /**
   * Realiza la conversion entre la entidad y el dto
   * 
   * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0FacturasManualesTablaDTO getTmct0FacturasManualesTablaDTO(Tmct0BajasManuales datosFacturasManualesBaja,
                                                                        String descrAli) {

    LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

    Tmct0FacturasManualesTablaDTO facturasManualesTablaDTO = new Tmct0FacturasManualesTablaDTO();

    try {

      facturasManualesTablaDTO.setId(datosFacturasManualesBaja.getId());
      facturasManualesTablaDTO.setFhFechaCre(datosFacturasManualesBaja.getFhFechaCre());
      facturasManualesTablaDTO.setFhFechaFac(datosFacturasManualesBaja.getFhFechaFac());
      facturasManualesTablaDTO.setEmpContraparte(descrAli);
      facturasManualesTablaDTO.setEstado(datosFacturasManualesBaja.getEstado().getNombre());
      facturasManualesTablaDTO.setImpBaseImponible(datosFacturasManualesBaja.getImpBaseImponible());
      facturasManualesTablaDTO.setImpImpuesto(datosFacturasManualesBaja.getImpImpuesto());
      facturasManualesTablaDTO.setNbDocNumero(datosFacturasManualesBaja.getNbDocNumero());
      facturasManualesTablaDTO.setCoefImpCostes(datosFacturasManualesBaja.getCoefImpCostes());
      facturasManualesTablaDTO.setBaseImponCostes(datosFacturasManualesBaja.getBaseImponCostes());
      facturasManualesTablaDTO.setCuotaRepercut(datosFacturasManualesBaja.getCuotaRepercut());
      facturasManualesTablaDTO.setImFinSvb(datosFacturasManualesBaja.getImFinSvb());
      facturasManualesTablaDTO.setImFinDiv(datosFacturasManualesBaja.getImFinDiv());
      facturasManualesTablaDTO.setNbComentarios(datosFacturasManualesBaja.getNbComentarios());
      if (datosFacturasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_ANULADA)
          || datosFacturasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)
          || datosFacturasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_PDTE_CONTABILIZAR)) {
        facturasManualesTablaDTO.setEsModificable(false);
      } else {
        facturasManualesTablaDTO.setEsModificable(true);
      }

      if (datosFacturasManualesBaja.getEstado().getNombre().equals(Constantes.ESTADO_CONTABILIZADA)) {
        facturasManualesTablaDTO.setEsAnulable(true);
      } else {
        facturasManualesTablaDTO.setEsAnulable(false);
      }

    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FacturasManualesTablaDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesTablaDTO");

    return facturasManualesTablaDTO;

  }

  /**
   * Realiza la conversion entre la entidad y el dto Tmct0FacturasManualesDTO
   * 
   * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0FacturasManualesDTO getTmct0FacturasManualesDTO(Tmct0FacturasManuales datosFacturasManuales) {

    LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesDTO");

    Tmct0FacturasManualesDTO facturasManualesDTO = new Tmct0FacturasManualesDTO();

    try {

      facturasManualesDTO.setFhFechaCre(datosFacturasManuales.getFhFechaCre());
      facturasManualesDTO.setFhFechaFac(datosFacturasManuales.getFhFechaFac());
      facturasManualesDTO.setIdAlias(datosFacturasManuales.getIdAlias());
      facturasManualesDTO.setIdEstado(datosFacturasManuales.getEstado().getIdestado());
      facturasManualesDTO.setImpBaseImponible(datosFacturasManuales.getImpBaseImponible());
      facturasManualesDTO.setImpImpuesto(datosFacturasManuales.getImpImpuesto());
      facturasManualesDTO.setNbDocNumero(datosFacturasManuales.getNbDocNumero());
      facturasManualesDTO.setCoefImpCostes(datosFacturasManuales.getCoefImpCostes());
      facturasManualesDTO.setBaseImponCostes(datosFacturasManuales.getBaseImponCostes());
      facturasManualesDTO.setCuotaRepercut(datosFacturasManuales.getCuotaRepercut());

    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FacturasManualesDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesDTO");

    return facturasManualesDTO;

  }

  /**
   * Realiza la conversion entre la entidad y el ClientesDTO a Tmct0FacturasManualesDTO
   * 
   * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
   * @param clienteDTO -> DTO para añadir al objeto cliente del Tmct0FacturasManualesDTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0FacturasManualesDTO getTmct0FacturasManualesDetalleDTO(Tmct0FacturasManuales datosFacturasManuales/*
                                                                                                                 * ,
                                                                                                                 * ClienteDTO
                                                                                                                 * cliente
                                                                                                                 */) {

    LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesDetalleDTO");

    Tmct0FacturasManualesDTO facturasManualesDTO = new Tmct0FacturasManualesDTO();

    try {

      facturasManualesDTO.setAuditDate(datosFacturasManuales.getAuditDate());
      facturasManualesDTO.setAuditUser(datosFacturasManuales.getAuditUser());
      facturasManualesDTO.setBaseImponCostes(datosFacturasManuales.getBaseImponCostes());
      facturasManualesDTO.setCoefImpCostes(datosFacturasManuales.getCoefImpCostes());
      facturasManualesDTO.setCuotaRepercut(datosFacturasManuales.getCuotaRepercut());
      facturasManualesDTO.setFhFechaCre(datosFacturasManuales.getFhFechaCre());
      facturasManualesDTO.setFhFechaFac(datosFacturasManuales.getFhFechaFac());
      facturasManualesDTO.setFhInicio(datosFacturasManuales.getFhInicio());
      facturasManualesDTO.setId(datosFacturasManuales.getId());
      facturasManualesDTO.setIdAlias(datosFacturasManuales.getIdAlias());
      if (datosFacturasManuales.getCausaExencion() != null) {
          facturasManualesDTO.setIdCausaExencion(datosFacturasManuales.getCausaExencion().getId());
        }
      facturasManualesDTO.setIdClaveRegimen(datosFacturasManuales.getClaveRegimen().getId());
      facturasManualesDTO.setIdCodImpuesto(new Integer(String.valueOf(datosFacturasManuales.getCodImpuesto().getId())));
      facturasManualesDTO.setIdEntregaBien(datosFacturasManuales.getIdEntregaBien());
      facturasManualesDTO.setIdEstado(datosFacturasManuales.getEstado().getIdestado());
      facturasManualesDTO.setIdMoneda(datosFacturasManuales.getMoneda().getId());
      facturasManualesDTO.setIdTipoFactura(datosFacturasManuales.getTipoFactura().getId());
      facturasManualesDTO.setImFinDiv(datosFacturasManuales.getImFinDiv());
      facturasManualesDTO.setImFinSvb(datosFacturasManuales.getImFinSvb());
      facturasManualesDTO.setImpBaseImponible(datosFacturasManuales.getImpBaseImponible());
      facturasManualesDTO.setImpImpuesto(datosFacturasManuales.getImpImpuesto());
      facturasManualesDTO.setNbComentarios(datosFacturasManuales.getNbComentarios());
      facturasManualesDTO.setNbDocNumero(datosFacturasManuales.getNbDocNumero());
      facturasManualesDTO.setPeriodo(datosFacturasManuales.getPeriodo());

      if (datosFacturasManuales.getTmct0Exentas() != null) {
        facturasManualesDTO.setIdExenta(String.valueOf(datosFacturasManuales.getTmct0Exentas().getId()));
      }
      if (datosFacturasManuales.getTmct0Sujetas() != null) {
        facturasManualesDTO.setIdSujeta(String.valueOf(datosFacturasManuales.getTmct0Sujetas().getId()));
      }
    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FacturasManualesDetalleDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesDetalleDTO");

    return facturasManualesDTO;

  }

  /**
   * Realiza la conversion entre la entidad y el ClientesDTO a Tmct0FacturasManualesDTO
   * 
   * @param datosFacturasManuales -> Entidad que se quiere trasformar en DTO
   * @param clienteDTO -> DTO para añadir al objeto cliente del Tmct0FacturasManualesDTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0FacturasManualesDTO getTmct0FacturasManualesBajaDetalleDTO(Tmct0BajasManuales datosFacturasManuales/*
                                                                                                                  * ,
                                                                                                                  * ClienteDTO
                                                                                                                  * cliente
                                                                                                                  */) {

    LOG.info("INICIO - ASSEMBLER - getTmct0FacturasManualesDetalleDTO");

    Tmct0FacturasManualesDTO facturasManualesDTO = new Tmct0FacturasManualesDTO();

    try {

      facturasManualesDTO.setAuditDate(datosFacturasManuales.getAuditDate());
      facturasManualesDTO.setAuditUser(datosFacturasManuales.getAuditUser());
      facturasManualesDTO.setBaseImponCostes(datosFacturasManuales.getBaseImponCostes());
      facturasManualesDTO.setCoefImpCostes(datosFacturasManuales.getCoefImpCostes());
      facturasManualesDTO.setCuotaRepercut(datosFacturasManuales.getCuotaRepercut());
      facturasManualesDTO.setFhFechaCre(datosFacturasManuales.getFhFechaCre());
      facturasManualesDTO.setFhFechaFac(datosFacturasManuales.getFhFechaFac());
      facturasManualesDTO.setFhInicio(datosFacturasManuales.getFhInicio());
      facturasManualesDTO.setId(datosFacturasManuales.getId());
      facturasManualesDTO.setIdAlias(datosFacturasManuales.getIdAlias());
      if( datosFacturasManuales.getCausaExencion() != null){
    	  facturasManualesDTO.setIdCausaExencion(datosFacturasManuales.getCausaExencion().getId());  
      }
      facturasManualesDTO.setIdClaveRegimen(datosFacturasManuales.getClaveRegimen().getId());
      facturasManualesDTO.setIdCodImpuesto(new Integer(String.valueOf(datosFacturasManuales.getCodImpuesto().getId())));
      facturasManualesDTO.setIdEntregaBien(datosFacturasManuales.getIdEntregaBien());
      facturasManualesDTO.setIdEstado(datosFacturasManuales.getEstado().getIdestado());
      facturasManualesDTO.setIdMoneda(datosFacturasManuales.getMoneda().getId());
      facturasManualesDTO.setIdTipoFactura(datosFacturasManuales.getTipoFactura().getId());
      facturasManualesDTO.setImFinDiv(datosFacturasManuales.getImFinDiv());
      facturasManualesDTO.setImFinSvb(datosFacturasManuales.getImFinSvb());
      facturasManualesDTO.setImpBaseImponible(datosFacturasManuales.getImpBaseImponible());
      facturasManualesDTO.setImpImpuesto(datosFacturasManuales.getImpImpuesto());
      facturasManualesDTO.setNbComentarios(datosFacturasManuales.getNbComentarios());
      facturasManualesDTO.setNbDocNumero(datosFacturasManuales.getNbDocNumero());
      facturasManualesDTO.setPeriodo(datosFacturasManuales.getPeriodo());

      if (datosFacturasManuales.getTmct0Exentas() != null) {
        facturasManualesDTO.setIdExenta(String.valueOf(datosFacturasManuales.getTmct0Exentas().getId()));
      }
      if (datosFacturasManuales.getTmct0Sujetas() != null) {
        facturasManualesDTO.setIdSujeta(String.valueOf(datosFacturasManuales.getTmct0Sujetas().getId()));
      }
    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FacturasManualesDetalleDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FacturasManualesDetalleDTO");

    return facturasManualesDTO;

  }

  /**
   * Realiza la conversion entre los parámetros de la visual y un objeto filtroDTO
   * 
   * @param paramsObjects -> parámetros que manda la visual
   * @return Entidad con los datos de la visual tratados
   * @throws ParseException
   */
  public Tmct0FiltroFacturasManualesDTO getTmct0FiltroFacturasManualesDTO(Map<String, Object> paramsObjects) throws ParseException {

    LOG.info("INICIO - ASSEMBLER - getTmct0FiltroFacturasManualesDTO");

    Tmct0FiltroFacturasManualesDTO filtroFacturasManuales = new Tmct0FiltroFacturasManualesDTO();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    try {

      if ((paramsObjects.get("fechaDesde") != null) && (!paramsObjects.get("fechaDesde").equals(""))) {
        filtroFacturasManuales.setFechaDesde(formatter.parse(String.valueOf(paramsObjects.get("fechaDesde"))));
      }
      if ((paramsObjects.get("fechaHasta") != null) && (!paramsObjects.get("fechaHasta").equals(""))) {
        filtroFacturasManuales.setFechaHasta(formatter.parse(String.valueOf(paramsObjects.get("fechaHasta"))));
      }
      if ((paramsObjects.get("idAlias") != null) && (!paramsObjects.get("idAlias").equals(""))) {
        filtroFacturasManuales.setIdAlias(new BigInteger(String.valueOf(paramsObjects.get("idAlias"))));
      }
      if ((paramsObjects.get("idEstado") != null) && (!paramsObjects.get("idEstado").equals(""))) {
        filtroFacturasManuales.setIdEstado(Integer.parseInt(String.valueOf(paramsObjects.get("idEstado"))));
      }
      if ((paramsObjects.get("nbDocNumero") != null) && (!paramsObjects.get("nbDocNumero").equals(""))) {
        filtroFacturasManuales.setNbDocNumero(new BigInteger(String.valueOf(paramsObjects.get("nbDocNumero"))));
      }

    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FiltroFacturasManualesDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FiltroFacturasManualesDTO");

    return filtroFacturasManuales;

  }

  /**
   * Realiza la conversion entre los parámetros de la visual y un objeto filtroDTO
   * 
   * @param paramsObjects -> parámetros que manda la visual
   * @return Entidad con los datos de la visual tratados
   * @throws ParseException
   */
  public Tmct0FiltroFacturasManualesDTO getTmct0FiltroBajasManualesDTO(Map<String, Object> paramsObjects) throws ParseException {

    LOG.info("INICIO - ASSEMBLER - getTmct0FiltroBajasManualesDTO");

    Tmct0FiltroFacturasManualesDTO filtroFacturasManuales = new Tmct0FiltroFacturasManualesDTO();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    try {

      if (!paramsObjects.get("fechaDesde").equals("")) {
        filtroFacturasManuales.setFechaDesde(formatter.parse(String.valueOf(paramsObjects.get("fechaDesde"))));
      }
      if (!paramsObjects.get("fechaHasta").equals("")) {
        filtroFacturasManuales.setFechaHasta(formatter.parse(String.valueOf(paramsObjects.get("fechaHasta"))));
      }
      if (!paramsObjects.get("idAlias").equals("")) {
        filtroFacturasManuales.setIdAlias(new BigInteger(String.valueOf(paramsObjects.get("idAlias"))));
      }
      if (!paramsObjects.get("nbDocNumero").equals("")) {
        filtroFacturasManuales.setNbDocNumero(new BigInteger(String.valueOf(paramsObjects.get("nbDocNumero"))));
      }

    } catch (Exception e) {
      LOG.error("Error metodo getTmct0FiltroBajasManualesDTO -  " + e.getMessage(), e);
      throw (e);
    }

    LOG.info("FIN - ASSEMBLER - getTmct0FiltroBajasManualesDTO");

    return filtroFacturasManuales;

  }
}
