package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0Exentas;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ExentasDAO extends JpaRepository<Tmct0Exentas,Integer>{
	
	@Query("SELECT exe FROM Tmct0Exentas exe ORDER BY exe.id ASC")
	public List<Tmct0Exentas> findAllOrderByClave();

}
