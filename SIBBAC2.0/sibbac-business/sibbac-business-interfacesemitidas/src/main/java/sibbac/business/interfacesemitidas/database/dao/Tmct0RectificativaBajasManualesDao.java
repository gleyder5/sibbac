package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0RectificativaBajasManualesDao extends JpaRepository<Tmct0RectificativaBajasManuales, BigInteger> {
	
	@Modifying @Query("UPDATE Tmct0RectificativaBajasManuales SET enviado = true WHERE id = :id ")
	public int marcaEnviado(@Param("id") BigInteger id);
	
	@Query("SELECT f FROM Tmct0RectificativaBajasManuales f WHERE (f.enviado is null or f.enviado = false)")
	public List<Tmct0RectificativaBajasManuales> findNoEnviadas();

}
