package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0CausaExencion;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0CausasExencionDAO extends JpaRepository<Tmct0CausaExencion,Integer>{
	
	@Query("SELECT cauExe FROM Tmct0CausaExencion cauExe ORDER BY cauExe.clave ASC")
	public List<Tmct0CausaExencion> findAllOrderByClave();

}
