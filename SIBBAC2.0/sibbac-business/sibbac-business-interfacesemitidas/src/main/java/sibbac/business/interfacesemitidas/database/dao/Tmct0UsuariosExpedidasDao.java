package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sibbac.business.interfacesemitidas.database.model.Tmct0UsuariosExpedidas;

/**
 * Data access object de Tmct0UsuariosExpedidasDao.
 */
@Repository
public interface Tmct0UsuariosExpedidasDao extends JpaRepository<Tmct0UsuariosExpedidas, Long> {
	
	@Query("SELECT uEx.email FROM Tmct0UsuariosExpedidas uEx WHERE uEx.fhBaja IS NULL ORDER BY uEx.email asc")
	public List<String> findAllEmailActivosOrderByEmail();
	
}
