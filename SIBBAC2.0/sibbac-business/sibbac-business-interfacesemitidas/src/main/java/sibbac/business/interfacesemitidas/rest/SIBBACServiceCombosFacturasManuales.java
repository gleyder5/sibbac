package sibbac.business.interfacesemitidas.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.interfacesemitidas.database.dao.Tmct0CausasExencionDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0ClaveRegimenDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0CodigosImpuestosDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0ExentasDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAOImpl;
import sibbac.business.interfacesemitidas.database.dao.Tmct0InterfacesEmitidasConstDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0MonedasDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0SujetasDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0TiposFacturaDAO;
import sibbac.business.interfacesemitidas.database.model.Tmct0CausaExencion;
import sibbac.business.interfacesemitidas.database.model.Tmct0ClaveRegimen;
import sibbac.business.interfacesemitidas.database.model.Tmct0CodigoImpuesto;
import sibbac.business.interfacesemitidas.database.model.Tmct0Exentas;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0InterfacesEmitidasConst;
import sibbac.business.interfacesemitidas.database.model.Tmct0Monedas;
import sibbac.business.interfacesemitidas.database.model.Tmct0Sujetas;
import sibbac.business.interfacesemitidas.database.model.Tmct0TiposFactura;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceCombosFacturasManuales implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceCombosFacturasManuales.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0CausasExencionDAO tmct0CausasExencionDao;

  @Autowired
  Tmct0ClaveRegimenDAO tmct0ClaveRegimenDao;

  @Autowired
  Tmct0CodigosImpuestosDAO tmct0CodigosImpuestosDao;

  @Autowired
  Tmct0ExentasDAO tmct0ExentasDao;

  @Autowired
  Tmct0SujetasDAO tmct0SujetasDao;

  @Autowired
  Tmct0MonedasDAO tmct0MonedasDao;

  @Autowired
  Tmct0TiposFacturaDAO tmct0TiposFacturaDao;

  @Autowired
  Tmct0InterfacesEmitidasConstDao tmct0InterfacesEmitidasConstDao;

  @Autowired
  Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

  @Autowired
  Tmct0FacturasManualesDAOImpl tmct0FacturasManualesDaoImpl;

  @Autowired
  private Tmct0aliDao aliDao;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private Tmct0InterfacesEmitidasConstDao interfacesEmitidasConstDao;

  /**
   * The Enum ComandoCombosFacturasManuales.
   */
  private enum ComandoCombosFacturasManuales {

    /** Consultar el listado de causas de exención */
    CONSULTAR_CAUSAS_EXENCION("consultarCausasExencion"),

    /** Consultar el listado de clave regimen */
    CONSULTAR_CLAVE_REGIMEN("consultarClaveRegimen"),

    /** Consultar el listado de códigos de impuestos */
    CONSULTAR_CODIGOS_IMPUESTOS("consultarCodigosImpuestos"),

    /** Consultar el listado de causas de exención */
    CONSULTAR_MONEDAS("consultarMonedas"),

    /** Consultar el listado de tipos de factura */
    CONSULTAR_TIPOS_FACTURA("consultarTiposFactura"),

    /** Consultar el listado de entrega de un bien */
    CONSULTAR_ENTREGA_BIEN("consultarEntregaBien"),

    /** Consultar el listado de alias */
    CONSULTAR_ALIAS("consultarAlias"),

    /** Consultar los estados de las facturas manuales */
    CONSULTAR_ESTADOS("consultarEstados"),

    /** Consultar exentas */
    CONSULTAR_EXENTAS("consultarExentas"),

    /** Consultar sujetas */
    CONSULTAR_SUJETAS("consultarSujetas"),

    /** Consultar facturas */
    CONSULTAR_FACTURAS_RECTIFICADAS("consultarFacturasRectificadas"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandosCombosFacturasManuales.
     *
     * @param command the command
     */
    private ComandoCombosFacturasManuales(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandoCombosFacturasManuales command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceCombosFacturasManuales");

      switch (command) {
        case CONSULTAR_CAUSAS_EXENCION:
          result.setResultados(consultarCausasExencion(paramsObjects));
          break;
        case CONSULTAR_CLAVE_REGIMEN:
          result.setResultados(consultarClaveRegimen(paramsObjects));
          break;
        case CONSULTAR_CODIGOS_IMPUESTOS:
          result.setResultados(consultarCodigosImpuestos(paramsObjects));
          break;
        case CONSULTAR_MONEDAS:
          result.setResultados(consultarMonedas(paramsObjects));
          break;
        case CONSULTAR_TIPOS_FACTURA:
          result.setResultados(consultarTiposFactura(paramsObjects));
          break;
        case CONSULTAR_ENTREGA_BIEN:
          result.setResultados(consultarEntregaBien(paramsObjects));
          break;
        case CONSULTAR_ALIAS:
          result.setResultados(consultarAlias());
          break;
        case CONSULTAR_ESTADOS:
          result.setResultados(consultarEstadosDistintos());
          break;
        case CONSULTAR_EXENTAS:
          result.setResultados(consultarExentas());
          break;
        case CONSULTAR_SUJETAS:
          result.setResultados(consultarSujetas());
          break;
        case CONSULTAR_FACTURAS_RECTIFICADAS:
          result.setResultados(consultarFacturasRectificadas());
          break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceCombosFacturasManuales");
    return result;
  }

  /**
   * Consulta de las causas de exención
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarCausasExencion(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarCausasExencion");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaCausasExencion = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de causas de exención
      List<Tmct0CausaExencion> lisCausasExencion = tmct0CausasExencionDao.findAllOrderByClave();

      // Se transforman a lista de DTO
      for (Tmct0CausaExencion tmct0CausasExencion : lisCausasExencion) {
        try {
          SelectDTO causaExencionDTO = new SelectDTO(tmct0CausasExencion.getId().toString(),
                                                     tmct0CausasExencion.getClave().toUpperCase() + " - "
                                                         + tmct0CausasExencion.getValor().toUpperCase());
          listaCausasExencion.add(causaExencionDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarCausasExencion - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de causas de exención a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarCausasExencion -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarCausasExencion -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaCausasExencion", listaCausasExencion);

    LOG.info("FIN - SERVICIO - consultarCausasExencion");

    return result;
  }

  /**
   * Consulta de las claves regimen
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarClaveRegimen(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarClaveRegimen");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaClavesRegimen = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de claves regimen
      List<Tmct0ClaveRegimen> lisClavesRegimen = tmct0ClaveRegimenDao.findAllOrderByClave();

      // Se transforman a lista de DTO
      for (Tmct0ClaveRegimen tmct0ClaveRegimen : lisClavesRegimen) {
        try {
          SelectDTO claveRegimenDTO = new SelectDTO(tmct0ClaveRegimen.getId().toString(),
                                                    tmct0ClaveRegimen.getValor().toUpperCase());
          listaClavesRegimen.add(claveRegimenDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarClaveRegimen - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de las claves regimen a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarClaveRegimen -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarClaveRegimen -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaClaveRegimen", listaClavesRegimen);

    LOG.info("FIN - SERVICIO - consultarClaveRegimen");

    return result;
  }

  /**
   * Consulta de los codigos impuestos
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarCodigosImpuestos(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarCodigosImpuestos");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaCodigosImpuestos = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de facturas manuales
      List<Tmct0CodigoImpuesto> lisCodigosImpuestos = tmct0CodigosImpuestosDao.findAllOrderByIdImpuesto();

      // Se transforman a lista de DTO
      for (Tmct0CodigoImpuesto tmct0CodigosImpuestos : lisCodigosImpuestos) {
        try {
          SelectDTO codigoImpuestoDTO = new SelectDTO(
                                                      tmct0CodigosImpuestos.getId().toString(),
                                                      (tmct0CodigosImpuestos.getIdImpuesto() + " / "
                                                       + tmct0CodigosImpuestos.getSubImpuesto() + " " + tmct0CodigosImpuestos.getDescCortaSub()
                                                                                                                             .toUpperCase()));
          listaCodigosImpuestos.add(codigoImpuestoDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarCodigosImpuestos - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de codigos de impuestos a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarCodigosImpuestos -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarCodigosImpuestos -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaCodigosImpuestos", listaCodigosImpuestos);

    LOG.info("FIN - SERVICIO - consultarCodigosImpuestos");

    return result;
  }

  /**
   * Consulta de las monedas
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarMonedas(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarMonedas");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaMonedas = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de monedas
      List<Tmct0Monedas> lisMonedas = tmct0MonedasDao.findAllOrderByNbCodigo();

      // Se transforman a lista de DTO
      for (Tmct0Monedas tmct0Monedas : lisMonedas) {
        try {
          SelectDTO monedasDTO = new SelectDTO(tmct0Monedas.getId().toString(), tmct0Monedas.getNbCodigo()
                                                                                            .toUpperCase()
                                                                                + " - "
                                                                                + tmct0Monedas.getNbDescripcion()
                                                                                              .toUpperCase());
          listaMonedas.add(monedasDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarMonedas - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de monedas a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarMonedas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarMonedas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaMonedas", listaMonedas);

    LOG.info("FIN - SERVICIO - consultarMonedas");

    return result;
  }

  /**
   * Consulta de los tipos de factura
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarTiposFactura(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarTiposFactura");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaTiposFacturas = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de tipos de factura
      List<Tmct0TiposFactura> lisTiposFacturas = tmct0TiposFacturaDao.findAllOrderByClave();

      // Se transforman a lista de DTO
      for (Tmct0TiposFactura tmct0TiposFacturas : lisTiposFacturas) {
        try {
          SelectDTO tipoFacturasDTO = new SelectDTO(tmct0TiposFacturas.getId().toString(),
                                                    tmct0TiposFacturas.getClave().toUpperCase() + " - "
                                                        + tmct0TiposFacturas.getValor().toUpperCase());
          listaTiposFacturas.add(tipoFacturasDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarTiposFacturas - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de los tipos de factura a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarTiposFactura -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarTiposFactura -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaTiposFacturas", listaTiposFacturas);

    LOG.info("FIN - SERVICIO - consultarTiposFacturas");

    return result;
  }

  /**
   * Consulta entrega bien
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarEntregaBien(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarEntregaBien");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaEntregaBien = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de entrega bien
      List<Tmct0InterfacesEmitidasConst> lisEntregaBien = tmct0InterfacesEmitidasConstDao.findByGrupo(Constantes.GRUPO_ENTREGABIEN);

      // Se transforman a lista de DTO
      for (Tmct0InterfacesEmitidasConst tmct0EntregaBien : lisEntregaBien) {
        try {
          SelectDTO entregaBienDTO = new SelectDTO(tmct0EntregaBien.getClave(), tmct0EntregaBien.getValor()
                                                                                                .toUpperCase());
          listaEntregaBien.add(entregaBienDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarEntregaBien - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de los tipos de factura a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarEntregaBien -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarEntregaBien -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaEntregaBien", listaEntregaBien);

    LOG.info("FIN - SERVICIO - consultarEntregaBien");

    return result;
  }

  /**
   * Carga inicial de los datos estaticos de la pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> consultarAlias() throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarAlias");

    Map<String, Object> result = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaAlias = new ArrayList<SelectDTO>();
      SelectDTO select = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Calendar cal = Calendar.getInstance();
      Integer fechaActivo = Integer.parseInt(sdf.format(cal.getTime()));
      List<Tmct0aliDTO> listAlias = aliDao.findAllAli(fechaActivo);

      for (Tmct0aliDTO tmct0aliDTO : listAlias) {
        select = new SelectDTO(tmct0aliDTO.getAlias(), tmct0aliDTO.getAlias().toUpperCase() + " -  "
                                                       + tmct0aliDTO.getDescripcion().toUpperCase());
        listaAlias.add(select);
      }
      result.put("listaAlias", listaAlias);

    } catch (Exception e) {
      LOG.error("Error metodo consultarAlias -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - consultarEntregaBien");

    return result;
  }

  /**
   * Consulta de estados distintos
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarEstadosDistintos() throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarEstadosDistintos");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaEstados = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de estados distintos que existen en la tabla TMCT0_FACTURA_MANUAL
      List<Object> lisEstados = tmct0FacturasManualesDaoImpl.consultarEstadosDistintos();

      // Se transforman a lista de DTO
      for (Object tmct0Estados : lisEstados) {
        try {
          SelectDTO estadoDTO = new SelectDTO(((Object[]) tmct0Estados)[0].toString(),
                                              ((Object[]) tmct0Estados)[1].toString().toUpperCase());
          listaEstados.add(estadoDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarEstadosDistintos - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de estados a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarEstadosDistintos -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarEstadosDistintos -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaEstados", listaEstados);

    LOG.info("FIN - SERVICIO - consultarEstadosDistintos");

    return result;
  }

  /**
   * Consulta de exentas
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarExentas() throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarExentas");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaExentas = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de facturas manuales
      List<Tmct0Exentas> lisExentas = tmct0ExentasDao.findAllOrderByClave();

      // Se transforman a lista de DTO
      for (Tmct0Exentas tmct0Exentas : lisExentas) {
        try {
          SelectDTO exentaDTO = new SelectDTO(tmct0Exentas.getId().toString(), tmct0Exentas.getValor().toUpperCase());
          listaExentas.add(exentaDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarExentas - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de codigos de impuestos a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarExentas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarExentas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaExentas", listaExentas);

    LOG.info("FIN - SERVICIO - consultarExentas");

    return result;
  }

  /**
   * Consulta de sujetas
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarSujetas() throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - consultarSujetas");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaSujetas = new ArrayList<Object>();

    try {

      // Realizamos la llamada a la consulta de facturas manuales
      List<Tmct0Sujetas> lisSujetas = tmct0SujetasDao.findAllOrderByClave();

      // Se transforman a lista de DTO
      for (Tmct0Sujetas tmct0Sujetas : lisSujetas) {
        try {
          SelectDTO sujetaDTO = new SelectDTO(tmct0Sujetas.getId().toString(), tmct0Sujetas.getValor().toUpperCase());
          listaSujetas.add(sujetaDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarSujetas - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de codigos de impuestos a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarSujetas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarSujetas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaSujetas", listaSujetas);

    LOG.info("FIN - SERVICIO - consultarSujetas");

    return result;
  }

  /**
   * Consulta todas las facturas para mostrarlas en un combo
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarFacturasRectificadas() throws SIBBACBusinessException, ParseException {

    LOG.info("INICIO - SERVICIO - consultarFacturas");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaFacturas = new ArrayList<Object>();

    try {

      // Recuperamos el ID ESTADO que pertenece al literal FACTURA CONTABILIZADA
      Tmct0estado estadoFAct = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_CONTABILIZADA)
                                                                                .getValor());

      // Realizamos la llamada a la consulta de facturas manuales
      List<Tmct0FacturasManuales> lisFacturas = tmct0FacturasManualesDao.findAllOrderById(estadoFAct.getIdestado());

      // Se transforman a lista de DTO
      for (Tmct0FacturasManuales tmct0Facturas : lisFacturas) {
        try {
          SelectDTO sujetaDTO = new SelectDTO(tmct0Facturas.getId().toString(), tmct0Facturas.getNbDocNumero().toString(), tmct0Facturas.getImpBaseImponible().toString());
          listaFacturas.add(sujetaDTO);
        } catch (Exception e) {
          LOG.error("Error metodo consultarFacturas - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de codigos de impuestos a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      LOG.error("Error metodo consultarFacturas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    } catch (Exception e) {
      LOG.error("Error metodo consultarFacturas -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaFacturasRectificadas", listaFacturas);

    LOG.info("FIN - SERVICIO - consultarFacturas");

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandoCombosFacturasManuales getCommand(String command) {
    ComandoCombosFacturasManuales[] commands = ComandoCombosFacturasManuales.values();
    ComandoCombosFacturasManuales result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandoCombosFacturasManuales.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    return null;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

}
