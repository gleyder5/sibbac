package sibbac.business.interfacesemitidas.dto;

import java.text.ParseException;
import sibbac.business.wrappers.common.StringHelper;

/**
 *	Bloque 02 - ERRORES: necesario en la respuesta.
 */
public class ErroresDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private String tipoError;
	private String claseError;
	private String atributoError;
	private String codigoError;
	private String descripcionError;

	@Override
	public String toString() {
		try {
			return StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padRight(tipoError, 2) + StringHelper.padRight(claseError, 2)
					+ StringHelper.padRight(atributoError, 10) + StringHelper.padRight(codigoError, 5)
					+ StringHelper.padRight(descripcionError, 200);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return this.tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return this.secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public String getTipoError() {
		return this.tipoError;
	}

	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}

	public String getClaseError() {
		return this.claseError;
	}

	public void setClaseError(String claseError) {
		this.claseError = claseError;
	}

	public String getAtributoError() {
		return this.atributoError;
	}

	public void setAtributoError(String atributoError) {
		this.atributoError = atributoError;
	}

	public String getCodigoError() {
		return this.codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getDescripcionError() {
		return this.descripcionError;
	}

	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	
	/**
	 *	Popula DTO Errores segun la fila con datos.
	 *	@param line
	 * 	@throws ParseException 
	 */
	public void popularErroresDTO(String line) throws ParseException {
		this.tipoRegistro = line.substring(0, 2);
		this.secuencialRegistro = Integer.valueOf(line.substring(2, 9));
		this.tipoError = line.substring(9, 11);
		this.claseError = line.substring(11, 13);
		this.atributoError = line.substring(13, 23);
		this.codigoError = line.substring(23, 28);
		this.descripcionError = line.substring(28, 228);
	}

}
