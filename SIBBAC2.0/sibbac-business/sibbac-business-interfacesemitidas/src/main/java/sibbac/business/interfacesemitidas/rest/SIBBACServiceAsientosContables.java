package sibbac.business.interfacesemitidas.rest;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.interfacesemitidas.bo.Tmct0FacturasManualesBo;
import sibbac.business.interfacesemitidas.dto.assembler.AsientosContablesAssembler;
import sibbac.business.interfacesemitidas.dto.pantallas.AsientosContablesFilterDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.BusquedaAsientosContablesDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.DetalleAsientoContableDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceAsientosContables implements
		SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceAsientosContables.class);
	
	@Autowired
	private AsientosContablesAssembler asientosContablesAssembler;
	
	@Autowired
	private Tmct0FacturasManualesBo tmct0FacturasManualesBo;
	
	private Map<String, Object> catalogos = new HashMap<String, Object>();
	
	  /**
	   * The Enum ComandosAsientosContables.
	   */
	private enum ComandosAsientosContables {

		/** Inicializa los datos del filtro de pantalla */
		CARGA_SELECTS("CargaSelects"),
		BUSCAR_ASIENTOS_CONTABLES("BuscarAsientosContables"),
		VER_ASIENTO_CONTABLE("VerAsientoContable"),
		SIN_COMANDO("");

		/** The command. */
		private String command;

		/**
		 * Instantiates a new comandos Asientos Contables.
		 * @param command the command
		 */
		private ComandosAsientosContables(String command) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}
	
	@Override
	public WebResponse process(WebRequest webRequest)
			throws SIBBACBusinessException {
		Map<String, String> paramsFilters = webRequest.getFilters();
		Map<String, Object> paramsObjects = webRequest.getParamsObject();
		WebResponse result = new WebResponse();
		Map<String, Object> resultados = new HashMap<String, Object>();
		ComandosAsientosContables command = getCommand(webRequest.getAction());

		try {
			LOG.debug("Entro al servicio SIBBACServiceAsientosContables");

			switch (command) {
			case CARGA_SELECTS:
				result.setResultados(this.cargarSelectsFiltro());
				break;
			case BUSCAR_ASIENTOS_CONTABLES:
				result.setResultados(this.buscarAsientosContables(paramsFilters));
				break;
			case VER_ASIENTO_CONTABLE:
				result.setResultados(this.verAsientoContable(paramsObjects));
				break;
			default:
				result.setError("An action must be set. Valid actions are: " + command);
			}
		} catch (SIBBACBusinessException e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError(e.getMessage());
		} catch (Exception e) {
			resultados.put("status", "KO");
			result.setResultados(resultados);
			result.setError("ERROR");
		}

		LOG.debug("Salgo del servicio SIBBACServiceAsientosContables");
		return result;
	}

	@Override
	public List<String> getAvailableCommands() {
		return null;
	}

	@Override
	public List<String> getFields() {
		return null;
	}

	/**
	   * Carga inicial de los datos de selects de la pantalla.
	   * 
	   * @return
	   * @throws SIBBACBusinessException
	   */
	private Map<String, Object> cargarSelectsFiltro() throws SIBBACBusinessException {
		LOG.trace("Iniciando datos para el filtro - cargarSelectsFiltro");
		Map<String, Object> map = new HashMap<>();
		try {
			catalogos.put("listaAlias", tmct0FacturasManualesBo.getCatalogoAlias());
			catalogos.put("listaTipoFactura", tmct0FacturasManualesBo.getCatalogoTiposFactura());
			map.put("catalogos", catalogos);
			
		} catch (Exception e) {
			LOG.error("Error metodo init - cargarSelectsFiltro " + e.getMessage(), e);
			throw new SIBBACBusinessException(
					"Error en la carga de datos iniciales de las opciones de busqueda - cargarSelectsFiltro");
		}
		LOG.debug("Fin inicializacion datos filtro - cargarSelectsFiltro");
		return map;
	}
	
	/**
	 * Se recobra lista generada en la busqueda de respuestas de asientos contables.
	 * @param 	paramsFilters
	 * @return	Map<String, Object>
	 * @throws 	SIBBACBusinessException
	 */
	private Map<String, Object> buscarAsientosContables(Map<String, String> paramsFilters) 
			throws SIBBACBusinessException {
		LOG.debug("[buscarAsientosContables] Inicio");
		Map<String, Object> result = new HashMap<String, Object>();
		AsientosContablesFilterDTO asientosContablesFilterDTO = 
			this.asientosContablesAssembler.getAsientosContablesFilterDto(paramsFilters);
		
		List<BusquedaAsientosContablesDTO> listaBusquedaAsientosContablesDTO = 
			this.tmct0FacturasManualesBo.findAsientosContables(asientosContablesFilterDTO);
		
		result.put("listaBusquedaAsientosContablesDTO", listaBusquedaAsientosContablesDTO);
		LOG.debug("[buscarAsientosContables] Fin");
		return result;
	}
	
	/**
	 * 	Ver asiento contable.
	 *	@param paramsObject 
	 * 	@return Map<String, Object>
	 * 	@throws SIBBACBusinessException
	 */
	 private Map<String, DetalleAsientoContableDTO> verAsientoContable(
			 Map<String, Object> paramsObject) throws SIBBACBusinessException {
		 LOG.debug("[verAsientoContable] Inicio");
		 DetalleAsientoContableDTO detalleAsientoContableDTO = 
		 tmct0FacturasManualesBo.getDetalleAsientoContableModalById(
				(String) paramsObject.get("id"));
		 LOG.debug("[verAsientoContable] Fin");
		 return Collections.singletonMap("detalleAsientoContableDTO", detalleAsientoContableDTO);
	 }
		
	/**
	 * Gets the command.
	 *
	 * @param command the command
	 * @return the command
	 */
	private ComandosAsientosContables getCommand(String command) {
		for (ComandosAsientosContables thiscommand : ComandosAsientosContables.values()) {
			if (thiscommand.getCommand().equalsIgnoreCase(command)) {
				LOG.debug("Se selecciona la accion: {}", thiscommand);
				return thiscommand;
			}
		}
		return ComandosAsientosContables.SIN_COMANDO;
	}
	
}
