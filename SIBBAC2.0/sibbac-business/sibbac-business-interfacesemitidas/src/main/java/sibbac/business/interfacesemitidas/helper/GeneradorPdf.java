package sibbac.business.interfacesemitidas.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.Constantes;
import sibbac.common.SIBBACBusinessException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class GeneradorPdf {

	private static final Logger LOG = LoggerFactory.getLogger(GeneradorPdf.class);

	@Autowired
	private ApplicationContext context;

	@Value("${mail.source.folder}")
	private String templatesFolder;

	public Map<String, Object> generarPdfFacturas(Object[] objPdf, Map<String, String> mapConstantes,
	    boolean esRectificativa, boolean esBaja) throws SIBBACBusinessException {

		Map<String, Object> result = new HashMap<String, Object>();
		float left = 0;
		float right = 0;
		float top = 40;
		float bottom = 40;
		Document document = new Document(PageSize.A4, left, right, top, bottom);
		Font fontLegal = new Font(FontFamily.UNDEFINED, 8, Font.NORMAL, BaseColor.BLACK);
		Font fontNormal = new Font(FontFamily.UNDEFINED, 10, Font.NORMAL, BaseColor.BLACK);
		String logoName = templatesFolder + File.separator + "LogotipoGlobalBanking.png";
		String logoRectificativaName = templatesFolder + File.separator + "LogotipoRectificativa.png";
		String dowJonesName = templatesFolder + File.separator + "dowJones.png";
		String ftse4Name = templatesFolder + File.separator + "Ftse4Good.png";
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// PdfWriter writer = PdfWriter.getInstance(document, new
			// FileOutputStream("C:/Users/adrian.jimenez/Desktop/pruebaPDF.pdf"));
			PdfWriter writer = PdfWriter.getInstance(document, baos);
			document.open();

			PdfPTable table = new PdfPTable(4); // 4 columnas.
			table.setTotalWidth(new float[] { 55, 180, 180, 280 });// Tamaño de las
			                                                       // columnas

			// FILA 1 (TEXTO LEGAL LATERAL DERECHO Y LOGO DEL SANTANDER)
			Chunk fra = new Chunk(mapConstantes.get(Constantes.CLAVE_LEGAL), fontLegal);
			fra.setBackground(new BaseColor(166, 172, 176), 57f, 10f, 55f, 10f);
			Phrase frase = new Phrase(fra);
			PdfPCell cel1fil1 = new PdfPCell(frase);
			cel1fil1.setRotation(90);
			cel1fil1.setVerticalAlignment(Element.ALIGN_LEFT);
			cel1fil1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel1fil1.setRowspan(24);
			cel1fil1.setBorder(Rectangle.NO_BORDER);

			Image logoSantander = Image.getInstance(context.getResource("file:" + logoName).getURL());
			logoSantander.scaleAbsolute(160, 44);
			logoSantander.setAbsolutePosition(80f, 769f);
			document.add(logoSantander);

			if (esRectificativa || esBaja) {
				Image logoRectificativa = Image.getInstance(context.getResource("file:" + logoRectificativaName).getURL());
				logoRectificativa.scaleAbsolute(180, 30);
				logoRectificativa.setAbsolutePosition(95f, 660f);
				document.add(logoRectificativa);
			}

			// FILA 2 (FECHA DE LA FACTURA)

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Chunk fecFact = new Chunk(formatter.format(((Timestamp) objPdf[0])), fontNormal);
			Phrase fraseFecFact = new Phrase(fecFact);
			PdfPCell cel2fil2 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil2 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil2 = new PdfPCell(fraseFecFact);
			cel4fil2.setPaddingTop(80);
			cel2fil2.setBorder(Rectangle.NO_BORDER);
			cel3fil2.setBorder(Rectangle.NO_BORDER);
			cel4fil2.setBorder(Rectangle.NO_BORDER);

			// FILA 3 (NOMBRE SANTANDER)
			Chunk nombreSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			Phrase fraseNombreSantander = new Phrase(nombreSantander);
			PdfPCell cel2fil3 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil3 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil3 = new PdfPCell(fraseNombreSantander);
			cel2fil3.setBorder(Rectangle.NO_BORDER);
			cel3fil3.setBorder(Rectangle.NO_BORDER);
			cel4fil3.setBorder(Rectangle.NO_BORDER);

			// FILA 4 (CIF)
			Chunk cif = new Chunk("CIF: " + mapConstantes.get(Constantes.CLAVE_CIF_SANTANDER), fontNormal);
			Phrase frasecif = new Phrase(cif);
			PdfPCell cel2fil4 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil4 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil4 = new PdfPCell(frasecif);
			cel2fil4.setBorder(Rectangle.NO_BORDER);
			cel3fil4.setBorder(Rectangle.NO_BORDER);
			cel4fil4.setBorder(Rectangle.NO_BORDER);

			// FILA DIRECCIÓN SANTANDER
			Chunk direccionSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_DIRECCION_SANTANDER), fontNormal);
			Phrase fraseDireccionSantander = new Phrase(direccionSantander);
			PdfPCell cel2DireccionSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3DireccionSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4DireccionSantander = new PdfPCell(fraseDireccionSantander);
			cel2DireccionSantander.setBorder(Rectangle.NO_BORDER);
			cel3DireccionSantander.setBorder(Rectangle.NO_BORDER);
			cel4DireccionSantander.setBorder(Rectangle.NO_BORDER);

			// FILA PROVINCIA SANTANDER
			Chunk provinciaSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_PROVINCIA_SANTANDER), fontNormal);
			Phrase fraseProvinciaSantander = new Phrase(provinciaSantander);
			PdfPCell cel2ProvinciaSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3ProvinciaSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4ProvinciaSantander = new PdfPCell(fraseProvinciaSantander);
			cel2ProvinciaSantander.setBorder(Rectangle.NO_BORDER);
			cel3ProvinciaSantander.setBorder(Rectangle.NO_BORDER);
			cel4ProvinciaSantander.setBorder(Rectangle.NO_BORDER);

			// FILA PAIS SANTANDER
			Chunk paisSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_PAIS_SANTANDER), fontNormal);
			Phrase frasePaisSantander = new Phrase(paisSantander);
			PdfPCell cel2PaisSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3PaisSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4PaisSantander = new PdfPCell(frasePaisSantander);
			cel2PaisSantander.setBorder(Rectangle.NO_BORDER);
			cel3PaisSantander.setBorder(Rectangle.NO_BORDER);
			cel4PaisSantander.setBorder(Rectangle.NO_BORDER);

			// FILA 5 (EMPRESA CONTRAPARTE)
			Chunk empContr = new Chunk(objPdf[1].toString().trim(), fontNormal);
			empContr.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 0f, 1f);
			Phrase fraseEmpContr = new Phrase(empContr);
			PdfPCell cel4fil5 = new PdfPCell(fraseEmpContr);
			cel4fil5.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel4fil5.setColspan(3);
			cel4fil5.setPaddingTop(30);
			cel4fil5.setBorder(Rectangle.NO_BORDER);

			// CIF EMPRESA
			Chunk cifEmprContr = new Chunk("CIF: " + objPdf[10].toString().trim(), fontNormal);
			Phrase fraseCifEmprContr = new Phrase(cifEmprContr);
			PdfPCell cel4cifEmprContr = new PdfPCell(fraseCifEmprContr);
			cel4cifEmprContr.setColspan(3);
			cel4cifEmprContr.setBorder(Rectangle.NO_BORDER);

			// FILA 6 (DIRECCION EMPRESA CONTRAPARTE)
			Chunk dirEmprContr = new Chunk(objPdf[2].toString().trim(), fontNormal);
			Phrase fraseDirEmprContr = new Phrase(dirEmprContr);
			PdfPCell cel4fil6 = new PdfPCell(fraseDirEmprContr);
			cel4fil6.setColspan(3);
			cel4fil6.setBorder(Rectangle.NO_BORDER);

			// FILA 7 (PROVINCIA EMPRESA CONTRAPARTE)
			Chunk provEmpContr = new Chunk(objPdf[3].toString().trim(), fontNormal);
			Phrase fraseProvEmpContr = new Phrase(provEmpContr);
			PdfPCell cel4fil7 = new PdfPCell(fraseProvEmpContr);
			cel4fil7.setColspan(3);
			cel4fil7.setBorder(Rectangle.NO_BORDER);

			// FILA 8 (PAIS EMPRESA CONTRAPARTE)
			Chunk paisEmpContr = new Chunk(objPdf[4].toString().trim(), fontNormal);
			Phrase frasePaisEmpContr = new Phrase(paisEmpContr);
			PdfPCell cel4fil8 = new PdfPCell(frasePaisEmpContr);
			cel4fil8.setColspan(3);
			cel4fil8.setBorder(Rectangle.NO_BORDER);

			// FILA 9 (NÚMERO DE FACTURA)
			Chunk labelNumFact = new Chunk("INVOICE NUMBER", fontNormal);
			Phrase fraseLabelNumFact = new Phrase(labelNumFact);
			PdfPCell cel3fil9 = new PdfPCell();
			cel3fil9.setColspan(2);
			cel3fil9.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel3fil9.setPaddingTop(40);
			cel3fil9.setBorder(Rectangle.NO_BORDER);
			Chunk numFact = new Chunk(objPdf[5].toString().trim(), fontNormal);
			Phrase fraseNumFact = new Phrase(numFact);
			PdfPCell cel4fil9 = new PdfPCell();
			Paragraph fraseCombinada = new Paragraph();
			fraseCombinada.add(fraseLabelNumFact);
			fraseCombinada.add(new Phrase("    "));
			fraseCombinada.add(fraseNumFact);
			cel4fil9.addElement(fraseCombinada);
			cel4fil9.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil9.setPaddingTop(40);
			cel4fil9.setBorder(Rectangle.NO_BORDER);

			// FILA 10 (COLUMNAS DATOS FACTURA)
			Chunk labelDatosFactura = new Chunk("BROKERAGE INVOICE", fontNormal);
			labelDatosFactura.setBackground(new BaseColor(166, 172, 176), 68f, 1f, 140f, 1f);
			Phrase fraseLabelDatosFactura = new Phrase(labelDatosFactura);
			PdfPCell cel2fil10 = new PdfPCell(fraseLabelDatosFactura);
			cel2fil10.setColspan(2);
			cel2fil10.setPaddingTop(40);
			cel2fil10.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil10.setBorder(Rectangle.NO_BORDER);
			Chunk labelDatosFactura2 = new Chunk("INVOICE AMOUNT", fontNormal);
			labelDatosFactura2.setBackground(new BaseColor(166, 172, 176), 0f, 1f, 52f, 1f);
			Phrase fraseLabelDatosFactura2 = new Phrase(labelDatosFactura2);
			PdfPCell cel4fil10 = new PdfPCell(fraseLabelDatosFactura2);
			cel4fil10.setPaddingTop(40);
			cel4fil10.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil10.setBorder(Rectangle.NO_BORDER);

			// FILA 11 (DATOS FACTURA)
			Chunk valorDatosFactura = new Chunk();
			String datFact = objPdf[11].toString().trim();
			datFact = datFact + "\nPERIODO: " + formatter.format(((Timestamp) objPdf[0]));
			if (datFact.length() > 100) {
				datFact = datFact.substring(0, 99);
				valorDatosFactura = new Chunk(datFact, fontNormal);
			} else {
				valorDatosFactura = new Chunk(pad(datFact, 100, '\u00a0'), fontNormal);
			}
			Phrase fraseValorDatosFactura = new Phrase(valorDatosFactura);
			PdfPCell cel2fil11 = new PdfPCell(fraseValorDatosFactura);
			cel2fil11.setColspan(2);
			cel2fil11.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel2fil11.setBorder(Rectangle.NO_BORDER);
			Chunk valorDatosFactura2 = new Chunk(((BigDecimal) objPdf[8]).setScale(2, RoundingMode.HALF_UP) + " €",
			    fontNormal);
			if (esBaja) {
				if (((BigDecimal) objPdf[8]).signum() == -1) {
					valorDatosFactura2 = new Chunk(((BigDecimal) objPdf[8]).negate().setScale(2, RoundingMode.HALF_UP) + " €",
					    fontNormal);
				}
			}
			Phrase fraseValorDatosFactura2 = new Phrase(valorDatosFactura2);
			PdfPCell cel4fil11 = new PdfPCell(fraseValorDatosFactura2);
			cel4fil11.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil11.setBorder(Rectangle.NO_BORDER);

			// FILA 12 (COLUMNAS CANTIDADES FACTURA)
			Chunk labelTaxBase = new Chunk("TAX BASE", fontNormal);
			labelTaxBase.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 40f, 1f);
			Phrase fraseLabelTaxBase = new Phrase(labelTaxBase);
			PdfPCell cel2fil12 = new PdfPCell(fraseLabelTaxBase);
			cel2fil12.setPaddingTop(40);
			cel2fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil12.setBorder(Rectangle.NO_BORDER);
			Chunk labelTax = new Chunk("TAX", fontNormal);
			labelTax.setBackground(new BaseColor(166, 172, 176), 51f, 1f, 50f, 1f);
			Phrase fraseLabelTax = new Phrase(labelTax);
			PdfPCell cel3fil12 = new PdfPCell(fraseLabelTax);
			cel3fil12.setPaddingTop(40);
			cel3fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel3fil12.setBorder(Rectangle.NO_BORDER);
			Chunk labelTotalAmount = new Chunk("TOTAL AMOUNT", fontNormal);
			labelTotalAmount.setBackground(new BaseColor(166, 172, 176), 60f, 1f, 57f, 1f);
			Phrase fraseLabelTotalAmount = new Phrase(labelTotalAmount);
			PdfPCell cel4fil12 = new PdfPCell(fraseLabelTotalAmount);
			cel4fil12.setPaddingTop(40);
			cel4fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil12.setBorder(Rectangle.NO_BORDER);

			// FILA 13 (CANTIDADES FACTURA)
			Chunk valorTaxBase = new Chunk(((BigDecimal) objPdf[8]).setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			if (esBaja) {
				if (((BigDecimal) objPdf[8]).signum() == -1) {
					valorTaxBase = new Chunk(((BigDecimal) objPdf[8]).negate().setScale(2, RoundingMode.HALF_UP) + " €",
					    fontNormal);
				}
			}
			Phrase fraseValorTaxBase = new Phrase(valorTaxBase);
			PdfPCell cel2fil13 = new PdfPCell(fraseValorTaxBase);
			cel2fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil13.setBorder(Rectangle.NO_BORDER);
			Chunk valorTax = new Chunk(((BigDecimal) objPdf[9]).setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			if (esBaja) {
				if (((BigDecimal) objPdf[9]).signum() == -1) {
					valorTax = new Chunk(((BigDecimal) objPdf[9]).negate().setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
				}
			}
			Phrase fraseValorTax = new Phrase(valorTax);
			PdfPCell cel3fil13 = new PdfPCell(fraseValorTax);
			cel3fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel3fil13.setBorder(Rectangle.NO_BORDER);
			BigDecimal suma = (BigDecimal) objPdf[8];
			suma = suma.add((BigDecimal) objPdf[9]);
			Chunk valorTotalAmount = new Chunk(suma.setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			if (esBaja) {
				if (suma.signum() == -1) {
					valorTotalAmount = new Chunk(suma.negate().setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
				}
			}
			Phrase fraseValorTotalAmount = new Phrase(valorTotalAmount);
			PdfPCell cel4fil13 = new PdfPCell(fraseValorTotalAmount);
			cel4fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil13.setBorder(Rectangle.NO_BORDER);

			// FILA 14 (REFERENCIA FACTURA)
			Chunk invRefer = new Chunk("", fontNormal);
			Phrase fraseInvRefer = new Phrase(invRefer);
			PdfPCell cel2fil14 = new PdfPCell(fraseInvRefer);
			cel2fil14.setColspan(3);
			cel2fil14.setPaddingTop(40);
			cel2fil14.setBorder(Rectangle.NO_BORDER);

			// FILA 15 (DESCRIPCION SANTANDER)
			Chunk descSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_DESCRIPCION_SANTANDER), fontNormal);
			Phrase frasedescSantander = new Phrase(descSantander);
			PdfPCell cel2fil15 = new PdfPCell(frasedescSantander);
			cel2fil15.setColspan(3);
			cel2fil15.setPaddingTop(120);
			cel2fil15.setBorder(Rectangle.NO_BORDER);

			// FILA 16 (NUMERO DE CUENTA)
			Chunk labelBankAccount = new Chunk(Constantes.CLAVE_BANK_ACCOUNT, fontNormal);
			labelBankAccount.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 50f, 1f);
			Phrase fraseLabelBankAccount = new Phrase(labelBankAccount);
			PdfPCell cel2fil16 = new PdfPCell(fraseLabelBankAccount);
			cel2fil16.setBorder(Rectangle.NO_BORDER);
			Chunk bankAccount = new Chunk(mapConstantes.get(Constantes.CLAVE_BANK_ACCOUNT), fontNormal);
			bankAccount.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 103f, 1f);
			Phrase fraseBankAccount = new Phrase(bankAccount);
			PdfPCell cel3fil16 = new PdfPCell(fraseBankAccount);
			cel3fil16.setColspan(2);
			cel3fil16.setBorder(Rectangle.NO_BORDER);

			// FILA 17 (IBAN)
			Chunk labelIban = new Chunk(Constantes.CLAVE_IBAN, fontNormal);
			Phrase fraseLabelIban = new Phrase(labelIban);
			PdfPCell cel2fil17 = new PdfPCell(fraseLabelIban);
			cel2fil17.setBorder(Rectangle.NO_BORDER);
			Chunk valorIban = new Chunk(mapConstantes.get(Constantes.CLAVE_IBAN), fontNormal);
			Phrase fraseValorIban = new Phrase(valorIban);
			PdfPCell cel3fil17 = new PdfPCell(fraseValorIban);
			cel3fil17.setColspan(2);
			cel3fil17.setBorder(Rectangle.NO_BORDER);

			// FILA 18 (BIC CODE)
			Chunk labelBicCode = new Chunk(Constantes.CLAVE_BIC_CODE, fontNormal);
			labelBicCode.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 53f, 1f);
			Phrase fraseLabelBicCode = new Phrase(labelBicCode);
			PdfPCell cel2fil18 = new PdfPCell(fraseLabelBicCode);
			cel2fil18.setBorder(Rectangle.NO_BORDER);
			Chunk valorBicCode = new Chunk(mapConstantes.get(Constantes.CLAVE_BIC_CODE), fontNormal);
			valorBicCode.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 170f, 1f);
			Phrase fraseValorBicCode = new Phrase(valorBicCode);
			PdfPCell cel3fil18 = new PdfPCell(fraseValorBicCode);
			cel3fil18.setColspan(2);
			cel3fil18.setBorder(Rectangle.NO_BORDER);

			// FILA 19 (BENEFICIARY BIC)
			Chunk labelBeneficiaryBic = new Chunk(Constantes.CLAVE_BENEFICIARY_BIC, fontNormal);
			Phrase fraseLabelBeneficiaryBic = new Phrase(labelBeneficiaryBic);
			PdfPCell cel2fil19 = new PdfPCell(fraseLabelBeneficiaryBic);
			cel2fil19.setBorder(Rectangle.NO_BORDER);
			Chunk valorBeneficiaryBic = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY_BIC), fontNormal);
			Phrase fraseValorBeneficiaryBic = new Phrase(valorBeneficiaryBic);
			PdfPCell cel3fil19 = new PdfPCell(fraseValorBeneficiaryBic);
			cel3fil19.setColspan(2);
			cel3fil19.setBorder(Rectangle.NO_BORDER);

			// FILA 20 (BENEFICIARY)
			Chunk labelBeneficiary = new Chunk(Constantes.CLAVE_BENEFICIARY, fontNormal);
			labelBeneficiary.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 53f, 1f);
			Phrase fraseLabelBeneficiary = new Phrase(labelBeneficiary);
			PdfPCell cel2fil20 = new PdfPCell(fraseLabelBeneficiary);
			cel2fil20.setBorder(Rectangle.NO_BORDER);
			Chunk valorBeneficiary = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			valorBeneficiary.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 60f, 1f);
			Phrase fraseValorBeneficiary = new Phrase(valorBeneficiary);
			PdfPCell cel3fil20 = new PdfPCell(fraseValorBeneficiary);
			cel3fil20.setColspan(2);
			cel3fil20.setBorder(Rectangle.NO_BORDER);

			// FILA 21 (FOOTER)
			Chunk santanderSAU = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			Phrase fraseSantanderSAU = new Phrase(santanderSAU);
			Image logoDowJones = Image.getInstance(context.getResource("file:" + dowJonesName).getURL());
			logoDowJones.scalePercent(25);
			logoDowJones.setAbsolutePosition(320f, 40f);
			document.add(logoDowJones);
			Image logoFTSE = Image.getInstance(context.getResource("file:" + ftse4Name).getURL());
			logoFTSE.scaleAbsolute(60, 50);
			logoFTSE.setAbsolutePosition(460f, 55f);
			document.add(logoFTSE);
			PdfPCell cel2fil21 = new PdfPCell();
			cel2fil21.addElement(fraseSantanderSAU);
			cel2fil21.setColspan(3);
			cel2fil21.setPaddingTop(40);
			cel2fil21.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cel2fil21.setBorder(Rectangle.NO_BORDER);

			table.addCell(cel1fil1);
			table.addCell(cel2fil2);
			table.addCell(cel3fil2);
			table.addCell(cel4fil2);
			table.addCell(cel2fil3);
			table.addCell(cel3fil3);
			table.addCell(cel4fil3);
			table.addCell(cel2fil4);
			table.addCell(cel3fil4);
			table.addCell(cel4fil4);
			table.addCell(cel2DireccionSantander);
			table.addCell(cel3DireccionSantander);
			table.addCell(cel4DireccionSantander);
			table.addCell(cel2ProvinciaSantander);
			table.addCell(cel3ProvinciaSantander);
			table.addCell(cel4ProvinciaSantander);
			table.addCell(cel2PaisSantander);
			table.addCell(cel3PaisSantander);
			table.addCell(cel4PaisSantander);
			table.addCell(cel4fil5);
			table.addCell(cel4cifEmprContr);
			table.addCell(cel4fil6);
			table.addCell(cel4fil7);
			table.addCell(cel4fil8);
			table.addCell(cel3fil9);
			table.addCell(cel4fil9);
			table.addCell(cel2fil10);
			table.addCell(cel4fil10);
			table.addCell(cel2fil11);
			table.addCell(cel4fil11);
			table.addCell(cel2fil12);
			table.addCell(cel3fil12);
			table.addCell(cel4fil12);
			table.addCell(cel2fil13);
			table.addCell(cel3fil13);
			table.addCell(cel4fil13);
			table.addCell(cel2fil14);
			table.addCell(cel2fil15);
			table.addCell(cel2fil16);
			table.addCell(cel3fil16);
			table.addCell(cel2fil17);
			table.addCell(cel3fil17);
			table.addCell(cel2fil18);
			table.addCell(cel3fil18);
			table.addCell(cel2fil19);
			table.addCell(cel3fil19);
			table.addCell(cel2fil20);
			table.addCell(cel3fil20);
			table.addCell(cel2fil21);
			document.add(table);
			document.close();
			writer.close();

			// Formamos el nombre del fichero
			String numeroFactura = objPdf[5].toString().trim();
			final String nombreFichero, prefijo, clavePrefijo;
			clavePrefijo = (esRectificativa || esBaja) ? Constantes.CLAVE_NOMBRE_RECTIFICATIVA
			    : Constantes.CLAVE_NOMBRE_NORMAL;
			prefijo = mapConstantes.get(clavePrefijo);
			nombreFichero = defineNombreFichero(prefijo, numeroFactura);
			result.put("nombreFichero", nombreFichero);
			result.put("file", baos.toByteArray());
			return result;
		} catch (RuntimeException e) {
			LOG.error("[generarPdfFacturas] Error no controlado al generar documento PDF", e);
			throw e;
		} catch (DocumentException e) {
			LOG.error("[generarPdfFacturas] Error de generación de PDF", e);
			throw new SIBBACBusinessException("Error de generación de PDF", e);
		} catch (IOException ioex) {
			LOG.error("[generarPdfFacturas] Error de Entrada/Salida al generar PDF", ioex);
			throw new SIBBACBusinessException("Error de Entrada/Salida al generar PDF", ioex);
		}
	}

	public Map<String, Object> generarPdfFacturasManualesGrupoIVA(Object[] objPdf, Map<String, String> mapConstantes)
	    throws SIBBACBusinessException {

		Map<String, Object> result = new HashMap<String, Object>();
		float left = 0;
		float right = 0;
		float top = 40;
		float bottom = 40;
		Document document = new Document(PageSize.A4, left, right, top, bottom);
		Font fontLegal = new Font(FontFamily.UNDEFINED, 8, Font.NORMAL, BaseColor.BLACK);
		Font fontNormal = new Font(FontFamily.UNDEFINED, 10, Font.NORMAL, BaseColor.BLACK);
		String logoName = templatesFolder + File.separator + "LogotipoGlobalBanking.png";
		String dowJonesName = templatesFolder + File.separator + "dowJones.png";
		String ftse4Name = templatesFolder + File.separator + "Ftse4Good.png";
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// PdfWriter writer = PdfWriter.getInstance(document, new
			// FileOutputStream("C:/Users/adrian.jimenez/Desktop/pruebaPDF.pdf"));
			PdfWriter writer = PdfWriter.getInstance(document, baos);
			document.open();

			PdfPTable table = new PdfPTable(4); // 4 columnas.
			table.setTotalWidth(new float[] { 55, 180, 180, 280 });// Tamaño de las
			                                                       // columnas

			// FILA 1 (TEXTO LEGAL LATERAL DERECHO Y LOGO DEL SANTANDER)
			Chunk fra = new Chunk(mapConstantes.get(Constantes.CLAVE_LEGAL), fontLegal);
			fra.setBackground(new BaseColor(166, 172, 176), 57f, 10f, 55f, 10f);
			Phrase frase = new Phrase(fra);
			PdfPCell cel1fil1 = new PdfPCell(frase);
			cel1fil1.setRotation(90);
			cel1fil1.setVerticalAlignment(Element.ALIGN_LEFT);
			cel1fil1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel1fil1.setRowspan(24);
			cel1fil1.setBorder(Rectangle.NO_BORDER);

			Image logoSantander = Image.getInstance(context.getResource("file:" + logoName).getURL());
			logoSantander.scaleAbsolute(160, 44);
			logoSantander.setAbsolutePosition(80f, 769f);
			document.add(logoSantander);

			// FILA 2 (FECHA DE LA FACTURA)

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Chunk fecFact = new Chunk(formatter.format(((Timestamp) objPdf[0])), fontNormal);
			Phrase fraseFecFact = new Phrase(fecFact);
			PdfPCell cel2fil2 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil2 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil2 = new PdfPCell(fraseFecFact);
			cel4fil2.setPaddingTop(50);
			cel2fil2.setBorder(Rectangle.NO_BORDER);
			cel3fil2.setBorder(Rectangle.NO_BORDER);
			cel4fil2.setBorder(Rectangle.NO_BORDER);

			// FILA 3 (NOMBRE SANTANDER)
			Chunk nombreSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			Phrase fraseNombreSantander = new Phrase(nombreSantander);
			PdfPCell cel2fil3 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil3 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil3 = new PdfPCell(fraseNombreSantander);
			cel2fil3.setBorder(Rectangle.NO_BORDER);
			cel3fil3.setBorder(Rectangle.NO_BORDER);
			cel4fil3.setBorder(Rectangle.NO_BORDER);

			// FILA 4 (CIF)
			Chunk cif = new Chunk("CIF: " + mapConstantes.get(Constantes.CLAVE_CIF_SANTANDER), fontNormal);
			Phrase frasecif = new Phrase(cif);
			PdfPCell cel2fil4 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil4 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil4 = new PdfPCell(frasecif);
			cel2fil4.setBorder(Rectangle.NO_BORDER);
			cel3fil4.setBorder(Rectangle.NO_BORDER);
			cel4fil4.setBorder(Rectangle.NO_BORDER);

			// FILA DIRECCIÓN SANTANDER
			Chunk direccionSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_DIRECCION_SANTANDER), fontNormal);
			Phrase fraseDireccionSantander = new Phrase(direccionSantander);
			PdfPCell cel2DireccionSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3DireccionSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4DireccionSantander = new PdfPCell(fraseDireccionSantander);
			cel2DireccionSantander.setBorder(Rectangle.NO_BORDER);
			cel3DireccionSantander.setBorder(Rectangle.NO_BORDER);
			cel4DireccionSantander.setBorder(Rectangle.NO_BORDER);

			// FILA PROVINCIA SANTANDER
			Chunk provinciaSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_PROVINCIA_SANTANDER), fontNormal);
			Phrase fraseProvinciaSantander = new Phrase(provinciaSantander);
			PdfPCell cel2ProvinciaSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3ProvinciaSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4ProvinciaSantander = new PdfPCell(fraseProvinciaSantander);
			cel2ProvinciaSantander.setBorder(Rectangle.NO_BORDER);
			cel3ProvinciaSantander.setBorder(Rectangle.NO_BORDER);
			cel4ProvinciaSantander.setBorder(Rectangle.NO_BORDER);

			// FILA PAIS SANTANDER
			Chunk paisSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_PAIS_SANTANDER), fontNormal);
			Phrase frasePaisSantander = new Phrase(paisSantander);
			PdfPCell cel2PaisSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3PaisSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4PaisSantander = new PdfPCell(frasePaisSantander);
			cel2PaisSantander.setBorder(Rectangle.NO_BORDER);
			cel3PaisSantander.setBorder(Rectangle.NO_BORDER);
			cel4PaisSantander.setBorder(Rectangle.NO_BORDER);

			// FILA 5 (EMPRESA CONTRAPARTE)
			Chunk empContr = new Chunk(objPdf[1].toString().trim(), fontNormal);
			empContr.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 0f, 1f);
			Phrase fraseEmpContr = new Phrase(empContr);
			PdfPCell cel4fil5 = new PdfPCell(fraseEmpContr);
			cel4fil5.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel4fil5.setColspan(3);
			cel4fil5.setPaddingTop(30);
			cel4fil5.setBorder(Rectangle.NO_BORDER);

			// CIF EMPRESA
			Chunk cifEmprContr = new Chunk("CIF: " + objPdf[10].toString().trim(), fontNormal);
			Phrase fraseCifEmprContr = new Phrase(cifEmprContr);
			PdfPCell cel4cifEmprContr = new PdfPCell(fraseCifEmprContr);
			cel4cifEmprContr.setColspan(3);
			cel4cifEmprContr.setBorder(Rectangle.NO_BORDER);

			// FILA 6 (DIRECCION EMPRESA CONTRAPARTE)
			Chunk dirEmprContr = new Chunk(objPdf[2].toString().trim(), fontNormal);
			Phrase fraseDirEmprContr = new Phrase(dirEmprContr);
			PdfPCell cel4fil6 = new PdfPCell(fraseDirEmprContr);
			cel4fil6.setColspan(3);
			cel4fil6.setBorder(Rectangle.NO_BORDER);

			// FILA 7 (PROVINCIA EMPRESA CONTRAPARTE)
			Chunk provEmpContr = new Chunk(objPdf[3].toString().trim(), fontNormal);
			Phrase fraseProvEmpContr = new Phrase(provEmpContr);
			PdfPCell cel4fil7 = new PdfPCell(fraseProvEmpContr);
			cel4fil7.setColspan(3);
			cel4fil7.setBorder(Rectangle.NO_BORDER);

			// FILA 8 (PAIS EMPRESA CONTRAPARTE)
			Chunk paisEmpContr = new Chunk(objPdf[4].toString().trim(), fontNormal);
			Phrase frasePaisEmpContr = new Phrase(paisEmpContr);
			PdfPCell cel4fil8 = new PdfPCell(frasePaisEmpContr);
			cel4fil8.setColspan(3);
			cel4fil8.setBorder(Rectangle.NO_BORDER);

			// FILA 9 (NÚMERO DE FACTURA)
			Chunk labelNumFact = new Chunk("FACTURA NUMERO", fontNormal);
			Phrase fraseLabelNumFact = new Phrase(labelNumFact);
			PdfPCell cel3fil9 = new PdfPCell();
			cel3fil9.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel3fil9.setPaddingTop(40);
			cel3fil9.setBorder(Rectangle.NO_BORDER);
			Chunk numFact = new Chunk("SVGIVA-" + objPdf[5].toString().trim(), fontNormal);
			Phrase fraseNumFact = new Phrase(numFact);
			PdfPCell cel4fil9 = new PdfPCell();
			Paragraph fraseCombinada = new Paragraph();
			fraseCombinada.add(fraseLabelNumFact);
			fraseCombinada.add(new Phrase("    "));
			fraseCombinada.add(fraseNumFact);
			fraseCombinada.setAlignment(Element.ALIGN_RIGHT);
			cel4fil9.setColspan(2);
			cel4fil9.addElement(fraseCombinada);
			cel4fil9.setPaddingTop(40);
			cel4fil9.setBorder(Rectangle.NO_BORDER);

			// FILA 10 (COLUMNAS DATOS FACTURA)
			Chunk labelDatosFactura = new Chunk("FACTURACION DE COMISIONES", fontNormal);
			labelDatosFactura.setBackground(new BaseColor(166, 172, 176), 44f, 1f, 140f, 1f);
			Phrase fraseLabelDatosFactura = new Phrase(labelDatosFactura);
			PdfPCell cel2fil10 = new PdfPCell(fraseLabelDatosFactura);
			cel2fil10.setColspan(2);
			cel2fil10.setPaddingTop(30);
			cel2fil10.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil10.setBorder(Rectangle.NO_BORDER);
			Chunk labelDatosFactura2 = new Chunk("IMPORTE DE FACTURA", fontNormal);
			labelDatosFactura2.setBackground(new BaseColor(166, 172, 176), 0f, 1f, 52f, 1f);
			Phrase fraseLabelDatosFactura2 = new Phrase(labelDatosFactura2);
			PdfPCell cel4fil10 = new PdfPCell(fraseLabelDatosFactura2);
			cel4fil10.setPaddingTop(30);
			cel4fil10.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil10.setBorder(Rectangle.NO_BORDER);

			// FILA 11 (DATOS FACTURA)
			Chunk valorDatosFactura = new Chunk();
			String datFact = objPdf[6].toString().trim();
			if (datFact.length() > 500) {
				datFact = datFact.substring(0, 499);
				valorDatosFactura = new Chunk(datFact, fontNormal);
			} else {
				valorDatosFactura = new Chunk(pad(datFact + " ", 500, '\u00a0'), fontNormal);
			}
			Phrase fraseValorDatosFactura = new Phrase(valorDatosFactura);
			PdfPCell cel2fil11 = new PdfPCell(fraseValorDatosFactura);
			cel2fil11.setColspan(2);
			cel2fil11.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel2fil11.setBorder(Rectangle.NO_BORDER);
			Chunk valorDatosFactura2 = new Chunk(((BigDecimal) objPdf[8]).setScale(2, RoundingMode.HALF_UP) + " €",
			    fontNormal);
			Phrase fraseValorDatosFactura2 = new Phrase(valorDatosFactura2);
			PdfPCell cel4fil11 = new PdfPCell(fraseValorDatosFactura2);
			cel4fil11.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil11.setBorder(Rectangle.NO_BORDER);

			// FILA 12 (COLUMNAS CANTIDADES FACTURA)
			Chunk labelTaxBase = new Chunk(
			    "(*)BASE IMPONIBLE IVA GRUPO (" + ((BigDecimal) objPdf[12]).setScale(2, RoundingMode.HALF_UP) + "%)",
			    fontNormal);
			labelTaxBase.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 40f, 1f);
			Phrase fraseLabelTaxBase = new Phrase(labelTaxBase);
			PdfPCell cel2fil12 = new PdfPCell(fraseLabelTaxBase);
			cel2fil12.setColspan(2);
			cel2fil12.setPaddingTop(40);
			cel2fil12.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel2fil12.setBorder(Rectangle.NO_BORDER);
			// Chunk labelTax = new Chunk("IVA(21%)", fontNormal);
			// labelTax.setBackground(new BaseColor(166, 172, 176), 51f, 1f, 50f, 1f);
			// Phrase fraseLabelTax = new Phrase(labelTax);
			// PdfPCell cel3fil12 = new PdfPCell(fraseLabelTax);
			// cel3fil12.setPaddingTop(40);
			// cel3fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			// cel3fil12.setBorder(Rectangle.NO_BORDER);
			Chunk labelTotalAmount = new Chunk("IVA(21%)               IMPORTE TOTAL", fontNormal);
			labelTotalAmount.setBackground(new BaseColor(166, 172, 176), 60f, 1f, 30f, 1f);
			Phrase fraseLabelTotalAmount = new Phrase(labelTotalAmount);
			PdfPCell cel4fil12 = new PdfPCell(fraseLabelTotalAmount);
			cel4fil12.setPaddingTop(40);
			cel4fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil12.setBorder(Rectangle.NO_BORDER);

			// FILA 13 (CANTIDADES FACTURA)
			Chunk valorTaxBase = new Chunk(((BigDecimal) objPdf[8]).setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			Phrase fraseValorTaxBase = new Phrase(valorTaxBase);
			PdfPCell cel2fil13 = new PdfPCell(fraseValorTaxBase);
			cel2fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil13.setBorder(Rectangle.NO_BORDER);
			BigDecimal suma = (BigDecimal) objPdf[8];
			suma = suma.add((BigDecimal) objPdf[9]);
			Chunk valorTax = new Chunk("                                                   "
			    + ((BigDecimal) objPdf[9]).setScale(2, RoundingMode.HALF_UP) + " €                    "
			    + suma.setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			Phrase fraseValorTax = new Phrase(valorTax);
			PdfPCell cel3fil13 = new PdfPCell(fraseValorTax);
			cel3fil13.setColspan(2);
			cel3fil13.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel3fil13.setBorder(Rectangle.NO_BORDER);
			// Chunk valorTotalAmount = new Chunk(, fontNormal);
			// Phrase fraseValorTotalAmount = new Phrase(valorTotalAmount);
			// PdfPCell cel4fil13 = new PdfPCell(fraseValorTotalAmount);
			// cel4fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			// cel4fil13.setBorder(Rectangle.NO_BORDER);

			// FILA 14 (REFERENCIA FACTURA)
			Chunk pieGrupoIva = new Chunk(mapConstantes.get(Constantes.CLAVE_PIE_PAGINA_GRUPO_IVA), fontNormal);
			Phrase frasePieGrupoIva = new Phrase(pieGrupoIva);
			PdfPCell cel2fil14 = new PdfPCell(frasePieGrupoIva);
			cel2fil14.setColspan(3);
			cel2fil14.setPaddingTop(80);
			cel2fil14.setBorder(Rectangle.NO_BORDER);

			// FILA 15 (DESCRIPCION SANTANDER)
			Chunk descSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_DESCRIPCION_SANTANDER), fontNormal);
			Phrase frasedescSantander = new Phrase(descSantander);
			PdfPCell cel2fil15 = new PdfPCell(frasedescSantander);
			cel2fil15.setColspan(3);
			cel2fil15.setPaddingTop(30);
			cel2fil15.setBorder(Rectangle.NO_BORDER);

			// FILA 16 (NUMERO DE CUENTA)
			Chunk labelBankAccount = new Chunk(Constantes.CLAVE_BANK_ACCOUNT, fontNormal);
			labelBankAccount.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 50f, 1f);
			Phrase fraseLabelBankAccount = new Phrase(labelBankAccount);
			PdfPCell cel2fil16 = new PdfPCell(fraseLabelBankAccount);
			cel2fil16.setBorder(Rectangle.NO_BORDER);
			Chunk bankAccount = new Chunk(mapConstantes.get(Constantes.CLAVE_BANK_ACCOUNT), fontNormal);
			bankAccount.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 103f, 1f);
			Phrase fraseBankAccount = new Phrase(bankAccount);
			PdfPCell cel3fil16 = new PdfPCell(fraseBankAccount);
			cel3fil16.setColspan(2);
			cel3fil16.setBorder(Rectangle.NO_BORDER);

			// FILA 17 (IBAN)
			Chunk labelIban = new Chunk(Constantes.CLAVE_IBAN, fontNormal);
			Phrase fraseLabelIban = new Phrase(labelIban);
			PdfPCell cel2fil17 = new PdfPCell(fraseLabelIban);
			cel2fil17.setBorder(Rectangle.NO_BORDER);
			Chunk valorIban = new Chunk(mapConstantes.get(Constantes.CLAVE_IBAN), fontNormal);
			Phrase fraseValorIban = new Phrase(valorIban);
			PdfPCell cel3fil17 = new PdfPCell(fraseValorIban);
			cel3fil17.setColspan(2);
			cel3fil17.setBorder(Rectangle.NO_BORDER);

			// FILA 18 (BIC CODE)
			Chunk labelBicCode = new Chunk(Constantes.CLAVE_BIC_CODE, fontNormal);
			labelBicCode.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 53f, 1f);
			Phrase fraseLabelBicCode = new Phrase(labelBicCode);
			PdfPCell cel2fil18 = new PdfPCell(fraseLabelBicCode);
			cel2fil18.setBorder(Rectangle.NO_BORDER);
			Chunk valorBicCode = new Chunk(mapConstantes.get(Constantes.CLAVE_BIC_CODE), fontNormal);
			valorBicCode.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 170f, 1f);
			Phrase fraseValorBicCode = new Phrase(valorBicCode);
			PdfPCell cel3fil18 = new PdfPCell(fraseValorBicCode);
			cel3fil18.setColspan(2);
			cel3fil18.setBorder(Rectangle.NO_BORDER);

			// FILA 19 (BENEFICIARY BIC)
			Chunk labelBeneficiaryBic = new Chunk(Constantes.CLAVE_BENEFICIARY_BIC, fontNormal);
			Phrase fraseLabelBeneficiaryBic = new Phrase(labelBeneficiaryBic);
			PdfPCell cel2fil19 = new PdfPCell(fraseLabelBeneficiaryBic);
			cel2fil19.setBorder(Rectangle.NO_BORDER);
			Chunk valorBeneficiaryBic = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY_BIC), fontNormal);
			Phrase fraseValorBeneficiaryBic = new Phrase(valorBeneficiaryBic);
			PdfPCell cel3fil19 = new PdfPCell(fraseValorBeneficiaryBic);
			cel3fil19.setColspan(2);
			cel3fil19.setBorder(Rectangle.NO_BORDER);

			// FILA 20 (BENEFICIARY)
			Chunk labelBeneficiary = new Chunk(Constantes.CLAVE_BENEFICIARY, fontNormal);
			labelBeneficiary.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 53f, 1f);
			Phrase fraseLabelBeneficiary = new Phrase(labelBeneficiary);
			PdfPCell cel2fil20 = new PdfPCell(fraseLabelBeneficiary);
			cel2fil20.setBorder(Rectangle.NO_BORDER);
			Chunk valorBeneficiary = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			valorBeneficiary.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 60f, 1f);
			Phrase fraseValorBeneficiary = new Phrase(valorBeneficiary);
			PdfPCell cel3fil20 = new PdfPCell(fraseValorBeneficiary);
			cel3fil20.setColspan(2);
			cel3fil20.setBorder(Rectangle.NO_BORDER);

			// FILA 21 (FOOTER)
			Chunk santanderSAU = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			Phrase fraseSantanderSAU = new Phrase(santanderSAU);
			Image logoDowJones = Image.getInstance(context.getResource("file:" + dowJonesName).getURL());
			logoDowJones.scalePercent(25);
			logoDowJones.setAbsolutePosition(320f, 40f);
			document.add(logoDowJones);
			Image logoFTSE = Image.getInstance(context.getResource("file:" + ftse4Name).getURL());
			logoFTSE.scaleAbsolute(60, 50);
			logoFTSE.setAbsolutePosition(460f, 55f);
			document.add(logoFTSE);
			PdfPCell cel2fil21 = new PdfPCell();
			cel2fil21.addElement(fraseSantanderSAU);
			cel2fil21.setColspan(3);
			cel2fil21.setPaddingTop(40);
			cel2fil21.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cel2fil21.setBorder(Rectangle.NO_BORDER);

			table.addCell(cel1fil1);
			table.addCell(cel2fil2);
			table.addCell(cel3fil2);
			table.addCell(cel4fil2);
			table.addCell(cel2fil3);
			table.addCell(cel3fil3);
			table.addCell(cel4fil3);
			table.addCell(cel2fil4);
			table.addCell(cel3fil4);
			table.addCell(cel4fil4);
			table.addCell(cel2DireccionSantander);
			table.addCell(cel3DireccionSantander);
			table.addCell(cel4DireccionSantander);
			table.addCell(cel2ProvinciaSantander);
			table.addCell(cel3ProvinciaSantander);
			table.addCell(cel4ProvinciaSantander);
			table.addCell(cel2PaisSantander);
			table.addCell(cel3PaisSantander);
			table.addCell(cel4PaisSantander);
			table.addCell(cel4fil5);
			table.addCell(cel4cifEmprContr);
			table.addCell(cel4fil6);
			table.addCell(cel4fil7);
			table.addCell(cel4fil8);
			table.addCell(cel3fil9);
			table.addCell(cel4fil9);
			table.addCell(cel2fil10);
			table.addCell(cel4fil10);
			table.addCell(cel2fil11);
			table.addCell(cel4fil11);
			table.addCell(cel2fil12);
			// table.addCell(cel3fil12);
			table.addCell(cel4fil12);
			table.addCell(cel2fil13);
			table.addCell(cel3fil13);
			// table.addCell(cel4fil13);
			table.addCell(cel2fil14);
			table.addCell(cel2fil15);
			table.addCell(cel2fil16);
			table.addCell(cel3fil16);
			table.addCell(cel2fil17);
			table.addCell(cel3fil17);
			table.addCell(cel2fil18);
			table.addCell(cel3fil18);
			table.addCell(cel2fil19);
			table.addCell(cel3fil19);
			table.addCell(cel2fil20);
			table.addCell(cel3fil20);
			table.addCell(cel2fil21);

			document.add(table);
			document.close();
			writer.close();

			// Formamos el nombre del fichero
			String numeroFactura = objPdf[5].toString().trim();
			String nombreFichero = defineNombreFichero(mapConstantes.get(Constantes.CLAVE_NOMBRE_GRUPO_IVA), numeroFactura);
			result.put("nombreFichero", nombreFichero);
			result.put("file", baos.toByteArray());
			return result;
		} catch (RuntimeException e) {
			LOG.error("[generarPdfFacturasManualesGrupoIVA] Error no controlado al generar documento PDF", e);
			throw e;
		} catch (DocumentException e) {
			LOG.error("[generarPdfFacturasManualesGrupoIVA] Error de generación de PDF", e);
			throw new SIBBACBusinessException("Error de generación de PDF", e);
		} catch (IOException ioex) {
			LOG.error("[generarPdfFacturasManualesGrupoIVA] Error de Entrada/Salida al generar PDF", ioex);
			throw new SIBBACBusinessException("Error de Entrada/Salida al generar PDF", ioex);
		}
	}

	public Map<String, Object> generarPdfFacturasRectificativasGrupoIVA(Object[] objPdf,
	    Map<String, String> mapConstantes) throws SIBBACBusinessException {

		Map<String, Object> result = new HashMap<String, Object>();
		float left = 0;
		float right = 0;
		float top = 40;
		float bottom = 40;
		Document document = new Document(PageSize.A4, left, right, top, bottom);
		Font fontLegal = new Font(FontFamily.UNDEFINED, 8, Font.NORMAL, BaseColor.BLACK);
		Font fontNormal = new Font(FontFamily.UNDEFINED, 10, Font.NORMAL, BaseColor.BLACK);
		String logoName = templatesFolder + File.separator + "LogotipoGlobalBanking.png";
		String dowJonesName = templatesFolder + File.separator + "dowJones.png";
		String ftse4Name = templatesFolder + File.separator + "Ftse4Good.png";
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// PdfWriter writer = PdfWriter.getInstance(document, new
			// FileOutputStream("C:/Users/adrian.jimenez/Desktop/pruebaPDF.pdf"));
			PdfWriter writer = PdfWriter.getInstance(document, baos);
			document.open();

			PdfPTable table = new PdfPTable(4); // 4 columnas.
			table.setTotalWidth(new float[] { 55, 180, 180, 280 });// Tamaño de las
			                                                       // columnas

			// FILA 1 (TEXTO LEGAL LATERAL DERECHO Y LOGO DEL SANTANDER)
			Chunk fra = new Chunk(mapConstantes.get(Constantes.CLAVE_LEGAL), fontLegal);
			fra.setBackground(new BaseColor(166, 172, 176), 57f, 10f, 55f, 10f);
			Phrase frase = new Phrase(fra);
			PdfPCell cel1fil1 = new PdfPCell(frase);
			cel1fil1.setRotation(90);
			cel1fil1.setVerticalAlignment(Element.ALIGN_LEFT);
			cel1fil1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel1fil1.setRowspan(27);
			cel1fil1.setBorder(Rectangle.NO_BORDER);

			Image logoSantander = Image.getInstance(context.getResource("file:" + logoName).getURL());
			logoSantander.scaleAbsolute(160, 44);
			logoSantander.setAbsolutePosition(80f, 767f);
			document.add(logoSantander);

			// FILA 2 (FECHA DE LA FACTURA)

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Chunk fecFact = new Chunk(formatter.format(((Timestamp) objPdf[0])), fontNormal);
			Phrase fraseFecFact = new Phrase(fecFact);
			PdfPCell cel2fil2 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil2 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil2 = new PdfPCell(fraseFecFact);
			cel4fil2.setPaddingTop(50);
			cel2fil2.setBorder(Rectangle.NO_BORDER);
			cel3fil2.setBorder(Rectangle.NO_BORDER);
			cel4fil2.setBorder(Rectangle.NO_BORDER);

			// FILA 3 (NOMBRE SANTANDER)
			Chunk nombreSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			Phrase fraseNombreSantander = new Phrase(nombreSantander);
			PdfPCell cel2fil3 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil3 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil3 = new PdfPCell(fraseNombreSantander);
			cel2fil3.setBorder(Rectangle.NO_BORDER);
			cel3fil3.setBorder(Rectangle.NO_BORDER);
			cel4fil3.setBorder(Rectangle.NO_BORDER);

			// FILA 4 (CIF)
			Chunk cif = new Chunk("CIF: " + mapConstantes.get(Constantes.CLAVE_CIF_SANTANDER), fontNormal);
			Phrase frasecif = new Phrase(cif);
			PdfPCell cel2fil4 = new PdfPCell(new Paragraph(""));
			PdfPCell cel3fil4 = new PdfPCell(new Paragraph(""));
			PdfPCell cel4fil4 = new PdfPCell(frasecif);
			cel2fil4.setBorder(Rectangle.NO_BORDER);
			cel3fil4.setBorder(Rectangle.NO_BORDER);
			cel4fil4.setBorder(Rectangle.NO_BORDER);

			// FILA DIRECCIÓN SANTANDER
			Chunk direccionSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_DIRECCION_SANTANDER), fontNormal);
			Phrase fraseDireccionSantander = new Phrase(direccionSantander);
			PdfPCell cel2DireccionSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3DireccionSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4DireccionSantander = new PdfPCell(fraseDireccionSantander);
			cel2DireccionSantander.setBorder(Rectangle.NO_BORDER);
			cel3DireccionSantander.setBorder(Rectangle.NO_BORDER);
			cel4DireccionSantander.setBorder(Rectangle.NO_BORDER);

			// FILA PROVINCIA SANTANDER
			Chunk provinciaSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_PROVINCIA_SANTANDER), fontNormal);
			Phrase fraseProvinciaSantander = new Phrase(provinciaSantander);
			PdfPCell cel2ProvinciaSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3ProvinciaSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4ProvinciaSantander = new PdfPCell(fraseProvinciaSantander);
			cel2ProvinciaSantander.setBorder(Rectangle.NO_BORDER);
			cel3ProvinciaSantander.setBorder(Rectangle.NO_BORDER);
			cel4ProvinciaSantander.setBorder(Rectangle.NO_BORDER);

			// FILA PAIS SANTANDER
			Chunk paisSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_PAIS_SANTANDER), fontNormal);
			Phrase frasePaisSantander = new Phrase(paisSantander);
			PdfPCell cel2PaisSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel3PaisSantander = new PdfPCell(new Paragraph(""));
			PdfPCell cel4PaisSantander = new PdfPCell(frasePaisSantander);
			cel2PaisSantander.setBorder(Rectangle.NO_BORDER);
			cel3PaisSantander.setBorder(Rectangle.NO_BORDER);
			cel4PaisSantander.setBorder(Rectangle.NO_BORDER);

			// FILA 5 (EMPRESA CONTRAPARTE)
			Chunk empContr = new Chunk(objPdf[1].toString().trim(), fontNormal);
			empContr.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 0f, 1f);
			Phrase fraseEmpContr = new Phrase(empContr);
			PdfPCell cel4fil5 = new PdfPCell(fraseEmpContr);
			cel4fil5.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel4fil5.setColspan(3);
			cel4fil5.setPaddingTop(30);
			cel4fil5.setBorder(Rectangle.NO_BORDER);

			// CIF EMPRESA
			Chunk cifEmprContr = new Chunk("CIF: " + objPdf[10].toString().trim(), fontNormal);
			Phrase fraseCifEmprContr = new Phrase(cifEmprContr);
			PdfPCell cel4cifEmprContr = new PdfPCell(fraseCifEmprContr);
			cel4cifEmprContr.setColspan(3);
			cel4cifEmprContr.setBorder(Rectangle.NO_BORDER);

			// FILA 6 (DIRECCION EMPRESA CONTRAPARTE)
			Chunk dirEmprContr = new Chunk(objPdf[2].toString().trim(), fontNormal);
			Phrase fraseDirEmprContr = new Phrase(dirEmprContr);
			PdfPCell cel4fil6 = new PdfPCell(fraseDirEmprContr);
			cel4fil6.setColspan(3);
			cel4fil6.setBorder(Rectangle.NO_BORDER);

			// FILA 7 (PROVINCIA EMPRESA CONTRAPARTE)
			Chunk provEmpContr = new Chunk(objPdf[3].toString().trim(), fontNormal);
			Phrase fraseProvEmpContr = new Phrase(provEmpContr);
			PdfPCell cel4fil7 = new PdfPCell(fraseProvEmpContr);
			cel4fil7.setColspan(3);
			cel4fil7.setBorder(Rectangle.NO_BORDER);

			// FILA 8 (PAIS EMPRESA CONTRAPARTE)
			Chunk paisEmpContr = new Chunk(objPdf[4].toString().trim(), fontNormal);
			Phrase frasePaisEmpContr = new Phrase(paisEmpContr);
			PdfPCell cel4fil8 = new PdfPCell(frasePaisEmpContr);
			cel4fil8.setColspan(3);
			cel4fil8.setBorder(Rectangle.NO_BORDER);

			// FILA 9 (NÚMERO DE FACTURA)
			Chunk labelNumFact = new Chunk("FACTURA RECTIFICATIVA NUMERO", fontNormal);
			Phrase fraseLabelNumFact = new Phrase(labelNumFact);
			PdfPCell cel3fil9 = new PdfPCell();
			cel3fil9.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cel3fil9.setPaddingTop(40);
			cel3fil9.setBorder(Rectangle.NO_BORDER);
			Chunk numFact = new Chunk("SVRGIVA-" + objPdf[5].toString().trim(), fontNormal);
			Phrase fraseNumFact = new Phrase(numFact);
			PdfPCell cel4fil9 = new PdfPCell();
			Paragraph fraseCombinada = new Paragraph();
			fraseCombinada.add(fraseLabelNumFact);
			fraseCombinada.add(new Phrase("    "));
			fraseCombinada.add(fraseNumFact);
			fraseCombinada.setAlignment(Element.ALIGN_RIGHT);
			cel4fil9.setColspan(2);
			cel4fil9.addElement(fraseCombinada);
			cel4fil9.setPaddingTop(40);
			cel4fil9.setBorder(Rectangle.NO_BORDER);

			// FILA 10 (COLUMNAS DATOS FACTURA)
			Chunk labelDatosFactura = new Chunk("CONCEPTO", fontNormal);
			labelDatosFactura.setBackground(new BaseColor(166, 172, 176), 96f, 1f, 140f, 1f);
			Phrase fraseLabelDatosFactura = new Phrase(labelDatosFactura);
			PdfPCell cel2fil10 = new PdfPCell(fraseLabelDatosFactura);
			cel2fil10.setColspan(2);
			cel2fil10.setPaddingTop(10);
			cel2fil10.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil10.setBorder(Rectangle.NO_BORDER);
			Chunk labelDatosFactura2 = new Chunk("IMPORTE TOTAL", fontNormal);
			labelDatosFactura2.setBackground(new BaseColor(166, 172, 176), 11f, 1f, 70f, 1f);
			Phrase fraseLabelDatosFactura2 = new Phrase(labelDatosFactura2);
			PdfPCell cel4fil10 = new PdfPCell(fraseLabelDatosFactura2);
			cel4fil10.setPaddingTop(10);
			cel4fil10.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil10.setBorder(Rectangle.NO_BORDER);

			// FILA 11 (DATOS FACTURA)
			Chunk valorDatosFactura = new Chunk();
			String datFact = objPdf[6].toString().trim();
			if (datFact.length() > 500) {
				datFact = datFact.substring(0, 499);
				valorDatosFactura = new Chunk(datFact, fontNormal);
			} else {
				valorDatosFactura = new Chunk(pad(datFact + " ", 500, '\u00a0'), fontNormal);
			}
			Phrase fraseValorDatosFactura = new Phrase(valorDatosFactura);
			PdfPCell cel2fil11 = new PdfPCell(fraseValorDatosFactura);
			cel2fil11.setColspan(2);
			cel2fil11.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel2fil11.setBorder(Rectangle.NO_BORDER);
			Chunk valorDatosFactura2 = new Chunk(((BigDecimal) objPdf[8]).setScale(2, RoundingMode.HALF_UP) + " €",
			    fontNormal);
			Phrase fraseValorDatosFactura2 = new Phrase(valorDatosFactura2);
			PdfPCell cel4fil11 = new PdfPCell(fraseValorDatosFactura2);
			cel4fil11.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil11.setBorder(Rectangle.NO_BORDER);

			// FILA BASE IMPONIBLE ANTERIOR
			Chunk baseImponibleAnterior = new Chunk(mapConstantes.get(Constantes.CLAVE_BASE_IMPONIBLE_ANTERIOR), fontNormal);
			Phrase fraseBaseImponibleAnterior = new Phrase(baseImponibleAnterior);
			PdfPCell celBaseImponibleAnterior = new PdfPCell(fraseBaseImponibleAnterior);
			celBaseImponibleAnterior.setColspan(2);
			celBaseImponibleAnterior.setBorder(Rectangle.NO_BORDER);
			Chunk labelImpBaseImponibleAnterior = new Chunk(((BigDecimal) objPdf[13]).setScale(2, RoundingMode.HALF_UP) + "",
			    fontNormal);
			Phrase fraseImpBaseImponibleAnterior = new Phrase(labelImpBaseImponibleAnterior);
			PdfPCell celImpBaseImponibleAnterior = new PdfPCell(fraseImpBaseImponibleAnterior);
			celImpBaseImponibleAnterior.setHorizontalAlignment(Element.ALIGN_RIGHT);
			celImpBaseImponibleAnterior.setBorder(Rectangle.NO_BORDER);

			// FILA BASE IMPONIBLE ACTUAL
			Chunk baseImponibleActual = new Chunk(
			    mapConstantes.get(Constantes.CLAVE_BASE_IMPONIBLE_ACTUAL) + " " + Calendar.getInstance().get(Calendar.YEAR),
			    fontNormal);
			Phrase fraseBaseImponibleActual = new Phrase(baseImponibleActual);
			PdfPCell celBaseImponibleActual = new PdfPCell(fraseBaseImponibleActual);
			celBaseImponibleActual.setBorder(Rectangle.NO_BORDER);
			celBaseImponibleActual.setColspan(2);
			Chunk labelImpBaseImponibleActual = new Chunk(((BigDecimal) objPdf[14]).setScale(2, RoundingMode.HALF_UP) + "",
			    fontNormal);
			Phrase fraseImpBaseImponibleActual = new Phrase(labelImpBaseImponibleActual);
			PdfPCell celImpBaseImponibleActual = new PdfPCell(fraseImpBaseImponibleActual);
			celImpBaseImponibleActual.setHorizontalAlignment(Element.ALIGN_RIGHT);
			celImpBaseImponibleActual.setBorder(Rectangle.NO_BORDER);

			// FILA 12 (COLUMNAS CANTIDADES FACTURA)
			Chunk labelTaxBase = new Chunk(
			    "(*)BASE IMPONIBLE IVA GRUPO (" + ((BigDecimal) objPdf[12]).setScale(2, RoundingMode.HALF_UP) + "%)",
			    fontNormal);
			labelTaxBase.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 40f, 1f);
			Phrase fraseLabelTaxBase = new Phrase(labelTaxBase);
			PdfPCell cel2fil12 = new PdfPCell(fraseLabelTaxBase);
			cel2fil12.setColspan(2);
			cel2fil12.setPaddingTop(30);
			cel2fil12.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel2fil12.setBorder(Rectangle.NO_BORDER);
			// Chunk labelTax = new Chunk("IVA(21%)", fontNormal);
			// labelTax.setBackground(new BaseColor(166, 172, 176), 51f, 1f, 50f, 1f);
			// Phrase fraseLabelTax = new Phrase(labelTax);
			// PdfPCell cel3fil12 = new PdfPCell(fraseLabelTax);
			// cel3fil12.setPaddingTop(40);
			// cel3fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			// cel3fil12.setBorder(Rectangle.NO_BORDER);
			Chunk labelTotalAmount = new Chunk("IVA(21%)               IMPORTE TOTAL", fontNormal);
			labelTotalAmount.setBackground(new BaseColor(166, 172, 176), 60f, 1f, 30f, 1f);
			Phrase fraseLabelTotalAmount = new Phrase(labelTotalAmount);
			PdfPCell cel4fil12 = new PdfPCell(fraseLabelTotalAmount);
			cel4fil12.setPaddingTop(30);
			cel4fil12.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel4fil12.setBorder(Rectangle.NO_BORDER);

			// FILA 13 (CANTIDADES FACTURA)
			Chunk valorTaxBase = new Chunk(((BigDecimal) objPdf[8]).setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			Phrase fraseValorTaxBase = new Phrase(valorTaxBase);
			PdfPCell cel2fil13 = new PdfPCell(fraseValorTaxBase);
			cel2fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			cel2fil13.setBorder(Rectangle.NO_BORDER);
			BigDecimal suma = (BigDecimal) objPdf[8];
			suma = suma.add((BigDecimal) objPdf[9]);
			Chunk valorTax = new Chunk("                                                   "
			    + ((BigDecimal) objPdf[9]).setScale(2, RoundingMode.HALF_UP) + " €                    "
			    + suma.setScale(2, RoundingMode.HALF_UP) + " €", fontNormal);
			Phrase fraseValorTax = new Phrase(valorTax);
			PdfPCell cel3fil13 = new PdfPCell(fraseValorTax);
			cel3fil13.setColspan(2);
			cel3fil13.setHorizontalAlignment(Element.ALIGN_LEFT);
			cel3fil13.setBorder(Rectangle.NO_BORDER);
			// Chunk valorTotalAmount = new Chunk(, fontNormal);
			// Phrase fraseValorTotalAmount = new Phrase(valorTotalAmount);
			// PdfPCell cel4fil13 = new PdfPCell(fraseValorTotalAmount);
			// cel4fil13.setHorizontalAlignment(Element.ALIGN_CENTER);
			// cel4fil13.setBorder(Rectangle.NO_BORDER);

			// FILA 14 (REFERENCIA FACTURA)
			Chunk facturasRectificadas = new Chunk("FACTURAS RECTIFICADAS", fontNormal);
			facturasRectificadas.setBackground(new BaseColor(166, 172, 176), 155f, 1f, 170f, 1f);
			Phrase fraseFacturasRectificadas = new Phrase(facturasRectificadas);
			PdfPCell cel2fil14 = new PdfPCell(fraseFacturasRectificadas);
			cel2fil14.setColspan(3);
			cel2fil14.setPaddingTop(20);
			cel2fil14.setBorder(Rectangle.NO_BORDER);
			cel2fil14.setHorizontalAlignment(Element.ALIGN_CENTER);

			// NUMEROS DE FACTURAS RECTIFICADAS
			List<String> listaFacturasRectificativas = new ArrayList<String>();
			if (null != objPdf[15] && !objPdf[15].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[15].toString().trim());
			}
			if (null != objPdf[16] && !objPdf[16].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[16].toString().trim());
			}
			if (null != objPdf[17] && !objPdf[17].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[17].toString().trim());
			}
			if (null != objPdf[18] && !objPdf[18].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[18].toString().trim());
			}
			if (null != objPdf[19] && !objPdf[19].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[19].toString().trim());
			}
			if (null != objPdf[20] && !objPdf[20].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[20].toString().trim());
			}
			if (null != objPdf[21] && !objPdf[21].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[21].toString().trim());
			}
			if (null != objPdf[22] && !objPdf[22].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[22].toString().trim());
			}
			if (null != objPdf[23] && !objPdf[23].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[23].toString().trim());
			}
			if (null != objPdf[24] && !objPdf[24].equals("")) {
				listaFacturasRectificativas.add("SVGIVA-" + objPdf[24].toString().trim());
			}
			String cadenaFacturasRectificativas = listaFacturasRectificativas.toString();
			cadenaFacturasRectificativas = cadenaFacturasRectificativas.replace("[", "");
			cadenaFacturasRectificativas = cadenaFacturasRectificativas.replace("]", "");
			Chunk facturasRectificativas = new Chunk();

			if (cadenaFacturasRectificativas.length() > 160) {
				cadenaFacturasRectificativas = cadenaFacturasRectificativas.substring(0, 159);
				facturasRectificativas = new Chunk(cadenaFacturasRectificativas, fontNormal);
			} else {
				facturasRectificativas = new Chunk(pad(cadenaFacturasRectificativas + " ", 160, '\u00a0'), fontNormal);
			}
			Phrase fraseFacturasRectificativas = new Phrase(facturasRectificativas);
			PdfPCell celFacturasRectificativas = new PdfPCell(fraseFacturasRectificativas);
			celFacturasRectificativas.setColspan(3);
			celFacturasRectificativas.setHorizontalAlignment(Element.ALIGN_LEFT);
			celFacturasRectificativas.setBorder(Rectangle.NO_BORDER);

			// FILA 15 (DESCRIPCION SANTANDER)
			Chunk descSantander = new Chunk(mapConstantes.get(Constantes.CLAVE_DESCRIPCION_SANTANDER), fontNormal);
			Phrase frasedescSantander = new Phrase(descSantander);
			PdfPCell cel2fil15 = new PdfPCell(frasedescSantander);
			cel2fil15.setColspan(3);
			cel2fil15.setPaddingTop(50);
			cel2fil15.setBorder(Rectangle.NO_BORDER);

			// FILA 16 (NUMERO DE CUENTA)
			Chunk labelBankAccount = new Chunk(Constantes.CLAVE_BANK_ACCOUNT, fontNormal);
			labelBankAccount.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 50f, 1f);
			Phrase fraseLabelBankAccount = new Phrase(labelBankAccount);
			PdfPCell cel2fil16 = new PdfPCell(fraseLabelBankAccount);
			cel2fil16.setBorder(Rectangle.NO_BORDER);
			Chunk bankAccount = new Chunk(mapConstantes.get(Constantes.CLAVE_BANK_ACCOUNT), fontNormal);
			bankAccount.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 103f, 1f);
			Phrase fraseBankAccount = new Phrase(bankAccount);
			PdfPCell cel3fil16 = new PdfPCell(fraseBankAccount);
			cel3fil16.setColspan(2);
			cel3fil16.setBorder(Rectangle.NO_BORDER);

			// FILA 17 (IBAN)
			Chunk labelIban = new Chunk(Constantes.CLAVE_IBAN, fontNormal);
			Phrase fraseLabelIban = new Phrase(labelIban);
			PdfPCell cel2fil17 = new PdfPCell(fraseLabelIban);
			cel2fil17.setBorder(Rectangle.NO_BORDER);
			Chunk valorIban = new Chunk(mapConstantes.get(Constantes.CLAVE_IBAN), fontNormal);
			Phrase fraseValorIban = new Phrase(valorIban);
			PdfPCell cel3fil17 = new PdfPCell(fraseValorIban);
			cel3fil17.setColspan(2);
			cel3fil17.setBorder(Rectangle.NO_BORDER);

			// FILA 18 (BIC CODE)
			Chunk labelBicCode = new Chunk(Constantes.CLAVE_BIC_CODE, fontNormal);
			labelBicCode.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 53f, 1f);
			Phrase fraseLabelBicCode = new Phrase(labelBicCode);
			PdfPCell cel2fil18 = new PdfPCell(fraseLabelBicCode);
			cel2fil18.setBorder(Rectangle.NO_BORDER);
			Chunk valorBicCode = new Chunk(mapConstantes.get(Constantes.CLAVE_BIC_CODE), fontNormal);
			valorBicCode.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 170f, 1f);
			Phrase fraseValorBicCode = new Phrase(valorBicCode);
			PdfPCell cel3fil18 = new PdfPCell(fraseValorBicCode);
			cel3fil18.setColspan(2);
			cel3fil18.setBorder(Rectangle.NO_BORDER);

			// FILA 19 (BENEFICIARY BIC)
			Chunk labelBeneficiaryBic = new Chunk(Constantes.CLAVE_BENEFICIARY_BIC, fontNormal);
			Phrase fraseLabelBeneficiaryBic = new Phrase(labelBeneficiaryBic);
			PdfPCell cel2fil19 = new PdfPCell(fraseLabelBeneficiaryBic);
			cel2fil19.setBorder(Rectangle.NO_BORDER);
			Chunk valorBeneficiaryBic = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY_BIC), fontNormal);
			Phrase fraseValorBeneficiaryBic = new Phrase(valorBeneficiaryBic);
			PdfPCell cel3fil19 = new PdfPCell(fraseValorBeneficiaryBic);
			cel3fil19.setColspan(2);
			cel3fil19.setBorder(Rectangle.NO_BORDER);

			// FILA 20 (BENEFICIARY)
			Chunk labelBeneficiary = new Chunk(Constantes.CLAVE_BENEFICIARY, fontNormal);
			labelBeneficiary.setBackground(new BaseColor(166, 172, 176), 1f, 1f, 53f, 1f);
			Phrase fraseLabelBeneficiary = new Phrase(labelBeneficiary);
			PdfPCell cel2fil20 = new PdfPCell(fraseLabelBeneficiary);
			cel2fil20.setBorder(Rectangle.NO_BORDER);
			Chunk valorBeneficiary = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			valorBeneficiary.setBackground(new BaseColor(166, 172, 176), 36f, 1f, 60f, 1f);
			Phrase fraseValorBeneficiary = new Phrase(valorBeneficiary);
			PdfPCell cel3fil20 = new PdfPCell(fraseValorBeneficiary);
			cel3fil20.setColspan(2);
			cel3fil20.setBorder(Rectangle.NO_BORDER);

			// FILA 21 (FOOTER)
			Chunk santanderSAU = new Chunk(mapConstantes.get(Constantes.CLAVE_BENEFICIARY), fontNormal);
			Phrase fraseSantanderSAU = new Phrase(santanderSAU);
			Image logoDowJones = Image.getInstance(context.getResource("file:" + dowJonesName).getURL());
			logoDowJones.scalePercent(25);
			logoDowJones.setAbsolutePosition(320f, 40f);
			document.add(logoDowJones);
			Image logoFTSE = Image.getInstance(context.getResource("file:" + ftse4Name).getURL());
			logoFTSE.scaleAbsolute(60, 50);
			logoFTSE.setAbsolutePosition(460f, 55f);
			document.add(logoFTSE);
			PdfPCell cel2fil21 = new PdfPCell();
			cel2fil21.addElement(fraseSantanderSAU);
			cel2fil21.setColspan(3);
			cel2fil21.setPaddingTop(40);
			cel2fil21.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cel2fil21.setBorder(Rectangle.NO_BORDER);

			table.addCell(cel1fil1);
			table.addCell(cel2fil2);
			table.addCell(cel3fil2);
			table.addCell(cel4fil2);
			table.addCell(cel2fil3);
			table.addCell(cel3fil3);
			table.addCell(cel4fil3);
			table.addCell(cel2fil4);
			table.addCell(cel3fil4);
			table.addCell(cel4fil4);
			table.addCell(cel2DireccionSantander);
			table.addCell(cel3DireccionSantander);
			table.addCell(cel4DireccionSantander);
			table.addCell(cel2ProvinciaSantander);
			table.addCell(cel3ProvinciaSantander);
			table.addCell(cel4ProvinciaSantander);
			table.addCell(cel2PaisSantander);
			table.addCell(cel3PaisSantander);
			table.addCell(cel4PaisSantander);
			table.addCell(cel4fil5);
			table.addCell(cel4cifEmprContr);
			table.addCell(cel4fil6);
			table.addCell(cel4fil7);
			table.addCell(cel4fil8);
			table.addCell(cel3fil9);
			table.addCell(cel4fil9);
			table.addCell(cel2fil10);
			table.addCell(cel4fil10);
			table.addCell(cel2fil11);
			table.addCell(cel4fil11);
			table.addCell(celBaseImponibleAnterior);
			table.addCell(celImpBaseImponibleAnterior);
			table.addCell(celBaseImponibleActual);
			table.addCell(celImpBaseImponibleActual);
			table.addCell(cel2fil12);
			// table.addCell(cel3fil12);
			table.addCell(cel4fil12);
			table.addCell(cel2fil13);
			table.addCell(cel3fil13);
			// table.addCell(cel4fil13);
			table.addCell(cel2fil14);
			table.addCell(celFacturasRectificativas);
			table.addCell(cel2fil15);
			table.addCell(cel2fil16);
			table.addCell(cel3fil16);
			table.addCell(cel2fil17);
			table.addCell(cel3fil17);
			table.addCell(cel2fil18);
			table.addCell(cel3fil18);
			table.addCell(cel2fil19);
			table.addCell(cel3fil19);
			table.addCell(cel2fil20);
			table.addCell(cel3fil20);
			table.addCell(cel2fil21);

			document.add(table);
			document.close();
			writer.close();

			// Formamos el nombre del fichero
			String numeroFactura = objPdf[5].toString().trim();
			String nombreFichero = defineNombreFichero(mapConstantes.get(Constantes.CLAVE_NOMBRE_RECTIFICATIVA_GRUPO_IVA),
			    numeroFactura);
			result.put("nombreFichero", nombreFichero);
			result.put("file", baos.toByteArray());
			return result;
		} catch (RuntimeException e) {
			LOG.error("[generarPdfFacturasRectificativasGrupoIVA] Error no controlado al generar documento PDF", e);
			throw e;
		} catch (DocumentException e) {
			LOG.error("[generarPdfFacturasRectificativasGrupoIVA] Error de generación de PDF", e);
			throw new SIBBACBusinessException("Error de generación de PDF", e);
		} catch (IOException ioex) {
			LOG.error("[generarPdfFacturasRectificativasGrupoIVA] Error de Entrada/Salida al generar PDF", ioex);
			throw new SIBBACBusinessException("Error de Entrada/Salida al generar PDF", ioex);
		}
	}

	private String pad(String str, int size, char padChar) {
		StringBuffer padded = new StringBuffer(str);
		while (padded.length() < size) {
			padded.append(padChar);
		}
		return padded.toString();
	}

	private String defineNombreFichero(String prefijo, String numeroFactura) {
		final SimpleDateFormat formatteryyyymmdd;
		final Date date;
		final String yyyymmdd;

		date = new Date();
		formatteryyyymmdd = new SimpleDateFormat("yyyyMMdd_HHmmss");
		yyyymmdd = formatteryyyymmdd.format(date);
		return String.format("%s_%s_%s.pdf", prefijo, numeroFactura, yyyymmdd);
	}
}
