package sibbac.business.interfacesemitidas.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAOImpl;
import sibbac.business.interfacesemitidas.database.dao.Tmct0InterfacesEmitidasConstDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0RectificativasManualesDAOImpl;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;
import sibbac.business.interfacesemitidas.dto.pantallas.AsientosContablesFilterDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.BusquedaAsientosContablesDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.DetalleAsientoContableDTO;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

/**
 * Business object de Tmct0FacturasManualesBo.
 */
@Service
public class Tmct0FacturasManualesBo extends AbstractBo<Tmct0FacturasManuales, BigInteger, Tmct0FacturasManualesDAO> {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0FacturasManualesBo.class);

  @Autowired
  private Tmct0FacturasManualesDAOImpl tmct0FacturasManualesDAOImpl;

  @Autowired
  private Tmct0RectificativasManualesDAO tmct0RectificativasManualesDAO;

  @Autowired
  private Tmct0RectificativasManualesDAOImpl tmct0RectificativasManualesDAOImpl;

  @Autowired
  private Tmct0estadoDao tmct0estadoDao;

  @Autowired
  private Tmct0InterfacesEmitidasConstDao interfacesEmitidasConstDao;

  /**
   * Retorna el catalogo de alias.
   * 
   * @return List<SelectDTO>
   * @throws SIBBACBusinessException
   */
  public List<SelectDTO> getCatalogoAlias() throws SIBBACBusinessException {
    return this.tmct0FacturasManualesDAOImpl.getCatalogoAlias();
  }

  /**
   * Retorna el catalogo de tipos de factura.
   * 
   * @return List<SelectDTO>
   * @throws SIBBACBusinessException
   */
  public List<SelectDTO> getCatalogoTiposFactura() throws SIBBACBusinessException {
    return this.tmct0FacturasManualesDAOImpl.getCatalogoTiposFactura();
  }

  /**
   * Utilizado para la busqueda de asientos contables.
   * 
   * @param asientosContablesFilterDTO asientosContablesFilterDTO
   * @return List<BusquedaAsientosContablesDTO> List<BusquedaAsientosContablesDTO>
   * @throws SIBBACBusinessException SIBBACBusinessException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public List<BusquedaAsientosContablesDTO> findAsientosContables(AsientosContablesFilterDTO asientosContablesFilterDTO) throws SIBBACBusinessException {
    List<BusquedaAsientosContablesDTO> listaAsientosContables = new ArrayList<BusquedaAsientosContablesDTO>();
    // Recuperamos el ID ESTADO que pertenece al literal FACTURA ANULADA
    Tmct0estado estadoFactAnulada = tmct0estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_ANULADA).getValor());
    // Recuperamos el ID ESTADO que pertenece al literal FACTURA CONTABILIZADA
    Tmct0estado estadoFActCont = tmct0estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_CONTABILIZADA).getValor());
    
    listaAsientosContables.addAll(this.tmct0FacturasManualesDAOImpl.findAsientosContablesFactManuales(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL,
    																								  estadoFactAnulada.getIdestado(),estadoFActCont.getIdestado(),asientosContablesFilterDTO));
    listaAsientosContables.addAll(this.tmct0FacturasManualesDAOImpl.findAsientosContablesFactManuales(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF,
    																								  estadoFactAnulada.getIdestado(),estadoFActCont.getIdestado(),asientosContablesFilterDTO));
    listaAsientosContables.addAll(this.tmct0FacturasManualesDAOImpl.findAsientosContablesFactManuales(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL_BAJA,
    																								  estadoFactAnulada.getIdestado(),estadoFActCont.getIdestado(),asientosContablesFilterDTO));
    listaAsientosContables.addAll(this.tmct0FacturasManualesDAOImpl.findAsientosContablesFactManuales(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF_BAJA,
    																								  estadoFactAnulada.getIdestado(),estadoFActCont.getIdestado(),asientosContablesFilterDTO));
    return listaAsientosContables;
  }

  /**
   * Retorna datos de la modal de asientos contables.
   *
   * @param id id
   * @return DetalleAsientoContableDTO DetalleAsientoContableDTO
   * @throws SIBBACBusinessException SIBBACBusinessException
   */
  public DetalleAsientoContableDTO getDetalleAsientoContableModalById(String id) throws SIBBACBusinessException {
    return this.tmct0FacturasManualesDAOImpl.getDetalleAsientoContableModalById(id);
  }

  /**
   * Anula datos de factura manual o rectificada segun corresponda.
   *
   * @param idFacturaManual idFacturaManual
   * @param idRectifManual idRectifManual
   * @throws SIBBACBusinessException SIBBACBusinessException
   */
  @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
  public void anularFacturaManualRectif(String entidadFactura, BigInteger id) throws SIBBACBusinessException {
  	String message;
    Tmct0estado estadoAnulado = tmct0estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_ANULADA)
                                                                                      .getValor());
    Tmct0estado estadoPteContabilizar = tmct0estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_PDTE_CONTABILIZAR)
                                                                                            .getValor());

    // Se encuentran detalles del asiento relacionados a la factura.
    if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL.equals(entidadFactura)) {
      // Caso factura manual.
      Tmct0FacturasManuales facturaManual = null;

      if (estadoAnulado != null) {
        // Cuando se da de baja la factura se cambia estado a '604' (Anulada) (Buscar por literal)
        facturaManual = this.dao.findOne(id);
        facturaManual.setEstado(estadoAnulado);
        this.dao.saveAndFlush(facturaManual);

        // Se genera una factura de baja nueva, estado enviado a 0 y idestado 'factura contabilizada' (por literal)
        // de ahí cambiarle el importe a la factura (negado) y el nro. factura agrego un 9 por delante
        if (estadoPteContabilizar != null) {
          this.tmct0FacturasManualesDAOImpl.createEntityBajaManual(facturaManual, estadoPteContabilizar);
        } else {
        	message = "No se ha encontrado estado buscado en TMCT0ESTADO";
        	LOG.error("[anularFacturaManualRectif] {}", message);
          throw new SIBBACBusinessException(message);
        }
      } else {
      	message = "No se ha encontrado estado buscado en TMCT0ESTADO";
      	LOG.error("[anularFacturaManualRectif] {}", message);
        throw new SIBBACBusinessException(message);
      }
    } else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF.equals(entidadFactura)) {
      // Caso factura rectificada manual.
      Tmct0RectificativaManual facturaRectif = null;

      if (estadoAnulado != null) {
        // Cuando se da de baja la factura se cambia estado a '604' (Anulada) (Buscar por literal)
        facturaRectif = this.tmct0RectificativasManualesDAO.findOne(id);
        facturaRectif.setEstado(estadoAnulado);
        this.tmct0RectificativasManualesDAO.saveAndFlush(facturaRectif);

        // Se genera una factura de baja nueva, estado enviado a 0 y idestado 'factura contabilizada' (por literal)
        // de ahí cambiarle el importe a la factura (negado) y el nro. factura agrego un 9 por delante
        if (estadoPteContabilizar != null) {
          this.tmct0RectificativasManualesDAOImpl.createEntityBajaRectifManual(facturaRectif, estadoPteContabilizar);
        } else {
        	message = "No se ha encontrado estado buscado en TMCT0ESTADO";
        	LOG.error("[anularFacturaManualRectif] {}", message);
          throw new SIBBACBusinessException(message);
        }
      } else {
      	message = "No se ha encontrado estado buscado en TMCT0ESTADO";
      	LOG.error("[anularFacturaManualRectif] {}", message);
        throw new SIBBACBusinessException(message);
      }
    }
  }
}
