package sibbac.business.interfacesemitidas.dto;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Datos de factura. Forma parte del bloque 01 de los datos de factura.
 * 
 * @author adrian.prause
 *
 */
public class DesglosePorFacturaDTO {

	private String sujetasExentasCausa;
	private String sujetasNoExentasTipoNoExenta;
	private String noSujetasTipoNoSujetas;

	@Override
	public String toString() {
		try {
			return StringHelper.padRight(sujetasExentasCausa, 2)
					+ StringHelper.padRight(sujetasNoExentasTipoNoExenta, 2)
					+ StringHelper.padRight(noSujetasTipoNoSujetas, 2);
		} catch (Exception e) {
			return "";
		}
	}

	public String getSujetasExentasCausa() {
		return sujetasExentasCausa;
	}

	public void setSujetasExentasCausa(String sujetasExentasCausa) {
		this.sujetasExentasCausa = sujetasExentasCausa;
	}

	public String getSujetasNoExentasTipoNoExenta() {
		return sujetasNoExentasTipoNoExenta;
	}

	public void setSujetasNoExentasTipoNoExenta(String sujetasNoExentasTipoNoExenta) {
		this.sujetasNoExentasTipoNoExenta = sujetasNoExentasTipoNoExenta;
	}

	public String getNoSujetasTipoNoSujetas() {
		return noSujetasTipoNoSujetas;
	}

	public void setNoSujetasTipoNoSujetas(String noSujetasTipoNoSujetas) {
		this.noSujetasTipoNoSujetas = noSujetasTipoNoSujetas;
	}

}
