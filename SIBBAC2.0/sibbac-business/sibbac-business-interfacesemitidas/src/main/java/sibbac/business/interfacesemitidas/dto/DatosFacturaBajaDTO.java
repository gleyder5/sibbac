package sibbac.business.interfacesemitidas.dto;

import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 01
 * 
 * @author adrian.prause
 *
 */
public class DatosFacturaBajaDTO {

	private String tipoRegistro;
	private Integer secuencialRegistro;
	private String claveUnica;
	private String otrosDatos;
	private IdentificacionFacturaDTO identificacion;

	@Override
	public String toString() {
		try {
			return StringHelper.padRight(tipoRegistro, 2) + StringHelper.padCerosLeft(secuencialRegistro, 7)
					+ StringHelper.padRight(claveUnica, 100) + identificacion + StringHelper.padRight(otrosDatos, 500);
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Integer getSecuencialRegistro() {
		return secuencialRegistro;
	}

	public void setSecuencialRegistro(Integer secuencialRegistro) {
		this.secuencialRegistro = secuencialRegistro;
	}

	public String getClaveUnica() {
		return claveUnica;
	}

	public void setClaveUnica(String claveUnica) {
		this.claveUnica = claveUnica;
	}

	public String getOtrosDatos() {
		return otrosDatos;
	}

	public void setOtrosDatos(String otrosDatos) {
		this.otrosDatos = otrosDatos;
	}

	public IdentificacionFacturaDTO getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(IdentificacionFacturaDTO identificacion) {
		this.identificacion = identificacion;
	}

}
