package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0InterfacesEmitidasConst;
import sibbac.business.interfacesemitidas.dto.ClienteDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.AsientosContablesFilterDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.BusquedaAsientosContablesDTO;
import sibbac.business.interfacesemitidas.dto.pantallas.DetalleAsientoContableDTO;
import sibbac.business.interfacesemitidas.helper.InterfacesEmitidasHelper;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;

@Repository
public class Tmct0FacturasManualesDAOImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0FacturasManualesDAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private AliasDao aliasDao;

	@Autowired
	private Tmct0InterfacesEmitidasConstDao interfacesEmitidasConstDao;

	public List<Tmct0FacturasManuales> consultarFacturasManuales(Tmct0FiltroFacturasManualesDTO filtroFacturasManuales) {

		LOG.info("INICIO - DAOIMPL - consultarFacturasManuales");

		List<Tmct0FacturasManuales> listaFacturasManuales = new ArrayList<Tmct0FacturasManuales>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT fm FROM Tmct0FacturasManuales fm WHERE 1=1");

			if (null != filtroFacturasManuales.getNbDocNumero()) {
				consulta.append(" AND fm.nbDocNumero = :nbDocNumero");
				parameters.put("nbDocNumero", filtroFacturasManuales.getNbDocNumero());
			}

			if (null != filtroFacturasManuales.getIdAlias()) {
				consulta.append(" AND fm.idAlias = :idAlias");
				parameters.put("idAlias", filtroFacturasManuales.getIdAlias());
			}

			if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta().equals("")))) {
				consulta.append(" AND fm.fhFechaCre BETWEEN :fechaDesde AND :fechaHasta");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			} else if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null == filtroFacturasManuales.getFechaHasta() || filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fm.fhFechaCre >= :fechaDesde");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
			} else if ((null == filtroFacturasManuales.getFechaDesde() || filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fm.fhFechaCre <= :fechaHasta");
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			}

			if (null != filtroFacturasManuales.getIdEstado()) {
				consulta.append(" AND fm.estado.idestado = :idEstado");
				parameters.put("idEstado", filtroFacturasManuales.getIdEstado());
			}

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0FacturasManuales> query = entityManager.createQuery(consulta.toString(),
					Tmct0FacturasManuales.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades factura manual con los datos recuperados.
			listaFacturasManuales = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFacturasManuales -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFacturasManuales");

		return listaFacturasManuales;
	}

	public List<Tmct0BajasManuales> consultarFacturasManualesBaja(Tmct0FiltroFacturasManualesDTO filtroFacturasManuales) {

		LOG.info("INICIO - DAOIMPL - consultarFacturasManualesBaja");

		List<Tmct0BajasManuales> listaFacturasManualesBaja = new ArrayList<Tmct0BajasManuales>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT fm FROM Tmct0BajasManuales fm WHERE 1=1");

			if (null != filtroFacturasManuales.getNbDocNumero()) {
				consulta.append(" AND fm.nbDocNumero = :nbDocNumero");
				parameters.put("nbDocNumero", filtroFacturasManuales.getNbDocNumero());
			}

			if (null != filtroFacturasManuales.getIdAlias()) {
				consulta.append(" AND fm.idAlias = :idAlias");
				parameters.put("idAlias", filtroFacturasManuales.getIdAlias());
			}

			if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta().equals("")))) {
				consulta.append(" AND fm.fhFechaCre BETWEEN :fechaDesde AND :fechaHasta");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			} else if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null == filtroFacturasManuales.getFechaHasta() || filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fm.fhFechaCre >= :fechaDesde");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
			} else if ((null == filtroFacturasManuales.getFechaDesde() || filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fm.fhFechaCre <= :fechaHasta");
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			}

			// MFG Se comenta esta parte porque estan en la tabla de bajas pero no tienen porque estar en estado de baja.
			// if (null != filtroFacturasManuales.getIdEstado()) {
			// consulta.append(" AND fm.estado.idestado = :idEstado");
			// parameters.put("idEstado", filtroFacturasManuales.getIdEstado());
			// }

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0BajasManuales> query = entityManager.createQuery(consulta.toString(), Tmct0BajasManuales.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades factura manual con los datos recuperados.
			listaFacturasManualesBaja = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFacturasManualesBaja -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFacturasManualesBaja");

		return listaFacturasManualesBaja;
	}

	public List<Tmct0FacturasManuales> consultarFacturasContabilizadas(Tmct0FiltroFacturasManualesDTO filtroFacturasManuales) {

		LOG.info("INICIO - DAOIMPL - consultarFacturasContabilizadas");

		List<Tmct0FacturasManuales> listaFacturasManuales = new ArrayList<Tmct0FacturasManuales>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder(
					"SELECT fm FROM Tmct0FacturasManuales fm WHERE fm.estado.idestado "
							+ "= (SELECT e.idestado FROM Tmct0estado e WHERE e.nombre = '"
							+ interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_CONTABILIZADA)
							.getValor() + "')");

			if (null != filtroFacturasManuales.getNbDocNumero()) {
				consulta.append(" AND fm.nbDocNumero = :nbDocNumero");
				parameters.put("nbDocNumero", filtroFacturasManuales.getNbDocNumero());
			}

			if (null != filtroFacturasManuales.getIdAlias()) {
				consulta.append(" AND fm.idAlias = :idAlias");
				parameters.put("idAlias", filtroFacturasManuales.getIdAlias());
			}

			if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta().equals("")))) {
				consulta.append(" AND fm.fhFechaCre BETWEEN :fechaDesde AND :fechaHasta");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			} else if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null == filtroFacturasManuales.getFechaHasta() || filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fm.fhFechaCre >= :fechaDesde");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
			} else if ((null == filtroFacturasManuales.getFechaDesde() || filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fm.fhFechaCre <= :fechaHasta");
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			}

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0FacturasManuales> query = entityManager.createQuery(consulta.toString(),
					Tmct0FacturasManuales.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades factura manual con los datos recuperados.
			listaFacturasManuales = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFacturasContabilizadas -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFacturasContabilizadas");

		return listaFacturasManuales;
	}

	@SuppressWarnings("unchecked")
	public ClienteDTO consultarClienteByAlias(BigInteger idAlias) {

		LOG.info("INICIO - DAOIMPL - consultarClienteByAlias");

		ClienteDTO cliente = new ClienteDTO();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			LOG.info("INICIO - DAOIMPL - Se va a realizar la llamada a BBDD a la consulta de cliente por idAlias");
			StringBuilder consulta = new StringBuilder(
					"SELECT addr.NBPROVIN, pais.NBPAIS, cli.TPRESIDE, cli.TPIDENTI, cli.CIF FROM TMCT0CLI cli "
							+ "JOIN TMCT0_ADDRESSES addr ON addr.IDCLIENT=cli.IDCLIENT "
							+ "JOIN TMCT0ALI ali ON ali.CDBROCLI=cli.CDBROCLI "
							+ "JOIN TMCT0PAISES pais ON pais.CDISOALF2=cli.CDPAIS "
							+ "WHERE ali.CDALIASS = :idAlias");

			parameters.put("idAlias", idAlias);

			Query query = null;
			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			query = entityManager.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			List<Object> resultList = null;

			resultList = query.getResultList();

			if (resultList.size() > 0) {
				Object[] objCliente = (Object[]) resultList.get(0);

				cliente.setProvincia(objCliente[0].toString());
				cliente.setPaisResidencia(objCliente[1].toString());
				if ((objCliente[2].toString()).equals("R")) {
					cliente.setTipoResidencia("Residencial");
				} else if ((objCliente[2].toString()).equals("NR")) {
					cliente.setTipoResidencia("No Residencial");
				}
				cliente.setTpdocumento(objCliente[3].toString());
				cliente.setNumdocumento(objCliente[4].toString());
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarFacturasManuales -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarClienteByAlias");

		return cliente;
	}

	/**
	 * Retorna el catalogo de alias.
	 * 
	 * @return List<SelectDTO>
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoAlias() throws SIBBACBusinessException {

		List<SelectDTO> aliases = new ArrayList<SelectDTO>();
		Query query;
		StringBuffer qlString = new StringBuffer(
				"SELECT FM.ID_ALIAS AS al1, FM.ID_ALIAS AS al2 FROM BSNBPSQL.TMCT0_FACTURA_MANUAL FM "
						+ "UNION SELECT RFM.ID_ALIAS AS al1, RFM.ID_ALIAS AS al2 FROM BSNBPSQL.TMCT0_FACTURA_MANUAL_RECTIFICATIVA RFM");
		try {
			query = entityManager.createNativeQuery(qlString.toString());
			List<?> resultList = query.getResultList();
			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					SelectDTO dto = new SelectDTO(String.valueOf(((Object[]) obj)[0]), String.valueOf(((Object[]) obj)[1]));
					aliases.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return aliases;
	}

	/**
	 * Retorna el catalogo de tipos de factura.
	 * 
	 * @return List<SelectDTO>
	 * @throws SIBBACBusinessException
	 */
	public List<SelectDTO> getCatalogoTiposFactura() throws SIBBACBusinessException {
		List<SelectDTO> tipos = new ArrayList<SelectDTO>();
		Query query;
		StringBuffer qlString = new StringBuffer("SELECT TF.ID, TF.VALOR FROM BSNBPSQL.TMCT0_TIPO_FACTURA TF");
		try {
			query = entityManager.createNativeQuery(qlString.toString());
			List<?> resultList = query.getResultList();
			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					SelectDTO dto = new SelectDTO(String.valueOf(((Object[]) obj)[0]), String.valueOf(((Object[]) obj)[1]));
					tipos.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return tipos;
	}

	/**
	 * Busqueda de asientos contables para facturas manuales.
	 * 
	 * @param prefijo prefijo: M es manuales - R es rectificativa
	 * @param asientosContablesFilterDTO asientosContablesFilterDTO
	 * @return List<BusquedaAsientosContablesDTO> List<BusquedaAsientosContablesDTO>
	 * @throws SIBBACBusinessException SIBBACBusinessException
	 */
	public List<BusquedaAsientosContablesDTO> findAsientosContablesFactManuales(String prefijo,
			Integer estadoFactAnulada, Integer estadoFactCont, AsientosContablesFilterDTO asientosContablesFilterDTO) throws SIBBACBusinessException {

		/**
		 * TODO - Cambiar las FKs de apunte detalle para que apunten a las nuevas tablas de factura manual y rectificada
		 * cuando se confirme.
		 */

		List<BusquedaAsientosContablesDTO> respuestaAsientosContables = new ArrayList<BusquedaAsientosContablesDTO>();
		Query query;
		StringBuffer qlString = new StringBuffer("SELECT ");
		Map<String, Object> parameters = new HashMap<String, Object>();

		if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL.equals(prefijo)) {
			qlString.append(" tcad.id, " + " est.nombre, " + " tca.importe, " + " tca.tipo_apunte, " + " tfm.nbDocNumero, "
					+ " tfm.nbComentarios, " + " tfm.idAlias, " + " tfm.impImpuesto, " + " tfm.fhFechaCre ");
			qlString.append(" FROM Tmct0ContaApunteDetalle tcad" + " LEFT JOIN tcad.tmct0ContaApuntes tca "
					+ " JOIN tcad.tmct0FacturasManuales tfm " + " LEFT JOIN tfm.estado est ");
		} else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF.equals(prefijo)) {
			qlString.append(" tcad.id, " + " est.nombre, " + " tca.importe, " + " tca.tipo_apunte, " + " tfm.nbDocNumero, "
					+ " tfm.nbComentarios, " + " tfm.idAlias, " + " tfm.impImpuesto, " + " tfm.fhFechaCre ");
			qlString.append(" FROM Tmct0ContaApunteDetalle tcad" + " LEFT JOIN tcad.tmct0ContaApuntes tca "
					+ " JOIN tcad.tmct0RectificadasManuales tfm " + " LEFT JOIN tfm.estado est ");
		} else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL_BAJA.equals(prefijo)) {
			qlString.append(" tcad.id, " + " est.nombre, " + " tca.importe, " + " tca.tipo_apunte, " + " tfm.nbDocNumero, "
					+ " tfm.nbComentarios, " + " tfm.idAlias, " + " tfm.impImpuesto, " + " tfm.fhFechaCre ");
			qlString.append(" FROM Tmct0ContaApunteDetalle tcad" + " LEFT JOIN tcad.tmct0ContaApuntes tca "
					+ " JOIN tcad.tmct0FacturasManualesBaja tfm " + " LEFT JOIN tfm.estado est ");
		} else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF_BAJA.equals(prefijo)) {
			qlString.append(" tcad.id, " + " est.nombre, " + " tca.importe, " + " tca.tipo_apunte, " + " tfm.nbDocNumero, "
					+ " tfm.nbComentarios, " + " tfm.idAlias, " + " tfm.impImpuesto, " + " tfm.fhFechaCre ");
			qlString.append(" FROM Tmct0ContaApunteDetalle tcad" + " LEFT JOIN tcad.tmct0ContaApuntes tca "
					+ " JOIN tcad.tmct0RectificadasManualesBaja tfm " + " LEFT JOIN tfm.estado est ");
		}

		qlString.append(" WHERE (tfm.estado.idestado = :estadoFactAnulada OR tfm.estado.idestado = :estadoFactCont) ");

		if (StringUtils.isNotBlank(asientosContablesFilterDTO.getCompania())) {
			qlString.append(" AND tfm.idAlias = :idAlias ");
		}

		if (StringUtils.isNotBlank(asientosContablesFilterDTO.getCompaniaNot())) {
			qlString.append(" AND tfm.idAlias <> :idAliasNot ");
		}

		if (asientosContablesFilterDTO.getFecha() != null) {
			qlString.append(" AND tfm.fhFechaCre BETWEEN :fhFechaCreInit AND :fhFechaCreEnd ");
		}

		if (asientosContablesFilterDTO.getIdTipoFactura() != null) {
			qlString.append(" AND tfm.tipoFactura.id = :idTipoFactura ");
		}

		if (asientosContablesFilterDTO.getIdTipoFacturaNot() != null) {
			qlString.append(" AND tfm.tipoFactura.id <> :idTipoFacturaNot ");
		}

		try {
			query = entityManager.createQuery(qlString.toString());

			parameters.put("estadoFactAnulada", estadoFactAnulada);
			parameters.put("estadoFactCont", estadoFactCont);

			if (StringUtils.isNotBlank(asientosContablesFilterDTO.getCompania())) {
				parameters.put("idAlias", new BigInteger(asientosContablesFilterDTO.getCompania()));
			}

			if (StringUtils.isNotBlank(asientosContablesFilterDTO.getCompaniaNot())) {
				parameters.put("idAliasNot", new BigInteger(asientosContablesFilterDTO.getCompaniaNot()));
			}

			if (asientosContablesFilterDTO.getFecha() != null) {
				parameters.put("fhFechaCreInit", DateHelper.getStartOfDay(asientosContablesFilterDTO.getFecha()));
				parameters.put("fhFechaCreEnd", DateHelper.getEndOfDay(asientosContablesFilterDTO.getFecha()));
			}

			if (asientosContablesFilterDTO.getIdTipoFactura() != null) {
				parameters.put("idTipoFactura", asientosContablesFilterDTO.getIdTipoFactura());
			}

			if (asientosContablesFilterDTO.getIdTipoFacturaNot() != null) {
				parameters.put("idTipoFacturaNot", asientosContablesFilterDTO.getIdTipoFacturaNot());
			}

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			List<?> resultList = query.getResultList();

			if (CollectionUtils.isNotEmpty(resultList)) {
				for (Object obj : resultList) {
					Object[] resultado = (Object[]) obj;
					String descrAli = this.aliasDao.findDescraliByCdaliass(String.valueOf((BigInteger) resultado[6]));
					BusquedaAsientosContablesDTO dto = BusquedaAsientosContablesDTO.convertObjectToBusqAsientoContableDTO((Object[]) obj,
							prefijo,
							descrAli);
					respuestaAsientosContables.add(dto);
				}
			}
		} catch (Exception ex) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
			throw new SIBBACBusinessException();
		}
		return respuestaAsientosContables;
	}

	@SuppressWarnings("unchecked")
	public List<Object> consultarEstadosDistintos() {

		LOG.info("INICIO - DAOIMPL - consultarEstadosDistintos");

		List<Object> listaEstados = new ArrayList<Object>();

		try {
			StringBuilder consulta = new StringBuilder(
					"SELECT E.IDESTADO, E.NOMBRE FROM TMCT0_INTERFACES_EMITIDAS_CONST IEC "
							+ "JOIN TMCT0ESTADO E ON E.NOMBRE=IEC.VALOR "
							+ "WHERE IEC.GRUPO='Tmct0estado'");

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			// Se forman todas las entidades de estado con los datos recuperados.
			listaEstados = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarEstadosDistintos -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarEstadosDistintos");

		return listaEstados;
	}

	/**
	 * Retorna datos de la modal de asientos contables.
	 *
	 * @param prefijoId prefijoId - R: Rectificadas - M: Manuales
	 * @return DetalleAsientoContableDTO DetalleAsientoContableDTO
	 * @throws SIBBACBusinessException SIBBACBusinessException
	 */
	public DetalleAsientoContableDTO getDetalleAsientoContableModalById(String prefijoId) throws SIBBACBusinessException {

		Query query;
		DetalleAsientoContableDTO dto = null;
		StringBuilder queryString = null;
		String prefijo = "";

		if(prefijoId.indexOf(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL_BAJA)>-1){
			prefijo = prefijoId.substring(0, 2);
		}else{ 
			if(prefijoId.indexOf(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF_BAJA)>-1){
				prefijo = prefijoId.substring(0, 2);
			}else { 
				prefijo = prefijoId.substring(0, 1);
			}
		}


		if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL.equals(prefijo)) {
			queryString = new StringBuilder("SELECT TCA.CUENTA_CONTABLE, TCC.NOMBRE, TCA.IMPORTE, TCA.TIPO_APUNTE, TFM.NBDOCNUMERO, TFM.NBCOMENTARIOS "
					+ "FROM TMCT0_CONTA_APUNTE_DETALLE CAD, TMCT0_CONTA_APUNTES TCA, TMCT0_FACTURA_MANUAL TFM, TMCT0_CUENTA_CONTABLE TCC "
					+ "WHERE TCA.CUENTA_CONTABLE = TCC.CUENTA AND CAD.IDAPUNTE = TCA.ID AND CAD.ID_FACTURA_M = TFM.ID AND CAD.ID = :idApunteDetalle");
		} else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF.equals(prefijo)) {
			queryString = new StringBuilder("SELECT TCA.CUENTA_CONTABLE, TCC.NOMBRE, TCA.IMPORTE, TCA.TIPO_APUNTE, TRFM.NBDOCNUMERO, TRFM.NBCOMENTARIOS "
					+ "FROM TMCT0_CONTA_APUNTE_DETALLE CAD, TMCT0_CONTA_APUNTES TCA, TMCT0_FACTURA_MANUAL_RECTIFICATIVA TRFM, TMCT0_CUENTA_CONTABLE TCC "
					+ "WHERE TCA.CUENTA_CONTABLE = TCC.CUENTA AND CAD.IDAPUNTE = TCA.ID AND TRFM.ID = CAD.ID_FACTURA_MR AND CAD.ID = :idApunteDetalle");
		}else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL_BAJA.equals(prefijo)) {
			queryString = new StringBuilder("SELECT TCA.CUENTA_CONTABLE, TCC.NOMBRE, TCA.IMPORTE, TCA.TIPO_APUNTE, TFMB.NBDOCNUMERO, TFMB.NBCOMENTARIOS "
					+ "FROM TMCT0_CONTA_APUNTE_DETALLE CAD, TMCT0_CONTA_APUNTES TCA, TMCT0_FACTURA_MANUAL_BAJA TFMB, TMCT0_CUENTA_CONTABLE TCC "
					+ "WHERE TCA.CUENTA_CONTABLE = TCC.CUENTA AND CAD.IDAPUNTE = TCA.ID AND CAD.ID_FACTURA_MB = TFMB.ID AND CAD.ID = :idApunteDetalle");
		} else if (sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF_BAJA.equals(prefijo)) {
			queryString = new StringBuilder("SELECT TCA.CUENTA_CONTABLE, TCC.NOMBRE, TCA.IMPORTE, TCA.TIPO_APUNTE, TRFMB.NBDOCNUMERO, TRFMB.NBCOMENTARIOS "
					+ "FROM TMCT0_CONTA_APUNTE_DETALLE CAD, TMCT0_CONTA_APUNTES TCA, TMCT0_FACTURA_MANUAL_RECTIFICATIVA_BAJA TRFMB, TMCT0_CUENTA_CONTABLE TCC "
					+ "WHERE TCA.CUENTA_CONTABLE = TCC.CUENTA AND CAD.IDAPUNTE = TCA.ID AND TRFMB.ID = CAD.ID_FACTURA_MRB AND CAD.ID = :idApunteDetalle");
		}

		try {

			query = entityManager.createNativeQuery(queryString.toString());

			Integer id = null;
			if(prefijoId.indexOf(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_MANUAL_BAJA)>-1){
				id = new Integer(prefijoId.substring(2, prefijoId.length()));
			}else{ 
				if(prefijoId.indexOf(sibbac.business.interfacesemitidas.utils.Constantes.PREFJIJO_FACTURA_RECTIF_BAJA)>-1){
					id = new Integer(prefijoId.substring(2, prefijoId.length()));
				}else { 
					id = new Integer(prefijoId.substring(1, prefijoId.length()));
				}
			}

			if (id != null) {
				query.setParameter("idApunteDetalle", id);
			}

			Object object = query.getSingleResult();

			if (object != null) {
				Object[] resultado = (Object[]) object;
				dto = DetalleAsientoContableDTO.convertObjectToDetalleAsientoContableDTO(resultado);
			}
		} catch (Exception e) {
			LOG.error("Error metodo getDetalleAsientoContableModalById -  " + e.getMessage(), e);
			throw (e);
		}
		return dto;
	}

	public void eliminarFacturasManuales(List<BigInteger> listIdsFactManuales) {

		LOG.info("INICIO - DAOIMPL - eliminarFacturasManuales");

		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("DELETE FROM TMCT0_FACTURA_MANUAL FM "
					+ "WHERE FM.ID IN :listIdsFactManuales");

			parameters.put("listIdsFactManuales", listIdsFactManuales);

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades de estado con los datos recuperados.
			int numFactEliminadas = query.executeUpdate();
			LOG.debug("Facturas Manuales eliminadas {}", numFactEliminadas);
		} catch (Exception e) {
			LOG.error("Error metodo eliminarFacturasManuales -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - eliminarFacturasManuales");
	}

	//
	// @SuppressWarnings("unchecked")
	// public List<Object> consultaridEstado(String nombreEstado) {
	//
	// LOG.info("INICIO - DAOIMPL - consultaridEstado");
	//
	// Map<String, Object> parameters = new HashMap<String, Object>();
	// List<Object> listaIdEstado = new ArrayList<Object>();
	//
	// try {
	// StringBuilder consulta = new StringBuilder(
	// "SELECT E.IDESTADO FROM TMCT0ESTADO E WHERE E.NOMBRE = "
	// +
	// "(SELECT IEC.VALOR FROM TMCT0_INTERFACES_EMITIDAS_CONST IEC WHERE IEC.CLAVE = :nombreEstado AND IEC.GRUPO = 'Tmct0estado')");
	//
	// parameters.put("nombreEstado", nombreEstado);
	//
	// LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
	// Query query = null;
	// query = entityManager.createNativeQuery(consulta.toString());
	//
	// for (Entry<String, Object> entry : parameters.entrySet()) {
	// query.setParameter(entry.getKey(), entry.getValue());
	// }
	//
	// // Se forman todas las entidades de estado con los datos recuperados.
	// listaIdEstado = query.getResultList();
	//
	// } catch (Exception e) {
	// LOG.error("Error metodo consultaridEstado -  " + e.getMessage(), e);
	// throw (e);
	// }
	//
	// LOG.info("FIN - DAOIMPL - consultaridEstado");
	//
	// return listaIdEstado;
	// }

	/**
	 * Se crea una entidad de baja al anular la factura manual.
	 *
	 * @param entity entity
	 * @param newState newState
	 * @return BigInteger BigInteger
	 * @throws SIBBACBusinessException SIBBACBusinessException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public BigInteger createEntityBajaManual(Tmct0FacturasManuales entity, Tmct0estado newState) throws SIBBACBusinessException {
		// se pone un 9 por delante a nbdocnumero, estado contabilizado, enviado a 0, IMFINSVB negado
		Tmct0BajasManuales tmct0BajasManuales = new Tmct0BajasManuales();
		tmct0BajasManuales.setAuditDate(new Date());
		tmct0BajasManuales.setAuditUser(entity.getAuditUser());
		tmct0BajasManuales.setBaseImponCostes(entity.getBaseImponCostes());
		tmct0BajasManuales.setCausaExencion(entity.getCausaExencion());
		tmct0BajasManuales.setClaveRegimen(entity.getClaveRegimen());
		tmct0BajasManuales.setCodImpuesto(entity.getCodImpuesto());
		tmct0BajasManuales.setCoefImpCostes(entity.getCoefImpCostes());
		tmct0BajasManuales.setCuotaRepercut(entity.getCuotaRepercut());
		tmct0BajasManuales.setEnviado(false);
		tmct0BajasManuales.setEstado(newState);
		tmct0BajasManuales.setFhFechaCre(new Date());
		tmct0BajasManuales.setFhFechaFac(entity.getFhFechaFac());
		tmct0BajasManuales.setFhInicio(entity.getFhInicio());
		tmct0BajasManuales.setIdAlias(entity.getIdAlias());
		tmct0BajasManuales.setIdEntregaBien(entity.getIdEntregaBien());
		tmct0BajasManuales.setImFinDiv(entity.getImFinDiv().negate());
		tmct0BajasManuales.setImFinSvb(entity.getImFinSvb().negate());
		tmct0BajasManuales.setImpBaseImponible(entity.getImpBaseImponible().negate());
		tmct0BajasManuales.setImpImpuesto(entity.getImpImpuesto().negate());
		tmct0BajasManuales.setMoneda(entity.getMoneda());
		tmct0BajasManuales.setNbComentarios(entity.getNbComentarios());
		boolean esGrupoIva = false;
		if (entity.getClaveRegimen().getId() == 6) {
			esGrupoIva = true;
		}
		BigInteger numeroBaja = getNbDocNumeroFacturaManual(esGrupoIva, true);
		tmct0BajasManuales.setNbDocNumero(numeroBaja);
		tmct0BajasManuales.setNbDocNumeroOriginal(entity.getNbDocNumero());
		tmct0BajasManuales.setPeriodo(entity.getPeriodo());
		tmct0BajasManuales.setTipoFactura(entity.getTipoFactura());
		tmct0BajasManuales.setTmct0Exentas(entity.getTmct0Exentas());
		tmct0BajasManuales.setTmct0Sujetas(entity.getTmct0Sujetas());
		entityManager.persist(tmct0BajasManuales);
		return tmct0BajasManuales.getId();
	}

	/**
	 * Se obtiene numero de factura manual (nbDocNumero). Se basa en tener nros de factura con valores contiguos, por
	 * ejemplo: Ids facturas: 1,2,4,5,6,7 Nros factura: 1,2,4,5,6,3
	 * 
	 * @return BigInteger BigInteger
	 * @throws SIBBACBusinessException SIBBACBusinessException
	 */
	public BigInteger getNbDocNumeroFacturaManual(boolean esGrupoIva, boolean esManualBaja) throws SIBBACBusinessException {
		String identif = sibbac.business.interfacesemitidas.utils.Constantes.IDENTIFICADOR_FACTURA_MANUAL;
		String tabla = "BSNBPSQL.TMCT0_FACTURA_MANUAL";
		String whereAnio = " WHERE SUBSTR(NBDOCNUMERO, 2,2) = '";
		String whereGrupoiva = "' AND SUBSTR(NBDOCNUMERO, 1,1)";
		if (esGrupoIva) {
			identif = sibbac.business.interfacesemitidas.utils.Constantes.IDENTIFICADOR_FACTURA_GRUPO_IVA;
		}
		if (esManualBaja) {
			tabla = "BSNBPSQL.TMCT0_FACTURA_MANUAL_BAJA";
			whereAnio = " WHERE SUBSTR(NBDOCNUMERO, 3,2) = '";
			whereGrupoiva = "' AND SUBSTR(NBDOCNUMERO, 2,1)";
		}
		String sAnio = String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2, 4);

		try {
			StringBuilder select = new StringBuilder();
			if (esGrupoIva) {
				select = new StringBuilder("SELECT NBDOCNUMERO FROM "+tabla+whereAnio+sAnio+whereGrupoiva+ " = '6' ORDER BY NBDOCNUMERO ASC");
			} else {
				select = new StringBuilder("SELECT NBDOCNUMERO FROM "+tabla+whereAnio+sAnio+whereGrupoiva+ " NOT IN ('6') ORDER BY NBDOCNUMERO ASC");
			}
			Query query = entityManager.createNativeQuery(select.toString());
			List<?> listaNbdocnumero = query.getResultList();

			if (CollectionUtils.isNotEmpty(listaNbdocnumero)) {
				int count = 1;
				for (Object obj : listaNbdocnumero) {
					BigInteger nbDocNumeroOrdenado = BigInteger.valueOf(count);
					BigInteger nbDocNumeroTabla = ((BigInteger) obj);
					BigInteger nbDocNumeroTablaStripped = new BigInteger(String.valueOf(nbDocNumeroTabla).substring(3, 8));
					if (esManualBaja) {
						nbDocNumeroTablaStripped = new BigInteger(String.valueOf(nbDocNumeroTabla).substring(4, 8));
					}
					if (nbDocNumeroOrdenado.compareTo(nbDocNumeroTablaStripped) != 0) {
						if (esManualBaja) {
							return InterfacesEmitidasHelper.getNumeroFacturaFormateadoBaja(identif, nbDocNumeroOrdenado);
						} else { 
							return InterfacesEmitidasHelper.getNumeroFacturaFormateado(identif, nbDocNumeroOrdenado);
						}
					}
					count++;
				}
				BigInteger nbDocNumeroOrdenado = (BigInteger) listaNbdocnumero.get(listaNbdocnumero.size() - 1);
				return nbDocNumeroOrdenado.add(BigInteger.valueOf(1));

			} else {
				if (esManualBaja) {
					return InterfacesEmitidasHelper.getNumeroFacturaFormateadoBaja(identif, BigInteger.ONE);
				} else {
					return InterfacesEmitidasHelper.getNumeroFacturaFormateado(identif, BigInteger.ONE);
				}
			}

		} catch (RuntimeException e) {
			LOG.error("[getNbDocNumero] Error no controlado al intentar obtener numero de factura");
			throw new SIBBACBusinessException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Object[] consultarDatosPdf(BigInteger nbDocNumero, boolean esBaja) {

		LOG.info("INICIO - DAOIMPL - consultarDatosPdf");

		Map<String, Object> parameters = new HashMap<String, Object>();
		Object[] objPdf = new Object[1];
		String tablaFact = "TMCT0_FACTURA_MANUAL";
		if (esBaja) {
			tablaFact = "TMCT0_FACTURA_MANUAL_BAJA";
		}

		try {
			LOG.info("INICIO - DAOIMPL - Se va a realizar la llamada a BBDD a la consulta de datos para el pdf por idAlias");
			StringBuilder consulta = new StringBuilder(
					"SELECT fm.FHFECHAFAC AS FECHAFACTURA,"
							+ "ali.DESCRALI AS EMPCONTRAPARTE, "
							+ "CONCAT(CONCAT(CONCAT(CONCAT(addr.TPDOMICI,' '),addr.NBDOMICI),' '),addr.NUDOMICI) AS DIRECCION,"
							+ "CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(addr.CDPOSTAL,' '),addr.NBCIUDAD),' '),'('),addr.NBPROVIN),')') AS DIRECCIONPOSTAL,"
							+ "pais.NBPAIS AS PAIS,"
							+ "fm.NBDOCNUMERO AS NUMFACTURA,"
							+ "fm.NBCOMENTARIOS AS COMENTARIOS,"
							+ "fm.IMFINSVB AS IMPORTEFACT,"
							+ "fm.IMP_BASE_IMPONIBLE AS TAXBASE,"
							+ "fm.IMP_IMPUESTO AS TAX,"
							+ "cli.CIF AS BICCODE,"
							+ "txtIdioma.NBDESCRIPCIONES AS BROKERAGE,"
							+ "fm.COEF_IMP_COSTES AS COEFIMPCOSTES "
							+ "FROM TMCT0CLI cli "
							+ "JOIN TMCT0_ADDRESSES addr ON addr.IDCLIENT=cli.IDCLIENT "
							+ "JOIN TMCT0ALI ali ON ali.CDBROCLI=cli.CDBROCLI "
							+ "JOIN TMCT0_ALIAS alia ON alia.CDBROCLI=cli.CDBROCLI "
							+ "JOIN "+tablaFact+" fm ON trim(fm.ID_ALIAS) = ali.CDALIASS "
							+ "JOIN TMCT0PAISES pais ON pais.CDISOALF2=cli.CDPAIS "
							+ "JOIN TMCT0_TEXTO_IDIOMA txtIdioma ON txtIdioma.ID_IDIOMA=alia.ID_IDIOMA AND txtIdioma.ID_TEXTO=67 "
							+ "WHERE fm.NBDOCNUMERO = :nbDocNumero");

			parameters.put("nbDocNumero", nbDocNumero);

			Query query = null;
			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			query = entityManager.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			List<Object> resultList = null;

			resultList = query.getResultList();

			if (resultList.size() > 0) {
				objPdf = (Object[]) resultList.get(0);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarDatosPdf -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarDatosPdf");

		return objPdf;
	}

	public Map<String, String> consultarConstantes(String grupo) {

		LOG.info("INICIO - DAOIMPL - consultarConstantes");

		List<Tmct0InterfacesEmitidasConst> listaInterfacesEmitidasConst = new ArrayList<Tmct0InterfacesEmitidasConst>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		Map<String, String> mapConstantes = new HashMap<String, String>(); 

		try {
			StringBuilder consulta = new StringBuilder("SELECT iec FROM Tmct0InterfacesEmitidasConst iec WHERE grupo = :grupo");

			parameters.put("grupo", grupo);

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0InterfacesEmitidasConst> query = entityManager.createQuery(consulta.toString(),
					Tmct0InterfacesEmitidasConst.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades constantes con los datos recuperados.
			listaInterfacesEmitidasConst = query.getResultList();

			if (null!=listaInterfacesEmitidasConst && listaInterfacesEmitidasConst.size()>0) {
				for (int i=0; i<listaInterfacesEmitidasConst.size(); i++) {
					mapConstantes.put(listaInterfacesEmitidasConst.get(i).getClave(), listaInterfacesEmitidasConst.get(i).getValor());
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarConstantes -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarConstantes");

		return mapConstantes;
	}
}
