package sibbac.business.interfacesemitidas.dto.pantallas;

import java.util.Date;

/**
 *	DTO que transfiere los filtros de la busqueda de respuesta errores GC.
 */
public class RespuestaErroresExpedidasFilterDTO {

	private String nombreFichero;
	private String nombreFicheroNot;
	private String estado;
	private String estadoNot;
	private String tipoFichero;
	private String tipoFicheroNot;
	private Date fechaDesde;
	private Date fechaHasta;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public RespuestaErroresExpedidasFilterDTO() {
		super();
	}

	public String getNombreFichero() {
		return this.nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTipoFichero() {
		return this.tipoFichero;
	}

	public void setTipoFichero(String tipoFichero) {
		this.tipoFichero = tipoFichero;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getNombreFicheroNot() {
		return this.nombreFicheroNot;
	}

	public void setNombreFicheroNot(String nombreFicheroNot) {
		this.nombreFicheroNot = nombreFicheroNot;
	}

	public String getEstadoNot() {
		return this.estadoNot;
	}

	public void setEstadoNot(String estadoNot) {
		this.estadoNot = estadoNot;
	}

	public String getTipoFicheroNot() {
		return this.tipoFicheroNot;
	}

	public void setTipoFicheroNot(String tipoFicheroNot) {
		this.tipoFicheroNot = tipoFicheroNot;
	}
	
}
