package sibbac.business.interfacesemitidas.database.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.interfacesemitidas.database.model.Tmct0InterfacesEmitidasConst;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;
import sibbac.business.interfacesemitidas.dto.ClienteDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.helper.InterfacesEmitidasHelper;
import sibbac.common.SIBBACBusinessException;

@Repository
public class Tmct0RectificativasManualesDAOImpl {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0RectificativasManualesDAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	public List<Tmct0RectificativaManual> consultarRectificativasManuales(Tmct0FiltroFacturasManualesDTO filtroFacturasManuales) {

		LOG.info("INICIO - DAOIMPL - consultarFacturasRectificativasManuales");

		List<Tmct0RectificativaManual> listaFacturasRectificativasManuales = new ArrayList<Tmct0RectificativaManual>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT fr FROM Tmct0RectificativaManual fr WHERE 1=1");

			if (null != filtroFacturasManuales.getNbDocNumero()) {
				consulta.append(" AND fr.nbDocNumero = :nbDocNumero");
				parameters.put("nbDocNumero", filtroFacturasManuales.getNbDocNumero());
			}

			if (null != filtroFacturasManuales.getIdAlias()) {
				consulta.append(" AND fr.idAlias = :idAlias");
				parameters.put("idAlias", filtroFacturasManuales.getIdAlias());
			}

			if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta().equals("")))) {
				consulta.append(" AND fr.fhFechaCre BETWEEN :fechaDesde AND :fechaHasta");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			} else if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null == filtroFacturasManuales.getFechaHasta() || filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fr.fhFechaCre >= :fechaDesde");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
			} else if ((null == filtroFacturasManuales.getFechaDesde() || filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fr.fhFechaCre <= :fechaHasta");
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			}

			if (null != filtroFacturasManuales.getIdEstado()) {
				consulta.append(" AND fr.estado.idestado = :idEstado");
				parameters.put("idEstado", filtroFacturasManuales.getIdEstado());
			}

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0RectificativaManual> query = entityManager.createQuery(consulta.toString(),
					Tmct0RectificativaManual.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades factura manual con los datos recuperados.
			listaFacturasRectificativasManuales = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarFacturasRectificativasManuales -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarFacturasRectificativasManuales");

		return listaFacturasRectificativasManuales;
	}

	public List<Tmct0RectificativaBajasManuales> consultarRectificativasManualesBaja(Tmct0FiltroFacturasManualesDTO filtroFacturasManuales) {

		LOG.info("INICIO - DAOIMPL - consultarRectificativasManualesBaja");

		List<Tmct0RectificativaBajasManuales> listaFacturasRectificativasManualesBaja = new ArrayList<Tmct0RectificativaBajasManuales>();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			StringBuilder consulta = new StringBuilder("SELECT fr FROM Tmct0RectificativaBajasManuales fr WHERE 1=1");

			if (null != filtroFacturasManuales.getNbDocNumero()) {
				consulta.append(" AND fr.nbDocNumero = :nbDocNumero");
				parameters.put("nbDocNumero", filtroFacturasManuales.getNbDocNumero());
			}

			if (null != filtroFacturasManuales.getIdAlias()) {
				consulta.append(" AND fr.idAlias = :idAlias");
				parameters.put("idAlias", filtroFacturasManuales.getIdAlias());
			}

			if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta().equals("")))) {
				consulta.append(" AND fr.fhFechaCre BETWEEN :fechaDesde AND :fechaHasta");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			} else if ((null != filtroFacturasManuales.getFechaDesde() && !filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null == filtroFacturasManuales.getFechaHasta() || filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fr.fhFechaCre >= :fechaDesde");
				parameters.put("fechaDesde", filtroFacturasManuales.getFechaDesde());
			} else if ((null == filtroFacturasManuales.getFechaDesde() || filtroFacturasManuales.getFechaDesde().equals(""))
					&& ((null != filtroFacturasManuales.getFechaHasta() && !filtroFacturasManuales.getFechaHasta()
					.equals("")))) {
				consulta.append(" AND fr.fhFechaCre <= :fechaHasta");
				parameters.put("fechaHasta", filtroFacturasManuales.getFechaHasta());
			}

			// MFG las recitificas de baja son de la tabla de bajas pero no estan en estado de baja seleccionado
			// if (null != filtroFacturasManuales.getIdEstado()) {
			// consulta.append(" AND fr.estado.idestado = :idEstado");
			// parameters.put("idEstado", filtroFacturasManuales.getIdEstado());
			// }

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0RectificativaBajasManuales> query = entityManager.createQuery(consulta.toString(),
					Tmct0RectificativaBajasManuales.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades factura manual con los datos recuperados.
			listaFacturasRectificativasManualesBaja = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultarRectificativasManualesBaja -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarRectificativasManualesBaja");

		return listaFacturasRectificativasManualesBaja;
	}

	@SuppressWarnings("unchecked")
	public List<Object> consultaridEstado(String nombreEstado) {

		LOG.info("INICIO - DAOIMPL - consultaridEstado");

		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Object> listaIdEstado = new ArrayList<Object>();

		try {
			StringBuilder consulta = new StringBuilder(
					"SELECT E.IDESTADO FROM TMCT0ESTADO E WHERE E.NOMBRE = "
							+ "(SELECT IEC.VALOR FROM TMCT0_INTERFACES_EMITIDAS_CONST IEC WHERE IEC.CLAVE = :nombreEstado AND IEC.GRUPO = 'Tmct0estado')");

			parameters.put("nombreEstado", nombreEstado);

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			Query query = null;
			query = entityManager.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades de estado con los datos recuperados.
			listaIdEstado = query.getResultList();

		} catch (Exception e) {
			LOG.error("Error metodo consultaridEstado -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultaridEstado");

		return listaIdEstado;
	}

	@SuppressWarnings("unchecked")
	public ClienteDTO consultarClienteByAlias(BigInteger idAlias) {

		LOG.info("INICIO - DAOIMPL - consultarClienteByAlias");

		ClienteDTO cliente = new ClienteDTO();
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			LOG.info("INICIO - DAOIMPL - Se va a realizar la llamada a BBDD a la consulta de cliente por idAlias");
			StringBuilder consulta = new StringBuilder(
					"SELECT addr.NBPROVIN, pais.NBPAIS, cli.TPRESIDE, cli.TPIDENTI, cli.CIF FROM TMCT0CLI cli "
							+ "JOIN TMCT0_ADDRESSES addr ON addr.IDCLIENT=cli.IDCLIENT "
							+ "JOIN TMCT0ALI ali ON ali.CDBROCLI=cli.CDBROCLI "
							+ "JOIN TMCT0PAISES pais ON pais.CDISOALF2=cli.CDPAIS "
							+ "WHERE ali.CDALIASS = :idAlias");

			parameters.put("idAlias", idAlias);

			Query query = null;
			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			query = entityManager.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			List<Object> resultList = null;

			resultList = query.getResultList();

			if (resultList.size() > 0) {
				Object[] objCliente = (Object[]) resultList.get(0);

				cliente.setProvincia(objCliente[0].toString());
				cliente.setPaisResidencia(objCliente[1].toString());
				if ((objCliente[2].toString()).equals("R")) {
					cliente.setTipoResidencia("Residencial");
				} else if ((objCliente[2].toString()).equals("NR")) {
					cliente.setTipoResidencia("No Residencial");
				}
				cliente.setTpdocumento(objCliente[3].toString());
				cliente.setNumdocumento(objCliente[4].toString());

			}
		} catch (Exception e) {
			LOG.error("Error metodo consultarFacturasManuales -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarClienteByAlias");

		return cliente;
	}

	/**
	 * Se obtiene numero de factura rectificativa manual (nbDocNumero). Se basa en tener nros de factura con valores
	 * contiguos, por ejemplo: Ids facturas: 1,2,4,5,6,7 Nros factura: 1,2,4,5,6,3
	 * 
	 * @return BigInteger BigInteger
	 * @throws SIBBACBusinessException SIBBACBusinessException
	 */
	public BigInteger getNbDocNumeroRectificativa(boolean esGrupoIva, boolean esRectificativaBaja) throws SIBBACBusinessException {
		String identif = sibbac.business.interfacesemitidas.utils.Constantes.IDENTIFICADOR_FACTURA_RECTIF;
		String tabla = "BSNBPSQL.TMCT0_FACTURA_MANUAL_RECTIFICATIVA";
		String whereAnio = " WHERE SUBSTR(NBDOCNUMERO, 2,2) = '";
		String whereGrupoiva = "' AND SUBSTR(NBDOCNUMERO, 1,1)";
		if (esGrupoIva) {
			identif = sibbac.business.interfacesemitidas.utils.Constantes.IDENTIFICADOR_FACTURA_RECTIFICATIVA_GRUPO_IVA;
		}
		if (esRectificativaBaja) {
			tabla = "BSNBPSQL.TMCT0_FACTURA_MANUAL_RECTIFICATIVA_BAJA";
			whereAnio = " WHERE SUBSTR(NBDOCNUMERO, 3,2) = '";
			whereGrupoiva = "' AND SUBSTR(NBDOCNUMERO, 2,1)";
		}
		String sAnio = String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2, 4);

		try {
			StringBuilder select = new StringBuilder();
			if (esGrupoIva) {
				select = new StringBuilder("SELECT NBDOCNUMERO FROM "+tabla+whereAnio+sAnio+whereGrupoiva+ " = '5' ORDER BY NBDOCNUMERO ASC");
			} else {
				select = new StringBuilder("SELECT NBDOCNUMERO FROM "+tabla+whereAnio+sAnio+whereGrupoiva+" NOT IN ('5') ORDER BY NBDOCNUMERO ASC");
			}
//			select = new StringBuilder("SELECT NBDOCNUMERO FROM BSNBPSQL.TMCT0_FACTURA_MANUAL_RECTIFICATIVA WHERE SUBSTR(NBDOCNUMERO, 2,2) = " + sAnio + " ORDER BY NBDOCNUMERO ASC"); 
			Query query = entityManager.createNativeQuery(select.toString());
			List<?> listaNbdocnumero = query.getResultList();

			if (CollectionUtils.isNotEmpty(listaNbdocnumero)) {
				int count = 1;
				for (Object obj : listaNbdocnumero) {
					BigInteger nbDocNumeroOrdenado = BigInteger.valueOf(count);
					BigInteger nbDocNumeroTabla = ((BigInteger) obj);
					BigInteger nbDocNumeroTablaStripped = new BigInteger(String.valueOf(nbDocNumeroTabla).substring(3, 8));
					if (esRectificativaBaja) {
						nbDocNumeroTablaStripped = new BigInteger(String.valueOf(nbDocNumeroTabla).substring(4, 8));
					}
					if (nbDocNumeroOrdenado.compareTo(nbDocNumeroTablaStripped) != 0) {
						if (esRectificativaBaja) {
							return InterfacesEmitidasHelper.getNumeroFacturaFormateadoBaja(identif, nbDocNumeroOrdenado);
						} else { 
							return InterfacesEmitidasHelper.getNumeroFacturaFormateado(identif, nbDocNumeroOrdenado);
						}
					}
					count++;
				}

				BigInteger nbDocNumeroOrdenado = (BigInteger) listaNbdocnumero.get(listaNbdocnumero.size() - 1);
				return nbDocNumeroOrdenado.add(BigInteger.valueOf(1));

			} else {
				if (esRectificativaBaja) {
					return InterfacesEmitidasHelper.getNumeroFacturaFormateadoBaja(identif, BigInteger.ONE);
				} else {
					return InterfacesEmitidasHelper.getNumeroFacturaFormateado(identif, BigInteger.ONE);
				}
				
			}

		} catch (RuntimeException e) {
			LOG.error("[getNbDocNumero] Error no controlado al intentar obtener numero de factura", e);
			throw new SIBBACBusinessException(e);
		}
	}

	/**
	 * Se crea una entidad de baja rectificada al anular la factura manual rectificada.
	 *
	 * @param entity entity
	 * @param newState newState
	 * @return BigInteger BigInteger
	 * @throws SIBBACBusinessException SIBBACBusinessException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public BigInteger createEntityBajaRectifManual(Tmct0RectificativaManual entity, Tmct0estado newState) throws SIBBACBusinessException {
		// se pone un 9 por delante a nbdocnumero, estado contabilizado, enviado a 0, IMFINSVB negado
		Tmct0RectificativaBajasManuales tmct0RectifBajasManuales = new Tmct0RectificativaBajasManuales();
		tmct0RectifBajasManuales.setAuditDate(new Date());
		tmct0RectifBajasManuales.setAuditUser(entity.getAuditUser());
		tmct0RectifBajasManuales.setBaseImponCostes(entity.getBaseImponCostes());
		tmct0RectifBajasManuales.setCausaExencion(entity.getCausaExencion());
		tmct0RectifBajasManuales.setClaveRegimen(entity.getClaveRegimen());
		tmct0RectifBajasManuales.setCodImpuesto(entity.getCodImpuesto());
		tmct0RectifBajasManuales.setCoefImpCostes(entity.getCoefImpCostes());
		tmct0RectifBajasManuales.setCuotaRepercut(entity.getCuotaRepercut());
		tmct0RectifBajasManuales.setEnviado(false);
		tmct0RectifBajasManuales.setEstado(newState);
		tmct0RectifBajasManuales.setFhFechaCre(new Date());
		tmct0RectifBajasManuales.setFhFechaFac(entity.getFhFechaFac());
		tmct0RectifBajasManuales.setFhInicio(entity.getFhInicio());
		tmct0RectifBajasManuales.setIdAlias(entity.getIdAlias());
		tmct0RectifBajasManuales.setIdEntregaBien(entity.getIdEntregaBien());
		tmct0RectifBajasManuales.setImFinDiv(entity.getImFinDiv().negate());
		tmct0RectifBajasManuales.setImFinSvb(entity.getImFinSvb().negate());
		tmct0RectifBajasManuales.setImpBaseImponible(entity.getImpBaseImponible().negate());
		tmct0RectifBajasManuales.setImpImpuesto(entity.getImpImpuesto().negate());
		tmct0RectifBajasManuales.setMoneda(entity.getMoneda());
		tmct0RectifBajasManuales.setNbComentarios(entity.getNbComentarios());
		boolean esGrupoIva = false;
		if (entity.getClaveRegimen().getId() == 6) {
			esGrupoIva = true;
		}
		BigInteger numeroBaja = getNbDocNumeroRectificativa(esGrupoIva, true);
		tmct0RectifBajasManuales.setNbDocNumero(numeroBaja);
		tmct0RectifBajasManuales.setNbDocNumeroOriginal(entity.getNbDocNumero());
		tmct0RectifBajasManuales.setNbDocNumero0(entity.getNbDocNumero0());
		tmct0RectifBajasManuales.setNbDocNumero1(entity.getNbDocNumero1());
		tmct0RectifBajasManuales.setNbDocNumero2(entity.getNbDocNumero2());
		tmct0RectifBajasManuales.setNbDocNumero3(entity.getNbDocNumero3());
		tmct0RectifBajasManuales.setNbDocNumero4(entity.getNbDocNumero4());
		tmct0RectifBajasManuales.setNbDocNumero5(entity.getNbDocNumero5());
		tmct0RectifBajasManuales.setNbDocNumero6(entity.getNbDocNumero6());
		tmct0RectifBajasManuales.setNbDocNumero7(entity.getNbDocNumero7());
		tmct0RectifBajasManuales.setNbDocNumero8(entity.getNbDocNumero8());
		tmct0RectifBajasManuales.setNbDocNumero9(entity.getNbDocNumero9());

		tmct0RectifBajasManuales.setPeriodo(entity.getPeriodo());
		tmct0RectifBajasManuales.setTipoFactura(entity.getTipoFactura());
		tmct0RectifBajasManuales.setTmct0Exentas(entity.getTmct0Exentas());
		tmct0RectifBajasManuales.setTmct0Sujetas(entity.getTmct0Sujetas());
		tmct0RectifBajasManuales.setBaseImponibleActual(entity.getBaseImponibleActual());
		tmct0RectifBajasManuales.setBaseImponibleAnterior(entity.getBaseImponibleAnterior());
		entityManager.persist(tmct0RectifBajasManuales);
		return tmct0RectifBajasManuales.getId();
	}

	@SuppressWarnings("unchecked")
	public Object[] consultarDatosPdf(BigInteger nbDocNumero, boolean esBaja) {

		LOG.info("INICIO - DAOIMPL - consultarDatosPdf");

		Map<String, Object> parameters = new HashMap<String, Object>();
		Object[] objPdf = new Object[1];
		String tablaFact = "TMCT0_FACTURA_MANUAL_RECTIFICATIVA";
		if (esBaja) {
			tablaFact = "TMCT0_FACTURA_MANUAL_RECTIFICATIVA_BAJA";
		}

		try {
			LOG.info("INICIO - DAOIMPL - Se va a realizar la llamada a BBDD a la consulta de datos para el pdf por idAlias");
			StringBuilder consulta = new StringBuilder(
					"SELECT fm.FHFECHAFAC AS FECHAFACTURA,"
							+ "ali.DESCRALI AS EMPCONTRAPARTE, "
							+ "CONCAT(CONCAT(CONCAT(CONCAT(addr.TPDOMICI,' '),addr.NBDOMICI),' '),addr.NUDOMICI) AS DIRECCION,"
							+ "CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(addr.CDPOSTAL,' '),addr.NBCIUDAD),' '),'('),addr.NBPROVIN),')') AS DIRECCIONPOSTAL,"
							+ "pais.NBPAIS AS PAIS,"
							+ "fm.NBDOCNUMERO AS NUMFACTURA,"
							+ "fm.NBCOMENTARIOS AS COMENTARIOS,"
							+ "fm.IMFINSVB AS IMPORTEFACT,"
							+ "fm.IMP_BASE_IMPONIBLE AS TAXBASE,"
							+ "fm.IMP_IMPUESTO AS TAX,"
							+ "cli.CIF AS BICCODE,"
							+ "txtIdioma.NBDESCRIPCIONES AS BROKERAGE,"
							+ "fm.COEF_IMP_COSTES AS COEFIMPCOSTES,"
							+ "fm.BASE_IMPONIBLE_ANTERIOR AS BASEANTERIOR,"
							+ "fm.BASE_IMPONIBLE_ACTUAL AS BASEACTUAL,"
							+ "fm.NBDOCNUMERO0,"
							+ "fm.NBDOCNUMERO1,"
							+ "fm.NBDOCNUMERO2,"
							+ "fm.NBDOCNUMERO3,"
							+ "fm.NBDOCNUMERO4,"
							+ "fm.NBDOCNUMERO5,"
							+ "fm.NBDOCNUMERO6,"
							+ "fm.NBDOCNUMERO7,"
							+ "fm.NBDOCNUMERO8,"
							+ "fm.NBDOCNUMERO9 "
							+ "FROM TMCT0CLI cli "
							+ "JOIN TMCT0_ADDRESSES addr ON addr.IDCLIENT=cli.IDCLIENT "
							+ "JOIN TMCT0ALI ali ON ali.CDBROCLI=cli.CDBROCLI "
							+ "JOIN TMCT0_ALIAS alia ON alia.CDBROCLI=cli.CDBROCLI "
							+ "JOIN "+tablaFact+" fm ON fm.ID_ALIAS=ali.CDALIASS "
							+ "JOIN TMCT0PAISES pais ON pais.CDISOALF2=cli.CDPAIS "
							+ "JOIN TMCT0_TEXTO_IDIOMA txtIdioma ON txtIdioma.ID_IDIOMA=alia.ID_IDIOMA AND txtIdioma.ID_TEXTO=67 "
							+ "WHERE fm.NBDOCNUMERO = :nbDocNumero");

			parameters.put("nbDocNumero", nbDocNumero);

			Query query = null;
			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			query = entityManager.createNativeQuery(consulta.toString());

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			List<Object> resultList = null;

			resultList = query.getResultList();

			if (resultList.size() > 0) {
				objPdf = (Object[]) resultList.get(0);
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarDatosPdf -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarDatosPdf");

		return objPdf;
	}

	public Map<String, String> consultarConstantes(String grupo) {

		LOG.info("INICIO - DAOIMPL - consultarConstantes");

		List<Tmct0InterfacesEmitidasConst> listaInterfacesEmitidasConst = new ArrayList<Tmct0InterfacesEmitidasConst>();
		Map<String, Object> parameters = new HashMap<String, Object>();
		Map<String, String> mapConstantes = new HashMap<String, String>(); 

		try {
			StringBuilder consulta = new StringBuilder("SELECT iec FROM Tmct0InterfacesEmitidasConst iec WHERE grupo = :grupo");

			parameters.put("grupo", grupo);

			LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
			TypedQuery<Tmct0InterfacesEmitidasConst> query = entityManager.createQuery(consulta.toString(),
					Tmct0InterfacesEmitidasConst.class);

			for (Entry<String, Object> entry : parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}

			// Se forman todas las entidades constantes con los datos recuperados.
			listaInterfacesEmitidasConst = query.getResultList();

			if (null!=listaInterfacesEmitidasConst && listaInterfacesEmitidasConst.size()>0) {
				for (int i=0; i<listaInterfacesEmitidasConst.size(); i++) {
					mapConstantes.put(listaInterfacesEmitidasConst.get(i).getClave(), listaInterfacesEmitidasConst.get(i).getValor());
				}
			}

		} catch (Exception e) {
			LOG.error("Error metodo consultarConstantes -  " + e.getMessage(), e);
			throw (e);
		}

		LOG.info("FIN - DAOIMPL - consultarConstantes");

		return mapConstantes;
	}
}
