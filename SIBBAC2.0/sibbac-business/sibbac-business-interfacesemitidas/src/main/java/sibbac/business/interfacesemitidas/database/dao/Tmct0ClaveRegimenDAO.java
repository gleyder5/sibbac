package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0ClaveRegimen;

/**
 * @author Neoris
 *
 */
@Repository
public interface Tmct0ClaveRegimenDAO extends JpaRepository<Tmct0ClaveRegimen,Integer>{
	
	@Query("SELECT claveReg FROM Tmct0ClaveRegimen claveReg ORDER BY claveReg.id ASC")
	public List<Tmct0ClaveRegimen> findAllOrderByClave();

}
