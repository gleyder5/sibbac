package sibbac.business.interfacesemitidas.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * DTO para la gestion de TMCT0_FACTURA_MANUAL
 * 
 * @author Neoris
 *
 */
public class Tmct0RectificativasManualesTablaDTO {

  /**
   * id
   */
  private BigInteger id;

  /**
   * fecha de creación de la factura
   */
  private Date fhFechaCre;

  /**
   * fecha de contabilización factura
   */
  private Date fhFechaFac;

  /**
   * número de la factura (autogenerado)
   */
  private BigInteger nbDocNumero;

  /**
   * identificador del alias asociado al cliente
   */
  private String empContraparte;

  /**
   * nombre del estado
   */
  private String estado;

  /**
   * Importe correspondiente a la parte Sujeta / Exenta o No. importe de total de la factura
   */
  private BigDecimal impBaseImponible;

  /**
   * según impuesto la cuota que se añade a la base imponible que sumada a ésta da el total de la factura. En caso de
   * exenta sería 0, en caso de no exenta según impuesto
   */
  private BigDecimal impImpuesto;

  /**
   * este campo será obligatoria si en el campo Clave Regimen Especial o Trascendencia se ha rellenado con el código 07
   * Régimen especial grupo de entidades en IVA (nivel avanzado), podrá ir a cero si el usuario no introduce nada. Es un
   * porcentaje
   */
  private BigDecimal coefImpCostes;

  /**
   * este campo se informara igual que el anterior
   */
  private BigDecimal baseImponCostes;

  /**
   * es la cuota resultante de aplicar un tipo impositivo a la base imponible
   */
  private BigDecimal cuotaRepercut;

  /**
   * es el importe total de la factura
   */
  private BigDecimal imFinSvb;

  /**
   * es el importe en divisa
   */
  private BigDecimal imFinDiv;

  /**
   * Comentario de la factura
   */
  private String nbComentarios;

  /**
   * Indicador de si la factura es modificable o no
   */
  private boolean esModificable;

  /**
   * Indicador de si la factura es anulable o no
   */
  private boolean esAnulable;
  
  /**
   * Importe correspondiente a la base imponible anterior de una factura rectificativa GRUPO IVA
   */
  private BigDecimal baseImponibleAnterior;

  /**
   * Importe correspondiente a la base imponible actual de una factura rectificativa GRUPO IVA
   */
  private BigDecimal baseImponibleActual;
  
  /**
   * identificador de la clave régimen asociada a la factura extraída de la tabla TMCT0_CLAVE_REGIMEN
   */
  private Integer claveRegimen;

  public Tmct0RectificativasManualesTablaDTO() {

  }

  /**
   * Constructor con parámetros
   */
  public Tmct0RectificativasManualesTablaDTO(Date fhFechaCre,
                                             Date fhFechaFac,
                                             BigInteger nbDocNumero,
                                             String empContraparte,
                                             String estado,
                                             BigDecimal impBaseImponible,
                                             BigDecimal impImpuesto,
                                             BigDecimal coefImpCostes,
                                             BigDecimal baseImponCostes,
                                             BigDecimal cuotaRepercut,
                                             BigDecimal imFinSvb,
                                             BigDecimal imFinDiv,
                                             String nbComentarios,
                                             BigInteger id,
                                             boolean esModificable,
                                             BigDecimal baseImponibleAnterior,
                                             BigDecimal baseImponibleActual,
                                             Integer claveRegimen) {

    this.fhFechaCre = fhFechaCre;
    this.fhFechaFac = fhFechaFac;
    this.nbDocNumero = nbDocNumero;
    this.empContraparte = empContraparte;
    this.estado = estado;
    this.impBaseImponible = impBaseImponible;
    this.impImpuesto = impImpuesto;
    this.coefImpCostes = coefImpCostes;
    this.baseImponCostes = baseImponCostes;
    this.cuotaRepercut = cuotaRepercut;
    this.imFinSvb = imFinSvb;
    this.imFinDiv = imFinDiv;
    this.nbComentarios = nbComentarios;
    this.id = id;
    this.esModificable = esModificable;
    this.baseImponibleAnterior=baseImponibleAnterior;
    this.baseImponibleActual=baseImponibleActual;
    this.claveRegimen=claveRegimen;
  }

  /**
 * @return the claveRegimen
 */
public Integer getClaveRegimen() {
	return claveRegimen;
}

/**
 * @param claveRegimen the claveRegimen to set
 */
public void setClaveRegimen(Integer claveRegimen) {
	this.claveRegimen = claveRegimen;
}

/**
 * @return the baseImponibleAnterior
 */
public BigDecimal getBaseImponibleAnterior() {
	return baseImponibleAnterior;
}

/**
 * @param baseImponibleAnterior the baseImponibleAnterior to set
 */
public void setBaseImponibleAnterior(BigDecimal baseImponibleAnterior) {
	this.baseImponibleAnterior = baseImponibleAnterior;
}

/**
 * @return the baseImponibleActual
 */
public BigDecimal getBaseImponibleActual() {
	return baseImponibleActual;
}

/**
 * @param baseImponibleActual the baseImponibleActual to set
 */
public void setBaseImponibleActual(BigDecimal baseImponibleActual) {
	this.baseImponibleActual = baseImponibleActual;
}

/**
   * @return the fhFechaCre
   */
  public Date getFhFechaCre() {
    return fhFechaCre;
  }

  /**
   * @param fhFechaCre the fhFechaCre to set
   */
  public void setFhFechaCre(Date fhFechaCre) {
    this.fhFechaCre = fhFechaCre;
  }

  /**
   * @return the fhFechaFac
   */
  public Date getFhFechaFac() {
    return fhFechaFac;
  }

  /**
   * @param fhFechaFac the fhFechaFac to set
   */
  public void setFhFechaFac(Date fhFechaFac) {
    this.fhFechaFac = fhFechaFac;
  }

  /**
   * @return the nbDocNumero
   */
  public BigInteger getNbDocNumero() {
    return nbDocNumero;
  }

  /**
   * @param nbDocNumero the nbDocNumero to set
   */
  public void setNbDocNumero(BigInteger nbDocNumero) {
    this.nbDocNumero = nbDocNumero;
  }

  /**
   * @return the idAlias
   */
  public String getEmpContraparte() {
    return empContraparte;
  }

  /**
   * @param idAlias the idAlias to set
   */
  public void setEmpContraparte(String empContraparte) {
    this.empContraparte = empContraparte;
  }

  /**
   * @return the idEstado
   */
  public String getEstado() {
    return estado;
  }

  /**
   * @param idEstado the idEstado to set
   */
  public void setEstado(String estado) {
    this.estado = estado;
  }

  /**
   * @return the impBaseImponible
   */
  public BigDecimal getImpBaseImponible() {
    return impBaseImponible;
  }

  /**
   * @param impBaseImponible the impBaseImponible to set
   */
  public void setImpBaseImponible(BigDecimal impBaseImponible) {
    this.impBaseImponible = impBaseImponible;
  }

  /**
   * @return the impImpuesto
   */
  public BigDecimal getImpImpuesto() {
    return impImpuesto;
  }

  /**
   * @param impImpuesto the impImpuesto to set
   */
  public void setImpImpuesto(BigDecimal impImpuesto) {
    this.impImpuesto = impImpuesto;
  }

  /**
   * @return the coefImpCostes
   */
  public BigDecimal getCoefImpCostes() {
    return coefImpCostes;
  }

  /**
   * @param coefImpCostes the coefImpCostes to set
   */
  public void setCoefImpCostes(BigDecimal coefImpCostes) {
    this.coefImpCostes = coefImpCostes;
  }

  /**
   * @return the baseImponCostes
   */
  public BigDecimal getBaseImponCostes() {
    return baseImponCostes;
  }

  /**
   * @param baseImponCostes the baseImponCostes to set
   */
  public void setBaseImponCostes(BigDecimal baseImponCostes) {
    this.baseImponCostes = baseImponCostes;
  }

  /**
   * @return the cuotaRepercut
   */
  public BigDecimal getCuotaRepercut() {
    return cuotaRepercut;
  }

  /**
   * @param cuotaRepercut the cuotaRepercut to set
   */
  public void setCuotaRepercut(BigDecimal cuotaRepercut) {
    this.cuotaRepercut = cuotaRepercut;
  }

  /**
   * @return the imFinSvb
   */
  public BigDecimal getImFinSvb() {
    return imFinSvb;
  }

  /**
   * @param imFinSvb the imFinSvb to set
   */
  public void setImFinSvb(BigDecimal imFinSvb) {
    this.imFinSvb = imFinSvb;
  }

  /**
   * @return the imFinDiv
   */
  public BigDecimal getImFinDiv() {
    return imFinDiv;
  }

  /**
   * @param imFinDiv the imFinDiv to set
   */
  public void setImFinDiv(BigDecimal imFinDiv) {
    this.imFinDiv = imFinDiv;
  }

  /**
   * @return the nbComentarios
   */
  public String getNbComentarios() {
    return nbComentarios;
  }

  /**
   * @param nbComentarios the nbComentarios to set
   */
  public void setNbComentarios(String nbComentarios) {
    this.nbComentarios = nbComentarios;
  }

  /**
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * @return the esModificable
   */
  public boolean isEsModificable() {
    return esModificable;
  }

  /**
   * @param esModificable the esModificable to set
   */
  public void setEsModificable(boolean esModificable) {
    this.esModificable = esModificable;
  }

  public boolean isEsAnulable() {
    return esAnulable;
  }

  public void setEsAnulable(boolean esAnulable) {
    this.esAnulable = esAnulable;
  }
}
