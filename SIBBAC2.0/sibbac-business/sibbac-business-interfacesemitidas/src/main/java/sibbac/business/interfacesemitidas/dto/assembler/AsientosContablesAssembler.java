package sibbac.business.interfacesemitidas.dto.assembler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.interfacesemitidas.dto.pantallas.AsientosContablesFilterDTO;

/**
 * 	Assembler para asientos contables.
 */
@Service
public class AsientosContablesAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AsientosContablesAssembler.class);
  
	private static DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");
  
	/**
	 * 	Realiza la conversion entre los valores tomados de la pantalla y el DTO.
	 * 	@param paramsObjects ->  Valores que recibe el service de la pantalla y que se quieren transformar en DTO.
	 * 	@return  Entidad con los datos del DTO tratados
	 */
	 public AsientosContablesFilterDTO getAsientosContablesFilterDto(Map<String, String> paramsFilters) {

		 AsientosContablesFilterDTO asientosContablesFilterDTO = new AsientosContablesFilterDTO();

		 if (paramsFilters.get("compania") != null) {
			 asientosContablesFilterDTO.setCompania(String.valueOf(paramsFilters.get("compania")));
		 }

		 if (paramsFilters.get("companiaNot") != null) {
			 asientosContablesFilterDTO.setCompaniaNot(String.valueOf(paramsFilters.get("companiaNot")));
		 }

		 if (paramsFilters.get("tipoFactura") != null) {
			 asientosContablesFilterDTO.setIdTipoFactura(Integer.valueOf(paramsFilters.get("tipoFactura")));
		 }

		 if (paramsFilters.get("tipoFacturaNot") != null) {
			 asientosContablesFilterDTO.setIdTipoFacturaNot(Integer.valueOf(paramsFilters.get("tipoFacturaNot")));
		 }

		 if (paramsFilters.get("fecha") != null) {
			 try {
				 asientosContablesFilterDTO.setFecha(dF.parse(paramsFilters.get("fecha")));
			 } catch (ParseException e) {
				 LOG.error("No valida la fecha", e);
			 }
		 }
		 return asientosContablesFilterDTO;
	 }
  
}
