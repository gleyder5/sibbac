package sibbac.business.interfacesemitidas.dto;

/**
 * DTO para la gestion de TMCT0_FACTURA_MANUAL
 * @author Neoris
 *
 */
public class ClienteDTO {
	
	/**
	 * Provincia del cliente
	 */
	private String provincia;
	
	/**
	 * País de residencia del cliente
	 */
	private String paisResidencia;
	
	/**
	 * Tipo de Residencia del cliente
	 */
	private String tipoResidencia;
	
	/**
	 * Número de documento del cliente
	 */
	private String numdocumento;
	
	/**
	 * Tipo de documento del cliente
	 */
	private String tpdocumento;

	public ClienteDTO () {

	}

	/**
	 * Constructor con parámetros
	 */
	public ClienteDTO(String provincia,
			String paisResidencia,
			String tipoResidencia,
			String numdocumento,
			String tpdocumento) {

		this.provincia = provincia;
		this.paisResidencia = paisResidencia;
		this.tipoResidencia = tipoResidencia;
		this.numdocumento = numdocumento;
		this.tpdocumento = tpdocumento;
	}

	/**
	 * @return the provincia
	 */
	public String getProvincia() {
		return provincia;
	}

	/**
	 * @param provincia the provincia to set
	 */
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	/**
	 * @return the paisResidencia
	 */
	public String getPaisResidencia() {
		return paisResidencia;
	}

	/**
	 * @param paisResidencia the paisResidencia to set
	 */
	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	/**
	 * @return the tipoResidencia
	 */
	public String getTipoResidencia() {
		return tipoResidencia;
	}

	/**
	 * @param tipoResidencia the tipoResidencia to set
	 */
	public void setTipoResidencia(String tipoResidencia) {
		this.tipoResidencia = tipoResidencia;
	}

	/**
	 * @return the numdocumento
	 */
	public String getNumdocumento() {
		return numdocumento;
	}

	/**
	 * @param numdocumento the numdocumento to set
	 */
	public void setNumdocumento(String numdocumento) {
		this.numdocumento = numdocumento;
	}

	/**
	 * @return the tpdocumento
	 */
	public String getTpdocumento() {
		return tpdocumento;
	}

	/**
	 * @param tpdocumento the tpdocumento to set
	 */
	public void setTpdocumento(String tpdocumento) {
		this.tpdocumento = tpdocumento;
	}

	
}
