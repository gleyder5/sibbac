package sibbac.business.interfacesemitidas.dto.pantallas;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 *	DTO que transfiere los filtros de la modal de asiento contable.
 */
public class DetalleAsientoContableDTO {

	private String cuentaMayor;
	private String cuentaMayorName;
	private String descripcion;
	private String importe;
	private String tipoMovimiento;
	private String factura;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public DetalleAsientoContableDTO() {
		super();
	}

	public DetalleAsientoContableDTO(String cuentaMayor, String auxiliar,
			String descripcion, String importe, String tipoMovimiento,
			String factura) {
		super();
		this.cuentaMayor = cuentaMayor;
		this.cuentaMayorName = auxiliar;
		this.descripcion = descripcion;
		this.importe = importe;
		this.tipoMovimiento = tipoMovimiento;
		this.factura = factura;
	}

	public String getCuentaMayor() {
		return this.cuentaMayor;
	}

	public void setCuentaMayor(String cuentaMayor) {
		this.cuentaMayor = cuentaMayor;
	}

	public String getCuentaMayorName() {
		return this.cuentaMayorName;
	}

	public void setCuentaMayorName(String cuentaMayorName) {
		this.cuentaMayorName = cuentaMayorName;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImporte() {
		return this.importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getTipoMovimiento() {
		return this.tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getFactura() {
		return this.factura;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}
	
	/**
	 *	Convierte a DTO un array de Object con propiedades.
	 *	@param obj
	 *	@param descrAli: Descripcion alias
	 *	@return DetalleAsientoContableDTO  
	 */
	public static DetalleAsientoContableDTO convertObjectToDetalleAsientoContableDTO(
		Object[] obj) {
		DetalleAsientoContableDTO detalleAsientoContableDTO = new DetalleAsientoContableDTO();
		detalleAsientoContableDTO.setCuentaMayor((String) obj[0]);
		detalleAsientoContableDTO.setCuentaMayorName((String) obj[1]);
		detalleAsientoContableDTO.setImporte(String.format("%,.2f", ((BigDecimal) obj[2]).setScale(2, RoundingMode.DOWN)));
		detalleAsientoContableDTO.setTipoMovimiento(String.valueOf((Character) obj[3]));
		detalleAsientoContableDTO.setFactura(String.valueOf((BigInteger) obj[4]));
		detalleAsientoContableDTO.setDescripcion((String) obj[5]);
		// cdaliass + nombreAlias + nbdocnumero
		
		return detalleAsientoContableDTO;
	}
	
}