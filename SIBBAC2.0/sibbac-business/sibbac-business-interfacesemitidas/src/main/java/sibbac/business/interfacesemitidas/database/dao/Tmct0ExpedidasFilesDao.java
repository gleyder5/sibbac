package sibbac.business.interfacesemitidas.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.interfacesemitidas.database.model.Tmct0ExpedidasFiles;

/**
 * Data access object de Tmct0ExpedidasFiles.
 */
@Repository
public interface Tmct0ExpedidasFilesDao extends JpaRepository<Tmct0ExpedidasFiles, Long> {
	
	@Query( "SELECT tExp FROM Tmct0ExpedidasFiles tExp WHERE tExp.nbFichero = :tExp" )
	public List<Tmct0ExpedidasFiles> findByNombreFichero(@Param( "tExp" ) String tExp);
	
}
