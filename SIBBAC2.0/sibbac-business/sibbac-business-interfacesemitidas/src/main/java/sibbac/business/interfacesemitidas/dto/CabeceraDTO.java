package sibbac.business.interfacesemitidas.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sibbac.business.interfacesemitidas.enums.InterfacesEmitidasEnums;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.common.StringHelper;

/**
 * Bloque 00
 * 
 * @author adrian.prause
 *
 */
public class CabeceraDTO {

	private String tipoRegistro;
	private String interfaz;
	private Date fechaGeneracionFichero;
	private String empresaDeclarante;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public CabeceraDTO() {
		super();
	}

	/**
	 *	Constructor con argumentos. 
	 */
	public CabeceraDTO(String tipoRegistro, String interfaz,
			Date fechaGeneracionFichero, String empresaDeclarante) {
		super();
		this.tipoRegistro = tipoRegistro;
		this.interfaz = interfaz;
		this.fechaGeneracionFichero = fechaGeneracionFichero;
		this.empresaDeclarante = empresaDeclarante;
	}
	
	public String getFechaFormatoISO() {
		SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO_GUIONES);
		return format.format(fechaGeneracionFichero);
	}
	
	public String getFechaFormatoFechaFichero() {
		SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
		return format.format(fechaGeneracionFichero);
	}

	@Override
	public String toString() {
		String cabecera = "";
		try {
			cabecera = StringHelper.padRight(tipoRegistro, 2) + StringHelper.padRight(interfaz, 3)
					+ getFechaFormatoISO() + StringHelper.padRight(empresaDeclarante, 4);
			if(this.interfaz.equals(InterfacesEmitidasEnums.InterfazEnum.EXPEDIDA.getId())){
				return StringHelper.padRight(cabecera, 2449);
			}else{
				return StringHelper.padRight(cabecera, 739);
			}
		} catch (Exception e) {
			return "";
		}
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getInterfaz() {
		return interfaz;
	}

	public void setInterfaz(String interfaz) {
		this.interfaz = interfaz;
	}

	public Date getFechaGeneracionFichero() {
		return fechaGeneracionFichero;
	}

	public void setFechaGeneracionFichero(Date fechaGeneracionFichero) {
		this.fechaGeneracionFichero = fechaGeneracionFichero;
	}

	public String getEmpresaDeclarante() {
		return empresaDeclarante;
	}

	public void setEmpresaDeclarante(String empresaDeclarante) {
		this.empresaDeclarante = empresaDeclarante;
	}

	/**
	 *	Popula DTO cabecera segun la fila con datos.
	 *	@param line
	 * 	@throws ParseException 
	 */
	public void popularCabeceraDTO(String line) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_FECHA_ISO);
		this.tipoRegistro = line.substring(0, 2);
		this.interfaz = line.substring(2, 5);
		this.fechaGeneracionFichero = format.parse(line.substring(5, 15));
		this.empresaDeclarante = line.substring(15, 19);
	}
	
}
