package sibbac.business.interfacesemitidas.enums;

/**
 * Clase con las enumeraciones utilizadas en Interfaces Emitidas sibbac.
 */
public class InterfacesEmitidasEnums {

	/**
	 * 	Enumeracion con codigos de retorno BLOQUE 01 - DATOS FACTURA
	 */
	public static enum CodigosRetornoBloque01DatosFacturaEnum {

		OPERACION_ACEPTADA("00", "Operación aceptada"), 
		OPERACION_ACEPTADA_CON_ERRORES("10", "Operación aceptada con errores"), 
		OPERACION_RECHAZADA("20", "Operación rechazada"), 
		ERROR_DB2("99", "Error DB2");

		private String id, description;

		private CodigosRetornoBloque01DatosFacturaEnum(
				String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}
	}
	
	/**
	 *	Enumeracion para tipo de registro indicado en los bloques.
	 */
	public static enum TipoRegistroEnum {
		
		CABECERA("00", "CABECERA"),
		BLOQUE_01("01", "BLOQUE 01"),
		BLOQUE_02("02", "BLOQUE 02"),
		BLOQUE_04("04", "BLOQUE 04"),
		BLOQUE_06("06", "BLOQUE 06"),
		BLOQUE_07("07", "BLOQUE 07"),
		BLOQUE_09("09", "BLOQUE 09");
		
		private String id, description;

		private TipoRegistroEnum(
				String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}
	}
	
	/**
	 * 	Enumeracion para tipo de error en BLOQUE 02 - ERRORES.
	 */
	public static enum TipoErrorBloque02ErroresEnum {
		
		FALTA_ATTR_OBLIGATORIO("10", "Falta atributo obligatorio"),
		ERROR_FORMATO_ATTR("20", "Error en formato de atributo"),
		ERROR_VALOR_ATTR("30", "Error en valor de atributo"),
		ERROR_VALIDACION_ATTR("40", "Error en validación de atributo");
		
		private String id, description;

		private TipoErrorBloque02ErroresEnum(
				String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}
		
	}
	
	/**
	 * 	Enumeracion para clase de error en BLOQUE 02 - ERRORES.
	 */
	public static enum ClaseErrorBloque02ErroresEnum {
		
		ERROR_NO_BLOQUEANTE("AV", "Error No Bloqueante"),
		ERROR_BLOQUEANTE("ER", "Error Bloqueante");
		
		private String id, description;

		private ClaseErrorBloque02ErroresEnum(
				String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}
	}
	
	/**
	 * 	Enumeracion para codigo de error en BLOQUE 02 - ERRORES.
	 */
	public static enum CodigoErrorBloque02ErroresEnum {
		
		ERROR_ATRIBUTOS("1", "Error atributos"),
		ERROR_VALIDACIONES("2", "Error en validaciones"),
		ERROR_TECNICO("9", "Error técnico");
		
		private String prefix, description;

		private CodigoErrorBloque02ErroresEnum(
				String _prefix, String _description) {
			prefix = _prefix;
			description = _description;
		}
		
		public String getPrefix() {
			return prefix;
		}

		public String getDescription() {
			return description;
		}
	}
	
	/**
	 * 	Enumeracion para retorno de errores.
	 */
	public static enum RetornoErroresEnum {
		
		ERROR_DB2(
				"40", "ER", "90001", "Error DB2"),
		FALTA_ATTR_OBLIGATORIO(
				"10", "ER", "10001", "Falta atributo obligatorio"),
		FORMATO_ATTR_INCORRECTO(
				"20", "ER", "10002", "Formato del atributo es incorrecto"),
		VALOR_ATTR_INCORRECTO(
				"30", "ER/AV", "10003", "El valor del atributo no es correcto"),
		REPRESENTANTE_LEGAL_MENOR(
				"40", "ER", "20001", "Al tratarse de un menor, la identificación del representante legal debe contener valor"),
		OPERACION_DUPLICADA(
				"40", "ER", "20002", "Operación duplicada"),
		OPERACION_DADA_BAJA(
				"40", "ER", "20003", "Operación ya dada de baja"),
		OPERACION_INEXISTENTE(
				"40", "ER", "20004", "Operación no existe"),
		CODIGO_PAIS_OBLIGATORIO_IDENTIF_NO_NIF_IVA(
				"40", "ER", "20005", "Código de País es obligatorio cuando Tipo Identificación es distinto de NIF-IVA"),
		FECHA_EXPED_MAYOR_FECHA_ACTUAL(
				"40", "ER", "20006", "La Fecha de Expedición es Superior a la fecha actual"),
		NUMSERIEFACTURAEMISORRESUMENFIN_SIN_DECLARAR(
				"40", "ER", "20007", "Tipo factura es de Asientos Resumen, NumSerieFacturaEmisorResumenFin está sin declarar"),
		TIPO_FACTURA_NO_ASIENTOS_RESUMEN(
				"40", "ER", "20008", "El tipo de factura no es Asientos Resumen y tiene Número Serie Factura Emisor Resumen Fin declarado"),
		NO_COINCIDE_IDENTIF_MENOR_REPRES_LEGAL(
				"40", "ER", "20009", "No pueden coincidir la identificación del menor y su representante legal"),
		VALOR_ATTR_EMISOR_FACTURA_SOLO_02(
				"40", "ER", "20010", "Valor del atributo Emisor factura - IDType - IDOtro sólo puede valer 02"),
		FACTURA_RECTIF_TIPO_RECTIF_SIN_VALOR(
				"40", "ER", "20011", "Si la factura es de tipo rectificativa, el Campo Tipo Rectificativa debe tener valor"),
		FACTURA_NO_RECTIF_TIPO_RECTIF_CON_VALOR(
				"40", "ER", "20012", "Si la factura no es de tipo rectificativa, el Campo Tipo Rectificativa no debe tener valor"),
		FALTA_FACTURAS_AGRUPADAS_FACTURA_EMITIDA(
				"40", "ER", "20013", "Debe informarse la lista de facturas Agrupadas sólo si la factura es de tipo Factura emitida en sustitución de facturas simplificadas facturadas y declaradas"),
		FACTURA_NO_RECTIF_CON_LISTA_FACTURAS_RECTIF(
				"40", "ER", "20014", "Si la factura no es de tipo Rectificativa, la lista de Facturas Rectificadas no podrá venir informado"),
		FACTURA_REF_SUST_IMPORTE_RECTIF_NO_OBLIG(
				"40", "ER", "20015", "Si la factura es de tipo Rectificativa por sustitución, el importe de rectificación es obligatorio"),
		FACTURA_NO_REF_SUST_IMPORTE_RECTIF_SIN_VALOR(
				"40", "ER", "20016", "Si la factura no es de tipo Rectificativa por Sustitución, el bloque de Importe Rectificación no debe tener valor"),
		PARTE_SUJETA_EXENTA_NO_EXENTA(
				"40", "ER", "20017", "Las operaciones podrán tener dentro de la parte sujeta, parte exenta y/o parte no exenta. Por tanto, puede aparecer solo un bloque o ambos, pero al menos debe aparecer uno (Exenta y/o No Exenta)"),
		PARTE_SUJETA_NO_SUJETA(
				"40", "ER", "20018", "Las operaciones podrán tener parte sujeta y parte no sujeta. Por tanto, puede aparecer solo un bloque o ambos, pero al menos debe aparecer uno (Sujeta y/o No sujeta)"),
		CODIGO_PAIS_NIF_IVA_NO_COINCIDE_ID(
				"40", "ER", "20019", "El código del país indicado para la identificación del NIF-IVA no coincide con los dos primeros caracteres del ID"),
		DESGLOSE_TIPO_OPER_PRESTACION(
				"40", "ER", "20020", "Desglose tipo de Operación necesita al menos Prestación de Servicio o Entrega/Adquisición o ambas"),
		SIN_PAGOS_FACTURAS_BAJA(
				"40", "ER", "20021", "No se pueden incluir pagos de facturas dadas de baja"),
		SIN_PAGOS_FACTURAS_CRITERIO_CAJA(
				"40", "ER", "20022", "No se pueden incluir pagos de facturas cuyo régimen no corresponde a criterio de caja"),
		EMPR_DECLARANTE_NO_EXISTE(
				"40", "ER", "20023", "Empresa Declarante no existente entre las identificadas como declarantes para Organismo Oficial"),
		OPER_EXCEPCIONADA_OPER_BANCARIA_FACT_NO_RECTIF(
				"40", "ER", "20024", "Si se trata de una operación Excepcionada por operativa bancaria , el tipo de factura no puede ser rectificativa (R1/R2/R3/R4/R5/RR)"),
		TIPO_FACT_RECTIF_OPER_EXCEPCIONADA(
				"40", "ER", "20025", "El tipo de factura no puede ser rectificativa (R1/R2/R3/R4/R5/RR) si se trata de una operación excepcionada"),
		COMISIONES_DIV_GEOF_OFICINA_OPER_OBLIGATORIO(
				"40", "ER", "20026", "Si se trata de una prestación de servicios (comisiones) el bloque de la división geográfica de la oficina es obligatorio"),
		NO_INFORMAR_BLOQUE_DIV_GEOGR_COMIS(
				"40", "ER", "20027", "El bloque de la división geográfica de la oficina no se debe informar si no se trata de una prestación de servicios (comisiones)"),
		BLOQUE_DIV_GEOGR_OBLIGATORIO(
				"40", "ER", "20029", "Si se trata de una entrega/adquisición de bienes el bloque de la división geográfica del domicilio del bien es obligatorio"),
		NO_INFORMAR_BLOQUE_DIV_GEOGR_ENTR_ADQ(
				"40", "ER", "20030", "El bloque de la división geográfica del domicilio del bien no se debe informar si no se trata de una entrega/adquisición de bienes"),
		ENVIO_INFO_CONTABÑE_SIN_INTERF(
				"40", "ER", "20031", "Se ha indicado que se envía información contable y no se ha recibido la interface correspondiente"),
		BLOQUE_EMISOR_SIN_FACTURA_RECIBIDA(
				"40", "ER", "20032", "Si no se trata de una factura recibida no se debe informar el bloque de emisor de la factura"),
		BLOQUE_EMISOR_FACTURA_NO_INFORMAR(
				"40", "ER", "20033", "El bloque de emisor de la factura no se debe informar si no se trata de una factura recibida"),
		EMISOR_FACT_RESID_EMIS_FACT(
				"40", "ER", "20034", "Se tiene que informar Emisor factura - Identificación Residente o Emisor factura - ID-Otro"),
		NO_INFORMAR_BCC_ESTADO_CONT(
				"40", "ER", "20035", "Si ha el estado de contabilización es distinto de 2 no se debe informar el bloque de la cuenta contable");
		
//		40	ER	20036	El bloque de la cuenta contable no se debe informar si el estado de contabilización es igual a 2
//		40	ER	20037	El bloque de CVC no se debe informar si el estado de contabilización es igual a 2
//		40	ER	20038	El bloque de Subcuenta no se debe informar si el estado de contabilización es igual a 2
//		40	ER	20039	Si ha el estado de contabilización es distinto de 1 no se debe informar el importe por diferencia contabilizado 
//		40	ER	20040	El importe por diferencia contabilizado no se debe informar si el estado de contabilización es distinto a 1
//		40	ER	20041	Si la información contable no es de un impuesto  no se debe informar el bloque Impuesto asociado
//		40	ER	20042	El bloque de impuesto asociado no se debe informar si el estado si la información contable no es de un impuesto
//		40	ER	20043	Si el tipo de comunicación es modificación o baja se podrá informar la Clave única de identificación de la operación en origen (a modificar o dar de baja) o el bloque de Identificación factura.
//		40	ER	20044	Si el tipo de comunicación es alta no se podrá informar la Clave única de identificación de la operación en origen (a modificar o dar de baja).
//		40	ER	20045	Si ha el tipo de retención es igual a 0 no se debe informar el importe de retención
//		40	ER	20046	El importe de retención  no se debe informar si el tipo de retención es igual a 0
//		40	ER	20047	Si ha el tipo de retención es igual a 0 no se debe informar el porcentaje de retención
//		40	ER	20048	El porcentaje de retención  no se debe informar si el tipo de retención es igual a 0
//		40	ER	20049	Si no se trata de una adquisición de un bien no se debe informar el bloque Identificación de la adquisición
//		40	ER	20050	El bloque Identificación de la adquisición no se debe informar si no se trata de una adquisición de un bien
//		40	ER	20051	Si no se trata de una entrega de un bien no se debe informar el bloque Identificación de la entrega
//		40	ER	20052	El bloque Identificación de la entrega no se debe informar si no se trata de una entrega de un bien
//		40	ER	20053	Si se trata de un pago en metálico asociado a una factura expedida  se debe informar el bloque Identificación factura
//		40	ER	20054	El bloque Identificación factura se debe informar si no se trata  de un pago en metálico asociado a una factura expedida 
//		40	ER	20055	Si se trata de un error en una operación que tiene una factura asociada  se debe informar el bloque Identificación factura
//		40	ER	20056	El bloque Identificación factura se debe informar si  se trata  de un un error en una operación 
//		40	AV	20057	Si el campo ClaveRegimenEspecialOTrascendencia tiene un valor de 14, el bloque de datos Inmueble es obligatorio
//		40	AV	20058	No se debe informar el campo base Imponible A Coste en facturas expedidas si el campo ClaveRegimenEspecialOTrascendencia tiene un valor distinto de 07
//		40	AV	20059	La contraparte es obligatorio siempre que la factura no sea de tipo simplificada o asiento resumen
//		40	AV	20060	Error si ClaveRegimenEspecialOTranscedencia distinto de 14 y se incluye el bloque de Inmuebles
//		40	AV	20061	Alguna de las facturas rectificadas no existen en el sistema
//		40	AV	20062	No se debe informar el campo base Imponible A Coste en facturas recibidas si el campo ClaveRegimenEspecialOTrascendencia tiene un valor distinto de 07
//		40	AV	20063	La factura contiene un desglose a nivel de factura cuando le corresponde un desglose a nivel de operación, por ser una factura no simplificada ni asiento resumen y la contraparte contiene un IdOtro o un NIF que empiece por N
//		40	AV	20064	Si el campo ClaveRegimenEspecialOTrascendencia tiene un valor de 15, el bloque de datos Inmueble es obligatorio
//		40	AV	20065	Error si ClaveRegimenEspecialOTranscedencia distinto de 15 y se incluye el bloque de Inmuebles
		
		private String tipoError, claseError, codigoError, descripcion;

		private RetornoErroresEnum(
				String _tipoError, String _claseError, String _codigoError, String _descripcion) {
			tipoError = _tipoError;
			claseError = _claseError;
			codigoError = _codigoError;
			descripcion = _descripcion;
		}

		public String getTipoError() {
			return tipoError;
		}

		public String getClaseError() {
			return claseError;
		}
		
		public String getCodigoError() {
			return codigoError;
		}
		
		public String getDescripcion() {
			return descripcion;
		}
	}
	
	/**
	 * Enumeracion para Interfaz bloque 00.
	 */
	public static enum InterfazEnum {

		EXPEDIDA("EXP", "EXPEDIDAS"),
		ANULADAS("EXB", "BAJAXPEDIDAS"),
		SII_EXPEDIDAS("SII EXPEDIDAS", "SII EXPEDIDAS"),
		GC_EXPEDIDAS("GI EXPEDIDAS", "GC EXPEDIDAS");

		private String id, description;

		private InterfazEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para Empresa declarante bloque 02.
	 */
	public static enum EmpresaDeclaranteEnum {

		CARGABAL("0402", "0402 - CARGABAL");

		private String id, description;

		private EmpresaDeclaranteEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para Clave regimen especial bloque 02.
	 */
	public static enum ClaveRegimenEspecialEnum {

		CLAVE_01(1, "Operación de régimen común."), CLAVE_06(6, "Régimen especial grupo de entidades en IVA.");

		private Integer id;
		private String description;

		private ClaveRegimenEspecialEnum(Integer _id, String _description) {
			id = _id;
			description = _description;
		}

		public Integer getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para indicador de entrega.
	 */
	public static enum IndicadorEntregaEnum {

		BIEN("1", "Entregado bien");

		private String id, description;

		private IndicadorEntregaEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para Códigos de impuesto.
	 */
	public static enum CodigoImpuestoEnum {

		CODIGO_09("09", "Impuesto código 09"),
		CODIGO_01("01", "Impuesto código 01");

		private String id, description;

		private CodigoImpuestoEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para Subtipos de impuesto.
	 */
	public static enum SubtipoImpuestoEnum {

		SUBTIPO_01("01", "Subtipo código 09");

		private String id, description;

		private SubtipoImpuestoEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para Subtipos de impuesto.
	 */
	public static enum CodigoPostalEnum {

		OTROS("99", "Otros"), MADRID("28", "Madrid");

		private String id, description;

		private CodigoPostalEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * Enumeracion para Subtipos de impuesto.
	 */
	public static enum TipoComunicacionEnum {

		A0("A0", "Alta"),
		A1("A1", "Modificación");

		private String id, description;

		private TipoComunicacionEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}
	
	/**
	 * Enumeracion para tipos de factura
	 */
	public static enum TipoFacturaEnum {

		F1("F1", "Alta");

		private String id, description;

		private TipoFacturaEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}
	
	/**
	 * Enumeracion para Moneda
	 */
	public static enum MonedaEnum {

		EUR("EUR", "Euro");

		private String id, description;

		private MonedaEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}
	
	/**
	 * Enumeracion para Causa desglose factura
	 */
	public static enum CausaDesgloseFacturaEnum {

		E1("E1", "Exentas");

		private String id, description;

		private CausaDesgloseFacturaEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}
	
	/**
	 * Enumeracion para indicador de excepcion.
	 */
	public static enum IndicadorExcepcionEnum {

		EXCEPCIONADA("1", "Excepcionada");

		private String id, description;

		private IndicadorExcepcionEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}
	
	/**
	 * Enumeracion para indicador de excepcion.
	 */
	public static enum OrigenOperacionEnum {

		VA("VA", "VA");

		private String id, description;

		private OrigenOperacionEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}
	
	/**
	 * Enumeracion para indicador de excepcion.
	 */
	public static enum IndicadorDatosContableEnum {

		NO_SE_ENVIA_OPERACION("0", "No se envia");

		private String id, description;

		private IndicadorDatosContableEnum(String _id, String _description) {
			id = _id;
			description = _description;
		}

		public String getId() {
			return id;
		}

		public String getDescription() {
			return description;
		}

	}

	/**
	 * NIF, NIE, CIF, BIC, OTROS
	 * 
	 */
	public static enum TipoCodigoIdentificacion {
		
		NIF('N', "01"), NIE('E', "01"), CIF('C', "01"), BIC('B', "06"), OTROS('O', "06");

		private Character codigo;
		private String equivalencia;

		public static TipoCodigoIdentificacion fromValue(Character codigo) {
			if (NIF.getCodigo().equals(codigo)) {
				return NIF;
			}
			if (NIE.getCodigo().equals(codigo)) {
				return NIE;
			}
			if (CIF.getCodigo().equals(codigo)) {
				return CIF;
			}
			if (BIC.getCodigo().equals(codigo)) {
				return BIC;
			}
			if (OTROS.getCodigo().equals(codigo)) {
				return OTROS;
			}
			return OTROS;
		}
		
		private TipoCodigoIdentificacion(Character codigo, String equivalencia) {
			this.codigo = codigo;
			this.equivalencia = equivalencia;
		}

		public Character getCodigo() {
			return codigo;
		}

		public void setCodigo(Character codigo) {
			this.codigo = codigo;
		}

		public String getEquivalencia() {
			return equivalencia;
		}

		public void setEquivalencia(String equivalencia) {
			this.equivalencia = equivalencia;
		}
		
	}
	  
}