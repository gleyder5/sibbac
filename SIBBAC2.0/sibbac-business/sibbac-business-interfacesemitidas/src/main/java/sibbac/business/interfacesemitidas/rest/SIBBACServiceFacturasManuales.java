package sibbac.business.interfacesemitidas.rest;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.interfacesemitidas.database.dao.Tmct0BajasManualesDao;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAO;
import sibbac.business.interfacesemitidas.database.dao.Tmct0FacturasManualesDAOImpl;
import sibbac.business.interfacesemitidas.database.dao.Tmct0InterfacesEmitidasConstDao;
import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.dto.ClienteDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FacturasManualesTablaDTO;
import sibbac.business.interfacesemitidas.dto.Tmct0FiltroFacturasManualesDTO;
import sibbac.business.interfacesemitidas.dto.assembler.Tmct0FacturasManualesAssembler;
import sibbac.business.interfacesemitidas.dto.assembler.Tmct0RectificativasManualesAssembler;
import sibbac.business.interfacesemitidas.helper.GeneradorPdf;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceFacturasManuales implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceFacturasManuales.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0FacturasManualesAssembler tmct0FacturasManualesAssembler;

  @Autowired
  Tmct0RectificativasManualesAssembler tmct0RectificativasManualesAssembler;

  @Autowired
  Tmct0FacturasManualesDAO tmct0FacturasManualesDao;

  @Autowired
  Tmct0BajasManualesDao tmct0BajasManualesDao;

  @Autowired
  Tmct0FacturasManualesDAOImpl tmct0FacturasManualesDaoImpl;

  @Autowired
  private AliasDao aliasdao;

  @Autowired
  private Tmct0aliDao tmct0AliDao;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private Tmct0InterfacesEmitidasConstDao interfacesEmitidasConstDao;
  
  @Autowired
  private GeneradorPdf generadorPdf;

  /**
   * The Enum ComandoFacturasManuales.
   */
  private enum ComandoFacturasManuales {

    /** Crear un factura manual con los datos Recibidos */
    CREAR_FACTURA_MANUAL("crearFacturaManual"),

    /** Modificar un factura manual con los datos Recibidos */
    MODIFICAR_FACTURA_MANUAL("modificarFacturaManual"),

    /** Borrar un factura manual con los datos Recibidos */
    BORRAR_FACTURA_MANUAL("borrarFacturasManuales"),

    /** Consultar un factura manual con los datos Recibidos */
    CONSULTAR_FACTURAS_MANUALES("consultarFacturasManuales"),

    /** Consulta el detalle de una factura manual con los datos Recibidos */
    DETALLE_FACTURA_MANUAL("detalleFacturaManual"),

    /** Consulta del cliente */
    CONSULTA_CLIENTE("consultarCliente"),

    /** crear y contabilizar */
    CONTABILIZAR_CREAR("crearContabilizar"),

    /** modificar y contabilizar */
    CONTABILIZAR_MODIFICAR("modificarContabilizar"),

    /** Contabilizar */
    CONTABILIZAR("contabilizar"),

    /** Consulta el próximo número de factura que se asignará */
    CONSULTAR_PROX_NUMFACT("consultarProximoNumeroFacturaManual"),
    
    /** Consulta el próximo número de factura grupo iva que se asignará */
    CONSULTAR_PROX_NUMFACT_GRUPO_IVA("consultarProximoNumeroFacturaManualGrupoIva"),

    /** Exportar la factura a PDF */
    EXPORTAR_PDF("exportarPdf"),
    
    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandoFacturasManuales(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandoFacturasManuales command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceFacturasManuales");

      switch (command) {
        case CONSULTAR_FACTURAS_MANUALES:
          result.setResultados(consultarFacturasManuales(paramsObjects));
          break;
        case CREAR_FACTURA_MANUAL:
          result.setResultados(crearFacturaManual(paramsObjects));
          break;
        case MODIFICAR_FACTURA_MANUAL:
          result.setResultados(modificarFacturaManual(paramsObjects));
          break;
        case DETALLE_FACTURA_MANUAL:
          result.setResultados(detalleFacturaManual(paramsObjects));
          break;
        case BORRAR_FACTURA_MANUAL:
          result.setResultados(eliminarFacturasManuales(paramsObjects));
          break;
        case CONSULTA_CLIENTE:
          result.setResultados(consultarCliente(paramsObjects));
          break;
        case CONTABILIZAR_CREAR:
          result.setResultados(crearYContabilizar(paramsObjects));
          break;
        case CONTABILIZAR_MODIFICAR:
          result.setResultados(modificarYContabilizar(paramsObjects));
          break;
        case CONTABILIZAR:
          result.setResultados(contabilizar(paramsObjects, true));
          break;
        case CONSULTAR_PROX_NUMFACT:
          result.setResultados(consultarProxNumFact(false));
          break;
        case CONSULTAR_PROX_NUMFACT_GRUPO_IVA:
            result.setResultados(consultarProxNumFact(true));
            break;
        case EXPORTAR_PDF:
          result.setResultados(exportarPdf(paramsObjects));
        break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceFacturasManuales");
    return result;
  }

  /**
   * Consulta de las facturas manuales a raiz de los datos insertados en pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarFacturasManuales(Map<String, Object> paramObjects) throws SIBBACBusinessException,
                                                                                         ParseException {

    LOG.info("INICIO - SERVICIO - consultarFacturasManuales");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaFacturasManualesTablaDTO = new ArrayList<Object>();
    List<String> listCdAliass = new ArrayList<String>();
    List<Tmct0FacturasManuales> listaFacturasManuales = new ArrayList<Tmct0FacturasManuales>();
    List<Tmct0BajasManuales> listaFacturasManualesBaja = new ArrayList<Tmct0BajasManuales>();

    try {
      // Transformamos los parámetros de filtrado en un objeto
      Tmct0FiltroFacturasManualesDTO filtroFacturasManuales = tmct0FacturasManualesAssembler.getTmct0FiltroFacturasManualesDTO(paramObjects);

      // Recuperamos el ID ESTADO que pertenece al literal FACTURA BAJA
      Tmct0estado estadoFAct = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_BAJA)
                                                                                .getValor());

      // Si en el filtro se ha seleccionado el estado FACTURA BAJA se recuperará el listado de la tabla
      // FACTURA_MANUAL_RECTIFICATIVA_BAJA
      if (null != filtroFacturasManuales && null != filtroFacturasManuales.getIdEstado()
          && filtroFacturasManuales.getIdEstado().equals(estadoFAct.getIdestado())) {
        listaFacturasManualesBaja = tmct0FacturasManualesDaoImpl.consultarFacturasManualesBaja(filtroFacturasManuales);

        // Se guardan en un array todos los idAlias (cdAliass) para recuperar las descripciones asociadas a ellos
        for (Tmct0BajasManuales tmct0FacturasManualesBaja : listaFacturasManualesBaja) {
          listCdAliass.add(tmct0FacturasManualesBaja.getIdAlias().toString());
        }

        // Se transforman a lista de tablaDTO
        for (int i = 0; i < listaFacturasManualesBaja.size(); i++) {
          try {
            String descrAli = aliasdao.findDescraliByCdaliass(listCdAliass.get(i));
            Tmct0FacturasManualesTablaDTO facturasManualesTasblaDTO = tmct0FacturasManualesAssembler.getTmct0FacturasManualesTablaDTO(listaFacturasManualesBaja.get(i),
                                                                                                                                      descrAli);
            listaFacturasManualesTablaDTO.add(facturasManualesTasblaDTO);
          } catch (Exception e) {
            LOG.error("Error metodo consultarFacturasManuales - " + e.getMessage(), e);
            throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
          }
        }
      } else {
        // Realizamos la llamada a la consulta de facturas manuales
        listaFacturasManuales = tmct0FacturasManualesDaoImpl.consultarFacturasManuales(filtroFacturasManuales);

        // Se guardan en un array todos los idAlias (cdAliass) para recuperar las descripciones asociadas a ellos
        for (Tmct0FacturasManuales tmct0FacturasManuales : listaFacturasManuales) {
          listCdAliass.add(tmct0FacturasManuales.getIdAlias().toString());
        }

        // Se transforman a lista de tablaDTO
        for (int i = 0; i < listaFacturasManuales.size(); i++) {
          try {
            String descrAli = aliasdao.findDescraliByCdaliass(listCdAliass.get(i));
            Tmct0FacturasManualesTablaDTO facturasManualesTasblaDTO = tmct0FacturasManualesAssembler.getTmct0FacturasManualesTablaDTO(listaFacturasManuales.get(i),
                                                                                                                                      descrAli);
            listaFacturasManualesTablaDTO.add(facturasManualesTasblaDTO);
          } catch (Exception e) {
            LOG.error("Error metodo consultarFacturasManuales - " + e.getMessage(), e);
            throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
          }
        }
      }

    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo consultarFacturasManuales -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("listaFacturasManuales", listaFacturasManualesTablaDTO);

    LOG.info("FIN - SERVICIO - consultarFacturasManuales");

    return result;
  }

  /**
   * Crea una factura manual a raiz de los datos insertados en pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> crearFacturaManual(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
                                                                                   ParseException {

    LOG.info("INICIO - SERVICIO - crearFacturaManual");

    Map<String, Object> result = new HashMap<String, Object>();

    try {
      // Transformamos los parámetros de la visual en Entidad
      Tmct0FacturasManuales filtroFacturaManual = tmct0FacturasManualesAssembler.getTmct0FacturasManualesEntidad(paramsObjects,
                                                                                                                 false);

      // Comprobamos si el alias existe
      List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroFacturaManual.getIdAlias()));

      // Si no existe el alias se informa al usuario
      if (null == listaAlias || listaAlias.size() == 0) {
        LOG.error("Se esta intentanto crear una factura manual con con una empresa contraparte que no existe -  "
                  + filtroFacturaManual.getNbDocNumero());
        result.put("existeAlias", "NO");
        return result;
      }

      // Comprobamos si el número de factura ya existe
      List<Tmct0FacturasManuales> listaFacturasManuales = tmct0FacturasManualesDao.findBynbDocNumero(filtroFacturaManual.getNbDocNumero());

      // Si ya hay una factura con ese número se informa
      if (listaFacturasManuales.size() > 0) {
        LOG.error("Se esta intentanto crear una factura manual con el mismo número de factura que una ya existente -  "
                  + filtroFacturaManual.getNbDocNumero());
        throw new SIBBACBusinessException(
                                          "Se esta intentanto crear una factura manual con el mismo número de factura que una ya existente "
                                              + filtroFacturaManual.getNbDocNumero());
      }

      Tmct0estado estadoFActNew = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_DEFECTO_CREACION)
                                                                                   .getValor());

      if (estadoFActNew != null) {
        filtroFacturaManual.setEstado(estadoFActNew);
      } else {
        throw new SIBBACBusinessException(
                                          "No se ha podido determinar el estado inicial de la factura, consulte con el administrador ");
      }

      // Realizamos la llamada a la creación de factura manual
      tmct0FacturasManualesDao.save(filtroFacturaManual);// crearFacturaManual(filtroFacturaManual);

      // Indicamos que el resultado de la creación en la BBDD ha sido correcto
      result.put("status", "OK");

    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo crearFacturaManual -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - crearFacturaManual");

    return result;
  }

  /**
   * Modifica una factura manual a raiz de los datos insertados en pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> modificarFacturaManual(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
                                                                                       ParseException {

    LOG.info("INICIO - SERVICIO - modificarFacturaManual");

    Map<String, Object> result = new HashMap<String, Object>();

    try {
      // Transformamos los parámetros de la visual en Entidad
      Tmct0FacturasManuales filtroFacturaManual = tmct0FacturasManualesAssembler.getTmct0FacturasManualesEntidad(paramsObjects,
                                                                                                                 true);

      // Comprobamos si el alias existe
      List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroFacturaManual.getIdAlias()));

      // Si no existe el alias se informa al usuario
      if (null == listaAlias || listaAlias.size() == 0) {
        LOG.error("Se esta intentanto crear una factura manual con con una empresa contraparte que no existe -  "
                  + filtroFacturaManual.getNbDocNumero());
        result.put("existeAlias", "NO");
        return result;
      }

      // Realizamos la llamada a la creación de factura manual
      tmct0FacturasManualesDao.save(filtroFacturaManual);

      // Indicamos que el resultado de la creación en la BBDD ha sido correcto
      result.put("status", "OK");

    } catch (Exception e) {
      LOG.error("Error metodo modificarFacturasManuales -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - modificarFacturaManual");

    return result;
  }

  /**
   * Consulta el detalle de una factura manual
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> detalleFacturaManual(Map<String, Object> paramObjects) throws SIBBACBusinessException,
                                                                                    ParseException {

    LOG.info("INICIO - SERVICIO - detalleFacturasManuales");

    Map<String, Object> result = new HashMap<String, Object>();
    Object objFacturaManualDTO = new Object();

    try {
      // MFG Si el numero empieza por 9 se obtiene de las de bajas y sino de las normales

      String sNbDocumento = String.valueOf(paramObjects.get("nbDocNumero"));

      if (sNbDocumento.startsWith("9")) { // Factura de baja
        try {
          // Se obtiene la entidad por el id de la tabla de bajas
          Tmct0BajasManuales tmct0FacturasManualBaja = tmct0BajasManualesDao.findOne(new BigInteger(
                                                                                                    String.valueOf(paramObjects.get("id"))));

          // Convertimos la entidad Tmct0FacturasManualesDTO
          Tmct0FacturasManualesDTO facturasManualesDTO = tmct0FacturasManualesAssembler.getTmct0FacturasManualesBajaDetalleDTO(tmct0FacturasManualBaja);

          objFacturaManualDTO = facturasManualesDTO;
        } catch (Exception e) {
          LOG.error("Error metodo detalleFacturaManual - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales baja a DTO");
        }

      } else {
        // Se obtiene la entidad por el id de la tabla
        Tmct0FacturasManuales facturaManual = tmct0FacturasManualesDao.findOne(new BigInteger(
                                                                                              String.valueOf(paramObjects.get("id"))));
        try {
          // Convertimos la entidad y el clientesDTO recuperados a Tmct0FacturasManualesDTO
          Tmct0FacturasManualesDTO facturasManualesDTO = tmct0FacturasManualesAssembler.getTmct0FacturasManualesDetalleDTO(facturaManual);

          objFacturaManualDTO = facturasManualesDTO;
        } catch (Exception e) {
          LOG.error("Error metodo detalleFacturaManual - " + e.getMessage(), e);
          throw new SIBBACBusinessException("Error en la transformación de la consulta de facturas manuales a DTO");
        }
      }

    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo detalleFacturaManual -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("detalleFacturaManual", objFacturaManualDTO);

    LOG.info("FIN - SERVICIO - detalleFacturasManuales");

    return result;
  }

  /**
   * Consulta el cliente a raiz de un idAlias
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarCliente(Map<String, Object> paramObjects) throws SIBBACBusinessException,
                                                                                ParseException {

    LOG.info("INICIO - SERVICIO - consultarCliente");

    Map<String, Object> result = new HashMap<String, Object>();
    Object clienteDTO = new Object();

    try {
      // Hacemos la llamada a BBDD para recuperar los datos de cliente a raiz del idAlias de la entidad
      ClienteDTO cliente = tmct0FacturasManualesDaoImpl.consultarClienteByAlias(new BigInteger(
                                                                                               String.valueOf(paramObjects.get("idAlias"))));

      clienteDTO = cliente;
    } catch (Exception e) {
      LOG.error("Error metodo consultarCliente - " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("cliente", clienteDTO);

    LOG.info("FIN - SERVICIO - consultarCliente");

    return result;
  }

  /**
   * Elimina las facturas manuales seleccionadas.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> eliminarFacturasManuales(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.info("INICIO - SERVICIO - eliminarFacturasManuales");
    Map<String, Object> result = new HashMap<String, Object>();

    try {
      List<LinkedHashMap<?,?>> listaFMsBorrar = (List<LinkedHashMap<?,?>>) paramsObject.get("listaFacturasManualesBorrar");
      List<BigInteger> listIdsFacturasManualesBorrar = new ArrayList<BigInteger>();

      if (CollectionUtils.isNotEmpty(listaFMsBorrar)) {
        for (int x = 0; x < listaFMsBorrar.size(); x++) {
          LinkedHashMap<?,?> map = listaFMsBorrar.get(x);

          // Marcado modificable.
          Boolean isModificable = map != null ? (Boolean) map.get("esModificable") : null;
          // Id de factura a borrar.
          Integer id = map.get("id") != null ? (Integer) map.get("id") : null;

          // Solo se pueden borrar las facturas marcadas como modificables.
          if (BooleanUtils.isTrue(isModificable) && id != null) {
            listIdsFacturasManualesBorrar.add(new BigInteger(String.valueOf(id)));
          } else {
            throw new SIBBACBusinessException("Error: El registro a borrar está en un estado no modificable");
          }
        }
        this.tmct0FacturasManualesDao.deleteFromId(listIdsFacturasManualesBorrar);
      } else {
        throw new SIBBACBusinessException("Error: No hay registros a borrar.");
      }
    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo eliminarFacturasManuales -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("status", "OK");
    LOG.info("FIN - SERVICIO - eliminarFacturasManuales");
    return result;
  }

  /**
   * Crea una factura manual a raiz de los datos insertados en pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> crearYContabilizar(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
                                                                                   ParseException {

    LOG.info("INICIO - SERVICIO - crearYContabilizar");

    Map<String, Object> result = new HashMap<String, Object>();

    try {
      // Transformamos los parámetros de la visual en Entidad
      Tmct0FacturasManuales filtroFacturaManual = tmct0FacturasManualesAssembler.getTmct0FacturasManualesEntidad(paramsObjects,
                                                                                                                 false);

      // Comprobamos si el alias existe
      List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroFacturaManual.getIdAlias()));

      // Si no existe el alias se informa al usuario
      if (null == listaAlias || listaAlias.size() == 0) {
        LOG.error("Se esta intentanto crear una factura manual con con una empresa contraparte que no existe -  "
                  + filtroFacturaManual.getNbDocNumero());
        result.put("existeAlias", "NO");
        return result;
      }

      // Comprobamos si el número de factura ya existe
      List<Tmct0FacturasManuales> listaFacturasManuales = tmct0FacturasManualesDao.findBynbDocNumero(filtroFacturaManual.getNbDocNumero());

      // Si ya hay una factura con ese número se informa
      if (listaFacturasManuales.size() > 0) {
        LOG.error("Se esta intentanto crear una factura manual con el mismo número de factura que una ya existente -  "
                  + filtroFacturaManual.getNbDocNumero());
        throw new SIBBACBusinessException(
                                          "Se esta intentanto crear una factura manual con el mismo número de factura que una ya existente "
                                              + filtroFacturaManual.getNbDocNumero());
      }

      // Se asigna a la factura el estado Pte de contabilizar
      Tmct0estado estadoFActCont = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_PDTE_CONTABILIZAR)
                                                                                    .getValor());

      if (estadoFActCont != null) {
        filtroFacturaManual.setEstado(estadoFActCont);
      } else {
        throw new SIBBACBusinessException(
                                          "No se ha podido determinar el estado  PTE de contabilizar.  Consulte con el administrador  ");
      }

      // Realizamos la llamada a la creación de factura manual
      tmct0FacturasManualesDao.save(filtroFacturaManual);// crearFacturaManual(filtroFacturaManual);

      // MFG Se quita esta llamada que daba un casque jpa porque volvia a crear otra entidad nueva.
      // llamamos al método contabilizar
      // contabilizar(paramsObjects, false);

      // Indicamos que el resultado de la creación en la BBDD ha sido correcto
      result.put("status", "OK");

    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo crearYContabilizar -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - crearYContabilizar");

    return result;
  }

  /**
   * Modifica una factura manual a raiz de los datos insertados en pantalla
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> modificarYContabilizar(Map<String, Object> paramsObjects) throws SIBBACBusinessException,
                                                                                       ParseException {

    LOG.info("INICIO - SERVICIO - modificarYContabilizar");

    Map<String, Object> result = new HashMap<String, Object>();

    try {
      // Transformamos los parámetros de la visual en Entidad
      Tmct0FacturasManuales filtroFacturaManual = tmct0FacturasManualesAssembler.getTmct0FacturasManualesEntidad(paramsObjects,
                                                                                                                 true);

      // Comprobamos si el alias existe
      List<String> listaAlias = tmct0AliDao.existeAlias(String.valueOf(filtroFacturaManual.getIdAlias()));

      // Si no existe el alias se informa al usuario
      if (null == listaAlias || listaAlias.size() == 0) {
        LOG.error("Se esta intentanto crear una factura manual con con una empresa contraparte que no existe -  "
                  + filtroFacturaManual.getNbDocNumero());
        result.put("existeAlias", "NO");
        return result;
      }

      // Se asigna a la factura el estado Pte de contabilizar
      Tmct0estado estadoFActCont = estadoDao.findByNombre(interfacesEmitidasConstDao.findByClave(Constantes.ESTADO_PDTE_CONTABILIZAR)
                                                                                    .getValor());

      if (estadoFActCont != null) {
        filtroFacturaManual.setEstado(estadoFActCont);
      } else {
        throw new SIBBACBusinessException(
                                          "No se ha podido determinar el estado  PTE de contabilizar.  Consulte con el administrador  ");
      }

      // Realizamos la llamada a la creación de factura manual
      tmct0FacturasManualesDao.save(filtroFacturaManual);

      // Indicamos que el resultado de la creación en la BBDD ha sido correcto
      result.put("status", "OK");

    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo modificarYContabilizar -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - modificarYContabilizar");

    return result;
  }

  /**
   * Contabilizar
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> contabilizar(Map<String, Object> paramsObjects, boolean esModificacion) throws SIBBACBusinessException,
                                                                                                     ParseException {

    LOG.info("INICIO - SERVICIO - contabilizar");

    Map<String, Object> result = new HashMap<String, Object>();

    try {
      // Transformamos los parámetros de la visual en Entidad
      // MFG se cambia la llamda a volver a crear la entidad por una que recupera ya que viene desde el detalle y no hay
      // modificaciones
      /*
       * Tmct0FacturasManuales filtroFacturaManual =
       * tmct0FacturasManualesAssembler.getTmct0FacturasManualesEntidad(paramsObjects, esModificacion);
       */

      Tmct0FacturasManuales filtroFacturaManual = tmct0FacturasManualesDao.findOne(new BigInteger(
                                                                                                  String.valueOf(paramsObjects.get("id"))));

      // Se asigna a la factura el estado Pte de contabilizar
      Tmct0estado estadoFActCont = estadoDao.findByNombre(Constantes.ESTADO_PDTE_CONTABILIZAR);

      if (estadoFActCont != null) {
        filtroFacturaManual.setEstado(estadoFActCont);
      } else {
        throw new SIBBACBusinessException(
                                          "No se ha podido determinar el estado  PTE de contabilizar.  Consulte con el administrador  ");
      }

      // Realizamos la modificación del estado
      tmct0FacturasManualesDao.save(filtroFacturaManual);

      // Indicamos que el resultado de la creación en la BBDD ha sido correcto
      result.put("status", "OK");

    } catch (SIBBACBusinessException e) {
      throw e;
    } catch (Exception e) {
      LOG.error("Error metodo contabilizar -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - contabilizar");

    return result;
  }

  /**
   * Contabilizar
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> consultarProxNumFact(boolean esGrupoIva) throws SIBBACBusinessException, ParseException {

    LOG.info("INICIO - SERVICIO - consultarProxNumFact");

    Map<String, Object> result = new HashMap<String, Object>();
    BigInteger proxNumFact;

    try {
      // Hacemos la llamada a BBDD para recuperar los datos de cliente a raiz del idAlias de la entidad
      proxNumFact = tmct0FacturasManualesDaoImpl.getNbDocNumeroFacturaManual(esGrupoIva,false);
    } catch (Exception e) {
      LOG.error("Error metodo consultarProxNumFact - " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    result.put("proximoNumeroFacturaManual", proxNumFact);

    LOG.info("FIN - SERVICIO - consultarProxNumFact");

    return result;
  }
  
  /**
   * Exporta a pdf la factura seleccionada
   * 
   * @return
   * @throws SIBBACBusinessException
   * @throws ParseException
   */
  private Map<String, Object> exportarPdf(Map<String, Object> paramObjects) throws SIBBACBusinessException,
                                                                                    ParseException {

    LOG.info("INICIO - SERVICIO - exportarPdf");

    Map<String, Object> result = new HashMap<String, Object>();
    Object[] objPdf = new Object[1];
    boolean esBaja = false;

    try {

      BigInteger sNbDocumento = new BigInteger(String.valueOf(paramObjects.get("nbDocNumero")));
      if (String.valueOf(paramObjects.get("nbDocNumero")).startsWith("9")) { // Factura de baja
    	  objPdf = tmct0FacturasManualesDaoImpl.consultarDatosPdf(sNbDocumento,true);
    	  esBaja = true;
      } else{
    	  objPdf = tmct0FacturasManualesDaoImpl.consultarDatosPdf(sNbDocumento,false);
      }
        
        Map<String, String> mapConstantes = tmct0FacturasManualesDaoImpl.consultarConstantes(Constantes.GRUPO_GENERADORPDF);

        if (null!=objPdf && objPdf.length>0 && null!=objPdf[0] && null!=mapConstantes && mapConstantes.size()>0) {
        	if (String.valueOf(paramObjects.get("nbDocNumero")).startsWith("6")) {
        		result = generadorPdf.generarPdfFacturasManualesGrupoIVA(objPdf,mapConstantes);
        	} else {
        		result = generadorPdf.generarPdfFacturas(objPdf,mapConstantes,false,esBaja);
        	}
        	
        } else {
            LOG.error("Error metodo exportarPdf -  Alguno de los datos del cliente necesarios para la generación del PDF no está en BBDD");
            result.put("status", "KO");
            return result;
        }

    } catch (Exception e) {
      LOG.error("Error metodo exportarPdf -  " + e.getMessage(), e);
      result.put("status", "KO");
      return result;
    }

    LOG.info("FIN - SERVICIO - exportarPdf");

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandoFacturasManuales getCommand(String command) {
    ComandoFacturasManuales[] commands = ComandoFacturasManuales.values();
    ComandoFacturasManuales result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandoFacturasManuales.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    return null;
  }

  @Override
  public List<String> getFields() {
    return null;
  }

}
