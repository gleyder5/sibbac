package sibbac.business.interfacesemitidas.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.interfacesemitidas.database.model.Tmct0BajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0FacturasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaBajasManuales;
import sibbac.business.interfacesemitidas.database.model.Tmct0RectificativaManual;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaRectificativa;
import sibbac.common.utils.FormatDataUtils;

public class FacturaDTO {

	private boolean baja = false;
	private boolean regular = false;
	private boolean regularBaja = false;
	private boolean manual = false;
	private boolean rectificadaManual = false;
	private boolean rectificadaBaja= false;
	private boolean manualBaja= false;

	private Long idFactura;
	private Integer idEstado;
	private String fechaCreacion;
	private Long idAlias;
	private String comentarios;
	private BigDecimal baseImponible;
	private BigDecimal importeTotal;
	private long numeroFactura;
	private Date fecha;
	private BigDecimal porcentaje;
	private String nombreAlias;

	private BigDecimal impBaseImponible;
	private BigDecimal impImpuesto;
	private BigInteger idCodImpuesto;
	private BigDecimal coefImpCostes;
	private BigDecimal baseImponCostes;
	private BigDecimal importeFactura;
	private BigDecimal cuotaRepercut;

	private Integer claveRegimen;
	private String tipoFactura;
	private String exenta = "";
	private String sujetaNoExenta;
	private String idFacturaARectificar;
	private String idsFacturasARectificar;
	
	private BigInteger numeroFacturaOriginal;

	public FacturaDTO(final Factura factura) {

		this.regular = true;
		this.idEstado = factura.getEstado().getIdestado();
		this.idFactura = factura.getId();
		this.comentarios = factura.getComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFechaCreacion());
		this.baseImponible = factura.getImfinsvb();
		this.idAlias = factura.getIdAlias();
		this.numeroFactura = factura.getNumeroFactura();
		this.fecha = factura.getFechaCreacion();
		this.porcentaje = new BigDecimal(factura.getPorcentaje());
		this.importeTotal = factura.getImfinsvb();
		this.nombreAlias = factura.getNombreAlias();
		this.tipoFactura = "F1";
		this.exenta = "E";
	}

	public FacturaDTO(Tmct0FacturasManuales factura) {

		this.manual = true;
		this.idEstado = factura.getEstado().getIdestado();
		this.idFactura = factura.getId().longValue();
		this.comentarios = factura.getNbComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFhFechaCre());
		this.baseImponible = factura.getImFinSvb();
		this.idAlias = factura.getIdAlias().longValue();
		this.numeroFactura = factura.getNbDocNumero().longValue();
		this.fecha = factura.getFhFechaCre();
		this.porcentaje = factura.getPorcentajeImpuesto();
		this.importeTotal = factura.getImFinSvb();

		this.impBaseImponible = factura.getImpBaseImponible();
		this.impImpuesto = factura.getImpImpuesto();
		this.coefImpCostes = factura.getCoefImpCostes();
		this.baseImponCostes = factura.getBaseImponCostes();
		this.idCodImpuesto = new BigInteger(String.valueOf(factura.getCodImpuesto().getId()));
		if (null!=factura.getTmct0Sujetas()) {
		this.sujetaNoExenta = factura.getTmct0Sujetas().getClave();
		}
		this.claveRegimen = factura.getClaveRegimen().getId();
		this.tipoFactura = factura.getTipoFactura().getClave();
		this.exenta = factura.getTmct0Exentas().getClave();
		this.importeFactura = factura.getImFinSvb();

	}

	public FacturaDTO(Tmct0RectificativaManual factura) {

		this.rectificadaManual = true;
		this.idEstado = factura.getEstado().getIdestado();
		this.idFactura = factura.getId().longValue();
		this.comentarios = factura.getNbComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFhFechaCre());
		this.baseImponible = factura.getImpBaseImponible();
		this.idAlias = factura.getIdAlias().longValue();
		this.numeroFactura = factura.getNbDocNumero().longValue();
		this.fecha = factura.getFhFechaCre();
		this.porcentaje = factura.getPorcentajeImpuesto();
		this.importeTotal = factura.getImFinSvb();
		this.impBaseImponible = factura.getImpBaseImponible();
		this.impImpuesto = factura.getImpImpuesto();
		this.idCodImpuesto = new BigInteger(String.valueOf(factura.getCodImpuesto().getId()));
		this.tipoFactura = factura.getTipoFactura().getClave();
		this.exenta = factura.getTmct0Exentas().getClave();
		if (null!=factura.getTmct0Sujetas()) {
		this.sujetaNoExenta = factura.getTmct0Sujetas().getClave();
		}
		this.coefImpCostes = factura.getCoefImpCostes();
		this.baseImponCostes = factura.getBaseImponCostes();
		this.importeFactura = factura.getImFinSvb();
		String facturasARectificar = "";
		if (null != factura.getNbDocNumero0()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero0();
		}
		if (null != factura.getNbDocNumero1()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero1();
		}
		if (null != factura.getNbDocNumero2()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero2();
		}
		if (null != factura.getNbDocNumero3()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero3();
		}
		if (null != factura.getNbDocNumero4()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero4();
		}
		if (null != factura.getNbDocNumero5()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero5();
		}
		if (null != factura.getNbDocNumero6()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero6();
		}
		if (null != factura.getNbDocNumero7()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero7();
		}
		if (null != factura.getNbDocNumero8()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero8();
		}
		if (null != factura.getNbDocNumero9()) {
			facturasARectificar = facturasARectificar+factura.getNbDocNumero9();
		}
		this.idsFacturasARectificar = facturasARectificar;
		this.cuotaRepercut = factura.getCuotaRepercut();
		this.claveRegimen = factura.getClaveRegimen().getId();
	}

	/**
	 * @return the idsFacturasARectificar
	 */
	public String getIdsFacturasARectificar() {
		return idsFacturasARectificar;
	}

	/**
	 * @param idsFacturasARectificar the idsFacturasARectificar to set
	 */
	public void setIdsFacturasARectificar(String idsFacturasARectificar) {
		this.idsFacturasARectificar = idsFacturasARectificar;
	}

	public FacturaDTO(FacturaRectificativa factura) {

		this.rectificadaManual = true;
		this.regular = true;
		this.idEstado = factura.getEstado().getIdestado();
		this.idFactura = factura.getId().longValue();
		this.comentarios = factura.getComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFechaCreacion());
		this.idAlias = factura.getAlias().getId();
		this.numeroFactura = factura.getNumeroFactura();
		this.fecha = factura.getFechaCreacion();
		this.porcentaje = new BigDecimal(0);
		this.importeTotal = factura.getImfinsvb();
		this.impBaseImponible = factura.getFactura().getFacturaSugerida().getImfinsvb();
		this.impImpuesto = factura.getImporteImpuestos();
		this.idCodImpuesto = new BigInteger("1");
		this.tipoFactura = "R1";
		this.exenta = "E";
		this.idFacturaARectificar = factura.getFactura().getNumeroFactura().toString();
		this.nombreAlias = factura.getFactura().getNombreAlias();
		this.baseImponible = factura.getFactura().getImfinsvb();
	}
	
	public FacturaDTO(Tmct0RectificativaBajasManuales factura) {

		this.rectificadaManual = true;
		this.regularBaja = true;
		this.rectificadaBaja= true;
		this.idEstado = factura.getEstado().getIdestado();
		this.idFactura = factura.getId().longValue();
		this.comentarios = factura.getNbComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFhFechaCre());
		this.baseImponible = factura.getImpBaseImponible();
		this.idAlias = factura.getIdAlias().longValue();
		this.numeroFactura = factura.getNbDocNumero().longValue();
		this.fecha = factura.getFhFechaCre();
		this.porcentaje = factura.getPorcentajeImpuesto();
		this.importeTotal = factura.getImFinSvb();
		this.impBaseImponible = factura.getImpBaseImponible();
		this.impImpuesto = factura.getImpImpuesto();
		this.idCodImpuesto = new BigInteger("1");
		this.tipoFactura = "R1";
		this.exenta = "E";
		this.coefImpCostes = factura.getCoefImpCostes();
		this.baseImponCostes = factura.getBaseImponCostes();
		this.numeroFacturaOriginal = factura.getNbDocNumeroOriginal();
	}

	public FacturaDTO(Tmct0BajasManuales factura) {
		
		//Las que sean EXENTAS se tratarán exactamente igual que las AUTOMÁTICAS RECTIFICATIVAS y las que sean NO EXENTAS se tratarán igual que las MANUALES RECTIFICATIVAS
		if (factura.getTmct0Exentas().getClave().equals("E")) {
			this.rectificadaManual = true;
			this.regular = true;
			this.regularBaja = true;
			this.manualBaja= true;
			this.idEstado = factura.getEstado().getIdestado();
			this.idFactura = factura.getId().longValue();
			this.comentarios = factura.getNbComentarios();
			this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFhFechaCre());
			this.idAlias = factura.getIdAlias().longValue();
			this.numeroFactura = factura.getNbDocNumero().longValue();
			this.fecha = factura.getFhFechaCre();
			this.porcentaje = new BigDecimal(0);
			this.importeTotal = factura.getImFinSvb().negate();
			this.impBaseImponible = factura.getImFinSvb().negate();
			this.impImpuesto = factura.getImporteImpuestos().negate();
			this.idCodImpuesto = new BigInteger("1");
			this.tipoFactura = "R1";
			this.exenta = "E";
			this.baseImponible = factura.getImFinSvb().negate();
			String facturasARectificar = "";
			this.idsFacturasARectificar = facturasARectificar+factura.getNbDocNumero().toString().substring(1, 4)+"0"+factura.getNbDocNumero().toString().substring(4, 8);
			this.numeroFacturaOriginal = factura.getNbDocNumeroOriginal();
		} else if (factura.getTmct0Exentas().getClave().equals("NE")){
			this.rectificadaManual = true;
			this.regularBaja = true;
			this.manualBaja= true;
			this.idEstado = factura.getEstado().getIdestado();
			this.idFactura = factura.getId().longValue();
			this.comentarios = factura.getNbComentarios();
			this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFhFechaCre());
			this.baseImponible = factura.getImFinSvb().negate();
			this.idAlias = factura.getIdAlias().longValue();
			this.numeroFactura = factura.getNbDocNumero().longValue();
			this.fecha = factura.getFhFechaCre();
			this.porcentaje = factura.getPorcentajeImpuesto();
			this.importeTotal = factura.getImFinSvb().negate();;
			this.impBaseImponible = factura.getImpBaseImponible().negate();;
			this.impImpuesto = factura.getImpImpuesto().negate();
			this.idCodImpuesto = new BigInteger(String.valueOf(factura.getCodImpuesto().getId()));
			this.tipoFactura = "R1";
			this.exenta = factura.getTmct0Exentas().getClave();
			if (null!=factura.getTmct0Sujetas()) {
			this.sujetaNoExenta = factura.getTmct0Sujetas().getClave();
			}
			this.coefImpCostes = factura.getCoefImpCostes();
			this.baseImponCostes = factura.getBaseImponCostes();
			this.importeFactura = factura.getImFinSvb().negate();
			String facturasARectificar = "";
			this.idsFacturasARectificar = facturasARectificar+factura.getNbDocNumero().toString().substring(1, 4)+"0"+factura.getNbDocNumero().toString().substring(4, 8);
			this.cuotaRepercut = factura.getCuotaRepercut();
			this.claveRegimen = factura.getClaveRegimen().getId();
			this.numeroFacturaOriginal = factura.getNbDocNumeroOriginal();
		}

//		this.manual = true;
//		this.baja = true;
//		this.idEstado = factura.getEstado().getIdestado();
//		this.idFactura = factura.getId().longValue();
//		this.comentarios = factura.getNbComentarios();
//		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFhFechaCre());
//		this.baseImponible = factura.getImpBaseImponible();
//		this.idAlias = null;
//		factura.getIdAlias();
//		this.numeroFactura = factura.getNbDocNumero().longValue();
//		this.fecha = factura.getFhFechaCre();
//		this.porcentaje = factura.getPorcentajeImpuesto();
//		this.importeTotal = factura.getImporteTotal();
//		this.impBaseImponible = factura.getImpBaseImponible();
//		this.impImpuesto = factura.getImpImpuesto();
//		this.idCodImpuesto = new BigInteger(String.valueOf(factura.getCodImpuesto().getId()));
//		this.tipoFactura = factura.getTipoFactura().getClave();
//		this.exenta = factura.getTmct0Exentas().getClave();
	}
	
	/**
	 * @return the rectificadaBaja
	 */
	public boolean isRectificadaBaja() {
		return rectificadaBaja;
	}

	/**
	 * @param rectificadaBaja the rectificadaBaja to set
	 */
	public void setRectificadaBaja(boolean rectificadaBaja) {
		this.rectificadaBaja = rectificadaBaja;
	}

	/**
	 * @return the manualBaja
	 */
	public boolean isManualBaja() {
		return manualBaja;
	}

	/**
	 * @param manualBaja the manualBaja to set
	 */
	public void setManualBaja(boolean manualBaja) {
		this.manualBaja = manualBaja;
	}

	/**
	 * @return the causaExencion
	 */
	public String getCausaExencion() {
		return exenta;
	}

	/**
	 * @param causaExencion the causaExencion to set
	 */
	public void setCausaExencion(String exenta) {
		this.exenta = exenta;
	}

	
	/**
	 * @return the tipoFactura
	 */
	public String getTipoFactura() {
		return tipoFactura;
	}

	/**
	 * @param tipoFactura the tipoFactura to set
	 */
	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}

	public boolean isBaja() {
		return baja;
	}

	public void setBaja(boolean baja) {
		this.baja = baja;
	}

	public BigInteger getId() {
		return new BigInteger(idFactura.toString());
	}

	public BigInteger getIdCodImpuesto() {
		return idCodImpuesto;
	}

	public void setIdCodImpuesto(BigInteger idCodImpuesto) {
		this.idCodImpuesto = idCodImpuesto;
	}

	public String getNombreAlias() {
		return nombreAlias;
	}

	public void setNombreAlias(String nombreAlias) {
		this.nombreAlias = nombreAlias;
	}

	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	/**
	 * @return the idFactura
	 */
	public Long getIdFactura() {
		return idFactura;
	}

	/**
	 * @param idFacturaSugerida
	 *            the idFacturaSugerida to set
	 */
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios
	 *            the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios.replaceAll("\t"," ");
		this.comentarios = this.comentarios.replaceAll("\n"," ");
	}

	/**
	 * @return the baseImponible
	 */
	public BigDecimal getBaseImponible() {
		return baseImponible;
	}

	/**
	 * @param baseImponible
	 *            the baseImponible to set
	 */
	public void setBaseImponible(BigDecimal baseImponible) {
		this.baseImponible = baseImponible;
	}

	public long getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(long numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public boolean isAceptadaConErrores() {
		return EstadosEnumerados.FACTURA.ACEPTADA_CON_ERRORES.getId().equals(idEstado);
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}

	public boolean isRectificadaManual() {
		return rectificadaManual;
	}

	public void setRectificadaManual(boolean rectificadaManual) {
		this.rectificadaManual = rectificadaManual;
	}

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public BigDecimal getImpBaseImponible() {
		return impBaseImponible;
	}

	public void setImpBaseImponible(BigDecimal impBaseImponible) {
		this.impBaseImponible = impBaseImponible;
	}

	public BigDecimal getImpImpuesto() {
		return impImpuesto;
	}

	public void setImpImpuesto(BigDecimal impImpuesto) {
		this.impImpuesto = impImpuesto;
	}

	public Long getIdAlias() {
		return idAlias;
	}

	public void setIdAlias(Long idAlias) {
		this.idAlias = idAlias;
	}

	public BigDecimal getCoefImpCostes() {
		return coefImpCostes;
	}

	public void setCoefImpCostes(BigDecimal coefImpCostes) {
		this.coefImpCostes = coefImpCostes;
	}

	public BigDecimal getBaseImponCostes() {
		return baseImponCostes;
	}

	public void setBaseImponCostes(BigDecimal baseImponCostes) {
		this.baseImponCostes = baseImponCostes;
	}

	public boolean isRegular() {
		return regular;
	}

	public void setRegular(boolean regular) {
		this.regular = regular;
	}

	public Integer getClaveRegimen() {
		return claveRegimen;
	}

	public void setClaveRegimen(Integer claveRegimen) {
		this.claveRegimen = claveRegimen;
	}
	
	/**
	 * @return the exenta
	 */
	public String getExenta() {
		return exenta;
	}

	/**
	 * @param exenta the exenta to set
	 */
	public void setExenta(String exenta) {
		this.exenta = exenta;
	}

	/**
	 * @return the sujetaNoExenta
	 */
	public String getSujetaNoExenta() {
		return sujetaNoExenta;
	}

	/**
	 * @param sujetaNoExenta the sujetaNoExenta to set
	 */
	public void setSujetaNoExenta(String sujetaNoExenta) {
		this.sujetaNoExenta = sujetaNoExenta;
	}
	

	/**
	 * @return the idFacturaARectificar
	 */
	public String getIdFacturaARectificar() {
		return idFacturaARectificar;
	}

	/**
	 * @param idFacturaARectificar the idFacturaARectificar to set
	 */
	public void setIdFacturaARectificar(String idFacturaARectificar) {
		this.idFacturaARectificar = idFacturaARectificar;
	}
	
	/**
	 * @return the cuotaRepercut
	 */
	public BigDecimal getCuotaRepercut() {
		return cuotaRepercut;
	}

	/**
	 * @param cuotaRepercut the cuotaRepercut to set
	 */
	public void setCuotaRepercut(BigDecimal cuotaRepercut) {
		this.cuotaRepercut = cuotaRepercut;
	}

	/**
	 * @return the importeFactura
	 */
	public BigDecimal getImporteFactura() {
		return importeFactura;
	}

	/**
	 * @param importeFactura the importeFactura to set
	 */
	public void setImporteFactura(BigDecimal importeFactura) {
		this.importeFactura = importeFactura;
	}
	
	/**
	 * @return the regularBaja
	 */
	public boolean isRegularBaja() {
		return regularBaja;
	}

	/**
	 * @param regularBaja the regularBaja to set
	 */
	public void setRegularBaja(boolean regularBaja) {
		this.regularBaja = regularBaja;
	}
	
	/**
	 * @return the numeroFacturaOriginal
	 */
	public BigInteger getNumeroFacturaOriginal() {
		return numeroFacturaOriginal;
	}

	/**
	 * @param numeroFacturaOriginal the numeroFacturaOriginal to set
	 */
	public void setNumeroFacturaOriginal(BigInteger numeroFacturaOriginal) {
		this.numeroFacturaOriginal = numeroFacturaOriginal;
	}
}
