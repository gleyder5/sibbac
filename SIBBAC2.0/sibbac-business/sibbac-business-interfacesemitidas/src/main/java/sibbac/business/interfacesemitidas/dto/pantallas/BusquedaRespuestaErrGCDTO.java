package sibbac.business.interfacesemitidas.dto.pantallas;

import java.math.BigInteger;
import java.util.Date;

/**
 *	DTO que representa busqueda de respuesta errores GC.
 */
public class BusquedaRespuestaErrGCDTO {

	private Long id;
	private String nombreFichero;
	private String tipoFichero;
	private Date fechaEnvioRecepcion;
	private String estado;
	private String asociacion;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public BusquedaRespuestaErrGCDTO() {
		super();
	}
	
	/**
	 *	Constructor con argumentos.
	 *	@param id
	 *	@param nombreFichero
	 *	@param tipoFichero	 
	 *	@param fechaEnvioRecepcion	 
	 *	@param estado	 
	 *	@param asociacion	 
	 */
	public BusquedaRespuestaErrGCDTO(Long id, String nombreFichero, String tipoFichero,
			Date fechaEnvioRecepcion, String estado, String asociacion) {
		super();
		this.id = id;
		this.nombreFichero = nombreFichero;
		this.tipoFichero = tipoFichero;
		this.fechaEnvioRecepcion = fechaEnvioRecepcion;
		this.estado = estado;
		this.asociacion = asociacion;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreFichero() {
		return this.nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public String getTipoFichero() {
		return this.tipoFichero;
	}

	public void setTipoFichero(String tipoFichero) {
		this.tipoFichero = tipoFichero;
	}
	
	public Date getFechaEnvioRecepcion() {
		return this.fechaEnvioRecepcion;
	}

	public void setFechaEnvioRecepcion(Date fechaEnvioRecepcion) {
		this.fechaEnvioRecepcion = fechaEnvioRecepcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getAsociacion() {
		return this.asociacion;
	}

	public void setAsociacion(String asociacion) {
		this.asociacion = asociacion;
	}
	
	/**
	 *	Convierte a DTO un array de Object con propiedades.
	 *	@param obj
	 *	@return BusquedaRespuestaErrGCDTO  
	 */
	public static BusquedaRespuestaErrGCDTO convertObjectToClienteDTO(Object[] obj) {
		BusquedaRespuestaErrGCDTO busqRespErrDTO = new BusquedaRespuestaErrGCDTO();
		busqRespErrDTO.setId(((BigInteger) obj[0]).longValue());
		busqRespErrDTO.setNombreFichero(((String) obj[1]).trim());
		busqRespErrDTO.setTipoFichero(((String) obj[2]).trim());
		busqRespErrDTO.setFechaEnvioRecepcion(((Date) obj[3]));

		// Campos no obligatorios.
		if (obj[4] != null) {
			busqRespErrDTO.setEstado(((String) obj[4]).trim());
		}
		if (obj[5] != null) {
			busqRespErrDTO.setAsociacion(((String) obj[5]).trim());
		}
		return busqRespErrDTO;
	}
	
}
