package sibbac.business.interfacesemitidas.dto.pantallas;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 *	DTO que representa busqueda de asientos contables.
 */
public class BusquedaAsientosContablesDTO {

	private String prefijoId;
	private String estado;
	private BigDecimal totalDebeHaber;
	private String tipoApunte;
	private String nroFactura;
	private String descripcionFactura;
	private String empresaContraparte;
	private BigDecimal importeImpuestos;
	private Date fechaFactura;
	
	/**
	 *	Constructor sin argumentos. 
	 */
	public BusquedaAsientosContablesDTO() {
		super();
	}
	
	/**
	 *	Constructor con argumentos.
	 *	@param	id
	 *	@param 	estado
	 *	@param 	totalDebeHaber
	 *	@param 	tipoApunte
	 *	@param 	nroFactura
	 *	@param 	descripcionFactura
	 *	@param 	empresaContraparte
	 *	@param 	importeImpuestos
	 *	@param 	fechaFactura
	 */
	public BusquedaAsientosContablesDTO(
			String prefijoId, String estado, BigDecimal totalDebeHaber,
			String tipoApunte, String nroFactura, String descripcionFactura,
			String empresaContraparte, BigDecimal importeImpuestos,
			Date fechaFactura) {
		super();
		this.prefijoId = prefijoId;
		this.estado = estado;
		this.totalDebeHaber = totalDebeHaber;
		this.tipoApunte = tipoApunte;
		this.nroFactura = nroFactura;
		this.descripcionFactura = descripcionFactura;
		this.empresaContraparte = empresaContraparte;
		this.importeImpuestos = importeImpuestos;
		this.fechaFactura = fechaFactura;
	}
	
	public String getPrefijoId() {
		return this.prefijoId;
	}

	public void setPrefijoId(String prefijoId) {
		this.prefijoId = prefijoId;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigDecimal getTotalDebeHaber() {
		return this.totalDebeHaber;
	}

	public void setTotalDebeHaber(BigDecimal totalDebeHaber) {
		this.totalDebeHaber = totalDebeHaber;
	}

	public String getTipoApunte() {
		return this.tipoApunte;
	}

	public void setTipoApunte(String tipoApunte) {
		this.tipoApunte = tipoApunte;
	}

	public String getNroFactura() {
		return this.nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getDescripcionFactura() {
		return this.descripcionFactura;
	}

	public void setDescripcionFactura(String descripcionFactura) {
		this.descripcionFactura = descripcionFactura;
	}

	public String getEmpresaContraparte() {
		return this.empresaContraparte;
	}

	public void setEmpresaContraparte(String empresaContraparte) {
		this.empresaContraparte = empresaContraparte;
	}

	public BigDecimal getImporteImpuestos() {
		return this.importeImpuestos;
	}

	public void setImporteImpuestos(BigDecimal importeImpuestos) {
		this.importeImpuestos = importeImpuestos;
	}

	public Date getFechaFactura() {
		return this.fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	
	/**
	 *	Convierte a DTO un array de Object con propiedades.
	 *	@param obj
	 *	@param prefijo - R: Rectificadas - M: Manuales
	 *	@param descrAli: descripcion del alias
	 *	@return BusquedaAsientosContablesDTO  
	 */
	public static BusquedaAsientosContablesDTO convertObjectToBusqAsientoContableDTO(
		Object[] obj, String prefijo, String descrAli) {
		BusquedaAsientosContablesDTO busqAsientosContablesDTO = new BusquedaAsientosContablesDTO();
		busqAsientosContablesDTO.setPrefijoId(prefijo + ((Integer) obj[0]));
		
		if (obj[1] != null) {
			busqAsientosContablesDTO.setEstado(((String) obj[1]));
		}
		if (obj[2] != null) {
			busqAsientosContablesDTO.setTotalDebeHaber(((BigDecimal) obj[2]));
		}
		if (obj[3] != null) {
			busqAsientosContablesDTO.setTipoApunte(String.valueOf((Character) obj[3]));
		}
		if (obj[4] != null) {
			busqAsientosContablesDTO.setNroFactura(String.valueOf(((BigInteger) obj[4])));
		}
		if (obj[5] != null) {
			busqAsientosContablesDTO.setDescripcionFactura(((String) obj[5]));			
		}
		if (obj[6] != null && StringUtils.isNotBlank(descrAli)) {
			busqAsientosContablesDTO.setEmpresaContraparte(descrAli);
		} else {
			busqAsientosContablesDTO.setEmpresaContraparte("");
		}
		if (obj[7] != null) {
			busqAsientosContablesDTO.setImporteImpuestos(((BigDecimal) obj[7]));			
		}
		if (obj[8] != null) {
			busqAsientosContablesDTO.setFechaFactura(((Date) obj[8]));			
		}
		return busqAsientosContablesDTO;
	}
	
}
