package sibbac.business.comunicaciones.pti.task;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import sibbac.business.comunicaciones.pti.jms.JmsMessagesPtiBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_DDBB_TO_PTI, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 30/30 19-21 ? * *")
public class TaskDdbbToPti extends WrapperTaskConcurrencyPrevent {

  @Autowired
  @Qualifier(value = "JmsMessagesPtiBo")
  private JmsMessagesPtiBo jmsMessagesBo;

  public TaskDdbbToPti() {
  }

  @Override
  public void executeTask() throws Exception {

  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return null;
  }

  private Runnable getNewRunnableToSendPti(final ConnectionFactory cf, final String destination, final boolean isBatch,
      final List<Long> listIds) {
    Runnable r = new Runnable() {

      @Override
      public void run() {

        // leer msgs con multihilo
        List<PTIMessage> ptiMessages = new ArrayList<PTIMessage>();
        try {
          try (Connection c = jmsMessagesBo.getNewConnection(cf);) {
            c.start();
            try (Session session = jmsMessagesBo.getNewSession(c, true);) {
              try {
                if (isBatch) {
                  jmsMessagesBo.sendPtiMessageBatch(session, destination, ptiMessages);
                }
                else {
                  try (MessageProducer producer = jmsMessagesBo.getNewProducer(session, destination);) {
                    for (PTIMessage ptiMessage : ptiMessages) {
                      jmsMessagesBo.sendPtiMessageOnline(session, producer, ptiMessage);
                    }
                  }
                }
                session.commit();
              }
              catch (PTIMessageException e) {
                if (session != null) {
                  session.rollback();
                }
              }
              catch (JMSException e) {
                LOG.debug(e.getMessage(), e);
              }
            }
          }
        }
        catch (JMSException e) {
          LOG.debug(e.getMessage(), e);
        }
      }
    };
    return r;
  }
}
