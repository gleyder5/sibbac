package sibbac.business.desglose.bo;

import java.util.concurrent.Callable;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.common.SIBBACBusinessException;

class DesgloseAutomaticoTmct0aloCallable implements Callable<Void> {

  private DesgloseTransactionalBo desgloseAutomatico;
  private DesgloseConfigDTO config;
  private Tmct0aloId alloId;

  DesgloseAutomaticoTmct0aloCallable(DesgloseTransactionalBo desgloseAutomatico, DesgloseConfigDTO config,
      Tmct0aloId alloId) {
    this.config = config;
    this.desgloseAutomatico = desgloseAutomatico;
    this.alloId = alloId;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    desgloseAutomatico.procesoDesgloseNacionalAutomatico(alloId, config);
    return null;
  }

  Tmct0aloId getAlloIdId() {
    return alloId;
  }

  void setAlloIdId(Tmct0aloId alloId) {
    this.alloId = alloId;
  }

}
