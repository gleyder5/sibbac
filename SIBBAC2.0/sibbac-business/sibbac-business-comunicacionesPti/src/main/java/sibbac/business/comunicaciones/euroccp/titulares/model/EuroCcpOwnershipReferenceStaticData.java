package sibbac.business.comunicaciones.euroccp.titulares.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.database.DBConstants.EURO_CCP;

@Entity
@Table(name = EURO_CCP.OWNERSHIP_REFERENCE_STATIC_DATA)
public class EuroCcpOwnershipReferenceStaticData extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -5897975641237870508L;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_FILE_SENT", nullable = false)
  private EuroCcpFileSent fileSent;
  @Column(name = "ESTADO_PROCESADO")
  private Integer estadoProcesado;
  @Temporal(TemporalType.DATE)
  @Column(name = "FIRST_SENT_DATE")
  private Date firstSentDate;
  @Column(name = "OWNER_REFERENCE", length = 20)
  private String ownerReference = "";
  @Column(name = "OWNER_NAME", length = 140)
  private String ownerName = "";
  @Column(name = "OWNER_SURNAME", length = 40)
  private String firstSurname = "";
  @Column(name = "SECOND_NAME", length = 40)
  private String secondName = "";
  @Column(name = "IDENTIFICATION_TYPE", length = 1)
  private Character typeIdentificacion;
  @Column(name = "IDENTIFICATION_CODE", length = 11)
  private String identificationCode = "";
  @Column(name = "NATURAL_LEGAL_IND", length = 1)
  private Character naturalLegalInd;
  @Column(name = "NATIONALITY", length = 3)
  private String nationality = "";
  @Column(name = "NATIONAL_FOREIGN_IND", length = 1)
  private Character nationalForeignInd;
  @Column(name = "OWNER_TYPE", length = 1)
  private Character ownerType;
  @Column(name = "ADDRESS", length = 40)
  private String address = "";
  @Column(name = "CITY", length = 20)
  private String city = "";
  @Column(name = "POSTAL_CODE", length = 5)
  private String postalCode = "";
  @Column(name = "COUNTRY_RESIDENCE", length = 3)
  private String countryResidence = "";
  @Column(name = "BIRTH_DATE")
  private Date birthDate;

  public EuroCcpOwnershipReferenceStaticData() {
  }

  public EuroCcpFileSent getFileSent() {
    return fileSent;
  }

  public Integer getEstadoProcesado() {
    return estadoProcesado;
  }

  public void setEstadoProcesado(Integer estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public String getOwnerReference() {
    return ownerReference;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public String getFirstSurname() {
    return firstSurname;
  }

  public String getSecondName() {
    return secondName;
  }

  public Character getTypeIdentificacion() {
    return typeIdentificacion;
  }

  public String getIdentificationCode() {
    return identificationCode;
  }

  public Character getNaturalLegalInd() {
    return naturalLegalInd;
  }

  public String getNationality() {
    return nationality;
  }

  public Character getNationalForeignInd() {
    return nationalForeignInd;
  }

  public Character getOwnerType() {
    return ownerType;
  }

  public String getAddress() {
    return address;
  }

  public String getCity() {
    return city;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public String getCountryResidence() {
    return countryResidence;
  }

  public void setFileSent(EuroCcpFileSent fileSent) {
    this.fileSent = fileSent;
  }

  public void setOwnerReference(String ownerReference) {
    this.ownerReference = ownerReference;
  }

  public Date getFirstSentDate() {
    return firstSentDate;
  }

  public void setFirstSentDate(Date firstSentDate) {
    this.firstSentDate = firstSentDate;
  }

  public void setOwnerName(String ownerName) {
    this.ownerName = ownerName;
  }

  public void setFirstSurname(String firstSurname) {
    this.firstSurname = firstSurname;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public void setTypeIdentificacion(Character typeIdentificacion) {
    this.typeIdentificacion = typeIdentificacion;
  }

  public void setIdentificationCode(String identificationCode) {
    this.identificationCode = identificationCode;
  }

  public void setNaturalLegalInd(Character naturalLegalInd) {
    this.naturalLegalInd = naturalLegalInd;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public void setNationalForeignInd(Character nationalForeignInd) {
    this.nationalForeignInd = nationalForeignInd;
  }

  public void setOwnerType(Character ownerType) {
    this.ownerType = ownerType;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public void setCountryResidence(String countryResidence) {
    this.countryResidence = countryResidence;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  @Override
  public String toString() {
    return String
        .format(
            "EuroCcpOwnershipReferenceStaticData [fileSent=%s, estadoProcesado=%s, firstSentDate=%s, ownerReference=%s, ownerName=%s, firstSurname=%s, secondName=%s, typeIdentificacion=%s, identificationCode=%s, naturalLegalInd=%s, nationality=%s, nationalForeignInd=%s, ownerType=%s, address=%s, city=%s, postalCode=%s, countryResidence=%s, birthDate=%s]",
            fileSent, estadoProcesado, firstSentDate, ownerReference, ownerName, firstSurname, secondName,
            typeIdentificacion, identificationCode, naturalLegalInd, nationality, nationalForeignInd, ownerType,
            address, city, postalCode, countryResidence, birthDate);
  }

}
