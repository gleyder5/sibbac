package sibbac.business.desglose.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

public class DesgloseManualGrupoEjecucionDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5500795621573127776L;

  private final BigDecimal precio;
  private BigDecimal titulos;
  private final List<DesgloseManualEjecucionDTO> listEjecucionesGrupoEjecucion;

  public DesgloseManualGrupoEjecucionDTO(BigDecimal precio) {
    this.precio = precio;
    this.titulos = BigDecimal.ZERO;
    this.listEjecucionesGrupoEjecucion = new ArrayList<DesgloseManualEjecucionDTO>();
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public boolean isEmpty() {
    return this.titulos.compareTo(BigDecimal.ZERO) <= 0;
  }

  public synchronized void add(DesgloseManualEjecucionDTO eje) {
    listEjecucionesGrupoEjecucion.add(eje);
    this.titulos = this.titulos.add(eje.getImtitulos());
  }

  public synchronized void addAll(List<DesgloseManualEjecucionDTO> listEjes) {
    for (DesgloseManualEjecucionDTO desgloseManualEjecucionDTO : listEjes) {
      this.add(desgloseManualEjecucionDTO);
    }
  }

  public synchronized List<DesgloseManualEjecucionDTO> remove(BigDecimal titulosDevolver) {

    List<DesgloseManualEjecucionDTO> toReturn = new ArrayList<DesgloseManualEjecucionDTO>();
    if (!isEmpty()) {
      if (titulosDevolver.compareTo(this.titulos) >= 0) {
        this.titulos = BigDecimal.ZERO;
        toReturn.addAll(this.listEjecucionesGrupoEjecucion);
        this.listEjecucionesGrupoEjecucion.clear();
        return toReturn;
      }
      else {
        BigDecimal titulosRemovidos = BigDecimal.ZERO;
        DesgloseManualEjecucionDTO ejecucionPartida;
        for (DesgloseManualEjecucionDTO eje : this.listEjecucionesGrupoEjecucion) {
          if (eje.getImtitulos().compareTo(titulosDevolver.subtract(titulosRemovidos)) >= 0) {
            eje.setImtitulos(eje.getImtitulos().subtract(titulosDevolver.subtract(titulosRemovidos)));
            ejecucionPartida = new DesgloseManualEjecucionDTO(eje);
            ejecucionPartida.setImtitulos(titulosDevolver.subtract(titulosRemovidos));
            titulosRemovidos = titulosDevolver;
            toReturn.add(ejecucionPartida);
            break;
          }
          else {
            titulosRemovidos = titulosRemovidos.add(eje.getImtitulos());
            toReturn.add(eje);

          }
          if (titulosRemovidos.compareTo(titulosDevolver) >= 0) {
            break;
          }
        }

        if (!toReturn.isEmpty()) {
          this.listEjecucionesGrupoEjecucion.removeAll(toReturn);
        }
        this.titulos = this.titulos.subtract(titulosDevolver);
      }
    }
    return toReturn;
  }

  public synchronized DesgloseManualEjecucionDTO removeOne() {
    if (!isEmpty()) {
      List<DesgloseManualEjecucionDTO> toReturn = remove(BigDecimal.ONE);
      if (CollectionUtils.isNotEmpty(toReturn))
        return toReturn.get(0);
    }
    return null;

  }
}
