package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.DesgloseIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.InformacionCompensacionDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.contabilidad.database.dao.ApunteContableAlcDao;
import sibbac.business.contabilidad.database.model.ApunteContableAlc;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.Tmct0referenciaTitularBo;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.dao.AlcOrdenDetalleFacturaDao;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0alcHelper;
import sibbac.business.wrappers.database.model.AlcEstadoCont;
import sibbac.business.wrappers.database.model.AlcOrdenDetalleFactura;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0afiId;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;

@Service
public class CuadreEccSepararMovimientosBo {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccSepararMovimientosBo.class);

  private static final String AUDIT_USER = "CECC_SEP_M";

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucioBo;

  @Autowired
  private Tmct0logBo logBo;

  @Qualifier(value = "cuadreEccRevisarAndImportarDesglosesSubListBo")
  @Autowired
  private CuadreEccRevisarAndImportarDesglosesSubListBo revisarDesglosesSubList;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  @Autowired
  private AlcOrdenDetalleFacturaDao alcOrdenesDetalleFacturaDao;

  @Autowired
  private Tmct0referenciaTitularBo refTitBo;

  @Autowired
  private Tmct0enviotitularidadDao tiDao;

  @Autowired
  private ApunteContableAlcDao apunteContableAlcDao;

  @Autowired
  private Tmct0afiDao afiDao;

  @Autowired
  private DesgloseTmct0alcHelper desgloseAlcHelper;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao dcRtDao;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  public CuadreEccSepararMovimientosBo() {
  }

  /**
   * @param beans
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void separarMovimientos(List<CuadreEccDbReaderRecordBeanDTO> beans) throws SIBBACBusinessException {
    LOG.trace("[CuadreEccSepararMovimientosBo :: separarMovimientos] inicio");
    Tmct0alo alo;
    List<Tmct0alc> alcs;
    int updates;
    Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosByIc;
    for (CuadreEccDbReaderRecordBeanDTO bean : beans) {
      LOG.trace("[CuadreEccSepararMovimientosBo :: separarMovimientos] procesando bean: {}", bean);
      LOG.trace("[CuadreEccSepararMovimientosBo :: separarMovimientos] buscando alcs: {}", bean);
      try {
        alo = aloBo.findById(bean.getTmct0aloId());
        alcs = alcBo.findByAlloId(bean.getTmct0aloId());
        if (CollectionUtils.isNotEmpty(alcs)) {
          for (Tmct0alc tmct0alc : alcs) {

            if (tmct0alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              movimientosByIc = new HashMap<InformacionCompensacionDTO, List<Tmct0movimientoecc>>();

              this.recuperarInformacionAndAgruparByInformacionCompensacionAndCrearMovimientosFaltan(tmct0alc,
                  movimientosByIc);

              if (movimientosByIc.size() <= 0) {
                LOG.warn("[CuadreEccSepararMovimientosBo :: separarMovimientos] NO TIENE MOVIMIENTOS: {}", bean);
                String msg = MessageFormat
                    .format(
                        "CUADRE_ECC:SEPARAR_MOVS_BY_IC: NO TIENE MOVS ALC:{0} IMP:{5}, ESTS asig:{1} tit:{2} cont:{3} clearing:{4}, IMP_NET:{6} COMM_LIQ:{7}",
                        tmct0alc.getId().getNucnfliq(), tmct0alc.getCdestadoasig(),
                        tmct0alc.getEstadotit() != null ? tmct0alc.getEstadotit().getIdestado() : 0, tmct0alc
                            .getEstadocont() != null ? tmct0alc.getEstadocont().getIdestado() : 0, tmct0alc
                            .getEstadoentrec() != null ? tmct0alc.getEstadoentrec().getIdestado() : 0, tmct0alc
                            .getImefeagr(), tmct0alc.getImnfiliq(), tmct0alc.getImfinsvb());
                logBo.insertarRegistro(tmct0alc.getId().getNuorden(), tmct0alc.getId().getNbooking(), tmct0alc.getId()
                    .getNucnfclt(), msg, "", AUDIT_USER);
              }
              else if (movimientosByIc.size() == 1) {
                LOG.trace("[CuadreEccSepararMovimientosBo :: separarMovimientos] Movimientos correctos por alc: {}",
                    bean);
                String msg = MessageFormat
                    .format(
                        "CUADRE_ECC:SEPARAR_MOVS_BY_IC: MOVS OK ALC:{0} IMP:{5}, ESTS asig:{1} tit:{2} cont:{3} clearing:{4}, IMP_NET:{6} COMM_LIQ:{7}",
                        tmct0alc.getId().getNucnfliq(), tmct0alc.getCdestadoasig(),
                        tmct0alc.getEstadotit() != null ? tmct0alc.getEstadotit().getIdestado() : 0, tmct0alc
                            .getEstadocont() != null ? tmct0alc.getEstadocont().getIdestado() : 0, tmct0alc
                            .getEstadoentrec() != null ? tmct0alc.getEstadoentrec().getIdestado() : 0, tmct0alc
                            .getImefeagr(), tmct0alc.getImnfiliq(), tmct0alc.getImfinsvb());
                this.revisarEstadosAndInformacionCompensacion(tmct0alc);
                logBo.insertarRegistro(tmct0alc.getId().getNuorden(), tmct0alc.getId().getNbooking(), tmct0alc.getId()
                    .getNucnfclt(), msg, "", AUDIT_USER);

              }
              else {
                LOG.warn("[CuadreEccSepararMovimientosBo :: separarMovimientos] Mas de una ic por alc: {}", bean);
                this.separarAlcsByInformacionCompensacion(alo, tmct0alc, movimientosByIc);

              }

            }
          }// fin alc0s
        }
        LOG.trace(
            "[CuadreEccSepararMovimientosBo :: separarMovimientos] Actualizando estados cuadre ecc sibbac de alo {} a {}",
            bean.getTmct0aloId(), EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.MOVIMIENTOS_REVISADOS.getId());
        updates = cuadreEccSibbacBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnfclt(
            EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.MOVIMIENTOS_REVISADOS.getId(), bean.getTmct0aloId()
                .getNuorden(), bean.getTmct0aloId().getNbooking(), bean.getTmct0aloId().getNucnfclt());
        LOG.trace(
            "[CuadreEccSepararMovimientosBo :: separarMovimientos] se han actualizando {} estados cuadre ecc sibbac de alo {} a {}",
            updates, bean.getTmct0aloId(), EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.MOVIMIENTOS_REVISADOS.getId());
      }
      catch (Exception e) {
        logBo.insertarRegistroNewTransaction(bean.getTmct0aloId().getNuorden(), bean.getTmct0aloId().getNbooking(),
            e.getMessage(), "", AUDIT_USER);
        LOG.trace(
            "[CuadreEccSepararMovimientosBo :: separarMovimientos] Actualizando estados cuadre ecc sibbac de alo {} a {}",
            bean.getTmct0aloId(), EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.MOVIMIENTOS_REVISADOS_ERROR.getId());
        updates = cuadreEccSibbacBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnfcltNewTransaction(
            EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.MOVIMIENTOS_REVISADOS_ERROR.getId(), bean.getTmct0aloId()
                .getNuorden(), bean.getTmct0aloId().getNbooking(), bean.getTmct0aloId().getNucnfclt());
        LOG.trace(
            "[CuadreEccSepararMovimientosBo :: separarMovimientos] se han actualizando {} estados cuadre ecc sibbac de alo {} a {}",
            updates, bean.getTmct0aloId(),
            EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.MOVIMIENTOS_REVISADOS_ERROR.getId());
        throw new SIBBACBusinessException("INCIDENCIA procesando la orden:" + bean.getTmct0aloId(), e);
      }
    }

    LOG.trace("[CuadreEccSepararMovimientosBo :: separarMovimientos] final");
  }

  /**
   * @param tmct0alc
   */
  private void revisarEstadosAndInformacionCompensacion(Tmct0alc tmct0alc) {
    LOG.trace("[CuadreEccSepararMovimientosBo :: revisarEstadosAndInformacionCompensacion] inicio");

    LOG.debug("[CuadreEccSepararMovimientosBo :: separarMovimientos] el alc tiene estado {} revisamos los estados.",
        tmct0alc.getCdestadoasig());
    List<Tmct0desglosecamara> dcs = dcBo.findAllByTmct0alcId(tmct0alc.getId());
    for (Tmct0desglosecamara dc : dcs) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && dc.getTmct0estadoByCdestadoasig().getIdestado() <= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
        LOG.debug("[CuadreEccSepararMovimientosBo :: separarMovimientos] el dc tiene estado {} revisamos los estados.",
            dc.getTmct0estadoByCdestadoasig().getIdestado());
        cuadreEccSibbacBo.escalarEstadoFromMovimientos(dc, AUDIT_USER);

      }
      cuadreEccSibbacBo.escalarInformacionCompensacionFromMovimientos(dc, AUDIT_USER);
    }

    LOG.trace("[CuadreEccSepararMovimientosBo :: revisarEstadosAndInformacionCompensacion] final");
  }

  /**
   * @param tmct0alc
   * @param movimientosByIc
   */
  private void recuperarInformacionAndAgruparByInformacionCompensacionAndCrearMovimientosFaltan(Tmct0alc tmct0alc,
      Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosByIc) {
    LOG.trace("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] inicio");
    LOG.trace("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] buscando dcs: {}",
        tmct0alc.getId());
    // dcs = dcBo.findAllByTmct0alcId(tmct0alc.getId());
    Tmct0movimientoecc movCreado;
    List<Tmct0desglosecamara> dcs = tmct0alc.getTmct0desglosecamaras();
    List<Tmct0movimientoecc> movs;
    List<Tmct0movimientoecc> movsByIc;
    List<Tmct0movimientooperacionnuevaecc> movsN;
    InformacionCompensacionDTO ic;
    BigDecimal titulosAcumuladosDc = BigDecimal.ZERO;
    for (Tmct0desglosecamara dc : dcs) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        titulosAcumuladosDc = titulosAcumuladosDc.add(dc.getNutitulos());
        LOG.trace("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] buscando movs: {}",
            tmct0alc.getId());
        movs = movBo.findByIdDesgloseCamara(dc.getIddesglosecamara());
        // movs = dc.getTmct0movimientos();
        dc.setTmct0movimientos(movs);
        BigDecimal titulosAcumuladosDct = BigDecimal.ZERO;
        BigDecimal titulosAcumuladosMov = BigDecimal.ZERO;
        for (Tmct0movimientoecc mov : movs) {
          if (mov.getCdestadomov() != null && mov.getCdestadomov().trim().equals(EstadosEccMovimiento.FINALIZADO.text)) {
            titulosAcumuladosMov = titulosAcumuladosMov.add(mov.getImtitulos());
            ic = new InformacionCompensacionDTO(mov.getMiembroCompensadorDestino(), mov.getCuentaCompensacionDestino(),
                mov.getMiembroCompensadorDestino(), mov.getCodigoReferenciaAsignacion());
            LOG.debug(
                "[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] encontrado mov {} titulos e ic {}",
                mov.getImtitulos(), ic);
            if ((movsByIc = movimientosByIc.get(ic)) == null) {
              movsByIc = new ArrayList<Tmct0movimientoecc>();
              movimientosByIc.put(ic, movsByIc);
            }
            movsByIc.add(mov);

            movsN = movNBo.findByIdMovimiento(mov.getIdmovimiento());
            mov.setTmct0movimientooperacionnuevaeccs(movsN);
            // movsN = mov.getTmct0movimientooperacionnuevaeccs();
            if (CollectionUtils.isNotEmpty(movsN)) {
              BigDecimal titulosAcumuladosMovN = BigDecimal.ZERO;
              for (Tmct0movimientooperacionnuevaecc movNFor : movsN) {
                titulosAcumuladosMovN = titulosAcumuladosMovN.add(movNFor.getImtitulos());
                if (movNFor.getNudesglose() != null) {
                  movNFor.setTmct0desgloseclitit(dctBo.findById(movNFor.getNudesglose()));
                  if (movNFor.getTmct0desgloseclitit() == null) {
                    LOG.warn("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] movimiento operacion nueva no encuentra desglose que lo creo.");
                  }
                  else {
                    titulosAcumuladosDct = titulosAcumuladosDct.add(movNFor.getTmct0desgloseclitit().getImtitulos());
                  }
                }
                else {

                  LOG.warn("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] movimiento operacion nueva no tiene nudesglose.");
                }
              }
              if (titulosAcumuladosMovN.compareTo(mov.getImtitulos()) != 0) {
                LOG.warn(
                    "[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] Titulos movN: {} <> titulos mov: {}. idmov:{} ",
                    titulosAcumuladosMovN, mov.getImtitulos(), mov.getIdmovimiento());
              }
            }
            else {
              LOG.warn("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] no tiene movs op nueva");

            }

          }// movimiento finalizado
        }// fin for movs
        if (titulosAcumuladosDct.compareTo(dc.getNutitulos()) != 0) {
          LOG.warn(
              "[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] Titulos dct: {} <> titulos dc: {}. iddc:{} ",
              titulosAcumuladosDct, dc.getNutitulos(), dc.getIddesglosecamara());

          for (Tmct0desgloseclitit dct : dc.getTmct0desgloseclitits()) {
            movsN = movNBo.findByNudesglose(dct.getNudesglose());
            BigDecimal titulosAcumulados = BigDecimal.ZERO;
            BigDecimal efectivoAcumulado = BigDecimal.ZERO;
            for (Tmct0movimientooperacionnuevaecc movN : movsN) {
              if (movN.getTmct0movimientoecc().getCdestadomov() != null
                  && movN.getTmct0movimientoecc().getCdestadomov().trim().equals(EstadosEccMovimiento.FINALIZADO.text)) {
                titulosAcumulados = titulosAcumulados.add(movN.getImtitulos());
                efectivoAcumulado = efectivoAcumulado.add(movN.getImefectivo());
              }
            }

            if (titulosAcumulados.compareTo(BigDecimal.ZERO) == 0) {
              movCreado = crearMovimientosDesglosesSinMovimientos(dc, dct);
              if (movCreado != null) {
                ic = new InformacionCompensacionDTO(movCreado.getMiembroCompensadorDestino(),
                    movCreado.getCuentaCompensacionDestino(), movCreado.getMiembroCompensadorDestino(),
                    movCreado.getCodigoReferenciaAsignacion());
                LOG.debug(
                    "[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] encontrado mov {} titulos e ic {}",
                    movCreado.getImtitulos(), ic);
                if ((movsByIc = movimientosByIc.get(ic)) == null) {
                  movsByIc = new ArrayList<Tmct0movimientoecc>();
                  movimientosByIc.put(ic, movsByIc);
                }
                movsByIc.add(movCreado);
              }
            }
            else if (titulosAcumulados.compareTo(dct.getImtitulos()) < 0) {
              LOG.warn("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion]INCIDENCIA- titulos movimiento y desglose no coinciden");
            }
          }
        }

        if (titulosAcumuladosMov.compareTo(dc.getNutitulos()) != 0) {
          LOG.warn(
              "[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] Titulos mov: {} <> titulos dc: {}. iddc:{} ",
              titulosAcumuladosMov, dc.getNutitulos(), dc.getIddesglosecamara());
        }
      }
    }// fin for dcs
    if (titulosAcumuladosDc.compareTo(tmct0alc.getNutitliq()) != 0) {
      LOG.warn(
          "[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] Titulos dcs: {} <> titulos alc: {}. alc:{} ",
          titulosAcumuladosDc, tmct0alc.getNutitliq(), tmct0alc.getId());
    }
    LOG.trace("[CuadreEccSepararMovimientosBo :: agruparInformacionByInformacionCompensacion] final");
  }

  /**
   * @param alo
   * @param tmct0alc
   * @param movimientosByIc
   * @throws SIBBACBusinessException
   */
  private void separarAlcsByInformacionCompensacion(Tmct0alo alo, Tmct0alc tmct0alc,
      Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosByIc) throws SIBBACBusinessException {
    LOG.trace("[CuadreEccSepararMovimientosBo :: separarAlcsByInformacionCompensacion] inicio");

    List<Tmct0alc> alcsMovsIc = crearAlcsSeparadosPorICMovs(alo, tmct0alc, movimientosByIc);
    BigDecimal titulosAcumulados = BigDecimal.ZERO;
    for (Tmct0alc tmct0alc2 : alcsMovsIc) {
      titulosAcumulados = titulosAcumulados.add(tmct0alc2.getNutitliq());
    }
    LOG.debug(
        "[CuadreEccSepararMovimientosBo :: separarAlcsByInformacionCompensacion] titulos alc original: {} titulos alcs nuevos: {}",
        tmct0alc.getNutitliq(), titulosAcumulados);
    if (titulosAcumulados.compareTo(tmct0alc.getNutitliq()) != 0) {
      String msg = MessageFormat.format(
          "INCIDENCIA-Titulos nuevos alc {0} no coinciden con los titulos {1} del alc viejo {2} ", titulosAcumulados,
          tmct0alc.getNutitliq(), tmct0alc.getId());
      throw new SIBBACBusinessException(msg);
    }

    this.persistirAlcs(alcsMovsIc);

    alcsMovsIc = alcBo.findByAlloIdActivos(alo.getId());
    CalculosEconomicosNacionalBo calculos = new CalculosEconomicosNacionalBo(alo, alcsMovsIc);
    calculos.acumulateFromTmct0alcs();

    for (Tmct0alc tmct0alc2 : alcsMovsIc) {
      this.revisarEstadosAndInformacionCompensacion(tmct0alc2);
    }

    this.clonarTitulares(tmct0alc, alcsMovsIc);

    this.crearLogsProceso(true, tmct0alc, false, alcsMovsIc);

    this.repartirContabilidadAndFacturacion(tmct0alc, alcsMovsIc);

    this.crearLogsProceso(false, tmct0alc, true, alcsMovsIc);
    // tmct0alc.setEstadocont(new
    // Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    tmct0alc.setEstadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
    revisarDesglosesSubList.bajaAlc(tmct0alc, new ArrayList<DesgloseIFDTO>());
    tmct0alc.setCdusuaud(AUDIT_USER);
    alcBo.save(tmct0alc);

    LOG.trace("[CuadreEccSepararMovimientosBo :: separarAlcsByInformacionCompensacion] final");

  }

  /**
   * @param alo
   * @param tmct0alc
   * @param movimientosByIc
   * @return
   */
  private List<Tmct0alc> crearAlcsSeparadosPorICMovs(Tmct0alo alo, Tmct0alc tmct0alc,
      Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosByIc) {
    LOG.trace("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] inicio");
    Tmct0infocompensacion ic;
    List<Tmct0movimientoecc> movsByIc;
    List<Tmct0movimientooperacionnuevaecc> movsN;

    List<Tmct0alc> alcsMovsIc = new ArrayList<Tmct0alc>();
    Short maxNucnfliq = alcBo.getMaxNucnfliqByTmct0aloId(new Tmct0aloId(tmct0alc.getId().getNucnfclt(), tmct0alc
        .getId().getNbooking(), tmct0alc.getId().getNuorden()));

    for (InformacionCompensacionDTO icFor : movimientosByIc.keySet()) {
      LOG.trace("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] Procesando movimientos de ic: {}",
          icFor);
      movsByIc = movimientosByIc.get(icFor);

      if (CollectionUtils.isNotEmpty(movsByIc)) {
        Map<String, Tmct0desglosecamara> dcsByCamara = new HashMap<String, Tmct0desglosecamara>();
        Tmct0alc alcClonado = new Tmct0alc();
        CloneObjectHelper.copyBasicFields(tmct0alc, alcClonado, null);
        // alcClonado.setIdinfocomp(null);
        // TODO QUITAR CUANDO SUBAN LA ULTIMA VERSION DE WRAPPERS, SE SOLUCIONA
        // EN EL CLEAR ECONOMIC DATA DEL ACUMULATE
        // INICIO
        alcClonado.setImderges(BigDecimal.ZERO);
        alcClonado.setImcanoncontreu(BigDecimal.ZERO);
        alcClonado.setImcanoncontrdv(BigDecimal.ZERO);
        alcClonado.setImderliq(BigDecimal.ZERO);
        alcClonado.setImcanonliqeu(BigDecimal.ZERO);
        alcClonado.setImcanonliqdv(BigDecimal.ZERO);

        alcClonado.setImcanoncompeu(BigDecimal.ZERO);
        alcClonado.setImcanoncompdv(BigDecimal.ZERO);
        alcClonado.setImnfiliq(BigDecimal.ZERO);

        // TODO QUITAR CUANDO SUBAN LA ULTIMA VERSION DE WRAPPERS, SE SOLUCIONA
        // EN EL CLEAR ECONOMIC DATA DEL ACUMULATE
        // FINAL
        if (alcClonado.getEstadocont() == null || alcClonado.getEstadocont().getIdestado() == null) {
          LOG.warn("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] el estado contable del alc original es incorrecto.");
          alcClonado.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
        }
        if (alcClonado.getEstadoentrec() == null || alcClonado.getEstadoentrec().getIdestado() == null) {
          LOG.warn("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] el estado ent rec del alc original es incorrecto.");
          alcClonado.setEstadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
        }
        // String nbooking, String nuorden, short nucnfliq, String nucnfclt)
        maxNucnfliq = new Short(maxNucnfliq + 1 + "");
        alcClonado.setId(new Tmct0alcId(tmct0alc.getId().getNbooking(), tmct0alc.getId().getNuorden(), new Short(
            maxNucnfliq), tmct0alc.getNucnfclt()));

        alcClonado.setNutitliq(BigDecimal.ZERO);
        alcClonado.setTmct0desglosecamaras(new ArrayList<Tmct0desglosecamara>());
        alcClonado.setEstadosContables(new ArrayList<AlcEstadoCont>());
        alcClonado.setFhaudit(new Date());
        alcClonado.setCdusuaud(AUDIT_USER);

        for (Tmct0movimientoecc movFor : movsByIc) {
          LOG.debug("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] clonando mov titulos: {} ic: {}",
              movFor.getImtitulos(), icFor);

          if (movFor.getTmct0movimientooperacionnuevaeccs().get(0).getTmct0desgloseclitit() == null) {
            LOG.warn("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] No encontrado dct");
            continue;
          }
          Tmct0desglosecamara dcClonado = dcsByCamara.get(StringUtils.trim(movFor
              .getTmct0movimientooperacionnuevaeccs().get(0).getTmct0desgloseclitit().getCdcamara()));

          if (dcClonado == null) {
            Tmct0desglosecamara dc = dcBo.findById(movFor.getTmct0desglosecamara().getIddesglosecamara());
            dcClonado = new Tmct0desglosecamara();
            CloneObjectHelper.copyBasicFields(dc, dcClonado, null);
            dcClonado.setIddesglosecamara(null);
            // dcClonado.setTmct0DesgloseCamaraEnvioRt(new
            // ArrayList<Tmct0DesgloseCamaraEnvioRt>());
            dcClonado.setTmct0desgloseclitits(new ArrayList<Tmct0desgloseclitit>());
            dcClonado.setTmct0DesgloseCamaraEnvioRt(new ArrayList<Tmct0DesgloseCamaraEnvioRt>());
            dcClonado.setTmct0movimientos(new ArrayList<Tmct0movimientoecc>());
            dcClonado.setNutitulos(BigDecimal.ZERO);
            dcClonado.setAuditFechaCambio(new Date());
            dcClonado.setAuditUser(AUDIT_USER);

            ic = new Tmct0infocompensacion();
            ic.setCdctacompensacion(movFor.getCuentaCompensacionDestino());
            ic.setCdmiembrocompensador(movFor.getMiembroCompensadorDestino());
            ic.setCdrefasignacion(movFor.getCodigoReferenciaAsignacion());
            ic.setAuditFechaCambio(new Date());
            ic.setAuditUser(AUDIT_USER);
            dcClonado.setTmct0infocompensacion(ic);

            LOG.debug("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] clonando ic: {}", ic);
            if (dcClonado.getTmct0estadoByCdestadocont() == null
                || dcClonado.getTmct0estadoByCdestadocont().getIdestado() == null) {
              LOG.warn("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] el estado contable del alc original es incorrecto.");
              dcClonado.setTmct0estadoByCdestadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO
                  .getId()));
            }
            if (dcClonado.getTmct0estadoByCdestadoentrec() == null
                || dcClonado.getTmct0estadoByCdestadoentrec().getIdestado() == null) {
              LOG.warn("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] el estado ent rec del alc original es incorrecto.");
              dcClonado
                  .setTmct0estadoByCdestadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
            }
            LOG.debug("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] clonando dc titulos: {}",
                dc.getNutitulos());
            dcsByCamara.put(
                StringUtils.trim(movFor.getTmct0movimientooperacionnuevaeccs().get(0).getTmct0desgloseclitit()
                    .getCdcamara()), dcClonado);
            dcClonado.setTmct0alc(alcClonado);
            alcClonado.getTmct0desglosecamaras().add(dcClonado);

          }

          Tmct0movimientoecc movClonado = new Tmct0movimientoecc();
          CloneObjectHelper.copyBasicFields(movFor, movClonado, null);
          movClonado.setTmct0movimientooperacionnuevaeccs(new ArrayList<Tmct0movimientooperacionnuevaecc>());
          movClonado.setIdmovimiento(null);
          movClonado.setAuditFechaCambio(new Date());
          movClonado.setAuditUser(AUDIT_USER);
          movClonado.setTmct0desglosecamara(dcClonado);
          dcClonado.getTmct0movimientos().add(movClonado);

          movsN = movFor.getTmct0movimientooperacionnuevaeccs();
          for (Tmct0movimientooperacionnuevaecc movNFor : movsN) {
            LOG.debug(
                "[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] clonando movn titulos: {} nudesglose: {} ic: {}",
                movNFor.getImtitulos(), movNFor.getNudesglose(), icFor);
            Tmct0movimientooperacionnuevaecc movNClonado = new Tmct0movimientooperacionnuevaecc();
            CloneObjectHelper.copyBasicFields(movNFor, movNClonado, null);
            movNClonado.setIdmovimientooperacionnuevaec(null);
            movNClonado.setNudesglose(null);
            movNClonado.setTmct0movimientoecc(movClonado);
            movNClonado.setAuditFechaCambio(new Date());
            movNClonado.setAuditUser(AUDIT_USER);
            movClonado.getTmct0movimientooperacionnuevaeccs().add(movNClonado);
            LOG.debug(
                "[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] clonando dct titulos: {} nudesglose: {} ic: {}",
                movNFor.getTmct0desgloseclitit().getImtitulos(), movNFor.getTmct0desgloseclitit().getNudesglose(),
                icFor);
            Tmct0desgloseclitit dctClonado = new Tmct0desgloseclitit();
            CloneObjectHelper.copyBasicFields(movNFor.getTmct0desgloseclitit(), dctClonado, null);
            dctClonado.setNudesglose(null);
            dctClonado.setAuditFechaCambio(new Date());
            dctClonado.setAuditUser(AUDIT_USER);
            movNClonado.setTmct0desgloseclitit(dctClonado);
            dctClonado.setTmct0desglosecamara(dcClonado);
            dcClonado.getTmct0desgloseclitits().add(dctClonado);

            alcClonado.setNutitliq(alcClonado.getNutitliq().add(dctClonado.getImtitulos()));
            dcClonado.setNutitulos(dcClonado.getNutitulos().add(dctClonado.getImtitulos()));

          }

        }// fin for movs

        alcsMovsIc.add(alcClonado);

      }
    }
    LOG.trace("[CuadreEccSepararMovimientosBo :: crearAlcsSeparadosPorICMovs] fin");
    return alcsMovsIc;
  }

  /**
   * @param alcOriginal
   * @param alcsSustitutos
   */
  private void clonarTitulares(Tmct0alc alcOriginal, List<Tmct0alc> alcsSustitutos) {
    LOG.trace("[CuadreEccSepararMovimientosBo :: clonarTitulares] inicio");

    LOG.trace("[CuadreEccSepararMovimientosBo :: clonarTitulares] buscando titulares activos...");
    List<Tmct0afi> titularesActivosAlc = afiDao.findActivosByAlcData(alcOriginal.getId().getNuorden(), alcOriginal
        .getId().getNbooking(), alcOriginal.getId().getNucnfclt(), alcOriginal.getId().getNucnfliq());
    LOG.trace("[CuadreEccSepararMovimientosBo :: clonarTitulares] encontrados {} titulares activos.",
        titularesActivosAlc.size());

    if (CollectionUtils.isNotEmpty(titularesActivosAlc)) {
      for (Tmct0alc tmct0alc : alcsSustitutos) {
        List<Tmct0afi> afisClonados = new ArrayList<Tmct0afi>();
        for (Tmct0afi tmct0afi : titularesActivosAlc) {
          Tmct0afi afiClonado = new Tmct0afi();
          CloneObjectHelper.copyBasicFields(tmct0afi, afiClonado, null);
          afiClonado.setId(new Tmct0afiId(tmct0alc.getId().getNuorden(), tmct0alc.getId().getNbooking(), tmct0alc
              .getId().getNucnfclt(), tmct0alc.getId().getNucnfliq(), afiDao.getNewTmct0afiSequence()));
          afiClonado.setFhaudit(new Date());
          afiClonado.setCdusuaud(AUDIT_USER);
          afisClonados.add(afiClonado);
        }
        afiDao.save(afisClonados);
      }
    }
    else {
      LOG.warn("[CuadreEccSepararMovimientosBo :: clonarTitulares] no tiene titulares dados de alta.");
    }
    LOG.trace("[CuadreEccSepararMovimientosBo :: clonarTitulares] inicio");
  }

  /**
   * @param alcOriginal
   * @param alcsSustitutos
   */
  private void repartirContabilidadAndFacturacion(Tmct0alc alcOriginal, List<Tmct0alc> alcsSustitutos) {
    LOG.trace("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] inicio");

    // facturas
    // AlcOrdenes
    // relacion con conta
    // ApunteContableAlc
    Collections.sort(alcsSustitutos, new Comparator<Tmct0alc>() {

      @Override
      public int compare(Tmct0alc arg0, Tmct0alc arg1) {
        if (arg0.getImefeagr().compareTo(arg1.getImefeagr()) == 0) {
          return new BigDecimal(arg0.getId().getNucnfliq() + "").compareTo(new BigDecimal(arg1.getId().getNucnfliq()
              + ""));
        }
        else
          return arg0.getImefeagr().compareTo(arg1.getImefeagr());
      }
    });
    LOG.debug("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] buscando facturas.");
    AlcOrdenes factura = alcOrdenesBo.findByTmct0alc(alcOriginal);
    if (factura != null) {
      LOG.debug("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] encontrada factura: {}",
          factura.getId());
      List<AlcOrdenDetalleFactura> detalleFactura = factura.getAlcOrdenDetallesFactura();
      Collections.sort(detalleFactura, new Comparator<AlcOrdenDetalleFactura>() {

        @Override
        public int compare(AlcOrdenDetalleFactura arg0, AlcOrdenDetalleFactura arg1) {
          return arg0.getImfinsvb().compareTo(arg1.getImfinsvb());
        }

      });

      Map<Long, BigDecimal> acumuladoMap = new HashMap<Long, BigDecimal>();
      BigDecimal acumulado;
      int i = 0;
      for (Tmct0alc tmct0alc : alcsSustitutos) {
        i++;
        LOG.debug(
            "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] asignando factura {} al alc {}",
            factura.getId(), tmct0alc.getId());
        AlcOrdenes facturaClonada = new AlcOrdenes();
        CloneObjectHelper.copyBasicFields(factura, facturaClonada, null);
        facturaClonada.setId(null);
        facturaClonada.setAlcOrdenDetallesFactura(new ArrayList<AlcOrdenDetalleFactura>());
        facturaClonada.setNuorden(tmct0alc.getId().getNuorden());
        facturaClonada.setNbooking(tmct0alc.getId().getNbooking());
        facturaClonada.setNucnfclt(tmct0alc.getId().getNucnfclt());
        facturaClonada.setNucnfliq(tmct0alc.getId().getNucnfliq());
        facturaClonada.setAuditDate(new Date());
        facturaClonada.setAuditUser(AUDIT_USER);

        facturaClonada = alcOrdenesBo.save(facturaClonada);

        for (AlcOrdenDetalleFactura detalle : detalleFactura) {
          if ((acumulado = acumuladoMap.get(detalle.getId())) == null) {
            acumulado = BigDecimal.ZERO;
          }

          AlcOrdenDetalleFactura detalleClonado = new AlcOrdenDetalleFactura();
          CloneObjectHelper.copyBasicFields(detalle, detalleClonado, null);
          detalleClonado.setId(null);
          detalleClonado.setAlcOrden(facturaClonada);
          if (i == alcsSustitutos.size()) {
            detalleClonado.setImfinsvb(detalle.getImfinsvb().subtract(acumulado));
          }
          else {
            detalleClonado.setImfinsvb(detalle.getImfinsvb().multiply(tmct0alc.getImefeagr())
                .divide(alcOriginal.getImefeagr(), 2, RoundingMode.HALF_DOWN));

          }
          acumulado = acumulado.add(detalleClonado.getImfinsvb());
          acumuladoMap.put(detalle.getId(), acumulado);
          detalleClonado.setAuditDate(new Date());
          detalleClonado.setAuditUser(AUDIT_USER);
          facturaClonada.getAlcOrdenDetallesFactura().add(detalleClonado);
        }

        alcOrdenesDetalleFacturaDao.save(facturaClonada.getAlcOrdenDetallesFactura());
      }
    }
    if (alcOriginal.getEstadocont().getIdestado() > EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_COBRO_BME.getId()) {
      LOG.debug("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] El estado contable requiere de repartir la contabilidad.");
      LOG.debug("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] buscando apuntes contables.");
      List<ApunteContableAlc> apuntesContables = apunteContableAlcDao.findAllByAlc(alcOriginal);
      LOG.debug(
          "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] encontrados {} apuntes contables.",
          apuntesContables.size());
      for (ApunteContableAlc apunteContableAlc : apuntesContables) {
        BigDecimal importeAcumulado = BigDecimal.ZERO;
        int i = 0;
        for (Tmct0alc tmct0alc : alcsSustitutos) {
          BigDecimal importe = apunteContableAlc.getImporte();
          i++;
          if (i == alcsSustitutos.size()) {
            importe = apunteContableAlc.getImporte().subtract(importeAcumulado);
          }
          else {
            importe = apunteContableAlc.getImporte().multiply(tmct0alc.getImefeagr())
                .divide(alcOriginal.getImefeagr(), 2, RoundingMode.HALF_DOWN);
          }
          LOG.debug(
              "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] Al alc:{} le corresponden {} del apunte contable alc {} apunte {}",
              tmct0alc.getId(), importe, apunteContableAlc.getId(),
              apunteContableAlc.getApunte() != null ? apunteContableAlc.getApunte().getId() : -1);
          ApunteContableAlc apunteClonado = new ApunteContableAlc();
          CloneObjectHelper.copyBasicFields(apunteContableAlc, apunteClonado, null);
          apunteClonado.setId(null);
          apunteClonado.setAlc(tmct0alc);
          apunteClonado.setImporte(importe);
          apunteClonado.setAuditDate(new Date());
          apunteClonado.setAuditUser(AUDIT_USER);
          apunteContableAlcDao.save(apunteClonado);
          importeAcumulado = importeAcumulado.add(importe);

        }
      }
      LOG.debug(
          "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] Colocando estado contabilidad EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO al alc original {}",
          alcOriginal.getId());
      alcOriginal.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));

      List<Tmct0desglosecamara> dcs = dcBo.findAllByTmct0alcId(alcOriginal.getId());
      for (Tmct0desglosecamara dc : dcs) {
        LOG.trace(
            "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] Colocando estado contabilidad EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO al dc original {}",
            dc.getIddesglosecamara());
        dc.setTmct0estadoByCdestadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
        dc.setAuditFechaCambio(new Date());
        dc.setAuditUser(AUDIT_USER);
      }

      dcBo.save(dcs);
    }
    else {
      LOG.debug("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] El estado contable se puede retroceder.");
      for (Tmct0alc tmct0alc : alcsSustitutos) {
        LOG.debug(
            "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] Colocando estado contabilidad EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO al alc sustituto {}",
            tmct0alc.getId());
        tmct0alc.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
        List<Tmct0desglosecamara> dcs = dcBo.findAllByTmct0alcId(tmct0alc.getId());
        for (Tmct0desglosecamara dc : dcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            LOG.trace(
                "[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] Colocando estado contabilidad EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO al dc sustituto {}",
                dc.getIddesglosecamara());
            dc.setTmct0estadoByCdestadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
            dc.setAuditFechaCambio(new Date());
            dc.setAuditUser(AUDIT_USER);
          }
        }
        dcBo.save(dcs);
      }
      alcsSustitutos = alcBo.save(alcsSustitutos);
    }
    LOG.trace("[CuadreEccSepararMovimientosBo :: repartirContabilidadAndFacturacion] final");
  }

  /**
   * @param alcs
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void persistirAlcs(List<Tmct0alc> alcs) {
    List<Tmct0desglosecamara> dcs;
    List<Tmct0desgloseclitit> dcts;
    List<Tmct0movimientoecc> movs;
    List<Tmct0movimientooperacionnuevaecc> movsn;
    List<Tmct0enviotitularidad> tis;
    List<Tmct0DesgloseCamaraEnvioRt> rts;
    Tmct0referenciatitular refTit;
    for (Tmct0alc tmct0alc : alcs) {
      dcs = new ArrayList<Tmct0desglosecamara>(tmct0alc.getTmct0desglosecamaras());
      tmct0alc.getTmct0desglosecamaras().clear();
      refTit = tmct0alc.getReferenciaTitular();
      if (refTit != null) {
        tis = new ArrayList<Tmct0enviotitularidad>(refTit.getTmct0enviotitularidads());
        refTit.getTmct0enviotitularidads().clear();
        refTit = refTitBo.save(refTit);
        tiDao.save(tis);// los dc rt estan colgando del desglose camara
      }

      tmct0alc = alcBo.save(tmct0alc);
      for (Tmct0desglosecamara dc : dcs) {
        dcts = new ArrayList<Tmct0desgloseclitit>(dc.getTmct0desgloseclitits());
        dc.getTmct0desgloseclitits().clear();

        rts = new ArrayList<Tmct0DesgloseCamaraEnvioRt>(dc.getTmct0DesgloseCamaraEnvioRt());
        dc.getTmct0DesgloseCamaraEnvioRt().clear();

        movs = new ArrayList<Tmct0movimientoecc>(dc.getTmct0movimientos());
        dc.getTmct0movimientos().clear();
        if (dc.getTmct0infocompensacion() != null) {
          dc.setTmct0infocompensacion(icDao.save(dc.getTmct0infocompensacion()));
          if (tmct0alc.getIdinfocomp() == null) {
            tmct0alc.setIdinfocomp(dc.getTmct0infocompensacion().getIdinfocomp());
          }
        }

        dc = dcBo.save(dc);
        dctBo.save(dcts);
        for (Tmct0DesgloseCamaraEnvioRt rt : rts) {
          if (rt.getTmct0desgloseclitit() != null) {
            rt.setNudesglose(rt.getTmct0desgloseclitit().getNudesglose());
          }
        }
        dcRtDao.save(rts);

        for (Tmct0movimientoecc mov : movs) {
          movsn = new ArrayList<Tmct0movimientooperacionnuevaecc>(mov.getTmct0movimientooperacionnuevaeccs());
          mov.getTmct0movimientooperacionnuevaeccs().clear();
          mov = movBo.save(mov);
          for (Tmct0movimientooperacionnuevaecc movn : movsn) {
            if (movn.getTmct0desgloseclitit() != null) {
              movn.setNudesglose(movn.getTmct0desgloseclitit().getNudesglose());
            }
          }// fin for movn
          movNBo.save(movsn);

        }

      }

    }
  }

  /**
   * @param alcOriginal
   * @param alcsSustitutos
   */
  private void crearLogsProceso(boolean fromAlcOriginal, Tmct0alc alcOriginal, boolean fromAlcSustitutos,
      List<Tmct0alc> alcsSustitutos) {
    String msg;
    if (fromAlcOriginal) {
      msg = MessageFormat
          .format(
              "CUADRE_ECC:SEPARAR_MOVS_BY_IC: BAJA ALC:{0} IMP:{5}, ESTS asig:{1} tit:{2} cont:{3} clearing:{4}, IMP_NET:{6} COMM_LIQ:{7}",
              alcOriginal.getId().getNucnfliq(), alcOriginal.getCdestadoasig(),
              alcOriginal.getEstadotit() != null ? alcOriginal.getEstadotit().getIdestado() : 0, alcOriginal
                  .getEstadocont() != null ? alcOriginal.getEstadocont().getIdestado() : 0, alcOriginal
                  .getEstadoentrec() != null ? alcOriginal.getEstadoentrec().getIdestado() : 0, alcOriginal
                  .getImefeagr(), alcOriginal.getImnfiliq(), alcOriginal.getImfinsvb());
      logBo.insertarRegistro(alcOriginal.getId().getNuorden(), alcOriginal.getId().getNbooking(), alcOriginal.getId()
          .getNucnfclt(), msg, "", AUDIT_USER);
    }
    if (fromAlcSustitutos) {
      for (Tmct0alc tmct0alc : alcsSustitutos) {
        msg = MessageFormat
            .format(
                "CUADRE_ECC:SEPARAR_MOVS_BY_IC: ALTA ALC:{0} IMP:{5}, ESTS asig:{1} tit:{2} cont:{3} clearing:{4}, IMP_NET:{6} COMM_LIQ:{7}",
                tmct0alc.getId().getNucnfliq(), tmct0alc.getCdestadoasig(), tmct0alc.getEstadotit() != null ? tmct0alc
                    .getEstadotit().getIdestado() : 0, tmct0alc.getEstadocont() != null ? tmct0alc.getEstadocont()
                    .getIdestado() : 0, tmct0alc.getEstadoentrec() != null ? tmct0alc.getEstadoentrec().getIdestado()
                    : 0, tmct0alc.getImefeagr(), tmct0alc.getImnfiliq(), tmct0alc.getImfinsvb());
        logBo.insertarRegistro(tmct0alc.getId().getNuorden(), tmct0alc.getId().getNbooking(), tmct0alc.getId()
            .getNucnfclt(), msg, "", AUDIT_USER);
      }
    }
  }

  /**
   * 
   */
  private Tmct0movimientoecc crearMovimientosDesglosesSinMovimientos(Tmct0desglosecamara dc, Tmct0desgloseclitit dct) {
    LOG.trace("[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] inicio. {}", dct);
    CuadreEccEjecucion eje = null;
    String tpopebolSinEcc = cfgBo.getTpopebolsSinEcc();
    if (tpopebolSinEcc != null && tpopebolSinEcc.contains(StringUtils.trim(dct.getTpopebol()))) {
      LOG.debug("[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] buscar ejecucion SIN ECC por informacion mercado...");
      eje = cuadreEccEjecucioBo
          .findByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNeg(
              dct.getFeejecuc(), dct.getCdcamara(), dct.getCdisin(), dct.getCdmiembromkt(), dct.getCdsentido(),
              dct.getCdsegmento(), dct.getTpopebol(), dct.getNuordenmkt(), dct.getNurefexemkt(),
              dct.getCdPlataformaNegociacion());

    }
    else if (!StringUtils.isBlank(dct.getCdoperacionecc())) {
      LOG.trace("[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] buscando eje por ecc: {}",
          dct.getCdoperacionecc());
      eje = cuadreEccEjecucioBo.findByNumOpAndIdEcc(dct.getCdoperacionecc(), dct.getCdcamara());
    }
    else if (StringUtils.isBlank(dct.getCdoperacionecc()) && !StringUtils.isBlank(dct.getNumeroOperacionDCV())) {
      LOG.trace("[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] buscando eje por dcv: {}",
          dct.getNumeroOperacionDCV());
      eje = cuadreEccEjecucioBo.findByIdEccAndNumOpDcvAndFechaNegAndSentido(dct.getCdcamara(),
          dct.getNumeroOperacionDCV(), dct.getFeejecuc(), dct.getCdsentido());
    }
    else {
      LOG.warn(
          "[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] INCICENDIA no es posible buscar la ejecucion para este proceso. {}.",
          dct);
    }

    Tmct0movimientoecc movCreado = null;
    Tmct0movimientooperacionnuevaecc movnCreado;
    if (eje != null) {
      movCreado = new Tmct0movimientoecc();

      movCreado.setTmct0desglosecamara(dc);
      movCreado.setAuditFechaCambio(new Date());
      movCreado.setAuditUser(AUDIT_USER);
      movCreado.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
      movCreado.setFechaLiquidacion(dc.getFeliquidacion());
      movCreado.setImefectivo(dct.getImefectivo());
      movCreado.setImtitulos(dct.getImtitulos());
      movCreado.setIsin(dct.getCdisin());
      movCreado.setSentido(dct.getCdsentido());

      movCreado.setCuentaCompensacionDestino(eje.getCuentaCompensacionMiembro());
      movCreado.setMiembroCompensadorDestino(eje.getMiembroPropietarioCuenta());
      movCreado.setCodigoReferenciaAsignacion(eje.getRefAsignacionExt());
      dct.setCuentaCompensacionDestino(eje.getCuentaCompensacionMiembro());
      dct.setMiembroCompensadorDestino(eje.getMiembroPropietarioCuenta());
      dct.setCodigoReferenciaAsignacion(eje.getRefAsignacionExt());
      if (StringUtils.isBlank(dct.getNumeroOperacionDCV())) {
        dct.setNumeroOperacionDCV(eje.getNumOpDcv());
      }
      dct = dctBo.save(dct);
      movCreado = movBo.save(movCreado);
      movnCreado = new Tmct0movimientooperacionnuevaecc();
      movnCreado.setTmct0movimientoecc(movCreado);
      movnCreado.setTmct0desgloseclitit(dct);
      movnCreado.setNudesglose(dct.getNudesglose());
      movnCreado.setCdmiembromkt(dct.getCdmiembromkt());
      movnCreado.setCdoperacionecc(dct.getCdoperacionecc());
      movnCreado.setNuoperacionprev(dct.getNuoperacionprev());
      movnCreado.setNuoperacioninic(dct.getNuoperacioninic());
      movnCreado.setImprecio(dct.getImprecio());
      movnCreado.setImefectivo(dct.getImefectivo());
      movnCreado.setImtitulos(dct.getImtitulos());
      movnCreado.setAuditFechaCambio(new Date());
      movnCreado.setAuditUser(AUDIT_USER);
      movnCreado = movNBo.save(movnCreado);
      movCreado.getTmct0movimientooperacionnuevaeccs().add(movnCreado);

    }
    else {
      LOG.warn(
          "[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] INCICENDIA no hemos encontrado la ejecucion. {}.",
          dct);
    }
    LOG.trace("[CuadreEccSepararMovimientosBo :: crearMovimientosDesglosesSinMovimientos] final");
    return movCreado;
  }
}
