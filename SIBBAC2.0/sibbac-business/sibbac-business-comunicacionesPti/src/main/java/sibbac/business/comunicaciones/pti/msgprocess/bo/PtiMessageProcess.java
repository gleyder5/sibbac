package sibbac.business.comunicaciones.pti.msgprocess.bo;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageVersion;

public abstract class PtiMessageProcess<T extends PTIMessage> {

  public abstract void process(PTIMessageVersion version, T ptiMessage, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException;

}
