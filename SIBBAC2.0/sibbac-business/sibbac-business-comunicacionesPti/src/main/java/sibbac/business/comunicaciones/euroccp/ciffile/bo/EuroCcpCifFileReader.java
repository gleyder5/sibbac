package sibbac.business.comunicaciones.euroccp.ciffile.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpMessageType;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpAggregatesNetSettlementsFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpCashPositionFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpClearedGrossTradesSpainFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpProcessedMoneyMovementFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpProcessedSettledMovementFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpProcessedUnsettledMovementFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpSettledPositionFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpSettlementInstructionFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpSpanishSettlementInstructionFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpTrailerFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.mapper.EuroCcpUnsettledPositionFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpTrailer;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Service
public class EuroCcpCifFileReader {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpCifFileReader.class);

  @Value("${sibbac.euroccp.file.reader.transaction.size:1}")
  private int transactionSize;

  @Value("${sibbac.euroccp.file.reader.thread.pool.size:1}")
  private int threadSize;

  @Autowired
  private EuroCcpCifFileReaderSubList euroCcpFileReaderSubList;

  public EuroCcpCifFileReader() {
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void procesarFicherosCif(Path rutaFicheros, Path rutaTratados, String filePattern, Charset charset,
      boolean hasTestigo) throws SIBBACBusinessException {

    List<Path> ficheros = new ArrayList<Path>();
    try {
      try (DirectoryStream<Path> dir = Files.newDirectoryStream(rutaFicheros, filePattern);) {
        for (Path ficheroCif : dir) {
          ficheros.add(ficheroCif);
        }

      }
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format(
          "No se ha podido buscar los ficheros en la ruta %s con pattern %s %s ", rutaFicheros.toString(), filePattern,
          e.getMessage()), e);
    }

    if (!ficheros.isEmpty()) {
      LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper = getNewLinMapper();
      Collections.sort(ficheros, new Comparator<Path>() {

        @Override
        public int compare(Path arg0, Path arg1) {
          return arg0.toString().compareTo(arg1.toString());
        }
      });

      for (Path ficheroCif : ficheros) {
        this.procesarCifFile(ficheroCif, rutaTratados, hasTestigo, charset, lineMapper);
      }
      ficheros.clear();
    }

  }

  private void procesarCifFile(Path file, Path pathTratado, boolean hasTestigo, Charset charset,
      LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper) throws SIBBACBusinessException {

    LOG.info("[procesarCifFile] procesando fichero: {}", file.toString());
    String originalName = file.getFileName().toString();
    String basename = FilenameUtils.getBaseName(originalName);
    String extension = FilenameUtils.getExtension(originalName);

    if (hasTestigo) {
      this.checkTestigo(file);
    }

    String concurrentFileName = FormatDataUtils.convertDateToConcurrentFileName(new Date());

    Path ficheroProcesandose = Paths.get(file.getParent().toString(),
        String.format("%s_%s.procesandose", basename, concurrentFileName));

    Path ficheroTratado = Paths.get(pathTratado.toString(),
        String.format("%s_%s.%s", basename, concurrentFileName, extension));

    this.move(file, ficheroProcesandose);

    try {
      if (originalName.toUpperCase().endsWith(".ZIP")) {
        this.procesarZip(ficheroProcesandose, charset, lineMapper);
      }
      else if (originalName.toUpperCase().endsWith(".GZ")) {
        this.procesarGz(ficheroProcesandose, charset, lineMapper);
      }
      else {
        this.procesarTxt(ficheroProcesandose, charset, lineMapper);
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("No se ha podido procesar el fichero %s por %s ",
          file.toString(), e.getMessage()), e);
    }

    this.move(ficheroProcesandose, ficheroTratado);

    if (hasTestigo) {
      this.deleteTestigo(file);
    }
  }

  private void checkTestigo(Path file) throws SIBBACBusinessException {
    String originalName = file.getFileName().toString();
    String basename = FilenameUtils.getBaseName(originalName);

    Path testigo = Paths.get(file.getParent().toString(), String.format("%s.TST", basename));
    if (!Files.exists(testigo)) {
      throw new SIBBACBusinessException(MessageFormat.format("No encontrado fichero testigo para fichero: {0}",
          file.toString()));
    }
  }

  private void deleteTestigo(Path file) throws SIBBACBusinessException {
    String originalName = file.getFileName().toString();
    String basename = FilenameUtils.getBaseName(originalName);

    Path testigo = Paths.get(file.getParent().toString(), String.format("%s.TST", basename));
    try {
      Files.deleteIfExists(testigo);
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format(
          "No se ha podido borrar el fichero testigo: %s para el fichero: %s  %s", testigo.toString(), file.toString(),
          e.getMessage()), e);
    }

  }

  private void move(Path origen, Path destino) throws SIBBACBusinessException {
    LOG.info("[move] moviendo fichero de {} a {}", origen.toString(), destino.toString());
    try {
      Files.move(origen, destino, StandardCopyOption.ATOMIC_MOVE);
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido mover el fichero: %s a %s", origen.toString(),
          destino.toString()), e);
    }
  }

  private void procesarZip(Path file, Charset charset, LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper)
      throws SIBBACBusinessException {
    ZipEntry zipEntry;

    try (ZipInputStream zipValidar = new ZipInputStream(Files.newInputStream(file, StandardOpenOption.READ), charset);
        BufferedReader brValidar = new BufferedReader(new InputStreamReader(zipValidar, charset));) {
      while ((zipEntry = zipValidar.getNextEntry()) != null) {
        try {
          this.validarCif(brValidar, lineMapper);
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(String.format("Incidencia validando el fichero %s del zip %s %s",
              zipEntry.getName(), file.toString(), e.getMessage()), e);
        }
        finally {
          zipValidar.closeEntry();
        }
      }

    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido abrir el fichero para leer: %s, %s",
          file.toString(), e.getMessage()), e);
    }

    try (ZipInputStream zipLeer = new ZipInputStream(Files.newInputStream(file, StandardOpenOption.READ), charset);
        BufferedReader brLeer = new BufferedReader(new InputStreamReader(zipLeer, charset));) {

      while ((zipEntry = zipLeer.getNextEntry()) != null) {
        try {
          this.procesarCif(brLeer, lineMapper);
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(String.format("Incidencia procesando el fichero %s del zip %s %s",
              zipEntry.getName(), file.toString(), e.getMessage()), e);
        }
        finally {
          zipLeer.closeEntry();
        }
      }

    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido abrir el fichero para leer: %s, %s",
          file.toString(), e.getMessage()), e);
    }

  }

  private void procesarGz(Path file, Charset charset, LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper)
      throws SIBBACBusinessException {
    try {
      try (BufferedReader brValidar = new BufferedReader(new InputStreamReader(new GZIPInputStream(
          Files.newInputStream(file, StandardOpenOption.READ)), charset));
          BufferedReader brLeer = new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(
              file, StandardOpenOption.READ)), charset));) {
        this.procesarCif(brValidar, brLeer, lineMapper);

      }
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido abrir el fichero para leer: %s, %s",
          file.toString(), e.getMessage()), e);
    }
  }

  private void procesarTxt(Path file, Charset charset, LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper)
      throws SIBBACBusinessException {

    try {
      try (BufferedReader brValidar = Files.newBufferedReader(file, charset);
          BufferedReader brLeer = Files.newBufferedReader(file, charset);) {
        this.procesarCif(brValidar, brLeer, lineMapper);
      }
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido abrir el fichero para leer: %s, %s",
          file.toString(), e.getMessage()), e);
    }
  }

  private void procesarCif(BufferedReader brValidar, BufferedReader brLeer,
      LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper) throws SIBBACBusinessException {
    this.validarCif(brValidar, lineMapper);
    this.procesarCif(brLeer, lineMapper);

  }

  private void validarCif(BufferedReader br, LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper)
      throws SIBBACBusinessException {
    String linea;
    EuroCcpLineaFicheroRecordBean bean;
    try {
      int lineNumber = 0;
      while ((linea = br.readLine()) != null) {
        lineNumber++;

        bean = this.parsearLineaFicheroCif(linea, lineMapper, lineNumber);

        if (StringUtils.isNotBlank(bean.getTipoRegistro())) {
          if (bean.getTipoRegistro().equals(EuroCcpMessageType.TYPE_TRAILER_910.text)) {
            EuroCcpTrailer fileTrailer = (EuroCcpTrailer) bean;
            if (lineNumber != fileTrailer.getTotalNumberRecords()) {
              throw new SIBBACBusinessException(String.format(
                  "El numero de registros del trailer %s no coincide con el del fichero %s",
                  fileTrailer.getTotalNumberRecords(), lineNumber));
            }
          }

        }
        else {
          throw new SIBBACBusinessException(String.format("El tipo del registro no puede ser null. %s", linea));
        }
      }
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido leer el fichero. %s", e.getMessage()), e);
    }
  }

  private EuroCcpLineaFicheroRecordBean parsearLineaFicheroCif(String linea,
      LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper, int lineNumber) throws SIBBACBusinessException {
    try {
      return lineMapper.mapLine(linea, lineNumber);
    }
    catch (Exception e) {
      LOG.error("[parsearLineaFicheroCif] Error de mapero", e);
      throw new SIBBACBusinessException(String.format("No se ha podido mapear la linea pos %s  %s por %s", lineNumber,
          linea, e.getMessage()), e);
    }
  }

  private void procesarCif(BufferedReader br, LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper)
      throws SIBBACBusinessException {
    String linea;
    EuroCcpLineaFicheroRecordBean bean;
    List<EuroCcpLineaFicheroRecordBean> listBeans = new ArrayList<EuroCcpLineaFicheroRecordBean>();
    List<Future<Void>> procesos = new ArrayList<Future<Void>>();
    ExecutorService executor = Executors.newFixedThreadPool(threadSize);
    try {
      int lineNumber = 0;
      int posFirstBean = 0;
      while ((linea = br.readLine()) != null) {
        lineNumber++;
        bean = this.parsearLineaFicheroCif(linea, lineMapper, lineNumber);

        if (StringUtils.isNotBlank(bean.getTipoRegistro())) {
          listBeans.add(bean);
        }
        else {
          throw new SIBBACBusinessException(String.format("El tipo del registro no puede ser null. %s", linea));
        }

        if (listBeans.size() >= getTransactionSize()) {
          procesos.add(executor.submit(this.getNewCallableForProcessFile(listBeans, posFirstBean)));
          listBeans = new ArrayList<EuroCcpLineaFicheroRecordBean>();
          posFirstBean = lineNumber + 1;
        }
      }

      if (CollectionUtils.isNotEmpty(listBeans)) {
        procesos.add(executor.submit(this.getNewCallableForProcessFile(listBeans, posFirstBean)));
      }

    }
    catch (IOException e) {
      executor.shutdownNow();
      throw new SIBBACBusinessException(String.format("No se ha podido leer el fichero. %s", e.getMessage()), e);
    }
    catch (SIBBACBusinessException e) {
      executor.shutdownNow();
      throw e;
    }
    finally {
      if (!executor.isShutdown()) {
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        try {
          // esperamos = procesos * 5 segundos * (transacctionsize / threadSize)
          // + 30 minutos

          executor.awaitTermination((long) (procesos.size() * 5 * (transactionSize / threadSize)) + 30,
              TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
          throw new SIBBACBusinessException("INCIDECIA-La ejecucion se ha interrumpido", e);
        }
      }
    }

    for (Future<Void> future : procesos) {
      try {
        future.get(2, TimeUnit.SECONDS);
      }
      catch (TimeoutException e) {
        future.cancel(true);
        LOG.warn("[procesarCif] Se ha cancelado el proceso por {}", e.getMessage(), e);
        throw new SIBBACBusinessException("INCIDECIA-La ejecucion ha dado un error de timeout.", e);
      }
      catch (InterruptedException | ExecutionException e) {
        LOG.warn(e.getMessage(), e);
        throw new SIBBACBusinessException("INCIDECIA-La ejecucion se ha interrumpido", e);
      }

    }
  }

  private Callable<Void> getNewCallableForProcessFile(final List<EuroCcpLineaFicheroRecordBean> beans, final int order) {
    Callable<Void> c = new Callable<Void>() {

      @Override
      public Void call() throws SIBBACBusinessException {
        euroCcpFileReaderSubList.grabarRegistros(beans, order);
        return null;
      }
    };
    return c;
  }

  private LineMapper<EuroCcpLineaFicheroRecordBean> getNewLinMapper() {
    LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper = initLineMapper();
    this.initFieldSetMappers(lineMapper);
    this.initLineTokenizers(lineMapper);
    return lineMapper;
  }

  private LineMapper<EuroCcpLineaFicheroRecordBean> initLineMapper() {
    return new PatternMatchingCompositeLineMapper<EuroCcpLineaFicheroRecordBean>();
  }

  private void initLineTokenizers(LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper) {
    Map<String, LineTokenizer> lineTokenizers = new HashMap<String, LineTokenizer>();
    EuroCcpProcessedUnsettledMovementFieldSetMapper euroCcpProcessedunsettledMovementFieldsetMapper = new EuroCcpProcessedUnsettledMovementFieldSetMapper();
    final FixedLengthTokenizer euroCcpProcessedUnsettledMovementLineTokenizer = new FixedLengthTokenizer();
    euroCcpProcessedUnsettledMovementLineTokenizer.setNames(euroCcpProcessedunsettledMovementFieldsetMapper
        .getFieldNames());
    euroCcpProcessedUnsettledMovementLineTokenizer.setColumns(euroCcpProcessedunsettledMovementFieldsetMapper
        .getColumnsRanges());
    euroCcpProcessedUnsettledMovementLineTokenizer.setStrict(false);

    lineTokenizers.put(EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_409.text + "*",
        euroCcpProcessedUnsettledMovementLineTokenizer);
    lineTokenizers.put(EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_410.text + "*",
        euroCcpProcessedUnsettledMovementLineTokenizer);

    EuroCcpProcessedSettledMovementFieldSetMapper euroCcpProcessedSettledMovementFieldsetMapper = new EuroCcpProcessedSettledMovementFieldSetMapper();
    final FixedLengthTokenizer euroCcpProcessedSettledMovementLineTokenizer = new FixedLengthTokenizer();
    euroCcpProcessedSettledMovementLineTokenizer
        .setNames(euroCcpProcessedSettledMovementFieldsetMapper.getFieldNames());
    euroCcpProcessedSettledMovementLineTokenizer.setColumns(euroCcpProcessedSettledMovementFieldsetMapper
        .getColumnsRanges());
    euroCcpProcessedSettledMovementLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_PROCESSED_SETTLED_MOVEMENT_411.text + "*",
        euroCcpProcessedSettledMovementLineTokenizer);

    EuroCcpClearedGrossTradesSpainFieldSetMapper euroCcpClearedGrossTradesSpainFieldSetMapper = new EuroCcpClearedGrossTradesSpainFieldSetMapper();
    final FixedLengthTokenizer euroCcpClearedGrossTradesSpainTokenizer = new FixedLengthTokenizer();
    euroCcpClearedGrossTradesSpainTokenizer.setNames(euroCcpClearedGrossTradesSpainFieldSetMapper.getFieldNames());
    euroCcpClearedGrossTradesSpainTokenizer.setColumns(euroCcpClearedGrossTradesSpainFieldSetMapper.getColumnsRanges());
    euroCcpClearedGrossTradesSpainTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_CLEARED_GROSS_TRADES_SPAIN_412.text + "*",
        euroCcpClearedGrossTradesSpainTokenizer);

    EuroCcpAggregatesNetSettlementsFieldSetMapper euroCcpAggregatesNetSettlementsFieldsetMapper = new EuroCcpAggregatesNetSettlementsFieldSetMapper();
    final FixedLengthTokenizer euroCcpAggregatesNetSettlementsLineTokenizer = new FixedLengthTokenizer();
    euroCcpAggregatesNetSettlementsLineTokenizer
        .setNames(euroCcpAggregatesNetSettlementsFieldsetMapper.getFieldNames());
    euroCcpAggregatesNetSettlementsLineTokenizer.setColumns(euroCcpAggregatesNetSettlementsFieldsetMapper
        .getColumnsRanges());
    euroCcpAggregatesNetSettlementsLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_AGGREGATES_NET_SETTLEMENTS_415.text + "*",
        euroCcpAggregatesNetSettlementsLineTokenizer);

    EuroCcpUnsettledPositionFieldSetMapper euroCcpUnsettledPositionFieldSetMapper = new EuroCcpUnsettledPositionFieldSetMapper();
    final FixedLengthTokenizer euroCcpUnsettledPositionLineTokenizer = new FixedLengthTokenizer();
    euroCcpUnsettledPositionLineTokenizer.setNames(euroCcpUnsettledPositionFieldSetMapper.getFieldNames());
    euroCcpUnsettledPositionLineTokenizer.setColumns(euroCcpUnsettledPositionFieldSetMapper.getColumnsRanges());
    euroCcpUnsettledPositionLineTokenizer.setStrict(false);
    lineTokenizers
        .put(EuroCcpMessageType.TYPE_UNSETTLED_POSITION_420.text + "*", euroCcpUnsettledPositionLineTokenizer);

    EuroCcpSettledPositionFieldSetMapper euroCcpSettledPositionFieldSetMapper = new EuroCcpSettledPositionFieldSetMapper();
    final FixedLengthTokenizer euroCcpSettledPositionLineTokenizer = new FixedLengthTokenizer();
    euroCcpSettledPositionLineTokenizer.setNames(euroCcpSettledPositionFieldSetMapper.getFieldNames());
    euroCcpSettledPositionLineTokenizer.setColumns(euroCcpSettledPositionFieldSetMapper.getColumnsRanges());
    euroCcpSettledPositionLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_SETTLED_POSITION_421.text + "*", euroCcpSettledPositionLineTokenizer);

    EuroCcpSettlementInstructionFieldSetMapper euroCcpSettlementInstructionFieldSetMapper = new EuroCcpSettlementInstructionFieldSetMapper();
    final FixedLengthTokenizer euroCcpSettlementInstructionLineTokenizer = new FixedLengthTokenizer();
    euroCcpSettlementInstructionLineTokenizer.setNames(euroCcpSettlementInstructionFieldSetMapper.getFieldNames());
    euroCcpSettlementInstructionLineTokenizer.setColumns(euroCcpSettlementInstructionFieldSetMapper.getColumnsRanges());
    euroCcpSettlementInstructionLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_SETTLEMENT_INSTRUCTION_450.text + "*",
        euroCcpSettlementInstructionLineTokenizer);

    EuroCcpSpanishSettlementInstructionFieldSetMapper euroCcpSpanishSettlementInstructionFieldSetMapper = new EuroCcpSpanishSettlementInstructionFieldSetMapper();
    final FixedLengthTokenizer euroCcpSpanishSettlementInstructionLineTokenizer = new FixedLengthTokenizer();
    euroCcpSpanishSettlementInstructionLineTokenizer.setNames(euroCcpSpanishSettlementInstructionFieldSetMapper
        .getFieldNames());
    euroCcpSpanishSettlementInstructionLineTokenizer.setColumns(euroCcpSpanishSettlementInstructionFieldSetMapper
        .getColumnsRanges());
    euroCcpSpanishSettlementInstructionLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_SPANISH_SETTLEMENT_INSTRUCTION_452.text + "*",
        euroCcpSpanishSettlementInstructionLineTokenizer);

    EuroCcpProcessedMoneyMovementFieldSetMapper euroCcpProcessedMoneyMovementFieldSetMapper = new EuroCcpProcessedMoneyMovementFieldSetMapper();
    final FixedLengthTokenizer euroCcpProcessedMoneyMovementLineTokenizer = new FixedLengthTokenizer();
    euroCcpProcessedMoneyMovementLineTokenizer.setNames(euroCcpProcessedMoneyMovementFieldSetMapper.getFieldNames());
    euroCcpProcessedMoneyMovementLineTokenizer.setColumns(euroCcpProcessedMoneyMovementFieldSetMapper
        .getColumnsRanges());
    euroCcpProcessedMoneyMovementLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_PROCESSED_MONEY_MOVEMENT_600.text + "*",
        euroCcpProcessedMoneyMovementLineTokenizer);

    EuroCcpCashPositionFieldSetMapper euroCcpCashPositionFieldSetMapper = new EuroCcpCashPositionFieldSetMapper();
    final FixedLengthTokenizer euroCcpCashPositionLineTokenizer = new FixedLengthTokenizer();
    euroCcpCashPositionLineTokenizer.setNames(euroCcpCashPositionFieldSetMapper.getFieldNames());
    euroCcpCashPositionLineTokenizer.setColumns(euroCcpCashPositionFieldSetMapper.getColumnsRanges());
    euroCcpCashPositionLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_CASH_POSITION_610.text + "*", euroCcpCashPositionLineTokenizer);

    EuroCcpTrailerFieldSetMapper euroCcpTrailerFieldSetMapper = new EuroCcpTrailerFieldSetMapper();
    final FixedLengthTokenizer euroCcpTrailerFieldLineTokenizer = new FixedLengthTokenizer();
    euroCcpTrailerFieldLineTokenizer.setNames(euroCcpTrailerFieldSetMapper.getFieldNames());
    euroCcpTrailerFieldLineTokenizer.setColumns(euroCcpTrailerFieldSetMapper.getColumnsRanges());
    euroCcpTrailerFieldLineTokenizer.setStrict(false);
    lineTokenizers.put(EuroCcpMessageType.TYPE_TRAILER_910.text + "*", euroCcpTrailerFieldLineTokenizer);
    ((PatternMatchingCompositeLineMapper<EuroCcpLineaFicheroRecordBean>) lineMapper).setTokenizers(lineTokenizers);
  }

  protected void initFieldSetMappers(LineMapper<EuroCcpLineaFicheroRecordBean> lineMapper) {
    Map<String, FieldSetMapper<EuroCcpLineaFicheroRecordBean>> fieldSetMappers = new HashMap<String, FieldSetMapper<EuroCcpLineaFicheroRecordBean>>();
    fieldSetMappers.put(EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_409.text + "*",
        new EuroCcpProcessedUnsettledMovementFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_410.text + "*",
        new EuroCcpProcessedUnsettledMovementFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_PROCESSED_SETTLED_MOVEMENT_411.text + "*",
        new EuroCcpProcessedSettledMovementFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_CLEARED_GROSS_TRADES_SPAIN_412.text + "*",
        new EuroCcpClearedGrossTradesSpainFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_AGGREGATES_NET_SETTLEMENTS_415.text + "*",
        new EuroCcpAggregatesNetSettlementsFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_UNSETTLED_POSITION_420.text + "*",
        new EuroCcpUnsettledPositionFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_SETTLED_POSITION_421.text + "*",
        new EuroCcpSettledPositionFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_SETTLEMENT_INSTRUCTION_450.text + "*",
        new EuroCcpSettlementInstructionFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_SPANISH_SETTLEMENT_INSTRUCTION_452.text + "*",
        new EuroCcpSpanishSettlementInstructionFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_PROCESSED_MONEY_MOVEMENT_600.text + "*",
        new EuroCcpProcessedMoneyMovementFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_CASH_POSITION_610.text + "*", new EuroCcpCashPositionFieldSetMapper());
    fieldSetMappers.put(EuroCcpMessageType.TYPE_TRAILER_910.text + "*", new EuroCcpTrailerFieldSetMapper());
    ((PatternMatchingCompositeLineMapper<EuroCcpLineaFicheroRecordBean>) lineMapper)
        .setFieldSetMappers(fieldSetMappers);
  }

  public int getThreadSize() {
    return threadSize;
  }

  public int getTransactionSize() {
    return transactionSize;
  }

}
