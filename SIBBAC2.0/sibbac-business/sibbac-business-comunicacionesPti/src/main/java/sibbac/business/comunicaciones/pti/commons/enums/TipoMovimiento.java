package sibbac.business.comunicaciones.pti.commons.enums;

public enum TipoMovimiento {

  TAKE_UP("16"),
  GIVE_UP("15");

  public String text;

  private TipoMovimiento(String text) {
    this.text = text;
  }

}
