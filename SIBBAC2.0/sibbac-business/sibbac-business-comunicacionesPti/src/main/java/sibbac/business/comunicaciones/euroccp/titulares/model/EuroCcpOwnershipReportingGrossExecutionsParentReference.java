package sibbac.business.comunicaciones.euroccp.titulares.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import sibbac.database.DBConstants.EURO_CCP;
import sibbac.database.model.ATable;

@Table(name = EURO_CCP.OWNERSHIP_REPORTING_GROSS_EXECUTION_PARENT_REFERENCE)
@Entity
public class EuroCcpOwnershipReportingGrossExecutionsParentReference extends
                                                                    ATable<EuroCcpOwnershipReportingGrossExecutionsParentReference> {

  /**
   * 
   */
  private static final long serialVersionUID = -5599354852621418791L;

  @OneToOne
  @JoinColumn(name = "ID_PARENT_REFERENCE", nullable = false)
  private EuroCcpOwnershipReportingGrossExecutions parentReference;

  @OneToOne
  @JoinColumn(name = "ID_CHILD_REFERENCE", nullable = false)
  private EuroCcpOwnershipReportingGrossExecutions childReference;

  @Column(name = "NUMBER_SHARES", length = 10, scale = 0, nullable = false)
  private BigDecimal numberShares;

  public EuroCcpOwnershipReportingGrossExecutions getParentReference() {
    return parentReference;
  }

  public EuroCcpOwnershipReportingGrossExecutions getChildReference() {
    return childReference;
  }

  public BigDecimal getNumberShares() {
    return numberShares;
  }

  public void setParentReference(EuroCcpOwnershipReportingGrossExecutions parentReference) {
    this.parentReference = parentReference;
  }

  public void setChildReference(EuroCcpOwnershipReportingGrossExecutions childReference) {
    this.childReference = childReference;
  }

  public void setNumberShares(BigDecimal numberShares) {
    this.numberShares = numberShares;
  }

}
