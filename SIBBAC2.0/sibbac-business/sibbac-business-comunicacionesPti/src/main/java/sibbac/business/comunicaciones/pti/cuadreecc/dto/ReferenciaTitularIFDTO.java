package sibbac.business.comunicaciones.pti.cuadreecc.dto;


public class ReferenciaTitularIFDTO {

  private final String referenciaTitular;
  private final String referenciaAdicional;

  public ReferenciaTitularIFDTO(String referenciaTitular, String referenciaAdicional) {
    super();
    this.referenciaTitular = referenciaTitular;
    this.referenciaAdicional = referenciaAdicional;
  }

  public String getReferenciaTitular() {
    return referenciaTitular;
  }

  public String getReferenciaAdicional() {
    return referenciaAdicional;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((referenciaAdicional == null) ? 0 : referenciaAdicional.hashCode());
    result = prime * result + ((referenciaTitular == null) ? 0 : referenciaTitular.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ReferenciaTitularIFDTO other = (ReferenciaTitularIFDTO) obj;
    if (referenciaAdicional == null) {
      if (other.referenciaAdicional != null)
        return false;
    } else if (!referenciaAdicional.equals(other.referenciaAdicional))
      return false;
    if (referenciaTitular == null) {
      if (other.referenciaTitular != null)
        return false;
    } else if (!referenciaTitular.equals(other.referenciaTitular))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "ReferenciaTitularIFDTO [referenciaTitular=" + referenciaTitular + ", referenciaAdicional="
           + referenciaAdicional + "]";
  }
  
  

}
