package sibbac.business.comunicaciones.ordenes.exceptions;

public class XmlOrdenInternalReceptionException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -1174868203331293705L;

  public XmlOrdenInternalReceptionException() {
  }

  public XmlOrdenInternalReceptionException(String arg0) {
    super(arg0);
  }

  public XmlOrdenInternalReceptionException(Throwable arg0) {
    super(arg0);
  }

  public XmlOrdenInternalReceptionException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public XmlOrdenInternalReceptionException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

}
