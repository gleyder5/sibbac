package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccValidacionTitulosReferenciasFicheroEccDbReader")
public class CuadreEccValidacionTitulosReferenciasFicheroEccDbReader extends
    WrapperMultiThreadedDbReader<CuadreEccIdDTO> {

  protected static final Logger LOG = LoggerFactory
      .getLogger(CuadreEccValidacionTitulosReferenciasFicheroEccDbReader.class);

  @Value("${sibbac.cuadreecc.max.reg.process.in.execution:100000}")
  private int maxRegToProcess;

  @Value("${sibbac.cuadreecc.validacion.titulos.referencia.fichero.ecc.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadreecc.validacion.titulos.referencia.fichero.ecc.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  @Qualifier(value = "cuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean")
  @Autowired
  private CuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean cuadreEccRevTitulosRunnBean;

  public CuadreEccValidacionTitulosReferenciasFicheroEccDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<CuadreEccIdDTO> getBeanToProcessBlock() {
    return cuadreEccRevTitulosRunnBean;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.info("[CuadreEccValidacionTitulosReferenciasFicheroEccDbReader :: preExecuteTask] inicio");
    LOG.info("[CuadreEccValidacionTitulosReferenciasFicheroEccDbReader :: preExecuteTask] buscando ejecuciones...");
    List<CuadreEccIdDTO> listBean = cuadreEccEjecucionBo
        .findIdentificacionOperacionCuadreEccDTOForValidacionTitulosReferenciasFicheroEcc(maxRegToProcess);
    LOG.info(
        "[CuadreEccValidacionTitulosReferenciasFicheroEccDbReader :: preExecuteTask] encontrados {} ejecuciones que revisar.",
        listBean.size());
    beanIterator = listBean.iterator();
    LOG.info("[CuadreEccValidacionTitulosReferenciasFicheroEccDbReader :: preExecuteTask] fin");
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
