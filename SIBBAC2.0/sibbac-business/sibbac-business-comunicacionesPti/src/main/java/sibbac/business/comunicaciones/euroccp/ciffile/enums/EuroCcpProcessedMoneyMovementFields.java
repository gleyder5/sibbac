package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpProcessedMoneyMovementFields implements WrapperFileReaderFieldEnumInterface {

  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  SUBACCOUNT_NUMBER("subaccountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  OPPOSITE_PARTY_CODE("oppositePartyCode", 6, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_DATE("transactionDate", 8, 0, WrapperFileReaderFieldType.DATE),
  VALUE_DATE("valueDate", 8, 0, WrapperFileReaderFieldType.DATE),
  JOURNAL_ENTRY_AMOUNT("journalEntryAmount", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  JOURNAL_ENTRY_AMOUNT_DC("journalEntryAmountDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  JOURNAL_ACCOUNT_CODE("journalAccountCode", 4, 0, WrapperFileReaderFieldType.NUMERIC),
  GROSS_POSITION_INDICATOR("grossPositionIndicator", 1, 0, WrapperFileReaderFieldType.CHAR),
  CASH_BALANCE_DESCRIPTION("cashBalanceDescription", 24, 0, WrapperFileReaderFieldType.STRING),
  CASH_BALANCE_REFERENCE("cashBalanceReference", 9, 0, WrapperFileReaderFieldType.STRING),
  FILLER("filler", 373, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpProcessedMoneyMovementFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
