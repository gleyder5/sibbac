package sibbac.business.comunicaciones.euroccp.common;

import java.util.Map;

import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.batch.support.PatternMatcher;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

public class RegexPatternMatchingCompositeLineMapper<T> implements LineMapper<T>, InitializingBean {

  private RegexPatternMatchingCompositeLineTokenizer tokenizer = new RegexPatternMatchingCompositeLineTokenizer();

  private PatternMatcher<FieldSetMapper<T>> patternMatcher;

  /*
   * (non-Javadoc)
   * @see org.springframework.batch.item.file.mapping.LineMapper#mapLine(java.lang .String, int)
   */
  @Override
  public T mapLine(String line, int lineNumber) throws Exception {
    return patternMatcher.match(line).mapFieldSet(this.tokenizer.tokenize(line));
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
   */
  @Override
  public void afterPropertiesSet() throws Exception {
    this.tokenizer.afterPropertiesSet();
    Assert.isTrue(this.patternMatcher != null, "The 'fieldSetMappers' property must be non-empty");
  }

  public void setTokenizers(Map<String, LineTokenizer> tokenizers) {
    this.tokenizer.setTokenizers(tokenizers);
  }

  public void setFieldSetMappers(Map<String, FieldSetMapper<T>> fieldSetMappers) {
    Assert.isTrue(!fieldSetMappers.isEmpty(), "The 'fieldSetMappers' property must be non-empty");
    this.patternMatcher =   new RegexPatternMatcher<FieldSetMapper<T>>(fieldSetMappers); 
  }

}
