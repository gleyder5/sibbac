package sibbac.business.comunicaciones.page.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.commons.enums.TipoMovimiento;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

@Component("MovimientosSalidaPdteAceptarTraspaso")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MovimientosSalidaPdteAceptarTraspaso extends AbstractDynamicPageQuery {


  private static final String NBOOKING = "B.NBOOKING";
  private static final String REF_CLIENTE = "A.REF_CLIENTE";
  private static final String FECHA_CONTRATACION = "B.FETRADE";
  private static final String FECHA_LIQUIDACION = "B.FEVALOR";
  private static final String ALIAS = "B.CDALIAS";
  private static final String ISIN = "B.CDISIN";
  private static final String COMPRA_VENTA = "B.CDTPOPER";

  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("N. Booking", "NBOOKING"),
      new DynamicColumn("N. Desg. Cliente", "NUCNFCLT"), new DynamicColumn("N. Desg. Liquidador", "NUCNFLIQ"),
      new DynamicColumn("Ref. Cliente", "REF_CLIENTE"), new DynamicColumn("Estado", "ESTADO"),
      new DynamicColumn("Alias", "ALIAS"), new DynamicColumn("Desc. Alias", "DESC_ALIAS"),
      new DynamicColumn("Cámara", "CAMARA"), new DynamicColumn("Compensador", "MIEMBRO_COMPENSADOR_DESTINO"),
      new DynamicColumn("Desc. Compensador", "DESC_MIEMBRO_COMPENSADOR_DESTINO"),
      new DynamicColumn("Ref. Give-Up", "CD_REF_ASIGNACION"),
      new DynamicColumn("F. Contratación", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("F. Liquidación", "FEVALOR", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Sentido", "SENTIDO"), new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Desc. Isin", "DESC_ISIN"),
      new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Precio", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Efectivo", "EFECTIVO", DynamicColumn.ColumnType.N4),
      new DynamicColumn("Ref.Movimiento", "CDREFMOVIMIENTOECC") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new DateDynamicFilter(FECHA_CONTRATACION, "F. Contratación"));
    res.add(new DateDynamicFilter(FECHA_LIQUIDACION, "F. Liquidación"));
    res.add(new StringDynamicFilter(COMPRA_VENTA, "Sentido"));
    res.add(new StringDynamicFilter(REF_CLIENTE, "Referencia cliente"));
    res.add(new StringDynamicFilter(NBOOKING, "Número booking"));
    res.add(new StringDynamicFilter(ALIAS, "Alias"));
    res.add(new StringDynamicFilter(ISIN, "ISIN"));
    return res;
  };

  public MovimientosSalidaPdteAceptarTraspaso() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    StringBuilder sb = new StringBuilder(
        "SELECT COALESCE(C.NBOOKING, '') NBOOKING, COALESCE(C.NUCNFCLT, '') NUCNFCLT, COALESCE(C.NUCNFLIQ, '') NUCNFLIQ, COALESCE(A.REF_CLIENTE, '') REF_CLIENTE, ");
    sb.append(" COALESCE(EST.NOMBRE, '') ESTADO, COALESCE(B.CDALIAS, '') ALIAS, COALESCE(B.DESCRALI, '') DESC_ALIAS,")
        .append(
            " MF.CDCAMARA CAMARA, COALESCE(MF.MIEMBRO_COMPENSADOR_DESTINO, '') MIEMBRO_COMPENSADOR_DESTINO, COALESCE(MORI.NB_DESCRIPCION, '') DESC_MIEMBRO_COMPENSADOR_DESTINO, MF.CD_REF_ASIGNACION, MF.FCONTRATACION FETRADE, ")
        .append(
            " MF.FLIQUIDACION FEVALOR, MF.SENTIDO, MF.CDISIN ISIN, COALESCE(VAL.DESCRIPCION, '') DESC_ISIN, SUM(COALESCE(MN.IMTITULOS, 0.0)) TITULOS, MF.PRECIO, SUM(COALESCE(MN.IMEFECTIVO, 0.0)) EFECTIVO, MF.CDREFMOVIMIENTOECC");
    return sb.toString();
  }

  @Override
  public String getFrom() {
    String casePti = "'S'";
    String valorActivo = "'S'";
    String estadoMovimientoCancelado = String.format("'%s'", EstadosEccMovimiento.CANCELADO_INT.text);
    StringBuilder sb = new StringBuilder(
        "FROM TMCT0MOVIMIENTOECC_FIDESSA MF LEFT OUTER JOIN TMCT0_COMPENSADOR MORI ON MF.MIEMBRO_COMPENSADOR_DESTINO=MORI.NB_NOMBRE ");
    // sb.append(" LEFT OUTER JOIN TMCT0_COMPENSADOR MORI2 ON MF.MIEMBRO_COMPENSADOR_DESTINO=MORI2.NB_NOMBRE ");
    sb.append(" LEFT JOIN TMCT0_VALORES VAL ON MF.CDISIN=VAL.CODIGO_DE_VALOR AND VAL.ACTIVO = {0} ")
        .append(" LEFT JOIN TMCT0MOVIMIENTOECC M ON MF.CDREFMOVIMIENTO = M.CDREFMOVIMIENTO AND M.CDESTADOMOV <> {1} ")
        .append(
            " LEFT JOIN TMCT0MOVIMIENTOOPERACIONNUEVAECC MN ON M.IDMOVIMIENTO = MN.IDMOVIMIENTO AND MN.ID_MOVIMIENTO_FIDESSA = MF.ID_MOVIMIENTO_FIDESSA ")
        .append(
            " LEFT JOIN TMCT0DESGLOSECAMARA DC ON M.IDDESGLOSECAMARA = DC.IDDESGLOSECAMARA AND DC.CDESTADOASIG > {2} AND DC.FELIQUIDACION = MF.FLIQUIDACION ")
        .append(
            " LEFT JOIN TMCT0BOK B ON DC.NUORDEN = B.NUORDEN AND DC.NBOOKING = B.NBOOKING AND B.CDESTADOASIG > {2} AND B.CASEPTI = {3} ")
        .append(" LEFT JOIN TMCT0ALO A ON DC.NUORDEN = A.NUORDEN AND DC.NBOOKING = A.NBOOKING  ")
        .append(
            " LEFT JOIN TMCT0ALC C ON DC.NUORDEN = C.NUORDEN AND DC.NBOOKING = C.NBOOKING AND DC.NUCNFCLT= C.NUCNFCLT AND DC.NUCNFLIQ = C.NUCNFLIQ ")
        .append(" LEFT JOIN TMCT0ESTADO EST ON DC.CDESTADOASIG = EST.IDESTADO ");
    return MessageFormat.format(sb.toString(), valorActivo, estadoMovimientoCancelado,
        EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(), casePti);
  }

  @Override
  public String getWhere() {
    return String.format("WHERE MF.CDTIPOMOV='%s' AND MF.CDESTADOMOV='%s' ", TipoMovimiento.GIVE_UP.text,
        EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text);
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "GROUP BY C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ, A.REF_CLIENTE, EST.NOMBRE, B.CDALIAS, B.DESCRALI, MF.CDCAMARA, "
        + "MF.MIEMBRO_COMPENSADOR_DESTINO, MORI.NB_DESCRIPCION, MF.CD_REF_ASIGNACION, MF.FCONTRATACION, "
        + "MF.FLIQUIDACION, MF.SENTIDO, MF.CDISIN, VAL.DESCRIPCION, MF.PRECIO, MF.CDREFMOVIMIENTOECC ";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    boolean lanzarEx = true;
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      switch (filter.getField()) {
        case NBOOKING:
        case REF_CLIENTE:
        case FECHA_CONTRATACION:
        case FECHA_LIQUIDACION:

          if (!filter.getValues().isEmpty()) {
            lanzarEx = false;
          }
          break;

        default:
          break;
      }
      if (!lanzarEx) {
        break;
      }
    }
    if (lanzarEx) {
      throw new SIBBACBusinessException("Mandatory filter not set.");
    }
  }

}
