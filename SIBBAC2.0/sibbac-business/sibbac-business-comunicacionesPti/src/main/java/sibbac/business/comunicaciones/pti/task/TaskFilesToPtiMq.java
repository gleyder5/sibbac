package sibbac.business.comunicaciones.pti.task;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.jms.JMSException;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.comunicaciones.pti.mq.bo.FileToPtiMqBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME,
           name = Task.GROUP_COMUNICACIONES_ECC.JOB_ENVIO_FICHERO_MQ_PTI,
           intervalUnit = IntervalUnit.SECOND,
           interval = 30)
public class TaskFilesToPtiMq extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private FileToPtiMqBo filesystemToPti;

  @Value("${daemons.files.to.pti.mq.file.pattern}")
  private String filePattern;

  @Value("${daemons.files.to.pti.mq.online.out.path}")
  private String pathOnline;

  @Value("${daemons.files.to.pti.mq.batch.out.path}")
  private String pathBatch;
  @Value("${daemons.files.to.pti.mq.sd.out.path}")
  private String pathSd;

  public TaskFilesToPtiMq() {
  }

  private void enviarPath(Path workPath, boolean batch, boolean scriptDiviend) throws SIBBACBusinessException,
                                                                              IOException, JMSException {
    Path resultFilePath;
    String originalName;
    Path tmp = Paths.get(workPath.toString(), "tmp");
    if (Files.notExists(tmp)) {
      Files.createDirectories(tmp);
    }
    Path directorioDestino = Paths.get(workPath.toString(), "tratados");
    if (Files.notExists(directorioDestino)) {
      Files.createDirectories(directorioDestino);
    }
    // LOS PROCESOS ESCRIBIRAN ENTORNO_TIPOMSG_PROCESO_XXXXXXXXXXXXXXX.DAT
    // EL PROCESO DE ANALIZAR REVISARA ESTOS FICHEROS, CREARA UNO AGRUPANDO POR DESTINO Y PUEDE QUE DEBA AGRUPAR POR
    // FECHA DE CONTRATACION EN EL CASO DE LOS RT'S Y TI'S

    // FORMATO NOMBRE FICHERO
    // ENTORNO_ORIGEN_DESTINO_TIPOSMSG_LOTE_XXXXXXXXXXXXXXXXXXXX.DAT
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(workPath, filePattern);) {

      for (Path path : directoryStream) {
        originalName = path.toFile().getName();
        resultFilePath = filesystemToPti.enviarFicheros(path, batch, scriptDiviend);
        filesystemToPti.escribirPtiOut(resultFilePath, originalName);
        filesystemToPti.moveToTratados(directorioDestino, resultFilePath, originalName, true);
      }
    }
  }

  @Override
  public void executeTask() throws Exception {
    LOG.debug("[TaskFilesToPtiMq :: executeTask inicio.]");

    Path workPath = Paths.get(pathOnline);
    enviarPath(workPath, false, false);
    workPath = Paths.get(pathBatch);
    enviarPath(workPath, true, false);
    workPath = Paths.get(pathSd);
    enviarPath(workPath, true, true);

    LOG.debug("[TaskFilesToPtiMq :: executeTask fin.]");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.ENVIO_FICHEROS_PTI;
  }

}
