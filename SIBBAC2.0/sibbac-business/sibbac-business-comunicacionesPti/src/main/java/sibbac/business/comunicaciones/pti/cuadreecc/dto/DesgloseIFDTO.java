package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;

public class DesgloseIFDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6008833706909707593L;
  private final String numeroOperacion;
  private final ReferenciaTitularIFDTO referenciaTitular;
  private final BigDecimal numeroTitulos;

  private String entidadLiquidadora;

  // CALCULOS ECONOMICOS

  private BigDecimal brutoMercado;
  private BigDecimal brutoCliente;
  private BigDecimal netoClienteFidessa;
  private BigDecimal comisionCliente;
  private BigDecimal comisionOrdenante;
  private BigDecimal comisionImcomsis;
  private BigDecimal comisionImbansis;
  private BigDecimal comisionBanco;
  private BigDecimal comisionDevolucion;
  private BigDecimal comisionAjuste;

  private BigDecimal canonCompensacion;
  private BigDecimal canonLiquidacion;
  private BigDecimal canonContratacionTeorico;
  private BigDecimal canonContratacionAplicado;

  private BigDecimal netoCliente;
  private BigDecimal ajustePrecio;

  private BigDecimal comisionLiquidacion;
  private BigDecimal comisionBeneficio;

  // calculos economicos para el reparto
  private BigDecimal numeroTitulosReparto;
  private BigDecimal brutoMercadoReparto;
  private BigDecimal brutoClienteReparto;
  private BigDecimal netoClienteFidessaReparto;
  private BigDecimal comisionClienteReparto;
  private BigDecimal comisionOrdenanteReparto;
  private BigDecimal comisionImcomsisReparto;
  private BigDecimal comisionImbansisReparto;
  private BigDecimal comisionBancoReparto;
  private BigDecimal comisionDevolucionReparto;
  private BigDecimal comisionAjusteReparto;

  private BigDecimal canonCompensacionReparto;
  private BigDecimal canonLiquidacionReparto;
  private BigDecimal canonContratacionTeoricoReparto;
  private BigDecimal canonContratacionAplicadoReparto;

  private BigDecimal netoClienteReparto;
  private BigDecimal ajustePrecioReparto;

  private BigDecimal comisionLiquidacionReparto;
  private BigDecimal comisionBeneficioReparto;

  private final IdentificacionOperacionDatosMercadoDTO datosMercado;

  private Integer cdestadoasig, cdestadotit, cdestadocont, cdestadoentrec;

  private Long iddesglosecamara, nudesglose;
  private Long idejecucionalocation;

  private String nuejecuc;

  private String cuentaCompensacion;
  private String miembroCompensador;
  private String referenciaAsignacion;

  private CuadreEccReferencia cuadreEccReferencia;
  private CuadreEccSibbac cuadreEccSibbac;
  private Tmct0desgloseclitit desgloseRelacionado;
  private EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionUtilizada;

  public DesgloseIFDTO(String numeroOperacion,
                       ReferenciaTitularIFDTO referenciaTitular,
                       BigDecimal numeroTitulos,
                       IdentificacionOperacionDatosMercadoDTO datosMercado) {
    super();
    this.numeroOperacion = numeroOperacion;
    this.referenciaTitular = referenciaTitular;
    this.numeroTitulos = numeroTitulos;
    this.datosMercado = datosMercado;
  }

  public String getNumeroOperacion() {
    return numeroOperacion;
  }

  public ReferenciaTitularIFDTO getReferenciaTitular() {
    return referenciaTitular;
  }

  public BigDecimal getNumeroTitulos() {
    return numeroTitulos;
  }

  public IdentificacionOperacionDatosMercadoDTO getDatosMercado() {
    return datosMercado;
  }

  public Integer getCdestadoasig() {
    return cdestadoasig;
  }

  public Integer getCdestadotit() {
    return cdestadotit;
  }

  public Integer getCdestadocont() {
    return cdestadocont;
  }

  public Integer getCdestadoentrec() {
    return cdestadoentrec;
  }

  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  public void setCdestadotit(Integer cdestadotit) {
    this.cdestadotit = cdestadotit;
  }

  public void setCdestadocont(Integer cdestadocont) {
    this.cdestadocont = cdestadocont;
  }

  public void setCdestadoentrec(Integer cdestadoentrec) {
    this.cdestadoentrec = cdestadoentrec;
  }

  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  public Long getNudesglose() {
    return nudesglose;
  }

  public Long getIdejecucionalocation() {
    return idejecucionalocation;
  }

  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  public void setIdejecucionalocation(Long idejecucionalocation) {
    this.idejecucionalocation = idejecucionalocation;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public String getEntidadLiquidadora() {
    return entidadLiquidadora;
  }

  public void setEntidadLiquidadora(String entidadLiquidadora) {
    this.entidadLiquidadora = entidadLiquidadora;
  }

  public BigDecimal getBrutoMercado() {
    return brutoMercado;
  }

  public BigDecimal getBrutoCliente() {
    return brutoCliente;
  }

  public BigDecimal getNetoClienteFidessa() {
    return netoClienteFidessa;
  }

  public BigDecimal getComisionCliente() {
    return comisionCliente;
  }

  public BigDecimal getComisionOrdenante() {
    return comisionOrdenante;
  }

  public BigDecimal getComisionImcomsis() {
    return comisionImcomsis;
  }

  public BigDecimal getComisionImbansis() {
    return comisionImbansis;
  }

  public BigDecimal getComisionBanco() {
    return comisionBanco;
  }

  public BigDecimal getComisionDevolucion() {
    return comisionDevolucion;
  }

  public BigDecimal getComisionAjuste() {
    return comisionAjuste;
  }

  public void setComisionAjuste(BigDecimal comisionAjuste) {
    this.comisionAjuste = comisionAjuste;
  }

  public BigDecimal getCanonCompensacion() {
    return canonCompensacion;
  }

  public BigDecimal getCanonLiquidacion() {
    return canonLiquidacion;
  }

  public BigDecimal getCanonContratacionTeorico() {
    return canonContratacionTeorico;
  }

  public BigDecimal getCanonContratacionAplicado() {
    return canonContratacionAplicado;
  }

  public BigDecimal getNetoCliente() {
    return netoCliente;
  }

  public BigDecimal getAjustePrecio() {
    return ajustePrecio;
  }

  public BigDecimal getComisionLiquidacion() {
    return comisionLiquidacion;
  }

  public BigDecimal getComisionBeneficio() {
    return comisionBeneficio;
  }

  public void setBrutoMercado(BigDecimal brutoMercado) {
    this.brutoMercado = brutoMercado;
  }

  public void setBrutoCliente(BigDecimal brutoCliente) {
    this.brutoCliente = brutoCliente;
  }

  public void setNetoClienteFidessa(BigDecimal brutoClienteFidessa) {
    this.netoClienteFidessa = brutoClienteFidessa;
  }

  public void setComisionCliente(BigDecimal comisionCliente) {
    this.comisionCliente = comisionCliente;
  }

  public void setComisionOrdenante(BigDecimal comisionOrdenante) {
    this.comisionOrdenante = comisionOrdenante;
  }

  public void setComisionImcomsis(BigDecimal comisionImcomsis) {
    this.comisionImcomsis = comisionImcomsis;
  }

  public void setComisionImbansis(BigDecimal comisionImbansis) {
    this.comisionImbansis = comisionImbansis;
  }

  public void setComisionBanco(BigDecimal comisionBanco) {
    this.comisionBanco = comisionBanco;
  }

  public void setComisionDevolucion(BigDecimal comisionDevolucion) {
    this.comisionDevolucion = comisionDevolucion;
  }

  public void setCanonCompensacion(BigDecimal canonCompensacion) {
    this.canonCompensacion = canonCompensacion;
  }

  public void setCanonLiquidacion(BigDecimal canonLiquidacion) {
    this.canonLiquidacion = canonLiquidacion;
  }

  public void setCanonContratacionTeorico(BigDecimal canonContratacionTeorico) {
    this.canonContratacionTeorico = canonContratacionTeorico;
  }

  public void setCanonContratacionAplicado(BigDecimal canonContratacionAplicado) {
    this.canonContratacionAplicado = canonContratacionAplicado;
  }

  public void setNetoCliente(BigDecimal netoCliente) {
    this.netoCliente = netoCliente;
  }

  public void setAjustePrecio(BigDecimal ajustePrecio) {
    this.ajustePrecio = ajustePrecio;
  }

  public void setComisionLiquidacion(BigDecimal comisionLiquidacion) {
    this.comisionLiquidacion = comisionLiquidacion;
  }

  public void setComisionBeneficio(BigDecimal comisionBeneficio) {
    this.comisionBeneficio = comisionBeneficio;
  }

  public BigDecimal getNumeroTitulosReparto() {
    return numeroTitulosReparto;
  }

  public BigDecimal getBrutoMercadoReparto() {
    return brutoMercadoReparto;
  }

  public BigDecimal getBrutoClienteReparto() {
    return brutoClienteReparto;
  }

  public BigDecimal getNetoClienteFidessaReparto() {
    return netoClienteFidessaReparto;
  }

  public BigDecimal getComisionClienteReparto() {
    return comisionClienteReparto;
  }

  public BigDecimal getComisionOrdenanteReparto() {
    return comisionOrdenanteReparto;
  }

  public BigDecimal getComisionImcomsisReparto() {
    return comisionImcomsisReparto;
  }

  public BigDecimal getComisionImbansisReparto() {
    return comisionImbansisReparto;
  }

  public BigDecimal getComisionBancoReparto() {
    return comisionBancoReparto;
  }

  public BigDecimal getComisionDevolucionReparto() {
    return comisionDevolucionReparto;
  }

  public BigDecimal getComisionAjusteReparto() {
    return comisionAjusteReparto;
  }

  public BigDecimal getCanonCompensacionReparto() {
    return canonCompensacionReparto;
  }

  public BigDecimal getCanonLiquidacionReparto() {
    return canonLiquidacionReparto;
  }

  public BigDecimal getCanonContratacionTeoricoReparto() {
    return canonContratacionTeoricoReparto;
  }

  public BigDecimal getCanonContratacionAplicadoReparto() {
    return canonContratacionAplicadoReparto;
  }

  public BigDecimal getNetoClienteReparto() {
    return netoClienteReparto;
  }

  public BigDecimal getAjustePrecioReparto() {
    return ajustePrecioReparto;
  }

  public BigDecimal getComisionLiquidacionReparto() {
    return comisionLiquidacionReparto;
  }

  public BigDecimal getComisionBeneficioReparto() {
    return comisionBeneficioReparto;
  }

  public void setNumeroTitulosReparto(BigDecimal numeroTitulosReparto) {
    this.numeroTitulosReparto = numeroTitulosReparto;
  }

  public void setBrutoMercadoReparto(BigDecimal brutoMercadoReparto) {
    this.brutoMercadoReparto = brutoMercadoReparto;
  }

  public void setBrutoClienteReparto(BigDecimal brutoClienteReparto) {
    this.brutoClienteReparto = brutoClienteReparto;
  }

  public void setNetoClienteFidessaReparto(BigDecimal netoClienteFidessaReparto) {
    this.netoClienteFidessaReparto = netoClienteFidessaReparto;
  }

  public void setComisionClienteReparto(BigDecimal comisionClienteReparto) {
    this.comisionClienteReparto = comisionClienteReparto;
  }

  public void setComisionOrdenanteReparto(BigDecimal comisionOrdenanteReparto) {
    this.comisionOrdenanteReparto = comisionOrdenanteReparto;
  }

  public void setComisionImcomsisReparto(BigDecimal comisionImcomsisReparto) {
    this.comisionImcomsisReparto = comisionImcomsisReparto;
  }

  public void setComisionImbansisReparto(BigDecimal comisionImbansisReparto) {
    this.comisionImbansisReparto = comisionImbansisReparto;
  }

  public void setComisionBancoReparto(BigDecimal comisionBancoReparto) {
    this.comisionBancoReparto = comisionBancoReparto;
  }

  public void setComisionDevolucionReparto(BigDecimal comisionDevolucionReparto) {
    this.comisionDevolucionReparto = comisionDevolucionReparto;
  }

  public void setComisionAjusteReparto(BigDecimal comisionAjusteReparto) {
    this.comisionAjusteReparto = comisionAjusteReparto;
  }

  public void setCanonCompensacionReparto(BigDecimal canonCompensacionReparto) {
    this.canonCompensacionReparto = canonCompensacionReparto;
  }

  public void setCanonLiquidacionReparto(BigDecimal canonLiquidacionReparto) {
    this.canonLiquidacionReparto = canonLiquidacionReparto;
  }

  public void setCanonContratacionTeoricoReparto(BigDecimal canonContratacionTeoricoReparto) {
    this.canonContratacionTeoricoReparto = canonContratacionTeoricoReparto;
  }

  public void setCanonContratacionAplicadoReparto(BigDecimal canonContratacionAplicadoReparto) {
    this.canonContratacionAplicadoReparto = canonContratacionAplicadoReparto;
  }

  public void setNetoClienteReparto(BigDecimal netoClienteReparto) {
    this.netoClienteReparto = netoClienteReparto;
  }

  public void setAjustePrecioReparto(BigDecimal ajustePrecioReparto) {
    this.ajustePrecioReparto = ajustePrecioReparto;
  }

  public void setComisionLiquidacionReparto(BigDecimal comisionLiquidacionReparto) {
    this.comisionLiquidacionReparto = comisionLiquidacionReparto;
  }

  public void setComisionBeneficioReparto(BigDecimal comisionBeneficioReparto) {
    this.comisionBeneficioReparto = comisionBeneficioReparto;
  }

  public Tmct0desgloseclitit getDesgloseRelacionado() {
    return desgloseRelacionado;
  }

  public void setDesgloseRelacionado(Tmct0desgloseclitit desgloseRelacionado) {
    this.desgloseRelacionado = desgloseRelacionado;
  }

  public String getCuentaCompensacion() {
    return cuentaCompensacion;
  }

  public String getMiembroCompensador() {
    return miembroCompensador;
  }

  public String getReferenciaAsignacion() {
    return referenciaAsignacion;
  }

  public void setCuentaCompensacion(String cuentaCompensacion) {
    this.cuentaCompensacion = cuentaCompensacion;
  }

  public void setMiembroCompensador(String miembroCompensador) {
    this.miembroCompensador = miembroCompensador;
  }

  public void setReferenciaAsignacion(String referenciaAsignacion) {
    this.referenciaAsignacion = referenciaAsignacion;
  }

  public EjecucionAlocationProcesarDesglosesOpEspecialesDTO getEjecucionUtilizada() {
    return ejecucionUtilizada;
  }

  public void setEjecucionUtilizada(EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionUtilizada) {
    this.ejecucionUtilizada = ejecucionUtilizada;
  }

  public void loadEconomicData(Tmct0desgloseclitit dct) {
    // calculos
    setBrutoMercado(dct.getImefectivo());
    setBrutoMercadoReparto(dct.getImefectivo());

    setCanonContratacionAplicado(dct.getImcanoncontreu());
    setCanonContratacionAplicadoReparto(dct.getImcanoncontreu());
    setCanonContratacionTeorico(dct.getImcanoncontrdv());
    setCanonContratacionTeoricoReparto(dct.getImcanoncontrdv());

    setBrutoCliente(dct.getImtotbru());
    setBrutoClienteReparto(dct.getImtotbru());
    setNetoClienteFidessa(dct.getImtotnet());
    setNetoClienteFidessaReparto(dct.getImtotnet());
    setComisionCliente(dct.getImcomisn());
    setComisionClienteReparto(dct.getImcomisn());
    setComisionOrdenante(dct.getImcomdvo());
    setComisionOrdenanteReparto(dct.getImcomdvo());
    setComisionImcomsis(dct.getImcomsis());
    setComisionImcomsisReparto(dct.getImcomsis());
    setComisionImbansis(dct.getImbansis());
    setComisionImbansisReparto(dct.getImbansis());
    setComisionBanco(dct.getImcombco());
    setComisionBancoReparto(dct.getImcombco());
    setComisionDevolucion(dct.getImcombrk());
    setComisionDevolucionReparto(dct.getImcombrk());
    setNetoCliente(dct.getImnfiliq());
    setNetoClienteReparto(dct.getImnfiliq());
    setAjustePrecio(dct.getImntbrok());
    setAjustePrecioReparto(dct.getImntbrok());
    setComisionLiquidacion(dct.getImfinsvb());
    setComisionLiquidacionReparto(dct.getImfinsvb());
    setComisionBeneficio(dct.getImcomsvb());
    setComisionBeneficioReparto(dct.getImcomsvb());
    setComisionAjuste(dct.getImajusvb());
    setComisionAjusteReparto(dct.getImajusvb());
  }

  public CuadreEccReferencia getCuadreEccReferencia() {
    return cuadreEccReferencia;
  }

  public CuadreEccSibbac getCuadreEccSibbac() {
    return cuadreEccSibbac;
  }

  public void setCuadreEccReferencia(CuadreEccReferencia cuadreEccReferencia) {
    this.cuadreEccReferencia = cuadreEccReferencia;
  }

  public void setCuadreEccSibbac(CuadreEccSibbac cuadreEccSibbac) {
    this.cuadreEccSibbac = cuadreEccSibbac;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((datosMercado == null) ? 0 : datosMercado.hashCode());
    result = prime * result + ((numeroOperacion == null) ? 0 : numeroOperacion.hashCode());
    result = prime * result + ((numeroTitulos == null) ? 0 : numeroTitulos.hashCode());
    result = prime * result + ((referenciaTitular == null) ? 0 : referenciaTitular.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DesgloseIFDTO other = (DesgloseIFDTO) obj;
    if (datosMercado == null) {
      if (other.datosMercado != null)
        return false;
    } else if (!datosMercado.equals(other.datosMercado))
      return false;
    if (numeroOperacion == null) {
      if (other.numeroOperacion != null)
        return false;
    } else if (!numeroOperacion.equals(other.numeroOperacion))
      return false;
    if (numeroTitulos == null) {
      if (other.numeroTitulos != null)
        return false;
    } else if (numeroTitulos.compareTo(other.numeroTitulos) != 0)
      return false;
    if (referenciaTitular == null) {
      if (other.referenciaTitular != null)
        return false;
    } else if (!referenciaTitular.equals(other.referenciaTitular))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "DesgloseIFDTO [numeroOperacion=" + numeroOperacion + ", referenciaTitular=" + referenciaTitular
           + ", numeroTitulos=" + numeroTitulos + ", datosMercado=" + datosMercado + ", cdestadoasig=" + cdestadoasig
           + ", cdestadotit=" + cdestadotit + ", cdestadocont=" + cdestadocont + ", cdestadoentrec=" + cdestadoentrec
           + "]";
  }

}
