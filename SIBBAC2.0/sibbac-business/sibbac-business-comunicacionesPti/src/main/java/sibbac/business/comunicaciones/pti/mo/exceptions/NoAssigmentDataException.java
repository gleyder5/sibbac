package sibbac.business.comunicaciones.pti.mo.exceptions;

import sibbac.common.SIBBACBusinessException;

public class NoAssigmentDataException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = 6711153890586923240L;

  public NoAssigmentDataException() {
    // TODO Auto-generated constructor stub
  }

  public NoAssigmentDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

  public NoAssigmentDataException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public NoAssigmentDataException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public NoAssigmentDataException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
