package sibbac.business.comunicaciones.imputacions3.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.mo.bo.MovimientosBo;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoPtiMensajeDataException;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageVersion;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class ImputacionS3Multithread {

  protected static final Logger LOG = LoggerFactory.getLogger(ImputacionS3Bo.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private MovimientosBo movimientosBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  /**
   * @param cdoperacionecc
   * @param cuentaCompensacionS3
   * @throws NoPtiMensajeDataException
   * @throws PTIMessageException
   * @throws JMSException
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED)
  public String crearImputacionesS3(List<String> cdoperacionecc, String cuentaCompensacionS3,
      PTIMessageVersion version, String auditUser) throws NoPtiMensajeDataException, PTIMessageException, JMSException,
      SIBBACBusinessException {
    int posIni = 0;
    int posEnd = 0;
    int incremento = 1;
    List<String> subList;
    ImputacionS3Callable process;
    String camaraNacional = cfgBo.getIdPti();
    String camaraEuroCcp = cfgBo.getIdEuroCcp();
    ConnectionFactory cf = movimientosBo.getConnectionFactory();
    Future<BigDecimal> future;
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    tfb.setNameFormat("Thread-ImputacionS3-%d");
    ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());
    List<Future<BigDecimal>> futures = new ArrayList<Future<BigDecimal>>();
    ImputacionS3ConfigDTO config = new ImputacionS3ConfigDTO(cf, cuentaCompensacionS3, version, camaraEuroCcp,
        camaraNacional, auditUser);
    try {
      while (cdoperacionecc.size() > posEnd) {
        posEnd = Math.min(posEnd + incremento, cdoperacionecc.size());
        subList = cdoperacionecc.subList(posIni, posEnd);
        process = ctx.getBean(ImputacionS3Callable.class, config, subList);
        future = executor.submit(process);
        futures.add(future);
        posIni = posEnd;
      }

    }
    catch (Exception e) {
      executor.shutdownNow();
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {
      if (!executor.isShutdown()) {
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        try {
          executor.awaitTermination(2, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage());
          LOG.trace(e.getMessage(), e);

        }
      }
    }

    BigDecimal titulos = this.recuperarResultadosFutures(futures);

    return MessageFormat.format("Se han imputado a S3 {0} titulos de {1} operaciones distintas",
        titulos.setScale(0, RoundingMode.HALF_DOWN), cdoperacionecc.size());

  }

  private BigDecimal recuperarResultadosFutures(List<Future<BigDecimal>> futures) throws SIBBACBusinessException {
    BigDecimal titulos = BigDecimal.ZERO;
    for (Future<BigDecimal> future : futures) {
      try {
        titulos = titulos.add(future.get(1, TimeUnit.SECONDS));
      }
      catch (TimeoutException e) {
        future.cancel(true);
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
      catch (InterruptedException | ExecutionException e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
    }
    return titulos;
  }

}
