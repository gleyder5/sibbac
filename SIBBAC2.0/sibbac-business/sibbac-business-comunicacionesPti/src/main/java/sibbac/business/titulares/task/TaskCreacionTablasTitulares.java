package sibbac.business.titulares.task;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.titulares.bo.CreacionTablasTitularesMultithread;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_CREACION_TABLAS_TITULARES, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-23 ? * MON-FRI")
public class TaskCreacionTablasTitulares extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private CreacionTablasTitularesMultithread creacionTablasTitularesMultithread;

  @Override
  public void executeTask() throws Exception {
    creacionTablasTitularesMultithread.crearTablasTitulares(false, "SIBBAC20");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_CREACION_TABLAS_TITULARES;
  }

}
