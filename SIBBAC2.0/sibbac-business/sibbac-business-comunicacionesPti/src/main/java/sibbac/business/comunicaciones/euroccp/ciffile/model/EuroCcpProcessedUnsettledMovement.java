package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.PROCESSED_UNSETTLED_MOVEMENT)
@Entity
public class EuroCcpProcessedUnsettledMovement extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "SUBACCOUNT_NUMBER", length = 10, nullable = false)
  private String subaccountNumber;

  @Column(name = "OPPOSITE_PARTY_CODE", length = 6, nullable = false)
  private String oppositePartyCode;

  @Column(name = "PRODUCT_GROUP_CODE", length = 2, nullable = false)
  private String productGroupCode;

  @Column(name = "EXCHANGE_CODE_TRADE", length = 4, nullable = false)
  private String exchangeCodeTrade;

  @Column(name = "SYMBOL", length = 6, nullable = false)
  private String symbol;

  @Column(name = "TYPE_FIELD", length = 1, nullable = true)
  private Character typeField;

  @Temporal(TemporalType.DATE)
  @Column(name = "EXPIRATION_DATE", nullable = true)
  private Date expirationDate;

  @Column(name = "EXERCISE_PRICE", length = 15, scale = 7, nullable = true)
  private BigDecimal exercisePrice;

  @Column(name = "EXTERNAL_MEMBER", length = 10, nullable = true)
  private String externalMember;

  @Column(name = "EXTERNAL_ACCOUNT", length = 15, nullable = false)
  private String externalAccount;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;

  @Column(name = "MOVEMENT_CODE", length = 2, nullable = false)
  private String movementCode;

  @Column(name = "BUY_SELL_CODE", length = 1, nullable = false)
  private Character buySellCode;

  @Column(name = "PROCESSED_QUANTITY_LONG", length = 12, scale = 2, nullable = true)
  private BigDecimal processedQuantityLong;

  @Column(name = "PROCESSED_QUANTITY_SHORT", length = 12, scale = 2, nullable = true)
  private BigDecimal processedQuantityShort;

  @Column(name = "CLEARING_FEE", length = 12, scale = 4, nullable = true)
  private BigDecimal clearingFee;

  @Column(name = "CLEARING_FEE_DC", length = 1, nullable = true)
  private Character clearingFeeDC;

  @Column(name = "CLEARING_FEE_CURRENCY_CODE", length = 3, nullable = true)
  private String clearingFeeCurrencyCode;

  @Column(name = "COUNTER_VALUE", length = 18, scale = 2, nullable = true)
  private BigDecimal counterValue;

  @Column(name = "COUNTER_VALUE_DC", length = 1, nullable = true)
  private Character counterValueDC;

  @Column(name = "COUNTER_VALUE_CURRENCY_CODE", length = 3, nullable = true)
  private String counterValueCurrencyCode;

  @Column(name = "CUPON_INTEREST", length = 18, scale = 2, nullable = true)
  private BigDecimal cuponInterest;

  @Column(name = "CUPON_INTEREST_DC", length = 1, nullable = true)
  private Character cuponInterestDC;

  @Column(name = "CUPON_INTEREST_CURRENCY_CODE", length = 3, nullable = true)
  private String cuponInterestCurrencyCode;

  @Column(name = "EFFECTIVE_VALUE", length = 18, scale = 2, nullable = false)
  private BigDecimal effectiveValue;

  @Column(name = "EFFECTIVE_VALUE_DC", length = 1, nullable = false)
  private Character effectiveValueDC;

  @Column(name = "TRANSACTION_PRICE", length = 15, scale = 7, nullable = false)
  private BigDecimal transactionPrice;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRANSACTION_DATE", nullable = false)
  private Date transactionDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "SETTLEMENT_DATE", nullable = false)
  private Date settlementDate;

  @Column(name = "UNSETTLED_REFERENCE", length = 16, nullable = false)
  private String unsettledReference;

  @Column(name = "UNSETTLED_REFERENCE_ORIGINAL", length = 16, nullable = false)
  private String unsettledReferenceOriginal;

  @Column(name = "EXTERNAL_TRANSACTION_ID_EXCHANGE", length = 20, nullable = false)
  private String externalTransactionIdExchange;

  @Column(name = "SETTLEMENT_INSTRUCTION_REFERENCE", length = 9, nullable = true)
  private String settlementInstructionReference;

  @Column(name = "ORDER_NUMBER", length = 10, nullable = true)
  private String orderNumber;

  @Column(name = "ISIN_CODE", length = 12, nullable = false)
  private String isinCode;

  @Column(name = "TRADER_INITIALS", length = 6, nullable = true)
  private String traderInitials;

  @Column(name = "ULV_TRADING_UNIT", length = 11, scale = 4, nullable = true)
  private String ulvTradingUnit;

  @Column(name = "TRANSACTION_ORIGIN", length = 4, nullable = true)
  private String transactionOrigin;

  @Column(name = "EXECUTION_TRADING_ID", length = 6, nullable = true)
  private String executionTradingId;

  @Column(name = "DEPOT_ID", length = 6, nullable = false)
  private String depotId;

  @Column(name = "SAFE_KEEPING_ID", length = 10, nullable = false)
  private String safeKeepingId;

  @Column(name = "COMMENT_FIELD", length = 21, nullable = true)
  private String commentField;

  @Temporal(TemporalType.TIME)
  @Column(name = "TIMESTAMP_FIELD", length = 6, nullable = true)
  private Date timestampField;

  @Column(name = "TRANSACTION_TYPE_CODE", length = 3, nullable = false)
  private String transactionTypeCode;

  @Column(name = "EXTERNAL_POSITION_ACCOUNT_ID", length = 30, nullable = true)
  private String externalPositionAccountId;

  @Column(name = "DUAL_LISTED_INDICATOR", length = 1, nullable = true)
  private Character dualListedIndicator;

  @Column(name = "FILLER", length = 96, nullable = true)
  private String filler;

  public EuroCcpProcessedUnsettledMovement() {
  }

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getSubaccountNumber() {
    return subaccountNumber;
  }

  public String getOppositePartyCode() {
    return oppositePartyCode;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public String getExchangeCodeTrade() {
    return exchangeCodeTrade;
  }

  public String getSymbol() {
    return symbol;
  }

  public Character getTypeField() {
    return typeField;
  }

  public Date getExpirationDate() {
    return expirationDate;
  }

  public BigDecimal getExercisePrice() {
    return exercisePrice;
  }

  public String getExternalMember() {
    return externalMember;
  }

  public String getExternalAccount() {
    return externalAccount;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getMovementCode() {
    return movementCode;
  }

  public Character getBuySellCode() {
    return buySellCode;
  }

  public BigDecimal getProcessedQuantityLong() {
    return processedQuantityLong;
  }

  public BigDecimal getProcessedQuantityShort() {
    return processedQuantityShort;
  }

  public BigDecimal getClearingFee() {
    return clearingFee;
  }

  public Character getClearingFeeDC() {
    return clearingFeeDC;
  }

  public String getClearingFeeCurrencyCode() {
    return clearingFeeCurrencyCode;
  }

  public BigDecimal getCounterValue() {
    return counterValue;
  }

  public Character getCounterValueDC() {
    return counterValueDC;
  }

  public String getCounterValueCurrencyCode() {
    return counterValueCurrencyCode;
  }

  public BigDecimal getCuponInterest() {
    return cuponInterest;
  }

  public Character getCuponInterestDC() {
    return cuponInterestDC;
  }

  public String getCuponInterestCurrencyCode() {
    return cuponInterestCurrencyCode;
  }

  public BigDecimal getEffectiveValue() {
    return effectiveValue;
  }

  public Character getEffectiveValueDC() {
    return effectiveValueDC;
  }

  public BigDecimal getTransactionPrice() {
    return transactionPrice;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public Date getSettlementDate() {
    return settlementDate;
  }

  public String getUnsettledReference() {
    return unsettledReference;
  }

  public String getExternalTransactionIdExchange() {
    return externalTransactionIdExchange;
  }

  public String getSettlementInstructionReference() {
    return settlementInstructionReference;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public String getIsinCode() {
    return isinCode;
  }

  public String getTraderInitials() {
    return traderInitials;
  }

  public String getUlvTradingUnit() {
    return ulvTradingUnit;
  }

  public String getTransactionOrigin() {
    return transactionOrigin;
  }

  public String getExecutionTradingId() {
    return executionTradingId;
  }

  public String getDepotId() {
    return depotId;
  }

  public String getSafeKeepingId() {
    return safeKeepingId;
  }

  public String getCommentField() {
    return commentField;
  }

  public Date getTimestampField() {
    return timestampField;
  }

  public String getTransactionTypeCode() {
    return transactionTypeCode;
  }

  public String getExternalPositionAccountId() {
    return externalPositionAccountId;
  }

  public Character getDualListedIndicator() {
    return dualListedIndicator;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setSubaccountNumber(String subaccountNumber) {
    this.subaccountNumber = subaccountNumber;
  }

  public void setOppositePartyCode(String oppositePartyCode) {
    this.oppositePartyCode = oppositePartyCode;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public void setExchangeCodeTrade(String exchangeCodeTrade) {
    this.exchangeCodeTrade = exchangeCodeTrade;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public void setTypeField(Character typeField) {
    this.typeField = typeField;
  }

  public void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }

  public void setExercisePrice(BigDecimal exercisePrice) {
    this.exercisePrice = exercisePrice;
  }

  public void setExternalMember(String externalMember) {
    this.externalMember = externalMember;
  }

  public void setExternalAccount(String externalAccount) {
    this.externalAccount = externalAccount;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setMovementCode(String movementCode) {
    this.movementCode = movementCode;
  }

  public void setBuySellCode(Character buySellCode) {
    this.buySellCode = buySellCode;
  }

  public void setProcessedQuantityLong(BigDecimal processedQuantityLong) {
    this.processedQuantityLong = processedQuantityLong;
  }

  public void setProcessedQuantityShort(BigDecimal processedQuantityShort) {
    this.processedQuantityShort = processedQuantityShort;
  }

  public void setClearingFee(BigDecimal clearingFee) {
    this.clearingFee = clearingFee;
  }

  public void setClearingFeeDC(Character clearingFeeDC) {
    this.clearingFeeDC = clearingFeeDC;
  }

  public void setClearingFeeCurrencyCode(String clearingFeeCurrencyCode) {
    this.clearingFeeCurrencyCode = clearingFeeCurrencyCode;
  }

  public void setCounterValue(BigDecimal counterValue) {
    this.counterValue = counterValue;
  }

  public void setCounterValueDC(Character counterValueDC) {
    this.counterValueDC = counterValueDC;
  }

  public void setCounterValueCurrencyCode(String counterValueCurrencyCode) {
    this.counterValueCurrencyCode = counterValueCurrencyCode;
  }

  public void setCuponInterest(BigDecimal cuponInterest) {
    this.cuponInterest = cuponInterest;
  }

  public void setCuponInterestDC(Character cuponInterestDC) {
    this.cuponInterestDC = cuponInterestDC;
  }

  public void setCuponInterestCurrencyCode(String cuponInterestCurrencyCode) {
    this.cuponInterestCurrencyCode = cuponInterestCurrencyCode;
  }

  public void setEffectiveValue(BigDecimal effectiveValue) {
    this.effectiveValue = effectiveValue;
  }

  public void setEffectiveValueDC(Character effectiveValueDC) {
    this.effectiveValueDC = effectiveValueDC;
  }

  public void setTransactionPrice(BigDecimal transactionPrice) {
    this.transactionPrice = transactionPrice;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setSettlementDate(Date settlementDate) {
    this.settlementDate = settlementDate;
  }

  public void setUnsettledReference(String unsettledReference) {
    this.unsettledReference = unsettledReference;
  }

  public void setExternalTransactionIdExchange(String externalTransactionIdExchange) {
    this.externalTransactionIdExchange = externalTransactionIdExchange;
  }

  public void setSettlementInstructionReference(String settlementInstructionReference) {
    this.settlementInstructionReference = settlementInstructionReference;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public void setIsinCode(String isinCode) {
    this.isinCode = isinCode;
  }

  public void setTraderInitials(String traderInitials) {
    this.traderInitials = traderInitials;
  }

  public void setUlvTradingUnit(String ulvTradingUnit) {
    this.ulvTradingUnit = ulvTradingUnit;
  }

  public void setTransactionOrigin(String transactionOrigin) {
    this.transactionOrigin = transactionOrigin;
  }

  public void setExecutionTradingId(String executionTradingId) {
    this.executionTradingId = executionTradingId;
  }

  public void setDepotId(String depotId) {
    this.depotId = depotId;
  }

  public void setSafeKeepingId(String safeKeepingId) {
    this.safeKeepingId = safeKeepingId;
  }

  public void setCommentField(String commentField) {
    this.commentField = commentField;
  }

  public void setTimestampField(Date timestampField) {
    this.timestampField = timestampField;
  }

  public void setTransactionTypeCode(String transactionTypeCode) {
    this.transactionTypeCode = transactionTypeCode;
  }

  public void setExternalPositionAccountId(String externalPositionAccountId) {
    this.externalPositionAccountId = externalPositionAccountId;
  }

  public void setDualListedIndicator(Character dualListedIndicator) {
    this.dualListedIndicator = dualListedIndicator;
  }

  public String getUnsettledReferenceOriginal() {
    return unsettledReferenceOriginal;
  }

  public void setUnsettledReferenceOriginal(String unsettledReferenceOriginal) {
    this.unsettledReferenceOriginal = unsettledReferenceOriginal;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

}
