package sibbac.business.comunicaciones.pti.cuadreecc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.envers.NotAudited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a
 * {@link sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia.database.model.ECCMessageInTI } .
 * Entity: "ECCMessageInTI".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table(name = DBConstants.CUADRE_ECC.CUADRE_ECC_REFERENCIAS)
@Entity
@XmlRootElement
// @Audited
public class CuadreEccReferencia extends ATable<CuadreEccReferencia> implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -8180318255156593773L;

  @Column(name = "ESTADO_PROCESADO")
  @XmlAttribute
  private Integer estadoProcesado;

  /**
   * Guardamos estado de asignacion - dos estados PTDE_IF y CUADRE_IF
   */
  @XmlAttribute
  @Column(name = "ESTADO", nullable = false)
  private Integer estado;

  /**
   * referencia titular - No podra empezar por AA� (uso reservado a el PTI)
   */
  @Column(name = "REF_TITULAR", nullable = false, length = 20)
  @XmlAttribute
  private String refTitular;

  /**
   * referencia adicional - Referencia de uso libre
   */
  @Column(name = "REF_ADICIONAL", nullable = true, length = 20)
  @XmlAttribute
  private String refAdicional;

  /**
   * indicador de rectificacion
   */
  @Column(name = "IND_RECTIFICACION", nullable = false, length = 1)
  @XmlAttribute
  private String indRectificacion;

  /**
   * Num. Valores/ Importe Nominal
   */
  @Column(name = "NUM_VALORES_IMP_NOMINAL", nullable = false, precision = 18, scale = 6)
  @XmlAttribute
  private BigDecimal numValoresImpNominal;

  /**
   * Num. Valores/ Importe Nominal
   */
  @Column(name = "NUM_VALORES_IMP_NOMINAL_PENDIENTE_DESGLOSE", nullable = false, precision = 18, scale = 6)
  @XmlAttribute
  private BigDecimal numValoresImpNominalPendienteDesglose;

  /**
   * Numero de operacion de la ECC o del DCV segun corresponda
   */
  @Column(name = "NUM_OP", nullable = false, length = 35)
  @XmlAttribute
  private String numOp;

  /**
   * campo de entidad comunicadora de la referencia titular - Codigo de miembro de mercado o BIC de la Entidad
   * Participante
   */
  @Column(name = "ENT_COMUNICADORA", nullable = false, length = 11)
  @XmlAttribute
  private String entComunicadora;

  /**
   * fecha de cuadre
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_CUADRE", nullable = true)
  @XmlAttribute
  private Date fechaCuadre;

  /**
   * Num. Valores/ Importe Nominal fallido
   */
  @Column(name = "FALLIDO_NUM_VALORES_IMP_NOMINAL", nullable = false, precision = 18, scale = 6)
  @XmlAttribute
  private BigDecimal fallidoNumValoresImpNominal;

  /**
   * Importe Efectivo
   */
  @Column(name = "IMP_EFECTIVO", nullable = false, precision = 15, scale = 2)
  @XmlAttribute
  private BigDecimal impEfectivo;

  /**
   * N- Nominal U-unidades monetarias
   */
  @Column(name = "IND_COTIZACION", nullable = false, length = 1)
  @XmlAttribute
  private String indCotizacion;

  /**
   * pais de residencia
   */
  @Column(name = "PAIS_RESIDENCIA", nullable = false, length = 3)
  @XmlAttribute
  private String paisResidencia;

  /**
   * codigo postal
   */
  @Column(name = "COD_POSTAL", nullable = false, length = 5)
  @XmlAttribute
  private String codPostal;

  /**
   * domicilio
   */
  @Column(name = "DOMICILIO", nullable = false, length = 40)
  @XmlAttribute
  private String domicilio;

  /**
   * poblacion
   */
  @Column(name = "POBLACION", nullable = false, length = 40)
  @XmlAttribute
  private String poblacion;

  /**
   * campo de fecha de ejecucion - formato AAAA-MM-DD
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_EJE", nullable = false)
  @XmlAttribute
  private Date fechaEje;

  /**
   * campo de fecha de ejecucion - formato AAAA-MM-DD
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_LIQ", nullable = false)
  @XmlAttribute
  private Date fechaLiq;

  /**
   * REGISTRO R03
   */

  /**
   * BIC de la ECC
   */
  @Column(name = "ID_ECC", nullable = false, length = 11)
  @XmlAttribute
  private String idEcc;

  /**
   * Segmento de la ECC
   */
  @Column(name = "SEGMENTO_ECC", nullable = false, length = 2)
  @XmlAttribute
  private String segmentoEcc;

  @XmlAttribute
  @Column(name = "CDISIN", nullable = true, length = 12)
  private String cdisin;

  @XmlAttribute
  @Column(name = "SENTIDO", nullable = true, length = 1)
  private Character sentido;

  @XmlAttribute
  @Column(name = "CDSEGMENTO", nullable = false, length = 2)
  private String cdsegmento;

  @XmlAttribute
  @Column(name = "CDOPERACIONMKT", nullable = false, length = 2)
  private String cdoperacionmkt;

  @XmlAttribute
  @Column(name = "CDMIEMBROMKT", nullable = false, length = 4)
  private String cdmiembromkt;

  @XmlAttribute
  @Column(name = "NUORDENMKT", nullable = false, length = 32)
  private String nuordenmkt;

  @XmlAttribute
  @Column(name = "NUEXEMKT", nullable = false, length = 32)
  private String nuexemkt;

  @XmlAttribute
  @Column(name = "TIPO_CUENTA_LIQUIDACION_IBERCLEAR", nullable = true, length = 2)
  private String tipoCuentaLiquidacionIberclear;

  @XmlAttribute
  @Column(name = "CUENTA_LIQUIDACION", nullable = true, length = 35)
  private String cuentaLiquidacion;

  /**
   * Canon Bursatil Teorico
   */
  @Column(name = "CANON_TEORICO", nullable = false, precision = 15, scale = 2)
  @XmlAttribute
  private BigDecimal canonTeorico;

  /**
   * Canon Bursatil Aplicado
   */
  @Column(name = "CANON_APLICADO", nullable = false, precision = 15, scale = 2)
  @XmlAttribute
  private BigDecimal canonAplicado;

  @XmlAttribute
  @Column(name = "ID_CUADRE_ECC_EJECUCION", nullable = true)
  private Long idCuadreEccEjecucion;

  /**
   * Relacion con tabla ECCMSGINTIAUX
   */
  // @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, targetEntity = ECCMessageInTI_R2.class, mappedBy =
  // "Id")
  @NotAudited
  @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE
  }, fetch = FetchType.LAZY, mappedBy = "cuadreEccReferencia")
  private Set<CuadreEccTitular> titulares = new HashSet<CuadreEccTitular>();

  @Transient
  private List<CuadreEccLog> cuadreEccLog = new ArrayList<CuadreEccLog>();

  // ----------------------------------------------- Bean Constructors

  public CuadreEccReferencia() {
    super();
  }

  // ------------------------------------------- Bean methods: Getters // Setters

  /**
   * Añade una linea a la lista de registro R2
   * 
   * @param R2
   * @return
   */
  public boolean addTitulares(final CuadreEccTitular registroR2) {
    if (CollectionUtils.isEmpty(titulares)) {
      titulares = new HashSet<CuadreEccTitular>();
    }
    registroR2.setCuadreEccReferencia(this);

    return titulares.add(registroR2);

  }

  public String getRefTitular() {
    return refTitular;
  }

  public String getRefAdicional() {
    return refAdicional;
  }

  public String getIndRectificacion() {
    return indRectificacion;
  }

  public BigDecimal getNumValoresImpNominal() {
    return numValoresImpNominal;
  }

  public BigDecimal getNumValoresImpNominalPendienteDesglose() {
    return numValoresImpNominalPendienteDesglose;
  }

  public String getNumOp() {
    return numOp;
  }

  public String getCuentaLiquidacion() {
    return cuentaLiquidacion;
  }

  public String getTipoCuentaLiquidacionIberclear() {
    return tipoCuentaLiquidacionIberclear;
  }

  public Integer getEstado() {
    return estado;
  }

  public BigDecimal getCanonTeorico() {
    return canonTeorico;
  }

  public BigDecimal getCanonAplicado() {
    return canonAplicado;
  }

  public String getCodPostal() {
    return codPostal;
  }

  public String getDomicilio() {
    return domicilio;
  }

  public String getEntComunicadora() {
    return entComunicadora;
  }

  public BigDecimal getFallidoNumValoresImpNominal() {
    return fallidoNumValoresImpNominal;
  }

  public Date getFechaCuadre() {
    return fechaCuadre;
  }

  public BigDecimal getImpEfectivo() {
    return impEfectivo;
  }

  public String getIndCotizacion() {
    return indCotizacion;
  }

  public String getPaisResidencia() {
    return paisResidencia;
  }

  public String getPoblacion() {
    return poblacion;
  }

  public Date getFechaEje() {
    return fechaEje;
  }

  public String getIdEcc() {
    return idEcc;
  }

  public String getSegmentoEcc() {
    return segmentoEcc;
  }

  public String getCdisin() {
    return cdisin;
  }

  public Character getSentido() {
    return sentido;
  }

  public String getCdsegmento() {
    return cdsegmento;
  }

  public String getCdoperacionmkt() {
    return cdoperacionmkt;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  public Long getIdCuadreEccEjecucion() {
    return idCuadreEccEjecucion;
  }

  public Set<CuadreEccTitular> getTitulares() {
    return titulares;
  }

  public void setRefTitular(String refTitular) {
    this.refTitular = refTitular;
  }

  public void setRefAdicional(String refAdicional) {
    this.refAdicional = refAdicional;
  }

  public void setIndRectificacion(String indRectificacion) {
    this.indRectificacion = indRectificacion;
  }

  public void setNumValoresImpNominal(BigDecimal numValoresImpNominal) {
    this.numValoresImpNominal = numValoresImpNominal;
  }

  public void setNumValoresImpNominalPendienteDesglose(BigDecimal numValoresImpNominalPendienteDesglose) {
    this.numValoresImpNominalPendienteDesglose = numValoresImpNominalPendienteDesglose;
  }

  public void setNumOp(String numOp) {
    this.numOp = numOp;
  }

  public void setCuentaLiquidacion(String cuentaLiquidacion) {
    this.cuentaLiquidacion = cuentaLiquidacion;
  }

  public void setTipoCuentaLiquidacionIberclear(String tipoCuentaLiquidacionIberclear) {
    this.tipoCuentaLiquidacionIberclear = tipoCuentaLiquidacionIberclear;
  }

  public void setEstado(Integer estado) {
    this.estado = estado;
  }

  public void setCanonTeorico(BigDecimal canonTeorico) {
    this.canonTeorico = canonTeorico;
  }

  public void setCanonAplicado(BigDecimal canonAplicado) {
    this.canonAplicado = canonAplicado;
  }

  public void setCodPostal(String codPostal) {
    this.codPostal = codPostal;
  }

  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }

  public void setEntComunicadora(String entComunicadora) {
    this.entComunicadora = entComunicadora;
  }

  public void setFallidoNumValoresImpNominal(BigDecimal fallidoNumValoresImpNominal) {
    this.fallidoNumValoresImpNominal = fallidoNumValoresImpNominal;
  }

  public void setFechaCuadre(Date fechaCuadre) {
    this.fechaCuadre = fechaCuadre;
  }

  public void setImpEfectivo(BigDecimal impEfectivo) {
    this.impEfectivo = impEfectivo;
  }

  public void setIndCotizacion(String indCotizacion) {
    this.indCotizacion = indCotizacion;
  }

  public void setPaisResidencia(String paisResidencia) {
    this.paisResidencia = paisResidencia;
  }

  public void setPoblacion(String poblacion) {
    this.poblacion = poblacion;
  }

  public void setFechaEje(Date fechaEje) {
    this.fechaEje = fechaEje;
  }

  public void setIdEcc(String idEcc) {
    this.idEcc = idEcc;
  }

  public void setSegmentoEcc(String segmentoEcc) {
    this.segmentoEcc = segmentoEcc;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  public void setCdoperacionmkt(String cdoperacionmkt) {
    this.cdoperacionmkt = cdoperacionmkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  public void setIdCuadreEccEjecucion(Long idCuadreEccEjecucion) {
    this.idCuadreEccEjecucion = idCuadreEccEjecucion;
  }

  public void setTitulares(Set<CuadreEccTitular> titulares) {
    this.titulares = titulares;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fechaEje == null) ? 0 : fechaEje.hashCode());
    result = prime * result + ((numOp == null) ? 0 : numOp.hashCode());
    result = prime * result + ((refTitular == null) ? 0 : refTitular.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CuadreEccReferencia other = (CuadreEccReferencia) obj;
    if (fechaEje == null) {
      if (other.fechaEje != null)
        return false;
    } else if (!fechaEje.equals(other.fechaEje))
      return false;
    if (numOp == null) {
      if (other.numOp != null)
        return false;
    } else if (!numOp.equals(other.numOp))
      return false;
    if (refTitular == null) {
      if (other.refTitular != null)
        return false;
    } else if (!refTitular.equals(other.refTitular))
      return false;
    return true;
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public Date getFechaLiq() {
    return fechaLiq;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setFechaLiq(Date fechaLiq) {
    this.fechaLiq = fechaLiq;
  }

  public List<CuadreEccLog> getCuadreEccLog() {
    return cuadreEccLog;
  }

  public void setEstadoProcesado(Integer estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setCuadreEccLog(List<CuadreEccLog> cuadreEccLog) {
    this.cuadreEccLog = cuadreEccLog;
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "[ID - TI - CuadreEccReferencia==" + this.id + ", FECHA_EJE==" + this.fechaEje + ", NUM_OP==" + this.numOp
           + ", SENTIDO==" + this.sentido + ", REF_TITULAR==" + this.refTitular + ", REF_ADICIONAL=="
           + this.refAdicional + " ]";

  }
}
