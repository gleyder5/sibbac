package sibbac.business.comunicaciones.euroccp.realigment.dto;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterTypeEnum;
import sibbac.business.comunicaciones.euroccp.realigment.model.EuroCcpExecutionRealigment;

public class EuroCcpExecutionRealigmentDTO extends EuroCcpCommonSendRegisterDTO {

  /**
   * 
   */
  private static final long serialVersionUID = 1245983863524074126L;

  private Date tradeDate;

  private String executionReference;

  private String mic;

  private String accountNumberFrom;

  private String accountNumberTo;

  private BigDecimal numberShares;

  private Character processingStatus;

  private String errorCode;

  private String errorMessage;

  private String filler;

  public EuroCcpExecutionRealigmentDTO() {
    super(EuroCcpCommonSendRegisterTypeEnum.RECORD);
  }

  public EuroCcpExecutionRealigmentDTO(EuroCcpExecutionRealigment hb) {
    this();
    setTradeDate(hb.getTradeDate());
    setExecutionReference(hb.getExecutionReference());
    setMic(hb.getMic());
    setAccountNumberFrom(hb.getAccountNumberFrom().substring(hb.getAccountNumberFrom().length() - 4));
    setAccountNumberTo(hb.getAccountNumberTo().substring(hb.getAccountNumberTo().length() - 4));
    setNumberShares(hb.getNumberShares());
    setProcessingStatus(hb.getProcessingStatus());
    if (hb.getErrorCode() != null) {
      setErrorCode(hb.getErrorCode().getErrorCode());
      setErrorMessage(hb.getErrorCode().getErrorMessage());
    }

  }

  public Date getTradeDate() {
    return tradeDate;
  }

  public String getExecutionReference() {
    return executionReference;
  }

  public String getMic() {
    return mic;
  }

  public String getAccountNumberFrom() {
    return accountNumberFrom;
  }

  public String getAccountNumberTo() {
    return accountNumberTo;
  }

  public BigDecimal getNumberShares() {
    return numberShares;
  }

  public Character getProcessingStatus() {
    return processingStatus;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public String getFiller() {
    return filler;
  }

  public void setTradeDate(Date tradeDate) {
    this.tradeDate = tradeDate;
  }

  public void setExecutionReference(String executionReference) {
    this.executionReference = executionReference;
  }

  public void setMic(String mic) {
    this.mic = mic;
  }

  public void setAccountNumberFrom(String accountNumberFrom) {
    this.accountNumberFrom = accountNumberFrom;
  }

  public void setAccountNumberTo(String accountNumberTo) {
    this.accountNumberTo = accountNumberTo;
  }

  public void setNumberShares(BigDecimal numberShares) {
    this.numberShares = numberShares;
  }

  public void setProcessingStatus(Character processingStatus) {
    this.processingStatus = processingStatus;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String toString() {
    return "EuroCcpExecutionRealigmentDTO [tradeDate=" + tradeDate + ", executionReference=" + executionReference
        + ", mic=" + mic + ", accountNumberFrom=" + accountNumberFrom + ", accountNumberTo=" + accountNumberTo
        + ", numberShares=" + numberShares + ", processingStatus=" + processingStatus + ", errorCode=" + errorCode
        + ", errorMessage=" + errorMessage + ", filler=" + filler + "]";
  }
  
  

}
