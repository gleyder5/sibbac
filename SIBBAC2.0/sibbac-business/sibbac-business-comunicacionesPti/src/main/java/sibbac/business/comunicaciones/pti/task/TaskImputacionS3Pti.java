package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.imputacions3.bo.IManualImputacionS3Pti;
import sibbac.business.comunicaciones.imputacions3.bo.IManualImputacionS3PtiMab;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_IMPUTACION_S3_PTI, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 50-59 13-17 ? * MON-FRI")
public class TaskImputacionS3Pti extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private IManualImputacionS3Pti manualImputacionS3Pti;
  
  @Autowired
  private IManualImputacionS3PtiMab manualImputacionS3PtiMab;

  @Override
  public void executeTask() throws Exception {
    manualImputacionS3Pti.executeTask();
    manualImputacionS3PtiMab.executeTask();

  }

  @Override
  public TIPO determinarTipoApunte() {
    return manualImputacionS3Pti.determinarTipoApunte();
  }

}
