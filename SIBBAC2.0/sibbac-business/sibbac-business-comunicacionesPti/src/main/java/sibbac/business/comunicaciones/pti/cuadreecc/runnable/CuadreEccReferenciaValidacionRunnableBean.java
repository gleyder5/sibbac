package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccReferenciaBo;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccValidacionReferenciasEccRunnableBean")
public class CuadreEccReferenciaValidacionRunnableBean extends IRunnableBean<CuadreEccIdDTO> {

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  public CuadreEccReferenciaValidacionRunnableBean() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<CuadreEccIdDTO> beans, int order) throws SIBBACBusinessException {
    try {
      cuadreEccReferenciaBo.validarCuadreEccReferencias(beans);
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException("INCIDENCIA- orden: " + order + " beans: " + beans, e);
    } finally {
      getObserver().sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<CuadreEccIdDTO> clone() {
    return this;
  }

}
