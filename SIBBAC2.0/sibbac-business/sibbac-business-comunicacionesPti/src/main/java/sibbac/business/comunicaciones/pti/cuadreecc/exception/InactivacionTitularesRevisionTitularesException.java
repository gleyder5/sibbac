package sibbac.business.comunicaciones.pti.cuadreecc.exception;

import sibbac.common.SIBBACBusinessException;

public class InactivacionTitularesRevisionTitularesException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = 8976378822532047886L;

  public InactivacionTitularesRevisionTitularesException() {
    // TODO Auto-generated constructor stub
  }

  public InactivacionTitularesRevisionTitularesException(String message,
                                                         Throwable cause,
                                                         boolean enableSuppression,
                                                         boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

  public InactivacionTitularesRevisionTitularesException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public InactivacionTitularesRevisionTitularesException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public InactivacionTitularesRevisionTitularesException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
