package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccSibbacCargaOperacionesTraspasadasRunnableBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccSibbacCargaOperacionesTraspasadasDbReader")
public class CuadreEccSibbacCargaOperacionesTraspasadasDbReader extends
    WrapperMultiThreadedDbReader<CuadreEccDbReaderRecordBeanDTO> {
  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CuadreEccSibbacCargaOperacionesTraspasadasDbReader.class);

  @Value("${sibbac.cuadre.ecc.max.reg.process.in.execution:100000}")
  private int maxRegToProcess;

  @Value("${sibbac.cuadre.ecc.carga.codigos.op.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadre.ecc.carga.codigos.op.thread.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Qualifier(value = "cuadreEccSibbacCargaOperacionesTraspasadasRunnableBean")
  @Autowired
  private CuadreEccSibbacCargaOperacionesTraspasadasRunnableBean cuadreEccSibbacCargaOperacionesTraspasadasRunnableBean;

  public CuadreEccSibbacCargaOperacionesTraspasadasDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<CuadreEccDbReaderRecordBeanDTO> getBeanToProcessBlock() {
    return cuadreEccSibbacCargaOperacionesTraspasadasRunnableBean;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasDbReader :: preExecuteTask] inicio");
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasDbReader :: preExecuteTask] buscando operaciones a procesar...");
    // FIXME query que devuelve id's de cuadre ecc sibbac, problema de
    // concurrencia debe devolver o los datos de mercado
    // o los allos a tratar
    // concurrencia al actualizar la ejecucion y al toquetear los desgloses y
    // movimientos
    List<CuadreEccDbReaderRecordBeanDTO> beans = cuadreEccSibbacBo
        .findAllCuadreEccIdDTOForCargaOperacionesSibbacTask(maxRegToProcess);

    LOG.debug(
        "[CuadreEccSibbacCargaOperacionesTraspasadasDbReader :: preExecuteTask] encontradas {} operaciones a procesar.",
        beans.size());
    beanIterator = beans.iterator();
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasDbReader :: preExecuteTask] fin");
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
