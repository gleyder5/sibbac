package sibbac.business.comunicaciones.ordenes.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.SalesTraderDao;
import sibbac.business.fase0.database.model.SalesTrader;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0brk;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;

public class CachedValuesLoadXmlOrdenDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 6276308012570185576L;

  private boolean isScriptDividendInstance;

  private final boolean isLoadInternacional;

  private final String aliasScriptDividend;

  private final String tpopbolSinEcc;

  private final Map<String, String> settlementExchangeCdmercad;

  private final Map<String, String> settlementExchangeCdclsmdo;

  private final Map<String, String> cdbrokerCdmiembro;

  private final Map<String, String> darkPoolMicToMic;

  private final Map<String, String> clearingPlatformClearingPlatform;

  private final Map<String, Boolean> isCdbrokerMtf;
  private final Map<String, Boolean> existBroker;

  private final Map<String, String> descripcionAlias;

  private final Map<String, Character> retailSpecialOp;

  private final Map<String, List<Holder>> holders;

  private final Map<String, Character> disociar;

  private final Map<String, Boolean> existsSalesTrader;

  public CachedValuesLoadXmlOrdenDTO(boolean isScriptDividendInstance, boolean isLoadInternacional,
      String aliasScriptDividend, String tpopbolSinEcc) {
    this.isScriptDividendInstance = isScriptDividendInstance;
    this.isLoadInternacional = isLoadInternacional;
    this.aliasScriptDividend = aliasScriptDividend;

    this.tpopbolSinEcc = tpopbolSinEcc;

    // this.settlementExchangeCdmercad = new ConcurrentHashMap<String,
    // String>();
    // this.settlementExchangeCdclsmdo = new ConcurrentHashMap<String,
    // String>();
    // this.isCdbrokerMtf = new ConcurrentHashMap<String, Boolean>();
    // this.cdbrokerCdmiembro = new ConcurrentHashMap<String, String>();
    // this.descripcionAlias = new ConcurrentHashMap<String, String>();
    // this.retailSpecialOp = new ConcurrentHashMap<String, Character>();
    // this.holders = new ConcurrentHashMap<String, List<Holder>>();
    // this.disociar = new ConcurrentHashMap<String, Character>();

    this.settlementExchangeCdmercad = new HashMap<String, String>();
    this.settlementExchangeCdclsmdo = new HashMap<String, String>();
    this.clearingPlatformClearingPlatform = new HashMap<String, String>();
    this.isCdbrokerMtf = new HashMap<String, Boolean>();
    this.cdbrokerCdmiembro = new HashMap<String, String>();
    this.descripcionAlias = new HashMap<String, String>();
    this.retailSpecialOp = new HashMap<String, Character>();
    this.holders = new HashMap<String, List<Holder>>();
    this.disociar = new HashMap<String, Character>();
    this.existBroker = new HashMap<String, Boolean>();
    this.existsSalesTrader = new HashMap<String, Boolean>();
    this.darkPoolMicToMic = new HashMap<String, String>();

  }

  public boolean isScriptDividendInstance() {
    return isScriptDividendInstance;
  }

  public boolean isLoadInternacional() {
    return isLoadInternacional;
  }

  public boolean isAliasScriptDividend(String alias) {
    return aliasScriptDividend != null && aliasScriptDividend.contains(alias.trim());
  }

  public boolean isTpopebolSinEcc(String tpopebol) {
    return tpopbolSinEcc != null && tpopbolSinEcc.contains(tpopebol.trim());
  }

  public String getSettlementExchangeCdmercad(Tmct0xasBo conversiones, String settlementExchange) {
    String cdmercad = null;
    if (settlementExchangeCdmercad.containsKey(settlementExchange.trim())) {
      cdmercad = settlementExchangeCdmercad.get(settlementExchange.trim());
    }
    else {
      synchronized (settlementExchangeCdmercad) {
        cdmercad = settlementExchangeCdmercad.get(settlementExchange.trim());
        if (cdmercad == null) {
          cdmercad = conversiones.getSettlementExchangeCdmercad(settlementExchange);
          if (cdmercad == null || cdmercad.trim().isEmpty()) {
            cdmercad = settlementExchange;
          }
          settlementExchangeCdmercad.put(settlementExchange.trim(), cdmercad);
        }
      }
    }

    return cdmercad;
  }

  public String getSettlementExchangeCdclsmdo(Tmct0xasBo conversiones, String settlementExchange) {
    String cdclsmdo = null;
    if (settlementExchangeCdclsmdo.containsKey(settlementExchange.trim())) {
      cdclsmdo = settlementExchangeCdclsmdo.get(settlementExchange.trim());
    }
    else {
      synchronized (settlementExchangeCdclsmdo) {
        cdclsmdo = settlementExchangeCdclsmdo.get(settlementExchange.trim());
        if (cdclsmdo == null) {
          cdclsmdo = conversiones.getSettlementExchangeCdclsmdo(settlementExchange);
          settlementExchangeCdclsmdo.put(settlementExchange.trim(), cdclsmdo);
        }
      }
    }

    return cdclsmdo;
  }

  public String getCdbrokerCdmiembro(Tmct0xasBo conversiones, String cdbroker) {
    String cdmiembro = null;
    if (cdbrokerCdmiembro.containsKey(cdbroker.trim())) {
      cdmiembro = cdbrokerCdmiembro.get(cdbroker.trim());
    }
    else {
      synchronized (cdbrokerCdmiembro) {
        cdmiembro = cdbrokerCdmiembro.get(cdbroker.trim());
        if (cdmiembro == null) {
          cdmiembro = conversiones.getCdbrokerCdmiembro(cdbroker);
          if (cdmiembro == null || cdmiembro.trim().isEmpty()) {
            cdmiembro = cdbroker;
          }
          cdbrokerCdmiembro.put(cdbroker.trim(), cdmiembro);
        }
      }
    }

    return cdmiembro;
  }

  public String getClearingPlatformClearingPlatform(Tmct0xasBo conversiones, String clearingPlatform) {
    String clearingPlatformResult = null;
    if (this.clearingPlatformClearingPlatform.containsKey(clearingPlatform.trim())) {
      clearingPlatformResult = clearingPlatformClearingPlatform.get(clearingPlatform.trim());
    }
    else {
      synchronized (clearingPlatformClearingPlatform) {
        clearingPlatformResult = clearingPlatformClearingPlatform.get(clearingPlatform.trim());
        if (clearingPlatformResult == null) {
          clearingPlatformResult = conversiones.getClearingPlatformClearingPlatform(clearingPlatform);
          if (clearingPlatformResult == null || clearingPlatformResult.trim().isEmpty()) {
            clearingPlatformResult = clearingPlatform;
          }
          clearingPlatformClearingPlatform.put(clearingPlatform.trim(), clearingPlatformResult);
        }
      }
    }

    return clearingPlatformResult;
  }

  public String getDarkPoolMicToMic(Tmct0xasBo conversiones, String darkPoolMic) {
    String mic = null;
    if (this.darkPoolMicToMic.containsKey(darkPoolMic.trim())) {
      mic = darkPoolMicToMic.get(darkPoolMic.trim());
    }
    else {
      synchronized (darkPoolMicToMic) {
        mic = darkPoolMicToMic.get(darkPoolMic.trim());
        if (mic == null) {
          mic = conversiones.getDarkPoolMicToMic(darkPoolMic);
          if (mic == null || mic.trim().isEmpty()) {
            mic = darkPoolMic;
          }
          darkPoolMicToMic.put(darkPoolMic.trim(), mic);
        }
      }
    }

    return mic;
  }

  private boolean isCdBrokerMtf(Tmct0brkBo brkBo, String value) {
    return brkBo.isCdbrokerMtf(isCdbrokerMtf, value);
  }

  public boolean isMtf(Tmct0brkBo brkBo, String tradingVenue, String executionTradingVenue) {
    return tradingVenue != null && isCdBrokerMtf(brkBo, tradingVenue) || executionTradingVenue != null
        && isCdBrokerMtf(brkBo, executionTradingVenue);
  }

  public boolean existBroker(Tmct0brkBo brkBo, String cdbroker) {
    if (cdbroker != null) {
      String key = cdbroker.trim();
      Boolean res = this.existBroker.get(key);
      if (res == null) {
        synchronized (this.existBroker) {
          res = this.existBroker.get(key);
          if (res == null) {
            Tmct0brk brk = brkBo.findByCdbroker(cdbroker);
            res = brk != null;
            this.existBroker.put(key, res);
          }
        }
      }
      return res;
    }
    else {
      return false;
    }

  }

  public void setScriptDividendInstance(boolean isScriptDividendInstance) {
    this.isScriptDividendInstance = isScriptDividendInstance;
  }

  public List<Holder> getHolders(Tmct0tfiBo tfiBo, String cdholder, BigDecimal fetrade) {
    List<Holder> listHolders = null;
    String key = String.format("%s%s", cdholder.trim(), fetrade.toString());
    if (holders.containsKey(key)) {
      listHolders = holders.get(key);
    }
    else {
      synchronized (holders) {
        listHolders = holders.get(key);
        if (listHolders == null) {
          listHolders = tfiBo.findAllHoldersByCdholder(cdholder, fetrade);
          holders.put(key, listHolders);
        }
      }
    }
    return listHolders;
  }

  private synchronized void loadAlias(Tmct0aliBo aliBo, String cdalias) {
    Tmct0ali ali = aliBo.findByFirstAliasOrder(cdalias);
    if (ali != null) {
      descripcionAlias.put(cdalias.trim(), ali.getDescrali() == null ? "" : ali.getDescrali().trim());
      retailSpecialOp.put(cdalias.trim(), ali.getRetailSpecialOp() == null ? 'N' : ali.getRetailSpecialOp());
      disociar.put(cdalias.trim(), ali.getDisociar() == null ? 'N' : ali.getDisociar());
    }
    else {
      descripcionAlias.put(cdalias.trim(), "");
      retailSpecialOp.put(cdalias.trim(), ' ');
      disociar.put(cdalias.trim(), ' ');
    }
  }

  public String getDescrali(Tmct0aliBo aliBo, String cdalias) {
    String descrali = null;
    if (descripcionAlias.containsKey(cdalias.trim())) {
      descrali = descripcionAlias.get(cdalias.trim());
    }
    else {
      synchronized (descripcionAlias) {
        descrali = descripcionAlias.get(cdalias.trim());
        if (descrali == null) {
          loadAlias(aliBo, cdalias);
          descrali = descripcionAlias.get(cdalias.trim());
        }
      }
    }
    return descrali;
  }

  public Character getDisociar(Tmct0aliBo aliBo, String cdalias) {
    Character disociarRes = null;
    if (disociar.containsKey(cdalias.trim())) {
      disociarRes = disociar.get(cdalias.trim());
    }
    else {
      synchronized (disociar) {
        disociarRes = disociar.get(cdalias.trim());
        if (disociarRes == null) {
          loadAlias(aliBo, cdalias);
          disociarRes = disociar.get(cdalias.trim());
        }
      }
    }
    return disociarRes;
  }

  public String getDescraliSubcuenta(Tmct0actBo actBo, String cuenta, String subcuenta, Integer fecha) {
    String key = String.format("%s%s%s", cuenta.trim(), subcuenta.trim(), fecha.toString());
    String descrali = null;
    if (descripcionAlias.containsKey(key)) {
      descrali = descripcionAlias.get(key);
    }
    else {
      synchronized (descripcionAlias) {
        descrali = descripcionAlias.get(key);
        if (descrali == null) {
          List<Tmct0act> listActs = actBo.findByCdaliassAndCdsubctaAndFecha(cuenta, subcuenta, fecha);
          if (listActs != null && !listActs.isEmpty()) {
            descrali = listActs.get(0).getDescrali().trim();
            descripcionAlias.put(key, descrali);
          }
          else {
            descrali = "";
            descripcionAlias.put(key, descrali);
          }

        }
      }
    }
    return descrali;
  }

  public Character getRetailSpecialOp(Tmct0aliBo aliBo, String cdalias) {
    Character retailScpecialOp = null;
    if (retailSpecialOp.containsKey(cdalias.trim())) {
      retailScpecialOp = retailSpecialOp.get(cdalias.trim());
    }
    else {
      synchronized (retailSpecialOp) {
        retailScpecialOp = retailSpecialOp.get(cdalias.trim());
        if (retailScpecialOp == null) {
          loadAlias(aliBo, cdalias);
          retailScpecialOp = retailSpecialOp.get(cdalias.trim());
        }
      }
    }
    return retailScpecialOp;
  }

  public boolean existsSalesTrade(SalesTraderDao dao, int codigoCorto, Date fecha) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String key = String.format("%s_%s", codigoCorto, sdf.format(fecha));

    Boolean res = this.existsSalesTrader.get(key);
    if (res == null) {
      synchronized (this.existsSalesTrader) {
        res = this.existsSalesTrader.get(key);
        if (res == null) {
          List<SalesTrader> l = dao.findAllByCodigoCortoAndFecha(codigoCorto, fecha);
          res = l != null && !l.isEmpty();
          this.existsSalesTrader.put(key, res);
        }
      }
    }
    return res;
  }

  public void clearCache() {
    settlementExchangeCdmercad.clear();
    settlementExchangeCdclsmdo.clear();
    isCdbrokerMtf.clear();
    cdbrokerCdmiembro.clear();
    descripcionAlias.clear();
    retailSpecialOp.clear();
    holders.clear();
    disociar.clear();
    this.clearingPlatformClearingPlatform.clear();
    this.existBroker.clear();
    this.existsSalesTrader.clear();
    darkPoolMicToMic.clear();
  }

}
