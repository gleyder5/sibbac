package sibbac.business.comunicaciones.euroccp.titulares.mapper;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;

import sibbac.business.wrappers.tasks.thread.WrapperFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public class EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator<T> extends FormatterLineAggregator<T> {

  private WrapperFieldSetMapper<T> fielSetMapper;

  public EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator() {
  }

  public EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator(WrapperFieldSetMapper<T> fieldSetMapper) {
    this();
    this.fielSetMapper = fieldSetMapper;
  }

  @Override
  protected String doAggregate(Object[] fields) {
    int i = 0;
    Object value;
    String valueAsSt;
    for (WrapperFileReaderFieldEnumInterface field : this.fielSetMapper.getFields()) {
      value = fields[i];
      if (value != null) {
        if (field.getFielType() == WrapperFileReaderFieldType.NUMERIC
            || field.getFielType() == WrapperFileReaderFieldType.DATE
            || field.getFielType() == WrapperFileReaderFieldType.TIME
            || field.getFielType() == WrapperFileReaderFieldType.TIMESTAMP) {
          if (field.getFielType() == WrapperFileReaderFieldType.NUMERIC) {
            BigDecimal valueBd = new BigDecimal(value.toString());
            valueBd = valueBd.movePointRight(field.getScale());
            valueBd = valueBd.setScale(0);
            valueAsSt = StringUtils.leftPad(valueBd.toString(), field.getLength(), '0');
            value = valueAsSt;
          } else if (field.getFielType() == WrapperFileReaderFieldType.DATE) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            valueAsSt = sdf.format(value);
            valueAsSt = StringUtils.leftPad(valueAsSt, field.getLength(), '0');
            value = valueAsSt;
          } else if (field.getFielType() == WrapperFileReaderFieldType.TIME) {
            SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
            valueAsSt = sdf.format(value);
            valueAsSt = StringUtils.leftPad(valueAsSt, field.getLength(), '0');
            value = valueAsSt;
          } else if (field.getFielType() == WrapperFileReaderFieldType.TIMESTAMP) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            valueAsSt = sdf.format(value);
            valueAsSt = StringUtils.leftPad(valueAsSt, field.getLength(), '0');
            value = valueAsSt;
          }
        }
        fields[i] = value;
      }

      i++;
    }
    return super.doAggregate(fields);
  }
}
