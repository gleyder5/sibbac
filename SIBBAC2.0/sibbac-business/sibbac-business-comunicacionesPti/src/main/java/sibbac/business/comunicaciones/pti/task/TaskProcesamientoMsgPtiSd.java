package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.msgprocess.bo.PtiMessageProcessBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.pti.PTIMessageFactory;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_PROCESAMIENTO_MENSAJES_PTI_SD, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-22 ? * MON-FRI")
public class TaskProcesamientoMsgPtiSd extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskProcesamientoMsgPtiSd.class);

  @Autowired
  private PtiMessageProcessBo ptiMessageProcess;

  @Autowired
  private PtiCommomns ptiCommons;

  public TaskProcesamientoMsgPtiSd() {
  }

  @Override
  public void executeTask() throws Exception {
    LOG.debug("[executeTask] inicio.");
    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }
    LOG.debug("[executeTask] Procesando msg pti...");
    ptiMessageProcess.executeTask(true);
    LOG.debug("[executeTask] fin.");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_PROCESAMIENTO_MENSAJES_PTI_SD;
  }

}
