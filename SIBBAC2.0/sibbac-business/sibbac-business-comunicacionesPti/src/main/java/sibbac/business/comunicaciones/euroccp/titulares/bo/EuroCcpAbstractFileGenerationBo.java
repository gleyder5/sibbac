package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.PathResource;
import org.springframework.mail.MailSendException;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileSentDao;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileTypeDao;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpOwnershipReferenceStaticDataDao;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReferenceStaticData;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.tasks.thread.WrapperFieldSetMapper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;

public abstract class EuroCcpAbstractFileGenerationBo<T, K> {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpAbstractFileGenerationBo.class);

  @Autowired
  protected EuroCcpFileSentDao euroCcpFileSentDao;

  @Autowired
  protected EuroCcpFileSentBo euroCcpFileSentBo;

  @Autowired
  protected Tmct0cfgBo cfgBo;

  @Autowired
  protected Tmct0estadoBo estBo;

  @Autowired
  protected Tmct0logBo logBo;

  @Autowired
  protected EuroCcpFileTypeDao euroCcpFileTypeDao;

  @Autowired
  protected EuroCcpFileSentDao euroCcpSentFileDao;

  @Autowired
  protected EuroCcpOwnershipReferenceStaticDataDao euroCcpFileSentLineDao;

  @Autowired
  private SendMail sendMail;

  public EuroCcpAbstractFileGenerationBo() {
  }

  protected abstract void processBeansAndWriteTmpFile(Path tmpPath, String tmpFileName, Date sentDate, List<T> beans,
      Map<String, FlatFileItemWriter<K>> mapFicherosByClientNumber,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosPrincipal,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosTmp, int lastClientNumberPositions, Charset charset)
      throws SIBBACBusinessException;

  public abstract void updateEstadoProcesado(EuroCcpFileSent principalFile, int estadoProcesado, String auditUser);

  protected abstract EuroCcpFileTypeEnum getPrincipalFileType();

  protected abstract EuroCcpFileTypeEnum getTmpFileType();

  protected abstract boolean getWriteFooterPrincipalFile();

  /**
   * @param workPathSt
   * @param tmpPathSt
   * @param pendientesFolder
   * @param enviadosFolder
   * @param tratadosFolder
   * @param fileCharset
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void appendTmpFiles(String workPathSt, String tmpPathSt, String pendientesFolder, String enviadosFolder,
      String tratadosFolder, String compressExtension, Charset fileCharset, TimeZone timeZone)
      throws SIBBACBusinessException {

    Path workPath = Paths.get(workPathSt);
    Path tmpPath = Paths.get(workPathSt.toString(), tmpPathSt);
    Path pendientesPath = Paths.get(workPathSt.toString(), pendientesFolder);
    Path enviadosPath = Paths.get(workPathSt.toString(), enviadosFolder);
    Path tratadosPath = Paths.get(workPathSt.toString(), tratadosFolder);
    LOG.trace("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] check paths...");
    this.checkPaths(workPath, tmpPath, pendientesPath, enviadosPath, tratadosPath);

    List<EuroCcpFileSent> principalFilesNotSent = euroCcpSentFileDao.findAllByEstadoProcesadoAndFileType(
        EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId(), getPrincipalFileType().name());
    List<EuroCcpFileSent> txtsSinEnviar;
    for (EuroCcpFileSent principalFileRegister : principalFilesNotSent) {
      txtsSinEnviar = euroCcpSentFileDao.findAllByIdParentFileAndEstadoProcesado(principalFileRegister.getId(),
          EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
      if (CollectionUtils.isNotEmpty(txtsSinEnviar)) {

        try {
          LOG.trace("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] append tmp files...");
          this.appendTmpFiles(principalFileRegister, txtsSinEnviar, tmpPath, tratadosPath, pendientesPath,
              compressExtension, fileCharset, getWriteFooterPrincipalFile(), timeZone);

        }
        catch (IOException e) {
          throw new SIBBACBusinessException(e);
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e);
        }
      }
      else {
        LOG.warn("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] el fichero: {} no tiene txt's.",
            principalFileRegister.getFileName());
      }
    }

  }

  /**
   * @param workPathSt
   * @param tmpFolder
   * @param pendientesFolder
   * @param enviadosFolder
   * @param tratadosFolder
   * @param to
   * @param cc
   * @param subject
   * @param body
   * @param fileCharset
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void enviarFilesByEmail(String workPathSt, String tmpFolder, String pendientesFolder, String enviadosFolder,
      String tratadosFolder, List<String> to, List<String> cc, String subject, String body)
      throws SIBBACBusinessException {

    Path workPath = Paths.get(workPathSt);
    Path tmpPath = Paths.get(workPathSt.toString(), tmpFolder);
    Path pendientesPath = Paths.get(workPathSt.toString(), pendientesFolder);
    Path enviadosPath = Paths.get(workPathSt.toString(), enviadosFolder);
    Path tratadosPath = Paths.get(workPathSt.toString(), tratadosFolder);
    LOG.trace("[EuroCcpOwnershipReferenceStaticDataFileWriterBo :: enviarFilesByEmail] check paths...");
    this.checkPaths(workPath, tmpPath, pendientesPath, enviadosPath, tratadosPath);

    List<EuroCcpFileSent> principalFileSinEnviar = euroCcpSentFileDao.findAllByEstadoProcesadoAndFileType(
        EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId(), getPrincipalFileType().name());
    for (EuroCcpFileSent principalFileReg : principalFileSinEnviar) {
      try {
        LOG.info("[EuroCcpOwnershipReferenceStaticDataFileWriterBo :: enviarFilesByEmail] enviando mail fichero {}...",
            principalFileReg.getFilePath());
        this.enviarFicheroEuroccpByEmail(principalFileReg, enviadosPath, to, cc, subject, body);

      }
      catch (IOException e) {
        throw new SIBBACBusinessException(e);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e);
      }

    }

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void creacionFicheroTmp(List<T> beans, Path tmpPath, String tmpFileName, int lastClientNumberPositions,
      Charset charset) throws SIBBACBusinessException {

    String msg;

    Date sentDate = new Date();
    EuroCcpFileType fileType = null;
    List<EuroCcpFileSent> principalFiles;

    if (CollectionUtils.isNotEmpty(beans)) {
      Map<String, FlatFileItemWriter<K>> mapFicherosByClientNumber = new HashMap<String, FlatFileItemWriter<K>>();
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosPrincipal = new HashMap<String, EuroCcpFileSent>();
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosTmp = new HashMap<String, EuroCcpFileSent>();
      try {

        LOG.debug(
            "[EuroCcpAbstractFileGenerationBo :: crearFicheroTxtExcelGeneracionTitulares] buscando reg. fichero principal fecha: {} - Type: {}...",
            sentDate, getPrincipalFileType());
        principalFiles = euroCcpFileSentDao.findAllBySentDateAndFileTypeAndEstadoProcesado(sentDate,
            getPrincipalFileType().name(), ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());
        if (CollectionUtils.isEmpty(principalFiles)) {
          throw new SIBBACBusinessException("INCIDENCIA- No hay ningun registro principal file sent creado. Date: "
              + sentDate + " type: " + getPrincipalFileType());
        }
        else {
          for (EuroCcpFileSent excelFileSentFor : principalFiles) {
            if (mapRegistrosFicherosEnviadosPrincipal.get(excelFileSentFor.getClientNumber()) != null) {
              msg = MessageFormat
                  .format(
                      "INCIDENCIA- Mas de un reg principal file sent disponible para el envio. ClientNumber: {0} Date: {1} type: {2}",
                      excelFileSentFor.getClientNumber(), sentDate, getPrincipalFileType());
              throw new SIBBACBusinessException(msg);
            }
            mapRegistrosFicherosEnviadosPrincipal.put(excelFileSentFor.getClientNumber(), excelFileSentFor);
          }
        }

        LOG.debug(
            "[EuroCcpAbstractFileGenerationBo :: crearFicheroTxtExcelGeneracionTitulares] Buscando file type: {}",
            getTmpFileType());
        fileType = euroCcpFileTypeDao.findByCdCodigo(getTmpFileType().name());

        if (fileType == null) {
          throw new SIBBACBusinessException("INCIDENCIA- File type no encontrado en tabla: " + getTmpFileType());
        }

        this.processBeansAndWriteTmpFile(tmpPath, tmpFileName, sentDate, beans, mapFicherosByClientNumber,
            mapRegistrosFicherosEnviadosPrincipal, mapRegistrosFicherosEnviadosTmp, lastClientNumberPositions, charset);

      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e);
      }
      finally {
        for (FlatFileItemWriter<K> euroCcpOwnershipReferenceStaticDataDTO : mapFicherosByClientNumber.values()) {
          LOG.debug("[EuroCcpAbstractFileGenerationBo :: crearFicheroTxtExcelGeneracionTitulares] cerrando excel-txt writer...");
          euroCcpOwnershipReferenceStaticDataDTO.close();
        }

        mapFicherosByClientNumber.clear();
        mapRegistrosFicherosEnviadosPrincipal.clear();
        mapRegistrosFicherosEnviadosTmp.clear();
      }
    }
  }

  /**
   * @param file
   * @param charset
   * @return
   */
  protected FlatFileItemWriter<K> getNewWriter(Path file, Charset charset, WrapperFieldSetMapper<K> mapper) {
    FlatFileItemWriter<K> writer = new FlatFileItemWriter<K>();
    writer.setEncoding(charset.name());
    writer.setForceSync(true);
    writer.setAppendAllowed(false);
    writer.setShouldDeleteIfEmpty(true);
    writer.setShouldDeleteIfExists(false);
    writer.setTransactional(true);
    writer.setName(file.toString());

    writer.setLineAggregator(mapper.getLineAggregator());

    // FormatterLineAggregator<K> lineAggregator = new
    // FormatterLineAggregator<K>();
    // lineAggregator.setFormat(mapper.getFormat());
    // BeanWrapperFieldExtractor<K> fieldExtractor = new
    // BeanWrapperFieldExtractor<K>();
    // fieldExtractor.setNames(mapper.getFieldNames());
    // lineAggregator.setFieldExtractor(fieldExtractor);
    // writer.setLineAggregator(lineAggregator);

    writer.setResource(new PathResource(file));
    writer.open(new ExecutionContext());
    // TODO se puede poner un footer
    // writer.setFooterCallback(footerCallback);
    return writer;
  }

  /**
   * @param principalFileRegister
   * @param appendTmpFile
   * @param appendCompressTmpFile
   * @param excelPendienteEnviarFile
   * @param compress
   * @param compressExtension
   * @param fileCharset
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void comprimirPrincipalFileAndMoverPdteEnviar(EuroCcpFileSent principalFileRegister, Path appendTmpFile,
      Path appendCompressTmpFile, Path excelPendienteEnviarFile, boolean compress, String compressExtension,
      Charset fileCharset) throws SIBBACBusinessException {
    try {
      if (Files.exists(appendTmpFile) && Files.size(appendTmpFile) > 0) {
        LOG.debug(
            "[EuroCcpAbstractFileGenerationBo :: comprimirPrincipalFileAndMoverPdteEnviar] moviendo fichero de: {} a {} ...",
            appendTmpFile.toString(), excelPendienteEnviarFile.toString());
        if (compress) {
          LOG.debug("[EuroCcpAbstractFileGenerationBo :: comprimirPrincipalFileAndMoverPdteEnviar] Comprimiendo fichero pendiente enviar...");
          byte[] buffer = new byte[1024];
          try (InputStream is = Files.newInputStream(appendTmpFile, StandardOpenOption.READ);
              OutputStream os = Files.newOutputStream(appendCompressTmpFile, StandardOpenOption.CREATE_NEW,
                  StandardOpenOption.WRITE);) {
            if (compressExtension.equalsIgnoreCase("ZIP")) {
              ZipOutputStream zos = null;
              try {
                zos = new ZipOutputStream(os, fileCharset);
                ZipEntry entry = new ZipEntry(principalFileRegister.getFileName());
                zos.putNextEntry(entry);
                IOUtils.copyLarge(is, zos, buffer);
                zos.closeEntry();
              }
              catch (IOException e) {
                throw e;
              }
              finally {
                if (zos != null) {
                  zos.close();
                }
              }
            }
            else {
              GZIPOutputStream gzos = null;
              try {
                gzos = new GZIPOutputStream(os);
                IOUtils.copyLarge(is, gzos, buffer);
              }
              catch (IOException e) {
                throw e;
              }
              finally {
                if (gzos != null) {
                  gzos.close();
                }
              }
            }

          } // autocloseable is, os
          Files.move(appendCompressTmpFile, excelPendienteEnviarFile, StandardCopyOption.ATOMIC_MOVE);
          Files.delete(appendTmpFile);
        }
        else {
          Files.move(appendTmpFile, excelPendienteEnviarFile, StandardCopyOption.ATOMIC_MOVE);
        }
        principalFileRegister.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
        principalFileRegister.setFileName(excelPendienteEnviarFile.getFileName().toString());
        principalFileRegister.setFilePath(excelPendienteEnviarFile.toString());
        principalFileRegister.setAuditDate(new Date());
        principalFileRegister.setAuditUser("EURO_CCP_E");
        principalFileRegister = euroCcpFileSentDao.save(principalFileRegister);
      }
      else {
        LOG.debug(
            "[EuroCcpAbstractFileGenerationBo :: comprimirPrincipalFileAndMoverPdteEnviar] No se ha escrito nada borramos el fichero: {}",
            appendTmpFile.toString());
        Files.delete(appendTmpFile);
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("INCIDENCIA- Moviendo el fichero " + appendTmpFile.toString() + " a "
          + excelPendienteEnviarFile.toString(), e);
    }
  }

  /**
   * @param principalFile
   * @param ficherosTmp
   * @param tratadosPath
   * @param fileCharset
   * @throws IOException
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void comprimirTemporalesAndMoverTratados(EuroCcpFileSent principalFile, List<EuroCcpFileSent> ficherosTmp,
      Path tratadosPath, Charset fileCharset) throws IOException, SIBBACBusinessException {
    byte[] buffer = new byte[1024];

    Path path;
    String tratadosFileName = principalFile.getFileName() + FormatDataUtils.convertDateToConcurrentFileName(new Date())
        + ".zip";
    Path pathOutPut = Paths.get(tratadosPath.toString(), tratadosFileName);

    try (OutputStream os = Files.newOutputStream(pathOutPut, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);) {
      ZipOutputStream zos = null;

      try {
        zos = new ZipOutputStream(os, fileCharset);
        ZipEntry ze = new ZipEntry(principalFile.getFileName().toString());
        zos.putNextEntry(ze);
        path = Paths.get(principalFile.getFilePath());
        LOG.debug("[EuroCcpAbstractFileGenerationBo :: comprimirAndTratados] tratando fichero: {}", path.toString());
        try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);) {
          IOUtils.copyLarge(is, (OutputStream) zos, buffer);
        }
        zos.closeEntry();
        for (EuroCcpFileSent txt : ficherosTmp) {
          path = Paths.get(txt.getFilePath());
          ze = new ZipEntry(path.getFileName().toString());
          zos.putNextEntry(ze);
          LOG.debug("[EuroCcpAbstractFileGenerationBo :: comprimirAndTratados] tratando fichero: {}", path.toString());
          try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);) {
            IOUtils.copyLarge(is, (OutputStream) zos, buffer);
          }

          zos.closeEntry();
          Files.delete(path);

          LOG.debug("[EuroCcpAbstractFileGenerationBo :: comprimirAndTratados] movido de: {} a: {}", path.toString(),
              pathOutPut.toString());

          txt.setFilePath(pathOutPut.toString());
          txt.setAuditDate(new Date());
          txt.setAuditUser("EURO_CCP_E");
          txt = euroCcpFileSentDao.save(txt);
        }// fin for
      }
      catch (Exception e) {
        throw new SIBBACBusinessException("INCIDENCIA-Creando backup de fichero enviado.", e);
      }
      finally {
        if (zos != null) {
          zos.close();
        }
      }
    }// fin autocloseable
  }

  /**
   * @param workPathSt
   * @param tmpPathSt
   * @param pendientesPathSt
   * @param enviadosPathSt
   * @param tratadosPathSt
   * @throws SIBBACBusinessException
   */
  public void createPathsIfNotExists(String workPathSt, String tmpPathSt, String pendientesPathSt,
      String enviadosPathSt, String tratadosPathSt) throws SIBBACBusinessException {
    Path workPath = Paths.get(workPathSt);
    Path tmpPath = Paths.get(workPath.toString(), tmpPathSt);
    Path pendientesPath = Paths.get(workPath.toString(), pendientesPathSt);
    Path enviadosPath = Paths.get(workPath.toString(), enviadosPathSt);
    Path tratadosPath = Paths.get(workPath.toString(), tratadosPathSt);
    this.checkPaths(workPath, tmpPath, pendientesPath, enviadosPath, tratadosPath);
  }

  /**
   * @param workPath
   * @param tmpPath
   * @param pendientesPath
   * @param enviadosPath
   * @param tratadosPath
   * @throws SIBBACBusinessException
   */
  public void checkPaths(Path workPath, Path tmpPath, Path pendientesPath, Path enviadosPath, Path tratadosPath)
      throws SIBBACBusinessException {
    LOG.trace("[EuroCcpAbstractFileGenerationBo :: checkPaths] path: {}", workPath.toString());
    if (Files.notExists(workPath)) {
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: checkPaths] no existe la ruta: {}", workPath.toString());
      try {
        Files.createDirectories(workPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("INCIENCIA- No se puede crear el directorio: " + workPath.toString(), e);
      }
    }

    LOG.trace("[EuroCcpAbstractFileGenerationBo :: checkPaths] path: {}", pendientesPath.toString());
    if (Files.notExists(pendientesPath)) {
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: checkPaths] no existe la ruta: {}", pendientesPath.toString());
      try {
        Files.createDirectories(pendientesPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("INCIENCIA- No se puede crear el directorio: " + pendientesPath.toString(), e);
      }
    }

    LOG.trace("[EuroCcpAbstractFileGenerationBo :: checkPaths] path: {}", tmpPath.toString());
    if (Files.notExists(tmpPath)) {
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: checkPaths] no existe la ruta: {}", tmpPath.toString());
      try {
        Files.createDirectories(tmpPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("INCIENCIA- No se puede crear el directorio: " + tmpPath.toString(), e);
      }
    }

    LOG.trace("[EuroCcpAbstractFileGenerationBo :: checkPaths] path: {}", enviadosPath.toString());
    if (Files.notExists(enviadosPath)) {
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: checkPaths] no existe la ruta: {}", enviadosPath.toString());
      try {
        Files.createDirectories(enviadosPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("INCIENCIA- No se puede crear el directorio: " + enviadosPath.toString(), e);
      }
    }

    LOG.trace("[EuroCcpAbstractFileGenerationBo :: checkPaths] path: {}", tratadosPath.toString());
    if (Files.notExists(tratadosPath)) {
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: checkPaths] no existe la ruta: {}", tratadosPath.toString());
      try {
        Files.createDirectories(tratadosPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("INCIENCIA- No se puede crear el directorio: " + tratadosPath.toString(), e);
      }
    }
  }

  /**
   * @param euroCcpFileSentExcel
   * @param enviadosPath
   * @param to
   * @param cc
   * @param subject
   * @param body
   * @throws IOException
   * @throws IllegalArgumentException
   * @throws MessagingException
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void enviarFicheroEuroccpByEmail(EuroCcpFileSent euroCcpFileSentExcel, Path enviadosPath, List<String> to,
      List<String> cc, String subject, String body) throws IOException, IllegalArgumentException, MessagingException,
      MailSendException, SIBBACBusinessException {

    List<InputStream> path2attach;
    List<String> nameDest;

    Path pathExcelSinEnviar = Paths.get(euroCcpFileSentExcel.getFilePath());
    if (Files.notExists(pathExcelSinEnviar)) {
      throw new SIBBACBusinessException("INCIDENCIA- Fichero fichero no encontrado " + pathExcelSinEnviar.toString());
    }

    LOG.debug("[EuroCcpAbstractFileGenerationBo :: enviarFicheroEuroccpByEmail] Abriendo input stream {}...",
        euroCcpFileSentExcel.toString());
    try (InputStream is = Files.newInputStream(pathExcelSinEnviar, StandardOpenOption.READ);) {
      path2attach = new ArrayList<InputStream>();
      nameDest = new ArrayList<String>();
      path2attach.add(is);
      nameDest.add(pathExcelSinEnviar.getFileName().toString());
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: enviarFicheroEuroccpByEmail] Enviando Email...");
      try {
        sendMail.sendMail(to, subject, body, path2attach, nameDest, cc);
      }
      catch (IllegalArgumentException | MessagingException | MailSendException | IOException e) {
        throw e;
      }
      finally {
        path2attach.clear();
        nameDest.clear();
      }
    }

    this.moverRutaEnvio(euroCcpFileSentExcel, enviadosPath);

    LOG.debug("[EuroCcpAbstractFileGenerationBo :: enviarFicheroEuroccpByEmail] Grabado registro testigo del envio {}",
        euroCcpFileSentExcel);

  }

  /**
   * @param euroCcpFileSentExcel
   * @param outputPath
   * @throws IOException
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void moverRutaEnvio(String outputPathSt) throws SIBBACBusinessException {
    Path outputPath = Paths.get(outputPathSt);
    LOG.trace("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] check paths...");
    if (Files.notExists(outputPath)) {
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: checkPaths] no existe la ruta: {}", outputPath.toString());
      try {
        Files.createDirectories(outputPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("INCIENCIA- No se puede crear el directorio: " + outputPath.toString(), e);
      }
    }

    List<EuroCcpFileSent> principalFilesNotSent = euroCcpSentFileDao.findAllByEstadoProcesadoAndFileType(
        EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId(), getPrincipalFileType().name());
    for (EuroCcpFileSent principalFileRegister : principalFilesNotSent) {

      try {
        this.moverRutaEnvio(principalFileRegister, outputPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException(e);
      }

    }

  }

  /**
   * @param euroCcpPrincipalFile
   * @param outputPath
   * @throws IOException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void moverRutaEnvio(EuroCcpFileSent euroCcpPrincipalFile, Path outputPath) throws IOException {

    Path ficheroSinEnviar = Paths.get(euroCcpPrincipalFile.getFilePath());

    Path ficheroEnviado = Paths.get(outputPath.toString(), euroCcpPrincipalFile.getFileName());

    LOG.debug("[EuroCcpAbstractFileGenerationBo :: moverRutaEnvio] Moviendo fichero {} a {}...",
        ficheroSinEnviar.toString(), ficheroEnviado.toString());
    Files.move(ficheroSinEnviar, ficheroEnviado, StandardCopyOption.ATOMIC_MOVE);
    LOG.debug("[EuroCcpAbstractFileGenerationBo :: moverRutaEnvio] Movido fichero {} a {}. OK.",
        ficheroSinEnviar.toString(), ficheroEnviado.toString());

    euroCcpPrincipalFile.setFileName(ficheroEnviado.getFileName().toString());
    euroCcpPrincipalFile.setFilePath(ficheroEnviado.toString());
    euroCcpPrincipalFile.setAuditDate(new Date());
    euroCcpPrincipalFile.setAuditUser("EURO_CCP_E");
    euroCcpPrincipalFile.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
    euroCcpPrincipalFile = euroCcpFileSentDao.save(euroCcpPrincipalFile);

    updateEstadoProcesado(euroCcpPrincipalFile, EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId(),
        "EURO_CCP_E");

  }

  /**
   * @param tmpPath
   * @param pendientesPath
   * @param tmpFileNamePattern
   * @param excelFinalFileName
   * @throws SIBBACBusinessException
   * @throws IOException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void appendTmpFiles(EuroCcpFileSent principalFileRecord, List<EuroCcpFileSent> ficherosTmp, Path tmpPath,
      Path tratadosPath, Path pendientesPath, String compressExtension, Charset fileCharset, boolean writeFooter,
      TimeZone timeZone) throws SIBBACBusinessException, IOException {
    int lineNumber;
    int countWrites = 0;
    String line;
    Path file;
    // FlatFileItemWriter<String> principalFileWriter = null;

    EuroCcpOwnershipReferenceStaticData titular = null;
    List<String> lines = new ArrayList<String>();

    if (CollectionUtils.isNotEmpty(ficherosTmp)) {
      String appenFinalName = principalFileRecord.getFileName();
      String appendFinalBaseName = FilenameUtils.getBaseName(appenFinalName);
      String pendienteEnviarName = null;
      boolean compress = StringUtils.isNotBlank(compressExtension)
          && (compressExtension.equalsIgnoreCase("ZIP") || compressExtension.equalsIgnoreCase("GZ"));

      if (compress) {
        if (compressExtension.equalsIgnoreCase("ZIP")) {
          pendienteEnviarName = appendFinalBaseName + "." + compressExtension;
        }
        else if (compressExtension.equalsIgnoreCase("GZ")) {
          pendienteEnviarName = appenFinalName + "." + compressExtension;
        }
        else {
          throw new SIBBACBusinessException("INCIDENCIA- Metodo de compresion no valido: " + compressExtension);
        }
      }
      else {
        pendienteEnviarName = appenFinalName;
      }

      LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] creando fichero: {}", appenFinalName);

      Path appendTmpFile = Paths.get(tmpPath.toString(), appenFinalName);
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] tmp File: {}", appendTmpFile.toString());

      Path appendTmpPendienteEnviarFile = Paths.get(tmpPath.toString(), pendienteEnviarName);
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] tmp File: {}",
          appendTmpPendienteEnviarFile.toString());
      Path appendPendienteEnviarFile = Paths.get(pendientesPath.toString(), pendienteEnviarName);
      LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] pdt enviar File: {}", appenFinalName.toString());

      if (Files.exists(appendTmpFile)) {
        throw new SIBBACBusinessException("INCIDENCIA- Fichero: " + appendTmpFile.toString() + " existe en: "
            + tmpPath.toString());
      }

      if (Files.exists(appendTmpPendienteEnviarFile)) {
        throw new SIBBACBusinessException("INCIDENCIA- Fichero: " + appendTmpPendienteEnviarFile.toString()
            + " existe en: " + tmpPath.toString());
      }

      if (Files.exists(appendPendienteEnviarFile)) {
        throw new SIBBACBusinessException("INCIDENCIA- Fichero: " + appendPendienteEnviarFile.toString()
            + " existe en: " + pendientesPath.toString());
      }

      LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] Abriendo Output stream: {} ...",
          appendTmpFile.toString());
      try (OutputStream os = Files.newOutputStream(appendTmpFile, StandardOpenOption.CREATE_NEW,
          StandardOpenOption.WRITE);
          OutputStreamWriter osw = new OutputStreamWriter(os, fileCharset);
          BufferedWriter bw = new BufferedWriter(osw);) {

        LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] Inicializando line Mapper...");

        LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] Inicializando excel writer...");

        for (EuroCcpFileSent euroCcpFileSentTxt : ficherosTmp) {
          file = Paths.get(euroCcpFileSentTxt.getFilePath());
          if (Files.notExists(file)) {
            throw new SIBBACBusinessException("INCIDENCIA- Fichero no encontrado: " + file.toString());
          }
          lineNumber = 0;
          try (InputStream is = Files.newInputStream(file, StandardOpenOption.READ);
              InputStreamReader isr = new InputStreamReader(is, fileCharset);
              BufferedReader br = new BufferedReader(isr);) {
            while ((line = br.readLine()) != null) {
              lineNumber++;

              LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] file: {} lineNumber: {} line: {}",
                  file.toString(), lineNumber, line);

              LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] escribiendo titular: {} ...", titular);
              lines.add(line);
              try {
                bw.write(line);
                bw.newLine();
                countWrites++;
              }
              catch (Exception e) {
                throw new SIBBACBusinessException("INCIDENCIA- Escribiendo el titular en excel, fichero: "
                    + file.toString() + " lineNumber: " + lineNumber + " linea: " + line, e);
              }
              lines.clear();
            }// fin while lines

          }
          catch (Exception e) {
            throw new SIBBACBusinessException(e);
          }
          LOG.debug("[EuroCcpAbstractFileGenerationBo :: appendTmpFiles] Cambiando estadoProcesado a : {} ...",
              EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
          euroCcpFileSentTxt.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
          euroCcpFileSentTxt.setAuditDate(new Date());
          euroCcpFileSentTxt.setAuditUser("EURO_CCP_S");
          euroCcpFileSentTxt = euroCcpFileSentDao.save(euroCcpFileSentTxt);
        }// fin while files
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(timeZone);
        StringBuilder sb = new StringBuilder(principalFileRecord.getClientNumber().substring(
            principalFileRecord.getClientNumber().length() - 4));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sb.append(sdf.format(calendar.getTime())).append(String.format("%010d", countWrites));
        String output = sb.toString();
        output = StringUtils.rightPad(output, 256, ' ');
        bw.write(output);
        bw.newLine();
      }

      // principalFileWriter = new FlatFileItemWriter<String>();
      // ExecutionContext ec = new ExecutionContext();
      // try {
      // principalFileWriter.setEncoding(fileCharset.name());
      // principalFileWriter.setLineAggregator(new
      // PassThroughLineAggregator<String>());
      // principalFileWriter.setForceSync(true);
      // principalFileWriter.setAppendAllowed(false);
      // principalFileWriter.setShouldDeleteIfEmpty(true);
      // principalFileWriter.setShouldDeleteIfExists(false);
      // principalFileWriter.setTransactional(true);
      // principalFileWriter.setName(appendTmpFile.toString());
      // principalFileWriter.setResource(new PathResource(appendTmpFile));
      // principalFileWriter.open(ec);
      //
      // if (writeFooter) {
      // principalFileWriter.setFooterCallback(getEuroCcpFlatFileFooterCallBack(principalFileRecord.getClientNumber(),
      // timeZoneSt, countWrites));
      // }
      // } catch (ItemStreamException e) {
      // throw new SIBBACBusinessException(e);
      // } finally {
      // if (principalFileWriter != null) {
      // principalFileWriter.close();
      // }
      // }

      this.comprimirPrincipalFileAndMoverPdteEnviar(principalFileRecord, appendTmpFile, appendTmpPendienteEnviarFile,
          appendPendienteEnviarFile, compress, compressExtension, fileCharset);
      this.comprimirTemporalesAndMoverTratados(principalFileRecord, ficherosTmp, tratadosPath, fileCharset);
    }
  }

  // private FlatFileFooterCallback getEuroCcpFlatFileFooterCallBack(final
  // String clientNumber,
  // final String timeZoneSt,
  // final int numberRecords) {
  //
  // FlatFileFooterCallback fffc = new FlatFileFooterCallback() {
  //
  // @Override
  // public void writeFooter(Writer writer) throws IOException {
  // Calendar calendar = Calendar.getInstance();
  // TimeZone timeZone = TimeZone.getTimeZone(timeZoneSt);
  // calendar.setTimeZone(timeZone);
  // StringBuilder sb = new
  // StringBuilder(clientNumber.substring(clientNumber.length() - 4));
  // SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
  // sb.append(sdf.format(calendar.getTime())).append(String.format("%010d",
  // numberRecords));
  // String output = sb.toString();
  // output = StringUtils.rightPad(output, 256, ' ');
  // writer.write(output);
  //
  // }
  // };
  // return fffc;
  // }
}
