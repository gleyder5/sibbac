package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpTrailerFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpTrailer;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpTrailerFieldSetMapper extends WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean> {

  public EuroCcpTrailerFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpTrailerFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {

    return new EuroCcpTrailer();
  }

}
