package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.dto.PtiMessageFileReaderDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionCuadreEccDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.ti.R03;
import sibbac.pti.messages.ti.TI;

@Service
public class CuadreEccFileReaderSubList {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccFileReaderSubList.class);

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  @Autowired
  private CuadreEccTitularBo cuadreEccTitularBo;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0xasBo xasBo;

  @Value("${sibbac.pti.cuadreecc.idecc:MEFFESBBCRV}")
  private String idEccCuadreEcc;

  public CuadreEccFileReaderSubList() {
  }

  /**
   * @param beans a procesar
   * @param operacionesBorradas aqui se le pasa los numeros de operacion que ya
   * se han borrado para no volver a intentarlo
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void grabarCuadreEccSibbacReferenciasAndTitulares(List<PtiMessageFileReaderDTO> beans,
      Collection<IdentificacionOperacionCuadreEccDTO> operacionesBorradas, String refTitularAUnknown,
      int maxRegsInSimpleThread) throws SIBBACBusinessException {
    LOG.trace("[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] inicio.");
    LOG.trace("[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] beans {}.", beans);
    // DATOS OPERACION
    AN an = null;
    // DATOS TITULAR
    TI ti;

    int countDeletes;

    IdentificacionOperacionCuadreEccDTO identificacionOperacion = null;

    Long idEjecucion = null;
    CuadreEccEjecucion ejecucion = null;

    for (PtiMessageFileReaderDTO ptiMessageFileReaderDTO : beans) {
      countDeletes = 0;
      LOG.trace("[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] Procesando {}.",
          ptiMessageFileReaderDTO);
      if (ptiMessageFileReaderDTO.getMsg() != null) {
        if (ptiMessageFileReaderDTO.getMsg().getTipo() == PTIMessageType.AN) {

          an = (AN) ptiMessageFileReaderDTO.getMsg();

          ejecucion = null;

          identificacionOperacion = cuadreEccEjecucionBo.getIdentificacionOperacionCuadreEccDTO(an);

          BigDecimal fechaLiqBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
              .convertDateToString(identificacionOperacion.getFechaLiquidacion()));
          BigDecimal fechaEnvioBd = null;
          try {
            fechaEnvioBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(an
                .getFechaDeEnvio()));
          }
          catch (PTIMessageException e) {
            throw new SIBBACBusinessException("No se ha podido recuperar la fecha de envio del msg, AN: " + an, e);
          }
          if (fechaEnvioBd.compareTo(fechaLiqBd) < 0) {

            try {
              LOG.debug(
                  "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] Se ignora AN por fecha envio {} < fecha liquidacion {} - \n{}\n{}",
                  fechaEnvioBd, fechaLiqBd, an, an.toPTIFormat());
            }
            catch (PTIMessageException e) {
              LOG.trace(e.getMessage(), e);
            }
            an = null;
            identificacionOperacion = null;
            ejecucion = null;
            continue;
          }

          try {

            if (!operacionesBorradas.contains(identificacionOperacion)
                && operacionesBorradas.add(identificacionOperacion)) {
              LOG.debug(
                  "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] Buscando preexistentes para reprocesarlos : {} ...",
                  identificacionOperacion);

              if ((ejecucion = cuadreEccEjecucionBo.saveCuadreEccEjecucion(an)) != null) {

                countDeletes = cuadreEccEjecucionBo.deletePreviousDataByCuadreEccEjecucion(ejecucion, true, false,
                    false, maxRegsInSimpleThread);

                if (countDeletes > 0) {
                  LOG.debug(
                      "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] Se trata de un reprocesamiento, se han borrado {} registros.",
                      countDeletes);
                }
                // cuadreEccSibbacBo.grabarCuadreEccSibbac(ejecucion,
                // identificacionOperacion);
              }

            }
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(
                "Incidencia al borrar los datos por reproceso " + identificacionOperacion, e);
          }

        }
        else if (ptiMessageFileReaderDTO.getMsg().getTipo() == PTIMessageType.TI && an != null
            && identificacionOperacion != null) {
          LOG.trace("[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] Procesando TI con datos de titularidad.");
          ti = (TI) ptiMessageFileReaderDTO.getMsg();

          if (ejecucion != null) {
            idEjecucion = ejecucion.getId();
          }
          else {
            ejecucion = cuadreEccEjecucionBo.findByIdentificacionNumOpNuevo(identificacionOperacion);

            if (ejecucion != null) {
              idEjecucion = ejecucion.getId();
            }
            else {
              LOG.warn(
                  "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] No encontrado ejecucion. {}",
                  identificacionOperacion);
              continue;
            }
          }
          cuadreEccReferenciaBo.grabarCuadreEccReferencia(ti, identificacionOperacion, idEjecucion, refTitularAUnknown);

        }
        else if (an == null) {
          LOG.warn(
              "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] No hemos encontrado un msg AN que acompañe al titular {}",
              ptiMessageFileReaderDTO);
        }
        else {
          LOG.warn(
              "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] NO contiene un mensaje valido {}.",
              ptiMessageFileReaderDTO);
        }
      }
      else {
        LOG.warn(
            "[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] NO contiene mensaje {}.",
            ptiMessageFileReaderDTO);
      }
    }
    LOG.trace("[CuadreEccFileReaderSubList :: grabarCuadreEccSibbacReferenciasAndTitulares] fin.");
  }

  public Map<String, List<PtiMessageFileReaderDTO>> agruparPorNumeroOperacion(List<PtiMessageFileReaderDTO> titulares) {
    LOG.trace("[CuadreEccFileReaderSubList :: agruparPorNumeroOperacion] inicio.");
    Map<String, List<PtiMessageFileReaderDTO>> agrupadosPorNumeroOperacion = new HashMap<String, List<PtiMessageFileReaderDTO>>();
    TI ti;
    R03 r03;
    String operacion;
    PtiMessageFileReaderDTO anPrevio = null;
    for (PtiMessageFileReaderDTO ptiMessageFileReaderDTO : titulares) {
      if (ptiMessageFileReaderDTO.getMsg().getTipo() == PTIMessageType.AN) {
        anPrevio = ptiMessageFileReaderDTO;
      }
      else if (ptiMessageFileReaderDTO.getMsg().getTipo() == PTIMessageType.TI) {
        ti = (TI) ptiMessageFileReaderDTO.getMsg();
        r03 = (R03) ti.getAll(PTIMessageRecordType.R03).get(0);
        operacion = r03.getNumeroOperacion();
        if (agrupadosPorNumeroOperacion.get(operacion) == null) {
          agrupadosPorNumeroOperacion.put(operacion, new ArrayList<PtiMessageFileReaderDTO>());
          if (anPrevio != null) {
            agrupadosPorNumeroOperacion.get(operacion).add((PtiMessageFileReaderDTO) anPrevio);
          }
        }
        agrupadosPorNumeroOperacion.get(operacion).add(ptiMessageFileReaderDTO);
      }
    }
    LOG.trace("[CuadreEccFileReaderSubList :: agruparPorNumeroOperacion] fin.");
    return agrupadosPorNumeroOperacion;
  }
}
