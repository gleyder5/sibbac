package sibbac.business.comunicaciones.traspasos.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIException;
import sibbac.pti.PTIMessageFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class TraspasosEccMultithreadBo {

  private static final Logger LOG = LoggerFactory.getLogger(TraspasosEccMultithreadBo.class);

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private PtiCommomns ptiCommons;

  @Autowired
  private TraspasosEccBo traspasoEccBo;

  @Autowired
  private TraspasosEccWithUserLockBo traspasosEccWithUserLock;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0aloBo aloBo;

  public TraspasosEccMultithreadBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public CallableResultDTO crearTraspasosMultithread(List<Tmct0alcId> listAlcId, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {

    LOG.debug(
        "[crearTraspasosMultithread] inicio-{} alcs:{} desgloses:{} ops:{} cta.ori:{} cta.dest:{} miembro:{} ref.asig:{}",
        config.getAuditUser(), listAlcId, config.getListNudesgloses(), config.getListCdoperacionecc(),
        config.getCtaCompensacionOrigen(), config.getCtaCompensacionDestino(), config.getMiembroDestino(),
        config.getRefAsignacionGiveUp());

    CallableResultDTO res = new CallableResultDTO();

    List<Future<CallableResultDTO>> listProcesosLanzados = new ArrayList<>();
    Map<Future<CallableResultDTO>, Tmct0bokId> relacionBookFuture = new HashMap<Future<CallableResultDTO>, Tmct0bokId>();

    if (CollectionUtils.isNotEmpty(listAlcId)) {
      if (PTIMessageFactory.getCredentials() == null)
        try {
          ptiCommons.initPTICredentials();
        }
        catch (PTIException e) {
          throw new SIBBACBusinessException("No se ha podido configurar la conexion con pti.");
        }

      try {
        this.lanzarProcesosTraspasos(listAlcId, config, listProcesosLanzados, relacionBookFuture);

        res = this.recuperarResultadoProcesos(listProcesosLanzados, relacionBookFuture);
      }
      catch (SIBBACBusinessException e) {
        throw e;
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
      finally {
        listProcesosLanzados.clear();
        relacionBookFuture.clear();
      }
    }
    LOG.debug("[crearTraspasosMultithread] fin-{}", config.getAuditUser());
    return res;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public CallableResultDTO crearAsignacionesMultithread(String auditUser) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    TraspasosEccConfigDTO config = getNewConfig(null, null, null, null, null, null, auditUser);

    if (!config.isPtiSessionOpen()) {
      LOG.warn("[crearAsignacionesMultithread] la sesion pti esta cerrada.");
      res.addErrorMessage("La sesion pti esta cerrada.");
    }
    else {

      List<Tmct0alcId> listAlcId = dcBo.findAllAlcsWithDesglosesPdteAsignar();

      LOG.debug(
          "[crearAsignacionesMultithread] inicio-{} alcs:{} desgloses:{} ops:{} cta.ori:{} cta.dest:{} miembro:{} ref.asig:{}",
          config.getAuditUser(), listAlcId, config.getListNudesgloses(), config.getListCdoperacionecc(),
          config.getCtaCompensacionOrigen(), config.getCtaCompensacionDestino(), config.getMiembroDestino(),
          config.getRefAsignacionGiveUp());
      if (CollectionUtils.isNotEmpty(listAlcId)) {
        List<Future<CallableResultDTO>> listProcesosLanzados = new ArrayList<>();
        Map<Future<CallableResultDTO>, Tmct0bokId> relacionAloFuture = new HashMap<Future<CallableResultDTO>, Tmct0bokId>();

        if (PTIMessageFactory.getCredentials() == null)
          try {
            ptiCommons.initPTICredentials();
          }
          catch (PTIException e) {
            throw new SIBBACBusinessException("No se ha podido configurar la conexion con pti.");
          }

        try {
          this.lanzarProcesosAsignaciones(listAlcId, config, listProcesosLanzados, relacionAloFuture);

          res = this.recuperarResultadoProcesos(listProcesosLanzados, relacionAloFuture);
        }
        catch (SIBBACBusinessException e) {
          throw e;
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
        finally {
          listProcesosLanzados.clear();
          relacionAloFuture.clear();
        }
      }
    }
    LOG.debug("[crearAsignacionesMultithread] fin-{}", config.getAuditUser());
    return res;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public CallableResultDTO crearAsignacionesSinTitularMultithread(String auditUser) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    TraspasosEccConfigDTO config = getNewConfig(null, null, null, null, null, null, auditUser);

    if (!config.isPtiSessionOpen()) {
      LOG.warn("[crearAsignacionesMultithread] la sesion pti esta cerrada.");
      res.addErrorMessage("La sesion pti esta cerrada.");
    }
    else {

      List<Tmct0aloId> listAlcId = aloBo.findAllTmct0aloIdPdteEnviarMovimientoAsignacionSinTitular(new Date());

      LOG.debug(
          "[crearAsignacionesMultithread] inicio-{} alcs:{} desgloses:{} ops:{} cta.ori:{} cta.dest:{} miembro:{} ref.asig:{}",
          config.getAuditUser(), listAlcId, config.getListNudesgloses(), config.getListCdoperacionecc(),
          config.getCtaCompensacionOrigen(), config.getCtaCompensacionDestino(), config.getMiembroDestino(),
          config.getRefAsignacionGiveUp());

      List<Future<CallableResultDTO>> listProcesosLanzados = new ArrayList<>();

      Map<Future<CallableResultDTO>, Tmct0bokId> relacionBookFuture = new HashMap<Future<CallableResultDTO>, Tmct0bokId>();

      if (CollectionUtils.isNotEmpty(listAlcId)) {
        if (PTIMessageFactory.getCredentials() == null)
          try {
            ptiCommons.initPTICredentials();
          }
          catch (PTIException e) {
            throw new SIBBACBusinessException("No se ha podido configurar la conexion con pti.");
          }

        try {
          this.lanzarProcesosAsignacionesSinTitular(listAlcId, config, listProcesosLanzados, relacionBookFuture);

          res = this.recuperarResultadoProcesos(listProcesosLanzados, relacionBookFuture);
        }
        catch (SIBBACBusinessException e) {
          throw e;
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
        finally {
          listProcesosLanzados.clear();
          relacionBookFuture.clear();
        }
      }
    }
    LOG.debug("[crearAsignacionesMultithread] fin-{}", config.getAuditUser());
    return res;

  }

  @Transactional
  private void lanzarProcesosTraspasos(List<Tmct0alcId> listAlcId, TraspasosEccConfigDTO config,
      List<Future<CallableResultDTO>> listProcesosLanzados,
      Map<Future<CallableResultDTO>, Tmct0bokId> relacionBookFuture) throws SIBBACBusinessException {
    LOG.trace("[lanzarProcesosTraspasos] inicio");
    int threadPoolSize = 5;
    List<Tmct0alcId> listAlcIdsAgrupadosByBook;
    ExecutorService executor;
    Callable<CallableResultDTO> callableTraspaso;
    Future<CallableResultDTO> proceso;

    executor = Executors.newFixedThreadPool(threadPoolSize);
    try {
      Map<Tmct0bokId, List<Tmct0alcId>> mapListAlcIdsAgrupadosByBooking = agruparAlcsByBoking(listAlcId);
      for (Tmct0bokId bookingId : mapListAlcIdsAgrupadosByBooking.keySet()) {
        LOG.trace("[lanzarProcesosTraspasos] encolando proceso para alo: {}", bookingId);
        listAlcIdsAgrupadosByBook = mapListAlcIdsAgrupadosByBooking.get(bookingId);
        callableTraspaso = getNewCallableProcessToCrearTraspasos(bookingId, listAlcIdsAgrupadosByBook, config);

        proceso = executor.submit(callableTraspaso);
        relacionBookFuture.put(proceso, bookingId);
        listProcesosLanzados.add(proceso);
      }
      mapListAlcIdsAgrupadosByBooking.clear();
    }
    catch (Exception e) {
      LOG.warn("[lanzarProcesosTraspasos] ha ocurrido una excepcion lanzando los hilos, paramos el ejecutor: {}",
          executor, e);
      executor.shutdownNow();
      throw new SIBBACBusinessException(String.format("Incidencia lanzando los procesos. %s", e.getMessage()), e);
    }
    finally {
      if (!executor.isShutdown()) {
        LOG.trace("[lanzarProcesosTraspasos] hemos terminado de lanzar procesos, paramos el ejecutor. {}", executor);
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        LOG.trace("[lanzarProcesosTraspasos] todos los procesos no han teminado, los esperamos. {}", executor);
        try {
          executor.awaitTermination(listAlcId.size() < 1000 ? 5 : 10, TimeUnit.MINUTES);
          LOG.trace("[lanzarProcesosTraspasos] ha terminado el tiempo de espera, continuamos. {}", executor);
        }
        catch (InterruptedException e) {
          throw new SIBBACBusinessException(String.format("Incidencia parando el ejecutor de procesos. %s",
              e.getMessage()), e);
        }
      }
    }
    LOG.trace("[lanzarProcesosTraspasos] fin");
  }

  @Transactional
  private void lanzarProcesosAsignaciones(List<Tmct0alcId> listAlcId, TraspasosEccConfigDTO config,
      List<Future<CallableResultDTO>> listProcesosLanzados,
      Map<Future<CallableResultDTO>, Tmct0bokId> relacionBookFuture) throws SIBBACBusinessException {
    LOG.trace("[lanzarProcesosAsignaciones] inicio");
    int threadPoolSize = 25;
    List<Tmct0alcId> listAlcIdsAgrupadosByBooking;
    ExecutorService executor;
    Callable<CallableResultDTO> callableTraspaso;
    Future<CallableResultDTO> proceso;
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    tfb.setNameFormat("Thread-Asignaciones-%d");
    executor = Executors.newFixedThreadPool(threadPoolSize, tfb.build());
    try {
      Map<Tmct0bokId, List<Tmct0alcId>> mapListAlcIdsAgrupadosByAlo = agruparAlcsByBoking(listAlcId);
      for (Tmct0bokId bookingId : mapListAlcIdsAgrupadosByAlo.keySet()) {
        LOG.trace("[lanzarProcesosAsignaciones] encolando proceso para bok: {}", bookingId);
        listAlcIdsAgrupadosByBooking = mapListAlcIdsAgrupadosByAlo.get(bookingId);
        callableTraspaso = getNewCallableProcessToCrearAsignaciones(bookingId, listAlcIdsAgrupadosByBooking, config);

        proceso = executor.submit(callableTraspaso);
        relacionBookFuture.put(proceso, bookingId);
        listProcesosLanzados.add(proceso);
      }
      mapListAlcIdsAgrupadosByAlo.clear();
    }
    catch (Exception e) {
      LOG.warn("[lanzarProcesosAsignaciones] ha ocurrido una excepcion lanzando los hilos, paramos el ejecutor: {}",
          executor, e);
      executor.shutdownNow();
      throw new SIBBACBusinessException(String.format("Incidencia lanzando los procesos. %s", e.getMessage()), e);
    }
    finally {
      if (!executor.isShutdown()) {
        LOG.trace("[lanzarProcesosAsignaciones] hemos terminado de lanzar procesos, paramos el ejecutor. {}", executor);
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        LOG.trace("[lanzarProcesosAsignaciones] todos los procesos no han teminado, los esperamos. {}", executor);
        try {
          executor.awaitTermination(listAlcId.size() < 1000 ? 5 : 10, TimeUnit.MINUTES);
          LOG.trace("[lanzarProcesosAsignaciones] ha terminado el tiempo de espera, continuamos. {}", executor);
        }
        catch (InterruptedException e) {
          throw new SIBBACBusinessException(String.format("Incidencia parando el ejecutor de procesos. %s",
              e.getMessage()), e);
        }
      }
    }
    LOG.trace("[lanzarProcesosAsignaciones] fin");
  }

  @Transactional
  private void lanzarProcesosAsignacionesSinTitular(List<Tmct0aloId> listAlcId, TraspasosEccConfigDTO config,
      List<Future<CallableResultDTO>> listProcesosLanzados,
      Map<Future<CallableResultDTO>, Tmct0bokId> relacionBookFuture) throws SIBBACBusinessException {
    LOG.trace("[lanzarProcesosAsignaciones] inicio");
    int threadPoolSize = 25;
    List<Tmct0aloId> listAlcIdsAgrupadosByBooking;
    ExecutorService executor;
    Callable<CallableResultDTO> callableTraspaso;
    Future<CallableResultDTO> proceso;
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    tfb.setNameFormat("Thread-AsignacionesSinTitular-%d");
    executor = Executors.newFixedThreadPool(threadPoolSize, tfb.build());
    try {
      Map<Tmct0bokId, List<Tmct0aloId>> mapListAlcIdsAgrupadosByAlo = agruparAlosByBoking(listAlcId);
      for (Tmct0bokId bookingId : mapListAlcIdsAgrupadosByAlo.keySet()) {
        LOG.trace("[lanzarProcesosAsignaciones] encolando proceso para bok: {}", bookingId);
        listAlcIdsAgrupadosByBooking = mapListAlcIdsAgrupadosByAlo.get(bookingId);
        callableTraspaso = getNewCallableProcessToCrearAsignacionesSinTitular(bookingId, listAlcIdsAgrupadosByBooking,
            config);

        proceso = executor.submit(callableTraspaso);
        relacionBookFuture.put(proceso, bookingId);
        listProcesosLanzados.add(proceso);
      }
      mapListAlcIdsAgrupadosByAlo.clear();
    }
    catch (Exception e) {
      LOG.warn("[lanzarProcesosAsignaciones] ha ocurrido una excepcion lanzando los hilos, paramos el ejecutor: {}",
          executor, e);
      executor.shutdownNow();
      throw new SIBBACBusinessException(String.format("Incidencia lanzando los procesos. %s", e.getMessage()), e);
    }
    finally {
      if (!executor.isShutdown()) {
        LOG.trace("[lanzarProcesosAsignaciones] hemos terminado de lanzar procesos, paramos el ejecutor. {}", executor);
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        LOG.trace("[lanzarProcesosAsignaciones] todos los procesos no han teminado, los esperamos. {}", executor);
        try {
          executor.awaitTermination(listAlcId.size() < 1000 ? 5 : 10, TimeUnit.MINUTES);
          LOG.trace("[lanzarProcesosAsignaciones] ha terminado el tiempo de espera, continuamos. {}", executor);
        }
        catch (InterruptedException e) {
          throw new SIBBACBusinessException(String.format("Incidencia parando el ejecutor de procesos. %s",
              e.getMessage()), e);
        }
      }
    }
    LOG.trace("[lanzarProcesosAsignaciones] fin");
  }

  @Transactional
  private CallableResultDTO recuperarResultadoProcesos(List<Future<CallableResultDTO>> listProcesosLanzados,
      Map<Future<CallableResultDTO>, Tmct0bokId> relacionBookFuture) throws SIBBACBusinessException {

    CallableResultDTO res = new CallableResultDTO();
    CallableResultDTO resultadoProceso;
    String msg;
    if (CollectionUtils.isNotEmpty(listProcesosLanzados)) {
      for (Future<CallableResultDTO> procesoFor : listProcesosLanzados) {
        LOG.trace("[recuperarResultadoProcesos] recuperando resultado proceso: {}", procesoFor);
        try {
          try {
            LOG.trace("[recuperarResultadoProcesos] esperaremos por cada resultado 10 segundos, pasado el tiempo daremos excepcion...");
            resultadoProceso = procesoFor.get(1, TimeUnit.SECONDS);

            if (resultadoProceso != null) {
              res.append(resultadoProceso);
            }
          }
          catch (TimeoutException e) {
            LOG.warn(e.getMessage(), e);
            if (relacionBookFuture.get(procesoFor) != null) {
              msg = String.format("%s/%s los traspasos no se han podido generar por timeout.",
                  relacionBookFuture.get(procesoFor).getNuorden(), relacionBookFuture.get(procesoFor).getNbooking());
              res.addErrorMessage(msg);
              LOG.warn("[recuperarResultadoProcesos] {}", msg);
            }
            procesoFor.cancel(true);
          }

        }
        catch (ExecutionException e) {
          LOG.warn(e.getMessage(), e);
          if (e.getCause() != null) {
            res.addErrorMessage(e.getCause().getMessage());
          }
          else {
            res.addErrorMessage(e.getMessage());
          }
        }
        catch (InterruptedException e) {
          LOG.warn("[recuperarResultadoProcesos] Se ha cancelado un proceso {}", procesoFor, e);
          LOG.warn(e.getMessage(), e);
          if (relacionBookFuture.get(procesoFor) != null) {
            msg = String.format("%s/%s El proceso encargado se ha cancelado.", relacionBookFuture.get(procesoFor)
                .getNuorden(), relacionBookFuture.get(procesoFor).getNbooking());
            res.addErrorMessage(msg);
            LOG.warn("[recuperarResultadoProcesos] {}", msg);
          }
        }
        catch (RuntimeException e) {
          LOG.warn("[recuperarResultadoProcesos] incidencia inesperado en la evaluacion del resultado del proceso {}",
              procesoFor, e);
          LOG.warn(e.getMessage(), e);
          if (relacionBookFuture.get(procesoFor) != null) {
            msg = String.format("%s/%s Error inesperado en uno de los procesos. %s", relacionBookFuture.get(procesoFor)
                .getNuorden(), relacionBookFuture.get(procesoFor).getNbooking(), e.getMessage());
            res.addErrorMessage(msg);
            LOG.warn("[recuperarResultadoProcesos] {}", msg);
          }
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }

      }
    }
    return res;
  }

  private Callable<CallableResultDTO> getNewCallableProcessToCrearTraspasos(final Tmct0bokId bookingId,
      final List<Tmct0alcId> listAlcId, final TraspasosEccConfigDTO config) {

    Callable<CallableResultDTO> callable = new Callable<CallableResultDTO>() {
      @Override
      public CallableResultDTO call() throws SIBBACBusinessException {
        return traspasosEccWithUserLock.crearTraspasosWithUserLock(bookingId, listAlcId, config);
      }
    };

    return callable;
  }

  private Callable<CallableResultDTO> getNewCallableProcessToCrearAsignaciones(final Tmct0bokId bookingId,
      final List<Tmct0alcId> listAlcId, final TraspasosEccConfigDTO config) {

    Callable<CallableResultDTO> callable = new Callable<CallableResultDTO>() {
      @Override
      public CallableResultDTO call() throws SIBBACBusinessException {
        return traspasosEccWithUserLock.crearAsignacionesWithUserLock(bookingId, listAlcId, config);
      }
    };

    return callable;
  }

  private Callable<CallableResultDTO> getNewCallableProcessToCrearAsignacionesSinTitular(final Tmct0bokId bookingId,
      final List<Tmct0aloId> listAlcId, final TraspasosEccConfigDTO config) {

    Callable<CallableResultDTO> callable = new Callable<CallableResultDTO>() {
      @Override
      public CallableResultDTO call() throws SIBBACBusinessException {
        return traspasosEccWithUserLock.crearAsignacionesSinTitularWithUserLock(bookingId, listAlcId, config);
      }
    };

    return callable;
  }

  private Map<Tmct0bokId, List<Tmct0alcId>> agruparAlcsByBoking(List<Tmct0alcId> listAlcId) {
    Map<Tmct0bokId, List<Tmct0alcId>> map = new HashMap<Tmct0bokId, List<Tmct0alcId>>();
    Tmct0bokId key;
    for (Tmct0alcId alc : listAlcId) {
      key = new Tmct0bokId(alc.getNbooking(), alc.getNuorden());
      if (map.get(key) == null) {
        map.put(key, new ArrayList<Tmct0alcId>());
      }
      map.get(key).add(alc);
    }

    return map;
  }

  private Map<Tmct0bokId, List<Tmct0aloId>> agruparAlosByBoking(List<Tmct0aloId> listAlcId) {
    Map<Tmct0bokId, List<Tmct0aloId>> map = new HashMap<Tmct0bokId, List<Tmct0aloId>>();
    Tmct0bokId key;
    for (Tmct0aloId alc : listAlcId) {
      key = new Tmct0bokId(alc.getNbooking(), alc.getNuorden());
      if (map.get(key) == null) {
        map.put(key, new ArrayList<Tmct0aloId>());
      }
      map.get(key).add(alc);
    }

    return map;
  }

  public TraspasosEccConfigDTO getNewConfig(List<Long> listNudesgloses, List<String> listCdoperacionecc,
      String ctaCompensacionOrigen, String ctaCompensacionDestino, String miembroDestino, String refAsignacionGiveUp,
      String auditUser) throws SIBBACBusinessException {
    return traspasoEccBo.getNewConfigForTraspasos(listNudesgloses, listCdoperacionecc, ctaCompensacionOrigen,
        ctaCompensacionDestino, miembroDestino, refAsignacionGiveUp, auditUser);
  }

}
