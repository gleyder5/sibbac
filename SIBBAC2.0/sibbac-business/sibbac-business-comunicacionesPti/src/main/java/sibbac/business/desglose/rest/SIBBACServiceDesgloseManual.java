package sibbac.business.desglose.rest;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.desglose.bo.DesgloseBo;
import sibbac.business.desglose.bo.DesgloseExcelBo;
import sibbac.business.desglose.bo.DesgloseMultithread;
import sibbac.business.desglose.bo.DesgloseWithUserLock;
import sibbac.business.desglose.commons.AlgoritmoDesgloseManual;
import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.DesgloseManualDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.desglose.commons.TipoDesgloseManual;
import sibbac.business.desglose.query.AlcQuery;
import sibbac.business.desglose.query.DesgloseManualPageQuery;
import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ReglaBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0parametrizacionBo;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.data.AloDesgloseData;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClearerDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.FilterInfo;
import sibbac.database.QueryFilter;
import sibbac.database.bo.QueryBo;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;
import sibbac.webapp.security.database.model.Tmct0Users;
import sibbac.webapp.security.session.UserSession;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/desglose/manual")
public class SIBBACServiceDesgloseManual {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceDesgloseManual.class);

  private static final String JSON_UTF8 = "application/json;charset=UTF-8";

  @Autowired
  private QueryBo queryBo;

  @Autowired
  private DesgloseWithUserLock desgloseWithLock;

  @Autowired
  private DesgloseMultithread desgloseMultithreadBo;

  @Autowired
  private DesgloseBo desgloseBo;

  @Autowired
  private DesgloseExcelBo desgloseExcelBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0tfiBo titularesInstruccionesBo;

  @Autowired
  private Tmct0UsersBo usersBo;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private Tmct0ReglaBo reglaAsignacionBo;

  @Autowired
  private SendMail sendMail;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentasDeCompensacionBo;

  @Autowired
  private Tmct0parametrizacionBo parametrizacionReglaBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;

  @Value("${sibbac.nacional.desglose.excel.result.email.default:}")
  private String defaultResultEmailDesgloseExcel;
  @Value("${sibbac.nacional.desglose.excel.result.email.subject:}")
  private String subjectResultEmaliDesgloseExcel;
  @Value("${sibbac.nacional.desglose.excel.result.email.body:}")
  private String bodyResultEmailDesgloseExcel;

  @Value("${sibbac.nacional.desglose.excel.result.page.limit:20}")
  private int limiteResultadoPantallaDesgloseExcel;

  @Value("${sibbac.nacional.desglose.pantalla.result.email.default:}")
  private String defaultResultEmailDesglosePantalla;
  @Value("${sibbac.nacional.desglose.pantalla.result.email.subject:}")
  private String subjectResultEmaliDesglosePantalla;
  @Value("${sibbac.nacional.desglose.pantalla.result.email.body:}")
  private String bodyResultEmailDesglosePantalla;

  @Value("${sibbac.nacional.desglose.pantalla.result.page.limit:20}")
  private int limiteResultadoPantallaDesglosePantalla;

  @Value("${sibbac.nacional.desglose.pantalla.read.desgloses.thread.size:5}")
  private int readDesglosesPantallaThreadSize;

  @Value("${environment}")
  private String environment;

  @RequestMapping(value = "/upload/{filename}", method = RequestMethod.PUT)
  public void upload(@PathVariable("filename") String filename, InputStream is, HttpServletResponse response,
      HttpSession session) throws SIBBACBusinessException {
    CallableResultDTO res, resProcess;
    final ObjectMapper mapper;
    final String usuarioAuditoria;
    final UserSession userSession;

    LOG.info("[upload] Leido fichero {}", filename);
    res = new CallableResultDTO();
    userSession = UserSession.class.cast(session.getAttribute("UserSession"));
    Objects.requireNonNull(userSession);
    usuarioAuditoria = StringUtils.substring(userSession.getName(), 0, 10);
    LOG.debug("[upload] usuario de auditoria: {}", usuarioAuditoria);
    try (DesgloseConfigDTO config = ctx.getBean(DesgloseConfigDTO.class)) {
      config.setAuditUser(usuarioAuditoria);
      config.setTipoDesgloseManual(TipoDesgloseManual.EXCEL);
      resProcess = desgloseExcelBo.desglosarExcel(is, filename, config);
      if (resProcess != null) {
        res.append(resProcess);
      }
    }
    res = this.prepararResultadoPantalla(this.subjectResultEmaliDesgloseExcel, this.bodyResultEmailDesgloseExcel,
        this.defaultResultEmailDesgloseExcel, this.limiteResultadoPantallaDesgloseExcel, filename, is,
        usuarioAuditoria, res);

    mapper = new ObjectMapper();
    response.setContentType(JSON_UTF8);
    try (OutputStream os = new BufferedOutputStream(response.getOutputStream())) {
      mapper.writeValue(os, res);
    }
    catch (IOException e) {
      LOG.error("[upload] Error al mapear respuesta", e);
    }
  }

  private CallableResultDTO prepararResultadoPantalla(String subjectResultEmaliDesglose,
      String bodyResultEmailDesglose, String defaultResultEmailDesglose, int limiteResultadoPantallaDesglose,
      String filename, InputStream is, String usuarioAuditoria, CallableResultDTO origen) {
    int countresultados = origen.getMessage().size() + origen.getErrorMessage().size();
    if (countresultados > limiteResultadoPantallaDesglose) {

      StringBuilder sb = new StringBuilder("");
      CallableResultDTO resAux = new CallableResultDTO();

      sb.append("\tSe han recuperado ").append(origen.getMessage().size()).append(" mensajes ok y ")
          .append(origen.getErrorMessage().size()).append(" errores.\n");
      resAux.addMessage(String.format("Se ha superado el limite de %d resultados para mostrar en pantalla",
          limiteResultadoPantallaDesglose));
      if (origen.getMessage().size() > 0) {
        resAux.addMessage(String.format("Se han procesado %d de manera correcta", origen.getMessage().size()));
        sb.append("\n\t\tMENSAJES OK: \n\n");
        for (String msg : origen.getMessage()) {
          sb.append("\t").append(msg).append("\n");
        }
      }

      if (origen.getErrorMessage().size() > 0) {
        resAux.addErrorMessage(String.format("Se han producido %d errores", origen.getErrorMessage().size()));
        sb.append("\n\t\tMENSAJES ERROR: \n\n");
        for (String msg : origen.getErrorMessage()) {
          sb.append("\t").append(msg).append("\n");
        }
      }

      try {
        this.enviarEmailResultado(subjectResultEmaliDesglose, bodyResultEmailDesglose, defaultResultEmailDesglose,
            filename, is, usuarioAuditoria, sb.toString(), resAux);
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        resAux = new CallableResultDTO();
        resAux.addErrorMessage(e.getMessage());
        resAux.append(origen);
      }
      return resAux;
    }
    else {
      return origen;
    }
  }

  private void enviarEmailResultado(String subjectResultEmaliDesgloseExcel, String bodyResultEmailDesgloseExcel,
      String defaultResultEmailDesgloseExcel, String filename, InputStream is, String usuarioAuditoria, String msgBody,
      CallableResultDTO res) throws SIBBACBusinessException {
    List<String> to = new ArrayList<String>();

    List<String> cc = new ArrayList<String>();
    List<String> nameDest = new ArrayList<String>();
    List<InputStream> path2attach = new ArrayList<InputStream>();
    if (is != null) {
      path2attach.add(is);
      nameDest.add(filename);
    }
    String subject = MessageFormat.format(subjectResultEmaliDesgloseExcel, filename, usuarioAuditoria, environment);
    String body = MessageFormat.format(bodyResultEmailDesgloseExcel, msgBody);
    String correousuario = null;
    Tmct0Users user = usersBo.getByUsername(usuarioAuditoria);
    if (user != null && user.getEmail() != null && !user.getEmail().trim().isEmpty()) {
      correousuario = user.getEmail().trim();
      to.add(correousuario);
      res.addMessage(String.format("Se ha enviado un correo con el detalle del proceso a la direccion: '%s'",
          correousuario));
      if (defaultResultEmailDesgloseExcel != null && !defaultResultEmailDesgloseExcel.trim().isEmpty()) {
        cc.addAll(Arrays.asList(defaultResultEmailDesgloseExcel.split(",")));
        res.addMessage(String.format("Se ha enviado copia del correo con el detalle del proceso a la direccion: '%s'",
            defaultResultEmailDesgloseExcel));
      }
    }
    else {
      if (defaultResultEmailDesgloseExcel != null && !defaultResultEmailDesgloseExcel.trim().isEmpty()) {
        to.addAll(Arrays.asList(defaultResultEmailDesgloseExcel.split(",")));
        res.addMessage(String.format("Se ha enviado un correo con el detalle del proceso a la direccion: '%s'",
            defaultResultEmailDesgloseExcel));
      }
    }

    if (cc.isEmpty() && to.isEmpty()) {
      throw new SIBBACBusinessException(
          "INCIDENCIA-No se ha podido mandar correo con el resultado. No hay direcciones donde enviar.");
    }
    try {
      sendMail.sendMail(to, subject, body, path2attach, nameDest, cc);
    }
    catch (MailSendException | IllegalArgumentException | MessagingException | IOException e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA-No se ha podido mandar correo con el resultado por %s", e.getMessage()), e);
    }
  }

  @RequestMapping(value = "/alos/filters", method = RequestMethod.GET)
  public List<FilterInfo> getAloQueryFilters() {
    return new DesgloseManualPageQuery().getFiltersInfo();
  }

  @RequestMapping(value = "/alos/columns", method = RequestMethod.GET)
  public List<DynamicColumn> getAloQueryColumns() {
    return new DesgloseManualPageQuery().getColumns();
  }

  @RequestMapping(value = "/alos", method = RequestMethod.GET)
  public void getAloQuery(HttpServletRequest request, HttpServletResponse servletResponse) throws Exception {
    final AbstractDynamicPageQuery adpq = ctx.getBean("DesgloseManualPageQuery", AbstractDynamicPageQuery.class);
    final StreamResult streamResult;

    LOG.debug("[executeQuery] Inicio...");
    adpq.fillDynamicQuery(request.getParameterMap());
    adpq.setIsoFormat(true);
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.JSON_OBJECTS, streamResult);
    LOG.debug("[executeQuery] Fin");
  }

  @RequestMapping(value = "/{nuorden}/{nbooking}/{nucnfclt}/", method = RequestMethod.POST)
  public CallableResultDTO desglosaManualmente(@PathVariable("nuorden") String nuorden,
      @PathVariable("nbooking") String nbooking, @PathVariable("nucnfclt") String nucnfclt,
      @RequestBody SolicitudDesgloseManual solicitud, HttpSession session) {
    final Tmct0bokId bokId;
    final Tmct0aloId aloId;
    final CallableResultDTO result;
    final List<SolicitudDesgloseManual> listSolicitudDesgloseManual;
    final String usuarioAuditoria;
    final UserSession userSession;

    Objects.requireNonNull(nuorden);
    Objects.requireNonNull(nbooking);
    Objects.requireNonNull(nucnfclt);
    result = new CallableResultDTO();
    try {
      userSession = UserSession.class.cast(session.getAttribute("UserSession"));
      Objects.requireNonNull(userSession);
      usuarioAuditoria = StringUtils.substring(userSession.getName(), 0, 10);
      LOG.debug("[desgloseManualmente] Usuario de auditoria: {}", usuarioAuditoria);
      try (DesgloseConfigDTO config = ctx.getBean(DesgloseConfigDTO.class)) {
        config.setAuditUser(usuarioAuditoria);
        aloId = new Tmct0aloId(nucnfclt, nbooking, nuorden);
        bokId = new Tmct0bokId(nbooking, nuorden);
        solicitud.setTmct0aloId(aloId);
        listSolicitudDesgloseManual = Collections.singletonList(solicitud);
        desgloseWithLock.desgloseManual(bokId, listSolicitudDesgloseManual, config);
      }
    }
    catch (SIBBACBusinessException sbex) {
      result.addErrorMessage(sbex.getMessage());
    }
    catch (RuntimeException ex) {
      result.addErrorMessage("Error inesperado, contacte con administrador");
      LOG.error("[desglosaManualmente] Error inesperado al desglosar manualmente", ex);
    }
    return result;
  }

  @RequestMapping(value = "/{nuorden}/{nbooking}/{nucnfclt}", method = RequestMethod.GET, produces = JSON_UTF8)
  public AloDesgloseData consultaDesglose(@PathVariable("nuorden") String nuorden,
      @PathVariable("nbooking") String nbooking, @PathVariable("nucnfclt") String nucnfclt)
      throws SIBBACBusinessException {
    final Tmct0aloId aloId;
    final AloDesgloseData alo;
    final String message;

    Objects.requireNonNull(nuorden);
    Objects.requireNonNull(nbooking);
    Objects.requireNonNull(nucnfclt);
    aloId = new Tmct0aloId(nucnfclt, nbooking, nuorden);
    alo = aloBo.findAloDesgloseData(aloId);
    if (alo == null) {
      message = MessageFormat.format("No existe un alo con la identificacion {0}", aloId.toString());
      LOG.info("[consultaDesglose] {}", message);
      throw new SIBBACBusinessException(message);
    }
    return alo;
  }

  @RequestMapping(value = "/{nuorden}/{nbooking}/{nucnfclt}/alcs", method = RequestMethod.GET, produces = JSON_UTF8)
  public void consultaDesgloseAlc(@PathVariable("nuorden") String nuorden, @PathVariable("nbooking") String nbooking,
      @PathVariable("nucnfclt") String nucnfclt, HttpServletResponse servletResponse) throws IOException {
    final StreamResult streamResult;
    final Tmct0aloId aloId;
    final QueryFilter queryFilter;

    aloId = new Tmct0aloId(nucnfclt, nbooking, nuorden);
    queryFilter = new AlcQuery(aloId);
    streamResult = new HttpStreamResultHelper(servletResponse);
    try {
      Objects.requireNonNull(nuorden);
      Objects.requireNonNull(nbooking);
      Objects.requireNonNull(nucnfclt);
      queryBo.executeQuery(queryFilter, FormatStyle.JSON_OBJECTS, streamResult);
    }
    catch (RuntimeException ioex) {
      LOG.error("[consultaDesglose] Error al consultar desglose", ioex);
      streamResult.sendErrorMessage("Error inesperado");
    }
  }

  @RequestMapping(value = "/autocompleter/cuentaCompensacion", method = RequestMethod.GET)
  public List<LabelValueObject> getCuentaCompensacionList() {
    return cuentasDeCompensacionBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/miembroCompensador", method = RequestMethod.GET)
  public List<LabelValueObject> getMiembroCompensadorList() {
    return compensadorBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/reglasAsignacion", method = RequestMethod.GET)
  public List<LabelValueObject> getReglasAsignacionList() {
    return reglaAsignacionBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/clearersNacional", method = RequestMethod.GET)
  public List<SettlementDataClearerDTO> getClearersList() {
    List<SettlementDataClearerDTO> res = settlementDataBo.getAllClearersNacionalActivos();
    res.add(0, new SettlementDataClearerDTO());
    return res;
  }

  @RequestMapping(value = "/list/algoritmoDesglose", method = RequestMethod.GET)
  public List<LabelValueObject> listAlgoritmosDesglose() {
    List<LabelValueObject> res = new ArrayList<LabelValueObject>();
    res.add(new LabelValueObject(AlgoritmoDesgloseManual.FEWEST.name(), AlgoritmoDesgloseManual.FEWEST.name()));
    res.add(new LabelValueObject(AlgoritmoDesgloseManual.CLEARING_INFORMATION.name(),
        AlgoritmoDesgloseManual.CLEARING_INFORMATION.name()));
    res.add(new LabelValueObject(AlgoritmoDesgloseManual.MANUAL.name(), AlgoritmoDesgloseManual.MANUAL.name()));
    res.add(new LabelValueObject(AlgoritmoDesgloseManual.PRICE.name(), AlgoritmoDesgloseManual.PRICE.name()));
    res.add(new LabelValueObject(AlgoritmoDesgloseManual.PROPORTIONAL.name(), AlgoritmoDesgloseManual.PROPORTIONAL
        .name()));
    return res;
  }

  @RequestMapping(value = "/alcs/baja", method = RequestMethod.POST, produces = JSON_UTF8)
  public List<Tmct0alcId> getListAlosSeleccionados(@RequestBody List<Tmct0aloId> listAlosSeleccionados) {
    return alcBo.findAllTmct0alcIdByListTmct0aloIdAndDeAlta(listAlosSeleccionados);
  }

  @RequestMapping(value = "/new/desglose/manual/{aloId}", method = RequestMethod.GET)
  public DesgloseManualDTO getNewDesgloseManual(@PathVariable("aloId") Tmct0aloId aloId,
      @RequestBody SolicitudDesgloseManual solicitud) throws SIBBACBusinessException {
    DesgloseManualDTO desglose = null;
    Tmct0alo alo = aloBo.findById(aloId);
    if (alo != null) {
      if (solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.FEWEST
          || solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.PROPORTIONAL
          || solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.MANUAL) {

        BigDecimal titulosDesglosados = BigDecimal.ZERO;
        if (solicitud.getAltas() != null) {
          for (DesgloseManualDTO alta : solicitud.getAltas()) {
            if (alta.getTitulos() != null) {
              titulosDesglosados = titulosDesglosados.add(alta.getTitulos());
            }
          }
        }

        SettlementData sd = settlementDataBo.getSettlementDataNacional(alo);
        // TODO VER QUE PASA EN ESTE CASO CON LA CONFIG
        desglose = desgloseMultithreadBo.getNewDesgloseManual(alo, sd, null);

        if (alo.getNutitcli().compareTo(titulosDesglosados) > 0) {
          desglose.setTitulos(alo.getNutitcli().subtract(titulosDesglosados));
        }
      }

    }
    return desglose;
  }

  @RequestMapping(value = "/listSolicitudDesglose/{algoritmo}", method = RequestMethod.POST, produces = JSON_UTF8)
  public List<SolicitudDesgloseManual> getListSolicitudDesgloseByAlgoritmo(
      @PathVariable("algoritmo") AlgoritmoDesgloseManual algoritmo,
      @RequestBody List<Tmct0aloId> listAlosSeleccionados, HttpSession session) throws SIBBACBusinessException {
    UserSession userSession = UserSession.class.cast(session.getAttribute("UserSession"));
    String usuarioAuditoria = StringUtils.substring(userSession.getName(), 0, 10);
    LOG.debug("[getListSolicitudDesgloseByAlgoritmo] usuario de auditoria: {}", usuarioAuditoria);
    return desgloseMultithreadBo.crearSolicitudesDesglose(algoritmo, listAlosSeleccionados,
        readDesglosesPantallaThreadSize, usuarioAuditoria);
  }

  @RequestMapping(value = "/holderAutocompleter/{holderData}/{fechaContratacion}", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<Holder> getListHolders(@PathVariable("holderData") String holderData,
      @PathVariable("fechaContratacion") String fechaContratacion) throws SIBBACBusinessException {
    BigDecimal fetrade = FormatDataUtils.convertStringToBigDecimal(fechaContratacion.replace("-", ""));
    return titularesInstruccionesBo.findAllHolderForAutocompleter(holderData, fetrade);
  }

  @RequestMapping(value = "/holder/{holder}/{fechaContratacion}", method = RequestMethod.GET, produces = JSON_UTF8)
  public Holder getHolder(@PathVariable("holder") String cdholder,
      @PathVariable("fechaContratacion") String fechaContratacion) throws SIBBACBusinessException {
    BigDecimal fetrade = FormatDataUtils.convertStringToBigDecimal(fechaContratacion.replace("-", ""));
    List<Holder> listHolders = titularesInstruccionesBo.findAllHoldersByCdholder(cdholder, fetrade);
    if (!listHolders.isEmpty()) {
      return listHolders.get(0);
    }
    else {
      return null;
    }
  }

  @RequestMapping(value = "/desglosar", method = RequestMethod.POST, produces = JSON_UTF8)
  public CallableResultDTO desglosarPantalla(@RequestBody List<SolicitudDesgloseManual> desgloses, HttpSession session)
      throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    UserSession userSession = UserSession.class.cast(session.getAttribute("UserSession"));
    String usuarioAuditoria = StringUtils.substring(userSession.getName(), 0, 10);
    LOG.debug("[upload] usuario de auditoria: {}", usuarioAuditoria);
    try (DesgloseConfigDTO config = ctx.getBean(DesgloseConfigDTO.class)) {
      config.setAuditUser(usuarioAuditoria);
      config.setTipoDesgloseManual(TipoDesgloseManual.MANUAL);
      res.append(desgloseMultithreadBo.desgloseManual(desgloses, config));
    }

    res = this.prepararResultadoPantalla(this.subjectResultEmaliDesglosePantalla, this.bodyResultEmailDesglosePantalla,
        this.defaultResultEmailDesglosePantalla, this.limiteResultadoPantallaDesglosePantalla, "", null,
        usuarioAuditoria, res);
    return res;
  }
}
