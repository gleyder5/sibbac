package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpCashPositionFields implements WrapperFileReaderFieldEnumInterface {

  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  SUBACCOUNT_NUMBER("subaccountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  CASH_AMOUNT_IDENTIFIER("cashAmountIdentifier", 8, 0, WrapperFileReaderFieldType.STRING),
  CASH_POSITION_CHANGE("cashPositionChange", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  CASH_POSITION_CHANGE_DC("cashPositionChangeDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  CASH_POSITION_NEW("cashPositionNew", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  CASH_POSITION_NEW_DC("cashPositionNewDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  CASH_POSITION_DESCRIPTION("cashPositionDescription", 40, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_PRICE("currencyPrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  FILLER("filler", 353, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpCashPositionFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
