package sibbac.business.comunicaciones.pti.ce.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.jms.JmsMessagesPtiBo;
import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.dao.Tmct0desgloseclititDao;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.IndicadorCapacidad;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.common.SIBBACPTICommons;
import sibbac.pti.messages.ce.CE;
import sibbac.pti.messages.ce.R00;

@Service
public class EnvioCeBo {

  protected static final Logger LOG_PTI_OUT = LoggerFactory.getLogger("PTI_OUT");

  @Autowired
  private Tmct0operacioncdeccBo opBo;

  @Autowired
  private Tmct0desgloseclititDao dctDao;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  @Qualifier(value = "JmsMessagesPtiBo")
  private JmsMessagesPtiBo jmsSendMessage;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void enviarCe(ConnectionFactory cf, String destination, String cdoperacionecc, PTIMessageVersion version,
      Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException, PTIMessageException, JMSException {
    Tmct0operacioncdecc op = opBo.findByCdoperacionecc(cdoperacionecc);

    if (op != null) {
      String cifSvb = cache.getCifSvb();

      BigDecimal mitadTitulos = op.getNutitulos().divide(new BigDecimal(2), 0, RoundingMode.HALF_UP);

      BigDecimal titulosSv = dctDao.sumImtitulosByNuoperacioninicAndExistsCdholder(cdoperacionecc, cifSvb);

      if (titulosSv != null && titulosSv.compareTo(mitadTitulos) >= 0) {
        op.setCeenviado('S');
        op.setAuditFechaCambio(new Date());
        op.setAuditUser("SIBBAC20");
        opBo.saveAndFlush(op);
        if (op.getCdindcapacidad() != IndicadorCapacidad.PROPIO.charAt(0)) {

          CE ce = this.crearCe(op, IndicadorCapacidad.PROPIO, version, cache);
          if (ce != null) {
            this.escribirLog(op, IndicadorCapacidad.PROPIO, cdoperacionecc);
            this.send(cf, destination, ce);
          }

        }
      }
      else {
        BigDecimal titulosNotSv = dctDao.sumImtitulosByNuoperacioninicAndExistsDistinctCdholder(cdoperacionecc, cifSvb);
        if (titulosNotSv != null && titulosNotSv.compareTo(mitadTitulos) > 0) {
          op.setCeenviado('S');
          op.setAuditFechaCambio(new Date());
          op.setAuditUser("SIBBAC20");
          opBo.saveAndFlush(op);
          if (op.getCdindcapacidad() != IndicadorCapacidad.AJENO.charAt(0)) {
            CE ce = this.crearCe(op, IndicadorCapacidad.AJENO, version, cache);

            if (ce != null) {
              this.escribirLog(op, IndicadorCapacidad.AJENO, cdoperacionecc);
              this.send(cf, destination, ce);
            }
          }
        }
      }
    }
  }

  private void send(ConnectionFactory cf, String destination, PTIMessage ce) throws JMSException, PTIMessageException {
    try (Connection connection = jmsSendMessage.getNewConnection(cf)) {
      try (Session session = jmsSendMessage.getNewSession(connection, true)) {
        try (MessageProducer mp = jmsSendMessage.getNewPtiMessageProducer(session, destination);) {
          jmsSendMessage.sendPtiMessageOnline(session, mp, ce);
          session.commit();
        }
        catch (Exception e) {
          session.rollback();
          throw e;
        }
      }
    }
    LOG_PTI_OUT.info(ce.toPTIFormat());
  }

  private CE crearCe(Tmct0operacioncdecc op, String indicadorCapacidad, PTIMessageVersion version,
      Tmct0xasAndTmct0cfgConfig cache) throws PTIMessageException {
    CE ce = (CE) PTIMessageFactory.getInstance(version, PTIMessageType.CE);

    if (op.getCdmiembromkt() != null)
      SIBBACPTICommons.setDestinoAndUsuarioDestino(ce, op.getCdmiembromkt().trim());
    R00 r00 = (R00) ce.getRecordInstance(PTIMessageRecordType.R00);
    r00.setMiembroMercado(op.getCdmiembromkt());
    r00.setFechaContratacion(op.getFcontratacion());
    r00.setCodigoIsin(op.getCdisin());
    r00.setSegmentoMercado(op.getCdsegmento());
    r00.setNumeroEjecucion(new BigDecimal(op.getNuexemkt().trim()));
    r00.setTipoOperacion(op.getCdoperacionmkt());
    r00.setIndicadorCapacidadTReporting(indicadorCapacidad);
    r00.setIndicadorExenta("N");
    String sentidoPti = cache.getSentidoSibbacSentidoPti(op.getCdsentido() + "");
    if (sentidoPti != null) {
      r00.setIndicadorCompraVenta(sentidoPti);
    }
    else if (op.getCdsentido() == 'C') {
      r00.setIndicadorCompraVenta("1");
    }
    else {
      r00.setIndicadorCompraVenta("2");
    }

    ce.addRecord(PTIMessageRecordType.R00, r00);
    return ce;
  }

  private void escribirLog(Tmct0operacioncdecc op, String nuevoIndicadorCapacidad, String cdoperacionecc) {
    List<Tmct0alcId> listAlcid = dctDao.findAllAlcIdByNuoperacioinic(cdoperacionecc);
    if (!listAlcid.isEmpty()) {
      String msg = String.format("Enviado CE Ind.Capacidad Op.ECC:'%s'-Nuexemkt:'%s' a '%s'-'%s'.", cdoperacionecc, op
          .getNuexemkt(), nuevoIndicadorCapacidad, nuevoIndicadorCapacidad.equals(IndicadorCapacidad.PROPIO) ? "PROPIO"
          : "AJENO");
      for (Tmct0alcId tmct0alcId : listAlcid) {
        logBo.insertarRegistroNewTransaction(tmct0alcId.getNuorden(), tmct0alcId.getNbooking(),
            tmct0alcId.getNucnfclt(), msg, "", "SIBBAC20");
      }
    }
  }
}
