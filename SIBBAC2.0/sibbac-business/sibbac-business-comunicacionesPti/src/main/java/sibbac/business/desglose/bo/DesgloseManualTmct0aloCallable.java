package sibbac.business.desglose.bo;

import java.util.concurrent.Callable;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

class DesgloseManualTmct0aloCallable implements Callable<CallableResultDTO> {

  private DesgloseTransactionalBo desgloseTransaccionalBo;
  private DesgloseConfigDTO config;
  private SolicitudDesgloseManual solicitudDesgloseManual;

  DesgloseManualTmct0aloCallable(DesgloseTransactionalBo desgloseTransaccionalBo, DesgloseConfigDTO config,
      SolicitudDesgloseManual alloId) {
    this.config = config;
    this.desgloseTransaccionalBo = desgloseTransaccionalBo;
    this.solicitudDesgloseManual = alloId;
  }

  @Override
  public CallableResultDTO call() throws SIBBACBusinessException {
    return desgloseTransaccionalBo.procesoDesgloseNacionalManual(solicitudDesgloseManual, config);
  }

  SolicitudDesgloseManual getSolicitudDesgloseManual() {
    return solicitudDesgloseManual;
  }

  void setSolicitudDesgloseManual(SolicitudDesgloseManual solicitudDesgloseManual) {
    this.solicitudDesgloseManual = solicitudDesgloseManual;
  }

}
