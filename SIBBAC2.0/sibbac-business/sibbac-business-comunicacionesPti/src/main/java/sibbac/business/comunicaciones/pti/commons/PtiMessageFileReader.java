package sibbac.business.comunicaciones.pti.commons;

import org.springframework.batch.item.file.LineMapper;

import sibbac.business.comunicaciones.pti.commons.dto.PtiMessageFileReaderDTO;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedFileReader;

public abstract class PtiMessageFileReader extends WrapperMultiThreadedFileReader<PtiMessageFileReaderDTO> {

  public PtiMessageFileReader(PtiMessageFileReaderLineMapper ptiLineMapper) {
    super();
    this.lineMapper = ptiLineMapper;
  }

  @Override
  protected LineMapper<PtiMessageFileReaderDTO> initLineMapper() {
    return lineMapper;
  }

  @Override
  protected void initLineTokenizers(LineMapper<PtiMessageFileReaderDTO> lineMapper) {
  }

  @Override
  protected void initFieldSetMappers(LineMapper<PtiMessageFileReaderDTO> lineMapper) {
  }

}
