package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpUnsettledPositionFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpUnsettledPosition;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpUnsettledPositionFieldSetMapper extends
                                                          WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean> {

  public EuroCcpUnsettledPositionFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpUnsettledPositionFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {

    return new EuroCcpUnsettledPosition();
  }

}
