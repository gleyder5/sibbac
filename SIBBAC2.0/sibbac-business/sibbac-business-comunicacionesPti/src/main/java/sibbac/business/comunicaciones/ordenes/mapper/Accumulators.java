package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0ctaBo;
import sibbac.business.wrappers.database.bo.Tmct0ctgBo;
import sibbac.business.wrappers.database.bo.Tmct0ctoBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.dao.Tmct0ctdDao;
import sibbac.business.wrappers.database.dao.Tmct0galDao;
import sibbac.business.wrappers.database.dao.Tmct0greDao;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctaId;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0ctgId;
import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0ctoId;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0gre;
import sibbac.business.wrappers.database.model.Tmct0ord;

@Service
public class Accumulators {

  private static final Logger LOG = LoggerFactory.getLogger(Accumulators.class);

  @Autowired
  private XmlTmct0cto xmlTmct0cto;

  @Autowired
  private XmlTmct0cta xmlTmct0cta;

  @Autowired
  private XmlTmct0ctg xmlTmct0ctg;

  @Autowired
  private XmlTmct0log xmlTmct0log;

  @Autowired
  private Tmct0greDao greDao;

  @Autowired
  private Tmct0galDao galDao;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0ctoBo ctoBo;

  @Autowired
  private Tmct0ctaBo ctaBo;

  @Autowired
  private Tmct0ctgBo ctgBo;

  @Autowired
  private Tmct0ctdDao ctdDao;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void accumulateAllValues(Tmct0bok tmct0bok) throws XmlOrdenExceptionReception {

    Tmct0ord tmct0ord = tmct0bok.getTmct0ord();
    int numTmct0ctgs = 0;
    List<Tmct0gal> gals;
    List<Tmct0alo> alos;
    List<Tmct0cto> ctos;
    List<Tmct0ctg> ctgs;
    List<Tmct0cta> ctas;
    LOG.debug("[accumulateAllValues] buscando tmct0gres {}...", tmct0bok.getId());
    List<Tmct0gre> gres = greDao.findAllByNuordenAndNbooking(tmct0bok.getId().getNuorden(), tmct0bok.getId()
        .getNbooking());
    for (Tmct0gre tmct0gre : gres) {
      LOG.debug("[accumulateAllValues] buscando tmct0gals {}...", tmct0gre.getId());
      gals = galDao.findAllByNuordenAndNbookingAndNuagraje(tmct0gre.getId().getNuorden(), tmct0gre.getId()
          .getNbooking(), tmct0gre.getId().getNuagreje());
      for (Tmct0gal tmct0gal : gals) {
        LOG.debug("[accumulateAllValues] buscando tmct0gal {}...", tmct0gal.getId());
        Tmct0cto tmct0cto = null;
        Tmct0ctg tmct0ctg = null;
        Tmct0cta tmct0cta = null;
        String nuorden = tmct0gal.getId().getNuorden().trim();
        String nbooking = tmct0gal.getId().getNbooking().trim();
        String nucnfclt = tmct0gal.getId().getNucnfclt().trim();
        String cdbroker = tmct0gal.getTmct0gre().getCdbroker().trim();
        String cdmercad = tmct0gal.getTmct0gre().getCdmercad().trim();

        /*
         * obtener alo de la list para extraer datos de cara a rellenar los
         * campos de las tablas de acumulacion
         */
        Tmct0alo tmct0alo = null;
        alos = aloBo.findAllByNuordenAndNbooking(nuorden, nbooking);
        Iterator<Tmct0alo> itTmct0alo = alos.iterator();
        while (tmct0alo == null && itTmct0alo.hasNext()) {
          Tmct0alo aux = itTmct0alo.next();
          if (nuorden.equals(aux.getId().getNuorden().trim()) && nbooking.equals(aux.getId().getNbooking().trim())
              && nucnfclt.equals(aux.getId().getNucnfclt().trim())
              && !aux.getTmct0sta().getId().getCdestado().trim().equals("002")
              && !aux.getTmct0sta().getId().getCdestado().trim().equals("003")) {
            tmct0alo = aux;
            LOG.debug("[accumulateAllValues] encontrado tmct0alo {}...", tmct0alo.getId());
          }
        }
        if (null == tmct0alo) {
          throw new XmlOrdenExceptionReception(nuorden, nbooking, cdbroker, nucnfclt,
              "Allocation in execution groups, is not defined in xml");
        }

        /*
         * buscamos en la lista de XmlTmct0cto si existe en ese caso acumulamos
         * los datos en caso contrario creamos objeto y acumulamos datos
         */
        LOG.debug("[accumulateAllValues] buscando tmct0ctos {}...", nuorden);
        ctos = ctoBo.findAllByNuorden(nuorden);
        Iterator<Tmct0cto> itTmct0cto = ctos.iterator();
        while (tmct0cto == null && itTmct0cto.hasNext()) {
          Tmct0cto aux = itTmct0cto.next();
          if (nuorden.equals(aux.getId().getNuorden().trim()) && cdbroker.equals(aux.getId().getCdbroker().trim())
              && cdmercad.equals(aux.getId().getCdmercad().trim())) {
            tmct0cto = aux;
            LOG.debug("[accumulateAllValues] encontrado tmct0cto {}...", tmct0cto.getId());
            break;
          }
        }

        if (tmct0cto == null) {
          LOG.debug("[accumulateAllValues] creando nuevo tmct0cto {}...", nuorden);
          tmct0cto = new Tmct0cto();
          tmct0cto.setId(new Tmct0ctoId(cdmercad, nuorden, cdbroker));
          tmct0cto.setTmct0ord(tmct0ord);
          tmct0ord.getTmct0ctos().add(tmct0cto);
          xmlTmct0cto.accumulateNew(tmct0gal, tmct0alo, tmct0cto);
          tmct0cto = ctoBo.save(tmct0cto);
          /*
           * Como el objeto tmct0cto es nuevo pasamos directamente a crear el
           * tmct0ctg
           */

          tmct0ctg = new Tmct0ctg();
          tmct0ctg.setId(new Tmct0ctgId(nbooking, cdbroker, nuorden, cdmercad));
          tmct0ctg.setTmct0cto(tmct0cto);
          LOG.debug("[accumulateAllValues] creando nuevo tmct0ctg {}...", tmct0ctg.getId());
          xmlTmct0ctg.accumulateNew(tmct0gal, tmct0alo, getPrecioMedioCentro(tmct0bok, tmct0gal), tmct0ctg);
          tmct0ctg = ctgBo.save(tmct0ctg);
          tmct0cto.getTmct0ctgs().add(tmct0ctg);
          numTmct0ctgs++;
          /*
           * Como tanto el objeto tmct0cto como el tmct0ctg son nuevos creamos
           * el tmct0cta
           */
          tmct0cta = new Tmct0cta();
          tmct0cta.setId(new Tmct0ctaId(nucnfclt, cdmercad, nuorden, cdbroker, nbooking));
          tmct0cta.setTmct0ctg(tmct0ctg);
          LOG.debug("[accumulateAllValues] creando nuevo tmct0cta {}...", tmct0cta.getId());
          xmlTmct0cta.accumulateNew(tmct0gal, tmct0alo, tmct0cta);
          tmct0cta = ctaBo.save(tmct0cta);
          tmct0ctg.getTmct0ctas().add(tmct0cta);
        }
        else {
          LOG.debug("[accumulateAllValues] acumulando tmct0cto {}...", tmct0cto.getId());
          xmlTmct0cto.accumulateAdd(tmct0gal, tmct0alo, tmct0cto);
          tmct0cto = ctoBo.save(tmct0cto);
          /*
           * Como el objeto NO tmct0cto es nuevo buscamos en la lista de objetos
           * un ctg coincidente
           */
          LOG.debug("[accumulateAllValues] buscando tmct0ctgs {}...", tmct0bok.getId());
          ctgs = ctgBo.findAllByNuodenAndNbooking(nuorden, nbooking);
          Iterator<Tmct0ctg> itTmct0ctg = ctgs.iterator();
          while (tmct0ctg == null && itTmct0ctg.hasNext()) {
            Tmct0ctg aux = itTmct0ctg.next();
            if (nuorden.equals(aux.getId().getNuorden().trim()) && cdbroker.equals(aux.getId().getCdbroker().trim())
                && cdmercad.equals(aux.getId().getCdmercad().trim())
                && nbooking.equals(aux.getId().getNbooking().trim())) {
              tmct0ctg = aux;
              LOG.debug("[accumulateAllValues] encontrado tmct0ctg {}...", tmct0ctg.getId());
              break;
            }
          }
          if (tmct0ctg == null) {
            tmct0ctg = new Tmct0ctg();
            tmct0ctg.setId(new Tmct0ctgId(nbooking, cdbroker, nuorden, cdmercad));
            tmct0ctg.setTmct0cto(tmct0cto);
            LOG.debug("[accumulateAllValues] creando nuevo tmct0ctg {}...", tmct0ctg.getId());
            xmlTmct0ctg.accumulateNew(tmct0gal, tmct0alo, getPrecioMedioCentro(tmct0bok, tmct0gal), tmct0ctg);
            tmct0ctg = ctgBo.save(tmct0ctg);
            tmct0cto.getTmct0ctgs().add(tmct0ctg);
            numTmct0ctgs++;
            /*
             * Como el objeto tmct0ctg es nuevo pasamos directamente a crear el
             * tmct0cta
             */
            tmct0cta = new Tmct0cta();
            tmct0cta.setId(new Tmct0ctaId(nucnfclt, cdmercad, nuorden, cdbroker, nbooking));
            tmct0cta.setTmct0ctg(tmct0ctg);
            LOG.debug("[accumulateAllValues] creando nuevo tmct0cta {}...", tmct0cta.getId());
            xmlTmct0cta.accumulateNew(tmct0gal, tmct0alo, tmct0cta);
            tmct0cta = ctaBo.save(tmct0cta);
            tmct0ctg.getTmct0ctas().add(tmct0cta);
          }
          else {
            LOG.debug("[accumulateAllValues] acumulando tmct0ctg {}...", tmct0ctg.getId());
            xmlTmct0ctg.accumulateAdd(tmct0gal, tmct0alo, tmct0ctg);
            tmct0ctg = ctgBo.save(tmct0ctg);
            /*
             * Como el objeto tmct0ctg NO es nuevo buscamos en la lista de
             * objetos un cta coincidente
             */
            LOG.debug("[accumulateAllValues] buscando tmct0ctas {}...", tmct0alo.getId());
            ctas = ctaBo.findAllByNuodenAndNbookingAndNucnfclt(nuorden, nbooking, nucnfclt);
            Iterator<Tmct0cta> itTmct0cta = ctas.iterator();
            while (tmct0cta == null && itTmct0cta.hasNext()) {
              Tmct0cta aux = itTmct0cta.next();
              if (nuorden.equals(aux.getId().getNuorden().trim()) && cdbroker.equals(aux.getId().getCdbroker().trim())
                  && cdmercad.equals(aux.getId().getCdmercad().trim())
                  && nbooking.equals(aux.getId().getNbooking().trim())
                  && nucnfclt.equals(aux.getId().getNucnfclt().trim())) {
                tmct0cta = aux;
                LOG.debug("[accumulateAllValues] encontrado tmct0cta {}...", tmct0cta.getId());
                break;
              }
            }
            if (tmct0cta == null) {
              tmct0cta = new Tmct0cta();
              tmct0cta.setId(new Tmct0ctaId(nucnfclt, cdmercad, nuorden, cdbroker, nbooking));
              tmct0cta.setTmct0ctg(tmct0ctg);
              LOG.debug("[accumulateAllValues] creando nuevo tmct0cta {}...", tmct0cta.getId());
              xmlTmct0cta.accumulateNew(tmct0gal, tmct0alo, tmct0cta);
              tmct0cta = ctaBo.save(tmct0cta);
              tmct0ctg.getTmct0ctas().add(tmct0cta);
            }
            else {
              LOG.debug("[accumulateAllValues] acumulando tmct0cta {}...", tmct0cta.getId());
              xmlTmct0cta.accumulateAdd(tmct0gal, tmct0alo, tmct0cta);
              tmct0cta = ctaBo.save(tmct0cta);
            }
          }
        }
      }

    }

    /*
     * Si es internacional cargamos los objetos tmct0ctd
     */
    if ('E' == tmct0ord.getCdclsmdo()) {
      ctas = ctaBo.findAllByNuodenAndNbooking(tmct0bok.getId().getNuorden(), tmct0bok.getId().getNbooking());
      if (CollectionUtils.isNotEmpty(ctas)) {
        for (Tmct0cta tmct0cta : ctas) {
          LOG.debug("[accumulateAllValues] acumulando tmct0ctd {}...", tmct0cta.getId());
          XmlTmct0ctd xmlTmct0ctd = new XmlTmct0ctd(tmct0cta);
          xmlTmct0ctd.accumulateNew();

          // Solo NO routing
          if (!ordBo.isRouting(tmct0ord)) {
            // Cargar Cdrefban si este viene informado en bok para control
            // saldos Partenon
            if (!tmct0bok.getCdrefban().trim().equals("")) {
              xmlTmct0ctd.getJDOObject().setCdrefban(tmct0bok.getCdrefban().trim());
            }
          }
          ctdDao.save(xmlTmct0ctd.getJDOObject());
          tmct0cta.getTmct0ctds().add(xmlTmct0ctd.getJDOObject());
        }
      }
    }

    /*
     * actualizacion marca multicentro
     */
    if (numTmct0ctgs > 1) {
      LOG.debug("[accumulateAllValues] inbokmul == S");
      tmct0bok.setInbokmul('S');
      bokBo.save(tmct0bok);
      ctgs = ctgBo.findAllByNuodenAndNbooking(tmct0bok.getId().getNuorden(), tmct0bok.getId().getNbooking());
      if (CollectionUtils.isNotEmpty(ctgs)) {
        for (Tmct0ctg tmct0ctg : ctgs) {
          tmct0ctg.setInbokmul('S');

        }
        ctgBo.save(ctgs);
      }
    }
    xmlTmct0log.xml_mapping(tmct0ord, tmct0bok.getId().getNbooking(), "Centros creados");

  }

  /**
   * Calcula el precio medio por mercado
   * 
   * @param bok El Booking que contiene los datos
   * @param tmct0gal El gruo de ejcución por el que realizar la agrupación
   * @return Elk precio medio por mercado
   */
  private BigDecimal getPrecioMedioCentro(Tmct0bok bok, Tmct0gal tmct0gal) {

    BigDecimal precio = new BigDecimal(0);
    BigDecimal div = new BigDecimal(0);

    for (Iterator<Tmct0eje> iterator = bok.getTmct0ejes().iterator(); iterator.hasNext();) {
      Tmct0eje tmct0eje = (Tmct0eje) iterator.next();
      if (tmct0eje.getCdbroker().equalsIgnoreCase(tmct0gal.getTmct0gre().getCdbroker())
          && tmct0eje.getCdmercad().equalsIgnoreCase(tmct0gal.getTmct0gre().getCdmercad())
          && tmct0eje.getId().getNbooking().equalsIgnoreCase(tmct0gal.getId().getNbooking())
          && tmct0eje.getId().getNuorden().equalsIgnoreCase(tmct0gal.getId().getNuorden())) {

        precio = precio.add(tmct0eje.getImcbmerc().multiply(tmct0eje.getNutiteje()).setScale(4, RoundingMode.HALF_UP));
        div = div.add(tmct0eje.getNutiteje());
      }
    }
    BigDecimal medio = new BigDecimal(0);
    try {
      medio = precio.divide(div, 4, RoundingMode.HALF_UP);
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      medio = new BigDecimal(0);
    }
    // System.out.println(tmct0gal.getTmct0gres().getCdbroker() + " " +
    // precio +" / " + div + " = " + medio);
    return medio;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void persistCentros(Tmct0ord tmct0ord) {
    List<Tmct0cta> ctas;
    List<Tmct0ctg> ctgs;
    List<Tmct0ctd> ctds;
    List<Tmct0cto> ctos = tmct0ord.getTmct0ctos();
    for (Tmct0cto tmct0cto : ctos) {
      ctgs = new ArrayList<Tmct0ctg>(tmct0cto.getTmct0ctgs());

      tmct0cto.getTmct0ctgs().clear();
      tmct0cto = ctoBo.save(tmct0cto);
      for (Tmct0ctg tmct0ctg : ctgs) {
        ctas = new ArrayList<Tmct0cta>(tmct0ctg.getTmct0ctas());

        tmct0ctg.getTmct0ctas().clear();
        tmct0ctg = ctgBo.save(tmct0ctg);

        for (Tmct0cta tmct0cta : ctas) {
          ctds = new ArrayList<Tmct0ctd>(tmct0cta.getTmct0ctds());
          tmct0cta.getTmct0ctds().clear();
          tmct0cta = ctaBo.save(tmct0cta);
          ctdDao.save(ctds);
        }
      }
    }

  }

}
