package sibbac.business.comunicaciones.euroccp.common;

import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.batch.support.PatternMatcher;
import org.springframework.util.Assert;

public class RegexPatternMatcher<S> extends PatternMatcher<S> {

  private Map<String, S> map;

  public RegexPatternMatcher(Map<String, S> map) {
    super(map);
    this.map = map;
    Assert.notNull(this.map, "A non-null key must be provided to match against.");
  }

  @Override
  public S match(String line) {
    S value = null;
    Assert.notNull(line, "A non-null key must be provided to match against.");

    for (String key : map.keySet()) {
      if (Pattern.matches(key, line)) {
        value = map.get(key);
        break;
      }
    }

    if (value == null) {
      throw new IllegalStateException("Could not find a matching pattern for key=[" + line + "]");
    }
    return value;
  }

}
