package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.PROCESSED_SETTLED_MOVEMENT)
@Entity
public class EuroCcpProcessedSettledMovement extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "SUBACCOUNT_NUMBER", length = 10, nullable = false)
  private String subaccountNumber;

  @Column(name = "OPPOSITE_PARTY_CODE", length = 6, nullable = false)
  private String oppositePartyCode;

  @Column(name = "PRODUCT_GROUP_CODE", length = 2, nullable = false)
  private String productGroupCode;

  @Column(name = "SYMBOL", length = 6, nullable = false)
  private String symbol;

  @Column(name = "TYPE_FIELD", length = 1, nullable = true)
  private Character typeField;

  @Temporal(TemporalType.DATE)
  @Column(name = "EXPIRATION_DATE", nullable = true)
  private Date expirationDate;

  @Column(name = "EXERCISE_PRICE", length = 15, scale = 7, nullable = true)
  private BigDecimal exercisePrice;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;

  @Column(name = "MOVEMENT_CODE", length = 2, nullable = false)
  private String movementCode;

  @Column(name = "PROCESSED_QUANTITY_LONG", length = 12, scale = 2, nullable = true)
  private BigDecimal processedQuantityLong;

  @Column(name = "PROCESSED_QUANTITY_SHORT", length = 12, scale = 2, nullable = true)
  private BigDecimal processedQuantityShort;

  @Column(name = "COUNTER_VALUE", length = 18, scale = 2, nullable = true)
  private BigDecimal counterValue;

  @Column(name = "COUNTER_VALUE_DC", length = 1, nullable = true)
  private Character counterValueDC;

  @Column(name = "COUNTER_VALUE_CURRENCY_CODE", length = 3, nullable = true)
  private String counterValueCurrencyCode;

  @Column(name = "CUPON_INTEREST", length = 18, scale = 2, nullable = true)
  private BigDecimal cuponInterest;

  @Column(name = "CUPON_INTEREST_DC", length = 1, nullable = true)
  private Character cuponInterestDC;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRANSACTION_DATE", nullable = false)
  private Date transactionDate;

  @Column(name = "ISIN_CODE", length = 12, nullable = false)
  private String isinCode;

  @Column(name = "DEPOT_SETTLED_REFERENCE", length = 9, nullable = false)
  private String depotSettledReference;

  @Temporal(TemporalType.DATE)
  @Column(name = "VALUE_DATE", nullable = true)
  private Date valueDate;

  @Column(name = "COMMENT_FIELD", length = 21, nullable = true)
  private String commentField;

  @Column(name = "DEPOT_ID", length = 6, nullable = false)
  private String depotId;

  @Column(name = "SAFE_KEEPING_ID", length = 2, nullable = false)
  private String safeKeepingId;

  @Column(name = "FILLER", length = 281, nullable = true)
  private String filler;

  public EuroCcpProcessedSettledMovement() {
  }

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getSubaccountNumber() {
    return subaccountNumber;
  }

  public String getOppositePartyCode() {
    return oppositePartyCode;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public String getSymbol() {
    return symbol;
  }

  public Character getTypeField() {
    return typeField;
  }

  public Date getExpirationDate() {
    return expirationDate;
  }

  public BigDecimal getExercisePrice() {
    return exercisePrice;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getMovementCode() {
    return movementCode;
  }

  public BigDecimal getProcessedQuantityLong() {
    return processedQuantityLong;
  }

  public BigDecimal getProcessedQuantityShort() {
    return processedQuantityShort;
  }

  public BigDecimal getCounterValue() {
    return counterValue;
  }

  public Character getCounterValueDC() {
    return counterValueDC;
  }

  public String getCounterValueCurrencyCode() {
    return counterValueCurrencyCode;
  }

  public BigDecimal getCuponInterest() {
    return cuponInterest;
  }

  public Character getCuponInterestDC() {
    return cuponInterestDC;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public String getIsinCode() {
    return isinCode;
  }

  public String getDepotSettledReference() {
    return depotSettledReference;
  }

  public Date getValueDate() {
    return valueDate;
  }

  public String getCommentField() {
    return commentField;
  }

  public String getDepotId() {
    return depotId;
  }

  public String getSafeKeepingId() {
    return safeKeepingId;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setSubaccountNumber(String subaccountNumber) {
    this.subaccountNumber = subaccountNumber;
  }

  public void setOppositePartyCode(String oppositePartyCode) {
    this.oppositePartyCode = oppositePartyCode;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public void setTypeField(Character typeField) {
    this.typeField = typeField;
  }

  public void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }

  public void setExercisePrice(BigDecimal exercisePrice) {
    this.exercisePrice = exercisePrice;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setMovementCode(String movementCode) {
    this.movementCode = movementCode;
  }

  public void setProcessedQuantityLong(BigDecimal processedQuantityLong) {
    this.processedQuantityLong = processedQuantityLong;
  }

  public void setProcessedQuantityShort(BigDecimal processedQuantityShort) {
    this.processedQuantityShort = processedQuantityShort;
  }

  public void setCounterValue(BigDecimal counterValue) {
    this.counterValue = counterValue;
  }

  public void setCounterValueDC(Character counterValueDC) {
    this.counterValueDC = counterValueDC;
  }

  public void setCounterValueCurrencyCode(String counterValueCurrencyCode) {
    this.counterValueCurrencyCode = counterValueCurrencyCode;
  }

  public void setCuponInterest(BigDecimal cuponInterest) {
    this.cuponInterest = cuponInterest;
  }

  public void setCuponInterestDC(Character cuponInterestDC) {
    this.cuponInterestDC = cuponInterestDC;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setIsinCode(String isinCode) {
    this.isinCode = isinCode;
  }

  public void setDepotSettledReference(String depotSettledReference) {
    this.depotSettledReference = depotSettledReference;
  }

  public void setValueDate(Date valueDate) {
    this.valueDate = valueDate;
  }

  public void setCommentField(String commentField) {
    this.commentField = commentField;
  }

  public void setDepotId(String depotId) {
    this.depotId = depotId;
  }

  public void setSafeKeepingId(String safeKeepingId) {
    this.safeKeepingId = safeKeepingId;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

}
