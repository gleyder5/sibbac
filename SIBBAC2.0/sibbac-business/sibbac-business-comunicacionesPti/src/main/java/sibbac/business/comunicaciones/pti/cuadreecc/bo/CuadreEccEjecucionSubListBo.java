package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccEjecucionDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R00;
import sibbac.pti.messages.an.R01;
import sibbac.pti.messages.an.R02;
import sibbac.pti.messages.an.R03;
import sibbac.pti.messages.an.R05;
import sibbac.pti.messages.an.R06;
import sibbac.pti.messages.an.R07;

@Service
public class CuadreEccEjecucionSubListBo {
  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccEjecucionBo.class);

  @Autowired
  private CuadreEccEjecucionDao cuadreEccEjecucionDao;

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0xasBo xasBo;

  @Value("${sibbac.pti.cuadreecc.idecc:MEFFESBBCRV}")
  private String idEccCuadreEcc;

  public CuadreEccEjecucionSubListBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public CuadreEccEjecucion saveCuadreEccEjecucion(AN an) throws SIBBACBusinessException, PTIMessageException {
    LOG.trace("[CuadreEccEjecucionSubListBo :: saveCuadreEccEjecucion] inicio.");
    CuadreEccEjecucion cuadreEccEjecucion = null;
    R00 r00 = null;
    R01 r01 = null;
    R02 r02 = null;
    R03 r03 = null;
    R05 r05 = null;
    R06 r06 = null;
    R07 r07 = null;

    Date fechaLiquidacion = null;

    if (an.getAll(PTIMessageRecordType.R00).size() > 0) {
      r00 = (R00) an.getAll(PTIMessageRecordType.R00).get(0);
    }

    if (an.getAll(PTIMessageRecordType.R01).size() > 0) {
      r01 = (R01) an.getAll(PTIMessageRecordType.R01).get(0);
    }

    if (an.getAll(PTIMessageRecordType.R02).size() > 0) {
      r02 = (R02) an.getAll(PTIMessageRecordType.R02).get(0);
    }

    if (an.getAll(PTIMessageRecordType.R03).size() > 0) {
      r03 = (R03) an.getAll(PTIMessageRecordType.R03).get(0);
    }

    if (an.getAll(PTIMessageRecordType.R05).size() > 0) {
      r05 = (R05) an.getAll(PTIMessageRecordType.R05).get(0);
    }

    if (an.getAll(PTIMessageRecordType.R06).size() > 0) {
      r06 = (R06) an.getAll(PTIMessageRecordType.R06).get(0);
    }

    if (an.getAll(PTIMessageRecordType.R07).size() > 0) {
      r07 = (R07) an.getAll(PTIMessageRecordType.R07).get(0);
    }

    if (r02 != null && r06 != null && r07 != null) {
      if (r01 != null) {
        fechaLiquidacion = r01.getFechaLiquidacionTeorica();
      }
      else if (r05 != null) {
        fechaLiquidacion = r05.getFechaLiquidacionTeorica();
      }

      if (fechaLiquidacion == null) {
        LOG.warn("[CuadreEccEjecucionSubListBo :: saveCuadreEccEjecucion] INCIDENCIA- Fecha de liquidacion NULL. {}",
            an);
        return cuadreEccEjecucion;
      }

      Character sentido = xasBo.getSentidoPtiSentidoSibbac(r02.getSentido()).charAt(0);

      String cdoperacionmkt = r02.getCodigoDeOperacionMercado();
      String cdoperacionMktsSinEcc = cfgBo.getTpopebolsSinEcc();
      String idEcc = r06.getIdECC();

      if (cdoperacionMktsSinEcc != null && cdoperacionmkt != null && cdoperacionMktsSinEcc.contains(cdoperacionmkt)) {
        idEcc = idEccCuadreEcc;
      }

      BigDecimal fechaLiqBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
          .convertDateToString(fechaLiquidacion));
      BigDecimal fechaEnvioBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(an
          .getFechaDeEnvio()));
      if (fechaEnvioBd.compareTo(fechaLiqBd) >= 0) {

        if (r01 != null && StringUtils.isNotBlank(r01.getNumeroOperacion())) {
          cuadreEccEjecucion = cuadreEccEjecucionDao.findByNumOpAndIdEcc(r01.getNumeroOperacion(), idEcc);

        }
        else if (r07 != null && StringUtils.isNotBlank(r07.getNumeroOperacionDCV())) {
          cuadreEccEjecucion = cuadreEccEjecucionDao
              .findByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNegAndNumOpDcv(
                  r02.getFechaNegociacion(), idEcc, r02.getCodigoDeValor(), r02.getMiembroMercado(), sentido,
                  r02.getSegmento(), r02.getCodigoDeOperacionMercado(), r02.getNumeroDeLaOrdenDeMercado() + "",
                  r02.getNumeroDeEjecucionDeMercado() + "", r02.getIdentificacionDeLaPlataforma(),
                  r07.getNumeroOperacionDCV());
        }

        if (cuadreEccEjecucion == null) {

          cuadreEccEjecucion = new CuadreEccEjecucion();

        }
        else {
          cuadreEccLogDao.deleteByIdCuadreEccEjecucion(cuadreEccEjecucion.getId());
        }
        cuadreEccEjecucion.setAuditDate(new Date());
        cuadreEccEjecucion.setAuditUser("ANFDOP");
        cuadreEccEjecucion
            .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC
                .getId());
        if (r00 != null) {
          cuadreEccEjecucion.setMiembroPropietarioCuenta(r00.getMiembro());
          cuadreEccEjecucion.setCuentaCompensacionMiembro(r00.getCuentaDeCompensacion());
          cuadreEccEjecucion.setMiembroCompensadorCuenta(r00.getMiembroCompensador());
          cuadreEccEjecucion.setEntParticipanteMiembro(r00.getEntidadParticipanteBIC());
          cuadreEccEjecucion.setCuentaLiquidacionMiembro(r00.getCuentaLiquidacion());
          if (r00.getCuentaLiquidacion().length() >= 27) {
            cuadreEccEjecucion.setTipoCuentaLiquidacionMiembro(r00.getCuentaLiquidacion().substring(24, 26));
          }
        }

        if (r01 != null) {

          if (StringUtils.isNotBlank(r01.getIndicadorAnotacion())) {
            cuadreEccEjecucion.setIndAnotacion(r01.getIndicadorAnotacion().charAt(0));
          }
          else {
            cuadreEccEjecucion.setIndAnotacion(' ');
          }
          cuadreEccEjecucion.setNumOp(r01.getNumeroOperacion());
          if (StringUtils.isNotBlank(r01.getIndicadorPosicion())) {
            cuadreEccEjecucion.setIndPosicion(r01.getIndicadorPosicion().charAt(0));
          }
          else {
            cuadreEccEjecucion.setIndPosicion(' ');
          }
          if (StringUtils.isNotBlank(r01.getCodigoOperacion())) {
            cuadreEccEjecucion.setCodOp(r01.getCodigoOperacion().charAt(0));
          }
          else {
            cuadreEccEjecucion.setCodOp(' ');
          }
          cuadreEccEjecucion.setRefAsignacionExt(r01.getReferenciaAsignacionExterna());
          cuadreEccEjecucion.setMnemotecnico(r01.getMnemotecnico());
          cuadreEccEjecucion.setFechaContratacion(r01.getFechaContratacion());
          cuadreEccEjecucion.setFechaLiq(r01.getFechaLiquidacionTeorica());
          cuadreEccEjecucion.setFechaRegistroEcc(r01.getFechaDeRegistroEnLaECC());
          cuadreEccEjecucion.setHoraRegistro(r01.getHoraRegistro());
          cuadreEccEjecucion.setValoresImpNominal(r01.getValoresImportNominal());
          cuadreEccEjecucion.setValoresImpNominalPendienteCuadre(r01.getValoresImportNominal());
          cuadreEccEjecucion.setValoresImpNominalDisponible(r01.getValoresImportNominalDisponibles());
          cuadreEccEjecucion.setEfectivoDisponible(r01.getEfectivoDisponible());
          cuadreEccEjecucion.setValoresImpNominalRetenido(r01.getValoresImporteNominalRetenidos());
          cuadreEccEjecucion.setEfectivoRetenido(r01.getEfectivoRetenido());
          cuadreEccEjecucion.setEfectivo(r01.getEfectivo());
          cuadreEccEjecucion.setNumOpPrevia(r01.getNumeroOperacionPrevia());
          cuadreEccEjecucion.setNumOpInicial(r01.getNumeroOperacionInicial());
          cuadreEccEjecucion.setRefComun(r01.getReferenciaComun());
          cuadreEccEjecucion.setCorretaje(r01.getCorretaje());
          cuadreEccEjecucion.setDivisa(r01.getDivisa());
        }
        // R02
        cuadreEccEjecucion.setValoresImpNominalEje(r02.getValoresImporteNominal());
        cuadreEccEjecucion.setPlataformaNeg(r02.getIdentificacionDeLaPlataforma());
        cuadreEccEjecucion.setSegmentoNeg(r02.getSegmento());
        cuadreEccEjecucion.setFechaNeg(r02.getFechaNegociacion());
        cuadreEccEjecucion.setHoraNeg(r02.getHoraNegociacion());
        cuadreEccEjecucion.setNumEjeMercado(r02.getNumeroDeEjecucionDeMercado());
        cuadreEccEjecucion.setCodOpMercado(r02.getCodigoDeOperacionMercado());
        cuadreEccEjecucion.setMiembroMercadoNeg(r02.getMiembroMercado());
        if (StringUtils.isNotBlank(r02.getIndicadorCotizacion())) {
          cuadreEccEjecucion.setIndCotizacion(r02.getIndicadorCotizacion().charAt(0));
        }
        else {
          cuadreEccEjecucion.setIndCotizacion(' ');
        }
        cuadreEccEjecucion.setIsin(r02.getCodigoDeValor());
        cuadreEccEjecucion.setSentido(sentido);

        cuadreEccEjecucion.setPrecio(r02.getPrecio());
        // FIXME FALTA EFECTIVOEJE
        // cuadreEccEjecucion.setEfectivo(r02.getImporteEfectivo());
        cuadreEccEjecucion.setFechaOrdenMercado(r02.getFechaDeLaOrdenDeMercado());
        cuadreEccEjecucion.setHoraOrdenMercado(r02.getHoraDeLaOrdenDeMercado());
        cuadreEccEjecucion.setNumOrdenMercado(r02.getNumeroDeLaOrdenDeMercado());
        cuadreEccEjecucion.setUsuario(r02.getUsuario());
        cuadreEccEjecucion.setRefCliente(r02.getReferenciaDeCliente());
        cuadreEccEjecucion.setRefExterna(r02.getReferenciaExterna());
        if (StringUtils.isNotBlank(r02.getIndicadorCapacidad())) {
          cuadreEccEjecucion.setIndCapacidad(r02.getIndicadorCapacidad().charAt(0));
        }
        else {
          cuadreEccEjecucion.setIndCapacidad(' ');
        }
        cuadreEccEjecucion.setInfAdicionalOrd(r02.getInformacionAdicionalDeLaOrden());

        // R03
        if (r03 != null) {
          cuadreEccEjecucion.setCicloLiquidacion(r03.getCicloLiquidacion());
          cuadreEccEjecucion.setTipoInstruccionLiquidacion(r03.getTipoInstruccionLiquidacion());
          cuadreEccEjecucion.setRefEventoCorporativo(r03.getReferenciaEventoCorporativo());
        }

        if (r05 != null) {
          cuadreEccEjecucion.setValoresImpNominal(r05.getValoresImporteNominal());
          cuadreEccEjecucion.setValoresImpNominalPendienteCuadre(r05.getValoresImporteNominal());
          cuadreEccEjecucion.setDivisa(r05.getDivisa());
          cuadreEccEjecucion.setIdPlataformaNeg(r05.getIdentificacionDeLaPlataforma());
          cuadreEccEjecucion.setFechaOp(r05.getFechaOperacion());
          cuadreEccEjecucion.setFechaLiq(r05.getFechaLiquidacionTeorica());
          cuadreEccEjecucion.setRefOpDcv(r05.getReferenciaOperacionDCV());
          cuadreEccEjecucion.setEntParticipantePlataforma(r05.getEntidadParticipante());
          cuadreEccEjecucion.setFechaCase(r05.getFechaDeCase());
          cuadreEccEjecucion.setHoraCase(r05.getHoraDeCase());
          cuadreEccEjecucion.setTipoOpPlataforma(r05.getTipoDeOperacionPlataforma());
          cuadreEccEjecucion.setCuentaLiquidacionEje(r05.getCuentaLiquidacion());
          if (r05.getCuentaLiquidacion().length() >= 27) {
            cuadreEccEjecucion.setTipoCuentaLiquidacionEje(r05.getCuentaLiquidacion().substring(24, 26));
          }
          cuadreEccEjecucion.setOrdenanteParticipante(r05.getOrdenanteParticipante());
          cuadreEccEjecucion.setCodParticipante(r05.getCodigoParticipante());
          cuadreEccEjecucion.setCcv(r05.getCCV());
          cuadreEccEjecucion.setLiquidacionParcial(r05.getLiquidacionParcial());
          cuadreEccEjecucion.setLiquidacionTiempoReal(r05.getLiquidacionTiempoReal());
          if (StringUtils.isNotBlank(r05.getIndIntervencionEcc())) {
            cuadreEccEjecucion.setIndIntervencicionEcc(r05.getIndIntervencionEcc().charAt(0));
          }
          else {
            cuadreEccEjecucion.setIndIntervencicionEcc(' ');
          }
          cuadreEccEjecucion.setMiembroCompensadorEcc(r05.getMiembroCompensador());
          cuadreEccEjecucion.setCuentaCompensacionEcc(r05.getCuentaCompensacionComunicadaDCV());
          if (r05.getCuentaCompensacionComunicadaDCV().length() >= 27) {
            cuadreEccEjecucion.setTipoCuentaCompensacionEcc(r05.getCuentaCompensacionComunicadaDCV().substring(24, 26));
          }
          cuadreEccEjecucion.setEfectivo(r05.getImporteEfectivo());
        }

        // r06
        cuadreEccEjecucion.setIdEcc(idEcc);
        cuadreEccEjecucion.setSegmentoEcc(r06.getSegmentoECC());
        cuadreEccEjecucion.setCanonTeorico(r06.getCanonBursatilTeorico());
        cuadreEccEjecucion.setCanonAplicado(r06.getCanonBursatilAplicado());
        if (StringUtils.isNotBlank(r06.getSituacionOperacion())) {
          cuadreEccEjecucion.setSituacionOp(r06.getSituacionOperacion().charAt(0));
        }
        else {
          cuadreEccEjecucion.setSituacionOp(' ');
        }
        cuadreEccEjecucion.setTitulosFallidosOp(r06.getTitulosFallidosEnOP());

        if (r07 != null) {
          // r07
          cuadreEccEjecucion.setNumOpDcv(r07.getNumeroOperacionDCV());
          cuadreEccEjecucion.setNumOpIlEcc(r07.getNumeroOperacionILdeLaECC());
          cuadreEccEjecucion.setTipoOpDcv(r07.getTipoOperacionDCV());
          cuadreEccEjecucion.setEstadoIl(r07.getEstadoIL());
          cuadreEccEjecucion.setTitulosOpIl(r07.getTitulosOPenIL());
          cuadreEccEjecucion.setEfectivoOpIl(r07.getEfectivoOPenIL());
          cuadreEccEjecucion.setTitulosFallidosOpIl(r07.getTitulosFallidosOPenIL());
        }

        if (r02.getValoresImporteNominal() != null
            && (cuadreEccEjecucion.getValoresImpNominal() == null || cuadreEccEjecucion.getValoresImpNominal()
                .compareTo(BigDecimal.ZERO) == 0)) {
          cuadreEccEjecucion.setValoresImpNominal(r02.getValoresImporteNominal());
        }
        if (r02.getValoresImporteNominal() != null
            && (cuadreEccEjecucion.getValoresImpNominalPendienteCuadre() == null || cuadreEccEjecucion
                .getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) == 0)) {
          cuadreEccEjecucion.setValoresImpNominalPendienteCuadre(r02.getValoresImporteNominal());
        }

        if (r02.getImporteEfectivo() != null
            && (cuadreEccEjecucion.getEfectivo() == null || cuadreEccEjecucion.getEfectivo().compareTo(BigDecimal.ZERO) == 0)) {
          cuadreEccEjecucion.setEfectivo(r02.getImporteEfectivo());
        }

        cuadreEccEjecucion = cuadreEccEjecucionDao.save(cuadreEccEjecucion);

      }
      else {
        LOG.debug(
            "[CuadreEccEjecucionSubListBo :: saveCuadreEccEjecucion] Ignorado por F.Envio : {} < F.Liquidacion : {} , AN : {}.",
            an.getFechaDeEnvio(), fechaLiquidacion, an);
      }
    }
    LOG.trace("[CuadreEccEjecucionSubListBo :: saveCuadreEccEjecucion] fin.");
    return cuadreEccEjecucion;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void saveEjecucionesFromPtiEccFile(List<AN> ans) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccEjecucionSubListBo :: saveEjecucionesFromPtiEccFile] inicio.");
    for (AN an : ans) {
      try {
        saveCuadreEccEjecucion(an);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException("Incidencia con el AN: " + an, e);
      }
    }// fin for
    LOG.debug("[CuadreEccEjecucionSubListBo :: saveEjecucionesFromPtiEccFile] fin.");
  }
}
