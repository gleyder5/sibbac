package sibbac.business.comunicaciones.euroccp.ciffile.enums;

public enum EuroCcpProductGroupCode {

  CLAIMS("CL"),
  BOND("BO"),
  STOCK_DIVIDEND("SD"),
  STOCK("ST"),
  RIGHTS("RI");
  public String text;

  private EuroCcpProductGroupCode(String text) {
    this.text = text;
  }
}
