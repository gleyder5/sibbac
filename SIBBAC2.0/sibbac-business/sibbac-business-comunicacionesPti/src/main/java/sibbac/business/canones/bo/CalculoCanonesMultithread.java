package sibbac.business.canones.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.canones.model.CanonSolicitaRecalculo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class CalculoCanonesMultithread {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CalculoCanonesMultithread.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private ReglasCanonesBo reglasCanonesBo;

  @Autowired
  private CalculoCanonesWithUserLock calculoCanon;

  @Autowired
  private CanonSolicitaRecalculoBo canonSolicitaRecalculoBo;

  public void procesaSolicitudesRecalculo() throws SIBBACBusinessException {
    final List<CanonSolicitaRecalculo> solicitudes;
    final SolicitudesThreadExecutor executor;

    solicitudes = canonSolicitaRecalculoBo.findAll();
    executor = new SolicitudesThreadExecutor(ctx, solicitudes, 5);
    executor.execute();
  }

  public void bajaCanonCalculadoDesglosesBaja(boolean isScriptDividend, String auditUser)
      throws SIBBACBusinessException {
    char isscrdiv = isScriptDividend ? 'S' : 'N';
    List<Tmct0bokId> listBookId = alcBo.findAllTmct0bokIdByCanonCalculadoAndBaja('S', isscrdiv);
    if (!listBookId.isEmpty()) {
      for (Tmct0bokId tmct0bokId : listBookId) {
        calculoCanon.bajaCanonCalculadoDesglosesBaja(tmct0bokId, auditUser);
      }
    }

  }

  @Transactional
  public void calcularCanon(boolean isScriptDividend, String auditUser) throws SIBBACBusinessException {
    List<Object[]> listAgrupacion;
    char isscrdiv = isScriptDividend ? 'S' : 'N';
    LOG.debug("[calcularCanon] buscando fechas con operaciones sin calcular el canon...");
    List<Date> listFechas = alcBo.findAllDistinctFetradeByCanonCalculadoAndConReferenciaTitular('N');
    LOG.debug("[calcularCanon] encontradas {}-{} fechas con operaciones sin calcular el canon...", listFechas.size(),
        listFechas);
    for (Date fecha : listFechas) {
      LOG.debug("[calcularCanon] buscando agrupaciones para la fecha: {}.", fecha);
      listAgrupacion = alcBo
          .findAllDistinctFetradeCdisinCdtpoperByFeejeliqAndCanonCalculadoAndConReferenciaTitularAndCasado(fecha, 'N',
              isscrdiv);

      LOG.debug("[calcularCanon] encontradas {} fechas con operaciones sin calcular el canon...", listAgrupacion.size());
      if (!listAgrupacion.isEmpty()) {
        this.calcularCanon(isScriptDividend, listAgrupacion, auditUser);
      }
    }
  }

  private void calcularCanon(boolean isScriptDividend, List<Object[]> listAgrupacion, String auditUser)
      throws SIBBACBusinessException {
    String msg;
    String datosAgrupacion;

    CalculoCanonesCallable calculoCanonCallable;
    if (CollectionUtils.isNotEmpty(listAgrupacion)) {

      try (CanonesConfigDTO configCanones = reglasCanonesBo.getCanonesConfig()) {
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        if (isScriptDividend) {
          tfb.setNameFormat("Thread-CalculoCanon-SD-%d");
        }
        else {
          tfb.setNameFormat("Thread-CalculoCanon-%d");
        }
        ExecutorService executor = Executors.newFixedThreadPool(10, tfb.build());
        Future<Void> future;
        List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
        Map<Future<Void>, CalculoCanonesCallable> mapCallables = new HashMap<Future<Void>, CalculoCanonesCallable>();
        try {
          for (Object[] agrupacion : listAgrupacion) {
            try {
              calculoCanonCallable = ctx.getBean(CalculoCanonesCallable.class, isScriptDividend, (Date) agrupacion[0],
                  (String) agrupacion[1], (Character) agrupacion[2], configCanones, auditUser);

              future = executor.submit(calculoCanonCallable);
              listFutures.add(future);
              mapCallables.put(future, calculoCanonCallable);
            }
            catch (Exception e) {
              msg = String.format("Incidencia lanzando hilo para  %s %s", agrupacion, e.getMessage());
              throw new SIBBACBusinessException(msg, e);
            }
          }

          executor.shutdown();
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();

        }
        finally {

          try {
            // esperamos a los hilos 10 minutos por hilo + 30 minutos de minimo
            executor.awaitTermination(2 * listAgrupacion.size() + 10, TimeUnit.HOURS);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }

          for (Future<Void> future2 : listFutures) {
            calculoCanonCallable = mapCallables.get(future2);
            datosAgrupacion = null;
            if (calculoCanonCallable != null) {
              datosAgrupacion = calculoCanonCallable.getId();
              LOG.trace("[crearTablasTitulares] revisando resultado hilo : {}", datosAgrupacion);
            }
            try {
              future2.get(2, TimeUnit.MINUTES);
            }
            catch (InterruptedException | ExecutionException e) {
              LOG.warn("[crearTablasTitulares] incidencia con  {} {}", datosAgrupacion, e.getMessage(), e);
            }
            catch (TimeoutException e) {
              if (!future2.isDone() && future2.cancel(true)) {
                LOG.warn("[crearTablasTitulares] cancelando el future: {} : {}", future2, datosAgrupacion);
              }
              else {
                LOG.warn("[crearTablasTitulares] incidencia con  {} {}", datosAgrupacion, e.getMessage(), e);
              }
            }
            catch (Exception e) {
              LOG.warn("[crearTablasTitulares] incidencia con  {} {}", datosAgrupacion, e.getMessage(), e);
              // throw new SIBBACBusinessException(e.getMessage(), e);
            }
          }

          if (!executor.isTerminated()) {
            executor.shutdownNow();
            try {
              executor.awaitTermination(1, TimeUnit.MINUTES);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
            }
          }
          mapCallables.clear();
          listAgrupacion.clear();
          listFutures.clear();
        }

      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("No se ha podido recuperar las reglas de canon activas: %s",
            e.getMessage()), e);
      }
    }
  }
}
