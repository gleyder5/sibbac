package sibbac.business.desglose.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class DesgloseWithUserLock {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(DesgloseWithUserLock.class);

  @Value("${sibbac.nacional.desglose.automatico.tmct0alo.threadpool.size:5}")
  private int threadSizeAutomatico;

  @Value("${sibbac.nacional.desglose.automatico.tmct0alo.min.threaded:10}")
  private int minTmct0aloThreadedAutomatico;

  @Value("${sibbac.nacional.desglose.manual.tmct0alo.threadpool.size:5}")
  private int threadSizeManual;

  @Value("${sibbac.nacional.desglose.manual.tmct0alo.min.threaded:10}")
  private int minTmct0aloThreadedManual;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private DesgloseTransactionalBo desgloseTransaccionalBo;

  public DesgloseWithUserLock() {
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void desgloseAutomatico(Tmct0bokId id, DesgloseConfigDTO config) throws SIBBACBusinessException {
    boolean imLock = false;

    try {
      bokBo.bloquearBooking(id, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), config.getAuditUser());
      imLock = true;
      List<Tmct0aloId> listAlos = aloBo.findAllTmct0aloIdByNuordenAndNbookingAndCdestadoasig(id.getNuorden(),
          id.getNbooking(), EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSAR.getId());
      if (CollectionUtils.isNotEmpty(listAlos)) {
        if (listAlos.size() < minTmct0aloThreadedAutomatico) {
          for (Tmct0aloId tmct0aloId : listAlos) {
            desgloseTransaccionalBo.procesoDesgloseNacionalAutomatico(tmct0aloId, config);
          }
        }
        else {
          this.lanzarHilosDesglosarTmct0aloAutomatico(listAlos, config);
        }
      }

    }
    catch (LockUserException e) {
      LOG.warn(e.getMessage(), e);
    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }

    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(id, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), config.getAuditUser());
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException("No se ha podido desbloquar el booking");
        }
      }
    }
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public CallableResultDTO desgloseManual(Tmct0bokId id, List<SolicitudDesgloseManual> listSolicitudDesgloseManual,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    boolean imLock = false;
    try {
      bokBo.bloquearBooking(id, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), config.getAuditUser());
      imLock = true;
      if (CollectionUtils.isNotEmpty(listSolicitudDesgloseManual)) {
        if (listSolicitudDesgloseManual.size() < minTmct0aloThreadedManual) {
          for (SolicitudDesgloseManual solicitudDesgloseManual : listSolicitudDesgloseManual) {
            res.append(desgloseTransaccionalBo.procesoDesgloseNacionalManual(solicitudDesgloseManual, config));
          }
        }
        else {
          res.append(this.lanzarHilosDesglosarTmct0aloManual(listSolicitudDesgloseManual, config));
        }
      }
    }
    catch (LockUserException e) {
      LOG.warn(e.getMessage(), e);
      logBo
          .insertarRegistroNewTransaction(id.getNuorden(), id.getNbooking(), e.getMessage(), "", config.getAuditUser());
      res.addErrorMessage(String.format("Booking [%s/%s] %s", id.getNuorden().trim(), id.getNbooking().trim(),
          e.getMessage()));
    }
    catch (SIBBACBusinessException e) {
      res.addErrorMessage(e.getMessage());
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(id, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), config.getAuditUser());
        }
        catch (LockUserException e) {
          logBo.insertarRegistroNewTransaction(id.getNuorden(), id.getNbooking(), "No se ha podido desbloquear", "",
              config.getAuditUser());
          res.addErrorMessage(String.format("Booking [%s/%s] No se ha podido desbloquar.", id.getNuorden().trim(), id
              .getNbooking().trim()));
        }
      }
    }
    return res;
  }

  private void lanzarHilosDesglosarTmct0aloAutomatico(List<Tmct0aloId> listAlos, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    Tmct0aloId aloid;
    String msg;
    ThreadFactoryBuilder tBuilder = new ThreadFactoryBuilder();
    tBuilder.setNameFormat("Thread-DesgloseAutomaticoMultiAllocation-%d");
    ExecutorService executor = Executors.newScheduledThreadPool(threadSizeAutomatico, tBuilder.build());
    DesgloseAutomaticoTmct0aloCallable desgloseAutomaticoCallable;
    Future<Void> future;
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    Map<Future<Void>, DesgloseAutomaticoTmct0aloCallable> mapCallables = new HashMap<Future<Void>, DesgloseAutomaticoTmct0aloCallable>();
    try {
      for (Tmct0aloId tmct0aloId : listAlos) {
        LOG.debug("[lanzarHilosDesglosarTmct0aloAutomatico] lanzando hilo para procesar allo: {}", tmct0aloId);
        try {
          desgloseAutomaticoCallable = new DesgloseAutomaticoTmct0aloCallable(desgloseTransaccionalBo, config,
              tmct0aloId);
          future = executor.submit(desgloseAutomaticoCallable);
          listFutures.add(future);
          mapCallables.put(future, desgloseAutomaticoCallable);
        }
        catch (Exception e) {
          msg = String.format("Incidencia lanzando hilo para alo %s %s", tmct0aloId, e.getMessage());
          throw new SIBBACBusinessException(msg, e);
        }
      }
      executor.shutdown();
    }
    catch (Exception e1) {
      LOG.warn(e1.getMessage(), e1);
      executor.shutdownNow();
    }
    finally {
      try {
        executor.awaitTermination(5 * listAlos.size() + 10, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage(), e);
      }

      for (Future<Void> future2 : listFutures) {
        desgloseAutomaticoCallable = mapCallables.get(future2);
        aloid = null;
        if (desgloseAutomaticoCallable != null) {
          aloid = desgloseAutomaticoCallable.getAlloIdId();
          LOG.trace("[lanzarHilosDesglosarTmct0aloAutomatico] revisando resultado hilo alo: {}", aloid);
        }
        try {
          future2.get(2, TimeUnit.MINUTES);
        }
        catch (InterruptedException | ExecutionException e) {
          LOG.warn("[lanzarHilosDesglosarTmct0aloAutomatico] incidencia con alo {} {}", aloid, e.getMessage(), e);
        }
        catch (TimeoutException e) {
          if (!future2.isDone() && future2.cancel(true)) {
            LOG.warn("[lanzarHilosDesglosarTmct0aloAutomatico] cancelando el future: {} alo: {}", future2, aloid);
          }
          else {
            LOG.warn("[lanzarHilosDesglosarTmct0aloAutomatico] incidencia con alo {} {}", aloid, e.getMessage(), e);
          }
        }
        catch (Exception e) {
          LOG.warn("[lanzarHilosDesglosarTmct0aloAutomatico] incidencia con alo {} {}", aloid, e.getMessage(), e);
          // throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }

      if (!executor.isTerminated()) {
        executor.shutdownNow();
        try {
          executor.awaitTermination(1, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
      }
    }

  }

  private CallableResultDTO lanzarHilosDesglosarTmct0aloManual(List<SolicitudDesgloseManual> listAlos,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    SolicitudDesgloseManual aloid;
    String msg;
    ThreadFactoryBuilder tBuilder = new ThreadFactoryBuilder();
    tBuilder.setNameFormat(String.format("Thread-DesgloseManualMultiAllocation-%s-%s-%s",
        config.getTipoDesgloseManual(), config.getAuditUser(), "%d"));
    ExecutorService executor = Executors.newScheduledThreadPool(threadSizeManual, tBuilder.build());
    DesgloseManualTmct0aloCallable desgloseAutomaticoCallable;
    Future<CallableResultDTO> future;
    List<Future<CallableResultDTO>> listFutures = new ArrayList<Future<CallableResultDTO>>();
    Map<Future<CallableResultDTO>, DesgloseManualTmct0aloCallable> mapCallables = new HashMap<Future<CallableResultDTO>, DesgloseManualTmct0aloCallable>();
    try {
      for (SolicitudDesgloseManual tmct0aloId : listAlos) {
        LOG.debug("[lanzarHilosDesglosarTmct0aloManual] lanzando hilo para procesar allo: {}", tmct0aloId);
        try {
          desgloseAutomaticoCallable = new DesgloseManualTmct0aloCallable(desgloseTransaccionalBo, config, tmct0aloId);
          future = executor.submit(desgloseAutomaticoCallable);
          listFutures.add(future);
          mapCallables.put(future, desgloseAutomaticoCallable);
        }
        catch (Exception e) {
          msg = String.format("Incidencia lanzando hilo para alo %s %s", tmct0aloId, e.getMessage());
          throw new SIBBACBusinessException(msg, e);
        }
      }
      executor.shutdown();
    }
    catch (Exception e1) {
      LOG.warn(e1.getMessage(), e1);
      executor.shutdownNow();
    }
    finally {
      try {
        executor.awaitTermination(5 * listAlos.size() + 10, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage(), e);
      }

      for (Future<CallableResultDTO> future2 : listFutures) {
        desgloseAutomaticoCallable = mapCallables.get(future2);
        aloid = null;
        if (desgloseAutomaticoCallable != null) {
          aloid = desgloseAutomaticoCallable.getSolicitudDesgloseManual();
          LOG.trace("[lanzarHilosDesglosarTmct0aloManual] revisando resultado hilo alo: {}", aloid);
        }
        try {
          res.append(future2.get(2, TimeUnit.MINUTES));
        }
        catch (InterruptedException | ExecutionException e) {
          LOG.warn("[lanzarHilosDesglosarTmct0aloManual] incidencia con alo {} {}", aloid, e.getMessage(), e);
        }
        catch (TimeoutException e) {
          if (!future2.isDone() && future2.cancel(true)) {
            LOG.warn("[lanzarHilosDesglosarTmct0aloManual] cancelando el future: {} alo: {}", future2, aloid);
          }
          else {
            LOG.warn("[lanzarHilosDesglosarTmct0aloManual] incidencia con alo {} {}", aloid, e.getMessage(), e);
          }
        }
        catch (Exception e) {
          LOG.warn("[lanzarHilosDesglosarTmct0aloManual] incidencia con alo {} {}", aloid, e.getMessage(), e);
          res.addErrorMessage(e.getMessage());
        }
      }

      if (!executor.isTerminated()) {
        executor.shutdownNow();
        try {
          executor.awaitTermination(1, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
      }
    }
    return res;
  }
}
