package sibbac.business.desglose.commons;

public enum AlgoritmoDesgloseManual {

  FEWEST, PROPORTIONAL, CLEARING_INFORMATION, PRICE, MANUAL;

}
