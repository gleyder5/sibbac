package sibbac.business.comunicaciones.traspasos.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.mo.exceptions.CodigosOperacionEccDesactualizadosException;
import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class TraspasosEccNoTransaccionalBo {

  private static final Logger LOG = LoggerFactory.getLogger(TraspasosEccNoTransaccionalBo.class);

  @Autowired
  private TraspasosEccBo traspasosEccBo;

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public CallableResultDTO procesoEnvioAsignaciones(Tmct0alcId alcId, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {

    CallableResultDTO res = new CallableResultDTO();
    try {
      return traspasosEccBo.crearAsignaciones(alcId, config);
    }
    catch (CodigosOperacionEccDesactualizadosException e) {
      traspasosEccBo.actualizarEstadoPorErrorEnvioAsignacion(alcId,
          EstadosEnumerados.ASIGNACIONES.PDTE_ACTUALIZAR_CODIGOS_OPERACION.getId(), e, config);

    }
    catch (SIBBACBusinessException e) {
      traspasosEccBo.actualizarEstadoPorErrorEnvioAsignacion(alcId,
          EstadosEnumerados.ASIGNACIONES.ERROR_DATOS_ASIGNACION.getId(), e, config);

    }
    catch (Exception e) {
      String msg = String.format("%s/%s/%s/%s %s", alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
          alcId.getNucnfliq(), e.getMessage());

      LOG.warn(msg, e);

      throw new SIBBACBusinessException(msg, e);
    }
    return res;
  }

}
