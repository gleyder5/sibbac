package sibbac.business.comunicaciones.euroccp.titulares.writer;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReferenceStaticDataDTO;
import sibbac.common.SIBBACBusinessException;

public class EuroCcpOwnershipReferenceStaticDataItemWriter implements
                                                          ItemWriter<EuroCcpOwnershipReferenceStaticDataDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpOwnershipReferenceStaticDataItemWriter.class);

  private String fileName;
  private int currRow;
  private Charset charset;
  private Workbook workBook;

  public EuroCcpOwnershipReferenceStaticDataItemWriter(String fileName, Charset charset) {
    currRow = 0;
    this.fileName = fileName;
    this.charset = charset;
  }

  public Workbook initWorkbook(List<String> headerTexts) throws SIBBACBusinessException {
    String extension = FilenameUtils.getExtension(fileName);
    LOG.info("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] file extension: {}", extension);
    if (StringUtils.isBlank(extension) || !extension.equalsIgnoreCase("XLSX") && !extension.equalsIgnoreCase("XLS")) {
      throw new SIBBACBusinessException("INCIDENCIA- Extension de fichero no reconocida: " + fileName);
    }
    try {
      if (extension.equalsIgnoreCase("XLSX")) {
        LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] creando objeto : {}",
                  SXSSFWorkbook.class);
        workBook = new SXSSFWorkbook();
        ((SXSSFWorkbook) workBook).setCompressTempFiles(true);
        // LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] Aniadiendo contrasenia de modificacion...");
        // ((SXSSFWorkbook) workBook).setRevisionsPassword("Ps$349ñ8713ç919¡'*`319873",
        // HashAlgorithm.fromString("SHA-256"));
        LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] Aniadida contrasenia de modificacion.");
      } else {
        LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] creando objeto : {}",
                  HSSFWorkbook.class);
        workBook = new HSSFWorkbook();
      }
    } catch (Exception e) {
      LOG.error("INCIDENCIA- Inicializando el workbook.", e);
      throw new SIBBACBusinessException("INCIDENCIA- Inicializando el workbook.", e);
    }
    LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] creado objeto : {}", workBook);
    LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] Creando sheet HOLDERS...");
    Sheet sheet = workBook.createSheet("HOLDERS");
    addHeader(workBook, sheet, headerTexts);
    // sheet.createFreezePane(0, 3, 0, 3);
    // sheet.setDefaultColumnWidth(20);

    LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] crada sheet {}", sheet);
    return workBook;
  }

  private void addHeader(Workbook wb, Sheet sh, List<String> headerTexts) {
    if (CollectionUtils.isNotEmpty(headerTexts)) {
      if (headerTexts.size() == 1) {
        String[] headersSplit = headerTexts.get(0).split(",");
        headerTexts = Arrays.asList(headersSplit);
      }
      CellStyle style = wb.createCellStyle();
      Font font = wb.createFont();
      font.setFontName(HSSFFont.FONT_ARIAL);
      font.setBold(true);
      font.setFontHeightInPoints((short) 11);
      style.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
      // style.setFillPattern(CellStyle.SOLID_FOREGROUND);
      style.setAlignment(CellStyle.ALIGN_CENTER);
      style.setFont(font);

      Row header = sh.createRow(currRow++);
      for (int i = 0; i < headerTexts.size(); i++) {

        Cell cell = header.createCell(i);
        // cell.setCellType(CellType.STRING);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellValue(headerTexts.get(i).trim());
        cell.setCellStyle(style);
        sh.autoSizeColumn(i);
      }
    } else {
      LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] No se crea header por no estar configurada {}",
                sh);
    }
  }

  @Override
  public void write(List<? extends EuroCcpOwnershipReferenceStaticDataDTO> items) throws Exception {

    Sheet sheet = workBook.getSheetAt(0);
    for (EuroCcpOwnershipReferenceStaticDataDTO euroCcpOwnershipReferenceStaticData : items) {
      LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: initWorkbook] create row: {}", currRow);
      Row row = sheet.createRow(currRow);
      int column = 0;
      // referencia titular hay que sacarla del alc
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getOwnerReference().trim(), column++);
      // nombre
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getOwnerName().trim(), column++);
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getFirstSurname().trim(), column++);
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getSecondName().trim(), column++);

      // tipo identificacion
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getTypeIdentificacion().toString(), column++);
      // identificacion
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getIdentificationCode().trim(), column++);

      // tipo persona fisica juridica
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getNaturalLegalInd().toString(), column++);
      // codigo iso nacionalidad
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getNationality().trim(), column++);

      // nacional/extrenjero
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getNationalForeignInd().toString(), column++);
      // owner type
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getOwnerType().toString(), column++);
      // addres
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getAddress().trim(), column++);
      // ciudad
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getCity().trim(), column++);

      // codigo postal solo si espaniol
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getPostalCode().trim(), column++);
      // pais residencia
      createStringCell(row, euroCcpOwnershipReferenceStaticData.getCountryResidence().trim(), column++);
      currRow++;
    }

  }

  private void createStringCell(Row row, String val, int col) {
    LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: createStringCell] creando cell coll: {} val: {} charset: {}",
              col, val, charset);
    Cell cell = row.createCell(col);
    // cell.setCellType(CellType.STRING);
    cell.setCellType(Cell.CELL_TYPE_STRING);
    cell.setCellValue(val.trim());
    LOG.debug("[EuroCcpOwnershipReferenceStaticDataItemWriter :: createStringCell] creada cell coll: {} val: {} charset: {}",
              col, val, charset);
  }

  // private void createNumericCell(Row row, Double val, int col) {
  // Cell cell = row.createCell(col);
  // cell.setCellType(Cell.CELL_TYPE_NUMERIC);
  // cell.setCellValue(val);
  // }
  //
  // private void createDateCell(Row row, Date val, int col, String pattern) {
  //
  // Cell cell = row.createCell(col);
  // cell.setCellType(Cell.CELL_TYPE_STRING);
  // if (val != null) {
  // cell.setCellValue((new SimpleDateFormat(pattern)).format(val));
  // } else {
  //
  // }
  // }
}
