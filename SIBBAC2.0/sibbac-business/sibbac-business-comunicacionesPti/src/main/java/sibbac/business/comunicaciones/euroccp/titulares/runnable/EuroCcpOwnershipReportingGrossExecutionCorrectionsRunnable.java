package sibbac.business.comunicaciones.euroccp.titulares.runnable;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionCorrectionsFileWriterBo;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpOwnershipReportingGrossExecutionCorrectionsRunnable")
public class EuroCcpOwnershipReportingGrossExecutionCorrectionsRunnable extends
    IRunnableBean<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory
      .getLogger(EuroCcpOwnershipReportingGrossExecutionCorrectionsRunnable.class);

  @Value("${sibbac.euroccp.titulares.file.charset}")
  private Charset fileCharset;

  @Value("${sibbac.euroccp.out.path}")
  private String workPathSt;

  @Value("${sibbac.euroccp.out.tmp.folder}")
  private String tmpFolder;

  @Value("${sibbac.euroccp.out.pendientes.folder}")
  private String pendientesFolder;

  @Value("${sibbac.euroccp.out.enviados.folder}")
  private String enviadosFolder;

  @Value("${sibbac.euroccp.out.tratados.folder}")
  private String tratadosFolder;

  @Value("${sibbac.euroccp.titulares.crg.txt.file.name}")
  private String tmpFileName;

  @Value("${sibbac.euroccp.titulares.crg.file.name.client.number.last.positions}")
  private int clientNumberLastPositions;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionCorrectionsFileWriterBo")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionCorrectionsFileWriterBo euroCcpOwnershipReportingGrossExecutionsBo;

  public EuroCcpOwnershipReportingGrossExecutionCorrectionsRunnable() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> beans, int order)
      throws SIBBACBusinessException {
    Path tmpPath = Paths.get(workPathSt, tmpFolder);
    if (Files.notExists(tmpPath)) {
      throw new SIBBACBusinessException("INCIDENCIA- No existe directorio: " + tmpPath.toString());
    }

    try {
      euroCcpOwnershipReportingGrossExecutionsBo.creacionFicheroTmp(beans, tmpPath, tmpFileName,
          clientNumberLastPositions, fileCharset);
    }
    catch (Exception e) {
      getObserver().setException(e);
      LOG.debug(e.getMessage(), e);
    }
    finally {
      getObserver().sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> clone() {
    return this;
  }

}
