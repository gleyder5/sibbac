package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.PROCESSED_MONEY_MOVEMENT)
@Entity
public class EuroCcpProcessedMoneyMovement extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "SUBACCOUNT_NUMBER", length = 10, nullable = false)
  private String subaccountNumber;

  @Column(name = "OPPOSITE_PARTY_CODE", length = 6, nullable = false)
  private String oppositePartyCode;

  @Column(name = "PRODUCT_GROUP_CODE", length = 2, nullable = true)
  private String productGroupCode;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRANSACTION_DATE", nullable = false)
  private Date transactionDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "VALUE_DATE", nullable = false)
  private Date valueDate;

  @Column(name = "JOURNAL_ENTRY_AMOUNT", length = 18, scale = 2, nullable = false)
  private BigDecimal journalEntryAmount;

  @Column(name = "JOURNAL_ENTRY_AMOUNT_DC", length = 1, nullable = false)
  private Character journalEntryAmountDC;

  @Column(name = "JOURNAL_ACCOUNT_CODE", length = 4, scale = 0, nullable = false)
  private BigDecimal journalAccountCode;

  @Column(name = "GROSS_POSITION_INDICATOR", length = 1, nullable = false)
  private Character grossPositionIndicator;

  @Column(name = "CASH_BALANCE_DESCRIPTION", length = 25, nullable = true)
  private String cashBalanceDescription;

  @Column(name = "CASH_BALANCE_REFERENCE", length = 9, nullable = false)
  private String cashBalanceReference;

  @Column(name = "FILLER", length = 373, nullable = true)
  private String filler;

  public EuroCcpProcessedMoneyMovement() {
  }

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getSubaccountNumber() {
    return subaccountNumber;
  }

  public String getOppositePartyCode() {
    return oppositePartyCode;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public Date getValueDate() {
    return valueDate;
  }

  public BigDecimal getJournalEntryAmount() {
    return journalEntryAmount;
  }

  public Character getJournalEntryAmountDC() {
    return journalEntryAmountDC;
  }

  public BigDecimal getJournalAccountCode() {
    return journalAccountCode;
  }

  public Character getGrossPositionIndicator() {
    return grossPositionIndicator;
  }

  public String getCashBalanceDescription() {
    return cashBalanceDescription;
  }

  public String getCashBalanceReference() {
    return cashBalanceReference;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setSubaccountNumber(String subaccountNumber) {
    this.subaccountNumber = subaccountNumber;
  }

  public void setOppositePartyCode(String oppositePartyCode) {
    this.oppositePartyCode = oppositePartyCode;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setValueDate(Date valueDate) {
    this.valueDate = valueDate;
  }

  public void setJournalEntryAmount(BigDecimal journalEntryAmount) {
    this.journalEntryAmount = journalEntryAmount;
  }

  public void setJournalEntryAmountDC(Character journalEntryAmountDC) {
    this.journalEntryAmountDC = journalEntryAmountDC;
  }

  public void setJournalAccountCode(BigDecimal journalAccountCode) {
    this.journalAccountCode = journalAccountCode;
  }

  public void setGrossPositionIndicator(Character grossPositionIndicator) {
    this.grossPositionIndicator = grossPositionIndicator;
  }

  public void setCashBalanceDescription(String cashBalanceDescription) {
    this.cashBalanceDescription = cashBalanceDescription;
  }

  public void setCashBalanceReference(String cashBalanceReference) {
    this.cashBalanceReference = cashBalanceReference;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }
  
  

}
