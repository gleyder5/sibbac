package sibbac.business.comunicaciones.pti.cuadreecc.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a
 * {@link sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac.database.model.ElementosIFinanciero }.
 * Entity: "ElementosIFinanciero".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table(name = DBConstants.CUADRE_ECC.CUADRE_ECC_SIBBAC)
@Entity
@XmlRootElement
// @Audited
public class CuadreEccSibbac extends ATable<CuadreEccSibbac> implements java.io.Serializable {

  private static final long serialVersionUID = 2326585946760757040L;

  @Column(name = "ESTADO_PROCESADO")
  @XmlAttribute
  private int estadoProcesado;

  @XmlAttribute
  @Column(name = "NUORDEN", nullable = false, length = 20)
  private String nuorden;

  @XmlAttribute
  @Column(name = "NBOOKING", nullable = false, length = 20)
  private String nbooking;

  @XmlAttribute
  @Column(name = "NUCNFCLT", nullable = false, length = 20)
  private String nucnfclt;

  @XmlAttribute
  @Column(name = "NUCNFLIQ", nullable = true, precision = 4)
  private short nucnfliq;

  @XmlAttribute
  @Column(name = "IDDESGLOSECAMARA", nullable = true)
  private Long iddesglosecamara;

  @XmlAttribute
  @Column(name = "NUDESGLOSE", nullable = true)
  private Long nudesglose;

  @XmlAttribute
  @Column(name = "IDEJECUCIONALOCATION", nullable = true)
  private Long idejecucionalocation;

  @XmlAttribute
  @Column(name = "NUEJECUC", nullable = true, length = 20)
  private String nuejecuc;

  @XmlAttribute
  @Column(name = "ESTADO", nullable = false)
  private Integer estado;

  @XmlAttribute
  @Temporal(TemporalType.DATE)
  @Column(name = "FEOPERAC", nullable = false)
  private Date feoperac;

  @XmlAttribute
  @Temporal(TemporalType.DATE)
  @Column(name = "FEVALOR", length = 4)
  private Date fevalor = new Date();

  @XmlAttribute
  @Column(name = "CDISIN", nullable = true, length = 12)
  private String cdisin;

  @XmlAttribute
  @Column(name = "SENTIDO", nullable = true, length = 1)
  private Character sentido;

  @Column(name = "CDCAMARA", nullable = false, length = 11)
  private String cdcamara;

  @XmlAttribute
  @Column(name = "CDSEGMENTO", nullable = false, length = 2)
  private String cdsegmento;

  @XmlAttribute
  @Column(name = "CDOPERACIONMKT", nullable = false, length = 2)
  private String cdoperacionmkt;

  @XmlAttribute
  @Column(name = "CDMIEMBROMKT", nullable = false, length = 4)
  private String cdmiembromkt;

  @XmlAttribute
  @Column(name = "NUORDENMKT", nullable = false, length = 9)
  private String nuordenmkt;

  @XmlAttribute
  @Column(name = "NUEXEMKT", nullable = false, length = 9)
  private String nuexemkt;

  @XmlAttribute
  @Column(name = "NUM_TITULOS_DES_SIBBAC", nullable = false, precision = 16, scale = 5)
  private BigDecimal numTitulosDesSibbac;

  @XmlAttribute
  @Column(name = "NUM_TITULOS_DES_SIBBAC_PDTE", nullable = false, precision = 16, scale = 5)
  private BigDecimal numTitulosDesSibbacPdte;

  @XmlAttribute
  @Column(name = "NUM_TITULOS_DES_ECC", nullable = true, precision = 16, scale = 5)
  private BigDecimal numTitulosDesEcc;

  @XmlAttribute
  @Column(name = "NUM_TITULOS_DES_ECC_PDTE", nullable = true, precision = 16, scale = 5)
  private BigDecimal numTitulosDesEccPdte;

  @XmlAttribute
  @Column(name = "REF_TITULAR", nullable = false, length = 20)
  private String refTitular;

  /**
   * referencia adicional - Referencia de uso libre
   */
  @Column(name = "REF_ADICIONAL", nullable = true, length = 20)
  @XmlAttribute
  private String refAdicional;

  /**
   * referencia titular - No podra empezar por AA� (uso reservado a el PTI)
   */
  @Column(name = "REF_TITULAR_ECC", nullable = true, length = 20)
  @XmlAttribute
  private String refTitularEcc;

  /**
   * referencia adicional - Referencia de uso libre
   */
  @Column(name = "REF_ADICIONAL_ECC", nullable = true, length = 20)
  @XmlAttribute
  private String refAdicionalEcc;

  /**
   * Numero de operacion de la ECC o del DCV segun corresponda
   */
  @Column(name = "NUM_OP", nullable = true, length = 35)
  @XmlAttribute
  private String numOp;

  /**
   * Numero de operacion de la ECC o del DCV segun corresponda
   */
  @Column(name = "NUM_OP_PREVIA", nullable = true, length = 35)
  @XmlAttribute
  private String numOpPrevia;

  /**
   * Numero de operacion de la ECC o del DCV segun corresponda
   */
  @Column(name = "NUM_OP_INICIAL", nullable = true, length = 35)
  @XmlAttribute
  private String numOpInicial;

  @XmlAttribute
  @Column(name = "CUENTA_LIQUIDACION", nullable = true, length = 35)
  private String cuentaLiquidacion;

  @XmlAttribute
  @Column(name = "TIPO_CUENTA_LIQUIDACION_IBERCLEAR", nullable = true, length = 2)
  private String tipoCuentaLiquidacionIberclear;

  @XmlAttribute
  @Column(name = "IMPRECIO", nullable = true, precision = 13, scale = 6)
  private BigDecimal imprecio;

  @XmlAttribute
  @Column(name = "IMEFECTIVO", nullable = true, precision = 15, scale = 2)
  private BigDecimal imefectivo;

  @XmlAttribute
  @Column(name = "IMTOTBRU", precision = 18, scale = 8)
  private BigDecimal imtotbru;

  @XmlAttribute
  @Column(name = "IMCANONCOMPDV", precision = 18, scale = 8)
  private BigDecimal imcanoncompdv;

  @XmlAttribute
  @Column(name = "IMCANONCOMPEU", precision = 18, scale = 8)
  private BigDecimal imcanoncompeu;

  @XmlAttribute
  @Column(name = "IMCANONCONTRDV", precision = 18, scale = 8)
  private BigDecimal imcanoncontrdv;

  @XmlAttribute
  @Column(name = "IMCANONCONTREU", precision = 18, scale = 8)
  private BigDecimal imcanoncontreu;

  @XmlAttribute
  @Column(name = "IMCANONLIQDV", precision = 18, scale = 8)
  private BigDecimal imcanonliqdv;

  @XmlAttribute
  @Column(name = "IMCANONLIQEU", precision = 18, scale = 8)
  private BigDecimal imcanonliqeu;

  @XmlAttribute
  @Column(name = "IMCOMISN", nullable = true, precision = 18, scale = 8)
  private BigDecimal imcomisn;

  @XmlAttribute
  @Column(name = "IMFINSVB", nullable = true, precision = 18, scale = 8)
  private BigDecimal imfinsvb;

  @XmlAttribute
  @Column(name = "IMCOMSVB", precision = 18, scale = 4)
  private BigDecimal imcomsvb;

  @XmlAttribute
  @Column(name = "IMCOMBCO", precision = 18, scale = 4)
  private BigDecimal imcombco;

  @XmlAttribute
  @Column(name = "IMCOMDVO", precision = 18, scale = 4)
  private BigDecimal imcomdvo;

  @XmlAttribute
  @Column(name = "IMNTBROK", precision = 18, scale = 4)
  private BigDecimal imntbrok;

  @XmlAttribute
  @Column(name = "IMNETLIQ", precision = 18, scale = 4)
  private BigDecimal imnetliq;

  @XmlAttribute
  @Column(name = "IMNFILIQ", precision = 18, scale = 4)
  private BigDecimal imnfiliq;

  @XmlAttribute
  @Column(name = "IMCOMBRK", precision = 18, scale = 4)
  private BigDecimal imcombrk;

  @XmlAttribute
  @Column(name = "IMAJUSVB", precision = 18, scale = 4)
  private BigDecimal imajusvb;

  @XmlAttribute
  @Column(name = "IMTOTNET", nullable = true, precision = 18, scale = 8)
  private BigDecimal imtotnet;

  @XmlAttribute
  @Column(name = "IMCOMSIS", precision = 18, scale = 4, nullable = true)
  private BigDecimal imcomsis;

  @XmlAttribute
  @Column(name = "IMBANSIS", precision = 18, scale = 4, nullable = true)
  private BigDecimal imbansis;

  /**
   */
  @Column(name = "ID_CUADRE_ECC_EJECUCION")
  @XmlAttribute
  private Long idCuadreEccEjecucion;

  /**
   */
  @Column(name = "ID_CUADRE_ECC_REFERENCIA")
  @XmlAttribute
  private Long idCuadreEccReferencia;

  @Transient
  private List<CuadreEccLog> cuadreEccLogs = new ArrayList<CuadreEccLog>();

  @Transient
  private Long idgrupoeje;

  public CuadreEccSibbac() {
    super();

  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public short getNucnfliq() {
    return nucnfliq;
  }

  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  public Long getNudesglose() {
    return nudesglose;
  }

  public Long getIdejecucionalocation() {
    return idejecucionalocation;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public Integer getEstado() {
    return estado;
  }

  public Date getFeoperac() {
    return feoperac;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public String getCdisin() {
    return cdisin;
  }

  public Character getSentido() {
    return sentido;
  }

  public String getCdcamara() {
    return cdcamara;
  }

  public String getCdsegmento() {
    return cdsegmento;
  }

  public String getCdoperacionmkt() {
    return cdoperacionmkt;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  public BigDecimal getNumTitulosDesSibbac() {
    return numTitulosDesSibbac;
  }

  public BigDecimal getNumTitulosDesSibbacPdte() {
    return numTitulosDesSibbacPdte;
  }

  public BigDecimal getNumTitulosDesEcc() {
    return numTitulosDesEcc;
  }

  public BigDecimal getNumTitulosDesEccPdte() {
    return numTitulosDesEccPdte;
  }

  public String getRefTitular() {
    return refTitular;
  }

  public String getRefAdicional() {
    return refAdicional;
  }

  public String getRefTitularEcc() {
    return refTitularEcc;
  }

  public String getRefAdicionalEcc() {
    return refAdicionalEcc;
  }

  public String getNumOp() {
    return numOp;
  }

  public String getNumOpPrevia() {
    return numOpPrevia;
  }

  public String getNumOpInicial() {
    return numOpInicial;
  }

  public String getCuentaLiquidacion() {
    return cuentaLiquidacion;
  }

  public String getTipoCuentaLiquidacionIberclear() {
    return tipoCuentaLiquidacionIberclear;
  }

  public Long getIdCuadreEccEjecucion() {
    return idCuadreEccEjecucion;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNucnfliq(short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  public void setIdejecucionalocation(Long idejecucionalocation) {
    this.idejecucionalocation = idejecucionalocation;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public void setEstado(Integer estado) {
    this.estado = estado;
  }

  public void setFeoperac(Date feoperac) {
    this.feoperac = feoperac;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  public void setCdoperacionmkt(String cdoperacionmkt) {
    this.cdoperacionmkt = cdoperacionmkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  public void setNumTitulosDesSibbac(BigDecimal numTitulosDesSibbac) {
    this.numTitulosDesSibbac = numTitulosDesSibbac;
  }

  public void setNumTitulosDesSibbacPdte(BigDecimal numTitulosDesSibbacPdte) {
    this.numTitulosDesSibbacPdte = numTitulosDesSibbacPdte;
  }

  public void setNumTitulosDesEcc(BigDecimal numTitulosDesEcc) {
    this.numTitulosDesEcc = numTitulosDesEcc;
  }

  public void setNumTitulosDesEccPdte(BigDecimal numTitulosDesEccPdte) {
    this.numTitulosDesEccPdte = numTitulosDesEccPdte;
  }

  public void setRefTitular(String refTitular) {
    this.refTitular = refTitular;
  }

  public void setRefAdicional(String refAdicional) {
    this.refAdicional = refAdicional;
  }

  public void setRefTitularEcc(String refTitularEcc) {
    this.refTitularEcc = refTitularEcc;
  }

  public void setRefAdicionalEcc(String refAdicionalEcc) {
    this.refAdicionalEcc = refAdicionalEcc;
  }

  public void setNumOp(String numOp) {
    this.numOp = numOp;
  }

  public void setNumOpPrevia(String numOpPrevia) {
    this.numOpPrevia = numOpPrevia;
  }

  public void setNumOpInicial(String numOpInicial) {
    this.numOpInicial = numOpInicial;
  }

  public void setCuentaLiquidacion(String cuentaLiquidacion) {
    this.cuentaLiquidacion = cuentaLiquidacion;
  }

  public void setTipoCuentaLiquidacionIberclear(String tipoCuentaLiquidacionIberclear) {
    this.tipoCuentaLiquidacionIberclear = tipoCuentaLiquidacionIberclear;
  }

  public BigDecimal getImprecio() {
    return imprecio;
  }

  public BigDecimal getImefectivo() {
    return imefectivo;
  }

  public BigDecimal getImtotbru() {
    return imtotbru;
  }

  public BigDecimal getImcanoncompdv() {
    return imcanoncompdv;
  }

  public BigDecimal getImcanoncompeu() {
    return imcanoncompeu;
  }

  public BigDecimal getImcanoncontrdv() {
    return imcanoncontrdv;
  }

  public BigDecimal getImcanoncontreu() {
    return imcanoncontreu;
  }

  public BigDecimal getImcanonliqdv() {
    return imcanonliqdv;
  }

  public BigDecimal getImcanonliqeu() {
    return imcanonliqeu;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public BigDecimal getImfinsvb() {
    return imfinsvb;
  }

  public BigDecimal getImcomsvb() {
    return imcomsvb;
  }

  public BigDecimal getImcombco() {
    return imcombco;
  }

  public BigDecimal getImcomdvo() {
    return imcomdvo;
  }

  public BigDecimal getImntbrok() {
    return imntbrok;
  }

  public BigDecimal getImnetliq() {
    return imnetliq;
  }

  public BigDecimal getImnfiliq() {
    return imnfiliq;
  }

  public BigDecimal getImcombrk() {
    return imcombrk;
  }

  public BigDecimal getImajusvb() {
    return imajusvb;
  }

  public BigDecimal getImtotnet() {
    return imtotnet;
  }

  public BigDecimal getImcomsis() {
    return imcomsis;
  }

  public BigDecimal getImbansis() {
    return imbansis;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public void setImfinsvb(BigDecimal imfinsvb) {
    this.imfinsvb = imfinsvb;
  }

  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  public void setImcomdvo(BigDecimal imcomdvo) {
    this.imcomdvo = imcomdvo;
  }

  public void setImntbrok(BigDecimal imntbrok) {
    this.imntbrok = imntbrok;
  }

  public void setImnetliq(BigDecimal imnetliq) {
    this.imnetliq = imnetliq;
  }

  public void setImnfiliq(BigDecimal imnfiliq) {
    this.imnfiliq = imnfiliq;
  }

  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  public void setImajusvb(BigDecimal imajusvb) {
    this.imajusvb = imajusvb;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public void setImcomsis(BigDecimal imcomsis) {
    this.imcomsis = imcomsis;
  }

  public void setImbansis(BigDecimal imbansis) {
    this.imbansis = imbansis;
  }

  public void setIdCuadreEccEjecucion(Long idCuadreEccEjecucion) {
    this.idCuadreEccEjecucion = idCuadreEccEjecucion;
  }

  public Long getIdCuadreEccReferencia() {
    return idCuadreEccReferencia;
  }

  public void setIdCuadreEccReferencia(Long idCuadreEccReferencia) {
    this.idCuadreEccReferencia = idCuadreEccReferencia;
  }

  public List<CuadreEccLog> getCuadreEccLogs() {
    return cuadreEccLogs;
  }

  public void setCuadreEccLogs(List<CuadreEccLog> cuadreEccLogs) {
    this.cuadreEccLogs = cuadreEccLogs;
  }

  public Long getIdgrupoeje() {
    return idgrupoeje;
  }

  public void setIdgrupoeje(Long idgrupoeje) {
    this.idgrupoeje = idgrupoeje;
  }

  @Override
  public String toString() {
    return "CuadreEccSibbac [nuorden=" + nuorden + ", nbooking=" + nbooking + ", nucnfclt=" + nucnfclt + ", nucnfliq="
           + nucnfliq + ", iddesglosecamara=" + iddesglosecamara + ", nudesglose=" + nudesglose + ", nuejecuc="
           + nuejecuc + ", feoperac=" + feoperac + ", cdisin=" + cdisin + ", sentido=" + sentido + ", id=" + id + "]";
  }
  
  
  
  

}
