package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

@Service
public class XmlTmct0cto {

  public XmlTmct0cto() {
    super();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void accumulateNew(Tmct0gal tmct0gal, Tmct0alo tmct0alo, Tmct0cto tmct0cto) {

    tmct0cto.getId().setCdbroker(tmct0gal.getTmct0gre().getCdbroker());
    tmct0cto.getId().setCdmercad(tmct0gal.getTmct0gre().getCdmercad());

    if (tmct0cto.getNutitagr().compareTo(BigDecimal.ZERO) == 0) {

      tmct0cto.setImcammed(tmct0alo.getImcammed().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcamnet(tmct0alo.getImcamnet().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImtotbru(tmct0gal.getImbrmerc().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImtotnet(tmct0gal.getImtotnet().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setEutotbru(tmct0gal.getImbrmreu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setEutotnet(tmct0gal.getEutotnet().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImnetbrk(new BigDecimal(0));
      tmct0cto.setEunetbrk(new BigDecimal(0));
      tmct0cto.setImcomisn(tmct0gal.getImcomisn().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcomieu(tmct0gal.getImcomieu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcomdev(tmct0gal.getImcomdev().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setEucomdev(tmct0gal.getEucomdev().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImfibrdv(tmct0gal.getImfibrdv().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImfibreu(tmct0gal.getImfibreu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImsvb(tmct0gal.getImsvb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImsvbeu(tmct0gal.getImsvbeu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImbrmerc(tmct0gal.getImbrmercb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImbrmreu(tmct0gal.getImbrmreub().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImgastos(tmct0gal.getImgastosb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImgatdvs(tmct0gal.getImgatdvsb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImimpdvs(tmct0gal.getImimpdvsb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImimpeur(tmct0gal.getImimpeurb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcconeu(tmct0gal.getImcconeu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcliqeu(tmct0gal.getImcliqeu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImdereeu(tmct0gal.getImdereeu().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setNutitagr(tmct0gal.getNutitagr().setScale(0, BigDecimal.ROUND_HALF_UP));
      Tmct0sta tmct0sta = new Tmct0sta(new Tmct0staId(TypeCdestado.NEW_REGISTER.getValue(), TypeCdtipest.TMCT0CTO.getValue()));
      tmct0cto.setTmct0sta(tmct0sta);
      tmct0cto.setFhaudit(new Date());
      tmct0cto.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);
    }
    else {

      tmct0cto.setImcammed(tmct0alo.getImcammed().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcamnet(tmct0alo.getImcamnet().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImtotbru(tmct0gal.getImbrmerc().add(tmct0gal.getImbrmerc()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImtotnet(tmct0cto.getImtotnet().add(tmct0gal.getImtotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setEutotbru(tmct0cto.getImbrmreu().add(tmct0gal.getImbrmreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setEutotnet(tmct0cto.getEutotnet().add(tmct0gal.getEutotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImnetbrk(new BigDecimal(0));
      tmct0cto.setEunetbrk(new BigDecimal(0));
      tmct0cto.setImcomisn(tmct0cto.getImcomisn().add(tmct0gal.getImcomisn()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcomieu(tmct0cto.getImcomieu().add(tmct0gal.getImcomieu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcomdev(tmct0cto.getImcomdev().add(tmct0gal.getImcomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setEucomdev(tmct0cto.getEucomdev().add(tmct0gal.getEucomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImfibrdv(tmct0cto.getImfibrdv().add(tmct0gal.getImfibrdv()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImfibreu(tmct0cto.getImfibreu().add(tmct0gal.getImfibreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImsvb(tmct0cto.getImsvb().add(tmct0gal.getImsvb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImsvbeu(tmct0cto.getImsvbeu().add(tmct0gal.getImsvbeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImbrmerc(tmct0cto.getImbrmerc().add(tmct0gal.getImbrmercb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImbrmreu(tmct0cto.getImbrmreu().add(tmct0gal.getImbrmreub()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImgastos(tmct0cto.getImgastos().add(tmct0gal.getImgastosb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImgatdvs(tmct0cto.getImgatdvs().add(tmct0gal.getImgatdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImimpdvs(tmct0cto.getImimpdvs().add(tmct0gal.getImimpdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImimpeur(tmct0cto.getImimpeur().add(tmct0gal.getImimpeurb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcconeu(tmct0cto.getImcconeu().add(tmct0gal.getImcconeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImcliqeu(tmct0cto.getImcliqeu().add(tmct0gal.getImcliqeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setImdereeu(tmct0cto.getImdereeu().add(tmct0gal.getImdereeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0cto.setNutitagr(tmct0cto.getNutitagr().add(tmct0gal.getNutitagr()).setScale(0, BigDecimal.ROUND_HALF_UP));

      tmct0cto.setFhaudit(new Date());
    }

    // tmct0cto.setFk_tmct0sta_12(StatusMulticenter.getStateTableJDO(StatusMulticenter.TMCT0CTO,
    // StatusMulticenter.STATE_INPUT_SYSTEM,
    // hpm));
    tmct0cto.setTmct0sta(tmct0alo.getTmct0sta());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void accumulateAdd(Tmct0gal tmct0gal, Tmct0alo tmct0alo, Tmct0cto tmct0cto) {

    tmct0cto.setImtotbru(tmct0cto.getImbrmerc().add(tmct0gal.getImbrmerc()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImtotnet(tmct0cto.getImtotnet().add(tmct0gal.getImtotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setEutotbru(tmct0cto.getImbrmreu().add(tmct0gal.getImbrmreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setEutotnet(tmct0cto.getEutotnet().add(tmct0gal.getEutotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImnetbrk(new BigDecimal(0));
    tmct0cto.setEunetbrk(new BigDecimal(0));
    tmct0cto.setImcomisn(tmct0cto.getImcomisn().add(tmct0gal.getImcomisn()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImcomieu(tmct0cto.getImcomieu().add(tmct0gal.getImcomieu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImcomdev(tmct0cto.getImcomdev().add(tmct0gal.getImcomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setEucomdev(tmct0cto.getEucomdev().add(tmct0gal.getEucomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImfibrdv(tmct0cto.getImfibrdv().add(tmct0gal.getImfibrdv()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImfibreu(tmct0cto.getImfibreu().add(tmct0gal.getImfibreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImsvb(tmct0cto.getImsvb().add(tmct0gal.getImsvb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImsvbeu(tmct0cto.getImsvbeu().add(tmct0gal.getImsvbeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImbrmerc(tmct0cto.getImbrmerc().add(tmct0gal.getImbrmercb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImbrmreu(tmct0cto.getImbrmreu().add(tmct0gal.getImbrmreub()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImgastos(tmct0cto.getImgastos().add(tmct0gal.getImgastosb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImgatdvs(tmct0cto.getImgatdvs().add(tmct0gal.getImgatdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImimpdvs(tmct0cto.getImimpdvs().add(tmct0gal.getImimpdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImimpeur(tmct0cto.getImimpeur().add(tmct0gal.getImimpeurb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImcconeu(tmct0cto.getImcconeu().add(tmct0gal.getImcconeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImcliqeu(tmct0cto.getImcliqeu().add(tmct0gal.getImcliqeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setImdereeu(tmct0cto.getImdereeu().add(tmct0gal.getImdereeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cto.setNutitagr(tmct0cto.getNutitagr().add(tmct0gal.getNutitagr()).setScale(0, BigDecimal.ROUND_HALF_UP));

    tmct0cto.setFhaudit(new Date());

  }

}
