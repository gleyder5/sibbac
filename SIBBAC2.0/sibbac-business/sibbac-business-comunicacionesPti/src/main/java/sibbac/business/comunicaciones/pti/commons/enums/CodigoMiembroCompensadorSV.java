package sibbac.business.comunicaciones.pti.commons.enums;

public enum CodigoMiembroCompensadorSV {
  SV("V025", "0038"),
  BANCO("V012", "0049");

  public String text;
  public String liquidador;

  private CodigoMiembroCompensadorSV(String text, String liquidador) {
    this.text = text;
    this.liquidador = liquidador;
  }
}
