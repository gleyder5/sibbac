package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.escalado.bo.EscalaEstadoAsignacionBo;
import sibbac.business.comunicaciones.pti.escalado.bo.ActualizacionCodigosOperacionPtiMultithreadBo;
import sibbac.business.comunicaciones.pti.escalado.bo.EscaladoEstadoMovimientosMutithreadBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_ESCALADO_ASIGNACION_SD, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0/10 7-23,0-3 ? * MON-FRI")
public class TaskEscaladoAsignacionSD extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEscaladoAsignacionSD.class);

  @Autowired
  private EscaladoEstadoMovimientosMutithreadBo escaladoEstadoMovimientosBo;

  @Autowired
  private EscalaEstadoAsignacionBo escalaAsignacionBo;

  @Autowired
  private ActualizacionCodigosOperacionPtiMultithreadBo actualizacionCodigosOperacionBo;

  public TaskEscaladoAsignacionSD() {
  }

  @Override
  public void executeTask() throws Exception {
    LOG.debug("[executeTask] inicio");
    boolean isSd = true;
    LOG.debug("[executeTask] vamos a escalar los estados de los movimientos...");
    escaladoEstadoMovimientosBo.escalarEstadosMovimientos(isSd, "SIBBAC20");
    LOG.debug("[executeTask] vamos a escalar los estados de las ordenes..");
    escalaAsignacionBo.escalarEstados(isSd);

    actualizacionCodigosOperacionBo.actualizarCodigosOperacionPtiMultithread(isSd, "SIBBAC20");
    // LOG.debug("[executeTask] vamos a escalar los estados de las ordenes SD...");
    // escalaAsignacionBo.escalarEstados(true);
    LOG.debug("[executeTask] fin");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_DAEMONS_ESCALADO_ESTADO_ASIGNACION_SD;
  }

}
