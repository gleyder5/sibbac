package sibbac.business.comunicaciones.euroccp.titulares.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpOwnershipReferenceStaticDataFields implements WrapperFileReaderFieldEnumInterface {

  OWNER_REFERENCE("ownerReference", 20, 0, WrapperFileReaderFieldType.STRING),
  OWNER_NAME("ownerName", 140, 0, WrapperFileReaderFieldType.STRING),
  FIRST_SURNAME("firstSurname", 40, 0, WrapperFileReaderFieldType.STRING),
  SECOND_NAME("secondName", 40, 0, WrapperFileReaderFieldType.STRING),
  TYPE_IDENTIFICATION("typeIdentificacion", 1, 0, WrapperFileReaderFieldType.CHAR),
  IDENTIFICATION_CODE("identificationCode", 11, 0, WrapperFileReaderFieldType.STRING),
  NATURAL_LEGAL_IND("naturalLegalInd", 1, 0, WrapperFileReaderFieldType.CHAR),
  NATIONALITY("nationality", 3, 0, WrapperFileReaderFieldType.STRING),
  NATIONAL_FOREIGN_IND("nationalForeignInd", 1, 0, WrapperFileReaderFieldType.CHAR),
  OWNER_TYPE("ownerType", 1, 0, WrapperFileReaderFieldType.CHAR),
  ADDRES("address", 40, 0, WrapperFileReaderFieldType.STRING),
  CITY("city", 20, 0, WrapperFileReaderFieldType.STRING),
  POSTAL_CODE("postalCode", 5, 0, WrapperFileReaderFieldType.STRING),
  COUNTRY_RESIDENCE("countryResidence", 3, 0, WrapperFileReaderFieldType.STRING),
  BIRTH_DATE("birthDate", 8, 0, WrapperFileReaderFieldType.STRING)
  ;

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpOwnershipReferenceStaticDataFields(String name, int length, int scale,
      WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
