package sibbac.business.comunicaciones.pti.cuadreecc.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccTitular;

public interface CuadreEccTitularDao extends JpaRepository<CuadreEccTitular, Long> {

  public List<CuadreEccTitular> findByCuadreEccReferencia(CuadreEccReferencia ti);

  @Modifying
  @Query("DELETE FROM CuadreEccTitular o WHERE o.cuadreEccReferencia.id IN ( SELECT m.id FROM CuadreEccReferencia m "
         + " WHERE m.fechaEje = :fechaEjecucion AND m.numOp = :noper1 " + " AND m.sentido = :sentido ) ")
  Integer deleteByFechaejecucionAndNumerooperacionAndSentido(@Param("fechaEjecucion") Date fechaEjecucion,
                                                             @Param("noper1") String numerooperacion1,
                                                             @Param("sentido") Character sentido);

  @Modifying
  @Query("DELETE FROM CuadreEccTitular o WHERE o.cuadreEccReferencia.id = :idReferencia")
  Integer deleteByIdCuadreEccReferencia(@Param("idReferencia") long idReferencia);
  
  @Query("SELECT o.id  FROM CuadreEccTitular o WHERE o.cuadreEccReferencia.id = :id ")
  List<Long> findIdByCuadreEccReferenciaId(@Param("id") Long idTi);

  @Query("SELECT o  FROM CuadreEccTitular o WHERE o.cuadreEccReferencia.id = :id ")
  List<CuadreEccTitular> findByCuadreEccReferenciaId(@Param("id") Long idTi);
}
