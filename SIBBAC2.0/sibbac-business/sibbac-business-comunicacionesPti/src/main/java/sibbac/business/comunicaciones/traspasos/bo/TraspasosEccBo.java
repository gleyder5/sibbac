package sibbac.business.comunicaciones.traspasos.bo;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.realigment.bo.EuroCcpExecutionRealigmentBo;
import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.mo.bo.MovimientosBo;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoAssigmentDataException;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoPtiMensajeDataException;
import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessagesBo;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageVersion;

@Service
public class TraspasosEccBo {

  private static final Logger LOG = LoggerFactory.getLogger(TraspasosEccBo.class);

  @Value("${jms.pti.queue.online.output.queue}")
  private String moDestinationSt;

  @Value("${jms.pti.qmanager.name}")
  private String ptiJndiName;

  @Value("${jms.pti.qmanager.host}")
  private String ptiMqHost;

  @Value("${jms.pti.qmanager.port}")
  private int ptiMqPort;

  @Value("${jms.pti.qmanager.channel}")
  private String ptiMqChannel;

  @Value("${jms.pti.qmanager.qmgrname}")
  private String ptiMqManagerName;

  @Value("${sibbac.realigment.cdestadoasig.lock.send}")
  private String listCdestadoasigLockSend;

  @Value("${sibbac.realigment.cdestadoentrec.gt.lock.send}")
  private Integer cdestadoentrecGtLockSend;

  @Value("${sibbac.realigment.pti.transfer.hour.end}")
  private BigDecimal horaCorteTraspaso;
  @Value("${sibbac.realigment.pti.give.up.hour.end}")
  private BigDecimal horaCorteGiveUp;
  @Value("${sibbac.realigment.pti.mab.transfer.hour.end}")
  private BigDecimal horaCorteTraspasoMab;
  @Value("${sibbac.realigment.pti.mab.give.up.hour.end}")
  private BigDecimal horaCorteGiveUpMab;
  @Value("${sibbac.realigment.euroccp.transfer.hour.end}")
  private BigDecimal horaCorteTraspasoEuroccp;

  @Value("${sibbac.nacional.segmentos.mab}")
  private String segmentosMab;

  @Value("${sibbac.nacional.segmentos.sin.ecc}")
  private String segmentosSinEcc;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0estadoBo estadoBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0menBo menBo;

  @Autowired
  private MovimientosBo movimientosPtiBo;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsMessageBo;

  @Autowired
  private EuroCcpExecutionRealigmentBo movimientosEuroCcp;

  @Autowired
  private Tmct0CuentasDeCompensacionBo ccBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;

  @Autowired
  private PtiCommomns ptiCommons;

  public TraspasosEccBo() {
  }

  protected ConnectionFactory getConnectionFactory() throws SIBBACBusinessException {
    LOG.trace("[getConnectionFactory] buscando conexion jms con pti...");
    ConnectionFactory cf;
    try {
      cf = jmsMessageBo.getConnectionFactoryFromContext(ptiJndiName);
    }
    catch (NamingException e) {
      try {
        cf = jmsMessageBo.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
      }
      catch (JMSException e1) {
        throw new SIBBACBusinessException("Imposible recuperar conexion con camara nacional.", e1);
      }
    }
    return cf;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void actualizarEstadoPorErrorEnvioAsignacion(Tmct0alcId alcId, int estado, Exception e,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {

    String msg = String.format("%s/%s/%s/%s %s", alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
        alcId.getNucnfliq(), e.getMessage());

    LOG.warn(msg, e);

    String msg1 = String.format("No se ha podido hacer la asignacion por: %s", e.getMessage());

    Tmct0estado estadoHb = estadoBo.findById(estado);

    if (estadoHb == null) {
      throw new SIBBACBusinessException(MessageFormat.format("El estado: {0} no se encuentra en la bbdd.", estado));
    }

    Tmct0alc alc = alcBo.findById(alcId);
    List<Tmct0desglosecamara> listDc = alc.getTmct0desglosecamaras();

    for (Tmct0desglosecamara dc : listDc) {

      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && dc.getTmct0estadoByCdestadoasig().getIdestado() != estado) {
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg1, dc
            .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());

        msg = String.format("Cambiando estado de dc: '%s' de '%s' a '%s'", dc.getIddesglosecamara(), dc
            .getTmct0estadoByCdestadoasig().getNombre(), estadoHb.getNombre());

        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, dc
            .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());

        dcBo.ponerEstadoAsignacion(dc, estado, config.getAuditUser());
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public CallableResultDTO crearAsignaciones(Tmct0alcId alcId, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {
    String msg;
    CallableResultDTO res = new CallableResultDTO();

    if (alcId == null) {
      throw new SIBBACBusinessException("El alc es obligatorio.");
    }

    TraspasosEccConfigDTO configFor;
    Tmct0infocompensacion ic;
    List<Tmct0desgloseclitit> listDesglosesEnviar;
    List<String> camaras = new ArrayList<String>();
    camaras.add(config.getCamaraNacional());
    camaras.add(config.getCamaraEuroccp());

    List<PTIMessage> mensajes = new ArrayList<PTIMessage>();
    LOG.debug("[crearAsignaciones] buscando desgloses en estado PDTE ENVIAR... para el alc {}", alcId);
    List<Tmct0desglosecamara> listDcs = dcBo.findAllByTmct0alcIdAndCdestadoasigAndInCamarasCompensacion(alcId,
        EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId(), camaras);
    if (CollectionUtils.isNotEmpty(listDcs)) {

      if ((StringUtils.isNotBlank(config.getCtaCompensacionDestino()) || StringUtils.isNotBlank(config
          .getCtaCompensacionOrigen()))
          && StringUtils.equals(config.getCtaCompensacionDestino(), config.getCtaCompensacionOrigen())) {
        throw new SIBBACBusinessException("La cuenta origen y destino no pueden ser iguales.");
      }

      Tmct0bokId bookingId = new Tmct0bokId(alcId.getNbooking(), alcId.getNuorden());

      // validamos el case
      bokBo.validarCasePti(bookingId);

      LOG.trace("[crearAsignaciones] camaras configuradas: {} {}", config.getCamaraEuroccp(),
          config.getCamaraNacional());

      for (Tmct0desglosecamara dc : listDcs) {
        LOG.trace("[crearAsignaciones] creando configuracion necesaria para hacer la asignacion...");
        ic = dc.getTmct0infocompensacion();

        configFor = this.getConfigAsignacion(config, ic);

        listDesglosesEnviar = this.getDesglosesEnviarAsignaciones(alcId, dc, configFor);

        if (CollectionUtils.isNotEmpty(listDesglosesEnviar)) {

          this.validacionEstadosEnvio(dc, configFor);

          this.crearMovimientosSinEnviarCuentaDestinoIgualOrigen(alcId, dc, listDesglosesEnviar, configFor);

          // el metodo anterior puede quitar elementos de la lista a enviar, por
          // ello debemos volver a validar que la lista no es vacia
          if (CollectionUtils.isNotEmpty(listDesglosesEnviar)) {

            if (configFor.getCamaraNacional().trim().equals(dc.getTmct0CamaraCompensacion().getCdCodigo().trim())) {
              LOG.trace("[crearAsignaciones] inicio modulo envio traspasos a PTI...");
              this.validacionHorasEnvioCamaraNacional(dc, configFor);
              mensajes.addAll(movimientosPtiBo.crearMensajesTraspaso(alcId, listDesglosesEnviar, configFor, res));

            }
            else if (configFor.getCamaraEuroccp().trim().equals(dc.getTmct0CamaraCompensacion().getCdCodigo().trim())) {

              this.validacionHorasEnvioCamaraEuroccpAsignacion(dc, configFor);
              BigDecimal horaActual = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
                  .convertTimeToString(new Date()));
              if (horaActual.compareTo(config.getHoraCorteTraspasoEuroccp()) >= 0) {
                LOG.debug("No se puede hacer un traspaso a la camara {} despues de las {} horas.", dc
                    .getTmct0CamaraCompensacion().getCdCodigo(), config.getHoraCorteTraspasoEuroccp());
                continue;
              }
              else {
                movimientosEuroCcp.insertTraspasoEuroCcp(alcId, listDesglosesEnviar, configFor, res);
              }
            }
            else {
              msg = String.format("Camara no reconocida '%s'", dc.getTmct0CamaraCompensacion().getCdCodigo());
              throw new SIBBACBusinessException(msg);
            }
          }

        }
        else {
          msg = "No hay desgloses que enviar en las asignaciones.";
          LOG.warn("[{}] - {}", alcId, msg);
          logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, dc
              .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
          dcBo.ponerEstadoAsignacion(dc, EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId(),
              config.getAuditUser());
        }

      }

      dcBo.flush();
      if (CollectionUtils.isNotEmpty(mensajes)) {
        movimientosPtiBo.sendMessagesToMq(mensajes, config);
      }

    }

    return res;
  }

  private TraspasosEccConfigDTO getConfigAsignacion(TraspasosEccConfigDTO config, Tmct0infocompensacion ic) {
    TraspasosEccConfigDTO configFor = new TraspasosEccConfigDTO();
    configFor.setPtiMessageDestintion(config.getPtiMessageDestintion());
    configFor.setPtiMessageVersion(config.getPtiMessageVersion());
    configFor.setCamaraEuroccp(config.getCamaraEuroccp());
    configFor.setCamaraNacional(config.getCamaraNacional());
    configFor.setConnectionFactory(config.getConnectionFactory());
    configFor.setAuditUser(config.getAuditUser());
    if (StringUtils.isNotBlank(ic.getCdctacompensacion())) {
      configFor.setCtaCompensacionDestino(ic.getCdctacompensacion());
    }
    else {
      configFor.setMiembroDestino(ic.getCdmiembrocompensador());
      configFor.setRefAsignacionGiveUp(ic.getCdrefasignacion());
    }
    configFor.setHolydays(config.getHolydays());

    configFor.setWorkDaysOfWeek(config.getWorkDaysOfWeek());

    if (StringUtils.isNotBlank(ic.getCdmiembrocompensador()) && StringUtils.isNotBlank(ic.getCdrefasignacion())) {
      LOG.trace("[crearAsignaciones] buscando el flag de corretaje para el miembro {}...", ic.getCdmiembrocompensador());
      Tmct0Compensador compensador = compensadorBo.findByNbNombre(ic.getCdmiembrocompensador());
      if (compensador != null) {
        configFor.setCorretajePti(compensador.getCorretajePti());
      }
    }

    configFor.setTraspaso(false);
    configFor.getEstadosEccVivos().addAll(config.getEstadosEccVivos());
    configFor.setEstadoEnviandose(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE.getId());
    configFor.setPtiSessionOpen(config.isPtiSessionOpen());
    configFor.setAuditUser(config.getAuditUser());
    configFor.setCdestadoentrecGtNoEnviarTraspasos(config.getCdestadoentrecGtNoEnviarTraspasos());
    configFor.setHoraCorteGiveUp(config.getHoraCorteGiveUp());
    configFor.setHoraCorteGiveUpMab(config.getHoraCorteGiveUpMab());
    configFor.setHoraCorteTraspaso(config.getHoraCorteTraspaso());
    configFor.setHoraCorteTraspasoEuroccp(config.getHoraCorteTraspasoEuroccp());
    configFor.setHoraCorteTraspasoMab(config.getHoraCorteTraspasoMab());
    configFor.setEstadosAsignacionNoEnviarTraspasos(config.getEstadosAsignacionNoEnviarTraspasos());

    configFor.setSegmentosMab(config.getSegmentosMab());

    configFor.setSegmentosSinEcc(config.getSegmentosSinEcc());
    return configFor;
  }

  private List<Tmct0desgloseclitit> getDesglosesEnviarAsignaciones(Tmct0alcId alcId, Tmct0desglosecamara dc,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {
    List<Tmct0desgloseclitit> listDesglosesEnviar = new ArrayList<Tmct0desgloseclitit>();
    List<Tmct0movimientooperacionnuevaecc> listMovimientos;

    List<Tmct0desgloseclitit> listDesgloses = dc.getTmct0desgloseclitits();

    if (CollectionUtils.isNotEmpty(config.getSegmentosSinEcc())) {
      listDesgloses = dctBo.filtarByConEcc(listDesgloses, config.getSegmentosSinEcc());
    }

    for (Tmct0desgloseclitit dct : listDesgloses) {
      if (dct.getTmct0desglosecamara().getTmct0alc().getCanonCalculado() != null
          && dct.getTmct0desglosecamara().getTmct0alc().getCanonCalculado() == 'S') {
        LOG.trace("[crearAsignaciones] buscando movimientos vivos previos del desglose {}...", dct.getNudesglose());
        listMovimientos = movnBo.findByInCdestadomovAndNudesglose(config.getEstadosEccVivos(), dct.getNudesglose());

        if (CollectionUtils.isEmpty(listMovimientos)) {
          LOG.trace("[crearAsignaciones] no tiene movimientos vivos previos...");
          listDesglosesEnviar.add(dct);
        }
        else {
          LOG.trace("[crearAsignaciones] si tiene movimientos vivos previos...");
          boolean enCuentaCompensacion = true;
          for (Tmct0movimientooperacionnuevaecc movn : listMovimientos) {
            if (StringUtils.isBlank(movn.getTmct0movimientoecc().getCuentaCompensacionDestino())
                || StringUtils.equals(movn.getTmct0movimientoecc().getCuentaCompensacionDestino(),
                    config.getCtaCompensacionDestino())) {
              LOG.warn(
                  "[crearAsignaciones] el desglose {} no se puede enviar la cta origen y destino es la misma {}...",
                  dct.getNudesglose(), movn.getTmct0movimientoecc().getCuentaCompensacionDestino());
              enCuentaCompensacion = false;
              break;
            }
          }

          if (enCuentaCompensacion) {
            listDesglosesEnviar.add(dct);
          }
        }
      }

    }

    dctBo.recuperarAnotaciones(alcId, listDesglosesEnviar, config.getAuditUser());

    return listDesglosesEnviar;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public CallableResultDTO crearTraspasos(Tmct0alcId alcId, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {
    String msg;
    List<Tmct0desgloseclitit> listDcts;
    List<PTIMessage> mensajes = new ArrayList<PTIMessage>();
    CallableResultDTO res = new CallableResultDTO();
    if (alcId == null) {
      throw new SIBBACBusinessException("El alc es obligatorio.");
    }

    LOG.trace("[crearTraspasos] buscando desglosescamara del alc {}...", alcId);
    List<Tmct0desglosecamara> listDcs = dcBo.findAllByTmct0alcActivosIdAndCdestadoasig(alcId);
    if (CollectionUtils.isNotEmpty(listDcs)) {

      try {
        if ((StringUtils.isNotBlank(config.getCtaCompensacionDestino()) || StringUtils.isNotBlank(config
            .getCtaCompensacionOrigen()))
            && StringUtils.equals(config.getCtaCompensacionDestino(), config.getCtaCompensacionOrigen())) {
          throw new SIBBACBusinessException("La cuenta origen y destino no pueden ser iguales.");
        }

        LOG.trace("[crearTraspasos] camaras configuradas: {} {}", config.getCamaraEuroccp(), config.getCamaraNacional());
        Map<Long, List<Tmct0desgloseclitit>> mapDesglosesByIdDesgloseCamara = new HashMap<Long, List<Tmct0desgloseclitit>>();
        for (Tmct0desglosecamara dc : listDcs) {

          LOG.trace("[crearTraspasos] buscando desglosesclitit del desglose camra: {}", dc.getIddesglosecamara());

          listDcts = this.getListaDesglosesFiltradosTraspasos(alcId, dc, config);

          if (CollectionUtils.isEmpty(listDcts)) {
            msg = String.format("%s/%s/%s/%s Despues de filtrar no se han encontrado desgloses que traspasar.",
                alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), alcId.getNucnfliq());
            res.addErrorMessage(msg);
            LOG.warn("[crearTraspasos] {}", msg);
          }
          LOG.trace("[crearTraspasos] aniadiendo desgloses para traspasar: {} ...", listDcts);
          mapDesglosesByIdDesgloseCamara.put(dc.getIddesglosecamara(), listDcts);
        }

        for (Tmct0desglosecamara dc : listDcs) {
          listDcts = mapDesglosesByIdDesgloseCamara.get(dc.getIddesglosecamara());

          if (CollectionUtils.isNotEmpty(listDcts)) {

            this.validacionEstadosEnvio(dc, config);

            this.crearMovimientosSinEnviarCuentaDestinoIgualOrigen(alcId, dc, listDcts, config);

            if (config.getCamaraNacional().trim().equals(dc.getTmct0CamaraCompensacion().getCdCodigo().trim())) {
              LOG.trace("[crearTraspasos] inicio modulo envio traspasos a PTI...");
              this.validacionHorasEnvioCamaraNacional(dc, config);

              mensajes.addAll(movimientosPtiBo.crearMensajesTraspaso(alcId, listDcts, config, res));

            }
            else if (config.getCamaraEuroccp().trim().equals(dc.getTmct0CamaraCompensacion().getCdCodigo().trim())) {

              this.validacionHorasEnvioCamaraEuroccp(dc, config);

              movimientosEuroCcp.insertTraspasoEuroCcp(alcId, listDcts, config, res);
            }
            else {
              msg = String.format("Camara no reconocida '%s'", dc.getTmct0CamaraCompensacion().getCdCodigo());
              throw new SIBBACBusinessException(msg);
            }
          }
        }

        dcBo.flush();
        if (CollectionUtils.isNotEmpty(mensajes)) {
          movimientosPtiBo.sendMessagesToMq(mensajes, config);
        }
      }
      catch (SIBBACBusinessException e) {
        LOG.warn(e.getMessage(), e);
        msg = String.format("%s/%s/%s/%s %s", alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
            alcId.getNucnfliq(), e.getMessage());
        throw new SIBBACBusinessException(msg, e);
      }

    }
    else {
      msg = String.format("%s/%s/%s/%s No encontrados desgloses.", alcId.getNuorden(), alcId.getNbooking(),
          alcId.getNucnfclt(), alcId.getNucnfliq());
      res.addErrorMessage(msg);

      LOG.warn("[crearTraspasos] {}", msg);

    }
    return res;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED, rollbackFor = Exception.class)
  public CallableResultDTO crearTraspasosSinTitular(Tmct0aloId aloid, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {
    String msg;
    CallableResultDTO res = new CallableResultDTO();
    if (aloid == null) {
      throw new SIBBACBusinessException("El alo es obligatorio.");
    }
    try {
      movimientosPtiBo.sendMensajesMoAsignacionSinTitular(aloid, config);
    }
    catch (NoPtiMensajeDataException e1) {
      msg = "No hay mensajes que enviar.";
      LOG.warn("INCIDENCIA {}", msg);
      LOG.trace(e1.getMessage(), e1);
      aloBo.ponerEstadoAsignacionPorExcepcion(aloid, EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR.getId(),
          msg, config.getAuditUser());

    }
    catch (NoAssigmentDataException e1) {
      msg = String.format("Informacion asignacion incorrecta. %s", e1.getMessage());
      LOG.warn("INCIDENCIA {}", msg);
      LOG.trace(e1.getMessage(), e1);
      aloBo.ponerEstadoAsignacionPorExcepcion(aloid, EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR.getId(),
          msg, config.getAuditUser());
    }
    catch (SIBBACBusinessException e1) {
      if (e1.getCause() != null && e1.getCause() instanceof DataIntegrityViolationException) {
        msg = String.format("Error en los datos. %s", e1.getMessage());

      }
      else {
        msg = e1.getMessage();
      }
      LOG.warn("INCIDENCIA {}", msg);
      LOG.trace(e1.getMessage(), e1);
      aloBo.ponerEstadoAsignacionPorExcepcion(aloid, EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR.getId(),
          msg, config.getAuditUser());
    }
    catch (DataIntegrityViolationException e1) {
      msg = String.format("Error en los datos. %s", e1.getMessage());
      LOG.warn("INCIDENCIA {}", msg);
      LOG.trace(e1.getMessage(), e1);
      aloBo.ponerEstadoAsignacionPorExcepcion(aloid, EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR.getId(),
          msg, config.getAuditUser());
    }
    catch (Exception e1) {
      msg = String.format("Error indeterminado. %s", e1.getMessage());
      LOG.warn("INCIDENCIA {}", msg);
      LOG.trace(e1.getMessage(), e1);
      aloBo.ponerEstadoAsignacionPorExcepcion(aloid, EstadosEnumerados.ASIGNACIONES.INCIDENCIAS.getId(), msg,
          config.getAuditUser());
    }

    return res;
  }

  protected List<Tmct0desgloseclitit> getListaDesglosesFiltradosTraspasos(Tmct0alcId alcId, Tmct0desglosecamara dc,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {
    List<Tmct0desgloseclitit> listDcts = dctBo.findByIddesglosecamara(dc.getIddesglosecamara());

    if (CollectionUtils.isNotEmpty(config.getSegmentosSinEcc())) {
      listDcts = dctBo.filtarByConEcc(listDcts, config.getSegmentosSinEcc());
    }

    if (CollectionUtils.isNotEmpty(config.getListNudesgloses())) {
      LOG.trace("[crearTraspasos] filtrando por lista de id de tmct0desglosesclitit {}...", config.getListNudesgloses());
      listDcts = dctBo.filtarById(listDcts, config.getListNudesgloses());
    }

    if (CollectionUtils.isNotEmpty(config.getListCdoperacionecc())) {
      LOG.trace("[crearTraspasos] filtrando por lista de cdoperacionecc de tmct0desglosesclitit {}...",
          config.getListCdoperacionecc());
      listDcts = dctBo.filtarByCdoperacionecc(listDcts, config.getListCdoperacionecc());
    }

    if (StringUtils.isNotBlank(config.getCtaCompensacionOrigen())) {
      LOG.trace("[crearTraspasos] filtrando por cuenta compensacion origen de tmct0desglosesclitit {}...",
          config.getCtaCompensacionOrigen());
      listDcts = dctBo.filtarByCuentaCompensacion(listDcts, config.getCtaCompensacionOrigen(),
          config.getCtaCompensacionDestino());
    }

    List<Tmct0desgloseclitit> listDesglosesCanonCalculado = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : listDcts) {
      if (dct.getTmct0desglosecamara().getTmct0alc().getCanonCalculado() != null
          && dct.getTmct0desglosecamara().getTmct0alc().getCanonCalculado() == 'S') {
        listDesglosesCanonCalculado.add(dct);
      }
    }
    listDcts.clear();
    listDcts.addAll(listDesglosesCanonCalculado);
    listDesglosesCanonCalculado.clear();
    dctBo.recuperarAnotaciones(alcId, listDcts, config.getAuditUser());

    return listDcts;
  }

  private void validacionEstadosEnvio(Tmct0desglosecamara dc, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {

    this.validacionEstadosEnvio(dc.getTmct0estadoByCdestadoasig(), dc.getTmct0alc().getEstadoentrec(),
        dc.getTmct0estadoByCdestadoentrec(), dc.getTmct0CamaraCompensacion(), config);

  }

  private void validacionEstadosEnvio(Tmct0estado estadoAsignacion, Tmct0estado estadoS3Cliente,
      Tmct0estado estadoS3Mercado, Tmct0CamaraCompensacion camara, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {
    LOG.trace("[validacionEstadosEnvio] ejecutando validaciones de coherecia de traspasos...");
    String msg;

    if (estadoAsignacion == null) {
      msg = "El estado asignacion es obligatorio";
      LOG.warn("[validacionEstadosEnvio] {}", msg);
      throw new SIBBACBusinessException(msg);
    }

    if (CollectionUtils.isNotEmpty(config.getEstadosAsignacionNoEnviarTraspasos())
        && config.getEstadosAsignacionNoEnviarTraspasos().contains(estadoAsignacion.getIdestado())) {
      msg = String.format("El desglose en estado '%s' no permite el traspaso.", estadoAsignacion.getNombre());
      LOG.warn("[validacionEstadosEnvio] {}", msg);
      throw new SIBBACBusinessException(msg);
    }
    if (estadoS3Cliente != null) {
      if (config.getCdestadoentrecGtNoEnviarTraspasos() != null
          && estadoS3Cliente.getIdestado() > config.getCdestadoentrecGtNoEnviarTraspasos()) {
        msg = String.format("Pata cliente en estado '%s' no permite el traspaso.", estadoS3Cliente.getNombre());
        LOG.warn("[validacionEstadosEnvio] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }

    if (estadoS3Mercado != null) {
      if (config.getCdestadoentrecGtNoEnviarTraspasos() != null
          && estadoS3Mercado.getIdestado() > config.getCdestadoentrecGtNoEnviarTraspasos()) {
        msg = String.format("Pata mercado en estado '%s' no permite el traspaso.", estadoS3Mercado.getNombre());
        LOG.warn("[validacionEstadosEnvio] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }

  }

  private void validacionHorasEnvioCamaraEuroccp(Tmct0desglosecamara dc, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {

    this.validacionHorasEnvioCamaraEuroccpAsignacion(dc, config);

    BigDecimal horaActual = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertTimeToString(new Date()));

    if (horaActual.compareTo(config.getHoraCorteTraspasoEuroccp()) >= 0) {
      throw new SIBBACBusinessException(String.format(
          "No se puede hacer un traspaso a la camara %s despues de las %s horas", dc.getTmct0CamaraCompensacion()
              .getCdCodigo(), config.getHoraCorteTraspasoEuroccp()));
    }

  }

  private void validacionHorasEnvioCamaraEuroccpAsignacion(Tmct0desglosecamara dc, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {
    String msg;

    BigDecimal fechaActual = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    BigDecimal horaActual = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertTimeToString(new Date()));

    BigDecimal fechaLiquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(dc
        .getFeliquidacion()));

    BigDecimal fechaContratacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(dc
        .getFecontratacion()));

    if (StringUtils.isBlank(config.getCtaCompensacionDestino())) {
      msg = String.format("Para la camara '%s' la cuenta de compensacion es obligatoria.", config.getCamaraEuroccp());
      throw new SIBBACBusinessException(msg);
    }

    Tmct0CuentasDeCompensacion ctaDestino = ccBo.findByCdCodigo(config.getCtaCompensacionDestino());
    if (ctaDestino == null || StringUtils.isBlank(ctaDestino.getAccountNumber())) {
      msg = String.format("No encontrada configuracion EuroCcp para cta '%s'", config.getCtaCompensacionDestino());
      throw new SIBBACBusinessException(msg);
    }

    config.setAccountNumberEuroCcpDestino(ctaDestino.getAccountNumber());

    if (fechaActual.compareTo(fechaLiquidacion) >= 0) {
      throw new SIBBACBusinessException(String.format(
          "No se puede hacer traspasos para la camara: '%s' despues de la fecha de liquidacion.",
          config.getCamaraEuroccp()));
    }

    if (fechaActual.compareTo(fechaContratacion) > 0 && horaActual.compareTo(config.getHoraCorteTraspasoEuroccp()) > 0) {
      throw new SIBBACBusinessException(String.format(
          "No se puede hacer traspasos para la camara: '%s' en D+1 despues de la hora limite: '%s'.",
          config.getCamaraEuroccp(), config.getHoraCorteTraspasoEuroccp()));
    }

  }

  private void validacionHorasEnvioCamaraNacional(Tmct0desglosecamara dc, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {

    if (!config.isPtiSessionOpen()) {
      throw new SIBBACBusinessException("La sesion con PTI esta cerrada, no se pueden hacer traspasos.");
    }
    BigDecimal fechaActual = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    BigDecimal horaActual = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertTimeToString(new Date()));

    BigDecimal fechaLiquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(dc
        .getFeliquidacion()));
    BigDecimal fechaContratacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(dc
        .getFecontratacion()));

    if (fechaActual.compareTo(fechaLiquidacion) >= 0) {
      throw new SIBBACBusinessException("No se puede hacer traspasos despues de la fecha de liquidacion.");
    }

    if (fechaActual.compareTo(fechaContratacion) > 0 && fechaActual.compareTo(fechaLiquidacion) < 0) {

      // TODO ver que pasa si un desglose camara es parte mab y parte
      boolean isMab = dc.getTmct0desgloseclitits() != null && !dc.getTmct0desgloseclitits().isEmpty()
          && dc.getTmct0desgloseclitits().get(0).getCdsegmento() != null
          && config.getSegmentosMab().contains(dc.getTmct0desgloseclitits().get(0).getCdsegmento().trim());
      if (isMab) {
        if (StringUtils.isNotBlank(config.getRefAsignacionGiveUp())
            && horaActual.compareTo(config.getHoraCorteGiveUpMab()) >= 0) {
          throw new SIBBACBusinessException(String.format(
              "No se puede hacer un Give-up a la camara %s despues de las %s horas para un MAB", dc
                  .getTmct0CamaraCompensacion().getCdCodigo(), config.getHoraCorteGiveUpMab()));
        }
        else if (StringUtils.isBlank(config.getRefAsignacionGiveUp())
            && horaActual.compareTo(config.getHoraCorteTraspasoMab()) >= 0) {
          throw new SIBBACBusinessException(String.format(
              "No se puede hacer un traspaso a la camara %s despues de las %s horas para un MAB", dc
                  .getTmct0CamaraCompensacion().getCdCodigo(), config.getHoraCorteTraspasoMab()));
        }
      }
      else {
        if (StringUtils.isNotBlank(config.getRefAsignacionGiveUp())
            && horaActual.compareTo(config.getHoraCorteGiveUp()) >= 0) {
          throw new SIBBACBusinessException(String.format(
              "No se puede hacer un Give-up a la camara %s despues de las %s horas", dc.getTmct0CamaraCompensacion()
                  .getCdCodigo(), config.getHoraCorteGiveUp()));
        }
        else if (StringUtils.isBlank(config.getRefAsignacionGiveUp())
            && horaActual.compareTo(config.getHoraCorteTraspaso()) >= 0) {
          throw new SIBBACBusinessException(String.format(
              "No se puede hacer un traspaso a la camara %s despues de las %s horas", dc.getTmct0CamaraCompensacion()
                  .getCdCodigo(), config.getHoraCorteTraspaso()));
        }
      }
    }
  }

  public TraspasosEccConfigDTO getNewConfigForTraspasos(List<Long> listNudesgloses, List<String> listCdoperacionecc,
      String ctaCompensacionOrigen, String ctaCompensacionDestino, String miembroDestino, String refAsignacionGiveUp,
      String auditUser) throws SIBBACBusinessException {
    TraspasosEccConfigDTO config = new TraspasosEccConfigDTO();
    config.setListNudesgloses(listNudesgloses);
    config.setListCdoperacionecc(listCdoperacionecc);
    config.setCtaCompensacionOrigen(ctaCompensacionOrigen);
    config.setCtaCompensacionDestino(ctaCompensacionDestino);
    config.setMiembroDestino(miembroDestino);
    config.setRefAsignacionGiveUp(refAsignacionGiveUp);

    config.setAuditUser(auditUser);

    try {
      config.setConnectionFactory(getConnectionFactory());
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("Incidencia conectandose a pti %s", e.getMessage()), e);
    }

    String camaraMTF = cfgBo.getIdEuroCcp();
    if (StringUtils.isBlank(camaraMTF)) {
      throw new SIBBACBusinessException("Camara Euroccp no configurada.");
    }
    config.setCamaraEuroccp(camaraMTF);

    String camaraNacional = cfgBo.getIdPti();
    if (StringUtils.isBlank(camaraNacional)) {
      throw new SIBBACBusinessException("Camara Nacional no configurada.");
    }
    config.setCamaraNacional(camaraNacional);

    PTIMessageVersion ptiMessageVersion = cfgBo.getPtiMessageVersionFromTmct0cfg(new Date());
    if (ptiMessageVersion == null) {
      throw new SIBBACBusinessException("Version de la mensajeria pti no configurada.");

    }
    config.setPtiMessageVersion(ptiMessageVersion);

    try {
      config.setPtiSessionOpen(menBo.isSessionOpened(false));
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("Incidencia revisando la session de pti %s", e.getMessage()), e);
    }

    if (StringUtils.isBlank(moDestinationSt)) {
      throw new SIBBACBusinessException("El destino de la mensajeria pti no esta configurada.");
    }
    config.setPtiMessageDestintion(moDestinationSt);

    config.setEstadoEnviandose(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO.getId());

    config.getEstadosEccVivos().addAll(cfgBo.findEstadosEccActivos());

    if (StringUtils.isNotBlank(miembroDestino) && StringUtils.isNotBlank(refAsignacionGiveUp)) {
      Tmct0Compensador compensador = compensadorBo.findByNbNombre(miembroDestino);
      if (compensador == null) {
        throw new SIBBACBusinessException(String.format("Miembro compensador no encontrado '%s'", miembroDestino));
      }
      else {
        config.setCorretajePti(compensador.getCorretajePti());
      }
    }

    config.setHolydays(cfgBo.findHolidaysBolsa());
    config.setWorkDaysOfWeek(cfgBo.getWorkDaysOfWeek());

    if (horaCorteGiveUp == null) {
      throw new SIBBACBusinessException("Hora corte envio give up no configurada.");
    }
    if (horaCorteGiveUpMab == null) {
      throw new SIBBACBusinessException("Hora corte envio give up MAB no configurada.");
    }
    if (horaCorteTraspaso == null) {
      throw new SIBBACBusinessException("Hora corte envio traspasos no configurada.");
    }
    if (horaCorteTraspasoMab == null) {
      throw new SIBBACBusinessException("Hora corte envio traspasos MAB no configurada.");
    }

    if (horaCorteTraspasoEuroccp == null) {
      throw new SIBBACBusinessException("Hora corte envio traspasos EUROCCP no configurada.");
    }

    config.setHoraCorteGiveUp(horaCorteGiveUp);
    config.setHoraCorteGiveUpMab(horaCorteGiveUpMab);
    config.setHoraCorteTraspaso(horaCorteTraspaso);
    config.setHoraCorteTraspasoEuroccp(horaCorteTraspasoEuroccp);
    config.setHoraCorteTraspasoMab(horaCorteTraspasoMab);
    config.setCdestadoentrecGtNoEnviarTraspasos(cdestadoentrecGtLockSend);
    String[] estados = listCdestadoasigLockSend.split(",");

    config.setEstadosAsignacionNoEnviarTraspasos(new ArrayList<Integer>());
    for (String cdestadoasig : estados) {
      config.getEstadosAsignacionNoEnviarTraspasos().add(Integer.parseInt(cdestadoasig));
    }

    config.setSegmentosMab(Arrays.asList(segmentosMab.split(",")));

    config.setSegmentosSinEcc(Arrays.asList(segmentosSinEcc.split(",")));

    return config;
  }

  private void crearMovimientosSinEnviarCuentaDestinoIgualOrigen(Tmct0alcId alcId, Tmct0desglosecamara dc,
      List<Tmct0desgloseclitit> listDesgloses, TraspasosEccConfigDTO config) throws SIBBACBusinessException {
    Tmct0anotacionecc an;
    List<Tmct0movimientooperacionnuevaecc> listMovsn;
    List<Tmct0desgloseclitit> toRemove = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : listDesgloses) {
      an = dct.getAnotacion();
      if (an != null) {
        // FIXME PASAR ESTE CONTROL A EL METODO DE CREAR MOVIMIENTOS DESTINO
        // IGUAL
        listMovsn = null;
        if (dct.getNudesglose() != null) {
          listMovsn = movnBo.findByNudesglose(dct.getNudesglose());
        }
        if (config.getCtaCompensacionDestino() != null
            && !config.getCtaCompensacionDestino().trim().isEmpty()
            && an.getTmct0CuentasDeCompensacion().getCdCodigo().trim()
                .equals(config.getCtaCompensacionDestino().trim())
            && !this.hayMovimientoEnCuenta(listMovsn, config.getCtaCompensacionDestino())) {

          toRemove.add(dct);
        }
      }
    }
    if (!toRemove.isEmpty()) {

      movimientosPtiBo.crearMovimientosSinEnviarCuentaDestinoIgualOrigen(alcId, dc, toRemove, config);
      listDesgloses.removeAll(toRemove);

      toRemove.clear();
    }
  }

  private boolean hayMovimientoEnCuenta(List<Tmct0movimientooperacionnuevaecc> listMovsn, String cta) {
    boolean res = false;
    if (cta != null && !cta.trim().isEmpty() && listMovsn != null) {
      for (Tmct0movimientooperacionnuevaecc movn : listMovsn) {
        if (movn.getTmct0movimientoecc().getCdestadomov() != null
            && movn.getTmct0movimientoecc().getCdestadomov() != null
            && !movn.getTmct0movimientoecc().getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)
            && movn.getTmct0movimientoecc().getCuentaCompensacionDestino() != null
            && cta.trim().equals(movn.getTmct0movimientoecc().getCuentaCompensacionDestino().trim())) {
          res = true;
          break;
        }
      }
    }
    return res;
  }

}
