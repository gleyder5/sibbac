package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettlementInstuction;

@Component
public interface EuroCcpSettlementInstructionDao extends JpaRepository<EuroCcpSettlementInstuction, Long> {
  
  EuroCcpSettlementInstuction findBySettlementInstructionReference(String settlementInstructionReference);

}