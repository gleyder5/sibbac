package sibbac.business.comunicaciones.pti.task;

import java.util.Calendar;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.mo.bo.AsignacionMovimientosDesglosesMultithreadBo;
import sibbac.business.comunicaciones.traspasos.bo.TraspasosEccMultithreadBo;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.pti.PTIMessageFactory;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_CREACION_MO_ASIGNACION, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 9-21 ? * MON-FRI")
public class TaskCreacionMosAsignacion extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private PtiCommomns ptiCommons;

  @Autowired
  private TraspasosEccMultithreadBo traspasosBo;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  private AsignacionMovimientosDesglosesMultithreadBo asignacionMovimientosDesglosesBo;

  public TaskCreacionMosAsignacion() {
  }

  @Override
  public void executeTask() throws Exception {

    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }

    List<Integer> activationDaysCfg = tmct0cfgBo.findDiasActivacionProcesos();

    Calendar now = Calendar.getInstance();

    if (!activationDaysCfg.contains(now.get(Calendar.DAY_OF_WEEK))) {
      LOG.warn("[TaskCreacionMosAsignacion :: executeTask] No es un dia de activacion del proceso.");
      return;
    }

    if (!tmct0menBo.isSessionOpened(false)) {
      LOG.warn("[TaskCreacionMosAsignacion :: executeTask] LA SESION PTI ESTA CERRADA.");
      return;
    }

    traspasosBo.crearAsignacionesMultithread("SIBBAC20");

    traspasosBo.crearAsignacionesSinTitularMultithread("SIBBAC20");

//    asignacionMovimientosDesglosesBo.engancharMovimientosFidessaDesgloses(false, "SIBBAC20");

  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_CREACION_MO_ASIGNACION_DESGLOSE_CAMARA;
  }

}