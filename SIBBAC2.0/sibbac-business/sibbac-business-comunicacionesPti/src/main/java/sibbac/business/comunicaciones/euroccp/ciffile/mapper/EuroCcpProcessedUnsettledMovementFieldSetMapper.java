package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpProcessedUnsettledMovementFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpProcessedUnsettledMovementFieldSetMapper extends
                                                            WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean> {

  public EuroCcpProcessedUnsettledMovementFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpProcessedUnsettledMovementFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {
    return new EuroCcpProcessedUnsettledMovement();
  }

}
