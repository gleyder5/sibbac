package sibbac.business.canones.bo;

import java.util.Comparator;

import sibbac.business.wrappers.database.model.Tmct0bokId;

public class Tmct0bokIdComparator implements Comparator<Tmct0bokId> {

  @Override
  public int compare(Tmct0bokId arg0, Tmct0bokId arg1) {
    int nuorden = arg0.getNuorden().compareTo(arg1.getNuorden());
    if (nuorden == 0) {
      return arg0.getNbooking().compareTo(arg1.getNbooking());
    }
    else {
      return nuorden;
    }
  }

}
