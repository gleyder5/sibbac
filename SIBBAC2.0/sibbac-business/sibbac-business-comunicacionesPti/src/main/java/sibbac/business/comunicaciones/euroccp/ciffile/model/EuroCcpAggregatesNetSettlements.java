package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.AGGREGATES_NET_SETTLEMENTS)
@Entity
public class EuroCcpAggregatesNetSettlements extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "PRODUCT_GROUP_CODE", length = 2, nullable = false)
  private String productGroupCode;

  @Column(name = "EXCHANGE_CODE_TRADE", length = 4, nullable = true)
  private String exchangeCodeTrade;

  @Column(name = "SYMBOL", length = 6, nullable = false)
  private String symbol;

  @Column(name = "ISIN_CODE", length = 12, nullable = false)
  private String isinCode;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRANSACTION_DATE", nullable = false)
  private Date transactionDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "SETTLEMENT_DATE", nullable = false)
  private Date settlementDate;

  @Column(name = "TRANSACTION_ORIGIN", length = 4, nullable = true)
  private String transactionOrigin;

  @Column(name = "TRADE_GROUP_IDENTIFICATION", length = 1, nullable = true)
  private Character tradeGroupIdentification;

  @Column(name = "DEPOT_ID", length = 6, nullable = false)
  private String depotId;

  @Column(name = "SETTLEMENT_INSTRUCTION_REFERENCE", length = 9, nullable = false)
  private String settlementInstructionReference;

  @Column(name = "RECEIVE_CODE", length = 3, nullable = false)
  private String receiveCode;

  @Column(name = "TRANSACTION_QUANTITY_TOTAL_BUY", length = 12, scale = 2, nullable = true)
  private BigDecimal transactionQuantityTotalBuy;

  @Column(name = "DELIVER_CODE", length = 3, nullable = false)
  private String deliverCode;

  @Column(name = "TRANSACTION_QUANTITY_TOTAL_SELL", length = 12, scale = 2, nullable = true)
  private BigDecimal transactionQuantityTotalSell;

  @Column(name = "RECEIVE_DELIVER_CODE_NET", length = 3, nullable = false)
  private String receiveDeliverCodeNet;

  @Column(name = "TRANSACTION_QUANTITY_TOTAL_NET", length = 12, scale = 2, nullable = true)
  private BigDecimal transactionQuantityTotalNet;

  @Column(name = "AVERAGE_PRICE", length = 18, scale = 7, nullable = true)
  private BigDecimal averagePrice;

  @Column(name = "SETTLEMENT_AMOUNT_TOTAL_BUY", length = 18, scale = 2, nullable = true)
  private BigDecimal settlementAmountTotalBuy;

  @Column(name = "SETTLEMENT_AMOUNT_TOTAL_BUY_DC", length = 1, nullable = true)
  private Character settlementAmountTotalBuyDC;

  @Column(name = "SETTLEMENT_AMOUNT_TOTAL_SELL", length = 18, scale = 2, nullable = true)
  private BigDecimal settlementAmountTotalSell;

  @Column(name = "SETTLEMENT_AMOUNT_TOTAL_SELL_DC", length = 1, nullable = true)
  private Character settlementAmountTotalSellDC;

  @Column(name = "SETTLEMENT_AMOUNT_TOTAL_NET", length = 18, scale = 2, nullable = true)
  private BigDecimal settlementAmountTotalNet;

  @Column(name = "SETTLEMENT_AMOUNT_TOTAL_NET_DC", length = 1, nullable = true)
  private Character settlementAmountTotalNetDC;

  @Column(name = "PLACE_SAFEKEEPING", length = 11, nullable = false)
  private String placeSafekeeping;

  @Column(name = "PLACE_SETTLEMENT", length = 11, nullable = false)
  private String placeSettlement;

  @Column(name = "BUYER_SELLER_CONTEXT", length = 8, nullable = true)
  private String buyerSellerContext;

  @Column(name = "BUYER_SELLER_CODE", length = 11, nullable = true)
  private String buyerSellerCode;

  @Column(name = "BUYER_SELLER_ACCOUNT_CODE", length = 12, nullable = true)
  private String buyerSellerAccountCode;

  @Column(name = "REC_DEL_AGENT_CONTEXT", length = 8, nullable = true)
  private String recDelAgentContext;

  @Column(name = "REC_DEL_AGENT_CODE", length = 11, nullable = true)
  private String recDelAgentCode;

  @Column(name = "REC_DEL_AGENT_ACCOUNT_CODE", length = 12, nullable = true)
  private String recDelAgentAccountCode;

  @Column(name = "FILLER", length = 200, nullable = true)
  private String filler;

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public String getExchangeCodeTrade() {
    return exchangeCodeTrade;
  }

  public String getSymbol() {
    return symbol;
  }

  public String getIsinCode() {
    return isinCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public Date getSettlementDate() {
    return settlementDate;
  }

  public String getTransactionOrigin() {
    return transactionOrigin;
  }

  public Character getTradeGroupIdentification() {
    return tradeGroupIdentification;
  }

  public String getDepotId() {
    return depotId;
  }

  public String getSettlementInstructionReference() {
    return settlementInstructionReference;
  }

  public String getReceiveCode() {
    return receiveCode;
  }

  public BigDecimal getTransactionQuantityTotalBuy() {
    return transactionQuantityTotalBuy;
  }

  public String getDeliverCode() {
    return deliverCode;
  }

  public BigDecimal getTransactionQuantityTotalSell() {
    return transactionQuantityTotalSell;
  }

  public String getReceiveDeliverCodeNet() {
    return receiveDeliverCodeNet;
  }

  public BigDecimal getTransactionQuantityTotalNet() {
    return transactionQuantityTotalNet;
  }

  public BigDecimal getAveragePrice() {
    return averagePrice;
  }

  public BigDecimal getSettlementAmountTotalBuy() {
    return settlementAmountTotalBuy;
  }

  public Character getSettlementAmountTotalBuyDC() {
    return settlementAmountTotalBuyDC;
  }

  public BigDecimal getSettlementAmountTotalSell() {
    return settlementAmountTotalSell;
  }

  public Character getSettlementAmountTotalSellDC() {
    return settlementAmountTotalSellDC;
  }

  public BigDecimal getSettlementAmountTotalNet() {
    return settlementAmountTotalNet;
  }

  public Character getSettlementAmountTotalNetDC() {
    return settlementAmountTotalNetDC;
  }

  public String getPlaceSafekeeping() {
    return placeSafekeeping;
  }

  public String getPlaceSettlement() {
    return placeSettlement;
  }

  public String getBuyerSellerContext() {
    return buyerSellerContext;
  }

  public String getBuyerSellerCode() {
    return buyerSellerCode;
  }

  public String getBuyerSellerAccountCode() {
    return buyerSellerAccountCode;
  }

  public String getRecDelAgentContext() {
    return recDelAgentContext;
  }

  public String getRecDelAgentCode() {
    return recDelAgentCode;
  }

  public String getRecDelAgentAccountCode() {
    return recDelAgentAccountCode;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public void setExchangeCodeTrade(String exchangeCodeTrade) {
    this.exchangeCodeTrade = exchangeCodeTrade;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public void setIsinCode(String isinCode) {
    this.isinCode = isinCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setSettlementDate(Date settlementDate) {
    this.settlementDate = settlementDate;
  }

  public void setTransactionOrigin(String transactionOrigin) {
    this.transactionOrigin = transactionOrigin;
  }

  public void setTradeGroupIdentification(Character tradeGroupIdentification) {
    this.tradeGroupIdentification = tradeGroupIdentification;
  }

  public void setDepotId(String depotId) {
    this.depotId = depotId;
  }

  public void setSettlementInstructionReference(String settlementInstructionReference) {
    this.settlementInstructionReference = settlementInstructionReference;
  }

  public void setReceiveCode(String receiveCode) {
    this.receiveCode = receiveCode;
  }

  public void setTransactionQuantityTotalBuy(BigDecimal transactionQuantityTotalBuy) {
    this.transactionQuantityTotalBuy = transactionQuantityTotalBuy;
  }

  public void setDeliverCode(String deliverCode) {
    this.deliverCode = deliverCode;
  }

  public void setTransactionQuantityTotalSell(BigDecimal transactionQuantityTotalSell) {
    this.transactionQuantityTotalSell = transactionQuantityTotalSell;
  }

  public void setReceiveDeliverCodeNet(String receiveDeliverCodeNet) {
    this.receiveDeliverCodeNet = receiveDeliverCodeNet;
  }

  public void setTransactionQuantityTotalNet(BigDecimal transactionQuantityTotalNet) {
    this.transactionQuantityTotalNet = transactionQuantityTotalNet;
  }

  public void setAveragePrice(BigDecimal averagePrice) {
    this.averagePrice = averagePrice;
  }

  public void setSettlementAmountTotalBuy(BigDecimal settlementAmountTotalBuy) {
    this.settlementAmountTotalBuy = settlementAmountTotalBuy;
  }

  public void setSettlementAmountTotalBuyDC(Character settlementAmountTotalBuyDC) {
    this.settlementAmountTotalBuyDC = settlementAmountTotalBuyDC;
  }

  public void setSettlementAmountTotalSell(BigDecimal settlementAmountTotalSell) {
    this.settlementAmountTotalSell = settlementAmountTotalSell;
  }

  public void setSettlementAmountTotalSellDC(Character settlementAmountTotalSellDC) {
    this.settlementAmountTotalSellDC = settlementAmountTotalSellDC;
  }

  public void setSettlementAmountTotalNet(BigDecimal settlementAmountTotalNet) {
    this.settlementAmountTotalNet = settlementAmountTotalNet;
  }

  public void setSettlementAmountTotalNetDC(Character settlementAmountTotalNetDC) {
    this.settlementAmountTotalNetDC = settlementAmountTotalNetDC;
  }

  public void setPlaceSafekeeping(String placeSafekeeping) {
    this.placeSafekeeping = placeSafekeeping;
  }

  public void setPlaceSettlement(String placeSettlement) {
    this.placeSettlement = placeSettlement;
  }

  public void setBuyerSellerContext(String buyerSellerContext) {
    this.buyerSellerContext = buyerSellerContext;
  }

  public void setBuyerSellerCode(String buyerSellerCode) {
    this.buyerSellerCode = buyerSellerCode;
  }

  public void setBuyerSellerAccountCode(String buyerSellerAccountCode) {
    this.buyerSellerAccountCode = buyerSellerAccountCode;
  }

  public void setRecDelAgentContext(String recDelAgentContext) {
    this.recDelAgentContext = recDelAgentContext;
  }

  public void setRecDelAgentCode(String recDelAgentCode) {
    this.recDelAgentCode = recDelAgentCode;
  }

  public void setRecDelAgentAccountCode(String recDelAgentAccountCode) {
    this.recDelAgentAccountCode = recDelAgentAccountCode;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

}
