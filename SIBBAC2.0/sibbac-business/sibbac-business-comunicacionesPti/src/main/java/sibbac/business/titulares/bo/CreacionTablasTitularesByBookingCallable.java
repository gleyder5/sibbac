package sibbac.business.titulares.bo;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class CreacionTablasTitularesByBookingCallable implements Callable<Void> {

  private Tmct0bokId id;
  private TitularesConfigDTO config;

  @Autowired
  private CreacionTablasTitularesWithUserLock creacionTablasTitularesBo;

  public CreacionTablasTitularesByBookingCallable(Tmct0bokId id, TitularesConfigDTO config) {
    super();
    this.id = id;
    this.config = config;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    creacionTablasTitularesBo.crearTablasTitulares(id, config);
    return null;
  }

  public Tmct0bokId getId() {
    return this.id;
  }
}
