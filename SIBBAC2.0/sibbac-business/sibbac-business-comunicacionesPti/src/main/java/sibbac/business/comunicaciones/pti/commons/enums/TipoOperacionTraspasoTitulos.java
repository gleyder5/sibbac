package sibbac.business.comunicaciones.pti.commons.enums;

public enum TipoOperacionTraspasoTitulos {

  SALES_BUY("M"),
  TRANSFER("T"),
  GIVE_UP("G");

  public String text;

  private TipoOperacionTraspasoTitulos(String text) {
    this.text = text;
  }

}
