package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccLog;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;

public class CuadreEccSibbacCargaOpDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5658722053674031391L;

  private Tmct0alo tmct0alo;
  private CuadreEccSibbac cuadreEccSibbac;
  private CuadreEccEjecucion cuadreEccEjecucion;
  private List<CuadreEccLog> joins = new ArrayList<CuadreEccLog>();
  private Tmct0desglosecamara desgloseCamaraOriginal;
  private Tmct0desgloseclitit desgloseOriginal;
  private Tmct0movimientoecc movimientoOriginal;
  private Tmct0movimientooperacionnuevaecc movimientoNuevoOriginal;
  private List<Tmct0desgloseclitit> desglosesCreados = new ArrayList<Tmct0desgloseclitit>();
  private List<Tmct0movimientoecc> movimientosCreados = new ArrayList<Tmct0movimientoecc>();
  private List<Tmct0movimientooperacionnuevaecc> movimientosNuevoCreados = new ArrayList<Tmct0movimientooperacionnuevaecc>();

  public CuadreEccSibbacCargaOpDTO() {
  }

  public CuadreEccSibbac getCuadreEccSibbac() {
    return cuadreEccSibbac;
  }

  public List<CuadreEccLog> getJoins() {
    return joins;
  }

  public Tmct0desgloseclitit getDesgloseOriginal() {
    return desgloseOriginal;
  }

  public Tmct0movimientoecc getMovimientoOriginal() {
    return movimientoOriginal;
  }

  public Tmct0movimientooperacionnuevaecc getMovimientoNuevoOriginal() {
    return movimientoNuevoOriginal;
  }

  public List<Tmct0desgloseclitit> getDesglosesCreados() {
    return desglosesCreados;
  }

  public List<Tmct0movimientoecc> getMovimientosCreados() {
    return movimientosCreados;
  }

  public List<Tmct0movimientooperacionnuevaecc> getMovimientosNuevoCreados() {
    return movimientosNuevoCreados;
  }

  public void setCuadreEccSibbac(CuadreEccSibbac cuadreEccSibbac) {
    this.cuadreEccSibbac = cuadreEccSibbac;
  }

  public void setJoins(List<CuadreEccLog> joins) {
    this.joins = joins;
  }

  public void setDesgloseOriginal(Tmct0desgloseclitit desgloseOriginal) {
    this.desgloseOriginal = desgloseOriginal;
  }

  public void setMovimientoOriginal(Tmct0movimientoecc movimientoOriginal) {
    this.movimientoOriginal = movimientoOriginal;
  }

  public void setMovimientoNuevoOriginal(Tmct0movimientooperacionnuevaecc movimientoNuevoOriginal) {
    this.movimientoNuevoOriginal = movimientoNuevoOriginal;
  }

  public void setDesglosesCreados(List<Tmct0desgloseclitit> desglosesCreados) {
    this.desglosesCreados = desglosesCreados;
  }

  public void setMovimientosCreados(List<Tmct0movimientoecc> movimientosCreados) {
    this.movimientosCreados = movimientosCreados;
  }

  public void setMovimientosNuevoCreados(List<Tmct0movimientooperacionnuevaecc> movimientosNuevoCreados) {
    this.movimientosNuevoCreados = movimientosNuevoCreados;
  }

  public Tmct0alo getTmct0alo() {
    return tmct0alo;
  }

  public void setTmct0alo(Tmct0alo tmct0alo) {
    this.tmct0alo = tmct0alo;
  }

  public Tmct0desglosecamara getDesgloseCamaraOriginal() {
    return desgloseCamaraOriginal;
  }

  public void setDesgloseCamaraOriginal(Tmct0desglosecamara desgloseCamaraOriginal) {
    this.desgloseCamaraOriginal = desgloseCamaraOriginal;
  }

  public CuadreEccEjecucion getCuadreEccEjecucion() {
    return cuadreEccEjecucion;
  }

  public void setCuadreEccEjecucion(CuadreEccEjecucion cuadreEccEjecucion) {
    this.cuadreEccEjecucion = cuadreEccEjecucion;
  }

}
