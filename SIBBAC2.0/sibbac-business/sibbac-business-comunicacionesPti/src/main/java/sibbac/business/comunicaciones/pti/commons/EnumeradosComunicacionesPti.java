package sibbac.business.comunicaciones.pti.commons;

public interface EnumeradosComunicacionesPti {

  public enum TipoCuentaIberclear {
    CUENTA_PROPIA("P0", false),
    CUENTA_TERCEROS("T0", true),
    CUENTA_INDIVIDUAL("I0", false),
    CUENTA_INDIVIDUAL_LLEVANZA_DIRECTA("I1", false),
    CUENTA_INDIVIDUAL_IF("F0", false),
    CUENTA_OMNIBUS_DCV_IBERCLEAR("0M", false);

    private String tipo;
    private boolean cuentaTerceros;

    private TipoCuentaIberclear(String tipo, boolean cuentaTerceros) {
      this.setTipo(tipo);
      this.setCuentaTerceros(cuentaTerceros);
    }

    public String getTipo() {
      return tipo;
    }

    public void setTipo(String tipo) {
      this.tipo = tipo;
    }

    public boolean isCuentaTerceros() {
      return cuentaTerceros;
    }

    public void setCuentaTerceros(boolean cuentaTerceros) {
      this.cuentaTerceros = cuentaTerceros;
    }

  }

}
