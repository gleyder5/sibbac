package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.sql.Timestamp;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctdId;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

public class XmlTmct0ctd {

  private Tmct0ctd tmct0ctd;

  public XmlTmct0ctd(Tmct0cta tmct0cta) {
    super();
    tmct0ctd = new Tmct0ctd();
    tmct0ctd.setTmct0cta(tmct0cta);
    tmct0ctd.setId(new Tmct0ctdId(tmct0cta.getId().getNuorden(), tmct0cta.getId().getCdbroker(), tmct0cta.getId()
        .getCdmercad(), tmct0cta.getId().getNbooking(), tmct0cta.getId().getNucnfclt(), null));
  }

  public void accumulateNew() {

    tmct0ctd.getId().setNualosec(1);

    tmct0ctd.setCdalias(tmct0ctd.getTmct0cta().getCdalias());
    tmct0ctd.setDescrali(tmct0ctd.getTmct0cta().getDescrali());
    tmct0ctd.setImcammed(tmct0ctd.getTmct0cta().getImcammed());
    tmct0ctd.setImcamnet(tmct0ctd.getTmct0cta().getImcamnet());
    tmct0ctd.setImtotbru(tmct0ctd.getTmct0cta().getImtotbru());
    tmct0ctd.setEutotbru(tmct0ctd.getTmct0cta().getEutotbru());
    tmct0ctd.setImtotnet(tmct0ctd.getTmct0cta().getImtotnet());
    tmct0ctd.setEutotnet(tmct0ctd.getTmct0cta().getEutotnet());
    tmct0ctd.setImnetbrk(tmct0ctd.getTmct0cta().getImnetbrk());
    tmct0ctd.setEunetbrk(tmct0ctd.getTmct0cta().getEunetbrk());
    tmct0ctd.setImcomisn(tmct0ctd.getTmct0cta().getImcomisn());
    tmct0ctd.setImcomieu(tmct0ctd.getTmct0cta().getImcomieu());
    tmct0ctd.setImcomdev(tmct0ctd.getTmct0cta().getImcomdev());
    tmct0ctd.setEucomdev(tmct0ctd.getTmct0cta().getEucomdev());
    tmct0ctd.setImfibrdv(tmct0ctd.getTmct0cta().getImfibrdv());
    tmct0ctd.setImfibreu(tmct0ctd.getTmct0cta().getImfibreu());
    tmct0ctd.setImsvb(tmct0ctd.getTmct0cta().getImsvb());
    tmct0ctd.setImsvbeu(tmct0ctd.getTmct0cta().getImsvbeu());
    tmct0ctd.setImbrmerc(tmct0ctd.getTmct0cta().getImbrmerc());
    tmct0ctd.setImbrmreu(tmct0ctd.getTmct0cta().getImbrmreu());
    tmct0ctd.setImgastos(tmct0ctd.getTmct0cta().getImgastos());
    tmct0ctd.setImgatdvs(tmct0ctd.getTmct0cta().getImgatdvs());
    tmct0ctd.setImimpdvs(tmct0ctd.getTmct0cta().getImimpdvs());
    tmct0ctd.setImimpeur(tmct0ctd.getTmct0cta().getImimpeur());
    tmct0ctd.setImcconeu(tmct0ctd.getTmct0cta().getImcconeu());
    tmct0ctd.setImcliqeu(tmct0ctd.getTmct0cta().getImcliqeu());
    tmct0ctd.setImdereeu(tmct0ctd.getTmct0cta().getImdereeu());
    tmct0ctd.setNutitagr(tmct0ctd.getTmct0cta().getNutitagr());
    tmct0ctd.setImnetobr(tmct0ctd.getTmct0cta().getImnetobr());
    tmct0ctd.setPodevolu(tmct0ctd.getTmct0cta().getPodevolu());
    tmct0ctd.setPotaxbrk(tmct0ctd.getTmct0cta().getPotaxbrk());
    tmct0ctd.setPccombrk(tmct0ctd.getTmct0cta().getPccombrk());
    tmct0ctd.setCdcleare(tmct0ctd.getTmct0cta().getCdcleare());
    tmct0ctd.setAcctatcle(tmct0ctd.getTmct0cta().getAcctatcle());
    tmct0ctd.setTysetcle(tmct0ctd.getTmct0cta().getTysetcle());
    tmct0ctd.setCdbencle(tmct0ctd.getTmct0cta().getCdbencle());
    tmct0ctd.setAcbencle(tmct0ctd.getTmct0cta().getAcbencle());
    tmct0ctd.setIdbencle(tmct0ctd.getTmct0cta().getIdbencle());
    tmct0ctd.setGclecode(tmct0ctd.getTmct0cta().getGclecode());
    tmct0ctd.setGcleacct(tmct0ctd.getTmct0cta().getGcleacct());
    tmct0ctd.setGcleid(tmct0ctd.getTmct0cta().getGcleid());
    tmct0ctd.setLclecode(tmct0ctd.getTmct0cta().getLclecode());
    tmct0ctd.setLcleacct(tmct0ctd.getTmct0cta().getLcleacct());
    tmct0ctd.setLcleid(tmct0ctd.getTmct0cta().getLcleid());
    tmct0ctd.setPccleset(tmct0ctd.getTmct0cta().getPccleset());
    tmct0ctd.setSendbecle(tmct0ctd.getTmct0cta().getSendbecle());
    tmct0ctd.setTpsetcle(tmct0ctd.getTmct0cta().getTpsetcle());
    tmct0ctd.setCdcustod(tmct0ctd.getTmct0cta().getCdcustod());
    tmct0ctd.setAcctatcus(tmct0ctd.getTmct0cta().getAcctatcus());
    tmct0ctd.setTysetcus(tmct0ctd.getTmct0cta().getTysetcus());
    tmct0ctd.setCdbencus(tmct0ctd.getTmct0cta().getCdbencus());
    tmct0ctd.setAcbencus(tmct0ctd.getTmct0cta().getAcbencus());
    tmct0ctd.setIdbencus(tmct0ctd.getTmct0cta().getIdbencus());
    tmct0ctd.setGcuscode(tmct0ctd.getTmct0cta().getGcuscode());
    tmct0ctd.setGcusacct(tmct0ctd.getTmct0cta().getGcusacct());
    tmct0ctd.setGcusid(tmct0ctd.getTmct0cta().getGcusid());
    tmct0ctd.setLcuscode(tmct0ctd.getTmct0cta().getLcuscode());
    tmct0ctd.setLcusacct(tmct0ctd.getTmct0cta().getLcusacct());
    tmct0ctd.setLcusid(tmct0ctd.getTmct0cta().getLcusid());
    tmct0ctd.setPccusset(tmct0ctd.getTmct0cta().getPccusset());
    tmct0ctd.setSendbecus(tmct0ctd.getTmct0cta().getSendbecus());
    tmct0ctd.setTpsetcus(tmct0ctd.getTmct0cta().getTpsetcus());
    tmct0ctd.setCdentliq(tmct0ctd.getTmct0cta().getCdentliq());
    tmct0ctd.setCdrefban(tmct0ctd.getTmct0cta().getCdrefban());
    tmct0ctd.setNbtitula(tmct0ctd.getTmct0cta().getNbtitula());
    tmct0ctd.setFevalor(tmct0ctd.getTmct0cta().getFevalor());
    // FIX:Inicio
    tmct0ctd.setFevalorr(tmct0ctd.getTmct0cta().getFevalorr());
    // FIX:Fin
    tmct0ctd.setFetrade(tmct0ctd.getTmct0cta().getFetrade());
    tmct0ctd.setCdstcexc(tmct0ctd.getTmct0cta().getCdstcexc());
    tmct0ctd.setCdmoniso(tmct0ctd.getTmct0cta().getCdmoniso());
    tmct0ctd.setImcamalo(tmct0ctd.getTmct0cta().getImcamalo());
    tmct0ctd.setImmarpri(tmct0ctd.getTmct0cta().getImmarpri());
    tmct0ctd.setImclecha(tmct0ctd.getTmct0cta().getImclecha());
    tmct0ctd.setPkbroker1(tmct0ctd.getTmct0cta().getPkbroker1());
    tmct0ctd.setPkentity(tmct0ctd.getTmct0cta().getPkentity());
    tmct0ctd.setCdtpoper(tmct0ctd.getTmct0cta().getCdtpoper());
    tmct0ctd.setCdisin(tmct0ctd.getTmct0cta().getCdisin());
    tmct0ctd.setNbvalors(tmct0ctd.getTmct0cta().getNbvalors());
    tmct0ctd.setNuoprout(tmct0ctd.getTmct0cta().getNuoprout());
    tmct0ctd.setNupareje(tmct0ctd.getTmct0cta().getNupareje());
    tmct0ctd.setNuopeaud("");
    tmct0ctd.setPcefecup(new BigDecimal(0));
    tmct0ctd.setCdmodord("N");
    tmct0ctd.setCdordena("");

    tmct0ctd.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0ctd.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);

    // tmct0ctd.setFk_tmct0sta_9(StatusMulticenter.getStateTableJDO(StatusMulticenter.TMCT0CTD,
    // StatusMulticenter.STATE_INPUT_SYSTEM, hpm));
    Tmct0sta tmct0sta = new Tmct0sta(new Tmct0staId(TypeCdestado.NEW_REGISTER.getValue(),
        TypeCdtipest.TMCT0CTD.getValue()));

    tmct0ctd.setTmct0sta(tmct0sta);

    tmct0ctd.setCdindgas(tmct0ctd.getTmct0cta().getCdindgas());

    tmct0ctd.setImgasliqdv(tmct0ctd.getTmct0cta().getImgasliqdv());
    tmct0ctd.setImgasliqeu(tmct0ctd.getTmct0cta().getImgasliqeu());

    tmct0ctd.setImgasgesdv(tmct0ctd.getTmct0cta().getImgasgesdv());
    tmct0ctd.setImgasgeseu(tmct0ctd.getTmct0cta().getImgasgeseu());

    tmct0ctd.setImgastotdv(tmct0ctd.getTmct0cta().getImgastotdv());
    tmct0ctd.setImgastoteu(tmct0ctd.getTmct0cta().getImgastoteu());

    tmct0ctd.setCdtyp("");
  }

  public Tmct0ctd getJDOObject() {
    return tmct0ctd;
  }
}
