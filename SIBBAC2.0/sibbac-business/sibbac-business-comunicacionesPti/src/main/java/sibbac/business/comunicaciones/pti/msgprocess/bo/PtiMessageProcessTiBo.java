package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0DesgloseCamaraEnvioRtBo;
import sibbac.business.wrappers.database.bo.Tmct0enviotitularidadBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.ti.R00;
import sibbac.pti.messages.ti.R02;
import sibbac.pti.messages.ti.TI;
import sibbac.pti.messages.ti.TIControlBlock;

@Service("ptiMessageProcessTiBo")
public class PtiMessageProcessTiBo extends PtiMessageProcess<TI> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessTiBo.class);

  @Autowired
  private Tmct0enviotitularidadBo tiBo;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtBo rtBo;

  @Autowired
  private Tmct0logBo logBo;

  public PtiMessageProcessTiBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, TI ti, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    R00 r00;
    R02 r02;
    String nuorden;
    String nbooking;
    String nucnfclt;
    String msgLog;
    boolean rectificacion;
    boolean error;
    String rTitular;
    String refenciaAdicional;
    Date fecontratacion;
    String miembro;
    List<Tmct0alc> alcs;
    Tmct0enviotitularidad envioTitularidad;
    // Procesamiento del mensaje recibido de tipo TI
    LOG.info(": Inicio recepcion TI");

    List<PTIMessageRecord> r00s = ti.getAll(PTIMessageRecordType.R00);
    List<PTIMessageRecord> r02s = ti.getAll(PTIMessageRecordType.R02);
    TIControlBlock controlBlock = (TIControlBlock) ti.getControlBlock();
    String codigoError = controlBlock.getCodigoDeError();
    String descripcionError = controlBlock.getTextoDeError();
    String codigoErrorR02 = "";

    if (CollectionUtils.isNotEmpty(r00s)) {

      for (PTIMessageRecord record : r00s) {
        r00 = (R00) record;
        r02 = null;
        rTitular = r00.getReferenciaTitular();
        refenciaAdicional = r00.getReferenciaAdicional().trim();
        fecontratacion = new Date(r00.getFechaEjecucion().getTime());
        miembro = r00.getEntidadComunicadoraRefTitular().trim();

        LOG.debug("Procesando TI ## Ref.Titular : {} ## Ref.Adicional : {} ## F.Contratacion : {} ## Mimebro : {}",
            rTitular, refenciaAdicional, fecontratacion, miembro);
        envioTitularidad = tiBo.findByReferenciaTitularAndReferenciaAdicionalAndFechaInicioAndCdmiembromktAndCdcamara(
            rTitular, refenciaAdicional, fecontratacion, miembro, cache.getIdPti());
        if (envioTitularidad == null) {
          LOG.debug(
              "Procesando TI ## Ref.Titular : {} ## Ref.Adicional : {} ## F.Contratacion : {} ## Mimebro : {}, ERROR no encontrado en TMCT0REFERENCIATITULAR",
              rTitular, refenciaAdicional, fecontratacion, miembro);

        }
        else {

          error = !"000".equalsIgnoreCase(codigoError) && !"".equals(codigoError.trim())
              && !descripcionError.trim().isEmpty();

          if (error) {
            if (CollectionUtils.isNotEmpty(r02s)) {
              r02 = (R02) r02s.get(0);
            }
            if (r02 != null) {
              codigoErrorR02 = r02.getCodigoDeError();
            }
            LOG.debug("Error en la reception del TI ## Err1 : {} ## Descrption : {} ## Error Especifico : {}",
                codigoError, descripcionError, codigoErrorR02);
            envioTitularidad.setEstado('R');
          }
          else {
            LOG.debug("Reception TI aceptado.");
            envioTitularidad.setEstado('A');
          }
          envioTitularidad.setFrecepcion(new Date());
          envioTitularidad.setAuditFechaCambio(new Date());
          envioTitularidad.setAuditUser("SIBBAC20");
          envioTitularidad = tiBo.save(envioTitularidad);

          alcs = rtBo.findAllAlcsByIdEnvioTitularidadAndNotCdestadoDeAlta(envioTitularidad.getIdenviotitularidad());
          if (CollectionUtils.isEmpty(alcs)) {
            LOG.warn("NO ENCONTRADOS RT'S ENVIADOS");
          }
          else {

            for (Tmct0alc tmct0alc : alcs) {
              nuorden = tmct0alc.getId().getNuorden().trim();
              nbooking = tmct0alc.getId().getNbooking().trim();
              nucnfclt = tmct0alc.getId().getNucnfclt();
              rectificacion = EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId() == tmct0alc
                  .getEstadotit().getIdestado();
              msgLog = "Recepcion TI";
              if (rectificacion) {
                msgLog = String.format("%s rectificacion", msgLog);
              }

              msgLog = String.format("%s Ref.Tit: '%s' Ref.Adicional: '%s' F.Trade: '%s' Miembro: '%s'", msgLog,
                  rTitular, refenciaAdicional, FormatDataUtils.convertDateToString(fecontratacion), miembro);

              if (error) {
                msgLog = String.format("%s rechazado. Err: '%s'-'%s'-'%s'", msgLog, codigoError, descripcionError,
                    codigoErrorR02);
              }
              else {
                msgLog = String.format("%s aceptado", msgLog);

              }
              LOG.debug("[{}/{}/{}] {}", nuorden, nbooking, nucnfclt, msgLog);
              if (tmct0alc.getTmct0alo().getTmct0bok().getTmct0ord().getIsscrdiv() != 'S') {
                logBo.insertarRegistro(nuorden, nbooking, nucnfclt, msgLog, "", "SIBBAC20");
              }

            }
          }
        }
      }

    }
    else {
      LOG.warn("TI NO TIENE R00'S");
    }

  }
}
