package sibbac.business.comunicaciones.imputacions3.bo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.mo.exceptions.NoPtiMensajeDataException;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.tasks.WrapperUserTaskConcurrencyPrevent;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageVersion;

@Service
public class ManualUserImputacionS3EuroCcp extends WrapperUserTaskConcurrencyPrevent implements
    IManualUserImputacionS3EuroCcp {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(ManualUserImputacionS3EuroCcp.class);

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentasCompensacionBo;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private FestivoBo festivoBo;

  @Value("${sibbac.nacional.defaults.cta.compensacion}")
  private String ctasDefault;

  @Autowired
  private ImputacionS3Multithread imputacionS3Multithread;

  @Autowired
  private ImputacionS3Bo imputacionS3Bo;

  public ManualUserImputacionS3EuroCcp() {
    LOG.info("new ManualUserImputacionS3EuroCcp");
  }

  @Override
  public void executeTask() throws Exception {

    List<Integer> activationDaysCfg = tmct0cfgBo.findDiasActivacionProcesos();

    Calendar now = Calendar.getInstance();

    if (!activationDaysCfg.contains(now.get(Calendar.DAY_OF_WEEK))) {
      LOG.warn("[TaskCreacionMosAsignacion :: executeTask] No es un dia de activacion del proceso.");
      return;
    }

    this.enviarInputacionesS3(false, "SIBBAC20");

  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public CallableResultDTO executeTaskManualUser(String auditUser) throws Exception {
    return this.enviarInputacionesS3(true, auditUser);
  }

  private CallableResultDTO enviarInputacionesS3(boolean isEjecucionManualUsuario, String auditUser)
      throws SIBBACBusinessException, PTIMessageException, JMSException, ParseException {
    BigDecimal nowHora;
    DateFormat df = new SimpleDateFormat("HHmmss");
    BigDecimal horaIniImputaciones;
    BigDecimal horaEndImputaciones;

    Calendar now = Calendar.getInstance();
    CallableResultDTO res = new CallableResultDTO();

    try {
      String camara = tmct0cfgBo.getIdEuroCcp();
      List<Date> holydays = festivoBo.findAllFestivosNacionales();

      List<String> ctasDef = Arrays.asList(ctasDefault.split(","));
      List<Integer> workDaysOfWeek = tmct0cfgBo.getWorkDaysOfWeek();
      Date horaIniImputacionesDate = df.parse(tmct0cfgBo.findHoraInicioImputacionesS3EuroCcp());
      Date horaEndImputacionesDate = df.parse(tmct0cfgBo.findHoraFinalImputacionesS3EuroCcp());

      PTIMessageVersion version = tmct0cfgBo.getPtiMessageVersionFromTmct0cfg(new Date());

      Date sinceDateS3 = DateHelper.getPreviousWorkDate(now.getTime(), 1, workDaysOfWeek, holydays);

      String cuentaCompensacionS3 = cuentasCompensacionBo.findCuentaCompensacionS3();

      LOG.info("[enviarInputacionesS3] config: horaInicio: {} horaFin: {} ctas. Origen: {} fecha: {} cta. Destino: {}",
          horaIniImputacionesDate, horaEndImputacionesDate, ctasDef, sinceDateS3, cuentaCompensacionS3);

      nowHora = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(now.getTime(),
          FormatDataUtils.TIME_FORMAT));

      if (cuentaCompensacionS3 == null || cuentaCompensacionS3.trim().isEmpty()) {
        throw new SIBBACBusinessException("No hay cuenta de compensacion configurada para la imputacion S3.");
      }

      if (DateHelper.isHoliday(now, holydays)) {
        throw new SIBBACBusinessException("No de puede ejecutar la imputacion S3 un dia festivo.");
      }
      else if (DateHelper.isWeekend(now.getTime(), workDaysOfWeek)) {
        throw new SIBBACBusinessException("No de puede ejecutar la imputacion S3 en fin de semana.");
      }
      else if (isEjecucionManualUsuario) {

        this.crearImputacionesS3(sinceDateS3, ctasDef, camara, cuentaCompensacionS3, version, auditUser, res);

      }
      else {

        imputacionS3Bo.validarHoraInicioFinBotonazo(horaIniImputacionesDate, horaEndImputacionesDate);

        horaIniImputaciones = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(
            horaIniImputacionesDate, FormatDataUtils.TIME_FORMAT));

        horaEndImputaciones = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(
            horaEndImputacionesDate, FormatDataUtils.TIME_FORMAT));

        if (nowHora.compareTo(horaIniImputaciones) >= 0 && nowHora.compareTo(horaEndImputaciones) <= 0) {
          this.crearImputacionesS3(sinceDateS3, ctasDef, camara, cuentaCompensacionS3, version, auditUser, res);
        }
        else {
          LOG.info("[enviarImputacionS3] No es la hora de ejecucion del botonazo EUROCCP.");
        }
      }

    }
    catch (Exception e) {
      if (isEjecucionManualUsuario) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
        res.addErrorMessage(e.getMessage());
      }
      else {
        throw e;
      }
    }
    return res;
  }

  private void crearImputacionesS3(Date sinceDateS3, List<String> ctasDef, String camara, String cuentaCompensacionS3,
      PTIMessageVersion version, String auditUser, CallableResultDTO res) throws NoPtiMensajeDataException,
      PTIMessageException, JMSException, SIBBACBusinessException {
    LOG.info("[crearImputacionesS3] Buscando operaciones ecc con titulos disponibles para enviar a S3...");

    List<String> cdoperacioneccs = anBo.findAllCdoperacioneccTitulosDisponiblesForEnvioS3AndCamara(sinceDateS3,
        ctasDef, camara);

    LOG.info("[crearImputacionesS3] Encontradas {} operaciones con titulos disponibles de {}", cdoperacioneccs.size(),
        sinceDateS3);
    if (CollectionUtils.isNotEmpty(cdoperacioneccs)) {
      String msg = imputacionS3Multithread.crearImputacionesS3(cdoperacioneccs, cuentaCompensacionS3, version,
          auditUser);
      res.addMessage(msg);
    }
    else {
      res.addMessage("No hay operaciones euroccp que imputar.");
    }
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_IMPUTACION_S3_EUROCCP;
  }

}
