package sibbac.business.comunicaciones.pti.commons.enums;

public enum EstadosEccMovimiento {
  RECHAZADO_ECC("5"),
  PDTE_ACEPTAR_COMPENSADOR("6"),
  FINALIZADO("9"),
  CANCELADO("12"),
  PDTE_ACEPTAR_COMPENSADOR_ORIGEN("13"),
  ENVIANDOSE_INT("20"),
  CANCELADO_INT("21");

  public String text;

  private EstadosEccMovimiento(String text) {
    this.text = text;
  }
}
