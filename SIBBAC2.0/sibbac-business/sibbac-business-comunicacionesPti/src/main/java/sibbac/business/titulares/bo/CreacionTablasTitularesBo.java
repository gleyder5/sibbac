package sibbac.business.titulares.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0DesgloseCamaraEnvioRtBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0enviotitularidadBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0referenciaTitularBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;

@Service
public class CreacionTablasTitularesBo {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CreacionTablasTitularesBo.class);

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0referenciaTitularBo refTitBo;

  @Autowired
  private Tmct0enviotitularidadBo tiBo;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtBo rtBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void crearTablasForProcessNewTransaction(Tmct0alcId alcId, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    LOG.trace("[crearTablasForProcessNewTransaction] inicio {}", alcId);
    Tmct0alc alc = alcBo.findById(alcId);
    if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
        && alc.getEstadotit().getIdestado() < EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId()) {

      if (alc.getCdrefban().trim().length() > 20) {
        throw new SIBBACBusinessException(String.format("Referencia adicional tiene tamaño incorrecto: '%s'", alc
            .getCdrefban().trim()));
      }

      boolean isRouting = ordBo.isRouting(alc.getTmct0alo().getTmct0bok().getTmct0ord());
      boolean isScriptDividend = ordBo.isScriptDividend(alc.getTmct0alo().getTmct0bok().getTmct0ord());
      List<String> listIdentificaciones = afiBo.findAllCdholderByAlcAndActivos(alcId.getNuorden(), alcId.getNbooking(),
          alcId.getNucnfclt(), alcId.getNucnfliq());

      if (isRouting || isScriptDividend) {
        this.crearTablasTitularesRouting(alc, listIdentificaciones, config);
      }
      else {

        if (listIdentificaciones != null && !listIdentificaciones.isEmpty()) {

          this.crearTablasTitularesNoRouting(alc, listIdentificaciones, config);
        }
        else {
          throw new SIBBACBusinessException("Una orden no ROUTING debe tener titulares");
        }
      }

      listIdentificaciones.clear();

      LOG.trace("[crearTablasForProcessNewTransaction] fin {}", alcId);
    }
    else {
      LOG.warn("[crearTablasForProcessNewTransaction] alc : {} en estado {} no permite creacion tablas titulares.",
          alcId, alc.getEstadotit().getNombre());
    }
  }

  @Transactional
  public void crearTablasTitularesDesgloseByHolders(Tmct0alc alc, List<Holder> listHolders, TitularesConfigDTO config)
      throws SIBBACBusinessException {

    if (alc.getCdrefban().trim().length() > 20) {
      throw new SIBBACBusinessException(String.format("Referencia adicional tiene tamaño incorrecto: '%s'", alc
          .getCdrefban().trim()));
    }

    boolean isRouting = ordBo.isRouting(alc.getTmct0alo().getTmct0bok().getTmct0ord());
    boolean isScriptDividend = ordBo.isScriptDividend(alc.getTmct0alo().getTmct0bok().getTmct0ord());
    List<String> listIdentificaciones = this.getListaIdentificacionesByHolder(listHolders);
    if (isRouting || isScriptDividend) {
      this.crearTablasTitularesRouting(alc, listIdentificaciones, config);
    }
    else if (listHolders != null && !listHolders.isEmpty()) {
      this.crearTablasTitularesNoRouting(alc, listIdentificaciones, config);
    }
  }

  private List<String> getListaIdentificacionesByHolder(List<Holder> listHolders) {
    List<String> listIdentificaciones = new ArrayList<String>();
    if (listHolders != null)
      for (Holder holder : listHolders) {
        listIdentificaciones.add(holder.getCdholder().trim());
      }
    return listIdentificaciones;
  }

  private List<String> getListaIdentificacionesByTmct0afi(List<Tmct0afi> listHolders) {
    List<String> listIdentificaciones = new ArrayList<String>();
    if (listHolders != null)
      for (Tmct0afi holder : listHolders) {
        listIdentificaciones.add(holder.getCdholder().trim());
      }
    return listIdentificaciones;
  }

  @Transactional
  public void crearTablasTitularesByTmct0afi(Tmct0alc alc, List<Tmct0afi> listHolders, TitularesConfigDTO config)
      throws SIBBACBusinessException {

    if (alc.getCdrefban().trim().length() > 20) {
      throw new SIBBACBusinessException(String.format("Referencia adicional tiene tamaño incorrecto: '%s'", alc
          .getCdrefban().trim()));
    }

    boolean isRouting = ordBo.isRouting(alc.getTmct0alo().getTmct0bok().getTmct0ord());
    boolean isScriptDividend = ordBo.isScriptDividend(alc.getTmct0alo().getTmct0bok().getTmct0ord());

    List<String> listIdentificaciones = this.getListaIdentificacionesByTmct0afi(listHolders);
    if (isRouting || isScriptDividend) {
      this.crearTablasTitularesRouting(alc, listIdentificaciones, config);
    }
    else if (listHolders != null && !listHolders.isEmpty()) {
      this.crearTablasTitularesNoRouting(alc, listIdentificaciones, config);
    }
    listIdentificaciones.clear();
  }

  private void crearTablasTitularesNoRouting(Tmct0alc alc, List<String> listIdentificaciones, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    Tmct0referenciatitular ref = alc.getReferenciaTitular();

    String referenciaTitularPti = null;

    if (ref == null) {
      BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
      BigDecimal fliquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alc
          .getFeopeliq()));
      if (now.compareTo(fliquidacion) >= 0) {
        // rectificacion
        ref = refTitBo.crearNuevaRefernciaTitularNewTransaction(alc.getCdrefban(), config.getAuditUser());
      }
      else {

        try {
          referenciaTitularPti = refTitBo.findReferenciaTitularFromReferenciasTitularFis(alc.getCdrefban(),
              listIdentificaciones);
        }
        catch (Exception e1) {
          throw new SIBBACBusinessException(String.format(
              "No se ha podido recuperar la Referencia para ref.adicional: '%s' holders: '%s'. err: %s", alc
                  .getCdrefban().trim(), listIdentificaciones, e1.getMessage()), e1);
        }

        if (referenciaTitularPti == null) {
          synchronized (this) {
            try {
              referenciaTitularPti = refTitBo.findReferenciaTitularFromReferenciasTitularFis(alc.getCdrefban(),
                  listIdentificaciones);
            }
            catch (Exception e1) {
              throw new SIBBACBusinessException(String.format(
                  "No se ha podido recuperar la Referencia para ref.adicional: '%s' holders: '%s'. err: %s", alc
                      .getCdrefban().trim(), listIdentificaciones, e1.getMessage()), e1);
            }
            if (referenciaTitularPti == null) {
              ref = refTitBo.crearNuevaReferenciaTitularNewTransaction(alc.getCdrefban(), listIdentificaciones,
                  config.getAuditUser());
              if (ref != null) {
                referenciaTitularPti = ref.getCdreftitularpti();
              }
            }
            else {
              ref = refTitBo.findByCdreftitularptiAndReferenciaAdicional(referenciaTitularPti, alc.getCdrefban());
            }
          }
        }
        else {
          ref = refTitBo.findByCdreftitularptiAndReferenciaAdicional(referenciaTitularPti, alc.getCdrefban());
        }
      }

      alc.setReferenciaTitular(ref);
      alc.setRftitaud(ref.getCdreftitularpti().trim());
      alc.setCdreftit(ref.getCdreftitularpti().trim());
      alc.setFhaudit(new Date());
      alc.setCdusuaud(config.getAuditUser());

      String msg = String.format(
          "Alc: '%s' Calculada referencia titular NO ROUTING '%s' ref.adicional: '%s' titulares: (", alc.getId()
              .getNucnfliq(), ref.getCdreftitularpti().trim(), ref.getReferenciaAdicional().trim(),
          listIdentificaciones);

      for (int i = 0; i < listIdentificaciones.size(); i++) {
        if (i == 0) {
          msg = String.format("%s'%s'", msg, listIdentificaciones.get(i));
        }
        else {
          msg = String.format("%s,'%s'", msg, listIdentificaciones.get(i));
        }
      }
      msg = String.format("%s).", msg);

      LOG.debug("[crearTablasTitularesNoRouting] alc: {} - {}", alc.getId(), msg);
      logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
          config.getAuditUser());
    }
    if (alc.getTmct0alo().getTmct0bok().getCasepti() == 'S' && !listIdentificaciones.isEmpty()) {
      this.crearTiRts(alc, ref, config);
    }

  }

  private boolean crearTiRts(Tmct0alc alc, Tmct0referenciatitular ref, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    String msg;
    boolean creados = false;
    boolean ponerEnCurso = false;
    List<Tmct0afi> titulares;
    List<Tmct0desgloseclitit> listDct;

    Map<String, Tmct0enviotitularidad> mapEnvioTitularidad = new HashMap<String, Tmct0enviotitularidad>();

    List<Tmct0desgloseclitit> listDesglosesAlc = new ArrayList<Tmct0desgloseclitit>();
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    Collection<String> listKeysBuscadas = new HashSet<String>();
    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    BigDecimal limitFliquidacion = this.getLimitFliquidacion(alc.getFeopeliq(),
        this.getMaxDaysEnvioTitulares(alc, config), config);
    if (now.compareTo(limitFliquidacion) < 0) {
      titulares = afiBo.findActivosByAlcData(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId()
          .getNucnfclt(), alc.getId().getNucnfliq());
      for (Tmct0desglosecamara dc : listDcs) {
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
          if (dc.getTmct0estadoByCdestadotit().getIdestado() == EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId()
              || dc.getTmct0estadoByCdestadotit().getIdestado() == EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR_MODIFICACION
                  .getId()
              || dc.getTmct0estadoByCdestadotit().getIdestado() == EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR_RECTIFICACION
                  .getId()) {

            listDct = dc.getTmct0desgloseclitits();

            listDct = this.filtrarDesglosesCtaTercerosOrGiveUp(listDct, config.getCamaraEuroccp());

            if (listDct.isEmpty()) {
              ponerEnCurso = true;
              msg = String
                  .format(
                      "Alc: '%s' camara: '%s' no tiene desgloses en give-up o cta de terceros, no se necesita enviar titulares.",
                      alc.getId().getNucnfliq(), dc.getTmct0CamaraCompensacion().getCdCodigo().trim());
              LOG.warn("[crearTiRts] {} - {}", alc.getId(), msg);
              logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(),
                  msg, "", config.getAuditUser());
              dc.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.TITULARIDAD_ACEPTADA
                  .getId()));
              continue;
            }

            listDesglosesAlc.addAll(listDct);
            // BUSCAMOS TI'S QUE EXISTAN
            this.crearRegistroEnvioTitularidadIfNotExists(alc, ref, dc, listDct, listKeysBuscadas, mapEnvioTitularidad,
                config.getCamaraNacional(), false, config.getAuditUser());
            this.crearRegistroEnvioTitularidadIfNotExists(alc, ref, dc, listDct, listKeysBuscadas, mapEnvioTitularidad,
                config.getCamaraEuroccp(), true, config.getAuditUser());

            // this.crearRegistroEnvioTitularidadIfNotExistsTitulares(alc,
            // titulares, ref, dc, listDct, listKeysBuscadas,
            // mapEnvioTitularidad, config.getCamaraEuroccp(),
            // config.getAuditUser());

            // POR ULTIMO CREAMOS LOS RT'S
            this.crearNuevosRegistrosComunicacionOperaciones(alc, titulares, ref, dc, listDct, mapEnvioTitularidad,
                config);
            creados = true;
          }
        }
      }// fin for
      mapEnvioTitularidad.clear();
      listKeysBuscadas.clear();
      if (creados) {
        this.cambiarEstadosTitularidad(alc, listDesglosesAlc, config);
      }
      else if (ponerEnCurso) {
        msg = String.format("Alc: '%s' No contiene ningun titular que enviar.", alc.getId().getNucnfliq());
        LOG.warn("[crearTiRts] {} - {}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            config.getAuditUser());
        alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId()));
        alc.setFhaudit(new Date());
        alc.setCdusuaud(config.getAuditUser());
      }
    }
    else {
      msg = String.format("Alc: '%s' Ha pasado el tiempo de comunicacion de titulares, no se enviara nada.", alc
          .getId().getNucnfliq());
      LOG.warn("[crearTiRts] {} - {}", alc.getId(), msg);
      logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
          config.getAuditUser());
      this.cambiarEstadosTitularidad(alc, config);
    }
    return creados;
  }

  private List<Tmct0desgloseclitit> filtrarDesglosesCtaTercerosOrGiveUp(
      List<Tmct0desgloseclitit> listDesglosesOriginales, String camaraEuroCcp) {
    String cta;
    Tmct0CuentasDeCompensacion cc;
    Map<String, Tmct0CuentasDeCompensacion> cached = new HashMap<String, Tmct0CuentasDeCompensacion>();
    List<Tmct0desgloseclitit> res = new ArrayList<Tmct0desgloseclitit>();

    for (Tmct0desgloseclitit dct : listDesglosesOriginales) {
      if (dct.getMiembroCompensadorDestino() != null && !dct.getMiembroCompensadorDestino().trim().isEmpty()
          && dct.getCodigoReferenciaAsignacion() != null && !dct.getCodigoReferenciaAsignacion().trim().isEmpty()) {
        res.add(dct);
      }
      else if (dct.getCuentaCompensacionDestino() != null && !dct.getCuentaCompensacionDestino().trim().isEmpty()) {
        cta = dct.getCuentaCompensacionDestino().trim();

        cc = cuentaCompensacionBo.findByCdodigoCached(cached, cta);
        if (cc != null && cc.getTmct0TipoCuentaDeCompensacion().getCdCodigo().trim().startsWith("CT")) {
          if (dct.getTmct0desglosecamara().getTmct0CamaraCompensacion().getCdCodigo().trim()
              .equals(camaraEuroCcp.trim())) {
            dct.setEuroccpClientNumber(cc.getClientNumber());
          }
          res.add(dct);

        }

      }
    }
    cached.clear();

    return res;
  }

  private boolean crearRegistroEnvioTitularidadIfNotExists(Tmct0alc alc, Tmct0referenciatitular ref,
      Tmct0desglosecamara dc, List<Tmct0desgloseclitit> listDct, Collection<String> listKeysBuscadas,
      Map<String, Tmct0enviotitularidad> mapEnvioTitularidad, String idCamara, boolean isEuroccp, String auditUser)
      throws SIBBACBusinessException {
    String key;
    boolean faltaTi = false;
    Tmct0enviotitularidad ti;
    String miembroMercado;
    boolean isFromCamara;
    for (Tmct0desgloseclitit dct : listDct) {
      ti = null;
      if (isEuroccp && dct.getEuroccpClientNumber() != null) {
        miembroMercado = dct.getEuroccpClientNumber();
      }
      else {
        miembroMercado = dct.getCdmiembromkt();
      }

      isFromCamara = dct.getTmct0desglosecamara().getTmct0CamaraCompensacion().getCdCodigo().trim()
          .equals(idCamara.trim());

      if (isFromCamara) {

        key = this.getKeyMapEnvioTitularidad(ref.getCdreftitularpti(), dc.getFecontratacion(), miembroMercado, dc
            .getTmct0CamaraCompensacion().getCdCodigo(), null);

        ti = mapEnvioTitularidad.get(key);
        if (ti == null) {
          if (!listKeysBuscadas.contains(key)) {
            ti = this.findEnvioTitularidad(ref.getCdreftitularpti(), alc.getCdrefban(), dc.getFecontratacion(),
                miembroMercado, dc.getTmct0CamaraCompensacion().getCdCodigo());

            listKeysBuscadas.add(key);
            if (ti != null) {
              mapEnvioTitularidad.put(key, ti);
            }
            else {
              ti = this.crearRegistroEnvioTitularidadIfNotExists(alc, ref, dc, miembroMercado, auditUser);
              mapEnvioTitularidad.put(key, ti);
              faltaTi = true;
            }
          }
        }
      }
    }
    return faltaTi;
  }

  private String getKeyMapEnvioTitularidad(String referencia, Date fecha, String miembroMercado, String camara,
      String identificacionTitular) {
    String key = String.format("%s-%s-%s-%s-%s", FormatDataUtils.convertDateToString(fecha), referencia.trim(),
        camara.trim(), miembroMercado.trim(), identificacionTitular != null ? identificacionTitular.trim() : "");
    return key;
  }

  private Tmct0enviotitularidad findEnvioTitularidad(String referencia, String referenciaAdicional, Date fecha,
      String miembroMercado, String camara) throws SIBBACBusinessException {

    try {
      return tiBo.findByReferenciaTitularAndReferenciaAdicionalAndFechaInicioAndCdmiembromktAndCdcamara(referencia,
          referenciaAdicional, fecha, miembroMercado, camara);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(
          String.format(
              "No se ha podido recuperar los TI's enviados para ref.tit: '%s' ref.adicional: '%s' fecha: '%s' miembro: '%s' camara: '%s' err: %s",
              referencia, referenciaAdicional, fecha, miembroMercado, camara, e.getMessage()), e);
    }

  }

  private synchronized Tmct0enviotitularidad crearRegistroEnvioTitularidadIfNotExists(Tmct0alc alc,
      Tmct0referenciatitular ref, Tmct0desglosecamara dc, String miembroMercado, String auditUser)
      throws SIBBACBusinessException {
    Tmct0enviotitularidad ti = this.findEnvioTitularidad(ref.getCdreftitularpti(), alc.getCdrefban(),
        alc.getFeejeliq(), miembroMercado, dc.getTmct0CamaraCompensacion().getCdCodigo());
    if (ti == null) {
      ti = tiBo.crearEnvioTitularidadNewTransaction(dc, miembroMercado, ref, auditUser);
    }
    return ti;
  }

  private void crearNuevosRegistrosComunicacionOperaciones(Tmct0alc alc, List<Tmct0afi> titulares,
      Tmct0referenciatitular ref, Tmct0desglosecamara dc, List<Tmct0desgloseclitit> listDct,
      Map<String, Tmct0enviotitularidad> mapEnvioTitularidad, TitularesConfigDTO config) {

    Tmct0DesgloseCamaraEnvioRt oldRt;
    Tmct0DesgloseCamaraEnvioRt newRt;
    List<Tmct0DesgloseCamaraEnvioRt> listOldRts = new ArrayList<Tmct0DesgloseCamaraEnvioRt>(
        dc.getTmct0DesgloseCamaraEnvioRt());
    List<Tmct0DesgloseCamaraEnvioRt> listNewRts = this.getNewListRegistroComunicacionOperaciones(dc, titulares, ref,
        listDct, mapEnvioTitularidad, config.getCamaraNacional(), config.getCamaraEuroccp(), config.getAuditUser());

    Map<String, Tmct0DesgloseCamaraEnvioRt> mapNewRts = this.getMapByAgr(listNewRts);
    Map<String, Tmct0DesgloseCamaraEnvioRt> mapOldRts = this.getMapByAgr(listOldRts);

    // dar bajas
    for (String key : mapOldRts.keySet()) {
      oldRt = mapOldRts.get(key);
      newRt = mapNewRts.get(key);
      if (newRt == null) {
        if (oldRt.getEstado() == null || oldRt.getEstado() != 'C') {
          oldRt.setEstado('C');
          oldRt.setAudit_fecha_cambio(new Date());
          oldRt.setAudit_user(config.getAuditUser());
          rtBo.save(oldRt);
        }
      }

    }

    for (String key : mapNewRts.keySet()) {
      oldRt = mapOldRts.get(key);
      newRt = mapNewRts.get(key);
      if (oldRt == null) {
        newRt.setAudit_fecha_cambio(new Date());
        newRt.setAudit_user(config.getAuditUser());
        dc.getTmct0DesgloseCamaraEnvioRt().add(rtBo.save(newRt));
      }
      else {
        if (oldRt.getEstado() == null || oldRt.getEstado() != 'A' && oldRt.getEstado() != 'P') {
          oldRt.setEstado('P');
          oldRt.setAudit_fecha_cambio(new Date());
          oldRt.setAudit_user(config.getAuditUser());
          rtBo.save(oldRt);
        }
      }
    }

    mapNewRts.clear();
    mapNewRts.clear();
    listNewRts.clear();
    listOldRts.clear();
  }

  private String getkeyRt(Tmct0DesgloseCamaraEnvioRt rt) {
    return String.format("%s##%s##%s##%s##%s##%s##%s##%s", rt.getNudesglose(),
        rt.getImtitulos().setScale(2, RoundingMode.HALF_UP), StringUtils.trim(rt.getCdmiembromkt()),
        rt.getFecontratacion(), StringUtils.trim(rt.getCdPlataformaNegociacion()),
        StringUtils.trim(rt.getCriterioComunicacionTitular()), StringUtils.trim(rt.getReferenciaTitular()),
        StringUtils.trim(""));
  }

  private Map<String, Tmct0DesgloseCamaraEnvioRt> getMapByAgr(List<Tmct0DesgloseCamaraEnvioRt> listRts) {
    Map<String, Tmct0DesgloseCamaraEnvioRt> res = new HashMap<String, Tmct0DesgloseCamaraEnvioRt>();
    for (Tmct0DesgloseCamaraEnvioRt rt : listRts) {
      res.put(this.getkeyRt(rt), rt);
    }
    return res;
  }

  private List<Tmct0DesgloseCamaraEnvioRt> getNewListRegistroComunicacionOperaciones(Tmct0desglosecamara dc,
      List<Tmct0afi> titulares, Tmct0referenciatitular ref, List<Tmct0desgloseclitit> listDct,
      Map<String, Tmct0enviotitularidad> mapEnvioTitularidad, String camaranaNacional, String camaraEuroccp,
      String auditUser) {
    String key;
    String miembroMercado;
    Tmct0enviotitularidad ti;
    boolean isPti;
    boolean isEuroccp;
    List<Tmct0DesgloseCamaraEnvioRt> res = new ArrayList<Tmct0DesgloseCamaraEnvioRt>();

    for (Tmct0desgloseclitit dct : listDct) {

      isEuroccp = dct.getTmct0desglosecamara().getTmct0CamaraCompensacion().getCdCodigo().trim()
          .equals(camaraEuroccp.trim());
      isPti = dct.getTmct0desglosecamara().getTmct0CamaraCompensacion().getCdCodigo().trim()
          .equals(camaranaNacional.trim());
      if (isEuroccp && dct.getEuroccpClientNumber() != null) {
        miembroMercado = dct.getEuroccpClientNumber();
      }
      else {
        miembroMercado = dct.getCdmiembromkt();
      }
      if (isPti || isEuroccp) {
        key = this.getKeyMapEnvioTitularidad(ref.getCdreftitularpti(), dc.getFecontratacion(), miembroMercado, dc
            .getTmct0CamaraCompensacion().getCdCodigo(), null);
        ti = mapEnvioTitularidad.get(key);
        res.add(rtBo.crearDesgloseCamaraEnvioRt(dc, dct, ref, ti, camaranaNacional, camaraEuroccp, auditUser));
      }
    }
    return res;
  }

  private void crearTablasTitularesRouting(Tmct0alc alc, List<String> listIdentificaciones, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    Tmct0referenciatitular ref;
    if (alc.getCdrefban().trim().length() != 20) {
      throw new SIBBACBusinessException(String.format("Referencia adicional ROUTING tiene tamaño incorrecto: '%s'", alc
          .getCdrefban().trim()));
    }
    ref = alc.getReferenciaTitular();
    if (ref == null) {
      BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
      BigDecimal fliquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alc
          .getFeopeliq()));
      if (now.compareTo(fliquidacion) >= 0) {
        // rectificacion
        ref = refTitBo.crearNuevaRefernciaTitularNewTransaction(alc.getCdrefban(), config.getAuditUser());
      }
      else {
        ref = refTitBo.findByCdreftitularptiAndReferenciaAdicional(alc.getCdrefban(), alc.getCdrefban());
        if (ref == null) {
          ref = refTitBo.saveSyncronizedNewTransaction(alc.getCdrefban(), alc.getCdrefban(), config.getAuditUser());
        }
      }
      alc.setReferenciaTitular(ref);
      alc.setRftitaud(ref.getCdreftitularpti().trim());
      alc.setCdreftit(ref.getCdreftitularpti().trim());
      alc.setFhaudit(new Date());
      alc.setCdusuaud(config.getAuditUser());

      String msg = String.format("Alc: '%s' Calculada referencia titular ROUTING '%s' ref.adicional: '%s'.", alc
          .getId().getNucnfliq(), ref.getCdreftitularpti().trim(), ref.getReferenciaAdicional().trim());

      LOG.debug("[crearTablasTitularesRouting] alc: {} - {}", alc.getId(), msg);

      logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
          config.getAuditUser());
    }
    if (alc.getTmct0alo().getTmct0bok().getCasepti() == 'S' && !listIdentificaciones.isEmpty()) {
      this.crearTiRts(alc, ref, config);
    }

  }

  private void cambiarEstadosTitularidad(Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses,
      TitularesConfigDTO config) throws SIBBACBusinessException {
    int estado;
    BigDecimal limitFliquidacion;
    int daysTitularDefinitivo;
    int maxDaysTitularDefinitivo = 0;
    boolean cambiarEstadoAlc = false;
    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    BigDecimal fliquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alc
        .getFeopeliq()));
    if (alc.getReferenciaTitular() != null) {

      for (Tmct0desgloseclitit dct : listDesgloses) {
        if (dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
            .getId()) {
          daysTitularDefinitivo = this.getDaysEnvioTitulares(dct, config);
          maxDaysTitularDefinitivo = Math.max(daysTitularDefinitivo, maxDaysTitularDefinitivo);

          limitFliquidacion = this.getLimitFliquidacion(alc.getFeopeliq(), daysTitularDefinitivo, config);
          if (now.compareTo(limitFliquidacion) > 0) {
            estado = EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId();
          }
          else if (now.compareTo(fliquidacion) >= 0) {
            estado = EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId();

          }
          else {
            estado = EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId();
          }
          if (dct.getTmct0desglosecamara().getTmct0estadoByCdestadotit().getIdestado() != estado) {
            dct.getTmct0desglosecamara().setTmct0estadoByCdestadotit(new Tmct0estado(estado));
            dct.getTmct0desglosecamara().setAuditFechaCambio(new Date());
            dct.getTmct0desglosecamara().setAuditUser(config.getAuditUser());
            cambiarEstadoAlc = true;
          }
        }
      }
    }

    limitFliquidacion = this.getLimitFliquidacion(alc.getFeopeliq(), maxDaysTitularDefinitivo, config);
    if (now.compareTo(limitFliquidacion) > 0) {
      alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId()));
      alc.setFhaudit(new Date());
      alc.setCdusuaud(config.getAuditUser());
    }
    else if (cambiarEstadoAlc) {
      alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId()));
      alc.setFhaudit(new Date());
      alc.setCdusuaud(config.getAuditUser());
    }
  }

  private void cambiarEstadosTitularidad(Tmct0alc alc, TitularesConfigDTO config) throws SIBBACBusinessException {
    boolean cambiarEstadoAlc = false;
    int daysTitularDefinitivo;
    BigDecimal limitFliquidacion;
    int maxDaysTitularDefinitivo = 0;
    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    BigDecimal fliquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alc
        .getFeopeliq()));
    if (alc.getReferenciaTitular() != null) {

      List<Tmct0desglosecamara> listDesgloses = alc.getTmct0desglosecamaras();
      for (Tmct0desglosecamara dc : listDesgloses) {
        daysTitularDefinitivo = this.getDaysEnvioTitulares(dc, config);
        maxDaysTitularDefinitivo = Math.max(daysTitularDefinitivo, maxDaysTitularDefinitivo);
        limitFliquidacion = this.getLimitFliquidacion(alc.getFeopeliq(), daysTitularDefinitivo, config);
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
            && now.compareTo(limitFliquidacion) > 0) {
          if (dc.getTmct0estadoByCdestadotit().getIdestado() != EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA
              .getId()) {
            dc.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA
                .getId()));
            dc.setAuditFechaCambio(new Date());
            dc.setAuditUser(config.getAuditUser());
            cambiarEstadoAlc = true;
          }
          else {
            LOG.debug("[cambiarEstadosTitularidad] {} ya esta en titular definitiva...", alc.getId());
          }
        }
        else if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
            .getId()) {
          if (now.compareTo(fliquidacion) >= 0) {
            dc.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION
                .getId()));
          }
          else {
            dc.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId()));
          }
          dc.setAuditFechaCambio(new Date());
          dc.setAuditUser(config.getAuditUser());
          cambiarEstadoAlc = true;
        }
      }

    }
    limitFliquidacion = this.getLimitFliquidacion(alc.getFeopeliq(), maxDaysTitularDefinitivo, config);
    if (now.compareTo(limitFliquidacion) > 0) {
      alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId()));
      alc.setFhaudit(new Date());
      alc.setCdusuaud(config.getAuditUser());
    }
    else if (cambiarEstadoAlc) {
      alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId()));
      alc.setFhaudit(new Date());
      alc.setCdusuaud(config.getAuditUser());
    }

  }

  private int getDaysEnvioTitulares(Tmct0desgloseclitit dct, TitularesConfigDTO config) {
    return this.getDaysEnvioTitulares(dct.getTmct0desglosecamara(), config);
  }

  private int getDaysEnvioTitulares(Tmct0desglosecamara dc, TitularesConfigDTO config) {
    int daysTitularDefinitivo = 15;
    if (dc.getTmct0CamaraCompensacion() != null && dc.getTmct0CamaraCompensacion().getCdCodigo() != null) {
      if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(config.getCamaraNacional())) {
        daysTitularDefinitivo = config.getLimiteDiasEnvioTitularesNacional();
      }
      else if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(config.getCamaraEuroccp())) {
        daysTitularDefinitivo = config.getLimiteDiasEnvioTitularesEuroccp();
      }
    }
    return daysTitularDefinitivo;
  }

  private int getMaxDaysEnvioTitulares(Tmct0alc alc, TitularesConfigDTO config) {
    int daysTitularDefinitivo = 0;
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listDcs) {
      if (dc.getTmct0estadoByCdestadoasig() != null
          && dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        daysTitularDefinitivo = Math.max(daysTitularDefinitivo, this.getDaysEnvioTitulares(dc, config));
      }
    }
    return daysTitularDefinitivo;
  }

  private BigDecimal getLimitFliquidacion(Date fliquidacion, int days, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    Date limitFliquidacion = DateHelper.getNextWorkDate(fliquidacion, days, config.getWorkDaysOfWeek(),
        config.getHolidays());
    try {
      return FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(limitFliquidacion));
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e);
    }
  }

}
