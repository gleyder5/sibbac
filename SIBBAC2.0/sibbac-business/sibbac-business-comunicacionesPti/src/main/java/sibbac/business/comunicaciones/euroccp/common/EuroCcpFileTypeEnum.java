package sibbac.business.comunicaciones.euroccp.common;

public enum EuroCcpFileTypeEnum {

  ORS,
  ORG,
  CRG,
  ERS,
  ERG,
  ORS_TMP,
  ERS_TMP,
  ORG_TMP,
  CRG_TMP,
  ERG_TMP;

}
