package sibbac.business.comunicaciones.ordenes.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaFromInputsBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_LOAD_XML_FIDESSA_MQ, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-22 ? * MON-FRI")
public class TaskLoadXmlFidessaFromMQ extends WrapperTaskConcurrencyPrevent {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskLoadXmlFidessaFromMQ.class);

  @Autowired
  private LoadXmlOrdenFidessaFromInputsBo loaXmlOrdenFirdessaFromMqBo;

  public TaskLoadXmlFidessaFromMQ() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void executeTask() throws Exception {
    LOG.debug("[TaskLoadXmlFidessaFromMQ :: execute] inicio.");
    loaXmlOrdenFirdessaFromMqBo.executeTask();
    LOG.debug("[TaskLoadXmlFidessaFromMQ :: execute] fin.");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_LOAD_XML_FIDESSA;
  }

}
