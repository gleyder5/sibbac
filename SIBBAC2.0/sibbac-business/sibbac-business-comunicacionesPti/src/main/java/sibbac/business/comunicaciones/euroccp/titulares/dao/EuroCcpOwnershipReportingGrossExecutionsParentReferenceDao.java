package sibbac.business.comunicaciones.euroccp.titulares.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReportingGrossExecutionsParentReference;

@Component
public interface EuroCcpOwnershipReportingGrossExecutionsParentReferenceDao extends JpaRepository<EuroCcpOwnershipReportingGrossExecutionsParentReference, Long> {

}
