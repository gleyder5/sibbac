package sibbac.business.comunicaciones.euroccp.titulares.dto;

import java.text.SimpleDateFormat;

import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReferenceStaticData;
import sibbac.business.wrappers.database.model.RecordBean;

public class EuroCcpOwnershipReferenceStaticDataDTO extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -5897975641237870508L;

  private String ownerReference = "";
  private String ownerName = "";
  private String firstSurname = "";
  private String secondName = "";
  private Character typeIdentificacion;
  private String identificationCode = "";
  private Character naturalLegalInd;
  private String nationality = "";
  private Character nationalForeignInd;
  private Character ownerType;
  private String address = "";
  private String city = "";
  private String postalCode = "";
  private String countryResidence = "";
  private String birthDate;

  public EuroCcpOwnershipReferenceStaticDataDTO() {
  }

  public EuroCcpOwnershipReferenceStaticDataDTO(EuroCcpOwnershipReferenceStaticData hb, String birthDateFormat) {
    SimpleDateFormat sdf = new SimpleDateFormat(birthDateFormat);
    setOwnerReference(hb.getOwnerReference());
    setOwnerName(hb.getOwnerName());
    setFirstSurname(hb.getFirstSurname());
    setSecondName(hb.getSecondName());
    setTypeIdentificacion(hb.getTypeIdentificacion());
    setIdentificationCode(hb.getIdentificationCode());
    setNaturalLegalInd(hb.getNaturalLegalInd());
    setNationality(hb.getNationality());
    setNationalForeignInd(hb.getNationalForeignInd());
    setOwnerType(hb.getOwnerType());
    setAddress(hb.getAddress());
    setCity(hb.getCity());
    setPostalCode(hb.getPostalCode());
    setCountryResidence(hb.getCountryResidence());
    if (hb.getBirthDate() != null) {
      setBirthDate(sdf.format(hb.getBirthDate()));
    }
    else {
      setBirthDate("");
    }
  }

  public String getOwnerReference() {
    return ownerReference;
  }

  public String getOwnerName() {
    return ownerName;
  }

  public String getFirstSurname() {
    return firstSurname;
  }

  public String getSecondName() {
    return secondName;
  }

  public Character getTypeIdentificacion() {
    return typeIdentificacion;
  }

  public String getIdentificationCode() {
    return identificationCode;
  }

  public Character getNaturalLegalInd() {
    return naturalLegalInd;
  }

  public String getNationality() {
    return nationality;
  }

  public Character getNationalForeignInd() {
    return nationalForeignInd;
  }

  public Character getOwnerType() {
    return ownerType;
  }

  public String getAddress() {
    return address;
  }

  public String getCity() {
    return city;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public String getCountryResidence() {
    return countryResidence;
  }

  public void setOwnerReference(String ownerReference) {
    this.ownerReference = ownerReference;
  }

  public void setOwnerName(String ownerName) {
    this.ownerName = ownerName;
  }

  public void setFirstSurname(String firstSurname) {
    this.firstSurname = firstSurname;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public void setTypeIdentificacion(Character typeIdentificacion) {
    this.typeIdentificacion = typeIdentificacion;
  }

  public void setIdentificationCode(String identificationCode) {
    this.identificationCode = identificationCode;
  }

  public void setNaturalLegalInd(Character naturalLegalInd) {
    this.naturalLegalInd = naturalLegalInd;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public void setNationalForeignInd(Character nationalForeignInd) {
    this.nationalForeignInd = nationalForeignInd;
  }

  public void setOwnerType(Character ownerType) {
    this.ownerType = ownerType;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public void setCountryResidence(String countryResidence) {
    this.countryResidence = countryResidence;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  @Override
  public String toString() {
    return String
        .format(
            "EuroCcpOwnershipReferenceStaticDataDTO [ownerReference=%s, ownerName=%s, firstSurname=%s, secondName=%s, typeIdentificacion=%s, identificationCode=%s, naturalLegalInd=%s, nationality=%s, nationalForeignInd=%s, ownerType=%s, address=%s, city=%s, postalCode=%s, countryResidence=%s, birthDate=%s]",
            ownerReference, ownerName, firstSurname, secondName, typeIdentificacion, identificationCode,
            naturalLegalInd, nationality, nationalForeignInd, ownerType, address, city, postalCode, countryResidence,
            birthDate);
  }

}
