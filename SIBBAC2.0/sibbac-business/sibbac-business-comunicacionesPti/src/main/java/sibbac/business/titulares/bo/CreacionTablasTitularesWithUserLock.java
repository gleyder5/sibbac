package sibbac.business.titulares.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class CreacionTablasTitularesWithUserLock {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CreacionTablasTitularesWithUserLock.class);

  @Value("${sibbac.nacional.creacion.tablas.titulares.subthread.pool.size:3}")
  private int threadPoolSize;

  @Value("${sibbac.nacional.creacion.tablas.titulares.subthread.min.alc:1000}")
  private int minAlcs;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private CreacionTablasTiularesWriteLog creacionTablasTitularesBo;

  @Autowired
  private ApplicationContext ctx;

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void crearTablasTitulares(Tmct0bokId id, TitularesConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[crearTablasTitulares] inicio {}", id);
    boolean imLock = false;

    try {
      bokBo.bloquearBooking(id, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(),
          EstadosEnumerados.TITULARIDADES.TRATANDOSE.getId(), config.getAuditUser());
      imLock = true;
      List<Tmct0alcId> listAlcs = alcBo.findAllTmct0alcIdByNuordenAndNbookingAndActivosAndNotCdestadotit(
          id.getNuorden(), id.getNbooking(), EstadosEnumerados.TITULARIDADES.INCIDENCIAS.getId());
      if (CollectionUtils.isNotEmpty(listAlcs)) {
        if (listAlcs.size() < minAlcs) {
          for (Tmct0alcId alcId : listAlcs) {
            creacionTablasTitularesBo.crearTablasForProcess(alcId, config);
          }
        }
        else {
          this.lanzarHilosCreacionTablas(id, listAlcs, config);
        }
      }
      else {
        LOG.warn("[crearTablasTitulares] el booking : {} no tiene alcs para crear las tablas de titulares.", id);
      }

    }
    catch (LockUserException e) {
      LOG.warn(e.getMessage(), e);
    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }

    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(id, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(),
              EstadosEnumerados.TITULARIDADES.EN_CURSO.getId(), config.getAuditUser());

        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException("No se ha podido desbloquar el booking");
        }
      }
    }
    LOG.trace("[crearTablasTitulares] fin {}", id);
  }

  private void lanzarHilosCreacionTablas(Tmct0bokId id, List<Tmct0alcId> listAlcId, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    long size;
    Tmct0alcId alcid;
    String msg;
    ThreadFactoryBuilder tBuilder = new ThreadFactoryBuilder();
    tBuilder.setNameFormat(String.format("Thread-CreacionTablasTitularesBooking-%s-%s", id.getNbooking().trim(), "%d"));
    ExecutorService executor = Executors.newScheduledThreadPool(threadPoolSize, tBuilder.build());
    CreacionTablasTitularesByTmct0alcCallable desgloseAutomaticoCallable;
    Future<Void> future;
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    Map<Future<Void>, CreacionTablasTitularesByTmct0alcCallable> mapCallables = new HashMap<Future<Void>, CreacionTablasTitularesByTmct0alcCallable>();
    try {
      for (Tmct0alcId alcId : listAlcId) {
        LOG.debug("[lanzarHilosCreacionTablas] lanzando hilo para procesar allo: {}", alcId);
        try {
          desgloseAutomaticoCallable = ctx.getBean(CreacionTablasTitularesByTmct0alcCallable.class, alcId, config);
          future = executor.submit(desgloseAutomaticoCallable);
          listFutures.add(future);
          mapCallables.put(future, desgloseAutomaticoCallable);
          size = ((ThreadPoolExecutor) executor).getTaskCount()
              - ((ThreadPoolExecutor) executor).getCompletedTaskCount();
          if (size > 10000) {
            Thread.sleep(500);
          }
        }
        catch (Exception e) {
          msg = String.format("Incidencia lanzando hilo para alc %s %s", alcId, e.getMessage());
          throw new SIBBACBusinessException(msg, e);
        }
      }
      executor.shutdown();
    }
    catch (Exception e1) {
      LOG.warn(e1.getMessage(), e1);
      executor.shutdownNow();
    }
    finally {
      try {
        executor.awaitTermination(5 * listAlcId.size() + 10, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage(), e);
      }

      for (Future<Void> future2 : listFutures) {
        desgloseAutomaticoCallable = mapCallables.get(future2);
        alcid = null;
        if (desgloseAutomaticoCallable != null) {
          alcid = desgloseAutomaticoCallable.getTmct0alcId();
          LOG.trace("[lanzarHilosCreacionTablas] revisando resultado hilo alc: {}", alcid);
        }
        try {
          future2.get(2, TimeUnit.MINUTES);
        }
        catch (InterruptedException | ExecutionException e) {
          LOG.warn("[lanzarHilosCreacionTablas] incidencia con alc {} {}", alcid, e.getMessage(), e);
        }
        catch (TimeoutException e) {
          if (!future2.isDone() && future2.cancel(true)) {
            LOG.warn("[lanzarHilosCreacionTablas] cancelando el future: {} alc: {}", future2, alcid);
          }
          else {
            LOG.warn("[lanzarHilosCreacionTablas] incidencia con alc {} {}", alcid, e.getMessage(), e);
          }
        }
        catch (Exception e) {
          LOG.warn("[lanzarHilosCreacionTablas] incidencia con alc {} {}", alcid, e.getMessage(), e);
          // throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }

      if (!executor.isTerminated()) {
        executor.shutdownNow();
        try {
          executor.awaitTermination(1, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
      }
    }

  }
}
