package sibbac.business.comunicaciones.euroccp.ciffile.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpProcessedUnsettledMovementDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpAccountType;
import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpMovementCode;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;
import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0caseoperacionejeBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.dao.Tmct0movimientoeccFidessaDao;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;
import sibbac.database.bo.AbstractBo;

@Service
public class EuroCcpProcessedUnsettledMovementBo extends
    AbstractBo<EuroCcpProcessedUnsettledMovement, Long, EuroCcpProcessedUnsettledMovementDao> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpProcessedUnsettledMovementBo.class);

  @Value("${sibbac.euroccp.defaults.cta.compensacion:}")
  private String ctaDiariaEurorCcp;

  @Autowired
  private Tmct0operacioncdeccBo opEccBo;

  @Autowired
  private Tmct0caseoperacionejeBo caseEjeBo;

  @Autowired
  private Tmct0anotacioneccBo anotacionBo;

  @Autowired
  private Tmct0movimientoeccFidessaDao movimientoFidessaDao;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  public EuroCcpProcessedUnsettledMovementBo() {
  }

  // ****************************************************QUERYS*******************************************************/

  public EuroCcpProcessedUnsettledMovement findByUnsettledReferenceTradeOrTransferOrBalance(String unsettledReference) {
    EuroCcpProcessedUnsettledMovement res = null;
    List<EuroCcpProcessedUnsettledMovement> l = dao.findByUnsettledReferenceTradeOrTransferOrBalance(
        unsettledReference, EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId());
    if (!l.isEmpty()) {
      res = l.get(0);
    }
    return res;
  }

  /**
   * @param estadoProcesado
   * @param fechaLiqLte
   * @return
   */
  public List<EuroCcpIdDTO> findEuroCcpIdDTOByEstadoProcesadoAndTransactionDateLte(int estadoProcesado,
      Date transactionDateLte, int maxRegs) {
    List<EuroCcpIdDTO> res = new ArrayList<EuroCcpIdDTO>();
    List<EuroCcpIdDTO> resAux;

    List<Date> transactionDates = dao.findDistinctTransactionDateByEstadoProcesado(estadoProcesado);
    Pageable page = new PageRequest(0, maxRegs);
    if (CollectionUtils.isNotEmpty(transactionDates)) {
      for (Date transactionDate : transactionDates) {
        if (transactionDate.compareTo(transactionDateLte) <= 0) {
          LOG.trace(
              "[findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndTransactionDateLte] Buscando EuroCcpProcessedUnsettledMovement de transaction date: {}",
              transactionDate);
          resAux = dao.findIdByEstadoProcesadoAndTransactionDate(estadoProcesado, transactionDate, page);

          LOG.trace(
              "[findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndTransactionDateLte] Encontradas {} EuroCcpProcessedUnsettledMovement de transaction date: {}",
              resAux.size(), transactionDate);

          if (CollectionUtils.isNotEmpty(resAux)) {

            res.addAll(resAux);
            if (res.size() >= maxRegs) {
              break;
            }
          }
        }
      }
    }
    return res;
  }

  // ************************************************QUERYS fin
  // *******************************************************/

  // ****************************************************BUSSINES*******************************************************/

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void grabarOperacioncdeccAndCase(List<EuroCcpIdDTO> ids, String cdcamaraEuroCcp, String auditUser)
      throws SIBBACBusinessException {
    LOG.trace("[grabarOperacioncdeccAndCase] inicio");
    EuroCcpProcessedUnsettledMovement movement;

    for (EuroCcpIdDTO euroCcpIdDTO : ids) {
      LOG.trace("[grabarOperacioncdeccAndCase] id: {}", euroCcpIdDTO);
      movement = dao.findOne(euroCcpIdDTO.getId());
      if (movement != null) {
        if (movement.getMovementCode().equals(EuroCcpMovementCode.TRADE_01.text)) {
          this.procesarTrade01(movement, cdcamaraEuroCcp, auditUser);
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.TRADE_CORRECTION_04.text)) {
          this.procesarTradeCorrection04(movement, cdcamaraEuroCcp, auditUser);
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.TRADE_TRANSFER_05.text)) {
          this.procesarTradeTransfer05(movement, cdcamaraEuroCcp, auditUser);
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.TRADE_BALANCE_06.text)) {
          this.procesarTradeBalance06(movement, cdcamaraEuroCcp, auditUser);
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.TRADE_OUT_07.text)) {
          LOG.warn("[grabarOperacioncdeccAndCase] NOT IMPLEMENTED- Trade Out.");
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.TRADE_IN_08.text)) {
          LOG.warn("[grabarOperacioncdeccAndCase] NOT IMPLEMENTED- Trade In.");
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.POSITION_OUT_15.text)) {
          LOG.warn("[grabarOperacioncdeccAndCase] NOT IMPLEMENTED- Position Out.");
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId());
        }
        else if (movement.getMovementCode().equals(EuroCcpMovementCode.POSITION_IN_16.text)) {
          LOG.warn("[grabarOperacioncdeccAndCase] NOT IMPLEMENTED- Position In.");
          movement.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId());
        }

        movement.setAuditDate(new Date());
        movement.setAuditUser(auditUser);
        save(movement);

      }
    }
    LOG.trace("[grabarOperacioncdeccAndCase] fin");
  }

  private void procesarTrade01(EuroCcpProcessedUnsettledMovement movement, String cdcamaraEuroCcp, String auditUser)
      throws SIBBACBusinessException {
    LOG.info("[procesarTrade01] Trade.");
    Tmct0operacioncdecc opEcc;
    Tmct0anotacionecc anotacion;
    Tmct0caseoperacioneje caseEje;
    Tmct0movimientoeccFidessa movFidessa;
    try {
      opEcc = opEccBo.findByCdoperacionecc(movement.getUnsettledReference());
      if (opEcc != null) {
        LOG.warn("[procesarTrade01] ha llegado una retrasmision de la op: {}", opEcc.getCdoperacionecc());
      }
      else {
        LOG.trace("[procesarTrade01] Trade.");
        opEcc = this.createOperacioncdecc(movement, cdcamaraEuroCcp, auditUser);

        opEcc = opEccBo.save(opEcc);
        LOG.trace("[procesarTrade01] save op ecc: {}", opEcc);

        caseEje = this.createCase(opEcc);

        caseEje = caseEjeBo.save(caseEje);
        LOG.trace("[procesarTrade01] save case: {}", caseEje);

        anotacion = this.createAnotacion(opEcc, auditUser);
        anotacion.setCdoperacion('M');
        anotacionBo.save(anotacion);
        LOG.trace("[procesarTrade01] save anotacion: {}", anotacion);
        //Comprobamos si la cuenta en la que cae la operación es la que tenemos definida por defecto
        if (ctaDiariaEurorCcp == null || opEcc.getTmct0CuentasDeCompensacion() != null
            && !ctaDiariaEurorCcp.trim().equals(opEcc.getTmct0CuentasDeCompensacion().getCdCodigo().trim())) {
          LOG.trace("[procesarTrade01] La cuenta del trade no es la cuenta configurada como diaria:{} cta trade: {}.",
              ctaDiariaEurorCcp, opEcc.getTmct0CuentasDeCompensacion().getCdCodigo());
          movFidessa = this.createMovFidessa(movement, opEcc, auditUser);
          movFidessa = movimientoFidessaDao.save(movFidessa);
          LOG.trace("[procesarTrade01] save mov.fidessa: {}", movFidessa);
        }
        else {
          LOG.trace("[procesarTrade01] La cuenta de la operacion es la diaria por lo que no insertamos movimiento.");
        }
        LOG.trace("[procesarTrade01] save case: {}", caseEje);
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA- procesando TRADE 01 id: '%s' op: '%s' err: %s",
          movement.getId(), movement.getUnsettledReference(), e.getMessage()), e);
    }
  }

/**
 * Restamos los títulos al 01
 * */
  private void procesarTradeCorrection04(EuroCcpProcessedUnsettledMovement movement, String cdcamaraEuroCcp,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[procesarTradeCorrection04] Trade Correction.");
    Tmct0anotacionecc anotacion;
    try {
      LOG.trace("[procesarTradeCorrection04] buscando op ecc...");
      Tmct0operacioncdecc opEcc = opEccBo.findByCdoperacionecc(movement.getUnsettledReference());

      if (opEcc != null) {
        if (movement.getBuySellCode() == 'B') {
          LOG.trace("[procesarTradeCorrection04] BUY restando titulos disponibles {} menos {}.",
              opEcc.getImtitulosdisponibles(), movement.getProcessedQuantityShort());
          opEcc.setImtitulosdisponibles(opEcc.getImtitulosdisponibles().subtract(movement.getProcessedQuantityShort()));
        }
        else {
          LOG.trace("[procesarTradeCorrection04] SELL restando titulos disponibles {} menos {}.",
              opEcc.getImtitulosdisponibles(), movement.getProcessedQuantityLong());
          opEcc.setImtitulosdisponibles(opEcc.getImtitulosdisponibles().subtract(movement.getProcessedQuantityLong()));

        }
        LOG.trace("[procesarTradeCorrection04] restando efectivo disponible a la op {} menos {}.",
            opEcc.getImefectivodisponible(), movement.getEffectiveValue());
        opEcc.setImefectivodisponible(opEcc.getImefectivodisponible().subtract(movement.getEffectiveValue()));
        opEcc.setAuditFechaCambio(new Date());
        opEcc.setAuditUser(auditUser);
        opEccBo.save(opEcc);
      }

      LOG.trace("[procesarTradeCorrection04] buscando anotacion...");
      anotacion = anotacionBo.findByCdoperacionecc(movement.getUnsettledReference());
      if (anotacion != null) {
        if (movement.getBuySellCode() == 'B') {
          LOG.trace("[procesarTradeCorrection04] BUY restando titulos disponibles {} menos {}.",
              anotacion.getImtitulosdisponibles(), movement.getProcessedQuantityShort());
          anotacion.setImtitulosdisponibles(anotacion.getImtitulosdisponibles().subtract(
              movement.getProcessedQuantityShort()));
        }
        else {
          LOG.trace("[procesarTradeCorrection04] SELL restando titulos disponibles {} menos {}.",
              anotacion.getImtitulosdisponibles(), movement.getProcessedQuantityLong());
          anotacion.setImtitulosdisponibles(anotacion.getImtitulosdisponibles().subtract(
              movement.getProcessedQuantityLong()));
        }
        LOG.trace("[procesarTradeCorrection04] restando efectivo disponible a la anotacion {} menos {}.",
            anotacion.getImefectivodisponible(), movement.getEffectiveValue());
        anotacion.setImefectivodisponible(anotacion.getImefectivodisponible().subtract(movement.getEffectiveValue()));
        anotacion.setAuditFechaCambio(new Date());
        anotacion.setAuditUser(auditUser);
        anotacionBo.save(anotacion);
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA- procesando TRADE CORRECTION 04 id: '%s' op: '%s' err: %s", movement.getId(),
          movement.getUnsettledReference(), e.getMessage()), e);
    }
  }

  private void procesarTradeTransfer05(EuroCcpProcessedUnsettledMovement movement, String cdcamaraEuroCcp,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[procesarTradeTransfer05] Trade Transfer.");
    EuroCcpProcessedUnsettledMovement movementTradeOrTransferAnterior = null;
    EuroCcpProcessedUnsettledMovement movementTradeCorrection = null;
    EuroCcpProcessedUnsettledMovement movementInicial = null;
    Tmct0anotacionecc anotacion;
    Tmct0operacioncdecc opEcc;
    Tmct0movimientoeccFidessa movFidessa;
    try {
      anotacion = anotacionBo.findByCdoperacionecc(movement.getUnsettledReference());
      if (anotacion != null) {
        LOG.warn("[procesarTradeTransfer05] ha llegado una retrasmision de la op: {}", anotacion.getCdoperacionecc());
        List<Tmct0movimientoeccFidessa> listMovimientos = movimientoFidessaDao.findAllByCdoperacionecc(anotacion
            .getCdoperacionecc());
        if (!listMovimientos.isEmpty()) {
          movFidessa = listMovimientos.get(0);
          this.actualizarCodigosOperacionMovimientosOrdenes(anotacion, movFidessa, auditUser);
        }
      }     
      else {
        movementTradeCorrection = this.getLastTradeCorrection(movement);
        if (movementTradeCorrection != null) {
          movementTradeOrTransferAnterior = this.getLastTradeOrTransferFromTradeCorrection(movementTradeCorrection);
          if (movementTradeOrTransferAnterior != null) {
            LOG.trace("[procesarTradeTransfer05] Trade.");
            opEcc = this.createOperacioncdecc(movement, cdcamaraEuroCcp, auditUser);
            LOG.trace("[procesarTradeTransfer05] save op ecc: {}", opEcc);

            anotacion = this.createAnotacion(opEcc, auditUser);
            anotacion.setCdoperacion('T');
            anotacion.setNuoperacionprev(movementTradeOrTransferAnterior.getUnsettledReference());
            if (movementTradeOrTransferAnterior.getMovementCode().equals(EuroCcpMovementCode.TRADE_01.text)) {
              anotacion.setNuoperacioninic(movementTradeOrTransferAnterior.getUnsettledReference());
            }
            else {
              movementInicial = this.getInitialTrade(movementTradeOrTransferAnterior);
              if (movementInicial != null) {
                anotacion.setNuoperacioninic(movementInicial.getUnsettledReference());
              }
              else {
                LOG.warn("[procesarTradeTransfer05] no se ha encontrado la operacion inicial para: {}",
                    movement.getUnsettledReference());
              }
            }

            anotacionBo.save(anotacion);
            LOG.trace("[procesarTradeTransfer05] save anotacion: {}", anotacion);

            movFidessa = this.createMovFidessa(movement, opEcc, auditUser);
            movFidessa.setNuoperacionprev(anotacion.getNuoperacionprev());
            movFidessa.setCdrefnotificacion(anotacion.getCdoperacionecc());
            movFidessa.setCdrefnotificacionprev(anotacion.getNuoperacionprev());
            movFidessa = movimientoFidessaDao.save(movFidessa);

            this.actualizarCodigosOperacionMovimientosOrdenes(anotacion, movFidessa, auditUser);

            LOG.trace("[procesarTradeTransfer05] save mov.fidessa: {}", movFidessa);

          }
          else {
            LOG.warn(
                "[procesarTradeTransfer05] Err: no encontrado trade or trade transfer anterior para el trade correction: {} trade transfer: {}",
                movementTradeCorrection, movement);
          }
        }
        else {
          LOG.warn("[procesarTradeTransfer05] Err: no encontrado trade correction anterior para el trade transfer: {}",
              movement);
        }
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA- procesando TRADE TRANSFER 05 id: '%s' op: '%s' err: %s", movement.getId(),
          movement.getUnsettledReference(), e.getMessage()), e);
    }
  }

  private void procesarTradeBalance06(EuroCcpProcessedUnsettledMovement movement, String cdcamaraEuroCcp,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[procesarTradeBalance06] Trade balance.");
    EuroCcpProcessedUnsettledMovement movementTradeOrTransferAnterior = null;
    EuroCcpProcessedUnsettledMovement movementTradeCorrection = null;
    Tmct0anotacionecc anotacion;
    Tmct0operacioncdecc opEcc;
    try {

      movementTradeCorrection = this.getLastTradeCorrection(movement);
      if (movementTradeCorrection != null) {
        movementTradeOrTransferAnterior = this.getLastTradeOrTransferFromTradeCorrection(movementTradeCorrection);
        if (movementTradeOrTransferAnterior != null) {
          LOG.trace("[procesarTradeBalance06] buscando op ecc...");
          opEcc = opEccBo.findByCdoperacionecc(movementTradeOrTransferAnterior.getUnsettledReference());

          if (opEcc != null) {
            if (movement.getBuySellCode() == 'B') {
              LOG.trace("[procesarTradeBalance06] BUY sumando titulos disponibles {} menos {}.",
                  opEcc.getImtitulosdisponibles(), movement.getProcessedQuantityLong());
              opEcc.setImtitulosdisponibles(movement.getProcessedQuantityLong().setScale(6, RoundingMode.HALF_UP));
            }
            else {
              LOG.trace("[procesarTradeBalance06] SELL sumando titulos disponibles {} menos {}.",
                  opEcc.getImtitulosdisponibles(), movement.getProcessedQuantityShort());
              opEcc.setImtitulosdisponibles(movement.getProcessedQuantityShort().setScale(6, RoundingMode.HALF_UP));

            }
            LOG.trace("[procesarTradeBalance06] sumando efectivo disponible a la op {} menos {}.",
                opEcc.getImefectivodisponible(), movement.getEffectiveValue());
            opEcc.setImefectivodisponible(movement.getEffectiveValue().setScale(2, RoundingMode.HALF_UP));
            opEcc.setAuditFechaCambio(new Date());
            opEcc.setAuditUser(auditUser);
            opEccBo.save(opEcc);
          }

          LOG.trace("[procesarTradeBalance06] buscando anotacion...");
          anotacion = anotacionBo.findByCdoperacionecc(movementTradeOrTransferAnterior.getUnsettledReference());
          if (anotacion != null) {
            if (movement.getBuySellCode() == 'B') {
              LOG.trace("[procesarTradeBalance06] BUY sumando titulos disponibles {} menos {}.",
                  anotacion.getImtitulosdisponibles(), movement.getProcessedQuantityLong());
              anotacion.setImtitulosdisponibles(movement.getProcessedQuantityLong().setScale(6, RoundingMode.HALF_UP));
            }
            else {
              LOG.trace("[procesarTradeBalance06] SELL sumando titulos disponibles {} menos {}.",
                  anotacion.getImtitulosdisponibles(), movement.getProcessedQuantityShort());
              anotacion.setImtitulosdisponibles(movement.getProcessedQuantityShort().setScale(6, RoundingMode.HALF_UP));
            }
            LOG.trace("[procesarTradeBalance06] sumando efectivo disponible a la anotacion {} menos {}.",
                anotacion.getImefectivodisponible(), movement.getEffectiveValue());
            anotacion.setImefectivodisponible(movement.getEffectiveValue().setScale(2, RoundingMode.HALF_UP));
            anotacion.setAuditFechaCambio(new Date());
            anotacion.setAuditUser(auditUser);
            anotacionBo.save(anotacion);
          }
        }
        else {
          LOG.warn(
              "[procesarTradeBalance06] Err: no encontrado trade or trade transfer anterior para el trade correction: {} trade balance: {}",
              movementTradeCorrection, movement);
        }
      }
      else {
        LOG.warn("[procesarTradeBalance06] Err: no encontrado trade correction anterior para el trade balance: {}",
            movement);
      }

    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA- procesando TRADE BALANCE 06 id: '%s' op: '%s' err: %s", movement.getId(),
          movement.getUnsettledReference(), e.getMessage()), e);
    }
  }

  private EuroCcpProcessedUnsettledMovement getLastTradeCorrection(EuroCcpProcessedUnsettledMovement movement) {
    EuroCcpProcessedUnsettledMovement res = null;
    List<EuroCcpProcessedUnsettledMovement> l = dao
        .findAllByTransactionDateAndEstadoProcesadoGtAndExchangeCodeTradeAndExternalTransactionIdExchangeAndMovementCodeAndIdLt(
            movement.getTransactionDate(), EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId(),
            movement.getExchangeCodeTrade(), movement.getExternalTransactionIdExchange(),
            EuroCcpMovementCode.TRADE_CORRECTION_04.text, movement.getId(), (movement.getId() + 1));

    if (!l.isEmpty()) {
      res = l.get(0);
    }   
    
    if (res != null) {
      EuroCcpProcessedUnsettledMovement previous = this.findByUnsettledReferenceTradeOrTransferOrBalance(res
          .getUnsettledReference());

      if (previous != null && previous.getMovementCode().equals(EuroCcpMovementCode.TRADE_BALANCE_06.text)) {
        res = this.getLastTradeCorrection(previous);
      }
    }
    return res;
  }

  private EuroCcpProcessedUnsettledMovement getLastTradeOrTransferFromTradeCorrection(
      EuroCcpProcessedUnsettledMovement movementTradeCorrection) {
    EuroCcpProcessedUnsettledMovement res = null;
    if (movementTradeCorrection.getMovementCode().equals(EuroCcpMovementCode.TRADE_CORRECTION_04.text)) {
      res = this.findByUnsettledReferenceTradeOrTransferOrBalance(movementTradeCorrection.getUnsettledReference());
    }

    if (res != null) {
      EuroCcpProcessedUnsettledMovement previous = this.findByUnsettledReferenceTradeOrTransferOrBalance(res
          .getUnsettledReference());

      if (previous != null && previous.getMovementCode().equals(EuroCcpMovementCode.TRADE_BALANCE_06.text)) {
        movementTradeCorrection = this.getLastTradeCorrection(previous);
        if (movementTradeCorrection != null) {
          res = getLastTradeOrTransferFromTradeCorrection(movementTradeCorrection);
        }
      }
    }
    return res;

  }

  private void actualizarCodigosOperacionMovimientosOrdenes(Tmct0anotacionecc anotacion,
      Tmct0movimientoeccFidessa movFidessa, String auditUser) {
    this.actualizarCodigosOperacionMovimientosOrdenesPorDesgloses(anotacion, movFidessa, auditUser);

  }

  @SuppressWarnings("unused")
  private void actualizarCodigosOperacionMovimientosOrdenesPorMovimientosOpNueva(Tmct0anotacionecc anotacion,
      Tmct0movimientoeccFidessa movFidessa, String auditUser) {
    List<Tmct0movimientooperacionnuevaecc> listMovsn = movnBo.findBycdoperacionecc(anotacion.getNuoperacionprev());
    this.actualizarCodigosOperacionMovimientosOrdenes(anotacion, movFidessa, listMovsn, auditUser);
  }

  private void actualizarCodigosOperacionMovimientosOrdenesPorDesgloses(Tmct0anotacionecc anotacion,
      Tmct0movimientoeccFidessa movFidessa, String auditUser) {
    List<Tmct0movimientooperacionnuevaecc> listMovsn;
    List<Tmct0desgloseclitit> listDesgloses = dctBo.findAllByCdoperacionecc(movFidessa.getNuoperacionprev());
    if (CollectionUtils.isNotEmpty(listDesgloses)) {
      for (Tmct0desgloseclitit dct : listDesgloses) {
        if (dct != null
            && (dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.ENVIANDOSE
                .getId() || dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO
                .getId())) {
          listMovsn = movnBo.findByNudesglose(dct.getNudesglose());
          this.actualizarCodigosOperacionMovimientosOrdenes(anotacion, movFidessa, listMovsn, auditUser);

          this.darBajaMovimientoAnterior(movFidessa, listMovsn, auditUser);

        }
      }
    }
  }

  private void darBajaMovimientoAnterior(Tmct0movimientoeccFidessa movFidessa,
      List<Tmct0movimientooperacionnuevaecc> listMovn, String auditUser) {
    if (listMovn.size() > 1) {
      Tmct0movimientoecc movecc;
      for (Tmct0movimientooperacionnuevaecc movn : listMovn) {
        movecc = movn.getTmct0movimientoecc();
        if (movecc.getCdestadomov() != null
            && !movecc.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)
            && movecc.getTmct0desglosecamara() != null
            && movecc.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {
          if (movecc.getCuentaCompensacionDestino() != null && movFidessa.getCuentaCompensacionDestino() != null
              && !movecc.getCuentaCompensacionDestino().trim().equals(movFidessa.getCuentaCompensacionDestino().trim())
              && movFidessa.getCdoperacionecc() != null && movn.getCdoperacionecc() != null
              && !movFidessa.getCdoperacionecc().trim().equals(movn.getCdoperacionecc().trim())) {
            movecc.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
            movecc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
            movecc.setAuditFechaCambio(new Date());
            movecc.setAuditUser(auditUser);
            movBo.save(movecc);
          }
        }
      }
    }
  }

  private void actualizarCodigosOperacionMovimientosOrdenes(Tmct0anotacionecc anotacion,
      Tmct0movimientoeccFidessa movFidessa, List<Tmct0movimientooperacionnuevaecc> listMovsn, String auditUser) {
    Tmct0movimientoecc movecc;
    Tmct0desgloseclitit dct;
    if (CollectionUtils.isNotEmpty(listMovsn)) {
      for (Tmct0movimientooperacionnuevaecc movn : listMovsn) {
        movecc = movn.getTmct0movimientoecc();
        if (movecc.getCdestadomov() != null
            && !movecc.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)
            && movecc.getTmct0desglosecamara() != null
            && movecc.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {
          if (movecc.getCuentaCompensacionDestino() != null && movFidessa.getCuentaCompensacionDestino() != null
              && movecc.getCuentaCompensacionDestino().trim().equals(movFidessa.getCuentaCompensacionDestino().trim())
              && movFidessa.getNuoperacionprev() != null && movn.getCdoperacionecc() != null
              && movFidessa.getNuoperacionprev().trim().equals(movn.getCdoperacionecc().trim())
              && movFidessa.getImtitulosPendientesAsignar().compareTo(movn.getImtitulos()) == 0) {

            movFidessa.setImtitulosPendientesAsignar(BigDecimal.ZERO);
            movFidessa.setCdrefmovimiento(movecc.getCdrefmovimiento());
            movFidessa.setCdrefinternaasig(movecc.getCdrefinternaasig());

            movn.setIdMovimientoFidessa(movFidessa.getIdMovimientoFidessa());
            movn.setCdoperacionecc(movFidessa.getCdoperacionecc());
            movn.setAuditFechaCambio(new Date());
            movn.setAuditUser(auditUser);
            movnBo.save(movn);
            movecc.setCdrefmovimientoecc(movFidessa.getCdrefmovimientoecc());
            movecc.setCdrefnotificacion(movFidessa.getCdrefnotificacion());
            movecc.setCdrefnotificacionprev(movFidessa.getCdrefnotificacionprev());
            movecc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
            if (movecc.getCdestadomov() != null
                && !movecc.getCdestadomov().trim().equals(EstadosEccMovimiento.FINALIZADO.text)) {
              movecc.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
            }
            movecc.setAuditFechaCambio(new Date());
            movecc.setAuditUser(auditUser);
            movBo.save(movecc);

            if (movn.getNudesglose() != null) {
              dct = dctBo.findById(movn.getNudesglose());
              if (dct != null
                  && dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                      .getId()) {
                dctBo.actualizarCodigosOperacion(movFidessa, dct, auditUser);
                dctBo.save(dct);
              }
            }
            break;
          }
        }
      }
    }
  }

  private EuroCcpProcessedUnsettledMovement getInitialTrade(EuroCcpProcessedUnsettledMovement movement) {
    if (movement != null) {
      List<EuroCcpProcessedUnsettledMovement> l = dao
          .findAllByTransactionDateAndEstadoProcesadoGtAndExchangeCodeTradeAndExternalTransactionIdExchangeAndMovementCode(
              movement.getTransactionDate(), EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId(),
              movement.getExchangeCodeTrade(), movement.getExternalTransactionIdExchange(),
              EuroCcpMovementCode.TRADE_01.text);
      if (!l.isEmpty()) {
        return l.get(0);
      }
    }
    return null;
  }

  private Tmct0operacioncdecc createOperacioncdecc(EuroCcpProcessedUnsettledMovement movement, String cdcamaraEuroCcp,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[createOperacioncdecc] inicio");

    Tmct0CuentasDeCompensacion cc;

    Tmct0operacioncdecc opEcc = new Tmct0operacioncdecc();
    opEcc.setCdcamara(cdcamaraEuroCcp);
    opEcc.setCdisin(movement.getIsinCode());
    opEcc.setCdmiembromkt("");
    opEcc.setCdoperacionecc(movement.getUnsettledReference());
    opEcc.setCdoperacionmkt("");
    opEcc.setCdPlataformaNegociacion(movement.getExchangeCodeTrade());
    opEcc.setCdsegmento("");
    if (movement.getAccountType().equals(EuroCcpAccountType.CLIENT.text)) {
      LOG.trace("[createOperacioncdecc] AccountType: {} Ajena.", movement.getAccountType());
      opEcc.setCdindcapacidad('A');
    }
    else {
      LOG.trace("[createOperacioncdecc] AccountType: {} Propia.", movement.getAccountType());
      opEcc.setCdindcapacidad('P');
    }

    opEcc.setCdindcotizacion('U');
    opEcc.setCdrefasigext("");
    opEcc.setCdrefcomun("");
    opEcc.setCdrefcliente("");
    opEcc.setCdrefextorden("");
    opEcc.setCorretaje(BigDecimal.ZERO);
    LOG.trace("[grabarOperacioncdeccAndCase] searching cta C.Number: {} Acc.Number: {} Sub.Acc.Number: {} ...",
        movement.getClientNumber(), movement.getAccountNumber(), movement.getSubaccountNumber());

    cc = cuentaCompensacionBo.findByClientNumberAndAccountNumberAndSubaccountNumber(movement.getClientNumber(),
        movement.getAccountNumber(), movement.getSubaccountNumber());
    if (cc == null) {
      throw new SIBBACBusinessException(MessageFormat.format(
          "INCIDENCIA-No encontrada la cuenta C.Number: {0} Acc.Number: {1} Sub.Acc.Number: {2}",
          movement.getClientNumber(), movement.getAccountNumber(), movement.getSubaccountNumber()));
    }

    LOG.trace("[grabarOperacioncdeccAndCase] settlement cta: {}", cc.getCdCodigo());
    opEcc.setTmct0CuentasDeCompensacion(cc);
    // opEcc.setCdindcotizacion('N');

    // TODO XAS CONVERSION SENDITO EUROCCP
    if (movement.getBuySellCode() == 'B') {
      LOG.trace("[grabarOperacioncdeccAndCase] BUY.");
      opEcc.setCdsentido('C');
      opEcc.setImtitulosdisponibles(movement.getProcessedQuantityLong().setScale(6, RoundingMode.HALF_UP));
      opEcc.setImtitulosretenidos(BigDecimal.ZERO);
      opEcc.setNutitulos(movement.getProcessedQuantityLong().setScale(6, RoundingMode.HALF_UP));
    }
    else {
      LOG.trace("[grabarOperacioncdeccAndCase] SELL.");
      opEcc.setCdsentido('V');
      opEcc.setImtitulosdisponibles(movement.getProcessedQuantityShort().setScale(6, RoundingMode.HALF_UP));
      opEcc.setImtitulosretenidos(BigDecimal.ZERO);
      opEcc.setNutitulos(movement.getProcessedQuantityShort().setScale(6, RoundingMode.HALF_UP));
    }

    opEcc.setFcontratacion(movement.getTransactionDate());
    opEcc.setFliqteorica(movement.getSettlementDate());
    opEcc.setFnegociacion(movement.getTransactionDate());
    opEcc.setFregistroecc(movement.getProcessingDate());
    opEcc.setFordenmkt(movement.getTransactionDate());
    opEcc.setHnegociacion(movement.getTimestampField());
    opEcc.setHordenmkt(movement.getTimestampField());
    opEcc.setHregistroecc(movement.getTimestampField());
    opEcc.setImefectivo(movement.getEffectiveValue());
    opEcc.setImefectivodisponible(movement.getEffectiveValue().setScale(2, RoundingMode.HALF_UP));
    opEcc.setImefectivoretenido(BigDecimal.ZERO);
    opEcc.setImprecio(movement.getTransactionPrice());
    opEcc.setNuoperacioninic(movement.getUnsettledReference());
    opEcc.setNuoperacionprev(movement.getUnsettledReference());
    opEcc.setNuordenmkt("");
    opEcc.setNuexemkt(movement.getExternalTransactionIdExchange().substring(1));
    opEcc.setAuditFechaCambio(new Date());
    opEcc.setAuditUser(auditUser);
    LOG.trace("[createOperacioncdecc] fin");
    return opEcc;
  }

  private Tmct0caseoperacioneje createCase(Tmct0operacioncdecc opEcc) {
    LOG.trace("[createCase] inicio");
    Tmct0caseoperacioneje caseEje = new Tmct0caseoperacioneje();
    caseEje.setTmct0operacioncdecc(opEcc);
    CloneObjectHelper.copyBasicFields(opEcc, caseEje, null);
    caseEje.setFeexemkt(opEcc.getFcontratacion());
    caseEje.setHoexemkt(opEcc.getHordenmkt());
    caseEje.setHoordenmkt(opEcc.getHordenmkt());
    caseEje.setFeordenmkt(opEcc.getFordenmkt());

    caseEje.setNutitulostotal(opEcc.getNutitulos());
    caseEje.setNutitpendcase(opEcc.getNutitulos());
    caseEje.setAuditFechaCambio(new Date());
    LOG.trace("[createCase] fin");
    return caseEje;
  }

  private Tmct0anotacionecc createAnotacion(Tmct0operacioncdecc opEcc, String auditUser) {
    LOG.trace("[createAnotacion] inicio");
    Tmct0anotacionecc anotacion = new Tmct0anotacionecc();
    CloneObjectHelper.copyBasicFields(opEcc, anotacion, null);
    anotacion.setAuditFechaCambio(new Date());
    anotacion.setAuditUser(auditUser);
    anotacion.setTmct0CuentasDeCompensacion(opEcc.getTmct0CuentasDeCompensacion());
    LOG.trace("[createAnotacion] fin");
    return anotacion;
  }

  private Tmct0movimientoeccFidessa createMovFidessa(EuroCcpProcessedUnsettledMovement movement,
      Tmct0operacioncdecc opEcc, String auditUser) {
    LOG.trace("[createMovFidessa] inicio");
    Tmct0movimientoeccFidessa movFidessa = new Tmct0movimientoeccFidessa();
    CloneObjectHelper.copyBasicFields(opEcc, movFidessa, null);
    movFidessa.setCdindcotizacion(opEcc.getCdindcotizacion());
    movFidessa.setFechaOrden(opEcc.getFordenmkt());
    movFidessa.setHoraOrden(opEcc.getHordenmkt());
    movFidessa.setFliquidacion(opEcc.getFliqteorica());
    movFidessa.setFnegociacion(opEcc.getFnegociacion());
    movFidessa.setIndicadorCapacidad(opEcc.getCdindcapacidad());
    movFidessa.setMiembroCompensadorDestino("");
    movFidessa.setMiembroDestino("");
    movFidessa.setMiembroMercado(opEcc.getCdmiembromkt());
    movFidessa.setMiembroOrigen("");
    movFidessa.setMnemotecnico("");
    movFidessa.setPlataforma(opEcc.getCdPlataformaNegociacion());
    movFidessa.setPrecio(opEcc.getImprecio());
    movFidessa.setSegmento(opEcc.getCdsegmento());
    movFidessa.setSentido(opEcc.getCdsentido());
    movFidessa.setUsuarioDestino("");
    movFidessa.setUsuarioOrigen("");
    movFidessa.setCuentaCompensacionDestino(opEcc.getTmct0CuentasDeCompensacion().getCdCodigo());
    movFidessa.setCdrefmovimiento(movement.getUnsettledReference());
    movFidessa.setCdrefmovimientoecc(movement.getUnsettledReference());
    movFidessa.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
    movFidessa.setCdOperacionMercado(opEcc.getCdoperacionmkt());
    movFidessa.setCdrefnotificacion(movement.getUnsettledReference());
    movFidessa.setCdrefnotificacionprev("");
    movFidessa.setCdtipomov("");
    movFidessa.setCdrefasignacion("");
    movFidessa.setCdRefAsignacion("");
    movFidessa.setCdrefinternaasig("");
    movFidessa.setEnvios3('N');
    movFidessa.setImtitulosPendientesAsignar(opEcc.getNutitulos());
    movFidessa.setImtitulos(opEcc.getNutitulos());
    movFidessa.setAuditFechaCambio(new Date());
    movFidessa.setAuditUser(auditUser);
    LOG.trace("[createMovFidessa] fin");
    return movFidessa;

  }

  // ****************************************************BUSSINES*******************************************************/

}