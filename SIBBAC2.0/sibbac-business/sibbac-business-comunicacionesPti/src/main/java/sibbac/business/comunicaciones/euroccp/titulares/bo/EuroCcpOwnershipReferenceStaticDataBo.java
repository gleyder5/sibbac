package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpOwnershipReferenceStaticDataDao;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReferenceStaticDataDTO;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReferenceStaticData;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.database.bo.AbstractBo;

@Service
public class EuroCcpOwnershipReferenceStaticDataBo extends
    AbstractBo<EuroCcpOwnershipReferenceStaticData, Long, EuroCcpOwnershipReferenceStaticDataDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpOwnershipReferenceStaticDataBo.class);

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0enviotitularidadDao envioTiDao;

  public List<EuroCcpOwnershipReferenceStaticData> findByOwnerReferenceAndEstadoProcesadoGte(String ownerReference,
      int estadoProcesadoGte) {
    return dao.findByOwnerReferenceAndEstadoProcesadoGte(ownerReference, estadoProcesadoGte);
  }

  public EuroCcpOwnershipReferenceStaticData findByOwnerReferenceAndIdentificationCodeAndEstadoProcesadoGte(
      String ownerReference, String identification, int estadoProcesadoGte) {
    return dao.findByOwnerReferenceAndIdentificationCodeAndEstadoProcesadoGte(ownerReference, identification,
        estadoProcesadoGte);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public synchronized void rechazarDatosEstaticosTiOrsNewTransaction(Tmct0alcId alcId, Date feejecuc,
      String cdmiembromkt, String cdcamara, String ownerReferenceFrom, String ownerReferenceTo, String auditUser) {

    this.rechazarDatosEstaticosTiOrs(alcId, feejecuc, cdmiembromkt, cdcamara, ownerReferenceTo, "to", auditUser);
    this.rechazarDatosEstaticosTiOrs(alcId, feejecuc, cdmiembromkt, cdcamara, ownerReferenceFrom, "from", auditUser);
  }

  private void rechazarDatosEstaticosTiOrs(Tmct0alcId alcId, Date feejecuc, String cdmiembromkt, String cdcamara,
      String ownerReference, String toFrom, String auditUser) {
    String msg;
    Tmct0enviotitularidad envTiTo;

    if (ownerReference != null && !ownerReference.trim().isEmpty()) {
      List<EuroCcpOwnershipReferenceStaticData> listDatosEstaticosTitular = dao
          .findByOwnerReferenceAndEstadoProcesadoGte(ownerReference,
              EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());

      if (listDatosEstaticosTitular.isEmpty()) {
        msg = MessageFormat.format(
            "EuroCcp-RevEstadoOrgs-ORS-ERR-Unknown owner reference {1} {0} no se ha encontrado registro del envio.",
            ownerReference, toFrom);
        LOG.warn("[procesarRecepcionTitulares] {}", msg);
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
      }
      else {
        for (EuroCcpOwnershipReferenceStaticData datosEstaticosTitular : listDatosEstaticosTitular) {
          datosEstaticosTitular.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO
              .getId());
          datosEstaticosTitular.setAuditDate(new Date());
          datosEstaticosTitular.setAuditUser(auditUser);
          datosEstaticosTitular = dao.save(datosEstaticosTitular);
          msg = MessageFormat.format(
              "EuroCcp-RevEstadoOrgs-ORS-ERR-Unknown owner reference {2} {0} {1} se da de baja el registro enviado.",
              ownerReference, datosEstaticosTitular.getIdentificationCode(), toFrom);
          LOG.warn("[procesarRecepcionTitulares] {}", msg);
          logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

        }
        envTiTo = envioTiDao.findByReferenciaTitularAndFechaInicioAndCdmiembromktAndCdcamara(ownerReference, feejecuc,
            cdmiembromkt, cdcamara);
        if (envTiTo != null && envTiTo.getEstado() != 'R') {
          envTiTo.setAuditFechaCambio(new Date());
          envTiTo.setAuditUser(auditUser);
          envTiTo.setEstado('R');
          msg = MessageFormat.format("EuroCcp-RevEstadoOrgs-ERR-TI-Datos titular {0} rechazados.", ownerReference);
          LOG.warn("[procesarRecepcionTitulares] {}", msg);
          logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
              "EuroCcp-RevEstadoOrgs-ORS-Datos titular rechazados.", "", "EUROCCP_R");
        }
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public synchronized void aceptaDatosEstaticosTiOrsNewTransaction(Tmct0alcId alcId, Date feejecuc,
      String cdmiembromkt, String cdcamara, String ownerReferenceFrom, String ownerReferenceTo, String auditUser) {

    this.aceptaDatosEstaticosTitularTiOrs(alcId, feejecuc, cdmiembromkt, cdcamara, ownerReferenceTo, "to", auditUser);
    this.aceptaDatosEstaticosTitularTiOrs(alcId, feejecuc, cdmiembromkt, cdcamara, ownerReferenceFrom, "from",
        auditUser);

  }

  private void aceptaDatosEstaticosTitularTiOrs(Tmct0alcId alcId, Date feejecuc, String cdmiembromkt, String cdcamara,
      String ownerReference, String toFrom, String auditUser) {

    String msg;
    Tmct0enviotitularidad envTi;
    List<EuroCcpOwnershipReferenceStaticData> listDatosEstaticosTitular;
    if (ownerReference != null && !ownerReference.trim().isEmpty()) {

      listDatosEstaticosTitular = dao.findByOwnerReferenceAndEstadoProcesadoGte(ownerReference,
          EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());

      for (EuroCcpOwnershipReferenceStaticData datosEstaticosTitular : listDatosEstaticosTitular) {
        datosEstaticosTitular.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
        datosEstaticosTitular.setAuditDate(new Date());
        datosEstaticosTitular.setAuditUser(auditUser);
        datosEstaticosTitular = dao.save(datosEstaticosTitular);
        msg = MessageFormat.format(
            "EuroCcp-RevEstadoOrgs-ORS-Datos titularidad {3} {0}-{1} EUROCCP ya estaban aceptados.", ownerReference,
            datosEstaticosTitular.getIdentificationCode(), toFrom);
        LOG.warn("[procesarRecepcionTitulares] {}", msg);
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

      }

      listDatosEstaticosTitular = dao.findByOwnerReferenceAndEstadoProcesadoEq(ownerReference,
          EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());

      for (EuroCcpOwnershipReferenceStaticData datosEstaticosTitular : listDatosEstaticosTitular) {
        datosEstaticosTitular.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
        datosEstaticosTitular.setAuditDate(new Date());
        datosEstaticosTitular.setAuditUser(auditUser);
        datosEstaticosTitular = dao.save(datosEstaticosTitular);
        msg = MessageFormat.format("EuroCcp-RevEstadoOrgs-ORS-Datos titularidad {3} {0}-{1} EUROCCP aceptados.",
            ownerReference, datosEstaticosTitular.getIdentificationCode(), toFrom);
        LOG.warn("[procesarRecepcionTitulares] {}", msg);
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

      }
      envTi = envioTiDao.findByReferenciaTitularAndFechaInicioAndCdmiembromktAndCdcamara(ownerReference, feejecuc,
          cdmiembromkt, cdcamara);
      if (envTi != null && envTi.getEstado() != 'A') {
        envTi.setAuditFechaCambio(new Date());
        envTi.setAuditUser(auditUser);
        envTi.setEstado('A');
        msg = MessageFormat.format("EuroCcp-RevEstadoOrgs-TI-Datos titular {1} {0} aceptado.", ownerReference, toFrom);
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
      }
      else {
        msg = MessageFormat.format("EuroCcp-RevEstadoOrgs-TI-Datos titular {1} {0} ya estaban aceptados.",
            ownerReference, toFrom);
        LOG.debug("[procesarRecepcionTitulares] {}", msg);
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public EuroCcpOwnershipReferenceStaticData saveNewTransaction(EuroCcpFileSent file,
      EuroCcpOwnershipReferenceStaticDataDTO dto, String birthDateFormat) {
    return this.saveNew(file, dto, birthDateFormat);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public EuroCcpOwnershipReferenceStaticData saveNew(EuroCcpFileSent file, EuroCcpOwnershipReferenceStaticDataDTO dto,
      String birthDateFormat) {
    SimpleDateFormat sdf = new SimpleDateFormat(birthDateFormat);
    EuroCcpOwnershipReferenceStaticData hb = new EuroCcpOwnershipReferenceStaticData();
    hb.setOwnerReference(dto.getOwnerReference());
    hb.setOwnerName(dto.getOwnerName());
    hb.setFirstSurname(dto.getFirstSurname());
    hb.setSecondName(dto.getSecondName());
    hb.setTypeIdentificacion(dto.getTypeIdentificacion());
    hb.setIdentificationCode(dto.getIdentificationCode());
    hb.setNaturalLegalInd(dto.getNaturalLegalInd());
    hb.setNationality(dto.getNationality());
    hb.setNationalForeignInd(dto.getNationalForeignInd());
    hb.setOwnerType(dto.getOwnerType());
    hb.setAddress(dto.getAddress());
    hb.setCity(dto.getCity());
    hb.setPostalCode(dto.getPostalCode());
    hb.setCountryResidence(dto.getCountryResidence());
    if (dto.getBirthDate() != null && !dto.getBirthDate().trim().isEmpty()) {
      try {
        hb.setBirthDate(sdf.parse(dto.getBirthDate()));
      }
      catch (ParseException e) {
        LOG.trace(e.getMessage(), e);
      }
    }

    Date firstSentDate = this.findFistDateSentValid(dto.getOwnerReference());
    if (firstSentDate != null) {
      hb.setFirstSentDate(firstSentDate);
    }
    else {
      hb.setFirstSentDate(new Date());
    }
    hb.setFileSent(file);
    hb = save(hb);
    return hb;
  }

  public Date findFistDateSentValid(String ownerReference) {
    return dao.minFirstSentDateByOwnerReferenceAndEstadoProcesadoGte(ownerReference,
        EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
  }

}
