package sibbac.business.comunicaciones.euroccp.ciffile.enums;

public enum EuroCcpAccountType {

  CLIENT("CLNT"),
  HOUSE("HSE"),
  SUSPENSE("SUSP");
  public String text;

  private EuroCcpAccountType(String text) {
    this.text = text;
  }
}
