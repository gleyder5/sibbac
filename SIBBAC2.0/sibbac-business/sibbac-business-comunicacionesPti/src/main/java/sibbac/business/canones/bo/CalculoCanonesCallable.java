package sibbac.business.canones.bo;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class CalculoCanonesCallable implements Callable<Void> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CalculoCanonesCallable.class);

  private boolean isScriptDividend;
  private Date fecha;
  private String isin;
  private char sentido;

  private CanonesConfigDTO configCanones;
  private String auditUser;

  @Autowired
  private CalculoCanonesWithUserLock calculoCanonesWithUserLock;

  @Autowired
  private Tmct0alcBo alcBo;

  public CalculoCanonesCallable(boolean isScriptDividend, Date fecha, String isin, char sentido,
      CanonesConfigDTO configCanones, String auditUser) {
    super();
    this.isScriptDividend = isScriptDividend;
    this.fecha = fecha;
    this.isin = isin;
    this.sentido = sentido;
    this.configCanones = configCanones;
    this.auditUser = auditUser;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    char isscrdiv = isScriptDividend ? 'S' : 'N';
    LOG.debug("[call] buscando bookings fecha: {}, isin: {}, sentido: {} ...", fecha, isin, sentido);
    List<Tmct0bokId> listBookId = alcBo
        .findAllTmct0bokIdByFechaAndCdisinAndSentidoAndCasadoSinCanonCalculadoConReferenciaTitular(fecha, isin,
            sentido, 'N', isscrdiv);

    LOG.debug("[call] encontrados {} bookings fecha: {}, isin: {}, sentido: {} ...", listBookId.size(), fecha, isin,
        sentido);

    for (Tmct0bokId id : listBookId) {
      calculoCanonesWithUserLock.calcularCanon(id, configCanones, auditUser);
    }

    return null;
  }

  public String getId() {
    return String.format("fecha: %s, isin: %s, sentido: %s", this.fecha, this.isin, this.sentido);
  }
}
