package sibbac.business.comunicaciones.euroccp.realigment.mapper;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.RegexPatternMatchingCompositeLineMapper;

public class EuroCcpExecutionRealigmentLineMapper extends
    RegexPatternMatchingCompositeLineMapper<EuroCcpCommonSendRegisterDTO> {

  public EuroCcpExecutionRealigmentLineMapper() {
  }

}
