package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.conciliacion.database.bo.FicheroCuentaECCBo;
import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0caseoperacionejeBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.CodigosOperacion;
import sibbac.common.TiposSibbac.IndicadoresOperacion;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R00;
import sibbac.pti.messages.an.R01;
import sibbac.pti.messages.an.R02;
import sibbac.pti.messages.an.R03;
import sibbac.pti.messages.an.R04;
import sibbac.pti.messages.an.R05;
import sibbac.pti.messages.an.R06;
import sibbac.pti.messages.an.R07;

@Service("ptiMessageProcessAnBo")
public class PtiMessageProcessAnBo extends PtiMessageProcess<AN> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessAnBo.class);

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCCBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;

  @Autowired
  private Tmct0operacioncdeccBo opEccBo;

  @Autowired
  private Tmct0caseoperacionejeBo caseOpEjeBo;

  @Autowired
  private Tmct0anotacioneccBo anotacionEccBo;

  @Autowired
  private FicheroCuentaECCBo ficheroCuentaECCBo;

  public PtiMessageProcessAnBo() {
  }

  @Override
  @SuppressWarnings("unused")
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, AN an, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    // Los objetos.
    R00 r00 = null;
    R01 r01 = null;
    R02 r02 = null;
    R03 r03 = null;
    R04 r04 = null;
    R05 r05 = null;
    R06 r06 = null;
    R07 r07 = null;
    List<PTIMessageRecord> r00s, r01s, r02s, r03s, r04s, r05s, r06s, r07s = null;

    r00s = an.getAll(PTIMessageRecordType.R00);
    r01s = an.getAll(PTIMessageRecordType.R01);
    r02s = an.getAll(PTIMessageRecordType.R02);
    r03s = an.getAll(PTIMessageRecordType.R03);
    r04s = an.getAll(PTIMessageRecordType.R04);
    r05s = an.getAll(PTIMessageRecordType.R05);
    r06s = an.getAll(PTIMessageRecordType.R06);
    r07s = an.getAll(PTIMessageRecordType.R07);

    // LOG.debug("OPERATIVA AN:");

    // siempre ha de venir un solo registro R00 para todo AN
    if (CollectionUtils.isNotEmpty(r00s) && r00s.size() == 1) {
      r00 = (R00) r00s.get(0);
      if (r02s != null && !r02s.isEmpty()) {
        r02 = (R02) r02s.get(0);
      }
      if (r03s != null && !r03s.isEmpty()) {
        r03 = (R03) r03s.get(0);
      }

      if (r04s != null && !r04s.isEmpty()) {
        r04 = (R04) r04s.get(0);
      }

      if (r05s != null && !r05s.isEmpty()) {
        r05 = (R05) r05s.get(0);
      }

      if (r05s != null && !r05s.isEmpty()) {
        r05 = (R05) r05s.get(0);
      }

      if (r06s != null && !r06s.isEmpty()) {
        r06 = (R06) r06s.get(0);
      }

      if (r07s != null && !r07s.isEmpty()) {
        r07 = (R07) r07s.get(0);
      }

      // para el registro de operaciones como para la actualizacion ha de venir
      // un solo R01
      if (CollectionUtils.isNotEmpty(r01s) && r01s.size() == 1) {
        r01 = (R01) r01s.get(0);

        String codigooperacion = r01.getCodigoOperacion().trim();
        String indicadorAnotacion = r01.getIndicadorAnotacion().trim();

        if (IndicadoresOperacion.NUEVA_OPERACION.equals(indicadorAnotacion)) {
          this.procesarNuevaOperacion(codigooperacion, an, r00, r01, r02, cache);
        }
        else if (IndicadoresOperacion.ACTUALIZACION_OPERACION.equals(indicadorAnotacion)) {
          this.procesarActualizacionOperacion(codigooperacion, an, r00, r01, r02, cache);
        }
        else {
          LOG.debug("  > UNKNOWN: Otro tipo de anotacion: [indicadorAnotacion=={}]", r01.getIndicadorAnotacion());
          LOG.error(". La recepcion de los mensaje R01 o R02 son nulos o vacios.");
        }
      }
      else {
        // solo puede ser un mensaje de saldos en cuenta.

        if (CollectionUtils.isNotEmpty(r04s)) {
          LOG.debug("Saldo en cuenta: {} - {}", r00s, r04s);
          ficheroCuentaECCBo.procesarAnSaldoCuenta(an, version);
        }
        else {
          LOG.error(". Mensaje AN erroneo. Numero de registros erroneos");
        }
      }

    }
    else {
      // error
      LOG.info("AN SIN REGISTROS R00 INFORMADOS:" + an.toString());
    }
  }

  private void procesarNuevaOperacion(String codigooperacion, AN an, R00 r00, R01 r01, R02 r02, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug("    > Nueva operacion de mercado {}", codigooperacion);
    // nueva operacion
    if (codigooperacion.equals(CodigosOperacion.OPERACION_DE_ASIGNACION_EXTERNA)
        || codigooperacion.equals(CodigosOperacion.OPERACION_DE_ASIGNACION_IMTERNA)
        || codigooperacion.equals(CodigosOperacion.TRASPASO)) {
      // nueva anotacion
      LOG.debug("    > Asignación externa o Interna o Transpaso--> {}", codigooperacion);
      createNewAnotation(r00, r01, r02, cache);

    }
    else if (isOperacionMercado(codigooperacion)) {

      // nueva operacion
      LOG.debug("    > Nueva operacion de mercado {}", codigooperacion);
      createNewOperation(r00, r01, r02, cache);
      createNewAnotation(r00, r01, r02, cache);
    }
    else if (codigooperacion.equals(CodigosOperacion.GENERACION_INSTRUCCION_DE_LIQUIDACION)) {
      LOG.debug("    > GENERACION_INSTRUCCION_DE_LIQUIDACION: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.PRESTAMO_IDA)) {
      LOG.debug("    > PRESTAMO_IDA: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.PRESTAMO_VUELTA)) {
      LOG.debug("    > PRESTAMO_VUELTA: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.RECOMPRA)) {
      LOG.debug("    > RECOMPRA: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.LIQUIDACION_EN_EFECTIVO)) {
      LOG.debug("    > LIQUIDACION_EN_EFECTIVO: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.ACTUALIZACION_DE_COLATERAL_DE_PRESTAMO)) {
      LOG.debug("    > ACTUALIZACION_DE_COLATERAL_DE_PRESTAMO: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.PRESTAMO_DE_AJUSTE)) {
      LOG.debug("    > PRESTAMO_DE_AJUSTE: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.LIQUIDACION_DE_IL)) {
      LOG.debug("    > LIQUIDACION_DE_IL: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.AJUSTE_POR_EVENTOS)) {
      LOG.debug("    > AJUSTE_POR_EVENTOS: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.NETEO_AGREGACION)) {
      LOG.debug("    > NETEO_AGREGACION: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.CANCELACION)) {
      LOG.debug("    > CANCELACION: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.BILATERAL_PROCEDENTE_DE_IBERCLEAR)) {
      LOG.debug("    > BILATERAL_PROCEDENTE_DE_IBERCLEAR: {} - {}", codigooperacion, an);
    }
  }

  private void procesarActualizacionOperacion(String codigooperacion, AN an, R00 r00, R01 r01, R02 r02, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug("    > Actualizacion operacion de mercado {}", codigooperacion);

    if (codigooperacion.equals(CodigosOperacion.OPERACION_DE_ASIGNACION_EXTERNA)
        || codigooperacion.equals(CodigosOperacion.OPERACION_DE_ASIGNACION_IMTERNA)
        || codigooperacion.equals(CodigosOperacion.TRASPASO) || isOperacionMercado(codigooperacion)) {

      // nueva operacion
      updateOperation(r00, r01, r02, cache);
    }
    else if (codigooperacion.equals(CodigosOperacion.GENERACION_INSTRUCCION_DE_LIQUIDACION)) {
      LOG.debug("    > GENERACION_INSTRUCCION_DE_LIQUIDACION.: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.PRESTAMO_IDA)) {
      LOG.debug("    > PRESTAMO_IDA.: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.PRESTAMO_VUELTA)) {
      LOG.debug("    > PRESTAMO_VUELTA.: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.RECOMPRA)) {
      LOG.debug("    > RECOMPRA.: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.LIQUIDACION_EN_EFECTIVO)) {
      LOG.debug("    > LIQUIDACION_EN_EFECTIVO: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.ACTUALIZACION_DE_COLATERAL_DE_PRESTAMO)) {
      LOG.debug("    > ACTUALIZACION_DE_COLATERAL_DE_PRESTAMO: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.PRESTAMO_DE_AJUSTE)) {
      LOG.debug("    > PRESTAMO_DE_AJUSTE: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.LIQUIDACION_DE_IL)) {
      LOG.debug("    > LIQUIDACION_DE_IL: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.AJUSTE_POR_EVENTOS)) {
      LOG.debug("    > AJUSTE_POR_EVENTOS: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.NETEO_AGREGACION)) {
      LOG.debug("    > NETEO_AGREGACION: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.CANCELACION)) {
      LOG.debug("    > CANCELACION: {} - {}", codigooperacion, an);
    }
    else if (codigooperacion.equals(CodigosOperacion.BILATERAL_PROCEDENTE_DE_IBERCLEAR)) {
      LOG.debug("    > BILATERAL_PROCEDENTE_DE_IBERCLEAR: {} - {}", codigooperacion, an);
    }
  }

  private boolean isOperacionMercado(String codigooperacion) {
    return codigooperacion.equals(CodigosOperacion.COMPRA_VENTA_DE_MERCADO)
        || codigooperacion.equals(CodigosOperacion.BLOQUE)
        || codigooperacion.equals(CodigosOperacion.OPERACION_ESPECIAL)
        || codigooperacion.equals(CodigosOperacion.OPERACIONES_DELTA)
        || codigooperacion.equals(CodigosOperacion.OPERACIONES_A_VALOR_LIQUIDATIVO_SICAVS)
        || codigooperacion.equals(CodigosOperacion.EJERCICIOS_Y_VENCIMIENTOS_DEL_SEGMENTO_DE_DERIVADOS)
        || codigooperacion.equals(CodigosOperacion.BILATERAL_PROCEDENTE_DE_IBERCLEAR);

  }

  /**
   * 
   * @param pm
   * @param r01
   * @param r02
   * @throws Exception
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void createNewAnotation(R00 r00, R01 r01, R02 r02, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("Incio creacion nueva Anotacion ");
    Tmct0anotacionecc anotacionEcc = anotacionEccBo.findByCdoperacionecc(r01.getNumeroOperacion());
    if (anotacionEcc == null) {
      anotacionEcc = new Tmct0anotacionecc();
    }
    mappingDataAnotacionR01(r01, anotacionEcc, cache);
    mappingDataAnotacionR02(r02, anotacionEcc, cache);

    String cdCuentaComp = r00.getCuentaDeCompensacion();
    String miembroGiveUP = r00.getMiembroCompensador();

    if (StringUtils.isNotBlank(miembroGiveUP)) {
      anotacionEcc.setCompensador(compensadorBo.findByNbNombre(miembroGiveUP));
    }

    if (StringUtils.isNotBlank(cdCuentaComp)) {
      Tmct0CuentasDeCompensacion ctaComp = cuentaCCBo.findByCdCodigo(cdCuentaComp);
      if (ctaComp != null) {
        anotacionEcc.setTmct0CuentasDeCompensacion(ctaComp);
      }
    }

    anotacionEcc.setCuentaLiquidacion(r00.getCuentaLiquidacion());
    anotacionEcc.setEntidadParticipante(r00.getEntidadParticipanteBIC());
    anotacionEcc.setAuditFechaCambio(new Date());
    anotacionEcc.setAuditUser("SIBBAC20");
    anotacionEccBo.save(anotacionEcc);
    LOG.debug("Fin creacion nueva Anotacion ");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingDataAnotacionR01(R01 r01, Tmct0anotacionecc anotacionEcc, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug("Incio creacion R01 Anotacion ## cdoperacionecc : {} ", r01.getNumeroOperacion());
    anotacionEcc.setCdoperacionecc(r01.getNumeroOperacion());

    Character cdsentido = cache.getSentidoPtiSentidoSibbac(r01.getSentido());
    if (cdsentido != null) {
      anotacionEcc.setCdsentido(cdsentido);
    }
    else {
      anotacionEcc.setCdsentido(r01.getSentido().charAt(0));
    }
    anotacionEcc.setCdindposicion(r01.getIndicadorPosicion().charAt(0));
    anotacionEcc.setCdoperacion(r01.getCodigoOperacion().charAt(0));
    anotacionEcc.setCdrefasigext(r01.getReferenciaAsignacionExterna());
    anotacionEcc.setFcontratacion(new Date(r01.getFechaContratacion().getTime()));
    anotacionEcc.setFliqteorica(new Date(r01.getFechaLiquidacionTeorica().getTime()));
    anotacionEcc.setFregistroecc(new Date(r01.getFechaDeRegistroEnLaECC().getTime()));
    anotacionEcc.setHregistroecc(new Date(r01.getHoraRegistro().getTime()));
    anotacionEcc.setNutitulos(r01.getValoresImportNominal());
    anotacionEcc.setImprecio(r01.getPrecio());
    anotacionEcc.setImefectivo(r01.getEfectivo());
    anotacionEcc.setImtitulosdisponibles(r01.getValoresImportNominalDisponibles());
    anotacionEcc.setImefectivodisponible(r01.getEfectivoDisponible());
    anotacionEcc.setImefectivoretenido(r01.getEfectivoRetenido());
    anotacionEcc.setImtitulosretenidos(r01.getValoresImporteNominalRetenidos());
    anotacionEcc.setNuoperacionprev(r01.getNumeroOperacionPrevia());
    anotacionEcc.setNuoperacioninic(r01.getNumeroOperacionInicial());
    anotacionEcc.setCdrefcomun(r01.getReferenciaComun());
    try {
      anotacionEcc.setCorretaje(r01.getCorretaje());
    }
    catch (Exception e) {
      anotacionEcc.setCorretaje(BigDecimal.ZERO);
      LOG.error("El corretaje viene mal");
    }
    anotacionEcc.setAuditFechaCambio(new Date());
    LOG.debug("Fin creacion R01 Anotacion ## cdoperacionecc : {} ", r01.getNumeroOperacion());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingDataAnotacionR02(R02 r02, Tmct0anotacionecc anotacionEcc, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug("Inicio creacion R02 Anotacion ## cdoperacionecc : {} ", anotacionEcc.getCdoperacionecc());
    anotacionEcc.setCdcamara(cache.getIdPti());
    anotacionEcc.setCdPlataformaNegociacion(r02.getIdentificacionDeLaPlataforma());
    anotacionEcc.setCdsegmento(r02.getSegmento());
    anotacionEcc.setFnegociacion(new Date(r02.getFechaNegociacion().getTime()));
    anotacionEcc.setHnegociacion(new Date(r02.getHoraNegociacion().getTime()));
    anotacionEcc.setNuexemkt(r02.getNumeroDeEjecucionDeMercado().toString());
    anotacionEcc.setCdoperacionmkt(r02.getCodigoDeOperacionMercado());
    anotacionEcc.setCdmiembromkt(r02.getMiembroMercado());
    anotacionEcc.setCdindcotizacion(r02.getIndicadorCotizacion().charAt(0));
    anotacionEcc.setCdisin(r02.getCodigoDeValor());
    anotacionEcc.setFordenmkt(new Date(r02.getFechaDeLaOrdenDeMercado().getTime()));
    try {
      anotacionEcc.setHordenmkt(new Date(r02.getHoraDeLaOrdenDeMercado().getTime()));
    }
    catch (Exception e) {
      anotacionEcc.setHordenmkt(new Date(r02.getHoraNegociacion().getTime()));
      LOG.warn("La hora de la orden de meracado viene mal, se pondra en su lugar hora de negociacion");
    }
    anotacionEcc.setNuordenmkt(r02.getNumeroDeLaOrdenDeMercado().toString());
    anotacionEcc.setCdrefcliente(r02.getReferenciaDeCliente());
    anotacionEcc.setCdrefextorden(r02.getReferenciaExterna());
    if (StringUtils.isNotBlank(r02.getIndicadorCapacidad())) {
      Character indCapacidad = cache.getIndicadorCapacidadPtiIndicadorCapacidad(r02.getIndicadorCapacidad());
      if (indCapacidad != null) {
        anotacionEcc.setCdindcapacidad(indCapacidad);
      }
      else {
        anotacionEcc.setCdindcapacidad(r02.getIndicadorCapacidad().charAt(0));
      }
    }
    else {
      throw new SIBBACBusinessException("Indicador de capacidad no informado");
    }
    anotacionEcc.setAuditFechaCambio(new Date());
    LOG.debug("Fin creacion R02 Anotacion ## cdoperacionecc : {} ", anotacionEcc.getCdoperacionecc());
  }

  /**
   * Crea una nueva operacion de la cuenta diaria Se da de alta en
   * OPERACIONCDECC en el historico y en CASEOPERACIONEJE
   * 
   * @param r01
   * @param r02
   * @throws Exception
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void createNewOperation(R00 r00, R01 r01, R02 r02, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("Incio creacion OperacionCdEcc ");
    Tmct0operacioncdecc operacioncdecc = new Tmct0operacioncdecc();

    mappingDataOperacionR01(r00, r01, operacioncdecc, cache);
    mappingDataOperacionR02(r02, operacioncdecc, cache);
    operacioncdecc.setEnvioS3("N");
    operacioncdecc = opEccBo.save(operacioncdecc);
    Tmct0caseoperacioneje caseoperacioneje = new Tmct0caseoperacioneje();
    mappingDataCaseOperacion(r01, r02, operacioncdecc, caseoperacioneje);
    // insertarHistOperacion(operacioncdecc);
    caseOpEjeBo.save(caseoperacioneje);
    LOG.debug("Fin creacion OperacionCdEcc ");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingDataOperacionR01(R00 r00, R01 r01, Tmct0operacioncdecc operacioncdecc, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug(
        "Inicio Insertando o Actualizando datos de R00 y R01 OperacionEcc para la operacion ecc ## id : {} ## cdoperacionecc : {}",
        operacioncdecc.getIdoperacioncdecc(), operacioncdecc.getCdoperacionecc());
    Character cdsentido = cache.getSentidoPtiSentidoSibbac(r01.getSentido());
    if (cdsentido != null) {
      operacioncdecc.setCdsentido(cdsentido);
    }
    else {
      operacioncdecc.setCdsentido(r01.getSentido().charAt(0));
    }
    operacioncdecc.setCdindposicion(r01.getIndicadorPosicion().charAt(0));
    operacioncdecc.setCdoperacionecc(r01.getNumeroOperacion());
    operacioncdecc.setCdrefasigext(r01.getReferenciaAsignacionExterna());
    operacioncdecc.setFcontratacion(new Date(r01.getFechaContratacion().getTime()));
    operacioncdecc.setFliqteorica(new Date(r01.getFechaLiquidacionTeorica().getTime()));
    operacioncdecc.setFregistroecc(new Date(r01.getFechaDeRegistroEnLaECC().getTime()));
    operacioncdecc.setHregistroecc(new Date(r01.getHoraRegistro().getTime()));
    operacioncdecc.setImefectivo(r01.getEfectivo());
    operacioncdecc.setNutitulos(r01.getValoresImportNominal());
    operacioncdecc.setImprecio(r01.getPrecio());
    operacioncdecc.setImefectivo(r01.getEfectivo());
    if (operacioncdecc.getImtitulosdisponibles() == null || r01.getValoresImportNominalDisponibles() != null
        && r01.getValoresImportNominalDisponibles().compareTo(operacioncdecc.getImtitulosdisponibles()) < 0) {
      operacioncdecc.setImtitulosdisponibles(r01.getValoresImportNominalDisponibles());
    }
    if (operacioncdecc.getImefectivodisponible() == null || r01.getEfectivoDisponible() != null
        && r01.getEfectivoDisponible().compareTo(operacioncdecc.getImefectivodisponible()) < 0) {
      operacioncdecc.setImefectivodisponible(r01.getEfectivoDisponible());
    }
    if (operacioncdecc.getImefectivoretenido() == null || r01.getEfectivoRetenido() != null
        && r01.getEfectivoRetenido().compareTo(operacioncdecc.getImefectivoretenido()) < 0) {
      operacioncdecc.setImefectivoretenido(r01.getEfectivoRetenido());
    }
    if (operacioncdecc.getImtitulosretenidos() == null || r01.getValoresImporteNominalRetenidos() != null
        && r01.getValoresImporteNominalRetenidos().compareTo(operacioncdecc.getImtitulosretenidos()) < 0) {
      operacioncdecc.setImtitulosretenidos(r01.getValoresImporteNominalRetenidos());
    }
    operacioncdecc.setNuoperacionprev(r01.getNumeroOperacionPrevia());
    operacioncdecc.setNuoperacioninic(r01.getNumeroOperacionInicial());
    operacioncdecc.setCdrefcomun(r01.getReferenciaComun());

    operacioncdecc.setCdtipooperacion(r01.getCodigoOperacion().charAt(0));
    if (r00 != null) {
      String cdCuentaComp = r00.getCuentaDeCompensacion();
      if (StringUtils.isNotBlank(cdCuentaComp)) {
        // String cdMiembro = r00.getMiembro();
        // String cdMiembroComp = r00.getMiembroCompensador();

        // Tmct0Compensador compensador =
        // Tools_tmct0compensador.getTmct0CompensadorByNombre(cdMiembroComp,
        // getHaleePersistenceManager());
        Tmct0CuentasDeCompensacion ctaComp = cuentaCCBo.findByCdCodigo(cdCuentaComp);
        if (ctaComp != null) {
          operacioncdecc.setTmct0CuentasDeCompensacion(ctaComp);
        }
      }
    }
    try {
      operacioncdecc.setCorretaje(r01.getCorretaje());
    }
    catch (Exception e) {
      operacioncdecc.setCorretaje(BigDecimal.ZERO);
      LOG.error("El corretaje viene mal");
    }

    operacioncdecc.setAuditFechaCambio(new Date());

    LOG.debug(
        "Fin Insertando o Actualizando datos de R00 y R01 OperacionEcc para la operacion ecc ## id : {} ## cdoperacionecc : {}",
        operacioncdecc.getIdoperacioncdecc(), operacioncdecc.getCdoperacionecc());

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingDataOperacionR02(R02 r02, Tmct0operacioncdecc operacioncdecc, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug(
        "Inicio Insertando o Actualizando datos de R02 OperacionEcc para la operacion ecc ## id : {} ## cdoperacionecc : {}",
        operacioncdecc.getIdoperacioncdecc(), operacioncdecc.getCdoperacionecc());
    operacioncdecc.setCdcamara(cache.getIdPti());
    operacioncdecc.setCdPlataformaNegociacion(r02.getIdentificacionDeLaPlataforma());
    operacioncdecc.setCdsegmento(r02.getSegmento());
    operacioncdecc.setFnegociacion(new Date(r02.getFechaNegociacion().getTime()));
    operacioncdecc.setHnegociacion(new Date(r02.getHoraNegociacion().getTime()));
    operacioncdecc.setNuexemkt(r02.getNumeroDeEjecucionDeMercado());
    operacioncdecc.setCdoperacionmkt(r02.getCodigoDeOperacionMercado());
    operacioncdecc.setCdmiembromkt(r02.getMiembroMercado());
    operacioncdecc.setCdindcotizacion(r02.getIndicadorCotizacion().charAt(0));
    operacioncdecc.setCdisin(r02.getCodigoDeValor());
    operacioncdecc.setFordenmkt(new Date(r02.getFechaDeLaOrdenDeMercado().getTime()));
    try {
      operacioncdecc.setHordenmkt(new Date(r02.getHoraDeLaOrdenDeMercado().getTime()));
    }
    catch (Exception e) {
      operacioncdecc.setHordenmkt(new Date(r02.getHoraNegociacion().getTime()));
      LOG.warn("La hora de la orden de meracado viene mal, se pondra en su lugar hora de negociacion");
    }
    operacioncdecc.setNuordenmkt(r02.getNumeroDeLaOrdenDeMercado());
    operacioncdecc.setCdrefcliente(r02.getReferenciaDeCliente());
    operacioncdecc.setCdrefextorden(r02.getReferenciaExterna());
    if (StringUtils.isNotBlank(r02.getIndicadorCapacidad())) {
      Character indCapacidad = cache.getIndicadorCapacidadPtiIndicadorCapacidad(r02.getIndicadorCapacidad());
      if (indCapacidad != null) {
        operacioncdecc.setCdindcapacidad(indCapacidad);
      }
      else {
        operacioncdecc.setCdindcapacidad(r02.getIndicadorCapacidad().charAt(0));
      }
    }
    else {
      throw new SIBBACBusinessException("Indicador de capacidad no informado");
    }
    operacioncdecc.setNuordenmkt(operacioncdecc.getNuordenmkt());
    operacioncdecc.setAuditFechaCambio(new Date());

    LOG.debug(
        "Fin Insertando o Actualizando datos de R02 OperacionEcc para la operacion ecc ## id : {} ## cdoperacionecc : {}",
        operacioncdecc.getIdoperacioncdecc(), operacioncdecc.getCdoperacionecc());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingDataCaseOperacion(R01 r01, R02 r02, Tmct0operacioncdecc operacioncdecc,
      Tmct0caseoperacioneje caseoperacioneje) throws SIBBACBusinessException {
    LOG.debug("Inicio Creacion caseoperacioneje ## cdoperacionecc : {}", r01.getNumeroOperacion());
    caseoperacioneje.setTmct0operacioncdecc(operacioncdecc);

    caseoperacioneje.setCdoperacionecc(operacioncdecc.getCdoperacionecc());
    caseoperacioneje.setNuexemkt(operacioncdecc.getNuexemkt());

    caseoperacioneje.setCdcamara(operacioncdecc.getCdcamara());
    caseoperacioneje.setCdPlataformaNegociacion(operacioncdecc.getCdPlataformaNegociacion());
    caseoperacioneje.setFeordenmkt(operacioncdecc.getFordenmkt());

    try {
      caseoperacioneje.setHoordenmkt(new Date(r02.getHoraDeLaOrdenDeMercado().getTime()));
    }
    catch (Exception e) {
      caseoperacioneje.setHoordenmkt(new Date(r02.getHoraNegociacion().getTime()));
    }
    caseoperacioneje.setFeexemkt(operacioncdecc.getFcontratacion());
    caseoperacioneje.setHoexemkt(operacioncdecc.getHnegociacion());
    caseoperacioneje.setNuordenmkt(operacioncdecc.getNuordenmkt());
    caseoperacioneje.setCdsentido(operacioncdecc.getCdsentido());

    caseoperacioneje.setCdsegmento(operacioncdecc.getCdsegmento());
    caseoperacioneje.setNutitulostotal(r01.getValoresImportNominal());
    caseoperacioneje.setCdisin(operacioncdecc.getCdisin());
    caseoperacioneje.setNutitpendcase(new BigDecimal(caseoperacioneje.getNutitulostotal().toString()));
    caseoperacioneje.setAuditFechaCambio(new Date());
    caseoperacioneje.setNutitpendcase(new BigDecimal(caseoperacioneje.getNutitulostotal().toString()));
    LOG.debug("Fin Creacion caseoperacioneje ## cdoperacionecc : {}", r01.getNumeroOperacion());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void updateOperation(R00 r00, R01 r01, R02 r02, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("Incio actualizacion OperacionCdEcc ");
    Tmct0operacioncdecc opAntigua = opEccBo.findByCdoperacionecc(r01.getNumeroOperacion());

    if (opAntigua != null) {
      mappingDataOperacionR01(r00, r01, opAntigua, cache);
      mappingDataOperacionR02(r02, opAntigua, cache);
      opAntigua = opEccBo.save(opAntigua);
    }
    Tmct0anotacionecc anotacion = anotacionEccBo.findByCdoperacionecc(r01.getNumeroOperacion());
    if (anotacion != null) {
      mappingDataAnotacionR01(r01, anotacion, cache);
      mappingDataAnotacionR02(r02, anotacion, cache);
      anotacionEccBo.save(anotacion);
    }
    else {
      createNewAnotation(r00, r01, r02, cache);// añadido
    }

    LOG.debug("Fin actualizacion OperacionCdEcc ");
  }

}